--   ======================================================================= 
--       Copyright (c) 1996 Oracle Corporation, Redwood Shores, CA, USA      
--                            All rights reserved.                           
--   ======================================================================= 
--    FILENAME                                                               
--     	OEXUDEPS.pls                                                         
--                                                                           
--    DESCRIPTION     							     
--    	                                                      	  	     
--      Package spec OE_Dependencies					     
-- 
--	This package contains a procedure which marks dependent
--	attributes so that they can be cleared when the source
--	attribute is changed.
--
--	We are currently hard coding the dependent attributes in
--	the package body (in a plsql table a records), but long term
--	the dependencies will be stored in database tables and we
--	will load the table of records from that rather than hard 
--	code it.
--
--    NOTES                                                                  
--    HISTORY                                                                
--     	01-24-97  Amr A El-Arabaty Created				     
--      05-NOV-2001   Nithya Lakshmanan  Bug 2092104: Introduced Dep_Rec/Tbl_Type
--                                       structures so that enabled_flag can also
--                                       be stored in dependencies table.
--   ======================================================================= 

SET VERIFY OFF;
WHENEVER OSERROR EXIT FAILURE ROLLBACK;
WHENEVER SQLERROR EXIT FAILURE ROLLBACK;
REM Added for ARU db drv auto generation
REM dbdrv: sql ~PROD ~PATH ~FILE none none none package &phase=pls checkfile(115.6=120.0):~PROD:~PATH:~FILE

CREATE or REPLACE PACKAGE OE_Dependencies AS
/* $Header: OEXUDEPS.pls 120.0.12010000.1 2008/07/25 07:55:32 appldev ship $ */

--  Max number of dependent attributes.

G_MAX          CONSTANT    NUMBER := 100;

--  Global table holding attribute dependencies.

TYPE Dep_Rec_Type IS RECORD
(attribute               NUMBER
,enabled_flag            VARCHAR2(1) := 'Y'
);

TYPE Dep_Tbl_TYPE IS TABLE OF Dep_Rec_Type
INDEX BY BINARY_INTEGER;

g_dep_tbl          Dep_Tbl_Type;

g_entity_code      VARCHAR2(30) := NULL;


PROCEDURE   Mark_Dependent
(   p_entity_code	IN  VARCHAR2				,
    p_source_attr_tbl	IN  OE_GLOBALS.Number_Tbl_Type :=
				OE_GLOBALS.G_MISS_NUMBER_TBL	,
    p_dep_attr_tbl	OUT NOCOPY /* file.sql.39 change */  OE_GLOBALS.Number_Tbl_Type
);


PROCEDURE   Clear_Dependent_Table;

END OE_Dependencies;
/

COMMIT;
EXIT;















