SET SERVEROUTPUT ON SIZE 1000000
SET ECHO OFF 
SET VERIFY OFF
SET DEFINE "&"
DECLARE
  --
  v_source_system varchar2(30) := '&1';
  --
  v_purchasing_site   varchar2(1) :='Y';
  v_create_dm         varchar2(1) :='Y';
  --
  v_vendor_name       ap_suppliers.vendor_name%type;
  v_vendor_site_code  ap_supplier_sites_all.vendor_site_code%type;
  --
  b_job_status        boolean;
  n_vendor_site_id    ap_supplier_sites_all.vendor_site_id%type;
  --
  cursor gsc_vendor_sites is
	select aps.vendor_id          vendor_id
          ,aps.vendor_name        vendor_name
          ,apss.vendor_site_code  vendor_site_code
          ,apss.vendor_site_id    vendor_site_id
	from ap_suppliers           aps,
	   ap_supplier_sites_all    apss,
	   ap_invoices_all          aip
	where 1 = 1
	and apss.org_id         =mo_global.get_current_org_id
    --and apss.vendor_site_id =2235
	and aps.vendor_id       =aip.vendor_id
	and aps.vendor_id       =apss.vendor_id
	and aip.vendor_site_id  =apss.vendor_site_id
	and aip.attribute11     =v_source_system --'P2P'
	and ((nvl(apss.purchasing_site_flag, 'N') ='N') OR (nvl(apss.create_debit_memo_flag, 'N') ='N')) --added 09/25/2014
	--and (apss.purchasing_site_flag is null  or apss.create_debit_memo_flag is null) --commented out 09/25/2014
	group by aps.vendor_id, aps.vendor_name, apss.vendor_site_code, apss.vendor_site_id;
  --
  type gsc_sites_tbl is table of gsc_vendor_sites%rowtype index by binary_integer;
  gsc_sites gsc_sites_tbl;
  --
  procedure print_output (p_message in varchar2) is
  begin 
   if (fnd_global.conc_request_id >0) then
    fnd_file.put_line(fnd_file.output, p_message);
   else
    dbms_output.put_line(p_message);
   end if;
  exception
   when others then 
    dbms_output.put_line('Error in print_output, msg ='||sqlerrm);
  end print_output;
  --
  procedure print_log (p_message in varchar2) is
  begin 
   if (fnd_global.conc_request_id >0) then
    fnd_file.put_line(fnd_file.log, p_message);
   else
    dbms_output.put_line(p_message);
   end if;
  exception
   when others then 
    dbms_output.put_line('Error in print_log, msg ='||sqlerrm);
  end print_log;
  --  
BEGIN
  print_log(' ');
  print_log('Current ORG_ID: '||mo_global.get_current_org_id);
  print_log(' ');
  begin 
   if (mo_global.get_current_org_id is not null) then
    --
    begin 
     --
     open  gsc_vendor_sites;
     fetch gsc_vendor_sites bulk collect into gsc_sites;
     close gsc_vendor_sites;
     --  
     if gsc_sites.count >0 then
      --
      for idx in gsc_sites.first .. gsc_sites.last loop
       --
       v_vendor_name       :=gsc_sites(idx).vendor_name;
       v_vendor_site_code  :=gsc_sites(idx).vendor_site_code;
       n_vendor_site_id    :=gsc_sites(idx).vendor_site_id;
       --
       begin  
        --
        savepoint init_here;
        update ap_supplier_sites_all
           set purchasing_site_flag    =case when purchasing_site_flag is null then v_purchasing_site else purchasing_site_flag end
              ,create_debit_memo_flag  =case when create_debit_memo_flag is null then v_create_dm else purchasing_site_flag end
        where  1 =1
          and  vendor_site_id =gsc_sites(idx).vendor_site_id;
        --
        if (SQL%Rowcount >0) then
         print_output(
                        'Purchasing Site /Create Debit Memo flags are set for Vendor: '
                       ||gsc_sites(idx).vendor_name
                       ||', Site Code: '
                       ||gsc_sites(idx).vendor_site_code
                     );
        else
         print_output(
                        'Failed to set Purchasing Site /Create Debit Memo flags for Vendor: '
                       ||gsc_sites(idx).vendor_name
                       ||', Site Code: '
                       ||gsc_sites(idx).vendor_site_code         
                     );        
        end if;
        --
       exception
        when others then
          print_log
              (
                   'Update failed for vendor / site_code /site_id : '||
                 gsc_sites(idx).vendor_name||' /'||
                 gsc_sites(idx).vendor_site_code||' /'||
                 gsc_sites(idx).vendor_site_id
              );        
          rollback to init_here;
       end;
       --
      end loop;
      --
     else
      --
      print_log('Exit, gsc_sites.count =0');
      --
     end if;
     --
    exception
     when others then
      print_log('Issue in fetch of cursor gsc_vendor_sites, msg ='||sqlerrm); 
    end;
    --
   else
    print_log('Operating unit is not set, Please check the concurrent job submit single request window.');
    --
    b_job_status := Fnd_Concurrent.Set_Completion_Status('ERROR', 'Operating unit is not set, Please check the concurrent job submit single request window.');
    --
   end if;
  exception
   when others then
    print_log('Inside Main, current vendor_name /site_code /site_id : '||v_vendor_name||' /'||v_vendor_site_code||' /'||n_vendor_site_id);
    print_log('Inside Main, msg ='||sqlerrm);
    rollback;
  end;
EXCEPTION 
 WHEN OTHERS THEN
  print_log('@GSC supplier site flag update, outer block, msg ='||sqlerrm);
END;
/
EXIT;
/