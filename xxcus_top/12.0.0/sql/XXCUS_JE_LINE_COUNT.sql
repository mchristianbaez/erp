-- ================================================================
-- Date     : 09/18/2009
-- Author   : John Bozik
-- Activity : Script to get JE line counts
--
-- ================================================================
 
clear buffer
set serveroutput on
set feedback on
TTITLE 'Oracle JE Line Counts :by Total: by Category :by Source'
SET HEADING ON
set termout on 


DEFINE  Period_Name = '&1';


select gcc.segment1 Product, count(*) 
  from gl.gl_code_combinations gcc, gl.gl_je_lines glj
 where glj.period_name = '&Period_Name'
   and gcc.code_combination_id = glj.code_combination_id
 group by gcc.segment1
 order by gcc.segment1
/ 
select gcc.segment1 Product, glg.user_je_category_name Category, count(*)
  from gl.gl_code_combinations gcc,
       gl.gl_je_lines          glj,
       gl.gl_je_headers        glh,
       gl.gl_je_categories_tl  glg
 where glj.period_name = '&Period_Name'
   and glj.period_name = glh.period_name
   and gcc.code_combination_id = glj.code_combination_id
   and glj.je_header_id = glh.je_header_id
   and glh.je_category = glg.je_category_name
   and glh.currency_code in ('USD','CAD')
 group by gcc.segment1, glg.user_je_category_name
 order by segment1
/
select gcc.segment1 Product, gls.user_je_source_name Source, count(*)
  from gl.gl_code_combinations gcc,
       gl.gl_je_lines          glj,
       gl.gl_je_headers        glh,
       gl.gl_je_sources_tl     gls
 where glj.period_name = '&Period_Name'
   and glj.period_name = glh.period_name
   and gcc.code_combination_id = glj.code_combination_id
   and glj.je_header_id = glh.je_header_id
   and glh.je_source = gls.je_source_name
   and glh.currency_code in ('USD','CAD')
 group by gcc.segment1, gls.user_je_source_name
 order by segment1
/
 
set serveroutput off
set feedback off
TTITLE off 
set heading off
clear buffer 
