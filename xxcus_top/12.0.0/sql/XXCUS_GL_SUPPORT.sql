-- ================================================================
-- Date     : 09/18/2009
-- Author   : John Bozik
-- Activity : Script to check GL interfaces
--
-- ================================================================

set newpage 2
clear buffer
set serveroutput on
--set feedback on
--TTITLE LEFT 'Run Date'
SET HEADING ON
set termout on 
col sysdate                    heading SYSTEM_DATE          format a15
select sysdate from dual
/ 


TTITLE LEFT 'Total Number of Records in GL_INTERFACE:'
col count(*)	heading TOTAL_RECORDS   format 9999999999
select count(*) from gl.gl_interface
/ 


TTITLE LEFT 'Total Number of Records in GL_INTERFACE by Status and Source:'
col STATUS                    	heading STATUS          format a10
col user_je_source_name       	heading SOURCE_NAME     format a20
col count(user_je_source_name)  heading TOTAL_RECORDS   format 9999999999
select distinct status, user_je_source_name, count(user_je_source_name)
from gl.gl_interface
group by status, user_je_source_name
/ 


TTITLE LEFT 'Posted Batches by Source:'
col gls.user_je_source_name       	heading SOURCE_NAME     format a20
col count     				heading TOTAL_RECORDS   format 9999999999
SELECT   gls.user_je_source_name,COUNT(DISTINCT glh.je_batch_id) Count
from  gl.gl_je_headers glh, gl.gl_je_sources_tl gls
where glh.je_source = gls.je_source_name
and glh.posted_date like sysdate
group by   gls.user_je_source_name
order by user_je_source_name
/ 


TTITLE LEFT 'Non-Posted Batches. USD (2061) and CAD (2063) Ledgers ONLY:'
SET HEADING ON
SET LINESIZE 1000
SET RECSEP OFF
col status				heading STATUS		format a10
col default_period_name		heading PERIOD_NAME		format A11
col ledger_id				heading LEDGER_ID		format 999999
col email_address			heading CREATOR_EMAIL	format a35
col name				heading BATCH_NAME		format A70 WORD_WRAPPED
select glb.status, glb.default_period_name, glh.ledger_id, fnd.email_address, glb.name
from gl.gl_je_batches glb, apps.fnd_user fnd, gl.gl_je_headers glh
where glb.status <> 'P'
and glb.je_batch_id = glh.je_batch_id
and glb.created_by = fnd.user_id
and glh.ledger_id in (2061,2063)
order by glb.default_effective_date, glh.ledger_id
/ 


clear columns
TTITLE LEFT 'Non-Posted Batches. All Other Ledgers:'
SET HEADING ON
SET LINESIZE 1000
SET RECSEP OFF
col status				heading STATUS		format a6
col default_period_name		heading PERIOD_NAME		format A11
col ledger_id				heading LEDGER_ID		format 99999
col gll.name				heading LEDGER_NAME		format a20
col email_address			heading CREATOR_EMAIL	format a35
col glb.name				heading BATCH_NAME		format A70 WORD_WRAPPED

select glb.status, glb.default_period_name, glh.ledger_id, gll.name, fnd.email_address, glb.name
from gl.gl_je_batches glb, apps.fnd_user fnd, gl.gl_je_headers glh, gl.gl_ledgers gll
where glb.status <> 'P'
and glb.je_batch_id = glh.je_batch_id
and glb.created_by = fnd.user_id
and glh.ledger_id NOT in (2061,2063)
and glh.ledger_id = gll.ledger_id(+)
order by glb.default_effective_date, glh.ledger_id
/


set serveroutput off
set feedback off
TTITLE off 
set heading off
clear buffer 

