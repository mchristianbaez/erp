SET SERVEROUTPUT ON SIZE 1000000
SET ECHO OFF 
SET VERIFY OFF
SET DEFINE "&"
DECLARE
  --
  source_folder  varchar2(150) :='&1'; 
  source_file1   varchar2(150) :='&2'; 
  source_file2   varchar2(150) :='&3';
  zip_folder     varchar2(150) :='&4'; 
  zip_file       varchar2(150) :='&5';  
  v_command      varchar2(4000) :=Null;
  v_result       varchar2(4000) :=Null;
  --
  procedure print_log(p_message in varchar2) is
  begin
   if fnd_global.conc_request_id >0 then
    fnd_file.put_line(fnd_file.log, p_message);
   else
    dbms_output.put_line(substr(p_message, 1, 255));
   end if;
  exception
   when others then
    fnd_file.put_line(fnd_file.log, 'Issue in print_log routine ='||sqlerrm);
  end print_log;
  --
BEGIN
  --
  print_log('');
  print_log('Source Folder   ='||source_folder);
  print_log('Source File1    ='||source_file1);
  print_log('Source File2    ='||source_file2);
  print_log('Zip File Folder ='||zip_folder);
  print_log('Zip File        ='||zip_file);  
  print_log(''); 
  --
  BEGIN 
	-- change file permissions for easy move or delete
	--
	v_command :='chmod 777'
			   ||' '
			   ||source_folder
			   ||'/' 
			   ||source_file1; 
	  --
	  SELECT XXWC_EDI_IFACE_PKG.XXWC_OSCOMMAND_RUN (v_command)
	  INTO   v_result 
	  FROM   DUAL; 
	--
	if (v_result is null) then
	  -- 
      Null;
	  -- 
	else
	   dbms_output.put_line('Failed to modify permission for '||source_folder||'/'||source_file1); 
       dbms_output.put_line('Msg for source_file1 ='||v_result);	   	   
	end if;
	--
	v_command :='chmod 777'
			   ||' '
			   ||source_folder
			   ||'/' 
			   ||source_file2; 
	  --
	  SELECT XXWC_EDI_IFACE_PKG.XXWC_OSCOMMAND_RUN (v_command)
	  INTO   v_result 
	  FROM   DUAL; 
	--
	if (v_result is null) then
	  -- 
      Null;
	  -- 
	else
	   dbms_output.put_line('Failed to modify permission for '||source_folder||'/'||source_file2);
       dbms_output.put_line('Msg for source_file2 ='||v_result);	   
	end if;
	--
	-- create the zip file
    v_command := 'zip -j' 
				 ||' '
				 ||zip_folder
				 ||'/' 
				 ||zip_file
				 ||' '                    
				 ||source_folder
				 ||'/'
				 ||source_file1
				 ||' '
				 ||source_folder
				 ||'/'                     
				 ||source_file2;
	  --
	  SELECT XXWC_EDI_IFACE_PKG.XXWC_OSCOMMAND_RUN (v_command)
	  INTO   v_result 
	  FROM   DUAL; 
	--
	if (v_result is null) then
	  -- 
      v_command := 'chmod 777' 
				 ||' '
				 ||zip_folder
				 ||'/' 
				 ||zip_file;
	  --
	  SELECT XXWC_EDI_IFACE_PKG.XXWC_OSCOMMAND_RUN (v_command)
	  INTO   v_result 
	  FROM   DUAL;
	  -- 
	else
	   dbms_output.put_line('Failed to create zip '||zip_folder||'/'||zip_file);  
	   dbms_output.put_line('Zip file creation, msg ='||v_result);
	end if;
	--	
  EXCEPTION
	WHEN OTHERS THEN 
	 dbms_output.put_line('Error in calling JAVA OS Command, msg ='||sqlerrm);
  END; 
EXCEPTION 
 WHEN OTHERS THEN
  dbms_output.put_line('Error encountered: '||sqlerrm);
END;
/
EXIT;
/