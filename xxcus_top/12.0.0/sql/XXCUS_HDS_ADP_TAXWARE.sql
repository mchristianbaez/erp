 /* *******************************************************************************************
  * HDSupply - Oracle Applications
  * Author: Balaguru Seshadri
  * Module: Oracle EB Tax
  * Scope: ADP taxware Monthly Updates.
  * Called by: Concurrent program XXHDS Upload Monthly Taxware Updates
  * &&1 => Caller code [ Use PARTIAL_TAX or PARTIAL_PROD ]
  * &&2 => P_FILE_PATH
  * &&3 => P_FILE_NAME
  * &&4 => Request Submitted by
  * &&5 => Request ID
 ******************************************************************************************* */

SET VERIFY OFF 
SET PAGESIZE 0;
SET LINESIZE 79
WHENEVER SQLERROR EXIT FAILURE ROLLBACK;
WHENEVER OSERROR  EXIT FAILURE ROLLBACK;
SET SERVEROUTPUT ON SIZE 1000000;
VARIABLE P_TEXT1 VARCHAR2(2000);
VARIABLE P_TEXT2 VARCHAR2(2000);

DECLARE
    p_caller_code  VARCHAR2(30)   :='&&1';
    p_file_path    VARCHAR2(100) :='&&2';
    p_file_name    VARCHAR2(50)  :='&&3';
    p_user_id      NUMBER        :=&&4;
    p_req_id       NUMBER        :=&&5;
    retc VARCHAR2(2000);
    ebuf VARCHAR2(2000);
    
BEGIN
 if p_caller_code ='PARTIAL_TAX' then

    :P_TEXT1 := 'Before calling Taxware.taxsp_partial_tax_update for file ['||p_file_name||']';
  DBMS_OUTPUT.PUT_LINE(LPAD(' ',12,' ')||'          *****   '||:P_TEXT1||'    *****');
  
     Taxware.taxsp_partial_tax_update(p_file_path, p_file_name);
  
    :P_TEXT2 := 'After calling Taxware.taxsp_partial_tax_update for file ['||p_file_name||']';
  DBMS_OUTPUT.PUT_LINE(LPAD(' ',12,' ')||'          *****   '||:P_TEXT2||'    *****'); 
    
 elsif p_caller_code ='PARTIAL_PROD' then

    :P_TEXT1 := 'Before calling Taxware.taxsp_partial_prod_update for file ['||p_file_name||']';
  DBMS_OUTPUT.PUT_LINE(LPAD(' ',12,' ')||'          *****   '||:P_TEXT1||'    *****');
  
     Taxware.taxsp_partial_prod_update (p_file_path, p_file_name);
  
    :P_TEXT2 := 'After calling Taxware.taxsp_partial_prod_update for file ['||p_file_name||']';
  DBMS_OUTPUT.PUT_LINE(LPAD(' ',12,' ')||'          *****   '||:P_TEXT2||'    *****'); 
    
 else
 
  :P_TEXT2 :='Unknown parameter, Not sure which taxware partial plsql package to invoke';
  DBMS_OUTPUT.PUT_LINE(LPAD(' ',12,' ')||'          *****   '||:P_TEXT2||'    *****');
  
 end if; 

EXCEPTION
 WHEN OTHERS THEN
  RAISE_APPLICATION_ERROR(-20010,'File: xxcus_hds_adp_txware.sql'||SQLERRM);
END; 
/
EXIT;

