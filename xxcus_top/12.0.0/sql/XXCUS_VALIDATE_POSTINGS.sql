-- ================================================================
-- Date     : 10/16/2004
-- Author   : John Bozik
-- Activity : Script to check for prior period postings
--
-- ================================================================

clear buffer
set serveroutput on
set feedback on
TTITLE 'Prior Period Postings|Report'
SET HEADING ON
set termout off


col name                    heading JOURNAL_NAME          format a40
col period_name             heading PERIOD            format a8
col posted_date             heading POSTED_DATE            format a11
col user_name               heading POSTED_BY            format a12      

DEFINE  ydate = '&1';
DEFINE  Period1 = '&2';
DEFINE  Period2 = '&3';

select b.name, b.period_name, b.posted_date, a.user_name
  from apps.fnd_user a, gl.gl_je_headers b
 where status = 'P'
   and a.user_id = b.last_updated_by
   and b.posted_date like '&ydate%'
   and  b.period_name not like '&Period1'
  and b.period_name not like '&Period2'
/

set serveroutput off
set feedback off
TTITLE off
set heading off
clear buffer