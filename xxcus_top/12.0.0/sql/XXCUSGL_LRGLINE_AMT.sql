-- ================================================================
-- Date     : 4/15/2014
-- Author   : John Bozik
-- Activity : Script to check for posted large lines
--
-- ================================================================
 
clear buffer
SET LINESIZE 80
SET PAGESIZE 9999
SET MARKUP HTML ON SPOOL ON
SET HEADING ON

SET MARKUP -
HTML ON -
HEAD '<TITLE>Journal Line Large Amount Check</TITLE>' -
BODY "" -

TTITLE CENTER BOLD 'Journal Line Large Amount Check' SKIP 3 -

DEFINE  p_line_of_bus = '&1';
DEFINE  p_period_name = '&2';
DEFINE  p_amount_threshold = '&3'; 

select   jeb.name as "Batch Name",
         jes.user_je_source_name "Source",
         jeh.name "Journal Name",
         jel.effective_date "Effective Date",
         jel.je_line_num "Line Number",
         gcc.segment1 || '.' || gcc.segment2 || '.' || gcc.segment3 || '.' ||
         gcc.segment4 || '.' || gcc.segment5 || '.' || gcc.segment6 "GL Account Combination",
         jel.entered_dr "Debit",
         jel.entered_cr "Credit"
    from gl.gl_je_lines          jel,
         gl.gl_code_combinations gcc,
         gl.gl_je_headers        jeh,
         gl.gl_je_batches        jeb,
         apps.gl_je_sources_vl   jes
   where 1 = 1
     and upper(gcc.segment1) = upper('&p_line_of_bus') --Parameter: Line of Business
     and jeh.period_name = '&p_period_name' --Parameter: Period name
     and (jel.entered_dr >= ('&p_amount_threshold') --Parameter: Amount Threshold
         OR jel.entered_dr <= ('-' || '&p_amount_threshold') --Parameter: Amount Threshold
         OR jel.entered_cr >= ('&p_amount_threshold') --Parameter: Amount Threshold
         OR jel.entered_cr <= ('-' || '&p_amount_threshold')) --Parameter: Amount Threshold
     and jel.je_header_id = jeh.je_header_id
     and jeh.je_batch_id = jeb.je_batch_id
     and jel.code_combination_id = gcc.code_combination_id
     and jeh.je_source = jes.je_source_name
     and jeh.ledger_id in (2061, 2063)
   order by jel.effective_date, jeb.name, jeh.name, jel.je_line_num
;

SPOOL OFF
SET MARKUP HTML OFF

