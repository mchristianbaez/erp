LOAD DATA
INFILE *
INTO TABLE xxcus.xxcusce_999_interface_tbl
APPEND
FIELDS TERMINATED BY '|'
optionally enclosed by '"'
TRAILING NULLCOLS
(
Export_id,
Aba_number,
Cust_Acct_number,
bank_account_number,
Bank_batch_id,
Bank_trans_id,
Anydoc_trans_id,
trx_date, 
amount,
trx_number,
Document_type,
Biztalk_batch_id,
Creation_date,
image_url,
status_dsp        CONSTANT 'Available',
status            CONSTANT 'Available',
trx_type     	CONSTANT 'CASH',
currency_code     CONSTANT 'USD',
summerized		CONSTANT 'N',
request_id		CONSTANT '-1',
file_name		CONSTANT 'NADA'
 )