LOAD DATA APPEND
INTO TABLE gl.gl_interface
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
   (ledger_id             "LTRIM(RTRIM(:ledger_id))",
    reference1            "LTRIM(RTRIM(:reference1))",
    accounting_date       "TO_DATE(LTRIM(RTRIM(:accounting_date)), 'DD-MON-RRRR')",
    period_name           "LTRIM(RTRIM(:period_name))",
    currency_code         "LTRIM(RTRIM(:currency_code))",
    segment1              "LTRIM(RTRIM(:segment1))",
    segment2              "LTRIM(RTRIM(:segment2))",
    segment3              "LTRIM(RTRIM(:segment3))",
    segment4              "LTRIM(RTRIM(:segment4))",
    segment5              "LTRIM(RTRIM(:segment5))",
    segment6              "LTRIM(RTRIM(:segment6))",
    segment7              "LTRIM(RTRIM(:segment7))",
    entered_dr            ,
    entered_cr            ,
    reference2            "LTRIM(RTRIM(:reference1))",
    reference4            "LTRIM(RTRIM(:reference1))",
    reference5            "LTRIM(RTRIM(:reference1))",
    date_created          SYSDATE,
    created_by            CONSTANT 7,
    status                CONSTANT 'NEW',
    actual_flag           CONSTANT 'A',
    user_je_source_name   CONSTANT 'Conversion',
    user_je_category_name CONSTANT 'Other'
    )

