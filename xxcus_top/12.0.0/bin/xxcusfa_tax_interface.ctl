LOAD DATA
INFILE *
INTO TABLE FA.FA_Tax_Interface
APPEND
FIELDS TERMINATED BY ','
optionally enclosed by '"'  
TRAILING NULLCOLS
(  book_type_code,
   Asset_number,
   date_placed_in_service,
   DEPRN_METHOD_CODE,
   LIFE_IN_MONTHS,
   cost,
   YTD_DEPRN,
   DEPRN_RESERVE,
   Posting_status CONSTANT 'POST'
   )
