-- ******************************************************************************
-- *  Copyright (c) 2014 HDS Supply
-- *  All rights reserved.
-- ******************************************************************************
-- *   $Header XXCUSCE_PNC_INBOUND.CTL $
-- *   Module Name: XXCUSCE_PNC_INBOUND.ctl
-- *
-- *   PURPOSE:   This is the control file for loading the staging table XXWC.XXCUSCE_PNC_INBOUND_STG_TBL
-- *
-- *   REVISIONS:
-- *   Ver        Date        Author                     Description
-- *   ---------  ----------  ---------------         -------------------------
-- *   1.0        02/18/2014  Maharajan Shunmugam	Initial Version
-- * ****************************************************************************

LOAD DATA
INFILE *
TRUNCATE
INTO TABLE XXCUS.XXCUSCE_PNC_INBOUND_STG_TBL
WHEN (01) = 'I'
FIELDS TERMINATED BY ','
optionally enclosed by '"'
TRAILING NULLCOLS
( RECORD_TYPE          ,
  INVOICE_NUMBER       ,
  INVOICE_DATE         "to_date(:INVOICE_DATE,'MM/DD/RRRR')",
  CLIENT_REF_NUMBER    ,
  DOCUMENT_NUMBER      ,
  INVOICE_AMOUNT       ,
  TAX_AMOUNT           ,
  DISCOUNT_AMOUNT      ,
  CREATION_DATE        SYSDATE)
 