-- ******************************************************************************
-- *  Copyright (c) 2014 HDS Supply
-- *  All rights reserved.
-- ******************************************************************************
-- *   $Header XXCUSCE_BOA_INBOUND.CTL $
-- *   Module Name: XXCUSCE_BOA_INBOUND.ctl
-- *
-- *   PURPOSE:   This is the control file for loading the staging table XXCUS.XXCUSCE_BOA_INBOUND_STG_TBL
-- *
-- *   REVISIONS:
-- *   Ver        Date        Author                     Description
-- *   ---------  ----------  ---------------         -------------------------
-- *   1.0        06/12/2018  Vamshi Singirikonda   	Initial Version (TMS 20180227-00179)
-- * ****************************************************************************
OPTIONS(skip=1)
LOAD DATA
INFILE *
TRUNCATE
INTO TABLE XXCUS.XXCUSCE_BOA_INBOUND_STG_TBL
FIELDS TERMINATED BY '|'
optionally enclosed by '"'
TRAILING NULLCOLS
(   Req_Name ,
  Req_Date "to_date(:Req_Date,'MM/DD/RRRR')",
  Card_Nickname ,
  Vendor_Name ,
  Amount ,
  Purchase_Date "to_date(:Purchase_Date,'MM/DD/RRRR')",
  Post_Date "to_date(:Post_Date,'MM/DD/RRRR')",
  Txn_Number ,
  Req_Number ,
  CREATION_DATE        SYSDATE)