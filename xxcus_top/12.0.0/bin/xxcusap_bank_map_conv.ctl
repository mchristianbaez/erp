LOAD DATA
INFILE *
REPLACE
INTO TABLE xxcus.xxcusap_bank_map_tbl
FIELDS TERMINATED BY ','
optionally enclosed by '"'
TRAILING NULLCOLS
(
branch_num,
r11_bank_name,
r12_bank_name,
r12_branch_name,
r11_branch_name,
address_line1,
address_line2,
address_line3, 
city,
state,
zip,
country,
phone,
record_cnt
 )
