/***********************************************************************************
File Name: XXWC_QP_WEBBER_SKU_CATG_DIR
PROGRAM TYPE: SQL DIRECTORY file
HISTORY
PURPOSE: Definition of Master Catalog Directory where the files will be transferred
====================================================================================
VERSION DATE          AUTHOR(S)       DESCRIPTION
------- -----------   --------------- ----------------------------------------------
1.0     06/23/2014   Ram Talluri    Initial creation of the file
************************************************************************************/
CREATE OR REPLACE DIRECTORY 
XXWC_QP_WEBBER_SKU_CATG_DIR AS 
'/xx_iface/ebizprd/outbound/Sales_and_Fulfillment/PRICING/WEBBER';


--GRANT EXECUTE, READ, WRITE ON DIRECTORY XXWC_QP_WEBBER_SKU_CATG_DIR TO APPS WITH GRANT OPTION;

GRANT EXECUTE, READ, WRITE ON DIRECTORY XXWC_QP_WEBBER_SKU_CATG_DIR TO XXWC_DEV_ADMIN WITH GRANT OPTION;