/*************************************************************************
   *   $Header XXCUS_GSC_PP_CHECKRECON_DBDIR.sql $
   *   Module Name: XXCUS_GSC_PP_CHECKRECON.sql
   *
   *   PURPOSE:   This script is used to create db directory for XXCUS_GSC_PP_CHECKRECON
   *
   *   REVISIONS:
   * ESMS        TMS              Date          Version  Comments
   * =========== ===============  ==========    =======  ========================
   * 192733      20160225-00147   01-MAR-2016   1.0      Created.
  ****************************************************************************/
DECLARE
l_db_name VARCHAR2(20) :=NULL;
l_path VARCHAR2(240) :=NULL;
l_sql VARCHAR2(240) :=NULL;
--
BEGIN

SELECT lower(name) 
  INTO l_db_name
  FROM v$database;
  --
  l_path :='/xx_iface/'||l_db_name||'/outbound/uc4/psoft/payroll/checkrecon';
  --
  l_sql :='CREATE OR REPLACE DIRECTORY XXCUS_GSC_PP_CHECKRECON_OB_DIR as'||' '||''''||l_path||'''';
  --
  dbms_output.put_line('DBA Directory Path: '||l_path);
  dbms_output.put_line(' ');
  dbms_output.put_line('SQL: '||l_sql);
  dbms_output.put_line(' ');  
  dbms_output.put_line('Begin setup of directory XXCUS_GSC_PP_CHECKRECON_OB_DIR');  
  EXECUTE IMMEDIATE  l_sql;
  dbms_output.put_line('End setup of directory XXCUS_GSC_PP_CHECKRECON_OB_DIR');    
END;
/