/***********************************************************************************
File Name: XXWC_WCS_SALESCATALOG_DIR
PROGRAM TYPE: SQL DIRECTORY file
HISTORY
PURPOSE: Definition of Sales Catalog Directory where the files will be transferred
====================================================================================
VERSION DATE          AUTHOR(S)        DESCRIPTION
------- -----------   ---------------  ----------------------------------------------
1.0     12/27/2012    Jyotsna Bidkar   Initial creation of the file
************************************************************************************/

CREATE OR REPLACE DIRECTORY XXWC_WCS_SALESCATALOG_DIR as '/xx_iface/ebizprd/outbound/wcs/salescatalog';