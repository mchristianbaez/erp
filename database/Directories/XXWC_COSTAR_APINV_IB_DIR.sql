/**************************************************************************
   $Header XXWC_COSTAR_APINV_DIR_IB $
   Module Name: XXWC_COSTAR_APINV_DIR_IB.sql

   PURPOSE:   This script is used to create the directory location for 
              exporting file using XXWC CoStar AP Invoices Import
              for importing AP Invoices from a pipe separated file.
   REVISIONS:
   Ver        Date        Author             Description
   ---------  ----------  ---------------   -------------------------
    1.0       13/06/2018  Ashwin Sridhar    Initial Build - Task ID: 20180709-00059
/*************************************************************************/
DECLARE
v_sql_stmt VARCHAR2(200);
v_db_name  VARCHAR2(50);

BEGIN

  SELECT lower(name)
  INTO v_db_name
  FROM v$database;

  v_sql_stmt:='CREATE OR REPLACE DIRECTORY XXWC_COSTAR_APINV_DIR_IB AS '||'''/xx_iface/'||v_db_name||'/inbound/uc4/costar/ap/CI_WC''';

  EXECUTE IMMEDIATE v_sql_stmt;
  
EXCEPTION
WHEN OTHERS THEN

  DBMS_OUTPUT.PUT_LINE('OUTER BLOCK : '||SQLERRM);

END;
/

GRANT ALL ON DIRECTORY XXWC_COSTAR_APINV_DIR_IB TO PUBLIC;
/
