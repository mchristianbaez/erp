/***********************************************************************************
File Name: XXWC_WCS_CATALOG_DIR
PROGRAM TYPE: SQL DIRECTORY file
HISTORY
PURPOSE: Definition of Master Catalog Directory where the files will be transferred
====================================================================================
VERSION DATE          AUTHOR(S)       DESCRIPTION
------- -----------   --------------- ----------------------------------------------
1.0     09/23/2012    Rajiv Rathod    Initial creation of the file
************************************************************************************/

CREATE OR REPLACE DIRECTORY XXWC_WCS_CATALOG_DIR as '/xx_iface/ebizprd/outbound/wcs/catalog';