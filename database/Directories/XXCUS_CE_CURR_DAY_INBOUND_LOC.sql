   /*
      ************************************************************************
    HD Supply
    All rights reserved.
   **************************************************************************
     PURPOSE:   GSC Current day cash positioning BAI file process 

     REVISIONS:
     Ver        Date                Author                           Description
     ---------  ----------         ---------------                  -------------------------------------------------------------------------------------------------------------
     1.0       09/21/2016  Balaguru Seshadri     TMS: 20160901-00241 / ESMS 322411 - GSC Current day cash positioning BAI file process - Location of inbound file                                                                                                  
   ************************************************************************* 
  */
DECLARE
   l_db_name       VARCHAR2 (20) := NULL;
   l_path_inb     VARCHAR2 (240) := NULL;
   l_path_oub   VARCHAR2 (240) := NULL;
   l_sql           VARCHAR2 (240) := NULL;
--
BEGIN
   --
   SELECT LOWER (name) INTO l_db_name FROM v$database;
   --
   l_path_inb := '/xx_iface/' || l_db_name || '/inbound/uc4/current_day_cash';
   --
   l_sql :=
         'CREATE OR REPLACE DIRECTORY XXCUS_CE_CURR_DAY_INBOUND_LOC as'
      || ' '
      || ''''
      || l_path_inb
      || '''';

   DBMS_OUTPUT.put_line ('DBA Directory Path: ' || l_path_inb);
   DBMS_OUTPUT.put_line (' ');
   DBMS_OUTPUT.put_line ('SQL: ' || l_sql);
   DBMS_OUTPUT.put_line (' ');
   DBMS_OUTPUT.put_line ('Begin setup of directory XXCUS_CE_CURR_DAY_INBOUND_LOC');
   DBMS_OUTPUT.put_line (' ');   
   EXECUTE IMMEDIATE l_sql;
   DBMS_OUTPUT.put_line ('End setup of directory XXCUS_CE_CURR_DAY_INBOUND_LOC');
EXCEPTION
 WHEN OTHERS THEN
  DBMS_OUTPUT.PUT_LINE('OUTER BLOCK : '||SQLERRM);
END;
/
GRANT ALL ON DIRECTORY XXCUS_CE_CURR_DAY_INBOUND_LOC TO PUBLIC;
/