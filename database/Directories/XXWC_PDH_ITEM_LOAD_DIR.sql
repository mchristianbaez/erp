/*************************************************************************
   *   $Header XXWC_PDH_ITEM_LOAD_DIR.sql $
   *   Module Name: XXWC_PDH_ITEM_LOAD_DIR.sql
   *
   *   PURPOSE:   This script is used to create db directory for ITEM Load
   *
   *   REVISIONS:
   *   Ver        Date         Author                Description
   *   ---------  ----------   ---------------       -------------------------
   *   1.0        26-MAY-2015   Christian Baez   Initial Version TMS# 20160223-00029 
  ****************************************************************************/
DECLARE
l_db_name VARCHAR2(20);
BEGIN
SELECT name 
  INTO l_db_name
  FROM v$database;

    IF l_db_name = 'EBSQA' THEN
    EXECUTE IMMEDIATE 
        'CREATE OR REPLACE DIRECTORY XXWC_PDH_ITEM_LOAD_DIR AS ''/xx_iface/ebsqa/inbound/pdh/b2b_cat''';
    ELSIF l_db_name = 'EBSDEV' THEN
    EXECUTE IMMEDIATE 
        'CREATE OR REPLACE DIRECTORY XXWC_PDH_ITEM_LOAD_DIR AS ''/xx_iface/ebsdev/inbound/pdh/b2b_cat''';
        ELSE
    EXECUTE IMMEDIATE 
        'CREATE OR REPLACE DIRECTORY XXWC_PDH_ITEM_LOAD_DIR AS ''/xx_iface/ebsprd/inbound/pdh/b2b_cat''';    
    END IF;    
END;
/
GRANT ALL ON DIRECTORY XXWC_PDH_ITEM_LOAD_DIR TO EA_APEX;
GRANT ALL ON DIRECTORY XXWC_PDH_ITEM_LOAD_DIR TO XXWC;
/