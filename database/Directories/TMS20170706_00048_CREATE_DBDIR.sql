/*******************************************************************************************************
  -- Script Name TMS20170706_00048_CREATE_DBDIR.sql
  -- ***************************************************************************************************
  --
  -- PURPOSE: script to create directory
  -- HISTORY
  -- ===================================================================================================
  -- ===================================================================================================
  -- VERSION DATE          AUTHOR(S)       DESCRIPTION
  -- ------- -----------   --------------- -------------------------------------------------------------
  -- 1.0     14-Jul-2017   Niraj K Ranjan   TMS#20170706-00048   Jobsite codes need to be added for Clark Construction Group
********************************************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000;
DECLARE
l_db_name VARCHAR2(20) :=NULL;
l_path VARCHAR2(240) :=NULL;
l_sql VARCHAR2(240) :=NULL;
l_dir_name VARCHAR2(30) := 'XXWC_CLARK_SHIPTO_SITE_LOAD';
--
BEGIN

SELECT lower(name) 
  INTO l_db_name
  FROM v$database;

  l_path :='/xx_iface/'||l_db_name||'/inbound/clark_shipto_site';

  l_sql :='CREATE OR REPLACE DIRECTORY '||l_dir_name||' as'||' '||''''||l_path||'''';

  dbms_output.put_line('DBA Directory Path: '||l_path);
  dbms_output.put_line(' ');
  dbms_output.put_line('SQL: '||l_sql);
  dbms_output.put_line(' ');  
  dbms_output.put_line('Begin setup of directory '||l_dir_name);  
  EXECUTE IMMEDIATE l_sql;
  dbms_output.put_line('End setup of directory '||l_dir_name);    
  
END;
/
GRANT ALL ON DIRECTORY XXWC_CLARK_SHIPTO_SITE_LOAD TO PUBLIC WITH GRANT OPTION;
/

