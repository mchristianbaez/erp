/*************************************************************************
   *   $Header XXWC_SILVERPOP_OUT_DBDIR_DB.sql $
   *   Module Name: XXWC_SILVERPOP_OUT_DBDIR_DB.sql
   *
   *   PURPOSE:   This script is used to create db directory for Silverpop
   *
   *   REVISIONS:
   *   Ver        Date         Author                Description
   *   ---------  ----------   ---------------       -------------------------
   *   1.0        01-Mar-2016   Neha Saini   Initial Version TMS# 20160211-00160 
  ****************************************************************************/
DECLARE
l_db_name VARCHAR2(20);

BEGIN

SELECT name 
  INTO l_db_name
  FROM v$database;

    IF l_db_name = 'EBSQA' THEN
    EXECUTE IMMEDIATE 
        'CREATE OR REPLACE DIRECTORY XXWC_SILVERPOP_OB_DIR as ''/xx_iface/ebsqa/outbound/SilverPop''';
    ELSIF l_db_name = 'EBSDEV' THEN
    EXECUTE IMMEDIATE 
        'CREATE OR REPLACE DIRECTORY XXWC_SILVERPOP_OB_DIR as ''/xx_iface/ebsdev/outbound/SilverPop''';
        ELSE
    EXECUTE IMMEDIATE 
        'CREATE OR REPLACE DIRECTORY XXWC_SILVERPOP_OB_DIR as ''/xx_iface/ebizprd/outbound/SilverPop''';    
    END IF;    

END;
/
