--//============================================================================
--//
--// Object Name         :: xxwc_pdh_ib_dir
--//
--// Object Type         :: Directory
--//
--// Object Description  :: This is PDH inbound directory name
--//
--// Version Control
--//============================================================================
--// Vers    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Mahesh Kudle     15/03/2014    Initial Build - TMS#20140207-00083
--//============================================================================
CREATE OR REPLACE directory xxwc_pdh_ib_dir
  AS '/xx_iface/ebizprd/inbound/pdh/image_management/';