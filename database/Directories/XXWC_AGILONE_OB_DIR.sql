--//============================================================================
--//
--// Object Name         :: xxwc_agilone_ob_dir
--//
--// Object Type         :: Directory
--//
--// Object Description  :: This is AgilOne outbound directory name
--//
--// Version Control
--//============================================================================
--// Vers    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Mahesh Kudle     26/05/2014    Initial Build - TMS#20140602-00295
--//============================================================================
CREATE OR REPLACE directory xxwc_agilone_ob_dir
  AS '/xx_iface/ebizfqa/outbound/agilone';