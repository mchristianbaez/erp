--//============================================================================
--//
--// Object Name         :: xxwc_pricing_ob_dir
--//
--// Object Type         :: Directory
--//
--// Object Description  :: This is pricing outbound directory name
--//
--// Version Control
--//============================================================================
--// Vers    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Krishna          26/07/2014    Initial Build - TMS#20130709-01006
--//============================================================================
CREATE OR REPLACE directory xxwc_pricing_ob_dir
  AS '/xx_iface/ebizfqa/outbound/pricing';