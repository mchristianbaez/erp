 /**************************************************************************
      $Header XXWC_CREDIT_CUST_UPD_FILE $
      Module Name: XXWC_CREDIT_CUST_UPD_FILE.sql

      PURPOSE:   This script  is used to create a DB directory
      REVISIONS:
      Ver        Date        Author             Description
      ---------  ----------  ---------------   -------------------------
       1.0       05/12/2016  Neha Saini         Initial Build - Task ID: 20131016-00419
    /*************************************************************************/
DECLARE
   l_db_name       VARCHAR2 (20) := NULL;
   l_path          VARCHAR2 (240) := NULL;
   l_sql           VARCHAR2 (240) := NULL;

BEGIN
   SELECT LOWER (name) INTO l_db_name FROM v$database;
 
   l_path := '/xx_iface/' || l_db_name || '/outbound/credit_cust_update';
 
   l_sql :=
         'CREATE OR REPLACE DIRECTORY XXWC_CREDIT_CUST_UPD_FILE as'
      || ' '
      || ''''
      || l_path
      || '''';

   DBMS_OUTPUT.put_line ('DBA Directory Path: ' || l_path);
   DBMS_OUTPUT.put_line (' ');
   DBMS_OUTPUT.put_line ('SQL: ' || l_sql);
   DBMS_OUTPUT.put_line (' ');
   DBMS_OUTPUT.put_line (
      'Begin setup of directory XXWC_CREDIT_CUST_UPD_FILE');

   EXECUTE IMMEDIATE l_sql;

   DBMS_OUTPUT.put_line (
      'End setup of directory XXWC_CREDIT_CUST_UPD_FILE');
END;
/
GRANT ALL
   ON DIRECTORY XXWC_CREDIT_CUST_UPD_FILE
   TO PUBLIC
   WITH GRANT OPTION;
