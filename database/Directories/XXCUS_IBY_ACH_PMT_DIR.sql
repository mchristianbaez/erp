--
-- TMS 20150812-00198
-- XXCUS.XXCUS_IBY_ACH_LOG  (Table) 
-- Purpose: Logical directory name to the uc4 sungard location under xx_iface.
-- 
CREATE OR REPLACE DIRECTORY XXCUS_IBY_ACH_PMT_DIR AS '/xx_iface/ebizprd/outbound/uc4/ap/sungard';