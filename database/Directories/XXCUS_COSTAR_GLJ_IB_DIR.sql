/**************************************************************************
   $Header XXCUS_COSTAR_GLJ_IB_DIR $
   Module Name: XXCUS_COSTAR_GLJ_IB_DIR.sql

   PURPOSE:   This script is used to create the directory location for 
              exporting file using XXCUS CoStar GL Journal Import
              for importing GL Journals from a pipe separated file.
   REVISIONS:
   Ver        Date        Author             	  Description
   ---------  ----------  ---------------   	  -------------------------
    1.0       08/07/2018  Vamshi Singirikonda  	Initial Build - Task ID: 20180709-00115						  
/*************************************************************************/
DECLARE
v_sql_stmt VARCHAR2(200);
v_db_name  VARCHAR2(50);

BEGIN

  SELECT lower(name)
  INTO v_db_name
  FROM v$database;

  v_sql_stmt:='CREATE OR REPLACE DIRECTORY XXCUS_COSTAR_GLJ_IB_DIR AS '||'''/xx_iface/'||v_db_name||'/inbound/uc4/costar/gl/HDS_CRP''';

  EXECUTE IMMEDIATE v_sql_stmt;
  
EXCEPTION
WHEN OTHERS THEN

  DBMS_OUTPUT.PUT_LINE('OUTER BLOCK : '||SQLERRM);

END;
/

GRANT ALL ON DIRECTORY XXCUS_COSTAR_GLJ_IB_DIR TO PUBLIC;
/
