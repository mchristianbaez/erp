/******************************************************************************
NAME:     XXWC_FND_PRINTER_TL_TO_EA_APEX_SELECT_ROLE.sql

PURPOSE:  Grant on APPS.FND_PRINTER_TL to EA_APEX


REVISIONS:
Ver        Date        Author           Description
---------  -----------  ---------------  ------------------------------------
1.0        20-Feb-2015  Gopi Damuluri    Initial Version.
				    TMS 20150220-00107 - Move DMS Functionality to APEX on EBS
******************************************************************************/
GRANT SELECT ON APPS.FND_PRINTER_TL TO EA_APEX;
