/******************************************************************************
NAME:     XXWC_DMS_SALES_RECEIPT_PRC_TO_EA_APEX_EXCECUTE_ROLE.sql

PURPOSE:  Grant on APPS.XXWC_DMS_SALES_RECEIPT_PRC to EA_APEX


REVISIONS:
Ver        Date        Author           Description
---------  -----------  ---------------  ------------------------------------
1.0        20-Feb-2015  Gopi Damuluri    Initial Version.
				    TMS 20150220-00107 - Move DMS Functionality to APEX on EBS
******************************************************************************/
GRANT EXECUTE ON APPS.XXWC_DMS_SALES_RECEIPT_PRC TO EA_APEX;