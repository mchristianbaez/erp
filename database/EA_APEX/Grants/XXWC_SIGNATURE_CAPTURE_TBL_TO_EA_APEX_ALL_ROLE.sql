/******************************************************************************
NAME:     XXWC_SIGNATURE_CAPTURE_TBL_TO_EA_APEX_ALL_ROLE.sql

PURPOSE:  Grant on XXWC.XXWC_SIGNATURE_CAPTURE_TBL to EA_APEX


REVISIONS:
Ver        Date        Author           Description
---------  -----------  ---------------  ------------------------------------
1.0        20-Feb-2015  Gopi Damuluri    Initial Version.
				    TMS 20150220-00107 - Move DMS Functionality to APEX on EBS
******************************************************************************/
GRANT ALL ON XXWC.XXWC_SIGNATURE_CAPTURE_TBL TO EA_APEX;