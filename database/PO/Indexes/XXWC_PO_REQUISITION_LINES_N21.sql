--
-- XXWC_PO_REQUISITION_LINES_N21  (Index) 
--
CREATE INDEX PO.XXWC_PO_REQUISITION_LINES_N21 ON PO.PO_REQUISITION_LINES_ALL
(REQUISITION_HEADER_ID, REQUISITION_LINE_ID, SOURCE_ORGANIZATION_ID, DESTINATION_ORGANIZATION_ID, ITEM_ID)
TABLESPACE APPS_TS_TX_DATA;


