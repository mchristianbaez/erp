-----------------------------------------------------------------------------------------------------------------------------
/*************************************************************************************************************************
  $Header XXWC_OE_ORDER_LN_AL_N16
  File Name: XXWC_OE_ORDER_LN_AL_N16.sql
  PURPOSE:   
  REVISIONS:
     Ver        Date         Author         Description
     ---------  ----------   --------    -----------------------------------------------------------------------------------
     1.0        06-Jan-2016  SIVA        TMS#20161220-00128 Invoice Pre-Register report - Program Running for longer time
****************************************************************************************************************************/
CREATE INDEX ONT.XXWC_OE_ORDER_LN_AL_N16 ON ONT.OE_ORDER_LINES_ALL (USER_ITEM_DESCRIPTION,FLOW_STATUS_CODE) TABLESPACE APPS_TS_TX_DATA
/
