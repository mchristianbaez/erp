   /*************************************************************************
   *   $Header XXWC_OE_ORDER_LN_AL_N11.sql $
   *   Purpose : Index for Performance issue
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        16-Mar-2015 Shankar Hariharan         Initial Version 20140919-00308
   * ***************************************************************************/

CREATE INDEX ONT.XXWC_OE_ORDER_LN_AL_N11
   ON ONT.OE_ORDER_LINES_ALL (INVENTORY_ITEM_ID,
                              SHIP_TO_ORG_ID,
                              CREATION_DATE,
                              LINE_TYPE_ID)
   TABLESPACE APPS_TS_TX_IDX
/   