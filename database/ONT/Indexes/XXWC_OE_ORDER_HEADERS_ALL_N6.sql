/*************************************************************************
$Header XXWC_OE_ORDER_HEADERS_ALL_N6.sql $
PURPOSE: Index on OE_ORDER_HEADERS_ALL
REVISIONS:
Ver        Date         Author                Description
---------  -----------  ------------------    ----------------
1.0 	   3-Sep-2015  Manjula Chellappan    TMS# 20150903-00030 - Add Index to improve performance 	
**************************************************************************/

  CREATE INDEX "ONT"."XXWC_OE_ORDER_HEADERS_ALL_N6" ON "ONT"."OE_ORDER_HEADERS_ALL" ("ORDERED_DATE") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 131072 NEXT 131072 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "APPS_TS_TX_DATA" ;
