/********************************************************************************
   $Header XXWC_OE_PAYMENTS_N1.SQL $
   Module Name: XXWC_OE_PAYMENTS_N1

   PURPOSE:   Index on table OE_PAYMENTS

   REVISIONS:
   Ver        Date        Author                  Description
   ---------  ----------  ---------------         -------------------------
   1.0        02/17/2016  Manjula Chellappan      TMS# 20160217-00016 - Create Index on OE_PAYMENTS
********************************************************************************/
CREATE INDEX ONT.XXWC_OE_PAYMENTS_N1
 ON ONT.OE_PAYMENTS (PAYMENT_TRX_ID,HEADER_ID,PAYMENT_NUMBER); 