   /*************************************************************************
   *   $Header XXWC_OE_ORDER_LN_AL_N12.sql $
   *   Purpose : Index for Performance issue
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        22-Apr-2016 Rakesh Patel            Initial Version 20160422-00045 
   * ***************************************************************************/

CREATE INDEX ONT.XXWC_OE_ORDER_LN_AL_N12
ON ONT.OE_ORDER_LINES_ALL
(TRUNC(CREATION_DATE), 
 LINE_ID,
 LINK_TO_LINE_ID)
LOGGING
TABLESPACE APPS_TS_TX_IDX 
NOPARALLEL
ONLINE
/ 