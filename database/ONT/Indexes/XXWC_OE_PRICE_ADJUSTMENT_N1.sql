-----------------------------------------------------------------------------------------------------------------------------
/*************************************************************************************************************************
  $Header XXWC_OE_PRICE_ADJUSTMENT_N1
  File Name: XXWC_OE_PRICE_ADJUSTMENT_N1.sql
  PURPOSE:   
  REVISIONS:
     Ver          Date         Author         Description
     ---------  ----------   --------    -----------------------------------------------------------------------------------
     1.0        03-Jan-2018    SIVA        TMS#20171219-00082 Manual Price Override Report- Report Running for longer time
****************************************************************************************************************************/
CREATE INDEX ONT.XXWC_OE_PRICE_ADJUSTMENT_N1 ON ONT.OE_PRICE_ADJUSTMENTS
  (
    HEADER_ID,
    LINE_ID,
    LIST_LINE_TYPE_CODE
  )
  TABLESPACE APPS_TS_TX_DATA
/
