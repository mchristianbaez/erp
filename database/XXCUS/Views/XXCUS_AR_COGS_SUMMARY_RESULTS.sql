
  CREATE OR REPLACE FORCE VIEW "XXCUS"."XXCUS_AR_COGS_SUMMARY_RESULTS" ("GL_LOCATION", "AR_REV_COGS_SUMMARY") AS 
  select tbl_b.gl_location gl_location,
       tbl_b.order_number||'|'||
       tbl_b.total_sales||'|'||
       tbl_b.total_cogs||'|'||
       tbl_b.cogs_rev_diff||'|'||
       tbl_b.cogs_percent||'|'   AR_REV_COGS_SUMMARY
from   xxcus.xxcus_gl_ar_rev_cogs_final_b tbl_b;
