
  CREATE OR REPLACE FORCE VIEW "XXCUS"."XXCUS_TAX_RETURN_RPT_SUMMARY_V" ("TAX_AUDIT_SUMMARY_DATA") AS 
  SELECT    'PERIOD'
          || '|'
          || 'INVOICE_NUMBER'
          || '|'
          || 'TOTAL_TAX'
          || '|'
             TAX_AUDIT_SUMMARY_DATA
     FROM DUAL
    WHERE 1 = 1
   UNION ALL
   SELECT    PERIOD
          || '|'
          || INVOICE_NUMBER
          || '|'
          || TOTAL_TAX
          || '|'
             TAX_AUDIT_SUMMARY_DATA
     FROM XXCUS_TAX_RETURN_REPORT_B
    WHERE 1 = 1;
