/*
History
======
Version Date           Author                              Notes
======  ========== ====================== =========================================
1.0     12/15/2017 Balaguru Seshadri      TMS 20171215-00180 - new indexes on xxcus pam income lob table 
*/
CREATE INDEX xxcus.xxcus_pam_income_lob_tbl_n1 ON xxcus.xxcus_pam_income_lob_tbl
(pam_year, sys_year)
/
CREATE INDEX xxcus.xxcus_pam_income_lob_tbl_n2 ON xxcus.xxcus_pam_income_lob_tbl
(cust_id, period_type,rebate_type,pam_year,plan_id,nvl(bu_id, 0),month_id,sys_year)
/
CREATE INDEX xxcus.xxcus_pam_income_lob_tbl_n3 ON xxcus.xxcus_pam_income_lob_tbl
(cust_id, period_type,rebate_type,pam_year,sys_year)
/
CREATE INDEX xxcus.xxcus_pam_purchase_lob_tbl_n1 ON xxcus.xxcus_pam_purchase_lob_tbl
(pam_year)
/
CREATE INDEX xxcus.xxcus_pam_purchase_lob_tbl_n2 ON xxcus.xxcus_pam_purchase_lob_tbl
(cust_id, period_type,pam_year,nvl(bu_id, 0),month_id)
/