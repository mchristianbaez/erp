/*
  @GSC Incident 615950 - Rebates fixes for parallel threads based on stress test results from EBSPRD
  WC TMS: 20160316-00196 
  Date: 04/15/2016
*/
DROP INDEX XXCUS.XXCUS_OZF_XLA_ACCRUALS_B_N18;
CREATE INDEX XXCUS.XXCUS_OZF_XLA_ACCRUALS_B_N18 ON XXCUS.XXCUS_OZF_XLA_ACCRUALS_B
(xaeh_period_name ,resale_line_attribute11, resale_line_attribute12, coop_yes_no, override_fin_loc)
  TABLESPACE APPS_TS_TX_IDX
  PCTFREE    10
  INITRANS   11
  MAXTRANS   255
  STORAGE    (
              INITIAL          2M
              NEXT             2M
              MINEXTENTS       1
              MAXEXTENTS       UNLIMITED
              PCTINCREASE      0
              BUFFER_POOL      DEFAULT
              FLASH_CACHE      DEFAULT
              CELL_FLASH_CACHE DEFAULT
             )
NOLOGGING
LOCAL (  
  PARTITION JAN
    NOLOGGING
    TABLESPACE APPS_TS_TX_IDX
    PCTFREE    10
    INITRANS   11
    MAXTRANS   255
    STORAGE    (
                INITIAL          2M
                NEXT             2M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION FEB
    NOLOGGING
    TABLESPACE APPS_TS_TX_IDX
    PCTFREE    10
    INITRANS   11
    MAXTRANS   255
    STORAGE    (
                INITIAL          2M
                NEXT             2M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION MAR
    NOLOGGING
    TABLESPACE APPS_TS_TX_IDX
    PCTFREE    10
    INITRANS   11
    MAXTRANS   255
    STORAGE    (
                INITIAL          2M
                NEXT             2M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION APR
    NOLOGGING
    TABLESPACE APPS_TS_TX_IDX
    PCTFREE    10
    INITRANS   11
    MAXTRANS   255
    STORAGE    (
                INITIAL          2M
                NEXT             2M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION MAY
    NOLOGGING
    TABLESPACE APPS_TS_TX_IDX
    PCTFREE    10
    INITRANS   11
    MAXTRANS   255
    STORAGE    (
                INITIAL          2M
                NEXT             2M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION JUN
    NOLOGGING
    TABLESPACE APPS_TS_TX_IDX
    PCTFREE    10
    INITRANS   11
    MAXTRANS   255
    STORAGE    (
                INITIAL          2M
                NEXT             2M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION JUL
    NOLOGGING
    TABLESPACE APPS_TS_TX_IDX
    PCTFREE    10
    INITRANS   11
    MAXTRANS   255
    STORAGE    (
                INITIAL          2M
                NEXT             2M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION AUG
    NOLOGGING
    TABLESPACE APPS_TS_TX_IDX
    PCTFREE    10
    INITRANS   11
    MAXTRANS   255
    STORAGE    (
                INITIAL          2M
                NEXT             2M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION SEP
    NOLOGGING
    TABLESPACE APPS_TS_TX_IDX
    PCTFREE    10
    INITRANS   11
    MAXTRANS   255
    STORAGE    (
                INITIAL          2M
                NEXT             2M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION OCT
    NOLOGGING
    TABLESPACE APPS_TS_TX_IDX
    PCTFREE    10
    INITRANS   11
    MAXTRANS   255
    STORAGE    (
                INITIAL          2M
                NEXT             2M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION NOV
    NOLOGGING
    TABLESPACE APPS_TS_TX_IDX
    PCTFREE    10
    INITRANS   11
    MAXTRANS   255
    STORAGE    (
                INITIAL          2M
                NEXT             2M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION DEC
    NOLOGGING
    TABLESPACE APPS_TS_TX_IDX
    PCTFREE    10
    INITRANS   11
    MAXTRANS   255
    STORAGE    (
                INITIAL          2M
                NEXT             2M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION NA
    NOLOGGING
    TABLESPACE APPS_TS_TX_IDX
    PCTFREE    10
    INITRANS   11
    MAXTRANS   255
    STORAGE    (
                INITIAL          2M
                NEXT             2M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
                BUFFER_POOL      DEFAULT
               )
)
PARALLEL 8;
--
ALTER INDEX XXCUS.XXCUS_OZF_XLA_ACCRUALS_B_N18 NOPARALLEL;