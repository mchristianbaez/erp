/*
History
======
Version Date       Author                 Notes
======  ========== ====================== =========================================
1.0     08/22/2016 Balaguru Seshadri      TMS 20160902-00099  ESMS 449491  - Rebate Data Reconcilation
*/
CREATE INDEX XXCUS.XXCUS_REBATE_RECPT_HIST_TBL_N5 ON XXCUS.XXCUS_REBATE_RECPT_HISTORY_TBL
(BU_NM, VNDR_RECPT_UNQ_NBR)
NOLOGGING
TABLESPACE XXCUS_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
PARALLEL 8
/
ALTER INDEX XXCUS.XXCUS_REBATE_RECPT_HIST_TBL_N5 NOPARALLEL
/