--
-- XXCUS_ERROR_PK  (Index) 
--
CREATE UNIQUE INDEX XXCUS.XXCUS_ERROR_PK ON XXCUS.XXCUS_ERRORS_TBL
(CALLED_FROM, CREATION_DATE, CALLING, ERROR_ID)
TABLESPACE XXCUS_IDX;


