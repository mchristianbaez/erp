--
-- XXCUSAP_PNC_SUBMIT_TBL_N1  (Index) 
--
CREATE INDEX XXCUS.XXCUSAP_PNC_SUBMIT_TBL_N1 ON XXCUS.XXCUSAP_PNC_SUBMIT_TBL
(PROCESSED, CARD_PROGRAM_NAME)
TABLESPACE XXCUS_DATA;


