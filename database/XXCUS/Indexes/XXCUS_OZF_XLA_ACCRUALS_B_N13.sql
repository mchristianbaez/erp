--
-- XXCUS_OZF_XLA_ACCRUALS_B_N13  (Index) 
--
CREATE BITMAP INDEX XXCUS.XXCUS_OZF_XLA_ACCRUALS_B_N13 ON XXCUS.XXCUS_OZF_XLA_ACCRUALS_B
(RESALE_LINE_ATTRIBUTE6, COOP_YES_NO, OVERRIDE_FIN_LOC)
PARALLEL ( DEGREE 8 INSTANCES 1 );


