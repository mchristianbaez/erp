/**************************************************************************
   $Header XXCUS_CONCUR_EMP_DIR_OB $
   Module Name: XXCUS_CONCUR_EMP_DIR_OB.sql

   PURPOSE:   This script is used to create the directory location for 
              exporting file using HDS CONCUR: Generate Concur Employee Export File
              for generating Employee pipe separated file.
   REVISIONS:
   Ver        Date        Author             Description
   ---------  ----------  ---------------   -------------------------
    1.0       08/05/2018  Ashwin Sridhar    Initial Build - Task ID: 20131016-00419
/*************************************************************************/
DECLARE
v_sql_stmt VARCHAR2(200);
v_db_name  VARCHAR2(50);

BEGIN

  SELECT lower(name)
  INTO v_db_name
  FROM v$database;

  v_sql_stmt:='CREATE OR REPLACE DIRECTORY XXCUS_EMP_FILE_EXPORT_DIR AS '||'''/xx_iface/'||v_db_name||'/outbound/concur/emp''';

  EXECUTE IMMEDIATE v_sql_stmt;
  
EXCEPTION
WHEN OTHERS THEN

  DBMS_OUTPUT.PUT_LINE('OUTER BLOCK : '||SQLERRM);

END;
/

GRANT ALL ON DIRECTORY XXCUS_EMP_FILE_EXPORT_DIR TO PUBLIC;
/
