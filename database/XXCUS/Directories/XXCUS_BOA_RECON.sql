/*
   Ticket#              Date         Author             Notes
   -------------------------------------------------------------------------------------------------------------------------------------------------------
   TMS 20180227-00179   06/15/2018   Balaguru Seshadri  BANK OF AMERICA CARD AND CONCUR GSC GO LIVE
*/
DECLARE
  --
   l_db_name    VARCHAR2 (20) := NULL;
   l_path_inb   VARCHAR2 (240) := NULL;
   l_path_oub   VARCHAR2 (240) := NULL;
   l_sql        VARCHAR2 (240) := NULL;
  --
BEGIN
   --
   SELECT LOWER (name) INTO l_db_name FROM v$database;
   --
   l_path_inb := '/xx_iface/' || l_db_name || '/xtr/inbound';
   --
   l_sql :=
         'CREATE OR REPLACE DIRECTORY XXCUS_BOA_RECON as'
      || ' '
      || ''''
      || l_path_inb
      || '''';

   DBMS_OUTPUT.put_line ('DBA Directory Path: ' || l_path_inb);
   DBMS_OUTPUT.put_line (' ');
   DBMS_OUTPUT.put_line ('SQL: ' || l_sql);
   DBMS_OUTPUT.put_line (' ');
   DBMS_OUTPUT.put_line ('Begin setup of directory XXCUS_BOA_RECON');
   DBMS_OUTPUT.put_line (' ');   
   EXECUTE IMMEDIATE l_sql;
   DBMS_OUTPUT.put_line ('End setup of directory XXCUS_BOA_RECON');
EXCEPTION
 WHEN OTHERS THEN
  DBMS_OUTPUT.PUT_LINE('OUTER BLOCK : '||SQLERRM);
END;
/
GRANT ALL ON DIRECTORY XXCUS_BOA_RECON TO PUBLIC;
/