/**************************************************************************
   Module Name: ORACLE TRADE MANAGEMENT

   PURPOSE:   RECURRING ACCRUAL PROCESS
   REVISIONS:
   Ver        Date        Author             	Description
   ---------  ----------  ---------------   	-------------------------
    1.0       10/23/2018  Bala Seshadri   	Initial Build - Task ID: 20180116-00033
/*************************************************************************/
DROP SEQUENCE XXCUS.XXCUS_OZF_RECURR_ACCR_RUNID_S;
CREATE SEQUENCE XXCUS.XXCUS_OZF_RECURR_ACCR_RUNID_S
  START WITH 2018000001
  MAXVALUE 9999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  NOORDER;
