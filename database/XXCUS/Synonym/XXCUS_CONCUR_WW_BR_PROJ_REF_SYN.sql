/*
   Ticket#                               Date         Author             Notes
   -------------------------------------------------------------------------------------------------------------------------------------------------------
   TMS 20170412-00112 / ESMS 554216      02/17/2017   Balaguru Seshadri  Concur process related tables
*/
--
CREATE OR REPLACE SYNONYM EA_APEX.XXCUS_CONCUR_WW_BR_PROJ_REF FOR XXCUS.XXCUS_CONCUR_WW_BR_PROJ_REF;
--
CREATE OR REPLACE SYNONYM APPS.XXCUS_CONCUR_WW_BR_PROJ_REF FOR XXCUS.XXCUS_CONCUR_WW_BR_PROJ_REF;
--