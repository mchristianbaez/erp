/*
   Ticket#                               Date         Author             Notes
   -------------------------------------------------------------------------------------------------------------------------------------------------------
   TMS 20170412-00112 / ESMS 554216      02/17/2017   Balaguru Seshadri  Concur process related tables
*/
CREATE OR REPLACE TRIGGER XXCUS.XXCUS_CONCUR_WW_BR_PROJ_TRG
BEFORE INSERT
ON XXCUS.XXCUS_CONCUR_WW_BR_PROJ_REF
REFERENCING NEW AS New OLD AS Old
FOR EACH ROW
DECLARE
tmpVar NUMBER;
/******************************************************************************
   NAME:       
   PURPOSE:    

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        3/16/2017      bs006141       1. Created this trigger.
******************************************************************************/
BEGIN
   tmpVar := 0;

   SELECT XXCUS.XXCUS_CONCUR_WW_BR_PROJ_S1.NEXTVAL INTO tmpVar FROM dual;
   :NEW.ID := tmpVar;

   EXCEPTION
     WHEN OTHERS THEN
       -- Consider logging the error and then re-raise
       RAISE;
END ;
/
