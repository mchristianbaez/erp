/**************************************************************************
 $Header XXCUS_STEPTEC_AUDIT_TRG.trg $    
 Module Name: XXCUS_STEPTEC_AUDIT_TRG

 PURPOSE:   This Trigger will capture the Taxware audit information

 REVISIONS:
 Ver        Date            Author            Description
 ---------  ----------      ---------------   ------------------------
 1.0        16-Aug-2013                       Initial Version - 
                                              ESMS#218791
 1.1        11-May-2017    Saini Neha         TMS#20170421-00060 -- added
                                              extra fields for audit
******************************************************************************/	  
DROP TRIGGER TAXWARE.XXCUS_STEPTEC_AUDIT_TRG;

CREATE OR REPLACE TRIGGER TAXWARE.XXCUS_STEPTEC_AUDIT_TRG
          BEFORE UPDATE OF cproductflag, cspecialrateflag, cactivecompflag 
		  ON TAXWARE.STEPTEC_TBL
          FOR EACH ROW
DECLARE          
v_host          VARCHAR2(2000);
v_ip_address    VARCHAR2(2000);
v_terminal      VARCHAR2(2000);
v_sessionid     VARCHAR2(2000);
v_session_user  VARCHAR2(2000);
v_sid           VARCHAR2(2000);
v_global_uid    VARCHAR2(2000);
v_os_user       VARCHAR2(2000);
d_date          DATE;
v_user_id       NUMBER; -- Ver#1.1
/*
ESMS Ticket  Date
==========  =====
218791      16-Aug-2013

*/ 
 BEGIN
    SELECT SYS_CONTEXT ('USERENV', 'HOST')
    INTO   v_host
    FROM   dual;
    SELECT SYS_CONTEXT ('USERENV', 'IP_ADDRESS')
    INTO   v_ip_address
    FROM   dual; 
    SELECT SYS_CONTEXT ('USERENV', 'TERMINAL')
    INTO   v_terminal
    FROM   dual;
    SELECT SYS_CONTEXT ('USERENV', 'SESSIONID')
    INTO   v_sessionid
    FROM   dual;
    SELECT SYS_CONTEXT ('USERENV', 'SESSION_USER')
    INTO   v_session_user
    FROM   dual;
    SELECT SYS_CONTEXT ('USERENV', 'SID')
    INTO   v_sid
    FROM   dual;
    SELECT SYS_CONTEXT ('USERENV', 'GLOBAL_UID')
    INTO   v_global_uid
    FROM   dual;
    SELECT SYS_CONTEXT('USERENV', 'OS_USER')
    INTO   v_os_user
    FROM   dual;    
    SELECT SYSDATE
    INTO   d_date
    FROM   dual;
    
    IF v_session_user ='APPS' THEN
      v_session_user :=apps.fnd_global.user_name;
   -- Else   -- Ver#1.1
   --  Null; -- Ver#1.1
    END IF;
	
	--<START> Ver#1.1
	 BEGIN
      SELECT c.user_name
        INTO v_session_user
        FROM apps.hz_cust_acct_sites_all a,
             apps.hz_party_sites b,
             apps.fnd_user c
       WHERE a.party_site_id = b.party_site_id
         AND b.party_site_number = :old.key_1
         AND c.user_id = a.LAST_UPDATED_BY;
     EXCEPTION
      WHEN OTHERS THEN      
         v_session_user:=apps.fnd_global.user_name;
     END;
    --<START> Ver#1.1
	
    INSERT INTO xxcus.xxcus_steptec_tbl_audit_b 
     (
       updated_rowid
      ,os_user
      ,host
      ,ip_address
      ,terminal
      ,sessionid
      ,session_user
      ,sid
      ,global_uid
      ,last_update_date
      ,old_cproduct_flag
      ,new_cproduct_flag
      ,old_spl_rate_flag
      ,new_spl_rate_flag
      ,old_key_1
      ,new_key_1      
      ,old_key_2
      ,new_key_2      
      ,old_key_3
      ,new_key_3      
      ,old_cjurislevel
      ,new_cjurislevel   
      ,old_sstatecode
      ,new_sstatecode 
      ,old_skeyoccurnum
      ,new_skeyoccurnum
      ,old_cactivecompflag
      ,new_cactivecompflag   
      ,old_CDEFLTTOSTCERT
      ,new_CDEFLTTOSTCERT
      ,old_SCUSTOMERNAME
      ,new_SCUSTOMERNAME   
     )
    VALUES
     (
       :new.rowid
      ,v_os_user
      ,v_host
      ,v_ip_address
      ,v_terminal
      ,v_sessionid
      ,v_session_user
      ,v_sid
      ,v_global_uid
      ,d_date
      ,:old.cproductflag
      ,:new.cproductflag
      ,:old.cspecialrateflag
      ,:new.cspecialrateflag
      ,:old.key_1
      ,:new.key_1      
      ,:old.key_2
      ,:new.key_2      
      ,:old.key_3
      ,:new.key_3      
      ,:old.cjurislevel
      ,:new.cjurislevel   
      ,:old.sstatecode
      ,:new.sstatecode 
      ,:old.skeyoccurnum
      ,:new.skeyoccurnum
      ,:old.cactivecompflag
      ,:new.cactivecompflag
      ,:old.CDEFLTTOSTCERT
      ,:new.CDEFLTTOSTCERT
      ,:old.SCUSTOMERNAME
      ,:new.SCUSTOMERNAME         
     );                      
 EXCEPTION
  WHEN OTHERS THEN
   v_host :=NULL;
END;
/
GRANT ALL ON APPLSYS.FND_USER TO TAXWARE WITH GRANT OPTION;
/
GRANT ALL ON AR.HZ_CUST_ACCT_SITES_ALL TO TAXWARE WITH GRANT OPTION;
/
GRANT ALL ON AR.HZ_PARTY_SITES TO TAXWARE WITH GRANT OPTION;
/
