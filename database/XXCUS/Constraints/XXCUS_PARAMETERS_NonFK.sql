ALTER TABLE XXCUS.XXCUS_PARAMETERS ADD (
  CONSTRAINT XXCUS_PARAMETERS_PK
  PRIMARY KEY
  (PROCEDURE_NAME, PARM_NAME)
  USING INDEX XXCUS.XXCUS_PARAMETERS_PK
  ENABLE VALIDATE);

