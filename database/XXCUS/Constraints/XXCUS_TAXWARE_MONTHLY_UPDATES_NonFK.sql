ALTER TABLE XXCUS.XXCUS_TAXWARE_MONTHLY_UPDATES ADD (
  UNIQUE (REQUEST_ID)
  USING INDEX
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
                BUFFER_POOL      DEFAULT
               )
  ENABLE VALIDATE);

