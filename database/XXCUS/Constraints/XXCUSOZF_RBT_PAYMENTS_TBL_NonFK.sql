ALTER TABLE XXCUS.XXCUSOZF_RBT_PAYMENTS_TBL ADD (
  PRIMARY KEY
  (REBATE_PAYMENT_ID)
  USING INDEX
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
                BUFFER_POOL      DEFAULT
               )
  ENABLE VALIDATE);

