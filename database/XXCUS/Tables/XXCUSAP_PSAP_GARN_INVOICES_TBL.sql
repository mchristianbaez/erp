CREATE TABLE XXCUS.XXCUSAP_PSAP_GARN_INVOICES_TBL
(
  SITE                        VARCHAR2(30 BYTE),
  INVOICE_NUM                 VARCHAR2(60 BYTE),
  INVOICE_DATE                DATE,
  INVOICE_AMOUNT              NUMBER,
  PAYMENT_METHOD_LOOKUP_CODE  VARCHAR2(30 BYTE),
  ATTRIBUTE2_DED_CODE         VARCHAR2(15 BYTE),
  ATTRIBUTE3_EMP_NUM          VARCHAR2(30 BYTE),
  ATTRIBUTE1_CASE_ID          VARCHAR2(60 BYTE),
  CHECK_COMMENTS              VARCHAR2(80 BYTE),
  CREATION_DATE               DATE,
  LAST_UPDATE_DATE            DATE,
  PROCESSED_FLAG              VARCHAR2(15 BYTE),
  IMPORT_GROUP_ID             VARCHAR2(60 BYTE),
  INTERFACE_ID                NUMBER,
  INVOICE_ID                  NUMBER,
  LUMP                        VARCHAR2(1 BYTE),
  IOCOMMENTS                  VARCHAR2(240 BYTE),
  COMMENTS_MSGS               VARCHAR2(240 BYTE),
  PAY_GRP_LOOKUP              VARCHAR2(30 BYTE),
  GARNISH_ID                  NUMBER
)
TABLESPACE XXCUS_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


