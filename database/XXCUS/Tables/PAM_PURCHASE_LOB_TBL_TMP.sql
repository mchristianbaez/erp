/*
-- 
-- --------------------------------------------------------------------------------------------------------------------------------
TICKET#                                     DATE             AUTHOR                            NOTES  
-- --------------------------------------------------------------------------------------------------------------------------------
TMS 20171013-00238 / ESMS 538724            07/05/2017       BALAGURU SESHADRI                 NEW EBS PAM CUSTOM TABLES
--
*/
--
DROP TABLE XXCUS.PAM_PURCHASE_LOB_TBL_TMP;
--
CREATE TABLE XXCUS.PAM_PURCHASE_LOB_TBL_TMP
(
  ID                 NUMBER,
  MVID               VARCHAR2(30 BYTE),
  CUST_ID            NUMBER,
  MV_NAME            VARCHAR2(255 BYTE),
  LOB_ID             NUMBER,
  BU_ID              NUMBER,
  PERIOD_TYPE        VARCHAR2(20 BYTE),
  YEAR               NUMBER,
  MONTH_ID           NUMBER,
  MONTH_NAME         VARCHAR2(30 BYTE),
  PUR_AMOUNT         NUMBER,
  SOURCE             VARCHAR2(20 BYTE),
  EBS_CREATED_BY     NUMBER,
  EBS_CREATION_DATE  DATE,
  REQUEST_ID         NUMBER
)
;
--
COMMENT ON TABLE XXCUS.PAM_PURCHASE_LOB_TBL_TMP IS 'TMS 20171013-00238'
