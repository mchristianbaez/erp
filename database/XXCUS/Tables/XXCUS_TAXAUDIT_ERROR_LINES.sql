
  CREATE TABLE "XXCUS"."XXCUS_TAXAUDIT_ERROR_LINES" 
   (	"INVOICE_NUMBER" VARCHAR2(30), 
	"INVOICE_SOURCE" VARCHAR2(50), 
	"CUSTOMER_TRX_ID" NUMBER, 
	"CUSTOMER_TRX_LINE_ID" NUMBER, 
	"HEADERNO" NUMBER, 
	"AR_TAXWARE_DETAILNO" NUMBER, 
	"EXTENDED_AMOUNT" NUMBER, 
	"INVOICE_LINE" NUMBER, 
	"ORDER_LINE" NUMBER, 
	"AR_CREATION_DATE" DATE, 
	"AR_INVOICE_DATE" DATE, 
	"SALES_ORDER" VARCHAR2(30), 
	"SO_HEADER_ID" NUMBER, 
	"SO_LINE_ID" NUMBER, 
	"ITEM_ID" NUMBER, 
	"ITEM_NUMBER" VARCHAR2(80), 
	"DESCRIPTION" VARCHAR2(240), 
	"INVOICE_QTY" NUMBER, 
	"INVOICE_UNIT_PRICE" NUMBER, 
	"WAREHOUSE_ID" NUMBER, 
	"BRANCH" VARCHAR2(30), 
	"SHIP_TO_CUSTOMER_ID" NUMBER, 
	"SHIP_TO_SITE_USE_ID" NUMBER, 
	"BILL_TO_CUSTOMER_ID" NUMBER, 
	"BILL_TO_SITE_USE_ID" NUMBER, 
	"BATCH_SOURCE_ID" NUMBER, 
	"PERIOD_NAME" VARCHAR2(10), 
	"EBTAX_LINE_TOTAL" NUMBER, 
	"TAXWARE_LINE_TOTAL" NUMBER, 
	"PROCESS_FLAG" VARCHAR2(1), 
	"POINT_OF_TAXATION" VARCHAR2(20), 
	"SHIP_METHOD_CODE" VARCHAR2(30), 
	"SHIP_VIA" VARCHAR2(80), 
	"BRANCH_ADDRESS1" VARCHAR2(150), 
	"BRANCH_CITY" VARCHAR2(60), 
	"BRANCH_STATE" VARCHAR2(60), 
	"BRANCH_POSTAL_CODE" VARCHAR2(60), 
	"BRANCH_COUNTY" VARCHAR2(60), 
	"BRANCH_GEOCODE" VARCHAR2(20)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 0 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "XXCUS_DATA" ;
