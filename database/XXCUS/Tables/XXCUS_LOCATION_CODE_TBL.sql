CREATE TABLE XXCUS.XXCUS_LOCATION_CODE_TBL
(
  FRU                VARCHAR2(4 BYTE)           NOT NULL,
  EFFDT              DATE                       NOT NULL,
  FRU_DESCR          VARCHAR2(75 BYTE),
  INACTIVE           VARCHAR2(1 BYTE),
  LOB_BRANCH         VARCHAR2(6 BYTE),
  LOB_DEPT           VARCHAR2(6 BYTE),
  LOB_COMPANY        VARCHAR2(3 BYTE)           NOT NULL,
  SYSTEM_CD          VARCHAR2(30 BYTE),
  SYSTEM_CODE        VARCHAR2(100 BYTE),
  ENTRP_ENTITY       VARCHAR2(10 BYTE),
  ENTRP_LOC          VARCHAR2(5 BYTE),
  ENTRP_CC           VARCHAR2(4 BYTE),
  AP_DOCUM_FLAG      VARCHAR2(1 BYTE),
  HC_FLAG            VARCHAR2(1 BYTE),
  BUSINESS_UNIT      VARCHAR2(5 BYTE),
  COMPANY            VARCHAR2(3 BYTE),
  IEXP_FRU_OVERRIDE  VARCHAR2(4 BYTE)           NOT NULL,
  FRU_OVERRIDE       VARCHAR2(11 BYTE),
  CREATION_DT        DATE,
  CREATEOPRID        VARCHAR2(30 BYTE),
  UPDATE_DT          DATE,
  UPDATED_USERID     VARCHAR2(30 BYTE)
)
TABLESPACE XXCUS_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

COMMENT ON TABLE XXCUS.XXCUS_LOCATION_CODE_TBL IS 'Table to hold translation values for the location code pieces (FRU, LOC, Cost Center) from ET';

COMMENT ON COLUMN XXCUS.XXCUS_LOCATION_CODE_TBL.FRU IS 'Holds the FRU (branch) designation - old l_corp_id field in xxhsi_location_code';

COMMENT ON COLUMN XXCUS.XXCUS_LOCATION_CODE_TBL.EFFDT IS 'Effective date of the data - only the most recent view of data is in this table';

COMMENT ON COLUMN XXCUS.XXCUS_LOCATION_CODE_TBL.FRU_DESCR IS 'FRU description';

COMMENT ON COLUMN XXCUS.XXCUS_LOCATION_CODE_TBL.INACTIVE IS 'Inactive flag';

COMMENT ON COLUMN XXCUS.XXCUS_LOCATION_CODE_TBL.LOB_BRANCH IS 'LOB legacy branch (old leg_seg_one)';

COMMENT ON COLUMN XXCUS.XXCUS_LOCATION_CODE_TBL.LOB_DEPT IS 'LOB legacy department (old leg_seg_two)';

COMMENT ON COLUMN XXCUS.XXCUS_LOCATION_CODE_TBL.SYSTEM_CD IS 'POS system code (old l_source)';

COMMENT ON COLUMN XXCUS.XXCUS_LOCATION_CODE_TBL.ENTRP_ENTITY IS 'Enterprise entity segment (old ora_seg_one) - prefixed with P for Khalix';

COMMENT ON COLUMN XXCUS.XXCUS_LOCATION_CODE_TBL.ENTRP_LOC IS 'Enterprise location segment (old ora_seg_two)';

COMMENT ON COLUMN XXCUS.XXCUS_LOCATION_CODE_TBL.ENTRP_CC IS 'Enterprise cost center segment (old ora_seg_three)';

COMMENT ON COLUMN XXCUS.XXCUS_LOCATION_CODE_TBL.AP_DOCUM_FLAG IS 'Indicates this location interfaces AP information to or from Documentum (old ap_interface_flag)';

COMMENT ON COLUMN XXCUS.XXCUS_LOCATION_CODE_TBL.HC_FLAG IS 'Include in headcount flag - has employees';

COMMENT ON COLUMN XXCUS.XXCUS_LOCATION_CODE_TBL.BUSINESS_UNIT IS 'Business unit';

COMMENT ON COLUMN XXCUS.XXCUS_LOCATION_CODE_TBL.COMPANY IS 'Company';

COMMENT ON COLUMN XXCUS.XXCUS_LOCATION_CODE_TBL.FRU_OVERRIDE IS 'GL FRU override';

COMMENT ON COLUMN XXCUS.XXCUS_LOCATION_CODE_TBL.CREATION_DT IS 'Creation date';

COMMENT ON COLUMN XXCUS.XXCUS_LOCATION_CODE_TBL.CREATEOPRID IS 'Created by';

COMMENT ON COLUMN XXCUS.XXCUS_LOCATION_CODE_TBL.UPDATE_DT IS 'Last update date';

COMMENT ON COLUMN XXCUS.XXCUS_LOCATION_CODE_TBL.UPDATED_USERID IS 'Last updated by';



