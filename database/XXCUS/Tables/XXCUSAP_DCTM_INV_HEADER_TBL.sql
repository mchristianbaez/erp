CREATE TABLE XXCUS.XXCUSAP_DCTM_INV_HEADER_TBL
(
  DCTM_OBJECT_ID            VARCHAR2(60 BYTE)   NOT NULL,
  CREATION_DATE             DATE                DEFAULT SYSDATE,
  CREATED_BY                VARCHAR2(30 BYTE),
  INVOICE_ID                NUMBER(15),
  INVOICE_NUM               VARCHAR2(50 BYTE)   NOT NULL,
  INVOICE_TYPE_LOOKUP_CODE  VARCHAR2(25 BYTE),
  INVOICE_DATE              DATE,
  PO_NUMBER                 VARCHAR2(20 BYTE),
  VENDOR_NUM                VARCHAR2(30 BYTE),
  VENDOR_NAME               VARCHAR2(240 BYTE),
  VENDOR_SITE_ID            NUMBER,
  INVOICE_AMOUNT            NUMBER              NOT NULL,
  INVOICE_CURRENCY_CODE     VARCHAR2(15 BYTE),
  DESCRIPTION               VARCHAR2(240 BYTE),
  IMAGE_LINK                VARCHAR2(500 BYTE),
  ORG_ID                    NUMBER,
  SOURCE                    VARCHAR2(80 BYTE),
  GROUP_ID                  VARCHAR2(80 BYTE),
  ATTRIBUTE1                VARCHAR2(150 BYTE),
  ATTRIBUTE2                VARCHAR2(150 BYTE),
  ATTRIBUTE3                VARCHAR2(150 BYTE),
  ATTRIBUTE4                VARCHAR2(150 BYTE),
  ATTRIBUTE5                VARCHAR2(150 BYTE),
  ATTRIBUTE6                VARCHAR2(150 BYTE),
  ATTRIBUTE7                VARCHAR2(150 BYTE),
  ATTRIBUTE8                VARCHAR2(150 BYTE),
  ATTRIBUTE9                VARCHAR2(150 BYTE),
  ATTRIBUTE10               VARCHAR2(150 BYTE),
  ATTRIBUTE11               VARCHAR2(150 BYTE),
  ATTRIBUTE12               VARCHAR2(150 BYTE),
  ATTRIBUTE13               VARCHAR2(150 BYTE),
  ATTRIBUTE14               VARCHAR2(150 BYTE),
  ATTRIBUTE15               VARCHAR2(150 BYTE),
  INTERFACED_FLAG           VARCHAR2(10 BYTE),
  REQUEST_ID                NUMBER,
  LAST_UPDATE_DATE          DATE,
  LAST_UPDATED_BY           NUMBER(15),
  BATCH                     NUMBER
)
TABLESPACE XXCUS_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

COMMENT ON COLUMN XXCUS.XXCUSAP_DCTM_INV_HEADER_TBL.ATTRIBUTE11 IS 'Invoice Type';



