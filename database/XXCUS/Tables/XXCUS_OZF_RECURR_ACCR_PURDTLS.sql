/**************************************************************************
   Module Name: ORACLE TRADE MANAGEMENT

   PURPOSE:   RECURRING ACCRUAL PROCESS
   REVISIONS:
   Ver        Date        Author             	Description
   ---------  ----------  ---------------   	-------------------------
    1.0       10/23/2018  Bala Seshadri   	Initial Build - Task ID: 20180116-00033
/*************************************************************************/
DROP TABLE XXCUS.XXCUS_OZF_RECURR_ACCR_PURDTLS CASCADE CONSTRAINTS;
CREATE TABLE XXCUS.XXCUS_OZF_RECURR_ACCR_PURDTLS
(
  RECORD_SEQ              NUMBER,
  RUN_ID                  NUMBER                DEFAULT 0,
  LINE_NUM                NUMBER                DEFAULT 0,
  CUST_ACCOUNT_ID         NUMBER,
  PLAN_ID                 NUMBER                DEFAULT 0,
  BRANCH_NAME             VARCHAR2(150 BYTE),
  BRANCH_CUST_ACCT_ID     NUMBER                DEFAULT 0,
  LINE_RECEIPT_AMOUNT     NUMBER,
  OBJECT_TYPE             VARCHAR2(10 BYTE),
  OBJECT_ID               NUMBER                DEFAULT 0,
  BRANCH_TOTAL_PURCHASE   NUMBER,
  TOTAL_RECURRING_AMOUNT  NUMBER,
  LINE_RECURRING_AMOUNT   NUMBER,
  STATUS_CODE             VARCHAR2(150 BYTE)    DEFAULT 'NEW'                 NOT NULL,
  STATUS_DESC             VARCHAR2(240 BYTE),
  UTILIZATION_ID          NUMBER                DEFAULT 0,
  EBS_UPDATED_BY          NUMBER,
  EBS_UPDATE_DATE         DATE,
  API_STATUS              VARCHAR2(1 BYTE),
  API_MESSAGE             VARCHAR2(2000 BYTE),
  DETAIL_SEQ              VARCHAR2(50 BYTE)     NOT NULL,
  RUN_MODE                VARCHAR2(20 BYTE)
)
TABLESPACE XXCUS_DATA
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;
COMMENT ON TABLE XXCUS.XXCUS_OZF_RECURR_ACCR_PURDTLS IS 'TMS 20180116-00033, FRESH DESK: 7696, SNOW: RITM0040124';