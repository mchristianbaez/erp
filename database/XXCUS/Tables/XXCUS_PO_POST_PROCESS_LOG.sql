   /* ************************************************************************
    HD Supply
    All rights reserved.
   **************************************************************************
     PURPOSE: Rename the oracle pdf output to a different file name and email it to the requisition requestor.
                         Original file is created by Oracle Payments job "HDS PO Output for Communication".
     REVISIONS:
     Ticket                          Ver             Date               Author                            Description
     ----------------------    ----------   ----------          ------------------------      -------------------------
     20171004-00368  1.0           10/04/2017    Bala Seshadri              Created.
   **************************************************************************/
create table xxcus.xxcus_po_post_process_log
(
 po_header_id number
,po_number varchar2(20)
,org_id number
,po_print_request_id number
,po_email_request_id number
,concurrent_outfile varchar2(240)
,temp_file varchar2(240)
,preparer_email varchar2(240)
,email_subject  varchar2(240)
,email_from varchar2(240)
,status varchar2(240)
,creation_date date
);
--
comment on table xxcus.xxcus_po_post_process_log is 'TMS 20171004-00368';
--