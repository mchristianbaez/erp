CREATE TABLE XXCUS.XXCUS_WC_DW_GL_HDR_T
(
  BATCH                  NUMBER,
  JE_HEADER_ID           NUMBER,
  USER_JE_CATEGORY_NAME  VARCHAR2(30 BYTE),
  NAME                   VARCHAR2(100 BYTE)
)
TABLESPACE XXCUS_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


