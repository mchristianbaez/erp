CREATE TABLE XXCUS.SYSTEM_EVENT_LOG_14OCT
(
  EVENT_NUMBER  NUMBER                          NOT NULL,
  EVENT_DESC    VARCHAR2(200 BYTE)              NOT NULL,
  DATE_STMP     VARCHAR2(20 BYTE)               NOT NULL
)
TABLESPACE XXCUS_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


