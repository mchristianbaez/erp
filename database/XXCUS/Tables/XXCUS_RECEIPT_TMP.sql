CREATE TABLE XXCUS.XXCUS_RECEIPT_TMP
(
  BU_NM               VARCHAR2(240 BYTE),
  VNDR_RECPT_UNQ_NBR  VARCHAR2(240 BYTE)
)
TABLESPACE XXCUS_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


