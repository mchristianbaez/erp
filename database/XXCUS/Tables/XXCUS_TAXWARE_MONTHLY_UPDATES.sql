CREATE TABLE XXCUS.XXCUS_TAXWARE_MONTHLY_UPDATES
(
  REQUEST_ID     NUMBER,
  CREATED_BY     NUMBER,
  CREATION_DATE  DATE
)
TABLESPACE XXCUS_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

COMMENT ON TABLE XXCUS.XXCUS_TAXWARE_MONTHLY_UPDATES IS 'Store the request id of the concurrent job that ran the monthly taxware updates';



