/**************************************************************************
   Module Name: ORACLE TRADE MANAGEMENT

   PURPOSE:   RECURRING ACCRUAL PROCESS
   REVISIONS:
   Ver        Date        Author             	Description
   ---------  ----------  ---------------   	-------------------------
    1.0       10/23/2018  Bala Seshadri   	Initial Build - Task ID: 20180116-00033
/*************************************************************************/
DROP TABLE XXCUS.XXCUS_OZF_RECURRING_ACCR_ERR CASCADE CONSTRAINTS;
CREATE TABLE XXCUS.XXCUS_OZF_RECURRING_ACCR_ERR
(
  RECORD_SEQ     NUMBER,
  ERR_SEQ        NUMBER,
  ERR_MESSAGE    VARCHAR2(4000 BYTE),
  LINE_NUM       NUMBER,
  RUN_ID         NUMBER,
  CREATED_BY     NUMBER,
  CREATION_DATE  DATE                           DEFAULT sysdate,
  DETAIL_SEQ     NUMBER                         DEFAULT 0
)
TABLESPACE XXCUS_DATA
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;
COMMENT ON TABLE XXCUS.XXCUS_OZF_RECURRING_ACCR_ERR IS 'TMS 20180116-00033, FRESH DESK: 7696, SNOW: RITM0040124';
