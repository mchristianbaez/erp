
  CREATE TABLE "XXCUS"."XXCUS_OZF_GL_JOURNALS_B" 
   (	"GL_SOURCE" VARCHAR2(25), 
	"GL_CATEGORY" VARCHAR2(25), 
	"FISCAL_MONTH" VARCHAR2(3), 
	"GL_PERIOD" VARCHAR2(15), 
	"XLA_ACCRUAL_PERIOD" VARCHAR2(15), 
	"XAEH_GL_XFER_STATUS_CODE" VARCHAR2(30), 
	"GL_SEGMENTS" VARCHAR2(181), 
	"GL_PRODUCT" VARCHAR2(25), 
	"GL_LOCATION" VARCHAR2(25), 
	"GL_ACCOUNT" VARCHAR2(25), 
	"JL_ACCOUNTING_DATE" DATE, 
	"XAEH_ACCOUNTING_DATE" DATE, 
	"ACCOUNTING_CLASS_CODE" VARCHAR2(30), 
	"SLA_EVENT_TYPE" VARCHAR2(30), 
	"GL_BATCH_NAME" VARCHAR2(100), 
	"GL_JOURNAL_NAME" VARCHAR2(100), 
	"GL_LINE_NUM" NUMBER(15,0), 
	"AMOUNT" NUMBER, 
	"SOURCE_DISTRIBUTION_TYPE" VARCHAR2(30), 
	"UTILIZATION_ID" NUMBER(38,0), 
	"GL_CCID" NUMBER(15,0), 
	"GL_SL_LINK_TABLE" VARCHAR2(30), 
	"GL_SL_LINK_ID" NUMBER, 
	"GL_LEDGER_NAME" VARCHAR2(30), 
	"GL_LEDGER_ID" NUMBER(15,0), 
	"EVENT_ID" NUMBER(15,0), 
	"ENTITY_ID" NUMBER(15,0), 
	"EVENT_TYPE_CODE" VARCHAR2(30), 
	"AE_HEADER_ID" NUMBER, 
	"AE_LINE_NUM" NUMBER, 
	"OFU_MVID_NAME" VARCHAR2(150), 
	"OFU_MVID" VARCHAR2(150), 
	"XLA_TRANSFER_MODE_CODE" VARCHAR2(1), 
	"ADJUSTMENT_TYPE" VARCHAR2(30), 
	"ADJUSTMENT_TYPE_ID" NUMBER, 
	"OBJECT_ID" NUMBER, 
	"UTIL_ACCTD_AMOUNT" NUMBER, 
	"UTILIZATION_TYPE" VARCHAR2(30), 
	"OFU_CUST_ACCOUNT_ID" NUMBER, 
	"QP_LIST_HEADER_ID" NUMBER, 
	"QP_GLOBAL_FLAG" VARCHAR2(1), 
	"FUND_ID" NUMBER, 
	"FUND_NAME" VARCHAR2(150), 
	"OFU_YEAR_ID" NUMBER, 
	"REFERENCE_ID" NUMBER, 
	"AMS_ACTIVITY_BUDGET_ID" NUMBER, 
	"OFFER_CODE" VARCHAR2(30), 
	"OFFER_NAME" VARCHAR2(2000), 
	"OFFER_TYPE" VARCHAR2(30), 
	"OFFER_STATUS_CODE" VARCHAR2(30), 
	"ACTIVITY_MEDIA_ID" NUMBER, 
	"ACTIVITY_MEDIA_NAME" VARCHAR2(120), 
	"CALENDAR_YEAR" VARCHAR2(150), 
	"BILL_TO_PARTY_NAME" VARCHAR2(150), 
	"RESALE_DATE_CREATED" DATE, 
	"DATE_INVOICED" DATE, 
	"PO_NUMBER" VARCHAR2(50), 
	"DATE_ORDERED" DATE, 
	"SELLING_PRICE" NUMBER, 
	"QUANTITY" NUMBER
   ) PCTFREE 10 PCTUSED 0 INITRANS 1 MAXTRANS 255 NOCOMPRESS 
  STORAGE(
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "XXCUS_DATA" 
  PARTITION BY LIST ("FISCAL_MONTH") 
 (PARTITION "JAN"  VALUES ('JAN') SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 0 INITRANS 10 MAXTRANS 255 NOCOMPRESS NOLOGGING 
  STORAGE(INITIAL 8388608 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "XXCUS_DATA" , 
 PARTITION "FEB"  VALUES ('FEB') SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 0 INITRANS 10 MAXTRANS 255 NOCOMPRESS NOLOGGING 
  STORAGE(INITIAL 8388608 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "XXCUS_DATA" , 
 PARTITION "MAR"  VALUES ('MAR') SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 0 INITRANS 10 MAXTRANS 255 NOCOMPRESS NOLOGGING 
  STORAGE(INITIAL 8388608 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "XXCUS_DATA" , 
 PARTITION "APR"  VALUES ('APR') SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 0 INITRANS 10 MAXTRANS 255 NOCOMPRESS NOLOGGING 
  STORAGE(INITIAL 8388608 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "XXCUS_DATA" , 
 PARTITION "MAY"  VALUES ('MAY') SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 0 INITRANS 10 MAXTRANS 255 NOCOMPRESS NOLOGGING 
  STORAGE(INITIAL 8388608 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "XXCUS_DATA" , 
 PARTITION "JUN"  VALUES ('JUN') SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 0 INITRANS 10 MAXTRANS 255 NOCOMPRESS NOLOGGING 
  STORAGE(INITIAL 8388608 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "XXCUS_DATA" , 
 PARTITION "JUL"  VALUES ('JUL') SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 0 INITRANS 10 MAXTRANS 255 NOCOMPRESS NOLOGGING 
  STORAGE(INITIAL 8388608 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "XXCUS_DATA" , 
 PARTITION "AUG"  VALUES ('AUG') SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 0 INITRANS 10 MAXTRANS 255 NOCOMPRESS NOLOGGING 
  STORAGE(INITIAL 8388608 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "XXCUS_DATA" , 
 PARTITION "SEP"  VALUES ('SEP') SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 0 INITRANS 10 MAXTRANS 255 NOCOMPRESS NOLOGGING 
  STORAGE(INITIAL 8388608 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "XXCUS_DATA" , 
 PARTITION "OCT"  VALUES ('OCT') SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 0 INITRANS 10 MAXTRANS 255 NOCOMPRESS NOLOGGING 
  STORAGE(INITIAL 8388608 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "XXCUS_DATA" , 
 PARTITION "NOV"  VALUES ('NOV') SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 0 INITRANS 10 MAXTRANS 255 NOCOMPRESS NOLOGGING 
  STORAGE(INITIAL 8388608 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "XXCUS_DATA" , 
 PARTITION "DEC"  VALUES ('DEC') SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 0 INITRANS 10 MAXTRANS 255 NOCOMPRESS NOLOGGING 
  STORAGE(INITIAL 8388608 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "XXCUS_DATA" , 
 PARTITION "NA"  VALUES (DEFAULT) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 0 INITRANS 10 MAXTRANS 255 NOCOMPRESS NOLOGGING 
  STORAGE(INITIAL 8388608 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "XXCUS_DATA" ) 
  PARALLEL ;
