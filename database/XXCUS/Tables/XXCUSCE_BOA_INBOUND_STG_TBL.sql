-- ******************************************************************************
-- *  Copyright (c) 2014 HDS Supply
-- *  All rights reserved.
-- ******************************************************************************
-- *   TABLE Name: XXCUSCE_BOA_INBOUND_STG_TBL 
-- *
-- *   PURPOSE:   SCOPE: BANK OF AMERICA CARD AND CONCUR GSC GO LIVE
-- *
-- *   REVISIONS:
-- *   Ver        Date        Author                     Description
-- *   ---------  ----------  ---------------         -------------------------
-- *   1.0        06/12/2018  Vamshi Singirikonda   	Initial Version (TMS 20180227-00179)
-- * ****************************************************************************
--
DROP TABLE XXCUS.XXCUSCE_BOA_INBOUND_STG_TBL;
--
CREATE TABLE XXCUS.XXCUSCE_BOA_INBOUND_STG_TBL 
   (	  Req_Name NUMBER,
  Req_Date DATE,
  Card_Nickname VARCHAR2(200),
  Vendor_Name VARCHAR2(1000),
  Amount NUMBER,
  Purchase_Date DATE,
  Post_Date DATE,
  Txn_Number VARCHAR2(100),
  Req_Number VARCHAR2(100),
  Creation_Date DATE 
   );
  
