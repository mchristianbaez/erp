/*
   Ticket#                                                                  Date                Ver#            Author                       Notes
   -----------------------------------------------------------------------------------------------------------------------------------------------------------
   TMS 20170712-00039 / RITM0011266    07/12/2017    1.0            Create out of pocket table for Waterworks separation. 
*/
drop table xxcus.xxcus_src_concur_sae_oop_ww;
--
create table xxcus.xxcus_src_concur_sae_oop_ww
(
  payment_batch      varchar2(255 byte),
  check_date         date,
  check_number       varchar2(30 byte),
  employee_number    varchar2(30 byte),
  emp_first_name     varchar2(150 byte),
  emp_last_name      varchar2(150 byte),
  terminated_date    date,
  employee_status    varchar2(10 byte),
  amount             number,
  payment_type_flag  varchar2(10 byte),
  line_numbers       varchar2(400 byte),
  ebs_created_by     number,
  ebs_creation_date  date,
  request_id         number,
  erp_platform       varchar2(30 char),
  record_status      varchar2(30 byte),
  crt_btch_id        number,
  concur_batch_id    number,
  attribute_1 varchar2(60),
  attribute_2 varchar2(60),
  attribute_3 varchar2(60),
  attribute_4 varchar2(60),
  attribute_5 varchar2(60),        
  attribute_6 varchar2(60),
  attribute_7 varchar2(60),
  attribute_8 varchar2(60),
  attribute_9 varchar2(60),
  attribute_10 varchar2(60),
  attribute_11 varchar2(60),
  attribute_12 varchar2(60),
  attribute_13 varchar2(60),
  attribute_14 varchar2(60),        
  attribute_15 varchar2(60)              
)
;
--
comment on table xxcus.xxcus_src_concur_sae_oop_ww is 'TMS 20170712-00039 / RITM0011266';
--