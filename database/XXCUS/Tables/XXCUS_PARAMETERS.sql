CREATE TABLE XXCUS.XXCUS_PARAMETERS
(
  PROCEDURE_NAME  VARCHAR2(50 BYTE)             NOT NULL,
  PARM_NAME       VARCHAR2(100 BYTE)            NOT NULL,
  PARM_VALUE      VARCHAR2(400 BYTE)
)
TABLESPACE XXCUS_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          1M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


