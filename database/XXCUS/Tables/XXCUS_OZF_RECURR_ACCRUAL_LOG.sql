/**************************************************************************
   Module Name: ORACLE TRADE MANAGEMENT

   PURPOSE:   RECURRING ACCRUAL PROCESS
   REVISIONS:
   Ver        Date        Author             	Description
   ---------  ----------  ---------------   	-------------------------
    1.0       10/23/2018  Bala Seshadri   	Initial Build - Task ID: 20180116-00033
/*************************************************************************/
DROP TABLE XXCUS.XXCUS_OZF_RECURR_ACCRUAL_LOG CASCADE CONSTRAINTS;
CREATE TABLE XXCUS.XXCUS_OZF_RECURR_ACCRUAL_LOG
(
  REQUEST_ID                NUMBER,
  LINE_TYPE                 VARCHAR2(20 BYTE),
  RECORD_SEQ                NUMBER,
  LINE_NUM                  NUMBER,
  DETAIL_SEQ                NUMBER,
  RESALE_LINE_ID            NUMBER,
  UTILIZATION_ID            NUMBER              DEFAULT 0,
  MVID                      VARCHAR2(30 BYTE),
  MVID_NAME                 VARCHAR2(360 BYTE),
  OFFER_NAME                VARCHAR2(255 BYTE),
  CURRENCY                  VARCHAR2(3 BYTE),
  RECEIPT_START_DATE        DATE,
  RECEIPT_END_DATE          DATE,
  REASON_NAME               VARCHAR2(150 BYTE),
  LOB_NAME                  VARCHAR2(360 BYTE),
  BU_NAME                   VARCHAR2(360 BYTE),
  BRANCH_NAME               VARCHAR2(150 BYTE),
  STATUS_CODE               VARCHAR2(240 BYTE),
  STATUS_DESC               VARCHAR2(240 BYTE),
  RECEIPT_AMOUNT            NUMBER,
  BRANCH_TOTAL_RECEIPT      NUMBER,
  APPLIED_RECURRING_AMOUNT  NUMBER,
  TOTAL_RECURRING_AMOUNT    NUMBER,
  CUST_ACCOUNT_ID           NUMBER,
  PLAN_ID                   NUMBER,
  LOB_PARTY_ID              NUMBER,
  BU_PARTY_ID               NUMBER,
  BRANCH_CUST_ACCT_ID       NUMBER,
  RUN_ID                    NUMBER,
  FUND_ID                   NUMBER,
  EBS_UPDATED_BY            NUMBER,
  EBS_UPDATE_DATE           DATE,
  RUN_MODE                  VARCHAR2(20 BYTE)
)
TABLESPACE XXCUS_DATA
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;
COMMENT ON TABLE XXCUS.XXCUS_OZF_RECURR_ACCRUAL_LOG IS 'TMS 20180116-00033, FRESH DESK: 7696, SNOW: RITM0040124';