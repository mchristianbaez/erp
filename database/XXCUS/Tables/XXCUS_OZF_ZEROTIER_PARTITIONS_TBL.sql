/*
TMS 20160602-00010 / ESMS 321264
Author: balaguru seshadri
Date: 07/07/2016
Scope:  The new table will hold the the tier zero partitions created.
*/
drop table xxcus.xxcus_ozf_zerotier_partitions cascade constraints;
--
create table xxcus.xxcus_ozf_zerotier_partitions
(
  partition_name    varchar2(10 byte),
  creation_date     date,
  created_by        number,
  request_id        number,
  last_update_date  date,
  last_updated_by   number
)
tablespace xxcus_data
result_cache (mode default)
pctused    0
pctfree    10
initrans   1
maxtrans   255
storage    (
            initial          64k
            next             1m
            minextents       1
            maxextents       unlimited
            pctincrease      0
            buffer_pool      default
            flash_cache      default
            cell_flash_cache default
           )
logging 
nocompress 
nocache
noparallel
monitoring;
--
comment on table xxcus.xxcus_ozf_zerotier_partitions is 'TMS  20160602-00010 / ESMS 321264';
--
