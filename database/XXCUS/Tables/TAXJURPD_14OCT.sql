CREATE TABLE XXCUS.TAXJURPD_14OCT
(
  STCODE       NUMBER(2)                        NOT NULL,
  JURINTRCODE  VARCHAR2(2 BYTE)                 NOT NULL,
  JURCNTYCODE  VARCHAR2(2 BYTE)                 NOT NULL,
  JURCITYCODE  VARCHAR2(2 BYTE)                 NOT NULL,
  JURTRNSCODE  VARCHAR2(2 BYTE)                 NOT NULL,
  JURISCODE    VARCHAR2(2 BYTE)                 NOT NULL
)
TABLESPACE XXCUS_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


