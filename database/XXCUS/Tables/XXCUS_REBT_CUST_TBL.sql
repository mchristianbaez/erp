CREATE TABLE XXCUS.XXCUS_REBT_CUST_TBL
(
  REBT_CUST_ACCT_ID  NUMBER(15),
  RESALE_BATCH_ID    NUMBER(15),
  ORG_ID             NUMBER(15),
  STATUS_CODE        VARCHAR2(30 BYTE),
  REQUEST_CODE       VARCHAR2(1 BYTE)
)
TABLESPACE XXCUS_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


