/*
 -- Author: Balaguru Seshadri
 -- Scope: This package extracts premises and operations data for use by Real Estate SuperSearch UI.
 -- Modification History
 -- ESMS         RFC     Date           Version   Comments
 -- =========== ======== ============== =======   ============================================================================================================
 -- 315719               10-FEB-2016    1.0       New script. TMS 20160203-00038
*/ 
CREATE TABLE XXCUS.XXCUS_OPN_OAF_SS_PREM_OPER_ALL
(
  LOC              VARCHAR2(30 BYTE),
  LOC_TYPE         VARCHAR2(30 BYTE),
  RER_ID           VARCHAR2(30 BYTE),
  ADDRESS          VARCHAR2(301 BYTE),
  SF_ACREAGE       NUMBER,
  SECTION          VARCHAR2(90 BYTE),
  BU               VARCHAR2(30 BYTE),
  ORACLE_ID        VARCHAR2(5 BYTE),
  BR               VARCHAR2(6 BYTE),
  FRU              VARCHAR2(10 BYTE),
  FRU_DESCRIPTION  VARCHAR2(75 BYTE),
  OPS_TYPE         VARCHAR2(80 BYTE),
  OPS_SF           NUMBER,
  OPS_START        DATE,
  OPS_END          DATE,
  OPS_STATUS       VARCHAR2(8 BYTE),
  COMMENTS         VARCHAR2(2000),
  LEASE_ID         NUMBER,
  LEASE_CHANGE_ID  NUMBER,
  DISPLAY_SEQ      NUMBER
)
TABLESPACE XXCUS_DATA
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;
--
COMMENT ON TABLE XXCUS.XXCUS_OPN_OAF_SS_PREM_OPER_ALL IS 'TMS : 20160203-00038 / ESMS : 315719';
--
