
  CREATE TABLE "XXCUS"."XXCUS_BACKUP_9_LOCS_DELETED" 
   (	"LOCATION_ID" NUMBER, 
	"LAST_UPDATE_DATE" DATE, 
	"LAST_UPDATED_BY" NUMBER, 
	"CREATION_DATE" DATE, 
	"CREATED_BY" NUMBER, 
	"LAST_UPDATE_LOGIN" NUMBER, 
	"LOCATION_PARK_ID" NUMBER, 
	"LOCATION_TYPE_LOOKUP_CODE" VARCHAR2(30), 
	"LOCATION_CODE" VARCHAR2(90), 
	"LOCATION_ALIAS" VARCHAR2(30), 
	"BUILDING" VARCHAR2(50), 
	"LEASE_OR_OWNED" VARCHAR2(30), 
	"FLOOR" VARCHAR2(20), 
	"OFFICE" VARCHAR2(20), 
	"ADDRESS_ID" NUMBER, 
	"MAX_CAPACITY" NUMBER, 
	"OPTIMUM_CAPACITY" NUMBER, 
	"RENTABLE_AREA" NUMBER, 
	"USABLE_AREA" NUMBER, 
	"ALLOCATE_COST_CENTER_CODE" VARCHAR2(30), 
	"UOM_CODE" VARCHAR2(3), 
	"DESCRIPTION" VARCHAR2(50), 
	"PARENT_LOCATION_ID" NUMBER, 
	"INTERFACE_FLAG" VARCHAR2(1), 
	"REQUEST_ID" NUMBER, 
	"PROGRAM_APPLICATION_ID" NUMBER, 
	"PROGRAM_ID" NUMBER, 
	"PROGRAM_UPDATE_DATE" DATE, 
	"STATUS" VARCHAR2(1), 
	"SPACE_TYPE_LOOKUP_CODE" VARCHAR2(30), 
	"ATTRIBUTE_CATEGORY" VARCHAR2(30), 
	"ATTRIBUTE1" VARCHAR2(150), 
	"ATTRIBUTE2" VARCHAR2(150), 
	"ATTRIBUTE3" VARCHAR2(150), 
	"ATTRIBUTE4" VARCHAR2(150), 
	"ATTRIBUTE5" VARCHAR2(150), 
	"ATTRIBUTE6" VARCHAR2(150), 
	"ATTRIBUTE7" VARCHAR2(150), 
	"ATTRIBUTE8" VARCHAR2(150), 
	"ATTRIBUTE9" VARCHAR2(150), 
	"ATTRIBUTE10" VARCHAR2(150), 
	"ATTRIBUTE11" VARCHAR2(150), 
	"ATTRIBUTE12" VARCHAR2(150), 
	"ATTRIBUTE13" VARCHAR2(150), 
	"ATTRIBUTE14" VARCHAR2(150), 
	"ATTRIBUTE15" VARCHAR2(150), 
	"ORG_ID" NUMBER(15,0), 
	"SOURCE" VARCHAR2(80), 
	"PROPERTY_ID" NUMBER, 
	"CLASS" VARCHAR2(30), 
	"STATUS_TYPE" VARCHAR2(30), 
	"SUITE" VARCHAR2(30), 
	"GROSS_AREA" NUMBER, 
	"ASSIGNABLE_AREA" NUMBER, 
	"COMMON_AREA" NUMBER, 
	"COMMON_AREA_FLAG" VARCHAR2(1), 
	"FUNCTION_TYPE_LOOKUP_CODE" VARCHAR2(30), 
	"STANDARD_TYPE_LOOKUP_CODE" VARCHAR2(30), 
	"ACTIVE_START_DATE" DATE, 
	"ACTIVE_END_DATE" DATE, 
	"BOOKABLE_FLAG" VARCHAR2(1), 
	"OCCUPANCY_STATUS_CODE" VARCHAR2(30), 
	"ASSIGNABLE_EMP" VARCHAR2(30), 
	"ASSIGNABLE_CC" VARCHAR2(30), 
	"ASSIGNABLE_CUST" VARCHAR2(30), 
	"DISPOSITION_CODE" VARCHAR2(30), 
	"ACC_TREATMENT_CODE" VARCHAR2(30)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "XXCUS_DATA" ;
