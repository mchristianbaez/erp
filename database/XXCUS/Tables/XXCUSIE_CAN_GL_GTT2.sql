
  CREATE GLOBAL TEMPORARY TABLE "XXCUS"."XXCUSIE_CAN_GL_GTT2" 
   (	"DESCR" VARCHAR2(50), 
	"REPORT_NUM" VARCHAR2(50), 
	"SEGMENT1" VARCHAR2(25), 
	"SEGMENT2" VARCHAR2(25), 
	"SEGMENT3" VARCHAR2(25), 
	"SEGMENT4" VARCHAR2(25), 
	"SEGMENT5" VARCHAR2(25), 
	"SEGMENT6" VARCHAR2(25), 
	"SEGMENT7" VARCHAR2(25), 
	"PERIOD_NAME" VARCHAR2(15), 
	"ACCOUNTED_CR" NUMBER, 
	"ACCOUNTED_DR" NUMBER, 
	"EMPLOYEE_NUM" VARCHAR2(30)
   ) ON COMMIT PRESERVE ROWS ;
