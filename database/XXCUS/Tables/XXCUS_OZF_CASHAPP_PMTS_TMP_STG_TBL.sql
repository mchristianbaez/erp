/*************************************************************************
  $Header XXCUS_OZF_CASHAPP_PMTS_TMP_STG_TBL.sql $
  Module Name: XXCUS_OZF_CASHAPP_PMTS_TMP_STG_TBL

  PURPOSE: Used by HDS Rebates AR Auto Cash Application Process 

  REVISIONS:
  Ver        Date                   Author                          Description
  ---------  -----------          ------------------             ----------------
  1.0        03-Aug-2015  Balaguru Seshadri     Initial Version TMS # 20150710-00251

**************************************************************************/
--
DROP TABLE XXCUS.XXCUS_OZF_CASHAPP_PMTS_TMP_STG CASCADE CONSTRAINTS;
--
CREATE TABLE XXCUS.XXCUS_OZF_CASHAPP_PMTS_TMP_STG
(
  CASH_RECEIPT_ID  NUMBER,
  REQUEST_ID       NUMBER
);
--
COMMENT ON TABLE XXCUS.XXCUS_OZF_CASHAPP_PMTS_TMP_STG IS 'TMS 20150710-00251';
--