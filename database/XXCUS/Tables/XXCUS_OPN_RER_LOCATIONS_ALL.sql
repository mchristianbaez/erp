   /* ************************************************************************
    HD Supply
    All rights reserved.
    Ticket                    Ver          Date         Author                       Description
    ----------------------    ----------   ----------   ------------------------     -------------------------
    Not available             1.0          08/21/2015   Production version     
    TMS 20150909-00140        1.1          08/21/2015   Balaguru Seshadri            Remove comemnted fields listed in Ver 1.1
    TMS 20151014-00024        1.2          10/22/2015   Balaguru Seshadri            Add new fields HDS_TOTAL_BLDG_SF_FOR_RER, HDS_TOTAL_LAND_SF_FOR_RER,
	                                                                                 HDS_TEN_LOC_RENTABLE_AREA_SF and hds_loc_tie_breaker_seq.
    TMS 20160302-00017        1.3          03/04/2016   Balaguru Seshadri            Add extract date field.
   ************************************************************************ */
DROP TABLE XXCUS.XXCUS_OPN_RER_LOCATIONS_ALL CASCADE CONSTRAINTS;
--
CREATE TABLE XXCUS.XXCUS_OPN_RER_LOCATIONS_ALL
(
  RER_ID                     VARCHAR2(30 BYTE),
  RER_NAME                   VARCHAR2(50 BYTE),
  MASTER_RER_ID              VARCHAR2(30 BYTE),
  MASTER_RER_NAME            VARCHAR2(50 BYTE),
  HDS_OPN_PRIMARY_LOC_FOR_RER VARCHAR2(1), --Ver 1.1
  LEASE_TYPE                 VARCHAR2(80 BYTE),
  LEASE_CLASS                VARCHAR2(80 BYTE),
  ABSTRACTED_BY              VARCHAR2(100 BYTE),
  APPROVAL_STATUS            VARCHAR2(150 BYTE),
  LEASE_STATUS               VARCHAR2(150 BYTE),
  SCHEDULED_YEAR_START_DATE  DATE,
  PRI_LOC_CODE               VARCHAR2(30 BYTE),
  PRI_LOC_NAME               VARCHAR2(150 BYTE),
  LEASE_EXECUTION            DATE,
  LEASE_COMMENCEMENT         DATE,
  LEASE_TERMINATION          DATE,
  LEASE_EXTN_END_DATE        DATE,
  GL_EXPENSE_PRODUCT         VARCHAR2(10 BYTE),
  GL_EXPENSE                 VARCHAR2(150 BYTE),
  GL_LIABILITY               VARCHAR2(150 BYTE),
  GL_ACCRUED_LIABILITY       VARCHAR2(150 BYTE),
  --BU                         VARCHAR2(20 BYTE), --Ver 1.1 
  --BU_DESCRIPTION             VARCHAR2(60 BYTE), --Ver 1.1 
  LOB_ABB                         VARCHAR2(30 BYTE), --Ver 1.1 
  LOB_DBA             VARCHAR2(60 BYTE), --Ver 1.1 
  LOB_BRANCH                 VARCHAR2(60 BYTE),
  SUBLOB_ABB VARCHAR2(30 BYTE),--Ver 1.1 
  SUBLOB_DBA VARCHAR2(30 BYTE),--Ver 1.1 
  SUB_BU_RANK NUMBER,--Ver 1.1
  ORACLE_ID                  VARCHAR2(30 BYTE),
  FRU                        VARCHAR2(20 BYTE),
  FRU_DESCRIPTION            VARCHAR2(60 BYTE),
  REM_NTID                   VARCHAR2(60 BYTE),
  REM_NAME                   VARCHAR2(60 BYTE),
  LOC_TYPE_LOOKUP_CODE       VARCHAR2(30 BYTE),
  LOC_TYPE                   VARCHAR2(30 BYTE),
  LOC_CODE                   VARCHAR2(90 BYTE),
  SECTION_SUITE              VARCHAR2(150 BYTE), --Ver 3.2
  OPN_TENANCY_SUITE VARCHAR2(150 BYTE), --Ver 3.2
  TENANCY_PRI_FLAG           VARCHAR2(30 BYTE),
  TENANCY_USAGE              VARCHAR2(80 BYTE),
  --EST_OCCU_DATE              DATE, --Ver 1.1
  --ACTUAL_OCCU_DATE           DATE, --Ver 1.1
  --EXP_DATE                   DATE, --Ver 1.1
  --FIN_OBLIG_END_DATE         DATE, --Ver 1.1
  --RECOVERY_TYPE              VARCHAR2(80 BYTE), --Ver 1.1
  --RECOVERY_SPACE_STD         VARCHAR2(80 BYTE), --Ver 1.1
  --CUSTOMER_NUMBER            VARCHAR2(30 BYTE), --Ver 1.1
  --CUSTOMER_NAME              VARCHAR2(150 BYTE), --Ver 1.1
  --BILL_TO_SITE               VARCHAR2(40 BYTE), --Ver 1.1
  ADDRESS_LINE1              VARCHAR2(150 BYTE),
  ADDRESS_LINE2              VARCHAR2(150 BYTE),
  ADDRESS_LINE3              VARCHAR2(150 BYTE),
  CITY                       VARCHAR2(150 BYTE),
  POSTAL_CODE                VARCHAR2(30 BYTE),
  COUNTY                     VARCHAR2(60 BYTE),
  STATE_PROVINCE             VARCHAR2(150 BYTE),
  COUNTRY_CODE               VARCHAR2(30 BYTE),
  COUNTRY                    VARCHAR2(150 BYTE),
  --REGION                     VARCHAR2(150 BYTE), --Ver 1.1
  --OFFICE_PARK                VARCHAR2(150 BYTE), --Ver 1.1
  --PROPERTY_NAME              VARCHAR2(150 BYTE), --Ver 1.1
  --BLDG_LAND_NAME             VARCHAR2(150 BYTE), --Ver 1.1
  --FLOOR_PARCEL_NAME          VARCHAR2(150 BYTE), --Ver 1.1
  --SECTION_YARD_NAME          VARCHAR2(150 BYTE), --Ver 1.1
  LEGACY_ID                  VARCHAR2(150 BYTE),
  MORTGAGED                  VARCHAR2(150 BYTE),
  RELATED_PARTY              VARCHAR2(150 BYTE),
  RELATED_PARTY_NAME         VARCHAR2(150 BYTE),
  RELATED_PARTY_COMMENTS     VARCHAR2(150 BYTE),
  SALE_LEASEBACK             VARCHAR2(150 BYTE),
  GUARANTOR                  VARCHAR2(150 BYTE),
  RER_DOC_LINK               VARCHAR2(150 BYTE),
  CAPITAL_LEASE              VARCHAR2(150 BYTE),
  RENT_TYPE                  VARCHAR2(150 BYTE),
  LEASE_STATUS_CODE          VARCHAR2(30 BYTE),
  APPROVAL_STATUS_CODE       VARCHAR2(1 BYTE),
  LEASE_TYPE_CODE            VARCHAR2(30 BYTE),
  LEASE_CLASS_CODE           VARCHAR2(30 BYTE),
  RECOVERY_SPACE_STD_CODE    VARCHAR2(30 BYTE),
  RECOVERY_TYPE_CODE         VARCHAR2(30 BYTE),
  TENANCY_USAGE_CODE         VARCHAR2(30 BYTE),
  hds_total_bldg_sf_for_rer number, -- Ver 1.2
  hds_total_land_sf_for_rer number, -- Ver 1.2
  hds_ten_loc_rentable_area_sf number, -- Ver 1.2
  hds_loc_tie_breaker_seq number, --Ver 1.2
  TENANCY_ID                 NUMBER,
  TEN_LOCN_ID                NUMBER,
  LEA_LOCN_ID                NUMBER,
  LEASE_ID                   NUMBER,
  LEASE_CHANGE_ID            NUMBER,
  CUSTOMER_ID                NUMBER(15),
  CUSTOMER_SITE_USE_ID       NUMBER(15),
  ABSTRACTED_BY_USER_ID      NUMBER,
  ORG_ID                     NUMBER(15),
  ADDRESS_ID                 NUMBER,
  LEA_EXPENSE_CCID           NUMBER,
  LEA_LIABILITY_CCID         NUMBER(15),
  LEA_ACCRUAL_CCID           NUMBER(15),
  REM_USER_ID                NUMBER,
  LOC_ACTIVE_START_DATE DATE,
  LOC_ACTIVE_END_DATE DATE,
  EXTRACT_DATE DATE
)
TABLESPACE XXCUS_DATA
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
NOLOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;
--
COMMENT ON TABLE XXCUS.XXCUS_OPN_RER_LOCATIONS_ALL IS 'TMS 20151014-00024 / ESMS 304543';
--
GRANT SELECT ON XXCUS.XXCUS_OPN_RER_LOCATIONS_ALL TO INTERFACE_DSTAGE;
--