CREATE TABLE XXCUS.XXCUSOIE_BULLET_TRAIN_Q1_TBL
(
  CREDIT_CARD_TRX_ID  NUMBER                    NOT NULL
)
TABLESPACE XXCUS_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


