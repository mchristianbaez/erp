CREATE TABLE XXCUS.XXCUSCE_BANK_FILE_TRG
(
  STG_TRX_ID         VARCHAR2(300 BYTE),
  DIRECTORY_NAME     VARCHAR2(500 BYTE),
  FILE_NAME          VARCHAR2(500 BYTE),
  CREATION_DATE      VARCHAR2(50 BYTE),
  CREATED_BY         VARCHAR2(20 BYTE),
  LAST_UPDATE_DATE   VARCHAR2(50 BYTE),
  LAST_UPDATE_LOGIN  VARCHAR2(10 BYTE),
  ERROR_COMMENT      VARCHAR2(500 BYTE),
  BANK_NAME          VARCHAR2(250 BYTE),
  PD_OR_CD_FLAG      VARCHAR2(10 BYTE),
  STATUS             VARCHAR2(100 BYTE),
  CREATION_TIME      VARCHAR2(50 BYTE),
  CHECK_SUM          VARCHAR2(100 BYTE),
  FILE_DATE          VARCHAR2(50 BYTE),
  FILE_TIME          VARCHAR2(100 BYTE)
)
TABLESPACE XXCUS_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          1M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


