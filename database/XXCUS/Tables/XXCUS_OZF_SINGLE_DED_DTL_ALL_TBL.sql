--
CREATE TABLE XXCUS.XXCUS_OZF_SINGLE_DED_DTL_ALL
(
 invoice_num           VARCHAR2(30)
,invoice_date          DATE
,start_date            DATE
,end_date              DATE
,rebate_type           VARCHAR2(60)
,timeframe             VARCHAR2(10)
,rebate_amount         NUMBER
,purchase_amount       NUMBER
,collection_rate       NUMBER
,mvid_number           VARCHAR2(30)
,mvid_name             VARCHAR2(150)
,display_flag          VARCHAR2(1)
,hdr_customer_trx_id   NUMBER
,customer_trx_id       NUMBER
,org_id                NUMBER
,request_id            NUMBER
,created_by            NUMBER
,creation_date         DATE
);
--
CREATE INDEX XXCUS.XXCUS_OZF_SINGLE_DED_DTL_N1 ON XXCUS.XXCUS_OZF_SINGLE_DED_DTL_ALL (HDR_CUSTOMER_TRX_ID);
--
COMMENT ON TABLE XXCUS.XXCUS_OZF_SINGLE_DED_DTL_ALL IS 'RFC 43035 / ESMS 269629';
--