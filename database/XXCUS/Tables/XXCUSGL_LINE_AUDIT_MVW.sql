CREATE TABLE XXCUS.XXCUSGL_LINE_AUDIT_MVW
(
  SEGMENT1             VARCHAR2(25 BYTE),
  SEGMENT2             VARCHAR2(25 BYTE),
  SEGMENT3             VARCHAR2(25 BYTE),
  SEGMENT4             VARCHAR2(25 BYTE),
  SEGMENT5             VARCHAR2(25 BYTE),
  SEGMENT6             VARCHAR2(25 BYTE),
  SEGMENT7             VARCHAR2(25 BYTE),
  JE_LINE_NUM          NUMBER(15)               NOT NULL,
  LEDGER_ID            NUMBER(15)               NOT NULL,
  PERIOD_NAME          VARCHAR2(15 BYTE)        NOT NULL,
  EFFECTIVE_DATE       DATE                     NOT NULL,
  POSTED_DATE          DATE,
  DESCRIPTION          VARCHAR2(240 BYTE),
  USER_JE_SOURCE_NAME  VARCHAR2(25 BYTE)        NOT NULL,
  JE_BATCH_ID          NUMBER(15)               NOT NULL,
  BATCHNAME            VARCHAR2(100 BYTE)       NOT NULL,
  JE_HEADER_ID         NUMBER(15)               NOT NULL,
  HEADERNAME           VARCHAR2(100 BYTE)       NOT NULL,
  FULL_NAME            VARCHAR2(240 BYTE),
  ENTERED_DR           NUMBER,
  ENTERED_CR           NUMBER
)
TABLESPACE XXCUS_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


