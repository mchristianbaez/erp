CREATE TABLE XXCUS.XXCUSHR_PER_ASSIGN_PRIOR_TBL
(
  ASSIGNMENT_ID                   NUMBER(10)    NOT NULL,
  EFFECTIVE_START_DATE            DATE          NOT NULL,
  EFFECTIVE_END_DATE              DATE          NOT NULL,
  BUSINESS_GROUP_ID               NUMBER(15)    NOT NULL,
  RECRUITER_ID                    NUMBER(10),
  GRADE_ID                        NUMBER(15),
  POSITION_ID                     NUMBER(15),
  JOB_ID                          NUMBER(15),
  ASSIGNMENT_STATUS_TYPE_ID       NUMBER(9)     NOT NULL,
  PAYROLL_ID                      NUMBER(9),
  LOCATION_ID                     NUMBER(15),
  PERSON_REFERRED_BY_ID           NUMBER(10),
  SUPERVISOR_ID                   NUMBER(10),
  SPECIAL_CEILING_STEP_ID         NUMBER(15),
  PERSON_ID                       NUMBER(10)    NOT NULL,
  RECRUITMENT_ACTIVITY_ID         NUMBER(15),
  SOURCE_ORGANIZATION_ID          NUMBER(15),
  ORGANIZATION_ID                 NUMBER(15)    NOT NULL,
  PEOPLE_GROUP_ID                 NUMBER(15),
  SOFT_CODING_KEYFLEX_ID          NUMBER(15),
  VACANCY_ID                      NUMBER(15),
  PAY_BASIS_ID                    NUMBER(9),
  ASSIGNMENT_SEQUENCE             NUMBER(15)    NOT NULL,
  ASSIGNMENT_TYPE                 VARCHAR2(1 BYTE) NOT NULL,
  PRIMARY_FLAG                    VARCHAR2(30 BYTE) NOT NULL,
  APPLICATION_ID                  NUMBER(15),
  ASSIGNMENT_NUMBER               VARCHAR2(30 BYTE),
  CHANGE_REASON                   VARCHAR2(30 BYTE),
  COMMENT_ID                      NUMBER(15),
  DATE_PROBATION_END              DATE,
  DEFAULT_CODE_COMB_ID            NUMBER(15),
  EMPLOYMENT_CATEGORY             VARCHAR2(30 BYTE),
  FREQUENCY                       VARCHAR2(30 BYTE),
  INTERNAL_ADDRESS_LINE           VARCHAR2(80 BYTE),
  MANAGER_FLAG                    VARCHAR2(30 BYTE),
  NORMAL_HOURS                    NUMBER(22,3),
  PERF_REVIEW_PERIOD              NUMBER(15),
  PERF_REVIEW_PERIOD_FREQUENCY    VARCHAR2(30 BYTE),
  PERIOD_OF_SERVICE_ID            NUMBER(15),
  PROBATION_PERIOD                NUMBER(22,2),
  PROBATION_UNIT                  VARCHAR2(30 BYTE),
  SAL_REVIEW_PERIOD               NUMBER(15),
  SAL_REVIEW_PERIOD_FREQUENCY     VARCHAR2(30 BYTE),
  SET_OF_BOOKS_ID                 NUMBER(15),
  SOURCE_TYPE                     VARCHAR2(30 BYTE),
  TIME_NORMAL_FINISH              VARCHAR2(5 BYTE),
  TIME_NORMAL_START               VARCHAR2(5 BYTE),
  BARGAINING_UNIT_CODE            VARCHAR2(30 BYTE),
  LABOUR_UNION_MEMBER_FLAG        VARCHAR2(30 BYTE),
  HOURLY_SALARIED_CODE            VARCHAR2(30 BYTE),
  CONTRACT_ID                     NUMBER(9),
  COLLECTIVE_AGREEMENT_ID         NUMBER(9),
  CAGR_ID_FLEX_NUM                NUMBER(15),
  CAGR_GRADE_DEF_ID               NUMBER(15),
  ESTABLISHMENT_ID                NUMBER(15),
  REQUEST_ID                      NUMBER(15),
  PROGRAM_APPLICATION_ID          NUMBER(15),
  PROGRAM_ID                      NUMBER(15),
  PROGRAM_UPDATE_DATE             DATE,
  ASS_ATTRIBUTE_CATEGORY          VARCHAR2(30 BYTE),
  ASS_ATTRIBUTE1                  VARCHAR2(150 BYTE),
  ASS_ATTRIBUTE2                  VARCHAR2(150 BYTE),
  ASS_ATTRIBUTE3                  VARCHAR2(150 BYTE),
  ASS_ATTRIBUTE4                  VARCHAR2(150 BYTE),
  ASS_ATTRIBUTE5                  VARCHAR2(150 BYTE),
  ASS_ATTRIBUTE6                  VARCHAR2(150 BYTE),
  ASS_ATTRIBUTE7                  VARCHAR2(150 BYTE),
  ASS_ATTRIBUTE8                  VARCHAR2(150 BYTE),
  ASS_ATTRIBUTE9                  VARCHAR2(150 BYTE),
  ASS_ATTRIBUTE10                 VARCHAR2(150 BYTE),
  ASS_ATTRIBUTE11                 VARCHAR2(150 BYTE),
  ASS_ATTRIBUTE12                 VARCHAR2(150 BYTE),
  ASS_ATTRIBUTE13                 VARCHAR2(150 BYTE),
  ASS_ATTRIBUTE14                 VARCHAR2(150 BYTE),
  ASS_ATTRIBUTE15                 VARCHAR2(150 BYTE),
  ASS_ATTRIBUTE16                 VARCHAR2(150 BYTE),
  ASS_ATTRIBUTE17                 VARCHAR2(150 BYTE),
  ASS_ATTRIBUTE18                 VARCHAR2(150 BYTE),
  ASS_ATTRIBUTE19                 VARCHAR2(150 BYTE),
  ASS_ATTRIBUTE20                 VARCHAR2(150 BYTE),
  ASS_ATTRIBUTE21                 VARCHAR2(150 BYTE),
  ASS_ATTRIBUTE22                 VARCHAR2(150 BYTE),
  ASS_ATTRIBUTE23                 VARCHAR2(150 BYTE),
  ASS_ATTRIBUTE24                 VARCHAR2(150 BYTE),
  ASS_ATTRIBUTE25                 VARCHAR2(150 BYTE),
  ASS_ATTRIBUTE26                 VARCHAR2(150 BYTE),
  ASS_ATTRIBUTE27                 VARCHAR2(150 BYTE),
  ASS_ATTRIBUTE28                 VARCHAR2(150 BYTE),
  ASS_ATTRIBUTE29                 VARCHAR2(150 BYTE),
  ASS_ATTRIBUTE30                 VARCHAR2(150 BYTE),
  LAST_UPDATE_DATE                DATE,
  LAST_UPDATED_BY                 NUMBER(15),
  LAST_UPDATE_LOGIN               NUMBER(15),
  CREATED_BY                      NUMBER(15),
  CREATION_DATE                   DATE,
  TITLE                           VARCHAR2(30 BYTE),
  OBJECT_VERSION_NUMBER           NUMBER(9),
  NOTICE_PERIOD                   NUMBER(10),
  NOTICE_PERIOD_UOM               VARCHAR2(30 BYTE),
  EMPLOYEE_CATEGORY               VARCHAR2(30 BYTE),
  WORK_AT_HOME                    VARCHAR2(30 BYTE),
  JOB_POST_SOURCE_NAME            VARCHAR2(240 BYTE),
  POSTING_CONTENT_ID              NUMBER(15),
  PERIOD_OF_PLACEMENT_DATE_START  DATE,
  VENDOR_ID                       NUMBER(15),
  VENDOR_EMPLOYEE_NUMBER          VARCHAR2(30 BYTE),
  VENDOR_ASSIGNMENT_NUMBER        VARCHAR2(30 BYTE),
  ASSIGNMENT_CATEGORY             VARCHAR2(30 BYTE),
  PROJECT_TITLE                   VARCHAR2(30 BYTE),
  APPLICANT_RANK                  NUMBER,
  VENDOR_SITE_ID                  NUMBER(15),
  PO_HEADER_ID                    NUMBER(15),
  PO_LINE_ID                      NUMBER(15),
  PROJECTED_ASSIGNMENT_END        DATE,
  GRADE_LADDER_PGM_ID             NUMBER(15),
  SUPERVISOR_ASSIGNMENT_ID        NUMBER(15)
)
TABLESPACE XXCUS_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


