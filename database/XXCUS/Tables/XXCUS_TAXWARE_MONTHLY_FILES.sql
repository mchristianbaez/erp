CREATE TABLE XXCUS.XXCUS_TAXWARE_MONTHLY_FILES
(
  REQUEST_ID     NUMBER                         NOT NULL,
  FILE_PATH      VARCHAR2(80 BYTE)              NOT NULL,
  FILE_NAME      VARCHAR2(80 BYTE)              NOT NULL,
  CREATED_BY     NUMBER,
  CREATION_DATE  DATE
)
TABLESPACE APPS_TS_TX_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             128K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


