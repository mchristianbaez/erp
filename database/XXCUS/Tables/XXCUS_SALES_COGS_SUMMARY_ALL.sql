
  CREATE TABLE "XXCUS"."XXCUS_SALES_COGS_SUMMARY_ALL" 
   (	"GL_LOCATION" VARCHAR2(10), 
	"AR_SALES_COGS_SUMMARY" VARCHAR2(400)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 0 INITRANS 1 MAXTRANS 255 NOCOMPRESS NOLOGGING
  STORAGE(INITIAL 2097152 NEXT 2097152 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "XXCUS_DATA" ;
