CREATE TABLE XXCUS.XXCUSGL_PR_ACCRUAL_TBL
(
  NAME                      VARCHAR2(100 BYTE),
  EFFECTIVE_DATE            DATE,
  JE_HEADER_ID              NUMBER(15),
  JE_LINE_NUM               NUMBER(15),
  ACCOUNTED_DR              NUMBER,
  ACCOUNTED_CR              NUMBER,
  ACCRUAL_DR                NUMBER,
  ACCRUAL_CR                NUMBER,
  TAX_DR                    NUMBER,
  TAX_CR                    NUMBER,
  SEGMENT1                  VARCHAR2(25 BYTE),
  SEGMENT2                  VARCHAR2(25 BYTE),
  SEGMENT3                  VARCHAR2(25 BYTE),
  SEGMENT4                  VARCHAR2(25 BYTE),
  SEGMENT5                  VARCHAR2(25 BYTE),
  SEGMENT6                  VARCHAR2(25 BYTE),
  SEGMENT7                  VARCHAR2(25 BYTE),
  DESCRIPTION               VARCHAR2(250 BYTE),
  CURRENCY_CONVERSION_TYPE  VARCHAR2(30 BYTE),
  CURRENCY_CODE             VARCHAR2(3 BYTE),
  LEDGER_ID                 NUMBER
)
TABLESPACE XXCUS_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


