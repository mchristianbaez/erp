CREATE TABLE XXCUS.TAXCNTYTAX_14OCT
(
  STCODE         NUMBER(2),
  CNTYCODE       VARCHAR2(3 BYTE),
  CNTYNAME       VARCHAR2(26 BYTE)              NOT NULL,
  CURDATE        DATE                           NOT NULL,
  CURSALERATE    NUMBER(5,5)                    NOT NULL,
  CURUSERATE     NUMBER(5,5)                    NOT NULL,
  CURSPECRATE    NUMBER(5,5)                    NOT NULL,
  PRIORDATE      DATE                           NOT NULL,
  PRIORSALERATE  NUMBER(5,5)                    NOT NULL,
  PRIORUSERATE   NUMBER(5,5)                    NOT NULL,
  PRIORSPECRATE  NUMBER(5,5)                    NOT NULL,
  ADMNCODE       VARCHAR2(1 BYTE),
  TAXCODE        VARCHAR2(10 BYTE),
  EXCPCODE       VARCHAR2(1 BYTE)
)
TABLESPACE XXCUS_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


