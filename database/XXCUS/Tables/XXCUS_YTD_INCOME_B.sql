DROP TABLE XXCUS.XXCUS_YTD_INCOME_B CASCADE CONSTRAINTS;
--
CREATE TABLE XXCUS.XXCUS_YTD_INCOME_B
(
  FISCAL_PERIOD            VARCHAR2(15 BYTE),
  MVID                     VARCHAR2(30 BYTE),
  OFU_CUST_ACCOUNT_ID      NUMBER,
  VENDOR_NAME              VARCHAR2(150 BYTE),
  LOB                      VARCHAR2(150 BYTE),
  LOB_ID                   NUMBER,
  BU                       VARCHAR2(150 BYTE),
  BRANCH                   VARCHAR2(150 BYTE),
  CUST_ACCT_BRANCH_NAME    VARCHAR2(30 BYTE),
  POSTED_FIN_LOCATION      VARCHAR2(10 BYTE),
  POSTED_BU_BRANCH         VARCHAR2(10 BYTE),
  OFU_ATTRIBUTE10          VARCHAR2(150 BYTE),
  UTILIZATION_TYPE         VARCHAR2(30 BYTE),
  ADJUSTMENT_TYPE          VARCHAR2(30 BYTE),
  ADJUSTMENT_TYPE_ID       NUMBER,
  CURRENCY_CODE            VARCHAR2(30 BYTE),
  LEDGER_NAME              VARCHAR2(30 BYTE),
  PLAN_ID                  NUMBER,
  OFFER_NAME               VARCHAR2(240 BYTE),
  STATUS                   VARCHAR2(30 BYTE),
  REBATE_TYPE              VARCHAR2(6 BYTE),
  AGREEMENT_YEAR           NUMBER,
  CALENDAR_YEAR            NUMBER,
  FISCAL_YEAR              NUMBER,
  FISCAL_MONTH_ID          NUMBER,
  PERIOD_ID                NUMBER,
  CALENDAR_MONTH_ID        NUMBER,
  PURCHASES                NUMBER,
  ACCRUAL_AMOUNT           NUMBER,
  CREATION_DATE            DATE,
  CREATED_BY               NUMBER,
  ORG_ID                   NUMBER,
  PROCESS_STATUS_CODE      VARCHAR2(1 BYTE),
  EVENT_STATUS_CODE        VARCHAR2(1 BYTE),
  GL_TRANSFER_STATUS_CODE  VARCHAR2(1 BYTE)
)
TABLESPACE XXCUS_DATA
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;
--
COMMENT ON TABLE XXCUS.XXCUS_YTD_INCOME_B IS 'ESMS 283147. Added four fields to the routine extract_ytd_income.';
--
CREATE INDEX XXCUS.XXCUS_YTD_INCOME_B_N1 ON XXCUS.XXCUS_YTD_INCOME_B
(MVID)
LOGGING
TABLESPACE XXCUS_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
NOPARALLEL;
--
CREATE INDEX XXCUS.XXCUS_YTD_INCOME_B_N10 ON XXCUS.XXCUS_YTD_INCOME_B
(PLAN_ID)
LOGGING
TABLESPACE XXCUS_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
NOPARALLEL;
--
CREATE INDEX XXCUS.XXCUS_YTD_INCOME_B_N11 ON XXCUS.XXCUS_YTD_INCOME_B
(OFFER_NAME)
LOGGING
TABLESPACE XXCUS_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
NOPARALLEL;
--
CREATE INDEX XXCUS.XXCUS_YTD_INCOME_B_N12 ON XXCUS.XXCUS_YTD_INCOME_B
(AGREEMENT_YEAR, '4712')
LOGGING
TABLESPACE XXCUS_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
NOPARALLEL;
--
CREATE INDEX XXCUS.XXCUS_YTD_INCOME_B_N13 ON XXCUS.XXCUS_YTD_INCOME_B
(CALENDAR_YEAR, CALENDAR_MONTH_ID)
LOGGING
TABLESPACE XXCUS_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
NOPARALLEL;
--
CREATE INDEX XXCUS.XXCUS_YTD_INCOME_B_N14 ON XXCUS.XXCUS_YTD_INCOME_B
(FISCAL_YEAR, FISCAL_MONTH_ID)
LOGGING
TABLESPACE XXCUS_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
NOPARALLEL;
--
CREATE INDEX XXCUS.XXCUS_YTD_INCOME_B_N15 ON XXCUS.XXCUS_YTD_INCOME_B
(FISCAL_PERIOD)
LOGGING
TABLESPACE XXCUS_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
NOPARALLEL;
--
CREATE INDEX XXCUS.XXCUS_YTD_INCOME_B_N16 ON XXCUS.XXCUS_YTD_INCOME_B
(POSTED_FIN_LOCATION)
LOGGING
TABLESPACE XXCUS_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
NOPARALLEL;
--
CREATE INDEX XXCUS.XXCUS_YTD_INCOME_B_N17 ON XXCUS.XXCUS_YTD_INCOME_B
(PERIOD_ID)
LOGGING
TABLESPACE XXCUS_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
NOPARALLEL;
--
CREATE INDEX XXCUS.XXCUS_YTD_INCOME_B_N2 ON XXCUS.XXCUS_YTD_INCOME_B
(OFU_CUST_ACCOUNT_ID)
LOGGING
TABLESPACE XXCUS_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
NOPARALLEL;
--
CREATE INDEX XXCUS.XXCUS_YTD_INCOME_B_N3 ON XXCUS.XXCUS_YTD_INCOME_B
(VENDOR_NAME)
LOGGING
TABLESPACE XXCUS_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
NOPARALLEL;
--
CREATE INDEX XXCUS.XXCUS_YTD_INCOME_B_N4 ON XXCUS.XXCUS_YTD_INCOME_B
(LOB)
LOGGING
TABLESPACE XXCUS_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
NOPARALLEL;
--
CREATE INDEX XXCUS.XXCUS_YTD_INCOME_B_N5 ON XXCUS.XXCUS_YTD_INCOME_B
(LOB_ID)
LOGGING
TABLESPACE XXCUS_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
NOPARALLEL;
--
CREATE INDEX XXCUS.XXCUS_YTD_INCOME_B_N6 ON XXCUS.XXCUS_YTD_INCOME_B
(BU, '1')
LOGGING
TABLESPACE XXCUS_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
NOPARALLEL;
--
CREATE INDEX XXCUS.XXCUS_YTD_INCOME_B_N7 ON XXCUS.XXCUS_YTD_INCOME_B
(BRANCH, '1')
LOGGING
TABLESPACE XXCUS_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
NOPARALLEL;
--
CREATE INDEX XXCUS.XXCUS_YTD_INCOME_B_N8 ON XXCUS.XXCUS_YTD_INCOME_B
(OFU_ATTRIBUTE10, '1')
LOGGING
TABLESPACE XXCUS_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
NOPARALLEL;
--
CREATE INDEX XXCUS.XXCUS_YTD_INCOME_B_N9 ON XXCUS.XXCUS_YTD_INCOME_B
(UTILIZATION_TYPE)
LOGGING
TABLESPACE XXCUS_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
NOPARALLEL;
--
CREATE INDEX XXCUS.XXCUS_YTD_INCOME_B_N18 ON XXCUS.XXCUS_YTD_INCOME_B
(ORG_ID)
TABLESPACE XXCUS_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
NOPARALLEL;
--
CREATE INDEX XXCUS.XXCUS_YTD_INCOME_B_N19 ON XXCUS.XXCUS_YTD_INCOME_B
(PROCESS_STATUS_CODE)
TABLESPACE XXCUS_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
NOPARALLEL;
--
CREATE INDEX XXCUS.XXCUS_YTD_INCOME_B_N20 ON XXCUS.XXCUS_YTD_INCOME_B
(EVENT_STATUS_CODE)
TABLESPACE XXCUS_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
NOPARALLEL;
--
CREATE INDEX XXCUS.XXCUS_YTD_INCOME_B_N21 ON XXCUS.XXCUS_YTD_INCOME_B
(GL_TRANSFER_STATUS_CODE)
TABLESPACE XXCUS_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
NOPARALLEL;