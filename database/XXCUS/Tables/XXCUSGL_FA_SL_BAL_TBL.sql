CREATE TABLE XXCUS.XXCUSGL_FA_SL_BAL_TBL
(
  SEGMENT1       VARCHAR2(2 BYTE),
  SEGMENT2       VARCHAR2(5 BYTE),
  SEGMENT3       VARCHAR2(4 BYTE),
  SEGMENT4       VARCHAR2(6 BYTE),
  SEGMENT5       VARCHAR2(5 BYTE),
  ACCT_DESC      VARCHAR2(50 BYTE),
  AMOUNT         NUMBER,
  CURRENCY_CODE  VARCHAR2(3 BYTE)
)
TABLESPACE XXCUS_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


