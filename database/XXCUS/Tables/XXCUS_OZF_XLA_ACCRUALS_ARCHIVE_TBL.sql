/*
  @GSC Incident 615950 - Rebates archive table to store fiscal period accruals data
  WC TMS: 20160316-00196 
  Date: 04/15/2016
*/
DROP TABLE XXCUS.XXCUS_OZF_XLA_ACCRUALS_ARCHIVE;
CREATE TABLE XXCUS.XXCUS_OZF_XLA_ACCRUALS_ARCHIVE
(
  XLA_ACCRUAL_ID                  NUMBER,
  OFU_REQUEST_ID                  NUMBER,
  UTILIZATION_TYPE                VARCHAR2(30 BYTE),
  ADJUSTMENT_TYPE                 VARCHAR2(30 BYTE),
  ADJUSTMENT_TYPE_ID              NUMBER,
  OBJECT_ID                       NUMBER,
  REBATE_AMOUNT                   NUMBER,
  UTIL_ACCTD_AMOUNT               NUMBER,
  EVENT_TYPE_CODE                 VARCHAR2(30 BYTE),
  ENTITY_CODE                     VARCHAR2(30 BYTE),
  EVENT_ID                        NUMBER,
  UTILIZATION_ID                  NUMBER,
  OXA_ORG_ID                      NUMBER,
  OFU_GL_DATE                     DATE,
  OZF_LEDGER_ID                   VARCHAR2(150 BYTE),
  OZF_LEDGER_NAME                 VARCHAR2(30 BYTE),
  XLAE_ENTITY_ID                  NUMBER(15),
  XLAE_TRXN_DATE                  DATE,
  XLAE_EVENT_DATE                 DATE,
  XLAE_EVENT_STATUS_CODE          VARCHAR2(1 BYTE),
  XLAE_PROCESS_STATUS_CODE        VARCHAR2(1 BYTE),
  XLAE_EVENT_NUMBER               NUMBER(38),
  XLAE_ONHOLD_FLAG                VARCHAR2(1 BYTE),
  XLA_APPLICATION_ID              NUMBER(15),
  XAEH_AE_HEADER_ID               NUMBER(15),
  XAEH_ACCOUNTING_DATE            DATE,
  XAEH_GL_XFER_STATUS_CODE        VARCHAR2(30 BYTE),
  XAEH_GL_XFER_DATE               DATE,
  XAEH_JE_CATEGORY                VARCHAR2(30 BYTE),
  FISCAL_MONTH                    VARCHAR2(3 BYTE),
  XAEH_PERIOD_NAME                VARCHAR2(15 BYTE),
  ACCOUNTING_ERROR_ID             NUMBER(15),
  ACCOUNTING_BATCH_ID             NUMBER(15),
  ENCODED_MSG                     VARCHAR2(2000 BYTE),
  ERR_AE_LINE_NUM                 NUMBER(15),
  MESSAGE_NUMBER                  NUMBER(9),
  ERROR_SOURCE_CODE               VARCHAR2(30 BYTE),
  OFFER_ID                        NUMBER,
  OFFER_CODE                      VARCHAR2(30 BYTE),
  OFFER_NAME                      VARCHAR2(2000 BYTE),
  OFFER_TYPE                      VARCHAR2(30 BYTE),
  AUTOPAY_PARTY_ID                NUMBER,
  BENEFICIARY_ACCOUNT_ID          NUMBER,
  OFFER_STATUS_CODE               VARCHAR2(30 BYTE),
  OFFER_CUSTOM_SETUP_ID           NUMBER,
  OFFER_USER_STATUS_ID            NUMBER,
  OFFER_OWNER_ID                  NUMBER,
  ACTIVITY_MEDIA_ID               NUMBER,
  ACTIVITY_MEDIA_NAME             VARCHAR2(120 BYTE),
  OFFER_AUTOPAY_FLAG              VARCHAR2(1 BYTE),
  OFU_CURRENCY_CODE               VARCHAR2(30 BYTE),
  RECEIPTS_CURRENCY               VARCHAR2(30 BYTE),
  AUTO_RENEWAL                    VARCHAR2(240 BYTE),
  CALENDAR_YEAR                   VARCHAR2(240 BYTE),
  UNTIL_YEAR                      VARCHAR2(240 BYTE),
  PAYMENT_METHOD                  VARCHAR2(240 BYTE),
  PAYMENT_FREQUENCY               VARCHAR2(240 BYTE),
  GUARANTEED_AMOUNT               VARCHAR2(240 BYTE),
  QP_LIST_HEADER_ID               NUMBER,
  QP_GLOBAL_FLAG                  VARCHAR2(1 BYTE),
  QP_ACTIVE_FLAG                  VARCHAR2(1 BYTE),
  QP_ORIG_ORG_ID                  NUMBER,
  OFU_YEAR_ID                     NUMBER,
  FUND_ID                         NUMBER,
  FUND_NAME                       VARCHAR2(240 BYTE),
  PLAN_TYPE                       VARCHAR2(30 BYTE),
  COMPONENT_ID                    NUMBER,
  COMPONENT_TYPE                  VARCHAR2(30 BYTE),
  OBJECT_TYPE                     VARCHAR2(30 BYTE),
  REFERENCE_TYPE                  VARCHAR2(30 BYTE),
  REFERENCE_ID                    NUMBER,
  AMS_ACTIVITY_BUDGET_ID          NUMBER,
  PRODUCT_LEVEL_TYPE              VARCHAR2(30 BYTE),
  PRODUCT_ID                      NUMBER,
  RESALE_DATE_CREATED             DATE,
  DATE_INVOICED                   DATE,
  PO_NUMBER                       VARCHAR2(50 BYTE),
  PURCHASE_UOM_CODE               VARCHAR2(3 BYTE),
  UOM_CODE                        VARCHAR2(3 BYTE),
  PURCHASE_PRICE                  NUMBER,
  OFU_CUST_ACCOUNT_ID             NUMBER,
  OFU_MVID_NAME                   VARCHAR2(150 BYTE),
  OFU_MVID                        VARCHAR2(150 BYTE),
  BILLTO_CUST_ACCT_ID_BR          NUMBER,
  BILLTO_CUST_ACCT_BR_NAME        VARCHAR2(150 BYTE),
  BILLTO_CUST_ACCT_BR_ACCT_NAME   VARCHAR2(30 BYTE),
  BILL_TO_PARTY_ID                NUMBER,
  BILL_TO_PARTY_NAME_LOB          VARCHAR2(360 BYTE),
  DATE_ORDERED                    DATE,
  SELLING_PRICE                   NUMBER,
  QUANTITY                        NUMBER,
  INVENTORY_ITEM_ID               NUMBER,
  ITEM_NUMBER                     VARCHAR2(150 BYTE),
  RESALE_LINE_ATTRIBUTE1          VARCHAR2(150 BYTE),
  RESALE_LINE_ATTRIBUTE2          VARCHAR2(150 BYTE),
  RESALE_LINE_ATTRIBUTE3          VARCHAR2(150 BYTE),
  RESALE_LINE_ATTRIBUTE5          VARCHAR2(150 BYTE),
  RESALE_LINE_ATTRIBUTE6          VARCHAR2(150 BYTE),
  RESALE_LINE_ATTRIBUTE11         VARCHAR2(150 BYTE),
  RESALE_LINE_ATTRIBUTE12         VARCHAR2(150 BYTE),
  RESALE_LINE_ATTRIBUTE13         VARCHAR2(150 BYTE),
  RESALE_LINE_ATTRIBUTE14         VARCHAR2(150 BYTE),
  PRICING_ADJ_ID                  NUMBER,
  OFU_EXCHANGE_RATE_DATE          DATE,
  OFU_EXCHANGE_RATE               NUMBER,
  OFU_BILLTO_CUST_ACCOUNT_ID_BR   NUMBER,
  OFU_PRODUCT_ID                  NUMBER,
  SOLD_FROM_CUST_ACCOUNT_ID_MVID  NUMBER,
  SOLD_FROM_SITE_ID_MVID          NUMBER,
  SOLD_FROM_PARTY_NAME_MVID       VARCHAR2(150 BYTE),
  POSTED_FIN_LOC                  VARCHAR2(10 BYTE),
  COOP_YES_NO                     VARCHAR2(1 BYTE),
  POSTED_BU_BRANCH                VARCHAR2(10 BYTE),
  OVERRIDE_FIN_LOC                VARCHAR2(1 BYTE),
  OFU_ATTRIBUTE10                 VARCHAR2(150 BYTE),
  OFU_CREATION_DATE               DATE
)
NOCOMPRESS 
TABLESPACE XXCUS_DATA
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
NOLOGGING
PARTITION BY LIST (XAEH_PERIOD_NAME)
(  
--- Begin 2012 partitions
  PARTITION JAN2012 VALUES ('Jan-2012')
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION FEB2012 VALUES ('Feb-2012')
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION MAR2012 VALUES ('Mar-2012')
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION APR2012 VALUES ('Apr-2012')
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION MAY2012 VALUES ('May-2012')
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION JUN2012 VALUES ('Jun-2012')
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION JUL2012 VALUES ('Jul-2012')
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION AUG2012 VALUES ('Aug-2012')
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION SEP2012 VALUES ('Sep-2012')
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION OCT2012 VALUES ('Oct-2012')
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION NOV2012 VALUES ('Nov-2012')
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION DEC2012 VALUES ('Dec-2012')
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),
--  End of 2012 partitions   
-- Begin 2013 partitions
  PARTITION JAN2013 VALUES ('Jan-2013')
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION FEB2013 VALUES ('Feb-2013')
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION MAR2013 VALUES ('Mar-2013')
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION APR2013 VALUES ('Apr-2013')
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION MAY2013 VALUES ('May-2013')
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION JUN2013 VALUES ('Jun-2013')
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION JUL2013 VALUES ('Jul-2013')
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION AUG2013 VALUES ('Aug-2013')
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION SEP2013 VALUES ('Sep-2013')
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION OCT2013 VALUES ('Oct-2013')
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION NOV2013 VALUES ('Nov-2013')
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION DEC2013 VALUES ('Dec-2013')
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),
--  End of 2013 partitions
--- Begin 2014 partitions
  PARTITION JAN2014 VALUES ('Jan-2014')
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION FEB2014 VALUES ('Feb-2014')
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION MAR2014 VALUES ('Mar-2014')
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION APR2014 VALUES ('Apr-2014')
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION MAY2014 VALUES ('May-2014')
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION JUN2014 VALUES ('Jun-2014')
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION JUL2014 VALUES ('Jul-2014')
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION AUG2014 VALUES ('Aug-2014')
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION SEP2014 VALUES ('Sep-2014')
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION OCT2014 VALUES ('Oct-2014')
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION NOV2014 VALUES ('Nov-2014')
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION DEC2014 VALUES ('Dec-2014')
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),
--  End of 2014 partitions                    
  PARTITION NA VALUES (DEFAULT)
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               )           
)
NOCACHE
NOPARALLEL
MONITORING;