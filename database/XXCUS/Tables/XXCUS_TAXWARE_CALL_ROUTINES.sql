CREATE TABLE XXCUS.XXCUS_TAXWARE_CALL_ROUTINES
(
  REQUEST_ID        NUMBER,
  EXECUTION_METHOD  VARCHAR2(40 BYTE),
  FILE_PATH         VARCHAR2(150 BYTE),
  FILE_NAME         VARCHAR2(150 BYTE),
  EXECUTION_STATUS  VARCHAR2(40 BYTE),
  CREATED_BY        NUMBER,
  CREATION_DATE     DATE
)
TABLESPACE XXCUS_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

COMMENT ON TABLE XXCUS.XXCUS_TAXWARE_CALL_ROUTINES IS 'Store the sql loader and plsql executions performed for the different set of files for history.';



