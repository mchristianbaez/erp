CREATE TABLE XXCUS.XXHDS_MARKETSEER_MVW
(
  PRP_ID                 VARCHAR2(90 BYTE),
  SBU_PRP                VARCHAR2(121 BYTE),
  LAT                    VARCHAR2(150 BYTE),
  LONGITUDE              VARCHAR2(150 BYTE),
  LOB                    VARCHAR2(30 BYTE),
  SBU                    VARCHAR2(30 BYTE),
  PRIME_OPS_TYPE_1       VARCHAR2(240 BYTE),
  PRIME_OPS_TYPE_2       VARCHAR2(80 BYTE),
  LEGACY                 VARCHAR2(150 BYTE),
  FRU                    VARCHAR2(150 BYTE),
  LOC_ID                 VARCHAR2(90 BYTE),
  ADDRESS                VARCHAR2(481 BYTE),
  CITY                   VARCHAR2(60 BYTE),
  ST_PROV                VARCHAR2(60 BYTE),
  POSTAL_CODE            VARCHAR2(60 BYTE),
  PRIME_LEASE_RER        VARCHAR2(50 BYTE),
  RER_TYPE               VARCHAR2(100 BYTE),
  EXP_DATE               DATE,
  REM                    VARCHAR2(240 BYTE),
  PRP_TOTAL_SFT          NUMBER,
  PRP_VAC_SFT            NUMBER,
  LND_TOTAL_ACRE         NUMBER,
  ORACLE_IDS             VARCHAR2(150 BYTE),
  FRU_DESCRIPTION        VARCHAR2(250 BYTE),
  CBSA_NUMBER            VARCHAR2(50 BYTE),
  METROAREA              VARCHAR2(255 BYTE),
  EVP                    VARCHAR2(450 BYTE),
  LOCATION_TYPE_1        VARCHAR2(30 BYTE),
  ONE_TEAM_SELLING_AREA  VARCHAR2(50 BYTE)
)
TABLESPACE XXCUS_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


