CREATE TABLE XXCUS.XXCUSIE_CTI_AUD_TBL
(
  JE_HEADER_ID        NUMBER(15)                NOT NULL,
  JE_LINE_NUM         NUMBER(15)                NOT NULL,
  PERIOD_NAME         VARCHAR2(15 BYTE),
  CONC_REQUEST_ID     NUMBER,
  PROCESSED_DATETIME  DATE,
  USER_NAME           VARCHAR2(200 BYTE),
  RESP_NAME           VARCHAR2(200 BYTE),
  STAGE               VARCHAR2(200 BYTE),
  STG_DATE            DATE,
  RUN_NUM             NUMBER,
  SEQUENCE            NUMBER
)
TABLESPACE XXCUS_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


