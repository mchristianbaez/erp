CREATE TABLE XXCUS.XXCUS_ERRORS_EMAIL
(
  EMAIL_GROUP   VARCHAR2(240 BYTE)              NOT NULL,
  EMAIL_MODULE  VARCHAR2(10 BYTE)               NOT NULL,
  EMAIL_TIMER   NUMBER                          NOT NULL
)
TABLESPACE XXCUS_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          1M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


