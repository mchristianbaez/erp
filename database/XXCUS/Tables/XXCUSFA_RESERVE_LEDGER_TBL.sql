/***************************************************************************
-- ************************************************************************
-- $Header XXCUSFA_RESERVE_LEDGER_TBL $
-- Module Name: XXCUS 

-- REVISIONS:
-- Ver        Date        Author             Description
-- ---------  ----------  ----------         ----------------   
-- 1.0       01/7/2016    prod version       initial creation    
-- 1.1       01/7/2016    Neha Saini          change for TMS#20151015-00139 Added column original cost.                             
-- **************************************************************************/
DROP TABLE XXCUS.XXCUSFA_RESERVE_LEDGER_TBL CASCADE CONSTRAINTS;

CREATE TABLE XXCUS.XXCUSFA_RESERVE_LEDGER_TBL
(
  ASSET_ID                NUMBER(15),
  DH_CCID                 NUMBER(15),
  DATE_PLACED_IN_SERVICE  DATE,
  METHOD_CODE             VARCHAR2(36 BYTE),
  LIFE                    NUMBER(6),
  RATE                    NUMBER,
  CAPACITY                NUMBER,
  COST                    NUMBER,
  DEPRN_AMOUNT            NUMBER,
  YTD_DEPRN               NUMBER,
  PERCENT                 NUMBER,
  TRANSACTION_TYPE        VARCHAR2(3 BYTE),
  DEPRN_RESERVE           NUMBER,
  PERIOD_COUNTER          NUMBER(15),
  DATE_EFFECTIVE          DATE,
  DEPRN_RESERVE_ACCT      VARCHAR2(75 BYTE),
  RESERVE_ACCT            VARCHAR2(75 BYTE),
  DISTRIBUTION_ID         NUMBER(15),
  UNITS_ASSIGNED          NUMBER,
  LOCATION_ID             NUMBER,
  REMAINING_LIFE1         NUMBER,
  ADJUSTMENT_AMOUNT       NUMBER,
  ORIGINAL_COST           NUMBER,--TMS#20151015-00139 addded  ver 1.1
  ORIGINAL_UNITS          NUMBER --TMS#20151015-00139 addded  ver 1.1
)
TABLESPACE XXCUS_DATA
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MAXSIZE          UNLIMITED
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

GRANT SELECT ON XXCUS.XXCUSFA_RESERVE_LEDGER_TBL TO XXCUS_EIS_ROLE;
