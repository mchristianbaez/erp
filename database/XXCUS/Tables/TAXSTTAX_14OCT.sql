CREATE TABLE XXCUS.TAXSTTAX_14OCT
(
  STCODE         NUMBER(2),
  STNAME         VARCHAR2(26 BYTE)              NOT NULL,
  CNTYTAXIND     VARCHAR2(1 BYTE),
  LOCLTAXIND     VARCHAR2(1 BYTE),
  CNTYTRANIND    VARCHAR2(1 BYTE),
  LOCLTRANIND    VARCHAR2(1 BYTE),
  CURDATE        DATE                           NOT NULL,
  CURSALERATE    NUMBER(5,5)                    NOT NULL,
  CURUSERATE     NUMBER(5,5)                    NOT NULL,
  CURSPECRATE    NUMBER(5,5)                    NOT NULL,
  PRIORDATE      DATE,
  PRIORSALERATE  NUMBER(5,5)                    NOT NULL,
  PRIORUSERATE   NUMBER(5,5)                    NOT NULL,
  PRIORSPECRATE  NUMBER(5,5)                    NOT NULL,
  MAXTAX         NUMBER(5,5),
  JURCODE        VARCHAR2(1 BYTE),
  ADMNCODE       VARCHAR2(1 BYTE)
)
TABLESPACE XXCUS_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


