
  CREATE TABLE "XXCUS"."XXCUS_OPN_LEASE_TAB_NOTES" 
   (	"LEASE_NAME" VARCHAR2(50), 
	"LEASE_NUMBER" VARCHAR2(30), 
	"PRIMARY_LOCATION" VARCHAR2(90), 
	"NOTE_TYPE" VARCHAR2(80), 
	"NOTE_DATE" DATE, 
	"UPDATED_BY_USER" VARCHAR2(100), 
	"DESCRIPTION" VARCHAR2(4000), 
	"NOTE_HEADER_ID" NUMBER, 
	"NOTE_DETAIL_ID" NUMBER, 
	"LEASE_ID" NUMBER, 
	"PRIMARY_LOC_ID" NUMBER, 
	"EXTRACT_DATE" DATE, 
	"CREATED_BY" NUMBER
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "XXCUS_DATA" ;
