CREATE TABLE XXCUS.TAXLOCLTAX_14OCT
(
  STCODE         NUMBER(2),
  ZIPCODE        VARCHAR2(5 BYTE),
  GEOCODE        VARCHAR2(2 BYTE),
  CITYNAME       VARCHAR2(26 BYTE)              NOT NULL,
  CNTYCODE       VARCHAR2(3 BYTE)               NOT NULL,
  DUPL           VARCHAR2(1 BYTE),
  CURDATE        DATE,
  CURSALERATE    NUMBER(5,5),
  CURUSERATE     NUMBER(5,5),
  CURSPECRATE    NUMBER(5,5),
  PRIORDATE      DATE,
  PRIORSALERATE  NUMBER(5,5)                    NOT NULL,
  PRIORUSERATE   NUMBER(5,5)                    NOT NULL,
  PRIORSPECRATE  NUMBER(5,5)                    NOT NULL,
  CNTYTAXIND     NUMBER,
  ZIPEXTFIRST    VARCHAR2(4 BYTE),
  ZIPEXTLAST     VARCHAR2(4 BYTE),
  ADMNCODE       VARCHAR2(1 BYTE),
  TAXCODE        VARCHAR2(10 BYTE),
  EXCPCODE       VARCHAR2(1 BYTE)
)
TABLESPACE XXCUS_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


