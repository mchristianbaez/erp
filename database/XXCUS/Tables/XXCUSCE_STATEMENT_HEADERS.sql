CREATE TABLE XXCUS.XXCUSCE_STATEMENT_HEADERS
(
  STATEMENT_HEADER_ID        NUMBER(15)         NOT NULL,
  BANK_ACCOUNT_ID            NUMBER(15)         NOT NULL,
  STATEMENT_NUMBER           VARCHAR2(50 BYTE)  NOT NULL,
  STATEMENT_DATE             DATE               NOT NULL,
  CREATED_BY                 NUMBER(15)         NOT NULL,
  CREATION_DATE              DATE               NOT NULL,
  LAST_UPDATED_BY            NUMBER(15)         NOT NULL,
  LAST_UPDATE_DATE           DATE               NOT NULL,
  AUTO_LOADED_FLAG           VARCHAR2(1 BYTE)   NOT NULL,
  GL_DATE                    DATE               NOT NULL,
  CONTROL_BEGIN_BALANCE      NUMBER,
  CONTROL_TOTAL_DR           NUMBER,
  CONTROL_TOTAL_CR           NUMBER,
  CONTROL_END_BALANCE        NUMBER,
  CONTROL_DR_LINE_COUNT      NUMBER,
  CONTROL_CR_LINE_COUNT      NUMBER,
  CURRENCY_CODE              VARCHAR2(15 BYTE),
  LAST_UPDATE_LOGIN          NUMBER(15),
  ATTRIBUTE_CATEGORY         VARCHAR2(30 BYTE),
  ATTRIBUTE1                 VARCHAR2(150 BYTE),
  ATTRIBUTE2                 VARCHAR2(150 BYTE),
  ATTRIBUTE3                 VARCHAR2(150 BYTE),
  ATTRIBUTE4                 VARCHAR2(150 BYTE),
  ATTRIBUTE5                 VARCHAR2(150 BYTE),
  ATTRIBUTE6                 VARCHAR2(150 BYTE),
  ATTRIBUTE7                 VARCHAR2(150 BYTE),
  ATTRIBUTE8                 VARCHAR2(150 BYTE),
  ATTRIBUTE9                 VARCHAR2(150 BYTE),
  ATTRIBUTE10                VARCHAR2(150 BYTE),
  ATTRIBUTE11                VARCHAR2(150 BYTE),
  ATTRIBUTE12                VARCHAR2(150 BYTE),
  ATTRIBUTE13                VARCHAR2(150 BYTE),
  ATTRIBUTE14                VARCHAR2(150 BYTE),
  ATTRIBUTE15                VARCHAR2(150 BYTE),
  STATEMENT_COMPLETE_FLAG    VARCHAR2(1 BYTE),
  ORG_ID                     NUMBER(15)         DEFAULT TO_NUMBER(DECODE(SUBSTRB(USERENV('CLIENT_INFO'),1,1),' ',NULL,SUBSTRB(USERENV('CLIENT_INFO'),1,10))),
  DOC_SEQUENCE_ID            NUMBER,
  DOC_SEQUENCE_VALUE         NUMBER,
  CHECK_DIGITS               VARCHAR2(30 BYTE),
  CASHFLOW_BALANCE           NUMBER,
  INT_CALC_BALANCE           NUMBER,
  ONE_DAY_FLOAT              NUMBER,
  TWO_DAY_FLOAT              NUMBER,
  GLOBAL_ATTRIBUTE_CATEGORY  VARCHAR2(150 BYTE),
  GLOBAL_ATTRIBUTE1          VARCHAR2(150 BYTE),
  GLOBAL_ATTRIBUTE2          VARCHAR2(150 BYTE),
  GLOBAL_ATTRIBUTE3          VARCHAR2(150 BYTE),
  GLOBAL_ATTRIBUTE4          VARCHAR2(150 BYTE),
  GLOBAL_ATTRIBUTE5          VARCHAR2(150 BYTE),
  GLOBAL_ATTRIBUTE6          VARCHAR2(150 BYTE),
  GLOBAL_ATTRIBUTE7          VARCHAR2(150 BYTE),
  GLOBAL_ATTRIBUTE8          VARCHAR2(150 BYTE),
  GLOBAL_ATTRIBUTE9          VARCHAR2(150 BYTE),
  GLOBAL_ATTRIBUTE10         VARCHAR2(150 BYTE),
  GLOBAL_ATTRIBUTE11         VARCHAR2(150 BYTE),
  GLOBAL_ATTRIBUTE12         VARCHAR2(150 BYTE),
  GLOBAL_ATTRIBUTE13         VARCHAR2(150 BYTE),
  GLOBAL_ATTRIBUTE14         VARCHAR2(150 BYTE),
  GLOBAL_ATTRIBUTE15         VARCHAR2(150 BYTE),
  GLOBAL_ATTRIBUTE16         VARCHAR2(150 BYTE),
  GLOBAL_ATTRIBUTE17         VARCHAR2(150 BYTE),
  GLOBAL_ATTRIBUTE18         VARCHAR2(150 BYTE),
  GLOBAL_ATTRIBUTE19         VARCHAR2(150 BYTE),
  GLOBAL_ATTRIBUTE20         VARCHAR2(150 BYTE)
)
TABLESPACE XXCUS_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          1M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


