 /*
   Ticket#                            Date         Author            Notes
   -------------------------------------------------------------------------------------------------------------------------------------------------------------------
   TMS:20170524-00141 / ESMS 561767   05/28/2017   Balaguru Seshadri Concur accrual process related tables  
 */
--
CREATE TABLE XXCUS.XXCUS_CONCUR_ACCR_REQ_SET_JOBS
(
  REPORT_SET_REQUEST_ID  NUMBER                 DEFAULT 0,
  WRAPPER_REQ_ID         NUMBER                 DEFAULT 0
)
;
--
COMMENT ON TABLE XXCUS.XXCUS_CONCUR_ACCR_REQ_SET_JOBS IS  'TMS:20170524-00141 / ESMS 561767 ';
