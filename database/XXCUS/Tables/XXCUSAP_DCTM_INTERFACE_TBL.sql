CREATE TABLE XXCUS.XXCUSAP_DCTM_INTERFACE_TBL
(
  GROUP_ID           VARCHAR2(80 BYTE)          NOT NULL,
  INVOICE_COUNT      NUMBER                     NOT NULL,
  INVOICE_AMT_TOTAL  NUMBER                     NOT NULL,
  LINE_COUNT         NUMBER                     NOT NULL,
  LINE_AMT_TOTAL     NUMBER                     NOT NULL,
  CREATION_DATE      DATE                       DEFAULT SYSDATE               NOT NULL,
  CREATED_BY         VARCHAR2(30 BYTE)          DEFAULT 'INTERFACE_ECM'       NOT NULL,
  UPDATE_DATE        DATE,
  UPDATED_BY         NUMBER(15),
  STATUS             VARCHAR2(15 BYTE)          DEFAULT 'NEW'                 NOT NULL,
  REQUEST_ID         NUMBER,
  ORG_ID             NUMBER
)
TABLESPACE XXCUS_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

COMMENT ON COLUMN XXCUS.XXCUSAP_DCTM_INTERFACE_TBL.GROUP_ID IS 'Unique Identifier for batch being delivered that allows payables to import the batch';

COMMENT ON COLUMN XXCUS.XXCUSAP_DCTM_INTERFACE_TBL.INVOICE_COUNT IS 'Total count of invoice headers';

COMMENT ON COLUMN XXCUS.XXCUSAP_DCTM_INTERFACE_TBL.INVOICE_AMT_TOTAL IS 'Total amount of invoice headers';

COMMENT ON COLUMN XXCUS.XXCUSAP_DCTM_INTERFACE_TBL.LINE_COUNT IS 'Total count of invoice lines';

COMMENT ON COLUMN XXCUS.XXCUSAP_DCTM_INTERFACE_TBL.LINE_AMT_TOTAL IS 'Total amount of invoice lines';

COMMENT ON COLUMN XXCUS.XXCUSAP_DCTM_INTERFACE_TBL.REQUEST_ID IS 'Concurrent Request ID used to import invoices with the group id';



