CREATE TABLE XXCUS.XLA_AE_HEADERS_BAK_7_28
(
  AE_HEADER_ID                    NUMBER(15)    NOT NULL,
  APPLICATION_ID                  NUMBER(15)    NOT NULL,
  LEDGER_ID                       NUMBER(15)    NOT NULL,
  ENTITY_ID                       NUMBER(15)    NOT NULL,
  EVENT_ID                        NUMBER(15)    NOT NULL,
  EVENT_TYPE_CODE                 VARCHAR2(30 BYTE) NOT NULL,
  ACCOUNTING_DATE                 DATE          NOT NULL,
  GL_TRANSFER_STATUS_CODE         VARCHAR2(30 BYTE) NOT NULL,
  GL_TRANSFER_DATE                DATE,
  JE_CATEGORY_NAME                VARCHAR2(30 BYTE) NOT NULL,
  ACCOUNTING_ENTRY_STATUS_CODE    VARCHAR2(30 BYTE) NOT NULL,
  ACCOUNTING_ENTRY_TYPE_CODE      VARCHAR2(30 BYTE) NOT NULL,
  AMB_CONTEXT_CODE                VARCHAR2(30 BYTE),
  PRODUCT_RULE_TYPE_CODE          VARCHAR2(1 BYTE),
  PRODUCT_RULE_CODE               VARCHAR2(30 BYTE),
  PRODUCT_RULE_VERSION            VARCHAR2(30 BYTE),
  DESCRIPTION                     VARCHAR2(1996 BYTE),
  DOC_SEQUENCE_ID                 NUMBER,
  DOC_SEQUENCE_VALUE              NUMBER,
  ACCOUNTING_BATCH_ID             NUMBER(15),
  COMPLETION_ACCT_SEQ_VERSION_ID  NUMBER(15),
  CLOSE_ACCT_SEQ_VERSION_ID       NUMBER(15),
  COMPLETION_ACCT_SEQ_VALUE       NUMBER,
  CLOSE_ACCT_SEQ_VALUE            NUMBER,
  BUDGET_VERSION_ID               NUMBER(15),
  FUNDS_STATUS_CODE               VARCHAR2(30 BYTE),
  ENCUMBRANCE_TYPE_ID             NUMBER(15),
  BALANCE_TYPE_CODE               VARCHAR2(1 BYTE) NOT NULL,
  REFERENCE_DATE                  DATE,
  COMPLETED_DATE                  DATE,
  PERIOD_NAME                     VARCHAR2(15 BYTE),
  PACKET_ID                       NUMBER(15),
  COMPLETION_ACCT_SEQ_ASSIGN_ID   NUMBER(15),
  CLOSE_ACCT_SEQ_ASSIGN_ID        NUMBER(15),
  DOC_CATEGORY_CODE               VARCHAR2(30 BYTE),
  ATTRIBUTE_CATEGORY              VARCHAR2(30 BYTE),
  ATTRIBUTE1                      VARCHAR2(150 BYTE),
  ATTRIBUTE2                      VARCHAR2(150 BYTE),
  ATTRIBUTE3                      VARCHAR2(150 BYTE),
  ATTRIBUTE4                      VARCHAR2(150 BYTE),
  ATTRIBUTE5                      VARCHAR2(150 BYTE),
  ATTRIBUTE6                      VARCHAR2(150 BYTE),
  ATTRIBUTE7                      VARCHAR2(150 BYTE),
  ATTRIBUTE8                      VARCHAR2(150 BYTE),
  ATTRIBUTE9                      VARCHAR2(150 BYTE),
  ATTRIBUTE10                     VARCHAR2(150 BYTE),
  ATTRIBUTE11                     VARCHAR2(150 BYTE),
  ATTRIBUTE12                     VARCHAR2(150 BYTE),
  ATTRIBUTE13                     VARCHAR2(150 BYTE),
  ATTRIBUTE14                     VARCHAR2(150 BYTE),
  ATTRIBUTE15                     VARCHAR2(150 BYTE),
  GROUP_ID                        NUMBER(15),
  DOC_SEQUENCE_VERSION_ID         NUMBER(15),
  DOC_SEQUENCE_ASSIGN_ID          NUMBER(15),
  CREATION_DATE                   DATE          NOT NULL,
  CREATED_BY                      NUMBER(15)    NOT NULL,
  LAST_UPDATE_DATE                DATE          NOT NULL,
  LAST_UPDATED_BY                 NUMBER(15)    NOT NULL,
  LAST_UPDATE_LOGIN               NUMBER(15),
  PROGRAM_UPDATE_DATE             DATE,
  PROGRAM_APPLICATION_ID          NUMBER(15),
  PROGRAM_ID                      NUMBER(15),
  REQUEST_ID                      NUMBER(15),
  UPG_BATCH_ID                    NUMBER(15),
  UPG_SOURCE_APPLICATION_ID       NUMBER(15),
  UPG_VALID_FLAG                  VARCHAR2(1 BYTE),
  ZERO_AMOUNT_FLAG                VARCHAR2(1 BYTE),
  PARENT_AE_HEADER_ID             NUMBER(15),
  PARENT_AE_LINE_NUM              NUMBER(15),
  ACCRUAL_REVERSAL_FLAG           VARCHAR2(1 BYTE),
  MERGE_EVENT_ID                  NUMBER(15),
  NEED_BAL_FLAG                   VARCHAR2(1 BYTE)
)
TABLESPACE XXCUS_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


