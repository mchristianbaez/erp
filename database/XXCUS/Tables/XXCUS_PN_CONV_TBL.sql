CREATE TABLE XXCUS.XXCUS_PN_CONV_TBL
(
  TABLE_NAME     VARCHAR2(50 BYTE)              NOT NULL,
  R11_COUNT      NUMBER(15),
  R12_COUNT      NUMBER(15),
  SEQUENCE       VARCHAR2(50 BYTE),
  SEQ_R11_NUM    NUMBER(15),
  SEQ_R12_NUM    NUMBER(15),
  CREATION_DATE  DATE
)
TABLESPACE XXCUS_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


