/*
   Ticket#                               Date         Author             Notes
   -------------------------------------------------------------------------------------------------------------------------------------------------------
   TMS 20170412-00106 / ESMS 554216      02/17/2017   Balaguru Seshadri  Concur process related tables
*/
--
DROP SEQUENCE XXCUS.XXCUS_CONCUR_TIEOUT_S;
--
CREATE SEQUENCE XXCUS.XXCUS_CONCUR_TIEOUT_S
  START WITH 20170001
  MAXVALUE 9999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  CACHE 20
  NOORDER;
--  
