
  CREATE TABLE "XXCUS"."XXCUSCE_PNC_INBOUND_STG_TBL" 
   (	"RECORD_TYPE" VARCHAR2(1), 
	"INVOICE_NUMBER" VARCHAR2(250), 
	"INVOICE_DATE" DATE, 
	"CLIENT_REF_NUMBER" VARCHAR2(100), 
	"DOCUMENT_NUMBER" NUMBER, 
	"INVOICE_AMOUNT" NUMBER, 
	"TAX_AMOUNT" VARCHAR2(240), 
	"DISCOUNT_AMOUNT" VARCHAR2(240), 
	"CREATION_DATE" DATE
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "XXCUS_DATA" ;
