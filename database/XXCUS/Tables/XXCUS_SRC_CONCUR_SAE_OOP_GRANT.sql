/*
   Ticket#                               Date         Author             Notes
   -------------------------------------------------------------------------------------------------------------------------------------------------------
   TMS 20170412-00122 / ESMS 554216      02/17/2017   Balaguru Seshadri  Concur process related tables
*/
--
GRANT ALL ON XXCUS.XXCUS_SRC_CONCUR_SAE_OOP TO APPS WITH GRANT OPTION;
--