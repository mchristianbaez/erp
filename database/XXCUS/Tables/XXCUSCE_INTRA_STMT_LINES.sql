CREATE TABLE XXCUS.XXCUSCE_INTRA_STMT_LINES
(
  STATEMENT_LINE_ID            NUMBER(15)       NOT NULL,
  STATEMENT_HEADER_ID          NUMBER(15)       NOT NULL,
  LINE_NUMBER                  NUMBER(15)       NOT NULL,
  TRX_DATE                     DATE             NOT NULL,
  TRX_TYPE                     VARCHAR2(30 BYTE) NOT NULL,
  AMOUNT                       NUMBER           NOT NULL,
  STATUS                       VARCHAR2(30 BYTE),
  CREATED_BY                   NUMBER(15)       NOT NULL,
  CREATION_DATE                DATE             NOT NULL,
  LAST_UPDATED_BY              NUMBER(15)       NOT NULL,
  LAST_UPDATE_DATE             DATE             NOT NULL,
  TRX_CODE_ID                  NUMBER(15),
  EFFECTIVE_DATE               DATE,
  BANK_TRX_NUMBER              VARCHAR2(240 BYTE),
  TRX_TEXT                     VARCHAR2(255 BYTE),
  CUSTOMER_TEXT                VARCHAR2(80 BYTE),
  INVOICE_TEXT                 VARCHAR2(30 BYTE),
  BANK_ACCOUNT_TEXT            VARCHAR2(30 BYTE),
  CURRENCY_CODE                VARCHAR2(15 BYTE),
  EXCHANGE_RATE_TYPE           VARCHAR2(30 BYTE),
  EXCHANGE_RATE                NUMBER,
  EXCHANGE_RATE_DATE           DATE,
  ORIGINAL_AMOUNT              NUMBER,
  CHARGES_AMOUNT               NUMBER,
  LAST_UPDATE_LOGIN            NUMBER(15),
  RECONCILE_TO_STATEMENT_FLAG  VARCHAR2(1 BYTE),
  ATTRIBUTE_CATEGORY           VARCHAR2(30 BYTE),
  ATTRIBUTE1                   VARCHAR2(150 BYTE),
  ATTRIBUTE2                   VARCHAR2(150 BYTE),
  ATTRIBUTE3                   VARCHAR2(150 BYTE),
  ATTRIBUTE4                   VARCHAR2(150 BYTE),
  ATTRIBUTE5                   VARCHAR2(150 BYTE),
  ATTRIBUTE6                   VARCHAR2(150 BYTE),
  ATTRIBUTE7                   VARCHAR2(150 BYTE),
  ATTRIBUTE8                   VARCHAR2(150 BYTE),
  ATTRIBUTE9                   VARCHAR2(150 BYTE),
  ATTRIBUTE10                  VARCHAR2(150 BYTE),
  ATTRIBUTE11                  VARCHAR2(150 BYTE),
  ATTRIBUTE12                  VARCHAR2(150 BYTE),
  ATTRIBUTE13                  VARCHAR2(150 BYTE),
  ATTRIBUTE14                  VARCHAR2(150 BYTE),
  ATTRIBUTE15                  VARCHAR2(150 BYTE)
)
TABLESPACE XXCUS_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          1M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


