/*
History
======
Version Date       Author                 Notes
======  ========== ====================== =========================================
1.0     08/22/2016 Balaguru Seshadri      TMS 20160902-00099  ESMS 449491  - Rebate Data Reconcilation
*/
CREATE TABLE XXCUS.XXCUS_REBATE_RECEIPT_TBL
(
  GEO_LOC_HIER_LOB_NM  VARCHAR2(50 BYTE)        NOT NULL,
  SRC_SYS_NM           VARCHAR2(30 BYTE),
  BU_NM                VARCHAR2(50 BYTE)        NOT NULL,
  BU_CD                VARCHAR2(50 BYTE)        NOT NULL,
  FISCAL_PER_ID        NUMBER,
  VNDR_RECPT_UNQ_NBR   VARCHAR2(100 BYTE)       NOT NULL,
  PT_VNDR_ID           NUMBER,
  PT_VNDR_CD           VARCHAR2(50 BYTE)        NOT NULL,
  SF_VNDR_ID           NUMBER                   NOT NULL,
  SF_VNDR_CD           VARCHAR2(50 BYTE)        NOT NULL,
  REBT_VNDR_ID         VARCHAR2(200 BYTE),
  REBT_VNDR_CD         VARCHAR2(50 BYTE),
  REBT_VNDR_NM         VARCHAR2(100 BYTE),
  PROD_ID              NUMBER                   NOT NULL,
  SKU_CD               VARCHAR2(100 BYTE)       NOT NULL,
  VNDR_PART_NBR        VARCHAR2(50 BYTE),
  SHP_TO_BRNCH_ID      NUMBER,
  SHP_TO_BRNCH_CD      VARCHAR2(50 BYTE),
  SHP_TO_BRNCH_NM      VARCHAR2(100 BYTE),
  PO_NBR               VARCHAR2(50 BYTE),
  PO_CRT_DT            DATE,
  RECPT_CRT_DT         DATE                     NOT NULL,
  DROP_SHP_FLG         VARCHAR2(1 BYTE),
  PROD_UOM_CD          VARCHAR2(30 BYTE),
  ITEM_COST_AMT        NUMBER(18,5)             NOT NULL,
  CURNC_FLG            VARCHAR2(3 BYTE)         NOT NULL,
  RECPT_QTY            NUMBER                   NOT NULL,
  VNDR_SHP_QTY         NUMBER,
  BUY_PKG_UNT_CNT      NUMBER,
  AS_OF_DT             DATE,
  UPDATE_ACTION        VARCHAR2(20 BYTE),
  UPDATED_DATE         DATE,
  CURRENT_WEEK_RECORD  VARCHAR2(1 BYTE)
)
NOCOMPRESS 
TABLESPACE XXCUS_DATA
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
NOLOGGING
PARTITION BY LIST (BU_NM)
(  
  PARTITION ELE VALUES ('ELECTRICAL')
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   1
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION UTL_USA VALUES ('UTILISERV')
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   1
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION CTI VALUES ('INTERIORS')
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   1
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION WW VALUES ('WATERWORKS')
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   1
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION RR VALUES ('REPAIR_REMODEL')
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   1
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION FM VALUES ('FACILITIES MAINTENANCE')
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   1
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION WCC VALUES ('WHITECAP CONSTRUCTION')
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   1
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION WHCP VALUES ('WHITECAP')
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   1
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION USABB VALUES ('USABLUEBOOK')
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   1
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION OTHER VALUES (DEFAULT)
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   1
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               )
)
NOCACHE
NOPARALLEL
MONITORING;
--
COMMENT ON TABLE XXCUS.XXCUS_REBATE_RECEIPT_TBL IS 'TMS  20160711-00227 - ESMS 449491';
--