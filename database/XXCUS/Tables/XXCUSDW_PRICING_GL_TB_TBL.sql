CREATE TABLE XXCUS.XXCUSDW_PRICING_GL_TB_TBL
(
  DAY_SEQ      NUMBER                           NOT NULL,
  AUDIT_DATE   DATE                             NOT NULL,
  DAILY_COUNT  NUMBER                           NOT NULL,
  AUDIT_FLAG   VARCHAR2(1 BYTE)                 NOT NULL
)
TABLESPACE XXCUS_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          1M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


