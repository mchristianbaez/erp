/*
   Ticket#                               Date         Author             Notes
   -------------------------------------------------------------------------------------------------------------------------------------------------------
   TMS 20170412-00106 / ESMS 554216      02/17/2017   Balaguru Seshadri  Concur process related tables
*/
--
DROP TABLE XXCUS.XXCUS_SRC_CONCUR_SAE_OOP;
--
CREATE TABLE XXCUS.XXCUS_SRC_CONCUR_SAE_OOP
(
  PAYMENT_BATCH      VARCHAR2(255 BYTE),
  CHECK_DATE         DATE,
  CHECK_NUMBER       VARCHAR2(30 BYTE),
  EMPLOYEE_NUMBER    VARCHAR2(30 BYTE),
  EMP_FIRST_NAME     VARCHAR2(150 BYTE),
  EMP_LAST_NAME      VARCHAR2(150 BYTE),
  TERMINATED_DATE    DATE,
  EMPLOYEE_STATUS    VARCHAR2(10 BYTE),
  AMOUNT             NUMBER,
  PAYMENT_TYPE_FLAG  VARCHAR2(10 BYTE),
  LINE_NUMBERS       VARCHAR2(400 BYTE),
  EBS_CREATED_BY     NUMBER,
  EBS_CREATION_DATE  DATE,
  REQUEST_ID         NUMBER,
  ERP_PLATFORM       VARCHAR2(30 CHAR),
  RECORD_STATUS      VARCHAR2(30 BYTE),
  CRT_BTCH_ID        NUMBER,
  CONCUR_BATCH_ID    NUMBER
)
;
--
COMMENT ON TABLE XXCUS.XXCUS_SRC_CONCUR_SAE_OOP IS 'TMS 20170412-00106 / ESMS 554216';
--
GRANT ALL ON XXCUS.XXCUS_SRC_CONCUR_SAE_OOP TO APPS;
--
