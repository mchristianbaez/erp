CREATE TABLE XXCUS.TAXAUDIT_HEADER_14OCT
(
  HEADERNO        NUMBER(15),
  ORACLEID        NUMBER(15)                    NOT NULL,
  INVNUM          VARCHAR2(20 BYTE),
  DIVCODE         VARCHAR2(20 BYTE),
  COMPID          VARCHAR2(20 BYTE),
  CUSTNUM         VARCHAR2(20 BYTE),
  CUSTNAME        VARCHAR2(20 BYTE),
  BILLTOCUSTID    VARCHAR2(20 BYTE),
  BILLTOCUSTNAME  VARCHAR2(20 BYTE),
  INVDATE         DATE                          NOT NULL,
  TRANSDATE       DATE                          NOT NULL,
  FISCALDATE      DATE
)
TABLESPACE XXCUS_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


