CREATE TABLE XXCUS.XXCUS_REBATE_PRODUCT_CURR_TBL
(
  GEO_LOC_HIER_LOB_NM  VARCHAR2(50 BYTE)        NOT NULL,
  BU_NM                VARCHAR2(50 BYTE)        NOT NULL,
  SRC_SYS_NM           VARCHAR2(30 BYTE),
  PROD_ID              NUMBER                   NOT NULL,
  SKU_CD               VARCHAR2(100 BYTE)       NOT NULL,
  SKU_DESC_LNG         VARCHAR2(100 BYTE)       NOT NULL,
  VNDR_PART_NBR        VARCHAR2(100 BYTE),
  PROD_PURCH_UOM       VARCHAR2(30 BYTE),
  PO_REPL_COST         NUMBER(18,5),
  PRIM_UPC             VARCHAR2(40 BYTE),
  SECONDARY_UPC        VARCHAR2(40 BYTE),
  TERTIARY_UPC         VARCHAR2(40 BYTE),
  UNSPSC_CD            VARCHAR2(10 BYTE),
  AS_OF_DT             DATE,
  REQUEST_ID           NUMBER,
  STATUS_FLAG          VARCHAR2(1 BYTE),
  PROCESS_DATE         DATE
)
TABLESPACE XXCUS_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


