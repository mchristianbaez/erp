/**************************************************************************
   Module Name: ORACLE TRADE MANAGEMENT

   PURPOSE:   RECURRING ACCRUAL PROCESS
   REVISIONS:
   Ver        Date        Author             	Description
   ---------  ----------  ---------------   	-------------------------
    1.0       10/23/2018  Bala Seshadri   	Initial Build - Task ID: 20180116-00033
/*************************************************************************/
DROP TABLE XXCUS.XXCUS_OZF_UTILS_STG CASCADE CONSTRAINTS;
CREATE TABLE XXCUS.XXCUS_OZF_UTILS_STG
(
  RECORD_SEQ      NUMBER,
  RUN_ID          NUMBER,
  UTILIZATION_ID  NUMBER,
  REQUEST_ID      NUMBER,
  RUN_MODE        VARCHAR2(10 BYTE),
  EBS_UPDATED_BY  NUMBER,
  LINE_NUM        NUMBER                        DEFAULT 0,
  OBJECT_ID       NUMBER                        DEFAULT 0,
  DETAIL_SEQ      NUMBER                        DEFAULT 0
)
TABLESPACE XXCUS_DATA
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;
CREATE INDEX XXCUS.XXCUS_OZF_UTILS_STG_U1 ON XXCUS.XXCUS_OZF_UTILS_STG
(RECORD_SEQ, RUN_ID, UTILIZATION_ID)
LOGGING
TABLESPACE XXCUS_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
NOPARALLEL;
COMMENT ON TABLE XXCUS.XXCUS_OZF_UTILS_STG IS 'TMS 20180116-00033, FRESH DESK: 7696, SNOW: RITM0040124';