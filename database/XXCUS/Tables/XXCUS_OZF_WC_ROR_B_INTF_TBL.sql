/*
 -- 
 --  Ticket                                                 Date                      Author                                                       Notes
 --  =========================   =============   ============================== ==========================================================
 --  TMS 20160809-00142                08/09/2016         Balaguru Seshadri                                 ESMS 430286 - New routine to extract White Cap ROR Data
 -- 
*/
DROP TABLE XXCUS.XXCUS_OZF_WC_ROR_B_INTF CASCADE CONSTRAINTS;
CREATE TABLE XXCUS.XXCUS_OZF_WC_ROR_B_INTF
(
  CURRENT_AGREEMENT_YEAR  NUMBER,
  MVID                    VARCHAR2(30 BYTE),
  VENDOR                  VARCHAR2(360 BYTE),
  PLAN_ID                 NUMBER,
  OFFER_NAME              VARCHAR2(2000 BYTE),
  PURCHASES               NUMBER,
  TOTAL_ACCRUALS          NUMBER,
  SYSTEM_ACCRUALS         NUMBER,
  MANUAL_ADJUSTMENTS      NUMBER,
  TOTAL_ROR               NUMBER,
  SYSTEM_ROR              NUMBER,
  MANUAL_ROR              NUMBER,
  CREATED_BY              NUMBER,
  CREATION_DATE           DATE
)
TABLESPACE XXCUS_DATA
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;
