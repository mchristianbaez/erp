CREATE TABLE XXCUS.TAXSTPROD_14OCT
(
  PRODCODE         VARCHAR2(5 BYTE),
  STCODE           NUMBER(2),
  CURRATE          NUMBER(5,5)                  NOT NULL,
  PRIORRATE        NUMBER(5,5)                  NOT NULL,
  EFFDATE          DATE                         NOT NULL,
  MAXCURAMT        NUMBER(14,2)                 NOT NULL,
  MAXPRIORAMT      NUMBER(14,2)                 NOT NULL,
  MAXCURTAXABLE    NUMBER(14,2)                 NOT NULL,
  MAXPRIORTAXABLE  NUMBER(14,2)                 NOT NULL,
  MAXCURRATE1      NUMBER(5,5)                  NOT NULL,
  MAXPRIORRATE1    NUMBER(5,5)                  NOT NULL,
  MAXCURRATE2      NUMBER(5,5)                  NOT NULL,
  MAXPRIORRATE2    NUMBER(5,5)                  NOT NULL,
  MAXEFFDATE       DATE,
  MAXCURCODE       VARCHAR2(2 BYTE),
  MAXPRIORCODE     VARCHAR2(2 BYTE),
  PRODDESC         VARCHAR2(12 BYTE),
  MAXTAX           VARCHAR2(1 BYTE),
  RECCITY          VARCHAR2(1 BYTE),
  RECCNTY          VARCHAR2(1 BYTE),
  TAXST            VARCHAR2(1 BYTE),
  TAXCNTY          VARCHAR2(1 BYTE),
  TAXCITY          VARCHAR2(1 BYTE),
  TAXTRAN          VARCHAR2(1 BYTE)
)
TABLESPACE XXCUS_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


