/*
   Ticket#                               Date         Author             Notes
   -------------------------------------------------------------------------------------------------------------------------------------------------------
   TMS 20170412-00106 / ESMS 554216      02/17/2017   Balaguru Seshadri  Concur process related tables
*/
--
DROP TABLE XXCUS.XXCUS_CONCUR_SAE_HEADER;
--
CREATE TABLE XXCUS.XXCUS_CONCUR_SAE_HEADER
(
  SAE_TYPE           VARCHAR2(30 BYTE)          DEFAULT 'EXTRACT',
  BATCH_DATE         DATE                       NOT NULL,
  RECORD_COUNT       NUMBER                     NOT NULL,
  JOURNAL_AMOUNT     NUMBER                     NOT NULL,
  CONCUR_BATCH_ID    NUMBER                     NOT NULL,
  CRT_BTCH_ID        NUMBER,
  BATCH_STATUS       VARCHAR2(60 BYTE),
  HDS_CREATED_BY     VARCHAR2(20 BYTE),
  HDS_CREATION_DATE  DATE,
  LAST_UPDATED_BY    NUMBER,
  LAST_UPDATE_DATE   DATE,
  REQUEST_ID         NUMBER                     DEFAULT 0,
  EMAIL_FLAG         VARCHAR2(1 BYTE)           DEFAULT 'N',
  RUN_ID             NUMBER                     DEFAULT 0,
  FISCAL_PERIOD      VARCHAR2(10 BYTE)
)
;
--
COMMENT ON TABLE XXCUS.XXCUS_CONCUR_SAE_HEADER IS 'TMS 20170412-00106 / ESMS 554216';
--

