CREATE TABLE XXCUS.XXCUS_ADP_TAXWARE_DML
(
  PROD_CODE              VARCHAR2(30 BYTE)      NOT NULL,
  POST_PROCESS_SQL_TEXT  CLOB                   NOT NULL
)
TABLESPACE XXCUS_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


