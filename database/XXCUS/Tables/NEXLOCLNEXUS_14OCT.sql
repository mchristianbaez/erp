CREATE TABLE XXCUS.NEXLOCLNEXUS_14OCT
(
  MERCHANTID        VARCHAR2(20 BYTE),
  BUSNLOCN          VARCHAR2(13 BYTE),
  STCODE            NUMBER(2),
  RECTYPE           VARCHAR2(1 BYTE),
  NAME              VARCHAR2(26 BYTE),
  TAX_REGISTRATION  VARCHAR2(100 BYTE),
  CALCULATE_TAX     VARCHAR2(1 BYTE)            NOT NULL
)
TABLESPACE XXCUS_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


