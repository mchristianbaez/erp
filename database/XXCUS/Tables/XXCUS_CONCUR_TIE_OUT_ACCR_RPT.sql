/*
Ticket#                            Date         Author            Notes
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
TMS 20170530-00025 / ESMS 561767   05/30/2017   Balaguru Seshadri Concur accrual process related tables  
*/
--
DROP TABLE XXCUS.XXCUS_CONCUR_TIE_OUT_ACCR_RPT;
--
CREATE TABLE XXCUS.XXCUS_CONCUR_TIE_OUT_ACCR_RPT
(
  REPORT_TYPE       VARCHAR2(30 BYTE),
  EXTRACT_TYPE      VARCHAR2(30 BYTE),
  SEQUENCE          NUMBER,
  LINE_ITEM_NAME    VARCHAR2(60 BYTE),
  LINE_ITEM_AMOUNT  NUMBER,
  LINE_COMMENTS     VARCHAR2(150 BYTE),
  STATUS            VARCHAR2(60 BYTE),
  EMAIL_FLAG        VARCHAR2(1 BYTE),
  CREATION_DATE     DATE,
  CREATED_BY        NUMBER,
  REQUEST_ID        NUMBER,
  RUN_ID            NUMBER,
  ACCRUAL_RUN_DATE  DATE,
  FISCAL_PERIOD     VARCHAR2(10 BYTE)
)
;
--
COMMENT ON TABLE XXCUS.XXCUS_CONCUR_TIE_OUT_ACCR_RPT IS 'TMS 20170530-00025 / ESMS 561767'
--
