/*
TMS 20160602-00010 / ESMS 321264
Author: balaguru seshadri
Date: 07/07/2016
Scope: The new table will hold the the tier zero accrual data extracted.
*/
DROP TABLE XXCUS.XXCUS_OZF_TIER_ZERO_ACCRUALS_B;
CREATE TABLE XXCUS.XXCUS_OZF_TIER_ZERO_ACCRUALS_B
(
  XLA_ACCRUAL_ID                  NUMBER,
  OFU_REQUEST_ID                  NUMBER,
  UTILIZATION_TYPE                VARCHAR2(30 BYTE),
  ADJUSTMENT_TYPE                 VARCHAR2(30 BYTE),
  ADJUSTMENT_TYPE_ID              NUMBER,
  OBJECT_ID                       NUMBER,
  REBATE_AMOUNT                   NUMBER,
  UTIL_ACCTD_AMOUNT               NUMBER,
  EVENT_TYPE_CODE                 VARCHAR2(30 BYTE),
  ENTITY_CODE                     VARCHAR2(30 BYTE),
  EVENT_ID                        NUMBER,
  UTILIZATION_ID                  NUMBER,
  OXA_ORG_ID                      NUMBER,
  OFU_GL_DATE                     DATE,
  OZF_LEDGER_ID                   VARCHAR2(150 BYTE),
  OZF_LEDGER_NAME                 VARCHAR2(30 BYTE),
  XLAE_ENTITY_ID                  NUMBER(15),
  XLAE_TRXN_DATE                  DATE,
  XLAE_EVENT_DATE                 DATE,
  XLAE_EVENT_STATUS_CODE          VARCHAR2(1 BYTE),
  XLAE_PROCESS_STATUS_CODE        VARCHAR2(1 BYTE),
  XLAE_EVENT_NUMBER               NUMBER(38),
  XLAE_ONHOLD_FLAG                VARCHAR2(1 BYTE),
  XLA_APPLICATION_ID              NUMBER(15),
  XAEH_AE_HEADER_ID               NUMBER(15),
  XAEH_ACCOUNTING_DATE            DATE,
  XAEH_GL_XFER_STATUS_CODE        VARCHAR2(30 BYTE),
  XAEH_GL_XFER_DATE               DATE,
  XAEH_JE_CATEGORY                VARCHAR2(30 BYTE),
  FISCAL_MONTH                    VARCHAR2(3 BYTE),
  XAEH_PERIOD_NAME                VARCHAR2(15 BYTE),
  ACCOUNTING_ERROR_ID             NUMBER(15),
  ACCOUNTING_BATCH_ID             NUMBER(15),
  ENCODED_MSG                     VARCHAR2(2000 BYTE),
  ERR_AE_LINE_NUM                 NUMBER(15),
  MESSAGE_NUMBER                  NUMBER(9),
  ERROR_SOURCE_CODE               VARCHAR2(30 BYTE),
  OFFER_ID                        NUMBER,
  OFFER_CODE                      VARCHAR2(30 BYTE),
  OFFER_NAME                      VARCHAR2(2000 BYTE),
  OFFER_TYPE                      VARCHAR2(30 BYTE),
  AUTOPAY_PARTY_ID                NUMBER,
  BENEFICIARY_ACCOUNT_ID          NUMBER,
  OFFER_STATUS_CODE               VARCHAR2(30 BYTE),
  OFFER_CUSTOM_SETUP_ID           NUMBER,
  OFFER_USER_STATUS_ID            NUMBER,
  OFFER_OWNER_ID                  NUMBER,
  ACTIVITY_MEDIA_ID               NUMBER,
  ACTIVITY_MEDIA_NAME             VARCHAR2(120 BYTE),
  OFFER_AUTOPAY_FLAG              VARCHAR2(1 BYTE),
  OFU_CURRENCY_CODE               VARCHAR2(30 BYTE),
  RECEIPTS_CURRENCY               VARCHAR2(30 BYTE),
  AUTO_RENEWAL                    VARCHAR2(240 BYTE),
  CALENDAR_YEAR                   VARCHAR2(240 BYTE),
  UNTIL_YEAR                      VARCHAR2(240 BYTE),
  PAYMENT_METHOD                  VARCHAR2(240 BYTE),
  PAYMENT_FREQUENCY               VARCHAR2(240 BYTE),
  GUARANTEED_AMOUNT               VARCHAR2(240 BYTE),
  QP_LIST_HEADER_ID               NUMBER,
  QP_GLOBAL_FLAG                  VARCHAR2(1 BYTE),
  QP_ACTIVE_FLAG                  VARCHAR2(1 BYTE),
  QP_ORIG_ORG_ID                  NUMBER,
  OFU_YEAR_ID                     NUMBER,
  FUND_ID                         NUMBER,
  FUND_NAME                       VARCHAR2(240 BYTE),
  PLAN_TYPE                       VARCHAR2(30 BYTE),
  COMPONENT_ID                    NUMBER,
  COMPONENT_TYPE                  VARCHAR2(30 BYTE),
  OBJECT_TYPE                     VARCHAR2(30 BYTE),
  REFERENCE_TYPE                  VARCHAR2(30 BYTE),
  REFERENCE_ID                    NUMBER,
  AMS_ACTIVITY_BUDGET_ID          NUMBER,
  PRODUCT_LEVEL_TYPE              VARCHAR2(30 BYTE),
  PRODUCT_ID                      NUMBER,
  RESALE_DATE_CREATED             DATE,
  DATE_INVOICED                   DATE,
  PO_NUMBER                       VARCHAR2(50 BYTE),
  PURCHASE_UOM_CODE               VARCHAR2(3 BYTE),
  UOM_CODE                        VARCHAR2(3 BYTE),
  PURCHASE_PRICE                  NUMBER,
  OFU_CUST_ACCOUNT_ID             NUMBER,
  OFU_MVID_NAME                   VARCHAR2(150 BYTE),
  OFU_MVID                        VARCHAR2(150 BYTE),
  BILLTO_CUST_ACCT_ID_BR          NUMBER,
  BILLTO_CUST_ACCT_BR_NAME        VARCHAR2(150 BYTE),
  BILLTO_CUST_ACCT_BR_ACCT_NAME   VARCHAR2(30 BYTE),
  BILL_TO_PARTY_ID                NUMBER,
  BILL_TO_PARTY_NAME_LOB          VARCHAR2(360 BYTE),
  DATE_ORDERED                    DATE,
  SELLING_PRICE                   NUMBER,
  QUANTITY                        NUMBER,
  INVENTORY_ITEM_ID               NUMBER,
  ITEM_NUMBER                     VARCHAR2(150 BYTE),
  RESALE_LINE_ATTRIBUTE1          VARCHAR2(150 BYTE),
  RESALE_LINE_ATTRIBUTE2          VARCHAR2(150 BYTE),
  RESALE_LINE_ATTRIBUTE3          VARCHAR2(150 BYTE),
  RESALE_LINE_ATTRIBUTE5          VARCHAR2(150 BYTE),
  RESALE_LINE_ATTRIBUTE6          VARCHAR2(150 BYTE),
  RESALE_LINE_ATTRIBUTE11         VARCHAR2(150 BYTE),
  RESALE_LINE_ATTRIBUTE12         VARCHAR2(150 BYTE),
  RESALE_LINE_ATTRIBUTE13         VARCHAR2(150 BYTE),
  RESALE_LINE_ATTRIBUTE14         VARCHAR2(150 BYTE),
  PRICING_ADJ_ID                  NUMBER,
  OFU_EXCHANGE_RATE_DATE          DATE,
  OFU_EXCHANGE_RATE               NUMBER,
  OFU_BILLTO_CUST_ACCOUNT_ID_BR   NUMBER,
  OFU_PRODUCT_ID                  NUMBER,
  SOLD_FROM_CUST_ACCOUNT_ID_MVID  NUMBER,
  SOLD_FROM_SITE_ID_MVID          NUMBER,
  SOLD_FROM_PARTY_NAME_MVID       VARCHAR2(150 BYTE),
  POSTED_FIN_LOC                  VARCHAR2(10 BYTE),
  COOP_YES_NO                     VARCHAR2(1 BYTE),
  POSTED_BU_BRANCH                VARCHAR2(10 BYTE),
  OVERRIDE_FIN_LOC                VARCHAR2(1 BYTE),
  OFU_ATTRIBUTE10                 VARCHAR2(150 BYTE),
  OFU_CREATION_DATE               DATE,
  FISCAL_PARTITION                 VARCHAR2(10)
)
NOCOMPRESS 
TABLESPACE XXCUS_DATA
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
NOLOGGING
PARTITION BY LIST (FISCAL_PARTITION)
(  
  PARTITION NA VALUES (DEFAULT)
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               )
)
NOCACHE
MONITORING;
--
COMMENT ON TABLE XXCUS.XXCUS_OZF_TIER_ZERO_ACCRUALS_B IS 'TMS  20160602-00010 ESMS 321264';
--