CREATE TABLE XXCUS.XXCUSGL_BAL_EXTRACT_TBL
(
  COMBINATION_ID               NUMBER,
  SEGMENT1                     VARCHAR2(2 BYTE),
  SEGMENT2                     VARCHAR2(5 BYTE),
  SEGMENT3                     VARCHAR2(4 BYTE),
  SEGMENT4                     VARCHAR2(6 BYTE),
  SEGMENT5                     VARCHAR2(5 BYTE),
  SEGMENT6                     VARCHAR2(5 BYTE),
  SEGMENT7                     VARCHAR2(5 BYTE),
  DESCRIPTION                  VARCHAR2(50 BYTE),
  PERIOD_NET_DR                NUMBER,
  PERIOD_NET_CR                NUMBER,
  CURRENCY_CODE                VARCHAR2(3 BYTE),
  BEGIN_BALANCE_DR             NUMBER,
  BEGIN_BALANCE_CR             NUMBER,
  PERIOD_YEAR                  NUMBER(15),
  PERIOD_NUM                   NUMBER(15),
  CCID_ENABLED                 VARCHAR2(1 BYTE),
  DETAIL_POSTING_ALLOWED_FLAG  VARCHAR2(1 BYTE),
  CCID_END_DATE                DATE,
  ACCT_ENABLED                 VARCHAR2(1 BYTE),
  ACCT_END_DATE                DATE,
  PROD_ENABLED                 VARCHAR2(1 BYTE),
  PROD_END_DATE                DATE,
  LOC_ENABLED                  VARCHAR2(1 BYTE),
  LOC_END_DATE                 DATE,
  CC_ENABLED                   VARCHAR2(1 BYTE),
  CC_END_DATE                  DATE,
  PROJ_ENABLED                 VARCHAR2(1 BYTE),
  PROJ_END_DATE                DATE,
  FISCAL_SUM_DR                NUMBER,
  FISCAL_SUM_CR                NUMBER
)
TABLESPACE XXCUS_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          1M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


