CREATE TABLE XXCUS.XXCUSPN_PRODUCTS_TBL
(
  PRODUCT      VARCHAR2(240 BYTE),
  ORA_SEG_ONE  VARCHAR2(10 BYTE),
  L_CORP_ID    VARCHAR2(10 BYTE),
  LOCATION     VARCHAR2(240 BYTE)
)
TABLESPACE XXCUS_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


