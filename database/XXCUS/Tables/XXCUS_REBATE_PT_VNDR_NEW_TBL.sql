CREATE TABLE XXCUS.XXCUS_REBATE_PT_VNDR_NEW_TBL
(
  PT_VNDR_ID     NUMBER,
  PT_VNDR_CD     VARCHAR2(50 BYTE),
  BU_NM          VARCHAR2(100 BYTE),
  CREATION_DATE  DATE
)
TABLESPACE XXCUS_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


