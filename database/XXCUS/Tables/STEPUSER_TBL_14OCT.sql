CREATE TABLE XXCUS.STEPUSER_TBL_14OCT
(
  USERNAME  VARCHAR2(30 BYTE),
  USERPSWD  NUMBER(10),
  USERTYPE  VARCHAR2(5 BYTE)                    NOT NULL,
  USERROLE  VARCHAR2(30 BYTE)                   NOT NULL
)
TABLESPACE XXCUS_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


