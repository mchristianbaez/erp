   /* ************************************************************************
    HD Supply
    All rights reserved.
    Ticket                   Date         Author             Description
    ----------------------   ----------   ----------         ------------------------      
    TMS 20151008-00085       11/19/2015   Balaguru Seshadri  Add end_cust_party_id field data type number.     
   ************************************************************************ */ 
--
CREATE TABLE XXCUS.XXCUSOZF_REBATE_ADJUST_TBL
(
  INTERFACE_ID        NUMBER                    NOT NULL,
  LIST_HEADER_ID      NUMBER                    NOT NULL,
  OFFER_NAME          VARCHAR2(240 BYTE),
  OFFER_CODE          VARCHAR2(100 BYTE),
  AMOUNT              NUMBER,
  CURRENCY_CODE       VARCHAR2(3 BYTE),
  CUST_ACCOUNT_ID     NUMBER,
  LOB_PARTY_ID        NUMBER,
  LOCATION_ID         NUMBER,
  ITEM_TYPE           VARCHAR2(50 BYTE),
  ITEM_ID             NUMBER,
  EFF_START_DATE      DATE,
  EFF_END_DATE        DATE,
  ADJUST_REASON       VARCHAR2(100 BYTE),
  LAST_UPDATE_DATE    DATE,
  LAST_UPDATED_BY     NUMBER,
  CREATION_DATE       DATE,
  CREATED_BY          NUMBER,
  STATUS_CODE         VARCHAR2(1 BYTE),
  MVID                VARCHAR2(30 BYTE),
  LOB_PARTY           VARCHAR2(150 BYTE),
  BRANCH_NAME         VARCHAR2(150 BYTE),
  REQUEST_ID          VARCHAR2(30 BYTE),
  CONC_JOB_SUBMIT     VARCHAR2(1 BYTE),
  ADJUST_REASON_NAME  VARCHAR2(100 BYTE),
  END_CUST_PARTY_ID   NUMBER -- TMS 20151008-00085 
);
--
COMMENT ON TABLE XXCUS.XXCUSOZF_REBATE_ADJUST_TBL IS 'TMS 20151008-00085';
--
CREATE OR REPLACE SYNONYM APPS.XXCUSOZF_REBATE_ADJUST_TBL FOR XXCUS.XXCUSOZF_REBATE_ADJUST_TBL;
--
