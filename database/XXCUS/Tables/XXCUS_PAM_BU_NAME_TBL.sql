/*
-- 
-- --------------------------------------------------------------------------------------------------------------------------------
TICKET#                            DATE              AUTHOR                           NOTES  
-- --------------------------------------------------------------------------------------------------------------------------------
TMS 20171013-00238 / ESMS 538724   07/05/2017       BALAGURU SESHADRI                 NEW EBS PAM CUSTOM TABLES
TMS 20171218-00072                 12/18/2017       BALAGURU SESHADRI                 ADD NEW COLUMN BU_STATUS
-- 
*/
--
DROP TABLE XXCUS.XXCUS_PAM_BU_NAME_TBL CASCADE CONSTRAINTS;
--
CREATE TABLE XXCUS.XXCUS_PAM_BU_NAME_TBL
(
  ID                 NUMBER                     NOT NULL,
  COUNTRY            VARCHAR2(30 BYTE),
  LOB_NAME           VARCHAR2(60 BYTE),
  LOB_ID             NUMBER,
  BU_NAME            VARCHAR2(60 BYTE),
  BU_ID              NUMBER,
  EBS_CREATION_DATE  DATE,
  EBS_CREATED_BY     NUMBER,
  REQUEST_ID         NUMBER,
  BU_ACTIVE          VARCHAR2(1) -- TMS 20171218-00072
)
;
--
CREATE INDEX XXCUS.XXCUS_PAM_BU_NAME_TBL_N1 ON XXCUS.XXCUS_PAM_BU_NAME_TBL (LOB_ID);
--
CREATE UNIQUE INDEX XXCUS.XXCUS_PAM_BU_NAME_TBL_U1 ON XXCUS.XXCUS_PAM_BU_NAME_TBL (ID);
--
COMMENT ON TABLE XXCUS.XXCUS_PAM_BU_NAME_TBL  IS 'TMS 20171218-00072';
--