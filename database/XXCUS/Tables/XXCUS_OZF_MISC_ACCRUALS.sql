/**************************************************************************
   Module Name: ORACLE TRADE MANAGEMENT

   PURPOSE:   RECURRING ACCRUAL PROCESS
   REVISIONS:
   Ver        Date        Author             	Description
   ---------  ----------  ---------------   	-------------------------
    1.0       10/23/2018  Bala Seshadri   	Initial Build - Task ID: 20180116-00033
/*************************************************************************/
DROP TABLE XXCUS.XXCUS_OZF_MISC_ACCRUALS CASCADE CONSTRAINTS;
CREATE TABLE XXCUS.XXCUS_OZF_MISC_ACCRUALS
(
  RECORD_SEQ                NUMBER,
  LINE_NUM                  NUMBER,
  CUST_ACCOUNT_ID           NUMBER,
  PLAN_ID                   NUMBER,
  BRANCH_NAME               VARCHAR2(150 BYTE),
  PURCHASE_PERCENT          NUMBER,
  RECURRING_ACCRUAL_AMOUNT  NUMBER,
  STATUS_CODE               VARCHAR2(150 BYTE)  NOT NULL,
  STATUS_DESC               VARCHAR2(240 BYTE),
  UTILIZATION_ID            NUMBER,
  EBS_UPDATED_BY            NUMBER,
  EBS_UPDATE_DATE           DATE,
  BRANCH_CUST_ACCT_ID       NUMBER,
  RUN_ID                    NUMBER,
  API_STATUS                VARCHAR2(1 BYTE),
  REQUEST_ID                NUMBER,
  RUN_MODE                  VARCHAR2(10 BYTE)
)
TABLESPACE XXCUS_DATA
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;
COMMENT ON TABLE XXCUS.XXCUS_OZF_MISC_ACCRUALS IS 'TMS 20180116-00033, FRESH DESK: 7696, SNOW: RITM0040124';