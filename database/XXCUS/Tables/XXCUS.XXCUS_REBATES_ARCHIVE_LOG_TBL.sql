/*
  @GSC Incident 615950 - Rebates archive process log table
  WC TMS: 20160316-00196 
  Date: 04/15/2016
*/
DROP TABLE XXCUS.XXCUS_REBATES_ARCHIVE_LOG CASCADE CONSTRAINTS;

CREATE TABLE XXCUS.XXCUS_REBATES_ARCHIVE_LOG
(
  FISCAL_PERIOD     VARCHAR2(10 BYTE),
  DELETE_SQL        CLOB,
  INSERT_SQL        CLOB,
  REQUEST_ID        NUMBER,
  CREATED_BY        NUMBER,
  CREATION_DATE     DATE,
  RECORDS_ARCHIVED  NUMBER,
  RECORDS_DELETED   NUMBER,
  RECORD_TYPE VARCHAR2(30)
)
LOB (DELETE_SQL) STORE AS (
  TABLESPACE  XXCUS_DATA
  ENABLE      STORAGE IN ROW
  CHUNK       8192
  RETENTION
  NOCACHE
  LOGGING
      STORAGE    (
                  INITIAL          64K
                  NEXT             1M
                  MINEXTENTS       1
                  MAXEXTENTS       UNLIMITED
                  PCTINCREASE      0
                  BUFFER_POOL      DEFAULT
                  FLASH_CACHE      DEFAULT
                  CELL_FLASH_CACHE DEFAULT
                 ))
LOB (INSERT_SQL) STORE AS (
  TABLESPACE  XXCUS_DATA
  ENABLE      STORAGE IN ROW
  CHUNK       8192
  RETENTION
  NOCACHE
  LOGGING
      STORAGE    (
                  INITIAL          64K
                  NEXT             1M
                  MINEXTENTS       1
                  MAXEXTENTS       UNLIMITED
                  PCTINCREASE      0
                  BUFFER_POOL      DEFAULT
                  FLASH_CACHE      DEFAULT
                  CELL_FLASH_CACHE DEFAULT
                 ))
TABLESPACE XXCUS_DATA
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;
