CREATE TABLE XXCUS.XXCUSGL_FA_SUBLED_TBL
(
  ASSET_ID                NUMBER(15),
  DH_CCID                 NUMBER(15),
  DATE_PLACED_IN_SERVICE  DATE,
  METHOD_CODE             VARCHAR2(12 BYTE),
  LIFE                    NUMBER(6),
  RATE                    NUMBER,
  CAPACITY                NUMBER,
  COST                    NUMBER,
  DEPRN_AMOUNT            NUMBER,
  YTD_DEPRN               NUMBER,
  PERCENT                 NUMBER,
  TRANSACTION_TYPE        VARCHAR2(1 BYTE),
  DEPRN_RESERVE           NUMBER,
  PERIOD_COUNTER          NUMBER(15),
  DATE_EFFECTIVE          DATE,
  DEPRN_RESERVE_ACCT      VARCHAR2(25 BYTE),
  RESERVE_ACCT            VARCHAR2(25 BYTE),
  DISTRIBUTION_ID         NUMBER(15),
  UNITS_ASSIGNED          NUMBER,
  LOCATION_ID             NUMBER,
  SEGMENT1                VARCHAR2(2 BYTE),
  SEGMENT2                VARCHAR2(5 BYTE),
  SEGMENT3                VARCHAR2(4 BYTE),
  SEGMENT5                VARCHAR2(5 BYTE),
  ASSET_COST_ACCT         VARCHAR2(6 BYTE),
  COST_ACCT_DESC          VARCHAR2(50 BYTE),
  DEP_RESV_DESC           VARCHAR2(50 BYTE),
  CURRENCY_CODE           VARCHAR2(3 BYTE)
)
TABLESPACE XXCUS_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


