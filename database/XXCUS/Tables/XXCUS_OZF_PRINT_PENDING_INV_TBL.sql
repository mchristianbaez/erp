--
CREATE TABLE xxcus.xxcus_ozf_print_pending_inv
(
 mvid_name             VARCHAR2(150)
,mvid_number           VARCHAR2(30)
,invoice_number        VARCHAR2(30)
,invoice_date          DATE
,invoice_type          VARCHAR2(30)
,invoice_currency      VARCHAR2(10)
,invoice_amount        NUMBER
,balance_due           NUMBER
,offer_code            VARCHAR2(30)
,offer_name            VARCHAR2(150)
,offer_cal_year        VARCHAR2(10)
,invoice_line_num      NUMBER
,lob_id                VARCHAR2(30)
,lob_name              VARCHAR2(150)
,pmt_freq_code         VARCHAR2(30)
,pmt_freq_desc         VARCHAR2(30)
,org_id                NUMBER
,cust_account_id       NUMBER
,customer_trx_id       NUMBER
,cust_trx_type_id      NUMBER
,customer_trx_line_id  NUMBER
,claim_id              NUMBER
,claim_line_id         NUMBER
,list_header_id        NUMBER
,request_id            NUMBER
,lob_request_id        NUMBER
,created_by            NUMBER
,creation_date         DATE
);
--
COMMENT ON TABLE xxcus.xxcus_ozf_print_pending_inv IS 'RFC 43035 / ESMS 269629';
--
CREATE INDEX xxcus.xxcus_ozf_print_pending_inv_N1 ON xxcus.xxcus_ozf_print_pending_inv(LOB_ID);
--