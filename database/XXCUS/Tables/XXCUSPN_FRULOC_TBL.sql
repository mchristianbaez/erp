CREATE TABLE XXCUS.XXCUSPN_FRULOC_TBL
(
  FRU            VARCHAR2(5 BYTE),
  LOCATION_CODE  VARCHAR2(10 BYTE),
  PRODUCT        VARCHAR2(240 BYTE)
)
TABLESPACE XXCUS_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


