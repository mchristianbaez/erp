ALTER TABLE XXCUS.XXCUS_BULLET_IEXP_TBL
  ADD (tax_province varchar2(3),
       gst_amount number,
       pst_amount number,
       qst_amount number,
       hst_amount number,
       org_id number);
