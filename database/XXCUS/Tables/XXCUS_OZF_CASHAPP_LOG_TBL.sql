/*************************************************************************
  $Header XXCUS_OZF_CASHAPP_LOG_TBL.sql $
  Module Name: XXCUS_OZF_CASHAPP_LOG_TBL

  PURPOSE: Used by HDS Rebates AR Auto Cash Application Process 

  REVISIONS:
  Ver        Date                   Author                          Description
  ---------  -----------          ------------------             ----------------
  1.0        03-Aug-2015  Balaguru Seshadri     Initial Version TMS # 20150710-00251
  1.1        05/31/2016    Balaguru Seshadri    TMS 20160525-00069 / ESMS 327337

**************************************************************************/
CREATE TABLE XXCUS.XXCUS_OZF_CASHAPP_LOG
(
  REBATE_TYPE           VARCHAR2(30),
  CALENDAR_YEAR    VARCHAR2(10),
  AGREEMENT            VARCHAR2(2000),
  CUSTOMER_ID number,
  CUSTOMER_NUMBER varchar2(30),
  CUSTOMER_NAME varchar2(150),  
  CASH_RECEIPT_ID  NUMBER,
  RECEIPT_NUMBER   VARCHAR2(30 BYTE),
  RECEIPT_DATE     DATE,
  RECEIPT_AMOUNT   NUMBER,
  CUSTOMER_TRX_ID  NUMBER,
  INVOICE_NUMBER   VARCHAR2(30 BYTE),
  INVOICE_DATE     DATE,
  INVOICE_AMOUNT   NUMBER,
  APPLIED_AMOUNT   NUMBER,
  LOG_SEQ          NUMBER,
  API_STATUS       VARCHAR2(30 BYTE),
  API_MSG_INDEX    NUMBER,
  API_MESSAGE      VARCHAR2(4000 BYTE),
  EXECUTION_MODE   VARCHAR2(20),
  REQUEST_ID NUMBER,
  RUN_DATE DATE,
  CREATED_BY NUMBER,
  ORG_ID NUMBER --Ver 1.1
);
 COMMENT ON TABLE XXCUS.XXCUS_OZF_CASHAPP_LOG IS 'TMS 20160525-00069 / ESMS 327337';
 --