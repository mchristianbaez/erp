CREATE TABLE XXCUS.XXCUSOIE_TE_ACCRUAL_HIST_TBL
(
  ORACLE_ACCOUNTS           VARCHAR2(50 BYTE),
  ORACLE_PRODUCT            VARCHAR2(30 BYTE),
  ORACLE_LOCATION           VARCHAR2(30 BYTE),
  ORACLE_COST_CENTER        VARCHAR2(30 BYTE),
  ORACLE_ACCOUNT            VARCHAR2(30 BYTE),
  SEGMENT5                  VARCHAR2(30 BYTE),
  SEGMENT6                  VARCHAR2(30 BYTE),
  SEGMENT7                  VARCHAR2(30 BYTE),
  ITEM_DESCRIPTION          VARCHAR2(250 BYTE),
  LINE_AMOUNT               NUMBER,
  CURRENCY_CODE             VARCHAR2(50 BYTE),
  FULL_NAME                 VARCHAR2(250 BYTE),
  EMPLOYEE_NUMBER           VARCHAR2(30 BYTE),
  EMP_DEFAULT_PROD          VARCHAR2(30 BYTE),
  EMP_DEFAULT_LOC           VARCHAR2(30 BYTE),
  EMP_DEFAULT_COSTCTR       VARCHAR2(30 BYTE),
  FRU                       VARCHAR2(50 BYTE),
  MERCHANT_NAME             VARCHAR2(250 BYTE),
  CREDIT_CARD_TRX_ID        NUMBER,
  TRANSACTION_DATE          DATE,
  CARD_PROGRAM_ID           NUMBER,
  CARD_PROGRAM_NAME         VARCHAR2(250 BYTE),
  MCC_CODE_NO               VARCHAR2(250 BYTE),
  MCC_CODE                  VARCHAR2(250 BYTE),
  IMPORTED_TO_AP            VARCHAR2(10 BYTE),
  IMPORTED_TO_GL            VARCHAR2(10 BYTE),
  QUERY_NUM                 VARCHAR2(10 BYTE),
  QUERY_DESCR               VARCHAR2(250 BYTE),
  PARM_VALUE                VARCHAR2(100 BYTE),
  EXPENSE_REPORT_STATUS     VARCHAR2(30 BYTE),
  START_EXPENSE_DATE        DATE,
  END_EXPENSE_DATE          DATE,
  ACCRUAL_PERIOD            VARCHAR2(10 BYTE),
  EXPENSE_REPORT_NUMBER     NUMBER,
  DISTRIBUTION_LINE_NUMBER  NUMBER
)
TABLESPACE XXCUS_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


