/*
History
 TMS                             ESMS         Ver    Author                       Comments
 ============================================================================================                  
                                              1.0   Balaguru Seshadri              Header missing      
 20160815-00072                  438366       1.1   Balaguru Seshadri              Add new columns       
*/
--
DROP TABLE XXCUS.XXCUSPN_RENT_TBL CASCADE CONSTRAINTS;
--
CREATE TABLE XXCUS.XXCUSPN_RENT_TBL
(
  LOB                VARCHAR2(10 BYTE),
  RER_ID             VARCHAR2(25 BYTE),
  PRIME_LOC          VARCHAR2(25 BYTE),
  PRIME_ADDRESS      VARCHAR2(150 BYTE),
  PRIME_CITY         VARCHAR2(100 BYTE),
  PRIME_ST_PRV       VARCHAR2(60 BYTE),
  POSTAL_CODE        VARCHAR2(10 BYTE),
  RER_TYPE           VARCHAR2(30 BYTE),
  COMM_DATE          DATE,
  EXP_DATE           DATE,
  RER_STATUS         VARCHAR2(30 BYTE),
  ORACLE_ID          VARCHAR2(25 BYTE),
  FRU_ID             VARCHAR2(4 BYTE),
  BR_#               VARCHAR2(6 BYTE),
  PAYMENT_TERM_TYPE  VARCHAR2(50 BYTE),
  YEAR               NUMBER,
  JANUARY            NUMBER,
  FEBRUARY           NUMBER,
  MARCH              NUMBER,
  APRIL              NUMBER,
  MAY                NUMBER,
  JUNE               NUMBER,
  JULY               NUMBER,
  AUGUST             NUMBER,
  SEPTEMBER          NUMBER,
  OCTOBER            NUMBER,
  NOVEMBER           NUMBER,
  DECEMBER           NUMBER,
  ANNUAL_TOTAL       NUMBER,
  START_DT           VARCHAR2(25 BYTE),
  VENDOR_CODE VARCHAR2(30), --Ver 1.1
  VENDOR_NAME VARCHAR2(150), --Ver 1.1
  FREQUENCY VARCHAR2(30), --Ver 1.1
  GL_CODE VARCHAR2(30), --Ver 1.1
  TERM_COMMENTS VARCHAR2(2000) --Ver 1.1
)
TABLESPACE XXCUS_DATA
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;
--
COMMENT ON TABLE XXCUS.XXCUSPN_RENT_TBL IS 'TMS 20160815-00072 / ESMS 438366';
--