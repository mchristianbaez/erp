CREATE TABLE XXCUS.XXCUSAP_AUDIT_FRT_PAY_EXT_TBL
(
  ID                 NUMBER                     NOT NULL,
  INVOICE_COUNT      NUMBER                     NOT NULL,
  AMOUNT             NUMBER                     NOT NULL,
  LAST_RUN_DATE      DATE                       NOT NULL,
  LAST_RUN_DURATION  INTERVAL DAY(2) TO SECOND(6) NOT NULL
)
TABLESPACE XXCUS_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

COMMENT ON TABLE XXCUS.XXCUSAP_AUDIT_FRT_PAY_EXT_TBL IS 'Table used to hold each scheduled execution record';

COMMENT ON COLUMN XXCUS.XXCUSAP_AUDIT_FRT_PAY_EXT_TBL.ID IS 'Unique record number';

COMMENT ON COLUMN XXCUS.XXCUSAP_AUDIT_FRT_PAY_EXT_TBL.INVOICE_COUNT IS 'Total invoice count for validation';

COMMENT ON COLUMN XXCUS.XXCUSAP_AUDIT_FRT_PAY_EXT_TBL.AMOUNT IS 'Total invoice amount for validation';

COMMENT ON COLUMN XXCUS.XXCUSAP_AUDIT_FRT_PAY_EXT_TBL.LAST_RUN_DATE IS 'Last run date to hold each execution date last ran';

COMMENT ON COLUMN XXCUS.XXCUSAP_AUDIT_FRT_PAY_EXT_TBL.LAST_RUN_DURATION IS 'Last execution run duration';



