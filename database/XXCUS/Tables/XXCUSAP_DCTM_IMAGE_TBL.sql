CREATE TABLE XXCUS.XXCUSAP_DCTM_IMAGE_TBL
(
  GROUP_ID       VARCHAR2(80 BYTE)              NOT NULL,
  INVOICE_NUM    VARCHAR2(50 BYTE)              NOT NULL,
  INVOICE_TYPE   VARCHAR2(25 BYTE)              NOT NULL,
  IMAGE_LINK     VARCHAR2(500 BYTE)             NOT NULL,
  ORG_ID         NUMBER(15),
  CREATION_DATE  DATE                           DEFAULT SYSDATE               NOT NULL,
  CREATED_BY     VARCHAR2(30 BYTE)              DEFAULT 'INTERFACE_ECM'       NOT NULL,
  UPDATE_DATE    DATE,
  UPDATED_BY     NUMBER(15),
  STATUS         VARCHAR2(15 BYTE)              DEFAULT 'NEW'                 NOT NULL
)
TABLESPACE XXCUS_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

COMMENT ON COLUMN XXCUS.XXCUSAP_DCTM_IMAGE_TBL.GROUP_ID IS 'Unique Identifier for batch being delivered that allows processing of the batch';

COMMENT ON COLUMN XXCUS.XXCUSAP_DCTM_IMAGE_TBL.INVOICE_TYPE IS 'Identifies where the image is to be attached';

COMMENT ON COLUMN XXCUS.XXCUSAP_DCTM_IMAGE_TBL.IMAGE_LINK IS 'URL for the image that will be attached';



