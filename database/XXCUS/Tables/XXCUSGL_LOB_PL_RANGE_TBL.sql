CREATE TABLE XXCUS.XXCUSGL_LOB_PL_RANGE_TBL
(
  LOB            VARCHAR2(100 BYTE),
  PNL_ORDER      NUMBER,
  PNL_HIERARCHY  VARCHAR2(50 BYTE),
  FROM_ACCOUNT   NUMBER,
  THRU_ACCOUNT   NUMBER,
  ROLLUP_LEVEL1  VARCHAR2(200 BYTE),
  ROLLUP_LEVEL2  VARCHAR2(200 BYTE)
)
TABLESPACE XXCUS_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


