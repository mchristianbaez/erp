CREATE TABLE XXCUS.XXCUS_REBATE_RECEIPT_CURR_TBL
(
  GEO_LOC_HIER_LOB_NM  VARCHAR2(50 BYTE)        NOT NULL,
  SRC_SYS_NM           VARCHAR2(30 BYTE),
  BU_NM                VARCHAR2(50 BYTE)        NOT NULL,
  BU_CD                VARCHAR2(50 BYTE)        NOT NULL,
  FISCAL_PER_ID        NUMBER,
  VNDR_RECPT_UNQ_NBR   VARCHAR2(100 BYTE)       NOT NULL,
  PT_VNDR_ID           NUMBER,
  PT_VNDR_CD           VARCHAR2(50 BYTE)        NOT NULL,
  SF_VNDR_ID           NUMBER                   NOT NULL,
  SF_VNDR_CD           VARCHAR2(50 BYTE)        NOT NULL,
  REBT_VNDR_ID         VARCHAR2(200 BYTE),
  REBT_VNDR_CD         VARCHAR2(50 BYTE),
  REBT_VNDR_NM         VARCHAR2(240 BYTE),
  PROD_ID              NUMBER                   NOT NULL,
  SKU_CD               VARCHAR2(100 BYTE)       NOT NULL,
  VNDR_PART_NBR        VARCHAR2(100 BYTE),
  SHP_TO_BRNCH_ID      NUMBER,
  SHP_TO_BRNCH_CD      VARCHAR2(50 BYTE),
  SHP_TO_BRNCH_NM      VARCHAR2(100 BYTE),
  PO_NBR               VARCHAR2(50 BYTE),
  PO_CRT_DT            DATE,
  RECPT_CRT_DT         DATE                     NOT NULL,
  DROP_SHP_FLG         VARCHAR2(1 BYTE),
  PROD_UOM_CD          VARCHAR2(30 BYTE),
  ITEM_COST_AMT        NUMBER(18,5)             NOT NULL,
  CURNC_FLG            VARCHAR2(3 BYTE)         NOT NULL,
  RECPT_QTY            NUMBER                   NOT NULL,
  VNDR_SHP_QTY         NUMBER,
  BUY_PKG_UNT_CNT      NUMBER,
  AS_OF_DT             DATE,
  REQUEST_ID           NUMBER,
  STATUS_FLAG          VARCHAR2(2 BYTE),
  PROCESS_DATE         DATE,
  RECEIPT              NUMBER,
  CUST_ACCOUNT_ID      NUMBER,
  PARTY_ID             NUMBER(15),
  PARTY_NAME           VARCHAR2(360 BYTE)
)
TABLESPACE XXCUS_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


