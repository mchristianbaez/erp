
  CREATE TABLE "XXCUS"."XXCUS_USABB_10NOV2014" 
   (	"GEO_LOC_HIER_LOB_NM" VARCHAR2(50), 
	"SRC_SYS_NM" VARCHAR2(30) NOT NULL ENABLE, 
	"BU_NM" VARCHAR2(50) NOT NULL ENABLE, 
	"BU_CD" VARCHAR2(50) NOT NULL ENABLE, 
	"FISCAL_PER_ID" NUMBER NOT NULL ENABLE, 
	"VNDR_RECPT_UNQ_NBR" VARCHAR2(100) NOT NULL ENABLE, 
	"PT_VNDR_ID" NUMBER NOT NULL ENABLE, 
	"PT_VNDR_CD" VARCHAR2(50) NOT NULL ENABLE, 
	"SF_VNDR_ID" NUMBER NOT NULL ENABLE, 
	"SF_VNDR_CD" VARCHAR2(50) NOT NULL ENABLE, 
	"REBT_VNDR_ID" VARCHAR2(200), 
	"REBT_VNDR_CD" VARCHAR2(50) NOT NULL ENABLE, 
	"REBT_VNDR_NM" VARCHAR2(100), 
	"PROD_ID" NUMBER NOT NULL ENABLE, 
	"SKU_CD" VARCHAR2(100) NOT NULL ENABLE, 
	"VNDR_PART_NBR" VARCHAR2(50), 
	"PART_DESC" VARCHAR2(150), 
	"SHP_TO_BRNCH_ID" NUMBER, 
	"SHP_TO_BRNCH_CD" VARCHAR2(50), 
	"SHP_TO_BRNCH_NM" VARCHAR2(100), 
	"PO_NBR" VARCHAR2(50), 
	"PO_CRT_DT" DATE, 
	"RECPT_CRT_DT" DATE NOT NULL ENABLE, 
	"DROP_SHP_FLG" VARCHAR2(1), 
	"PROD_UOM_CD" VARCHAR2(30), 
	"ITEM_COST_AMT" NUMBER(18,5), 
	"CURNC_FLG" VARCHAR2(3) NOT NULL ENABLE, 
	"RECPT_QTY" NUMBER, 
	"VNDR_SHP_QTY" NUMBER, 
	"BUY_PKG_UNT_CNT" NUMBER, 
	"AS_OF_DT" DATE
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "XXCUS_DATA" ;
