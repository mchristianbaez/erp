CREATE GLOBAL TEMPORARY TABLE XXCUS.XXCUSIE_FM_GL_GTT2
(
  DESCR         VARCHAR2(50 BYTE),
  REPORT_NUM    VARCHAR2(50 BYTE),
  SEGMENT1      VARCHAR2(25 BYTE),
  SEGMENT2      VARCHAR2(25 BYTE),
  SEGMENT3      VARCHAR2(25 BYTE),
  SEGMENT4      VARCHAR2(25 BYTE),
  SEGMENT5      VARCHAR2(25 BYTE),
  SEGMENT6      VARCHAR2(25 BYTE),
  SEGMENT7      VARCHAR2(25 BYTE),
  PERIOD_NAME   VARCHAR2(15 BYTE),
  ACCOUNTED_CR  NUMBER,
  ACCOUNTED_DR  NUMBER,
  EMPLOYEE_NUM  VARCHAR2(30 BYTE)
)
ON COMMIT PRESERVE ROWS;


