DROP TABLE XXCUS.XXCUS_ARTAX_EXTRACT_ERRORS_ALL;
CREATE TABLE XXCUS.XXCUS_ARTAX_EXTRACT_ERRORS_ALL
(
  RECORD_TYPE VARCHAR2(30)
 ,CUSTOMER_TRX_ID NUMBER
 ,TRX_NUMBER VARCHAR2(30)
 ,PERIOD_NAME VARCHAR2(20)
 ,REQUEST_ID NUMBER
);
COMMENT ON TABLE XXCUS.XXCUS_ARTAX_EXTRACT_ERRORS_ALL IS 'Incident: 575152';
