CREATE TABLE XXCUS.XXCUS_USR_LOGIN_TBL
(
  EMPLOYEE_NUMBER  VARCHAR2(50 BYTE),
  NT_LOGIN         VARCHAR2(50 BYTE)
)
TABLESPACE XXCUS_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


