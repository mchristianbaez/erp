--
CREATE TABLE XXCUS.XXCUS_OZF_SINGLE_DED_HDR_ALL
(
 invoice_num           VARCHAR2(30)
,invoice_date          DATE
,currency_code         VARCHAR2(10)
,invoice_amount        NUMBER
,ytd_rebate            NUMBER
,ytd_purchases         NUMBER
,ytd_collection_rate   NUMBER
,posting_lob           VARCHAR2(150)
,lob_id                VARCHAR(30)
,mvid_number           VARCHAR2(30)
,mvid_name             VARCHAR2(150)
,hds_divisions         VARCHAR2(240)
,payment_frequency     VARCHAR2(30)
,offer_name            VARCHAR2(240)
,hdr_customer_trx_id   NUMBER
,calendar_year         VARCHAR2(10)
,cust_account_id       NUMBER
,cust_trx_type_id      NUMBER
,list_header_id        NUMBER
,org_id                NUMBER
,print_flag            VARCHAR2(1)
,original_print_date   DATE
,request_id            NUMBER
,created_by            NUMBER
,creation_date         DATE
,reprint_user_id       NUMBER
,reprint_date          DATE
,reprint_count         NUMBER DEFAULT 0
);
--
CREATE UNIQUE INDEX XXCUS.XXCUS_OZF_SINGLE_DED_HDR_U1 ON XXCUS.XXCUS_OZF_SINGLE_DED_HDR_ALL (HDR_CUSTOMER_TRX_ID);
--
CREATE INDEX XXCUS.XXCUS_OZF_SINGLE_DED_HDR_N1 ON XXCUS.XXCUS_OZF_SINGLE_DED_HDR_ALL (REQUEST_ID, PRINT_FLAG);
--
COMMENT ON TABLE XXCUS.XXCUS_OZF_SINGLE_DED_HDR_ALL IS 'RFC 43035 / ESMS 269629';
--