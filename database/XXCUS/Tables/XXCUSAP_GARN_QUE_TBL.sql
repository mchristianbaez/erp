CREATE TABLE XXCUS.XXCUSAP_GARN_QUE_TBL
(
  IMPORT_GROUP_ID  VARCHAR2(60 BYTE),
  PS_BATCH         NUMBER,
  CREATED_DTTM     DATE,
  PROCESSED        VARCHAR2(10 BYTE),
  PROCESSED_DTTM   DATE,
  PULL_CNT         NUMBER
)
TABLESPACE XXCUS_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


