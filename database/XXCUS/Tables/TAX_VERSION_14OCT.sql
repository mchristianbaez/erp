CREATE TABLE XXCUS.TAX_VERSION_14OCT
(
  RECORD_NUM  NUMBER(15),
  LANGUAGE    VARCHAR2(50 BYTE),
  PRODUCT     VARCHAR2(75 BYTE),
  REL_TYPE    VARCHAR2(25 BYTE),
  REL_NUM     VARCHAR2(10 BYTE),
  VER_NUM     VARCHAR2(20 BYTE)                 NOT NULL,
  TIMESTAMP   DATE                              NOT NULL,
  NOTES       VARCHAR2(200 BYTE)
)
TABLESPACE XXCUS_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


