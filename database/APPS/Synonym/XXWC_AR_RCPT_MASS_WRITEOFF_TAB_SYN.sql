/*************************************************************************
    *   Synonym Name: XXWC_AR_RCPT_MASS_WRITEOFF_TAB_SYN
    *
    *   PURPOSE:   Create synonym
	
	HISTORY
    ===============================================================================
    VERSION DATE          AUTHOR(S)              DESCRIPTION
    ------- -----------   --------------- -----------------------------------------
    1.0     5/07/2018     Niraj K ranjan         TMS#20160218-00198   AR APEX project - Ability to apply payments in mass
*****************************************************************************/
CREATE SYNONYM XXWC_AR_RCPT_MASS_WRITEOFF_TBL FOR XXWC.XXWC_AR_RCPT_MASS_WRITEOFF_TBL;
CREATE SYNONYM XXWC_AR_RCPT_MASS_REAPPLY_TBL FOR XXWC.XXWC_AR_RCPT_MASS_REAPPLY_TBL;