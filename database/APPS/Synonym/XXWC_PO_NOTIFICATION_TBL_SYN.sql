/*************************************************************************
      Script : XXWC_PO_NOTIFICATION_TBL_SYN.sql

      PURPOSE:   To create synonym

      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------
      1.0        17/10/2017  Niraj K Ranjan          TMS#20170712-00193   Late PO notification
 ****************************************************************************/
 CREATE SYNONYM APPS.XXWC_PO_NOTIFICATION_TBL FOR XXWC.XXWC_PO_NOTIFICATION_TBL;