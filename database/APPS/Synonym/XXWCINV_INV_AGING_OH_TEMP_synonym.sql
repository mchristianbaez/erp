CREATE OR REPLACE SYNONYM APPS.XXWCINV_INV_AGING_OH_TEMP FOR XXWC.XXWCINV_INV_AGING_OH_TEMP
/***************************************************************************
    $Header APPS.XXWCINV_INV_AGING_OH_TEMP $
    Module Name: XXWCINV_INV_AGING_OH_TEMP
    PURPOSE: Apps Synonym for XXWC.XXWCINV_INV_AGING_OH_TEMP
 
    REVISIONS:
    Ver    Date       	Author                Description
    ------ ---------  	------------------    ----------------
     1.0    16-DEC-2015    Lee Spitzer         TMS # 20150928-00193 Branch Aging Report
/***************************************************************************/;