/******************************************************************************************************************
   NAME:       XXWC_AR_RCPT_SYN_SCRIPT.sql
   PURPOSE:    Script to create SYNONYM for conversion table XXWC_AR_RECEIPTS_CONV.
   
   REVISIONS:
   Ver        Date          Author            Description
   ---------  -----------  ---------------  ---------------------------------------------------------------------
   1.0        03-Apr-2018  Ashwin Sridhar     TMS#20180319-00242--AH HARRIS Customer Conversion
******************************************************************************************************************/
CREATE OR REPLACE SYNONYM APPS.XXWC_AR_RECEIPTS_CONV FOR XXWC.XXWC_AR_RECEIPTS_CONV;