   /******************************************************************************
      NAME:       XXWC_OCM_ORDER_HOLD_REL_TAB.sql
      PURPOSE:    Synonym for table XXWC_OCM_ORDER_HOLD_REL_TAB

      REVISIONS:
      Ver        Date        Author               Description
      ---------  ----------  ---------------  ------------------------------------
      1.0        01/30/2017     Niraj K Ranjan   Initial Version TMS#20150811-00089   
	                                             Credit - Remove capability of Branch 
												 Users to release Hard Hold or No Change option
   ******************************************************************************/
CREATE OR REPLACE SYNONYM XXWC_OCM_ORDER_HOLD_REL_TAB FOR XXWC.XXWC_OCM_ORDER_HOLD_REL_TAB;