/******************************************************************************
   NAME:       XXWC_CSP_DM_MAINTENANCE_TAB.sql
   PURPOSE:    Synonym for table XXWC_CSP_DM_MAINTENANCE_TAB
   
   REVISIONS:
   Ver        Date        Author               Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        09/06/2017  Niraj K Ranjan   Initial Version TMS#20150625-00057   
                                           CSP Enhance bundle - Item #4  CSP Approval Workflow
******************************************************************************/
CREATE OR REPLACE SYNONYM APPS.XXWC_CSP_DM_MAINTENANCE_TAB FOR XXWC.XXWC_CSP_DM_MAINTENANCE_TAB;