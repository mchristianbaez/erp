CREATE OR REPLACE SYNONYM APPS.XXWCINV_INV_AGING_BRANCH_TBL FOR XXWC.XXWCINV_INV_AGING_BRANCH_TBL
/***************************************************************************
    $Header APPS.XXWCINV_INV_AGING_BRANCH_TBL $
    Module Name: XXWCINV_INV_AGING_BRANCH_TBL
    PURPOSE: Apps Synonym for XXWC.XXWCINV_INV_AGING_BRANCH_TBL
 
    REVISIONS:
    Ver    Date       	Author                Description
    ------ ---------  	------------------    ----------------
     1.0    16-DEC-2015    Lee Spitzer         TMS # 20150928-00193 Branch Aging Report
/***************************************************************************/;