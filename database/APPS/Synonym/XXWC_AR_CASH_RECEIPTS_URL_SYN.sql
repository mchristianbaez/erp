 /**********************************************************************************
  -- Synonym Script: XXWC_AR_CASH_RECEIPTS_URL_SYN.sql
  -- *******************************************************************************
  --
  -- PURPOSE: Synonym for table XXWC.XXWC_AR_CASH_RECEIPTS_URL_STG and XXWC.XXWC_AR_CASH_RECEIPTS_ALL_BKP
  -- HISTORY
  -- ===============================================================================
  -- ===============================================================================
  -- VERSION DATE          AUTHOR(S)       DESCRIPTION
  -- ------- -----------   --------------- -----------------------------------------
  -- 1.0     12-Sep-2016   Niraj K Ranjan   Initial Version - TMS#20150903-00017   IT - data script for new URL on lockbox receipts
************************************************************************************/
CREATE OR REPLACE SYNONYM XXWC_AR_CASH_RECEIPTS_ALL_BKP FOR XXWC.XXWC_AR_CASH_RECEIPTS_ALL_BKP;
CREATE OR REPLACE SYNONYM XXWC_AR_CASH_RECEIPTS_URL_STG FOR XXWC.XXWC_AR_CASH_RECEIPTS_URL_STG;
CREATE OR REPLACE SYNONYM XXWC_AR_CASH_RCPT_TBL_TMP     FOR XXWC.XXWC_AR_CASH_RCPT_TBL_TMP;