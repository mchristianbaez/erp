DROP MATERIALIZED VIEW APPS.XXWC_OBIEE_CT__CTL_MV;
CREATE MATERIALIZED VIEW APPS.XXWC_OBIEE_CT__CTL_MV (CREATED_BY,CREATION_DATE,CUSTOMER_TRX_ID,CUSTOMER_TRX_LINE_ID,DESCRIPTION,EXTENDED_AMOUNT,INTERFACE_LINE_ATTRIBUTE6,INTERFACE_LINE_CONTEXT,ACTUAL_INVENTORY_ITEM_ID,LAST_UPDATE_DATE,LAST_UPDATED_BY,LINE_NUMBER,LINE_TYPE,LINK_TO_CUST_TRX_LINE_ID,ORG_ID,QUANTITY_CREDITED,QUANTITY_INVOICED,QUANTITY_ORDERED,SALES_ORDER,SALES_ORDER_DATE,UNIT_SELLING_PRICE,UOM_CODE,WAREHOUSE_ID,TAX_AMOUNT,RN)
BUILD IMMEDIATE
REFRESH COMPLETE ON DEMAND
WITH PRIMARY KEY
AS 
SELECT /* sub-MV used by APPS.XXWC_OBIEE_CUST_TRANS_INCR_MV
       -- 1. This is the main source of the transaction lines w line_type ='LINE'
          Incremental date range filter will be applied in the main MV
          2. replacement for call to
           apps.xxwc_mv_routines_pkg.get_tax_amount (sq_ra_cust_trx_lines_all.customer_trx_id
                                                   , sq_ra_cust_trx_lines_all.customer_trx_line_id)
           The originally used function (get_tax_amount) doesn't limit the ar.ra_customer_trx_lines_all
           records by date range which created the need to pull 100% of the 'LINE' and 'TAX' records
       --
       -- This MV will be refreshed before the main query in
       -- XXWC_OBIEE_CUST_TRANS_INCR_MV
       */
      created_by
      ,creation_date
      ,customer_trx_id
      ,customer_trx_line_id
      ,description
      ,extended_amount
      ,interface_line_attribute6
      ,interface_line_context
      ,inventory_item_id AS actual_inventory_item_id
      ,last_update_date
      ,last_updated_by
      ,line_number
      ,line_type
      ,link_to_cust_trx_line_id
      ,org_id
      ,quantity_credited
      ,quantity_invoiced
      ,quantity_ordered
      ,sales_order
      ,sales_order_date
      ,unit_selling_price
      ,uom_code
      ,warehouse_id
      ,CASE
          WHEN line_type = 'TAX'
          THEN
             SUM (extended_amount) OVER (PARTITION BY customer_trx_id, link_to_cust_trx_line_id)
          ELSE
             0
       END
          AS tax_amount
      ,ROW_NUMBER () OVER (PARTITION BY customer_trx_id, link_to_cust_trx_line_id ORDER BY ROWNUM) rn
  FROM ar.ra_customer_trx_lines_all
 WHERE line_type IN ('LINE', 'TAX');


COMMENT ON MATERIALIZED VIEW APPS.XXWC_OBIEE_CT__CTL_MV IS 'snapshot table for snapshot APPS.XXWC_OBIEE_CT__CTL_MV';
