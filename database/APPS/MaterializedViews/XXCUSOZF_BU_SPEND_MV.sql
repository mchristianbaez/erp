  -- ------------------------------------------------------------------------------
  -- Copyright 2010 HD Supply  Inc (Orlando, FL) - All rights reserved
  --
  --
  --    NAME:       XXCUS_TM_MISC_PKG.pck
  --
  --    REVISIONS:
  --    Ver        Date        Author                 Description
  --    ---------  ----------  ---------------        -------------------------------
  --    1.0        04/12/2014  Balguru Sheshadri      Initial Build           
  --    1.1        10/29/2018  Ashwin Sridhar         TMS#20180510-00245 Collection Report update collector by BU 
  --================================================================================
  CREATE MATERIALIZED VIEW "APPS"."XXCUSOZF_BU_SPEND_MV" ("SPEND", "END_CUST_PARTY_ID", "SOLD_FROM_CUST_ACCOUNT_ID")
  ORGANIZATION HEAP PCTFREE 10 PCTUSED 0 INITRANS 2 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 131072 NEXT 131072 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "APPS_TS_TX_DATA" 
  BUILD IMMEDIATE
  USING INDEX 
  REFRESH COMPLETE ON DEMAND START WITH sysdate+0 NEXT SYSDATE + 7
  USING DEFAULT LOCAL ROLLBACK SEGMENT
  USING ENFORCED CONSTRAINTS DISABLE QUERY REWRITE
  AS SELECT SUM (orl.quantity * orl.selling_price) spend,
         orl.end_cust_party_id,
         orl.sold_from_cust_account_id
    FROM ozf_resale_lines_all orl
   WHERE     1 = 1
        -- AND orl.date_ordered BETWEEN ADD_MONTHS (SYSDATE, -6) AND SYSDATE --Commented by Ashwin.S on 29-Oct-2018 for TMS#20180510-00245
   AND orl.date_ordered BETWEEN ADD_MONTHS (SYSDATE, NVL(FND_PROFILE.VALUE('XXCUSOZF_MV_MONTHS'),-6)) AND SYSDATE         --Added by Ashwin.S on 29-Oct-2018 for TMS#20180510-00245
GROUP BY orl.end_cust_party_id, orl.sold_from_cust_account_id;

   COMMENT ON MATERIALIZED VIEW "APPS"."XXCUSOZF_BU_SPEND_MV"  IS 'snapshot table for snapshot APPS.XXCUSOZF_BU_SPEND_MV';
