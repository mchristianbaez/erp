--DROP MATERIALIZED VIEW APPS.XXWC_B2B_CAT_LIST_MV;
CREATE MATERIALIZED VIEW APPS.XXWC_B2B_CAT_LIST_MV
refresh force on demand
start with to_date('02-06-2016', 'dd-mm-yyyy') next trunc(SYSDATE)+1  
as
/*************************************************************************
 Copyright (c) 2012 HD Supply
 All rights reserved.
**************************************************************************
   $Header XXWC_B2B_CAT_LIST_MV$
  Module Name: XXWC_B2B_CAT_LIST_MV

  PURPOSE:

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)   TMS TASK        DESCRIPTION
  -- ------- -----------   ------------ ---------      -------------------------
  -- 1.0     26-Mar-2016   Pahwa, Nancy                Initially Created 
TMS# 20160223-00029   
**************************************************************************/
SELECT distinct CATALOG , item_catalog_group_id
FROM XXWC_B2B_CAT_LIST_VW;
/