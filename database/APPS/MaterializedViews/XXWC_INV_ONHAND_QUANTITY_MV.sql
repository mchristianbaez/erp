--
-- XXWC_INV_ONHAND_QUANTITY_MV  (Materialized View) 
--
CREATE MATERIALIZED VIEW APPS.XXWC_INV_ONHAND_QUANTITY_MV (BUSINESS_UNIT,SOURCE_SYSTEM,OPERATING_UNIT_ID,OPERATING_UNIT_NAME,INVENTORY_ITEM_ID,ORGANIZATION_ID,INTERFACE_DATE,BRANCH_NAME,BRANCH_LOC_NUM,BU_SKU,LONG_DESCRIPTION,SHORT_DESCRIPTION,ORIGINDATE,SNAPSHOTDATE,MOVING_AVG_COST,ONHAND_QTY,ON_ORDER_QTY,SERVICE_QTY,TRANSIT_QTY,AVAILABLE_QTY,CUST_CONSIGNED_QTY,VENDOR_CONSIGNED_QTY,REVIEW_QTY,DEFECTIVE_QTY,OVERSHIP_QTY,DISPLAY_QTY,STOCK_QTY)
TABLESPACE APPS_TS_TX_DATA
PCTUSED    0
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             128K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
BUILD IMMEDIATE
REFRESH COMPLETE ON DEMAND
WITH PRIMARY KEY
AS 
SELECT x1.business_unit
      ,x1.source_system
      ,x1.operating_unit_id
      ,x1.operating_unit_name
      ,x1.inventory_item_id
      ,x1.organization_id
      ,x1.interface_date
      ,x1.branch_name
      ,x1.branch_loc_num
      ,x1.bu_sku
      ,x1.long_description
      ,x1.short_description
      ,x1.origindate
      ,x1.snapshotdate
      ,ROUND (x1.moving_avg_cost, 2) moving_avg_cost
      ,ROUND (x1.onhand_qty, 2) onhand_qty
      ,ROUND (
          xxwc_mv_routines_pkg.get_open_po_qty (x1.inventory_item_id
                                               ,x1.organization_id)
         ,2)
          on_order_qty
      ,ROUND (
          xxwc_mv_routines_pkg.get_backordered_so_item_qty (
             x1.inventory_item_id
            ,x1.organization_id)
         ,2)
          service_qty
      ,ROUND (
          xxwc_mv_routines_pkg.get_transit_qty (x1.inventory_item_id
                                               ,x1.organization_id)
         ,2)
          transit_qty
      ,ROUND (xxwc_mv_routines_pkg.get_onhand_qty (x1.inventory_item_id
                                                  ,x1.organization_id
                                                  ,NULL
                                                  ,'ATR')
             ,2)
          available_qty
      ,0 cust_consigned_qty
      ,ROUND (
          xxwc_mv_routines_pkg.get_vendor_consigned_qty (x1.inventory_item_id
                                                        ,x1.organization_id)
         ,2)
          vendor_consigned_qty
      ,0 review_qty
      ,ROUND (xxwc_mv_routines_pkg.get_onhand_qty (x1.inventory_item_id
                                                  ,x1.organization_id
                                                  ,'Damage RTV'
                                                  ,'QOH')
             ,2)
          defective_qty
      ,ROUND ( (CASE
                   WHEN xxwc_mv_routines_pkg.get_onhand_qty (
                           x1.inventory_item_id
                          ,x1.organization_id
                          ,NULL
                          ,'QOH') < 0
                   THEN
                      xxwc_mv_routines_pkg.get_onhand_qty (
                         x1.inventory_item_id
                        ,x1.organization_id
                        ,NULL
                        ,'QOH')
                   ELSE
                      0
                END)
             ,2)
          overship_qty
      ,0 display_qty
      ,ROUND ( (CASE
                   WHEN NVL (xxwc_mv_routines_pkg.get_item_category (
                                x1.inventory_item_id
                               ,x1.organization_id
                               ,'Sales Velocity'
                               ,1)
                            ,'X') != 'N'
                   THEN
                      xxwc_mv_routines_pkg.get_onhand_qty (
                         x1.inventory_item_id
                        ,x1.organization_id
                        ,'General'
                        ,'QOH')
                   ELSE
                      0
                END)
             ,2)
          stock_qty
  FROM (  SELECT 'WHITECAP' business_unit
                ,'Oracle EBS' source_system
                ,hou.organization_id operating_unit_id
                ,hou.name operating_unit_name
                ,moqd.inventory_item_id
                ,moqd.organization_id
                ,SYSDATE interface_date
                ,ood.organization_name branch_name
                ,mp.organization_code branch_loc_num
                ,msib.segment1 bu_sku
                ,msit.long_description
                ,msib.description short_description
                ,msib.creation_date origindate
                ,SYSDATE snapshotdate
                ,cst_cost_api.get_item_cost (1.0
                                            ,moqd.inventory_item_id
                                            ,moqd.organization_id)
                    moving_avg_cost
                ,SUM (moqd.primary_transaction_quantity) onhand_qty
            FROM mtl_onhand_quantities_detail moqd
                ,mtl_system_items_b msib
                ,mtl_system_items_tl msit
                ,mtl_parameters mp
                ,org_organization_definitions ood
                ,hr_operating_units hou
           WHERE     moqd.organization_id = mp.organization_id
                 AND moqd.inventory_item_id = msib.inventory_item_id
                 AND moqd.organization_id = msib.organization_id
                 AND moqd.inventory_item_id = msit.inventory_item_id
                 AND moqd.organization_id = msit.organization_id
                 AND mp.organization_id = ood.organization_id
                 AND ood.operating_unit = hou.organization_id
        GROUP BY 'WHITECAP'
                ,'Oracle EBS'
                ,hou.organization_id
                ,hou.name
                ,moqd.inventory_item_id
                ,moqd.organization_id
                ,SYSDATE
                ,ood.organization_name
                ,mp.organization_code
                ,msib.segment1
                ,msit.long_description
                ,msib.description
                ,msib.creation_date
                ,SYSDATE
                ,cst_cost_api.get_item_cost (1.0
                                            ,moqd.inventory_item_id
                                            ,moqd.organization_id)) x1;


COMMENT ON MATERIALIZED VIEW APPS.XXWC_INV_ONHAND_QUANTITY_MV IS 'snapshot table for snapshot APPS.XXWC_INV_ONHAND_QUANTITY_MV';
