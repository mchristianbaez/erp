Drop MATERIALIZED VIEW APPS.XXWC_COMM_APP_AR_INVOICES_MV;
--
-- XXWC_COMM_APP_AR_INVOICES_MV  (Materialized View) 
--
CREATE MATERIALIZED VIEW "APPS"."XXWC_COMM_APP_AR_INVOICES_MV" ("OPERATING_UNIT_ID", "OPERATING_UNIT_NAME", "CUSTOMER_TRX_ID", "ORDER_NUMBER", "INVOICE_NUMBER", "ACCOUNT_NUMBER", "BILL_TO_JOB_NUMBER", "SUBORDER_NUMBER", "SALESREP_NUMBER", "SALESREP_NAME", "PURCHASE_ORDER", "PAYMENT_TERM", "TRX_DATE", "GL_DATE", "TRX_CREATION_DATE", "ITEM_SALES_TOTAL", "TAX_ORIGINAL", "FREIGHT_ORIGINAL", "INVOICE_TOTAL", "PRISM_COST", "LINE_COST", "BRANCH_COST", "GROSS_MARGIN", "GP_PERCENT", "BATCH_SOURCE")
BUILD IMMEDIATE
REFRESH COMPLETE ON DEMAND
WITH PRIMARY KEY
AS
SELECT x1.operating_unit_id,
       x1.operating_unit_name,
       x1.customer_trx_id,
       x1.order_number,
       x1.invoice_number,
       x1.account_number,
       x1.bill_to_job_number,
       (x1.order_number || '-' || x1.invoice_number) suborder_number,
       x1.salesrep_number,
       x1.salesrep_name,
       x1.purchase_order,
       x1.payment_term,
       x1.trx_date,
       x1.gl_date,
       x1.trx_creation_date,
       x1.amount_line_items_original item_sales_total,
       x1.tax_original,
       x1.freight_original,
       x1.amount_due_original invoice_total,
       x1.prism_cost,
       SUM(x1.line_cost) line_cost,
       ROUND((CASE
               WHEN x1.batch_source IN ('PRISM', 'PRISM REFUND', 'CONVERSION') THEN
                x1.prism_cost
               ELSE
                SUM(x1.line_cost)
             END),
             2) branch_cost,
       ROUND((CASE
               WHEN x1.batch_source IN ('PRISM', 'PRISM REFUND', 'CONVERSION') THEN
                (x1.amount_line_items_original - x1.prism_cost)
               ELSE
                (x1.amount_line_items_original - SUM(x1.line_cost))
             END),
             2) gross_margin,
       ROUND((CASE
               WHEN x1.batch_source IN ('PRISM', 'PRISM REFUND', 'CONVERSION') THEN
                (((x1.amount_line_items_original - x1.prism_cost) /
                DECODE(x1.amount_line_items_original,
                         0,
                         1,
                         x1.amount_line_items_original)) * 100)
               ELSE
                (((x1.amount_line_items_original - SUM(x1.line_cost)) /
                DECODE(x1.amount_line_items_original,
                         0,
                         1,
                         x1.amount_line_items_original)) * 100)
             END),
             2) gp_percent,
       x1.batch_source
  FROM (SELECT hou.organization_id operating_unit_id,
               hou.name operating_unit_name,
               rcta.customer_trx_id,
               (CASE
                 WHEN rbsa.name IN ('ORDER MANAGEMENT',
                                    'ORDER ENTRY',
                                    'REPAIR OM SOURCE',
                                    'STANDARD OM SOURCE',
                                    'CONVERSION') THEN
                  NVL(rctla.sales_order, rctla.interface_line_attribute1)
                 WHEN rbsa.name IN ('PRISM', 'PRISM REFUND') THEN
                  NVL(rctla.sales_order, rctla.interface_line_attribute15)
                 ELSE
                  NVL(rctla.sales_order, rctla.interface_line_attribute1) -- may need to reconsider
               END) order_number,
               rcta.trx_number invoice_number,
               hca.account_number, --NULL suborder_number,
               apsa.amount_line_items_original,
               apsa.tax_original,
               apsa.freight_original,
               apsa.amount_due_original,
               rctla.line_type,
               rctla.line_number,
               rctla.inventory_item_id,
               rctla.unit_selling_price,
               NVL(NVL(rctla.quantity_invoiced, rctla.quantity_credited),
                   rctla.quantity_ordered) invoice_quantity,
               rctla.extended_amount,
               (CASE
                 WHEN rbsa.name IN ('ORDER MANAGEMENT',
                                    'ORDER ENTRY',
                                    'REPAIR OM SOURCE',
                                    'STANDARD OM SOURCE') THEN
                  NVL(
                      -- 07/27/2012 CG: Changed to use function to pull line cost
                      /*(SELECT (  unit_cost
                         * NVL
                          (NVL
                            (rctla.quantity_invoiced,
                            rctla.quantity_credited
                           ),
                            rctla.quantity_ordered
                           )
                          )
                       FROM oe_order_lines_all
                      WHERE TO_CHAR (line_id) =
                         rctla.interface_line_attribute6),*/
                      -- 08/28/2012 CG: Changed item cost pull to match EDW/Order line query
                      -- for vendor quote and engineering charges
                      /*( xxwc_mv_routines_pkg.get_order_line_cost
                      (TO_NUMBER
                        (rctla.interface_line_attribute6)
                      )*/
                      -- 08/31/2012 CG: Removed conditions for Engineering items
                      -- they will behave as standard items with B2B POs
                      (ROUND((NVL(xxwc_mv_routines_pkg.get_vendor_quote_cost(TO_NUMBER(rctla.interface_line_attribute6)),
                                  NVL(xxwc_mv_routines_pkg.get_order_line_cost(TO_NUMBER(rctla.interface_line_attribute6)),
                                      0))),
                             2) * NVL(NVL(rctla.quantity_invoiced,
                                           rctla.quantity_credited),
                                       rctla.quantity_ordered)),
                      0)
                 ELSE
                  0
               END) line_cost,
               (CASE
                 WHEN rbsa.name IN ('PRISM', 'PRISM REFUND', 'CONVERSION') THEN
                 -- TMS 20150818-00010 change implemented by Nancy remove this old code on 07/23/2015  
                 /*  NVL (
                    (SELECT   net_sales_cost
                     FROM   xxwc.xxwc_ar_prism_cls_ord_summ_stg
                    WHERE     trx_number = rcta.trx_number
                        AND NVL (net_sales_cost, 0) != 0
                        AND ROWNUM = 1)
                  , 0
                 )*/
                 --end remove and add new code
                  APPS.XXWC_COMM_APP_AR_INVOICES_FNC(rcta.trx_number) --new code added on 07/23/2015
                 ELSE
                  0
               END) prism_cost
               -- NULL gp_percent,
               -- 10/03/12 CG: Modified to accomodate Prism House Account Changes
               -- , NVL (jrs.salesrep_number, 'No Rep') salesrep_number
              ,
               (CASE
                 WHEN rbsa.name LIKE 'PRISM%' THEN
                  NVL(xxwc_mv_routines_pkg.get_prism_inv_house_rep(rcta.trx_number),
                      NVL(jrs.salesrep_number, 'No Rep'))
                 ELSE
                  NVL(jrs.salesrep_number, 'No Rep')
               END) salesrep_number
               --, NVL (xxwc_mv_routines_pkg.get_salesrep_info (jrs.salesrep_number), 'No Rep') salesrep_name
              ,
               (CASE
                 WHEN rbsa.name LIKE 'PRISM%' THEN
                  NVL(xxwc_mv_routines_pkg.get_salesrep_info(NVL(xxwc_mv_routines_pkg.get_prism_inv_house_rep(rcta.trx_number),
                                                                 jrs.salesrep_number)),
                      'No Rep')
                 ELSE
                  NVL(xxwc_mv_routines_pkg.get_salesrep_info(jrs.salesrep_number),
                      'No Rep')
               END) salesrep_name
               -- 10/03/12 CG:
              , -- 08/17/2012 CG: Changed to be creation date
               -- rcta.trx_date,
               (CASE
                 WHEN (rbsa.name LIKE 'PRISM%' OR rbsa.name IN ('CONVERSION')) THEN
                  (TRUNC(rcta.creation_date) - 1)
                 ELSE
                  TRUNC(rcta.creation_date)
               END) trx_date,
               apsa.gl_date,
               rcta.purchase_order,
               rtt.description payment_term,
               hps_bill.party_site_number bill_to_job_number,
               hps_bill.party_site_name bill_to_job_name,
               hcsua_bill.location bill_to_job_description,
               rcta.creation_date trx_creation_date,
               rbsa.name batch_source,
               rcta.interface_header_context,
               rctla.interface_line_context,
               rctla.interface_line_attribute1,
               rctla.interface_line_attribute6,
               rctla.interface_line_attribute15,
               rcta.bill_to_customer_id bill_to_cust_acct_id,
               rcta.sold_to_customer_id sold_to_cust_acct_id,
               rcta.ship_to_customer_id ship_to_cust_acct_id
          FROM ra_customer_trx_all       rcta,
               ra_customer_trx_lines_all rctla,
               hr_operating_units        hou,
               ar_payment_schedules_all  apsa,
               hz_cust_site_uses_all     hcsua_bill,
               hz_cust_acct_sites_all    hcasa_bill,
               hz_party_sites            hps_bill,
               hz_cust_accounts          hca,
               ra_terms_tl               rtt,
               jtf.jtf_rs_salesreps      jrs, -- jtf_rs_defresources_v jrre,
               -- hr.per_all_people_f papf,
               ra_batch_sources_all rbsa
         WHERE rctla.customer_trx_id = rcta.customer_trx_id --AND rctla.org_id  = rcta.org_id
           AND rctla.line_type = 'LINE'
           -- TMS 20150818-00010 comment out old code  on 07/23/2015 by Nancy Pahwa
           /* AND TRUNC (rcta.creation_date) >=
               (TRUNC (SYSDATE)
              - fnd_profile.VALUE (
                 'XXWC_COMM_APP_INV_FROM_INV_DAYS'
                ))
           AND TRUNC (rcta.creation_date) <=
               (TRUNC (SYSDATE)
              - fnd_profile.VALUE (
                 'XXWC_COMM_APP_INV_TO_INV_DAYS'
                ))*/
           -- old code end
            --new code added on 07/23/2015
           AND rcta.creation_date between
               trunc(SYSDATE) -
               fnd_profile.VALUE('XXWC_COMM_APP_INV_FROM_INV_DAYS') and
               trunc(SYSDATE) -
               fnd_profile.VALUE('XXWC_COMM_APP_INV_TO_INV_DAYS')
          --new code end
           AND rcta.org_id = hou.organization_id
           AND hou.name = 'HDS White Cap - Org'
              -- Payment Schedule
           AND rcta.customer_trx_id = apsa.customer_trx_id
           AND rcta.org_id = apsa.org_id
           AND rcta.bill_to_site_use_id = hcsua_bill.site_use_id
           AND hcsua_bill.cust_acct_site_id = hcasa_bill.cust_acct_site_id
           AND hcasa_bill.party_site_id = hps_bill.party_site_id
           AND rcta.bill_to_customer_id = hca.cust_account_id
              -- Payment Term
           AND rcta.term_id = rtt.term_id(+)
           AND rtt.language(+) = 'US'
              -- Sales Rep Data
           AND rcta.primary_salesrep_id = jrs.salesrep_id(+)
           AND rcta.org_id = jrs.org_id(+)
              -- AND  jrs.resource_id = jrre.resource_id(+)
              -- AND  jrs.person_id = papf.person_id(+)
              -- AND  TRUNC (papf.effective_start_date(+)) <= TRUNC (SYSDATE)
              -- AND  NVL (TRUNC (papf.effective_end_date(+)), TRUNC (SYSDATE)) >= TRUNC (SYSDATE)
              -- Batch Source
           AND rcta.batch_source_id = rbsa.batch_source_id
           AND rcta.org_id = rbsa.org_id) x1
 GROUP BY x1.operating_unit_id,
          x1.operating_unit_name,
          x1.customer_trx_id,
          x1.order_number,
          x1.invoice_number,
          x1.account_number,
          x1.bill_to_job_number,
          (x1.order_number || '-' || x1.invoice_number),
          x1.salesrep_number,
          x1.salesrep_name,
          x1.purchase_order,
          x1.payment_term,
          x1.trx_date,
          x1.gl_date,
          x1.trx_creation_date,
          x1.amount_line_items_original,
          x1.tax_original,
          x1.freight_original,
          x1.amount_due_original,
          x1.prism_cost,
          x1.batch_source;
  CREATE INDEX "APPS"."XXWC_COMM_APP_AR_INV_MV_N1" ON "APPS"."XXWC_COMM_APP_AR_INVOICES_MV" ("TRX_CREATION_DATE") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 131072 NEXT 131072 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "APPS_TS_TX_DATA" ;
  CREATE INDEX "APPS"."XXWC_COMM_APP_AR_INV_MV_N2" ON "APPS"."XXWC_COMM_APP_AR_INVOICES_MV" ("INVOICE_NUMBER") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS NOLOGGING 
  STORAGE(INITIAL 131072 NEXT 131072 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "APPS_TS_TX_DATA" ;
  COMMENT ON MATERIALIZED VIEW "APPS"."XXWC_COMM_APP_AR_INVOICES_MV"  IS 'snapshot table for snapshot APPS.XXWC_COMM_APP_AR_INVOICES_MV';
/