
  CREATE MATERIALIZED VIEW "APPS"."XXCUSOZF_RBTINCOME_MV" ("MVID", "LOB_ID", "BU_ID", "FY_PERIOD_ID", "FISCAL_YEAR", "CALENDAR_YEAR", "CY_PERIOD_ID", "ACCRUED_AMOUNT", "ACCRUED_AMOUNT_USD", "GRP_MVID", "GRP_LOB", "GRP_BU", "GRP_FY_PERIOD", "GRP_FY_YEAR", "GRP_CY_PERIOD", "GRP_CY_YEAR")
  ORGANIZATION HEAP PCTFREE 10 PCTUSED 0 INITRANS 2 MAXTRANS 255 NOCOMPRESS NOLOGGING
  STORAGE(INITIAL 2097152 NEXT 2097152 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "APPS_TS_TX_DATA" 
  BUILD IMMEDIATE
  USING INDEX 
  REFRESH COMPLETE ON DEMAND
  USING DEFAULT LOCAL ROLLBACK SEGMENT
  USING ENFORCED CONSTRAINTS DISABLE QUERY REWRITE
  AS SELECT /*+ PARALLEL(ofu 8) PARALLEL(orl 8) */
         ofu.cust_account_id mvid,
         orl.bill_to_party_id lob_id,
         orl.end_cust_party_id bu_id,
         t.ent_period_id fy_period_id,
         t.ent_year_id fiscal_year,
         TO_CHAR (t.report_date, 'YYYY') calendar_year,
         t.month_id cy_period_id,
         SUM (acctd_amount) accrued_amount,
         sum(ofu.univ_curr_amount) accrued_amount_usd,
         GROUPING (ofu.cust_account_id) grp_mvid,
         GROUPING (orl.bill_to_party_id) grp_lob,
         GROUPING (orl.end_cust_party_id) grp_bu,
         GROUPING (t.ent_period_id) grp_fy_period,
         GROUPING (t.ent_year_id) grp_fy_year,
         GROUPING (t.month_id) grp_cy_period,
         GROUPING (TO_CHAR (t.report_date, 'YYYY')) grp_cy_year
    FROM ozf_funds_utilized_all_b ofu, ozf_resale_lines_all orl, ozf_time_day t
   WHERE     1 = 1
         AND t.ent_year_id >= TO_CHAR (SYSDATE, 'YYYY') - 3
         AND t.report_date = TRUNC (ofu.gl_date)
         AND orl.resale_line_id = ofu.object_id
         AND ofu.gl_posted_flag = 'Y'
GROUP BY CUBE (ofu.cust_account_id,
               orl.bill_to_party_id,
               orl.end_cust_party_id,
               t.ent_period_id,
               t.ent_year_id,
               t.month_id,
               TO_CHAR (t.report_date, 'YYYY'));
