
  CREATE MATERIALIZED VIEW "APPS"."XXCUSOZF_PURCHASES_MV" ("MVID", "LOB_ID", "BU_ID", "BRANCH_ID", "PERIOD_ID", "QTR_ID", "YEAR_ID", "CALENDAR_YEAR", "CALENDAR_PERIOD", "TOTAL_PURCHASES", "GRP_MVID", "GRP_LOB", "GRP_BU_ID", "GRP_BRANCH", "GRP_PERIOD", "GRP_QTR", "GRP_YEAR", "GRP_CAL_YEAR", "GRP_CAL_PERIOD")
  ORGANIZATION HEAP PCTFREE 10 PCTUSED 0 INITRANS 2 MAXTRANS 255 NOCOMPRESS NOLOGGING
  STORAGE(INITIAL 4194304 NEXT 4194304 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "APPS_TS_TX_DATA" 
  PARALLEL 4 
  BUILD IMMEDIATE
  USING INDEX 
  REFRESH COMPLETE ON DEMAND
  USING DEFAULT LOCAL ROLLBACK SEGMENT
  USING ENFORCED CONSTRAINTS DISABLE QUERY REWRITE
  AS SELECT orl.sold_from_cust_account_id mvid,
          orl.bill_to_party_id lob_id,
          orl.end_cust_party_id bu_id,
          orl.bill_to_cust_account_id branch_id,
          period.ent_period_id period_id,
          qtr.ent_qtr_id qtr_id,
          year.ent_year_id year_id,
          TO_CHAR (t.report_date, 'YYYY') calendar_year,
          t.month_id calendar_period,
         SUM (orl.selling_price * orl.quantity) total_purchases,
         GROUPING (orl.sold_from_cust_account_id) grp_mvid,
         GROUPING (orl.bill_to_party_id) grp_lob,
         GROUPING (orl.end_cust_party_id) grp_bu_id,
         GROUPING (orl.bill_to_cust_account_id) grp_branch,
         GROUPING (period.ent_period_id) grp_period,
         GROUPING (qtr.ent_qtr_id) grp_qtr,
         GROUPING (year.ent_year_id) grp_year,
         GROUPING (TO_CHAR (t.report_date, 'YYYY')) grp_cal_year,
         GROUPING (t.month_id) grp_cal_period
    FROM ozf_resale_lines_all orl,
         ozf_time_ent_period period,
         ozf_time_ent_qtr qtr,
         ozf_time_ent_year year,
         ozf_time_day t
   WHERE     1 = 1
         AND t.ent_year_id >= 2012
         AND t.ent_year_id >= TO_CHAR (SYSDATE, 'YYYY') - 3
         AND t.report_date = TRUNC (orl.date_ordered)
         AND t.ent_period_id = period.ent_period_id
         AND t.ent_qtr_id = qtr.ent_qtr_id
         AND t.ent_year_id = year.ent_year_id
GROUP BY CUBE (orl.sold_from_cust_account_id,
               orl.bill_to_party_id,
               orl.bill_to_cust_account_id,
               period.ent_period_id,
               qtr.ent_qtr_id,
               year.ent_year_id,
               t.month_id,
               TO_CHAR (t.report_date, 'YYYY'),
               orl.end_cust_party_id
              );
