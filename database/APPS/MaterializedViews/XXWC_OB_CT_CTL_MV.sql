DROP MATERIALIZED VIEW    APPS.XXWC_OB_CT_CTL_MV;

CREATE MATERIALIZED VIEW APPS.XXWC_OB_CT_CTL_MV (CREATED_BY,CREATION_DATE,CUSTOMER_TRX_ID,CUSTOMER_TRX_LINE_ID,DESCRIPTION,EXTENDED_AMOUNT,INTERFACE_LINE_ATTRIBUTE6,INTERFACE_LINE_CONTEXT,ACTUAL_INVENTORY_ITEM_ID,LAST_UPDATE_DATE,LAST_UPDATED_BY,LINE_NUMBER,LINE_TYPE,LINK_TO_CUST_TRX_LINE_ID,ORG_ID,QUANTITY_CREDITED,QUANTITY_INVOICED,QUANTITY_ORDERED,SALES_ORDER,SALES_ORDER_DATE,UNIT_SELLING_PRICE,UOM_CODE,WAREHOUSE_ID,TAX_AMOUNT,RN)
TABLESPACE APPS_TS_TX_DATA
NOCACHE
NOCOMPRESS
BUILD IMMEDIATE
REFRESH COMPLETE ON DEMAND
WITH PRIMARY KEY
AS 
/*************************************************************************
 Copyright (c) 2016 HD Supply
 All rights reserved.
**************************************************************************
   $Header XXWC_OB_CT_CTL_MV$
  Module Name: XXWC_OB_CT_CTL_MV

  PURPOSE:
 
 sub-MV used by APPS.XXWC_OB_CUST_TRX_INCR_MV
          1. This is the main source of the transaction lines w line_type ='LINE'
          Incremental date range filter will be applied in the main MV
          2. replacement for call to
           apps.xxwc_mv_routines_pkg.get_tax_amount (sq_ra_cust_trx_lines_all.customer_trx_id
                                                   , sq_ra_cust_trx_lines_all.customer_trx_line_id)
           The originally used function (get_tax_amount) doesn't limit the ar.ra_customer_trx_lines_all
           records by date range which created the need to pull 100% of the 'LINE' and 'TAX' records
       
        This MV will be refreshed before the main query in
        XXWC_OB_CUST_TRX_INCR_MV
         

  REVISIONS:
  
   VERSION  DATE        AUTHOR(S)        TMS TASK          DESCRIPTION
   -------  ----------  ---------------  ---------------   ----------------------------------------------
   1.0      2016-07-26  Steve Moffa      20160720-00055    Initially Created 
 
**************************************************************************/
SELECT 
      created_by
      ,creation_date
      ,customer_trx_id
      ,customer_trx_line_id
      ,description
      ,extended_amount
      ,interface_line_attribute6
      ,interface_line_context
      ,inventory_item_id AS actual_inventory_item_id
      ,last_update_date
      ,last_updated_by
      ,line_number
      ,line_type
      ,link_to_cust_trx_line_id
      ,org_id
      ,quantity_credited
      ,quantity_invoiced
      ,quantity_ordered
      ,sales_order
      ,sales_order_date
      ,unit_selling_price
      ,uom_code
      ,warehouse_id
      ,CASE
         WHEN line_type = 'TAX'
         THEN
           SUM (extended_amount) OVER (PARTITION BY customer_trx_id, link_to_cust_trx_line_id)
         ELSE
           0
       END
         AS tax_amount
      ,ROW_NUMBER () OVER (PARTITION BY customer_trx_id, link_to_cust_trx_line_id ORDER BY ROWNUM) rn
  FROM ar.ra_customer_trx_lines_all
 WHERE line_type IN ('LINE', 'TAX') and LAST_UPDATE_DATE > sysdate -2;

COMMENT ON MATERIALIZED VIEW APPS.XXWC_OB_CT_CTL_MV IS 'snapshot table for snapshot APPS.XXWC_OB_CT_CTL_MV';

CREATE BITMAP INDEX APPS.XXWC_OB_CT_CTL_MV_B1 ON APPS.XXWC_OB_CT_CTL_MV
(LINE_TYPE)
TABLESPACE APPS_TS_TX_IDX;

CREATE BITMAP INDEX APPS.XXWC_OB_CT_CTL_MV_B2 ON APPS.XXWC_OB_CT_CTL_MV
(LAST_UPDATE_DATE)
TABLESPACE APPS_TS_TX_IDX;

CREATE INDEX        APPS.XXWC_OB_CT_CTL_MV_N1 ON APPS.XXWC_OB_CT_CTL_MV
(LINK_TO_CUST_TRX_LINE_ID)
TABLESPACE APPS_TS_TX_IDX
COMPRESS 1;

CREATE BITMAP INDEX APPS.XXWC_OB_CT_CTL_MV_B3 ON APPS.XXWC_OB_CT_CTL_MV
(INTERFACE_LINE_ATTRIBUTE6)
TABLESPACE APPS_TS_TX_IDX;

BEGIN
  SYS.DBMS_STATS.GATHER_TABLE_STATS (
     OwnName           => 'APPS'
    ,TabName           => 'XXWC_OB_CT_CTL_MV'
    ,Estimate_Percent  => 100
    ,Method_Opt        => 'FOR ALL COLUMNS SIZE SKEWONLY'
    ,Degree            => NULL
    ,Cascade           => TRUE
    ,No_Invalidate  => FALSE);
END;
/