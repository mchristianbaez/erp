DROP MATERIALIZED VIEW   APPS.XXWC_OB_ORD_LN_COST_MV;

CREATE MATERIALIZED VIEW APPS.XXWC_OB_ORD_LN_COST_MV (TRX_SOURCE_LINE_ID,ORDER_LINE_COST)
TABLESPACE APPS_TS_TX_DATA
NOCACHE
NOCOMPRESS
BUILD IMMEDIATE
REFRESH COMPLETE ON DEMAND
WITH PRIMARY KEY
AS 
/*************************************************************************
 Copyright (c) 2016 HD Supply
 All rights reserved.
**************************************************************************
   $Header XXWC_OB_ORD_LN_COST_MV$
  Module Name: XXWC_OB_ORD_LN_COST_MV

  PURPOSE:
 
    sub-MV used by APPS.XXWC_OB_CUST_TRX_INCR_MV
         -- replacement for original OBI code calls to
                     FUNCTION apps.xxwc_mv_routines_pkg.get_order_line_cost
         --
         -- This MV will be refreshed before the main query in
         -- XXWC_OB_CUST_TRX_INCR_MV
  

  REVISIONS:
  
   VERSION  DATE        AUTHOR(S)        TMS TASK          DESCRIPTION
   -------  ----------  ---------------  ---------------   ----------------------------------------------
   1.0      2016-07-26  Steve Moffa      20160720-00055    Initially Created 
   
 
**************************************************************************/
SELECT 
      trx_source_line_id
      ,order_line_cost
  FROM (SELECT mmt.trx_source_line_id
              ,NVL (mmt.transaction_cost, NVL (mmt.actual_cost, 0)) AS order_line_cost
              ,ROW_NUMBER ()
               OVER (PARTITION BY mmt.trx_source_line_id
                     ORDER BY NVL (mmt.transaction_cost, NVL (mmt.actual_cost, 0)) DESC)
                 AS rn
          FROM inv.mtl_material_transactions mmt
               INNER JOIN inv.mtl_transaction_types mtt ON mmt.transaction_type_id = mtt.transaction_type_id
         WHERE mtt.transaction_type_name IN
                 ('RMA Receipt', 'RMA Return', 'Sales Order Pick', 'Sales order issue')
                 and mmt.trx_source_line_id in (select line_id from  APPS.XXWC_OB_CT_ORD_LN_MV)
                 )
 WHERE rn = 1;

COMMENT ON MATERIALIZED VIEW APPS.XXWC_OB_ORD_LN_COST_MV IS 'snapshot table for snapshot APPS.XXWC_OB_ORD_LN_COST_MV';

BEGIN
  SYS.DBMS_STATS.GATHER_TABLE_STATS (
     OwnName           => 'APPS'
    ,TabName           => 'XXWC_OB_ORD_LN_COST_MV'
    ,Estimate_Percent  => 100
    ,Method_Opt        => 'FOR ALL COLUMNS SIZE SKEWONLY'
    ,Degree            => NULL
    ,Cascade           => TRUE
    ,No_Invalidate  => FALSE);
END;
/