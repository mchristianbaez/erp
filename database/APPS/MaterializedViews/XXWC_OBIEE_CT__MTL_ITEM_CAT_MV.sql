DROP MATERIALIZED VIEW APPS.XXWC_OBIEE_CT__MTL_ITEM_CAT_MV;
CREATE MATERIALIZED VIEW APPS.XXWC_OBIEE_CT__MTL_ITEM_CAT_MV (CATEGORY_CONCAT_SEGS,INVENTORY_ITEM_ID,ORGANIZATION_ID)
BUILD IMMEDIATE
REFRESH COMPLETE ON DEMAND
WITH PRIMARY KEY
AS 
SELECT /* sub-MV used by APPS.XXWC_OBIEE_CUST_TRANS_INCR_MV
       -- replacement for apps.xxwc_mv_routines_pkg.get_item_category call
       -- Original OBIEE call looks like this:
              , (CASE
                   WHEN     sq_ra_cust_trx_lines_all.inventory_item_id IS NULL
                        AND sq_ra_cust_trx_lines_all.warehouse_id IS NULL
                   THEN
                     NULL
                   ELSE
                     SUBSTR (apps.xxwc_mv_routines_pkg.get_item_category (
                               sq_ra_cust_trx_lines_all.inventory_item_id
                              ,sq_ra_cust_trx_lines_all.warehouse_id
                              ,'Inventory Category'
                              ,0)
                            ,1
                            ,40)
                 END)
                 prod_cat_class
       --
       -- This MV will be refreshed before the main query in
       -- XXWC_OBIEE_CUST_TRANS_INCR_MV
       */
      category_concat_segs, inventory_item_id, organization_id
  FROM apps.mtl_item_categories_v
 WHERE category_set_name = 'Inventory Category';


COMMENT ON MATERIALIZED VIEW APPS.XXWC_OBIEE_CT__MTL_ITEM_CAT_MV IS 'snapshot table for snapshot APPS.XXWC_OBIEE_CT__MTL_ITEM_CAT_MV';
