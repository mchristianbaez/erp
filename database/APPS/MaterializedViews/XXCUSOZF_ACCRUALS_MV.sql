
  CREATE MATERIALIZED VIEW "APPS"."XXCUSOZF_ACCRUALS_MV" ("MVID", "LOB_ID", "BRANCH_ID", "PLAN_ID", "ADJUSTMENT_TYPE_ID", "REBATE_TYPE_ID", "PERIOD_ID", "QTR_ID", "YEAR_ID", "CALENDAR_YEAR", "ACCRUED_AMOUNT", "GRP_MVID", "GRP_LOB", "GRP_BRANCH", "GRP_PLAN_ID", "GRP_REBATE_TYPE", "GRP_ADJ_TYPE", "GRP_PERIOD", "GRP_QTR", "GRP_YEAR", "GRP_CAL_YEAR")
  ORGANIZATION HEAP PCTFREE 10 PCTUSED 0 INITRANS 2 MAXTRANS 255 NOCOMPRESS NOLOGGING
  STORAGE(INITIAL 4194304 NEXT 4194304 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "APPS_TS_TX_DATA" 
  PARALLEL 4 
  BUILD IMMEDIATE
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 149 
  STORAGE(INITIAL 131072 NEXT 131072 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "APPS_TS_TX_DATA" 
  REFRESH COMPLETE ON DEMAND
  USING DEFAULT LOCAL ROLLBACK SEGMENT
  USING ENFORCED CONSTRAINTS DISABLE QUERY REWRITE
  AS SELECT ofu.cust_account_id mvid,
         orl.bill_to_party_id lob_id,
         orl.bill_to_cust_account_id branch_id,
         ofu.plan_id plan_id,
         DECODE (ofu.utilization_type,
                 'ADJUSTMENT', ofu.adjustment_type_id,
                 -99)
            adjustment_type_id,
         med.DESCRIPTION rebate_type_id,
         period.ent_period_id period_id,
         qtr.ent_qtr_id qtr_id,
         year.ent_year_id year_id,
         TO_CHAR (t.report_date, 'YYYY') calendar_year,
         SUM (acctd_amount) accrued_amount,
         GROUPING (ofu.cust_account_id) grp_mvid,
         GROUPING (orl.bill_to_party_id) grp_lob,
         GROUPING (orl.bill_to_cust_account_id) grp_branch,
         GROUPING (ofu.plan_id) grp_plan_id,
         GROUPING (med.description) grp_rebate_type,
         GROUPING (
            DECODE (ofu.utilization_type,
                    'ADJUSTMENT', ofu.adjustment_type_id,
                    -99))
            grp_adj_type,
         GROUPING (period.ent_period_id) grp_period,
         GROUPING (qtr.ent_qtr_id) grp_qtr,
         GROUPING (year.ent_year_id) grp_year,
         GROUPING (TO_CHAR (t.report_date, 'YYYY')) grp_cal_year
    FROM ozf_funds_utilized_all_b ofu,
         ozf_resale_lines_all orl,
         ozf_offers oo,
         ams_media_tl med,
         ozf_time_ent_period period,
         ozf_time_ent_qtr qtr,
         ozf_time_ent_year year,
         ozf_time_day t
   WHERE     1 = 1
         AND t.ent_year_id >= 2012 --We may need to change this in future to use parameter somehow.
         --AND t.ent_year_id >=TO_CHAR (SYSDATE, 'YYYY') - 3
         AND t.report_date = TRUNC (ofu.gl_date)
         AND orl.resale_line_id = ofu.object_id
         AND oo.qp_list_header_id = ofu.plan_id
         AND oo.activity_media_id = med.media_id
         AND t.ent_period_id = period.ent_period_id
         AND t.ent_qtr_id = qtr.ent_qtr_id
         AND t.ent_year_id = year.ent_year_id
         AND ofu.gl_posted_flag = 'Y'
GROUP BY CUBE (ofu.cust_account_id,
               orl.bill_to_party_id,
               orl.bill_to_cust_account_id,
               ofu.plan_id,
               DECODE (ofu.utilization_type,
                       'ADJUSTMENT', ofu.adjustment_type_id,
                       -99),
               med.DESCRIPTION,
               period.ent_period_id,
               qtr.ent_qtr_id,
               year.ent_year_id,
               TO_CHAR (t.report_date, 'YYYY'))
;
