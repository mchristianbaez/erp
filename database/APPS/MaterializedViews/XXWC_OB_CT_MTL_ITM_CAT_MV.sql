DROP   MATERIALIZED VIEW APPS.XXWC_OB_CT_MTL_ITM_CAT_MV;

CREATE MATERIALIZED VIEW APPS.XXWC_OB_CT_MTL_ITM_CAT_MV (CATEGORY_CONCAT_SEGS,INVENTORY_ITEM_ID,ORGANIZATION_ID)
TABLESPACE APPS_TS_TX_DATA
NOCACHE
NOCOMPRESS
BUILD IMMEDIATE
REFRESH COMPLETE ON DEMAND
WITH PRIMARY KEY
AS 
/*************************************************************************
 Copyright (c) 2016 HD Supply
 All rights reserved.
**************************************************************************
   $Header XXWC_OB_CT_MTL_ITM_CAT_MV$
  Module Name: XXWC_OB_CT_MTL_ITM_CAT_MV

  PURPOSE:
 
  sub-MV used by APPS.XXWC_OB_CUST_TRX_INCR_MV
       -- replacement for apps.xxwc_mv_routines_pkg.get_item_category call
       -- Original OBIEE call looks like this:
              , (CASE
                   WHEN     sq_ra_cust_trx_lines_all.inventory_item_id IS NULL
                        AND sq_ra_cust_trx_lines_all.warehouse_id IS NULL
                   THEN
                     NULL
                   ELSE
                     SUBSTR (apps.xxwc_mv_routines_pkg.get_item_category (
                               sq_ra_cust_trx_lines_all.inventory_item_id
                              ,sq_ra_cust_trx_lines_all.warehouse_id
                              ,'Inventory Category'
                              ,0)
                            ,1
                            ,40)
                 END)
                 prod_cat_class
       --
       -- This MV will be refreshed before the main query in
       -- XXWC_OB_CUST_TRX_INCR_MV
  

  REVISIONS:
  
   VERSION  DATE        AUTHOR(S)        TMS TASK          DESCRIPTION
   -------  ----------  ---------------  ---------------   ----------------------------------------------
   1.0      2016-07-26  Steve Moffa      20160720-00055    Initially Created 
   
 
**************************************************************************/
SELECT 
      category_concat_segs
      ,inventory_item_id
      ,organization_id
  FROM apps.mtl_item_categories_v
 WHERE category_set_name = 'Inventory Category';

COMMENT ON MATERIALIZED VIEW APPS.XXWC_OB_CT_MTL_ITM_CAT_MV IS 'snapshot table for snapshot APPS.XXWC_OBIEE_CT__MTL_ITEM_CAT_MV';

BEGIN
  SYS.DBMS_STATS.GATHER_TABLE_STATS (
     OwnName           => 'APPS'
    ,TabName           => 'XXWC_OB_CT_MTL_ITM_CAT_MV'
    ,Estimate_Percent  => 100
    ,Method_Opt        => 'FOR ALL COLUMNS SIZE SKEWONLY'
    ,Degree            => NULL
    ,Cascade           => TRUE
    ,No_Invalidate  => FALSE);
END;
/