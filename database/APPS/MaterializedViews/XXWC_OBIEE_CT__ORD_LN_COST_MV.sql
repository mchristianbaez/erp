DROP MATERIALIZED VIEW APPS.XXWC_OBIEE_CT__ORD_LN_COST_MV;
CREATE MATERIALIZED VIEW APPS.XXWC_OBIEE_CT__ORD_LN_COST_MV (TRX_SOURCE_LINE_ID,ORDER_LINE_COST)
BUILD IMMEDIATE
REFRESH COMPLETE ON DEMAND
WITH PRIMARY KEY
AS 
SELECT /* sub-MV used by APPS.XXWC_OBIEE_CUST_TRANS_INCR_MV
         -- replacement for original OBI code calls to
                     FUNCTION apps.xxwc_mv_routines_pkg.get_order_line_cost
         --
         -- This MV will be refreshed before the main query in
         -- XXWC_OBIEE_CUST_TRANS_INCR_MV
         */
      trx_source_line_id, order_line_cost
  FROM (SELECT mmt.trx_source_line_id
              ,NVL (mmt.transaction_cost, NVL (mmt.actual_cost, 0)) AS order_line_cost
              ,ROW_NUMBER ()
               OVER (PARTITION BY mmt.trx_source_line_id
                     ORDER BY NVL (mmt.transaction_cost, NVL (mmt.actual_cost, 0)) DESC)
                  AS rn
          FROM inv.mtl_material_transactions mmt
               INNER JOIN inv.mtl_transaction_types mtt ON mmt.transaction_type_id = mtt.transaction_type_id
         WHERE mtt.transaction_type_name IN ('RMA Receipt'
                                            ,'RMA Return'
                                            ,'Sales Order Pick'
                                            ,'Sales order issue'))
 WHERE rn = 1;


COMMENT ON MATERIALIZED VIEW APPS.XXWC_OBIEE_CT__ORD_LN_COST_MV IS 'snapshot table for snapshot APPS.XXWC_OBIEE_CT__ORD_LN_COST_MV';
