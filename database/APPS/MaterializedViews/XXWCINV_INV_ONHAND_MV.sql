DROP MATERIALIZED VIEW APPS.XXWCINV_INV_ONHAND_MV;

CREATE MATERIALIZED VIEW APPS.XXWCINV_INV_ONHAND_MV 
TABLESPACE APPS_TS_TX_DATA
PCTUSED    0
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             128K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOCACHE
LOGGING
NOCOMPRESS
NOPARALLEL
BUILD IMMEDIATE
REFRESH COMPLETE
START WITH TO_DATE('13-Dec-2012 09:15:00','dd-mon-yyyy hh24:mi:ss')
NEXT TRUNC(SYSDATE+1) + 0/24 
WITH PRIMARY KEY
AS 
/* Formatted on 2012/06/21 17:39 (Formatter Plus v4.8.8) */
SELECT msi.inventory_item_id, msi.organization_id, msi.segment1 item_number,
       msi.description item_description, mp.organization_code LOCATION,
       mp.attribute8 district, mp.attribute9 region, mp.attribute6 market,
       DECODE (mc.segment2, 
                '6000', 'Y', 
                '6007', 'Y',
                '6016', 'Y',
                '6019', 'Y',
                '6021', 'Y',
                '6022', 'Y',
                '6023', 'Y',
                'N') rebar,
       mc.concatenated_segments item_category,
                                              --           Nvl(MC.attribute1, FND_PROFILE.Value('XXWC_DFLT_RESERVE_GROUP')) reserve_group,
                                              ap.segment1 supplier_number,
       ap.vendor_name supplier_name, ap.vendor_id, moqd.orig_date_received,
       moqd.primary_transaction_quantity,
       (moqd.primary_transaction_quantity * cic.item_cost) item_cost,
       src.vendor_name src_vendor
  FROM mtl_system_items_b msi,
       mtl_parameters mp,
       mtl_item_categories mic,
       mtl_categories_kfv mc,
       mtl_category_sets_tl mcs,
       mtl_onhand_quantities_detail moqd,
       cst_item_costs cic,
       cst_cost_types cct,
       ap_suppliers ap,
       (SELECT distinct rsh.vendor_id, rsl.item_id, rsl.to_organization_id --Added Distinct 6/21/2012 LHS
          FROM rcv_shipment_headers rsh,
               rcv_shipment_lines rsl,
               rcv_transactions rt
         WHERE rsh.shipment_header_id = rsl.shipment_header_id
           AND rsh.receipt_source_code = 'VENDOR'
           AND rt.shipment_header_id = rsh.shipment_header_id
           AND rt.shipment_line_id = rsl.shipment_line_id
           AND rt.transaction_type = 'RECEIVE'
           AND rt.transaction_date =
                  (SELECT MAX (rt2.transaction_date)
                     FROM rcv_shipment_lines rsl2, rcv_transactions rt2
                    WHERE rsl2.item_id = rsl.item_id
                      AND rsl2.to_organization_id = rsl.to_organization_id
                      AND rt2.shipment_line_id = rsl2.shipment_line_id
                      AND rt2.transaction_type = 'RECEIVE')) rcv,
       (SELECT sra.organization_id, sra.inventory_item_id,
               sr.sourcing_rule_name, mp.organization_code vendor_name
          FROM mrp_sourcing_rules sr,
               mrp_sr_receipt_org sro,
               mrp_sr_source_org sso,
               mrp_sr_assignments sra,
               mrp_assignment_sets mas,
               mtl_parameters mp
         WHERE sr.sourcing_rule_id = sro.sourcing_rule_id
           AND TRUNC (SYSDATE) BETWEEN sro.effective_date
                                   AND NVL (disable_date, SYSDATE + 1)
           AND sro.sr_receipt_id = sso.sr_receipt_id
           AND sra.sourcing_rule_id = sr.sourcing_rule_id
           AND sra.assignment_type = 6                   -- 3=item  6=item/org
           AND sra.assignment_set_id = mas.assignment_set_id
           AND mas.assignment_set_name = 'WC Default'
           AND sso.source_organization_id = mp.organization_id
           AND sso.source_type = 1              -- 1=transfer-from  3=buy-from
        UNION
        SELECT sra.organization_id, sra.inventory_item_id,
               sr.sourcing_rule_name, ap.vendor_name vendor_name
          FROM mrp_sourcing_rules sr,
               mrp_sr_receipt_org sro,
               mrp_sr_source_org sso,
               mrp_sr_assignments sra,
               mrp_assignment_sets mas,
               ap_suppliers ap
         WHERE sr.sourcing_rule_id = sro.sourcing_rule_id
           AND TRUNC (SYSDATE) BETWEEN sro.effective_date
                                   AND NVL (disable_date, SYSDATE + 1)
           AND sro.sr_receipt_id = sso.sr_receipt_id
           AND sra.sourcing_rule_id = sr.sourcing_rule_id
           AND sra.assignment_type = 6                   -- 3=item  6=item/org
           AND sra.assignment_set_id = mas.assignment_set_id
           AND mas.assignment_set_name = 'WC Default'
           AND sso.vendor_id = ap.vendor_id
           AND sso.source_type = 3              -- 1=transfer-from  3=buy-from
                                  ) src
 WHERE msi.organization_id = mp.organization_id
   AND msi.inventory_item_id = mic.inventory_item_id
   AND msi.organization_id = mic.organization_id
   AND mic.category_id = mc.category_id
   --       And MC.structure_id = MCS.structure_id
   AND mic.category_set_id = mcs.category_set_id
   AND mcs.category_set_name = 'Inventory Category'
   AND msi.inventory_item_id = moqd.inventory_item_id
   AND msi.organization_id = moqd.organization_id
   AND msi.inventory_item_id = cic.inventory_item_id
   AND msi.organization_id = cic.organization_id
   AND cic.cost_type_id = cct.cost_type_id
   AND cct.cost_type = 'Average'
   AND msi.inventory_item_id = rcv.item_id(+)
   AND msi.organization_id = rcv.to_organization_id(+)
   AND rcv.vendor_id = ap.vendor_id(+)
   AND msi.inventory_item_id = src.inventory_item_id(+)
   AND msi.organization_id = src.organization_id(+);

COMMENT ON MATERIALIZED VIEW APPS.XXWCINV_INV_ONHAND_MV IS 'snapshot table for snapshot APPS.XXWCINV_INV_ONHAND_MV';
