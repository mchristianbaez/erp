  /********************************************************************************
  FILE NAME: XXWC_ADDRESS_REC.sql
  
  PROGRAM TYPE: Address record type for online sales order form
  
  PURPOSE: Address record type for online sales order form
  
  HISTORY
  ==========================================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- ----------------------------------------------------
  1.0     03/07/2017    Rakesh P.       TMS#20170111-00059-Hand Write Oracle API for Web Service
                                        Initial Version
  *******************************************************************************************/
CREATE OR REPLACE TYPE xxwc.xxwc_address_rec
AS
  OBJECT (   customer_id                  NUMBER,
             country                      VARCHAR2(60),
             address1                     VARCHAR2(240),
             address2                     VARCHAR2(240),
             address3                     VARCHAR2(240),
             address4                     VARCHAR2(240),
             city                         VARCHAR2(60),
             county                       VARCHAR2(60),
             state                        VARCHAR2(60),
             postal_code                  VARCHAR2(60),
             sold_to_flag                 VARCHAR2(1),
             ship_to_flag                 VARCHAR2(1),
             bill_to_flag                 VARCHAR2(1),
             deliver_to_flag              VARCHAR2(1),
             location                     VARCHAR2(240),
             last_name                    VARCHAR2(3000),
             first_name                   VARCHAR2(3000),
             title                        VARCHAR2(240),
             email                        VARCHAR2(240),  
             contact_num                  VARCHAR2(240), 
             fax_num                      VARCHAR2(240),   
             user_id                      NUMBER
            ,STATIC FUNCTION g_miss_null RETURN xxwc_address_rec
            );
/

CREATE OR REPLACE TYPE BODY xxwc.xxwc_address_rec
AS
   STATIC FUNCTION g_miss_null
       RETURN xxwc_address_rec
   AS
   BEGIN
      RETURN xxwc_address_rec ( customer_id            => NULL,
                                country                => NULL,
                                address1               => NULL,
                                address2               => NULL,
                                address3               => NULL,
                                address4               => NULL,
                                city                   => NULL,
                                county                 => NULL,
                                state                  => NULL,
                                postal_code            => NULL,
                                sold_to_flag           => NULL,
                                ship_to_flag           => NULL,
                                bill_to_flag           => NULL,
                                deliver_to_flag        => NULL,
                                location               => NULL,
                                last_name              => NULL,
                                first_name             => NULL,
                                title                  => NULL,
                                email                  => NULL,
                                contact_num            => NULL,
                                fax_num                => NULL,
                                user_id                => NULL
                                   );
   END g_miss_null;
END;
/

CREATE PUBLIC SYNONYM xxwc_address_rec FOR xxwc.xxwc_address_rec;

show err;
EXIT;