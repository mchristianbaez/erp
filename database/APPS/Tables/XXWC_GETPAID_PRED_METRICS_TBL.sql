/*************************************************************************
  $Header XXWC_GETPAID_PRED_METRICS_TBL $
  Module Name: XXWC_GETPAID_PRED_METRICS_TBL

  PURPOSE: Table to load GetPaid Predictive Metrics information

  REVISIONS:
  Ver        Date        Author             Description
  ---------  ----------  ---------------    -------------------------
  1.0        11/11/2013  Gopi Damuluri      Initial Version
**************************************************************************/
CREATE TABLE xxwc.xxwc_getpaid_pred_metrics_tbl (CUSTOMER_NUMBER   VARCHAR2(30)
                                               , RAW_SCORE         NUMBER
                                               , CREATED_BY        NUMBER
                                               , CREATION_DATE     DATE
                                               , LAST_UpDATED_BY   NUMBER
                                               , LAST_UPDATE_DATE  DATE
                                               , STATUS            VARCHAR2(1)
                                               , ERROR_MESSAGE     VARCHAR2(2000));