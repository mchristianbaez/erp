CREATE OR REPLACE VIEW apps.xxwc_ar_lien_release_hdr_vw
AS
   SELECT
   /******************************************************************************
      NAME:       apps.xxwc_ar_lien_release_hdr_vw
      PURPOSE:    View used in the header section of lien release form

      REVISIONS:
      Ver        Date        Author           Description
      ---------  ----------  ---------------  ------------------------------------
      1.0        11/22/2013      shariharan    Initial Version
   ******************************************************************************/   
    a.customer_name,
          a.customer_number,
          b.city,
          b.state,
          b.postal_code,
          b.address1||','||b.address2||','||b.address3||','||b.address4 concat_address,
          b.concatenated_address,
          b.site_number,
          b.orig_system_reference orig_system_reference_add,
          c.site_use_code,
          c.primary_flag,
          c.location,
          DECODE (
             SUBSTR (
                c.location,
                DECODE (INSTR (c.location, '-', 1) + 1,
                        1, LENGTH (c.location) + 1,
                        INSTR (c.location, '-', 1) + 1)),
             b.site_number, NULL,
             SUBSTR (
                c.location,
                DECODE (INSTR (c.location, '-', 1) + 1,
                        1, LENGTH (c.location) + 1,
                        INSTR (c.location, '-', 1) + 1)))
             cust_job_num,
          c.orig_system_reference orig_system_reference_site,
          b.attribute_category nto_flag,
          NULL po_number,
          c.creation_date,
          b.customer_id,
          b.address_id,
          c.site_use_id,
          c.bill_to_site_use_id,
          (SELECT site_use_id
             FROM hz_cust_site_uses_all x, hz_cust_acct_sites_all y
            WHERE     x.cust_acct_site_id = y.cust_acct_site_id
                  AND y.cust_account_id = a.customer_id
                  AND x.site_use_code = 'BILL_TO'
                  AND primary_flag = 'Y')
             primary_bt_site_use_id,
          (SELECT credit_classification
             FROM hz_customer_profiles p
            WHERE     p.cust_account_id = a.customer_id
                  AND p.site_use_id = c.bill_to_site_use_id)
             credit_classification
     FROM ar_customers a, xxwc_ar_addresses_v b, hz_cust_site_uses_all c
    WHERE     a.customer_id = b.customer_id
          AND b.address_id = c.cust_acct_site_id
          AND c.site_use_code = 'SHIP_TO'
/

CREATE OR REPLACE VIEW apps.xxwc_ar_lien_release_detail_vw
AS
   SELECT 
   /******************************************************************************
      NAME:       apps.xxwc_ar_lien_release_detail_vw
      PURPOSE:    View used in the line section of lien release form

      REVISIONS:
      Ver        Date        Author           Description
      ---------  ----------  ---------------  ------------------------------------
      1.0        11/22/2013      shariharan    Initial Version
   ******************************************************************************/   
   a.header_id,
          a.order_number,
          a.ordered_date,
          a.cust_po_number,
          a.order_type,
          a.ship_to,
          a.salesrep_id,
          (SELECT name
             FROM ra_salesreps b
            WHERE b.salesrep_id = a.salesrep_id)
             salesrep_name,
          a.ship_to_org_id,
          d.address_id,
          d.site_number,
          apps.oe_oe_totals_summary.prt_order_total (a.header_id) order_total,
          a.ship_from_org_id,
          e.organization_code,
          e.organization_name,
          a.flow_status_code,
          apps.xxwc_ar_lien_release_pkg.get_rem_order_total (a.header_id)
             amount_remaining,
          c.location
     FROM oe_order_headers_v a,
          hz_Cust_site_uses_all c,
          xxwc_ar_addresses_v d,
          org_organization_definitions e
    WHERE     a.ship_to_org_id = c.site_use_id
          AND c.cust_acct_site_id = d.address_id
          AND e.organization_id = a.ship_from_org_id
          --and a.flow_status_Code not in ('CLOSED','CANCELLED','ENTERED','DRAFT','DRAFT_CUSTOMER_REJECTED','LOST','OFFER_EXPIRED','PENDING_CUSTOMER_ACCEPTANCE')
          AND a.flow_status_Code NOT IN ('CLOSED',
                                         'CANCELLED',
                                         'DRAFT',
                                         'DRAFT_CUSTOMER_REJECTED',
                                         'LOST',
                                         'OFFER_EXPIRED')
          AND a.ordered_date IS NOT NULL;
  
create global temporary table xxwc.xxwc_ar_line_release_hdr_gt
(customer_id number,
 address_id number,
 site_use_id number,
 bill_to_site_use_id number,
 primary_bt_site_use_id number,
 credit_classification varchar2(30),
 amount_requested number
)
/

create synonym apps.xxwc_ar_line_release_hdr_gt for xxwc.xxwc_ar_line_release_hdr_gt
/

CREATE OR REPLACE VIEW apps.xxwc_ar_payment_schedules_vw
AS
   SELECT
   /******************************************************************************
      NAME:       apps.xxwc_ar_payment_schedules_vw
      PURPOSE:    View used to display outstanding invoices in lien release form

      REVISIONS:
      Ver        Date        Author           Description
      ---------  ----------  ---------------  ------------------------------------
      1.0        11/22/2013      shariharan    Initial Version
   ******************************************************************************/     
    trx_number,
          trx_date,
          type_name,
          ct_purchase_order,
          interface_header_attribute1,
          due_date,
          amount_due_original,
          amount_due_remaining,
          customer_id,
          customer_site_use_id,
          su_location
          /*,
          (SELECT b.concatenated_address
             FROM xxwc_ar_addresses_v b, hz_cust_site_uses_all c
            WHERE     b.customer_id = a.customer_id
                  AND b.address_id = c.cust_acct_site_id
                  AND c.site_use_code = 'BILL_TO'
                  AND primary_flag = 'Y'
                  AND ROWNUM = 1)
             primary_location*/
     FROM ar_payment_schedules_v a
    WHERE     a.status <> 'CL'
          AND (a.customer_id, a.customer_site_use_id) IN (SELECT customer_id,
                                                                 NVL (
                                                                    bill_to_site_use_id,
                                                                    primary_bt_site_use_id)
                                                            FROM xxwc_ar_line_release_hdr_gt)
/  


create table xxwc.xxwc_ar_lien_rel_action_tbl
(
 customer_id number,
 address_id  number,
 site_use_id number,
 bill_to_site_use_id number,
 credit_classification varchar2(30),
 lien_release_type varchar2(30),
 amount_requested number,
 notes varchar2(240),
 request_id  number,
 process_flag varchar2(1),
 error_message varchar2(2000),
 creation_date date,
 created_by number,
 last_update_date date,
 last_updated_by number,
 last_update_login number
)
/

create index xxwc.xxwc_ar_lien_rel_action_n1
on xxwc.xxwc_ar_lien_rel_action_tbl(request_id)
/

create synonym apps.xxwc_ar_lien_rel_action_tbl for xxwc.xxwc_ar_lien_rel_action_tbl
/

create sequence xxwc.xxwc_ar_lien_rel_action_s
/

create synonym apps.xxwc_ar_lien_rel_action_s for xxwc.xxwc_ar_lien_rel_action_s
/