ALTER TABLE XXWC.XXWC_BPA_IMPACT_REPORT_TEMP
  ADD (PURCHASE_HISTORY NUMBER
      ,PURCHASE_COST NUMBER
--      ,PURCHASE_AVG NUMBER
      ,FORECAST_SPEND NUMBER
      ,PURCHASE_IMPACT NUMBER
      ,PURCHASE_CHANGE NUMBER);
      
ALTER TABLE XXWC.XXWC_BPA_IMPACT_REPORT_TEMP
  DROP COLUMN PURCHASE_AVG;