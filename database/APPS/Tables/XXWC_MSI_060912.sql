--
-- XXWC_MSI_060912  (Table) 
--
CREATE TABLE APPS.XXWC_MSI_060912
(
  INVENTORY_ITEM_ID      NUMBER                 NOT NULL,
  ORGANIZATION_ID        NUMBER                 NOT NULL,
  COST_OF_SALES_ACCOUNT  NUMBER,
  SALES_ACCOUNT          NUMBER
)
TABLESPACE APPS_TS_TX_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             128K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


