Alter Table Xxwc.Xxwc_Inv_Min_Max_Temp
  Add Inventory_Planning_Code Number;

Alter Table Xxwc.Xxwc_Inv_Min_Max_Temp
  Add SAFETY_STOCK Number;
  
Alter Table Xxwc.Xxwc_Inv_Min_Max_Temp
  Add REORDER_POINT Number;

Alter Table Xxwc.Xxwc_Inv_Min_Max_Temp
  Add Lead_Time_Demand Number;

Alter Table Xxwc.Xxwc_Inv_Min_Max_Temp
  Add Amu Number;

Alter Table Xxwc.Xxwc_Inv_Min_Max_Temp
  Add Reserve_Stock NUMBER;

Alter Table Xxwc.Xxwc_Inv_Min_Max_Temp
  Add Eoq NUMBER;

Alter Table Xxwc.Xxwc_Inv_Min_Max_Temp
  Add Process_Lead_Time Number;

Alter Table Xxwc.Xxwc_Inv_Min_Max_Temp
  Add Lead_Time Number;

Alter Table Xxwc.Xxwc_Inv_Min_Max_Temp
  Add  Review_TIME number;

