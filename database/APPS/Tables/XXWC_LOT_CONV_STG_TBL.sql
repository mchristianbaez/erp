/*************************************************************************
  $Header XXWC_LOT_CONV_STG_TBL $
  Module Name: XXWC_LOT_CONV_STG_TBL

  PURPOSE: Store OnHand for Lot Control Items in Oralce

  REVISIONS:
  Ver        Date        Author                     Description
  ---------  ----------  ---------------    -------------------------
  1.0        08/16/2012  Gopi Damuluri      Initial Version
**************************************************************************/
CREATE TABLE XXWC.XXWC_LOT_CONV_STG_TBL(ITEM_NUM,
   INVENTORY_ITEM_ID,
   ORGANIZATION_ID,
   DATE_RECEIVED,
   LAST_UPDATE_DATE,
   LAST_UPDATED_BY,
   CREATION_DATE,
   CREATED_BY,
   LAST_UPDATE_LOGIN,
   TRANSACTION_QUANTITY,
   SUBINVENTORY_CODE,
   REVISION,
   LOCATOR_ID,
   CREATE_TRANSACTION_ID,
   UPDATE_TRANSACTION_ID,
   LOT_NUMBER,
   ORIG_DATE_RECEIVED,
   COST_GROUP_ID,
   CONTAINERIZED_FLAG,
   PROJECT_ID,
   TASK_ID,
   SECONDARY_UOM_CODE,
   SECONDARY_TRANSACTION_QUANTITY,
   STATUS_ID,
   IS_CONSIGNED,
   ERROR_MESSAGE,
   STATUS
)
AS (SELECT msib.segment1 item_num,
          moqd.INVENTORY_ITEM_ID inventory_item_id,
          moqd.ORGANIZATION_ID organization_id,
          moqd.DATE_RECEIVED date_received,
          moqd.LAST_UPDATE_DATE last_update_date,
          moqd.LAST_UPDATED_BY last_updated_by,
          moqd.CREATION_DATE creation_date,
          moqd.CREATED_BY created_by,
          moqd.LAST_UPDATE_LOGIN last_update_login,
          moqd.PRIMARY_TRANSACTION_QUANTITY TRANSACTION_QUANTITY,
          moqd.SUBINVENTORY_CODE subinventory_code,
          moqd.REVISION revision,
          moqd.LOCATOR_ID locator_id,
          moqd.CREATE_TRANSACTION_ID create_transaction_id,
          moqd.UPDATE_TRANSACTION_ID update_transaction_id,
          moqd.LOT_NUMBER lot_number,
          moqd.ORIG_DATE_RECEIVED orig_date_received,
          moqd.COST_GROUP_ID cost_group_id,
          moqd.CONTAINERIZED_FLAG containerized_flag,
          moqd.PROJECT_ID project_id,
          moqd.TASK_ID task_id,
          moqd.SECONDARY_UOM_CODE SECONDARY_UOM_CODE,
          moqd.SECONDARY_TRANSACTION_QUANTITY SECONDARY_TRANSACTION_QUANTITY,
          moqd.STATUS_ID,
          IS_CONSIGNED,
          RPAD('-',200,' ') ERROR_MESSAGE,
          'N' STATUS
     FROM MTL_ONHAND_QUANTITIES_DETAIL moqd
        , mtl_system_items_b msib
    WHERE 1 = 1
      AND moqd.inventory_item_id = msib.inventory_item_id
      AND moqd.organization_id   = msib.organization_id
      AND moqd.lot_number       IS NOT NULL
      UNION
      SELECT msib.segment1 item_num,
             msib.inventory_item_id             inventory_item_id,
             222                                organization_id,
             TRUNC(SYSDATE)                     date_received,
             TRUNC(SYSDATE)                     last_update_date,
             1296                               last_updated_by,
             TRUNC(SYSDATE)                     creation_date,
             1296                               created_by,
             -1                                 last_update_login,
             0                                  transaction_quantity,
             'General'                          subinventory_code,
             '1'                                  revision,
             NULL                               locator_id,
             NULL                               create_transaction_id,
             NULL                               update_transaction_id,
             NULL                               lot_number,
             TRUNC(SYSDATE)                     orig_date_received,
             NULL                               cost_group_id,
             NULL                               containerized_flag,
             NULL                               project_id,
             NULL                               task_id,
             NULL                               SECONDARY_UOM_CODE,
             NULL                               SECONDARY_TRANSACTION_QUANTITY,
             NULL                               STATUS_ID,
             msib.consigned_flag                IS_CONSIGNED,
             RPAD('-',200,' ')                  ERROR_MESSAGE,
             'N'                                STATUS
        FROM mtl_system_items_b msib
       WHERE 1 = 1
         AND msib.lot_control_code = 2
         AND NOT EXISTS (SELECT '1'
                           FROM mtl_onhand_quantities_detail moqd
                          WHERE 1 = 1
                            AND moqd.inventory_item_id = msib.inventory_item_id
                            AND moqd.organization_id   = msib.organization_id)
                        );