--Create the header level record
INSERT INTO ece_interface_columns(
interface_column_id,
interface_table_id,
interface_column_name,
table_name,
column_name,
record_number,
position,
width,
data_type,
conversion_sequence,
record_layout_code,
record_layout_qualifier,
conversion_group_id,
xref_category_allowed,
element_tag_name,
external_level,
map_id,
staging_column,
creation_date,
created_by,
last_update_date,
last_updated_by,
last_update_login)
SELECT
ece_interface_column_id_s.NEXTVAL,
(SELECT eit.interface_table_id
FROM ece_interface_tables eit,
ece_mappings em
WHERE em.map_code = 'EC_POCO_FF' AND
eit.output_level = '1' AND
em.map_id = eit.map_id),
NULL,
NULL,
'DIRECT_OR_STANDARD',
NULL,
NULL,
NULL,
NULL,
NULL,
NULL,
NULL,
NULL,
'N',
NULL,
1,
(SELECT map_id FROM ece_mappings WHERE map_code = 'EC_POCO_FF'),
'FIELD498',
SYSDATE,
1,
SYSDATE,
1,
1
FROM DUAL;


--Create the header level record
INSERT INTO ece_interface_columns(
interface_column_id,
interface_table_id,
interface_column_name,
table_name,
column_name,
record_number,
position,
width,
data_type,
conversion_sequence,
record_layout_code,
record_layout_qualifier,
conversion_group_id,
xref_category_allowed,
element_tag_name,
external_level,
map_id,
staging_column,
creation_date,
created_by,
last_update_date,
last_updated_by,
last_update_login)
SELECT
ece_interface_column_id_s.NEXTVAL,
(SELECT eit.interface_table_id
FROM ece_interface_tables eit,
ece_mappings em
WHERE em.map_code = 'EC_POCO_FF' AND
eit.output_level = '1' AND
em.map_id = eit.map_id),
NULL,
NULL,
'DIRECT_PHONE',
NULL,
NULL,
NULL,
NULL,
NULL,
NULL,
NULL,
NULL,
'N',
NULL,
1,
(SELECT map_id FROM ece_mappings WHERE map_code = 'EC_POCO_FF'),
'FIELD497',
SYSDATE,
1,
SYSDATE,
1,
1
FROM DUAL;