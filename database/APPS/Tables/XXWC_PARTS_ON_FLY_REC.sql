  /********************************************************************************
  FILE NAME: XXWC_PARTS_ON_FLY_REC.sql
  
  PROGRAM TYPE: Part on fly for online sales order form
  
  PURPOSE: Part on fly for online sales order form
  
  HISTORY
  ==========================================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- ----------------------------------------------------
  1.0     03/07/2017    Rakesh P.       TMS#20170111-00059-Hand Write Oracle API for Web Service
                                        Initial Version
  *******************************************************************************************/
 CREATE OR REPLACE TYPE xxwc.xxwc_parts_on_fly_rec
AS
  OBJECT (   
        ITEM_NUMBER              VARCHAR2(40) ,
        ITEM_DESCRIPTION         VARCHAR2(240),
        INVENTORY_ITEM_ID        NUMBER ,
        MST_ORGANIZATION_ID      NUMBER,
        ASSGN_ORGANIZATION_ID    NUMBER,
        PRICE_LIST_HEADER_ID     NUMBER,
        ITEM_TYPE_CODE           VARCHAR2(30),
        ITEM_TEMPLATE_ID         NUMBER,
        PRIMARY_UOM_CODE         VARCHAR2(10),
        WEIGHT_UOM_CODE          VARCHAR2(10),
        UNIT_WEIGHT              NUMBER,
        BUYER_ID                 NUMBER,
        VENDOR_ID                NUMBER,
        VENDOR_SITE_ID           NUMBER,
        MANUFACTURER_PART_NUMBER VARCHAR2(30),
        ORG_ID                   NUMBER,
        CATEGORY_ID              NUMBER,
        HAZARDOUS_MATERIAL_FLAG  VARCHAR2(1),
        HAZARD_CLASS_ID          NUMBER,
        LIST_PRICE               NUMBER(15,5),
        MARKET_PRICE             NUMBER(10,2),
        ITEM_COST                NUMBER(15,5) ,
        RADIO_GROUP_VALUE        VARCHAR2(1),
        TRANSACTION_ID           NUMBER,
        ORGANIZATION_CODE        VARCHAR2(30),
        STATIC FUNCTION g_miss_null RETURN xxwc_parts_on_fly_rec
            );
/

CREATE OR REPLACE TYPE BODY xxwc.xxwc_parts_on_fly_rec
AS
   STATIC FUNCTION g_miss_null
       RETURN xxwc_parts_on_fly_rec
   AS
   BEGIN
      RETURN xxwc_parts_on_fly_rec ( ITEM_NUMBER              => NULL,
                                ITEM_DESCRIPTION         => NULL,
                                INVENTORY_ITEM_ID        => NULL,
                                MST_ORGANIZATION_ID      => NULL,
                                ASSGN_ORGANIZATION_ID    => NULL,
                                PRICE_LIST_HEADER_ID     => NULL,
                                ITEM_TYPE_CODE           => NULL,
                                ITEM_TEMPLATE_ID         => NULL,
                                PRIMARY_UOM_CODE         => NULL,
                                WEIGHT_UOM_CODE          => NULL,
                                UNIT_WEIGHT              => NULL,
                                BUYER_ID                 => NULL,
                                VENDOR_ID                => NULL,
                                VENDOR_SITE_ID           => NULL,
                                MANUFACTURER_PART_NUMBER => NULL,
                                ORG_ID                   => NULL,
                                CATEGORY_ID              => NULL,
                                HAZARDOUS_MATERIAL_FLAG  => NULL,
                                HAZARD_CLASS_ID          => NULL,
                                LIST_PRICE               => NULL,
                                MARKET_PRICE             => NULL,
                                ITEM_COST                => NULL,
                                RADIO_GROUP_VALUE        => NULL,
                                TRANSACTION_ID           => NULL,
                                ORGANIZATION_CODE        => NULL
                                   );
   END g_miss_null;
END;
/

CREATE PUBLIC SYNONYM xxwc_parts_on_fly_rec FOR xxwc.xxwc_parts_on_fly_rec;

show err;
EXIT;