/*************************************************************************
*   PROCEDURE Name: xxwc_dms_doclink_tbl
*
*   PURPOSE:   Added additional columns for xxwc_dms_doclink_tbl table

HISTORY
===============================================================================
VERSION DATE          AUTHOR(S)              DESCRIPTION
------- -----------   --------------- -----------------------------------------
1.0     11/10/2017    Pattabhi Avula      TMS#20171003-00065Initial Release

*************************************************************************/

ALTER TABLE xxwc.xxwc_dms_doclink_tbl ADD(CREATION_DATE	DATE, CREATED_BY	NUMBER(15),LAST_UPDATE_DATE	DATE,LAST_UPDATED_BY	NUMBER(15));
/