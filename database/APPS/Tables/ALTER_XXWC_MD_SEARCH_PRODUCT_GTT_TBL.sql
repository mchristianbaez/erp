/*************************************************************************
  $Header ALTER_XXWC_MD_SEARCH_PRODUCT_GTT_TBL.sql $
  Module Name: ALTER_XXWC_MD_SEARCH_PRODUCT_GTT_TBL.sql

  PURPOSE:   To Add the column open_sales_orders

  REVISIONS:
  Ver        Date        Author                     Description
  ---------  ----------  ---------------         -------------------------
  1.0        03/25/2016  Rakesh Patel            TMS 20151023-00037/20160328-00211 - Shipping Extension Modification for Counter Order Removal and PUBD
**************************************************************************/
ALTER TABLE XXWC.XXWC_MD_SEARCH_PRODUCT_GTT_TBL ADD (open_sales_orders NUMBER);


