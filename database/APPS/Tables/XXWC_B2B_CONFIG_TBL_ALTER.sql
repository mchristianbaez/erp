/******************************************************************************
      $Header XXWC_B2B_CONFIG_TBL_ALTER.sql $
      Module Name: XXWC_B2B_CONFIG_TBL.sql

      PURPOSE:   This XXWC_B2B_CONFIG_TBL is altered with new column like 
	             business_event_eligible and this table is used for AR 
				 customer b2b setup XXWC_AR_CUST_B2B_SETUP_PKG package
				 and triggered by oracle.apps.ar.hz.CustAccount.create event

      REVISIONS:
      Ver        Date         Author                 Description
      ---------  ----------   ---------------        --------------------------
      1.0        22-Aug-2017  Pattabhi Avula         TMS#20170804-00096 - Add 
	                                                 Party ID for Clark Account
	                                                 Initial version
*******************************************************************************/
ALTER TABLE XXWC.XXWC_B2B_CONFIG_TBL ADD(business_event_eligible VARCHAR2(5) DEFAULT 'N')
/