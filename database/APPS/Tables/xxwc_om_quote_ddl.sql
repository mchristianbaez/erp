CREATE TABLE xxwc.xxwc_om_quote_headers
(
   quote_number        NUMBER
 , valid_until         DATE
 , cust_account_id     NUMBER
 , primary_site_use_id NUMBER
 , site_use_id         NUMBER
 , contact_id          NUMBER
 , salesrep_id         NUMBER
 , shipping_method     VARCHAR2 (100)
 , organization_id     NUMBER
 , line_amount         NUMBER
 , tax_amount          NUMBER
 , gross_margin        NUMBER
 , notes               VARCHAR2 (2000)
 , attribute_category      VARCHAR2 (30)
 , attribute1              VARCHAR2 (150)
 , attribute2              VARCHAR2 (150)
 , attribute3              VARCHAR2 (150)
 , attribute4              VARCHAR2 (150)
 , attribute5              VARCHAR2 (150)
 , attribute6              VARCHAR2 (150)
 , attribute7              VARCHAR2 (150)
 , attribute8              VARCHAR2 (150)
 , attribute9              VARCHAR2 (150)
 , attribute10             VARCHAR2 (150)
 , attribute11             VARCHAR2 (150)
 , attribute12             VARCHAR2 (150)
 , attribute13             VARCHAR2 (150)
 , attribute14             VARCHAR2 (150)
 , attribute15             VARCHAR2 (150)
 , creation_date       DATE
 , created_by          NUMBER
 , last_update_date    DATE
 , last_updated_by     NUMBER
 , last_update_login   NUMBER
)
/


CREATE TABLE xxwc.xxwc_om_quote_lines
(
   quote_number        NUMBER
 , line_seq            NUMBER
 --, organization_id     NUMBER
 , inventory_item_id   NUMBER
 , item_desc           VARCHAR2 (240)
 , average_cost        NUMBER
 , special_cost        NUMBER
 , line_quantity       NUMBER
 , gm_percent          NUMBER
 , gm_selling_price    NUMBER
-- , pricing_method      VARCHAR2 (30)
-- , pricing_value       NUMBER
 , list_price          NUMBER
 , extended_amount     NUMBER
 , tax_amount          NUMBER
--, gross_margin        NUMBER
 , notes               VARCHAR2 (2000)
 , attribute_category      VARCHAR2 (30)
 , attribute1              VARCHAR2 (150)
 , attribute2              VARCHAR2 (150)
 , attribute3              VARCHAR2 (150)
 , attribute4              VARCHAR2 (150)
 , attribute5              VARCHAR2 (150)
 , attribute6              VARCHAR2 (150)
 , attribute7              VARCHAR2 (150)
 , attribute8              VARCHAR2 (150)
 , attribute9              VARCHAR2 (150)
 , attribute10             VARCHAR2 (150)
 , attribute11             VARCHAR2 (150)
 , attribute12             VARCHAR2 (150)
 , attribute13             VARCHAR2 (150)
 , attribute14             VARCHAR2 (150)
 , attribute15             VARCHAR2 (150)
 , creation_date       DATE
 , created_by          NUMBER
 , last_update_date    DATE
 , last_updated_by     NUMBER
 , last_update_login   NUMBER
)
/

CREATE SEQUENCE xxwc.xxwc_om_quote_header_s
/

CREATE UNIQUE INDEX xxwc.xxwc_om_quote_headers_u1
   ON xxwc.xxwc_om_quote_headers (quote_number)
/

CREATE UNIQUE INDEX xxwc.xxwc_om_quote_lines_u1
   ON xxwc.xxwc_om_quote_lines (quote_number, line_seq)
/

CREATE UNIQUE INDEX xxwc.xxwc_om_quote_lines_u2
   ON xxwc.xxwc_om_quote_lines (quote_number, inventory_item_id)
/

CREATE SYNONYM apps.xxwc_om_quote_headers FOR xxwc.xxwc_om_quote_headers
/

CREATE SYNONYM apps.xxwc_om_quote_lines FOR xxwc.xxwc_om_quote_lines
/

CREATE SYNONYM apps.xxwc_om_quote_header_s FOR xxwc.xxwc_om_quote_header_s
/


CREATE OR REPLACE VIEW apps.xxwc_om_quote_headers_v
AS
   SELECT a.Quote_Number
        , upper(NVL (b.description, b.user_name)) Created_Name
        , (SELECT name
             FROM apps.ra_salesreps
            WHERE salesrep_id = a.salesrep_id AND ROWNUM = 1)
             salesrep_name
        , (SELECT acc.account_number || ' - ' || hzp.party_name
             FROM apps.hz_cust_accounts acc, apps.hz_parties hzp
            WHERE acc.party_id = hzp.party_id
                  AND acc.cust_account_id = a.cust_account_id)
             customer_info
        , (SELECT su.location
             FROM apps.hz_cust_site_uses_all su, apps.ar_addresses_v ad
            WHERE su.site_Use_id = a.site_use_id
                  AND su.cust_acct_site_id = ad.address_id)
             job_location
        , (SELECT org_code_name
             FROM apps.xxwc_inv_organization_v inv
            WHERE inv.organization_id = a.organization_id)
             branch
        , a.created_by
        , a.salesrep_id
        , a.cust_account_id
        , a.site_use_id
        , a.organization_id
     FROM apps.xxwc_om_quote_headers a
        , apps.fnd_user b
    WHERE     a.created_by = b.user_id
/







