--
-- XXWC_GETPAID_ARCUST_V_TBL  (Table) 
--
CREATE TABLE APPS.XXWC_GETPAID_ARCUST_V_TBL
(
  REC_LINE              VARCHAR2(2973 BYTE),
  ORG_ID                NUMBER(15),
  AMOUNT_DUE_REMAINING  NUMBER,
  ACCOUNT_NUMBER        VARCHAR2(30 BYTE)       NOT NULL
)
TABLESPACE APPS_TS_TX_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             128K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


