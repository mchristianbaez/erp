   /*************************************************************************
   *  Copyright (c) 2016 HD Supply.
   *  All rights reserved.
   **************************************************************************
   *   $Header XXWC_AR_CUST_BAL_MV_MONTHLY.sql$
   *   Module Name: XXWC_AR_CUST_BAL_MV_MONTHLY.sql
   *
   *   PURPOSE:   This script is to create a table
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0       6/1/2016     Neha Saini             TMS# 20150716-00173  get mv snapshot table
   * ******************************************************************************/
DROP TABLE XXWC.XXWC_AR_CUST_BAL_MV_MONTHLY CASCADE CONSTRAINTS;

CREATE TABLE XXWC.XXWC_AR_CUST_BAL_MV_MONTHLY
(
  CUST_ACCOUNT_ID                NUMBER,
  CUSTOMER_ACCOUNT_NUMBER        VARCHAR2(30 BYTE),
  PRISM_CUSTOMER_NUMBER          VARCHAR2(150 BYTE),
  CUSTOMER_NAME                  VARCHAR2(360 BYTE),
  BILL_TO_SITE_USE_ID            NUMBER,
  BILL_TO_PARTY_SITE_NUMBER      VARCHAR2(30 BYTE),
  BILL_TO_PRISM_SITE_NUMBER      VARCHAR2(150 BYTE),
  BILL_TO_PARTY_SITE_NAME        VARCHAR2(240 BYTE),
  BILL_TO_SITE_NAME              VARCHAR2(100 BYTE),
  TRX_BILL_TO_SITE_NAME          VARCHAR2(100 BYTE),
  TRX_PARTY_SITE_NUMBER          VARCHAR2(30 BYTE),
  TRX_PARTY_SITE_NAME            VARCHAR2(240 BYTE),
  BILL_TO_ADDRESS1               VARCHAR2(240 BYTE),
  BILL_TO_ADDRESS2               VARCHAR2(240 BYTE),
  BILL_TO_ADDRESS3               VARCHAR2(240 BYTE),
  BILL_TO_ADDRESS4               VARCHAR2(240 BYTE),
  BILL_TO_CITY                   VARCHAR2(160 BYTE),
  BILL_TO_CITY_PROVINCE          VARCHAR2(160 BYTE),
  BILL_TO_ZIP_CODE               VARCHAR2(160 BYTE),
  BILL_TO_COUNTRY                VARCHAR2(160 BYTE),
  PAYMENT_SCHEDULE_ID            NUMBER,
  CUSTOMER_TRX_ID                NUMBER,
  RCTA_CCID                      NUMBER,
  CASH_RECEIPT_ID                NUMBER,
  ACRA_CCID                      NUMBER,
  INVOICE_NUMBER                 VARCHAR2(150 BYTE),
  RECEIPT_NUMBER                 VARCHAR2(150 BYTE),
  BRANCH_LOCATION                VARCHAR2(4000 BYTE),
  TRX_NUMBER                     VARCHAR2(130 BYTE),
  TRX_DATE                       DATE,
  DUE_DATE                       DATE,
  TRX_TYPE                       VARCHAR2(120 BYTE),
  TRANSATION_BALANCE             NUMBER,
  TRANSACTION_REMAINING_BALANCE  NUMBER,
  AGE                            NUMBER,
  CURRENT_BALANCE                NUMBER,
  THIRTY_DAYS_BAL                NUMBER,
  SIXTY_DAYS_BAL                 NUMBER,
  NINETY_DAYS_BAL                NUMBER,
  ONE_EIGHTY_DAYS_BAL            NUMBER,
  THREE_SIXTY_DAYS_BAL           NUMBER,
  OVER_THREE_SIXTY_DAYS_BAL      NUMBER,
  LAST_PAYMENT_DATE              DATE,
  CUSTOMER_ACCOUNT_STATUS        VARCHAR2(180 BYTE),
  SITE_CREDIT_HOLD               VARCHAR2(54 BYTE),
  CUSTOMER_PROFILE_CLASS         VARCHAR2(124 BYTE),
  COLLECTOR_NAME                 VARCHAR2(124 BYTE),
  CREDIT_ANALYST                 VARCHAR2(360 BYTE),
  ACCOUNT_MANAGER                VARCHAR2(360 BYTE),
  ACCOUNT_BALANCE                NUMBER,
  CUST_PAYMENT_TERM              VARCHAR2(240 BYTE),
  REMIT_TO_ADDRESS_CODE          VARCHAR2(150 BYTE),
  STMT_BY_JOB                    VARCHAR2(150 BYTE),
  SEND_STATEMENT_FLAG            VARCHAR2(1 BYTE),
  SEND_CREDIT_BAL_FLAG           VARCHAR2(1 BYTE),
  ACCOUNT_CREDIT_HOLD            VARCHAR2(1 BYTE),
  TRX_CUSTOMER_ID                NUMBER,
  TRX_BILL_SITE_USE_ID           NUMBER,
  CUSTOMER_PO_NUMBER             VARCHAR2(150 BYTE),
  PMT_STATUS                     VARCHAR2(130 BYTE),
  SALESREP_NUMBER                VARCHAR2(130 BYTE),
  ACCOUNT_STATUS                 VARCHAR2(240 BYTE),
  TWELVE_MONTHS_SALES            NUMBER,
  YARD_CREDIT_LIMIT              NUMBER,
  ACCT_PHONE_NUMBER              VARCHAR2(4000 BYTE),
  TRX_BILL_TO_ADDRESS1           VARCHAR2(240 BYTE),
  TRX_BILL_TO_ADDRESS2           VARCHAR2(240 BYTE),
  TRX_BILL_TO_ADDRESS3           VARCHAR2(240 BYTE),
  TRX_BILL_TO_ADDRESS4           VARCHAR2(240 BYTE),
  TRX_BILL_TO_CITY               VARCHAR2(160 BYTE),
  TRX_BILL_TO_CITY_PROV          VARCHAR2(160 BYTE),
  TRX_BILL_TO_ZIP_CODE           VARCHAR2(160 BYTE),
  TRX_BILL_TO_COUNTRY            VARCHAR2(160 BYTE),
  IN_PROCESS_TOTAL               NUMBER,
  LARGEST_BALANCE                NUMBER,
  HIGH_CREDIT                    NUMBER,
  AUTO_APPLY_CM                  VARCHAR2(1 BYTE)
);
