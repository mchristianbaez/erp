create table xxwc.XXWC_UPDATE_CUST_PROF_AMT_TBL (CUST_ACCT_PROFILE_AMT_ID        NUMBER
                                               , OBJECT_VERSION_NUMBER           NUMBER
                                               , UPDATED_CREDIT_LIMIT            NUMBER
                                               , STATUS                          VARCHAR2(1)
                                               , ERROR_MESSAGE                   VARCHAR2(2000)
);