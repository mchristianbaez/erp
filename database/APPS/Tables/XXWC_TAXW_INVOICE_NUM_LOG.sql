-- Create Taxware Invoice Log Table 

-- Create table
create table XXWC.XXWC_TAXW_INVOICE_NUM_LOG
(
  creation_date               DATE,
  batch_source_name           VARCHAR2(50),
  interface_line_attribute2   VARCHAR2(150),
  interface_line_attribute3   VARCHAR2(150),
  orig_system_ship_address_id NUMBER(15),
  cust_trx_type_id            NUMBER(15),
  sales_order                 VARCHAR2(50),
  trx_number                  NUMBER,
  ship_date_actual            DATE,
  cust_trx_id                 NUMBER,
  orig_system_bill_contact_id NUMBER(15),
  orig_system_ship_contact_id NUMBER(15),
  purchase_order              VARCHAR2(80),
  interface_line_attribute10  VARCHAR2(150),
  warehouse_id                NUMBER(15)
);

/
CREATE INDEX XXWC.XXWC_TAXW_INVOICE_NUM_NDX ON XXWC.XXWC_TAXW_INVOICE_NUM_LOG
(INTERFACE_LINE_ATTRIBUTE2, ORIG_SYSTEM_SHIP_ADDRESS_ID, ORIG_SYSTEM_BILL_CONTACT_ID
  ,ORIG_SYSTEM_SHIP_CONTACT_ID ,PURCHASE_ORDER,INTERFACE_LINE_ATTRIBUTE10,SALES_ORDER, 
  WAREHOUSE_ID,SHIP_DATE_ACTUAL,cust_trx_type_id);
/


--Shut down the Auto Numbering Sequencing from the Batch Source.

update ra_batch_sources_all
	set auto_trx_numbering_flag = 'N'
           where name = 'STANDARD OM SOURCE'  
 	and ORG_ID = 162;


update ra_batch_sources_all
	set auto_trx_numbering_flag = 'N'
           where name = 'ORDER MANAGEMENT'  
 	and ORG_ID = 162;


update ra_batch_sources_all
	set auto_trx_numbering_flag = 'N'
           where name = 'REPAIR OM SOURCE'  
 	and ORG_ID = 162;


-- create sequence
--Create Custom Sequences

-- Order Management sequence
drop sequence apps.xxwc_taxw_ominvoices_seq;
    
CREATE SEQUENCE APPS.XXwc_TAXW_OMINVOICES_SEQ
  START WITH 10000240253
  MAXVALUE 90000112139
  MINVALUE 10000240253
  CYCLE
  NOCACHE
  NOORDER;



-- Standard OM sequence
drop sequence apps.xxwc_taxw_stdominvoices_seq;
    
CREATE SEQUENCE APPS.XXwc_TAXW_STDOMINVOICES_SEQ
  START WITH 50000157172
  MAXVALUE 90000112139
  MINVALUE 50000157172
  CYCLE
  NOCACHE
  NOORDER;
  
  
  
-- Repair OM sequence
drop sequence apps.xxwc_taxw_repominvoices_seq;
    
CREATE SEQUENCE APPS.XXwc_TAXW_REPOMINVOICES_SEQ
  START WITH 70000004524
  MAXVALUE 90000112139
  MINVALUE 70000004524
  CYCLE
  NOCACHE
  NOORDER;
  

COMMIT;

-- commit trigger



