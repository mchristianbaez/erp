--
-- XXWC_GETPAID_AREXTI_V_TBL  (Table) 
--
CREATE TABLE APPS.XXWC_GETPAID_AREXTI_V_TBL
(
  REC_LINE              VARCHAR2(2420 BYTE),
  ORG_ID                NUMBER(15),
  ACCOUNT_NUMBER        VARCHAR2(30 BYTE),
  TRX_NUMBER            VARCHAR2(30 BYTE),
  AMOUNT_DUE_REMAINING  NUMBER
)
TABLESPACE APPS_TS_TX_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             128K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


