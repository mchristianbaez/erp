/******************************************************************************
      $Header xxwc.xxwc_invoice_lines_bk_tbl.sql $
      Module Name: xxwc.xxwc_invoice_lines_bk_tbl.sql

      PURPOSE:   This table xxwc.xxwc_invoice_lines_bk_tbl will be backup of ra interface lines all table

      REVISIONS:
      Ver        Date         Author                 Description
      ---------  ----------   ---------------        --------------------------
      1.0        14-Mar-2018  Nancy Pahwa         Task ID: 20180312-00253 
*******************************************************************************/
create table xxwc.xxwc_invoice_lines_bk_tbl as select * from ra_interface_lines_all where 1 =2;