BEGIN
 
  xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'XXWC_INV_PROD_LOC_TBL');

  EXECUTE IMMEDIATE 'CREATE TABLE XXWC.XXWC_INV_PROD_LOC_TBL
         ( BUSINESS_UNIT                  VARCHAR2 (250 BYTE)
          ,SOURCE_SYSTEM                  VARCHAR2 (250 BYTE)
          ,OPERATING_UNIT_ID              NUMBER
          ,OPERATING_UNIT_NAME            VARCHAR2 (250 BYTE)
          ,INTERFACE_DATE                 DATE
          ,BRANCH_NUMBER                  VARCHAR2 (250 BYTE)
          ,BRANCH_NAME                    VARCHAR2 (250 BYTE)
          ,SU_SKU                         VARCHAR2 (250 BYTE)
          ,SKU_START_DATE                 DATE
          ,STOCK_INDICATOR                VARCHAR2 (250 BYTE)
          ,PROD_RANK                      VARCHAR2 (250 BYTE)
          ,DEPT_NUMBER                    VARCHAR2 (250 BYTE)
          ,PROD_STATUS_CODE               VARCHAR2 (250 BYTE)
          ,BUYER_NUMBER                   VARCHAR2 (250 BYTE)
          ,PO_REPLACEMENT_COST            NUMBER
          ,PO_REPL_COST_EFFECT_DATE       VARCHAR2 (250 BYTE)
          ,LIST_PRICE                     NUMBER
          ,LIST_PRICE_EFF_DATE            VARCHAR2 (250 BYTE)
          ,STD_COST_AMOUNT                NUMBER
          ,STD_COST_EFFE_DATE             VARCHAR2 (250 BYTE)
          ,BRANCH_COST                    VARCHAR2 (250 BYTE)
          ,BRANCH_COST_EFFE_DATE          VARCHAR2 (250 BYTE)
          ,PROD_VELOCITY_CODE             VARCHAR2 (250 BYTE)
          ,PROD_LINE_STATUS_CODE          VARCHAR2 (250 BYTE)
          ,PROD_LINE_STATUS_DESC          VARCHAR2 (250 BYTE)
          ,PART_VENDOR_CODE               VARCHAR2 (250 BYTE)
          ,MIN_QTY                        NUMBER
          ,MAX_QTY                        NUMBER
          ,SAFETY_STOCK                   NUMBER
          ,ORDER_LEAD_DAYS                NUMBER
          ,MINIMUM_GROSS_MARGIN           NUMBER
          ,MIN_SELL_PRICE_AMT             NUMBER
          ,LAST_COUNT_QTY                 NUMBER
          ,LAST_COUNTED_BY                VARCHAR2 (250 BYTE)
          ,LAST_COUNT_DATE                DATE
          ,LEAD_TIME_EXPIRATION_DATE      VARCHAR2 (250 BYTE)
          ,LAST_PO_RECEIPT_DATE           DATE
          ,LAST_TRANSFER_OUT_DATE         DATE
          ,LAST_TRANSFER_IN_DATE          DATE
          ,LAST_WORK_ORDER_IN_DATE        VARCHAR2 (250 BYTE)
          ,LAST_WORK_ORDER_OUT_DATE       VARCHAR2 (250 BYTE)
          ,LAST_SALES_DATE                DATE
          ,TWELVE_MONTH_WO_OUT_QTY        VARCHAR2 (250 BYTE)
          ,TWELVE_MONTH_TRX_OUT_QTY       VARCHAR2 (250 BYTE)
          ,TWELVE_MONTH_STOCK_SALE_QTY    VARCHAR2 (250 BYTE)
          ,TWELVE_MONTH_DIRECT_SALE_QTY   VARCHAR2 (250 BYTE)
          ,FORECAST_HITS                  VARCHAR2 (250 BYTE)
          ,FORECAST_PERIOD                VARCHAR2 (250 BYTE)
          ,OVERRIDE_BRANCH_HITS           VARCHAR2 (250 BYTE)
          ,TWELVE_MONTH_HITS              VARCHAR2 (250 BYTE)
          ,PRICE_SHEET                    VARCHAR2 (250 BYTE)
          ,PRICE_SHEET_EFFE_DATE          DATE
          ,WAREHOUSE_BRANCH               VARCHAR2 (250 BYTE)
          ,WAREHOUSE_BRANCH_FLAG          VARCHAR2 (250 BYTE)
          ,SEASONAL_FLAG                  VARCHAR2 (250 BYTE)
          ,TREND_PERCENT                  VARCHAR2 (250 BYTE)
          ,PRIMARY_BIN_LOCATION           VARCHAR2 (250 BYTE)
          ,PRIMARY_UNIT_OF_MEASURE        VARCHAR2 (250 BYTE)
          ,ORGANIZATION_ID                NUMBER
          ,INVENTORY_ITEM_ID              NUMBER
          ,SOURCE_SUP_PRIM_PAY_SITE       VARCHAR2 (250 BYTE))';
  
  xxwc_common_tunning_helpers.alter_table_temp ('XXWC', 'XXWC_INV_PROD_LOC_TBL');
END;
/