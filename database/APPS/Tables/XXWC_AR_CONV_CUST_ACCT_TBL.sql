/******************************************************************************************************************
   NAME:       XXWC_AR_CONV_CUST_ACCT_TBL.sql
   PURPOSE:    Table to store Conversion customer data.
   
   REVISIONS:
   Ver        Date          Author            Description
   ---------  -----------  ---------------  ---------------------------------------------------------------------
   1.0        13-Mar-2018  Ashwin Sridhar     TMS#20180308-00290 -AH HARRIS Customer Conversion
******************************************************************************************************************/
CREATE TABLE XXWC.XXWC_AR_CONV_CUST_ACCT_TBL
(RECORD_ID                     NUMBER         
,SALES_FORCE_ID                VARCHAR2(240)  
,CUSTOMER_NUMBER               VARCHAR2(240)  
,BUSINESS_NAME                 VARCHAR2(240)  
,BUSINESS_PHONE                VARCHAR2(240)  
,BUSINESS_FAX                  VARCHAR2(240)  
,EMAIL_ADDRESS                 VARCHAR2(240)  
,SALESREP_ID                   VARCHAR2(240)  
,TYPE_OF_BUSINESS              VARCHAR2(240)  
,PROFILE_CLASS                 VARCHAR2(240)  
,CREDIT_MANAGER                VARCHAR2(240)  
,CREDIT_CLASSIFICATION         VARCHAR2(240)  
,ACCOUNT_STATUS                VARCHAR2(240)  
,PREDOMINANT_TRADE             VARCHAR2(240)  
,STATUS                        VARCHAR2(240)  
,BILLING_ADDRESS               VARCHAR2(240)  
,BILLING_CITY                  VARCHAR2(240)  
,BILLING_COUNTY                VARCHAR2(240)  
,BILLING_STATE                 VARCHAR2(240)  
,BILLING_COUNTRY               VARCHAR2(240)  
,BILLING_ZIP_CODE              VARCHAR2(240)  
,SHIPPING_ADDRESS              VARCHAR2(240)  
,SHIPPING_CITY                 VARCHAR2(240)  
,SHIPPING_COUNTY               VARCHAR2(240)  
,SHIPPING_STATE                VARCHAR2(240)  
,SHIPPING_COUNTRY              VARCHAR2(240)  
,SHIPPING_ZIP_CODE             VARCHAR2(240)  
,YARD_JOB_ACCNT_PROJECT        VARCHAR2(240)  
,CREDIT_LIMIT                  VARCHAR2(240)  
,DEFAULT_JOB_CREDIT_LIMIT      VARCHAR2(240)  
,CREDIT_DECISIONING            VARCHAR2(240)  
,AP_CONTACT_FIRST_NAME         VARCHAR2(240)  
,AP_CONTACT_LAST_NAME          VARCHAR2(240)  
,AP_PHONE                      VARCHAR2(240)  
,AP_EMAIL                      VARCHAR2(240)  
,INVOICE_EMAIL_ADDRESS         VARCHAR2(240)  
,POD_EMAIL_ADDRESS             VARCHAR2(240)  
,SOA_EMAIL_ADDRESS             VARCHAR2(240)  
,PURCHASE_ORDER_REQUIRED       VARCHAR2(240)  
,DUNSNUMBER                    VARCHAR2(100)  
,PROCESS_FLAG                  VARCHAR2(10)   
,PROCESS_MESSAGE               VARCHAR2(2000)
,BILL_TO_FLAG                  VARCHAR2(1)    
,SHIP_TO_FLAG                  VARCHAR2(1)   
,CREATION_DATE                 DATE           
,CREATED_BY                    NUMBER  
,ORACLE_CUSTOMER_NUMBER        VARCHAR2(240)
,REQUEST_ID                    NUMBER
);       

CREATE OR REPLACE SYNONYM APPS.XXWC_AR_CONV_CUST_ACCT_TBL FOR XXWC.XXWC_AR_CONV_CUST_ACCT_TBL;