  /********************************************************************************
  FILE NAME: XXWC_PRODUCTS_PRICE_HW_TBL.sql
  
  PROGRAM TYPE: Product Price table for online sales order form
  
  PURPOSE: Product Price table for online sales order form
  
  HISTORY
  ==========================================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- ----------------------------------------------------
  1.0     03/07/2017    Rakesh P.       TMS#20170111-00059-Hand Write Oracle API for Web Service
                                        Initial Version
  *******************************************************************************************/
CREATE TABLE XXWC.XXWC_PRODUCTS_PRICE_HW_TBL
( 
  INVENTORY_ITEM_ID       NUMBER(15)        NOT NULL,
  ORGANIZATION_ID         NUMBER(15),
  MARKET_PRICE            NUMBER,
  ON_HAND_QTY             NUMBER(15), 
  OPEN_ORDERS_QTY         NUMBER(15),
  AVAILABLE_QTY           NUMBER(15), 
  CREATION_DATE           DATE                     NOT NULL,
  CREATED_BY              NUMBER(15)               NOT NULL,
  LAST_UPDATE_DATE        DATE                     NOT NULL,
  LAST_UPDATED_BY         NUMBER(15)               NOT NULL,
  REQUEST_ID              NUMBER(15),
  ATTRIBUTE1              VARCHAR2(150 BYTE),
  ATTRIBUTE2              VARCHAR2(150 BYTE),
  ATTRIBUTE3              VARCHAR2(150 BYTE),
  ATTRIBUTE4              VARCHAR2(150 BYTE),
  ATTRIBUTE5              VARCHAR2(150 BYTE),
  ATTRIBUTE6              VARCHAR2(150 BYTE),
  ATTRIBUTE7              VARCHAR2(150 BYTE),
  ATTRIBUTE8              VARCHAR2(150 BYTE),
  ATTRIBUTE9              VARCHAR2(150 BYTE),
  ATTRIBUTE10             VARCHAR2(150 BYTE)
);

GRANT SELECT ON XXWC.XXWC_PRODUCTS_PRICE_HW_TBL TO INTERFACE_MSSQL;

