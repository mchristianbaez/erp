/******************************************************************************
FILE NAME: XXWC_QP_TABLE_ALTER_SCRIPT.sql

PROGRAM TYPE: Table

PURPOSE: This script contains ALTER command to add new columns to QP Price List 
         Conversion staging tables.
HISTORY
===============================================================================
VERSION DATE          AUTHOR(S)              DESCRIPTION
------- -----------   --------------- -----------------------------------------
1.0     12/04/2018    Ashwin Sridhar       TMS#20180328-00040-AH HARRIS Pricing Conversion
********************************************************************************/
ALTER TABLE xxwc.xxwc_prl_header_stg ADD REQUEST_ID NUMBER;

ALTER TABLE xxwc.xxwc_prl_lines_stg ADD REQUEST_ID NUMBER;