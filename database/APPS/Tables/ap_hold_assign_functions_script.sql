/* Formatted on 18-Feb-2014 04:34:54 (QP5 v5.206) */
-- create form function first
  --select * from xxwc.xxwc_dirt_user_resp_level
DECLARE
    v_max_menu_entry    NUMBER;

    v_rowid             VARCHAR2 (124);
    v_menu_id           NUMBER;
    v_requestid         NUMBER;

    v_phase             fnd_lookups.meaning%TYPE;

    v_status            fnd_lookups.meaning%TYPE;

    v_devphase          fnd_lookups.meaning%TYPE;

    v_devstatus         fnd_lookups.meaning%TYPE;

    v_message           fnd_concurrent_requests.req_information%TYPE;
    v_resp_appl_id      NUMBER;
    v_resp_id           NUMBER;
    v_menu_enty_exist   NUMBER := 0;
 
BEGIN
    --*****************************************
    --how to use this procedure
    --find resp name
    --select *  from  apps.fnd_responsibility_tl where  responsibility_name  LIKE 'HDS Purchasing%'
    /*
    --find function_id
    SELECT function_id
      FROM fnd_form_functions_vl
     WHERE user_function_name = 'Show Invoice Line Details'
     --run it
    begin
    xxwc_add_function_to_menu (42454,'HDS Purchasing Sr Mgr - WC');
    end;
     */

    SELECT application_id, responsibility_id
      INTO v_resp_appl_id, v_resp_id
      FROM apps.fnd_responsibility_tl
     WHERE responsibility_name LIKE 'System Administrator';

    EXECUTE IMMEDIATE 'alter session set NLS_LANGUAGE = AMERICAN';

    EXECUTE IMMEDIATE 'alter session set NLS_TERRITORY = AMERICA';

    apps.fnd_global.apps_initialize (user_id => 0, resp_id => v_resp_id, resp_appl_id => v_resp_appl_id);

    FOR o IN (SELECT function_id
                FROM fnd_form_functions_vl
               WHERE user_function_name = 'XXWC AP Hold Notifications'
              UNION ALL
              SELECT function_id
                FROM fnd_form_functions_vl
               WHERE user_function_name = 'Show Invoice Line Details')
    LOOP
        FOR r
            IN (SELECT DISTINCT fm.menu_name
                  FROM apps.fnd_responsibility_tl frt
                      ,apps.fnd_responsibility fr
                      ,apps.fnd_menus_tl fmt
                      ,apps.fnd_menus fm
                      ,apps.fnd_application_tl fat
                      ,apps.fnd_application fa
                 WHERE     frt.responsibility_id(+) = fr.responsibility_id
                       AND fr.menu_id = fmt.menu_id
                       AND fr.menu_id = fm.menu_id
                       AND fat.application_id = fa.application_id
                       AND fa.application_id = fr.application_id
                       AND frt.language = 'US'
                       AND fr.responsibility_key IN (SELECT responsibility_key FROM xxwc.xxwc_dirt_user_resp_level)) --- LIKE 'HDS Purchasing%')
        LOOP
            v_menu_enty_exist := 0;
            DBMS_OUTPUT.put_line (r.menu_name);

            SELECT COUNT (1)
              INTO v_menu_enty_exist
              FROM fnd_menus fm, fnd_menu_entries fme
             WHERE fm.menu_name = r.menu_name AND fm.menu_id = fme.menu_id AND fme.function_id = o.function_id;

            IF v_menu_enty_exist = 0
            THEN
                  SELECT fm.menu_id, MAX (entry_sequence) + 101
                    INTO v_menu_id, v_max_menu_entry
                    FROM fnd_menus fm, fnd_menu_entries fme
                   WHERE fm.menu_name = r.menu_name AND fm.menu_id = fme.menu_id
                GROUP BY fm.menu_id;

                fnd_menu_entries_pkg.insert_row (x_rowid               => v_rowid
                                                ,x_menu_id             => v_menu_id
                                                ,x_entry_sequence      => v_max_menu_entry
                                                ,x_sub_menu_id         => NULL
                                                ,x_function_id         => o.function_id
                                                ,x_grant_flag          => 'Y'
                                                ,x_prompt              => NULL
                                                ,x_description         => NULL
                                                ,x_creation_date       => SYSDATE
                                                ,x_created_by          => 0
                                                ,x_last_update_date    => SYSDATE
                                                ,x_last_updated_by     => 0
                                                ,x_last_update_login   => -1);
            END IF;
        END LOOP;

        COMMIT;
        v_requestid := apps.fnd_request.submit_request (application => 'FND', program => 'FNDSCMPI', argument1 => 'No');

        DBMS_OUTPUT.put_line ('Stage 4: ... Submitted Request ID = ' || TO_CHAR (v_requestid));

        IF v_requestid = 0
        THEN
            ROLLBACK;

            raise_application_error (
                -20001
               ,   'ERROR: Running concurrent request "Compile Security" failed (Request ID = '
                || TO_CHAR (v_requestid)
                || ').');
        ELSE
            COMMIT;

            IF fnd_concurrent.wait_for_request (request_id   => v_requestid
                                               ,interval     => 5
                                               ,                                       -- seconds to wait between checks
                                                max_wait     => 9999
                                               ,                                        -- max number of seconds to wait
                                                phase        => v_phase
                                               ,status       => v_status
                                               ,dev_phase    => v_devphase
                                               ,dev_status   => v_devstatus
                                               ,MESSAGE      => v_message) = TRUE
            THEN
                IF (v_phase <> 'Completed' OR v_status <> 'Normal' OR v_message <> 'Normal completion')
                THEN
                    raise_application_error (
                        -20002
                       ,   'ERROR: Running concurrent request "Compile Security" invalid status information returned (Request ID = '
                        || TO_CHAR (v_requestid)
                        || ').');
                END IF;
            END IF;
        END IF;
    END LOOP;
                     /* Formatted on 18-Feb-2014 05:37:08 (QP5 v5.206) */
UPDATE fnd_menu_entries_vl
   SET prompt = 'XXWC AP Hold Notifications'
 WHERE function_id IN (SELECT function_id
                         FROM fnd_form_functions_vl
                        WHERE user_function_name = 'XXWC AP Hold Notifications');
                        commit;
END;
 
                                
               
 

--verify
 
SELECT *
  FROM fnd_menu_entries
 WHERE (menu_id, entry_sequence) IN
           (SELECT fm.menu_id, fme.entry_sequence
              FROM fnd_menus fm, fnd_menu_entries fme
             WHERE     fm.menu_name IN
                           (SELECT DISTINCT fm.menu_name
                              FROM apps.fnd_responsibility_tl frt
                                  ,apps.fnd_responsibility fr
                                  ,apps.fnd_menus_tl fmt
                                  ,apps.fnd_menus fm
                                  ,apps.fnd_application_tl fat
                                  ,apps.fnd_application fa
                             WHERE     frt.responsibility_id(+) = fr.responsibility_id
                                   AND fr.menu_id = fmt.menu_id
                                   AND fr.menu_id = fm.menu_id
                                   AND fat.application_id = fa.application_id
                                   AND fa.application_id = fr.application_id
                                   AND frt.language = 'US'
                                   AND fr.responsibility_key IN
                                           (SELECT responsibility_key FROM xxwc.xxwc_dirt_user_resp_level))
                   AND fm.menu_id = fme.menu_id
                   AND fme.function_id IN (SELECT function_id
                                             FROM fnd_form_functions_vl
                                            WHERE user_function_name = 'XXWC AP Hold Notifications'
                                           UNION ALL
                                           SELECT function_id
                                             FROM fnd_form_functions_vl
                                            WHERE user_function_name = 'Show Invoice Line Details'))
