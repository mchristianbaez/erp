/*
PROJECT: COSTAR REAL ESTATE MANAGEMENT SYSTEM
DATE: 06/11/2018
AUTHOR: BALA SESHADRI / ASHWIN SRIDHAR / VAMSHI SINGIRIKONDA
SCOPE: Houses the WhiteCap CoStar payments data that are about to be created as Oracle AP Invoices.
TMS # 20180709-00059
*/
--
DROP TABLE XXCUS.XXCUS_COSTAR_AP_EXPORT_INTF;
--
CREATE TABLE XXCUS.XXCUS_COSTAR_AP_EXPORT_INTF
(
  RUN_ID               NUMBER                   DEFAULT 0                     NOT NULL,
  GROUP_ID             VARCHAR2(50)             NOT NULL,
  FILE_NAME            VARCHAR2(150)            NOT NULL,
  FISCAL_PERIOD        VARCHAR2(10)             NOT NULL,
  RECORD_TYPE          VARCHAR2(30)             NOT NULL,
  INVOICE_NUMBER       VARCHAR2(30)             NOT NULL,
  INVOICE_DATE         DATE                     NOT NULL,
  LOCATION_ID          VARCHAR2(100)            NOT NULL,
  LOCATION_NAME        VARCHAR2(240)            NOT NULL,
  VENDOR_ID            VARCHAR2(30)             NOT NULL,
  ADDRESS_ID           VARCHAR2(15),
  TERMS                VARCHAR2(30),
  INVOICE_DESCRIPTION  VARCHAR2(240),
  CURRENCY             VARCHAR2(10),
  INVOICE_AMOUNT       NUMBER,
  LINE_NUMBER          NUMBER,
  LINE_DESCRIPTION     VARCHAR2(240)            NOT NULL,
  LINE_AMOUNT          NUMBER                   NOT NULL,
  GL_EXTERNAL_ID       VARCHAR2(150),	
  GL_ALLOC_CODE1       VARCHAR2(150),
  GL_ALLOC_CODE2       VARCHAR2(150),
  GL_ALLOC_CODE3       VARCHAR2(150),
  GL_ALLOC_CODE4       VARCHAR2(150),
  GL_ACCOUNT_NAME      VARCHAR2(240),
  GL_ACCOUNT_CODE      VARCHAR2(240),
  GL_EVENT_NAME        VARCHAR2(240),
  CREATED_BY           NUMBER,
  CREATION_DATE        DATE,
  LAST_UPDATED_BY      NUMBER,
  LAST_UPDATE_DATE     DATE,
  REQUEST_ID           NUMBER                   DEFAULT 0                     NOT NULL,
  STATUS               VARCHAR2(240)            DEFAULT 'NEW'                 NOT NULL,
  ORG_ID  NUMBER DEFAULT 162  NOT NULL
)
;
--
COMMENT ON TABLE XXCUS.XXCUS_COSTAR_AP_EXPORT_INTF  IS 'TMS 20180610-00001';
--
--GRANT ALL ON XXCUS.XXWC_COSTAR_AP_EXPORT_INTF TO APPS;
--