--
-- XXWC_PARAM_LIST  (Table) 
--
CREATE TABLE APPS.XXWC_PARAM_LIST
(
  LIST_ID            NUMBER,
  LIST_NAME          VARCHAR2(100 BYTE),
  LIST_VALUES        CLOB,
  LIST_TYPE          VARCHAR2(100 BYTE),
  PUBLIC_FLAG        VARCHAR2(1 BYTE),
  USER_ID            NUMBER,
  CREATED_BY         NUMBER,
  CREATION_DATE      DATE,
  LAST_UPDATE_DATE   DATE,
  LAST_UPDATED_BY    NUMBER,
  LAST_UPDATE_LOGIN  NUMBER
)
TABLESPACE XXEIS_DATA
PCTUSED    0
PCTFREE    10
INITRANS   10
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             2520K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


