/*************************************************************************
  $Header XXWC_INV_ONHAND_QUANTITY_MV_ALTER.sql $
  Module Name: XXWC_INV_ONHAND_QUANTITY_MV_ALTER.sql

  PURPOSE:   To remove Parallelism 

  REVISIONS:
  Ver        Date        Author                     Description
  ---------  ----------  ---------------         -------------------------
  1.0        04/19/2016  Rakesh Patel            TMS-20160407-00197 - Remove Parallelism - XXWC_INV_ONHAND_QUANTITY_MV
**************************************************************************/
ALTER TABLE XXWC_INV_ONHAND_QUANTITY_MV NOPARALLEL;