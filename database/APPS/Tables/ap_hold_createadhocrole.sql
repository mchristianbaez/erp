/* Formatted on 2/26/2014 2:32:20 PM (QP5 v5.206) */
DECLARE
    v_role_name   VARCHAR2 (64) := 'XXWC_APINVHDN';
    v_role_display_name   VARCHAR2 (64) := 'XXWC Invoice Hold';
BEGIN
    wf_directory.createadhocrole (role_name                 => v_role_name
                                 ,                                                      --       in out nocopy varchar2,
                                  role_display_name         => v_role_display_name
                                 ,                                                      --      in out nocopy  varchar2,
                                  language                  => NULL
                                 ,                                              --            in  varchar2 default null,
                                  territory                 => NULL
                                 ,                                                --          in  varchar2 default null,
                                  role_description          => 'Used for AP invoice Hold Workflow'
                                 ,                                                    --      in  varchar2 default null,
                                  notification_preference   => 'QUERY'
                                 ,                                                           --in varchar2 default null,
                                  role_users                => NULL
                                 ,                                                   --       in  varchar2 default null,
                                  email_address             => NULL
                                 ,                                                 --         in  varchar2 default null,
                                  fax                       => NULL
                                 ,                                                  --        in  varchar2 default null,
                                  status                    => 'ACTIVE'
                                 ,                                              --        in  varchar2 default 'ACTIVE',
                                  expiration_date           => NULL
                                 ,                                                         --     in  date default null,
                                  parent_orig_system        => 'WF_LOCAL_ROLES'
                                 ,                                                      --     in varchar2 default null,
                                  parent_orig_system_id     => 0
                                 ,                                                         --    in number default null,
                                  owner_tag                 => NULL            --            in  varchar2 default null);
                                                                   );
END;
