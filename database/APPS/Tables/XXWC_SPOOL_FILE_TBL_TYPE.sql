  /********************************************************************************
  FILE NAME: XXWC_SPOOL_FILE_TBL_TYPE.sql
  
  PROGRAM TYPE: Table Type
  
  PURPOSE: Table type 
  
  HISTORY
  ==========================================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- ----------------------------------------------------
  1.0     02/09/2017    P.Vamshidhar    TMS#20161104-00076  - SEO - Local Inventory Ads (LIA)
                                        Initial Version
  *******************************************************************************************/
CREATE OR REPLACE TYPE APPS.XXWC_SPOOL_FILE_TBL AS OBJECT
(
   file_name VARCHAR2 (150),
   total_lines NUMBER,
   session_id NUMBER,
   start_date DATE,
   end_date DATE
)
/
CREATE OR REPLACE TYPE APPS.XXWC_SPOOL_FILE_TBL_NTT
   AS TABLE OF XXWC_SPOOL_FILE_TBL
/