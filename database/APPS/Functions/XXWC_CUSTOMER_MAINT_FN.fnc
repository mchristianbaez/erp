CREATE OR REPLACE FUNCTION APPS.xxwc_customer_maint_fn(p_cust_account_id IN NUMBER)
  RETURN number AS
/****************************************************************************************
  *   $Header XXWC_PLANNING_DATE $
  *   Module Name: XXWC_PLANNING_DATE
  *
  *   PURPOSE:   PLT Need by date personalization
  *
  *   REVISIONS:
  *   Ver        Date        Author                     Description
  *   ---------  ----------  ---------------         -------------------------
  *    1.0      05/25/2016  Nancy Pahwa             Initial Version
 *****************************************************************************************/
  l_ttm_amount      NUMBER;
  l_due_amount      NUMBER;
  l_dso             NUMBER := 0;
  l_cust_account_id NUMBER;
BEGIN
  BEGIN
    l_cust_account_id := p_cust_account_id;
    BEGIN
      SELECT NVL(SUM(amount_due_original), 0)
        INTO l_ttm_amount
        FROM ar_payment_schedules
       WHERE customer_id = l_cust_account_id
         AND TRUNC(SYSDATE - trx_date) <= 90
         AND class <> 'PMT';
    
    EXCEPTION
      WHEN OTHERS THEN
        l_ttm_amount := 0;
    END;
  
    BEGIN
      SELECT NVL(SUM(amount_due_remaining), 0)
        INTO l_due_amount
        FROM ar_payment_schedules
       WHERE customer_id = l_cust_account_id;
    
    EXCEPTION
      WHEN OTHERS THEN
        l_due_amount := 0;
    END;
  
    IF l_ttm_amount <> 0 AND l_due_amount <> 0 THEN
      l_dso := ROUND(l_due_amount / (l_ttm_amount / 90), 1);
    
    else
      l_dso := 0;
    END IF;
    return(l_dso);
  exception
    when others then
      return 0;
  end;
END;
/