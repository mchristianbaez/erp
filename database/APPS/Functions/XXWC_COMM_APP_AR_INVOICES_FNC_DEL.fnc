create or replace function XXWC_COMM_APP_AR_INVOICES_FNC(p_trx_number in varchar2)
  return number is
  /*************************************************************************
    HD Supply
    All rights reserved.
   **************************************************************************

     PURPOSE: get net sales cost.

     REVISIONS:
     Ticket              Ver         Date         Author         Description                  
     ----------        ----------   --------     ---------       ---------------                                                                         
    TMS 20150818-00010    1.0        07/23/2015   Nancy Pahwa     Created      
   ************************************************************************* */  
  l_net_sales_cost number default 0;
BEGIN
  begin
    SELECT net_sales_cost into l_net_sales_cost
      FROM xxwc.xxwc_ar_prism_cls_ord_summ_stg s,
           ra_customer_trx_all                 rcta,
           ra_batch_sources_all                rbsa
     WHERE s.trx_number = rcta.trx_number
       and rcta.batch_source_id = rbsa.batch_source_id
       AND rcta.org_id = rbsa.org_id
       and s.trx_number = p_trx_number
       AND NVL(s.net_sales_cost, 0) != 0
       AND ROWNUM = 1;
       return l_net_sales_cost;
  exception
    when no_data_found then
      l_net_sales_cost := 0;
  end;
  return l_net_sales_cost;
EXCEPTION
  WHEN OTHERS THEN
    raise_application_error(-20001,
                            'XXWC_COMM_APP_AR_INVOICES_FNC: ' || SQLERRM);
END XXWC_COMM_APP_AR_INVOICES_FNC;
