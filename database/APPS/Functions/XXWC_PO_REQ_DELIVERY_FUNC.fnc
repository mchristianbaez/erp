CREATE OR REPLACE FUNCTION APPS.XXWC_PO_REQ_DELIVERY_FUNC (
   P_PO_HEADER_ID   IN NUMBER)
   RETURN VARCHAR2
/*************************************************************************************************************************
Function: XXWC_PO_REQ_DELIVERY_FUNC
Description: Function to requested date from sals order.;
HISTORY
==========================================================================================================================
VERSION DATE               AUTHOR(S)       DESCRIPTION
------- -----------------  --------------- -------------------------------------------------------------------------------
1.0     26-Sep-2017        P.Vamshidhar    Initial version TMS# 20170322-00192
                                           Purchasing:  Direct PO's not reflecting updated Expected Date on printed copy
*************************************************************************************************************************/
IS
   lvc_return_date   VARCHAR2 (100);
BEGIN
   IF P_PO_HEADER_ID IS NULL
   THEN
      lvc_return_date := NULL;
   ELSE
      SELECT TO_CHAR (request_date, 'YYYY/MM/DD HH24:MI:SS')
        INTO lvc_return_date
        FROM apps.oe_drop_ship_sources odss, apps.oe_order_headers_all ooh
       WHERE     odss.po_header_id = P_PO_HEADER_ID
             AND odss.header_id = ooh.header_id
             AND ROWNUM = 1;
   END IF;

   RETURN lvc_return_date;
EXCEPTION
   WHEN OTHERS
   THEN
      lvc_return_date := NULL;
      RETURN lvc_return_date;
END;
/