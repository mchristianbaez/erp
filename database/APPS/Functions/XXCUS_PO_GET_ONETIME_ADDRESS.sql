create or replace function APPS.XXCUS_PO_GET_ONETIME_ADDRESS
   /* ************************************************************************
    HD Supply
    All rights reserved.
   **************************************************************************

     PURPOSE: get one time address for GSC IProcurement Reqs/PO.

     REVISIONS:
     Ticket                                     Ver         Date                Author                           Description
     ---------                                  ----------  ----------        ------------------------      -------------------------
     ESMS 289744                    1.0         6/23/2015    Bala Seshadri               1. Created   
    TMS 20151007-00187    1.1         10/08/2015  Bala Seshadri               2. Improve error handling 
   ************************************************************************* */
(
 p_po_header_id        in number
,p_po_line_id          in number
,p_po_line_location_id in  number
,p_request_id in number --Ver 1.1
) return varchar2 is
  --
  PRAGMA AUTONOMOUS_TRANSACTION; --Ver 1.1
  --
    l_long_text varchar2(32767) :=Null;
  --
    l_err_msg       VARCHAR2 (2000);
    l_request_id number :=p_request_id; --Ver 1.1
    l_err_callfrom    VARCHAR2 (175) := 'apps.xxcus_po_get_onetime_address';
    l_err_callpoint   VARCHAR2 (175) := 'START';
    l_distro_list     VARCHAR2 (80) := 'HDSOracleDevelopers@hdsupply.com';
    l_module          VARCHAR2 (80) := 'HDS GSC PO PRINT'; --'HDS GSC PO'; --Ver 1.1
    l_message         VARCHAR2 (1000);
    l_sqlcode         VARCHAR2 (1000);
    l_sqlerrm         VARCHAR2 (1000);      
  --
begin
     --
     l_err_callpoint :='Calling function xxcus_po_get_onetime_address';
     --
select fdlt_f.long_text 
into   l_long_text 
from   fnd_attached_documents fad_f
,      fnd_documents fd_f
,      fnd_documents_tl fdt_f
,      fnd_document_categories_tl fdct_f
,      fnd_documents_long_text fdlt_f
where  1 = 1
and    fad_f.document_id = fd_f.document_id
and    fad_f.document_id = fdt_f.document_id
and    fdct_f.category_id = fd_f.category_id
and    fd_f.media_id = fdlt_f.media_id
and    fad_f.pk1_value =
                         (
                           select to_char(c.requisition_line_id)
                           from   po_distributions_all b
                                 ,po_req_distributions_all c
                           where 1 =1
                             and  b.po_header_id     =p_po_header_id --2242162
                             and  b.po_line_id       =p_po_line_id --4127355
                             and  b.line_location_id =p_po_line_location_id --6622210
                             and  c.distribution_id  =b.req_distribution_id
                             and  rownum <2
                         );
 --
 return l_long_text;
 --                         
exception
-- Begin Ver 1.1
when no_data_found then
        l_err_msg := substr(   'No Data Found: Error_Stack...' || --Ver 1.1
                                                 dbms_utility.format_error_stack() || ' Error_Backtrace...' || --Ver 1.1
                                                 dbms_utility.format_error_backtrace() --Ver 1.1
                                             , 1, 2000 --Ver 1.1
                                            );  --Ver 1.1    
        xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                            ,p_calling           => 'p_po_line_location_id ='||p_po_line_location_id --l_err_callpoint Ver 1.1
                                            ,p_request_id  => l_request_id --Ver 1.1                                            
                                            ,p_error_desc     => substr(sqlerrm --Ver 1.1
                                                                          ,1
                                                                          ,240) --Ver 1.1
                                            ,p_ora_error_msg  => substr(l_err_msg --Ver 1.1
                                                                          ,1
                                                                          ,2000) --Ver 1.1
                                            ,p_distribution_list => l_distro_list
                                            ,p_module            => l_module);
 commit; 
when too_many_rows then
        l_err_msg := substr(   'Too many rows: Error_Stack...' || --Ver 1.1
                                                 dbms_utility.format_error_stack() || ' Error_Backtrace...' || --Ver 1.1
                                                 dbms_utility.format_error_backtrace() --Ver 1.1
                                             , 1, 2000 --Ver 1.1
                                            );  --Ver 1.1    
        xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                            ,p_calling           => 'p_po_line_location_id ='||p_po_line_location_id --l_err_callpoint Ver 1.1
                                            ,p_request_id  => l_request_id --Ver 1.1                                            
                                            ,p_error_desc     => substr(sqlerrm --Ver 1.1
                                                                          ,1
                                                                          ,240) --Ver 1.1
                                            ,p_ora_error_msg  => substr(l_err_msg --Ver 1.1
                                                                          ,1
                                                                          ,2000) --Ver 1.1
                                            ,p_distribution_list => l_distro_list
                                            ,p_module            => l_module);
 commit; 
--End Ver 1.1 
when others then
        l_err_msg := substr(   'When-Others: Error_Stack...' || --Ver 1.1
                                                 dbms_utility.format_error_stack() || ' Error_Backtrace...' || --Ver 1.1
                                                 dbms_utility.format_error_backtrace() --Ver 1.1
                                             , 1, 2000 --Ver 1.1
                                            );  --Ver 1.1    
        xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                            ,p_calling           => 'p_po_line_location_id ='||p_po_line_location_id --l_err_callpoint Ver 1.1
                                            ,p_request_id  => l_request_id --Ver 1.1
                                            ,p_error_desc     => substr(sqlerrm --Ver 1.1
                                                                          ,1
                                                                          ,240) --Ver 1.1
                                            ,p_ora_error_msg  => substr(l_err_msg --Ver 1.1
                                                                          ,1
                                                                          ,2000) --Ver 1.1
                                            ,p_distribution_list => l_distro_list
                                            ,p_module            => l_module);
 commit;
end xxcus_po_get_onetime_address;
/