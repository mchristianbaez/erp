create or replace function APPS.XXWC_CF_FND_B2B_TEXTFormula return Varchar2 is
/****************************************************************************************************
Function: XXWC_CF_FND_B2B_TEXTFormula
Description: Function to get weekend if input date in weekend date;
HISTORY
=====================================================================================================
VERSION DATE               AUTHOR(S)       DESCRIPTION
------- -----------------  --------------- ----------------------------------------------------------
1.0     11-Aug-2017        Nancy Pahwa    Initial version Task ID: 20170220-00171
*****************************************************************************************************/

l_message varchar2(4000);
begin
  FND_MESSAGE.CLEAR;
  FND_MESSAGE.SET_NAME('XXWC','XXWC_OM_B2B_TEXT');
  l_message := FND_MESSAGE.GET;
  return (l_message);
  DBMS_OUTPUT.PUT_LINE(l_message);
end;
/
