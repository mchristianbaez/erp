CREATE OR REPLACE FUNCTION APPS.XXWC_SLA_GET_TAX_LOCATION

    (P_CUSTOMER_TRX_LINE_ID IN NUMBER)

    RETURN VARCHAR2

  /******************************************************************************
      NAME:       XXWC_SLA_GET_TAX_LOCATION

      PURPOSE:    To retrieve the Tax Location Segment2 value

      Logic:     1) Find corresponding line tied to the tax line
                 2) Grab the segment2 value from the corresponding line

      ESMS:      185888
      RFC:       35954

      REVISIONS:
      Ver        Date        Author           Description
      ---------  ----------  ---------------  ------------------------------------
      1.0        03-JAN-13   Lee Spitzer       1. Created the function
      1.1        06-FEB-13   Jonathan Gaviria  1. Deploy updated function
	  1.2        17-OCT-14   Veera 			   TMS#20141001-00168 Modified the code for Canada OU
   ******************************************************************************/

   IS

    l_segment2 VARCHAR2(25) DEFAULT 'BW080';
    l_cust_trx_line_id NUMBER;
    l_ccid NUMBER;
    l_exception exception;

BEGIN


    BEGIN

      SELECT link_to_cust_trx_line_id
      INTO   l_cust_trx_line_id
      FROM   apps.ra_customer_trx_lines
      WHERE  customer_trx_line_id = p_customer_trx_line_id
      AND    line_type = 'TAX';

    EXCEPTION

        WHEN OTHERS THEN

            raise l_exception;

    END;


    BEGIN

      SELECT code_combination_id
      INTO   l_ccid
      FROM   apps.RA_CUST_TRX_LINE_GL_DIST
      WHERE  customer_trx_line_id = l_cust_trx_line_id;

    EXCEPTION

      WHEN OTHERS THEN

        RAISE l_exception;

    END;


    BEGIN

      SELECT segment2
      INTO   l_segment2
      FROM   gl_code_combinations
      where  code_combination_id = l_ccid;

    EXCEPTION

        WHEN OTHERS THEN

            RAISE l_exception;

    END;


    RETURN  l_segment2;

EXCEPTION

    WHEN l_exception THEN

      RETURN l_segment2;

    WHEN OTHERS THEN

      RETURN l_segment2;

END;
/
