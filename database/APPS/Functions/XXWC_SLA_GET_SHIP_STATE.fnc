--
-- XXWC_SLA_GET_SHIP_STATE  (Function) 
--
CREATE OR REPLACE FUNCTION APPS.XXWC_SLA_GET_SHIP_STATE (
   P_CUSTOMER_TRX_ID        IN NUMBER
  ,P_CUSTOMER_TRX_LINE_ID   IN NUMBER)
   RETURN VARCHAR2
IS
   /******************************************************************************
      NAME:       XXWC_SLA_GET_SHIP_DATE

      PURPOSE:    To retrieve the US State from the invoice

      Logic:     1) Retrieve the ship to state from the line level
                 2) If no line ship to state found at line level, return the header level
                 3) If no ship to state defined at line or header, retrun null

      REVISIONS:
      Ver        Date        Author           Description
      ---------  ----------  ---------------  ------------------------------------
      1.0        29-FEB-12   Lee Spitzer       1. Created the function
	  1.1		 14-OCT-14	 Veera C		   TMS#20141001-00168  Modified the code as per the Canada OU Test
   ******************************************************************************/

   l_customer_trx_id   NUMBER DEFAULT NULL;        --lookup of customer_trx_id
   l_state             VARCHAR2 (60) DEFAULT NULL;            --State variable
BEGIN
   --Get the line ship to
   BEGIN
      --Get the state from the line level
      SELECT hl.STATE
        INTO l_state
        FROM apps.ra_customer_trx_lines rctla
            ,apps.hz_cust_site_uses hcsua
            ,apps.hz_cust_acct_sites hcasa
            ,hz_locations hl
            ,hz_party_sites hps
       WHERE     rctla.customer_trx_line_id = p_customer_trx_line_id
             AND rctla.ship_to_site_use_id = hcsua.site_use_id
             AND hcsua.cust_acct_site_id = hcasa.cust_acct_site_id
             AND hcasa.PARTY_SITE_ID = hps.party_site_id
             AND hl.location_id = hps.location_id;
   EXCEPTION
      WHEN OTHERS
      THEN
         l_state := NULL; ----Return Null if an exception occurs at the line ship to
   END;



   IF l_state IS NULL
   THEN
      --If we did not find a state at the customer site level and the customer trx id is not null, get the state at the header invoice level
      BEGIN
         SELECT hl.STATE
           INTO l_state
           FROM apps.ra_customer_trx rcta
               ,apps.hz_cust_site_uses hcsua
               ,apps.hz_cust_acct_sites hcasa
               ,hz_locations hl
               ,hz_party_sites hps
          WHERE     rcta.customer_trx_id = p_customer_trx_id
                AND rcta.ship_to_site_use_id = hcsua.site_use_id
                AND hcsua.cust_acct_site_id = hcasa.cust_acct_site_id
                AND hcasa.party_site_id = hps.party_site_id
                AND hl.location_id = hps.location_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_state := NULL; --Return Null if an exception occurs at the header ship to
      END;
   END IF;

   --- if l_state is still null then try the header if a customer_trx_line_id was passed

   IF l_state IS NULL
   THEN
      --If we did not find a state at the customer site level and the customer trx id is not null, get the state at the header invoice level
      BEGIN
         SELECT customer_trx_id
           INTO l_customer_trx_id
           FROM apps.ra_customer_trx_lines
          WHERE customer_trx_line_id = p_customer_trx_line_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_customer_trx_id := NULL;
      END;



      IF l_customer_trx_id IS NOT NULL
      THEN
         BEGIN
            SELECT hl.STATE
              INTO l_state
              FROM apps.ra_customer_trx rcta
                  ,apps.hz_cust_site_uses hcsua
                  ,apps.hz_cust_acct_sites hcasa
                  ,hz_locations hl
                  ,hz_party_sites hps
             WHERE     rcta.customer_trx_id = l_customer_trx_id --parameter may not be passed with p_customer_trx_id use look up from line incase of NVL
                   AND rcta.ship_to_site_use_id = hcsua.site_use_id
                   AND hcsua.cust_acct_site_id = hcasa.cust_acct_site_id
                   AND hcasa.party_site_id = hps.party_site_id
                   AND hl.location_id = hps.location_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_state := NULL; --Return Null if an exception occurs at the header ship to
         END;
      END IF;
   END IF;


   RETURN l_state;                                 --Return Null or State Code
END;
/

