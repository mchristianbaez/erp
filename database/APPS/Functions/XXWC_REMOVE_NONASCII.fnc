/* Formatted on 10-Sep-2013 16:24:09 (QP5 v5.206) */
-- Start of DDL Script for Function APPS.XXWC_REMOVE_NONASCII
-- Generated 10-Sep-2013 16:24:08 from APPS@EBIZFQA

CREATE OR REPLACE FUNCTION apps.xxwc_remove_nonascii (p_in_string VARCHAR2)
    RETURN VARCHAR2
IS
    v_return   VARCHAR2 (1024);
BEGIN
    v_return := p_in_string;

    v_return := REGEXP_REPLACE (ASCIISTR (REGEXP_REPLACE (p_in_string, '[[:cntrl:]]', NULL)), '\\[[:xdigit:]]{4}', '');

    RETURN v_return;
EXCEPTION
    WHEN OTHERS
    THEN
        RETURN v_return;
END;
/

-- End of DDL Script for Function APPS.XXWC_REMOVE_NONASCII
