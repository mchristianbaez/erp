CREATE OR REPLACE FUNCTION APPS.xxwc_om_has_cust_del_docs_fn (p_header_id in number, p_line_id in number, p_ship_from_org_id in number) return varchar2
is
	/*
		Purpose: return Y or N, depending on whether the given header/line/ship_from_org combination has
		at least one delivery document in the xxwc.xxwc_wsh_shipping_stg table.
	*/
    -- 11/14/2013 CG: TMS 20131111-00179: Function defined to remove NOT EXIST from main query in the BKO Visibility Proc to aid performance
    -- 17/10/2014 Veera TMS#20141001-00168 :Modified the code for canada OU 
	l_has_del_docs  varchar2(1);
begin
    l_has_del_docs := null;
    begin
        SELECT   'Y'
        into    l_has_del_docs
         FROM   apps.xxwc_wsh_shipping_stg x1
        WHERE   x1.header_id = p_header_id
        AND     x1.line_id = p_line_id
        AND     x1.ship_from_org_id = p_ship_from_org_id
        and     rownum = 1;
    exception
    when no_data_found then
        l_has_del_docs := 'N';
    when others then
        l_has_del_docs := 'N';
    end;
    
    return l_has_del_docs;

exception
when others then
    return 'N';
end xxwc_om_has_cust_del_docs_fn;
/


GRANT EXECUTE, DEBUG ON APPS.XXWC_OM_HAS_CUST_DEL_DOCS_FN TO DBSNMP;

