create or replace function apps.xxwc_om_get_order_qty(i_header_id in number, i_item_id in number)
return number
is
   /*************************************************************************
   *   $Header xxwc_get_order_qty.fnc$
   *   Module Name: xxwc_get_order_qty.fnc
   *
   *   PURPOSE:   This function is used to display the order qty in the RMA return LOV (ONT_RMA_ORDER_NUMBER)
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        09/01/2013  Shankar Hariharan        Initial Version
   *   1.1		  13/10/2014  Veera 				   TMS#20141001-00168 Modified the code as per the Canada OU Test 
   * ***************************************************************************/
l_ord_qty number;
begin
 if i_header_id is not null and i_item_id is not null then
  begin
  select nvl(sum(ordered_quantity),0)
    into l_ord_qty
    from apps.oe_order_lines
   where header_id=i_header_id
     and inventory_item_id=i_item_id
     and flow_status_code='CLOSED';
     return l_ord_qty;
  exception
   when others then return 0;
  end;   
 else
  return 0;
 end if;
end;
/