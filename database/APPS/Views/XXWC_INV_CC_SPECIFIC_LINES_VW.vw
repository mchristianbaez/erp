--
-- XXWC_INV_CC_SPECIFIC_LINES_VW  (View)
--

CREATE OR REPLACE FORCE VIEW APPS.XXWC_INV_CC_SPECIFIC_LINES_VW
(
   ROW_ID
  ,CC_SPECIFIC_LINE_ID
  ,CYCLECOUNT_HDR_ID
  ,TYPE
  ,INV_ORG_ID
  ,ITEM_NUMBER_CATEGORY
  ,INV_ITEM_ID
  ,CATEGORY
  ,CATEGORY_ID
  ,PROCESS_DATE
  ,LAST_UPDATE_DATE
  ,LAST_UPDATED_BY
  ,CREATION_DATE
  ,CREATED_BY
  ,LAST_UPDATE_LOGIN
  ,OBJECT_VERSION_NUMBER
  ,PROCESS_STATUS
  ,ORGANIZATION_CODE
  ,ORGANIZATION_NAME
  ,DESCRIPTION
)
AS
   SELECT lines.ROWID row_id
         ,cc_specific_line_id
         ,cyclecount_hdr_id
         ,TYPE
         ,inv_org_id
         ,DECODE (lines.inv_item_id, NULL, lines.category, lines.item_number)
             item_number_category
         ,inv_item_id
         ,category
         ,lines.category_id
         ,process_date
         ,lines.last_update_date
         ,lines.last_updated_by
         ,lines.creation_date
         ,lines.created_by
         ,lines.last_update_login
         ,lines.object_version_number
         ,lines.process_status
         ,ood.organization_code
         ,ood.organization_name
         ,DECODE (lines.inv_item_id, NULL, cat.description, msi.description)
             description
     FROM xxwc_inv_cc_specific_lines lines
         ,org_organization_definitions ood
         ,mtl_system_items msi
         ,mtl_categories_v cat
    WHERE     lines.inv_org_id = ood.organization_id
          AND lines.inv_item_id = msi.inventory_item_id(+)
          AND lines.inv_org_id = msi.organization_id(+)
          AND lines.category_id = cat.category_id(+)
          AND ood.operating_unit = fnd_profile.VALUE ('ORG_ID');


