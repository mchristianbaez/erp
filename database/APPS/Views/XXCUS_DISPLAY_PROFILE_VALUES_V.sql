
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUS_DISPLAY_PROFILE_VALUES_V" ("PROFILE_OPTION_ID", "PROFILE_OPTION_NAME", "PROFILE_NAME", "LEVEL_ID", "LEVEL_SET", "CONTEXT", "PROFILE_VALUE") AS 
  select 
p.profile_option_id profile_option_id,
p.profile_option_name profile_option_name, 
n.user_profile_option_name Profile_Name, 
v.level_id level_id,
decode(v.level_id, 10001, 'Site', 10002, 'Application', 
10003, 'Responsibility', 10004, 'User', 10005, 'Server', 
10007, 'SERVRESP', 'UnDef') LEVEL_SET, 
decode(to_char(v.level_id), '10001', '', 
'10002', app.application_name,
'10003', rsp.responsibility_name, --key, 
'10005', svr.node_name, 
'10006', org.name, 
'10004', usr.user_name, 
'10007', 'Serv/resp', 
'UnDef') "CONTEXT", 
case
 when v.profile_option_value ='Y' then 'Yes'
 when v.profile_option_value ='N' then 'No'
 else v.profile_option_value
end Profile_Value 
from fnd_profile_options p, 
fnd_profile_option_values v, 
fnd_profile_options_tl n, 
fnd_user usr,  
fnd_application_tl app,
fnd_responsibility_tl rsp, 
fnd_nodes svr, 
hr_operating_units org 
where 1 =1
--and n.user_profile_option_name in ('ECE: Input File Path','FND: Debug Log Enabled')
and p.profile_option_id = v.profile_option_id (+) 
and p.profile_option_name = n.profile_option_name 
and usr.user_id (+) = v.level_value 
and rsp.application_id (+) = v.level_value_application_id 
and rsp.responsibility_id (+) = v.level_value 
and app.application_id (+) = v.level_value 
and svr.node_id (+) = v.level_value 
and org.organization_id (+) = v.level_value 
order by level_set;
