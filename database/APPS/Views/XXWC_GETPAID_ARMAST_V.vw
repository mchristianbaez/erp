CREATE OR REPLACE VIEW APPS.XXWC_GETPAID_ARMAST_V AS
------------------------------------------------------------------------------------------------------------------------------
-- CONVERSION Invoices
-- rcta.ship_to_site_use_id IS NOT NULL
------------------------------------------------------------------------------------------------------------------------------
SELECT (RPAD(NVL(hca.account_number, ' '), 20, ' ')                                            --    CUSTOMER_NUMBER  
     || RPAD(NVL(rcta.trx_number, ' '), 20, ' ')                                               --    TRANSACTION_NUMBER
     || RPAD(NVL(TO_CHAR(rcta.trx_date, 'MMDDYYYY'), ' '),8, ' ')                              --    TRANSACTION_DATE
     || LPAD(NVL(TO_CHAR(apsa.AMOUNT_DUE_REMAINING,'9999999990.99'), '0.00'),20, ' ')          --    UNPAID_BALANCE
     || LPAD(NVL(TO_CHAR(apsa.due_date - rcta.trx_date), '0.00'),4,' ')                        --    NET_DUE_DAYS
     || LPAD(NVL(TO_CHAR(gp_dmp.invamt,'9999999990.99'),'0.00'),20,' ')                        --    TRANSACTION_AMOUNT
     || RPAD(NVL(SUBSTR(hcsu_s.location, 1, DECODE(instr(hcsu_s.location,'-',-1),0,LENGTH(hcsu_s.location),instr(hcsu_s.location,'-',-1)- 1) ), ' '), 30, ' ') --    REFERENCE_NUMBER
     || RPAD(NVL(rcta.purchase_order,' '),54,' ')                                              --    PURCHASE_ORDER_NUMBER
     || RPAD(' ', 10, ' ')                                                                     --    DIVISION 
     || LPAD(NVL( TO_CHAR(gp_dmp.tax,'9999999990.99'),'0.00'),20,' ')                          --    TAX
     || LPAD(NVL( TO_CHAR(gp_dmp.freight,'9999999990.99'), '0.00'),20,' ')                     --    FREIGHT
     || LPAD(NVL( TO_CHAR(gp_dmp.other,'9999999990.99'),'0.00'),20,' ')                        --    OTHER
     || RPAD(NVL( gp_dmp.arstat , ' '), 1, ' ')                                                --    TRANSACTION_TYPE  
     || RPAD(DECODE(NVL( hcp_s.credit_hold,hcp.credit_hold),'Y','H', 'N', 'N'),20,' ')         --    CHECK_NUMBER 
     || RPAD(' ', 10, ' ')                                                                     --    PROBLEM_NUMBER
     || RPAD(NVL((SELECT al.lookup_code
                    FROM apps.ar_receivable_applications araa
                       , ar_lookups al
                   WHERE 1 = 1 
                     AND al.lookup_type = 'XXWC_DCMT_REASON_CODES'
                     AND araa.receivable_application_id IN (SELECT MAX(receivable_application_id) 
                                                              FROM apps.ar_receivable_applications araa2
                                                             WHERE 1 = 1
                                                               AND araa.applied_customer_trx_id = araa2.applied_customer_trx_id
                                                               AND araa.org_id = araa2.org_id
                                                            )
                     AND araa.applied_customer_trx_id = rcta.customer_trx_id
                     AND araa.attribute1 = al.meaning
                     AND to_char(araa.org_id) = araa.attribute_category), ' '), 10, ' ')       --    REASON_CODE
     || RPAD(NVL((SELECT jrdv.resource_name
                   FROM jtf_rs_defresources_v jrdv
                      , apps.ra_salesreps      rsa
                  WHERE rcta.primary_salesrep_id = rsa.salesrep_id
                    AND rcta.org_id = rsa.org_id
                    AND rsa.person_id = jrdv.source_id)
        , ' '),10,' ')                                                                         --    SALES_PERSON
     || RPAD(NVL( TO_CHAR(hps_s.party_site_number) , ' '), 20, ' ')                            --    SHIP_TO_ID
     || RPAD(' ', 20, ' ')                                                                     --    SRC_INVOICE 
     || RPAD(' ', 10, ' ')                                                                     --    TRANSACTION_CURRENCY
     || LPAD(NVL( TO_CHAR(gp_dmp.invamt,'9999999990.99') , '0.00'), 20, ' ')                   --    TRANS_CURR_ORIG_AMT
     || LPAD(NVL( TO_CHAR(apsa.amount_due_remaining,'9999999990.99'), '0.00'), 20, ' ')        --    TRANSACTION_CURRENCY_BALANCE
     || LPAD('0.00', 20, ' ')                                                                  --    LOCAL_CURRENCY_ORIGINAL_AMOUNT
     || LPAD('0.00', 20, ' ')                                                                  --    LOCAL_CURRENCY_BALANCE
     || RPAD(NVL(flv.lookup_code,' '), 10, ' ')                                                --    DIVISION_CODE
     || RPAD(NVL(LPAD(gp_exti_dmp.warehse, 3, '0'), '000'), 10, ' ')                           --    SALES_AREA
     || RPAD(NVL(gp_dmp.FLEXFIELD1, ' '), 35, ' ')                                             --    FLEXFIELD1
     || RPAD(NVL(gp_dmp.FLEXFIELD2,' '), 35, ' ')                                              --    FLEXFIELD2     
     || RPAD(NVL(gp_dmp.FLEXFIELD3,' '), 35, ' ')                                              --    FLEXFIELD3
     || RPAD(NVL(gp_dmp.FLEXFIELD4,' '), 35, ' ')                                              --    FLEXFIELD4
     || RPAD(NVL(gp_dmp.FLEXFIELD5,' '), 35, ' ')                                              --    FLEXFIELD5
     || LPAD(NVL(TO_CHAR(gp_dmp.FLEXNUM1,'9999999990.99'),'0.00'), 20, ' ')                    --    FLEXNUM1
--     || LPAD(NVL(TO_CHAR(gp_dmp.FLEXNUM2,'9999999990.99'),'0.00'), 20, ' ')                    --    FLEXNUM2
     || LPAD(NVL(TO_CHAR(xxwc_ar_getpaid_ob_pkg.get_credit_limit(rcta.customer_trx_id , rcta.org_id ),'9999999990.99'),'0.00'), 20, ' ')  --    FLEXNUM2
     || LPAD(NVL(TO_CHAR(xxwc_ar_getpaid_ob_pkg.get_flexnum3(rcta.org_id
                                                           , apsa.class
                                                           , hps_s.party_site_number
                                                           , rcta.ship_to_site_use_id
                                                           , hps_s.location_id
                                                           , rcta.interface_header_attribute2
                                                           , rcta.interface_header_attribute8)
                                                     ,'9999999990.99'),' '), 20, ' ')       --    FLEXNUM3
     || LPAD(NVL(TO_CHAR(gp_dmp.FLEXNUM4,'9999999990.99'),'0.00'), 20, ' ')                    --    FLEXNUM4
     || LPAD(NVL(TO_CHAR(gp_dmp.FLEXNUM5,'9999999990.99'),'0.00'), 20, ' ')                    --    FLEXNUM5
     || RPAD(' ', 8, ' ')                                                                      --    FLEXDATE1     
     || RPAD(' ', 8, ' ')                                                                      --    FLEXDATE2
     || RPAD(' ', 8, ' ')                                                                      --    FLEXDATE3
     || RPAD(' ', 8, ' ')                                                                      --    FLEXDATE4
     || RPAD(TO_CHAR(rcta.creation_date, 'MMDDYYYY'), 8, ' ')                                  --    FLEXDATE5
     || RPAD(' ', 10, ' ')                                                                     --    DISCOUNT_CODE
     || RPAD(' ', 10, ' ')                                                                     --    AR_TARGET_SYSTEM
     || RPAD(NVL(gp_dmp.FLEXFIELD6, ' '), 50, ' ')                                             --    FLEXFIELD6
     || RPAD(NVL(gp_dmp.FLEXFIELD7, ' '), 50, ' ')                                             --    FLEXFIELD7
     || RPAD(NVL(gp_dmp.FLEXFIELD8, ' '), 50, ' ')                                             --    FLEXFIELD8
     || RPAD(NVL(flv.attribute1, ' '), 50, ' ')                                                --    FLEXFIELD9
     || RPAD(NVL(flv.attribute2, ' '), 50, ' ')                                                --    FLEXFIELD10
     || RPAD(NVL((CASE WHEN SUBSTR(hcsu_s.location, instr(hcsu_s.location,'-',-1)+ 1 ) = hps_s.party_site_number       
                  THEN
                     RPAD(' ', 50, ' ')
                  WHEN instr(hcsu_s.location,'-',-1) = 0 THEN 
                     RPAD(' ', 50, ' ')
                  ELSE
                     RPAD(SUBSTR(hcsu_s.location, instr(hcsu_s.location,'-',-1)+ 1 ), 50, ' ')  
                  END)
       , ' '), 50, ' ')                                                                        --    FLEXFIELD11
     || RPAD(NVL(hcas_s.attribute19, ' '), 50, ' ')                                            --    FLEXFIELD12
     || RPAD(NVL(substr(gp_dmp.FLEXFIELD13,1,15)||' '||SUBSTR(NVL(hcp.account_status,' '),1,1), '080' ), 50, ' ')  --    FLEXFIELD13
     || RPAD(NVL(gp_dmp.flexfield14, '0'), 50, ' ')                                            --    FLEXFIELD14
     || RPAD(NVL(xxwc_ar_getpaid_ob_pkg.get_lien_by_state(rcta.bill_to_site_use_id) , ' '), 50, ' ') --   FLEXFIELD15
     || RPAD(' ', 10, ' ')                                                                     --   JOURNAL_IDENTIFIER
      ) REC_LINE , rcta.org_id 
  FROM apps.ra_customer_trx                 rcta
     , apps.hz_cust_accounts                hca
     , apps.hz_cust_accounts                hca_s
     , apps.hz_cust_acct_sites              hcas_s
     , apps.hz_cust_site_uses               hcsu_s
     , hz_party_sites                      hps_s
--     , apps.hz_cust_accounts                hca_b
--     , apps.hz_cust_acct_sites              hcas_b
--     , apps.hz_cust_site_uses               hcsu_b
--     , hz_party_sites                      hps_b
--     , hz_locations                        hl_b
     , ra_terms                            rt
     , apps.ar_payment_schedules            apsa
     , apps.ra_cust_trx_types               rctt
     , fnd_lookup_values_vl                flv
     , hz_customer_profiles                hcp
     , hz_customer_profiles                hcp_s
     -- , ra_batch_sources_all         rbsa
     , apps.xxwc_armast_getpaid_dump_tbl gp_dmp
     , apps.xxwc_arexti_getpaid_dump_tbl gp_exti_dmp
 WHERE 1 = 1
   AND hca.cust_account_id = rcta.bill_to_customer_id
   -- AND rcta.batch_source_id        = rbsa.batch_source_id
   -- AND rcta.org_id                 = rbsa.org_id
   AND xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id )   = 'CONVERSION'
   AND hca_S.cust_account_id = rcta.ship_to_customer_id
   AND hcas_s.cust_acct_site_id = hcsu_s.cust_acct_site_id
   AND hcas_s.party_site_id =  hps_s.party_site_id
   AND rcta.ship_to_customer_id IS NOT NULL
   AND rcta.ship_to_site_use_id = hcsu_s.site_use_id
--   AND rcta.bill_to_site_use_id = hcsu_b.site_use_id
--   AND hcas_b.cust_acct_site_id = hcsu_b.cust_acct_site_id
--   AND hcas_b.party_site_id =  hps_b.party_site_id
--   AND hca_b.cust_account_id = rcta.bill_to_customer_id
--   AND hps_b.location_id = hl_b.location_id
   AND hca.cust_account_id = hcp.cust_account_id
   AND hcp.site_use_id IS  NULL
   AND rcta.ship_to_site_use_id = hcp_s.site_use_id   (+) 
   AND rcta.cust_trx_type_id = rctt.cust_trx_type_id
   AND apsa.customer_trx_id = rcta.customer_trx_id
   AND rcta.term_id = rt.term_id (+)
   AND rcta.org_id = rctt.org_id
   AND flv.lookup_type = 'XXWC_GETPAID_REMIT_TO_CODES'
   AND NVL(hcp.attribute2, '2') = flv.lookup_code
   AND apsa.status = 'OP' AND apsa.amount_due_remaining != 0 AND apsa.org_id = rcta.org_id
   AND rcta.trx_number = gp_dmp.invno
   AND hca.account_number = gp_dmp.custno
   AND rcta.trx_number = gp_exti_dmp.invno
   AND hca.account_number = gp_exti_dmp.custno
UNION
------------------------------------------------------------------------------------------------------------------------------
-- NON-CONVERSION Invoices
-- rcta.ship_to_site_use_id IS NOT NULL
------------------------------------------------------------------------------------------------------------------------------
SELECT (RPAD(NVL(hca.account_number, ' '), 20, ' ')                                            --    CUSTOMER_NUMBER  
     || RPAD(NVL(rcta.trx_number, ' '), 20, ' ')                                               --    TRANSACTION_NUMBER
     || RPAD(NVL(TO_CHAR(rcta.trx_date, 'MMDDYYYY'), ' '),8, ' ')                              --    TRANSACTION_DATE
     || LPAD(NVL(TO_CHAR(apsa.amount_due_remaining,'9999999990.99'), '0.00'),20, ' ')          --    UNPAID_BALANCE
     || LPAD(NVL(TO_CHAR(apsa.due_date - rcta.trx_date), '0.00'),4,' ')                        --    NET_DUE_DAYS
     || LPAD(NVL(TO_CHAR(apsa.amount_due_original,'9999999990.99'),'0.00'),20,' ')             --    TRANSACTION_AMOUNT
     || RPAD(NVL(SUBSTR(hcsu_s.location, 1, DECODE(instr(hcsu_s.location,'-',-1),0,LENGTH(hcsu_s.location),instr(hcsu_s.location,'-',-1)- 1) ), ' '), 30, ' ') --    REFERENCE_NUMBER
     || RPAD(NVL(rcta.purchase_order,' '),54,' ')                                              --    PURCHASE_ORDER_NUMBER
     || RPAD(' ', 10, ' ')                                                                     --    DIVISION 
     || LPAD(NVL( TO_CHAR(apsa.tax_original,'9999999990.99'),'0.00'),20,' ')                   --    TAX
     || LPAD(NVL( TO_CHAR(apsa.freight_original,'9999999990.99'), '0.00'),20,' ')              --    FREIGHT
     || LPAD(NVL( TO_CHAR(apsa.amount_due_original -(apsa.amount_line_items_original + apsa.tax_original + apsa.freight_original),'9999999990.99'),'0.00'),20,' ')   --    OTHER
     || RPAD(NVL( DECODE(rctt.name,'Interest Invoice','F',rctt.name)  , ' '), 1, ' ')          --    TRANSACTION_TYPE   
     || RPAD(DECODE(NVL( hcp_s.credit_hold,hcp.credit_hold),'Y','H', 'N', 'N'),20,' ')         --    CHECK_NUMBER -- ?????
     || RPAD(' ', 10, ' ')                                                                     --    PROBLEM_NUMBER
     || RPAD(NVL((SELECT al.lookup_code
                    FROM apps.ar_receivable_applications araa
                       , ar_lookups al
                   WHERE 1 = 1 
                     AND al.lookup_type = 'XXWC_DCMT_REASON_CODES'
                     AND araa.receivable_application_id IN (SELECT MAX(receivable_application_id) 
                                                              FROM apps.ar_receivable_applications araa2
                                                             WHERE 1 = 1
                                                               AND araa.applied_customer_trx_id = araa2.applied_customer_trx_id
                                                               AND araa.org_id = araa2.org_id
                                                            )
                     AND araa.applied_customer_trx_id = rcta.customer_trx_id
                     AND araa.attribute1 = al.meaning
                     AND to_char(araa.org_id) = araa.attribute_category), ' '), 10, ' ')       --    REASON_CODE
     || RPAD(NVL((SELECT jrdv.resource_name
                   FROM jtf_rs_defresources_v jrdv
                      , apps.ra_salesreps      rsa
                  WHERE rcta.primary_salesrep_id = rsa.salesrep_id
                    AND rcta.org_id = rsa.org_id                    AND rsa.person_id = jrdv.source_id)
        , ' '),10,' ')                                                                         --    SALES_PERSON
     || RPAD(NVL( TO_CHAR(hps_s.party_site_number) , ' '), 20, ' ')                            --    SHIP_TO_ID
     || RPAD(' ', 20, ' ')                                                                     --    SRC_INVOICE 
     || RPAD(' ', 10, ' ')                                                                     --    TRANSACTION_CURRENCY
     || LPAD(NVL( TO_CHAR(apsa.amount_due_original,'9999999990.99') , '0.00'), 20, ' ')        --    TRANS_CURR_ORIG_AMT
     || LPAD(NVL( TO_CHAR(apsa.amount_due_remaining,'9999999990.99'), '0.00'), 20, ' ')        --    TRANSACTION_CURRENCY_BALANCE
     || LPAD('0.00', 20, ' ')                                                                  --    LOCAL_CURRENCY_ORIGINAL_AMOUNT
     || LPAD('0.00', 20, ' ')                                                                  --    LOCAL_CURRENCY_BALANCE
     || RPAD(NVL(flv.lookup_code,' '), 10, ' ')                                                --    DIVISION_CODE
     || RPAD(NVL((CASE WHEN xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id ) IN ('ORDER MANAGEMENT','STANDARD OM SOURCE', 'REPAIR OM SOURCE')
                  THEN (SELECT LPAD(ood.organization_code,3,'0')
                          FROM apps.oe_order_headers           ooha
                             , org_organization_definitions   ood
                         WHERE ooha.order_number            = NVL(rcta.interface_header_attribute1, rcta.ct_reference)
                           AND ooha.ship_from_org_id        = ood.organization_id
                           AND ROWNUM = 1
                       )
                  WHEN xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id ) = 'PRISM'
                  THEN lpad(rcta.interface_header_attribute7,3,'0')
                  ELSE ' '
                  END),' '), 10, ' ')                                                             --    SALES_AREA
     || RPAD(NVL((case when xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id ) in ('ORDER MANAGEMENT','STANDARD OM SOURCE', 'REPAIR OM SOURCE')
                  then (select DECODE(COUNT(1),0,'INVOICE','RENTAL INVOICE')
                        from apps.oe_order_headers      ooha
                           , oe_transaction_types_tl  ott
                       where ooha.order_number = NVL(rcta.interface_header_attribute1, rcta.ct_reference) 
                       and ooha.order_type_id = ott.transaction_type_id
                       and ott.name IN ('WC SHORT TERM RENTAL', 'WC LONG TERM RENTAL')
                       and rownum = 1
                       )
                when xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id ) = 'PRISM'
                then rcta.interface_header_attribute8
                else 'INVOICE'
                end),' '), 35, ' ')                                                             --    FLEXFIELD1
     || RPAD(NVL((case when xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id ) in ('ORDER MANAGEMENT','STANDARD OM SOURCE', 'REPAIR OM SOURCE')
                  then (select xxwc_mv_routines_pkg.get_user_employee(ooha.created_by)
                        from apps.oe_order_headers ooha
                       where ooha.order_number = NVL(rcta.interface_header_attribute1, rcta.ct_reference) 
                       and rownum = 1)
                when xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id ) = 'PRISM'
                then rcta.interface_header_attribute10
                else NULL
                end),' '), 35, ' ')                                                             --    FLEXFIELD2     
     || RPAD(NVL((case when xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id ) in ('ORDER MANAGEMENT','STANDARD OM SOURCE', 'REPAIR OM SOURCE')
                  then (select xxwc_mv_routines_pkg.get_contact_name(ooha.invoice_to_contact_id)
                        from apps.oe_order_headers ooha
                       where ooha.order_number = NVL(rcta.interface_header_attribute1, rcta.ct_reference) 
                       and rownum = 1)
                when xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id ) = 'PRISM'
                then rcta.interface_header_attribute9
                else xxwc_mv_routines_pkg.get_contact_name(rcta.bill_to_contact_id)
                end)
                ,' '), 35, ' ')                                                                --    FLEXFIELD3
     || RPAD(NVL((case when xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id ) in ('ORDER MANAGEMENT','STANDARD OM SOURCE', 'REPAIR OM SOURCE')
                  then (select oola.revrec_signature
                        from apps.oe_order_headers ooha
                               , apps.oe_order_lines oola
                       where ooha.order_number = NVL(rcta.interface_header_attribute1, rcta.ct_reference)
                       and    ooha.header_id = oola.header_id
                       and    oola.revrec_signature is not null 
                       and rownum = 1)
                when xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id ) = 'PRISM'
                then rcta.interface_header_attribute11
                else NULL
                end)
                                 ,' '), 35, ' ')                                               --    FLEXFIELD4
     || RPAD(NVL((CASE WHEN xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id ) IN ('ORDER MANAGEMENT','STANDARD OM SOURCE', 'REPAIR OM SOURCE')
                  THEN (SELECT RPAD(NVL(houv.location_code, ' '), 35, ' ')
                          FROM apps.oe_order_headers           ooha
                             , hr_organization_units_v        houv
                         WHERE ooha.order_number            = NVL(rcta.interface_header_attribute1, rcta.ct_reference)
                           AND ooha.ship_from_org_id        = houv.organization_id
                           AND ROWNUM = 1
                       )
                  WHEN xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id ) = 'PRISM'
                  THEN (SELECT RPAD(NVL(hla.location_code, ' '), 35, ' ')
                          FROM apps.hr_locations hla
                         WHERE SUBSTR(location_code, 1, INSTR(location_code, '-') - 2) = lpad(rcta.interface_header_attribute7,3,'0')
                           AND ROWNUM = 1)
                  ELSE ' '
                  END),' '), 35, ' ')                                                          --    FLEXFIELD5
     || LPAD(NVL(ROUND((NVL(apsa.tax_original,0)/DECODE(apsa.amount_due_original - NVL(apsa.tax_original,0) - NVL(apsa.freight_original,0), 0 , 1))*100, 2),'0.00'), 20, ' ')  --    FLEXNUM1
/*     || LPAD(NVL((SELECT to_char(hcpa_s.overall_credit_limit)
                  FROM hz_cust_profile_amts hcpa_s
                 WHERE hcpa_s.site_use_id = hcsu_s.site_use_id)
                , '0.00')
             , 20, ' ')                                                                        --    FLEXNUM2 -- ????? Credit Limit for Ship To
*/
     || LPAD(NVL(TO_CHAR(xxwc_ar_getpaid_ob_pkg.get_credit_limit(rcta.customer_trx_id , rcta.org_id ),'9999999990.99'),'0.00'), 20, ' ')  --    FLEXNUM2
     || LPAD(NVL(TO_CHAR(xxwc_ar_getpaid_ob_pkg.get_flexnum3(rcta.org_id
                                                           , apsa.class
                                                           , hps_s.party_site_number
                                                           , rcta.ship_to_site_use_id
                                                           , hps_s.location_id
                                                           , rcta.interface_header_attribute2
                                                           , rcta.interface_header_attribute8)
                                                     ,'9999999990.99'),' '), 20, ' ')       --    FLEXNUM3
     || LPAD(NVL(TO_CHAR((apsa.amount_due_original - NVL(apsa.tax_original,0)),'9999999990.99'),'0.00'), 20, ' ')  --    FLEXNUM4
     || LPAD(NVL(TO_CHAR(apsa.tax_original,'9999999990.99'),'0.00'), 20, ' ')                  --    FLEXNUM5
     || RPAD(' ', 8, ' ')                                                                      --    FLEXDATE1     
     || RPAD(' ', 8, ' ')                                                                      --    FLEXDATE2
     || RPAD(' ', 8, ' ')                                                                      --    FLEXDATE3
     || RPAD(' ', 8, ' ')                                                                      --    FLEXDATE4
     || RPAD(TO_CHAR(rcta.creation_date, 'MMDDYYYY'), 8, ' ')                                  --    FLEXDATE5
     || RPAD(' ', 10, ' ')                                                                     --    DISCOUNT_CODE
     || RPAD(' ', 10, ' ')                                                                     --    AR_TARGET_SYSTEM
     || RPAD(NVL((CASE WHEN xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id ) IN ('ORDER MANAGEMENT','STANDARD OM SOURCE', 'REPAIR OM SOURCE')
                  THEN (SELECT RPAD(NVL(houv.telephone_number_1, ' '), 50, ' ')                                       --    FLEXFIELD6
                            || RPAD(NVL(houv.address_line_1, ' '), 50, ' ')                                           --    FLEXFIELD7
                            || RPAD( NVL(houv.town_or_city||','||houv.region_2||' '||houv.postal_code, ' '), 50, ' ') --    FLEXFIELD8
                          FROM apps.oe_order_headers           ooha
                             , hr_organization_units_v        houv
                         WHERE ooha.order_number            = NVL(rcta.interface_header_attribute1, rcta.ct_reference)
                           AND ooha.ship_from_org_id        = houv.organization_id
                           AND ROWNUM = 1
                       )
                  WHEN xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id ) = 'PRISM'
                  THEN (SELECT RPAD(NVL(hla.telephone_number_1, ' '), 50, ' ')                                       --    FLEXFIELD6
                            || RPAD(NVL(hla.address_line_1, ' '), 50, ' ')                                           --    FLEXFIELD7
                            || RPAD(NVL(hla.town_or_city||','||hla.region_2||' '||hla.postal_code, ' '), 50, ' ')    --    FLEXFIELD8
                          FROM apps.hr_locations hla
                             , hr_all_organization_units hou
                         WHERE SUBSTR(location_code, 1, INSTR(location_code, '-') - 2) = lpad(rcta.interface_header_attribute7,3,'0')
                           AND SYSDATE BETWEEN hou.date_from AND NVL(hou.date_to,SYSDATE + 1)
                           AND hla.location_id = hou.location_id
                           AND ROWNUM = 1)
                  ELSE ' '
                  END),' '), 150, ' ')
     || RPAD(NVL(flv.attribute1, ' '), 50, ' ')                                                  --    FLEXFIELD9
     || RPAD(NVL(flv.attribute2, ' '), 50, ' ')                                                  --    FLEXFIELD10
     || RPAD(NVL((CASE WHEN SUBSTR(hcsu_s.location, instr(hcsu_s.location,'-',-1)+ 1 ) = hps_s.party_site_number       
                  THEN
                     RPAD(' ', 50, ' ')
                  WHEN instr(hcsu_s.location,'-',-1) = 0 THEN 
                     RPAD(' ', 50, ' ')
                  ELSE
                     RPAD(SUBSTR(hcsu_s.location, instr(hcsu_s.location,'-',-1)+ 1 ), 50, ' ')  
                  END)
       , ' '), 50, ' ')                                                                        --    FLEXFIELD11
     || RPAD(NVL(hcas_s.attribute19, ' '), 50, ' ')                                              --    FLEXFIELD12
     || RPAD(NVL((CASE WHEN xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id ) IN ('ORDER MANAGEMENT','STANDARD OM SOURCE', 'REPAIR OM SOURCE')
                  THEN (SELECT LPAD(ood.organization_code,3,'0')
                          FROM apps.oe_order_headers           ooha
                             , org_organization_definitions   ood
                         WHERE ooha.order_number            = NVL(rcta.interface_header_attribute1, rcta.ct_reference)
                           AND ooha.ship_from_org_id        = ood.organization_id
                           AND ROWNUM = 1
                       )
                  WHEN xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id ) = 'PRISM'
                  THEN lpad(NVL(rcta.interface_header_attribute7,'080'),3,'0')
                  ELSE '080'
                  END)||'  '||RPAD(hps_s.party_site_number,11,' '),' ')||SUBSTR(NVL(SUBSTR(NVL(hcp.account_status,' '),1,1),' '),1,1), 50, ' ')  --   FLEXFIELD13
     || RPAD('0', 50, ' ')                                                                     --    FLEXFIELD14
     || RPAD(NVL(xxwc_ar_getpaid_ob_pkg.get_lien_by_state(rcta.bill_to_site_use_id) , ' '), 50, ' ') --   FLEXFIELD15
     || RPAD(' ', 10, ' ')                                                                     --    JOURNAL_IDENTIFIER
      ) REC_LINE , rcta.org_id
  FROM apps.ra_customer_trx          rcta
     , apps.hz_cust_accounts         hca
     , apps.hz_cust_accounts         hca_s
     , apps.hz_cust_acct_sites       hcas_s
     , apps.hz_cust_site_uses        hcsu_s
     , hz_party_sites               hps_s
--     , apps.hz_cust_accounts         hca_b
--     , apps.hz_cust_acct_sites       hcas_b
--     , apps.hz_cust_site_uses        hcsu_b
--     , hz_party_sites               hps_b
--     , hz_locations                 hl_b
     , ra_terms                     rt
     , apps.ar_payment_schedules     apsa
     , apps.ra_cust_trx_types        rctt
     -- , ra_batch_sources_all         rbsa
     , fnd_lookup_values_vl         flv
     , hz_customer_profiles         hcp
     , hz_customer_profiles         hcp_s
 WHERE 1 = 1
   -- AND rcta.batch_source_id                      = rbsa.batch_source_id
   -- AND rcta.org_id                               = rbsa.org_id
   AND xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id )   <> 'CONVERSION'
   AND hca.cust_account_id                       = rcta.bill_to_customer_id
   AND rcta.ship_to_customer_id                  IS NOT NULL
   AND rcta.ship_to_site_use_id                  = hcsu_s.site_use_id
   AND hca_S.cust_account_id                     = rcta.ship_to_customer_id
   AND hcas_s.cust_acct_site_id                  = hcsu_s.cust_acct_site_id
   AND hcas_s.party_site_id                      = hps_s.party_site_id
   AND rcta.ship_to_site_use_id                  = hcsu_s.site_use_id
--   AND rcta.bill_to_site_use_id                  = hcsu_b.site_use_id
--   AND hcas_b.cust_acct_site_id                  = hcsu_b.cust_acct_site_id
--   AND hcas_b.party_site_id                      = hps_b.party_site_id
--   AND hca_b.cust_account_id                     = rcta.bill_to_customer_id
--   AND hps_b.location_id                         = hl_b.location_id
   AND hca.cust_account_id                       = hcp.cust_account_id
   AND hcp.site_use_id                          IS NULL
   AND rcta.ship_to_site_use_id                  = hcp_s.site_use_id   (+) 
   AND rcta.cust_trx_type_id                     = rctt.cust_trx_type_id
   AND apsa.customer_trx_id                      = rcta.customer_trx_id
   AND rcta.term_id                              = rt.term_id (+)
   AND rcta.org_id                               = rctt.org_id
   AND flv.lookup_type                           = 'XXWC_GETPAID_REMIT_TO_CODES'
   AND NVL(hcp.attribute2, '2')                  = flv.lookup_code
   AND apsa.status                               = 'OP' 
   AND apsa.amount_due_remaining                != 0 
   AND apsa.org_id                               = rcta.org_id
UNION
------------------------------------------------------------------------------------------------------------------------------
-- CONVERSION Invoices
-- rcta.ship_to_address_id IS NOT NULL
------------------------------------------------------------------------------------------------------------------------------
SELECT (RPAD(NVL(hca.account_number, ' '), 20, ' ')                                            --    CUSTOMER_NUMBER  
     || RPAD(NVL(rcta.trx_number, ' '), 20, ' ')                                               --    TRANSACTION_NUMBER
     || RPAD(NVL(TO_CHAR(rcta.trx_date, 'MMDDYYYY'), ' '),8, ' ')                              --    TRANSACTION_DATE
     || LPAD(NVL(TO_CHAR(apsa.AMOUNT_DUE_REMAINING,'9999999990.99'), '0.00'),20, ' ')          --    UNPAID_BALANCE
     || LPAD(NVL(TO_CHAR(apsa.due_date - rcta.trx_date), '0.00'),4,' ')                        --    NET_DUE_DAYS
     || LPAD(NVL(TO_CHAR(gp_dmp.invamt,'9999999990.99'),'0.00'),20,' ')                        --    TRANSACTION_AMOUNT
     || RPAD(NVL(SUBSTR(hcsu_s.location, 1, DECODE(instr(hcsu_s.location,'-',-1),0,LENGTH(hcsu_s.location),instr(hcsu_s.location,'-',-1)- 1) ), ' '), 30, ' ') --    REFERENCE_NUMBER
     || RPAD(NVL(rcta.purchase_order,' '),54,' ')                                              --    PURCHASE_ORDER_NUMBER
     || RPAD(' ', 10, ' ')                                                                     --    DIVISION 
     || LPAD(NVL( TO_CHAR(gp_dmp.tax,'9999999990.99'),'0.00'),20,' ')                          --    TAX
     || LPAD(NVL( TO_CHAR(gp_dmp.freight,'9999999990.99'), '0.00'),20,' ')                     --    FREIGHT
     || LPAD(NVL( TO_CHAR(gp_dmp.other,'9999999990.99'),'0.00'),20,' ')                        --    OTHER
     || RPAD(NVL( gp_dmp.arstat  , ' '), 1, ' ')                                               --    TRANSACTION_TYPE
     || RPAD(DECODE(NVL( hcp_s.credit_hold,hcp.credit_hold),'Y','H', 'N', 'N'),20,' ')         --    CHECK_NUMBER -- ?????
     || RPAD(' ', 10, ' ')                                                                     --    PROBLEM_NUMBER
     || RPAD(NVL((SELECT al.lookup_code
                    FROM apps.ar_receivable_applications araa
                       , ar_lookups al
                   WHERE 1 = 1 
                     AND al.lookup_type = 'XXWC_DCMT_REASON_CODES'
                     AND araa.receivable_application_id IN (SELECT MAX(receivable_application_id) 
                                                              FROM apps.ar_receivable_applications araa2
                                                             WHERE 1 = 1
                                                               AND araa.applied_customer_trx_id = araa2.applied_customer_trx_id
                                                               AND araa.org_id = araa2.org_id
                                                            )
                     AND araa.applied_customer_trx_id = rcta.customer_trx_id
                     AND araa.attribute1 = al.meaning
                     AND to_char(araa.org_id) = araa.attribute_category), ' '), 10, ' ')       --    REASON_CODE
     || RPAD(NVL((SELECT jrdv.resource_name
                   FROM jtf_rs_defresources_v jrdv
                      , apps.ra_salesreps      rsa
                  WHERE rcta.primary_salesrep_id = rsa.salesrep_id
                    AND rcta.org_id = rsa.org_id
                    AND rsa.person_id = jrdv.source_id)
        , ' '),10,' ')                                                                         --    SALES_PERSON
     || RPAD(NVL( TO_CHAR(hps_s.party_site_number) , ' '), 20, ' ')                            --    SHIP_TO_ID
     || RPAD(' ', 20, ' ')                                                                     --    SRC_INVOICE 
     || RPAD(' ', 10, ' ')                                                                     --    TRANSACTION_CURRENCY
     || LPAD(NVL( TO_CHAR(gp_dmp.invamt,'9999999990.99') , '0.00'), 20, ' ')                   --    TRANS_CURR_ORIG_AMT
     || LPAD(NVL( TO_CHAR(apsa.AMOUNT_DUE_REMAINING,'9999999990.99'), '0.00'), 20, ' ')        --    TRANSACTION_CURRENCY_BALANCE
     || LPAD('0.00', 20, ' ')                                                                  --    LOCAL_CURRENCY_ORIGINAL_AMOUNT
     || LPAD('0.00', 20, ' ')                                                                  --    LOCAL_CURRENCY_BALANCE
     || RPAD(NVL(flv.lookup_code,' '), 10, ' ')                                                --    DIVISION_CODE
     || RPAD(NVL(LPAD(gp_exti_dmp.warehse, 3, '0'), '000'), 10, ' ')                           --    SALES_AREA
     || RPAD(NVL(gp_dmp.FLEXFIELD1, ' '), 35, ' ')                                             --    FLEXFIELD1
     || RPAD(NVL(gp_dmp.FLEXFIELD2,' '), 35, ' ')                                              --    FLEXFIELD2     
     || RPAD(NVL(gp_dmp.FLEXFIELD3,' '), 35, ' ')                                              --    FLEXFIELD3
     || RPAD(NVL(gp_dmp.FLEXFIELD4,' '), 35, ' ')                                              --    FLEXFIELD4
     || RPAD(NVL(gp_dmp.FLEXFIELD5,' '), 35, ' ')                                              --    FLEXFIELD5
     || LPAD(NVL(TO_CHAR(gp_dmp.FLEXNUM1,'9999999990.99'),'0.00'), 20, ' ')                    --    FLEXNUM1
--     || LPAD(NVL(TO_CHAR(gp_dmp.FLEXNUM2,'9999999990.99'),'0.00'), 20, ' ')                    --    FLEXNUM2
     || LPAD(NVL(TO_CHAR(xxwc_ar_getpaid_ob_pkg.get_credit_limit(rcta.customer_trx_id , rcta.org_id ),'9999999990.99'),'0.00'), 20, ' ')  --    FLEXNUM2
     || LPAD(NVL(TO_CHAR(xxwc_ar_getpaid_ob_pkg.get_flexnum3(rcta.org_id
                                                           , apsa.class
                                                           , hps_s.party_site_number
                                                           , rcta.ship_to_site_use_id
                                                           , hps_s.location_id
                                                           , rcta.interface_header_attribute2
                                                           , rcta.interface_header_attribute8)
                                                     ,'9999999990.99'),' '), 20, ' ')       --    FLEXNUM3
     || LPAD(NVL(TO_CHAR(gp_dmp.FLEXNUM4,'9999999990.99'),'0.00'), 20, ' ')                    --    FLEXNUM4
     || LPAD(NVL(TO_CHAR(gp_dmp.FLEXNUM5,'9999999990.99'),'0.00'), 20, ' ')                    --    FLEXNUM5
     || RPAD(' ', 8, ' ')                                                                      --    FLEXDATE1     
     || RPAD(' ', 8, ' ')                                                                      --    FLEXDATE2
     || RPAD(' ', 8, ' ')                                                                      --    FLEXDATE3
     || RPAD(' ', 8, ' ')                                                                      --    FLEXDATE4
     || RPAD(TO_CHAR(rcta.creation_date, 'MMDDYYYY'), 8, ' ')                                  --    FLEXDATE5
     || RPAD(' ', 10, ' ')                                                                     --    DISCOUNT_CODE
     || RPAD(' ', 10, ' ')                                                                     --    AR_TARGET_SYSTEM
     || RPAD(NVL(gp_dmp.FLEXFIELD6, ' '), 50, ' ')                                             --    FLEXFIELD6
     || RPAD(NVL(gp_dmp.FLEXFIELD7, ' '), 50, ' ')                                             --    FLEXFIELD7
     || RPAD(NVL(gp_dmp.FLEXFIELD8, ' '), 50, ' ')                                             --    FLEXFIELD8
     || RPAD(NVL(flv.attribute1, ' '), 50, ' ')                                                  --    FLEXFIELD9
     || RPAD(NVL(flv.attribute2, ' '), 50, ' ')                                                  --    FLEXFIELD10
     || RPAD(NVL((CASE WHEN SUBSTR(hcsu_s.location, instr(hcsu_s.location,'-',-1)+ 1 ) = hps_s.party_site_number       
                  THEN
                     RPAD(' ', 50, ' ')
                  WHEN instr(hcsu_s.location,'-',-1) = 0 THEN 
                     RPAD(' ', 50, ' ')
                  ELSE
                     RPAD(SUBSTR(hcsu_s.location, instr(hcsu_s.location,'-',-1)+ 1 ), 50, ' ')  
                  END)
       , ' '), 50, ' ')                                                                        --    FLEXFIELD11
     || RPAD(NVL(hcas_s.attribute19, ' '), 50, ' ')                                              --    FLEXFIELD12
     || RPAD(NVL(substr(gp_dmp.FLEXFIELD13,1,15)||' '||SUBSTR(NVL(hcp.account_status,' '),1,1), '080' ), 50, ' ') --    FLEXFIELD13
     || RPAD(NVL(gp_dmp.flexfield14, '0'), 50, ' ')                                            --    FLEXFIELD14
     || RPAD(NVL(xxwc_ar_getpaid_ob_pkg.get_lien_by_state(rcta.bill_to_site_use_id) , ' '), 50, ' ') --   FLEXFIELD15
     || RPAD(' ', 10, ' ')                                                                     --    JOURNAL_IDENTIFIER
      ) REC_LINE , rcta.org_id
  FROM apps.ra_customer_trx rcta
     , apps.hz_cust_accounts hca
     , apps.hz_cust_accounts hca_s
     , apps.hz_cust_acct_sites hcas_s
     , apps.hz_cust_site_uses hcsu_s
     , hz_party_sites hps_s
--     , apps.hz_cust_accounts hca_b
--     , apps.hz_cust_acct_sites hcas_b
--     , apps.hz_cust_site_uses hcsu_b
--     , hz_party_sites hps_b
--     , hz_locations hl_b
     , ra_terms rt
     , apps.ar_payment_schedules apsa
     , apps.ra_cust_trx_types rctt
     , fnd_lookup_values_vl flv
     , hz_customer_profiles hcp
     , hz_customer_profiles hcp_s
     -- , ra_batch_sources_all         rbsa
     , apps.xxwc_armast_getpaid_dump_tbl gp_dmp
     , apps.xxwc_arexti_getpaid_dump_tbl gp_exti_dmp     
 WHERE 1 = 1
   AND hca.cust_account_id = rcta.bill_to_customer_id
   -- AND rcta.batch_source_id        = rbsa.batch_source_id
   -- AND rcta.org_id                 = rbsa.org_id
   AND xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id )   = 'CONVERSION'   
   AND rcta.ship_to_customer_id IS NOT NULL
   AND rcta.ship_to_site_use_id = hcsu_s.site_use_id
   AND hca_S.cust_account_id = rcta.ship_to_customer_id
   AND hcas_s.cust_acct_site_id = rcta.ship_to_address_id
   AND hcas_s.party_site_id =  hps_s.party_site_id
   AND hcas_s.cust_acct_site_id = hcsu_s.cust_acct_site_id
--   AND hcas_b.cust_acct_site_id = hcsu_b.cust_acct_site_id
--   AND hcas_b.cust_acct_site_id = rcta.bill_to_address_id
--   AND hcas_b.party_site_id =  hps_b.party_site_id
--   AND hca_b.cust_account_id = rcta.bill_to_customer_id
--   AND hps_b.location_id = hl_b.location_id
   AND hca.cust_account_id = hcp.cust_account_id
   AND hcp.site_use_id IS  NULL
   AND rcta.ship_to_site_use_id = hcp_s.site_use_id   (+) 
   AND rcta.cust_trx_type_id = rctt.cust_trx_type_id
   AND apsa.customer_trx_id = rcta.customer_trx_id
   AND rcta.term_id = rt.term_id (+)
   AND rcta.org_id = rctt.org_id
   AND flv.lookup_type = 'XXWC_GETPAID_REMIT_TO_CODES'
   AND NVL(hcp.attribute2, '2') = flv.lookup_code
   AND rcta.trx_number = gp_dmp.invno
   AND hca.account_number = gp_dmp.custno
   AND rcta.trx_number = gp_exti_dmp.invno
   AND hca.account_number = gp_exti_dmp.custno
   AND apsa.status = 'OP' AND apsa.amount_due_remaining != 0 
   AND apsa.org_id = rcta.org_id
UNION
------------------------------------------------------------------------------------------------------------------------------
-- NON-CONVERSION Invoices
-- rcta.ship_to_site_use_id IS NOT NULL
------------------------------------------------------------------------------------------------------------------------------
/*SELECT (RPAD(NVL(hca.account_number, ' '), 20, ' ')                                            --    CUSTOMER_NUMBER  
     || RPAD(NVL(rcta.trx_number, ' '), 20, ' ')                                               --    TRANSACTION_NUMBER
     || RPAD(NVL(TO_CHAR(rcta.trx_date, 'MMDDYYYY'), ' '),8, ' ')                              --    TRANSACTION_DATE
     || LPAD(NVL(TO_CHAR(apsa.amount_due_remaining,'9999999990.99'), '0.00'),20, ' ')          --    UNPAID_BALANCE
     || LPAD(NVL(TO_CHAR(apsa.due_date - rcta.trx_date), '0.00'),4,' ')                        --    NET_DUE_DAYS
     || LPAD(NVL(TO_CHAR(apsa.amount_due_original,'9999999990.99'),'0.00'),20,' ')             --    TRANSACTION_AMOUNT
     || RPAD(NVL(SUBSTR(hcsu_s.location, 1, DECODE(instr(hcsu_s.location,'-',-1),0,LENGTH(hcsu_s.location),instr(hcsu_s.location,'-',-1)- 1) ), ' '), 30, ' ') --    REFERENCE_NUMBER
     || RPAD(NVL(rcta.purchase_order,' '),54,' ')                                              --    PURCHASE_ORDER_NUMBER
     || RPAD(' ', 10, ' ')                                                                     --    DIVISION 
     || LPAD(NVL( TO_CHAR(apsa.tax_original,'9999999990.99'),'0.00'),20,' ')                   --    TAX
     || LPAD(NVL( TO_CHAR(apsa.freight_original,'9999999990.99'), '0.00'),20,' ')              --    FREIGHT
     || LPAD(NVL( TO_CHAR(apsa.amount_due_original - (apsa.amount_line_items_original + apsa.tax_original + apsa.freight_original),'9999999990.99'),'0.00'),20,' ')   --    OTHER
     || RPAD(NVL( DECODE(rctt.name,'Interest Invoice','F',rctt.name)  , ' '), 1, ' ')          --    TRANSACTION_TYPE
     || RPAD(DECODE(NVL( hcp_s.credit_hold,hcp.credit_hold),'Y','H', 'N', 'N'),20,' ')         --    CHECK_NUMBER -- ?????
     || RPAD(' ', 10, ' ')                                                                     --    PROBLEM_NUMBER
     || RPAD(NVL((SELECT al.lookup_code
                    FROM apps.ar_receivable_applications araa
                       , ar_lookups al
                   WHERE 1 = 1 
                     AND al.lookup_type = 'XXWC_DCMT_REASON_CODES'
                     AND araa.receivable_application_id IN (SELECT MAX(receivable_application_id) 
                                                              FROM apps.ar_receivable_applications araa2
                                                             WHERE 1 = 1
                                                               AND araa.applied_customer_trx_id = araa2.applied_customer_trx_id
                                                               AND araa.org_id = araa2.org_id
                                                            )
                     AND araa.applied_customer_trx_id = rcta.customer_trx_id
                     AND araa.attribute1 = al.meaning
                     AND to_char(araa.org_id) = araa.attribute_category), ' '), 10, ' ')       --    REASON_CODE
     || RPAD(NVL((SELECT jrdv.resource_name
                   FROM jtf_rs_defresources_v jrdv
                      , apps.ra_salesreps      rsa
                  WHERE rcta.primary_salesrep_id = rsa.salesrep_id
                    AND rcta.org_id = rsa.org_id
                    AND rsa.person_id = jrdv.source_id)
        , ' '),10,' ')                                                                         --    SALES_PERSON
     || RPAD(NVL( TO_CHAR(hps_s.party_site_number) , ' '), 20, ' ')                            --    SHIP_TO_ID
     || RPAD(' ', 20, ' ')                                                                     --    SRC_INVOICE 
     || RPAD(' ', 10, ' ')                                                                     --    TRANSACTION_CURRENCY
     || LPAD(NVL( TO_CHAR(apsa.amount_due_original,'9999999990.99') , '0.00'), 20, ' ')        --    TRANS_CURR_ORIG_AMT
     || LPAD(NVL( TO_CHAR(apsa.amount_due_remaining,'9999999990.99'), '0.00'), 20, ' ')        --    TRANSACTION_CURRENCY_BALANCE
     || LPAD('0.00', 20, ' ')                                                                  --    LOCAL_CURRENCY_ORIGINAL_AMOUNT
     || LPAD('0.00', 20, ' ')                                                                  --    LOCAL_CURRENCY_BALANCE
     || RPAD(NVL(flv.lookup_code,' '), 10, ' ')                                                --    DIVISION_CODE
     || RPAD(NVL((CASE WHEN xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id ) IN ('ORDER MANAGEMENT','STANDARD OM SOURCE', 'REPAIR OM SOURCE')
                  THEN (SELECT LPAD(ood.organization_code,3,'0')
                          FROM apps.oe_order_headers           ooha
                             , org_organization_definitions   ood
                         WHERE ooha.order_number            = NVL(rcta.interface_header_attribute1, rcta.ct_reference)
                           AND ooha.ship_from_org_id        = ood.organization_id
                           AND ROWNUM = 1
                       )
                  WHEN xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id ) = 'PRISM'
                  THEN lpad(rcta.interface_header_attribute7,3,'0')
                  ELSE ' '
                  END),' '), 10, ' ')                                                             --    SALES_AREA
     || RPAD(NVL((case when xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id ) in ('ORDER MANAGEMENT','STANDARD OM SOURCE', 'REPAIR OM SOURCE')
                  then (select DECODE(COUNT(1),0,'INVOICE','RENTAL INVOICE')
                        from apps.oe_order_headers      ooha
                           , oe_transaction_types_tl  ott
                       where ooha.order_number = NVL(rcta.interface_header_attribute1, rcta.ct_reference) 
                       and ooha.order_type_id = ott.transaction_type_id
                       and ott.name IN ('WC SHORT TERM RENTAL', 'WC LONG TERM RENTAL')
                       and rownum = 1
                       )
                when xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id ) = 'PRISM'
                then rcta.interface_header_attribute8
                else 'INVOICE'
                end),' '), 35, ' ')                                                             --    FLEXFIELD1
     || RPAD(NVL((case when xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id ) in ('ORDER MANAGEMENT','STANDARD OM SOURCE', 'REPAIR OM SOURCE')
                  then (select xxwc_mv_routines_pkg.get_user_employee(ooha.created_by)
                        from apps.oe_order_headers ooha
                       where ooha.order_number = NVL(rcta.interface_header_attribute1, rcta.ct_reference) 
                       and rownum = 1)
                when xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id ) = 'PRISM'
                then rcta.interface_header_attribute10
                else NULL
                end),' '), 35, ' ')                                                             --    FLEXFIELD2     
     || RPAD(NVL((case when xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id ) in ('ORDER MANAGEMENT','STANDARD OM SOURCE', 'REPAIR OM SOURCE')
                  then (select xxwc_mv_routines_pkg.get_contact_name(ooha.invoice_to_contact_id)
                        from apps.oe_order_headers ooha
                       where ooha.order_number = NVL(rcta.interface_header_attribute1, rcta.ct_reference) 
                       and rownum = 1)
                when xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id ) = 'PRISM'
                then rcta.interface_header_attribute9
                else xxwc_mv_routines_pkg.get_contact_name(rcta.bill_to_contact_id)
                end)
                ,' '), 35, ' ')                                                                --    FLEXFIELD3
     || RPAD(NVL((case when xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id ) in ('ORDER MANAGEMENT','STANDARD OM SOURCE', 'REPAIR OM SOURCE')
                  then (select oola.revrec_signature
                        from apps.oe_order_headers ooha
                               , apps.oe_order_lines oola
                       where ooha.order_number = NVL(rcta.interface_header_attribute1, rcta.ct_reference)
                       and    ooha.header_id = oola.header_id
                       and    oola.revrec_signature is not null 
                       and rownum = 1)
                when xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id ) = 'PRISM'
                then rcta.interface_header_attribute11
                else NULL
                end)
                                 ,' '), 35, ' ')                                               --    FLEXFIELD4
     || RPAD(NVL((CASE WHEN xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id ) IN ('ORDER MANAGEMENT','STANDARD OM SOURCE', 'REPAIR OM SOURCE')
                  THEN (SELECT RPAD(NVL(houv.location_code, ' '), 35, ' ')
                          FROM apps.oe_order_headers           ooha
                             , hr_organization_units_v        houv
                         WHERE ooha.order_number            = NVL(rcta.interface_header_attribute1, rcta.ct_reference)
                           AND ooha.ship_from_org_id        = houv.organization_id
                           AND ROWNUM = 1
                       )
                  WHEN xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id ) = 'PRISM'
                  THEN (SELECT RPAD(NVL(hla.location_code, ' '), 35, ' ')
                          FROM apps.hr_locations hla
                         WHERE SUBSTR(location_code, 1, INSTR(location_code, '-') - 2) = rcta.interface_header_attribute7
                           AND ROWNUM = 1)
                  ELSE ' '
                  END),' '), 35, ' ')                                                          --    FLEXFIELD5
     || LPAD(NVL(ROUND((NVL(apsa.tax_original,0)/DECODE(apsa.amount_due_original - NVL(apsa.tax_original,0) - NVL(apsa.freight_original,0), 0 , 1))*100, 2),'0.00'), 20, ' ')  --    FLEXNUM1
     || LPAD(NVL(TO_CHAR(xxwc_ar_getpaid_ob_pkg.get_credit_limit(rcta.customer_trx_id , rcta.org_id ),'9999999990.99'),'0.00'), 20, ' ')  --    FLEXNUM2
     || LPAD(NVL(TO_CHAR(xxwc_ar_getpaid_ob_pkg.get_flexnum3(rcta.org_id
                                                           , apsa.class
                                                           , hps_s.party_site_number
                                                           , rcta.ship_to_site_use_id
                                                           , hps_s.location_id
                                                           , rcta.interface_header_attribute2
                                                           , rcta.interface_header_attribute8)
                                                     ,'9999999990.99'),' '), 20, ' ')       --    FLEXNUM3
     || LPAD(NVL(TO_CHAR((apsa.amount_due_original - NVL(apsa.tax_original,0)),'9999999990.99'),'0.00'), 20, ' ')           --    FLEXNUM4
     || LPAD(NVL(TO_CHAR(apsa.tax_original,'9999999990.99'),'0.00'), 20, ' ')                  --    FLEXNUM5
     || RPAD(' ', 8, ' ')                                                                      --    FLEXDATE1     
     || RPAD(' ', 8, ' ')                                                                      --    FLEXDATE2
     || RPAD(' ', 8, ' ')                                                                      --    FLEXDATE3
     || RPAD(' ', 8, ' ')                                                                      --    FLEXDATE4
     || RPAD(TO_CHAR(rcta.creation_date, 'MMDDYYYY'), 8, ' ')                                  --    FLEXDATE5
     || RPAD(' ', 10, ' ')                                                                     --    DISCOUNT_CODE
     || RPAD(' ', 10, ' ')                                                                     --    AR_TARGET_SYSTEM
     || RPAD(NVL((CASE WHEN xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id ) IN ('ORDER MANAGEMENT','STANDARD OM SOURCE', 'REPAIR OM SOURCE')
                  THEN (SELECT RPAD(NVL(houv.telephone_number_1, ' '), 50, ' ')                                       --    FLEXFIELD6
                            || RPAD(NVL(houv.address_line_1, ' '), 50, ' ')                                           --    FLEXFIELD7
                            || RPAD( NVL(houv.town_or_city||','||houv.region_2||' '||houv.postal_code, ' '), 50, ' ') --    FLEXFIELD8
                          FROM apps.oe_order_headers           ooha
                             , hr_organization_units_v        houv
                         WHERE ooha.order_number            = NVL(rcta.interface_header_attribute1, rcta.ct_reference)
                           AND ooha.ship_from_org_id        = houv.organization_id
                           AND ROWNUM = 1
                       )
                  WHEN xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id ) = 'PRISM'
                  THEN (SELECT RPAD(NVL(hla.telephone_number_1, ' '), 50, ' ')                                       --    FLEXFIELD6
                            || RPAD(NVL(hla.address_line_1, ' '), 50, ' ')                                           --    FLEXFIELD7
                            || RPAD(NVL(hla.town_or_city||','||hla.region_2||' '||hla.postal_code, ' '), 50, ' ')    --    FLEXFIELD8
                          FROM apps.hr_locations hla
                             , hr_all_organization_units hou
                         WHERE SUBSTR(location_code, 1, INSTR(location_code, '-') - 2) = rcta.interface_header_attribute7
                           AND hla.location_id = hou.location_id
                           AND SYSDATE BETWEEN hou.date_from AND NVL(hou.date_to,SYSDATE + 1)
                           AND ROWNUM = 1)
                  ELSE ' '
                  END),' '), 150, ' ')
     || RPAD(NVL(flv.attribute1, ' '), 50, ' ')                                                  --    FLEXFIELD9
     || RPAD(NVL(flv.attribute2, ' '), 50, ' ')                                                  --    FLEXFIELD10
     || RPAD(NVL((CASE WHEN SUBSTR(hcsu_s.location, instr(hcsu_s.location,'-',-1)+ 1 ) = hps_s.party_site_number       
                  THEN
                     RPAD(' ', 50, ' ')
                  WHEN instr(hcsu_s.location,'-',-1) = 0 THEN 
                     RPAD(' ', 50, ' ')
                  ELSE
                     RPAD(SUBSTR(hcsu_s.location, instr(hcsu_s.location,'-',-1)+ 1 ), 50, ' ')  
                  END)
       , ' '), 50, ' ')                                                                        --    FLEXFIELD11
     || RPAD(NVL(hcas_s.attribute19, ' '), 50, ' ')                                              --    FLEXFIELD12
     || RPAD(NVL((CASE WHEN xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id ) IN ('ORDER MANAGEMENT','STANDARD OM SOURCE', 'REPAIR OM SOURCE')
                  THEN (SELECT LPAD(ood.organization_code,3,'0')
                          FROM apps.oe_order_headers           ooha
                             , org_organization_definitions   ood
                         WHERE ooha.order_number            = NVL(rcta.interface_header_attribute1, rcta.ct_reference)
                           AND ooha.ship_from_org_id        = ood.organization_id
                           AND ROWNUM = 1
                       )
                  WHEN xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id ) = 'PRISM'
                  THEN lpad(NVL(rcta.interface_header_attribute7,'080'),3,'0')
                  ELSE '080'
                  END)||'  '||RPAD(hps_s.party_site_number,11,' ')||SUBSTR(NVL(hcp.account_status,' '),1,1),' '), 50, ' ')  --   FLEXFIELD13
     || RPAD('0', 50, ' ')                                                                     --    FLEXFIELD14
     || RPAD(NVL(xxwc_ar_getpaid_ob_pkg.get_lien_by_state(rcta.bill_to_site_use_id) , ' '), 50, ' ') --   FLEXFIELD15
     || RPAD(' ', 10, ' ')                                                                     --    JOURNAL_IDENTIFIER
      ) REC_LINE , rcta.org_id
  FROM apps.ra_customer_trx rcta
     , apps.hz_cust_accounts hca
     , apps.hz_cust_accounts hca_s
     , apps.hz_cust_acct_sites hcas_s
     , apps.hz_cust_site_uses hcsu_s
     , hz_party_sites hps_s
--     , apps.hz_cust_accounts hca_b
--     , apps.hz_cust_acct_sites hcas_b
--     , apps.hz_cust_site_uses hcsu_b
--     , hz_party_sites hps_b
--     , hz_locations hl_b
     , ra_terms rt
--     , apps.ra_salesreps rsa
--     , jtf_rs_defresources_v jrdv
     , apps.ar_payment_schedules apsa
     , apps.ra_cust_trx_types rctt
     -- , ra_batch_sources_all rbsa
     , fnd_lookup_values_vl flv
     , hz_customer_profiles hcp
     , hz_customer_profiles hcp_s
 WHERE 1 = 1
   AND hca.cust_account_id = rcta.bill_to_customer_id
   -- AND rcta.batch_source_id        = rbsa.batch_source_id
   -- AND rcta.org_id                 = rbsa.org_id
   AND xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id )   <> 'CONVERSION'
   AND rcta.ship_to_customer_id IS NOT NULL
   AND rcta.ship_to_site_use_id = hcsu_s.site_use_id
   AND hca_S.cust_account_id = rcta.ship_to_customer_id
   AND hcas_s.cust_acct_site_id = rcta.ship_to_address_id
   AND hcas_s.party_site_id =  hps_s.party_site_id
--   AND hcas_b.cust_acct_site_id = rcta.bill_to_address_id
--   AND hcas_s.cust_acct_site_id = hcsu_s.cust_acct_site_id
--   AND hcas_b.cust_acct_site_id = hcsu_b.cust_acct_site_id
--   AND hcas_b.party_site_id =  hps_b.party_site_id
--   AND hca_b.cust_account_id = rcta.bill_to_customer_id
--   AND hps_b.location_id = hl_b.location_id
   AND hca.cust_account_id = hcp.cust_account_id
   AND hcp.site_use_id IS  NULL
   AND rcta.ship_to_site_use_id = hcp_s.site_use_id   (+) 
   AND rcta.cust_trx_type_id = rctt.cust_trx_type_id
   AND apsa.customer_trx_id = rcta.customer_trx_id
   AND rcta.term_id = rt.term_id (+)
   AND rcta.org_id = rctt.org_id
   AND flv.lookup_type = 'XXWC_GETPAID_REMIT_TO_CODES'
   AND NVL(hcp.attribute2, '2') = flv.lookup_code
   AND hcas_s.org_id = rcta.org_id
   AND hcsu_s.org_id = rcta.org_id   
--   AND hcas_b.org_id = rcta.org_id
--   AND hcsu_b.org_id = rcta.org_id   
   AND apsa.status = 'OP' AND apsa.amount_due_remaining != 0 AND apsa.org_id = rcta.org_id
UNION
*/
------------------------------------------------------------------------------------------------------------------------------
-- NON-CONVERSION Invoices
-- rcta.ship_to_customer_id IS NULL
------------------------------------------------------------------------------------------------------------------------------
SELECT (RPAD(NVL(hca.account_number, ' '), 20, ' ')                                            --    CUSTOMER_NUMBER  
     || RPAD(NVL(rcta.trx_number, ' '), 20, ' ')                                               --    TRANSACTION_NUMBER
     || RPAD(NVL(TO_CHAR(rcta.trx_date, 'MMDDYYYY'), ' '),8, ' ')                              --    TRANSACTION_DATE
     || LPAD(NVL(TO_CHAR(apsa.amount_due_remaining,'9999999990.99'), '0.00'),20, ' ')          --    UNPAID_BALANCE
     || LPAD(NVL(TO_CHAR(apsa.due_date - rcta.trx_date), '0.00'),4,' ')                        --    NET_DUE_DAYS
     || LPAD(NVL(TO_CHAR(apsa.amount_due_original,'9999999990.99'),'0.00'),20,' ')             --    TRANSACTION_AMOUNT
     || RPAD(NVL(SUBSTR(hcsu_s.location, 1, DECODE(instr(hcsu_s.location,'-',-1),0,LENGTH(hcsu_s.location),instr(hcsu_s.location,'-',-1)- 1) ), ' '), 30, ' ') --    REFERENCE_NUMBER
     || RPAD(NVL(rcta.purchase_order,' '),54,' ')                                              --    PURCHASE_ORDER_NUMBER
     || RPAD(' ', 10, ' ')                                                                     --    DIVISION 
     || LPAD(NVL( TO_CHAR(apsa.tax_original,'9999999990.99'),'0.00'),20,' ')                   --    TAX
     || LPAD(NVL( TO_CHAR(apsa.freight_original,'9999999990.99'), '0.00'),20,' ')              --    FREIGHT
     || LPAD(NVL( TO_CHAR(apsa.amount_due_original -(apsa.amount_line_items_original + apsa.tax_original + apsa.freight_original),'9999999990.99'),'0.00'),20,' ')   --    OTHER
     || RPAD(NVL( DECODE(rctt.name,'Interest Invoice','F',rctt.name)  , ' '), 1, ' ')          --    TRANSACTION_TYPE   
     || RPAD(DECODE(NVL( hcp_s.credit_hold,hcp.credit_hold),'Y','H', 'N', 'N'),20,' ')         --    CHECK_NUMBER -- ?????
     || RPAD(' ', 10, ' ')                                                                     --    PROBLEM_NUMBER
     || RPAD(NVL((SELECT al.lookup_code
                    FROM apps.ar_receivable_applications araa
                       , ar_lookups al
                   WHERE 1 = 1 
                     AND al.lookup_type = 'XXWC_DCMT_REASON_CODES'
                     AND araa.receivable_application_id IN (SELECT MAX(receivable_application_id) 
                                                              FROM apps.ar_receivable_applications araa2
                                                             WHERE 1 = 1
                                                               AND araa.applied_customer_trx_id = araa2.applied_customer_trx_id
                                                               AND araa.org_id = araa2.org_id
                                                            )
                     AND araa.applied_customer_trx_id = rcta.customer_trx_id
                     AND araa.attribute1 = al.meaning
                     AND to_char(araa.org_id) = araa.attribute_category), ' '), 10, ' ')       --    REASON_CODE
     || RPAD(NVL((SELECT jrdv.resource_name
                   FROM jtf_rs_defresources_v jrdv
                      , apps.ra_salesreps      rsa
                  WHERE rcta.primary_salesrep_id = rsa.salesrep_id
                    AND rcta.org_id = rsa.org_id
                    AND rsa.person_id = jrdv.source_id)
        , ' '),10,' ')                                                                         --    SALES_PERSON
     || RPAD(NVL( TO_CHAR(hps_s.party_site_number) , ' '), 20, ' ')                            --    SHIP_TO_ID
     || RPAD(' ', 20, ' ')                                                                     --    SRC_INVOICE 
     || RPAD(' ', 10, ' ')                                                                     --    TRANSACTION_CURRENCY
     || LPAD(NVL( TO_CHAR(apsa.amount_due_original,'9999999990.99') , '0.00'), 20, ' ')        --    TRANS_CURR_ORIG_AMT
     || LPAD(NVL( TO_CHAR(apsa.amount_due_remaining,'9999999990.99'), '0.00'), 20, ' ')        --    TRANSACTION_CURRENCY_BALANCE
     || LPAD('0.00', 20, ' ')                                                                  --    LOCAL_CURRENCY_ORIGINAL_AMOUNT
     || LPAD('0.00', 20, ' ')                                                                  --    LOCAL_CURRENCY_BALANCE
     || RPAD(NVL(flv.lookup_code,' '), 10, ' ')                                                --    DIVISION_CODE
     || RPAD(NVL((CASE WHEN xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id ) IN ('ORDER MANAGEMENT','STANDARD OM SOURCE', 'REPAIR OM SOURCE')
                  THEN (SELECT LPAD(ood.organization_code,3,'0')
                          FROM apps.oe_order_headers           ooha
                             , org_organization_definitions   ood
                         WHERE ooha.order_number            = NVL(rcta.interface_header_attribute1, rcta.ct_reference)
                           AND ooha.ship_from_org_id        = ood.organization_id
                           AND ROWNUM = 1
                       )
                  WHEN xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id ) = 'PRISM'
                  THEN lpad(rcta.interface_header_attribute7,3,'0')
                  ELSE ' '
                  END),' '), 10, ' ')                                                             --    SALES_AREA
     || RPAD(NVL((case when xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id ) in ('ORDER MANAGEMENT','STANDARD OM SOURCE', 'REPAIR OM SOURCE')
                  then (select DECODE(COUNT(1),0,'INVOICE','RENTAL INVOICE')
                        from apps.oe_order_headers      ooha
                           , oe_transaction_types_tl  ott
                       where ooha.order_number = NVL(rcta.interface_header_attribute1, rcta.ct_reference) 
                       and ooha.order_type_id = ott.transaction_type_id
                       and ott.name IN ('WC SHORT TERM RENTAL', 'WC LONG TERM RENTAL')
                       and rownum = 1
                       )
                when xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id ) = 'PRISM'
                then rcta.interface_header_attribute8
                else 'INVOICE'
                end),' '), 35, ' ')                                                             --    FLEXFIELD1
     || RPAD(NVL((case when xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id ) in ('ORDER MANAGEMENT','STANDARD OM SOURCE', 'REPAIR OM SOURCE')
                  then (select xxwc_mv_routines_pkg.get_user_employee(ooha.created_by)
                        from apps.oe_order_headers ooha
                       where ooha.order_number = NVL(rcta.interface_header_attribute1, rcta.ct_reference) 
                       and rownum = 1)
                when xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id ) = 'PRISM'
                then rcta.interface_header_attribute10
                else NULL
                end),' '), 35, ' ')                                                             --    FLEXFIELD2     
     || RPAD(NVL((case when xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id ) in ('ORDER MANAGEMENT','STANDARD OM SOURCE', 'REPAIR OM SOURCE')
                  then (select xxwc_mv_routines_pkg.get_contact_name(ooha.invoice_to_contact_id)
                        from apps.oe_order_headers ooha
                       where ooha.order_number = NVL(rcta.interface_header_attribute1, rcta.ct_reference) 
                       and rownum = 1)
                when xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id ) = 'PRISM'
                then rcta.interface_header_attribute9
                else xxwc_mv_routines_pkg.get_contact_name(rcta.bill_to_contact_id)
                end)
                ,' '), 35, ' ')                                                                --    FLEXFIELD3
     || RPAD(NVL((case when xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id ) in ('ORDER MANAGEMENT','STANDARD OM SOURCE', 'REPAIR OM SOURCE')
                  then (select oola.revrec_signature
                        from apps.oe_order_headers ooha
                               , apps.oe_order_lines oola
                       where ooha.order_number = NVL(rcta.interface_header_attribute1, rcta.ct_reference)
                       and    ooha.header_id = oola.header_id
                       and    oola.revrec_signature is not null 
                       and rownum = 1)
                when xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id ) = 'PRISM'
                then rcta.interface_header_attribute11
                else NULL
                end)
                                 ,' '), 35, ' ')                                               --    FLEXFIELD4
     || RPAD(NVL((CASE WHEN xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id ) IN ('ORDER MANAGEMENT','STANDARD OM SOURCE', 'REPAIR OM SOURCE')
                  THEN (SELECT RPAD(NVL(houv.location_code, ' '), 35, ' ')
                          FROM apps.oe_order_headers           ooha
                             , hr_organization_units_v        houv
                         WHERE ooha.order_number            = NVL(rcta.interface_header_attribute1, rcta.ct_reference)
                           AND ooha.ship_from_org_id        = houv.organization_id
                           AND ROWNUM = 1
                       )
                  WHEN xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id ) = 'PRISM'
                  THEN (SELECT RPAD(NVL(hla.location_code, ' '), 35, ' ')
                          FROM apps.hr_locations hla
                         WHERE SUBSTR(location_code, 1, INSTR(location_code, '-') - 2) = lpad(rcta.interface_header_attribute7,3,'0')
                           AND ROWNUM = 1)
                  ELSE ' '
                  END),' '), 35, ' ')                                                          --    FLEXFIELD5
     || LPAD(NVL(ROUND((NVL(apsa.tax_original,0)/DECODE(apsa.amount_due_original - NVL(apsa.tax_original,0) - NVL(apsa.freight_original,0), 0 , 1))*100, 2),'0.00'), 20, ' ')  --    FLEXNUM1
/*     || LPAD(NVL((SELECT to_char(hcpa_s.overall_credit_limit)
                  FROM hz_cust_profile_amts hcpa_s
                 WHERE hcpa_s.site_use_id = hcsu_s.site_use_id)
                , '0.00')
             , 20, ' ')                                                                        --    FLEXNUM2 -- ????? Credit Limit for Ship To
*/
     || LPAD(NVL(TO_CHAR(xxwc_ar_getpaid_ob_pkg.get_credit_limit(rcta.customer_trx_id , rcta.org_id ),'9999999990.99'),'0.00'), 20, ' ')  --    FLEXNUM2
     || LPAD(NVL(TO_CHAR(xxwc_ar_getpaid_ob_pkg.get_flexnum3(rcta.org_id
                                                           , apsa.class
                                                           , hps_s.party_site_number
                                                           , rcta.ship_to_site_use_id
                                                           , hps_s.location_id
                                                           , rcta.interface_header_attribute2
                                                           , rcta.interface_header_attribute8)
                                                     ,'9999999990.99'),' '), 20, ' ')       --    FLEXNUM3
     || LPAD(NVL(TO_CHAR((apsa.amount_due_original - NVL(apsa.tax_original,0)),'9999999990.99'),'0.00'), 20, ' ')           --    FLEXNUM4
     || LPAD(NVL(TO_CHAR(apsa.tax_original,'9999999990.99'),'0.00'), 20, ' ')                  --    FLEXNUM5
     || RPAD(' ', 8, ' ')                                                                      --    FLEXDATE1     
     || RPAD(' ', 8, ' ')                                                                      --    FLEXDATE2
     || RPAD(' ', 8, ' ')                                                                      --    FLEXDATE3
     || RPAD(' ', 8, ' ')                                                                      --    FLEXDATE4
     || RPAD(TO_CHAR(rcta.creation_date, 'MMDDYYYY'), 8, ' ')                                  --    FLEXDATE5
     || RPAD(' ', 10, ' ')                                                                     --    DISCOUNT_CODE
     || RPAD(' ', 10, ' ')                                                                     --    AR_TARGET_SYSTEM
     || RPAD(NVL((CASE WHEN xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id ) IN ('ORDER MANAGEMENT','STANDARD OM SOURCE', 'REPAIR OM SOURCE')
                  THEN (SELECT RPAD(NVL(houv.telephone_number_1, ' '), 50, ' ')                                       --    FLEXFIELD6
                            || RPAD(NVL(houv.address_line_1, ' '), 50, ' ')                                           --    FLEXFIELD7
                            || RPAD( NVL(houv.town_or_city||','||houv.region_2||' '||houv.postal_code, ' '), 50, ' ') --    FLEXFIELD8
                          FROM apps.oe_order_headers           ooha
                             , hr_organization_units_v        houv
                         WHERE ooha.order_number            = NVL(rcta.interface_header_attribute1, rcta.ct_reference)
                           AND ooha.ship_from_org_id        = houv.organization_id
                           AND ROWNUM = 1
                       )
                  WHEN xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id ) = 'PRISM'
                  THEN (SELECT RPAD(NVL(hla.telephone_number_1, ' '), 50, ' ')                                       --    FLEXFIELD6
                            || RPAD(NVL(hla.address_line_1, ' '), 50, ' ')                                           --    FLEXFIELD7
                            || RPAD(NVL(hla.town_or_city||','||hla.region_2||' '||hla.postal_code, ' '), 50, ' ')    --    FLEXFIELD8
                          FROM apps.hr_locations hla
                             , hr_all_organization_units hou
                         WHERE SUBSTR(location_code, 1, INSTR(location_code, '-') - 2) = lpad(rcta.interface_header_attribute7,3,'0')
                           AND hla.location_id = hou.location_id
                           AND SYSDATE BETWEEN hou.date_from AND NVL(hou.date_to,SYSDATE + 1)
                           AND ROWNUM = 1)
                  ELSE ' '
                  END),' '), 150, ' ')
     || RPAD(NVL(flv.attribute1, ' '), 50, ' ')                                                  --    FLEXFIELD9
     || RPAD(NVL(flv.attribute2, ' '), 50, ' ')                                                  --    FLEXFIELD10
     || RPAD(NVL((CASE WHEN SUBSTR(hcsu_s.location, instr(hcsu_s.location,'-',-1)+ 1 ) = hps_s.party_site_number       
                  THEN
                     RPAD(' ', 50, ' ')
                  WHEN instr(hcsu_s.location,'-',-1) = 0 THEN 
                     RPAD(' ', 50, ' ')
                  ELSE
                     RPAD(SUBSTR(hcsu_s.location, instr(hcsu_s.location,'-',-1)+ 1 ), 50, ' ')  
                  END)
       , ' '), 50, ' ')                                                                        --    FLEXFIELD11
     || RPAD(NVL(hcas_s.attribute19, ' '), 50, ' ')                                              --    FLEXFIELD12
     || RPAD(NVL((CASE WHEN xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id ) IN ('ORDER MANAGEMENT','STANDARD OM SOURCE', 'REPAIR OM SOURCE')
                  THEN (SELECT LPAD(ood.organization_code,3,'0')
                          FROM apps.oe_order_headers           ooha
                             , org_organization_definitions   ood
                         WHERE ooha.order_number            = NVL(rcta.interface_header_attribute1, rcta.ct_reference)
                           AND ooha.ship_from_org_id        = ood.organization_id
                           AND ROWNUM = 1
                       )
                  WHEN xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id ) = 'PRISM'
                  THEN lpad(NVL(rcta.interface_header_attribute7,'080'),3,'0')
                  ELSE '080'
                  END)||'  '||RPAD(hps_s.party_site_number,11,' ')||SUBSTR(NVL(hcp.account_status,' '),1,1),' '), 50, ' ')   --   FLEXFIELD13
     || RPAD('0', 50, ' ')                                                                     --    FLEXFIELD14
     || RPAD(NVL(xxwc_ar_getpaid_ob_pkg.get_lien_by_state(rcta.bill_to_site_use_id) , ' '), 50, ' ') --   FLEXFIELD15
     || RPAD(' ', 10, ' ')                                                                     --    JOURNAL_IDENTIFIER
      ) REC_LINE , rcta.org_id
  FROM apps.ra_customer_trx rcta
     , apps.hz_cust_accounts hca
     , apps.hz_cust_accounts hca_s
     , apps.hz_cust_acct_sites hcas_s
     , apps.hz_cust_site_uses hcsu_s
     , hz_party_sites hps_s
--     , apps.hz_cust_accounts hca_b
--     , apps.hz_cust_acct_sites hcas_b
--     , apps.hz_cust_site_uses hcsu_b
--     , hz_party_sites hps_b
--     , hz_locations hl_b
     , ra_terms rt
--     , apps.ra_salesreps rsa
--     , jtf_rs_defresources_v jrdv
     , apps.ar_payment_schedules apsa
     , apps.ra_cust_trx_types rctt
     -- , ra_batch_sources_all rbsa
     , fnd_lookup_values_vl flv
     , hz_customer_profiles hcp
     , hz_customer_profiles hcp_s
 WHERE 1 = 1
   AND hca.cust_account_id = rcta.bill_to_customer_id
   -- AND rcta.batch_source_id        = rbsa.batch_source_id
   -- AND rcta.org_id                 = rbsa.org_id
   AND xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id )   <> 'CONVERSION'   
   AND rcta.ship_to_customer_id IS NULL
   AND hca_s.cust_account_id = rcta.bill_to_customer_id
   AND hca_s.cust_account_id = hcas_s.cust_account_id
   AND hcas_s.cust_acct_site_id = hcsu_s.cust_acct_site_id
   AND NVL(hcas_s.ship_to_flag, 'N') = 'P'
   AND hcsu_s.site_use_code = 'SHIP_TO'
   AND hcsu_s.primary_flag = 'Y'
   AND hcas_s.party_site_id =  hps_s.party_site_id
--   AND rcta.bill_to_site_use_id = hcsu_b.site_use_id
--   AND hcas_b.cust_acct_site_id = hcsu_b.cust_acct_site_id
--   AND hcas_b.party_site_id =  hps_b.party_site_id
--   AND hca_b.cust_account_id = rcta.bill_to_customer_id
--   AND hps_b.location_id = hl_b.location_id
   AND hca.cust_account_id = hcp.cust_account_id
   AND hcp.site_use_id IS  NULL
   AND rcta.ship_to_site_use_id = hcp_s.site_use_id   (+) 
   AND rcta.cust_trx_type_id = rctt.cust_trx_type_id
   AND apsa.customer_trx_id = rcta.customer_trx_id
   AND rcta.term_id = rt.term_id (+)
   AND rcta.org_id = rctt.org_id
   AND flv.lookup_type = 'XXWC_GETPAID_REMIT_TO_CODES'
   AND NVL(hcp.attribute2, '2') = flv.lookup_code
   AND apsa.status = 'OP' AND apsa.amount_due_remaining != 0 AND apsa.org_id = rcta.org_id
UNION
------------------------------------------------------------------------------------------------------------------------------
-- CONVERSION Invoices
-- rcta.ship_to_customer_id IS NULL
------------------------------------------------------------------------------------------------------------------------------
SELECT (RPAD(NVL(hca.account_number, ' '), 20, ' ')                                            --    CUSTOMER_NUMBER  
     || RPAD(NVL(rcta.trx_number, ' '), 20, ' ')                                               --    TRANSACTION_NUMBER
     || RPAD(NVL(TO_CHAR(rcta.trx_date, 'MMDDYYYY'), ' '),8, ' ')                              --    TRANSACTION_DATE
     || LPAD(NVL(TO_CHAR(apsa.AMOUNT_DUE_REMAINING,'9999999990.99'), '0.00'),20, ' ')          --    UNPAID_BALANCE
     || LPAD(NVL(TO_CHAR(apsa.due_date - rcta.trx_date), '0.00'),4,' ')                        --    NET_DUE_DAYS
     || LPAD(NVL(TO_CHAR(gp_dmp.invamt,'9999999990.99'),'0.00'),20,' ')                        --    TRANSACTION_AMOUNT
     || RPAD(NVL(SUBSTR(hcsu_s.location, 1, DECODE(instr(hcsu_s.location,'-',-1),0,LENGTH(hcsu_s.location),instr(hcsu_s.location,'-',-1)- 1) ), ' '), 30, ' ') --    REFERENCE_NUMBER
     || RPAD(NVL(rcta.purchase_order,' '),54,' ')                                              --    PURCHASE_ORDER_NUMBER
     || RPAD(' ', 10, ' ')                                                                     --    DIVISION 
     || LPAD(NVL( TO_CHAR(gp_dmp.tax,'9999999990.99'),'0.00'),20,' ')                          --    TAX
     || LPAD(NVL( TO_CHAR(gp_dmp.freight,'9999999990.99'), '0.00'),20,' ')                     --    FREIGHT
     || LPAD(NVL( TO_CHAR(gp_dmp.other,'9999999990.99'),'0.00'),20,' ')                        --    OTHER
     || RPAD(NVL( gp_dmp.arstat  , ' '), 1, ' ')                                               --    TRANSACTION_TYPE
     || RPAD(DECODE(NVL( hcp_s.credit_hold,hcp.credit_hold),'Y','H', 'N', 'N'),20,' ')         --    CHECK_NUMBER 
     || RPAD(' ', 10, ' ')                                                                     --    PROBLEM_NUMBER
     || RPAD(NVL((SELECT al.lookup_code
                    FROM apps.ar_receivable_applications araa
                       , ar_lookups al
                   WHERE 1 = 1 
                     AND al.lookup_type = 'XXWC_DCMT_REASON_CODES'
                     AND araa.receivable_application_id IN (SELECT MAX(receivable_application_id) 
                                                              FROM apps.ar_receivable_applications araa2
                                                             WHERE 1 = 1
                                                               AND araa.applied_customer_trx_id = araa2.applied_customer_trx_id
                                                               AND araa.org_id = araa2.org_id
                                                            )
                     AND araa.applied_customer_trx_id = rcta.customer_trx_id
                     AND araa.attribute1 = al.meaning
                     AND to_char(araa.org_id) = araa.attribute_category), ' '), 10, ' ')       --    REASON_CODE
     || RPAD(NVL((SELECT jrdv.resource_name
                   FROM jtf_rs_defresources_v jrdv
                      , apps.ra_salesreps      rsa
                  WHERE rcta.primary_salesrep_id = rsa.salesrep_id
                    AND rcta.org_id = rsa.org_id
                    AND rsa.person_id = jrdv.source_id)
        , ' '),10,' ')                                                                         --    SALES_PERSON
     || RPAD(NVL( TO_CHAR(hps_s.party_site_number) , ' '), 20, ' ')                            --    SHIP_TO_ID
     || RPAD(' ', 20, ' ')                                                                     --    SRC_INVOICE 
     || RPAD(' ', 10, ' ')                                                                     --    TRANSACTION_CURRENCY
     || LPAD(NVL( TO_CHAR(gp_dmp.invamt,'9999999990.99') , '0.00'), 20, ' ')                   --    TRANS_CURR_ORIG_AMT
     || LPAD(NVL( TO_CHAR(apsa.amount_due_remaining,'9999999990.99'), '0.00'), 20, ' ')        --    TRANSACTION_CURRENCY_BALANCE
     || LPAD('0.00', 20, ' ')                                                                  --    LOCAL_CURRENCY_ORIGINAL_AMOUNT
     || LPAD('0.00', 20, ' ')                                                                  --    LOCAL_CURRENCY_BALANCE
     || RPAD(NVL(flv.lookup_code,' '), 10, ' ')                                                --    DIVISION_CODE
     || RPAD(NVL(LPAD(gp_exti_dmp.warehse, 3, '0'), '000'), 10, ' ')                           --    SALES_AREA
     || RPAD(NVL(gp_dmp.FLEXFIELD1, ' '), 35, ' ')                                             --    FLEXFIELD1
     || RPAD(NVL(gp_dmp.FLEXFIELD2,' '), 35, ' ')                                              --    FLEXFIELD2     
     || RPAD(NVL(gp_dmp.FLEXFIELD3,' '), 35, ' ')                                              --    FLEXFIELD3
     || RPAD(NVL(gp_dmp.FLEXFIELD4,' '), 35, ' ')                                              --    FLEXFIELD4
     || RPAD(NVL(gp_dmp.FLEXFIELD5,' '), 35, ' ')                                              --    FLEXFIELD5
     || LPAD(NVL(TO_CHAR(gp_dmp.FLEXNUM1,'9999999990.99'),'0.00'), 20, ' ')                    --    FLEXNUM1
--     || LPAD(NVL(TO_CHAR(gp_dmp.FLEXNUM2,'9999999990.99'),'0.00'), 20, ' ')                    --    FLEXNUM2
     || LPAD(NVL(TO_CHAR(xxwc_ar_getpaid_ob_pkg.get_credit_limit(rcta.customer_trx_id , rcta.org_id ),'9999999990.99'),'0.00'), 20, ' ')  --    FLEXNUM2
     || LPAD(NVL(TO_CHAR(xxwc_ar_getpaid_ob_pkg.get_flexnum3(rcta.org_id
                                                           , apsa.class
                                                           , hps_s.party_site_number
                                                           , rcta.ship_to_site_use_id
                                                           , hps_s.location_id
                                                           , rcta.interface_header_attribute2
                                                           , rcta.interface_header_attribute8)
                                                     ,'9999999990.99'),' '), 20, ' ')       --    FLEXNUM3
     || LPAD(NVL(TO_CHAR(gp_dmp.FLEXNUM4,'9999999990.99'),'0.00'), 20, ' ')                    --    FLEXNUM4
     || LPAD(NVL(TO_CHAR(gp_dmp.FLEXNUM5,'9999999990.99'),'0.00'), 20, ' ')                    --    FLEXNUM5
     || RPAD(' ', 8, ' ')                                                                      --    FLEXDATE1     
     || RPAD(' ', 8, ' ')                                                                      --    FLEXDATE2
     || RPAD(' ', 8, ' ')                                                                      --    FLEXDATE3
     || RPAD(' ', 8, ' ')                                                                      --    FLEXDATE4
     || RPAD(TO_CHAR(rcta.creation_date, 'MMDDYYYY'), 8, ' ')                                  --    FLEXDATE5
     || RPAD(' ', 10, ' ')                                                                     --    DISCOUNT_CODE
     || RPAD(' ', 10, ' ')                                                                     --    AR_TARGET_SYSTEM
     || RPAD(NVL(gp_dmp.FLEXFIELD6, ' '), 50, ' ')                                             --    FLEXFIELD6
     || RPAD(NVL(gp_dmp.FLEXFIELD7, ' '), 50, ' ')                                             --    FLEXFIELD7
     || RPAD(NVL(gp_dmp.FLEXFIELD8, ' '), 50, ' ')                                             --    FLEXFIELD8
     || RPAD(NVL(flv.attribute1, ' '), 50, ' ')                                                  --    FLEXFIELD9
     || RPAD(NVL(flv.attribute2, ' '), 50, ' ')                                                  --    FLEXFIELD10
     || RPAD(NVL((CASE WHEN SUBSTR(hcsu_s.location, instr(hcsu_s.location,'-',-1)+ 1 ) = hps_s.party_site_number
                  THEN
                     RPAD(' ', 50, ' ')
                  WHEN instr(hcsu_s.location,'-',-1) = 0 THEN 
                     RPAD(' ', 50, ' ')
                  ELSE
                     RPAD(SUBSTR(hcsu_s.location, instr(hcsu_s.location,'-',-1)+ 1 ), 50, ' ')  
                  END)
       , ' '), 50, ' ')                                                                        --    FLEXFIELD11
     || RPAD(NVL(hcas_s.attribute19, ' '), 50, ' ')                                              --    FLEXFIELD12
     || RPAD(NVL(substr(gp_dmp.FLEXFIELD13,1,15)||' '||SUBSTR(NVL(hcp.account_status,' '),1,1), '080' ), 50, ' ') --    FLEXFIELD13
     || RPAD(NVL(gp_dmp.flexfield14, '0'), 50, ' ')                                            --    FLEXFIELD14
     || RPAD(NVL(xxwc_ar_getpaid_ob_pkg.get_lien_by_state(rcta.bill_to_site_use_id) , ' '), 50, ' ') --   FLEXFIELD15
     || RPAD(' ', 10, ' ')                                                                     --    JOURNAL_IDENTIFIER
      ) REC_LINE , rcta.org_id
  FROM apps.ra_customer_trx rcta
     , apps.hz_cust_accounts hca
     , apps.hz_cust_accounts hca_s
     , apps.hz_cust_acct_sites hcas_s
     , apps.hz_cust_site_uses hcsu_s
     , hz_party_sites hps_s
--     , apps.hz_cust_accounts hca_b
--     , apps.hz_cust_acct_sites hcas_b
--     , apps.hz_cust_site_uses hcsu_b
--     , hz_party_sites hps_b
--     , hz_locations hl_b
     , ra_terms rt
     , apps.ar_payment_schedules apsa
     , apps.ra_cust_trx_types rctt
     , fnd_lookup_values_vl flv
     , hz_customer_profiles hcp
     , hz_customer_profiles hcp_s
     -- , ra_batch_sources_all         rbsa
     , apps.xxwc_armast_getpaid_dump_tbl gp_dmp
     , apps.xxwc_arexti_getpaid_dump_tbl gp_exti_dmp
 WHERE 1 = 1
   AND hca.cust_account_id = rcta.bill_to_customer_id
--   AND NVL(rcta.interface_header_context, '&*^%$#@') = 'CONVERSION'
   -- AND rcta.batch_source_id        = rbsa.batch_source_id
   -- AND rcta.org_id                 = rbsa.org_id
   AND xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id )   = 'CONVERSION'
   AND rcta.ship_to_customer_id IS NULL
   AND hca_s.cust_account_id = rcta.bill_to_customer_id
   AND hcas_s.cust_account_id = hca_s.cust_account_id
   AND hcas_s.cust_acct_site_id = hcsu_s.cust_acct_site_id
   AND NVL(hcas_s.ship_to_flag, 'N') = 'P'
   AND hcsu_s.site_use_code = 'SHIP_TO'
   AND hcas_s.party_site_id =  hps_s.party_site_id
--   AND rcta.bill_to_site_use_id = hcsu_b.site_use_id
--   AND hcas_b.cust_acct_site_id = hcsu_b.cust_acct_site_id
--   AND hcas_b.party_site_id =  hps_b.party_site_id
--   AND hca_b.cust_account_id = rcta.bill_to_customer_id
--   AND hps_b.location_id = hl_b.location_id
   AND hca.cust_account_id = hcp.cust_account_id
   AND hcp.site_use_id IS  NULL
   AND rcta.ship_to_site_use_id = hcp_s.site_use_id   (+) 
   AND rcta.cust_trx_type_id = rctt.cust_trx_type_id
   AND apsa.customer_trx_id = rcta.customer_trx_id
   AND rcta.term_id = rt.term_id (+)
   AND rcta.org_id = rctt.org_id
   AND flv.lookup_type = 'XXWC_GETPAID_REMIT_TO_CODES'
   AND NVL(hcp.attribute2, '2') = flv.lookup_code
   AND apsa.status = 'OP' AND apsa.amount_due_remaining != 0 AND apsa.org_id = rcta.org_id
   AND rcta.trx_number = gp_dmp.invno
   AND hca.account_number = gp_dmp.custno
   AND rcta.trx_number = gp_exti_dmp.invno
   AND hca.account_number = gp_exti_dmp.custno
UNION
------------------------------------------------------------------------------------------------------------------------------
-- CONVERSION Invoices
-- Customer does not have Ship-To Address
------------------------------------------------------------------------------------------------------------------------------
SELECT (RPAD(NVL(hca.account_number, ' '), 20, ' ')                                            --    CUSTOMER_NUMBER  
     || RPAD(NVL(rcta.trx_number, ' '), 20, ' ')                                               --    TRANSACTION_NUMBER
     || RPAD(NVL(TO_CHAR(rcta.trx_date, 'MMDDYYYY'), ' '),8, ' ')                              --    TRANSACTION_DATE
     || LPAD(NVL(TO_CHAR(apsa.AMOUNT_DUE_REMAINING,'9999999990.99'), '0.00'),20, ' ')          --    UNPAID_BALANCE
     || LPAD(NVL(TO_CHAR(apsa.due_date - rcta.trx_date), '0.00'),4,' ')                        --    NET_DUE_DAYS
     || LPAD(NVL(TO_CHAR(gp_dmp.invamt,'9999999990.99'),'0.00'),20,' ')                        --    TRANSACTION_AMOUNT
     || RPAD(NVL(gp_dmp.refno, ' '), 30, ' ')                                       --    REFERENCE_NUMBER
     || RPAD(NVL(rcta.purchase_order,' '),54,' ')                                              --    PURCHASE_ORDER_NUMBER
     || RPAD(' ', 10, ' ')                                                                     --    DIVISION 
     || LPAD(NVL( TO_CHAR(gp_dmp.tax,'9999999990.99'),'0.00'),20,' ')                          --    TAX
     || LPAD(NVL( TO_CHAR(gp_dmp.freight,'9999999990.99'), '0.00'),20,' ')                     --    FREIGHT
     || LPAD(NVL( TO_CHAR(gp_dmp.other,'9999999990.99'),'0.00'),20,' ')                        --    OTHER
     || RPAD(NVL( gp_dmp.arstat  , ' '), 1, ' ')                                               --    TRANSACTION_TYPE
     || RPAD(DECODE(hcp.credit_hold,'Y','H', 'N', 'N'),20,' ')                                 --    CHECK_NUMBER 
     || RPAD(' ', 10, ' ')                                                                     --    PROBLEM_NUMBER
     || RPAD(NVL((SELECT al.lookup_code
                    FROM apps.ar_receivable_applications araa
                       , ar_lookups al
                   WHERE 1 = 1 
                     AND al.lookup_type = 'XXWC_DCMT_REASON_CODES'
                     AND araa.receivable_application_id IN (SELECT MAX(receivable_application_id) 
                                                              FROM apps.ar_receivable_applications araa2
                                                             WHERE 1 = 1
                                                               AND araa.applied_customer_trx_id = araa2.applied_customer_trx_id
                                                               AND araa.org_id = araa2.org_id
                                                            )
                     AND araa.applied_customer_trx_id = rcta.customer_trx_id
                     AND araa.attribute1 = al.meaning
                     AND to_char(araa.org_id) = araa.attribute_category), ' '), 10, ' ')       --    REASON_CODE
                     
     || RPAD(NVL((SELECT jrdv.resource_name
                   FROM jtf_rs_defresources_v jrdv
                      , apps.ra_salesreps      rsa
                  WHERE rcta.primary_salesrep_id = rsa.salesrep_id
                    AND rcta.org_id = rsa.org_id
                    AND rsa.person_id = jrdv.source_id)
        , ' '),10,' ')                                                                         --    SALES_PERSON
     || RPAD(NVL( TO_CHAR(gp_dmp.contactid) , ' '), 20, ' ')                                  --    SHIP_TO_ID
     || RPAD(' ', 20, ' ')                                                                     --    SRC_INVOICE 
     || RPAD(' ', 10, ' ')                                                                     --    TRANSACTION_CURRENCY
     || LPAD(NVL( TO_CHAR(gp_dmp.invamt,'9999999990.99') , '0.00'), 20, ' ')                   --    TRANS_CURR_ORIG_AMT
     || LPAD(NVL( TO_CHAR(apsa.amount_due_remaining,'9999999990.99'), '0.00'), 20, ' ')        --    TRANSACTION_CURRENCY_BALANCE
     || LPAD('0.00', 20, ' ')                                                                  --    LOCAL_CURRENCY_ORIGINAL_AMOUNT
     || LPAD('0.00', 20, ' ')                                                                  --    LOCAL_CURRENCY_BALANCE
     || RPAD(NVL(flv.lookup_code,' '), 10, ' ')                                                --    DIVISION_CODE
     || RPAD(NVL(LPAD(gp_exti_dmp.warehse, 3, '0'), '000'), 10, ' ')                           --    SALES_AREA
     || RPAD(NVL(gp_dmp.FLEXFIELD1, ' '), 35, ' ')                                             --    FLEXFIELD1
     || RPAD(NVL(gp_dmp.FLEXFIELD2,' '), 35, ' ')                                              --    FLEXFIELD2     
     || RPAD(NVL(gp_dmp.FLEXFIELD3,' '), 35, ' ')                                              --    FLEXFIELD3
     || RPAD(NVL(gp_dmp.FLEXFIELD4,' '), 35, ' ')                                              --    FLEXFIELD4
     || RPAD(NVL(gp_dmp.FLEXFIELD5,' '), 35, ' ')                                              --    FLEXFIELD5
     || LPAD(NVL(TO_CHAR(gp_dmp.FLEXNUM1,'9999999990.99'),'0.00'), 20, ' ')                    --    FLEXNUM1
--     || LPAD(NVL(TO_CHAR(gp_dmp.FLEXNUM2,'9999999990.99'),'0.00'), 20, ' ')                    --    FLEXNUM2
     || LPAD(NVL(TO_CHAR(xxwc_ar_getpaid_ob_pkg.get_credit_limit(rcta.customer_trx_id , rcta.org_id ),'9999999990.99'),'0.00'), 20, ' ')  --    FLEXNUM2
     || LPAD(NVL(TO_CHAR(xxwc_ar_getpaid_ob_pkg.get_flexnum3(rcta.org_id
                                                           , apsa.class
                                                           , NULL
                                                           , rcta.ship_to_site_use_id
                                                           , NULL
                                                           , rcta.interface_header_attribute2
                                                           , rcta.interface_header_attribute8)
                                                     ,'9999999990.99'),' '), 20, ' ')       --    FLEXNUM3
     || LPAD(NVL(TO_CHAR(gp_dmp.FLEXNUM4,'9999999990.99'),'0.00'), 20, ' ')                    --    FLEXNUM4
     || LPAD(NVL(TO_CHAR(gp_dmp.FLEXNUM5,'9999999990.99'),'0.00'), 20, ' ')                    --    FLEXNUM5
     || RPAD(' ', 8, ' ')                                                                      --    FLEXDATE1     
     || RPAD(' ', 8, ' ')                                                                      --    FLEXDATE2
     || RPAD(' ', 8, ' ')                                                                      --    FLEXDATE3
     || RPAD(' ', 8, ' ')                                                                      --    FLEXDATE4
     || RPAD(TO_CHAR(rcta.creation_date, 'MMDDYYYY'), 8, ' ')                                  --    FLEXDATE5
     || RPAD(' ', 10, ' ')                                                                     --    DISCOUNT_CODE
     || RPAD(' ', 10, ' ')                                                                     --    AR_TARGET_SYSTEM
     || RPAD(NVL(gp_dmp.FLEXFIELD6, ' '), 50, ' ')                                             --    FLEXFIELD6
     || RPAD(NVL(gp_dmp.FLEXFIELD7, ' '), 50, ' ')                                             --    FLEXFIELD7
     || RPAD(NVL(gp_dmp.FLEXFIELD8, ' '), 50, ' ')                                             --    FLEXFIELD8
     || RPAD(NVL(flv.attribute1, ' '), 50, ' ')                                                --    FLEXFIELD9
     || RPAD(NVL(flv.attribute2, ' '), 50, ' ')                                                --    FLEXFIELD10
     || RPAD(NVL(gp_dmp.FLEXFIELD11, ' '), 50, ' ')                                            --    FLEXFIELD11
     || RPAD(' ' , 50, ' ')                                                                    --    FLEXFIELD12
     || RPAD(NVL(substr(gp_dmp.FLEXFIELD13,1,15)||' '||SUBSTR(NVL(hcp.account_status,' '),1,1),'080'), 50, ' ')  --    FLEXFIELD13
     || RPAD(NVL(gp_dmp.flexfield14, '0'), 50, ' ')                                            --    FLEXFIELD14
     || RPAD(NVL(xxwc_ar_getpaid_ob_pkg.get_lien_by_state(rcta.bill_to_site_use_id) , ' '), 50, ' ') --   FLEXFIELD15
     || RPAD(' ', 10, ' ')                                                                     --    JOURNAL_IDENTIFIER
      ) REC_LINE , rcta.org_id
  FROM apps.ra_customer_trx rcta
     , apps.hz_cust_accounts hca
--     , apps.hz_cust_accounts hca_b
--     , apps.hz_cust_acct_sites hcas_b
--     , apps.hz_cust_site_uses hcsu_b
--     , hz_party_sites hps_b
--     , hz_locations hl_b
     , ra_terms rt
     , apps.ar_payment_schedules apsa
     , apps.ra_cust_trx_types rctt
     , fnd_lookup_values_vl flv
     , hz_customer_profiles hcp
     -- , ra_batch_sources_all         rbsa
     , apps.xxwc_armast_getpaid_dump_tbl gp_dmp
     , apps.xxwc_arexti_getpaid_dump_tbl gp_exti_dmp
 WHERE 1 = 1
   AND hca.cust_account_id = rcta.bill_to_customer_id
--   AND NVL(rcta.interface_header_context, '&*^%$#@') = 'CONVERSION'
--   AND rcta.batch_source_id             = rbsa.batch_source_id
--   AND rcta.org_id                      = rbsa.org_id
   AND xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id )        = 'CONVERSION'
   AND rcta.ship_to_customer_id IS NULL
   AND NOT EXISTS (SELECT '1'
                     FROM apps.hz_cust_acct_sites hcas_s
                        , apps.hz_cust_site_uses  hcsu_s
                    WHERE 1 = 1
                      AND hcas_s.cust_account_id   = rcta.bill_to_customer_id
                      AND hcas_s.cust_acct_site_id = hcsu_s.cust_acct_site_id
                      AND hcas_s.org_id            = rcta.org_id
                      AND hcsu_s.site_use_code     = 'SHIP_TO')
--   AND rcta.bill_to_site_use_id = hcsu_b.site_use_id
--   AND hcas_b.cust_acct_site_id = hcsu_b.cust_acct_site_id
--   AND hcas_b.party_site_id =  hps_b.party_site_id
--   AND hca_b.cust_account_id = rcta.bill_to_customer_id
--   AND hps_b.location_id = hl_b.location_id
   AND hca.cust_account_id = hcp.cust_account_id
   AND hcp.site_use_id IS  NULL
   AND rcta.cust_trx_type_id = rctt.cust_trx_type_id
   AND apsa.customer_trx_id = rcta.customer_trx_id
   AND rcta.term_id = rt.term_id (+)
   AND rcta.org_id = rctt.org_id
   AND flv.lookup_type = 'XXWC_GETPAID_REMIT_TO_CODES'
   AND NVL(hcp.attribute2, '2') = flv.lookup_code
   AND apsa.status = 'OP' AND apsa.amount_due_remaining != 0 AND apsa.org_id = rcta.org_id
   AND rcta.trx_number = gp_dmp.invno
   AND hca.account_number = gp_dmp.custno
   AND rcta.trx_number = gp_exti_dmp.invno
   AND hca.account_number = gp_exti_dmp.custno
UNION
------------------------------------------------------------------------------------------------------------------------------
-- CONVERSION Cash
------------------------------------------------------------------------------------------------------------------------------
SELECT RPAD(NVL(hca.account_number, ' '), 20, ' ')                                            -- CUSTNO
    || RPAD(NVL(dmp.INVNO, ' '), 20, ' ')                                             -- INVNO
    || RPAD(NVL(TO_CHAR(dmp.INVDTE,'MMDDYYYY'), ' '), 8, ' ')                                            -- INVDTE
    || LPAD(NVL(TO_CHAR(apsa.amount_due_remaining,'9999999990.99'), ' '), 20, ' ')    -- BALANCE
    || LPAD(NVL(TO_CHAR(dmp.PNET), ' '), 4, ' ')                                               -- PNET
    || LPAD(NVL(TO_CHAR(acra.amount,'9999999990.99'), ' '), 20, ' ')                  -- INVAMT
    || RPAD(NVL(dmp.REFNO, ' '), 30, ' ')                                                       -- REFNO
    || RPAD(NVL(dmp.PONUM, ' '), 54, ' ')                                                       -- PONUM
    || RPAD(NVL(dmp.DIVISION, ' '), 10, ' ')                                                    -- DIVISION
    || LPAD(NVL(TO_CHAR(dmp.TAX,'9999999990.99'), ' '), 20, ' ')                                                         -- TAX
    || LPAD(NVL(TO_CHAR(dmp.FREIGHT,'9999999990.99'), ' '), 20, ' ')                                                     -- FREIGHT
    || LPAD(NVL(TO_CHAR(dmp.OTHER,'9999999990.99'), ' '), 20, ' ')                                                       -- OTHER
    || RPAD(NVL(dmp.ARSTAT, ' '), 1, ' ')                                                      -- ARSTAT
    || RPAD(NVL(dmp.CHECKNUM, ' '), 20, ' ')                                                    -- CHECKNUM --?????
    || RPAD(NVL(dmp.PROBNUM, ' '), 10, ' ')                                                     -- PROBNUM
    || RPAD(NVL(dmp.REASCODE, ' '), 10, ' ')                                                    -- REASCODE --????
    || RPAD(NVL(dmp.SALESPN, ' '), 10, ' ')                                                     -- SALESPN --????
    || RPAD(NVL(dmp.CONTACTID, ' '), 20, ' ')                                                   -- CONTACTID
    || RPAD(NVL(dmp.SRCINVOICE, ' '), 20, ' ')                                                  -- SRCINVOICE
    || RPAD('USD', 10, ' ')                                                           -- TRANCURR
    || LPAD(NVL(TO_CHAR(dmp.TRANORIG,'9999999990.99'), ' '), 20, ' ')                                                    -- TRANORIG
    || LPAD(NVL(TO_CHAR(apsa.amount_due_remaining,'9999999990.99'), ' '), 20, ' ')    -- TRANBAL
    || LPAD(NVL(TO_CHAR(acra.amount,'9999999990.99'), ' '), 20, ' ')                  -- LOCORIG
    || LPAD(NVL(TO_CHAR(apsa.amount_due_remaining,'9999999990.99'), ' '), 20, ' ')    -- LOCBAL
    || RPAD(NVL(dmp.DIVCODE, ' '), 10, ' ')                                                     -- DIVCODE
    || RPAD(NVL(dmp.SALESAREA, ' '), 10, ' ')                                                   -- SALESAREA
    || RPAD(NVL(dmp.FLEXFIELD1, ' '), 35, ' ')                                                  -- FLEXFIELD1
    || RPAD(NVL(dmp.FLEXFIELD2, ' '), 35, ' ')                                                  -- FLEXFIELD2
    || RPAD(NVL(dmp.FLEXFIELD3, ' '), 35, ' ')                                                  -- FLEXFIELD3
    || RPAD(NVL(dmp.FLEXFIELD4, ' '), 35, ' ')                                                  -- FLEXFIELD4
    || RPAD(NVL(dmp.FLEXFIELD5, ' '), 35, ' ')                                                  -- FLEXFIELD5
    || LPAD(NVL(TO_CHAR(dmp.FLEXNUM1), ' '), 20, ' ')                                                    -- FLEXNUM1
    || LPAD(NVL(TO_CHAR(dmp.FLEXNUM2), ' '), 20, ' ')                                                    -- FLEXNUM2
    || LPAD(' ', 20, ' ')                                                                                -- FLEXNUM3
    || LPAD(NVL(TO_CHAR(dmp.FLEXNUM4), ' '), 20, ' ')                                                    -- FLEXNUM4
    || LPAD(NVL(TO_CHAR(dmp.FLEXNUM5), ' '), 20, ' ')                                                    -- FLEXNUM5
    || RPAD(NVL(TO_CHAR(dmp.FLEXDATE1,'MMDDYYYY'), ' '), 8, ' ')                                                    -- FLEXDATE1
    || RPAD(NVL(TO_CHAR(dmp.FLEXDATE2,'MMDDYYYY'), ' '), 8, ' ')                                                    -- FLEXDATE2
    || RPAD(NVL(TO_CHAR(dmp.FLEXDATE3,'MMDDYYYY'), ' '), 8, ' ')                                                    -- FLEXDATE3
    || RPAD(NVL(TO_CHAR(dmp.FLEXDATE4,'MMDDYYYY'), ' '), 8, ' ')                                                    -- FLEXDATE4
    || RPAD(TO_CHAR(acra.creation_date, 'MMDDYYYY'), 8, ' ')                                    -- FLEXDATE5
    || RPAD(NVL(dmp.DISCODE, ' '), 10, ' ')                                                     -- DISCODE
    || RPAD(NVL(dmp.ARTARGETSYSTEM, ' '), 10, ' ')                                              -- ARTARGETSYSTEM
    || RPAD(NVL(dmp.FLEXFIELD6, ' '), 50, ' ')                                                  -- FLEXFIELD6
    || RPAD(NVL(dmp.FLEXFIELD7, ' '), 50, ' ')                                                  -- FLEXFIELD7
    || RPAD(NVL(dmp.FLEXFIELD8, ' '), 50, ' ')                                                  -- FLEXFIELD8
    || RPAD(NVL(dmp.FLEXFIELD9, ' '), 50, ' ')                                                  -- FLEXFIELD9
    || RPAD(NVL(dmp.FLEXFIELD10, ' '), 50, ' ')                                                 -- FLEXFIELD10
    || RPAD(NVL(dmp.FLEXFIELD11, ' '), 50, ' ')                                                 -- FLEXFIELD11
    || RPAD(' ', 50, ' ')                                                                       -- FLEXFIELD12
    || RPAD(NVL(substr(dmp.FLEXFIELD13,1,15)||' '||SUBSTR(NVL(hcp.account_status,' '),1,1),'080'), 50, ' ')  -- FLEXFIELD13
    || RPAD(NVL(dmp.FLEXFIELD14, ' '), 50, ' ')                                                 -- FLEXFIELD14
    || RPAD(NVL(dmp.FLEXFIELD15, ' '), 50, ' ')                                                 -- FLEXFIELD15
    || RPAD(' ', 10, ' ')                                                             -- GA_JOURNAL_ID
             REC_LINE , acra.org_id
  FROM apps.ar_cash_receipts     acra
     , apps.ar_payment_schedules apsa
     , ar_receipt_methods       arm
     , ar_receipt_classes       arc
     , apps.xxwc_armast_getpaid_dump_tbl dmp
     , apps.hz_cust_accounts     hca
     , hz_customer_profiles     hcp
 WHERE 1 = 1
   AND acra.type                    = 'CASH'
   AND acra.cash_receipt_id         = apsa.cash_receipt_id
   AND apsa.amount_due_remaining   <> 0
   AND acra.receipt_method_id       = arm.receipt_method_id
   AND arm.receipt_class_id         = arc.receipt_class_id
   AND arc.name                     = 'CONV - Receipt'
   AND TO_CHAR(acra.receipt_number) = dmp.INVNO
   AND hca.cust_account_id          = acra.PAY_FROM_CUSTOMER
   AND hca.cust_account_id          = hcp.cust_account_id
   AND hcp.site_use_id             IS  NULL
   AND hca.account_number           = dmp.custno
UNION
------------------------------------------------------------------------------------------------------------------------------
-- CONVERSION Cash
-- hca.attribute16 has PRISM Customer#
------------------------------------------------------------------------------------------------------------------------------
SELECT RPAD(NVL(hca.account_number, ' '), 20, ' ')                                            -- CUSTNO
    || RPAD(NVL(dmp.INVNO, ' '), 20, ' ')                                             -- INVNO
    || RPAD(NVL(TO_CHAR(dmp.INVDTE,'MMDDYYYY'), ' '), 8, ' ')                                            -- INVDTE
    || LPAD(NVL(TO_CHAR(apsa.amount_due_remaining,'9999999990.99'), ' '), 20, ' ')    -- BALANCE
    || LPAD(NVL(TO_CHAR(dmp.PNET), ' '), 4, ' ')                                               -- PNET
    || LPAD(NVL(TO_CHAR(acra.amount,'9999999990.99'), ' '), 20, ' ')                  -- INVAMT
    || RPAD(NVL(dmp.REFNO, ' '), 30, ' ')                                                       -- REFNO
    || RPAD(NVL(dmp.PONUM, ' '), 54, ' ')                                                       -- PONUM
    || RPAD(NVL(dmp.DIVISION, ' '), 10, ' ')                                                    -- DIVISION
    || LPAD(NVL(TO_CHAR(dmp.TAX,'9999999990.99'), ' '), 20, ' ')                                                         -- TAX
    || LPAD(NVL(TO_CHAR(dmp.FREIGHT,'9999999990.99'), ' '), 20, ' ')                                                     -- FREIGHT
    || LPAD(NVL(TO_CHAR(dmp.OTHER,'9999999990.99'), ' '), 20, ' ')                                                       -- OTHER
    || RPAD(NVL(dmp.ARSTAT, ' '), 1, ' ')                                                      -- ARSTAT
    || RPAD(NVL(dmp.CHECKNUM, ' '), 20, ' ')                                                    -- CHECKNUM --?????
    || RPAD(NVL(dmp.PROBNUM, ' '), 10, ' ')                                                     -- PROBNUM
    || RPAD(NVL(dmp.REASCODE, ' '), 10, ' ')                                                    -- REASCODE --????
    || RPAD(NVL(dmp.SALESPN, ' '), 10, ' ')                                                     -- SALESPN --????
    || RPAD(NVL(dmp.CONTACTID, ' '), 20, ' ')                                                   -- CONTACTID
    || RPAD(NVL(dmp.SRCINVOICE, ' '), 20, ' ')                                                  -- SRCINVOICE
    || RPAD('USD', 10, ' ')                                                           -- TRANCURR
    || LPAD(NVL(TO_CHAR(dmp.TRANORIG,'9999999990.99'), ' '), 20, ' ')                                                    -- TRANORIG
    || LPAD(NVL(TO_CHAR(apsa.amount_due_remaining,'9999999990.99'), ' '), 20, ' ')    -- TRANBAL
    || LPAD(NVL(TO_CHAR(acra.amount,'9999999990.99'), ' '), 20, ' ')                  -- LOCORIG
    || LPAD(NVL(TO_CHAR(apsa.amount_due_remaining,'9999999990.99'), ' '), 20, ' ')    -- LOCBAL
    || RPAD(NVL(dmp.DIVCODE, ' '), 10, ' ')                                                     -- DIVCODE
    || RPAD(NVL(dmp.SALESAREA, ' '), 10, ' ')                                                   -- SALESAREA
    || RPAD(NVL(dmp.FLEXFIELD1, ' '), 35, ' ')                                                  -- FLEXFIELD1
    || RPAD(NVL(dmp.FLEXFIELD2, ' '), 35, ' ')                                                  -- FLEXFIELD2
    || RPAD(NVL(dmp.FLEXFIELD3, ' '), 35, ' ')                                                  -- FLEXFIELD3
    || RPAD(NVL(dmp.FLEXFIELD4, ' '), 35, ' ')                                                  -- FLEXFIELD4
    || RPAD(NVL(dmp.FLEXFIELD5, ' '), 35, ' ')                                                  -- FLEXFIELD5
    || LPAD(NVL(TO_CHAR(dmp.FLEXNUM1), ' '), 20, ' ')                                                    -- FLEXNUM1
    || LPAD(NVL(TO_CHAR(dmp.FLEXNUM2), ' '), 20, ' ')                                                    -- FLEXNUM2
    || LPAD(' ', 20, ' ')                                                                                -- FLEXNUM3
    || LPAD(NVL(TO_CHAR(dmp.FLEXNUM4), ' '), 20, ' ')                                                    -- FLEXNUM4
    || LPAD(NVL(TO_CHAR(dmp.FLEXNUM5), ' '), 20, ' ')                                                    -- FLEXNUM5
    || RPAD(NVL(TO_CHAR(dmp.FLEXDATE1,'MMDDYYYY'), ' '), 8, ' ')                                                    -- FLEXDATE1
    || RPAD(NVL(TO_CHAR(dmp.FLEXDATE2,'MMDDYYYY'), ' '), 8, ' ')                                                    -- FLEXDATE2
    || RPAD(NVL(TO_CHAR(dmp.FLEXDATE3,'MMDDYYYY'), ' '), 8, ' ')                                                    -- FLEXDATE3
    || RPAD(NVL(TO_CHAR(dmp.FLEXDATE4,'MMDDYYYY'), ' '), 8, ' ')                                                    -- FLEXDATE4
    || RPAD(TO_CHAR(acra.creation_date, 'MMDDYYYY'), 8, ' ')                                    -- FLEXDATE5
    || RPAD(NVL(dmp.DISCODE, ' '), 10, ' ')                                                     -- DISCODE
    || RPAD(NVL(dmp.ARTARGETSYSTEM, ' '), 10, ' ')                                              -- ARTARGETSYSTEM
    || RPAD(NVL(dmp.FLEXFIELD6, ' '), 50, ' ')                                                  -- FLEXFIELD6
    || RPAD(NVL(dmp.FLEXFIELD7, ' '), 50, ' ')                                                  -- FLEXFIELD7
    || RPAD(NVL(dmp.FLEXFIELD8, ' '), 50, ' ')                                                  -- FLEXFIELD8
    || RPAD(NVL(dmp.FLEXFIELD9, ' '), 50, ' ')                                                  -- FLEXFIELD9
    || RPAD(NVL(dmp.FLEXFIELD10, ' '), 50, ' ')                                                 -- FLEXFIELD10
    || RPAD(NVL(dmp.FLEXFIELD11, ' '), 50, ' ')                                                 -- FLEXFIELD11
    || RPAD(' ', 50, ' ')                                                                       -- FLEXFIELD12
    || RPAD(NVL(substr(dmp.FLEXFIELD13,1,15)||' '||SUBSTR(NVL(hcp.account_status,' '),1,1),'080'), 50, ' ')  -- FLEXFIELD13
    || RPAD(NVL(dmp.FLEXFIELD14, ' '), 50, ' ')                                                 -- FLEXFIELD14
    || RPAD(NVL(dmp.FLEXFIELD15, ' '), 50, ' ')                                                 -- FLEXFIELD15
    || RPAD(' ', 10, ' ')                                                             -- GA_JOURNAL_ID
             REC_LINE , acra.org_id
  FROM apps.ar_cash_receipts     acra
     , apps.ar_payment_schedules apsa
     , ar_receipt_methods       arm
     , ar_receipt_classes       arc
     , apps.xxwc_armast_getpaid_dump_tbl dmp
     , apps.hz_cust_accounts     hca
     , hz_customer_profiles     hcp
 WHERE 1 = 1
   AND acra.type                    = 'CASH'
   AND acra.cash_receipt_id         = apsa.cash_receipt_id
   AND apsa.amount_due_remaining   <> 0
   AND acra.receipt_method_id       = arm.receipt_method_id
   AND arm.receipt_class_id         = arc.receipt_class_id
   AND arc.name                     = 'CONV - Receipt'
   AND TO_CHAR(acra.receipt_number) = dmp.INVNO
   AND hca.cust_account_id          = acra.PAY_FROM_CUSTOMER
   AND hca.cust_account_id          = hcp.cust_account_id
   AND hcp.site_use_id             IS  NULL
   AND hca.account_number           != dmp.custno
   AND hca.attribute6                = dmp.custno
UNION
------------------------------------------------------------------------------------------------------------------------------
-- NON-CONVERSION Cash
-- ReceiptMethod != 'WC PRISM Lockbox'
------------------------------------------------------------------------------------------------------------------------------
SELECT RPAD(NVL(hca.account_number, ' '), 20, ' ')                            -- CUSTNO
    || RPAD(NVL(acra.receipt_number, ' '), 20, ' ')                           -- INVNO
    || RPAD(NVL(TO_CHAR(acra.receipt_date, 'MMDDYYYY'), ' '), 8, ' ')         -- INVDTE
    || LPAD(NVL(TO_CHAR(apsa.amount_due_remaining,'9999999990.99'), ' '), 20, ' ')   -- BALANCE
    || LPAD('0', 4, ' ')                                                      -- PNET
    || LPAD(NVL(TO_CHAR(acra.amount,'9999999990.99'), ' '), 20, ' ')          -- INVAMT
    || RPAD(' ', 30, ' ')                                                     -- REFNO
    || RPAD(' ', 54, ' ')                                                     -- PONUM
    || RPAD(' ', 10, ' ')                                                     -- DIVISION
    || LPAD(' ', 20, ' ')                                                     -- TAX
    || LPAD(' ', 20, ' ')                                                     -- FREIGHT
    || LPAD(' ', 20, ' ')                                                     -- OTHER
    || DECODE(NVL(arm.attribute1,'N'), 'Y', 'O','U')                          -- ARSTAT
    || RPAD( ' ' , 20, ' ')                                                   -- CHECKNUM --?????
    || RPAD(' ', 10, ' ')                                                     -- PROBNUM
    || RPAD(' ', 10, ' ')                                                     -- REASCODE --????
    || RPAD(' ', 10, ' ')                                                     -- SALESPN --????
    || RPAD(' ', 20, ' ')                                                     -- CONTACTID
    || RPAD(' ', 20, ' ')                                                     -- SRCINVOICE
    || RPAD('USD', 10, ' ')                                                   -- TRANCURR
    || LPAD(NVL(TO_CHAR(acra.amount,'9999999990.99'), ' '), 20, ' ')                 -- TRANORIG
    || LPAD(NVL(TO_CHAR(apsa.amount_due_remaining,'9999999990.99'), ' '), 20, ' ')   -- TRANBAL
    || LPAD(NVL(TO_CHAR(acra.amount,'9999999990.99'), ' '), 20, ' ')                 -- LOCORIG
    || LPAD(NVL(TO_CHAR(apsa.amount_due_remaining,'9999999990.99'), ' '), 20, ' ')   -- LOCBAL
    || RPAD(' ', 10, ' ')                                                     -- DIVCODE
    || RPAD(NVL(arm.attribute2,'80'), 10, ' ')                                -- SALESAREA
    || RPAD(' ', 35, ' ')                                                     -- FLEXFIELD1
    || RPAD(' ', 35, ' ')                                                     -- FLEXFIELD2
    || RPAD(' ', 35, ' ')                                                     -- FLEXFIELD3
    || RPAD(' ', 35, ' ')                                                     -- FLEXFIELD4
    || RPAD(hla.location_code, 35, ' ')                                       -- FLEXFIELD5
    || LPAD(' ', 20, ' ')                                                     -- FLEXNUM1
    || LPAD(' ', 20, ' ')                                                     -- FLEXNUM2
    || LPAD(' ', 20, ' ')                                                     -- FLEXNUM3
    || LPAD(NVL(TO_CHAR(acra.amount,'9999999990.99'), ' '), 20, ' ')          -- FLEXNUM4
    || LPAD(' ', 20, ' ')                                                     -- FLEXNUM5
    || RPAD(' ', 8, ' ')                                                      -- FLEXDATE1
    || RPAD(' ', 8, ' ')                                                      -- FLEXDATE2
    || RPAD(' ', 8, ' ')                                                      -- FLEXDATE3
    || RPAD(' ', 8, ' ')                                                      -- FLEXDATE4
    || RPAD(TO_CHAR(acra.creation_date, 'MMDDYYYY'), 8, ' ')                  -- FLEXDATE5
    || RPAD(' ', 10, ' ')                                                     -- DISCODE
    || RPAD(' ', 10, ' ')                                                     -- ARTARGETSYSTEM
    || RPAD(NVL(hla.telephone_number_1, ' '), 50, ' ')                                       --    FLEXFIELD6
    || RPAD(NVL(hla.address_line_1, ' '), 50, ' ')                                           --    FLEXFIELD7
    || RPAD(NVL(hla.town_or_city||','||hla.region_2||' '||hla.postal_code, ' '), 50, ' ')    --    FLEXFIELD8
    || RPAD(' ', 50, ' ')                                                     -- FLEXFIELD9
    || RPAD(' ', 50, ' ')                                                     -- FLEXFIELD10
    || RPAD(' ', 50, ' ')                                                     -- FLEXFIELD11
    || RPAD(' ', 50, ' ')                                                     -- FLEXFIELD12
    || RPAD(NVL(RPAD(NVL(arm.attribute2,'080'), 16, ' ')||SUBSTR(NVL(hcp.account_status,' '),1,1),'080'), 50, ' ')   -- FLEXFIELD13
    || RPAD(' ', 50, ' ')                                                     -- FLEXFIELD14
    || RPAD(' ', 50, ' ')                                                     -- FLEXFIELD15
    || RPAD(' ', 10, ' ')                                                     -- GA_JOURNAL_ID
             REC_LINE , acra.org_id
  FROM apps.ar_cash_receipts     acra
     , apps.hz_cust_accounts     hca
     , apps.ar_payment_schedules apsa
     , ar_receipt_methods       arm
     , ar_receipt_classes       arc
     , apps.hr_locations         hla
     , hr_all_organization_units hou
     , hz_customer_profiles     hcp
 WHERE 1 = 1
   AND acra.type                    = 'CASH'
   AND acra.cash_receipt_id         = apsa.cash_receipt_id
   AND hca.cust_account_id          = apsa.customer_id
   AND hca.cust_account_id          = hcp.cust_account_id
   AND hcp.site_use_id             IS  NULL
   AND apsa.amount_due_remaining   <> 0
   AND acra.receipt_method_id       = arm.receipt_method_id
   AND arm.receipt_class_id         = arc.receipt_class_id
   AND arc.name                     != 'CONV - Receipt'
   AND arm.name                     != 'WC PRISM Lockbox'
   AND hla.inventory_organization_id IS NOT NULL
   AND hla.location_id               = hou.location_id
   AND SYSDATE BETWEEN hou.date_from AND NVL(hou.date_to,SYSDATE + 1)
   AND SUBSTR(hla.location_code, 1, INSTR(hla.location_code, '-') - 2) = NVL(arm.attribute2,fnd_profile.VALUE ('XXWC_CORPORATE_ORGANIZATION')) --- 'WCC') replaced the 'WCC' value with profile value by pattabhi for TMS# 20141001-00057 
UNION
------------------------------------------------------------------------------------------------------------------------------
-- NON-CONVERSION Cash
-- ReceiptMethod = 'WC PRISM Lockbox'
------------------------------------------------------------------------------------------------------------------------------
   SELECT RPAD(NVL(hca.account_number, ' '), 20, ' ')                            -- CUSTNO
       || RPAD(NVL(acra.receipt_number||'-P', ' '), 20, ' ')                     -- INVNO
       || RPAD(NVL(TO_CHAR(acra.receipt_date, 'MMDDYYYY'), ' '), 8, ' ')         -- INVDTE
       || LPAD(NVL(TO_CHAR(apsa.amount_due_remaining,'9999999990.99'), ' '), 20, ' ')   -- BALANCE
       || LPAD('0', 4, ' ')                                                      -- PNET
       || LPAD(NVL(TO_CHAR(acra.amount,'9999999990.99'), ' '), 20, ' ')          -- INVAMT
       || RPAD(' ', 30, ' ')                                                     -- REFNO
       || RPAD(' ', 54, ' ')                                                     -- PONUM
       || RPAD(' ', 10, ' ')                                                     -- DIVISION
       || LPAD(' ', 20, ' ')                                                     -- TAX
       || LPAD(' ', 20, ' ')                                                     -- FREIGHT
       || LPAD(' ', 20, ' ')                                                     -- OTHER
       || DECODE(NVL(arm.attribute1,'N'), 'Y', 'O','U')                          -- ARSTAT
       || RPAD(' ' , 20, ' ')                                                    -- CHECKNUM --?????
       || RPAD(' ', 10, ' ')                                                     -- PROBNUM
       || RPAD(' ', 10, ' ')                                                     -- REASCODE --????
       || RPAD(' ', 10, ' ')                                                     -- SALESPN --????
       || RPAD(' ', 20, ' ')                                                     -- CONTACTID
       || RPAD(' ', 20, ' ')                                                     -- SRCINVOICE
       || RPAD('USD', 10, ' ')                                                   -- TRANCURR
       || LPAD(NVL(TO_CHAR(acra.amount,'9999999990.99'), ' '), 20, ' ')                 -- TRANORIG
       || LPAD(NVL(TO_CHAR(apsa.amount_due_remaining,'9999999990.99'), ' '), 20, ' ')   -- TRANBAL
       || LPAD(NVL(TO_CHAR(acra.amount,'9999999990.99'), ' '), 20, ' ')                 -- LOCORIG
       || LPAD(NVL(TO_CHAR(apsa.amount_due_remaining,'9999999990.99'), ' '), 20, ' ')   -- LOCBAL
       || RPAD(' ', 10, ' ')                                                     -- DIVCODE
       || RPAD(NVL(arm.attribute2,'80'), 10, ' ')                                -- SALESAREA
       || RPAD(' ', 35, ' ')                                                     -- FLEXFIELD1
       || RPAD(' ', 35, ' ')                                                     -- FLEXFIELD2
       || RPAD(' ', 35, ' ')                                                     -- FLEXFIELD3
       || RPAD(' ', 35, ' ')                                                     -- FLEXFIELD4
       || RPAD(hla.location_code, 35, ' ')                                       -- FLEXFIELD5
       || LPAD(' ', 20, ' ')                                                     -- FLEXNUM1
       || LPAD(' ', 20, ' ')                                                     -- FLEXNUM2
       || LPAD(' ', 20, ' ')                                                     -- FLEXNUM3
       || LPAD(NVL(TO_CHAR(acra.amount,'9999999990.99'), ' '), 20, ' ')          -- FLEXNUM4
       || LPAD(' ', 20, ' ')                                                     -- FLEXNUM5
       || RPAD(' ', 8, ' ')                                                      -- FLEXDATE1
       || RPAD(' ', 8, ' ')                                                      -- FLEXDATE2
       || RPAD(' ', 8, ' ')                                                      -- FLEXDATE3
       || RPAD(' ', 8, ' ')                                                      -- FLEXDATE4
       || RPAD(TO_CHAR(acra.creation_date, 'MMDDYYYY'), 8, ' ')                  -- FLEXDATE5
       || RPAD(' ', 10, ' ')                                                     -- DISCODE
       || RPAD(' ', 10, ' ')                                                     -- ARTARGETSYSTEM
       || RPAD(NVL(hla.telephone_number_1, ' '), 50, ' ')                                       --    FLEXFIELD6
       || RPAD(NVL(hla.address_line_1, ' '), 50, ' ')                                           --    FLEXFIELD7
       || RPAD(NVL(hla.town_or_city||','||hla.region_2||' '||hla.postal_code, ' '), 50, ' ')    --    FLEXFIELD8
       || RPAD(' ', 50, ' ')                                                     -- FLEXFIELD9
       || RPAD(' ', 50, ' ')                                                     -- FLEXFIELD10
       || RPAD(' ', 50, ' ')                                                     -- FLEXFIELD11
       || RPAD(' ', 50, ' ')                                                     -- FLEXFIELD12
       || RPAD(NVL(RPAD(NVL(arm.attribute2,'080'), 16, ' ')||SUBSTR(NVL(hcp.account_status,' '),1,1),'080'), 50, ' ')  -- FLEXFIELD13
       || RPAD(' ', 50, ' ')                                                     -- FLEXFIELD14
       || RPAD(' ', 50, ' ')                                                     -- FLEXFIELD15
       || RPAD(' ', 10, ' ')                                                     -- GA_JOURNAL_ID
                REC_LINE , acra.org_id
     FROM apps.ar_cash_receipts     acra
        , apps.hz_cust_accounts     hca
        , apps.ar_payment_schedules apsa
        , ar_receipt_methods       arm
        , ar_receipt_classes       arc
        , apps.hr_locations         hla
        , hr_all_organization_units hou
        , hz_customer_profiles     hcp
    WHERE 1 = 1
      AND acra.type                    = 'CASH'
      AND acra.cash_receipt_id         = apsa.cash_receipt_id
      AND hca.cust_account_id          = apsa.customer_id
      AND hca.cust_account_id          = hcp.cust_account_id
      AND hcp.site_use_id             IS  NULL
      AND apsa.amount_due_remaining   <> 0
      AND acra.receipt_method_id       = arm.receipt_method_id
      AND arm.receipt_class_id         = arc.receipt_class_id
      AND arc.name                     != 'CONV - Receipt'
      AND arm.name                     = 'WC PRISM Lockbox'
      AND hla.inventory_organization_id IS NOT NULL
      AND hla.location_id              = hou.location_id
      AND SYSDATE BETWEEN hou.date_from AND NVL(hou.date_to,SYSDATE + 1)
      AND SUBSTR(hla.location_code, 1, INSTR(hla.location_code, '-') - 2) = NVL(arm.attribute2,fnd_profile.VALUE ('XXWC_CORPORATE_ORGANIZATION')) --- 'WCC') replaced the 'WCC' value with profile value by pattabhi for TMS# 20141001-00057 
