---------------------------------------------------------------------------------------------------------------
/**************************************************************************************************************
  $Header APPS.XXEIS_2156499_TIVHBO_V $
  PURPOSE	  : EIS Delete Favorite Report List
  REVISIONS   :
  VERSION 	 DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0      26-Jul-2016        Siva   		 TMS#20160628-00049  
**************************************************************************************************************/
DROP VIEW APPS.XXEIS_2156499_TIVHBO_V;
CREATE OR REPLACE VIEW APPS.XXEIS_2156499_TIVHBO_V
AS 
SELECT XDFT.DESCRIPTION
FROM
XXEIS.XXWC_DEL_FAV_REP_TBL XDFT
/
