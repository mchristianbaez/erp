
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUSAP_FREIGHT_INVOICE_VIEW" ("INVOICE_NUM", "RECEIVED_DATE", "BRANCH", "GL_ACCOUNT", "INVOICE_AMOUNT", "VENDOR_NAME", "PAYMENT_STATUS", "AMOUNT_PAID", "CHECK_NUMBER", "PAYMENT_DATE") AS 
  SELECT ia.invoice_num
      ,trunc(ia.creation_date) received_date
      ,ia.attribute2 branch
      ,gcc.segment4 gl_account
      ,ia.invoice_amount
      ,s.vendor_name
      ,(CASE ia.payment_status_flag
         WHEN 'N' THEN
          'No'
         ELSE
          'Yes'
       END) payment_status
      ,ia.amount_paid
      ,aca.check_number
      ,aca.check_date payment_date
  FROM ap.ap_invoices_all         ia
      ,ap.ap_suppliers            s
      ,ap.ap_invoice_payments_all ipa
      ,ap.AP_CHECKS_ALL aca
      ,ap.AP_INVOICE_LINES_ALL ila
      ,gl.gl_code_combinations gcc
 WHERE s.vendor_id = ia.vendor_id
   AND ia.invoice_id = ipa.invoice_id(+)
   and ipa.check_id = aca.check_id(+)
   and ia.invoice_id = ila.invoice_id
   and ila.default_dist_ccid = gcc.code_combination_id(+)
   AND ia.pay_group_lookup_code = 'FREIGHT'
 ORDER BY received_date DESC
;
