CREATE OR REPLACE VIEW APPS.XXEIS_856486_VHZETR_V AS
WITH hds_fiscal AS
(
  SELECT extract(YEAR FROM SYSDATE) yr FROM dual
)
SELECT lh.lob
      ,lh.lease_num rer_id
      ,NULL mlo_published_comments -- need more info
      ,lh.location_code prime_loc
      ,lh.address prime_address
      ,lh.city prime_city
      ,CASE lh.country
         WHEN 'US' THEN
          lh.state
         ELSE
          lh.province
       END prime_st_prv
      ,lh.zip_code prime_postal_code
      ,lh.lease_type rer_type
      ,lp.payment_purpose
      ,lp.payment_term_type
      ,lh.lease_commencement_date comm_date
      ,lh.lease_termination_date exp_date
      ,lh.lease_status rer_status
      ,lh.capital_lease
      ,NULL cpi_fmv_escalations -- need more info
      ,lp.gl_location oracle_id
      ,lp.fru fru_id
      ,lp.lob_branch br_#
      ,lp.currency_code currency
      -- begin calculations for current year and upto 15 yrs at the max
      ,nvl((SELECT ((p.annual_total -nvl(p.january, 0))+(nvl(p1.january, 0)))
             FROM xxcus.xxcuspn_lease_payments_tbl p
                 ,xxcus.xxcuspn_lease_payments_tbl p1
            WHERE p.lease_id = lh.lease_id
              AND p.payment_purpose = lp.payment_purpose
              AND p.payment_term_type = lp.payment_term_type
              AND p.frequency_code = lp.frequency_code
              AND p.percentage = lp.percentage
              AND nvl(p.vendor_id, 0) = nvl(lp.vendor_id, 0)
              AND p.year_ca = lp.year_ca
              AND p.year_ca = (hds_fiscal.yr)
              --
              AND p1.lease_id(+) = p.lease_id
              AND p1.payment_purpose(+) = p.payment_purpose
              AND p1.payment_term_type(+) = p.payment_term_type
              AND p1.frequency_code(+) = p.frequency_code
              AND p1.percentage(+) = p.percentage
              --AND p1.vendor_id(+) =p.vendor_id
              AND nvl(p1.vendor_id, 0) =nvl(p.vendor_id, 0) --ESMS 273056
              AND p1.year_ca(+) = p.year_ca + 1 
              --         
            ), 0) current_year -- end of current year                  
      ,nvl((SELECT ((p.annual_total -nvl(p.january, 0))+(nvl(p1.january, 0)))
             FROM xxcus.xxcuspn_lease_payments_tbl p
                 ,xxcus.xxcuspn_lease_payments_tbl p1
            WHERE p.lease_id = lh.lease_id
              AND p.payment_purpose = lp.payment_purpose
              AND p.payment_term_type = lp.payment_term_type
              AND p.frequency_code = lp.frequency_code
              AND p.percentage = lp.percentage
              AND nvl(p.vendor_id, 0) = nvl(lp.vendor_id, 0)
              AND p.year_ca = (hds_fiscal.yr) + 1
              --
              AND p1.lease_id(+) = p.lease_id
              AND p1.payment_purpose(+) = p.payment_purpose
              AND p1.payment_term_type(+) = p.payment_term_type
              AND p1.frequency_code(+) = p.frequency_code
              AND p1.percentage(+) = p.percentage
              --AND p1.vendor_id(+) =p.vendor_id
              AND nvl(p1.vendor_id, 0) =nvl(p.vendor_id, 0) --ESMS 273056              
              AND p1.year_ca(+) = p.year_ca + 1  
              --             
            ), 0) year_2 -- end of year2
      ,nvl((SELECT ((p.annual_total -nvl(p.january, 0))+(nvl(p1.january, 0)))
             FROM xxcus.xxcuspn_lease_payments_tbl p
                 ,xxcus.xxcuspn_lease_payments_tbl p1
            WHERE p.lease_id = lh.lease_id
              AND p.payment_purpose = lp.payment_purpose
              AND p.payment_term_type = lp.payment_term_type
              AND p.frequency_code = lp.frequency_code
              AND p.percentage = lp.percentage
              AND nvl(p.vendor_id, 0) = nvl(lp.vendor_id, 0)
              AND p.year_ca = (hds_fiscal.yr) + 2
              --
              AND p1.lease_id(+) = p.lease_id
              AND p1.payment_purpose(+) = p.payment_purpose
              AND p1.payment_term_type(+) = p.payment_term_type
              AND p1.frequency_code(+) = p.frequency_code
              AND p1.percentage(+) = p.percentage
              --AND p1.vendor_id(+) =p.vendor_id
              AND nvl(p1.vendor_id, 0) =nvl(p.vendor_id, 0) --ESMS 273056
              AND p1.year_ca(+) = p.year_ca + 1              
              --
            ), 0) year_3 -- end of year3
      ,nvl((SELECT ((p.annual_total -nvl(p.january, 0))+(nvl(p1.january, 0)))
             FROM xxcus.xxcuspn_lease_payments_tbl p
                 ,xxcus.xxcuspn_lease_payments_tbl p1
            WHERE p.lease_id = lh.lease_id
              AND p.payment_purpose = lp.payment_purpose
              AND p.payment_term_type = lp.payment_term_type
              AND p.frequency_code = lp.frequency_code
              AND p.percentage = lp.percentage
              AND nvl(p.vendor_id, 0) = nvl(lp.vendor_id, 0)
              AND p.year_ca = (hds_fiscal.yr) + 3
              --
              AND p1.lease_id(+) = p.lease_id
              AND p1.payment_purpose(+) = p.payment_purpose
              AND p1.payment_term_type(+) = p.payment_term_type
              AND p1.frequency_code(+) = p.frequency_code
              AND p1.percentage(+) = p.percentage
              --AND p1.vendor_id(+) =p.vendor_id
              AND nvl(p1.vendor_id, 0) =nvl(p.vendor_id, 0) --ESMS 273056
              AND p1.year_ca(+) = p.year_ca + 1             
              --
            ), 0) year_4  -- end of year4      
      ,nvl((SELECT ((p.annual_total -nvl(p.january, 0))+(nvl(p1.january, 0)))
             FROM xxcus.xxcuspn_lease_payments_tbl p
                 ,xxcus.xxcuspn_lease_payments_tbl p1
            WHERE p.lease_id = lh.lease_id
              AND p.payment_purpose = lp.payment_purpose
              AND p.payment_term_type = lp.payment_term_type
              AND p.frequency_code = lp.frequency_code
              AND p.percentage = lp.percentage
              AND nvl(p.vendor_id, 0) = nvl(lp.vendor_id, 0)
              AND p.year_ca = (hds_fiscal.yr) + 4
              --
              AND p1.lease_id(+) = p.lease_id
              AND p1.payment_purpose(+) = p.payment_purpose
              AND p1.payment_term_type(+) = p.payment_term_type
              AND p1.frequency_code(+) = p.frequency_code
              AND p1.percentage(+) = p.percentage
              --AND p1.vendor_id(+) =p.vendor_id
              AND nvl(p1.vendor_id, 0) =nvl(p.vendor_id, 0) --ESMS 273056
              AND p1.year_ca(+) = p.year_ca + 1             
              --
            ), 0) year_5  -- end of year5     
      ,nvl((SELECT ((p.annual_total -nvl(p.january, 0))+(nvl(p1.january, 0)))
             FROM xxcus.xxcuspn_lease_payments_tbl p
                 ,xxcus.xxcuspn_lease_payments_tbl p1
            WHERE p.lease_id = lh.lease_id
              AND p.payment_purpose = lp.payment_purpose
              AND p.payment_term_type = lp.payment_term_type
              AND p.frequency_code = lp.frequency_code
              AND p.percentage = lp.percentage
              AND nvl(p.vendor_id, 0) = nvl(lp.vendor_id, 0)
              AND p.year_ca = (hds_fiscal.yr) + 5
              --
              AND p1.lease_id(+) = p.lease_id
              AND p1.payment_purpose(+) = p.payment_purpose
              AND p1.payment_term_type(+) = p.payment_term_type
              AND p1.frequency_code(+) = p.frequency_code
              AND p1.percentage(+) = p.percentage
              --AND p1.vendor_id(+) =p.vendor_id
              AND nvl(p1.vendor_id, 0) =nvl(p.vendor_id, 0) --ESMS 273056
              AND p1.year_ca(+) = p.year_ca + 1             
              --
            )
, 0) year_6 -- end of year6      
      ,nvl((SELECT ((p.annual_total -nvl(p.january, 0))+(nvl(p1.january, 0)))
             FROM xxcus.xxcuspn_lease_payments_tbl p
                 ,xxcus.xxcuspn_lease_payments_tbl p1             
            WHERE p.lease_id = lh.lease_id
              AND p.payment_purpose = lp.payment_purpose
              AND p.payment_term_type = lp.payment_term_type
             AND p.frequency_code = lp.frequency_code
              AND p.percentage = lp.percentage
              AND nvl(p.vendor_id, 0) = nvl(lp.vendor_id, 0)
              AND p.year_ca = (hds_fiscal.yr) + 6
              --
              AND p1.lease_id(+) = p.lease_id
              AND p1.payment_purpose(+) = p.payment_purpose
              AND p1.payment_term_type(+) = p.payment_term_type
              AND p1.frequency_code(+) = p.frequency_code
              AND p1.percentage(+) = p.percentage
              --AND p1.vendor_id(+) =p.vendor_id
              AND nvl(p1.vendor_id, 0) =nvl(p.vendor_id, 0) --ESMS 273056
              AND p1.year_ca(+) = p.year_ca + 1              
              --
            ), 0) year_7 -- end of year7      
      ,nvl((SELECT ((p.annual_total -nvl(p.january, 0))+(nvl(p1.january, 0)))
             FROM xxcus.xxcuspn_lease_payments_tbl p
                 ,xxcus.xxcuspn_lease_payments_tbl p1             
            WHERE p.lease_id = lh.lease_id
              AND p.payment_purpose = lp.payment_purpose
              AND p.payment_term_type = lp.payment_term_type
              AND p.frequency_code = lp.frequency_code
              AND p.percentage = lp.percentage
              AND nvl(p.vendor_id, 0) = nvl(lp.vendor_id, 0)
              AND p.year_ca = (hds_fiscal.yr) + 7
              --
              AND p1.lease_id(+) = p.lease_id
              AND p1.payment_purpose(+) = p.payment_purpose
              AND p1.payment_term_type(+) = p.payment_term_type
              AND p1.frequency_code(+) = p.frequency_code
              AND p1.percentage(+) = p.percentage
              --AND p1.vendor_id(+) =p.vendor_id
              AND nvl(p1.vendor_id, 0) =nvl(p.vendor_id, 0) --ESMS 273056
              AND p1.year_ca(+) = p.year_ca + 1                
              --
            ), 0) year_8  -- end of year8     
      ,nvl((SELECT ((p.annual_total -nvl(p.january, 0))+(nvl(p1.january, 0)))
             FROM xxcus.xxcuspn_lease_payments_tbl p
                 ,xxcus.xxcuspn_lease_payments_tbl p1             
            WHERE p.lease_id = lh.lease_id
              AND p.payment_purpose = lp.payment_purpose
              AND p.payment_term_type = lp.payment_term_type
              AND p.frequency_code = lp.frequency_code
              AND p.percentage = lp.percentage
              AND nvl(p.vendor_id, 0) = nvl(lp.vendor_id, 0)
              AND p.year_ca = (hds_fiscal.yr) + 8
              --
              AND p1.lease_id(+) = p.lease_id
              AND p1.payment_purpose(+) = p.payment_purpose
              AND p1.payment_term_type(+) = p.payment_term_type
              AND p1.frequency_code(+) = p.frequency_code
              AND p1.percentage(+) = p.percentage
              --AND p1.vendor_id(+) =p.vendor_id
              AND nvl(p1.vendor_id, 0) =nvl(p.vendor_id, 0) --ESMS 273056
              AND p1.year_ca(+) = p.year_ca + 1              
              --
            ), 0) year_9 -- end of year9       
      ,nvl((SELECT ((p.annual_total -nvl(p.january, 0))+(nvl(p1.january, 0)))
             FROM xxcus.xxcuspn_lease_payments_tbl p
                 ,xxcus.xxcuspn_lease_payments_tbl p1             
            WHERE p.lease_id = lh.lease_id
              AND p.payment_purpose = lp.payment_purpose
              AND p.payment_term_type = lp.payment_term_type
              AND p.frequency_code = lp.frequency_code
              AND p.percentage = lp.percentage
              AND nvl(p.vendor_id, 0) = nvl(lp.vendor_id, 0)
              AND p.year_ca = (hds_fiscal.yr) + 9
              --
              AND p1.lease_id(+) = p.lease_id
              AND p1.payment_purpose(+) = p.payment_purpose
              AND p1.payment_term_type(+) = p.payment_term_type
              AND p1.frequency_code(+) = p.frequency_code
              AND p1.percentage(+) = p.percentage
              --AND p1.vendor_id(+) =p.vendor_id
              AND nvl(p1.vendor_id, 0) =nvl(p.vendor_id, 0) --ESMS 273056
              AND p1.year_ca(+) = p.year_ca + 1               
              --
            ), 0) year_10  -- end of year10     
      ,nvl((SELECT ((p.annual_total -nvl(p.january, 0))+(nvl(p1.january, 0)))
             FROM xxcus.xxcuspn_lease_payments_tbl p
                 ,xxcus.xxcuspn_lease_payments_tbl p1             
            WHERE p.lease_id = lh.lease_id
              AND p.payment_purpose = lp.payment_purpose
              AND p.payment_term_type = lp.payment_term_type
              AND p.frequency_code = lp.frequency_code
              AND p.percentage = lp.percentage
              AND nvl(p.vendor_id, 0) = nvl(lp.vendor_id, 0)
              AND p.year_ca = (hds_fiscal.yr) + 10
              --
              AND p1.lease_id(+) = p.lease_id
              AND p1.payment_purpose(+) = p.payment_purpose
              AND p1.payment_term_type(+) = p.payment_term_type
              AND p1.frequency_code(+) = p.frequency_code
              AND p1.percentage(+) = p.percentage
              --AND p1.vendor_id(+) =p.vendor_id
              AND nvl(p1.vendor_id, 0) =nvl(p.vendor_id, 0) --ESMS 273056
              AND p1.year_ca(+) = p.year_ca + 1               
              --
            ), 0) year_11 -- end of year11
      ,nvl((SELECT ((p.annual_total -nvl(p.january, 0))+(nvl(p1.january, 0)))
             FROM xxcus.xxcuspn_lease_payments_tbl p
                 ,xxcus.xxcuspn_lease_payments_tbl p1             
            WHERE p.lease_id = lh.lease_id
              AND p.payment_purpose = lp.payment_purpose
              AND p.payment_term_type = lp.payment_term_type
              AND p.frequency_code = lp.frequency_code
              AND p.percentage = lp.percentage
              AND nvl(p.vendor_id, 0) = nvl(lp.vendor_id, 0)
              AND p.year_ca = (hds_fiscal.yr) + 11
              --
              AND p1.lease_id(+) = p.lease_id
              AND p1.payment_purpose(+) = p.payment_purpose
              AND p1.payment_term_type(+) = p.payment_term_type
              AND p1.frequency_code(+) = p.frequency_code
              AND p1.percentage(+) = p.percentage
              --AND p1.vendor_id(+) =p.vendor_id
              AND nvl(p1.vendor_id, 0) =nvl(p.vendor_id, 0) --ESMS 273056
              AND p1.year_ca(+) = p.year_ca + 1                
              --
            ), 0) year_12 -- end of year12       
      ,nvl((SELECT ((p.annual_total -nvl(p.january, 0))+(nvl(p1.january, 0)))
             FROM xxcus.xxcuspn_lease_payments_tbl p
                 ,xxcus.xxcuspn_lease_payments_tbl p1             
            WHERE p.lease_id = lh.lease_id
              AND p.payment_purpose = lp.payment_purpose
              AND p.payment_term_type = lp.payment_term_type
              AND p.frequency_code = lp.frequency_code
              AND p.percentage = lp.percentage
              AND nvl(p.vendor_id, 0) = nvl(lp.vendor_id, 0)
              AND p.year_ca = (hds_fiscal.yr) + 12
              --
              AND p1.lease_id(+) = p.lease_id
              AND p1.payment_purpose(+) = p.payment_purpose
              AND p1.payment_term_type(+) = p.payment_term_type
              AND p1.frequency_code(+) = p.frequency_code
              AND p1.percentage(+) = p.percentage
              --AND p1.vendor_id(+) =p.vendor_id
              AND nvl(p1.vendor_id, 0) =nvl(p.vendor_id, 0) --ESMS 273056
              AND p1.year_ca(+) = p.year_ca + 1               
              --
            )
, 0) year_13 -- end of year13       
      ,nvl((SELECT ((p.annual_total -nvl(p.january, 0))+(nvl(p1.january, 0)))
             FROM xxcus.xxcuspn_lease_payments_tbl p
                 ,xxcus.xxcuspn_lease_payments_tbl p1             
            WHERE p.lease_id = lh.lease_id
              AND p.payment_purpose = lp.payment_purpose
              AND p.payment_term_type = lp.payment_term_type
              AND p.frequency_code = lp.frequency_code
              AND p.percentage = lp.percentage
              AND nvl(p.vendor_id, 0) = nvl(lp.vendor_id, 0)
              AND p.year_ca = (hds_fiscal.yr) + 13
              --
              AND p1.lease_id(+) = p.lease_id
              AND p1.payment_purpose(+) = p.payment_purpose
              AND p1.payment_term_type(+) = p.payment_term_type
              AND p1.frequency_code(+) = p.frequency_code
              AND p1.percentage(+) = p.percentage
              --AND p1.vendor_id(+) =p.vendor_id
              AND nvl(p1.vendor_id, 0) =nvl(p.vendor_id, 0) --ESMS 273056
              AND p1.year_ca(+) = p.year_ca + 1              
              --
            ), 0) year_14 -- end of year14      
      ,nvl((SELECT ((p.annual_total -nvl(p.january, 0))+(nvl(p1.january, 0)))
             FROM xxcus.xxcuspn_lease_payments_tbl p
                 ,xxcus.xxcuspn_lease_payments_tbl p1             
            WHERE p.lease_id = lh.lease_id
              AND p.payment_purpose = lp.payment_purpose
              AND p.payment_term_type = lp.payment_term_type
              AND p.frequency_code = lp.frequency_code
              AND p.percentage = lp.percentage
              AND nvl(p.vendor_id, 0) = nvl(lp.vendor_id, 0)
              AND p.year_ca = (hds_fiscal.yr) + 14
              --
              AND p1.lease_id(+) = p.lease_id
              AND p1.payment_purpose(+) = p.payment_purpose
              AND p1.payment_term_type(+) = p.payment_term_type
              AND p1.frequency_code(+) = p.frequency_code
              AND p1.percentage(+) = p.percentage
              --AND p1.vendor_id(+) =p.vendor_id
              AND nvl(p1.vendor_id, 0) =nvl(p.vendor_id, 0) --ESMS 273056
              AND p1.year_ca(+) = p.year_ca + 1               
              --
            ), 0) year_15 -- end of year15
  FROM xxcus.xxcuspn_lease_headers_tbl  lh
      ,hds_fiscal
      ,(
         select *
         from   xxcus.xxcuspn_lease_payments_tbl
         where  1 =1
           and  year_ca =(SELECT extract(YEAR FROM SYSDATE) FROM dual)
       ) lp
WHERE 1 =1
  AND lh.lease_id      =lp.lease_id(+)
  AND hds_fiscal.yr(+) =lp.year_ca
ORDER BY rer_id
         ,payment_purpose
         ,payment_term_type
         ,frequency_code
         ,percentage
         ,vendor_id
         ,year_ca
;
COMMENT ON TABLE APPS.XXEIS_856486_VHZETR_V IS 'ESMS 273056'; 
