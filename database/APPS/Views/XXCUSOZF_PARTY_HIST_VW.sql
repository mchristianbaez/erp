
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUSOZF_PARTY_HIST_VW" ("PRIMARY_LOB", "BUSINESS_UNIT", "MVID", "MVID_NAME", "PAYMENT_TYPE", "DOCUMENT_NUMBER", "REBATE_PROGRAM", "DOCUMENT_DATE", "AMOUNT", "TIMEFRAME", "GL_DATE", "STATUS", "AR_INVOICE_NUM", "DEPOSIT_DATE", "IMAGE_URL", "CURRENCY_CODE", "ORG_NAME", "CREATION_DATE", "USER_NAME", "LAST_UPDATE_DATE") AS 
  SELECT hp1.known_as primary_lob,
    hp1.party_name business_unit,
    REPLACE(hca.account_number, '~MSTR', '') mvid,
    hp.party_name mvid_name,
    rm.printed_name payment_type,
    cr.receipt_number document_number,
    ofa.short_name rebate_program,
    cr.receipt_date document_date,
    cr.amount amount,
    cr.attribute2 timeframe,
    ra.gl_date gl_date,
    cr.status,
    rct.trx_number ar_invoice_num,
    cr.deposit_date,
    cr.comments image_url,
    cr.currency_code currency_code,
    hr.name org_name,
    cr.creation_date,
    usr.user_name,
    cr.last_update_date
  FROM ar_cash_receipts_all cr,
    ar_receivable_applications_all ra,
    ra_customer_trx_all rct,
    Ozf.Ozf_Funds_All_Tl Ofa,
    ra_customer_trx_lines_all rctl,
    ozf_claim_lines_all ocl,
    ozf_offers oo,
    ar_receipt_methods rm,
    hz_parties hp,
    hz_cust_accounts hca,
    hz_parties hp1,
    hr_operating_units hr,
    fnd_user usr
  WHERE 1                            = 1
  AND cr.attribute1                  = TO_CHAR(hp1.party_id)
  AND cr.receipt_method_id           = rm.receipt_method_id
  AND ra.cash_receipt_id(+)          = cr.cash_receipt_id
  AND ra.display(+)                  = 'Y'
  AND hr.organization_id             = cr.org_id
  AND ra.applied_customer_trx_id     = rct.customer_trx_id(+)
  AND rct.customer_trx_id            = rctl.customer_trx_id(+)
  AND rctl.interface_line_attribute2 = ocl.claim_id(+)
  AND ocl.activity_id     = oo.qp_list_header_id(+)
  AND oo.budget_source_id = ofa.fund_id(+)
  AND hca.cust_account_id = cr.pay_from_customer
  AND hca.party_id        = hp.party_id
  AND usr.user_id         = cr.created_by
  AND cr.org_id          IN (101, 102)
;
