--//============================================================================
--//
--// Object Name         :: xxwc_user_lists_v
--//
--// Object Type         :: View
--//
--// Object Description  :: This is WC users list extract to AgilOne
--//
--// Version Control
--//============================================================================
--// Vers    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Mahesh Kudle     26/05/2014    Initial Build - TMS#20140602-00295
--//============================================================================
CREATE OR REPLACE VIEW apps.xxwc_user_lists_v
(
   operating_unit_id
  ,wc_users
)
AS
   SELECT 162 operating_unit_id
         , (   user_name
            || '|'
            || full_name
            || '|'
            || email_address
            || '|'
            || fax
            || '|'
            || employee_number
            || '|'
            || job_description
            || '|'
            || fru
            || '|'
            || fru_description
            || '|'
            || lob_branch
            || '|'
            || region
            || '|'
            || date_last_logged_in)
             wc_users
     FROM apps.xxeis_943_roaztl_v;