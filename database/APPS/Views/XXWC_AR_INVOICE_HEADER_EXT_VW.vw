/* Formatted on 7/23/2013 1:48:35 PM (QP5 v5.115.810.9015) */
CREATE OR REPLACE FORCE VIEW apps.xxwc_ar_invoice_header_ext_vw (
   operating_unit_id
 , interface_date
 , invoice_header_data
)
AS
   SELECT	x1.operating_unit_id
		  , x1.invoice_creation_date interface_date
		  , (	x1.business_unit
			 || '|'
			 || x1.source_system
			 || '|'
			 || x1.invoice_unique_id
			 || '|'
			 || x1.invoice_number
			 || '|'
			 || x1.order_number
			 || '|'
			 || x1.release_identifier
			 || '|'
			 || TO_CHAR (x1.invoice_post_date, 'MM/DD/YYYY')
			 || '|'
			 || TO_CHAR (x1.invoice_create_date, 'MM/DD/YYYY')
			 || '|'
			 || TO_CHAR (x1.order_create_date, 'MM/DD/YYYY')
			 || '|'
			 || TO_CHAR (x1.order_date, 'MM/DD/YYYY')
			 || '|'
			 || TO_CHAR (x1.order_last_update_date, 'MM/DD/YYYY')
			 || '|'
			 || TO_CHAR (x1.order_price_date, 'MM/DD/YYYY')
			 || '|'
			 || x1.invoice_type
			 || '|'
			 -- 10/19/2012 CG: Changed to be able to roll up at account level with PB
			 -- || x1.bill_to_cust_acct_id
			 || NVL (x1.cust_primary_bt_site, x1.bill_to_cust_acct_id)
			 || '|'
			 -- 10/19/2012 CG: Replaced with Trx Bill To
			 -- || x1.sold_to_cust_acct_id
			 || x1.bill_to_cust_acct_id
			 || '|'
			 || x1.ship_to_cust_acct_id
			 || '|'
             -- 07/23/2013 TMS 20130723-01067: Changed to trim and remove special chars
			 || substr(REGEXP_REPLACE (x1.ship_to_addr_name, '[^[:alnum:]|[:blank:]|[:punct:]]+', ''), 1, 100)
			 || '|'
			 || substr(REGEXP_REPLACE (x1.ship_to_address1, '[^[:alnum:]|[:blank:]|[:punct:]]+', '') , 1, 100)
			 || '|'
			 || substr(REGEXP_REPLACE (x1.ship_to_address2, '[^[:alnum:]|[:blank:]|[:punct:]]+', '') , 1, 100)
			 || '|'
			 || substr(REGEXP_REPLACE (x1.ship_to_address3, '[^[:alnum:]|[:blank:]|[:punct:]]+', '') , 1, 100)
			 || '|'
			 || substr(REGEXP_REPLACE (x1.ship_to_city, '[^[:alnum:]|[:blank:]|[:punct:]]+', '') , 1, 100)
             -- 07/23/2013 TMS 20130723-01067: Changed to trim and remove special chars
			 || '|'
			 || x1.ship_to_state
			 || '|'
			 || x1.ship_to_zip
			 || '|'
			 || x1.ship_to_country
			 || '|'
			 || x1.sales_branch
			 || '|'
			 || x1.price_branch
			 || '|'
			 || x1.inside_salesrep_id
			 || '|'
			 || x1.outside_salesrep_id
			 || '|'
			 || x1.order_white_salesrep_id
			 || '|'
			 || x1.ordered_by_name
			 || '|'
			 || x1.order_method
			 || '|'
			 || x1.project_job_id
			 || '|'
			 || x1.backorder_flag
			 || '|'
			 || x1.total_inb_freight_billed_amt
			 || '|'
			 || x1.total_inb_freight_expensed_amt
			 || '|'
			 || x1.total_out_freight_billed_amt
			 || '|'
			 || x1.total_out_freight_expensed_amt
			 || '|'
			 || x1.total_tax_amt
			 || '|'
			 || x1.total_invoice_amt
			 || '|'
			 || x1.invoice_discount_amount
			 || '|'
			 || x1.pmt_term_description
			 || '|'
			 || x1.pmt_disc_percent
			 || '|'
			 || x1.invoice_source_code
			 || '|'
			 || x1.sales_channel
			 || '|'
			 || x1.order_type
			 || '|'
			 || x1.quote_number
			 || '|'
			 || x1.direct_invoice_flag
			 || '|'
			 || x1.direct_vendor_id
			 || '|'
			 || x1.po_id
			 || '|'
			 || x1.customer_class_desc
			 || '|'
			 || x1.miscellaneous_charge                   
			 || '|'                              --Added position from 52 to 70 by Maha on 4/30/2014 for TMS#20140318-00095 
			 || ''
 			 || '|'
			 || ''
                         || '|'
                         || ''
                         || '|'
			 || ''
 			 || '|'
			 || ''
                         || '|'
                         || ''
                         || '|'
 			 || ''
 			 || '|'
			 || ''
                         || '|'
                         || ''
                         || '|'
			 || ''
 			 || '|'
			 || ''
                         || '|'
                         || ''																														
                         || '|'
 			 || ''
 			 || '|'
			 || ''
                         || '|'
                         || ''
                         || '|'
			 || ''
 			 || '|'
			 || ''
                         || '|'
                         || ''
                         || '|' 			
                         || x1.order_source_reference)     
			   invoice_header_data
	 FROM	xxwc_ar_invoice_header_vw x1;


