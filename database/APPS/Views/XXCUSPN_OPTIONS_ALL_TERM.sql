
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUSPN_OPTIONS_ALL_TERM" ("OPTION_ID", "LAST_UPDATE_DATE", "LAST_UPDATED_BY", "CREATION_DATE", "CREATED_BY", "LAST_UPDATE_LOGIN", "LEASE_ID", "LEASE_CHANGE_ID", "OPTION_NUM", "OPTION_TYPE_CODE", "START_DATE", "EXPIRATION_DATE", "OPTION_SIZE", "UOM_CODE", "OPTION_STATUS_LOOKUP_CODE", "ATTRIBUTE_CATEGORY", "ATTRIBUTE1", "ATTRIBUTE2", "ATTRIBUTE3", "ATTRIBUTE4", "ATTRIBUTE5", "ATTRIBUTE6", "ATTRIBUTE7", "ATTRIBUTE8", "ATTRIBUTE9", "ATTRIBUTE10", "ATTRIBUTE11", "ATTRIBUTE12", "ATTRIBUTE13", "ATTRIBUTE14", "ATTRIBUTE15", "ORG_ID", "OPTION_EXER_START_DATE", "OPTION_EXER_END_DATE", "OPTION_ACTION_DATE", "OPTION_COST", "OPTION_AREA_CHANGE", "OPTION_REFERENCE", "OPTION_NOTICE_REQD", "OPTION_COMMENTS") AS 
  (SELECT poa."OPTION_ID",poa."LAST_UPDATE_DATE",poa."LAST_UPDATED_BY",poa."CREATION_DATE",poa."CREATED_BY",poa."LAST_UPDATE_LOGIN",poa."LEASE_ID",poa."LEASE_CHANGE_ID",poa."OPTION_NUM",poa."OPTION_TYPE_CODE",poa."START_DATE",poa."EXPIRATION_DATE",poa."OPTION_SIZE",poa."UOM_CODE",poa."OPTION_STATUS_LOOKUP_CODE",poa."ATTRIBUTE_CATEGORY",poa."ATTRIBUTE1",poa."ATTRIBUTE2",poa."ATTRIBUTE3",poa."ATTRIBUTE4",poa."ATTRIBUTE5",poa."ATTRIBUTE6",poa."ATTRIBUTE7",poa."ATTRIBUTE8",poa."ATTRIBUTE9",poa."ATTRIBUTE10",poa."ATTRIBUTE11",poa."ATTRIBUTE12",poa."ATTRIBUTE13",poa."ATTRIBUTE14",poa."ATTRIBUTE15",poa."ORG_ID",poa."OPTION_EXER_START_DATE",poa."OPTION_EXER_END_DATE",poa."OPTION_ACTION_DATE",poa."OPTION_COST",poa."OPTION_AREA_CHANGE",poa."OPTION_REFERENCE",poa."OPTION_NOTICE_REQD",poa."OPTION_COMMENTS"
  FROM pn.pn_options_all poa
      ,(SELECT lease_id
              ,MIN(expiration_date) expiration_date
              ,MIN(start_date) start_date
              ,MIN(option_num) option_num
          FROM pn.pn_options_all
         WHERE option_type_code = 'TERM'
           AND expiration_date > sysdate
           AND option_status_lookup_code = 'OPEN'
         GROUP BY lease_id) poa2
 WHERE poa.lease_id = poa2.lease_id
   AND poa.expiration_date = poa2.expiration_date
   AND poa.start_date = poa2.start_date
   AND poa.option_num = poa2.option_num
   AND poa.option_type_code = 'TERM'
   AND poa.option_status_lookup_code = 'OPEN')
;
