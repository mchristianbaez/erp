
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUS_CALYEAR_CLAIM_AGREEMENTS" ("AGREEMENT_NAME", "AGREEMENT_DESC", "AGREEMENT_CODE", "AGREEMENT_PAY_FREQUENCY", "AGREEMENT_ID", "CUST_ACCOUNT_ID", "CALENDAR_YEAR", "REBATE_TYPE") AS 
  SELECT UNIQUE
             qph.description
          || ' ['
          || qph.name
          || ', Offer Status: '
          || offers.status_code
          || ' ]'
             agreement_name,
          qph.description agreement_desc,
          qph.name agreement_code,
          qph.attribute6 agreement_pay_frequency,
          qph.list_header_id agreement_id,
          trx.bill_to_customer_id cust_account_id,
          qph.attribute7 calendar_year,
          CASE
             WHEN UPPER (med.description) IN ('COOP', 'COOP_MIN') THEN 'COOP'
             ELSE 'REBATE'
          END
             rebate_type
     FROM ra_customer_trx trx,
          ozf_claims_all clh,
          ozf_claim_lines_all cld,
          qp_list_headers_all qph,
          ozf_offers offers,
          ams_media_vl med
    WHERE     1 = 1
          AND trx.interface_header_context = 'CLAIM'
          AND clh.claim_id = TO_NUMBER (trx.interface_header_attribute2)
          AND cld.claim_id = clh.claim_id
          AND cld.claim_line_id = TO_NUMBER (trx.interface_header_attribute3)
          AND qph.list_header_id = cld.activity_id
          AND offers.qp_list_header_id = qph.list_header_id
          AND med.media_id = offers.activity_media_id
          AND med.media_type_code = 'DEAL';
