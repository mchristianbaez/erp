
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUS_OZF_TERRITORIES_V" ("DESCRIPTION", "TERRITORY_CODE") AS 
  select territory_code||' ['||description||']' description, territory_code 
/*
Note: ESMS ticket 229451, Date: 14-NOV-2013
*/
from   fnd_territories_tl
where  1 =1
  and  territory_code IN ('CA', 'US')
  and  source_lang ='US';
