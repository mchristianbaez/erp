CREATE OR REPLACE VIEW XXWC_GETPAID_ARMAST_TEST_V AS
SELECT rcta.org_id ORG_ID , hca.account_number , rcta.trx_number, apsa.AMOUNT_DUE_REMAINING
  FROM apps.ra_customer_trx                 rcta
     , apps.hz_cust_accounts                hca
     , apps.hz_cust_accounts                hca_s
     , apps.hz_cust_acct_sites              hcas_s
     , apps.hz_cust_site_uses               hcsu_s
     , hz_party_sites                      hps_s
--     , apps.hz_cust_accounts                hca_b
--     , apps.hz_cust_acct_sites              hcas_b
--     , apps.hz_cust_site_uses               hcsu_b
--     , hz_party_sites                      hps_b
--     , hz_locations                        hl_b
     , ra_terms                            rt
     , apps.ar_payment_schedules            apsa
     , apps.ra_cust_trx_types               rctt
     , fnd_lookup_values_vl                flv
     , hz_customer_profiles                hcp
     , hz_customer_profiles                hcp_s
     -- , ra_batch_sources_all         rbsa
     , xxwc.xxwc_armast_getpaid_dump_tbl gp_dmp
     , xxwc.xxwc_arexti_getpaid_dump_tbl gp_exti_dmp
 WHERE 1 = 1
   AND hca.cust_account_id = rcta.bill_to_customer_id
   -- AND rcta.batch_source_id        = rbsa.batch_source_id
   -- AND rcta.org_id                 = rbsa.org_id
   AND xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id )   = 'CONVERSION'
   AND hca_S.cust_account_id = rcta.ship_to_customer_id
   AND hcas_s.cust_acct_site_id = hcsu_s.cust_acct_site_id
   AND hcas_s.party_site_id =  hps_s.party_site_id
   AND rcta.ship_to_customer_id IS NOT NULL
   AND rcta.ship_to_site_use_id = hcsu_s.site_use_id
--   AND rcta.bill_to_site_use_id = hcsu_b.site_use_id
--   AND hcas_b.cust_acct_site_id = hcsu_b.cust_acct_site_id
--   AND hcas_b.party_site_id =  hps_b.party_site_id
--   AND hca_b.cust_account_id = rcta.bill_to_customer_id
--   AND hps_b.location_id = hl_b.location_id
   AND hca.cust_account_id = hcp.cust_account_id
   AND hcp.site_use_id IS  NULL
   AND rcta.ship_to_site_use_id = hcp_s.site_use_id   (+)
   AND rcta.cust_trx_type_id = rctt.cust_trx_type_id
   AND apsa.customer_trx_id = rcta.customer_trx_id
   AND rcta.term_id = rt.term_id (+)
   AND rcta.org_id = rctt.org_id
   AND flv.lookup_type = 'XXWC_GETPAID_REMIT_TO_CODES'
   AND NVL(hcp.attribute2, '2') = flv.lookup_code
   AND apsa.status = 'OP' AND apsa.amount_due_remaining != 0 AND apsa.org_id = rcta.org_id
   AND rcta.trx_number = gp_dmp.invno
   AND hca.account_number = gp_dmp.custno
   AND rcta.trx_number = gp_exti_dmp.invno
   AND hca.account_number = gp_exti_dmp.custno
UNION
------------------------------------------------------------------------------------------------------------------------------
-- NON-CONVERSION Invoices
-- rcta.ship_to_site_use_id IS NOT NULL
------------------------------------------------------------------------------------------------------------------------------
SELECT rcta.org_id ORG_ID , hca.account_number , rcta.trx_number, apsa.AMOUNT_DUE_REMAINING
  FROM apps.ra_customer_trx          rcta
     , apps.hz_cust_accounts         hca
     , apps.hz_cust_accounts         hca_s
     , apps.hz_cust_acct_sites       hcas_s
     , apps.hz_cust_site_uses        hcsu_s
     , hz_party_sites               hps_s
--     , apps.hz_cust_accounts         hca_b
--     , apps.hz_cust_acct_sites       hcas_b
--     , apps.hz_cust_site_uses        hcsu_b
--     , hz_party_sites               hps_b
--     , hz_locations                 hl_b
     , ra_terms                     rt
     , apps.ar_payment_schedules     apsa
     , apps.ra_cust_trx_types        rctt
     -- , ra_batch_sources_all         rbsa
     , fnd_lookup_values_vl         flv
     , hz_customer_profiles         hcp
     , hz_customer_profiles         hcp_s
 WHERE 1 = 1
   -- AND rcta.batch_source_id                      = rbsa.batch_source_id
   -- AND rcta.org_id                               = rbsa.org_id
   AND xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id )   <> 'CONVERSION'
   AND hca.cust_account_id                       = rcta.bill_to_customer_id
   AND rcta.ship_to_customer_id                  IS NOT NULL
   AND rcta.ship_to_site_use_id                  = hcsu_s.site_use_id
   AND hca_S.cust_account_id                     = rcta.ship_to_customer_id
   AND hcas_s.cust_acct_site_id                  = hcsu_s.cust_acct_site_id
   AND hcas_s.party_site_id                      = hps_s.party_site_id
   AND rcta.ship_to_site_use_id                  = hcsu_s.site_use_id
--   AND rcta.bill_to_site_use_id                  = hcsu_b.site_use_id
--   AND hcas_b.cust_acct_site_id                  = hcsu_b.cust_acct_site_id
--   AND hcas_b.party_site_id                      = hps_b.party_site_id
--   AND hca_b.cust_account_id                     = rcta.bill_to_customer_id
--   AND hps_b.location_id                         = hl_b.location_id
   AND hca.cust_account_id                       = hcp.cust_account_id
   AND hcp.site_use_id                          IS NULL
   AND rcta.ship_to_site_use_id                  = hcp_s.site_use_id   (+)
   AND rcta.cust_trx_type_id                     = rctt.cust_trx_type_id
   AND apsa.customer_trx_id                      = rcta.customer_trx_id
   AND rcta.term_id                              = rt.term_id (+)
   AND rcta.org_id                               = rctt.org_id
   AND flv.lookup_type                           = 'XXWC_GETPAID_REMIT_TO_CODES'
   AND NVL(hcp.attribute2, '2')                  = flv.lookup_code
   AND apsa.status                               = 'OP'
   AND apsa.amount_due_remaining                != 0
   AND apsa.org_id                               = rcta.org_id
UNION
------------------------------------------------------------------------------------------------------------------------------
-- CONVERSION Invoices
-- rcta.ship_to_address_id IS NOT NULL
------------------------------------------------------------------------------------------------------------------------------
SELECT rcta.org_id ORG_ID , hca.account_number , rcta.trx_number, apsa.AMOUNT_DUE_REMAINING
  FROM apps.ra_customer_trx rcta
     , apps.hz_cust_accounts hca
     , apps.hz_cust_accounts hca_s
     , apps.hz_cust_acct_sites hcas_s
     , apps.hz_cust_site_uses hcsu_s
     , hz_party_sites hps_s
--     , apps.hz_cust_accounts hca_b
--     , apps.hz_cust_acct_sites hcas_b
--     , apps.hz_cust_site_uses hcsu_b
--     , hz_party_sites hps_b
--     , hz_locations hl_b
     , ra_terms rt
     , apps.ar_payment_schedules apsa
     , apps.ra_cust_trx_types rctt
     , fnd_lookup_values_vl flv
     , hz_customer_profiles hcp
     , hz_customer_profiles hcp_s
     -- , ra_batch_sources_all         rbsa
     , xxwc.xxwc_armast_getpaid_dump_tbl gp_dmp
     , xxwc.xxwc_arexti_getpaid_dump_tbl gp_exti_dmp
 WHERE 1 = 1
   AND hca.cust_account_id = rcta.bill_to_customer_id
   -- AND rcta.batch_source_id        = rbsa.batch_source_id
   -- AND rcta.org_id                 = rbsa.org_id
   AND xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id )   = 'CONVERSION'
   AND rcta.ship_to_customer_id IS NOT NULL
   AND rcta.ship_to_site_use_id = hcsu_s.site_use_id
   AND hca_S.cust_account_id = rcta.ship_to_customer_id
   AND hcas_s.cust_acct_site_id = rcta.ship_to_address_id
   AND hcas_s.party_site_id =  hps_s.party_site_id
   AND hcas_s.cust_acct_site_id = hcsu_s.cust_acct_site_id
--   AND hcas_b.cust_acct_site_id = hcsu_b.cust_acct_site_id
--   AND hcas_b.cust_acct_site_id = rcta.bill_to_address_id
--   AND hcas_b.party_site_id =  hps_b.party_site_id
--   AND hca_b.cust_account_id = rcta.bill_to_customer_id
--   AND hps_b.location_id = hl_b.location_id
   AND hca.cust_account_id = hcp.cust_account_id
   AND hcp.site_use_id IS  NULL
   AND rcta.ship_to_site_use_id = hcp_s.site_use_id   (+)
   AND rcta.cust_trx_type_id = rctt.cust_trx_type_id
   AND apsa.customer_trx_id = rcta.customer_trx_id
   AND rcta.term_id = rt.term_id (+)
   AND rcta.org_id = rctt.org_id
   AND flv.lookup_type = 'XXWC_GETPAID_REMIT_TO_CODES'
   AND NVL(hcp.attribute2, '2') = flv.lookup_code
   AND rcta.trx_number = gp_dmp.invno
   AND hca.account_number = gp_dmp.custno
   AND rcta.trx_number = gp_exti_dmp.invno
   AND hca.account_number = gp_exti_dmp.custno
   AND apsa.status = 'OP' AND apsa.amount_due_remaining != 0
   AND apsa.org_id = rcta.org_id
UNION
------------------------------------------------------------------------------------------------------------------------------
-- NON-CONVERSION Invoices
-- rcta.ship_to_site_use_id IS NOT NULL
------------------------------------------------------------------------------------------------------------------------------
/*SELECT rcta.org_id ORG_ID , hca.account_number , rcta.trx_number, apsa.AMOUNT_DUE_REMAINING
  FROM apps.ra_customer_trx rcta
     , apps.hz_cust_accounts hca
     , apps.hz_cust_accounts hca_s
     , apps.hz_cust_acct_sites hcas_s
     , apps.hz_cust_site_uses hcsu_s
     , hz_party_sites hps_s
--     , apps.hz_cust_accounts hca_b
--     , apps.hz_cust_acct_sites hcas_b
--     , apps.hz_cust_site_uses hcsu_b
--     , hz_party_sites hps_b
--     , hz_locations hl_b
     , ra_terms rt
--     , RA_SALESREPS_ALL rsa
--     , jtf_rs_defresources_v jrdv
     , apps.ar_payment_schedules apsa
     , apps.ra_cust_trx_types rctt
     -- , ra_batch_sources_all rbsa
     , fnd_lookup_values_vl flv
     , hz_customer_profiles hcp
     , hz_customer_profiles hcp_s
 WHERE 1 = 1
   AND hca.cust_account_id = rcta.bill_to_customer_id
   -- AND rcta.batch_source_id        = rbsa.batch_source_id
   -- AND rcta.org_id                 = rbsa.org_id
   AND xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id )   <> 'CONVERSION'
   AND rcta.ship_to_customer_id IS NOT NULL
   AND rcta.ship_to_site_use_id = hcsu_s.site_use_id
   AND hca_S.cust_account_id = rcta.ship_to_customer_id
   AND hcas_s.cust_acct_site_id = rcta.ship_to_address_id
   AND hcas_s.party_site_id =  hps_s.party_site_id
--   AND hcas_b.cust_acct_site_id = rcta.bill_to_address_id
--   AND hcas_s.cust_acct_site_id = hcsu_s.cust_acct_site_id
--   AND hcas_b.cust_acct_site_id = hcsu_b.cust_acct_site_id
--   AND hcas_b.party_site_id =  hps_b.party_site_id
--   AND hca_b.cust_account_id = rcta.bill_to_customer_id
--   AND hps_b.location_id = hl_b.location_id
   AND hca.cust_account_id = hcp.cust_account_id
   AND hcp.site_use_id IS  NULL
   AND rcta.ship_to_site_use_id = hcp_s.site_use_id   (+)
   AND rcta.cust_trx_type_id = rctt.cust_trx_type_id
   AND apsa.customer_trx_id = rcta.customer_trx_id
   AND rcta.term_id = rt.term_id (+)
   AND rcta.org_id = rctt.org_id
   AND flv.lookup_type = 'XXWC_GETPAID_REMIT_TO_CODES'
   AND NVL(hcp.attribute2, '2') = flv.lookup_code
   AND hcas_s.org_id = rcta.org_id
   AND hcsu_s.org_id = rcta.org_id
--   AND hcas_b.org_id = rcta.org_id
--   AND hcsu_b.org_id = rcta.org_id
   AND apsa.status = 'OP' AND apsa.amount_due_remaining != 0 AND apsa.org_id = rcta.org_id
UNION
*/
------------------------------------------------------------------------------------------------------------------------------
-- NON-CONVERSION Invoices
-- rcta.ship_to_customer_id IS NULL
------------------------------------------------------------------------------------------------------------------------------
SELECT rcta.org_id ORG_ID , hca.account_number , rcta.trx_number, apsa.AMOUNT_DUE_REMAINING
  FROM apps.ra_customer_trx rcta
     , apps.hz_cust_accounts hca
     , apps.hz_cust_accounts hca_s
     , apps.hz_cust_acct_sites hcas_s
     , apps.hz_cust_site_uses hcsu_s
     , hz_party_sites hps_s
--     , apps.hz_cust_accounts hca_b
--     , apps.hz_cust_acct_sites hcas_b
--     , apps.hz_cust_site_uses hcsu_b
--     , hz_party_sites hps_b
--     , hz_locations hl_b
     , ra_terms rt
--     , RA_SALESREPS_ALL rsa
--     , jtf_rs_defresources_v jrdv
     , apps.ar_payment_schedules apsa
     , apps.ra_cust_trx_types rctt
     -- , ra_batch_sources_all rbsa
     , fnd_lookup_values_vl flv
     , hz_customer_profiles hcp
     , hz_customer_profiles hcp_s
 WHERE 1 = 1
   AND hca.cust_account_id = rcta.bill_to_customer_id
   -- AND rcta.batch_source_id        = rbsa.batch_source_id
   -- AND rcta.org_id                 = rbsa.org_id
   AND xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id )   <> 'CONVERSION'
   AND rcta.ship_to_customer_id IS NULL
   AND hca_s.cust_account_id = rcta.bill_to_customer_id
   AND hca_s.cust_account_id = hcas_s.cust_account_id
   AND hcas_s.cust_acct_site_id = hcsu_s.cust_acct_site_id
   AND NVL(hcas_s.ship_to_flag, 'N') = 'P'
   AND hcsu_s.site_use_code = 'SHIP_TO'
   AND hcsu_s.primary_flag = 'Y'
   AND hcas_s.party_site_id =  hps_s.party_site_id
--   AND rcta.bill_to_site_use_id = hcsu_b.site_use_id
--   AND hcas_b.cust_acct_site_id = hcsu_b.cust_acct_site_id
--   AND hcas_b.party_site_id =  hps_b.party_site_id
--   AND hca_b.cust_account_id = rcta.bill_to_customer_id
--   AND hps_b.location_id = hl_b.location_id
   AND hca.cust_account_id = hcp.cust_account_id
   AND hcp.site_use_id IS  NULL
   AND rcta.ship_to_site_use_id = hcp_s.site_use_id   (+)
   AND rcta.cust_trx_type_id = rctt.cust_trx_type_id
   AND apsa.customer_trx_id = rcta.customer_trx_id
   AND rcta.term_id = rt.term_id (+)
   AND rcta.org_id = rctt.org_id
   AND flv.lookup_type = 'XXWC_GETPAID_REMIT_TO_CODES'
   AND NVL(hcp.attribute2, '2') = flv.lookup_code
   AND apsa.status = 'OP' AND apsa.amount_due_remaining != 0 AND apsa.org_id = rcta.org_id
UNION
------------------------------------------------------------------------------------------------------------------------------
-- CONVERSION Invoices
-- rcta.ship_to_customer_id IS NULL
------------------------------------------------------------------------------------------------------------------------------
SELECT rcta.org_id ORG_ID , hca.account_number , rcta.trx_number, apsa.AMOUNT_DUE_REMAINING
  FROM apps.ra_customer_trx rcta
     , apps.hz_cust_accounts hca
     , apps.hz_cust_accounts hca_s
     , apps.hz_cust_acct_sites hcas_s
     , apps.hz_cust_site_uses hcsu_s
     , hz_party_sites hps_s
--     , apps.hz_cust_accounts hca_b
--     , apps.hz_cust_acct_sites hcas_b
--     , apps.hz_cust_site_uses hcsu_b
--     , hz_party_sites hps_b
--     , hz_locations hl_b
     , ra_terms rt
     , apps.ar_payment_schedules apsa
     , apps.ra_cust_trx_types rctt
     , fnd_lookup_values_vl flv
     , hz_customer_profiles hcp
     , hz_customer_profiles hcp_s
     -- , ra_batch_sources_all         rbsa
     , xxwc.xxwc_armast_getpaid_dump_tbl gp_dmp
     , xxwc.xxwc_arexti_getpaid_dump_tbl gp_exti_dmp
 WHERE 1 = 1
   AND hca.cust_account_id = rcta.bill_to_customer_id
--   AND NVL(rcta.interface_header_context, '&*^%$#@') = 'CONVERSION'
   -- AND rcta.batch_source_id        = rbsa.batch_source_id
   -- AND rcta.org_id                 = rbsa.org_id
   AND xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id )   = 'CONVERSION'
   AND rcta.ship_to_customer_id IS NULL
   AND hca_s.cust_account_id = rcta.bill_to_customer_id
   AND hcas_s.cust_account_id = hca_s.cust_account_id
   AND hcas_s.cust_acct_site_id = hcsu_s.cust_acct_site_id
   AND NVL(hcas_s.ship_to_flag, 'N') = 'P'
   AND hcsu_s.site_use_code = 'SHIP_TO'
   AND hcas_s.party_site_id =  hps_s.party_site_id
--   AND rcta.bill_to_site_use_id = hcsu_b.site_use_id
--   AND hcas_b.cust_acct_site_id = hcsu_b.cust_acct_site_id
--   AND hcas_b.party_site_id =  hps_b.party_site_id
--   AND hca_b.cust_account_id = rcta.bill_to_customer_id
--   AND hps_b.location_id = hl_b.location_id
   AND hca.cust_account_id = hcp.cust_account_id
   AND hcp.site_use_id IS  NULL
   AND rcta.ship_to_site_use_id = hcp_s.site_use_id   (+)
   AND rcta.cust_trx_type_id = rctt.cust_trx_type_id
   AND apsa.customer_trx_id = rcta.customer_trx_id
   AND rcta.term_id = rt.term_id (+)
   AND rcta.org_id = rctt.org_id
   AND flv.lookup_type = 'XXWC_GETPAID_REMIT_TO_CODES'
   AND NVL(hcp.attribute2, '2') = flv.lookup_code
   AND apsa.status = 'OP' AND apsa.amount_due_remaining != 0 AND apsa.org_id = rcta.org_id
   AND rcta.trx_number = gp_dmp.invno
   AND hca.account_number = gp_dmp.custno
   AND rcta.trx_number = gp_exti_dmp.invno
   AND hca.account_number = gp_exti_dmp.custno
UNION
------------------------------------------------------------------------------------------------------------------------------
-- CONVERSION Invoices
-- Customer does not have Ship-To Address
------------------------------------------------------------------------------------------------------------------------------
SELECT rcta.org_id ORG_ID , hca.account_number , rcta.trx_number, apsa.AMOUNT_DUE_REMAINING
  FROM apps.ra_customer_trx rcta
     , apps.hz_cust_accounts hca
--     , apps.hz_cust_accounts hca_b
--     , apps.hz_cust_acct_sites hcas_b
--     , apps.hz_cust_site_uses hcsu_b
--     , hz_party_sites hps_b
--     , hz_locations hl_b
     , ra_terms rt
     , apps.ar_payment_schedules apsa
     , apps.ra_cust_trx_types rctt
     , fnd_lookup_values_vl flv
     , hz_customer_profiles hcp
     -- , ra_batch_sources_all         rbsa
     , xxwc.xxwc_armast_getpaid_dump_tbl gp_dmp
     , xxwc.xxwc_arexti_getpaid_dump_tbl gp_exti_dmp
 WHERE 1 = 1
   AND hca.cust_account_id = rcta.bill_to_customer_id
--   AND NVL(rcta.interface_header_context, '&*^%$#@') = 'CONVERSION'
--   AND rcta.batch_source_id             = rbsa.batch_source_id
--   AND rcta.org_id                      = rbsa.org_id
   AND xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id )        = 'CONVERSION'
   AND rcta.ship_to_customer_id IS NULL
   AND NOT EXISTS (SELECT '1'
                     FROM apps.hz_cust_acct_sites hcas_s
                        , apps.hz_cust_site_uses  hcsu_s
                    WHERE 1 = 1
                      AND hcas_s.cust_account_id   = rcta.bill_to_customer_id
                      AND hcas_s.cust_acct_site_id = hcsu_s.cust_acct_site_id
                      AND hcas_s.org_id            = rcta.org_id
                      AND hcsu_s.site_use_code     = 'SHIP_TO')
--   AND rcta.bill_to_site_use_id = hcsu_b.site_use_id
--   AND hcas_b.cust_acct_site_id = hcsu_b.cust_acct_site_id
--   AND hcas_b.party_site_id =  hps_b.party_site_id
--   AND hca_b.cust_account_id = rcta.bill_to_customer_id
--   AND hps_b.location_id = hl_b.location_id
   AND hca.cust_account_id = hcp.cust_account_id
   AND hcp.site_use_id IS  NULL
   AND rcta.cust_trx_type_id = rctt.cust_trx_type_id
   AND apsa.customer_trx_id = rcta.customer_trx_id
   AND rcta.term_id = rt.term_id (+)
   AND rcta.org_id = rctt.org_id
   AND flv.lookup_type = 'XXWC_GETPAID_REMIT_TO_CODES'
   AND NVL(hcp.attribute2, '2') = flv.lookup_code
   AND apsa.status = 'OP' AND apsa.amount_due_remaining != 0 AND apsa.org_id = rcta.org_id
   AND rcta.trx_number = gp_dmp.invno
   AND hca.account_number = gp_dmp.custno
   AND rcta.trx_number = gp_exti_dmp.invno
   AND hca.account_number = gp_exti_dmp.custno
UNION
------------------------------------------------------------------------------------------------------------------------------
-- CONVERSION Cash
------------------------------------------------------------------------------------------------------------------------------
SELECT acra.ORG_ID , hca.account_number , acra.receipt_number trx_number, apsa.AMOUNT_DUE_REMAINING
  FROM apps.ar_cash_receipts     acra
     , apps.ar_payment_schedules apsa
     , ar_receipt_methods       arm
     , ar_receipt_classes       arc
     , xxwc.xxwc_armast_getpaid_dump_tbl dmp
     , apps.hz_cust_accounts     hca
     , hz_customer_profiles     hcp
 WHERE 1 = 1
   AND acra.type                    = 'CASH'
   AND acra.cash_receipt_id         = apsa.cash_receipt_id
   AND apsa.amount_due_remaining   <> 0
   AND acra.receipt_method_id       = arm.receipt_method_id
   AND arm.receipt_class_id         = arc.receipt_class_id
   AND arc.name                     = 'CONV - Receipt'
   AND TO_CHAR(acra.receipt_number) = dmp.INVNO
   AND hca.cust_account_id          = acra.PAY_FROM_CUSTOMER
   AND hca.cust_account_id          = hcp.cust_account_id
   AND hcp.site_use_id             IS  NULL
   AND hca.account_number           = dmp.custno
UNION
------------------------------------------------------------------------------------------------------------------------------
-- CONVERSION Cash
-- hca.attribute16 has PRISM Customer#
------------------------------------------------------------------------------------------------------------------------------
SELECT acra.ORG_ID  , hca.account_number , acra.receipt_number trx_number, apsa.AMOUNT_DUE_REMAINING
  FROM apps.ar_cash_receipts     acra
     , apps.ar_payment_schedules apsa
     , ar_receipt_methods       arm
     , ar_receipt_classes       arc
     , xxwc.xxwc_armast_getpaid_dump_tbl dmp
     , apps.hz_cust_accounts     hca
     , hz_customer_profiles     hcp
 WHERE 1 = 1
   AND acra.type                    = 'CASH'
   AND acra.cash_receipt_id         = apsa.cash_receipt_id
   AND apsa.amount_due_remaining   <> 0
   AND acra.receipt_method_id       = arm.receipt_method_id
   AND arm.receipt_class_id         = arc.receipt_class_id
   AND arc.name                     = 'CONV - Receipt'
   AND TO_CHAR(acra.receipt_number) = dmp.INVNO
   AND hca.cust_account_id          = acra.PAY_FROM_CUSTOMER
   AND hca.cust_account_id          = hcp.cust_account_id
   AND hcp.site_use_id             IS  NULL
   AND hca.account_number           != dmp.custno
   AND hca.attribute6                = dmp.custno
UNION
------------------------------------------------------------------------------------------------------------------------------
-- NON-CONVERSION Cash
-- ReceiptMethod != 'WC PRISM Lockbox'
------------------------------------------------------------------------------------------------------------------------------
SELECT acra.ORG_ID , hca.account_number , acra.receipt_number trx_number, apsa.AMOUNT_DUE_REMAINING
  FROM apps.ar_cash_receipts     acra
     , apps.hz_cust_accounts     hca
     , apps.ar_payment_schedules apsa
     , ar_receipt_methods       arm
     , ar_receipt_classes       arc
     , apps.hr_locations         hla
     , hr_all_organization_units hou
     , hz_customer_profiles     hcp
 WHERE 1 = 1
   AND acra.type                    = 'CASH'
   AND acra.cash_receipt_id         = apsa.cash_receipt_id
   AND hca.cust_account_id          = apsa.customer_id
   AND hca.cust_account_id          = hcp.cust_account_id
   AND hcp.site_use_id             IS  NULL
   AND apsa.amount_due_remaining   <> 0
   AND acra.receipt_method_id       = arm.receipt_method_id
   AND arm.receipt_class_id         = arc.receipt_class_id
   AND arc.name                     != 'CONV - Receipt'
   AND arm.name                     != 'WC PRISM Lockbox'
   AND hla.inventory_organization_id IS NOT NULL
   AND hla.location_id               = hou.location_id
   AND SYSDATE BETWEEN hou.date_from AND NVL(hou.date_to,SYSDATE + 1)
   AND SUBSTR(hla.location_code, 1, INSTR(hla.location_code, '-') - 2) = NVL(arm.attribute2,fnd_profile.VALUE ('XXWC_CORPORATE_ORGANIZATION'))  -- 'WCC' commented and added the profile by pattabhi for TMS# 20141001-00057
UNION
------------------------------------------------------------------------------------------------------------------------------
-- NON-CONVERSION Cash
-- ReceiptMethod = 'WC PRISM Lockbox'
------------------------------------------------------------------------------------------------------------------------------
   SELECT acra.ORG_ID , hca.account_number , acra.receipt_number trx_number, apsa.AMOUNT_DUE_REMAINING
     FROM apps.ar_cash_receipts     acra
        , apps.hz_cust_accounts     hca
        , apps.ar_payment_schedules apsa
        , ar_receipt_methods       arm
        , ar_receipt_classes       arc
        , apps.hr_locations         hla
        , hr_all_organization_units hou
        , hz_customer_profiles     hcp
    WHERE 1 = 1
      AND acra.type                    = 'CASH'
      AND acra.cash_receipt_id         = apsa.cash_receipt_id
      AND hca.cust_account_id          = apsa.customer_id
      AND hca.cust_account_id          = hcp.cust_account_id
      AND hcp.site_use_id             IS  NULL
      AND apsa.amount_due_remaining   <> 0
      AND acra.receipt_method_id       = arm.receipt_method_id
      AND arm.receipt_class_id         = arc.receipt_class_id
      AND arc.name                     != 'CONV - Receipt'
      AND arm.name                     = 'WC PRISM Lockbox'
      AND hla.inventory_organization_id IS NOT NULL
      AND hla.location_id              = hou.location_id
      AND SYSDATE BETWEEN hou.date_from AND NVL(hou.date_to,SYSDATE + 1)
      AND SUBSTR(hla.location_code, 1, INSTR(hla.location_code, '-') - 2) = NVL(arm.attribute2,fnd_profile.VALUE ('XXWC_CORPORATE_ORGANIZATION'))  -- 'WCC' commented and added the profile by pattabhi for TMS# 20141001-00057
