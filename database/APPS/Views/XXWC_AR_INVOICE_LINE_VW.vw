CREATE OR REPLACE FORCE VIEW APPS.XXWC_AR_INVOICE_LINE_VW
(
/******************************************************************************
   NAME:       APPS.XXWC_AR_INVOICE_LINE_VW
   PURPOSE:  This view is used to extract all the Receivable invoice line details

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        04/04/2015   Raghavendra S   TMS# 20150303-00075 - FIN - Addition of Code Snippet for Line Breaks in All EDW Extract Programs
                                            and Views for all free entry text fields.
******************************************************************************/
   BUSINESS_UNIT,
   SOURCE_SYSTEM,
   OPERATING_UNIT_ID,
   OPERATING_UNIT_NAME,
   INVOICE_UNIQUE_ID,
   INVOICE_POST_DATE,
   INVOICE_LINE_NUM,
   INVOICE_LINE_TYPE,
   LINE_PRICE_DATE,
   LINE_SHIP_DATE,
   SHIP_FROM_LOCATION,
   SHIP_METHOD,
   SKU_IDENTIFIER,
   SKU_SHORT_DESCRIPTION,
   STOCK_STATUS,
   CUST_PO_NUMBER,
   CUST_PRODUCT_NUMBER,
   UNIT_OF_MEASURE,
   INVOICE_QUANTITY,
   SALES_UNIT_PRICE,
   QTY_PER_SALES_UNIT,
   PRODUCT_UNIT_COST,
   LINE_TAX_AMOUNT,
   ORDER_QUANTITY,
   VENDOR_PART_NUMBER,
   INV_LINE_DISC_AMOUNT,
   PRICING_LEVEL,
   LIST_PRICE,
   QTY_BREAK_FLAG,
   LINE_SOURCE_CODE,
   OVERRIDDEN_SELL_PRICE,
   OVERRIDEN_PRODUCT_COST,
   BRANCH_COST,
   OVERRIDDEN_BRANCH_COST,
   PO_REPLACEMENT_COST,
   PRICE_BASIS,
   SELL_GROUP,
   PRICE_FORMULA,
   PRICE_CONSTANT,
   PRICE_TYPE,
   REFERENCE_PRICE,
   VENDOR_ID,
   INTERFACE_LINE_CONTEXT,
   INV_LINE_CREATION_DATE,
   LINE_TYPE_ID,
   SOURCE_TYPE,
   SHIPPING_METHOD,
   ORDER_TYPE
)
AS
   SELECT 'WHITECAP' business_unit,
          'Oracle EBS' source_system,
          hou.organization_id operating_unit_id,
          hou.name operating_unit_name,
          rctla.customer_trx_id invoice_unique_id,
          rctla.invoice_post_date, -- 08/01/2012 CG: Changed per email from Rich, based on business use of the dates
                                 -- rctla_gd.gl_date invoice_post_date,
                                 -- rctla.creation_date invoice_post_date,
          REGEXP_REPLACE(rctla.line_number,  '[[:cntrl:]]', ' ') invoice_line_num, -- Added for V 1.0 
          REGEXP_REPLACE((CASE
              WHEN     rctla.line_type = 'LINE'
                   AND NVL (msib.inventory_item_flag, 'N') = 'Y'
              THEN
                 'M'
              WHEN     rctla.line_type = 'LINE'
                   AND NVL (msib.inventory_item_flag, 'N') = 'N'
              THEN
                 'O'
              WHEN rctla.line_type = 'FREIGHT'
              THEN
                 'F'
              WHEN rctla.line_type = 'TAX'
              THEN
                 'T'
              ELSE
                 'M'
           END),  '[[:cntrl:]]', ' ') invoice_line_type, -- Added for V 1.0 
          TRUNC (NVL (oola.pricing_date, rctla.sales_order_date))
             line_price_date,
          TRUNC (NVL (oola.actual_shipment_date, rctla.trx_date))
             line_ship_date, -- 10/07/2013 CG: TMS 20130912-00744: Updated to consider manual invoices and rebills
                           -- , mp.organization_code ship_from_location
          
          REGEXP_REPLACE((NVL (
              mp.organization_code,
              (SELECT organization_code
                 FROM apps.mtl_parameters
                WHERE     TO_CHAR (organization_id) =
                             rctla.interface_line_attribute10
                      AND ROWNUM = 1))),  '[[:cntrl:]]', ' ') ship_from_location -- Added for V 1.0 
          , REGEXP_REPLACE(oola.freight_carrier_code,  '[[:cntrl:]]', ' ') ship_method -- 06/26/2012 CG: Adjusted to mark the tax and freight lines -- Added for V 1.0 
                                               -- 02/18/2013 CG: TMS 20130214-01693: Adding to remove line feed special chars
          , REGEXP_REPLACE (
             REPLACE (
                REPLACE (
                   (CASE
                       WHEN     rctla.line_type = 'FREIGHT'
                            AND NVL (msib.segment1, rctla.description)
                                   IS NULL
                       THEN
                          'Freight'
                       WHEN     rctla.line_type = 'TAX'
                            AND NVL (msib.segment1, rctla.description)
                                   IS NULL
                       THEN
                          'Tax'
                       --08/25/2013 CG: TMS 20130822-01179: Added to pull true rental item instead of charge item number
                       WHEN     oola.line_id IS NOT NULL
                            AND UPPER (msib.segment1) =
                                   UPPER ('Rental Charge')
                       THEN
                          NVL (
                             NVL (
                                apps.xxwc_mv_routines_pkg.get_om_rental_item (
                                   oola.line_id,
                                   'NUMBER'),
                                msib.segment1),
                             'Unknown Item')
                       ELSE
                          -- 07/23/2013 TMS 20130723-01067: Changed to default unknown items for manual invoices
                          -- NVL (NVL (msib.segment1, rctla.description) , 'Unknown Item')
                          NVL (msib.segment1, 'Unknown Item')
                    END),
                   CHR (10),
                   ' '),
                '|',
                '/'),
             '[[:cntrl:]]')
             sku_identifier                                  -- 06/26/2012 CG:
                        -- 20141203-00170    EDW Feeds - Remove line breaks and Pipe from the SKU Description columns by Raghavendra Added Replace with '|','/'
                           -- 02/18/2013 CG: TMS 20130214-01693: Adding to remove line feed special chars
                           --08/25/2013 CG: TMS 20130822-01179: Updated to pull true rental item instead of charge item number
          ,
          REGEXP_REPLACE (
             REPLACE (
                REPLACE (
                   (CASE
                       WHEN     oola.line_id IS NOT NULL
                            AND UPPER (msib.segment1) =
                                   UPPER ('Rental Charge')
                       THEN
                          NVL (
                             apps.xxwc_mv_routines_pkg.get_om_rental_item (
                                oola.line_id,
                                'DESC'),
                             NVL (msib.description, rctla.description))
                       ELSE
                          NVL (msib.description, rctla.description)
                    END),
                   CHR (10)),
                '|',
                '/'),
             '[[:cntrl:]]')
             sku_short_description,
             -- 20141203-00170    EDW Feeds - Remove line breaks and Pipe from the SKU Description columns by Raghavendra Added Replace with '|','/'
          (CASE
              WHEN NVL (msib.inventory_item_flag, 'N') = 'Y' THEN 'S'
              ELSE 'N'
           END)
             stock_status,
          REGEXP_REPLACE(REPLACE (NVL (oola.cust_po_number, rctla.purchase_order),
                   CHR (10),
                   ' '),  '[[:cntrl:]]', ' ')
             cust_po_number, -- Added for V 1.0 
          NULL cust_product_number,
          NVL (rctla.uom_code, 'EA') unit_of_measure, -- adjusted to pull invoice_quantity correctly
          /*NVL (NVL (NVL (NVL (rctla.quantity_invoiced,
            oola.fulfilled_quantity
              ),
              rctla.quantity_credited
             ),
              rctla.quantity_ordered
            ),
           1
              ) invoice_quantity,*/
          NVL (
             NVL (NVL (rctla.quantity_invoiced, rctla.quantity_credited),
                  rctla.quantity_ordered),
             1)
             invoice_quantity,                      -- 07/11/2012 CG Commented
          -- rctla.extended_amount
          -- 08/20/2012 CG Changed to consider NULL unit selling price of CMs
          -- 11/04/2012 CG: Changed to handle 0 qty, error of 0 divisor

          /*NVL (            -- 07/30/2014 Commented by Veera for TMS#20140710-00426 --start
               rctla.unit_selling_price                         -- 11/04/2012 CG
                                       -- , (NVL (rctla.extended_amount, 0) / NVL ( NVL ( NVL (rctla.quantity_invoiced , rctla.quantity_credited) , rctla.quantity_ordered ) , 1 ) )
               ,
               (CASE
                   WHEN NVL (
                           NVL (
                              NVL (rctla.quantity_invoiced,
                                   rctla.quantity_credited),
                              rctla.quantity_ordered),
                           1) = 0
                   THEN
                      0
                   ELSE
                      (  NVL (rctla.extended_amount, 0)
                       / NVL (
                            NVL (
                               NVL (rctla.quantity_invoiced,
                                    rctla.quantity_credited),
                               rctla.quantity_ordered),
                            1))
                END))
               sales_unit_price,*/
          -- 07/30/2014 Commented by Veera for TMS#20140710-00426 --End

          rctla.unit_selling_price sales_unit_price, -- 07/30/2014 Added by Veera for TMS#20140710-00426
          1 qty_per_sales_unit,                        -- 07/22/2012 CGonzalez
          -- NVL (oola.unit_cost, 0) product_unit_cost,
          -- 08/16/2012 CGonzalez: Changed to handle Engineering Charges
          -- and vendor quote excecptions
          /*ROUND
              (NVL (xxwc_mv_routines_pkg.get_order_line_cost (oola.line_id),
              0
             ),
            2
             ) product_unit_cost,*/

          -- 07/28/2014 Commented by Veera for TMS#20140710-00407,TMS#20140710-00409 --start
          /* ROUND (
              (CASE
                  WHEN msib.segment1 = 'ENGINEERING'
                  THEN
                     TO_NUMBER (oola.attribute8)
                  ELSE
                     NVL (
                        apps.xxwc_mv_routines_pkg.get_vendor_quote_cost (
                           oola.line_id),
                        NVL (
                           apps.xxwc_mv_routines_pkg.get_order_line_cost (
                              oola.line_id),
                           0))
               END),
              2)
              product_unit_cost,*/
          -- 07/28/2014 Commented by Veera for TMS#20140710-00407,TMS#20140710-00409 --End

          -- 07/28/2014 Added below code  by Veera for TMS#20140710-00407,TMS#20140710-00409
          NVL (
             APPS.xxwc_mv_routines_add_pkg.get_sales_unit_price (
                'COST',
                rctla.customer_trx_line_id),
             NVL (
                apps.xxwc_mv_routines_pkg.get_order_line_cost (oola.line_id),
                0))
             product_unit_cost,
          --rctla_tax.extended_amount linetaxamt,
          -- 06/26/12 CG Changed to use MV Routines function
          -- Standard function uses syn that is not getting initialized
          /*arp_trx_line_util.get_tax_amount
            (rctla.customer_trx_id,
            rctla.customer_trx_line_id,
             NULL
             ) line_tax_amount,*/
          apps.xxwc_mv_routines_pkg.get_tax_amount (
             rctla.customer_trx_id,
             rctla.customer_trx_line_id)
             line_tax_amount,
          NVL (rctla.quantity_ordered, oola.ordered_quantity) order_quantity,
          NULL vendor_part_number,
          NULL inv_line_disc_amount,
          NULL pricing_level, -- 07/11/2012 CG Changed to NVL with the list price
          NVL (oola.unit_list_price, rctla.unit_selling_price) list_price,
          NULL qty_break_flag -- 05/02/13 TMS 20130506-00725: CG modified to limit field length
                             ,
          SUBSTR (
             apps.xxwc_mv_routines_add_pkg.get_last_modifier (oola.header_id,
                                                              oola.line_id),
             1,
             50)
             line_source_code,
          apps.xxwc_mv_routines_add_pkg.get_overridden_sell_price (
             oola.header_id,
             oola.line_id)
             overridden_sell_price,
          NULL overriden_product_cost, -- 06/07/2012 CG: Changed to pull item cost
          -- NVL (msib.attribute9, msib.attribute10) branch_cost,
          -- 06/08/2012 CG
          /*ROUND
              (cst_cost_api.get_item_cost
              (1.0,
            NVL (oola.inventory_item_id, rctla.inventory_item_id),
             NVL
              (oola.ship_from_org_id,
            NVL
             (rctla.warehouse_id,
             xxwc_mv_routines_pkg.get_mst_organization_id
             (rctla.org_id)
              )
             )
            ),
              2
             ) branch_cost,*/
          NVL (oola.unit_cost, 0) branch_cost,
          NULL overridden_branch_cost,
          apps.xxwc_mv_routines_pkg.get_last_po_price (
             rctla.inventory_item_id,
             NVL (
                rctla.warehouse_id,
                apps.xxwc_mv_routines_pkg.get_mst_organization_id (
                   rctla.org_id)))
             po_replacement_cost,
          apps.xxwc_mv_routines_add_pkg.get_original_modifier (
             oola.header_id,
             oola.line_id)
             price_basis,
          NULL sell_group,
          NULL price_formula,
          NULL price_constant,
          apps.xxwc_mv_routines_add_pkg.get_price_type (oola.header_id,
                                                        oola.line_id)
             price_type,
          NULL reference_price,
          (CASE
              WHEN NVL (oola.source_type_code, 'INTERNAL') = 'EXTERNAL'
              THEN
                 SUBSTR (
                    apps.xxwc_mv_routines_pkg.get_oe_ds_line_vendor (
                       oola.line_id),
                    1,
                    46)
              ELSE
                 NULL
           END)
             vendor_id,
          rctla.interface_line_context,
          rctla.creation_date inv_line_creation_date, -- 08/25/2013 CG: TMS 20130822-01179: Added to exclude rental return                                ,
          oola.line_type_id,
          REGEXP_REPLACE((SELECT MEANING --------Added by Harsha for TMS#20131118-00053 -- Modify EDW feed to better account for direct sales
             FROM apps.fnd_lookup_values
            WHERE     lookup_type = 'SOURCE_TYPE'
                  AND lookup_code = oola.SOURCE_TYPE_CODE
                  AND ENABLED_FLAG = 'Y'
                  AND TRUNC (SYSDATE) BETWEEN TRUNC (
                                                 NVL (START_DATE_ACTIVE,
                                                      SYSDATE))
                                          AND TRUNC (
                                                 NVL (END_DATE_ACTIVE,
                                                      SYSDATE))),  '[[:cntrl:]]', ' ') -- Added for V1.0
             SOURCE_TYPE,
          REGEXP_REPLACE((SELECT MEANING
             FROM apps.fnd_lookup_values
            WHERE     lookup_type = 'SHIP_METHOD'
                  AND lookup_code = oola.SHIPPING_METHOD_CODE
                  AND ENABLED_FLAG = 'Y'
                  AND TRUNC (SYSDATE) BETWEEN TRUNC (
                                                 NVL (START_DATE_ACTIVE,
                                                      SYSDATE))
                                          AND TRUNC (
                                                 NVL (END_DATE_ACTIVE,
                                                      SYSDATE))),  '[[:cntrl:]]', ' ') -- Added for V1.0
             SHIPPING_METHOD,
          REGEXP_REPLACE((SELECT description
             FROM apps.oe_order_headers_all ooh, --apps.OE_ORDER_TYPES_V oot       --Commented by Veera for TMS#20140717-00188
                  ont.oe_transaction_types_tl oot --Added by Veera for TMS#20140717-00188
            WHERE     ooh.header_id = oola.header_id
                  AND ooh.order_type_id = oot.transaction_type_id),  '[[:cntrl:]]', ' ') -- Added for V1.0
             order_type --------Added by Harsha for TMS#20131118-00053 -- Modify EDW feed to better account for direct sales
     FROM apps.hr_operating_units hou,
          xxwc.ar_invoice_line_temp rctla,
          apps.oe_order_lines_all oola,
          apps.mtl_parameters mp,
          apps.mtl_system_items_b msib
    WHERE     rctla.org_id = hou.organization_id
          -- AND rctla.line_type != 'TAX'
          AND rctla.interface_line_attribute6 = TO_CHAR (oola.line_id(+))
          AND oola.ship_from_org_id = mp.organization_id(+)
          AND rctla.inventory_item_id = msib.inventory_item_id(+)
          AND TO_CHAR (msib.organization_id(+)) =
                 NVL ( (rctla.interface_line_attribute10),
                      TO_CHAR (rctla.mst_organization_id))
-- Added 05/22/2012 CG To exclude Prism/Conversion Lines
;