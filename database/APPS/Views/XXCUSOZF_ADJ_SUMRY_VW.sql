
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUSOZF_ADJ_SUMRY_VW" ("MVID", "MV_NAME", "CAL_YEAR", "FISCAL_PERIOD", "PERIOD_ID", "REBATE_TYPE", "ELECTRICAL", "CREATIVE_TOUCH_INTERIORS", "FACILITIES_MAINTENANCE", "UTILITIES", "CROWN_BOLT", "WHITE_CAP", "WATERWORKS", "INDUSTRIAL_PVF", "PLUMBING_HVAC", "REPAIR_REMODEL", "HD_SUPPLY_CANADA") AS 
  SELECT mvid,
       mv_name,
       cal_year,
       fiscal_period,
       period_id,
       rebate_type
      ,SUM(ELE) "ELECTRICAL"
      ,SUM(CTI) "CREATIVE_TOUCH_INTERIORS"
      ,SUM(FM)  "FACILITIES_MAINTENANCE"
      ,SUM(UTL) "UTILITIES"
      ,SUM(CB)  "CROWN_BOLT"
      ,SUM(WC)  "WHITE_CAP"
      ,SUM(WW)  "WATERWORKS"
      ,SUM(IPVF) "INDUSTRIAL_PVF"
      ,SUM(PLM)  "PLUMBING_HVAC"
      ,SUM(RR)   "REPAIR_REMODEL"
      ,SUM(CAN)  "HD_SUPPLY_CANADA"
FROM
(SELECT hca.account_number mvid,
       hp.party_name mv_name,
       qlhv.attribute7 cal_year,
       DECODE(accr.rebate_type_id,'COOP','COOP','COOP_MIN','COOP','REBATE') rebate_type,
       accr.period_id period_id,
       period.name fiscal_period
      ,XXCUS_OZF_RPTUTIL_PKG.get_lob_adj(accr.period_id,accr.plan_id,'ELECTRICAL',accr.rebate_type_id,hca.cust_account_id) ELE
      ,XXCUS_OZF_RPTUTIL_PKG.get_lob_adj(accr.period_id,accr.plan_id,'CREATIVE TOUCH INTERIORS',accr.rebate_type_id,hca.cust_account_id) CTI
      ,XXCUS_OZF_RPTUTIL_PKG.get_lob_adj(accr.period_id,accr.plan_id,'FACILITIES MAINTENANCE',accr.rebate_type_id,hca.cust_account_id) FM
      ,XXCUS_OZF_RPTUTIL_PKG.get_lob_adj(accr.period_id,accr.plan_id,'UTILITIES',accr.rebate_type_id,hca.cust_account_id) UTL
      ,XXCUS_OZF_RPTUTIL_PKG.get_lob_adj(accr.period_id,accr.plan_id,'CROWN BOLT',accr.rebate_type_id,hca.cust_account_id) CB
      ,XXCUS_OZF_RPTUTIL_PKG.get_lob_adj(accr.period_id,accr.plan_id,'WHITE CAP',accr.rebate_type_id,hca.cust_account_id) WC
      ,XXCUS_OZF_RPTUTIL_PKG.get_lob_adj(accr.period_id,accr.plan_id,'WATERWORKS',accr.rebate_type_id,hca.cust_account_id) WW
      ,XXCUS_OZF_RPTUTIL_PKG.get_lob_adj(accr.period_id,accr.plan_id,'INDUSTRIAL PVF',accr.rebate_type_id,hca.cust_account_id) IPVF
      ,XXCUS_OZF_RPTUTIL_PKG.get_lob_adj(accr.period_id,accr.plan_id,'PLUMBING & HVAC',accr.rebate_type_id,hca.cust_account_id) PLM
      ,XXCUS_OZF_RPTUTIL_PKG.get_lob_adj(accr.period_id,accr.plan_id,'REPAIR & REMODEL',accr.rebate_type_id,hca.cust_account_id) RR
      ,XXCUS_OZF_RPTUTIL_PKG.get_lob_adj(accr.period_id,accr.plan_id,'HD SUPPLY CANADA',accr.rebate_type_id,hca.cust_account_id) CAN
  FROM xxcusozf_accruals_mv accr,
     hz_cust_accounts hca,
     hz_parties hp,
     ozf_time_ent_period period,
     qp_list_headers_vl qlhv
WHERE 1=1
AND accr.mvid=hca.cust_account_id
AND accr.plan_id=qlhv.list_header_id
AND hca.party_id=hp.party_id
AND accr.period_id=period.ent_period_id
AND grp_mvid=0
AND grp_period=0
AND grp_rebate_type=0
AND grp_plan_id=0
AND grp_cal_year=1
AND grp_lob=1
AND grp_qtr=1
AND grp_branch=1
AND grp_year=1
AND grp_adj_type=1)
GROUP BY mvid,
         mv_name,
         cal_year,
         fiscal_period,
         period_id,
         rebate_type
;
