--
-- XXWC_INV_CYCLECOUNT_HDR_VW  (View)
--

CREATE OR REPLACE FORCE VIEW APPS.XXWC_INV_CYCLECOUNT_HDR_VW
(
   CYCLECOUNT_HDR_ID
  ,CYCLE_COUNT_NAME
  ,NOTES
  ,PROCESS_TYPE
  ,PROCESS_DATE
  ,PROCESS_NOW_FLAG
  ,PROCESS_STATUS
  ,LAST_UPDATE_DATE
  ,LAST_UPDATED_BY
  ,CREATION_DATE
  ,CREATED_BY
  ,LAST_UPDATE_LOGIN
  ,OBJECT_VERSION_NUMBER
  ,USER_NAME
)
AS
     SELECT hdr.cyclecount_hdr_id
           ,hdr.cycle_count_name
           ,hdr.notes
           ,hdr.process_type
           ,hdr.process_date
           ,hdr.process_now_flag
           ,hdr.process_status
           ,hdr.last_update_date
           ,hdr.last_updated_by
           ,hdr.creation_date
           ,hdr.created_by
           ,hdr.last_update_login
           ,hdr.object_version_number
           ,fu.user_name
       FROM xxwc_inv_cyclecount_hdr hdr, fnd_user fu
      WHERE hdr.created_by = fu.user_id
   ORDER BY hdr.cyclecount_hdr_id DESC;


