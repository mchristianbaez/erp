
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUSOZF_REBATES" ("MVID_NUM", "MVID_NAME", "AGREEMENT_NAME", "DESCRIPTION", "AGREEMENT_YR", "AGREEMENT_STATUS", "ACTIVITY_TYPE", "AGREEMENT_START_DATE", "AGREEMENT_END_DATE", "STOCK_RECEIPT_START_DATE", "STOCK_RECEIPT_END_DATE", "REBATE_TYPE", "ORG", "OFFER_CODE", "AUTOPAY_ENABLED_FLAG", "CURRENCY_CODE", "AUTO_RENEWAL", "GLOBAL_FLAG", "UNTIL_YEAR", "CONTRACT_NAME", "PAYMENT_METHOD", "PAYMENT_FREQ", "PROGRAM_NAME", "PROGRAM_START_DATE", "PROGRAM_END_DATE", "PROGRAM_STATUS", "MASS_COPY", "COPIED_FROM_REBATE", "BACK_TO_OR_MIN_GUARANTEE_AMT", "YTD_ACCRUAL", "ACTIVE_FLAG", "FREIGHT_RATE", "CASH_DISCOUNT_RATE", "TRADE_PROFILE_ID") AS 
  SELECT DISTINCT REPLACE(hp1.party_number, '~MSTR', '') mvid_num,
      hp1.party_name 		mvid_name,
      qlhv.description 		agreement_name,
      qlhv.comments 		description,
      qlhv.attribute7 		agreement_yr,
      oo.status_code 		agreement_status,
      med.media_name 		activity_type,
      qlhv.start_date_active 	agreement_start_date,
      qlhv.end_date_active 	agreement_end_date,
      qlhv.start_date_active_first stock_receipt_start_date,
      qlhv.end_date_active_first   stock_receipt_end_date,
      oo.offer_type 		rebate_type,
      hr.name 			org,
      oo.offer_code 		offer_code,
      oo.autopay_flag 		autopay_enabled_flag,
      oo.transaction_currency_code currency_code,
      qlhv.attribute1 		   auto_renewal,
      qlhv.global_flag 		   global_flag,
      qlhv.attribute2 		   until_year,
      qlhv.ATTRIBUTE4 		   contract_name,
      qlhv.attribute5 		   payment_method,
      qlhv.attribute6 		   payment_freq,
      ofa.short_name 		   program_name,
      ofa.start_date_active 	   program_start_date,
      ofa.end_date_active 	   program_end_date,
      ofa.status_code 		   program_status,
      qlhv.attribute3 		   mass_copy,
      qlhv.attribute8 		   copied_from_rebate,
      qlhv.attribute9 		   Back_to_or_Min_Guarantee_Amt,
      NVL(ofu.amount,0) 	   YTD_Accrual,
      qlhv.active_flag 		   active_flag,
      NVL(ctp.attribute2,0) 	   freight_rate,
      NVL(ctp.attribute3,0) 	   cash_discount_rate,
      ctp.trade_profile_id 	   trade_profile_id
    FROM Ar.Hz_Cust_Accounts Hca1,
         Ar.Hz_Parties Hp1,
         apps.qp_list_headers_vl qlhv,
         HR_OPERATING_UNITS hr,
         Qp.Qp_Qualifiers Qq,
         ozf.ozf_offers oo,
         apps.Ozf_Funds_All_VL Ofa,
         Ozf.Ozf_Act_Budgets Oab,
         apps.ams_media_vl med,
         apps.ozf_cust_trd_prfls_all ctp,
        (SELECT SUM(fu.acctd_amount) amount,
                fu.plan_id
           FROM APPS.ozf_funds_utilized_all_b fu,
                APPS.qp_list_headers_b qpb
          WHERE fu.plan_type= 'OFFR'
            AND fu.plan_id= qpb.list_header_id
          --AND fu.plan_curr_amount_remaining <> 0
            AND fu.utilization_type IN ('ACCRUAL', 'ADJUSTMENT')
          GROUP BY fu.plan_id
        ) ofu
    WHERE hca1.party_id           = hp1.party_id(+)
      AND hca1.cust_account_id      = ctp.cust_account_id(+)
      AND qlhv.list_header_id      = qq.list_header_id(+)
      AND ctp.org_id(+)             = 101
      AND ofu.plan_id(+)               = qlhv.list_header_id
      AND qq.qualifier_context(+)   = 'SOLD_BY'
      AND qq.qualifier_attribute(+) = 'QUALIFIER_ATTRIBUTE2'
      AND qq.qualifier_attr_value   = TO_CHAR(hca1.cust_account_id(+))
      AND oab.budget_source_id       = ofa.fund_id
      AND oab.budget_source_type='FUND'
    --AND oab.status_code= 'APPROVED'
      AND oab.transfer_type='REQUEST'
      AND oab.act_budget_used_by_id = oo.qp_list_header_id
      AND Oo.Qp_List_Header_Id      = Qlhv.List_Header_Id
      AND oo.offer_type            IN ('ACCRUAL', 'VOLUME_OFFER')
      AND oo.org_id                 = hr.organization_id(+)
      AND med.media_id              = oo.activity_media_id
      AND Med.Media_Name NOT       IN ('Payment Terms', 'Freight')
;
