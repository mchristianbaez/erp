DROP VIEW APPS.XXWC_OM_QUOTE_CONTACT_VW;

CREATE OR REPLACE FORCE VIEW APPS.XXWC_OM_QUOTE_CONTACT_VW
/******************************************************************************
   NAME        :  APPS.XXWC_OM_QUOTE_CONTACT_VW
   PURPOSE     :  This view is created for deriving Contact Info for OM Quote Form

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        07/01/2015  Gopi Damuluri    TMS#20150505-00024 OM Quote Form - Contact Info
******************************************************************************/
(
   FULL_NAME,
   CUST_ACCOUNT_ID,
   PHONE_TYPE_MEANING,
   PHONE_NUMBER,
   CONTACT_ID
)
AS
     SELECT TRIM (party.person_first_name || ' ' || party.person_last_name)
               full_name,
            role_acct.cust_account_id,
            DECODE (hcp.contact_point_type, 'PHONE', 'Telephone')
               phone_type_meaning,
            hcp.phone_area_code || hcp.phone_number phone_number,
            acct_role.orig_system_reference contact_id
       FROM hz_cust_account_roles acct_role,
            hz_parties party,
            hz_parties rel_party,
            hz_relationships rel,
            hz_org_contacts org_cont,
            hz_cust_accounts role_acct,
            hz_contact_points hcp
      WHERE     acct_role.party_id = rel.party_id
            AND acct_role.cust_acct_site_id IS NULL
            AND NOT EXISTS
                   (SELECT '1'
                      FROM hz_role_responsibility rr
                     WHERE rr.cust_account_role_id =
                              acct_role.cust_account_role_id)
            AND acct_role.role_type = 'CONTACT'
            AND org_cont.party_relationship_id = rel.relationship_id
            AND rel.subject_id = party.party_id
            AND rel_party.party_id = rel.party_id
            AND acct_role.cust_account_id = role_acct.cust_account_id
            AND role_acct.party_id = rel.object_id
            AND rel.subject_table_name = 'HZ_PARTIES'
            AND rel.object_table_name = 'HZ_PARTIES'
            AND hcp.contact_point_type(+) = 'PHONE'
            AND hcp.status(+) = 'A'
            AND rel_party.party_id = hcp.owner_table_id(+)
            AND hcp.owner_table_name(+) = 'HZ_PARTIES'
            AND party.status = 'A'
            AND rel_party.status = 'A'
            AND rel.status = 'A'
            AND acct_role.status = 'A'
   --                and role_acct.party_id = 166278
   ORDER BY UPPER (party.person_first_name);
/