/* Formatted on 2012/08/31 09:40 (Formatter Plus v4.8.8) */
CREATE OR REPLACE FORCE VIEW apps.xxwc_ap_invoice_header_ext_vw (operating_unit_id,
                                                                 interface_date,
                                                                 invoice_header_data
                                                                )
AS
   SELECT operating_unit_id, vendor_invoice_date interface_date,
          (   business_unit
           || '|'
           || source_system
           || '|'
           || document_number
           || '|'
           || vendor_number
           || '|'
           || vendor_invoice_number
           || '|'
           || invoice_type_code
           || '|'
           || TO_CHAR (vendor_invoice_date, 'MM/DD/YYYY')
           || '|'
           || TO_CHAR (vendor_invoice_entered_date, 'MM/DD/YYYY')
           || '|'
           || TO_CHAR (vendor_invoice_last_upd_date, 'MM/DD/YYYY')
           || '|'
           || vendor_invoice_status
           || '|'
           || payment_terms_id
           || '|'
           || TO_CHAR (payment_due_date, 'MM/DD/YYYY')
           || '|'
           || TO_CHAR (payment_date, 'MM/DD/YYYY')
           || '|'
           || payment_amount
           || '|'
           || cash_discount_amount
           || '|'
           || inv_merch_amount
           || '|'
           || inv_freight_charges
           || '|'
           || inv_tax_amount
           || '|'
           || inv_misc_charges
           || '|'
           || net_invoice_total
          ) invoice_header_data
     FROM xxwc_ap_invoice_header_vw;


