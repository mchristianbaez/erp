
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUSOZF_REBATE_SUMMARY_VW" ("YTD_INCOME", "YTD_PURCHASE", "INVOICED_AMOUNT", "APPLIED_PAYMENTS", "UNAPPLIED_PAYMENTS", "TOTAL_OUTSTANDING", "COLLECTOR_NAME", "MVID", "CUST_ACCOUNT_ID", "VENDOR_NAME", "AGING_LESS_THAN_30", "AGING_BETWEEN_31_60", "AGING_BETWEEN_61_90", "AGING_BETWEEN_91_120", "AGING_GREATER_THAN_120", "AGREEMENT_YEAR") AS 
  SELECT nvl(ytd_income, 0) ytd_income
      ,nvl(ytd_purchase, 0) ytd_purchase
      ,nvl(invoiced_payments, 0) invoiced_amount
      ,nvl(applied_payments, 0) applied_payments
      ,nvl(unapplied_payments, 0) unapplied_payments
      ,(nvl(invoiced_payments, 0) -
       (nvl(am.applied_payments, 0) + nvl(um.unapplied_payments, 0))) total_outstanding
      ,collector_name
      ,a.mvid
      ,a.cust_account_id
      ,a.mvid_name vendor_name
      ,aging_less_than_30
      ,aging_between_31_60
      ,aging_between_61_90
      ,aging_between_91_120
      ,aging_greater_than_120
      ,a.agreement_year
  FROM (SELECT mvid
              ,cust_account_id
              ,SUM(ytd_income) ytd_income
              ,agreement_year
              ,mvid_name
              ,collector_name
          FROM (SELECT accr1.mvid cust_account_id
                      ,SUM(accr1.accrued_amount) ytd_income
                      ,qlhv.attribute7 agreement_year
                      ,REPLACE(hzp.party_number, '~MSTR', '') mvid
                      ,hzp.party_name mvid_name
                      ,ac.name collector_name
                      ,decode(accr1.rebate_type_id
                             ,'COOP'
                             ,'COOP'
                             ,'COOP_MIN'
                             ,'COOP'
                             ,'REBATE') rebate_type_id
                  FROM xxcusozf_accruals_mv accr1
                      ,qp_list_headers_vl   qlhv
                      ,hz_cust_accounts     hzca
                      ,hz_parties           hzp
                      ,hz_customer_profiles hcp
                      ,ar_collectors        ac
                 WHERE 1 = 1
                   AND accr1.grp_mvid = 0
                   AND accr1.grp_branch = 0
                   AND accr1.grp_adj_type = 1
                   AND accr1.grp_rebate_type = 0
                   AND accr1.grp_plan_id = 0
                   AND accr1.grp_qtr = 1
                   AND accr1.grp_year = 1
                   AND accr1.grp_lob = 1
                   AND accr1.grp_cal_year = 0
                   AND accr1.grp_period = 1
                   AND accr1.plan_id = qlhv.list_header_id
                   AND accr1.mvid = hzca.cust_account_id
                   AND hzca.party_id = hzp.party_id
                   AND hzca.cust_account_id = hcp.cust_account_id
                   AND ac.collector_id = hcp.collector_id
                 GROUP BY accr1.mvid
                         ,hzp.party_name
                         ,accr1.rebate_type_id
                         ,hzp.party_number
                         ,ac.name
                         ,qlhv.attribute7)
         GROUP BY mvid
                 ,cust_account_id
                 ,mvid_name
                 ,collector_name
                 ,agreement_year) a
      ,(SELECT SUM(pm1.total_purchases) ytd_purchase
              ,pm1.calendar_year
              ,pm1.mvid mvid_id
              ,REPLACE(hzp.party_number, '~MSTR', '') mvid
          FROM xxcusozf_purchases_mv pm1
              ,hz_parties            hzp
              ,hz_cust_accounts      hzca
         WHERE 1 = 1
           AND pm1.grp_mvid = 0
           AND pm1.grp_branch = 1
           AND pm1.grp_cal_year = 0
           AND pm1.grp_period = 1
           AND pm1.grp_qtr = 1
           AND pm1.grp_year = 1
           AND pm1.grp_lob = 1
           AND pm1.grp_lob = 1
           AND pm1.grp_cal_period = 1
           AND pm1.mvid = hzca.cust_account_id
           AND hzp.party_id = hzca.party_id
           AND hzp.attribute1 = 'HDS_MVID'
         GROUP BY pm1.mvid, pm1.calendar_year, hzp.party_number) b
      ,(SELECT SUM(amount_due_original) invoiced_payments
              ,SUM(aging_less_than_30) aging_less_than_30
              ,SUM(aging_between_31_60) aging_between_31_60
              ,SUM(aging_between_61_90) aging_between_61_90
              ,SUM(aging_between_91_120) aging_between_91_120
              ,SUM(aging_greater_than_120) aging_greater_than_120
              ,mvid
              ,agreement_year
          FROM (SELECT (CASE
                         WHEN (SYSDATE - apsa.due_date) < 30 THEN
                          apsa.amount_due_remaining
                         ELSE
                          0
                       END) aging_less_than_30
                      ,(CASE
                         WHEN (SYSDATE - apsa.due_date) BETWEEN 31 AND 60 THEN
                          apsa.amount_due_remaining
                         ELSE
                          0
                       END) aging_between_31_60
                      ,(CASE
                         WHEN (SYSDATE - apsa.due_date) BETWEEN 61 AND 90 THEN
                          apsa.amount_due_remaining
                         ELSE
                          0
                       END) aging_between_61_90
                      ,(CASE
                         WHEN (SYSDATE - apsa.due_date) BETWEEN 91 AND 120 THEN
                          apsa.amount_due_remaining
                         ELSE
                          0
                       END) aging_between_91_120
                      ,(CASE
                         WHEN (SYSDATE - apsa.due_date) > 120 THEN
                          apsa.amount_due_remaining
                         ELSE
                          0
                       END) aging_greater_than_120
                      ,apsa.amount_due_original
                      ,rcta.bill_to_customer_id cust_account_id
                      ,REPLACE(hp1.party_number, '~MSTR', '') mvid
                      ,qlhv.attribute7 agreement_year
                  FROM hz_parties                hp1
                      ,hz_cust_accounts          hca
                      ,ra_customer_trx_all       rcta
                      ,ar_payment_schedules_all  apsa
                      ,qp_list_headers_vl        qlhv
                      ,ozf_claim_lines_all       ocl
                      ,ams_media_tl              med
                      ,ozf_offers                oo
                      ,ra_customer_trx_lines_all rctl
                      ,ra_cust_trx_types_all     rctt
                 WHERE 1 = 1
                   AND hca.party_id = hp1.party_id
                   AND rctt.cust_trx_type_id = rcta.cust_trx_type_id
                   AND oo.qp_list_header_id = qlhv.list_header_id
                   AND to_number(rctl.interface_line_attribute2) =
                       ocl.claim_id
                   AND ocl.activity_id = oo.qp_list_header_id
                   AND rcta.org_id IN (101, 102)
                   AND rcta.customer_trx_id = apsa.customer_trx_id
                   AND qlhv.attribute7 > = '2010'
                   AND oo.activity_media_id = med.media_id
                   AND rcta.customer_trx_id = rctl.customer_trx_id
                   AND rcta.bill_to_customer_id = hca.cust_account_id
                   AND apsa.class = 'INV'
                   AND hp1.attribute1 = 'HDS_MVID')
         GROUP BY mvid, agreement_year) c
      ,(SELECT SUM(applied_amount) applied_payments, mvid, agreement_year
          FROM (SELECT cr.amount applied_amount
                      ,hp.party_name mvid_name
                      ,REPLACE(hp.party_number, '~MSTR', '') mvid
                      ,hca.cust_account_id cust_account_id
                      ,qlhv.attribute7 agreement_year
                      ,hp.party_name master_vendor
                      ,rct.trx_number trx_number
                  FROM ra_customer_trx_all            rct
                      ,ra_customer_trx_lines_all      rctl
                      ,ar_cash_receipts_all           cr
                      ,ar_receipt_methods             rm
                      ,hz_parties                     hp1
                      ,ar_receivable_applications_all ra
                      ,ozf_claim_lines_all            ocl
                      ,ozf_offers                     oo
                      ,qp_list_headers_vl             qlhv
                      ,ams_media_vl                   amv
                      ,hz_cust_accounts               hca
                      ,hz_parties                     hp
                 WHERE 1 = 1
                   AND rct.org_id IN (101, 102)
                   AND rct.customer_trx_id = rctl.customer_trx_id
                   AND cr.status = 'APP'
                   AND cr.receipt_method_id = rm.receipt_method_id
                   AND rm.name = 'REBATE_DEDUCTION'
                   AND to_number(cr.attribute1) = hp1.party_id
                   AND hp1.attribute1 = 'HDS_BU'
                   AND rct.customer_trx_id = ra.applied_customer_trx_id
                   AND cr.cash_receipt_id = ra.cash_receipt_id
                   AND oo.qp_list_header_id = qlhv.list_header_id
                   AND rctl.interface_line_attribute2 =
                       to_char(ocl.claim_id)
                   AND ocl.activity_id = oo.qp_list_header_id
                   AND oo.qp_list_header_id = qlhv.list_header_id
                   AND amv.media_id = oo.activity_media_id
                   AND cr.pay_from_customer = hca.cust_account_id
                   AND hca.party_id = hp.party_id
                   AND hp.attribute1 = 'HDS_MVID'
                UNION ALL
                SELECT ra.amount_applied applied_payments
                      ,hp.party_name mvid_name
                      ,REPLACE(hp.party_number, '~MSTR', '') mvid
                      ,hca.cust_account_id cust_account_id
                      ,qlhv.attribute7 agreement_year
                      ,hp.party_name master_vendor
                      ,rct.trx_number trx_number
                  FROM ra_customer_trx_all            rct
                      ,ra_customer_trx_lines_all      rctl
                      ,ar_cash_receipts_all           cr
                      ,ar_receipt_methods             rm
                      ,hz_parties                     hp1
                      ,ar_receivable_applications_all ra
                      ,ozf_claim_lines_all            ocl
                      ,ozf_offers                     oo
                      ,qp_list_headers_vl             qlhv
                      ,ams_media_vl                   amv
                      ,hz_cust_accounts               hca
                      ,hz_parties                     hp
                 WHERE 1 = 1
                   AND rct.org_id IN (101, 102)
                   AND rct.customer_trx_id = rctl.customer_trx_id
                   AND cr.status = 'APP'
                   AND cr.receipt_method_id = rm.receipt_method_id
                   AND rm.name IN ('REBATE_CREDIT', 'REBATE_CHECK')
                   AND to_number(cr.attribute1) = hp1.party_id
                   AND hp1.attribute1 = 'HDS_BU'
                   AND rct.customer_trx_id = ra.applied_customer_trx_id
                   AND cr.cash_receipt_id = ra.cash_receipt_id
                   AND oo.qp_list_header_id = qlhv.list_header_id
                   AND rctl.interface_line_attribute2 =
                       to_char(ocl.claim_id)
                   AND ocl.activity_id = oo.qp_list_header_id
                   AND oo.qp_list_header_id = qlhv.list_header_id
                   AND amv.media_id = oo.activity_media_id
                   AND cr.pay_from_customer = hca.cust_account_id
                   AND hca.party_id = hp.party_id
                   AND hp.attribute1 = 'HDS_MVID')
         GROUP BY mvid, agreement_year) am
      ,(SELECT SUM(unapplied_amount) unapplied_payments, mvid
          FROM (SELECT cr.amount unapplied_amount
                      ,hp.party_name mvid_name
                      ,REPLACE(hp.party_number, '~MSTR', '') mvid
                      ,hca.cust_account_id cust_account_id
                      ,hp.party_name master_vendor
                      ,hp.party_id
                      ,hp1.party_id
                  FROM ar_receipt_methods   rm
                      ,ar_cash_receipts_all cr
                      ,hz_parties           hp1
                      ,hz_cust_accounts     hca
                      ,hz_parties           hp
                 WHERE 1 = 1
                   AND rm.name IN
                       ('REBATE_CREDIT', 'REBATE_CHECK', 'REBATE_DEDUCTION')
                   AND rm.receipt_method_id = cr.receipt_method_id
                   AND cr.org_id IN (101, 102)
                   AND cr.status = 'UNAPP'
                   AND to_number(cr.attribute1) = hp1.party_id
                   AND cr.pay_from_customer = hca.cust_account_id
                   AND hca.party_id = hp.party_id
                   AND hp.attribute1 = 'HDS_MVID')
         GROUP BY mvid) um
 WHERE 1 = 1
   AND a.mvid = b.mvid(+)
   AND a.agreement_year = b.calendar_year(+)
   AND c.mvid = a.mvid
   AND c.agreement_year = a.agreement_year
   AND a.mvid = am.mvid(+)
   AND a.mvid = um.mvid(+)
   AND a.agreement_year = am.agreement_year(+)
UNION ALL
SELECT NULL ytd_income
      ,e.ytd_purchase
      ,NULL invoiced_payments
      ,NULL applied_payments
      ,f.unapplied_payments unapplied_payments
      ,(NULL - (NULL + f.unapplied_payments)) total_outstanding
      ,d.collector_name
      ,d.mvid mvid
      ,d.cust_account_id
      ,d.mvid_name vendor_name
      ,NULL aging_less_than_30
      ,NULL aging_between_31_60
      ,NULL aging_between_61_90
      ,NULL aging_between_90_120
      ,NULL aging_greater_than_120
      ,d.agreement_year
  FROM (SELECT DISTINCT hca.account_number mvid1
                       ,hca.cust_account_id cust_account_id
                       ,hp.party_name mvid_name
                       ,REPLACE(hp.party_number, '~MSTR', '') mvid
                       ,ar.name collector_name
                       ,qlhv.attribute7 agreement_year
                       ,decode(med.description
                              ,'COOP'
                              ,'COOP'
                              ,'COOP_MIN'
                              ,'COOP'
                              ,'REBATE') rebate_type_id
                       ,qlhv.description
                       ,qlhv.list_header_id
          FROM qp_list_headers_vl   qlhv
              ,qp_qualifiers        qq
              ,hz_cust_accounts     hca
              ,hz_parties           hp
              ,hz_customer_profiles hcp
              ,ar_collectors        ar
              ,ozf_offers           oo
              ,ams_media_tl         med
         WHERE qlhv.list_header_id = qq.list_header_id
           AND qq.qualifier_context = 'SOLD_BY'
           AND qq.qualifier_attribute = 'QUALIFIER_ATTRIBUTE2'
           AND to_number(qq.qualifier_attr_value) = hca.cust_account_id
           AND hca.party_id = hp.party_id
           AND hp.attribute1 = 'HDS_MVID'
           AND hca.cust_account_id = hcp.cust_account_id
           AND hcp.collector_id = ar.collector_id
           AND qq.list_header_id = oo.qp_list_header_id
           AND oo.activity_media_id = med.media_id
           AND hca.cust_account_id NOT IN
               (SELECT mvid
                  FROM xxcusozf_accruals_mv accr1
                 WHERE mvid = hca.cust_account_id)) d
      ,(SELECT SUM(pm1.total_purchases) ytd_purchase
              ,pm1.calendar_year agreement_year
              ,pm1.mvid cust_account_id
              ,REPLACE(hp.party_number, '~MSTR', '') mvid
          FROM xxcusozf_purchases_mv pm1
              ,hz_cust_accounts      hca
              ,hz_parties            hp
         WHERE 1 = 1
           AND pm1.grp_mvid = 0
           AND pm1.grp_branch = 0
           AND pm1.grp_cal_year = 0
           AND pm1.grp_period = 1
           AND pm1.grp_qtr = 1
           AND pm1.grp_year = 1
           AND pm1.grp_lob = 1
           AND pm1.mvid = hca.cust_account_id
           AND hca.party_id = hp.party_id
           AND hp.attribute1 = 'HDS_MVID'
         GROUP BY pm1.mvid, hp.party_number, pm1.calendar_year) e
      ,(SELECT SUM(unapplied_amount) unapplied_payments, mvid
          FROM (SELECT cr.amount unapplied_amount
                      ,REPLACE(hp.party_number, '~MSTR', '') mvid
                      ,hp.party_name mvid_name
                      ,hca.cust_account_id cust_account_id
                      ,hp.party_name master_vendor
                  FROM ar_cash_receipts_all cr
                      ,ar_receipt_methods   rm
                      ,hz_parties           hp
                      ,hz_cust_accounts     hca
                      ,hz_parties           hp1
                 WHERE 1 = 1
                   AND cr.org_id IN (101, 102)
                   AND to_number(cr.attribute1) = hp1.party_id
                   AND cr.receipt_method_id = rm.receipt_method_id
                   AND hp.attribute1 = 'HDS_MVID'
                   AND cr.pay_from_customer = hca.cust_account_id
                   AND hca.party_id = hp.party_id
                   AND rm.name IN
                       ('REBATE_CREDIT', 'REBATE_CHECK', 'REBATE_DEDUCTION'))
         GROUP BY mvid) f
 WHERE 1 = 1
   AND d.mvid = e.mvid(+)
   AND d.agreement_year = e.agreement_year(+)
   AND d.mvid = f.mvid(+)
;
