/* Formatted on 10/31/2012 12:46:45 PM (QP5 v5.115.810.9015) */
--
-- XXWC_AP_SUPP_SHIP_SITE_EXT_VW  (View)
--

CREATE OR REPLACE FORCE VIEW apps.xxwc_ap_supp_ship_site_ext_vw (
   operating_unit_id
 , interface_date
 , ship_from_site_data
)
AS
   SELECT	operating_unit_id
		  , supplier_creation_date interface_date
		  , (	business_unit
			 || '|'
			 || source_system
			 || '|'
			 || site_identifier
			 || '|'
             -- 10/31/2012 CG: Changed to be the primary pay site
			 -- || vendor_number
             || primary_pay_site
			 || '|'
			 || vendor_name
			 || '|'
			 || ship_from_tax_id
			 || '|'
			 || ultimate_duns
			 || '|'
			 || hq_duns
			 || '|'
			 || duns
			 || '|'
			 || vendor_site_code
			 || '|'
			 || address1
			 || '|'
			 || address23
			 || '|'
			 || city
			 || '|'
			 || state_province
			 || '|'
			 || postal_code
			 || '|'
			 || fax_number
			 || '|'
			 || country
			 || '|'
			 || phone_number
			 || '|'
			 || site_status
			 || '|'
			 || payment_term_id)
			   ship_from_site_data
	 FROM	xxwc_ap_supplier_sites_vw
	WHERE	NVL (purchasing_site_flag, 'N') = 'Y';


