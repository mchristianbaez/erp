--
-- XXWC_RICE_RECON_VIEW  (View)
--

CREATE OR REPLACE FORCE VIEW APPS.XXWC_RICE_RECON_VIEW
(
   APPLICATION_NAME
  ,PROGRAM_TYPE
  ,SYSTEM_PROGRAM_NAME
  ,USER_PROGRAM_NAME
  ,LINE_COUNT
  ,LAST_UPDATE_DATE
  ,PROCESS_TYPE_DESCRIPTION
  ,PARENT_OBJECT
  ,OTHER_COMPONENTS
  ,LAST_UPDATE_USER
  ,LAST_UPDATE_USER_DESCRIPTION
  ,CREATED_BY_USER
  ,CREATED_BY_USER_DESCRIPTION
)
AS
     SELECT application_name
           ,program_type
           ,system_program_name
           ,user_program_name
           ,line_count
           ,last_update_date
           ,' ' process_type_description
           ,' ' parent_object
           ,' ' other_components
           ,last_update_user
           ,last_update_user_description
           ,created_by_user
           ,created_by_user_description
       FROM (SELECT fa.application_name
                   ,'CONCURRENT_PROGRAM' program_type
                   ,fcp.concurrent_program_name system_program_name
                   ,fcp.user_concurrent_program_name user_program_name
                   ,fcp.last_update_date last_update_date
                   ,fu1.user_name last_update_user
                   ,fu1.description last_update_user_description
                   ,fu2.user_name created_by_user
                   ,fu2.description created_by_user_description
                   ,0 line_count
               FROM apps.fnd_concurrent_programs_vl fcp
                   ,apps.fnd_application_vl fa
                   ,apps.fnd_user fu1
                   ,apps.fnd_user fu2
              WHERE     (   fcp.concurrent_program_name LIKE 'XX%'
                         OR UPPER (fcp.user_concurrent_program_name) LIKE 'XX%')
                    AND fcp.enabled_flag = 'Y'
                    AND fcp.application_id = fa.application_id
                    AND UPPER (fa.application_name) LIKE 'XX%'
                    AND TRUNC (fcp.last_update_date) >
                           TO_DATE ('16-DEC-2000', 'DD-MON-YYYY') --for specific dates
                    AND fcp.created_by = fu1.user_id
                    AND fcp.last_updated_by = fu2.user_id
                    AND fu1.user_name NOT IN ('AUTOINSTALL', 'INITIAL SETUP')
             UNION ALL
             SELECT fa.application_name
                   ,'FORM' program_type
                   ,fff.function_name system_program_name
                   ,fff.user_function_name user_program_name
                   ,fff.last_update_date last_update_date
                   ,fu1.user_name last_update_user
                   ,fu1.description last_update_user_description
                   ,fu2.user_name created_by_user
                   ,fu2.description created_by_user_description
                   ,0 line_count
               FROM apps.fnd_form_functions_vl fff
                   ,apps.fnd_form_vl ff
                   ,apps.fnd_application_vl fa
                   ,apps.fnd_user fu1
                   ,apps.fnd_user fu2
              WHERE     UPPER (fa.application_name) LIKE 'XX%'
                    AND ff.application_id = fa.application_id
                    AND ff.form_id = fff.form_id
                    AND TRUNC (fff.last_update_date) >
                           TO_DATE ('16-DEC-2000', 'DD-MON-YYYY') --for specific dates
                    AND fff.created_by = fu1.user_id
                    AND fff.last_updated_by = fu2.user_id
                    AND fu1.user_name NOT IN ('AUTOINSTALL', 'INITIAL SETUP')
             UNION ALL
             SELECT DISTINCT fa.application_name
                            ,'PERSONALIZATION' program_type
                            ,fff.function_name system_program_name
                            ,fff.user_function_name user_program_name
                            ,fff.last_update_date last_update_date
                            ,fu1.user_name last_update_user
                            ,fu1.description last_update_user_description
                            ,fu2.user_name created_by_user
                            ,fu2.description created_by_user_description
                            ,0 line_count
               FROM apps.fnd_form_custom_rules ffcr
                   ,apps.fnd_form_functions_vl fff
                   ,apps.fnd_application_vl fa
                   ,apps.fnd_user fu1
                   ,apps.fnd_user fu2
              WHERE     ffcr.function_name = fff.function_name
                    AND fff.application_id = fa.application_id
                    AND TRUNC (fff.last_update_date) >
                           TO_DATE ('31-JAN-2000', 'DD-MON-YYYY') --for specific dates
                    AND fff.created_by = fu1.user_id
                    AND fff.last_updated_by = fu2.user_id
                    AND fu1.user_name NOT IN ('AUTOINSTALL', 'INITIAL SETUP')
             UNION ALL
             SELECT fa.application_name
                   ,'ALERTS' program_type
                   ,aa.alert_name system_program_name
                   ,aa.description user_program_name
                   ,aa.last_update_date last_update_date
                   ,fu1.user_name last_update_user
                   ,fu1.description last_update_user_description
                   ,fu2.user_name created_by_user
                   ,fu2.description created_by_user_description
                   ,0 line_count
               FROM apps.alr_alerts aa
                   ,apps.fnd_application_vl fa
                   ,apps.fnd_user fu1
                   ,apps.fnd_user fu2
              WHERE     UPPER (aa.alert_name) LIKE 'XX%'
                    AND aa.application_id = fa.application_id
                    AND (   UPPER (fa.application_name) LIKE 'XX%'
                         OR fa.application_name = 'Oracle Alert')
                    AND aa.enabled_flag = 'Y'
                    AND TRUNC (aa.last_update_date) >
                           TO_DATE ('31-JAN-2000', 'DD-MON-YYYY') --for specific dates
                    AND fa.created_by = fu1.user_id
                    AND fa.last_updated_by = fu2.user_id
                    AND fu1.user_name NOT IN ('AUTOINSTALL', 'INITIAL SETUP')
             UNION ALL
             SELECT sub_query.application_name
                   ,sub_query.program_type
                   ,sub_query.system_program_name
                   ,sub_query.user_program_name
                   ,sub_query.last_ddl_time last_update_date
                   ,NULL last_update_user
                   ,NULL last_update_user_description
                   ,NULL created_by_user
                   ,NULL created_by_user_description
                   ,sub_query.line_count
               FROM (SELECT DISTINCT
                            fa.application_name
                           ,'DATABASE OBJECTS' program_type
                           ,DO.owner || ':' || object_type system_program_name
                           ,DO.object_name user_program_name
                           ,SUBSTR (DO.object_name, 5, 1) calc1
                           ,DECODE (fa.application_short_name
                                   ,NULL, '_'
                                   ,SUBSTR (DO.object_name, 5, 1))
                               calc2
                           ,DO.last_ddl_time
                           , (SELECT COUNT (1)
                                FROM dba_source xx
                               WHERE     xx.owner = DO.owner
                                     AND xx.NAME = DO.object_name
                                     AND xx.TYPE = DO.object_type)
                               line_count
                       FROM dba_objects DO, apps.fnd_application_vl fa
                      WHERE     DO.owner NOT IN ('XX%')
                            AND fa.application_short_name(+) =
                                   SUBSTR (object_name, 2, 3)
                            AND DO.object_name LIKE 'XX%'
                            ---for specific dates
                            AND TRUNC (DO.last_ddl_time) >
                                   TO_DATE ('31-JAN-2000', 'DD-MON-YYYY')) sub_query
              WHERE sub_query.calc1 =
                       DECODE (sub_query.application_name
                              ,NULL, sub_query.calc2
                              ,sub_query.calc1)
             UNION ALL
             SELECT fa.application_name
                   ,'XML TEMPLATE' program_type
                   ,xdo.template_code system_program_name
                   ,xdo.template_name user_program_name
                   ,xdo.last_update_date last_update_date
                   ,NULL last_update_user
                   ,NULL last_update_user_description
                   ,NULL created_by_user
                   ,NULL created_by_user_description
                   ,0 line_count
               FROM xdo_templates_vl xdo, apps.fnd_application_vl fa
              WHERE     xdo.application_short_name LIKE ('XX%')
                    AND fa.application_short_name(+) =
                           xdo.application_short_name
                    AND TRUNC (xdo.last_update_date) >
                           TO_DATE ('31-JAN-2000', 'DD-MON-YYYY') ---for specific dates
             UNION ALL
             SELECT DISTINCT fa.application_name
                            ,'DATABASE OBJECTS' program_type
                            ,DO.owner || ':' || object_type system_program_name
                            ,DO.object_name user_program_name
                            ,DO.last_ddl_time last_update_date
                            ,NULL last_update_user
                            ,NULL last_update_user_description
                            ,NULL created_by_user
                            ,NULL created_by_user_description
                            ,0 line_count
               FROM dba_objects DO, apps.fnd_application_vl fa
              WHERE     DO.owner IN ('XX%')
                    AND fa.application_short_name(+) =
                           SUBSTR (object_name, 2, 3)
                    AND TRUNC (DO.last_ddl_time) >
                           TO_DATE ('31-JAN-2000', 'DD-MON-YYYY') ---for specific dates
             ORDER BY 1, 3)
   ORDER BY 2, 3, 4;


