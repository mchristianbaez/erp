CREATE OR REPLACE VIEW APPS.XXWC_STOCK_LOCATOR_VW as
Select MP.organization_code, 
       MIL.segment1 stock_loc,
       (MP.organization_code||':'||MIL.segment1) stock_loc_desc
 From MTL_ITEM_LOCATIONS_KFV MIL, 
      MTL_PARAMETERS MP 
Where MP.organization_id =  MIL.organization_id(+) 
And MIL.subinventory_code(+) = 'General'
order by 1,2;

