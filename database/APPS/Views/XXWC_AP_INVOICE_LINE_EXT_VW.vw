/* Formatted on 2012/08/31 09:41 (Formatter Plus v4.8.8) */
CREATE OR REPLACE FORCE VIEW apps.xxwc_ap_invoice_line_ext_vw (operating_unit_id,
                                                               interface_date,
                                                               invoice_line_data
                                                              )
AS
   SELECT operating_unit_id, invoice_date interface_date,
          (   business_unit
           || '|'
           || source_system
           || '|'
           || document_number
           || '|'
           || invoice_line_number
           || '|'
           || invoice_type
           || '|'
           || TO_CHAR (inv_line_gl_post_date, 'MM/DD/YYYY')
           || '|'
           || invoice_line_type
           || '|'
           || invoice_line_amount
           || '|'
           || TO_CHAR (last_update_date, 'MM/DD/YYYY')
           || '|'
           || gl_account_number
           || '|'
           || po_number
           || '|'
           || branch
          ) invoice_line_data
     FROM xxwc_ap_invoice_line_vw;


