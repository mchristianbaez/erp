
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUSPN_PRIM_FRU_OPS_TYPE_VW" ("TAG", "BU_ID", "LOC_CODE", "FRU") AS 
  (
      SELECT MIN(to_number(looky.tag)) tag
            ,rollups.bu_id
            ,substr(loca.location_code, 1, 5) loc_code, spc.attribute1 fru
      FROM pn.pn_locations_all                                            loca
          ,pn.pn_space_assign_emp_all                                     spc
          ,apps.xxcus_location_code_vw                                    loco
          ,hdsoracle.xxhsi_pnld_lobrollup@apxprd_lnk.hsi.hughessupply.com rollups
          ,fnd_lookup_values_vl                                           looky
      WHERE loca.location_id = spc.location_id
      AND loca.active_end_date =
            (SELECT MAX(active_end_date)
             FROM pn.pn_locations_all
             WHERE location_id = loca.location_id)
      AND loca.space_type_lookup_code != 'LAWSONADMIN'
      AND looky.enabled_flag = 'Y'
      AND looky.lookup_type = 'PN_SPACE_TYPE'
      AND loca.active_end_date >= SYSDATE
      AND loca.space_type_lookup_code = looky.lookup_code
      AND spc.attribute1 = loco.fru
      AND loco.entrp_entity = rollups.fps_id
      GROUP BY rollups.bu_id, substr(loca.location_code, 1, 5), spc.attribute1)
;
