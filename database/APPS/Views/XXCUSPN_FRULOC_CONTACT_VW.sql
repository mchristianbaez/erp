
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUSPN_FRULOC_CONTACT_VW" ("LOCATION_CODE", "LOCATION_CODE5", "CONTACT_ADDRESS_TYPE", "ADDRESS", "SUITEUNIT", "STATEPROV", "CITY", "POSTAL", "COMPANY_SITE_ID", "COMPANY_ID", "COMPANY_NUMBER", "COMP_NAME", "PHONE", "FAX") AS 
  SELECT DISTINCT loc.location_code
               ,substr(loc.location_code, 1, 5) location_code5
               ,cosite.lease_role_type contact_address_type
               ,nvl(contaddr.address_line1,
                    decode(cosite.lease_role_type, 'SC',
                            (SELECT DISTINCT address_line1
                                FROM apxcmmn.hds_fruloc_vw@apxprd_lnk.hsi.hughessupply.com
                               WHERE primary_bld =
                                     substr(loc.location_code, 1, 5)
                                 AND rownum = 1), contaddr.address_line1)) address
               ,nvl(contaddr.address_line2,
                    decode(cosite.lease_role_type, 'SC',
                            (SELECT DISTINCT address_line2
                                FROM apxcmmn.hds_fruloc_vw@apxprd_lnk.hsi.hughessupply.com
                               WHERE primary_bld =
                                     substr(loc.location_code, 1, 5)
                                 AND rownum = 1), contaddr.address_line2)) suiteunit
               ,nvl(contaddr.state,
                    decode(cosite.lease_role_type, 'SC',
                            (SELECT DISTINCT state_province
                                FROM apxcmmn.hds_fruloc_vw@apxprd_lnk.hsi.hughessupply.com
                               WHERE primary_bld =
                                     substr(loc.location_code, 1, 5)
                                 AND rownum = 1), contaddr.state)) stateprov
               ,nvl(contaddr.city,
                    decode(cosite.lease_role_type, 'SC',
                            (SELECT DISTINCT city
                                FROM apxcmmn.hds_fruloc_vw@apxprd_lnk.hsi.hughessupply.com
                               WHERE primary_bld =
                                     substr(loc.location_code, 1, 5)
                                 AND rownum = 1), contaddr.city)) city
               ,nvl(contaddr.zip_code,
                    decode(cosite.lease_role_type, 'SC',
                            (SELECT DISTINCT zip_code
                                FROM apxcmmn.hds_fruloc_vw@apxprd_lnk.hsi.hughessupply.com
                               WHERE primary_bld =
                                     substr(loc.location_code, 1, 5)
                                 AND rownum = 1), contaddr.zip_code)) postal
               ,cosite.company_site_id
               ,cosite.company_id
               ,comp.company_number
               ,upper(comp.name) comp_name
               ,(SELECT phone_number
                   FROM pn.pn_contacts_all con
                       ,pn.pn_phones_all   y
                  WHERE cosite.company_site_id = con.company_site_id(+)
                    AND y.contact_id = con.contact_id
                    AND y.phone_type = 'GEN') phone
               ,(SELECT phone_number
                   FROM pn.pn_contacts_all con
                       ,pn.pn_phones_all   y
                  WHERE cosite.company_site_id = con.company_site_id(+)
                    AND y.contact_id = con.contact_id
                    AND y.phone_type = 'FAX') fax
  FROM pn_locations_all              loc
      ,pn_addresses_all              contaddr
      ,pn.pn_contact_assignments_all conass
      ,pn.pn_company_sites_all       cosite
      ,pn.pn_companies_all           comp
 WHERE loc.location_id = conass.location_id
   AND conass.company_site_id = cosite.company_site_id(+)
   AND cosite.address_id = contaddr.address_id(+)
   AND cosite.company_id = comp.company_id(+)
   AND loc.active_end_date = '31-DEC-4712'
;
