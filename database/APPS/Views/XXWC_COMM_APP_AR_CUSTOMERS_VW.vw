/* Formatted on 11/1/2012 4:50:57 PM (QP5 v5.115.810.9015) */
--
-- XXWC_COMM_APP_AR_CUSTOMERS_VW  (View)
--

CREATE OR REPLACE FORCE VIEW apps.xxwc_comm_app_ar_customers_vw (
   job_name
 , job_number
 , job_description
 , account_number
 , master_number
 , prism_account_number
 , prism_site_number
 , customer_name
 , address1
 , address2
 , address3
 , address4
 , city
 , postal_code
 , state_provice
 , phone_number
 , salesrep_number
 , salesrep_name
 , site_creation_date
 , party_site_id
 , status
)
AS
   SELECT	DISTINCT
			(NVL (hps.party_site_name, hcsua.location)) job_name
		  , hps.party_site_number job_number
		  , hcsua.location job_description
		  , hca.account_number
		  , hp.party_number master_number
		  , hca.attribute6 prism_account_number
		  , hcasa.attribute17 prism_site_number
		  , 		-- this may not be true depending on the roll up i've seen
		   hp.party_name customer_name
		  , hl.address1
		  , hl.address2
		  , hl.address3
		  , hl.address4
		  , hl.city
		  , hl.postal_code
		  , NVL (hl.state, hl.province) state_provice
		  , /*, xxwc_mv_routines_pkg.get_phone_fax_number('PHONE'
									, hp.party_id
								  , null
								, 'GEN') phone_number*/
		   NULL phone_number
		  , jrs.salesrep_number
		  , NVL (NVL (jrs.name, papf.full_name), jrre.resource_name)
			   salesrep_name
		  , hps.creation_date site_creation_date
		  , hps.party_site_id
		  , hps.status
	 FROM	hz_cust_accounts hca
		  , hz_parties hp
		  , hz_cust_acct_sites_all hcasa
		  , hz_party_sites hps
		  , hz_locations hl
		  , hz_cust_site_uses_all hcsua
		  , jtf_rs_salesreps jrs
		  , jtf_rs_defresources_v jrre
		  , hr.per_all_people_f papf
		  , hr_operating_units hou
	WHERE		hca.party_id = hp.party_id
			AND hca.cust_account_id = hcasa.cust_account_id
			AND hcasa.party_site_id = hps.party_site_id
			AND hps.location_id = hl.location_id
			AND hcasa.cust_acct_site_id = hcsua.cust_acct_site_id
			AND hcsua.primary_salesrep_id = jrs.salesrep_id(+)
			AND hcsua.org_id = jrs.org_id(+)
			AND jrs.resource_id = jrre.resource_id(+)
			AND jrs.person_id = papf.person_id(+)
			AND TRUNC (papf.effective_start_date(+)) <= TRUNC (SYSDATE)
			AND NVL (TRUNC (papf.effective_end_date(+)), TRUNC (SYSDATE)) >=
				  TRUNC (SYSDATE)
			AND hcsua.org_id = hou.organization_id
			AND hou.name = 'HDS White Cap - Org';


--GRANT SELECT ON APPS.XXWC_COMM_APP_AR_CUSTOMERS_VW TO INTERFACE_APEXWC;

