
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUS_OZF_RECEIPT_VW" ("BU_NM", "ORACLE_UNPROCESSED_TOTAL", "FISCAL_PERIOD_ID", "PERIOD_NAME") AS 
  SELECT  /*+ PARALLEL (AUTO) */
       bu_nm
      ,SUM((CASE
             WHEN item_cost_amt < 0 THEN
              item_cost_amt * -1
             ELSE
              item_cost_amt
           END) * ((CASE
             WHEN item_cost_amt < 0 THEN
              recpt_qty * -1
             ELSE
              recpt_qty
           END) / (CASE
             WHEN buy_pkg_unt_cnt IS NULL THEN
              1
             WHEN buy_pkg_unt_cnt = 0 THEN
              1
             ELSE
              buy_pkg_unt_cnt
           END))) oracle_unprocessed_total
      ,period_year || lpad(period_num, 2, 0) fiscal_per_id
      ,period_name
  FROM xxcus.xxcus_rebate_receipt_curr_tbl, gl.gl_periods
 WHERE bu_nm IN (SELECT /*+ RESULT_CACHE */
                  flv.description
                   FROM apps.fnd_lookup_values flv
                  WHERE flv.lookup_type = 'XXCUS_REBATE_BU_XREF'
                    AND flv.enabled_flag = 'Y'
                    AND nvl(flv.end_date_active, SYSDATE) >= SYSDATE
                    AND flv.description = bu_nm)
   AND status_flag = 'N'
   and recpt_crt_dt between start_date and end_date
 GROUP BY bu_nm, period_year || lpad(period_num, 2, 0), period_name
;
