
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUSOZF_ACCRUAL_OFFER_V" ("MVID", "LOB", "VENDOR_RESALE_LINE", "OFFER_NAME", "VENDOR_ACCRUALS", "OFFER_CODE", "ACTIVITY_TYPE", "PAY_TO_VENDOR_CODE", "BU_BRANCH_CODE", "LOCATION_SEGMENT", "PRODUCT", "RESALE_ORDER_DATE", "PURCHASES", "ACCRUALS") AS 
  SELECT hzca.attribute2 MVID,
  orl.bill_to_party_name LOB,
  orl.sold_from_party_name Vendor_Resale_Line,
  ofu.offer_name Offer_Name,
  ofu.ofu_mvid_name Vendor_Accruals,
  ofu.offer_code Offer_Code,
  ofu.activity_media_name Activity_Type,
  orl.line_attribute1 Pay_To_Vendor_Code,
  orl.line_attribute6 Bu_Branch_Code,
  orl.line_attribute12 Location_Segment,
  orl.line_attribute11 Product,
  orl.date_ordered Resale_Order_Date,
  SUM(nvl(selling_price*quantity,0)) Purchases,
  SUM(nvl(ofu.amount,0)) Accruals
FROM ozf_resale_lines_all orl,
  hz_cust_accounts hzca,
  (SELECT object_id ,
    offer_name,
    ofu_mvid_name,
   OFFER_CODE,
    activity_media_name,     
    SUM(NVL(util_acctd_amount,0)
    ) amount
  FROM xxcus.xxcus_ozf_xla_accruals_b
  where 1=1
  and offer_id <> 69797
  GROUP BY object_id ,
    offer_name,
    ofu_mvid_name,
   OFFER_CODE,
   activity_media_name
    ) ofu
WHERE ofu.object_id(+)            = orl.resale_line_id
AND hzca.cust_account_id          = orl.sold_from_cust_account_id
--AND orl.sold_from_cust_account_id = '1953' ---LIMIT RECEIPTS TO SPECIFIC CUSTOMER
--AND orl.date_ordered BETWEEN '01-JAN-2013' AND '04-AUG-2013'
GROUP BY ofu.offer_name,
  hzca.attribute2,
  orl.bill_to_party_name,
  orl.sold_from_party_name,
  ofu.ofu_mvid_name,
  ofu.offer_name,
  ofu.offer_code ,
  ofu.activity_media_name ,
  orl.line_attribute1 ,
  orl.line_attribute6 ,
 orl.line_attribute12,
  orl.line_attribute11,
  orl.date_ordered
ORDER BY 2;
