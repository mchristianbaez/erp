
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUSOZF_RBT_ADJ_REASON_V" ("REASON_CODE", "REASON", "REASON_NAME") AS 
  SELECT lookup_code reason_code, meaning reason, description reason_name
     FROM fnd_lookup_values
    WHERE     1 = 1
          AND lookup_type = 'XXCUS_REB_ADJ_REASON'
          AND enabled_flag = 'Y'
   ORDER BY TO_NUMBER(TAG) ASC;
