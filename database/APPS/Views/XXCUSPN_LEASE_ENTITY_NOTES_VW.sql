
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUSPN_LEASE_ENTITY_NOTES_VW" ("LEASE_ID", "NOTE_TYPE", "TEXT", "LEASE_TYPE_CODE", "UPDATED_BY_USER_NAME", "LEASE_CLASS_CODE") AS 
  SELECT nh.lease_id, nh.note_type, nd.TEXT,  lea.LEASE_TYPE_CODE, nh.updated_by_user_name, lea.lease_class_code
FROM pn_note_headers_v nh,
  pn_note_details_vl nd,
  (SELECT lea.lease_id, lea.lease_type_code, lea.lease_class_code FROM pn_leases_all lea) lea
WHERE nh.note_header_id = nd.note_header_id
AND lea.lease_id = nh.lease_id
AND nh.note_type_lookup_code = DECODE(lea.LEASE_TYPE_CODE, 'LSEE', 'TCUR',
                                                           'LSE',  'LLCUR',
                                                           'ISUB', 'LLCUR',
                                                           'OWNO', 'OCUR',
                                                           'ISA',  DECODE(lea.lease_class_code,'DIRECT','TCUR','SUB_LEASE','LLCUR'),
                                                           NULL)
;
