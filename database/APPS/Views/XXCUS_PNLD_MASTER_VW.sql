CREATE OR REPLACE FORCE VIEW "APPS"."XXCUS_PNLD_MASTER_VW" ("FRU", "SEGMENT_DESCRIPTION", "FRU_LOCATION_NAME", "BU_DESCRIPTION", "POS_SYSTEM", "OPS_NICKNAME", "PRIMARY_OPS_TYPE", "OPERATIONS_TYPE", "FRU_TYPE", "OPS_STATUS", "FRU_STATUS", "LOB_OCCUPANCY", "CURRENCY", "LANGUAGE", "PRIMARY_BLD", "TENURE", "TIMEZONE", "FRU_OPEN_DATE", "SEGMENT_ID", "LOB_NAME", "BU_DESC", "EMP_SPACE_ASSIGN_ID", "LOCATION_ID", "EMP_ASSIGN_START_DATE", "ORACLE_ID", "ALLOCATED_AREA_PCT", "ALLOCATED_AREA", "UTILIZED_AREA", "LOB_BRANCH", "LOCATION_TYPE", "LOC_DESC", "LONGITUDE", "LATITUDE", "LOCATION_CODE", "LOCATION_CODE_LVL1", "LOCATION_CODE_LVL2", "LOCATION_CODE_LVL3", "LOC_SQAURE_FEET", "LOC_ACRE", "OPERATING_HOURS", "WALK_IN", "CUSTOMER_SITE", "SIGNAGE", "OPERATIONS_OPEN_DATE", "OPERATIONS_CLOSED_DATE", "OPERATIONS_SQUARE_FOOTAGE", "OPERATIONS_ACREAGE", "UOM_CODE", "RENTABLE_AREA", "USABLE_AREA", "GROSS_AREA", "ADDRESS_LINE1", "ADDRESS_LINE2", "ADDRESS_LINE3", "ADDRESS_LINE4", "COUNTY", "CITY", "STATE", "PROVINCE", "COUNTRY", "ZIP_CODE", "ADDRESS_ID", "ASSIGNMENT_TYPE", "SITE_TYPE", "COUNTYNAME", "METRO_LATITUDE", "METRO_LONGITUDE", "METROAREA", "CONTACT_ADDRESS_TYPE", "REMIT_ADDRESS", "MAILING_ADDRESS", "MAILING_SUITE", "FRU_COUNT", "LOC_COUNT", "PHONE", "FAX", "FED_TAX_ID", "COMPANY", "BRANCH_MANAGER", "EMP_COUNT", "LOCATION_OPEN_DATE", "LOCATION_CLOSE_DATE", "TENANCY_TYPE", "MARKET_CLUSTER") AS 
  SELECT DISTINCT empsp.attribute1 fru
               ,gldcr.product segment_description
               ,gldcr.location fru_location_name
               ,rollups.bu_desc bu_desc
               ,gldcr.pos_system
               ,loc1.building ops_nickname
               ,loc.space_type_lookup_code primary_ops_type
               ,loc_space.meaning operations_type
               ,loc.standard_type_lookup_code fru_type --placeholder
               ,CASE
                  WHEN trunc(empsp.emp_assign_start_date) > trunc(SYSDATE) THEN
                   'FUTURE'
                  WHEN empsp.emp_assign_end_date IS NOT NULL THEN
                   'INACTIVE'
                  ELSE
                   'ACTIVE'
                END ops_status
               ,CASE
                  WHEN loc.standard_type_lookup_code = 'REAL' THEN
                   'INACTIVE'
                  WHEN (loc.standard_type_lookup_code = 'PPL' OR
                       loc.standard_type_lookup_code = 'BOTH')
                       AND
                       trunc(empsp.emp_assign_start_date) <= trunc(SYSDATE) THEN
                   'ACTIVE'
                  WHEN empsp.emp_assign_end_date IS NOT NULL THEN
                   'INACTIVE'
                  ELSE
                   'ACTIVE'
                END fru_status
               ,prop.portfolio
               ,prop.attribute2 currency
               ,prop.attribute3 LANGUAGE
               ,prop.attribute4 primary_bld
               ,prop_tenure.meaning tenure
               ,prop_zone.meaning timezone
               ,gldcr.fru_create_date fru_open_date
               ,rollups.fps_id
               ,rollups.bu_id lob_name
               ,rollups.fps_desc
               ,empsp.emp_space_assign_id
               ,empsp.location_id
               ,empsp.emp_assign_start_date
               ,empsp.cost_center_code
               ,empsp.allocated_area_pct
               ,empsp.allocated_area
               ,empsp.utilized_area
               ,corp.lob_branch --Or above
               ,decode(loc.location_type_lookup_code, 'OFFICE', 'SECTION',
                       'SECTION', 'YARD', loc.location_type_lookup_code) location_type
               ,decode(loc.location_type_lookup_code, 'BUILDING',
                       loc.building, 'FLOOR', loc.floor, 'OFFICE',
                       loc.office) loc_desc
               ,loc1.attribute1 longitude
               ,loc1.attribute2 latitude
               ,prop.property_code location_code
               ,loc1.location_code location_code_lvl1
               ,loc2.location_code location_code_lvl2
               ,loc.location_code location_code_lvl3
               ,loc1.gross_area loc_sqaure_feet
               ,loc1.gross_area / 43560 loc_acre
               ,loc.attribute3 operating_hours
               ,loc.attribute4 walk_in
               ,loc.attribute5 customer_site
               ,loc_feat.description signage
               ,loc.active_start_date operations_open_date
               ,decode(loc.active_end_date,
                       to_date('31-DEC-4712', 'DD-MON-YYYY'), NULL,
                       loc.active_end_date) operations_closed_date
               ,loc.assignable_area operations_square_footage
               ,loc.assignable_area / 43560 operations_acreage
               ,loc.uom_code
               ,loc.rentable_area
               ,loc.usable_area
               ,loc_gross.gross_area
               ,addr.address_line1
               ,addr.address_line2
               ,addr.address_line3
               ,addr.address_line4
               ,addr.county
               ,addr.city
               ,addr.state
               ,addr.province
               ,decode(addr.country, 'US', 'United States', 'CA', 'Canada',
                       addr.country) country
               ,addr.zip_code
               ,loc1.address_id
               ,loc_assign.meaning assignment_type
               ,loc.function_type_lookup_code site_type
               ,cbsanamer.countyname
               ,cbsanamer.latitude metro_latitude
               ,cbsanamer.longitude metro_longitude
               ,cbsanamer.metroarea metroarea
               ,conttype.contact_address_type -- v1.2
               ,decode(conttype.contact_address_type, 'REMIT',
                       conttype.address) remit_address
               ,decode(conttype.contact_address_type, 'MAILING',
                       conttype.address) mailing_address
               ,decode(conttype.contact_address_type, 'MAILING',
                       conttype.suiteunit) mailing_suite
               ,(SELECT COUNT(a.fru)
                   FROM xxcus.xxcuspn_ld_financial_det_tbl a
                  WHERE a.location_code =
                        nvl(prop.property_code, loc1.location_code)) fru_count
               ,(SELECT COUNT(b.location_code)
                   FROM xxcus.xxcuspn_ld_opsover_tbl b
                  WHERE b.fru = empsp.attribute1) loc_count
               ,(SELECT phone_number
                   FROM pn.pn_contacts_all con
                       ,pn.pn_phones_all   y
                  WHERE conttype.company_site_id = con.company_site_id(+)
                    AND y.contact_id = con.contact_id
                  AND y.primary_flag = 'Y'               --483500
                    AND y.phone_type = 'GEN') phone
               ,(SELECT phone_number
                   FROM pn.pn_contacts_all con
                       ,pn.pn_phones_all   y
                  WHERE conttype.company_site_id = con.company_site_id(+)
                    AND y.contact_id = con.contact_id
                    AND y.phone_type = 'FAX') fax
               ,(SELECT DISTINCT sale.federal_tax_id
                   FROM  xxcus.xxcuspn_ld_financial_det_tbl fin
                       ,hdsoracle.pnld_salestax@apxprd_lnk      sale
                  WHERE fin.location_code_lvl3 = loc.location_code
                    AND fin.fru = empsp.attribute1
                    AND (fin.fru = sale.
                         fru(+) AND fin.location_code_lvl1 = sale.loc_id(+))) fed_tax_id
               ,(SELECT company_number
                   FROM pn.pn_companies_all
                  WHERE company_id = conttype.company_id) company
               ,NULL branch_manager
               ,0 emp_count
                --SR 34678 START
               ,loc3.active_start_date location_open_date
               ,decode(loc3.active_end_date,
                       to_date('31-DEC-4712', 'DD-MON-YYYY'), NULL,
                       loc3.active_end_date) location_close_date
               ,prop_tenure2.meaning tenancy_type
               -- adding market_cluster
               ,prop.attribute5 market_cluster
--SR 34678 END
  FROM pn_space_assign_emp_all empsp
      ,pn_locations_all loc
      ,pn_addresses_all addr
      ,pn.pn_properties_all prop
      ,(SELECT meaning
              ,lookup_code
          FROM fnd_lookup_values
         WHERE lookup_type LIKE 'PN_SPACE_TYPE') loc_space
      ,xxcus.xxcuspn_ld_gldesc_tbl gldcr
      ,(SELECT loc2.location_id
              ,loc2.parent_location_id
              ,loc2.active_end_date
              ,loc2.location_code
              ,loc2.space_type_lookup_code
          FROM pn.pn_locations_all loc2) loc2
      ,(SELECT loc1.location_id
              ,loc1.parent_location_id
              ,gross_area
              ,address_id
              ,loc1.property_id
              ,loc1.building
              ,loc1.attribute1
              ,loc1.attribute2
              ,loc1.location_code
              ,loc1.active_end_date
          FROM pn.pn_locations_all loc1) loc1
       --SR 34678 START
      ,(SELECT loc3.location_id
              ,loc3.parent_location_id
              ,loc3.address_id
              ,loc3.property_id
              ,loc3.building
              ,loc3.location_code
              ,loc3.lease_or_owned
              ,loc3.active_end_date
              ,loc3.active_start_date
          FROM pn.pn_locations_all loc3
         WHERE loc3.location_type_lookup_code IN ('BUILDING', 'LAND')
           AND loc3.active_start_date =
               (SELECT MAX(la.active_start_date)
                  FROM pn.pn_locations_all la
                 WHERE loc3.location_code = la.location_code)) loc3
      ,(SELECT meaning
              ,lookup_code
          FROM fnd_lookup_values
         WHERE lookup_type = 'PN_LEASED_OR_OWNED') prop_tenure2
       --SR 34678 END
      ,(SELECT fru
              ,entrp_loc
              ,lob_branch
          FROM apps.xxcus_location_code_vw) corp
      ,pn.pn_locations_all loc_gross
      ,(SELECT meaning
              ,lookup_code
          FROM fnd_lookup_values
         WHERE lookup_type = 'PN_FUNCTION_TYPE') loc_site
      ,(SELECT meaning
              ,lookup_code
          FROM fnd_lookup_values
         WHERE lookup_type = 'PN_STANDARD_TYPE') loc_assign
      ,apps.xxcuspn_ld_lob_rollup_vw rollups
      ,(SELECT DISTINCT substr(loc.location_code, 1, 5) location_code
                       ,cosite.lease_role_type contact_address_type
                       ,contaddr.address_line1 address
                       ,contaddr.address_line2 suiteunit
                       ,contaddr.state stateprov
                       ,contaddr.city
                       ,contaddr.zip_code postal
                       ,cosite.company_site_id
                       ,cosite.company_id
          FROM pn_locations_all              loc
              ,pn_addresses_all              contaddr
              ,pn.pn_contact_assignments_all conass
              ,pn.pn_company_sites_all       cosite
         WHERE loc.location_id = conass.location_id
           AND conass.company_site_id = cosite.company_site_id(+)
           AND cosite.address_id = contaddr.address_id(+)
           AND loc.active_end_date = '31-DEC-4712') conttype
      ,(SELECT meaning
              ,lookup_code
          FROM fnd_lookup_values
         WHERE lookup_type = 'PN_ZONE_TYPE') prop_zone
      ,(SELECT meaning
              ,lookup_code
          FROM fnd_lookup_values
         WHERE lookup_type = 'PN_LEASED_OR_OWNED') prop_tenure
      ,xxcus.xxcuspn_ld_site_detail_tbl cbsanamer
      ,(SELECT location_feature_id
              ,location_id
              ,description
          FROM pn_location_features_all
         WHERE location_feature_lookup_code = 'SIGN') loc_feat
 WHERE empsp.location_id = loc.location_id(+)
   AND loc1.address_id = addr.address_id(+)
   AND loc1.property_id = prop.property_id(+)
   AND empsp.cost_center_code = gldcr.segment2(+)
   AND loc_gross.location_code = loc1.location_code
   AND loc_gross.active_end_date = '31-DEC-4712'
   AND rollups.fps_id = gldcr.segment1
   AND loc.standard_type_lookup_code = loc_assign.lookup_code(+)
   AND loc.function_type_lookup_code = loc_site.lookup_code(+)
   AND loc.parent_location_id = loc2.location_id
   AND loc2.parent_location_id = loc1.location_id
      --SR 34678 START
   AND loc3.location_id = loc1.location_id
   AND loc3.lease_or_owned = prop_tenure2.lookup_code(+)
      --SR 34678 END
   AND empsp.attribute1 = corp.FRU
   AND loc.space_type_lookup_code = loc_space.lookup_code(+)
   AND gldcr.l_corp_id = empsp.attribute1
   AND loc.active_end_date = '31-DEC-4712'
   AND loc1.active_end_date = '31-DEC-4712'
   AND loc2.active_end_date = '31-DEC-4712'
   AND nvl(empsp.emp_assign_end_date, to_date('31-DEC-4712', 'DD-MON-YYYY')) =
       (SELECT MAX(nvl(spc.emp_assign_end_date,
                       to_date('31-DEC-4712', 'DD-MON-YYYY')))
          FROM pn.pn_space_assign_emp_all spc
         WHERE spc.attribute1 = empsp.attribute1
           AND spc.location_id = empsp.location_id)
   AND loc1.location_code = conttype.location_code(+)
   AND prop.zone = prop_zone.lookup_code(+)
   AND prop.tenure = prop_tenure.lookup_code(+)
   AND prop.property_code = cbsanamer.location_code(+)
   AND loc1.location_id = loc_feat.location_id(+)
;
