/* Formatted on 2/18/2013 2:47:15 PM (QP5 v5.115.810.9015) */
--
-- XXWC_INV_PRODUCT_LOC_VW	(View)
--

CREATE OR REPLACE FORCE VIEW apps.xxwc_inv_product_loc_vw (
   business_unit
 , source_system
 , operating_unit_id
 , operating_unit_name
 , interface_date
 , branch_number
 , branch_name
 , su_sku
 , sku_start_date
 , stock_indicator
 , prod_rank
 , dept_number
 , prod_status_code
 , buyer_number
 , po_replacement_cost
 , po_repl_cost_effect_date
 , list_price
 , list_price_eff_date
 , std_cost_amount
 , std_cost_effe_date
 , branch_cost
 , branch_cost_effe_date
 , prod_velocity_code
 , prod_line_status_code
 , prod_line_status_desc
 , part_vendor_code
 , min_qty
 , max_qty
 , safety_stock
 , order_lead_days
 , minimum_gross_margin
 , min_sell_price_amt
 , last_count_qty
 , last_counted_by
 , last_count_date
 , lead_time_expiration_date
 , last_po_receipt_date
 , last_transfer_out_date
 , last_transfer_in_date
 , last_work_order_in_date
 , last_work_order_out_date
 , last_sales_date
 , twelve_month_wo_out_qty
 , twelve_month_trx_out_qty
 , twelve_month_stock_sale_qty
 , twelve_month_direct_sale_qty
 , forecast_hits
 , forecast_period
 , override_branch_hits
 , twelve_month_hits
 , price_sheet
 , price_sheet_effe_date
 , warehouse_branch
 , warehouse_branch_flag
 , seasonal_flag
 , trend_percent
 , primary_bin_location
 , primary_unit_of_measure
 , organization_id
 , inventory_item_id
 , source_sup_prim_pay_site
)
AS
	 SELECT   'WHITECAP' business_unit
			, 'Oracle EBS' source_system
			, hou.organization_id operating_unit_id
			, hou.name operating_unit_name
			, msib.creation_date interface_date
			, mp.organization_code branch_number
			, ood.organization_name branch_name
			, msib.segment1 su_sku
			, msib.creation_date sku_start_date
			, (CASE
				  WHEN SUBSTR (
						  xxwc_mv_routines_pkg.get_item_category (
							 msib.inventory_item_id
						   , msib.organization_id
						   , 'Sales Velocity'
						   , 1
						  )
						, 1
						, 40
					   ) IN
							 ('1', '2', '3', '4', '5', '6', '7', '8', '9', 'E')
				  THEN
					 'S'
				  ELSE
					 'N'
			   END)
				 stock_indicator
			, SUBSTR (
				 xxwc_mv_routines_pkg.get_item_category (msib.inventory_item_id
													   , msib.organization_id
													   , 'Sales Velocity'
													   , 1)
			   , 1
			   , 163
			  )
				 prod_rank
			, NULL dept_number
			, msib.inventory_item_status_code prod_status_code
			, papf.employee_number buyer_number
			,												  -- 06/03/2012 CG
			  /*xxwc_mv_routines_pkg.get_last_po_price
						(msib.inventory_item_id,
						  msib.organization_id
					  ) po_replacement_cost,*/
			  msib.list_price_per_unit po_replacement_cost
			, NULL po_repl_cost_effect_date
			, xxwc_mv_routines_pkg.get_item_qp_list_price (
				 msib.inventory_item_id
			  )
				 list_price
			, NULL list_price_eff_date
			, cst_cost_api.get_item_cost (1.0
										, msib.inventory_item_id
										, msib.organization_id)
				 std_cost_amount
			, NULL std_cost_effe_date
			, NULL branch_cost
			, NULL branch_cost_effe_date
			, msib.attribute12 prod_velocity_code
			, NULL prod_line_status_code
			, NULL prod_line_status_desc
			-- 11/20/2012 CG: changed per email from Rich
			/*, SUBSTR (
						 xxwc_mv_routines_pkg.get_item_sr_vendor (
						  msib.inventory_item_id
						 , msib.organization_id
						   )
					   , 1
						   , 30
						)
						part_vendor_code*/
			, SUBSTR (
				 xxwc_mv_routines_pkg.get_default_supplier (
					msib.inventory_item_id
				  , (CASE
						WHEN msib.source_type = 1
							 AND msib.source_organization_id IS NOT NULL
						THEN
						   msib.source_organization_id
						ELSE
						   msib.organization_id
					 END)
				  , 'IDENTIFIER'
				 )
			   , 1
			   , 30
			  )
				 part_vendor_code
			-- 11/20/2012 CG
			, msib.min_minmax_quantity min_qty
			, msib.max_minmax_quantity max_qty
			, xxwc_mv_routines_pkg.get_safety_stock_qty (msib.inventory_item_id
													   , msib.organization_id)
				 safety_stock
			, (msib.preprocessing_lead_time + msib.full_lead_time)
				 order_lead_days
			, xxwc_om_pricing_guardrail_pkg.get_margin_disc_info (
				 'MARGIN'
			   , msib.inventory_item_id
			   , msib.organization_id
			   , mp.attribute6
			   , SYSDATE
			  )
				 minimum_gross_margin
			, (cst_cost_api.get_item_cost (1.0
										 , msib.inventory_item_id
										 , msib.organization_id)
			   * (xxwc_om_pricing_guardrail_pkg.get_margin_disc_info (
					 'MARGIN'
				   , msib.inventory_item_id
				   , msib.organization_id
				   , mp.attribute6
				   , SYSDATE
				  )
				  / 100))
				 min_sell_price_amt
			, xxwc_mv_routines_pkg.get_last_count_qty (msib.inventory_item_id
													 , msib.organization_id)
				 last_count_qty
			,			  -- 08/10/12 CG: Changed to pull NTID instead of name
			  /*SUBSTR
				   (xxwc_mv_routines_pkg.get_last_counted_by
							(msib.inventory_item_id,
							  msib.organization_id
							 ),
					 1,
				   240
						) last_counted_by,*/
			  SUBSTR (
				 xxwc_mv_routines_pkg.get_last_counted_by_ntid (
					msib.inventory_item_id
				  , msib.organization_id
				 )
			   , 1
			   , 100
			  )
				 last_counted_by
			, xxwc_mv_routines_pkg.get_last_count_date (msib.inventory_item_id
													  , msib.organization_id)
				 last_count_date
			, NULL lead_time_expiration_date
			, xxwc_mv_routines_pkg.get_last_receipt_date (
				 msib.inventory_item_id
			   , msib.organization_id
			   , 'PO'
			  )
				 last_po_receipt_date
			, xxwc_mv_routines_pkg.get_last_transf_out_date (
				 msib.inventory_item_id
			   , msib.organization_id
			  )
				 last_transfer_out_date
			, xxwc_mv_routines_pkg.get_last_receipt_date (
				 msib.inventory_item_id
			   , msib.organization_id
			   , 'Intransit'
			  )
				 last_transfer_in_date
			, NULL last_work_order_in_date
			, NULL last_work_order_out_date
			, xxwc_mv_routines_pkg.get_last_sales_date (msib.inventory_item_id
													  , msib.organization_id)
				 last_sales_date
			, NULL twelve_month_wo_out_qty
			, NULL twelve_month_trx_out_qty
			, NULL twelve_month_stock_sale_qty
			, NULL twelve_month_direct_sale_qty
			, msib.attribute13 forecast_hits
			,			  -- 06/25/2012 CG: Changed to be 160 expecting number
			  -- '4 Months' forecast_period,
			  '160' forecast_period
			, NULL override_branch_hits
			, msib.attribute15 twelve_month_hits
			, NULL price_sheet
			, msib.creation_date price_sheet_effe_date
			, mp_source.organization_code warehouse_branch
			,		 -- 07/27/2012 CG: Modified to reflect document definition
			  -- of being mother branch
			  /*DECODE (msib.source_type,
						 1, 'Inventory',
					   2, 'Supplier',
					  3, 'Subinventory',
						NULL
					  ) warehouse_branch_flag,*/
			  SUBSTR (
				 xxwc_mv_routines_pkg.is_item_branch_source (
					msib.inventory_item_id
				  , msib.organization_id
				 )
			   , 1
			   , 1
			  )
				 warehouse_branch_flag
			, SUBSTR (
				 xxwc_mv_routines_pkg.get_item_category (msib.inventory_item_id
													   , msib.organization_id
													   , 'Purchase Flag'
													   , 1)
			   , 1
			   , 163
			  )
				 seasonal_flag
			, NULL trend_percent
			,						  -- 06/25/2012 CG Updated to use function
			 SUBSTR (
				 xxwc_mv_routines_pkg.get_item_dflt_loc (msib.inventory_item_id
													   , msib.organization_id
													   , NULL
													   , 1)
			   , 1
			   , 40
			  )
				 primary_bin_location
			, msib.primary_unit_of_measure
			, mp.organization_id
			, msib.inventory_item_id
			-- 01/06/2013 CG: Added to pull vendor pay site
            -- 02/18/2013 CG: TMS 20130218-01098, changing to pull vendor for SPECIALS based on last PO.
            --                Altered bottom piece for regular items to simplify
			, ( CASE 
                    WHEN msib.item_type = 'SPECIAL' OR msib.segment1 like 'SP/%'
                        THEN XXWC_MV_ROUTINES_PKG.GET_SPECIAL_LAST_PAY_SITE ( msib.inventory_item_id, msib.organization_id, hou.organization_id )
                    ELSE
                        SUBSTR ( xxwc_mv_routines_pkg.get_edw_supp_prim_pay_site (
                                                                                  xxwc_mv_routines_pkg.get_default_supplier ( 
                                                                                                                                msib.inventory_item_id
                                                                                                                                , (CASE WHEN msib.source_type = 1 AND msib.source_organization_id IS NOT NULL
                                                                                                                                      THEN msib.source_organization_id
                                                                                                                                      ELSE msib.organization_id
                                                                                                                                   END)
                                                                                                                                , 'IDENTIFIER'
                                                                                                                             )
                                                                                  , hou.organization_id
                                                                                 )
                                 , 1
                                 , 80
                               )
				END ) source_sup_prim_pay_site
	   FROM   mtl_parameters mp
			, org_organization_definitions ood
			, mtl_system_items_b msib
			, hr_operating_units hou
			, per_all_people_f papf
			, mtl_parameters mp_source
	  WHERE 	  mp.organization_id = ood.organization_id
			  AND mp.organization_id = msib.organization_id
			  AND ood.operating_unit = hou.organization_id
			  AND msib.buyer_id = papf.person_id(+)
			  AND TRUNC (SYSDATE) BETWEEN papf.effective_start_date(+)
									  AND  papf.effective_end_date(+)
			  AND msib.source_organization_id = mp_source.organization_id(+)
   ORDER BY   mp.organization_code, msib.segment1;