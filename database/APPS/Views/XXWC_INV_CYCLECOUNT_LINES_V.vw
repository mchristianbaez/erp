--
-- XXWC_INV_CYCLECOUNT_LINES_V  (View)
--

CREATE OR REPLACE FORCE VIEW APPS.XXWC_INV_CYCLECOUNT_LINES_V
(
   CYCLECOUNT_LINE_ID
  ,CYCLECOUNT_HDR_ID
  ,CYCLE_COUNT_NAME
  ,NOTES
  ,PROCESS_TYPE
  ,PROCESS_DATE
  ,CREATION_DATE
  ,CREATED_BY
  ,INV_ORG_ID
  ,PROCESS_STATUS
  ,STORE_LOCATION_CODE
  ,STORE_LOCATION
  ,USER_NAME
)
AS
   SELECT lines.cyclecount_line_id
         ,hdr.cyclecount_hdr_id
         ,hdr.cycle_count_name
         ,hdr.notes
         ,hdr.process_type
         ,hdr.process_date
         ,hdr.creation_date
         ,hdr.created_by
         ,lines.inv_org_id
         ,lines.process_status
         ,org.organization_code store_location_code
         ,org.organization_name store_location
         ,fu.user_name
     FROM xxwc_inv_cyclecount_hdr hdr
         ,xxwc_inv_cyclecount_lines lines
         ,org_organization_definitions org
         ,fnd_user fu
    WHERE     hdr.cyclecount_hdr_id = lines.cyclecount_hdr_id
          AND lines.inv_org_id = org.organization_id
          AND hdr.created_by = fu.user_id;


