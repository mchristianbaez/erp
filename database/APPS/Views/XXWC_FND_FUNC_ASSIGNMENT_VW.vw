--
-- XXWC_FND_FUNC_ASSIGNMENT_VW  (View)
--

CREATE OR REPLACE FORCE VIEW APPS.XXWC_FND_FUNC_ASSIGNMENT_VW
(
   RESPONSIBILITY_NAME
  ,RESPONSIBILITY_KEY
  ,USER_FUNCTION_NAME
  ,FUNCTION_NAME
  ,FUNCTION_ID
  ,USER_MENU_NAME
  ,MENU_NAME
  ,LEVEL_NUM
  ,PATH
  ,CODE_PATH
)
AS
     SELECT DISTINCT r.responsibility_name
                    ,r.responsibility_key
                    ,fft.user_function_name
                    ,ff.function_name
                    ,ff.function_id
                    ,mut.user_menu_name
                    ,m.menu_name
                    ,menus.level_num
                    ,r.responsibility_name || menus.PATH AS PATH
                    ,r.RESPONSIBILITY_KEY || menus.code_path AS code_path
       FROM fnd_form_functions_tl fft
           ,fnd_form_functions ff
           ,fnd_menu_entries me
           ,fnd_menus_tl mut
           ,fnd_menus m
           ,fnd_responsibility_vl r
           ,(    SELECT DISTINCT
                        LEVEL level_num
                       ,me.menu_id
                       ,CONNECT_BY_ROOT me.menu_id AS root_id
                       ,SYS_CONNECT_BY_PATH (metl.user_menu_name, '\') AS PATH
                       ,SYS_CONNECT_BY_PATH (m.menu_name, '\') AS code_path
                   FROM fnd_menu_entries me, fnd_menus_tl metl, fnd_menus m
                  WHERE me.menu_id = metl.menu_id AND me.menu_id = m.menu_id
             START WITH me.menu_id IN
                           (SELECT menu_id
                              FROM fnd_responsibility fr
                                  ,fnd_responsibility_tl frt
                             WHERE     fr.responsibility_id =
                                          frt.responsibility_id
                                   AND fr.responsibility_key LIKE 'XXWC%')
             CONNECT BY PRIOR me.sub_menu_id = me.menu_id) menus
      WHERE     fft.function_id = ff.function_id
            AND ff.function_id = me.function_id
            AND mut.menu_id = menus.menu_id
            AND mut.menu_id = m.menu_id
            AND me.menu_id = mut.menu_id
            AND r.responsibility_key LIKE 'XXWC%'
            -- and r.RESPONSIBILITY_NAME='HDS Senior Buyer - WC'
            AND NVL (r.end_date, CURRENT_DATE) >= CURRENT_DATE
            AND NVL (r.start_date, CURRENT_DATE) <= CURRENT_DATE
            AND menus.root_id = r.menu_id
            AND ff.function_id NOT IN
                   (SELECT frf.action_id
                      FROM fnd_resp_functions frf
                     WHERE     frf.action_id = ff.function_id
                           AND frf.responsibility_id = r.responsibility_id
                           AND frf.rule_type = 'F')
            AND menus.menu_id NOT IN
                   (SELECT frf.action_id
                      FROM fnd_resp_functions frf
                     WHERE     frf.action_id = me.menu_id
                           AND frf.responsibility_id = r.responsibility_id
                           AND frf.rule_type = 'M')
   ORDER BY r.responsibility_name, level_num, mut.user_menu_name;

COMMENT ON TABLE APPS.XXWC_FND_FUNC_ASSIGNMENT_VW IS 'This view recursively lists all functions found in any
menu used by a Responsibility whose code begins with XXWC.
It respects menu and function exclusions. This should only
be run with a tight WHERE clause because the result set can
be large.';



