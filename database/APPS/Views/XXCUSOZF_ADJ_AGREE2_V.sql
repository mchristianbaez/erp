
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUSOZF_ADJ_AGREE2_V" ("MVID", "MV_NAME", "CAL_YEAR", "AGREEMENT", "FISCAL_PERIOD", "PERIOD_ID", "REBATE_TYPE", "YTD_SPEND", "YTD_INC", "CURR_PERIOD_ADJ", "CURR_PERIOD_INC", "CURR_PERIOD_SPEND") AS 
  SELECT mvid,
       mv_name,
       cal_year,
       agreement,
       fiscal_period,
       period_id,
       rebate_type
      ,SUM(ytd_spend) YTD_SPEND
      ,SUM(ytd_inc)   YTD_INC
      ,SUM(cur_adj_inc) CURR_PERIOD_ADJ
      ,SUM(get_curr_inc) CURR_PERIOD_INC
      ,sum(get_cur_spend) CURR_PERIOD_SPEND
FROM(
select SUBSTR(hca.account_number, 0, INSTR(hca.account_number, '~')-1) mvid,
       hp.party_name mv_name,
       qlhv.attribute7 cal_year,
       qlhv.description agreement,
       DECODE(nvl(accr.rebate_type_id,0),'COOP','COOP','COOP_MIN','COOP','REBATE') rebate_type,
       accr.period_id period_id,
       period.name fiscal_period,
       XXCUS_OZF_RPTUTIL_PKG.get_ytd_spend(qlhv.attribute7,accr.period_id,hca.cust_account_id) ytd_spend,
       XXCUS_OZF_RPTUTIL_PKG.get_ytd_inc(accr.period_id,accr.plan_id,accr.rebate_type_id,hca.cust_account_id) ytd_inc,
       XXCUS_OZF_RPTUTIL_PKG.get_cur_adj(accr.period_id,accr.plan_id,accr.rebate_type_id,hca.cust_account_id) cur_adj_inc,
       XXCUS_OZF_RPTUTIL_PKG.get_curr_inc(accr.period_id,accr.plan_id,accr.rebate_type_id,hca.cust_account_id)get_curr_inc,
        XXCUS_OZF_RPTUTIL_PKG.get_cur_spend(qlhv.attribute7,accr.period_id,hca.cust_account_id)get_cur_spend
       
from xxcusozf_accruals_mv accr,
     hz_cust_accounts hca,
     hz_parties hp,
     qp_list_headers_vl qlhv,
     ozf_time_ent_period period
where 1 =1
and grp_mvid         =0
and grp_cal_year     =1
and grp_lob          =1
and grp_period       =0
and grp_qtr          =1
and grp_plan_id      =0
and grp_branch       =1
and grp_year         =1
and grp_adj_type     =1
and grp_rebate_type  =0
and accr.mvid       =hca.cust_account_id
and hca.party_id    =hp.party_id
and hca.account_number like ('%MSTR%')
and accr.plan_id    =qlhv.list_header_id
and accr.period_id  =period.ent_period_id

)
GROUP BY mvid,
         mv_name,
         cal_year,
         period_id,
         fiscal_period,
         agreement,
         rebate_type
order by 3,6;
