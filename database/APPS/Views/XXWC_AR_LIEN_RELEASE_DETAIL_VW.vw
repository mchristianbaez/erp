/* Formatted on 10/1/2014 10:26:21 AM (QP5 v5.254.13202.43190) */
CREATE OR REPLACE FORCE VIEW APPS.XXWC_AR_LIEN_RELEASE_DETAIL_VW
(
   HEADER_ID,
   ORDER_NUMBER,
   ORDERED_DATE,
   CUST_PO_NUMBER,
   ORDER_TYPE,
   SHIP_TO,
   SALESREP_ID,
   SALESREP_NAME,
   SHIP_TO_ORG_ID,
   ADDRESS_ID,
   SITE_NUMBER,
   ORDER_TOTAL,
   SHIP_FROM_ORG_ID,
   ORGANIZATION_CODE,
   ORGANIZATION_NAME,
   FLOW_STATUS_CODE,
   AMOUNT_REMAINING,
   LOCATION
)
AS
   SELECT /******************************************************************************
             NAME:       apps.xxwc_ar_lien_release_detail_vw
             PURPOSE:    View used in the line section of lien release form

             REVISIONS:
             Ver        Date        Author           Description
             ---------  ----------  ---------------  ------------------------------------
             1.0        11/22/2013      shariharan    Initial Version
          ******************************************************************************/
         a.header_id,
          a.order_number,
          a.ordered_date,
          a.cust_po_number,
          a.order_type,
          a.ship_to,
          a.salesrep_id,
          (SELECT name
             FROM ra_salesreps b
            WHERE b.salesrep_id = a.salesrep_id)
             salesrep_name,
          a.ship_to_org_id,
          d.address_id,
          d.site_number,
          apps.oe_oe_totals_summary.prt_order_total (a.header_id) order_total,
          a.ship_from_org_id,
          e.organization_code,
          e.organization_name,
          a.flow_status_code,
          apps.xxwc_ar_lien_release_pkg.get_rem_order_total (a.header_id)
             amount_remaining,
          c.location
     FROM oe_order_headers_v a,
          apps.hz_Cust_site_uses c,
          xxwc_ar_addresses_v d,
          org_organization_definitions e
    WHERE     a.ship_to_org_id = c.site_use_id
          AND c.cust_acct_site_id = d.address_id
          AND e.organization_id = a.ship_from_org_id
          --and a.flow_status_Code not in ('CLOSED','CANCELLED','ENTERED','DRAFT','DRAFT_CUSTOMER_REJECTED','LOST','OFFER_EXPIRED','PENDING_CUSTOMER_ACCEPTANCE')
          AND a.flow_status_Code NOT IN ('CLOSED',
                                         'CANCELLED',
                                         'DRAFT',
                                         'DRAFT_CUSTOMER_REJECTED',
                                         'LOST',
                                         'OFFER_EXPIRED')
          AND a.ordered_date IS NOT NULL;
