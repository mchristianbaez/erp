
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUS_REBT_PRODUCT_V" ("SKU_CD", "INVENTORY_ITEM_ID", "ITEM_DESC", "VNDR_PART_NBR", "UPC", "CATEGORY_ID", "CAT_SEG1", "CAT_SEG2", "BU_NM", "SRC_SYS_NM", "LOB", "MSTR_VNDR_NM", "MSTR_VNDR_CD", "CUST_ACCOUNT_ID") AS 
  select distinct r.sku_cd,
                i.inventory_item_id,
                i.attribute6,
                i.attribute3,
                i.attribute5,
                c.category_id,
                cb.segment1,
                cb.segment2,
                r.bu_nm,
                r.src_sys_nm,
                r.geo_loc_hier_lob_nm,
                p.party_name,
                hca.attribute2,
                hca.cust_account_id
  from inv.mtl_system_items_b               i,
       inv.mtl_item_categories              c,
       inv.mtl_categories_b                 cb,
       xxcus.xxcus_rebate_recpt_history_tbl r,
       ar.hz_parties                        p,
       ar.hz_cust_accounts                  hca
 where i.organization_id = 84
   and i.enabled_flag = 'Y'
   and i.inventory_item_id = c.inventory_item_id
   and c.organization_id = 84
   and c.category_set_id = 1100000041
   and c.category_id = cb.category_id
   and i.description = r.sku_cd || '~' || r.bu_nm
   and r.rebt_vndr_cd = p.attribute2
   and p.attribute1 = 'HDS_MVID'
   and p.party_id = hca.party_id
   and hca.attribute1 = 'HDS_MVID';
