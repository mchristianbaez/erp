/*******************************************************************************************************************
File Name: XXWC_EGO_INTRNAL_AT_ATTR_AGV.sql

TYPE:     VIEW

Description: 

VERSION DATE          AUTHOR(S)       DESCRIPTION
------- -----------   --------------- -----------------------------------------------------------------------------
1.0     01-MAR-2015   Shankar Vanga   TMS#20150126-00044  - Create Single Item Workflow Tool
                                      
********************************************************************************************************************/

CREATE OR REPLACE FORCE VIEW APPS.XXWC_EGO_INTRNAL_AT_ATTR_AGV
(
   EXTENSION_ID,
   INVENTORY_ITEM_ID,
   ORGANIZATION_ID,
   DATA_LEVEL_ID,
   PK1_VALUE,
   PK2_VALUE,
   REVISION_ID,
   XXWC_VENDOR_NUMBER_ATTR,
   XXWC_VENDOR_NUMBER_ATTR_DISP,
   XXWC_COUNTRY_ORIGIN_ATTR,
   XXWC_COUNTRY_ORIGIN_ATTR_DISP,
   XXWC_ROUNDING_FACTOR,
   SEARCH_T1,
   SEARCH_T2,
   SEARCH_T3,
   XXWC_SUGGESTED_RETAIL_PRICE,
   XXWC_RETAIL_UOM_ATTR,
   XXWC_FULFILLMENT_ATTR,
   XXWC_FULFILLMENT_ATTR_DISP,
   XXWC_ITEM_LEVEL_ATTR,
   XXWC_ITEM_LEVEL_ATTR_DISP,
   XXWC_CONFLICT_MINERALS_ATTR,
   XXWC_CONFLICT_MINERALS_A_DISP
)
AS
   SELECT EXTENSION_ID,
          INVENTORY_ITEM_ID,
          ORGANIZATION_ID,
          DATA_LEVEL_ID,
          PK1_VALUE,
          PK2_VALUE,
          REVISION_ID,
          C_EXT_ATTR1 XXWC_VENDOR_NUMBER_ATTR,
          (SELECT DISTINCT vendor_info
             FROM XXWC_EGO_SUPPLIERS_ACTV_DCT_VW
            WHERE 1 = 1 AND vendor_number = C_EXT_ATTR1)
             XXWC_VENDOR_NUMBER_ATTR_DISP,
          C_EXT_ATTR2 XXWC_COUNTRY_ORIGIN_ATTR,
          (SELECT DISTINCT meaning
             FROM FND_LOOKUP_VALUES
            WHERE     1 = 1
                  AND LOOKUP_TYPE = 'XXWC_TERRITORIES'
                  AND ENABLED_FLAG = 'Y'
                  AND NVL (END_DATE_ACTIVE, SYSDATE) >= SYSDATE
                  AND lookup_code = C_EXT_ATTR2)
             XXWC_COUNTRY_ORIGIN_ATTR_DISP,
          N_EXT_ATTR1 XXWC_ROUNDING_FACTOR,
          C_EXT_ATTR3 SEARCH_T1,
          C_EXT_ATTR4 SEARCH_T2,
          C_EXT_ATTR5 SEARCH_T3,
          C_EXT_ATTR9 XXWC_SUGGESTED_RETAIL_PRICE,
          C_EXT_ATTR10 XXWC_RETAIL_UOM_ATTR,
          C_EXT_ATTR7 XXWC_FULFILLMENT_ATTR,
          (SELECT DISTINCT DISPLAY_NAME
             FROM EGO_VALUE_SET_VALUES_V
            WHERE     VALUE_SET_ID = 1016979
                  AND ENABLED_CODE = 'Y'
                  AND (NVL (START_DATE, SYSDATE - 1) < SYSDATE)
                  AND (NVL (END_DATE, SYSDATE + 1) > SYSDATE)
                  AND INTERNAL_NAME = C_EXT_ATTR7)
             XXWC_FULFILLMENT_ATTR_DISP,
          C_EXT_ATTR6 XXWC_ITEM_LEVEL_ATTR,
          (SELECT DISTINCT DISPLAY_NAME
             FROM EGO_VALUE_SET_VALUES_V
            WHERE     VALUE_SET_ID = 1016978
                  AND ENABLED_CODE = 'Y'
                  AND (NVL (START_DATE, SYSDATE - 1) < SYSDATE)
                  AND (NVL (END_DATE, SYSDATE + 1) > SYSDATE)
                  AND INTERNAL_NAME = C_EXT_ATTR6)
             XXWC_ITEM_LEVEL_ATTR_DISP,
          C_EXT_ATTR11 XXWC_CONFLICT_MINERALS_ATTR,
          (SELECT DISTINCT DISPLAY_NAME
             FROM EGO_VALUE_SET_VALUES_V
            WHERE     VALUE_SET_ID = 1016977
                  AND ENABLED_CODE = 'Y'
                  AND (NVL (START_DATE, SYSDATE - 1) < SYSDATE)
                  AND (NVL (END_DATE, SYSDATE + 1) > SYSDATE)
                  AND INTERNAL_NAME = C_EXT_ATTR11)
             XXWC_CONFLICT_MINERALS_A_DISP
     FROM EGO_MTL_SY_ITEMS_EXT_VL
    WHERE attr_group_id = 861;