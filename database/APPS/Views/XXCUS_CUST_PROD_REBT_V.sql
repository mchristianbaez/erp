
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUS_CUST_PROD_REBT_V" ("CUST_ACCT_ID") AS 
  select DISTINCT to_number(q.qualifier_attr_value) cust_acct_id
  from QP_QUALIFIERS q
 where q.qualifier_context = 'SOLD_BY'
   and q.QUALIFIER_ATTRIBUTE = 'QUALIFIER_ATTRIBUTE2'
   and q.active_flag = 'Y'
   and NVL(q.end_date_active, sysdate + 30) > sysdate
   and list_header_id in
       (select list_header_id
          from QP_PRICING_ATTRIBUTES a
         where a.list_header_id = q.list_header_id
           and a.product_attribute_context = 'ITEM'
           and a.product_attribute = 'PRICING_ATTRIBUTE2');
