
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUSOZF_PAYMENT_TPLATE_VW" ("MVID", "CUST_ID", "MV_NAME", "CAL_YEAR", "AGREEMENT", "REBATE_TYPE", "ELE_INVOICE", "CTI_INVOICE", "FM_INVOICE", "UTL_INVOICE", "CB_INVOICE", "WC_INVOICE", "WW_INVOICE", "IPVF_INVOICE", "PLM_INVOICE", "RR_INVOICE", "CAN_INVOICE", "CORP_PAYMENT", "ELE_PAYMENT", "CTI_PAYMENT", "FM_PAYMENT", "UTL_PAYMENT", "CB_PAYMENT", "WC_PAYMENT", "WW_PAYMENT", "IPVF_PAYMENT", "PLM_PAYMENT", "RR_PAYMENT", "CAN_PAYMENT", "ELE_BALANCE", "CTI_BALANCE", "FM_BALANCE", "UTL_BALANCE", "CB_BALANCE", "WC_BALANCE", "WW_BALANCE", "IPVF_BALANCE", "PLM_BALANCE", "RR_BALANCE", "CAN_BALANCE", "CONTRACT_NOTES") AS 
  SELECT mvid,
       cust_id,
       mv_name,
       cal_year,
       agreement,
       rebate_type
      ,XXCUS_OZF_RPTUTIL_PKG.get_invoice_amt(agreement_id,cust_id,'ELECTRICAL') ELE_INVOICE
      ,XXCUS_OZF_RPTUTIL_PKG.get_invoice_amt(agreement_id,cust_id,'CREATIVE TOUCH INTERIORS') CTI_INVOICE
      ,XXCUS_OZF_RPTUTIL_PKG.get_invoice_amt(agreement_id,cust_id,'FACILITIES MAINTENANCE') FM_INVOICE
      ,XXCUS_OZF_RPTUTIL_PKG.get_invoice_amt(agreement_id,cust_id,'UTILITIES') UTL_INVOICE
      ,XXCUS_OZF_RPTUTIL_PKG.get_invoice_amt(agreement_id,cust_id,'CROWN BOLT') CB_INVOICE
      ,XXCUS_OZF_RPTUTIL_PKG.get_invoice_amt(agreement_id,cust_id,'WHITE CAP') WC_INVOICE
      ,XXCUS_OZF_RPTUTIL_PKG.get_invoice_amt(agreement_id,cust_id,'WATERWORKS') WW_INVOICE
      ,XXCUS_OZF_RPTUTIL_PKG.get_invoice_amt(agreement_id,cust_id,'INDUSTRIAL PVF') IPVF_INVOICE
      ,XXCUS_OZF_RPTUTIL_PKG.get_invoice_amt(agreement_id,cust_id,'PLUMBING null') PLM_INVOICE
      ,XXCUS_OZF_RPTUTIL_PKG.get_invoice_amt(agreement_id,cust_id,'REPAIR null') RR_INVOICE
      ,XXCUS_OZF_RPTUTIL_PKG.get_invoice_amt(agreement_id,cust_id,'HD SUPPLY CANADA') CAN_INVOICE
      ,XXCUS_OZF_RPTUTIL_PKG.get_payment_amt(agreement_id,cust_id,'CORPORATE') CORP_PAYMENT
      ,XXCUS_OZF_RPTUTIL_PKG.get_payment_amt(agreement_id,cust_id,'ELECTRICAL') ELE_PAYMENT
      ,XXCUS_OZF_RPTUTIL_PKG.get_payment_amt(agreement_id,cust_id,'CREATIVE TOUCH INTERIORS') CTI_PAYMENT
      ,XXCUS_OZF_RPTUTIL_PKG.get_payment_amt(agreement_id,cust_id,'FACILITIES MAINTENANCE') FM_PAYMENT
      ,XXCUS_OZF_RPTUTIL_PKG.get_payment_amt(agreement_id,cust_id,'UTILITIES') UTL_PAYMENT
      ,XXCUS_OZF_RPTUTIL_PKG.get_payment_amt(agreement_id,cust_id,'CROWN BOLT') CB_PAYMENT
      ,XXCUS_OZF_RPTUTIL_PKG.get_payment_amt(agreement_id,cust_id,'WHITE CAP') WC_PAYMENT
      ,XXCUS_OZF_RPTUTIL_PKG.get_payment_amt(agreement_id,cust_id,'WATERWORKS') WW_PAYMENT
      ,XXCUS_OZF_RPTUTIL_PKG.get_payment_amt(agreement_id,cust_id,'INDUSTRIAL PVF') IPVF_PAYMENT
      ,XXCUS_OZF_RPTUTIL_PKG.get_payment_amt(agreement_id,cust_id,'PLUMBING null') PLM_PAYMENT
      ,XXCUS_OZF_RPTUTIL_PKG.get_payment_amt(agreement_id,cust_id,'REPAIR null') RR_PAYMENT
      ,XXCUS_OZF_RPTUTIL_PKG.get_payment_amt(agreement_id,cust_id,'HD SUPPLY CANADA') CAN_PAYMENT
      ,XXCUS_OZF_RPTUTIL_PKG.get_balance_amt(agreement_id,cust_id,'ELECTRICAL') ELE_BALANCE
      ,XXCUS_OZF_RPTUTIL_PKG.get_balance_amt(agreement_id,cust_id,'CREATIVE TOUCH INTERIORS') CTI_BALANCE
      ,XXCUS_OZF_RPTUTIL_PKG.get_balance_amt(agreement_id,cust_id,'FACILITIES MAINTENANCE') FM_BALANCE
      ,XXCUS_OZF_RPTUTIL_PKG.get_balance_amt(agreement_id,cust_id,'UTILITIES') UTL_BALANCE
      ,XXCUS_OZF_RPTUTIL_PKG.get_balance_amt(agreement_id,cust_id,'CROWN BOLT') CB_BALANCE
      ,XXCUS_OZF_RPTUTIL_PKG.get_balance_amt(agreement_id,cust_id,'WHITE CAP') WC_BALANCE
      ,XXCUS_OZF_RPTUTIL_PKG.get_balance_amt(agreement_id,cust_id,'WATERWORKS') WW_BALANCE
      ,XXCUS_OZF_RPTUTIL_PKG.get_balance_amt(agreement_id,cust_id,'INDUSTRIAL PVF') IPVF_BALANCE
      ,XXCUS_OZF_RPTUTIL_PKG.get_balance_amt(agreement_id,cust_id,'PLUMBING null') PLM_BALANCE
      ,XXCUS_OZF_RPTUTIL_PKG.get_balance_amt(agreement_id,cust_id,'REPAIR null') RR_BALANCE
      ,XXCUS_OZF_RPTUTIL_PKG.get_balance_amt(agreement_id,cust_id,'HD SUPPLY CANADA') CAN_BALANCE
      ,contract_notes
FROM(
select distinct hca.attribute2 mvid ,
       hca.cust_account_id cust_id,
       hp.party_name mv_name,
       qlhv.attribute7 cal_year,
       qlhv.description agreement,
       qlhv.list_header_id agreement_id,
       DECODE(med.description,'COOP','COOP','COOP_MIN','COOP','REBATE') rebate_type ,
       oo.status_code,
       oo.offer_code,
       ofa.description contract_notes
from qp_list_headers_vl qlhv ,
     ozf_offers oo,
     ams_media_vl med,
     qp_qualifiers qq,
     hz_parties hp,
     hz_cust_accounts hca,
     ozf_act_budgets oab,
     ozf_funds_all_vl ofa
where qlhv.list_header_id=qq.list_header_id
  and qq.qualifier_context='SOLD_BY'
  and oo.qp_list_header_id=qlhv.list_header_id
  and oo.activity_media_id= med.media_id
  and hca.cust_account_id=TO_NUMBER(qq.qualifier_attr_value)
  and qlhv.list_header_id=oab.act_budget_used_by_id
  and oab.budget_source_type='FUND'
  and oab.status_code='APPROVED'
  and oab.arc_act_budget_used_by='OFFR'
  and ofa.fund_id=oab.budget_source_id
  and hca.party_id=hp.party_id
  and oo.status_code='ACTIVE'
  and qq.qualifier_attribute='QUALIFIER_ATTRIBUTE2'
  and qq.list_line_id = -1)
GROUP BY mvid,
         cust_id,
         mv_name,
         cal_year,
         agreement,
         agreement_id,
         contract_notes,
         rebate_type;
