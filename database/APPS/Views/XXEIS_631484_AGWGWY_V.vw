CREATE OR REPLACE FORCE VIEW APPS.XXEIS_631484_AGWGWY_V
(
   PARTY_CREATION_DATE,
   ACCOUNT_CREATION_DATE,
   ACCOUNT_NUMBER,
   CLEANSED_NAME,
   ACCOUNT_NAME,
   PROFILE_CLASS_NAME,
   PREDOMINANT_TRADE,
   CUSTOMER__CLASSIFICATION,
   SALES_CHANNEL,
   BILLING_ADDRESS_1,
   BILLING_ADDRESS_2,
   BILLING_ADDRESS_3,
   BILLING_ADDRESS_4,
   BILLING_CITY,
   BILLING_STATE,
   BILLING_ZIP,
   SIC_CODE,
   ACCOUNT_MANAGER_NAME,
   EEID,
   PHONE_NUMBER,
   EMAIL_ADDRESS,
   YARD_LIMIT
)
AS
   SELECT hp.creation_date Party_Creation_Date,
          hca.creation_date Account_Creation_Date,
          hca.account_number Account_Number,
          TRIM (
             REPLACE (
                REPLACE (
                   REPLACE (
                      REPLACE (REPLACE (hca.account_name, 'CASH', ''),
                               'cash',
                               ''),
                      '%',
                      ' '),
                   '/',
                   ''),
                'Cash',
                ''))
             Cleansed_Name,
          hca.account_name Account_Name,
          hcpc.name Profile_Class_Name,
          hca.attribute9 Predominant_Trade,
          hca.customer_class_code Customer__Classification,
          hca.sales_channel_code Sales_Channel,
          hl.address1 Billing_Address_1,
          hl.address2 Billing_Address_2,
          hl.address3 Billing_Address_3,
          hl.address4 Billing_Address_4,
          hl.city Billing_City,
          hl.state Billing_State,
          hl.postal_code Billing_Zip,
          hop.sic_code SIC_Code,
          jrdv.resource_name Account_Manager_Name,
          jrdv.source_number EEID,
          hcpts.raw_phone_number Phone_Number,
          hcpts2.email_address Email_Address,
          ---- 7/23/2014 diane added start 20140220-00170
          (SELECT b.overall_credit_limit
             FROM ar.hz_cust_site_uses_all a,
                  ar.hz_cust_profile_amts b,
                  ar.hz_cust_acct_sites_all c
            WHERE     1 = 1
                  AND c.cust_account_id = hca.cust_account_id
                  AND a.status = 'A'
                  AND c.status = 'A'
                  AND c.cust_acct_site_id = a.cust_acct_site_id
                  AND c.cust_account_id = b.cust_account_id
                  AND a.site_use_id = b.site_use_id
                  AND a.attribute1 = 'YARD'
                  AND ROWNUM = 1)
             YARD_LIMIT       --- 7/23/2014 diane added end TMS 20140220-00170
     FROM ar.hz_organization_profiles hop,
          ar.hz_party_sites hps,
          ar.hz_locations hl,
          ar.hz_customer_profiles hcp,
          ar.hz_cust_profile_classes hcpc,
          ar.hz_parties hp
          INNER JOIN ar.hz_cust_accounts hca ON hp.party_id = hca.party_id
          INNER JOIN ar.hz_cust_acct_sites_all hcasa
             ON hca.cust_account_id = hcasa.cust_account_id
          INNER JOIN ar.hz_cust_site_uses_all hcsua
             ON hcasa.cust_acct_site_id = hcsua.cust_acct_site_id
          LEFT OUTER JOIN apps.jtf_rs_salesreps jrs
             ON jrs.salesrep_id = hcsua.primary_salesrep_id
          LEFT OUTER JOIN apps.jtf_rs_defresources_v jrdv
             ON jrs.resource_id = jrdv.resource_id
          LEFT OUTER JOIN ar.hz_contact_points hcpts
             ON     hcpts.owner_table_id = hp.party_id
                AND hcpts.contact_point_type = 'PHONE'
                AND hcpts.primary_flag = 'Y'
                AND hcpts.status = 'A'
                AND hcpts.owner_table_name = 'HZ_PARTIES'
          LEFT OUTER JOIN ar.hz_contact_points hcpts2
             ON     hcpts2.owner_table_id = hp.party_id
                AND hcpts2.contact_point_type = 'EMAIL'
                AND hcpts2.primary_flag = 'Y'
                AND hcpts2.status = 'A'
                AND hcpts2.owner_table_name = 'HZ_PARTIES'
    WHERE     hop.party_id = hp.party_id
          AND hcasa.party_site_id = hps.party_site_id
          AND hps.location_id = hl.location_id
          AND hca.cust_account_id = hcp.cust_account_id
          AND hcp.profile_class_id = hcpc.profile_class_id
          AND hcsua.site_use_code = 'BILL_TO'
          AND hcsua.primary_flag = 'Y'
          AND hcp.site_use_id IS NULL
          AND (   jrs.org_id = '162'
               OR (jrs.org_id IS NULL AND hcsua.primary_salesrep_id IS NULL))
          AND hop.effective_end_date IS NULL
          AND hcpc.name NOT IN ('Intercompany Customers', 'WC Branches')
          AND hcsua.org_id = '162'
          AND TRUNC (hca.creation_date) > TRUNC (SYSDATE) - 7;