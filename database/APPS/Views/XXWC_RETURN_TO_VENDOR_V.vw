CREATE OR REPLACE FORCE VIEW APPS.XXWC_RETURN_TO_VENDOR_V
(
   SEGMENT1,
   VENDOR_NAME,
   DISPLAYED_FIELD,
   ATTRIBUTE1,
   INVOICE_AMOUNT
)
AS
   SELECT DISTINCT pv.segment1 segment1,
                   pv.vendor_name vendor_name,
                   alc1.displayed_field displayed_field,
                   ail.attribute1,
                   ai.invoice_amount  --   --Added by Maha for tms#20131016-00468
     FROM apps.ap_invoices ai,
          ap_suppliers pv,
          apps.ap_supplier_sites pvs,
          ap_lookup_codes alc1,
          apps.ap_invoice_lines ail
    WHERE     pv.vendor_id = ai.vendor_id
          AND pv.vendor_id = pvs.vendor_id
          AND pvs.vendor_site_id = ai.vendor_site_id
          AND alc1.lookup_type = 'INVOICE TYPE'
          AND alc1.lookup_code = ai.invoice_type_lookup_code
          AND ail.invoice_id = ai.invoice_id
          -- AND alc1.displayed_field = 'Credit Memo'            --commented and added below for Task id 20131016-00468
          AND alc1.displayed_field IN ('Credit Memo', 'Debit Memo')
