create or replace view apps.xxwc_inv_cc_blockout_dates_vw as            
SELECT   BLOCKOUT_DATE_ID,
            INV_ORG_ID,
            START_DATE,
            END_DATE,
            TYPE,
            dat.LAST_UPDATE_DATE,
            dat.LAST_UPDATED_BY,
            dat.CREATION_DATE,
            dat.CREATED_BY,
            dat.LAST_UPDATE_LOGIN,
            dat.OBJECT_VERSION_NUMBER,
            org.ORGANIZATION_CODE,
            org.ORGANIZATION_NAME,
            lkp.meaning type_meaning
     FROM   apps.xxwc_inv_cc_blockout_dates dat,
            apps.org_organization_definitions org,
            apps.fnd_lookup_values_vl lkp
    WHERE   dat.inv_org_id = org.organization_id
            AND lkp.lookup_code(+) = dat.TYPE --added 1/11/2013 by Lee Spitzer to fix records without types
            AND lkp.lookup_type(+) = 'XXWC_BLOCKOUT_DATE_TYPES'  --added 1/11/2013 by Lee Spitzer to fix records without types
            AND lkp.enabled_flag(+) = 'Y'  --added 1/11/2013 by Lee Spitzer to fix records without types
ORDER BY START_DATE DESC; --added 1/24/2013 by Lee Spitzer to fix sorting