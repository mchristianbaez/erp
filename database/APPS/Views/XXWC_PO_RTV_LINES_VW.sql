/*************************************************************************
  $Header XXWC_PO_RTV_LINES_VW $
  Module Name: XXWC_PO_RTV_LINES_VW

  PURPOSE: Return to Vendor Transactions Line Details for Form

  REVISIONS:
  Ver        Date        Author                Description
  ---------  ----------  ------------------    ----------------
  1.0        04/10/2014  Manjula Chellappan    Initial Version
**************************************************************************/
CREATE OR REPLACE FORCE VIEW APPS.XXWC_PO_RTV_LINES_VW
(
   RTV_LINE_ID,
   RTV_HEADER_ID,
   INVENTORY_ITEM_ID,
   LINE_NUMBER,
   ITEM_NUMBER,
   SUBINVENTORY,
   RETURN_QUANTITY,
   UOM_CODE,
   RETURN_UNIT_PRICE,
   RETURN_REASON,
   NOTES,
   PROCESSED_FLAG,
   CREATED_BY,
   CREATION_DATE,
   LAST_UPDATED_BY,
   LAST_UPDATE_DATE,
   LAST_UPDATE_LOGIN,
   ATTRIBUTE1,
   ATTRIBUTE2,
   ATTRIBUTE3,
   ATTRIBUTE4,
   ATTRIBUTE5,
   ATTRIBUTE6,
   ATTRIBUTE7,
   ATTRIBUTE8,
   ATTRIBUTE9,
   ATTRIBUTE10,
   ATTRIBUTE11,
   ATTRIBUTE12,
   ATTRIBUTE13,
   ATTRIBUTE14,
   ATTRIBUTE15
)
AS
SELECT a.rtv_line_id,
          a.rtv_header_id,
          a.inventory_item_id,
          a.line_number,
          (SELECT segment1
             FROM mtl_system_items_b
            WHERE     inventory_item_id = a.inventory_item_id
                  AND organization_id = 222)
             item_number,
          a.subinventory,
          a.return_quantity,
          a.uom_code,
          a.return_unit_price,
          a.return_reason,
          a.notes,
          a.processed_flag,
          a.created_by,
          a.creation_date,
          a.last_updated_by,
          a.last_update_date,
          a.last_update_login,
          a.attribute1,
          a.attribute2,
          a.attribute3,
          a.attribute4,
          a.attribute5,
          a.attribute6,
          a.attribute7,
          a.attribute8,
          a.attribute9,
          a.attribute10,
          a.attribute11,
          a.attribute12,
          a.attribute13,
          a.attribute14,
          a.attribute15
     FROM xxwc.xxwc_po_rtv_lines_tbl a
    WHERE 1 = 1
/
