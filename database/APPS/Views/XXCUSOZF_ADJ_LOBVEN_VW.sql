
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUSOZF_ADJ_LOBVEN_VW" ("MVID", "MV_NAME", "CAL_YEAR", "FISCAL_PERIOD", "PERIOD_ID", "YTD_SPEND", "YTD_INCOME", "CURR_SPEND", "CURR_INCOME", "ELECTRICAL_ACCR", "CREATIVE_TOUCH_INTERIORS_ACCR", "FACILITIES_MAINTENANCE_ACCR", "UTILITIES_ACCR", "CROWN_BOLT_ACCR", "WHITE_CAP_ACCR", "WATERWORKS_ACCR", "INDUSTRIAL_PVF_ACCR", "PLUMBING_HVAC_ACCR", "REPAIR_REMODEL_ACCR", "HD_SUPPLY_CANADA_ACCR", "ELECTRICAL_ADJ", "CREATIVE_TOUCH_INTERIORS_ADJ", "FACILITIES_MAINTENANCE_ADJ", "UTILITIES_ADJ", "CROWN_BOLT_ADJ", "WHITE_CAP_ADJ", "WATERWORKS_ADJ", "INDUSTRIAL_PVF_ADJ", "PLUMBING_HVAC_ADJ", "REPAIR_REMODEL_ADJ", "HD_SUPPLY_CANADA_ADJ") AS 
  SELECT mvid,
       mv_name,
       cal_year,
       fiscal_period,
       period_id,
       SUM(ytd_spend)  ytd_spend
      ,SUM(ytd_inc)    ytd_income
      ,SUM(curr_spend) curr_spend
      ,SUM(curr_inc)   curr_income
      ,SUM(ELE_ACCR) "ELECTRICAL_ACCR"
      ,SUM(CTI_ACCR) "CREATIVE_TOUCH_INTERIORS_ACCR"
      ,SUM(FM_ACCR)  "FACILITIES_MAINTENANCE_ACCR"
      ,SUM(UTL_ACCR) "UTILITIES_ACCR"
      ,SUM(CB_ACCR)  "CROWN_BOLT_ACCR"
      ,SUM(WC_ACCR)  "WHITE_CAP_ACCR"
      ,SUM(WW_ACCR)  "WATERWORKS_ACCR"
      ,SUM(IPVF_ACCR) "INDUSTRIAL_PVF_ACCR"
      ,SUM(PLM_ACCR)  "PLUMBING_HVAC_ACCR"
      ,SUM(RR_ACCR)   "REPAIR_REMODEL_ACCR"
      ,SUM(CAN_ACCR)  "HD_SUPPLY_CANADA_ACCR"
      ,SUM(ELE_ADJ) "ELECTRICAL_ADJ"
      ,SUM(CTI_ADJ) "CREATIVE_TOUCH_INTERIORS_ADJ"
      ,SUM(FM_ADJ)  "FACILITIES_MAINTENANCE_ADJ"
      ,SUM(UTL_ADJ) "UTILITIES_ADJ"
      ,SUM(CB_ADJ)  "CROWN_BOLT_ADJ"
      ,SUM(WC_ADJ)  "WHITE_CAP_ADJ"
      ,SUM(WW_ADJ)  "WATERWORKS_ADJ"
      ,SUM(IPVF_ADJ) "INDUSTRIAL_PVF_ADJ"
      ,SUM(PLM_ADJ)  "PLUMBING_HVAC_ADJ"
      ,SUM(RR_ADJ)   "REPAIR_REMODEL_ADJ"
      ,SUM(CAN_ADJ)  "HD_SUPPLY_CANADA_ADJ"
FROM
(SELECT hca.account_number mvid,
       hp.party_name mv_name,
       qlhv.attribute7 cal_year,
       DECODE(accr.rebate_type_id,'COOP','COOP','COOP_MIN','COOP','REBATE') rebate_type,
       accr.period_id period_id,
       period.name fiscal_period,
       XXCUS_OZF_RPTUTIL_PKG.get_ytd_spend(qlhv.attribute7,accr.period_id,hca.cust_account_id)-XXCUS_OZF_RPTUTIL_PKG.get_cur_spend(qlhv.attribute7,accr.period_id,hca.cust_account_id) ytd_spend,
       XXCUS_OZF_RPTUTIL_PKG.get_cur_spend(qlhv.attribute7,accr.period_id,hca.cust_account_id) curr_spend,
       XXCUS_OZF_RPTUTIL_PKG.get_ytd_inc(accr.period_id,accr.plan_id,accr.rebate_type_id,hca.cust_account_id) ytd_inc,
       XXCUS_OZF_RPTUTIL_PKG.get_curr_inc(accr.period_id,accr.plan_id,accr.rebate_type_id,hca.cust_account_id) curr_inc,
       XXCUS_OZF_RPTUTIL_PKG.get_lob_accr(accr.period_id,accr.plan_id,'ELECTRICAL',accr.rebate_type_id,hca.cust_account_id) ELE_ACCR
      ,XXCUS_OZF_RPTUTIL_PKG.get_lob_accr(accr.period_id,accr.plan_id,'CREATIVE TOUCH INTERIORS',accr.rebate_type_id,hca.cust_account_id) CTI_ACCR
      ,XXCUS_OZF_RPTUTIL_PKG.get_lob_accr(accr.period_id,accr.plan_id,'FACILITIES MAINTENANCE',accr.rebate_type_id,hca.cust_account_id) FM_ACCR
      ,XXCUS_OZF_RPTUTIL_PKG.get_lob_accr(accr.period_id,accr.plan_id,'UTILITIES',accr.rebate_type_id,hca.cust_account_id) UTL_ACCR
      ,XXCUS_OZF_RPTUTIL_PKG.get_lob_accr(accr.period_id,accr.plan_id,'CROWN BOLT',accr.rebate_type_id,hca.cust_account_id) CB_ACCR
      ,XXCUS_OZF_RPTUTIL_PKG.get_lob_accr(accr.period_id,accr.plan_id,'WHITE CAP',accr.rebate_type_id,hca.cust_account_id) WC_ACCR
      ,XXCUS_OZF_RPTUTIL_PKG.get_lob_accr(accr.period_id,accr.plan_id,'WATERWORKS',accr.rebate_type_id,hca.cust_account_id) WW_ACCR
      ,XXCUS_OZF_RPTUTIL_PKG.get_lob_accr(accr.period_id,accr.plan_id,'INDUSTRIAL PVF',accr.rebate_type_id,hca.cust_account_id) IPVF_ACCR
      ,XXCUS_OZF_RPTUTIL_PKG.get_lob_accr(accr.period_id,accr.plan_id,'PLUMBING & HVAC',accr.rebate_type_id,hca.cust_account_id) PLM_ACCR
      ,XXCUS_OZF_RPTUTIL_PKG.get_lob_accr(accr.period_id,accr.plan_id,'REPAIR & REMODEL',accr.rebate_type_id,hca.cust_account_id) RR_ACCR
      ,XXCUS_OZF_RPTUTIL_PKG.get_lob_accr(accr.period_id,accr.plan_id,'HD SUPPLY CANADA',accr.rebate_type_id,hca.cust_account_id) CAN_ACCR
      ,XXCUS_OZF_RPTUTIL_PKG.get_lob_adj(accr.period_id,accr.plan_id,'ELECTRICAL',accr.rebate_type_id,hca.cust_account_id) ELE_ADJ
      ,XXCUS_OZF_RPTUTIL_PKG.get_lob_adj(accr.period_id,accr.plan_id,'CREATIVE TOUCH INTERIORS',accr.rebate_type_id,hca.cust_account_id) CTI_ADJ
      ,XXCUS_OZF_RPTUTIL_PKG.get_lob_adj(accr.period_id,accr.plan_id,'FACILITIES MAINTENANCE',accr.rebate_type_id,hca.cust_account_id) FM_ADJ
      ,XXCUS_OZF_RPTUTIL_PKG.get_lob_adj(accr.period_id,accr.plan_id,'UTILITIES',accr.rebate_type_id,hca.cust_account_id) UTL_ADJ
      ,XXCUS_OZF_RPTUTIL_PKG.get_lob_adj(accr.period_id,accr.plan_id,'CROWN BOLT',accr.rebate_type_id,hca.cust_account_id) CB_ADJ
      ,XXCUS_OZF_RPTUTIL_PKG.get_lob_adj(accr.period_id,accr.plan_id,'WHITE CAP',accr.rebate_type_id,hca.cust_account_id) WC_ADJ
      ,XXCUS_OZF_RPTUTIL_PKG.get_lob_adj(accr.period_id,accr.plan_id,'WATERWORKS',accr.rebate_type_id,hca.cust_account_id) WW_ADJ
      ,XXCUS_OZF_RPTUTIL_PKG.get_lob_adj(accr.period_id,accr.plan_id,'INDUSTRIAL PVF',accr.rebate_type_id,hca.cust_account_id) IPVF_ADJ
      ,XXCUS_OZF_RPTUTIL_PKG.get_lob_adj(accr.period_id,accr.plan_id,'PLUMBING & HVAC',accr.rebate_type_id,hca.cust_account_id) PLM_ADJ
      ,XXCUS_OZF_RPTUTIL_PKG.get_lob_adj(accr.period_id,accr.plan_id,'REPAIR & REMODEL',accr.rebate_type_id,hca.cust_account_id) RR_ADJ
      ,XXCUS_OZF_RPTUTIL_PKG.get_lob_adj(accr.period_id,accr.plan_id,'HD SUPPLY CANADA',accr.rebate_type_id,hca.cust_account_id) CAN_ADJ
  FROM xxcusozf_accruals_mv accr,
       hz_cust_accounts hca,
       hz_parties hp,
       ozf_time_ent_period period,
       qp_list_headers_vl qlhv
WHERE 1=1
AND accr.mvid=hca.cust_account_id
AND accr.plan_id=qlhv.list_header_id
AND hca.party_id=hp.party_id
AND accr.period_id=period.ent_period_id
AND grp_mvid=0
AND grp_period=0
AND grp_rebate_type=0
AND grp_plan_id=0
AND grp_cal_year=1
AND grp_lob=1
AND grp_qtr=1
AND grp_branch=1
AND grp_year=1
AND grp_adj_type=1)
GROUP BY mvid,
         mv_name,
         cal_year,
         fiscal_period,
         period_id
;
