
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUS_PAM_PURCHASE_LOB_V" ("MVID", "CUST_ID", "MV_NAME", "LOB_NAME", "CAL_YEAR", "MONTH_ID", "MONTH_NAME", "PUR_AMOUNT") AS 
  SELECT HCA.ATTRIBUTE2 MVID,
            HCA.CUST_ACCOUNT_ID CUST_ID,
            HP.PARTY_NAME MV_NAME,
            HP1.PARTY_NAME LOB_NAME,
            PUR.CALENDAR_YEAR CAL_YEAR,
            TO_CHAR (SUBSTR (PUR.CALENDAR_PERIOD, 6, 2), '99') MONTH_ID,
            DECODE (SUBSTR (PUR.CALENDAR_PERIOD, 6, 2),
                    '01', 'Jan',
                    '02', 'Feb',
                    '03', 'Mar',
                    '04', 'Apr',
                    '05', 'May',
                    '06', 'Jun',
                    '07', 'Jul',
                    '08', 'Aug',
                    '09', 'Sep',
                    '10', 'Oct',
                    '11', 'Nov',
                    'Dec')
               MONTH_NAME,
            SUM (PUR.TOTAL_PURCHASES) PUR_AMOUNT
       FROM APPS.XXCUSOZF_PURCHASES_MV PUR,
            APPS.HZ_CUST_ACCOUNTS HCA,
            APPS.HZ_PARTIES HP,
            APPS.HZ_PARTIES HP1
      WHERE     1 = 1
            AND PUR.MVID = HCA.CUST_ACCOUNT_ID
            AND HCA.PARTY_ID = HP.PARTY_ID
            AND PUR.LOB_ID = HP1.PARTY_ID
            AND GRP_MVID = 0
            AND GRP_LOB = 0
            AND GRP_BU_ID = 1
            AND GRP_PERIOD = 1
            AND GRP_QTR = 1
            AND GRP_BRANCH = 1
            AND GRP_YEAR = 1
            AND GRP_CAL_YEAR = 0
            AND GRP_CAL_PERIOD = 0
            AND HP1.PARTY_NAME NOT LIKE '%PLUM%'
            AND HP1.PARTY_NAME NOT LIKE '%INDUSTRIAL%'
            AND PUR.CALENDAR_YEAR > 2012
   GROUP BY HCA.ATTRIBUTE2,
            HCA.CUST_ACCOUNT_ID,
            HP.PARTY_NAME,
            HP1.PARTY_NAME,
            PUR.CALENDAR_YEAR,
            PUR.CALENDAR_PERIOD;
