--
-- XXWC_MSTR_PARTS_EXCL_LIST_VW  (View)
--

CREATE OR REPLACE FORCE VIEW APPS.XXWC_MSTR_PARTS_EXCL_LIST_VW
(
   PARTS_EXCL_LIST_ID
  ,ITEM_ID
  ,LAST_UPDATE_DATE
  ,LAST_UPDATED_BY
  ,CREATION_DATE
  ,CREATED_BY
  ,LAST_UPDATE_LOGIN
  ,OBJECT_VERSION_NUMBER
  ,ITEM_NUMBER
  ,ITEM_DESCRIPTION
)
AS
   SELECT parts_excl_list_id
         ,excl.item_id
         ,excl.last_update_date
         ,excl.last_updated_by
         ,excl.creation_date
         ,excl.created_by
         ,excl.last_update_login
         ,excl.object_version_number
         ,msi.segment1 item_number
         ,msi.description item_description
     FROM xxwc_mstr_parts_excl_list excl, mtl_system_items msi
    WHERE     excl.item_id = msi.inventory_item_id
          AND msi.organization_id =
                 (SELECT organization_id
                    FROM mtl_parameters
                   WHERE     master_organization_id = organization_id
                         AND organization_code = 'MST');


