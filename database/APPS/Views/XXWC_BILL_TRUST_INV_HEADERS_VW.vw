 /*******************************************************************************
      View: APPS.XXWC_BILL_TRUST_INV_HEADERS_VW
  --    REVISIONS:
  --    Ver        Date        Author              Description
  --    ---------  ----------  ---------------     -------------------------------   
  --     1.0       10/01/2013  Andre Rivas         TMS# 20141222-00045 - Reverting 
  --                                               back Multi-Org Changes.
  --     1.1        07/23/2018 Pattabhi Avual      TMS#20180723-00037 - Changes to 
  --                                               BillTrust Invoice Outbound Interface 
  --                                               for AHH Transactions
  *******************************************************************************/
CREATE OR REPLACE FORCE EDITIONABLE VIEW APPS.XXWC_BILL_TRUST_INV_HEADERS_VW (
BRANCH_NAME,
BRANCH_ADDRESS, 
HEADER_RECORD_TYPE, 
PARTY_ID, 
CUSTOMER_ACCOUNT_NUMBER, 
CUSTOMER_NAME, 
REMIT_TO_ADDRESS_CODE, 
REMIT_TO_LN1, 
REMIT_TO_LN2,
REMIT_TO_LN3, 
REMIT_TO_LN4, 
REMIT_TO_LN5,
CUST_SHIP_TO_NUMBER, 
CUST_SHIP_TO_NAME, 
CUST_SHIP_TO_LOCATION, 
CUST_SHIP_TO_ADDRESS1, 
CUST_SHIP_TO_ADDRESS2,
CUST_SHIP_TO_ADDRESS3, 
CUST_SHIP_TO_CITY, 
CUST_SHIP_TO_STATE_PROV, 
CUST_SHIP_TO_ZIP_CODE, 
CUST_SHIP_TO_COUNTRY, 
CUST_BILL_TO_NUMBER, 
CUST_BILL_TO_NAME, 
CUST_BILL_TO_LOCATION, 
CUST_BILL_TO_ADDRESS1, 
CUST_BILL_TO_ADDRESS2, 
CUST_BILL_TO_CITY, 
CUST_BILL_TO_STATE_PROV, 
CUST_BILL_TO_ZIP_CODE, 
CUST_BILL_TO_COUNTRY, 
INVOICE_NUMBER, 
INVOICE_DATE, 
PO_NUMBER, 
ORDER_NUMBER,
ORDER_DATE, 
TERMS, 
SHIP_VIA, 
ORDERED_BY, 
ACCOUNT_MANAGER, 
ACCOUNT_MANAGER_PHONE, 
TAKEN_BY, 
ACCT_JOB_NO, 
CUSTOMER_JOB_NO, 
RECEIVED_BY, 
INVOICE_ID,
TRANSACTION_TYPE, 
BATCH_SOURCE, 
ORG_ID, 
ORDER_FROM_ORG_ID, 
BRANCH_CODE, 
WHS_BRANCH_NAME, 
BRANCH_ADDRESS1, 
BRANCH_ADDRESS2, 
BRANCH_CITY, 
BRANCH_STATE, 
BRANCH_ZIP_CODE, 
BRANCH_REGION_1, 
BRANCH_COUNTRY, 
BRANCH_PHONE_NUMBER, 
TERRITORY_CODE, 
TOTAL_RECORD_TYPE, 
TOTAL_GROSS, 
TOTAL_TAX, 
TOTAL_SHIPPING_AND_HANDLING, 
TOTAL_INVOICE, 
TAX_RATE, 
BT_SEND_DATE, 
ORDER_TYPE, 
RENTAL_TYPE, 
TRX_SHIP_STATE, 
PR_SHIP_STATE, 
AMOUNT_DUE_REMAINING, 
AMOUNT_LINE_ITEMS_REMAINING, 
AMOUNT_APPLIED, 
ACCT_PAYMENT_TERM)
AS
  SELECT NULL branch_name ,
    NULL branch_address , --------- Header Record
    'H' header_record_type ,     -- Sold To Customr
    hp_bill.party_id ,           -- TMS# 20140219-00148
    hca_sold.account_number customer_account_number
    -- 02/13/2013 CG: Changed to use the account description instead of the part name
    ,
    NVL(hca_sold.account_name, hp_sold.party_name) customer_name ,
    NVL (hcp.attribute2, '2') remit_to_address_code , -- 02/23/2012 CG Added remit to address lines
    SUBSTR (flv.description, 1, INSTR (flv.description , ',' , 1 , 1)                                                  - 1) remit_to_ln1 ,
    SUBSTR (flv.description, INSTR (flv.description , ',' , 1 , 1)                                                     + 2, INSTR (flv.description , ',' , 1 , 2) - INSTR (flv.description , ',' , 1 , 1) - 2) remit_to_ln2 ,
    SUBSTR (flv.description, INSTR (flv.description , ',' , 1 , 2)                                                     + 2, INSTR (flv.description , ',' , 1 , 3) - INSTR (flv.description , ',' , 1 , 2) - 2) remit_to_ln3 ,
    DECODE (INSTR (flv.description , ',' , 1 , 4) , 0 , SUBSTR (flv.description, INSTR (flv.description , ',' , 1 , 3) + 2, LENGTH (flv.description) - INSTR (flv.description , ',' , 1 , 3) - 1) , SUBSTR (flv.description, INSTR (flv.description , ',' , 1 , 3) + 2, INSTR (flv.description , ',' , 1 , 4) - INSTR (flv.description , ',' , 1 , 3) - 2)) remit_to_ln4 ,
    (
    CASE
      WHEN INSTR (flv.description , ',' , 1 , 4) = 0
      AND INSTR (flv.description , ',' , 1 , 5)  = 0
      THEN NULL
      ELSE SUBSTR (flv.description, INSTR (flv.description , ',' , 1 , 4) + 2, LENGTH (flv.description) - INSTR (flv.description , ',' , 1 , 5))
    END) remit_to_ln5 , -- Ship To Information
    -- 02/23/2012 CG Changed from UAT1, to use Primary Ship if no Ship on Invoice
    -- Customer Ship To number went to be the Party Site Number
    --nvl(hca_ship.account_number, hca_pr_ship.account_number) cust_ship_to_number,
    -- 02/23/12 CGonzalez: Changed to trip everything after the -
    --nvl(hps_ship.party_site_number,hps_pr_ship.party_site_number) cust_ship_to_number,
    -- 06/05/12 CGonzalez: corrected to check the true ship to before using the primary ship to
    -- change made for all ship to associated fields
    (
    CASE
      WHEN rcta.ship_to_site_use_id IS NOT NULL
      THEN SUBSTR (hps_ship.party_site_number , 1 , DECODE (INSTR (hps_ship.party_site_number , '-' , 1 , 1), 0, LENGTH (hps_ship.party_site_number), INSTR (hps_ship.party_site_number , '-' , 1 , 1)             - 1))
      ELSE SUBSTR (hps_pr_ship.party_site_number , 1 , DECODE (INSTR (hps_pr_ship.party_site_number , '-' , 1 , 1), 0, LENGTH (hps_pr_ship.party_site_number), INSTR (hps_pr_ship.party_site_number , '-' , 1 , 1) - 1))
    END) cust_ship_to_number ,
    (
    CASE
      WHEN rcta.ship_to_customer_id IS NOT NULL
      THEN hp_ship.party_name
      ELSE hp_pr_ship.party_name
    END) cust_ship_to_name ,
    (
    CASE
      WHEN rcta.ship_to_site_use_id IS NOT NULL
      THEN hcsu_ship.location
      ELSE hcsu_pr_ship.location
    END) cust_ship_to_location ,
    (
    CASE
      WHEN rcta.ship_to_site_use_id IS NOT NULL
      THEN regexp_replace(hl_ship.address1, '[[:cntrl:]]', ' ')    -- TMS 20140710-00138
      ELSE regexp_replace(hl_pr_ship.address1, '[[:cntrl:]]', ' ') -- TMS 20140710-00138
    END) cust_ship_to_address1 ,
    (
    CASE
      WHEN rcta.ship_to_site_use_id IS NOT NULL
      THEN regexp_replace(hl_ship.address2, '[[:cntrl:]]', ' ')    -- TMS 20140710-00138
      ELSE regexp_replace(hl_pr_ship.address2, '[[:cntrl:]]', ' ') -- TMS 20140710-00138
    END) cust_ship_to_address2 ,
    (
    CASE
      WHEN rcta.ship_to_site_use_id IS NOT NULL
      THEN regexp_replace(hl_ship.address3, '[[:cntrl:]]', ' ')    -- TMS 20140710-00138
      ELSE regexp_replace(hl_pr_ship.address3, '[[:cntrl:]]', ' ') -- TMS 20140710-00138
    END) cust_ship_to_address3 ,
    (
    CASE
      WHEN rcta.ship_to_site_use_id IS NOT NULL
      THEN hl_ship.city
      ELSE hl_pr_ship.city
    END) cust_ship_to_city ,
    (
    CASE
      WHEN rcta.ship_to_site_use_id IS NOT NULL
      THEN NVL (hl_ship.state, hl_ship.province)
      ELSE NVL (hl_pr_ship.state, hl_pr_ship.province)
    END) cust_ship_to_state_prov ,
    (
    CASE
      WHEN rcta.ship_to_site_use_id IS NOT NULL
      THEN hl_ship.postal_code
      ELSE hl_pr_ship.postal_code
    END) cust_ship_to_zip_code ,
    (
    CASE
      WHEN rcta.ship_to_site_use_id IS NOT NULL
      THEN hl_ship.postal_code
      ELSE hl_pr_ship.postal_code
    END) cust_ship_to_country , -- Bill To Information
    -- 02/23/2012 CG Changed from UAT1
    --hca_bill.account_number cust_bill_to_number,
    hca_sold.account_number cust_bill_to_number
    -- 02/13/2013 CG: Changed to use the account description instead of the part name
    ,
    NVL(hca_bill.account_name, hp_bill.party_name) cust_bill_to_name ,
    hcsu_bill.location cust_bill_to_location ,
    regexp_replace(hl_bill.address1 , '[[:cntrl:]]', ' ') cust_bill_to_address1 -- TMS 20140710-00138
    ,
    regexp_replace((hl_bill.address2
    || ' '
    || hl_bill.address3) , '[[:cntrl:]]', ' ') -- TMS 20140710-00138
    cust_bill_to_address2 ,
    hl_bill.city cust_bill_to_city ,
    NVL (hl_bill.state, hl_bill.province) cust_bill_to_state_prov ,
    hl_bill.postal_code cust_bill_to_zip_code ,
    hl_bill.country cust_bill_to_country , -- Invoice Information
    rcta.trx_number invoice_number ,
    rcta.trx_date invoice_date , -- 06/05/2012 CG Changed for AHH Invoices where the PO Number match
    -- rcta.purchase_order po_number,
    (
    CASE
      --WHEN rbsa.name LIKE 'PRISM%' -- Ver#1.1
	  WHEN rbsa.name LIKE '%HARRIS%'  -- Ver#1.1
      AND rcta.ct_reference = LTRIM (rcta.purchase_order, '0')
      THEN NULL
      ELSE rcta.purchase_order
    END) po_number ,
    (
    CASE
      WHEN rbsa.name IN ('ORDER MANAGEMENT', 'STANDARD OM SOURCE')
      THEN rcta.interface_header_attribute1
      ELSE
        (SELECT sales_order
        FROM ra_customer_trx_lines_all rctla
        WHERE rctla.customer_trx_id = rcta.customer_trx_id
        AND rctla.sales_order      IS NOT NULL
        AND ROWNUM                  = 1
        )
    END) order_number ,
    (
    CASE
      WHEN rbsa.name IN ('ORDER MANAGEMENT', 'STANDARD OM SOURCE')
      THEN
        (SELECT TO_CHAR (ooha.ordered_date, 'MM/DD/YYYY')
        FROM oe_order_headers_all ooha
        WHERE ooha.order_number = rcta.interface_header_attribute1
        AND ROWNUM              = 1
        )
      ELSE
        (SELECT TO_CHAR (sales_order_date, 'MM/DD/YYYY')
        FROM ra_customer_trx_lines_all rctla
        WHERE rctla.customer_trx_id = rcta.customer_trx_id
        AND rctla.sales_order_date IS NOT NULL
        AND ROWNUM                  = 1
        )
    END) order_date ,
    (
    CASE
      WHEN rctta.TYPE = 'CM'
      THEN 'IMMEDIATE'
      ELSE rtt.description
    END) terms , -- 02/23/2012 CG Changed from UAT1 to use the Ship Method from the Order Header
    --rcta.ship_via,
    (
    CASE
      WHEN rbsa.name IN ('ORDER MANAGEMENT', 'STANDARD OM SOURCE')
      THEN
        (SELECT wcsm.ship_method_code_meaning
        FROM oe_order_headers_all ooha ,
          wsh_carrier_ship_methods_v wcsm
        WHERE ooha.order_number       = rcta.interface_header_attribute1
        AND ooha.shipping_method_code = wcsm.ship_method_code
        AND ROWNUM                    = 1
        )
        -- 05/16/2012 CGonzalez: Added mapping to pull HARRIS data from
        -- Interface attribute fields
      -- WHEN rbsa.name LIKE 'PRISM%'  -- Ver#1.1
	  WHEN rbsa.name LIKE '%HARRIS%'  -- Ver#1.1
      THEN rcta.interface_header_attribute12
      ELSE rcta.ship_via
    END) ship_via , -- 02/23/2012
    (
    CASE
      WHEN rbsa.name IN ('ORDER MANAGEMENT', 'STANDARD OM SOURCE')
      THEN
        (SELECT xxwc_mv_routines_pkg.get_contact_name ( ooha.invoice_to_contact_id )
        FROM oe_order_headers_all ooha
        WHERE ooha.order_number = rcta.interface_header_attribute1
        AND ROWNUM              = 1
        )
        -- 05/16/2012 CGonzalez: Added mapping to pull HARRIS data from
        -- Interface attribute fields
      --WHEN rbsa.name LIKE 'PRISM%'  -- Ver#1.1
	  WHEN rbsa.name LIKE '%HARRIS%'  -- Ver#1.1
      THEN rcta.interface_header_attribute9
      WHEN rbsa.name = 'REPAIR OM SOURCE' -- TMS# 20130823-00477 Added "REPAIR OM SOURCE"
      THEN
        (SELECT DISTINCT hp.party_name
        FROM cs_estimate_details ced ,
          cs_hz_sr_contact_points ciab ,
          hz_parties hp ,
          oe_order_headers_all ooh
        WHERE 1                      = 1
        AND ooh.order_number         = rcta.interface_header_attribute1
        AND ced.order_header_id      = ooh.header_id
        AND ced.incident_id          = ciab.incident_id
        AND ced.original_SOURCE_CODE = 'DR'
        AND hp.party_id              = ciab.party_id
        AND rownum                   = 1
        )
      ELSE xxwc_mv_routines_pkg.get_contact_name ( rcta.bill_to_contact_id )
    END) ordered_by , -- 04/26/2012 CGonzalez: Changed to pull the manager based on Trx and
    -- Bill to if trx is no sales credit
    --NVL (jrs.NAME, papf.full_name) account_manager,
    xxwc_mv_routines_pkg.get_bt_trx_salesrep ( rcta.primary_salesrep_id , hcsu_bill.primary_salesrep_id , rcta.org_id ) account_manager , -- 05/16/2012 CGonzalez: Account Manager Phone Number
    xxwc_mv_routines_pkg.get_bt_trx_salesrep_phone ( rcta.primary_salesrep_id , hcsu_bill.primary_salesrep_id , rcta.org_id ) account_manager_phone ,
    (
    CASE
      WHEN rbsa.name IN ('ORDER MANAGEMENT', 'STANDARD OM SOURCE')
      THEN
        (SELECT xxwc_mv_routines_pkg.get_user_employee ( ooha.created_by )
        FROM oe_order_headers_all ooha
        WHERE ooha.order_number = rcta.interface_header_attribute1
        AND ROWNUM              = 1
        )
        -- 05/16/2012 CGonzalez: Added mapping to pull HARRIS data from
        -- Interface attribute fields
      WHEN rbsa.name = 'REPAIR OM SOURCE' -- TMS# 20130823-00477 Added "REPAIR OM SOURCE"
      THEN
        (SELECT jrr.resource_name
        FROM cs_incidents_all_b cia ,
          jtf_rs_resource_extns_tl jrr ,
          oe_order_headers_all ooh ,
          cs_estimate_details ced
        WHERE 1                 =1
        AND ooh.order_number    = rcta.interface_header_attribute1
        AND ced.order_header_id = ooh.header_id
        AND cia.incident_id     = ced.incident_id
        AND jrr.resource_id     = cia.incident_owner_id
        AND rownum              = 1
        )
     -- WHEN rbsa.name LIKE 'PRISM%'  -- Ver#1.1
	 WHEN rbsa.name LIKE '%HARRIS%'   -- Ver#1.1
      THEN rcta.interface_header_attribute10
      ELSE NULL
    END) taken_by , -- 02/23/2012 CG: Changed from UAT to be the Ship To Site Number
    --hcsu_ship.location acct_job_no,
    /*substr(hps_ship.party_site_number, 1, decode(instr(hps_ship.party_site_number,'-',1,1)
    , 0, length(hps_ship.party_site_number)
    , instr(hps_ship.party_site_number,'-',1,1)-1)
    ) acct_job_no,*/
    SUBSTR ( NVL (hps_ship.party_site_number , hps_pr_ship.party_site_number) , 1 , DECODE ( INSTR ( NVL (hps_ship.party_site_number , hps_pr_ship.party_site_number) , '-' , 1 , 1 ) , 0 , LENGTH(NVL (hps_ship.party_site_number , hps_pr_ship.party_site_number)) , INSTR ( NVL (hps_ship.party_site_number , hps_pr_ship.party_site_number) , '-' , 1 , 1 ) - 1 ) ) acct_job_no ,
    (
    CASE
      WHEN SUBSTR ( NVL (hps_ship.party_site_number , hps_pr_ship.party_site_number) , 1 , DECODE ( INSTR ( NVL (hps_ship.party_site_number , hps_pr_ship.party_site_number) , '-' , 1 , 1 ) , 0 , LENGTH(NVL (hps_ship.party_site_number , hps_pr_ship.party_site_number)) , INSTR ( NVL (hps_ship.party_site_number , hps_pr_ship.party_site_number) , '-' , 1 , 1 ) - 1 ) ) = TRIM(SUBSTR (hcsu_ship.location , -- 05/17/2012 CGonzalez: Changed to read from right to left for first "-"
        -- INSTR (hcsu_ship.LOCATION, '-', 1, 1) + 1
        INSTR (hcsu_ship.location, '-', -1) + 1))
      THEN NULL
      ELSE TRIM(SUBSTR (hcsu_ship.location , -- 05/17/2012 CGonzalez: Changed to read from right to left for first "-"
        -- INSTR (hcsu_ship.LOCATION, '-', 1, 1) + 1
        INSTR (hcsu_ship.location, '-', -1) + 1))
    END) customer_job_no ,
    (
    CASE
      WHEN rbsa.name IN ('ORDER MANAGEMENT', 'STANDARD OM SOURCE')
      THEN
        (SELECT oola.revrec_signature
        FROM oe_order_headers_all ooha ,
          oe_order_lines_all oola
        WHERE ooha.order_number    = rcta.interface_header_attribute1
        AND ooha.header_id         = oola.header_id
        AND oola.revrec_signature IS NOT NULL
        AND ROWNUM                 = 1
        )
        -- 05/16/2012 CGonzalez: Added mapping to pull HARRIS data from
        -- Interface attribute fields
      --WHEN rbsa.name LIKE 'PRISM%'  -- Ver#1.1
	  WHEN rbsa.name LIKE '%HARRIS%'  -- Ver#1.1
      THEN rcta.interface_header_attribute11
      ELSE NULL
    END) received_by ,
    rcta.customer_trx_id invoice_id , -- Changed 03/1/2012 CGonzalez
    -- rctta.type transaction_type,
    -- 03/08/12 CGonzalez: changed to accomodate HARRIS data
    -- 06/05/12 CGonzalez: changed to ignore the interface_header_attribute8
    --  for HARRIS invoices except rental invoices
    (
    CASE
      WHEN ( rctta.TYPE = 'INV'
     -- AND rbsa.name NOT LIKE 'PRISM%'  -- Ver#1.1
	  AND rbsa.name NOT LIKE '%HARRIS%'  -- Ver#1.1
      AND rcta.interface_header_attribute2 LIKE '%RENTAL%')
     -- OR (rbsa.name LIKE 'PRISM%'  -- Ver#1.1
	 OR (rbsa.name LIKE '%HARRIS%'   -- Ver#1.1
      AND rcta.interface_header_attribute8 = 'RENTAL INVOICE' --'R'
        )
      THEN 'REN'
      WHEN (rctta.TYPE = 'CM'
     -- AND rbsa.name NOT LIKE 'PRISM%'  -- Ver#1.1
	 AND rbsa.name NOT LIKE '%HARRIS%'  -- Ver#1.1
      AND rcta.interface_header_attribute2 LIKE '%RENTAL%'
      AND apsa.amount_due_original = 0) -- TMS (20130401-01000)
      THEN 'RER'
      WHEN rctta.TYPE = 'CM'
        /* 06/05/12 CGonzalez: Above note
        OR (     rbsa.NAME LIKE 'HARRIS%'
        AND rcta.interface_header_attribute8 = 'C'
        )*/
      THEN 'CRM'
      WHEN rctta.TYPE = 'DM'
        /* 06/05/12 CGonzalez: Above note
        OR (     rbsa.NAME LIKE 'HARRIS%'
        AND rcta.interface_header_attribute8 = 'D'
        )*/
      THEN 'DBM'
      ELSE rctta.TYPE
    END) transaction_type ,
    rbsa.name batch_source ,
    rcta.org_id , -- 01/23/2012 CGonzalez
    (
    CASE
      WHEN rbsa.name IN ('ORDER MANAGEMENT', 'STANDARD OM SOURCE')
      THEN rcta.interface_header_attribute10
        --xxwc_mv_routines_pkg.get_order_shp_from_org_id (rcta.interface_header_attribute1)
      ELSE NULL
    END) order_from_org_id , -- 05/07/2012 CG: Had to change to accomodate noninventory locations from HARRIS
    -- mp.organization_code branch_code,
    SUBSTR (hla.location_code, 1, INSTR (hla.location_code , '-' , 1 , 1) - 2) , -- 02/23/2012 CG
    hla.location_code whs_branch_name ,                                          -- 02/23/2012 CG
    regexp_replace(hla.address_line_1 , '[[:cntrl:]]', ' ') branch_address1      -- TMS 20140710-00138
    ,
    regexp_replace(( hla.address_line_2
    || DECODE (hla.address_line_3, NULL, '', '|')
    || hla.address_line_3) , '[[:cntrl:]]', ' ') branch_address2 -- TMS 20140710-00138
    ,
    hla.town_or_city branch_city ,
    hla.region_2 branch_state ,
    hla.postal_code branch_zip_code ,
    hla.region_1 branch_region_1 ,
    hla.country branch_country , -- 05/16/2012 CGonzalez: Formatted phone number
    -- hla.telephone_number_1 branch_phone_number,
    xxwc_mv_routines_pkg.format_phone_number (hla.telephone_number_1) branch_phone_number ,
    'TERR' territory_code , -- Unknown Source Defaulting TERR for now
    -- 01/23/2012 CGonzalez
    --------- Totals Record
    'T' total_record_type ,
    apsa.amount_line_items_original total_gross ,
    apsa.tax_original total_tax ,
    apsa.freight_original total_shipping_and_handling ,
    apsa.amount_due_original total_invoice ,                                  -- 01/23/2012 CGonzalez
    xxwc_mv_routines_pkg.get_tax_rate (rcta.customer_trx_id, NULL) tax_rate , -- 01/23/2012 CGonzalez
    -- 03/23/2012 CGonzalez: added DFF to track BT Send date
    rcta.attribute15 , --04/17/2012 rcta.attribute2,
    -- 04/12/2012 CGonzalez: Added rental order type information
    (
    CASE
      WHEN rbsa.name IN ('ORDER MANAGEMENT', 'STANDARD OM SOURCE')
      THEN rcta.interface_header_attribute2
      ELSE NULL
    END) order_type ,
    (
    CASE
      WHEN rbsa.name IN ('ORDER MANAGEMENT', 'STANDARD OM SOURCE')
      THEN
        (SELECT ooha.attribute1
        FROM oe_order_headers_all ooha
        WHERE ooha.order_number = rcta.interface_header_attribute1
        AND ROWNUM              = 1
        )
     -- WHEN rbsa.name LIKE 'PRISM%'  -- Ver#1.1
	  WHEN rbsa.name LIKE '%HARRIS%'  -- Ver#1.1
      THEN xxwc_mv_routines_pkg.get_prism_rental_type ( rcta.trx_number )
      ELSE NULL
    END) rental_type , -- 12/28/12 CG: Added for CA 1% Surcharge
    hl_ship.state trx_ship_state ,
    hl_pr_ship.state pr_ship_state
    -- 02/13/2013 CG: TMS 20121217-00785 - added to pull CM Remaining balance into extract
    ,
    apsa.amount_due_remaining ,
    apsa.amount_line_items_remaining ,
    apsa.amount_applied
    -- 03/01/2013 CG: TMS 20121217-00785 added to pull the customer account payment term
    ,
    acct_rtt.description acct_payment_term
  FROM ar.ra_customer_trx_all rcta ,
    ar.ar_payment_schedules_all apsa , -- Sold To Data
    ar.hz_cust_accounts hca_sold ,
    ar.hz_parties hp_sold ,
    ar.hz_customer_profiles hcp ,
    ar.hz_cust_profile_classes hcpc -- TMS# 20130312-01672
    ,                               -- Bill To Data
    ar.hz_cust_accounts hca_bill ,
    ar.hz_parties hp_bill ,
    ar.hz_cust_site_uses_all hcsu_bill ,
    ar.hz_cust_acct_sites_all hcasa_bill ,
    ar.hz_party_sites hps_bill ,
    ar.hz_locations hl_bill , -- Ship To Data
    ar.hz_cust_accounts hca_ship ,
    ar.hz_parties hp_ship ,
    ar.hz_cust_site_uses_all hcsu_ship ,
    ar.hz_cust_acct_sites_all hcasa_ship ,
    ar.hz_party_sites hps_ship ,
    ar.hz_locations hl_ship , -- Primary Ship To Data
    ar.hz_cust_accounts hca_pr_ship ,
    ar.hz_parties hp_pr_ship ,
    ar.hz_cust_site_uses_all hcsu_pr_ship ,
    ar.hz_cust_acct_sites_all hcasa_pr_ship ,
    ar.hz_party_sites hps_pr_ship ,
    ar.hz_locations hl_pr_ship , -- Invoice Data
    ar.ra_terms_tl rtt ,
    ar.ra_terms_tl rtt_c ,
    ar.ra_cust_trx_types_all rctta ,
    ra_batch_sources_all rbsa , -- 04/26/12 CGonzalez: Commented and moved pull to function
    --jtf.jtf_rs_salesreps jrs,
    --hr.per_all_people_f papf,
    -- 04/26/12 CGonzalez
    -- 01/23/2012 Branch Info
    -- 05/07/2012 CG: Had to change to accomodate noninventory locations from HARRIS
    -- inv.mtl_parameters mp,
    hr.hr_locations_all hla,        -- Issue 759: 08/01/2012 CG: Changed to correct issue of branch mult locs
   -- hr_all_organization_units haou , -- Issue 759  -- Ver#1.1
    -- 02/23/2012 Added Lookup Address Parsing
    fnd_lookup_values flv
    -- 03/01/2013 CG: TMS 20121217-00785 added to pull the customer account payment term
    ,
    ar.ra_terms_tl acct_rtt
  WHERE rcta.status_trx    = 'OP'
  AND rcta.complete_flag   = 'Y'
  AND hcp.profile_class_id = hcpc.profile_class_id -- TMS# 20130312-01672
  AND hcpc.standard_terms  = rtt_c.term_id         -- TMS# 20130312-01672
  AND rcta.customer_trx_id = apsa.customer_trx_id
    -- 04/13/2012 CG excluded fully paid invoices
    -- 08/10/2012 CG: changed to pull invoices where the amount due original - amount paid
    -- is greater than zero, to exclude invoices that have discounts
    -- AND ABS (apsa.amount_due_remaining) > 0
  AND NVL(rtt_c.name,'$$$$') NOT                                                 IN ('COD','PRFRDCASH') -- Added TMS# 20130312-01672
  AND ( ( ( ABS (apsa.amount_due_original) - NVL (ABS (apsa.amount_applied), 0))  > 0)
  OR ( ( ABS (apsa.amount_due_original)    - NVL (ABS (apsa.amount_applied), 0)) <= 0 -- Added TMS# 20130312-01672
    -- - NVL (ABS (apsa.amount_applied), 0)) = 0    -- Commented TMS# 20130312-01672
  AND apsa.class = 'CM'))
    -- Sold To Data
  AND rcta.sold_to_customer_id = hca_sold.cust_account_id
  AND hca_sold.party_id        = hp_sold.party_id
  AND hca_sold.cust_account_id = hcp.cust_account_id
  AND hca_sold.party_id        = hcp.party_id -- 05/30/2012
  AND hcp.site_use_id         IS NULL
    -- Bill to Data
  AND rcta.bill_to_customer_id = hca_bill.cust_account_id
  AND hca_bill.party_id        = hp_bill.party_id
    -- 02/23/2012 CG Changed from UAT1 to use the Primary Bill To Always
    --AND rcta.bill_to_site_use_id = hcsu_bill.site_use_id
  AND hca_bill.cust_account_id          = hcasa_bill.cust_account_id
  AND hcasa_bill.status                 = 'A' -- 05/30/2012
  AND hcasa_bill.bill_to_flag           = 'P' -- 05/30/2012
  AND hcasa_bill.cust_acct_site_id      = hcsu_bill.cust_acct_site_id
  AND hcsu_bill.site_use_code           = 'BILL_TO'
  AND NVL (hcsu_bill.primary_flag, 'N') = 'Y'
    -- 02/23/2012 CG
  AND hcasa_bill.party_site_id = hps_bill.party_site_id
  AND hps_bill.location_id     = hl_bill.location_id
    -- Ship To Data
  AND rcta.ship_to_customer_id    = hca_ship.cust_account_id(+)
  AND hca_ship.party_id           = hp_ship.party_id(+)
  AND rcta.ship_to_site_use_id    = hcsu_ship.site_use_id(+)
  AND hcsu_ship.cust_acct_site_id = hcasa_ship.cust_acct_site_id(+)
    --AND hcasa_ship.status(+) in ('A','I') -- 05/30/2012
    --AND hcasa_ship.ship_to_flag(+) in ('P','Y') -- 05/30/2012
  AND hcasa_ship.party_site_id = hps_ship.party_site_id(+)
  AND hps_ship.location_id     = hl_ship.location_id(+)
    -- 02/23/2012 CG
    -- Primary Ship To Data
  AND rcta.sold_to_customer_id             = hca_pr_ship.cust_account_id
  AND hca_pr_ship.party_id                 = hp_pr_ship.party_id
  AND hca_pr_ship.cust_account_id          = hcasa_pr_ship.cust_account_id
  AND hcasa_pr_ship.status                 = 'A' -- 05/30/2012
  AND hcasa_pr_ship.ship_to_flag           = 'P' -- 05/30/2012
  AND hcasa_pr_ship.cust_acct_site_id      = hcsu_pr_ship.cust_acct_site_id
  AND hcsu_pr_ship.site_use_code           = 'SHIP_TO'
  AND NVL (hcsu_pr_ship.primary_flag, 'N') = 'Y'
  AND hcasa_pr_ship.party_site_id          = hps_pr_ship.party_site_id
  AND hps_pr_ship.location_id              = hl_pr_ship.location_id
    -- Invoice Data
  AND rcta.term_id    = rtt.term_id(+)
  AND rtt.language(+) = 'US'
    -- 02/23/2012 CG Changed from UAT to be the rep on the account
    --AND rcta.primary_salesrep_id = jrs.salesrep_id(+)
    -- 04/26/12 CGonzalez
    --AND hcsu_bill.primary_salesrep_id = jrs.salesrep_id(+)
    --AND hcsu_bill.org_id = jrs.org_id(+)
    --AND jrs.person_id = papf.person_id(+)
    --AND TRUNC (papf.effective_start_date(+)) <= TRUNC (SYSDATE)
    --AND NVL (TRUNC (papf.effective_end_date(+)), TRUNC (SYSDATE)) >= TRUNC (SYSDATE)
    -- 04/26/12 CGonzalez
  AND rcta.cust_trx_type_id = rctta.cust_trx_type_id
  AND rcta.org_id           = rctta.org_id
  AND rcta.batch_source_id  = rbsa.batch_source_id
  AND rcta.org_id           = rbsa.org_id
    -- 01/23/2012 Branch Info
    -- 03/08/2012 CGonzalez adjusted to accomodate data from PRISM
 --  AND ( (rbsa.name NOT LIKE 'PRISM%' -- Ver#1.1
  AND ( (rbsa.name NOT LIKE '%HARRIS%'  -- Ver#1.1
  AND hla.BILL_TO_SITE_FLAG = 'Y'       -- Ver#1.1
  AND NVL (rcta.interface_header_attribute10 ,
    (SELECT TO_CHAR (organization_id)
    FROM mtl_parameters
    WHERE organization_code = 'WCC'
    ) -- Issue 759: 08/01/2012 CG: Changed to correct issue of branch mult locs
    --) = TO_CHAR (hla.inventory_organization_id)
   /* --<START> Ver#1.1 )                  = TO_CHAR (haou.organization_id)
  AND haou.location_id = hla.location_id -- Issue 759
    -- 05/07/2012 CG: Had to change to accomodate noninventory locations from PRISM
    -- ) = TO_CHAR (mp.organization_id)
    )
  OR ( rbsa.name LIKE 'PRISM%'
    -- 05/30/2012 CG Added NVL to WCC for Prism Trx without branch info
    -- AND NVL (rcta.interface_header_attribute7, 'WCC') =
  AND NVL (LPAD (rcta.interface_header_attribute7, 3, '0') , 'WCC') = -- Issue 759: 08/01/2012 CG: Changed to correct issue of branch mult locs
    -- SUBSTR (hla.location_code,
    -- 1,
    -- INSTR (hla.location_code, '-', 1, 1) - 2
    -- )
    SUBSTR (haou.name, 1, INSTR (haou.name , '-' , 1 , 1) - 2)
  AND haou.location_id = hla.location_id -- Issue 759
    -- 05/07/2012 CG: Had to change to accomodate noninventory locations from PRISM
    --AND rcta.interface_header_attribute7 = mp.organization_code
    ) --OR mp.organization_code = 'MST'
    )
    -- 05/07/2012 CG: Had to change to accomodate noninventory locations from PRISM
    --AND mp.organization_id = hla.inventory_organization_id(+)
    -- 05/07/2012 CG:
    -- 02/23/2012 CG */
	) = (SELECT TO_CHAR (haou.organization_id)
    FROM hr_all_organization_units haou
    WHERE haou.location_id = hla.location_id
    AND ROWNUM             = 1
    ) )
  OR (rbsa.name LIKE '%HARRIS%'
  AND hla.BILL_TO_SITE_FLAG                    = 'Y'
  AND UPPER(rcta.interface_header_attribute7) IN
    (SELECT UPPER(stg.ahh_warehouse_number)
    FROM xxwc.xxwc_ap_branch_list_stg_tbl stg
    WHERE SUBSTR (hla.location_code, 1, 3) = stg.oracle_branch_number
    ) ))   -- <END> -- Ver#1.1
  AND NVL (hcp.attribute2, '2') = flv.lookup_code
  AND flv.lookup_type           = 'XXWC_REMIT_TO_ADDRESS_CODES'
    -- 03/01/2013 CG: TMS 20121217-00785 added to pull the customer account payment term
  AND hcp.standard_terms   = acct_rtt.term_id(+)
  AND acct_rtt.language(+) = 'US'
  ORDER BY hca_bill.account_number
  /