CREATE OR REPLACE FORCE VIEW APPS.XXWC_AR_PAYMENT_SCHEDULES_VW
(
   TRX_NUMBER,
   TRX_DATE,
   TYPE_NAME,
   CT_PURCHASE_ORDER,
   INTERFACE_HEADER_ATTRIBUTE1,
   DUE_DATE,
   AMOUNT_DUE_ORIGINAL,
   AMOUNT_DUE_REMAINING,
   CUSTOMER_ID,
   CUSTOMER_SITE_USE_ID,
   SU_LOCATION
)
AS
   SELECT /******************************************************************************
             NAME:       apps.xxwc_ar_payment_schedules_vw
             PURPOSE:    View used to display outstanding invoices in lien release form

             REVISIONS:
             Ver        Date        Author           Description
             ---------  ----------  ---------------  ------------------------------------
             1.0        11/22/2013      shariharan    Initial Version
			 1.1        05/01/2017      neha saini    updated for TMS Task ID: 20170215-00038
          ******************************************************************************/
         trx_number,
          trx_date,
          type_name,
          ct_purchase_order,
          interface_header_attribute1,
          due_date,
          amount_due_original,
          amount_due_remaining,
          customer_id,
          customer_site_use_id,
          su_location
     /*,
     (SELECT b.concatenated_address
        FROM xxwc_ar_addresses_v b, apps.hz_cust_site_uses c
       WHERE     b.customer_id = a.customer_id
             AND b.address_id = c.cust_acct_site_id
             AND c.site_use_code = 'BILL_TO'
             AND primary_flag = 'Y'
             AND ROWNUM = 1)
        primary_location*/
    -- commented for ver1.1 FROM ar_payment_schedules_v a
	FROM XXWC_AR_PAYMENT_SCHEDULES_V a;
   -- WHERE     a.status <> 'CL' -- commented for ver1.1
         -- AND (a.customer_id, a.customer_site_use_id) IN (SELECT customer_id, -- commented for ver1.1
                                                            --     NVL ( -- commented for ver1.1
                                                             --       bill_to_site_use_id, -- commented for ver1.1
                                                             --       primary_bt_site_use_id) -- commented for ver1.1
                                                          --  FROM apps.xxwc_ar_line_release_hdr_gt); -- commented for ver1.1
