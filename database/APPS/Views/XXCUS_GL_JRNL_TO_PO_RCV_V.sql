CREATE OR REPLACE FORCE VIEW "APPS"."XXCUS_GL_JRNL_TO_PO_RCV_V" ("JE_BATCH_ID", "BATCH_NAME", "BATCH_DESCRIPTION", "JE_HEADER_ID", "LEDGER_NAME", "JE_SOURCE", "JE_CATEGORY", "JE_PERIOD", "JE_NAME", "JE_DESCRIPTION", "JE_CURRENCY", "JE_STATUS", "JE_TYPE", "JE_POSTED_DATE", "JE_CREATION_DATE", "JE_CREATED_BY", "JE_BALANCED", "ACCRUAL_REVERSAL", "JE_MULTI_BAL_SEG", "ACCRUAL_REV_EFFECTIVE_DATE", "ACCRUAL_REV_PERIOD_NAME", "ACCRUAL_REV_STATUS", "ACCRUAL_REV_JE_HEADER_ID", "ACCRUAL_REV_CHANGE_SIGN", "REVERSED_JE_HEADER_ID", "JE_CURR_CONV_RATE", "JE_CURR_CONV_TYPE", "JE_CURR_CONV_DATE", "JE_EXTERNAL_REF", "JE_LINE_NUM", "JE_EFFECTIVE_DATE", "JE_ENTERED_DR", "JE_ENTERED_CR", "JE_ACCOUNTED_DR", "JE_ACCOUNTED_CR", "JE_LINE_DESCRIPTION", "GL_SL_LINK_ID", "GL_SL_LINK_TABLE", "REFERENCE_10", "CODE_COMBINATION_ID", "GL_ACCOUNT_STRING", "BUDGET_NAME", "BUDGET_TYPE", "BUDGET_VERSION_ID", "BUDGET_STATUS", "BUDGET_DESCRIPTION", "ENCUMBRANCE_TYPE", "RECEIPT_NUM", "PO_NUMBER", "SUPPLIER_NUMBER", "SUPPLIER", "SUPPLIER_SITE", "SLA_ENTERED_DR", "SLA_ENTERED_CR", "SLA_ACCOUNTED_DR", "SLA_ACCOUNTED_CR", "SLA_ACCOUNTED_NET", "SLA_ACCOUNT_CLASS", "SLA_EVENT_TYPE", "TRANSFER_DATE_FROM_SLA_TO_GL", "SLA_ACCOUNTING_DATE", "ENCUMBRANCE_TYPE_ID", "AE_HEADER_ID", "AE_LINE_NUM", "APPLICATION_ID", "TEMP_LINE_NUM", "REF_AE_HEADER_ID", "TRANSACTION_ID", "SHIPMENT_LINE_ID", "SHIPMENT_HEADER_ID", "VENDOR_ID", "GCC#BRANCH", "JL#BRANCH", "JL#DEPT", "JL#ACCOUNT", "JL#SUB_ACCT", "JL#POS_BRANCH", "JL#WC_FORMULA", "RH#PRINTED", "GCC50328PRODUCT", "GCC50328PRODUCTDESCR", "GCC50328LOCATION", "GCC50328LOCATIONDESCR", "GCC50328COST_CENTER", "GCC50328COST_CENTERDESCR", "GCC50328ACCOUNT", "GCC50328ACCOUNTDESCR", "GCC50328PROJECT_CODE", "GCC50328PROJECT_CODEDESCR", "GCC50328FUTURE_USE", "GCC50328FUTURE_USEDESCR", "GCC50328FUTURE_USE_2", "GCC50328FUTURE_USE_2DESCR", "GCC50368PRODUCT", "GCC50368PRODUCTDESCR", "GCC50368DIVISION", "GCC50368DIVISIONDESCR", "GCC50368DEPARTMENT", "GCC50368DEPARTMENTDESCR", "GCC50368ACCOUNT", "GCC50368ACCOUNTDESCR", "GCC50368SUBACCOUNT", "GCC50368SUBACCOUNTDESCR", "GCC50368FUTURE_USE", "GCC50368FUTURE_USEDESCR") AS 
  SELECT jb.je_batch_id,
          jb.name Batch_Name,
          jb.description Batch_Description,
          jh.je_header_id,
          le.name ledger_name,
          jes.user_je_source_name JE_Source,
          jec.user_je_category_name JE_Category,
          jh.period_name JE_Period,
          jh.name JE_Name,
          jh.description JE_Description,
          jh.currency_code JE_Currency,
          DECODE (
             jh.status,
             'P', 'Posted',
             'U', 'Unposted',
             'F', 'Error7 - Showing invalid journal entry lines or no journal entry lines',
             'K', 'Error10 - Showing unbalanced intercompany journal entry',
             'Z', 'Error7 - Showing invalid journal entry lines or no journal entry lines',
             'Unknown')
             JE_Status,
          DECODE (jh.actual_flag,
                  'A', 'Actual',
                  'B', 'Budget',
                  'E', 'Encumbrance')
             JE_Type,
          jh.posted_date JE_Posted_Date,
          jh.creation_date JE_Creation_Date,
          xxeis.eis_rs_gl_fin_com_util_pkg.eis_get_user_name (jh.created_by)
             JE_Created_By,
          xxeis.eis_rs_gl_fin_com_util_pkg.eis_get_yes_no (
             jh.balanced_je_flag)
             JE_Balanced,
          xxeis.eis_rs_gl_fin_com_util_pkg.eis_get_yes_no (
             jh.accrual_rev_flag)
             Accrual_Reversal,
          xxeis.eis_rs_gl_fin_com_util_pkg.eis_get_yes_no (
             jh.multi_bal_seg_flag)
             JE_Multi_Bal_Seg,
          jh.accrual_rev_effective_date,
          jh.accrual_rev_period_name,
          jh.accrual_rev_status,
          jh.accrual_rev_je_header_id,
          xxeis.eis_rs_gl_fin_com_util_pkg.eis_get_yes_no (
             jh.accrual_rev_change_sign_flag)
             Accrual_Rev_Change_Sign,
          jh.reversed_je_header_id,
          jh.currency_conversion_rate JE_Curr_Conv_Rate,
          jh.currency_conversion_type JE_Curr_Conv_Type,
          jh.currency_conversion_date JE_Curr_Conv_Date,
          jh.external_reference JE_external_Ref,
          jl.je_line_num JE_Line_Num,
          jl.effective_date JE_Effective_Date,
          jl.entered_dr JE_Entered_DR,
          jl.entered_cr JE_Entered_CR,
          jl.accounted_dr JE_Accounted_DR,
          jl.accounted_cr JE_Accounted_CR,
          jl.description JE_Line_Description,
          --          jl.reference_1 SL_Source,
          --          jl.reference_2 PO_Header_id,
          --          jl.reference_3 PO_Distribution_ID,
          --          jl.reference_4 PO_Number,
          --          jl.reference_5 RCV_Transaction_ID,
          --          jl.reference_6,
          --          jl.reference_7,
          --          jl.reference_8,
          --          jl.reference_9,
          jl.gl_sl_link_id,
          jl.gl_sl_link_table,
          jl.reference_10,
          gcc.code_combination_id,
          gcc.concatenated_segments GL_Account_String,
          gbv.budget_name,
          gbv.budget_type,
          gbv.budget_version_id,
          DECODE (gbv.status,  'O', 'Open',  'C', 'Closed',  'F', 'Future')
             Budget_Status,
          gbv.description Budget_description,
          gle.encumbrance_type,
          rh.receipt_num,
          poh.segment1 PO_Number,
          pov.segment1 Supplier_Number,
          pov.vendor_name Supplier,
          povs.vendor_site_code Supplier_Site,
          xdl.unrounded_entered_dr sla_entered_dr,
          xdl.unrounded_entered_cr sla_entered_cr,
          xdl.unrounded_accounted_dr sla_accounted_dr,
          xdl.unrounded_accounted_cr sla_accounted_cr,
--            NVL (xdl.unrounded_accounted_cr, 0)
--          - NVL (xdl.unrounded_accounted_dr, 0)
--             sla_accounted_net,
            NVL (xdl.unrounded_accounted_dr, 0)
          - NVL (xdl.unrounded_accounted_cr, 0)
             sla_accounted_net,
          xel.accounting_class_code sla_account_class,
          xeh.event_type_code sla_event_type,
          xeh.gl_transfer_date transfer_date_from_sla_to_gl,
          xeh.accounting_date sla_accounting_date,
          gle.encumbrance_type_id,
          xel.ae_header_id,
          xel.ae_line_num,
          xel.application_id,
          xdl.temp_line_num,
          xdl.ref_ae_header_id,
          rt.transaction_id,
          rl.shipment_line_id,
          rh.shipment_header_id,
          pov.vendor_id                                --descr#flexfield#start
                       ,
          xxeis.eis_rs_dff.decode_valueset ('XXCUS_FRU', GCC.ATTRIBUTE1, 'I')
             GCC#Branch,
          xxeis.eis_rs_dff.decode_valueset ('XXCUS_FRU', JL.ATTRIBUTE1, 'I')
             JL#Branch,
          JL.ATTRIBUTE2 JL#Dept,
          JL.ATTRIBUTE3 JL#Account,
          JL.ATTRIBUTE4 JL#Sub_Acct,
          JL.ATTRIBUTE5 JL#POS_Branch,
          JL.ATTRIBUTE6 JL#WC_Formula,
          RH.ATTRIBUTE1 RH#Printed                       --descr#flexfield#end
                                  --gl#accountff#start
          ,
          GCC.SEGMENT1 GCC#50328#PRODUCT,
          xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT1,
                                                'XXCUS_GL_PRODUCT')
             GCC#50328#PRODUCT#DESCR,
          GCC.SEGMENT2 GCC#50328#LOCATION,
          xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT2,
                                                'XXCUS_GL_LOCATION')
             GCC#50328#LOCATION#DESCR,
          GCC.SEGMENT3 GCC#50328#COST_CENTER,
          xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT3,
                                                'XXCUS_GL_COSTCENTER')
             GCC#50328#COST_CENTER#DESCR,
          GCC.SEGMENT4 GCC#50328#ACCOUNT,
          xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT4,
                                                'XXCUS_GL_ACCOUNT')
             GCC#50328#ACCOUNT#DESCR,
          GCC.SEGMENT5 GCC#50328#PROJECT_CODE,
          xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT5,
                                                'XXCUS_GL_PROJECT')
             GCC#50328#PROJECT_CODE#DESCR,
          GCC.SEGMENT6 GCC#50328#FURTURE_USE,
          xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT6,
                                                'XXCUS_GL_FUTURE_USE1')
             GCC#50328#FURTURE_USE#DESCR,
          GCC.SEGMENT7 GCC#50328#FUTURE_USE_2,
          xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT7,
                                                'XXCUS_GL_FUTURE_USE_2')
             GCC#50328#FUTURE_USE_2#DESCR,
          GCC.SEGMENT1 GCC#50368#PRODUCT,
          xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT1,
                                                'XXCUS_GL_LTMR_PRODUCT')
             GCC#50368#PRODUCT#DESCR,
          GCC.SEGMENT2 GCC#50368#DIVISION,
          xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT2,
                                                'XXCUS_GL_ LTMR _DIVISION')
             GCC#50368#DIVISION#DESCR,
          GCC.SEGMENT3 GCC#50368#DEPARTMENT,
          xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT3,
                                                'XXCUS_GL_ LTMR _DEPARTMENT')
             GCC#50368#DEPARTMENT#DESCR,
          GCC.SEGMENT4 GCC#50368#ACCOUNT,
          xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT4,
                                                'XXCUS_GL_ LTMR _ACCOUNT')
             GCC#50368#ACCOUNT#DESCR,
          GCC.SEGMENT5 GCC#50368#SUBACCOUNT,
          xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT5,
                                                'XXCUS_GL_ LTMR _SUBACCOUNT')
             GCC#50368#SUBACCOUNT#DESCR,
          GCC.SEGMENT6 GCC#50368#FUTURE_USE,
          xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT6,
                                                'XXCUS_GL_ LTMR _FUTUREUSE')
             GCC#50368#FUTURE_USE#DESCR
     --gl#accountff#end
     FROM apps.gl_ledgers le,
          apps.gl_je_batches jb,
          apps.gl_je_headers jh,
          apps.gl_je_lines jl,
          apps.gl_code_combinations_kfv gcc,
          apps.gl_je_sources jes,
          apps.gl_je_categories jec,
          apps.gl_import_references gir,
          apps.gl_budget_versions gbv,
          apps.gl_encumbrance_types gle,
          apps.xla_ae_lines xel,
          apps.xla_ae_headers xeh,
          apps.xla_distribution_links xdl,
          apps.rcv_receiving_sub_ledger rsl,
          apps.rcv_transactions rt,
          apps.rcv_shipment_lines rl,
          apps.rcv_shipment_headers rh,
          apps.po_vendors pov,
          apps.po_vendor_sites povs,
          apps.po_headers_all poh
    WHERE     1 = 1
          AND le.ledger_id = jh.ledger_id
          AND jb.je_batch_id = jh.je_batch_id
          AND jh.je_header_id = jl.je_header_id
          AND jl.code_combination_id = gcc.code_combination_id
          AND jh.je_source = jes.je_source_name
          AND jh.je_category = jec.je_category_name
          AND jh.budget_version_id = gbv.budget_version_id(+)
          AND jh.encumbrance_type_id = gle.encumbrance_type_id(+)
          AND gir.je_header_id = jl.je_header_id
          AND gir.je_line_num = jl.je_line_num
          AND gir.gl_sl_link_id = xel.gl_sl_link_id(+)
          AND gir.gl_sl_link_table = xel.gl_sl_link_table(+)
          AND xel.ae_header_id = xdl.ae_header_id(+)
          AND xel.ae_line_num = xdl.ae_line_num(+)
          AND xel.ae_header_id = xeh.ae_header_id(+)
          AND xdl.application_id = 707 --new 10/16
          AND xdl.source_distribution_id_num_1 = rsl.rcv_sub_ledger_id(+)
          AND xdl.source_distribution_type(+) = 'RCV_RECEIVING_SUB_LEDGER'
          AND rsl.rcv_transaction_id = rt.transaction_id(+)
          AND rt.shipment_line_id = rl.shipment_line_id(+)
          AND rl.shipment_header_id = rh.shipment_header_id(+)
          AND poh.po_header_id(+) = rl.po_header_id
          AND rh.vendor_id = pov.vendor_id(+)
          AND rh.vendor_site_id = povs.vendor_site_id(+)
          AND jh.je_source = 'Cost Management' --Sbala commented this out on 8/23/2012. Wrong source for PO receiving journal entries. Originally it was 'Purchasing'
--          AND gl_security_pkg.validate_access (jh.ledger_id) = 'TRUE'
--          AND xxeis.eis_gl_security_pkg.validate_access (
--                 jh.ledger_id,
--                 gcc.code_combination_id) = 'TRUE';;
