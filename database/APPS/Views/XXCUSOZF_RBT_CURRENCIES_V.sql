
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUSOZF_RBT_CURRENCIES_V" ("NAME", "CURRENCY_CODE", "SORT_SEQ") AS 
  SELECT name
         ,currency_code
         ,case
           when currency_code ='USD' then 1
           when currency_code ='CAD' then 2
           else 3            
          end sort_seq
     FROM fnd_currencies_tl
    WHERE 1 =1
      AND currency_code IN ('USD', 'CAD')
      AND language ='US';
