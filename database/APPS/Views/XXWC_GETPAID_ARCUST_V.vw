CREATE OR REPLACE VIEW XXWC_GETPAID_ARCUST_V AS
------------------------------------------------------------------------------------------------------------------------------
-- View to dervie all the customer details from Oracle.
-- Does not use any DataDumps from GetPaid.
------------------------------------------------------------------------------------------------------------------------------
SELECT DISTINCT (RPAD(NVL(hca.account_number, ' '),20,' ')                                     --   CUSTNO
     || RPAD(' ',20,' ')                                                              --   PARENT
     || RPAD(NVL(hca.account_name, ' '),50,' ')                                       --   COMPANY
     || RPAD(NVL(hl.address1, ' '),50,' ')                                            --   ADDRESS1
     || RPAD(NVL(hl.address2, ' '),50,' ')                                            --   ADDRESS2
     || RPAD(NVL(hl.address3, ' '),50,' ')                                            --   ADDRESS3
     || RPAD(NVL(hl.city, ' '),50,' ')                                                --   CITY
     || RPAD(NVL(hl.state, ' '),50,' ')                                               --   STATE
     || RPAD(NVL(hl.postal_code, ' '),20,' ')                                         --   ZIP
     || RPAD(NVL(hl.country, ' '),50,' ')                                             --   COUNTRY
     || RPAD(NVL( (SELECT raw_phone_number
                     FROM hz_contact_points 
                    WHERE owner_table_id     = hp.party_id
                      AND owner_table_name   = 'HZ_PARTIES' 
                      AND contact_point_type = 'PHONE' 
                      AND phone_line_type    = 'GEN'
                      AND status             = 'A'
                      AND ROWNUM             = 1)
                , ' ')
             , 35, ' ')                                                               --   PHONE
     || RPAD(NVL( (SELECT raw_phone_number
                     FROM hz_contact_points 
                    WHERE owner_table_id     = hp.party_id
                      AND owner_table_name   = 'HZ_PARTIES' 
                      AND contact_point_type = 'PHONE' 
                      AND phone_line_type    = 'FAX'
                      AND status             = 'A'
                      AND ROWNUM             = 1)
             , ' '), 35, ' ')                                                         --   FAXNO
     || RPAD(NVL(hcpc.attribute1, ' '),10,' ')                                        --   TYPE
     || RPAD(NVL(hca.comments, ' '),65,' ')                                           --   ARCOMMENT
     || LPAD(NVL(TO_CHAR(hcpa.overall_credit_limit,'9999999990.99'), '0.00'),20,' ')  --   LIMIT
     || RPAD(NVL((SELECT NVL(rsa.salesrep_number, '9999999999')
                    FROM apps.ra_salesreps      rsa
                   WHERE hcsu.primary_salesrep_id = rsa.salesrep_id
                     AND hcas.org_id = rsa.org_id
                     AND rownum = 1)
        , ' '),10,' ')                                                                --   SALESMN
     || RPAD(hp.party_name,20,' ')                                                    --   CONTACT
     || RPAD(' ',20,' ')                                                              --   TITLE
     || RPAD(NVL((SELECT TO_CHAR(MAX(acra.receipt_date), 'MMDDYYYY') 
                    FROM apps.ar_cash_receipts acra
                   WHERE acra.pay_FROM_customer = TO_CHAR(hca.cust_account_id) )
              , ' ')
              , 8, ' ')                                                               --   LASTPAY
     || RPAD(NVL(NVL((SELECT TO_CHAR(MAX(rcta.trx_date), 'MMDDYYYY') 
                        FROM apps.ra_customer_trx rcta
                       WHERE rcta.bill_to_customer_id = TO_CHAR(hca.cust_account_id) )  
                   , (SELECT TO_CHAR(ldate, 'MMDDYYYY')  
                        FROM xxwc.xxwc_arcust_getpaid_dump_tbl 
                       WHERE custno = hca.account_number
                         AND ROWNUM = 1))
              , ' ')
              , 8, ' ')                                                               --   LDATE  
     || RPAD(NVL((SELECT RPAD(al.meaning,10,' ')
                    FROM jtf_rs_defresources_v jrdv
                       , ar_lookups al
                   WHERE jrdv.SOURCE_ID = ac.employee_id
                     AND jrdv.user_name = al.lookup_code
                     AND al.lookup_type = 'XXWC_GETPAID_COLLECTOR_XREF'
                     AND sysdate between al.start_date_active and nvl(al.end_date_active, sysdate + 1)
                     AND al.enabled_flag = 'Y'
                     AND ROWNUM = 1)
                ,' ')
             , 10, ' ')                                                               --   TERR
     || LPAD(NVL(TO_CHAR((SELECT SUM(apsa.amount_due_remaining)
                            FROM apps.ar_payment_schedules apsa
                           WHERE apsa.customer_id = hca.cust_account_id
                 ),'9999999990.99') 
              , '0.00')
              , 20, ' ')                                                              --   BALANCE   
     || RPAD(' ',10,' ')                                                              --   REFKEY1
     || RPAD(' ',10,' ')                                                              --   REFKEY2
     || RPAD(' ',8,' ')                                                               --   INVCHGDT
     || RPAD(' ',10,' ')                                                              --   LOCCURR
     || RPAD(' ',10,' ')                                                              --   CREDITSCOR
     || RPAD(' ',10,' ')                                                              --   RESOLVER01
     || RPAD(' ',10,' ')                                                              --   RESOLVER02
     || RPAD(' ',10,' ')                                                              --   RESOLVER03
     || RPAD(' ',10,' ')                                                              --   RESOLVER04
     || RPAD(' ',10,' ')                                                              --   RESOLVER05
     || RPAD(' ',10,' ')                                                              --   RESOLVER06
     || RPAD(' ',10,' ')                                                              --   RESOLVER07
     || RPAD(' ',10,' ')                                                              --   RESOLVER08
     || RPAD(' ',10,' ')                                                              --   RESOLVER09
     || RPAD(' ',10,' ')                                                              --   RESOLVER10
     || RPAD(TO_CHAR(NVL(hca.ACCOUNT_ESTABLISHED_DATE, SYSDATE),'MMDDYYYY'),8 ,' ')   --   DATEOPEND
     || LPAD(NVL((SELECT TO_CHAR(MAX(ats.op_bal_high_watermark),'9999999990.99') 
                    FROM ar_trx_summary ats
                   WHERE ats.cust_account_id = hca.cust_account_id )
              , '0.00')
              , 20, ' ')                                                              --   HIGHBAL
     || LPAD(NVL((SELECT TO_CHAR(acra.amount,'9999999990.99')
                    FROM apps.ar_cash_receipts acra
                   WHERE acra.cash_receipt_id IN (SELECT MAX(acra2.cash_receipt_id) 
                                                    FROM apps.ar_cash_receipts acra2
                                                   WHERE acra2.pay_FROM_customer = TO_CHAR(hca.cust_account_id)
                                                  )
                   ), '0.00')
                   , 20, ' ')                                                         --   LASTAMT   
     || LPAD(NVL((SELECT NVL(TO_CHAR(SUM(amount_due_original),'9999999990.99'), '0.00')
                    FROM apps.ar_payment_schedules
                   WHERE customer_id               = hca.cust_account_id --10052
                     AND TO_CHAR(trx_date, 'YYYY') = (TO_CHAR(sysdate, 'YYYY')- 1 ))
            , ' ')
         , 20,' ')                                                                    --   LASTYSALES 
     || LPAD('0.00',20,' ')                                                           --   LASTQSALES 
     || RPAD(NVL(TO_CHAR(hp.duns_number), '0'),11,' ')                                --   DUNS
     || RPAD(' ',11,' ')                                                              --   ULTIMATEDUNS
     || RPAD(NVL(hop.tax_reference, ' '),11,' ')                                      --   FEDID
     || RPAD(' ',9,' ')                                                               --   BNKRTNUM
     || LPAD(NVL((SELECT NVL(TO_CHAR(SUM(amount_due_original),'9999999990.99'), '0.00')
                    FROM apps.ar_payment_schedules
                   WHERE customer_id               = hca.cust_account_id
                     AND TO_CHAR(trx_date, 'YYYY') = (TO_CHAR(sysdate, 'YYYY')))  
         , ' ')
         , 20,' ')
                                                                                      --   YTDSALES 
     || LPAD('0.00',20,' ')                                                           --   LTDSALES
     || RPAD(' ',8,' ')                                                               --   FINYE
     || RPAD(' ',8,' ')                                                               --   CREDREVDTE
     || RPAD(' ',8,' ')                                                               --   EXPCRDLMTDTE
     || RPAD(' ',8,' ')                                                               --   LCVDTE
     || RPAD('Accounts',50,' ')                                                       --   FIRSTNAME
     || RPAD('Payable',50,' ')                                                        --   LASTNAME 
     || RPAD(' ',20,' ')                                                              --   BUSINESSUNIT
     || RPAD(' ',10,' ')                                                              --   CREDIT_ACCT
     || RPAD(' ',10,' ')                                                              --   TICKER
     || RPAD(NVL(DECODE(credit_hold, 'Y','H', ' '), ' '),10,' ')                      --   CREDIT_STATUS
     || LPAD(NVL(hop.sic_code, '0'),10,' ')                                           --   SIC_CODE
     || RPAD(' ',4,' ')                                                               --   CUST_START_YR
     || RPAD(' ',4,' ')                                                               --   BUS_START_YR
     || RPAD(' ',8,' ')                                                               --   CRSCDT
     || RPAD(' ',8,' ')                                                               --   CRSTDT
     || RPAD(' ',8,' ')                                                               --   CRLIDT
     || RPAD(NVL(rt.description, ' '),10,' ')                                         --   SALES_TERM 
     || RPAD(NVL((SELECT email_address
                    FROM hz_contact_points 
                   WHERE owner_table_id     = hp.party_id 
                     AND owner_table_name   = 'HZ_PARTIES' 
                     AND contact_point_type = 'EMAIL' 
                     AND primary_flag       = 'Y'
                     AND status             = 'A'
                     AND ROWNUM             = 1)     
                  , ' '),50,' ')                                                      --   EMAIL
     || RPAD(' ',10,' ')                                                              --   ARBDR
     || RPAD(' ',10,' ')                                                              --   CMACCT_ID
     || RPAD(' ',8,' ')                                                               --   CRRVDT
     || RPAD(NVL((SELECT RPAD(jrdv.user_name,10,' ')
                    FROM jtf_rs_defresources_v jrdv
                   WHERE jrdv.SOURCE_ID = ac.employee_id
                     AND ROWNUM = 1)
                ,' ')
             , 10, ' ')                                                               --   PRISMTERRITORY
         ) REC_LINE
     , hcas.org_id ORG_ID                   
  FROM apps.hz_cust_accounts     hca
     , apps.hz_cust_acct_sites   hcas
     , apps.hz_cust_site_uses    hcsu
     , hz_parties               hp
     , hz_party_sites           hps
     , hz_locations             hl
     , hz_customer_profiles     hcp
     , hz_cust_profile_classes  hcpc
     , hz_cust_profile_amts     hcpa
     , hz_organization_profiles hop
--     , ra_salesreps_all         rsa
     , ra_terms                 rt
     , ar_collectors            ac
     , per_all_people_f         ppf
 WHERE 1 = 1
   AND EXISTS (SELECT '1'
                 FROM apps.ar_payment_schedules apsa
                WHERE 1 = 1
                  AND apsa.customer_id = hca.cust_account_id
                  AND apsa.amount_due_remaining != 0
              )
   AND hca.cust_account_id              = hcas.cust_account_id
   AND hcas.cust_acct_site_id           = hcsu.cust_acct_site_id
   AND hcsu.site_use_code               = 'BILL_TO'
   AND hca.party_id                     = hp.party_id
   AND hca.party_id                     = hps.party_id
   AND hp.party_id                      = hop.party_id   
   AND hcas.party_site_id               = hps.party_site_id
   AND hps.location_id                  = hl.location_id
   AND hca.cust_account_id              = hcp.cust_account_id
   AND hcp.site_use_id                 IS NULL
   AND hcp.cust_account_profile_id      = hcpa.cust_account_profile_id (+)
   AND hcp.profile_class_id             = hcpc.profile_class_id
   AND nvl(hcas.bill_to_flag, 'N')      = 'P'
   AND hcp.collector_id                 = ac.collector_id
   AND hcsu.status                      = 'A'   
--   AND hcsu.PRIMARY_SALESREP_ID         = rsa.salesrep_id (+)
--   AND rsa.org_id                       = hcas.org_id
   and hcp.standard_terms               = rt.term_id (+)
   and ac.employee_id                   = ppf.person_id (+)
   and SYSDATE BETWEEN NVL(hop.effective_start_date, SYSDATE - 1) AND NVL(hop.effective_end_date, SYSDATE + 1)
   and SYSDATE BETWEEN NVL(ppf.effective_start_date, SYSDATE - 1) AND NVL(ppf.effective_end_date, SYSDATE + 1);
