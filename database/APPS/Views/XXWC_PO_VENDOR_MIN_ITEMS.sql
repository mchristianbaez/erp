/* Formatted on 2/6/2014 3:39:29 PM (QP5 v5.206) */
-- Start of DDL Script for View APPS.XXWC_PO_VENDOR_MIN_ITEMS
-- Generated 2/6/2014 3:39:27 PM from APPS@EBIZDEV

CREATE OR REPLACE VIEW apps.xxwc_po_vendor_min_items
(
    sourcing_rule_name
   ,organization_code
   ,organization_id
   ,vendor_id
   ,vendor
   ,vendor_site_id
   ,vendor_site
   ,inventory_item_id
   ,segment1
   ,description
   ,lot_control_code
)
AS
    SELECT msr.sourcing_rule_name
          ,mp.organization_code
          ,mp.organization_id
          ,ass.vendor_id
          ,ass.vendor_name vendor
          ,assa.vendor_site_id
          ,assa.vendor_site_code vendor_site
          ,msib.inventory_item_id
          ,msib.segment1
          ,msib.description
          ,msib.lot_control_code
      FROM mrp_sourcing_rules msr
          ,mtl_parameters mp
          ,mrp_sr_receipt_org msro
          ,mrp_sr_source_org msso
          ,ap_suppliers ass
          ,apps.ap_supplier_sites assa
          ,mrp_sr_assignments msa
          ,mtl_system_items_b msib
          ,mrp_assignment_sets mas
     WHERE     mp.organization_id = NVL (msr.organization_id, mp.organization_id)
           AND msr.sourcing_rule_id = msro.sourcing_rule_id
           AND msro.sr_receipt_id = msso.sr_receipt_id
           AND msso.source_type = 3                                                                           --buy from
           AND msso.vendor_id = ass.vendor_id
           AND msso.vendor_site_id = assa.vendor_site_id
           AND ass.vendor_id = assa.vendor_id
           AND SYSDATE BETWEEN msro.effective_date AND NVL (msro.disable_date, SYSDATE)
           AND msr.sourcing_rule_id = msa.sourcing_rule_id
           AND NVL (msr.organization_id, mp.organization_id) = msa.organization_id
           AND msib.inventory_item_id = msa.inventory_item_id
           AND msib.organization_id = msa.organization_id
           -- 01/22/13 CG: TMS 20130121-00856. Remove inventory sourced items from the Vendor Minimum Form
           AND NVL (msib.source_type, -1) != 1
           AND msa.assignment_set_id = mas.assignment_set_id
           AND msso.source_type = 3                                                                           --Buy From
           AND mas.assignment_set_id = fnd_profile.VALUE ('MRP_DEFAULT_ASSIGNMENT_SET')
           AND (NVL (msib.min_minmax_quantity, 0) <> 0 OR NVL (msib.max_minmax_quantity, 0) <> 0)
           AND NVL (xxwc_mv_routines_pkg.get_item_category (msib.inventory_item_id
                                                           ,mp.organization_id
                                                           ,'Sales Velocity'
                                                           ,1)
                   ,'ZZ') != 'N'
/

-- End of DDL Script for View APPS.XXWC_PO_VENDOR_MIN_ITEMS
