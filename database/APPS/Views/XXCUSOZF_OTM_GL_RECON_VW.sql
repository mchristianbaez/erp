
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUSOZF_OTM_GL_RECON_VW" ("PERIOD_NAME", "PERIOD_ID", "GL_TRANSFER_STATUS", "OTM_RECEIVABLE_TOTAL", "OTM_INCOME_TOTAL", "GL_RECEIVABLE_TOTAL", "GL_INCOME_TOTAL") AS 
  SELECT aeh.period_name PERIOD_NAME ,
       period.ent_period_id PERIOD_ID,
       aeh.gl_transfer_status_code GL_TRANSFER_STATUS,
      -- sum(acctd_amount) accr_amount,
       sum(ael1.accounted_dr) -  sum(ael1.accounted_cr)   OTM_RECEIVABLE_TOTAL,
       sum(ael2.accounted_cr) -  sum(ael2.accounted_dr)   OTM_INCOME_TOTAL,
       sum(NVL(gir1.reference_9,0)) - sum(NVL(gir1.reference_10,0))  GL_RECEIVABLE_TOTAL,
       sum(NVL(gir2.reference_10,0)) -  sum(NVL(gir2.reference_9,0)) GL_INCOME_TOTAL

FROM   xla_ae_headers aeh,
       xla_ae_lines ael1,
       xla_ae_lines ael2,
       xla_events eve,
    -- ozf_xla_accruals accr,
    -- ozf_funds_utilized_all_b ofu,
       OZF.ozf_time_ent_period period,
       gl_import_references gir1,
       gl_import_references gir2
WHERE 1=1
  AND  aeh.application_id=682
  AND  aeh.ae_header_id=ael1.ae_header_id
  AND  aeh.ae_header_id=ael2.ae_header_id
  AND  period.name=aeh.period_name
  AND  ael1.accounting_class_code = 'EXPENSE'
  AND  ael2.accounting_class_code='LIABILITY'
  AND  aeh.event_id=eve.event_id
  AND  gir1.gl_sl_link_id(+)=ael1.gl_sl_link_id
  AND  gir2.gl_sl_link_id(+)=ael2.gl_sl_link_id
--AND  accr.event_id=eve.event_id
--AND  ofu.utilization_id=accr.utilization_id
GROUP BY aeh.period_name,
         period.ent_period_id,
         aeh.gl_transfer_status_code
ORDER BY 2,3
;
