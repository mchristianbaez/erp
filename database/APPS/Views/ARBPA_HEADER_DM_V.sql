CREATE OR REPLACE FORCE VIEW apps.arbpa_header_DM_v (
customer_trx_id,
customer_trx_line_id,
line_number,
line_type,
description,
quantity,
unit_of_measure_name,
unit_price,
extended_amount,
sales_order,
uom_code,
trx_number,
tax_exists_for_this_line_flag,
line_tax_rate,
tax_code,
printed_tax_name,
interface_line_attribute1,
interface_line_attribute2,
interface_line_attribute3,
interface_line_attribute4,
interface_line_attribute5,
interface_line_attribute6,
interface_line_attribute7,
interface_line_attribute8,
interface_line_attribute9,
interface_line_attribute10,
interface_line_attribute11,
interface_line_attribute12,
interface_line_attribute13,
interface_line_attribute14,
interface_line_attribute15,
unformatted_unit_price,
attribute1,
attribute2,
attribute3,
attribute4,
attribute5,
attribute6,
attribute7,
attribute8,
attribute9,
attribute10,
attribute11,
attribute12,
attribute13,
attribute14,
attribute15,
set_of_books_id,
reason_code,
quantity_ordered,
quantity_credited,
unit_standard_price,
sales_order_line,
sales_order_date,
accounting_rule_duration,
attribute_category,
rule_start_date,
interface_line_context,
sales_order_source,
revenue_amount,
default_ussgl_transaction_code,
default_ussgl_trx_code_context,
last_period_to_credit,
item_context,
tax_exempt_flag,
tax_exempt_number,
tax_exempt_reason_code,
tax_vendor_return_code,
global_attribute_category,
gross_unit_selling_price,
gross_extended_amount,
extended_acctd_amount,
mrc_extended_acctd_amount,
org_id,
global_attribute1,
global_attribute2,
global_attribute3,
global_attribute4,
global_attribute5,
global_attribute6,
global_attribute7,
global_attribute8,
global_attribute9,
global_attribute10,
global_attribute11,
global_attribute12,
global_attribute13,
global_attribute14,
global_attribute15,
global_attribute16,
global_attribute17,
global_attribute18,
global_attribute19,
global_attribute20,
inventory_item_id,
term_desc,
branch,
branch_name,
delivery_id,
address1,  -- TMS# 20140116-00093
address2,  -- TMS# 20140116-00093
address3,  -- TMS# 20140116-00093
address4,  -- TMS# 20140116-00093
address5,   -- TMS# 20140116-00093
remit_to_address_code,  -- TMS# 20140116-00093
remit_to_ln1,  -- TMS# 20140116-00093
remit_to_ln2,  -- TMS# 20140116-00093
remit_to_ln3,  -- TMS# 20140116-00093
remit_to_ln4
)
AS
SELECT lines.customer_trx_id customer_trx_id,
lines.customer_trx_line_id customer_trx_line_id,
TO_CHAR (lines.line_number) line_number,
lines.line_type line_type,
NVL (ar_invoice_sql_func_pub.get_description(lines.customer_trx_line_id),
lines.description) description,
TO_CHAR (NVL (lines.quantity_invoiced, lines.quantity_credited)) quantity,
uom.unit_of_measure unit_of_measure_name,
TO_CHAR (NVL (lines.unit_selling_price, 0),
fnd_currency.get_format_mask (trx.invoice_currency_code, 40)) unit_price,
TO_CHAR (lines.extended_amount,
fnd_currency.get_format_mask (trx.invoice_currency_code, 40)) extended_amount,
lines.sales_order,
lines.uom_code,
trx.trx_number,
ar_invoice_sql_func_pub.get_taxyn (lines.customer_trx_line_id)
tax_exists_for_this_line_flag,
ar_bpa_utils_pkg.fn_get_line_taxrate (lines.customer_trx_line_id)
line_tax_rate,
ar_bpa_utils_pkg.fn_get_line_taxcode (lines.customer_trx_line_id)
tax_code,
ar_bpa_utils_pkg.fn_get_line_taxname (lines.customer_trx_line_id)
printed_tax_name,
lines.interface_line_attribute1,
lines.interface_line_attribute2,
lines.interface_line_attribute3,
lines.interface_line_attribute4,
lines.interface_line_attribute5,
lines.interface_line_attribute6,
lines.interface_line_attribute7,
lines.interface_line_attribute8,
lines.interface_line_attribute9,
lines.interface_line_attribute10,
lines.interface_line_attribute11,
lines.interface_line_attribute12,
lines.interface_line_attribute13,
lines.interface_line_attribute14,
lines.interface_line_attribute15,
TO_CHAR (NVL (lines.unit_selling_price, 0)) unformatted_unit_price,
lines.attribute1,
lines.attribute2,
lines.attribute3,
lines.attribute4,
lines.attribute5,
lines.attribute6,
lines.attribute7,
lines.attribute8,
lines.attribute9,
lines.attribute10,
lines.attribute11,
lines.attribute12,
lines.attribute13,
lines.attribute14,
lines.attribute15,
lines.set_of_books_id,
lines.reason_code,
lines.quantity_ordered,
lines.quantity_credited,
lines.unit_standard_price,
lines.sales_order_line,
lines.sales_order_date,
lines.accounting_rule_duration,
lines.attribute_category,
lines.rule_start_date,
lines.interface_line_context,
lines.sales_order_source,
lines.revenue_amount,
lines.default_ussgl_transaction_code,
lines.default_ussgl_trx_code_context,
lines.last_period_to_credit,
lines.item_context,
lines.tax_exempt_flag,
lines.tax_exempt_number,
lines.tax_exempt_reason_code,
lines.tax_vendor_return_code,
lines.global_attribute_category,
lines.gross_unit_selling_price,
lines.gross_extended_amount,
lines.extended_acctd_amount,
lines.mrc_extended_acctd_amount,
lines.org_id,
lines.global_attribute1,
lines.global_attribute2,
lines.global_attribute3,
lines.global_attribute4,
lines.global_attribute5,
lines.global_attribute6,
lines.global_attribute7,
lines.global_attribute8,
lines.global_attribute9,
lines.global_attribute10,
lines.global_attribute11,
lines.global_attribute12,
lines.global_attribute13,
lines.global_attribute14,
lines.global_attribute15,
lines.global_attribute16,
lines.global_attribute17,
lines.global_attribute18,
lines.global_attribute19,
lines.global_attribute20,
lines.inventory_item_id,
trm.description term_desc,
org.organization_code branch,
org.organization_name branch_name,
XXWC_AR_BILL_TRUST_INTF_PKG.get_delivery_id(trx.CUSTOMER_TRX_ID) delivery_id, -- TMS# 20140116-00093
hl_bill.address1,  -- TMS# 20140116-00093
hl_bill.address2,  -- TMS# 20140116-00093
hl_bill.address3,  -- TMS# 20140116-00093
hl_bill.address4,  -- TMS# 20140116-00093
hl_bill.city||', '||hl_bill.state||' '||hl_bill.postal_code address5   -- TMS# 20140116-00093
, NVL (hcp.attribute2, '2') remit_to_address_code
, SUBSTR (flv.description, 1, INSTR (flv.description
                  , ','
                  , 1
                  , 1)
             - 1)
 remit_to_ln1
, SUBSTR (flv.description, INSTR (flv.description
                , ','
                , 1
                , 1)
               + 2,   INSTR (flv.description
                   , ','
                   , 1
                   , 2)
                - INSTR (flv.description
                   , ','
                   , 1
                   , 1)
                - 2)
 remit_to_ln2
, SUBSTR (flv.description, INSTR (flv.description
                , ','
                , 1
                , 2)
               + 2,   INSTR (flv.description
                   , ','
                   , 1
                   , 3)
                - INSTR (flv.description
                   , ','
                   , 1
                   , 2)
                - 2)
 ||', '|| DECODE (INSTR (flv.description
       , ','
       , 1
       , 4)
    , 0
    , SUBSTR (flv.description, INSTR (flv.description
                    , ','
                    , 1
                    , 3)
                   + 2,   LENGTH (flv.description)
                    - INSTR (flv.description
                       , ','
                       , 1
                       , 3)
                    - 1)
    , SUBSTR (flv.description, INSTR (flv.description
                    , ','
                    , 1
                    , 3)
                   + 2,   INSTR (flv.description
                       , ','
                       , 1
                       , 4)
                    - INSTR (flv.description
                       , ','
                       , 1
                       , 3)
                    - 2))
 remit_to_ln3
, (CASE
  WHEN INSTR (flv.description
        , ','
        , 1
        , 4) = 0
       AND INSTR (flv.description
        , ','
        , 1
        , 5) = 0
  THEN
     NULL
  ELSE
     SUBSTR (flv.description, INSTR (flv.description
                   , ','
                   , 1
                   , 4)
                  + 2, LENGTH (flv.description)
                   - INSTR (flv.description
                      , ','
                      , 1
                      , 5))
END)
 remit_to_ln4
FROM mtl_units_of_measure_tl uom,
ra_customer_trx_lines_all lines,
ra_customer_trx_all trx,
ra_terms trm,
ar.hz_cust_accounts hca_bill,             -- TMS# 20140116-00093  
ar.hz_cust_site_uses_all  hcsu_bill,      -- TMS# 20140116-00093
ar.hz_cust_acct_sites_all hcasa_bill,     -- TMS# 20140116-00093
ar.hz_party_sites         hps_bill,       -- TMS# 20140116-00093
ar.hz_locations           hl_bill,        -- TMS# 20140116-00093
fnd_lookup_values         flv,            -- TMS# 20140116-00093
ar.hz_customer_profiles   hcp,             -- TMS# 20140116-00093
ra_batch_sources_all      rbsa, -- TMS# 20140116-00093
org_organization_definitions org
WHERE trx.customer_trx_id = lines.customer_trx_id
AND trx.complete_flag = 'Y'
AND trx.term_id = trm.term_id
AND lines.uom_code = uom.uom_code(+)
AND lines.line_type = 'LINE'
AND trx.org_id = lines.org_id
-- TMS# 20140116-00093 > Start
--and trx.interface_header_attribute10 = to_char(org.organization_id)
and rbsa.batch_source_id = trx.batch_source_id 
and rbsa.org_id          = 162 
AND (   (rbsa.name NOT LIKE 'PRISM%' AND NVL (trx.interface_header_attribute10, 227) = TO_CHAR (org.organization_id))
   OR (rbsa.name LIKE 'PRISM%' AND NVL (LPAD (trx.interface_header_attribute7, 3, '0'), 'WCC') = org.organization_code)
  )
AND hca_bill.cust_account_id          = trx.bill_to_customer_id
AND hcasa_bill.cust_account_id        = hca_bill.cust_account_id
AND hcasa_bill.status                 = 'A'                        
AND hcasa_bill.bill_to_flag           = 'P'                  
AND hcasa_bill.cust_acct_site_id      = hcsu_bill.cust_acct_site_id
AND hcsu_bill.site_use_code           = 'BILL_TO'
AND NVL (hcsu_bill.primary_flag, 'N') = 'Y'
AND hcasa_bill.party_site_id          = hps_bill.party_site_id
AND hca_bill.cust_account_id          = hcp.cust_account_id
AND hcp.site_use_id                  IS NULL
AND hps_bill.location_id              = hl_bill.location_id
AND NVL (hcp.attribute2, '2')         = flv.lookup_code
AND flv.lookup_type                   = 'XXWC_REMIT_TO_ADDRESS_CODES'
-- TMS# 20140116-00093 < End
-- AND uom.language = 'US';
