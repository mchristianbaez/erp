
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUS_UNEARNEDREBATE_V" ("LOB", "PERIOD", "CURRENT_AMOUNT", "ROLLING_AMOUNT") AS 
  select
 b.lob_name lob,b.period period
, sum( case when a.period_seq =b.period_seq then nvl(a.amount,0) else 0 end ) Current_amount
,sum( case when a.period_seq between b.period_seq-11 and b.period_seq  then nvl(a.amount,0) else 0 end) Rolling_Amount
from (select p.period_seq period_seq,p.ent_period_id period_id,p.name period, t.lob_id lob_id
,t.party_name lob_name
, nvl(t.accrued_amount,0) amount
from
(select row_number() over (order by ent_period_id) period_seq,ent_period_id, name
from ozf_time_ent_period) p
left outer
join ( select accr.period_id period_id, accr.lob_id lob_id, accr.accrued_amount accrued_amount,hp.party_name party_name
from xxcusozf_accruals_mv accr join
 hz_parties hp on hp.party_id=accr.lob_id
where  accr.grp_mvid=1
AND accr.grp_branch=1
AND accr.grp_adj_type=1
AND accr.grp_rebate_type=1
AND accr.grp_plan_id=1
AND accr.grp_qtr=1
AND accr.grp_year=0
AND accr.grp_lob=0
AND accr.grp_period=0
and accr.grp_cal_year=0
and accr.lob_id not in ('2063','2064')) t on p.ent_period_id=t.period_id
--where p.name='Jun-2013'
order by t.lob_id,p.period_seq)   a  --left outer
join (select lob_name,lob_id,period,period_seq from  (select p.period_seq period_seq,p.ent_period_id period_id,p.name period, t.lob_id lob_id
,t.party_name lob_name
, nvl(t.accrued_amount,0) amount
from
(select row_number() over (order by ent_period_id) period_seq,ent_period_id, name
from ozf_time_ent_period) p
left outer
join ( select accr.period_id period_id, accr.lob_id lob_id, accr.accrued_amount accrued_amount,hp.party_name party_name
from xxcusozf_accruals_mv accr join
 hz_parties hp on hp.party_id=accr.lob_id
where  accr.grp_mvid=1
AND accr.grp_branch=1
AND accr.grp_adj_type=1
AND accr.grp_rebate_type=1
AND accr.grp_plan_id=1
AND accr.grp_qtr=1
AND accr.grp_year=0
AND accr.grp_lob=0
AND accr.grp_period=0
and accr.grp_cal_year=0
and accr.lob_id not in ('2063','2064')) t on p.ent_period_id=t.period_id
--where p.name='Jun-2013'
order by t.lob_id,p.period_seq)
where period in (select distinct name from  ozf_time_ent_period)) b
on a.lob_id=b.lob_id
where  a.period_seq between b.period_seq-11 and b.period_seq
group by b.lob_name,b.period
order by b.lob_name
;
