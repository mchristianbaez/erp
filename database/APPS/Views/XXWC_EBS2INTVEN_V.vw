--
-- XXWC_EBS2INTVEN_V  (View)
--

CREATE OR REPLACE FORCE VIEW APPS.XXWC_EBS2INTVEN_V
(
   REC_LINE
  ,ORG_ID
)
AS
     SELECT    pvs.vendor_site_code                                --  COMPANY
            || '|'
            || pv.segment1                                    --  VENDORNUMBER
            || '|'
            || pv.vendor_name                                         --  NAME
            || '|'
            || pvs.address_line1                                  --  ADDRESS1
            || '|'
            || pvs.address_line2                                  --  ADDRESS2
            || '|'
            || pvs.city                                               --  CITY
            || '|'
            || pvs.state                                             --  STATE
            || '|'
            || pvs.zip                                          --  POSTALCODE
            || '|'
            || DECODE (
                  pvc.area_code
                 ,NULL, pvc.phone
                 ,   pvc.area_code
                  || '-'
                  || SUBSTR (pvc.phone, 1, 3)
                  || '-'
                  || REPLACE (REPLACE (SUBSTR (pvc.phone, 4, 7), '-', '')
                             ,'.'
                             ,''))
            --  TELEPHONE
            || '|'
            || DECODE (
                  pvc.fax_area_code
                 ,NULL, pvc.fax
                 ,   pvc.fax_area_code
                  || '-'
                  || SUBSTR (pvc.fax, 1, 3)
                  || '-'
                  || REPLACE (REPLACE (SUBSTR (pvc.fax, 4, 7), '-', '')
                             ,'.'
                             ,''))
            --  FAX
            || '|'
            || apt.description                                       --  TERMS
            || '|'
            || pvc.first_name
            || ' '
            || pvc.last_name                                       --  CONTACT
            || '|'
            || SUBSTR (pv.vendor_type_lookup_code, 1, 1)        --  VENDORTYPE
            || '|'
            || atl.DISCOUNT_PERCENT                               --  DISCOUNT
               REC_LINE
           ,pvs.ORG_ID
       FROM po_vendors pv
           ,apps.po_vendor_sites pvs
           ,ap.ap_terms_tl apt
           ,AP_TERMS_LINES atl
           ,po_vendor_contacts pvc
      WHERE     1 = 1
            AND pv.vendor_id = pvs.vendor_id
            AND pvc.vendor_site_id(+) = pvs.vendor_site_id
            AND NVL (pvs.terms_id, pv.terms_id) = apt.term_id
            AND apt.term_id = atl.term_id
            AND NVL (pvs.inactive_date, SYSDATE + 1) > SYSDATE
            AND SYSDATE BETWEEN apt.start_date_active
                            AND NVL (apt.end_date_active, SYSDATE + 1)
            AND SYSDATE BETWEEN pv.start_date_active
                            AND NVL (pv.end_date_active, SYSDATE + 1)
            AND vendor_type_lookup_code IN
                   ('VENDOR', 'EXPENSE', 'UTILITY', 'FREIGHT')
   ORDER BY pv.segment1;


