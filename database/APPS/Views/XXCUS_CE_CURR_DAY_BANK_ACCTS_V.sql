/*
 MODULE: CASH MANAGEMENT
 AUTHOR: BALAGURU SESHADRI
 DATE: 10/24/2016
 SCOPE: HDS TREASURY: PROCESS CURRENT DAY BANK BAI FILES
 TICKET: ESMS 322411 / TMS 20160901-00241
*/
--
CREATE OR REPLACE FORCE VIEW APPS.XXCUS_CE_CURR_DAY_BANK_ACCTS_V
(
   BANK_NAME,
   BANK_BRANCH_NAME,
   BANK_ACCOUNT_NAME,
   BANK_ACCOUNT_NUM,
   CURRENCY_CODE,
   CURRENT_DAY_FILE_NAME,
   BANK_FILE_CREATE_DATE,
   EBS_JOB_ARGUMENT1,
   BANK_FILE_SENDER_ID,
   BANK_FILE_RECEIVER_ID
)
AS
   SELECT DISTINCT BANK_NAME,
          BANK_BRANCH_NAME,
          BANK_ACCOUNT_NAME,
          BANK_ACCOUNT_NUM,
          CURRENCY_CODE,
          CURRENT_DAY_FILE_NAME,
          BANK_FILE_CREATE_DATE,
          JOB_PARAMETER EBS_JOB_ARGUMENT1,
          SENDER_ID BANK_FILE_SENDER_ID,
          RECEIVER_ID BANK_FILE_RECEIVER_ID
     FROM XXCUS.XXCUS_CE_STMT_HDR_INT
    WHERE 1 = 1;
--
COMMENT ON TABLE APPS.XXCUS_CE_CURR_DAY_BANK_ACCTS_V IS 'ESMS 322411 / TMS 20160901-00241';
--
