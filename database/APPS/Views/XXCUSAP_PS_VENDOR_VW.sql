
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUSAP_PS_VENDOR_VW" ("VENDOR_SITE_NAME", "VENDOR_NAME", "ADDRESS_LINE1", "ADDRESS_LINE2", "ADDRESS_LINE3", "ADDRESS_LINE4", "CITY", "STATE", "ZIP", "FIPS_NUMBER", "SITE_CREATION_DATE", "SITE_LAST_UPDATE_DATE", "VENDOR_INACTIVE_DATE", "SITE_INACTIVE_DATE", "VENDOR_ID", "VENDOR_SITE_ID", "VENDOR_CREATION_DATE", "VENDOR_LAST_UPDATE_DATE", "PAYMENT_METHOD") AS 
  select s.vendor_site_code vendor_site_name,
       replace(v.vendor_name,',','') vendor_name,
       replace(s.address_line1,',','') address_line1,
       replace(s.address_line2,',','') address_line2,
       replace(s.address_line3,',','') address_line3,
       replace(s.address_line4,',','') address_line4,
       replace(s.city,',','') city,
       SUBSTR(replace(NVL(s.state, s.province),',',''),1,6) state, -- only six char allowed in PeopleSoft
       replace(s.zip,',','') zip,
       replace(s.attribute1,',','') FIPS_number,
       s.creation_date site_creation_date,
       s.last_update_date site_last_update_date,
       v.end_date_active vendor_inactive_date,
       s.inactive_date site_inactive_date,
       v.vendor_id,   -- Added to keep
       s.vendor_site_id,
       v.creation_date vendor_creation_date, -- Added to check vendor creation
       v.last_update_date vendor_last_update_date, -- Added to check vendor change
       s.payment_method_lookup_code payment_method
  from ap.ap_suppliers v, ap.ap_supplier_sites_all s
 where v.vendor_id = s.vendor_id
   and s.org_id = 166
;
