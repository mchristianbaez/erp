CREATE OR REPLACE VIEW APPS.XXWC_B2B_CAT_ATTR_VW
(attribute_name, attribute_id)
AS
/*************************************************************************
 Copyright (c) 2012 HD Supply
 All rights reserved.
**************************************************************************
   $Header XXWC.XXWC_B2B_CAT_ATTR_VW$
  Module Name: XXWC.XXWC_B2B_CAT_ATTR_VW

  PURPOSE:

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)   TMS TASK        DESCRIPTION
  -- ------- -----------   ------------ ---------      -------------------------
  -- 1.0     26-Mar-2016   Pahwa, Nancy                Initially Created 
TMS# 20160223-00029   
**************************************************************************/
SELECT eav.attr_Display_name  , eav.ATTR_ID
FROM apps.fnd_flex_value_sets ffvs,
  apps.fnd_flex_values_vl ffv,
  apps.EGO_ATTRS_V eav
WHERE ffvs.flex_value_set_name              = 'XXWC_EXCLUDED_UDA_VS'
AND ffvs.flex_value_set_id                  = ffv.flex_value_set_id
AND eav.attr_name                           = ffv.flex_value
AND ffv.enabled_flag                        = 'Y'
AND NVL ( ffv.end_date_active, SYSDATE + 1) > SYSDATE;
/
