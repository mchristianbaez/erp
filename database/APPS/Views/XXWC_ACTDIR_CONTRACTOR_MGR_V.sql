--//============================================================================
--//
--// Object Name         :: APPS.XXWC_ACTDIR_CONTRACTOR_MGR_V
--//
--// Object Type         :: View
--//
--// Object Description  :: This view shows the enterprise wide contractor and their managers.
--//
--// Version Control
--//============================================================================
--// Vers    Author             Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Bala Seshadri     07/11/2018    Enterprise contractor - manager view (TMS: 20180710-00075)
--//============================================================================
DROP VIEW APPS.XXWC_ACTDIR_CONTRACTOR_MGR_V;
CREATE OR REPLACE FORCE VIEW APPS.XXWC_ACTDIR_CONTRACTOR_MGR_V
(
   CONTRACTORS_NTID,
   CONTRACTOR_ID,
   CONTRACTOR_LAST_NAME,
   CONTRACTOR_FIRST_NAME,
   CONTRACTOR_JOB_TITLE,
   WORK_PHONE,
   WORK_PHONE_EXTENSION,
   CONTRACTOR_COMPANY,
   MANAGER_ID,
   MANAGER_FIRST_NAME,
   MANAGER_LAST_NAME
)
AS
   SELECT CONTRACTORS.NTID CONTRACTORS_NTID,
          TO_CHAR (TO_NUMBER (REGEXP_REPLACE (CONTRACTORS.NTID, '[^0-9]+')))
             CONTRACTOR_ID,
          CONTRACTORS.LAST_NAME CONTRACTOR_LAST_NAME,
          CONTRACTORS.FIRST_NAME CONTRACTOR_FIRST_NAME,
          CONTRACTORS.JOB_DESCR CONTRACTOR_JOB_TITLE,
          CONTRACTORS.WORK_PHONE WORK_PHONE,
          CONTRACTORS.WORK_EXT WORK_PHONE_EXTENSION,
          CONTRACTORS.CONTRACTORCOMPANY CONTRACTOR_COMPANY,
          CASE
             WHEN SPVR_EMPLID <> '00000'
             THEN
                TO_CHAR (
                   TO_NUMBER (
                      REGEXP_REPLACE (CONTRACTORS.SPVR_EMPLID, '[^0-9]+')))
             ELSE
                TO_CHAR (
                   TO_NUMBER (
                      REGEXP_REPLACE (CONTRACTORS.SPVR_NTID, '[^0-9]+')))
          END
             MANAGER_ID,
          CONTRACTORS.SPVR_FIRST_NAME MANAGER_FIRST_NAME,
          CONTRACTORS.SPVR_LAST_NAME MANAGER_LAST_NAME
     FROM HDSCMMN.HR_AD_ALL_VW@eaapxprd.hsi.hughessupply.com CONTRACTORS
          LEFT OUTER JOIN
          HDSCMMN.HR_EMPLOYEE_ALL_VW@eaapxprd.hsi.hughessupply.com EMP
             ON TO_NUMBER (REGEXP_REPLACE (CONTRACTORS.NTID, '[^0-9]+')) =
                   EMP.EMPLID
          LEFT OUTER JOIN
          HDSCMMN.ET_LOCATION_CODE@eaapxprd.hsi.hughessupply.com ET
             ON CONTRACTORS.FRU = ET.FRU
          LEFT OUTER JOIN
          HDSCMMN.CONCUR_BU_XREF@eaapxprd.hsi.hughessupply.com CONTROL_TABLE
             ON CONTROL_TABLE.BUSINESS_UNIT = CONTRACTORS.PS_BUSINESS_UNIT
          LEFT OUTER JOIN
          HDSCMMN.CONCUR_FRU_OVERRIDE@eaapxprd.hsi.hughessupply.com FRUO
             ON CONTRACTORS.FRU = FRUO.FRU
          LEFT OUTER JOIN
          HDSCMMN.CONCUR_EMP_OVERRIDE@eaapxprd.hsi.hughessupply.com EMPO
             ON EMP.EMPLID = EMPO.EMPLID
    WHERE     1 = 1
          AND CONTRACTORS.USERTYPE = 'Contractor'
          AND CONTRACTORS.EMAIL_ADDR IS NOT NULL
          AND TO_NUMBER (REGEXP_REPLACE (CONTRACTORS.NTID, '[^0-9]+'))
                 IS NOT NULL
          AND TO_NUMBER (REGEXP_REPLACE (CONTRACTORS.SPVR_NTID, '[^0-9]+'))
                 IS NOT NULL
          AND (   (    (TRUNC (CONTRACTORS.TERMINATION_DT) + 90) >=
                          TRUNC (SYSDATE)
                   AND CONTROL_TABLE.BUSINESS_UNIT <> 'WW1US')
               OR (    CONTRACTORS.USER_ACCT_STATUS_CODE = 'A'
                   AND CONTROL_TABLE.BUSINESS_UNIT <> 'WW1US'))
          AND (   CONTROL_TABLE.BUSINESS_UNIT IS NOT NULL
               OR FRUO.FRU IS NOT NULL)
          AND ET.ENTRP_ENTITY <> 'PPH'
/
COMMENT ON TABLE APPS.XXWC_ACTDIR_CONTRACTOR_MGR_V IS 'TMS: 20180710-00075'
/
GRANT SELECT ON APPS.XXWC_ACTDIR_CONTRACTOR_MGR_V TO XXCUS
/
GRANT SELECT ON APPS.XXWC_ACTDIR_CONTRACTOR_MGR_V TO XXWC
/