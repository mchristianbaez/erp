
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUS_TENANCY_LEASE_V" ("TENANCY_ID", "TENANCY_LEASE_ID", "TENANCY_LOC_ID", "TENANCY_USAGE", "TENANCY_LOC_TYPE", "TENANCY_LOCATION_CODE", "TENANCY_LOC_TYPE_CODE", "PARENT_LOCATION_ID", "OFFICE", "BLDG_RENTABLE_AREA", "LAND_RENTABLE_AREA") AS 
  select ten.tenancy_id 
      ,ten.lease_id tenancy_lease_id
      ,ten.location_id tenancy_loc_id
      ,(
         select meaning
         from   fnd_lookups
         where  1 =1
           and  lookup_type ='PN_TENANCY_USAGE_TYPE'
           and  lookup_code =ten.tenancy_usage_lookup_code
       ) tenancy_usage
      ,(
        select meaning
        from   fnd_lookups
        where  1 =1
          and  lookup_type ='PN_LOCATION_TYPE'
          and  lookup_code =ten_loc.location_type_lookup_code
       ) tenancy_loc_type
      ,ten_loc.location_code tenancy_location_code
      ,ten_loc.location_type_lookup_code tenancy_loc_type_code
      ,ten_loc.parent_location_id
      ,ten_loc.office
      ,case
         when ten_loc.location_type_lookup_code IN ('FLOOR', 'OFFICE') then (ten_loc.rentable_area)
         when ten_loc.location_type_lookup_code ='BUILDING' then (ten_loc.gross_area)
         else to_number(null) 
       end bldg_rentable_area
      ,case
        when ten_loc.location_type_lookup_code ='LAND' then (ten_loc.gross_area)
        when ten_loc.location_type_lookup_code IN ('SECTION', 'PARCEL') then ten_loc.rentable_area
        else to_number(null) 
       end land_rentable_area
from pn_tenancies_all ten
    ,pn_locations_all ten_loc
where 1 =1
and ten_loc.location_id(+) =ten.location_id
and trunc(sysdate) between ten_loc.active_start_date and ten_loc.active_end_date;
