/*
History
 TMS                             ESMS         Ver    Author                       Comments
 ============================================================================================                  
                                              1.0   Balaguru Seshadri              Header missing      
 20160815-00072                  438366       1.1   Balaguru Seshadri              Add new columns       
*/
--
DROP VIEW APPS.XXCUSPN_LEASE_RENT_VW;
--
CREATE OR REPLACE FORCE VIEW APPS.XXCUSPN_LEASE_RENT_VW
(
   LOB,
   RER_ID,
   PRIME_LOC,
   PRIME_ADDRESS,
   PRIME_CITY,
   PRIME_ST_PRV,
   POSTAL_CODE,
   RER_TYPE,
   COMM_DATE,
   EXP_DATE,
   RER_STATUS,
   ORACLE_ID,
   FRU_ID,
   BR_#,
   PAYMENT_TERM_TYPE,
   YEAR,
   JANUARY,
   FEBRUARY,
   MARCH,
   APRIL,
   MAY,
   JUNE,
   JULY,
   AUGUST,
   SEPTEMBER,
   OCTOBER,
   NOVEMBER,
   DECEMBER,
   ANNUAL_TOTAL,
   START_DT,
  VENDOR_CODE, --Ver 1.1
  VENDOR_NAME, --Ver 1.1
  FREQUENCY, --Ver 1.1
  GL_CODE, --Ver 1.1
  TERM_COMMENTS --Ver 1.1   
)
AS
   SELECT lh.lob,
          lh.lease_num rer_id,
          lh.location_code prime_loc,
          lh.address prime_address,
          lh.city prime_city,
          CASE lh.country WHEN 'US' THEN lh.state ELSE lh.province END
             prime_st_prv,
          LH.Zip_Code prime_postal_code,
          lh.lease_type rer_type,
          lh.lease_commencement_date comm_date,
          lh.lease_termination_date exp_date,
          lh.lease_status rer_status,
          lp.gl_location oracle_id,
          lp.fru fru_id,
          lp.lob_branch br_#,
          lp.payment_term_type,
          lp.year_ca year,
          lp.january,
          lp.february,
          lp.march,
          lp.april,
          lp.may,
          lp.june,
          lp.july,
          lp.august,
          lp.september,
          lp.october,
          lp.november,
          lp.december,
          lp.annual_total,
          NULL start_dt,
          ( --Begin Ver 1.1
            select /*+ RESULT_CACHE */  segment1
            from apps.ap_suppliers
            where 1 =1
            and vendor_id =lp.vendor_id
          ) vendor_code, --End Ver 1.1
          lp.vendor_name, --Ver 1.1
          lp.frequency_code frequency, --Ver 1.1
          lp.gl_account gl_code, --Ver 1.1
          (  --Begin Ver 1.1
            case
              when lp.frequency_code <>'Monthly' then substr(term_comments, 1, 2000)
              else Null -- for monthly terms print blank
            end
          ) term_comments --End Ver 1.1
     FROM xxcus.xxcuspn_lease_headers_tbl lh,
          xxcus.xxcuspn_lease_payments_tbl lp
    WHERE lh.lease_id = lp.lease_id;
--
COMMENT ON TABLE APPS.XXCUSPN_LEASE_RENT_VW IS 'TMS 20160815-00072 / ESMS 438366';
--