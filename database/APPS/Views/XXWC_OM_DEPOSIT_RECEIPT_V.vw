CREATE OR REPLACE FORCE VIEW APPS.XXWC_OM_DEPOSIT_RECEIPT_V
(
   HEADER_ID
 , PAYMENT_NUMBER
 , PAYMENT_TYPE_CODE
 , PREPAID_AMOUNT
 , CHECK_NUMBER
 , RECEIPT_NUMBER
 , RECEIPT_DATE
 , RECEIPT_METHOD_NAME
 , AMOUNT
 , CASH_RECEIPT_ID
 , DOC_SEQUENCE_VALUE
 , MAX_REFUND_AMOUNT
 , CARD_ISSUER_CODE
 , CARD_NUMBER
 , TRXN_EXTENSION_ID
)
AS
   SELECT DISTINCT
          a.header_id
        , a.payment_number
        , a.payment_type_code
        , a.prepaid_amount
        , a.check_number
        , b.receipt_number
        , b.receipt_date
        , c.name
        , b.amount
        , b.cash_receipt_id
        , b.doc_sequence_value
        , (SELECT NVL (SUM (amount_applied), 0)
             FROM apps.ar_receivable_applications_v d
            WHERE d.cash_receipt_id = b.cash_receipt_id
                  AND d.applied_payment_schedule_id = -7)
             max_refund_amount
        , NULL card_issuer_code
        , NULL card_number
        , a.trxn_extension_id
     /*used in cash refunds from from OM*/
     FROM oe_payments a
        , ar_receipt_methods c
        , (SELECT DISTINCT b.receipt_number
                         , b.receipt_date
                         , b.amount
                         , b.cash_receipt_id
                         , c.payment_set_id
                         , b.receipt_method_id
                         , b.doc_sequence_value
                         , b.payment_trxn_extension_id
             FROM apps.ar_cash_receipts b, apps.ar_receivable_applications c
            WHERE c.cash_receipt_id = b.cash_receipt_id
                  AND c.payment_set_id IS NOT NULL) b
    WHERE     a.payment_level_code = 'ORDER'
          AND a.payment_collection_event = 'PREPAY'
          AND a.payment_set_id = b.payment_set_id
          AND DECODE (a.payment_type_code
                    , 'CHECK', a.check_number
                    , b.doc_sequence_value) = b.receipt_number
          AND a.receipt_method_id = b.receipt_method_id
          AND a.trxn_extension_id IS NULL
          AND a.receipt_method_id=c.receipt_method_id
   UNION
   SELECT DISTINCT
          a.header_id
        , a.payment_number
        , a.payment_type_code
        , a.prepaid_amount
        , a.check_number
        , b.receipt_number
        , b.receipt_date
        , c.name
        , b.amount
        , b.cash_receipt_id
        , b.doc_sequence_value
        , (SELECT NVL (SUM (amount_applied), 0)
             FROM apps.ar_receivable_applications_v d
            WHERE d.cash_receipt_id = b.cash_receipt_id
                  AND d.applied_payment_schedule_id = -7)
             max_refund_amount
        , x.card_issuer_code
        , LTRIM (x.card_number, 'X') card_number
        , a.trxn_extension_id
     /*used in cash refunds from from OM*/
     FROM oe_payments a
        , ar_receipt_methods c  
        , (SELECT DISTINCT b.receipt_number
                         , b.receipt_date
                         , b.amount
                         , b.cash_receipt_id
                         , c.payment_set_id
                         , b.receipt_method_id
                         , b.doc_sequence_value
                         , b.payment_trxn_extension_id
             FROM apps.ar_cash_receipts b, apps.ar_receivable_applications c
            WHERE c.cash_receipt_id = b.cash_receipt_id
                  AND c.payment_set_id IS NOT NULL) b
        , apps.IBY_TRXN_EXTENSIONS_V x
        , apps.IBY_TRXN_EXTENSIONS_V y
    WHERE     a.payment_level_code = 'ORDER'
          AND a.payment_collection_event = 'PREPAY'
          AND a.payment_set_id = b.payment_set_id
          AND DECODE (a.payment_type_code
                    , 'CHECK', a.check_number
                    , b.doc_sequence_value) = b.receipt_number
          AND a.receipt_method_id = b.receipt_method_id
          AND a.receipt_method_id=c.receipt_method_id
          AND a.trxn_extension_id IS NOT NULL
          AND a.trxn_extension_id = x.trxn_extension_id
          AND b.payment_trxn_extension_id = y.trxn_extension_id
          AND b.receipt_number = y.trxn_ref_number2
          AND x.instrument_id = y.instrument_id
          AND x.instr_assignment_id = y.instr_assignment_id
          AND y.origin_application_id = 222
          AND y.trxn_ref_number1 = 'RECEIPT';
