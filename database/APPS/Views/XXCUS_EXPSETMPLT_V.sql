
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUS_EXPSETMPLT_V" ("EMPLOYEE_NUMBER", "PERSON_ID", "TEMPLATE_ID", "TEMPLATE_NAME") AS 
  SELECT DISTINCT employee_number
               ,a.person_id
               ,to_number(d.tag)
               ,d.description
  FROM hr.per_all_people_f      a
      ,hr.per_all_assignments_f t
      ,gl.gl_code_combinations  g
      ,apps.fnd_lookup_values   d
      ,apps.fnd_user            e
 WHERE a.person_type_id = 6
   AND a.person_id = e.employee_id
   AND a.person_id = t.person_id
   AND a.effective_start_date <= t.effective_start_date
   AND a.effective_start_date <= t.effective_end_date
   AND a.effective_end_date >= t.effective_start_date
   AND t.default_code_comb_id = g.code_combination_id
   AND d.lookup_type = 'HDS_AP_EXPENSE_TEMPLATE'
   AND E.USER_ID = fnd_profile.value('USER_ID')
   AND (a.effective_end_date >=
       (trunc(SYSDATE) - (SELECT to_number(description)
                             FROM apps.fnd_lookup_values
                            WHERE lookup_type = 'HDS_HDS_IEXPENSE'
                              AND lookup_code = 'DAYS_BACK')) OR
       a.effective_end_date = to_date('31-DEC-4712', 'DD-MON-YYYY'))
   AND (CASE
         WHEN g.segment1 = d.lookup_code THEN
          1
         WHEN g.segment1 NOT IN
              (SELECT d.lookup_code
                 FROM apps.fnd_lookup_values d
                WHERE d.lookup_type = 'HDS_AP_EXPENSE_TEMPLATE') AND
              d.lookup_code = '0' THEN
          1
         WHEN e.user_id IN (SELECT to_number(description)
                              FROM apps.fnd_lookup_values
                             WHERE lookup_type = 'HDS_HDS_IEXPENSE'
                               AND lookup_code LIKE 'AUDIT_%') THEN
          1
       END) = 1

;
