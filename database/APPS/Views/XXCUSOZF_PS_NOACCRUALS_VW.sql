
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUSOZF_PS_NOACCRUALS_VW" ("MVID", "MASTER_VENDOR", "BU_SKU", "LOB", "LOB_SPEND", "START_DATE", "END_DATE") AS 
  SELECT hca.account_number mvid
      ,a.ship_from_party_name master_vendor
      ,msi.description bu_sku
      ,a.bill_to_party_name lob
      ,SUM((a.selling_price * a.quantity)) lob_spend
      ,period.start_date
      ,period.end_date
  FROM ozf.ozf_resale_lines_all a,
       hz_cust_accounts hca,
       mtl_system_items_b msi,
       ozf_time_ent_period period,
       (SELECT distinct TO_NUMBER(qq.qualifier_attr_value) cust_id, qlhv.list_header_id offer_id
         FROM  qp_list_headers_vl qlhv,
               qp_pricing_attributes qpa,
               qp_qualifiers qq
       WHERE  qlhv.list_header_id=qq.list_header_id
         AND  qpa.list_header_id=qlhv.LIST_HEADER_ID
         AND  qq.qualifier_context='SOLD_BY'
         AND qq.qualifier_attribute = 'QUALIFIER_ATTRIBUTE2'
         AND qpa.product_attribute='PRICING_ATTRIBUTE25'
         AND qlhv.active_flag='Y') offer
WHERE  1=1
   AND a.sold_from_cust_account_id = offer.cust_id
   AND a.sold_from_cust_account_id = hca.cust_account_id
   AND  msi.organization_id=84
   AND  msi.inventory_item_id=a.inventory_item_id
   AND a.date_ordered >= period.start_date ---enter start date for receipts
   AND a.date_ordered <= period.end_date ---enter end date for receipts
 --AND period.start_date >= '01-JAN-2011'
 --AND period.end_date <= '31-DEC-2011'
   AND NOT EXISTS (SELECT 1
                     FROM ozf_funds_utilized_all_b ofu,
                          apps.mtl_item_categories_v c
                      WHERE ofu.plan_id=offer.offer_id
                        AND ofu.object_id=a.resale_line_id
                        AND c.inventory_item_id=ofu.product_id
                        and c.CATEGORY_SET_ID=1100000041
                        AND c.organization_id=84
                        AND c.segment1 = hca.attribute2
                        AND ofu.cust_account_id=hca.cust_account_id)
GROUP BY a.ship_from_party_name
        ,a.bill_to_party_name
        ,msi.description
        ,hca.account_number
        ,period.start_date
        ,period.end_date
;
