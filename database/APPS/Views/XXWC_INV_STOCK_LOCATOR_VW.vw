CREATE OR REPLACE VIEW APPS.XXWC_INV_STOCK_LOCATOR_VW
AS
     /*************************************************************************
       $Header XXWC_INV_STOCK_LOCATOR_VW.vw $
       Module Name: XXWC_INV_STOCK_LOCATOR_VW
       PURPOSE: Stock Locator View for WEB ADI upload LOV for Locator field
       REVISIONS:
       Ver        Date         Author                Description
       ---------  -----------  ------------------    ----------------
       1.0        18-May-2015  Manjula Chellappan    Initial Version TMS # 20150430-00019
     **************************************************************************/
     SELECT MP.organization_code,
            MIL.segment1 stock_loc,
            (MP.organization_code || ':' || MIL.segment1) stock_loc_desc,
            mil.subinventory_code 
       FROM MTL_ITEM_LOCATIONS_KFV MIL, MTL_PARAMETERS MP
      WHERE     MP.organization_id = MIL.organization_id(+)            
   ORDER BY 1, 2;