
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUSOZF_ACCRUAL_REGION_V" ("MVID", "VENDOR_NAME", "LOB", "VENDOR", "OFFER_NAME", "BU_BRANCH_CODE", "ATTRIBUTE2", "STATE_TERITORY", "BRANCH_LOCATION", "RESALE_ORDER_DATE", "PURCHASES", "ACCRUALS") AS 
  select hzca.attribute2 MVID, 
       hzp.party_name Vendor_Name, 
       orl.bill_to_party_name LOB,  
       orl.sold_from_party_name vendor,
       ofu.offer_name Offer_Name,
       orl.line_attribute6 BU_BRANCH_Code,
       hca.attribute2 Attribute2,
       substr(hca.attribute2, instr(hca.attribute2, '-')+1,2) State_Teritory,
       hca.account_number branch_location,
       orl.date_ordered Resale_Order_Date,
       SUM(nvl(selling_price*quantity,0)) Purchases,
       SUM(nvl(ofu.amount,0)) Accruals
       
  from ozf_resale_lines_all orl,
        hz_cust_accounts hzca,
       hz_cust_accounts hca,  
       hz_parties hzp,
       (SELECT object_id
                , offer_name
                ,OFFER_CODE
                ,SUM(NVL(util_acctd_amount,0) ) amount
        FROM xxcus.xxcus_ozf_xla_accruals_b
            where 1=1
              and offer_id <> 69797   
              GROUP BY object_id
                        ,offer_name
                        ,OFFER_CODE
                         ) ofu
WHERE 1=1
and hca.cust_account_id = orl.bill_to_cust_account_id 
and ofu.object_id(+)= orl.resale_line_id
  AND   hzca.cust_account_id = orl.sold_from_cust_account_id 
  AND   hzp.party_id =hzca.party_id 
-- and hzca.cust_account_id=1953
--  AND orl.date_ordered between '01-JAN-2013' and '31-DEC-2013'


  GROUP BY  ofu.offer_name,
            hzca.attribute2,
            hca.account_number,
            hzp.party_name,
            orl.line_attribute6,
            orl.bill_to_party_name,
            orl.sold_from_party_name,
            hca.attribute2,
            orl.date_ordered;
