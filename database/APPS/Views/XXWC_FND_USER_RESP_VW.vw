--
-- XXWC_FND_USER_RESP_VW  (View)
--

CREATE OR REPLACE FORCE VIEW APPS.XXWC_FND_USER_RESP_VW
(
   USER_NAME
  ,DESCRIPTION
  ,RESPONSIBILITY_NAME
  ,EMAIL_ADDRESS
  ,RESPONSIBILITY_KEY
  ,SOURCE
  ,START_DATE
  ,END_DATE
  ,CREATED_BY
)
AS
   SELECT DISTINCT fu.user_name
                  ,fu.description
                  ,frv.responsibility_name
                  ,fu.email_address
                  ,frv.RESPONSIBILITY_KEY
                  ,furga.source
                  ,furga.start_date
                  ,furga.end_date
                  ,fu2.description Created_By
     FROM apps.fnd_user fu
         ,(SELECT user_id
                 ,responsibility_id
                 ,start_date
                 ,end_date
                 ,created_by
                 ,'direct' source
             FROM apps.fnd_user_resp_groups_direct
           UNION
           SELECT user_id
                 ,responsibility_id
                 ,start_date
                 ,end_date
                 ,created_by
                 ,'indirect' source
             FROM apps.fnd_user_resp_groups_indirect) furga
         ,apps.fnd_responsibility_vl frv
         ,apps.fnd_user fu2
    WHERE     fu.user_id = furga.user_id
          AND fu2.user_id = furga.created_by
          AND furga.responsibility_id = frv.responsibility_id
          AND furga.start_date <= SYSDATE
          AND NVL (furga.end_date, SYSDATE + 1) > SYSDATE
          AND fu.start_date <= SYSDATE
          AND NVL (fu.end_date, SYSDATE + 1) > SYSDATE
          AND frv.start_date <= SYSDATE
          AND NVL (frv.end_date, SYSDATE + 1) > SYSDATE
          AND (   fu.employee_id IN
                     (SELECT per.person_id
                        FROM PER_PEOPLE_f per
                             INNER JOIN per_all_assignments_f ass
                                ON ass.person_id =
                                      per.person_id
                             INNER JOIN gl_code_combinations gc
                                ON gc.code_combination_id =
                                      ass.default_code_comb_id
                       WHERE     gc.segment1 = '0W'
                             AND NVL (per.effective_end_date, SYSDATE + 1) >
                                    SYSDATE)
               OR frv.RESPONSIBILITY_KEY LIKE 'XX%')
--AND frv.RESPONSIBILITY_KEY <> 'HDS_INTERNET_EXPENSES_USER'
;

COMMENT ON TABLE APPS.XXWC_FND_USER_RESP_VW IS 'This view returns all the responsibilities granted to each user. It includes
all White Cap associates (GL segment 0W), plus a record for each XXWC responsibility
assigned to other associates even if they don''t have the 0W default account.';



