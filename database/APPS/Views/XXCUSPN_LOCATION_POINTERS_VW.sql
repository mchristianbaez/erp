
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUSPN_LOCATION_POINTERS_VW" ("LOCATION_CODE", "BLD_LOC", "FLR_LOC", "SEC_LOC", "ADDRESS_ID") AS 
  (SELECT bld.location_code, bld.location_id bld_loc, flr.location_id flr_loc, sec.location_id sec_loc, ADDr.Address_ID
FROM  pn.pn_locations_all bld,
      pn.pn_locations_all flr,
      pn.pn_locations_all sec,
      pn.pn_addresses_all addr
WHERE sec.parent_location_id = flr.location_id
AND   flr.parent_location_id = bld.location_id
AND   bld.address_id = addr.Address_Id
AND   bld.active_end_date = (SELECT MAX(active_end_date) FROM pn.pn_locations_all WHERE location_id =bld.location_id)
AND   flr.active_end_date = (SELECT MAX(active_end_date) FROM pn.pn_locations_all WHERE location_id =flr.location_id)
AND   sec.active_end_date = (SELECT MAX(active_end_date) FROM pn.pn_locations_all WHERE location_id =sec.location_id))
;
