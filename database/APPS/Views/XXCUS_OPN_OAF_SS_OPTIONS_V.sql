  /*
  ===========================================================================
    Module: Oracle Property Manager
    Type: HDS GSC custom view
    PURPOSE: Used by HDS OPN Property Manager -Oracle Application Form SuperSearch UI TAB  "Options"
    HISTORY
  ==============================================================================================================================
      VERSION DATE                  AUTHOR(S)                 DESCRIPTION                       TICKET
      -------        -----------           ---------------                ------------------------------    ---------------                          
      1.0            30-Dec-2015   Balaguru Seshadri  Created.                                 TMS 20160209-00169           
  */  
CREATE OR REPLACE FORCE VIEW APPS.XXCUS_OPN_OAF_SS_OPTIONS_V AS
SELECT A.OPTION_TYPE TYPE
             ,A.OPTION_STATUS_TYPE STATUS
             ,TRUNC(A.OPTION_EXER_START_DATE) NOTICE_START
             ,TRUNC(A.OPTION_EXER_END_DATE) NOTICE_END
             ,REGEXP_REPLACE(A.OPTION_REFERENCE, '[[:cntrl:]]', '') OPTION_REFERENCE
             ,REGEXP_REPLACE(A.OPTION_COMMENTS, '[[:cntrl:]]', '') OPTION_COMMENTS                         
             ,TRUNC(A.START_DATE) PERIOD_START
             ,TRUNC(A.EXPIRATION_DATE) PERIOD_END
             ,A.LEASE_ID
             ,A.LEASE_CHANGE_ID
FROM APPS.PN_OPTIONS_V A
WHERE 1 =1;
--
COMMENT ON TABLE APPS.XXCUS_OPN_OAF_SS_OPTIONS_V IS 'TMS 20160209-00169 OR ESMS: 195667';
--