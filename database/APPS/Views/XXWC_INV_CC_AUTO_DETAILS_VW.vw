--
-- XXWC_INV_CC_AUTO_DETAILS_VW  (View)
--

CREATE OR REPLACE FORCE VIEW APPS.XXWC_INV_CC_AUTO_DETAILS_VW
(
   CYCLECOUNT_HDR_ID
  ,CYCLECOUNT_LINE_ID
  ,CYCLECOUNT_LINE_DTL_ID
  ,CYCLE_COUNT_NAME
  ,PROCESS_TYPE
  ,PROCESS_NOW_FLAG
  ,INV_ORG_ID
  ,INV_LOCATION_ID
  ,BIN_LOCATION
  ,ORGANIZATION_NAME
  ,ORGANIZATION_CODE
  ,PROCESS_DATE
  ,PROCESS_STATUS
  ,OBJECT_VERSION_NUMBER
  ,ITEM_NUMBER
  ,DESCRIPTION
)
AS
   SELECT hdr.cyclecount_hdr_id
         ,lines.cyclecount_line_id
         ,dtls.cyclecount_line_dtl_id
         ,cycle_count_name
         ,process_type
         ,process_now_flag
         ,dtls.inv_org_id
         ,inv_location_id
         ,dtls.bin bin_location
         ,org.organization_name
         ,org.organization_code
         ,dtls.process_date
         ,dtls.process_status
         ,dtls.object_version_number
         ,msi.segment1 item_number
         ,msi.description
     FROM xxwc_inv_cyclecount_line_dtls dtls
         ,xxwc_inv_cyclecount_lines lines
         ,xxwc_inv_cyclecount_hdr hdr
         ,org_organization_definitions org
         ,mtl_system_items msi
    WHERE     dtls.cyclecount_line_id = lines.cyclecount_line_id
          AND lines.cyclecount_hdr_id = hdr.cyclecount_hdr_id
          AND dtls.inv_org_id = org.organization_id
          AND dtls.inv_item_id = msi.inventory_item_id
          AND dtls.inv_org_id = msi.organization_id;


