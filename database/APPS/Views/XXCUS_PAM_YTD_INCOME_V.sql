CREATE OR REPLACE FORCE VIEW "APPS"."XXCUS_PAM_YTD_INCOME_V"
(
   "CUST_ID",
   "MVID",
   "LOB_ID",
   "LOB_NAME",
   "MV_NAME",
   "REBATE_TYPE",
   "CALENDAR_YEAR",
   "ACCRUED_AMT"
)
AS
     SELECT OFU_CUST_ACCOUNT_ID       CUST_ID,
            MVID,
            LOB_ID,
            LOB LOB_NAME,
            C.PARTY_NAME              MV_NAME,
            REBATE_TYPE,
            TO_CHAR (AGREEMENT_YEAR)  CALENDAR_YEAR,
            SUM (ACCRUAL_AMOUNT)      ACCRUED_AMT
       FROM XXCUS.XXCUS_YTD_INCOME_B
           ,XXCUS.XXCUS_REBATE_CUSTOMERS C
      WHERE     1 = 1
            AND C.CUSTOMER_ID         =OFU_CUST_ACCOUNT_ID
            AND C.CUSTOMER_ATTRIBUTE1 ='HDS_MVID'
            AND CALENDAR_YEAR         >2012
   GROUP BY OFU_CUST_ACCOUNT_ID,
            MVID,
            LOB_ID,
            LOB,
            C.PARTY_NAME,
            REBATE_TYPE,
            TO_CHAR (AGREEMENT_YEAR);

--
COMMENT ON TABLE APPS.XXCUS_PAM_YTD_INCOME_V IS 'RFC 43034 / ESMS 280464';
--