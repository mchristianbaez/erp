--
-- XXWC_OM_AUTH_BUYERS_LOV_VW  (View)
--
-- Description : Used by "Bill To Contact" LOV on Sales Order.
--
CREATE OR REPLACE VIEW APPS.XXWC_OM_AUTH_BUYERS_LOV_VW
AS
SELECT name, customer_id,
       org_id ,
       contact_id,
       job_title,
       email_address,
       address1,
       address2,
       address3,
       address4,
       address5,
       role,
       additional_column1,
       additional_column2,
       location
FROM (
select distinct con.name name,con.org_id , con.customer_id,
       con.contact_id,
       con.job_title,
       con.email_address,
       loc.address1,
       loc.address2,
       loc.address3,
       loc.address4,
       decode(loc.city,null,null,loc.city||' ,')||
       decode(loc.state,null,null,loc.state||' ,')||
       decode(loc.postal_code,null,null,loc.postal_code||' ,')||
       decode(loc.country,null,null,loc.country) address5,
       role.responsibility_type role,
                null additional_column1,
                null additional_column2,
       site_use.location
from oe_contacts_v con,
     hz_role_responsibility role,
     hz_cust_acct_sites acct_site,
     hz_locations loc,
     hz_party_sites party_site,
     hz_cust_site_uses_all site_use
  where 1= 1 
    and con.status='A'
    and role.cust_account_role_id(+) = con.contact_id
    and nvl(role.responsibility_type,'BILL_TO' ) = 'BILL_TO' 
    and acct_site.cust_acct_site_id(+) = con.org_id
    and acct_site.party_site_id = party_site.party_site_id(+)
    and party_site.location_id = loc.location_id(+)
    and site_use.cust_acct_site_id(+) =  acct_site.cust_acct_site_id 
    and (con.job_title IS NULL AND con.org_id IS NULL)
    and nvl(site_use.site_use_code,'BILL_TO')='BILL_TO'
    AND NOT EXISTS (select '1' 
                                 from oe_contacts_v con2,
                                         hz_role_responsibility role2 
                              where con2.customer_id = con.customer_id 
                                  and con2.status='A'
                                   and role2.cust_account_role_id = con2.contact_id
                                   and role2.responsibility_type = 'AUTH_BUYER')   
UNION
select distinct con.name name,con.org_id , con.customer_id,
       con.contact_id,
       con.job_title,
       con.email_address,
       loc.address1,
       loc.address2,
       loc.address3,
       loc.address4,
       decode(loc.city,null,null,loc.city||' ,')||
       decode(loc.state,null,null,loc.state||' ,')||
       decode(loc.postal_code,null,null,loc.postal_code||' ,')||
       decode(loc.country,null,null,loc.country) address5,
       role.responsibility_type role,
                null additional_column1,
                null additional_column2,
       site_use.location
from oe_contacts_v con,
     hz_role_responsibility role,
     hz_cust_acct_sites acct_site,
     hz_locations loc,
     hz_party_sites party_site,
     hz_cust_site_uses_all site_use
  where 1 = 1 
    and con.status='A'
    and role.cust_account_role_id(+) = con.contact_id    
    and acct_site.cust_acct_site_id(+) = con.org_id
    and acct_site.party_site_id = party_site.party_site_id(+)
    and party_site.location_id = loc.location_id(+)
    and site_use.cust_acct_site_id(+) =  acct_site.cust_acct_site_id 
    AND role.responsibility_type = 'AUTH_BUYER'
)