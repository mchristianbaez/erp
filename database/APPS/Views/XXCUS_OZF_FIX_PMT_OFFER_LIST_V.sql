   /*************************************************************************  
     $Header XXCUS_OZF_FIX_PMT_OFFER_MVID_V.sql $
     Module Name: XXCUS_OZF_FIX_PMT_OFFER_MVID_V
     --
     PURPOSE:   This package is called by the concurrent program HDS Rebates: Auto Clear Rebate Payments
     --
     REVISIONS:
     Ver        Date           Author             Ticket                       Description
     ---------  ----------     --------------     -------------------------    -------------------------------------------------
     1.0        04/26/2016     Balaguru Seshadri  TMS:  20160427-00036         Created
                                                  ESMS: 256550
                                                                                      
   **************************************************************************/
CREATE OR REPLACE FORCE VIEW APPS.XXCUS_OZF_FIX_PMT_OFFER_LIST_V
(
   OFFER_DESC,
   OFFER_CODE,
   CUST_ACCOUNT_ID
)
AS
     SELECT DISTINCT
            QLHV.DESCRIPTION||' - '||OO.OFFER_CODE||' - '||Initcap(OO.STATUS_CODE)||' - '||QLHV.ATTRIBUTE7  OFFER,
            OO.OFFER_CODE OFFER_CODE,
            HCA.CUST_ACCOUNT_ID CUST_ACCOUNT_ID
       FROM QP_LIST_HEADERS_VL QLHV,
            OZF_OFFERS OO,
            QP_QUALIFIERS QQ,
            HZ_CUST_ACCOUNTS HCA
      WHERE     1 = 1
            AND OO.QP_LIST_HEADER_ID = QLHV.LIST_HEADER_ID
            AND OO.STATUS_CODE <> 'COMPLETED'       --OO.STATUS_CODE ='ACTIVE'
            AND QQ.LIST_HEADER_ID(+) = QLHV.LIST_HEADER_ID
            AND QQ.QUALIFIER_CONTEXT(+) = 'SOLD_BY'
            AND QQ.QUALIFIER_ATTRIBUTE(+) = 'QUALIFIER_ATTRIBUTE2'
            AND HCA.CUST_ACCOUNT_ID(+) = TO_NUMBER (QQ.QUALIFIER_ATTR_VALUE);
COMMENT ON TABLE APPS.XXCUS_OZF_FIX_PMT_OFFER_LIST_V IS 'TMS 20160427-00036';
