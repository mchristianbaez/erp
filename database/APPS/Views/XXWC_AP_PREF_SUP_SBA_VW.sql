CREATE OR REPLACE VIEW APPS.XXWC_AP_PREF_SUP_SBA_VW
/*************************************************************************
  $Header XXWC_AP_PREF_SUP_SBA_VW.sql $
  Module Name: XXWC_AP_PREF_SUP_SBA_VW

  PURPOSE: View for Preferred Supplier Category SBA Owner Values

  TMS Task Id :  20141104-00115

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    ----------------
  1.0        29-Oct-2014  Manjula Chellappan    Initial Version
**************************************************************************/
AS
   SELECT b.flex_value_id supplier_sba_id,
          b.flex_value supplier_sba,
          b.description supplier_sba_desc
     FROM fnd_flex_value_sets a, fnd_flex_values_vl b
    WHERE     a.flex_value_set_name = 'XXWC_SBA_OWNERS'
          AND a.flex_value_set_id = b.flex_value_set_id
          AND TRUNC (SYSDATE) BETWEEN TRUNC (
                                         NVL (b.start_date_active,
                                              SYSDATE - 1))
                                  AND TRUNC (
                                         NVL (b.end_date_active, SYSDATE + 1))
          AND b.enabled_flag = 'Y'
   UNION
   SELECT -1, 'All', 'All' FROM DUAL