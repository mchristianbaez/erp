CREATE OR REPLACE FORCE VIEW APPS.XXWC_INV_PRODUCT_MASTER_VW
(
/******************************************************************************
   NAME:       APPS.XXWC_INV_PRODUCT_MASTER_VW
   PURPOSE:  This view is used to extract all Inventory Item Details

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        31/08/2012  Consuelo        Initial Version
   1.0        03/09/2015   Raghavendra S   TMS# 20150303-00075 - FIN - Addition of Code Snippet for Line Breaks in All EDW Extract Programs
                                            and Views for all free entry text fields.
   1.1        07/18/2016   Neha Saini       TMS#20160629-00017 -- replaced  package name with EDW package for performance fix.
******************************************************************************/
   BUSINESS_UNIT,
   OPERATING_UNIT_ID,
   INTERFACE_DATE,
   OPERATING_UNIT_NAME,
   SOURCE_SYSTEM,
   SKU_NUMBER,
   SKU_LONG_DESC,
   SKU_SHORT_DESC,
   SKU_OTHER_DESC,
   SKU_START_DATE,
   MANUFACTURER_NAME,
   VENDOR_PART_NUMBER,
   PRIMARY_UPC,
   SECONDARYUPC,
   TERTIARYUPC,
   PROD_HIE_LVL_1_VAL,
   PROD_HIE_LVL_2_VAL,
   PROD_HIE_LVL_3_VAL,
   PROD_HIE_LVL_4_VAL,
   UNSPSC_CODE,
   PO_REPLACEMENTCOST,
   EFF_DATE_PO_REPL_COST,
   STANDARD_COST,
   STANDARD_COST_EFF_DATE,
   TRADE_PRICE,
   EFF_DATE_TRADE_PRICE,
   PURCH_UOM,
   PURCH_UOM_QTY,
   CONV_FACT_PURCH_REC_UOM,
   PRICING_UOM,
   PRICING_UOM_QTY,
   STOCKING_UOM,
   STOCKING_UOM_QTY,
   UNITS_PER_PKG,
   UNIT_PER_PKG_UOM,
   CASE_QTY,
   PALLET_QTY,
   UNIT_LENGTH,
   UNIT_HEIGHT,
   UNIT_WIDTH,
   DIM_MEASUREMENT_TYPE,
   SINGLE_WEIGHT_BUY_PKG,
   WEIGHT_MEASUREMENT_TYPE,
   CUBE_BUY_PKG,
   CUBE_MEASUREMENT_TYPE,
   IMPORT_FLAG,
   COUNTRY_OF_ORIGIN,
   COMMODITY_FLAG,
   PRIMARY_COMODITY,
   SECONDARY_COMMODITY,
   ITEM_FINISH,
   IMPORT_DUTY,
   MIN_ORDER_QTY,
   MIN_ORDER_QTY_UOM,
   INNER_PACK_SKU,
   GL_CODE,
   GL_CODE_DESC,
   NMFC_CODE,
   PALLET_TYPE,
   CUST_PROD_ATTR_LABEL1,
   CUST_PROD_ATTR_VALUE1,
   CUST_PROD_ATTR_LABEL2,
   CUST_PROD_ATTR_VALUE2,
   CUST_PROD_ATTR_LABEL3,
   CUST_PROD_ATTR_VALUE3,
   CUST_PROD_ATTR_LABEL4,
   CUST_PROD_ATTR_VALUE4,
   CUST_PROD_ATTR_LABEL5,
   CUST_PROD_ATTR_VALUE5,
   CUST_PROD_ATTR_LABEL6,
   CUST_PROD_ATTR_VALUE6,
   CUST_PROD_ATTR_LABEL7,
   CUST_PROD_ATTR_VALUE7,
   CUST_PROD_ATTR_LABEL8,
   CUST_PROD_ATTR_VALUE8,
   CUST_PROD_ATTR_LABEL9,
   CUST_PROD_ATTR_VALUE9,
   CUST_PROD_ATTR_LABEL10,
   CUST_PROD_ATTR_VALUE10,
   PRICE_MATRIX,
   HAZARDOUS_MATERIAL,
   HAZARD_CLASS
)
AS
   SELECT 'WHITECAP' business_unit,
          hou.organization_id operating_unit_id,
          SYSDATE interface_date,
          hou.name operating_unit_name,
          'Oracle EBS' source_system,
          REGEXP_REPLACE(msib.segment1,'[[:cntrl:]]', ' ') sku_number -- 02/18/2013 CG: TMS 20130214-01693: Adding to remove line feed special chars
 --  , replace(msit.long_description, chr(10), ' ') sku_long_desc  Commented by Raghavendra for TMS # 20141203-00170
          ,
          REGEXP_REPLACE (REPLACE (msit.long_description, '|', '/'),
                          '[[:cntrl:]]')
             sku_long_desc -- Added  by Raghavendra for removing the line breaks TMS # 20141203-00170
                          --  , replace(msib.description, chr(10), ' ') sku_short_desc  Commented by Raghavendra for TMS # 20141203-00170
          ,
          REGEXP_REPLACE (REPLACE (msib.description, '|', '/'),
                          '[[:cntrl:]]')
             sku_short_desc -- Added  by Raghavendra for removing the line breaks TMS # 20141203-00170
                           ,
          NULL sku_other_desc,
          msib.creation_date sku_start_date,
          NULL manufacturer_name,
          /*SUBSTR (
             xxwc_mv_routines_pkg.get_item_cross_reference (
                msib.inventory_item_id,
                'VENDOR',
                1),
             1,
             255) vendor_part_number,*/ -- commented for V 2.0
          REGEXP_REPLACE(SUBSTR (
             xxwc_mv_routines_pkg.get_item_cross_reference (
                msib.inventory_item_id,
                'VENDOR',
                1),
             1,
             255),'[[:cntrl:]]', ' ') 
             vendor_part_number, -- Added for V 2.0
          /*SUBSTR (
             xxwc_mv_routines_pkg.get_item_cross_reference (
                msib.inventory_item_id,
                'UPC',
                1),
             1,
             255) primary_upc,*/ -- commented for V 2.0
          REGEXP_REPLACE(SUBSTR (
             xxwc_mv_routines_pkg.get_item_cross_reference (
                msib.inventory_item_id,
                'UPC',
                1),
             1,
             255),'[[:cntrl:]]', ' ') 
             primary_upc, -- Added for V 2.0
          /*SUBSTR (
             xxwc_mv_routines_pkg.get_item_cross_reference (
                msib.inventory_item_id,
                'UPC',
                2),
             1,
             255) secondaryupc,*/-- commented for V 2.0
          REGEXP_REPLACE(SUBSTR (
             xxwc_mv_routines_pkg.get_item_cross_reference (
                msib.inventory_item_id,
                'UPC',
                2),
             1,
             255),'[[:cntrl:]]', ' ') 
             secondaryupc,-- Added for V1.0
          /*SUBSTR (
             xxwc_mv_routines_pkg.get_item_cross_reference (
                msib.inventory_item_id,
                'UPC',
                3),
             1,
             255)tertiaryupc,*/ -- commented for V 2.0
          REGEXP_REPLACE(SUBSTR (
             xxwc_mv_routines_pkg.get_item_cross_reference (
                msib.inventory_item_id,
                'UPC',
                3),
             1,
             255),'[[:cntrl:]]', ' ') 
             tertiaryupc, -- Added for V 2.0
             -- 06/06/2012 CG: Changed to use the function and prevent multiple records
          -- mcb_inv.segment1 prod_hie_lvl_1_val,
          -- mcb_inv.segment2 prod_hie_lvl_2_val,
          -- mcb_inv.segment3 prod_hie_lvl_3_val,
          -- mcb_inv.segment4 prod_hie_lvl_4_val,
          /*SUBSTR (xxwc_mv_routines_pkg.get_item_category (
                     msib.inventory_item_id,
                     msib.organization_id,
                     'Inventory Category',
                     1),
                  1,
                  40) prod_hie_lvl_1_val,*/ -- Commentd for V 2.0
          REGEXP_REPLACE(SUBSTR (xxwc_mv_routines_pkg.get_item_category (
                     msib.inventory_item_id,
                     msib.organization_id,
                     'Inventory Category',
                     1),
                  1,
                  40),'[[:cntrl:]]', ' ') 
             prod_hie_lvl_1_val, -- Added for V 2.0
          /*SUBSTR (xxwc_mv_routines_pkg.get_item_category (
                     msib.inventory_item_id,
                     msib.organization_id,
                     'Inventory Category',
                     2),
                  1,
                  40),'[[:cntrl:]]', ' ') 
             prod_hie_lvl_2_val,*/ -- Commented for V 2.0
          REGEXP_REPLACE(SUBSTR (xxwc_mv_routines_pkg.get_item_category (
                     msib.inventory_item_id,
                     msib.organization_id,
                     'Inventory Category',
                     2),
                  1,
                  40),'[[:cntrl:]]', ' ') 
             prod_hie_lvl_2_val, -- Added for V 2.0
          /*SUBSTR (xxwc_mv_routines_pkg.get_item_category (
                     msib.inventory_item_id,
                     msib.organization_id,
                     'Inventory Category',
                     3),
                  1,
                  40) prod_hie_lvl_3_val,*/ -- Commented for V 2.0
          REGEXP_REPLACE(SUBSTR (xxwc_mv_routines_pkg.get_item_category (
                     msib.inventory_item_id,
                     msib.organization_id,
                     'Inventory Category',
                     3),
                  1,
                  40),'[[:cntrl:]]', ' ') 
             prod_hie_lvl_3_val, -- Added for V 2.0
          /*SUBSTR (xxwc_mv_routines_pkg.get_item_category (
                     msib.inventory_item_id,
                     msib.organization_id,
                     'Inventory Category',
                     4),
                  1,
                  40) prod_hie_lvl_4_val,*/ -- Commented for V 2.0
          REGEXP_REPLACE(SUBSTR (xxwc_mv_routines_pkg.get_item_category (
                     msib.inventory_item_id,
                     msib.organization_id,
                     'Inventory Category',
                     4),
                  1,
                  40),'[[:cntrl:]]', ' ') 
             prod_hie_lvl_4_val, -- Added for V 2.0
          NULL unspsc_code,
          msib.list_price_per_unit po_replacementcost,
          NULL eff_date_po_repl_cost,
          NULL standard_cost,
          NULL standard_cost_eff_date, -- 06/06/2012 CG: Changed to pull the price list value
          --      default to contingency 20K if not in a PRL
          -- msib.list_price_per_unit trade_price,
          --    xxwc_mv_routines_pkg.get_item_qp_list_price (
          --       msib.inventory_item_id
          --    )
          --       trade_price      --commented and added below by Maha on 8/12/14 for TMS#20140729-00024
          --ver1.1 changes starts TMS#20160629-00017 
--          APPS.xxwc_qp_market_price_util_pkg.get_market_list_price (
--             msib.inventory_item_id,
--             msib.organization_id)
--             trade_price,
          APPS.XXWC_EBS_EDW_INTF_PKG.get_market_list_price (
             msib.inventory_item_id,
             msib.organization_id)
             trade_price, --ver1.1 changes ends TMS#20160629-00017 
          NULL eff_date_trade_price,
          msib.primary_uom_code purch_uom,
          1 purch_uom_qty,
          1 conv_fact_purch_rec_uom,
          msib.primary_uom_code pricing_uom,
          1 pricing_uom_qty,
          msib.primary_uom_code stocking_uom,
          1 stocking_uom_qty,
          NULL units_per_pkg,      -- msib.fixed_lot_multiplier units_per_pkg,
          msib.primary_uom_code unit_per_pkg_uom,
          NULL case_qty,
          NULL pallet_qty,
          msib.unit_length,
          msib.unit_height,
          msib.unit_width,
          msib.volume_uom_code dim_measurement_type,
          msib.unit_weight single_weight_buy_pkg,
          msib.weight_uom_code weight_measurement_type,
          NULL cube_buy_pkg,
          NULL cube_measurement_type,
          /*(CASE
              WHEN (    msib.attribute10 IS NOT NULL
                    AND msib.attribute10 NOT IN ('US', 'USA'))
              THEN
                 'Import'
              ELSE
                 NULL
           END)  import_flag*/ -- Commented for V 2.0
          REGEXP_REPLACE((CASE
              WHEN (    msib.attribute10 IS NOT NULL
                    AND msib.attribute10 NOT IN ('US', 'USA'))
              THEN
                 'Import'
              ELSE
                 NULL
           END),'[[:cntrl:]]', ' ') 
             import_flag, -- Added for V 2.0
          -- msib.attribute10 country_of_origin, -- commneted for V 2.0
          REGEXP_REPLACE(msib.attribute10,'[[:cntrl:]]', ' ') country_of_origin, -- Added for V 2.0
          NULL commodity_flag,
          NULL primary_comodity,
          NULL secondary_commodity,
          NULL item_finish,
          -- msib.attribute27  import_duty, -- commented for V 2.0
          REGEXP_REPLACE(msib.attribute27,'[[:cntrl:]]', ' ')  import_duty, -- Added for V 2.0
          msib.minimum_order_quantity min_order_qty,
          msib.primary_uom_code min_order_qty_uom,
          NULL inner_pack_sku,
          NULL gl_code,
          NULL gl_code_desc,
          NULL nmfc_code,
          NULL pallet_type,
          'Brigade' cust_prod_attr_label1,
           /*(CASE
              WHEN xxwc_mv_routines_pkg.get_item_category (
                      msib.inventory_item_id,
                      msib.organization_id,
                      'PRIVATE LABEL',
                      1) = 'Brigade'
              THEN
                 'Y'
              ELSE
                 'N'
           END)
             cust_prod_attr_value1,*/ -- commented for V 2.0
          REGEXP_REPLACE((CASE
              WHEN xxwc_mv_routines_pkg.get_item_category (
                      msib.inventory_item_id,
                      msib.organization_id,
                      'PRIVATE LABEL',
                      1) = 'Brigade'
              THEN
                 'Y'
              ELSE
                 'N'
           END),'[[:cntrl:]]', ' ')
             cust_prod_attr_value1, -- Added for V 2.0
          NULL cust_prod_attr_label2,
          NULL cust_prod_attr_value2,
          'Shelf Life Days' cust_prod_attr_label3,
          msib.shelf_life_days cust_prod_attr_value3,
          'Lot Control Code' cust_prod_attr_label4,
          flv_lot.meaning cust_prod_attr_value4, -- 07/18/2012 CG: Modified per email on 07/05/2012
          'WEB FLG' cust_prod_attr_label5,
          /*NVL (SUBSTR (xxwc_mv_routines_pkg.get_item_category (
                          msib.inventory_item_id,
                          msib.organization_id,
                          'Website Item',
                          1),
                       1,
                       40),
               'N')
             cust_prod_attr_value5*/  -- commented for V 2.0
          REGEXP_REPLACE(NVL (SUBSTR (xxwc_mv_routines_pkg.get_item_category (
                          msib.inventory_item_id,
                          msib.organization_id,
                          'Website Item',
                          1),
                       1,
                       40),
               'N'),'[[:cntrl:]]', ' ')
             cust_prod_attr_value5 -- Added for V 2.0
             -- 07/18/2012 CG: Modified per email on 07/05/2012
                  -- 09/26/2012 CG: Modifier per email on 09/27/2012 from Rich
            -- 11/20/2012 CG: Nulled out these attributes since they should be
                                          --            in the branch loc file
 /*, 'Default Vendor Identifier' cust_prod_attr_label6
    , SUBSTR (
         xxwc_mv_routines_pkg.get_default_supplier (
        msib.inventory_item_id
        , msib.organization_id
       , 'IDENTIFIER'
      )
    , 1
      , 45
    )
        cust_prod_attr_value6
    , 'Default Vendor Name' cust_prod_attr_label7
    , SUBSTR (
       xxwc_mv_routines_pkg.get_default_supplier (
        msib.inventory_item_id
        , msib.organization_id
       , 'NAME'
      )
    , 1
      , 240
    )
       cust_prod_attr_value7*/
          ,
          NULL cust_prod_attr_label6,
          NULL cust_prod_attr_value6,
          NULL cust_prod_attr_label7,
          NULL cust_prod_attr_value7 -- 09/26/2012 CG: Modifier per email on 09/27/2012 from Rich
                                    ,
          NULL cust_prod_attr_label8,
          NULL cust_prod_attr_value8,
          NULL cust_prod_attr_label9,
          NULL cust_prod_attr_value9,
          NULL cust_prod_attr_label10,
          NULL cust_prod_attr_value10,
          NULL price_matrix,
          msib.hazardous_material_flag hazardous_material,
          phct.hazard_class
     FROM mtl_system_items_b msib,
          mtl_system_items_tl msit,
          mtl_parameters mp,
          org_organization_definitions ood,
          hr_operating_units hou, -- 06/06/2012 CG: Changed to use the function and prevent multiple records
          -- Inventory Category
          -- mtl_item_categories mic_inv,
          -- mtl_category_sets_v mcst_inv,
          -- mtl_categories_b mcb_inv,
          -- 06/06/2012 CG: Changed to use the function and prevent multiple records
          -- Private Label Category
          -- mtl_item_categories mic_pl,
          -- mtl_category_sets_v mcst_pl,
          -- mtl_categories_b mcb_pl,
          po_hazard_classes_tl phct,
          fnd_lookup_values flv_lot
    WHERE     msib.inventory_item_id = msit.inventory_item_id
          AND msib.organization_id = msit.organization_id
          AND msit.language = 'US'
          AND msib.organization_id = mp.organization_id
          AND mp.organization_code = 'MST'
          AND mp.organization_id = ood.organization_id
          AND ood.operating_unit = hou.organization_id
          -- 06/06/2012 CG: Changed to use the function and prevent multiple records
          -- Inventory Category
          -- AND msib.inventory_item_id = mic_inv.inventory_item_id(+)
          -- AND msib.organization_id = mic_inv.organization_id(+)
          -- AND mic_inv.category_id = mcb_inv.category_id(+)
          -- AND mic_inv.category_set_id = mcst_inv.category_set_id(+)
          -- AND mcst_inv.category_set_name(+) = 'Inventory Category'
          -- 06/06/2012 CG: Changed to use the function and prevent multiple records
          -- Private Label Category
          -- AND msib.inventory_item_id = mic_pl.inventory_item_id(+)
          -- AND msib.organization_id = mic_pl.organization_id(+)
          -- AND mic_pl.category_id = mcb_pl.category_id(+)
          -- AND mic_pl.category_set_id = mcst_pl.category_set_id(+)
          -- AND mcst_pl.category_set_name(+) = 'PRIVATE LABEL'
          AND msib.hazard_class_id = phct.hazard_class_id(+)
          AND phct.language(+) = 'US'
          AND msib.lot_control_code = flv_lot.lookup_code
          AND flv_lot.lookup_type = 'MTL_LOT_CONTROL'
          AND flv_lot.view_application_id = 700;
