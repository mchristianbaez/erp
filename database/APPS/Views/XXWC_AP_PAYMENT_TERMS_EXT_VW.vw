/* Formatted on 2012/08/31 09:41 (Formatter Plus v4.8.8) */
CREATE OR REPLACE FORCE VIEW apps.xxwc_ap_payment_terms_ext_vw (operating_unit_id,
                                                                interface_date,
                                                                payment_term_data
                                                               )
AS
   SELECT operating_unit_id, SYSDATE interface_date,
          (   business_unit
           || '|'
           || source_system
           || '|'
           || payterm_id
           || '|'
           || payment_term_description
           || '|'
           || perc_discount
           || '|'
           || discount_days
           || '|'
           || due_days
           || '|'
           || proxy_term
           || '|'
           || proxy_day
           || '|'
           || proxy_month
          ) payment_term_data
     FROM xxwc_ap_payment_terms_vw;


