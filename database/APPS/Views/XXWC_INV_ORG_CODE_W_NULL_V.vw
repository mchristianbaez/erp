/* Formatted on 11/12/2013 2:36:17 PM (QP5 v5.115.810.9015) */
CREATE OR REPLACE FORCE VIEW apps.xxwc_inv_org_code_w_null_v (
   organization_code
)
AS
	 SELECT   x1.organization_code
	   FROM   (  SELECT   organization_code FROM mtl_parameters
			   UNION
				 SELECT   NULL FROM DUAL) x1
   ORDER BY   1 ASC NULLS FIRST;


COMMENT ON TABLE apps.xxwc_inv_org_code_w_null_v IS '11/12/2013 CG: TMS 20131112-00284: New view for Bin Maint Org Content parameter to be able to default to NULL';
