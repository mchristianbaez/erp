
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUSOZF_PS_NOACCRUAL_VW" ("MVID", "VENDOR", "SHIP_FROM_VENDOR", "BU_SKU", "LOB", "PERIOD", "RECEIPT_DATE", "SELLING_PRICE", "QUANTITY") AS 
  SELECT  substr(hca.account_number, 0, instr(hca.account_number, '~') - 1) mvid
      ,hp.party_name vendor
      ,a.ship_from_party_name ship_from_vendor
      ,msi.description bu_sku
      ,a.bill_to_party_name lob
      ,period.name period
      ,a.date_ordered receipt_date
      ,a.selling_price selling_price
      ,a.quantity quantity
        FROM ozf.ozf_resale_lines_all a
      ,hz_cust_accounts hca
      ,mtl_system_items_b msi
      --,apps.mtl_item_categories_v
      ,ozf_time_ent_period period
      ,hz_parties hp
      ,(SELECT DISTINCT to_number(qq.qualifier_attr_value) cust_id
                       --,qlhv.list_header_id offer_id --, qlhv.description rebate_name
          FROM qp_list_headers_vl    qlhv
              ,qp_pricing_attributes qpa
              ,qp_qualifiers         qq
         WHERE qlhv.list_header_id = qq.list_header_id
           AND qpa.list_header_id = qlhv.list_header_id
           AND qq.qualifier_context = 'SOLD_BY'
           AND qq.qualifier_attribute = 'QUALIFIER_ATTRIBUTE2'
           AND qpa.product_attribute = 'PRICING_ATTRIBUTE25'
           AND qlhv.active_flag = 'Y') offer
WHERE 1 = 1
   AND hp.party_id = hca.party_id
      --and offer.offer_id=
   AND a.sold_from_cust_account_id = offer.cust_id
   AND a.sold_from_cust_account_id = hca.cust_account_id
   AND msi.organization_id = 84
   AND msi.inventory_item_id = a.inventory_item_id
   AND a.date_ordered >= period.start_date ---enter start date for receipts
   AND a.date_ordered <= period.end_date ---enter end date for receipts\
  -- AND hp.party_name = 'QUEEN CITY PLASTICS INC'
   --AND a.date_ordered BETWEEN '30-JAN-2012' AND '26-FEB-2012'
   --and resale_line_id = 11270386
      -- and a.inventory_item_id=1498341
   AND NOT EXISTS
(SELECT 1
          FROM ozf_funds_utilized_all_b ofu, apps.mtl_item_categories_v c
         WHERE ofu.object_id = a.resale_line_id
           AND c.inventory_item_id = ofu.product_id
           AND c.category_set_id = 1100000041
           AND c.organization_id = 84
           AND c.segment1 = hca.attribute2
           AND ofu.cust_account_id = hca.cust_account_id
           --AND ofu.utilization_type LIKE 'ADJ%'
           )
 GROUP BY substr(hca.account_number, 0, instr(hca.account_number, '~') - 1)
         ,hp.party_name
         ,a.ship_from_party_name
          -- ,offer.rebate_name
         ,msi.description
         ,a.bill_to_party_name
         ,period.name
         ,a.date_ordered
         ,a.selling_price
         ,a.quantity
;
