--
-- XXWC_INV_LOTLABLES_REP_V  (View)
--

CREATE OR REPLACE FORCE VIEW APPS.XXWC_INV_LOTLABLES_REP_V
(
   LOT_NUM
  ,ORGANIZATION_ID
  ,EXPIRATION_DATE
  ,INVENTORY_ITEM_ID
)
AS
   SELECT mln.lot_number
         ,mln.organization_id
         ,mln.expiration_date
         ,mln.inventory_item_id
     FROM mtl_lot_numbers mln
    WHERE EXISTS
             (SELECT *
                FROM mtl_onhand_quantities_detail moqd
               WHERE     moqd.inventory_item_id = mln.inventory_item_id
                     AND moqd.organization_id = mln.organization_id
                     AND moqd.lot_number = mln.lot_number);


