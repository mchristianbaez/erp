
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUSOZF_INC_TREND_VW" ("MVID", "MV_NAME", "LOB", "CAL_YEAR", "REBATE_TYPE", "JAN_INC", "FEB_INC", "MAR_INC", "APR_INC", "MAY_INC", "JUN_INC", "JUL_INC", "AUG_INC", "SEP_INC", "OCT_INC", "NOV_INC", "DEC_INC") AS 
  SELECT mvid,
            mv_name,
            LOB,
            cal_year,
            rebate_type,
            SUM (JAN_INC) JAN_INC,
            SUM (FEB_INC) FEB_INC,
            SUM (MAR_INC) MAR_INC,
            SUM (APR_INC) APR_INC,
            SUM (MAY_INC) MAY_INC,
            SUM (JUN_INC) JUN_INC,
            SUM (JUL_INC) JUL_INC,
            SUM (AUG_INC) AUG_INC,
            SUM (SEP_INC) SEP_INC,
            SUM (OCT_INC) OCT_INC,
            SUM (NOV_INC) NOV_INC,
            SUM (DEC_INC) DEC_INC
       FROM (  SELECT hca.account_number mvid,
                      hp.party_name mv_name,
                      hp1.party_name LOB,
                      accr.calendar_year cal_year,
                      --accr.rebate_type_id rebate_type,
                      DECODE (accr.rebate_type_id,
                              'COOP', 'COOP',
                              'COOP_MIN', 'COOP',
                              'REBATE')
                         rebate_type,
                      XXCUS_OZF_RPTUTIL_PKG.get_period_income (
                         accr.calendar_year,
                         hca.cust_account_id,
                         hp1.party_id,
                         accr.rebate_type_id,
                         12)
                         JAN_INC,
                      XXCUS_OZF_RPTUTIL_PKG.get_period_income (
                         accr.calendar_year,
                         hca.cust_account_id,
                         hp1.party_id,
                         accr.rebate_type_id,
                         1)
                         FEB_INC,
                      XXCUS_OZF_RPTUTIL_PKG.get_period_income (
                         accr.calendar_year,
                         hca.cust_account_id,
                         hp1.party_id,
                         accr.rebate_type_id,
                         2)
                         MAR_INC,
                      XXCUS_OZF_RPTUTIL_PKG.get_period_income (
                         accr.calendar_year,
                         hca.cust_account_id,
                         hp1.party_id,
                         accr.rebate_type_id,
                         3)
                         APR_INC,
                      XXCUS_OZF_RPTUTIL_PKG.get_period_income (
                         accr.calendar_year,
                         hca.cust_account_id,
                         hp1.party_id,
                         accr.rebate_type_id,
                         4)
                         MAY_INC,
                      XXCUS_OZF_RPTUTIL_PKG.get_period_income (
                         accr.calendar_year,
                         hca.cust_account_id,
                         hp1.party_id,
                         accr.rebate_type_id,
                         5)
                         JUN_INC,
                      XXCUS_OZF_RPTUTIL_PKG.get_period_income (
                         accr.calendar_year,
                         hca.cust_account_id,
                         hp1.party_id,
                         accr.rebate_type_id,
                         6)
                         JUL_INC,
                      XXCUS_OZF_RPTUTIL_PKG.get_period_income (
                         accr.calendar_year,
                         hca.cust_account_id,
                         hp1.party_id,
                         accr.rebate_type_id,
                         7)
                         AUG_INC,
                      XXCUS_OZF_RPTUTIL_PKG.get_period_income (
                         accr.calendar_year,
                         hca.cust_account_id,
                         hp1.party_id,
                         accr.rebate_type_id,
                         8)
                         SEP_INC,
                      XXCUS_OZF_RPTUTIL_PKG.get_period_income (
                         accr.calendar_year,
                         hca.cust_account_id,
                         hp1.party_id,
                         accr.rebate_type_id,
                         9)
                         OCT_INC,
                      XXCUS_OZF_RPTUTIL_PKG.get_period_income (
                         accr.calendar_year,
                         hca.cust_account_id,
                         hp1.party_id,
                         accr.rebate_type_id,
                         10)
                         NOV_INC,
                      XXCUS_OZF_RPTUTIL_PKG.get_period_income (
                         accr.calendar_year,
                         hca.cust_account_id,
                         hp1.party_id,
                         accr.rebate_type_id,
                         11)
                         DEC_INC
                 FROM xxcusozf_accruals_mv accr,
                      hz_cust_accounts hca,
                      hz_parties hp,
                      hz_parties hp1
                WHERE     1 = 1
                    and grp_mvid=0
                    and grp_cal_year=0
                    and grp_lob=0
                    and grp_period=0
                    and grp_qtr=1
                    and grp_plan_id=1
                    and grp_branch=1
                    and grp_year=1
                    and grp_adj_type=1
                    and grp_rebate_type=0
                    and accr.mvid=hca.cust_account_id
                    and hca.party_id=hp.party_id
                    and accr.lob_id=hp1.party_id
                  /*
                      AND accr.mvid = hca.cust_account_id
                      AND hca.party_id = hp.party_id
                      AND accr.lob_id = hp1.party_id
                      --and accr.calendar_year=2011
                      --and accr.mvid =
                      AND grp_mvid = 0
                      AND grp_cal_year = 0
                      AND grp_lob = 0
                      AND grp_period = 0
                      AND grp_qtr = 1
                      AND grp_plan_id = 1
                      AND grp_branch = 1
                      AND grp_year = 1
                      AND grp_adj_type = 1
                      AND grp_rebate_type = 0
                   */
             GROUP BY hca.account_number,
                      hp.party_name,
                      accr.calendar_year,
                      hca.cust_account_id,
                      hp1.party_name,
                      hp1.party_id,
                      accr.rebate_type_id
             UNION
             SELECT DISTINCT hca.account_number mvid,
                             hp.party_name mv_name,
                             NULL LOB,
                             qlhv.attribute7 cal_year,
                             NULL income_type,
                             0,
                             0,
                             0,
                             0,
                             0,
                             0,
                             0,
                             0,
                             0,
                             0,
                             0,
                             0
               FROM qp_list_headers_vl qlhv,
                    qp_qualifiers qq,
                    hz_parties hp,
                    hz_cust_accounts hca
              WHERE     qlhv.list_header_id = qq.list_header_id
                    AND qq.qualifier_context = 'SOLD_BY'
                    AND hca.cust_account_id =
                           TO_NUMBER (qq.qualifier_attr_value)
                    AND hca.party_id = hp.party_id
                    AND qq.qualifier_attribute = 'QUALIFIER_ATTRIBUTE2'
                    AND qq.list_line_id = -1
                    AND NOT EXISTS (select 1
                                    from xxcusozf_accruals_mv accr
                                     WHERE 1 =1
                                       and grp_mvid=0
                                       and grp_cal_year=0
                                       and grp_lob=0
                                       and grp_period=0
                                       and grp_qtr=1
                                       and grp_plan_id=1
                                       and grp_branch=1
                                       and grp_year=1
                                       and grp_adj_type=1
                                       and grp_rebate_type=0
                                       and accr.mvid=hca.cust_account_id
                                       and accr.calendar_year = qlhv.attribute7
                                    )
                    /*
                    AND NOT EXISTS
                               (SELECT 1
                                  FROM xxcusozf_accruals_mv accr
                                 WHERE     accr.mvid = hca.cust_account_id
                                       AND accr.calendar_year = qlhv.attribute7
                                       AND grp_mvid = 0
                                       AND grp_cal_year = 0
                                       AND grp_lob = 0
                                       AND grp_period = 0
                                       AND grp_qtr = 1
                                       AND grp_plan_id = 1
                                       AND grp_branch = 1
                                       AND grp_year = 1
                                       AND grp_adj_type = 1
                                       AND grp_rebate_type = 0
                                       )
                   */
)
   GROUP BY mvid,
            mv_name,
            LOB,
            cal_year,
            rebate_type
   ORDER BY 1
;
