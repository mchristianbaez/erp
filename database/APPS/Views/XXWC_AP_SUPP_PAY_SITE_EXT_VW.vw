/* Formatted on 2012/08/31 09:41 (Formatter Plus v4.8.8) */
CREATE OR REPLACE FORCE VIEW apps.xxwc_ap_supp_pay_site_ext_vw (operating_unit_id,
                                                                interface_date,
                                                                pay_site_data
                                                               )
AS
   SELECT operating_unit_id, supplier_creation_date interface_date,
          (   business_unit
           || '|'
           || source_system
           || '|'
           || vendor_type_lookup_code
           || '|'
           || site_status
           || '|'
           || vendor_number
           || vendor_site_code
           || '|'
           || vendor_name
           || '|'
           || pay_to_vendor_name
           || '|'
           || pay_to_tax_payer_id
           || '|'
           || SUBSTR (address1, 1, 250)
           || '|'
           || SUBSTR (address2, 1, 250)
           || '|'
           || SUBSTR (address3, 1, 250)
           || '|'
           || SUBSTR (city, 1, 40)
           || '|'
           || SUBSTR (state_province, 1, 50)
           || '|'
           || SUBSTR (postal_code, 1, 20)
           || '|'
           || SUBSTR (fax_number, 1, 20)
           || '|'
           || SUBSTR (country, 1, 30)
           || '|'
           || SUBSTR (phone_number, 1, 20)
           || '|'
           || SUBSTR (vendor_name, 1, 250)
           || '|'
           || SUBSTR (address1, 1, 50)
           || '|'
           || SUBSTR (address23, 1, 50)
           || '|'
           || city
           || '|'
           || state_province
           || '|'
           || postal_code
           || '|'
           || fax_number
           || '|'
           || country
           || '|'
           || phone_number
           || '|'
           || payment_term_id
           || '|'
           || TO_CHAR (supplier_creation_date, 'MM/DD/YYYY')
          ) pay_site_data
     FROM xxwc_ap_supplier_sites_vw
    WHERE NVL (pay_site_flag, 'N') = 'Y';


