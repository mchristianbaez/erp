
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUSGL_LOB_BRANCH_APEX_VW" ("FRU", "FRU_DESCR", "LOCATION", "LOB_BRANCH", "BUSINESS_UNIT", "POSTING_ENABLED") AS 
  SELECT upper(xlc.fru) fru
      ,xlc.fru_descr
      ,ffv.flex_value location
      ,xlc.lob_branch
      ,xlc.business_unit
      ,substr(ffv.compiled_value_attributes, 3, 1) posting_enabled
  FROM xxcus.xxcus_location_code_tbl xlc, apps.fnd_flex_values ffv
 WHERE xlc.entrp_loc = ffv.flex_value
   AND ffv.flex_value_set_id = 1014548
;
