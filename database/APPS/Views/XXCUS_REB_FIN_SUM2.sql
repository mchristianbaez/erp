
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUS_REB_FIN_SUM2" ("COLLECTOR", "CUST_ID", "MVID", "VENDOR_NAME", "AGREEMENT_YEAR", "PURCHASES", "INCOME", "INVOICE_AMOUNT", "APPLIED_AMOUNT", "UNAPPLIED_AMOUNT", "AGING_30", "AGING_31_60", "AGING_61_90", "AGING_91_120", "AGING_OVER_120") AS 
  SELECT 
  DISTINCT AR.NAME COLLECTOR,
  m.cust_id cust_id,
  m.mvid mvid,
  m.vendor_name Vendor_Name,
  m.agreement_year Agreement_Year,
  m.Purchases Purchases,
  m.Income Income,
  m.Invoice_Amount Invoice_Amount,
  m.Applied_Amount Applied_Amount,
  m.Unapplied_Amount Unapplied_Amount,
  m.Aging_30 Aging_30,
  m.Aging_31_60 Aging_31_60,
  M.AGING_61_90 AGING_61_90,
  m.Aging_91_120 Aging_91_120,
  m.Aging_over_120 Aging_over_120
  from 
    (
    select
      cust_id,
      mvid,
      vendor_name,
      agreement_year,
      -- max(Collector) Collector,
      sum(Purchases) Purchases,
      SUM(INCOME) INCOME,
      sum(Invoice) Invoice_Amount,
      sum(Applied_Amount) Applied_Amount,
      sum(Unapplied_Amount) Unapplied_Amount,
      sum ( Aging_30) Aging_30,
      sum(Aging_31_60) Aging_31_60,
      SUM( AGING_61_90) AGING_61_90,
      sum( Aging_91_120) Aging_91_120,
      sum(Aging_over_120) Aging_over_120
    FROM 
        (

        --PART 1 Get Purchase Details and Unapplied Payments 

    SELECT 
       UNAPP.CUST_ID CUST_ID
        ,C.CUSTOMER_ATTRIBUTE2 MVID
      ,c.party_name vendor_name
      , TO_CHAR(UNAPP.AGREEMENT_YEAR) AGREEMENT_YEAR
      ,SUM(PURCHASES) PURCHASES
       ,0 INCOME
       ,0 INVOICE
       ,0 APPLIED_AMOUNT
      ,SUM(UNAPPLIED_AMOUNT) UNAPPLIED_AMOUNT
      ,0 AGING_30
       ,0 AGING_31_60
        ,0 AGING_61_90
         ,0 AGING_91_120
          ,0 Aging_over_120
    FROM 
    (
        SELECT 
          P.MVID CUST_ID
         ,to_number(P.CALENDAR_YEAR) AGREEMENT_YEAR
          ,P.TOTAL_PURCHASES PURCHASES
          , 0 unapplied_amount
        FROM 
          APPS.XXCUSOZF_PURCHASES_MV P
        WHERE 1=1
          AND grp_mvid =0
          AND GRP_CAL_YEAR =0
          AND grp_lob =1
          AND grp_period =1
          AND grp_qtr =1
          AND grp_branch =1
          AND GRP_YEAR =1
          AND GRP_CAL_PERIOD =1
                   
          union   
                 
        SELECT 
            CR.PAY_FROM_CUSTOMER CUST_ID
            , to_number(substr(cr.attribute2,-4,4)) AGREEMENT_YEAR
              ,0 Purchases
            ,sum (abs(sch.amount_due_remaining))unapplied_amount
          FROM 
            ar.ar_cash_receipts_all cr,
            AR.AR_RECEIPT_METHODS RM,
            APPS.HZ_PARTIES HP,
            APPS.HZ_CUST_ACCOUNTS HCA,
            ar.AR_PAYMENT_SCHEDULES_ALL SCH
          WHERE 1 =1
            AND cr.status NOT IN ('REV')
            AND cr.receipt_method_id =rm.receipt_method_id
            AND hca.cust_account_id =cr.pay_from_customer
            AND hca.party_id =hp.party_id
            AND cr.org_id IN (101,102)
            AND sch.CASH_RECEIPT_ID = cr.CASH_RECEIPT_ID
            AND rm.printed_name IN ('REBATE_CREDIT',
            'REBATE_CHECK',
            'REBATE_DEDUCTION','CONVERSION')
            AND CR.STATUS='UNAPP' 
          GROUP BY 
            CR.PAY_FROM_CUSTOMER 
            ,  to_number(substr(cr.attribute2,-4,4))
              ) UNAPP,
               xxcus.xxcus_rebate_customers c
          WHERE 1=1
            AND UNAPP.CUST_ID =C.CUSTOMER_ID
          GROUP BY 
            UNAPP.CUST_ID
            ,UNAPP.AGREEMENT_YEAR
            ,C.CUSTOMER_ATTRIBUTE2
            ,C.PARTY_NAME 

  UNION 
----PART 2 Get YTD Income 

  SELECT 
      XLA.OFU_CUST_ACCOUNT_ID CUST_ID,
      xla.ofu_mvid mvid,
      c.party_name vendor_name,
      xla.calendar_year agreement_year,
      -- null Collector,
      0 Purchases,
      sum(util_acctd_amount) Income,
      0 Invoice,
      0 Applied_Amount,
      0 Unapplied_Amount,
      0 Aging_30,
      0 Aging_31_60,
      0 Aging_61_90,
      0 Aging_91_120,
      0 AGING_OVER_120
  FROM 
      xxcus.xxcus_ozf_xla_accruals_b xla,
      xxcus.xxcus_rebate_customers c
  WHERE 1=1
      and c.customer_id=xla.ofu_cust_account_id
      and c.customer_attribute1='HDS_MVID'
  GROUP BY 
        XLA.OFU_CUST_ACCOUNT_ID
      , xla.ofu_mvid
      , c.party_name
      , XLA.CALENDAR_YEAR 

  UNION


  --PART 3  GET INVOICE AMOUNT AND PAYMENT AMOUNT
  SELECT 
      CUST_ID CUST_ID
      ,MVID mvid
      ,MV_NAME VENDOR_NAME
      ,to_char(AGREEMENT_YEAR) agreement_year 
      ,0 PURCHASES
      ,0 INCOME
      ,SUM(NVL(INVOICE_AMOUNT,0)) INVOICE
      ,SUM(NVL(PAYMENT_AMOUNT,0)) APPLIED_AMOUNT
      ,0 UNAPPLIED_AMOUNT
      ,SUM(AGING_30) AGING_30
      ,SUM(AGING_31_60) AGING_31_60
      ,SUM(AGING_61_90) AGING_61_90
      ,SUM(AGING_91_120) AGING_91_120
      ,SUM(AGING_OVER_120) AGING_OVER_120
  from 
      (
       -- INVOICE AMOUNT
        SELECT  
          C.CUSTOMER_ID CUST_ID,
          C.CUSTOMER_ATTRIBUTE2 MVID,
          C.PARTY_NAME MV_NAME,
          QLHV.ATTRIBUTE7 AGREEMENT_YEAR,
          SUM( NVL(aps.amount_due_original,0)+nvl(aps.amount_adjusted,0)) INVOICE_AMOUNT,
          sum(nvl(aps.amount_applied,0)) Payment_amount,
          SUM(CASE WHEN SYSDATE-APS.DUE_DATE<31 THEN ABS(APS.AMOUNT_DUE_REMAINING) ELSE 0 END) AGING_30,
          sum(case when sysdate-aps.due_date between 31  AND 60 THEN ABS(aps.AMOUNT_DUE_REMAINING) ELSE 0 END) AGING_31_60,
          sum(case when sysdate-aps.due_date between 61 AND 90 THEN ABS(aps.AMOUNT_DUE_REMAINING) ELSE 0 END) AGING_61_90,
          SUM(CASE WHEN SYSDATE-APS.DUE_DATE BETWEEN 91 AND 120 THEN ABS(APS.AMOUNT_DUE_REMAINING) ELSE 0 END) AGING_91_120,
          SUM(CASE WHEN SYSDATE-aps.DUE_DATE>120 THEN ABS(aps.AMOUNT_DUE_REMAINING) ELSE 0 END) AGING_OVER_120
       FROM 
         APPS.AR_PAYMENT_SCHEDULES_ALL APS,
         apps.ra_CUSTOMER_TRX_ALL hdr,
          APPS.QP_LIST_HEADERS_VL QLHV,
          APPS.OZF_CLAIM_LINES_ALL XRF,
          --APPS.FND_LOOKUP_VALUES FLV,
          OZF.OZF_OFFERS OO ,
          APPS.AMS_MEDIA_VL MED ,
          APPS.OZF_CLAIMS_ALL OCA,
          XXCUS.XXCUS_REBATE_CUSTOMERS C

         
        WHERE 1=1
           AND C.CUSTOMER_ATTRIBUTE1='HDS_MVID'
           AND HDR.ORG_ID IN ('101','102')
           AND C.CUSTOMER_ID = HDR.BILL_TO_CUSTOMER_ID 
           AND APS.CUSTOMER_TRX_ID=HDR.CUSTOMER_TRX_ID
           AND OCA.CLAIM_NUMBER=HDR.INTERFACE_HEADER_ATTRIBUTE1
           AND OCA.CLAIM_ID    = XRF.CLAIM_ID
           AND XRF.ACTIVITY_ID = QLHV.LIST_HEADER_ID         
          -- AND FLV.LOOKUP_TYPE = 'XXCUS_REBATE_BU_XREF'
           --AND FLV.MEANING = HDR.ATTRIBUTE2(+)
           AND OO.QP_LIST_HEADER_ID       = QLHV.LIST_HEADER_ID
           AND OO.ACTIVITY_MEDIA_ID       = MED.MEDIA_ID          
        GROUP BY 
          C.CUSTOMER_ID,
          C.CUSTOMER_ATTRIBUTE2,
          C.PARTY_NAME,
          QLHV.ATTRIBUTE7

      )

GROUP BY 
    CUST_ID
    ,MVID
    ,MV_NAME
    ,AGREEMENT_YEAR 

     )
    GROUP BY 
      cust_id,
      mvid,
      vendor_name,
      agreement_year ) m,
      APPS.HZ_CUSTOMER_PROFILES HCP,
      apps.ar_collectors ar
  WHERE 1=1
    AND M.CUST_ID = HCP.CUST_ACCOUNT_ID
    AND AR.COLLECTOR_ID = HCP.COLLECTOR_ID 
    AND HCP.SITE_USE_ID IS NULL

;
