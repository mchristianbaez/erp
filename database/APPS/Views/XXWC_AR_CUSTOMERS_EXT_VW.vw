/* Formatted on 10/4/2013 2:17:19 PM (QP5 v5.115.810.9015) */
CREATE OR REPLACE FORCE VIEW apps.xxwc_ar_customers_ext_vw (
   operating_unit_id
 , interface_date
 , customer_data
)
AS
   SELECT	x1.operating_unit_id
		  , SYSDATE interface_date
		  , (	substr(x1.business_unit, 0,30)
			 || '|'
			 || substr(x1.source_system, 0,20)
			 || '|'
			 || substr(TRIM (x1.cust_unique_identifier), 0,50)
			 || '|'
			 || substr(TRIM (x1.cust_account_code), 0, 50)
			 || '|'
			 || REGEXP_REPLACE (substr(TRIM (x1.cust_account_name), 0, 100), '\|', '')
			 || '|'
			 -- 11/16/12 CG: Changed
			 --|| TRIM (x1.cust_acount_type)
			 || substr(TRIM (x1.site_use_type), 0,50)
			 || '|'
			 || substr(TRIM (x1.cust_account_class),0,50)
			 || '|'
			 || substr(TRIM (x1.cust_rollup_level1_label),0,20)
			 || '|'
			 || REGEXP_REPLACE (substr(TRIM (x1.cust_rollup_level1_code),0,30), '\|', '')
			 || '|'
			 || REGEXP_REPLACE (substr(TRIM (x1.cust_rollup_level1_desc),0,100), '\|', '')
			 || '|'
			 || substr(TRIM (x1.cust_rollup_level2_label),0,20)
			 || '|'
			 || substr(TRIM (x1.cust_rollup_level2_code),0,50)
			 || '|'
			 || substr(TRIM (x1.cust_rollup_level2_desc),0,100)
			 || '|'
			 || substr(TRIM (x1.cust_rollup_level3_label),0,20)
			 || '|'
			 || substr(TRIM (x1.cust_rollup_level3_code),0,30)
			 || '|'
			 || substr(TRIM (x1.cust_rollup_level3_value),0,100)
			 || '|'
			 || substr(TRIM (x1.reporting_group_identifier),0,50)
			 || '|'
			 || substr(TRIM (x1.homelocation),0,50)
			 || '|'
			 || substr(TRIM (x1.in_freight_exempt_flag),0,1)
			 || '|'
			 || substr(TRIM (x1.out_freight_exempt_flag),0,1)
			 || '|'
			 || substr(TRIM (x1.passthrough_disc_eligible_flag),0,1)
			 || '|'
			 || TRIM (x1.rebatepercentage)
			 || '|'
			 || substr(TRIM (x1.payment_terms_desc),0,100)
			 || '|'
			 || TRIM (x1.payment_term_disc_perc)
			 || '|'
			 || TRIM (x1.credit_limit_amt)
			 || '|'
			 || substr(TRIM (x1.taxid),0,50)
			 || '|'
			 -- 01/30/13 CG: TMS 20130122-01580 - Added the Customer Account Date Established
			 || TO_CHAR (NVL (x1.account_established_date, x1.creation_date)
					   , 'MM/DD/YYYY')
			 || '|'
			 || TO_CHAR (x1.last_update_date, 'MM/DD/YYYY')
			 || '|'
			 || TO_CHAR (x1.firstsaledate, 'MM/DD/YYYY')
			 || '|'
			 || TO_CHAR (x1.lastsaledate, 'MM/DD/YYYY')
			 || '|'
			 || substr(TRIM (x1.cust_account_status),0,50)
			 || '|'
			 || REGEXP_REPLACE (substr(TRIM (x1.address1),0,100), '\|', '')
			 || '|'
			 || REGEXP_REPLACE (substr(TRIM (x1.address2),0,100), '\|', '')
			 || '|'
			 || REGEXP_REPLACE (substr(TRIM (x1.address3),0,100), '\|', '')
			 || '|'
			 || substr(TRIM (x1.city),0,100)
			 || '|'
			 || substr(TRIM (x1.state_province),0,2)
			 || '|'
			 || substr(TRIM (x1.postal_code),0,10)
			 || '|'
			 || substr(TRIM (x1.country),0,2)
			 || '|'
			 || REGEXP_REPLACE (substr(TRIM (x1.main_phone_number),0,20), '\|', '')
			 || '|'
			 || REGEXP_REPLACE (substr(TRIM (x1.main_fax_number),0,20), '\|', '')
			 || '|'
			 || REGEXP_REPLACE (substr(TRIM (x1.main_email),0,50), '\|', '')
			 || '|'
			 || substr(TRIM (x1.inside_salesrep_id),0,50)
			 || '|'
			 || substr(TRIM (x1.outside_salesrep_id),0,50)
			 || '|'
			 || REGEXP_REPLACE (substr(TRIM (x1.default_bill_to_site_name),0,100), '\|', '')
			 || '|'
			 || REGEXP_REPLACE (substr(TRIM (x1.bill_to_address1),0,100), '\|', '')
			 || '|'
			 || REGEXP_REPLACE (substr(TRIM (x1.bill_to_address2),0,100), '\|', '')
			 || '|'
			 || REGEXP_REPLACE (substr(TRIM (x1.bill_to_address3),0,100), '\|', '')
			 || '|'
			 || substr(TRIM (x1.bill_to_city),0,100)
			 || '|'
			 || substr(TRIM (x1.bill_to_state_province),0,2)
			 || '|'
			 || substr(TRIM (x1.bill_to_postal_code),0,10)
			 || '|'
			 || substr(TRIM (x1.bill_to_country),0,2)
			 || '|'
			 || substr(TRIM (x1.custom_cust_attr1_label),0,30)
			 || '|'
			 || REGEXP_REPLACE (substr(TRIM (x1.custom_cust_attr1_value),0,100), '\|', '')
			 || '|'
			 || substr(TRIM (x1.custom_cust_attr2_label),0,30)
			 || '|'
			 || REGEXP_REPLACE (substr(TRIM (x1.custom_cust_attr2_value),0,100), '\|', '')
			 || '|'
			 || substr(TRIM (x1.custom_cust_attr3_label),0,30)
			 || '|'
			 || REGEXP_REPLACE (substr(TRIM (x1.custom_cust_attr3_value),0,100), '\|', '')
			 || '|'
			 || substr(TRIM (x1.custom_cust_attr4_label),0,30)
			 || '|'
			 || REGEXP_REPLACE (substr(TRIM (x1.custom_cust_attr4_value),0,100), '\|', '')
			 || '|'
			 || substr(TRIM (x1.custom_cust_attr5_label),0,30)
			 || '|'
			 || REGEXP_REPLACE (substr(TRIM (x1.custom_cust_attr5_value),0,100), '\|', '')
             -- 10/04/2013 CG: TMS 20130815-00678
             || '|'
             || substr(TRIM(x1.TAX_EXEMPT_CERT_NUM),0,50)
             || '|'
             || TRIM(x1.TAX_EXEMPT_CERT_EXP_DATE)
             || '|'
             || substr(TRIM(x1.TAX_LOCALITY),0,20)
             || '|'
             || substr(TRIM(x1.PRICING_PROFIILE),0,7)
             || '|'
             || TRIM(x1.NAICS_CODE)
             || '|'
             || REGEXP_REPLACE (substr(TRIM(x1.DODGE_NUMBER),0,50), '\|', '')
             )
			   customer_data
	 FROM	XXWC.XXWC_AR_CUSTOMERS_MV_TBL x1; -- TMS# 20141212-00194


