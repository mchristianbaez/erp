create or replace view apps.xxwcpo_bpa_interface_v
as
select distinct document_number
/*
Created by Shankar Hariharan 03-Jan-2013
Used in xxwc_bpa_document_number value set definition.
and called from BPA Price upload WEBADI
*/
from xxwc.xxwcpo_bpa_interface
union
select 'ALL' from dual
/

