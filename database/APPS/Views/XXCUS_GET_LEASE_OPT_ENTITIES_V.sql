
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUS_GET_LEASE_OPT_ENTITIES_V" ("HDS_ENTITY", "NON_HDS_ENTITY", "LEASE_ID") AS 
  select 
         case 
          when tcur_lsee_tbl.text is null then torig_lsee_tbl.text
          else tcur_lsee_tbl.text
         end  hds_entity
        ,case 
          when llcur_lsee_tbl.text is null then llorig_lsee_tbl.text
          else llcur_lsee_tbl.text
         end  non_hds_entity 
        ,le.lease_id lease_id        
  from
   pn_leases_all le  
  ,(
    select ndet.text
          ,nhdr.lease_id
    from   pn_note_details ndet
          ,pn_note_headers nhdr
    where 1 =1
      and  ndet.note_header_id        =nhdr.note_header_id
      and  nhdr.note_type_lookup_code ='TCUR'
  ) tcur_lsee_tbl
 ,(
    select ndet.text
          ,nhdr.lease_id
    from   pn_note_details ndet
          ,pn_note_headers nhdr
          ,pn_leases_all le
    where 1 =1
      and  ndet.note_header_id        =nhdr.note_header_id
      and  nhdr.note_type_lookup_code ='TORIG' 
  ) torig_lsee_tbl 
 ,(
    select ndet.text
          ,nhdr.lease_id
    from   pn_note_details ndet
          ,pn_note_headers nhdr
    where 1 =1
      and  ndet.note_header_id        =nhdr.note_header_id
      and  nhdr.note_type_lookup_code ='LLCUR' 
  ) llcur_lsee_tbl   
 ,(
    select ndet.text
          ,nhdr.lease_id
    from   pn_note_details ndet
          ,pn_note_headers nhdr
    where 1 =1
      and  ndet.note_header_id        =nhdr.note_header_id
      and  nhdr.note_type_lookup_code ='LLORIG' 
  ) llorig_lsee_tbl
  where 1 =1 
    and le.lease_type_code            ='LSEE'
    and torig_lsee_tbl.lease_id(+)       =le.lease_id
    and tcur_lsee_tbl.lease_id(+)        =le.lease_id
    and llcur_lsee_tbl.lease_id(+)       =le.lease_id
    and llorig_lsee_tbl.lease_id(+)      =le.lease_id
 UNION  
  select 
         case 
          when ocur_owno_tbl.text is null then oorig_owno_tbl.text
          else ocur_owno_tbl.text
         end  hds_entity
        ,Null non_hds_entity 
        ,le.lease_id lease_id        
  from
   pn_leases_all le  
  ,(
    select ndet.text
          ,nhdr.lease_id
    from   pn_note_details ndet
          ,pn_note_headers nhdr
    where 1 =1
      and  ndet.note_header_id        =nhdr.note_header_id
      and  nhdr.note_type_lookup_code ='OCUR'
  ) ocur_owno_tbl
 ,(
    select ndet.text
          ,nhdr.lease_id
    from   pn_note_details ndet
          ,pn_note_headers nhdr
    where 1 =1
      and  ndet.note_header_id        =nhdr.note_header_id
      and  nhdr.note_type_lookup_code ='OORIG' 
  ) oorig_owno_tbl 
  where 1 =1
    and 'OWNO'                       =le.lease_type_code 
    and oorig_owno_tbl.lease_id(+)   =le.lease_id
    and ocur_owno_tbl.lease_id(+)    =le.lease_id    
 UNION  
  select 
         case 
          when tcur_lsee_tbl.text is null then torig_lsee_tbl.text
          else tcur_lsee_tbl.text
         end  hds_entity
        ,case 
          when llcur_lsee_tbl.text is null then llorig_lsee_tbl.text
          else llcur_lsee_tbl.text
         end  non_hds_entity 
        ,le.lease_id lease_id        
  from
   pn_leases_all le  
  ,(
    select ndet.text
          ,nhdr.lease_id
    from   pn_note_details ndet
          ,pn_note_headers nhdr
    where 1 =1
      and  ndet.note_header_id        =nhdr.note_header_id
      and  nhdr.note_type_lookup_code ='LLCUR'
  ) tcur_lsee_tbl
 ,(
    select ndet.text
          ,nhdr.lease_id
    from   pn_note_details ndet
          ,pn_note_headers nhdr
          ,pn_leases_all le
    where 1 =1
      and  ndet.note_header_id        =nhdr.note_header_id
      and  nhdr.note_type_lookup_code ='LLORIG' 
  ) torig_lsee_tbl 
 ,(
    select ndet.text
          ,nhdr.lease_id
    from   pn_note_details ndet
          ,pn_note_headers nhdr
    where 1 =1
      and  ndet.note_header_id        =nhdr.note_header_id
      and  nhdr.note_type_lookup_code ='TCUR' 
  ) llcur_lsee_tbl   
 ,(
    select ndet.text
          ,nhdr.lease_id
    from   pn_note_details ndet
          ,pn_note_headers nhdr
    where 1 =1
      and  ndet.note_header_id        =nhdr.note_header_id
      and  nhdr.note_type_lookup_code ='TORIG' 
  ) llorig_lsee_tbl
  where 1 =1 
    and le.lease_type_code            IN ('LSE', 'ISUB')
    and torig_lsee_tbl.lease_id(+)       =le.lease_id
    and tcur_lsee_tbl.lease_id(+)        =le.lease_id
    and llcur_lsee_tbl.lease_id(+)       =le.lease_id
    and llorig_lsee_tbl.lease_id(+)      =le.lease_id
 UNION
   select Null hds_entity
         ,Null non_hds_entity 
         ,le.lease_id lease_id
   from   pn_leases_all le
   where  1 =1
     and  lease_type_code IN ('NON', 'NONL', 'ISA');
