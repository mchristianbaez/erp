/* Formatted on 4/3/2013 4:28:14 AM (QP5 v5.206) */
-- Start of DDL Script for View APPS.XXWC_CUSTOMER_SITES_SR
-- Generated 4/3/2013 4:28:10 AM from APPS@EBIZDEV

CREATE OR REPLACE VIEW apps.xxwc_customer_sites_sr
(
    cust_account_id
   ,party_id
   ,account_number
   ,cust_accounts_status
   ,account_name
   ,cust_acct_site_id
   ,cust_acct_sites_status
   ,org_id
   ,cust_acct_sites_bill_to_flag
   ,cust_acct_sites_ship_to_flag
   ,party_site_name
   ,party_site_number
   ,location_id
   ,party_site_status
   ,address1
   ,address2
   ,city
   ,state
   ,postal_code
   ,location
   ,bill_to_site_use_id
   ,site_use_id
   ,site_use_code
   ,primary_flag
   ,cust_site_uses_status
   ,cust_site_uses_org_id
   ,primary_salesrep_id
   ,attribute6
   ,attribute17
   ,attribute19
)
AS
    SELECT "CUST_ACCOUNT_ID"
          ,"PARTY_ID"
          ,"ACCOUNT_NUMBER"
          ,"CUST_ACCOUNTS_STATUS"
          ,"ACCOUNT_NAME"
          ,"CUST_ACCT_SITE_ID"
          ,"CUST_ACCT_SITES_STATUS"
          ,"ORG_ID"
          ,"CUST_ACCT_SITES_BILL_TO_FLAG"
          ,"CUST_ACCT_SITES_SHIP_TO_FLAG"
          ,"PARTY_SITE_NAME"
          ,"PARTY_SITE_NUMBER"
          ,"LOCATION_ID"
          ,"PARTY_SITE_STATUS"
          ,"ADDRESS1"
          ,"ADDRESS2"
          ,"CITY"
          ,"STATE"
          ,"POSTAL_CODE"
          ,"LOCATION"
          ,"BILL_TO_SITE_USE_ID"
          ,"SITE_USE_ID"
          ,"SITE_USE_CODE"
          ,"PRIMARY_FLAG"
          ,"CUST_SITE_USES_STATUS"
          ,"CUST_SITE_USES_ORG_ID"
          ,"PRIMARY_SALESREP_ID"
          ,"ATTRIBUTE6"
          ,"ATTRIBUTE17"
          ,"ATTRIBUTE19"
      FROM xxwc_customer_sites_vr
     WHERE site_use_code = 'SHIP_TO'
/

-- End of DDL Script for View APPS.XXWC_CUSTOMER_SITES_SR
