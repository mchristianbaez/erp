
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUS_LOCATION_CODE_VW" ("FRU", "FRU_DESCR", "INACTIVE", "LOB_BRANCH", "LOB_DEPT", "ENTRP_ENTITY", "ENTRP_LOC", "ENTRP_CC", "AP_DOCUM_FLAG", "HC_FLAG", "CREATION_DT", "SYSTEM_CD", "SYSTEM_CODE", "IEXP_FRU_OVERRIDE", "COUNTRY", "STATE", "EFFDT", "BUSINESS_UNIT") AS 
  (SELECT TRIM(fru)
      ,TRIM(fru_descr)
      ,TRIM(inactive)
      ,TRIM(lob_branch)
      ,TRIM(lob_dept)
      ,substr(entrp_entity, 2, 2) entrp_entity
      ,TRIM(entrp_loc)
      ,nvl(TRIM(entrp_cc), '0000') entrp_cc
      ,TRIM(ap_docum_flag)
      ,TRIM(hc_flag)
      ,creation_dt
      ,TRIM(system_cd)
      ,TRIM(SYSTEM_CODE)
      ,TRIM(iexp_fru_override)
      ,TRIM(country)
      ,TRIM(state)
      ,TRIM(EFFDT)
      ,TRIM(BUSINESS_UNIT)
  FROM xxcus.xxcus_location_code_tbl)
;
