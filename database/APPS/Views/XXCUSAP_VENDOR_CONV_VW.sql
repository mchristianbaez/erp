
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUSAP_VENDOR_CONV_VW" ("VENDOR_NAME", "VENDOR_ID_R12", "VENDOR_ID_R11", "VENDOR_SITE_CODE", "SITE_ID_R12", "SITE_ID_R11") AS 
  SELECT vendor_name
      ,v.vendor_id        vendor_id_r12
      ,s.attribute15      vendor_id_r11
      ,s.vendor_site_code
      ,s.vendor_site_id   site_id_r12
      ,s.attribute14      site_id_r11
  FROM ap.ap_suppliers v, ap.ap_supplier_sites_all s
 WHERE v.vendor_id = s.vendor_id
   AND s.org_id = 163
   and s.attribute15 is not null
   and vendor_site_code <> 'OFFICE'
;
