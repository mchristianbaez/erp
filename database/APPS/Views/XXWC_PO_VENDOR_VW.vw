--
-- XXWC_PO_VENDOR_VW  (View)
--

CREATE OR REPLACE FORCE VIEW APPS.XXWC_PO_VENDOR_VW
(
   VENDOR_ID
  ,VENDOR_NUMBER
  ,VENDOR_NAME
  ,PURCHASING_SITE_FLAG
)
AS
   SELECT a.vendor_id || '-' || b.vendor_site_id vendor_id
         ,a.segment1 || '-' || a.vendor_name || ' ### ' || b.vendor_site_code
             vendor_number
         ,a.vendor_name
         ,b.purchasing_site_flag
     -- used in LOV for PO Vendor Minimum upload and Freight Burden upload
     -- created by Shankar Hariharan
     FROM ap_suppliers a, ap_supplier_sites_all b -- Modified to Table by Manjula on 10-Dec-14 for TMS# 20141001-00057 Multiorg
    WHERE     a.vendor_id = b.vendor_id
          AND a.enabled_flag = 'Y'
          AND b.org_id = FND_PROFILE.VALUE('ORG_ID') -- Modified by Manjula on 10-Dec-14 for TMS# 20141001-00057  Multiorg 
		               --xxwc_ascp_scwb_pkg.get_wc_org_id
;


