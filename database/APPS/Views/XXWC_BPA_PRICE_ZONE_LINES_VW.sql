create or replace view XXWC_BPA_PRICE_ZONE_LINES_VW as
select distinct
   xppzt.po_header_id
  ,xppzt.inventory_item_id
  ,XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 0, 'PRICE') NATIONAL_PRICE
  ,XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 1, 'PRICE') PRICE_ZONE_PRICE1
  ,XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 2, 'PRICE') PRICE_ZONE_PRICE2
  ,XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 3, 'PRICE') PRICE_ZONE_PRICE3
  ,XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 4, 'PRICE') PRICE_ZONE_PRICE4
  ,XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 5, 'PRICE') PRICE_ZONE_PRICE5
  ,XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 6, 'PRICE') PRICE_ZONE_PRICE6
  ,XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 7, 'PRICE') PRICE_ZONE_PRICE7
  ,XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 8, 'PRICE') PRICE_ZONE_PRICE8
  ,XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 9, 'PRICE') PRICE_ZONE_PRICE9
  ,XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 10, 'PRICE') PRICE_ZONE_PRICE10
  ,XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 11, 'PRICE') PRICE_ZONE_PRICE11
  ,XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 12, 'PRICE') PRICE_ZONE_PRICE12
  ,XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 13, 'PRICE') PRICE_ZONE_PRICE13
  ,XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 14, 'PRICE') PRICE_ZONE_PRICE14
  ,XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 15, 'PRICE') PRICE_ZONE_PRICE15
  ,XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 16, 'PRICE') PRICE_ZONE_PRICE16
  ,XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 17, 'PRICE') PRICE_ZONE_PRICE17
  ,XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 18, 'PRICE') PRICE_ZONE_PRICE18
  ,XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 19, 'PRICE') PRICE_ZONE_PRICE19
  ,XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 20, 'PRICE') PRICE_ZONE_PRICE20
  ,XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 21, 'PRICE') PRICE_ZONE_PRICE21
  ,XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 22, 'PRICE') PRICE_ZONE_PRICE22
  ,XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 23, 'PRICE') PRICE_ZONE_PRICE23
  ,XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 24, 'PRICE') PRICE_ZONE_PRICE24
  ,XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 25, 'PRICE') PRICE_ZONE_PRICE25
  ,XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 26, 'PRICE') PRICE_ZONE_PRICE26
  ,XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 27, 'PRICE') PRICE_ZONE_PRICE27
  ,XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 28, 'PRICE') PRICE_ZONE_PRICE28
  ,XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 29, 'PRICE') PRICE_ZONE_PRICE29
  ,XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 30, 'PRICE') PRICE_ZONE_PRICE30
FROM   XXWC.XXWC_BPA_PRICE_ZONE_TBL xppzt
WHERE org_id=FND_PROFILE.VALUE('ORG_ID')