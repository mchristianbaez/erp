/* Formatted on 2012/08/31 09:41 (Formatter Plus v4.8.8) */
CREATE OR REPLACE FORCE VIEW apps.xxwc_inv_transfers_ext_vw (operating_unit_id,
                                                             interface_date,
                                                             transfer_data
                                                            )
AS
   SELECT operating_unit_id, order_date interface_date,
          (   business_unit
           || '|'
           || source_system
           || '|'
           || ship_from_branch_num
           || '|'
           || ship_from_branch_name
           || '|'
           || ship_to_branch_num
           || '|'
           || ship_to_branch_name
           || '|'
           || transfer_trx_identifier
           || '|'
           || line_status
           || '|'
           || transfer_type
           || '|'
           || TO_CHAR (order_date, 'MM/DD/YYYY')
           || '|'
           || TO_CHAR (shipped_date, 'MM/DD/YYYY')
           || '|'
           || TO_CHAR (receipt_date, 'MM/DD/YYYY')
           || '|'
           || sku_number
           || '|'
           || sku_description
           || '|'
           || ordered_quantity
           || '|'
           || shipped_quantity
           || '|'
           || received_quantity
           || '|'
           || transaction_cost
           || '|'
           || po_uom
           || '|'
           || conv_factor_for_each
           || '|'
           || po_ref_num
           || '|'
           || TO_CHAR (last_update_date, 'MM/DD/YYYY')
          ) transfer_data
     FROM xxwc_inv_transfers_vw;


