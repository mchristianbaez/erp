/* Formatted on 12/9/2013 3:01:13 PM (QP5 v5.115.810.9015) */
CREATE OR REPLACE FORCE VIEW apps.xxwc_po_vendor_minimum_vw (
   vendor_id
 , vendor_name
 , vendor_site_id
 , vendor_site_code
 , address_line1
 , address_line2
 , city
 , state
 , zip
 , organization_id
 , organization_code
 , organization_name
 , vendor_min_dollar
 , freight_min_dollar
 , freight_min_uom
 , freight_terms_lookup_code
 -- 12/10/13 CG: TMS 20130904-00654: Added two new fields
 , ppd_freight_units
 , notes
)
AS
   SELECT    xvm.vendor_id
          , pov.vendor_name
          , xvm.vendor_site_id
          , povs.vendor_site_code
          , povs.address_line1
          , povs.address_line2
          , povs.city
          , povs.state
          , povs.zip
          , xvm.organization_id
          , ood.organization_code
          , ood.organization_name
          , xvm.vendor_min_dollar
          , xvm.freight_min_dollar
          , xvm.freight_min_uom
          , pov.freight_terms_lookup_code
          -- 12/10/13 CG: TMS 20130904-00654: Added two new fields
          , xvm.ppd_freight_units
          -- , xvm.notes
          , substr(xxwc_ascp_scwb_pkg.get_vendor_minimum_notes(xvm.vendor_id
                                                      , xvm.vendor_site_id
                                                      , xvm.organization_id),1,4000)
     FROM   apps.xxwc_po_vendor_minimum xvm
          , ap_suppliers pov
          , apps.ap_supplier_sites povs
          , org_organization_definitions ood
    WHERE        xvm.vendor_id = pov.vendor_id
            AND xvm.vendor_site_id = povs.vendor_site_id(+)
            AND xvm.organization_id = ood.organization_id;


