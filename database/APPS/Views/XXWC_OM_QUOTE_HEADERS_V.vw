/* Formatted on 7/22/2013 10:28:37 AM (QP5 v5.115.810.9015) */
 /********************************************************************************
  FILE NAME: xxwc_om_quote_headers_v
  
  PROGRAM TYPE: view
  
  PURPOSE: This view used in XXWC_OM_QUTOE.fmb
  ===============================================================================
  VERSION DATE          AUTHOR(S)           DESCRIPTION
  ------- -----------   ---------------     -----------------------------------------
  1.0     07/26/2013    Consuelo Gonzalez   Initial creation (TMS#20130618-01268) 
  1.1     01/10/2017    Pattabhi Avula      TMS#20160708-00206 -- Added Creation_date
  ********************************************************************************/
CREATE OR REPLACE FORCE VIEW apps.xxwc_om_quote_headers_v (
   quote_number
 , created_name
 , salesrep_name
 , customer_info
 , job_location
 , branch
 , created_by
 , creation_date  -- Ver#1.1
 , salesrep_id
 , cust_account_id
 , site_use_id
 , organization_id
 , notes
)
AS
   
SELECT    a.quote_number
          , UPPER (NVL (b.description, b.user_name)) created_name
          , (SELECT   name
               FROM   apps.ra_salesreps
              WHERE   salesrep_id = a.salesrep_id AND ROWNUM = 1)
               salesrep_name
          , (SELECT   acc.account_number || ' - ' || hzp.party_name
               FROM   apps.hz_cust_accounts acc, apps.hz_parties hzp
              WHERE   acc.party_id = hzp.party_id
                      AND acc.cust_account_id = a.cust_account_id)
               customer_info
          , (SELECT   su.location
               FROM   apps.hz_cust_site_uses su, apps.ar_addresses_v ad
              WHERE   su.site_use_id = a.site_use_id
                      AND su.cust_acct_site_id = ad.address_id)
               job_location
          , (SELECT   org_code_name
               FROM   apps.xxwc_inv_organization_v inv
              WHERE   inv.organization_id = a.organization_id)
               branch
          , a.created_by
		  , a.creation_date -- Ver#1.1
          , a.salesrep_id
          , a.cust_account_id
          , a.site_use_id
          , a.organization_id
          -- 07/22/2013 CG: TMS 20130618-01268: Point 2: Adding header notes to result screen
          , a.notes
     FROM    apps.xxwc_om_quote_headers a, apps.fnd_user b
    WHERE    a.created_by = b.user_id
/