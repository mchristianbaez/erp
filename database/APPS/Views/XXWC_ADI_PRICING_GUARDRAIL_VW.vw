--
-- XXWC_ADI_PRICING_GUARDRAIL_VW  (View)
--

CREATE OR REPLACE FORCE VIEW APPS.XXWC_ADI_PRICING_GUARDRAIL_VW
(
   PRICING_GUARDRAIL_ID
  ,PRICE_ZONE
  ,CATEGORY
  ,ITEM_CATEOGRY_ID
  ,ITEM_NUMBER
  ,INVENTORY_ITEM_ID
  ,MIN_MARGIN
  ,MAX_DISCOUNT
  ,START_DATE
  ,END_DATE
)
AS
   SELECT pgr.PRICING_GUARDRAIL_ID
         ,pgr.PRICE_ZONE
         , (SELECT mc.CATEGORY_CONCAT_SEGS
              FROM MTL_CATEGORIES_V mc
             WHERE mc.Category_ID = pgr.Item_Category_ID)
             Category
         ,pgr.Item_Category_ID Item_Cateogry_ID
         , (SELECT msi.Segment1
              FROM mtl_System_Items_B msi
             WHERE     msi.Organization_ID =
                          FND_Profile.VALUE ('XXWC_ITEM_MASTER_ORG')
                   AND msi.Inventory_Item_ID = pgr.Inventory_Item_ID)
             Item_Number
         ,pgr.Inventory_Item_ID
         ,MIN_MARGIN
         ,MAX_DISCOUNT
         ,START_DATE
         ,END_DATE
     FROM XXWC_OM_PRICING_GUARDRAIL pgr
    WHERE pgr.Status = 'CURRENT';


