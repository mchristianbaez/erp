--
-- XXWC_BPA_PO_LINES_VW  (View)
--

CREATE OR REPLACE FORCE VIEW APPS.XXWC_BPA_PO_LINES_VW
(
   PO_HEADER_ID
  ,PO_LINE_ID
  ,LINE_NUM
  ,ITEM_NUMBER
  ,DESCRIPTION
  ,SUPPLIER_ITEM
  ,UNIT_PRICE
  ,SHIPMENT_NUM
  ,SHIP_TO_ORG
  ,QUANTITY
  ,PRICE_OVERRIDE
  ,START_DATE
  ,END_DATE
  ,NOT_TO_EXCEED_PRICE
  ,ALLOW_PRICE_OVERRIDE_FLAG
)
AS
   SELECT PL.PO_HEADER_ID
         ,PL.PO_LINE_ID
         ,PL.line_num
         ,MSI.segment1 item_number
         ,MSI.description
         ,PL.vendor_product_num supplier_item
         ,PL.unit_price
         ,PLL.shipment_num
         ,DECODE (MP.organization_code, 'MST', NULL, MP.organization_code)
             ship_to_org
         ,PLL.quantity
         ,PLL.price_override
         ,PLL.start_date
         ,PLL.end_date
         ,PL.NOT_TO_EXCEED_PRICE
         ,PL.ALLOW_PRICE_OVERRIDE_FLAG
     FROM apps.PO_LINES PL
         ,apps.PO_LINE_LOCATIONS PLL
         ,MTL_SYSTEM_ITEMS_B MSI
         ,MTL_PARAMETERS MP
    WHERE     PL.item_id = MSI.inventory_item_id
          AND pl.po_line_Id = pll.po_line_id(+)
          AND PLL.po_release_id IS NULL
          AND PLL.shipment_type(+) = 'PRICE BREAK'
          AND MSI.organization_id =
                 FND_PROFILE.VALUE ('XXWC_ITEM_MASTER_ORG')
          AND MP.organization_id(+) = PLL.ship_to_organization_id
          AND NVL (PL.Cancel_flag, 'N') = 'N';


