/* Formatted on 2012/08/31 09:41 (Formatter Plus v4.8.8) */
CREATE OR REPLACE FORCE VIEW apps.xxwc_gl_natural_accts_ext_vw (operating_unit_id,
                                                                interface_date,
                                                                gl_accounts_data
                                                               )
AS
   SELECT x1.operating_unit_id, SYSDATE interface_date,
          (   x1.business_unit
           || '|'
           || x1.source_system
           || '|'
           || x1.segment_value
           || '|'
           || x1.description
           || '|'
           || x1.khalix_account
          ) gl_accounts_data
     FROM xxwc_gl_natural_accts_vw x1
    WHERE flex_value_set_name = 'XXCUS_GL_ACCOUNT';


