
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUSOZF_RECON_SUMMARY_VW" ("BU_NM", "SDW_SPEND_TOTAL", "ORACLE_UNPROCESSED_TOTAL", "ORACLE_BASE_TABLE_TOTAL", "ORACLE_FAILED_INTERFACE_TOTAL", "PERIOD_NAME") AS 
  SELECT bu_nm
      ,SUM(sdw_spend_total) sdw_spend_total
      ,SUM(oracle_unprocessed_total) oracle_unprocessed_total
      ,SUM(oracle_base_table_total) oracle_base_table_total
      ,SUM(oracle_failed_interface_total) oracle_failed_interface_total
      ,period_name
  FROM (SELECT bu_nm
                       ,sdw_spend_total
                       ,0               oracle_unprocessed_total
                       ,0               oracle_base_table_total
                       ,0               oracle_failed_interface_total
                       ,period_name
          FROM apps.xxcus_ozf_sdw_vw
        UNION
        SELECT bu_nm
              ,0                        sdw_spend_total
              ,oracle_unprocessed_total
              ,0                        oracle_base_table_total
              ,0                        oracle_failed_interface_total
              ,period_name
          FROM apps.xxcus_ozf_receipt_vw
        UNION
        SELECT bu_nm
              ,0                       sdw_spend_total
              ,0                       oracle_unprocessed_total
              ,oracle_base_table_total
              ,0                       oracle_failed_interface_total
              ,period_name
          FROM apps.xxcus_ozf_resale_line_vw
        UNION
        SELECT bu_nm
              ,0                             sdw_spend_total
              ,0                             oracle_unprocessed_total
              ,0                             oracle_base_table_total
              ,oracle_failed_interface_total
              ,period_name

          FROM apps.xxcus_ozf_resale_line_int_vw
        )
 WHERE 1 = 1
 GROUP BY bu_nm, period_name
;
