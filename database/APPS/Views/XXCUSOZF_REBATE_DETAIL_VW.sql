
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUSOZF_REBATE_DETAIL_VW" ("COLLECTOR", "MVID", "CUST_ID", "VENDOR_NAME", "AGREEMENT_YEAR", "TRANSACTION_TYPE", "INVOICE_TOTAL", "TRANSACTION_REMAINING_BALANCE", "GL_DATE", "AGREEMENT_NAME", "DAYS_OUTSTANDING", "INVOICE_DATE", "INVOICE_NUMBER", "REBATE_TYPE_ID", "AGING_LESS_THAN_30", "AGING_BETWEEN_31_60", "AGING_BETWEEN_61_90", "AGING_BETWEEN_91_120", "AGING_GREATER_THAN_120", "AGING_BUCKET") AS 
  SELECT distinct
ar.NAME collector
,MVID
,cust_id
,vendor_name
,agreement_year
,transaction_type
,invoice_total
,transaction_remaining_balance
,gl_date
,agreement_name
,round(days_outstanding, 0) days_outstanding
,invoice_date
,invoice_number
,rebate_type_id
,aging_less_than_30
,aging_between_31_60
,aging_between_61_90
,aging_between_91_120
,aging_greater_than_120
,(CASE WHEN aging_less_than_30 <> 0 THEN '0-30 DAYS' WHEN aging_between_31_60 <> 0 THEN '31-60 DAYS' WHEN aging_between_61_90 <> 0 THEN '61-90 DAYS' WHEN aging_between_91_120 <> 0 THEN '91-120 DAYS' WHEN aging_greater_than_120 <> 0 THEN '121+ DAYS' ELSE 'N/A' END) aging_bucket
FROM 
(
SELECT rcta.bill_to_customer_id cust_id
,hp1.party_name vendor_name
,REPLACE(hp1.party_number, '~MSTR',
'') mvid
,qlhv.attribute7 agreement_year
,rctt.name transaction_type
,apsa.amount_due_original invoice_total
,apsa.amount_due_remaining transaction_remaining_balance
,apsa.gl_date gl_date
,qlhv.description agreement_name
,SYSDATE - apsa.due_date days_outstanding
,rcta.trx_date invoice_date
,rcta.trx_number invoice_number
,decode(med.description
,'COOP'
,'COOP'
,'COOP_MIN'
,'COOP'
,'REBATE') rebate_type_id
,(CASE WHEN (SYSDATE - apsa.due_date) < 31 THEN apsa.amount_due_remaining ELSE 0 END) aging_less_than_30
,(CASE WHEN (SYSDATE - apsa.due_date) BETWEEN 31
AND 60 THEN apsa.amount_due_remaining ELSE 0 END) aging_between_31_60
,(CASE WHEN (SYSDATE - apsa.due_date) BETWEEN 61
AND 90 THEN apsa.amount_due_remaining ELSE 0 END) aging_between_61_90
,(CASE WHEN (SYSDATE - apsa.due_date) BETWEEN 91
AND 120 THEN apsa.amount_due_remaining ELSE 0 END) aging_between_91_120
,(CASE WHEN (SYSDATE - apsa.due_date) > 120 THEN apsa.amount_due_remaining ELSE 0 END) aging_greater_than_120
FROM hz_parties hp1
,hz_cust_accounts hca
,ra_customer_trx_all rcta
,ar_payment_schedules_all apsa
,qp_list_headers_vl qlhv
,ozf_claim_lines_all ocl
,ams_media_tl med
,ozf_offers oo
,ra_customer_trx_lines_all rctl
,ra_cust_trx_types_all rctt
WHERE 1 = 1
AND hca.party_id = hp1.party_id
AND rctt.cust_trx_type_id = rcta.cust_trx_type_id
AND oo.qp_list_header_id = qlhv.list_header_id
AND rctl.interface_line_attribute2 = to_char(ocl.claim_id)
AND ocl.activity_id = oo.qp_list_header_id
AND apsa.trx_number = rcta.trx_number
AND qlhv.attribute7 > = '2010'
AND oo.activity_media_id = med.media_id
AND rcta.customer_trx_id = rctl.customer_trx_id
AND rcta.bill_to_customer_id = hca.cust_account_id
AND apsa.class = 'INV'
AND HP1.ATTRIBUTE1 = 'HDS_MVID'
)
,HZ_CUSTOMER_PROFILES HCP,
             AR_COLLECTORS        AR
             WHERE 1=1
             AND cust_id = HCP.CUST_ACCOUNT_ID
            AND ar.collector_id = hcp.collector_id;
