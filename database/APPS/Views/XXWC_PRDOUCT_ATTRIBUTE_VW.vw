--
-- XXWC_PRDOUCT_ATTRIBUTE_VW  (View)
--

CREATE OR REPLACE FORCE VIEW APPS.XXWC_PRDOUCT_ATTRIBUTE_VW
(
   SEGMENT_CODE
  ,SEGMENT_MAPPING_COLUMN
  ,USER_SEGMENT_NAME
)
AS
   SELECT seg.Segment_Code, Segment_Mapping_Column, seg.User_Segment_Name
     FROM qp_segments_v seg, QP_PRC_CONTEXTS_V prc
    WHERE     prc.PRC_CONTEXT_CODE = 'ITEM'
          AND seg.Segment_Code LIKE '%ITEM%'
          AND seg.Prc_Context_ID = prc.PRC_Context_ID
          AND seg.seeded_flag = 'Y';


