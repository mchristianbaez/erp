
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUS_RECEIPT_PROCESSED_V" ("GEO_LOC_HIER_LOB_NM", "SRC_SYS_NM", "BU_NM", "BU_CD", "FISCAL_PER_ID", "VNDR_RECPT_UNQ_NBR", "PT_VNDR_ID", "PT_VNDR_CD", "SF_VNDR_ID", "SF_VNDR_CD", "REBT_VNDR_ID", "REBT_VNDR_CD", "REBT_VNDR_NM", "PROD_ID", "SKU_CD", "VNDR_PART_NBR", "SHP_TO_BRNCH_ID", "SHP_TO_BRNCH_CD", "SHP_TO_BRNCH_NM", "PO_NBR", "PO_CRT_DT", "RECPT_CRT_DT", "DROP_SHP_FLG", "PROD_UOM_CD", "ITEM_COST_AMT", "CURNC_FLG", "RECPT_QTY", "VNDR_SHP_QTY", "BUY_PKG_UNT_CNT", "AS_OF_DT", "REQUEST_ID", "STATUS_FLAG", "PROCESS_DATE", "RECEIPT", "CUST_ACCOUNT_ID", "PARTY_ID", "PARTY_NAME") AS 
  select GEO_LOC_HIER_LOB_NM,
SRC_SYS_NM,
BU_NM,
BU_CD,
FISCAL_PER_ID,
VNDR_RECPT_UNQ_NBR,
PT_VNDR_ID,
PT_VNDR_CD,
SF_VNDR_ID,
SF_VNDR_CD,
REBT_VNDR_ID,
REBT_VNDR_CD,
REBT_VNDR_NM,
PROD_ID,
SKU_CD,
VNDR_PART_NBR,
SHP_TO_BRNCH_ID,
SHP_TO_BRNCH_CD,
SHP_TO_BRNCH_NM,
PO_NBR,
PO_CRT_DT,
RECPT_CRT_DT,
DROP_SHP_FLG,
PROD_UOM_CD,
ITEM_COST_AMT,
CURNC_FLG,
RECPT_QTY,
VNDR_SHP_QTY,
BUY_PKG_UNT_CNT,
AS_OF_DT,
REQUEST_ID,
STATUS_FLAG,
PROCESS_DATE,
RECEIPT,
CUST_ACCOUNT_ID,
PARTY_ID,
PARTY_NAME from xxcus.xxcus_rebate_recpt_history_tbl
union all
select GEO_LOC_HIER_LOB_NM,
SRC_SYS_NM,
BU_NM,
BU_CD,
FISCAL_PER_ID,
VNDR_RECPT_UNQ_NBR,
PT_VNDR_ID,
PT_VNDR_CD,
SF_VNDR_ID,
SF_VNDR_CD,
REBT_VNDR_ID,
REBT_VNDR_CD,
REBT_VNDR_NM,
PROD_ID,
SKU_CD,
VNDR_PART_NBR,
SHP_TO_BRNCH_ID,
SHP_TO_BRNCH_CD,
SHP_TO_BRNCH_NM,
PO_NBR,
PO_CRT_DT,
RECPT_CRT_DT,
DROP_SHP_FLG,
PROD_UOM_CD,
ITEM_COST_AMT,
CURNC_FLG,
RECPT_QTY,
VNDR_SHP_QTY,
BUY_PKG_UNT_CNT,
AS_OF_DT,
REQUEST_ID,
STATUS_FLAG,
PROCESS_DATE,
RECEIPT,
CUST_ACCOUNT_ID,
PARTY_ID,
PARTY_NAME from xxcus.xxcus_rebate_receipt_curr_tbl c
 where c.status_flag = 'P'
;
