
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUSAPEX_P2P_COSTCENTER_VW" ("FLEX_VALUE", "DESCRIPTION", "ENABLED_FLAG") AS 
  SELECT v.flex_value, tl.description, enabled_flag
          FROM apps.fnd_flex_values        v
              ,applsys.fnd_flex_values_tl  tl
              ,applsys.fnd_flex_value_sets s
         WHERE flex_value_set_name = 'XXCUS_GL_COSTCENTER'
           AND v.flex_value_set_id = s.flex_value_set_id
           AND v.flex_value_id = tl.flex_value_id
           AND tl.language = 'US'
;
