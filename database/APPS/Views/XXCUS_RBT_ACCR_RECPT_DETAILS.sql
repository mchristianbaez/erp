
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUS_RBT_ACCR_RECPT_DETAILS" ("MVID", "LOB", "VENDOR_RESALE_LINE", "OFFER_NAME", "VENDOR_ACCRUALS", "OFFER_CODE", "PO_NUMBER", "ACTIVITY_TYPE", "PAY_TO_VENDOR_CODE", "BU_BRANCH_CODE", "LOCATION_SEGMENT", "PRODUCT", "RESALE_ORDER_DATE", "CREATION_DATE", "PURCHASES", "ACCRUALS") AS 
  select
      C.CUSTOMER_ATTRIBUTE2 MVID,
      ORL.BILL_TO_PARTY_NAME lob,
      C.PARTY_NAME VENDOR_RESALE_LINE,
      OFU.OFFER_NAME OFFER_NAME,
      OFU.OFU_MVID_NAME VENDOR_ACCRUALS,
      OFU.OFFER_CODE OFFER_CODE,
      OFU.PO_NUMBER PO_NUMBER,
      OFU.ACTIVITY_MEDIA_NAME ACTIVITY_TYPE,
      ORL.LINE_ATTRIBUTE1 PAY_TO_VENDOR_CODE,
      ORL.LINE_ATTRIBUTE6 BU_BRANCH_CODE,
      ORL.LINE_ATTRIBUTE12 LOCATION_SEGMENT,
      ORL.LINE_ATTRIBUTE11 PRODUCT,
      ORL.DATE_ORDERED RESALE_ORDER_DATE,
      TRUNC(ORL.CREATION_DATE) CREATION_DATE,
      SUM(NVL(SELLING_PRICE*QUANTITY,0)) PURCHASES,
      SUM(NVL(OFU.AMOUNT,0)) ACCRUALS
from 
      APPS.OZF_RESALE_LINES_ALL ORL,
      XXCUS.XXCUS_REBATE_CUSTOMERS C,
      (select 
           OBJECT_ID
          ,OFFER_NAME
          ,OFU_MVID_NAME
          ,OFFER_CODE
          ,PO_NUMBER
          ,ACTIVITY_MEDIA_NAME
          ,SUM(NVL(UTIL_ACCTD_AMOUNT,0) ) AMOUNT
        from 
            XXCUS.XXCUS_OZF_XLA_ACCRUALS_B
        where 1=1
          and OFFER_STATUS_CODE not  in ('TERMINATED')
        group by 
            OBJECT_ID
            ,OFFER_NAME
            ,OFU_MVID_NAME
            ,OFFER_CODE
            ,PO_NUMBER
            ,ACTIVITY_MEDIA_NAME ) OFU
where 1=1
      and OFU.OBJECT_ID(+) = ORL.RESALE_LINE_ID
      and C.CUSTOMER_ID = ORL.SOLD_FROM_CUST_ACCOUNT_ID 
group by 
      OFU.OFFER_NAME,
      C.CUSTOMER_ATTRIBUTE2,
      ORL.BILL_TO_PARTY_NAME,
      C.PARTY_NAME,
      OFU.OFU_MVID_NAME,
      OFU.OFFER_NAME,
      OFU.OFFER_CODE
      , OFU.PO_NUMBER
      , OFU.ACTIVITY_MEDIA_NAME
      , ORL.LINE_ATTRIBUTE1
      , ORL.LINE_ATTRIBUTE6
      , ORL.LINE_ATTRIBUTE12,
      ORL.LINE_ATTRIBUTE11,
      ORL.DATE_ORDERED,
      TRUNC(ORL.CREATION_DATE)

;
