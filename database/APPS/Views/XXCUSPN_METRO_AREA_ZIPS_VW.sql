
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUSPN_METRO_AREA_ZIPS_VW" ("TIMEZONE", "COUNTYNAME", "METROAREA", "LATITUDE", "LONGITUDE", "ZIPCODE") AS 
  (select timezone, countyname, replace(cbsaname,'"') Metroarea,  Latitude, Longitude, zipcode
from XXCUSPN_METRO_AREA_ZIPS_TBL
WHERE primaryrecord = 'P'
)
;
