
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUS_VEND_OFF_LOB" ("LOB", "MVID", "VENDOR", "BRANCH_LOCATION", "BRANCH_NAME", "REBATE_NAME", "REBATE_TYPE", "OFFER_TYPE", "OFFER_CODE", "PROGRAM_NAME", "RECEIPT_DATE", "PURCHASE_PRICE", "SELLING_PRICE", "QUANTITY", "REBATE_AMOUNT", "GL_AMOUNT", "ATTRIBUTE2", "ATTRIBUTE3", "ACTIVE_FLAG", "UTILIZATION_TYPE", "COMPONENT_TYPE", "OBJECT_ID", "COMPONENT_ID", "LIST_HEADER_ID", "SOLD_FROM_CUST_ACCOUNT_ID", "CUST_ACCOUNT_ID_HCA1", "PARTY_ID_HCA1", "PARTY_ID_HP1", "BILL_TO_CUST_ACCOUNT_ID", "CUST_ACCOUNT_ID_HCA2", "PARTY_ID_HCA2", "PARTY_ID_HP2", "FUND_ID_OFU", "FUND_ID_OFA", "MEDIA_ID", "ACTIVITY_MEDIA_ID", "QP_LIST_HEADER_ID", "ORIG_ORG_ID", "CUST_ACCOUNT_ID") AS 
  select hp2.party_name LOB,
       SUBSTR(hca1.account_number, 0, INSTR(hca1.account_number, '~')-1) MVID,
       hp1.party_name vendor,
       hca2.account_number branch_location,
       hca2.attribute3 branch_name,
       qlhv.description rebate_name,
       med.media_name rebate_type,
       oo.offer_type,
       oo.offer_code,
       ofa.short_name program_name,
       orl.date_ordered receipt_date,
       orl.purchase_price,
       orl.selling_price,
       orl.quantity,
       ofu.plan_curr_amount rebate_amount,
       ofu.acctd_amount gl_amount,
       trd.attribute2,
       trd.attribute3,
       qlhv.active_flag,
       ofu.utilization_type,
       ofu.component_type,
       ofu.object_id,
       ofu.component_id,
       qlhv.LIST_HEADER_ID,
       orl.sold_from_cust_account_id,
       hca1.cust_account_id cust_account_id_hca1 ,
       hca1.party_id party_id_hca1,
       hp1.party_id party_id_hp1,
       orl.bill_to_cust_account_id,
       hca2.cust_account_id cust_account_id_hca2,
       hca2.party_id party_id_hca2,
       hp2.party_id party_id_hp2,
       ofu.fund_id fund_id_ofu,
       ofa.fund_id fund_id_ofa,
       med.MEDIA_ID,
       oo.activity_media_id,
       oo.qp_list_header_id,
       qlhv.orig_org_id,
       trd.cust_account_id
from   ozf_funds_utilized_all_b ofu,
       ozf_resale_lines_all     orl,
       hz_cust_accounts         hca1,
       hz_parties               hp1,
       hz_cust_accounts         hca2,
       hz_parties               hp2,
       qp_list_headers_vl       qlhv,
       ozf_funds_all_tl         ofa,
       ozf_offers               oo,
       ams_media_vl             med,
       ozf_cust_trd_prfls_all   trd
where  ofu.utilization_type = 'ACCRUAL'
   and ofu.object_id = orl.resale_line_id
   and ofu.component_id = qlhv.LIST_HEADER_ID
   and orl.sold_from_cust_account_id = hca1.cust_account_id
   and hca1.party_id = hp1.party_id
   and orl.bill_to_cust_account_id = hca2.cust_account_id
   and hca2.party_id = hp2.party_id
   and ofu.fund_id = ofa.fund_id
   and med.MEDIA_ID = oo.activity_media_id
   and oo.qp_list_header_id = qlhv.LIST_HEADER_ID
   and trd.cust_account_id(+) = orl.sold_from_cust_account_id
UNION
  select hp2.party_name LOB,
       SUBSTR(hca1.account_number, 0, INSTR(hca1.account_number, '~')-1) MVID,
       hp1.party_name vendor,
       hca2.account_number branch_location,
       hca2.attribute3 branch_name,
       qlhv.description rebate_name,
       med.media_name rebate_type,
       oo.offer_type,
       oo.offer_code,
       ofa.short_name program_name,
       orl.date_ordered receipt_date,
       orl.purchase_price,
       orl.selling_price,
       orl.quantity,
       ofu.plan_curr_amount rebate_amount,
       ofu.acctd_amount gl_amount,
       trd.attribute2,
       trd.attribute3,
       qlhv.active_flag,
       ofu.utilization_type,
       ofu.component_type,
       ofu.object_id,
       ofu.component_id,
       qlhv.LIST_HEADER_ID,
       orl.sold_from_cust_account_id,
       hca1.cust_account_id cust_account_id_hca1 ,
       hca1.party_id party_id_hca1,
       hp1.party_id party_id_hp1,
       orl.bill_to_cust_account_id,
       hca2.cust_account_id cust_account_id_hca2,
       hca2.party_id party_id_hca2,
       hp2.party_id party_id_hp2,
       ofu.fund_id fund_id_ofu,
       ofa.fund_id fund_id_ofa,
       med.MEDIA_ID,
       oo.activity_media_id,
       oo.qp_list_header_id,
       qlhv.orig_org_id,
       trd.cust_account_id
from   ozf_funds_utilized_all_b ofu,
       ozf_resale_lines_all     orl,
       hz_cust_accounts         hca1,
       hz_parties               hp1,
       hz_cust_accounts         hca2,
       hz_parties               hp2,
       qp_list_headers_vl       qlhv,
       ozf_funds_all_tl         ofa,
       ozf_offers               oo,
       ams_media_vl             med,
       ozf_cust_trd_prfls_all   trd
where ofu.utilization_type = 'ADJUSTMENT'
   and ofu.object_id = orl.resale_line_id
   and ofu.component_id = qlhv.LIST_HEADER_ID
   and orl.sold_from_cust_account_id = hca1.cust_account_id
   and hca1.party_id = hp1.party_id
   and orl.bill_to_cust_account_id = hca2.cust_account_id
   and hca2.party_id = hp2.party_id
   and ofu.fund_id = ofa.fund_id
   and med.MEDIA_ID = oo.activity_media_id
   and oo.qp_list_header_id = qlhv.LIST_HEADER_ID
   and trd.cust_account_id(+) = orl.sold_from_cust_account_id
;
