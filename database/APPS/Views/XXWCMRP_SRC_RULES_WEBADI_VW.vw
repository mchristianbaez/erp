--
-- XXWCMRP_SRC_RULES_WEBADI_VW  (View)
--

CREATE OR REPLACE FORCE VIEW APPS.XXWCMRP_SRC_RULES_WEBADI_VW
(
   SORT_LEVEL
  ,SOURCING_RULE_NAME
  ,DESCRIPTION
)
AS
   SELECT 2 SORT_LEVEL, SOURCING_RULE_NAME, DESCRIPTION
     FROM MRP_SOURCING_RULES SR
    WHERE EXISTS
             (SELECT 'x'
                FROM MRP_SR_RECEIPT_ORG SRO
               WHERE     SR.sourcing_rule_id = SRO.sourcing_rule_id
                     AND TRUNC (SYSDATE) BETWEEN SRO.effective_date
                                             AND NVL (SRO.disable_date
                                                     ,SYSDATE + 1))
   UNION
   SELECT 1 SORT_LEVEL
         ,'ALL' SOURCING_RULE_NAME
         ,'ALL SOURCING RULES' DESCRIPTION
     FROM DUAL;


