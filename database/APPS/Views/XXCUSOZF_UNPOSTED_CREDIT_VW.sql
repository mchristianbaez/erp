
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUSOZF_UNPOSTED_CREDIT_VW" ("BUSINESS_UNIT", "PAYMENT_METHOD", "MASTER_VENDOR", "MVID", "PAYMENT_NUMBER", "PAYMENT_DATE", "PAYMENT_AMOUNT", "CURRENCY", "IMAGE_URL", "TIME_FRAME") AS 
  SELECT line_of_business business_unit,
       payment_method,
       party_name master_vendor,
       customer_number mvid,
       payment_number,
       payment_date,
       payment_amount,
       currency_code currency,
       image_url,
       payment_time_frame time_frame
  FROM xxcus.xxcusozf_rbt_payments_tbl rpt,
       hz_parties hp
 WHERE hp.party_number = rpt.customer_number
   AND payment_method = 'REBATE_CREDIT'
   AND rpt.status = 'N'
;
