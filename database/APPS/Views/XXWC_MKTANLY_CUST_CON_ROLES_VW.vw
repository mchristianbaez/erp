CREATE OR REPLACE VIEW APPS.XXWC_MKTANLY_CUST_CON_ROLES_VW AS 
/********************************************************************************************************************************
--   NAME:       apps.XXWC_MKTANLY_CUST_CON_ROLES_VW
-- REVISIONS:
--   Ver        Date        Author           Description
--   ---------  ----------  ---------------  ------------------------------------
--   1.0        09/05/2018   P.Vamshidhar      TMS#20180907-00001 -  Building Customer Outbound MV's for Marketing Analytics
**********************************************************************************************************************************/
SELECT DISTINCT 
  hca.account_number "Account_Number" ,
  SUB.PARTY_ID contact_party_id,
  hrr.responsibility_type Role,
  hrr.last_update_date Last_Update_Date,
  hrr.creation_date Created_Date
FROM apps.hz_cust_accounts hca ,
  apps.hz_parties obj ,
  apps.hz_relationships rel ,
  apps.hz_contact_points hcp ,
  apps.hz_parties sub,
  apps.hz_cust_account_roles hcar,
  APPS.HZ_ROLE_RESPONSIBILITY HRR
WHERE hca.party_id        = rel.object_id
AND hca.party_id          = obj.party_id
AND rel.subject_id        = sub.party_id
AND rel.relationship_type = 'CONTACT'
AND rel.directional_flag  = 'F'
AND rel.party_id          = hcp.owner_table_id
AND hcp.owner_table_name  = 'HZ_PARTIES'
AND rel.party_id              = hcar.party_id
AND hca.cust_account_id       = hcar.cust_account_id
AND hcar.cust_account_role_id = HRR.cust_account_role_id
ORDER BY hca.account_number
/