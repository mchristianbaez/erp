/*************************************************************************
 $Header XXWC_PO_RTV_HEADERS_VW $
  Module Name: XXWC_PO_RTV_HEADERS_VW

  PURPOSE: Return to Vendor Transactions Header Details for the Form

  REVISIONS:
  Ver        Date        Author                Description
  ---------  ----------  ------------------    ----------------
  1.0        04/10/2014  Manjula Chellappan    Initial Version
  1.1        09/29/2014  Manjula chellappan    TMS # 20141014-00200
					       Update RTV form query to restrict 
					       duplicate contact 
  1.2       09/30/2014   Pattabhi Avula        TMS # 20141001-00057
											   Multi Org Changes done
  1.3       12/02/2014   Pattabhi Avula 	   TMS # 20141001-00057 
											   ap_supplier_sites_all table added
											   by Manjula in sub-query to restrict 
											   duplicate records, after this change
											   I have made the multi org changes
**************************************************************************/

CREATE OR REPLACE FORCE VIEW APPS.XXWC_PO_RTV_HEADERS_VW
(
   RTV_HEADER_ID,
   SHIP_FROM_BRANCH_ID,
   SHIP_FROM_BRANCH,
   BUYER_ID,
   BUYER_NAME,
   SUPPLIER_ID,
   SUPPLIER_NAME,
   SUPPLIER_SITE_ID,
   SUPPLIER_SITE,
   SUPPLIER_CONTACT_ID,
   SUPPLIER_CONTACT,
   PO_HEADER_ID,
   PO_NUMBER,
   RETURN_NUMBER,
   WC_RETURN_NUMBER,
   SUPPLIER_ADDRESS,
   NOTES,
   RESTOCKING_FEE,
   RTV_STATUS_CODE,
   RTV_DATE,
   ERROR_MESSAGE,
   CREATED_BY,
   CREATION_DATE,
   LAST_UPDATED_BY,
   LAST_UPDATE_DATE,
   LAST_UPDATE_LOGIN,
   ATTRIBUTE1,
   ATTRIBUTE2,
   ATTRIBUTE3,
   ATTRIBUTE4,
   ATTRIBUTE5,
   ATTRIBUTE6,
   ATTRIBUTE7,
   ATTRIBUTE8,
   ATTRIBUTE9,
   ATTRIBUTE10,
   ATTRIBUTE11,
   ATTRIBUTE12,
   ATTRIBUTE13,
   ATTRIBUTE14,
   ATTRIBUTE15
)
AS
   SELECT a.rtv_header_id,
          a.ship_from_branch_id,
          d.organization_name,
          a.buyer_id,
          e.description,
          a.supplier_id,
          b.vendor_name,
          a.supplier_site_id,
          c.vendor_site_code,
          a.supplier_contact_id,
          (SELECT DECODE (pvc.first_name,
                          NULL, pvc.last_name,
                          pvc.last_name || ', ' || pvc.first_name)
             FROM apps.po_vendor_contacts pvc
      --Added by Manjula on 29-Sep-14 to restrict duplicate contact TMS # 20141014-00200 
                  , apps.ap_supplier_sites assa -- V 1.3 Added by Manjula
            WHERE     vendor_contact_id = a.supplier_contact_id
                  AND pvc.vendor_site_id = assa.vendor_site_id
                  AND (DECODE (pvc.first_name,
                               NULL, pvc.last_name,
                               pvc.last_name || ', ' || pvc.first_name)) =
                         assa.vendor_site_code),
          a.po_header_id,
          (SELECT segment1
             FROM apps.po_headers
            WHERE po_header_id = a.po_header_id),
          a.return_number,
          a.wc_return_number,
          a.supplier_address,
          -- TMS# 20140619-00123 Added by Manjula on 20-Jun-14
          notes,
          a.restocking_fee,
          a.rtv_status_code,
          --TO_DATE(DECODE(rtv_status_code,'Pending',a.creation_date,'Confirmed',a.last_update_date)) rtv_date,
          a.creation_date rtv_date,
          a.error_message,
          a.created_by,
          a.creation_date,
          a.last_updated_by,
          a.last_update_date,
          a.last_update_login,
          a.attribute1,
          a.attribute2,
          a.attribute3,
          a.attribute4,
          a.attribute5,
          a.attribute6,
          a.attribute7,
          a.attribute8,
          a.attribute9,
          a.attribute10,
          a.attribute11,
          a.attribute12,
          a.attribute13,
          a.attribute14,
          a.attribute15
     FROM apps.xxwc_po_rtv_headers_tbl a,
          apps.ap_suppliers b,
          apps.ap_supplier_sites c,
          apps.org_organization_definitions d,
          apps.fnd_user e
    WHERE     1 = 1
          AND a.supplier_id = b.vendor_id
          AND a.supplier_site_id = c.vendor_site_id(+)
          AND a.ship_from_branch_id = d.organization_id
          AND a.buyer_id = e.user_id;
/