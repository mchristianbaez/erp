
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUS_BANK_BALANCES" ("CONTROL_END_BALANCE", "STATEMENT_DATE", "BANK_ACCOUNT_NAME", "BANK_ACCOUNT_NUM", "CURRENCY_CODE", "SEGMENT1", "SEGMENT2", "SEGMENT3", "SEGMENT4", "SEGMENT5", "SEGMENT6", "SEGMENT7") AS 
  select hd.control_end_balance,
       hd.statement_date,
       ba.bank_account_name,
       ba.bank_account_num,
       ba.currency_code,
       gl.segment1,
       gl.segment2,
       gl.segment3,
       gl.segment4,
       gl.segment5,
       gl.segment6,
       gl.segment7

       

from ce.ce_statement_headers hd,
     ce.ce_bank_accounts ba,
     gl.gl_code_combinations gl

where hd.bank_account_id = ba.bank_account_id and
      ba.asset_code_combination_id = gl.code_combination_id

;
