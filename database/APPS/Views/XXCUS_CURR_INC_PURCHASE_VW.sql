
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUS_CURR_INC_PURCHASE_VW" ("CURRENT_YTD_REBATE_INCOME", "CURRENT_YTD_COOP_INCOME", "CURRENT_YTD_PURCHASES", "PLOB_NAME", "PLOB_ID", "PMVID", "PMVID_NAME", "MVID", "LOB_ID", "MVID_NAME", "LOB_NAME", "CALENDAR_YEAR", "PCALENDAR_YEAR", "PROGRAM_NAME") AS 
  SELECT r.Current_ytd_rebate_income,
    r.Current_ytd_coop_income,
    pm.Current_YTD_Purchases,
    pm.lob_name plob_name,
    pm.lob_id plob_id,
    pm.mvid pmvid,
    pm.mvid_name pmvid_name,
    r.mvid,
    r.lob_id,
    r.mvid_name,
    r.lob_name,
    r.calendar_year,
    pm.calendar_year pcalendar_year,
    r.program_name
  FROM
    (SELECT SUM(Current_rebate_ytd_income) Current_ytd_rebate_income,
      SUM(Current_coop_ytd_income)Current_ytd_coop_income,
      reb.mvid,
      reb.lob_id,
      reb.mvid_name,
      reb.lob_name,
      reb.calendar_year,
      reb.program_name
    FROM
      (SELECT (
        CASE
          WHEN curr_rebate_type_id NOT IN ('COOP')
          THEN rebinc.Current_ytd_income
          ELSE 0
        END)Current_rebate_ytd_income,
        (
        CASE
          WHEN curr_rebate_type_id IN ('COOP')
          THEN rebinc.Current_ytd_income
          ELSE 0
        END)Current_coop_ytd_income,
        rebinc.mvid,
        rebinc.mvid_name,
        rebinc.lob_name,
        rebinc.lob_id,
        rebinc.calendar_year,
        rebinc.program_name
      FROM
        (SELECT SUM(accr1.accrued_amount) Current_ytd_income,
          DECODE(accr1.rebate_type_id, 'COOP', 'COOP', 'COOP_MIN', 'COOP', 'REBATE') curr_rebate_type_id,
          qlhv.attribute7 calendar_year,
          accr1.lob_id,
          accr1.mvid,
          hzp.party_name mvid_name,
          hzp1.party_name LOB_Name,
          ofa.description program_name
        FROM xxcusozf_accruals_mv accr1,
          Ozf.Ozf_Funds_All_Tl Ofa,
          Ozf.Ozf_Act_Budgets oab,
          ozf_offers oo,
          qp_list_headers_vl qlhv,
          hz_cust_accounts hzca,
          hz_parties hzp,
          hz_parties hzp1
        WHERE 1                   = 1
        AND accr1.grp_mvid        = 0
        AND accr1.grp_branch      = 1
        AND accr1.grp_adj_type    = 1
        AND accr1.grp_rebate_type = 0
        AND accr1.grp_plan_id     = 0
        AND accr1.grp_qtr         = 1
        AND accr1.grp_year        = 1
        AND accr1.grp_lob         = 0
        AND accr1.grp_cal_year    = 1
        AND accr1.plan_id = qlhv.list_header_id
        AND accr1.grp_period = 1
        AND hzp1.party_id        = accr1.lob_id
        AND hzp1.attribute1      = 'HDS_LOB'
        AND accr1.mvid           = hzca.cust_account_id
        AND hzp.party_id         = hzca.party_id
        AND oo.qp_list_header_id = accr1.plan_id
        AND oo.qp_list_header_id = oab.act_budget_used_by_id
        AND oab.arc_act_budget_used_by = 'OFFR'
        AND oab.budget_source_type = 'FUND'
        AND oab.transfer_type = 'REQUEST'
        AND oab.budget_source_id = ofa.fund_id
      --AND oo.budget_source_id  = ofa.fund_id
        GROUP BY qlhv.attribute7,
          accr1.mvid,
          hzp.party_name,
          hzp1.party_name,
          accr1.lob_id,
          ofa.description,
          accr1.rebate_type_id
        ) rebinc
      ) reb
    GROUP BY reb.mvid,
      reb.mvid_name,
      reb.lob_name,
      reb.lob_id,
      reb.program_name,
      reb.calendar_year
    ) r,
    (SELECT SUM(pm1.total_purchases) Current_YTD_Purchases,
      pm1.calendar_year calendar_year,
      pm1.mvid,
      pm1.lob_id,
      hp.party_name Lob_Name,
      hp1.party_name Mvid_Name
    FROM XXCUSOZF_PURCHASES_MV pm1,
      hz_parties hp,
      hz_parties hp1,
      hz_cust_accounts hca
    WHERE 1                	=1
    AND pm1.grp_mvid       	=0
    AND pm1.grp_branch     	=1
    AND pm1.grp_cal_year   	=0
    AND pm1.grp_period     	=1
    AND pm1.grp_cal_period 	=1
    AND pm1.grp_qtr    		=1
    AND pm1.grp_year   		=1
    AND pm1.grp_lob    		=0
    AND hp.party_id    		=pm1.lob_id
    AND pm1.mvid       		=hca.cust_account_id
    AND hca.party_id   		=hp1.party_id
    AND hp1.attribute1 		='HDS_MVID'
    AND pm1.lob_id     		=hp.party_id
    AND hp.attribute1  		='HDS_LOB'
    GROUP BY pm1.calendar_year,
      	     pm1.mvid,
      	     hp.party_name,
      	     hp1.party_name,
      	     pm1.lob_id
    ) pm
  WHERE 1                =1
  AND r.mvid(+)          = pm.mvid
  AND r.calendar_year(+) = pm.calendar_year
  AND r.lob_id(+)        = pm.lob_id
;
