
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUSTM_REBATE_VNDR_V" ("REBT_VNDR_CD") AS 
  select hca.attribute2
  from ar.hz_parties               p,
       ar.hz_cust_accounts         hca,
       apps.xxcus_cust_prod_rebt_v v
 where p.attribute1 = 'HDS_MVID'
   and p.party_id = hca.party_id
   and hca.attribute1 = 'HDS_MVID'
   and hca.cust_account_id = v.cust_acct_id
   union
   select distinct rebt_vndr_cd
   from hdsoracle.hds_rebate_classify@apxprd_lnk;
