/* Formatted on 1/30/2013 12:07:26 PM (QP5 v5.115.810.9015) */
--
-- XXWC_JTF_SALESREPS_EXT_VW  (View)
--

CREATE OR REPLACE FORCE VIEW apps.xxwc_jtf_salesreps_ext_vw (
   operating_unit_id
 , interface_date
 , salesrep_data
)
AS
   SELECT	operating_unit_id
		  , SYSDATE interface_date
		  , (	business_unit
			 || '|'
			 || source_system
			 || '|'
			 || salesrep_number
			 || '|'
			 || sales_rep_name
			 || '|'
			 || sales_rep_first_name
			 || '|'
			 || sales_rep_last_name
			 || '|'
			 || manager_salesrep_id
			 || '|'
			 || sales_region_code
			 || '|'
			 || sales_region_name
			 || '|'
			 || sales_region_manager
			 || '|'
			 || sales_district_code
			 || '|'
			 || sales_district_name
			 || '|'
			 || sales_district_manager
			 || '|'
			 || sales_area_code
			 || '|'
			 || sales_area_name
			 || '|'
			 || sales_area_manager
			 || '|'
			 || sales_territory_code
			 || '|'
			 || sales_territory_name
			 || '|'
			 || sales_territory_manager
			 || '|'
             -- 01/30/13 CG: TMS 20130122-01578 Retrieve employees branch location,
             -- based on employee assignment location or FRU Location in DFF
			 -- || branch_location
             || NVL(papf_assigment_loc, FRU_Location)
             -- 01/30/13 CG
			 || '|'
			 || ntl_acct_sales_rep_flag
			 || '|'
			 || resource_id
			 || '|'
			 || TO_CHAR (sales_rep_creation_date, 'MM/DD/YYYY')
			 || '|'
			 || TO_CHAR (sales_rep_last_update_date, 'MM/DD/YYYY'))
			   salesrep_data
	 FROM	xxwc_jtf_salesreps_vw;


