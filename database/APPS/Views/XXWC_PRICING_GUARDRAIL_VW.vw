--
-- XXWC_PRICING_GUARDRAIL_VW  (View)
--

CREATE OR REPLACE FORCE VIEW APPS.XXWC_PRICING_GUARDRAIL_VW
(
   PRICING_GUARDRAIL_ID
  ,PRICE_ZONE
  ,CATEGORY
  ,ITEM_CATEOGRY_ID
  ,ITEM_NUMBER
  ,INVENTORY_ITEM_ID
  ,MIN_MARGIN
  ,MAX_DISCOUNT
  ,START_DATE
  ,END_DATE
)
AS
   SELECT pgr.PRICING_GUARDRAIL_ID
         ,pgr.PRICE_ZONE
         ,mcc.CATEGORY_CONCAT_SEGMENTS Category
         ,pgr.Item_Category_ID Item_Cateogry_ID
         ,msi.segment1 Item_Number
         ,msi.Inventory_Item_ID
         ,MIN_MARGIN
         ,MAX_DISCOUNT
         ,START_DATE
         ,END_DATE
     FROM XXWC_OM_PRICING_GUARDRAIL pgr
         ,mtl_System_Items msi
         ,MTL_CATEGORY_SET_VALID_CATS_V mcc
    WHERE     pgr.Inventory_Item_ID = msi.Inventory_item_ID(+)
          AND msi.Organization_ID(+) =
                 FND_Profile.VALUE ('XXWC_ITEM_MASTER_ORG')
          AND pgr.Item_Category_Id = mcc.Category_ID(+)
          AND mcc.Category_set_ID(+) =
                 FND_PROFILE.VALUE ('XXWC_PARTS_ON_FLY_CATEGORY_SET')
          AND pgr.Status = 'CURRENT';


