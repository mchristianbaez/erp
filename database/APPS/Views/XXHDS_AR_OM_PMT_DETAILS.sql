
  CREATE OR REPLACE FORCE VIEW "APPS"."XXHDS_AR_OM_PMT_DETAILS" ("HEADER_ID", "WC_BRANCH", "ORDER_NUMBER", "ORDER_TYPE", "PAYMENT_NUMBER", "PAYMENT_TYPE_CODE", "PREPAID_AMOUNT", "CHECK_NUMBER", "CASH_RECEIPT_ID", "RECEIVABLE_APPLICATION_ID", "CASH_RECEIPT_HISTORY_ID", "ARD_LINE_ID", "ARD_SOURCE_TABLE", "RECEIVABLES_TRX_ID", "ACTIVITY_TYPE", "PAYMENT_SET_ID", "PAYMENT_LEVEL_CODE", "PAYMENT_PERCENTAGE", "CC_BRAND", "IDENTIFYING_NUMBER", "IDENTIFYING_NAME", "INSTRUMENT_TYPE", "OE_PMTS_TRXN_EXTN_ID", "CASH_RECEIPT_PMT_TRXN_EXTN_ID") AS 
  SELECT a.application_ref_id header_id,
          o.organization_code wc_branch,
          a.application_ref_num order_number,
          t.name order_type,
          p.payment_number,
          p.payment_type_code,
          p.prepaid_amount,
          p.check_number,
          a.cash_receipt_id,
          a.receivable_application_id,
          a.cash_receipt_history_id,
          ard.line_id ard_line_id,
          ard.source_table ard_source_table,
          a.receivables_trx_id,
          r.name activity_type,
          a.payment_set_id,
          p.payment_level_code,
          p.payment_percentage,
          TO_CHAR (NULL) CC_BRAND,
          TO_CHAR (NULL) IDENTIFYING_NUMBER,
          TO_CHAR (NULL) IDENTIFYING_NAME,
          TO_CHAR (NULL) INSTRUMENT_TYPE,
          p.trxn_extension_id oe_pmts_trxn_extn_id,
          TO_NUMBER (NULL) cash_receipt_pmt_trxn_extn_id
     FROM ar_receivable_applications_all a,
          ar_distributions_all ard,
          oe_order_headers_all h,
          oe_payments p,
          oe_transaction_types_v t,
          org_organization_definitions o,
          ar_receivables_trx r
    WHERE     1 = 1
          AND a.cash_receipt_history_id = ard.source_id
          AND ard.source_table = 'CRH'
          AND a.payment_set_id IS NOT NULL
          AND a.application_ref_type = 'OM'
          AND a.receivables_trx_id IS NOT NULL
          AND r.receivables_trx_id = a.receivables_trx_id
          AND p.header_id = TO_NUMBER (a.application_ref_id)
          AND p.payment_set_id = a.payment_set_id
          AND p.prepaid_amount = a.amount_applied
          AND p.payment_type_code IN ('CASH', 'CHECK')
          AND h.header_id = p.header_id
          AND t.transaction_type_id(+) = h.order_type_id
          AND o.organization_id(+) = h.ship_from_org_id
   --and ard.line_id =13042800 --use xdl_src_dist_id_num1
   --and a.cash_receipt_id =727632 --745057
   UNION
   SELECT a.application_ref_id,
          o.organization_code,
          a.application_ref_num,
          t.name order_type,
          p.payment_number,
          p.payment_type_code,
          p.prepaid_amount,
          p.check_number,
          a.cash_receipt_id,
          a.receivable_application_id,
          a.cash_receipt_history_id,
          ard.line_id ard_line_id,
          ard.source_table,
          a.receivables_trx_id,
          r.name,
          a.payment_set_id,
          p.payment_level_code,
          p.payment_percentage,
          x.card_issuer_code,
          x.card_number cc_id_number,
          x.card_holder_name cc_id_name,
          x.authtxn_instrtype,
          p.trxn_extension_id oe_pmts_trxn_extn_id,
          x.trxn_extension_id cash_receipt_pmt_trxn_extn_id
     FROM ar_receivable_applications a,
          ar_cash_receipts b,
          ar_distributions_all ard,
          oe_order_headers_all h,
          oe_payments p,
          apps.xxhds_iby_trxn_extensions_v x,
          oe_transaction_types_v t,
          org_organization_definitions o,
          ar_receivables_trx r
    WHERE     1 = 1
          --and a.display ='Y'
          AND a.cash_receipt_history_id = ard.source_id
          AND b.cash_receipt_id = a.cash_receipt_id
          AND ard.source_table = 'CRH'
          AND a.payment_set_id IS NOT NULL
          AND a.application_ref_type = 'OM'
          AND a.receivables_trx_id IS NOT NULL
          AND r.receivables_trx_id = a.receivables_trx_id
          AND p.header_id = TO_NUMBER (a.application_ref_id)
          AND p.payment_set_id = a.payment_set_id
          --and p.prepaid_amount =a.amount_applied
          AND p.payment_type_code = 'CREDIT_CARD'
          AND x.trxn_extension_id = b.payment_trxn_extension_id
          AND h.header_id = p.header_id
          AND t.transaction_type_id(+) = h.order_type_id
          AND o.organization_id(+) = h.ship_from_org_id;
