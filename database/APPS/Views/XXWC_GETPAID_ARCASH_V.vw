--
-- XXWC_GETPAID_ARCASH_V  (View)
--

CREATE OR REPLACE FORCE VIEW APPS.XXWC_GETPAID_ARCASH_V
(
   REC_LINE
  ,ORG_ID
)
AS
   SELECT    RPAD (NVL (hca.account_number, ' '), 20, ' ')           -- CUSTNO
          || NVL (
                (SELECT    RPAD (NVL (rcta.trx_number, ' '), 20, ' ') -- INVNO
                        || RPAD (' ', 30, ' ')                     -- BANK_IND
                        || RPAD (
                              NVL (
                                 SUBSTR (rba.name, INSTR (rba.name, '_') + 1)
                                ,' ')
                             ,20
                             ,' ')                                 -- BATCH_NO
                   FROM apps.ra_customer_trx rcta
                       ,apps.ar_receivable_applications araa
                       ,apps.ra_batches rba
                  WHERE     araa.cash_receipt_id = apsa.cash_receipt_id
                        AND araa.applied_customer_trx_id =
                               rcta.customer_trx_id
                        AND rba.batch_id = rcta.batch_id
                        AND acra.org_id = araa.org_id
                        AND rcta.org_id = araa.org_id
                        AND ROWNUM = 1)
               ,RPAD ('0', 50, ' ') || RPAD ('0', 20, ' '))
          || RPAD (NVL (acra.CURRENCY_CODE, ' '), 10, ' ')         -- CURRCODE
          || RPAD (NVL (acra.receipt_number, ' '), 20, ' ')     -- RECEIPT_NUM
          || LPAD (NVL (TO_CHAR (acra.amount, '9999999990.99'), ' ')
                  ,20
                  ,' ')                                         -- RECEIPT_AMT
          || RPAD (NVL (TO_CHAR (acra.deposit_date, 'MMDDYYYY'), ' ')
                  ,8
                  ,' ')                                        -- DEPOSIT_DATE
          || RPAD ('1', 20, ' ')                              -- DEPOSIT_DISCR
          || NVL (
                (SELECT LPAD (
                           NVL (
                              TO_CHAR (SUM (araa.amount_applied)
                                      ,'9999999990.99')
                             ,' ')
                          ,20
                          ,' ')                                     -- APP_AMT
                   FROM apps.ar_receivable_applications araa
                  WHERE     araa.cash_receipt_id = apsa.cash_receipt_id
                        AND acra.org_id = araa.org_id)
               ,LPAD ('0', 20, ' '))
          || NVL (
                (SELECT RPAD (
                           NVL (TO_CHAR (MIN (araa.apply_date), 'MMDDYYYY')
                               ,' ')
                          ,8
                          ,' ')                                    -- APP_DATE
                   FROM apps.ar_receivable_applications araa
                  WHERE     araa.cash_receipt_id = apsa.cash_receipt_id
                        AND acra.org_id = araa.org_id)
               ,RPAD ('01010001', 8, ' '))
          || RPAD ('0', 2, ' ')                                   -- OPERATION
          || LPAD (' ', 10, ' ')                              -- GA_JOURNAL_ID
          || RPAD (NVL (acra.TYPE, ' '), 1, ' ')                   -- TYPE_IND
             REC_LINE
         ,acra.ORG_ID
     FROM apps.ar_cash_receipts acra
         ,apps.hz_cust_accounts hca
         ,apps.ar_payment_schedules apsa
         ,ar_receipt_methods arm
    WHERE     1 = 1
          AND acra.TYPE = 'CASH'
          AND acra.cash_receipt_id = apsa.cash_receipt_id
          AND hca.cust_account_id = apsa.customer_id
          AND acra.receipt_method_id = arm.receipt_method_id
          AND arm.name != 'WC PRISM Lockbox'
          AND apsa.amount_due_remaining <> 0
   UNION
   --------------------------------------------------------------------
   -- Cash details for - "WC PRISM Lockbox" Receipt Method
   --------------------------------------------------------------------
   SELECT    RPAD (NVL (hca.account_number, ' '), 20, ' ')           -- CUSTNO
          || NVL (
                (SELECT    RPAD (NVL (rcta.trx_number || '-P', ' '), 20, ' ') -- INVNO
                        || RPAD (' ', 30, ' ')                     -- BANK_IND
                        || RPAD (
                              NVL (
                                 SUBSTR (rba.name, INSTR (rba.name, '_') + 1)
                                ,' ')
                             ,20
                             ,' ')                                 -- BATCH_NO
                   FROM apps.ra_customer_trx rcta
                       ,apps.ar_receivable_applications araa
                       ,apps.ra_batches rba
                  WHERE     araa.cash_receipt_id = apsa.cash_receipt_id
                        AND araa.applied_customer_trx_id =
                               rcta.customer_trx_id
                        AND rba.batch_id = rcta.batch_id
                        AND acra.org_id = araa.org_id
                        AND rcta.org_id = araa.org_id
                        AND ROWNUM = 1)
               ,RPAD ('0', 50, ' ') || RPAD ('0', 20, ' '))
          || RPAD (NVL (acra.CURRENCY_CODE, ' '), 10, ' ')         -- CURRCODE
          || RPAD (NVL (acra.receipt_number, ' '), 20, ' ')     -- RECEIPT_NUM
          || LPAD (NVL (TO_CHAR (acra.amount, '9999999990.99'), ' ')
                  ,20
                  ,' ')                                         -- RECEIPT_AMT
          || RPAD (NVL (TO_CHAR (acra.deposit_date, 'MMDDYYYY'), ' ')
                  ,8
                  ,' ')                                        -- DEPOSIT_DATE
          || RPAD ('1', 20, ' ')                              -- DEPOSIT_DISCR
          || NVL (
                (SELECT LPAD (
                           NVL (
                              TO_CHAR (SUM (araa.amount_applied)
                                      ,'9999999990.99')
                             ,' ')
                          ,20
                          ,' ')                                     -- APP_AMT
                   FROM apps.ar_receivable_applications araa
                  WHERE     araa.cash_receipt_id = apsa.cash_receipt_id
                        AND acra.org_id = araa.org_id)
               ,LPAD ('0', 20, ' '))
          || NVL (
                (SELECT RPAD (
                           NVL (TO_CHAR (MIN (araa.apply_date), 'MMDDYYYY')
                               ,' ')
                          ,8
                          ,' ')                                    -- APP_DATE
                   FROM apps.ar_receivable_applications araa
                  WHERE     araa.cash_receipt_id = apsa.cash_receipt_id
                        AND acra.org_id = araa.org_id)
               ,RPAD ('01010001', 8, ' '))
          || RPAD ('0', 2, ' ')                                   -- OPERATION
          || LPAD (' ', 10, ' ')                              -- GA_JOURNAL_ID
          || RPAD (NVL (acra.TYPE, ' '), 1, ' ')                   -- TYPE_IND
             REC_LINE
         ,acra.ORG_ID
     FROM apps.ar_cash_receipts acra
         ,apps.hz_cust_accounts hca
         ,apps.ar_payment_schedules apsa
         ,ar_receipt_methods arm
    WHERE     1 = 1
          AND acra.TYPE = 'CASH'
          AND acra.cash_receipt_id = apsa.cash_receipt_id
          AND hca.cust_account_id = apsa.customer_id
          AND acra.receipt_method_id = arm.receipt_method_id
          AND arm.name = 'WC PRISM Lockbox'
          AND apsa.amount_due_remaining <> 0;


