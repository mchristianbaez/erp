/* Formatted on 2012/08/31 09:41 (Formatter Plus v4.8.8) */
CREATE OR REPLACE FORCE VIEW apps.xxwc_gl_natural_accts_vw (business_unit,
                                                            source_system,
                                                            operating_unit_id,
                                                            operating_unit_name,
                                                            ledger_name,
                                                            ledger_category_code,
                                                            chart_of_accounts_id,
                                                            application_id,
                                                            id_flex_code,
                                                            application_column_name,
                                                            segment_name,
                                                            segment_num,
                                                            flex_value_set_id,
                                                            flex_value_set_name,
                                                            flex_value_id,
                                                            segment_value,
                                                            khalix_account,
                                                            description,
                                                            enabled_flag,
                                                            ledger_id,
                                                            ledger_currency,
                                                            business_unit_id,
                                                            business_unit_desc
                                                           )
AS
   SELECT   'WHITECAP' business_unit, 'Oracle EBS' source_system,
            hou.organization_id org_id, hou.NAME operating_unit_name,
            gl.NAME ledger_name, gl.ledger_category_code,
            gl.chart_of_accounts_id, seg.application_id, seg.id_flex_code,
            seg.application_column_name, seg.segment_name, seg.segment_num,
            seg.flex_value_set_id, ffvs.flex_value_set_name,
            ffv.flex_value_id, ffv.flex_value segment_value,
            'A' || ffv.flex_value khalix_account, ffvt.description,
            ffv.enabled_flag, ffv.attribute1 ledger_id,
            ffv.attribute2 ledger_currency, ffv.attribute3 business_unit_id,
            ffv.attribute4 business_unit_desc
       FROM hr_operating_units hou,
            gl_ledgers gl,
            fnd_id_flex_segments seg,
            fnd_flex_value_sets ffvs,
            fnd_flex_values ffv,
            fnd_flex_values_tl ffvt
      WHERE hou.set_of_books_id = TO_CHAR (gl.ledger_id)
        AND gl.chart_of_accounts_id = seg.id_flex_num
        AND seg.id_flex_code = 'GL#'
        AND seg.application_id = 101
        AND seg.flex_value_set_id = ffvs.flex_value_set_id
        AND ffvs.flex_value_set_id = ffv.flex_value_set_id
        AND ffv.flex_value_id = ffvt.flex_value_id
        AND ffvt.LANGUAGE = 'US'
   ORDER BY hou.organization_id, seg.segment_num, ffvs.flex_value_set_name;


CREATE PUBLIC SYNONYM XXWC_GL_NATURAL_ACCTS_VW FOR APPS.XXWC_GL_NATURAL_ACCTS_VW;


