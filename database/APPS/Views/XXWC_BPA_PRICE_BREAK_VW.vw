CREATE OR REPLACE FORCE VIEW APPS.XXWC_BPA_PRICE_BREAK_VW
(
   ORG_ID
 , BPA
 , VENDOR_NAME
 , LINE_NUM
 , ITEM_NUMBER
 , DESCRIPTION
 , SUPPLIER_ITEM
 , UNIT_PRICE
 , SHIPMENT_NUM
 , SHIP_TO_ORG
 , QUANTITY
 , PRICE_OVERRIDE
 , START_DATE
 , END_DATE
 , NOT_TO_EXCEED_PRICE
 , ALLOW_PRICE_OVERRIDE_FLAG
 , CASCADE_FLAG
)
AS
   SELECT PH.org_id
        , PH.segment1 BPA
        , AP.vendor_name
        , POL.line_num
        , POL.item_number
        , POL.description
        , POL.supplier_item
        , POL.unit_price
        , POL.shipment_num
        , POL.ship_to_org
        , POL.quantity
        , POL.price_override
        , POL.start_date
        , POL.end_date
        , POL.NOT_TO_EXCEED_PRICE
        , POL.ALLOW_PRICE_OVERRIDE_FLAG
        , 'Y'
     FROM apps.PO_HEADERS PH, XXWC_BPA_PO_LINES_VW pol, AP_SUPPLIERS AP
    WHERE     PH.po_header_id = POL.po_header_id(+)
          AND ph.type_lookup_code = 'BLANKET'
          AND PH.vendor_id = AP.vendor_id;
