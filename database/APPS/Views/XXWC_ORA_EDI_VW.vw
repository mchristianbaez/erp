/* Formatted on 10/1/2014 10:28:53 AM (QP5 v5.254.13202.43190) */
CREATE OR REPLACE FORCE VIEW APPS.XXWC_ORA_EDI_VW
(
   FILE_NAME,
   VENDOR_NUMBER,
   VENDOR_NAME,
   INVOICE_NUMBER,
   INVOICE_AMOUNT,
   PO_NUMBER,
   PAYMENT_TERM,
   INVOICE_DATE,
   CREATION_DATE,
   PROCESS_FLAG,
   ERROR_MESSAGE,
   LAST_RECEIVED_DATE,
   PROCESS_DATE
)
AS
   (SELECT file_name,
           TRIM (i.vendor_number),
           TRIM (i.vendor_name),
           TRIM (i.invoice_number),
           TRIM (i.invoice_amount),
           TRIM (i.po_number),
           t.name payment_term,
           i.invoice_date,
           SYSDATE creation_date,
           i.process_flag,
           i.cp_log_file,
           i.insert_date,
           i.process_date
      FROM xxwc.xxwc_edi_inbound_files_history i,
           ap.ap_suppliers v,
           ap.ap_terms_tl t
     WHERE i.vendor_number = v.segment1 AND v.terms_id = t.term_id);


GRANT SELECT ON APPS.XXWC_ORA_EDI_VW TO INTERFACE_APEXWC;
