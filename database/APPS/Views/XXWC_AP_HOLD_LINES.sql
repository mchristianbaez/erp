/* Formatted on 2/28/2014 8:30:25 PM (QP5 v5.206) */
-- Start of DDL Script for View APPS.XXWC_AP_HOLD_LINES
-- Generated 2/28/2014 8:30:24 PM from APPS@ebizrnd

-- Drop the old instance of XXWC_AP_HOLD_LINES
DROP VIEW apps.xxwc_ap_hold_lines
/

CREATE OR REPLACE VIEW apps.xxwc_ap_hold_lines
(
    item_number
   ,inv_item_desc
   ,inv_line_number
   ,po_line_number
   ,po_line_shipment_qty
   ,inv_line_qty_invoiced
   ,uom
   ,inv_unit_price
   ,po_unit_price
   ,hold_reason
   ,invoice_id
   ,recipient_role
   ,po_number
   ,hold_id
   ,second_page_link
   ,notification_id
   ,notif_subject
)
AS
      SELECT item_number
            ,inv_item_desc
            ,inv_line_number
            ,po_line_number
            ,po_line_shipment_qty
            ,inv_line_qty_invoiced
            ,uom
            ,inv_unit_price
            ,po_unit_price
            ,hold_reason
            ,invoice_id
            ,original_recipient
            ,po_number
            ,hold_id
            ,second_page_link
            ,notification_id
            ,notif_subject
        FROM apps.xxwc_ap_hold_notifications
       WHERE notification_status IN ('XXWC', 'OPEN')
    ORDER BY inv_line_number, po_line_number
/

-- End of DDL Script for View APPS.XXWC_AP_HOLD_LINES
