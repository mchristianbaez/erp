
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUS_AR_COGS_FINAL_V" ("GL_SOURCE", "GL_ACCOUNT", "ORDER_NUMBER", "AMOUNT") AS 
  select gl_source      
      ,substr(gl_account,1,3)  gl_account
      ,to_char(order_number) order_number
      ,nvl(sum(amount), 0) amount
from   xxcus.xxcus_match_gl_rev_b
where  1 =1
  and  order_number is not null
group by gl_source, substr(gl_account,1,3), order_number
union all
select gl_source
      ,substr(gl_account,1,3)  gl_account
      ,'Purchasing/Payables/Others' order_number --to_char(null) order_number
      ,nvl(sum(amount), 0) amount
from   xxcus.xxcus_match_gl_rev_b
where  1 =1
  and  order_number is null
group by gl_source, substr(gl_account,1,3), order_number
union all
select gl_source
      ,substr(gl_account,1,3)  gl_account
      ,to_char(order_number) order_number
      ,nvl(sum(amount), 0) amount
from   xxcus.xxcus_match_gl_cogs_b
where  1 =1
  and  order_number is not null
group by gl_source, substr(gl_account,1,3), order_number
union all
select gl_source
      ,substr(gl_account,1,3)  gl_account
      ,'Purchasing/Payables/Others' order_number
      ,nvl(sum(amount), 0) amount
from   xxcus.xxcus_match_gl_cogs_b
where  1 =1
  and  order_number is null
group by gl_source, substr(gl_account,1,3), order_number;
