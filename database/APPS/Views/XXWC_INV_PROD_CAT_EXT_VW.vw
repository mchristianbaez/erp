/* Formatted on 2/18/2014 10:03:48 AM (QP5 v5.254.13202.43190) */
CREATE OR REPLACE FORCE VIEW APPS.XXWC_INV_PROD_CAT_EXT_VW
(
   OPERATING_UNIT_ID,
   INTERFACE_DATE,
   PROD_CATEGORY_DATA
)
AS
   SELECT operating_unit_id,
          interface_date,
          (   business_unit
           || '|'
           || source_system
           || '|'
           || category
           || '|'
           || category_value
           || '|'
           || category_desc
           || '|'
           || sub_category
           || '|'
           || sub_category_value
           || '|'
           || sub_category_desc
           || '|'
           || product_line
           || '|'
           || product_line_value
           || '|'
           || product_line_desc
           || '|'
           || cat_class
           || '|'
           || cat_class_value
           || '|'
           || cat_class_desc)
             prod_category_data
     FROM apps.xxwc_inv_product_categories_vw;