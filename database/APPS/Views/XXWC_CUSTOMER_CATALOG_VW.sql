   /***********************************************************************************************************************
   *   $Header XXWC_CUSTOMER_CATALOG_VW $
   *   Module Name: XXWC_CUSTOMER_CATALOG_VW.sql
   *
   *   PURPOSE:   This View used in value set to get Party information.
   *
   *   REVISIONS:
   *   Ver        Date         Author                Description
   *   ---------  ----------   ---------------       -----------------------------------------------------------------
   *   1.0        16-Mar-2016  P.Vamshidhar          Initial Version
   *                                                 TMS#20160219-00063 Attribute Enrichment and Extract for Kiewit
   * *********************************************************************************************************************/

CREATE OR REPLACE FORCE VIEW APPS.XXWC_CUSTOMER_CATALOG_VW
(
   ACCOUNT_NAME,
   CUST_ACCOUNT_ID
)
AS
   SELECT 'All' PARTY_NAME, NULL cust_account_id FROM DUAL
   UNION
   SELECT PARTY_NAME, PARTY_ID cust_account_id
     FROM apps.HZ_PARTIES hcaa
    WHERE     hcaa.status = 'A'
          --AND NVL (hcaa.ACCOUNT_TERMINATION_DATE, SYSDATE + 1) > SYSDATE
          AND EXISTS
                 (SELECT 1
                    FROM XXWC.XXWC_B2B_CAT_CUSTOMER_TBL xct
                   WHERE xct.customer_id = hcaa.PARTY_ID)
   ORDER BY PARTY_NAME;


GRANT SELECT ON APPS.XXWC_CUSTOMER_CATALOG_VW TO EA_APEX;
/
