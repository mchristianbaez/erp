
CREATE OR REPLACE FORCE VIEW APPS.XXWC_AR_INVOICE_LINE_EXT_VW
(
   OPERATING_UNIT_ID,
   INTERFACE_DATE,
   INVOICE_LINE_DATA
)
AS
   SELECT x1.operating_unit_id,
          x1.inv_line_creation_date interface_date,
          (   x1.business_unit
           || '|'
           || x1.source_system
           || '|'
           || x1.invoice_unique_id
           || '|'
           || TO_CHAR (x1.invoice_post_date, 'MM/DD/YYYY')
           || '|'
           || x1.invoice_line_num
           || '|'
           || x1.invoice_line_type
           || '|'
           || TO_CHAR (x1.line_price_date, 'MM/DD/YYYY')
           || '|'
           || TO_CHAR (x1.line_ship_date, 'MM/DD/YYYY')
           || '|'
           || x1.ship_from_location
           || '|'
           || x1.ship_method
           || '|'
           -- 07/23/2013 TMS 20130723-01067: Changed to trim and remove special chars
           || SUBSTR (
                 REGEXP_REPLACE (x1.sku_identifier,
                                 '[^[:alnum:]|[:blank:]|[:punct:]]+',
                                 ''),
                 1,
                 50)
           || '|'
           || SUBSTR (
                 REGEXP_REPLACE (x1.sku_short_description,
                                 '[^[:alnum:]|[:blank:]|[:punct:]]+',
                                 ''),
                 1,
                 50)
           || '|'
           || x1.stock_status
           || '|'
           || SUBSTR (
                 REGEXP_REPLACE (x1.cust_po_number,
                                 '[^[:alnum:]|[:blank:]|[:punct:]]+',
                                 ''),
                 1,
                 50)
           -- 07/23/2013 TMS 20130723-01067: Changed to trim and remove special chars
           || '|'
           || x1.cust_product_number
           || '|'
           || x1.unit_of_measure
           || '|'
           || x1.invoice_quantity
           || '|'
           || x1.sales_unit_price
           || '|'
           || x1.qty_per_sales_unit
           || '|'
           || x1.product_unit_cost
           || '|'
           || x1.line_tax_amount
           || '|'
           || x1.order_quantity
           || '|'
           || x1.vendor_part_number
           || '|'
           || x1.inv_line_disc_amount
           || '|'
           || x1.pricing_level
           || '|'
           || x1.list_price
           || '|'
           || x1.qty_break_flag
           || '|'
           || x1.line_source_code
           || '|'
           || x1.overridden_sell_price
           || '|'
           || x1.overriden_product_cost
           || '|'
           || x1.branch_cost
           || '|'
           || x1.overridden_branch_cost
           || '|'
           || x1.po_replacement_cost
           || '|'
           || x1.price_basis
           || '|'
           || x1.sell_group
           || '|'
           || x1.price_formula
           || '|'
           || x1.price_constant
           || '|'
           || x1.price_type
           || '|'
           || x1.reference_price
           || '|'
           || x1.vendor_id
           || '|' --------Added by Harsha for TMS#20131118-00053 -- Modify EDW feed to better account for direct sales
           || NULL
           || '|'
           || NULL
           || '|'
           || NULL
           || '|'
           || NULL
           || '|'
           || NULL
           || '|'
           || NULL
           || '|'
           || NULL
           || '|'
           || NULL
           || '|'
           || NULL
           || '|'
           || NVL (
                 (CASE
                     WHEN x1.ORDER_TYPE = 'INTERNAL ORDER'
                     THEN
                        'N'
                     WHEN    x1.source_type = 'External'
                          OR x1.shipping_method = 'DIRECT SHIP'
                     THEN
                        'Y'
                     ELSE
                        'N'
                  END),
                 'N')) --------Added by Harsha for TMS#20131118-00053 -- Modify EDW feed to better account for direct sales
             invoice_line_data
     FROM APPS.xxwc_ar_invoice_line_vw x1
    WHERE /*(   (NVL (x1.line_type_id, -1) != 1007)
           OR (NVL (x1.line_type_id, -1) = 1007 AND sales_unit_price != 0))*/  --Commented by Veera for TMS#20140717-00188 
        1 = 1
       AND ((NVL (x1.line_type_id, -1) != 1007)
       OR NOT EXISTS
                  (SELECT 1
                     FROM APPS.xxwc_ar_invoice_line_vw
                    WHERE     NVL (line_type_id, -1) = 1007
                          AND NVL (sales_unit_price, 0) = 0
                          AND ORDER_TYPE IN ('WC LONG TERM RENTAL',
                                             'WC SHORT TERM RENTAL')
                          AND invoice_unique_id = x1.invoice_unique_id))   --Added by Veera for TMS#20140717-00188 
        ;
           
    