
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUS_PAM_YTD_PURCH_V" ("CUST_ID", "MVID", "LOB_ID", "LOB_NAME", "CALENDAR_YEAR", "TOTAL_PURCHASES") AS 
  SELECT P.MVID CUST_ID,
            HCA.ATTRIBUTE2 MVID,
            P.LOB_ID LOB_ID,
            Z.PARTY_NAME LOB_NAME,
            P.CALENDAR_YEAR CALENDAR_YEAR,
            SUM (P.TOTAL_PURCHASES) TOTAL_PURCHASES
       FROM APPS.XXCUSOZF_PURCHASES_MV P,
            APPS.HZ_CUST_ACCOUNTS HCA,
            APPS.HZ_PARTIES Z
      WHERE     1 = 1
            AND P.MVID = HCA.CUST_ACCOUNT_ID
            AND P.LOB_ID = Z.PARTY_ID
            AND GRP_MVID = 0
            AND GRP_CAL_YEAR = 0
            AND GRP_LOB = 0
            AND GRP_BU_ID = 1
            AND GRP_PERIOD = 1
            AND GRP_QTR = 1
            AND GRP_BRANCH = 1
            AND GRP_YEAR = 1
            AND GRP_CAL_PERIOD = 1
            AND P.LOB_ID NOT IN ('2063', '2064')
            AND P.CALENDAR_YEAR > 2012
   GROUP BY P.MVID,
            HCA.ATTRIBUTE2,
            P.LOB_ID,
            Z.PARTY_NAME,
            P.CALENDAR_YEAR;
