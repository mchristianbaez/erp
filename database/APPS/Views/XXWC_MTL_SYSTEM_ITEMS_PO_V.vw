--
-- XXWC_MTL_SYSTEM_ITEMS_PO_V  (View)
--

CREATE OR REPLACE FORCE VIEW APPS.XXWC_MTL_SYSTEM_ITEMS_PO_V
(
   ORGANIZATION_ID
  ,ORGANIZATION_CODE
  ,SEGMENT1
  ,ATTRIBUTE21
  ,ATTRIBUTE25
  ,ATTRIBUTE10
  ,ATTRIBUTE22
)
AS
   SELECT a.organization_id
         ,a.organization_code
         ,b.segment1
         ,b.attribute21
         ,b.attribute25
         ,b.attribute10
         ,b.attribute22
     FROM mtl_parameters a, mtl_system_items b
    WHERE a.organization_id = b.organization_id;


