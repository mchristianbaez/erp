CREATE OR REPLACE VIEW APPS.XXWC_EGO_SUPPLIERS_ACTV_VW
/*********************************************************************************
File Name: XXWC_EGO_SUPPLIERS_ACTV_VW
PROGRAM TYPE: View definition
HISTORY
PURPOSE: This view is created to filter out inactive suppliers
         TMS Ticker # 20140709-00148
==================================================================================
VERSION DATE          AUTHOR(S)           DESCRIPTION
------- -----------   ---------------     --------------------------------------------
1.0     22-Aug-2014   KPIT                Initial view definition  TMS Ticker # 20140709-00148
1.1     18-Dec-2014   Manjula Chellappan  20141001-00164 MultiOrg changes
*********************************************************************************/
AS
   SELECT DISTINCT a.segment1 || '-' || a.vendor_name vendor_info,
          a.segment1 vendor_number, a.vendor_id, b.org_id
     FROM apps.ap_suppliers a, apps.ap_supplier_sites_all b
    WHERE a.vendor_type_lookup_code = 'VENDOR'
      AND a.enabled_flag = 'Y'
      AND NVL (a.end_date_active, SYSDATE) >= SYSDATE
      AND a.vendor_id = b.vendor_id
--TMS # 20141001-00164 MultiOrg changes By Manjula on 18-Dec-14
      AND b.org_id = FND_PROFILE.VALUE('ORG_ID');