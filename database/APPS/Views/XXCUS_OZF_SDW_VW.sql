
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUS_OZF_SDW_VW" ("BU_NM", "SDW_SPEND_TOTAL", "FISCAL_PERIOD_ID", "PERIOD_NAME") AS 
  SELECT  /*+ PARALLEL (AUTO) */
      flv.description bu_nm
      ,SUM((nvl(recpt_qty, 0) / (CASE
             WHEN buy_pkg_unt_cnt IS NULL THEN
              1
             WHEN buy_pkg_unt_cnt = 0 THEN
              1
             ELSE
              buy_pkg_unt_cnt
           END)) * nvl(item_cost_amt, 0)) sdw_spend_total
      ,fiscal_per_id
      ,period_name
  FROM xxcus.xxcus_rebate_recon_sdw_tbl s
      ,apps.fnd_lookup_values           flv
      ,gl.gl_periods
 WHERE flv.lookup_type = 'XXCUS_REBATE_BU_XREF'
   AND flv.enabled_flag = 'Y'
   AND nvl(flv.end_date_active, SYSDATE) >= SYSDATE
   AND flv.meaning = s.bu_nm
   AND fiscal_per_id = period_year || lpad(period_num, 2, 0)
 GROUP BY flv.description, fiscal_per_id, period_name
;
