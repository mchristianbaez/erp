/* Formatted on 2/28/2014 8:31:15 PM (QP5 v5.206) */
-- Start of DDL Script for View APPS.XXWC_AP_HOLD_NOTIF_HIST
-- Generated 2/28/2014 8:31:13 PM from APPS@ebizrnd

-- Drop the old instance of XXWC_AP_HOLD_NOTIF_HIST
DROP VIEW apps.xxwc_ap_hold_notif_hist
/

CREATE OR REPLACE VIEW apps.xxwc_ap_hold_notif_hist
(
    approval_history_id
   ,invoice_id
   ,sequence_num
   ,action
   ,approver_id
   ,approver_name
   ,amount_approved
   ,notes
   ,created_by
   ,creation_date
   ,last_update_date
   ,last_updated_by
   ,last_update_login
   ,org_id
   ,notification_order
   ,orig_system
   ,item_class
   ,item_id
   ,line_number
   ,hold_id
   ,history_type
)
AS
    SELECT apinvapprovalhistalleo.approval_history_id
          ,apinvapprovalhistalleo.invoice_id
          ,apinvapprovalhistalleo.iteration sequence_num
          ,apinvapprovalhistalleo.response action
          ,apinvapprovalhistalleo.approver_id
          ,apinvapprovalhistalleo.approver_name approver_name
          ,apinvapprovalhistalleo.amount_approved
          ,apinvapprovalhistalleo.approver_comments notes
          ,apinvapprovalhistalleo.created_by
          ,apinvapprovalhistalleo.creation_date creation_date
          ,apinvapprovalhistalleo.last_update_date
          ,apinvapprovalhistalleo.last_updated_by
          ,apinvapprovalhistalleo.last_update_login
          ,apinvapprovalhistalleo.org_id
          ,apinvapprovalhistalleo.notification_order
          ,apinvapprovalhistalleo.orig_system
          ,apinvapprovalhistalleo.item_class
          ,apinvapprovalhistalleo.item_id
          ,apinvapprovalhistalleo.line_number
          ,apinvapprovalhistalleo.hold_id
          ,apinvapprovalhistalleo.history_type
      FROM apps.ap_inv_aprvl_hist apinvapprovalhistalleo
/

-- End of DDL Script for View APPS.XXWC_AP_HOLD_NOTIF_HIST
