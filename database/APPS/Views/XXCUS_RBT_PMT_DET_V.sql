  /*******************************************************************************
      View: APPS.XXCUS_RBT_PMT_DET_V
  --    REVISIONS:
  --    Ver        Date        Author                                 Tikcet                                      Description
  --    ---------  ----------  ---------------                       -------------------------------   ---------------------------------
  --     1.5        10/07/205    Balaguru Seshadri    TMS 20151001-00046      Created. 
  *******************************************************************************/
CREATE OR REPLACE FORCE VIEW APPS.XXCUS_RBT_PMT_DET_V
(
   ORG_ID,
   MVID,
   VENDOR_NAME,
   AGREEMENT_YEAR,
   AGREEMENT_NAME,
   AGREEMENT_CODE,
   REBATE_TYPE
)
AS
   SELECT org_id,
          mvid,
          mv_name vendor_name,
          agreement_yr agreement_year,
          agreement_name,
          agreement_code,
          activity_type rebate_type
     FROM XXCUS_PAM_PROGRAM_V
    WHERE     1 = 1
          AND agreement_status IN ('ACTIVE', 'ONHOLD')
          AND agreement_yr > 2013;
--
COMMENT ON TABLE APPS.XXCUS_RBT_PMT_DET_V IS 'TMS 20151001-00046';
--
GRANT SELECT  ON APPS.XXCUS_RBT_PMT_DET_V TO INTERFACE_ECM;
--          