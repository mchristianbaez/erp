CREATE OR REPLACE VIEW XXWC_GETPAID_AREXTI_V AS
SELECT RPAD(NVL(hca.account_number, ' '),20,' ')                                                         --  CUSTOMER_NUMBER
     || RPAD(NVL(rcta.trx_number, ' '),20,' ')                                                           --  TRANSACTION_NUMBER
     || RPAD(NVL(hcsu_s.location, ' '),35,' ')                                                           --  SHIP_TO_NAME
     || RPAD(NVL(hl_s.address1, ' '),50,' ')                                                             --  SHIP_TO_ADDRESS_1
     || RPAD(NVL(TRIM(hl_s.address2), ' '),50,' ')                                                       --  SHIP_TO_ADDRESS_2
     || RPAD(NVL(hl_s.city, ' '),50,' ')                                                                 --  SHIP_TO_ADDRESS_3
     || RPAD(NVL(hl_s.state, ' '),50,' ')                                                                --  SHIP_TO_ADDRESS_4
     || RPAD(NVL(hl_s.postal_code, ' '),50,' ')                                                          --  SHIP_TO_ADDRESS_5
     || RPAD(NVL(rcta.ct_reference, ' '),11,' ')                                                         --  ORDER_NUMBER
     || RPAD(NVL((SELECT RPAD(NVL(TO_CHAR(rctl.sales_order_date,'MMDDYYYY')
                                , TO_CHAR(rcta.trx_date,'MMDDYYYY')),8,' ')
                    FROM apps.ra_customer_trx_lines rctl
                   WHERE rctl.customer_trx_id = rcta.customer_trx_id
                     AND ROWNUM = 1)
                , ' ')
              , 8, ' ')                                                                                  --  ORDER_DATE
     || RPAD(NVL((CASE WHEN xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id ) IN ('ORDER MANAGEMENT','STANDARD OM SOURCE'
                                         , 'REPAIR OM SOURCE')
                  THEN (SELECT ood.organization_code
                          FROM apps.oe_order_headers           ooha
                             , org_organization_definitions   ood
                         WHERE ooha.order_number            = NVL(rcta.interface_header_attribute1, rcta.ct_reference)
                           AND ooha.ship_from_org_id        = ood.organization_id
                           AND ROWNUM = 1)
                  WHEN xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id ) = 'PRISM'
                  THEN rcta.interface_header_attribute7
                  ELSE ' '
                  END),' '), 35, ' ')                                                                    --  WAREHOUSE
     || RPAD(NVL(rt.description, ' '),20,' ')                                                            --  PAYMENT_TERMS
     || RPAD(NVL((CASE WHEN xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id ) IN ('ORDER MANAGEMENT','STANDARD OM SOURCE'
                                        , 'REPAIR OM SOURCE')
             THEN (SELECT ship_method_meaning
                     FROM apps.oe_order_headers ooh
                        , wsh_carrier_services wcs
                    WHERE ooh.shipping_method_code IS NOT NULL
                      AND ooh.shipping_method_code  = wcs.ship_method_code
                      AND TO_CHAR(ooh.order_number) = NVL(rcta.interface_header_attribute1, rcta.ct_reference)
                      AND ROWNUM                    = 1)
                  WHEN xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id ) = 'PRISM'
                  THEN rcta.interface_header_attribute12
                  ELSE ' '
                  END)
                , '99:WILL CALL'),30,' ')                                                                --  SHIP_VIA_NUMBER
     || RPAD(NVL(' ', ' '),15,' ')                                                                       --  ORDER_STATUS
     || RPAD(NVL(' ', ' '),5,' ')                                                                        --  ORDER_SHIPMENT_CARRIER
     || LPAD(NVL(TO_CHAR(apsa.discount_original,'9999999990.99'), '0.00'),20,' ')                        --  DISCOUNT_AMOUNT
     || LPAD(NVL(TO_CHAR(rtld.discount_days), '0'),3,' ')                                                --  DISCOUNT_DAYS
     || RPAD(NVL((SELECT jrdv.source_first_name||' '||jrdv.source_last_name
                    FROM jtf_rs_defresources_v jrdv
                       , apps.ra_salesreps      rsa
                   WHERE rcta.primary_salesrep_id = rsa.salesrep_id
                     AND rcta.org_id              = rsa.org_id
                     AND rsa.person_id = jrdv.source_id
                     AND ROWNUM                   = 1)
        , ' '),10,' ')                                                                                   --  SALES_MAN
     || RPAD(NVL(rcta.fob_point, ' '),30,' ')                                                            --  FOB_POINT
     || RPAD(NVL(TO_CHAR(rcta.trx_date,'MMDDYYYY'), ' '),8,' ')                                          --  SHIP_DATE
     || RPAD(NVL(hca.account_name, ' '),50,' ')                                                          --  BILL_TO_NAME
--     || xxwc_ar_getpaid_ob_pkg.get_bill_to_addr(rcta.org_id
--                                              , hca.cust_account_id
--                                              , hcsu_s.bill_to_site_use_id)                              --  BILL_TO_ADDRESS
     || RPAD (NVL (hl_b.address1, ' '), 50, ' ')    --  BILL_TO_ADDRESS_1
     || RPAD (NVL (TRIM (hl_b.address2), ' '), 50, ' ') --  BILL_TO_ADDRESS_2
     || RPAD (NVL (hl_b.city, ' '), 50, ' ')        --  BILL_TO_ADDRESS_3
     || RPAD (NVL (hl_b.state, ' '), 50, ' ')       --  BILL_TO_ADDRESS_4
     || RPAD (NVL (hl_b.postal_code, ' '), 50, ' ') --  BILL_TO_ADDRESS_5
     REC_LINE
     , rcta.org_id --, hca.account_number, hca.cust_account_id , rcta.trx_number, apsa.AMOUNT_DUE_REMAINING
  FROM apps.ra_customer_trx rcta
     -- , ra_batch_sources_all rbsa
     , apps.hz_cust_accounts hca
     , ra_terms rt
     , ra_terms_lines_discounts rtld
     , apps.hz_cust_site_uses hcsu_s
     , apps.hz_cust_acct_sites hcas_s
     , hz_party_sites hps_s
     , hz_locations hl_s
     , apps.hz_cust_site_uses hcsu_b
     , apps.hz_cust_acct_sites hcas_b
     , hz_party_sites hps_b
     , hz_locations hl_b
     , apps.ar_payment_schedules apsa
 WHERE 1                        = 1
   AND rcta.bill_to_customer_id = hca.cust_account_id
   AND rcta.customer_trx_id     = apsa.customer_trx_id
   AND rcta.ship_to_site_use_id = hcsu_s.site_use_id
   AND hcas_s.cust_acct_site_id = hcsu_s.cust_acct_site_id
   AND hcas_s.party_site_id     = hps_s.party_site_id
   AND hps_s.location_id        = hl_s.location_id
--   AND hcsu_b.site_use_id       = hcsu_s.bill_to_site_use_id
   AND hcas_b.cust_acct_site_id = hcsu_b.cust_acct_site_id
   AND hca.cust_account_id      = hcas_b.cust_account_id
   AND hcsu_b.site_use_code     = 'BILL_TO'
   AND hcsu_b.primary_flag      = 'Y'
   AND hcas_b.party_site_id     = hps_b.party_site_id
   AND hps_b.location_id        = hl_b.location_id
   AND rcta.term_id             = rt.term_id (+)
   AND rt.term_id               = rtld.term_id (+)
--   AND NVL(rcta.interface_header_context, '&*^%$#@') <> 'CONVERSION'
   -- AND rcta.batch_source_id        = rbsa.batch_source_id
   -- AND rcta.org_id                 = rbsa.org_id
   AND NVL(xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id ), '&*^%$#@')   <> 'CONVERSION'
   AND apsa.status = 'OP' AND apsa.amount_due_remaining != 0 AND apsa.org_id = rcta.org_id
UNION
------------------------------------------------------------------------------------------------------------------------------
-- NON-CONVERSION Invoices
-- rcta.ship_to_site_use_id IS NULL AND rcta.ship_to_address_id IS NOT NULL
------------------------------------------------------------------------------------------------------------------------------
SELECT RPAD(NVL(hca.account_number, ' '),20,' ')                                                      --  CUSTOMER_NUMBER
     || RPAD(NVL(rcta.trx_number, ' '),20,' ')                                                        --  TRANSACTION_NUMBER
     || RPAD(NVL(hcsu_s.location, ' '),35,' ')                                                        --  SHIP_TO_NAME
     || RPAD(NVL(hl_s.address1, ' '),50,' ')                                                          --  SHIP_TO_ADDRESS_1
     || RPAD(NVL(TRIM(hl_s.address2), ' '),50,' ')                                                    --  SHIP_TO_ADDRESS_2
     || RPAD(NVL(hl_s.city, ' '),50,' ')                                                              --  SHIP_TO_ADDRESS_3
     || RPAD(NVL(hl_s.state, ' '),50,' ')                                                             --  SHIP_TO_ADDRESS_4
     || RPAD(NVL(hl_s.postal_code, ' '),50,' ')                                                       --  SHIP_TO_ADDRESS_5
     || RPAD(NVL(rcta.ct_reference, ' '),11,' ')                                                      --  ORDER_NUMBER
     || RPAD(NVL((SELECT RPAD(NVL(TO_CHAR(rctl.sales_order_date,'MMDDYYYY')
                                , TO_CHAR(rcta.trx_date,'MMDDYYYY')),8,' ')
                    FROM apps.ra_customer_trx_lines rctl
                   WHERE rctl.customer_trx_id = rcta.customer_trx_id
                     AND rownum = 1)
             , ' ')
             , 8, ' ')                                                                                --  ORDER_DATE
     || RPAD(NVL((CASE WHEN xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id ) IN ('ORDER MANAGEMENT','STANDARD OM SOURCE'
                                        , 'REPAIR OM SOURCE')
                  THEN (SELECT ood.organization_code
                          FROM apps.oe_order_headers           ooha
                             , org_organization_definitions   ood
                         WHERE ooha.order_number            = NVL(rcta.interface_header_attribute1, rcta.ct_reference)
                           AND ooha.ship_from_org_id        = ood.organization_id
                           AND ROWNUM = 1
                       )
                  WHEN xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id ) = 'PRISM'
                  THEN rcta.interface_header_attribute7
                  ELSE ' '
                  END),' '), 35, ' ')                                                                 --  WAREHOUSE
     || RPAD(NVL(rt.description, ' '),20,' ')                                                         --  PAYMENT_TERMS
     || RPAD(NVL((CASE WHEN xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id ) IN ('ORDER MANAGEMENT','STANDARD OM SOURCE'
                                        , 'REPAIR OM SOURCE')
             THEN (SELECT ship_method_meaning
                   FROM apps.oe_order_headers ooh
                      , wsh_carrier_services wcs
                  WHERE ooh.shipping_method_code IS NOT NULL
                    AND ooh.shipping_method_code  = wcs.ship_method_code
                    AND TO_CHAR(ooh.order_number) = NVL(rcta.interface_header_attribute1, rcta.ct_reference)
                    AND ROWNUM                    = 1)
                  WHEN xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id ) = 'PRISM'
                  THEN rcta.interface_header_attribute12
                  ELSE ' '
                  END)
                , '99:WILL CALL'),30,' ')                                                             --  SHIP_VIA_NUMBER
     || RPAD(NVL(' ', ' '),15,' ')                                                                    --  ORDER_STATUS
     || RPAD(NVL(' ', ' '),5,' ')                                                                     --  ORDER_SHIPMENT_CARRIER
     || LPAD(NVL(TO_CHAR(apsa.discount_original,'9999999990.99'), '0.00'),20,' ')                     --  DISCOUNT_AMOUNT
     || LPAD(NVL(TO_CHAR(rtld.discount_days), '0'),3,' ')                                             --  DISCOUNT_DAYS
     || RPAD(NVL((SELECT jrdv.source_first_name||' '||jrdv.source_last_name
                   FROM jtf_rs_defresources_v      jrdv
                      , apps.ra_salesreps           rsa
                  WHERE rcta.primary_salesrep_id = rsa.salesrep_id
                    AND rcta.org_id              = rsa.org_id
                    AND rsa.person_id = jrdv.source_id
                    AND ROWNUM                   = 1)
        , ' '),10,' ')                                                                                --  SALES_MAN
     || RPAD(NVL(rcta.fob_point, ' '),30,' ')                                                         --  FOB_POINT
     || RPAD(NVL(TO_CHAR(rcta.trx_date,'MMDDYYYY'), ' '),8,' ')                                       --  SHIP_DATE
     || RPAD(NVL(hca.account_name, ' '),50,' ')                                                       --  BILL_TO_NAME
--     || xxwc_ar_getpaid_ob_pkg.get_bill_to_addr(rcta.org_id
--                                              , hca.cust_account_id
--                                              , hcsu_s.bill_to_site_use_id)                           --  BILL_TO_ADDRESS
     || RPAD (NVL (hl_b.address1, ' '), 50, ' ')    --  BILL_TO_ADDRESS_1
     || RPAD (NVL (TRIM (hl_b.address2), ' '), 50, ' ') --  BILL_TO_ADDRESS_2
     || RPAD (NVL (hl_b.city, ' '), 50, ' ')        --  BILL_TO_ADDRESS_3
     || RPAD (NVL (hl_b.state, ' '), 50, ' ')       --  BILL_TO_ADDRESS_4
     || RPAD (NVL (hl_b.postal_code, ' '), 50, ' ') --  BILL_TO_ADDRESS_5
     REC_LINE
     , rcta.org_id  --, hca.account_number, hca.cust_account_id , rcta.trx_number, apsa.AMOUNT_DUE_REMAINING
  FROM apps.ra_customer_trx rcta
     -- , ra_batch_sources_all rbsa
     , apps.hz_cust_accounts hca
     , ra_terms rt
     , ra_terms_lines_discounts rtld
     , apps.hz_cust_site_uses hcsu_s
     , apps.hz_cust_acct_sites hcas_s
     , hz_party_sites hps_s
     , hz_locations hl_s
     , apps.hz_cust_site_uses hcsu_b
     , apps.hz_cust_acct_sites hcas_b
     , hz_party_sites hps_b
     , hz_locations hl_b
     , apps.ar_payment_schedules apsa
 WHERE 1 = 1
   AND rcta.bill_to_customer_id = hca.cust_account_id
   AND rcta.customer_trx_id     =  apsa.customer_trx_id
   AND rcta.ship_to_address_id  =  hcas_s.cust_acct_site_id
   AND rcta.ship_to_site_use_id IS NULL
   AND hcas_s.cust_acct_site_id = hcsu_s.cust_acct_site_id
   AND hcas_s.party_site_id     = hps_s.party_site_id
   AND hps_s.location_id        = hl_s.location_id
   AND hca.cust_account_id      = hcas_b.cust_account_id
   AND hcas_b.cust_acct_site_id = hcsu_b.cust_acct_site_id
   AND hcsu_s.site_use_code     = 'SHIP_TO'
   AND hcsu_b.site_use_code     = 'BILL_TO'
--   AND hcsu_b.site_use_id       = hcsu_s.bill_to_site_use_id
   AND hcsu_b.primary_flag = 'Y'
   AND hcas_b.party_site_id     = hps_b.party_site_id
   AND hps_b.location_id        = hl_b.location_id
   AND rcta.term_id             = rt.term_id (+)
   AND rt.term_id               = rtld.term_id (+)
--   AND NVL(rcta.interface_header_context, '&*^%$#@') <> 'CONVERSION'
   -- AND rcta.batch_source_id        = rbsa.batch_source_id
   -- AND rcta.org_id                 = rbsa.org_id
   AND NVL(xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id ), '&*^%$#@')   <> 'CONVERSION'
   AND apsa.status = 'OP' AND apsa.amount_due_remaining != 0 AND apsa.org_id = rcta.org_id
UNION
------------------------------------------------------------------------------------------------------------------------------
-- CONVERSION Invoices
-- rcta.ship_to_site_use_id IS NOT NULL
------------------------------------------------------------------------------------------------------------------------------
SELECT RPAD(NVL(hca.account_number, ' '),20,' ')                                                      --  CUSTOMER_NUMBER
     || RPAD(NVL(rcta.trx_number, ' '),20,' ')                                                        --  TRANSACTION_NUMBER
     || RPAD(NVL(hcsu_s.location, ' '),35,' ')                                                        --  SHIP_TO_NAME
     || RPAD(NVL(hl_s.address1, ' '),50,' ')                                      --  SHIP_TO_ADDRESS_1
     || RPAD(NVL(TRIM(hl_s.address2), ' '),50,' ')                               --  SHIP_TO_ADDRESS_2
     || RPAD(NVL(hl_s.city, ' '),50,' ')                                                              --  SHIP_TO_ADDRESS_3
     || RPAD(NVL(hl_s.state, ' '),50,' ')                                                             --  SHIP_TO_ADDRESS_4
     || RPAD(NVL(hl_s.postal_code, ' '),50,' ')                                                       --  SHIP_TO_ADDRESS_5
     || RPAD(NVL(rcta.ct_reference, ' '),11,' ')                                                      --  ORDER_NUMBER
     || RPAD(NVL(TO_CHAR(gp_dmp.ordate,'MMDDYYYY'), ' '), 8, ' ')                                     --  ORDER_DATE
     || RPAD(NVL(gp_dmp.warehse, '000'),35,' ')                                                       --  WAREHOUSE
     || RPAD(NVL(rt.description, ' '),20,' ')                                                         --  PAYMENT_TERMS
     || RPAD(NVL(gp_dmp.shipvia, '99:WILL CALL'),30,' ')                                              --  SHIP_VIA_NUMBER
     || RPAD(NVL(' ', ' '),15,' ')                                                                    --  ORDER_STATUS
     || RPAD(NVL(' ', ' '),5,' ')                                                                     --  ORDER_SHIPMENT_CARRIER
     || LPAD(NVL(TO_CHAR(apsa.discount_original,'9999999990.99'), '0.00'),20,' ')                     --  DISCOUNT_AMOUNT
     || LPAD(NVL(TO_CHAR(rtld.discount_days), '0'),3,' ')                                             --  DISCOUNT_DAYS
     || RPAD(NVL((SELECT jrdv.source_first_name||' '||jrdv.source_last_name
                   FROM jtf_rs_defresources_v jrdv
                      , apps.ra_salesreps      rsa
                  WHERE rcta.primary_salesrep_id = rsa.salesrep_id
                    AND rcta.org_id = rsa.org_id
                    AND rsa.person_id = jrdv.source_id)
        , ' '),10,' ')                                                                                --  SALES_MAN
     || RPAD(NVL(rcta.fob_point, ' '),30,' ')                                                         --  FOB_POINT
     || RPAD(NVL(TO_CHAR(rcta.trx_date,'MMDDYYYY'), ' '),8,' ')                                       --  SHIP_DATE
     || RPAD(NVL(hca.account_name, ' '),50,' ')                                                        --  BILL_TO_NAME
--     || xxwc_ar_getpaid_ob_pkg.get_bill_to_addr(rcta.org_id
--                                              , hca.cust_account_id
--                                              , hcsu_s.bill_to_site_use_id)                              --  BILL_TO_ADDRESS
     || RPAD (NVL (hl_b.address1, ' '), 50, ' ')    --  BILL_TO_ADDRESS_1
     || RPAD (NVL (TRIM (hl_b.address2), ' '), 50, ' ') --  BILL_TO_ADDRESS_2
     || RPAD (NVL (hl_b.city, ' '), 50, ' ')        --  BILL_TO_ADDRESS_3
     || RPAD (NVL (hl_b.state, ' '), 50, ' ')       --  BILL_TO_ADDRESS_4
     || RPAD (NVL (hl_b.postal_code, ' '), 50, ' ') --  BILL_TO_ADDRESS_5
     REC_LINE
     , rcta.org_id  --, hca.account_number, hca.cust_account_id , rcta.trx_number, apsa.AMOUNT_DUE_REMAINING
  FROM apps.ra_customer_trx rcta
     -- , ra_batch_sources_all rbsa
     , apps.hz_cust_accounts hca
     , ra_terms rt
     , ra_terms_lines_discounts rtld
     , apps.hz_cust_site_uses hcsu_s
     , apps.hz_cust_acct_sites hcas_s
     , hz_party_sites hps_s
     , hz_locations hl_s
     , apps.hz_cust_site_uses hcsu_b
     , apps.hz_cust_acct_sites hcas_b
     , hz_party_sites hps_b
     , hz_locations hl_b
     , apps.ar_payment_schedules apsa
     , apps.xxwc_arexti_getpaid_dump_tbl gp_dmp
 WHERE 1                        = 1
   AND rcta.bill_to_customer_id = hca.cust_account_id
   AND rcta.customer_trx_id     = apsa.customer_trx_id
   AND rcta.ship_to_site_use_id = hcsu_s.site_use_id
   AND hcas_s.cust_acct_site_id = hcsu_s.cust_acct_site_id
   AND hcas_s.party_site_id     = hps_s.party_site_id
   AND hps_s.location_id        = hl_s.location_id
   AND hca.cust_account_id      = hcas_b.cust_account_id
   AND hcas_b.cust_acct_site_id = hcsu_b.cust_acct_site_id
   AND hcsu_b.site_use_code     = 'BILL_TO'
   AND hcsu_b.primary_flag      = 'Y'
--   AND hcsu_b.site_use_id       = hcsu_s.bill_to_site_use_id
   AND hcas_b.party_site_id     = hps_b.party_site_id
   AND hps_b.location_id        = hl_b.location_id
   AND rcta.term_id             = rt.term_id (+)
   AND rt.term_id               = rtld.term_id (+)
   AND rcta.trx_number          = gp_dmp.invno
   AND hca.account_number       = gp_dmp.custno
--   AND NVL(rcta.interface_header_context, '&*^%$#@') = 'CONVERSION'
   -- AND rcta.batch_source_id        = rbsa.batch_source_id
   -- AND rcta.org_id                 = rbsa.org_id
   AND NVL(xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id ), '&*^%$#@')   = 'CONVERSION'
   AND apsa.status = 'OP' AND apsa.amount_due_remaining != 0 AND apsa.org_id = rcta.org_id
UNION
------------------------------------------------------------------------------------------------------------------------------
-- CONVERSION Invoices
-- SHIP_TO_SITE_USE_ID IS NULL AND rcta.ship_to_address_id IS NOT NULL
------------------------------------------------------------------------------------------------------------------------------
SELECT RPAD(NVL(hca.account_number, ' '),20,' ')                                                      --  CUSTOMER_NUMBER
     || RPAD(NVL(rcta.trx_number, ' '),20,' ')                                                        --  TRANSACTION_NUMBER
     || RPAD(NVL(hcsu_s.location, ' '),35,' ')                                                        --  SHIP_TO_NAME
     || RPAD(NVL(hl_s.address1, ' '),50,' ')                                      --  SHIP_TO_ADDRESS_1
     || RPAD(NVL(TRIM(hl_s.address2), ' '),50,' ')                               --  SHIP_TO_ADDRESS_2
     || RPAD(NVL(hl_s.city, ' '),50,' ')                                                              --  SHIP_TO_ADDRESS_3
     || RPAD(NVL(hl_s.state, ' '),50,' ')                                                             --  SHIP_TO_ADDRESS_4
     || RPAD(NVL(hl_s.postal_code, ' '),50,' ')                                                       --  SHIP_TO_ADDRESS_5
     || RPAD(NVL(rcta.ct_reference, ' '),11,' ')                                                      --  ORDER_NUMBER
     || RPAD(NVL(TO_CHAR(gp_dmp.ordate,'MMDDYYYY') , ' '), 8, ' ')                                                       --  ORDER_DATE
     || RPAD(NVL(gp_dmp.warehse, '000'),35,' ')                                                          --  WAREHOUSE
     || RPAD(NVL(rt.description, ' '),20,' ')                                                         --  PAYMENT_TERMS
     || RPAD(NVL(gp_dmp.shipvia, '99:WILL CALL'),30,' ')                                               --  SHIP_VIA_NUMBER
     || RPAD(NVL(' ', ' '),15,' ')                                                                    --  ORDER_STATUS
     || RPAD(NVL(' ', ' '),5,' ')                                                                     --  ORDER_SHIPMENT_CARRIER
     || LPAD(NVL(TO_CHAR(apsa.discount_original,'9999999990.99'), '0.00'),20,' ')                     --  DISCOUNT_AMOUNT
     || LPAD(NVL(TO_CHAR(rtld.discount_days), '0'),3,' ')                                             --  DISCOUNT_DAYS
     || RPAD(NVL((SELECT jrdv.source_first_name||' '||jrdv.source_last_name
                   FROM jtf_rs_defresources_v jrdv
                      , apps.ra_salesreps      rsa
                  WHERE rcta.primary_salesrep_id = rsa.salesrep_id
                    AND rcta.org_id = rsa.org_id
                    AND rsa.person_id = jrdv.source_id)
        , ' '),10,' ')                                                                                --  SALES_MAN
     || RPAD(NVL(rcta.fob_point, ' '),30,' ')                                                         --  FOB_POINT
     || RPAD(NVL(TO_CHAR(rcta.trx_date,'MMDDYYYY'), ' '),8,' ')                                       --  SHIP_DATE
     || RPAD(NVL(hca.account_name, ' '),50,' ')                                                        --  BILL_TO_NAME
--     || xxwc_ar_getpaid_ob_pkg.get_bill_to_addr(rcta.org_id
--                                              , hca.cust_account_id
--                                              , hcsu_s.bill_to_site_use_id)                              --  BILL_TO_ADDRESS
     || RPAD (NVL (hl_b.address1, ' '), 50, ' ')    --  BILL_TO_ADDRESS_1
     || RPAD (NVL (TRIM (hl_b.address2), ' '), 50, ' ') --  BILL_TO_ADDRESS_2
     || RPAD (NVL (hl_b.city, ' '), 50, ' ')        --  BILL_TO_ADDRESS_3
     || RPAD (NVL (hl_b.state, ' '), 50, ' ')       --  BILL_TO_ADDRESS_4
     || RPAD (NVL (hl_b.postal_code, ' '), 50, ' ') --  BILL_TO_ADDRESS_5
     REC_LINE
     , rcta.org_id  --, hca.account_number, hca.cust_account_id , rcta.trx_number, apsa.AMOUNT_DUE_REMAINING
  FROM apps.ra_customer_trx rcta
     -- , ra_batch_sources_all rbsa
     , apps.hz_cust_accounts hca
     , ra_terms rt
     , ra_terms_lines_discounts rtld
     , apps.hz_cust_site_uses hcsu_s
     , apps.hz_cust_acct_sites hcas_s
     , hz_party_sites hps_s
     , hz_locations hl_s
     , apps.hz_cust_site_uses hcsu_b
     , apps.hz_cust_acct_sites hcas_b
     , hz_party_sites hps_b
     , hz_locations hl_b
     , apps.ar_payment_schedules apsa
     , apps.xxwc_arexti_getpaid_dump_tbl gp_dmp
 WHERE 1 = 1
   AND rcta.bill_to_customer_id = hca.cust_account_id
   AND rcta.customer_trx_id =  apsa.customer_trx_id
   AND rcta.ship_to_address_id =  hcas_s.cust_acct_site_id
   AND rcta.ship_to_site_use_id IS NULL
   AND hcas_s.cust_acct_site_id = hcsu_s.cust_acct_site_id
   AND hcas_s.party_site_id = hps_s.party_site_id
   AND hps_s.location_id = hl_s.location_id
   AND hca.cust_account_id = hcas_b.cust_account_id
   AND hcas_b.cust_acct_site_id = hcsu_b.cust_acct_site_id
   AND hcsu_s.site_use_code = 'SHIP_TO'
   AND hcsu_b.site_use_code = 'BILL_TO'
   AND hcsu_b.primary_flag = 'Y'
--   AND hcsu_b.site_use_id       = hcsu_s.bill_to_site_use_id
   AND hcas_b.party_site_id = hps_b.party_site_id
   AND hps_b.location_id = hl_b.location_id
   AND rcta.term_id = rt.term_id (+)
   AND rt.term_id = rtld.term_id (+)
   AND rcta.trx_number          = gp_dmp.invno
   AND hca.account_number       = gp_dmp.custno
   AND apsa.status = 'OP' AND apsa.amount_due_remaining != 0 AND apsa.org_id = rcta.org_id
--   AND NVL(rcta.interface_header_context, '&*^%$#@') = 'CONVERSION'
   -- AND rcta.batch_source_id        = rbsa.batch_source_id
   -- AND rcta.org_id                 = rbsa.org_id
   AND NVL(xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id ), '&*^%$#@')   = 'CONVERSION'
UNION
------------------------------------------------------------------------------------------------------------------------------
-- NON-CONVERSION
-- rcta.ship_to_site_use_id IS NULL AND rcta.ship_to_address_id IS NULL
------------------------------------------------------------------------------------------------------------------------------
SELECT RPAD(NVL(hca.account_number, ' '),20,' ')                                                      --  CUSTOMER_NUMBER
     || RPAD(NVL(rcta.trx_number, ' '),20,' ')                                                        --  TRANSACTION_NUMBER
     || RPAD(NVL(hcsu_s.location, ' '),35,' ')                                                        --  SHIP_TO_NAME
     || RPAD(NVL(hl_s.address1, ' '),50,' ')                                      --  SHIP_TO_ADDRESS_1
     || RPAD(NVL(TRIM(hl_s.address2), ' '),50,' ')                                --  SHIP_TO_ADDRESS_2
     || RPAD(NVL(hl_s.city, ' '),50,' ')                                                              --  SHIP_TO_ADDRESS_3
     || RPAD(NVL(hl_s.state, ' '),50,' ')                                                             --  SHIP_TO_ADDRESS_4
     || RPAD(NVL(hl_s.postal_code, ' '),50,' ')                                                       --  SHIP_TO_ADDRESS_5
     || RPAD(NVL(rcta.ct_reference, ' '),11,' ')                                                      --  ORDER_NUMBER
     || RPAD(NVL((SELECT RPAD(NVL(TO_CHAR(rctl.sales_order_date,'MMDDYYYY'), TO_CHAR(rcta.trx_date,'MMDDYYYY')),8,' ')
           FROM apps.ra_customer_trx_lines rctl
          WHERE rctl.customer_trx_id = rcta.customer_trx_id
            AND rownum = 1
                 )
             , ' ')
             , 8, ' ')                                                                                  --  ORDER_DATE
     || RPAD(NVL((CASE WHEN xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id ) IN ('ORDER MANAGEMENT','STANDARD OM SOURCE', 'REPAIR OM SOURCE')
                  THEN (SELECT ood.organization_code
                          FROM apps.oe_order_headers           ooha
                             , org_organization_definitions   ood
                         WHERE ooha.order_number            = NVL(rcta.interface_header_attribute1, rcta.ct_reference)
                           AND ooha.ship_from_org_id        = ood.organization_id
                           AND ROWNUM = 1
                       )
                  WHEN xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id ) = 'PRISM'
                  THEN rcta.interface_header_attribute7
                  ELSE ' '
                  END),' '), 35, ' ')                                                                 --  WAREHOUSE
     || RPAD(NVL(rt.description, ' '),20,' ')                                                         --  PAYMENT_TERMS
     || RPAD(NVL((CASE WHEN xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id ) IN ('ORDER MANAGEMENT','STANDARD OM SOURCE', 'REPAIR OM SOURCE')
                  THEN (SELECT ship_method_meaning
                        FROM apps.oe_order_headers ooh
                           , wsh_carrier_services wcs
                       WHERE ooh.shipping_method_code IS NOT NULL
                         AND ooh.shipping_method_code  = wcs.ship_method_code
                         AND TO_CHAR(ooh.order_number) = NVL(rcta.interface_header_attribute1, rcta.ct_reference)
                         AND ROWNUM                    = 1)
                  WHEN xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id ) = 'PRISM'
                  THEN rcta.interface_header_attribute12
                  ELSE ' '
                  END)
                , '99:WILL CALL'),30,' ')                                                             --  SHIP_VIA_NUMBER
     || RPAD(NVL(' ', ' '),15,' ')                                                                    --  ORDER_STATUS
     || RPAD(NVL(' ', ' '),5,' ')                                                                     --  ORDER_SHIPMENT_CARRIER
     || LPAD(NVL(TO_CHAR(apsa.discount_original,'9999999990.99'), '0.00'),20,' ')                     --  DISCOUNT_AMOUNT
     || LPAD(NVL(TO_CHAR(rtld.discount_days), '0'),3,' ')                                             --  DISCOUNT_DAYS
     || RPAD(NVL((SELECT jrdv.source_first_name||' '||jrdv.source_last_name
                    FROM jtf_rs_defresources_v jrdv
                       , apps.ra_salesreps      rsa
                   WHERE rcta.primary_salesrep_id = rsa.salesrep_id
                     AND rcta.org_id = rsa.org_id
                     AND rsa.person_id = jrdv.source_id)
        , ' '),10,' ')                                                                                --  SALES_MAN
     || RPAD(NVL(rcta.fob_point, ' '),30,' ')                                                         --  FOB_POINT
     || RPAD(NVL(TO_CHAR(rcta.trx_date,'MMDDYYYY'), ' '),8,' ')                                       --  SHIP_DATE
     || RPAD(NVL(hca.account_name, ' '),50,' ')                                                        --  BILL_TO_NAME
--     || xxwc_ar_getpaid_ob_pkg.get_bill_to_addr(rcta.org_id
--                                              , hca.cust_account_id
--                                              , hcsu_s.bill_to_site_use_id)                              --  BILL_TO_ADDRESS
     || RPAD (NVL (hl_b.address1, ' '), 50, ' ')    --  BILL_TO_ADDRESS_1
     || RPAD (NVL (TRIM (hl_b.address2), ' '), 50, ' ') --  BILL_TO_ADDRESS_2
     || RPAD (NVL (hl_b.city, ' '), 50, ' ')        --  BILL_TO_ADDRESS_3
     || RPAD (NVL (hl_b.state, ' '), 50, ' ')       --  BILL_TO_ADDRESS_4
     || RPAD (NVL (hl_b.postal_code, ' '), 50, ' ') --  BILL_TO_ADDRESS_5
     REC_LINE
     , rcta.org_id --, hca.account_number, hca.cust_account_id , rcta.trx_number, apsa.AMOUNT_DUE_REMAINING
  FROM apps.ra_customer_trx rcta
     -- , ra_batch_sources_all rbsa
     , apps.hz_cust_accounts hca
     , ra_terms rt
     , ra_terms_lines_discounts rtld
     , apps.hz_cust_site_uses hcsu_s
     , apps.hz_cust_acct_sites hcas_s
     , hz_party_sites hps_s
     , hz_locations hl_s
     , apps.hz_cust_site_uses hcsu_b
     , apps.hz_cust_acct_sites hcas_b
     , hz_party_sites hps_b
     , hz_locations hl_b
     , apps.ar_payment_schedules apsa
 WHERE 1                        = 1
   AND rcta.bill_to_customer_id = hca.cust_account_id
   AND rcta.customer_trx_id     = apsa.customer_trx_id
   AND rcta.ship_to_site_use_id IS NULL
   AND hca.cust_account_id      = hcas_s.cust_account_id
   AND hcas_s.cust_acct_site_id = hcsu_s.cust_acct_site_id
   AND hcas_s.party_site_id     = hps_s.party_site_id
   AND hps_s.location_id        = hl_s.location_id
   AND hca.cust_account_id      = hcas_b.cust_account_id
   AND hcas_b.cust_acct_site_id = hcsu_b.cust_acct_site_id
   AND hcsu_s.site_use_code     = 'SHIP_TO'
   AND hcsu_s.primary_flag      = 'Y'
   AND hcsu_b.site_use_code     = 'BILL_TO'
   AND hcsu_b.primary_flag      = 'Y'
--   AND hcsu_b.site_use_id       = hcsu_s.bill_to_site_use_id
   AND hcas_b.party_site_id     = hps_b.party_site_id
   AND hps_b.location_id        = hl_b.location_id
   AND rcta.term_id             = rt.term_id (+)
   AND rt.term_id               = rtld.term_id (+)
--   AND NVL(rcta.interface_header_context, '&*^%$#@') <> 'CONVERSION'
   -- AND rcta.batch_source_id        = rbsa.batch_source_id
   -- AND rcta.org_id                 = rbsa.org_id
   AND NVL(xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id ), '&*^%$#@')   <> 'CONVERSION'
   AND apsa.status              = 'OP'
   AND apsa.amount_due_remaining != 0
   AND apsa.org_id              = rcta.org_id
UNION
------------------------------------------------------------------------------------------------------------------------------
-- CONVERSION
-- No Ship-To Customer on Invoice
------------------------------------------------------------------------------------------------------------------------------
SELECT RPAD(NVL(hca.account_number, ' '),20,' ')                                                      --  CUSTOMER_NUMBER
     || RPAD(NVL(rcta.trx_number, ' '),20,' ')                                                        --  TRANSACTION_NUMBER
     || RPAD(NVL(hcsu_s.location, ' '),35,' ')                                                        --  SHIP_TO_NAME
     || RPAD(NVL(hl_s.address1, ' '),50,' ')                                      --  SHIP_TO_ADDRESS_1
     || RPAD(NVL(TRIM(hl_s.address2), ' '),50,' ')                               --  SHIP_TO_ADDRESS_2
     || RPAD(NVL(hl_s.city, ' '),50,' ')                                                              --  SHIP_TO_ADDRESS_3
     || RPAD(NVL(hl_s.state, ' '),50,' ')                                                             --  SHIP_TO_ADDRESS_4
     || RPAD(NVL(hl_s.postal_code, ' '),50,' ')                                                       --  SHIP_TO_ADDRESS_5
     || RPAD(NVL(rcta.ct_reference, ' '),11,' ')                                                      --  ORDER_NUMBER
     || RPAD(NVL(TO_CHAR(gp_dmp.ordate,'MMDDYYYY') , ' '), 8, ' ')                                                       --  ORDER_DATE
     || RPAD(NVL(gp_dmp.warehse, '000'),35,' ')                                                          --  WAREHOUSE
     || RPAD(NVL(rt.description, ' '),20,' ')                                                         --  PAYMENT_TERMS
     || RPAD(NVL(gp_dmp.shipvia, '99:WILL CALL'),30,' ')                                               --  SHIP_VIA_NUMBER
     || RPAD(NVL(' ', ' '),15,' ')                                                                    --  ORDER_STATUS
     || RPAD(NVL(' ', ' '),5,' ')                                                                     --  ORDER_SHIPMENT_CARRIER
     || LPAD(NVL(TO_CHAR(apsa.discount_original,'9999999990.99'), '0.00'),20,' ')                     --  DISCOUNT_AMOUNT
     || LPAD(NVL(TO_CHAR(rtld.discount_days), '0'),3,' ')                                             --  DISCOUNT_DAYS
     || RPAD(NVL((SELECT jrdv.source_first_name||' '||jrdv.source_last_name
                   FROM jtf_rs_defresources_v jrdv
                      , apps.ra_salesreps      rsa
                  WHERE rcta.primary_salesrep_id = rsa.salesrep_id
                    AND rcta.org_id = rsa.org_id
                    AND rsa.person_id = jrdv.source_id)
        , ' '),10,' ')                                                                                --  SALES_MAN
     || RPAD(NVL(rcta.fob_point, ' '),30,' ')                                                         --  FOB_POINT
     || RPAD(NVL(TO_CHAR(rcta.trx_date,'MMDDYYYY'), ' '),8,' ')                                       --  SHIP_DATE
     || RPAD(NVL(hca.account_name, ' '),50,' ')                                                        --  BILL_TO_NAME
--     || xxwc_ar_getpaid_ob_pkg.get_bill_to_addr(rcta.org_id
--                                              , hca.cust_account_id
--                                              , hcsu_s.bill_to_site_use_id)                              --  BILL_TO_ADDRESS
     || RPAD (NVL (hl_b.address1, ' '), 50, ' ')    --  BILL_TO_ADDRESS_1
     || RPAD (NVL (TRIM (hl_b.address2), ' '), 50, ' ') --  BILL_TO_ADDRESS_2
     || RPAD (NVL (hl_b.city, ' '), 50, ' ')        --  BILL_TO_ADDRESS_3
     || RPAD (NVL (hl_b.state, ' '), 50, ' ')       --  BILL_TO_ADDRESS_4
     || RPAD (NVL (hl_b.postal_code, ' '), 50, ' ') --  BILL_TO_ADDRESS_5
     REC_LINE
     , rcta.org_id --, hca.account_number, hca.cust_account_id , rcta.trx_number, apsa.AMOUNT_DUE_REMAINING
  FROM apps.ra_customer_trx rcta
     -- , ra_batch_sources_all rbsa
     , apps.hz_cust_accounts hca
     , ra_terms rt
     , ra_terms_lines_discounts rtld
     , apps.hz_cust_site_uses hcsu_s
     , apps.hz_cust_acct_sites hcas_s
     , hz_party_sites hps_s
     , hz_locations hl_s
     , apps.hz_cust_site_uses hcsu_b
     , apps.hz_cust_acct_sites hcas_b
     , hz_party_sites hps_b
     , hz_locations hl_b
     , apps.ar_payment_schedules apsa
     , apps.xxwc_arexti_getpaid_dump_tbl gp_dmp
 WHERE 1 = 1
   AND rcta.bill_to_customer_id = hca.cust_account_id
   AND rcta.customer_trx_id =  apsa.customer_trx_id
   AND hca.cust_account_id      = hcas_s.cust_account_id
   AND rcta.ship_to_customer_id IS NULL
   AND hcas_s.cust_acct_site_id = hcsu_s.cust_acct_site_id
   AND hcas_s.party_site_id = hps_s.party_site_id
   AND hps_s.location_id = hl_s.location_id
   AND hca.cust_account_id = hcas_b.cust_account_id
   AND hcas_b.cust_acct_site_id = hcsu_b.cust_acct_site_id
   AND hcsu_s.site_use_code = 'SHIP_TO'
   AND hcsu_b.site_use_code = 'BILL_TO'
   AND hcsu_b.primary_flag = 'Y'
--   AND hcsu_b.site_use_id       = hcsu_s.bill_to_site_use_id
   AND hcsu_s.primary_flag = 'Y'
   AND hcas_b.party_site_id = hps_b.party_site_id
   AND hps_b.location_id = hl_b.location_id
   AND rcta.term_id = rt.term_id (+)
   AND rt.term_id = rtld.term_id (+)
   AND rcta.trx_number          = gp_dmp.invno
   AND hca.account_number       = gp_dmp.custno
   AND apsa.status = 'OP' AND apsa.amount_due_remaining != 0 AND apsa.org_id = rcta.org_id
--   AND NVL(rcta.interface_header_context, '&*^%$#@') = 'CONVERSION'
   -- AND rcta.batch_source_id        = rbsa.batch_source_id
   -- AND rcta.org_id                 = rbsa.org_id
   AND NVL(xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id ), '&*^%$#@')   = 'CONVERSION'
UNION
------------------------------------------------------------------------------------------------------------------------------
-- CONVERSION
-- No Ship-To Site for Customer
------------------------------------------------------------------------------------------------------------------------------
SELECT RPAD(NVL(hca.account_number, ' '),20,' ')                                                      --  CUSTOMER_NUMBER
     || RPAD(NVL(rcta.trx_number, ' '),20,' ')                                                        --  TRANSACTION_NUMBER
     || RPAD(NVL(gp_dmp.shipname, ' '),35,' ')                                                        --  SHIP_TO_NAME
     || RPAD(NVL(gp_dmp.shipadd1, ' '),50,' ')                                                        --  SHIP_TO_ADDRESS_1
     || RPAD(NVL(gp_dmp.shipadd2, ' '),50,' ')                                                        --  SHIP_TO_ADDRESS_2
     || RPAD(NVL(gp_dmp.shipadd3, ' '),50,' ')                                                        --  SHIP_TO_ADDRESS_3
     || RPAD(NVL(gp_dmp.shipadd4, ' '),50,' ')                                                        --  SHIP_TO_ADDRESS_4
     || RPAD(NVL(gp_dmp.shipadd5, ' '),50,' ')                                                        --  SHIP_TO_ADDRESS_5
     || RPAD(NVL(rcta.ct_reference, ' '),11,' ')                                                      --  ORDER_NUMBER
     || RPAD(NVL(TO_CHAR(gp_dmp.ordate,'MMDDYYYY') , ' '), 8, ' ')                                                       --  ORDER_DATE
     || RPAD(NVL(gp_dmp.warehse, '000'),35,' ')                                                       --  WAREHOUSE
     || RPAD(NVL(rt.description, ' '),20,' ')                                                         --  PAYMENT_TERMS
     || RPAD(NVL(gp_dmp.shipvia, '99:WILL CALL'),30,' ')                                              --  SHIP_VIA_NUMBER
     || RPAD(NVL(' ', ' '),15,' ')                                                                    --  ORDER_STATUS
     || RPAD(NVL(' ', ' '),5,' ')                                                                     --  ORDER_SHIPMENT_CARRIER
     || LPAD(NVL(TO_CHAR(apsa.discount_original,'9999999990.99'), '0.00'),20,' ')                     --  DISCOUNT_AMOUNT
     || LPAD(NVL(TO_CHAR(rtld.discount_days), '0'),3,' ')                                             --  DISCOUNT_DAYS
     || RPAD(NVL((SELECT jrdv.source_first_name||' '||jrdv.source_last_name
                   FROM jtf_rs_defresources_v jrdv
                      , apps.ra_salesreps      rsa
                  WHERE rcta.primary_salesrep_id = rsa.salesrep_id
                    AND rcta.org_id = rsa.org_id
                    AND rsa.person_id = jrdv.source_id)
        , ' '),10,' ')                                                                                --  SALES_MAN
     || RPAD(NVL(rcta.fob_point, ' '),30,' ')                                                         --  FOB_POINT
     || RPAD(NVL(TO_CHAR(rcta.trx_date,'MMDDYYYY'), ' '),8,' ')                                       --  SHIP_DATE
     || RPAD(NVL(hca.account_name, ' '),50,' ')                                                        --  BILL_TO_NAME
--     || xxwc_ar_getpaid_ob_pkg.get_bill_to_addr(rcta.org_id
--                                              , hca.cust_account_id
--                                              , NULL)                                                 --  BILL_TO_ADDRESS
     || RPAD (NVL (hl_b.address1, ' '), 50, ' ')    --  BILL_TO_ADDRESS_1
     || RPAD (NVL (TRIM (hl_b.address2), ' '), 50, ' ') --  BILL_TO_ADDRESS_2
     || RPAD (NVL (hl_b.city, ' '), 50, ' ')        --  BILL_TO_ADDRESS_3
     || RPAD (NVL (hl_b.state, ' '), 50, ' ')       --  BILL_TO_ADDRESS_4
     || RPAD (NVL (hl_b.postal_code, ' '), 50, ' ') --  BILL_TO_ADDRESS_5
     REC_LINE
     , rcta.org_id --, hca.account_number, hca.cust_account_id , rcta.trx_number, apsa.AMOUNT_DUE_REMAINING
  FROM apps.ra_customer_trx rcta
     -- , ra_batch_sources_all rbsa
     , apps.hz_cust_accounts hca
     , ra_terms rt
     , ra_terms_lines_discounts rtld
     , apps.hz_cust_site_uses hcsu_b
     , apps.hz_cust_acct_sites hcas_b
     , hz_party_sites hps_b
     , hz_locations hl_b
     , apps.ar_payment_schedules apsa 
     , apps.xxwc_arexti_getpaid_dump_tbl gp_dmp
 WHERE 1 = 1
   AND rcta.bill_to_customer_id = hca.cust_account_id
   AND rcta.customer_trx_id =  apsa.customer_trx_id
   AND rcta.ship_to_customer_id IS NULL
   AND NOT EXISTS (SELECT '1'
                     FROM apps.hz_cust_acct_sites hcas_s
                        , apps.hz_cust_site_uses  hcsu_s
                    WHERE 1 = 1
                      AND hcas_s.cust_account_id   = rcta.bill_to_customer_id
                      AND hcas_s.cust_acct_site_id = hcsu_s.cust_acct_site_id
                      AND hcas_s.org_id            = rcta.org_id
                      AND hcsu_s.site_use_code     = 'SHIP_TO')
   AND hca.cust_account_id = hcas_b.cust_account_id
   AND hcas_b.cust_acct_site_id = hcsu_b.cust_acct_site_id
   AND hcsu_b.site_use_code = 'BILL_TO'
   AND hcsu_b.primary_flag = 'Y'
   AND hcas_b.party_site_id = hps_b.party_site_id
   AND hps_b.location_id = hl_b.location_id
   AND rcta.term_id = rt.term_id (+)
   AND rt.term_id = rtld.term_id (+)
   AND rcta.trx_number          = gp_dmp.invno
   AND hca.account_number       = gp_dmp.custno
   AND apsa.status = 'OP' AND apsa.amount_due_remaining != 0 AND apsa.org_id = rcta.org_id
--   AND NVL(rcta.interface_header_context, '&*^%$#@') = 'CONVERSION'
   -- AND rcta.batch_source_id        = rbsa.batch_source_id
   -- AND rcta.org_id                 = rbsa.org_id
   AND NVL(xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id ), '&*^%$#@')   = 'CONVERSION'
UNION
------------------------------------------------------------------------------------------------------------------------------
-- CONVERSION
-- Cash Receipts
------------------------------------------------------------------------------------------------------------------------------
SELECT RPAD(NVL(hca.account_number, ' '),20,' ')                                                      --  CUSTOMER_NUMBER
     || RPAD(NVL(acra.receipt_number, ' '),20,' ')                                                    --  TRANSACTION_NUMBER
     || RPAD(' ',35,' ')                                                                              --  SHIP_TO_NAME
     || RPAD(' ',50,' ')                                                                              --  SHIP_TO_ADDRESS_1
     || RPAD(' ',50,' ')                                                                              --  SHIP_TO_ADDRESS_2
     || RPAD(' ',50,' ')                                                                              --  SHIP_TO_ADDRESS_3
     || RPAD(' ',50,' ')                                                                              --  SHIP_TO_ADDRESS_4
     || RPAD(' ',50,' ')                                                                              --  SHIP_TO_ADDRESS_5
     || NVL((SELECT RPAD(NVL(araa.application_ref_num, ' '),11,' ')                                   --  ORDER_NUMBER
               FROM apps.ar_receivable_applications araa
              WHERE araa.cash_receipt_id         = apsa.cash_receipt_id
                AND ROWNUM                      = 1
             ), RPAD('0', 11, ' '))
     || RPAD(NVL(TO_CHAR(acra.receipt_date,'MMDDYYYY'), ' '),8,' ')                                        --  ORDER_DATE
     || RPAD(NVL(arm.attribute2, '80'),35,' ')                                                        --  WAREHOUSE
     || RPAD(' ',20,' ')                                                                              --  PAYMENT_TERMS
     || RPAD(' ', 30,' ')                                                                             --  SHIP_VIA_NUMBER
     || RPAD(' ',15,' ')                                                                        --  ORDER_STATUS
     || RPAD(' ',5,' ')                                                                         --  ORDER_SHIPMENT_CARRIER
     || LPAD('0.00',20,' ')                                                                           --  DISCOUNT_AMOUNT
     || LPAD(' ',3,' ')                                                                               --  DISCOUNT_DAYS
     || RPAD(NVL((SELECT jrdv.source_first_name||' '||jrdv.source_last_name
                   FROM jtf_rs_defresources_v jrdv
                      , apps.ra_salesreps      rsa
                  WHERE hcsu_b.primary_salesrep_id = rsa.salesrep_id
                    AND acra.org_id = rsa.org_id
                    AND rsa.person_id = jrdv.source_id)
        , ' '),10,' ')                                                                                --  SALES_MAN
     || RPAD( ' ',30,' ')                                                                             --  FOB_POINT
     || RPAD(NVL(TO_CHAR(acra.receipt_date,'MMDDYYYY'), ' '),8,' ')                                   --  SHIP_DATE
     || RPAD(NVL(hca.account_name, ' '),50,' ')                                                        --  BILL_TO_NAME
     || RPAD(NVL(hl_b.address1, ' '),50,' ')                                      --  BILL_TO_ADDRESS_1
     || RPAD(NVL(TRIM(hl_b.address2), ' '),50,' ')                                --  BILL_TO_ADDRESS_2
     || RPAD(NVL(hl_b.city, ' '),50,' ')                                                              --  BILL_TO_ADDRESS_3
     || RPAD(NVL(hl_b.state, ' '),50,' ')                                                             --  BILL_TO_ADDRESS_4
     || RPAD(NVL(hl_b.postal_code, ' '),50,' ')                                                       --  BILL_TO_ADDRESS_5
     REC_LINE
    , acra.ORG_ID --, hca.account_number, hca.cust_account_id , acra.receipt_number, apsa.AMOUNT_DUE_REMAINING
  FROM apps.ar_cash_receipts     acra
     , apps.hz_cust_accounts     hca
     , apps.hz_cust_site_uses hcsu_b
     , apps.hz_cust_acct_sites hcas_b
     , hz_party_sites hps_b
     , hz_locations hl_b
     , apps.ar_payment_schedules apsa
     , ar_receipt_methods       arm
     , ar_receipt_classes       arc
--     , hr_locations_all         hla
     , apps.xxwc_armast_getpaid_dump_tbl dmp
 WHERE 1 = 1
   AND hcsu_b.site_use_id         = acra.customer_site_use_id
   AND hcas_b.cust_acct_site_id = hcsu_b.cust_acct_site_id
   AND hcsu_b.site_use_code     = 'BILL_TO'
   AND hcas_b.party_site_id     = hps_b.party_site_id
   AND hps_b.location_id        = hl_b.location_id
   AND acra.type                    = 'CASH'
   AND acra.cash_receipt_id         = apsa.cash_receipt_id
   AND hca.cust_account_id          = apsa.customer_id
   AND apsa.amount_due_remaining   <> 0
   AND acra.receipt_method_id       = arm.receipt_method_id
   AND arm.receipt_class_id         = arc.receipt_class_id
   AND arc.name                     = 'CONV - Receipt'
--   AND SUBSTR(hla.location_code, 1, INSTR(hla.location_code, '-') - 2) = NVL(arm.attribute2,'WCC')
   AND TO_CHAR(acra.receipt_number) = dmp.INVNO
   AND hca.cust_account_id          = acra.pay_from_customer
   AND hca.account_number           = dmp.custno
UNION
------------------------------------------------------------------------------------------------------------------------------
-- CONVERSION
-- Cash Receipts, Customer# on hca.attribute16
------------------------------------------------------------------------------------------------------------------------------
SELECT RPAD(NVL(hca.account_number, ' '),20,' ')                                                      --  CUSTOMER_NUMBER
     || RPAD(NVL(acra.receipt_number, ' '),20,' ')                                                    --  TRANSACTION_NUMBER
     || RPAD(' ',35,' ')                                                                              --  SHIP_TO_NAME
     || RPAD(' ',50,' ')                                                                              --  SHIP_TO_ADDRESS_1
     || RPAD(' ',50,' ')                                                                              --  SHIP_TO_ADDRESS_2
     || RPAD(' ',50,' ')                                                                              --  SHIP_TO_ADDRESS_3
     || RPAD(' ',50,' ')                                                                              --  SHIP_TO_ADDRESS_4
     || RPAD(' ',50,' ')                                                                              --  SHIP_TO_ADDRESS_5
     || NVL((SELECT RPAD(NVL(araa.application_ref_num, ' '),11,' ')                                   --  ORDER_NUMBER
               FROM apps.ar_receivable_applications araa
              WHERE araa.cash_receipt_id         = apsa.cash_receipt_id
                AND ROWNUM                      = 1
             ), RPAD('0', 11, ' '))
     || RPAD(NVL(TO_CHAR(acra.receipt_date,'MMDDYYYY'), ' '),8,' ')                                        --  ORDER_DATE
     || RPAD(NVL(arm.attribute2, '80'),35,' ')                                                        --  WAREHOUSE
     || RPAD(' ',20,' ')                                                                              --  PAYMENT_TERMS
     || RPAD(' ', 30,' ')                                                                             --  SHIP_VIA_NUMBER
     || RPAD(' ',15,' ')                                                                        --  ORDER_STATUS
     || RPAD(' ',5,' ')                                                                         --  ORDER_SHIPMENT_CARRIER
     || LPAD('0.00',20,' ')                                                                           --  DISCOUNT_AMOUNT
     || LPAD(' ',3,' ')                                                                               --  DISCOUNT_DAYS
     || RPAD(NVL((SELECT jrdv.source_first_name||' '||jrdv.source_last_name
                   FROM jtf_rs_defresources_v jrdv
                      , apps.ra_salesreps      rsa
                  WHERE hcsu_b.primary_salesrep_id = rsa.salesrep_id
                    AND acra.org_id = rsa.org_id
                    AND rsa.person_id = jrdv.source_id)
        , ' '),10,' ')                                                                                --  SALES_MAN
     || RPAD( ' ',30,' ')                                                                             --  FOB_POINT
     || RPAD(NVL(TO_CHAR(acra.receipt_date,'MMDDYYYY'), ' '),8,' ')                                   --  SHIP_DATE
     || RPAD(NVL(hca.account_name, ' '),50,' ')                                                        --  BILL_TO_NAME
     || RPAD(NVL(hl_b.address1, ' '),50,' ')                                      --  BILL_TO_ADDRESS_1
     || RPAD(NVL(TRIM(hl_b.address2), ' '),50,' ')                                --  BILL_TO_ADDRESS_2
     || RPAD(NVL(hl_b.city, ' '),50,' ')                                                              --  BILL_TO_ADDRESS_3
     || RPAD(NVL(hl_b.state, ' '),50,' ')                                                             --  BILL_TO_ADDRESS_4
     || RPAD(NVL(hl_b.postal_code, ' '),50,' ')                                                       --  BILL_TO_ADDRESS_5
     REC_LINE
    , acra.ORG_ID --, hca.account_number, hca.cust_account_id , acra.receipt_number, apsa.AMOUNT_DUE_REMAINING
  FROM apps.ar_cash_receipts     acra
     , apps.hz_cust_accounts     hca
     , apps.hz_cust_site_uses hcsu_b
     , apps.hz_cust_acct_sites hcas_b
     , hz_party_sites hps_b
     , hz_locations hl_b
     , apps.ar_payment_schedules apsa
     , ar_receipt_methods       arm
     , ar_receipt_classes       arc
--     , hr_locations_all         hla
     , apps.xxwc_armast_getpaid_dump_tbl dmp
 WHERE 1 = 1
   AND hcsu_b.site_use_id         = acra.customer_site_use_id
   AND hcas_b.cust_acct_site_id = hcsu_b.cust_acct_site_id
   AND hcsu_b.site_use_code     = 'BILL_TO'
   AND hcas_b.party_site_id     = hps_b.party_site_id
   AND hps_b.location_id        = hl_b.location_id
   AND acra.type                    = 'CASH'
   AND acra.cash_receipt_id         = apsa.cash_receipt_id
   AND hca.cust_account_id          = apsa.customer_id
   AND apsa.amount_due_remaining   <> 0
   AND acra.receipt_method_id       = arm.receipt_method_id
   AND arm.receipt_class_id         = arc.receipt_class_id
   AND arc.name                     = 'CONV - Receipt'
--   AND SUBSTR(hla.location_code, 1, INSTR(hla.location_code, '-') - 2) = NVL(arm.attribute2,'WCC')
   AND TO_CHAR(acra.receipt_number) = dmp.INVNO
   AND hca.cust_account_id          = acra.pay_from_customer
   AND hca.account_number           != dmp.custno
   AND hca.attribute6               = dmp.custno
UNION
------------------------------------------------------------------------------------------------------------------------------
-- NON-CONVERSION
-- Cash Receipts, ReceiptMethod != 'WC PRISM Lockbox'
------------------------------------------------------------------------------------------------------------------------------
SELECT RPAD(NVL(hca.account_number, ' '),20,' ')                                                      --  CUSTOMER_NUMBER
     || RPAD(NVL(acra.receipt_number, ' '),20,' ')                                                    --  TRANSACTION_NUMBER
     || RPAD(' ',35,' ')                                                                              --  SHIP_TO_NAME
     || RPAD(' ',50,' ')                                                                              --  SHIP_TO_ADDRESS_1
     || RPAD(' ',50,' ')                                                                              --  SHIP_TO_ADDRESS_2
     || RPAD(' ',50,' ')                                                                              --  SHIP_TO_ADDRESS_3
     || RPAD(' ',50,' ')                                                                              --  SHIP_TO_ADDRESS_4
     || RPAD(' ',50,' ')                                                                              --  SHIP_TO_ADDRESS_5
     || NVL((SELECT RPAD(NVL(araa.application_ref_num, ' '),11,' ')                                   --  ORDER_NUMBER
               FROM apps.ar_receivable_applications araa
              WHERE araa.cash_receipt_id         = apsa.cash_receipt_id
                AND ROWNUM                      = 1
             ), RPAD('0', 11, ' '))
     || RPAD(NVL(TO_CHAR(acra.receipt_date,'MMDDYYYY'), ' '),8,' ')                                        --  ORDER_DATE
     || RPAD(NVL(arm.attribute2, '80'),35,' ')                                                        --  WAREHOUSE
     || RPAD(' ',20,' ')                                                                              --  PAYMENT_TERMS
     || RPAD(' ', 30,' ')                                                                             --  SHIP_VIA_NUMBER
     || RPAD(' ',15,' ')                                                                        --  ORDER_STATUS
     || RPAD(' ',5,' ')                                                                         --  ORDER_SHIPMENT_CARRIER
     || LPAD('0.00',20,' ')                                                                           --  DISCOUNT_AMOUNT
     || LPAD(' ',3,' ')                                                                               --  DISCOUNT_DAYS
     || RPAD(NVL((SELECT jrdv.source_first_name||' '||jrdv.source_last_name
                   FROM jtf_rs_defresources_v jrdv
                      , apps.ra_salesreps      rsa
                  WHERE hcsu_b.primary_salesrep_id = rsa.salesrep_id
                    AND acra.org_id = rsa.org_id
                    AND rsa.person_id = jrdv.source_id)
        , ' '),10,' ')                                                                                --  SALES_MAN
     || RPAD( ' ',30,' ')                                                                             --  FOB_POINT
     || RPAD(NVL(TO_CHAR(acra.receipt_date,'MMDDYYYY'), ' '),8,' ')                                   --  SHIP_DATE
     || RPAD(NVL(hca.account_name, ' '),50,' ')                                                        --  BILL_TO_NAME
     || RPAD(NVL(hl_b.address1, ' '),50,' ')                                      --  BILL_TO_ADDRESS_1
     || RPAD(NVL(TRIM(hl_b.address2), ' '),50,' ')                                --  BILL_TO_ADDRESS_2
     || RPAD(NVL(hl_b.city, ' '),50,' ')                                                              --  BILL_TO_ADDRESS_3
     || RPAD(NVL(hl_b.state, ' '),50,' ')                                                             --  BILL_TO_ADDRESS_4
     || RPAD(NVL(hl_b.postal_code, ' '),50,' ')                                                       --  BILL_TO_ADDRESS_5
     REC_LINE
    , acra.ORG_ID --, hca.account_number, hca.cust_account_id , acra.receipt_number, apsa.AMOUNT_DUE_REMAINING
  FROM apps.ar_cash_receipts     acra
     , apps.hz_cust_accounts     hca
     , apps.hz_cust_site_uses hcsu_b
     , apps.hz_cust_acct_sites hcas_b
     , hz_party_sites hps_b
     , hz_locations hl_b
     , apps.ar_payment_schedules apsa
     , ar_receipt_methods       arm
     , ar_receipt_classes       arc
--     , hr_locations_all         hla
 WHERE 1 = 1
   AND hcsu_b.site_use_id         = acra.customer_site_use_id
   AND hcas_b.cust_acct_site_id = hcsu_b.cust_acct_site_id
   AND hcsu_b.site_use_code     = 'BILL_TO'
   AND hcas_b.party_site_id     = hps_b.party_site_id
   AND hps_b.location_id        = hl_b.location_id
   AND acra.type                    = 'CASH'
   AND acra.cash_receipt_id         = apsa.cash_receipt_id
   AND hca.cust_account_id          = apsa.customer_id
   AND apsa.amount_due_remaining   <> 0
   AND acra.receipt_method_id       = arm.receipt_method_id
   AND arm.receipt_class_id         = arc.receipt_class_id
   AND arc.name                     != 'CONV - Receipt'
   AND arm.name                     != 'WC PRISM Lockbox'
--   AND SUBSTR(hla.location_code, 1, INSTR(hla.location_code, '-') - 2) = NVL(arm.attribute2,'WCC')
   AND hca.cust_account_id          = acra.pay_from_customer
UNION
------------------------------------------------------------------------------------------------------------------------------
-- NON-CONVERSION
-- Cash Receipts, ReceiptMethod = 'WC PRISM Lockbox'
------------------------------------------------------------------------------------------------------------------------------
SELECT RPAD(NVL(hca.account_number, ' '),20,' ')                                                      --  CUSTOMER_NUMBER
     || RPAD(NVL(acra.receipt_number||'-P', ' '),20,' ')                                                    --  TRANSACTION_NUMBER
     || RPAD(' ',35,' ')                                                                              --  SHIP_TO_NAME
     || RPAD(' ',50,' ')                                                                              --  SHIP_TO_ADDRESS_1
     || RPAD(' ',50,' ')                                                                              --  SHIP_TO_ADDRESS_2
     || RPAD(' ',50,' ')                                                                              --  SHIP_TO_ADDRESS_3
     || RPAD(' ',50,' ')                                                                              --  SHIP_TO_ADDRESS_4
     || RPAD(' ',50,' ')                                                                              --  SHIP_TO_ADDRESS_5
     || NVL((SELECT RPAD(NVL(araa.application_ref_num, ' '),11,' ')                                   --  ORDER_NUMBER
               FROM apps.ar_receivable_applications araa
              WHERE araa.cash_receipt_id         = apsa.cash_receipt_id
                AND ROWNUM                      = 1
             ), RPAD('0', 11, ' '))
     || RPAD(NVL(TO_CHAR(acra.receipt_date,'MMDDYYYY'), ' '),8,' ')                                        --  ORDER_DATE
     || RPAD(NVL(arm.attribute2, '80'),35,' ')                                                        --  WAREHOUSE
     || RPAD(' ',20,' ')                                                                              --  PAYMENT_TERMS
     || RPAD(' ', 30,' ')                                                                             --  SHIP_VIA_NUMBER
     || RPAD(' ',15,' ')                                                                        --  ORDER_STATUS
     || RPAD(' ',5,' ')                                                                         --  ORDER_SHIPMENT_CARRIER
     || LPAD('0.00',20,' ')                                                                           --  DISCOUNT_AMOUNT
     || LPAD(' ',3,' ')                                                                               --  DISCOUNT_DAYS
     || RPAD(NVL((SELECT jrdv.source_first_name||' '||jrdv.source_last_name
                   FROM jtf_rs_defresources_v jrdv
                      , apps.ra_salesreps      rsa
                  WHERE hcsu_b.primary_salesrep_id = rsa.salesrep_id
                    AND acra.org_id = rsa.org_id
                    AND rsa.person_id = jrdv.source_id)
        , ' '),10,' ')                                                                                --  SALES_MAN
     || RPAD( ' ',30,' ')                                                                             --  FOB_POINT
     || RPAD(NVL(TO_CHAR(acra.receipt_date,'MMDDYYYY'), ' '),8,' ')                                   --  SHIP_DATE
     || RPAD(NVL(hca.account_name, ' '),50,' ')                                                        --  BILL_TO_NAME
     || RPAD(NVL(hl_b.address1, ' '),50,' ')                                      --  BILL_TO_ADDRESS_1
     || RPAD(NVL(TRIM(hl_b.address2), ' '),50,' ')                                --  BILL_TO_ADDRESS_2
     || RPAD(NVL(hl_b.city, ' '),50,' ')                                                              --  BILL_TO_ADDRESS_3
     || RPAD(NVL(hl_b.state, ' '),50,' ')                                                             --  BILL_TO_ADDRESS_4
     || RPAD(NVL(hl_b.postal_code, ' '),50,' ')                                                       --  BILL_TO_ADDRESS_5
     REC_LINE
    , acra.ORG_ID --, hca.account_number, hca.cust_account_id , acra.receipt_number, apsa.AMOUNT_DUE_REMAINING
  FROM apps.ar_cash_receipts     acra
     , apps.hz_cust_accounts     hca
     , apps.hz_cust_site_uses hcsu_b
     , apps.hz_cust_acct_sites hcas_b
     , hz_party_sites hps_b
     , hz_locations hl_b
     , apps.ar_payment_schedules apsa
     , ar_receipt_methods       arm
     , ar_receipt_classes       arc
--     , hr_locations_all         hla
 WHERE 1 = 1
   AND hcsu_b.site_use_id         = acra.customer_site_use_id
   AND hcas_b.cust_acct_site_id = hcsu_b.cust_acct_site_id
   AND hcsu_b.site_use_code     = 'BILL_TO'
   AND hcas_b.party_site_id     = hps_b.party_site_id
   AND hps_b.location_id        = hl_b.location_id
   AND acra.type                    = 'CASH'
   AND acra.cash_receipt_id         = apsa.cash_receipt_id
   AND hca.cust_account_id          = apsa.customer_id
   AND apsa.amount_due_remaining   <> 0
   AND acra.receipt_method_id       = arm.receipt_method_id
   AND arm.receipt_class_id         = arc.receipt_class_id
   AND arc.name                     != 'CONV - Receipt'
   AND arm.name                     = 'WC PRISM Lockbox'
--   AND SUBSTR(hla.location_code, 1, INSTR(hla.location_code, '-') - 2) = NVL(arm.attribute2,'WCC')
   AND hca.cust_account_id          = acra.pay_from_customer;
