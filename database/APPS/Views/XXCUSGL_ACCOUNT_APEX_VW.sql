
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUSGL_ACCOUNT_APEX_VW" ("ACCOUNT", "DESCRIPTION", "ACCOUNT_TYPE") AS 
  SELECT v.flex_value
      ,tl.description
      ,substr(v.compiled_value_attributes, 5, 1)
  FROM apps.fnd_flex_values        v
      ,applsys.fnd_flex_values_tl  tl
      ,applsys.fnd_flex_value_sets s
 WHERE flex_value_set_name = 'XXCUS_GL_ACCOUNT'
   AND v.flex_value_set_id = s.flex_value_set_id
   AND v.flex_value_id = tl.flex_value_id
   AND tl.language = 'US'
   AND substr(v.compiled_value_attributes, 3, 1) = 'Y'
;
