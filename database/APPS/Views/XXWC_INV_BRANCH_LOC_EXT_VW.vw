/* Formatted on 2012/06/21 15:47 (Formatter Plus v4.8.8) */
CREATE OR REPLACE FORCE VIEW apps.xxwc_inv_branch_loc_ext_vw (operating_unit_id,
                                                              interface_date,
                                                              branch_data
                                                             )
AS
   SELECT x1.operating_unit_id, SYSDATE interface_date,
          (   x1.business_unit
           || '|'
           || x1.source_system
           || '|'
           || x1.branch_id
           || '|'
           || x1.fru_number
           || '|'
           || x1.location_name
           || '|'
           || x1.alternate_name
           || '|'
           || x1.physaddrline1
           || '|'
           || x1.physaddrline2
           || '|'
           || x1.physaddrline3
           || '|'
           || x1.physaddrcity
           || '|'
           || x1.physaddrstate
           || '|'
           || x1.physaddrzip
           || '|'
           || x1.physaddrcountrycode
           || '|'
           || x1.mailaddr1
           || '|'
           || x1.mailaddr2
           || '|'
           || x1.mailaddr3
           || '|'
           || x1.mailaddrcity
           || '|'
           || x1.mailaddrstate
           || '|'
           || x1.mailaddrzip
           || '|'
           || x1.mailaddrcountrycode
           || '|'
           || x1.regionalmgrname
           || '|'
           || x1.region
           || '|'
           || x1.pricingregionname
           || '|'
           || x1.acquiredflag
           || '|'
           || x1.openflag
           || '|'
           || x1.phonenumber
           || '|'
           || x1.faxnumber
           || '|'
           || x1.customerservicephone
           || '|'
           || x1.hdsopendate
           || '|'
           || x1.hdscloseddate
           || '|'
           || TO_CHAR (x1.datemodified, 'MM/DD/YYYY')
           || '|'
           || x1.previousname
           || '|'
           || x1.branchoperationsmanager
           || '|'
           || x1.branchsalesmanager
           || '|'
           || x1.locationtype
          ) branch_data
     FROM xxwc_inv_branch_loc_vw x1;


