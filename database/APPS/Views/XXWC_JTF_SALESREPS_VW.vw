CREATE OR REPLACE FORCE VIEW APPS.XXWC_JTF_SALESREPS_VW
(
/******************************************************************************
   NAME:       APPS.XXWC_JTF_SALESREPS_VW
   PURPOSE:  This view is used to extract all the inventory Location Details

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        31/07/2012  Consuelo        Initial Version
   2.0        04/04/2015  Raghavendra S   TMS# 20150303-00075 - FIN - Addition of Code Snippet for Line Breaks in All EDW Extract Programs
                                            and Views for all free entry text fields.
   3.0        06/06/2016  Nancy Pahwa     TMS#20160303-00174 JTF_RS_SALESREPS.NAME is obsolete as from 11i onwards. Doc ID 550284.1
******************************************************************************/
   BUSINESS_UNIT,
   SOURCE_SYSTEM,
   OPERATING_UNIT_ID,
   OPERATING_UNIT_NAME,
   SALESREP_ID,
   EMPLOYEE_ID,
   RESOURCE_NUMBER,
   SALESREP_NUMBER,
   SALES_REP_NAME,
   SALES_REP_FIRST_NAME,
   SALES_REP_LAST_NAME,
   MANAGER_SALESREP_ID,
   SALES_REGION_CODE,
   SALES_REGION_NAME,
   SALES_REGION_MANAGER,
   SALES_DISTRICT_CODE,
   SALES_DISTRICT_NAME,
   SALES_DISTRICT_MANAGER,
   SALES_AREA_CODE,
   SALES_AREA_NAME,
   SALES_AREA_MANAGER,
   SALES_TERRITORY_CODE,
   SALES_TERRITORY_NAME,
   SALES_TERRITORY_MANAGER,
   BRANCH_LOCATION,
   NTL_ACCT_SALES_REP_FLAG,
   RESOURCE_ID,
   SALES_REP_CREATION_DATE,
   SALES_REP_LAST_UPDATE_DATE,
   REG_MGR_EMPID,
   REG_MGR_NTID,
   REG_MGR_NAME,
   DSTR_MGR_EMPID,
   DSTR_MGR_NTID,
   DSTR_MGR_NAME,
   NTL_MGR_EMPID,
   NTL_MGR_NTID,
   NTL_MGR_NAME,
   SALESREP_NTID,
   PAPF_ASSIGMENT_LOC,
   FRU_LOCATION
)
AS
SELECT 'WHITECAP' business_unit,
          'Oracle EBS' source_system,
          jrs.org_id operating_unit_id,
          hou.name operating_unit_name,
          jrs.salesrep_id,
          papf.person_id employee_id,
          -- jrre.resource_numbe  resource_number, -- commented for V 2.0
          -- jrs.salesrep_number,  '[[:cntrl:]]', ' ') resource_number,  -- commented forV 2.0
          -- NVL (NVL (jrs.name, papf.full_name), jrre_t.resource_name)  sales_rep_name,-- commented forV 2.0
          -- NVL (jrre.source_first_name, papf.first_name) sales_rep_first_name, -- commented for V 2.0
          -- NVL (jrre.source_last_name, papf.last_name) sales_rep_last_name, -- 06/03/2012 CG -- commented forV 2.0
          -- xds.nationalmanager_hremployeeid manager_salesrep_id,
          -- 06/06/2012 CG: Changed to pull the manager sales rep id
          -- jrre.source_mgr_id manager_salesrep_id,
          REGEXP_REPLACE(jrre.resource_number,  '[[:cntrl:]]', ' ') resource_number, -- Added forV 2.0
          REGEXP_REPLACE(jrs.salesrep_number,  '[[:cntrl:]]', ' ') resource_number,  -- Added forV 2.0
          -- v 3.0 Start
          /*REGEXP_REPLACE(NVL (NVL (jrs.name, papf.full_name), jrre_t.resource_name),  '[[:cntrl:]]', ' ')
             sales_rep_name,-- Added forV 2.0*/
           REGEXP_REPLACE(NVL (NVL (jrre_t.resource_name, papf.full_name), jrs.name),  '[[:cntrl:]]', ' ')
             sales_rep_name, -- v 3.0 End
          REGEXP_REPLACE(NVL (jrre.source_first_name, papf.first_name),  '[[:cntrl:]]', ' ') sales_rep_first_name, -- Added forV 2.0
          REGEXP_REPLACE(NVL (jrre.source_last_name, papf.last_name),  '[[:cntrl:]]', ' ') sales_rep_last_name, -- 06/03/2012 CG -- Added forV 2.0
          -- xds.nationalmanager_hremployeeid manager_salesrep_id,
          -- 06/06/2012 CG: Changed to pull the manager sales rep id
          -- jrre.source_mgr_id manager_salesrep_id,
          SUBSTR (
             xxwc_mv_routines_pkg.get_source_mgr_rep_num (jrre.source_mgr_id),
             1,
             30)  manager_salesrep_id,
          NULL sales_region_code,            -- 06/03/2012 CG: Per 05/15 Email
          NULL sales_region_name,            -- 06/03/2012 CG: Per 05/15 Email
          -- 06/03/2012 CG
          -- xxwc_mv_routines_pkg.get_salesrep_info (xds.regionalmanager_hremployeeid) sales_region_manager,
          -- 06/06/2012 CG Changed to pull the source manager for the sales area manager
          -- NVL (jrre_reg_mgr.source_mgr_name, jrre_hou_mgr.source_mgr_name), -- commented forV 2.0
          REGEXP_REPLACE(NVL (jrre_reg_mgr.source_mgr_name, jrre_hou_mgr.source_mgr_name),   '[[:cntrl:]]', ' ')
             sales_region_manager, -- Added forV 2.0
          NULL sales_district_code,          -- 06/03/2012 CG: Per 05/15 Email
          NULL sales_district_name,          -- 06/03/2012 CG: Per 05/15 Email
          /*SUBSTR (
             xxwc_mv_routines_pkg.get_salesrep_info (
                NVL (xds.districtmanager_hremployeeid,
                     xdh.districtmanager_hremployeeid)),
             1,
             240) sales_district_manager,*/ -- commented for V 2.0
          REGEXP_REPLACE(SUBSTR (
             xxwc_mv_routines_pkg.get_salesrep_info (
                NVL (xds.districtmanager_hremployeeid,
                     xdh.districtmanager_hremployeeid)),
             1,
             240),   '[[:cntrl:]]', ' ')
             sales_district_manager, -- Added forV 2.0
          NULL sales_area_code,              -- 06/03/2012 CG: Per 05/15 Email
          NULL sales_area_name,              -- 06/03/2012 CG: Per 05/15 Email
          -- 06/03/2012 CG
          -- xxwc_mv_routines_pkg.get_salesrep_info (xds.nationalmanager_hremployeeid)sales_area_manager,
           --03/07/2014:Harsha--TMS 20140306-00133  --EDW - Incorrect RVP Reporting for DMs
          /*SUBSTR (
             xxwc_mv_routines_pkg.get_salesrep_areamgr_info (
                NVL (xds.regionalmanager_hremployeeid,
                     xdh.regionalmanager_hremployeeid)),
             1,
             240) sales_area_manager,   */        -- commented for V 2.0
          REGEXP_REPLACE(SUBSTR (
             xxwc_mv_routines_pkg.get_salesrep_areamgr_info (
                NVL (xds.regionalmanager_hremployeeid,
                     xdh.regionalmanager_hremployeeid)),
             1,
             240),   '[[:cntrl:]]', ' ')
             sales_area_manager,           -- Added forV 2.0
              --03/07/2014:Harsha--TMS 20140306-00133  --EDW - Incorrect RVP Reporting for DMs
          NULL sales_territory_code,         -- 06/03/2012 CG: Per 05/15 Email
          NULL sales_territory_name,         -- 06/03/2012 CG: Per 05/15 Email
          NULL sales_territory_manager,      -- 06/03/2012 CG: Per 05/15 Email
          -- 06/06/2012 CG: Changed to pull the employee FRU instead of the branch
          /*SUBSTR
                (xxwc_mv_routines_pkg.get_salesrep_branch (papf.attribute1),
                1,
               3
             ) branch_location,*/
          -- papf.attribute1  branch_location, -- commented for V 2.0
          REGEXP_REPLACE(papf.attribute1,   '[[:cntrl:]]', ' ')  branch_location, -- Added forV 2.0
          NULL ntl_acct_sales_rep_flag,
          jrre.resource_id,
          jrre.creation_date sales_rep_creation_date,
          jrre.last_update_date sales_rep_last_update_date,
          NVL (xds.regionalmanager_hremployeeid,
               xdh.regionalmanager_hremployeeid)
             reg_mgr_empid,
          NVL (xds.regionalmanager_ntid, xdh.regionalmanager_ntid)
             reg_mgr_ntid,
          /*SUBSTR (
             xxwc_mv_routines_pkg.get_salesrep_info (
                NVL (xds.regionalmanager_hremployeeid,
                     xds.regionalmanager_hremployeeid)),
             1,
             240) reg_mgr_name,*/ -- Commented forV 2.0
          REGEXP_REPLACE(SUBSTR (
             xxwc_mv_routines_pkg.get_salesrep_info (
                NVL (xds.regionalmanager_hremployeeid,
                     xds.regionalmanager_hremployeeid)),
             1,
             240),   '[[:cntrl:]]', ' ')
             reg_mgr_name, -- Added forV 2.0
          NVL (xds.districtmanager_hremployeeid,
               xdh.districtmanager_hremployeeid)
             dstr_mgr_empid,
          NVL (xds.districtmanager_ntid, xdh.districtmanager_ntid)
             dstr_mgr_ntid,
          /* SUBSTR (
             xxwc_mv_routines_pkg.get_salesrep_info (
                NVL (xds.districtmanager_hremployeeid,
                     xdh.districtmanager_hremployeeid)),
             1,
             240) dstr_mgr_name,*/ -- commented for V 2.0
          REGEXP_REPLACE(SUBSTR (
             xxwc_mv_routines_pkg.get_salesrep_info (
                NVL (xds.districtmanager_hremployeeid,
                     xdh.districtmanager_hremployeeid)),
             1,
             240),   '[[:cntrl:]]', ' ')
             dstr_mgr_name, -- Added for V 2.0
          NVL (xds.nationalmanager_hremployeeid,
               xdh.nationalmanager_hremployeeid)
             ntl_mgr_empid,
          NVL (xds.nationalmanager_ntid, xdh.nationalmanager_ntid)
             ntl_mgr_ntid,
          /*SUBSTR (
             xxwc_mv_routines_pkg.get_salesrep_info (
                NVL (xds.nationalmanager_hremployeeid,
                     xdh.nationalmanager_hremployeeid)),
             1,
             240) ntl_mgr_name,*/ -- commented for V 2.0
          REGEXP_REPLACE(SUBSTR (
             xxwc_mv_routines_pkg.get_salesrep_info (
                NVL (xds.nationalmanager_hremployeeid,
                     xdh.nationalmanager_hremployeeid)),
             1,
             240),   '[[:cntrl:]]', ' ')
             ntl_mgr_name, -- Added forV 2.0
          xds.ntid salesrep_ntid -- 01/30/13 CG: TMS 20130122-01578 Retrieve employees branch location
                                ,
          SUBSTR (
             XXWC_MV_ROUTINES_PKG.GET_EMP_INV_BRANCH_LOC (papf.person_id),
             1,
             3)
             papf_assigment_loc,
          /*SUBSTR (xxwc_mv_routines_pkg.get_salesrep_branch (papf.attribute1),
                  1,
                  3) FRU_Location */ -- commented for V 2.0
          REGEXP_REPLACE(SUBSTR (xxwc_mv_routines_pkg.get_salesrep_branch (papf.attribute1),
                  1,
                  3),   '[[:cntrl:]]', ' ')
             FRU_Location -- Added for V 2.0
     FROM jtf_rs_salesreps jrs,
          jtf_rs_resource_extns jrre,
          jtf_rs_resource_extns_tl jrre_t,
          per_all_people_f papf,
          hr_operating_units hou,
          xxwc.dw_salesreps xds,
          jtf_rs_salesreps reg_mgr,
          jtf_rs_resource_extns jrre_reg_mgr,                    -- 07/11/2012
          xxwc.dw_houseaccts xdh,
          jtf_rs_salesreps hou_mgr,
          jtf_rs_resource_extns jrre_hou_mgr
    WHERE     jrs.resource_id = jrre.resource_id(+)
          AND jrre.resource_id = jrre_t.resource_id(+)
          AND jrre.category = jrre_t.category(+)
          AND jrs.person_id = papf.person_id(+)
          AND TRUNC (papf.effective_start_date(+)) <= TRUNC (SYSDATE)
          AND NVL (TRUNC (papf.effective_end_date(+)), TRUNC (SYSDATE)) >=
                 TRUNC (SYSDATE)
          AND jrs.org_id = hou.organization_id
          AND TO_NUMBER (jrs.salesrep_number) =
                 TO_NUMBER (xds.hremployeeid(+))                 --03/07/2014:Harsha--TMS 20140306-00133  --EDW - Incorrect RVP Reporting for DMs
          AND TO_NUMBER (xds.regionalmanager_hremployeeid) =
                 TO_NUMBER (reg_mgr.salesrep_number(+))           --03/07/2014:Harsha--TMS 20140306-00133  --EDW - Incorrect RVP Reporting for DMs
          AND reg_mgr.resource_id = jrre_reg_mgr.resource_id(+)
          AND jrs.salesrep_number = xdh.salesrepnumber(+)
          AND xdh.regionalmanager_hremployeeid = hou_mgr.salesrep_number(+)
          AND hou_mgr.resource_id = jrre_hou_mgr.resource_id(+)
   UNION
   SELECT DISTINCT
          'WHITECAP' business_unit,
          'Oracle EBS' source_system,
          162 operating_unit_id       -- hou.organization_id operating_unit_id
                               ,
          'HDS White Cap Org' operating_unit_name -- hou.name operating_unit_name
                                                 ,
          fu.user_id,
          papf.person_id employee_id,
          REGEXP_REPLACE(papf.employee_number,   '[[:cntrl:]]', ' ') employee_number, -- Added forV 2.0
          REGEXP_REPLACE(fu.user_name,   '[[:cntrl:]]', ' ') user_name, -- Added forV 2.0
          REGEXP_REPLACE(papf.full_name,   '[[:cntrl:]]', ' ') full_name, -- Added forV 2.0
          REGEXP_REPLACE(papf.first_name,   '[[:cntrl:]]', ' ') first_name, -- Added forV 2.0
          REGEXP_REPLACE(papf.last_name,   '[[:cntrl:]]', ' ') last_name, -- Added forV 2.0
          NULL manager_salesrep_id,
          NULL sales_region_code,
          NULL sales_region_name,
          NULL sales_region_manager,
          NULL sales_district_code,
          NULL sales_district_name,
          NULL sales_district_manager,
          NULL sales_area_code,
          NULL sales_area_name,
          NULL sales_area_manager,
          NULL sales_territory_code,
          NULL sales_territory_name,
          NULL sales_territory_manager,
          NULL branch_location,
          NULL ntl_acct_sales_rep_flag,
          NULL resource_id,
          TO_DATE ('01/01/1951', 'MM/DD/YYYY') sales_rep_creation_date,
          TO_DATE ('01/01/1951', 'MM/DD/YYYY') sales_rep_last_update_date,
          NULL reg_mgr_empid,
          NULL reg_mgr_ntid,
          NULL reg_mgr_name,
          NULL dstr_mgr_empid,
          NULL dstr_mgr_ntid,
          NULL dstr_mgr_name,
          NULL ntl_mgr_empid,
          NULL ntl_mgr_ntid,
          NULL ntl_mgr_name,
          NULL salesrep_ntid -- 01/30/13 CG: TMS 20130122-01578 Retrieve employees branch location
                            ,
          NULL papf_assigment_loc,
          NULL FRU_Location
     FROM fnd_user fu, per_all_people_f papf
    --, fnd_user_resp_groups_direct furgd
    --, fnd_profile_option_values fpov
    --, hr_operating_units hou
    WHERE     employee_id = papf.person_id(+)
          AND TRUNC (papf.effective_start_date(+)) <= TRUNC (SYSDATE)
          AND NVL (TRUNC (papf.effective_end_date(+)), TRUNC (SYSDATE)) >=
                 TRUNC (SYSDATE)
          -- 01/30/13 CG: TMS 20130122-01578 Remove XXWC_REF% usernames
          AND fu.user_name NOT LIKE 'XXWC_REF%';
