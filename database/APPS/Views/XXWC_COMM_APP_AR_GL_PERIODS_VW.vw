/* Formatted on 11/1/2012 4:51:07 PM (QP5 v5.115.810.9015) */
--
-- XXWC_COMM_APP_AR_GL_PERIODS_VW  (View)
--

CREATE OR REPLACE FORCE VIEW apps.xxwc_comm_app_ar_gl_periods_vw (
   operating_unit_id
 , operating_unit_name
 , period_year
 , period_name
 , start_date
 , end_date
 , period_num
 , quarter_num
 , closing_status
)
AS
   SELECT	hou.organization_id operating_unit_id
		  , hou.name operating_unit_name
		  , gps.period_year
		  , gps.period_name
		  , gps.start_date
		  , gps.end_date
		  , gps.period_num
		  , gps.quarter_num
		  , DECODE (gps.closing_status,
					'O', 'Open',
					'C', 'Closed',
					'N', 'Not Opened',
					'F', 'Future',
					gps.closing_status)
			   closing_status
	 FROM	gl_period_statuses gps
		  , fnd_application fa
		  , hr_operating_units hou
	WHERE		gps.application_id = fa.application_id
			AND fa.application_short_name = 'AR'
			AND gps.set_of_books_id = hou.set_of_books_id;


--GRANT SELECT ON APPS.XXWC_COMM_APP_AR_GL_PERIODS_VW TO INTERFACE_APEXWC;

