--
-- XXWC_SV_ITEM_CATEGORIES_V  (View)
--

CREATE OR REPLACE FORCE VIEW APPS.XXWC_SV_ITEM_CATEGORIES_V
(
   CATEGORY_SET_ID
  ,CATEGORY_SET_NAME
  ,CATEGORY_CONCAT_SEGMENTS
  ,CATEGORY_ID
  ,ORGANIZATION_ID
  ,INVENTORY_ITEM_ID
  ,SEGMENT1
)
AS
   SELECT mcs.Category_Set_ID
         ,mcs.Category_Set_Name
         ,mcsvc.CATEGORY_CONCAT_SEGMENTS
         ,mic.Category_ID
         ,mic.Organization_Id
         ,mic.Inventory_Item_ID
         ,msi.Segment1
     FROM mtl_Category_sets mcs
         ,MTL_CATEGORY_SET_VALID_CATS_V mcsvc
         ,MTL_ITEM_CATEGORIES mic
         ,mtl_System_Items_b msi
         ,mtl_Parameters mp
    WHERE     mcs.Category_Set_Name IS NOT NULL
          AND mcsvc.Category_Set_ID = mcs.Category_Set_ID
          AND mic.Category_Set_ID = mcsvc.Category_Set_ID
          AND mic.Category_ID = mcsvc.Category_ID
          AND msi.Inventory_Item_ID = mic.Inventory_Item_ID
          AND msi.Organization_ID = mic.Organization_ID
          AND msi.Organization_ID = mp.Organization_ID
          AND mic.Organization_Id IS NOT NULL
          AND mic.Organization_Id <> mp.Master_Organization_ID;


