/* Formatted on 3/26/2013 9:27:57 PM (QP5 v5.115.810.9015) */
--
-- XXWC_AR_CUSTOMER_CONTACT_VW	(View)
--

CREATE OR REPLACE FORCE VIEW apps.xxwc_ar_customer_contact_vw (
   cari
 , party_id
 , cust_account_id
 , cust_acct_site_id
 , location_id
 , contact_level
 , level_value
 , current_role_state
 , primary_flag
 , role_type
 , status
 , contact_number
 , party_name
 , person_first_name
 , person_middle_name
 , person_last_name
 , ps_reference
 , sub_party_id
 , party_type
 , relationship_id
 , owner_table_name
 , owner_table_id
 , rn
 , has_primary_bill_to
)
AS
   WITH contact_count
		  AS (	SELECT	 owner_table_name
					   , owner_table_id
					   , rn
					   , MAX (rec_count) rec_count2
				  FROM	 (SELECT   owner_table_name
								 , owner_table_id
								 , contact_type
								 , ROW_NUMBER ()
									  OVER (
										 PARTITION BY owner_table_name
													, owner_table_id
													, contact_type
										 ORDER BY
											owner_table_name
										  , owner_table_id
										  , contact_type
									  )
									  rn
								 , COUNT(contact_point_id)
									  OVER (
										 PARTITION BY owner_table_name
													, owner_table_id
													, contact_type
									  )
									  rec_count
							FROM   (SELECT	 hzpuicontactpointphoneeo.contact_point_id
										   , hzpuicontactpointphoneeo.contact_point_type
										   , hzpuicontactpointphoneeo.status
										   , hzpuicontactpointphoneeo.owner_table_name
										   , hzpuicontactpointphoneeo.owner_table_id
										   , hzpuicontactpointphoneeo.primary_flag
										   , NVL (
												hzpuicontactpointphoneeo.phone_line_type
											  , hzpuicontactpointphoneeo.contact_point_type
											 )
												contact_type
										   , hzpuicontactpointphoneeo.raw_phone_number
										   , hzpuicontactpointphoneeo.email_address
										   , hzpuicontactpointphoneeo.object_version_number
									  FROM	 hz_contact_points hzpuicontactpointphoneeo
									 WHERE	 hzpuicontactpointphoneeo.contact_point_type IN
												   ('PHONE', 'EMAIL')
											 AND hzpuicontactpointphoneeo.status =
												   'A') qrslt)
			  GROUP BY	 owner_table_name, owner_table_id, rn)
   SELECT	qrslt."CARI"
		  , qrslt."PARTY_ID"
		  , qrslt."CUST_ACCOUNT_ID"
		  , qrslt."CUST_ACCT_SITE_ID"
		  , qrslt."LOCATION_ID"
		  , qrslt."CONTACT_LEVEL"
		  , qrslt."LEVEL_VALUE"
		  , qrslt."CURRENT_ROLE_STATE"
		  , qrslt."PRIMARY_FLAG"
		  , qrslt."ROLE_TYPE"
		  , qrslt."STATUS"
		  , qrslt."CONTACT_NUMBER"
		  , qrslt."PARTY_NAME"
		  , qrslt."PERSON_FIRST_NAME"
		  , qrslt."PERSON_MIDDLE_NAME"
		  , qrslt."PERSON_LAST_NAME"
		  , qrslt."PS_REFERENCE"
		  , qrslt."SUB_PARTY_ID"
		  , qrslt."PARTY_TYPE"
		  , qrslt."RELATIONSHIP_ID"
		  , contact_count.owner_table_name
		  , contact_count.owner_table_id
		  , contact_count.rn
          , (case when qrslt."CONTACT_LEVEL" = 'ACCOUNT SITE' 
                then nvl((select hcsua.primary_flag 
                            from hz_cust_site_uses hcsua 
                           where hcsua.cust_acct_site_id = qrslt."CUST_ACCT_SITE_ID"
                           and   hcsua.status = 'A'
                           and   hcsua.site_use_code = 'BILL_TO'
                           and   primary_flag = 'Y'
                           and   rownum = 1), 'N')
             else 'N'
             end) has_primary_bill_to
	 FROM	   (SELECT	 hcar.cust_account_role_id AS cari
					   , hcar.party_id
					   , hcar.cust_account_id
					   , hcar.cust_acct_site_id
					   , hps.location_id
					   , (CASE
							 WHEN hcar.cust_acct_site_id IS NOT NULL
							 THEN
								'ACCOUNT SITE'
							 ELSE
								'ACCOUNT'
						  END)
							contact_level
					   , (CASE
							 WHEN hcar.cust_acct_site_id IS NOT NULL
							 THEN
								hcasa_hps.party_site_number
							 ELSE
								hca.account_number
						  END)
							level_value
					   , hcar.current_role_state
					   , hcar.primary_flag
					   , hcar.role_type
					   , hcar.status
					   , hoc.contact_number
					   , hpsub.party_name
					   , hpsub.person_first_name
					   , hpsub.person_middle_name
					   , hpsub.person_last_name
					   , hps.orig_system_reference AS ps_reference
					   , hpsub.party_id AS sub_party_id
					   , hpsub.party_type
					   , hr.relationship_id
				  FROM	 hz_cust_account_roles hcar
					   , hz_parties hpsub
					   , hz_parties hprel
					   , hz_org_contacts hoc
					   , hz_relationships hr
					   , hz_party_sites hps
					   , hz_cust_accounts hca
					   , hz_cust_acct_sites hcasa
					   , hz_party_sites hcasa_hps
				 WHERE		 hcar.role_type = 'CONTACT'
						 AND hcar.status = 'A'
						 AND hcar.party_id = hr.party_id
						 AND hr.party_id = hprel.party_id
						 AND hr.subject_id = hpsub.party_id
						 AND hoc.party_relationship_id = hr.relationship_id
						 AND hr.directional_flag = 'F'
						 AND hps.party_id(+) = hprel.party_id
						 AND NVL (hps.identifying_address_flag, 'Y') = 'Y'
						 AND NVL (hps.status, 'A') = 'A'
						 AND hcar.cust_account_id = hca.cust_account_id
						 AND hcar.cust_acct_site_id =
							   hcasa.cust_acct_site_id(+)
						 AND hcasa.party_site_id = hcasa_hps.party_site_id(+) --AND hcar.cust_account_id = 84731
																			 )
			   qrslt
			JOIN
			   contact_count
			ON contact_count.owner_table_name = 'HZ_PARTIES'
			   AND contact_count.owner_table_id = qrslt.party_id
   UNION
   select  act."CARI"
             , act."PARTY_ID"
              , act."CUST_ACCOUNT_ID"
              , act."CUST_ACCT_SITE_ID"
              , act."LOCATION_ID"
              , act."CONTACT_LEVEL"
              , act."LEVEL_VALUE"
              , act."CURRENT_ROLE_STATE"
              , act."PRIMARY_FLAG"
              , act."ROLE_TYPE"
              , act."STATUS"
              , act."CONTACT_NUMBER"
              , act."PARTY_NAME"
              , act."PERSON_FIRST_NAME"
              , act."PERSON_MIDDLE_NAME"
              , act."PERSON_LAST_NAME"
              , act."PS_REFERENCE"
              , act."SUB_PARTY_ID"
              , act."PARTY_TYPE"
              , act."RELATIONSHIP_ID"
            , contact_count.owner_table_name
            , contact_count.owner_table_id
            , contact_count.rn
            , (case when act."CONTACT_LEVEL" = 'ACCOUNT SITE' 
                then NVL( (select hcsua.primary_flag 
                            from hz_cust_site_uses hcsua 
                           where hcsua.cust_acct_site_id = act."CUST_ACCT_SITE_ID"
                           and   hcsua.status = 'A'
                           and   hcsua.site_use_code = 'BILL_TO'
                           and   primary_flag = 'Y'
                           and   rownum = 1), 'N')
             else 'N'
             end) has_primary_bill_to
    from    (                 
                select  hca.cust_account_id as CARI
                        , hp.party_id
                        , hca.cust_account_id
                        , null cust_acct_site_id
                        , null location_id
                        , 'PARTY' contact_level
                        , hca.account_number level_value
                        , null current_role_state
                        , null primary_flag
                        , null role_type
                        , null status
                        , null contact_number
                        , hp.party_name
                        , null person_first_name
                        , null person_middle_name
                        , null person_last_name
                        , hca.orig_system_reference ps_reference
                        , hp.party_id sub_party_id
                        , null party_type
                        , null relationship_id
                from    hz_cust_accounts hca
                        , hz_parties hp
                where   hca.party_id = hp.party_id
                and     exists (select 'Has party contact'
                                from    hz_contact_points hcp
                                where   hcp.contact_point_type in ('PHONE','EMAIL')
                                and     hcp.status = 'A'
                                and     hcp.owner_table_name = 'HZ_PARTIES'
                                and     hcp.owner_table_id = hp.party_id)
            ) act
    JOIN   contact_count ON contact_count.owner_table_name = 'HZ_PARTIES' 
                            and contact_count.owner_table_id = act.party_id                 
    UNION
    select  act_site."CARI"
             , act_site."PARTY_ID"
              , act_site."CUST_ACCOUNT_ID"
              , act_site."CUST_ACCT_SITE_ID"
              , act_site."LOCATION_ID"
              , act_site."CONTACT_LEVEL"
              , act_site."LEVEL_VALUE"
              , act_site."CURRENT_ROLE_STATE"
              , act_site."PRIMARY_FLAG"
              , act_site."ROLE_TYPE"
              , act_site."STATUS"
              , act_site."CONTACT_NUMBER"
              , act_site."PARTY_NAME"
              , act_site."PERSON_FIRST_NAME"
              , act_site."PERSON_MIDDLE_NAME"
              , act_site."PERSON_LAST_NAME"
              , act_site."PS_REFERENCE"
              , act_site."SUB_PARTY_ID"
              , act_site."PARTY_TYPE"
              , act_site."RELATIONSHIP_ID"
            , contact_count.owner_table_name
            , contact_count.owner_table_id
            , contact_count.rn
            , (case when act_site."CONTACT_LEVEL" = 'ACCOUNT SITE' 
                then NVL( (select hcsua.primary_flag 
                            from hz_cust_site_uses hcsua 
                           where hcsua.cust_acct_site_id = act_site."CUST_ACCT_SITE_ID"
                           and   hcsua.status = 'A'
                           and   hcsua.site_use_code = 'BILL_TO'
                           and   primary_flag = 'Y'
                           and   rownum = 1), 'N')
             else 'N'
             end) has_primary_bill_to
    from    (                 
                select  hca.cust_account_id as CARI
                        , hps.party_site_id party_id
                        , hca.cust_account_id
                        , hcasa.cust_acct_site_id
                        , hps.location_id
                        , 'ACCOUNT SITE' contact_level
                        , hps.party_site_number level_value
                        , null current_role_state
                        , null primary_flag
                        , null role_type
                        , null status
                        , null contact_number
                        , hp.party_name
                        , null person_first_name
                        , null person_middle_name
                        , null person_last_name
                        , hca.orig_system_reference ps_reference
                        , hp.party_id sub_party_id
                        , null party_type
                        , null relationship_id
                from    hz_cust_accounts hca
                        , hz_parties hp
                        , apps.hz_cust_acct_sites hcasa
                        , hz_party_sites hps
                where   hca.party_id = hp.party_id
                and     hca.cust_account_id = hcasa.cust_account_id
                and     hcasa.party_site_id = hps.party_site_id
                and     exists (select 'Has party contact'
                                from    hz_contact_points hcp
                                where   hcp.contact_point_type in ('PHONE','EMAIL')
                                and     hcp.status = 'A'
                                and     hcp.owner_table_name = 'HZ_PARTY_SITES'
                                and     hcp.owner_table_id = hps.party_site_id)
            ) act_site                       
    JOIN   contact_count ON contact_count.owner_table_name = 'HZ_PARTY_SITES'
                            and contact_count.owner_table_id = act_site.party_id 
   UNION
   SELECT	qrslt."CARI"
		  , qrslt."PARTY_ID"
		  , qrslt."CUST_ACCOUNT_ID"
		  , qrslt."CUST_ACCT_SITE_ID"
		  , qrslt."LOCATION_ID"
		  , qrslt."CONTACT_LEVEL"
		  , qrslt."LEVEL_VALUE"
		  , qrslt."CURRENT_ROLE_STATE"
		  , qrslt."PRIMARY_FLAG"
		  , qrslt."ROLE_TYPE"
		  , qrslt."STATUS"
		  , qrslt."CONTACT_NUMBER"
		  , qrslt."PARTY_NAME"
		  , qrslt."PERSON_FIRST_NAME"
		  , qrslt."PERSON_MIDDLE_NAME"
		  , qrslt."PERSON_LAST_NAME"
		  , qrslt."PS_REFERENCE"
		  , qrslt."SUB_PARTY_ID"
		  , qrslt."PARTY_TYPE"
		  , qrslt."RELATIONSHIP_ID"
		  , 'HZ_PARTIES' owner_table_name
		  , qrslt.party_id owner_table_id
		  , 1 rn
          , ( case when qrslt."CONTACT_LEVEL" = 'ACCOUNT SITE' 
                then NVL( (select hcsua.primary_flag 
                            from apps.hz_cust_site_uses hcsua 
                           where hcsua.cust_acct_site_id = qrslt."CUST_ACCT_SITE_ID"
                           and   hcsua.status = 'A'
                           and   hcsua.site_use_code = 'BILL_TO'
                           and   primary_flag = 'Y'
                           and   rownum = 1), 'N')
             else 'N'
             end ) has_primary_bill_to
	 FROM	(SELECT   hcar.cust_account_role_id AS cari
					, hcar.party_id
					, hcar.cust_account_id
					, hcar.cust_acct_site_id
					, hps.location_id
					, (CASE
						  WHEN hcar.cust_acct_site_id IS NOT NULL
						  THEN
							 'ACCOUNT SITE'
						  ELSE
							 'ACCOUNT'
					   END)
						 contact_level
					, (CASE
						  WHEN hcar.cust_acct_site_id IS NOT NULL
						  THEN
							 hcasa_hps.party_site_number
						  ELSE
							 hca.account_number
					   END)
						 level_value
					, hcar.current_role_state
					, hcar.primary_flag
					, hcar.role_type
					, hcar.status
					, hoc.contact_number
					, hpsub.party_name
					, hpsub.person_first_name
					, hpsub.person_middle_name
					, hpsub.person_last_name
					, hps.orig_system_reference AS ps_reference
					, hpsub.party_id AS sub_party_id
					, hpsub.party_type
					, hr.relationship_id
			   FROM   hz_cust_account_roles hcar
					, hz_parties hpsub
					, hz_parties hprel
					, hz_org_contacts hoc
					, hz_relationships hr
					, hz_party_sites hps
					, hz_cust_accounts hca
					, hz_cust_acct_sites hcasa
					, hz_party_sites hcasa_hps
			  WHERE 	  hcar.role_type = 'CONTACT'
					  AND hcar.status = 'A'
					  AND hcar.party_id = hr.party_id
					  AND hr.party_id = hprel.party_id
					  AND hr.subject_id = hpsub.party_id
					  AND hoc.party_relationship_id = hr.relationship_id
					  AND hr.directional_flag = 'F'
					  AND hps.party_id(+) = hprel.party_id
					  AND NVL (hps.identifying_address_flag, 'Y') = 'Y'
					  AND NVL (hps.status, 'A') = 'A'
					  AND hcar.cust_account_id = hca.cust_account_id
					  AND hcar.cust_acct_site_id = hcasa.cust_acct_site_id(+)
					  AND hcasa.party_site_id = hcasa_hps.party_site_id(+))
			qrslt;


