CREATE OR REPLACE VIEW APPS.XXWC_MKTANLY_CUST_CON_CONT_VW AS 
/********************************************************************************************************************************
--   NAME:       apps.XXWC_MKTANLY_CUST_CON_CONT_VW
-- REVISIONS:
--   Ver        Date        Author           Description
--   ---------  ----------  ---------------  ------------------------------------
--   1.0        09/05/2018   P.Vamshidhar      TMS#20180907-00001 -  Building Customer Outbound MV's for Marketing Analytics
**********************************************************************************************************************************/
SELECT hca.account_number Account_Number,
  hp_cont.party_id contact_party_id,
  hp_cont.PERSON_FIRST_NAME First_Name,
  hp_cont.PERSON_LAST_NAME Last_Name,
  hz_contact.contact_number Contact_Number,
  hz_contact.JOB_TITLE Job_Title,
  hz_contact.last_update_date Last_Update_Date,
  hz_contact.creation_date Created_Date
FROM apps.hz_cust_accounts hca,
  apps.hz_relationships hr,
  apps.hz_parties hp_cont,
  apps.hz_org_contacts hz_contact
WHERE 1 =1
AND hca.party_id          = hr.object_id
AND hr.object_table_name  ='HZ_PARTIES'
AND hr.object_type        ='ORGANIZATION'
AND hr.subject_id         = hp_cont.party_id
AND hr.subject_type       ='PERSON'
AND hr.subject_table_name ='HZ_PARTIES'
AND hp_cont.party_type    ='PERSON'
AND HR.relationship_id    = hz_contact.party_relationship_id
ORDER BY HCA.ACCOUNT_NUMBER
/