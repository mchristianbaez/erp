/* Formatted on 12/3/2012 6:12:42 PM (QP5 v5.206) */
-- Start of DDL Script for View APPS.XXWC_AR_CUST_ATTR_PROF_SITE
-- Generated 12/3/2012 6:12:38 PM from APPS@EBIZFQA

CREATE OR REPLACE VIEW apps.xxwc_ar_cust_attr_prof_site
(
    profile_rowid
   ,site_rowid
   ,collector_id
   ,credit_analyst_id
   ,profile_site_use_id
   ,site_site_use_id
   ,profile_status
   ,site_status
)
AS
    SELECT p.ROWID profile_rowid
          ,s.ROWID site_rowid
          ,p.collector_id
          ,credit_analyst_id
          ,p.site_use_id profile_site_use_id
          ,s.site_use_id site_site_use_id
          ,p.status profile_status
          ,NVL (s.status, 'A') site_status
      FROM hz_customer_profiles p, hz_cust_site_uses_all s
     WHERE p.site_use_id = s.site_use_id(+)
/

-- End of DDL Script for View APPS.XXWC_AR_CUST_ATTR_PROF_SITE
