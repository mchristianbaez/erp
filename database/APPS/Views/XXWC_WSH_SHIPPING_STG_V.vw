/* Formatted on 9/13/2013 4:03:45 PM (QP5 v5.115.810.9015) */
CREATE OR REPLACE FORCE VIEW apps.xxwc_wsh_shipping_stg_v (
   order_number
 , line_number
 , segment1
 , description
 , organization_code
 , ordered_quantity
 , force_ship_qty
 , transaction_qty
 , lot_number
 , reservation_id
 , status
 , header_id
 , line_id
 , delivery_id
 , delivery_detail_id
 , inventory_item_id
 , ship_from_org_id
 , creation_date
 , created_by
 , last_update_date
 , last_updated_by
 , ol_uom
 , ol_ord_qty
 , base_uom
 , base_uom_ship_quantity
 , order_uom_ship_qty
)
AS
   SELECT	b.order_number
		  , c.line_number || '.' || c.shipment_number
		  , d.segment1
		  , d.description
		  , e.organization_code
		  , a.ordered_quantity
		  , a.force_ship_qty
		  , a.transaction_qty
		  , a.lot_number
		  , a.reservation_id
		  , a.status
		  , a.header_id
		  , a.line_id
		  , a.delivery_id
		  , a.delivery_detail_id
		  , a.inventory_item_id
		  , a.ship_from_org_id
		  , a.creation_date
		  , a.created_by
		  , a.last_update_date
		  , a.last_updated_by
		  -- 08/14/2013 CG: 20130627-01130: Added order base quantity and UOM + Item Prim UOM
		  , c.order_quantity_uom ol_uom
		  , c.ordered_quantity ol_ord_qty
		  , d.primary_uom_code base_uom
		  , NVL (a.force_ship_qty, a.transaction_qty) base_uom_ship_qty
		  , NVL (
			   (a.force_ship_qty
				* po_uom_s.po_uom_convert (d.primary_unit_of_measure
										 , um.unit_of_measure
										 , c.inventory_item_id))
			 , (a.transaction_qty
				* po_uom_s.po_uom_convert (d.primary_unit_of_measure
										 , um.unit_of_measure
										 , c.inventory_item_id))
			)
			   order_uom_ship_qty
	 FROM	xxwc_wsh_shipping_stg a
		  , oe_order_headers_all b
		  , oe_order_lines_all c
		  , mtl_system_items d
		  , org_organization_definitions e
		  , mtl_units_of_measure um
	WHERE		a.header_id = b.header_id
			AND b.header_id = c.header_id
			AND a.line_id = c.line_id
			AND a.inventory_item_id = d.inventory_item_id
			AND a.ship_from_org_id = d.organization_id
			AND a.ship_from_org_id = e.organization_id
			AND c.order_quantity_uom = um.uom_code;


