CREATE OR REPLACE FORCE VIEW APPS.XXWC_INV_ITEM_SEARCH_VENDOR_V
(
   VENDOR_ID
 , SEGMENT1
 , VENDOR_NAME
 , VENDOR_SITE_ID
 , VENDOR_SITE_CODE
 , ADDRESS
 , PHONE
 , FAX
 , ORGANIZATION_ID
 , dest_organization_code
 , ITEM_ID
)
AS
   SELECT 
          -- Created by Shankar Hariharan on 12-19-2012
          -- Used in Vendor Info section of AIS
          DISTINCT vendor_id
                 , segment1
                 , vendor_name
                 , vendor_site_id
                 , vendor_site_code
                 , address
                 , phone
                 , fax
                 , organization_id
                 , dest_organization_code
                 , item_id
     FROM ( --1 where the branch sources from Supplier
           SELECT c.vendor_id
                , aps.segment1
                , aps.vendor_name
                , c.vendor_site_id
                , apss.vendor_site_code
                ,    apss.address_line1
                  || ','
                  || apss.address_line2
                  || ','
                  || apss.city
                  || ','
                  || apss.state
                  || ','
                  || apss.zip
                     address
                , apss.phone
                , apss.fax
                , a.organization_id
                , a.organization_code dest_organization_code
                , a.inventory_item_id item_id
             FROM MRP_SR_ASSIGNMENTS_V a
                , MRP_SR_RECEIPT_ORG_V b
                , MRP_SR_SOURCE_ORG_V c
                , ap_suppliers aps
                , apps.ap_supplier_sites apss
                , mtl_system_items msi
            WHERE     a.assignment_set_id = 1
                  AND c.source_type=3
                  AND a.sourcing_rule_id = b.sourcing_rule_id
                  AND sysdate between b.effective_date and nvl(b.disable_date, sysdate+1)
                  AND b.sr_receipt_id = c.sr_receipt_id
                  AND c.vendor_id = aps.vendor_id
                  AND c.vendor_site_id = apss.vendor_site_id
                  AND aps.vendor_id = apss.vendor_id
                  AND a.inventory_item_id=msi.inventory_item_id
                  AND a.organization_id=msi.organization_id
                  AND nvl(msi.source_type,2)=2
          union         
          --2 where the branch sources from another branch
       SELECT     a.source_organization_id vendor_id
                , c.organization_code segment1
                , c.organization_name vendor_name
                , null vendor_site_id
                , null vendor_site_code
                , null address
                , null phone
                , null fax
                , a.organization_id
                , b.organization_code dest_organization_code
                , a.inventory_item_id item_id
             FROM mtl_system_items a
                , org_organization_definitions b
                , org_organization_definitions c
                , mtl_parameters d
            WHERE     nvl(a.source_type,2)=1
                  AND a.organization_id = b.organization_id
                  AND a.source_organization_id = c.organization_id
                  AND a.organization_id=d.organization_id
                  AND d.master_organization_id=222
          union         
          --3 where the branch sources from another branch which in turn sources from another branch
       SELECT     a1.source_organization_id vendor_id
                , c.organization_code segment1
                , c.organization_name vendor_name
                , null vendor_site_id
                , null vendor_site_code
                , null address
                , null phone
                , null fax
                , a.organization_id
                , b.organization_code dest_organization_code
                , a.inventory_item_id item_id
             FROM mtl_system_items a
                , org_organization_definitions b
                , org_organization_definitions c
                , mtl_parameters d
                , mtl_system_items a1
            WHERE     nvl(a.source_type,2)=1
                  AND a1.organization_id = b.organization_id
                  AND a1.source_organization_id = c.organization_id
                  AND a.organization_id=d.organization_id
                  AND d.master_organization_id=222
                  AND a.inventory_item_id=a1.inventory_item_id
                  AND a.source_organization_id=a1.organization_id
                  AND nvl(a1.source_type,2)=1
          union
          --4 where the branch sources from another branch which sources from supplier
       SELECT     c.vendor_id
                , aps.segment1
                , aps.vendor_name
                , c.vendor_site_id
                , apss.vendor_site_code
                ,    apss.address_line1
                  || ','
                  || apss.address_line2
                  || ','
                  || apss.city
                  || ','
                  || apss.state
                  || ','
                  || apss.zip
                     address
                , apss.phone
                , apss.fax
                , msi.organization_id
                , ood.organization_code dest_organization_code
                , msi.inventory_item_id item_id
             FROM mtl_system_items msi
                , org_organization_definitions ood
                , mtl_parameters mp
                , mtl_system_items msi1
                , MRP_SR_ASSIGNMENTS_V a
                , MRP_SR_RECEIPT_ORG_V b
                , MRP_SR_SOURCE_ORG_V c
                , ap_suppliers aps
                , apps.ap_supplier_sites apss
            WHERE     nvl(msi.source_type,2)=1
                  AND msi.organization_id=mp.organization_id
                  AND mp.master_organization_id=222
                  AND msi.inventory_item_id=msi1.inventory_item_id
                  AND msi.source_organization_id=msi1.organization_id
                  AND msi1.organization_id = ood.organization_id
                  AND nvl(msi1.source_type,2)=2
                  AND a.inventory_item_id=msi1.inventory_item_id
                  AND a.organization_id=msi1.organization_id
                  AND a.assignment_set_id = 1
                  AND c.source_type=3
                  AND a.sourcing_rule_id = b.sourcing_rule_id
                  AND sysdate between b.effective_date and nvl(b.disable_date, sysdate+1)
                  AND b.sr_receipt_id = c.sr_receipt_id
                  AND c.vendor_id = aps.vendor_id
                  AND c.vendor_site_id = apss.vendor_site_id
                  AND aps.vendor_id = apss.vendor_id
          union
          --5 where the branch sources from another branch which sources from another branch
           -- which sources from supplier
       SELECT     c.vendor_id
                , aps.segment1
                , aps.vendor_name
                , c.vendor_site_id
                , apss.vendor_site_code
                ,    apss.address_line1
                  || ','
                  || apss.address_line2
                  || ','
                  || apss.city
                  || ','
                  || apss.state
                  || ','
                  || apss.zip
                     address
                , apss.phone
                , apss.fax
                , msi.organization_id
                , ood.organization_code dest_organization_code
                , msi.inventory_item_id item_id
             FROM mtl_system_items msi
                , org_organization_definitions ood
                , mtl_parameters mp
                , mtl_system_items msi1
                , mtl_system_items msi2
                , MRP_SR_ASSIGNMENTS_V a
                , MRP_SR_RECEIPT_ORG_V b
                , MRP_SR_SOURCE_ORG_V c
                , ap_suppliers aps
                , apps.ap_supplier_sites apss
            WHERE     nvl(msi.source_type,2)=1
                  AND msi.organization_id=mp.organization_id
                  AND mp.master_organization_id=222
                  AND msi.inventory_item_id=msi1.inventory_item_id
                  AND msi.source_organization_id=msi1.organization_id
                  AND nvl(msi1.source_type,2)=1
                  AND msi1.inventory_item_id=msi2.inventory_item_id
                  AND msi1.source_organization_id=msi2.organization_id
                  AND msi2.organization_id = ood.organization_id
                  AND nvl(msi2.source_type,2)=2
                  AND a.inventory_item_id=msi2.inventory_item_id
                  AND a.organization_id=msi2.organization_id
                  AND a.assignment_set_id = 1
                  AND c.source_type=3
                  AND a.sourcing_rule_id = b.sourcing_rule_id
                  AND sysdate between b.effective_date and nvl(b.disable_date, sysdate+1)
                  AND b.sr_receipt_id = c.sr_receipt_id
                  AND c.vendor_id = aps.vendor_id
                  AND c.vendor_site_id = apss.vendor_site_id
                  AND aps.vendor_id = apss.vendor_id
                 )
/