
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUSGL_FISCAL_YEAR_DATES_VW" ("DATETIMESTAMP", "FISCAL_YEAR_ID", "FISCAL_YEAR_DESC", "FISCAL_YEAR_BEGIN_DATE", "FISCAL_YEAR_END_DATE", "FISCAL_QUARTER_ID", "FISCAL_QUARTER_DESC", "FISCAL_QUARTER_BEGIN_DATE", "FISCAL_QUARTER_END_DATE", "FISCAL_MONTH_ID", "FISCAL_MONTH_DESC", "FISCAL_MONTH_BEGIN_DATE", "FISCAL_MONTH_END_DATE") AS 
  SELECT SYSDATE datetimestamp
      ,a.period_year fiscal_year_id
      ,'Fiscal Year ' || to_char(a.period_year) fiscal_year_desc
      ,(SELECT MIN(b.start_date)
          FROM gl.gl_periods b
         WHERE b.period_set_name = '4-4-QTR'
           AND (b.period_year) = (a.period_year)) fiscal_year_begin_date
      ,(SELECT MAX(c.end_date)
          FROM gl.gl_periods c
         WHERE c.period_set_name = '4-4-QTR'
           AND (c.period_year) = (a.period_year)) fiscal_year_end_date
      ,a.quarter_num fiscal_quarter_id
      ,CASE a.quarter_num
         WHEN 1 THEN
          a.quarter_num || 'st fiscal quarter for FY: ' ||
          to_char(a.period_year)
         WHEN 2 THEN
          a.quarter_num || 'nd fiscal quarter for FY: ' ||
          to_char(a.period_year)
         WHEN 3 THEN
          a.quarter_num || 'rd fiscal quarter for FY: ' ||
          to_char(a.period_year)
         WHEN 4 THEN
          a.quarter_num || 'th fiscal quarter for FY: ' ||
          to_char(a.period_year)
       END fiscal_quarter_desc
      ,quarter_start_date fiscal_quarter_begin_date
      ,CASE a.quarter_num
         WHEN 1 THEN
          (SELECT DISTINCT (d.quarter_start_date)
             FROM gl.gl_periods d
            WHERE d.period_set_name = '4-4-QTR'
              AND d.period_year = a.period_year
              AND quarter_num = 2)
         WHEN 2 THEN
          (SELECT DISTINCT d.quarter_start_date
             FROM gl.gl_periods d
            WHERE d.period_set_name = '4-4-QTR'
              AND d.period_year  = a.period_year
              AND quarter_num = 3)
         WHEN 3 THEN
          (SELECT DISTINCT d.quarter_start_date
             FROM gl.gl_periods d
            WHERE d.period_set_name = '4-4-QTR'
              AND d.period_year = a.period_year
              AND quarter_num = 4)
         WHEN 4 THEN
          (SELECT MAX(c.end_date)
             FROM gl.gl_periods c
            WHERE c.period_set_name = '4-4-QTR'
              AND c.period_year = a.period_year )
       END fiscal_quarter_end_date
      ,a.period_num fiscal_month_id
      ,a.period_name fiscal_month_desc
      ,a.start_date fiscal_month_begin_date
      ,a.end_date fiscal_month_end_date
  FROM gl.gl_periods a
 WHERE a.period_set_name = '4-4-QTR'
 ORDER BY a.period_year, a.quarter_num, a.period_num
;
