--//============================================================================
--//
--// Object Name         :: xxwc_inv_promocodes_v
--//
--// Object Type         :: View
--//
--// Object Description  :: This is Promo Codes extract to AgilOne
--//
--// Version Control
--//============================================================================
--// Vers    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Mahesh Kudle     26/05/2014    Initial Build - TMS#20140602-00295
--//============================================================================
CREATE OR REPLACE FORCE VIEW apps.xxwc_inv_promocodes_v
(
   operating_unit_id
  ,promocodes
)
AS
   SELECT DISTINCT
          inv.operating_unit_id
         , (   TRIM (inv.segment1)
            || '|'
            || TRIM (inv.description)
            || '|'
            || TRIM (inv.item_type)
            || '|'
            || TRIM (inv.item_category_class))
             promocodes
     FROM (SELECT DISTINCT ood.operating_unit operating_unit_id
                          ,msi.segment1
                          ,msi.description
                          ,msi.item_type item_type
                          ,mic.segment2 item_category_class
             FROM inv.mtl_system_items_b msi
                 ,apps.mtl_item_categories_v mic
                 ,apps.org_organization_definitions ood
            WHERE     mic.segment2 = 'PRMO'
                  AND msi.inventory_item_id = mic.inventory_item_id
                  AND msi.organization_id = mic.organization_id
                  AND msi.organization_id = ood.organization_id) inv;