
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUSOZF_ACCRUAL_PROD_SPEC_V" ("MVID", "VENDOR_NAME", "ACTIVITY_NAME", "REBATE_TYPE", "OFFER_CODE", "OFFER_NAME", "PERIOD_NAME", "LOB", "BU_BRANCH_NUMBER", "PO_NUMBER", "SKU_CD", "PRODUCT_DESC", "VENDOR_PART_NBR", "UPC", "RECEIPT_DATE", "CATEGORY_SEGMENT1", "CATEGORY_SEGMENT2", "CATEGORY_SET_NAME", "CATEGORY_SET_ID", "ACCRUALS", "PURCHASES") AS 
  SELECT accr.ofu_mvid mvid ,
    accr.ofu_mvid_name vendor_name ,
    accr.activity_media_name activity_name ,
    DECODE(accr.activity_media_name,'Coop','COOP','Coop Minimum','COOP','REBATE') rebate_type ,
    offer_code offer_code ,
    offer_name offer_name ,
    accr.xaeh_period_name period_name ,
    accr.resale_line_attribute2 lob ,
    accr.resale_line_attribute6 bu_branch_number ,
    accr.po_number po_number ,
    item.attribute8 SKU_cd ,
    item.attribute6 Product_desc ,
    item.attribute3 Vendor_part_nbr ,
    item.attribute5 UPC ,
    TRUNC(accr.date_ordered) receipt_date ,
    item.category_segment1 category_segment1 ,
    item.category_segment2 category_segment2 ,
    item.category_set_name category_set_name ,
    item.category_set_id category_set_id ,
    SUM(NVL(accr.util_acctd_amount,0)) Accruals ,
    SUM(NVL(accr.selling_price*accr.quantity,0)) Purchases
  FROM xxcus.xxcus_ozf_xla_accruals_b accr,
    xxcus.xxcus_ozf_rebate_items item
  WHERE 1                   =1
  AND accr.inventory_item_id=item.inventory_item_id
  and accr.ofu_mvid=item.category_segment1 
  and item.category_set_id='1100000041'
 -- AND ACCR.OFU_CUST_ACCOUNT_ID=3071
  GROUP BY accr.ofu_mvid ,
    accr.ofu_mvid_name ,
    accr.activity_media_name ,
    DECODE(accr.activity_media_name,'Coop','COOP','Coop Minimum','COOP','REBATE') ,
    offer_code ,
    offer_name ,
    accr.xaeh_period_name ,
    accr.resale_line_attribute2 ,
    accr.resale_line_attribute6 ,
    accr.po_number ,
    item.attribute8 ,
    item.attribute6 ,
    item.attribute3 ,
    item.attribute5 ,
    TRUNC(accr.date_ordered) ,
    item.category_segment1 ,
    item.category_segment2 ,
    item.category_set_name ,
    item.category_set_id;
