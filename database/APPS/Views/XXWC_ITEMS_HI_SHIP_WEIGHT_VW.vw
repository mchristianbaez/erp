CREATE OR REPLACE VIEW xxwc_items_hi_ship_weight_vw
AS
SELECT mp.organization_code, 
       msib.segment1, 
       msib.description,
       msib.unit_weight, 
       msib.attribute23 shipping_weight_override,
       msib.inventory_item_id,
       msib.organization_id
from   mtl_parameters mp,
       mtl_system_items_b msib
where  msib.organization_id = mp.organization_id;