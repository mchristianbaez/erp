CREATE OR REPLACE FORCE VIEW APPS.XXWC_INV_ORGANIZATION_V
(
   ORG_CODE_NAME,
   ORGANIZATION_ID
)
AS
   SELECT a.organization_code || ' - ' || a.organization_name org_code_name,
          a.organization_id
     FROM org_organization_definitions a, mtl_parameters b
    WHERE     a.organization_id = b.organization_id
          AND b.organization_id <> b.master_organization_id;