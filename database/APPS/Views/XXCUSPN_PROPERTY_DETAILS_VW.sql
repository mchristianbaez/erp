
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUSPN_PROPERTY_DETAILS_VW" ("PHYSICAL_PROP_DESC", "BUILD_LAND_SQFT", "FLOOR_PARCEL_NAME", "FLOOR_PARCEL_ID", "OFFICE_YARD_NAME", "OFFICE_YARD_ID", "OFFICE_YARD_RENTABLE", "OFFICE_YARD_USABLE", "OFFICE_YARD_ASSIGNABLE", "BUILD_LAND_LOCATION_ID", "BUILD_LAND_LOCATION_CODE") AS 
  SELECT  prop.property_name physical_prop_desc,
    prop.gross_area||' '||uom_code build_land_sqft,
    flr.floor  floor_parcel_name,
    flr.LOCATION_ALIAS  floor_parcel_id,
    offi.office office_yard_name,
    offi.LOCATION_ALIAS office_yard_id,
    offi.RENTABLE_AREA office_yard_rentable,
    offi.USABLE_AREA office_yard_usable,
    offi.ASSIGNABLE_AREA office_yard_assignable,
    prop.LOCATION_ID build_land_location_id,
    prop.location_code build_land_location_code
 FROM PN_BUILDINGS_V  prop,
     PN_FLOORS_V     flr,
    PN_OFFICES_V    offi
WHERE flr.parent_location_id = prop.location_id
AND   offi.parent_location_id = flr.location_id
AND   prop.active_end_date = (SELECT MAX(mx.active_end_date) FROM apps.pn_locations_all mx WHERE mx.location_id = prop.location_id)
;
