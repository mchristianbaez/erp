--
-- XXWC_OM_PRICING_GUARDRAIL_VW  (View)
--

CREATE OR REPLACE FORCE VIEW APPS.XXWC_OM_PRICING_GUARDRAIL_VW
(
   HEADER_ID
  ,ORDER_NUMBER
  ,ORDERED_DATE
  ,LINE_ID
  ,LINE_NUMBER
  ,ORDERED_ITEM
  ,UNIT_SELLING_PRICE
  ,UNIT_LIST_PRICE
  ,PRICING_QUANTITY
  ,PRICING_QUANTITY_UOM
  ,PRICING_DATE
  ,SHIP_FROM_ORG_ID
  ,SHIP_FROM_ORG
  ,PRICE_ZONE
  ,MIN_MARGIN
  ,MAX_DISCOUNT
  ,UNIT_COST
  ,ORDER_DISCOUNT
  ,ORDER_MARGIN
  ,ITEM_CATEGORY_CODE
)
AS
     SELECT ooh.header_id
           ,ooh.order_number
           ,ooh.ordered_date
           ,ool.line_id
           ,ool.line_number
           ,ool.ordered_item
           ,ool.unit_selling_price
           ,ool.unit_list_price
           ,ool.pricing_quantity
           ,ool.pricing_quantity_uom
           ,ool.pricing_date
           ,ool.ship_from_org_Id
           ,mp.organization_Code
           ,mp.Attribute6 price_zone
           ,pg.min_margin
           ,pg.max_discount
           ,ool.unit_cost
           ,DECODE (
               ool.Unit_List_Price
              ,0, 0.00
              ,ROUND (
                  (  (ool.unit_list_price - ool.unit_selling_price)
                   / NVL (ool.unit_list_price, 1)
                   * 100)
                 ,2))
               Order_Discount
           ,ROUND (ool.unit_cost * (1 + pg.min_margin / 100), 2) Order_Margin
           , (SELECT CATEGORY_CONCAT_SEGMENTS
                FROM MTL_CATEGORY_SET_VALID_CATS_V cat
               WHERE Cat.Category_Id = pg.Item_Category_ID)
               Item_Category_Code
       FROM apps.oe_order_headers ooh
           ,apps.oe_order_lines ool
           ,mtl_Parameters mp
           ,xxwc_om_pricing_guardrail pg
      WHERE     ooh.header_id = ool.header_id
            AND ool.ship_from_org_id = mp.Organization_id
            AND pg.Pricing_GuardRail_Id(+) =
                   xxwc_om_pricing_guardrail_pkg.Get_pricing_Guard_Rail_ID (
                      ool.Inventory_Item_ID
                     ,ool.Ship_From_Org_ID)
   -- And     ooh.Ordered_date(+) BETWEEN pg.start_date AND NVL(pg.end_date, ooh.ordered_date + 1)
   ORDER BY ooh.order_number, ool.line_number;


