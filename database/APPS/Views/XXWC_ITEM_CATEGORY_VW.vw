--
-- XXWC_ITEM_CATEGORY_VW  (View)
--

CREATE OR REPLACE FORCE VIEW APPS.XXWC_ITEM_CATEGORY_VW
(
   ITEM_CATEGORY
  ,CATEGORY_ID
  ,DESCRIPTION
)
AS
   SELECT MC.CATEGORY_CONCAT_SEGS CATEGORY_CONCAT_SEGMENTS
         ,mc.Category_ID
         ,MC.Description
     FROM MTL_CATEGORY_SETS_V mcs, MTL_CATEGORIES_V MC
    WHERE     mcs.Category_Set_ID =
                 FND_PROFILE.VALUE ('XXWC_PARTS_ON_FLY_CATEGORY_SET')
          AND mc.Structure_Id = mcs.Structure_ID
          AND mc.Enabled_flag = 'Y'
          AND (mc.disable_Date IS NULL OR mc.Disable_date <= SYSDATE);


