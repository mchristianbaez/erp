--
-- XXWCAP_DCTM_VENDOR_VW  (View)
--

CREATE OR REPLACE FORCE VIEW APPS.XXWCAP_DCTM_VENDOR_VW
(
   POS_ID
  ,POS_VENDOR_REMIT
  ,ORACLE_VENDOR_NAME
  ,ORACLE_SITE_ID
  ,ORACLE_VENDOR_NUM
  ,ADDRESS_LINE1
  ,ADDRESS_LINE2
  ,CITY
  ,STATE
  ,ZIP
)
AS
   SELECT 'DCM'
         ,NULL
         ,v.vendor_name
         ,s.vendor_site_id
         ,v.segment1
         ,s.address_line1
         ,s.address_line2
         ,s.city
         ,NVL (s.state, s.province)
         ,s.zip
     FROM ap.ap_suppliers v, ap.ap_supplier_sites_all s
    WHERE     v.vendor_id = s.vendor_id
          AND s.org_id = 162
          AND NVL (v.vendor_type_lookup_code, 'X') <> 'EMPLOYEE'
          AND v.enabled_flag = 'Y'
          AND NVL (v.end_date_active, SYSDATE) >= SYSDATE
          AND NVL (s.inactive_date, SYSDATE) >= SYSDATE
          AND s.pay_site_flag = 'Y'
          AND s.primary_pay_site_flag = 'Y';


