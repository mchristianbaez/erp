--
-- ENG_XXWC_NOTES_AGV  (View)
--

CREATE OR REPLACE FORCE VIEW apps.eng_xxwc_notes_agv
(
   extension_id
  ,change_id
  ,xxwc_notes_tl
)
AS
   SELECT extension_id, change_id, tl_ext_attr1 xxwc_notes_tl
     FROM eng_changes_ext_vl
    WHERE attr_group_id = 224;


