--
-- XXWC_EDI_TP_V  (View)
--

CREATE OR REPLACE FORCE VIEW APPS.XXWC_EDI_TP_V
(
   TP_GROUP_CODE
  ,TP_CODE
  ,TP_DESCRIPTION
  ,TP_REFERENCE_EXT1
  ,TP_REFERENCE_EXT2
  ,DOCUMENT_ID
  ,DOCUMENT_TYPE
  ,TRANSLATOR_CODE
  ,DOCUMENT_STANDARD
  ,VENDOR_CUSTOMER_NAME
  ,VENDOR_CUSTOMER_NUMBER
  ,VENDOR_CUSTOMER_SITE_CODE
  ,PURCHASING_SITE_FLAG
  ,PAY_SITE_FLAG
  ,ECE_TP_LOCATION_CODE
  ,DOCUMENT_ID_LOCATION_CODE
)
AS
   SELECT etg.tp_group_code
         ,eth.tp_code
         ,eth.tp_description
         ,eth.tp_reference_ext1
         ,eth.tp_reference_ext2
         ,etd.document_id
         ,etd.document_type
         ,etd.translator_code
         ,etd.document_standard
         ,ass.vendor_name vendor_customer_name
         ,ass.segment1 vendor_customer_number
         ,assa.vendor_site_code vendor_customer_site_code
         ,assa.purchasing_site_flag
         ,assa.pay_site_flag
         ,assa.ece_tp_location_code
         ,etd.document_id || '-' || assa.ece_tp_location_code
             document_id_location_code
     FROM ece_tp_headers eth
         ,ece_tp_details etd
         ,ece_tp_group etg
         ,ap_suppliers ass
         ,apps.ap_supplier_sites assa
    WHERE     eth.tp_header_id = etd.tp_header_id
          AND etg.tp_group_id = eth.tp_group_id
          AND eth.tp_header_id = assa.tp_header_id
          AND ass.vendor_id = assa.vendor_id
   UNION
   ---Customers
   SELECT etg.tp_group_code
         ,eth.tp_code
         ,eth.tp_description
         ,eth.tp_reference_ext1
         ,eth.tp_reference_ext2
         ,etd.document_id
         ,etd.document_type
         ,etd.translator_code
         ,etd.document_standard
         ,hp.party_name vendor_customer_name
         ,hca.account_number vendor_customer_number
         ,hcsua.location vendor_customer_site_code
         ,'' purchasing_site_flag
         ,'' pay_site_flag
         ,hcasa.ece_tp_location_code
         ,etd.document_id || '-' || hcasa.ece_tp_location_code
             document_id_location_code
     FROM ece_tp_headers eth
         ,ece_tp_details etd
         ,ece_tp_group etg
         ,hz_parties hp
         ,hz_party_sites hps
         ,hz_cust_accounts hca
         ,apps.HZ_CUST_SITE_USES hcsua
         ,apps.HZ_CUST_ACCT_SITES hcasa
         ,HZ_locations hl
    WHERE     eth.tp_header_id = etd.tp_header_id
          AND etg.tp_group_id = eth.tp_group_id
          AND hp.party_id = hps.party_id
          AND hp.party_id = hca.party_id
          AND hca.cust_account_id = hcasa.cust_account_id
          AND hcasa.party_site_id = hps.party_site_id
          AND hcasa.cust_acct_site_id = hcsua.cust_acct_site_id
          AND hps.location_id = hl.location_id
          AND eth.tp_header_id = hcasa.tp_header_id;


