CREATE OR REPLACE VIEW APPS.XXWC_INV_PRODUCT_LOC_EXT_VW
( 
/******************************************************************************
   NAME        :  APPS.XXWC_PO_RECEIPTS_VW
   PURPOSE     :  This view is created for extracting the Supplier and Supplier site details

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        22/06/2012   Consuelo        Initial Version
   2.0        03/04/2015   Raghavendra S   TMS# 20150303-00075 - FIN - Addition of Code Snippet for Line Breaks in All EDW Extract Programs
                                            and Views for all free entry test fields.
******************************************************************************/ 
   operating_unit_id
,  interface_date
,  product_loc_data ) 
AS
SELECT    operating_unit_id
          , interface_date
          , (    business_unit
             || '|'
             || branch_number
             || '|'
             -- || su_sku -- commented for V 2.0
             || REGEXP_REPLACE(su_sku,  '[[:cntrl:]]', ' ') -- Added for V 2.0
             || '|'
             || sku_start_date
             || '|'
             || stock_indicator
             || '|'
             || prod_rank
             || '|'
             || dept_number
             || '|'
             || prod_status_code
             || '|'
             || buyer_number
             || '|'
             || po_replacement_cost
             || '|'
             || po_repl_cost_effect_date
             || '|'
             || list_price
             || '|'
             || list_price_eff_date
             || '|'
             || std_cost_amount
             || '|'
             || std_cost_effe_date
             || '|'
             || branch_cost
             || '|'
             || branch_cost_effe_date
             || '|'
             || prod_velocity_code
             || '|'
             || prod_line_status_code
             || '|'
             -- || prod_line_status_desc  -- commented for V 2.0
             || REGEXP_REPLACE(prod_line_status_desc,  '[[:cntrl:]]', ' ')  -- Added for V 2.0
             || '|'
             -- 01/07/2013 CG: Changed to pull primary pay site
             -- || part_vendor_code
             || source_sup_prim_pay_site
             || '|'
             || min_qty
             || '|'
             || max_qty
             || '|'
             || safety_stock
             || '|'
             || order_lead_days
             || '|'
             || minimum_gross_margin
             || '|'
             || min_sell_price_amt
             || '|'
             || last_count_qty
             || '|'
             || last_counted_by
             || '|'
             || last_count_date
             || '|'
             || lead_time_expiration_date
             || '|'
             || last_po_receipt_date
             || '|'
             || last_transfer_out_date
             || '|'
             || last_transfer_in_date
             || '|'
             || last_work_order_in_date
             || '|'
             || last_work_order_out_date
             || '|'
             || last_sales_date
             || '|'
             || twelve_month_wo_out_qty
             || '|'
             || twelve_month_trx_out_qty
             || '|'
             || twelve_month_stock_sale_qty
             || '|'
             || twelve_month_direct_sale_qty
             || '|'
             || forecast_hits
             || '|'
             || forecast_period
             || '|'
             || override_branch_hits
             || '|'
             || twelve_month_hits
             || '|'
             || price_sheet
             || '|'
             || price_sheet_effe_date
             || '|'
             || warehouse_branch
             || '|'
             || warehouse_branch_flag
             || '|'
             || seasonal_flag
             || '|'
             || trend_percent
             || '|'
             || primary_bin_location)
               product_loc_data
FROM
--Added two select clause with RNUM logic to handle duplicate rows by Maha on 5/27/2014 for TMS#20140527-00197 
(  
  SELECT   operating_unit_id
           , interface_date
           , business_unit           
           , branch_number           
           , su_sku           
           , sku_start_date        
           , stock_indicator           
           , prod_rank           
           , dept_number           
           , prod_status_code           
           , buyer_number           
           , po_replacement_cost           
           , po_repl_cost_effect_date           
           , list_price           
           , list_price_eff_date           
           , std_cost_amount           
           , std_cost_effe_date           
           , branch_cost           
           , branch_cost_effe_date           
           , prod_velocity_code           
           , prod_line_status_code           
           , prod_line_status_desc   
           , source_sup_prim_pay_site     
           , min_qty           
           , max_qty           
           , safety_stock           
           , order_lead_days           
           , minimum_gross_margin           
           , min_sell_price_amt           
           , last_count_qty           
           , last_counted_by           
           , last_count_date           
           , lead_time_expiration_date           
           , last_po_receipt_date           
           , last_transfer_out_date           
           , last_transfer_in_date           
           , last_work_order_in_date           
           , last_work_order_out_date           
           , last_sales_date           
           , twelve_month_wo_out_qty           
           , twelve_month_trx_out_qty           
           , twelve_month_stock_sale_qty           
           , twelve_month_direct_sale_qty           
           , forecast_hits           
           , forecast_period           
           , override_branch_hits           
           , twelve_month_hits           
           , price_sheet           
           , price_sheet_effe_date           
           , warehouse_branch           
           , warehouse_branch_flag           
           , seasonal_flag           
           , trend_percent           
           , primary_bin_location    
           ,rnum
   FROM (
  SELECT     operating_unit_id
           , interface_date
           , business_unit           
           , branch_number           
           , su_sku           
           , TO_CHAR (sku_start_date, 'MM/DD/YYYY')   sku_start_date        
           , stock_indicator           
           , prod_rank           
           , dept_number           
           , prod_status_code           
           , buyer_number           
           , po_replacement_cost           
           , po_repl_cost_effect_date           
           , list_price           
           , list_price_eff_date           
           , std_cost_amount           
           , std_cost_effe_date           
           , branch_cost           
           , branch_cost_effe_date           
           , prod_velocity_code           
           , prod_line_status_code           
           , prod_line_status_desc   
           , source_sup_prim_pay_site     
           , min_qty           
           , max_qty           
           , safety_stock           
           , order_lead_days           
           , minimum_gross_margin           
           , min_sell_price_amt           
           , last_count_qty           
           , last_counted_by           
           , TO_CHAR (last_count_date, 'MM/DD/YYYY')        last_count_date   
           , lead_time_expiration_date           
           , last_po_receipt_date           
           , last_transfer_out_date           
           , last_transfer_in_date           
           , last_work_order_in_date           
           , last_work_order_out_date           
           , TO_CHAR (last_sales_date, 'MM/DD/YYYY')       last_sales_date    
           , twelve_month_wo_out_qty           
           , twelve_month_trx_out_qty           
           , twelve_month_stock_sale_qty           
           , twelve_month_direct_sale_qty           
           , forecast_hits           
           , forecast_period           
           , override_branch_hits           
           , twelve_month_hits           
           , price_sheet           
           , TO_CHAR (price_sheet_effe_date, 'MM/DD/YYYY')    price_sheet_effe_date       
           , warehouse_branch           
           , warehouse_branch_flag           
           , seasonal_flag           
           , trend_percent           
           , primary_bin_location           
           , row_number() over (partition by business_unit,branch_number,su_sku order by business_unit,branch_number,su_sku) rnum
   FROM    XXWC.xxwc_inv_prod_loc_tbl) --Added by Mahesh Kudle 15/10/13; TMS#20130626-01200
     --FROM    xxwc_inv_product_loc_vw;    --Commented by Mahesh Kudle 15/10/13; TMS#20130626-01200 )
 WHERE rnum = 1);

    
