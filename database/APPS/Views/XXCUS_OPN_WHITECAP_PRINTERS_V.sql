CREATE OR REPLACE FORCE VIEW APPS.XXCUS_OPN_WHITECAP_PRINTERS_V AS
/* 
-- **************************************************************************************************
-- Scope: This view will be the source for the APEX COMMON systems and the Web Application
--               Location Direcory Plus tool.
-- Grants:  Oracle EBIZPRD database user APPS has granted "SELECT" access to user INTERFACE_DSTAGE [See below]
-- $Header XXCUS_OPN_WHITECAP_PRINTERS_V.sql $
-- Module Name: HDS Property Manager
-- REVISIONS:
-- Ver         Date                 Author               Ticket#                                    Description
-- ---------  ----------          ----------               -------------------------            ------------------------------------------------------------------
-- 1.0         03/16/2016  Bala Seshadri   TMS 20160316-00132      Created.    
-- ************************************************************************************************** 
*/
SELECT  DISTINCT W.PRINTER_NAME
          ,FP.DESCRIPTION
          ,O.ORGANIZATION_CODE
          ,MP.ATTRIBUTE9 "REGION"
          ,MP.ATTRIBUTE8 "DISTRICT"
          ,MP.ATTRIBUTE10 "FRU"
          ,MP.ATTRIBUTE11 "LOB_BRANCH"
FROM
  APPS.WSH_REPORT_PRINTERS W
,APPS.FND_CONCURRENT_PROGRAMS_TL F
,APPS.FND_PRINTER_VL FP
,APPS.ORG_ORGANIZATION_DEFINITIONS O
,APPS.MTL_PARAMETERS MP
WHERE
  W.CONCURRENT_PROGRAM_ID = F.CONCURRENT_PROGRAM_ID AND
  MP.ORGANIZATION_ID = O.ORGANIZATION_ID AND
  FP.PRINTER_NAME = W.PRINTER_NAME AND
  W.APPLICATION_ID = F.APPLICATION_ID AND
  O.ORGANIZATION_ID = W.LEVEL_VALUE_ID AND
  W.ENABLED_FLAG = 'Y' AND
  W.DEFAULT_PRINTER_FLAG = 'Y'
ORDER BY
  O.ORGANIZATION_CODE
,W.PRINTER_NAME;
--
COMMENT ON TABLE APPS.XXCUS_OPN_WHITECAP_PRINTERS_V IS 'TMS: 20160316-00132 / ESMS: 320562';
--
GRANT SELECT ON APPS.XXCUS_OPN_WHITECAP_PRINTERS_V TO INTERFACE_DSTAGE;
--