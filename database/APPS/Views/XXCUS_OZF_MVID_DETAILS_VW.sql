
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUS_OZF_MVID_DETAILS_VW" ("MVID_NUM", "MVID_NAME", "TRADE_PROFILE_ID") AS 
  select DISTINCT REPLACE(hp1.party_number, '~MSTR', '') mvid_num,
                hp1.party_name      mvid_name,
                ctp.trade_profile_id

from  ozf_cust_trd_prfls_all  ctp,
      Ar.Hz_Cust_Accounts     Hca1,
      Ar.Hz_Parties           Hp1
where hca1.cust_account_id = ctp.cust_account_id(+)
AND hca1.party_id = hp1.party_id(+)
and hp1.attribute1 = 'HDS_MVID'
;
