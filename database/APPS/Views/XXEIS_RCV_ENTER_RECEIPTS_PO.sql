/* Formatted on 2/11/2014 9:26:20 PM (QP5 v5.206) */
-- Start of DDL Script for View APPS.XXEIS_RCV_ENTER_RECEIPTS_PO
-- Generated 2/11/2014 9:26:12 PM from APPS@EBIZFQA

CREATE OR REPLACE VIEW apps.xxeis_rcv_enter_receipts_po
(
    type_lookup_code
   ,segment1
   ,po_line_id
   ,line_num
   ,line_location_id
   ,shipment_num
   ,po_release_id
   ,release_num
   ,po_header_id
   ,ship_to_organization_id
   ,vendor_id
   ,vendor_site_id
   ,outside_operation_flag
   ,item_id
   ,ordered_uom
   ,primary_uom
   ,primary_uom_class
   ,allowed_units_lookup_code
   ,location_control_code
   ,restrict_locators_code
   ,restrict_subinventories_code
   ,shelf_life_cod
   ,shelf_life_days
   ,serial_number_control_code
   ,lot_control_code
   ,revision_qty_control_code
   ,item_revision
   ,category_id
   ,vendor_product_num
   ,ship_to_location_id
   ,location_code
   ,receiving_routing_id
   ,need_by_date
   ,expected_receipt_date
   ,quantity
   ,quantity_received
   ,quantity_cancelled
   ,government_context
   ,inspection_required_flag
   ,receipt_required_flag
   ,enforce_ship_to_location_code
   ,unit_price
   ,currency_code
   ,rate_type
   ,rate_date
   ,rate
   ,note_to_receiver
   ,attribute_category
   ,closed_code
   ,user_conversion_type
   ,match_option
   ,country_of_origin_code
   ,pll_note_to_receiver
   ,secondary_ordered_qty
   ,secondary_ordered_uom
   ,qc_grade
   ,secondary_default_ind
   ,org_id
   ,lcm_flag
   ,drop_ship_flag
)
AS
    SELECT poh.type_lookup_code
          ,poh.segment1
          ,poll.po_line_id
          ,pol.line_num
          ,poll.line_location_id
          ,poll.shipment_num
          ,poll.po_release_id
          ,por.release_num
          ,poh.po_header_id
          ,poll.ship_to_organization_id
          ,poh.vendor_id
          ,poh.vendor_site_id
          ,NVL (polt.outside_operation_flag, 'N') outside_operation_flag
          ,pol.item_id
          ,pol.unit_meas_lookup_code ordered_uom
          ,msi.primary_unit_of_measure primary_uom
          ,mum.uom_class primary_uom_class
          ,NVL (msi.allowed_units_lookup_code, 2) allowed_units_lookup_code
          ,NVL (msi.location_control_code, 1) location_control_code
          ,DECODE (msi.restrict_locators_code, 1, 'Y', 'N') restrict_locators_code
          ,DECODE (msi.restrict_subinventories_code, 1, 'Y', 'N') restrict_subinventories_code
          ,NVL (msi.shelf_life_code, 1) shelf_life_cod
          ,NVL (msi.shelf_life_days, 0) shelf_life_days
          ,msi.serial_number_control_code
          ,msi.lot_control_code
          ,DECODE (msi.revision_qty_control_code,  1, 'N',  2, 'Y',  'N') revision_qty_control_code
          ,pol.item_revision
          ,pol.category_id
          ,pol.vendor_product_num
          ,poll.ship_to_location_id
          ,hl.location_code
          ,poll.receiving_routing_id
          ,poll.need_by_date
          ,NVL (poll.promised_date, poll.need_by_date) expected_receipt_date
          ,poll.quantity
          ,poll.quantity_received
          ,poll.quantity_cancelled
          ,poll.government_context
          ,poll.inspection_required_flag
          ,poll.receipt_required_flag
          ,poll.enforce_ship_to_location_code
          ,NVL (poll.price_override, pol.unit_price) unit_price
          ,poh.currency_code
          ,poh.rate_type
          ,poh.rate_date
          ,poh.rate
          ,poh.note_to_receiver
          ,poll.attribute_category
          ,poll.closed_code
          ,dct.user_conversion_type
          ,poll.match_option
          ,poll.country_of_origin_code
          ,poll.note_to_receiver pll_note_to_receiver
          ,poll.secondary_quantity secondary_ordered_qty
          ,poll.secondary_unit_of_measure secondary_ordered_uom
          ,poll.preferred_grade qc_grade
          ,DECODE (msi.tracking_quantity_ind, 'PS', msi.secondary_default_ind, NULL) secondary_default_ind
          ,poll.org_id
          ,poll.lcm_flag
          ,NVL (poll.drop_ship_flag, 'N') drop_ship_flag
      FROM po_headers_all poh
          ,po_line_locations_all poll
          ,po_lines_all pol
          ,po_releases_all por
          ,hr_locations_all_tl hl
          ,mtl_system_items msi
          ,mtl_units_of_measure mum
          ,po_line_types_b polt
          ,gl_daily_conversion_types dct
          ,rcv_parameters rp
     WHERE     NVL (poll.approved_flag, 'N') = 'Y'
           AND NVL (poll.cancel_flag, 'N') = 'N'
           AND NVL (pol.clm_info_flag, 'N') = 'N'
           AND (NVL (pol.clm_option_indicator, 'B') <> 'O' OR NVL (pol.clm_exercised_flag, 'N') = 'Y')
           AND poll.closed_code IN ('OPEN', 'CLOSED FOR INVOICE')
           AND poll.shipment_type IN ('STANDARD', 'BLANKET', 'SCHEDULED')
           AND poh.po_header_id = poll.po_header_id
           AND pol.po_line_id = poll.po_line_id
           AND poll.po_release_id = por.po_release_id(+)
           AND poll.ship_to_location_id = hl.location_id(+)
           AND pol.line_type_id = polt.line_type_id(+)
           AND mum.unit_of_measure(+) = pol.unit_meas_lookup_code
           AND NVL (msi.organization_id, poll.ship_to_organization_id) = poll.ship_to_organization_id
           AND msi.inventory_item_id(+) = pol.item_id
           AND dct.conversion_type(+) = poh.rate_type
           AND NVL (poh.consigned_consumption_flag, 'N') = 'N'
           AND NVL (por.consigned_consumption_flag, 'N') = 'N'
           AND NVL (poll.matching_basis, 'QUANTITY') != 'AMOUNT'
           AND poll.payment_type IS NULL
           AND rp.organization_id = poll.ship_to_organization_id
           AND (NVL (rp.pre_receive, 'N') = 'N' OR (NVL (rp.pre_receive, 'N') = 'Y' AND NVL (poll.lcm_flag, 'N') = 'N'))
           AND (   EXISTS
                       (SELECT 'Not associated to WIP Job'
                          FROM po_distributions_all pod1, po_lines_all pltv
                         WHERE     pod1.po_line_id = pltv.po_line_id
                               AND pod1.line_location_id = poll.line_location_id
                               AND pod1.wip_entity_id IS NULL)
                OR EXISTS
                       (SELECT 'Jobs not related to EAM WO or Closed WIP Jobs'
                          FROM po_distributions_all pod1, po_lines_all pltv, wip_entities we
                         WHERE     pod1.po_line_id = pltv.po_line_id
                               AND pod1.line_location_id = poll.line_location_id
                               AND pod1.wip_entity_id = we.wip_entity_id
                               AND we.entity_type NOT IN (6, 7, 3))
                OR EXISTS
                       (SELECT 'Open EAM WO Receipts'
                          FROM po_distributions_all pod1
                              ,po_lines_all pltv
                              ,wip_entities we
                              ,wip_discrete_jobs wdj
                         WHERE     pod1.line_location_id = poll.line_location_id
                               AND pod1.wip_entity_id = we.wip_entity_id
                               AND we.wip_entity_id = wdj.wip_entity_id
                               AND we.entity_type = 6
                               AND wdj.status_type IN (3, 4, 6)))
/

-- End of DDL Script for View APPS.XXEIS_RCV_ENTER_RECEIPTS_PO
