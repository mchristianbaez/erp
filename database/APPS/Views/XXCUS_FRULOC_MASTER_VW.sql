CREATE OR REPLACE FORCE VIEW "APPS"."XXCUS_FRULOC_MASTER_VW" ("FRULOC", "FRU", "LOC", "SEGMENT_DESCRIPTION", "FRU_DESCRIPTION", "BU_DESCRIPTION", "POS_SYSTEM", "PRIMARY_OPS_TYPE", "OPERATIONS_TYPE", "ASSIGNMENT_TYPE", "OPS_STATUS", "EMP_STATUS", "CURRENCY", "LANGUAGE", "PRIMARY_BLD", "TIMEZONE", "FRU_OPEN_DATE", "FPS_ID", "LOB_NAME", "FPS_DESC", "EMP_SPACE_ASSIGN_ID", "LOCATION_ID", "EMP_ASSIGN_START_DATE", "PROPERTY_ID", "ALLOCATED_AREA_PCT", "ALLOCATED_AREA", "UTILIZED_AREA", "LOB_BRANCH", "LOCATION_TYPE", "LOC_DESC", "LONGITUDE", "LATITUDE", "LOCATION_CODE", "LOCATION_CODE_LVL1", "LOCATION_CODE_LVL2", "LOCATION_CODE_LVL3", "LOC_SQUARE_FEET", "LOC_ACRE", "OPERATING_HOURS", "WALK_IN", "CUSTOMER_SITE", "SIGNAGE", "OPERATIONS_OPEN_DATE", "OPERATIONS_CLOSED_DATE", "OPERATIONS_SQUARE_FOOTAGE", "OPERATIONS_ACREAGE", "UOM_CODE", "RENTABLE_AREA", "USABLE_AREA", "ADDRESS_LINE1", "ADDRESS_LINE2", "COUNTY", "CITY", "STATE", "PROVINCE", "COUNTRY", "ZIP_CODE", "ADDRESS_ID", "ASSIGNMENT_TYPE_DESCR", "SITE_TYPE", "FRU_COUNT", "LOC_COUNT", "PHONE", "FAX", "FED_TAX_ID", "BRANCH_MANAGER", "EMP_COUNT", "LOCATION_OPEN_DATE", "LOCATION_CLOSE_DATE", "TENANCY_TYPE", "COUNTRY_ABBR", "MARKET_CLUSTER", "LOC_IMAGE") AS 
  SELECT DISTINCT
          empsp.attribute1 || '-' || loc1.location_code fruloc,
          empsp.attribute1 fru,
          loc1.location_code LOC,
          gldcr.product segment_description,
          gldcr.location fru_description,
          rollups.bu_desc bu_desc,
          gldcr.pos_system,
          loc.space_type_lookup_code primary_ops_type,
          loc_space.meaning operations_type,
          loc.standard_type_lookup_code assignment_type,
          CASE
             WHEN TRUNC (empsp.emp_assign_start_date) > TRUNC (SYSDATE)
             THEN
                'FUTURE'
             WHEN empsp.emp_assign_end_date IS NOT NULL
             THEN
                'INACTIVE'
             ELSE
                'ACTIVE'
          END
             ops_status,
          CASE
             WHEN empsp.emp_assign_end_date IS NOT NULL
             THEN
                'INACTIVE'
             WHEN loc.standard_type_lookup_code = 'REAL'
             THEN
                'INACTIVE'
             WHEN     (   loc.standard_type_lookup_code = 'PPL'
                       OR loc.standard_type_lookup_code = 'BOTH')
                  AND TRUNC (empsp.emp_assign_start_date) <= TRUNC (SYSDATE)
             THEN
                'ACTIVE'
             ELSE
                'ACTIVE'
          END
             emp_status,
          prop.attribute2 currency,
          prop.attribute3 LANGUAGE,
          prop.attribute4 primary_bld,
          prop_zone.meaning timezone,
          gldcr.fru_create_date fru_open_date,
          rollups.fps_id                                      --SR 34543 Start
                                                    --rollups.sbu_id LOB_NAME,
          ,
          rollups.bu_id lob_name                                --SR 34543 End
                                ,
          rollups.fps_desc,
          empsp.emp_space_assign_id,
          empsp.location_id,
          empsp.emp_assign_start_date,
          empsp.cost_center_code,
          empsp.allocated_area_pct,
          empsp.allocated_area,
          empsp.utilized_area,
          corp.lob_branch,
          DECODE (loc.location_type_lookup_code,
                  'OFFICE', 'SECTION',
                  'SECTION', 'YARD',
                  loc.location_type_lookup_code)
             location_type,
          DECODE (loc.location_type_lookup_code,
                  'BUILDING', loc.building,
                  'FLOOR', loc.FLOOR,
                  'OFFICE', loc.office)
             loc_desc,
          loc1.attribute1 longitude,
          loc1.attribute2 latitude,
          prop.property_code location_code,
          loc1.location_code location_code_lvl1,
          loc2.location_code location_code_lvl2,
          loc.location_code location_code_lvl3,
          loc1.gross_area loc_sqaure_feet,
          loc1.gross_area / 43560 loc_acre,
          loc.attribute3 operating_hours,
          loc.attribute4 walk_in,
          loc.attribute5 customer_site,
          loc_feat.description signage,
          empsp.emp_assign_start_date operations_open_date          --sr 50860
 /*                decode(loc.active_end_date,
 to_date('31-DEC-4712', 'DD-MON-YYYY'),
 NULL,
 loc.active_end_date) Operations_closed_date,*/
          ,
          DECODE (empsp.emp_assign_end_date,
                  --sr 50860
                  TO_DATE ('31-DEC-4712', 'DD-MON-YYYY'), NULL,
                  empsp.emp_assign_end_date)
             operations_closed_date,
          loc.assignable_area operations_square_footage,
          loc.assignable_area / 43560 operations_acreage,
          loc.uom_code,
          loc.rentable_area,
          loc.usable_area,
          addr.address_line1,
          addr.address_line2,
          addr.county,
          addr.city,
          addr.state,
          addr.province,
          DECODE (addr.country,
                  'US', 'United States',
                  'CA', 'Canada',
                  --v1.1
                  'IN', 'India',
                  'CN', 'China',
                  addr.country)
             country,
          addr.zip_code,
          loc1.address_id,
          loc_assign.meaning assignment_type_descr,
          loc.function_type_lookup_code site_type,
          (SELECT COUNT (DISTINCT a.fru)
             FROM xxcus.xxcuspn_ld_financial_det_tbl a
            WHERE a.location_code =
                     NVL (prop.property_code, loc1.location_code))
             fru_count,
          (SELECT COUNT (DISTINCT b.location_code)
             FROM xxcus.xxcuspn_ld_opsover_tbl b
            WHERE b.fru = empsp.attribute1)
             loc_count /*               ,(SELECT t.phone_number             -- RFC 35962 Start
                                          FROM pn.pn_phones_all   t
                                              ,pn.pn_contacts_all y
                                         WHERE t.contact_id = y.contact_id
                                           AND y.company_site_id IN
                                               (SELECT company_site_id
                                                  FROM pn.pn_company_sites_all h
                                                      ,pn.pn_companies_all     c
                                                 WHERE h.company_id = c.company_id
                                                   AND c.company_number = rollups.bu_id
                                                   AND substr(h.name, 1,
                                                              (instr(h.name, ' - TELECOM') - 1)) =
                                                       loc1.location_code)
                                           AND t.phone_type = 'GEN') phone*/
                      ,
          (SELECT phone_number
             FROM hdsoracle.hds_phone_tbl@apxprd_lnk
            WHERE     phone_type = 'Main Number'
                  AND locid = loc1.location_code
                  AND fru = empsp.attribute1
                  AND ROWNUM=1    --  SR 520376
                  )
             phone /*               ,(SELECT t.phone_number
                                      FROM pn.pn_phones_all   t
                                          ,pn.pn_contacts_all y
                                     WHERE t.contact_id = y.contact_id
                                       AND y.company_site_id IN
                                           (SELECT company_site_id
                                              FROM pn.pn_company_sites_all h
                                                  ,pn.pn_companies_all     c
                                             WHERE h.company_id = c.company_id
                                               AND c.company_number = rollups.bu_id
                                               AND substr(h.name, 1,
                                                          (instr(h.name, ' - TELECOM') - 1)) =
                                                   loc1.location_code)
                                       AND t.phone_type = 'FAX') fax*/
                  ,
          (SELECT phone_number
             FROM hdsoracle.hds_phone_tbl@apxprd_lnk
            WHERE     phone_type = 'FAX'
                  AND locid = loc1.location_code
                  AND fru = empsp.attribute1
                  AND ROWNUM=1    --  SR 520376
                  )
             fax                                              -- RFC 35962 End
                ,
          (SELECT DISTINCT sale.federal_tax_id
             FROM hdsoracle.pnld_salestax@apxprd_lnk.hsi.hughessupply.com sale
            WHERE     sale.fru = empsp.attribute1
                  AND sale.loc_id = prop.property_code)
             fed_tax_id,
          NULL branch_manager,
          0 emp_count                                         --SR 34678 START
                     ,
          loc3.active_start_date location_open_date,
          DECODE (loc3.active_end_date,
                  TO_DATE ('31-DEC-4712', 'DD-MON-YYYY'), NULL,
                  loc3.active_end_date)
             location_close_date,
          prop_tenure2.meaning tenancy_type                     --SR 34678 END
                                           ,
          addr.country country_abbr                                     --v1.1
                                   --adding market_cluster
          ,
          prop.attribute5 market_cluster,
          loc1.attribute7 loc_image
     FROM pn_space_assign_emp_all empsp,
          pn_locations_all loc,
          pn_addresses_all addr,
          pn.pn_properties_all prop,
          (SELECT meaning, lookup_code
             FROM fnd_lookup_values
            WHERE lookup_type LIKE 'PN_SPACE_TYPE') loc_space,
          xxcus.xxcuspn_ld_gldesc_tbl gldcr,
          (SELECT loc2.location_id,
                  loc2.parent_location_id,
                  loc2.active_end_date,
                  loc2.location_code,
                  loc2.space_type_lookup_code
             FROM pn.pn_locations_all loc2) loc2,
          (SELECT loc1.location_id,
                  loc1.parent_location_id,
                  gross_area,
                  loc1.rentable_area                               --SR 104204
                                    ,
                  loc1.usable_area                                 --SR 104204
                                  ,
                  address_id,
                  loc1.property_id,
                  loc1.building,
                  loc1.attribute1,
                  loc1.attribute2,
                  loc1.location_code,
                  loc1.active_start_date                          -- SR 104204
                                        ,
                  loc1.active_end_date,
                  loc1.attribute7
             FROM pn.pn_locations_all loc1) loc1,
          --SR 34678 START
          (SELECT loc3.location_id,
                  loc3.parent_location_id,
                  loc3.address_id,
                  loc3.property_id,
                  loc3.building,
                  loc3.location_code,
                  loc3.lease_or_owned,
                  loc3.active_end_date,
                  loc3.active_start_date
             FROM pn.pn_locations_all loc3
            WHERE     loc3.location_type_lookup_code IN ('BUILDING', 'LAND')
                  AND loc3.active_start_date =
                         (SELECT MAX (la.active_start_date)
                            FROM pn.pn_locations_all la
                           WHERE loc3.location_code = la.location_code)) loc3,
          (SELECT meaning, lookup_code
             FROM fnd_lookup_values
            WHERE lookup_type = 'PN_LEASED_OR_OWNED') prop_tenure2 --SR 34678 END
                                                                  ,
          (SELECT fru, entrp_loc, lob_branch FROM apps.xxcus_location_code_vw) corp,
          (SELECT meaning, lookup_code
             FROM fnd_lookup_values
            WHERE lookup_type = 'PN_FUNCTION_TYPE') loc_site,
          (SELECT meaning, lookup_code
             FROM fnd_lookup_values
            WHERE lookup_type = 'PN_STANDARD_TYPE') loc_assign,
          apps.xxcuspn_ld_lob_rollup_vw rollups,
          (SELECT meaning, lookup_code
             FROM fnd_lookup_values
            WHERE lookup_type = 'PN_ZONE_TYPE') prop_zone,
          (SELECT location_feature_id, location_id, description
             FROM pn_location_features_all
            WHERE location_feature_lookup_code = 'SIGN') loc_feat
    WHERE     empsp.location_id = loc.location_id(+)
          AND loc1.address_id = addr.address_id(+)
          AND loc1.property_id = prop.property_id(+)
          AND empsp.cost_center_code = gldcr.segment2(+)
          AND rollups.fps_id = gldcr.segment1
          AND loc.standard_type_lookup_code = loc_assign.lookup_code(+)
          AND loc.function_type_lookup_code = loc_site.lookup_code(+)
          AND loc.parent_location_id = loc2.location_id
          AND loc2.parent_location_id = loc1.location_id
          --SR 34678 START
          AND loc3.location_id = loc1.location_id
          AND loc3.lease_or_owned = prop_tenure2.lookup_code(+)
          --SR 34678 END
          AND empsp.attribute1 = corp.fru
          AND loc.space_type_lookup_code = loc_space.lookup_code(+)
          AND gldcr.l_corp_id = empsp.attribute1
          /*   AND loc.active_end_date = '31-DEC-4712'      -- SR 104204
          AND loc1.active_end_date = '31-DEC-4712'
          AND loc2.active_end_date = '31-DEC-4712'*/
          AND NVL (empsp.emp_assign_end_date,
                   TO_DATE ('31-DEC-4712', 'DD-MON-YYYY')) =
                 (SELECT MAX (
                            NVL (spc.emp_assign_end_date,
                                 TO_DATE ('31-DEC-4712', 'DD-MON-YYYY')))
                    FROM pn.pn_space_assign_emp_all spc
                   WHERE     spc.attribute1 = empsp.attribute1
                         AND spc.location_id = empsp.location_id)
          AND prop.zone = prop_zone.lookup_code(+)
          AND loc1.location_id = loc_feat.location_id(+)
          -- SR 104204 BEGIN
          AND loc.active_start_date =
                 (SELECT MAX (active_start_date)
                    FROM pn.pn_locations_all
                   WHERE location_code = loc.location_code)
-- SR 104204 END
;
