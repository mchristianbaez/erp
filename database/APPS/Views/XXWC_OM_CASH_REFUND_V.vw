--
-- XXWC_OM_CASH_REFUND_V  (View)
--

CREATE OR REPLACE FORCE VIEW APPS.XXWC_OM_CASH_REFUND_V
(
   CASH_RECEIPT_ID
  ,RECEIPT_NUMBER
  ,RECEIPT_DATE
  ,PAYMENT_TYPE_CODE
  ,REFUND_DATE
  ,REFUND_AMOUNT
  ,CHECK_REFUND_AMOUNT
)
AS
   SELECT b.cash_receipt_id
         ,a.receipt_number
         ,a.receipt_date
         ,b.payment_type_code
         ,b.refund_date
         ,b.refund_amount
         ,b.check_refund_amount
     FROM ar_cash_receipts a, apps.xxwc_om_cash_refund_tbl b
    WHERE a.cash_receipt_id = b.cash_receipt_id;


