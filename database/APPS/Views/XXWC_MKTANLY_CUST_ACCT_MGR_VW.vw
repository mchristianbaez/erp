CREATE OR REPLACE VIEW APPS.XXWC_MKTANLY_CUST_ACCT_MGR_VW
AS
  /********************************************************************************************************************************
  --   NAME:       apps.XXWC_MKTANLY_CUST_ACCT_MGR_VW
  -- REVISIONS:
  --   Ver        Date        Author           Description
  --   ---------  ----------  ---------------  ------------------------------------
  --   1.0        09/05/2018   P.Vamshidhar      TMS#20180907-00001 -  Building Customer Outbound MV's for Marketing Analytics
  **********************************************************************************************************************************/
  SELECT DISTINCT hca.account_number,
    rsa.salesrep_number,
    rsa1.resource_name Salesrep,
    (SELECT phone_number
    FROM apps.per_phones
    WHERE parent_table='PER_ALL_PEOPLE_F'
    AND parent_id     =rsa.employee_id
    AND rownum        =1
    ) Accounts_Salesrep_Phone_Num,
    (SELECT email_address
    FROM apps.per_all_people_f
    WHERE person_id = rsa.employee_id
    AND rownum      =1
    ) email_address,
    rsa.salesrep_ntid Accounts_Salesrep_NT_ID,
    (SELECT xpea.job_descr
    FROM apps.per_all_people_f PAPF,
      XXCUS.XXCUSHR_PS_EMP_ALL_TBL XPEA
    WHERE PAPF.employee_number = xpea.employee_number
    AND papf.person_id         = rsa.employee_id
    AND rownum                 =1
    ) Account_Salesrep_Job_Desc,
    rsa.sales_rep_creation_date Created_Date,
    rsa.sales_rep_last_update_date Last_Update_Date
  FROM apps.hz_cust_accounts hca,
    apps.hz_cust_acct_sites_all hcas,
    apps.hz_cust_site_uses_all hcsu,
    APPS.XXWC_JTF_SALESREPS_VW rsa,
    APPS.JTF_RS_DEFRESOURCES_V rsa1
  WHERE 1                      =1
  AND hca.cust_account_id      =hcas.cust_account_id
  AND hcas.cust_acct_site_id   = hcsu.cust_acct_site_id
  AND hcsu.primary_flag        ='Y'
  AND hcsu.SITE_USE_CODE       ='BILL_TO'
  AND rsa.operating_unit_id    =162
  AND hcsu.primary_salesrep_id = rsa.salesrep_id (+)
  AND rsa.resource_id          = rsa1.resource_id 
  /