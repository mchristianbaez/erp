CREATE OR REPLACE VIEW XXWC_GETPAID_AREXTI_TEST_V AS
SELECT rcta.org_id , hca.account_number, hca.cust_account_id , rcta.trx_number, apsa.AMOUNT_DUE_REMAINING
  FROM apps.ra_customer_trx rcta
     -- , ra_batch_sources_all rbsa
     , apps.hz_cust_accounts hca
     , ra_terms rt
     , ra_terms_lines_discounts rtld
     , apps.hz_cust_site_uses hcsu_s
     , apps.hz_cust_acct_sites hcas_s
     , hz_party_sites hps_s
     , hz_locations hl_s
     , apps.hz_cust_site_uses hcsu_b
     , apps.hz_cust_acct_sites hcas_b
     , hz_party_sites hps_b
     , hz_locations hl_b
     , apps.ar_payment_schedules apsa
 WHERE 1                        = 1
   AND rcta.bill_to_customer_id = hca.cust_account_id
   AND rcta.customer_trx_id     = apsa.customer_trx_id
   AND rcta.ship_to_site_use_id = hcsu_s.site_use_id
   AND hcas_s.cust_acct_site_id = hcsu_s.cust_acct_site_id
   AND hcas_s.party_site_id     = hps_s.party_site_id
   AND hps_s.location_id        = hl_s.location_id
--   AND hcsu_b.site_use_id       = hcsu_s.bill_to_site_use_id
   AND hcas_b.cust_acct_site_id = hcsu_b.cust_acct_site_id
   AND hca.cust_account_id      = hcas_b.cust_account_id
   AND hcsu_b.site_use_code     = 'BILL_TO'
   AND hcsu_b.primary_flag      = 'Y'
   AND hcas_b.party_site_id     = hps_b.party_site_id
   AND hps_b.location_id        = hl_b.location_id
   AND rcta.term_id             = rt.term_id (+)
   AND rt.term_id               = rtld.term_id (+)
--   AND NVL(rcta.interface_header_context, '&*^%$#@') <> 'CONVERSION'
   -- AND rcta.batch_source_id        = rbsa.batch_source_id
   -- AND rcta.org_id                 = rbsa.org_id
   AND NVL(xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id ), '&*^%$#@')   <> 'CONVERSION'
   AND apsa.status = 'OP' AND apsa.amount_due_remaining != 0 AND apsa.org_id = rcta.org_id
UNION
------------------------------------------------------------------------------------------------------------------------------
-- NON-CONVERSION Invoices
-- rcta.ship_to_site_use_id IS NULL AND rcta.ship_to_address_id IS NOT NULL
------------------------------------------------------------------------------------------------------------------------------
SELECT rcta.org_id  , hca.account_number, hca.cust_account_id , rcta.trx_number, apsa.AMOUNT_DUE_REMAINING
  FROM apps.ra_customer_trx rcta
     -- , ra_batch_sources_all rbsa
     , apps.hz_cust_accounts hca
     , ra_terms rt
     , ra_terms_lines_discounts rtld
     , apps.hz_cust_site_uses hcsu_s
     , apps.hz_cust_acct_sites hcas_s
     , hz_party_sites hps_s
     , hz_locations hl_s
     , apps.hz_cust_site_uses hcsu_b
     , apps.hz_cust_acct_sites hcas_b
     , hz_party_sites hps_b
     , hz_locations hl_b
     , apps.ar_payment_schedules apsa
 WHERE 1 = 1
   AND rcta.bill_to_customer_id = hca.cust_account_id
   AND rcta.customer_trx_id     =  apsa.customer_trx_id
   AND rcta.ship_to_address_id  =  hcas_s.cust_acct_site_id
   AND rcta.ship_to_site_use_id IS NULL
   AND hcas_s.cust_acct_site_id = hcsu_s.cust_acct_site_id
   AND hcas_s.party_site_id     = hps_s.party_site_id
   AND hps_s.location_id        = hl_s.location_id
   AND hca.cust_account_id      = hcas_b.cust_account_id
   AND hcas_b.cust_acct_site_id = hcsu_b.cust_acct_site_id
   AND hcsu_s.site_use_code     = 'SHIP_TO'
   AND hcsu_b.site_use_code     = 'BILL_TO'
--   AND hcsu_b.site_use_id       = hcsu_s.bill_to_site_use_id
   AND hcsu_b.primary_flag = 'Y'
   AND hcas_b.party_site_id     = hps_b.party_site_id
   AND hps_b.location_id        = hl_b.location_id
   AND rcta.term_id             = rt.term_id (+)
   AND rt.term_id               = rtld.term_id (+)
--   AND NVL(rcta.interface_header_context, '&*^%$#@') <> 'CONVERSION'
   -- AND rcta.batch_source_id        = rbsa.batch_source_id
   -- AND rcta.org_id                 = rbsa.org_id
   AND NVL(xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id ), '&*^%$#@')   <> 'CONVERSION'
   AND apsa.status = 'OP' AND apsa.amount_due_remaining != 0 AND apsa.org_id = rcta.org_id
UNION
------------------------------------------------------------------------------------------------------------------------------
-- CONVERSION Invoices
-- rcta.ship_to_site_use_id IS NOT NULL
------------------------------------------------------------------------------------------------------------------------------
SELECT rcta.org_id  , hca.account_number, hca.cust_account_id , rcta.trx_number, apsa.AMOUNT_DUE_REMAINING
  FROM apps.ra_customer_trx rcta
     -- , ra_batch_sources_all rbsa
     , apps.hz_cust_accounts hca
     , ra_terms rt
     , ra_terms_lines_discounts rtld
     , apps.hz_cust_site_uses hcsu_s
     , apps.hz_cust_acct_sites hcas_s
     , hz_party_sites hps_s
     , hz_locations hl_s
     , apps.hz_cust_site_uses hcsu_b
     , apps.hz_cust_acct_sites hcas_b
     , hz_party_sites hps_b
     , hz_locations hl_b
     , apps.ar_payment_schedules apsa
     , xxwc.xxwc_arexti_getpaid_dump_tbl gp_dmp
 WHERE 1                        = 1
   AND rcta.bill_to_customer_id = hca.cust_account_id
   AND rcta.customer_trx_id     = apsa.customer_trx_id
   AND rcta.ship_to_site_use_id = hcsu_s.site_use_id
   AND hcas_s.cust_acct_site_id = hcsu_s.cust_acct_site_id
   AND hcas_s.party_site_id     = hps_s.party_site_id
   AND hps_s.location_id        = hl_s.location_id
   AND hca.cust_account_id      = hcas_b.cust_account_id
   AND hcas_b.cust_acct_site_id = hcsu_b.cust_acct_site_id
   AND hcsu_b.site_use_code     = 'BILL_TO'
   AND hcsu_b.primary_flag      = 'Y'
--   AND hcsu_b.site_use_id       = hcsu_s.bill_to_site_use_id
   AND hcas_b.party_site_id     = hps_b.party_site_id
   AND hps_b.location_id        = hl_b.location_id
   AND rcta.term_id             = rt.term_id (+)
   AND rt.term_id               = rtld.term_id (+)
   AND rcta.trx_number          = gp_dmp.invno
   AND hca.account_number       = gp_dmp.custno
--   AND NVL(rcta.interface_header_context, '&*^%$#@') = 'CONVERSION'
   -- AND rcta.batch_source_id        = rbsa.batch_source_id
   -- AND rcta.org_id                 = rbsa.org_id
   AND NVL(xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id ), '&*^%$#@')   = 'CONVERSION'
   AND apsa.status = 'OP' AND apsa.amount_due_remaining != 0 AND apsa.org_id = rcta.org_id
UNION
------------------------------------------------------------------------------------------------------------------------------
-- CONVERSION Invoices
-- SHIP_TO_SITE_USE_ID IS NULL AND rcta.ship_to_address_id IS NOT NULL
------------------------------------------------------------------------------------------------------------------------------
SELECT rcta.org_id  , hca.account_number, hca.cust_account_id , rcta.trx_number, apsa.AMOUNT_DUE_REMAINING
  FROM apps.ra_customer_trx rcta
     -- , ra_batch_sources_all rbsa
     , apps.hz_cust_accounts hca
     , ra_terms rt
     , ra_terms_lines_discounts rtld
     , apps.hz_cust_site_uses hcsu_s
     , apps.hz_cust_acct_sites hcas_s
     , hz_party_sites hps_s
     , hz_locations hl_s
     , apps.hz_cust_site_uses hcsu_b
     , apps.hz_cust_acct_sites hcas_b
     , hz_party_sites hps_b
     , hz_locations hl_b
     , apps.ar_payment_schedules apsa
     , xxwc.xxwc_arexti_getpaid_dump_tbl gp_dmp
 WHERE 1 = 1
   AND rcta.bill_to_customer_id = hca.cust_account_id
   AND rcta.customer_trx_id =  apsa.customer_trx_id
   AND rcta.ship_to_address_id =  hcas_s.cust_acct_site_id
   AND rcta.ship_to_site_use_id IS NULL
   AND hcas_s.cust_acct_site_id = hcsu_s.cust_acct_site_id
   AND hcas_s.party_site_id = hps_s.party_site_id
   AND hps_s.location_id = hl_s.location_id
   AND hca.cust_account_id = hcas_b.cust_account_id
   AND hcas_b.cust_acct_site_id = hcsu_b.cust_acct_site_id
   AND hcsu_s.site_use_code = 'SHIP_TO'
   AND hcsu_b.site_use_code = 'BILL_TO'
   AND hcsu_b.primary_flag = 'Y'
--   AND hcsu_b.site_use_id       = hcsu_s.bill_to_site_use_id
   AND hcas_b.party_site_id = hps_b.party_site_id
   AND hps_b.location_id = hl_b.location_id
   AND rcta.term_id = rt.term_id (+)
   AND rt.term_id = rtld.term_id (+)
   AND rcta.trx_number          = gp_dmp.invno
   AND hca.account_number       = gp_dmp.custno
   AND apsa.status = 'OP' AND apsa.amount_due_remaining != 0 AND apsa.org_id = rcta.org_id
--   AND NVL(rcta.interface_header_context, '&*^%$#@') = 'CONVERSION'
   -- AND rcta.batch_source_id        = rbsa.batch_source_id
   -- AND rcta.org_id                 = rbsa.org_id
   AND NVL(xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id ), '&*^%$#@')   = 'CONVERSION'
UNION
------------------------------------------------------------------------------------------------------------------------------
-- NON-CONVERSION
-- rcta.ship_to_site_use_id IS NULL AND rcta.ship_to_address_id IS NULL
------------------------------------------------------------------------------------------------------------------------------
SELECT rcta.org_id , hca.account_number, hca.cust_account_id , rcta.trx_number, apsa.AMOUNT_DUE_REMAINING
  FROM apps.ra_customer_trx rcta
     -- , ra_batch_sources_all rbsa
     , apps.hz_cust_accounts hca
     , ra_terms rt
     , ra_terms_lines_discounts rtld
     , apps.hz_cust_site_uses hcsu_s
     , apps.hz_cust_acct_sites hcas_s
     , hz_party_sites hps_s
     , hz_locations hl_s
     , apps.hz_cust_site_uses hcsu_b
     , apps.hz_cust_acct_sites hcas_b
     , hz_party_sites hps_b
     , hz_locations hl_b
     , apps.ar_payment_schedules apsa
 WHERE 1                        = 1
   AND rcta.bill_to_customer_id = hca.cust_account_id
   AND rcta.customer_trx_id     = apsa.customer_trx_id
   AND rcta.ship_to_site_use_id IS NULL
   AND hca.cust_account_id      = hcas_s.cust_account_id
   AND hcas_s.cust_acct_site_id = hcsu_s.cust_acct_site_id
   AND hcas_s.party_site_id     = hps_s.party_site_id
   AND hps_s.location_id        = hl_s.location_id
   AND hca.cust_account_id      = hcas_b.cust_account_id
   AND hcas_b.cust_acct_site_id = hcsu_b.cust_acct_site_id
   AND hcsu_s.site_use_code     = 'SHIP_TO'
   AND hcsu_s.primary_flag      = 'Y'
   AND hcsu_b.site_use_code     = 'BILL_TO'
   AND hcsu_b.primary_flag      = 'Y'
--   AND hcsu_b.site_use_id       = hcsu_s.bill_to_site_use_id
   AND hcas_b.party_site_id     = hps_b.party_site_id
   AND hps_b.location_id        = hl_b.location_id
   AND rcta.term_id             = rt.term_id (+)
   AND rt.term_id               = rtld.term_id (+)
--   AND NVL(rcta.interface_header_context, '&*^%$#@') <> 'CONVERSION'
   -- AND rcta.batch_source_id        = rbsa.batch_source_id
   -- AND rcta.org_id                 = rbsa.org_id
   AND NVL(xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id ), '&*^%$#@')   <> 'CONVERSION'
   AND apsa.status              = 'OP'
   AND apsa.amount_due_remaining != 0
   AND apsa.org_id              = rcta.org_id
UNION
------------------------------------------------------------------------------------------------------------------------------
-- CONVERSION
-- No Ship-To Customer on Invoice
------------------------------------------------------------------------------------------------------------------------------
SELECT rcta.org_id , hca.account_number, hca.cust_account_id , rcta.trx_number, apsa.AMOUNT_DUE_REMAINING
  FROM apps.ra_customer_trx rcta
     -- , ra_batch_sources_all rbsa
     , apps.hz_cust_accounts hca
     , ra_terms rt
     , ra_terms_lines_discounts rtld
     , apps.hz_cust_site_uses hcsu_s
     , apps.hz_cust_acct_sites hcas_s
     , hz_party_sites hps_s
     , hz_locations hl_s
     , apps.hz_cust_site_uses hcsu_b
     , apps.hz_cust_acct_sites hcas_b
     , hz_party_sites hps_b
     , hz_locations hl_b
     , apps.ar_payment_schedules apsa
     , xxwc.xxwc_arexti_getpaid_dump_tbl gp_dmp
 WHERE 1 = 1
   AND rcta.bill_to_customer_id = hca.cust_account_id
   AND rcta.customer_trx_id =  apsa.customer_trx_id
   AND hca.cust_account_id      = hcas_s.cust_account_id
   AND rcta.ship_to_customer_id IS NULL
   AND hcas_s.cust_acct_site_id = hcsu_s.cust_acct_site_id
   AND hcas_s.party_site_id = hps_s.party_site_id
   AND hps_s.location_id = hl_s.location_id
   AND hca.cust_account_id = hcas_b.cust_account_id
   AND hcas_b.cust_acct_site_id = hcsu_b.cust_acct_site_id
   AND hcsu_s.site_use_code = 'SHIP_TO'
   AND hcsu_b.site_use_code = 'BILL_TO'
   AND hcsu_b.primary_flag = 'Y'
--   AND hcsu_b.site_use_id       = hcsu_s.bill_to_site_use_id
   AND hcsu_s.primary_flag = 'Y'
   AND hcas_b.party_site_id = hps_b.party_site_id
   AND hps_b.location_id = hl_b.location_id
   AND rcta.term_id = rt.term_id (+)
   AND rt.term_id = rtld.term_id (+)
   AND rcta.trx_number          = gp_dmp.invno
   AND hca.account_number       = gp_dmp.custno
   AND apsa.status = 'OP' AND apsa.amount_due_remaining != 0 AND apsa.org_id = rcta.org_id
--   AND NVL(rcta.interface_header_context, '&*^%$#@') = 'CONVERSION'
   -- AND rcta.batch_source_id        = rbsa.batch_source_id
   -- AND rcta.org_id                 = rbsa.org_id
   AND NVL(xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id ), '&*^%$#@')   = 'CONVERSION'
UNION
------------------------------------------------------------------------------------------------------------------------------
-- CONVERSION
-- No Ship-To Site for Customer
------------------------------------------------------------------------------------------------------------------------------
SELECT rcta.org_id , hca.account_number, hca.cust_account_id , rcta.trx_number, apsa.AMOUNT_DUE_REMAINING
  FROM apps.ra_customer_trx rcta
     -- , ra_batch_sources_all rbsa
     , apps.hz_cust_accounts hca
     , ra_terms rt
     , ra_terms_lines_discounts rtld
     , apps.hz_cust_site_uses hcsu_b
     , apps.hz_cust_acct_sites hcas_b
     , hz_party_sites hps_b
     , hz_locations hl_b
     , apps.ar_payment_schedules apsa
     , xxwc.xxwc_arexti_getpaid_dump_tbl gp_dmp
 WHERE 1 = 1
   AND rcta.bill_to_customer_id = hca.cust_account_id
   AND rcta.customer_trx_id =  apsa.customer_trx_id
   AND rcta.ship_to_customer_id IS NULL
   AND NOT EXISTS (SELECT '1'
                     FROM apps.hz_cust_acct_sites hcas_s
                        , apps.hz_cust_site_uses  hcsu_s
                    WHERE 1 = 1
                      AND hcas_s.cust_account_id   = rcta.bill_to_customer_id
                      AND hcas_s.cust_acct_site_id = hcsu_s.cust_acct_site_id
                      AND hcas_s.org_id            = rcta.org_id
                      AND hcsu_s.site_use_code     = 'SHIP_TO')
   AND hca.cust_account_id = hcas_b.cust_account_id
   AND hcas_b.cust_acct_site_id = hcsu_b.cust_acct_site_id
   AND hcsu_b.site_use_code = 'BILL_TO'
   AND hcsu_b.primary_flag = 'Y'
   AND hcas_b.party_site_id = hps_b.party_site_id
   AND hps_b.location_id = hl_b.location_id
   AND rcta.term_id = rt.term_id (+)
   AND rt.term_id = rtld.term_id (+)
   AND rcta.trx_number          = gp_dmp.invno
   AND hca.account_number       = gp_dmp.custno
   AND apsa.status = 'OP' AND apsa.amount_due_remaining != 0 AND apsa.org_id = rcta.org_id
--   AND NVL(rcta.interface_header_context, '&*^%$#@') = 'CONVERSION'
   -- AND rcta.batch_source_id        = rbsa.batch_source_id
   -- AND rcta.org_id                 = rbsa.org_id
   AND NVL(xxwc_ar_getpaid_ob_pkg.get_inv_source(rcta.customer_trx_id  , rcta.org_id ), '&*^%$#@')   = 'CONVERSION'
UNION
------------------------------------------------------------------------------------------------------------------------------
-- CONVERSION
-- Cash Receipts
------------------------------------------------------------------------------------------------------------------------------
SELECT acra.ORG_ID , hca.account_number, hca.cust_account_id , acra.receipt_number, apsa.AMOUNT_DUE_REMAINING
  FROM apps.ar_cash_receipts     acra
     , apps.hz_cust_accounts     hca
     , apps.hz_cust_site_uses hcsu_b
     , apps.hz_cust_acct_sites hcas_b
     , hz_party_sites hps_b
     , hz_locations hl_b
     , apps.ar_payment_schedules apsa
     , ar_receipt_methods       arm
     , ar_receipt_classes       arc
--     , hr_locations_all         hla
     , xxwc.xxwc_armast_getpaid_dump_tbl dmp
 WHERE 1 = 1
   AND hcsu_b.site_use_id         = acra.customer_site_use_id
   AND hcas_b.cust_acct_site_id = hcsu_b.cust_acct_site_id
   AND hcsu_b.site_use_code     = 'BILL_TO'
   AND hcas_b.party_site_id     = hps_b.party_site_id
   AND hps_b.location_id        = hl_b.location_id
   AND acra.type                    = 'CASH'
   AND acra.cash_receipt_id         = apsa.cash_receipt_id
   AND hca.cust_account_id          = apsa.customer_id
   AND apsa.amount_due_remaining   <> 0
   AND acra.receipt_method_id       = arm.receipt_method_id
   AND arm.receipt_class_id         = arc.receipt_class_id
   AND arc.name                     = 'CONV - Receipt'
--   AND SUBSTR(hla.location_code, 1, INSTR(hla.location_code, '-') - 2) = NVL(arm.attribute2,'WCC')
   AND TO_CHAR(acra.receipt_number) = dmp.INVNO
   AND hca.cust_account_id          = acra.pay_from_customer
   AND hca.account_number           = dmp.custno
UNION
------------------------------------------------------------------------------------------------------------------------------
-- CONVERSION
-- Cash Receipts, Customer# on hca.attribute16
------------------------------------------------------------------------------------------------------------------------------
SELECT acra.ORG_ID , hca.account_number, hca.cust_account_id , acra.receipt_number, apsa.AMOUNT_DUE_REMAINING
  FROM apps.ar_cash_receipts     acra
     , apps.hz_cust_accounts     hca
     , apps.hz_cust_site_uses hcsu_b
     , apps.hz_cust_acct_sites hcas_b
     , hz_party_sites hps_b
     , hz_locations hl_b
     , apps.ar_payment_schedules apsa
     , ar_receipt_methods       arm
     , ar_receipt_classes       arc
--     , hr_locations_all         hla
     , xxwc.xxwc_armast_getpaid_dump_tbl dmp
 WHERE 1 = 1
   AND hcsu_b.site_use_id         = acra.customer_site_use_id
   AND hcas_b.cust_acct_site_id = hcsu_b.cust_acct_site_id
   AND hcsu_b.site_use_code     = 'BILL_TO'
   AND hcas_b.party_site_id     = hps_b.party_site_id
   AND hps_b.location_id        = hl_b.location_id
   AND acra.type                    = 'CASH'
   AND acra.cash_receipt_id         = apsa.cash_receipt_id
   AND hca.cust_account_id          = apsa.customer_id
   AND apsa.amount_due_remaining   <> 0
   AND acra.receipt_method_id       = arm.receipt_method_id
   AND arm.receipt_class_id         = arc.receipt_class_id
   AND arc.name                     = 'CONV - Receipt'
--   AND SUBSTR(hla.location_code, 1, INSTR(hla.location_code, '-') - 2) = NVL(arm.attribute2,'WCC')
   AND TO_CHAR(acra.receipt_number) = dmp.INVNO
   AND hca.cust_account_id          = acra.pay_from_customer
   AND hca.account_number           != dmp.custno
   AND hca.attribute6               = dmp.custno
UNION
------------------------------------------------------------------------------------------------------------------------------
-- NON-CONVERSION
-- Cash Receipts, ReceiptMethod != 'WC PRISM Lockbox'
------------------------------------------------------------------------------------------------------------------------------
SELECT acra.ORG_ID , hca.account_number, hca.cust_account_id , acra.receipt_number, apsa.AMOUNT_DUE_REMAINING
  FROM apps.ar_cash_receipts     acra
     , apps.hz_cust_accounts     hca
     , apps.hz_cust_site_uses hcsu_b
     , apps.hz_cust_acct_sites hcas_b
     , hz_party_sites hps_b
     , hz_locations hl_b
     , apps.ar_payment_schedules apsa
     , ar_receipt_methods       arm
     , ar_receipt_classes       arc
--     , hr_locations_all         hla
 WHERE 1 = 1
   AND hcsu_b.site_use_id         = acra.customer_site_use_id
   AND hcas_b.cust_acct_site_id = hcsu_b.cust_acct_site_id
   AND hcsu_b.site_use_code     = 'BILL_TO'
   AND hcas_b.party_site_id     = hps_b.party_site_id
   AND hps_b.location_id        = hl_b.location_id
   AND acra.type                    = 'CASH'
   AND acra.cash_receipt_id         = apsa.cash_receipt_id
   AND hca.cust_account_id          = apsa.customer_id
   AND apsa.amount_due_remaining   <> 0
   AND acra.receipt_method_id       = arm.receipt_method_id
   AND arm.receipt_class_id         = arc.receipt_class_id
   AND arc.name                     != 'CONV - Receipt'
   AND arm.name                     != 'WC PRISM Lockbox'
--   AND SUBSTR(hla.location_code, 1, INSTR(hla.location_code, '-') - 2) = NVL(arm.attribute2,'WCC')
   AND hca.cust_account_id          = acra.pay_from_customer
UNION
------------------------------------------------------------------------------------------------------------------------------
-- NON-CONVERSION
-- Cash Receipts, ReceiptMethod = 'WC PRISM Lockbox'
------------------------------------------------------------------------------------------------------------------------------
SELECT acra.ORG_ID , hca.account_number, hca.cust_account_id , acra.receipt_number, apsa.AMOUNT_DUE_REMAINING
  FROM apps.ar_cash_receipts     acra
     , apps.hz_cust_accounts     hca
     , apps.hz_cust_site_uses hcsu_b
     , apps.hz_cust_acct_sites hcas_b
     , hz_party_sites hps_b
     , hz_locations hl_b
     , apps.ar_payment_schedules apsa
     , ar_receipt_methods       arm
     , ar_receipt_classes       arc
--     , hr_locations_all         hla
 WHERE 1 = 1
   AND hcsu_b.site_use_id         = acra.customer_site_use_id
   AND hcas_b.cust_acct_site_id = hcsu_b.cust_acct_site_id
   AND hcsu_b.site_use_code     = 'BILL_TO'
   AND hcas_b.party_site_id     = hps_b.party_site_id
   AND hps_b.location_id        = hl_b.location_id
   AND acra.type                    = 'CASH'
   AND acra.cash_receipt_id         = apsa.cash_receipt_id
   AND hca.cust_account_id          = apsa.customer_id
   AND apsa.amount_due_remaining   <> 0
   AND acra.receipt_method_id       = arm.receipt_method_id
   AND arm.receipt_class_id         = arc.receipt_class_id
   AND arc.name                     != 'CONV - Receipt'
   AND arm.name                     = 'WC PRISM Lockbox'
--   AND SUBSTR(hla.location_code, 1, INSTR(hla.location_code, '-') - 2) = NVL(arm.attribute2,'WCC')
   AND hca.cust_account_id          = acra.pay_from_customer;
