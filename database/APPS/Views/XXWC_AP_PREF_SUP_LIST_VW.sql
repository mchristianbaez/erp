CREATE OR REPLACE VIEW APPS.XXWC_AP_PREF_SUP_LIST_VW
/*************************************************************************
  $Header XXWC_AP_PREF_SUP_LIST_VW.sql $
  Module Name: XXWC_AP_PREF_SUP_LIST_VW

  PURPOSE: View for Supplier Names

  TMS Task Id :  20141104-00115

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    ----------------
  1.0        29-Oct-2014  Manjula Chellappan    Initial Version
**************************************************************************/
AS
   SELECT a.vendor_id supplier_id,
          a.segment1 supplier_num,
          a.vendor_name supplier_name,
          attribute3 supplier_tier,
          attribute4 supplier_sba
     FROM ap_suppliers a
    WHERE     a.enabled_flag = 'Y'
          AND TRUNC (SYSDATE) BETWEEN TRUNC (
                                         NVL (start_date_active, SYSDATE - 1))
                                  AND TRUNC (
                                         NVL (end_date_active, SYSDATE + 1))
          AND vendor_type_lookup_code = 'VENDOR'
   UNION
   SELECT -1, ' All', ' All',NULL,NULL FROM DUAL
ORDER BY 3