--
-- XXWC_GETPAID_ARTRAN_V  (View)
--

CREATE OR REPLACE FORCE VIEW APPS.XXWC_GETPAID_ARTRAN_V
(
   REC_LINE
  ,ORG_ID
)
AS
   SELECT    RPAD (NVL (hca.account_number, ' '), 20, ' ') --  CUSTOMER_NUMBER
          || RPAD (NVL (rcta.trx_number, ' '), 20, ' ') --  TRANSACTION_NUMBER
          || RPAD (NVL (msib.segment1, ' '), 16, ' ') --  INVENTORY_ITEM_NUMBER
          || RPAD (NVL (rctl.description, ' '), 65, ' ')  --  ITEM_DESCRIPTION
          || LPAD ('0', 7, ' ')                  --  SALES_DISCOUNT_PERCENTAGE
          || LPAD (
                NVL (TO_CHAR (rctl.unit_selling_price, '9999999990.99')
                    ,'0.00')
               ,20
               ,' ')                                    --  UNIT_SELLING_PRICE
          || LPAD (NVL (TO_CHAR (rctl.quantity_invoiced), '0'), 12, ' ') --  SHIPMENT_QUANTITY
          || LPAD (
                NVL (TO_CHAR (rctl.extended_amount, '9999999990.99'), '0.00')
               ,20
               ,' ')                                  --  TOTAL_EXTENDED_PRICE
          || LPAD (NVL (TO_CHAR (rctl.line_number), '0'), 10, ' ') --  SEQUENCE_NUMBER
          || LPAD (NVL (TO_CHAR (rctl.line_number), '0'), 10, ' ') --  UNIQUE_ID
          || RPAD (NVL (rctl.uom_code, 'EA'), 10, ' ')     --  UNIT_OF_MEASURE
          || LPAD (
                NVL (TO_CHAR (rctl.quantity_ordered - rctl.quantity_invoiced)
                    ,'0')
               ,12
               ,' ')                                 --  QUANTITY_BACK_ORDERED
          || LPAD (NVL (TO_CHAR (rctl.quantity_ordered), '0'), 12, ' ') --  QUANTITY_ORDERED
          || RPAD (
                NVL (
                   (SELECT DECODE (COUNT (1), 0, 'N', 'T')
                      FROM apps.ra_customer_trx_lines rctl_t
                     WHERE     rctl_t.customer_Trx_id = rctl.customer_trx_id
                           AND rctl_t.link_to_cust_trx_line_id =
                                  rctl.customer_trx_line_id)
                  ,'N')
               ,1
               ,' ')                                     --  TAXABLE_ITEM_FLAG
             REC_LINE
         ,rcta.org_id
     FROM apps.ra_customer_trx rcta
         ,apps.ra_customer_trx_lines rctl
         ,mtl_system_items_b msib
         ,apps.hz_cust_accounts hca
         ,ra_terms rt
         ,ra_terms_lines_discounts rtld
         ,apps.ar_payment_schedules apsa
         ,org_organization_definitions ood
    WHERE     1 = 1
          AND rcta.customer_trx_id = rctl.customer_trx_id
          AND rctl.inventory_item_id = msib.inventory_item_id
          AND ood.organization_id = msib.organization_id
          AND ood.organization_code = 'MST'
          AND NVL (rcta.interface_header_context, '&*^%$#@') NOT IN
                 ('CONVERSION', 'PRISM INVOICES')
          AND rctl.line_type = 'LINE'
          AND hca.cust_account_id = rcta.bill_to_customer_id
          AND rt.term_id = rcta.term_id(+)
          AND rt.term_id = rtld.term_id(+)
          AND rcta.org_id = rctl.org_id
          AND apsa.customer_trx_id = rcta.customer_trx_id
          AND apsa.org_id = rcta.org_id
          AND apsa.status = 'OP'
          AND apsa.amount_due_remaining != 0
          AND rcta.trx_number NOT IN
                 (SELECT dmp.invno
                    FROM xxwc.xxwc_artran_getpaid_dump_tbl dmp)
   UNION
   ------------------------------------------------------------------------------------------------------------------------------
   -- NON-CONVERSION Invoices
   -- With No-Inventory Item
   ------------------------------------------------------------------------------------------------------------------------------
   SELECT    RPAD (NVL (hca.account_number, ' '), 20, ' ') --  CUSTOMER_NUMBER
          || RPAD (NVL (rcta.trx_number, ' '), 20, ' ') --  TRANSACTION_NUMBER
          || RPAD (' ', 16, ' ')                     --  INVENTORY_ITEM_NUMBER
          || RPAD (NVL (rctl.description, ' '), 65, ' ')  --  ITEM_DESCRIPTION
          || LPAD ('0', 7, ' ')                  --  SALES_DISCOUNT_PERCENTAGE
          || LPAD (
                NVL (TO_CHAR (rctl.unit_selling_price, '9999999990.99')
                    ,'0.00')
               ,20
               ,' ')                                    --  UNIT_SELLING_PRICE
          || LPAD (NVL (TO_CHAR (rctl.quantity_invoiced), '0'), 12, ' ') --  SHIPMENT_QUANTITY
          || LPAD (
                NVL (TO_CHAR (rctl.extended_amount, '9999999990.99'), '0.00')
               ,20
               ,' ')                                  --  TOTAL_EXTENDED_PRICE
          || LPAD (NVL (TO_CHAR (rctl.line_number), '0'), 10, ' ') --  SEQUENCE_NUMBER
          || LPAD (NVL (TO_CHAR (rctl.line_number), '0'), 10, ' ') --  UNIQUE_ID
          || RPAD (NVL (rctl.uom_code, 'EA'), 10, ' ')     --  UNIT_OF_MEASURE
          || LPAD (
                NVL (TO_CHAR (rctl.quantity_ordered - rctl.quantity_invoiced)
                    ,'0')
               ,12
               ,' ')                                 --  QUANTITY_BACK_ORDERED
          || LPAD (NVL (TO_CHAR (rctl.quantity_ordered), '0'), 12, ' ') --  QUANTITY_ORDERED
          || RPAD (
                NVL (
                   (SELECT DECODE (COUNT (1), 0, 'N', 'T')
                      FROM apps.ra_customer_trx_lines rctl_t
                     WHERE     rctl_t.customer_Trx_id = rctl.customer_trx_id
                           AND rctl_t.link_to_cust_trx_line_id =
                                  rctl.customer_trx_line_id)
                  ,'N')
               ,1
               ,' ')                                     --  TAXABLE_ITEM_FLAG
             REC_LINE
         ,rcta.org_id
     FROM apps.ra_customer_trx rcta
         ,apps.ra_customer_trx_lines rctl
         ,apps.hz_cust_accounts hca
         ,ra_terms rt
         ,ra_terms_lines_discounts rtld
         ,apps.ar_payment_schedules apsa
    WHERE     1 = 1
          AND rcta.customer_trx_id = rctl.customer_trx_id
          AND NVL (rcta.interface_header_context, '&*^%$#@') NOT IN
                 ('CONVERSION', 'PRISM INVOICES')
          AND rctl.line_type = 'LINE'
          AND hca.cust_account_id = rcta.bill_to_customer_id
          AND rt.term_id = rcta.term_id(+)
          AND rt.term_id = rtld.term_id(+)
          AND rcta.org_id = rctl.org_id
          AND rctl.inventory_item_id IS NULL
          AND apsa.customer_trx_id = rcta.customer_trx_id
          AND apsa.org_id = rcta.org_id
          AND apsa.status = 'OP'
          AND apsa.amount_due_remaining != 0
          AND rcta.trx_number NOT IN
                 (SELECT dmp.INVNO
                    FROM xxwc.xxwc_artran_getpaid_dump_tbl dmp)
   UNION
   ------------------------------------------------------------------------------------------------------------------------------
   -- CONVERSION Invoices
   ------------------------------------------------------------------------------------------------------------------------------
   SELECT    RPAD (hca.account_number, 20, ' ')            --  CUSTOMER_NUMBER
          || RPAD (INVNO, 20, ' ')                      --  TRANSACTION_NUMBER
          || RPAD (NVL (ITEM, 'No Item'), 16, ' ')   --  INVENTORY_ITEM_NUMBER
          || RPAD (NVL (DESCRIP, 'No Description'), 65, ' ') --  ITEM_DESCRIPTION
          || LPAD (NVL (DISC, '0.00'), 7, ' ')   --  SALES_DISCOUNT_PERCENTAGE
          || LPAD (TO_CHAR (NVL (PRICE, 0), '9999999990.99'), 20, ' ') --  UNIT_SELLING_PRICE
          || LPAD (NVL (QTYSHP, 0), 12, ' ')             --  SHIPMENT_QUANTITY
          || LPAD (TO_CHAR (NVL (EXTPRICE, 0), '9999999990.99'), 20, ' ') --  TOTAL_EXTENDED_PRICE
          || LPAD (NVL (SEQNUM, 99999999), 10, ' ')        --  SEQUENCE_NUMBER
          || LPAD (NVL (UNIQUEID, 0), 10, ' ')                   --  UNIQUE_ID
          || RPAD (NVL (UOM, 'EA'), 10, ' ')               --  UNIT_OF_MEASURE
          || LPAD (NVL (QTYBKO, 0), 12, ' ')         --  QUANTITY_BACK_ORDERED
          || LPAD (NVL (QTYORD, 0), 12, ' ')              --  QUANTITY_ORDERED
          || RPAD (NVL (TAXABLEITEM, 'N'), 1, ' ')       --  TAXABLE_ITEM_FLAG
             REC_LINE
         ,rcta.org_id ORG_ID
     FROM xxwc.xxwc_artran_getpaid_dump_tbl dmp
         ,apps.ra_customer_trx rcta
         ,apps.hz_cust_accounts hca
         ,apps.ar_payment_schedules apsa
    WHERE     1 = 1
          AND TRIM (dmp.invno) = rcta.trx_number
          AND TRIM (dmp.custno) = hca.account_number
          AND NVL (rcta.interface_header_context, '&*^%$#@') = 'CONVERSION'
          AND hca.cust_account_id = rcta.bill_to_customer_id
          AND apsa.customer_trx_id = rcta.customer_trx_id
          AND apsa.org_id = rcta.org_id
          AND apsa.status = 'OP'
          AND apsa.amount_due_remaining != 0
   UNION
   ------------------------------------------------------------------------------------------------------------------------------
   -- CONVERSION Invoices
   -- PRISM Customer# in Attribute16
   ------------------------------------------------------------------------------------------------------------------------------
   SELECT    RPAD (hca.account_number, 20, ' ')            --  CUSTOMER_NUMBER
          || RPAD (INVNO, 20, ' ')                      --  TRANSACTION_NUMBER
          || RPAD (NVL (ITEM, 'No Item'), 16, ' ')   --  INVENTORY_ITEM_NUMBER
          || RPAD (NVL (DESCRIP, 'No Description'), 65, ' ') --  ITEM_DESCRIPTION
          || LPAD (NVL (DISC, '0.00'), 7, ' ')   --  SALES_DISCOUNT_PERCENTAGE
          || LPAD (TO_CHAR (NVL (PRICE, 0), '9999999990.99'), 20, ' ') --  UNIT_SELLING_PRICE
          || LPAD (NVL (QTYSHP, 0), 12, ' ')             --  SHIPMENT_QUANTITY
          || LPAD (TO_CHAR (NVL (EXTPRICE, 0), '9999999990.99'), 20, ' ') --  TOTAL_EXTENDED_PRICE
          || LPAD (NVL (SEQNUM, 99999999), 10, ' ')        --  SEQUENCE_NUMBER
          || LPAD (NVL (UNIQUEID, 0), 10, ' ')                   --  UNIQUE_ID
          || RPAD (NVL (UOM, 'EA'), 10, ' ')               --  UNIT_OF_MEASURE
          || LPAD (NVL (QTYBKO, 0), 12, ' ')         --  QUANTITY_BACK_ORDERED
          || LPAD (NVL (QTYORD, 0), 12, ' ')              --  QUANTITY_ORDERED
          || RPAD (NVL (TAXABLEITEM, 'N'), 1, ' ')       --  TAXABLE_ITEM_FLAG
             REC_LINE
         ,rcta.org_id ORG_ID
     FROM xxwc.xxwc_artran_getpaid_dump_tbl dmp
         ,apps.ra_customer_trx rcta
         ,apps.hz_cust_accounts hca
         ,apps.ar_payment_schedules apsa
    WHERE     1 = 1
          AND TRIM (dmp.invno) = rcta.trx_number
          AND TRIM (dmp.custno) != hca.account_number
          AND TRIM (dmp.custno) = hca.attribute6
          AND NVL (rcta.interface_header_context, '&*^%$#@') = 'CONVERSION'
          AND hca.cust_account_id = rcta.bill_to_customer_id
          AND apsa.customer_trx_id = rcta.customer_trx_id
          AND apsa.org_id = rcta.org_id
          AND apsa.status = 'OP'
          AND apsa.amount_due_remaining != 0
   UNION
   ------------------------------------------------------------------------------------------------------------------------------
   -- PRISM Invoices
   ------------------------------------------------------------------------------------------------------------------------------
   SELECT    RPAD (NVL (hca.account_number, ' '), 20, ' ') --  CUSTOMER_NUMBER
          || RPAD (NVL (rcta.trx_number, ' '), 20, ' ') --  TRANSACTION_NUMBER
          || RPAD (NVL (rctl.interface_line_attribute4, ' '), 16, ' ') --  INVENTORY_ITEM_NUMBER
          || RPAD (NVL (rctl.description, ' '), 65, ' ')  --  ITEM_DESCRIPTION
          || LPAD ('0', 7, ' ')                  --  SALES_DISCOUNT_PERCENTAGE
          || LPAD (
                NVL (TO_CHAR (rctl.unit_selling_price, '9999999990.99')
                    ,'0.00')
               ,20
               ,' ')                                    --  UNIT_SELLING_PRICE
          || LPAD (NVL (TO_CHAR (rctl.quantity_invoiced), '0'), 12, ' ') --  SHIPMENT_QUANTITY
          || LPAD (
                NVL (TO_CHAR (rctl.extended_amount, '9999999990.99'), '0.00')
               ,20
               ,' ')                                  --  TOTAL_EXTENDED_PRICE
          || LPAD (NVL (TO_CHAR (rctl.line_number), '0'), 10, ' ') --  SEQUENCE_NUMBER
          || LPAD (NVL (TO_CHAR (rctl.line_number), '0'), 10, ' ') --  UNIQUE_ID
          || RPAD (NVL (rctl.uom_code, 'EA'), 10, ' ')     --  UNIT_OF_MEASURE
          || LPAD (NVL (rctl.interface_line_attribute3, '0'), 12, ' ') --  QUANTITY_BACK_ORDERED
          || LPAD (NVL (rctl.interface_line_attribute2, '0'), 12, ' ') --  QUANTITY_ORDERED
          || RPAD (
                NVL (
                   (SELECT DECODE (COUNT (1), 0, 'N', 'T')
                      FROM apps.ra_customer_trx_lines rctl_t
                     WHERE     rctl_t.customer_Trx_id = rctl.customer_trx_id
                           AND rctl_t.link_to_cust_trx_line_id =
                                  rctl.customer_trx_line_id)
                  ,'N')
               ,1
               ,' ')                                     --  TAXABLE_ITEM_FLAG
             REC_LINE
         ,rcta.org_id
     FROM apps.ra_customer_trx rcta
         ,apps.ra_customer_trx_lines rctl
         ,apps.hz_cust_accounts hca
         ,ra_terms rt
         ,ra_terms_lines_discounts rtld
         ,apps.ar_payment_schedules apsa
    WHERE     1 = 1
          AND rcta.customer_trx_id = rctl.customer_trx_id
          AND NVL (rcta.interface_header_context, '&*^%$#@') =
                 'PRISM INVOICES'
          AND rctl.line_type = 'LINE'
          AND hca.cust_account_id = rcta.bill_to_customer_id
          AND rt.term_id = rcta.term_id(+)
          AND rt.term_id = rtld.term_id(+)
          AND rcta.org_id = rctl.org_id
          AND rctl.inventory_item_id IS NULL
          AND apsa.customer_trx_id = rcta.customer_trx_id
          AND apsa.org_id = rcta.org_id
          AND apsa.status = 'OP'
          AND apsa.amount_due_remaining != 0
          AND rcta.trx_number NOT IN
                 (SELECT dmp.invno
                    FROM xxwc.xxwc_artran_getpaid_dump_tbl dmp);


