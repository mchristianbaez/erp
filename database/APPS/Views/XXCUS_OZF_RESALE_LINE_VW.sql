
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUS_OZF_RESALE_LINE_VW" ("BU_NM", "ORACLE_BASE_TABLE_TOTAL", "FISCAL_PERIOD_ID", "PERIOD_NAME") AS 
  SELECT  /*+ PARALLEL (AUTO) */
        orl.end_cust_party_name bu_name
       ,SUM(quantity * nvl(purchase_price, selling_price)) oracle_base_table_total
       ,period_year || lpad(period_num, 2, 0) fiscal_per_id
       ,period_name
   FROM ozf.ozf_resale_lines_all orl, gl.gl_periods
  WHERE 1 = 1
    AND trunc(orl.date_ordered) BETWEEN start_date AND end_date
    AND orl.end_cust_party_name IN
        (SELECT /*+ RESULT_CACHE */
          flv.description
           FROM apps.fnd_lookup_values flv
          WHERE flv.lookup_type = 'XXCUS_REBATE_BU_XREF'
            AND flv.enabled_flag = 'Y'
            AND nvl(flv.end_date_active, SYSDATE) >= SYSDATE
            AND flv.description = orl.end_cust_party_name)
  GROUP BY orl.end_cust_party_name, period_year || lpad(period_num, 2, 0), period_name
;
