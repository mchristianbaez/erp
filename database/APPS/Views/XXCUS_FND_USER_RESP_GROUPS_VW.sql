CREATE OR REPLACE FORCE VIEW APPS.XXCUS_FND_USER_RESP_GROUPS_VW
 -- -----------------------------------------------------------------------------
  -- -----------------------------------------------------------------------------
  --   VERSION DATE          AUTHOR(S)       DESCRIPTION
  -- ------- -----------   --------------- ---------------------------------
  -- 1.0     24-Apr-2011   Kathy Poling    Copied from R11 and changed for R12.
  -- 1.1     29-Sep-2014   Vijay Srinivasan ESMS 260727 - HR interface update to remove indirect
  --                                       access from terminated employees (Project #20268)
  --                                       RFC 42489
  -- 1.2    29-May-2015    Maharajan Shunmugam ESMS#278322 HR Interface not adding Justification when terminating access
  -- -----------------------------------------------------------------------------
(
   USER_ID,
   RESPONSIBILITY_ID,
   RESPONSIBILITY_APPLICATION_ID,
   SECURITY_GROUP_ID,
   START_DATE,
   END_DATE,
   CREATED_BY,
   CREATION_DATE,
   LAST_UPDATED_BY,
   LAST_UPDATE_DATE,
   LAST_UPDATE_LOGIN
)
AS
   SELECT U.user_id user_id,
          WUR.ROLE_ORIG_SYSTEM_ID RESPONSIBILITY_ID,
          (SELECT application_id
             FROM fnd_application
            WHERE application_short_name = /* Val between 1st and 2nd separator */
                     REPLACE (SUBSTR (WAUR.ROLE_NAME,
                                        INSTR (WAUR.ROLE_NAME,
                                               '|',
                                               1,
                                               1)
                                      + 1,
                                      (  INSTR (WAUR.ROLE_NAME,
                                                '|',
                                                1,
                                                2)
                                       - INSTR (WAUR.ROLE_NAME,
                                                '|',
                                                1,
                                                1)
                                       - 1)),
                              '%col',
                              ':'))
             RESPONSIBILITY_APPLICATION_ID,
          (SELECT security_group_id
             FROM fnd_security_groups
            WHERE security_group_key =           /* Val after 3rd separator */
                     REPLACE (SUBSTR (WAUR.ROLE_NAME,
                                        INSTR (WAUR.ROLE_NAME,
                                               '|',
                                               1,
                                               3)
                                      + 1),
                              '%col',
                              ':'))
             SECURITY_GROUP_ID,
          WAUR.START_DATE,
          WAUR.END_DATE END_DATE,
          WAUR.CREATED_BY CREATED_BY,
          WAUR.CREATION_DATE CREATION_DATE,
          WAUR.LAST_UPDATED_BY LAST_UPDATED_BY,
          WAUR.LAST_UPDATE_DATE LAST_UPDATE_DATE,
          WAUR.LAST_UPDATE_LOGIN LAST_UPDATE_LOGIN
     FROM fnd_user u,
          wf_all_user_role_assignments waur,
          wf_all_user_roles wur
    WHERE     u.user_name = waur.user_name
          AND waur.role_name = wur.role_name
          AND waur.user_name = wur.user_name
         -- AND ((wur.role_orig_system in ('FND_RESP') and wur.assignment_type = 'D')    -- commented and added below for version 1.2 
         --                           or 
         --                           wur.role_orig_system in ('UMX') 
         --                           )--VIJAY
          AND (   wur.role_orig_system IN ('FND_RESP')
               OR wur.role_orig_system IN ('UMX'))  
          AND NOT waur.role_name LIKE 'FND_RESP|%|ANY';

COMMENT ON TABLE APPS.XXCUS_FND_USER_RESP_GROUPS_VW IS 'RFC42489 ESMS 260727 - HR interface update to remove indirect access from terminated employees (Project #20268).';
