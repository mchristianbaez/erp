   /* ************************************************************************
    HD Supply
    All rights reserved.
    Ticket                    Ver          Date         Author                       Description
    ----------------------    ----------   ----------   ------------------------     -------------------------
    Not available             1.0          08/21/2015   Production version     
    TMS 20151001-00046        1.5          11/18/2015   Balaguru Seshadri            Add two new fields listed below with comments "Ver 1.5"
   ************************************************************************ */ 
CREATE OR REPLACE FORCE VIEW APPS.XXCUS_PAM_PROGRAM_V
(
   ORG_ID, --Ver 1.5
   MVID,
   CUST_ID,
   MV_NAME,
   AGREEMENT_YR,
   AGREEMENT_STATUS,
   AGREEMENT_CODE, --Ver 1.5  
   PROGRAM_NAME,
   PROGRAM_DESCRIPTION,
   MEDIA_NAME,
   REBATE_TYPE,
   AGREEMENT_NAME,
   AGREEMENT_DESCRIPTION,
   PAYMENT_METHOD,
   PAYMENT_FREQ,
   AUTO_RENEWAL,
   UNTIL_YEAR,
   ACTIVITY_TYPE,
   AUTOPAY_ENABLED_FLAG,
   GLOBAL_FLAG,
   BACK_TO_OR_MIN_GUARANTEE_AMT,
   CONTRACT_NBR, --ESMS 290358
   PLAN_ID   --ESMS 290358
)
AS
     SELECT qlhv.context, --Ver 1.5
            hca.attribute2 mvid,
            hca.cust_account_id cust_id,
            hp.party_name mv_name,
            qlhv.attribute7 agreement_yr,
            oo.status_code agreement_status,
            oo.offer_code agreement_code, --Ver 1.5
            ofat.short_name program_name,
            ofat.description program_description,
            med.media_name media_name,
            oo.offer_type rebate_type,
            qlhv.description agreement_name,
            qlhv.comments agreement_description,
            qlhv.attribute5 payment_method,
            qlhv.attribute6 payment_freq,
            qlhv.attribute1 auto_renewal,
            qlhv.attribute2 until_year,
            DECODE (med.description,
                    'COOP', 'COOP',
                    'COOP_MIN', 'COOP',
                    'REBATE')
               activity_type,
            oo.autopay_flag autopay_enabled_flag,
            qlhv.global_flag global_flag,
            qlhv.attribute9 back_to_or_min_guarantee_amt,
            qlhv.attribute4 CONTRACT_NBR, --ESMS 290358
            qlhv.list_header_id PLAN_ID --ESMS 290358
       FROM apps.qp_list_headers_vl qlhv,
            ozf.ozf_offers oo,
            apps.ams_media_vl med,
            qp.qp_qualifiers qq,
            ar.hz_parties hp,
            ar.hz_cust_accounts hca,
            ozf.ozf_act_budgets oab,
            apps.ozf_funds_all_vl ofa,
            ozf.ozf_funds_all_tl ofat
      WHERE     qlhv.list_header_id = qq.list_header_id
            AND qq.qualifier_context = 'SOLD_BY'
            AND oo.qp_list_header_id = qlhv.list_header_id
            AND oo.activity_media_id = med.media_id
            AND hca.cust_account_id = TO_NUMBER (qq.qualifier_attr_value)
            AND qlhv.list_header_id = oab.act_budget_used_by_id
            AND oab.budget_source_type = 'FUND'
            AND oab.status_code = 'APPROVED'
            AND oab.arc_act_budget_used_by = 'OFFR'
            AND ofa.fund_id = oab.budget_source_id
            AND hca.party_id = hp.party_id
            AND OFA.FUND_ID = OFAT.FUND_ID
            AND oo.status_code NOT IN ('TERMINATED', 'DRAFT')
            AND qq.qualifier_attribute = 'QUALIFIER_ATTRIBUTE2'
            AND qq.list_line_id = -1
            AND qlhv.attribute7 > 2012
   GROUP BY hca.attribute2,
            qlhv.context, --Ver 1.5
            oo.offer_code, --Ver 1.5
            hca.cust_account_id,
            hp.party_name,
            qlhv.attribute7,
            ofat.short_name,
            DECODE (med.description,
                    'COOP', 'COOP',
                    'COOP_MIN', 'COOP',
                    'REBATE'),
            qlhv.description,
            qlhv.comments,
            med.media_name,
            ofat.description,
            oo.offer_type,
            qlhv.comments,
            qlhv.attribute5,
            qlhv.attribute6,
            qlhv.attribute1,
            qlhv.attribute2,
            oo.autopay_flag,
            qlhv.global_flag,
            qlhv.attribute9,
            oo.status_code,
            qlhv.list_header_id,
            --qlhv.attribute4;  --ESMS 290358 --Ver 1.5
			qlhv.attribute4  --ESMS 290358 --Ver 1.5
	        /
COMMENT ON TABLE APPS.XXCUS_PAM_PROGRAM_V IS 'TMS 20151001-00046';