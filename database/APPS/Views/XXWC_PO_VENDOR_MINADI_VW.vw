/*******************************************************************************************************************
File Name: XXWC_PO_VENDOR_MINADI_VW.sql

TYPE:     VIEW

Description: 

VERSION DATE          AUTHOR(S)       DESCRIPTION
------- -----------   --------------- -----------------------------------------------------------------------------
1.0     13-AUG-2014   Lee Spitzer     TMS#20130904-000654 Initial version
        07-Oct-2014   Pattabhi Avula  TMS#20141001-00057 WC Canada Multiorg
2.0     15-May-2015   P.Vamshidhar    Added Multiorg
                                      TMS#20150515-00055 - Implement fixes to PO Vendor minimum WebADI upload 
                                      
********************************************************************************************************************/

CREATE OR REPLACE FORCE VIEW APPS.XXWC_PO_VENDOR_MINADI_VW
(
   VENDOR_ID,
   VENDOR_NUMBER,
   ORGANIZATION_ID,
   ORGANIZATION_CODE,
   VENDOR_MIN_DOLLAR,
   FREIGHT_MIN_DOLLAR,
   FREIGHT_MIN_UOM,
   PPD_FREIGHT_UNITS,
   NOTES,
   SUP_NUMBER,
   DEFAULT_FLAG,
   VEN_ID,
   VENDOR_NAME,
   VENDOR_SITE_ID,
   VENDOR_SITE_CODE,
   VENDOR_CONTACT_ID,
   VENDOR_CONTACT
)
AS
   SELECT --xvm.vendor_id || '-' || xvm.vendor_site_id vendor_id --Removed 8/13/2014 Lee Spitzer 20140726-00008 Update Vendor min Web ADI to download/ update default contact @SC Update Vendor min Web ADI to download/ update default contact
         xvm .vendor_id
          || '-'
          || xvm.vendor_site_id
          || '-'
          || xvm.vendor_contact_id
             vendor_id -- Added 20140726-00008 8/13/2014 Lee Spitzer Update Vendor min Web ADI to download/ update default contact @SC Update Vendor min Web ADI to download/ update default contact
                      ,
             pov.segment1
          || '-'
          || pov.vendor_name
          || ' ### '
          || povs.vendor_site_code
          || ' ### '
          || pvc.last_name
             vendor_number,
          xvm.organization_id,
          ood.organization_code,
          xvm.vendor_min_dollar,
          xvm.freight_min_dollar,
          xvm.freight_min_uom -- 12/09/CG: TMS 20130904-00654: Added two new columns
                             ,
          xvm.ppd_freight_units,
          xvm.notes              -- used in download for PO Vendor Minimum ADI
                   -- created by Shankar Hariharan
                   -- 01/21/14 CG TMS 20131216-00029: Added to be able to query all branches
          ,
          pov.segment1 sup_number,
          xvm.default_flag -- Added 20140726-00008 8/13/2014 Lee Spitzer Update Vendor min Web ADI to download/ update default contact @SC Update Vendor min Web ADI to download/ update default contact
                          ,
          pov.vendor_id,
          pov.vendor_name,
          povs.vendor_site_id,
          povs.vendor_site_code,
          pvc.vendor_contact_id,
          pvc.last_name
     FROM apps.xxwc_po_vendor_minimum xvm,
          ap_suppliers pov,
          apps.ap_supplier_sites_all povs,                     -- Added all by Vamshi 1.2 V on 14-May-2015
          org_organization_definitions ood,
          po_vendor_contacts pvc -- Added 20140726-00008 8/13/2014 Lee Spitzer Update Vendor min Web ADI to download/ update default contact @SC Update Vendor min Web ADI to download/ update default contact
    WHERE     xvm.vendor_id = pov.vendor_id
          AND xvm.organization_id = ood.organization_id
          AND povs.org_id = FND_PROFILE.VALUE ('ORG_ID')      -- Added condition by Vamshi 1.2 V on 14-May-2015
          AND xvm.vendor_site_id = povs.vendor_site_id
          AND pov.vendor_id = povs.vendor_id
          AND xvm.vendor_site_id = pvc.vendor_site_id(+)
          AND xvm.vendor_contact_id = pvc.vendor_contact_id(+);