
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUSPN_MASTER_LEASES_VW" ("LEASE_ID", "PARENT_LEASE_ID", "LEASE_NUM", "LEASE_NUM2") AS 
  (
select a.lease_id, a.parent_lease_id, a.lease_num, b.lease_num lease_num2
  from pn.pn_leases_all a,
       pn.pn_leases_all b
 where a.parent_lease_id = b.lease_id
 and   a.parent_lease_id is not null)
;
