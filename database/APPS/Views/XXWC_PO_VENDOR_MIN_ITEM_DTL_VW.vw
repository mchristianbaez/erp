/* Formatted on 09-Apr-2014 16:05:12 (QP5 v5.206) */
-- Start of DDL Script for View APPS.XXWC_PO_VENDOR_MIN_ITEM_DTL_VW
-- Generated 09-Apr-2014 16:05:06 from APPS@EBIZFQA
/************************************************************************************************************************************
    $Header XXWC_PO_VENDOR_MIN_ITEM_DTL_VW.vw $
    Module Name: XXWC_PO_VENDOR_MIN_ITEM_DTL_VW
    PURPOSE: Used in Vendor minimum form

    REVISIONS:
    Ver    Date         Author                Description
    ------ -----------  ------------------    ----------------
    1.0    Unknown      Unknown               Initial Version
	2.0    28-Mar-16    Manjula Chellappan    TMS# 20160119-00180 - Column FYTD of Add items to meet vendor minimums form is not showing right values.
  											  
***********************************************************************************************************************************/

CREATE OR REPLACE VIEW apps.xxwc_po_vendor_min_item_dtl_vw
(
    sourcing_rule_name
   ,organization_code
   ,organization_id
   ,vendor_id
   ,vendor
   ,vendor_site_id
   ,vendor_site
   ,inventory_item_id
   ,segment1
   ,description
   ,lot_control_code
   ,amu
   ,unit_weight
   ,weight_uom_code
   ,fixed_lot_multiplier
   ,min_minmax_quantity
   ,max_minmax_quantity
   ,list_price
   ,calc_list_price
   ,on_hand_quantity
   ,available_tt
   ,sales_velocity
   ,open_demand
   ,open_po
   ,open_req
   ,ord_qty
   ,ext_po_cost
   ,ext_weight
   ,perc_spread
   ,avail2
   ,sorting_seq
   ,six_month_sales -- Added for ver 2.0
)
AS
( ( (SELECT msr.sourcing_rule_name
               ,mp.organization_code
               ,mp.organization_id
               ,ass.vendor_id
               ,ass.vendor_name vendor
               ,assa.vendor_site_id
               ,assa.vendor_site_code vendor_site
               ,msib.inventory_item_id
               ,msib.segment1
               ,msib.description
               ,msib.lot_control_code
               ,NVL (msib.attribute20, 0) amu
               ,NVL (msib.unit_weight, 0) unit_weight
               ,msib.weight_uom_code
               ,NVL (msib.fixed_lot_multiplier, 0) fixed_lot_multiplier
               ,NVL (ROUND (msib.min_minmax_quantity, 0), 0) min_minmax_quantity
               ,NVL (ROUND (msib.max_minmax_quantity, 0), 0) max_minmax_quantity
               ,msib.list_price_per_unit
               ,xxwc_ascp_scwb_pkg.get_po_cost (msib.inventory_item_id
                                               ,mp.organization_id
                                               ,ass.vendor_id
                                               ,assa.vendor_site_id)
                    calc_po_cost
               ,xxwc_ascp_scwb_pkg.get_on_hand (msib.inventory_item_id, mp.organization_id, 'H') on_hand_qty
               ,xxwc_ascp_scwb_pkg.get_on_hand (msib.inventory_item_id, mp.organization_id, 'T') available_tt
               ,SUBSTR (xxwc_mv_routines_pkg.get_item_category (msib.inventory_item_id
                                                               ,mp.organization_id
                                                               ,'Sales Velocity'
                                                               ,1)
                       ,1
                       ,30)
                    sales_velocity
               , (xxwc_po_vendor_min_pkg.get_demand (msib.inventory_item_id
                                                    ,mp.organization_id
                                                    ,TO_DATE (fnd_profile.VALUE ('XXWC_VM_PLAN_HOR'), 'DD-MON-YYYY')
                                                    ,1
                                                    ,                                                       --P_NET_RSV,
                                                     2
                                                    ,                                                --P_INCLUDE_NONNET,
                                                     2
                                                    ,                                                   --P_INCLUDE_WIP,
                                                     1
                                                    ,                                                     --P_NET_UNRSV,
                                                     1
                                                    ,                                                       --P_NET_WIP,
                                                     NULL))
                    open_demand                                                                              -- P_SUBINV
               , (xxwc_po_vendor_min_pkg.get_po_qty (TO_DATE (fnd_profile.VALUE ('XXWC_VM_PLAN_HOR'), 'DD-MON-YYYY')
                                                    ,mp.organization_id
                                                    ,msib.inventory_item_id
                                                    ,1
                                                    ,                                                   -- p_include_po,
                                                     2
                                                    ,                                               -- p_include_nonnet,
                                                     2
                                                    ,                                                  -- p_include_wip,
                                                     2
                                                    ,                                                    --p_include_if,
                                                     NULL))
                    open_po                                                                                  -- p_subinv
               , (xxwc_po_vendor_min_pkg.get_req_qty (TO_DATE (fnd_profile.VALUE ('XXWC_VM_PLAN_HOR'), 'DD-MON-YYYY')
                                                     ,mp.organization_id
                                                     ,msib.inventory_item_id
                                                     ,1
                                                     ,                                                  -- p_include_po,
                                                      2
                                                     ,                                              -- p_include_nonnet,
                                                      2
                                                     ,                                                 -- p_include_wip,
                                                      2
                                                     ,                                                 --- p_include_if,
                                                      NULL))
                    open_req                                                                                 -- p_subinv
               , (xxwc_po_vendor_min_pkg.get_ord_qty (
                      NVL (ROUND (msib.max_minmax_quantity, 0), 0)
                     ,xxwc_ascp_scwb_pkg.get_on_hand (msib.inventory_item_id, mp.organization_id, 'T')
                     , (xxwc_po_vendor_min_pkg.get_po_qty (
                            TO_DATE (fnd_profile.VALUE ('XXWC_VM_PLAN_HOR'), 'DD-MON-YYYY')
                           ,mp.organization_id
                           ,msib.inventory_item_id
                           ,1
                           ,                                                                            -- p_include_po,
                            2
                           ,                                                                        -- p_include_nonnet,
                            2
                           ,                                                                           -- p_include_wip,
                            2
                           ,                                                                             --p_include_if,
                            NULL))
                     ,xxwc_po_vendor_min_pkg.get_req_qty (
                          TO_DATE (fnd_profile.VALUE ('XXWC_VM_PLAN_HOR'), 'DD-MON-YYYY')
                         ,mp.organization_id
                         ,msib.inventory_item_id
                         ,1
                         ,2
                         ,2
                         ,2
                         ,NULL)
                     ,xxwc_po_vendor_min_pkg.get_demand (
                          msib.inventory_item_id
                         ,mp.organization_id
                         ,TO_DATE (fnd_profile.VALUE ('XXWC_VM_PLAN_HOR'), 'DD-MON-YYYY')
                         ,1
                         ,2
                         ,2
                         ,1
                         ,1
                         ,NULL)
                     ,NVL (msib.fixed_lot_multiplier, 0)))
                    ord_qty
               -- 04/25/2013 TMS 20130424-01825: CG Changed to calculate with BPA Cost first the org list price (SCWB contains that logic)
               -- , (msib.list_price_per_unit
               , (  xxwc_ascp_scwb_pkg.get_po_cost (msib.inventory_item_id
                                                   ,mp.organization_id
                                                   ,ass.vendor_id
                                                   ,assa.vendor_site_id)
                  * xxwc_po_vendor_min_pkg.get_ord_qty (
                        NVL (ROUND (msib.max_minmax_quantity, 0), 0)
                       ,xxwc_ascp_scwb_pkg.get_on_hand (msib.inventory_item_id, mp.organization_id, 'T')
                       ,xxwc_po_vendor_min_pkg.get_po_qty (
                            TO_DATE (fnd_profile.VALUE ('XXWC_VM_PLAN_HOR'), 'DD-MON-YYYY')
                           ,mp.organization_id
                           ,msib.inventory_item_id
                           ,1
                           ,2
                           ,2
                           ,2
                           ,NULL)
                       ,xxwc_po_vendor_min_pkg.get_req_qty (
                            TO_DATE (fnd_profile.VALUE ('XXWC_VM_PLAN_HOR'), 'DD-MON-YYYY')
                           ,mp.organization_id
                           ,msib.inventory_item_id
                           ,1
                           ,2
                           ,2
                           ,2
                           ,NULL)
                       ,xxwc_po_vendor_min_pkg.get_demand (
                            msib.inventory_item_id
                           ,mp.organization_id
                           ,TO_DATE (fnd_profile.VALUE ('XXWC_VM_PLAN_HOR'), 'DD-MON-YYYY')
                           ,1
                           ,2
                           ,2
                           ,1
                           ,1
                           ,NULL)
                       ,NVL (msib.fixed_lot_multiplier, 0)))
                    ext_po_cost
               , (  NVL (msib.unit_weight, 0)
                  * xxwc_po_vendor_min_pkg.get_ord_qty (
                        NVL (ROUND (msib.max_minmax_quantity, 0), 0)
                       ,xxwc_ascp_scwb_pkg.get_on_hand (msib.inventory_item_id, mp.organization_id, 'T')
                       ,xxwc_po_vendor_min_pkg.get_po_qty (
                            TO_DATE (fnd_profile.VALUE ('XXWC_VM_PLAN_HOR'), 'DD-MON-YYYY')
                           ,mp.organization_id
                           ,msib.inventory_item_id
                           ,1
                           ,2
                           ,2
                           ,2
                           ,NULL)
                       ,xxwc_po_vendor_min_pkg.get_req_qty (
                            TO_DATE (fnd_profile.VALUE ('XXWC_VM_PLAN_HOR'), 'DD-MON-YYYY')
                           ,mp.organization_id
                           ,msib.inventory_item_id
                           ,1
                           ,2
                           ,2
                           ,2
                           ,NULL)
                       ,xxwc_po_vendor_min_pkg.get_demand (
                            msib.inventory_item_id
                           ,mp.organization_id
                           ,TO_DATE (fnd_profile.VALUE ('XXWC_VM_PLAN_HOR'), 'DD-MON-YYYY')
                           ,1
                           ,2
                           ,2
                           ,1
                           ,1
                           ,NULL)
                       ,NVL (msib.fixed_lot_multiplier, 0)))
                    ext_weight
               , (xxwc_po_vendor_min_pkg.get_perc_spread (
                      NVL (ROUND (msib.min_minmax_quantity, 0), 0)
                     ,                                                                                          -- p_min
                      NVL (ROUND (msib.max_minmax_quantity, 0), 0)
                     ,                                                                                          -- p_max
                      xxwc_ascp_scwb_pkg.get_on_hand (msib.inventory_item_id, mp.organization_id, 'T')
                     ,xxwc_po_vendor_min_pkg.get_po_qty (
                          TO_DATE (fnd_profile.VALUE ('XXWC_VM_PLAN_HOR'), 'DD-MON-YYYY')
                         ,mp.organization_id
                         ,msib.inventory_item_id
                         ,1
                         ,2
                         ,2
                         ,2
                         ,NULL)
                     ,xxwc_po_vendor_min_pkg.get_req_qty (
                          TO_DATE (fnd_profile.VALUE ('XXWC_VM_PLAN_HOR'), 'DD-MON-YYYY')
                         ,mp.organization_id
                         ,msib.inventory_item_id
                         ,1
                         ,2
                         ,2
                         ,2
                         ,NULL)
                     ,xxwc_po_vendor_min_pkg.get_demand (
                          msib.inventory_item_id
                         ,mp.organization_id
                         ,TO_DATE (fnd_profile.VALUE ('XXWC_VM_PLAN_HOR'), 'DD-MON-YYYY')
                         ,1
                         ,2
                         ,2
                         ,1
                         ,1
                         ,NULL)))
                    perc_spread
               , (  NVL (xxwc_ascp_scwb_pkg.get_on_hand (msib.inventory_item_id, mp.organization_id, 'T'), 0)
                  - NVL (xxwc_po_vendor_min_pkg.get_demand (
                             msib.inventory_item_id
                            ,mp.organization_id
                            ,TO_DATE (fnd_profile.VALUE ('XXWC_VM_PLAN_HOR'), 'DD-MON-YYYY')
                            ,1
                            ,2
                            ,2
                            ,1
                            ,1
                            ,NULL)
                        ,0))
                    avail2
               , (CASE
                      WHEN apps.xxwc_po_vendor_min_pkg.get_status (
                               NVL (ROUND (msib.min_minmax_quantity, 0), 0)
                              ,                                                                                 -- p_min
                               NVL (ROUND (msib.max_minmax_quantity, 0), 0)
                              ,                                                                                -- P_MAX,
                               xxwc_ascp_scwb_pkg.get_on_hand (msib.inventory_item_id, mp.organization_id, 'T')
                              ,                                                                          -- P_AVAILABLE,
                               xxwc_po_vendor_min_pkg.get_po_qty (
                                   TO_DATE (fnd_profile.VALUE ('XXWC_VM_PLAN_HOR'), 'DD-MON-YYYY')
                                  ,mp.organization_id
                                  ,msib.inventory_item_id
                                  ,1
                                  ,2
                                  ,2
                                  ,2
                                  ,NULL)
                              ,                                                                            -- P_OPEN_PO,
                               xxwc_po_vendor_min_pkg.get_req_qty (
                                   TO_DATE (fnd_profile.VALUE ('XXWC_VM_PLAN_HOR'), 'DD-MON-YYYY')
                                  ,mp.organization_id
                                  ,msib.inventory_item_id
                                  ,1
                                  ,2
                                  ,2
                                  ,2
                                  ,NULL)
                              ,                                                                           -- P_OPEN_REQ,
                               xxwc_po_vendor_min_pkg.get_demand (
                                   msib.inventory_item_id
                                  ,mp.organization_id
                                  ,TO_DATE (fnd_profile.VALUE ('XXWC_VM_PLAN_HOR'), 'DD-MON-YYYY')
                                  ,1
                                  ,2
                                  ,2
                                  ,1
                                  ,1
                                  ,NULL)) = 'Below Min'
                      THEN
                          1
                      WHEN apps.xxwc_po_vendor_min_pkg.get_status (
                               NVL (ROUND (msib.min_minmax_quantity, 0), 0)
                              ,                                                                                 -- p_min
                               NVL (ROUND (msib.max_minmax_quantity, 0), 0)
                              ,                                                                                -- P_MAX,
                               xxwc_ascp_scwb_pkg.get_on_hand (msib.inventory_item_id, mp.organization_id, 'T')
                              ,                                                                          -- P_AVAILABLE,
                               xxwc_po_vendor_min_pkg.get_po_qty (
                                   TO_DATE (fnd_profile.VALUE ('XXWC_VM_PLAN_HOR'), 'DD-MON-YYYY')
                                  ,mp.organization_id
                                  ,msib.inventory_item_id
                                  ,1
                                  ,2
                                  ,2
                                  ,2
                                  ,NULL)
                              ,                                                                            -- P_OPEN_PO,
                               xxwc_po_vendor_min_pkg.get_req_qty (
                                   TO_DATE (fnd_profile.VALUE ('XXWC_VM_PLAN_HOR'), 'DD-MON-YYYY')
                                  ,mp.organization_id
                                  ,msib.inventory_item_id
                                  ,1
                                  ,2
                                  ,2
                                  ,2
                                  ,NULL)
                              ,                                                                           -- P_OPEN_REQ,
                               xxwc_po_vendor_min_pkg.get_demand (
                                   msib.inventory_item_id
                                  ,mp.organization_id
                                  ,TO_DATE (fnd_profile.VALUE ('XXWC_VM_PLAN_HOR'), 'DD-MON-YYYY')
                                  ,1
                                  ,2
                                  ,2
                                  ,1
                                  ,1
                                  ,NULL)) = 'At Min'
                      THEN
                          2
                      WHEN apps.xxwc_po_vendor_min_pkg.get_status (
                               NVL (ROUND (msib.min_minmax_quantity, 0), 0)
                              ,                                                                                 -- p_min
                               NVL (ROUND (msib.max_minmax_quantity, 0), 0)
                              ,                                                                                -- P_MAX,
                               xxwc_ascp_scwb_pkg.get_on_hand (msib.inventory_item_id, mp.organization_id, 'T')
                              ,                                                                          -- P_AVAILABLE,
                               xxwc_po_vendor_min_pkg.get_po_qty (
                                   TO_DATE (fnd_profile.VALUE ('XXWC_VM_PLAN_HOR'), 'DD-MON-YYYY')
                                  ,mp.organization_id
                                  ,msib.inventory_item_id
                                  ,1
                                  ,2
                                  ,2
                                  ,2
                                  ,NULL)
                              ,                                                                            -- P_OPEN_PO,
                               xxwc_po_vendor_min_pkg.get_req_qty (
                                   TO_DATE (fnd_profile.VALUE ('XXWC_VM_PLAN_HOR'), 'DD-MON-YYYY')
                                  ,mp.organization_id
                                  ,msib.inventory_item_id
                                  ,1
                                  ,2
                                  ,2
                                  ,2
                                  ,NULL)
                              ,                                                                           -- P_OPEN_REQ,
                               xxwc_po_vendor_min_pkg.get_demand (
                                   msib.inventory_item_id
                                  ,mp.organization_id
                                  ,TO_DATE (fnd_profile.VALUE ('XXWC_VM_PLAN_HOR'), 'DD-MON-YYYY')
                                  ,1
                                  ,2
                                  ,2
                                  ,1
                                  ,1
                                  ,NULL)) = 'Below Max'
                      THEN
                          3
                      WHEN apps.xxwc_po_vendor_min_pkg.get_status (
                               NVL (ROUND (msib.min_minmax_quantity, 0), 0)
                              ,                                                                                 -- p_min
                               NVL (ROUND (msib.max_minmax_quantity, 0), 0)
                              ,                                                                                -- P_MAX,
                               xxwc_ascp_scwb_pkg.get_on_hand (msib.inventory_item_id, mp.organization_id, 'T')
                              ,                                                                          -- P_AVAILABLE,
                               xxwc_po_vendor_min_pkg.get_po_qty (
                                   TO_DATE (fnd_profile.VALUE ('XXWC_VM_PLAN_HOR'), 'DD-MON-YYYY')
                                  ,mp.organization_id
                                  ,msib.inventory_item_id
                                  ,1
                                  ,2
                                  ,2
                                  ,2
                                  ,NULL)
                              ,                                                                            -- P_OPEN_PO,
                               xxwc_po_vendor_min_pkg.get_req_qty (
                                   TO_DATE (fnd_profile.VALUE ('XXWC_VM_PLAN_HOR'), 'DD-MON-YYYY')
                                  ,mp.organization_id
                                  ,msib.inventory_item_id
                                  ,1
                                  ,2
                                  ,2
                                  ,2
                                  ,NULL)
                              ,                                                                           -- P_OPEN_REQ,
                               xxwc_po_vendor_min_pkg.get_demand (
                                   msib.inventory_item_id
                                  ,mp.organization_id
                                  ,TO_DATE (fnd_profile.VALUE ('XXWC_VM_PLAN_HOR'), 'DD-MON-YYYY')
                                  ,1
                                  ,2
                                  ,2
                                  ,1
                                  ,1
                                  ,NULL)) = 'At Max'
                      THEN
                          4
                      ELSE
                          5
                  END)
                    sorting_seq
--Added below column for Rev 2.0 					
				, NVL(( SELECT six_store_sale
                      FROM xxwc.xxwc_isr_details_all 
					 WHERE inventory_item_id = msib.inventory_item_id
                       AND organization_id = msib.organization_id),0) six_month_sales						
           FROM mrp_sourcing_rules msr
               ,mtl_parameters mp
               ,mrp_sr_receipt_org msro
               ,mrp_sr_source_org msso
               ,ap_suppliers ass
               ,ap_supplier_sites_all assa
               ,mrp_sr_assignments msa
               ,mtl_system_items_b msib
               ,mrp_assignment_sets mas
          WHERE     mp.organization_id = NVL (msr.organization_id, mp.organization_id)
                AND msr.sourcing_rule_id = msro.sourcing_rule_id
                AND msro.sr_receipt_id = msso.sr_receipt_id
                AND msso.source_type = 3                                                                      --buy from
                AND msso.vendor_id = ass.vendor_id
                AND msso.vendor_site_id = assa.vendor_site_id
                AND ass.vendor_id = assa.vendor_id
                AND SYSDATE BETWEEN msro.effective_date AND NVL (msro.disable_date, SYSDATE)
                AND msr.sourcing_rule_id = msa.sourcing_rule_id
                AND NVL (msr.organization_id, mp.organization_id) = msa.organization_id
                AND msib.inventory_item_id = msa.inventory_item_id
                AND msib.organization_id = msa.organization_id
                -- 01/22/13 CG: TMS 20130121-00856. Remove inventory sourced items from the Vendor Minimum Form
                AND NVL (msib.source_type, -1) != 1
                AND msa.assignment_set_id = mas.assignment_set_id
                AND msso.source_type = 3                                                                      --Buy From
                AND mas.assignment_set_id = fnd_profile.VALUE ('MRP_DEFAULT_ASSIGNMENT_SET')
                AND (NVL (msib.min_minmax_quantity, 0) <> 0 OR NVL (msib.max_minmax_quantity, 0) <> 0)
                AND NVL (xxwc_mv_routines_pkg.get_item_category (msib.inventory_item_id
                                                                ,mp.organization_id
                                                                ,'Sales Velocity'
                                                                ,1)
                        ,'ZZ') != 'N')))

/

-- End of DDL Script for View APPS.XXWC_PO_VENDOR_MIN_ITEM_DTL_VW
