CREATE OR REPLACE VIEW APPS.XXWC_BPA_PRICE_ZONE_VW AS
SELECT PO_NUMBER,
            VENDOR_NAME,
            VENDOR_NUM,
            VENDOR_SITE_CODE,
            ITEM_NUMBER,
            ITEM_DESCRIPTION,
            VENDOR_ITEM_NUMBER,
            EXPIRATION_DATE,
            NATIONAL_PRICE,
            NATIONAL_QUANTITY,
            NATIONAL_BREAK,
            PROMO_PRICE,
            PROMO_START_DATE,
            PROMO_END_DATE,
            PRICE_ZONE_PRICE1,
            PRICE_ZONE_QUANTITY1,
            PRICE_ZONE_QUANTITY_PRICE1,
            PRICE_ZONE_PRICE2,
            PRICE_ZONE_QUANTITY2,
            PRICE_ZONE_QUANTITY_PRICE2,
            PRICE_ZONE_PRICE3,
            PRICE_ZONE_QUANTITY3,
            PRICE_ZONE_QUANTITY_PRICE3,
            PRICE_ZONE_PRICE4,
            PRICE_ZONE_QUANTITY4,
            PRICE_ZONE_QUANTITY_PRICE4,
            PRICE_ZONE_PRICE5,
            PRICE_ZONE_QUANTITY5,
            PRICE_ZONE_QUANTITY_PRICE5,
            PRICE_ZONE_PRICE6,
            PRICE_ZONE_QUANTITY6,
            PRICE_ZONE_QUANTITY_PRICE6,
            PRICE_ZONE_PRICE7,
            PRICE_ZONE_QUANTITY7,
            PRICE_ZONE_QUANTITY_PRICE7,
            PRICE_ZONE_PRICE8,
            PRICE_ZONE_QUANTITY8,
            PRICE_ZONE_QUANTITY_PRICE8,
            PRICE_ZONE_PRICE9,
            PRICE_ZONE_QUANTITY9,
            PRICE_ZONE_QUANTITY_PRICE9,
            PRICE_ZONE_PRICE10,
            PRICE_ZONE_QUANTITY10,
            PRICE_ZONE_QUANTITY_PRICE10,
            PRICE_ZONE_PRICE11,
            PRICE_ZONE_QUANTITY11,
            PRICE_ZONE_QUANTITY_PRICE11,
            PRICE_ZONE_PRICE12,
            PRICE_ZONE_QUANTITY12,
            PRICE_ZONE_QUANTITY_PRICE12,
            PRICE_ZONE_PRICE13,
            PRICE_ZONE_QUANTITY13,
            PRICE_ZONE_QUANTITY_PRICE13,
            PRICE_ZONE_PRICE14,
            PRICE_ZONE_QUANTITY14,
            PRICE_ZONE_QUANTITY_PRICE14,
            PRICE_ZONE_PRICE15,
            PRICE_ZONE_QUANTITY15,
            PRICE_ZONE_QUANTITY_PRICE15,
            PRICE_ZONE_PRICE16,
            PRICE_ZONE_QUANTITY16,
            PRICE_ZONE_QUANTITY_PRICE16,
            PRICE_ZONE_PRICE17,
            PRICE_ZONE_QUANTITY17,
            PRICE_ZONE_QUANTITY_PRICE17,
            PRICE_ZONE_PRICE18,
            PRICE_ZONE_QUANTITY18,
            PRICE_ZONE_QUANTITY_PRICE18,
            PRICE_ZONE_PRICE19,
            PRICE_ZONE_QUANTITY19,
            PRICE_ZONE_QUANTITY_PRICE19,
            PRICE_ZONE_PRICE20,
            PRICE_ZONE_QUANTITY20,
            PRICE_ZONE_QUANTITY_PRICE20,
            PRICE_ZONE_PRICE21,
            PRICE_ZONE_QUANTITY21,
            PRICE_ZONE_QUANTITY_PRICE21,
            PRICE_ZONE_PRICE22,
            PRICE_ZONE_QUANTITY22,
            PRICE_ZONE_QUANTITY_PRICE22,
            PRICE_ZONE_PRICE23,
            PRICE_ZONE_QUANTITY23,
            PRICE_ZONE_QUANTITY_PRICE23,
            PRICE_ZONE_PRICE24,
            PRICE_ZONE_QUANTITY24,
            PRICE_ZONE_QUANTITY_PRICE24,
            PRICE_ZONE_PRICE25,
            PRICE_ZONE_QUANTITY25,
            PRICE_ZONE_QUANTITY_PRICE25,
            PRICE_ZONE_PRICE26,
            PRICE_ZONE_QUANTITY26,
            PRICE_ZONE_QUANTITY_PRICE26,
            PRICE_ZONE_PRICE27,
            PRICE_ZONE_QUANTITY27,
            PRICE_ZONE_QUANTITY_PRICE27,
            PRICE_ZONE_PRICE28,
            PRICE_ZONE_QUANTITY28,
            PRICE_ZONE_QUANTITY_PRICE28,
            PRICE_ZONE_PRICE29,
            PRICE_ZONE_QUANTITY29,
            PRICE_ZONE_QUANTITY_PRICE29,
            PRICE_ZONE_PRICE30,
            PRICE_ZONE_QUANTITY30,
            PRICE_ZONE_QUANTITY_PRICE30,
            ORG_PRICE_ZONE_1,
            ORG_PRICE_ZONE_2,
            ORG_PRICE_ZONE_3,
            ORG_PRICE_ZONE_4,
            ORG_PRICE_ZONE_5,
            ORG_PRICE_ZONE_6,
            ORG_PRICE_ZONE_7,
            ORG_PRICE_ZONE_8,
            ORG_PRICE_ZONE_9,
            ORG_PRICE_ZONE_10,
            ORG_PRICE_ZONE_11,
            ORG_PRICE_ZONE_12,
            ORG_PRICE_ZONE_13,
            ORG_PRICE_ZONE_14,
            ORG_PRICE_ZONE_15,
            ORG_PRICE_ZONE_16,
            ORG_PRICE_ZONE_17,
            ORG_PRICE_ZONE_18,
            ORG_PRICE_ZONE_19,
            ORG_PRICE_ZONE_20,
            ORG_PRICE_ZONE_21,
            ORG_PRICE_ZONE_22,
            ORG_PRICE_ZONE_23,
            ORG_PRICE_ZONE_24,
            ORG_PRICE_ZONE_25,
            ORG_PRICE_ZONE_26,
            ORG_PRICE_ZONE_27,
            ORG_PRICE_ZONE_28,
            ORG_PRICE_ZONE_29,
            ORG_PRICE_ZONE_30,
            PO_HEADER_ID,
            INVENTORY_ITEM_ID,
            VENDOR_ID,
            VENDOR_SITE_ID,
            VENDOR_CONTACT_ID,
            IMPLEMENT_DATE,
            PRICE_ZONE_QUANTITY,
            ORG_ID --Added 1/22/2015 by Lee Spitzer TMS Ticket 20140909-00032
       FROM (SELECT DISTINCT
                    pha.segment1 po_number,
                    ass.vendor_name,
                    ass.segment1 vendor_num,
                    assa.vendor_site_code,
                    msib.segment1 item_number,
                    msib.description item_description,
                    (SELECT cross_reference
                       FROM mtl_cross_references_b
                      WHERE     cross_reference_type = 'VENDOR'
                            AND inventory_item_id = msib.inventory_item_id
                            AND msib.organization_id =
                                   FND_PROFILE.VALUE ('XXWC_ITEM_MASTER_ORG')
                            AND ROWNUM = 1)
                       vendor_item_number             --,msib.primary_uom_code
                                         --,NULL UPC_CODE
                                         --,NULL MFG_PART_NUMBER
                    ,
                    pla.expiration_date,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       0,
                       'PRICE')
                       NATIONAL_PRICE,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_break_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       0,
                       xbpzqbv.price_zone_quantity,
                       'QUANTITY')
                       NATIONAL_QUANTITY,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_break_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       0,
                       xbpzqbv.price_zone_quantity,
                       'QUANTITY_PRICE')
                       NATIONAL_BREAK,
                    xppt.promo_price,
                    xppt.promo_start_date,
                    xppt.promo_end_date,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       1,
                       'PRICE')
                       PRICE_ZONE_PRICE1,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_break_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       1,
                       xbpzqbv.price_zone_quantity,
                       'QUANTITY')
                       PRICE_ZONE_QUANTITY1,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_break_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       1,
                       xbpzqbv.price_zone_quantity,
                       'QUANTITY_PRICE')
                       PRICE_ZONE_QUANTITY_PRICE1,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       2,
                       'PRICE')
                       PRICE_ZONE_PRICE2,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_break_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       2,
                       xbpzqbv.price_zone_quantity,
                       'QUANTITY')
                       PRICE_ZONE_QUANTITY2,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_break_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       2,
                       xbpzqbv.price_zone_quantity,
                       'QUANTITY_PRICE')
                       PRICE_ZONE_QUANTITY_PRICE2,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       3,
                       'PRICE')
                       PRICE_ZONE_PRICE3,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_break_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       3,
                       xbpzqbv.price_zone_quantity,
                       'QUANTITY')
                       PRICE_ZONE_QUANTITY3,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_break_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       3,
                       xbpzqbv.price_zone_quantity,
                       'QUANTITY_PRICE')
                       PRICE_ZONE_QUANTITY_PRICE3,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       4,
                       'PRICE')
                       PRICE_ZONE_PRICE4,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_break_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       4,
                       xbpzqbv.price_zone_quantity,
                       'QUANTITY')
                       PRICE_ZONE_QUANTITY4,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_break_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       4,
                       xbpzqbv.price_zone_quantity,
                       'QUANTITY_PRICE')
                       PRICE_ZONE_QUANTITY_PRICE4,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       5,
                       'PRICE')
                       PRICE_ZONE_PRICE5,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_break_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       5,
                       xbpzqbv.price_zone_quantity,
                       'QUANTITY')
                       PRICE_ZONE_QUANTITY5,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_break_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       5,
                       xbpzqbv.price_zone_quantity,
                       'QUANTITY_PRICE')
                       PRICE_ZONE_QUANTITY_PRICE5,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       6,
                       'PRICE')
                       PRICE_ZONE_PRICE6,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_break_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       6,
                       xbpzqbv.price_zone_quantity,
                       'QUANTITY')
                       PRICE_ZONE_QUANTITY6,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_break_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       6,
                       xbpzqbv.price_zone_quantity,
                       'QUANTITY_PRICE')
                       PRICE_ZONE_QUANTITY_PRICE6,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       7,
                       'PRICE')
                       PRICE_ZONE_PRICE7,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_break_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       7,
                       xbpzqbv.price_zone_quantity,
                       'QUANTITY')
                       PRICE_ZONE_QUANTITY7,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_break_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       7,
                       xbpzqbv.price_zone_quantity,
                       'QUANTITY_PRICE')
                       PRICE_ZONE_QUANTITY_PRICE7,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       8,
                       'PRICE')
                       PRICE_ZONE_PRICE8,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_break_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       8,
                       xbpzqbv.price_zone_quantity,
                       'QUANTITY')
                       PRICE_ZONE_QUANTITY8,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_break_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       8,
                       xbpzqbv.price_zone_quantity,
                       'QUANTITY_PRICE')
                       PRICE_ZONE_QUANTITY_PRICE8,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       9,
                       'PRICE')
                       PRICE_ZONE_PRICE9,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_break_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       9,
                       xbpzqbv.price_zone_quantity,
                       'QUANTITY')
                       PRICE_ZONE_QUANTITY9,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_break_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       9,
                       xbpzqbv.price_zone_quantity,
                       'QUANTITY_PRICE')
                       PRICE_ZONE_QUANTITY_PRICE9,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       10,
                       'PRICE')
                       PRICE_ZONE_PRICE10,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_break_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       10,
                       xbpzqbv.price_zone_quantity,
                       'QUANTITY')
                       PRICE_ZONE_QUANTITY10,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_break_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       10,
                       xbpzqbv.price_zone_quantity,
                       'QUANTITY_PRICE')
                       PRICE_ZONE_QUANTITY_PRICE10,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       11,
                       'PRICE')
                       PRICE_ZONE_PRICE11,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_break_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       11,
                       xbpzqbv.price_zone_quantity,
                       'QUANTITY')
                       PRICE_ZONE_QUANTITY11,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_break_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       11,
                       xbpzqbv.price_zone_quantity,
                       'QUANTITY_PRICE')
                       PRICE_ZONE_QUANTITY_PRICE11,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       12,
                       'PRICE')
                       PRICE_ZONE_PRICE12,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_break_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       12,
                       xbpzqbv.price_zone_quantity,
                       'QUANTITY')
                       PRICE_ZONE_QUANTITY12,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_break_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       12,
                       xbpzqbv.price_zone_quantity,
                       'QUANTITY_PRICE')
                       PRICE_ZONE_QUANTITY_PRICE12,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       13,
                       'PRICE')
                       PRICE_ZONE_PRICE13,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_break_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       13,
                       xbpzqbv.price_zone_quantity,
                       'QUANTITY')
                       PRICE_ZONE_QUANTITY13,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_break_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       13,
                       xbpzqbv.price_zone_quantity,
                       'QUANTITY_PRICE')
                       PRICE_ZONE_QUANTITY_PRICE13,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       14,
                       'PRICE')
                       PRICE_ZONE_PRICE14,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_break_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       14,
                       xbpzqbv.price_zone_quantity,
                       'QUANTITY')
                       PRICE_ZONE_QUANTITY14,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_break_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       14,
                       xbpzqbv.price_zone_quantity,
                       'QUANTITY_PRICE')
                       PRICE_ZONE_QUANTITY_PRICE14,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       15,
                       'PRICE')
                       PRICE_ZONE_PRICE15,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_break_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       15,
                       xbpzqbv.price_zone_quantity,
                       'QUANTITY')
                       PRICE_ZONE_QUANTITY15,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_break_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       15,
                       xbpzqbv.price_zone_quantity,
                       'QUANTITY_PRICE')
                       PRICE_ZONE_QUANTITY_PRICE15,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       16,
                       'PRICE')
                       PRICE_ZONE_PRICE16,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_break_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       16,
                       xbpzqbv.price_zone_quantity,
                       'QUANTITY')
                       PRICE_ZONE_QUANTITY16,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_break_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       16,
                       xbpzqbv.price_zone_quantity,
                       'QUANTITY_PRICE')
                       PRICE_ZONE_QUANTITY_PRICE16,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       17,
                       'PRICE')
                       PRICE_ZONE_PRICE17,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_break_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       17,
                       xbpzqbv.price_zone_quantity,
                       'QUANTITY')
                       PRICE_ZONE_QUANTITY17,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_break_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       17,
                       xbpzqbv.price_zone_quantity,
                       'QUANTITY_PRICE')
                       PRICE_ZONE_QUANTITY_PRICE17,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       18,
                       'PRICE')
                       PRICE_ZONE_PRICE18,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_break_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       18,
                       xbpzqbv.price_zone_quantity,
                       'QUANTITY')
                       PRICE_ZONE_QUANTITY18,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_break_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       18,
                       xbpzqbv.price_zone_quantity,
                       'QUANTITY_PRICE')
                       PRICE_ZONE_QUANTITY_PRICE18,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       19,
                       'PRICE')
                       PRICE_ZONE_PRICE19,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_break_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       19,
                       xbpzqbv.price_zone_quantity,
                       'QUANTITY')
                       PRICE_ZONE_QUANTITY19,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_break_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       19,
                       xbpzqbv.price_zone_quantity,
                       'QUANTITY_PRICE')
                       PRICE_ZONE_QUANTITY_PRICE19,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       20,
                       'PRICE')
                       PRICE_ZONE_PRICE20,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_break_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       20,
                       xbpzqbv.price_zone_quantity,
                       'QUANTITY')
                       PRICE_ZONE_QUANTITY20,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_break_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       20,
                       xbpzqbv.price_zone_quantity,
                       'QUANTITY_PRICE')
                       PRICE_ZONE_QUANTITY_PRICE20,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       21,
                       'PRICE')
                       PRICE_ZONE_PRICE21,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_break_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       21,
                       xbpzqbv.price_zone_quantity,
                       'QUANTITY')
                       PRICE_ZONE_QUANTITY21,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_break_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       21,
                       xbpzqbv.price_zone_quantity,
                       'QUANTITY_PRICE')
                       PRICE_ZONE_QUANTITY_PRICE21,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       22,
                       'PRICE')
                       PRICE_ZONE_PRICE22,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_break_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       22,
                       xbpzqbv.price_zone_quantity,
                       'QUANTITY')
                       PRICE_ZONE_QUANTITY22,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_break_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       22,
                       xbpzqbv.price_zone_quantity,
                       'QUANTITY_PRICE')
                       PRICE_ZONE_QUANTITY_PRICE22,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       23,
                       'PRICE')
                       PRICE_ZONE_PRICE23,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_break_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       23,
                       xbpzqbv.price_zone_quantity,
                       'QUANTITY')
                       PRICE_ZONE_QUANTITY23,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_break_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       23,
                       xbpzqbv.price_zone_quantity,
                       'QUANTITY_PRICE')
                       PRICE_ZONE_QUANTITY_PRICE23,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       24,
                       'PRICE')
                       PRICE_ZONE_PRICE24,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_break_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       24,
                       xbpzqbv.price_zone_quantity,
                       'QUANTITY')
                       PRICE_ZONE_QUANTITY24,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_break_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       24,
                       xbpzqbv.price_zone_quantity,
                       'QUANTITY_PRICE')
                       PRICE_ZONE_QUANTITY_PRICE24,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       25,
                       'PRICE')
                       PRICE_ZONE_PRICE25,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_break_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       25,
                       xbpzqbv.price_zone_quantity,
                       'QUANTITY')
                       PRICE_ZONE_QUANTITY25,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_break_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       25,
                       xbpzqbv.price_zone_quantity,
                       'QUANTITY_PRICE')
                       PRICE_ZONE_QUANTITY_PRICE25,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       26,
                       'PRICE')
                       PRICE_ZONE_PRICE26,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_break_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       26,
                       xbpzqbv.price_zone_quantity,
                       'QUANTITY')
                       PRICE_ZONE_QUANTITY26,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_break_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       26,
                       xbpzqbv.price_zone_quantity,
                       'QUANTITY_PRICE')
                       PRICE_ZONE_QUANTITY_PRICE26,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       27,
                       'PRICE')
                       PRICE_ZONE_PRICE27,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_break_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       27,
                       xbpzqbv.price_zone_quantity,
                       'QUANTITY')
                       PRICE_ZONE_QUANTITY27,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_break_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       27,
                       xbpzqbv.price_zone_quantity,
                       'QUANTITY_PRICE')
                       PRICE_ZONE_QUANTITY_PRICE27,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       28,
                       'PRICE')
                       PRICE_ZONE_PRICE28,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_break_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       28,
                       xbpzqbv.price_zone_quantity,
                       'QUANTITY')
                       PRICE_ZONE_QUANTITY28,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_break_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       28,
                       xbpzqbv.price_zone_quantity,
                       'QUANTITY_PRICE')
                       PRICE_ZONE_QUANTITY_PRICE28,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       29,
                       'PRICE')
                       PRICE_ZONE_PRICE29,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_break_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       29,
                       xbpzqbv.price_zone_quantity,
                       'QUANTITY')
                       PRICE_ZONE_QUANTITY29,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_break_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       29,
                       xbpzqbv.price_zone_quantity,
                       'QUANTITY_PRICE')
                       PRICE_ZONE_QUANTITY_PRICE29,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       30,
                       'PRICE')
                       PRICE_ZONE_PRICE30,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_break_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       30,
                       xbpzqbv.price_zone_quantity,
                       'QUANTITY')
                       PRICE_ZONE_QUANTITY30,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_break_info (
                       xppzt.po_header_id,
                       xppzt.inventory_item_id,
                       30,
                       xbpzqbv.price_zone_quantity,
                       'QUANTITY_PRICE')
                       PRICE_ZONE_QUANTITY_PRICE30,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_orgs (
                       pha.vendor_id,
                       pha.vendor_site_id,
                       1)
                       org_price_zone_1,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_orgs (
                       pha.vendor_id,
                       pha.vendor_site_id,
                       2)
                       org_price_zone_2,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_orgs (
                       pha.vendor_id,
                       pha.vendor_site_id,
                       3)
                       org_price_zone_3,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_orgs (
                       pha.vendor_id,
                       pha.vendor_site_id,
                       4)
                       org_price_zone_4,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_orgs (
                       pha.vendor_id,
                       pha.vendor_site_id,
                       5)
                       org_price_zone_5,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_orgs (
                       pha.vendor_id,
                       pha.vendor_site_id,
                       6)
                       org_price_zone_6,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_orgs (
                       pha.vendor_id,
                       pha.vendor_site_id,
                       7)
                       org_price_zone_7,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_orgs (
                       pha.vendor_id,
                       pha.vendor_site_id,
                       8)
                       org_price_zone_8,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_orgs (
                       pha.vendor_id,
                       pha.vendor_site_id,
                       9)
                       org_price_zone_9,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_orgs (
                       pha.vendor_id,
                       pha.vendor_site_id,
                       10)
                       org_price_zone_10,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_orgs (
                       pha.vendor_id,
                       pha.vendor_site_id,
                       11)
                       org_price_zone_11,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_orgs (
                       pha.vendor_id,
                       pha.vendor_site_id,
                       12)
                       org_price_zone_12,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_orgs (
                       pha.vendor_id,
                       pha.vendor_site_id,
                       13)
                       org_price_zone_13,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_orgs (
                       pha.vendor_id,
                       pha.vendor_site_id,
                       14)
                       org_price_zone_14,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_orgs (
                       pha.vendor_id,
                       pha.vendor_site_id,
                       15)
                       org_price_zone_15,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_orgs (
                       pha.vendor_id,
                       pha.vendor_site_id,
                       16)
                       org_price_zone_16,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_orgs (
                       pha.vendor_id,
                       pha.vendor_site_id,
                       17)
                       org_price_zone_17,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_orgs (
                       pha.vendor_id,
                       pha.vendor_site_id,
                       18)
                       org_price_zone_18,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_orgs (
                       pha.vendor_id,
                       pha.vendor_site_id,
                       19)
                       org_price_zone_19,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_orgs (
                       pha.vendor_id,
                       pha.vendor_site_id,
                       20)
                       org_price_zone_20,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_orgs (
                       pha.vendor_id,
                       pha.vendor_site_id,
                       21)
                       org_price_zone_21,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_orgs (
                       pha.vendor_id,
                       pha.vendor_site_id,
                       22)
                       org_price_zone_22,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_orgs (
                       pha.vendor_id,
                       pha.vendor_site_id,
                       23)
                       org_price_zone_23,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_orgs (
                       pha.vendor_id,
                       pha.vendor_site_id,
                       24)
                       org_price_zone_24,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_orgs (
                       pha.vendor_id,
                       pha.vendor_site_id,
                       25)
                       org_price_zone_25,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_orgs (
                       pha.vendor_id,
                       pha.vendor_site_id,
                       26)
                       org_price_zone_26,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_orgs (
                       pha.vendor_id,
                       pha.vendor_site_id,
                       27)
                       org_price_zone_27,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_orgs (
                       pha.vendor_id,
                       pha.vendor_site_id,
                       28)
                       org_price_zone_28,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_orgs (
                       pha.vendor_id,
                       pha.vendor_site_id,
                       29)
                       org_price_zone_29,
                    XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_orgs (
                       pha.vendor_id,
                       pha.vendor_site_id,
                       30)
                       org_price_zone_30,
                    pha.po_header_id,
                    msib.inventory_item_id,
                    pha.vendor_id,
                    pha.vendor_site_id,
                    pha.vendor_contact_id,
                    NULL implement_date,
                    xbpzqbv.price_zone_quantity,
                    pha.org_id  --Added 1/22/2015 by Lee Spitzer TMS Ticket 20140909-00032
               FROM xxwc.XXWC_BPA_PRICE_ZONE_TBL xppzt,
                    apps.po_headers_all pha,
                    apps.po_lines_all pla,
                    ap_suppliers ass,
                    apps.ap_supplier_sites_all assa,
                    mtl_system_items_b msib,
                    xxwc.XXWC_BPA_PROMO_TBL xppt,
                    XXWC_BPA_PRICE_ZONE_Q_BRKS_VW xbpzqbv
              WHERE     msib.organization_Id =
                           FND_PROFILE.VALUE ('XXWC_ITEM_MASTER_ORG')
                    AND pha.po_header_id = pla.po_header_id
                    AND pha.org_id = pla.org_id -- This condition added by Raghavendra on 19-Nov-2014 As per Canada OU WEB ADI Test
                    AND pha.org_id = assa.org_id -- This condition added by Raghavendra on 19-Nov-2014 As per Canada OU WEB ADI Test
                    AND xppzt.org_id(+) = pla.org_id -- This condition added by Raghavendra on 19-Nov-2014 As per Canada OU WEB ADI Test
                    AND pha.org_id = FND_PROFILE.VALUE ('ORG_ID') -- This condition added by Raghavendra on 19-Nov-2014 As per Canada OU WEB ADI Test
                    AND pla.po_header_id = xppzt.po_header_id(+)
                    AND pla.item_id = xppzt.inventory_item_id(+)
                    AND xppzt.po_header_id = xppt.po_header_id(+)
                    AND xppzt.inventory_item_id = xppt.inventory_item_id(+)
                    AND xppt.org_id(+) = xppzt.org_id
                    AND pha.vendor_id = ass.vendor_id
                    AND pha.vendor_id = assa.vendor_id
                    AND pha.vendor_site_id = assa.vendor_site_id
                    AND pla.item_Id = msib.inventory_item_id
                    AND SYSDATE BETWEEN NVL (pha.start_date, SYSDATE - 1)
                                    AND NVL (pha.end_date, SYSDATE + 1)
                    --AND    SYSDATE <= nvl(pla.expiration_date,SYSDATE+1) --Include Expired Lines
                    AND NVL (pha.cancel_flag, 'N') = 'N'
                    AND NVL (pla.cancel_flag, 'N') = 'N'
                    AND xppzt.po_header_id = xbpzqbv.po_header_id(+)
                    AND xppzt.inventory_item_id = xbpzqbv.inventory_item_id(+)
             UNION
             SELECT DISTINCT pha.segment1 po_number,
                             ass.vendor_name,
                             ass.segment1 vendor_num,
                             assa.vendor_site_code,
                             NULL                  --msib.segment1 item_number
                                 ,
                             NULL          --msib.description item_description
                                 ,
                             NULL                         --vendor_item_number
                                 --,NULL --msib.primary_uom_code
                                 --,NULL --UPC_CODE
                                 --,NULL --MFG_PART_NUMBER
                             ,
                             NULL                        --pla.expiration_date
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 0, 'PRICE') NATIONAL_PRICE
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 0, 'QUANTITY') NATIONAL_QUANTITY
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 0, 'QUANTITY_PRICE') NATIONAL_QUANTITY_PRICE
                                 ,
                             NULL                           --xppt.promo_price
                                 ,
                             NULL                      --xppt.promo_start_date
                                 ,
                             NULL                        --xppt.promo_end_date
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 1, 'PRICE') PRICE_ZONE1
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 1, 'QUANTITY') QUANTITY_ZONE1
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 1, 'QUANTITY_PRICE') PRICE_ZONE_QUANTITY_PRICE1
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 2, 'PRICE') PRICE_ZONE2
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 2, 'QUANTITY') QUANTITY_ZONE2
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 2, 'QUANTITY_PRICE') PRICE_ZONE_QUANTITY_PRICE2
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 3, 'PRICE') PRICE_ZONE3
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 3, 'QUANTITY') QUANTITY_ZONE3
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 3, 'QUANTITY_PRICE') PRICE_ZONE_QUANTITY_PRICE3
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 4, 'PRICE') PRICE_ZONE4
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 4, 'QUANTITY') QUANTITY_ZONE4
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 4, 'QUANTITY_PRICE') PRICE_ZONE_QUANTITY_PRICE4
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 5, 'PRICE') PRICE_ZONE5
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 5, 'QUANTITY') QUANTITY_ZONE5
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 5, 'QUANTITY_PRICE') PRICE_ZONE_QUANTITY_PRICE5
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 6, 'PRICE') PRICE_ZONE6
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 6, 'QUANTITY') QUANTITY_ZONE6
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 6, 'QUANTITY_PRICE') PRICE_ZONE_QUANTITY_PRICE6
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 7, 'PRICE') PRICE_ZONE7
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 7, 'QUANTITY') QUANTITY_ZONE7
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 7, 'QUANTITY_PRICE') PRICE_ZONE_QUANTITY_PRICE7
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 8, 'PRICE') PRICE_ZONE8
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 8, 'QUANTITY') QUANTITY_ZONE8
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 8, 'QUANTITY_PRICE') PRICE_ZONE_QUANTITY_PRICE8
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 9, 'PRICE') PRICE_ZONE9
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 9, 'QUANTITY') QUANTITY_ZONE9
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 9, 'QUANTITY_PRICE') PRICE_ZONE_QUANTITY_PRICE9
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 10, 'PRICE') PRICE_ZONE10
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 10, 'QUANTITY') QUANTITY_ZONE10
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 10, 'QUANTITY_PRICE') PRICE_ZONE_QUANTITY_PRICE10
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 11, 'PRICE') PRICE_ZONE11
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 11, 'QUANTITY') QUANTITY_ZONE11
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 11, 'QUANTITY_PRICE') PRICE_ZONE_QUANTITY_PRICE11
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 12, 'PRICE') PRICE_ZONE12
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 12, 'QUANTITY') QUANTITY_ZONE12
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 12, 'QUANTITY_PRICE') PRICE_ZONE_QUANTITY_PRICE12
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 13, 'PRICE') PRICE_ZONE13
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 13, 'QUANTITY') QUANTITY_ZONE13
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 13, 'QUANTITY_PRICE') PRICE_ZONE_QUANTITY_PRICE13
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 14, 'PRICE') PRICE_ZONE14
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 14, 'QUANTITY') QUANTITY_ZONE14
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 14, 'QUANTITY_PRICE') PRICE_ZONE_QUANTITY_PRICE14
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 15, 'PRICE') PRICE_ZONE15
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 15, 'QUANTITY') QUANTITY_ZONE15
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 15, 'QUANTITY_PRICE') PRICE_ZONE_QUANTITY_PRICE15
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 16, 'PRICE') PRICE_ZONE16
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 16, 'QUANTITY') QUANTITY_ZONE16
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 16, 'QUANTITY_PRICE') PRICE_ZONE_QUANTITY_PRICE16
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 17, 'PRICE') PRICE_ZONE17
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 17, 'QUANTITY') QUANTITY_ZONE17
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 17, 'QUANTITY_PRICE') PRICE_ZONE_QUANTITY_PRICE17
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 18, 'PRICE') PRICE_ZONE18
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 18, 'QUANTITY') QUANTITY_ZONE18
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 18, 'QUANTITY_PRICE') PRICE_ZONE_QUANTITY_PRICE18
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 19, 'PRICE') PRICE_ZONE19
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 19, 'QUANTITY') QUANTITY_ZONE19
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 19, 'QUANTITY_PRICE') PRICE_ZONE_QUANTITY_PRICE19
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 20, 'PRICE') PRICE_ZONE20
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 20, 'QUANTITY') QUANTITY_ZONE20
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 20, 'QUANTITY_PRICE') PRICE_ZONE_QUANTITY_PRICE20
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 21, 'PRICE') PRICE_ZONE21
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 21, 'QUANTITY') QUANTITY_ZONE21
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 21, 'QUANTITY_PRICE') PRICE_ZONE_QUANTITY_PRICE21
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 22, 'PRICE') PRICE_ZONE22
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 22, 'QUANTITY') QUANTITY_ZONE22
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 22, 'QUANTITY_PRICE') PRICE_ZONE_QUANTITY_PRICE22
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 23, 'PRICE') PRICE_ZONE23
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 23, 'QUANTITY') QUANTITY_ZONE23
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 23, 'QUANTITY_PRICE') PRICE_ZONE_QUANTITY_PRICE23
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 24, 'PRICE') PRICE_ZONE24
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 24, 'QUANTITY') QUANTITY_ZONE24
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 24, 'QUANTITY_PRICE') PRICE_ZONE_QUANTITY_PRICE24
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 25, 'PRICE') PRICE_ZONE25
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 25, 'QUANTITY') QUANTITY_ZONE25
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 25, 'QUANTITY_PRICE') PRICE_ZONE_QUANTITY_PRICE25
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 26, 'PRICE') PRICE_ZONE26
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 26, 'QUANTITY') QUANTITY_ZONE26
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 26, 'QUANTITY_PRICE') PRICE_ZONE_QUANTITY_PRICE26
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 27, 'PRICE') PRICE_ZONE27
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 27, 'QUANTITY') QUANTITY_ZONE27
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 27, 'QUANTITY_PRICE') PRICE_ZONE_QUANTITY_PRICE27
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 28, 'PRICE') PRICE_ZONE28
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 28, 'QUANTITY') QUANTITY_ZONE28
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 28, 'QUANTITY_PRICE') PRICE_ZONE_QUANTITY_PRICE28
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 29, 'PRICE') PRICE_ZONE29
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 29, 'QUANTITY') QUANTITY_ZONE29
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 29, 'QUANTITY_PRICE') PRICE_ZONE_QUANTITY_PRICE29
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 30, 'PRICE') PRICE_ZONE30
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 30, 'QUANTITY') QUANTITY_ZONE30
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_info(xppzt.po_header_id, xppzt.inventory_item_id, 30, 'QUANTITY_PRICE') PRICE_ZONE_QUANTITY_PRICE30
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_orgs(pha.vendor_id, pha.vendor_site_id, 1) org_price_zone_1
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_orgs(pha.vendor_id, pha.vendor_site_id, 2) org_price_zone_2
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_orgs(pha.vendor_id, pha.vendor_site_id, 3) org_price_zone_3
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_orgs(pha.vendor_id, pha.vendor_site_id, 4) org_price_zone_4
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_orgs(pha.vendor_id, pha.vendor_site_id, 5) org_price_zone_5
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_orgs(pha.vendor_id, pha.vendor_site_id, 6) org_price_zone_6
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_orgs(pha.vendor_id, pha.vendor_site_id, 7) org_price_zone_7
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_orgs(pha.vendor_id, pha.vendor_site_id, 8) org_price_zone_8
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_orgs(pha.vendor_id, pha.vendor_site_id, 9) org_price_zone_9
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_orgs(pha.vendor_id, pha.vendor_site_id, 10) org_price_zone_10
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_orgs(pha.vendor_id, pha.vendor_site_id, 11) org_price_zone_11
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_orgs(pha.vendor_id, pha.vendor_site_id, 12) org_price_zone_12
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_orgs(pha.vendor_id, pha.vendor_site_id, 13) org_price_zone_13
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_orgs(pha.vendor_id, pha.vendor_site_id, 14) org_price_zone_14
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_orgs(pha.vendor_id, pha.vendor_site_id, 15) org_price_zone_15
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_orgs(pha.vendor_id, pha.vendor_site_id, 16) org_price_zone_16
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_orgs(pha.vendor_id, pha.vendor_site_id, 17) org_price_zone_17
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_orgs(pha.vendor_id, pha.vendor_site_id, 18) org_price_zone_18
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_orgs(pha.vendor_id, pha.vendor_site_id, 19) org_price_zone_19
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_orgs(pha.vendor_id, pha.vendor_site_id, 20) org_price_zone_20
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_orgs(pha.vendor_id, pha.vendor_site_id, 21) org_price_zone_21
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_orgs(pha.vendor_id, pha.vendor_site_id, 22) org_price_zone_22
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_orgs(pha.vendor_id, pha.vendor_site_id, 23) org_price_zone_23
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_orgs(pha.vendor_id, pha.vendor_site_id, 24) org_price_zone_24
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_orgs(pha.vendor_id, pha.vendor_site_id, 25) org_price_zone_25
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_orgs(pha.vendor_id, pha.vendor_site_id, 26) org_price_zone_26
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_orgs(pha.vendor_id, pha.vendor_site_id, 27) org_price_zone_27
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_orgs(pha.vendor_id, pha.vendor_site_id, 28) org_price_zone_28
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_orgs(pha.vendor_id, pha.vendor_site_id, 29) org_price_zone_29
                                 ,
                             NULL --XXWC_BPA_PRICE_ZONE_PKG.get_price_zone_orgs(pha.vendor_id, pha.vendor_site_id, 30) org_price_zone_30
                                 ,
                             pha.po_header_id,
                             NULL,
                             pha.vendor_id,
                             pha.vendor_site_id,
                             pha.vendor_contact_id,
                             NULL implement_date,
                             NULL price_zone_quantity,
                             pha.org_id --Added 1/22/2015 by Lee Spitzer TMS Ticket 20140909-00032
               FROM apps.po_headers_all pha,
                    ap_suppliers ass,
                    apps.ap_supplier_sites_all assa
              WHERE     pha.vendor_id = ass.vendor_id
                    AND pha.vendor_id = assa.vendor_id
                    AND pha.org_id = assa.org_id -- -- This condition added by Raghavendra on 19-Nov-2014 As per Canada OU WEB ADI Test
                    AND pha.org_id = FND_PROFILE.VALUE ('ORG_ID') -- This condition added by Raghavendra on 19-Nov-2014 As per Canada OU WEB ADI Test
                    AND pha.vendor_site_id = assa.vendor_site_id
                    AND SYSDATE BETWEEN NVL (pha.start_date, SYSDATE - 1)
                                    AND NVL (pha.end_date, SYSDATE + 1)
                    AND NVL (pha.cancel_flag, 'N') = 'N'
                    AND NOT EXISTS
                               (SELECT *
                                  FROM apps.po_lines_all pla
                                 WHERE     pla.po_header_id = pha.po_header_id
                                       AND pla.org_id = pha.org_id -- This condition added by Raghavendra on 19-Nov-2014 As per Canada OU WEB ADI Test
                                       AND pla.org_id =
                                              FND_PROFILE.VALUE ('ORG_ID') -- This condition added by Raghavendra on 19-Nov-2014 As per Canada OU WEB ADI Test
                                       AND NVL (pla.cancel_flag, 'N') = 'N'
                                       AND SYSDATE <=
                                              NVL (pla.expiration_date,
                                                   SYSDATE + 1)))
   ORDER BY po_header_id,
            CASE WHEN expiration_date IS NULL THEN 0 ELSE 1 END,
            item_number;