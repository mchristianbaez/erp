--
-- XXWC_AR_CREDIT_COPY_TRX_VW  (View)
--

CREATE OR REPLACE FORCE VIEW APPS.XXWC_AR_CREDIT_COPY_TRX_VW
(
   CUSTOMER_TRX_ID
  ,TRX_NUMBER
  ,TRX_DATE
  ,CUSTOMER_ID
  ,CUSTOMER_NAME
  ,CUSTOMER_NUMBER
  ,BILL_TO_SITE_USE_ID
  ,BILL_TO_LOCATION
  ,BILL_TO_ADDRESS
  ,SHIP_TO_SITE_USE_ID
  ,SHIP_TO_LOCATION
  ,SHIP_TO_ADDRESS
  ,TRX_SOURCE_NAME
  ,TRX_TYPE_NAME
  ,ORDER_NUMBER
  ,BALANCE_DUE
  ,INVOICE_TOTAL
  ,REBILL_BILL_TO_SITE_USE_ID
  ,REBILL_SHIP_TO_SITE_USE_ID
  ,REASON_CODE
  ,SELECT_TRX
  ,CREATED_BY
  ,CREATION_DATE
  ,LAST_UPDATE_DATE
  ,LAST_UPDATED_BY
  ,LAST_UPDATE_LOGIN
  ,CREDIT_COPY_TRX_ID
  ,REV_CUSTOMER_TRX_ID
  ,NEW_CUSTOMER_TRX_ID
  ,BATCH_SOURCE_NAME
  ,PROCESS_STATUS
  ,ERROR_MESSAGE
  ,OBJECT_VERSION_NUMBER
  ,SHIP_TO_SITE_NUMBER
  ,BILL_TO_SITE_NUMBER
)
AS
   SELECT trx.customer_trx_id
         ,trx.trx_number
         ,trx.trx_date
         ,trx.sold_to_customer_id customer_id
         ,rac_sold_to_customer_name customer_name
         ,rac_sold_to_customer_num customer_number
         ,trx.bill_to_site_use_id
         ,su_bill_to_location bill_to_location
         ,raa_bill_to_concat_address bill_to_address
         ,trx.ship_to_site_use_id
         ,su_ship_to_location ship_to_location
         ,raa_ship_to_concat_address ship_to_address
         ,trx.bs_batch_source_name trx_source_name
         ,DECODE (trx.ctt_class
                 ,'INV', 'Invoice'
                 ,'CM', 'Credit Memo'
                 ,'DM', 'Debit Memo'
                 ,'Invoice')
             trx_type_name
         ,trx.ct_reference order_number
         ,XXWC_CREDIT_COPY_TRX_UTIL_PKG.get_balance_due (trx.customer_trx_id)
             balance_due
         ,XXWC_CREDIT_COPY_TRX_UTIL_PKG.get_invoice_total (
             trx.customer_trx_id)
             invoice_total
         ,xacc.rebill_bill_to_site_use_id
         ,xacc.rebill_ship_to_site_use_id
         ,xacc.reason_code
         ,NVL (xacc.select_trx, 'N') select_trx
         ,xacc.created_by
         ,xacc.creation_date
         ,xacc.last_update_date
         ,xacc.last_updated_by
         ,xacc.last_update_login
         ,xacc.credit_copy_trx_id
         ,xacc.rev_customer_trx_id
         ,xacc.new_customer_trx_id
         ,trx.bs_batch_source_name batch_source_name
         ,NVL (xacc.process_status, 'R') process_status
         ,xacc.error_message
         ,xacc.object_version_number
         ,xxwc_credit_copy_trx_util_pkg.get_ship_to_site__number (
             trx.ship_to_site_use_id
            ,trx.sold_to_customer_id)
             ship_to_site_number
         ,xxwc_credit_copy_trx_util_pkg.get_bill_to_site__number (
             trx.bill_to_site_use_id
            ,trx.sold_to_customer_id)
             bill_to_site_number
     FROM apps.xxwc_ar_credit_copy_trx xacc, ra_customer_trx_partial_v trx
    WHERE     xacc.customer_trx_id(+) = trx.customer_trx_id
          AND trx.complete_flag = 'Y'
          AND trx.status_trx = 'OP'
          /*AND trx.ctt_class IN ('INV','DM');*/
          AND trx.ctt_class IN ('INV', 'DM', 'CM')
          AND trx.bs_batch_source_name NOT LIKE  '%CONVERSION%'; -- Added by Maha for TMS# 20130909-00743


