CREATE OR REPLACE VIEW APPS.XXWC_GENERATE_FORECAST_ADI_VW as
/*************************************************************************
   *   $Header XXWC_GENERATE_FORECAST_ADI_VW
   *   Module Name: XXWC_GENERATE_FORECAST_ADI_VW
   *
   *   PURPOSE:   View used in XXWC Generate Demand Forecast via Web ADI Upload - TMS Ticket # 20130201-01386 
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        08/12/2013  Lee Spitzer                Initial Version
   * ***************************************************************************/

select mp.organization_id,
       mp.organization_code,
       mfd.forecast_designator,
       mfd.description,
       'DC' branch_dc_mode,
       'BRANCH' item_range,
       NULL item_category,
       NULL item_number,
       3 bucket_type,
       (Select MRP_CALENDAR.Date_Offset(mp.organization_id, 3, SYSDATE,1) From DUAL) start_date,--trunc(sysdate) start_date,
       12 num_of_forecast_periods,
       3  num_of_previous_periods,
       0 constant_seasonality,
       0 seasonality_1,
       0 seasonality_2,
       0 seasonality_3,
       0 seasonality_4,
       0 seasonality_5,
       0 seasonality_6,
       0 seasonality_7,
       0 seasonality_8,
       0 seasonality_9,
       0 seasonality_10,
       0 seasonality_11,
       0 seasonality_12
from   mtl_parameters mp,
       mrp_forecast_designators mfd
where  mp.organization_id = mfd.organization_id
and    mfd.forecast_set is not null
order by mp.organization_code;
