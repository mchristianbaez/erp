
  CREATE OR REPLACE FORCE VIEW "APPS"."XXHDS_CC_HOLDER_INFO" ("EMPLOYEE_ID", "MASKED_CARD_NUMBER", "CARDMEMBER_NAME", "CARD_PROGRAM_NAME", "CARDHOLDER_EMP_NAME", "CARDHOLDER_ACT_TERM_DATE", "EMPLOYEE_NUMBER", "CARD_ID") AS 
  SELECT DISTINCT card.employee_id employee_id
      ,'********' || substr(card.card_number, -4) masked_card_number
      ,card.cardmember_name cardmember_name
      ,cprog.card_program_name card_program_name
      ,papf.full_name cardholder_emp_name
      ,pps.actual_termination_date cardholder_act_term_date
      ,papf.employee_number
      ,card.card_id
  FROM ap.ap_cards_all           card
      ,ap.ap_card_programs_all   cprog
      ,hr.per_all_people_f       papf
      ,hr.per_periods_of_service pps
WHERE card.employee_id = papf.person_id
   AND papf.person_id = pps.person_id
   AND card.card_program_id = cprog.card_program_id
   AND papf.effective_end_date = '31-DEC-4712'

;
