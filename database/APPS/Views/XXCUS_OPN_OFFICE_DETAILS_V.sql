
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUS_OPN_OFFICE_DETAILS_V" ("ID", "NAME", "RENTABLE_AREA", "USABLE_AREA", "ASSIGNABLE_AREA", "VACANT_AREA", "BOOKABLE_FLAG", "COMMON", "COMMON_AREA_FLAG", "SECONDARY_CIRCULATION", "SPACE_TYPE_CODE", "SPACE_TYPE", "SITE_TYPE_CODE", "SITE_TYPE", "ASSIGNMENT_TYPE_CODE", "ASSIGNMENT_TYPE", "OCCU_STATUS_CODE", "OCCU_STATUS", "OCCU_EMP_ASSIGNABLE", "OCCU_DEPT_ASSIGNABLE", "OCCU_CUST_ASSIGNABLE", "OCCU_DISPOSITION_CODE", "OCCU_DISPOSITION", "OCCU_ACC_TREATMENT_CODE", "OCCU_ACC_TREATMENT", "OCCU_MAXIMUM", "OCCU_OPTIMUM", "OCCU_AREA_UTILIZED", "OCCU_MAX_VACANCY", "LOCATION_ID", "ACTIVE_START_DATE", "ACTIVE_END_DATE", "ATTRIBUTE_CATEGORY", "ATTRIBUTE1", "ATTRIBUTE2", "ATTRIBUTE3", "ATTRIBUTE4", "ATTRIBUTE5", "ATTRIBUTE6", "ATTRIBUTE7", "ATTRIBUTE8", "ATTRIBUTE9", "ATTRIBUTE10", "ATTRIBUTE11", "ATTRIBUTE12", "ATTRIBUTE13", "ATTRIBUTE14", "ATTRIBUTE15") AS 
  SELECT LOCATION_ALIAS                                                               ID
          ,OFFICE                                                                      NAME       
          ,RENTABLE_AREA                                                               RENTABLE_AREA
          ,USABLE_AREA                                                                 USABLE_AREA
          ,ASSIGNABLE_AREA                                                             ASSIGNABLE_AREA                     
          ,VACANT_AREA                                                                 VACANT_AREA
          ,Null                                                                        BOOKABLE_FLAG
          ,Null                                                                        COMMON
          ,COMMON_AREA_FLAG                                                            COMMON_AREA_FLAG
          ,SECONDARY_CIRCULATION                                                       SECONDARY_CIRCULATION          
          ,SPACE_TYPE_LOOKUP_CODE                                                      SPACE_TYPE_CODE
          ,SPACE_TYPE                                                                  SPACE_TYPE
          ,FUNCTION_TYPE_LOOKUP_CODE                                                   SITE_TYPE_CODE
          ,FUNCTION_TYPE                                                               SITE_TYPE
          ,STANDARD_TYPE_LOOKUP_CODE                                                   ASSIGNMENT_TYPE_CODE
          ,STANDARD_TYPE                                                               ASSIGNMENT_TYPE                                               
          ,OCCUPANCY_STATUS_CODE                                                       OCCU_STATUS_CODE
          ,CASE
            WHEN OCCUPANCY_STATUS_CODE ='Y' THEN 'Occupiable'
            ELSE 'Non-Occupiable'
           END                                                                         OCCU_STATUS                    
          ,ASSIGNABLE_EMP                                                              OCCU_EMP_ASSIGNABLE
          ,ASSIGNABLE_CC                                                               OCCU_DEPT_ASSIGNABLE
          ,ASSIGNABLE_CUST                                                             OCCU_CUST_ASSIGNABLE
          ,DISPOSITION_CODE                                                            OCCU_DISPOSITION_CODE 
          ,DISPOSITION                                                                 OCCU_DISPOSITION
          ,ACC_TREATMENT_CODE                                                          OCCU_ACC_TREATMENT_CODE         
          ,ACC_TREATMENT                                                               OCCU_ACC_TREATMENT
          ,MAX_CAPACITY                                                                OCCU_MAXIMUM
          ,OPTIMUM_CAPACITY                                                            OCCU_OPTIMUM
          ,UTILIZED_CAPACITY                                                           OCCU_AREA_UTILIZED
          ,VACANCY                                                                     OCCU_MAX_VACANCY   
          ,LOCATION_ID 
          ,ACTIVE_START_DATE
          ,ACTIVE_END_DATE
          ,ATTRIBUTE_CATEGORY
          ,ATTRIBUTE1
          ,ATTRIBUTE2
          ,ATTRIBUTE3
          ,ATTRIBUTE4
          ,ATTRIBUTE5
          ,ATTRIBUTE6
          ,ATTRIBUTE7
          ,ATTRIBUTE8
          ,ATTRIBUTE9
          ,ATTRIBUTE10
          ,ATTRIBUTE11
          ,ATTRIBUTE12
          ,ATTRIBUTE13
          ,ATTRIBUTE14
          ,ATTRIBUTE15          
    FROM   PN_OFFICES_V A
    WHERE  1 =1;
