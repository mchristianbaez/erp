CREATE OR REPLACE FORCE VIEW
 apps.XXWC_EGO_CROSS_REF_SUPP_VW (vendor_info, vendor_number)
/*********************************************************************************
File Name: xxwc_ego_cros_ref_supp_vw
PROGRAM TYPE: View definition
HISTORY
PURPOSE: This view is created get all the suppliers including the word 'ALL'
==================================================================================
VERSION DATE          AUTHOR(S)         DESCRIPTION
------- -----------   --------------- --------------------------------------------
1.0     27-Aug-2014   KPIT   Initial view definition  TMS # 20140210-00116
*********************************************************************************/                                                            
AS
   SELECT DISTINCT vendor_info, vendor_number
              FROM apps.xxwc_ego_suppliers_actv_dct_vw
   UNION ALL
   SELECT 'ALL', 'ALL'
     FROM DUAL;