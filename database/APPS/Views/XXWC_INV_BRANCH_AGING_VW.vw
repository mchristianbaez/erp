CREATE OR REPLACE VIEW APPS.XXWC_INV_BRANCH_AGING_VW AS
/***************************************************************************
    $Header APPS.XXWC_INV_BRANCH_AGING_VW $
    Module Name: XXWC_INV_BRANCH_AGING_VW
    PURPOSE: View used for Inventory Branch Aging Report
 
    REVISIONS:
    Ver    Date       	Author                Description
    ------ ---------  	------------------    ----------------
     1.0    16-DEC-2015    Lee Spitzer         TMS # 20150928-00193 Branch Aging Report
                                                Initial Version
/***************************************************************************/

SELECT  mmt.inventory_item_id
      , mmt.organization_id
      , mmt.transaction_date
      , mmt.attribute15
      , mmt.primary_quantity
      , decode(mmt.transaction_action_id,
               2,2,
               28,2,
               1) priority
      , sum(mmt.primary_quantity) OVER (PARTITION BY mmt.organization_id, mmt.inventory_item_id ORDER BY  decode(mmt.transaction_action_id,
               2,2,
               28,2,
               1), mmt.transaction_date DESC, mmt.transaction_id) running_qty
FROM    mtl_material_transactions mmt
WHERE  NVL(MMT.logical_transaction,-1) <> 1 --do not need to search for logical transaction
AND    MMT.transaction_action_id NOT IN (40,41,50,51,52) -- Ignore periodic cost updates
AND    MMT.transaction_source_type_id <> 14 -- The only transactions other than the ones ignored above that affect inventory valuation and have null cost_group_id are standard cost updates (non-PJM/WMS)
AND    nvl(mmt.primary_quantity,0) > 0 --only need to check for incremental transactions
order by       decode(mmt.transaction_action_id,
               2,2,
               28,2,
               1)
;