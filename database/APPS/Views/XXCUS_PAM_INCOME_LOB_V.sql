CREATE OR REPLACE FORCE VIEW "APPS"."XXCUS_PAM_INCOME_LOB_V"
(
   "MVID",
   "CUST_ID",
   "LOB_NAME",
   "MV_NAME",
   "REBATE_TYPE",
   "CAL_YEAR",
   "AGREEMENT_YEAR",
   "MONTH_ID",
   "MONTH_NAME",
   "AMOUNT"
)
AS
     SELECT MVID,
            OFU_CUST_ACCOUNT_ID         CUST_ID,
            LOB                         LOB_NAME,
            c.party_name                MV_NAME,
            REBATE_TYPE,
            TO_CHAR (CALENDAR_YEAR)     CAL_YEAR,
            TO_CHAR (AGREEMENT_YEAR)    AGREEMENT_YEAR,
            CALENDAR_MONTH_ID           MONTH_ID,
            DECODE (CALENDAR_MONTH_ID,
                    1, 'Jan',
                    2, 'Feb',
                    3, 'Mar',
                    4, 'Apr',
                    5, 'May',
                    6, 'Jun',
                    7, 'Jul',
                    8, 'Aug',
                    9, 'Sep',
                    10, 'Oct',
                    11, 'Nov',
                    'Dec')              MONTH_NAME,
            SUM (ACCRUAL_AMOUNT) Amount
       FROM XXCUS.xxcus_ytd_income_b, xxcus.xxcus_rebate_customers c
      WHERE     1 = 1
            AND C.CUSTOMER_ID         =OFU_CUST_ACCOUNT_ID
            AND C.CUSTOMER_ATTRIBUTE1 ='HDS_MVID'
            AND AGREEMENT_YEAR        > 2012
   GROUP BY MVID,
            OFU_CUST_ACCOUNT_ID,
            LOB,
            c.party_name,
            REBATE_TYPE,
            TO_CHAR (CALENDAR_YEAR),
            TO_CHAR (AGREEMENT_YEAR),
            CALENDAR_MONTH_ID;

--
COMMENT ON TABLE APPS.XXCUS_PAM_INCOME_LOB_V IS 'RFC 43034 / ESMS 280464';
--