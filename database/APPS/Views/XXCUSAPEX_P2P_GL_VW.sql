CREATE OR REPLACE VIEW APPS.XXCUSAPEX_P2P_GL_VW 
/******************************************************************************
   NAME:       APPS.XXCUSAPEX_P2P_GL_VW 
   PURPOSE:  This view is used to extract GL information from iprocurement

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        4/07/2015    Pahwa Nancy   ESMS-RFC # 42130 New View 
******************************************************************************/
AS
SELECT distinct    gl.code_combination_id, gl.segment1 Prd_Line,
       gl.segment2              Location,
       gl.segment3              Cost_Center,
       gl.segment4              Account,
       gl.segment5              Project_Code,
       gl.segment6              Ft_Use1,
       gl.segment7              Ft_Use2,
       d.description            description
FROM gl_code_combinations gl , xxcusgl_account_apex_vw d
where gl.segment4 = d.account;
