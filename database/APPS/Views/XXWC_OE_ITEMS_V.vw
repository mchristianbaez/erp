--
-- XXWC_OE_ITEMS_V  (View)
--

CREATE OR REPLACE FORCE VIEW APPS.XXWC_OE_ITEMS_V
(
   ITEM
  ,ITEM_DESCRIPTION
  ,INVENTORY_ITEM_ID
  ,INVENTORY_ITEM
  ,ITEM_IDENTIFIER_TYPE
  ,ORGANIZATION_ID
  ,ITEM_TYPE
  ,PRIMARY_UOM_CODE
)
AS
   SELECT B.concatenated_segments ITEM
         ,B.description ITEM_DESCRIPTION
         ,B.inventory_item_id INVENTORY_ITEM_ID
         ,B.concatenated_segments INVENTORY_ITEM
         ,'INT' ITEM_IDENTIFIER_TYPE
         ,B.organization_id ORGANIZATION_ID
         ,B.item_type item_type
         ,B.primary_uom_code
     FROM MTL_SYSTEM_ITEMS_B_KFV B
    WHERE     B.customer_order_enabled_flag = 'Y'
          AND bom_item_type IN (1, 4)
          AND B.organization_id =
                 oe_sys_parameters.VALUE ('MASTER_ORGANIZATION_ID')
   UNION ALL
   SELECT items.cross_reference ITEM
         ,NVL (items.description, b.description) ITEM_DESCRIPTION
         ,B.inventory_item_id INVENTORY_ITEM_ID
         ,B.concatenated_segments INVENTORY_ITEM
         ,items.cross_reference_type ITEM_IDENTIFIER_TYPE
         ,B.organization_id ORGANIZATION_ID
         ,B.item_type item_type
         ,B.primary_uom_code
     FROM mtl_cross_references items, MTL_SYSTEM_ITEMS_B_KFV B
    WHERE     items.cross_reference_type = 'VENDOR'
          AND items.inventory_item_id = B.inventory_item_id
          AND B.organization_id =
                 oe_sys_parameters.VALUE ('MASTER_ORGANIZATION_ID')
          AND B.customer_order_enabled_flag = 'Y'
          AND (   items.org_independent_flag = 'Y'
               OR items.organization_id =
                     oe_sys_parameters.VALUE ('MASTER_ORGANIZATION_ID'));


