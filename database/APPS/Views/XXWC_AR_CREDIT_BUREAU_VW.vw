--
-- XXWC_AR_CREDIT_BUREAU_VW  (View)
--

CREATE OR REPLACE FORCE VIEW APPS.XXWC_AR_CREDIT_BUREAU_VW
(
   FILE_REC
  ,ORG_ID
)
AS
     SELECT (   RPAD (NVL (SUBSTR (TO_CHAR (customer_number), 1, 20), ' ')
                     ,20
                     ,' ')                                  -- Customer Number
             || RPAD (NVL (SUBSTR (customer_name, 1, 25), ' '), 25, ' ')
             -- Customer Name
             || RPAD (NVL (SUBSTR (customer_address_1, 1, 25), ' '), 25, ' ')
             -- Customer Address 1
             || RPAD (NVL (SUBSTR (customer_address_2, 1, 25), ' '), 25, ' ')
             -- Customer Address 2
             || RPAD (NVL (SUBSTR (customer_address_3, 1, 25), ' '), 25, ' ')
             -- Customer Address 3
             || RPAD (NVL (SUBSTR (customer_address_4, 1, 25), ' '), 25, ' ')
             -- Customer Address 4
             || RPAD (NVL (SUBSTR (telephone_number, 1, 16), ' '), 16, ' ')
             -- telephone number
             || RPAD (NVL (SUBSTR (terms, 1, 13), ' '), 13, ' ')      -- terms
             || RPAD (NVL (SUBSTR (payment_experience, 1, 13), ' '), 13, ' ')
             -- payment experience
             || RPAD (NVL (SUBSTR (days_slow, 1, 3), ' '), 3, ' ')
             -- days slow
             || RPAD (
                   NVL (TO_CHAR (date_of_last_invoice, 'YYYYMMDD'), '########')
                  ,8
                  ,'#')                                -- date of last invoice
             || RPAD (NVL (TO_CHAR (high_credit_date, 'YYYYMMDD'), '########')
                     ,8
                     ,'#')                                 -- High credit date
             || RPAD (NVL (SUBSTR (average_days_to_pay, 1, 4), ' '), 4, ' ')
             -- avg days to pay
             || RPAD (NVL (LTRIM (TO_CHAR (high_credit, '9999990.99')), '0.00')
                     ,10
                     ,' ')                                      -- high_credit
             || RPAD (
                   NVL (LTRIM (TO_CHAR (aging_category_1, '9999990.99'))
                       ,'0.00')
                  ,10
                  ,' ')                                   -- bucket 1: current
             || RPAD (
                   NVL (LTRIM (TO_CHAR (aging_category_2, '9999990.99'))
                       ,'0.00')
                  ,10
                  ,' ')                                      -- bucket 2: 1-30
             || RPAD (
                   NVL (LTRIM (TO_CHAR (aging_category_3, '9999990.99'))
                       ,'0.00')
                  ,10
                  ,' ')                                     -- bucket 3: 31-60
             || RPAD (
                   NVL (LTRIM (TO_CHAR (aging_category_4, '9999990.99'))
                       ,'0.00')
                  ,10
                  ,' ')                                     -- bucket 4: 61-90
             || RPAD (
                   NVL (LTRIM (TO_CHAR (aging_category_5, '9999990.99'))
                       ,'0.00')
                  ,10
                  ,' ')                                   -- bucket 5: Over 90
             || RPAD (
                   NVL (LTRIM (TO_CHAR (total_amt_31_or_more, '9999990.99'))
                       ,'0.00')
                  ,10
                  ,' ')                                 -- Total AR 31 or more
             || RPAD (NVL (LTRIM (TO_CHAR (total_ar, '9999990.99')), '0.00')
                     ,10
                     ,' ')                                         -- total AR
             || RPAD (NVL (SUBSTR (branch_number, 1, 10), ' '), 10, ' ')
             -- branch number
             || RPAD (NVL (LTRIM (SUBSTR (credit_manager_name, 1, 25)), ' ')
                     ,25
                     ,' ')                              -- Credit Manager Name
             || RPAD (NVL (SUBSTR (line_of_business, 1, 25), ' '), 25, ' ')
             -- Line of business
             || RPAD (NVL (LTRIM (TO_CHAR (total_sales, '9999990.99')), '0.00')
                     ,10
                     ,' ')                                      -- total sales
                          )
               file_rec
           ,org_id
       FROM apps.XXWC_CB_FINAL_TBL
   ORDER BY customer_number;


