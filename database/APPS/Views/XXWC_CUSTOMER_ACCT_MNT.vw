/* Formatted on 16-Aug-2012 00:11:42 (QP5 v5.206) */
-- Start of DDL Script for View APPS.XXWC_CUSTOMER_ACCT_MNT
-- Generated 16-Aug-2012 00:11:38 from APPS@EBIZFQA

CREATE OR REPLACE VIEW XXWC_CUSTOMER_ACCT_MNT
(
    PARTY_NAME
   ,ACCOUNT_NUMBER
   ,PARTY_NUMBER
   ,STATUS
   ,ORG_ID
   ,CUST_ACCOUNT_ID
)
AS
    SELECT /*+ leading(c.party) use_nl(c.party,c.cust_acct) */
          PARTY.PARTY_NAME
          ,C.ACCOUNT_NUMBER ACCOUNT_NUMBER
          ,PARTY.PARTY_NUMBER
          ,C.STATUS
          ,CS.ORG_ID
          ,C.CUST_ACCOUNT_ID CUST_ACCOUNT_ID
      FROM HZ_CUST_ACCOUNTS C, HZ_PARTIES PARTY, HZ_CUST_ACCT_SITES_ALL CS
     WHERE     C.PARTY_ID = PARTY.PARTY_ID
           AND PARTY.PARTY_TYPE = 'ORGANIZATION'
           AND C.CUST_ACCOUNT_ID = CS.CUST_ACCOUNT_ID
           AND CS.BILL_TO_FLAG = 'P'
           AND CS.ORG_ID = 162
/

-- End of DDL Script for View APPS.XXWC_CUSTOMER_ACCT_MNT