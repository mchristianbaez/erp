--
-- XXWC_CUST_SITE_CONTACT_VW  (View)
--
  /********************************************************************************
  FILE NAME: XXWC_CUST_SITE_CONTACT_VW.vw
  
  PROGRAM TYPE: VIEW
  
  PURPOSE: To show customer site contact details
  
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       	DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     				 	Initial creation 
  2.0     21-JUL-2015  Maharajan Shunmugam      TMS#20150706-00044 ARS - Duplicate records in extract, multiple days have the same invoice number
  ********************************************************************************/
CREATE OR REPLACE FORCE VIEW apps.xxwc_cust_site_contact_vw
(
   cust_account_id
  ,account_name
  ,account_number
  ,site_use_code
  ,location
  ,party_site_name
  ,cust_account_site_id
  ,party_id
  ,con_first_name
  ,con_last_name
  ,email_address
  ,phone
  ,fax
  ,phone_line_type
  ,address1
  ,address2
  ,city
  ,state
  ,county
  ,postal_code
  ,responsibility_type
)
AS
   SELECT hca.cust_account_id
         ,hca.account_name
         ,hca.account_number
         ,sua.site_use_code
         ,sua.location
         ,ps.party_site_name
         ,acct_role.cust_acct_site_id
         ,acct_role.party_id
         ,party_con.person_first_name
         ,party_con.person_last_name
         ,party_addr.email_address
         ,phon.raw_phone_number phone
         ,NVL (fax.raw_phone_number, ' ') fax
         ,phon.phone_line_type
         ,party_addr.address1
         ,party_addr.address2
         ,party_addr.city
         ,party_addr.state
         ,party_addr.county
         ,party_addr.postal_code
         ,acct_role.responsibility_type
     FROM apps.hz_cust_accounts hca
         ,apps.hz_cust_acct_sites asa
         ,apps.hz_cust_site_uses sua
         ,apps.hz_party_sites ps
         ,(SELECT rol.cust_account_id
                 ,rol.cust_acct_site_id
                 ,rol.party_id
                 ,res.responsibility_type
             FROM apps.hz_cust_account_roles rol
                 ,(  SELECT rr.responsibility_type
                           ,r.cust_acct_site_id
			   ,MAX(r.cust_account_role_id) cust_account_role_id                        --Added for ver#2.0 by Maha for TMS#20150706-00044
                           ,MAX (r.creation_date) creation_date
                       FROM apps.hz_cust_account_roles r, apps.hz_role_responsibility rr
                      WHERE r.cust_account_role_id = rr.cust_account_role_id
                   GROUP BY r.cust_acct_site_id, rr.responsibility_type) res  
            WHERE     rol.cust_acct_site_id = res.cust_acct_site_id
                  AND rol.creation_date = res.creation_date
                  AND rol.cust_account_role_id = res.cust_account_role_id  --Added for ver#2.0 by Maha for TMS#20150706-00044
                  AND rol.status = 'A') acct_role
         ,apps.hz_relationships rel
         ,apps.hz_parties party_con
         ,apps.hz_parties party_addr
         ,(SELECT owner_table_id
                 ,owner_table_name
                 ,phone_line_type
                 ,raw_phone_number
             FROM apps.hz_contact_points
            WHERE     phone_line_type = 'GEN'
                  AND status = 'A'
                  AND NVL (owner_table_name, 'HZ_PARTIES') = 'HZ_PARTIES') phon
         ,(SELECT owner_table_id
                 ,owner_table_name
                 ,phone_line_type
                 ,raw_phone_number
             FROM apps.hz_contact_points
            WHERE     phone_line_type = 'FAX'
                  AND status = 'A'
                  AND NVL (owner_table_name, 'HZ_PARTIES') = 'HZ_PARTIES') fax
    WHERE     hca.cust_account_id = asa.cust_account_id
          AND asa.cust_acct_site_id = sua.cust_acct_site_id
          AND asa.party_site_id = ps.party_site_id
          AND asa.cust_acct_site_id = acct_role.cust_acct_site_id
          AND acct_role.party_id = rel.party_id
          AND rel.subject_id = party_con.party_id
          AND rel.party_id = party_addr.party_id
          AND acct_role.party_id = phon.owner_table_id(+)
          AND acct_role.party_id = fax.owner_table_id(+)
          AND sua.site_use_code = 'BILL_TO'                         --Added for ver#2.0 by Maha for TMS#20150706-00044
          AND rel.subject_type = 'PERSON';


