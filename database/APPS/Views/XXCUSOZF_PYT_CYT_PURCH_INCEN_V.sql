
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUSOZF_PYT_CYT_PURCH_INCEN_V" ("CURRENT_YTD_REBATE_INCOME", "CURRENT_YTD_COOP_INCOME", "CURRENT_YTD_PURCHASES", "CURRENT_YEAR_ROR", "PRIOR_COOP_YTD_INCOME", "PRIOR_REBATE_YTD_INCOME", "PRIOR_YTD_PURCHASES", "PRIOR_YEAR_ROR", "CURRENT_YEAR_PROGRAM", "PRIOR_YEAR_PROGRAM", "MVID_ACCOUNT", "MVID", "MVID_NAME", "CALENDAR_YEAR", "LOB_NAME", "LOB_ID") AS 
  SELECT ci.Current_ytd_rebate_income Current_ytd_rebate_income,
    ci.Current_ytd_coop_income Current_ytd_coop_income,
    ci.Current_YTD_Purchases Current_YTD_Purchases,
    ROUND(((ci.Current_ytd_rebate_income + ci.Current_ytd_coop_income) / DECODE(ci.Current_YTD_Purchases,0,1000000000000,ci.Current_YTD_Purchases)*100),2) Current_Year_ROR,
    pi.Current_ytd_coop_income Prior_coop_ytd_income,
    pi.Current_ytd_rebate_income Prior_rebate_ytd_income,
    pi.Current_YTD_Purchases Prior_YTD_Purchases,
    ROUND(((pi.Current_ytd_coop_income + pi.Current_ytd_rebate_income) / DECODE(pi.Current_YTD_Purchases,0,100000000000,pi.Current_YTD_Purchases)*100),2) Prior_Year_ROR,
    ci.program_name Current_Year_Program,
    pi.Program_Name Prior_year_Program,
    ci.pmvid mvid_account,
    SUBSTR(hca.account_number, 0, INSTR(hca.account_number, '~')-1),
    ci.pmvid_name mvid_name,
    ci.pcalendar_year calendar_year,
    ci.plob_name lob_name,
    ci.plob_id lob_id
  FROM XXCUS_CURR_INC_PURCHASE_VW pi,
       XXCUS_CURR_INC_PURCHASE_VW ci,
       ozf_time_ent_year y1,
       ozf_time_ent_year y2,
       hz_cust_accounts hca
  WHERE 1               =1
  and hca.attribute1='HDS_MVID'
  AND pi.pmvid_name           = ci.pmvid_name
  AND pi.plob_name         = ci.plob_name
  AND hca.cust_account_id    = ci.pmvid
   AND y1.ent_year_id      = ci.pcalendar_year
  AND y2.ent_year_id         = y1.ent_year_id - 1
  AND y2.ent_year_id      = pi.pcalendar_year
;
