
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUSOZF_ACCR_LOB_BU_V" ("MVID", "LOB", "BU_NM", "VENDOR_RESALE_LINE", "VENDOR_ACCRUALS", "OFFER_NAME", "ACTIVITY_MEDIA_NAME", "RESALE_ORDER_DATE", "PURCHASES", "ACCRUALS") AS 
  SELECT hzca.attribute2 MVID,
  orl.bill_to_party_name LOB,
  orl.line_attribute2 bu_nm,
  orl.sold_from_party_name Vendor_Resale_Line,
  ofu.ofu_mvid_name Vendor_Accruals,
  ofu.offer_name,
    ofu.activity_media_name,
  orl.date_ordered Resale_Order_Date,
  SUM(NVL(selling_price*quantity,0)) Purchases,
  SUM(NVL(ofu.amount,0)) Accruals
FROM ozf_resale_lines_all orl,
  hz_cust_accounts hzca,
  (SELECT 
      object_id,
    ofu_mvid_name,
    offer_name offer_name,
        activity_media_name,
    SUM(NVL(util_acctd_amount,0) ) amount
    FROM xxcus.xxcus_ozf_xla_accruals_b
  WHERE 1=1
  and offer_status_code='ACTIVE'
  GROUP BY 
  object_id,
    ofu_mvid_name,
    offer_name,
    activity_media_name
  ) ofu
WHERE 1=1
and ofu.object_id(+)   = orl.resale_line_id
AND hzca.cust_account_id = orl.sold_from_cust_account_id

GROUP BY hzca.attribute2,
  orl.bill_to_party_name,
  orl.sold_from_party_name,
  ofu.ofu_mvid_name,
  ofu.activity_media_name,
  ofu.offer_name,
  orl.line_attribute2,  
  orl.date_ordered;
