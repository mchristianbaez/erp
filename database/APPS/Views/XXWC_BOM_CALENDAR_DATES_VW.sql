CREATE OR REPLACE VIEW XXWC_BOM_CALENDAR_DATES_VW AS
  Select period_start_date calendar_date, calendar_code, 3 bucket_type
      From BOM_PERIOD_START_DATES
     UNION
    SELECT week_start_date calendar_date, calendar_code, 2 bucket_type
      From BOM_CAL_WEEK_START_DATES
     UNION
    SELECT calendar_date calendar_date, calendar_code, 1 bucket_type
      FROM BOM_CALENDAR_DATES
     Order by 1, 2 ,3 DESC;
     
