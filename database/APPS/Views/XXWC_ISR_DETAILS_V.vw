CREATE OR REPLACE VIEW APPS.XXWC_ISR_DETAILS_V
/**************************************************************************************************
    $Header XXWC_ISR_DETAILS_VW.sql $
    Module Name: XXWC_ISR_DETAILS_VW
    PURPOSE: View built on the EISR Table to use for EIS Reports
    TMS Task Id :  20141001-00063 - EISR Rearchitecure
                   20150317-00111 - EISR Enhancements for Purchasing
    REVISIONS:
    Ver    Date         Author                Description
    ------ -----------  ------------------    ----------------
    1.0    16-Oct-14    Manjula Chellappan    Initial Version
    1.1    15-Oct-15    P.Vamshidhar          TMS#20151008-00138 - Changed column names
**************************************************************************************************/
(
   ORG,
   PRE,
   ITEM_NUMBER,
   VENDOR_NUM,
   VENDOR_NAME,
   SOURCE,
   ST,
   DESCRIPTION,
   CAT,
   PPLT,
   PLT,
   UOM,
   CL,
   STK_FLAG,
   PM,
   MINN,
   MAXN,
   AMU,
   MF_FLAG,
   HIT6_SALES,
   AVER_COST,
   --ITEM_COST,  -- Commented in Rev 1.1 by Vamshi
   LIST_PRICE,                                   -- Added by Vamshi in Rev 1.1
   BPA_COST,
   BPA,
   QOH,
   AVAILABLE,
   AVAILABLEDOLLAR,
   JAN_SALES,
   FEB_SALES,
   MAR_SALES,
   APR_SALES,
   MAY_SALES,
   JUNE_SALES,
   JUL_SALES,
   AUG_SALES,
   SEP_SALES,
   OCT_SALES,
   NOV_SALES,
   DEC_SALES,
   HIT4_SALES,
   ONE_SALES,
   SIX_SALES,
   TWELVE_SALES,
   BIN_LOC,
   MC,
   FI_FLAG,
   FREEZE_DATE,
   RES,
   THIRTEEN_WK_AVG_INV,
   THIRTEEN_WK_AN_COGS,
   TURNS,
   BUYER,
   TS,
   SO,
   INVENTORY_ITEM_ID,
   ORGANIZATION_ID,
   SET_OF_BOOKS_ID,
   ORG_NAME,
   DISTRICT,
   REGION,
   ON_ORD,
   INV_CAT_SEG1,
   WT,
   SS,
   FML,
   OPEN_REQ,
   SOURCING_RULE,
   CLT,
   COMMON_OUTPUT_ID,
   PROCESS_ID,
   AVAIL2,
   INT_REQ,
   DIR_REQ,
   DEMAND,
   ITEM_STATUS_CODE,
   SITE_VENDOR_NUM,
   VENDOR_SITE,
   CORE,
   TIER,
   CAT_SBA_OWNER,
   MFG_PART_NUMBER,
   MST_VENDOR,
   MAKE_BUY,
   ORG_ITEM_STATUS,
   ORG_USER_ITEM_TYPE,
   MST_ITEM_STATUS,
   MST_USER_ITEM_TYPE,
   LAST_RECEIPT_DATE,
   ONHAND_GT_270,
   SUPERSEDE_ITEM,
   FLIP_DATE,
   OPERATING_UNIT
)
AS
   SELECT org,
          pre,
          item_number,
          vendor_num,
          vendor_name,
          source,
          st,
          description,
          cat,
          pplt,
          plt,
          uom,
          cl,
          stk_flag,
          pm,
          minn,
          maxn,
          amu,
          mf_flag,
          DECODE (
             xxeis.eis_po_xxwc_isr_util_pkg.get_isr_rpt_dc_mod_sub,
             'Yes', (  NVL (hit6_store_sales, 0)
                     + NVL (hit6_other_inv_sales, 0)),
             NVL (hit6_store_sales, 0))
             hit6_sales,
          aver_cost,
          --item_cost,   --Commented in Rev 1.1 by Vamshi
          List_price,                             --Added in Rev 1.1 by Vamshi
          bpa_cost,
          bpa,
          qoh,
          available,
          availabledollar,
          DECODE (
             xxeis.eis_po_xxwc_isr_util_pkg.get_isr_rpt_dc_mod_sub,
             'Yes', (NVL (jan_store_sale, 0) + NVL (jan_other_inv_sale, 0)),
             NVL (jan_store_sale, 0))
             jan_sales,
          DECODE (
             xxeis.eis_po_xxwc_isr_util_pkg.get_isr_rpt_dc_mod_sub,
             'Yes', (NVL (feb_store_sale, 0) + NVL (feb_other_inv_sale, 0)),
             NVL (feb_store_sale, 0))
             feb_sales,
          DECODE (
             xxeis.eis_po_xxwc_isr_util_pkg.get_isr_rpt_dc_mod_sub,
             'Yes', (NVL (mar_store_sale, 0) + NVL (mar_other_inv_sale, 0)),
             NVL (mar_store_sale, 0))
             mar_sales,
          DECODE (
             xxeis.eis_po_xxwc_isr_util_pkg.get_isr_rpt_dc_mod_sub,
             'Yes', (NVL (apr_store_sale, 0) + NVL (apr_other_inv_sale, 0)),
             NVL (apr_store_sale, 0))
             apr_sales,
          DECODE (
             xxeis.eis_po_xxwc_isr_util_pkg.get_isr_rpt_dc_mod_sub,
             'Yes', (NVL (may_store_sale, 0) + NVL (may_other_inv_sale, 0)),
             NVL (may_store_sale, 0))
             may_sales,
          DECODE (
             xxeis.eis_po_xxwc_isr_util_pkg.get_isr_rpt_dc_mod_sub,
             'Yes', (NVL (jun_store_sale, 0) + NVL (jun_other_inv_sale, 0)),
             NVL (jun_store_sale, 0))
             june_sales,
          DECODE (
             xxeis.eis_po_xxwc_isr_util_pkg.get_isr_rpt_dc_mod_sub,
             'Yes', (NVL (jul_store_sale, 0) + NVL (jul_other_inv_sale, 0)),
             NVL (jul_store_sale, 0))
             jul_sales,
          DECODE (
             xxeis.eis_po_xxwc_isr_util_pkg.get_isr_rpt_dc_mod_sub,
             'Yes', (NVL (aug_store_sale, 0) + NVL (aug_other_inv_sale, 0)),
             NVL (aug_store_sale, 0))
             aug_sales,
          DECODE (
             xxeis.eis_po_xxwc_isr_util_pkg.get_isr_rpt_dc_mod_sub,
             'Yes', (NVL (sep_store_sale, 0) + NVL (sep_other_inv_sale, 0)),
             NVL (sep_store_sale, 0))
             sep_sales,
          DECODE (
             xxeis.eis_po_xxwc_isr_util_pkg.get_isr_rpt_dc_mod_sub,
             'Yes', (NVL (oct_store_sale, 0) + NVL (oct_other_inv_sale, 0)),
             NVL (oct_store_sale, 0))
             oct_sales,
          DECODE (
             xxeis.eis_po_xxwc_isr_util_pkg.get_isr_rpt_dc_mod_sub,
             'Yes', (NVL (nov_store_sale, 0) + NVL (nov_other_inv_sale, 0)),
             NVL (nov_store_sale, 0))
             nov_sales,
          DECODE (
             xxeis.eis_po_xxwc_isr_util_pkg.get_isr_rpt_dc_mod_sub,
             'Yes', (NVL (dec_store_sale, 0) + NVL (dec_other_inv_sale, 0)),
             NVL (dec_store_sale, 0))
             dec_sales,
          DECODE (
             xxeis.eis_po_xxwc_isr_util_pkg.get_isr_rpt_dc_mod_sub,
             'Yes', (  NVL (hit4_store_sales, 0)
                     + NVL (hit4_other_inv_sales, 0)),
             NVL (hit4_store_sales, 0))
             hit4_sales,
          DECODE (
             xxeis.eis_po_xxwc_isr_util_pkg.get_isr_rpt_dc_mod_sub,
             'Yes', (NVL (one_store_sale, 0) + NVL (one_other_inv_sale, 0)),
             NVL (one_store_sale, 0))
             one_sales,
          DECODE (
             xxeis.eis_po_xxwc_isr_util_pkg.get_isr_rpt_dc_mod_sub,
             'Yes', (NVL (six_store_sale, 0) + NVL (six_other_inv_sale, 0)),
             NVL (six_store_sale, 0))
             six_sales,
          DECODE (
             xxeis.eis_po_xxwc_isr_util_pkg.get_isr_rpt_dc_mod_sub,
             'Yes', (  NVL (twelve_store_sale, 0)
                     + NVL (twelve_other_inv_sale, 0)),
             NVL (twelve_store_sale, 0))
             twelve_sales,
          bin_loc,
          mc,
          fi_flag,
          freeze_date,
          res,
          thirteen_wk_avg_inv,
          thirteen_wk_an_cogs,
          turns,
          buyer,
          ts,
          so,
          inventory_item_id,
          organization_id,
          set_of_books_id,
          org_name,
          district,
          region,
          on_ord,
          inv_cat_seg1,
          wt,
          ss,
          fml,
          open_req,
          sourcing_rule,
          clt,
          common_output_id,
          process_id,
          avail2,
          int_req,
          dir_req,
          demand,
          item_status_code,
          site_vendor_num,
          vendor_site,
          core,
          tier,
          cat_sba_owner,
          mfg_part_number,
          mst_vendor,
          make_buy,
          org_item_status,
          org_user_item_type,
          mst_item_status,
          mst_user_item_type,
          TRUNC (last_receipt_date) last_receipt_date,
          onhand_gt_270,
          supersede_item,
          TRUNC (flip_date) flip_date,
          operating_unit
     FROM xxwc.xxwc_isr_details_all
    WHERE 1 = 1;