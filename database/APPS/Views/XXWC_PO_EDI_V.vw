--
-- XXWC_PO_EDI_V  (View)
--

CREATE OR REPLACE FORCE VIEW APPS.XXWC_PO_EDI_V
(
   NAME
  ,PO_NUMBER
  ,EMAIL_ADDRESS
)
AS
   SELECT haou.name,                                     --operating_unit_name
                    pha.segment1 po_number, papf.email_address
     FROM per_all_people_f papf
         ,apps.po_headers pha
         ,hr_all_organization_units haou
    WHERE     pha.org_id = haou.organization_id
          AND pha.agent_id = papf.person_id
          AND SYSDATE BETWEEN papf.effective_start_date
                          AND papf.effective_end_date;


