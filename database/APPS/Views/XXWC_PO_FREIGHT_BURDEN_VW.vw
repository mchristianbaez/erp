CREATE OR REPLACE FORCE VIEW APPS.XXWC_PO_FREIGHT_BURDEN_VW
AS
  SELECT 
  frt_brdn.VENDOR_ID,
  frt_brdn.VENDOR_NUMBER, 
  frt_brdn.ORGANIZATION_ID, 
  frt_brdn.ORGANIZATION_CODE, 
  frt_brdn.FREIGHT_DUTY,
  frt_brdn.IMPORT_FLAG,
  frt_brdn.FREIGHT_PER_LB,
  frt_brdn.FREIGHT_BASIS,
  frt_brdn.VENDOR_TYPE,
  frt_brdn.SOURCE_ORGANIZATION_ID,
  frt_brdn.SOURCE_ORGANIZATION_CODE
  FROM (SELECT
    xvm.vendor_id
    || '-'
    || xvm.vendor_site_id vendor_id ,
    pov.segment1
    || '-'
    || pov.vendor_name
    || ' ### '
    || povs.vendor_site_code vendor_number ,
    xvm.organization_id ,
    ood.organization_code ,
    xvm.freight_duty ,
    xvm.import_flag,
    xvm.freight_per_lb,
    xvm.freight_basis,
    xvm.vendor_type,
    NULL source_organization_id,
    '' source_organization_code
  FROM
--TMS # 20141001-00164 on 10-Dec-14 by Manjula for MultiOrg
--Reverted the synonym to Table after WEB ADI testing for multiorg  
    xxwc.xxwc_po_freight_burden_tbl xvm ,
    ap_suppliers pov ,
    ap_supplier_sites_all povs ,
    org_organization_definitions ood
  WHERE
    xvm.vendor_id         = pov.vendor_id
  AND xvm.organization_id = ood.organization_id
  AND xvm.vendor_site_id  = povs.vendor_site_id
  AND pov.vendor_id = povs.vendor_id
  AND xvm.vendor_id IS NOT NULL
  AND xvm.vendor_site_id IS NOT NULL
--TMS # 20141001-00164 on 10-Dec-14 by Manjula for MultiOrg  
  AND xvm.org_id = FND_PROFILE.VALUE('ORG_ID')
  AND xvm.org_id = povs.org_id
 -- AND ROWNUM < 100
UNION
 SELECT
    '' vendor_id ,
    NULL vendor_number ,
    xvm.organization_id ,
    ood.organization_code ,
    xvm.freight_duty ,
    xvm.import_flag,
    xvm.freight_per_lb,
    xvm.freight_basis,
    xvm.vendor_type,
    xvm.source_organization_id,
    ood2.organization_code
  FROM
--TMS # 20141001-00164 on 10-Dec-14 by Manjula for MultiOrg
--Reverted the synonym to Table after WEB ADI testing for multiorg
    xxwc.xxwc_po_freight_burden_tbl xvm ,
    org_organization_definitions ood,
    org_organization_definitions ood2
  WHERE
    xvm.organization_id = ood.organization_id
  AND xvm.vendor_site_id IS NULL
  AND xvm.source_organization_id IS NOT NULL
  AND xvm.source_organization_id = ood2.organization_id
--TMS # 20141001-00164 on 10-Dec-14 by Manjula for MultiOrg  
  AND xvm.org_id = FND_PROFILE.VALUE('ORG_ID')
  --AND ROWNUM < 100
  ) frt_brdn
order by frt_brdn.organization_code, frt_brdn.vendor_type, frt_brdn.vendor_number, frt_brdn.source_organization_code;