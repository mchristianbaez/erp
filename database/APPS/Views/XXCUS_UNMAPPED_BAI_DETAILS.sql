
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUS_UNMAPPED_BAI_DETAILS" ("BANK_ACCOUNT_NUM", "TRX_CODE", "DESCRIPTION", "LINE_NUMBER", "TRX_DATE", "TRX_TYPE", "AMOUNT", "TRX_TEXT", "BANK_TRX_NUMBER", "CUSTOMER_TEXT", "INVOICE_TEXT", "BANK_ACCOUNT_TEXT", "REFERENCE_TXT", "JE_STATUS_FLAG", "ACCOUNTING_DATE") AS 
  SELECT ba.bank_account_num, 
       tr.trx_code, 
       tr.description, 
       li.line_number, 
       li.trx_date, 
       li.trx_type, 
       li.amount, 
       li.trx_text, 
       li.bank_trx_number, 
       li.customer_text, 
       li.invoice_text, 
       li.bank_account_text, 
       li.reference_txt, 
       li.je_status_flag, 
       li.accounting_date    
      
  FROM ce.ce_statement_lines li, 
     ce.ce_statement_headers hd, 
     ce.ce_bank_accounts ba,
     (SELECT trx_code, description, bank_account_id FROM ce.ce_transaction_codes tr GROUP BY trx_code, description, bank_account_id) tr
    
where li.statement_header_id = hd.statement_header_id and 
      hd.bank_account_id = ba.bank_account_id and 
      li.trx_code = tr.trx_code and 
      ba.bank_account_id = tr.bank_account_id and
      li.cashflow_id is NULL 

;
