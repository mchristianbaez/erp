
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUSPN_SYSUSERS_VW" ("USER_ID", "USER_NAME", "EMPLOYEE_NAME") AS 
  (select u.user_id, u.user_name
     , nvl(papf.full_name, 'Seeded User') "EMPLOYEE_NAME"
  from applsys.fnd_user u
     , hr.per_all_people_f papf
 where u.employee_id = papf.person_id(+)
   and u.end_date is null
   and papf.effective_end_date = '31-DEC-4712')
;
