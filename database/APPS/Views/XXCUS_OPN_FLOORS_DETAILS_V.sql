
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUS_OPN_FLOORS_DETAILS_V" ("ID", "NAME", "AREA_RENTABLE", "AREA_USABLE", "VACANT_AREA", "BOOKABLE_FLAG", "COMMON_AREA_FLAG", "COMMON", "AREA_ASSIGNABLE", "PRIMARY_CIRCULATION", "PRIMARY_OPS_TYPE_CODE", "PRIMARY_OPS_TYPE", "FUNCTION_OPS_TYPE_CODE", "FUNCTION_TYPE", "STANDARD_TYPE_TYPE_CODE", "STANDARD_TYPE", "OCCU_STATUS_CODE", "OCCU_STATUS", "OCCU_EMP_ASSIGNABLE", "OCCU_DEPT_ASSIGNABLE", "OCCU_CUST_ASSIGNABLE", "OCCU_DISPOSITION_CODE", "OCCU_DISPOSITION", "OCCU_ACC_TREATMENT_CODE", "OCCU_ACC_TREATMENT", "OCCU_MAXIMUM", "OCCU_OPTIMUM", "OCCU_AREA_UTILIZED", "OCCU_MAX_VACANCY", "LOCATION_ID", "ACTIVE_START_DATE", "ACTIVE_END_DATE", "ATTRIBUTE_CATEGORY", "ATTRIBUTE1", "ATTRIBUTE2", "ATTRIBUTE3", "ATTRIBUTE4", "ATTRIBUTE5", "ATTRIBUTE6", "ATTRIBUTE7", "ATTRIBUTE8", "ATTRIBUTE9", "ATTRIBUTE10", "ATTRIBUTE11", "ATTRIBUTE12", "ATTRIBUTE13", "ATTRIBUTE14", "ATTRIBUTE15") AS 
  SELECT LOCATION_ALIAS                                                              ID
          ,FLOOR                                                                       NAME       
          ,PNP_UTIL_FUNC.GET_FLOOR_RENTABLE_AREA(LOCATION_ID, ACTIVE_START_DATE)       AREA_RENTABLE
          ,PNP_UTIL_FUNC.GET_FLOOR_USABLE_AREA(LOCATION_ID, ACTIVE_START_DATE)         AREA_USABLE
          ,PNP_UTIL_FUNC.GET_VACANT_AREA(LOCATION_ID, ACTIVE_START_DATE)               VACANT_AREA
          ,NULL                                                                        BOOKABLE_FLAG
          ,NULL                                                                        COMMON_AREA_FLAG
          ,(
             SELECT COMMON_AREA
             FROM   PN_LOCATIONS_ALL
             WHERE  1 =1
               AND  LOCATION_ID =A.LOCATION_ID
           )                                                                           COMMON                                       
          ,PNP_UTIL_FUNC.GET_FLOOR_ASSIGNABLE_AREA(LOCATION_ID, ACTIVE_START_DATE)     AREA_ASSIGNABLE
          ,SECONDARY_CIRCULATION                                                       PRIMARY_CIRCULATION          
          ,SPACE_TYPE_LOOKUP_CODE                                                      PRIMARY_OPS_TYPE_CODE
          ,SPACE_TYPE                                                                  PRIMARY_OPS_TYPE
          ,FUNCTION_TYPE_LOOKUP_CODE                                                   FUNCTION_OPS_TYPE_CODE
          ,FUNCTION_TYPE                                                               FUNCTION_TYPE
          ,STANDARD_TYPE_LOOKUP_CODE                                                   STANDARD_TYPE_TYPE_CODE
          ,STANDARD_TYPE                                                               STANDARD_TYPE                                                  
          ,OCCUPANCY_STATUS_CODE                                                       OCCU_STATUS_CODE
          ,CASE
            WHEN OCCUPANCY_STATUS_CODE ='Y' THEN 'Occupiable'
            ELSE 'Non-Occupiable'
           END                                                                         OCCU_STATUS                    
          ,ASSIGNABLE_EMP                                                              OCCU_EMP_ASSIGNABLE
          ,ASSIGNABLE_CC                                                               OCCU_DEPT_ASSIGNABLE
          ,ASSIGNABLE_CUST                                                             OCCU_CUST_ASSIGNABLE
          ,DISPOSITION_CODE                                                            OCCU_DISPOSITION_CODE 
          ,DISPOSITION                                                                 OCCU_DISPOSITION
          ,ACC_TREATMENT_CODE                                                          OCCU_ACC_TREATMENT_CODE         
          ,ACC_TREATMENT                                                               OCCU_ACC_TREATMENT
          ,PNP_UTIL_FUNC.GET_FLOOR_MAX_CAPACITY(LOCATION_ID, ACTIVE_START_DATE)        OCCU_MAXIMUM
          ,PNP_UTIL_FUNC.GET_FLOOR_OPTIMUM_CAPACITY(LOCATION_ID, ACTIVE_START_DATE)    OCCU_OPTIMUM
          ,UTILIZED_CAPACITY                                                           OCCU_AREA_UTILIZED
          ,VACANCY                                                                     OCCU_MAX_VACANCY   
          ,LOCATION_ID
          ,ACTIVE_START_DATE
          ,ACTIVE_END_DATE
          ,ATTRIBUTE_CATEGORY
          ,ATTRIBUTE1
          ,ATTRIBUTE2
          ,ATTRIBUTE3
          ,ATTRIBUTE4
          ,ATTRIBUTE5
          ,ATTRIBUTE6
          ,ATTRIBUTE7
          ,ATTRIBUTE8
          ,ATTRIBUTE9
          ,ATTRIBUTE10
          ,ATTRIBUTE11
          ,ATTRIBUTE12
          ,ATTRIBUTE13
          ,ATTRIBUTE14
          ,ATTRIBUTE15                      
    FROM       PN_FLOORS_V A
    WHERE      1 =1;
