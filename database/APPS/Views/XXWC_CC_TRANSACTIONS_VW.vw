CREATE OR REPLACE VIEW APPS.XXWC_CC_TRANSACTIONS_VW AS
select *
FROM (SELECT 
 mp.organization_code
,msib.segment1 item_number
,mmt.INVENTORY_ITEM_ID
,mmt.ORGANIZATION_ID
,mmt.PRIMARY_QUANTITY
,mmt.SUBINVENTORY_CODE
,mmt.COST_GROUP_ID
,mmt.transaction_type_id
,case when mtt.transaction_type_name = 'Subinventory Transfer' and mmt.transaction_reference is not null then 'Standard ' || mmt.transaction_reference else mtt.transaction_type_name end transaction_type_name
,mmt.transaction_date
,mmt.transaction_source_id
,mmt.source_line_id
,mtln.lot_number lot_number
,xcqd.cycle_count_header_id
,xcqd.cycle_count_entry_id
,mmt.transaction_reference transaction_reference
FROM  MTL_MATERIAL_TRANSACTIONS MMT,
      XXWC.XXWC_CC_QUANTITIES_DETAIL_TBL XCQD,
      MTL_SYSTEM_ITEMS_B MSIB,
      MTL_PARAMETERS MP,
      MTL_TRANSACTION_TYPES MTT,
      MTL_TRANSACTION_LOT_NUMBERS MTLN
WHERE  MMT.transaction_date > XCQD.insert_date
AND    MMT.inventory_item_id = XCQD.inventory_item_id
AND    MMT.organization_id = XCQD.organization_id
AND    MMT.subinventory_code = XCQD.subinventory_code
AND    MMT.inventory_item_id = MSIB.inventory_item_id
AND    MMT.organization_id = MSIB.organization_id
AND    MP.organization_id = MMT.organization_id
AND    MMT.transaction_type_id = MTT.transaction_type_id
AND    MMT.transaction_id = MTLN.transaction_id(+)
AND    MMT.inventory_item_id = MTLN.inventory_item_id(+)
AND    MMT.organization_id = MTLN.organization_id(+)
--ORDER BY MMT.TRANSACTION_DATE
UNION
SELECT 
 mp.organization_code
,msib.segment1 item_number
, oola.INVENTORY_ITEM_ID
,oola.ship_from_org_id
,oola.ordered_quantity*-1
,oola.SUBINVENTORY
,NULL--oola.COST_GROUP_ID
,-1
,'Counter ' || ooha.order_number||'-'||oola.line_number||'.'||oola.shipment_number
,oola.creation_date transaction_date
,NULL
,oola.line_id
,mr.lot_number lot_number
,xcqd.cycle_count_header_id
,xcqd.cycle_count_entry_id
,NULL transaction_reference
FROM  apps.OE_ORDER_LINES OOLA,
      XXWC.XXWC_CC_QUANTITIES_DETAIL_TBL XCQD,
      apps.oe_order_headers ooha,
      oe_transaction_types_tl ottt,
      mtl_system_items_b msib,
      mtl_parameters mp,
      mtl_reservations mr
WHERE oola.inventory_item_id = XCQD.inventory_item_id
AND   oola.ship_from_org_id = XCQD.organization_id
AND   ottt.name like '%COUNTER%'
AND   ooha.header_id = oola.header_id
--AND   trunc(ooha.booked_date) >= trunc(XCQD.insert_date)
AND   oola.flow_status_code = 'BOOKED'
AND   ooha.order_type_id = ottt.transaction_type_id
AND   oola.booked_flag = 'Y'
AND   nvl(oola.cancelled_flag,'N') = 'N'
AND    oola.inventory_item_id = MSIB.inventory_item_id
AND    oola.ship_from_org_id = MSIB.organization_id
AND    oola.ship_from_org_id = MP.organization_id
AND    oola.line_id = mr.demand_source_line_Id(+)
AND    oola.inventory_item_id = mr.inventory_item_id(+)
and    oola.ship_from_org_id = mr.organization_id(+))
ORDER BY transaction_date; 

