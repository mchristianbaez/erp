CREATE OR REPLACE FORCE VIEW APPS.XXWC_INV_ITEM_SEARCH_V
   /*************************************************************************
   *   $Header XXWC_INV_ITEM_SEARCH_V.sql $
   *   Module Name: xxwc Advance Item Search view for Form
   *
   *   PURPOSE:   View create script for XXWC Advanced Item Search Form
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        27-Mar-2013 Consuelo Gonzalez       Initial Version for TMS20130215-00987
   *   1.1        02-Jul-2014 Shankar Hariharan       TMS20140619-00181 AIS form and OM Price
   *                                                  Check Form performance improvement
   *   1.2        08-Oct-2014 Veeranjaneya Reddy      TMS20141001-00164 Canada Multi Org changes(views)
   *   1.3        30-Oct-2015 Pattabhi Avula          TMS 20150930-00197 Update AIS to filter our 
   *                                                  Inactivated specials
   *   1.4        04-07-2016  Rakesh Patel            TMS#20160406-00101-PUBD - Modifications to XXWC INV Item Search Form for PUBD
   *   1.5        14-Jun-2016 Rakesh Patel            TMS-20160526-00196-EIS- Rental Utilization and Usage Rpt-WC - Quantity Demand is not working
   * ***************************************************************************/
(
   INVENTORY_ITEM_ID,
   ORGANIZATION_ID,
   SEGMENT1,
   DESCRIPTION,
   ORGANIZATION_CODE,
   ORG_NAME,
   PRICING_REGION,
   ON_HAND_QTY,
   OH_GENPCK_QTY,
   AVAILABLE_TO_RESERVE,
   AVAILABLE_TO_TRANSACT,
   GLOBAL_ON_HAND,
   ON_ORDER,
   OPEN_SALES_ORDERS,
   CATEGORY_DESCRIPTION,
   SALES_VELOCITY,
   LIST_PRICE,
   MIN_MINMAX_QUANTITY,
   MAX_MINMAX_QUANTITY,
   LOT_CONTROL_CODE,
   RENTAL_RETURN_QTY,
   COUNTER_ORDER_QTY,
   ITEM_TYPE,
   LAST_PRICE_PAID,
   STK_FLAG,
   PRIMARY_UOM,
   PUBD, ----Added by Rakesh Patel for TMS#20160406-00101
   OPEN_SALES_ORDERS_NEW -- Added by Rakesh Patel for TMS-20160526-00196
)
AS
   SELECT a.inventory_item_id,
          a.organization_id,
          UPPER (a.segment1) segment1,
          UPPER (a.description) description,
          b.organization_code,
          c.organization_name org_name,
          b.attribute9 pricing_region,
          xxwc_ascp_scwb_pkg.get_on_hand (a.inventory_item_id, a.organization_id, 'G') -- Shankar TMS 20130107-00851 17-Jan-2013 changed from H to RQ
                                                                                      on_hand_qty,
          xxwc_ascp_scwb_pkg.get_on_hand (a.inventory_item_id, a.organization_id, 'GP') ---- Added by Shankar 08-Mar-2013
             oh_genpck_qty,
          xxwc_ascp_scwb_pkg.get_on_hand (a.inventory_item_id, a.organization_id, 'R')
             available_to_reserve,
          xxwc_ascp_scwb_pkg.get_on_hand (a.inventory_item_id, a.organization_id, 'T') available_to_transact --New definition for Global on hand  -- Shankar TMS 20130107-00851 17-Jan-2013
     -- 07/23/2013 CG: TMS 20130715-00357: AIS Form Changes. Change to display Y/N for Global Onhand
 /*
  (NVL (
    (SELECT  SUM (transaction_quantity)
    FROM   mtl_onhand_quantities x
 WHERE x.inventory_item_id = a.inventory_item_id
   AND EXISTS
     (SELECT  1
    FROM mtl_secondary_inventories_fk_v si
     WHERE  si.organization_id =
      x.organization_id
    AND si.availability_type = 1
     AND si.secondary_inventory_name =
     x.subinventory_code))
  , 0
   )
   - (SELECT NVL (
    SUM(l.ordered_quantity
   * po_uom_s.po_uom_convert (
     um.unit_of_measure
   , a.primary_unit_of_measure
     , l.inventory_item_id
   ))
  , 0
  ) -- Shankar TMS 20130124-00974 23-Jan-2013
 FROM apps.oe_order_lines l
   , apps.oe_order_headers h
  , mtl_units_of_measure_vl um
  WHERE l.header_id = h.header_id
  AND h.flow_status_code NOT IN
    ('ENTERED', 'CLOSED', 'CANCELLED')
   AND ( (l.flow_status_code = 'AWAITING_SHIPPING')
    OR (l.flow_status_code = 'BOOKED'
  AND l.line_type_id = 1005)) -- Shankar TMS 20130107-00851 17-Jan-2013
  --AND l.ship_from_org_id = a.organization_id
  AND l.inventory_item_id = a.inventory_item_id
  AND l.order_quantity_uom = um.uom_code))
  */
          ,
          (CASE
              WHEN (SELECT COUNT (x.transaction_quantity) has_onhand
                      FROM mtl_onhand_quantities x
                     WHERE x.inventory_item_id = a.inventory_item_id) > 0
              THEN
                 'Y'
              ELSE
                 'N'
           END)
             global_on_hand,
             0 on_order, --20140619-00181 Shankar 26-Jun-2014
          --xxwc_inv_ais_pkg.get_onorder_qty (a.organization_id, a.inventory_item_id) on_order,
          (SELECT NVL (
                     SUM (
                          l.ordered_quantity
                        * po_uom_s.po_uom_convert (um.unit_of_measure,
                                                   a.primary_unit_of_measure,
                                                   l.inventory_item_id)),
                     0)                                    -- Shankar TMS 20130124-00974 23-Jan-2013
             FROM apps.oe_order_lines l, apps.oe_order_headers h, mtl_units_of_measure_vl um
            WHERE     l.header_id = h.header_id
                  AND h.flow_status_code NOT IN ('ENTERED', 'CLOSED', 'CANCELLED')
                  AND (   (    l.flow_status_code = 'AWAITING_SHIPPING'
                           AND NOT EXISTS
                                      (SELECT 'Out For Delivery'
                                         FROM apps.xxwc_wsh_shipping_stg x1
                                        WHERE     x1.header_id = l.header_id
                                              AND x1.line_id = l.line_id
                                              AND x1.ship_from_org_id = l.ship_from_org_id
                                              AND x1.status IN 'OUT_FOR_DELIVERY'))
                       OR (l.flow_status_code = 'BOOKED' AND l.line_type_id = 1005)) -- Shankar TMS 20130107-00851 17-Jan-2013
                  AND l.ship_from_org_id = a.organization_id
                  AND l.inventory_item_id = a.inventory_item_id
                  AND l.order_quantity_uom = um.uom_code)
             open_sales_orders,  --UnCommented by Rakesh Patel for TMS#20160526-00196 EIS report is using this column value
          UPPER (e.description) category_description -- 08/01/2013 CG: TMS 20130715-00357: Changed from subselect to function call for performance issues with SV condition from form
 /*(SELECT   g.segment1
  FROM mtl_item_categories f
  , mtl_categories g
   , mtl_category_sets h
   WHERE  a.organization_id = f.organization_id
 AND a.inventory_item_id = f.inventory_item_id
 AND f.category_id = g.category_id
  AND f.category_set_id = h.category_set_id
 AND h.category_set_name = 'Sales Velocity'
 AND h.structure_id = g.structure_id)*/
          ,
          xxwc_mv_routines_pkg.get_item_category (a.inventory_item_id,
                                                  a.organization_id,
                                                  'Sales Velocity',
                                                  1)
             sales_velocity -- 06/24/2013 CG: TMS 20130107-00832: Pricing Project Changed to use function to pull market list price
 /*, (SELECT  ll.operand
 FROM qp_secu_list_headers_v lh, qp_list_lines_v ll
   WHERE   lh.list_type_code = 'PRL'
  --AND (lh.name LIKE ('CATEGORY %') or lh.name='SPECIALS PRICE LIST')
   AND lh.name LIKE ('CATEGORY %')
    AND lh.name <> 'CATEGORY 7 - OTHER ITEMS'
  AND lh.list_header_id = ll.list_header_id
  AND SYSDATE BETWEEN NVL (ll.start_date_active
   , SYSDATE - 1)
 AND  NVL (ll.end_date_active
   , SYSDATE + 1)
 AND ll.product_id = a.inventory_item_id
    AND ROWNUM = 1)*/
          ,
          NVL (
             xxwc_qp_market_price_util_pkg.get_market_list_price (a.inventory_item_id,
                                                                  a.organization_id),
             0)
             list_price,
          a.min_minmax_quantity,
          a.max_minmax_quantity,
          a.lot_control_code                               -- Shankar TMS 20130107-00851 17-Jan-2013
                            ,
          (SELECT NVL (
                     SUM (
                          l.ordered_quantity
                        * -1
                        * po_uom_s.po_uom_convert (um.unit_of_measure,
                                                   a.primary_unit_of_measure,
                                                   l.inventory_item_id)),
                     0)
             FROM apps.oe_order_lines l, apps.oe_order_headers h, mtl_units_of_measure_vl um
            WHERE     l.header_id = h.header_id
                  AND h.flow_status_code NOT IN ('ENTERED', 'CLOSED', 'CANCELLED')
                  AND (l.flow_status_code = 'AWAITING_RETURN')
                  AND l.ship_from_org_id = a.organization_id
                  AND l.inventory_item_id = a.inventory_item_id
                  AND a.item_type = 'RENTAL'
                  AND l.order_quantity_uom = um.uom_code)
             rental_return_qty                  -- Shankar TMS 20130401-00990 09-May-2013 AIS Bundle
                              -- 06/13/2013 CG: TMS 20130611-01337: Added counter open lines
          ,
          (SELECT NVL (
                     SUM (
                          l.ordered_quantity
                        * po_uom_s.po_uom_convert (um.unit_of_measure,
                                                   a.primary_unit_of_measure,
                                                   l.inventory_item_id)),
                     0)                                    -- Shankar TMS 20130124-00974 23-Jan-2013
             FROM apps.oe_order_lines l, apps.oe_order_headers h, mtl_units_of_measure_vl um
            WHERE     l.header_id = h.header_id
                  AND h.flow_status_code NOT IN ('ENTERED', 'CLOSED', 'CANCELLED')
                  AND l.line_type_id = 1005
                  AND l.ship_from_org_id = a.organization_id
                  AND l.inventory_item_id = a.inventory_item_id
                  AND l.order_quantity_uom = um.uom_code)
             counter_order_qty -- 07/23/2013 CG: TMS 20130715-00357: AIS Form Changes. Point 3: Added to be able to query stock/non-stock
                              ,
          a.item_type,
          (xxwc_inv_ais_pkg.get_last_price_paid (
              TO_NUMBER (fnd_profile.VALUE ('XXWC_AIS_P_HEADER_ID')),
              TO_NUMBER (fnd_profile.VALUE ('XXWC_AIS_P_QUOTE_NUM')),
              a.inventory_item_id))
             last_price_paid -- 08/06/2013 CG: TMS 20130715-00357: Added link to ISR table to attempt to address perf for non-stock condition
                                                                                   -- , isr.stk_flag
                                                 -- 08/19/2013 CG: Temporarily corrected in TRN ONLY
          ,
          NULL stk_flag                     -- 11/27/2013 CG: TMS 20131121-00159: Adding Primary UOM
                       ,
          a.primary_uom_code,
          xxwc_ascp_scwb_pkg.get_on_hand (a.inventory_item_id, a.organization_id, 'PUBD') --Added by Rakesh Patel for TMS#20160406-00101
          PUBD,
		  xxwc_ascp_scwb_pkg.get_open_orders_qty(a.inventory_item_id, a.organization_id)--Added by Rakesh Patel for TMS#20160406-00101
             open_sales_orders_new --Added for TMS-20160526-00196
     FROM mtl_system_items_b a,
          mtl_parameters b,
          org_organization_definitions c,
          mtl_item_categories d,
          mtl_categories e
    -- 08/06/2013 CG: TMS 20130715-00357: Added link to ISR table to attempt to address perf for non-stock condition
    -- 08/19/2013 CG: Temporarily corrected in TRN ONLY
    -- , xxeis.eis_xxwc_po_isr_tab isr
    WHERE     a.organization_id = b.organization_id
          AND b.organization_id = c.organization_id
          AND b.master_organization_id = fnd_profile.VALUE ('XXWC_ITEM_MASTER_ORG')
          /*07/23/2013 CG: TMS 20130715-00357. Point 2: Added to allow global search
                AND b.master_organization_id <> b.organization_id*/
          /* commented 16-DEC-2012 by Shankar per requirement from Randy
               TMS 20121211-00825
              AND (a.item_type <> 'NON-STOCK'
              OR (a.item_type = 'NON-STOCK'
              AND EXISTS
               (SELECT 'x'
              FROM mtl_onhand_quantities q
               WHERE q.inventory_item_id = a.inventory_item_id
               AND q.organization_id = a.organization_id)))*/
          AND a.inventory_item_id = d.inventory_item_id
          AND a.organization_id = d.organization_id
          AND d.category_id = e.category_id
          AND e.structure_id = 101
		   -- Ver 1.3 Added below condition by Pattabhi on 30-Oct-2015 for TMS20150930-00197
          AND a.inventory_item_status_code!='Inactive'
/