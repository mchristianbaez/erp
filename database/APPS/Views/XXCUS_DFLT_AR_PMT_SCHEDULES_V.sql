
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUS_DFLT_AR_PMT_SCHEDULES_V" ("PAYMENT_SCHEDULE_ID", "CLASS", "TRX_NUMBER") AS 
  SELECT payment_schedule_id, class, trx_number
     FROM ar_payment_schedules_all
    WHERE payment_schedule_id = -1
   UNION ALL
   SELECT payment_schedule_id, class, trx_number
     FROM ar_payment_schedules_all
    WHERE payment_schedule_id = -2
   UNION ALL
   SELECT payment_schedule_id, class, trx_number
     FROM ar_payment_schedules_all
    WHERE payment_schedule_id = -3
   UNION ALL
   SELECT payment_schedule_id, class, trx_number
     FROM ar_payment_schedules_all
    WHERE payment_schedule_id = -4
   UNION ALL
   SELECT payment_schedule_id, class, trx_number
     FROM ar_payment_schedules_all
    WHERE payment_schedule_id = -5
   UNION ALL
   SELECT payment_schedule_id, class, trx_number
     FROM ar_payment_schedules_all
    WHERE payment_schedule_id = -6
   UNION ALL
   SELECT payment_schedule_id, class, trx_number
     FROM ar_payment_schedules_all
    WHERE payment_schedule_id = -7
   UNION ALL
   SELECT payment_schedule_id, class, trx_number
     FROM ar_payment_schedules_all
    WHERE payment_schedule_id = -8
   UNION ALL
   SELECT payment_schedule_id, class, trx_number
     FROM ar_payment_schedules_all
    WHERE payment_schedule_id = -9
   UNION ALL
   SELECT payment_schedule_id, class, trx_number
     FROM ar_payment_schedules_all
    WHERE payment_schedule_id = -10;
