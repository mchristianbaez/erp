CREATE OR REPLACE VIEW APPS.XXWC_ITEM_STOCK_LOCATOR_VW as
Select MP.organization_code, 
       MSI.organization_id,
       MSI.inventory_item_id,
       MSI.segment1 item_number, 
       MSI.DESCRIPTION,
       MIL.segment1 cur_location,
       -- 11/26/2013 CG: TMS 20131120-00024: Added for Bin Delete Option
       null delete_flag
 From MTL_SECONDARY_LOCATORS MSL, 
      MTL_ITEM_LOCATIONS_KFV MIL, 
      MTL_SYSTEM_ITEMS_B MSI, 
      MTL_PARAMETERS MP 
Where MP.organization_id = MSI.organization_id 
And MSI.organization_id = MSL.organization_id(+)
And MSI.inventory_item_id = MSL.inventory_item_id(+) 
And MSL.secondary_locator = MIL.inventory_location_id(+) 
And MIL.subinventory_code(+) = 'General';