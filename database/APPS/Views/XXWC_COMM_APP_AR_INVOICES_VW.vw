/* Formatted on 11/1/2012 4:51:17 PM (QP5 v5.115.810.9015) */
--
-- XXWC_COMM_APP_AR_INVOICES_VW  (View)
--

CREATE OR REPLACE FORCE VIEW apps.xxwc_comm_app_ar_invoices_vw (
   operating_unit_id
 , operating_unit_name
 , customer_trx_id
 , order_number
 , invoice_number
 , account_number
 , bill_to_job_number
 , suborder_number
 , salesrep_number
 , salesrep_name
 , purchase_order
 , payment_term
 , trx_date
 , gl_date
 , trx_creation_date
 , item_sales_total
 , tax_original
 , freight_original
 , invoice_total
 , prism_cost
 , line_cost
 , branch_cost
 , gross_margin
 , gp_percent
 , batch_source
)
AS
   SELECT	"OPERATING_UNIT_ID"
		  , "OPERATING_UNIT_NAME"
		  , "CUSTOMER_TRX_ID"
		  , "ORDER_NUMBER"
		  , "INVOICE_NUMBER"
		  , "ACCOUNT_NUMBER"
		  , "BILL_TO_JOB_NUMBER"
		  , "SUBORDER_NUMBER"
		  , "SALESREP_NUMBER"
		  , "SALESREP_NAME"
		  , "PURCHASE_ORDER"
		  , "PAYMENT_TERM"
		  , "TRX_DATE"
		  , "GL_DATE"
		  , "TRX_CREATION_DATE"
		  , "ITEM_SALES_TOTAL"
		  , "TAX_ORIGINAL"
		  , "FREIGHT_ORIGINAL"
		  , "INVOICE_TOTAL"
		  , "PRISM_COST"
		  , "LINE_COST"
		  , "BRANCH_COST"
		  , "GROSS_MARGIN"
		  , "GP_PERCENT"
		  , "BATCH_SOURCE"
	 FROM	apps.xxwc_comm_app_ar_invoices_mv;


--GRANT SELECT ON APPS.XXWC_COMM_APP_AR_INVOICES_VW TO INTERFACE_APEXWC;

