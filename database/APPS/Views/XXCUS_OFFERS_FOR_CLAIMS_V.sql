
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUS_OFFERS_FOR_CLAIMS_V" ("AGREEMENT_DESC", "AGREEMENT_CODE", "CAL_YEAR", "PAYMENT_FREQUENCY", "GLOBAL") AS 
  SELECT    B.DESCRIPTION
          || CASE
                WHEN b.global_flag = 'Y' THEN ', Global: Yes'
                ELSE ', Global: No'
             END
          || ', Frequency: '
          || b.attribute6
          || ', Calendar Year: '
          || b.attribute7          
             agreement_desc,
             B.NAME
          || '-'
          || B.LIST_HEADER_ID
          || '-'
          || B.ATTRIBUTE6
          || '-'
          || B.GLOBAL_FLAG
          || '-'
          || B.ATTRIBUTE7
          || '-'
          || A.ORG_ID AGREEMENT_CODE
         ,B.ATTRIBUTE7
         ,B.ATTRIBUTE6
         ,B.GLOBAL_FLAG
          
     /*
     Note: ESMS ticket 229451, Date: 14-NOV-2013
     */
     FROM OZF_OFFERS A, QP_LIST_HEADERS_VL B
    WHERE     1 = 1
          AND B.LIST_HEADER_ID = A.QP_LIST_HEADER_ID
          AND A.STATUS_CODE = 'ACTIVE'
          AND A.AUTOPAY_FLAG = 'Y'
          AND B.ATTRIBUTE7 >= '2013'                 --agreement calendar year
          AND B.ATTRIBUTE6 IN ('ANNUAL', 'QTRLY', 'MONTHLY', 'SEMIANNUAL') --agreement payment frequency;
