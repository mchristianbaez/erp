/* Formatted on 4/3/2013 4:27:27 AM (QP5 v5.206) */
-- Start of DDL Script for View APPS.XXWC_CUSTOMER_SITES_VR
-- Generated 4/3/2013 4:27:19 AM from APPS@EBIZDEV

CREATE OR REPLACE VIEW apps.xxwc_customer_sites_vr
(
    cust_account_id
   ,party_id
   ,account_number
   ,cust_accounts_status
   ,account_name
   ,cust_acct_site_id
   ,cust_acct_sites_status
   ,org_id
   ,cust_acct_sites_bill_to_flag
   ,cust_acct_sites_ship_to_flag
   ,party_site_name
   ,party_site_number
   ,location_id
   ,party_site_status
   ,address1
   ,address2
   ,city
   ,state
   ,postal_code
   ,location
   ,bill_to_site_use_id
   ,site_use_id
   ,site_use_code
   ,primary_flag
   ,cust_site_uses_status
   ,cust_site_uses_org_id
   ,primary_salesrep_id
   ,attribute6
   ,attribute17
   ,attribute19
)
AS
    SELECT cust_accounts.cust_account_id
          ,cust_accounts.party_id
          ,cust_accounts.account_number
          ,cust_accounts.status cust_accounts_status
          ,cust_accounts.account_name
          ,cust_acct_sites.cust_acct_site_id
          ,cust_acct_sites.status cust_acct_sites_status
          ,cust_acct_sites.org_id
          ,cust_acct_sites.bill_to_flag cust_acct_sites_bill_to_flag
          ,cust_acct_sites.ship_to_flag cust_acct_sites_ship_to_flag
          ,party_site.party_site_name
          ,party_site.party_site_number
          -- ,party_site.party_id
          ,party_site.location_id
          ,party_site.status party_site_status
          ,locations.address1
          ,locations.address2
          ,locations.city
          ,locations.state
          ,locations.postal_code
          ,cust_site_uses.location
          ,cust_site_uses.bill_to_site_use_id
          ,cust_site_uses.site_use_id
          ,cust_site_uses.site_use_code
          ,cust_site_uses.primary_flag
          ,cust_site_uses.status cust_site_uses_status
          ,cust_site_uses.org_id cust_site_uses_org_id
          ,cust_site_uses.primary_salesrep_id
          ,cust_accounts.attribute6
          ,cust_acct_sites.attribute17
          ,cust_acct_sites.attribute19
      FROM apps.hz_cust_accounts cust_accounts 
          ,apps.hz_cust_acct_sites cust_acct_sites  
          ,hz_party_sites party_site
          ,hz_locations locations
          ,apps.hz_cust_site_uses cust_site_uses  
     WHERE     cust_accounts.cust_account_id = cust_acct_sites.cust_account_id
           AND cust_acct_sites.party_site_id = party_site.party_site_id
           AND locations.location_id = party_site.location_id
           AND cust_site_uses.cust_acct_site_id = cust_acct_sites.cust_acct_site_id
/

-- End of DDL Script for View APPS.XXWC_CUSTOMER_SITES_VR
