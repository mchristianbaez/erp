CREATE OR REPLACE FORCE VIEW apps.xxeis_1231490_gtvgas_v
(
   account_number
  ,account_name
  ,name
  ,account_status
  ,attribute5
  ,account_creation_date
  ,party_number
  ,primary_phone_area_code
  ,creation_date
  ,attribute10
)
AS
   SELECT DISTINCT a.account_number
                  ,a.account_name
                  ,d.name
                  ,c.account_status
                  ,a.attribute5
                  ,a.account_established_date account_creation_date
                  ,b.party_number
                  ,b.primary_phone_area_code
                  ,a.creation_date
                  ,a.attribute10
     FROM ar.hz_cust_accounts a
         ,ar.hz_parties b
         ,ar.hz_customer_profiles c
         ,(SELECT DISTINCT collector_id, name FROM ar.ar_collectors) d
         ,ar.hz_cust_acct_sites_all f
    WHERE     1 = 1
          AND a.party_id = b.party_id
          AND a.cust_account_id = c.cust_account_id
          AND a.cust_account_id = f.cust_account_id
          AND c.collector_id = d.collector_id
          AND c.site_use_id IS NULL
          AND f.org_id IN '162';
