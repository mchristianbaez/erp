/* Formatted on 1/9/2014 4:07:48 PM (QP5 v5.115.810.9015) */
CREATE OR REPLACE FORCE VIEW apps.xxwc_po_sel_vendor_vw (
   batch_id
 , vendor_id
 , vendor_number
 , vendor_name
 , vendor_site_id
 , vendor_site_code
 , freight_terms_lookup_code
 , organization_id
 , organization_code
 , vendor_min_dollar
 , freight_min_dollar
 , freight_min_uom
 , line_weight
 , total_amount
  -- 01/08/14 CG: TMS 20131212-00176: Added new Vendor Min fields
 , ppd_freight_units
 , notes
)
AS
	 SELECT   xvm.batch_id
			, xvm.vendor_id
			, xvm.vendor_number
			, xvm.vendor_name
			, xvm.vendor_site_id
			, xvm.vendor_site_code
			, xvm.freight_terms_lookup_code
			, xvm.organization_id
			, xvm.organization_code
			, MIN (vendor_min_dollar) vendor_min_dollar
			, MIN (freight_min_dollar) freight_min_dollar
			, MIN (freight_min_uom) freight_min_uom
			, SUM (xvm.line_weight) line_weight
			, SUM (xvm.total_amount) total_amount
            -- 01/08/14 CG: TMS 20131212-00176: Added new Vendor Min fields
            , SUM (xvm.ppd_freight_units) ppd_freight_units
            , xvm.notes
	   FROM   xxwc_po_sel_vendor_item_vw xvm
   GROUP BY   xvm.batch_id
			, xvm.vendor_id
			, xvm.vendor_number
			, xvm.vendor_name
			, xvm.vendor_site_id
			, xvm.vendor_site_code
			, xvm.freight_terms_lookup_code
			, xvm.organization_id
			, xvm.organization_code
            -- 01/08/14 CG: TMS 20131212-00176: Added new Vendor Min fields
            , xvm.notes;


