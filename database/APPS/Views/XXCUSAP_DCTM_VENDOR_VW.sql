CREATE OR REPLACE FORCE VIEW APPS.XXCUSAP_DCTM_VENDOR_VW
(
   POS_ID,
   POS_VENDOR_REMIT,
   ORACLE_VENDOR_NAME,
   ORACLE_SITE_ID,
   ORACLE_VENDOR_NUM,
   ADDRESS_LINE1,
   ADDRESS_LINE2,
   CITY,
   STATE,
   ZIP,
   ORG_ID,
   PRIMARY_PAY_SITE_FLAG
)
AS
   SELECT 'DCM',
          NULL,
          v.vendor_name,
          s.vendor_site_id,
          v.segment1,
          s.address_line1,
          s.address_line2,
          s.city,
          NVL (s.state, s.province),
          s.zip,
          s.org_id,
          s.primary_pay_site_flag
     FROM ap.ap_suppliers v, ap.ap_supplier_sites_all s
    WHERE     v.vendor_id = s.vendor_id
          --and s.org_id = 163
          AND NVL (v.vendor_type_lookup_code, 'X') <> 'EMPLOYEE'
          AND v.enabled_flag = 'Y'
          AND s.pay_site_flag = 'Y'
          AND NVL (v.end_date_active, SYSDATE) >= SYSDATE
          AND NVL (s.inactive_date, SYSDATE) >= SYSDATE;

COMMENT ON TABLE APPS.XXCUSAP_DCTM_VENDOR_VW IS 'S276313, Note: add column primary_pay_site_flag';



GRANT SELECT ON APPS.XXCUSAP_DCTM_VENDOR_VW TO INTERFACE_APEXHDSORACLE;

GRANT SELECT ON APPS.XXCUSAP_DCTM_VENDOR_VW TO INTERFACE_BIZTALK;

GRANT SELECT ON APPS.XXCUSAP_DCTM_VENDOR_VW TO INTERFACE_ECM;
