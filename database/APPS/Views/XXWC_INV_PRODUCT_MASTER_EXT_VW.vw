DROP VIEW APPS.XXWC_INV_PRODUCT_MASTER_EXT_VW;

/* Formatted on 2/2/2015 10:08:38 AM (QP5 v5.254.13202.43190) */
CREATE OR REPLACE FORCE VIEW APPS.XXWC_INV_PRODUCT_MASTER_EXT_VW
(
   OPERATING_UNIT_ID,
   INTERFACE_DATE,
   PROD_MASTER_DATA
)
AS
   SELECT operating_unit_id,
          interface_date,
          (   business_unit
           || '|'
           || source_system
           || '|'
           || sku_number
           || '|'
           || sku_short_desc 
           || '|'
           || sku_other_desc
           || '|'
           || sku_long_desc Commented by Raghavendra for TMS # 20141203-00170
           || '|'
           || TO_CHAR (sku_start_date, 'MM/DD/YYYY')
           || '|'
           || manufacturer_name
           || '|'
           || vendor_part_number
           || '|'
           || primary_upc
           || '|'
           || secondaryupc
           || '|'
           || tertiaryupc
           || '|'
           -- 08/02/12 CG: Nulled out per email from Pedro and moved lvl2 to lvl4
           || NULL                                        --prod_hie_lvl_1_val
           || '|'
           || NULL                                        --prod_hie_lvl_2_val
           || '|'
           || NULL                                        --prod_hie_lvl_3_val
           || '|'
           || prod_hie_lvl_2_val                          --prod_hie_lvl_4_val
           || '|'
           || unspsc_code
           || '|'
           || po_replacementcost
           || '|'
           || eff_date_po_repl_cost
           || '|'
           || standard_cost
           || '|'
           || standard_cost_eff_date
           || '|'
           || trade_price
           || '|'
           || eff_date_trade_price
           || '|'
           || purch_uom
           || '|'
           || purch_uom_qty
           || '|'
           || conv_fact_purch_rec_uom
           || '|'
           || pricing_uom
           || '|'
           || pricing_uom_qty
           || '|'
           || stocking_uom
           || '|'
           || stocking_uom_qty
           || '|'
           || units_per_pkg
           || '|'
           || unit_per_pkg_uom
           || '|'
           || case_qty
           || '|'
           || pallet_qty
           || '|'
           || unit_length
           || '|'
           || unit_height
           || '|'
           || unit_width
           || '|'
           || dim_measurement_type
           || '|'
           || single_weight_buy_pkg
           || '|'
           || weight_measurement_type
           || '|'
           || cube_buy_pkg
           || '|'
           || cube_measurement_type
           || '|'
           || import_flag
           || '|'
           || country_of_origin
           || '|'
           || commodity_flag
           || '|'
           || primary_comodity
           || '|'
           || secondary_commodity
           || '|'
           || item_finish
           || '|'
           || import_duty
           || '|'
           || min_order_qty
           || '|'
           || min_order_qty_uom
           || '|'
           || inner_pack_sku
           || '|'
           || gl_code
           || '|'
           || gl_code_desc
           || '|'
           || nmfc_code
           || '|'
           || pallet_type
           || '|'
           || cust_prod_attr_label1
           || '|'
           || cust_prod_attr_value1
           || '|'
           || cust_prod_attr_label2
           || '|'
           || cust_prod_attr_value2
           || '|'
           || cust_prod_attr_label3
           || '|'
           || cust_prod_attr_value3
           || '|'
           || cust_prod_attr_label4
           || '|'
           || cust_prod_attr_value4
           || '|'
           || cust_prod_attr_label5
           || '|'
           || cust_prod_attr_value5
           || '|'
           || cust_prod_attr_label6
           || '|'
           || cust_prod_attr_value6
           || '|'
           || cust_prod_attr_label7
           || '|'
           || cust_prod_attr_value7
           || '|'
           || cust_prod_attr_label8
           || '|'
           || cust_prod_attr_value8
           || '|'
           || cust_prod_attr_label9
           || '|'
           || cust_prod_attr_value9
           || '|'
           || cust_prod_attr_label10
           || '|'
           || cust_prod_attr_value10
           || '|'
           || price_matrix
           || '|'
           || hazardous_material
           || '|'
           || hazard_class)
             prod_master_data
     FROM xxwc_inv_product_master_vw;
