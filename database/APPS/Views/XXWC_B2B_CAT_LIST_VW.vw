CREATE OR REPLACE VIEW APPS.XXWC_B2B_CAT_LIST_VW AS
/*************************************************************************
 Copyright (c) 2012 HD Supply
 All rights reserved.
**************************************************************************
   $Header XXWC_B2B_CAT_LIST_VW$
  Module Name: XXWC_B2B_CAT_LIST_VW

  PURPOSE:

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)   TMS TASK        DESCRIPTION
  -- ------- -----------   ------------ ---------      -------------------------
  -- 1.0     26-Mar-2016   Pahwa, Nancy                Initially Created 
TMS# 20160223-00029   
**************************************************************************/
SELECT B.SEGMENT1 ITEM_NUMBER, A.SEGMENT1 CATALOG , a.item_catalog_group_id
FROM MTL_ITEM_CATALOG_GROUPS_B_KFV A, APPS.MTL_SYSTEM_ITEMS B
WHERE A.ITEM_CATALOG_GROUP_ID = B.ITEM_CATALOG_GROUP_ID
AND B.ORGANIZATION_ID=222
AND B.INVENTORY_ITEM_STATUS_CODE = 'Active';
/
