--
-- XXWC_QP_PRICE_LIST_CONV_VW  (View)
--

CREATE OR REPLACE FORCE VIEW APPS.XXWC_QP_PRICE_LIST_CONV_VW
(
   LIST_HEADER_ID
  ,NAME
  ,OPERAND
  ,PRODUCT_ATTR_VALUE
  ,PRODUCT_ATTRIBUTE
  ,PRODUCT_UOM_CODE
  ,LIST_LINE_ID
  ,START_DATE_ACTIVE
  ,END_DATE_ACTIVE
  ,GENERATE_USING_FORMULA_ID
  ,PRICE_BY_FORMULA_ID
  ,ATTRIBUTE1
  ,PRECEDENCE
)
AS
   SELECT qlh.list_header_id
         ,qlh.name
         ,qll.operand
         ,xxwc_price_list_web_adi_pkg.Get_product_attr_value (
             qll.product_attribute
            ,qll.product_attr_value)
             product_attr_value
         ,DECODE (qll.product_attribute
                 ,'PRICING_ATTRIBUTE1', 'ITEM'
                 ,'PRICING_ATTRIBUTE2', 'ITEM_CATEGORY'
                 ,'ALL')
             product_attribute
         ,uom.unit_of_measure product_uom_code
         ,qll.list_line_id
         ,qll.start_date_active
         ,qll.end_date_active
         ,xxwc_price_list_web_adi_pkg.Get_Formula_name (
             qll.generate_using_formula_id)
             generate_using_formula_id
         ,xxwc_price_list_web_adi_pkg.Get_Formula_name (
             qll.price_by_formula_id)
             price_by_formula_id
         ,qll.attribute1
         ,qll.Product_Precedence
     FROM qp_list_headers qlh
         ,qp_list_lines_v qll
         ,mtl_units_of_measure_vl uom
    WHERE     qlh.list_type_code = 'PRL'
          AND qlh.list_header_id = qll.list_header_id(+)
          AND qll.product_uom_code = uom.uom_code(+)
          AND NVL(qll.END_DATE_ACTIVE, TRUNC(SYSDATE))>=TRUNC(SYSDATE);--TMS #20130304-00477 --added by Ram Talluri 3/4/2013


