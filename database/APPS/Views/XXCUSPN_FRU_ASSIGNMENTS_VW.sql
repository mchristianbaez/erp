
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUSPN_FRU_ASSIGNMENTS_VW" ("LOCATION_CODE", "STANDARD_TYPE_LOOKUP_CODE", "LOC1", "BLD_LOC", "FLR_LOC", "SEC_LOC", "ADDRESS_ID", "FRU", "EMP_ASSIGN_START_DATE", "EMP_ASSIGN_END_DATE", "COST_CENTER_CODE", "ALLOCATED_AREA_PCT", "ALLOCATED_AREA", "UOM_CODE", "UTILIZED_AREA", "EMP_SPACE_COMMENTS", "ORG_ID") AS 
  (SELECT DISTINCT sec.location_code ,sec.standard_type_lookup_code,bld.location_code loc1, bld.location_id bld_loc, flr.location_id flr_loc, sec.location_id sec_loc, ADDr.Address_Id, spaced.attribute1 FRU,
                spaced.emp_assign_start_date, spaced.emp_assign_end_date, spaced.cost_center_code, spaced.allocated_area_pct, spaced.allocated_area,
                spaced.uom_code, spaced.utilized_area, spaced.emp_space_comments, spaced.org_id
FROM  pn.pn_locations_all bld,
      pn.pn_locations_all flr,
      pn.pn_locations_all sec,
      pn.pn_addresses_all addr,
      pn.pn_space_assign_emp_all spaced
WHERE sec.parent_location_id = flr.location_id
AND   flr.parent_location_id = bld.location_id
AND   bld.address_id = addr.Address_Id(+)
AND   sec.location_id = spaced.Location_Id(+)
AND   ((nvl(sec.standard_type_lookup_code,'x') ='PPL') AND (nvl(SPACEd.Emp_Assign_End_Date,SYSDATE+1) >=sysdate))
AND   bld.active_end_date = (SELECT MAX(active_end_date) FROM pn.pn_locations_all WHERE location_id =bld.location_id)
AND   flr.active_end_date = (SELECT MAX(active_end_date) FROM pn.pn_locations_all WHERE location_id =flr.location_id)
AND   sec.active_end_date = (SELECT MAX(active_end_date) FROM pn.pn_locations_all WHERE location_id =sec.location_id)
AND   NVL(spaced.Emp_Assign_End_Date,'31-DEC-4712') = (SELECT MAX(NVL(emp_assign_end_date,'31-DEC-4712'))
                                     FROM pn.pn_space_assign_emp_all
                                     WHERE location_id= spaced.Location_Id
                                     AND   attribute1 = spaced.attribute1
                                     GROUP BY location_id, attribute1)
AND   NVL(spaced.Emp_Assign_start_Date,'31-DEC-4712') = (SELECT MAX(NVL(emp_assign_start_date,'31-DEC-4712'))
                                     FROM pn.pn_space_assign_emp_all
                                     WHERE location_id= spaced.Location_Id
                                     AND   attribute1 = spaced.attribute1
                                     GROUP BY location_id, attribute1)


UNION ALL

SELECT DISTINCT sec.location_code ,sec.standard_type_lookup_code,bld.location_code loc1, bld.location_id bld_loc, flr.location_id flr_loc, sec.location_id sec_loc, ADDr.Address_Id, spaced.attribute1 FRU,
                spaced.emp_assign_start_date, spaced.emp_assign_end_date, spaced.cost_center_code, spaced.allocated_area_pct, spaced.allocated_area,
                spaced.uom_code, spaced.utilized_area, spaced.emp_space_comments, spaced.org_id
FROM  pn.pn_locations_all bld,
      pn.pn_locations_all flr,
      pn.pn_locations_all sec,
      pn.pn_addresses_all addr,
      pn.pn_space_assign_emp_all spaced
WHERE sec.parent_location_id = flr.location_id
AND   flr.parent_location_id = bld.location_id
AND   bld.address_id = addr.Address_Id(+)
AND   sec.location_id = spaced.Location_Id(+)
and   nvl(sec.standard_type_lookup_code,'x') !='PPL'
AND   bld.active_end_date = (SELECT MAX(active_end_date) FROM pn.pn_locations_all WHERE location_id =bld.location_id)
AND   flr.active_end_date = (SELECT MAX(active_end_date) FROM pn.pn_locations_all WHERE location_id =flr.location_id)
AND   sec.active_end_date = (SELECT MAX(active_end_date) FROM pn.pn_locations_all WHERE location_id =sec.location_id)
AND   NVL(spaced.Emp_Assign_End_Date,'31-DEC-4712') = (SELECT MAX(NVL(emp_assign_end_date,'31-DEC-4712'))
                                     FROM pn.pn_space_assign_emp_all
                                     WHERE location_id= spaced.Location_Id
                                     AND   attribute1 = spaced.attribute1
                                     GROUP BY location_id, attribute1)
AND   NVL(spaced.Emp_Assign_start_Date,'31-DEC-4712') = (SELECT MAX(NVL(emp_assign_start_date,'31-DEC-4712'))
                                     FROM pn.pn_space_assign_emp_all
                                     WHERE location_id= spaced.Location_Id
                                     AND   attribute1 = spaced.attribute1
                                     GROUP BY location_id, attribute1))
;
