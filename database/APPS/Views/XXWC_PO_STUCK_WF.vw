/**************************************************************************
   $Header XXWC_PO_STUCK_WF_VW$
   Module Name: XXWC_PO_STUCK_WF_VW.vw

   PURPOSE:   This is a view for PO stuck without notification workflow .

   REVISIONS:
   Ver        Date        Author             Description
   ---------  ----------  ---------------   -------------------------
    1.0       02/01/2017  Pattabhi Avula     Initial creation(TMS#20160823-00016)
    
/*************************************************************************/
CREATE OR REPLACE VIEW apps.xxwc_po_stuck_wf_vw AS
SELECT segment1,
       'Requisition'  Document_type
  FROM po_requisition_headers_all
 WHERE org_id=162 
   AND authorization_status IN ('PRE-APPROVED','IN PROCESS') 
UNION ALL
SELECT segment1,
       'Purchase Order' Document_type
  FROM po_headers_all 
 WHERE org_id=162 
   AND authorization_status IN ('PRE-APPROVED','IN PROCESS')
/