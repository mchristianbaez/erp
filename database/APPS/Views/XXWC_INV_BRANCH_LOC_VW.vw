CREATE OR REPLACE FORCE VIEW APPS.XXWC_INV_BRANCH_LOC_VW
(
/******************************************************************************
   NAME:       APPS.XXWC_INV_BRANCH_LOC_VW
   PURPOSE:  This view is used to extract all the inventory Location Details

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        22/06/2012  Consuelo        Initial Version
   2.0        04/04/2015  Raghavendra S   TMS# 20150303-00075 - FIN - Addition of Code Snippet for Line Breaks in All EDW Extract Programs
                                            and Views for all free entry text fields.
******************************************************************************/
   BUSINESS_UNIT,
   SOURCE_SYSTEM,
   BRANCH_ID,
   FRU_NUMBER,
   LOCATION_NAME,
   ALTERNATE_NAME,
   PHYSADDRLINE1,
   PHYSADDRLINE2,
   PHYSADDRLINE3,
   PHYSADDRCITY,
   PHYSADDRSTATE,
   PHYSADDRZIP,
   PHYSADDRCOUNTRYCODE,
   MAILADDR1,
   MAILADDR2,
   MAILADDR3,
   MAILADDRCITY,
   MAILADDRSTATE,
   MAILADDRZIP,
   MAILADDRCOUNTRYCODE,
   REGIONALMGRNAME,
   REGION,
   PRICINGREGIONNAME,
   ACQUIREDFLAG,
   OPENFLAG,
   PHONENUMBER,
   FAXNUMBER,
   CUSTOMERSERVICEPHONE,
   HDSOPENDATE,
   HDSCLOSEDDATE,
   DATEMODIFIED,
   PREVIOUSNAME,
   BRANCHOPERATIONSMANAGER,
   BRANCHSALESMANAGER,
   LOCATIONTYPE,
   OPERATING_UNIT_ID,
   OPERATING_UNIT_NAME,
   EBS_ORG_CODE,
   EBS_ORG_ID
)
AS
     SELECT DISTINCT
            'WHITECAP' business_unit,
            'Oracle EBS' source_system, -- 05/22/2012 CG: Changed to Org Code Per 05/15 Email
            -- mp.attribute11 branch_id,
            mp.organization_code branch_id,
            -- mp.attribute10  fru_number,-- commented for V 2.0
            REGEXP_REPLACE(mp.attribute10,  '[[:cntrl:]]', ' ') fru_number, -- 05/22/2012 CG Substring per 05/15 Email -- Added for V 2.0
            -- hou.location_code location_name,
            REGEXP_REPLACE(TRIM (SUBSTR (hou.location_code,
                            INSTR (hou.location_code,
                                   '-',
                                   1,
                                   1)
                          + 1)),  '[[:cntrl:]]', ' ')
               location_name, -- Added for V 2.0
            NULL alternate_name,
            -- hou.address_line_1 physaddrline1, -- Commented for V 2.0
            -- hou.address_line_2 physaddrline2, -- Commented for V 2.0
            -- hou.address_line_3 physaddrline3, -- Commented for V 2.0
            REGEXP_REPLACE(hou.address_line_1,  '[[:cntrl:]]', ' ') physaddrline1, -- Added for V 2.0
            REGEXP_REPLACE(hou.address_line_2,  '[[:cntrl:]]', ' ') physaddrline2, -- Added for V 2.0
            REGEXP_REPLACE(hou.address_line_3,  '[[:cntrl:]]', ' ') physaddrline3, -- Added for V 2.0
            hou.town_or_city physaddrcity,
            hou.region_2 physaddrstate,
            hou.postal_code physaddrzip,
            hou.country physaddrcountrycode,
            NULL mailaddr1,
            NULL mailaddr2,
            NULL mailaddr3,
            NULL mailaddrcity,
            NULL mailaddrstate,
            NULL mailaddrzip,
            NULL mailaddrcountrycode,
            NULL regionalmgrname,
            REGEXP_REPLACE(mp.attribute9,  '[[:cntrl:]]', ' ') region, -- Added for V 2.0 
            REGEXP_REPLACE(mp.attribute6,  '[[:cntrl:]]', ' ') pricingregionname, -- Added for V 2.0 
            NULL acquiredflag,
            (CASE
                WHEN TRUNC (hou.date_to) < TRUNC (SYSDATE) THEN 'N'
                ELSE 'Y'
             END)
               openflag,
            hou.telephone_number_1 phonenumber,
            hou.telephone_number_2 faxnumber,
            NULL customerservicephone,
            NULL hdsopendate,
            NULL hdscloseddate,
            hou.last_update_date datemodified,
            NULL previousname,
            REGEXP_REPLACE(NVL (
               (SELECT papf.full_name
                  FROM per_all_people_f papf
                 WHERE     TO_NUMBER (mp.attribute13) = papf.person_id
                       AND TRUNC (papf.effective_start_date) <= TRUNC (SYSDATE)
                       AND NVL (TRUNC (papf.effective_end_date),
                                TRUNC (SYSDATE)) >= TRUNC (SYSDATE)),
               'UNDEFINED'),  '[[:cntrl:]]', ' ')
               branchoperationsmanager, --TMS#0203-00189 Added by harsha for Change Branch Location EDW extract to address Missing Branch Issue
            --NVL (papf.full_name, 'UNKNOWN') branchoperationsmanager, --TMS#0203-00189 commented by harsha for Change Branch Location EDW extract to address Missing Branch Issue
            NULL branchsalesmanager,
            REGEXP_REPLACE(mp.attribute7,  '[[:cntrl:]]', ' ') locationtype, -- Added for V 2.0 
            DECODE (hoi.org_information_context,
                    'Accounting Information', TO_NUMBER (hoi.org_information3),
                    TO_NUMBER (NULL))
               operating_unit_id,
            ou.name operating_unit_name,
            mp.organization_code ebs_org_code,
            mp.organization_id ebs_org_id
       FROM inv.mtl_parameters mp,
            apps.hr_organization_units_v hou,
            hr_organization_information hoi,
            hr_operating_units ou                                          --,
      -- hr.per_all_people_f papf
      WHERE     mp.organization_id = hou.organization_id
            AND hou.organization_id = hoi.organization_id
            AND (hoi.org_information_context || '') = 'Accounting Information'
            AND DECODE (
                   hoi.org_information_context,
                   'Accounting Information', TO_NUMBER (hoi.org_information3),
                   TO_NUMBER (NULL)) = ou.organization_id(+)
   --  AND NVL (TO_NUMBER (mp.attribute13), papf.person_id) =           --TMS#0203-00189 commented by harsha for Change Branch Location EDW extract to address Missing Branch Issue
   --      papf.person_id
   -- 11/05/2012 CG
   --  AND TRUNC (papf.effective_start_date) <= TRUNC (SYSDATE)         --TMS#0203-00189 commented by harsha for Change Branch Location EDW extract to address Missing Branch Issue
   --  AND NVL (TRUNC (papf.effective_end_date), TRUNC (SYSDATE)) >=    --TMS#0203-00189 commented by harsha for Change Branch Location EDW extract to address Missing Branch Issue
   --        TRUNC (SYSDATE)
   ORDER BY mp.organization_code;
