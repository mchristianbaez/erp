   /* ************************************************************************
    HD Supply
    All rights reserved.
    Ticket                                         Ver          Date                 Author                                 Description
    ----------------------                  ----------   ----------          ------------------------           -------------------------
    ESMS 257200                         1.0                                                                                   Missing header 
    TMS 20151217-00142        1.1         11/19/2015   Balaguru Seshadri            EDIT SQL query to use a custom table instead of hz tables
    Child: TMS 20151217-00142 , Parent: TMS  20151008-00085
   ************************************************************************ */ 
CREATE OR REPLACE FORCE VIEW APPS.XXCUSOZF_RBT_LOB_V
(
   LOB_PARTY_ID,
   LOB_PARTY_NAME
)
AS
--  BEGIN Ver 1.1
select  party_id, party_name 
from    xxcus.xxcus_rebate_customers
where 1 =1
      and party_attribute1 ='HDS_LOB'
group by party_id, party_name;
--  END Ver 1.1
/* --Ver 1.1
   SELECT PARTY_ID LOB_PARTY_ID, PARTY_NAME LOB_PARTY_NAME
     FROM HZ_PARTIES
    WHERE 1 = 1 AND ATTRIBUTE1 = 'HDS_LOB';
    -- RECORD GROUP: REBATE_LOB_RG, QUERY: SELECT LOB_PARTY_ID       ,LOB_PARTY_NAME FROM XXCUSOZF_RBT_LOB_V WHERE 1 =1 ORDER BY LOB_PARTY_NAME ASC
*/    --Ver 1.1
--
COMMENT ON TABLE APPS.XXCUSOZF_RBT_LOB_V IS 'ESMS 301618 / TMS 20151008-00085';
 