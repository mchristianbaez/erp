DROP VIEW APPS.XXWC_BRIG_INTER_ORG_XFER_VW;

CREATE OR REPLACE FORCE VIEW APPS.XXWC_BRIG_INTER_ORG_XFER_VW
/*************************************************************************
   *   $Header XXWC_BRIG_INTER_ORG_XFER_VW
   *   Module Name: XXWC_BRIG_INTER_ORG_XFER_VW
   *
   *   PURPOSE:   View used for Inter Org Transfer of Brigade Items
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        02/10/2015  Gopi Damuluri            TMS# TMS# 20150210-00099 Changed
   *                                                   Select statemet value to null as
   *                                                   per TMS request
   * ***************************************************************************/
(
   ITEM_NUMBER,
   SOURCE_ORGANIZATION,
   DESTINATION_ORGANIZATION,
   TRANSACTION_QUANTITY,
   SUBINVENTORY_CODE,
   ORGANIZATION_ID,
   INVENTORY_ITEM_ID,
   LOCATOR,
   ORIG_DATE_RECEIVED
)
AS
   SELECT NULL item_number,
          NULL source_organization,
          NULL destination_organization,
          NULL transaction_quantity,
          NULL subinventory_code,
          NULL organization_id,
          NULL inventory_item_id,
          NULL locator,
          NULL orig_date_received
     FROM DUAL;
