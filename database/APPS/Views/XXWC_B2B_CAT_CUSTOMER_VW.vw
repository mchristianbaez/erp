create or replace view apps.xxwc_b2b_cat_customer_vw
(account_name, cust_account_id)
as
/*************************************************************************
 Copyright (c) 2012 HD Supply
 All rights reserved.
**************************************************************************
   $Header xxwc_b2b_cat_customer_vw$
  Module Name: xxwc_b2b_cat_customer_vw

  PURPOSE:

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)   TMS TASK        DESCRIPTION
  -- ------- -----------   ------------ ---------      -------------------------
  -- 1.0     26-Mar-2016   Pahwa, Nancy                Initially Created 
TMS# 20160223-00029   
**************************************************************************/
select PARTY_NAME, PARTY_ID
    FROM hz_parties hcaa
where hcaa.party_type='ORGANIZATION'
and hcaa.status = 'A';
/
