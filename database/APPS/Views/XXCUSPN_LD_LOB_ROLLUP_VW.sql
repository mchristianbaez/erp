
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUSPN_LD_LOB_ROLLUP_VW" ("FPS_ID", "FPS_DESC", "BU_ID", "BU_DESC") AS 
  SELECT fvv.flex_value        fps_id
      ,fvv.description       fps_desc
      ,fvv.attribute3        bu_id
      ,fvv.attribute4        bu_desc
  FROM fnd_flex_values_vl fvv
where fvv.flex_value_set_id = 1014547
--and fvv.COMPILED_VALUE_ATTRIBUTES not like 'N%N'
--and fvv.FLEX_VALUE not in ('T','00')

;
