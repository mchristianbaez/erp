/* Formatted on 11/20/2014 8:05:23 AM (QP5 v5.254.13202.43190) */
CREATE OR REPLACE FORCE VIEW APPS.XXWC_BPA_LIST_V
/* Added Multi org changes for TMS Task ID: Task ID: 20141001-00164  -WC Canada: MultiOrg: Week of Sep 29 (Veeran) */
(
   VENDOR_ID,
   VENDOR_INFO,
   VENDOR_SITE_ID
)
AS
   SELECT DISTINCT
          a.vendor_id,
          b.vendor_name || '-' || c.vendor_site_code,
          a.vendor_site_id
     --View used in Price Break BPA upload WEBADI parameter
     FROM apps.po_headers_all a, ap_suppliers b, apps.ap_supplier_sites_all c
    WHERE     a.vendor_id = b.vendor_id
          AND a.vendor_site_id = c.vendor_site_id
          AND a.type_lookup_code = 'BLANKET'
          AND a.global_agreement_flag = 'Y'
          AND NVL (a.closed_code, 'X') <> 'CLOSED'
          --AND a.org_id=162  --01/10/2014 Commented by Veera As per Canada OU Test
          AND a.org_id = FND_PROFILE.VALUE ('ORG_ID') -- Added by Raghavendra on 19-Nov-2014As per Canada WED ADI Test
          AND a.org_id = c.org_id -- Added by Raghavendra on 19-Nov-2014As per Canada WED ADI Test
          AND NVL (a.cancel_flag, 'N') = 'N'
          AND SYSDATE BETWEEN NVL (a.start_date, SYSDATE - 1)
                          AND NVL (a.end_date, SYSDATE + 1);
