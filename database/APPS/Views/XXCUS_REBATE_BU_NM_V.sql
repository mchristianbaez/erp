
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUS_REBATE_BU_NM_V" ("BU_NM", "ORACLE_BU_NM", "FILE_DELIVERY", "LOB") AS 
  SELECT meaning, description, attribute5, tag
    FROM apps.fnd_lookup_values
   WHERE lookup_type = 'XXCUS_REBATE_BU_XREF'
     AND enabled_flag = 'Y'
     AND nvl(end_date_active, SYSDATE) >= SYSDATE
;
