--
-- XXWCPO_DCTM_PURCHASING_VW  (View)
--

CREATE OR REPLACE FORCE VIEW APPS.XXWCPO_DCTM_PURCHASING_VW
(
   PO_NUMBER
  ,PO_DATE
  ,ORDER_TYPE
  ,VENDOR_NUMBER
  ,SHIP_VIA
  ,FOB
  ,SHIP_TO_NAME
  ,SHIP_TO_ADDR1
  ,SHIP_TO_ADDR2
  ,SHIP_TO_CITY
  ,SHIP_TO_STATE
  ,SHIP_TO_ZIP
  ,NOTE
  ,TERMS
  ,REFERENCE
  ,QUANTITY
  ,TAX_TOTAL
  ,PO_TOTAL
  ,SPECIAL_INSTRUCTIONS
  ,RECEIVER_NUMBER
  ,PO_STATUS
  ,PO_FREIGHT
)
AS
   SELECT p.segment1 po_number
         ,TRUNC (approved_date) po_date
         ,type_lookup_code order_type
         ,s.segment1 vendor_number
         ,p.ship_via_lookup_code ship_via
         ,p.fob_lookup_code fob
         ,l.location_code ship_to_name
         ,l.address_line_1 ship_to_addr1
         ,l.address_line_2 ship_to_addr2
         ,l.town_or_city ship_to_city
         ,l.region_2 ship_to_state
         ,l.postal_code ship_to_zip
         ,p.comments note
         ,t.name terms
         ,NULL reference
         ,NULL quantity
         ,0 tax_total
         , (SELECT SUM (unit_price * quantity)
              FROM po.po_lines_all
             WHERE po_header_id = p.po_header_id)
             po_total
         ,NULL special_instructions
         ,NULL receiver_number
         ,p.closed_code po_status
         ,0 po_freight
     FROM po.po_headers_all p
         ,ap.ap_suppliers s
         ,hr.hr_locations_all l
         ,ap.ap_terms_tl t
    WHERE     p.org_id = 162
          AND approved_flag = 'Y'
          AND p.vendor_id = s.vendor_id
          AND p.ship_to_location_id = l.location_id
          AND p.terms_id = t.term_id;


