--
-- XXWC_AR_ADDRESSES_V  (View)
--

CREATE OR REPLACE FORCE VIEW apps.xxwc_ar_addresses_v
(
   row_id
  ,address_id
  ,customer_id
  ,last_update_date
  ,object_version
  ,last_updated_by
  ,creation_date
  ,created_by
  ,status
  ,orig_system_reference
  ,country
  ,territory_short_name
  ,address_style
  ,address1
  ,address2
  ,address3
  ,address4
  ,city
  ,county
  ,state
  ,province
  ,postal_code
  ,concatenated_address
  ,last_update_login
  ,territory_id
  ,address_key
  ,su_bill_to_flag
  ,su_bill_to_site_use_id
  ,su_ship_to_flag
  ,ship_to_site_use_id
  ,su_market_flag
  ,market_site_uses_id
  ,su_dun_flag
  ,dun_site_use_id
  ,su_stmt_flag
  ,stmt_site_use_id
  ,su_legal_flag
  ,legal_site_use_id
  ,key_account_flag
  ,language
  ,language_description
  ,attribute_category
  ,attribute1
  ,attribute2
  ,attribute3
  ,attribute4
  ,attribute5
  ,attribute6
  ,attribute7
  ,attribute8
  ,attribute9
  ,attribute10
  ,attribute11
  ,attribute12
  ,attribute13
  ,attribute14
  ,attribute15
  ,address_lines_phonetic
  ,customer_category
  ,customer_category_meaning
  ,edi_location
  ,global_attribute_category
  ,global_attribute1
  ,global_attribute2
  ,global_attribute3
  ,global_attribute4
  ,global_attribute5
  ,global_attribute6
  ,global_attribute7
  ,global_attribute8
  ,global_attribute9
  ,global_attribute10
  ,global_attribute11
  ,global_attribute12
  ,global_attribute13
  ,global_attribute14
  ,global_attribute15
  ,global_attribute16
  ,global_attribute17
  ,global_attribute18
  ,global_attribute19
  ,global_attribute20
  ,territory
  ,translated_customer_name
  ,sales_tax_geocode
  ,sales_tax_inside_city_limits
  ,party_site_id
  ,party_id
  ,party_location_id
  ,site_number
  ,identifying_address_flag
  ,attribute16
  ,attribute17
  ,attribute18
  ,attribute19
  ,attribute20
  ,party_site_last_update_date
  ,party_site_object_version
  ,loc_last_update_date
  ,loc_object_version
  ,addressee
  ,description
  ,short_description
  ,FLOOR
  ,house_number
  ,location_directions
  ,postal_plus4_code
  ,po_box_number
  ,street
  ,street_number
  ,street_suffix
  ,suite
  ,org_id
)
AS
   SELECT    /* PURPOSE: THE VIEW ARH_ADDRESSE_V IS FOR USE AS A BASE TABLE */
          /* FOR ORACLE FORMS 4 PROGRAMS */
          addr.ROWID row_id
         ,addr.cust_acct_site_id address_id
         ,addr.cust_account_id customer_id
         ,addr.last_update_date last_update_date
         ,addr.object_version_number object_version_number
         ,addr.last_updated_by last_updated_by
         ,addr.creation_date creation_date
         ,addr.created_by created_by
         ,addr.status status
         ,addr.orig_system_reference orig_system_reference
         ,loc.country country
         ,terr.territory_short_name territory_short_name
         ,terr.address_style address_style
         ,loc.address1 address1
         ,loc.address2 address2
         ,loc.address3 address3
         ,loc.address4 address4
         ,loc.city city
         ,loc.county county
         ,loc.state state
         ,loc.province province
         ,loc.postal_code postal_code
         ,arp_addr_pkg.format_address (loc.address_style
                                      ,loc.address1
                                      ,loc.address2
                                      ,loc.address3
                                      ,loc.address4
                                      ,loc.city
                                      ,loc.county
                                      ,loc.state
                                      ,loc.province
                                      ,loc.postal_code
                                      ,terr.territory_short_name)
             concatenated_address
         ,addr.last_update_login last_update_login
         ,addr.territory_id territory_id
         ,loc.address_key
         ,'N'
         ,0
         ,'N'
         ,0
         ,'N'
         ,0
         ,'N'
         ,0
         ,'N'
         ,0
         ,'N'
         ,0
         ,addr.key_account_flag key_account_flag
         ,loc.language language
         ,lang.description language_description
         ,addr.attribute_category attribute_category
         ,addr.attribute1 attribute1
         ,addr.attribute2 attribute2
         ,addr.attribute3 attribute3
         ,addr.attribute4 attribute4
         ,addr.attribute5 attribute5
         ,addr.attribute6 attribute6
         ,addr.attribute7 attribute7
         ,addr.attribute8 attribute8
         ,addr.attribute9 attribute9
         ,addr.attribute10 attribute10
         ,addr.attribute11 attribute11
         ,addr.attribute12 attribute12
         ,addr.attribute13 attribute13
         ,addr.attribute14 attribute14
         ,addr.attribute15 attribute15
         ,loc.address_lines_phonetic address_lines_phonetic
         ,addr.customer_category_code customer_category
         ,l_cat.meaning customer_category_meaning
         ,addr.ece_tp_location_code edi_location
         ,addr.global_attribute_category global_attribute_category
         ,addr.global_attribute1 global_attribute1
         ,addr.global_attribute2 global_attribute2
         ,addr.global_attribute3 global_attribute3
         ,addr.global_attribute4 global_attribute4
         ,addr.global_attribute5 global_attribute5
         ,addr.global_attribute6 global_attribute6
         ,addr.global_attribute7 global_attribute7
         ,addr.global_attribute8 global_attribute8
         ,addr.global_attribute9 global_attribute9
         ,addr.global_attribute10 global_attribute10
         ,addr.global_attribute11 global_attribute11
         ,addr.global_attribute12 global_attribute12
         ,addr.global_attribute13 global_attribute13
         ,addr.global_attribute14 global_attribute14
         ,addr.global_attribute15 global_attribute15
         ,addr.global_attribute16 global_attribute16
         ,addr.global_attribute17 global_attribute17
         ,addr.global_attribute18 global_attribute18
         ,addr.global_attribute19 global_attribute19
         ,addr.global_attribute20 global_attribute20
         ,addr.territory territory
         ,addr.translated_customer_name translated_customer_name
         ,loc.sales_tax_geocode sales_tax_geocode
         ,loc.sales_tax_inside_city_limits sales_tax_inside_city_limits /* ,ADDR.CUST_ACCT_SITE_ID */
         ,party_site.party_site_id
         ,party_site.party_id
         ,loc.location_id
         ,party_site.party_site_number
         ,party_site.identifying_address_flag
         ,addr.attribute16
         ,addr.attribute17
         ,addr.attribute18
         ,addr.attribute19
         ,addr.attribute20
         ,party_site.last_update_date
         ,party_site.object_version_number
         ,loc.last_update_date
         ,loc.object_version_number
         ,party_site.addressee
         ,loc.description
         ,loc.short_description
         ,loc.FLOOR
         ,loc.house_number
         ,loc.location_directions
         ,loc.postal_plus4_code
         ,loc.po_box_number
         ,loc.street
         ,loc.street_number
         ,loc.street_suffix
         ,loc.suite
         ,addr.org_id
     FROM ar_lookups l_cat
         ,fnd_territories_vl terr
         ,fnd_languages_vl lang
         ,hz_cust_acct_sites addr
         ,hz_party_sites party_site
         ,hz_locations loc
    --, HZ_LOC_ASSIGNMENTS LOC_ASSIGN
    WHERE     addr.customer_category_code = l_cat.lookup_code(+)
          AND l_cat.lookup_type(+) = 'ADDRESS_CATEGORY'
          AND loc.country = terr.territory_code(+)
          AND loc.language = lang.language_code(+)
          AND addr.party_site_id = party_site.party_site_id
          AND loc.location_id = party_site.location_id;


