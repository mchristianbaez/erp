/* Formatted on 8/16/2013 5:16:18 PM (QP5 v5.206) */
-- Start of DDL Script for View APPS.XXWC_INV_ONHAND_QTY_EXT_VW
-- Generated 8/16/2013 5:16:12 PM from APPS@EBIZDEV

CREATE OR REPLACE VIEW apps.xxwc_inv_onhand_qty_ext_vw
(
    operating_unit_id
   ,interface_date
   ,inventory_data
)
AS
    SELECT operating_unit_id
          ,TRUNC (SYSDATE) interface_date
          , (   business_unit
             || '|'
             || branch_loc_num
             || '|'
             || bu_sku
             || '|'
             || TO_CHAR (snapshotdate, 'MM/DD/YYYY')
             || '|'
             || NVL (moving_avg_cost, 0)
             || '|'
             || onhand_qty
             || '|'
             || on_order_qty
             || '|'
             || service_qty
             || '|'
             || transit_qty
             || '|'
             || available_qty
             || '|'
             || cust_consigned_qty
             || '|'
             || vendor_consigned_qty
             || '|'
             || review_qty
             || '|'
             || defective_qty
             || '|'
             || overship_qty
             || '|'
             || display_qty
             || '|'
             || stock_qty)
               inventory_data
      FROM xxwc.xxwc_inv_onhand_mv##XX
/

-- End of DDL Script for View APPS.XXWC_INV_ONHAND_QTY_EXT_VW
