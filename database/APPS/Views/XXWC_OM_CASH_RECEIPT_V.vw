CREATE OR REPLACE FORCE VIEW APPS.XXWC_OM_CASH_RECEIPT_V
(
   HEADER_ID
 , PAYMENT_NUMBER
 , PAYMENT_TYPE_CODE
 , PREPAID_AMOUNT
 , CHECK_NUMBER
 , RECEIPT_NUMBER
 , RECEIPT_DATE
 , RECEIPT_METHOD_NAME
 , AMOUNT
 , CASH_RECEIPT_ID
 , DOC_SEQUENCE_VALUE
 , MAX_REFUND_AMOUNT
 , CARD_ISSUER_CODE
 , CARD_NUMBER
 , TRXN_EXTENSION_ID
)
AS
   SELECT DISTINCT
          a.header_id
        , a.payment_number
        , a.payment_type_code
        , a.prepaid_amount
        , a.check_number
        , b.receipt_number
        , b.receipt_date
        , c.name
        , b.amount
        , b.cash_receipt_id
        , b.doc_sequence_value
        , (SELECT NVL (SUM (amount_applied), 0)
             FROM apps.ar_receivable_applications_v d
            WHERE d.cash_receipt_id = b.cash_receipt_id
                  AND d.applied_payment_schedule_id > 0)
             max_refund_amount
        , NULL card_issuer_code
        , NULL card_number
        , a.trxn_extension_id
     /*used in cash refunds from from OM*/
     FROM oe_payments a
        , ar_receipt_methods c
        , (SELECT DISTINCT b.receipt_number
                         , b.receipt_date
                         , b.amount
                         , b.cash_receipt_id
                         , c.payment_set_id
                         , b.receipt_method_id
                         , b.doc_sequence_value
                         , b.payment_trxn_extension_id
             FROM apps.ar_cash_receipts b, apps.ar_receivable_applications c
            WHERE c.cash_receipt_id = b.cash_receipt_id
                  AND c.payment_set_id IS NOT NULL) b
    WHERE     a.payment_level_code = 'ORDER'
          AND a.payment_collection_event = 'PREPAY'
          AND a.payment_set_id = b.payment_set_id
          AND DECODE (a.payment_type_code
                    , 'CHECK', a.check_number
                    , b.doc_sequence_value) = b.receipt_number
          AND a.receipt_method_id = b.receipt_method_id
          AND a.trxn_extension_id IS NULL
          AND a.receipt_method_id=c.receipt_method_id
   UNION
   SELECT DISTINCT
          a.header_id
        , a.payment_number
        , a.payment_type_code
        , a.prepaid_amount
        , a.check_number
        , b.receipt_number
        , b.receipt_date
        , c.name
        , b.amount
        , b.cash_receipt_id
        , b.doc_sequence_value
        , (SELECT NVL (SUM (amount_applied), 0)
             FROM apps.ar_receivable_applications_v d
            WHERE d.cash_receipt_id = b.cash_receipt_id
                  AND d.applied_payment_schedule_id > 0)
             max_refund_amount
        , x.card_issuer_code
        , LTRIM (x.card_number, 'X') card_number
        , a.trxn_extension_id
     /*used in cash refunds from from OM*/
     FROM oe_payments a
        , ar_receipt_methods c
        , (SELECT DISTINCT b.receipt_number
                         , b.receipt_date
                         , b.amount
                         , b.cash_receipt_id
                         , c.payment_set_id
                         , b.receipt_method_id
                         , b.doc_sequence_value
                         , b.payment_trxn_extension_id
             FROM apps.ar_cash_receipts b, apps.ar_receivable_applications c
            WHERE c.cash_receipt_id = b.cash_receipt_id
                  AND c.payment_set_id IS NOT NULL) b
        , apps.IBY_TRXN_EXTENSIONS_V x
        , apps.IBY_TRXN_EXTENSIONS_V y
    WHERE     a.payment_level_code = 'ORDER'
          AND a.payment_collection_event = 'PREPAY'
          AND a.payment_set_id = b.payment_set_id
          AND DECODE (a.payment_type_code
                    , 'CHECK', a.check_number
                    , b.doc_sequence_value) = b.receipt_number
          AND a.receipt_method_id = b.receipt_method_id
          AND a.receipt_method_id=c.receipt_method_id
          AND a.trxn_extension_id IS NOT NULL
          AND a.trxn_extension_id = x.trxn_extension_id
          AND b.payment_trxn_extension_id = y.trxn_extension_id
          AND b.receipt_number = y.trxn_ref_number2
          AND x.instrument_id = y.instrument_id
          AND x.instr_assignment_id = y.instr_assignment_id
          AND y.origin_application_id = 222
          AND y.trxn_ref_number1 = 'RECEIPT'
UNION
     SELECT DISTINCT
          b.header_id
        , g.payment_number
        , g.payment_type_code
        , g.prepaid_amount
        , g.check_number
        , f.receipt_number
        , f.receipt_date
        , h.name
        , f.amount
        , f.cash_receipt_id
        , f.doc_sequence_value
        , (SELECT NVL (SUM (amount_applied), 0)
             FROM apps.ar_receivable_applications_v arpv
            WHERE arpv.cash_receipt_id = f.cash_receipt_id
                  AND arpv.applied_payment_schedule_id > 0)
             max_refund_amount
        , NULL card_issuer_code
        , NULL card_number
        , g.trxn_extension_id
  from apps.oe_order_lines a, apps.oe_order_headers b, apps.ra_customer_trx_lines c ,
       apps.ra_customer_trx d , ar_receivable_applications e , ar_cash_receipts f ,
       oe_payments g , ar_receipt_methods h, apps.oe_order_lines i, ar_customer_profiles_v j 
 where a.header_id=b.header_id
   and to_char(a.line_id)=c.interface_line_attribute6
   and to_char(b.order_number)=c.interface_line_attribute1
   and c.line_type='LINE'
   and c.sales_order=b.order_number
   and c.interface_line_context='ORDER ENTRY'
   and c.customer_trx_id=d.customer_trx_id
   and d.customer_trx_id=e.applied_customer_trx_id
   and e.cash_receipt_id=f.cash_receipt_id
   --and e.payment_set_id IS NOT NULL
   and g.payment_set_id= (select arp.payment_set_id from apps.ar_receivable_applications arp
                           where arp.cash_receipt_id=f.cash_receipt_id
                             and arp.payment_set_id is not null
                             and rownum=1)
   and g.payment_level_code = 'ORDER'
   AND g.payment_collection_event = 'PREPAY'
--   AND g.payment_set_id = e.payment_set_id
   AND DECODE (g.payment_type_code
                    , 'CHECK', g.check_number
                    , f.doc_sequence_value) = f.receipt_number
   AND g.receipt_method_id = f.receipt_method_id
   AND g.trxn_extension_id IS NULL
   AND g.receipt_method_id=h.receipt_method_id
   AND i.inventory_item_id=2931195
   and g.header_id=i.header_id
   AND b.sold_to_org_id=j.customer_id
   and j.site_use_id is null 
   and j.customer_profile_class_id in (1040)
UNION
     SELECT DISTINCT
          b.header_id
        , g.payment_number
        , g.payment_type_code
        , g.prepaid_amount
        , g.check_number
        , f.receipt_number
        , f.receipt_date
        , h.name
        , f.amount
        , f.cash_receipt_id
        , f.doc_sequence_value
        , (SELECT NVL (SUM (amount_applied), 0)
             FROM apps.ar_receivable_applications_v arpv
            WHERE arpv.cash_receipt_id = f.cash_receipt_id
                  AND arpv.applied_payment_schedule_id > 0)
             max_refund_amount
        , x.card_issuer_code
        , LTRIM (x.card_number, 'X') card_number
        , g.trxn_extension_id
  from apps.oe_order_lines a, apps.oe_order_headers b, apps.ra_customer_trx_lines c ,
       apps.ra_customer_trx d , ar_receivable_applications e , ar_cash_receipts f ,
       oe_payments g , ar_receipt_methods h, apps.oe_order_lines i, ar_customer_profiles_v j
       , apps.IBY_TRXN_EXTENSIONS_V x
       , apps.IBY_TRXN_EXTENSIONS_V y
 where a.header_id=b.header_id
   and to_char(a.line_id)=c.interface_line_attribute6
   and to_char(b.order_number)=c.interface_line_attribute1
   and c.line_type='LINE'
   and c.sales_order=b.order_number
   and c.interface_line_context='ORDER ENTRY'
   and c.customer_trx_id=d.customer_trx_id
   and d.customer_trx_id=e.applied_customer_trx_id
   and e.cash_receipt_id=f.cash_receipt_id
   --and e.payment_set_id IS NOT NULL
   and g.payment_set_id= (select arp.payment_set_id from apps.ar_receivable_applications arp
                           where arp.cash_receipt_id=f.cash_receipt_id
                             and arp.payment_set_id is not null
                             and rownum=1)
   and g.payment_level_code = 'ORDER'
   AND g.payment_collection_event = 'PREPAY'
--   AND g.payment_set_id = e.payment_set_id
   AND DECODE (g.payment_type_code
                    , 'CHECK', g.check_number
                    , f.doc_sequence_value) = f.receipt_number
   AND g.receipt_method_id = f.receipt_method_id
   AND g.trxn_extension_id IS not NULL
   AND g.receipt_method_id=h.receipt_method_id
   AND i.inventory_item_id=2931195
   and g.header_id=i.header_id
   AND b.sold_to_org_id=j.customer_id
   and j.site_use_id is null 
   and j.customer_profile_class_id in (1040)
  AND g.trxn_extension_id IS NOT NULL
  AND g.trxn_extension_id = x.trxn_extension_id
  AND f.payment_trxn_extension_id = y.trxn_extension_id
  AND f.receipt_number = y.trxn_ref_number2
  AND x.instrument_id = y.instrument_id
  AND x.instr_assignment_id = y.instr_assignment_id
  AND y.origin_application_id = 222
  AND y.trxn_ref_number1 = 'RECEIPT'
/  
