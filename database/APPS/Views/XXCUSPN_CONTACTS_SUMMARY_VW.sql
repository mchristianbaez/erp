
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUSPN_CONTACTS_SUMMARY_VW" ("LEASE_ROLE", "CONTACT_NAME_ATTN", "ADDRESS", "PHONE", "FAX", "OTHER_PHONES", "EMAIL", "ACCOUNT_NUMBER", "VENDOR_TYPE", "VENDOR_NUM", "IS_NOTICE_COMPANY", "LEASE_ID") AS 
  SELECT ca.lease_role, c.last_name||','||c.first_name contact_name_Attn ,
    vendordff.name||','||ca.concatenated_address address,
    pgen.area_code||' '||pgen.phone_number||DECODE(pgen.extension, null, null,'   x')||pgen.extension Phone,
    pfax.area_code||' '||pfax.phone_number fax,
    pothers.area_code||' '||pothers.phone_number||DECODE(pothers.extension, null, null,'   x')||pothers.extension other_phones,
    c.email_Address email,
    sitedff.attribute2 account_number,
    vendordff.attribute3 vendor_type,
    vendordff.attribute4 vendor_num,
    DECODE(instr(lease_role, 'Notice'), 0, NUll, 'Yes') is_notice_company,
    ca.lease_id
FROM   PN_LEASE_CONTACT_ASSIGN_V  ca,
    PN_CONTACTS_V c,
    (SELECT * FROM PN_PHONES_V p1 WHERE p1.phone_type_is = 'General' AND p1.status = 'A') pgen,
      (SELECT * FROM PN_PHONES_V p2 WHERE p2.phone_type_is = 'Fax'  AND p2.status = 'A') pfax,
    (SELECT * FROM PN_PHONES_V p3 WHERE p3.phone_type_is NOT IN ('General', 'Fax') AND p3.status = 'A') pothers,
    (SELECT name, attribute2, attribute3, attribute4, company_id  FROM pn_companies_all) vendordff,
    (SELECT company_site_id, attribute2 FROM pn_company_sites_all) sitedff
WHERE  ca.status = 'A'
AND    ca.company_site_id = c.company_site_id(+)
AND    c.contact_id = pgen.contact_id(+)
AND    c.contact_id = pfax.contact_id(+)
AND    c.contact_id = pothers.contact_id(+)
AND    ca.company_id = vendordff.company_id
AND    ca.company_site_id = sitedff.company_site_id
;
