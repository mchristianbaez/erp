
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUSAPEX_P2P_VENDOR_VW" ("ORG_NAME", "ORG_ID", "VENDOR_ID", "VENDOR_NAME", "SEGMENT1", "VENDOR_SITE_ID", "ADDRESS_LINE1", "ADDRESS_LINE2", "CITY", "STATE", "ZIP", "VENDOR_SITE_CODE", "VENDOR_TYPE_LOOKUP_CODE", "PAY_GROUP_LOOKUP_CODE", "PAY_SITE_FLAG", "INACTIVE_DATE", "ENABLED_FLAG", "START_DATE_ACTIVE", "END_DATE_ACTIVE", "VENDOR_TERM", "VENDOR_TERM_DESCRIPTION", "VENDOR_SITE_TERM", "VENDOR_SITE_TERM_DESCRIPTION") AS 
  SELECT ou.name org_name,
          b.org_id org_id,         
          a.vendor_id,
          a.vendor_name,
          a.segment1,
          b.vendor_site_id,
          b.address_line1,
          b.address_line2,
          b.city,
          b.state,
          b.zip,
          b.vendor_site_code,
          a.vendor_type_lookup_code,
          b.pay_group_lookup_code,
          b.pay_site_flag,
          b.inactive_date,
          a.enabled_flag,
          a.start_date_active,
          a.end_date_active,
          a.terms_id vendor_term,
          ff.description vendor_term_description,
          b.terms_id vendor_site_term,
          gg.description vendor_site_term_description
     FROM ap.ap_suppliers a,
          ap.ap_supplier_sites_all b,
          ap.ap_terms_tl ff,
          ap.ap_terms_tl gg,
          hr_operating_units ou
    WHERE 1 =1
      AND a.vendor_id =b.vendor_id
      AND ff.term_id  =a.terms_id
      AND gg.term_id  =b.terms_id
      AND b.org_id    =ou.organization_id;
