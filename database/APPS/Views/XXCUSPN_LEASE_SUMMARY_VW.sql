  /*
  ===========================================================================
    Module: Oracle Property Manager
    Type: HDS GSC custom view
    PURPOSE: Used by HDS OPN Property Manager -Oracle Application Form SuperSearch UI TAB  "Options"
    HISTORY
  ==============================================================================================================================
      VERSION        DATE          AUTHOR(S)          DESCRIPTION                       TICKET
      -------        -----------   ---------------    ------------------------------    ---------------                          
	  1.0                                             Header Missing
      1.1            30-Dec-2015   Balaguru Seshadri  Replace complete view.            TMS 20160209-00169           
      1.2            03-Mar-2016   Balaguru Seshadri  Fix logic for loc_docs and subtenant field.            TMS 20160316-00172      
  */ 

CREATE OR REPLACE FORCE VIEW APPS.XXCUSPN_LEASE_SUMMARY_VW  -- Begin Ver 1.1
(
   RER_ID,
   LOC_ID,
   LOB,
   RECORD_NAME,
   ADDRESS,
   ADDRESS3,
   CITY,
   ST_PRV,
   RE_TYPE,
   RER_STATUS,
   OPS_STATUS,
   EXTENDED_PARTY,
   LANDLORD_NAME,
   RER_DATA,
   RER_DOCS,
   LOC_DATA,
   LOC_DOCS,
   OPS_TYPE,
   ZIP,
   COUNTY,
   OPS_STATUS_START,
   OPS_STATUS_END,
   LOB_ABB,
   LOB_BRANCH,
   FRU_ID,
   FRU_DESCRIPTION,
   MORTGAGED,
   RELATED_PARTY,
   REAL_ESTATE_MGR_ID,
   HDS_RENT_ACCT_ID,
   HDS_PROPERTY_MGR_ID,
   SUBTENANT_NAME,
   PAYMENT_CONTACT_NAME,
   LEASE_COMMENCEMENT_DATE,
   LEASE_EXECUTION_DATE,
   LEASE_TERMINATION_DATE,
   LEASE_EXTENSION_END_DATE,
   GUARANTOR,
   REAL_ESTATE_MGR_NAME,
   BUILD_LAND_LOCATION_ID,
   CURRENT_ENTITY,
   FPS_ID,
   ORA_SEG_TWO,
   USAGE_CODE,
   USAGE_NAME,
   LEGACY_ID,
   hds_opn_primary_loc_for_rer, --Use to pick the primary lease tenancy record. Only one record of the available tenancies is flagged a Y and all others a N
   tenancy_id,
   ten_locn_id,
   lea_locn_id,
   lease_id,
   customer_id,
   customer_site_use_id,
   abstracted_by_user_id,
   org_id,
   address_id,
   lea_expense_ccid,
   lea_liability_ccid,
   lea_accrual_ccid,
   rem_user_id,
   loc_active_start_date,
   loc_active_end_date,
   hds_total_bldg_sf_for_rer,
   hds_total_land_sf_for_rer,
   hds_ten_loc_rentable_area_sf,
   hds_loc_tie_breaker_seq, 
   lob_dba, 
   sublob_abb, 
   sublob_dba, 
   sub_bu_rank,
   lease_status_code,
   approval_status_code,
   lease_type_code,
   lease_class_code,
   recovery_space_std_code,
   recovery_type_code      
)
AS
   SELECT lea.rer_id, --lea.lease_num RER_id,
          lea.loc_code, --loc.location_code LOC_ID,
          lob_abb, --lobrollup.bu_id LOB,
          lea.rer_name, --lea.name record_name,
          lea.address_line1||' '||opn_tenancy_suite, --addr.address_line1 address,
          lea.address_line3 address3,
          -- 
          lea.city, --addr.city city,
          lea.state_province, --NVL (addr.state, addr.province) st_prv,
          lea.lease_type RE_type,
          --
          lea.lease_status,
          Null ops_status,
          case
           when lea.lease_type_code ='LSEE' then
             (
               nvl(
                          (
                             SELECT d.text landlord_name
                             FROM pn_note_headers h, pn_note_details d
                             WHERE     h.note_header_id = d.note_header_id
                                  AND h.lease_id = lea.lease_id
                                  AND h.note_type_lookup_code ='LLCUR'
                                  AND ROWNUM < 2
                           ) 
                  ,
                          (
                             SELECT d.text landlord_name
                             FROM pn_note_headers h, pn_note_details d
                             WHERE     h.note_header_id = d.note_header_id
                                  AND h.lease_id = lea.lease_id
                                  AND h.note_type_lookup_code ='LLORIG'
                                  AND ROWNUM < 2                          
                          )
                  )            
             )
           when lea.lease_type_code IN ('LSE', 'ISUB') then --Print subtenant name only income lease and income sub lease
             (
               nvl(
                          (
                             SELECT d.text landlord_name
                             FROM pn_note_headers h, pn_note_details d
                             WHERE     h.note_header_id = d.note_header_id
                                  AND h.lease_id = lea.lease_id
                                  AND h.note_type_lookup_code ='TCURR'
                                  AND ROWNUM < 2
                           ) 
                  ,
                          (
                             SELECT d.text landlord_name
                             FROM pn_note_headers h, pn_note_details d
                             WHERE     h.note_header_id = d.note_header_id
                                  AND h.lease_id = lea.lease_id
                                  AND h.note_type_lookup_code ='TORIG'
                                  AND ROWNUM < 2                          
                          )
                  )            
             )             
          else to_char(Null) --every other RER type print blank values
          end extended_party,          
          --
          /* --02/10/2016
          (SELECT d.text landlord_name
             FROM pn_note_headers h, pn_note_details d
            WHERE     h.note_header_id = d.note_header_id
                  AND h.lease_id = lea.lease_id
                  AND h.note_type_lookup_code =
                         DECODE (lea.lease_type_code,
                                 'LSEE', 'LLCUR',
                                 'LSE', 'TCUR',
                                 'ISUB', 'TCUR',
                                 NULL)
                  AND ROWNUM < 2)
             landlard_name,
          */ --02/10/2016
          case
           when lea.lease_type_code ='LSEE' then
             (
               nvl(
                          (
                             SELECT d.text landlord_name
                             FROM pn_note_headers h, pn_note_details d
                             WHERE     h.note_header_id = d.note_header_id
                                  AND h.lease_id = lea.lease_id
                                  AND h.note_type_lookup_code ='LLCUR'
                                  AND ROWNUM < 2
                           ) 
                  ,
                          (
                             SELECT d.text landlord_name
                             FROM pn_note_headers h, pn_note_details d
                             WHERE     h.note_header_id = d.note_header_id
                                  AND h.lease_id = lea.lease_id
                                  AND h.note_type_lookup_code ='LLORIG'
                                  AND ROWNUM < 2                          
                          )
                  )            
             )
          else to_char(Null) --if the RER type is other than expense lease return blank for landlord name
          end landlord_name,       
          lea.lease_id RER_data, --DO NOT change this as the internal id will be used by OAF to open the Oracle standard lease form using FND Function call
          lea.rer_doc_link RER_docs,
          lea.ten_locn_id LOC_data, -- --DO NOT change this as the internal id will be used by OAF to open the Oracle standard locations form using FND Function call
          (
            select attribute6
            from   pn_locations_all
            where 1 =1
            and location_id  =lea.ten_locn_id
            and active_start_date =lea.loc_active_start_date
            --and active_end_date   =lea.loc_active_end_date --Ver 1.2
            and nvl(active_end_date, to_date('12/31/4712', 'mm/dd/yyyy')) = nvl(active_end_date,  to_date('12/31/4712', 'mm/dd/yyyy')) --Ver 1.2
          ) LOC_docs,
          --
          Null ops_type,
          lea.postal_code,
          lea.county,
          Null ops_status_start,
          Null ops_status_end,
          lea.lob_abb,
          lea.lob_branch,
          lea.fru FRU_id,
		  lea.fru_description,
          lea.mortgaged,
          lea.related_party_name,
          lea.rem_ntid real_estate_mgr_id, 
          --
          (SELECT comp.company_id
             FROM apps.PN_Contact_Assignments_All cona,
                  apps.PN_Company_Sites_All comps,
                  apps.PN_Companies_All comp,
                  apps.fnd_lookups flv
            WHERE     cona.company_id = comps.company_id
                  AND cona.company_site_id = comps.company_site_id
                  AND comps.company_id = comp.company_id
                  AND flv.lookup_code = comps.lease_role_type
                  AND flv.lookup_type = 'PN_LEASE_ROLE_TYPE'
                  AND flv.meaning = 'HDS Rent Acct'
                  AND cona.lease_id = lea.lease_id
                  AND cona.status = 'A'
                  AND ROWNUM < 2)
             hds_rent_acct_id,
          --
          (SELECT comp.company_id
             FROM apps.PN_Contact_Assignments_All cona,
                  apps.PN_Company_Sites_All comps,
                  apps.PN_Companies_All comp,
                  apps.fnd_lookups flv
            WHERE     cona.company_id = comps.company_id
                  AND cona.company_site_id = comps.company_site_id
                  AND comps.company_id = comp.company_id
                  AND flv.lookup_code = comps.lease_role_type
                  AND flv.lookup_type = 'PN_LEASE_ROLE_TYPE'
                  AND flv.meaning = 'HDS Property Manager'
                  AND cona.lease_id = lea.lease_id
                  AND cona.status = 'A'
                  AND ROWNUM < 2)
             hds_property_mgr_id,
          --
          /* --02/10/2016
          (SELECT comp.name
             FROM apps.PN_Contact_Assignments_All cona,
                  apps.PN_Company_Sites_All comps,
                  apps.PN_Companies_All comp,
                  apps.fnd_lookups flv
            WHERE     cona.company_id = comps.company_id
                  AND cona.company_site_id = comps.company_site_id
                  AND comps.company_id = comp.company_id
                  AND flv.lookup_code = comps.lease_role_type
                  AND flv.lookup_type = 'PN_LEASE_ROLE_TYPE'
                  AND flv.meaning = 'Subtenant'
                  AND cona.lease_id = lea.lease_id
                  AND cona.status = 'A'
                  AND ROWNUM < 2)
             subtenant_name,
           */ --02/10/2016
          case
           when lea.lease_type_code IN ('LSE', 'ISUB') then --Print subtenant name only income lease and income sub lease
             (
               nvl(
                          (
                             SELECT d.text landlord_name
                             FROM pn_note_headers h, pn_note_details d
                             WHERE     h.note_header_id = d.note_header_id
                                  AND h.lease_id = lea.lease_id
                                  --AND h.note_type_lookup_code ='TCURR' --Ver 1.2
                                  AND h.note_type_lookup_code ='TCUR' --Ver 1.2                                  
                                  AND ROWNUM < 2
                           ) 
                  ,
                          (
                             SELECT d.text landlord_name
                             FROM pn_note_headers h, pn_note_details d
                             WHERE     h.note_header_id = d.note_header_id
                                  AND h.lease_id = lea.lease_id
                                  AND h.note_type_lookup_code ='TORIG'
                                  AND ROWNUM < 2                          
                          )
                  )            
             )
          else to_char(Null) --if the lease type is expense lease return blank for subtenant name
          end subtenant_name,              
          --
          (SELECT comp.name
             FROM apps.PN_Contact_Assignments_All cona,
                  apps.PN_Company_Sites_All comps,
                  apps.PN_Companies_All comp,
                  apps.fnd_lookups flv
            WHERE     cona.company_id = comps.company_id
                  AND cona.company_site_id = comps.company_site_id
                  AND comps.company_id = comp.company_id
                  AND flv.lookup_code = comps.lease_role_type
                  AND flv.lookup_type = 'PN_LEASE_ROLE_TYPE'
                  AND flv.meaning = 'Payment Contact'
                  AND cona.lease_id = lea.lease_id
                  AND cona.status = 'A'
                  AND ROWNUM < 2)
             payment_contact_name,
          --
          lea.lease_commencement lease_commencement_date,
          lea.lease_execution lease_execution_date,
          lea.lease_termination lease_termination_date,
          Null lease_extension_end_date,
          --
          lea.guarantor guarantor,
          lea.rem_name real_estate_mgr_name,
          Null build_land_location_id, --Pick from xxcus.xxcus_opn_fru_loc_all table
          (SELECT entn.text
             FROM apps.xxcuspn_lease_entity_notes_vw entn
            WHERE entn.lease_id = lea.lease_id AND ROWNUM < 2) current_entity,
          Null fps_id, --Pick from xxcus.xxcus_opn_fru_loc_all table
          lea.oracle_id ora_seg_two, --cc.segment2 ora_seg_two,
          lea.tenancy_usage_code usage_code,
          lea.tenancy_usage usage_name,
          lea.legacy_id,
          lea.hds_opn_primary_loc_for_rer,
          lea.tenancy_id,
          lea.ten_locn_id,
          lea.lea_locn_id,
          lea.lease_id,
          lea.customer_id,
          lea.customer_site_use_id,
          lea.abstracted_by_user_id,
          lea.org_id,
          lea.address_id,
          lea.lea_expense_ccid,
          lea.lea_liability_ccid,
          lea.lea_accrual_ccid,
          lea.rem_user_id,
          lea.loc_active_start_date,
          lea.loc_active_end_date,
          lea.hds_total_bldg_sf_for_rer,
          lea.hds_total_land_sf_for_rer,
          lea.hds_ten_loc_rentable_area_sf,
          lea.hds_loc_tie_breaker_seq,
          lob_dba, 
          sublob_abb, 
          sublob_dba, 
          sub_bu_rank,
           lease_status_code,
           approval_status_code,
           lease_type_code,
           lease_class_code,
           recovery_space_std_code,
           recovery_type_code                   
     FROM xxcus.xxcus_opn_rer_locations_all lea          
    WHERE 1 =1
    ;
--
COMMENT ON  TABLE APPS.XXCUSPN_LEASE_SUMMARY_VW IS 'TMS:  20160316-00172 / ESMS: 320528'; 
-- End Ver 1.1
-- 
-- Begin Ver 1.0
/*

  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUSPN_LEASE_SUMMARY_VW" ("RER_ID", "LOC_ID", "LOB", "RECORD_NAME", "ADDRESS", "CITY", "ST_PRV", "RE_TYPE", "RER_STATUS", "OPS_STATUS", "LANDLARD_NAME", "RER_DATA", "RER_DOCS", "LOC_DATA", "LOC_DOCS", "OPS_TYPE", "ZIP", "COUNTY", "OPS_STATUS_START", "OPS_STATUS_END", "LOB_BRANCH", "FRU_ID", "MORTGAGED", "RELATED_PARTY", "REAL_ESTATE_MGR_ID", "HDS_RENT_ACCT_ID", "HDS_PROPERTY_MGR_ID", "SUBTENANT_NAME", "PAYMENT_CONTACT_NAME", "LEASE_COMMENCEMENT_DATE", "LEASE_EXECUTION_DATE", "LEASE_TERMINATION_DATE", "LEASE_EXTENSION_END_DATE", "GUARANTOR", "REAL_ESTATE_MGR_NAME", "BUILD_LAND_LOCATION_ID", "CURRENT_ENTITY", "FPS_ID", "ORA_SEG_TWO", "USAGE_CODE", "USAGE_NAME", "LEGACY_ID") AS 
  SELECT lea.lease_num RER_id,
     loc.location_code LOC_ID,
     lobrollup.bu_id LOB,
       lea.name record_name,
     addr.address_line1 address,
     addr.city city,
     NVL(addr.state, addr.province) st_prv,
     (SELECT flv.meaning FROM apps.fnd_lookups flv
      WHERE  flv.lookup_type = 'PN_LEASE_TYPE'
    AND    flv.LOOKUP_CODE = lea.lease_type_code) RE_type,
     (SELECT flv.meaning FROM apps.fnd_lookups flv
      WHERE  flv.lookup_type = 'PN_LEASESTATUS_TYPE'
    AND    flv.LOOKUP_CODE = lea.lease_status) RER_status,
      'InActive' ops_status,
     (SELECT d.text landlord_name
      FROM   pn_note_headers h,
                  pn_note_details d
      WHERE  h.note_header_id = d.note_header_id
      AND    h.lease_id = lea.lease_id
      AND   h.note_type_lookup_code = DECODE(lea.lease_type_code, 'LSEE',  'LLCUR',
                                                                  'LSE' ,  'TCUR',
                                                                  'ISUB' ,  'TCUR',
                                                                  NULL)
      AND   ROWNUM < 2) landlard_name,
     lea.lease_id RER_data,
   ldet.attribute8 RER_docs,
     loc.location_id LOC_data, -- Location is  derived from tenencies; lease level  location id is not used
   NVL(loc.attribute6,
       (SELECT raddr.attribute6
         FROM   apps.pn_locations_all raddr
          WHERE raddr.parent_location_id is null
          AND   raddr.active_end_date = (SELECT MAX(mx.active_end_date) FROM apps.pn_locations_all mx WHERE mx.location_id = loc.location_id)
          CONNECT BY raddr.location_id = prior raddr.parent_location_id
          START WITH raddr.location_id = loc.location_id AND   raddr.active_end_date = (SELECT MAX(mx.active_end_date) FROM apps.pn_locations_all mx WHERE mx.location_id = loc.location_id))) LOC_docs,
-- Other searchable columns
     null ops_type, --  col fron len don't know yet
     addr.zip_code zip,
     addr.county county,
     null ops_status_start,
     null ops_status_end,
     (SELECT lob_branch
      FROM XXCUS_LOCATION_CODE_VW lxref
      WHERE lxref.fru = (SELECT fru
                     FROM XXCUS_LOCATION_CODE_VW lxref
                     WHERE lxref.entrp_loc = cc.segment2
                   AND ROWNUM < 2)
    AND ROWNUM < 2) LOB_branch,
   (SELECT fru
      FROM XXCUS_LOCATION_CODE_VW lxref
      WHERE lxref.entrp_loc = cc.segment2
    AND ROWNUM < 2) FRU_id,
     ldet.attribute2 mortgaged,
   (SELECT v.description
    FROM   apps.FND_FLEX_VALUE_SETS s, apps.fnd_flex_values_vl v
      WHERE upper(flex_value_set_name) = upper('xxcus_pn_rel_party')
      AND s.flex_value_set_id = v.flex_value_set_id
      AND v.flex_value = ldet.attribute3)  related_party,
     ldet.responsible_user real_estate_mgr_id,
   (SELECT comp.company_id
       FROM apps.PN_Contact_Assignments_All cona,
          apps.PN_Company_Sites_All comps,
         apps.PN_Companies_All comp,
         apps.fnd_lookups flv
     WHERE cona.company_id = comps.company_id
     AND   cona.company_site_id = comps.company_site_id
     AND   comps.company_id = comp.company_id
     AND   flv.lookup_code = comps.lease_role_type
     AND   flv.lookup_type = 'PN_LEASE_ROLE_TYPE'
     AND   flv.meaning = 'HDS Rent Acct'
     AND   cona.lease_id = lea.lease_id
     AND   cona.status = 'A'
   AND   ROWNUM < 2) hds_rent_acct_id,
    (SELECT comp.company_id
       FROM apps.PN_Contact_Assignments_All cona,
          apps.PN_Company_Sites_All comps,
         apps.PN_Companies_All comp,
         apps.fnd_lookups flv
     WHERE cona.company_id = comps.company_id
     AND   cona.company_site_id = comps.company_site_id
     AND   comps.company_id = comp.company_id
     AND   flv.lookup_code = comps.lease_role_type
     AND   flv.lookup_type = 'PN_LEASE_ROLE_TYPE'
     AND   flv.meaning = 'HDS Property Manager'
     AND   cona.lease_id = lea.lease_id
     AND   cona.status = 'A'
   AND   ROWNUM < 2) hds_property_mgr_id,
    (SELECT comp.name
       FROM apps.PN_Contact_Assignments_All cona,
          apps.PN_Company_Sites_All comps,
         apps.PN_Companies_All comp,
         apps.fnd_lookups flv
     WHERE cona.company_id = comps.company_id
     AND   cona.company_site_id = comps.company_site_id
     AND   comps.company_id = comp.company_id
     AND   flv.lookup_code = comps.lease_role_type
     AND   flv.lookup_type = 'PN_LEASE_ROLE_TYPE'
     AND   flv.meaning = 'Subtenant'
     AND   cona.lease_id = lea.lease_id
     AND   cona.status = 'A'
   AND   ROWNUM < 2) subtenant_name,
    (SELECT comp.name
       FROM apps.PN_Contact_Assignments_All cona,
          apps.PN_Company_Sites_All comps,
         apps.PN_Companies_All comp,
         apps.fnd_lookups flv
     WHERE cona.company_id = comps.company_id
     AND   cona.company_site_id = comps.company_site_id
     AND   comps.company_id = comp.company_id
     AND   flv.lookup_code = comps.lease_role_type
     AND   flv.lookup_type = 'PN_LEASE_ROLE_TYPE'
     AND   flv.meaning = 'Payment Contact'
     AND   cona.lease_id = lea.lease_id
     AND   cona.status = 'A'
   AND   ROWNUM < 2) payment_contact_name,
   ldet.lease_commencement_date,
   ldet.LEASE_EXECUTION_DATE,
   ldet.LEASE_TERMINATION_DATE,
   ldet.LEASE_EXTENSION_END_DATE,
   ldet.attribute7 guarantor,
   (SELECT description
    FROM apps.fnd_user u
    WHERE user_id = ldet.responsible_user) real_estate_mgr_name,
    (SELECT raddr.location_id
          FROM   apps.pn_locations_all raddr
          WHERE raddr.parent_location_id is null
          AND   raddr.active_end_date = (SELECT MAX(mx.active_end_date) FROM apps.pn_locations_all mx WHERE mx.location_id = raddr.location_id)
          CONNECT BY raddr.location_id = prior raddr.parent_location_id
          START WITH raddr.location_id = loc.location_id AND   raddr.active_end_date = (SELECT MAX(mx.active_end_date) FROM apps.pn_locations_all mx WHERE mx.location_id = raddr.location_id)) build_land_location_id,
     (SELECT entn.text
    FROM apps.xxcuspn_lease_entity_notes_vw entn
    WHERE entn.lease_id = lea.lease_id
    AND ROWNUM < 2 ) current_entity,
   lobrollup.fps_id,
   cc.segment2 ora_seg_two,
   ten.tenancy_usage_lookup_code usage_code,
   (SELECT l.meaning FROM apps.fnd_lookup_values l
    WHERE l.lookup_type = 'PN_TENANCY_USAGE_TYPE'
    AND l.lookup_code = ten.tenancy_usage_lookup_code
    AND ROWNUM < 2) usage_name,
   ldet.attribute1 legacy_id
FROM apps.PN_LEASES_ALL lea,
   apps.pn_tenancies_all ten,
   apps.pn_locations_all loc,
   apps.pn_Addresses_all addr
  ,apps.PN_Lease_Details_All  ldet
  ,apps.gl_code_combinations cc
  ,apps.XXCUSPN_LD_LOB_ROLLUP_VW lobrollup
WHERE lea.lease_id = ten.lease_id
-- to get locations that child to this tenancy location as well as tenancy location that exists in Space Assign
--
AND   loc.location_id  = ten.location_id
AND   loc.active_end_date = (SELECT MAX(mx.active_end_date) FROM apps.pn_locations_all mx WHERE mx.location_id = loc.location_id)
AND   addr.address_id =
           (SELECT raddr.address_id
          FROM   apps.pn_locations_all raddr
          WHERE raddr.parent_location_id is null
          AND   raddr.active_end_date = (SELECT MAX(mx.active_end_date) FROM apps.pn_locations_all mx WHERE mx.location_id = raddr.location_id)
          CONNECT BY raddr.location_id = prior raddr.parent_location_id
          START WITH raddr.location_id = loc.location_id
      AND   raddr.active_end_date = (SELECT MAX(mx.active_end_date) FROM apps.pn_locations_all mx WHERE mx.location_id = raddr.location_id)
      )
AND   lea.lease_id = ldet.lease_id
AND   NVL(ldet.expense_account_id, ldet.receivable_account_id) = cc.code_combination_id(+)
AND   cc.segment1 = lobrollup.fps_id(+)
;
*/
-- End Ver 1.0