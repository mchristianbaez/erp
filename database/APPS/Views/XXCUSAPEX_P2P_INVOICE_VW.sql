
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUSAPEX_P2P_INVOICE_VW" ("INVOICE_ID", "INVOICE_NUM", "INVOICE_DATE", "INVOICE_AMOUNT", "SCHEDULED DUE DATE", "CHECK_NUMBER", "CHECK_DATE", "AMOUNT", "PAYMENT_STATUS_FLAG", "AMOUNT_REMAINING", "LINE_AMOUNT", "EXPENSE_GL", "LIABILITY_GL", "ACCOUNTING_DATE", "PERIOD_NAME", "CREATION_DATE") AS 
  SELECT i.invoice_id
      ,invoice_num
      ,invoice_date
      ,invoice_amount
      ,ps.due_date "SCHEDULED DUE DATE"
       ,c.check_number
       ,c.check_date
       ,c.amount
       ,ps.payment_status_flag
       ,ps.amount_remaining
       ,d.amount line_amount
       ,exp.segment1 || '-' || exp.segment2 || '-' || exp.segment3 || '-' ||
       exp.segment4 || '-' || exp.segment5 || '-' || exp.segment6 || '-' ||
       exp.segment7 expense_gl
       ,(SELECT lia.segment1 || '-' || lia.segment2 || '-' || lia.segment3 || '-' ||
               lia.segment4 || '-' || lia.segment5 || '-' || lia.segment6 || '-' ||
               lia.segment7
          FROM apps.gl_code_combinations lia
         WHERE lia.code_combination_id =
               (SELECT accts_pay_code_combination_id
                  FROM ap.financials_system_params_all psp
                 WHERE psp.org_id = d.org_id)) liability_gl
       ,d.accounting_date
       ,d.period_name
       ,d.creation_date
  FROM ap.ap_invoices_all              i
      ,ap.ap_invoice_distributions_all d
      ,ap.ap_payment_schedules_all     ps
      ,ap.ap_invoice_payments_all      ip
      ,ap.ap_checks_all                c
      ,gl.gl_code_combinations         exp
 WHERE i.invoice_id = ps.invoice_id
   AND i.invoice_id = d.invoice_id
   AND i.invoice_id = ip.invoice_id(+)
   AND ip.check_id = c.check_id(+)
   AND d.org_id = 163
   AND nvl(c.status_lookup_code, '*') != 'VOIDED'
   AND d.dist_code_combination_id = exp.code_combination_id
;
