
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUSOZF_REBATE_SUM_V" ("COLLECTOR", "CUST_ID", "MVID", "VENDOR_NAME", "AGREEMENT_YEAR", "PURCHASES", "INCOME", "INVOICE_AMOUNT", "APPLIED_AMOUNT", "UNAPPLIED_AMOUNT", "AGING_30", "AGING_31_60", "AGING_61_90", "AGING_OVER_120") AS 
  select distinct
       ar.name COllector,
       m.cust_id cust_id,
       m.mvid mvid,
       m.vendor_name Vendor_Name, 
       m.agreement_year Agreement_Year,
       m.Purchases Purchases,
       m.Income Income,
       m.Invoice_Amount Invoice_Amount,
       m.Applied_Amount Applied_Amount,
       m.Unapplied_Amount Unapplied_Amount,
       m.Aging_30 Aging_30,
       m.Aging_31_60 Aging_31_60,
       m.Aging_61_90 Aging_61_90,
      m.Aging_over_120 Aging_over_120
    from 
 (
select cust_id,
      mvid,
      vendor_name, 
      agreement_year,
--      max(Collector) Collector,
      sum(Purchases) Purchases,
      sum(Income) Income,
      sum(Invoice_Amount) Invoice_Amount,
      sum(Applied_Amount) Applied_Amount,
      sum(Unapplied_Amount) Unapplied_Amount,
      sum ( Aging_30) Aging_30,
      sum(Aging_31_60) Aging_31_60,
      sum( Aging_61_90) Aging_61_90,
      sum(Aging_over_120) Aging_over_120
      from 
      (
  
        
--PART 1 Get Purchase Details and Unapplied Payments
       SELECT 
       c.customer_id cust_id,
       c.customer_attribute2 mvid,
       c.party_name vendor_name,       
           p.calendar_year agreement_year,
          SUM(p.total_purchases ) Purchases,
          0 Income,
          0 Invoice_Amount,
          0 Applied_Amount,
          sum(unapp.unapplied_amount) Unapplied_Amount,
          0 Aging_30,
          0 Aging_31_60,
          0 Aging_61_90,
          0 Aging_91_120,
          0 Aging_over_120
  FROM apps.xxcusozf_purchases_mv p, 
     xxcus.xxcus_rebate_customers c,
--    to get  Unapplied Amount 
    (   SELECT cr.pay_from_customer cust_id,

                 sum (abs(sch.amount_due_remaining))unapplied_amount      
            FROM ar_cash_receipts_all cr,
                 ar_receipt_methods rm,
                 hz_parties hp,
                 hz_cust_accounts hca,
                 AR_PAYMENT_SCHEDULES_ALL sch
            WHERE 1                  =1
            AND cr.status NOT       IN ('REV')
            AND cr.receipt_method_id =rm.receipt_method_id
            AND hca.cust_account_id  =cr.pay_from_customer
            AND hca.party_id         =hp.party_id
            AND cr.org_id           IN (101,102)
            AND sch.CASH_RECEIPT_ID  = cr.CASH_RECEIPT_ID
            AND rm.printed_name IN ('REBATE_CREDIT', 'REBATE_CHECK', 'REBATE_DEDUCTION','CONVERSION')
            AND cr.status='UNAPP'
            group by 
                  cr.pay_from_customer 
            
                    )unapp
     
  WHERE p.mvid       =c.customer_id
  and unapp.cust_id(+)= c.customer_id
  AND grp_mvid       =0
  AND grp_cal_year   =0
  AND grp_lob        =1
  AND grp_period     =1
  AND grp_qtr        =1
  AND grp_branch     =1
  AND grp_year       =1
  AND grp_cal_period =1
  and c.customer_attribute1='HDS_MVID'

   GROUP BY
          c.customer_id,
          c.customer_attribute2,
          c.party_name ,
          p.calendar_year 

          
          union
----PART 2  Get YTD Income

  select
        xla.ofu_cust_account_id cust_id,
        xla.ofu_mvid  mvid,
        xla.ofu_mvid_name vendor_name,
        xla.calendar_year agreement_year,
--        null Collector,
        0 Purchases,
        sum(util_acctd_amount) Income,
        0 Invoice_Amount,
          0 Applied_Amount,
          0 Unapplied_Amount,
          0 Aging_30,
          0 Aging_31_60,
          0 Aging_61_90,
          0 Aging_91_120,
          0 Aging_over_120
  FROM xxcus.xxcus_ozf_xla_accruals_b xla,xxcus.xxcus_rebate_customers c
       where 1=1
       and c.customer_id=xla.ofu_cust_account_id 
       and  c.customer_attribute1='HDS_MVID'
      
     group by 
        xla.ofu_cust_account_id ,
        xla.ofu_mvid ,
        xla.ofu_mvid_name ,
        xla.calendar_year
       
        union
       
----PART 3 Get Invoice Amount and Applied Payments
select cust_id,
        mvid,
        vendor_name,
        agreement_year,
--        null Collector,
        0 Purchases,
        0 Income,
        sum(nvl(Invoice,0)) Invoice,
        sum(nvl(Applied_Amount,0)) Amount,
       0 Unapplied_Amount,
          0 Aging_30,
          0 Aging_31_60,
          0 Aging_61_90,
          0 Aging_91_120,
          0 Aging_over_120
          from
    (SELECT 
     cust_id ,
     mvid ,  
    mv_name vendor_name ,
    agreement_year ,
    lob_name,
    SUM(xxcus_ozf_rptutil_pkg.get_invoice_amt(plan_id ,cust_id ,lob_name)) Invoice ,
    SUM(xxcus_ozf_rptutil_pkg.get_payment_amt(plan_id ,cust_id ,lob_name)) Applied_Amount
  FROM
    (SELECT hca.attribute2 mvid ,
      hca.cust_account_id cust_id ,
      hp.party_name mv_name ,
      qlhv.attribute7 agreement_year ,
       hp2.party_id lob_id ,
      hp2.party_name lob_name ,
      qlhv.list_header_id plan_id
        FROM apps.qp_list_headers_vl qlhv ,
      ozf.ozf_offers oo ,
      apps.ams_media_vl med ,
      qp.qp_qualifiers qq ,
      ar.hz_parties hp ,
      ar.hz_cust_accounts hca ,
      ozf.ozf_act_budgets oab ,
      apps.ozf_funds_all_vl ofa ,
      ar.ra_customer_trx_all rct ,
      ar.hz_relationships rel ,
      ar.hz_parties hp2
    WHERE qlhv.list_header_id      = qq.list_header_id
    AND qq.qualifier_context       = 'SOLD_BY'
    AND oo.qp_list_header_id       = qlhv.list_header_id
    AND oo.activity_media_id       = med.media_id
    AND hca.cust_account_id        = to_number(qq.qualifier_attr_value)
    AND qlhv.list_header_id        = oab.act_budget_used_by_id
    AND oab.budget_source_type     = 'FUND'
    AND oab.status_code            = 'APPROVED'
    AND oab.arc_act_budget_used_by = 'OFFR'
    AND ofa.fund_id                = oab.budget_source_id
    AND hca.party_id               = hp.party_id
    AND oo.status_code             = 'ACTIVE'
    AND qq.qualifier_attribute     = 'QUALIFIER_ATTRIBUTE2'
    AND qq.list_line_id            = -1
    AND rct.org_id                  IN (101, 102)
    AND qlhv.list_header_id        = to_number(rct.attribute5)
    AND rel.object_id              = to_number(rct.attribute1)
    AND rel.relationship_code      = 'HEADQUARTERS_OF'
    AND rel.subject_id             = hp2.party_id
    AND hp2.party_id NOT          IN ('2064','2063')
   AND qlhv.attribute7>2012
    GROUP BY hca.attribute2 ,
      hca.cust_account_id ,
      hp.party_name ,
      qlhv.attribute7 ,
        hp2.party_id ,
      hp2.party_name ,
      qlhv.list_header_id
    )
  GROUP BY
     cust_id ,
     mvid ,  
    mv_name  ,
    agreement_year ,
    lob_name
    )
    group by cust_id,
              mvid,
              vendor_name,
              agreement_year
    

        
  union 
  
   --- PART 4 Get Aging Invoice Amounts 
select apsa.CUSTOMER_ID cust_id,
       hca.attribute2 mvid,
       hp1.party_name vendor_name,
        qlhv.attribute7 agreement_year,
--        null Collector,
        0 Purchases,
        0 Income,
        0 Invoice,
        0 Amount,
       0 Unapplied_Amount,
     sum(case when sysdate-apsa.due_date<31  then abs(apsa.amount_due_remaining) else 0 end) Aging_30,
      sum(case when sysdate-apsa.due_date between 31 and 60  then abs(apsa.amount_due_remaining) else 0 end) Aging_31_60,
      sum(case when sysdate-apsa.due_date between 61 and 90 then abs(apsa.amount_due_remaining) else 0 end)  Aging_61_90,
      sum(case when sysdate-apsa.due_date between 91 and 120  then abs(apsa.amount_due_remaining) else 0 end) Aging_91_120,
       sum(case when sysdate-apsa.due_date>120  then abs(apsa.amount_due_remaining) else 0 end) Aging_over_120
       
  FROM                hz_parties                hp1
                      ,hz_cust_accounts          hca
                      ,ra_customer_trx_all       rcta
                      , ar_payment_schedules_all  apsa
                      ,qp_list_headers_vl        qlhv
                      ,ozf_claim_lines_all       ocl
                      ,ams_media_tl              med
                      ,ozf_offers                oo
                      ,ra_customer_trx_lines_all rctl
                      ,ra_cust_trx_types_all     rctt
                 WHERE 1 = 1
                   AND hca.party_id = hp1.party_id
                   AND rctt.cust_trx_type_id = rcta.cust_trx_type_id
                   AND oo.qp_list_header_id = qlhv.list_header_id
                   AND to_number(rctl.interface_line_attribute2) =
                       ocl.claim_id
                   AND ocl.activity_id = oo.qp_list_header_id
                   AND rcta.org_id IN (101, 102)
                   AND rcta.customer_trx_id = apsa.customer_trx_id
                   AND qlhv.attribute7 > = '2010'
                   AND oo.activity_media_id = med.media_id
                   AND rcta.customer_trx_id = rctl.customer_trx_id
                   AND rcta.bill_to_customer_id = hca.cust_account_id
                   AND apsa.class = 'INV'
                   AND hp1.attribute1 = 'HDS_MVID'
    group by apsa.CUSTOMER_ID,
             hca.attribute2,
             hp1.party_name,
              qlhv.attribute7 )

        group by cust_id,
              mvid,
              vendor_name,
              agreement_year
              )
              m,
             HZ_CUSTOMER_PROFILES HCP,
             ar_collectors        ar
              
              where
              1=1
                 and m.cust_id = hcp.cust_account_id
            AND ar.collector_id = hcp.collector_id;
