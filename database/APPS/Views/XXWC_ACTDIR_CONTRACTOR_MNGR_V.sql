-------------------------------------------------------------------------------------------
/*****************************************************************************************
  $Header XXEIS.XXWC_ACTDIR_CONTRACTOR_MNGR_V.vw $
  Module Name: EiS eXpress Administrator
  PURPOSE: Oracle User Access Review - WC
  REVISIONS:
  Ver          Date        Author                Description
  ---------  ----------  ------------------    ----------------
  1.0		 8/02/2018      Siva			   TMS#20180710-00075
******************************************************************************************/
---------------------------------------------------------------------------------------------
CREATE OR REPLACE VIEW APPS.XXWC_ACTDIR_CONTRACTOR_MNGR_V (CONTRACTORS_NTID, CONTRACTOR_ID, CONTRACTOR_LAST_NAME, CONTRACTOR_FIRST_NAME, CONTRACTOR_JOB_TITLE, WORK_PHONE, WORK_PHONE_EXTENSION, CONTRACTOR_COMPANY, MANAGER_ID, MANAGER_FIRST_NAME, MANAGER_LAST_NAME, SPVR_EMAIL_ADDR)
AS
  SELECT contractors.ntid contractors_ntid,
    TO_CHAR(to_number(regexp_replace(contractors.ntid, '[^0-9]+' ))) contractor_id,
    contractors.last_name contractor_last_name,
    contractors.first_name contractor_first_name,
    contractors.job_descr contractor_job_title,
    contractors.work_phone work_phone,
    contractors.work_ext work_phone_extension,
    contractors.contractorcompany contractor_company,
    CASE
      WHEN spvr_emplid <> '00000'
      THEN TO_CHAR(to_number(regexp_replace(contractors.spvr_emplid, '[^0-9]+' )))
      ELSE TO_CHAR(to_number(regexp_replace(contractors.spvr_ntid, '[^0-9]+' )))
    END manager_id,
    contractors.spvr_first_name manager_first_name,
    contractors.spvr_last_name manager_last_name,
    contractors.spvr_email_addr
  FROM hdscmmn.hr_ad_all_vw@eaapxprd.hsi.hughessupply.com contractors
  LEFT OUTER JOIN hdscmmn.hr_employee_all_vw@eaapxprd.hsi.hughessupply.com emp
  ON to_number(regexp_replace(contractors.ntid, '[^0-9]+' ))= emp.emplid
  LEFT OUTER JOIN hdscmmn.et_location_code@eaapxprd.hsi.hughessupply.com et
  ON contractors.fru = et.fru
  LEFT OUTER JOIN hdscmmn.concur_bu_xref@eaapxprd.hsi.hughessupply.com control_table
  ON control_table.business_unit = contractors.ps_business_unit
  LEFT OUTER JOIN hdscmmn.concur_fru_override@eaapxprd.hsi.hughessupply.com fruo
  ON contractors.fru = fruo.fru
  LEFT OUTER JOIN hdscmmn.concur_emp_override@eaapxprd.hsi.hughessupply.com empo
  ON emp.emplid                                                     = empo.emplid
  WHERE 1                                                           =1
  AND contractors.usertype                                          ='Contractor'
  AND contractors.email_addr                                       IS NOT NULL
  AND to_number(regexp_replace(contractors.ntid, '[^0-9]+' ))      IS NOT NULL
  AND to_number(regexp_replace(contractors.spvr_ntid, '[^0-9]+' )) IS NOT NULL
  AND ( ( ( TRUNC(contractors.termination_dt) + 90 )               >= TRUNC(sysdate)
  AND control_table.business_unit                                  <> 'WW1US')
  OR (contractors.user_acct_status_code                             = 'A'
  AND control_table.business_unit                                  <> 'WW1US') )
  AND (control_table.business_unit                                 IS NOT NULL
  OR fruo.fru                                                      IS NOT NULL)
  AND et.entrp_entity                                              <> 'PPH'
/
COMMENT ON TABLE APPS.XXWC_ACTDIR_CONTRACTOR_MNGR_V IS 'TMS: 20180710-00075'
/
GRANT SELECT ON APPS.XXWC_ACTDIR_CONTRACTOR_MNGR_V TO XXCUS
/
GRANT SELECT ON APPS.XXWC_ACTDIR_CONTRACTOR_MNGR_V TO XXWC
/  