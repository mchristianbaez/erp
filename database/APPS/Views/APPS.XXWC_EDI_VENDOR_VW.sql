CREATE OR REPLACE FORCE VIEW "APPS"."XXWC_EDI_VENDOR_VW" ("VENDOR_ID", "VENDOR_NUMBER", "VENDOR_NAME", "PAYMENT_TERM", "PARTY_ID", "PARTY_SITE_NAME", "PARTY_SITE_NUMBER", "SITE_USE_TYPE", "STATUS", "ADDRESS1", "ADDRESS2", "CITY", "STATE", "POSTAL_CODE") AS 
  (SELECT DISTINCT v.vendor_id
               ,v.segment1    vendor_number
               ,v.vendor_name
               ,t.name        payment_term
               ,v.party_id
               ,s.PARTY_SITE_NAME
               ,s.PARTY_SITE_NUMBER
               ,s.SITE_USE_TYPE
               ,s.STATUS
               ,s.ADDRESS1
               ,s.ADDRESS2
               ,s.CITY
               ,s.STATE
               ,s.POSTAL_CODE
  FROM apps.po_vendors  v
      ,apps.ap_terms_vl t
      ,hz_party_sites_v s
 WHERE s.party_id = v.party_id
   AND v.terms_id = t.term_id)
;
  GRANT SELECT ON "APPS"."XXWC_EDI_VENDOR_VW" TO "INTERFACE_APEXWC";
