/* Formatted on 25-Feb-2013 13:21:15 (QP5 v5.206) */
-- Start of DDL Script for View APPS.XXWC_CUSTOMER_ATTR_UPDATE
-- Generated 25-Feb-2013 13:21:08 from APPS@EBIZFQA

CREATE OR REPLACE VIEW apps.xxwc_customer_attr_update
(
    prof_rowid
   ,cust_rowid
   ,site_uses_rowid
   ,cust_account_id
   ,cust_account_profile_id
   ,cust_acct_site_id
   ,site_use_id
   ,party_site_party_id
   ,lv
   ,account_number
   ,profile_party_id
   ,account_name
   ,account_address
   ,site_address
   ,state
   ,party_site_number
   ,account_status
   ,profile_account_status
   ,account_freight_term
   ,collector_id
   ,collector_name
   ,credit_analyst_id
   ,credit_analyst_name
   ,credit_checking
   ,credit_hold
   ,remmit_to_code_attribute2
   ,account_payment_terms
   ,standard_terms_name
   ,standard_terms_description
   ,credit_classification
   ,profile_class_id
   ,profile_class_name
   ,credit_classification_meaning
   ,account_status_meaning
   ,purposes
   ,site_use_code
   ,primary_flag
   ,acct_site_bill_to_flag
   ,acct_site_ship_to_flag
   ,site_status
   ,location
   ,org_id
   ,primary_salesrep_id
   ,salesreps_name
   ,site_freight_terms
   ,acct_site_party_site_id
   ,acct_site_status
   ,site_uses_creation_date
   ,level_flag
)
AS
    (SELECT prof.ROWID prof_rowid
           ,cust.ROWID cust_rowid
           ,site_uses.ROWID site_uses_rowid
           ,cust.cust_account_id
           ,prof.cust_account_profile_id
           ,acct_site.cust_acct_site_id
           ,site_uses.site_use_id site_use_id
           ,party_site.party_id party_site_party_id
           ,NVL2 (TO_CHAR (prof.site_use_id), 'Site', 'Account') lv
           ,cust.account_number
           ,prof.party_id profile_party_id
           ,cust.account_name
           ,arh_addr_pkg.format_address (NULL
                                        ,party.address1
                                        ,party.address2
                                        ,party.address3
                                        ,party.address4
                                        ,party.city
                                        ,party.county
                                        ,party.state
                                        ,party.province
                                        ,party.postal_code
                                        ,NULL)
                account_address
           ,NVL (arh_addr_pkg.format_address (loc.address_style
                                             ,loc.address1
                                             ,loc.address2
                                             ,loc.address3
                                             ,loc.address4
                                             ,loc.city
                                             ,loc.county
                                             ,loc.state
                                             ,loc.province
                                             ,loc.postal_code
                                             ,NULL)
                ,arh_addr_pkg.format_address (NULL
                                             ,party.address1
                                             ,party.address2
                                             ,party.address3
                                             ,party.address4
                                             ,party.city
                                             ,party.county
                                             ,party.state
                                             ,party.province
                                             ,party.postal_code
                                             ,NULL))
                site_address
           ,NVL (loc.state, party.state) state
           ,party_site.party_site_number
           ,prof.account_status
           ,cust.status profile_account_status
           ,cust.freight_term account_freight_term
           ,prof.collector_id
           ,col.name collector_name
           ,prof.credit_analyst_id
           ,xxwc_ar_customer_maint_form.credit_analyst (prof.credit_analyst_id) credit_analyst_name
           ,prof.credit_checking
           ,prof.credit_hold
           ,prof.attribute2 remmit_to_code_attribute2
           ,prof.standard_terms account_payment_terms
           ,term.name standard_terms_name
           ,term.description standard_terms_description
           ,prof.credit_classification
           ,pc.profile_class_id
           ,pc.name profile_class_name
           ,arpt_sql_func_util.get_lookup_meaning ('AR_CMGT_CREDIT_CLASSIFICATION', prof.credit_classification)
                credit_classification_meaning
           ,arpt_sql_func_util.get_lookup_meaning ('ACCOUNT_STATUS', prof.account_status) account_status_meaning
           --,NVL (xxwc_ar_customer_maint_form.get_acct_site_purposes (acct_site.cust_acct_site_id), 'Account') purposes
           ,NVL (xxwc_ar_customer_maint_form.get_site_purposes (site_uses.site_use_id), 'Account') purposes
           ,site_uses.site_use_code
           ,site_uses.primary_flag
           ,acct_site.bill_to_flag acct_site_bill_to_flag
           ,acct_site.ship_to_flag acct_site_ship_to_flag
           ,site_uses.status site_status
           ,site_uses.location
           ,site_uses.org_id
           ,site_uses.primary_salesrep_id
           ,xxwc_ar_customer_maint_form.get_sales_rep (site_uses.org_id, site_uses.primary_salesrep_id) salesreps_name
           ,site_uses.freight_term site_freight_terms
           ,acct_site.party_site_id acct_site_party_site_id
           ,acct_site.status acct_site_status
           ,site_uses.creation_date site_uses_creation_date
           ,'P' level_flag
       FROM hz_cust_accounts cust
           ,hz_customer_profiles prof
           ,hz_cust_profile_classes pc
           ,(SELECT *
               FROM apps.hz_cust_site_uses s
              WHERE s.org_id = mo_global.get_current_org_id) site_uses  -- 29/09/2014 Removed org_id and added mo_global.get_current_org_id  by pattabhi for Canada & US OU Testing 
           ,apps.hz_cust_acct_sites acct_site
           ,hz_party_sites party_site
           ,hz_locations loc
           ,hz_parties party
           ,ar_collectors col
           ,ra_terms term
      WHERE     cust.cust_account_id = prof.cust_account_id
            AND prof.profile_class_id = pc.profile_class_id
            AND prof.site_use_id = site_uses.site_use_id(+)
            AND acct_site.cust_acct_site_id(+) = site_uses.cust_acct_site_id
            AND loc.location_id(+) = party_site.location_id
            AND acct_site.party_site_id = party_site.party_site_id(+)
            AND cust.party_id = party.party_id
            AND prof.collector_id = col.collector_id(+)
            AND prof.standard_terms = term.term_id(+)
     UNION
     SELECT prof.ROWID prof_rowid
           ,cust.ROWID cust_rowid
           ,site_uses.ROWID site_uses_rowid
           ,cust.cust_account_id
           ,prof.cust_account_profile_id
           ,acct_site.cust_acct_site_id
           ,site_uses.site_use_id site_use_id
           ,party_site.party_id party_site_party_id
           ,NVL2 (TO_CHAR (site_uses.site_use_id), 'Site', 'Account') lv
           -- ,NVL2 (TO_CHAR (prof.site_use_id), 'Site', 'Account') lv
           ,cust.account_number
           ,prof.party_id profile_party_id
           ,cust.account_name
           ,arh_addr_pkg.format_address (NULL
                                        ,party.address1
                                        ,party.address2
                                        ,party.address3
                                        ,party.address4
                                        ,party.city
                                        ,party.county
                                        ,party.state
                                        ,party.province
                                        ,party.postal_code
                                        ,NULL)
                account_address
           ,NVL (arh_addr_pkg.format_address (loc.address_style
                                             ,loc.address1
                                             ,loc.address2
                                             ,loc.address3
                                             ,loc.address4
                                             ,loc.city
                                             ,loc.county
                                             ,loc.state
                                             ,loc.province
                                             ,loc.postal_code
                                             ,NULL)
                ,arh_addr_pkg.format_address (NULL
                                             ,party.address1
                                             ,party.address2
                                             ,party.address3
                                             ,party.address4
                                             ,party.city
                                             ,party.county
                                             ,party.state
                                             ,party.province
                                             ,party.postal_code
                                             ,NULL))
                site_address
           ,NVL (loc.state, party.state) state
           ,party_site.party_site_number
           ,prof.account_status
           ,cust.status profile_account_status
           ,cust.freight_term account_freight_term
           ,prof.collector_id
           ,col.name collector_name
           ,prof.credit_analyst_id
           ,xxwc_ar_customer_maint_form.credit_analyst (prof.credit_analyst_id) credit_analyst_name
           ,prof.credit_checking
           ,prof.credit_hold
           ,prof.attribute2 remmit_to_code_attribute2
           ,prof.standard_terms account_payment_terms
           ,term.name standard_terms_name
           ,term.description standard_terms_description
           ,prof.credit_classification
           ,pc.profile_class_id
           ,pc.name profile_class_name
           ,                                                                                                  --meanings
            arpt_sql_func_util.get_lookup_meaning ('AR_CMGT_CREDIT_CLASSIFICATION', prof.credit_classification)
                credit_classification_meaning
           ,arpt_sql_func_util.get_lookup_meaning ('ACCOUNT_STATUS', prof.account_status) account_status_meaning
           ,NVL (xxwc_ar_customer_maint_form.get_site_purposes (site_uses.site_use_id), 'Account') purposes
           --,NVL (xxwc_ar_customer_maint_form.get_acct_site_purposes (acct_site.cust_acct_site_id), 'Account') purposes
           ,site_uses.site_use_code
           ,site_uses.primary_flag
           ,acct_site.bill_to_flag acct_site_bill_to_flag
           ,acct_site.ship_to_flag acct_site_ship_to_flag
           ,site_uses.status site_status
           ,site_uses.location
           ,site_uses.org_id
           ,site_uses.primary_salesrep_id
           ,xxwc_ar_customer_maint_form.get_sales_rep (site_uses.org_id, site_uses.primary_salesrep_id) salesreps_name
           ,site_uses.freight_term site_freight_terms
           ,acct_site.party_site_id acct_site_party_site_id
           ,acct_site.status acct_site_status
           ,site_uses.creation_date site_uses_creation_date
           ,'S' level_flag
       FROM hz_cust_accounts cust
           ,hz_customer_profiles prof
           ,hz_cust_profile_classes pc
           --,HZ_CUST_SITE_USES_ALL SITE_USES
           ,(SELECT *
               FROM apps.hz_cust_site_uses s
              WHERE s.org_id = mo_global.get_current_org_id) site_uses  -- 29/09/2014 Removed org_id and added mo_global.get_current_org_id  by pattabhi for Canada & US OU Testing
           ,apps.hz_cust_acct_sites acct_site
           ,hz_party_sites party_site
           ,hz_locations loc
           ,hz_parties party
           ,ar_collectors col
           ,ra_terms term
      WHERE     cust.cust_account_id = prof.cust_account_id                                             --**************
            AND prof.profile_class_id = pc.profile_class_id
            AND prof.site_use_id IS NULL
            AND acct_site.cust_account_id = cust.cust_account_id
            -- AND prof.site_use_id = site_uses.site_use_id(+)
            AND acct_site.cust_acct_site_id = site_uses.cust_acct_site_id
            AND loc.location_id(+) = party_site.location_id
            AND acct_site.party_site_id = party_site.party_site_id(+)
            AND cust.party_id = party.party_id                                                          --**************
            AND prof.collector_id = col.collector_id(+)
            AND prof.standard_terms = term.term_id(+)                                                             --test
            AND NOT EXISTS
                    (SELECT 'X'
                       FROM hz_customer_profiles rt
                      WHERE rt.site_use_id = site_uses.site_use_id AND rt.cust_account_id = prof.cust_account_id))
/

-- End of DDL Script for View APPS.XXWC_CUSTOMER_ATTR_UPDATE
