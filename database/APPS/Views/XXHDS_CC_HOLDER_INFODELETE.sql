
  CREATE OR REPLACE FORCE VIEW "APPS"."XXHDS_CC_HOLDER_INFODELETE" ("EMPLOYEE_ID", "MASKED_CARD_NUMBER", "CARDMEMBER_NAME", "CARD_PROGRAM_NAME", "CARDHOLDER_EMP_NAME", "CARDHOLDER_ACT_TERM_DATE", "EMPLOYEE_NUMBER", "CARD_ID") AS 
  SELECT card.employee_id employee_id,
       '********' || substr(card.card_number, -4) masked_card_number,
       card.cardmember_name cardmember_name,
       cprog.card_program_name card_program_name,
       papf.full_name cardholder_emp_name,
       empterm.actual_termination_date cardholder_act_term_date,
       papf.employee_number,
       card.card_id
  FROM ap.ap_cards_all card,
       ap.ap_card_programs_all cprog,
       hr.per_all_people_f papf,
       (SELECT distinct pps.person_id,
                        pps.actual_termination_date,
                        MAX(pps.date_start)
          FROM hr.per_periods_of_service pps
         where PPS.PERSON_ID NOT IN
               (SELECT pps.person_id
                  FROM hr.per_periods_of_service pps
                 WHERE pps.ACTUAL_TERMINATION_DATE IS NULL)
          -- AND pps.last_standard_process_date is null
         GROUP BY person_id, pps.actual_termination_date) empterm
 WHERE card.employee_id = papf.person_id
   AND papf.person_id = empterm.person_id(+)
   AND card.card_program_id = cprog.card_program_id
   AND papf.effective_end_date = '31-DEC-4712'

;
