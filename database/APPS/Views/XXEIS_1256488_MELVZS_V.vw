CREATE OR REPLACE FORCE VIEW apps.xxeis_1256488_melvzs_v
(
   party_id
  ,account_number
  ,account_name
  ,account_status
  ,send_statements
  ,attribute5
  ,creation_date
  ,party_number
  ,primary_phone_area_code
  ,attribute10
)
AS
   SELECT DISTINCT a.party_id
                  ,a.account_number
                  ,a.account_name
                  ,c.account_status
                  ,c.send_statements
                  ,a.attribute5
                  ,a.creation_date
                  ,b.party_number
                  ,b.primary_phone_area_code
                  ,a.attribute10
     FROM ar.hz_cust_accounts a, ar.hz_parties b, ar.hz_customer_profiles c
    WHERE a.party_id = b.party_id AND a.cust_account_id = c.cust_account_id
--and a.attribute5 is null
;
