
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUS_PEOPLESOFT_LOC_VW" ("LOCATION_CODE", "EFFECTIVE_DATE", "STATUS", "LOC_DESCR", "DESCRSHORT", "COUNTRY", "ADDRESS_LINE1", "ADDRESS_LINE2", "CITY", "COUNTY", "STATE", "ZIPCODE", "TIME_ZONE", "LAST_UPDATE_DATE", "OTSELLAREA", "BLDG_LATITUDE", "BLDG_LONGITUDE") AS 
  SELECT DISTINCT
            loc.location_code,
            CASE TO_CHAR (ld.active_end_date, 'mm/dd/yyyy')
               WHEN '12/31/4712' THEN ld.active_start_date
               ELSE ld.active_end_date
            END
               effective_date,
            DECODE (TO_CHAR (loc.active_end_date, 'mm/dd/yyyy'),
                    '12/31/4712', 'A',
                    'I')
               status,
            addr.city || ' ' || addr.state || addr.province loc_descr,
            SUBSTR (NVL (addr.state || addr.province || ' ' || addr.city, ' '),
                    1,
                    10)
               descrshort,
            NVL (addr.country, ' ') county,
            NVL (addr.address_line1, ' ') address_line1,
            NVL (addr.address_line2, ' ') address_line2,
            NVL (addr.city, ' ') city,
            NVL (addr.county, ' ') county,
            NVL (addr.state || addr.province, ' ') state_province,
            NVL (addr.zip_code, ' ') zip_code,
            NVL (prop_zone.meaning, ' ') time_zone,
            loc.last_update_date,
            NVL (prop.attribute5, ' ') OTSELLAREA,     --Added ESMS SR 197137,
            loc.attribute2,
            loc.attribute1
       FROM pn.pn_properties_all prop,
            pn.pn_locations_all loc,
            (SELECT meaning, lookup_code
               FROM apps.fnd_lookup_values
              WHERE lookup_type = 'PN_ZONE_TYPE') prop_zone,
            pn.pn_addresses_all addr,
            (  SELECT location_id,
                      MAX (l.active_start_date) active_start_date,
                      MAX (l.active_end_date) active_end_date
                 FROM pn.pn_locations_all l
             GROUP BY location_id) ld
      WHERE     ld.location_id = loc.location_id
            AND loc.property_id = prop.property_id
            AND ld.active_start_date = loc.active_start_date
            AND loc.address_id = addr.address_id
            AND prop.zone = prop_zone.lookup_code(+)
            AND loc.location_type_lookup_code = 'BUILDING'
   ORDER BY loc.location_code;
