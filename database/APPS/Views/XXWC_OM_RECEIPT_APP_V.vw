--
-- XXWC_OM_RECEIPT_APP_V  (View)
--

CREATE OR REPLACE FORCE VIEW APPS.XXWC_OM_RECEIPT_APP_V
(
   CASH_RECEIPT_ID
  ,CUSTOMER_NUMBER
  ,TRX_TYPE_NAME
  ,TRX_NUMBER
  ,AMOUNT_APPLIED
  ,APPLY_DATE
)
AS
   SELECT a.cash_receipt_id
         ,a.customer_number
         ,a.trx_type_name
         ,a.trx_number
         ,a.amount_applied
         ,a.apply_date
     /*used in cash refunds from from OM*/
     FROM ar_receivable_applications_v a;


