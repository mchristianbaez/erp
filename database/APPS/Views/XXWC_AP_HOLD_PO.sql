/* Formatted on 2/28/2014 8:31:53 PM (QP5 v5.206) */
-- Start of DDL Script for View APPS.XXWC_AP_HOLD_PO
-- Generated 2/28/2014 8:31:51 PM from APPS@ebizrnd

-- Drop the old instance of XXWC_AP_HOLD_PO
DROP VIEW apps.xxwc_ap_hold_po
/

CREATE OR REPLACE VIEW apps.xxwc_ap_hold_po
(
    po_number
   ,supplier_name
   ,vendor_id
   ,shipto_org
   ,days_on_hold
   ,branch_manager
   ,buyer_role
   ,buyer_name
   ,recipient_role
   ,invoice_id
)
AS
    (  SELECT DISTINCT po_number
                      ,supplier_name
                      ,vendor_id
                      ,shipto_org
                      ,MAX (TRUNC (SYSDATE) - TRUNC (wf_item_start_date)) days_on_hold
                      ,branch_man_name branch_manager
                      ,buyer_role
                      ,buyer_name
                      ,original_recipient
                      ,invoice_id
         FROM apps.xxwc_ap_hold_notifications
        WHERE notification_status IN ('XXWC', 'OPEN')
     GROUP BY po_number
             ,supplier_name
             ,vendor_id
             ,shipto_org
             ,branch_man_name
             ,buyer_role
             ,buyer_name
             ,original_recipient
             ,invoice_id)
/

-- End of DDL Script for View APPS.XXWC_AP_HOLD_PO
