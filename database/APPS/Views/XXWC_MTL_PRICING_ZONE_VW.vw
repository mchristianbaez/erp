--
-- XXWC_MTL_PRICING_ZONE_VW  (View)
--

CREATE OR REPLACE FORCE VIEW APPS.XXWC_MTL_PRICING_ZONE_VW
(
   PRICING_ZONE
)
AS
   SELECT a.flex_value
     FROM fnd_flex_values_vl a, fnd_flex_value_sets b
    WHERE     a.flex_value_set_id = b.flex_value_set_id
          AND b.flex_value_set_name = 'WC_PRICE_ZONES'
          AND a.enabled_flag = 'Y'
          AND SYSDATE BETWEEN NVL (start_date_active, SYSDATE - 1)
                          AND NVL (end_date_active, SYSDATE + 1);


