CREATE OR REPLACE FORCE VIEW APPS.XXWCAP_INVOICES_ALL_VW
(
   INVOICE_ID,
   VENDOR_NAME,
   VENDOR_NUMBER,
   VENDOR_SITE_CODE,
   INVOICE_NUMBER,
   INVOICE_AMOUNT,
   INVOICE_DATE,
   SOURCE,
   PAYMENT_STATUS
)
AS
   SELECT i.invoice_id,
          v.vendor_name,
          v.attribute1,
          s.vendor_site_code,
          i.invoice_num,
          i.invoice_amount,
          i.invoice_date,
          i.source,
          i.payment_status_flag
     FROM apps.ap_invoices i, apps.ap_suppliers v, APPS.ap_supplier_sites s
    WHERE     i.vendor_id = v.vendor_id
          AND i.vendor_site_id = s.vendor_site_id;
         -- AND i.org_id = 162; -- 30/09/2014 Commented by pattabhi for Canada & US OU Testing 


GRANT SELECT ON APPS.XXWCAP_INVOICES_ALL_VW TO INTERFACE_APEXWC;