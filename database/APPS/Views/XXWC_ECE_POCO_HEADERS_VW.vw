CREATE OR REPLACE FORCE VIEW APPS.XXWC_ECE_POCO_HEADERS_VW
/*************************************************************************
  $Header XXWC_ECE_POCO_HEADERS_VW.vw $
  Module Name : XXWC_ECE_POCO_HEADERS_VW

  PURPOSE     : View to list the purchase orders ready for POCO EDI transmission
  TMS Task Id : 20141218-00011

  REVISIONS:
  Ver        Date        Author                Description
  ---------  ----------  ------------------    ----------------
  1.0        04-Feb-2015  Manjula Chellappan    Initial Version
**************************************************************************/
(
   PO_HEADER_ID,
   PO_NUMBER,
   REVISION_NUM,
   CREATION_DATE,
   LAST_UPDATE_DATE,
   APPROVED_DATE,
   PRINTED_DATE,
   PRINT_COUNT,
   EDI_PROCESSED_FLAG,
   VENDOR_ID,
   VENDOR_NUM,
   VENDOR_NAME,
   VENDOR_SITE_ID,
   EDI_LOCATION_CODE,
   VENDOR_SITE_CODE
)
AS
   SELECT ph.po_header_id,
          ph.segment1 po_number,
          ph.revision_num,
          ph.creation_date,
          ph.last_update_date,
          ph.approved_date,
          pha.printed_date,
          pha.print_count,
          pha.edi_processed_flag,
          aps.vendor_id,
          aps.segment1 vendor_num,
          aps.vendor_name vendor_name,
          ass.vendor_site_id,
          ass.ece_tp_location_code edi_location_code,
          ass.vendor_site_code vendor_site_code
     FROM po_headers ph,
          po_headers_archive pha,
          ap_suppliers aps,
          ap_supplier_sites ass
    WHERE     ph.vendor_id = aps.vendor_id
          AND aps.vendor_id = ass.vendor_id
          AND ph.vendor_site_id = ass.vendor_site_id
          AND ph.edi_processed_flag = 'Y'
          AND NVL (ph.print_count, 0) > 0
          AND NVL (ph.authorization_status, 'X') = 'APPROVED'
          AND NVL (ph.edi_processed_flag, 'N') = 'Y'
          AND NVL (pha.edi_processed_flag, 'N') = 'N'
          AND NVL (ph.approved_flag, 'N') = 'Y'
          AND NVL (ph.user_hold_flag, 'N') = 'N'
          AND pha.latest_external_flag = 'Y'
          AND ph.po_header_id = pha.po_header_id
          AND ph.revision_num = pha.revision_num;