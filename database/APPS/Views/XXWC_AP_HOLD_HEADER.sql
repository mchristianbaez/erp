/* Formatted on 2/28/2014 8:28:55 PM (QP5 v5.206) */
-- Start of DDL Script for View APPS.XXWC_AP_HOLD_HEADER
-- Generated 2/28/2014 8:28:52 PM from APPS@ebizrnd

-- Drop the old instance of XXWC_AP_HOLD_HEADER
DROP VIEW apps.xxwc_ap_hold_header
/

CREATE OR REPLACE VIEW apps.xxwc_ap_hold_header
(
    agent_id
   ,buyer_role
   ,buyer_name
   ,po_number
   ,invoice_id
   ,hdr_invoice_total
   ,supplier_name
   ,item_lines_total
   ,shipto_org
   ,invoice_date
   ,invoice_num
   ,recipient_role
   ,recepient_name
   ,branch_manager
)
AS
    SELECT DISTINCT agent_id
                   ,buyer_role
                   ,buyer_name
                   ,po_number
                   ,invoice_id
                   ,hdr_invoice_total
                   ,supplier_name
                   ,item_lines_total
                   ,shipto_org
                   ,invoice_date
                   ,invoice_num
                   ,original_recipient
                   ,recepient_name
                   ,branch_manager
      FROM apps.xxwc_ap_hold_notifications
     WHERE notification_status IN ('XXWC', 'OPEN')
/

-- End of DDL Script for View APPS.XXWC_AP_HOLD_HEADER
