
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUSOZF_BRANCH_ACCRUALS_VW" ("LOB", "PERIOD", "BRANCH", "REBATE_INCOME", "COOP_INCOME", "TOTAL_INCOME") AS 
  SELECT lob
, period
, branch
, SUM(rebate_income) rebate_income,
SUM(coop_income) coop_income,
SUM(rebate_income) + SUM(coop_income) total_income
FROM (select hp.party_name lob
, period.name period
,substr(hca.account_number,instr(hca.account_number,'.',-1,1 )+1) branch
, a.accrued_amount rebate_income
,0 coop_income
from xxcusozf_accruals_mv a,
hz_cust_accounts hca,
hz_parties hp,
ozf_time_ent_period period
WHERE 1=1
AND grp_branch = 0
AND grp_lob=0
AND grp_rebate_type=0
AND grp_period = 0
AND grp_qtr=1
AND grp_mvid=1
AND grp_year=1
AND grp_adj_type=1
and grp_plan_id = 1
and grp_cal_year = 1
AND nvl(a.rebate_type_id,0) NOT IN ('COOP','COOP_MIN')
AND a.period_id=period.ent_period_id
AND a.lob_id=hca.party_id
AND a.branch_id=hca.cust_account_id
AND hca.party_id=hp.party_id
UNION select hp.party_name lob
, period.name period
,substr(hca.account_number,instr(hca.account_number,'.',-1,1 )+1) branch
, 0 rebate_income
,a.accrued_amount coop_income
from xxcusozf_accruals_mv a,
hz_cust_accounts hca,
hz_parties hp,
ozf_time_ent_period period
WHERE 1=1
AND grp_branch = 0
AND grp_lob=0
AND grp_rebate_type=0
AND grp_period = 0
AND grp_qtr=1
AND grp_mvid=1
AND grp_year=1
AND grp_adj_type=1
and grp_plan_id = 1
and grp_cal_year = 1
AND a.rebate_type_id IN ('COOP','COOP_MIN')
AND a.period_id=period.ent_period_id
AND a.lob_id=hca.party_id
AND a.branch_id=hca.cust_account_id
AND hca.party_id=hp.party_id
)
GROUP BY lob
, period
, branch
ORDER BY 1,3
;
