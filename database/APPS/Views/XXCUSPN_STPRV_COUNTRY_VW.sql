
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUSPN_STPRV_COUNTRY_VW" ("STPRV", "STPRV_DESCRIPTION", "COUNTRY") AS 
  SELECT distinct state stprv,
           t.meaning stprv_description, 'US' country
    FROM pn_addresses_all a, fnd_lookup_values t
    WHERE a.state = t.lookup_code
    AND   t.lookup_type = 'US_STATE'
    AND   a.country = 'US'
    UNION ALL
    SELECT distinct lookup_code code,
           t.meaning state, a.country
    FROM pn_addresses_all a, fnd_lookup_values t
    WHERE upper(a.province) = upper(t.lookup_code)
    AND   t.lookup_type = 'PN_STATE'
    AND   a.country IN ('CA')
;
