CREATE OR REPLACE VIEW XXWC_EGO_SUPPLIERS_ACTV_DCT_VW
/*********************************************************************************
File Name: XXWC_EGO_SUPPLIERS_ACTV_DCT_VW
PROGRAM TYPE: View definition
HISTORY
PURPOSE: This view is created to filter out inactive suppliers and select only distint Supplier to 
         get rid of duplicate suppliers because they are enabled at different org level in view
		 XXWC_EGO_SUPPLIERS_ACTV_VW. Tihs view was created as workaround to fix the error cause in
		 item import program in PDH.
         TMS Ticker # 20140709-00148
==================================================================================
VERSION DATE          AUTHOR(S)         DESCRIPTION
------- -----------   --------------- --------------------------------------------
1.0     19-AUG-2014   KPIT        Initial view definition TMS Ticker # 20140709-00148
*********************************************************************************/
AS
   SELECT DISTINCT vendor_info, vendor_number
     FROM apps.XXWC_EGO_SUPPLIERS_ACTV_VW;