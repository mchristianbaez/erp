CREATE OR REPLACE VIEW XXWC_VENDOR_SITES_VW AS
  SELECT ass.vendor_id,
         ass.vendor_name,
         ass.segment1 vendor_number,
         assa.vendor_site_id,
         assa.vendor_site_code,
         assa.purchasing_site_flag
  FROM   ap_suppliers ass,
         ap_supplier_sites_all assa
  WHERE  ass.vendor_id = assa.vendor_id
  AND    assa.org_id = FND_PROFILE.VALUE('ORG_ID')  -- Added this condition by Pattabhi based on Testing failed web ADI in EBIZFQA
  order by ass.vendor_name, assa.vendor_site_code;