
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUSAPEX_P2P_ACCOUNTS_VW" ("ORACLE_PRODUCT", "ORACLE_LOCATION", "ORACLE_DESCRIPTION", "LOB_LEGACY_BRANCH", "LEGACY_BRANCH") AS 
  SELECT DISTINCT l.entrp_entity oracle_product
               ,l.entrp_loc    oracle_location
               ,zz.description oracle_description
               ,l.lob_branch   lob_legacy_branch
               ,l.fru          legacy_branch
  FROM apps.xxcus_location_code_vw l
      ,(SELECT v.flex_value, tl.description
          FROM apps.fnd_flex_values        v
              ,applsys.fnd_flex_values_tl  tl
              ,applsys.fnd_flex_value_sets s
         WHERE flex_value_set_name = 'XXCUS_GL_LOCATION'
           AND v.flex_value_set_id = s.flex_value_set_id
           AND v.flex_value_id = tl.flex_value_id
           AND tl.language = 'US') zz
 WHERE l.entrp_loc = zz.flex_value
;
