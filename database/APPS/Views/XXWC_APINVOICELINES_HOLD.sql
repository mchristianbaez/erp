/* Formatted on 2/28/2014 8:32:39 PM (QP5 v5.206) */
-- Start of DDL Script for View APPS.XXWC_APINVOICELINES_HOLD
-- Generated 2/28/2014 8:32:37 PM from APPS@ebizrnd

-- Drop the old instance of XXWC_APINVOICELINES_HOLD
DROP VIEW apps.xxwc_apinvoicelines_hold
/

CREATE OR REPLACE VIEW apps.xxwc_apinvoicelines_hold
(
    hold_id
   ,hold_lookup_code
   ,hold_reason
   ,wf_status
   ,agent_id
   ,vendor_id
   ,org_id
   ,invoice_num
   ,invoice_date
   ,shipto_org
   ,invoice_id
   ,inv_line_number
   ,inv_line_amount
   ,inv_line_qty_invoiced
   ,uom
   ,inv_unit_price
   ,line_location_id
   ,last_updated_by
   ,disputable_flag
   ,line_type_lookup_code
   ,inventory_item_id
   ,inv_item_desc
   ,supplier_item_number
   ,vendor_item_desc
   ,matching_basis
   ,po_number
   ,quantity_overbilled
   ,quantity_not_received
   ,unit_price_variance
   ,amount_overbilled
   ,po_matched
   ,shipment_number
   ,po_line_number
   ,po_line_shipment_qty
   ,po_line_shipment_qty_rcved
   ,po_unit_price
   ,po_line_amount
   ,po_line_item_description
   ,po_line_need_by_date
   ,invoice_line_type
   ,original_amount
   ,original_quantity_invoiced
   ,original_unit_price
   ,po_distribution_id
   ,branch_manager
   ,ship_org_id
   ,notification_id
   ,notification_status
   ,recipient_role
   ,notif_mail_status
   ,notif_begin_date
   ,notif_end_date
   ,notif_due_date
   ,notif_subject
   ,responder
   ,item_key
   ,from_role
   ,lock_flag
)
AS
    (SELECT holds.hold_id
           ,holds.hold_lookup_code
           ,holds.hold_reason
           ,holds.wf_status
           ,ph.agent_id
           ,ph.vendor_id
           ,apinvoicelinesalleo.org_id
           ,ai.invoice_num
           ,ai.invoice_date
           ,NVL (loc.location_code
                , (SELECT location_code
                     FROM po_ship_to_loc_org_v
                    WHERE location_id = ph.ship_to_location_id AND ROWNUM = 1))
                shipto_org
           ,apinvoicelinesalleo.invoice_id
           ,apinvoicelinesalleo.line_number inv_line_number
           ,apinvoicelinesalleo.amount inv_line_amount
           ,apinvoicelinesalleo.quantity_invoiced inv_line_qty_invoiced
           ,apinvoicelinesalleo.unit_meas_lookup_code AS uom
           ,apinvoicelinesalleo.unit_price inv_unit_price
           ,apinvoicelinesalleo.po_line_location_id AS line_location_id
           ,apinvoicelinesalleo.last_updated_by
           ,apinvoicelinesalleo.disputable_flag
           ,apinvoicelinesalleo.line_type_lookup_code
           ,apinvoicelinesalleo.inventory_item_id
           ,apinvoicelinesalleo.item_description inv_item_desc
           ,pl.vendor_product_num supplier_item_number
           ,pl.item_description vendor_item_desc
           ,pl.matching_basis matching_basis
           ,ph.segment1 po_number
           ,DECODE (SIGN (pll.quantity_billed - pll.quantity), 1, (pll.quantity_billed - pll.quantity), NULL)
                quantity_overbilled
           , (pll.quantity - pll.quantity_received) quantity_not_received
           , (apinvoicelinesalleo.unit_price - pl.unit_price) unit_price_variance
           ,DECODE (SIGN (pll.amount_billed - pll.amount), 1, (pll.amount_billed - pll.amount), NULL) amount_overbilled
           ,DECODE (apinvoicelinesalleo.po_line_location_id, NULL, 'N', 'Y') po_matched
           ,pll.shipment_num shipment_number
           ,pl.line_num po_line_number
           ,pll.quantity po_line_shipment_qty
           ,pll.quantity_received po_line_shipment_qty_rcved
           ,pl.unit_price po_unit_price
           ,pl.unit_price * pll.quantity po_line_amount
           ,pl.item_description po_line_item_description
           ,pll.need_by_date po_line_need_by_date
           ,alc.displayed_field invoice_line_type
           ,apinvoicelinesalleo.amount AS original_amount
           ,apinvoicelinesalleo.quantity_invoiced AS original_quantity_invoiced
           ,apinvoicelinesalleo.unit_price AS original_unit_price
           ,apinvoicelinesalleo.po_distribution_id po_distribution_id
           ,apps.xxwc_ap_invoice_hold_notify.get_branch_manager (ph.ship_to_location_id) branch_manager
           , (SELECT organization_id
                FROM po_ship_to_loc_org_v
               WHERE location_id = ph.ship_to_location_id AND ROWNUM = 1)
                ship_org_id
           ,wfn.notification_id
           ,wfn.status notification_status
           ,wfn.recipient_role
           ,wfn.mail_status notif_mail_status
           ,wfn.begin_date notif_begin_date
           ,wfn.end_date notif_end_date
           ,wfn.due_date notif_due_date
           ,wfn.subject notif_subject
           ,wfn.responder
           ,wfn.item_key
           ,wfn.from_role
           ,0 lock_flag
       FROM apps.ap_invoice_lines apinvoicelinesalleo
           ,apps.po_line_locations pll
           ,apps.po_lines pl
           ,apps.po_headers ph
           ,ap_holds_all holds
           ,ap_lookup_codes alc
           ,rcv_transactions rcv
           ,apps.ap_invoices ai
           ,apps.hr_locations loc
           ,wf_notifications wfn
      WHERE     apinvoicelinesalleo.invoice_id = holds.invoice_id
            AND ai.invoice_id = apinvoicelinesalleo.invoice_id
            AND ai.cancelled_date IS NULL
            AND loc.location_id(+) = pll.ship_to_location_id
            AND NVL (apinvoicelinesalleo.discarded_flag, 'N') <> 'Y'
            AND apinvoicelinesalleo.line_type_lookup_code = alc.lookup_code
            AND alc.lookup_type = 'INVOICE LINE TYPE'
            AND holds.release_lookup_code IS NULL
            AND apinvoicelinesalleo.po_line_location_id = pll.line_location_id(+)
            AND pll.po_line_id = pl.po_line_id(+)
            AND pl.po_header_id = ph.po_header_id(+)
            AND apinvoicelinesalleo.rcv_transaction_id = rcv.transaction_id(+)
            AND apinvoicelinesalleo.po_line_location_id = holds.line_location_id
            AND apinvoicelinesalleo.match_type = 'ITEM_TO_PO'
            AND holds.wf_status = 'STARTED'
            AND wfn.MESSAGE_TYPE = 'APINVHDN'
            AND wfn.status IN ('OPEN', 'XXWC')
            AND NVL (wfn.mail_status, 'n') <> 'ERROR'
            AND wfn.message_name = 'POMATCHED_HLD_NOTIF_MSG'
            AND wfn.item_key = TO_CHAR (holds.hold_id))
/

-- End of DDL Script for View APPS.XXWC_APINVOICELINES_HOLD
