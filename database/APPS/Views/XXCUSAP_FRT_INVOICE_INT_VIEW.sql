
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUSAP_FRT_INVOICE_INT_VIEW" ("INVOICE_NUMBER", "CARRIER", "RECEIVED_DATE", "STATUS", "ERROR_MSG") AS 
  SELECT aii.invoice_num invoice_number
      ,sup.vendor_name
      ,aii.creation_date received_date
      ,aii.status
      ,(CASE aii.status
         WHEN 'REJECTED' THEN
          'Request ID: ' || aii.request_id || ' - ' ||
          air.reject_lookup_code
         ELSE
          NULL
       END) error_msg
  FROM ap.ap_invoices_interface aii
      ,ap_interface_rejections  air
      ,ap.ap_suppliers          sup
 WHERE sup.vendor_id = aii.vendor_id
   AND aii.invoice_id = air.parent_id(+)
   AND SOURCE = 'TMS'
;
