
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUS_VEND_LOV_V" ("MVID", "MV_NAME", "CUST_ID") AS 
  SELECT customer_attribute2 mvid, party_name mv_name, customer_id cust_id
    FROM xxcus.xxcus_rebate_customers
   WHERE 1 = 1
     AND party_attribute1 = 'HDS_MVID'
;
