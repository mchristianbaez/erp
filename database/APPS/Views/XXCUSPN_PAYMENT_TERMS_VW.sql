
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUSPN_PAYMENT_TERMS_VW" ("PAYMENT_PURPOSE", "PAYMENT_TERM_TYPE", "FREQUENCY_TYPE", "VENDOR_NAME", "VENDOR_NUMBER", "VENDOR_SITE", "LEASE_ID", "START_DATE", "END_DATE", "LOCATION", "ACTUAL_AMOUNT", "CURRENCY_CODE", "ESTIMATED_AMOUNT") AS 
  SELECT payment_purpose,
     payment_term_type,
     frequency_type,
     ca.company_name vendor_name,
     vendordff.attribute4 vendor_number,
     sitedff.attribute1 vendor_site,
     a.lease_id,
     a.start_date,
     a.end_date,
     (SELECT location_code FROM pn_locations_all l WHERE l.location_id = a.location_id AND   l.active_end_date = (SELECT MAX(mx.active_end_date) FROM apps.pn_locations_all mx WHERE mx.location_id = l.location_id)) location,
     to_char(actual_amount, '999,999,999.99') actual_amount, currency_code, to_char(estimated_amount, '999,999,999.99') estimated_amount
FROM   PN_PAYMENT_TERMS_V a,
       (SELECT DISTINCT lease_id, company_name, company_site_id, company_id FROM PN_LEASE_CONTACT_ASSIGN_V WHERE lease_role_type = 'PAYCONT') ca,
       (SELECT company_site_id, attribute1 FROM pn_company_sites_all) sitedff,
     (SELECT attribute4, company_id  FROM pn_companies_all) vendordff
WHERE  a.lease_id= ca.lease_id(+)
AND    sitedff.company_site_id(+) = ca.company_site_id
AND    vendordff.company_id(+) = ca.company_id
;
