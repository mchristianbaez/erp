--
-- XXWC_SR_FOR_ITEM_CLONING  (View)
--
--  Dependencies:
--   MTL_PARAMETERS (Synonym)
--   MTL_INTERORG_SHIP_METHODS (Synonym)
--   MRP_SOURCING_RULES (Synonym)
--   MRP_ASSIGNMENT_SETS (Synonym)
--   MRP_SR_RECEIPT_ORG (Synonym)
--   MRP_SR_ASSIGNMENTS_V (View)
--   PO_VENDORS (View)
--   PO_VENDOR_SITES_ALL (View)
--   MRP_GET_PROJECT (Package)
--   MFG_LOOKUPS (View)
--   MRP_SR_SOURCE_ORG (Synonym)
--

CREATE OR REPLACE FORCE VIEW xxwc_sr_for_item_cloning
(
    sourcing_rule_name,
    sourcing_rule_id,
    item_code,
    vendor_number,
    vendor_name,
    vendor_site_id,
    vendor_site_code,
    organization_id,
    inventory_item_id,
    sr_assignment_creation_date,
    sr_creation_date,
    region,
    district
)
AS
    SELECT msr.sourcing_rule_name,
           msr.sourcing_rule_id,
           msra.entity_name item_code,
           v.segment1 vendor_number,
           v.vendor_name,
           msso.vendor_site_id,
           vs.vendor_site_code,
           msra.organization_id,
           msra.inventory_item_id,
           msra.creation_date sr_assignment_creation_date,
           msr.creation_date sr_creation_date,
           mp.attribute9 region,
           mp.attribute8 district
      FROM apps.mrp_sr_assignments_v msra,
           mrp_assignment_sets mas,
           mrp_sourcing_rules msr,
           mrp_sr_source_org msso,
           ap_suppliers v,
           apps.ap_supplier_sites vs,
           mrp_sr_receipt_org sro,
           mtl_interorg_ship_methods sm,
           mtl_parameters mp,
           mfg_lookups mfglkp
     WHERE     msr.sourcing_rule_id = msra.sourcing_rule_id
           AND msra.assignment_set_id = mas.assignment_set_id
           AND mas.assignment_set_id = 1
           AND mfglkp.lookup_type = 'MRP_SOURCE_TYPE'
           AND msso.source_type = mfglkp.lookup_code
           AND mas.assignment_set_name = 'WC Default'
           AND v.vendor_id(+) = msso.vendor_id
           AND vs.vendor_site_id(+) = msso.vendor_site_id
           AND msr.sourcing_rule_id = sro.sourcing_rule_id
           AND sm.to_organization_id(+) = sro.receipt_organization_id
           AND sro.sr_receipt_id = msso.sr_receipt_id
           AND msso.ship_method IS NULL
           AND mp.attribute9 IS NOT NULL
           AND mp.organization_id = msra.organization_id
           AND msr.sourcing_rule_name NOT LIKE '88888%'
           AND msr.sourcing_rule_name NOT LIKE '99999%';
