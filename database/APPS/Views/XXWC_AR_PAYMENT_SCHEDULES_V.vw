/**************************************************************************
  File Name: XXWC_AR_PAYMENT_SCHEDULES_V

   PROGRAM TYPE: View Script

   PURPOSE: script to create Ar PAyment schedules data for AR lien Form.

   HISTORY
  =============================================================================
    VERSION DATE          AUTHOR(S)             DESCRIPTION
  --------- -----------   -------------------- -----------------------------------
    1.0     05/01/2017   Neha Saini           Task ID: 20170215-00038 Created this script.
  **************************************************************************/
DROP VIEW APPS.XXWC_AR_PAYMENT_SCHEDULES_V;

CREATE OR REPLACE FORCE VIEW APPS.XXWC_AR_PAYMENT_SCHEDULES_V
(
   TRX_NUMBER,
   TRX_DATE,
   CUSTOMER_ID,
   CUSTOMER_SITE_USE_ID,
   STATUS,
   SU_LOCATION,
   TYPE_NAME,
   CT_PURCHASE_ORDER,
   INTERFACE_HEADER_ATTRIBUTE1,
   DUE_DATE,
   AMOUNT_DUE_ORIGINAL,
   AMOUNT_DUE_REMAINING
)
AS
   SELECT PS.TRX_NUMBER,
          PS.TRX_DATE,
          PS.CUSTOMER_ID,
          PS.CUSTOMER_SITE_USE_ID,
          PS.STATUS,
          SU.LOCATION,
          CTT.NAME,
          CT.PURCHASE_ORDER,
          CT.INTERFACE_HEADER_ATTRIBUTE1,
          PS.DUE_DATE,
          PS.AMOUNT_DUE_ORIGINAL,
          PS.AMOUNT_DUE_REMAINING
     FROM apps.xxwc_ar_line_release_hdr_gt gt,
          ra_cust_trx_types_all ctt,
          ra_customer_trx_all ct,
          hz_cust_site_uses_all su,
          hz_cust_accounts cust_acct,
          ar_payment_schedules ps
    WHERE     1 = 1
          AND ps.customer_id = gt.customer_id
          AND ps.CUSTOMER_SITE_USE_ID =
                 NVL (gt.bill_to_site_use_id, gt.primary_bt_site_use_id)
          AND ps.status = 'OP'
          AND PS.CUSTOMER_ID = CUST_ACCT.CUST_ACCOUNT_ID
          AND PS.CUSTOMER_SITE_USE_ID = SU.SITE_USE_ID
          AND PS.CUSTOMER_TRX_ID = CT.CUSTOMER_TRX_ID
          AND CT.CUST_TRX_TYPE_ID = CTT.CUST_TRX_TYPE_ID
          AND CTT.ORG_ID = PS.ORG_ID
          AND CT.ORG_ID = PS.ORG_ID
          AND SU.ORG_ID = PS.ORG_ID
          AND PS.STATUS = NVL (ARP_VIEW_CONSTANTS.GET_STATUS, PS.STATUS)
   UNION
   SELECT PS.TRX_NUMBER,
          PS.TRX_DATE,
          PS.CUSTOMER_ID,
          PS.CUSTOMER_SITE_USE_ID,
          DECODE (AL_RISK_RECEIPT.LOOKUP_CODE, 'N', PS.STATUS, 'OP'), /* STATUS */
          SU.LOCATION,
          RM.NAME,
          NULL,
          NULL,
          PS.DUE_DATE,
          DECODE (AL_RISK_RECEIPT.LOOKUP_CODE,
                  'N', PS.AMOUNT_DUE_ORIGINAL,
                  -1 * ps.amount_due_original),      /* AMOUNT_DUE_ORIGINAL */
          DECODE (AL_RISK_RECEIPT.LOOKUP_CODE,
                  'N', PS.AMOUNT_DUE_REMAINING,
                  -1 * ps.amount_due_original)      /* AMOUNT_DUE_REMAINING */
     FROM apps.xxwc_ar_line_release_hdr_gt gt,
          ar_lookups al_risk_receipt,
          ar_receipt_methods rm,
          ar_cash_receipt_history_all crh_current,
          ar_cash_receipts_all cr,
          hz_cust_site_uses_all su,
          ar_payment_schedules ps,
          ar_receipt_classes rc
    WHERE     1 = 1
          AND ps.customer_id = gt.customer_id
          AND ps.CUSTOMER_SITE_USE_ID =
                 NVL (gt.bill_to_site_use_id, gt.primary_bt_site_use_id)
          AND PS.STATUS = 'OP'
          AND PS.CUSTOMER_SITE_USE_ID = SU.SITE_USE_ID(+)
          AND PS.CASH_RECEIPT_ID = CR.CASH_RECEIPT_ID
          AND CR.RECEIPT_METHOD_ID = RM.RECEIPT_METHOD_ID
          AND CR.CASH_RECEIPT_ID = CRH_CURRENT.CASH_RECEIPT_ID
          AND CRH_CURRENT.CURRENT_RECORD_FLAG = 'Y'
          AND rc.receipt_class_id = rm.receipt_class_id
          AND AL_RISK_RECEIPT.LOOKUP_TYPE = 'YES/NO'
          AND AL_RISK_RECEIPT.LOOKUP_CODE IN (DECODE (
                                                 NVL (
                                                    ARP_VIEW_CONSTANTS.GET_STATUS,
                                                    'X'),
                                                 'OP', 'X',
                                                 'N'),
                                              DECODE (
                                                 NVL (
                                                    ARP_VIEW_CONSTANTS.GET_INCL_RECEIPTS_AT_RISK,
                                                    'N'),
                                                 'N', 'N',
                                                 DECODE (
                                                    CRH_CURRENT.STATUS,
                                                    'REVERSED', 'N',
                                                    DECODE (
                                                       CRH_CURRENT.FACTOR_FLAG,
                                                       'N', DECODE (
                                                               CRH_CURRENT.STATUS,
                                                               'CLEARED', 'N',
                                                               DECODE (
                                                                  NVL (
                                                                     ARP_VIEW_CONSTANTS.GET_STATUS,
                                                                     'X'),
                                                                  'CL', 'X',
                                                                  'Y')),
                                                       DECODE (
                                                          CRH_CURRENT.STATUS,
                                                          'RISK_ELIMINATED', 'N',
                                                          DECODE (
                                                             NVL (
                                                                ARP_VIEW_CONSTANTS.GET_STATUS,
                                                                'X'),
                                                             'CL', 'X',
                                                             'Y'))))))
          AND NVL (ARP_VIEW_CONSTANTS.GET_SALES_ORDER, 0) = 0
          AND CRH_CURRENT.ORG_ID = PS.ORG_ID
          AND CR.ORG_ID = PS.ORG_ID
          AND PS.ORG_ID = SU.ORG_ID(+);