CREATE OR REPLACE VIEW XXWC_VENDOR_VW AS
  SELECT ass.vendor_id,
         ass.vendor_name,
         ass.segment1 vendor_number
  FROM   ap_suppliers ass
  ORDER BY ass.vendor_name;