--
-- ENG_XXWC_PIM_CR_BRAN_AGV  (View)
--

CREATE OR REPLACE FORCE VIEW apps.eng_xxwc_pim_cr_bran_agv
(
   extension_id
  ,change_id
  ,xxwc_branch_name
  ,xxwc_branch_name_disp
)
AS
   SELECT extension_id
         ,change_id
         ,c_ext_attr1 xxwc_branch_name
         , (SELECT DISTINCT organization_code
              FROM org_organization_definitions
             WHERE 1 = 1 AND organization_id = c_ext_attr1)
             xxwc_branch_name_disp
     FROM eng_changes_ext_vl
    WHERE attr_group_id = 221;


