CREATE OR REPLACE VIEW APPS.XXWC_MKTANLY_CUST_CON_EMAIL_VW
AS
  /********************************************************************************************************************************
  --   NAME:       apps.XXWC_MKTANLY_CUST_CON_EMAIL_VW
  -- REVISIONS:
  --   Ver        Date        Author           Description
  --   ---------  ----------  ---------------  ------------------------------------
  --   1.0        09/05/2018   P.Vamshidhar      TMS#20180907-00001 -  Building Customer Outbound MV's for Marketing Analytics
  **********************************************************************************************************************************/
  SELECT DISTINCT account_number "Account_Number" ,
    SUB.PARTY_ID contact_party_id,
    SUB.PERSON_FIRST_NAME First_Name ,
    SUB.PERSON_LAST_NAME Last_Name ,
    lower(hcp.email_address) email_address,
    hcp.primary_flag Primary_Email_Flag,
    hcp.contact_point_purpose contact_purpose,
    hcp.last_update_date Last_Update_Date,
    hcp.creation_date Created_Date
  FROM apps.hz_cust_accounts hca ,
    apps.hz_parties obj ,
    apps.hz_relationships rel ,
    apps.hz_contact_points hcp ,
    apps.hz_parties sub
  WHERE hca.party_id        = rel.object_id
  AND hca.party_id          = obj.party_id
  AND rel.subject_id        = sub.party_id
  AND rel.relationship_type = 'CONTACT'
  AND rel.directional_flag  = 'F'
  AND rel.party_id          = hcp.owner_table_id
  AND hcp.owner_table_name  = 'HZ_PARTIES'
    --AND hca.account_number    ='10000000016'
  AND hcp.contact_point_type= 'EMAIL'
  ORDER BY hca.account_number
/  