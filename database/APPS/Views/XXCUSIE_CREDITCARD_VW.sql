
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUSIE_CREDITCARD_VW" ("INSTRID", "CCNUMBER", "EXPIRYDATE", "CHNAME", "CARD_OWNER_ID", "ACTIVE_FLAG", "CARD_PURPOSE", "EMPLOYEE_ID", "CARD_NUMBER", "CARD_ID", "CARDMEMBER_NAME", "ORG_ID", "INACTIVE_DATE", "CARD_REFERENCE_ID", "CARD_PROGRAM_ID", "PARTY_NAME", "CARD_PROGRAM_NAME") AS 
  SELECT ic.instrid
      ,ic.ccnumber
      ,ic.expirydate
      ,ic.chname
      ,ic.card_owner_id
      ,ic.active_flag
      ,ic.card_purpose
      ,aca.employee_id
      ,aca.card_number
      ,aca.card_id
      ,aca.cardmember_name
      ,aca.org_id
      ,aca.inactive_date
      ,aca.card_reference_id
      ,aca.card_program_id
      ,hp.party_name
      ,acpa.card_program_name
  FROM iby_creditcard       ic
      ,ap_cards_all         aca
      ,hz_parties           hp
      ,ap_card_programs_all acpa
 WHERE ic.instrid(+) = aca.card_reference_id
   AND ic.card_owner_id = hp.party_id(+)
   AND aca.card_program_id = acpa.card_program_id(+)
;
