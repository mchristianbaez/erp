--
-- XXWC_AR_CREDIT_COPY_TRX_LOV_VW  (View)
--

CREATE OR REPLACE FORCE VIEW APPS.XXWC_AR_CREDIT_COPY_TRX_LOV_VW
(
   CUSTOMER_TRX_ID
  ,TRX_NUMBER
  ,CUSTOMER_ID
  ,CUSTOMER_NAME
  ,CUSTOMER_NUMBER
  ,BALANCE_DUE
  ,INVOICE_TOTAL
)
AS
   SELECT rct.customer_trx_id
         ,rct.trx_number
         ,rct.bill_to_customer_id customer_id
         ,hp.party_name customer_name
         ,hca.account_number customer_number
         ,XXWC_CREDIT_COPY_TRX_UTIL_PKG.get_balance_due (rct.customer_trx_id)
             balance_due
         ,XXWC_CREDIT_COPY_TRX_UTIL_PKG.get_invoice_total (
             rct.customer_trx_id)
             invoice_total
     FROM ra_customer_trx rct
         ,ra_batch_sources rbs
         ,ra_cust_trx_types rctt
         ,hz_parties hp
         ,hz_cust_accounts hca
    WHERE     rct.batch_source_id = rbs.batch_source_id
          AND rct.bill_to_customer_id = hca.cust_account_id
          AND rct.cust_trx_type_id = rctt.cust_trx_type_id
          AND hp.party_id = hca.party_id
          AND rct.status_trx = 'OP'
          AND rct.complete_flag = 'Y'
          AND rctt.TYPE IN ('INV', 'DM', 'CM')
          AND ( rbs.name NOT LIKE 'REBILL%' AND rbs.name NOT LIKE '%CONVERSION%') -- Modified by Maha for TMS# 20130909-00743
          AND NOT EXISTS
                 (SELECT '1'
                    FROM apps.xxwc_ar_credit_copy_trx trx
                   WHERE trx.customer_trx_id = rct.customer_trx_id);


