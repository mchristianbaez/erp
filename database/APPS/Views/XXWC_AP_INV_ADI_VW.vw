--
-- XXWC_AP_INV_ADI_VW  (View)
--

CREATE OR REPLACE FORCE VIEW apps.xxwc_ap_inv_adi_vw
(
   invoice_num
  ,invoice_type_lookup_code
  ,invoice_date
  ,po_number
  ,vendor_num
  ,vendor_name
  ,vendor_site_code
  ,invoice_amount
  ,invoice_currency_code
  ,exchange_rate
  ,exchange_rate_type
  ,exchange_date
  ,terms_name
  ,description
  ,awt_group_name
  ,attribute1
  ,attribute2
  ,attribute3
  ,attribute4
  ,attribute5
  ,attribute6
  ,attribute7
  ,attribute8
  ,attribute9
  ,attribute10
  ,attribute11
  ,attribute12
  ,attribute13
  ,attribute14
  ,attribute15
  ,source
  ,GROUP_ID
  ,payment_method_lookup_code
  ,pay_group_lookup_code
  ,gl_date
  ,line_type_lookup_code
  ,amount
  ,accounting_date
  ,line_description
  ,distribution_set_name
  ,dist_code_concatenated
  ,line_attribute1
  ,line_attribute2
  ,line_attribute3
  ,line_attribute4
  ,line_attribute5
  ,line_attribute6
  ,line_attribute7
  ,line_attribute8
  ,line_attribute9
  ,line_attribute10
  ,line_attribute11
  ,line_attribute12
  ,line_attribute13
  ,line_attribute14
  ,line_attribute15
  ,asset_book_type_code
  ,tax_classification_code
)
AS
   SELECT apia.invoice_num
         ,apia.invoice_type_lookup_code
         ,apia.invoice_date
         ,apia.po_number
         ,apia.vendor_num
         ,apia.vendor_name
         ,apia.vendor_site_code
         ,apia.invoice_amount
         ,apia.invoice_currency_code
         ,apia.exchange_rate
         ,apia.exchange_rate_type
         ,apia.exchange_date
         ,apia.terms_name
         ,apia.description
         ,apia.awt_group_name
         ,apia.attribute1
         ,apia.attribute2
         ,apia.attribute3
         ,apia.attribute4
         ,apia.attribute5
         ,apia.attribute6
         ,apia.attribute7
         ,apia.attribute8
         ,apia.attribute9
         ,apia.attribute10
         ,apia.attribute11
         ,apia.attribute12
         ,apia.attribute13
         ,apia.attribute14
         ,apia.attribute15
         ,apia.source
         ,apia.GROUP_ID
         ,apia.payment_method_lookup_code
         ,apia.pay_group_lookup_code
         ,apia.gl_date
         ,apil.line_type_lookup_code
         ,apil.amount
         ,apil.accounting_date
         ,apil.description line_description
         ,apil.distribution_set_name
         ,apil.dist_code_concatenated
         ,apil.attribute1 line_attribute1
         ,apil.attribute2 line_attribute2
         ,apil.attribute3 line_attribute3
         ,apil.attribute4 line_attribute4
         ,apil.attribute5 line_attribute5
         ,apil.attribute6 line_attribute6
         ,apil.attribute7 line_attribute7
         ,apil.attribute8 line_attribute8
         ,apil.attribute9 line_attribute9
         ,apil.attribute10 line_attribute10
         ,apil.attribute11 line_attribute11
         ,apil.attribute12 line_attribute12
         ,apil.attribute13 line_attribute13
         ,apil.attribute14 line_attribute14
         ,apil.attribute15 line_attribute15
         ,apil.asset_book_type_code
         ,apil.tax_classification_code
     FROM ap_invoices_interface apia, ap_invoice_lines_interface apil
    WHERE     apia.invoice_id = apil.invoice_id
          AND apia.status = 'NEW'
          AND 1 = 2
          AND apia.org_id = apil.org_id;


