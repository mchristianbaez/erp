CREATE OR REPLACE FORCE VIEW APPS.XXEIS_LP_FUEL_MILE_MONTH_V
   /* ************************************************************************
    HD Supply
    All rights reserved.
    Ticket                    Ver          Date         Author                       Description
    ----------------------    ----------   ----------   ------------------------     -------------------------
    Not available             1.0          08/21/2015   Production version     
    TMS 20150812-00201        1.1          08/21/2015   Balaguru Seshadri            Add oracle_location, creation_date and vendor_name
   ************************************************************************ */
(
   FULL_NAME,
   EMPLOYEE_NUMBER,
   EXPENSE_REPORT_NUMBER,
   TRANSACTION_DATE,
   DOW,
   AMOUNT_SPENT,
   ITEM_DESCRIPTION,
   SUB_CATEGORY,
   MERCHANT_NAME,
   MERCHANT_LOCATION,
   MCC_CODE,
   MCC_CODE_NO,
   JUSTIFICATION,
   BUSINESS_PURPOSE,
   REMARKS,
   ATTENDEES,
   APPROVER_NAME,
   QUERY_DESCR,
   CARD_PROGRAM_NAME,
   CARDMEMBER_NAME,
   ORACLE_PRODUCT,
   ORACLE_COST_CENTER,
   ORACLE_LOCATION, --Ver 1.1
   CREATION_DATE, --Ver 1.1
   VENDOR_NAME, --Ver 1.1   
   PERIOD_NAME,
   JOB_FAMILY,
   JOB_FAMILY_DESCR,
   LOB_NAME,
   RPT_GRP
)
AS
   (SELECT BT.full_name,
           BT.employee_number,
           BT.expense_report_number,
           BT.transaction_date,
           TO_CHAR (BT.transaction_date, 'Day') AS DOW,
           TO_CHAR (BT.line_amount, '$999,999,999.99') AS Amount_Spent,
           BT.item_description,
           BT.sub_category,
           BT.merchant_name,
           BT.merchant_location,
           BT.mcc_code,
           BT.mcc_code_no,
           BT.justification,
           BT.business_purpose,
           BT.remarks,
           BT.attendees,
           BT.approver_name,
           BT.query_descr,
           BT.card_program_name,
           BT.cardmember_name,
           BT.oracle_product,
           BT.oracle_cost_center,
		   BT.oracle_location, --Ver 1.1
           BT.creation_date, --Ver 1.1
           BT.vendor_name,  --Ver 1.1		   
           BT.period_name,
           EMPL.JOB_FAMILY,
           EMPL.JOB_FAMILY_DESCR,
           CASE LOB.LOB_NAME
              WHEN 'ELECTRICAL' THEN 'POWER SOLUTIONS'
              WHEN 'UTILITIES' THEN 'POWER SOLUTIONS'
              ELSE LOB.LOB_NAME
           END
              AS LOB_NAME,
           CASE
              WHEN EMPL.JOB_FAMILY = 'E01' THEN 'SLT'
              WHEN EMPL.JOB_FAMILY = 'V02' THEN 'OFFICER'
              WHEN LOB.LOB_NAME = 'ELECTRICAL' THEN 'POWER SOLUTIONS'
              WHEN LOB.LOB_NAME = 'UTILITIES' THEN 'POWER SOLUTIONS'
              ELSE LOB.LOB_NAME
           END
              AS RPT_GRP
      FROM XXCUS.Xxcus_Bullet_Iexp_Tbl BT,
           XXCUS.XXCUSHR_PS_EMP_ALL_TBL EMPL,
           APPS.XXEIS_LOB_NAMES_V LOB,
           (SELECT full_name, employee_number, period_name
              FROM XXCUs.Xxcus_Bullet_Iexp_Tbl
             WHERE     ( (   UPPER (item_description) IN 'FUEL'
                          OR UPPER (item_description) IN
                                'GAS AND OIL EXPENSE'
                          OR UPPER (item_description) LIKE
                                'GAS AND OIL EXPENSE (NON T%E RELATED)'))
                   AND (   (    UPPER (justification) NOT LIKE ('%RENT%')
                            AND UPPER (justification) NOT LIKE ('%FORK%')
                            AND UPPER (justification) NOT LIKE ('%PROPANE%'))
                        OR justification IS NULL)
                   AND (   (    UPPER (business_purpose) NOT LIKE ('%RENT%')
                            AND UPPER (business_purpose) NOT LIKE ('%FORK%')
                            AND UPPER (business_purpose) NOT LIKE
                                   ('%PROPANE%'))
                        OR business_purpose IS NULL)
                   AND (   (    UPPER (remarks) NOT LIKE ('%RENT%')
                            AND UPPER (remarks) NOT LIKE ('%FORK%')
                            AND UPPER (remarks) NOT LIKE ('%PROPANE%'))
                        OR remarks IS NULL)
                   AND (   (    UPPER (attendees) LIKE ('%RENT%')
                            AND UPPER (attendees) NOT LIKE ('%FORK%')
                            AND UPPER (attendees) NOT LIKE ('%PROPANE%'))
                        OR attendees IS NULL)
                   AND query_num IN ('1', '4', '6')
            INTERSECT
            SELECT full_name, employee_number, period_name
              FROM XXCUs.Xxcus_Bullet_Iexp_Tbl
             WHERE     (    (UPPER (item_description) LIKE '%MILEAGE%')
                        AND (   (    UPPER (justification) NOT LIKE
                                        ('%RENT%')
                                 AND UPPER (justification) NOT LIKE
                                        ('%FORK%')
                                 AND UPPER (justification) NOT LIKE
                                        ('%PROPANE%'))
                             OR justification IS NULL))
                   AND (   (    UPPER (business_purpose) NOT LIKE ('%RENT%')
                            AND UPPER (business_purpose) NOT LIKE ('%FORK%')
                            AND UPPER (business_purpose) NOT LIKE
                                   ('%PROPANE%'))
                        OR business_purpose IS NULL)
                   AND (   (    UPPER (remarks) NOT LIKE ('%RENT%')
                            AND UPPER (remarks) NOT LIKE ('%FORK%')
                            AND UPPER (remarks) NOT LIKE ('%PROPANE%'))
                        OR remarks IS NULL)
                   AND (   (    UPPER (attendees) LIKE ('%RENT%')
                            AND UPPER (attendees) NOT LIKE ('%FORK%')
                            AND UPPER (attendees) NOT LIKE ('%PROPANE%'))
                        OR attendees IS NULL)
                   AND query_num IN ('1', '4', '6')) XSACT
     WHERE     (    (   UPPER (BT.item_description) IN ('FUEL')
                     OR UPPER (BT.item_description) IN
                           ('GAS AND OIL EXPENSE')
                     OR UPPER (BT.item_description) LIKE
                           ('GAS AND OIL EXPENSE (NON T%E RELATED)')
                     OR UPPER (BT.item_description) LIKE '%MILEAGE%')
                AND (   (    UPPER (BT.justification) NOT LIKE ('%RENT%')
                         AND UPPER (BT.justification) NOT LIKE ('%FORK%')
                         AND UPPER (BT.justification) NOT LIKE ('%PROPANE%'))
                     OR BT.justification IS NULL)
                AND (   (    UPPER (BT.business_purpose) NOT LIKE ('%RENT%')
                         AND UPPER (BT.business_purpose) NOT LIKE ('%FORK%')
                         AND UPPER (BT.business_purpose) NOT LIKE
                                ('%PROPANE%'))
                     OR BT.business_purpose IS NULL)
                AND (   (    UPPER (BT.remarks) NOT LIKE ('%RENT%')
                         AND UPPER (BT.remarks) NOT LIKE ('%FORK%')
                         AND UPPER (BT.remarks) NOT LIKE ('%PROPANE%'))
                     OR BT.remarks IS NULL)
                AND (   (    UPPER (BT.attendees) LIKE ('%RENT%')
                         AND UPPER (BT.attendees) NOT LIKE ('%FORK%')
                         AND UPPER (BT.attendees) NOT LIKE ('%PROPANE%'))
                     OR BT.attendees IS NULL))
           AND BT.query_num IN ('1', '4', '6')
           AND BT.period_name = XSACT.period_name
           AND BT.full_name = XSACT.full_name
           AND BT.EMPLOYEE_NUMBER = XSACT.EMPLOYEE_NUMBER
           AND BT.EMPLOYEE_NUMBER = EMPL.EMPLOYEE_NUMBER
           AND BT.ORACLE_PRODUCT = LOB.ORACLE_PRODUCT)
   --AND BT.PERIOD_NAME = 'Oct-2012'
   --AND BT.EMPLOYEE_NUMBER = '38706'
   --AND upper(BT.full_name) LIKE 'DE%'
   ORDER BY BT.full_name;
--
COMMENT ON TABLE APPS.XXEIS_LP_FUEL_MILE_MONTH_V IS 'TMS 20150812-00201 / ESMS 288693';
--