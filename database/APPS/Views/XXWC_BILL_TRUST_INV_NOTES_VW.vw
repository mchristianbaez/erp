--
-- XXWC_BILL_TRUST_INV_NOTES_VW  (View)
--

CREATE OR REPLACE FORCE VIEW APPS.XXWC_BILL_TRUST_INV_NOTES_VW
(
   ORDER_LINE_ID
  ,LINE_ATTACH_DTYPE
  ,LINE_ATTACH_FORMAT
  ,ENTITY_NAME
  ,FUNCTION_NAME
  ,CATEGORY_DESCRIPTION
  ,SEQ_NUM
  ,LINE_TITLE
  ,LINE_ATTACH_DESC
  ,SHORT_TEXT
)
AS
     SELECT                                               /* MOAC_NO_CHANGE */
           pk1_value order_line_id
           ,datatype_name line_attach_dtype
           ,fndcatusg.format line_attach_format
           ,fndattdoc.entity_name
           ,fndattfn.function_name
           ,fnddoc.category_description
           ,fndattdoc.seq_num
           ,fnddoc.title line_title
           ,description line_attach_desc
           ,st.short_text
       FROM fnd_attachment_functions fndattfn
           ,fnd_doc_category_usages fndcatusg
           ,fnd_documents_vl fnddoc
           ,fnd_attached_documents fndattdoc
           ,fnd_documents_short_text st
      WHERE     fndattfn.attachment_function_id =
                   fndcatusg.attachment_function_id
            AND fndcatusg.category_id = fnddoc.category_id
            AND fnddoc.document_id = fndattdoc.document_id
            AND fndattdoc.entity_name IN
                   ('RA_CUSTOMER_TRX_ALL'
                   ,'RA_CUSTOMER_TRX_LINES_ALL'
                   ,'OE_ORDER_LINES')
            AND fnddoc.media_id = st.media_id
            AND fnddoc.datatype_id = 1
   ORDER BY fndattdoc.seq_num;


