DROP VIEW APPS.XXWC_CUST_SITE_LOC_SHARED_VW;

/* Formatted on 6/19/2012 11:53:14 AM (QP5 v5.115.810.9015) */
/*** Satish U: 18-JAN-2013 : Added Comments to the view                             **************** 
 ***Purpose  : View identifies all the Customer Sites that are sharing common party Site ***********
 **** THis view is called from Concurrent Program Assign distinct locations to customer sites ******
 ***************************************************************************************************/
CREATE OR REPLACE FORCE VIEW APPS.XXWC_CUST_SITE_LOC_SHARED_VW
(
   ACCOUNT_NUMBER,
   CUST_ACCOUNT_ID,
   ACCOUNT_NAME
)
AS
   SELECT   Account_Number, Cust_Account_Id, Account_Name
     FROM   apps.hz_cust_Accounts hca
    WHERE   EXISTS
               (SELECT   1
                  FROM   apps.Hz_Cust_Accounts hca1,
                         Hz_party_Sites hps2,
                         (  SELECT   COUNT ( * ), location_Id
                              FROM   hz_party_Sites
                          GROUP BY   location_Id
                            HAVING   COUNT ( * ) > 1) hps1,
                         apps.hz_cust_acct_sites hcas
                 WHERE       hca1.party_id = hps2.party_Id
                         AND hps1.location_id = hps2.Location_Id
                         AND hcas.party_site_id = hps2.party_Site_ID
                         -- AND hcas.org_id = 162   -- 28/09/2014 Commented by pattabhi for Canada & US OU Testing 
                         AND hca.party_id = hps2.party_id);