CREATE OR REPLACE FORCE VIEW apps.xxwc_ap_payment_terms_vw (
/******************************************************************************
   NAME        :  APPS.xxwc_ap_payment_terms_vw
   PURPOSE    :  This view is created for extracting the PAYABLE PAYMENT TERMS details

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        22/06/2012  Consuelo        Initial Version
   2.0        3/31/2015   Raghavendra s   TMS# 20150303-00075 - FIN - Addition of Code Snippet for Line Breaks in All EDW Extract Programs
                                           and Views for all free entry text fields.
******************************************************************************/
                                                            business_unit,
                                                            source_system,
                                                            operating_unit_id,
                                                            operating_unit_name,
                                                            payterm_id,
                                                            payment_term_description,
                                                            perc_discount,
                                                            discount_days,
                                                            due_days,
                                                            proxy_term,
                                                            proxy_day,
                                                            proxy_month
                                                           )
AS
   SELECT   'WHITECAP' business_unit, 'Oracle EBS' source_system,
            hou.organization_id operating_unit_id,
            hou.NAME operating_unit_name, apt.term_id payterm_id,
            -- apt.NAME payment_term_description, --  Commented for V 2.0
            REGEXP_REPLACE(apt.NAME,  '[[:cntrl:]]', ' ') payment_term_description, -- Added for V 2.0
            (atl.discount_percent / 100) perc_discount, atl.discount_days,
            atl.due_days,
            (CASE
                WHEN atl.discount_day_of_month IS NOT NULL
                   THEN 'Y'
                ELSE NULL
             END
            ) proxy_term,
            atl.discount_day_of_month proxy_day,
            atl.discount_months_forward proxy_month
       FROM ap_terms_tl apt, ap_terms_lines atl, hr_operating_units hou
      WHERE apt.term_id = atl.term_id(+)
   ORDER BY apt.NAME;


