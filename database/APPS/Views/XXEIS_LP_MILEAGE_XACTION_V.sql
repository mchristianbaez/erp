CREATE OR REPLACE FORCE VIEW APPS.XXEIS_LP_MILEAGE_XACTION_V
   /* ************************************************************************
    HD Supply
    All rights reserved.
    Ticket                    Ver          Date         Author                       Description
    ----------------------    ----------   ----------   ------------------------     -------------------------
    Not available             1.0          08/21/2015   Production version     
    TMS 20150812-00201        1.1          08/21/2015   Balaguru Seshadri            Add oracle_location, creation_date and vendor_name
   ************************************************************************ */
(
   FULL_NAME,
   EMPLOYEE_NUMBER,
   EXPENSE_REPORT_NUMBER,
   TRANSACTION_DATE,
   DOW,
   AMOUNT_SPENT,
   ITEM_DESCRIPTION,
   SUB_CATEGORY,
   MERCHANT_NAME,
   MERCHANT_LOCATION,
   MCC_CODE,
   MCC_CODE_NO,
   JUSTIFICATION,
   BUSINESS_PURPOSE,
   REMARKS,
   ATTENDEES,
   APPROVER_NAME,
   QUERY_DESCR,
   CARD_PROGRAM_NAME,
   CARDMEMBER_NAME,
   ORACLE_PRODUCT,
   ORACLE_COST_CENTER,
   ORACLE_LOCATION, --Ver 1.1
   CREATION_DATE, --Ver 1.1
   VENDOR_NAME, --Ver 1.1   
   PERIOD_NAME,
   JOB_FAMILY,
   JOB_FAMILY_DESCR,
   LOB_NAME,
   RPT_GRP
)
AS
   (SELECT "FULL_NAME",
           "EMPLOYEE_NUMBER",
           "EXPENSE_REPORT_NUMBER",
           "TRANSACTION_DATE",
           "DOW",
           "AMOUNT_SPENT",
           "ITEM_DESCRIPTION",
           "SUB_CATEGORY",
           "MERCHANT_NAME",
           "MERCHANT_LOCATION",
           "MCC_CODE",
           "MCC_CODE_NO",
           "JUSTIFICATION",
           "BUSINESS_PURPOSE",
           "REMARKS",
           "ATTENDEES",
           "APPROVER_NAME",
           "QUERY_DESCR",
           "CARD_PROGRAM_NAME",
           "CARDMEMBER_NAME",
           "ORACLE_PRODUCT",
           "ORACLE_COST_CENTER",
		   "ORACLE_LOCATION", -- Ver 1.1
		   "CREATION_DATE", --Ver 1.1
		   "VENDOR_NAME", --Ver 1.1
           "PERIOD_NAME",
           "JOB_FAMILY",
           "JOB_FAMILY_DESCR",
           "LOB_NAME",
           "RPT_GRP"
      FROM (SELECT BT.full_name,
                   BT.employee_number,
                   BT.expense_report_number,
                   BT.transaction_date,
                   TO_CHAR (BT.transaction_date, 'Day') AS DOW,
                   TO_CHAR (BT.line_amount, '$999,999,999.99')
                      AS Amount_Spent,
                   BT.item_description,
                   BT.sub_category,
                   BT.merchant_name,
                   BT.merchant_location,
                   BT.mcc_code,
                   BT.mcc_code_no,
                   BT.justification,
                   BT.business_purpose,
                   BT.remarks,
                   BT.attendees,
                   BT.approver_name,
                   BT.query_descr,
                   BT.card_program_name,
                   BT.cardmember_name,
                   BT.oracle_product,
                   BT.oracle_cost_center,
				   BT.oracle_location, --Ver 1.1
                   BT.creation_date, --Ver 1.1
                   BT.vendor_name,  --Ver 1.1				   
                   BT.period_name,
                   EMPL.JOB_FAMILY,
                   EMPL.JOB_FAMILY_DESCR,
                   CASE LOB.LOB_NAME
                      WHEN 'ELECTRICAL' THEN 'POWER SOLUTIONS'
                      WHEN 'UTILITIES' THEN 'POWER SOLUTIONS'
                      ELSE LOB.LOB_NAME
                   END
                      AS LOB_NAME,
                   CASE
                      WHEN EMPL.JOB_FAMILY = 'E01' THEN 'SLT'
                      WHEN EMPL.JOB_FAMILY = 'V02' THEN 'OFFICER'
                      WHEN LOB.LOB_NAME = 'ELECTRICAL' THEN 'POWER SOLUTIONS'
                      WHEN LOB.LOB_NAME = 'UTILITIES' THEN 'POWER SOLUTIONS'
                      ELSE LOB.LOB_NAME
                   END
                      AS RPT_GRP
              FROM XXCUS.Xxcus_Bullet_Iexp_Tbl BT,
                   XXCUS.XXCUSHR_PS_EMP_ALL_TBL EMPL,
                   APPS.XXEIS_LOB_NAMES_V LOB
             WHERE     BT.query_num IN ('4', '6')
                   AND BT.EMPLOYEE_NUMBER = EMPL.EMPLOYEE_NUMBER
                   AND BT.ORACLE_PRODUCT = LOB.ORACLE_PRODUCT
                   AND UPPER (item_description) LIKE '%MILEAGE%'
                   AND UPPER (product_descr) NOT IN ('MRO')
                   AND line_amount > 99.99
            UNION
            SELECT BT.full_name,
                   BT.employee_number,
                   BT.expense_report_number,
                   BT.transaction_date,
                   TO_CHAR (BT.transaction_date, 'Day') AS DOW,
                   TO_CHAR (BT.line_amount, '$999,999,999.99')
                      AS Amount_Spent,
                   BT.item_description,
                   BT.sub_category,
                   BT.merchant_name,
                   BT.merchant_location,
                   BT.mcc_code,
                   BT.mcc_code_no,
                   BT.justification,
                   BT.business_purpose,
                   BT.remarks,
                   BT.attendees,
                   BT.approver_name,
                   BT.query_descr,
                   BT.card_program_name,
                   BT.cardmember_name,
                   BT.oracle_product,
                   BT.oracle_cost_center,
				   BT.oracle_location, --Ver 1.1
                   BT.creation_date, --Ver 1.1
                   BT.vendor_name,  --Ver 1.1				   
                   BT.period_name,
                   EMPL.JOB_FAMILY,
                   EMPL.JOB_FAMILY_DESCR,
                   CASE LOB.LOB_NAME
                      WHEN 'ELECTRICAL' THEN 'POWER SOLUTIONS'
                      WHEN 'UTILITIES' THEN 'POWER SOLUTIONS'
                      ELSE LOB.LOB_NAME
                   END
                      AS LOB_NAME,
                   CASE
                      WHEN EMPL.JOB_FAMILY = 'E01' THEN 'SLT'
                      WHEN EMPL.JOB_FAMILY = 'V02' THEN 'OFFICER'
                      WHEN LOB.LOB_NAME = 'ELECTRICAL' THEN 'POWER SOLUTIONS'
                      WHEN LOB.LOB_NAME = 'UTILITIES' THEN 'POWER SOLUTIONS'
                      ELSE LOB.LOB_NAME
                   END
                      AS RPT_GRP
              FROM XXCUS.Xxcus_Bullet_Iexp_Tbl BT,
                   XXCUS.XXCUSHR_PS_EMP_ALL_TBL EMPL,
                   APPS.XXEIS_LOB_NAMES_V LOB,
                   (  SELECT XBT.EMPLOYEE_NUMBER,
                             XBT.FULL_NAME,
                             XBT.PERIOD_NAME,
                             COUNT (*)
                        FROM XXCUS.XXCUS_BULLET_IEXP_TBL XBT
                       WHERE     XBT.query_num IN ('4', '6')
                             AND UPPER (XBT.item_description) LIKE '%MILEAGE%'
                             AND UPPER (XBT.product_descr) IN ('MRO')
                             AND XBT.line_amount > 299.99
                    GROUP BY XBT.EMPLOYEE_NUMBER,
                             XBT.FULL_NAME,
                             XBT.PERIOD_NAME
                      HAVING COUNT (*) > 1) XBT
             WHERE     BT.query_num IN ('4', '6')
                   AND BT.EMPLOYEE_NUMBER = EMPL.EMPLOYEE_NUMBER
                   AND BT.ORACLE_PRODUCT = LOB.ORACLE_PRODUCT
                   AND BT.EMPLOYEE_NUMBER = XBT.EMPLOYEE_NUMBER
                   AND BT.FULL_NAME = XBT.FULL_NAME
                   AND BT.PERIOD_NAME = XBT.PERIOD_NAME
                   AND UPPER (item_description) LIKE '%MILEAGE%'
                   AND UPPER (product_descr) IN ('MRO')
                   AND line_amount > 299.99))
   --WHERE PERIOD_NAME = 'Oct-2012'
   --AND EMPLOYEE_NUMBER = '38706'
   --AND upper(full_name) LIKE 'DE%'
   ORDER BY full_name;
--
COMMENT ON TABLE APPS.XXEIS_LP_MILEAGE_XACTION_V IS 'TMS 20150812-00201 / ESMS 288693';
--