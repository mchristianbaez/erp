CREATE OR REPLACE VIEW apps.xxwc_om_co_receipt_v
AS
   /*
   View used in XXWC_OM_CO_REFUND from --Counter Order refunds from
   created by -- Shankar Hariharan
   creation_date - 03-Aug-2012
   28-N0v-12 View split into two for providing for credit card receipts not showing in refunds
   */
SELECT DISTINCT
          a.header_id
        , a.payment_number
        , a.payment_type_code
        , a.prepaid_amount
        , a.check_number
        , b.receipt_number
        , b.receipt_date
        , b.amount
        , b.cash_receipt_id
        , b.doc_sequence_value
        , (SELECT NVL (SUM (amount_applied), 0)
             FROM apps.ar_receivable_applications_v d
            WHERE d.cash_receipt_id = b.cash_receipt_id
                  AND d.applied_payment_schedule_id =-1)
             max_refund_amount
        , null card_issuer_code
        , null card_number
        , a.trxn_extension_id
     /*used in cash refunds from from OM*/
     FROM oe_payments a
        , (SELECT DISTINCT b.receipt_number
                         , b.receipt_date
                         , b.amount
                         , b.cash_receipt_id
                         , c.payment_set_id
                         , b.receipt_method_id
                         , b.doc_sequence_value
                         , b.payment_trxn_extension_id
             FROM apps.ar_cash_receipts b, apps.ar_receivable_applications c
            WHERE c.cash_receipt_id = b.cash_receipt_id
                  AND c.payment_set_id IS NOT NULL) b
    WHERE     a.payment_level_code = 'ORDER'
          AND a.payment_collection_event = 'PREPAY'
          AND a.payment_set_id = b.payment_set_id
          AND DECODE (a.payment_type_code
                    , 'CHECK', a.check_number
                    , b.doc_sequence_value) = b.receipt_number
          AND a.receipt_method_id = b.receipt_method_id
          AND a.trxn_extension_id is null
union          
   SELECT DISTINCT
          a.header_id
        , a.payment_number
        , a.payment_type_code
        , a.prepaid_amount
        , a.check_number
        , b.receipt_number
        , b.receipt_date
        , b.amount
        , b.cash_receipt_id
        , b.doc_sequence_value
        , (SELECT NVL (SUM (amount_applied), 0)
             FROM apps.ar_receivable_applications_v d
            WHERE d.cash_receipt_id = b.cash_receipt_id
                  AND d.applied_payment_schedule_id =-1)
             max_refund_amount
        , x.card_issuer_code
        , LTRIM (x.card_number, 'X') card_number
        , a.trxn_extension_id
     /*used in cash refunds from from OM*/
     FROM oe_payments a
        , (SELECT DISTINCT b.receipt_number
                         , b.receipt_date
                         , b.amount
                         , b.cash_receipt_id
                         , c.payment_set_id
                         , b.receipt_method_id
                         , b.doc_sequence_value
                         , b.payment_trxn_extension_id
             FROM apps.ar_cash_receipts b, apps.ar_receivable_applications c
            WHERE c.cash_receipt_id = b.cash_receipt_id
                  AND c.payment_set_id IS NOT NULL) b
        , apps.IBY_TRXN_EXTENSIONS_V x,apps.IBY_TRXN_EXTENSIONS_V y
    WHERE     a.payment_level_code = 'ORDER'
          AND a.payment_collection_event = 'PREPAY'
          AND a.payment_set_id = b.payment_set_id
          AND DECODE (a.payment_type_code
                    , 'CHECK', a.check_number
                    , b.doc_sequence_value) = b.receipt_number
          AND a.receipt_method_id = b.receipt_method_id
          AND a.trxn_extension_id is not null
          AND a.trxn_extension_id = x.trxn_extension_id
          and b.payment_trxn_extension_id=y.trxn_extension_id
          AND b.receipt_number=y.trxn_ref_number2
          and x.instrument_id=y.instrument_id
          and x.instr_assignment_id=y.instr_assignment_id
          and y.origin_application_id=222
          and y.trxn_ref_number1='RECEIPT'
