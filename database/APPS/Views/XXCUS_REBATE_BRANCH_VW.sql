
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUS_REBATE_BRANCH_VW" ("REBATES_FRU", "DW_BU_DESC", "DW_LOB_CD", "OPER_BRANCH", "LOB_BRANCH", "LOB_NAME", "FRU_DESCR", "STATE", "COUNTRY", "SYSTEM_CD", "SYSTEM_CODE", "ENTRP_ENTITY", "ENTRP_LOC", "ENTRP_CC", "BUSINESS_UNIT", "INACTIVE", "EFFDT") AS 
  (SELECT DISTINCT rebates_fru
               ,dw_bu_desc
               ,dw_lob_cd
               ,oper_branch
               ,lob_branch
               ,lv.description lob_name
               ,fru_descr
               ,state
               ,country
               ,system_cd
               ,system_code
               ,entrp_entity
               ,entrp_loc
               ,entrp_cc
               ,business_unit
               ,inactive
               ,EFFDT
  FROM xxcus.xxcus_rebate_branch_xref_tbl xrbxt
      ,apps.xxcus_location_code_vw        xlcv
      ,fnd_lookup_values_vl lv
 WHERE rebates_fru = fru
   AND lookup_type = 'XXCUS_BUSINESS_UNIT'
   AND XLCV.BUSINESS_UNIT = LV.LOOKUP_CODE)
;
