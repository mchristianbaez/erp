CREATE OR REPLACE FORCE VIEW apps.xxwc_inv_transfers_vw (
/******************************************************************************
   NAME        :  APPS.XXWC_INV_TRANSFERs_VW
   PURPOSE    :  This view is created for extracting the Inventory Transaction transfer details

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        31/07/2012  Consuelo        Initial Version
   2.0        3/4/2015    Raghavendra s   TMS# 20150303-00075 - FIN - Addition of Code Snippet for Line Breaks in All EDW Extract Programs
                                          and Views for all free entry test fields.
   3.0        27/05/2015  Raghavendra S    TMS# 20150302-00053 -- Update needed for EDW Transfers extract
******************************************************************************/
business_unit,
                                                         operating_unit_id,
                                                         operating_unit_name,
                                                         source_system,
                                                         ship_from_branch_num,
                                                         ship_from_branch_name,
                                                         ship_to_branch_num,
                                                         ship_to_branch_name,
                                                         transfer_trx_identifier,
                                                         line_status,
                                                         transfer_type,
                                                         order_date,
                                                         shipped_date,
                                                         receipt_date,
                                                         sku_number,
                                                         sku_description,
                                                         ordered_quantity,
                                                         shipped_quantity,
                                                         received_quantity,
                                                         transaction_cost,
                                                         po_uom,
                                                         conv_factor_for_each,
                                                         transferred_to_oe,
                                                         po_ref_num,
                                                         last_update_date,
                                                         so_number,
                                                         from_header_id,
                                                         from_line_id,
                                                         line_num,
                                                         quantity,
                                                         quantity_received,
                                                         from_org_id,
                                                         to_org_id,
                                                         to_header_id,
                                                         to_line_id,
                                                         transaction_id
                                                        )
AS
   SELECT 'WHITECAP' business_unit, 
           hou.organization_id operating_unit_id,
          hou.NAME operating_unit_name, 'Oracle EBS' source_system,
          mp_from.organization_code ship_from_branch_num,
          
          -- 05/30/2012 CG Changed to remove string before -
          -- ood_from.organization_name ship_from_branch_name,
          TRIM
             (SUBSTR (ood_from.organization_name,
                      INSTR (ood_from.organization_name, '-', 1, 1) + 1
                     )
             ) ship_from_branch_name,
          mp_to.organization_code ship_to_branch_num,
          
          -- 05/30/2012 CG Changed to remove string before -
          -- ood_to.organization_name ship_to_branch_name,
          TRIM
             (SUBSTR (ood_to.organization_name,
                      INSTR (ood_to.organization_name, '-', 1, 1) + 1
                     )
             ) ship_from_branch_name,
          
          -- 08/08/12 CG: Added the receipt id to make unique if there is more than one
          (req_ln.requisition_line_id || oola.line_id || rt.transaction_id
          ) transfer_trx_identifier,
          oola.flow_status_code line_status,
          (CASE
              WHEN mp_from.organization_id != mp_to.organization_id
                 THEN 'InterBranch'
              ELSE 'Unknown'
           END
          ) transfer_type,
          ooha.creation_date order_date,                         --ordered_date
          oola.actual_shipment_date shipped_date,
          rt.transaction_date receipt_date, 
         -- msib.segment1 sku_number, -- Commented for V 2.0
          REGEXP_REPLACE(msib.segment1,  '[[:cntrl:]]', ' ')  sku_number, -- Added for V 2.0
          --NVL (req_ln.item_description, msib.description) sku_description, -- Commented for V 2.0
          REGEXP_REPLACE(NVL (req_ln.item_description, msib.description),  '[[:cntrl:]]', ' ')  sku_description, -- Added for V 2.0
          req_ln.quantity ordered_quantity, oola.shipped_quantity,
          rt.quantity received_quantity,
          oola.unit_selling_price transaction_cost
                                                  -- Unit Price on sales order
          ,
          req_ln.unit_meas_lookup_code po_uom, 1 conv_factor_for_each,
          NVL (req_hdr.transferred_to_oe_flag, 'N') transferred_to_oe,
          --req_hdr.segment1 po_ref_num, -- Commented for V 2.0
          REGEXP_REPLACE(req_hdr.segment1,  '[[:cntrl:]]', ' ') po_ref_num, -- Added for V 2.0
          req_ln.last_update_date,
          --ooha.order_number so_number -- Commented for V 2.0
          REGEXP_REPLACE(ooha.order_number,  '[[:cntrl:]]', ' ') so_number, -- Added for V 2.0  
          req_hdr.requisition_header_id from_header_id,
          req_ln.requisition_line_id from_line_id, req_ln.line_num,
          req_ln.quantity, req_ln.quantity_received,
          req_ln.source_organization_id from_org_id,
          req_ln.destination_organization_id to_org_id,
          ooha.header_id to_header_id, oola.line_id to_line_id,
          rt.transaction_id
     FROM apps.po_requisition_headers req_hdr,
          apps.po_requisition_lines req_ln,
          apps.oe_order_headers ooha,
          oe_order_sources oos,
          apps.oe_order_lines oola,
          mtl_parameters mp_from,
          org_organization_definitions ood_from,
          mtl_parameters mp_to,
          org_organization_definitions ood_to,
          rcv_transactions rt,
          mtl_system_items_b msib,
          hr_operating_units hou
    WHERE req_hdr.type_lookup_code = 'INTERNAL'
      AND req_hdr.requisition_header_id = req_ln.requisition_header_id
      --AND req_hdr.requisition_header_id = ooha.source_document_id(+)
      AND req_ln.requisition_header_id = oola.source_document_id(+)
      AND req_ln.requisition_line_id = oola.source_document_line_id(+)
      AND oola.header_id = ooha.header_id(+)
      AND ooha.order_source_id = oos.order_source_id(+)
      AND oos.NAME(+) = 'Internal'
      AND req_ln.source_organization_id = mp_from.organization_id
      AND mp_from.organization_id = ood_from.organization_id
      AND req_ln.destination_organization_id = mp_to.organization_id
      AND mp_to.organization_id = ood_to.organization_id
      AND req_ln.requisition_line_id = rt.requisition_line_id(+)
      AND rt.source_document_code(+) = 'REQ'
      -- AND rt.transaction_type (+) = 'RECEIVE' commented for V 3.0
      AND rt.transaction_type = 'RECEIVE' -- Added for V 3.0
      AND req_ln.item_id = msib.inventory_item_id
      AND req_ln.destination_organization_id = msib.organization_id
      AND ood_from.operating_unit = hou.organization_id
   UNION
-- Intransit Shipments
   SELECT 'WHITECAP' business_unit, hou.organization_id operating_unit_id,
          hou.NAME operating_unit_name, 'Oracle EBS' source_system,
          mp_from.organization_code ship_from_branch_num,
          
          -- 05/30/2012 CG Changed to remove string before -
          -- ood_from.organization_name ship_from_branch_name,
          TRIM
             (SUBSTR (ood_from.organization_name,
                      INSTR (ood_from.organization_name, '-', 1, 1) + 1
                     )
             ) ship_from_branch_name,
          mp_to.organization_code ship_to_branch_num,
          
          -- 05/30/2012 CG Changed to remove string before -
          -- ood_to.organization_name ship_to_branch_name,
          TRIM
             (SUBSTR (ood_to.organization_name,
                      INSTR (ood_to.organization_name, '-', 1, 1) + 1
                     )
             ) ship_from_branch_name,
          
          -- 08/08/12 CG: Added the receipt id to make unique if there is more than one
          (rsl.shipment_header_id || rsl.shipment_line_id || rt.transaction_id
          ) transfer_trx_identifier,
          rsl.shipment_line_status_code line_status,
          (CASE
              WHEN mp_from.organization_id != mp_to.organization_id
                 THEN 'InterBranch'
              ELSE 'Unknown'
           END
          ) transfer_type,
          rsh.creation_date ordered_date, rsh.shipped_date,
          rt.transaction_date receipt_date, 
          --msib.segment1 sku_number, -- Commented for V 2.0
          REGEXP_REPLACE(msib.segment1,  '[[:cntrl:]]', ' ')  sku_number, -- Added for V 2.0
          --NVL (rsl.item_description, msib.description) sku_description, -- Commented for V 2.0
          REGEXP_REPLACE(NVL (rsl.item_description, msib.description),  '[[:cntrl:]]', ' ')  sku_description, -- Added for V 2.0
          rsl.quantity_shipped ordered_quantity,
          rsl.quantity_shipped shipped_quantity,
          rt.quantity received_quantity, rsl.transfer_cost transaction_cost,
          rsl.unit_of_measure po_uom, 1 conv_factor_for_each,
          -- In the bleow line REGEXP_REPLACE condition added by Pattabhi on 24-Dec-2014 for TMS# 20141223-00109
          NULL transferred_to_oe, REGEXP_REPLACE(rsh.shipment_num,'[[:cntrl:]]') po_ref_num, 
          rsl.last_update_date, NULL so_number,
          rsh.shipment_header_id from_header_id,
          rsl.shipment_line_id from_line_id, rsl.line_num, 0 quantity,
          rsl.quantity_received, rsl.from_organization_id from_org_id,
          rsl.to_organization_id to_org_id, 0 to_header_id, 0 to_line_id,
          rt.transaction_id
     FROM rcv_shipment_headers rsh,
          rcv_shipment_lines rsl,
          rcv_transactions rt,
          mtl_parameters mp_from,
          org_organization_definitions ood_from,
          mtl_parameters mp_to,
          org_organization_definitions ood_to,
          mtl_system_items_b msib,
          hr_operating_units hou
    WHERE rsh.receipt_source_code = 'INVENTORY'
      AND rsh.shipment_header_id = rsl.shipment_header_id
      AND rsl.source_document_code = 'INVENTORY'
      AND rsl.shipment_header_id = rt.shipment_header_id(+)
      AND rsl.shipment_line_id = rt.shipment_line_id(+)
      AND rt.transaction_type(+) = 'RECEIVE'
      AND rsl.from_organization_id = mp_from.organization_id
      AND mp_from.organization_id = ood_from.organization_id
      AND rsl.to_organization_id = mp_to.organization_id
      AND mp_to.organization_id = ood_to.organization_id
      AND rsl.item_id = msib.inventory_item_id
      AND rsl.to_organization_id = msib.organization_id
      AND ood_from.operating_unit = hou.organization_id;
