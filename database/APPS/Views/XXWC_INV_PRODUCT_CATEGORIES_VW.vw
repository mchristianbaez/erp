CREATE OR REPLACE FORCE VIEW APPS.XXWC_INV_PRODUCT_CATEGORIES_VW
(
/******************************************************************************
   NAME:       APPS.XXWC_INV_PRODUCT_CATEGORIES_VW
   PURPOSE:  This view is used to extract all the inventory Product Categories Details

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        31/07/2012   Consuelo        Initial Version
   2.0        23/04/2015   Raghavendra S   TMS# 20150303-00075 - FIN - Addition of Code Snippet for Line Breaks in All EDW Extract Programs
                                            and Views for all free entry text fields.
******************************************************************************/
   BUSINESS_UNIT,
   SOURCE_SYSTEM,
   CATEGORY,
   CATEGORY_VALUE,
   CATEGORY_DESC,
   SUB_CATEGORY,
   SUB_CATEGORY_VALUE,
   SUB_CATEGORY_DESC,
   PRODUCT_LINE,
   PRODUCT_LINE_VALUE,
   PRODUCT_LINE_DESC,
   CAT_CLASS,
   CAT_CLASS_VALUE,
   CAT_CLASS_DESC,
   OPERATING_UNIT_ID,
   INTERFACE_DATE
)
AS
   SELECT 'WHITECAP' business_unit,
          'Oracle EBS' source_system,
          'CAT' category,
          -- TO_NUMBER (mcv.attribute5) category_value, -- commented for V 2.0
          REGEXP_REPLACE(TO_NUMBER (mcv.attribute5),  '[[:cntrl:]]', ' ') category_value, -- added for V 2.0
          /*(SELECT ffvt.description
             FROM apps.fnd_flex_values ffv,
                  apps.fnd_flex_values_tl ffvt,
                  apps.fnd_flex_value_sets ffvs
            WHERE     ffv.flex_value_id = ffvt.flex_value_id
                  AND ffv.flex_value_set_id = ffvs.flex_value_set_id
                  AND ffvs.flex_value_set_name = 'XXWC_CATMGT_CATEGORIES'
                  AND ffv.flex_value = mcv.attribute5)
             category_desc,*/  -- commented for V 2.0
          REGEXP_REPLACE((SELECT ffvt.description
             FROM apps.fnd_flex_values ffv,
                  apps.fnd_flex_values_tl ffvt,
                  apps.fnd_flex_value_sets ffvs
            WHERE     ffv.flex_value_id = ffvt.flex_value_id
                  AND ffv.flex_value_set_id = ffvs.flex_value_set_id
                  AND ffvs.flex_value_set_name = 'XXWC_CATMGT_CATEGORIES'
                  AND ffv.flex_value = mcv.attribute5),  '[[:cntrl:]]', ' ')
             category_desc,-- added for V 2.0
          'SUBCAT' sub_category,
          -- TO_NUMBER (mcv.attribute6) sub_category_value, -- commented for V 2.0
          REGEXP_REPLACE(TO_NUMBER (mcv.attribute6),  '[[:cntrl:]]', ' ') sub_category_value, -- added for V 2.0
          
         /* (SELECT ffvt.description
             FROM apps.fnd_flex_values ffv,
                  apps.fnd_flex_values_tl ffvt,
                  apps.fnd_flex_value_sets ffvs
            WHERE     ffv.flex_value_id = ffvt.flex_value_id
                  AND ffv.flex_value_set_id = ffvs.flex_value_set_id
                  AND ffvs.flex_value_set_name = 'XXWC_CATMGT_SUBCATEGORIES'
                  AND ffv.flex_value = mcv.attribute6)      sub_category_desc,*/ -- commented for V 2.0
          REGEXP_REPLACE((SELECT ffvt.description
             FROM apps.fnd_flex_values ffv,
                  apps.fnd_flex_values_tl ffvt,
                  apps.fnd_flex_value_sets ffvs
            WHERE     ffv.flex_value_id = ffvt.flex_value_id
                  AND ffv.flex_value_set_id = ffvs.flex_value_set_id
                  AND ffvs.flex_value_set_name = 'XXWC_CATMGT_SUBCATEGORIES'
                  AND ffv.flex_value = mcv.attribute6),  '[[:cntrl:]]', ' ')
             sub_category_desc, -- added for V 2.0
          'TBD' product_line,
          '1' product_line_value,
          'N' product_line_desc,
          'CATCLASS' cat_class,
          mcv.segment2 cat_class_value,
          (SELECT ffvt.description
             FROM apps.fnd_flex_values ffv,
                  apps.fnd_flex_values_tl ffvt,
                  apps.fnd_flex_value_sets ffvs
            WHERE     ffv.flex_value_id = ffvt.flex_value_id
                  AND ffv.flex_value_set_id = ffvs.flex_value_set_id
                  AND ffvs.flex_value_set_name = 'XXWC_CATEGORY_CLASS'
                  AND ffv.flex_value = mcv.segment2)
             cat_class_desc,
         -- '162' operating_unit_id,  --01/10/2014 Commented by Veera as per the Canda OU Test
           mo_global.get_current_org_id operating_unit_id,
          SYSDATE interface_date
     FROM apps.mtl_categories_v mcv
    WHERE     mcv.structure_name = 'Item Categories'
          AND mcv.attribute5 IS NOT NULL
          AND mcv.attribute6 IS NOT NULL
          AND (mcv.disable_date IS NULL OR mcv.disable_date >= SYSDATE);