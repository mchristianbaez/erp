CREATE OR REPLACE FORCE VIEW APPS.XXWC_AR_LEGAL_GEN
(
   PARTY_ID,
   ACCOUNT_NUMBER,
   ACCOUNT_NAME,
   ACCOUNT_STATUS,
   NAME,
   SEND_STATEMENTS,
   ATTRIBUTE5,
   CREATION_DATE,
   PARTY_NUMBER,
   PRIMARY_PHONE_AREA_CODE,
   ATTRIBUTE10
)
AS
   SELECT DISTINCT a.party_id,
                   a.account_number,
                   a.account_name,
                   c.account_status,
                   d.name,                                   --- diane added TMS 20140919-00202 
                   c.send_statements,
                   a.attribute5,
                   a.creation_date,
                   b.party_number,
                   b.primary_phone_area_code,
                   a.attribute10
     FROM ar.hz_cust_accounts a,
          ar.hz_parties b,
          ar.hz_customer_profiles c,
          (SELECT DISTINCT collector_id, name FROM ar.ar_collectors) d ---- diane added TMS 20140919-00202
    WHERE     a.party_id = b.party_id
          AND a.cust_account_id = c.cust_account_id
          AND c.collector_id = d.collector_id                  ---- diane added TMS 20140919-00202 
          AND c.site_use_id IS NULL;                           ---- diane added TMS 20140919-00202
 