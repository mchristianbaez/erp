--
-- XXWC_QP_MODIFIERS_LIST_VW  (View)
--

CREATE OR REPLACE FORCE VIEW APPS.XXWC_QP_MODIFIERS_LIST_VW
(
   LIST_HEADER_ID
  ,MODIFIER_LIST_NAME
  ,MODIFIER_LIST_NUMBER
  ,CURRENCY_CODE
  ,LIST_TYPE_CODE
  ,START_DATE_ACTIVE
  ,END_DATE_ACTIVE
  ,GLOBAL_FLAG
  ,AUTOMATIC_FLAG
  ,LN_ORIG_SYS_LINE_REF
  ,LN_LIST_LINE_ID
  ,LN_LIST_LINE_NO
  ,LN_MODIFIER_LEVEL
  ,LN_LIST_LINE_TYPE
  ,LN_START_DATE_ACTIVE
  ,LN_END_DATE_ACTIVE
  ,LN_AUTOMATIC_FLAG
  ,LN_OVERRIDE_FLAG
  ,LN_PRICING_PHASE
  ,LN_PRODUCT_PRECEDENCE
  ,LN_PRICE_BREAK_TYPE_CODE
  ,PA_PRODUCT_UOM_CODE
  ,LN_ARITHMETIC_OPERATOR_TYPE
  ,LN_OPERAND
  ,LN_ACCRUAL_FLAG
  ,LN_INCOMPATIBILITY_GRP_CODE
  ,LN_PRICING_GROUP_SEQUENCE
  ,LN_INCLUDE_ON_RETURNS_FLAG
  ,LN_FORMULA
  ,LN_QUALIFICATION_IND
  ,PA_PRICING_ATTRIBUTE_ID
  ,PA_ORIG_SYS_PRICING_ATTR_REF
  ,PA_PRODUCT_ATTRIBUTE_CONTEXT
  ,PA_PRODUCT_ATTRIBUTE
  ,PA_PRODUCT_ATTR_VALUE_ITEM
  ,PA_PRODUCT_ATTR_VALUE_CAT
  ,PA_PRICING_ATTRIBUTE
  ,PA_COMPARISON_OPERATOR_CODE
  ,PA_PRICING_ATTRIBUTE_CONTEXT
  ,PA_PRICING_ATTR_VALUE_FROM
  ,PA_PRICING_ATTR_VALUE_TO
  ,PA_EXCLUDER_FLAG
  ,PA_ATTRIBUTE_GROUPING_NO
)
AS
   SELECT qlh.LIST_HEADER_ID
         ,qlh.DESCRIPTION
         ,qlh.NAME
         ,qlh.CURRENCY_CODE
         ,qlh.LIST_TYPE_CODE
         ,qlh.START_DATE_ACTIVE
         ,qlh.END_DATE_ACTIVE
         ,qlh.GLOBAL_FLAG
         ,qlh.AUTOMATIC_FLAG
         ,NULL
         ,qll.LIST_LINE_ID
         ,qll.LIST_LINE_NO
         ,qll.Modifier_Level
         ,qll.List_Line_Type
         ,qll.START_DATE_ACTIVE
         ,qll.END_DATE_ACTIVE
         ,qll.AUTOMATIC_FLAG
         ,qll.OVERRIDE_FLAG
         ,qll.Pricing_Phase
         ,qll.PRODUCT_PRECEDENCE
         ,qll.PRICE_BREAK_TYPE_CODE
         ,qll.PRODUCT_UOM_CODE
         ,qll.Arithmetic_Operator_Type
         ,qll.OPERAND
         ,qll.ACCRUAL_FLAG
         ,qll.INCOMPATIBILITY_GRP_CODE
         ,qll.PRICING_GROUP_SEQUENCE
         ,qll.INCLUDE_ON_RETURNS_FLAG
         ,qll.FORMULA
         ,NULL
         ,qll.PRICING_ATTRIBUTE_ID
         ,NULL
         ,qll.PRODUCT_ATTRIBUTE_CONTEXT
         ,qll.PRODUCT_ATTRIBUTE_TYPE
         ,DECODE (qll.PRODUCT_ATTR
                 ,'PRICING_ATTRIBUTE1', qll.Product_Attr_Value
                 ,NULL)
         ,DECODE (qll.PRODUCT_ATTR
                 ,'PRICING_ATTRIBUTE2', qll.Product_Attr_Value
                 ,NULL)
         ,qll.PRICING_ATTRIBUTE
         ,qll.COMPARISON_OPERATOR_CODE
         ,qll.PRICING_ATTRIBUTE_CONTEXT
         ,qll.PRICING_ATTR_VALUE_FROM
         ,qll.PRICING_ATTR_VALUE_TO
         ,qll.EXCLUDER_FLAG
         ,qll.ATTRIBUTE_GROUPING_NO
     FROM QP_List_Headers qlh
         ,qp_modifier_summary_v qll
         ,FND_LOOKUP_VALUES_VL mlt
    WHERE     qlh.List_Header_ID = qll.List_Header_Id
          AND mlt.Lookup_TYpe = 'HOMEPG_MODIFIER_LIST_TYPE'
          AND mlt.Lookup_code = qlh.List_type_Code
          AND NVL(qll.END_DATE_ACTIVE, TRUNC(SYSDATE))>=TRUNC(SYSDATE)--TMS #20130304-00477 --added by Ram Talluri 3/4/2013
   UNION
   SELECT qlh.LIST_HEADER_ID
         ,qlh.DESCRIPTION
         ,qlh.NAME
         ,qlh.CURRENCY_CODE
         ,qlh.LIST_TYPE_CODE
         ,qlh.START_DATE_ACTIVE
         ,qlh.END_DATE_ACTIVE
         ,qlh.GLOBAL_FLAG
         ,qlh.AUTOMATIC_FLAG
         ,NULL
         ,NULL
         ,NULL
         ,NULL
         ,NULL
         ,NULL
         ,NULL
         ,NULL
         ,NULL
         ,NULL
         ,NULL
         ,NULL
         ,NULL
         ,NULL
         ,NULL
         ,NULL
         ,NULL
         ,NULL
         ,NULL
         ,NULL
         ,NULL
         ,NULL
         ,NULL
         ,NULL
         ,NULL
         ,NULL
         ,NULL
         ,NULL
         ,NULL
         ,NULL
         ,NULL
         ,NULL
         ,NULL
         ,NULL
     FROM QP_List_Headers qlh, FND_LOOKUP_VALUES_VL mlt
    WHERE     mlt.Lookup_TYpe = 'HOMEPG_MODIFIER_LIST_TYPE'
          AND mlt.Lookup_code = qlh.List_type_Code
          AND NOT EXISTS
                 (SELECT 1
                    FROM Qp_list_Lines line
                   WHERE line.List_Header_ID = qlh.list_Header_ID);


