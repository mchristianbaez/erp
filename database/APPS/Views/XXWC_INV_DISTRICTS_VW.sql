  /****************************************************************************************************
   View: XXWC_INV_DISTRICTS_VW.sql  	
  Description: Districts information.
  HISTORY
  =====================================================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- ----------------------------------------------------------
  1.0     11-Feb-2016        P.Vamshidhar    Initial version TMS#20160209-00194
                                             Reporting Automation � Disposition   
  *****************************************************************************************************/

CREATE OR REPLACE VIEW XXWC_INV_DISTRICTS_VW AS 
SELECT flex_value DISTRICT_NAME
  FROM apps.fnd_flex_value_sets ffvs, apps.fnd_flex_values ffv
 WHERE     ffvs.flex_value_set_name = 'XXWC_DISTRICT'
       AND ffvs.flex_value_set_id = ffv.flex_value_set_id
       AND ENABLED_FLAG = 'Y'
       AND NVL (START_DATE_ACTIVE, SYSDATE - 1) <= SYSDATE
       AND NVL (START_DATE_ACTIVE, SYSDATE + 1) > SYSDATE;
/
GRANT SELECT ON APPS.XXWC_INV_DISTRICTS_VW TO INTERFACE_APEXWC;
/       
GRANT SELECT ON APPS.XXWC_INV_DISTRICTS_VW TO INTERFACE_DSTAGE;
/