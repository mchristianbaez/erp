--
-- XXWC_AR_RECPT_METHOD_CASHBR_VW  (View)
--

CREATE OR REPLACE FORCE VIEW APPS.XXWC_AR_RECPT_METHOD_CASHBR_VW
(
   NAME
  ,RECEIPT_METHOD_ID
  ,MEANING
  ,"bank_account_id"
  ,BANK_ACCOUNT_NUM
  ,BANK_NAME
  ,BANK_BRANCH_NAME
  ,"bank_branch_id"
  ,CURRENCY_CODE
  ,CREATION_STATUS
  ,RECEIPT_CLASS_NAME
  ,REMIT_FLAG
  ,NOTES_RECEIVABLE
  ,OVERRIDE_FLAG
  ,CREATION_METHOD_CODE
  ,PAYMENT_TYPE_CODE
  ,RECEIPT_MULTI_CURRENCY_FLAG
  ,ORG_ID
  ,OPERATING_UNIT
  ,DEF
  ,LEGAL_ENTITY_ID
)
AS
   SELECT rm.NAME
         ,rm.receipt_method_id
         ,SUBSTRB (
             arpt_sql_func_util.get_lookup_meaning ('RECEIPT_CREATION_STATUS'
                                                   ,rc.creation_status)
            ,1
            ,80)
             meaning
         ,ba.bank_acct_use_id "bank_account_id"
         ,SUBSTRB (
             ce_bank_and_account_util.get_masked_bank_acct_num (
                cba.bank_account_id)
            ,1
            ,360)
             bank_account_num
         ,bb.bank_name
         ,bb.bank_branch_name
         ,bb.branch_party_id "bank_branch_id"
         ,cba.currency_code
         ,rc.creation_status
         ,rc.NAME receipt_class_name
         ,rc.remit_flag
         ,rc.notes_receivable
         ,SUBSTRB (NVL (rma.override_remit_account_flag, 'Y'), 1, 1)
             override_flag
         ,rc.creation_method_code
         ,rm.payment_type_code
         ,cba.receipt_multi_currency_flag receipt_multi_currency_flag
         ,rma.org_id org_id
         ,SUBSTRB (mo_global.get_ou_name (rma.org_id), 1, 360) operating_unit
         ,DECODE (rma.org_id, fnd_profile.VALUE ('ORG_ID'), 0, 1) def
         ,cba.account_owner_org_id legal_entity_id
     FROM ar_receipt_methods rm
         ,ar_receipt_classes rc
         ,ce_bank_accounts cba
         ,apps.ce_bank_acct_uses ba
         ,ce_bank_branches_v bb
         ,apps.ar_receipt_method_accounts rma
         ,hr_operating_units hr
    WHERE     rm.receipt_class_id = rc.receipt_class_id
          AND rma.org_id = ba.org_id
          AND hr.organization_id = rma.org_id
          AND mo_global.check_access (hr.organization_id) = 'Y'
          AND rma.org_id = NVL (fnd_profile.VALUE ('ORG_ID'), rma.org_id)
          AND (SYSDATE BETWEEN rm.start_date AND NVL (rm.end_date, SYSDATE))
          AND (   (rc.creation_method_code = 'MANUAL')
               OR (rc.creation_method_code = 'BR_REMIT')
               OR (    rc.creation_method_code = 'AUTOMATIC'
                   AND rc.remit_flag = 'Y'
                   AND rc.confirm_flag = 'N'))
          AND cba.account_classification = 'INTERNAL'
          AND NVL (ba.end_date, SYSDATE + 1) > SYSDATE
          AND NVL (bb.end_date, SYSDATE + 1) > SYSDATE
          AND SYSDATE BETWEEN rma.start_date AND NVL (rma.end_date, SYSDATE)
          AND cba.currency_code = 'USD'
          AND cba.bank_branch_id = bb.branch_party_id
          AND rm.receipt_method_id = rma.receipt_method_id
          AND rma.remit_bank_acct_use_id = ba.bank_acct_use_id
          AND cba.bank_account_id = ba.bank_account_id
          AND cba.ar_use_allowed_flag = 'Y'
          AND NVL (ba.ar_use_enable_flag, 'N') = 'Y'
          AND rc.creation_status IN ('REMITTED', 'CONFIRMED', 'CLEARED')
          AND DECODE (
                 NVL (
                    fnd_profile.VALUE (
                       'XXWC_RESTRICT_RECEIPT_METHOD_BY_BRANCH')
                   ,'Y')
                ,'Y', 'ALL'
                ,SUBSTR (rm.NAME, 1, 3)) =
                 NVL (
                    fnd_profile.VALUE (
                       'XXWC_RESTRICT_RECEIPT_METHOD_BY_BRANCH')
                   ,'ALL');


