/* Formatted on 3/27/2013 9:43:27 AM (QP5 v5.115.810.9015) */
--
-- XXWC_WSH_SHIPPING_DEL_V	(View)
--

CREATE OR REPLACE FORCE VIEW apps.xxwc_wsh_shipping_del_v (header_id
														 , delivery_id
														 , order_number
														 , ship_from_org_id)
AS
   SELECT	DISTINCT a.header_id
				   , a.delivery_id
				   , b.order_number
				   , a.ship_from_org_id
	 FROM	apps.xxwc_wsh_shipping_stg a, apps.oe_order_headers b
	WHERE	a.header_id = b.header_id;


