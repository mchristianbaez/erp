/******************************************************************************
      NAME:       apps.xxwc_om_order_holds_pkg
      PURPOSE:    Package to create a case folder whenever a hard hold is placed.

      REVISIONS:
      Ver        Date        Author               Description
      ---------  ----------  ---------------  ------------------------------------
	  1.5        02-Feb-2017   Niraj K Ranjan      TMS#20150811-00093   Credit - Order Release Form Enhancements
******************************************************************************/
CREATE OR REPLACE VIEW apps.xxwc_ar_cmgt_folder_details_v
AS
   SELECT /*  Created by:  Shankar Hariharan
              Creation Date: 01-Jun-2013
             Purpose:  Used in custom Order hold release from
          */
         a.case_folder_id
        , a.case_folder_number
        , a.status
        , a.credit_request_id
        , b.application_number
        , a.credit_analyst_id
        , a.credit_classification
        , a.cust_account_id
        , a.party_id
        , a.site_use_id
        , a.review_type
        , a.check_list_id
        , c.party_name
        , c.party_number
        , d.account_number
        , d.account_name
        , e.location site_location
        , (SELECT z.party_site_number
             FROM hz_cust_site_uses x
                , hz_cust_acct_sites y
                , hz_party_sites z
            WHERE     x.site_use_id = e.site_use_id
                  AND x.cust_acct_site_id = y.cust_acct_site_id
                  AND y.party_site_id = z.party_site_id)
             site_number
        , e.attribute1 site_type
        , (SELECT data_point_value
             FROM AR_CMGT_cf_dtls
            WHERE case_folder_id = a.case_Folder_id AND data_point_id = 20144)
             site_prelim_flag
        , (SELECT data_point_value
             FROM AR_CMGT_cf_dtls
            WHERE case_folder_id = a.case_Folder_id AND data_point_id = 20148)
             job_info_on_file
        , b.source_column2 order_number
        , oe_oe_totals_summary.PRT_ORDER_TOTAL (b.source_column1) order_total
        , (SELECT data_point_value
             FROM AR_CMGT_cf_dtls
            WHERE case_folder_id = f.case_Folder_id AND data_point_id = 15)
             pending_order_amount
        , (SELECT data_point_value
             FROM AR_CMGT_cf_dtls
            WHERE case_folder_id = f.case_Folder_id AND data_point_id = 34)
             current_ar
        , (CASE
              WHEN b.cust_account_id = -99
              THEN
                 0
              WHEN b.site_use_id = -99
              THEN
                 NVL (
                    (SELECT TO_NUMBER (NVL (data_point_value, 0))
                       FROM AR_CMGT_cf_dtls
                      WHERE case_folder_id = f.case_Folder_id
                            AND data_point_id = 20158)
                  , (SELECT TO_NUMBER (NVL (data_point_value, 0))
                       FROM AR_CMGT_cf_dtls
                      WHERE case_folder_id = a.case_Folder_id
                            AND data_point_id = 20158))
              ELSE
                 NVL (
                    (SELECT TO_NUMBER (NVL (data_point_value, 0))
                       FROM AR_CMGT_cf_dtls
                      WHERE case_folder_id = f.case_Folder_id
                            AND data_point_id = 20150)
                  , (SELECT TO_NUMBER (NVL (data_point_value, 0))
                       FROM AR_CMGT_cf_dtls
                      WHERE case_folder_id = a.case_Folder_id
                            AND data_point_id = 20150))
           END)
             total_ar
        , NVL (
             (SELECT data_point_value
                FROM AR_CMGT_cf_dtls
               WHERE case_folder_id = f.case_Folder_id
                     AND data_point_id = 213)
           , (SELECT data_point_value
                FROM AR_CMGT_cf_dtls
               WHERE case_folder_id = a.case_Folder_id
                     AND data_point_id = 213))
             credit_exposure
        , (CASE
              WHEN b.cust_account_id = -99
              THEN
                 0
              WHEN b.site_use_id = -99
              THEN
                 (SELECT overall_credit_limit
                    FROM hz_cust_profile_amts
                   WHERE     cust_account_id = b.cust_account_id
                         AND site_use_id IS NULL
                         AND ROWNUM = 1)
              ELSE
                 (SELECT overall_credit_limit
                    FROM hz_cust_profile_amts
                   WHERE     cust_account_id = b.cust_account_id
                         AND site_use_id = b.site_use_id
                         AND ROWNUM = 1)
           END)
             credit_limit
        , (CASE
              WHEN b.cust_account_id = -99 THEN 'PARTY'
              WHEN b.site_use_id = -99 THEN 'ACCOUNT'
              ELSE 'SITE'
           END)
             review_level
        , (SELECT resource_name
             FROM jtf.jtf_rs_resource_extns_tl jtfrs
            WHERE     jtfrs.language = 'US'
                  AND jtfrs.resource_id = a.credit_analyst_id
                  AND ROWNUM = 1)
             credit_analyst_name
		--start of change for ver 1.5
        , (CASE
              WHEN b.cust_account_id = -99 --PARTY
              THEN
                 NULL
              WHEN b.site_use_id = -99 --ACCOUNT
              THEN
                 (SELECT Initcap(hcpc.name)
                    FROM hz_cust_profile_classes hcpc,
                         hz_customer_profiles hcp,
                         oe_order_headers_all ooh
                    WHERE hcp.profile_class_id = hcpc.profile_class_id
                    AND   ooh.order_number = b.source_column2
                    AND   ooh.sold_to_org_id = hcp.cust_account_id
                    AND   site_use_id is null
                    AND   ROWNUM = 1)
              ELSE  --SITE
                 (SELECT Initcap(hcpc.name)
                    FROM hz_cust_profile_classes hcpc,
                         hz_customer_profiles hcp,
                         oe_order_headers_all ooh
                    WHERE hcp.profile_class_id = hcpc.profile_class_id
                    AND   ooh.order_number = b.source_column2
                    AND   ooh.sold_to_org_id = hcp.cust_account_id
                    AND   hcp.site_use_id    = ooh.invoice_to_org_id
                    AND   ROWNUM = 1)
           END) Profile_Class
		,  (CASE
              WHEN b.cust_account_id = -99 --PARTY
              THEN
                 NULL
              WHEN b.site_use_id = -99 --ACCOUNT
              THEN
                 (SELECT Initcap(hcp.account_status)
                    FROM hz_customer_profiles hcp,
                         oe_order_headers_all ooh
                    WHERE ooh.order_number = b.source_column2
                    AND   ooh.sold_to_org_id = hcp.cust_account_id
                    AND   site_use_id is null
                    AND   ROWNUM = 1)
              ELSE  --SITE
                 (SELECT Initcap(hcp.account_status)
                    FROM hz_customer_profiles hcp,
                         oe_order_headers_all ooh
                    WHERE ooh.order_number   = b.source_column2
                    AND   ooh.sold_to_org_id = hcp.cust_account_id
                    AND   hcp.site_use_id    = ooh.invoice_to_org_id
                    AND   ROWNUM = 1)
            END) Account_Status
        , (SELECT ooh.cust_po_number 
		   FROM oe_order_headers_all ooh
		   WHERE ooh.order_number = b.source_column2
		   ) ord_po_number --ver 1.5
		, (SELECT ooh.header_id 
		   FROM oe_order_headers_all ooh
		   WHERE ooh.order_number = b.source_column2
		   ) ord_header_id
		--End of change for ver 1.5
     FROM apps.ar_cmgt_case_folders a
        , apps.ar_cmgt_credit_requests b
        , apps.hz_parties c
        , apps.hz_cust_accounts d
        , apps.hz_cust_site_uses e
        , apps.ar_cmgt_case_folders f
    WHERE     a.credit_request_id = b.credit_request_id
          AND a.cust_account_id = d.cust_account_id(+)
          AND a.party_id = c.party_id
          AND a.site_use_id = e.site_use_id(+)
          AND a.TYPE = 'CASE'
          AND b.source_column3 = 'ORDER'
          AND f.credit_request_id = b.credit_request_id
          AND f.case_folder_number = a.case_folder_number
          AND f.TYPE = 'DATA'
   UNION
   SELECT a.case_folder_id
        , a.case_folder_number
        , a.status
        , a.credit_request_id
        , b.application_number
        , a.credit_analyst_id
        , a.credit_classification
        , a.cust_account_id
        , a.party_id
        , a.site_use_id
        , a.review_type
        , a.check_list_id
        , c.party_name
        , c.party_number
        , d.account_number
        , d.account_name
        , e.location site_location
        , (SELECT z.party_site_number
             FROM hz_cust_site_uses x
                , hz_cust_acct_sites y
                , hz_party_sites z
            WHERE     x.site_use_id = e.site_use_id
                  AND x.cust_acct_site_id = y.cust_acct_site_id
                  AND y.party_site_id = z.party_site_id)
             site_number
        , e.attribute1 site_type
        , (SELECT data_point_value
             FROM AR_CMGT_cf_dtls
            WHERE case_folder_id = a.case_Folder_id AND data_point_id = 20144)
             site_prelim_flag
        , (SELECT data_point_value
             FROM AR_CMGT_cf_dtls
            WHERE case_folder_id = a.case_Folder_id AND data_point_id = 20148)
             job_info_on_file
        , b.source_column2 order_number
        , oe_oe_totals_summary.PRT_ORDER_TOTAL (b.source_column1) order_total
        , (SELECT data_point_value
             FROM AR_CMGT_cf_dtls
            WHERE case_folder_id = a.case_Folder_id AND data_point_id = 15)
             pending_order_amount
        , (SELECT data_point_value
             FROM AR_CMGT_cf_dtls
            WHERE case_folder_id = a.case_Folder_id AND data_point_id = 34)
             current_ar
        , (CASE
              WHEN b.cust_account_id = -99
              THEN
                 0
              WHEN b.site_use_id = -99
              THEN
                 (SELECT TO_NUMBER (NVL (data_point_value, 0))
                    FROM AR_CMGT_cf_dtls
                   WHERE case_folder_id = a.case_Folder_id
                         AND data_point_id = 20158)
              ELSE
                 (SELECT TO_NUMBER (NVL (data_point_value, 0))
                    FROM AR_CMGT_cf_dtls
                   WHERE case_folder_id = a.case_Folder_id
                         AND data_point_id = 20150)
           END)
             total_ar
        , (SELECT data_point_value
             FROM AR_CMGT_cf_dtls
            WHERE case_folder_id = a.case_Folder_id AND data_point_id = 213)
             credit_exposure
        , (CASE
              WHEN b.cust_account_id = -99
              THEN
                 0
              WHEN b.site_use_id = -99
              THEN
                 (SELECT overall_credit_limit
                    FROM hz_cust_profile_amts
                   WHERE     cust_account_id = b.cust_account_id
                         AND site_use_id IS NULL
                         AND ROWNUM = 1)
              ELSE
                 (SELECT overall_credit_limit
                    FROM hz_cust_profile_amts
                   WHERE     cust_account_id = b.cust_account_id
                         AND site_use_id = b.site_use_id
                         AND ROWNUM = 1)
           END)
             credit_limit
        , (CASE
              WHEN b.cust_account_id = -99 THEN 'PARTY'
              WHEN b.site_use_id = -99 THEN 'ACCOUNT'
              ELSE 'SITE'
           END)
             review_level
        , (SELECT resource_name
             FROM jtf.jtf_rs_resource_extns_tl jtfrs
            WHERE     jtfrs.language = 'US'
                  AND jtfrs.resource_id = a.credit_analyst_id
                  AND ROWNUM = 1)
             credit_analyst_name
		--start of change for ver 1.5
	    , (CASE
              WHEN b.cust_account_id = -99 --PARTY
              THEN
                 NULL
              WHEN b.site_use_id = -99 --ACCOUNT
              THEN
                 (SELECT Initcap(hcpc.name)
                    FROM hz_cust_profile_classes hcpc,
                         hz_customer_profiles hcp,
                         oe_order_headers_all ooh
                    WHERE hcp.profile_class_id = hcpc.profile_class_id
                    AND   ooh.order_number = b.source_column2
                    AND   ooh.sold_to_org_id = hcp.cust_account_id
                    AND   site_use_id is null
                    AND   ROWNUM = 1)
              ELSE  --SITE
                 (SELECT Initcap(hcpc.name)
                    FROM hz_cust_profile_classes hcpc,
                         hz_customer_profiles hcp,
                         oe_order_headers_all ooh
                    WHERE hcp.profile_class_id = hcpc.profile_class_id
                    AND   ooh.order_number = b.source_column2
                    AND   ooh.sold_to_org_id = hcp.cust_account_id
                    AND   hcp.site_use_id    = ooh.invoice_to_org_id
                    AND   ROWNUM = 1)
           END) Profile_Class
		,  (CASE
              WHEN b.cust_account_id = -99 --PARTY
              THEN
                 NULL
              WHEN b.site_use_id = -99 --ACCOUNT
              THEN
                 (SELECT Initcap(hcp.account_status)
                    FROM hz_customer_profiles hcp,
                         oe_order_headers_all ooh
                    WHERE ooh.order_number = b.source_column2
                    AND   ooh.sold_to_org_id = hcp.cust_account_id
                    AND   site_use_id is null
                    AND   ROWNUM = 1)
              ELSE  --SITE
                 (SELECT Initcap(hcp.account_status)
                    FROM hz_customer_profiles hcp,
                         oe_order_headers_all ooh
                    WHERE ooh.order_number   = b.source_column2
                    AND   ooh.sold_to_org_id = hcp.cust_account_id
                    AND   hcp.site_use_id    = ooh.invoice_to_org_id
                    AND   ROWNUM = 1)
            END) Account_Status
        , (SELECT ooh.cust_po_number 
		   FROM oe_order_headers_all ooh
		   WHERE ooh.order_number = b.source_column2
		   ) ord_po_number 
        , (SELECT ooh.header_id 
		   FROM oe_order_headers_all ooh
		   WHERE ooh.order_number = b.source_column2
		   ) ord_header_id
		--End of change for ver 1.5
     FROM apps.ar_cmgt_case_folders a
        , apps.ar_cmgt_credit_requests b
        , apps.hz_parties c
        , apps.hz_cust_accounts d
        , apps.hz_cust_site_uses e
    WHERE     a.credit_request_id = b.credit_request_id
          AND a.cust_account_id = d.cust_account_id(+)
          AND a.party_id = c.party_id
          AND a.site_use_id = e.site_use_id(+)
          AND a.TYPE = 'CASE'
          AND b.source_column3 = 'ORDER'
          AND NOT EXISTS
                     (SELECT 1
                        FROM apps.ar_cmgt_case_folders f
                       WHERE     f.credit_request_id = b.credit_request_id
                             AND f.case_folder_number = a.case_folder_number
                             AND f.TYPE = 'DATA')
/