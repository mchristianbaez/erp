/*************************************************************************
  $Header XXWC_PO_FREIGHT_BURDEN_TBL_VW $
  Module Name: XXWC_PO_FREIGHT_BURDEN_TBL_VW

  PURPOSE: View created for multiorg 
  TMS # : 20141001-00260

  REVISIONS:
  Ver        Date        Author                Description
  ---------  ----------  ------------------    ----------------
  1.0        11/11/2014  Manjula Chellappan    Initial Version
**************************************************************************/
CREATE OR REPLACE FORCE VIEW APPS.XXWC_PO_FREIGHT_BURDEN_TBL_VW
(
   ORGANIZATION_ID,
   VENDOR_ID,
   VENDOR_SITE_ID,
   FREIGHT_DUTY,
   IMPORT_FLAG,
   CREATION_DATE,
   CREATED_BY,
   LAST_UPDATE_DATE,
   LAST_UPDATED_BY,
   LAST_UPDATE_LOGIN,
   FREIGHT_PER_LB,
   SOURCE_ORGANIZATION_ID,
   FREIGHT_BASIS,
   VENDOR_TYPE
)
AS
   SELECT "ORGANIZATION_ID",
          "VENDOR_ID",
          "VENDOR_SITE_ID",
          "FREIGHT_DUTY",
          "IMPORT_FLAG",
          "CREATION_DATE",
          "CREATED_BY",
          "LAST_UPDATE_DATE",
          "LAST_UPDATED_BY",
          "LAST_UPDATE_LOGIN",
          "FREIGHT_PER_LB",
          "SOURCE_ORGANIZATION_ID",
          "FREIGHT_BASIS",
          "VENDOR_TYPE"
     FROM apps.xxwc_po_freight_burden_tbl;
