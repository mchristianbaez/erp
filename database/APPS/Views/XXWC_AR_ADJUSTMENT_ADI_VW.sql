create or replace view APPS.XXWC_AR_ADJUSTMENT_ADI_VW 
/********************************************************************************
FILE NAME: APPS.XXWC_AR_ADJUSTMENT_ADI_VW.sql

OBJECT TYPE: View

PURPOSE: View to download AR Invoice Adjustments using Web ADI.

HISTORY
===============================================================================
VERSION DATE          AUTHOR(S)       DESCRIPTION
------- -----------   --------------- -----------------------------------------
1.0     01/08/2013    Gopi Damuluri    Initial version.
********************************************************************************/
as select '' CUSTOMER_NUMBER       
                 , '' CUSTOMER_NAME         
                 , '' CUSTOMER_SITE_NUMBER  
                 , '' INVOICE_SOURCE        
                 , '' TRX_NUMBER            
                 , '' AMOUNT                
                 , '' APPLY_DATE            
                 , '' TYPE                  
                 , '' ADJUSTMENT_TYPE       
                 , '' REASON                
                 , '' COMMENTS              
                 , '' CODE_COMBINATION_ID   
                 , '' CREATED_FROM          
                 , '' RECEIVABLES_TRX_ID    
              from dual;