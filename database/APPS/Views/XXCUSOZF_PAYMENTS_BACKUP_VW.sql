
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUSOZF_PAYMENTS_BACKUP_VW" ("BUSINESS_UNIT", "PAYMENT_METHOD", "NAME", "RECEIPT_REFERENCE", "MVID", "MASTER_VENDOR", "PAYMENT_NUMBER", "PAYMENT_DATE", "PAYMENT_AMOUNT", "REIMBURSEMENT_AMOUNT", "CURRENCY_CODE", "CREATION_DATE", "IMAGE_URL", "INCOME_TYPE", "RECEIPT_TIMEFRAME", "PROGRAM_YEAR") AS 
  SELECT hp1.party_name                business_unit
      ,rm.printed_name               payment_method
      ,rm.name
      ,cr.customer_receipt_reference receipt_reference
      ,hca.account_number            mvid
      ,hp.party_name                 master_vendor
      ,cr.receipt_number             payment_number
      ,cr.receipt_date               payment_date
      ,cr.amount                     payment_amount
      ,(CASE
         WHEN cr.amount = 0 THEN
          ra.amount_applied
         ELSE
          NULL
       END)                          reimbursement_amount
      ,cr.currency_code              currency_code
      ,cr.creation_date              creation_date
      ,cr.comments                   image_url
      ,decode(amv.description
             ,'COOP'
             ,'Coop'
             ,'COOP_MIN'
             ,'Coop'
             ,'Rebate')              income_type
      ,cr.attribute2                 receipt_timeframe
      ,qlhv.attribute7               program_year
  FROM ar_cash_receipts_all           cr
      ,ra_customer_trx_all            rct
      ,ar_receivable_applications_all ra
      ,ozf_claim_lines_all            ocl
      ,ra_customer_trx_lines_all      rctl
      ,apps.ozf_offers                oo
      ,qp_list_headers_vl             qlhv
      ,apps.ams_media_vl              amv
      ,ar_receipt_methods             rm
      ,hz_parties                     hp
      ,hz_cust_accounts               hca
      ,hz_parties                     hp1
 WHERE 1 = 1
   AND cr.org_id IN (101, 102)
   AND cr.receipt_method_id = rm.receipt_method_id
   AND to_number(cr.attribute1) = hp1.party_id
   AND hp1.attribute_category = '101'
   AND hp1.attribute1 = 'HDS_BU'
   AND cr.cash_receipt_id = ra.cash_receipt_id
   AND ra.display = 'Y'
   AND ra.applied_customer_trx_id = rct.customer_trx_id(+)
   --AND rct.customer_trx_id = cr.attribute3
   AND rct.customer_trx_id = rctl.customer_trx_id(+)
   AND rctl.interface_line_attribute2 = ocl.claim_id(+)
   AND ocl.activity_id = oo.qp_list_header_id(+)
   AND oo.qp_list_header_id = qlhv.list_header_id
   AND amv.media_id = oo.activity_media_id
   AND rm.name = 'REBATE_DEDUCTION'
   AND cr.status NOT IN ('REV')
   AND cr.pay_from_customer = hca.cust_account_id
   AND hca.party_id = hp.party_id
UNION ALL
SELECT hp1.party_name                business_unit
      ,rm.printed_name               payment_method
      ,rm.name
      ,cr.customer_receipt_reference receipt_reference
      ,hca.account_number            mvid
      ,hp.party_name                 master_vendor
      ,cr.receipt_number             payment_number
      ,cr.receipt_date               payment_date
      ,cr.amount                     payment_amount
      ,NULL                          reimbursement_amount
      ,cr.currency_code              currency_code
      ,cr.creation_date              creation_date
      ,cr.comments                   image_url
      ,NULL                          income_type
      ,cr.attribute2                 receipt_timeframe
      ,NULL                          program_year
  FROM ar_cash_receipts_all cr
      ,ar_receipt_methods   rm
      ,hz_parties           hp
      ,hz_cust_accounts     hca
      ,hz_parties           hp1
 WHERE 1 = 1
   AND cr.org_id IN (101, 102)
   AND cr.status NOT IN ('REV')
   AND cr.receipt_method_id = rm.receipt_method_id
   AND to_number(cr.attribute1) = hp1.party_id
   AND hp1.attribute_category = '101'
   AND hp1.attribute1 = 'HDS_BU'
   AND cr.pay_from_customer = hca.cust_account_id
   AND hca.party_id = hp.party_id
   AND rm.name = 'REBATE_CREDIT'
 ORDER BY 8
;
