/* Formatted on 11/21/2014 10:52:51 PM (QP5 v5.254.13202.43190) */
CREATE OR REPLACE VIEW XXWC_VENDOR_PRICE_ZONE_VW
AS
     SELECT vendor_id,
            vendor_site_id,
            organization_id,
            price_zone,
            organization_code,
            vendor_name,
            vendor_number,
            vendor_site_code,
            org_pricing_zone,
            org_type,
            district,
            region,
            organization_name
       FROM (SELECT xvpzt.vendor_id,
                    xvpzt.vendor_site_id,
                    xvpzt.organization_id,
                    xvpzt.price_zone,
                    mp.organization_code,
                    ass.vendor_name,
                    ass.segment1 vendor_number,
                    assa.vendor_site_code,
                    mp.attribute6 org_pricing_zone,
                    mp.attribute7 org_type,
                    mp.attribute8 district,
                    mp.attribute9 region,
                    haou.name organization_name
               FROM XXWC.XXWC_VENDOR_PRICE_ZONE_TBL xvpzt,
                    mtl_parameters mp,
                    ap_suppliers ass,
                    ap_supplier_sites_all assa,
                    org_organization_definitions ood,  -- This Table is added by Manjula on 21-Nov-2014 for Canada Changes Testing
                    hr_all_organization_units haou
              WHERE     mp.organization_id = haou.organization_id
                    AND mp.master_organization_id = 222
                    AND mp.organization_code NOT IN ('MST')
                    AND UPPER (haou.name) NOT LIKE '%CORPORATE%' -- Replaced the WCC organization code with profile value by Manjula  on 21-Nov-2014 for Canada Changes -- Replaced the WCC organization code with profile value by Pattabhi on 19-Nov-2014 for Canada Changes
                    AND xvpzt.vendor_id = ass.vendor_id
                    AND xvpzt.vendor_id = assa.vendor_id
                    AND xvpzt.vendor_site_id = assa.vendor_site_id
                    AND mp.organization_id = xvpzt.organization_id
                    AND assa.purchasing_site_flag = 'Y'
                    -- Below condition added by Pattabhi on 19-Nov-2014 for WEB ADI testing
					 AND assa.org_id = FND_PROFILE.VALUE ('ORG_ID')
					-- Below Conditions Added by Manjula on 21-Nov-2014 for Canada Changes Testing
                    AND mp.organization_id = ood.organization_id    
                    AND assa.org_id = ood.operating_unit
             UNION ALL
             SELECT ass2.vendor_id,
                    assa2.vendor_site_id,
                    mp2.organization_id,
                    NULL price_zone,
                    mp2.organization_code,
                    ass2.vendor_name vendor_name,
                    ass2.segment1 vendor_number,
                    assa2.vendor_site_code vendor_site_code,
                    mp2.attribute6 org_pricing_zone,
                    mp2.attribute7 org_type,
                    mp2.attribute8 district,
                    mp2.attribute9 region,
                    haou.name organization_name
               FROM mtl_parameters mp2,
                    ap_suppliers ass2,
                    ap_supplier_sites_all assa2,
                    org_organization_definitions ood2,  -- This Table is added by Manjula on 21-Nov-2014 for Canada Changes Testing
                    hr_all_organization_units haou
              WHERE     mp2.organization_id = haou.organization_id
                    AND mp2.master_organization_id = 222
                    AND mp2.organization_code NOT IN ('MST')
                    AND UPPER (haou.name) NOT LIKE '%CORPORATE%' -- Replaced the WCC organization code with profile value by Manjula on 21-Nov-2014 for Canada Changes
                    AND ass2.vendor_id = assa2.vendor_id
                    AND assa2.purchasing_site_flag = 'Y'
				-- Below Conditions Added by Manjula on 21-Nov-2014 for Canada Changes Testing
                    AND mp2.organization_id = ood2.organization_id
                    AND assa2.org_id = ood2.operating_unit  
                    AND assa2.org_id = FND_PROFILE.VALUE ('ORG_ID') -- Added this condition by Pattabhi based on Testing failed web ADI in EBIZFQA
                    AND NOT EXISTS
                               (SELECT *
                                  FROM XXWC.XXWC_VENDOR_PRICE_ZONE_TBL xvpzt,
                                       mtl_parameters mp,
                                       ap_suppliers ass,
                                       org_organization_definitions ood,  -- This Table is added by Manjula on 21-Nov-2014 for Canada Changes Testing
                                       ap_supplier_sites_all assa
                                 WHERE     xvpzt.vendor_id = ass.vendor_id
                                       AND xvpzt.vendor_id = assa.vendor_id
                                       AND xvpzt.vendor_site_id =
                                              assa.vendor_site_id
                                       AND mp.organization_id =
                                              xvpzt.organization_id
                                       AND mp2.organization_Id =
                                              mp.organization_id
                                       AND ass.vendor_id = ass2.vendor_id
                                       AND assa.vendor_id = assa2.vendor_id
                                       AND assa.vendor_site_id =
                                              assa2.vendor_site_id
                                       AND assa.purchasing_site_flag = 'Y'									   
									    -- Below Conditions Added by Manjula on 21-Nov-2014 for Canada Changes Testing
                                       AND mp.organization_id =
                                              ood.organization_id   
                                       AND assa.org_id = ood.operating_unit  
                                       -- Below condition added by Pattabhi on 19-Nov-2014 for WEB ADI testing
                                       AND assa.org_id =
                                              FND_PROFILE.VALUE ('ORG_ID') -- Added this condition by Pattabhi based on Testing failed web ADI in EBIZFQA
                                                                          ))
   ORDER BY vendor_id, vendor_site_id, organization_code