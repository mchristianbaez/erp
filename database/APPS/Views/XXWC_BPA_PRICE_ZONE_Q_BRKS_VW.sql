  CREATE OR REPLACE FORCE VIEW "APPS"."XXWC_BPA_PRICE_ZONE_Q_BRKS_VW" ("PO_HEADER_ID", "INVENTORY_ITEM_ID", "PRICE_ZONE_QUANTITY") AS 
  select distinct 
   XBPZBT.po_header_id
  ,XBPZBT.inventory_item_id
  ,XBPZBT.price_zone_quantity
  FROM  XXWC.XXWC_BPA_PRICE_ZONE_BREAKS_TBL XBPZBT
  --WHERE xbpzbt.price_zone_quantity != 0 --TMS Ticket 20141024-00033 - Unable to expire BPA lines with updated item description and update Price Zone with null zoneRemoved 12/29/2014 TMS Ticket # 20141024-00033 Unable to expire BPA lines with updated item description and update Price Zone with null zone
  WHERE XBPZBT.org_id=fnd_profile.value('ORG_ID')
union
select distinct
  xppzt.po_header_id
 ,xppzt.inventory_item_id
 ,0
  FROM   XXWC.XXWC_BPA_PRICE_ZONE_TBL xppzt
  where  exists (select * 
                 FROM  XXWC.XXWC_BPA_PRICE_ZONE_BREAKS_TBL XBPZBT 
                 where xbpzbt.po_header_id = xppzt.po_header_id
                 and   xbpzbt.inventory_item_id = xppzt.inventory_item_id
                 and   XBPZBT.org_id=xppzt.org_id
                 and   XBPZBT.org_id=fnd_profile.value('ORG_ID')
                 and   xbpzbt.price_zone_quantity !=0)
    and xppzt.org_id=fnd_profile.value('ORG_ID');
