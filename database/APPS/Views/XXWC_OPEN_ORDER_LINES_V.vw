/* Formatted on 12/20/2013 1:06:22 PM (QP5 v5.256.13226.35510) */
CREATE OR REPLACE FORCE VIEW XXEIS.XXWC_OPEN_ORDER_LINES_V
(
   REGION,
   DISTRICT,
   BRANCH_NUMBER,
   BRANCH_NAME,
   HEADER_ORDER_DATE,
   LINE_CREATE_DATE,
   LINE_REQUEST_DATE,
   CREATED_BY_NTID,
   CREATED_BY_NAME,
   CUSTOMER_ACCT_NUMBER,
   CUSTOMER_ACCT_NAME,
   ORDER_TYPE,
   ORDER_NUMBER,
   HEADER_STATUS,
   LINE_NUMBER,
   LINE_STATUS,
   USER_ITEM_DESCRIPTION,
   UNIT_COST,
   ORDERED_QUANTITY,
   UNIT_SELLING_PRICE,
   PRICING_QUANTITY_UOM,
   AWAITING_SHIPPING_GROUP,
   SHIP_VIA,
   DELIVERY_CREATE_DATE,
   SKU,
   ORDER_QTY,
   UOM,
   UNIT_SELL_PRICE,
   EXT_SALES_AMT,
   UNIT_COST1,
   EXT_COST
)
AS
   SELECT xxwc."REGION",
          xxwc."DISTRICT",
          xxwc."BRANCH_NUMBER",
          xxwc."BRANCH_NAME",
          xxwc."HEADER_ORDER_DATE",
          xxwc."LINE_CREATE_DATE",
          xxwc."LINE_REQUEST_DATE",
          xxwc."CREATED_BY_NTID",
          xxwc."CREATED_BY_NAME",
          xxwc."CUSTOMER_ACCT_NUMBER",
          xxwc."CUSTOMER_ACCT_NAME",
          xxwc."ORDER_TYPE",
          xxwc."ORDER_NUMBER",
          xxwc."HEADER_STATUS",
          xxwc."LINE_NUMBER",
          xxwc."LINE_STATUS",
          xxwc."USER_ITEM_DESCRIPTION",
          xxwc."UNIT_COST",
          xxwc."ORDERED_QUANTITY",
          xxwc."UNIT_SELLING_PRICE",
          xxwc."PRICING_QUANTITY_UOM",
          xxwc."AWAITING_SHIPPING_GROUP",
          xxwc."SHIP_VIA",
          xxwc."DELIVERY_CREATE_DATE",
          xxwc."SKU",
          DECODE (xxwc.Order_Type,
                  'RETURN ORDER', (xxwc.ordered_quantity * -1),
                  xxwc.ordered_quantity)
             AS Order_Qty,
          xxwc.pricing_quantity_uom AS UOM,
          DECODE (xxwc.Order_Type,
                  'RETURN ORDER', (xxwc.unit_selling_price * -1),
                  unit_selling_price)
             AS Unit_Sell_Price,
          DECODE (
             xxwc.Order_Type,
             'RETURN ORDER',   ROUND (
                                  (  xxwc.ordered_quantity
                                   * xxwc.unit_selling_price),
                                  2)
                             * -1,
             ROUND ( (xxwc.ordered_quantity * xxwc.unit_selling_price), 2))
             AS Ext_Sales_Amt,
          DECODE (xxwc.Order_Type,
                  'RETURN ORDER', (xxwc.unit_cost * -1),
                  xxwc.unit_cost)
             AS Unit_Cost1,
          DECODE (
             xxwc.Order_Type,
             'RETURN ORDER',   ROUND (
                                  (xxwc.ordered_quantity * xxwc.unit_cost),
                                  2)
                             * -1,
             ROUND ( (xxwc.ordered_quantity * xxwc.unit_cost), 2))
             AS Ext_Cost
     FROM xxeis.xxwc_open_order_lines xxwc;