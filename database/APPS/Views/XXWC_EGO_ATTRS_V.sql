/***********************************************************************************
File Name: XXWC_EGO_ATTRS_V
PROGRAM TYPE: SQL VIEW file
HISTORY
PURPOSE: View created for selecting attribute data 
====================================================================================
VERSION DATE          AUTHOR(S)       DESCRIPTION
------- -----------   --------------- ----------------------------------------------
1.0     09/23/2012    Rajiv Rathod    Initial creation of the file
************************************************************************************/

CREATE OR REPLACE FORCE VIEW APPS.XXWC_EGO_ATTRS_V
(
   ATTR_ID,
   APPLICATION_ID,
   ATTR_GROUP_TYPE,
   ATTR_GROUP_NAME,
   ATTR_NAME,
   ATTR_DISPLAY_NAME,
   DESCRIPTION,
   LAST_UPDATE_DATE,
   DATABASE_COLUMN,
   DATA_TYPE_CODE,
   SEQUENCE,
   UNIQUE_KEY_FLAG,
   DEFAULT_VALUE,
   INFO_1,
   UOM_CLASS,
   CONTROL_LEVEL,
   VALUE_SET_ID,
   VALUE_SET_NAME,
   FORMAT_CODE,
   MAXIMUM_SIZE,
   VALIDATION_CODE,
   LONGLIST_FLAG,
   ENABLED_FLAG,
   ENABLED_MEANING,
   REQUIRED_FLAG,
   REQUIRED_MEANING,
   SEARCH_FLAG,
   SEARCH_MEANING,
   DISPLAY_CODE,
   DISPLAY_MEANING,
   ATTRIBUTE_CODE,
   VIEW_IN_HIERARCHY_CODE,
   EDIT_IN_HIERARCHY_CODE,
   CUSTOMIZATION_LEVEL,
   MINIMUM_VALUE,
   MAXIMUM_VALUE,
   VALIDATION_CODE_VS,
   READ_ONLY_FLAG,
   READ_ONLY_MEANING
)
AS
   SELECT   EXT.ATTR_ID ATTR_ID,
            FL_COL.APPLICATION_ID APPLICATION_ID,
            FL_COL.DESCRIPTIVE_FLEXFIELD_NAME ATTR_GROUP_TYPE,
            FL_COL.DESCRIPTIVE_FLEX_CONTEXT_CODE ATTR_GROUP_NAME,
            FL_COL.END_USER_COLUMN_NAME ATTR_NAME,
            TL.FORM_LEFT_PROMPT ATTR_DISPLAY_NAME,
            TL.DESCRIPTION DESCRIPTION,
            ext.last_update_date,
            FL_COL.APPLICATION_COLUMN_NAME DATABASE_COLUMN,
            EXT.DATA_TYPE DATA_TYPE_CODE,
            FL_COL.COLUMN_SEQ_NUM SEQUENCE,
            EXT.UNIQUE_KEY_FLAG UNIQUE_KEY_FLAG,
            FL_COL.DEFAULT_VALUE DEFAULT_VALUE,
            EXT.INFO_1 INFO_1,
            EXT.UOM_CLASS UOM_CLASS,
            EXT.CONTROL_LEVEL CONTROL_LEVEL,
            FL_COL.FLEX_VALUE_SET_ID VALUE_SET_ID,
            FFVS.FLEX_VALUE_SET_NAME VALUE_SET_NAME,
            FFVS.FORMAT_TYPE FORMAT_CODE,
            FFVS.MAXIMUM_SIZE MAXIMUM_SIZE,
            FFVS.VALIDATION_TYPE VALIDATION_CODE,
            FFVS.LONGLIST_FLAG LONGLIST_FLAG,
            FL_COL.ENABLED_FLAG ENABLED_FLAG,
            L1.MEANING ENABLED_MEANING,
            FL_COL.REQUIRED_FLAG REQUIRED_FLAG,
            L2.MEANING REQUIRED_MEANING,
            EXT.SEARCH_FLAG SEARCH_FLAG,
            L3.MEANING SEARCH_MEANING,
            FL_COL.DISPLAY_FLAG DISPLAY_CODE,
            L4.MEANING DISPLAY_MEANING,
            EXT.ATTRIBUTE_CODE ATTRIBUTE_CODE,
            EXT.VIEW_IN_HIERARCHY_CODE VIEW_IN_HIERARCHY_CODE,
            EXT.EDIT_IN_HIERARCHY_CODE EDIT_IN_HIERARCHY_CODE,
            EXT.CUSTOMIZATION_LEVEL CUSTOMIZATION_LEVEL,
            FFVS.MINIMUM_VALUE AS MINIMUM_VALUE,
            FFVS.MAXIMUM_VALUE AS MAXIMUM_VALUE,
            DECODE (FFVS.VALIDATION_TYPE, 'X', 'I', FFVS.VALIDATION_TYPE)
               AS VALIDATION_CODE_VS,
            EXT.READ_ONLY_FLAG READ_ONLY_FLAG,
            L5.MEANING READ_ONLY_MEANING
     FROM   FND_DESCR_FLEX_COLUMN_USAGES FL_COL,
            FND_DESCR_FLEX_COL_USAGE_TL TL,
            EGO_FND_DF_COL_USGS_EXT EXT,
            FND_FLEX_VALUE_SETS FFVS,
            FND_LOOKUP_VALUES L1,
            FND_LOOKUP_VALUES L2,
            FND_LOOKUP_VALUES L3,
            FND_LOOKUP_VALUES L4,
            FND_LOOKUP_VALUES L5
    WHERE   FL_COL.APPLICATION_ID = TL.APPLICATION_ID
            AND FL_COL.DESCRIPTIVE_FLEXFIELD_NAME =
                  TL.DESCRIPTIVE_FLEXFIELD_NAME
            AND FL_COL.DESCRIPTIVE_FLEX_CONTEXT_CODE =
                  TL.DESCRIPTIVE_FLEX_CONTEXT_CODE
            AND FL_COL.APPLICATION_COLUMN_NAME = TL.APPLICATION_COLUMN_NAME
            AND FL_COL.FLEX_VALUE_SET_ID = FFVS.FLEX_VALUE_SET_ID(+)
            AND TL.LANGUAGE = USERENV ('LANG')
            AND EXT.APPLICATION_ID(+) = FL_COL.APPLICATION_ID
            AND EXT.DESCRIPTIVE_FLEXFIELD_NAME(+) =
                  FL_COL.DESCRIPTIVE_FLEXFIELD_NAME
            AND EXT.DESCRIPTIVE_FLEX_CONTEXT_CODE(+) =
                  FL_COL.DESCRIPTIVE_FLEX_CONTEXT_CODE
            AND EXT.APPLICATION_COLUMN_NAME(+) =
                  FL_COL.APPLICATION_COLUMN_NAME
            AND L1.LOOKUP_TYPE = 'YES_NO'
            AND L1.LOOKUP_CODE = FL_COL.ENABLED_FLAG
            AND L1.LANGUAGE = USERENV ('LANG')
            AND L1.VIEW_APPLICATION_ID = 0
            AND L2.LOOKUP_TYPE = 'YES_NO'
            AND L2.LOOKUP_CODE = FL_COL.REQUIRED_FLAG
            AND L2.LANGUAGE = USERENV ('LANG')
            AND L2.VIEW_APPLICATION_ID = 0
            AND L3.LOOKUP_TYPE(+) = 'YES_NO'
            AND L3.LOOKUP_CODE(+) = EXT.SEARCH_FLAG
            AND L3.LANGUAGE(+) = USERENV ('LANG')
            AND L3.VIEW_APPLICATION_ID(+) = 0
            AND L4.LOOKUP_TYPE = 'EGO_EF_DISPLAY_TYPE'
            AND L4.LOOKUP_CODE = FL_COL.DISPLAY_FLAG
            AND L4.LANGUAGE = USERENV ('LANG')
            AND L4.VIEW_APPLICATION_ID = 0
            AND L5.LOOKUP_TYPE(+) = 'YES_NO'
            AND L5.LOOKUP_CODE(+) = EXT.READ_ONLY_FLAG
            AND L5.LANGUAGE(+) = USERENV ('LANG')
            AND L5.VIEW_APPLICATION_ID(+) = 0
			/