/* Formatted on 10/1/2014 10:27:53 AM (QP5 v5.254.13202.43190) */
CREATE OR REPLACE FORCE VIEW APPS.XXWC_AR_CUSTNOTES_ADI_VW
(
   CUST_ACCOUNT_ID,
   ACCOUNT_NUMBER,
   ACCOUNT_NAME,
   CUSTOMER_ACCOUNT_STATUS,
   TOTAL_AR,
   COLLECTOR_NAME,
   NOTE1,
   NOTE2,
   NOTE3,
   NOTE4,
   NOTE5,
   ACCOUNT_CUSTOMER_SOURCE,
   ACCOUNT_CREDIT_HOLD,
   ACCOUNT_PAYMENT_TERMS,
   ACCOUNT_PROFILE_CLASS_NAME,
   ACCOUNT_CREDIT_LIMIT,
   PO_REQUIRED
)
AS
   SELECT hca.cust_account_id,
          hca.account_number,
          hca.account_name,
          (SELECT flv.meaning
             FROM fnd_lookup_values flv
            WHERE     1 = 1
                  AND flv.lookup_type = 'ACCOUNT_STATUS'
                  AND flv.lookup_code = hcp.account_status)
             customer_account_status,
          (SELECT SUM (AMOUNT_DUE_REMAINING)
             FROM apps.ar_payment_schedules apsa
            WHERE     1 = 1
                  AND hca.cust_account_id = apsa.customer_id
                  --AND apsa.org_id = 162  --01/10/2014 Commented By Veera as per Canada OU Test
				  )
             Total_AR,
          ac.name collector_name,
          hca.ATTRIBUTE17 Note1,
          hca.ATTRIBUTE18 Note2,
          hca.ATTRIBUTE19 Note3,
          hca.ATTRIBUTE20 Note4,
          hca.ATTRIBUTE16 Note5,
          hca.ATTRIBUTE4 account_customer_source,
          hcp.credit_hold Account_Credit_Hold,
          hcp.standard_terms Account_Payment_Terms,
          hcpc.name Account_Profile_Class_Name,
          hcpa.overall_credit_limit Account_Credit_limit,
          hcas.attribute3 PO_Required
     FROM hz_cust_accounts_all hca,
          apps.hz_cust_acct_sites hcas,
          hz_customer_profiles hcp,
          hz_cust_profile_classes hcpc,
          hz_cust_profile_amts hcpa,
          ar_collectors ac
    WHERE     1 = 1                     -- and hca.account_number = '32231000'
          AND hca.cust_account_id = hcp.cust_account_id
          AND hcp.site_use_id IS NULL
          AND hca.cust_account_id = hcas.cust_account_id
          AND NVL (hcas.bill_to_flag, 'N') = 'P'
          AND hcp.collector_id = ac.collector_id
          AND hcp.profile_class_id = hcpc.profile_class_id
          AND hcp.cust_account_profile_id = hcpa.cust_account_profile_id
          AND NVL (hca.attribute_category, 'No') = 'Yes';
