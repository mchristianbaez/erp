
  CREATE OR REPLACE FORCE VIEW "APPS"."XXHDS_IBY_TRXN_EXTENSIONS_V" ("ROW_ID", "TRXN_EXTENSION_ID", "PAYMENT_CHANNEL_CODE", "PAYMENT_CHANNEL_NAME", "INSTR_ASSIGNMENT_ID", "INSTRUMENT_TYPE", "INSTRUMENT_ID", "CURRENCY_CODE", "CARD_NUMBER", "CARD_NUMBER_HASH1", "CARD_NUMBER_HASH2", "CARD_EXPIRYDATE", "MASKED_CARD_EXPIRYDATE", "CARD_EXPIRED_FLAG", "CARD_ISSUER_CODE", "CARD_ISSUER_NAME", "PURCHASECARD_SUBTYPE", "CARD_HOLDER_NAME", "ACCOUNT_NUMBER", "BANK_NUMBER", "INSTRUMENT_SECURITY_CODE", "VOICE_AUTHORIZATION_FLAG", "VOICE_AUTHORIZATION_DATE", "VOICE_AUTHORIZATION_CODE", "ORIGIN_APPLICATION_ID", "ORDER_ID", "PO_NUMBER", "PO_LINE_NUMBER", "TRXN_REF_NUMBER1", "TRXN_REF_NUMBER2", "PAYMENT_SYSTEM_ORDER_NUMBER", "ADDITIONAL_INFO", "AUTHORIZED_FLAG", "AUTHORIZATION_STATUS", "SETTLED_FLAG", "SETTLEMENT_STATUS", "RETURNED_FLAG", "RETURN_STATUS", "BANK_NAME", "BANK_BRANCH_NAME", "ENCRYPTED", "CARD_EXPIRATION_STATUS", "SEQ_TYPE_LAST", "AUTHTXN_INSTRTYPE", "SETTLETXN_INSTRTYPE", "RETURNTXN_INSTRTYPE") AS 
  SELECT x.ROWID,
          x.trxn_extension_id,
          p.payment_channel_code,
          pt.payment_channel_name,
          x.instr_assignment_id,
          NVL (u.instrument_type, p.instrument_type),
          u.instrument_id,
          b.currency_code,
          c.masked_cc_number,
          c.cc_number_hash1,
          c.cc_number_hash2,
          DECODE (c.encrypted, 'A', TO_DATE (NULL), c.expirydate),
          DECODE (c.encrypted, 'A', 'XX/XX', TO_CHAR (c.expirydate, 'mm/yy')),
          DECODE (c.expirydate,
                  NULL, c.expired_flag,
                  DECODE (SIGN (c.expirydate - SYSDATE), -1, 'Y', 'N')),
          c.card_issuer_code,
          it.card_issuer_name,
          c.purchasecard_subtype,
          NVL (c.chname, hzp.party_name),
          b.masked_bank_account_num,
          bankprofile.bank_or_branch_number,
          DECODE (
             DECODE (x.instrument_security_code,
                     NULL, NULL,
                     x.instr_sec_code_length),
             NULL, NULL,
             1, 'X',
             2, 'XX',
             3, 'XXX',
             4, 'XXXX',
             'XXXXX'),
          x.voice_authorization_flag,
          x.voice_authorization_date,
          x.voice_authorization_code,
          x.origin_application_id,
          x.order_id,
          x.po_number,
          x.po_line_number,
          x.trxn_ref_number1,
          x.trxn_ref_number2,
          NVL (x.payment_system_order_number,
               SUBSTR (iby_fndcpt_trxn_pub.get_tangible_id (
                          a.application_short_name,
                          x.order_id,
                          x.trxn_ref_number1,
                          x.trxn_ref_number2),
                       1,
                       80)),
          x.additional_info,
          DECODE (authtxn.status, NULL, 'N', 'Y'),
          DECODE (authtxn.status,
                  0, 'AUTH_SUCCESS',
                  100, 'AUTH_PENDING',
                  111, 'AUTH_PENDING',
                  NULL),
          DECODE (settletxn.status, NULL, 'N', 'Y'),
          DECODE (settletxn.status,
                  0, 'SETTLEMENT_SUCCESS',
                  100, 'SETTLEMENT_PENDING',
                  111, 'SETTLEMENT_PENDING',
                  11, 'SETTLEMENT_PENDING',
                  NULL),
          DECODE (returntxn.status, NULL, 'N', 'Y'),
          DECODE (returntxn.status,
                  0, 'RETURN_SUCCESS',
                  100, 'RETURN_PENDING',
                  111, 'RETURN_PENDING',
                  NULL),
          bhzp.party_name,
          brhzp.party_name,
          DECODE (u.instrument_type,
                  'CREDITCARD', NVL (c.encrypted, 'N'),
                  NVL (b.encrypted, 'N')),
          (SELECT lkp.meaning
             FROM fnd_lookups lkp
            WHERE     lkp.lookup_type = 'IBY_CARD_EXPIRATION_STATUS'
                  AND (DECODE (
                          DECODE (
                             c.expirydate,
                             NULL, c.expired_flag,
                             DECODE (SIGN (c.expirydate - SYSDATE),
                                     -1, 'Y',
                                     'N')),
                          'Y', 'EXPIRED',
                          'N', 'UNEXPIRED')) = lkp.lookup_code),
          x.SEQ_TYPE_LAST,
          authtxn.Instrtype authtxn_Instrtype,
          settletxn.Instrtype settletxn_Instrtype,
          returntxn.Instrtype returntxn_Instrtype
     FROM iby_creditcard c,
          iby_creditcard_issuers_b i,
          iby_creditcard_issuers_tl it,
          iby_ext_bank_accounts b,
          iby_fndcpt_pmt_chnnls_b p,
          iby_fndcpt_pmt_chnnls_tl pt,
          iby_fndcpt_tx_extensions x,
          iby_pmt_instr_uses_all u,
          hz_parties hzp,
          fnd_application a,
          hz_parties bhzp,
          hz_parties brhzp,
          hz_organization_profiles bankprofile,
          (SELECT op.trxn_extension_id, summ.status, summ.Instrtype
             FROM iby_trxn_summaries_all summ, iby_fndcpt_tx_operations op
            WHERE     (summ.transactionid = op.transactionid)
                  AND (reqtype = 'ORAPMTREQ')
                  AND (status IN (0, 100, 111))
                  AND (op.transactionid =
                          (SELECT MIN (transactionid)
                             FROM iby_fndcpt_tx_operations
                            WHERE trxn_extension_id = op.trxn_extension_id))
                  AND (   (trxntypeid IN (2, 3))
                       OR (    (trxntypeid = 20)
                           AND (summ.trxnmid =
                                   (SELECT MAX (trxnmid)
                                      FROM iby_trxn_summaries_all
                                     WHERE     transactionid =
                                                  summ.transactionid
                                           AND (reqtype = 'ORAPMTREQ')
                                           AND (status IN (0, 100, 111))
                                           AND (trxntypeid = 20)))))) authtxn,
          (SELECT Op.Trxn_Extension_Id,
                  Summ.Status,
                  summ.Instrtype,
                  RANK ()
                  OVER (PARTITION BY Op.Trxn_Extension_Id
                        ORDER BY op.Transactionid)
                     RANK
             FROM Iby_Trxn_Summaries_All Summ, Iby_Fndcpt_Tx_Operations Op
            WHERE     (Summ.Transactionid = Op.Transactionid)
                  AND (   (    Instrtype IN ('CREDITCARD', 'PURCHASECARD')
                           AND Reqtype = 'ORAPMTCAPTURE')
                       OR (Instrtype = 'BANKACCOUNT' AND Trxntypeid IS NULL)
                       OR (    Instrtype = 'PINLESSDEBITCARD'
                           AND Reqtype = 'ORAPMTREQ'))
                  AND (Status IN (0, 100, 111, 11))) settletxn,
          (SELECT op.trxn_extension_id, summ.status, summ.Instrtype
             FROM iby_trxn_summaries_all summ, iby_fndcpt_tx_operations op
            WHERE     (summ.transactionid = op.transactionid)
                  AND (reqtype = 'ORAPMTRETURN')
                  AND (status IN (0, 100, 111))
                  AND (op.transactionid =
                          (SELECT MIN (transactionid)
                             FROM iby_fndcpt_tx_operations
                            WHERE trxn_extension_id = op.trxn_extension_id))) returntxn
    WHERE     (x.instr_assignment_id = u.instrument_payment_use_id(+))
          AND (DECODE (u.instrument_type,
                       'CREDITCARD', u.instrument_id,
                       NULL) = c.instrid(+))
          AND (DECODE (u.instrument_type,
                       'BANKACCOUNT', u.instrument_id,
                       NULL) = b.ext_bank_account_id(+))
          AND (x.payment_channel_code = p.payment_channel_code)
          AND (c.card_issuer_code = i.card_issuer_code(+))
          AND (c.card_owner_id = hzp.party_id(+))
          AND (x.origin_application_id = a.application_id)
          AND (b.bank_id = bhzp.party_id(+))
          AND (b.branch_id = brhzp.party_id(+))
          AND (bhzp.party_id = bankProfile.party_id(+))
          AND (I.card_issuer_code = IT.card_issuer_code(+))
          AND (IT.language(+) = USERENV ('LANG'))
          AND (P.payment_channel_code = PT.payment_channel_code)
          AND (PT.language = USERENV ('LANG'))
          AND (x.trxn_extension_id = authtxn.trxn_extension_id(+))
          AND (x.trxn_extension_id = settletxn.trxn_extension_id(+))
          AND (x.trxn_extension_id = returntxn.trxn_extension_id(+))
          AND SYSDATE BETWEEN TRUNC (bankprofile.effective_start_date(+))
                          AND NVL (TRUNC (bankprofile.effective_end_date(+)),
                                   SYSDATE + 1)
          AND Settletxn.RANK(+) = 1;
