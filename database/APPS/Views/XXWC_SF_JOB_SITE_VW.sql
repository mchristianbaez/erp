CREATE OR REPLACE VIEW APPS.XXWC_SF_JOB_SITE_VW AS
/*************************************************************************
 Copyright (c) 2012 HD Supply
 All rights reserved.
**************************************************************************
   $Header XXWC_SF_JOB_SITE_VW $
  Module Name: XXWC_SF_JOB_SITE_VW

  PURPOSE:

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)   TMS TASK        DESCRIPTION
  -- ------- -----------   ------------ ---------      -------------------------
  -- 1.0     23-JUL-2018   Pahwa, Nancy                Initially Created
TMS# 20180619-00106 
**************************************************************************/
SELECT sa.cust_acct_site_id job_id,su.primary_salesrep_id,
       s.party_site_number || ' - ' || su.location || ' ' || l.address1 || ',' ||
       l.city || ',' || l.state || ',' || l.country || ',' || l.postal_code || ' - ' ||
       (select r.sr_name
          from xxwc_sr_salesreps r
         where r.salesrep_id = su.primary_salesrep_id) Job_details,
       a.cust_account_id
  FROM hz_locations           l,
       hz_party_sites         s,
       hz_parties             p,
       hz_cust_accounts       a,
       hz_cust_acct_sites_all sa,
       hz_cust_site_uses_all  su,
       fnd_territories_vl     t
 WHERE l.location_id = s.location_id
   AND s.party_id = p.party_id
   AND a.party_id = p.party_id
   AND sa.cust_account_id = a.cust_account_id
   AND sa.party_site_id = s.party_site_id
   AND sa.cust_acct_site_id = su.cust_acct_site_id
   AND t.territory_code = l.country
   AND a.status = 'A'
   AND sa.status = 'A'
      --and su.primary_flag = 'Y'
   and su.site_use_code = 'SHIP_TO';