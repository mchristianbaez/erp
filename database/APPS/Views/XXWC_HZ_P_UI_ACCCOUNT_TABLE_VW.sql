CREATE OR REPLACE FORCE VIEW APPS.XXWC_HZ_P_UI_ACCCOUNT_TABLE_VW
(
   CUST_ACCOUNT_ID,
   ACCOUNT_NUMBER,
   ACCOUNT_NAME,
   CUSTOMER_CLASS_CODE,
   ACCOUNT_ESTABLISHED_DATE,
   PRIMARY_SALESREP_ID,
   SALES_CHANNEL_CODE,
   CUSTOMER_TYPE,
   ORIG_SYSTEM_REFERENCE,
   STANDARD_TERMS,
   PARTY_ID,
   STATUS,
   TERM_NAME,
   PROFILE_CLASS_NAME,
   ACCOUNT_MEANING,
   PRISM_ACCOUNT_NUMBER,
   CR_HOLD,
   PRIMARY_BT_ADDRESS
)
AS
   SELECT a.cust_account_id,
          a.account_number,
          a.account_name,
          HZ_UTILITY_V2PUB.get_lookupmeaning ('FND_LOOKUP_VALUES',
                                              'CUSTOMER CLASS',
                                              a.customer_class_code)
             AS CUSTOMER_CLASS_CODE,
          a.account_established_date,
          a.primary_salesrep_id,
          HZ_UTILITY_V2PUB.get_lookupmeaning ('SO_LOOKUPS',
                                              'SALES_CHANNEL',
                                              a.sales_channel_code)
             AS SALES_CHANNEL_CODE,
          HZ_UTILITY_V2PUB.get_lookupmeaning ('FND_LOOKUP_VALUES',
                                              'CUSTOMER_TYPE',
                                              a.customer_type)
             AS CUSTOMER_TYPE,
          a.orig_system_reference,
          p.standard_terms,
          a.party_id,
          a.status,
          (SELECT rt.name
             FROM RA_TERMS_TL rt
            WHERE     rt.TERM_ID = p.STANDARD_TERMS
                  AND rt.LANGUAGE = USERENV ('LANG'))
             term_name,
          (SELECT hcpc.name
             FROM HZ_CUST_PROFILE_CLASSES hcpc
            WHERE p.PROFILE_CLASS_ID = hcpc.PROFILE_CLASS_ID)
             profile_class_name,
          HZ_UTILITY_V2PUB.get_lookupmeaning ('FND_LOOKUP_VALUES',
                                              'HZ_CPUI_REGISTRY_STATUS',
                                              a.status)
             AS ACCOUNT_MEANING,
          a.attribute6 prism_account_number,
          p.credit_hold Cr_Hold,
          (SELECT DISTINCT
                  (   hzl.address1
                   || ','
                   || hzl.city
                   || ','
                   || hzl.state
                   || ','
                   || hzl.postal_code)
             FROM apps.hz_cust_site_uses hzsu,
                  apps.hz_cust_acct_sites hzas,
                  hz_party_sites hzps,
                  hz_locations hzl
            --,hz_cust_accounts hzca
            --hz_parties       hzp
            WHERE     1 = 1
               --   AND hzsu.org_id = mo_global.get_current_org_id  -- 29/09/2014 Commented by pattabhi for Canada & US OU Testing 
                  AND hzsu.primary_flag = 'Y'
                  AND hzsu.site_use_code = 'BILL_TO'
                  AND hzsu.status = 'A'
                  AND hzas.cust_acct_site_id = hzsu.cust_acct_site_id
                  AND hzas.cust_account_id = a.cust_account_id
                  AND hzps.party_site_id = hzas.party_site_id
                  AND hzl.location_id = hzps.location_id --AND hzca.cust_account_id =hzas.cust_account_id
                                             --AND hzp.party_id =hzca.party_id
                                      --AND hzp.party_name like 'LODGING%KIT%'
          )
             primary_bt_address
     FROM hz_cust_accounts a, hz_customer_profiles p
    WHERE p.cust_account_id = A.cust_account_id AND site_use_id IS NULL;
COMMENT ON TABLE APPS.XXWC_HZ_P_UI_ACCCOUNT_TABLE_VW IS 'RFC 40097';