create or replace view apps.xxwc_oracle_attachment_vw as  -----------------------------------------------------------------------------
  -- � Copyright 2008, Nancy Pahwa
  -- All Rights Reserved
  --
  -- Name           : xxwc_oracle_attachment_vw- Cleanup Orphan attachment records in Oracle EBS
  -- Date Written   : 11-May-2016
  -- Author         : Nancy Pahwa
  --
  -- Modification History:
  --
  -- Version When         Who        Did what
  -- ------- -----------  --------   -----------------------------------------------------
  -- 1.1    11-July-2017  nancypahwa   Initially Created TMS# 20170616-00237
  ---------------------------------------------------------------------------------
select entity_name, count(1) count from FND_ATTACHED_DOCUMENTS
group by entity_name;