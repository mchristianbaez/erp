
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUSOZF_SUPPLIER_CATEGORY_VW" ("CONCATENATED_SEGMENTS", "CATEGORY_ID") AS 
  SELECT DISTINCT cat.concatenated_segments ,cat.category_id
  FROM mtl_categories_kfv cat,mtl_item_categories itm
 WHERE cat.category_id=itm.category_id
   AND itm.category_set_id = 1100000041
   AND itm.organization_id= 84
;
