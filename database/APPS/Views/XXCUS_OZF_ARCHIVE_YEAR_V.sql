/*
DATE: 05/31/2016
AUTHOR: BALAGURU SESHADRI
TMS 20160519-00078 / ESMS 327004
*/
CREATE OR REPLACE FORCE VIEW APPS.XXCUS_OZF_ARCHIVE_YEAR_V
(
   PERIOD_YEAR
)
AS
     SELECT glp1.period_year
       FROM apps.gl_periods glp1,
            (SELECT (period_year - 1) fiscal_yr_for_archive
               FROM apps.gl_periods
              WHERE     1 = 1
                    AND period_set_name = '4-4-QTR'
                    AND SYSDATE BETWEEN start_date AND end_date
                    AND adjustment_period_flag = 'N') glp2
      WHERE     1 = 1
            AND glp1.period_set_name = '4-4-QTR'
            AND (    glp1.period_year < glp2.fiscal_yr_for_archive
                 AND glp1.period_year >= 2012)
            AND glp1.adjustment_period_flag = 'N'
   GROUP BY glp1.period_year
   ORDER BY glp1.period_year ASC;
--
COMMENT ON TABLE APPS.XXCUS_OZF_ARCHIVE_YEAR_V IS 'TMS 20160519-00078 / ESMS 327004';
--   