
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUSPN_PRP_SUPPORT_VW" ("COUNTRY", "LOB", "ST_PRV", "PRP", "ADDRESS", "CITY", "RER_TYPE", "PRIMARY_OPS_TYPE", "TAG", "LOB_BR", "FRU", "FRU_DESCRIPTION", "PRP_FRU", "LOB_PRP", "ATTRIBUTE4") AS 
  (
SELECT COUNTRY,
       LOB,
       ST_PRV,
       PRP,
       ADDRESS,
       CITY,
       RER_TYPE,
       PRIMARY_OPS_TYPE,
       TAG,
       LOB_BR,
       FRU,
       FRU_DESCRIPTION,
       PRP_FRU,
       LOB_PRP,
       attribute4
FROM
(
SELECT prp.country COUNTRY,
       rollups.lob_ms LOB,
       nvl(addr.province, addr.state) ST_PRV,
       prp.property_code PRP,
       addr.address_line1 || ' ' || addr.address_line2 ADDRESS,
       addr.city CITY,
       ten.meaning RER_TYPE,
       opstype.meaning PRIMARY_OPS_TYPE,
       opstype.tag TAG,
       gld.leg_seg_one LOB_BR,
       gld.l_corp_id FRU,
       gld.location FRU_DESCRIPTION,
       prp.property_code || '-' || gld.l_corp_id PRP_FRU,
       rollups.lob_ms || '-' || prp.property_code LOB_PRP,
       prp.attribute4
  FROM pn.pn_properties_all prp,
       pn.pn_locations_all loc, --xxcuspn_current_location_vw loc,   --View does Active location calcs
       pn.pn_locations_all flr,
       pn.pn_locations_all sec,
       pn.pn_space_assign_emp_all spc,
       xxcus.xxcuspn_ld_gldesc_tbl gld,
       hdsoracle.XXHSI_PNLD_LOBROLLUP@apxprd_lnk.hsi.hughessupply.com rollups,
       pn.pn_addresses_all addr,
       xxcuspn_lu_values_spacetype_vw opstype,
       xxcuspn_prim_fru_ops_type_vw primops,
       xxcuspn_lu_values_leasedown_vw ten
WHERE  prp.property_id = loc.property_id
       AND loc.location_type_lookup_code IN ('BUILDING','LAND')
       AND trunc(sysdate) between trunc(loc.active_start_date) and trunc(nvl(loc.active_end_date,sysdate))
       --AND prp.attribute4 = loc.location_code   --line that cannot be put in until dupes are addressed
       AND loc.location_id = flr.parent_location_id
       AND flr.location_type_lookup_code = 'FLOOR'
       AND flr.location_id = sec.parent_location_id
       AND sec.location_type_lookup_code = 'OFFICE'
       AND sec.location_id = spc.location_id
       AND loc.address_id = addr.address_id
       AND sec.space_type_lookup_code = opstype.lookup_code
       AND spc.cost_center_code = gld.segment2
       AND gld.segment1 = rollups.fps_id
       AND gld.l_corp_id = primops.fru
       AND opstype.tag = primops.tag
       AND prp.property_code = primops.loc_code   --CG added
       AND prp.tenure = ten.lookup_code
       AND ten.LOOKUP_CODE IN ('L','O','MI')  --Leased, Owned, and Mixed
       AND prp.property_status = 'ACTIVE'
       AND trunc(sysdate) between trunc(flr.active_start_date) and trunc(nvl(flr.active_end_date,sysdate))
       AND trunc(sysdate) between trunc(sec.active_start_date) and trunc(nvl(sec.active_end_date,sysdate))
       AND sec.active_end_date >= DATE '4712-12-30'
       AND opstype.lookup_code IN ('ADM','DIST','FAB','OFF', 'STW') --LOB HQ, Distribution Center, Fabrication, Office, Storage Warehouse,
       AND trunc(sysdate) between trunc(spc.emp_assign_start_date) and trunc(nvl(spc.emp_assign_end_date,sysdate+1))
   )
--WHERE FRU IN ('4274')
GROUP BY
       COUNTRY,
       LOB,
       ST_PRV,
       PRP,
       ADDRESS,
       CITY,
       RER_TYPE,
       PRIMARY_OPS_TYPE,
       TAG,
       LOB_BR,
       FRU,
       FRU_DESCRIPTION,
       PRP_FRU,
       LOB_PRP,
       attribute4)
;
