
/* Formatted on 10/1/2014 10:25:22 AM (QP5 v5.254.13202.43190) */
CREATE OR REPLACE FORCE VIEW APPS.XXWC_AR_LIEN_RELEASE_HDR_VW
(
   CUSTOMER_NAME,
   CUSTOMER_NUMBER,
   CITY,
   STATE,
   POSTAL_CODE,
   CONCAT_ADDRESS,
   CONCATENATED_ADDRESS,
   SITE_NUMBER,
   ORIG_SYSTEM_REFERENCE_ADD,
   SITE_USE_CODE,
   PRIMARY_FLAG,
   LOCATION,
   CUST_JOB_NUM,
   ORIG_SYSTEM_REFERENCE_SITE,
   NTO_FLAG,
   PO_NUMBER,
   CREATION_DATE,
   CUSTOMER_ID,
   ADDRESS_ID,
   SITE_USE_ID,
   BILL_TO_SITE_USE_ID,
   PRIMARY_BT_SITE_USE_ID,
   CREDIT_CLASSIFICATION
)
AS
   SELECT a.customer_name,
          a.customer_number,
          b.city,
          b.state,
          b.postal_code,
             b.address1
          || ','
          || b.address2
          || ','
          || b.address3
          || ','
          || b.address4
             concat_address,
          b.concatenated_address,
          b.site_number,
          b.orig_system_reference orig_system_reference_add,
          c.site_use_code,
          c.primary_flag,
          c.location,
          DECODE (
             SUBSTR (
                c.location,
                DECODE (INSTR (c.location, '-', 1) + 1,
                        1, LENGTH (c.location) + 1,
                        INSTR (c.location, '-', 1) + 1)),
             b.site_number, NULL,
             SUBSTR (
                c.location,
                DECODE (INSTR (c.location, '-', 1) + 1,
                        1, LENGTH (c.location) + 1,
                        INSTR (c.location, '-', 1) + 1)))
             cust_job_num,
          c.orig_system_reference orig_system_reference_site,
          b.attribute_category nto_flag,
          NULL po_number,
          c.creation_date,
          b.customer_id,
          b.address_id,
          c.site_use_id,
          c.bill_to_site_use_id,
          (SELECT site_use_id
             FROM apps.hz_cust_site_uses x, apps.hz_cust_acct_sites y
            WHERE     x.cust_acct_site_id = y.cust_acct_site_id
                  AND y.cust_account_id = a.customer_id
                  AND x.site_use_code = 'BILL_TO'
                  AND primary_flag = 'Y')
             primary_bt_site_use_id,
          (SELECT credit_classification
             FROM hz_customer_profiles p
            WHERE     p.cust_account_id = a.customer_id
                  AND p.site_use_id = c.bill_to_site_use_id)
             credit_classification
     FROM ar_customers a, xxwc_ar_addresses_v b, apps.hz_cust_site_uses c
    WHERE     a.customer_id = b.customer_id
          AND b.address_id = c.cust_acct_site_id
          AND c.site_use_code = 'SHIP_TO';
