CREATE OR REPLACE VIEW APPS.XXWC_MKTANLY_CUST_ACCT_INFO_VW AS 
/********************************************************************************************************************************
--   NAME:       apps.XXWC_MKTANLY_CUST_ACCT_INFO_VW
-- REVISIONS:
--   Ver        Date        Author           Description
--   ---------  ----------  ---------------  ------------------------------------
--   1.0        09/05/2018   P.Vamshidhar      TMS#20180907-00001 -  Building Customer Outbound MV's for Marketing Analytics
**********************************************************************************************************************************/
SELECT hca.account_number Account_Number,
  hca.account_name Customer_Account_Name,
  hca.account_established_date Account_Start_Date,
  hca.party_id,
  ACCT_GRP.Account_Grouping Account_Grouping,
  ACCT_GRP.Account_Sub_Grouping Account_Sub_Grouping,
  (SELECT attribute4
  FROM APPS.hz_cust_profile_classes
  WHERE profile_class_id = hcp.profile_class_id
  AND rownum             =1
  ) Customer_type,
  (SELECT EMAIL_ADDRESS
  FROM apps.ar_phones_v
  WHERE owner_table_name    = 'HZ_PARTIES'
  AND owner_table_id        = hp.party_id
  AND CONTACT_POINT_PURPOSE = 'BUSINESS'
  AND phone_type            = 'EMAIL'
  AND ROWNUM                = 1
  ) customer_email,
  hca.attribute9 Predominant_Trade,
  hcp.credit_classification Customer_Classification,
  hca.status Account_Status,
  hp.tax_reference Tax_ID,
  hca.attribute6 Equifax_ID,
  hp.duns_number DUNS_ID,
  TRUNC(hca.last_update_date) Last_Update_Date,
  TRUNC(hca.creation_date) Created_Date
FROM apps.hz_cust_accounts hca,
  apps.hz_customer_profiles hcp,
  apps.hz_parties hp,
  (SELECT SQ_CUST_ACCNT.CUST_ACCOUNT_ID,
    SQ_CUST_ACCNT.X_GOV_PARENT2 Account_Grouping,
    SQ_CUST_ACCNT.X_GOV_PARENT1 Account_Sub_Grouping
  FROM
    (SELECT CA.CUST_ACCOUNT_ID,
      (SELECT HZ_PARTIES_GOV1.PARTY_NAME
      FROM INTERFACE_OBI.HZ_PARTIES HZ_PARTIES_GOV1
      WHERE SQ_HZ_RELATIONSHIPS1.GOV_PARENT1_PARTY_ID=HZ_PARTIES_GOV1.PARTY_ID
      ) X_GOV_PARENT1,
      (SELECT HZ_PARTIES_GOV2.PARTY_NAME
      FROM INTERFACE_OBI.HZ_PARTIES HZ_PARTIES_GOV2
      WHERE SQ_HZ_RELATIONSHIPS1.GOV_PARENT2_PARTY_ID=HZ_PARTIES_GOV2.PARTY_ID
      ) X_GOV_PARENT2
    FROM INTERFACE_OBI.HZ_CUST_ACCOUNTS CA,
      (SELECT HZ_RELATIONSHIPS.SUBJECT_ID GOV_PARENT2_PARTY_ID,
        SQ_HZ_RELATIONSHIPS.GOV_PARENT1_PARTY_ID GOV_PARENT1_PARTY_ID,
        SQ_HZ_RELATIONSHIPS.CUST_ACCOUNT_ID CUST_ACCOUNT_ID,
        SQ_HZ_RELATIONSHIPS.GOV_PARENT_LAST_UPDATE_DATE GOV_PARENT_LAST_UPDATE_DATE,
        HZ_RELATIONSHIPS.START_DATE X_NAT_GOV_START_DATE,
        NVL(HZ_RELATIONSHIPS.END_DATE,TO_DATE('4712-12-31 12:00:00','YYYY-MM-DD HH24:MI:SS')) X_NAT_GOV_END_DATE
      FROM
        (SELECT HZ_CUST_ACCOUNTS.CUST_ACCOUNT_ID CUST_ACCOUNT_ID,
          HZ_RELATIONSHIPS.SUBJECT_ID GOV_PARENT1_PARTY_ID,
          MAX(HZ_RELATIONSHIPS.LAST_UPDATE_DATE) GOV_PARENT_LAST_UPDATE_DATE
        FROM INTERFACE_OBI.HZ_CUST_ACCOUNTS HZ_CUST_ACCOUNTS,
          INTERFACE_OBI.HZ_RELATIONSHIPS HZ_RELATIONSHIPS
        WHERE (1                                =1)
        AND (HZ_CUST_ACCOUNTS.PARTY_ID          =HZ_RELATIONSHIPS.OBJECT_ID)
        AND (HZ_RELATIONSHIPS.RELATIONSHIP_CODE ='PARENT_OF'
        AND HZ_RELATIONSHIPS.SUBJECT_TABLE_NAME ='HZ_PARTIES'
        AND HZ_RELATIONSHIPS.OBJECT_TABLE_NAME  ='HZ_PARTIES'
        AND HZ_RELATIONSHIPS.RELATIONSHIP_TYPE IN ('National Accounts Grouping','GOVERNMENT ACCOUNT GROUPING')
        AND HZ_RELATIONSHIPS.STATUS             ='A')
        GROUP BY HZ_CUST_ACCOUNTS.CUST_ACCOUNT_ID,
          HZ_RELATIONSHIPS.SUBJECT_ID
        ) SQ_HZ_RELATIONSHIPS,
        INTERFACE_OBI.HZ_RELATIONSHIPS HZ_RELATIONSHIPS
      WHERE (1                                     =1)
      AND (SQ_HZ_RELATIONSHIPS.GOV_PARENT1_PARTY_ID=HZ_RELATIONSHIPS.OBJECT_ID)
      AND (HZ_RELATIONSHIPS.RELATIONSHIP_CODE      ='PARENT_OF'
      AND HZ_RELATIONSHIPS.SUBJECT_TABLE_NAME      ='HZ_PARTIES'
      AND HZ_RELATIONSHIPS.OBJECT_TABLE_NAME       ='HZ_PARTIES'
      AND HZ_RELATIONSHIPS.RELATIONSHIP_TYPE      IN ('National Accounts Grouping','GOVERNMENT ACCOUNT GROUPING')
      AND HZ_RELATIONSHIPS.STATUS                  ='A')
      ) SQ_HZ_RELATIONSHIPS1,
      INTERFACE_OBI.HZ_PARTIES HZ_PARTIES_EMAIL,
      INTERFACE_OBI.FND_LOOKUP_VALUES FND_LOOKUP_VALUES,
      INTERFACE_OBI.HZ_CUST_PROFILE_CLASSES HZ_CUST_PROFILE_CLASSES,
      INTERFACE_OBI.HZ_CUSTOMER_PROFILES HZ_CUSTOMER_PROFILES
    WHERE (1                                  =1)
    AND (CA.CUST_ACCOUNT_ID                   =HZ_CUSTOMER_PROFILES.CUST_ACCOUNT_ID)
    AND (HZ_CUSTOMER_PROFILES.PROFILE_CLASS_ID=HZ_CUST_PROFILE_CLASSES.PROFILE_CLASS_ID)
    AND (CA.PARTY_ID                          =HZ_PARTIES_EMAIL.PARTY_ID (+))
    AND (CA.CUST_ACCOUNT_ID                   =SQ_HZ_RELATIONSHIPS1.CUST_ACCOUNT_ID (+))
    AND (CA.CUSTOMER_CLASS_CODE               =FND_LOOKUP_VALUES.LOOKUP_CODE (+))
    AND (HZ_CUSTOMER_PROFILES.SITE_USE_ID    IS NULL)
    AND (FND_LOOKUP_VALUES.LOOKUP_TYPE (+)    ='CUSTOMER CLASS')
    ) SQ_CUST_ACCNT
  WHERE SQ_CUST_ACCNT.X_GOV_PARENT1 IS NOT NULL
  ) ACCT_GRP
WHERE 1                 =1
AND hca.cust_account_id = hcp.cust_account_id (+)
AND hcp.site_use_id    IS NULL
AND hca.party_id        = hp.party_id
AND hca.cust_account_id = ACCT_GRP.CUST_ACCOUNT_ID (+)
ORDER BY hca.account_number
/