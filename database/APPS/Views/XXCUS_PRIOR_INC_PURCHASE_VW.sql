
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUS_PRIOR_INC_PURCHASE_VW" ("PRIOR_REBATE_YTD_INCOME", "PRIOR_YTD_COOP_INCOME", "PRIOR_YTD_PURCHASES", "IMVID", "INC_LOB_ID", "PUR_LOB_ID", "PMVID", "MVID_NAME", "PLOB_NAME", "ILOB_NAME", "ICALENDAR_YEAR", "PCALENDAR_YEAR", "PROGRAM_NAME") AS 
  SELECT inc.Prior_rebate_ytd_income,
    inc.Prior_ytd_coop_income,
    pur.Prior_YTD_Purchases,
    inc.mvid imvid,
    inc.lob_id inc_lob_id,
    pur.lob_id pur_lob_id,
    pur.mvid pmvid,
    pur.mvid_name,
    pur.lob_name plob_name,
    inc.lob_name ilob_name,
    inc.calendar_year icalendar_year,
    pur.calendar_year pcalendar_year,
    inc.Program_Name
  FROM
    (SELECT SUM(Prior_rebate_ytd_income)Prior_rebate_ytd_income,
      SUM(Prior_coop_ytd_income) Prior_ytd_coop_income,
      reb.mvid mvid,
      reb.mvid_name,
      reb.lob_id,
      reb.lob_name,
      reb.calendar_year calendar_year,
      reb.Program_Name Program_Name
    FROM (
      (SELECT (
        CASE
          WHEN prior_rebate_type_id NOT IN ('COOP')
          THEN rebinc.Prior_ytd_income
          ELSE 0
        END)Prior_rebate_ytd_income,
        (
        CASE
          WHEN prior_rebate_type_id IN ('COOP')
          THEN rebinc.Prior_ytd_income
          ELSE 0
        END)Prior_coop_ytd_income,
        rebinc.mvid,
        rebinc.mvid_name,
        rebinc.lob_id,
        rebinc.calendar_year,
        rebinc.lob_name,
        rebinc.Program_Name Program_Name
      FROM
        (SELECT SUM(accr2.accrued_amount) Prior_ytd_income,
          DECODE(accr2.rebate_type_id, 'COOP', 'COOP', 'COOP_MIN', 'COOP', 'REBATE') prior_rebate_type_id,
          accr1.calendar_year,
          accr2.mvid,
          accr2.lob_id,
          hzp.party_name mvid_name,
          hzp1.party_name lob_name,
          ofa.description Program_Name
        FROM xxcusozf_accruals_mv accr1,
          xxcusozf_accruals_mv accr2,
          Ozf.Ozf_Funds_All_Tl Ofa,
          ozf_offers oo,
          hz_cust_accounts hzca,
          hz_parties hzp,
          hz_parties hzp1
        WHERE 1                   =1
        AND accr1.grp_mvid        = 0
        AND accr1.grp_branch      = 1
        AND accr1.grp_adj_type    = 1
        AND accr1.grp_rebate_type = 0
        AND accr1.grp_plan_id     = 0
        AND accr1.grp_qtr         = 1
        AND accr1.grp_year        = 1
        AND accr1.grp_lob         = 0
        AND accr1.grp_cal_year    = 0
        AND accr1.grp_period      = 1
        AND accr1.mvid            = hzca.cust_account_id
        AND hzp.party_id          = hzca.party_id
        AND oo.qp_list_header_id  = accr1.plan_id
        AND oo.budget_source_id   = ofa.fund_id
        AND accr2.grp_mvid        = 0
        AND accr2.grp_branch      = 1
        AND accr2.grp_adj_type    = 1
        AND accr2.grp_rebate_type = 0
        AND accr2.grp_plan_id     = 0
        AND accr2.grp_qtr         = 1
        AND accr2.grp_year        = 1
        AND accr2.grp_lob         = 0
        AND accr2.grp_cal_year    = 0
        AND accr2.grp_period      = 1
        AND accr1.lob_id          = hzp1.party_id
        AND hzp1.attribute1       = 'HDS_LOB'
        AND accr1.lob_id(+) = accr2.lob_id
        AND accr1.plan_id(+) = accr2.plan_id
        AND accr2.calendar_year = TO_CHAR(add_months(to_date(accr1.calendar_year, 'YYYY'), -12), 'YYYY')
        AND accr2.mvid          = accr1.mvid
        GROUP BY accr1.calendar_year,
          accr2.mvid,
          accr2.lob_id,
          hzp.party_name,
          hzp1.party_name,
          accr2.rebate_type_id,
          ofa.description,
          accr1.rebate_type_id
        ) rebinc
      )reb)
    GROUP BY reb.mvid,
      reb.lob_id,
      reb.lob_name,
      reb.mvid_name,
      reb.calendar_year,
      reb.Program_Name
    )inc,
    (SELECT SUM(pm2.total_purchases) Prior_YTD_Purchases ,
      pm1.calendar_year calendar_year,
      pm2.mvid,
      pm2.lob_id,
      hp1.party_name lob_name,
      hp.party_name mvid_name
    FROM XXCUSOZF_PURCHASES_MV pm1,
      XXCUSOZF_PURCHASES_MV pm2,
      hz_parties hp,
      hz_cust_accounts hca,
      hz_parties hp1
    WHERE 1                = 1
    AND pm1.grp_mvid       = 0
    AND pm1.grp_branch     = 1
    AND pm1.grp_qtr        = 1
    AND pm1.grp_year       = 1
    AND pm1.grp_lob        = 1
    AND pm1.grp_period     = 1
    AND pm1.grp_cal_year   =0
    AND pm1.GRP_CAL_PERIOD = 1
    AND pm2.grp_mvid       = 0
    AND pm2.grp_branch     = 1
    AND pm2.grp_qtr        = 1
    AND pm2.grp_year       = 1
    AND pm2.grp_lob        = 0
    AND pm2.grp_period     = 1
    AND pm2.grp_cal_year   =0
    AND pm2.GRP_CAL_PERIOD = 1
    AND pm2.calendar_year  = TO_CHAR(add_months(to_date(pm1.calendar_year, 'YYYY'), -12), 'YYYY')
    AND pm1.mvid           = pm2.mvid
    AND pm1.mvid       = hca.cust_account_id
    AND hca.party_id   = hp.party_id
    AND hp.attribute1  = 'HDS_MVID'
    AND pm2.lob_id     = hp1.party_id
    AND hp1.attribute1 = 'HDS_LOB'
    GROUP BY pm2.mvid,
      pm2.lob_id,
      hp1.party_name,
      hp.party_name,
      pm1.calendar_year
    ) pur
  WHERE 1                  =1
  AND pur.mvid             = inc.mvid(+)
  AND inc.calendar_year(+) = pur.calendar_year
  AND inc.lob_id(+)        = pur.lob_id
;
