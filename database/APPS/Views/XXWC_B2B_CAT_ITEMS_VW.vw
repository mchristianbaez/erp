create or replace view APPS.xxwc_b2b_cat_items_vw as
/*************************************************************************
 Copyright (c) 2012 HD Supply
 All rights reserved.
**************************************************************************
   $Header xxwc_b2b_cat_items_vw $
  Module Name: xxwc_b2b_cat_items_vw 

  PURPOSE:

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)   TMS TASK        DESCRIPTION
  -- ------- -----------   ------------ ---------      -------------------------
  -- 1.0     26-Mar-2016   Pahwa, Nancy                Initially Created 
TMS# 20160223-00029   
**************************************************************************/
select inventory_item_id,DESCRIPTION item_name, SEGMENT1 Item_number, item_catalog_group_id  from MTL_SYSTEM_ITEMS_B  WHERE ORGANIZATION_ID=222
and inventory_item_status_code = 'Active';
/
