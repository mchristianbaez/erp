/* Formatted on 4/15/2014 4:53:00 PM (QP5 v5.206) */
-- Start of DDL Script for View APPS.XXWC_WC_EGO_ITEM_MST_ATTR
-- Generated 4/15/2014 4:52:54 PM from APPS@EBIZFQA

CREATE OR REPLACE VIEW apps.xxwc_wc_ego_item_mst_attr
(
    attr_group_disp_name
   ,attr_display_name
   ,multi_row_code
   ,extension_id
   ,organization_id
   ,inventory_item_id
   ,revision_id
   ,last_update_date
   ,data_level_id
   ,pk1_value
   ,pk2_value
   ,pk3_value
   ,pk4_value
   ,pk5_value
   ,attributegroup_id
   ,application_id
   ,attribute_group_name
   ,attribute_id
   ,attribute_name
   ,data_type_code
   ,attribute_char_value
   ,attribute_number_value
   ,attribute_uom_value
   ,attribute_date_value
   ,attribute_datetime_value
   ,value_set_id
   ,validation_code
   ,description
)
AS
    SELECT ag.attr_group_disp_name
          ,agc.attr_display_name
          ,ag.multi_row_code
          ,extension_id AS extension_id
          ,uda.organization_id AS organization_id
          ,uda.inventory_item_id AS inventory_item_id
          ,uda.revision_id AS revision_id
          ,uda.last_update_date AS last_update_date
          ,data_level_id AS data_level_id
          ,pk1_value AS pk1_value
          ,pk2_value AS pk2_value
          ,pk3_value AS pk3_value
          ,pk4_value AS pk4_value
          ,pk5_value AS pk5_value
          ,ag.attr_group_id AS attributegroup_id
          ,agc.application_id AS application_id
          ,ag.attr_group_name AS attribute_group_name
          ,agc.attr_id AS attribute_id
          ,agc.attr_name AS attribute_name
          ,agc.data_type_code AS data_type_code
          ,DECODE (
               agc.data_type_code
              ,'C', DECODE (agc.database_column
                           ,'C_EXT_ATTR1', uda.c_ext_attr1
                           ,'C_EXT_ATTR2', uda.c_ext_attr2
                           ,'C_EXT_ATTR3', uda.c_ext_attr3
                           ,'C_EXT_ATTR4', uda.c_ext_attr4
                           ,'C_EXT_ATTR5', uda.c_ext_attr5
                           ,'C_EXT_ATTR6', uda.c_ext_attr6
                           ,'C_EXT_ATTR7', uda.c_ext_attr7
                           ,'C_EXT_ATTR8', uda.c_ext_attr8
                           ,'C_EXT_ATTR9', uda.c_ext_attr9
                           ,'C_EXT_ATTR10', uda.c_ext_attr10
                           ,'C_EXT_ATTR11', uda.c_ext_attr11
                           ,'C_EXT_ATTR12', uda.c_ext_attr12
                           ,'C_EXT_ATTR13', uda.c_ext_attr13
                           ,'C_EXT_ATTR14', uda.c_ext_attr14
                           ,'C_EXT_ATTR15', uda.c_ext_attr15
                           ,'C_EXT_ATTR16', uda.c_ext_attr16
                           ,'C_EXT_ATTR17', uda.c_ext_attr17
                           ,'C_EXT_ATTR18', uda.c_ext_attr18
                           ,'C_EXT_ATTR19', uda.c_ext_attr19
                           ,'C_EXT_ATTR20', uda.c_ext_attr20
                           ,'C_EXT_ATTR21', uda.c_ext_attr21
                           ,'C_EXT_ATTR22', uda.c_ext_attr22
                           ,'C_EXT_ATTR23', uda.c_ext_attr23
                           ,'C_EXT_ATTR24', uda.c_ext_attr24
                           ,'C_EXT_ATTR25', uda.c_ext_attr25
                           ,'C_EXT_ATTR26', uda.c_ext_attr26
                           ,'C_EXT_ATTR27', uda.c_ext_attr27
                           ,'C_EXT_ATTR28', uda.c_ext_attr28
                           ,'C_EXT_ATTR29', uda.c_ext_attr29
                           ,'C_EXT_ATTR30', uda.c_ext_attr30
                           ,'C_EXT_ATTR31', uda.c_ext_attr31
                           ,'C_EXT_ATTR32', uda.c_ext_attr32
                           ,'C_EXT_ATTR33', uda.c_ext_attr33
                           ,'C_EXT_ATTR34', uda.c_ext_attr34
                           ,'C_EXT_ATTR35', uda.c_ext_attr35
                           ,'C_EXT_ATTR36', uda.c_ext_attr36
                           ,'C_EXT_ATTR37', uda.c_ext_attr37
                           ,'C_EXT_ATTR38', uda.c_ext_attr38
                           ,'C_EXT_ATTR39', uda.c_ext_attr39
                           ,'C_EXT_ATTR40', uda.c_ext_attr40))
               AS attribute_char_value
          , (  DECODE (
                   agc.data_type_code
                  ,'N', DECODE (agc.database_column
                               ,'N_EXT_ATTR1', uda.n_ext_attr1
                               ,'N_EXT_ATTR2', uda.n_ext_attr2
                               ,'N_EXT_ATTR3', uda.n_ext_attr3
                               ,'N_EXT_ATTR4', uda.n_ext_attr4
                               ,'N_EXT_ATTR5', uda.n_ext_attr5
                               ,'N_EXT_ATTR6', uda.n_ext_attr6
                               ,'N_EXT_ATTR7', uda.n_ext_attr7
                               ,'N_EXT_ATTR8', uda.n_ext_attr8
                               ,'N_EXT_ATTR9', uda.n_ext_attr9
                               ,'N_EXT_ATTR10', uda.n_ext_attr10
                               ,'N_EXT_ATTR11', uda.n_ext_attr11
                               ,'N_EXT_ATTR12', uda.n_ext_attr12
                               ,'N_EXT_ATTR13', uda.n_ext_attr13
                               ,'N_EXT_ATTR14', uda.n_ext_attr14
                               ,'N_EXT_ATTR15', uda.n_ext_attr15
                               ,'N_EXT_ATTR16', uda.n_ext_attr16
                               ,'N_EXT_ATTR17', uda.n_ext_attr17
                               ,'N_EXT_ATTR18', uda.n_ext_attr18
                               ,'N_EXT_ATTR19', uda.n_ext_attr19
                               ,'N_EXT_ATTR20', uda.n_ext_attr20))
             / NVL (
                   (SELECT conversion_rate
                      FROM mtl_uom_conversions uomlist
                     WHERE     uomlist.uom_code =
                                   DECODE (
                                       agc.data_type_code
                                      ,'N', DECODE (agc.database_column
                                                   ,'N_EXT_ATTR1', uda.uom_ext_attr1
                                                   ,'N_EXT_ATTR2', uda.uom_ext_attr2
                                                   ,'N_EXT_ATTR3', uda.uom_ext_attr3
                                                   ,'N_EXT_ATTR4', uda.uom_ext_attr4
                                                   ,'N_EXT_ATTR5', uda.uom_ext_attr5
                                                   ,'N_EXT_ATTR6', uda.uom_ext_attr6
                                                   ,'N_EXT_ATTR7', uda.uom_ext_attr7
                                                   ,'N_EXT_ATTR8', uda.uom_ext_attr8
                                                   ,'N_EXT_ATTR9', uda.uom_ext_attr9
                                                   ,'N_EXT_ATTR10', uda.uom_ext_attr10
                                                   ,'N_EXT_ATTR11', uda.uom_ext_attr11
                                                   ,'N_EXT_ATTR12', uda.uom_ext_attr12
                                                   ,'N_EXT_ATTR13', uda.uom_ext_attr13
                                                   ,'N_EXT_ATTR14', uda.uom_ext_attr14
                                                   ,'N_EXT_ATTR15', uda.uom_ext_attr15
                                                   ,'N_EXT_ATTR16', uda.uom_ext_attr16
                                                   ,'N_EXT_ATTR17', uda.uom_ext_attr17
                                                   ,'N_EXT_ATTR18', uda.uom_ext_attr18
                                                   ,'N_EXT_ATTR19', uda.uom_ext_attr19
                                                   ,'N_EXT_ATTR20', uda.uom_ext_attr20))
                           AND inventory_item_id = 0)
                  ,1))
               AS attribute_number_value
          ,DECODE (
               agc.data_type_code
              ,'N', DECODE (agc.database_column
                           ,'N_EXT_ATTR1', uda.uom_ext_attr1
                           ,'N_EXT_ATTR2', uda.uom_ext_attr2
                           ,'N_EXT_ATTR3', uda.uom_ext_attr3
                           ,'N_EXT_ATTR4', uda.uom_ext_attr4
                           ,'N_EXT_ATTR5', uda.uom_ext_attr5
                           ,'N_EXT_ATTR6', uda.uom_ext_attr6
                           ,'N_EXT_ATTR7', uda.uom_ext_attr7
                           ,'N_EXT_ATTR8', uda.uom_ext_attr8
                           ,'N_EXT_ATTR9', uda.uom_ext_attr9
                           ,'N_EXT_ATTR10', uda.uom_ext_attr10
                           ,'N_EXT_ATTR11', uda.uom_ext_attr11
                           ,'N_EXT_ATTR12', uda.uom_ext_attr12
                           ,'N_EXT_ATTR13', uda.uom_ext_attr13
                           ,'N_EXT_ATTR14', uda.uom_ext_attr14
                           ,'N_EXT_ATTR15', uda.uom_ext_attr15
                           ,'N_EXT_ATTR16', uda.uom_ext_attr16
                           ,'N_EXT_ATTR17', uda.uom_ext_attr17
                           ,'N_EXT_ATTR18', uda.uom_ext_attr18
                           ,'N_EXT_ATTR19', uda.uom_ext_attr19
                           ,'N_EXT_ATTR20', uda.uom_ext_attr20))
               AS attribute_uom_value
          ,TO_CHAR (
               DECODE (
                   agc.data_type_code
                  ,'X', DECODE (agc.database_column
                               ,'D_EXT_ATTR1', uda.d_ext_attr1
                               ,'D_EXT_ATTR2', uda.d_ext_attr2
                               ,'D_EXT_ATTR3', uda.d_ext_attr3
                               ,'D_EXT_ATTR4', uda.d_ext_attr4
                               ,'D_EXT_ATTR5', uda.d_ext_attr5
                               ,'D_EXT_ATTR6', uda.d_ext_attr6
                               ,'D_EXT_ATTR7', uda.d_ext_attr7
                               ,'D_EXT_ATTR8', uda.d_ext_attr8
                               ,'D_EXT_ATTR9', uda.d_ext_attr9
                               ,'D_EXT_ATTR10', uda.d_ext_attr10))
              ,'MM/DD/YYYY')
               AS attribute_date_value
          ,TO_CHAR (
               DECODE (
                   agc.data_type_code
                  ,'Y', DECODE (agc.database_column
                               ,'D_EXT_ATTR1', uda.d_ext_attr1
                               ,'D_EXT_ATTR2', uda.d_ext_attr2
                               ,'D_EXT_ATTR3', uda.d_ext_attr3
                               ,'D_EXT_ATTR4', uda.d_ext_attr4
                               ,'D_EXT_ATTR5', uda.d_ext_attr5
                               ,'D_EXT_ATTR6', uda.d_ext_attr6
                               ,'D_EXT_ATTR7', uda.d_ext_attr7
                               ,'D_EXT_ATTR8', uda.d_ext_attr8
                               ,'D_EXT_ATTR9', uda.d_ext_attr9
                               ,'D_EXT_ATTR10', uda.d_ext_attr10))
              ,'MM/DD/YYYY HH24:MI:SS')
               AS attribute_datetime_value
          ,agc.value_set_id
          ,agc.validation_code
          ,agc.description
      FROM ego_attrs_v agc, ego_attr_groups_v ag, ego_mtl_sy_items_ext_b uda
     WHERE     uda.attr_group_id = ag.attr_group_id
           AND agc.application_id = ag.application_id
           AND agc.attr_group_type = ag.attr_group_type
           AND agc.attr_group_name = ag.attr_group_name
           AND agc.data_type_code != 'A'         
           AND ag.attr_group_disp_name = 'White Cap Internal Attributes'
           AND ag.attr_group_name LIKE 'XXWC%'
           AND uda.organization_id IN (SELECT organization_id
                                         FROM org_organization_definitions
                                        WHERE organization_name = 'WC Item Master')
/

-- End of DDL Script for View APPS.XXWC_WC_EGO_ITEM_MST_ATTR
