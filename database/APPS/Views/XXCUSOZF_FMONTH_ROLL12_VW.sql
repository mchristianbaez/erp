
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUSOZF_FMONTH_ROLL12_VW" ("PERIOD_AMOUNT", "LOB", "PERIOD", "ROLLING_AMOUNT") AS 
  SELECT SUM(period_amount) period_amount,
       LOB,
       PERIOD,
       SUM(rolling_amt) rolling_amount
FROM
(SELECT (CASE WHEN a.name = b.name THEN
            accr1.accrued_amount
         ELSE
            0
         END ) period_amount,
       hp.party_name LOB,
       a.name PERIOD,
       b.name period_b,
       accr1.period_id PERIOD_ID,
       accr2.accrued_amount ROLLING_AMT
FROM xxcusozf_accruals_mv accr1,
     xxcusozf_accruals_mv accr2,
     hz_parties hp,
     ozf_time_ent_period a,
     ozf_time_ent_period b
where 1=1
AND accr1.grp_mvid=1
AND accr1.grp_branch=1
AND accr1.grp_adj_type=1
AND accr1.grp_rebate_type=1
AND accr1.grp_plan_id=0
AND accr1.grp_qtr=1
AND accr1.grp_year=1
--AND accr1.plan_id=6521
AND accr1.grp_lob=0
AND accr1.grp_period=0
AND accr2.grp_mvid=1
AND accr2.grp_branch=1
AND accr2.grp_adj_type=1
AND accr2.grp_rebate_type=1
AND accr2.grp_plan_id=0
AND accr2.grp_qtr=1
AND accr2.grp_year=1
--AND accr2.plan_id=6521
AND accr2.grp_lob=0
AND accr2.grp_period=0
AND accr1.lob_id=accr2.lob_id
AND accr1.plan_id=accr2.plan_id
AND a.ent_period_id=accr1.period_id
AND b.start_date between ADD_MONTHS(a.start_date,-12) AND a.start_date
AND b.ent_period_id <= a.ent_period_id
AND b.ent_period_id=accr2.period_id
AND hp.party_id = accr1.lob_id)
GROUP BY LOB,
         PERIOD
ORDER BY 3
;
