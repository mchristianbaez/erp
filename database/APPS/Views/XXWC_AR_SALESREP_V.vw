--//============================================================================
--//
--// Object Name         :: xxwc_ar_salesrep_v
--//
--// Object Type         :: View
--//
--// Object Description  :: This is Sales Rep extract to AgilOne
--//
--// Version Control
--//============================================================================
--// Vers    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Mahesh Kudle     26/05/2014    Initial Build - TMS#20140602-00295
--//============================================================================
CREATE OR REPLACE VIEW apps.xxwc_ar_salesrep_v
(
   operating_unit_id
  ,sales_rep
)
AS
   SELECT 162 operating_unit_id
         , (   resource_name
            || '|'
            || user_name
            || '|'
            || salesrep_id
            || '|'
            || salesrep_number
            || '|'
            || source_mgr_name
            || '|'
            || manager_ntid
            || '|'
            || account_type)
             sales_rep
     FROM xxeis.eis_xxwc_salesrep_listing_v
    WHERE account_type_p = 'ALL';