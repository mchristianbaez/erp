--
-- XXWC_AR_PAYMENT_TERMS_VW  (View)
--

CREATE OR REPLACE FORCE VIEW apps.xxwc_ar_payment_terms_vw
(
   business_unit
  ,source_system
  ,operating_unit_id
  ,operating_unit_name
  ,payterm_id
  ,payment_term_description
  ,perc_discount
  ,discount_days
  ,due_days
  ,proxy_term
  ,proxy_day
  ,proxy_month
)
AS
     SELECT 'WHITECAP' business_unit
           ,'Oracle EBS' source_system
           ,hou.organization_id operating_unit_id
           ,hou.name operating_unit_name
           ,rtt.term_id payterm_id
           ,rtt.name payment_term_description
           ,rtld.discount_percent perc_discount
           ,rtld.discount_days
           ,rtl.due_days
           , (CASE WHEN rtl.due_day_of_month IS NOT NULL THEN 'Y' ELSE NULL END)
               proxy_term
           ,rtl.due_day_of_month proxy_day
           ,rtl.due_months_forward proxy_month
       FROM ar.ra_terms_tl rtt
           ,ar.ra_terms_lines rtl
           ,ar.ra_terms_lines_discounts rtld
           ,hr_operating_units hou
      WHERE     rtt.term_id = rtl.term_id(+)
            AND rtl.term_id = rtld.term_id(+)
            AND rtl.sequence_num = rtld.sequence_num(+)
   ORDER BY rtt.name;


