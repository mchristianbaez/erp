
  CREATE OR REPLACE FORCE VIEW "APPS"."XXHDS_PERCLOSE_RECON_PERIODS_V" ("PERIOD_NAME", "PERIOD_YEAR", "PERIOD_NUM") AS 
  SELECT DISTINCT
    period_name,
    period_year, --added by Lee Spitzer 1/2/2012 to assist with sorting
    period_num  
  FROM
    org_acct_periods
  WHERE
    1           =1
  --AND open_flag ='Y' --removed by Lee Spitzer 12/28/2012 to retrieve all periods
  ORDER BY
    period_year DESC,
    period_num DESC;
