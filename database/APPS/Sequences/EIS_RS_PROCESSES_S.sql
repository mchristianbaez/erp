DECLARE
  l_process_id1 NUMBER;
  l_process_id2 NUMBER;
  l_drop_stm varchar2(250);
  l_creat_stm1 varchar2(250);
  l_creat_stm2 varchar2(250);
BEGIN
  --Step 1:
  --//Get the max process id by running below query
  select max(process_id)
  INTO L_PROCESS_ID1
  FROM XXEIS.EIS_RS_PROCESSES;
  --also run the below query to confirm that the max process id generated same in both tables or not
  select max(process_id)
  INTO l_process_id2
  FROM xxeis.eis_rs_clobs;
  --Step 2:
  --drop the current sequence by running below query
  l_drop_stm:='drop sequence xxeis.eis_rs_processes_s';
  execute immediate l_drop_stm;
  DBMS_OUTPUT.PUT_LINE( 'l_process_id1'||l_process_id1);
  DBMS_OUTPUT.PUT_LINE( 'l_process_id2'||l_process_id2);
  --Step 3:
  --// Run the below query to start the sequence from max process_id genereated. replace max process_id with <process_id> in the query
    l_creat_stm1:='create sequence xxeis.eis_rs_processes_s start with '|| l_process_id1 ||' increment by 1';
      l_creat_stm1:='create sequence xxeis.eis_rs_processes_s start with '|| l_process_id2 ||' increment by 1';
       dbms_output.put_line( 'Line1');
  if l_process_id1 >= l_process_id2   then
  dbms_output.put_line( 'Line2');
 -- l_creat_stm1:='create sequence xxeis.eis_rs_processes_s start with '|| l_process_id1 ||' increment by 1';
  dbms_output.put_line( 'Line2.1');
    execute immediate l_creat_stm1;
 DBMS_OUTPUT.PUT_LINE( 'Line3');
--create sequence xxeis.eis_rs_processes_s start with 1712165 increment by 1;
    else
    dbms_output.put_line( 'Line4');
    l_creat_stm2:='create sequence xxeis.eis_rs_processes_s start with l_process_id2 increment by 1';
execute immediate l_creat_stm2;
DBMS_OUTPUT.PUT_LINE( 'Line5');
/*CREATE sequence XXEIS.EIS_RS_PROCESSES_S start with l_process_id2 increment BY 1;*/
END IF;
END;
/