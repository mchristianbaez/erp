--
-- XXWCAP_INV_INT_STG_TBL_SEQ  (Sequence) 
--
CREATE SEQUENCE APPS.XXWCAP_INV_INT_STG_TBL_SEQ
  START WITH 1
  MAXVALUE 9999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  NOCACHE
  NOORDER;


