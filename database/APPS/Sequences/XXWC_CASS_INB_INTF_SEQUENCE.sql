 /*************************************************************************

   **************************************************************************
     $Header XXWC_CASS_INBDET_INTF_SEQUENCE $
     Module Name: XXWC_CASS_INBDET_INTF_SEQUENCE.sql

     PURPOSE:   This script creates sequences for unique identifier in staging table for CASS interface

     REVISIONS:
     Ver        Date        Author             Description
     ---------  ----------  ---------------   -------------------------
      1.0       29/03/2016  Neha Saini         Initial Build - TMS# 20160328-00016

   /*************************************************************************/
DROP SEQUENCE XXWC.XXWC_CASS_INBDET_INTF_SEQUENCE;
CREATE SEQUENCE  XXWC.XXWC_CASS_INBDET_INTF_SEQUENCE
START WITH 1000
INCREMENT BY 1;
 
DROP SEQUENCE XXWC.XXWC_CASS_INBACC_INTF_SEQUENCE; 
CREATE SEQUENCE  XXWC.XXWC_CASS_INBACC_INTF_SEQUENCE
START WITH 1000
INCREMENT BY 1;