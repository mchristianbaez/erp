/*************************************************************************

   **************************************************************************
     $Header XXWC_INV_ITEM_AVG_ONH_TRAN_SEQ $
     Module Name: XXWC_INV_ITEM_AVG_ONH_TRAN_SEQ.sql

     PURPOSE:   This script creates sequences for unique identifier 
	            in staging table for Inventory auto adjustments

     REVISIONS:
     Ver        Date        Author             Description
     ---------  ----------  ---------------   -------------------------
      1.0       03/01/2018  Pattabhi Avula     Initial Build - TMS#20171024-00015

*************************************************************************/
DROP SEQUENCE XXWC.XXWC_INV_ITEM_AVG_ONH_TRAN_SEQ
/
CREATE SEQUENCE  XXWC.XXWC_INV_ITEM_AVG_ONH_TRAN_SEQ
START WITH 1
INCREMENT BY 1   
/ 