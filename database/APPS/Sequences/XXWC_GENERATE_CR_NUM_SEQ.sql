   /**************************************************************************
    File Name:XXWC_GENERATE_CR_NUMBER.pks
    PROGRAM TYPE: PL/SQL Package specification
    PURPOSE: Create change request number
    HISTORY
    -- Description   :
    --
    --
    -- Dependencies Tables        : None
    -- Dependencies Views         : None
    -- Dependencies Sequences     : XXWC_GENERATE_CR_NUM_SEQ
    -- Dependencies Procedures    : None
    -- Dependencies Functions     : None
    -- Dependencies Packages      : None
    -- Dependencies Types         : None
    -- Dependencies Database Links: None
    ================================================================
           Last Update Date : 12/20/2013
    ================================================================
    ================================================================
    VERSION DATE          AUTHOR(S)       DESCRIPTION
    ------- -----------   --------------- ---------------------------------
    1.0     20-Dec-2013   Praveen Pawar   Initial creation of the package
    1.1     22-Jan-2014   Geoff Caesar	  remove NOCACHE attr and fix name     
   **************************************************************************/
CREATE SEQUENCE XXWC_GENERATE_CR_NUM_SEQ
  INCREMENT BY 1
  START WITH 200
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  NOCYCLE
  NOCACHE;