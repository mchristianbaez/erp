--
-- Create Schema Script 
--   Database Version          : 11.2.0.2.0 
--   Database Compatible Level : 11.1.0 
--   Toad Version              : 11.5.0.56 
--   DB Connect String         : EBIZPRD 
--   Schema                    : APPS 
--   Script Created by         : XXWC_DEV_ADMIN 
--   Script Created at         : 6/12/2012 8:53:43 AM 
--   Physical Location         :  
--   Notes                     :  
--

-- Object Counts: 
--   Functions: 1       Lines of Code: 118 
--   Indexes: 3         Columns: 4          
--   Java Classes: 1 
--   Java Sources: 1 
--   Materialized Views: 2 
--   Object Privileges: 24 
--   Packages: 64       Lines of Code: 6605 
--   Package Bodies: 64 Lines of Code: 90725 
--   Procedures: 2      Lines of Code: 146 
--   Sequences: 4 
--   Synonyms: 63 
--   Tables: 11         Columns: 103        Constraints: 1      
--   Triggers: 1 
--   Views: 69          Columns: 796        



-- "Set define off" turns off substitution variables. 
Set define off; 


@./Sequences/XXWCAR_CASH_RCPTS_S.sql;
@./Sequences/XXWC_AP_DCTM_BATCH_S.sql;
@./Sequences/XXWC_AR_INV_STG_TBL_SEQ.sql;
@./Sequences/XXWC_PARAM_LIST_S.sql;
@./Tables/XXWC_ACCOUNT_STATUS_BKUP.sql;
@./Tables/XXWC_AR_INVOICES_CONV_BKUP.sql;
@./Tables/XXWC_COLLECTOR_CR_ANALYST_BKUP.sql;
@./Tables/XXWC_CUST_CLASSIFICATION_BKUP.sql;
@./Tables/XXWC_GETPAID_ARCUST_V_TBL.sql;
@./Tables/XXWC_GETPAID_AREXTI_V_TBL.sql;
@./Tables/XXWC_GETPAID_ARMAST_V_TBL.sql;
@./Tables/XXWC_MSI_060912.sql;
@./Tables/XXWC_PARAM_LIST.sql;
@./Indexes/XXWC_MSI_060912_N1.sql;
@./Indexes/XXWC_PARAM_LIST_IDX2.sql;
@./Indexes/XXWC_PARAM_LIST_PK.sql;
@./Packages/XXWCAP_INV_INT_PKG.pks;
@./Packages/XXWCAR_DCTM_PKG.pks;
@./Packages/XXWCAR_OCM_DNB_DATAPOINTS.pks;
@./Packages/XXWCAR_OCM_ORDR_HLD_DATAPOINTS.pks;
@./Packages/XXWCINV_ECO_APPROVAL_WF_PKG.pks;
@./Packages/XXWCINV_INV_AGING_PKG.pks;
@./Packages/XXWCINV_ITEM_MASS_UPLOAD_PKG.pks;
@./Packages/XXWCMRP_DEMAND_FORECAST_PKG.pks;
@./Packages/XXWCMSC_MSC_PUB_SAFETY_STK_PKG.pks;
@./Packages/XXWC_AR_CAO_ASSIGN_PKG.pks;
@./Packages/XXWC_AR_CASH_RECEIPT_LB_UTIL.pks;
@./Packages/XXWC_AR_PRISM_REFUND_PKG.pks;
@./Packages/XXWC_CMGT_CASE_FOLDER_PK.pks;
@./Packages/XXWC_CREDIT_COPY_TRX_UTIL_PKG.pks;
@./Packages/XXWC_CUST_CREDIT_LIMIT_PKG.pks;
@./Packages/XXWC_EDI_IFACE_PKG.pks;
@./Packages/XXWC_GEN_ROUTINES_PKG.pks;
@./Packages/XXWC_INV_UPDATE_DFF_PKG.pks;
@./Packages/XXWC_MRPRPROP_PKG.pks;
@./Packages/XXWC_MV_ROUTINES_PKG.pks;
@./Packages/XXWC_OB_COMMON_PKG.pks;
@./Packages/XXWC_OE_INV_IFACE_PVT.pks;
@./Packages/XXWC_OE_INV_IFACE_WF.pks;
@./Packages/XXWC_ONT_ROUTINES_PKG.pks;
@./Packages/XXWC_OZF_VENDOR_QUOTE_PKG.pks;
@./Packages/XXWC_PO_DOCUMENT_UTIL.pks;
@./Packages/XXWC_PO_FREIGHT_BURDEN_PKG.pks;
@./Packages/XXWC_PRICE_LIST_WEB_ADI_PKG.pks;
@./Packages/XXWC_RCV_UPDATE_LIST_PRICE.pks;
@./Packages/XXWC_RENTAL_ENGINE_PKG.pks;
@./Packages/XXWC_RENTAL_ITEMS_MASS_ADD_PKG.pks;
@./Packages/XXWC_TAX_BATCHTOSTEP.pks;
@./Packages/XXWC_WF_UTIL_PKG.pks;
@./Packages/XXWC_WSH_BACKORD_TXNS_PKG.pks;
@./Packages/XXWC_WSH_DOCUMENT_SETS.pks;
@./Packages/XXWC_WSH_PR_PICK_SLIP_NUMBER.pks;
@./PackageBodies/XXWCAP_INV_INT_PKG.pkb;
@./PackageBodies/XXWCAR_DCTM_PKG.pkb;
@./PackageBodies/XXWCAR_OCM_DNB_DATAPOINTS.pkb;
@./PackageBodies/XXWCAR_OCM_ORDR_HLD_DATAPOINTS.pkb;
@./PackageBodies/XXWCINV_ECO_APPROVAL_WF_PKG.pkb;
@./PackageBodies/XXWCINV_INV_AGING_PKG.pkb;
@./PackageBodies/XXWCINV_ITEM_MASS_UPLOAD_PKG.pkb;
@./PackageBodies/XXWCMRP_DEMAND_FORECAST_PKG.pkb;
@./PackageBodies/XXWCMSC_MSC_PUB_SAFETY_STK_PKG.pkb;
@./PackageBodies/XXWC_AR_CAO_ASSIGN_PKG.pkb;
@./PackageBodies/XXWC_AR_CASH_RECEIPT_LB_UTIL.pkb;
@./PackageBodies/XXWC_AR_PRISM_REFUND_PKG.pkb;
@./PackageBodies/XXWC_CMGT_CASE_FOLDER_PK.pkb;
@./PackageBodies/XXWC_CREDIT_COPY_TRX_UTIL_PKG.pkb;
@./PackageBodies/XXWC_CUST_CREDIT_LIMIT_PKG.pkb;
@./PackageBodies/XXWC_EDI_IFACE_PKG.pkb;
@./PackageBodies/XXWC_GEN_ROUTINES_PKG.pkb;
@./PackageBodies/XXWC_INV_UPDATE_DFF_PKG.pkb;
@./PackageBodies/XXWC_MRPRPROP_PKG.pkb;
@./PackageBodies/XXWC_MV_ROUTINES_PKG.pkb;
@./PackageBodies/XXWC_OB_COMMON_PKG.pkb;
@./PackageBodies/XXWC_OE_INV_IFACE_PVT.pkb;
@./PackageBodies/XXWC_OE_INV_IFACE_WF.pkb;
@./PackageBodies/XXWC_ONT_ROUTINES_PKG.pkb;
@./PackageBodies/XXWC_OZF_VENDOR_QUOTE_PKG.pkb;
@./PackageBodies/XXWC_PO_DOCUMENT_UTIL.pkb;
@./PackageBodies/XXWC_PO_FREIGHT_BURDEN_PKG.pkb;
@./PackageBodies/XXWC_PRICE_LIST_WEB_ADI_PKG.pkb;
@./PackageBodies/XXWC_RCV_UPDATE_LIST_PRICE.pkb;
@./PackageBodies/XXWC_RENTAL_ENGINE_PKG.pkb;
@./PackageBodies/XXWC_RENTAL_ITEMS_MASS_ADD_PKG.pkb;
@./PackageBodies/XXWC_TAX_BATCHTOSTEP.pkb;
@./PackageBodies/XXWC_WF_UTIL_PKG.pkb;
@./PackageBodies/XXWC_WSH_BACKORD_TXNS_PKG.pkb;
@./PackageBodies/XXWC_WSH_DOCUMENT_SETS.pkb;
@./PackageBodies/XXWC_WSH_PR_PICK_SLIP_NUMBER.pkb;
@./Functions/XXWC_SLA_GET_SHIP_STATE.fnc;
@./JavaSources/XXWC_OSCommand.jvs;
@./JavaClasses/XXWC_OSCommand.sql;
@./Views/XXWCAP_DCTM_VENDOR_VW.vw;
@./Views/XXWCINV_ITEM_CAT_WEBADI_VW.vw;
@./Views/XXWCMRP_SRC_RULES_WEBADI_VW.vw;
@./Views/XXWCPO_DCTM_PURCHASING_VW.vw;
@./Views/XXWC_AR_CREDIT_BUREAU_VW.vw;
@./Views/XXWC_AR_RECEIPT_METHOD_CASH_VW.vw;
@./Views/XXWC_AR_RECEIPT_METHOD_MISC_VW.vw;
@./Views/XXWC_AR_RECPT_METHOD_CASHBR_VW.vw;
@./Views/XXWC_BILL_TRUST_INV_HEADERS_VW.vw;
@./Views/XXWC_BILL_TRUST_INV_LINES_VW.vw;
@./Views/XXWC_BILL_TRUST_INV_NOTES_VW.vw;
@./Views/XXWC_BILL_TRUST_STMTS_VW.vw;
@./Views/XXWC_BPA_PO_LINES_VW.vw;
@./Views/XXWC_BPA_PRICE_BREAK_VW.vw;
@./Views/XXWC_CUSTOMER_BALANCE_VW.vw;
@./Views/XXWC_EBS2INTVEN_V.vw;
@./Views/XXWC_EDI_TP_V.vw;
@./Views/XXWC_GETPAID_ARCASH_TEST_V.vw;
@./Views/XXWC_GETPAID_ARCASH_V.vw;
@./Views/XXWC_GETPAID_ARCUST_TEST_V.vw;
@./Views/XXWC_GETPAID_ARCUST_V.vw;
@./Views/XXWC_GETPAID_AREXTI_TEST_V.vw;
@./Views/XXWC_GETPAID_AREXTI_V.vw;
@./Views/XXWC_GETPAID_ARMAST_TEST_V.vw;
@./Views/XXWC_GETPAID_ARMAST_V.vw;
@./Views/XXWC_GETPAID_ARTRAN_TEST_V.vw;
@./Views/XXWC_GETPAID_ARTRAN_V.vw;
@./Views/XXWC_INV_LOTLABLES_REP_V.vw;
@./Views/XXWC_ITEM_CATEGORY_VW.vw;
@./Views/XXWC_MTL_PRICING_ZONE_VW.vw;
@./Views/XXWC_MTL_SYSTEM_ITEMS_EHS_V.vw;
@./Views/XXWC_MTL_SYSTEM_ITEMS_PO_V.vw;
@./Views/XXWC_OE_ITEMS_V.vw;
@./Views/XXWC_OM_CASH_RECEIPT_V.vw;
@./Views/XXWC_OM_CASH_REFUND_V.vw;
@./Views/XXWC_OM_RECEIPT_APP_V.vw;
@./Views/XXWC_PO_EDI_V.vw;
@./Views/XXWC_PO_FREIGHT_BURDEN_VW.vw;
@./Views/XXWC_PO_VENDOR_VW.vw;
@./Views/XXWC_PRDOUCT_ATTRIBUTE_VW.vw;
@./Views/XXWC_QP_MODIFIERS_LIST_VW.vw;
@./Views/XXWC_QP_PRICE_LIST_CONV_VW.vw;
@./Views/XXWC_RETURN_TO_VENDOR_V.vw;
@./Views/XXWC_RICE_RECON_VIEW.vw;
@./Views/XXWC_SV_ITEM_CATEGORIES_V.vw;
@./Triggers/XXWC_PARAM_LIST_WHO.trg;
@./Synonym/XXWC_AR_PRISM_RENTAL_LINE_S.sql;
@./Synonym/XXWC_AR_PRISM_RENTAL_LN_STG.sql;
@./Synonym/XXWC_AR_PRISM_RENTAL_ORD_S.sql;
@./Synonym/XXWC_SHARE_VARIABLES.sql;
@./Synonym/XXWC_SALES_VELOCITY_GT.sql;
@./Synonym/XXWC_REMIT_TO_ADDR_XREF.sql;
@./Synonym/XXWC_QP_MODIFIER_LINE_CNV.sql;
@./Synonym/XXWC_QP_MODIFIER_INT.sql;
@./Synonym/XXWC_QP_MODIFIER_HDR_CNV.sql;
@./Synonym/XXWC_QP_BATCH_S.sql;
@./Synonym/XXWC_PRICING_GUARD_RAILS_GT.sql;
@./Synonym/XXWC_PO_VENDOR_MINIMUM.sql;
@./Synonym/XXWC_PO_SEL_ITEMS_TBL.sql;
@./Synonym/XXWC_PO_SEL_ITEMS_S.sql;
@./Synonym/XXWC_PGR_MESSAGE.sql;
@./Synonym/XXWC_PAYMENT_TERMS_XREF.sql;
@./Synonym/XXWC_PARTS_ON_FLY_GT.sql;
@./Synonym/XXWC_ONHAND_LOT_S.sql;
@./Synonym/XXWC_ONHAND_LOT_CNV.sql;
@./Synonym/XXWC_ONHAND_BAL_CNV.sql;
@./Synonym/XXWC_OM_PRICING_GUARDRAIL_S.sql;
@./Synonym/XXWC_OM_PRICING_GUARDRAIL.sql;
@./Synonym/XXWC_MSTR_PARTS_EXCL_LIST_S.sql;
@./Synonym/XXWC_MSTR_PARTS_EXCL_LIST.sql;
@./Synonym/XXWC_MRPRPROP_S.sql;
@./Synonym/XXWC_MRPRPROP.sql;
@./Synonym/XXWC_LEGAL_COLLECTION_IND_XREF.sql;
@./Synonym/XXWC_INV_PRODUCT_REQUEST_S.sql;
@./Synonym/XXWC_INV_PRODUCT_REQUEST.sql;
@./Synonym/XXWC_INV_PRODUCT_ORG.sql;
@./Synonym/XXWC_INV_CYCLECOUNT_LINE_DTL_S.sql;
@./Synonym/XXWC_INV_CYCLECOUNT_LINE_DTLS.sql;
@./Synonym/XXWC_INV_CYCLECOUNT_LINES_S.sql;
@./Synonym/XXWC_INV_CYCLECOUNT_LINES.sql;
@./Synonym/XXWC_INV_CYCLECOUNT_HDR_S.sql;
@./Synonym/XXWC_INV_CYCLECOUNT_HDR.sql;
@./Synonym/XXWC_INV_CYCLECOUNT_DAYS.sql;
@./Synonym/XXWC_INV_CC_SPECIFIC_LINES_S.sql;
@./Synonym/XXWC_INV_CC_SPECIFIC_LINES.sql;
@./Synonym/XXWC_INV_CC_PARTS_EXCL.sql;
@./Synonym/XXWC_INV_CC_BLOCKOUT_DATE_S.sql;
@./Synonym/XXWC_INV_CC_BLOCKOUT_DATES.sql;
@./Synonym/XXWC_INV_CC_BIN_EXCL.sql;
@./Synonym/XXWC_INVITEM_XREF_STG.sql;
@./Synonym/XXWC_INVITEM_STG.sql;
@./Synonym/XXWC_CUST_PROFILE_CLASS_XREF.sql;
@./Synonym/XXWC_CUST_CLASSIFICATION_XREF.sql;
@./Synonym/XXWC_CUSTOMER_INT_ERRORS.sql;
@./Synonym/XXWC_CR_CLASSIFICATION_XREF.sql;
@./Synonym/XXWC_COLLECTOR_CR_ANALYST_XREF.sql;
@./Synonym/XXWC_CC_STORE_LOCATIONS.sql;
@./Synonym/XXWC_CALC_UPDATE_LEAD_TIME.sql;
@./Synonym/XXWC_AR_SITES_CNV.sql;
@./Synonym/XXWC_AR_RECEIPTS_CONV.sql;
@./Synonym/XXWC_AR_HOUSE_ACCOUNTS_XREF.sql;
@./Synonym/XXWC_AR_CUSTOMERS_CNV.sql;
@./Synonym/XXWC_AR_CREDIT_COPY_TRX_S.sql;
@./Synonym/XXWC_AR_CREDIT_COPY_TRX.sql;
@./Synonym/XXWC_AR_CREATE_PROFILE_S.sql;
@./Synonym/XXWC_AR_CONTACT_POINTS_CNV.sql;
@./Synonym/XXWC_AP_INVOICE_INT.sql;
@./Synonym/XXWC_ACCOUNT_STATUS_XREF.sql;
@./Packages/XXWC_AR_BILL_TRUST_INTF_PKG.pks;
@./Packages/XXWC_AR_CUSTOMER_BALANCES_PKG.pks;
@./Packages/XXWC_AR_GETPAID_OB_PKG.pks;
@./Packages/XXWC_AR_INV_INT_PKG.pks;
@./Packages/XXWC_AR_OPEN_RECEIPTS_CONV_PKG.pks;
@./Packages/XXWC_AR_PRISM_CUST_IFACE_PKG.pks;
@./Packages/XXWC_ASCP_SCWB_PKG.pks;
@./Packages/XXWC_CREDIT_COPY_TRX_PKG.pks;
@./Packages/XXWC_CUSTOMER_CONVERSION_PKG.pks;
@./Packages/XXWC_INV_CC_BLOCKOUT_DATES_PKG.pks;
@./Packages/XXWC_INV_CYCLE_COUNT_PKG.pks;
@./Packages/XXWC_INV_NEW_PROD_REQ_PKG.pks;
@./Packages/XXWC_INV_ONHAND_CONV_PKG.pks;
@./Packages/XXWC_INV_PARTS_ON_FLY_PKG.pks;
@./Packages/XXWC_INV_SALES_VELOCITY_PKG.pks;
@./Packages/XXWC_ITEMCATEGCONV_PKG.pks;
@./Packages/XXWC_LEAD_TIME_PKG.pks;
@./Packages/XXWC_MISC_RECEIPT_IFACE_PKG.pks;
@./Packages/XXWC_MSTR_PARTS_EXCL_LIST_PKG.pks;
@./Packages/XXWC_OM_PRICING_GUARDRAIL_PKG.pks;
@./Packages/XXWC_POSOURCINGCONV_PKG.pks;
@./Packages/XXWC_QP_MODIFIER_WEB_ADI_PKG.pks;
@./Packages/XXWC_RENTAL_CREATE_BILLING_PKG.pks;
@./Packages/XXWC_WSH_PICK_LIST.pks;
@./PackageBodies/XXWC_AR_BILL_TRUST_INTF_PKG.pkb;
@./PackageBodies/XXWC_AR_CUSTOMER_BALANCES_PKG.pkb;
@./PackageBodies/XXWC_AR_GETPAID_OB_PKG.pkb;
@./PackageBodies/XXWC_AR_INV_INT_PKG.pkb;
@./PackageBodies/XXWC_AR_OPEN_RECEIPTS_CONV_PKG.pkb;
@./PackageBodies/XXWC_AR_PRISM_CUST_IFACE_PKG.pkb;
@./PackageBodies/XXWC_ASCP_SCWB_PKG.pkb;
@./PackageBodies/XXWC_CREDIT_COPY_TRX_PKG.pkb;
@./PackageBodies/XXWC_CUSTOMER_CONVERSION_PKG.pkb;
@./PackageBodies/XXWC_INV_CC_BLOCKOUT_DATES_PKG.pkb;
@./PackageBodies/XXWC_INV_CYCLE_COUNT_PKG.pkb;
@./PackageBodies/XXWC_INV_NEW_PROD_REQ_PKG.pkb;
@./PackageBodies/XXWC_INV_ONHAND_CONV_PKG.pkb;
@./PackageBodies/XXWC_INV_PARTS_ON_FLY_PKG.pkb;
@./PackageBodies/XXWC_INV_SALES_VELOCITY_PKG.pkb;
@./PackageBodies/XXWC_ITEMCATEGCONV_PKG.pkb;
@./PackageBodies/XXWC_LEAD_TIME_PKG.pkb;
@./PackageBodies/XXWC_MISC_RECEIPT_IFACE_PKG.pkb;
@./PackageBodies/XXWC_MSTR_PARTS_EXCL_LIST_PKG.pkb;
@./PackageBodies/XXWC_OM_PRICING_GUARDRAIL_PKG.pkb;
@./PackageBodies/XXWC_POSOURCINGCONV_PKG.pkb;
@./PackageBodies/XXWC_QP_MODIFIER_WEB_ADI_PKG.pkb;
@./PackageBodies/XXWC_RENTAL_CREATE_BILLING_PKG.pkb;
@./PackageBodies/XXWC_WSH_PICK_LIST.pkb;
@./Procedures/XXWCAR_CUSTOMER_INTERFACE_PRC.prc;
@./Procedures/XXWCAR_INVOICE_INTERFACE_PRC.prc;
@./Views/XXWC_ADI_PRICING_GUARDRAIL_VW.vw;
@./Views/XXWC_AR_CREDIT_COPY_TRX_LOV_VW.vw;
@./Views/XXWC_AR_CREDIT_COPY_TRX_VW.vw;
@./Views/XXWC_GETPAID_ARCASH_LOG_V.vw;
@./Views/XXWC_GETPAID_ARCUST_LOG_V.vw;
@./Views/XXWC_GETPAID_AREXTI_LOG_V.vw;
@./Views/XXWC_GETPAID_ARMAST_LOG_V.vw;
@./Views/XXWC_GETPAID_ARTRAN_LOG_V.vw;
@./Views/XXWC_INV_CC_AUTO_DETAILS_VW.vw;
@./Views/XXWC_INV_CC_BLOCKOUT_DATES_VW.vw;
@./Views/XXWC_INV_CC_SPECIFIC_LINES_VW.vw;
@./Views/XXWC_INV_CYCLECOUNT_HDR_VW.vw;
@./Views/XXWC_INV_CYCLECOUNT_LINES_V.vw;
@./Views/XXWC_INV_ITEM_SEARCH_V.vw;
@./Views/XXWC_MSTR_PARTS_EXCL_LIST_VW.vw;
@./Views/XXWC_OM_PRICING_GUARDRAIL_VW.vw;
@./Views/XXWC_PO_SEL_ONHAND_VW.vw;
@./Views/XXWC_PO_SEL_VENDOR_ITEM_VW.vw;
@./Views/XXWC_PO_SEL_VENDOR_VW.vw;
@./Views/XXWC_PO_VENDOR_MINADI_VW.vw;
@./Views/XXWC_PO_VENDOR_MINIMUM_VW.vw;
@./Views/XXWC_PRICING_GUARDRAIL_VW.vw;
@./Views/XXWC_QP_MODIFIER_INT_BATCH_VW.vw;
@./Views/XXWC_QP_MODIFIER_LIST_INT_VW.vw;
@./Synonym/XXWC_AR_PRISM_CUST_IFACE_PKG.sql;
@./Packages/XXWC_AP_INVOICE_CONV_PKG.pks;
@./Packages/XXWC_AP_SUPPLIERS_CONV_PKG.pks;
@./Packages/XXWC_AR_CASH_REFUND_PKG.pks;
@./Packages/XXWC_AR_INVOICE_CONV_PKG.pks;
@./PackageBodies/XXWC_AP_INVOICE_CONV_PKG.pkb;
@./PackageBodies/XXWC_AP_SUPPLIERS_CONV_PKG.pkb;
@./PackageBodies/XXWC_AR_CASH_REFUND_PKG.pkb;
@./PackageBodies/XXWC_AR_INVOICE_CONV_PKG.pkb;
@./Constraints/XXWC_PARAM_LIST_NonFK.sql;
@./MaterializedViews/XXWC_AR_CREDIT_BUREAU_MV.sql;
@./MaterializedViews/XXWCINV_INV_ONHAND_MV.sql;
@./Grants/XXWC_AR_CREDIT_BUREAU_MV_TO_APPS_DELETE_ROLE.sql;
@./Grants/XXWC_AR_CREDIT_BUREAU_MV_TO_APPS_INSERT_ROLE.sql;
@./Grants/XXWC_AR_CREDIT_BUREAU_MV_TO_APPS_SELECT_ROLE.sql;
@./Grants/XXWC_AR_CREDIT_BUREAU_MV_TO_APPS_UPDATE_ROLE.sql;
@./Grants/XXWC_EDI_TP_V_TO_INTERFACE_BIZTALK.sql;
@./Grants/XXWC_PO_EDI_V_TO_INTERFACE_BIZTALK.sql;
@./Grants/XXWCAR_DCTM_PKG_TO_INTERFACE_XXCUS.sql;
@./Grants/XXWC_OB_COMMON_PKG_TO_INTERFACE_XXCUS.sql;
@./Grants/XXWCAP_INV_INT_PKG_TO_XXCUS_EXECUTE_ROLE.sql;
@./Grants/XXWCAR_DCTM_PKG_TO_XXCUS_EXECUTE_ROLE.sql;
@./Grants/XXWC_OB_COMMON_PKG_TO_XXCUS_EXECUTE_ROLE.sql;
@./Grants/XXWCAP_DCTM_VENDOR_VW_TO_XXCUS_SELECT_MSSQL.sql;
@./Grants/XXWCPO_DCTM_PURCHASING_VW_TO_XXCUS_SELECT_MSSQL.sql;
@./Grants/XXWC_AR_CASH_RECEIPT_LB_UTIL_TO_XXWC_PRISM_EXECUTE_ROLE.sql;
@./Grants/XXWC_EDI_IFACE_PKG_TO_XXWC_PRISM_EXECUTE_ROLE.sql;
@./Grants/XXWC_AR_PRISM_CUST_IFACE_PKG_TO_INTERFACE_PRISM.sql;
@./Grants/XXWC_AR_BILL_TRUST_INTF_PKG_TO_INTERFACE_XXCUS.sql;
@./Grants/XXWC_AR_PRISM_CUST_IFACE_PKG_TO_INTERFACE_XXCUS.sql;
@./Grants/XXWC_AR_BILL_TRUST_INTF_PKG_TO_XXCUS_EXECUTE_ROLE.sql;
@./Grants/XXWC_AR_GETPAID_OB_PKG_TO_XXCUS_EXECUTE_ROLE.sql;
@./Grants/XXWC_AR_INV_INT_PKG_TO_XXCUS_EXECUTE_ROLE.sql;
@./Grants/XXWCAR_CUSTOMER_INTERFACE_PRC_TO_XXWC_EDIT_IFACE_ROLE.sql;
@./Grants/XXWCAR_INVOICE_INTERFACE_PRC_TO_XXWC_EDIT_IFACE_ROLE.sql;
@./Grants/XXWC_AR_CUSTOMER_BALANCES_PKG_TO_XXWC_PRISM_EXECUTE_ROLE.sql;
