--//============================================================================
--//
--// Object Name         :: APEXWC_LNK
--//
--// Object Type         :: Grant script.
--//
--// Object Description  :: DB Link to access the user data from apxcmmn
--//                        
--//
--// Version Control
--//============================================================================
--// Vers    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Pahwa Nancy     09/05/2015    Initial Build - TMS#20150901-00129 
--//============================================================================
create database link APEXWC_LNK
  connect to INTERFACE_APEXWC identified by "Welcome100" 
  using '(DESCRIPTION =    (ADDRESS = (PROTOCOL = TCP)(HOST = xpeprd.hdsupply.net)(PORT = 1543))    (CONNECT_DATA =      (SERVER = DEDICATED)      (SID=xpeprd)    )  )';
