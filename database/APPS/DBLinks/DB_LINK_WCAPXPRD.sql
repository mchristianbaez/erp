--//============================================================================
--//
--// Object Name         :: WCAPXPRD.HSI.HUGHESSUPPLY.COM
--//
--// Object Type         :: Grant script.
--//
--// Object Description  :: DB Link to access the user data in WCAPX...
--//                        
--//
--// Version Control
--//============================================================================
--// Vers    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Baez,Christian    02/02/2017   Initial Build - TMS#20160915-00144 
--//============================================================================
DROP DATABASE LINK WCAPXPRD.HSI.HUGHESSUPPLY.COM;

CREATE DATABASE LINK WCAPXPRD.HSI.HUGHESSUPPLY.COM
      connect to WC_APPS identified by "apps4wcprd"
      using '
				(DESCRIPTION =
	    		(ADDRESS = (PROTOCOL = TCP)(HOST = wcapxprd.hdsupply.net)(PORT = 1521))
	    		(CONNECT_DATA =
	      		(SERVER = DEDICATED)
	      		(SERVICE_NAME = wcapxprd)
	    	))';