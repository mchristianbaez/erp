/*************************************************************************
     $Header XXWC_SMARTSHEET_EXTRACT_PKG_GRANT  $
     Module Name: XXWC_SMARTSHEET_EXTRACT_PKG_GRANT.sql

     PURPOSE:   Grant to INTERFACE_OSO
                       
     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.0        24/07/2018  Niraj K Ranjan          TMS#20180725-00025   SmartSheets Extract               
**************************************************************************/
grant execute on apps.XXWC_SMARTSHEET_EXTRACT_PKG to INTERFACE_OSO;