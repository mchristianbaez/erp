/*
   Ticket#                         Date          Author             Notes
   ------------------------------  ------------  -----------------  ---------------------------------
   TMS 20181001-00001              09/27/2018    Ashwin Sridhar     GL Non Cash Debt Report
*/
--
GRANT SELECT,INSERT,UPDATE,DELETE ON XXCUS.XXCUS_GL_NON_CASH_DEBT_TBL TO BS006141;

GRANT SELECT,INSERT,UPDATE,DELETE ON XXCUS.XXCUS_GL_NON_CASH_DEBT_TBL TO KP059700;

GRANT SELECT,INSERT,UPDATE,DELETE ON XXCUS.XXCUS_GL_NON_CASH_DEBT_TBL TO AS066570;

--