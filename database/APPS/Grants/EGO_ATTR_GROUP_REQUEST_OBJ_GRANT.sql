/*
-- ********************************************************************************************
-- HISTORY
-- ============================================================================================
-- ============================================================================================
-- VERSION DATE          AUTHOR(S)       DESCRIPTION
-- ------- -----------   --------------- ------------------------------------------------------
-- 1.0     16-Oct-2015   P.Vamshidhar    TMS#20141006-00021 
--                                       Re-created table due to patch
*/

grant all on APPS.EGO_ATTR_GROUP_REQUEST_OBJ to DLS;
/
