--//============================================================================
--//
--// Object Name         :: xxwc_ebs_agilone_intf_pkg_grant
--//
--// Object Type         :: Grant
--//
--// Object Description  :: This is used to Grant the package to execute using interface_xxcus,interface_prism
--//
--// Version Control
--//============================================================================
--// Vers    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Mahesh Kudle     26/05/2014    Initial Build - TMS#20140602-00295
--//============================================================================
GRANT EXECUTE ON apps.xxwc_ebs_agilone_intf_pkg to interface_xxcus
/
GRANT EXECUTE ON apps.xxwc_ebs_agilone_intf_pkg to interface_prism
/