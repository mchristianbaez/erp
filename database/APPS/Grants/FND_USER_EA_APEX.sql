/*************************************************************************
 Copyright (c) 2012 HD Supply
 All rights reserved.
**************************************************************************
   $Header apps.fnd_user $
  Module Name: apps.fnd_user

  PURPOSE:Grant on apps.fnd_user to EA_APEX

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)   TMS TASK        DESCRIPTION
  -- ------- -----------   ------------ ---------      -------------------------
  -- 1.0     15-apr-2017   Pahwa, Nancy                Initially Created 
TMS# 20170302-00126
**************************************************************************/
GRANT select ON apps.fnd_user TO EA_APEX;