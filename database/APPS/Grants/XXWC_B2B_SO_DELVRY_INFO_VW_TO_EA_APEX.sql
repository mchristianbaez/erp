/********************************************************************************
   $Header XXWC_B2B_SO_DELVRY_INFO_VW_TO_EA_APEX.sql $
   Module Name: XXWC_B2B_SO_DELVRY_INFO_VW

   PURPOSE:  Grant to Apex on view - XXWC_B2B_SO_DELVRY_INFO_VW.

   REVISIONS:
   Ver        Date        Author                     Description
   ---------  ----------  ---------------         -------------------------
   1.0        08/09/2015  Gopi Damuluri           TMS# 20150615-00088
                                                  Initial Version
********************************************************************************/

GRANT SELECT ON APPS.XXWC_B2B_SO_DELVRY_INFO_VW TO EA_APEX WITH GRANT OPTION;

/