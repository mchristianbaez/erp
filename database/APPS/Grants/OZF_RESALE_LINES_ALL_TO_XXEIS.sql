-----------------------------------------------------------------------------------------------------------------------------
/*********************************************************************************************
-- HISTORY
-- ============================================================================================
-- ============================================================================================
-- VERSION DATE          AUTHOR(S)       DESCRIPTION
-- ------- -----------   --------------- ------------------------------------------------------
-- 1.0     12-Apr-2016   Pramod	  		 TMS#20160412-00031 --grant to XXEIS                                     
***********************************************************************************************/
GRANT SELECT ON APPS.OZF_RESALE_LINES_ALL TO XXEIS
/
