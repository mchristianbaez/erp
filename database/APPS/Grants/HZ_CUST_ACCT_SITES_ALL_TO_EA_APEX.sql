/********************************************************************************
   $Header HZ_CUST_ACCT_SITES_ALL_TO_EA_APEX.sql $
   Module Name: HZ_CUST_ACCT_SITES_ALL

   PURPOSE:  Grant to Apex on table - HZ_CUST_ACCT_SITES_ALL.

   REVISIONS:
   Ver        Date        Author                     Description
   ---------  ----------  ---------------         -------------------------
   1.0        10/20/2015  Gopi Damuluri           TMS# 20151019-00065
                                                  Initial Version
********************************************************************************/

GRANT SELECT ON APPS.HZ_CUST_ACCT_SITES_ALL TO EA_APEX WITH GRANT OPTION;