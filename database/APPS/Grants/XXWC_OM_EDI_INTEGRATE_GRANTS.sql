/*
-- ********************************************************************************************
-- HISTORY
-- ============================================================================================
-- ============================================================================================
-- VERSION DATE          AUTHOR(S)       DESCRIPTION
-- ------- -----------   --------------- ------------------------------------------------------
-- 1.0     13-Jul-2018   Pattabhi Avula  TMS#20180611-00012 
--                                       Customer Integration - Default Ship From ORG ID
*/
GRANT ALL ON XXWC.XXWC_EDI_IB_FILES_DTLS_TBL TO INTERFACE_XXCUS
/
GRANT ALL ON XXWC.XXWC_EDI_IB_FILES_EXTRNL_TBL TO INTERFACE_XXCUS
/
GRANT ALL ON APPS.XXWC_OM_EDI_INTEGRATE_PKG TO INTERFACE_XXCUS
/