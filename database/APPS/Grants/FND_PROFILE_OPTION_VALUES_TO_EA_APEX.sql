/********************************************************************************
   $Header FND_PROFILE_OPTION_VALUES_TO_EA_APEX.sql $
   Module Name: FND_PROFILE_OPTION_VALUES

   PURPOSE:  Grant to Apex on table - FND_PROFILE_OPTION_VALUES.

   REVISIONS:
   Ver        Date        Author                     Description
   ---------  ----------  ---------------         -------------------------
   1.0        05/02/2016  Gopi Damuluri           TMS# 20151110-00285
                                                  Initial Version
********************************************************************************/

GRANT SELECT ON APPS.FND_PROFILE_OPTION_VALUES TO EA_APEX WITH GRANT OPTION;