/*************************************************************************
  PURPOSE:   Grants provided to all custom objects to  EA_APEX user

  REVISIONS:
  Ver        Date        Author               Description
  ---------  ----------  ---------------      -------------------------
  1.0        12/11/2017  Pattabhi Avula       TMS#20171011-00108 - Picking QC
                                              project - EBS Initial Version
**************************************************************************/

GRANT SELECT, INSERT, UPDATE, DELETE ON APPS.XXWC_OM_PQC_PICKING_TBL TO EA_APEX
/
GRANT SELECT ON APPS.fnd_profile_options  TO EA_APEX
/
GRANT SELECT ON APPS.fnd_profile_option_values  TO EA_APEX
/
GRANT SELECT ON APPS.fnd_user  TO EA_APEX
/
GRANT SELECT ON APPLSYS.FND_LOOKUP_VALUES TO EA_APEX
/
GRANT SELECT ON apps.oe_order_headers_all TO EA_APEX
/
GRANT SELECT ON apps.oe_order_lines_all TO EA_APEX
/
GRANT SELECT ON xxwc.xxwc_wsh_shipping_stg TO EA_APEX
/
GRANT ALL ON APPS.XXWC_PICK_QC_IMPR_MNTS_PKG TO EA_APEX
/
GRANT SELECT, INSERT, UPDATE, DELETE ON APPS.XXWC_OM_PQC_LOAD_CHECK_TBL TO EA_APEX
/
GRANT SELECT ON XXCUS.XXCUSHR_PS_EMP_ALL_TBL TO EA_APEX
/
GRANT SELECT ON APPS.MTL_PARAMETERS TO EA_APEX
/
GRANT SELECT ON apps.fnd_responsibility_tl  TO EA_APEX
/
GRANT SELECT ON apps.fnd_application_tl  TO EA_APEX
/
GRANT SELECT ON apps.fnd_application  TO EA_APEX
/
GRANT SELECT ON apps.wf_local_roles  TO EA_APEX
/
GRANT SELECT ON apps.wf_user_role_assignments  TO EA_APEX
/
GRANT EXECUTE ON apps.xxwc_ont_routines_pkg  TO EA_APEX
/