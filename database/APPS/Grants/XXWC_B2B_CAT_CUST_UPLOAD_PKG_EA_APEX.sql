/*************************************************************************
Copyright (c) 2012 HD Supply
All rights reserved.
**************************************************************************
   $Header XXWC_B2B_CAT_CUST_UPLOAD_PKG$
  Module Name: XXWC_B2B_CAT_CUST_UPLOAD_PKG
  PURPOSE:

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)   TMS TASK        DESCRIPTION
  -- ------- -----------   ------------ ---------      -------------------------
  -- 1.0     14-FEB-2016   Pahwa, Nancy                Initially Created TMS# 20160223-00029 
**************************************************************************/
GRANT EXECUTE ON "APPS"."XXWC_B2B_CAT_CUST_UPLOAD_PKG" TO "EA_APEX";