/********************************************************************************
   $Header XXWC_B2B_INV_INTF_PKG_TO_EA_APEX.sql $
   Module Name: XXWC_B2B_INV_INTF_PKG

   PURPOSE:  Grant to Apex on Package - XXWC_B2B_INV_INTF_PKG.

   REVISIONS:
   Ver        Date        Author                     Description
   ---------  ----------  ---------------         -------------------------
   1.0        08/09/2015  Gopi Damuluri           TMS# 20160516-00148
                                                  Initial Version
********************************************************************************/

GRANT ALL ON APPS.XXWC_B2B_INV_INTF_PKG TO EA_APEX;

/