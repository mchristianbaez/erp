/********************************************************************************
   $Header MTL_PARAMETERS_TO_EA_APEX.sql $
   Module Name: MTL_PARAMETERS

   PURPOSE:  Grant to Apex on table - MTL_PARAMETERS.

   REVISIONS:
   Ver        Date        Author                     Description
   ---------  ----------  ---------------         -------------------------
   1.0        05/02/2016  Gopi Damuluri           TMS# 20151110-00285
                                                  Initial Version
********************************************************************************/

GRANT SELECT ON APPS.MTL_PARAMETERS TO EA_APEX WITH GRANT OPTION;