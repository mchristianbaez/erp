/*
TMS 20150710-00251
DATE: 04/19/2016
SCOPE: The grant is necessary for dbuser interface_apexhdsoracle to access the oracle apps owned view
               for the purpose of rebates cash application process.
*/
grant select on APPS.XXCUS_RBT_PMT_DET_V to interface_apexhdsoracle;