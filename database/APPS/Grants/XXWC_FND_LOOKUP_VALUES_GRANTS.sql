  /********************************************************************************
  FILE NAME: XXWC_FND_LOOKUP_VALUES_GRANTS.sql
  
  PROGRAM TYPE: SELECT access on FND_LOOKUP_VALUES table for CHECKIT apps
  
  PURPOSE: SELECT access on FND_LOOKUP_VALUES table for CHECKIT apps
  
  HISTORY
  ==========================================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- ----------------------------------------------------
  1.0     10/19/2017    Rakesh P.       TMS#20171019-00141-Read access to INTERFACE_OSO user on fnd_lookup_values table for CHECKIT app
  *******************************************************************************************/
GRANT SELECT on fnd_lookup_values to INTERFACE_OSO;
  