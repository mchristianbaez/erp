/********************************************************************************
   $Header HZ_CUST_ACCOUNTS_TO_INTERFACE_APEXWC.sql $
   Module Name: HZ_CUST_ACCOUNTS

   PURPOSE:  Grant to Apex on table - HZ_CUST_ACCOUNTS.

   REVISIONS:
   Ver        Date        Author                     Description
   ---------  ----------  ---------------         -------------------------
   1.0        02/013/2017  Pattabhi Avula         TMS# 20160915-00144
                                                  Initial Version
********************************************************************************/
-- GRANT SELECT ON AR.HZ_CUST_ACCOUNTS TO WC_APPS WITH GRANT OPTION;  

GRANT SELECT ON AR.HZ_CUST_ACCOUNTS TO INTERFACE_APEXWC WITH GRANT OPTION;
/