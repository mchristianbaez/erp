/*
   Ticket#                               Date         Author             Notes
   -------------------------------------------------------------------------------------------------------------------------------------------------------
   TMS 20180905-00001                    09/15/2018   Ashwin Sridhar     AHH Concur process related tables
*/
--
GRANT SELECT,INSERT,UPDATE,DELETE ON XXCUS.XXCUS_AHH_CONTRACTOR_TO_CONCUR TO BS006141;

GRANT SELECT,INSERT,UPDATE,DELETE ON XXCUS.XXCUS_AHH_CONTRACTOR_TO_CONCUR TO ID020048;

GRANT SELECT,INSERT,UPDATE,DELETE ON XXCUS.XXCUS_AHH_CONTRACTOR_TO_CONCUR TO KP059700;

GRANT SELECT,INSERT,UPDATE,DELETE ON XXCUS.XXCUS_AHH_CONTRACTOR_TO_CONCUR TO AS066570;

GRANT SELECT,INSERT,UPDATE,DELETE ON XXCUS.XXCUS_AHH_CONTRACTOR_TO_CONCUR TO VR073604;

--