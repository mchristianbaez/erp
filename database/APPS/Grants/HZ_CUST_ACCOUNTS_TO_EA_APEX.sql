/********************************************************************************
   $Header HZ_CUST_ACCOUNTS_TO_EA_APEX.sql $
   Module Name: HZ_CUST_ACCOUNTS

   PURPOSE:  Grant to Apex on table - HZ_CUST_ACCOUNTS.

   REVISIONS:
   Ver        Date        Author                     Description
   ---------  ----------  ---------------         -------------------------
   1.0        08/09/2015  Gopi Damuluri           TMS# 20150615-00088
                                                  Initial Version
********************************************************************************/

GRANT SELECT ON AR.HZ_CUST_ACCOUNTS TO EA_APEX WITH GRANT OPTION;

/