/********************************************************************************
   $Header MTL_SYSTEM_ITEMS_B_TO_EA_APEX$
   Module Name: MTL_SYSTEM_ITEMS_B

   PURPOSE:  Grant to Apex on table - MTL_SYSTEM_ITEMS_B.

   REVISIONS:
   Ver        Date        Author                     Description
   ---------  ----------  ---------------         -------------------------
   1.0        05/19/2016  Christian Baez           TMS# 20160223-00029
                                                  Initial Version
********************************************************************************/
GRANT SELECT ON APPS.MTL_SYSTEM_ITEMS_B TO EA_APEX WITH GRANT OPTION;
