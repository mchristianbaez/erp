/*************************************************************************
 Copyright (c) 2012 HD Supply
 All rights reserved.
**************************************************************************
   $Header APPS.XXWC_AIS_LOG_MV $
  Module Name: APPS.XXWC_AIS_LOG_MV

  PURPOSE:Grant on "XXWC"."XXWC_AIS_LOOKUP_TBL to EA_APEX

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)   TMS TASK        DESCRIPTION
  -- ------- -----------   ------------ ---------      -------------------------
  -- 1.0     15-July-2017   Pahwa, Nancy                Initially Created 
TMS# 20170613-00289
**************************************************************************/
GRANT select on APPS.XXWC_AIS_LOG_MV to ea_apex;