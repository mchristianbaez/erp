/********************************************************************************
   $Header XXWC_B2B_SO_IB_PKG_TO_EA_APEX.sql $
   Module Name: XXWC_B2B_SO_IB_PKG

   PURPOSE:  Grant to Apex on Package - XXWC_B2B_SO_IB_PKG.

   REVISIONS:
   Ver        Date        Author                     Description
   ---------  ----------  ---------------         -------------------------
   1.0        08/09/2015  Gopi Damuluri           TMS# 20150615-00088
                                                  Initial Version
********************************************************************************/

GRANT ALL ON APPS.XXWC_B2B_SO_IB_PKG TO EA_APEX;

/