/*************************************************************************
Copyright (c) 2012 HD Supply
All rights reserved.
**************************************************************************
   $Header "APPLSYS"."FND_CONCURRENT_REQUESTS"  $
  Module Name: grants on "APPLSYS"."FND_CONCURRENT_REQUESTS"  
  PURPOSE:

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)   TMS TASK        DESCRIPTION
  -- ------- -----------   ------------ ---------      -------------------------
  -- 1.0     26-APR-2018  Nancy Pahwa  20160218-00198  Initially Created
**************************************************************************/
GRANT SELECT ON "APPLSYS"."FND_CONCURRENT_REQUESTS" TO "EA_APEX";