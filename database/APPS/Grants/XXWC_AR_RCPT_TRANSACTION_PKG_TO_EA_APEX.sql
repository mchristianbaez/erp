/*************************************************************************
Copyright (c) 2012 HD Supply
All rights reserved.
**************************************************************************
   $Header "APPS"."XXWC_AR_RCPT_TRANSACTION_PKG" $
  Module Name: grants on "APPS"."XXWC_AR_RCPT_TRANSACTION_PKG" 
  PURPOSE:

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)   TMS TASK        DESCRIPTION
  -- ------- -----------   ------------ ---------      -------------------------
  -- 1.0     26-APR-2018  Nancy Pahwa  20160218-00198  Initially Created
**************************************************************************/
GRANT DEBUG ON "APPS"."XXWC_AR_RCPT_TRANSACTION_PKG" TO "EA_APEX";
GRANT EXECUTE ON "APPS"."XXWC_AR_RCPT_TRANSACTION_PKG" TO "EA_APEX";