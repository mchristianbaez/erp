--//============================================================================
--//
--// Object Name         :: APPS.XXCUSAPEX_P2P_INV_VW
--//
--// Object Type         :: Grant script.
--//
--// Object Description  :: Script is used to Grant Permissons on
--//                        APPS.XXCUSAPEX_P2P_INV_VW to interface_p2p
--//
--// Version Control
--//============================================================================
--// Vers    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Pahwa Nancy     04/07/2015    Initial Build - RFC # 42130 
--//============================================================================
grant select on APPS.XXCUSAPEX_P2P_INV_VW to interface_p2p;
