CREATE OR REPLACE TRIGGER apps.xxwc_oe_order_holds_ari_t1
   AFTER INSERT
   ON ONT.OE_ORDER_HOLDS_ALL
   REFERENCING NEW AS New OLD AS Old
   FOR EACH ROW
DECLARE
/******************************************************************************
   NAME:       XXWC_oe_order_holds_ari_t1
   PURPOSE:    Trigger to create a case folder whenever a hard hold is placed.

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        6/26/2013      shariharan       1. Created this trigger.
******************************************************************************/
BEGIN
   INSERT INTO xxwc_order_credit_holds_tbl (order_hold_id
                                          , hold_source_id
                                          , hold_entity_code
                                          , header_id
                                          , line_id
                                          , process_flag
                                          , creation_date
                                          , created_by
                                          , last_update_date
                                          , last_updated_by
                                          , last_update_login)
        VALUES (:new.order_hold_id
              , :new.hold_source_id
              , null
              , :new.header_id
              , :new.line_id
              , 'N'
              , SYSDATE
              , fnd_global.user_id
              , SYSDATE
              , fnd_global.user_id
              , fnd_global.login_id);
EXCEPTION
   WHEN OTHERS
   THEN
      NULL;
END;