create or replace trigger APPS.XXWC_SR_SALESREP_HIERARCHY_TRG
   before insert or update on XXWC.XXWC_SR_SALESREP_HIERARCHY
   for each row
declare
l_mgt_desc varchar2(80);
begin
      if inserting then
        if :NEW.SR_HIER_ID IS NULL THEN
          select APPS.XXWC_SR_SALESREP_HIERARCHY_S.nextval INTO :NEW.SR_HIER_ID
               from dual;
         end if;
            :NEW.last_update_date := SYSDATE;
      /*   if :NEW.last_update_user is null  then
           :NEW.last_update_user := nvl(v('APP_USER'),USER);
         end if;*/
--update XXWC_SR_SALESREPS set auto_update = 'N' where salesrep_id = :new.Sr_Id and auto_update != 'N';
if  :new.mgt_desc is null then 
 select MGT_DESC into l_mgt_desc from XXWC.XXWC_BR_MGMT_LEVELS where mgt_lvl = :new.mgt_lvl;
 :new.mgt_desc := l_mgt_desc;
end if;
      end if;
      if updating then
        :NEW.last_update_date := SYSDATE;
       -- :NEW.last_update_user := nvl(v('APP_USER'),USER);
  if  :new.mgt_lvl != :old.mgt_lvl then 
 select MGT_DESC into l_mgt_desc from XXWC.XXWC_BR_MGMT_LEVELS where mgt_lvl = :new.mgt_lvl;
 :new.mgt_desc := l_mgt_desc;
end if;
      end if;
    
EXCEPTION
  WHEN OTHERS THEN
    raise_application_error(-20001, 'XXWC_SR_SALESREP_HIERARCHY_TRG: ' || SQLERRM);


   end;


