CREATE OR REPLACE TRIGGER APPS.XXWCINV_MTL_ONHND_QTYS_DTL_TRG
   BEFORE DELETE
   ON MTL_ONHAND_QUANTITIES_DETAIL
   FOR EACH ROW
   WHEN (OLD.subinventory_code = 'Staging')
BEGIN
   -- insert records into staging table for shipments
   INSERT INTO XXWC.XXWCINV_INTERCO_TRANSFERS (transaction_id
                                              ,inventory_item_id
                                              ,organization_id
                                              ,subinventory_code
                                              ,locator_id
                                              ,revision
                                              ,orig_date_received
                                              ,quantity
                                              ,creation_date)
        VALUES (NULL                                         -- transaction_id
               ,:OLD.inventory_item_id                    -- inventory_item_id
               ,:OLD.organization_id                        -- organization_id
               ,:OLD.subinventory_code                    -- subinventory_code
               ,:OLD.locator_id                                 -- location_id
               ,:OLD.revision                                      -- revision
               ,:OLD.orig_date_received                  -- orig_date_received
               ,:OLD.primary_transaction_quantity                  -- quantity
               ,SYSDATE                                       -- creation_date
                       );
EXCEPTION
   WHEN OTHERS
   THEN
      NULL;
END;
/
