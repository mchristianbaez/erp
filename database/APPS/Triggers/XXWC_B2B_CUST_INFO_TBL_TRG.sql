CREATE OR REPLACE TRIGGER APPS.XXWC_B2B_CUST_INFO_TBL_TRG
   BEFORE INSERT
   ON XXWC.XXWC_B2B_CUST_INFO_TBL
   REFERENCING NEW AS New OLD AS Old
   FOR EACH ROW
DECLARE
   l_party_id   NUMBER DEFAULT NULL;
/*******************************************************************************
  Table:   XXWC_B2B_CUST_INFO_TBL_TRG
  Description: This trigger is used to update XXWC_B2B_CUST_INFO_TBL
  HISTORY
  ===============================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- ------------------------------------
  1.0     11-May-2014        Gopi Damuluri   Initial version
********************************************************************************/
BEGIN
   SELECT party_id
     INTO l_party_id
     FROM apps.hz_parties
    WHERE party_number = NVL(:OLD.PARTY_NUMBER, :NEW.PARTY_NUMBER)
       AND ROWNUM = 1;

   :NEW.PARTY_ID         := l_party_id;
   :NEW.CREATION_DATE    := SYSDATE;
   :NEW.CREATED_BY       := -1;
   :NEW.LAST_UPDATE_DATE := SYSDATE;
   :NEW.LAST_UPDATED_BY  := -1;
EXCEPTION
  WHEN NO_DATA_FOUND THEN
   NULL;
   WHEN OTHERS THEN
      -- Consider logging the error and then re-raise
      RAISE;
END XXWC_B2B_CUST_INFO_TBL_TRG;
/
