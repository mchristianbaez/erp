create or replace trigger APPS.XXWC_BR_MGMT_LEVELS_TRG
   before insert or update on XXWC.XXWC_BR_MGMT_LEVELS
   for each row

begin
      if inserting then
        if :NEW.MGT_ID IS NULL THEN
          select APPS.XXWC_BR_MGMT_LEVELS_S.nextval next_val INTO :NEW.MGT_ID
               from dual;
         end if;
            :NEW.last_update_date := SYSDATE;
      /*   if :NEW.last_update_user is null  then
           :NEW.last_update_user := nvl(v('APP_USER'),USER);
         end if;*/
end if;
    --  end if;
      if updating then
        :NEW.last_update_date := SYSDATE;
       end if;

EXCEPTION
  WHEN OTHERS THEN
    raise_application_error(-20001, 'XXWC_BR_MGMT_LEVELS_TRG: ' || SQLERRM);


   end;