CREATE OR REPLACE TRIGGER XXWC_OE_ORDER_LINES_BRI_T2
BEFORE INSERT ON OE_ORDER_LINES_ALL
FOR EACH ROW
DECLARE
   /*************************************************************************
     $Header XXWC_OE_ORDER_LINES_BRI_T1 $
     Module Name: XXWC_OE_ORDER_LINES_BRI_T1.sql

     PURPOSE:   This trigger resets the Out For Delivery DFF Attriibute16 

     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.1        01/23/2013  Satish Upadhyayula       Added code to Update Out For Delivery DFF Attribute value when Line is Created. 
     1.2        13/FEB/2013 Satish U               20130213-01750 When A New Line is created if it is not from Split Line ID then Attribute16 Should be NULL 
     1.3        16/OCT/2014 Maharajan Shunmugam    TMS# 20141001-00161 Canada Multi org changes
     
   **************************************************************************/

  
  l_Released_Status   Varchar2(1); 
  l_SOL_Force_Ship  Number ;  
  l_Source_Header_Type_Name  Varchar2(60) ; 
  
  
BEGIN


   l_SOL_Force_Ship  := TO_NUMBER(:NEW.ATTRIBUTE11);  
    -- Satihs U: 23-JAN-2013 : Added Following Code  to Update Out_For_Delivery when Sales Order Line Gets Split 
  If :New.SPLIT_FROM_LINE_ID IS NOT NULL Then 
     -- Check Released Status of Corresponding Delivery Lien 
     Begin 
          Select released_Status , Source_Header_Type_Name
          Into l_Released_Status , l_Source_Header_Type_Name
          From Wsh_Delivery_Details
          Where  Source_Line_ID = :New.LINE_ID ; 
          
       
     Exception 
        When Others Then 
             l_Released_Status := NULL; 
             :NEW.ATTRIBUTE16 := 'N'; 
           
          
     End ; 
     If l_Released_Status = 'B' Then 
        :New.ATTRIBUTE16 := 'N'; 
     Elsif l_Released_Status = 'C' Then 
              :New.ATTRIBUTE16 := 'D' ;
     ElsIF (l_SOL_Force_Ship IS NULL OR l_SOl_Force_Ship = 0 ) THEN 
                              --FND_Message.Debug('Satish : First If Statemetn'); 
  --      If l_Source_Header_Type_Name  in ('WC LONG TERM RENTAL','WC SHORT TERM RENTAL') THEN   -commented and added below for ver 1.3
          If l_Source_Header_Type_Name  in (fnd_profile.VALUE ('XXWC_WC_LONG_TERM_RENTAL_TYPE'),fnd_profile.VALUE ('XXWC_WC_SHORT_TERM_RENTAL_TYPE')) THEN
          :New.ATTRIBUTE16 := 'Y';   
        Else
           :New.ATTRIBUTE16 := 'Y';  
        End If;                    
     ELSIF l_SOL_Force_Ship > 0 THEN 
         :NEW.ATTRIBUTE16 := 'Y'; 
                      
     END IF;
  ELSE 
     -- Satish U  13-FEB-2013    TMS Ticket#   20130213-01750     
     :NEW.ATTRIBUTE16 := NULL; 
       
  End If ; 
Exception 
   When Others Then 
      :New.ATTRIBUTE16 := NULL; 
END;
/

