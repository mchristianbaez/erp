CREATE OR REPLACE TRIGGER APPS.XXWC_wsh_delivery_detail_ARUT1
AFTER UPDATE ON WSH_DELIVERY_DETAILS
referencing new as NEW old as OLD
FOR EACH ROW
DECLARE
   /*************************************************************************
     $Header XXWC_wsh_delivery_detail_ARUT1 $
     Module Name: XXWC_wsh_delivery_detail_ARUT1.sql

     PURPOSE:   This trigger inserts a line into a custom table if the whole
               order line in BO. Data is read from custom table by a concurrent
              program to update the subinventory back to General

     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.0        07/29/2012  Shankar Hariharan        Initial Version
   **************************************************************************/
     l_header_rec              Oe_Order_Pub.Header_Rec_Type;
   o_header_rec              Oe_order_pub.Header_Rec_Type ;
   o_header_val_rec          Oe_Order_Pub.Header_Val_Rec_Type;
   o_header_adj_tbl          Oe_Order_Pub.Header_Adj_Tbl_Type;
   o_header_adj_val_tbl      Oe_Order_Pub.Header_Adj_Val_Tbl_Type;
   o_header_price_att_tbl    Oe_Order_Pub.Header_Price_Att_Tbl_Type;
   o_header_adj_att_tbl      Oe_Order_Pub.Header_Adj_Att_Tbl_Type;
   o_header_adj_assoc_tbl    Oe_Order_Pub.Header_Adj_Assoc_Tbl_Type;
   o_header_scredit_tbl      Oe_Order_Pub.Header_Scredit_Tbl_Type;
   o_header_scredit_val_tbl  Oe_Order_Pub.Header_Scredit_Val_Tbl_Type;
   l_line_tbl                Oe_Order_Pub.Line_Tbl_Type;
   o_line_tbl                Oe_Order_Pub.Line_Tbl_Type;
   o_line_val_tbl            Oe_Order_Pub.Line_Val_Tbl_Type;
   o_line_adj_tbl            Oe_Order_Pub.Line_Adj_Tbl_Type;
   o_line_adj_val_tbl        Oe_Order_Pub.Line_Adj_Val_Tbl_Type;
   o_line_price_att_tbl      Oe_Order_Pub.Line_Price_Att_Tbl_Type;
   o_line_adj_att_tbl        Oe_Order_Pub.Line_Adj_Att_Tbl_Type;
   o_line_adj_assoc_tbl      Oe_Order_Pub.Line_Adj_Assoc_Tbl_Type;
   o_line_scredit_tbl        Oe_Order_Pub.Line_Scredit_Tbl_Type;
   o_line_scredit_val_tbl    Oe_Order_Pub.Line_Scredit_Val_Tbl_Type;
   o_lot_serial_tbl          Oe_Order_Pub.Lot_Serial_Tbl_Type;
   o_lot_serial_val_tbl      Oe_Order_Pub.Lot_Serial_Val_Tbl_Type;
   l_action_request_tbl      Oe_Order_Pub.Request_Tbl_Type;
   o_action_request_tbl      oe_order_pub.Request_Tbl_Type ;
   l_return_status              VARCHAR2 (240);
   l_msg_count                  NUMBER;
   l_msg_data                   VARCHAR2 (4000);
   l_exist                      VARCHAR2(1);
BEGIN
  IF :new.released_status='B' and :new.subinventory='GeneralPCK' and :new.source_code='OE' 
  then
    begin
     select 'x' 
       into l_exist
       from xxwc_om_upd_del_subinv 
      where header_id=:new.source_header_id 
        and line_id=:new.source_line_id
        and rownum=1; 
     exception
      when others then
        insert into xxwc_om_upd_del_subinv
     values (:new.source_header_id,:new.source_line_id, :new.subinventory);   
     end;    
     
 END IF;

END;
/
