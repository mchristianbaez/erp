CREATE OR REPLACE TRIGGER APPS.XXWC_RCV_LOT_TRANSACTIONS_AI
   /******************************************************************************
         NAME:       XXWC_RCV_LOT_TRANSACTIONS_AI

         PURPOSE:    To Print Lot Labels After Receiving Transaction

         Logic:     1) Print a lot label after has been inserted into RCV_LOT_TRANSACTIONS

         REVISIONS:
         Ver        Date        Author           Description
         ---------  ----------  ---------------  ------------------------------------
         1.0        23-APR12   Lee Spitzer       1. Created this trigger.
         2.0        06-JUN-12  Lee Spitzer       2. Update Request to pass :NEW.ITEM_ID
         2.1        14-JUN-12  Lee Spitzer       3. Update Copies Logic
      ******************************************************************************/
   AFTER INSERT
   ON PO.RCV_LOT_TRANSACTIONS
   REFERENCING NEW AS NEW OLD AS OLD
   FOR EACH ROW
   WHEN (NEW.CORRECTION_TRANSACTION_ID = -1)
DECLARE
   X_REQUEST_ID           NUMBER;
   l_XML_Layout           BOOLEAN;
   l_applicable           VARCHAR2 (1);
   l_user_id              NUMBER;
   l_set_mode             BOOLEAN;
   l_label_printer        VARCHAR2 (30);
   l_organization_id      NUMBER;
   l_shipment_header_id   NUMBER;
   l_set_print_options    BOOLEAN;
   l_uom_code             VARCHAR2 (3);
   l_copies               NUMBER;

   --Added for Exception Handling--
   g_user_id              NUMBER := fnd_global.user_id;
   g_login_id             NUMBER := fnd_profile.VALUE ('LOGIN_ID');
   g_err_callfrom         VARCHAR2 (75)
                             DEFAULT 'XXWC_RA_INTERFACE_LINES_ALL_BI';
   g_err_callpoint        VARCHAR2 (75) DEFAULT 'START';
   g_distro_list          VARCHAR2 (75)
                             DEFAULT 'HDSOracleDevelopers@hdsupply.com';
   l_err_msg              VARCHAR2 (2000);
   l_message              VARCHAR2 (2000);
BEGIN
   --Check for Label Printer

   l_label_printer := fnd_profile.VALUE ('XXWC_LABEL_PRINTER');


   --if a label priner profile is not defined then don't execute the rest of the trigger
   IF l_label_printer IS NOT NULL
   THEN
      BEGIN
         SELECT rt.organization_id, rt.shipment_header_id, rt.uom_code
           INTO l_organization_id, l_shipment_header_id, l_uom_code
           FROM rcv_transactions rt, po_lines_all pla
          WHERE     rt.transaction_id = :NEW.transaction_id
                AND rt.po_line_id = pla.po_line_id
                AND rt.transaction_type = 'DELIVER';
      EXCEPTION
         WHEN OTHERS
         THEN
            l_organization_id := NULL;
            l_shipment_header_id := NULL;
      END;



      IF l_organization_id IS NOT NULL AND l_shipment_header_id IS NOT NULL
      THEN
         --Removed 6/14/2012 -- this logic is now handled in the report and only need to pass 1 copy
         /* IF l_uom_code IN ('FT','GAL') THEN


             l_copies := least(4,:NEW.PRIMARY_QUANTITY);

          ELSE

             l_copies := :NEW.PRIMARY_QUANTITY;

          END IF;

           */
         l_xml_layout :=
            FND_REQUEST.ADD_LAYOUT ('XXWC'
                                   ,'XXWC_INV_LOTLABELS'
                                   ,'en'
                                   ,'US'
                                   ,'PDF');


         l_set_mode := FND_Request.Set_Mode (TRUE);

         l_set_print_options :=
            FND_Request.set_print_options (printer            => l_label_printer
                                          ,style              => NULL
                                          ,copies             => 1 --:NEW.PRIMARY_QUANTITY
                                          ,save_output        => TRUE
                                          ,print_together     => 'N'
                                          ,validate_printer   => 'RESOLVE');



         /* --Old Version to without item number in concurrent request parameter
             X_REQUEST_ID :=
                       fnd_request.submit_request(APPLICATION => 'XXWC'
                                                 ,PROGRAM =>     'XXWC_INV_LOTLABELS'
                                                 ,DESCRIPTION => 'XXWC Inventory Lot Labels Report'
                                                 ,SUB_REQUEST => FALSE
                                                 ,ARGUMENT1 => l_organization_id
                                                 ,ARGUMENT2 => l_shipment_header_id
                                                 ,ARGUMENT3 => :NEW.lot_num
                                                 ,ARGUMENT4 => NULL --:NEW.expiration_date
                                                 ,ARGUMENT5 => 'F'); --F = 4x1 S = 6X4 Label

         */

         X_REQUEST_ID :=
            fnd_request.submit_request (
               APPLICATION   => 'XXWC'
              ,PROGRAM       => 'XXWC_INV_LOTLABELS'
              ,DESCRIPTION   => 'XXWC Inventory Lot Labels Report'
              ,SUB_REQUEST   => FALSE
              ,ARGUMENT1     => l_organization_id
              ,ARGUMENT2     => l_shipment_header_id
              ,ARGUMENT3     => :NEW.item_id
              ,ARGUMENT4     => :NEW.lot_num
              ,ARGUMENT5     => NULL                    --:NEW.expiration_date
              ,ARGUMENT6     => 'F');                  --F = 4x1 S = 6X4 Label
      END IF;
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      l_err_msg := SQLCODE || SQLERRM;
      l_message := 'Error printing XXWC Inventory Lot Labels';

      xxcus_error_pkg.xxcus_error_main_api (
         p_called_from         => g_err_callfrom
        ,p_calling             => g_err_callpoint
        ,p_ora_error_msg       => l_err_msg
        ,p_error_desc          => l_message
        ,p_distribution_list   => g_distro_list
        ,p_module              => 'INV');
END;
/
