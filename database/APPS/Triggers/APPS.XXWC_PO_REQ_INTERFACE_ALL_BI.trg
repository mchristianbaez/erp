CREATE OR REPLACE TRIGGER APPS.XXWC_PO_REQ_INTERFACE_ALL_BI
   /******************************************************************************
      NAME:       XXWC_PO_REQUISITION_LINES_ALL_BI

      PURPOSE:    To assign Sales Velocity Segment to the Notes to Buyer Field

      Logic:     1) Query to the item's sales velocity segment1 category value
                 2) Replace the note to buyer field with the sales velocity value

      REVISIONS:
      Ver        Date        Author           Description
      ---------  ----------  ---------------  ------------------------------------
      1.0        23-FEB-12   Lee Spitzer       1. Created this trigger.
      2.0        05-MAY-12   Lee Spitzer       2. Update for exception handler
      2.1        06-NOV-12   Lee Spitzer       3. Update to change Drop Ship Orders Preparer Id = Buyer Id
      2.2        25-APR-13   Lee Spitzer       4. Update to default subinventrory and project id to support Freight Burden Extension TMS Ticket - 20130102-01229  
      2.3        19-JUL-13   Lee Spitzer       5. Updated Trigger to exclude expense items for Freight Burden as it's not necessary to associate a project to them 20130719-01013.  
      2.4        10-AUG-13   Lee Spitzer       6. Updated Trigger to add batch_id for interface_source_type code of INV to help group requisitions correctly by vendor org for Min-Max and ROP Requests 20130618-01280  
   ******************************************************************************/
BEFORE INSERT
   ON PO.PO_REQUISITIONS_INTERFACE_ALL
   REFERENCING NEW AS NEW OLD AS OLD
   FOR EACH ROW
DECLARE

    l_category_value varchar2(163); --Sales Velocity Category Value
    v_applicable varchar2(1); --If the trigger profile is enabled
    v_category_set_id number; --Profile Option for Category Set Id
       --Added for Exception Handling--
    v_exception        EXCEPTION;
    g_user_id          NUMBER := fnd_global.user_id;
    g_login_id         NUMBER := fnd_profile.value('LOGIN_ID');
    g_err_callfrom     VARCHAR2(75) DEFAULT 'XXWC_PO_REQ_INTERFACE_ALL_BI';
    g_err_callpoint    VARCHAR2(75) DEFAULT 'START';
    g_distro_list      VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
    l_err_msg          VARCHAR2(2000);
    l_message          VARCHAR2(2000);
    
    l_buyer_id          NUMBER; --Added 11/6/2012 to default preparer id to buyer id
    
    --Added Local Variables 4/25/2013 for TMS Ticket - 20130102-01229  
    l_default_project_id NUMBER := fnd_profile.value('XXWC_DEFAULT_DROP_SHIP_PROJECT');
    l_project_id        NUMBER;
    l_default_subinventory VARCHAR2(10);
    l_subinventory      VARCHAR2(10);
    --End Added Local Variables added 4/25/2013 for TMS Ticket - 20130102-01229  
    
BEGIN

  --Obtain the 'Y' or 'N' value FROM the profile option XXWC_RA_INTERFACE_LINES_TRIGGER
   v_applicable := fnd_profile.VALUE ('XXWC_PO_REQUISITION_LINES_ALL_TRIGGER');

   --If the Profile Option is enabled then 
   if v_applicable = 'Y' then


        --We can only obtain the category value if the item id is null
        
        if :NEW.item_id is not null then 


            v_category_set_id := fnd_profile.VALUE ('XXWC_PO_REQUISITION_DEFAULT_CATEGORY_SET');
            
            --if the profile option hasn't been set then do not execute the query;
            
            if v_category_set_id is not null then
            
                begin
                  select mcb.concatenated_segments
                  into   l_category_value
                  from   mtl_item_categories mic,
                         mtl_categories_b_kfv mcb
                  where  :NEW.item_id = mic.inventory_item_id
                  and    :NEW.destination_organization_id = mic.organization_id
                  and    mcb.category_id = mic.category_id
                  and    mic.category_set_id = v_category_set_id;
                exception
                    when others then
                            null; --Do nothing if we hit an exception;
                end;
           
        
                :NEW.NOTE_TO_BUYER := l_category_value; --set the categogry value to note to buyer;
        
            end if;

        end if;
   
        
        --For Drop Ship Orders, Default the SUGGESTED_BUYER_ID to the Preparer ID 
   
        if :NEW.INTERFACE_SOURCE_CODE = 'ORDER ENTRY' THEN
        
               --Look to see if the preparer is an active buyer
               
               BEGIN
                  select agent_id
                  into   l_buyer_id
                  from   po_agents
                  where  agent_id = :NEW.preparer_id
                  and    sysdate between nvl(start_date_active,sysdate-1) and nvl(end_date_active, sysdate+1);
               EXCEPTION
                    WHEN OTHERS THEN
                          l_buyer_id := NULL;
               END;
               
               --If we find a buyer id then default the suggested_buyer_id 
                IF l_buyer_id IS NOT NULL THEN
                
                       :NEW.SUGGESTED_BUYER_ID := l_buyer_id;
                       
                END IF;
                
              
              --added 4/25/2013, TMS Ticket -  20130102-01229  
              --Retrieve the project id and default project subinventory
                
           IF:NEW.DESTINATION_TYPE_CODE = 'INVENTORY' THEN --added If condition on 7/19/2013 20130719-01013.  
               
                  BEGIN
                    SELECT psn.project_id, psn.attribute1
                    into   l_project_id, l_default_subinventory
                    FROM   PJM_PROJECT_PARAMETERS pjp,
                           pjm_seiban_numbers psn
                    WHERE  psn.project_id = l_default_project_id
                    AND    psn.project_id = pjp.project_id
                    and    pjp.organization_Id = :NEW.destination_organization_id;
                  EXCEPTION
                   WHEN OTHERS THEN
                    l_project_id := NULL;
                  END;   
                  
             if l_project_id is not null then
                
                --Check to see if the Default Subinventory is assinged to the organization
                BEGIN
                   SELECT secondary_inventory_name
                   INTO   l_subinventory
                   FROM   mtl_secondary_inventories msi
                   WHERE  secondary_inventory_name = l_default_subinventory
                   AND    organization_id = :NEW.destination_organization_id;
                EXCEPTION
                    WHEN OTHERS THEN
                      l_subinventory := NULL;
                END;
                
              ELSE
              
                    l_subinventory := NULL;
              END IF;

            
              --Set the Project Id and Project Accounting Context Flag to Y
              if l_project_id is not null then --removed 7/19/2013
                :NEW.project_id := l_project_id;
                :NEW.project_accounting_context := 'Y';
              
              END IF;
              
              --Set the Default Subinventory
              IF l_subinventory IS NOT NULL THEN --Removed 7/19/2013
                :NEW.DESTINATION_SUBINVENTORY := l_subinventory;
              END IF;                     
            
            --End Added 4/25/2013, TMS Ticket - 20130102-01229  
           
            END IF;
            
          END IF;

        
        --Added 8/10/2013 TMS Ticket - 20130618-01280  
 
         if :NEW.INTERFACE_SOURCE_CODE = 'INV' THEN
       
              :NEW.BATCH_ID := :NEW.DESTINATION_ORGANIZATION_ID;
   
         END IF;
 
        --End Added 8/10/2013, TMS Ticket - 20130618-01280  
         
          
   end if;
   
exception 
    when others then
      
        l_message := 'Encountered when others exception on trigger';
        l_err_msg := SQLCODE || SQLERRM;
      
       xxcus_error_pkg.xxcus_error_main_api(p_called_from      => g_err_callfrom
                                          ,p_calling           => g_err_callpoint
                                          ,p_ora_error_msg     => l_err_msg
                                          ,p_error_desc        => l_message
                                          ,p_distribution_list => g_distro_list
                                          ,p_module            => 'PO');    
    
    
end;
/

