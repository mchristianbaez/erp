  /******************************************************************************
   NAME:     XXWC_SIGNATURE_CAPTURE_TBL_TRG
  
   PURPOSE:  Trigger to Convert DMS Signature CLOB to BLOB DataType
  
  
   REVISIONS:
   Ver        Date        Author           Description
   ---------  -----------  ---------------  ------------------------------------
   1.0        20-Feb-2015  Gopi Damuluri    Initial Version.
                                            TMS 20150220-00107 - Move DMS Functionality to APEX on EBS
  ******************************************************************************/

DROP TRIGGER APPS.XXWC_SIGNATURE_CAPTURE_TBL_TRG

CREATE OR REPLACE TRIGGER APPS.XXWC_SIGNATURE_CAPTURE_TBL_TRG
   BEFORE INSERT
   ON XXWC.XXWC_SIGNATURE_CAPTURE_TBL
   REFERENCING NEW AS New OLD AS Old
   FOR EACH ROW
DECLARE
 l NUMBER;
BEGIN
    :NEW.SIGNATURE_IMAGE_BLOB := HEXTORAW( NVL(:OLD.SIGNATURE_IMAGE_CLOB, :NEW.SIGNATURE_IMAGE_CLOB));
    :NEW.SIGNATURE_IMAGE_CLOB := NULL;
    :NEW.CREATION_DATE := SYSDATE;
    
EXCEPTION
WHEN OTHERS THEN
RAISE;    
END;
/