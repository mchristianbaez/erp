/***********************************************************************************
File Name: XXWC_CAT_CHANGE_TRG
PROGRAM TYPE: SQL TRIGGER file
HISTORY
PURPOSE: Trigger created to capture any changes done on mtl_item_categories and
         store the data changes in custom table used for generating parent catalog 
		 group relationship data file
====================================================================================
VERSION DATE          AUTHOR(S)       DESCRIPTION
------- -----------   --------------- ----------------------------------------------
1.0     09/23/2012    Rajiv Rathod    Initial creation of the file
************************************************************************************/

CREATE OR REPLACE TRIGGER APPS.XXWC_CAT_CHANGE_TRG
   AFTER UPDATE OR DELETE OR INSERT
   ON MTL_ITEM_CATEGORIES
   REFERENCING OLD AS OLD NEW AS NEW
   FOR EACH ROW
   WHEN (1 = 1)
DECLARE
   l_web_hier   number;
   dmltype      varchar2 (1);
BEGIN
   IF INSERTING
   THEN
      dmltype := 'I';
   ELSIF UPDATING
   THEN
      dmltype := 'U';
   ELSIF DELETING
   THEN
      dmltype := 'D';
   END IF;

   BEGIN
      SELECT   MCS.CATEGORY_SET_ID
        INTO   l_web_hier
        FROM   MTL_CATEGORY_SETS MCS
       WHERE   MCS.CATEGORY_SET_NAME = 'WC Web Hierarchy'
               AND MCS.CATEGORY_SET_ID =
                     NVL (:OLD.CATEGORY_SET_ID, :NEW.CATEGORY_SET_ID)
               AND NVL (:OLD.ORGANIZATION_ID, :NEW.ORGANIZATION_ID) =
                     TO_NUMBER (fnd_profile.VALUE ('XXWC_ITEM_MASTER_ORG'));
   EXCEPTION
      WHEN OTHERS
      THEN
         l_web_hier := NULL;
   END;

   IF (l_web_hier IS NOT NULL)
   THEN
      INSERT INTO XXWC.XXWC_CAT_CHANGE_TAB
        VALUES   (NVL (:OLD.INVENTORY_ITEM_ID, :NEW.INVENTORY_ITEM_ID),
                  NVL (:OLD.ORGANIZATION_ID, :NEW.ORGANIZATION_ID),
                  NVL (:OLD.CATEGORY_SET_ID, :NEW.CATEGORY_SET_ID),
                  :OLD.CATEGORY_ID,
                  :NEW.CATEGORY_ID,
                  NVL (:NEW.CREATED_BY, :OLD.CREATED_BY),
                  NVL (:NEW.LAST_UPDATE_DATE, :OLD.LAST_UPDATE_DATE),
                  XXWC_EGO_EVENT_LOG_TAB_S.NEXTVAL,
                  dmltype);
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      NULL;
END;
/