create or replace trigger APPS.XXWC_BR_DISTRICT_ROLLUP_TRG
   before insert or update on XXWC.XXWC_BR_DISTRICT_ROLLUP
   for each row
declare
l_mgt_desc varchar2(80);
begin
      if inserting then
        if :NEW.DIST_HIER_ID IS NULL THEN
          select APPS.XXWC_BR_DISTRICT_ROLLUP_S.nextval INTO :NEW.DIST_HIER_ID
               from dual;
         end if;
            :NEW.last_update_date := SYSDATE;
        /* if :NEW.last_update_user is null  then
           :NEW.last_update_user := nvl(v('APP_USER'),USER);
         end if;*/
if  :new.mgt_desc is null then 
 select MGT_DESC into l_mgt_desc from XXWC.XXWC_BR_MGMT_LEVELS where mgt_lvl = :new.mgt_lvl;
 :new.mgt_desc := l_mgt_desc;
end if;
      end if;
      if updating then
        :NEW.last_update_date := SYSDATE;
      --   :NEW.last_update_user := nvl(v('APP_USER'),USER);
       if  :new.mgt_lvl != :old.mgt_lvl then 
 select MGT_DESC into l_mgt_desc from XXWC.XXWC_BR_MGMT_LEVELS where mgt_lvl = :new.mgt_lvl;
 :new.mgt_desc := l_mgt_desc;
end if;
      end if;
EXCEPTION
  WHEN OTHERS THEN
    raise_application_error(-20001, 'XXWC_BR_DISTRICT_ROLLUP_TRG: ' || SQLERRM);


   end;