CREATE OR REPLACE TRIGGER APPS.XXWC_OE_ORDER_LINES_BRI_T1 BEFORE INSERT ON "ONT"."OE_ORDER_LINES_ALL" FOR EACH ROW
DECLARE
   /*************************************************************************
     $Header XXWC_OE_ORDER_LINES_BRI_T1 $
     Module Name: XXWC_OE_ORDER_LINES_BRI_T1.sql

     PURPOSE:   This trigger resets the subinventory and
                attribute11 (force ship field) 
                when a new line is created

     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.0        07/29/2012  Shankar Hariharan        Initial Version
   **************************************************************************/

  l_attribute1 varchar2(30);
BEGIN
  select nvl(attribute1,'N')
    into l_attribute1
    from oe_transaction_types_vl
   where transaction_type_id= :new.line_type_id;
  
  IF l_attribute1='Y' then
     IF :new.subinventory=xxwc_om_force_ship_pkg.g_pick_subinv then
     :new.subinventory := xxwc_om_force_ship_pkg.g_subinv;
     END IF;
     :new.attribute11 := null;
  END IF; 
END;
/
