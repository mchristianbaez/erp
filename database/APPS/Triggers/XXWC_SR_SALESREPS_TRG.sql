create or replace trigger APPS.XXWC_SR_SALESREPS_TRG
   before insert or update on XXWC.XXWC_SR_SALESREPS
   for each row

begin
      if inserting then
       null;
      end if;
      if updating then
        :NEW.last_update_date := SYSDATE;
       -- :NEW.last_update_user := NVL(v('APP_USER'),USER);
      end if;
EXCEPTION
  WHEN OTHERS THEN
    raise_application_error(-20001, 'XXWC_SR_SALESREPS_TRG: ' || SQLERRM);


   end;