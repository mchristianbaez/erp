CREATE OR REPLACE TRIGGER APPS.XXWC_PARAM_LIST_WHO
   BEFORE INSERT OR UPDATE
   ON APPS.XXWC_PARAM_LIST
   REFERENCING NEW AS NEW OLD AS OLD
   FOR EACH ROW
BEGIN
   IF INSERTING
   THEN
      :new.created_by := fnd_global.user_id;
      :new.creation_date := SYSDATE;
      :new.last_update_date := SYSDATE;
      :new.last_updated_by := fnd_global.user_id;
      :new.last_update_login := fnd_global.login_id;
   ELSE
      :new.last_update_date := SYSDATE;
      :new.last_updated_by := fnd_global.user_id;
      :new.last_update_login := fnd_global.login_id;
   END IF;
END;
/
