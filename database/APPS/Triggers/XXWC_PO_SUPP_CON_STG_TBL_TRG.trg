CREATE OR REPLACE TRIGGER APPS."XXWC_PO_SUPP_CON_STG_TBL_TRG"
   AFTER INSERT
   ON XXWC.XXWC_PO_SUPPLIER_CONFIRMATIONS
   REFERENCING NEW AS NEW OLD AS OLD
   FOR EACH ROW
DECLARE PRAGMA AUTONOMOUS_TRANSACTION;
   /**************************************************************************
   File Name: XXWC_PO_SUPP_CON_STG_TBL_TRG

   PROGRAM TYPE: SQL Script

   PURPOSE:

   HISTORY
   =============================================================================
          Last Update Date : 18-Apr-2018
   =============================================================================
   =============================================================================
   VERSION DATE          AUTHOR(S)          DESCRIPTION
   ------- -----------   -----------------  ------------------------------------
   1.0     18-Apr-2018   Ashwin Sridhar     Creation of trigger-TMS#20180305-00300--Automate PO Confirmations - Oracle EBS

   =============================================================================
   *****************************************************************************/
   l_err_msg         VARCHAR2 (3000);
   l_sec             VARCHAR2 (255);
   l_msg             VARCHAR2 (150);
   l_err_callfrom    VARCHAR2 (75) DEFAULT 'XXWC_PO_SUPP_CON_STG_TBL_TRG';
   l_err_callpoint   VARCHAR2 (75) DEFAULT 'START';
   l_distro_list     VARCHAR2 (75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
   req_id            NUMBER;
   RESULT            BOOLEAN;
   ln_responsibility_id NUMBER;
   ln_resp_appl_id      NUMBER;
   ln_user_id           NUMBER;
BEGIN
  BEGIN
    SELECT user_id
    INTO   ln_user_id
    FROM   apps.FND_USER
    WHERE  USER_NAME='XXWC_INT_SUPPLYCHAIN';
  EXCEPTION
  WHEN others THEN
    ln_user_id:=NULL;
  END;

  BEGIN
    SELECT responsibility_id
    ,      application_id
    INTO   ln_responsibility_id
    ,      ln_resp_appl_id
    FROM   apps.FND_RESPONSIBILITY_VL
    WHERE  RESPONSIBILITY_KEY='XXWC_PUR_SUPER_USER';
  EXCEPTION
  WHEN others THEN
    ln_responsibility_id:=NULL;
    ln_resp_appl_id     :=NULL;
  END;
  
   RESULT   := fnd_request.set_mode (TRUE);
   apps.fnd_global.apps_initialize(ln_user_id,ln_responsibility_id,ln_resp_appl_id);
   req_id   :=fnd_request.submit_request ('XXWC'
                                        , 'XXWC_PO_SUPP_CONF'
                                        , ''
                                        , TO_CHAR (SYSDATE + 30 / 86400, 'DD-MON-YYYY HH24:MI:SS')
                                        , FALSE
                                        , TO_NUMBER(:NEW.PO_NUMBER)
                                        );
   COMMIT;
 
   IF req_id = 0 THEN
     raise_application_error (-20000,'PO_Number : '||:NEW.PO_NUMBER||' - '||l_err_msg);
   END IF;          

EXCEPTION
WHEN others THEN
      l_msg := 'Failure in Trigger XXWC_AR_INV_STG_TBL_TRG: ' || SQLERRM;
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api (
         p_called_from         => l_err_callfrom
        ,p_calling             => l_err_callpoint
        ,p_ora_error_msg       => SQLERRM
        ,p_error_desc          => l_msg
        ,p_argument1           => :new.PO_NUMBER
        ,p_distribution_list   => l_distro_list
        ,p_module              => 'AR');
END;
/