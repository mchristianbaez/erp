CREATE OR REPLACE TRIGGER APPS.XXWC_EGO_CHECK_ITEM_TRG
   AFTER UPDATE
   ON ENG_ENGINEERING_CHANGES
   REFERENCING OLD AS OLD NEW AS NEW
   FOR EACH ROW
WHEN (
1=1
      )
BEGIN
   XXWC_EGO_ITEM_IMPORT_PKG.CHECK_ITEM_DUP(:NEW.CHANGE_ID, :NEW.STATUS_CODE);
EXCEPTION
   WHEN OTHERS
   THEN
      NULL;
END;
/
