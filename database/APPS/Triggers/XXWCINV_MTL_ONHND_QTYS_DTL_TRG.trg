CREATE OR REPLACE TRIGGER XXWCINV_MTL_ONHND_QTYS_DTL_TRG
BEFORE DELETE On MTL_ONHAND_QUANTITIES_DETAIL
FOR EACH ROW
WHEN (
OLD.subinventory_code in ('Staging','Xfer Disc') --Added 11/26/2012
--OLD.subinventory_code = ('Staging') --Removed 11/26/2012
      )
Begin

  -- insert records into staging table for shipments
  Insert Into XXWC.XXWCINV_INTERCO_TRANSFERS
         (transaction_id
         ,inventory_item_id
         ,organization_id
         ,subinventory_code
         ,locator_id
         ,revision
         ,orig_date_received
         ,quantity
         ,creation_date
         )
  Values (NULL                                 -- transaction_id
         ,:OLD.inventory_item_id               -- inventory_item_id
         ,:OLD.organization_id                 -- organization_id
         ,:OLD.subinventory_code               -- subinventory_code
         ,:OLD.locator_id                      -- location_id
         ,:OLD.revision                        -- revision
         ,:OLD.orig_date_received              -- orig_date_received
         ,:OLD.primary_transaction_quantity    -- quantity 
         ,SYSDATE                              -- creation_date
         );
         
Exception
  When OTHERS Then
    NULL;
End;
/
