CREATE OR REPLACE TRIGGER APPS.xxwc_mtl_forecast_param_trg
   BEFORE INSERT
   ON xxwc.xxwc_mtl_forecast_param_tbl
   FOR EACH ROW
/*************************************************************************
Copyright (c) 2012 HD Supply
All rights reserved.
**************************************************************************
   $Header XXWC_MTL_FORECAST_PARAM_TRG $
  Module Name: XXWC_MTL_FORECAST_PARAM_TRG
  PURPOSE:  To generate sno in XXWC_MTL_FORECAST_PARAM_TBL
  -- VERSION DATE          AUTHOR(S)           DESCRIPTION
  -- ------- -----------   ------------      -----------------------------------------
  -- 1.0     15-JUN-2017   P.Vamshidhar       TMS#20170228-00077 - Performance issue with XXWC MTL Generate Demand Forecast Program
************************************************************************************************/
BEGIN
   IF INSERTING
   THEN
     SELECT apps.xxwc_mtl_forecast_param_seq.nextval INTO :NEW.SNO FROM DUAL;
   END IF;
END;
/