create or replace 
TRIGGER APPS.XXWCINV_MTL_MATERIAL_TRX_TRG
AFTER INSERT On MTL_MATERIAL_TRANSACTIONS
FOR EACH ROW
Declare

  XXWC_ERROR                           EXCEPTION;
  
  X_RETURN      VARCHAR2(1);
  
  l_msg         VARCHAR2(1000);
  
  -- cursor to find records in staging table not related to inter-company transaction
  Cursor C1 Is
    Select orig_date_received,
           quantity,
           rowid row_id
      From XXWC.XXWCINV_INTERCO_TRANSFERS
     Where inventory_item_id = :NEW.inventory_item_id
       And organization_id = :NEW.organization_id
       And Nvl(revision, '~') = Nvl(:NEW.revision,'~')
       And subinventory_Code = :NEW.subinventory_code
       And Nvl(locator_id, -1) = Nvl(:NEW.locator_id,-1)
       And transaction_id Is NULL
       --Added 8/26/2013
       AND  nvl(shipment_number,:NEW.shipment_number) = :NEW.shipment_number
     --Order By creation_date;  --Removed 10/10/2013 TMS Ticket 20131011-00343 
     ORDER BY orig_date_received asc; --10/10/2013 TMS Ticket 20131011-00343 
  
  l_trx_type_id                        NUMBER;
  l_dummy                              VARCHAR2(1);
  l_qty                                NUMBER;
  
  p_trx_id NUMBER;
  p_xfer_trx_id NUMBER;
  l_message varchar2(10);
  l_exists NUMBER;
  
  l_stg_qty NUMBER;
  
  l_remaining_qty NUMBER;
  
  
BEGIN

  Begin

    -- determine if record originated as inter-org transfer
      Select transaction_type_id
      Into l_trx_type_id
      From MTL_TRANSACTION_TYPES MTT
      Where MTT.transaction_type_id = :NEW.transaction_type_id
     --And MTT.transaction_type_name = ('Int Order Intr Ship') --Removed 11/26/2012
     And MTT.transaction_type_name in ('Int Order Intr Ship','Intransit Shipment');--Added 11/26/2012

  Exception
    When NO_DATA_FOUND Then
      --RAISE XXWC_ERROR; --Removed 11/27/2012
       l_trx_type_id := NULL; --Added 11/27/2012
       
  End;

  if l_trx_type_id is not null then
  
    l_qty := Abs(:NEW.transaction_quantity);

    -- associate intercompany shipment transaction to unassociated records
          For R1 In C1 Loop
          
            l_remaining_qty := nvl(abs(l_qty),0) - nvl(abs(r1.quantity),0);
          
            Update XXWC.XXWCINV_INTERCO_TRANSFERS
               SET transaction_id = :NEW.transaction_id
             WHERE ROWID = R1.row_id
             and   transaction_id is null; --added 10/2/2013 TMS Ticket 20131011-00343  
               
            l_qty := l_qty - R1.quantity;

            
           --IF l_qty <= 0 THEN --Removed 10/10/2013 TMS Ticket 20131011-00343 
           IF l_remaining_qty = 0 THEN
              EXIT;
              --Added 10/10/2013 TMS Ticket 20131011-00343 
           ELSIF l_remaining_qty < 0 THEN
              
                --To keep the quantities in sync, we have to update the QTY in the staging table and insert the balance back into the table
                --added 10/10/2013 TMS Ticket 20131011-00343  
                 UPDATE XXWC.XXWCINV_INTERCO_TRANSFERS
                 SET quantity = nvl(quantity,0) - nvl(abs(l_remaining_qty),0)
                 WHERE ROWID = R1.row_id
                 ; 
              
              --added 10/10/2013 TMS Ticket 20131011-00343  
               Insert Into XXWC.XXWCINV_INTERCO_TRANSFERS
                                         (transaction_id
                                         ,inventory_item_id
                                         ,organization_id
                                         ,subinventory_code
                                         ,locator_id
                                         ,revision
                                         ,orig_date_received
                                         ,quantity
                                         ,creation_date
                                         ,shipment_number
                                         )
                SELECT  NULL
                     ,inventory_item_id
                     ,organization_id
                     ,subinventory_code
                     ,locator_id
                     ,revision
                     ,orig_date_received
                     ,abs(l_remaining_qty)
                     ,creation_date
                     ,shipment_number
              FROM   XXWC.XXWCINV_INTERCO_TRANSFERS
              WHERE  rowid = R1.row_id;
            
              EXIT;
            ELSE
                
                NULL;
              
            End If;
          
         End Loop;
    
   end if;
   

  BEGIN
       SELECT :NEW.TRANSACTION_ID,
               :NEW.transfer_transaction_id
       INTO p_trx_id, p_xfer_trx_id
       FROM MTL_TRANSACTION_TYPES MTT
       WHERE :NEW.transaction_type_id = MTT.transaction_type_id
       And MTT.transaction_type_name in ('Int Req Intr Rcpt','Intransit Receipt');
       
  EXCEPTION
            when others then
                 l_message := substr(SQLERRM,1,10);
                  p_trx_id := NULL;
                  p_xfer_trx_id := NULL;
            
  END;
    
  
  IF P_TRX_ID IS NOT NULL AND P_XFER_TRX_ID IS NOT NULL THEN
    
        
        BEGIN XXWCINV_INV_AGING_PKG.Update_Orig_Date_Rcvd_API(P_TRX_ID, P_XFER_TRX_ID); 
        EXCEPTION
            WHEN OTHERS THEN
                l_msg := ' ' || SQLCODE ||' '|| SQLERRM ||' ' ||DBMS_UTILITY.FORMAT_ERROR_STACK || '@' || DBMS_UTILITY.FORMAT_CALL_STACK;
                
                INSERT INTO XXWC.XXWC_INTERCO_EXCEPTION
                    (p_trx_id
                    ,p_xfer_trx_id
                    ,primary_quantity
                    ,process_flag 
                    ,MESSAGE)
                   VALUES
                    (p_trx_id
                    ,p_xfer_trx_id
                    ,:NEW.PRIMARY_QUANTITY
                    ,1
                    ,'WHEN OTHERS ERROR RUNNING BOD EXTENSION ' || SYSDATE || substr(l_msg,1,1000));
                            
        END;
      
                 
  END IF;

   

Exception
  When OTHERS Then
    NULL;
END; 