CREATE OR REPLACE TRIGGER APPS.XXWC_ECE_TP_DETAILS_AIUR_TRG
   /*************************************************************************
     $Header XXWC_ECE_TP_DETAILS_AIUR_TRG.trg $
     Module Name: XXWC_ECE_TP_DETAILS_AIUR_TRG

     PURPOSE: Trigger to invoke Procedure to disable the history PO transactions being 
	      triggered for EDI when the site is configured for EDI

     TMS Task Id :  20140421-00041

     REVISIONS:
     Ver        Date        Author                Description
     ---------  ----------  ------------------    ----------------
     1.0        04/30/2014  Manjula Chellappan    Initial Version
     1.1        10/28/2014  Manjula Chellappan    Modified for TMS #20141022-00012
     1.2        02/06/2015  Manjula Chellappan    Modified for TMS #20141218-00011

   **************************************************************************/

   AFTER INSERT OR UPDATE
   ON apps.ECE_TP_DETAILS
   FOR EACH ROW
-- Included document_id 'POCO' for Version 1.1 By Manjula on 10/28/2014 
-- Removed document_id 'POCO' for Version 1.2 By Manjula on 02/06/2015 - A New procedure is written for POCO
-- WHEN (new.document_id IN ('POO','POCO') AND new.edi_flag = 'Y')
 WHEN (new.document_id IN ('POO') AND new.edi_flag = 'Y')

DECLARE
   l_error_msg   VARCHAR2 (4000);
BEGIN
   APPS.XXWC_EDI_PO_HIST_TXN_DISABLE (:new.tp_header_id, l_error_msg);

-- changed the Error code to -20000 for Version 1.2 By Manjula on 02/06/2015

   IF l_error_msg IS NOT NULL
   THEN
      raise_application_error (
         -20000,
         'History PO transactions are not disabled for EDI'||substr(l_error_msg,50));
   END IF;
END;
/

SHO ERR
/
