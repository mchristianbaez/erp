CREATE OR REPLACE TRIGGER APPS.XXWCINV_MTL_MATERIAL_TRX_TRG
   BEFORE INSERT
   ON MTL_MATERIAL_TRANSACTIONS
   FOR EACH ROW
DECLARE
   XXWC_ERROR      EXCEPTION;

   -- cursor to find records in staging table not related to inter-company transaction
   CURSOR C1
   IS
        SELECT orig_date_received, quantity, ROWID row_id
          FROM XXWC.XXWCINV_INTERCO_TRANSFERS
         WHERE     inventory_item_id = :NEW.inventory_item_id
               AND organization_id = :NEW.organization_id
               AND NVL (revision, '~') = NVL (:NEW.revision, '~')
               AND subinventory_Code = :NEW.subinventory_code
               AND NVL (locator_id, -1) = NVL (:NEW.locator_id, -1)
               AND transaction_id IS NULL
      ORDER BY creation_date;

   l_trx_type_id   NUMBER;
   l_dummy         VARCHAR2 (1);
   l_qty           NUMBER;
BEGIN
   BEGIN
      -- determine if record originated as inter-org transfer
      SELECT transaction_type_id
        INTO l_trx_type_id
        FROM MTL_TRANSACTION_TYPES MTT
       WHERE     MTT.transaction_type_id = :NEW.transaction_type_id
             AND MTT.transaction_type_name = 'Int Order Intr Ship';
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         RAISE XXWC_ERROR;
   END;

   l_qty := ABS (:NEW.transaction_quantity);

   -- associate intercompany shipment transaction to unassociated records
   FOR R1 IN C1
   LOOP
      UPDATE XXWC.XXWCINV_INTERCO_TRANSFERS
         SET transaction_id = :NEW.transaction_id
       WHERE ROWID = R1.row_id;

      l_qty := l_qty - R1.quantity;

      IF l_qty <= 0
      THEN
         EXIT;
      END IF;
   END LOOP;
EXCEPTION
   WHEN OTHERS
   THEN
      NULL;
END;
/
