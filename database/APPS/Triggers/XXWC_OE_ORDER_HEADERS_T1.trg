CREATE OR REPLACE TRIGGER APPS.XXWC_OE_ORDER_HEADERS_T1
   BEFORE UPDATE
   ON apps.OE_ORDER_HEADERS_ALL
   REFERENCING NEW AS New OLD AS Old
   FOR EACH ROW
WHEN (
New.order_source_id in (1021,1001) --TMS #  20140219-00148(RVELICHETTI) added additional B2B LIASON 
         AND NVL (Old.booked_flag, 'N') = 'N'
         AND New.booked_flag = 'Y'
      )
DECLARE
/******************************************************************************
   NAME:       xxwc_oe_order_headers_t1
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2/25/2014      shariharan       1. Created this trigger.
   2.0        5/14/2014      rvelichetti     2. Modified the trigger to accomodate B2B LIASON
   NOTES:

******************************************************************************/
BEGIN
   SELECT xxwc_po_helpers.xxwc_find_user_name (fnd_global.USER_ID)
     INTO :New.attribute7
     FROM DUAL;

   :New.created_by := fnd_global.user_id;
END xxwc_oe_order_headers_t1;
/