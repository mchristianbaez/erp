CREATE OR REPLACE TRIGGER APPS.XXWC_OE_LINES_IFACE_ALL_TRG1
BEFORE INSERT
ON ONT.OE_LINES_IFACE_ALL
REFERENCING NEW AS New OLD AS Old
FOR EACH ROW
WHEN (
NEW.ORDER_SOURCE_ID = 10
      )
DECLARE

/******************************************************************************
   NAME:       XXWC_OE_LINES_IFACE_ALL_TRG1
   PURPOSE:    

   REVISIONS:
   Ver        Date        Author            Description
   ---------  ----------  ---------------   ------------------------------------
   1.0        11/07/2012    Consuelo Gonzalez 1. Initial definition to default ship
                                                 from org id on the order header 
   1.3        16/OCT/2014 Maharajan Shunmugam    TMS# 20141001-00161 Canada Multi org changes

   NOTES:

   Automatically available Auto Replace Keywords:
      Object Name:     XXWC_OE_LINES_IFACE_ALL_TRG1
      Sysdate:         11/07/2012
      Date and Time:   11/07/2012, 3:29:53 PM, and 8/9/2012 3:29:53 PM
      Username:         (set in TOAD Options, Proc Templates)
      Table Name:      OE_LINES_IFACE_ALL (set in the "New PL/SQL Object" dialog)
      Trigger Options:  (set in the "New PL/SQL Object" dialog)
******************************************************************************/

    l_line_notes         VARCHAR2(240);

BEGIN
    l_line_notes        := null;
                                              
    begin
        SELECT req_ln.attribute2 -- Notes     
         INTO  l_line_notes               
         FROM po_requisition_headers req_hdr,
              po_requisition_lines req_ln
        WHERE req_hdr.type_lookup_code = 'INTERNAL'
          AND req_hdr.requisition_header_id = :new.orig_sys_document_ref
          AND req_hdr.requisition_header_id = req_ln.requisition_header_id
          AND req_ln.requisition_line_id = :new.orig_sys_line_ref
          AND rownum = 1;
    exception
    when others then
        l_line_notes        := null;
    end;    
   
   :New.shipping_instructions := l_line_notes;
   
   EXCEPTION
     WHEN OTHERS THEN
       -- Consider logging the error and then re-raise
       null;
END XXWC_OE_LINES_IFACE_ALL_TRG1;
/
