CREATE OR REPLACE TRIGGER XXWC_AR_PAYMENT_SCHEDULES_TRG
  AFTER UPDATE OF amount_due_remaining
  ON ar.ar_payment_schedules_all
  REFERENCING NEW AS NEW OLD AS OLD
  FOR EACH ROW
WHEN (
new.org_id = 162 and 
new.class IN ('INV','CM','DM') and 
new.status = 'OP'
      )
DECLARE

  /**********************************************************************************************
   File Name: APPS.xxwc_ar_payment_schedules_trg
  
   PROGRAM TYPE: TRIGGER
  
   PURPOSE: Trigger to update reason code for invoice
  
   HISTORY
   =============================================================================
   =============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- ----------------------------------------
   1.0     12-Dec-2013   Kathy Poling     Created.
                                          SR 222712
   1.1     10-Mar-2014   Maharajan        TMS# 20130908-00094
		         Shunmugam        Added CM and DM in WHEN clause
                       
  ***********************************************************************************************/

  --initialize
  l_err_msg       VARCHAR2(3000);
  l_sec           VARCHAR2(255);
  l_err_callfrom  VARCHAR2(75) DEFAULT 'XXWC_AR_PAYMENT_SCHEDULES_TRG';
  l_err_callpoint VARCHAR2(75) DEFAULT 'START';
  l_distro_list   VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';

  l_sale_tax             VARCHAR2(75) := 'SALES TAX';
  l_due                  NUMBER;
  l_tax                  NUMBER;
  l_payment_schedule_id  NUMBER;
  l_amount_due_original  NUMBER;
  l_amount_due_remaining NUMBER;
  l_tax_original         NUMBER;
  l_status               VARCHAR2(75);
  l_class                VARCHAR2(75);
  l_customer_trx_id      NUMBER;
  l_created_by           NUMBER;
  l_last_updated_by      NUMBER;

BEGIN

  IF :new.amount_due_remaining != :old.amount_due_remaining
  THEN
    
    INSERT INTO xxwc.xxwc_invoice_shortpay_tbl
      (payment_schedule_id
      ,amount_due_original
      ,amount_due_remaining
      ,tax_original
      ,status
      ,CLASS
      ,customer_trx_id
      ,attribute_category
      ,attribute1
      ,created_by
      ,creation_date
      ,last_update_date
      ,last_updated_by)
    VALUES
      (:new.payment_schedule_id
      ,:new.amount_due_original
      ,:new.amount_due_remaining
      ,:new.tax_original
      ,:new.status
      ,:new.class
      ,:new.customer_trx_id
      ,:new.attribute_category
      ,:new.attribute1
      ,:new.created_by
      ,:new.creation_date
      ,:new.last_update_date
      ,:new.last_updated_by);
  
  END IF;

EXCEPTION
  WHEN OTHERS THEN
    l_err_msg := l_sec || ' Error_Stack...' ||
                 dbms_utility.format_error_stack() || ' Error_Backtrace...' ||
                 dbms_utility.format_error_backtrace();
  
    xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                        ,p_calling           => 'Reason Code Discrepancies'
                                        ,p_ora_error_msg     => l_err_msg
                                        ,p_error_desc        => 'Error updating reason code for payment_schedule_id: ' ||
                                                                :new.payment_schedule_id
                                        ,p_distribution_list => l_distro_list
                                        ,p_module            => 'AR');
END;
/