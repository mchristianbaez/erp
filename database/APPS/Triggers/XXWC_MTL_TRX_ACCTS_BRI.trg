CREATE OR REPLACE TRIGGER APPS.XXWC_MTL_TRX_ACCTS_BRI
/******************************************************************************
   NAME:       XXWC_MTL_TRX_ACCTS_BRI

   PURPOSE:    To assign the correct COGS account based upon an item's cost elements

   Logic:     1) Get the company (segment1) from the Order Type
              2) Keep the remaining segments as it is.

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        05-Jun-11   Gopi Damuluri        1. Created this trigger.
   1.1        18-Sep-12   Gopi Damuluri        Added the condition that Sales Order/
                                               internal order/RMA transaction types
   1.2        20-Oct-14	  Pattabhi Avula       TMS# 20141001-00172 Multi Org Changes done
******************************************************************************/
BEFORE INSERT
ON APPS.MTL_TRANSACTION_ACCOUNTS
FOR EACH ROW
DECLARE

   l_coa_id        NUMBER;
   v_segment1      gl_code_combinations.segment1%TYPE := NULL;
   v_segment2      gl_code_combinations.segment2%TYPE := NULL;
   v_segment3      gl_code_combinations.segment3%TYPE := NULL;
   v_segment4      gl_code_combinations.segment4%TYPE := NULL;
   v_segment5      gl_code_combinations.segment5%TYPE := NULL;
   v_segment6      gl_code_combinations.segment6%TYPE := NULL;
   v_segment7      gl_code_combinations.segment7%TYPE := NULL;

   v_new_cogs_ccid NUMBER;

   l_material_overhead_account NUMBER;
   l_segment2      gl_code_combinations.segment2%TYPE := NULL;
   v_new_ccid      NUMBER;
   v_class_code    wip_accounting_classes.class_code%type := NULL;
   v_exception exception;
   l_applicable    varchar2(10);

   l_direct_acct       ar_lookups.externally_visible_flag%TYPE;
   l_int_comp_acct     ar_lookups.description%TYPE;
   l_item_src          oe_order_lines_all.source_type_code%TYPE;
   l_item_type         VARCHAR2 (30);
   g_dflt_cat_class    VARCHAR2(5) := 'DFLT';
   l_ordr_line_category  oe_transaction_types_all.order_category_code%TYPE;
   l_ordr_line_id        NUMBER;

    l_status          BOOLEAN;
    l_concat_segs     VARCHAR2(80);
    l_cust_num   hz_cust_accounts_all.account_number%TYPE;

BEGIN

--COGS account is only applicable for Sales Order/internal order/RMA transaction types
IF (:new.transaction_source_type_id in (2, 8, 12) and :new.accounting_line_type = 35) THEN
    ----------------------------------------------------------------------------------
    -- Derive Customer and Order Details
    ----------------------------------------------------------------------------------
    BEGIN
      select distinct hca.account_number
           , oola.source_type_code
           , ottl.order_category_code
           , oola.line_id
        INTO l_cust_num
           , l_item_src
           , l_ordr_line_category
           , l_ordr_line_id
        from apps.oe_order_headers      ooha
           , apps.oe_order_lines        oola
           , apps.oe_transaction_types  ottl
           , mtl_material_transactions  mmt
           , apps.hz_cust_accounts      hca
       where ooha.header_id              = oola.header_id
         and hca.cust_account_id         = ooha.sold_to_org_id
         and oola.line_id                = mmt.trx_source_line_id
         AND oola.line_type_id           = ottl.transaction_type_id
         AND ottl.transaction_type_code  = 'LINE'
         AND oola.org_id                 = ottl.org_id
         and mmt.transaction_id          = :NEW.transaction_id;
    EXCEPTION
    WHEN OTHERS THEN
      RAISE v_exception;
    END;

    ----------------------------------------------------------------------------------
    -- Check if the Customer is an Intercompany Customer
    ----------------------------------------------------------------------------------
    BEGIN
     SELECT ar_l.description
       INTO l_int_comp_acct
       FROM ar_lookups ar_l
      WHERE ar_l.lookup_type  = 'XXWC_INTERCOMPANY_ACCOUNT'
        AND ar_l.enabled_flag = 'Y'
        AND SYSDATE BETWEEN ar_l.start_date_active AND NVL(ar_l.end_date_active,SYSDATE + 1)
        AND ar_l.lookup_code  = l_cust_num;
    EXCEPTION
    WHEN OTHERS THEN
      l_int_comp_acct := NULL;
    END;

    ----------------------------------------------------------------------------------
    -- If the Customer is an Intercompany Customer, perform nothing
    ----------------------------------------------------------------------------------
    IF l_int_comp_acct IS NOT NULL THEN
      RAISE v_exception;
    END IF;

    ----------------------------------------------------------------------------------
    -- If the OrderLine Category is RETURN, derive ActualOrder Line SourceType
    ----------------------------------------------------------------------------------
    IF l_ordr_line_category = 'RETURN' THEN
       BEGIN
         SELECT ool_ref.source_type_code
           INTO l_item_src
           FROM apps.oe_order_lines ool
              , apps.oe_order_lines ool_ref
          WHERE 1                     = 1
            AND ool.line_id           = l_ordr_line_id
            AND ool.reference_line_id = ool_ref.line_id;
       EXCEPTION
       WHEN OTHERS THEN
         l_item_src := 'INTERNAL';
       END;
    END IF;

     BEGIN
        -- Get chart of accounts ID.
        SELECT chart_of_accounts_id
          INTO l_coa_id
          FROM org_organization_definitions ood
         WHERE ood.organization_id = :new.organization_id;
     EXCEPTION
     WHEN OTHERS THEN
       RAISE v_exception;
     END;

     BEGIN
       -- Get the other segments from the Item COGS account.
       SELECT glcc.segment1
             ,glcc.segment2
             ,glcc.segment3
             ,glcc.segment4
             ,glcc.segment5
             ,glcc.segment6
             ,glcc.segment7
         INTO v_segment1
             ,v_segment2
             ,v_segment3
             ,v_segment4
             ,v_segment5
             ,v_segment6
             ,v_segment7
         FROM mtl_system_items_b msib
             ,gl_code_combinations glcc
        WHERE msib.cost_of_sales_account = glcc.code_combination_id
          AND msib.inventory_item_id     = :new.inventory_item_id
          AND msib.organization_id       = :new.organization_id;

     EXCEPTION
     WHEN NO_DATA_FOUND THEN
         BEGIN
           -- Get the other segments from the Organization COGS account.
           SELECT glcc.segment1
                 ,glcc.segment2
                 ,glcc.segment3
                 ,glcc.segment4
                 ,glcc.segment5
                 ,glcc.segment6
                 ,glcc.segment7
             INTO v_segment1
                 ,v_segment2
                 ,v_segment3
                 ,v_segment4
                 ,v_segment5
                 ,v_segment6
                 ,v_segment7
             FROM mtl_parameters mp
                 ,gl_code_combinations glcc
            WHERE mp.cost_of_sales_account = glcc.code_combination_id
              AND mp.organization_id       = :new.organization_id;

         EXCEPTION
           WHEN OTHERS THEN NULL;
         END;
     WHEN OTHERS THEN
         NULL;
     END;

      ----------------------------------------------------------------------------------
      -- Derive COGS Account
      ----------------------------------------------------------------------------------
      BEGIN
      SELECT arl.attribute1
        INTO l_direct_acct
        FROM mtl_item_categories_v micv
           , ar_lookups arl
       WHERE 1 = 1
         AND arl.lookup_type           = 'XXWC_REVENUE_ACCOUNT'
         AND arl.lookup_code           = micv.segment2
         AND SYSDATE BETWEEN arl.start_date_active AND NVL(arl.end_date_active,SYSDATE + 1)
         AND NVL(arl.enabled_flag,'Y') = 'Y'
         AND micv.category_set_name    = 'Inventory Category'
         AND micv.inventory_item_id    = :NEW.inventory_item_id
         AND micv.organization_id      = :NEW.organization_id;
      EXCEPTION
      WHEN OTHERS THEN
         BEGIN
         SELECT arl.attribute1
           INTO l_direct_acct
           FROM ar_lookups arl
          WHERE lookup_type           = 'XXWC_REVENUE_ACCOUNT'
            AND lookup_code           = g_dflt_cat_class
            AND SYSDATE BETWEEN start_date_active AND NVL(end_date_active,SYSDATE + 1)
            AND NVL(enabled_flag,'Y') = 'Y';

         EXCEPTION
         WHEN OTHERS THEN
           NULL;
         END;
      END;

      IF l_item_src = 'EXTERNAL' AND l_direct_acct IS NOT NULL THEN
        v_segment4 := l_direct_acct;
      END IF;

    BEGIN
      l_concat_segs := v_segment1||'.'||v_segment2||'.'||v_segment3||'.'||v_segment4||'.'||v_segment5||'.'||v_segment6||'.'||v_segment7;

      l_status := fnd_flex_keyval.validate_segs('CREATE_COMBINATION' --operation
                                               , 'SQLGL'
                                                --appl_short_name
                                               , 'GL#'
                                                --key_flex_code
                                               , l_coa_id
                                                --structure_number
                                               , l_concat_segs
                                                --concat_segments
                                               , 'V'
                                                --values_or_ids
                                               , SYSDATE
                                                --validation_date
                                               , 'ALL'
                                                --displayable
                                               , NULL
                                                --data_set
                                               , NULL
                                                --vrule
                                               , NULL
                                                --where_clause
                                               , NULL
                                                --get_columns
                                               , FALSE
                                                --allow_nulls
                                               , FALSE
                                                --allow_orphans
                                               , NULL
                                                --allow_orphans
                                               , NULL
                                                --resp_id
                                               , NULL
                                                --user_id
                                               , NULL
                                                --select_comb_from_view
                                               , NULL
                                                --no_combmsg
                                               , NULL
                                                --where_clause_msg
                                                );

    IF l_status THEN
      v_new_cogs_ccid        := fnd_flex_keyval.combination_id();
      :new.reference_account := v_new_cogs_ccid;
    ELSE
      :new.reference_account := :new.reference_account;
    END IF;
    EXCEPTION
      WHEN no_data_found THEN
        fnd_file.put_line(fnd_file.output,
                          'Can not create ccid for:  ' || l_concat_segs);
    END;

END IF; --COGS account is only applicable for Sales Order/internal order/RMA transaction types

EXCEPTION
  when v_exception then
          null;
  when others then
          null;
END XXWC_MTL_TRX_ACCTS_BRI;
/
