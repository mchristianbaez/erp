CREATE OR REPLACE TRIGGER APPS.XXWC_HZ_CUST_SITE_USES_BIR_TRG
   BEFORE INSERT
   ON HZ_CUST_SITE_USES_ALL
   FOR EACH ROW
  /*************************************************************************
   *   $Header APPS.XXWC_HZ_CUST_SITE_USES_BIR_TRG$
   *   Module Name: APPS.XXWC_HZ_CUST_SITE_USES_BIR_TRG.trg
   *
   *   PURPOSE:   This package is used by the workflow to create profile class and amount
   *
   *   REVISIONS:
   *   Ver        Date        Author                  Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0       		                              Initial Version
   *   1.1        09/04/2015  Manjula Chellappan      TMS# 20150611-00059 - Customer - Profile and Profile Amount tab not populating timely
***************************************************************************/
DECLARE
   -- variable declarations
   x_return_status                  VARCHAR2 (10);
   x_msg_count                      NUMBER;
   x_msg_data                       VARCHAR2 (2000);
   l_itemtype                       VARCHAR2 (100) := 'XXWCHZAP';
   l_itemkey                        VARCHAR2 (100);
   l_system_date                    VARCHAR2 (10);
   save_threshold                   NUMBER;
   v_request_id                     NUMBER DEFAULT 0;
   result                           BOOLEAN;
   l_User_Name                      FND_USER.USER_NAME%TYPE;
   C_SALESREP_NO_CREDITS   CONSTANT VARCHAR2 (50) := 'No Sales Credit';
   l_Module_Name                    VARCHAR2 (80)
                                       := 'XXWC_HZ_CUST_SITE_USES_BIR_TRG';

   -- Defined Cursors May29 2012 :SatishU : To assign Salesrep on newly created Site use Records

   CURSOR No_Sales_Credit_Cur
   IS
      SELECT jrs.salesrep_id salesrep_id
            ,jrs.resource_id
            ,jrs.person_id
            ,jrs.org_id
            ,jrs.salesrep_number
        FROM apps.jtf_rs_salesreps_mo_v jrs  -- Replaced the "jtf_rs_salesreps" table  by pattabhi on 10/15/2014 for 20141001-00172
       WHERE     UPPER (jrs.name) = UPPER (C_SALESREP_NO_CREDITS)
             AND jrs.Org_ID = :NEW.ORG_ID;

   CURSOR Salesrep_Cur
   IS
      SELECT jrs.salesrep_id salesrep_id
            ,jrs.resource_id
            ,jrs.person_id
            ,jrs.org_id
            ,jrs.salesrep_number
            ,NVL (jrs.name, papf.full_name) salesrep_name
            ,jrs.start_date_active
            ,jrs.end_date_active
            ,papf.employee_number
        FROM apps.jtf_rs_salesreps_mo_v jrs  -- Replaced the "jtf_rs_salesreps" table  by pattabhi on 10/15/2014 for 20141001-00172
            ,jtf_rs_resource_extns jrre
            ,hr.per_all_people_f papf
            --   , hz_cust_site_uses_all hcsu
            ,hz_cust_acct_sites_all hcas
       WHERE     jrs.resource_id = jrre.resource_id(+)
             AND jrs.person_id = papf.person_id(+)
             AND TRUNC (papf.effective_start_date(+)) <= TRUNC (SYSDATE)
             AND NVL (TRUNC (papf.effective_end_date(+)), TRUNC (SYSDATE)) >=
                    TRUNC (SYSDATE)
             AND hcas.CUST_ACCT_SITE_ID = :NEW.CUST_ACCT_SITE_ID
             AND jrs.salesrep_ID =
                    xxwc_cust_credit_limit_pkg.GET_SALESREP_ID (
                       hcas.Cust_Account_ID);
BEGIN
   xxwc_cust_credit_limit_pkg.log_msg (1
                                      ,l_Module_Name
                                      ,'++++++++++++++++++++++++++++++++++');

   -- 25-MAY-2012 : Satish U:  Added code to derive User Name
   BEGIN
      SELECT User_Name
        INTO l_User_Name
        FROM Fnd_User
       WHERE User_ID = NVL (:NEW.CREATED_BY, FND_GLOBAL.User_ID);
   EXCEPTION
      WHEN OTHERS
      THEN
         l_User_Name := 'SYSADMIN';
   END;

   SELECT TO_CHAR (SYSDATE, 'MMDDYY') INTO l_system_date FROM DUAL;

   --- 25-MAY-2012 : Satish U : Made changes to  Item Key Using Primary Key of the table.
   /*********Satish U****************
   SELECT      'XXWCHZAP-'
            || l_system_date
            || '-'
            || XXWC_AR_CREATE_PROFILE_S.NEXTVAL
     INTO   l_itemkey
     FROM   DUAL;
   *********************************/
   l_ItemKey := TO_CHAR (:NEW.Site_Use_ID);

   -- May 29 2012 : Added code to assign Salesrep on SHIP_TO and BILL_TO sites
   xxwc_cust_credit_limit_pkg.log_msg (
      1
     ,l_Module_Name
     ,'100: ++++++++++++++++++++++++++++++++++');

   IF :NEW.SITE_USE_CODE IN ('BILL_TO', 'SHIP_TO')
   THEN
      xxwc_cust_credit_limit_pkg.log_msg (
         1
        ,l_Module_Name
        ,' Site USe Code is : ' || :NEW.SITE_USE_CODE);

      FOR No_Sales_Credit_Rec IN No_Sales_Credit_Cur
      LOOP
         xxwc_cust_credit_limit_pkg.log_msg (
            1
           ,l_Module_Name
           ,   ' 101 : No Sales credit Salesrep ID : '
            || No_Sales_Credit_Rec.Salesrep_ID);
         xxwc_cust_credit_limit_pkg.log_msg (
            1
           ,l_Module_Name
           ,' 102 : Salesrep ID : ' || :NEW.PRIMARY_SALESREP_ID);

         IF (   (No_Sales_Credit_Rec.Salesrep_ID =
                    NVL (:NEW.PRIMARY_SALESREP_ID, -9999))
             OR (:NEW.PRIMARY_SALESREP_ID IS NULL))
         THEN
            xxwc_cust_credit_limit_pkg.log_msg (
               1
              ,l_Module_Name
              ,' 103 : Inside No SalesCredit Loop : ');
            xxwc_cust_credit_limit_pkg.log_msg (
               1
              ,l_Module_Name
              ,' 104 : Customer Account Site ID : ' || :NEW.CUST_ACCT_SITE_ID);

            FOR Salesrep_Rec IN Salesrep_Cur
            LOOP
               xxwc_cust_credit_limit_pkg.log_msg (
                  1
                 ,l_Module_Name
                 ,' 105 : Salesrep_ID : ' || Salesrep_Rec.Salesrep_ID);
               :New.PRIMARY_SALESREP_ID := Salesrep_Rec.Salesrep_ID;
            END LOOP;
         END IF;
      END LOOP;
   END IF;

   xxwc_cust_credit_limit_pkg.log_msg (
      1
     ,l_Module_Name
     ,'106 : ++++++++++++++++++++++++++++++++++');
   xxwc_cust_credit_limit_pkg.log_msg (1
                                      ,l_Module_Name
                                      ,'107 : l_itemkey =' || l_itemkey);
   xxwc_cust_credit_limit_pkg.log_msg (1
                                      ,l_Module_Name
                                      ,'108: call create_process');
   xxwc_cust_credit_limit_pkg.log_msg (1
                                      ,l_Module_Name
                                      ,'109: Created By ' || :NEW.CREATED_BY);
   xxwc_cust_credit_limit_pkg.log_msg (
      1
     ,l_Module_Name
     ,'110: Site Use Id ' || :NEW.SITE_USE_ID);
   xxwc_cust_credit_limit_pkg.log_msg (
      1
     ,l_Module_Name
     ,'111: CUST ACCT SITE ID ' || :NEW.CUST_ACCT_SITE_ID);
   xxwc_cust_credit_limit_pkg.log_msg (
      1
     ,l_Module_Name
     ,'112: Primary Flag ' || :NEW.PRIMARY_FLAG);

   --save_threshold := wf_engine.threshold;
   --wf_engine.threshold := -1;

   wf_engine.createprocess (l_itemtype
                           ,l_itemkey
                           ,'XXWC_CREATE_PROFILE_PROC');

   wf_engine.setitemattrtext (itemtype   => l_itemtype
                             ,itemkey    => l_itemkey
                             ,aname      => 'XXWC_SITE_USE_ID'
                             ,avalue     => :NEW.SITE_USE_ID);

   wf_engine.setitemattrtext (itemtype   => l_itemtype
                             ,itemkey    => l_itemkey
                             ,aname      => 'XXWC_CUST_ACCT_SITE_ID'
                             ,avalue     => :NEW.CUST_ACCT_SITE_ID);

   wf_engine.setitemattrtext (itemtype   => l_itemtype
                             ,itemkey    => l_itemkey
                             ,aname      => 'XXWC_USER_ID'
                             ,avalue     => :NEW.CREATED_BY);

   wf_engine.setitemattrtext (itemtype   => l_itemtype
                             ,itemkey    => l_itemkey
                             ,aname      => 'XXWC_PRIMARY_FLAG'
                             ,avalue     => :NEW.PRIMARY_FLAG);

   wf_engine.setitemattrtext (itemtype   => l_itemtype
                             ,itemkey    => l_itemkey
                             ,aname      => 'XXWC_CREATED_BY_MODULE'
                             ,avalue     => :NEW.CREATED_BY_MODULE);

   wf_engine.setitemattrtext (itemtype   => l_itemtype
                             ,itemkey    => l_itemkey
                             ,aname      => 'XXWC_SITE_USE_CODE'
                             ,avalue     => :NEW.SITE_USE_CODE);

   wf_engine.setitemattrtext (itemtype   => l_itemtype
                             ,itemkey    => l_itemkey
                             ,aname      => 'XXWC_ATTRIBUTE1'
                             ,avalue     => UPPER (:NEW.ATTRIBUTE1));

   -- Satish U: 25-MAY-2012 : Removed Hard Coded String
   wf_engine.setitemowner (itemtype   => l_itemtype
                          ,itemkey    => l_itemkey
                          ,owner      => l_User_Name);            --'VL002096'

   xxwc_cust_credit_limit_pkg.log_msg (1
                                      ,l_Module_Name
                                      ,'call start_process');
   wf_engine.startprocess (l_itemtype, l_itemkey);
--wf_engine.threshold := save_threshold;

--Added for Rev 1.1 Begin
xxwc_cust_credit_limit_pkg.submit_wf_process;
--Added for Rev 1.1 End

EXCEPTION
   WHEN OTHERS
   THEN
      xxwc_cust_credit_limit_pkg.log_msg (1
                                         ,l_Module_Name
                                         ,'error =' || SQLERRM);
      --IF wf_engine.threshold < 0
      --THEN
      --wf_engine.threshold := save_threshold;
      --END IF;
      NULL;
END;
/
