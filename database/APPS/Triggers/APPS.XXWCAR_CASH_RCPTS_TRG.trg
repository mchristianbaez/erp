CREATE OR REPLACE TRIGGER APPS.xxwcar_cash_rcpts_trg
   BEFORE INSERT
   ON xxwc.xxwcar_cash_rcpts_tbl
   REFERENCING NEW AS NEW OLD AS OLD
   FOR EACH ROW
DECLARE
   l_recpt_id   NUMBER := 0;
BEGIN
   IF :new.receipt_id IS NULL
   THEN
      SELECT apps.xxwcar_cash_rcpts_s.NEXTVAL INTO l_recpt_id FROM DUAL;

      :new.receipt_id := l_recpt_id;
   END IF;
END;
/
