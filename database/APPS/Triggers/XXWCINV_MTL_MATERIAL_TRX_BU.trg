CREATE OR REPLACE TRIGGER APPS.XXWCINV_MTL_MATERIAL_TRX_BU
/**************************************************************************************
      PURPOSE:    Freight Burden Extension Added for Hawaii - TMS Ticket 20130102-01229

      Logic:     1) Update the Transfer Cost and Transfer Percentage on MTL_MATERIAL_TRANSACTIONS for intransit shipments and intransit receipts for orgs that are enabled 
      
      REVISIONS:
      Ver        Date        Author           Description
      ---------  ----------  ---------------  ------------------------------------
      1.0        09-May-13   Lee Spitzer       1. Created this trigger.
      1.1        21-JUN-13   Lee Spitzer       2. Updated Trigger to exit the trigger if we don't find a record in the custom freight burden table
                                                  We don't want to take the org level default on transfers if there is not a specific transfer between orgs on the custom freight table
	  1.2        20-OCT-14   Pattabhi Avula    3. TMS# 20141001-00172  Multi org Changes done
      ******************************************************************************/
BEFORE INSERT OR UPDATE ON MTL_MATERIAL_TRANSACTIONS

REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
Declare

  l_trx_type_id                        NUMBER;
  l_transaction_type_name              VARCHAR2(30);
   
  l_po_freight_moh_rate     NUMBER;
  l_freight_basis           VARCHAR2(1);
  l_freight_per_lb          NUMBER;
  l_unit_weight             NUMBER;
  l_exception               EXCEPTION;
  
  l_moh_weight_cost         NUMBER;
  l_organization_id         NUMBER;
  l_source_organization_id  NUMBER;
  l_applicable              VARCHAR2(10);
  l_transfer_charge_yes_no  VARCHAR2(1);
   
BEGIN

  --Check to see if the profile option WC: Freight Costing Hook Enabled is set to Yes
   l_applicable := fnd_profile.VALUE ('XXWC_FREIGHT_COSTING_HOOK_ENABLED');

    IF l_applicable = 'Y' THEN

    --Find out of this is an intransit shipment or intransit receipt
      BEGIN
        -- determine if record originated as inter-org or intransit transfer
          SELECT transaction_type_id, transaction_type_name
          Into l_trx_type_id, l_transaction_type_name
          From MTL_TRANSACTION_TYPES MTT
          WHERE MTT.transaction_type_id = :NEW.transaction_type_id
          --AND MTT.transaction_type_name IN ('Int Order Intr Ship','Intransit Shipment','Int Req Intr Rcpt','Intransit Receipt');--Added 11/26/2012
          AND MTT.transaction_type_name IN ('Int Order Intr Ship','Int Req Intr Rcpt');--Added 11/26/2012
      EXCEPTION
          WHEN OTHERS THEN
              l_transaction_type_name := NULL;
              l_trx_type_id := NULL;
      END;
              
      --If this is shipment then set the transfer org as the shipping org and the receiving org as the organization to pull the transfer cost values
      IF l_transaction_type_name IN ('Int Order Intr Ship')  THEN--Added 11/26/2012
      
          l_source_organization_id := :NEW.organization_id;
          l_organization_id := :NEW.transfer_organization_id;
    
      ELSE
      
          l_source_organization_id := :NEW.transfer_organization_id;
          l_organization_id := :NEW.organization_id;
    
      END IF;
      
          
      IF l_trx_type_id is not null and l_source_organization_id is not null and l_organization_id is not null then 
    
    
         --Get the default org amount for PO Freight and resource_id
             BEGIN
                   SELECT nvl(ciod.usage_rate_or_amount,0),
                          nvl(br.attribute1,'C'),
                          nvl(ciod.attribute1,0),
                          nvl(br.attribute2,'N')
                   INTO   l_po_freight_moh_rate,
                          l_freight_basis,
                          l_freight_per_lb,
                          l_transfer_charge_yes_no
                   FROM   bom_resources br,
                          cst_item_overhead_defaults ciod
                   --WHERE  br.organization_id = i_org_id
                   WHERE  br.organization_id = l_organization_id
                   AND    br.cost_element_id = 2
                   AND    br.resource_code = 'PO Freight'
                   AND    br.organization_id = ciod.organization_id
                   and    br.resource_id = ciod.material_overhead_id;
             EXCEPTION
              WHEN others  THEN
                       l_po_freight_moh_rate := 0;
                       l_freight_basis := 'C';
                       l_freight_per_lb := 0;
                       RAISE l_exception;
             
             END;               
             
    
              BEGIN
                SELECT nvl(xpfbt.freight_duty,0),
                       --Added for Hawaii Freight Burden Extension 20130102-01229
                       nvl(xpfbt.freight_per_lb,0),
                       nvl(xpfbt.freight_basis,'C')
                INTO   l_po_freight_moh_rate,
                       l_freight_per_lb,
                       l_freight_basis
                FROM   apps.XXWC_PO_FREIGHT_BURDEN_TBL XPFBT
                --WHERE  XPFBT.organization_id = i_org_id
                WHERE  xpfbt.organization_id = l_organization_id
                AND    xpfbt.vendor_type = 'INTERNAL'
                AND    xpfbt.source_organization_id = l_source_organization_id;
            
            EXCEPTION
                WHEN others  THEN
                   --Removed on 6/21/2013 to version 1.1
                   /*    l_po_freight_moh_rate := l_po_freight_moh_rate; --get the rate from the org
                       l_freight_per_lb := l_freight_per_lb;
                       l_freight_basis := l_freight_basis; --get the org default basis;
                  */
                   RAISE l_exception;
            END;
        
        
             --Shipping Weight store on attribute 23 at the org level
             BEGIN
                  SELECT --Added for Hawaii Freight Burden Extension 20130102-01229
                         --Attribute23 is the Unit Weight Override or Unit Weight Per LB
                         nvl(msib.attribute23,nvl(unit_weight,0))
                  INTO   l_unit_weight 
                  FROM   mtl_system_items_b msib
                  --WHERE  msib.organization_id =i_org_id
                  WHERE  msib.organization_id = l_organization_id
                  AND    msib.inventory_item_id = :NEW.inventory_item_id; 
             EXCEPTION
                  WHEN others  THEN
                    l_unit_weight  := 0;
             END;
    
          
            l_moh_weight_cost := nvl(l_unit_weight,0) * nvl(l_freight_per_lb,0);
          
    
           IF l_freight_basis = 'W' AND l_transfer_charge_yes_no = 'Y' THEN
    
              :NEW.TRANSFER_PERCENTAGE :=   l_moh_weight_cost*100;
              :NEW.TRANSFER_COST := l_moh_weight_cost * ABS(NVL(:NEW.PRIMARY_QUANTITY,0));
              
            ELSIF l_freight_basis = 'C' AND l_transfer_charge_yes_no = 'Y' THEN
            
              :NEW.TRANSFER_PERCENTAGE := l_po_freight_moh_rate*100;
              :NEW.TRANSFER_COST := l_po_freight_moh_rate * ABS(NVL(:NEW.PRIMARY_QUANTITY,0));
        
            ELSE
            
                NULL;
            
            END IF;
    
      
      END IF;

  END IF;  

Exception
  WHEN l_exception THEN
      NULL;
  When OTHERS Then
    NULL;
END;
/
