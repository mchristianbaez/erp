
  CREATE OR REPLACE TRIGGER "APPS"."XXCUSWF_NOTIF_BEF_INS_TRG" 
  BEFORE INSERT OR UPDATE ON WF_NOTIFICATIONS
  REFERENCING NEW AS NEW OLD AS OLD
  FOR EACH ROW
begin
  IF INSERTING THEN
    IF (:new.message_type = 'APEXP' AND
       :new.message_name = 'XXCUS_WW_1_REQ_EXP_REP_APPRVL' AND
       :new.status = 'OPEN' AND :new.mail_status LIKE '%MAIL%') Then
      :new.mail_status := 'SENT';
    ELSIF :new.message_type = 'APEXP' AND
          :new.message_name = 'OIE_NO_MANAGER_RESPONSE' THEN
      :new.due_date := sysdate + 5;
    END IF;

  ELSIF UPDATING THEN
    IF :new.message_type = 'APEXP' AND
       :new.message_name = 'OIE_NO_MANAGER_RESPONSE' THEN
      :new.due_date := sysdate + 5;
    END IF;
  END IF;

END XXCUSWF_NOTIF_BEF_INS_TRG;



ALTER TRIGGER "APPS"."XXCUSWF_NOTIF_BEF_INS_TRG" ENABLE;
