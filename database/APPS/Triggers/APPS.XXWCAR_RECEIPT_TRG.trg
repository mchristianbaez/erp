CREATE OR REPLACE TRIGGER APPS.XXWCAR_RECEIPT_TRG
   AFTER INSERT
   ON ar.ar_cash_receipts_all
   REFERENCING NEW AS NEW OLD AS OLD
   FOR EACH ROW
 --  WHEN (NEW.attribute1 IS NOT NULL AND NEW.org_id = 162) -- Commented and replaced with the below condition by pattabhi on 10/15/2014 for TMS#  20141001-00172 
 when (NEW.attribute1 LIKE 'http://%')
DECLARE
   /**************************************************************************
     File Name: XXCUSAR_RECEIPT_TRG

     PROGRAM TYPE: SQL Script

     PURPOSE:      Attach Image url for receipt from Documentum for White Cap
                   receipts coming thru lockbox
     HISTORY
     =============================================================================
            Last Update Date : 12/12/2011
     =============================================================================
     =============================================================================
     VERSION DATE          AUTHOR(S)          DESCRIPTION
     ------- -----------   -----------------  ------------------------------------
     1.0     12-DEC-2011   Kathy Poling       Creation of trigger
     2.0     10/15/2014    Pattabhi Avula     TMS#  20141001-00172  Multi Org Changes for Canada OU
     =============================================================================
     *****************************************************************************/
   l_err_msg              VARCHAR2 (3000);
   l_sec                  VARCHAR2 (255);
   l_msg                  VARCHAR2 (150);
   l_entity               VARCHAR2 (40) := 'AR_CASH_RECEIPTS';
   v_category_id          NUMBER;
   v_doc_id               NUMBER (35);
   v_attach_doc_id        NUMBER (35);
   v_data_type_id         fnd_document_datatypes.datatype_id%TYPE;
   g_user_id              NUMBER := fnd_global.user_id;
   g_login_id             NUMBER := fnd_profile.VALUE ('LOGIN_ID');
   no_attachment_insert   EXCEPTION;

   l_err_callfrom         VARCHAR2 (75) DEFAULT 'XXWCAR_RECEIPTS_TRG';
   l_err_callpoint        VARCHAR2 (75) DEFAULT 'START';
   l_distro_list          VARCHAR2 (75)
                             DEFAULT 'HDSOracleDevelopers@hdsupply.com';
BEGIN
   l_msg :=
         'Starting the Image Link Processing for cash_receipt_id: '
      || :new.cash_receipt_id;

   l_err_callpoint :=
      'New receipt from DCTM lockbox creating a receipt in AR_CASH_RECEIPTS';

   BEGIN
      SELECT datatype_id
        INTO v_data_type_id
        FROM fnd_document_datatypes
       WHERE user_name = 'Web Page' AND datatype_id IS NOT NULL;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         l_msg := 'Datatype Id Not Found For - Web Page';

         RAISE no_attachment_insert;
      WHEN OTHERS
      THEN
         l_msg := 'Datatype Id Not Found For - Web_Page';

         RAISE no_attachment_insert;
   END;

   l_msg := 'v_data_type_id: ' || v_data_type_id;

   BEGIN
      SELECT category_id
        INTO v_category_id
        FROM fnd_document_categories_tl
       WHERE user_name = 'Miscellaneous' AND category_id IS NOT NULL;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         l_msg := 'Category Id Not Found For - Miscellaneous';

         RAISE no_attachment_insert;
      WHEN OTHERS
      THEN
         l_msg := 'Category Id Not Found For - Miscellaneous';

         RAISE no_attachment_insert;
   END;

   l_msg := 'v_category_id: ' || v_category_id;

   v_doc_id := NULL;
   v_attach_doc_id := NULL;

   SELECT fnd_documents_s.NEXTVAL INTO v_doc_id FROM DUAL;

   SELECT fnd_attached_documents_s.NEXTVAL INTO v_attach_doc_id FROM DUAL;

   INSERT INTO fnd_documents (document_id
                             ,last_update_date
                             ,last_updated_by
                             ,creation_date
                             ,created_by
                             ,last_update_login
                             ,datatype_id
                             ,category_id
                             ,security_type
                             ,security_id
                             ,publish_flag
                             ,usage_type
                             ,url)
        VALUES (v_doc_id
               ,SYSDATE
               ,:new.last_updated_by
               ,SYSDATE
               ,:new.created_by
               ,:new.last_update_login
               ,v_data_type_id
               ,v_category_id
               ,'2'
               ,'1'
               ,'Y'
               ,'O'
               ,:new.attribute1);

   l_msg := 'INSERT INTO FND_DOCUMENTS';

   INSERT INTO fnd_documents_tl (document_id
                                ,last_update_date
                                ,last_updated_by
                                ,creation_date
                                ,created_by
                                ,last_update_login
                                ,LANGUAGE
                                ,file_name
                                ,source_lang
                                ,description)
        VALUES (v_doc_id
               ,SYSDATE
               ,:new.last_updated_by
               ,SYSDATE
               ,:new.created_by
               ,:new.last_update_login
               ,'US'
               ,:new.attribute1
               ,'US'
               ,'Image Link');

   l_msg := 'INSERT INTO FND_DOCUMENTS_TL';

   INSERT INTO fnd_attached_documents (attached_document_id
                                      ,document_id
                                      ,last_update_date
                                      ,last_updated_by
                                      ,creation_date
                                      ,created_by
                                      ,last_update_login
                                      ,seq_num
                                      ,entity_name
                                      ,automatically_added_flag
                                      ,pk1_value)
        VALUES (v_attach_doc_id
               ,v_doc_id
               ,SYSDATE
               ,:new.last_updated_by
               ,SYSDATE
               ,:new.created_by
               ,:new.last_update_login
               ,'10'
               ,l_entity
               ,'N'
               ,:new.cash_receipt_id);

   l_msg :=
         'Document Inserted for Oracle cash_receipt_id: '
      || :new.cash_receipt_id;
EXCEPTION
   WHEN no_attachment_insert
   THEN
      ROLLBACK;

      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api (
         p_called_from         => l_err_callfrom
        ,p_calling             => l_err_callpoint
        --,p_request_id        => l_req_id
        ,p_ora_error_msg       => SQLERRM
        ,p_error_desc          => 'Error during insert url link on ar cash receipt'
        ,p_distribution_list   => l_distro_list
        ,p_module              => 'AR');
   WHEN OTHERS
   THEN
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api (
         p_called_from         => l_err_callfrom
        ,p_calling             => l_err_callpoint
        --,p_request_id        => l_req_id
        ,p_ora_error_msg       => SQLERRM
        ,p_error_desc          => 'Error during procedure import_inv'
        ,p_distribution_list   => l_distro_list
        ,p_module              => 'AR');
END xxcusap_inv_interface_trg;
/
