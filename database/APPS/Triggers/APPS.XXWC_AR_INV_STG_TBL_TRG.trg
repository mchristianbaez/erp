CREATE OR REPLACE TRIGGER APPS."XXWC_AR_INV_STG_TBL_TRG"
   BEFORE INSERT
   ON XXWC.XXWC_AR_INV_STG_TBL
   REFERENCING NEW AS NEW OLD AS OLD
   FOR EACH ROW
DECLARE
   /**************************************************************************
   File Name: XXWC_AR_INV_STG_TBL_TRG

   PROGRAM TYPE: SQL Script

   PURPOSE:

   HISTORY
   =============================================================================
          Last Update Date : 05/15/2012
   =============================================================================
   =============================================================================
   VERSION DATE          AUTHOR(S)          DESCRIPTION
   ------- -----------   -----------------  ------------------------------------
   1.0     14-May-2012   Gopi Damuluri      Creation of trigger

   =============================================================================
   *****************************************************************************/
   l_err_msg         VARCHAR2 (3000);
   l_sec             VARCHAR2 (255);
   l_msg             VARCHAR2 (150);

   l_err_callfrom    VARCHAR2 (75) DEFAULT 'XXWC_AR_INV_STG_TBL_TRG';
   l_err_callpoint   VARCHAR2 (75) DEFAULT 'START';
   l_distro_list     VARCHAR2 (75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
BEGIN
   ---------------------------------------------------------------------------------------------
   -- Update XXWC_AR_INV_STG_TBL.INTF_STG_ID using the sequence XXWC_AR_INV_STG_TBL_SEQ
   ---------------------------------------------------------------------------------------------
   --  IF (:new.intf_stg_id = '') AND (:NEW.status = 'NEW') THEN
   :NEW.intf_stg_id := XXWC_AR_INV_STG_TBL_SEQ.NEXTVAL;
--  END IF;

EXCEPTION
   WHEN OTHERS
   THEN
      l_msg := 'Failure in Trigger XXWC_AR_INV_STG_TBL_TRG: ' || SQLERRM;
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api (
         p_called_from         => l_err_callfrom
        ,p_calling             => l_err_callpoint
        --,p_request_id        => l_req_id
        ,p_ora_error_msg       => SQLERRM
        ,p_error_desc          => l_msg
        ,p_argument1           => :new.trx_number
        ,p_distribution_list   => l_distro_list
        ,p_module              => 'AR');
END XXWC_AR_INV_STG_TBL_TRG;
/
