
  CREATE OR REPLACE TRIGGER "APPS"."XXCUSCE_ECLIPSE_DEPOSIT_TRG" 
  BEFORE INSERT ON xxcus.xxcusce_eclipse_deposit_tbl
  REFERENCING NEW AS NEW OLD AS OLD
  FOR EACH ROW
DECLARE
  l_trx_id NUMBER := 0;

BEGIN
  IF :new.trx_id IS NULL
  THEN
    SELECT apps.xxcusce_interface_s.nextval INTO l_trx_id FROM dual;
    :new.trx_id := l_trx_id;

  END IF;
END;

ALTER TRIGGER "APPS"."XXCUSCE_ECLIPSE_DEPOSIT_TRG" ENABLE;
