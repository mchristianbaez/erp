CREATE OR REPLACE TRIGGER XXWC.XXWC_B2B_CUST_INFO_TBL_T1
   BEFORE INSERT
   ON XXWC.XXWC_B2B_CUST_INFO_TBL
   REFERENCING NEW AS New OLD AS Old
   FOR EACH ROW
DECLARE
   l_party_id   NUMBER DEFAULT NULL;
   l_rec_count  NUMBER := 0;
/******************************************************************************
   NAME:       XXWC_B2B_CUST_INFO_TBL_T1
   PURPOSE : Trigger to populate B2B Customer Information table.

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        04-Apr-2015 Gopi Damuluri    TMS# 20150302-00006
                                           Trigger used to populate PARTY_ID value to XXWC.XXWC_B2B_CUST_INFO_TBL
   1.1        08-Jan-2016  Gopi Damuluri   TMS# 20160204-00112 - B2B POD Enhancement
******************************************************************************/
BEGIN
   SELECT party_id
     INTO l_party_id
     FROM apps.hz_parties
    WHERE party_number = NVL(:OLD.party_number, :NEW.party_number)
      AND status       = 'A'; --  Version# 1.1

   :NEW.PARTY_ID := l_party_id;

EXCEPTION
   WHEN OTHERS THEN
      -- Consider logging the error and then re-raise
      RAISE;
END XXWC_B2B_CUST_INFO_TBL_T1;
/