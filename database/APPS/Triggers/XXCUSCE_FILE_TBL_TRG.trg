
  CREATE OR REPLACE TRIGGER "APPS"."XXCUSCE_FILE_TBL_TRG" 
  BEFORE INSERT ON xxcus.xxcusce_file_trg_tbl
  REFERENCING NEW AS NEW OLD AS OLD
  FOR EACH ROW
DECLARE
  l_stg_id NUMBER := 0;

BEGIN
  IF :new.stg_id IS NULL
  THEN
    SELECT apps.xxcusce_file_s.nextval INTO l_stg_id FROM dual;
    :new.stg_id        := l_stg_id;
    :new.creation_time := SYSDATE;
    :new.status        := 'PENDING';

  END IF;
END;

ALTER TRIGGER "APPS"."XXCUSCE_FILE_TBL_TRG" ENABLE;
