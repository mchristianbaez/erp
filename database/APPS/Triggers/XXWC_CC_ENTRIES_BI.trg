CREATE OR REPLACE TRIGGER APPS.XXWC_CC_ENTRIES_BI
 /******************************************************************************
      NAME:       XXXWC_CC_ENTRIES_BI

      PURPOSE:    To insert records into XXWC_CC_QUANTITIES_DETAIL_TBL when a new record is inserted into INV.MTL_CYCLE_COUNT_ENTRIES

      Logic:     1) When a new record is inserted into INV.MTL_CYCLE_COUNT_ENTRIES get the snap shot of the item's onhnad inventory  
      
      REVISIONS:
      Ver        Date        Author           Description
      ---------  ----------  ---------------  ------------------------------------
      1.0        20-JUN-13   Lee Spitzer       1. Created this trigger.  TMS Ticket - 20130214-01096 Implement Physical Inventory enhancements 
      1.1        09-SEP-13   Lee Spitzer       1.1 Updated tigger to include current item cost at the time of the snapshot TMS Ticket # 20130809-01349        
   ******************************************************************************/
BEFORE INSERT
   ON inv.mtl_cycle_count_entries
   REFERENCING NEW AS NEW OLD AS OLD
   FOR EACH ROW
WHEN ((NEW.ENTRY_STATUS_CODE = 1) OR --Through Cycle Count Schedule Request
       (NEW.ENTRY_STATUS_CODE = 3)) --Manual Request
DECLARE
    v_exception        EXCEPTION;
    g_user_id          NUMBER := fnd_global.user_id;
    g_login_id         NUMBER := fnd_profile.value('LOGIN_ID');
    g_err_callfrom     VARCHAR2(75) DEFAULT 'XXWC_CC_ENTRIES_BI';
    g_err_callpoint    VARCHAR2(75) DEFAULT 'START';
    g_distro_list      VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
    l_err_msg          VARCHAR2(2000);
    l_message          VARCHAR2(2000);
    l_date             DATE := SYSDATE;
    
    l_item_cost        NUMBER; --added 9/9/2013 TMS Ticket 20130809-01349  
    l_cost_type_id     NUMBER; --added 9/9/2013 TMS Ticket 20130809-01349 
    
BEGIN

  --Added 9/9/2013 to get the current item's average cost TMS Ticket 20130809-01349 
    
   BEGIN
    
      SELECT primary_cost_method
      INTO   l_cost_type_id
      FROM   mtl_parameters
      where  organization_id = :NEW.organization_id;
      
   EXCEPTION
      WHEN OTHERS THEN
            l_cost_type_id := 2; --Average Cost Type if we can't get it from the organization parameters
   END;
    
    
   --Added 9/9/2013 to get the current item's average cost TMS Ticket 20130809-01349 
    
    BEGIN
    
      SELECT  nvl(item_cost,0)
      INTO    l_item_cost
      FROM    cst_item_costs
      WHERE   organization_id   = :NEW.organization_id
      AND     inventory_item_id = :NEW.inventory_item_id
      AND     cost_type_id =  l_cost_type_id;
      
    EXCEPTION
      
      WHEN OTHERS THEN
              l_item_cost := 0;
   END;
   

  BEGIN    
      INSERT INTO XXWC.XXWC_CC_QUANTITIES_DETAIL_TBL
      (INSERT_DATE
      ,CYCLE_COUNT_ENTRY_ID 
      ,CYCLE_COUNT_HEADER_ID
      ,COUNT_LIST_SEQUENCE 
      ,STATUS_ID
      ,INVENTORY_ITEM_ID
      ,ORGANIZATION_ID
      ,DATE_RECEIVED
      ,LAST_UPDATE_DATE
      ,LAST_UPDATED_BY
      ,CREATION_DATE
      ,CREATED_BY
      ,LAST_UPDATE_LOGIN
      ,PRIMARY_TRANSACTION_QUANTITY
      ,SUBINVENTORY_CODE
      ,REVISION
      ,LOCATOR_ID
      ,CREATE_TRANSACTION_ID
      ,UPDATE_TRANSACTION_ID
      ,LOT_NUMBER
      ,ORIG_DATE_RECEIVED
      ,COST_GROUP_ID
      ,CONTAINERIZED_FLAG
      ,PROJECT_ID
      ,TASK_ID
      ,ONHAND_QUANTITIES_ID
      ,ORGANIZATION_TYPE
      ,OWNING_ORGANIZATION_ID
      ,OWNING_TP_TYPE
      ,PLANNING_ORGANIZATION_ID
      ,PLANNING_TP_TYPE
      ,TRANSACTION_UOM_CODE
      ,TRANSACTION_QUANTITY
      ,SECONDARY_UOM_CODE
      ,SECONDARY_TRANSACTION_QUANTITY
      ,IS_CONSIGNED
      ,LPN_ID
      ,ITEM_COST) --added item_cost 9/9/2013
      SELECT 
       L_DATE
      ,:NEW.CYCLE_COUNT_ENTRY_ID
      ,:NEW.CYCLE_COUNT_HEADER_ID
      ,:NEW.COUNT_LIST_SEQUENCE
      ,STATUS_ID
      ,INVENTORY_ITEM_ID
      ,ORGANIZATION_ID
      ,DATE_RECEIVED
      ,LAST_UPDATE_DATE
      ,LAST_UPDATED_BY
      ,CREATION_DATE
      ,CREATED_BY
      ,LAST_UPDATE_LOGIN
      ,PRIMARY_TRANSACTION_QUANTITY
      ,SUBINVENTORY_CODE
      ,REVISION
      ,LOCATOR_ID
      ,CREATE_TRANSACTION_ID
      ,UPDATE_TRANSACTION_ID
      ,LOT_NUMBER
      ,ORIG_DATE_RECEIVED
      ,COST_GROUP_ID
      ,CONTAINERIZED_FLAG
      ,PROJECT_ID
      ,TASK_ID
      ,ONHAND_QUANTITIES_ID
      ,ORGANIZATION_TYPE
      ,OWNING_ORGANIZATION_ID
      ,OWNING_TP_TYPE
      ,PLANNING_ORGANIZATION_ID
      ,PLANNING_TP_TYPE
      ,TRANSACTION_UOM_CODE
      ,TRANSACTION_QUANTITY
      ,SECONDARY_UOM_CODE
      ,SECONDARY_TRANSACTION_QUANTITY
      ,IS_CONSIGNED
      ,LPN_ID
      ,l_item_cost --Added 9/9/2013
      FROM MTL_ONHAND_QUANTITIES_DETAIL
      WHERE INVENTORY_ITEM_ID = :NEW.INVENTORY_ITEM_ID
      AND   ORGANIZATION_ID = :NEW.ORGANIZATION_ID
      AND   SUBINVENTORY_CODE = :NEW.SUBINVENTORY
      AND   nvl(LOT_NUMBER,'-99999') = nvl(:NEW.LOT_NUMBER,'-99999');
          
    END;


EXCEPTION

when others then
      
        l_message := 'Encountered when others exception on trigger';
        l_err_msg := SQLCODE || SQLERRM;
      
       xxcus_error_pkg.xxcus_error_main_api(p_called_from      => g_err_callfrom
                                          ,p_calling           => g_err_callpoint
                                          ,p_ora_error_msg     => l_err_msg
                                          ,p_error_desc        => l_message
                                          ,p_distribution_list => g_distro_list
                                          ,p_module            => 'INV');    
    
END;