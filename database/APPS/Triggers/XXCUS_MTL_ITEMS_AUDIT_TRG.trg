
  CREATE OR REPLACE TRIGGER "APPS"."XXCUS_MTL_ITEMS_AUDIT_TRG" 
          before update of attribute22 on inv.mtl_system_items_b
          for each row
  --ESMS ticket 209344
  --Author: Balaguru Seshadri
  --Date: 20-JUN-2013
  --Scope: Audit product avp code for the purpose of taxware exemptions.
  --When:We audit the records only when the taxware code [attribute22] is updated at the Master Inventory Org [MST] OR ID =222          
  WHEN (new.organization_id =222) declare 
 --         
 v_host          varchar2(2000);
 v_ip_address    varchar2(2000);
 v_terminal      varchar2(2000);
 v_sessionid     varchar2(2000);
 v_session_user  varchar2(2000);
 v_sid           varchar2(2000);
 v_os_user       varchar2(2000);
 --
 begin
    select sys_context ('USERENV', 'HOST')
    into   v_host
    from   dual;
    select sys_context ('USERENV', 'IP_ADDRESS')
    into   v_ip_address
    from   dual; 
    select sys_context ('USERENV', 'TERMINAL')
    into   v_terminal
    from   dual;
    select sys_context ('USERENV', 'SESSIONID')
    into   v_sessionid
    from   dual;
    select sys_context ('USERENV', 'SESSION_USER')
    into   v_session_user
    from   dual;
    select sys_context ('USERENV', 'SID')
    into   v_sid
    from   dual;
    select sys_context('USERENV', 'OS_USER')
    into   v_os_user
    from   dual;    
    insert into xxcus.xxcus_mtl_items_audit_b 
     (
       updated_rowid
      ,organization_id
      ,item_id
      ,item_number
      ,old_taxware_code
      ,new_taxware_code
      ,os_user
      ,host
      ,ip_address
      ,terminal
      ,sessionid
      ,session_user
      ,sid      
      ,created_by
      ,creation_date
      ,last_updated_by
      ,last_update_date           
     )
    values
     (
       :new.rowid
      ,:new.organization_id
      ,:new.inventory_item_id
      ,:new.segment1
      ,:old.attribute22
      ,:new.attribute22
      ,v_os_user
      ,v_host
      ,v_ip_address
      ,v_terminal
      ,v_sessionid
      ,v_session_user
      ,v_sid      
      ,:new.created_by
      ,sysdate
      ,:new.last_updated_by
      ,sysdate              
     ); 
 --                          
 exception
  when others then
   Null;
 end;
ALTER TRIGGER "APPS"."XXCUS_MTL_ITEMS_AUDIT_TRG" ENABLE;
