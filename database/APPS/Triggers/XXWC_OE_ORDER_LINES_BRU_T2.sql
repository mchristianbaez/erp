CREATE OR REPLACE TRIGGER APPS.XXWC_OE_ORDER_LINES_BRU_T2
BEFORE UPDATE ON OE_ORDER_LINES_ALL
FOR EACH ROW
  WHEN (
     NEW.ACTUAL_SHIPMENT_DATE IS NOT NULL  
      )
DECLARE
   /***********************************************************************************************************
     $Header XXWC_OE_ORDER_LINES_BRU_T2 $
     Module Name: XXWC_OE_ORDER_LINES_BRU_T2.sql

     PURPOSE:   This trigger resets the Out For Delivery DFF Attriibute16  when Sales Order Lines are Triggered 

     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.1        01/23/2013  Satish Upadhyayula       Added code to Update Out For Delivery DFF Attribute value when SO line is Shipped 
                                                     TMS # 20121217-00542 : Out For Delivery : 
   ************************************************************************************************************/

 
BEGIN

    -- Satihs U: 23-JAN-2013 : Added Following Code  to Update Out_For_Delivery when Sales Order Line When SO Line is Shipped 
  If :New.ACTUAL_SHIPMENT_DATE IS NOT NULL AND :NEW.ATTRIBUTE16 IN ( 'Y', 'N') Then 
     -- Check Released Status of Corresponding Delivery Lien 
     :NEW.ATTRIBUTE16 := 'D';
  End If; 
Exception 
   When Others Then 
      :New.ATTRIBUTE16 := 'D'; 
END;
/
