CREATE OR REPLACE TRIGGER "APPS"."XXCUSAP_PNC_TRG"
AFTER INSERT
ON XXCUS.XXCUSAP_PNC_TBL
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
when (NVL(NEW.File_name, '*') != '*')
DECLARE
  --PRAGMA autonomous_transaction;
  /**********************************************************************************************
   File Name: APPS.XXCUS_AP_PNC_TRG
  
   PROGRAM TYPE: TRIGGER
  
   PURPOSE: Triiger to submit the Visa Load Concurrent Program
  
   HISTORY
   =============================================================================
          Last Update Date : 05/25/2011
   =============================================================================
   =============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- ----------------------------------------
   1.0     25-MAY-2011   Kathy Poling     Created this package taken from R11.
   1.1     25-APR-2012   Luong Vu         Add notification to go to both HDSFASTAP
                                          and Oracle Developer group.
                                          Added additional set responsibility if the first
                                          attempt failed.
   1.2     05/21/2013    Kathy Poling     SR 204912 added the submit XXCUSIE_PNC_NOTIFICATION
                                          adding the parm file name 
   1.3     10/22/2014    Kathy Poling     RFC 42470 S272756 Changes for Canada iExpense PNC program.  
                                          Removed the hard coding and put into a lookup
   1.4     12/18/2014    Kathy Poling     RFC 42600 S273466 added file name to error handling                                                                                                                         
   NOTE: XXCUS_IEXPENSE_GL_PKG.AP_PNC_TRG_MONITOR is monitoring the submit stage table and send
   email to group  based on  processed flag = 'N' and file name
  ***********************************************************************************************/

  l_card_program_id ap_card_programs_all.card_program_id%TYPE;
  l_name            VARCHAR2(10);
  l_card_name       VARCHAR2(100);
  l_count           NUMBER := 1;
  l_card_prgm       VARCHAR2(100);  --V 1.3
  l_user            VARCHAR2(100);  --V 1.3
  l_responsibility  VARCHAR2(100);  --V 1.3

  --Submission variables
  l_req_id             NUMBER := 0;
  l_globalset          VARCHAR2(100);
  l_can_submit_request BOOLEAN := TRUE;
  l_sec                VARCHAR2(150);
  l_err_msg            CLOB;  --V 1.3

  -- Error DEBUG
  l_err_callfrom  VARCHAR2(75) DEFAULT 'XXCUSAP_PNC_TRG';
  l_err_callpoint VARCHAR2(110) DEFAULT 'START';
  l_hdsfastap     VARCHAR2(75) DEFAULT 'hds-HDSFASTap-u1@hdsupply.com';
  l_distro_list   VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';

BEGIN

  SELECT NAME INTO l_name FROM v$database;
--V 1.3    10/22/14 
BEGIN
  SELECT /*+ RESULT_CACHE */
   meaning, attribute1, attribute2
    INTO l_card_prgm, l_user, l_responsibility
    FROM fnd_lookup_values
   WHERE 1 = 1
     AND lookup_type = 'XXCUS PNC CREDIT CARD PROGRAMS'
     AND lookup_code = substr(:new.file_name, 7, 5)
     AND enabled_flag = 'Y'
     AND SYSDATE BETWEEN start_date_active AND
         nvl(end_date_active, SYSDATE + 1);
EXCEPTION
   WHEN no_data_found THEN
     l_card_prgm       :=NULL;
     l_user            :=NULL;
     l_responsibility  :=NULL;              
END;
--

  l_err_callpoint := 'SET RESPONSIBLITY';
  -- For some reasons, this step fail intermittently.  
  -- The workaround is to do it twice if it fail the first time.  It seems to work.
  l_can_submit_request := xxcus_misc_pkg.set_responsibility(l_user   --'HDSAPINTERFACE'   --V 1.3
                                                           ,l_responsibility
                                                           --'HDS Payables Manager (No Suppliers) - US GSC'  --V 1.3
                                                           );

  IF l_can_submit_request
  THEN
    l_sec:= 'Global Variable are set.';
  ELSE
    l_can_submit_request := xxcus_misc_pkg.set_responsibility(l_user   --'HDSAPINTERFACE'   --V 1.3
                                                           ,l_responsibility
                                                           --'HDS Payables Manager (No Suppliers) - US GSC'  --V 1.3
                                                           );
    IF l_can_submit_request
    THEN
      l_sec := 'Global Variable are set.';
    ELSE
      --l_globalset := 'Global Variable are not set.';  --V 1.3
      l_err_msg       := 'Global Variable are not set for the Responsibility of '|| l_responsibility || ' and the User of '|| l_user;
      RAISE program_error;
    END IF;
  END IF;

  l_err_callpoint := 'Select card_program_id';

  IF (l_can_submit_request)
  THEN
  
  --V 1.3    10/22/2014
  BEGIN
    SELECT card_program_id, card_program_name
      INTO l_card_program_id, l_card_name
      FROM ap_card_programs_all
     WHERE card_program_name = l_card_prgm;
  EXCEPTION
    WHEN no_data_found THEN
      l_err_msg := 'Lookup mapping for program not found for ' ||
                   l_card_prgm;
      RAISE program_error;
  END;
   
  IF  l_card_program_id is not null then            
   --             
 /*  V 1.3
    IF substr(:new.file_name, 7, 5) = '60005'
    THEN
      SELECT card_program_id, card_program_name
        INTO v_card_program_id, l_card_name
        FROM ap_card_programs_all
       WHERE card_program_name = 'WC PNC Inventory Card Program';
    ELSIF substr(:new.file_name, 7, 5) = '60007'
    THEN
      SELECT card_program_id, card_program_name
        INTO v_card_program_id, l_card_name
        FROM ap_card_programs_all
       WHERE card_program_name = 'CB PNC Employee-Pay Card Program';
    ELSIF substr(:new.file_name, 7, 5) = '64007'
    THEN
      SELECT card_program_id, card_program_name
        INTO v_card_program_id, l_card_name
        FROM ap_card_programs_all
       WHERE card_program_name = 'FM PNC Employee-Pay Card Program';
    ELSIF substr(:new.file_name, 7, 5) = '60047'
    THEN
      SELECT card_program_id, card_program_name
        INTO v_card_program_id, l_card_name
        FROM ap_card_programs_all
       WHERE card_program_name = 'WW PNC Employee-Pay Card Program';
    ELSE
      SELECT card_program_id, card_program_name
        INTO v_card_program_id, l_card_name
        FROM ap_card_programs_all
       WHERE card_program_name = 'HDS Company Pay Program';
    END IF;
  */
    l_err_callpoint := 'Submit request APXVVCF4';
  
    IF (fnd_request.set_mode(TRUE))
    THEN
      l_req_id := fnd_request.submit_request('SQLAP'
                                            ,'APXVVCF4'
                                            ,NULL
                                            ,SYSDATE
                                            ,FALSE
                                            ,l_card_program_id
                                            ,:new.directory_name ||
                                             :new.file_name
                                            ,chr(0));
      
    ELSE
      RAISE program_error;
    END IF;
  
    IF l_req_id < 1
    THEN
      RAISE program_error;
    END IF;
  
    BEGIN
      l_err_callpoint := 'Inserting table xxcusap_pnc_submit_tbl';
      
      INSERT INTO xxcus.xxcusap_pnc_submit_tbl
      VALUES
        (xxcusap_pnc_s.nextval
        ,:new.directory_name
        ,:new.file_name
        ,substr(:new.file_name, 7, 5)
        ,l_card_name
        ,SYSDATE
        ,l_req_id
        ,l_user --'HDSAPINTERFACE'    V 1.3
        ,l_responsibility --'HDS Payables Manager (No Suppliers) - US GSC'  V 1.3
        ,'N'
        ,NULL --v1.2
        ,NULL); --v1.2
      
    
    EXCEPTION
      WHEN OTHERS THEN
        xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                            ,p_calling           => l_err_callpoint
                                            ,p_request_id        => l_req_id
                                            ,p_ora_error_msg     => 'Prgm NM '||l_card_name||' File NM '||:new.file_name||' - '||SQLERRM
                                            ,p_error_desc        => 'Error insert into xxcus_PNC_SUBMIT_STG table.'
                                            ,p_distribution_list => l_distro_list
                                            ,p_module            => 'AP');
      
        xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                            ,p_calling           => l_err_callpoint
                                            ,p_request_id        => l_req_id
                                            ,p_ora_error_msg     => 'Prgm NM '||l_card_name||' File NM '||:new.file_name||' - '||SQLERRM
                                            ,p_error_desc        => 'Error insert into xxcus_PNC_SUBMIT_STG table.'
                                            ,p_distribution_list => l_hdsfastap
                                            ,p_module            => 'AP');
    END;
  
    --v1.2  5/17/2013
    l_err_callpoint := 'Submit request XXCUSIE_PNC_NOTIFICATION';
  
    IF (fnd_request.set_mode(TRUE))
    THEN
      l_req_id := fnd_request.submit_request('XXCUS'
                                            ,'XXCUSIE_PNC_NOTIFICATION'
                                            ,NULL
                                            ,SYSDATE
                                            ,FALSE
                                            ,:new.file_name);
      
    ELSE
      RAISE program_error;
    END IF;
  
    IF l_req_id < 1
    THEN
      RAISE program_error;
    END IF;
    --
  end if;
  END IF;

EXCEPTION
  WHEN program_error THEN
    l_err_msg := l_err_msg|| l_err_callpoint || l_sec 
                  || ' File name = '||:new.file_name  --Version 1.4 added file name
                  || ' ...Error_Stack...' ||
                   dbms_utility.format_error_stack() ||
                   ' Error_Backtrace...' ||
                   dbms_utility.format_error_backtrace();
                   
    xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                        ,p_calling           => l_err_callpoint
                                        ,p_request_id        => l_req_id
                                        ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                        ,p_error_desc        => 'Error running PNC load with program_error exception'
                                        ,p_distribution_list => l_distro_list
                                        ,p_module            => 'AP');
  
    xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                        ,p_calling           => l_err_callpoint
                                        ,p_request_id        => l_req_id
                                        ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                        ,p_error_desc        => 'Error running PNC load with program_error exception'
                                        ,p_distribution_list => l_hdsfastap
                                        ,p_module            => 'AP');
  
  WHEN OTHERS THEN
    l_err_msg := l_err_msg || l_sec || ' File name = '||:new.file_name  --Version 1.4 added file name
                   || ' ...Error_Stack...' ||
                   dbms_utility.format_error_stack() ||
                   ' Error_Backtrace...' ||
                   dbms_utility.format_error_backtrace();
  
    xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                        ,p_calling           => l_err_callpoint
                                        ,p_request_id        => l_req_id
                                        ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                        ,p_error_desc        => 'Error running PNC load with OTHERS Exception'
                                        ,p_distribution_list => l_distro_list
                                        ,p_module            => 'AP');
  
    xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                        ,p_calling           => l_err_callpoint
                                        ,p_request_id        => l_req_id
                                        ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                        ,p_error_desc        => 'Error running PNC load with OTHERS Exception'
                                        ,p_distribution_list => l_hdsfastap
                                        ,p_module            => 'AP');
  
END xxcusap_pnc_trg;
/
