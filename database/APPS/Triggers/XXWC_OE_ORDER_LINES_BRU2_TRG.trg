CREATE OR REPLACE TRIGGER "APPS"."XXWC_OE_ORDER_LINES_BRU2_TRG"
  AFTER UPDATE ON "ONT"."OE_ORDER_LINES_ALL"
  REFERENCING NEW AS NEW OLD AS OLD
  FOR EACH ROW  
-- Version# 1.1 > Start  
when (old.shipping_method_code NOT IN ('000001_Our Truck_P_LTL', '000001_Our Truck_P_GND', '000001_WCD_P_LTL', '000001_BRANCH_LOO_P_GND', '000001_DC_LOOP_TR_P_GND') AND
       old.booked_flag = 'Y' AND
       new.shipping_method_code IN ('000001_Our Truck_P_LTL', '000001_Our Truck_P_GND', '000001_WCD_P_LTL', '000001_BRANCH_LOO_P_GND', '000001_DC_LOOP_TR_P_GND') AND
       new.open_flag = 'Y')
-- Version# 1.1 < End
--when (old.shipping_method_code != '000001_Our Truck_P_LTL' AND
--       old.booked_flag = 'Y' AND
--       new.shipping_method_code = '000001_Our Truck_P_LTL' AND
--       new.open_flag = 'Y')                                               
DECLARE
  /******************************************************************************
   NAME:     XXWC_OE_ORDER_LINES_BRU2_TRG

   PURPOSE:  To capture the order lines that get shipping method updated to Our Truck
             for that delivery so it gets sent to MyLogistics.


   REVISIONS:
   Ver        Date        Author           Description
   ---------  -----------  ---------------  ------------------------------------
   1.0        11-Aug-2014  Kathy Poling     Created this trigger.
                                            TMS 20140606-00082
   1.1        02-Feb-2015  Gopi Damuluri    TMS# 20150128-00043 XXWC DMS File creation modification                                            
   1.2        12-Feb-2015  Gopi Damuluri    TMS# 20150216-00257 Added XXWC_WSH_SHIPPING_STG_N1 Index Hint
  ******************************************************************************/

  -- Error DEBUG
  l_err_callfrom  VARCHAR2(110) DEFAULT 'XXWC_OE_ORDER_LINES_BRU2_TRG';
  l_err_callpoint VARCHAR2(110) DEFAULT 'START';
  l_distro_list   VARCHAR2(240) DEFAULT 'hdsoracledevelopers@hdsupply.com';
  
  l_delivery_id   NUMBER;
  l_last_update_date DATE := sysdate;

BEGIN

  l_err_callpoint := 'Insert record xxwc_om_dms_change_tbl for line_id being updated: ' ||
                     :old.line_id;
  BEGIN
    SELECT /*+ index(stg XXWC.XXWC_WSH_SHIPPING_STG_N1) */ delivery_id -- Version# 1.2 Added hint
      INTO l_delivery_id
      FROM xxwc.xxwc_wsh_shipping_stg stg
     WHERE header_id = :new.header_id
       AND line_id = :new.line_id;
  EXCEPTION
    WHEN OTHERS THEN
      l_delivery_id := -1;
  END;

  IF l_delivery_id != -1
  THEN

  BEGIN
    INSERT INTO xxwc.xxwc_om_dms_change_tbl
      (header_id
      ,line_id
      ,delivery_id
      ,created_by
      ,creation_date
      ,last_updated_by
      ,last_update_date
      ,change_type
      ,org_id)
    VALUES
      (:new.header_id
      ,:new.line_id
      ,l_delivery_id
      ,:old.created_by
      ,:old.creation_date
      ,nvl(fnd_global.user_id(), :new.last_updated_by)
      ,l_last_update_date
      ,'U'
      ,:new.org_id);
   EXCEPTION
   WHEN OTHERS THEN
     NULL;
   END;
      
   BEGIN
      --delete from on demand staging if exists     
      DELETE FROM xxwc_om_dms_brnch_file_tbl
      where header_id = :new.header_id
      and delivery_id = l_delivery_id
      and creation_date < l_last_update_date;
   EXCEPTION
   WHEN OTHERS THEN
     NULL;
   END;

   BEGIN
    --delete from on DMS archive staging if exists -- Version# 1.1
    DELETE FROM xxwc_om_dms_change_archive_tbl
     WHERE header_id = :new.header_id
       AND delivery_id = l_delivery_id;
   EXCEPTION
   WHEN OTHERS THEN
     NULL;
   END;


  END IF;

EXCEPTION
  WHEN OTHERS THEN
    xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                        ,p_calling           => l_err_callpoint
                                        ,p_ora_error_msg     => substr(SQLERRM ||
                                                                       regexp_replace(l_err_callpoint
                                                                                     ,'[[:cntrl:]]'
                                                                                     ,NULL)
                                                                      ,1
                                                                      ,2000)
                                        ,p_error_desc        => 'Error inserting table for tracking changes for DMS'
                                        ,p_distribution_list => l_distro_list
                                        ,p_module            => 'OM');

END xxwc_oe_order_lines_bru2_trg;
/
