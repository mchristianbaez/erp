
  CREATE OR REPLACE TRIGGER "APPS"."XXCUS_AMS_MEDIA_TRG" 
BEFORE INSERT OR UPDATE
ON AMS.AMS_MEDIA_TL
REFERENCING NEW AS New OLD AS Old
FOR EACH ROW
DECLARE
tmpVar NUMBER;
v_media_type ams_media_b.media_type_code%type :=Null;
v_message ams_media_tl.description%type :=Null;
/*
 -- ****************** HISTORY **************************** 
 ESMS      DATE         COMMENTS
 ========  ============ ==============================================
 216229    19/AUG/2013  OTM Application Needs Personalization to Require a Media Description
*/
BEGIN
 begin
     select media_type_code
     into   v_media_type
     from   ams_media_b
     where  1 =1
       and  media_id =:new.media_id; 
 exception
  when no_data_found then
   v_media_type :='NONE';
  when others then
   v_media_type :='NONE';  
 end;
 if (v_media_type ='DEAL') and (:New.description Is Null)
  then
     fnd_message.clear;
     raise_application_error(-20100, 'HDS Rebates Validation : Description is required when the channel category is Deal.', FALSE);
     --fnd_message.raise_error;
 end if;
EXCEPTION
 WHEN OTHERS THEN
       -- Consider logging the error and then re-raise
   RAISE;
END XXCUS_AMS_MEDIA_TRG;
ALTER TRIGGER "APPS"."XXCUS_AMS_MEDIA_TRG" ENABLE;
