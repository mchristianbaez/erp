CREATE OR REPLACE TRIGGER APPS.XXWC_CC_ENTRIES_AU
 /******************************************************************************
      NAME:       XXXWC_CC_ENTRIES_AU

      PURPOSE:    To delete records from XXWC_CC_QUANTITIES_DETAIL_TBL when INV.MTL_CYCLE_COUNT_ENTRIES entry status changed from 4 or 5

      Logic:     1) Delete records from XXWC_CC_QUANTITIES_DETAIL_TBL where the cycle_count_header_id and cycle_count_entry_id match 
      
      REVISIONS:
      Ver        Date        Author           Description
      ---------  ----------  ---------------  ------------------------------------
      1.0        20-JUN-13   Lee Spitzer       1. Created this trigger.  TMS Ticket - 20130214-01096 Implement Physical Inventory enhancements 
@SC @FIN / Implement Physical Inventory enhancements ... 
      
   ******************************************************************************/
AFTER UPDATE
   ON inv.mtl_cycle_count_entries
   REFERENCING NEW AS NEW OLD AS OLD
   FOR EACH ROW
WHEN (NEW.ENTRY_STATUS_CODE in (4,5))
DECLARE
    v_exception        EXCEPTION;
    g_user_id          NUMBER := fnd_global.user_id;
    g_login_id         NUMBER := fnd_profile.VALUE('LOGIN_ID');
    g_err_callfrom     VARCHAR2(75) DEFAULT 'XXWC_CC_ENTRIES_AU';
    g_err_callpoint    VARCHAR2(75) DEFAULT 'START';
    g_distro_list      VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
    l_err_msg          VARCHAR2(2000);
    l_message          VARCHAR2(2000);
BEGIN

    BEGIN
    
      DELETE FROM XXWC.XXWC_CC_QUANTITIES_DETAIL_TBL
      WHERE INVENTORY_ITEM_ID = :NEW.INVENTORY_ITEM_ID
      AND   ORGANIZATION_ID = :NEW.ORGANIZATION_ID
      AND   SUBINVENTORY_CODE = :NEW.SUBINVENTORY
      AND   nvl(LOT_NUMBER,'-99999') = nvl(:NEW.LOT_NUMBER,'-99999')
      AND   CYCLE_COUNT_ENTRY_ID = :NEW.CYCLE_COUNT_ENTRY_ID
      AND   CYCLE_COUNT_HEADER_ID = :NEW.CYCLE_COUNT_HEADER_ID
      AND   COUNT_LIST_SEQUENCE = :NEW.COUNT_LIST_SEQUENCE;
         
    END;
    
EXCEPTION

    when others then
      
        l_message := 'Encountered when others exception on trigger';
        l_err_msg := SQLCODE || SQLERRM;
      
       xxcus_error_pkg.xxcus_error_main_api(p_called_from      => g_err_callfrom
                                          ,p_calling           => g_err_callpoint
                                          ,p_ora_error_msg     => l_err_msg
                                          ,p_error_desc        => l_message
                                          ,p_distribution_list => g_distro_list
                                          ,p_module            => 'INV');    
    
END;