CREATE OR REPLACE TRIGGER APPS.XXWC_OE_ORDER_HOLDS_ARU_T1
AFTER UPDATE ON OE_ORDER_HOLDS_ALL
FOR EACH ROW
WHEN (
NEW.HOLD_RELEASE_ID   IS NOT NULL And NEW.LINE_ID IS  NULL
      )
DECLARE
/*************************************************************************
     $Header XXWC_OE_ORDER_HOLDS_ARU_T1 $
     Module Name: XXWC_OE_ORDER_HOLDS_ARU_T1.sql

     PURPOSE:   This trigger releases PGR Holds at Line Level when Header Leve Holds are released 

     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.1        13/FEB/2013 Satish U              20130213-01750 To Address DB Trigger Mutation Issue 
     1.2        16/OCT/2014 Maharajan Shunmugam        TMS# 20141001-00161 Canada Multi org changes
     1.3        26-DEC-2014 Shankar Hariharan     TMS 20140723-00285-- Rewrite trigger to avoid record lock contention issues
   **************************************************************************/

  -- Satish U : 15-DEC-2011 Pricing GuardRails Variables
   -- Define Local Variables
   l_API_Name         Varchar2(60) := 'XXWC_OE_ORDER_HOLDS_BRU_T1' ;
   L_HOLD_ID          NUMBER;
   L_MSG_COUNT        NUMBER;
   l_msg_data         VARCHAR2(4000);
   l_Return_Status    Varchar2(1) ;


   L_ORDER_TBL        OE_HOLDS_PVT.ORDER_TBL_TYPE;
   l_Holds_Count      Number;
   l_Org_ID           Number ;


    l_OM_PG_Hold_ID         Number  := NVL(fnd_profile.VALUE ('XXWC_OM_PG_HOLD'),0); -- Satish U : 08-FEB-2013 : # 20130208-01001  Added NVL Function 
    l_OM_PG_HOLD_LU_BOOK    Number  := NVL(fnd_profile.VALUE ('XXWC_OM_PG_HOLD_LU_BOOK'),0); -- Satish U : 08-FEB-2013 : # 20130208-01001  Added NVL function 





     --Define variables for logging debug messages
   C_LEVEL_UNEXPECTED CONSTANT    NUMBER := 6;
   C_LEVEL_ERROR      CONSTANT    NUMBER := 5;
   C_LEVEL_EXCEPTION  CONSTANT    NUMBER := 4;
   C_LEVEL_EVENT      CONSTANT    NUMBER := 3;
   C_LEVEL_PROCEDURE  CONSTANT    NUMBER := 2;
   C_LEVEL_STATEMENT  CONSTANT    NUMBER := 1;

Begin


   xxwc_gen_routines_pkg.LOG_MSG (p_debug_level       => C_LEVEL_STATEMENT,
                                         p_mod_name      => l_API_NAME,
                                         P_DEBUG_MSG     => '2000: Begining of API : ' );
                                         
   xxwc_gen_routines_pkg.LOG_MSG (p_debug_level       => C_LEVEL_STATEMENT,
                                         p_mod_name      => l_API_NAME,
                                         P_DEBUG_MSG     => '2010: OM PG Hold ID : ' ||l_OM_PG_Hold_ID );
   xxwc_gen_routines_pkg.LOG_MSG (p_debug_level       => C_LEVEL_STATEMENT,
                                         p_mod_name      => l_API_NAME,
                                         P_DEBUG_MSG     => '2012: OM PG Hold LU BOOK : '|| l_OM_PG_HOLD_LU_BOOK );
   l_return_status := NULL;
   l_msg_data := NULL;
   l_msg_count := NULL;

   -- Satish U : 08-FEB-2013 : # 20130208-01001  :  Added Exists statement, 
   -- to check if any Sale sOrder Lines exists with pricing guard rail hold and not released
   Select Distinct Hold_Id
   Into l_Hold_ID
   From oe_hold_sources
   Where Hold_Source_ID = :NEW.Hold_Source_ID
   And Hold_ID In (l_OM_PG_Hold_ID, l_OM_PG_HOLD_LU_BOOK )
   And Org_ID = :NEW.ORG_ID  ; 
  
   xxwc_gen_routines_pkg.LOG_MSG (
         p_debug_level   => C_LEVEL_STATEMENT,
         p_mod_name      => l_API_NAME,
         P_DEBUG_MSG     => '2005: Hold ID:  ' || l_Hold_ID);

   /*
    XXWC_OM_PRICING_GUARDRAIL_PKG.DB_TRIGGER_RELEASE_HOLD2(
         p_header_id      => :NEW.header_id    ,
         p_hold_id        => l_HOLD_ID    ,
         P_Org_ID         => :NEW.ORG_ID    ,
         x_Return_Status  => l_Return_Status  ,
         x_Msg_Data       => l_Msg_Data );
    */
     insert 
       into apps.xxwc_om_pgr_hold_release_tbl
          (
            header_id,
            hold_id,
            org_id,
            process_flag,
            error_message,
            creation_date,
            created_by,
            last_update_date,
            last_updated_by,
            last_update_login      
          )
    values
          ( 
            :NEW.header_id,
            l_hold_id,
            :NEW.org_id,
            'N',
            null,
            sysdate,
            fnd_global.user_id,
            sysdate,
            fnd_global.user_id,
            fnd_global.login_id
          );
    
    l_Return_Status := fnd_api.g_ret_sts_success;
    
     IF l_Return_Status = fnd_api.g_ret_sts_success THEN
          xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   => C_LEVEL_STATEMENT,
                                             p_mod_name      => l_API_NAME,
                                             P_DEBUG_MSG     => '2010: Order Line Hold Released : Release Hold API Successful: ');
     Elsif l_Return_Status <> fnd_api.g_ret_sts_success THEN

        FOR l_index IN 1..l_msg_count  LOOP
           l_msg_data :=  oe_msg_pub.get
                                  (p_msg_index      => l_index ,
                                   p_encoded        => 'F');

          l_msg_data := l_msg_data || ' :' || l_msg_data;

        END LOOP;
        xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   => C_LEVEL_ERROR,
                                       p_mod_name      => l_API_NAME,
                                       P_DEBUG_MSG     => '2020: Release Hold API Errored Out : ' || l_msg_data);


     END IF; -- End If for Checking Return Status from Apply_Holds

EXCEPTION
   WHEN OTHERS THEN
        l_return_status := fnd_api.g_ret_sts_error;
        l_Msg_Data      := l_Msg_Data|| '- '|| sqlerrm;
        xxwc_gen_routines_pkg.LOG_MSG (p_debug_level   => C_LEVEL_ERROR,
                                             p_mod_name      =>  l_API_NAME,
                                             P_DEBUG_MSG     => '2050:  When Others Exception Release Hold API Errored Out : ' || l_msg_data);
End ;
/