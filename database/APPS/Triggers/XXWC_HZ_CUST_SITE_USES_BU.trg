CREATE OR REPLACE TRIGGER APPS.XXWC_HZ_CUST_SITE_USES_BU
   /***********************************************************************************************
   *   NAME:       XXWC_HZ_CUST_SITE_USES_BU                                                      *
   *                                                                                              *
   *      PURPOSE:    Before update trigger on HZ_CUST_SITE_USES_ALL                              *
   *                                                                                              *
   *                                                                                              *
   *      REVISIONS:                                                                              *
   *      Ver        Date        Author           Description                                     *
   *      ---------  ----------  ---------------  ------------------------------------            *
   *      1.0        21-JAN-15  Lee Spitzer       1. TMS Ticket 20121217-00636 DQM Update         *
   ***********************************************************************************************/

BEFORE UPDATE
   ON AR.HZ_CUST_SITE_USES_ALL
   REFERENCING NEW AS NEW OLD AS OLD
   FOR EACH ROW
WHEN (nvl(NEW.LOCATION,'ZZZ') != nvl(OLD.LOCATION,'ZZZ'))  --When the LOCATION field is updated

DECLARE
  
  l_message        VARCHAR2(2000);
  l_sqlerrm        VARCHAR2(1000);
  l_sqlcode        NUMBER;   
  l_party_site_id  NUMBER;
  l_exception      EXCEPTION;
  l_set_mode       BOOLEAN;
  x_request_id     NUMBER;
  l_call_from                 VARCHAR2(175) := 'XXWC_HZ_CUST_SITE_USES_BU';
  l_call_point                VARCHAR2(175) := 'Start';
  l_distro_list               VARCHAR2 (80) := 'hdsoracledevelopers@hdsupply.com';
  l_module                    VARCHAR2 (80) := 'AR';
  l_trigger_enabled           VARCHAR2(1);

BEGIN

  l_call_point := 'check if trigger is enabled';
  
  BEGIN
    l_trigger_enabled := nvl(FND_PROFILE.VALUE('XXWC_PARTY_SITE_UPDATE_TRIGGER'),'N');
  EXCEPTION
    WHEN OTHERS THEN
          l_sqlcode := SQLCODE;
          l_sqlerrm := SQLERRM;
          l_message := 'Error getting trigger value ' || l_sqlcode || l_sqlerrm;
                 
      RAISE L_EXCEPTION;
  END;
  
  IF l_trigger_enabled = 'Y' THEN
  
  
      l_call_point := 'get party_site_id';
      --get the party_site_id
      BEGIN
        SELECT hcasa.party_site_id
        INTO   l_party_site_id
        FROM   HZ_CUST_ACCT_SITES_ALL hcasa
        WHERE  hcasa.cust_acct_site_id = :NEW.cust_acct_site_id
        ;
      EXCEPTION
        WHEN OTHERS THEN
                      l_sqlcode := SQLCODE;
                      l_sqlerrm := SQLERRM;
                      l_message := 'Error getting party_site_id ' || l_sqlcode || l_sqlerrm;
                     
          RAISE L_EXCEPTION;
      END;
    
                --starting process to send the concurrent request and pass the party_site_id to the concurrent request
                l_set_mode := FND_Request.Set_Mode(TRUE);
      
                l_call_point := 'calling concurrent request';
                
                BEGIN
                  x_request_id :=
                  fnd_request.submit_request( APPLICATION => 'XXWC'
                                            , PROGRAM =>     'XXWC_PARTY_SITE_UPDATE'
                                            , DESCRIPTION => 'XXWC HZ Party Site Update'
                                            , SUB_REQUEST => FALSE
                                            , ARGUMENT1 => l_party_site_id
                                            );
                EXCEPTION
                  WHEN others THEN
                      l_sqlcode := SQLCODE;
                      l_sqlerrm := SQLERRM;
                      l_message := 'Error calling XXWC_PARTY_SITE_UPDATE ' || l_sqlcode || l_sqlerrm;
                      raise l_exception;
                END;

    END IF;

EXCEPTION
  WHEN L_EXCEPTION THEN
             l_message := l_message;
             
             xxcus_error_pkg.xxcus_error_main_api (  p_called_from         => l_call_from
                                                   , p_calling             => l_call_point
                                                   , p_ora_error_msg       => l_sqlerrm
                                                   , p_error_desc          => 'When others error in call ' || l_call_from || l_call_point || l_message
                                                   , p_distribution_list   => l_distro_list
                                                   , p_module              => l_module);

  WHEN OTHERS THEN
             l_sqlcode := SQLCODE;
             l_sqlerrm := SQLERRM;
             l_message := 'WHEN others calling XXWC_PARTY_SITE_UPDATE ' || l_sqlcode || l_sqlerrm;
             
             xxcus_error_pkg.xxcus_error_main_api (  p_called_from         => l_call_from
                                                   , p_calling             => l_call_point
                                                   , p_ora_error_msg       => l_sqlerrm
                                                   , p_error_desc          => 'When others error in call ' || l_call_from || l_call_point || l_message
                                                   , p_distribution_list   => l_distro_list
                                                   , p_module              => l_module);

END;
