CREATE OR REPLACE TRIGGER APPS.XXWC_OE_HEADERS_IFACE_ALL_TRG1
BEFORE INSERT
ON ONT.OE_HEADERS_IFACE_ALL
REFERENCING NEW AS New OLD AS Old
FOR EACH ROW
WHEN (
NEW.ORDER_SOURCE_ID = 10
      )
DECLARE

/******************************************************************************
   NAME:       XXWC_OE_HEADERS_IFACE_ALL_TRG1
   PURPOSE:    

   REVISIONS:
   Ver        Date        Author            Description
   ---------  ----------  ---------------   ------------------------------------
   1.0        8/9/2012    Consuelo Gonzalez   1. Initial definition to default ship
                                              from org id on the order header
   1.1        8/15/2012   Lee Spitzer         2. Updated orig_sys_document_ref 
   1.2        10/17/2014  Maharajan Shunmugam TMS# 20141001-00161  Canada Multi Org changes

   NOTES:

   Automatically available Auto Replace Keywords:
      Object Name:     XXWC_OE_HEADERS_IFACE_ALL_TRG1
      Sysdate:         8/9/2012
      Date and Time:   8/9/2012, 3:29:53 PM, and 8/9/2012 3:29:53 PM
      Username:         (set in TOAD Options, Proc Templates)
      Table Name:      OE_HEADERS_IFACE_ALL (set in the "New PL/SQL Object" dialog)
      Trigger Options:  (set in the "New PL/SQL Object" dialog)
******************************************************************************/

    l_ship_from_org_id  NUMBER;
    l_preparer_id       NUMBER;
    l_shipping_method   VARCHAR2(80);
    l_hdr_notes         VARCHAR2(240);
    l_emp_name          VARCHAR2(240);
    l_user_id           NUMBER;

BEGIN
    l_ship_from_org_id := null;
    l_preparer_id      := null;
    l_shipping_method  := null;
    l_hdr_notes        := null;
                                              
    begin
        SELECT     distinct(req_ln.source_organization_id) 
                   , req_hdr.preparer_id
                   , req_hdr.attribute2 -- Notes
                   , req_hdr.attribute3 -- Shipping Method                   
         INTO   l_ship_from_org_id
                , l_preparer_id 
                , l_hdr_notes
                , l_shipping_method
         FROM po_requisition_headers req_hdr,
              po_requisition_lines req_ln,
              mtl_parameters mp_from,
              org_organization_definitions ood_from,
              mtl_parameters mp_to,
              org_organization_definitions ood_to
        WHERE req_hdr.type_lookup_code = 'INTERNAL'
          --AND req_hdr.segment1 = :new.orig_sys_document_ref removed 8/15/2012
          AND req_hdr.requisition_header_id = :new.orig_sys_document_ref --added -8/15/2012
          AND req_hdr.requisition_header_id = req_ln.requisition_header_id
          --AND req_ln.requisition_header_id = :source_document_id
          AND req_ln.source_organization_id = mp_from.organization_id
          AND mp_from.organization_id = ood_from.organization_id
          AND req_ln.destination_organization_id = mp_to.organization_id
          AND mp_to.organization_id = ood_to.organization_id     
          AND rownum = 1;
    exception
    when others then
        l_ship_from_org_id := null;
        l_preparer_id      := null;
        l_shipping_method  := null;
        l_hdr_notes        := null;
    end;    
   
    l_emp_name  := null;
    l_user_id   := null;
    
    if l_preparer_id is not null then
    
        begin
        
            select  papf.full_name
                    , fu.user_id
            into    l_emp_name
                    , l_user_id
            from    per_all_people_f papf
                    , fnd_user fu
            where   papf.person_id = l_preparer_id
            and     trunc(effective_start_date) <= trunc(sysdate)
            and     nvl(trunc(effective_end_date), trunc(sysdate)) >= trunc(sysdate)
            and     papf.person_id = fu.employee_id(+)
            and     rownum = 1;
        
        exception
        when others then
            l_emp_name  := null;
            l_user_id   := null;
        end;
        
    end if;

   

   :New.ship_from_org_id := l_ship_from_org_id;
   -- :New.created_by := l_preparer_id;
   -- :New.attribute7 := l_preparer_id;
   --:New.created_by := l_user_id; Removed 11/16/2012
   :New.attribute7 := l_emp_name; 
   :New.shipping_instructions := l_hdr_notes;
   :New.shipping_method_code := l_shipping_method;

   EXCEPTION
     WHEN OTHERS THEN
       -- Consider logging the error and then re-raise
       null;
END XXWC_OE_HEADERS_IFACE_ALL_TRG1;
/
