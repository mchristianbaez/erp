CREATE OR REPLACE TRIGGER APPS.XXWC_ONT_PAYMENTS_ARU
   AFTER UPDATE OF PREPAID_AMOUNT
   ON ONT.OE_PAYMENTS
   REFERENCING NEW AS New OLD AS Old
   FOR EACH ROW
WHEN (
new.prepaid_amount <> old.prepaid_amount
         AND new.prepaid_amount < old.prepaid_amount
         AND new.payment_type_code in ('CASH','CHECK')
         AND new.payment_collection_event = 'PREPAY'
      )
DECLARE
   l_counter_order         VARCHAR2 (1);
   l_return_status         VARCHAR2 (1);
   l_msg_data              VARCHAR2 (2000);
   l_status_code           VARCHAR2 (80);
   l_cash_receipt_id       NUMBER;
   l_refund_email          VARCHAR2 (100)
                              := fnd_Profile.VALUE ('XXWC_OM_REFUND_REQ_EMAIL');
   l_return_order_number   NUMBER;
   l_max_refund_amount     NUMBER;
   l_applied_amt           NUMBER; 
   l_cc_type               VARCHAR2 (1);
/******************************************************************************
   NAME:  XXWC_ONT_PAYMENTS_ARU
   PURPOSE: Trigger to capture refund information for Cash Payment type

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        4/4/2013      shariharan       1. Created this trigger.
   1.1        04/11/2013  Shankar Hariharan  Oracle Credit Card Disconnect Project
   1.2        07/01/2013  Shankar Hariharan  TMS Ticket 20130618-01124--include check payment method for auto w/o
   1.3        10/20/2014  Pattabhi Avula     TMS# 20141001-00172  Multi Org Changes done
******************************************************************************/
BEGIN
   SELECT 'Y', flow_status_code, order_number
     INTO l_counter_order, l_status_code, l_return_order_number
     FROM oe_order_headers_v
    WHERE /*order_type = 'COUNTER ORDER'
      AND*/ header_id=:new.header_id;

   IF l_status_code NOT IN ('ENTERED', 'CANCELLED')
   THEN
      BEGIN
         SELECT cash_receipt_id
           INTO l_cash_receipt_id
           FROM xxwc_om_co_receipt_v
          WHERE     header_id = :new.header_id
                AND payment_number = :new.payment_number
                AND payment_type_code = :new.payment_type_code
                AND ROWNUM = 1;
          -- Oracle CC Disconnect Project Shankar Hariharan 04/11/2013
             IF :new.payment_type_code = 'CHECK' then
               BEGIN
               select 'Y'
                 into l_cc_type
                 from apps.ar_cash_receipts a, ar_receipt_methods b
                where a.receipt_method_id=b.receipt_method_id
                  and a.cash_receipt_id=l_cash_receipt_id
                  and (b.name like '%AMX%' or b.name like '%VMD%');
               EXCEPTION
                 when others then
                   l_cc_type := 'N';               
               END;  
             END IF;
          -- Oracle CC Disconnect Project Shankar Hariharan 04/11/2013 END
/*
          IF :new.payment_type_code = 'CASH' or
             (:new.payment_type_code = 'CHECK' and nvl(l_cc_type,'N') = 'Y') then
             */
          IF :new.payment_type_code in ('CASH','CHECK')  then                 
           insert 
             into xxwc_om_co_refunds_tbl
                (header_id,
                 cash_receipt_id,
                 refund_amount,
                 payment_type_code,
                 process_flag,
                 creation_date,
                 created_by,
                 last_update_date,
                 last_updated_by,
                 last_update_login
                )
           values
                (:new.header_id,
                  l_cash_receipt_id,
                  (:old.prepaid_amount - :new.prepaid_amount),
                  :new.payment_type_code,
                  'N',
                  sysdate,
                  fnd_global.user_id,
                  sysdate,
                  fnd_global.user_id,
                  fnd_global.login_id
                );     
          END IF;
      EXCEPTION
         WHEN OTHERS
         THEN
            UTL_MAIL.send (
               sender       => l_refund_email
             , recipients   => l_refund_email
             , cc           => NULL
             , bcc          => NULL
             , subject      => 'Cash Refund Request for Order '
                              || l_return_order_number
             , MESSAGE      =>   'Issue Processing Cash Refund-Identifying Cash Receipt '
                              || fnd_global.user_name
                              || '    Original Order Number: '
                              || l_return_order_number
                              || CHR (10)
                              || 'Requested Cash Refund: '
                              || (:old.prepaid_amount - :new.prepaid_amount)
             , mime_type    => 'text/plain; charset=us-ascii'
             , priority     => 3
             , replyto      => NULL);
      END;
   END IF;
   
EXCEPTION
   WHEN OTHERS
   THEN
      -- It is not a Counter order
      NULL;
END;
/