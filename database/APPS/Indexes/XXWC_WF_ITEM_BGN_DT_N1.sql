/********************************************************************************************************************************
    $Header XXWC_WF_ITEM_BGN_DT_N1.sql $
    Module Name: XXWC_WF_ITEM_BGN_DT_N1.sql

    PURPOSE:   Index creation script

    REVISIONS:
    Ver        Date        Author                     Description
    ---------  ----------  ---------------         -------------------------------------------------------------------------------
    1.0        11-Nov-2016 Niraj K Ranjan          Initial Version - TMS#20160804-00335   Create a concurrent program to Cancel 
	                                                                 --Errored Workflows so they may be purged by the purge job
*********************************************************************************************************************************/
CREATE INDEX APPS.XXWC_BGN_DT_N1 ON WF_ITEMS(TRUNC(begin_date));
