/******************************************************************************************************
-- File Name: TMS_20170817-00062_XXWC_OM_DMS_SHIP_CONFIRM_N5.sql
--
-- PROGRAM TYPE: Index script
--
-- PURPOSE: Index to improve performance for 'XXWC B2B Create POD Files' program
-- HISTORY
-- ==========================================================================================================
-- ==========================================================================================================
-- VERSION DATE          AUTHOR(S)       DESCRIPTION
-- ------- -----------   --------------- --------------------------------------------------------------------
-- 1.0     17-AUG-2017   Pattabhi Avula  TMS#20170817-00062  PERF: 'XXWC B2B Create POD Files' Concurrent Program 
                                         Performance issue fixes
--                                       Initial version

************************************************************************************************************/
CREATE INDEX xxwc.XXWC_OM_DMS_SHIP_CONFIRM_N5 ON xxwc.XXWC_OM_DMS_SHIP_CONFIRM_TBL(FILE_NAME)
/
