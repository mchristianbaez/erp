  /*******************************************************************************
  Index: APPS.XXWC_MD_SEARCH_MV_N6 
  Description: This index is used on XXWC_MD_SEARCH_PRODUCTS_MV Materialized View 
  and is used on AIS form
  HISTORY
  ===============================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     03-Mar-2016        Pahwa Nancy   TMS# 20160301-00328  Performance Tuning
  ********************************************************************************/
 create index APPS.XXWC_MD_SEARCH_MV_N6 on XXWC_MD_SEARCH_PRODUCTS_MV (inventory_item_id)

/