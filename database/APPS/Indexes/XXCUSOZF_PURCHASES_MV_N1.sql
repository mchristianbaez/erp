--
-- XXCUSOZF_PURCHASES_MV_N1  (Index) 
--
CREATE INDEX APPS.XXCUSOZF_PURCHASES_MV_N1 ON APPS.XXCUSOZF_PURCHASES_MV
(MVID, CALENDAR_YEAR, PERIOD_ID)
TABLESPACE APPS_TS_TX_DATA;


