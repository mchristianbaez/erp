/*************************************************************************
 Copyright (c) 2012 HD Supply
 All rights reserved.
**************************************************************************
   $Header APPS.XXWC_MD_PRODUCT_SEARCH_PREF $
  Module Name: APPS.XXWC_MD_PRODUCT_SEARCH_PREF 

  PURPOSE:

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)   TMS TASK        DESCRIPTION
  -- ------- -----------   ------------ ---------      -------------------------
  -- 1.0     1-SEP-2015   Pahwa, Nancy                Initially Created 
TMS# 20150825-00012
**************************************************************************/
begin 
  ctx_ddl.create_preference('XXWC_MD_PRODUCT_SEARCH_PREF', 'BASIC_WORDLIST'); 
  ctx_ddl.set_attribute('XXWC_MD_PRODUCT_SEARCH_PREF','SUBSTRING_INDEX','TRUE');
  ctx_ddl.set_attribute('XXWC_MD_PRODUCT_SEARCH_PREF','PREFIX_INDEX','TRUE');
  ctx_ddl.set_attribute('XXWC_MD_PRODUCT_SEARCH_PREF','PREFIX_MIN_LENGTH',2);
  ctx_ddl.set_attribute('XXWC_MD_PRODUCT_SEARCH_PREF', 'wildcard_maxterms', 20000) ;
  ctx_ddl.set_attribute('XXWC_MD_PRODUCT_SEARCH_PREF','FUZZY_MATCH','ENGLISH');
  ctx_ddl.set_attribute('XXWC_MD_PRODUCT_SEARCH_PREF','FUZZY_SCORE',70);
ctx_ddl.set_attribute('XXWC_MD_PRODUCT_SEARCH_PREF','STEMMER','ENGLISH');

end; 
/