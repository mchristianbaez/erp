/******************************************************************************************************
-- File Name: TMS_20170105-00157_XXWC_RCV_TRANSACTIONS_N29.sql
--
-- PROGRAM TYPE: Index script
--
-- PURPOSE: Index to improve performance for 'XXWC Return To Vendor' program
-- HISTORY
-- ==========================================================================================================
-- ==========================================================================================================
-- VERSION DATE          AUTHOR(S)       DESCRIPTION
-- ------- -----------   --------------- --------------------------------------------------------------------
-- 1.0     12-JAN-2017   Pattabhi Avula  TMS#20170105-00157  PERF: 'XXWC Return To Vendor' Concurrent Program 
                                         Performance issue fixes
--                                       Initial version

************************************************************************************************************/

CREATE INDEX PO.XXWC_RCV_TRANSACTIONS_N29  ON PO.RCV_TRANSACTIONS(VENDOR_ID)
/