/********************************************************************************
   $Header OE_ORDER_LINES_HISTORY_N1.sql $
   Module Name: OE_ORDER_LINES_HISTORY_N1

   PURPOSE:   Drop and Create standard Index on OE_ORDER_LINES_HISTORY table 

   REVISIONS:
   Ver        Date        Author                     Description
   ---------  ----------  ---------------         -------------------------
   1.0        09/14/2016  Rakesh Patel            TMS-20160829-00115 Index on OE_ORDER_LINES_HISTORY table 
********************************************************************************/
DROP INDEX ONT.OE_ORDER_LINES_HISTORY_N1;

CREATE INDEX ONT.OE_ORDER_LINES_HISTORY_N1 ON ONT.OE_ORDER_LINES_HISTORY(LINE_ID, HIST_TYPE_CODE, hist_creation_date) parallel;

ALTER INDEX ONT.OE_ORDER_LINES_HISTORY_N1 noparallel;