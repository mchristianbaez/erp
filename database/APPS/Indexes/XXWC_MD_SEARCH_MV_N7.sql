/*************************************************************************
 Copyright (c) 2012 HD Supply
 All rights reserved.
**************************************************************************
   $Header APPS.XXWC_MD_SEARCH_MV_N7 $
  Module Name: APPS.XXWC_MD_SEARCH_MV_N7

  PURPOSE:

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)   TMS TASK        DESCRIPTION
  -- ------- -----------   ------------ ---------      -------------------------
  -- 1.0     10-OCT-2016   Pahwa, Nancy                Initially Created 
TMS# 20160801-00182
**************************************************************************/
--drop index APPS.XXWC_MD_SEARCH_MV_N7;

CREATE INDEX "APPS"."XXWC_MD_SEARCH_MV_N7" ON "APPS"."XXWC_MD_SEARCH_PRODUCTS_MV" ("PARTNUMBER2") 
INDEXTYPE IS "CTXSYS"."CONTEXT"  PARAMETERS ('wordlist XXWC_MD_PRODUCT_SEARCH_PREF');
