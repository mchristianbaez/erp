/******************************************************************************************************
-- File Name: TMS_20170105-00157_XXWC_AP_INV_LN_N14.sql
--
-- PROGRAM TYPE: Index script
--
-- PURPOSE: Index to improve performance for 'XXWC Return To Vendor' program
-- HISTORY
-- ==========================================================================================================
-- ==========================================================================================================
-- VERSION DATE          AUTHOR(S)       DESCRIPTION
-- ------- -----------   --------------- --------------------------------------------------------------------
-- 1.0     12-JAN-2017   Pattabhi Avula  TMS#20170105-00157  PERF: 'XXWC Return To Vendor' Concurrent Program 
                                         Performance issue fixes
--                                       Initial version

************************************************************************************************************/
CREATE INDEX AP.XXWC_AP_INV_LN_N14 ON AP.AP_INVOICE_LINES_ALL(INVOICE_ID,PO_DISTRIBUTION_ID)
/