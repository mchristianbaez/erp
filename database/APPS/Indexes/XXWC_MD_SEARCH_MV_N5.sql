/*************************************************************************
 Copyright (c) 2012 HD Supply
 All rights reserved.
**************************************************************************
   $Header APPS.XXWC_MD_SEARCH_MV_N5 $
  Module Name: APPS.XXWC_MD_SEARCH_MV_N5

  PURPOSE:

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)   TMS TASK        DESCRIPTION
  -- ------- -----------   ------------ ---------      -------------------------
  -- 1.0     1-SEP-2015   Pahwa, Nancy                Initially Created 
TMS# 20150825-00012
**************************************************************************/
drop index APPS.XXWC_MD_SEARCH_MV_N5;
create index APPS.XXWC_MD_SEARCH_MV_N5 on XXWC_MD_SEARCH_PRODUCTS_MV(dummy)
indextype is ctxsys.context 
parameters ('DATASTORE XXWC_MD_PRODUCT_STORE_PREF
lexer		XXWC_MD_PRODUCT_STORE_LEX1   
 	 section group	XXWC_MD_PRODUCT_STORE_SG');