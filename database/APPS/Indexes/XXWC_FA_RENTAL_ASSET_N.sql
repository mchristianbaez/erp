/******************************************************************************************************
-- File Name: TMS_20170103-00322_XXWC_FA_RENTAL_ASSET_N.sql
--
-- PROGRAM TYPE: Index script
--
-- PURPOSE: Index to improve performance for 'XXWC Rental Asset Report' 
-- HISTORY
-- ==========================================================================================================
-- ==========================================================================================================
-- VERSION DATE          AUTHOR(S)       DESCRIPTION
-- ------- -----------   --------------- --------------------------------------------------------------------
-- 1.0     Date 5/10/2017 Neha Saini     TMS Task ID: 20170103-00322

************************************************************************************************************/

DROP INDEX INV.XXWC_MTL_ITEM_CATEGORIES_N30;

CREATE INDEX XXWC_MTL_ITEM_CATEGORIES_N30 ON INV.MTL_ITEM_CATEGORIES
(CATEGORY_ID,INVENTORY_ITEM_ID,ORGANIZATION_ID);

DROP INDEX INV.XXWC_MTL_MATERIAL_TRX_N34;

CREATE INDEX INV.XXWC_MTL_MATERIAL_TRX_N34 ON INV.MTL_MATERIAL_TRANSACTIONS("ORGANIZATION_ID","TRANSACTION_SOURCE_ID");

DROP INDEX INV.XXWC_MTL_SYSTEM_ITEMS_B_N19;

create index INV.XXWC_MTL_SYSTEM_ITEMS_B_N19 on INV.MTL_SYSTEM_ITEMS_B("SEGMENT1","ORGANIZATION_ID");

DROP INDEX ONT.XXWC_OE_TRX_TYPES_TL_N1;

create index ONT.XXWC_OE_TRX_TYPES_TL_N1 on ONT.OE_TRANSACTION_TYPES_TL("TRANSACTION_TYPE_ID");

DROP INDEX INV.XXWC_MTL_MATERIAL_TRX_N33;

create index INV.XXWC_MTL_MATERIAL_TRX_N33 on INV.MTL_MATERIAL_TRANSACTIONS("TRANSACTION_TYPE_ID");

DROP INDEX ONT.XXWC_OE_TRX_TYPES_TL_N2;

create index ONT.XXWC_OE_TRX_TYPES_TL_N2 on ONT.OE_TRANSACTION_TYPES_TL("NAME","TRANSACTION_TYPE_ID");

DROP INDEX INV.XXWC_MTL_MATERIAL_TRX_N32;

create index INV.XXWC_MTL_MATERIAL_TRX_N32 on INV.MTL_MATERIAL_TRANSACTIONS("ORGANIZATION_ID","INVENTORY_ITEM_ID");
/