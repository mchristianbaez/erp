/*************************************************************************
 Copyright (c) 2012 HD Supply
 All rights reserved.
**************************************************************************
   $Header APPS.XXWC_MD_SEARCH_MV2_N8 $
  Module Name: APPS.XXWC_MD_SEARCH_MV2_N8
  PURPOSE:

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)   TMS TASK        DESCRIPTION
  -- ------- -----------   ------------ ---------      -------------------------
  -- 1.0     1-NOV-2016   Pahwa, Nancy                Initially Created 
TMS# 20161102-00165  
**************************************************************************/
--drop index APPS.XXWC_MD_SEARCH_MV2_N8;
CREATE INDEX "APPS"."XXWC_MD_SEARCH_MV2_N8" ON "APPS"."XXWC_MD_SEARCH_PRODUCTS_MV2" ("DUMMY2") 
   INDEXTYPE IS "CTXSYS"."CONTEXT"  PARAMETERS ('DATASTORE XXWC_MD_PRODUCT_STORE_PREF1_V
lexer       XXWC_MD_PRODUCT_STORE_LEX2_V
      section group    XXWC_MD_PRODUCT_STORE_SG1_V');