/*************************************************************************
Copyright (c) 2012 HD Supply
All rights reserved.
**************************************************************************
   $Header XXWC_OE_ORDER_LINES_OFD_N1
  Module Name: XXWC_OE_ORDER_LINES_OFD_N1
  PURPOSE:

  REVISIONS:
  -- VERSION   DATE            AUTHOR(S)          DESCRIPTION
  -- -------   -----------     ------------       ---------      -------------------------
  -- 1.0       20-SEP-2016     Pattabhi Avula     Initially Created TMS# 20160630-00154 
**************************************************************************/
-- out for Delivery 
CREATE INDEX ONT.XXWC_OE_ORDER_LINES_OFD_N20 ON ONT.OE_ORDER_LINES_ALL (SHIP_FROM_ORG_ID,FLOW_STATUS_CODE,ATTRIBUTE16);
