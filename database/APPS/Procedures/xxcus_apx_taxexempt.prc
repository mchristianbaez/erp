CREATE OR REPLACE PROCEDURE xxcus_apx_taxexempt(p_cust_account_id IN NUMBER
                                                         ,p_sitenum IN VARCHAR2
                                                         ,p_state IN VARCHAR2
                                                         ,p_taxtype IN VARCHAR2
                                                         ,p_taxexempt IN VARCHAR2
                                                         ,x_errmsg OUT NOCOPY VARCHAR2
                                                         ,x_process OUT NOCOPY VARCHAR2
                                                         ) IS  PRAGMA AUTONOMOUS_TRANSACTION;

  /*******************************************************************************
  * Procedure:   xxcusar_customer_taxexempt_prc
  * Description: This procedure is used by an APEX App (XXWC Tax Exempt Upload)
                 to execute the AR API.
                 Apex App cannot directly execute the interface program due to Oracle
                 limitation on executing RPC over dblink.
  HISTORY
  ===============================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     11-Sep-2012        Manny           Initial creation of the package
  1.1     16-Apr-2013        Srinivas Reddy  Modified to add Tax Exempt DFF
                                             20130102-01222  
  ********************************************************************************/

  --Basic variables
    l_process            BOOLEAN DEFAULT TRUE;
    l_state                    VARCHAR2(3);
    l_party_site_id            NUMBER;
    l_cust_acct_site_id        NUMBER;
    l_location_id              NUMBER;

  --Intialize Variables
  l_err_msg      VARCHAR2(5000);
  l_err_code     NUMBER;
  l_sec          VARCHAR2(5000);
  l_req_id       NUMBER;
  l_err_callfrom VARCHAR2(75) := 'xxwcar_cust_taxexempt.taxexempt';
  l_distro_list  VARCHAR2(75) DEFAULT 'hdsoracledevelopers@hdsupply.com';

    --API
    x_return_status VARCHAR2(5000);
    x_msg_count     NUMBER;
    x_msg_data      VARCHAR2(5000);
    x_obj_ver       NUMBER;
    l_success       BOOLEAN DEFAULT TRUE;

    --Customer Account Site
    p_cust_acct_site_rec hz_cust_account_site_v2pub.cust_acct_site_rec_type;
    x_cust_acct_site_id  NUMBER;


  --Start Main Program
BEGIN
  l_sec := 'Start Main Program. Initializing variables';

  -- Call the procedure
  mo_global.init('AR');
--  mo_global.set_policy_context('M', 'USD');  

  x_errmsg :=NULL;
  x_process:='Y';


IF l_process THEN
    l_sec := 'Searching for party_site_id';
 BEGIN
   SELECT party_site_id, location_id
   INTO l_party_site_id, l_location_id
   FROM ar.hz_party_sites
   WHERE party_site_number = p_sitenum;
 EXCEPTION
   WHEN no_data_found THEN
     l_process := FALSE;
     x_errmsg  := 'could not find a party SITE ID for the customer name in the hz_parties_sites table';
     x_process := 'N';
 END;
END IF;


IF l_process THEN
    l_sec := 'Searching for matching USA state';
 BEGIN

   SELECT state 
   INTO l_state
   FROM ar.hz_locations 
   WHERE location_id = l_location_id
   AND   state = p_state;

 EXCEPTION
   WHEN no_data_found THEN
     l_process := FALSE;
     x_errmsg  := 'could not find a matching state for the record uploaded';
     x_process := 'N';
 END;
END IF;




IF l_process THEN
    l_sec := 'Searching for object version number';
 BEGIN

select object_version_number, cust_acct_site_id
INTO x_obj_ver, l_cust_acct_site_id
FROM ar.hz_cust_acct_sites_all
WHERE cust_account_id =p_cust_account_id
AND party_site_id = l_party_site_id;

 EXCEPTION
   WHEN no_data_found THEN
     l_process := FALSE;
     x_errmsg  := 'could not find the object version number for the record uploaded';
     x_process := 'N';
 END;
END IF;



 l_sec :='setting the record to its values';
 dbms_output.put_line(' taxtype '||p_taxtype||'.   cust account id - '||p_cust_account_id||' .  obj ver '||x_obj_ver);
 dbms_output.put_line(' taxexempt '||p_taxexempt||'.   cust account id - '||p_cust_account_id||' .  obj ver '||x_obj_ver);  -- Added by Srinivas Reddy on 04/16/2013
 
-- set the record type
p_cust_acct_site_rec.attribute15 :=p_taxtype;
p_cust_acct_site_rec.attribute16 :=p_taxexempt;   -- Added by Srinivas Reddy on 04/16/2013
p_cust_acct_site_rec.cust_acct_site_id :=l_cust_acct_site_id;




-- Do the API upload
IF l_process THEN 
 l_sec :='Doing the API';


hz_cust_account_site_v2pub.update_cust_acct_site (
                                                    'T',
                                                    p_cust_acct_site_rec,
                                                    x_obj_ver,
                                                    x_return_status,
                                                    x_msg_count,
                                                    x_msg_data);

  


IF x_return_status = fnd_api.g_ret_sts_success THEN

  l_sec := 'Location updated "cust_acct_site" Customer Account Site ID:  ' ||
           x_cust_acct_site_id || ' Status = ' ||
           x_return_status || ' Reason = ' || x_msg_data;
  dbms_output.put_line( l_sec);

     x_errmsg  := '';
     x_process := 'Y';

ELSE
  
  l_sec := 'Location NOT updated for  "cust_acct_site" Customer Account Site ID:  ' ||
           x_cust_acct_site_id || ' Status = ' ||
           x_return_status || ' Reason = ' || x_msg_data;
  dbms_output.put_line( l_sec);

     x_errmsg  := 'Error trying to do the API for this record uploaded'||' with reason '||x_msg_data;
     x_process := 'N';

end IF;

END IF;


COMMIT;

EXCEPTION
 
  WHEN OTHERS THEN
    x_errmsg  := x_msg_data||'   '||substr((l_err_msg || l_sec||' - ERROR ' ||
                         substr(SQLERRM, 1, 2000)), 1, 3000);   
    x_process := 'N';
    l_err_code := 2;
    l_err_msg  := substr((l_err_msg || ' ERROR ' ||
                         substr(SQLERRM, 1, 2000)), 1, 3000);
    dbms_output.put_line(l_sec||' - '||l_err_msg);
  
    xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_err_callfrom,
                                         p_calling => l_sec,
                                         p_request_id => l_req_id,
                                         p_ora_error_msg => substr(SQLERRM, 1, 2000),
                                         p_error_desc => substr(l_err_msg, 1, 2000),
                                         p_distribution_list => l_distro_list,
                                         p_module => 'AR');
END ;
/
