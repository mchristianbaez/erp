
  CREATE OR REPLACE PROCEDURE "APPS"."XXCUS_LOAD_PRICING_GL" (p_fperiod IN VARCHAR2) IS

  /*******************************************************************************
  * Procedure:   xxcus_load_pricing_gl
  * Description: Load a table that the datawarehouse user id hass access to based on
                 parameters that the DW provides to the package.  The period will be
                 the period name of that period.
  HISTORY
  ===============================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     XXHSI_LOAD_PRICING_GL  Jason Sloan     Initial creation of the package
  2.0     04/01/2012         Luong Vu        Upgrade to R12.
  ********************************************************************************/

  --Intialize Variables
  l_err_msg        VARCHAR2(2000);
  l_err_code       NUMBER;
  l_sec            VARCHAR2(150);
  l_procedure_name VARCHAR2(75) := 'XXCUS_LOAD_PRICING_GL';
  l_distro_list    VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';

  --Start Main Program
BEGIN
  l_sec := 'Start Main Program; ';
  fnd_file.put_line(fnd_file.log, l_sec);
  fnd_file.put_line(fnd_file.output, l_sec);

  -- Call the procedure
  xxcusdw_pricing_extract_pkg.load_gl_int_table(errbuf => l_err_msg,
                                                retcode => l_err_code,
                                                p_fperiod => p_fperiod);

EXCEPTION
  WHEN program_error THEN
    ROLLBACK;
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
    l_err_code := 2;
    l_err_msg  := 'Error in program ' || l_procedure_name || ' Error: ' ||
                  l_sec;
    l_err_msg  := l_err_msg || ' ERROR ' || substr(SQLERRM, 1, 2000);
  
    fnd_file.put_line(fnd_file.log, l_err_msg);
    fnd_file.put_line(fnd_file.output, l_err_msg);
  
    xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_procedure_name,
                                         p_calling => l_sec,
                                         p_ora_error_msg => SQLERRM,
                                         p_error_desc => l_err_msg,
                                         p_distribution_list => l_distro_list,
                                         p_request_id => NULL);
  WHEN OTHERS THEN
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
    l_err_code := 2;
    l_err_msg  := l_sec || l_err_msg || ' ERROR ' || SQLCODE ||
                  substr(SQLERRM, 1, 2000);
    fnd_file.put_line(fnd_file.log, l_err_msg);
    fnd_file.put_line(fnd_file.output, l_err_msg);
  
    xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_procedure_name,
                                         p_calling => l_sec,
                                         p_ora_error_msg => SQLERRM,
                                         p_error_desc => l_err_msg,
                                         p_distribution_list => l_distro_list,
                                         p_request_id => NULL);
END xxcus_load_pricing_gl;
;
