CREATE OR REPLACE PROCEDURE xxwc_oe_order_lines_index (p_owner VARCHAR2,p_index_name VARCHAR2)
IS
    v_index_name   VARCHAR2 (124);
    v_owner        VARCHAR2 (124);
BEGIN
    v_index_name := UPPER (p_index_name);
    v_owner := UPPER (p_owner);
    DBMS_STATS.gather_index_stats (ownname => v_owner, indname => v_index_name);
EXCEPTION
    WHEN OTHERS
    THEN
        NULL;
END;
