CREATE OR REPLACE PROCEDURE APPS.XXWC_PO_ACK_METHOD (
   p_po_header_id             IN     NUMBER,
   p_po_rev                      OUT NUMBER,
   p_po_comm_default             OUT VARCHAR2,
   p_po_comm_actual              OUT VARCHAR2,
   p_po_ack_exp                  OUT VARCHAR2,
   p_po_comm_date                OUT DATE,
   p_po_ack_date                 OUT DATE,
   p_po_rev_latest               OUT NUMBER,
   p_po_change_comm_default      OUT VARCHAR2,
   p_po_change_comm_actual       OUT VARCHAR2,
   p_po_change_ack_exp           OUT VARCHAR2,
   p_po_change_comm_date         OUT DATE,
   p_po_change_ack_date          OUT DATE)
IS
   /*************************************************************************
     $Header XXWC_PO_ACK_METHOD.prc $
     Module Name: XXWC_PO_ACK_METHOD

     PURPOSE: Procedure to populate the PO Acknowledgement communication method

     REVISIONS:
     Ver        Date         Author                Description
     ---------  ----------   ------------------    ----------------
     1.0        16-Mar-2015  Manjula Chellappan    Initial Version TMS# 20141113-00099
     1.1        16-Apr-2015  Manjula Chellappan    TMS# 20150415-00244 - Need change to PO Communications Info screen
   **************************************************************************/
   l_po_rev                   NUMBER;
   l_po_comm_default          VARCHAR2 (30);
   l_po_ack_exp               VARCHAR2 (30);
   l_po_comm_actual           VARCHAR2 (30);
   l_po_comm_date             DATE;
   l_po_ack_date              DATE;
   l_po_rev_latest            NUMBER;
   l_po_change_comm_default   VARCHAR2 (30);
   l_po_change_ack_exp        VARCHAR2 (30);
   l_po_change_comm_actual    VARCHAR2 (30);
   l_po_change_comm_date      DATE;
   l_po_change_ack_date       DATE;
   l_po_header_id             NUMBER;
   l_vendor_site_id           NUMBER;
   l_edi_flag                 VARCHAR2 (1);
   l_approved_date            DATE;
   l_tp_header_id             NUMBER;
   l_approved_flag            VARCHAR2 (1);
   l_edi_ack_flag             VARCHAR2 (5);
   l_others                   VARCHAR2 (15) := 'FAX / EMAIL';
   l_po_type                  VARCHAR2 (30);
   l_sec                      VARCHAR2 (400);
   l_distribution_list        fnd_user.email_address%TYPE
                                 := 'HDSOracleDevelopers@hdsupply.com';
BEGIN
   IF p_po_header_id IS NOT NULL
   THEN
      l_po_header_id := p_po_header_id;

      l_sec := 'Get PO Details';

      BEGIN
         SELECT revision_num,
                vendor_site_id,
                approved_flag,
                type_lookup_code
           INTO l_po_rev_latest,
                l_vendor_site_id,
                l_approved_flag,
                l_po_type
           FROM po_headers
          WHERE po_header_id = l_po_header_id;

         l_po_rev := 0;


         l_sec := 'Get Default communication method for Revision 0';

         BEGIN
            SELECT supplier_notif_method,
                   (SELECT edi_flag
                      FROM ece_tp_details
                     WHERE     tp_header_id = aps.tp_header_id
                           AND document_id = 'POO'
                           AND l_po_type = 'STANDARD')
                      edi_flag,
                   NVL (
                      (SELECT attribute1
                         FROM ece_tp_details
                        WHERE     tp_header_id = aps.tp_header_id
                              AND document_id = 'POO'
                              AND edi_flag = 'Y'
                              AND l_po_type = 'STANDARD'),
                      'N')
                      edi_ack_flag
              INTO l_po_comm_default, l_edi_flag, l_edi_ack_flag
              FROM ap_supplier_sites aps
             WHERE vendor_site_id = l_vendor_site_id;

            IF NVL (l_edi_flag, 'N') = 'Y'
            THEN
               l_po_comm_default := 'EDI';
               l_edi_flag := NULL;
            END IF;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_po_comm_default := l_others;
         END;

         l_sec := 'Get Actual communication method for Revision 0';

         IF NVL (l_approved_flag, 'N') <> 'Y' AND l_po_rev_latest = 0
         THEN
            l_po_comm_actual := NULL;
         ELSE
            l_sec :=
               'Check if EDI is the actual communication method and Date ';

            BEGIN
               SELECT 'EDI', printed_date, approved_date
                 INTO l_po_comm_actual, l_po_comm_date, l_approved_date
                 FROM po_headers_archive
                WHERE     po_header_id = l_po_header_id
                      AND revision_num = l_po_rev
                      AND edi_processed_flag = 'Y';

               IF l_po_comm_default <> 'EDI' AND l_po_comm_actual = 'EDI'
               THEN
                  l_po_comm_actual := l_po_comm_default;
                  l_po_comm_date := NULL;
               END IF;

               IF TRUNC (l_po_comm_date) > TRUNC (l_approved_date)
               THEN
                  BEGIN
                     SELECT xml_or_edi, creation_date
                       INTO l_po_comm_actual, l_po_comm_date
                       FROM XXWC_PO_COMM_HIST xpc
                      WHERE     1 = 1
                            AND po_header_id = l_po_header_id
                            AND xml_or_edi = 'EDI'
                            AND creation_date =
                                   (SELECT MAX (creation_date)
                                      FROM XXWC_PO_COMM_HIST
                                     WHERE     po_header_id =
                                                  xpc.po_header_id
                                           AND creation_date <=
                                                  NVL (
                                                     (SELECT revised_date
                                                        FROM po_headers_archive_all
                                                       WHERE     po_header_id =
                                                                    xpc.po_header_id
                                                             AND revision_num =
                                                                    1
                                                             AND NVL (
                                                                    Cancel_flag,
                                                                    'N') =
                                                                    'N'),
                                                     creation_date));
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        l_po_comm_actual := NULL;
                        l_po_comm_date := NULL;
                  END;
               END IF;
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_po_comm_actual := NULL;
                  l_po_comm_date := NULL;
            END;

            IF l_po_comm_actual IS NULL
            THEN
               l_sec := 'Get Overriden communication method';

               BEGIN
                  SELECT NVL (
                               DECODE (
                                  fax_number,
                                  NULL, NULL,
                                     'FAX'
                                  || DECODE (email_address, NULL, NULL, '/'))
                            || DECODE (email_address, NULL, NULL, 'EMAIL'),
                            l_others),
                         creation_date
                    INTO l_po_comm_actual, l_po_comm_date
                    FROM XXWC_PO_COMM_HIST xpc
                   WHERE     1 = 1
                         AND po_header_id = l_po_header_id
                         AND xml_or_edi <> 'EDI'
                         AND creation_date =
                                (SELECT MAX (creation_date)
                                   FROM XXWC_PO_COMM_HIST
                                  WHERE     po_header_id = xpc.po_header_id
                                        AND creation_date <=
                                               NVL (
                                                  (SELECT revised_date
                                                     FROM po_headers_archive_all
                                                    WHERE     po_header_id =
                                                                 xpc.po_header_id
                                                          AND revision_num =
                                                                 1
                                                          AND NVL (
                                                                 Cancel_flag,
                                                                 'N') = 'N'),
                                                  creation_date));
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     l_po_comm_actual := NULL;
                     l_po_comm_date := NULL;
               END;
            END IF;
         END IF;

         l_sec := 'Get Expected confirmation method for Revision 0';

         IF    (NVL (l_po_comm_actual, 'XX') = 'EDI' AND l_edi_ack_flag = 'Y')
            OR (l_po_comm_actual IS NULL)
         THEN
            l_po_ack_exp := l_po_comm_actual;
         ELSE
            l_po_ack_exp := l_others;
         END IF;

         l_sec := 'Get actual EDI Acknowledgment date for Revision 0';

         IF l_po_ack_exp = 'EDI'
         THEN
            BEGIN
               SELECT MAX (creation_date)
                 INTO l_po_ack_date
                 FROM PO_ACCEPTANCES
                WHERE     po_header_id = l_po_header_id
                      AND revision_num = l_po_rev;
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_po_ack_date := NULL;
            END;
         ELSE
            l_po_ack_date := NULL;
         END IF;


         l_sec := 'Get communication methods for Revision > 0';

         IF l_po_rev_latest > 0
         THEN
            l_sec := 'Get Default communication method for Revision > 0';

            BEGIN
               SELECT supplier_notif_method,
                      (SELECT edi_flag
                         FROM ece_tp_details
                        WHERE     tp_header_id = aps.tp_header_id
                              AND document_id = 'POCO'
                              AND l_po_type = 'STANDARD')
                         edi_flag
                 INTO l_po_change_comm_default, l_edi_flag
                 FROM ap_supplier_sites aps
                WHERE vendor_site_id = l_vendor_site_id;

               IF NVL (l_edi_flag, 'N') = 'Y'
               THEN
                  l_po_change_comm_default := 'EDI';
                  l_edi_flag := NULL;
               END IF;
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_po_change_comm_default := l_others;
            END;

            l_sec := 'Get Actual communication method for Revision > 0';


            IF NVL (l_approved_flag, 'N') <> 'Y'
            THEN
               l_po_change_comm_actual := NULL;
            ELSE
               l_sec :=
                  'Check if EDI is the actual communication method and Date ';
               l_approved_date := NULL;

               -- Added for Revision 1.1,  changed the valdiation sequence
               --Begin <<
               BEGIN
                  SELECT xml_or_edi, creation_date
                    INTO l_po_change_comm_actual, l_po_change_comm_date
                    FROM XXWC_PO_COMM_HIST xpc
                   WHERE     1 = 1
                         AND po_header_id = l_po_header_id
                         AND xml_or_edi = 'EDI'
                         AND creation_date =
                                (SELECT MAX (creation_date)
                                   FROM XXWC_PO_COMM_HIST
                                  WHERE po_header_id = xpc.po_header_id);

                  BEGIN
                     SELECT 'EDI', printed_date, approved_date
                       INTO l_po_change_comm_actual,
                            l_po_change_comm_date,
                            l_approved_date
                       FROM po_headers_archive
                      WHERE     po_header_id = l_po_header_id
                            AND revision_num = l_po_rev_latest
                            AND edi_processed_flag = 'Y';

                     IF     l_po_change_comm_default <> 'EDI'
                        AND l_po_change_comm_actual = 'EDI'
                     THEN
                        l_po_change_comm_actual := l_po_change_comm_default;
                        l_po_change_comm_date := NULL;
                     END IF;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        l_po_change_comm_actual := NULL;
                        l_po_change_comm_date := NULL;
                  END;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     l_po_change_comm_actual := NULL;
                     l_po_change_comm_date := NULL;
               END;

               --End >>

               --Commmented for Revision 1.1 , to change the valdiation sequence
               /*
                              BEGIN
                                 SELECT 'EDI', printed_date, approved_date
                                   INTO l_po_change_comm_actual,
                                        l_po_change_comm_date,
                                        l_approved_date
                                   FROM po_headers_archive
                                  WHERE     po_header_id = l_po_header_id
                                        AND revision_num = l_po_rev_latest
                                        AND edi_processed_flag = 'Y';

                                 IF     l_po_change_comm_default <> 'EDI'
                                    AND l_po_change_comm_actual = 'EDI'
                                 THEN
                                    l_po_change_comm_actual := l_po_change_comm_default;
                                    l_po_change_comm_date := NULL;
                                 END IF;
                              EXCEPTION
                                 WHEN OTHERS
                                 THEN
                                    l_po_change_comm_actual := NULL;
                                    l_po_change_comm_date := NULL;
                              END;


                              IF TRUNC (l_po_change_comm_date) > TRUNC (l_approved_date)
                              THEN
                                 BEGIN
                                    SELECT xml_or_edi, creation_date
                                      INTO l_po_change_comm_actual, l_po_change_comm_date
                                      FROM XXWC_PO_COMM_HIST xpc
                                     WHERE     1 = 1
                                           AND po_header_id = l_po_header_id
                                           AND xml_or_edi = 'EDI'
                                           AND creation_date =
                                                  (SELECT MAX (creation_date)
                                                     FROM XXWC_PO_COMM_HIST
                                                    WHERE po_header_id = xpc.po_header_id);
                                 EXCEPTION
                                    WHEN OTHERS
                                    THEN
                                       l_po_change_comm_actual := NULL;
                                       l_po_change_comm_date := NULL;
                                 END;
                              END IF;

               */
               IF l_po_change_comm_actual IS NULL
               THEN
                  BEGIN
                     SELECT NVL (
                                  DECODE (
                                     fax_number,
                                     NULL, NULL,
                                        'FAX'
                                     || DECODE (email_address,
                                                NULL, NULL,
                                                '/'))
                               || DECODE (email_address, NULL, NULL, 'EMAIL'),
                               l_others),
                            creation_date
                       INTO l_po_change_comm_actual, l_po_change_comm_date
                       FROM XXWC_PO_COMM_HIST xpc
                      WHERE     1 = 1
                            AND po_header_id = l_po_header_id
                            AND xml_or_edi <> 'EDI'
                            AND creation_date =
                                   (SELECT MAX (creation_date)
                                      FROM XXWC_PO_COMM_HIST
                                     WHERE po_header_id = xpc.po_header_id);
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        l_po_change_comm_actual := NULL;
                        l_po_change_comm_date := NULL;
                  END;
               END IF;
            END IF;

            l_sec := 'Get Expected confirmation method for Revision > 0';


            IF    (    NVL (l_po_change_comm_actual, 'XX') = 'EDI'
                   AND l_edi_ack_flag = 'Y')
               OR (l_po_change_comm_actual IS NULL)
            THEN
               l_po_change_ack_exp := l_po_change_comm_actual;
            ELSE
               l_po_change_ack_exp := l_others;
            END IF;


            l_sec := 'Get actual EDI Acknowledgment date for Revision > 0';


            IF l_po_change_ack_exp = 'EDI'
            THEN
               BEGIN
                  SELECT MAX (creation_date)
                    INTO l_po_change_ack_date
                    FROM PO_ACCEPTANCES
                   WHERE     po_header_id = l_po_header_id
                         AND revision_num = l_po_rev_latest;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     l_po_change_ack_date := NULL;
               END;
            ELSE
               l_po_change_ack_date := NULL;
            END IF;
         ELSIF l_po_rev_latest = 0
         THEN
            l_po_rev_latest := NULL;
         END IF;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_po_rev_latest := NULL;
      END;
   END IF;

   l_sec := 'Return Values to Out Parameters';

   l_po_change_comm_date := NULL; -- Date PO sent is not required for PO Changes
   p_po_rev := l_po_rev;
   p_po_comm_default := l_po_comm_default;
   p_po_comm_actual := l_po_comm_actual;
   p_po_ack_exp := l_po_ack_exp;
   p_po_comm_date := l_po_comm_date;
   p_po_ack_date := l_po_ack_date;
   p_po_rev_latest := l_po_rev_latest;
   p_po_change_comm_default := l_po_change_comm_default;
   p_po_change_comm_actual := l_po_change_comm_actual;
   p_po_change_ack_exp := l_po_change_ack_exp;
   p_po_change_comm_date := l_po_change_comm_date;
   p_po_change_ack_date := l_po_change_ack_date;
EXCEPTION
   WHEN OTHERS
   THEN
      p_po_rev := l_po_rev;
      p_po_comm_default := l_po_comm_default;
      p_po_comm_actual := l_po_comm_actual;
      p_po_ack_exp := l_po_ack_exp;
      p_po_comm_date := l_po_comm_date;
      p_po_ack_date := l_po_ack_date;
      p_po_rev_latest := l_po_rev_latest;
      p_po_change_comm_default := l_po_change_comm_default;
      p_po_change_comm_actual := l_po_change_comm_actual;
      p_po_change_ack_exp := l_po_change_ack_exp;
      p_po_change_comm_date := l_po_change_comm_date;
      p_po_change_ack_date := l_po_change_ack_date;


      xxcus_error_pkg.xxcus_error_main_api (
         p_called_from         => 'XXWC_PO_ACK_METHOD',
         p_calling             => l_sec,
         p_request_id          => NULL,
         p_ora_error_msg       => SUBSTR (
                                       ' Error_Stack...'
                                    || DBMS_UTILITY.format_error_stack ()
                                    || ' Error_Backtrace...'
                                    || DBMS_UTILITY.format_error_backtrace ()
                                    || SQLERRM,
                                    1,
                                    2000),
         p_error_desc          => SUBSTR (SQLERRM, 1, 240),
         p_distribution_list   => l_distribution_list,
         p_module              => 'PO');
END;
/
