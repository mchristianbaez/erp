
  CREATE OR REPLACE PROCEDURE "APPS"."XXCUS_TAXWARE_UPDATE_PRC" (errbuf     OUT VARCHAR2
                                                    ,retcode    OUT NUMBER
                                                    ,p_fromdate IN DATE
                                                    ,p_todate   IN DATE) IS

  /*******************************************************************************
  * Procedure:   xxcus_taxware_update
  * Description: This procedure is a one time data update.  Columns were added to
                 table for DocLink and now it's being used for other reporting
                 so need to up existing data with the additional columns.
  HISTORY
  ===============================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     16-May-2013        Kathy Poling    Initial creation of the procedure
                                             SR 194888
  ********************************************************************************/
  --Intialize Variables
  l_err_msg       VARCHAR2(2000);
  l_err_code      NUMBER;
  l_sec           VARCHAR2(500);
  l_req_id        NUMBER;
  l_err_callfrom  VARCHAR2(75) := 'XXCUS_TAXWARE_UPDATE_PRC';
  l_distro_list   VARCHAR2(75) DEFAULT 'hdsoracledevelopers@hdsupply.com';
  l_count         NUMBER := 0;
  l_grouping_rule NUMBER := 1001; --ORDER MANAGEMENT

BEGIN
  FOR c1 IN (SELECT rcta.customer_trx_id
                   ,rcta.creation_date
                   ,(CASE
                      WHEN rbsa.name LIKE 'PRISM%' AND
                           rcta.ct_reference =
                           ltrim(rcta.purchase_order, '0') THEN
                       NULL
                      ELSE
                       rcta.purchase_order
                    END) po_number
                   ,xxwcar_taxware_pkg.get_trx_salesrep_num(rcta.primary_salesrep_id
                                                           ,hcsu_bill.primary_salesrep_id
                                                           ,rcta.org_id) account_manager_num
                   ,xxwc_mv_routines_pkg.get_bt_trx_salesrep(rcta.primary_salesrep_id
                                                            ,hcsu_bill.primary_salesrep_id
                                                            ,rcta.org_id) account_manager
                   ,(CASE
                      WHEN rbsa.grouping_rule_id = l_grouping_rule THEN
                       (SELECT xxwc_mv_routines_pkg.get_user_employee(ooha.created_by)
                          FROM oe_order_headers_all ooha
                         WHERE ooha.order_number =
                               rcta.interface_header_attribute1
                           AND rownum = 1)
                      WHEN rbsa.name LIKE 'PRISM%' THEN
                       rcta.interface_header_attribute10
                      ELSE
                       NULL
                    END) taken_by
                   ,hla.location_code whs_branch_name
                   ,(CASE
                      WHEN rbsa.grouping_rule_id = l_grouping_rule THEN
                       rcta.interface_header_attribute1
                      ELSE
                       (SELECT sales_order
                          FROM ra_customer_trx_lines_all rctla
                         WHERE rctla.customer_trx_id = rcta.customer_trx_id
                           AND rctla.sales_order IS NOT NULL
                           AND rownum = 1)
                    END) order_number
                   ,(CASE
                      WHEN rbsa.grouping_rule_id = l_grouping_rule THEN
                       (SELECT ooha.ordered_date --to_char(ooha.ordered_date, 'MM/DD/YYYY')
                          FROM oe_order_headers_all ooha
                         WHERE ooha.order_number =
                               rcta.interface_header_attribute1
                           AND rownum = 1)
                      ELSE
                       (SELECT sales_order_date --to_char(sales_order_date, 'MM/DD/YYYY')
                          FROM ra_customer_trx_lines_all rctla
                         WHERE rctla.customer_trx_id = rcta.customer_trx_id
                           AND rctla.sales_order_date IS NOT NULL
                           AND rownum = 1)
                    END) order_date
                   ,(CASE
                      WHEN rctta.type = 'CM' THEN
                       'IMMEDIATE'
                      ELSE
                       rtt.description
                    END) terms
                   ,(CASE
                      WHEN rbsa.grouping_rule_id = l_grouping_rule THEN
                       (SELECT wcsm.ship_method_code_meaning
                          FROM oe_order_headers_all       ooha
                              ,wsh_carrier_ship_methods_v wcsm
                         WHERE ooha.order_number =
                               rcta.interface_header_attribute1
                           AND ooha.shipping_method_code =
                               wcsm.ship_method_code
                           AND rownum = 1)
                      WHEN rbsa.name LIKE 'PRISM%' THEN
                       rcta.interface_header_attribute12
                      ELSE
                       rcta.ship_via
                    END) ship_via
               FROM ar.ra_customer_trx_all      rcta
                   ,ar.ar_payment_schedules_all apsa
                    -- Bill To Data
                   ,ar.hz_cust_accounts       hca_bill
                   ,ar.hz_parties             hp_bill
                   ,ar.hz_cust_site_uses_all  hcsu_bill
                   ,ar.hz_cust_acct_sites_all hcasa_bill
                   ,ar.hz_party_sites         hps_bill
                   ,ar.hz_locations           hl_bill
                    -- Ship To Data
                   ,ar.hz_cust_accounts       hca_ship
                   ,ar.hz_parties             hp_ship
                   ,ar.hz_cust_site_uses_all  hcsu_ship
                   ,ar.hz_cust_acct_sites_all hcasa_ship
                   ,ar.hz_party_sites         hps_ship
                   ,ar.hz_locations           hl_ship
                    -- Invoice Data
                   ,ar.ra_terms_tl            rtt
                   ,ar.ra_cust_trx_types_all  rctta
                   ,ra_batch_sources_all      rbsa
                   ,hr.hr_locations_all       hla
                   ,hr_all_organization_units haou
              WHERE rcta.org_id = 162
                AND rcta.creation_date BETWEEN p_fromdate AND p_todate
                AND rcta.customer_trx_id = apsa.customer_trx_id
                   -- Bill to Data
                AND rcta.bill_to_customer_id = hca_bill.cust_account_id
                AND hca_bill.party_id = hp_bill.party_id
                AND rcta.bill_to_site_use_id = hcsu_bill.site_use_id
                AND hca_bill.cust_account_id = hcasa_bill.cust_account_id
                AND hcasa_bill.cust_acct_site_id =
                    hcsu_bill.cust_acct_site_id
                AND hcsu_bill.site_use_code = 'BILL_TO'
                AND hcasa_bill.party_site_id = hps_bill.party_site_id
                AND hps_bill.location_id = hl_bill.location_id
                   -- Ship To Data
                AND rcta.ship_to_customer_id = hca_ship.cust_account_id(+)
                AND hca_ship.party_id = hp_ship.party_id(+)
                AND rcta.ship_to_site_use_id = hcsu_ship.site_use_id(+)
                AND hcsu_ship.cust_acct_site_id =
                    hcasa_ship.cust_acct_site_id(+)
                AND hcasa_ship.party_site_id = hps_ship.party_site_id(+)
                AND hps_ship.location_id = hl_ship.location_id(+)
                   -- Invoice Data      --Version 1.1 ended
                AND rcta.term_id = rtt.term_id(+)
                AND rtt.language(+) = 'US'
                AND rcta.cust_trx_type_id = rctta.cust_trx_type_id
                AND rcta.org_id = rctta.org_id
                   --AND rcta.batch_source_id in (1003, 1008, 1009)
                AND rcta.batch_source_id = rbsa.batch_source_id
                AND rcta.org_id = rbsa.org_id
                AND ((rbsa.name NOT LIKE 'PRISM%' AND
                    nvl(rcta.interface_header_attribute10
                         ,(SELECT to_char(organization_id)
                            FROM mtl_parameters
                           WHERE organization_code = 'WCC')) =
                    to_char(haou.organization_id) AND
                    haou.location_id = hla.location_id) OR
                    (rbsa.name LIKE 'PRISM%' AND
                    nvl(lpad(rcta.interface_header_attribute7, 3, '0')
                         ,'WCC') =
                    substr(haou.name, 1, instr(haou.name, '-', 1, 1) - 2) AND
                    haou.location_id = hla.location_id))
                   --Version 1.1 ended
                AND rcta.customer_trx_id IN
                    (SELECT customer_trx_id
                       FROM xxwc.xxwcar_taxware_audit_tbl xtat
                      WHERE xtat.customer_trx_id = rcta.customer_trx_id
                        AND order_number IS NULL
                         OR po_number IS NULL
                         OR terms IS NULL
                         OR ship_via IS NULL))
  LOOP
  
    l_count := l_count + 1;
  
    UPDATE xxwc.xxwcar_taxware_audit_tbl
       SET po_number           = c1.po_number
          ,account_manager_num = c1.account_manager_num
          ,account_manager     = c1.account_manager
          ,taken_by            = c1.taken_by
          ,whs_branch_name     = c1.whs_branch_name
          ,order_number        = c1.order_number
          ,order_date          = c1.order_date
          ,terms               = c1.terms
          ,ship_via            = c1.ship_via
     WHERE customer_trx_id = c1.customer_trx_id;
  
  END LOOP;
  COMMIT;
  dbms_output.put_line('Records update:  ' || l_count);

EXCEPTION

  WHEN OTHERS THEN
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
    l_err_code := 2;
    l_err_msg  := substr((l_err_msg || ' ERROR ' ||
                         substr(SQLERRM, 1, 2000))
                        ,1
                        ,3000);
    dbms_output.put_line(l_sec || ' - ' || l_err_msg);
    fnd_file.put_line(fnd_file.log, l_err_msg);
    fnd_file.put_line(fnd_file.output, l_err_msg);
  
    xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                        ,p_calling           => l_sec
                                        ,p_request_id        => l_req_id
                                        ,p_ora_error_msg     => substr(SQLERRM
                                                                      ,1
                                                                      ,2000)
                                        ,p_error_desc        => substr(l_err_msg
                                                                      ,1
                                                                      ,2000)
                                        ,p_distribution_list => l_distro_list
                                        ,p_module            => 'AR');
END;
;
