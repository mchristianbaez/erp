CREATE OR REPLACE PROCEDURE apps.XXWC_PO_NEW_COMMUNICATION (itemtype IN VARCHAR2,
                                  itemkey IN VARCHAR2,
                                  actid IN NUMBER,
                                  funcmode IN VARCHAR2,
                                  resultout OUT NOCOPY VARCHAR2) is
 
    /*******************************************************************************
    
    Procedure Name: XXWC_PO_NEW_COMMUNICATION
    
    Script Owners: Lucidity Consulting Group.
    
    Client: White Cap
    
    Description: Procedure defined for workflow XXWC_POAPPRV to prevent execution of PO Output For Communication with Argument17 set to View
    
    History:
    Version   Date          Author          Description
    ----------------------------------------------------------
    1.0       12-Jun-2013   Lee Spitzer      Initial development.
    
    *******************************************************************************/
 
  x_progress varchar2(100);
  l_document_subtype po_headers.type_lookup_code%TYPE;
  l_document_type po_headers.type_lookup_code%TYPE;


  g_po_wf_debug VARCHAR2(1) := NVL(FND_PROFILE.VALUE('PO_SET_DEBUG_WORKFLOW_ON'), 'N');
  
  Begin

    x_progress := 'XXWC_PO_NEW_COMMUNICATION';

    IF (g_po_wf_debug = 'Y') THEN
      PO_WF_DEBUG_PKG.insert_debug(itemtype, itemkey, x_progress);
    END IF;


  -- <Bug 4100416 Start>: Do nothing in cancel or timeout modes.
    IF (funcmode <> wf_engine.eng_run)
      THEN
      resultout := wf_engine.eng_null;
      return;
    END IF;
  -- <Bug 4100416 End>


--Get the document type

    l_document_type := PO_WF_UTIL_PKG.GetItemAttrText (itemtype => itemtype,
                                                       itemkey => itemkey,
                                                       aname => 'DOCUMENT_TYPE');


    l_document_subtype := PO_WF_UTIL_PKG.GetItemAttrText (itemtype => itemtype,
                                                          itemkey => itemkey,
                                                          aname => 'DOCUMENT_SUBTYPE');

    x_progress := 'PO_COMMUNICATION_PVT.PO_NEW_COMMUNICATION: Verify whether XDO Product is installed or not';

    IF (g_po_wf_debug = 'Y') THEN
      PO_WF_DEBUG_PKG.insert_debug(itemtype, itemkey, x_progress);
    END IF;


    IF PO_COMMUNICATION_PVT.PO_COMMUNICATION_PROFILE = 'T' THEN
      IF l_document_type in ('PO', 'PA') and l_document_subtype in ('STANDARD', 'BLANKET', 'CONTRACT')
        OR (l_document_type = 'RELEASE' AND l_document_subtype = 'BLANKET' ) THEN
        --resultout := wf_engine.eng_completed || ':' || 'Y'; --Standard Functionaity Alway return
        resultout := wf_engine.eng_completed || ':' || 'N';
      ELSE
        resultout := wf_engine.eng_completed || ':' || 'N';
      END IF;

    Else
      resultout := wf_engine.eng_completed || ':' || 'N';
    END IF;

  EXCEPTION

    WHEN OTHERS THEN
      x_progress := 'XXWC_PO_NEW_COMMUNICATION: In Exception handler';
      IF (g_po_wf_debug = 'Y') THEN
        PO_WF_DEBUG_PKG.insert_debug(itemtype, itemkey, x_progress);
      END IF;
      wf_core.context('PO_COMMUNICATION_PVT', 'PO_NEW_COMMUNICATION', x_progress);
      raise;

  END XXWC_PO_NEW_COMMUNICATION;
