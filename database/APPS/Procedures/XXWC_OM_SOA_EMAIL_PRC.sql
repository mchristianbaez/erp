CREATE OR REPLACE PROCEDURE APPS.XXWC_OM_SOA_EMAIL_PRC (
   p_to_email_addr   IN     VARCHAR2,
   p_cc_email_addr   IN     VARCHAR2,
   p_request_id      IN     NUMBER,
   x_to_email_addr      OUT VARCHAR2,
   x_cc_email_addr      OUT VARCHAR2)
IS
   /*******************************************************************************
     * Procedure:   XXWC_OM_SOA_EMAIL_PRC
     * Description: This procedure is calling from Sales Receipt RDF
     HISTORY
     ===============================================================================
     VERSION DATE               AUTHOR(S)       DESCRIPTION
     ------- -----------------  --------------- -----------------------------------------
     1.0     21-Mar-2018        P.Vamshidhar    Initial creation of the procedure
                                                TMS# 20180320-00096
     ********************************************************************************/
   PROCEDURE email (p_email_add   IN     VARCHAR2,
                    x_email_add      OUT VARCHAR2,
                    p_flag               VARCHAR2)
   IS
      CURSOR CUR_EMAIL
      IS
         SELECT DISTINCT a.EMAIL_ADDRESS email_address
           FROM XXWC.XXWC_OM_SOA_EMAIL_GTT a
          WHERE a.FLAG = p_flag AND p_flag = 'TO'
         UNION ALL
         SELECT DISTINCT a.EMAIL_ADDRESS email_address
           FROM XXWC.XXWC_OM_SOA_EMAIL_GTT a
          WHERE     FLAG = p_flag
                AND p_flag = 'CC'
                AND NOT EXISTS
                       (SELECT 1
                          FROM XXWC.XXWC_OM_SOA_EMAIL_GTT b
                         WHERE     b.EMAIL_ADDRESS = a.email_address
                               AND b.flag = 'TO');


      from_email   VARCHAR2 (1000);
      new_email    VARCHAR2 (1000);
      l_email      VARCHAR2 (1000);
      ln_cnt       NUMBER := 1;
   BEGIN
      from_email := p_email_add;
      from_email := REPLACE (from_email, ';', ',');

      LOOP
         EXIT WHEN ln_cnt = 0;
         l_email := NULL;
         l_email := SUBSTR (from_email, 1, INSTR (from_email, ',', 1) - 1);

         INSERT INTO XXWC.XXWC_OM_SOA_EMAIL_GTT (EMAIL_ADDRESS, FLAG)
              VALUES (l_email, P_FLAG);

         ln_cnt := ln_cnt + 1;
         from_email := SUBSTR (from_email, INSTR (from_email, ',', 1) + 1);

         IF INSTR (from_email, ',', 1) = 0
         THEN
            INSERT INTO XXWC.XXWC_OM_SOA_EMAIL_GTT (EMAIL_ADDRESS, flag)
                 VALUES (from_email, p_flag);

            ln_cnt := 0;
         END IF;
      END LOOP;

      new_email := NULL;

      FOR REC_EMAIL IN CUR_EMAIL
      LOOP
         IF new_email IS NOT NULL
         THEN
            new_email := new_email || ',' || REC_EMAIL.EMAIL_ADDRESS;
         ELSE
            new_email := REC_EMAIL.EMAIL_ADDRESS;
         END IF;
      END LOOP;

      x_email_add := new_email;
   EXCEPTION
      WHEN OTHERS
      THEN
         x_email_add := p_email_add;
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'xxwc_om_soa_email_prc',
            p_calling             => 'xxwc_om_soa_email_prc child procedure',
            p_request_id          => p_request_id,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          => 'p_email_add:' || p_email_add,
            p_distribution_list   => 'WC-ITDEVALERTS-U1@HDSupply.com',
            p_module              => 'XXWC');
   END;
BEGIN
   IF p_to_email_addr IS NOT NULL
   THEN
      email (LOWER (p_to_email_addr), x_to_email_addr, 'TO');
   END IF;

   IF p_cc_email_addr IS NOT NULL
   THEN
      email (LOWER (p_cc_email_addr), x_cc_email_addr, 'CC');
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      xxcus_error_pkg.xxcus_error_main_api (
         p_called_from         => 'xxwc_om_soa_email_prc',
         p_calling             => 'xxwc_om_soa_email_prc',
         p_request_id          => p_request_id,
         p_ora_error_msg       => SQLERRM,
         p_error_desc          =>    'To email:'
                                  || p_to_email_addr
                                  || ' cc Email:'
                                  || p_cc_email_addr,
         p_distribution_list   => 'WC-ITDEVALERTS-U1@HDSupply.com',
         p_module              => 'XXWC');
      x_to_email_addr := p_to_email_addr;
      x_cc_email_addr := p_cc_email_addr;
END;
/