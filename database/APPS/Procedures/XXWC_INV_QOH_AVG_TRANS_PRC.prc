CREATE OR REPLACE 
PROCEDURE  APPS.XXWC_INV_QOH_AVG_TRANS_PRC(
          p_from_item_id    IN     NUMBER,
          p_from_item       IN     VARCHAR2,
          p_to_item_id      IN     NUMBER,
          p_to_item         IN     VARCHAR2,
          p_org_code        IN     VARCHAR2,
          p_cus_conv_rate   IN     NUMBER,
		  p_frm_std_rate    IN     NUMBER,
		  p_to_std_rate     IN     NUMBER,
          o_ret_msg         OUT    VARCHAR2)
IS

/*******************************************************************************
  * Procedure:   XXWC_INV_QOH_AVG_TRANS_PRC
  * Description: This procedure is calling from TRANSFER_BUTTON in
                 XXWC_INV_ITEM_AVG_ONH_TRANS.fmb.
  HISTORY
  ===============================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     10-Mar-2018        P.Vamshidhar    Initial creation of the procedure
                                             TMS#20171024-00015
  ********************************************************************************/
  
   CURSOR cur_validrec_stg
   IS
        SELECT msib.inventory_item_id,
               msib.organization_id,
               msib.primary_uom_code,
               mp.organization_code,
               msib.segment1,
               SUM (NVL (moq.transaction_quantity, 0)) transaction_quantity
          FROM apps.mtl_system_items_b msib,
               apps.mtl_onhand_quantities moq,
               apps.mtl_parameters mp
         WHERE     msib.inventory_item_id = moq.inventory_item_id
               AND msib.organization_id = moq.organization_id
               AND msib.inventory_item_id = p_from_item_id
               AND moq.subinventory_code = 'General'
               AND msib.organization_id = mp.organization_id
               AND EXISTS
                      (SELECT 1
                         FROM XXWC.XXWC_INV_QOH_AVG_TMP XIQA
                        WHERE XIQA.ORG_CODE = MP.ORGANIZATION_CODE)
      GROUP BY msib.inventory_item_id,
               msib.organization_id,
               msib.primary_uom_code,
               mp.organization_code,
               msib.segment1;

   CURSOR cur_org_list
   IS
      SELECT org_code FROM XXWC.XXWC_INV_QOH_AVG_TMP;


   ln_process_flag               NUMBER := 1;
   ln_lock_flag                  NUMBER := 2;
   lvc_source                    VARCHAR2 (100) := 'TYPE 14';
   ln_user_id                    FND_USER.USER_ID%TYPE := fnd_profile.VALUE ('USER_ID');
   ln_transaction_type_id        NUMBER;
   ln_source_line_id             NUMBER := 99;
   ln_source_header_id           NUMBER := 99;
   ln_transaction_mode           NUMBER := 3;
   ln_accts_pay_code_comb_id     GL_CODE_COMBINATIONS.CODE_COMBINATION_ID%TYPE;
   ln_trans_action_id            NUMBER;
   ln_from_item_cost             NUMBER;
   ln_to_item_cost               NUMBER;
   ln_go_item_avg_cost           NUMBER;
   lvc_org_list                  VARCHAR2 (32767);
   lvc_org_code                  MTL_PARAMETERS.ORGANIZATION_CODE%TYPE;
   ln_uom_rate                   NUMBER;
   ln_from_item_qty              NUMBER;
   ln_to_item_qty                NUMBER := 0;
   lvc_new_acct                  VARCHAR2 (10) := '501010';
   lvc_transaction_source        VARCHAR2 (100) := 'TYPE 14';
   lvc_ret_msg                   VARCHAR2 (32767);
   ln_transaction_header_id_ex   VARCHAR2 (100)
                                    := TO_CHAR (SYSDATE, 'DDHH24MISS');
   lvc_to_item_uom               VARCHAR2 (100);
   lvc_err_msg                   VARCHAR2 (32767);
   lvc_to_segment1               MTL_SYSTEM_ITEMS_B.SEGMENT1%TYPE;
   ln_tran_interface_id          NUMBER;
   ln_transaction_source_id      NUMBER;
   ln_trans_source_type_id       NUMBER;
   ln_count                      NUMBER := 0;
   lvc_msg_data                  VARCHAR2 (32767);
   ln_msg_count                  NUMBER;
   ln_retcode                    VARCHAR2 (1000);
   lvc_return_status             VARCHAR2 (10);
BEGIN
   EXECUTE IMMEDIATE ('truncate table XXWC.XXWC_INV_QOH_AVG_TMP');

   lvc_org_list := p_org_code;

   -- Inserting orgs info into temp table.
   IF lvc_org_list IS NOT NULL
   THEN
      LOOP
         IF NVL (INSTR (lvc_org_list, ',', 1), 0) = 0
         THEN
            lvc_org_code := lvc_org_list;

            INSERT INTO XXWC.XXWC_INV_QOH_AVG_TMP (ORG_CODE)
                 VALUES (lvc_org_code);

            lvc_org_list := NULL;
         ELSE
            lvc_org_code :=
               SUBSTR (lvc_org_list, 1, INSTR (lvc_org_list, ',', 1) - 1);
            lvc_org_list :=
               SUBSTR (lvc_org_list, INSTR (lvc_org_list, ',', 1) + 1);

            INSERT INTO XXWC.XXWC_INV_QOH_AVG_TMP (ORG_CODE)
                 VALUES (lvc_org_code);
         END IF;

         EXIT WHEN lvc_org_list IS NULL;
      END LOOP;
   ELSE
     INSERT INTO XXWC.XXWC_INV_QOH_AVG_TMP (ORG_CODE)
         (SELECT DISTINCT ORGANIZATION_CODE
            FROM MTL_ONHAND_QUANTITIES A, APPS.MTL_PARAMETERS B
           WHERE A.ORGANIZATION_ID = B.ORGANIZATION_ID
             AND A.INVENTORY_ITEM_ID = p_from_item_id);
   END IF;


   -- Checking to and from item exist in all mentioned orgs.
   IF P_ORG_CODE IS NOT NULL
   THEN
      FOR rec_org_list IN cur_org_list
      LOOP
         BEGIN
            ln_count := 0;

            SELECT COUNT (1)
              INTO ln_count
              FROM mtl_system_items_b msib, apps.mtl_parameters mp
             WHERE     msib.organization_id = mp.organization_id
                   AND mp.organization_code = rec_org_list.org_code
                   AND msib.inventory_item_id = p_from_item_id;

            IF ln_count = 0
            THEN
               lvc_err_msg := lvc_err_msg||
                     '*'
                  || p_from_item
                  || ' Item not assinged to '
                  || rec_org_list.org_code
                  || '*';
            END IF;
         EXCEPTION
            WHEN OTHERS
            THEN
               lvc_err_msg := 
                     'Error occured: From Item assignment checking '
                  || SUBSTR (SQLERRM, 1, 250);
         END;


         BEGIN
            ln_count := 0;

            SELECT COUNT (1)
              INTO ln_count
              FROM mtl_system_items_b msib, apps.mtl_parameters mp
             WHERE     msib.organization_id = mp.organization_id
                   AND mp.organization_code = rec_org_list.org_code
                   AND msib.inventory_item_id = p_to_item_id;

            IF ln_count = 0
            THEN
               lvc_err_msg := lvc_err_msg||
                     '*'
                  || p_to_item
                  || ' Item not assinged to '
                  || rec_org_list.org_code
                  || '*';
            END IF;
         EXCEPTION
            WHEN OTHERS
            THEN
               lvc_err_msg :=
                     'Error occured: From Item assignment checking '
                  || SUBSTR (SQLERRM, 1, 250);
         END;
      END LOOP;

      IF lvc_err_msg IS NOT NULL
      THEN
         RAISE PROGRAM_ERROR;
      END IF;
   END IF;


   BEGIN
      SELECT PRIMARY_UOM_CODE, segment1
        INTO lvc_to_item_uom, lvc_to_segment1
        FROM apps.mtl_system_items_b
       WHERE inventory_item_id = p_to_item_id AND organization_id = 222;
   EXCEPTION
      WHEN OTHERS
      THEN
         lvc_err_msg :=
            'Error occured while checking to item UOM and Item Number';
         RAISE PROGRAM_ERROR;
   END;

   FOR rec_validrec_stg IN cur_validrec_stg
   LOOP
      ln_from_item_qty := rec_validrec_stg.transaction_quantity;
      ln_to_item_qty := 0;
      ln_go_item_avg_cost := 0;
      ln_uom_rate := NULL;

      IF rec_validrec_stg.primary_uom_code <> lvc_to_item_uom
      THEN
         ln_uom_rate :=
            inv_convert.inv_um_convert (p_from_item_id,
                                        5,
                                        ln_from_item_qty,
                                        rec_validrec_stg.primary_uom_code,
                                        lvc_to_item_uom,
                                        NULL,
                                        NULL);         

         IF (p_frm_std_rate IS NULL OR p_to_std_rate IS NULL) AND p_cus_conv_rate IS NULL
         THEN
            lvc_err_msg :=
                  'UOM conversion has not defined for'
               || rec_validrec_stg.primary_uom_code
               || ' to '
               || lvc_to_item_uom;
            RAISE PROGRAM_ERROR;
         END IF;
      END IF;
      
      ln_to_item_qty := ln_from_item_qty * NVL (ln_uom_rate, 1);
      ln_from_item_cost :=
         ROUND (
            cst_cost_api.get_item_cost (1,
                                        P_FROM_ITEM_ID,
                                        rec_validrec_stg.organization_id),
            5);
      ln_to_item_cost :=
         ROUND (
            cst_cost_api.get_item_cost (1,
                                        p_to_item_id,
                                        rec_validrec_stg.organization_id),
            5);

      IF ln_from_item_cost <> NVL (ln_to_item_cost, ln_from_item_cost)
      THEN
         ln_go_item_avg_cost :=
            ROUND (
                 (  ln_from_item_cost
                  + NVL (ln_to_item_cost, ln_from_item_cost))
               / 2,
               5);

         -- Average cost update.
         XXWC_INV_ITEM_AVGCOST_UPD_PKG.AVG_COST_UPDATE_PRC (
            p_org_code    => rec_validrec_stg.organization_code,
            p_account     => lvc_new_acct,
            p_Item_num    => rec_validrec_stg.segment1,
            p_cost_type   => 'MATERIAL',
            p_new_cost    => ln_go_item_avg_cost,
            o_ret_msg     => lvc_ret_msg);

         IF lvc_ret_msg IS NOT NULL
         THEN
            lvc_err_msg := lvc_ret_msg;
            RAISE PROGRAM_ERROR;
         END IF;
      END IF;

      IF NVL (ln_go_item_avg_cost, 0) = 0
      THEN
         ln_go_item_avg_cost := ln_from_item_cost;
      END IF;      

      -- Onhand transfer.
      ln_transaction_type_id := 31;

      BEGIN
         SELECT disposition_id, distribution_account
           INTO ln_transaction_source_id, ln_accts_pay_code_comb_id
           FROM mtl_generic_dispositions
          WHERE     organization_id = rec_validrec_stg.organization_id
            AND segment1 = lvc_source;
      EXCEPTION
         WHEN OTHERS
         THEN
            lvc_err_msg := 'Error occured while checking trx source';            
            RAISE PROGRAM_ERROR;
	  END;      

      ln_tran_interface_id := mtl_material_transactions_s.NEXTVAL;

      BEGIN
         INSERT INTO mtl_transactions_interface (transaction_interface_id,
                                                 transaction_header_id,
                                                 transaction_uom,
                                                 transaction_date,
                                                 source_code,
                                                 source_line_id,
                                                 source_header_id,
                                                 process_flag,
                                                 transaction_mode,
                                                 lock_flag,
                                                 locator_id,
                                                 last_update_date,
                                                 last_updated_by,
                                                 creation_date,
                                                 created_by,
                                                 inventory_item_id,
                                                 subinventory_code,
                                                 organization_id,
                                                 transaction_source_name,
                                                 transaction_source_id,
                                                 transaction_quantity,
                                                 primary_quantity,
                                                 transaction_type_id,
                                                 transaction_cost,
                                                 transaction_reference)
              VALUES (ln_tran_interface_id,        -- transaction_interface_id
                      ln_transaction_header_id_ex,
                      rec_validrec_stg.primary_uom_code,
                      SYSDATE,                              --transaction date
                      'Account alias',                           --source code
                      ln_source_line_id,                      --source line id
                      ln_source_header_id,                  --source header id
                      ln_process_flag,                          --process flag
                      ln_transaction_mode,                  --transaction mode
                      ln_lock_flag,                                --lock flag
                      NULL,                                       --locator id
                      SYSDATE,                              --last update date
                      ln_user_id,                            --last updated by
                      SYSDATE,                                 --creation date
                      ln_user_id,                                 --created by
                      p_from_item_id,                      --inventory item id
                      'General',                      --From subinventory code
                      rec_validrec_stg.organization_id,      --organization id
                      lvc_source,                         --transaction source
                      ln_transaction_source_id,        --transaction source id
                      NVL (rec_validrec_stg.transaction_quantity, 0) * -1, --transaction quantity
                      rec_validrec_stg.transaction_quantity, --Primary quantity
                      ln_transaction_type_id,            --transaction type id
                      ln_from_item_cost,
                      lvc_to_segment1);
      EXCEPTION
         WHEN OTHERS
         THEN
            lvc_err_msg :=
                  'Error occured: Inserting data into trx interface1 '
               || SUBSTR (SQLERRM, 1, 250);            
            RAISE PROGRAM_ERROR;
      END;
      

      ln_tran_interface_id := mtl_material_transactions_s.NEXTVAL;

      --getting transaction source and transaction id to issue
      ln_trans_source_type_id := NULL;
      ln_trans_action_id := NULL;

      BEGIN
         SELECT transaction_source_type_id, transaction_action_id
           INTO ln_trans_source_type_id, ln_trans_action_id
           FROM mtl_transaction_types
          WHERE transaction_type_id = 41;
      EXCEPTION
         WHEN OTHERS
         THEN
            ln_trans_source_type_id := NULL;
            ln_trans_action_id := NULL;           
      END;

     
      ln_transaction_type_id := 41;

      ln_to_item_qty := rec_validrec_stg.transaction_quantity;

      IF p_cus_conv_rate IS NOT NULL
      THEN
         ln_to_item_qty :=
            rec_validrec_stg.transaction_quantity / NVL (p_cus_conv_rate, 1);
      END IF;

      --- Inserting MTL_TRANSACTIONS_INTERFACE to receive items.

      BEGIN
         INSERT INTO mtl_transactions_interface (transaction_interface_id,
                                                 transaction_header_id,
                                                 transaction_uom,
                                                 transaction_date,
                                                 source_code,
                                                 source_line_id,
                                                 source_header_id,
                                                 process_flag,
                                                 transaction_mode,
                                                 lock_flag,
                                                 locator_id,
                                                 last_update_date,
                                                 last_updated_by,
                                                 creation_date,
                                                 created_by,
                                                 inventory_item_id,
                                                 subinventory_code,
                                                 organization_id,
                                                 transaction_source_name,
                                                 transaction_source_id,
                                                 transaction_quantity,
                                                 primary_quantity,
                                                 transaction_type_id,
                                                 transaction_cost)
              VALUES (ln_tran_interface_id,        -- transaction_interface_id
                      ln_transaction_header_id_ex,
                      lvc_to_item_uom,
                      SYSDATE,                              --transaction date
                      'Account alias',                           --source code
                      ln_source_line_id,                      --source line id
                      ln_source_header_id,                  --source header id
                      ln_process_flag,                          --process flag
                      ln_transaction_mode,                  --transaction mode
                      ln_lock_flag,                                --lock flag
                      NULL,                                       --locator id
                      SYSDATE,                              --last update date
                      ln_user_id,                            --last updated by
                      SYSDATE,                                 --creation date
                      ln_user_id,                                 --created by
                      p_to_item_id,                        --inventory item id
                      'General',                      --From subinventory code
                      rec_validrec_stg.organization_id,      --organization id
                      lvc_source,                         --transaction source
                      ln_transaction_source_id,        --transaction source id
                      ln_to_item_qty,                   --transaction quantity
                      ln_to_item_qty,                       --Primary quantity
                      ln_transaction_type_id,            --transaction type id
                      ln_go_item_avg_cost                 --transactional_cost
                                         );
      EXCEPTION
         WHEN OTHERS
         THEN
            lvc_err_msg :=
                  'Error occured: Inserting data into trx interface2 '
               || SUBSTR (SQLERRM, 1, 250);            
            RAISE PROGRAM_ERROR;
      END;
      
   END LOOP;

   -- Submitting Transaction Processor
 
EXCEPTION
   WHEN PROGRAM_ERROR
   THEN
      o_ret_msg := lvc_err_msg;
   WHEN OTHERS
   THEN
      o_ret_msg := SUBSTR (SQLERRM, 1, 250);
END;
/