
  CREATE OR REPLACE PROCEDURE "APPS"."XXCUSAP_IMPORT_WRAP_PRC" (p_group_id IN VARCHAR2, p_result OUT NUMBER,
                      P_BATCH_ERROR_FLAG OUT VARCHAR2,
                      P_INVOICES_FETCHED OUT NUMBER,
                      P_INVOICES_CREATED OUT NUMBER,
                      P_TOTAL_INVOICE_AMOUNT OUT NUMBER,
                      P_PRINT_BATCH OUT VARCHAR2
) IS
-- Purpose:
-- This is  a wrapper procedure to call apps.ap_import_invoices_pkg. This --procedure is mainly created to go
-- around AUTHID CURRENT_USER in ap_import_invoices_pkg so biztalk oracle user --can call import package through
-- this procedure
-- AUTHID CURRENT_USER prohibits call to this API from Biztalk user and hence --modified in the previous design.
-- This caused every patch removing the change and regular maintenance is a risk --factor.
-- Now with this wrapper procedure in APPS, which in turn calls the unmodified --API resolves the issue
--
--
--
/*
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     11/17/2011    Kathy Poling     Initial creation of the procedure
  ********************************************************************************/
    l_batch_error_flag        VARCHAR2(15);
    l_invoices_fetched        NUMBER;
    l_invoices_created        NUMBER;
    l_total_invoice_amount    NUMBER;
    l_print_batch             VARCHAR2(15) := 'N';
    l_org                CONSTANT hr_all_organization_units.organization_id%TYPE := 163; --HD Supply US GSC
    l_can_submit_request      BOOLEAN;

-- Error DEBUG
    l_err_Callfrom VARCHAR2(75) default 'xxcusap_import_wrap_prc';
    l_err_callpoint VARCHAR2(75) default 'FREIGHT INVOICE START';
    l_distro_list VARCHAR2(75) default 'HDSOracleDevelopers@hdsupply.com';


begin


         apps.Fnd_Client_Info.SET_ORG_CONTEXT(163);

         IF apps.AP_IMPORT_INVOICES_PKG.IMPORT_INVOICES(
                                NULL, --:p_batch_name
                                NULL, --:p_gl_date
                                NULL, --:p_hold_code
                                NULL, --:p_hold_reason
                                1000, --:p_commit_cycles
                                'DCTM',  --:p_source
                                p_group_id, --p_group_id
                                NULL,    --p_conc_request_id
                                'N',     -- :p_debug_switch
                                l_org,   -- p_org_id
                                l_batch_error_flag,  --OUT
                                l_invoices_fetched,   --OUT
                                l_invoices_created,  --OUT
                                l_total_invoice_amount,  --OUT
                                l_print_batch,
                                'FGHT from DCTM.',
                                NULL,  --p_invoice_interface_id
                                'N',   --p_needs_invoice_approval
                                'Y') THEN       --p_commit
                p_result := 1;
                else
                p_result := 0;
        END IF;
-- OUT Params
          P_BATCH_ERROR_FLAG := l_batch_error_flag;
          P_INVOICES_FETCHED := l_invoices_fetched;
          P_INVOICES_CREATED := l_invoices_created;
          P_TOTAL_INVOICE_AMOUNT := l_total_invoice_amount;
          P_PRINT_BATCH := l_print_batch;

COMMIT;

EXCEPTION
     WHEN OTHERS THEN
     p_result := 0;
     xxcus_error_pkg.xxcus_error_main_api(   p_called_from =>  l_err_Callfrom
          ,p_calling => l_err_callpoint||' Not completed'
          ,p_ora_error_msg => SQLERRM
          ,p_error_desc=> 'OTHER Exception in xxcusap_import_wrap_prc'
          ,p_module=>'AP'
          ,p_argument1 => p_group_id
          ,p_argument2 => p_result
          ,p_argument3 => l_invoices_fetched
          ,p_distribution_list => l_distro_list
          ,p_request_id => NULL );

END;
;
