CREATE OR REPLACE PROCEDURE APPS.XXCUS_GSC_FND_ASSIGN_ROLES (retcode out varchar2, errbuf out varchar2) as
/*
 -- Author: Balaguru Seshadri
 -- Concurrent Job: HDS GSC: Assign User Roles and Deactivate Direct Responsibilities
 -- Parameters: None
 -- Scope: As part of the GRC project, we need Development assistance in removing all direct responsibilities 
 --        except iExpense User, from a list of users that will be provided by us.  We'll also provide a list 
 --        of users and the roles they are to receive.
 --
 -- Tables used: xxcus.xxcus_gsc_assign_roles_all
 --
 -- Note: The above table is of type external and the file corresponding to the external table is
 --       located at /xx_iface/ebiz???/inbound/grc/roles with the name xxcus_gsc_assign_roles.txt
 --       The file is a text file and delimited by PIPE [ | ].
 --       The file will and only contain two columns NT_ID and ROLE DESCRIPTION
 --  
 -- Modification History
 -- ESMS         RFC     Date           Version   Comments
 -- =========== ======== ============== =======   ============================================================================================================
 -- 264967      42498    17-DEC-2014    1.0       Install.
 */ 
  --
  p_errbuf           varchar2(32767);
  p_retcode          varchar2(32767);
  p_expiration_date  date;
  --
  p_admin_user       varchar2(32767);
  p_start_date       date;
  --
  v_role_name        varchar2(150) :=Null;
  v_project_desc     varchar2(150) :='Project: 20268 Go Live:';
 --
 type assignment_action is record
 (
   nt_id      varchar(50)
  ,role_desc  varchar(150)
  ,role_name  varchar(150)
  ,type       varchar(50)
  ,action     varchar(50)  
  ,status     varchar(50) 
 );
 --
 n_seq NUMBER :=0;
 --
 cursor get_users is
    select regexp_replace(trim(nt_id), '([^[:print:]])','') nt_id
    from   xxcus.xxcus_gsc_assign_roles_all 
    where  1 =1  
      --and  regexp_replace(trim(nt_id), '([^[:print:]])','') ='JA043983'
    group by regexp_replace(trim(nt_id), '([^[:print:]])','')
    order by 1 asc;
 --
 cursor get_user_roles (p_nt_id in varchar2) is
    select p_nt_id                                                      nt_id
          ,regexp_replace(trim(role_description), '([^[:print:]])','')  role_desc
    from   xxcus.xxcus_gsc_assign_roles_all
    where  1 =1
      and  nt_id =p_nt_id
      and  not exists
       (
        select '1'
        from   fnd_user                  u
              ,wf_user_role_assignments  wura
              ,wf_all_user_roles         wur
              ,wf_local_roles            wr
        where 1 =1
          and wura.user_name         =p_nt_id
          and wr.name                =regexp_replace(trim(role_description), '([^[:print:]])','')
          and u.user_name            =wura.user_name
          and wura.relationship_id   <> -1
          and wur.role_orig_system   ='FND_RESP'
          and not wura.role_name like 'FND_RESP|%|ANY'
          and wura.role_name         =wur.role_name
          and wura.user_name         =wur.user_name
          and wr.name                =wura.assigning_role       
       )      
    order by role_description; 
 --
 type action_tbl is table of assignment_action index by binary_integer;
 action_rec action_tbl;
 --
 cursor disable_direct_resp (p_user in varchar2) is
    select c.user_name user_name
          ,c.user_id user_id
          ,b.responsibility_key resp_key
          ,b.responsibility_name resp_name
          ,d.application_id resp_appl_id
          ,d.application_short_name resp_appl_short_name
          ,case when a.security_group_id =0 then 'STANDARD' else 'NA' end sec_grp_code
          ,a.start_date start_date
          ,trunc(sysdate) end_date
    from   fnd_user_resp_groups_direct a
          ,fnd_responsibility_vl       b
          ,fnd_user                    c
          ,fnd_application_vl          d
    where  1 =1
      and  d.application_id    =a.responsibility_application_id
      and  c.user_name         =p_user
      and  a.user_id           =c.user_id 
      and  b.responsibility_id =a.responsibility_id
      and  a.end_date is null --only direct responsiblities that are not end dated for a user are pulled and deactivated
      and  not exists --Exclude these direct responsibilities
           (
            select lookup_code resp_key  --HDS Internet Expenses User
            from   fnd_lookup_values_vl
            where  1 =1
              and  lookup_type ='XXCUS_GRC_ROLE_RESP_EXCLUSION'
              and  lookup_code =b.responsibility_key
           );
 --
 procedure print_log(p_message in varchar2) is
 begin
     if  fnd_global.conc_request_id >0 then
      fnd_file.put_line(fnd_file.log, p_message);
     else
      dbms_output.put_line(p_message);
     end if;
 end print_log;
 --
 --
 procedure print_output(p_message in varchar2) is
 begin
     if  fnd_global.conc_request_id >0 then
      fnd_file.put_line(fnd_file.output, p_message);
     else
      dbms_output.put_line(p_message);
     end if;
 end print_output;
 --
 function get_role_internal_name(p_role_display_name in varchar2) return varchar2 is
  --
  l_role_name wf_local_roles.name%type :=Null;
  --
 begin
  --
  select name  --role internal name
  into   l_role_name
  from   WF_LOCAL_ROLES
  where  1 =1
    and  display_name =p_role_display_name;
  --
  return l_role_name;
  --
 exception
  when no_data_found then
   return 'NONE';
  when too_many_rows then
   print_log('Too many rows found for role description '||p_role_display_name);  
   return 'MANY_ROWS';   
  when others then
   print_log('Other error in fetch of role name for description '||p_role_display_name);
   return 'ERR';
 end get_role_internal_name;
 --         
begin
 --
 print_log('Start: Main');
 --
 begin
  -- begin cursor get_users
  for user_rec in get_users loop
   --
   -- disable all active direct responsibilities
   --
   print_log('Current User: '||user_rec.nt_id);
   --
      begin 
       --
          for rec in disable_direct_resp (p_user =>user_rec.nt_id) loop 
            --
            -- even though we are calling addresp routine, it checks for active assignments
            -- and inactivates them when an end date is passed
            --
            --
            n_seq :=n_seq +1;
            --
            fnd_user_pkg.addresp
                   (
                     username        =>rec.user_name, --varchar2
                     resp_app        =>rec.resp_appl_short_name, --varchar2
                     resp_key        =>rec.resp_key, --varchar2,
                     security_group  =>rec.sec_grp_code, --varchar2,
                     description     =>v_project_desc, --varchar2,
                     start_date      =>rec.start_date, --date,
                     end_date        =>rec.end_date --date --when passed and if an assignment exists for a user will be end dated
                   );  
            -- 
               action_rec(n_seq).nt_id      :=rec.user_name;
               action_rec(n_seq).role_desc  :=rec.resp_name;
               action_rec(n_seq).role_name  :=rec.resp_key;
               action_rec(n_seq).type       :='Responsibility';
               action_rec(n_seq).action     :='Inactivate.';  
               action_rec(n_seq).status     :='Success';
            --   
          end loop;       
       --
      exception
       when others then
        print_log('@cursor disable_direct_resp for user '||user_rec.nt_id||', msg ='||sqlerrm);
      end;   
   --
   --
   -- assign all roles mentioned in the file  xxcus_gsc_assign_roles.txt 
   -- 
      begin 
       --
          for user_role_rec in get_user_roles (p_nt_id =>user_rec.nt_id) loop 
            --
            -- even though we are calling addresp routine, its check for active assignments
            -- and inactivates them when an end date is passed
            --
            print_log('Role =>'||user_role_rec.role_desc);
            --
            begin 
             --
             savepoint init_here;
             --             
             v_role_name :=get_role_internal_name(p_role_display_name =>user_role_rec.role_desc);
             --
             if v_role_name NOT IN ('NONE', 'MANY_ROWS', 'ERR') then
              --
              wf_local_synch.propagateuserrole
               (
                    p_user_name        =>user_role_rec.nt_id
                   ,p_role_name        =>v_role_name
                   ,p_assignmentreason =>v_project_desc
                   ,p_start_date       =>trunc(sysdate)
               );              
              -- 
              n_seq :=n_seq +1;
              --
               action_rec(n_seq).nt_id      :=user_role_rec.nt_id;
               action_rec(n_seq).role_desc  :=user_role_rec.role_desc;
               action_rec(n_seq).role_name  :=v_role_name;
               action_rec(n_seq).type       :='Role';
               action_rec(n_seq).action     :='Activate';  
               action_rec(n_seq).status     :='Success';
              --
              print_log('Assigned role '||user_role_rec.role_desc||' to user: '||user_rec.nt_id);
              --              
             else
              -- 
              n_seq :=n_seq +1;
              --
               action_rec(n_seq).nt_id      :=user_role_rec.nt_id;
               action_rec(n_seq).role_desc  :=user_role_rec.role_desc;
               action_rec(n_seq).role_name  :='Invalid Role Description.';
               action_rec(n_seq).type       :='Role';
               action_rec(n_seq).action     :='Activate';  
               action_rec(n_seq).status     :='Fail';
              --             
              --
              --print_log('Failed to get role name for description '||user_role_rec.role_desc||', v_role_name ='||v_role_name);
              --
             end if;
              --             
            exception
             when others then
              print_log('@caller wf_local_synch.propagateuserrole, msg ='||sqlerrm);
              rollback to init_here;
            end;  
            --    
          end loop;       
       --
      exception
       when others then
        print_log('@cursor get_user_roles for user '||user_rec.nt_id||', msg ='||sqlerrm);
      end;     
   --
  end loop;
  --
 exception
  when others then
   print_log('@cursor get_users, message ='||sqlerrm);
 end;
 -- end of cursor get_users
 --
 --fnd_global.apps_initialize(user_id =>0, resp_id =>20420, resp_appl_id =>1);     
 --
 --
 print_output('NT_ID,DESCRIPTION,NAME,TYPE,ACTION,STATUS');
 for idx in action_rec.first .. action_rec.last loop 
  --
  print_output
   (
       action_rec(idx).nt_id
    ||','||
       action_rec(idx).role_desc    
    ||','||
       action_rec(idx).role_name
    ||','||
       action_rec(idx).type
    ||','||
       action_rec(idx).action
    ||','||
       action_rec(idx).status                                   
   );
  --
 end loop;
 --
 commit;
 -- 
exception
 when others then
  print_log('Outer block of SQL script, message ='||sqlerrm);
end;
/