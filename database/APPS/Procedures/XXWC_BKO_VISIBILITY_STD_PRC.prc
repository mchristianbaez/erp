CREATE OR REPLACE PROCEDURE APPS.xxwc_bko_visibility_std_prc ( RETCODE     OUT VARCHAR2
                                   , ERRMSG     OUT VARCHAR2)
AS
    -- 11/11/2013 CG: TMS 20131111-00179: New temporary stand alone program for BKO Visibility review
    -- 17/10/2014 Veera : TMS#20141001-00168  Mulit Org Code Changes
	l_error_message2 clob;
    l_conc_req_id   number := fnd_global.conc_request_id;
    l_start_time    number;  
    l_end_time      number;
    l_login_id      number := fnd_global.login_id;
    
    cursor cur_order_lines is
        -- 11/11/2013 CG: TMS 20131111-00179: Update to query and temporary date range to try and complete program until index is deployed
        SELECT  oh.order_number
               , (ol.line_number || '.' || ol.shipment_number) line_number
               , ol.ordered_item
               , ol.ordered_quantity
               , ol.attribute11 force_ship_qty
               , ol.user_item_description
               , oh.header_id
               , ol.line_id
               , ol.ship_from_org_id
               , ol.inventory_item_id
               , ol.cancelled_flag
               , ol.attribute17 bko_dff
               , ol.ROWID ol_rid
          FROM     apps.oe_order_headers oh, apps.oe_order_lines ol
         WHERE         oh.order_type_id IN (1001, 1011)
                 AND oh.header_id = ol.header_id  
                 AND ol.line_type_id in (1002, 1012)
                 -- 11/12/2013 CG: TMS 20131111-00179: modified for tunning
                 AND ol.cancelled_flag = 'N'
                 AND ol.open_flag = 'Y'
                 AND ol.booked_flag = 'Y'
                 --AND trunc(ol.creation_date) >= trunc(SYSDATE - 10)
                 AND ol.creation_date >= trunc(SYSDATE - 10)
                 AND ol.source_type_code != 'EXTERNAL'
                 AND ol.attribute17 IS NULL
                 --AND ol.flow_status_code != 'CLOSED'
                 --AND NVL (ol.booked_flag, 'N') = 'Y'
                 -- 11/12/2013 CG: TMS 20131111-00179:
                 AND xxwc_om_has_cust_del_docs_fn (ol.header_id, ol.line_id, ol.ship_from_org_id) = 'N'
        UNION ALL
        SELECT  oh.order_number
               , (ol.line_number || '.' || ol.shipment_number) line_number
               , ol.ordered_item
               , ol.ordered_quantity
               , ol.attribute11 force_ship_qty
               , ol.user_item_description
               , oh.header_id
               , ol.line_id
               , ol.ship_from_org_id
               , ol.inventory_item_id
               , ol.cancelled_flag
               , ol.attribute17 bko_dff
               , ol.ROWID ol_rid
          FROM     apps.oe_order_headers oh, apps.oe_order_lines ol
         WHERE         oh.order_type_id IN (1001, 1011)
                 AND oh.header_id = ol.header_id  
                 AND ol.line_type_id in (1002, 1012)
                 -- 11/12/2013 CG: TMS 20131111-00179: modified for tunning
                 AND ol.cancelled_flag = 'N'
                 AND ol.open_flag = 'Y'
                 AND ol.booked_flag = 'Y'
                 -- AND trunc(ol.creation_date) >= trunc(SYSDATE - 10)
                 AND ol.creation_date >= trunc(SYSDATE - 10)
                 AND ol.last_update_date >= (SYSDATE - INTERVAL '10' MINUTE)
                 -- AND trunc(ol.last_update_date) >= trunc(sysdate - 25) --(SYSDATE - INTERVAL '10' MINUTE)
                 AND ol.source_type_code != 'EXTERNAL'
                 --AND ol.flow_status_code != 'CLOSED'
                 --AND NVL (ol.booked_flag, 'N') = 'Y'
                 -- 11/12/2013 CG: TMS 20131111-00179
                 AND xxwc_om_has_cust_del_docs_fn (ol.header_id, ol.line_id, ol.ship_from_org_id) = 'N';
                          
        /*select  oh.order_number
                , (ol.line_number||'.'||ol.shipment_number) line_number
                , ol.ordered_item
                , ol.ordered_quantity
                , ol.attribute11 force_ship_qty
                , ol.user_item_description
                , oh.header_id
                , ol.line_id
                , ol.ship_from_org_id
                , ol.inventory_item_id
                , ol.cancelled_flag
                , ol.attribute17 bko_dff
                , ol.rowid ol_rid
        from    oe_order_headers_all oh
                , oe_order_lines_all ol
        where   oh.order_type_id in (1001,1011)
        and     oh.header_id  = ol.header_id
        and     ol.creation_date >= (sysdate - 10)
        and     ol.flow_status_code != 'CLOSED'
        -- 08/25/2013 CG: TMS 20130826-00696: Update to exclude Drop Ships
        and     ol.source_type_code != 'EXTERNAL'
        and     ( 
                    (nvl(ol.booked_flag, 'N') = 'Y'
                     and ol.attribute17 is null
                     and not exists (select  'Delivery Docs Gen'
                                    from    apps.xxwc_wsh_shipping_stg x1
                                    where   x1.header_id = ol.header_id
                                    and     x1.line_id = ol.line_id
                                    and     x1.ship_from_org_id = ol.ship_from_org_id)
                    )
                    OR
                    ( nvl(ol.booked_flag, 'N') = 'Y'
                      and ol.last_update_date >= (sysdate - interval '15' minute)
                      and not exists (select  'Delivery Docs Gen'
                                        from    apps.xxwc_wsh_shipping_stg x1
                                        where   x1.header_id = ol.header_id
                                        and     x1.line_id = ol.line_id
                                        and     x1.ship_from_org_id = ol.ship_from_org_id)
                    )
                 );*/
    
    v_delivery_status     VARCHAR2 (124);
    l_shipped_qty         NUMBER;
    l_count               NUMBER;
    l_commit_limit        NUMBER;
    l_item_type           VARCHAR2(240);
    l_co_qty              NUMBER;
    
    -- 11/11/2013 CG: TMS 20131111-00179: Added for lock control
    v_locked_line              VARCHAR2 (1) := 'N';
    
BEGIN
    fnd_file.put_line(fnd_file.log, 'Starting BKO Status Updates...');
    fnd_file.put_line(fnd_file.log, '==============================');
    fnd_file.put_line(fnd_file.log, ' ');
    
    l_count := 0;
    l_commit_limit := 0;
    for c1 in cur_order_lines
    loop
        exit when cur_order_lines%notfound;
        
        -- 11/11/2013 CG: TMS 20131111-00179: Added for lock control
        BEGIN
            v_locked_line := xxwc_om_force_ship_pkg.is_row_locked (c1.ol_rid, 'OE_ORDER_LINES_ALL');
        EXCEPTION
            WHEN OTHERS
            THEN
                v_locked_line := 'Y';
        END;
        
        -- 11/11/2013 CG: TMS 20131111-00179: Added for lock control
        IF NVL(v_locked_line, 'N') = 'N' THEN
         
            fnd_file.put_line(fnd_file.log, 'Order '||c1.order_number||' - Line Number '||c1.line_number||' - BKO DFF: '||c1.bko_dff);
            fnd_file.put_line(fnd_file.log, 'Ordered Item '||c1.ordered_item||' - Ordered Qty: '||c1.ordered_quantity||' - Force Ship Qty: '||c1.force_ship_qty);
            fnd_file.put_line(fnd_file.log, 'Current Status '||c1.user_item_description);
            
            v_delivery_status := NULL;
            l_item_type       := NULL;
            l_co_qty          := NULL;
            
            BEGIN
                -- 11/12/2013 CG: TMS 20131111-00179: modified for tunning
                SELECT msib.item_type                  
                      , (SELECT /*+ INDEX (l XXWC_OE_ORDER_LN_AL_N9) */
                                NVL (
                                    SUM (
                                          l.ordered_quantity
                                        * apps.po_uom_s.po_uom_convert (um.unit_of_measure
                                                                  ,msib.primary_unit_of_measure
                                                                  ,l.inventory_item_id))
                                   ,0)
                           FROM apps.oe_order_lines l, 
                                --oe_order_headers_all h, 
                                mtl_units_of_measure_vl um
                          WHERE     /*l.header_id = h.header_id
                                AND h.flow_status_code NOT IN ('ENTERED', 'CLOSED', 'CANCELLED')
                                AND*/ l.line_type_id = 1005
                                AND l.cancelled_flag = 'N'
                                AND l.open_flag = 'Y'
                                AND l.booked_flag = 'Y'
                                AND l.ship_from_org_id = msib.organization_id
                                AND l.inventory_item_id = msib.inventory_item_id
                                AND l.order_quantity_uom = um.uom_code)
                  INTO l_item_type
                       , l_co_qty
                  FROM --oe_order_lines_all ol, 
                        mtl_system_items_b msib
                 WHERE     /*ol.line_id = c1.line_id
                       AND ol.inventory_item_id = msib.inventory_item_id
                       AND ol.ship_from_org_id = msib.organization_id*/
                       msib.inventory_item_id = c1.inventory_item_id
                       and  msib.organization_id = c1.ship_from_org_id;
                 -- 11/12/2013 CG: TMS 20131111-00179: modified for tunning
            EXCEPTION
                WHEN OTHERS
                THEN
                    l_item_type       := NULL;
                    l_co_qty          := NULL;
            END;
            

            IF NVL (l_item_type, 'UNKNOWN') IN ('INTANGIBLE', 'FRT', 'PTO')
            THEN
                v_delivery_status := 'Awaiting Shipping';
            ELSE
                IF c1.force_ship_qty IS NULL
                THEN
                    l_shipped_qty :=
                        xxwc_ont_routines_pkg.get_onhand (p_inventory_item_id   => c1.inventory_item_id
                                                         ,p_organization_id     => c1.ship_from_org_id
                                                         ,p_subinventory        => 'General'
                                                         ,p_return_type         => 'H');

                    IF l_shipped_qty < 0
                    THEN
                        l_shipped_qty := 0;
                    END IF;

                    l_shipped_qty := l_shipped_qty - l_co_qty;

                    fnd_file.put_line(fnd_file.log, 'l_shipped_qty: '||l_shipped_qty);

                    IF l_shipped_qty <= 0
                    THEN
                        l_shipped_qty := 0;
                        v_delivery_status := 'BACKORDERED';
                    ELSIF l_shipped_qty >= c1.ordered_quantity
                    THEN
                        v_delivery_status := 'Awaiting Shipping';
                    ELSIF l_shipped_qty < c1.ordered_quantity AND l_shipped_qty > 0
                    THEN
                        v_delivery_status := 'PARTIAL BACKORDERED';
                    ELSE
                        v_delivery_status := 'Unknown Stat';
                    END IF;
                ELSE
                    if c1.force_ship_qty = 0 then
                        v_delivery_status := 'BACKORDERED';
                    elsif c1.force_ship_qty = c1.ordered_quantity then
                        v_delivery_status := 'Awaiting Shipping';
                    elsif c1.force_ship_qty > 1 and c1.force_ship_qty < c1.ordered_quantity then
                        v_delivery_status := 'PARTIAL BACKORDERED';
                    else
                        v_delivery_status := 'Awaiting Shipping';
                    end if;
                END IF;
            END IF;

            -- if c1.user_item_description is not null and c1.user_item_description != v_delivery_status then
            --     v_delivery_status := c1.user_item_description; 
            -- end if;

            IF nvl(c1.cancelled_flag, 'N') = 'Y' THEN
                v_delivery_status := 'CANCELLED';
            END IF;
            
            fnd_file.put_line(fnd_file.log, 'New Status '||v_delivery_status);
            
            if c1.bko_dff is null or c1.user_item_description != v_delivery_status then
            
                begin
                    UPDATE oe_order_lines_all
                           SET user_item_description = v_delivery_status
                               ,attribute16 = DECODE (v_delivery_status
                                                      ,'OUT_FOR_DELIVERY', 'Y'
                                                      ,'OUT_FOR_DELIVERY/PARTIAL_BACKORDER', 'Y'
                                                      ,'DELIVERED', 'D'
                                                      ,NULL)
                               ,attribute17 = 'Y'
                         WHERE ROWID = c1.ol_rid;
                    
                    l_count := l_count + 1;
                exception
                when others then
                    fnd_file.put_line(fnd_file.log, 'Could not update line. Error: '||SQLERRM);
                end;
            end if;
        
            l_commit_limit := l_commit_limit + 1;
            
            if l_commit_limit >= 100 then
                commit;
            end if;        
            
            fnd_file.put_line(fnd_file.log, ' ');
            
        END IF;
        
    end loop;
    
    fnd_file.put_line(fnd_file.log, 'Updated '||l_count||'  order lines');
    
    commit;
EXCEPTION
WHEN OTHERS THEN
    l_error_message2 := 'xxwc_bko_visibility_std_prc '
                || 'Error_Stack...'
                || DBMS_UTILITY.format_error_stack ()
                || ' Error_Backtrace...'
                || DBMS_UTILITY.format_error_backtrace ();

    xxcus_error_pkg.xxcus_error_main_api (
        p_called_from         => 'xxwc_bko_visibility_std_prc'
       ,p_calling             => 'xxwc_bko_visibility_std_prc'
       ,p_request_id          => l_conc_req_id
       ,p_ora_error_msg       => l_error_message2
       ,p_error_desc          => 'Error running XXWC OM Backorder Visibility Processing - Stand Alone'
       ,p_distribution_list   => 'cgonzalez@luciditycg.com'
       ,p_module              => 'ONT');

    fnd_file.put_line(fnd_file.log, 'Error in main xxwc_bko_visibility_std_prc. Error: '||l_error_message2);
    
    errmsg := l_error_message2;
    retcode := '2';
END xxwc_bko_visibility_std_prc;
/


