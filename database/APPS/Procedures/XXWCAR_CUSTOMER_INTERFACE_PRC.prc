CREATE OR REPLACE PROCEDURE APPS.xxwcar_customer_interface_prc(p_mode IN VARCHAR2
                                                         ,p_user IN VARCHAR2
                                                         ,p_resp IN VARCHAR2) IS

  /*******************************************************************************
  * Procedure:   xxcusar_customer_interface_prc
  * Description: This procedure is used by an APEX App (XXWC Prism-EBS Customer
                 Interface Processing) to execute the interface program to commit changes.
                 Apex App cannot directly execute the interface program due to Oracle
                 limitation on executing RPC over dblink.
  HISTORY
  ===============================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     11-May-2012        Luong Vu        Initial creation of the package
  ********************************************************************************/

  --Intialize Variables
  l_err_msg      VARCHAR2(2000);
  l_err_code     NUMBER;
  l_sec          VARCHAR2(150);
  l_req_id       NUMBER;
  l_err_callfrom VARCHAR2(75) := 'XXWCAR_CUSTOMER_INTERFACE_PRC';
  l_distro_list  VARCHAR2(75) DEFAULT 'hdsoracledevelopers@hdsupply.com';


  --Start Main Program
BEGIN
  l_sec := 'Start Main Program; ';

  -- Call the procedure
  apps.xxwc_ar_prism_cust_iface_pkg.submit_job(errbuf => l_err_msg,
                                               retcode => l_err_code,
                                               p_process_type => p_mode,
                                               p_user_name => p_user,
                                               p_responsibility_name => p_resp);

EXCEPTION
  WHEN program_error THEN
    ROLLBACK;
    l_err_code := 2;
    l_err_msg  := substr((l_err_msg || ' ERROR ' ||
                         substr(SQLERRM, 1, 2000)), 1, 3000);
  
    xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_err_callfrom,
                                         p_calling => l_sec,
                                         p_request_id => l_req_id,
                                         p_ora_error_msg => substr(SQLERRM, 1,
                                                                    2000),
                                         p_error_desc => substr(l_err_msg, 1,
                                                                 2000),
                                         p_distribution_list => l_distro_list,
                                         p_module => 'AR');
  
  WHEN OTHERS THEN
    l_err_code := 2;
    l_err_msg  := substr((l_err_msg || ' ERROR ' ||
                         substr(SQLERRM, 1, 2000)), 1, 3000);
    dbms_output.put_line(l_err_msg);
  
    xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_err_callfrom,
                                         p_calling => l_sec,
                                         p_request_id => l_req_id,
                                         p_ora_error_msg => substr(SQLERRM, 1,
                                                                    2000),
                                         p_error_desc => substr(l_err_msg, 1,
                                                                 2000),
                                         p_distribution_list => l_distro_list,
                                         p_module => 'AR');
END xxwcar_customer_interface_prc;
/
