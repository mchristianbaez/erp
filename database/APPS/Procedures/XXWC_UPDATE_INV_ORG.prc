CREATE OR REPLACE PROCEDURE APPS.XXWC_UPDATE_INV_ORG (p_errbuf                 OUT  VARCHAR2,
                                                 p_retcode                OUT  NUMBER,
                                                 p_date_to                 IN  DATE)
AS
BEGIN
    UPDATE HR.HR_ALL_ORGANIZATION_UNITS
       SET date_to = p_date_to
     WHERE organization_id IN (SELECT mp.organization_id
         FROM xxwc.xxwc_enable_inv_org_list stg
            , INV.mtl_parameters mp
        WHERE stg.enable_flag = 'Y'
          AND stg.organization_code = mp.organization_code);
  COMMIT;
  p_retcode := 0;
EXCEPTION
WHEN OTHERS THEN
  p_retcode := 2;
  p_errbuf  := SQLERRM;
END XXWC_UPDATE_INV_ORG;
/
