CREATE OR REPLACE PROCEDURE APPS.xxwc_update_customer_profile
       IS

     p_customer_profile_rec_type     HZ_CUSTOMER_PROFILE_V2PUB.CUSTOMER_PROFILE_REC_TYPE;
     p_cust_account_profile_id       NUMBER;
     p_object_version_number         NUMBER;
     x_return_status                 VARCHAR2(2000);
     x_msg_count                     NUMBER;
     x_msg_data                      VARCHAR2(2000);

     CURSOR cur_cust_prof IS
     SELECT stg.cust_account_profile_id
          , stg.collector_id
          , stg.credit_analyst_id
          , hcp.object_version_number
       FROM xxwc.xxwc_update_cust_profile_tbl stg
          , hz_customer_profiles hcp
      WHERE 1 = 1
        AND NVL(stg.status,'N') IN ('N', 'E')
 --       AND stg.cust_account_profile_id IN (118920, 396641)
        AND stg.cust_account_profile_id = hcp.cust_account_profile_id;

BEGIN
   mo_global.init ('AR');
   fnd_global.apps_initialize (user_id           => 1296
                             , resp_id           => 20678
                             , resp_appl_id      => 222);

   mo_global.set_policy_context ('S', 162);
   fnd_global.set_nls_context ('AMERICAN');

   FOR rec_cust_prof IN cur_cust_prof LOOP
     p_customer_profile_rec_type.cust_account_profile_id := rec_cust_prof.cust_account_profile_id; -- Documentation on using TCA APIs - V2 Page 73
     p_customer_profile_rec_type.collector_id            := rec_cust_prof.collector_id;
     p_customer_profile_rec_type.credit_analyst_id       := rec_cust_prof.credit_analyst_id;
     p_object_version_number                             := rec_cust_prof.object_version_number;
--     p_object_version_number                             := 9; --rec_cust_prof.object_version_number;

     -- API Call
     hz_customer_profile_v2pub.update_customer_profile(p_init_msg_list              => fnd_api.g_true
                                     ,                 p_customer_profile_rec       => p_customer_profile_rec_type
                                     ,                 p_object_version_number      => p_object_version_number
                                     ,                 x_return_status              => x_return_status
                                     ,                 x_msg_count                  => x_msg_count
                                     ,                 x_msg_data                   => x_msg_data                );

     IF x_msg_count >1 THEN
        FOR I IN 1..x_msg_count LOOP
          dbms_output.put_line(SUBSTR(FND_MSG_PUB.Get(p_encoded=>
          FND_API.G_FALSE ), 1, 255));
        END LOOP;

        UPDATE xxwc.xxwc_update_cust_profile_tbl
           SET status        = 'E'
             , error_message = SUBSTR(FND_MSG_PUB.Get(p_encoded=>FND_API.G_FALSE ), 1, 255)
         WHERE cust_account_profile_id = rec_cust_prof.cust_account_profile_id;

     ELSE
        UPDATE xxwc.xxwc_update_cust_profile_tbl
           SET status = SUBSTR(x_return_status,1,1)
         WHERE cust_account_profile_id = rec_cust_prof.cust_account_profile_id;
     END IF;
   END LOOP; -- rec_cust_prof

COMMIT;

EXCEPTION
WHEN OTHERS THEN
  dbms_output.put_line('Error Message = '|| SQLERRM);
END XXWC_UPDATE_CUSTOMER_PROFILE;
/
