CREATE OR REPLACE PROCEDURE APPS.xxwc_dms_sales_receipt_prc(p_order_header_id IN NUMBER 
                                                          , p_num_copies      IN NUMBER
                                                          , p_group_id        IN NUMBER
                                                          , p_batch_id        IN NUMBER
                                                          , p_user_id         IN NUMBER
                                                          , p_printer_name    IN VARCHAR2
                                                          , p_right_fax       IN VARCHAR2
                                                          , p_fax             IN VARCHAR2
                                                          , p_fax_comment     IN VARCHAR2
                                                          ) IS
  /*******************************************************************************
  * Procedure:   XXWC_DMS_SALES_RECEIPT_PRC
  * Description: TMS# 20140606-00082 
                 This procedure is used by an APEX App (WC Signature Capture)
                 to submit CFD Reports.
                 Apex App cannot directly submit the reports due to Oracle
                 limitation on executing Concurrent Program over dblink.
  HISTORY
  ===============================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     11-May-2014        Gopi Damuluri   TMS# 20140606-00082 
                                             Initial creation of the procedure
  1.1     21-Nov-2014        Gopi Damuluri   TMS# 20141112-00097
                                             CFDs should not be printed when user hits 
                                             Cancel button on Signature Capture Page.
  1.2     12-May-2015        Gopi Damuluri   TMS# 20150311-00128
                                             CFDs should be printed when user hits Done button
                                             on Signature Capture Page.
  ********************************************************************************/
        ----------------------------------------------------------------------------------
        -- Variable definitions
        ----------------------------------------------------------------------------------
        g_dflt_email  VARCHAR2(75) DEFAULT 'HDSORACLEDEVELOPERS@hdsupply.com';

        l_err_msg               VARCHAR2(3000);
        l_err_code              NUMBER;
        l_responsibility_id     NUMBER;
        l_resp_application_id   NUMBER;
        l_batch_exists          NUMBER := 0;
        l_order_type_id         NUMBER;
        l_sec                   VARCHAR2(150);
        l_prog                  VARCHAR2(50);
        l_prog_desc             VARCHAR2(90);
        l_template              VARCHAR2(50);
        l_no_request_exp        EXCEPTION;
        l_responsibility_name   VARCHAR2(150) := 'HDS Order Mgmt Super User - WC';
        l_return NUMBER;
        
        l_ship_from_org_id      NUMBER;       -- Version# 1.2
        l_signature_name        VARCHAR2(60); -- Version# 1.2

        ----------------------------------------------------------------------------------
        -- Cursor to derive Print Request details
        ----------------------------------------------------------------------------------
        CURSOR cur_print_tbl IS
             SELECT batch_id
                  , group_id
                  , application
                  , program
                  , printer
               FROM xxwc.xxwc_print_requests_temp
              WHERE group_id = p_group_id
                AND process_flag = '1'; -- Version# 1.2

         PRAGMA AUTONOMOUS_TRANSACTION;

   BEGIN

/*
      l_err_msg  := 'p_batch_id - '||p_batch_id;

             UPDATE XXWC.XXWC_PRINT_REQUESTS_TEMP
                SET PRINTER            = l_err_msg,
                    COPIES             = p_num_copies,
                    LAST_UPDATE_DATE   = SYSDATE,
                    DMS_SIGN_FLAG      = '2'
                  , PROCESS_FLAG       = '2'
              WHERE GROUP_ID           = p_group_id;
              COMMIT;

      RAISE program_error;
*/

      l_sec := 'Derive ResponsibilityId and ApplicationId';
      ----------------------------------------------------------------------------------
      -- Derive ResponsibilityId and ApplicationId
      ----------------------------------------------------------------------------------
      BEGIN
        SELECT responsibility_id
             , application_id
          INTO l_responsibility_id
             , l_resp_application_id
          FROM fnd_responsibility_vl
         WHERE responsibility_name = l_responsibility_name
           AND SYSDATE BETWEEN start_date AND NVL(end_date, TRUNC(SYSDATE) + 1);
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
         l_err_msg := 'Responsibility '||l_responsibility_name||' not defined in Oracle';
         RAISE program_error;
      WHEN OTHERS THEN
         l_err_msg := 'Error deriving Responsibility_id for '||l_responsibility_name;
         RAISE program_error;
      END;

      ----------------------------------------------------------------------------------
      -- Check if Print Group exists
      ----------------------------------------------------------------------------------
       BEGIN
          SELECT COUNT(1)
            INTO l_batch_exists
            FROM XXWC.XXWC_PRINT_REQUESTS_TEMP
           WHERE GROUP_ID           = p_group_id
             AND process_flag       = '1' -- Version# 1.2
             ;
        EXCEPTION
        WHEN OTHERS THEN
           l_err_msg := 'While l_batch_exists '||SQLERRM;
           RAISE program_error;
        END;

--/* -- TMS# 20141112-00097 > Start

        ----------------------------------------------------------------------------------
        -- Derive Order Type Id
        ----------------------------------------------------------------------------------
        BEGIN
           SELECT order_type_id
                , ship_from_org_id
             INTO l_order_type_id
                , l_ship_from_org_id
             FROM oe_order_headers_all ooh
            WHERE 1 = 1
              AND ooh.header_id = p_order_header_id;
         EXCEPTION
         WHEN OTHERS THEN
           l_err_msg := 'While Order Type Id - '||SQLERRM;
           RAISE program_error;
         END;
--*/ -- TMS# 20141112-00097 < End

        ----------------------------------------------------------------------------------
        -- For COUNTER ORDER, REPAIR ORDER, RETURN ORDER types, do not perform any action
        ----------------------------------------------------------------------------------
        IF -- l_order_type_id IN (1004, 1009, 1006) AND  -- TMS# 20141112-00097
           p_batch_id = -9999 THEN  -- COUNTER ORDER, REPAIR ORDER, RETURN ORDER 

           BEGIN
             UPDATE XXWC.XXWC_PRINT_REQUESTS_TEMP
                SET PRINTER            = p_printer_name,
                    COPIES             = p_num_copies,
                    LAST_UPDATE_DATE   = SYSDATE,
                    DMS_SIGN_FLAG      = '2'
                  , PROCESS_FLAG       = '2' -- Version# 1.2
              WHERE GROUP_ID           = p_group_id
                ;

             UPDATE XXWC.XXWC_PRINT_REQUESTS_ARG_TEMP
                SET PROCESS_FLAG       = '2' -- Version# 1.2
              WHERE GROUP_ID           = p_group_id
                ;

           COMMIT;
           EXCEPTION
           WHEN OTHERS THEN
              l_err_msg := 'While updating Printer '||SQLERRM;
              RAISE program_error;
           END;
           
           RAISE l_no_request_exp;
        END IF;

        --------------------------------------------------------------------------------------------
        -- If Print batch exists in Print Requests table, update the Printer Name and # of Copies
        --------------------------------------------------------------------------------------------
        IF l_batch_exists > 0 THEN
           BEGIN
             UPDATE XXWC.XXWC_PRINT_REQUESTS_TEMP
                SET PRINTER            = p_printer_name,
                    COPIES             = p_num_copies,
                    LAST_UPDATE_DATE   = SYSDATE,
                    DMS_SIGN_FLAG      = '2'
              WHERE GROUP_ID           = p_group_id
                ;
           EXCEPTION
           WHEN OTHERS THEN
              l_err_msg := 'While updating Printer '||SQLERRM;
              RAISE program_error;
           END;

        --------------------------------------------------------------------------------------------
        -- Update Print Request Argument table with Right Fax Flag
        --------------------------------------------------------------------------------------------
           IF p_right_fax = 'Y' THEN
              BEGIN
                 UPDATE xxwc.xxwc_print_requests_arg_temp
                    SET value    = '1'
                  WHERE group_id = p_group_id
                    AND argument = '5';
              EXCEPTION
              WHEN OTHERS THEN
                l_err_msg := 'While arg 5 '||SQLERRM;
                RAISE program_error;
              END;
        --------------------------------------------------------------------------------------------
        -- Update Print Request Argument table with Right Fax Number
        --------------------------------------------------------------------------------------------
              BEGIN
                 UPDATE xxwc.xxwc_print_requests_arg_temp
                    SET value    = ''''||p_fax||''''
                  WHERE group_id = p_group_id
                    AND argument = '6';
              EXCEPTION
              WHEN OTHERS THEN
                l_err_msg := 'While arg 6 '||SQLERRM;
                RAISE program_error;
              END;
        --------------------------------------------------------------------------------------------
        -- Update Print Request Argument table with Right Fax Comments
        --------------------------------------------------------------------------------------------
              BEGIN
                 UPDATE xxwc.xxwc_print_requests_arg_temp
                    SET value    = ''''||trim(replace(p_fax_comment,chr(10),' '))||''''
                  WHERE group_id = p_group_id
                    AND argument = '7';
              EXCEPTION
              WHEN OTHERS THEN
                 l_err_msg := 'While arg 7'||SQLERRM;
                 RAISE program_error;
--                 NULL;
              END;
           END IF;

           COMMIT;              

        ELSE

-- NULL;
           IF l_order_type_id IN (1004, 1009, 1006) THEN  -- COUNTER ORDER, REPAIR ORDER, RETURN ORDER 

              -- Derive Program to be triggered based on PrintPrice Flag
              IF xxwc_ont_routines_pkg.check_print_price(p_order_header_id) = 1 THEN
                 l_prog      := 'XXWC_OM_SRECEIPT';
                 l_prog_desc := 'XXWC OM Sales Receipt';
                 l_template  := 'XXWC_OM_SRECEIPT';
              ELSE -- xxwc_ont_routines_pkg.check_print_price(p_order_header_id) = 2
                 l_prog      := 'XXWC_OM_SRECEIPT_CUSTOMER';
                 l_prog_desc := 'XXWC OM Customer Sales Receipt';
                 l_template  := 'XXWC_OM_SRECEIPT_CUSTOMER';
              END IF;

              BEGIN
                 INSERT INTO XXWC.XXWC_PRINT_REQUESTS_TEMP
                 (CREATED_BY
                 ,CREATION_DATE
                 ,LAST_UPDATED_BY
                 ,LAST_UPDATE_DATE
                 ,BATCH_ID
                 ,PROCESS_FLAG
                 ,GROUP_ID
                 ,APPLICATION
                 ,PROGRAM
                 ,DESCRIPTION
                 ,START_TIME
                 ,SUB_REQUEST
                 ,PRINTER
                 ,STYLE
                 ,COPIES
                 ,SAVE_OUTPUT
                 ,PRINT_TOGETHER
                 ,VALIDATE_PRINTER
                 ,TEMPLATE_APPL_NAME
                 ,TEMPLATE_CODE
                 ,TEMPLATE_LANGUAGE
                 ,TEMPLATE_TERRITORY
                 ,OUTPUT_FORMAT
                 ,DMS_SIGN_FLAG
                 ,NLS_LANGUAGE)
                 values
                 (p_user_id                            --CREATED_BY
                 ,SYSDATE                              -- CREATION_DATE
                 ,p_user_id                            --LAST_UDATED_BY
                 ,SYSDATE                              -- LAST_UPDATE_DATE
                 ,p_batch_id                           --BATCH_ID
                 ,'1'                                  --PROCESS_FLAG
                 ,p_group_id                           --GROUP_ID
                 ,'XXWC'                               --APPLICATION
                 , l_prog                              --PROGRAM
                 , l_prog_desc                         --DESCRIPTION
                 ,NULL                                 --START_TIME 
                 ,'FALSE'                              --SUB_REQUEST
                 , p_printer_name                      --PRINTER
                 ,NULL                                 --STYLE
                 , p_num_copies                        --COPIES
                 ,'TRUE'                               --SAVE_OUTPUT
                 ,'N'                                  --PRINT_TOGETHER
                 ,'RESOLVE'                            --VALIDATE_PRINTER
                 ,'XXWC'                               --TEMPLATE_APPL_NAME
                 ,l_template                           --TEMPLATE_CODE
                 ,'en'                                 --TEMPLATE_LANGUAGE
                 ,'US'                                 --TEMPLATE_TERRITORY
                 ,'PDF'                                --OUTPUT_FORMAT
                 ,'2'
                 ,'en'                                 --NLS_LANGUAGE
                 );
                 commit;

                 INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP values (p_batch_id,1,p_group_id,1,p_order_header_id);

                 INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP values (p_batch_id,1,p_group_id,2,NULL);

                 INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP values (p_batch_id,1,p_group_id,3,NULL);

                 INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP values (p_batch_id,1,p_group_id,4,NULL);

                 IF p_right_fax = 'Y' THEN
                    INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP values (p_batch_id,1,p_group_id,5,1);
                 ELSE
                    INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP values (p_batch_id,1,p_group_id,5,2);
                 END IF;

                 INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP values (p_batch_id,1,p_group_id,6,''''||p_fax||'''');

                 INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP values (p_batch_id,1,p_group_id,7,''''||trim(replace(p_fax_comment,chr(10),' '))||'''');

                 INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP values (p_batch_id,1,p_group_id,8,1);

                 COMMIT;
                 EXCEPTION
                 WHEN OTHERS THEN
                   NULL;
                 END;

           ELSIF l_order_type_id = 1001 THEN -- STANDARD ORDER
              
              -- Derive Program to be triggered based on PrintPrice Flag
-- xxwc_ont_routines_pkg.check_print_price(p_order_header_id)
                 l_prog      := 'XXWC_OM_PACK_SLIP';
                 l_prog_desc := 'XXWC OM Packing Slip';
                 l_template  := 'XXWC_OM_PACK_SLIP';

              BEGIN
                 INSERT INTO XXWC.XXWC_PRINT_REQUESTS_TEMP
                 (CREATED_BY
                 ,CREATION_DATE
                 ,LAST_UPDATED_BY
                 ,LAST_UPDATE_DATE
                 ,BATCH_ID
                 ,PROCESS_FLAG
                 ,GROUP_ID
                 ,APPLICATION
                 ,PROGRAM
                 ,DESCRIPTION
                 ,START_TIME
                 ,SUB_REQUEST
                 ,PRINTER
                 ,STYLE
                 ,COPIES
                 ,SAVE_OUTPUT
                 ,PRINT_TOGETHER
                 ,VALIDATE_PRINTER
                 ,TEMPLATE_APPL_NAME
                 ,TEMPLATE_CODE
                 ,TEMPLATE_LANGUAGE
                 ,TEMPLATE_TERRITORY
                 ,OUTPUT_FORMAT
                 ,DMS_SIGN_FLAG
                 ,NLS_LANGUAGE)
                 values
                 (p_user_id                            --CREATED_BY
                 ,SYSDATE                              -- CREATION_DATE
                 ,p_user_id                            --LAST_UDATED_BY
                 ,SYSDATE                              -- LAST_UPDATE_DATE
                 ,p_batch_id                           --BATCH_ID
                 ,'1'                                  --PROCESS_FLAG
                 ,p_group_id                           --GROUP_ID
                 ,'XXWC'                               --APPLICATION
                 , l_prog                              --PROGRAM
                 , l_prog_desc                         --DESCRIPTION
                 ,NULL                                 --START_TIME 
                 ,'FALSE'                              --SUB_REQUEST
                 , p_printer_name                      --PRINTER
                 ,NULL                                 --STYLE
                 , p_num_copies                        --COPIES
                 ,'TRUE'                               --SAVE_OUTPUT
                 ,'N'                                  --PRINT_TOGETHER
                 ,'RESOLVE'                            --VALIDATE_PRINTER
                 ,'XXWC'                               --TEMPLATE_APPL_NAME
                 ,l_template                           --TEMPLATE_CODE
                 ,'en'                                 --TEMPLATE_LANGUAGE
                 ,'US'                                 --TEMPLATE_TERRITORY
                 ,'PDF'                                --OUTPUT_FORMAT
                 ,'2'
                 ,'en'                                 --NLS_LANGUAGE
                 );
                 commit;

                 INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP values (p_batch_id,1,p_group_id,1,l_ship_from_org_id);

                 INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP values (p_batch_id,1,p_group_id,2,p_order_header_id);

                 INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP values (p_batch_id,1,p_group_id,3,xxwc_ont_routines_pkg.check_print_price(p_order_header_id));

                 INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP values (p_batch_id,1,p_group_id,4,NULL);

                 IF p_right_fax = 'Y' THEN
                    INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP values (p_batch_id,1,p_group_id,5,1);
                 ELSE
                    INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP values (p_batch_id,1,p_group_id,5,2);
                 END IF;

                 INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP values (p_batch_id,1,p_group_id,6,''''||p_fax||'''');

                 INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP values (p_batch_id,1,p_group_id,7,''''||trim(replace(p_fax_comment,chr(10),' '))||'''');

                 INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP values (p_batch_id,1,p_group_id,8,NULL);
                 
                 INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP values (p_batch_id,1,p_group_id,9,1);
                 
                 INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP values (p_batch_id,1,p_group_id,10,NULL);
                 
                 INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP values (p_batch_id,1,p_group_id,11,2);

                 COMMIT;
                 EXCEPTION
                 WHEN OTHERS THEN
                   NULL;
                 END;

           END IF; -- IF l_order_type_id IN () THEN
--           */
        END IF; -- IF l_batch_exists > 0 THEN

        IF -- l_order_type_id IN (1004, 1009, 1006, 1001, ) AND 
        NVL(p_batch_id,-1) != -9999 THEN  -- COUNTER ORDER, REPAIR ORDER, RETURN ORDER

          l_sec := 'Apps Initialize';
          ----------------------------------------------------------------------------------
          -- Apps Initialize
          ----------------------------------------------------------------------------------
          FND_GLOBAL.APPS_INITIALIZE (p_user_id, l_responsibility_id, l_resp_application_id);

          FOR rec_print_tbl IN cur_print_tbl LOOP
             l_return := XXWC_ONT_ROUTINES_PKG.SUBMIT_PRINT_BATCH(rec_print_tbl.batch_id,
                                                                  rec_print_tbl.group_id,
                                                                  1,                     -- ProcessFlag
                                                                  p_user_id,
                                                                  l_responsibility_id,
                                                                  l_resp_application_id);

             COMMIT;
          END LOOP;
        END IF;


   EXCEPTION
   WHEN l_no_request_exp THEN
      NULL;
   WHEN program_error THEN
      ROLLBACK;
      l_err_code := 2;
      l_err_msg  := substr ( (l_err_msg || ' ERROR ' || substr(SQLERRM, 1, 2000)), 1, 3000);
      fnd_file.put_line(fnd_file.log,l_err_msg);
      dbms_output.put_line(l_err_msg);

      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => 'XXWC_DMS_SALES_RECEIPT_PRC'
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => substr(SQLERRM,1,2000)
                                          ,p_error_desc        => l_err_msg
                                          ,p_distribution_list => g_dflt_email
                                          ,p_module            => 'XXWC');

   WHEN OTHERS THEN
      l_err_code := 2;
      l_err_msg  := substr ( (l_err_msg || ' ERROR ' || substr(SQLERRM, 1, 2000)), 1, 3000);
      fnd_file.put_line(fnd_file.log,l_err_msg);
      dbms_output.put_line(l_err_msg);

      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => 'XXWC_DMS_SALES_RECEIPT_PRC'
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => substr(SQLERRM, 1, 2000)
                                          ,p_error_desc        => 'Error in B2B Customer xxwc_dms_sales_receipt_prc procedure, When Others Exception'
                                          ,p_distribution_list => g_dflt_email
                                          ,p_module            => 'XXWC');


       fnd_file.put_line(fnd_file.log,'Error in  xxwc_dms_sales_receipt_prc procedure - '||SQLERRM);
   END xxwc_dms_sales_receipt_prc;
/

GRANT EXECUTE ON APPS.xxwc_dms_sales_receipt_prc TO INTERFACE_APEXWC;