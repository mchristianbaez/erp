CREATE OR REPLACE PROCEDURE APPS.XXWC_EDI_PO_HIST_TXN_DISABLE (
   p_tp_header_id   IN     NUMBER,
   p_error_msg         OUT VARCHAR2)
/*************************************************************************
  $Header XXWC_EDI_PO_HISTORY_TXN_DISABLE.prc $
  Module Name : XXWC_EDI_PO_HISTORY_TXN_DISABLE

  PURPOSE     : Procedure to disable the history PO transactions being triggered for EDI when
              the site is configured for EDI
  TMS Task Id :  20140421-00041

  REVISIONS:
  Ver        Date        Author                Description
  ---------  ----------  ------------------    ----------------
  1.0        04/29/2014  Manjula Chellappan    Initial Version
  1.1        10/14/2014     Veera C               TMS#20141001-00168  Modified the code as per the 
                                               Canada OU Test
**************************************************************************/

AS
   PRAGMA AUTONOMOUS_TRANSACTION;

   -- parameters --

   l_tp_header_id   NUMBER := p_tp_header_id;

   -- Local variables--

   l_error_msg      VARCHAR2 (4000);
   l_select_po      VARCHAR2 (2000);


   CURSOR cur_po_history
   IS
--      SELECT po_header_id
--        FROM apps.xxwc_edi_po_history_txn_tbl
--       WHERE tp_header_id = l_tp_header_id;
SELECT po_header_id,
                 segment1 po_number,
                 tp_header_id,
                 ece_tp_location_code edi_location_code,
                 edi_processed_flag edi_processed_flag_before,
                 NULL edi_processed_flag_after,
                 print_count print_count_before,
                 NULL print_count_after,
                 printed_date printed_date_before,
                 NULL printed_date_after,
                 'N'  disabled_status,
                 SYSDATE disabled_date,
                 NULL error_msg
            FROM apps.po_headers a, apps.ap_supplier_sites b
           WHERE     1 = 1
             AND a.vendor_site_id = b.vendor_site_id
--             AND authorization_status = 'APPROVED'
             AND TRUNC(a.creation_date) < TRUNC(SYSDATE)
             AND edi_processed_flag IS NULL
--             AND print_count = 0
--             AND printed_date IS NULL
             AND b.tp_header_id = l_tp_header_id;

BEGIN

/*
   BEGIN
      l_select_po :=
         'INSERT INTO xxwc_edi_po_history_txn_tbl (  po_header_id        ,
   po_number           ,
   tp_header_id        ,
   edi_location_code    ,
   edi_processed_flag_before  ,
   edi_processed_flag_after  ,
   print_count_before  ,
   print_count_after   ,
   printed_date_before ,
   printed_date_after ,
   disabled_status     ,
   disabled_date       ,
   error_msg           )  
         (SELECT po_header_id,
                 segment1,
                 tp_header_id,
                 ece_tp_location_code,
                 edi_processed_flag,
                 NULL,
                 print_count,
                 NULL,
                 printed_date,
                 NULL,
                 ''N'',
                 SYSDATE,
                 NULL
            FROM apps.po_headers a, apps.ap_supplier_sites b
           WHERE     1 = 1
                 AND a.vendor_site_id = b.vendor_site_id
                 AND authorization_status = ''APPROVED''
                 AND a.creation_date < SYSDATE
                 AND edi_processed_flag IS NULL
                 AND print_count = 0
                 AND printed_date IS NULL
                 AND b.tp_header_id = :l_tp_header_id
                 )';
*/
--COMMIT;

--      DBMS_OUTPUT.put_line ('l_select_po ' || l_select_po);

---      EXECUTE IMMEDIATE l_select_po USING l_tp_header_id;

      FOR rec_po_history IN cur_po_history
      LOOP
         BEGIN
            UPDATE po_headers
               SET edi_processed_flag = 'Y',
                   print_count = 1,
                   printed_date = SYSDATE
             WHERE 1 = 1 AND po_header_id = rec_po_history.po_header_id;

            BEGIN
               UPDATE po_headers_archive
                  SET edi_processed_flag = 'Y',
                      print_count = 1,
                      printed_date = SYSDATE
                WHERE 1 = 1 AND po_header_id = rec_po_history.po_header_id;
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_error_msg :=
                        l_error_msg
                     || 'Error 4 : Can not update PO_HEADERS_ARCHIVE_ALL '
                     || SQLERRM;

                  UPDATE xxwc_edi_po_history_txn_tbl
                     SET error_msg = l_error_msg
                   WHERE 1 = 1 AND po_header_id = rec_po_history.po_header_id;

                  EXIT;
            END;

            BEGIN
               UPDATE xxwc_edi_po_history_txn_tbl
                  SET edi_processed_flag_after = 'Y',
                      print_count_after = 1,
                      printed_date_after = SYSDATE,
                      disabled_status = 'Y'
                WHERE 1 = 1 AND po_header_id = rec_po_history.po_header_id;
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_error_msg :=
                        l_error_msg
                     || 'Error 3 : Can not update xxwc.xxwc_edi_po_history_txn_tbl '
                     || SQLERRM;

                  UPDATE xxwc_edi_po_history_txn_tbl
                     SET error_msg = l_error_msg
                   WHERE 1 = 1 AND po_header_id = rec_po_history.po_header_id;

                  EXIT;
            END;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_error_msg :=
                     l_error_msg
                  || 'Error 2 : Can not update PO_HEADERS_ALL '
                  || SQLERRM;

               UPDATE xxwc_edi_po_history_txn_tbl
                  SET error_msg = l_error_msg
                WHERE 1 = 1 AND po_header_id = rec_po_history.po_header_id;

               EXIT;
         END;
      END LOOP;
   --END;

   IF l_error_msg IS NULL
   THEN
      COMMIT;
   ELSE
      p_error_msg := l_error_msg;
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      l_error_msg := l_error_msg || 'Error 1 : Select records ' || SQLERRM;
      p_error_msg := l_error_msg;
END;
/

SHO ERR
/