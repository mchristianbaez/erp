create or replace procedure apps.XXCUS_OZF_RBT_ONHOLD_PRC (retcode out varchar2, errbuf out varchar2, p_cal_year in varchar2, p_status_code in varchar2) as
/*
 -- Author: Balaguru Seshadri
 -- Parameters: None
 -- Modification History
 -- ESMS                TMS                           Date                    Version   Comments
 -- =========== ===============  ==========       =======  ========================
 -- 310030          20151214-00129  14-DEC-2015   1.0           Created.
 */ 
--
CURSOR OFFERS IS
    SELECT A.OFFER_ID, A.ORG_ID, A.OFFER_CODE, A.QP_LIST_HEADER_ID, A.OBJECT_VERSION_NUMBER OBJ_VER_NUM, b.description offer_name
    FROM OZF_OFFERS A, QP_LIST_HEADERS_VL B 
    WHERE 1 =1
    AND A.ORG_ID IN (101, 102)
    AND B.LIST_HEADER_ID =A.QP_LIST_HEADER_ID
    AND B.ATTRIBUTE7 =p_cal_year
    AND A.STATUS_CODE =p_status_code;
    --
    x_offer_adjustment_id   NUMBER;
    x_offer_adjst_tier_id   NUMBER;
    x_offer_adj_line_id     NUMBER;
    x_object_version_number NUMBER;
  
    x_qp_list_header_id NUMBER;
    x_err_location      NUMBER;
  
    l_offer_adj_rec        ozf_offer_adjustment_pvt.offer_adj_rec_type;
    l_offadj_tier_rec_type ozf_offer_adj_tier_pvt.offadj_tier_rec_type;
  
    l_offadj_line_rec_type ozf_offer_adj_line_pvt.offadj_line_rec_type;
  
    l_modifier_list_rec ozf_offer_pub.modifier_list_rec_type;
    l_modifier_line_tbl ozf_offer_pub.modifier_line_tbl_type;
    l_na_qualifier_tbl  ozf_offer_pub.na_qualifier_tbl_type;
    l_prod_rec_tbl      ozf_offer_pub.prod_rec_tbl_type;
    l_offer_tier_tbl    ozf_offer_pub.offer_tier_tbl_type;
    l_excl_rec_tbl      ozf_offer_pub.excl_rec_tbl_type;
    l_discount_line_tbl ozf_offer_pub.discount_line_tbl_type;
    l_act_product_tbl   ozf_offer_pub.act_product_tbl_type;
    l_vo_pbh_tbl        ozf_offer_pub.vo_disc_tbl_type;
    l_vo_dis_tbl        ozf_offer_pub.vo_disc_tbl_type;
    l_vo_prod_tbl       ozf_offer_pub.vo_prod_tbl_type;
    l_qualifier_tbl     ozf_offer_pub.qualifiers_tbl_type;
    l_vo_mo_tbl         ozf_offer_pub.vo_mo_tbl_type;
    l_budget_tbl        ozf_offer_pub.budget_tbl_type;
  
    l_object_version_number NUMBER;
    l_obj_version           NUMBER;
    l_start_date            DATE;
    l_offer_id              NUMBER;
    l_description           qp_list_headers_vl.description%TYPE;
    l_offer_type            VARCHAR2(50);
    l_user_status_id        NUMBER;
  
    x_errbuf  VARCHAR2(2000);
    x_retcode VARCHAR2(2000);
  
    x_return_status VARCHAR2(1);
    x_msg_count     NUMBER;
    x_msg_data      VARCHAR2(4000);
    --
 --
     procedure print_log(p_message in varchar2) is
     begin
      if fnd_global.conc_request_id >0 then
       fnd_file.put_line(fnd_file.log, p_message);
       fnd_file.put_line(fnd_file.output, p_message);       
      else
       dbms_output.put_line(p_message);  
      end if;
     end print_log;
     --    
BEGIN
 --
 print_log('');
 print_log('Parameters:'); 
 print_log('=========='); 
 print_log('   Calendar Year :'||p_cal_year); 
 print_log('   Status Code :'||p_status_code); 
 print_log(''); 
 --
 print_log('Begin of script to put rebate offers for '||p_cal_year||' ONHOLD.');
 --
 FOR rec in offers LOOP
  mo_global.set_policy_context('S', rec.org_id);
  SAVEPOINT here_we_start;
  
      l_modifier_list_rec.qp_list_header_id     :=rec.qp_list_header_id;
      l_modifier_list_rec.object_version_number :=rec.obj_ver_num;     
      l_modifier_list_rec.status_code           :='ONHOLD';
      l_modifier_list_rec.user_status_id        :=1607;
      l_modifier_list_rec.modifier_operation    :='UPDATE';
      l_modifier_list_rec.offer_operation       :='UPDATE';
      l_modifier_list_rec.offer_id              :=rec.offer_id;
    
      ozf_offer_pub.process_modifiers(p_init_msg_list     => fnd_api.g_false
                                     ,p_api_version       => 1.0
                                     ,p_commit            => fnd_api.g_false
                                     ,x_return_status     => x_return_status
                                     ,x_msg_count         => x_msg_count
                                     ,x_msg_data          => x_msg_data
                                     ,p_offer_type        => l_offer_type
                                     ,p_modifier_list_rec => l_modifier_list_rec
                                     ,p_modifier_line_tbl => l_modifier_line_tbl
                                     ,p_qualifier_tbl     => l_qualifier_tbl
                                     ,p_budget_tbl        => l_budget_tbl
                                     ,p_act_product_tbl   => l_act_product_tbl
                                     ,p_discount_tbl      => l_discount_line_tbl
                                     ,p_excl_tbl          => l_excl_rec_tbl
                                     ,p_offer_tier_tbl    => l_offer_tier_tbl
                                     ,p_prod_tbl          => l_prod_rec_tbl
                                     ,p_na_qualifier_tbl  => l_na_qualifier_tbl
                                     ,x_qp_list_header_id => x_qp_list_header_id
                                     ,x_error_location    => x_err_location);
    
      IF ((x_return_status = fnd_api.g_ret_sts_error) OR (x_return_status = fnd_api.g_ret_sts_unexp_error)) THEN        
        print_log('offer code ='||rec.offer_code||'['||rec.offer_name||']'||', failed to update ONHOLD, x_msg_count ='||x_msg_count||', x_msg_data ='|| x_msg_data);
        ROLLBACK TO here_we_start;      
      ELSE       
        print_log('offer code ='||rec.offer_code||'['||rec.offer_name||']'||', updated successfully to ONHOLD');            
        NULL;
      END IF;          
 END LOOP;
 --
 COMMIT;
 print_log('End of script to put rebate offers for '||p_cal_year||' ONHOLD.');
 --
EXCEPTION
 WHEN OTHERS THEN
  ROLLBACK;
  print_log ('Main Block, Error =>'||SQLERRM);
END;
/