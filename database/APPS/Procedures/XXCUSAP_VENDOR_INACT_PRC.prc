
  CREATE OR REPLACE PROCEDURE "APPS"."XXCUSAP_VENDOR_INACT_PRC" (p_user           IN VARCHAR2
                                                    ,p_responsibility IN VARCHAR2
                                                    ,p_org            IN VARCHAR2) IS

  /*******************************************************************************
  * Procedure:   xxcusap_vendor_inact_iface_prc
  * Description: This procedure is used by an APEX App (VENDOR_MASS_INACT) to execute the interface program to commit changes.
                 Apex App cannot directly execute the interface program due to Oracle
                 limitation on executing RPC over dblink.
  HISTORY
  ===============================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     21-Nov-2012        Kathy Poling    Initial creation of the procedure
  1.1     30-Apr-2013        Kathy Poling    Adding parmeters 
                                             SR 190380
  ********************************************************************************/

  --Intialize Variables
  l_err_msg      VARCHAR2(2000);
  l_err_code     NUMBER;
  l_sec          VARCHAR2(500);
  l_req_id       NUMBER;
  l_err_callfrom VARCHAR2(75) := 'XXCUSAP_VENDOR_INACT_PRC';
  l_distro_list  VARCHAR2(75) DEFAULT 'hdsoracledevelopers@hdsupply.com';

  v_phase       VARCHAR2(50);
  v_status      VARCHAR2(50);
  v_dev_status  VARCHAR2(50);
  v_dev_phase   VARCHAR2(50);
  v_message     VARCHAR2(250);
  v_supplier_id NUMBER;
  v_rec_cnt     NUMBER := 0;
  v_interval    NUMBER := 30; -- In seconds
  v_max_time    NUMBER := 7500; -- In seconds

  --Start Main Program

BEGIN
  l_sec := 'Start call to submit request; ';

  -- Call the procedure
  apps.xxcusap_vendor_inact_pkg.submit_job(errbuf           => l_err_msg
                                          ,retcode          => l_err_code
                                          ,p_user           => p_user
                                          ,p_responsibility => p_responsibility
                                          ,p_org            => p_org);

EXCEPTION
  WHEN program_error THEN
    ROLLBACK;
    l_err_code := 2;
    l_err_msg  := substr((l_err_msg || ' ERROR ' ||
                         substr(SQLERRM, 1, 2000))
                        ,1
                        ,3000);
    dbms_output.put_line(l_sec || ' - ' || l_err_msg);
    fnd_file.put_line(fnd_file.log, l_err_msg);
    fnd_file.put_line(fnd_file.output, l_err_msg);
  
    xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                        ,p_calling           => l_sec
                                        ,p_request_id        => l_req_id
                                        ,p_ora_error_msg     => substr(SQLERRM
                                                                      ,1
                                                                      ,2000)
                                        ,p_error_desc        => substr(l_err_msg
                                                                      ,1
                                                                      ,2000)
                                        ,p_distribution_list => l_distro_list
                                        ,p_module            => 'AP');
  
  WHEN OTHERS THEN
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
    l_err_code := 2;
    l_err_msg  := substr((l_err_msg || ' ERROR ' ||
                         substr(SQLERRM, 1, 2000))
                        ,1
                        ,3000);
    dbms_output.put_line(l_sec || ' - ' || l_err_msg);
    fnd_file.put_line(fnd_file.log, l_err_msg);
    fnd_file.put_line(fnd_file.output, l_err_msg);
  
    xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                        ,p_calling           => l_sec
                                        ,p_request_id        => l_req_id
                                        ,p_ora_error_msg     => substr(SQLERRM
                                                                      ,1
                                                                      ,2000)
                                        ,p_error_desc        => substr(l_err_msg
                                                                      ,1
                                                                      ,2000)
                                        ,p_distribution_list => l_distro_list
                                        ,p_module            => 'AP');
END xxcusap_vendor_inact_prc;
;
