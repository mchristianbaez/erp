/* Formatted on 1/7/2014 7:30:30 PM (QP5 v5.206) */
-- Start of DDL Script for Procedure APPS.XXWC_LIST_DIRECTORY
-- Generated 1/7/2014 7:30:29 PM from APPS@EBIZFQA

CREATE OR REPLACE PROCEDURE apps.xxwc_list_directory (directory IN VARCHAR2)
AS
    LANGUAGE JAVA
		/*this source used to read given directory(permission apply) content and insert into global temporary table
     xxwc.directory_listing, PROCEDURE apps.xxwc_list_directory (directory IN VARCHAR2) is using this java source
     how to use:
     1.populate temp table xxwc.directory_listing by executing:
        begin
          apps.xxwc_list_directory('/xx_iface');
        end;
      2.see directory content:
      select * from xxwc.directory_listing 	*/
    NAME 'XXWCDirectory_Listing.GetList( java.lang.String )';
/

-- End of DDL Script for Procedure APPS.XXWC_LIST_DIRECTORY
