/******************************************************************************
HISTORY
===============================================================================
VERSION DATE          AUTHOR(S)              DESCRIPTION
------- -----------   --------------- -----------------------------------------
1.0     01/09/2013    Lucidity Consulting      Initial Release
2.0     14/10/2014    Veera                    Added for TMS#20141001-00168 
3.0     12/04/2018    Ashwin Sridhar           Added Statistics for TMS#20180328-00040-AH HARRIS Pricing Conversion
********************************************************************************/
CREATE OR REPLACE PROCEDURE APPS.xxwc_qp_prl_conversion (RETCODE   OUT NUMBER
                                                       , ERRMSG    OUT VARCHAR2)
IS                                                
    cursor  cur_price_lists is
            select  distinct(name) price_list_name
                    , description
            from    xxwc.xxwc_prl_header_stg
            where   status is null;
            
    cursor  cur_price_lists_qualifiers (p_prl_name varchar2) is
            select  distinct(warehouse) warehouse
            from    xxwc.xxwc_prl_header_stg x1
            where   x1.name = p_prl_name
            and     x1.status is null
            and     x1.warehouse is not null
            order by 1;

    cursor  cur_list_lines (p_prl_name varchar2) is
            select  x1.rowid rid
                    , x1.*
            from    xxwc.xxwc_prl_lines_stg x1
            where   x1.prl_name = p_prl_name            
            and     x1.status is null            
            order by 1;

    --Line Error cursor Added by Ashwin.S on 12-Apr-2018 for TMS#20180328-00040
    cursor  cur_lin_err(p_request_id IN NUMBER) is
            select  x1.PRL_NAME
            ,       x1.PRODUCT_ATTRIBUTE
            ,       x1.PRODUCT_VALUE
            ,       x1.VALUE
            ,       x1.ERR_MESSAGE
            from    xxwc.xxwc_prl_lines_stg x1
            where   1=1         
            and     x1.status     ='ERROR'  
            and     x1.request_id = p_request_id
            order by 1;

    l_cnt                           NUMBER;
    i                               NUMBER;
    j                               NUMBER;  
    k                               NUMBER;        
    l_api_version_number            NUMBER         := 1.0;
    l_init_msg_list                 VARCHAR2(10)   := FND_API.G_FALSE;
    l_return_values                 VARCHAR2(10)   := FND_API.G_FALSE;
    l_commit                        VARCHAR2(10)   := FND_API.G_TRUE;
    l_x_return_status               VARCHAR2(255)  := 'S';
    l_x_msg_count                   NUMBER;
    l_x_msg_data                    VARCHAR2(255);
    g_user_id                       NUMBER;
    l_price_list_rec                QP_PRICE_LIST_PUB.PRICE_LIST_REC_TYPE          := QP_PRICE_LIST_PUB.G_MISS_PRICE_LIST_REC;
    l_price_list_val_rec            QP_PRICE_LIST_PUB.PRICE_LIST_VAL_REC_TYPE      := QP_PRICE_LIST_PUB.G_MISS_PRICE_LIST_VAL_REC;
    l_price_list_line_tbl           QP_PRICE_LIST_PUB.PRICE_LIST_LINE_TBL_TYPE     := QP_PRICE_LIST_PUB.G_MISS_PRICE_LIST_LINE_TBL; 
    l_price_list_line_val_tbl       QP_PRICE_LIST_PUB.PRICE_LIST_LINE_VAL_TBL_TYPE := QP_PRICE_LIST_PUB.G_MISS_PRICE_LIST_LINE_VAL_TBL;
    l_qualifiers_tbl                QP_QUALIFIER_RULES_PUB.QUALIFIERS_TBL_TYPE     := QP_PRICE_LIST_PUB.G_MISS_QUALIFIERS_TBL;
    l_qualifiers_val_tbl            QP_QUALIFIER_RULES_PUB.QUALIFIERS_VAL_TBL_TYPE := QP_PRICE_LIST_PUB.G_MISS_QUALIFIERS_VAL_TBL;
    l_pricing_attr_tbl              QP_PRICE_LIST_PUB.PRICING_ATTR_TBL_TYPE        := QP_PRICE_LIST_PUB.G_MISS_PRICING_ATTR_TBL;
    l_pricing_attr_val_tbl_type     QP_PRICE_LIST_PUB.PRICING_ATTR_VAL_TBL_TYPE    := QP_PRICE_LIST_PUB.G_MISS_PRICING_ATTR_VAL_TBL;
    l_x_price_list_rec              QP_PRICE_LIST_PUB.PRICE_LIST_REC_TYPE;
    l_x_price_list_val_rec          QP_PRICE_LIST_PUB.PRICE_LIST_VAL_REC_TYPE;
    l_x_price_list_line_tbl         QP_PRICE_LIST_PUB.PRICE_LIST_LINE_TBL_TYPE;
    l_x_price_list_line_val_tbl     QP_PRICE_LIST_PUB.PRICE_LIST_LINE_VAL_TBL_TYPE;
    l_x_qualifiers_tbl              QP_QUALIFIER_RULES_PUB.QUALIFIERS_TBL_TYPE;
    l_x_qualifiers_val_tbl          QP_QUALIFIER_RULES_PUB.QUALIFIERS_VAL_TBL_TYPE;
    l_x_pricing_attr_tbl            QP_PRICE_LIST_PUB.PRICING_ATTR_TBL_TYPE;
    l_x_pricing_attr_val_tbl        QP_PRICE_LIST_PUB.PRICING_ATTR_VAL_TBL_TYPE;
    
    l_error_message_list            ERROR_HANDLER.ERROR_TBL_TYPE;
    l_price_list_line               QP_PRICE_LIST_PUB.PRICE_LIST_LINE_REC_TYPE    := QP_PRICE_LIST_PUB.G_MISS_PRICE_LIST_LINE_REC;
    l_price_list                    VARCHAR2(3);
    gpr_msg_data                    VARCHAR2(2000);         
    l_pricing_att_rec               QP_PRICE_LIST_PUB.PRICING_ATTR_REC_TYPE := QP_PRICE_LIST_PUB.G_MISS_PRICING_ATTR_REC;
    l_valid                         VARCHAR2(1);
    l_warehouse_id                  VARCHAR2(30);
    l_structure_name                VARCHAR2(240);
    l_org_id                        NUMBER;  --14/OCt/2014 TMS#20141001-00168 Added by Veera
    ln_rec_cnt                      NUMBER:=0;  --Added for TMS#20180328-00040 by Ashwin.S on 12-Apr-2018
    ln_suc_cnt                      NUMBER:=0;  --Added for TMS#20180328-00040 by Ashwin.S on 12-Apr-2018
    ln_err_cnt                      NUMBER:=0;  --Added for TMS#20180328-00040 by Ashwin.S on 12-Apr-2018
    ln_line_err_cnt                 NUMBER:=0;  --Added for TMS#20180328-00040 by Ashwin.S on 12-Apr-2018
    ln_conc_request_id              NUMBER:=fnd_global.conc_request_id; --Added for TMS#20180328-00040 by Ashwin.S on 12-Apr-2018
    
BEGIN

    fnd_file.put_line (fnd_file.output, 'Starting Price List Conversion');
    fnd_file.put_line (fnd_file.output, '=====================================');

    --Added for TMS#20180328-00040 by Ashwin.S on 12-Apr-2018
    SELECT  COUNT (DISTINCT(name))
    INTO    ln_rec_cnt
    FROM    xxwc.xxwc_prl_header_stg
    WHERE   status IS NULL;

    fnd_file.put_line (fnd_file.output, 'Count of Price List Records to be processed '||ln_rec_cnt);

    oe_msg_pub.initialize;
    l_org_id:=mo_global.get_current_org_id;
    mo_global.set_policy_context ('S', l_org_id); --14/OCt/2014 TMS#20141001-00168 Added by Veera
    --mo_global.set_policy_context ('S', 162);  --14/OCt/2014 TMS#20141001-00168 Commented by Veera
    
    --mo_global.init ('ONT');
    fnd_global.apps_initialize(user_id        => fnd_global.user_id,
                               resp_id        => fnd_global.resp_id,
                               resp_appl_id   => fnd_global.resp_appl_id);
    
    g_user_id := fnd_global.user_id;
    
    IF(g_user_id = -1) THEN
        BEGIN
            SELECT user_id
            INTO   g_user_id 
            FROM   fnd_user
            WHERE  user_name = 'SYSADMIN';
        EXCEPTION
        WHEN OTHERS THEN
            g_user_id := -1;
        END;
    END IF;
    
    --fnd_file.put_line (fnd_file.output, 'user id = ' || g_user_id); 
    for c1 in cur_price_lists
    loop
        exit when cur_price_lists%notfound;
        
        fnd_file.put_line (fnd_file.output, 'Processing Price list: '||c1.price_list_name);
        
        l_price_list_rec := QP_PRICE_LIST_PUB.G_MISS_PRICE_LIST_REC;
        
        BEGIN
          SELECT list_header_id
          INTO   l_price_list_rec.list_header_id
          FROM   qp_list_headers
          WHERE  name = c1.price_list_name
          AND    active_flag = 'Y'
          AND    SYSDATE BETWEEN nvl(start_date_active, SYSDATE - 1) AND nvl(end_date_active, SYSDATE + 1);
          
          l_price_list_rec.operation := QP_GLOBALS.G_OPR_UPDATE;
        EXCEPTION
          WHEN OTHERS THEN
            l_price_list_rec.created_by      := g_user_id;
            l_price_list_rec.creation_date   := sysdate;
            
            SELECT qp_list_headers_b_s.nextval
            INTO   l_price_list_rec.list_header_id
            FROM   dual;
            
            l_price_list_rec.operation := QP_GLOBALS.G_OPR_CREATE;
        END;

        --Added for TMS#20180328-00040 by Ashwin.S on 12-Apr-2018
        UPDATE xxwc.xxwc_prl_header_stg
        SET    LIST_HEADER_ID = l_price_list_rec.list_header_id
        ,      REQUEST_ID     = ln_conc_request_id
        WHERE  status IS NULL
        AND    NAME = c1.price_list_name; 

        COMMIT;
        
        l_price_list_rec.currency_code      := 'USD';
        l_price_list_rec.last_updated_by    := g_user_id;
        l_price_list_rec.last_update_date   := SYSDATE;
        l_price_list_rec.list_type_code     := 'PRL';
        l_price_list_rec.rounding_factor    := -5;
        l_price_list_rec.name               := c1.price_list_name;
        l_price_list_rec.description        := c1.description;
        l_price_list_rec.start_date_active  := trunc(sysdate);
        l_price_list_rec.active_flag        := 'Y';
        l_price_list_rec.automatic_flag     := 'Y';
        l_price_list_rec.mobile_download    := 'N';
        --l_price_list_rec.context            := '162';   --14/OCt/2014 TMS#20141001-00168 Commented by Veera
        l_price_list_rec.context            := l_org_id;  --14/OCt/2014 TMS#20141001-00168 Added by Veera
        l_price_list_rec.global_flag        := 'Y';
        l_price_list_rec.source_system_code := 'QP';
        l_price_list_rec.pte_code           := 'ORDFUL';
        l_price_list_rec.attribute10        := 'Market Price List'; --'Price List';
        
        -- Validating and loading qualifiers
        j := 1;
        l_qualifiers_tbl         := QP_PRICE_LIST_PUB.G_MISS_QUALIFIERS_TBL;
        for c2 in cur_price_lists_qualifiers (c1.price_list_name) 
        loop
            exit when cur_price_lists_qualifiers%notfound;
            
            begin            
                l_warehouse_id := null;
                select  to_char(organization_id)
                into    l_warehouse_id
                from    mtl_parameters mp
                where   organization_code = c2.warehouse
                and     rownum = 1;
                
                l_qualifiers_tbl(j).list_header_id              := l_price_list_rec.list_header_id;
                l_qualifiers_tbl(j).excluder_flag               := 'N';
                l_qualifiers_tbl(j).qualifier_grouping_no       := j;
                l_qualifiers_tbl(j).qualifier_context           := 'ORDER'; 
                l_qualifiers_tbl(j).qualifier_attribute         := 'QUALIFIER_ATTRIBUTE18';
                l_qualifiers_tbl(j).qualifier_attr_value        := l_warehouse_id;
                l_qualifiers_tbl(j).comparison_operator_code    := '='; 
                l_qualifiers_tbl(j).qualifier_precedence        := '540';
                
                l_cnt := 0;
                begin
                    select  count(*)
                    into    l_cnt
                    from    qp_qualifiers
                    where   list_header_id = l_price_list_rec.list_header_id
                    and     list_line_id = -1
                    and     qualifier_context = 'ORDER'
                    and     qualifier_attribute = 'QUALIFIER_ATTRIBUTE18'
                    and     qualifier_attr_value = l_warehouse_id
                    and     comparison_operator_code = '='
                    and     qualifier_precedence = '540';
                exception
                when others then
                    l_cnt := 0;
                end;
                
                if l_cnt = 0 then
                    l_qualifiers_tbl(j).operation                   := QP_GLOBALS.G_OPR_CREATE;
                else
                    l_qualifiers_tbl(j).operation                   := QP_GLOBALS.G_OPR_UPDATE;
                end if; 
                
                j := j + 1;
                
                -- l_qualifiers_tbl(i).qualifier_attribute_desc := 'INVENTORY_ORG_ID';
                -- l_qualifiers_tbl(i).QUALIFIER_ATTR_VALUE_DESC := 'Mixed';
            exception
            when others then
                fnd_file.put_line (fnd_file.output, 'Invalid Warehouse value: '||c2.warehouse); 
            end;
            
        end loop;
        
        -- Validating and loading lines        
        i := 1;
        l_pricing_attr_tbl       := QP_PRICE_LIST_PUB.G_MISS_PRICING_ATTR_TBL;
        l_price_list_line_tbl    := QP_PRICE_LIST_PUB.G_MISS_PRICE_LIST_LINE_TBL;
        for c2 in cur_list_lines (c1.price_list_name)
        loop
            exit when cur_list_lines%notfound;

        --Added for TMS#20180328-00040 by Ashwin.S on 12-Apr-2018
        UPDATE xxwc.xxwc_prl_lines_stg
        SET    LIST_HEADER_ID = l_price_list_rec.list_header_id
        ,      REQUEST_ID     = ln_conc_request_id
        WHERE  status IS NULL
        AND    PRL_NAME = c1.price_list_name
        AND    rowid = c2.rid;
            
            l_price_list_line   := QP_PRICE_LIST_PUB.G_MISS_PRICE_LIST_LINE_REC;
            l_pricing_att_rec   := QP_PRICE_LIST_PUB.G_MISS_PRICING_ATTR_REC;
            l_valid             := null;
            
            l_price_list_line.list_header_id            := l_price_list_rec.list_header_id;
            l_price_list_line.primary_uom_flag          := 'Y';
            l_price_list_line.created_by                := g_user_id;
            l_price_list_line.creation_date             := sysdate;
            l_price_list_line.start_date_active         := trunc(sysdate);
            l_price_list_line.last_updated_by           := g_user_id;
            l_price_list_line.last_update_date          := sysdate;
            
            l_pricing_att_rec.list_header_id            := l_price_list_line.list_header_id;
            l_pricing_att_rec.created_by                := g_user_id;
            l_pricing_att_rec.creation_date             := sysdate;
            l_pricing_att_rec.last_updated_by           := g_user_id;
            l_pricing_att_rec.last_update_date          := sysdate;
            l_pricing_att_rec.pricing_phase_id          := 1;
            l_pricing_att_rec.price_list_line_index     := i;
            
            IF c2.product_attribute = 'Item Number' then 
            
                l_price_list_line.list_line_type_code       := 'PLL';
                l_price_list_line.product_precedence        := 220;
                
                if c2.call_for_price = 'Yes' then
                    --l_price_list_line.context               := '162';  --14/OCt/2014 TMS#20141001-00168 Commented by Veera
                    l_price_list_line.context               := l_org_id; --14/OCt/2014 TMS#20141001-00168 Added by Veera
                    l_price_list_line.attribute1            := 'Y';
                end if;
                
                if c2.value is not null and c2.formula_name is null then
                
                    l_price_list_line.operand                   := c2.value;
                    l_price_list_line.arithmetic_operator       := 'UNIT_PRICE';
                
                elsif c2.value is null and c2.formula_name is not null then
                
                    begin
                        select  price_formula_id
                        into    l_price_list_line.price_by_formula_id
                        from    qp_price_formulas_vl
                        where   name = c2.formula_name
                        and     rownum = 1;
                    exception
                    when others then
                        fnd_file.put_line (fnd_file.output, 'Invalid formula: '||c2.formula_name);
                        --fnd_file.put_line (fnd_file.output, 'Setting unit price to 1');
                        --l_price_list_line.operand                   := 1;
                        --l_price_list_line.arithmetic_operator       := 'UNIT_PRICE';
                        l_valid := 'N';
                        begin                    
                            update  xxwc.xxwc_prl_lines_stg
                            set     status = 'ERROR'
                            ,       ERR_MESSAGE = 'INVALID_FORMULA' --Added for TMS#20180328-00040 by Ashwin.S on 12-Apr-2018
                            ,       REQUEST_ID     = ln_conc_request_id --Added for TMS#20180328-00040 by Ashwin.S on 12-Apr-2018
                            where   rowid = c2.rid;
                        exception
                        when others then
                            null;
                        end;
                    end;
                
                else
                    fnd_file.put_line (fnd_file.output, 'Invalid Value or formula assignment...setting to 1');
                    l_price_list_line.operand                   := 1;
                    l_price_list_line.arithmetic_operator       := 'UNIT_PRICE';
                end if;
                
                l_pricing_att_rec.product_attribute_context := 'ITEM';
                l_pricing_att_rec.product_attribute         := 'PRICING_ATTRIBUTE1';
                l_pricing_att_rec.product_attribute_datatype:= 'C';
                
                begin
                    select  to_char(msib.inventory_item_id)
                            , msib.primary_uom_code
                    into    l_pricing_att_rec.product_attr_value
                            , l_pricing_att_rec.product_uom_code
                    from    mtl_system_items_b msib
                            , mtl_parameters mp
                    where   msib.segment1 = c2.product_value
                    and     msib.organization_id = mp.organization_id
                    and     mp.organization_code = 'MST';
                    
                    l_valid := 'Y';
                exception
                when others then
                    fnd_file.put_line (fnd_file.output, 'Invalid item number: '||c2.product_value);
                    l_valid := 'N';  
                    begin                    
                        update  xxwc.xxwc_prl_lines_stg
                        set     status = 'ERROR' 
                        ,       ERR_MESSAGE = 'INVALID_ITEM' --Added for TMS#20180328-00040 by Ashwin.S on 12-Apr-2018
                        ,       REQUEST_ID  = ln_conc_request_id --Added for TMS#20180328-00040 by Ashwin.S on 12-Apr-2018
                        where   rowid = c2.rid;
                    exception
                    when others then
                        null;
                    end;
                end;
            
            ELSIF c2.product_attribute in ('Item Category','Pricing Category') then
            
                l_price_list_line.list_line_type_code       := 'PLL';
                
                if c2.value is not null and c2.formula_name is null then
                
                    l_price_list_line.operand                   := c2.value;
                    l_price_list_line.arithmetic_operator       := 'UNIT_PRICE';
                
                elsif c2.value is null and c2.formula_name is not null then
                
                    begin
                        select  price_formula_id
                        into    l_price_list_line.price_by_formula_id
                        from    qp_price_formulas_vl
                        where   name = c2.formula_name
                        and     rownum = 1;
                    exception
                    when others then
                        fnd_file.put_line (fnd_file.output, 'Invalid formula: '||c2.formula_name);
                        --fnd_file.put_line (fnd_file.output, 'Setting unit price to 1');
                        --l_price_list_line.operand                   := 1;
                        --l_price_list_line.arithmetic_operator       := 'UNIT_PRICE';
                        l_valid := 'N';
                        begin                    
                            update  xxwc.xxwc_prl_lines_stg
                            set     status = 'ERROR' 
                            ,       ERR_MESSAGE = 'INVALID_FORMULA'  --Added for TMS#20180328-00040 by Ashwin.S on 12-Apr-2018
                            ,       REQUEST_ID  = ln_conc_request_id --Added for TMS#20180328-00040 by Ashwin.S on 12-Apr-2018
                            where   rowid = c2.rid;
                        exception
                        when others then
                            null;
                        end;
                    end;
                
                else
                    fnd_file.put_line (fnd_file.output, 'Invalid Value or formula assignment...setting to 1');
                    l_price_list_line.operand                   := 1;
                    l_price_list_line.arithmetic_operator       := 'UNIT_PRICE';
                end if;
                
                l_pricing_att_rec.product_attribute_context := 'ITEM';
                l_pricing_att_rec.product_attribute_datatype:= 'C';
                
                begin
                    l_structure_name := null;
                    
                    select  to_char(category_id)
                            , structure_name
                    into    l_pricing_att_rec.product_attr_value
                            , l_structure_name
                    from    mtl_categories_v
                    where   category_concat_segs = c2.product_value
                    and     enabled_flag = 'Y'
                    and     rownum = 1;
                    
                    l_pricing_att_rec.product_uom_code := 'EA';
                    
                    if l_structure_name = 'Pricing Category' then
                        l_pricing_att_rec.product_attribute         := 'PRICING_ATTRIBUTE27';
                        l_price_list_line.product_precedence        := 250;
                    else
                        l_pricing_att_rec.product_attribute         := 'PRICING_ATTRIBUTE2';
                        l_price_list_line.product_precedence        := 290;
                    end if;
                    
                    l_valid := 'Y';
                exception
                when others then
                    fnd_file.put_line (fnd_file.output, 'Invalid item category: '||c2.product_value);
                    l_valid := 'N';
                    
                    begin                    
                        update  xxwc.xxwc_prl_lines_stg
                        set     status = 'ERROR'
                        ,       ERR_MESSAGE = 'INVALID_CAT' --Added for TMS#20180328-00040 by Ashwin.S on 12-Apr-2018
                        ,       REQUEST_ID  = ln_conc_request_id --Added for TMS#20180328-00040 by Ashwin.S on 12-Apr-2018
                        where   rowid = c2.rid;
                    exception
                    when others then
                        null;
                    end;
                end;
            
            ELSE
                fnd_file.put_line (fnd_file.output, 'Invalid product attribute: '||c2.product_attribute);
                l_valid := 'N';  
                
                begin                    
                    update  xxwc.xxwc_prl_lines_stg
                    set     status = 'ERROR' 
                    ,       ERR_MESSAGE = 'INVALID_PROD_ATTR' --Added for TMS#20180328-00040 by Ashwin.S on 12-Apr-2018
                    ,       REQUEST_ID  = ln_conc_request_id --Added for TMS#20180328-00040 by Ashwin.S on 12-Apr-2018
                    where   rowid = c2.rid;
                exception
                when others then
                    null;
                end;
                
            END IF;
            
            l_pricing_att_rec.list_line_id              := FND_API.G_MISS_NUM;
            
            SELECT qp_pricing_attributes_s.nextval
            INTO   l_pricing_att_rec.pricing_attribute_id
            FROM   dual;
            
            l_price_list_line.operation                 := QP_GLOBALS.G_OPR_CREATE;
            l_pricing_att_rec.operation                 := QP_GLOBALS.G_OPR_CREATE;
            
            if l_valid = 'Y' then 
                l_pricing_attr_tbl(i)    := l_pricing_att_rec;
                l_price_list_line_tbl(i) := l_price_list_line;
                i := i + 1;
            end if;
            
            IF(i > 1500) THEN
                fnd_file.put_line (fnd_file.output, 'Adding ' || i || ' price lines to price list');
            
                qp_price_list_pub.process_price_list( p_api_version_number      => l_api_version_number,
                                                      p_init_msg_list           => l_init_msg_list,
                                                      p_return_values           => l_return_values,
                                                      p_commit                  => l_commit,
                                                      x_return_status           => l_x_return_status,
                                                      x_msg_count               => l_x_msg_count,
                                                      x_msg_data                => l_x_msg_data,
                                                      p_price_list_rec          => l_price_list_rec,
                                                      p_price_list_val_rec      => l_price_list_val_rec,
                                                      p_price_list_line_tbl     => l_price_list_line_tbl,
                                                      p_price_list_line_val_tbl => l_price_list_line_val_tbl,
                                                      p_qualifiers_tbl          => l_qualifiers_tbl,
                                                      p_qualifiers_val_tbl      => l_qualifiers_val_tbl,
                                                      p_pricing_attr_tbl        => l_pricing_attr_tbl,
                                                      p_pricing_attr_val_tbl    => l_pricing_attr_val_tbl_type,
                                                      x_price_list_rec          => l_x_price_list_rec,
                                                      x_price_list_val_rec      => l_x_price_list_val_rec,
                                                      x_price_list_line_tbl     => l_x_price_list_line_tbl,
                                                      x_price_list_line_val_tbl => l_x_price_list_line_val_tbl,
                                                      x_qualifiers_tbl          => l_x_qualifiers_tbl, 
                                                      x_qualifiers_val_tbl      => l_x_qualifiers_val_tbl,
                                                      x_pricing_attr_tbl        => l_x_pricing_attr_tbl,
                                                      x_pricing_attr_val_tbl    => l_x_pricing_attr_val_tbl);
                    
                fnd_file.put_line (fnd_file.output, 'Return status = ' || l_x_return_status);
                  
                IF(l_x_return_status <> 'S') THEN
                    --  Error Processing
                    fnd_file.put_line (fnd_file.output, '  Message Count       = ' || l_x_msg_count);
                  
                    FOR K IN 1 .. l_x_msg_count LOOP 
                      gpr_msg_data := oe_msg_pub.get(p_msg_index => k, 
                                                     p_encoded => 'F'); 
                      fnd_file.put_line (fnd_file.output, TO_CHAR(i)||' MESSAGE TEXT  '|| gpr_msg_data);        
                    END LOOP;
                    RETURN;
                
                ELSE

                    --Added for TMS#20180328-00040 by Ashwin.S on 12-Apr-2018
                    UPDATE  xxwc.xxwc_prl_lines_stg
                    SET     status = 'LOADED'
                    ,       REQUEST_ID  = ln_conc_request_id                     
                    WHERE   rowid = c2.rid;

                    fnd_file.put_line (fnd_file.output, 'Loaded');
                    fnd_file.put_line (fnd_file.output, '************************');
                    fnd_file.put_line (fnd_file.output, 'List Line Table Type:');
                    fnd_file.put_line (fnd_file.output, ' ');
                    IF l_x_price_list_line_tbl.count > 0 THEN
                        FOR k in 1 .. l_x_price_list_line_tbl.count LOOP
                            fnd_file.put_line (fnd_file.output, 'Record = '|| k || 'Return Status = '|| l_x_price_list_line_tbl(k).return_status);
                            fnd_file.put_line (fnd_file.output, 'Record = '|| k || 'list_header_id = '|| l_x_price_list_line_tbl(k).list_header_id);
                            fnd_file.put_line (fnd_file.output, 'Record = '|| k || 'list_line_id = '|| l_x_price_list_line_tbl(k).list_line_id);
                            fnd_file.put_line (fnd_file.output, ' ');
                        END LOOP;
                    END IF;
                    
                    fnd_file.put_line (fnd_file.output, '************************');
                    fnd_file.put_line (fnd_file.output, 'Pricing Attribute Table Type:');
                    fnd_file.put_line (fnd_file.output, ' ');
                    IF l_x_pricing_attr_tbl.count > 0 THEN
                        FOR k in 1 .. l_x_pricing_attr_tbl.count LOOP
                            fnd_file.put_line (fnd_file.output, 'Record = '|| k || 'Return Status = '|| l_x_pricing_attr_tbl(k).return_status);
                            fnd_file.put_line (fnd_file.output, 'Record = '|| k || 'list_header_id = '|| l_x_pricing_attr_tbl(k).list_header_id);
                            fnd_file.put_line (fnd_file.output, 'Record = '|| k || 'list_line_id = '|| l_x_pricing_attr_tbl(k).list_line_id);
                            fnd_file.put_line (fnd_file.output, 'Record = '|| k || 'product_attribute = '|| l_x_pricing_attr_tbl(k).product_attribute);
                            fnd_file.put_line (fnd_file.output, 'Record = '|| k || 'product_attr_value = '|| l_x_pricing_attr_tbl(k).product_attr_value);
                            fnd_file.put_line (fnd_file.output, ' ');                        
                        end loop;
                    END IF;                                        
                    
                END IF;
                  
                l_price_list_rec.operation := QP_GLOBALS.G_OPR_UPDATE;
                COMMIT; 
                    
                i := 1;
                l_pricing_attr_tbl       := QP_PRICE_LIST_PUB.G_MISS_PRICING_ATTR_TBL;
                l_price_list_line_tbl    := QP_PRICE_LIST_PUB.G_MISS_PRICE_LIST_LINE_TBL; 
            END IF;
            
        end loop;
        --Loop End for Price List Lines...
        
        IF(i NOT IN (0, 1)) THEN 

            fnd_file.put_line (fnd_file.output, 'Adding ' || i || ' price lines to price list');
        
            qp_price_list_pub.process_price_list(  p_api_version_number      => l_api_version_number,
                                                   p_init_msg_list           => l_init_msg_list,
                                                   p_return_values           => l_return_values,
                                                   p_commit                  => l_commit,
                                                   x_return_status           => l_x_return_status,
                                                   x_msg_count               => l_x_msg_count,
                                                   x_msg_data                => l_x_msg_data,
                                                   p_price_list_rec          => l_price_list_rec,
                                                   p_price_list_val_rec      => l_price_list_val_rec,
                                                   p_price_list_line_tbl     => l_price_list_line_tbl,
                                                   p_price_list_line_val_tbl => l_price_list_line_val_tbl,
                                                   p_qualifiers_tbl          => l_qualifiers_tbl,
                                                   p_qualifiers_val_tbl      => l_qualifiers_val_tbl,
                                                   p_pricing_attr_tbl        => l_pricing_attr_tbl,
                                                   p_pricing_attr_val_tbl    => l_pricing_attr_val_tbl_type,
                                                   x_price_list_rec          => l_x_price_list_rec,
                                                   x_price_list_val_rec      => l_x_price_list_val_rec,
                                                   x_price_list_line_tbl     => l_x_price_list_line_tbl,
                                                   x_price_list_line_val_tbl => l_x_price_list_line_val_tbl,
                                                   x_qualifiers_tbl          => l_x_qualifiers_tbl, 
                                                   x_qualifiers_val_tbl      => l_x_qualifiers_val_tbl,
                                                   x_pricing_attr_tbl        => l_x_pricing_attr_tbl,
                                                   x_pricing_attr_val_tbl    => l_x_pricing_attr_val_tbl);
              
            fnd_file.put_line (fnd_file.output, 'Return status = ' || l_x_return_status);
            
            IF(l_x_return_status <> 'S') THEN
                --  Error Processing
                fnd_file.put_line (fnd_file.output, '  Message Count       = ' || l_x_msg_count);
          
                FOR K IN 1 .. l_x_msg_count 
                LOOP 
                    gpr_msg_data := oe_msg_pub.get(p_msg_index => k, p_encoded => 'F'); 
                    fnd_file.put_line (fnd_file.output, TO_CHAR(i)||' MESSAGE TEXT  '|| gpr_msg_data);        
                END LOOP;
                
                RETURN;
            
            ELSE
            
                fnd_file.put_line (fnd_file.output, 'Loaded');
                fnd_file.put_line (fnd_file.output, '************************');
                fnd_file.put_line (fnd_file.output, 'List Line Table Type:');
                fnd_file.put_line (fnd_file.output, ' ');
                IF l_x_price_list_line_tbl.count > 0 THEN
                    FOR k in 1 .. l_x_price_list_line_tbl.count LOOP

              --Added for TMS#20180328-00040 by Ashwin.S on 12-Apr-2018
              UPDATE  xxwc.xxwc_prl_lines_stg
              SET     status = 'LOADED' 
              ,       REQUEST_ID  = ln_conc_request_id   
              WHERE   list_header_id = l_price_list_rec.list_header_id
              AND     status IS NULL;

                        fnd_file.put_line (fnd_file.output, 'Record = '|| k || 'Return Status = '|| l_x_price_list_line_tbl(k).return_status);
                        fnd_file.put_line (fnd_file.output, 'Record = '|| k || 'list_header_id = '|| l_x_price_list_line_tbl(k).list_header_id);
                        fnd_file.put_line (fnd_file.output, 'Record = '|| k || 'list_line_id = '|| l_x_price_list_line_tbl(k).list_line_id);
                        fnd_file.put_line (fnd_file.output, ' ');
                    END LOOP;
                END IF;
                    
                fnd_file.put_line (fnd_file.output, '************************');
                fnd_file.put_line (fnd_file.output, 'Pricing Attribute Table Type:');
                fnd_file.put_line (fnd_file.output, ' ');
                IF l_x_pricing_attr_tbl.count > 0 THEN
                    FOR k in 1 .. l_x_pricing_attr_tbl.count LOOP
                        fnd_file.put_line (fnd_file.output, 'Record = '|| k || 'Return Status = '|| l_x_pricing_attr_tbl(k).return_status);
                        fnd_file.put_line (fnd_file.output, 'Record = '|| k || 'list_header_id = '|| l_x_pricing_attr_tbl(k).list_header_id);
                        fnd_file.put_line (fnd_file.output, 'Record = '|| k || 'list_line_id = '|| l_x_pricing_attr_tbl(k).list_line_id);
                        fnd_file.put_line (fnd_file.output, 'Record = '|| k || 'product_attribute = '|| l_x_pricing_attr_tbl(k).product_attribute);
                        fnd_file.put_line (fnd_file.output, 'Record = '|| k || 'product_attr_value = '|| l_x_pricing_attr_tbl(k).product_attr_value);
                        fnd_file.put_line (fnd_file.output, ' ');                        
                    end loop;
                END IF;
                
            END IF;
            
            l_price_list_rec.operation := QP_GLOBALS.G_OPR_UPDATE;
            
            COMMIT;
        END IF;
        
        IF (l_x_return_status = 'S') THEN
            -- updating loading pricelist lines
            update  xxwc.xxwc_prl_header_stg
            set     status = 'LOADED'
            where   name = c1.price_list_name
            and     status is null;

        --Added for TMS#20180328-00040 by Ashwin.S on 12-Apr-2018
        ELSE

            update  xxwc.xxwc_prl_header_stg
            set     status = 'ERROR'
            where   name = c1.price_list_name
            and     status is null;
     
        END IF;                                
           
    end loop;
    --Loop End for Headers...
    
    commit;

  --Added for TMS#20180328-00040 by Ashwin.S on 12-Apr-2018
  SELECT  COUNT (DISTINCT(name))
  INTO    ln_suc_cnt
  FROM    xxwc.xxwc_prl_header_stg
  WHERE   status='LOADED'
  AND     REQUEST_ID  = ln_conc_request_id;

  fnd_file.put_line (fnd_file.output, 'Count of Price List Records successfully processed '||ln_suc_cnt);
  
  --Added for TMS#20180328-00040 by Ashwin.S on 12-Apr-2018
  SELECT  COUNT (DISTINCT(name))
  INTO    ln_err_cnt
  FROM    xxwc.xxwc_prl_header_stg
  WHERE   status='ERROR'
  AND     REQUEST_ID  = ln_conc_request_id;
  
  fnd_file.put_line (fnd_file.output, 'Count of Price List Records Error '||ln_err_cnt);
  
  --Added for TMS#20180328-00040 by Ashwin.S on 12-Apr-2018
  SELECT COUNT(1)
  INTO   ln_line_err_cnt
  FROM   xxwc.xxwc_prl_lines_stg
  WHERE  status = 'ERROR'
  AND    REQUEST_ID  = ln_conc_request_id; 
  
  fnd_file.put_line (fnd_file.output, 'Count of Price List Line Records Error '||ln_line_err_cnt);
  
  --Added for TMS#20180328-00040 by Ashwin.S on 12-Apr-2018
  IF ln_line_err_cnt>0 THEN
  
    fnd_file.put_line (fnd_file.output, '                              ');
    fnd_file.put_line (fnd_file.output, 'Summary of Line Records Error ');
    fnd_file.put_line (fnd_file.output, '==============================');
    fnd_file.put_line (fnd_file.output,RPAD('Price List Name',100)||RPAD('Product Attibute',50)||RPAD('Product Value',40)||RPAD('Value',30)||RPAD('Error Message',100));
    fnd_file.put_line (fnd_file.output, RPAD('-',320,'-'));

    FOR rec_lin_err IN cur_lin_err(ln_conc_request_id) LOOP
    
      fnd_file.put_line (fnd_file.output,RPAD(rec_lin_err.PRL_NAME,100)||RPAD(rec_lin_err.PRODUCT_ATTRIBUTE,50)||RPAD(rec_lin_err.PRODUCT_VALUE,40)
                                       ||RPAD(rec_lin_err.VALUE,30)||RPAD(rec_lin_err.ERR_MESSAGE,100));
    
    END LOOP;  

  END IF;
  
EXCEPTION
   WHEN OTHERS
   THEN
      fnd_file.put_line (fnd_file.output, 'Exception Occured :');
      fnd_file.put_line (fnd_file.output, SQLCODE || ':' || SQLERRM);
      fnd_file.put_line (fnd_file.output, '=====================================');
END xxwc_qp_prl_conversion;
/