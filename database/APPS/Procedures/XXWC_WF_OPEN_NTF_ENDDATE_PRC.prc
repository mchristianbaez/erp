CREATE OR REPLACE PROCEDURE APPS.XXWC_WF_OPEN_NTF_ENDDATE_PRC (
   xerrbuf             OUT VARCHAR2,
   xretcode            OUT VARCHAR2,
   p_rec_limit      IN     NUMBER,
   p_commit_limit   IN     NUMBER)
IS
   /**************************************************************************************************************************
   TMS#20160727-00097  Issue with WF OMERROR build up on daily basis - running 5X longer than should- Non Prod instances
   Creatd by : P.Vamshidhar
   Date: 13-Aug-2016
   Purpose:  Data fix to abort workflow notifications.
   Procedure Name: XXWC_WF_OPEN_NTF_ENDDATE_PRC.sql
   /**************************************************************************************************************************/
   lncount   NUMBER := 0;

   CURSOR CUR_WF_ITEMS (
      cp_rec_limit    NUMBER)
   IS
        SELECT *
          FROM apps.wf_items
         WHERE     item_type = 'OMERROR'
               AND end_date IS NULL
               --AND ITEM_KEY IN ('WF3046530', 'WF3046521', 'WF3046523')
               AND TRUNC (begin_date) >= TRUNC (SYSDATE) - 1000
               AND ROWNUM < cp_rec_limit
      ORDER BY begin_date ASC;
BEGIN
   FND_FILE.PUT_LINE (
      FND_FILE.LOG,
      'Starting abort script.' || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));

   FOR I IN CUR_WF_ITEMS (p_rec_limit)
   LOOP
      lncount := lncount + 1;
      WF_ENGINE.abortProcess ('OMERROR', i.item_key);

      IF MOD (lncount, p_commit_limit) = 0
      THEN
         FND_FILE.PUT_LINE (
            FND_FILE.LOG,
               'Processed a thousand rows. Committing. Current count: '
            || lncount);
         COMMIT;
      END IF;
   END LOOP;


   FND_FILE.PUT_LINE (
      FND_FILE.LOG,
         'Finished abort script. Final commit. Processed this many lines: '
      || lncount
      || ' - '
      || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));


   COMMIT;
   xretcode := 0;
EXCEPTION
   WHEN OTHERS
   THEN
      FND_FILE.PUT_LINE (FND_FILE.LOG,
                         'Error Occured ' || SUBSTR (SQLERRM, 1, 500));
      xretcode := 2;
END;
/