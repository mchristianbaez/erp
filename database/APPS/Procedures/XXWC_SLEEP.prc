 
--run as apps, create this procedure, as xxeis user has not rights to call DBMS_LOCK package 
create or replace procedure apps.xxwc_sleep(p_seconds number)
is
begin
DBMS_LOCK.sleep(p_seconds);
end;

--after creating the procedure grant it to xxeis user
grant execute on apps.xxwc_sleep to xxeis
 
