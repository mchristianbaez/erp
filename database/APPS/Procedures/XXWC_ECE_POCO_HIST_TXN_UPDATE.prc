/* Formatted on 2/6/2015 2:27:35 PM (QP5 v5.254.13202.43190) */
CREATE OR REPLACE PROCEDURE apps.xxwc_ece_poco_hist_txn_update (
   p_vendor_site_id   IN NUMBER)
/*************************************************************************
  $Header XXWC_ECE_POCO_HIST_TXN_UPDATE.prc $
  Module Name : XXWC_ECE_POCO_HIST_TXN_UPDATE

  PURPOSE     : Procedure to update the purchase order flags to avoid EDI transmission
  TMS Task Id : 20141218-00011

  REVISIONS:
  Ver        Date        Author                Description
  ---------  ----------  ------------------    ----------------
  1.0        04-Feb-2015  Manjula Chellappan    Initial Version
**************************************************************************/

AS
   -- parameters --

   l_vendor_site_id      NUMBER := p_vendor_site_id;

   -- Local variables--

   l_error_msg           VARCHAR2 (4000);
   l_insert_po_stg       VARCHAR2 (4000);
   l_update_po_stg       VARCHAR2 (4000);
   l_update_po_stg_err   VARCHAR2 (4000);
   l_update_po_archive   VARCHAR2 (4000);
   l_update_po           VARCHAR2 (4000);
BEGIN
   l_insert_po_stg :=
      'INSERT INTO apps.xxwc_ece_poco_archive_tbl (po_header_id,
                                               po_number,
                                               revision_num,
                                               creation_date,
                                               last_update_date,
                                               approved_date,
                                               printed_date,
                                               print_count,
                                               edi_processed_flag,
                                               vendor_id,
                                               vendor_num,
                                               vendor_name,
                                               vendor_site_id,
                                               edi_location_code,
                                               vendor_site_code,
                                               status,
                                               error_msg,
                                               printed_date_after,
                                               print_count_after,
                                               edi_processed_flag_after)
      (SELECT po_header_id,
              po_number,
              revision_num,
              creation_date,
              last_update_date,
              approved_date,
              printed_date,
              print_count,
              edi_processed_flag,
              vendor_id,
              vendor_num,
              vendor_name,
              vendor_site_id,
              edi_location_code,
              vendor_site_code,
              ''I'',
              NULL,
              NULL,
              NULL,
              NULL
         FROM apps.xxwc_ece_poco_headers_vw
        WHERE     vendor_site_id = :l_vendor_site_id)';


   l_update_po_archive :=
      'UPDATE po_headers_archive
      SET edi_processed_flag = ''Y'',
          print_count = print_count + 1,
          printed_date = SYSDATE
    WHERE     1 = 1
          AND latest_external_flag = ''Y''
          AND po_header_id IN (SELECT po_header_id
                                 FROM apps.xxwc_ece_poco_archive_tbl
                                WHERE vendor_site_id = :l_vendor_site_id)';


   l_update_po :=
      'UPDATE po_headers
      SET print_count = print_count + 1, printed_date = SYSDATE
    WHERE     1 = 1          
          AND po_header_id IN (SELECT po_header_id
                                 FROM apps.xxwc_ece_poco_archive_tbl
                                WHERE vendor_site_id = :l_vendor_site_id)';


   l_update_po_stg := 'UPDATE apps.xxwc_ece_poco_archive_tbl
      SET printed_date_after = SYSDATE,
          print_count_after = print_count + 1,
          edi_processed_flag_after = ''Y'',
          status=''P''          
    WHERE vendor_site_id = :l_vendor_site_id';


   l_update_po_stg_err := 'UPDATE apps.xxwc_ece_poco_archive_tbl
      SET status =''E'',
      error_msg = :l_error_msg          
    WHERE vendor_site_id = :l_vendor_site_id';



   BEGIN
      EXECUTE IMMEDIATE l_insert_po_stg USING l_vendor_site_id;

      BEGIN
         EXECUTE IMMEDIATE l_update_po_archive USING l_vendor_site_id;

         BEGIN
            EXECUTE IMMEDIATE l_update_po USING l_vendor_site_id;

            BEGIN
               EXECUTE IMMEDIATE l_update_po_stg USING l_vendor_site_id;
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_error_msg :=
                        'Error in updatng XXWC_ECE_POCO_ARCHIVE_TBL Table'
                     || SQLERRM;
            END;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_error_msg := 'Error in updatng PO_HEADERS Table' || SQLERRM;
         END;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_error_msg :=
               'Error in updatng PO_HEADERS_ARCHIVE Table' || SQLERRM;
      END;
   EXCEPTION
      WHEN OTHERS
      THEN
         l_error_msg :=
               'Error in Inserting into XXWC_ECE_POCO_ARCHIVE_TBL Table'
            || SQLERRM;
   END;


   IF l_error_msg IS NULL
   THEN
      COMMIT;
   ELSE
      ROLLBACK;

      raise_application_error (
         -20000,
            'History POCOs are not disabled for EDI : '
         || SUBSTR (l_error_msg, 1, 150));
   END IF;
END;
/

SHO ERR
/