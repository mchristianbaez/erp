CREATE OR REPLACE PROCEDURE apps.xxwc_pdh_image_load_prc
                                     (p_errbuf               OUT VARCHAR2
                                     ,p_retcode              OUT NUMBER
                                     ,p_directory_path       IN  VARCHAR2
                                     ,p_user_name            IN  VARCHAR2
                                     ,p_responsibility_name  IN  VARCHAR2)
--//============================================================================
--//
--// Object Name         :: xxwc_pdh_image_load_prc
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This procedure will populate the table
--//                        xxwc.xxwc_ego_image_filenames.
--//
--// Version Control
--//============================================================================
--// Vers    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Mahesh Kudle     15/03/2014    Initial Build - TMS#20140207-00083
--//============================================================================
AS

--//============================================================================
--// Local variable declaration
--//============================================================================
l_responsibility_id     NUMBER;
l_application_id        NUMBER;
l_user_id               NUMBER;
l_request_id            NUMBER;
v_phase                 VARCHAR2 (50);
v_status                VARCHAR2 (50);
v_dev_status            VARCHAR2 (50);
v_dev_phase             VARCHAR2 (50);
v_message               VARCHAR2 (250);
v_error_message         VARCHAR2 (3000);
l_err_msg               VARCHAR2 (3000);
lv_rec_cnt              NUMBER;
l_dir_exists            VARCHAR2 (3000);
l_resp_application_id   NUMBER;
l_program_error         EXCEPTION;

CURSOR cur_tmp IS
SELECT * FROM xxwc.xxwc_pdh_image_filenames_tmp;

BEGIN

  DBMS_OUTPUT.put_line ('Entering xxwc_pdh_image_load_prc...');
  DBMS_OUTPUT.put_line ('p_user_name: ' || p_user_name);
  DBMS_OUTPUT.put_line ('p_responsibility_name: ' || p_responsibility_name);
  DBMS_OUTPUT.put_line ('p_ob_directory_name: ' || p_directory_path);

  -- Resetting verification variable
  l_user_id := NULL;
  l_responsibility_id := NULL;
  l_resp_application_id := NULL;


  EXECUTE IMMEDIATE 'TRUNCATE TABLE xxwc.xxwc_pdh_image_filenames_tmp';

  --//==========================================================================
  --// Validate directory path exists
  --//==========================================================================
  l_dir_exists := NULL;

  BEGIN
    SELECT directory_path
      INTO l_dir_exists
      FROM all_directories
     WHERE directory_name = p_directory_path;
  EXCEPTION
  WHEN NO_DATA_FOUND THEN
    l_err_msg := 'Directory does not exist in Oracle : '||p_directory_path;
    DBMS_OUTPUT.put_line (l_err_msg);
    RAISE l_program_error;
  WHEN OTHERS THEN
    l_err_msg := 'Error validating directory   :'||SQLERRM;
    RAISE l_program_error;
  END;

  --//==========================================================================
  --// Validate User ID
  --//==========================================================================
  BEGIN
    SELECT user_id
      INTO l_user_id
      FROM fnd_user
     WHERE 1 = 1
       AND user_name = UPPER(p_user_name)
       AND SYSDATE BETWEEN start_date AND NVL(end_date, TRUNC(SYSDATE) + 1);
  EXCEPTION
  WHEN NO_DATA_FOUND THEN
     l_err_msg := 'UserName - '||p_user_name||' not defined in Oracle';
     DBMS_OUTPUT.put_line (l_err_msg);
     RAISE l_program_error;
  WHEN OTHERS THEN
     l_err_msg := 'Error deriving user_id for UserName - '||p_user_name;
     DBMS_OUTPUT.put_line (l_err_msg);
     RAISE l_program_error;
  END;

  --//==========================================================================
  --// Validate ResponsibilityId and ResponsibilityApplicationId
  --//==========================================================================
  BEGIN
    SELECT responsibility_id
         , application_id
      INTO l_responsibility_id
         , l_resp_application_id
      FROM fnd_responsibility_vl
     WHERE responsibility_name = p_responsibility_name
       AND SYSDATE BETWEEN start_date AND NVL(end_date, TRUNC(SYSDATE) + 1);
  EXCEPTION
  WHEN NO_DATA_FOUND THEN
     l_err_msg := 'Responsibility - '||p_responsibility_name||' not defined in Oracle';
     DBMS_OUTPUT.put_line (l_err_msg);
     RAISE l_program_error;
  WHEN OTHERS THEN
     l_err_msg := 'Error deriving Responsibility_id for ResponsibilityName - '||p_responsibility_name;
     DBMS_OUTPUT.put_line (l_err_msg);
     RAISE l_program_error;
  END;

  --//==========================================================================
  --// To set environment context
  --//==========================================================================
  fnd_global.apps_initialize (l_user_id,l_responsibility_id,l_resp_application_id);

  --//==========================================================================
  --// Submitting Concurrent Request - Thumbnails
  --//==========================================================================
  l_request_id := apps.fnd_request.submit_request (application   => 'XXWC'
                                                  ,program       => 'XXWC_PDH_IMAGE_FILENAMES_TBL'
                                                  ,description   => 'XXWC Load PDH Image Filenames - SQLLDR'
                                                  ,start_time    => SYSDATE
                                                  ,sub_request   => FALSE
                                                  ,argument1     => l_dir_exists||'wc_pdhimages_thumbnail_list.txt');
  COMMIT;

  DBMS_OUTPUT.put_line('Thumbnail - Concurrent Program Request Submitted. Request ID: '|| l_request_id);

   IF (l_request_id != 0) THEN
            IF apps.fnd_concurrent.wait_for_request
                                       (l_request_id,
                                        6,
                                        9999,
                                        v_phase,
                                        v_status,
                                        v_dev_phase,
                                        v_dev_status,
                                        v_message
                                       )
            THEN
               v_error_message :=
                     'ReqID:'
                  || l_request_id
                  || '-DPhase:'
                  || v_dev_phase
                  || '-DStatus:'
                  || v_dev_status
                  || CHR (10)
                  || 'MSG:'
                  || v_message;

         -- Error Returned
               IF v_dev_phase != 'COMPLETE' OR v_dev_status != 'NORMAL'
               THEN
                  l_err_msg :=
                        'EBS Conc Prog Completed with problems - Thumbnails '
                     || v_error_message
                     || '.';
                  DBMS_OUTPUT.put_line (l_err_msg);
                  apps.fnd_file.put_line (apps.fnd_file.LOG, l_err_msg);
                  RAISE l_program_error;
               ELSE
                  p_retcode := 0;
               END IF;
            ELSE
               l_err_msg := 'EBS Conc Program Wait timed out - Thumbnails';
               DBMS_OUTPUT.put_line (l_err_msg);
               apps.fnd_file.put_line (apps.fnd_file.LOG, l_err_msg);
               RAISE l_program_error;
            END IF;
          ELSE
            l_err_msg := 'EBS Conc Program not initated - Thumbnails';
            DBMS_OUTPUT.put_line (l_err_msg);
            apps.fnd_file.put_line (apps.fnd_file.LOG, l_err_msg);
            RAISE l_program_error;
    END IF;

  --//==========================================================================
  --// Update TEMP table with 'T'
  --//==========================================================================
  UPDATE xxwc.xxwc_pdh_image_filenames_tmp
  SET    xxwc_image_filetype_attr  = 'T'
  ,      xxwc_image_filename_attr  = REPLACE (REPLACE (xxwc_image_filename_attr, CHR (10), ''), CHR (13),'')
  WHERE  xxwc_image_filetype_attr IS NULL;

  COMMIT;

  --//==========================================================================
  --// Submitting Concurrent Request - Fullsize
  --//==========================================================================
  l_request_id := apps.fnd_request.submit_request (application   => 'XXWC'
                                                  ,program       => 'XXWC_PDH_IMAGE_FILENAMES_TBL'
                                                  ,description   => 'XXWC Load PDH Image Filenames - SQLLDR'
                                                  ,start_time    => SYSDATE
                                                  ,sub_request   => FALSE
                                                  ,argument1     => l_dir_exists||'wc_pdhimages_fullsize_list.txt');

  COMMIT;

  DBMS_OUTPUT.put_line('Fullsize - Concurrent Program Request Submitted. Request ID: '|| l_request_id);

   IF (l_request_id != 0) THEN
            IF apps.fnd_concurrent.wait_for_request
                                       (l_request_id,
                                        6,
                                        9999,
                                        v_phase,
                                        v_status,
                                        v_dev_phase,
                                        v_dev_status,
                                        v_message
                                       )
            THEN
               v_error_message :=
                     'ReqID:'
                  || l_request_id
                  || '-DPhase:'
                  || v_dev_phase
                  || '-DStatus:'
                  || v_dev_status
                  || CHR (10)
                  || 'MSG:'
                  || v_message;

         -- Error Returned
               IF v_dev_phase != 'COMPLETE' OR v_dev_status != 'NORMAL'
               THEN
                  l_err_msg :=
                        'EBS Conc Prog Completed with problems - Fullsize '
                     || v_error_message
                     || '.';
                  DBMS_OUTPUT.put_line (l_err_msg);
                  apps.fnd_file.put_line (apps.fnd_file.LOG, l_err_msg);
                  RAISE l_program_error;
               ELSE
                  p_retcode := 0;
               END IF;
            ELSE
               l_err_msg := 'EBS Conc Program Wait timed out - Fullsize';
               DBMS_OUTPUT.put_line (l_err_msg);
               apps.fnd_file.put_line (apps.fnd_file.LOG, l_err_msg);
               RAISE l_program_error;
            END IF;
          ELSE
            l_err_msg := 'EBS Conc Program not initated - Fullsize';
            DBMS_OUTPUT.put_line (l_err_msg);
            apps.fnd_file.put_line (apps.fnd_file.LOG, l_err_msg);
            RAISE l_program_error;
    END IF;

  --//==========================================================================
  --// Update TEMP table with 'F'
  --//==========================================================================
  UPDATE xxwc.xxwc_pdh_image_filenames_tmp
  SET    xxwc_image_filetype_attr  = 'F'
  ,      xxwc_image_filename_attr  = REPLACE (REPLACE (xxwc_image_filename_attr, CHR (10), ''), CHR (13),'')
  WHERE  xxwc_image_filetype_attr IS NULL;

  COMMIT;

    --//========================================================================
    --// Populate Final table XXWC_EGO_IMAGE_FILENAMES
    --//========================================================================
    EXECUTE IMMEDIATE 'TRUNCATE TABLE xxwc.xxwc_ego_image_filenames';

    lv_rec_cnt := 0;

    FOR rec IN cur_tmp
        LOOP
            lv_rec_cnt := lv_rec_cnt + 1;

            INSERT INTO xxwc.xxwc_ego_image_filenames
                        (xxwc_image_filename_attr
                        ,xxwc_image_filetype_attr
                        ,last_update_date)
            VALUES      (rec.xxwc_image_filename_attr
                        ,rec.xxwc_image_filetype_attr
                        ,rec.last_update_date);
        END LOOP;

    COMMIT;

    DBMS_OUTPUT.put_line ('Total Number of records Inserted =' ||to_char(lv_rec_cnt));

    --//========================================================================
    --// Truncate TEMP table
    --//========================================================================
    EXECUTE IMMEDIATE 'TRUNCATE TABLE xxwc.xxwc_pdh_image_filenames_tmp';

--//============================================================================
--// EXCEPTION
--//============================================================================
EXCEPTION
      WHEN l_program_error
      THEN
         p_errbuf := 'Error';
         p_retcode := '2';
      WHEN OTHERS
      THEN
         p_errbuf := 'Error';
         p_retcode := '2';
         DBMS_OUTPUT.put_line (SQLERRM);

END xxwc_pdh_image_load_prc;