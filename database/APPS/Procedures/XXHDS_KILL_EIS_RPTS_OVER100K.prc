
  CREATE OR REPLACE PROCEDURE "APPS"."XXHDS_KILL_EIS_RPTS_OVER100K" 
              (
                 retcode out varchar2
                ,errbuf  out varchar2
                ,p_threshold in number
                ,p_email_id in varchar2
                ,p_dir in varchar2
              ) AS
              
    o_status boolean;
    o_message varchar2(2000);
    n_user_id number :=0;
    n_resp_appl_id number :=0;
    n_resp_id number :=0;
    n_request_id number :=0;
    my_process_id number :=0;
    v_conc_status varchar2(1) :='X';
    fileHandler UTL_FILE.FILE_TYPE;
    file_name varchar2(40) :='EIS_REPORTS_TERMINATED';
    g_request_id NUMBER :=0;
    g_dir varchar2(80) :=nvl(p_dir, '/usr/tmp');
    g_space varchar2(40) :=lpad(' ',10,' ');
    ln_request_id number :=0;
    killed_something boolean :=FALSE;
    
    cursor del_rpts is
    select c.user_name  user_name
             ,a.responsibility_id resp_id
             ,a.responsibility_application_id resp_appl_id
             ,a.request_id request_id
             ,a.process_id process_id
             ,a.report_name report_name
             ,a.employee_full_name employee_name
             ,a.report_id report_id
             ,a.email_address email_id
             ,a.actual_start_date request_start_time
             ,b.rows_retrieved rows_retrieved             
FROM xxeis.eis_rs_processes b, xxeis.eis_adm_reports_processes_v a, fnd_user c
where 1 =1
  and c.user_id =a.user_id
  and b.process_id =a.process_id
  and b.status ='P'
  and nvl(a.concurrent_status, 'Z') <> v_conc_status
  and nvl(b.rows_retrieved, 0) >p_threshold;
  --for example if rows retrieved are over 100k system will auto terminate it;
  
  type t_tbl is table of del_rpts%rowtype index by binary_integer;
  t_row t_tbl;  
  
BEGIN

 g_request_id :=fnd_global.conc_request_id;
  
 open del_rpts;
 fetch del_rpts bulk collect into t_row;
 close del_rpts;  

 if t_row.count >0 then 
  
    fileHandler := UTL_FILE.FOPEN (g_dir, file_name||'_'||g_request_id||'.txt', 'W');
    
  utl_file.putf(filehandler, 'List of EIS Reports terminated\n');    
  utl_file.putf(filehandler, '==============================\n');
  utl_file.putf(filehandler, 'Max Threshold ='||p_threshold||'\n'); 
  utl_file.putf(filehandler,'\n'); 
 
  for rec in 1 .. t_row.count loop
      
    my_process_id :=t_row(rec).process_id;
    
    fnd_file.put_line(fnd_file.log, 'Process id ='||my_process_id);
           
    xxeis.eis_rs_utility.kill_process(t_row(rec).process_id, o_message);
    
    killed_something :=TRUE;
    
    utl_file.putf(filehandler, '#'||rec||' \n');        
    utl_file.putf(filehandler, g_space||'EIS Process Id  ='||t_row(rec).process_id||'\n');
    utl_file.putf(filehandler, g_space||'EBS Request Id  ='||t_row(rec).request_id||'\n');    
    utl_file.putf(filehandler, g_space||'Rows Retrieved ='||t_row(rec).rows_retrieved||'\n');        
    utl_file.putf(filehandler, g_space||'Report Name ='||t_row(rec).report_name||'\n');
    utl_file.putf(filehandler, g_space||'Request Start Time ='||t_row(rec).request_start_time||'\n');    
    utl_file.putf(filehandler, g_space||'EBS Login Id  ='||t_row(rec).user_name||'\n');    
    utl_file.putf(filehandler, g_space||'Email Id ='||t_row(rec).email_id||'\n');     
    utl_file.putf(filehandler, g_space||'EIS Return Message ='||o_message||'\n');       
    utl_file.putf(filehandler,'\n');    
        
  end loop;  

  utl_file.putf(filehandler,'***************  End of report ************** \n');
  utl_file.putf(filehandler,'\n');
  utl_file.putf(filehandler,'This is an automatic reply. Please do not reply to this email. Thanks\n');
  
  utl_file.fclose(filehandler); --file creation is over. lets close the file handle 
  
 else
   
  Null;
  
  fnd_file.put_line(fnd_file.log, 'No EIS reports to terminate over '||p_threshold||' lines');
  
 end if;
 
 
 if (killed_something) then 
     ln_request_id :=fnd_request.submit_request
                          (
                           application      =>'XXCUS',
                           program          =>'XXHDS_EIS_KILL_EMAIL',
                           description      =>'',
                           start_time       =>'',
                           sub_request      =>FALSE,
                           argument1        =>p_email_id,
                           argument2        =>g_dir||'/'||file_name||'_'||g_request_id||'.txt',
                           argument3        =>file_name||'_'||g_request_id||'.txt'                                                                           
                          );
                          
     If ln_request_id >0 Then
      fnd_file.put_line(fnd_file.log,'XXHDS Kill EIS Report -Send Email job was submitted successfully (Request Id ='||ln_request_id||')');
      Commit;       
     Else
      fnd_file.put_line(fnd_file.log,'Failed to submit XXHDS Kill EIS Report -Send Email job (Request Id ='||ln_request_id||')');
     End If; 
                             
 end if;  

EXCEPTION
     WHEN OTHERS THEN
     fnd_file.put_line(fnd_file.log, 'process_id ='||my_process_id||', failed bcoz of '||sqlerrm);
END XXHDS_KILL_EIS_RPTS_OVER100K;;
