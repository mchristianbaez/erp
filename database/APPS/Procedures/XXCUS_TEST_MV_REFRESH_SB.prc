
  CREATE OR REPLACE PROCEDURE "APPS"."XXCUS_TEST_MV_REFRESH_SB" (retcode out varchar2, errbuf out varchar2) as
 --
 CURSOR TEST(p_cust_indx in number) IS
 SELECT     hca.cust_account_id
       , hca.account_number customer_account_number
       , hca.attribute6 prism_customer_number
       , hp.party_name customer_name
       , pb_hcsua.site_use_id bill_to_site_use_id
       , pb_hps.party_site_number bill_to_party_site_number
       , pb_hcasa.attribute17 bill_to_prism_site_number
       , pb_hps.party_site_name bill_to_party_site_name
       , pb_hcsua.location bill_to_site_name
       , trx_hcsua.location trx_bill_to_site_name
       , trx_hps.party_site_number trx_party_site_number
       , trx_hps.party_site_name trx_party_site_name
       , pb_hl.address1 bill_to_address1
       , pb_hl.address2 bill_to_address2
       , pb_hl.address3 bill_to_address3
       , pb_hl.address4 bill_to_address4
       , pb_hl.city bill_to_city
       , NVL (pb_hl.state, pb_hl.province) bill_to_city_province
       , pb_hl.postal_code bill_to_zip_code
       , pb_hl.country bill_to_country
       , apsa.payment_schedule_id
       , rcta.customer_trx_id
       , rctlgda.code_combination_id rcta_ccid
       , acra.cash_receipt_id
       ,              -- 06/14/2012 CG Corrected from using ADA to using ACRHA
        acrha.account_code_combination_id acra_ccid
       , rcta.trx_number invoice_number
       , acra.receipt_number
       , apps.xxwc_mv_routines_pkg.get_gl_code_segment (
            NVL (rctlgda.code_combination_id
               , acrha.account_code_combination_id)
          , 2
         )
            branch_location
       , apsa.trx_number
       , apsa.trx_date
       , apsa.due_date
       , (CASE
             WHEN apsa.class = 'INV' THEN 'Invoice'
             WHEN apsa.class = 'CM' THEN 'Credit Memo'
             WHEN apsa.class = 'DM' THEN 'Debit Memo'
             WHEN apsa.class = 'PMT' THEN 'Payment'
             ELSE apsa.class
          END)
            trx_type
       , apsa.amount_due_original transation_balance
       , apsa.amount_due_remaining transaction_remaining_balance
       , (TRUNC (SYSDATE) - TRUNC (apsa.due_date)) age
       , (CASE
             WHEN (TRUNC (SYSDATE) - TRUNC (apsa.due_date)) <= 0
             THEN
                apsa.amount_due_remaining
             ELSE
                0
          END)
            current_balance
       , (CASE
             WHEN (TRUNC (SYSDATE) - TRUNC (apsa.due_date)) BETWEEN 1 AND 30
             THEN
                apsa.amount_due_remaining
             ELSE
                0
          END)
            thirty_days_bal
       , (CASE
             WHEN (TRUNC (SYSDATE) - TRUNC (apsa.due_date)) BETWEEN 31 AND 60
             THEN
                apsa.amount_due_remaining
             ELSE
                0
          END)
            sixty_days_bal
       , (CASE
             WHEN (TRUNC (SYSDATE) - TRUNC (apsa.due_date)) BETWEEN 61 AND 90
             THEN
                apsa.amount_due_remaining
             ELSE
                0
          END)
            ninety_days_bal
       , (CASE
             WHEN (TRUNC (SYSDATE) - TRUNC (apsa.due_date)) BETWEEN 91
                                                                AND  180
             THEN
                apsa.amount_due_remaining
             ELSE
                0
          END)
            one_eighty_days_bal
       , (CASE
             WHEN (TRUNC (SYSDATE) - TRUNC (apsa.due_date)) BETWEEN 181
                                                                AND  360
             THEN
                apsa.amount_due_remaining
             ELSE
                0
          END)
            three_sixty_days_bal
       , (CASE
             WHEN (TRUNC (SYSDATE) - TRUNC (apsa.due_date)) >= 361
             THEN
                apsa.amount_due_remaining
             ELSE
                0
          END)
            over_three_sixty_days_bal
       , apps.xxwc_mv_routines_pkg.get_customer_last_payment_date (
            apsa.customer_id
         )
            last_payment_date
       ,                                                 --hcp.account_status,
        flv.meaning customer_account_status
       , hcp_trx.credit_hold site_credit_hold
       ,                                          -- hcp_trx.profile_class_id,
        hcpc.name customer_profile_class
       ,                                              -- hcp_trx.collector_id,
        ac.name collector_name
       ,                                          --hcp_trx.credit_analyst_id,
        jrret_cr.resource_name credit_analyst
       , NVL (NVL (jrs.name, papf.full_name), jrret.resource_name)
            account_manager
       , apps.xxwc_mv_routines_pkg.get_customer_open_balance (
            apsa.customer_id
         )
            account_balance
       , rtt.description cust_payment_term
       , NVL (hcp.attribute2, '2') remit_to_address_code
       , NVL (hcp.attribute3, 'N') stmt_by_job
       , NVL (hcp.send_statements, 'N') send_statement_flag
       , NVL (hcp.credit_balance_statements, 'N') send_credit_bal_flag
       , -- 06/19/2012 CG: added account level credit hold flag per EiS Request
        hcp.credit_hold account_credit_hold
       ,                                                      -- 06/19/2012 CG
        apsa.customer_id trx_customer_id
       , apsa.customer_site_use_id trx_bill_site_use_id
       , NVL (rcta.purchase_order, acra.customer_receipt_reference)
            customer_po_number
       , acra.status pmt_status
       , -- 07/20/2012 CG: Changes to add additional fields for Reserve Report
        jrs.salesrep_number
       -- 09/13/2012 CG: Added 3 new columns per Pedro Pagan and Stephen Gribbin (email 09/11/2012)
       , SUBSTR (
            (apps.xxwc_mv_routines_pkg.get_lookup_value ('ACCOUNT_STATUS'
                                                       , hcp.account_status
                                                       , NULL))
          , 1
          , 80
         )
            account_status
       , (apps.xxwc_mv_routines_pkg.get_prev_periods_sales (apsa.customer_id
                                                          , -12
                                                          , apsa.org_id))
            twelve_months_sales
       , (apps.xxwc_mv_routines_pkg.get_site_type_credit_limit (
             apsa.customer_id
           , NULL
           , 'YARD'
          ))
            yard_credit_limit
       -- 10/08/12 CG: Added
       , (apps.xxwc_mv_routines_pkg.format_phone_number (
             (apps.xxwc_mv_routines_pkg.get_phone_fax_number ('PHONE'
                                                            , hp.party_id
                                                            , NULL
                                                            , 'GEN'))
          ))
            acct_phone_number
       , trx_hl.address1 trx_bill_to_address1
       , trx_hl.address2 trx_bill_to_address2
       , trx_hl.address3 trx_bill_to_address3
       , trx_hl.address4 trx_bill_to_address4
       , trx_hl.city trx_bill_to_city
       , NVL (trx_hl.state, trx_hl.province) trx_bill_to_city_prov
       , trx_hl.postal_code trx_bill_to_zip_code
       , trx_hl.country trx_bill_to_country
       , 0 in_process_total -- (apps.xxwc_mv_routines_pkg.get_cust_in_proc_bal (apsa.customer_id)) in_process_total
       , 0 largest_balance -- (apps.xxwc_mv_routines_pkg.get_cust_high_balance (apsa.customer_id)) largest_balance
       , 0 high_credit -- (apps.xxwc_mv_routines_pkg.get_high_credit_ytd (apsa.customer_id)) high_credit
  FROM     ar.hz_cust_accounts hca
       , ar.hz_parties hp
       , ar.hz_cust_acct_sites_all pb_hcasa
       , ar.hz_party_sites pb_hps
       , ar.hz_cust_site_uses_all pb_hcsua
       , ar.hz_locations pb_hl
       , jtf.jtf_rs_salesreps jrs
       , jtf.jtf_rs_resource_extns jrre
       , jtf.jtf_rs_resource_extns_tl jrret
       , hr.per_all_people_f papf
       , ar.hz_customer_profiles hcp
       , ar.ar_payment_schedules_all apsa
       , ar.ra_customer_trx_all rcta
       , ar.ra_cust_trx_line_gl_dist_all rctlgda
       , ar.ar_cash_receipts_all acra
       ,       -- 06/14/2012 CG: Changed to point to the cash receipts history
         -- since it contains the latest posting
         -- ar.ar_distributions_all ada,
         ar.ar_cash_receipt_history_all acrha
       , ar.hz_cust_site_uses_all trx_hcsua
       , ar.hz_cust_acct_sites_all trx_hcasa
       , ar.hz_party_sites trx_hps
       , ar.hz_locations trx_hl
       , ar.hz_customer_profiles hcp_trx
       , ar.ra_terms_tl rtt
       , applsys.fnd_lookup_values flv
       , ar.hz_cust_profile_classes hcpc
       , ar.ar_collectors ac
       , jtf.jtf_rs_resource_extns_tl jrret_cr
       , apps.hr_operating_units hou
 WHERE   1 =1
  AND    MOD(hca.cust_account_id ,10) =p_cust_indx
        and      hca.cust_account_id = apsa.customer_id
         AND ABS (apsa.amount_due_remaining) > 0
         AND hca.party_id = hp.party_id
         -- Changed to accomodate accounts where the is no primary bill to
         AND (-- HAS PRIMARY BILL TO
              (EXISTS
                  (SELECT    'HAS PB'
                     FROM    hz_cust_acct_sites_all hcasa
                    WHERE        hcasa.cust_account_id = hca.cust_account_id
                            AND hcasa.status = 'A'
                            AND hcasa.bill_to_flag IN ('P', 'Y'))
               AND hca.cust_account_id = pb_hcasa.cust_account_id
               AND pb_hcasa.status = 'A'
               AND pb_hcasa.org_id = apsa.org_id
               AND pb_hcasa.bill_to_flag IN ('P', 'Y')
               AND pb_hcasa.party_site_id = pb_hps.party_site_id
               AND pb_hcasa.cust_acct_site_id = pb_hcsua.cust_acct_site_id
               AND pb_hcsua.site_use_code = 'BILL_TO'
               AND NVL (pb_hcsua.primary_flag, 'N') = 'Y'
               AND pb_hcsua.org_id = apsa.org_id
               AND pb_hps.location_id = pb_hl.location_id)
              OR-- DOESNT HAVE PRIMARY BILL TO
                (NOT EXISTS
                    (SELECT   'HAS PB'
                       FROM   hz_cust_acct_sites_all hcasa
                      WHERE       hcasa.cust_account_id = hca.cust_account_id
                              AND hcasa.status = 'A'
                              AND hcasa.bill_to_flag = 'P')
                 AND pb_hcsua.site_use_id = apsa.customer_site_use_id
                 AND pb_hcsua.org_id = apsa.org_id
                 AND pb_hcasa.cust_acct_site_id = pb_hcsua.cust_acct_site_id
                 AND pb_hcasa.party_site_id = pb_hps.party_site_id
                 AND pb_hcasa.org_id = apsa.org_id))
         -- Changed to accomodate accounts where the is no primary bill to
         AND pb_hps.location_id = pb_hl.location_id
         AND pb_hcsua.primary_salesrep_id = jrs.salesrep_id(+)
         AND pb_hcsua.org_id = jrs.org_id(+)
         AND jrs.person_id = papf.person_id(+)
         AND TRUNC (papf.effective_start_date(+)) <= TRUNC (SYSDATE)
         AND NVL (TRUNC (papf.effective_end_date(+)), TRUNC (SYSDATE)) >=
               TRUNC (SYSDATE)
         AND jrs.resource_id = jrre.resource_id(+)
         AND jrre.resource_id = jrret.resource_id(+)
         AND jrret.language(+) = 'US'
         AND jrre.category = jrret.category(+)
         AND hca.party_id = hcp.party_id
         AND hca.cust_account_id = hcp.cust_account_id
         AND hcp.site_use_id IS NULL
         AND apsa.customer_trx_id = rcta.customer_trx_id(+)
         AND rcta.customer_trx_id = rctlgda.customer_trx_id(+)
         AND rctlgda.customer_trx_line_id(+) IS NULL
         AND rctlgda.account_class(+) = 'REC'
         AND apsa.cash_receipt_id = acra.cash_receipt_id(+)
         -- 06/14/2012 CG: Changed to point to acrha
         -- AND acra.cash_receipt_id = ada.source_id(+)
         -- AND ada.source_table(+) = 'CRH'
         AND acra.cash_receipt_id = acrha.cash_receipt_id(+)
         AND acrha.first_posted_record_flag(+) = 'Y'
         AND apsa.customer_site_use_id = trx_hcsua.site_use_id(+)
         AND trx_hcsua.cust_acct_site_id = trx_hcasa.cust_acct_site_id(+)
         AND trx_hcasa.party_site_id = trx_hps.party_site_id(+)
         -- 10/08/2012 CG: Added Trx Address
         AND trx_hps.location_id = trx_hl.location_id(+)
         -- 10/08/2012 CG
         AND trx_hcsua.site_use_id = hcp_trx.site_use_id(+)
         AND hcp.standard_terms = rtt.term_id(+)
         AND hcp.account_status = flv.lookup_code(+)
         AND flv.lookup_type(+) = 'ACCOUNT_STATUS'
         AND flv.language(+) = 'US'
         AND hcp.profile_class_id = hcpc.profile_class_id(+)
         AND hcp.collector_id = ac.collector_id(+)
         AND hcp.credit_analyst_id = jrret_cr.resource_id(+)
         AND jrret_cr.language(+) = 'US'
         AND jrret_cr.category(+) = 'EMPLOYEE'
         AND apsa.org_id = hou.organization_id
         AND hou.name = 'HDS White Cap - Org';
 --         
  type test_tbl is table of test%rowtype index by binary_integer;
  test_rec test_tbl; 
 --
 cursor unknown_cust is 
SELECT     -1 cust_account_id
       , '-1' customer_account_number
       , '-1' prism_customer_number
       , 'UNKNOWN CUSTOMER' customer_name
       , -1 bill_to_site_use_id
       , '-1' bill_to_party_site_number
       , '-1' bill_to_prism_site_number
       , 'UNKNOWN SITE' bill_to_party_site_name
       , 'UNKNOWN SITE' bill_to_site_name
       , 'UNKNOWN SITE' trx_bill_to_site_name
       , '-1' trx_party_site_number
       , 'UNKNOWN SITE' trx_party_site_name
       , ' ' bill_to_address1
       , ' ' bill_to_address2
       , ' ' bill_to_address3
       , ' ' bill_to_address4
       , ' ' bill_to_city
       , ' ' bill_to_city_province
       , ' ' bill_to_zip_code
       , ' ' bill_to_country
       , apsa.payment_schedule_id
       , rcta.customer_trx_id
       , rctlgda.code_combination_id rcta_ccid
       , acra.cash_receipt_id
       ,              -- 06/14/2012 CG Corrected from using ADA to using ACRHA
        acrha.account_code_combination_id acra_ccid
       , rcta.trx_number invoice_number
       , acra.receipt_number
       , apps.xxwc_mv_routines_pkg.get_gl_code_segment (
            NVL (rctlgda.code_combination_id
               , acrha.account_code_combination_id)
          , 2
         )
            branch_location
       , apsa.trx_number
       , apsa.trx_date
       , apsa.due_date
       , (CASE
             WHEN apsa.class = 'INV' THEN 'Invoice'
             WHEN apsa.class = 'CM' THEN 'Credit Memo'
             WHEN apsa.class = 'DM' THEN 'Debit Memo'
             WHEN apsa.class = 'PMT' THEN 'Payment'
             ELSE apsa.class
          END)
            trx_type
       , apsa.amount_due_original transation_balance
       , apsa.amount_due_remaining transaction_remaining_balance
       , (TRUNC (SYSDATE) - TRUNC (apsa.due_date)) age
       , (CASE
             WHEN (TRUNC (SYSDATE) - TRUNC (apsa.due_date)) <= 0
             THEN
                apsa.amount_due_remaining
             ELSE
                0
          END)
            current_balance
       , (CASE
             WHEN (TRUNC (SYSDATE) - TRUNC (apsa.due_date)) BETWEEN 1 AND 30
             THEN
                apsa.amount_due_remaining
             ELSE
                0
          END)
            thirty_days_bal
       , (CASE
             WHEN (TRUNC (SYSDATE) - TRUNC (apsa.due_date)) BETWEEN 31 AND 60
             THEN
                apsa.amount_due_remaining
             ELSE
                0
          END)
            sixty_days_bal
       , (CASE
             WHEN (TRUNC (SYSDATE) - TRUNC (apsa.due_date)) BETWEEN 61 AND 90
             THEN
                apsa.amount_due_remaining
             ELSE
                0
          END)
            ninety_days_bal
       , (CASE
             WHEN (TRUNC (SYSDATE) - TRUNC (apsa.due_date)) BETWEEN 91
                                                                AND  180
             THEN
                apsa.amount_due_remaining
             ELSE
                0
          END)
            one_eighty_days_bal
       , (CASE
             WHEN (TRUNC (SYSDATE) - TRUNC (apsa.due_date)) BETWEEN 181
                                                                AND  360
             THEN
                apsa.amount_due_remaining
             ELSE
                0
          END)
            three_sixty_days_bal
       , (CASE
             WHEN (TRUNC (SYSDATE) - TRUNC (apsa.due_date)) >= 361
             THEN
                apsa.amount_due_remaining
             ELSE
                0
          END)
            over_three_sixty_days_bal
       , TO_DATE ('01/01/1952', 'MM/DD/YYYY') last_payment_date
       , ' ' customer_account_status
       , 'Y' site_credit_hold
       , ' ' customer_profile_class
       , ' ' collector_name
       , ' ' credit_analyst
       , ' ' account_manager
       , 0 account_balance
       , ' ' cust_payment_term
       , '2' remit_to_address_code
       , 'N' stmt_by_job
       , 'N' send_statement_flag
       , 'N' send_credit_bal_flag
       , -- 06/19/2012 CG: added account level credit hold flag per EiS Request
        'N' account_credit_hold
       ,                                                      -- 06/19/2012 CG
        apsa.customer_id trx_customer_id
       , apsa.customer_site_use_id trx_bill_site_use_id
       , NVL (rcta.purchase_order, acra.customer_receipt_reference)
            customer_po_number
       , acra.status pmt_status
       , -- 07/20/2012 CG: Changes to add additional fields for Reserve Report
        ' ' salesrep_number
       -- 90/13/2012 CG: Added 3 new columns per Pedro Pagan and Stephen Gribbin (email 09/11/2012)
       , ' ' account_status
       , 0 twelve_months_sales
       , 0 yard_credit_limit
       , ' ' acct_phone_number
       , ' ' trx_bill_to_address1
       , ' ' trx_bill_to_address2
       , ' ' trx_bill_to_address3
       , ' ' trx_bill_to_address4
       , ' ' trx_bill_to_city
       , ' ' trx_bill_to_city_prov
       , ' ' trx_bill_to_zip_code
       , ' ' trx_bill_to_country
       , 0 in_process_total
       , 0 largest_balance
       , 0 high_credit
  FROM     ar.ar_payment_schedules_all apsa
       , ar.ra_customer_trx_all rcta
       , ar.ra_cust_trx_line_gl_dist_all rctlgda
       , ar.ar_cash_receipts_all acra
       ,       -- 06/14/2012 CG: Changed to point to the cash receipts history
         -- since it contains the latest posting
         -- ar.ar_distributions_all ada,
         ar.ar_cash_receipt_history_all acrha
       , apps.hr_operating_units hou
 WHERE         apsa.customer_id IS NULL
         AND ABS (apsa.amount_due_remaining) > 0
         AND apsa.customer_trx_id = rcta.customer_trx_id(+)
         AND rcta.customer_trx_id = rctlgda.customer_trx_id(+)
         AND rctlgda.customer_trx_line_id(+) IS NULL
         AND rctlgda.account_class(+) = 'REC'
         AND apsa.cash_receipt_id = acra.cash_receipt_id(+)
         -- 06/14/2012 CG: Changed to point to acrha
         -- AND acra.cash_receipt_id = ada.source_id(+)
         -- AND ada.source_table(+) = 'CRH'
         AND acra.cash_receipt_id = acrha.cash_receipt_id(+)
         AND acrha.first_posted_record_flag(+) = 'Y'
         AND apsa.org_id = hou.organization_id
         AND hou.name = 'HDS White Cap - Org';
 --
  type t_unknown_tbl is table of unknown_cust%rowtype index by binary_integer;
  t_unknown_rec t_unknown_tbl;           
 --
  p_limit number :=10000; 
 --         
  procedure print_log(p_message in varchar2) is
  begin
   if fnd_global.conc_request_id >0 then
    fnd_file.put_line(fnd_file.log, p_message);
   else
    dbms_output.put_line(p_message);
   end if;
  exception
   when others then
    fnd_file.put_line(fnd_file.log, 'Issue in print_log routine ='||sqlerrm);
  end print_log;
 -- 
begin
 --
 execute immediate 'truncate table xxcus.test_ar_cust_bal_mv';
 --
 for n_mod in 0 ..9 loop
     --
     open test(n_mod);
     loop 
      fetch test bulk collect into test_rec limit p_limit;
      exit when test_rec.count =0;
      forall idx in test_rec.first .. test_rec.last 
       insert /*+ APPEND */ into XXCUS.TEST_AR_CUST_BAL_MV values test_rec(idx);
       commit;
     end loop;
     close test;
     --
 end loop;
 --
 --
     open unknown_cust;
     loop 
      fetch unknown_cust bulk collect into t_unknown_rec limit p_limit;
      exit when t_unknown_rec.count =0;
      forall idx in t_unknown_rec.first .. t_unknown_rec.last 
       insert /*+ APPEND */ into XXCUS.TEST_AR_CUST_BAL_MV values t_unknown_rec(idx);
       commit;
     end loop;
     close unknown_cust;
 -- 
exception
  when others then
   print_log('outer block ='||sqlerrm);
   print_log(sqlerrm);
   rollback;   
end xxcus_test_mv_refresh_sb;;
