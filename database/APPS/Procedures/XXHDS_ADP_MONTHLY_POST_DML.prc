
  CREATE OR REPLACE PROCEDURE "APPS"."XXHDS_ADP_MONTHLY_POST_DML" (retcode OUT VARCHAR2
                                                      ,errbuf  OUT VARCHAR2) IS
  /*******************************************************************************
  -- Module:  Oracle EB Tax and Taxware
  -- Author: Balaguru Seshadri
  -- Date: 07-17-2012
  -- Scope: This will be the last program to kick off completing the ADP Taxware monthly update process.
  --Rerun: If this program errors out, fix the error and rerun again bcoz everything is rolledback.
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)         DESCRIPTION
  ------- -----------   ---------------   -----------------------------------------
  1.0     07-17-2012    Balaguru Seshadri Initial creation of the procedure
  1.1     03/13/2013    Kathy Poling      SR 192669 CO Sales Tax fix added.  Taxware
                                          was calculating CO with used tax
  ********************************************************************************/

  n_count     NUMBER := 0;
  sql_text    VARCHAR2(4000);
  v_prod_code xxcus_adp_taxware_dml.prod_code%TYPE;

  CURSOR my_dml IS
    SELECT prod_code
          ,dbms_lob.substr(post_process_sql_text, 4000, 1) sql_text
      FROM xxcus_adp_taxware_dml
     WHERE 1 = 1;

  PROCEDURE print_log(p_message IN VARCHAR2) IS
  BEGIN
    IF fnd_global.conc_request_id > 0
    THEN
      fnd_file.put_line(fnd_file.log, p_message);
    ELSE
      dbms_output.put_line(p_message);
    END IF;
  END;

BEGIN
  print_log(p_message => '');
  print_log(p_message => 'Before calling ADP Taxware Monthly Updates -Post Processor');
  print_log(p_message => '==========================================');
  print_log(p_message => '');

  FOR rec IN my_dml
  LOOP
  
    v_prod_code := rec.prod_code;
  
    print_log(p_message => 'Current SQL statement =' || rec.sql_text);
    print_log(p_message => '');
  
    EXECUTE IMMEDIATE rec.sql_text;
  
    print_log(p_message => 'Record is saved for prod code ' || v_prod_code);
    print_log(p_message => '');
  
  END LOOP;

  print_log(p_message => '');
  print_log(p_message => 'After calling of ADP Taxware Monthly Update -Post Processor');
  print_log(p_message => '===========================================');

  --Version 1.1
  print_log(p_message => '');
  print_log(p_message => 'Before calling ADP Taxware Update for CO Taxing');
  print_log(p_message => '==========================================');
  print_log(p_message => '');

  UPDATE taxware.taxjurpd
     SET jurintrcode = '12', jurcntycode = '00', jurcitycode = '00'
   WHERE stcode = '06';

  BEGIN
    taxware.update_taxstinfo;
  END;

  print_log(p_message => '');
  print_log(p_message => 'After calling ADP Taxware Update for CO Taxing');
  print_log(p_message => '==========================================');
  print_log(p_message => '');
  --End Version 1.1   

EXCEPTION
  WHEN OTHERS THEN
    print_log(p_message => 'Error in xxhds_adp_monthly_postprocessor for prod code =' ||
                           v_prod_code);
    print_log(p_message => 'Error in xxhds_adp_monthly_postprocessor, ' ||
                           SQLERRM);
    ROLLBACK;
END xxhds_adp_monthly_post_dml;
;
