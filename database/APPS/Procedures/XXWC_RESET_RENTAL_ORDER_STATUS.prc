CREATE OR REPLACE procedure APPS.XXWC_RESET_RENTAL_ORDER_STATUS (retcode out varchar2, errbuf out varchar2, p_header_id in number, p_status in varchar2) as 
 
begin
 if p_status ='OPEN' then 
 
    Update oe_order_headers_all h
    Set open_flag = 'Y' ,flow_status_code = 'BOOKED' 
    where 1 =1
      and header_id =p_header_id
      and org_id = 162    
      and flow_status_code = 'CLOSED';
      
  FND_FILE.PUT_LINE(FND_FILE.LOG, 'Order header_id ='||p_header_id||' updated successfully to BOOKED.'); 
      
 elsif p_status ='CLOSE' then
 

    Update oe_order_headers_all h
    Set open_flag = 'N' , 
       flow_status_code ='CLOSED'
    where 1 =1
    and header_id =p_header_id    
    and org_id = 162  
    and flow_status_code = 'BOOKED'; 
    
  FND_FILE.PUT_LINE(FND_FILE.LOG, 'Order header_id ='||p_header_id||' updated successfully to CLOSE.');  
    
 else
  nULL;
 end if;
 COMMIT;
EXCEPTION
 when others then
  ROLLBACK;
  FND_FILE.PUT_LINE(FND_FILE.LOG, 'Issue in updating Order header_id ='||p_header_id||', error ='||sqlerrm);  
end;
/
