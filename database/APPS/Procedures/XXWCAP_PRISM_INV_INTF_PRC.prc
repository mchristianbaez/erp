CREATE OR REPLACE PROCEDURE APPS.XXWCAP_PRISM_INV_INTF_PRC(p_conc_name IN VARCHAR2
                                                        ,p_conc_arg1 IN VARCHAR2
                                                        ,p_user      IN VARCHAR2
                                                        ,p_resp      IN VARCHAR2
                                                        ,p_org       IN VARCHAR2
                                                        ,p_threshold IN NUMBER) IS

  /*******************************************************************************
  * Procedure:   xxwcap_prism_inv_intf_prc
  * Description: This procedure is used by an APEX App (XXWC Prism-EBS AP Invoice
                 Interface Processing) to execute the interface program to commit changes.
                 Apex App cannot directly execute the interface program due to Oracle
                 limitation on executing RPC over dblink.
  HISTORY
  ===============================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     14-Aug-2012        Gopi Damuluri   Initial creation of the package
  ********************************************************************************/

  --Intialize Variables
  l_err_msg      VARCHAR2(2000);
  l_err_code     NUMBER;
  l_sec          VARCHAR2(150);
  l_req_id       NUMBER;
  l_err_callfrom VARCHAR2(75) := 'xxwcap_prism_inv_intf_prc';
  l_distro_list  VARCHAR2(75) DEFAULT 'hdsoracledevelopers@hdsupply.com';


  --Start Main Program
BEGIN
  l_sec := 'Start Main Program; ';

  -- Call the procedure

  APPS.XXWCAP_INV_INT_PKG.UC4_CALL(p_errbuf              => l_err_msg
                                 , p_retcode             => l_err_code
                                 , p_conc_prg_name       => p_conc_name
                                 , p_conc_prg_arg1       => p_conc_arg1
                                 , p_user_name           => p_user
                                 , p_responsibility_name => p_resp
                                 , p_org_name            => p_org
                                 , p_threshold_value     => p_threshold);

EXCEPTION
  WHEN program_error THEN
    ROLLBACK;
    l_err_code := 2;
    l_err_msg  := substr((l_err_msg || ' ERROR ' ||
                         substr(SQLERRM, 1, 2000)), 1, 3000);

    xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_err_callfrom,
                                         p_calling => l_sec,
                                         p_request_id => l_req_id,
                                         p_ora_error_msg => substr(SQLERRM, 1,
                                                                    2000),
                                         p_error_desc => substr(l_err_msg, 1,
                                                                 2000),
                                         p_distribution_list => l_distro_list,
                                         p_module => 'AP');

  WHEN OTHERS THEN
    l_err_code := 2;
    l_err_msg  := substr((l_err_msg || ' ERROR ' ||
                         substr(SQLERRM, 1, 2000)), 1, 3000);
    dbms_output.put_line(l_err_msg);

    xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_err_callfrom,
                                         p_calling => l_sec,
                                         p_request_id => l_req_id,
                                         p_ora_error_msg => substr(SQLERRM, 1,
                                                                    2000),
                                         p_error_desc => substr(l_err_msg, 1,
                                                                 2000),
                                         p_distribution_list => l_distro_list,
                                         p_module => 'AP');
END XXWCAP_PRISM_INV_INTF_PRC;
/
