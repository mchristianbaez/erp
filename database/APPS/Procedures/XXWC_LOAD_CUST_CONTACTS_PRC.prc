CREATE OR REPLACE PROCEDURE APPS.xxwc_load_cust_contacts_prc (RETCODE     OUT NUMBER
                                                        , ERRMSG    OUT VARCHAR2)
IS
      -- TMS 20130606-01033 

      l_error_message          clob;
      l_conc_req_id            number := fnd_global.conc_request_id;
      
      -- PARTY PERSON RECOPRD VARIABLES
      ppersonrec               apps.hz_party_v2pub.person_rec_type;
      oppartyid                ar.hz_parties.party_id%TYPE;
      oppartynumber            ar.hz_parties.party_number%TYPE;
      opprofileid              NUMBER;
      
      --PARTY SITE CONTACT
      porgcontactrec           apps.hz_party_contact_v2pub.org_contact_rec_type;
      oocpartyid               ar.hz_parties.party_id%TYPE;
      oocpartynumber           ar.hz_parties.party_number%TYPE;
      oocpartyrelnumber        ar.hz_org_contacts.contact_number%TYPE;
      oocpartyrelid            ar.hz_party_relationships.party_relationship_id%TYPE;
      oocorgcontactid          ar.hz_org_contacts.org_contact_id%TYPE;
      
      -- CONTACT ROLE
      pcustacctrolerec         apps.hz_cust_account_role_v2pub.cust_account_role_rec_type;
      ocustacctroleid          ar.hz_cust_account_roles.cust_account_role_id%TYPE;
      
      --CONTACT VARIABLES
      pcontactpointrec         apps.hz_contact_point_v2pub.contact_point_rec_type;
      pedirec                  apps.hz_contact_point_v2pub.edi_rec_type;
      pemailrec                apps.hz_contact_point_v2pub.email_rec_type;
      pphonerec                apps.hz_contact_point_v2pub.phone_rec_type;
      ptelexrec                apps.hz_contact_point_v2pub.telex_rec_type;
      pwebrec                  apps.hz_contact_point_v2pub.web_rec_type;
      ocontactpointid          NUMBER;
      
      l_proceed                VARCHAR2 (1);
      
      p_cust_account_rec apps.HZ_CUST_ACCOUNT_V2PUB.CUST_ACCOUNT_REC_TYPE;
      p_object_version_number number := NULL;
      
      --
      ostatus                  VARCHAR2 (1);
      omsgcount                NUMBER;
      omsgdata                 VARCHAR2 (2000);
      v_dup_roll               NUMBER;
      c_flag                   VARCHAR2 (1);
      
      x_return_status         varchar2(2000);
      x_msg_count             number;
      x_msg_data              varchar2(2000);
      p_error                 varchar2(4000);
      
      l_cust_account_id        number;
      l_party_id               number;
      l_user_id                number;
      l_resp_id                number;
      l_resp_appl_id           number;
      
      l_count                  number;
      l_count_err              number;
      l_max_commit             number;
      l_commit_count           number;
      l_error_code             varchar2(80);
	  l_org_id                 NUMBER;-- 14/10/2014 TMS#20141001-00168 Added by Veera

      CURSOR cur_validate
      IS
         SELECT x1.rowid rd
                , x1.*
           FROM XXWC.XXWC_CUST_CONTACT_STG x1
          WHERE 1 = 1 AND status IS NULL;
      
      CURSOR cur_load_contacts
      IS
         SELECT distinct(customer_name)
                , account_number
                , first_name
                , last_name
                , cust_account_id
                , party_id
           FROM XXWC.XXWC_CUST_CONTACT_STG x1
          WHERE 1 = 1 
            AND status = 'VALID'
            AND (first_name is not null or last_name is not null);
            --AND email_address is not null
            --AND rownum <= 5;
            
      CURSOR cur_contact_points (p_cust_account_id number, p_first_name varchar2, p_last_name varchar2)
      IS
        SELECT x1.rowid rid
                , x1.*
           FROM XXWC.XXWC_CUST_CONTACT_STG x1
          WHERE 1 = 1 
            AND status = 'VALID'
            AND cust_Account_id = p_cust_account_id
            AND nvl(first_name, 'Y') = nvl(p_first_name, 'Y')
            AND nvl(last_name, 'Z') = nvl(p_last_name, 'Z');
      
BEGIN

    fnd_file.put_line (fnd_file.output,'Initializing Environment Variables...');
            
    apps.fnd_global.apps_initialize(fnd_global.user_id, 
                                    fnd_global.resp_id, 
                                    fnd_global.resp_appl_id);
    apps.mo_global.init('AR');
	l_org_id:=mo_global.get_current_org_id;  
	apps.fnd_client_info.set_org_context(l_org_id); --14/10/2014 TMS#20141001-00168 Added by Veera
    --apps.fnd_client_info.set_org_context('162');  --14/10/2014 TMS#20141001-00168 Commented by Veera
	
   
    -- Validate Data
    fnd_file.put_line (fnd_file.output,'Starting validation process...');
    l_count := 0;
    l_count_err := 0;
    l_commit_count := 0;
    FOR c0 in cur_validate
    loop
        exit when cur_validate%notfound;
        
        l_cust_account_id   := null;
        l_party_id          := null;
        l_error_code        := null;
        begin
            select  cust_account_id
                    , party_id
            into    l_cust_account_id
                    , l_party_id
            from    hz_cust_accounts hca
            where   nvl(hca.attribute6,hca.account_number) = c0.account_number;
        exception
        when too_many_rows then
            fnd_file.put_line (fnd_file.output,'Mult records found for Account Number '||c0.account_number);
            l_cust_account_id   := null;
            l_party_id          := null;
            l_error_code        := 'MULT_ROW_ERR';
        when no_data_found then
            fnd_file.put_line (fnd_file.output,'No records found for Account Number '||c0.account_number);
            l_cust_account_id   := null;
            l_party_id          := null;
            l_error_code        := 'NO_ACCT_ERR';
        when others then
            fnd_file.put_line (fnd_file.output,'Could not find EBS account for Prism Account Number '||c0.account_number||'. Error: '||SQLERRM);   
            l_cust_account_id   := null; 
            l_party_id          := null;
            l_error_code        := 'OTHR_ROW_ERR';
        end;
    
        begin
            update  XXWC.XXWC_CUST_CONTACT_STG
            set     cust_account_id = l_cust_account_id
                    , party_id = l_party_id
                    , status = nvl(l_error_code, 'VALID')
            where   rowid = c0.rd;
        exception
        when others then
            fnd_file.put_line (fnd_file.output,'Could not update table for Prism Account Number '||c0.account_number);    
        end;
        
        if l_cust_account_id is not null then
            l_count := l_count + 1;
        else
            l_count_err := l_count_err + 1;        
        end if;
        
        l_commit_count := l_commit_count + 1;
         if l_commit_count >= 100 then
            commit;
            l_commit_count := 0;
         end if;
    
    end loop;  
    
    commit;
    
    fnd_file.put_line (fnd_file.output,'Count of valid records: '||l_count);
    fnd_file.put_line (fnd_file.output,'Count of invalid records: '||l_count_err);
    fnd_file.put_line (fnd_file.output,' ');
    
    fnd_file.put_line (fnd_file.output,'Starting contact load...');
    
    l_count := 0;
    l_count_err := 0;
    l_commit_count := 0;
    FOR c1 IN cur_load_contacts
    LOOP
       exit when cur_load_contacts%notfound;
       
       ostatus := NULL;
       omsgcount := NULL;
       omsgdata := NULL;

       ppersonrec.person_first_name := trim(c1.first_name);
       ppersonrec.person_last_name := trim(c1.last_name);

       ppersonrec.created_by_module := 'TCA_V1_API';
       apps.hz_party_v2pub.create_person (p_init_msg_list      => 'T',
                                     p_person_rec         => ppersonrec,
                                     x_return_status      => ostatus,
                                     x_msg_count          => omsgcount,
                                     x_msg_data           => omsgdata,
                                     x_party_id           => oppartyid,
                                     x_party_number       => oppartynumber,
                                     x_profile_id         => opprofileid
                                    );

       IF ostatus <> 'S'
       THEN
         
          IF omsgcount > 1
          THEN
             FOR i IN 1 .. omsgcount
             LOOP
                fnd_file.put_line (fnd_file.output,'Error for acct '||c1.account_number||' contact '||c1.first_name||' - PERSON RECORD ' || SUBSTR (apps.fnd_msg_pub.get (p_encoded => apps.fnd_api.g_false), 1, 255 ) );
             END LOOP;
          ELSE
              fnd_file.put_line (fnd_file.output,'Error for acct '||c1.account_number||' contact '||c1.first_name||' - PERSON RECORD ' || omsgdata);
          END IF;

          l_proceed := 'N';
       ELSE
          l_proceed := 'Y';
       END IF; -- PERSON CREATION

       -- Contact creation
       IF l_proceed = 'Y'
       THEN
            
            ostatus := NULL;
            omsgcount := NULL;
            omsgdata := NULL;
            porgcontactrec.created_by_module := 'TCA_V1_API';
            porgcontactrec.job_title := NULL;
            porgcontactrec.party_rel_rec.subject_id := oppartyid;
            porgcontactrec.party_rel_rec.subject_type := 'PERSON';
            porgcontactrec.party_rel_rec.subject_table_name := 'HZ_PARTIES';
            porgcontactrec.party_rel_rec.object_id := c1.party_id;
            porgcontactrec.party_rel_rec.object_type := 'ORGANIZATION';
            porgcontactrec.party_rel_rec.object_table_name := 'HZ_PARTIES';
            porgcontactrec.party_rel_rec.relationship_code := 'CONTACT_OF';
            porgcontactrec.party_rel_rec.relationship_type := 'CONTACT';
            porgcontactrec.party_rel_rec.start_date := SYSDATE;
                      
            apps.hz_party_contact_v2pub.create_org_contact
                                   (p_init_msg_list        => 'T',
                                    p_org_contact_rec      => porgcontactrec,
                                    x_org_contact_id       => oocorgcontactid,
                                    x_party_rel_id         => oocpartyrelid,
                                    x_party_id             => oocpartyid,
                                    x_party_number         => oocpartyrelnumber,
                                    x_return_status        => ostatus,
                                    x_msg_count            => omsgcount,
                                    x_msg_data             => omsgdata
                                   );

            IF ostatus <> 'S'
            THEN
               
               IF omsgcount > 1
               THEN
                  FOR i IN 1 .. omsgcount
                  LOOP
                     fnd_file.put_line (fnd_file.output,'Error for acct '||c1.account_number||' contact '||c1.first_name||' - ORG CONTACT ' || SUBSTR (apps.fnd_msg_pub.get (p_encoded => apps.fnd_api.g_false), 1, 255 ) );
                  END LOOP;
               ELSE
                  fnd_file.put_line (fnd_file.output,'Error for acct '||c1.account_number||' contact '||c1.first_name||' - ORG CONTACT ' || omsgdata);
               END IF;
               l_proceed := 'N';
               
            ELSE
               l_proceed := 'Y';
            END IF;
              
       END IF; --  Person to Party link has been created

       IF l_proceed = 'Y' AND ostatus = 'S'
       THEN
          ostatus := NULL;
          omsgcount := NULL;
          omsgdata := NULL;
            
          pcustacctrolerec.party_id := oocpartyid;
          pcustacctrolerec.cust_account_id := c1.cust_account_id;
          pcustacctrolerec.created_by_module := 'TCA_V1_API';
          pcustacctrolerec.role_type := 'CONTACT';
                
          apps.hz_cust_account_role_v2pub.create_cust_account_role
                              (p_init_msg_list              => 'T',
                               p_cust_account_role_rec      => pcustacctrolerec,
                               x_cust_account_role_id       => ocustacctroleid,
                               x_return_status              => ostatus,
                               x_msg_count                  => omsgcount,
                               x_msg_data                   => omsgdata
                              );

          IF ostatus <> 'S'
          THEN
             IF omsgcount > 1
             THEN
                FOR i IN 1 .. omsgcount
                LOOP
                   fnd_file.put_line (fnd_file.output,'Error for acct '||c1.account_number||' contact '||c1.first_name||' - CUST ACCT ROLE ' || SUBSTR (apps.fnd_msg_pub.get (p_encoded => apps.fnd_api.g_false), 1, 255 ) );
                END LOOP;
             ELSE
                fnd_file.put_line (fnd_file.output,'Error for acct '||c1.account_number||' contact '||c1.first_name||' - CUST ACCT ROLE ' || omsgdata);
             END IF;

             l_proceed := 'N';
          ELSE
                   
             l_proceed := 'Y';
             
             for c2 in cur_contact_points (c1.cust_account_id, c1.first_name, c1.last_name)
             loop
                exit when cur_contact_points%notfound;
                
                -- Attach Phone Number
                if c2.phone_number is not null then
                    
                    ostatus := NULL;
                    omsgcount := NULL;
                    omsgdata := NULL;
                    pcontactpointrec := NULL;
                    pcontactpointrec.contact_point_type := 'PHONE';
                    pcontactpointrec.owner_table_name := 'HZ_PARTIES';
                    pcontactpointrec.owner_table_id := oocpartyid; 
                    --pcontactpointrec.contact_point_purpose := 'BUSINESS';
                    pcontactpointrec.created_by_module := 'TCA_V1_API';
                    
                    pphonerec.phone_country_code := NULL;
                    pphonerec.phone_area_code := NULL;
                    pphonerec.phone_number := trim(c2.phone_number);
                    pphonerec.phone_extension := NULL;
                    pphonerec.phone_line_type := 'GEN';
                   
                    hz_contact_point_v2pub.create_contact_point (
                        p_init_msg_list       => 'T',
                        p_contact_point_rec   => pcontactpointrec,
                        p_phone_rec           => pphonerec,
                        x_contact_point_id    => ocontactpointid,
                        x_return_status       => ostatus,
                        x_msg_count           => omsgcount,
                        x_msg_data            => omsgdata);

                    IF ostatus <> 'S'
                    THEN
                        IF omsgcount > 1
                        THEN
                            FOR i IN 1 .. omsgcount
                            LOOP
                                fnd_file.put_line (fnd_file.output,'Error for '||c1.account_number||' contact '||c1.first_name||' - Phone: '||c2.phone_number||' Error: '|| SUBSTR (fnd_msg_pub.get (p_encoded => fnd_api.g_false),1,255));                                
                            END LOOP;
                        ELSE
                            fnd_file.put_line (fnd_file.output,'Error for '||c1.account_number||' contact '||c1.first_name||' - Phone: '||c2.phone_number||' Error: '|| omsgdata);
                        END IF;
                        --l_proceed := 'N';
                    ELSE
                        --l_proceed := 'Y';
                        null;
                    END IF;
                    
                end if;
                
                -- Attach Email
                if c2.email_address is not null then
                
                    ostatus := NULL;
                    omsgcount := NULL;
                    omsgdata := NULL;
                    pcontactpointrec := NULL;
                    pcontactpointrec.contact_point_type := 'EMAIL';
                    pcontactpointrec.owner_table_name := 'HZ_PARTIES';
                    pcontactpointrec.owner_table_id := oocpartyid; 
                    --pcontactpointrec.contact_point_purpose := 'BUSINESS';
                    pcontactpointrec.created_by_module := 'TCA_V1_API';
                    
                    pemailrec.email_format := 'MAILHTML';
                    pemailrec.email_address := trim(c2.email_address);
                   
                    hz_contact_point_v2pub.create_contact_point (
                        p_init_msg_list       => 'T',
                        p_contact_point_rec   => pcontactpointrec,
                        p_email_rec           => pemailrec,
                        x_contact_point_id    => ocontactpointid,
                        x_return_status       => ostatus,
                        x_msg_count           => omsgcount,
                        x_msg_data            => omsgdata);

                    IF ostatus <> 'S'
                    THEN
                        IF omsgcount > 1
                        THEN
                            FOR i IN 1 .. omsgcount
                            LOOP
                                fnd_file.put_line (fnd_file.output,'Error for '||c1.account_number||' contact '||c1.first_name||' - Email: '||c2.email_address||' Error: '|| SUBSTR (fnd_msg_pub.get (p_encoded => fnd_api.g_false),1,255));                                
                            END LOOP;
                        ELSE
                            fnd_file.put_line (fnd_file.output,'Error for '||c1.account_number||' contact '||c1.first_name||' - Email: '||c2.email_address|| omsgdata);
                        END IF;
                        --l_proceed := 'N';
                    ELSE
                        --l_proceed := 'Y';
                        null;
                    END IF;
                    
                end if;
                
             end loop;
                 
          END IF;
       END IF;
         
       if nvl(l_proceed,'N') = 'Y' then
          begin
              update  XXWC.XXWC_CUST_CONTACT_STG
              set     contact_id = oocorgcontactid
                      , status = 'CREATED'
              where cust_Account_id = c1.cust_account_id
              AND   nvl(first_name, 'Y') = nvl(c1.first_name, 'Y')
              AND   nvl(last_name, 'Z') = nvl(c1.last_name, 'Z')
              AND   status = 'VALID';
              --where   rowid = c2.rd;
                
              l_count := l_count + 1;
          exception
          when others then
              fnd_file.put_line (fnd_file.output,'Could not update table after contact creation '||c1.account_number||' contact '||c1.first_name);    
          end;  
       else
          begin
              update  XXWC.XXWC_CUST_CONTACT_STG
              set     contact_id = oocorgcontactid
                      , status = 'ERR_CREATING'
              where cust_Account_id = c1.cust_account_id
              AND   nvl(first_name, 'Y') = nvl(c1.first_name, 'Y')
              AND   nvl(last_name, 'Z') = nvl(c1.last_name, 'Z')
              AND   status = 'VALID';
              --where   rowid = c2.rd;
              
              l_count_err := l_count_err + 1;
          exception
          when others then
              fnd_file.put_line (fnd_file.output,'Could not update table after contact creation '||c1.account_number||' contact '||c1.first_name);    
          end;
       end if;
         
       l_commit_count := l_commit_count + 1;
       if l_commit_count >= 100 then
          commit;
          l_commit_count := 0;
       end if;
       
    END LOOP;
      
    fnd_file.put_line (fnd_file.output,'Count of created contacts: '||l_count);
    fnd_file.put_line (fnd_file.output,'Count of errored contact creation: '||l_count_err);
      
    commit;

EXCEPTION
WHEN OTHERS THEN
    l_error_message := 'xxwc_load_cust_contacts_prc '
                || 'Error_Stack...'
                || DBMS_UTILITY.format_error_stack ()
                || ' Error_Backtrace...'
                || DBMS_UTILITY.format_error_backtrace ();

    xxcus_error_pkg.xxcus_error_main_api (
        p_called_from         => 'xxwc_load_cust_contacts_prc'
       ,p_calling             => 'xxwc_load_cust_contacts_prc'
       ,p_request_id          => l_conc_req_id
       ,p_ora_error_msg       => l_error_message
       ,p_error_desc          => 'Error running Customer Contact Load'
       ,p_distribution_list   => 'cgonzalez@luciditycg.com'
       ,p_module              => 'AR');

    fnd_file.put_line (fnd_file.output,'Error in main xxwc_load_cust_contacts_prc. Error: '||l_error_message);
    
    ERRMSG := l_error_message;
    RETCODE := '2';
    raise;        
END xxwc_load_cust_contacts_prc;
/


