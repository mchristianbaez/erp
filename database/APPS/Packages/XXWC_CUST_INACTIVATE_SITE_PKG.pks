CREATE OR REPLACE PACKAGE APPS.XXWC_CUST_INACTIVATE_SITE_PKG
AS
   /**************************************************************************
    *
    * FUNCTION | PROCEDURE | CURSOR
    *  XXWC_CUST_INACTIVATE_SITE_PKG
    *
    * DESCRIPTION
    *  FIN / Mass update of accounts that haven't had activity within the last XX month
    *
    * PARAMETERS
    * ==========
    * NAME              TYPE     DESCRIPTION
    * ----------------- -------- ---------------------------------------------
    * P_MODE           <IN>       Report Only shows only report and commit will inactivate sites
    * P_CLASS          <IN>       Class name
    * P_MONTH          <IN>       No of months
    *
    *
    *
    *
    * CALLED BY
    *   Which program, if any, calls this one
     * HISTORY
    * =======
    *
    * VERSION DATE        AUTHOR(S)       DESCRIPTION
    * ------- ----------- --------------- ------------------------------------
    * 1.00    11/11/2013   Maharajan S            Creation
    *
    *************************************************************************/
   PROCEDURE XXWC_CUST_INACTIVATE_SITE_PRC (errbuf       OUT VARCHAR2,
                                            retcode      OUT NUMBER,
                                            p_resp        IN VARCHAR2,
                                            P_resp_status IN VARCHAR2,
                                            p_mode    IN     VARCHAR2,
                                            p_class   IN     VARCHAR2,
                                            p_month   IN     NUMBER,
                                            p_date    IN     DATE,
                                            p_email   IN     VARCHAR2);
END;
/
