/* Formatted on 09-Apr-2014 16:25:00 (QP5 v5.206) */
-- Start of DDL Script for Package APPS.XXWC_PO_VENDOR_MIN_PKG
-- Generated 09-Apr-2014 16:24:56 from APPS@EBIZFQA

CREATE OR REPLACE PACKAGE apps.xxwc_po_vendor_min_pkg
AS
    /***************************************************************************
      $Header xxwc_po_vendor_min_pkg$
      Module Name: xxwc_po_vendor_min_pkg.pks

      PURPOSE:   This package is used by the Vendor Minimum Form

      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------
      1.0        10/30/2012  Consuelo Gonzalez      Initial Version
      2.0        10 April 2014 Changes for internal reqs by Consuelo and Rasikha. TMS 20131120-00030

    **************************************************************************/

    FUNCTION get_onhand_qty (item_id          IN NUMBER
                            ,lot_control      IN NUMBER
                            ,org_id           IN NUMBER
                            ,subinv           IN CHAR
                            ,include_nonnet   IN NUMBER)
        RETURN NUMBER;

    FUNCTION get_req_qty (supply_cutoff_date   IN DATE
                         ,org_id               IN NUMBER
                         ,current_item_id      IN NUMBER
                         ,include_po           IN NUMBER
                         ,include_nonnet       IN NUMBER
                         ,include_wip          IN NUMBER
                         ,include_if           IN NUMBER
                         ,subinv               IN CHAR)
        RETURN NUMBER;

    FUNCTION get_po_qty (supply_cutoff_date   IN DATE
                        ,org_id               IN NUMBER
                        ,current_item_id      IN NUMBER
                        ,include_po           IN NUMBER
                        ,include_nonnet       IN NUMBER
                        ,include_wip          IN NUMBER
                        ,include_if           IN NUMBER
                        ,subinv               IN CHAR)
        RETURN NUMBER;

    FUNCTION get_demand (item_id              IN NUMBER
                        ,org_id               IN NUMBER
                        ,demand_cutoff_date   IN DATE
                        ,net_rsv              IN NUMBER
                        ,include_nonnet       IN NUMBER
                        ,include_wip          IN NUMBER
                        ,net_unrsv            IN NUMBER
                        ,net_wip              IN NUMBER
                        ,subinv               IN CHAR)
        RETURN NUMBER;

    -- 02/05/14 CG: TMS 20131120-00030: Updated to consider FLM in final number
    FUNCTION get_ord_qty (p_max           IN NUMBER
                         ,p_available     IN NUMBER
                         ,p_open_po       IN NUMBER
                         ,p_open_req      IN NUMBER
                         ,p_open_demand   IN NUMBER
                         ,p_flm           IN NUMBER)
        RETURN NUMBER;

    FUNCTION get_status (p_min           IN NUMBER
                        ,p_max           IN NUMBER
                        ,p_available     IN NUMBER
                        ,p_open_po       IN NUMBER
                        ,p_open_req      IN NUMBER
                        ,p_open_demand   IN NUMBER)
        RETURN VARCHAR2;

    FUNCTION get_perc_spread (p_min           IN NUMBER
                             ,p_max           IN NUMBER
                             ,p_available     IN NUMBER
                             ,p_open_po       IN NUMBER
                             ,p_open_req      IN NUMBER
                             ,p_open_demand   IN NUMBER)
        RETURN NUMBER;

    FUNCTION get_period_sales (p_period_type IN VARCHAR2, p_inventory_item_id IN NUMBER, p_organization_id IN NUMBER)
        RETURN NUMBER;

    -- 12/12/13 CG:TMS 20131211-00210: Added to be able to pull with Price Breaks
    FUNCTION get_po_cost (i_inventory_item_id   IN NUMBER
                         ,i_organization_id     IN NUMBER
                         ,i_vendor_id           IN NUMBER
                         ,i_vendor_site_id      IN NUMBER
                         ,i_ord_qty             IN NUMBER)
        RETURN NUMBER;

    -- 01/16/14 CG: TMS 20131120-00030: Added to retrieve Incomplete Internal Req Qty's
    FUNCTION get_inc_int_req_qty (supply_cutoff_date   IN DATE
                                 ,dst_org_id           IN NUMBER
                                 ,current_item_id      IN NUMBER
                                 ,src_org_id           IN NUMBER)
        RETURN NUMBER;

    -- 01/21/14 CG: TMS 20131216-00029: New procedure based report to extract Vendor Min Data
    PROCEDURE xxwc_vendor_min_extract_rpt (errbuf             OUT VARCHAR2
                                          ,retcode            OUT NUMBER
                                          ,p_org_id        IN     NUMBER
                                          ,p_org_list_id   IN     NUMBER
                                          ,p_sup_list_id   IN     NUMBER);

    TYPE cursor_type IS REF CURSOR;

    TYPE view_tab IS TABLE OF xxwc.xxwc_po_vendor_min_int_stg%ROWTYPE
        INDEX BY BINARY_INTEGER;

    g_view_table    view_tab;
    g_view_record   view_tab;

    PROCEDURE xxwc_populate_temp (p_source_org_id         IN NUMBER
                                 ,p_inventory_item_id     IN NUMBER
                                 ,p_demand_cut_off_date   IN VARCHAR2);

    --added by Rasikha to tune the form 2/6/2014
    PROCEDURE populate_items_table (p_organization_id NUMBER, p_vendor_id NUMBER, p_vendor_site_id NUMBER);
END xxwc_po_vendor_min_pkg;
/

-- End of DDL Script for Package APPS.XXWC_PO_VENDOR_MIN_PKG
