CREATE OR REPLACE PACKAGE XXWC_SPEEDBUILD_IB_PKG AS
   /*************************************************************************
   *   $Header XXWC_SPEEDBUILD_IB_PKG.PKG $
   *   Module Name: XXWC_SPEEDBUILD_IB_PKG.PKG
   *
   *   PURPOSE:   This package is used for SpeedBuild SalesOrder Inbound Interface
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        11/18/2013  Gopi Damuluri             Initial Version   
   * ***************************************************************************/
   PROCEDURE load_interface (p_errbuf                 OUT  VARCHAR2,
                             p_retcode                OUT  NUMBER,
                             p_validate_only           IN  VARCHAR2
                            );

   PROCEDURE create_contact (p_customer_id   IN NUMBER
                           , p_first_name    IN VARCHAR2
                           , p_last_name     IN VARCHAR2
                           , p_contact_num   IN VARCHAR2
                           , p_cust_contact_id OUT NUMBER
                           , p_error_message OUT VARCHAR2);

END XXWC_SPEEDBUILD_IB_PKG;
/