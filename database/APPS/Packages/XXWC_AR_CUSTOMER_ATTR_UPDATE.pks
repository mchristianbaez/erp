/* Formatted on 25-Feb-2013 13:16:36 (QP5 v5.206) */
-- Start of DDL Script for Package APPS.XXWC_AR_CUSTOMER_ATTR_UPDATE
-- Generated 25-Feb-2013 13:16:33 from APPS@EBIZFQA

CREATE OR REPLACE PACKAGE apps.xxwc_ar_customer_attr_update
/**************************************************************************
*
* HEADER
 *   Source control header
*
* PROGRAM NAME
*  XXWC_AR_customer_attr_update
*
 * DESCRIPTION
*  used for WC mass account update FOR SALESREP, COLLECTOR, CREDIT ANALYST
*
*
* PARAMETERS
* ==========
* NAME              DESCRIPTION
.* ----------------- ------------------------------------------------------
*
*
* DEPENDENCIES
3 concurrent programs:  XXWC MASS UPDATE%
select * from xxwc.xxwc_account_mass_update_arch - keeps history of updates, can be used to revert changes
select * from xxwc.xxwc_account_mass_update_hist order by run_id desc --keeps all total changes

* xxwc.xxwc_account_mass_report global temporary table for reporting the changes
* xxwc.xxwc_account_mass_update_arch table - keep history of updates for 30 days
* xxwc.xxwc_account_mass_update_list global temporary table, data will be saved in  xxwc.xxwc_account_mass_update_arch table
*

*
*
* HISTORY
* =======
*
* VERSION DATE        AUTHOR(S)       DESCRIPTION
* ------- ----------- --------------- ------------------------------------
* 1.00    28-Nov-2012 Rasikha Galimova   Creation
* 1.01     25- feb-2012 Rasikha Galimova added adiitional parameter for account status , tms=20130121-00575 
*
*
*
*************************************************************************/

IS
    FUNCTION get_sales_rep (p_org_id NUMBER, p_salesrep_id NUMBER)
        RETURN VARCHAR2;

    FUNCTION credit_analyst (p_credit_analyst_id NUMBER)
        RETURN VARCHAR2;

    PROCEDURE xxwc_update_account (pr_cust_rowid             VARCHAR2
                                  ,pr_primary_salesrep_id    NUMBER
                                  ,p_last_updated_by         NUMBER
                                  ,p_last_update_date        DATE
                                  ,p_last_update_login       NUMBER
                                  ,p_responsibility_id       NUMBER
                                  ,p_org_id                  NUMBER);                                       --public api

    PROCEDURE xxwc_update_cust_site_uses (p_site_rowid           VARCHAR2
                                         ,p_salesrep_id          NUMBER
                                         ,p_freight_term         VARCHAR2
                                         ,p_last_updated_by      NUMBER
                                         ,p_last_update_date     DATE
                                         ,p_last_update_login    NUMBER
                                         ,p_responsibility_id    NUMBER
                                         ,p_org_id               NUMBER);                                   --public api

    PROCEDURE xxwc_update_customer_profile (p_prof_rowid           VARCHAR2
                                           ,p_salesrep_id          NUMBER
                                           ,p_collector_id         NUMBER
                                           ,p_remmit_to_code       VARCHAR2
                                           ,p_freight_term         VARCHAR2
                                           ,p_analyst_id           NUMBER
                                           ,p_credit_checking      VARCHAR2
                                           ,p_credit_hold          VARCHAR2
                                           ,p_payment_term_id      NUMBER
                                           ,p_profile_class_id     NUMBER
                                           ,p_account_status       VARCHAR2
                                           ,p_credit_class_code    VARCHAR2
                                           ,p_cust_rowid           VARCHAR2
                                           ,p_site_uses_rowid      VARCHAR2
                                           ,p_last_updated_by      NUMBER
                                           ,p_last_update_date     DATE
                                           ,p_last_update_login    NUMBER
                                           ,p_responsibility_id    NUMBER
                                           ,p_org_id               NUMBER
                                           ,p_level_flag           VARCHAR2);

    PROCEDURE change_credit_analyst_id (p_errbuf                       OUT VARCHAR2
                                       ,p_retcode                      OUT VARCHAR2
                                       ,p_old_credit_analyst_id            NUMBER
                                       ,p_new_credit_analyst_id            NUMBER
                                       ,p_state                            VARCHAR2
                                       ,p_include_nonactive_customer       VARCHAR2
                                       ,p_account_status                   VARCHAR2);

    PROCEDURE change_collector_id (p_errbuf                       OUT VARCHAR2
                                  ,p_retcode                      OUT VARCHAR2
                                  ,p_old_collector_id                 NUMBER
                                  ,p_new_collector_id                 NUMBER
                                  ,p_state                            VARCHAR2
                                  ,p_include_nonactive_customer       VARCHAR2
                                  ,p_account_status                   VARCHAR2);

    PROCEDURE change_sales_rep_id (p_errbuf                       OUT VARCHAR2
                                  ,p_retcode                      OUT VARCHAR2
                                  ,p_old_sales_rep_id                 NUMBER
                                  ,p_new_sales_rep_id                 NUMBER
                                  ,p_state                            VARCHAR2
                                  ,p_include_nonactive_customer       VARCHAR2
                                  ,p_account_status                   VARCHAR2);

    PROCEDURE xxwc_update_prifile_class (px_prof_rowid           VARCHAR2
                                        ,px_profile_class_id     NUMBER
                                        ,px_cust_rowid           VARCHAR2
                                        ,px_site_uses_rowid      VARCHAR2
                                        ,px_last_updated_by      NUMBER
                                        ,px_last_update_date     DATE
                                        ,px_last_update_login    NUMBER
                                        ,px_responsibility_id    NUMBER
                                        ,px_org_id               NUMBER);

    FUNCTION get_account_status (p_in_account_status VARCHAR2)
        RETURN VARCHAR2;
END;
-- Package spec
/

-- End of DDL Script for Package APPS.XXWC_AR_CUSTOMER_ATTR_UPDATE
