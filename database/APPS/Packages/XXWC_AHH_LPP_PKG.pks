CREATE OR REPLACE PACKAGE XXWC_AHH_LPP_PKG
IS
   /***********************************************************************************************************        
        $Header XXWC_AHH_LPP_PKG $
        Module Name: XXWC_AHH_LPP_PKG.pkb

        PURPOSE:   AHH LPP Conversion

        REVISIONS:
        Ver        Date        Author           Description
        ---------  ----------  ---------------  ------------------------------------------------------------------
        1.0        09/21/2018  Niraj K Ranjan    TMS#20180926-00003   AHH LLP price upload
   ****************************************************************************************************************/
   
   
   PROCEDURE main (x_errbuf OUT VARCHAR2, x_retcode OUT VARCHAR2 );
   
   

END XXWC_AHH_LPP_PKG;
/