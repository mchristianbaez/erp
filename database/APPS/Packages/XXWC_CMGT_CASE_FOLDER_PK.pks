--
-- XXWC_CMGT_CASE_FOLDER_PK  (Package) 
--
CREATE OR REPLACE PACKAGE APPS.xxwc_cmgt_case_folder_pk
AS
   /*************************************************************************
    *  Copyright (c) 2011 Lucidity Consulting Group
    *  All rights reserved.
    *************************************************************************
    *   $Header XXWC_CMGT_CASE_FOLDER_PK.pkb $
    *   Module Name: XXWC_CMGT_CASE_FOLDER_PK.pkb
    *
    *   PURPOSE:This package is used by the conc program -XXWC Close case folder
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         ----------------------
    *   1.0        09/19/2011  Vivek Lakaman             Initial Version
    * ************************************************************************/

   --Define variables for logging debug messages
   g_level_unexpected   CONSTANT NUMBER := 6;
   g_level_error        CONSTANT NUMBER := 5;
   g_level_exception    CONSTANT NUMBER := 4;
   g_LEVEL_EVENT        CONSTANT NUMBER := 3;
   g_LEVEL_PROCEDURE    CONSTANT NUMBER := 2;
   g_LEVEL_STATEMENT    CONSTANT NUMBER := 1;

   /*************************************************************************
    *   Procedure Name: Log_Msg
    *
    *************************************************************************
    *   Purpose       : To Log the debug message
    *   Parameter:
    *       IN
    *            p_debug_level  -Debug Level
    *            p_mod_name     -Module Name
    *            p_debug_msg    -Debug Message
    * ***********************************************************************/

   PROCEDURE log_msg (p_debug_level   IN NUMBER
                     ,p_mod_name      IN VARCHAR2
                     ,p_debug_msg     IN VARCHAR2);


   /*************************************************************************
    *   Procedure Name: submit_request
    *
    *************************************************************************
    *   Purpose       : To Log the debug message
    *   Parameter:
    *       OUT
    *            errbuf       - Error Message
    *            retcode      - Error Code
    * ***********************************************************************/
   PROCEDURE submit_request (errbuf OUT VARCHAR2, retcode OUT VARCHAR2);
END xxwc_cmgt_case_folder_pk;
/

