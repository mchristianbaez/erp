--
-- XXWC_RCV_UPDATE_LIST_PRICE  (Package) 
--
CREATE OR REPLACE PACKAGE APPS.XXWC_RCV_UPDATE_LIST_PRICE
AS
   /******************************************************************************
      NAME:       XXWC_RCV_UPDATE_LIST_PRICE

      PURPOSE:    Concurrent Program to Update Organization Item's List Price based on the receipt

      Logic:     1) Perform a receiving transactions
                 2) At the end of the day, run the concurrent request to look at the current day's transactions
                 3) Update the list price at the org site

      REVISIONS:
      Ver        Date        Author           Description
      ---------  ----------  ---------------  ------------------------------------
      1.0        07-MAR-2012  Lee Spitzer       1. Create the PL/SQL Package
      2.0        05-MAY-2012  Lee Spitzer       2. Updated Error Handling
   ******************************************************************************/

   /********************************************************************************
     PROCEDURE J UPDATE_ITEM_LIST_PRICE_CCR used to pass the parameters from the concurrent request to the UPDATE_ITEM_LIST_PRICE Procedure

       Parameters:
            ERRBUF             OUT NOCOPY VARCHAR2 --Standard for Oracle Concurrent Programs
           ,RETCODE            OUT     NUMBER --Standard for Oracle Concurrent Programs
           ,P_ORG_ID           IN      NUMBER --Operating Unit
           ,P_DATE             IN      VARCHAR2 --Date
           ,P_USER_ID          IN      NUMBER) --User ID

   ********************************************************************************/
   PROCEDURE UPDATE_ITEM_LIST_PRICE_CCR (ERRBUF         OUT NOCOPY VARCHAR2 --Standard for Oracle Concurrent Programs
                                        ,RETCODE        OUT        NUMBER --Standard for Oracle Concurrent Programs
                                        ,P_ORG_ID    IN            NUMBER --Operating Unit
                                        ,P_DATE      IN            VARCHAR2 --Date
                                        ,P_USER_ID   IN            NUMBER); --User ID

   /********************************************************************************
     PROCEDURE J UPDATE_ITEM_LIST_PRICE_CCR used to pass the parameters from the concurrent request to the UPDATE_ITEM_LIST_PRICE Procedure

       Parameters:
            P_ORG_ID           IN      NUMBER --Operating Unit
           ,P_DATE             IN      DATE --Date
           ,P_USER_ID          IN      NUMBER) --User ID

   ********************************************************************************/

   PROCEDURE UPDATE_ITEM_LIST_PRICE (P_ORG_ID          IN     NUMBER --Operating Unit
                                    ,P_DATE            IN     DATE      --Date
                                    ,P_USER_ID         IN     NUMBER
                                    ,P_RETURN_STATUS      OUT VARCHAR2); --User ID
END XXWC_RCV_UPDATE_LIST_PRICE;
/

