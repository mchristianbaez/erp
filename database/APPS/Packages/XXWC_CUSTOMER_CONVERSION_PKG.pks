CREATE OR REPLACE PACKAGE APPS.xxwc_customer_conversion_pkg
AS
    /***************************************************************************************
    *  Script Name: XXWC_CUSTOMER_CONVERSION_PKG.pks
    *
    *  Script Owners: Lucidity Consulting Group.
    *
    *  Client: White Cap.
    *
    *  Interface / Conversion Name: Customers Conversion.
    *
    *  Functional Purpose: Convert customers using HZ APIs.
    *
    *  History:
    *
    *  Version  Date          Author          Description
    *****************************************************************************************
    *  1.0      04-Sep-2011   Karun Puri     Initial development.
    *  1.1                    Vasanth S      Added logic for OE Shipments : Concurrent Program 
    *  1.2                    Satish U       Performance Turning 
    *  1.3                    Satish U       Added Concurrent Program to  Inactive Customers 
    *****************************************************************************************/

    g_parameter_prefix   VARCHAR2 (30);

    g_org_id             CONSTANT NUMBER := fnd_global.org_id;

    --- **************************************************************************************
    -- PROCEDURE populate_collectors 
    --  Satish U: This procedure will populate Collector Id in the XREF Table xxwc_collector_cr_analyst_xref
    -- Populate_Collectors API is called during validation process XXWC_VALIDATIONS 
    
    PROCEDURE populate_collectors;
    
    
    --- **************************************************************************************
    -- PROCEDURE populate_credit_analyst
    -- Satish U : This Procedure will populate Credit Analyst ID :
    --            In the XREF Table xxwc_collector_cr_analyst_xref. 

    PROCEDURE populate_credit_analyst;
    
    --- **************************************************************************************
    -- Procedure xxwc_validations :  API does Custoemr Conversion program validation and also calls APIS to populate
    --  Oracle IDs in some of the Staging Tables. 

    PROCEDURE xxwc_validations;
    
    
    --- **************************************************************************************
    -- Procedure Create_Delivery_Contact :  API creates Delivery Contacts either at Party or Party Site Level. 
    
    Procedure  Create_Delivery_Contact(
            p_Party_ID         in Number ,
            p_Party_Site_ID    In Number ,
            p_Delivery_Contact In Varchar2 ,
            x_Return_Status    Out  Varchar2, 
            x_Msg_Data         Out  Varchar2 );
            
            
   --- ***************************************************************************************************
    -- Procedure xxwc_customer_conv:  Main API called from Concurrent Program Customer Conversion Program  
    --  

    PROCEDURE xxwc_customer_conv (errbuf       OUT VARCHAR2,
                                  retcode      OUT VARCHAR2,
                                  p_mode           VARCHAR2,
                                  prefix    IN     VARCHAR2 DEFAULT NULL);
                                  
    --- **************************************************************************************
    -- Procedure xxpop_contact_info :  Currently this API is not used. 

    PROCEDURE xxpop_contact_info (p_site_use_type       IN     VARCHAR2,
                                  p_party_id            IN     NUMBER,
                                  p_cust_acct_id        IN     NUMBER,
                                  p_cust_acct_site_id   IN     NUMBER,
                                  p_rowid               IN     ROWID,
                                  p_out                    OUT VARCHAR2);
                                  
    --- ***************************************************************************************************
    -- Procedure xxpop_org_communication :  Organization Communication like Phone Number, Fax Number, Email 
    -- Information is created using this API  : Contact Points are created either at Party or Party Site Level
    
    PROCEDURE xxpop_org_communication (p_level             IN     VARCHAR2,
                                       p_party_id          IN     NUMBER,
                                       p_phone_area_code   IN     VARCHAR2,
                                       p_contact_phone     IN     VARCHAR2,
                                       p_fax_area_code     IN     VARCHAR2,
                                       p_contact_fax       IN     VARCHAR2,
                                       p_out               OUT VARCHAR2,
                                       p_err_msg           OUT VARCHAR2);

    --- **************************************************************************************
    -- Procedure get_cust_conversion_stats :  API that gets customer conversion statistics and displays them in outfile. 

    PROCEDURE get_cust_conversion_stats;
    
    --- **************************************************************************************
    -- Procedure populate_site_locations :  API to populate Customer Site Locations. It has the logic to identify 
    -- duplicate addresses with in a party and assign oracle location Id, Legacy Site Type column value in Customer Sites Staging table. 

    PROCEDURE populate_site_locations;


    --- ********************************************************************************************************************
    -- Procedure update_customer_without_sites :  API Identifies Customer Records with out any sites and updates their status as Inactive' 
    PROCEDURE update_customer_without_sites;
    
     --- ********************************************************************************************************************
    -- Procedure update_Sites_without_Customer :  API Identifies Orphan Customer  Sites Records 
    -- Sites with out corresponding Customer Records and updates status as 'E'  
    
    PROCEDURE update_Sites_without_Customer;
    
    --- ********************************************************************************************************************
    -- Procedure update_contacts_without_sites :  API Identifies Orphan Customer  Contacts, Records 
    -- ith out corresponding Customer  or Customer Customer Records and updates those records status as 'E'  
    
    PROCEDURE update_contacts_without_sites;
    
    --- ********************************************************************************************************************
    -- Procedure oe_ship_to :  Another Concurrent Program to process OE SHIP TO sites. This program should be called only 
    -- after main customer Conversion program is processed successfully. 
    -- API Processes all Customer Sites that are validated and whose customers are already processed through main program and 
    -- creates Customer Sites and its contacts 

    PROCEDURE oe_ship_to (errbuf       OUT VARCHAR2,
                          retcode      OUT VARCHAR2,
                          prefix    IN     VARCHAR2 DEFAULT NULL);
                          
    --- ********************************************************************************************************************
    -- Procedure xxpop_org_web_contact :  API to create WEB Contact at Party or Party Site Level . This API is called from 
    -- the main Customer Conversion program                  
    PROCEDURE xxpop_org_web_contact   (p_level             IN     VARCHAR2,
                                       p_party_id          IN     NUMBER,
                                       p_cod_comment       IN     VARCHAR2,
                                       p_keyword           IN     VARCHAR2, 
                                       p_out                  OUT VARCHAR2,
                                       p_err_msg              OUT VARCHAR2);
                                       
    ---**************************************************************************************-------------
    -- ***** Procedure Inactivate_Cust_Accounts : to Inactive Cust Accounts that are in Inactive Status in Staging Table*************---- 
    --    Initially all Customers are created in active status. Upon creation of AR transactions this procedure should be called 
    -- to inactivate customers that are supposed to be based on Staging table information. 
                                       
   PROCEDURE  Inactivate_Cust_Accounts (Errbuf   Out Varchar2, 
                                        Retcode  Out Varchar2 ) ; 
                                        
   ---**************************************************************************************-------------
   -- ***** Procedure Update_Party_Site_Locations:  Creates new Locations that are shared between party sites. 
   --******* If User Pass Customer Number as input parameter then the program looks for all party Sites for a given 
   --****** Customer else it process all party Sites that share Locations. 
   --*******************************************************************************************-------------------
                                       
   PROCEDURE  Update_Party_Site_Locations(Errbuf   Out Varchar2, 
                                          Retcode  Out Varchar2,
                                          p_Cust_Account_Id IN Number,
                                          P_Number_Of_Sites In Number) ; 
                                        
                                        
  
END xxwc_customer_conversion_pkg;
/
Show errors;
commit;
exit;