
  CREATE OR REPLACE PACKAGE "APPS"."XXCUS_AR_TRX_EXEMPTION_PKB" 
-- ESMS TICKET 195714
-- Used by HDS AR Invoice Exemption Summary
--Date 17-Jun-2013
AS
 
 g_ledger_id    number :=0;
 g_period_name  varchar2(10);
 g_product      varchar2(3);
 g_list_of_accounts varchar2(150);
 g_report_type  varchar2(40) :=''; 

 procedure main (
    retcode               out number
   ,errbuf                out varchar2
   ,p_ledger_id           in  number
   ,p_period_name         in  varchar2
  );  

end XXCUS_AR_TRX_EXEMPTION_PKB;;
