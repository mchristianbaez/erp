CREATE OR REPLACE PACKAGE APPS.XXWC_BRIG_INTER_ORG_XFER_PKG AS

   /*************************************************************************
   *   $Header XXWC_BRIG_INTER_ORG_XFER_PKG.PKG $
   *   Module Name: XXWC_BRIG_INTER_ORG_XFER_PKG.PKG
   *
   *   PURPOSE:   This package is used for Inter Org Transfer of Brigade Items
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        07/11/2014  Gopi Damuluri             Initial Version   
   * ***************************************************************************/

   PROCEDURE LOAD_INTERFACE (p_item_num                IN  VARCHAR2,
                             p_src_org                 IN  VARCHAR2,
                             p_dest_org                IN  VARCHAR2,
                             p_location                IN  VARCHAR2,
                             p_sub_inv                 IN  VARCHAR2,
                             p_quantity                IN  NUMBER);
END XXWC_BRIG_INTER_ORG_XFER_PKG;                             
/