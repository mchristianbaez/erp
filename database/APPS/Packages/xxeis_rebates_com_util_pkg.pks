CREATE OR REPLACE PACKAGE APPS.xxeis_rebates_com_util_pkg IS

  G_PERIOD_NAME         VARCHAR2 (50);   --10/2/13 SR 207441
  G_CURRENT_DAYS              NUMBER;    -- 03/19/2014 SR 243034
  g_total_days                 number;   -- 03/19/2014 SR 243034
  g_past_period              varchar2(10) :=Null; -- Ver 2.1
  /*******************************************************************************
  * Procedure:   CURRENT_CALENDAR
  * Description: This is called the Rebates SV Report
  *              Sourcing Volume performance related to Rebates by LOB
  *
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     05/20/2013    Kathy Poling    Initial creation of the procedure
                                        SR 200873
  2.0 	  03/21/2014    Balaguru Seshadri RFC 39804 / ESMS 243034
  2.1     07/05/2016    Balaguru Seshadri TMS 20160620-00178 / ESMS 357886
  ********************************************************************************/

  FUNCTION fiscal_period(p_fiscal_period_id IN VARCHAR2) RETURN NUMBER;

  FUNCTION current_calendar(p_fiscal_period_id IN NUMBER) RETURN NUMBER;

  FUNCTION prior_calendar(p_fiscal_period_id IN NUMBER) RETURN NUMBER;

  FUNCTION get_sv_data(p_fiscal_period_id IN VARCHAR2)
    RETURN xxcus_rebate_sv_tab
    PIPELINED;

  FUNCTION get_period RETURN NUMBER;

  FUNCTION sdw_receipt_recon(p_period IN VARCHAR2)
    RETURN xxcus_rebate_oracle_sdw_tab
    PIPELINED;

--10/2/13  SR 207441
  PROCEDURE SET_PERIOD_NAME (P_PERIOD VARCHAR);

    /*******************************************************************************
  * Functions:   get_current_days , get_total_days
  * Description: Used for passing parameter in EiS
  *
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     3/19/2014    Dragan Velimirovic    Initial creation of the functions
                                                SR 243034
  ********************************************************************************/

   FUNCTION GET_CURRENT_DAYS    RETURN NUMBER;
   FUNCTION get_total_days    RETURN number;
END xxeis_rebates_com_util_pkg;
/