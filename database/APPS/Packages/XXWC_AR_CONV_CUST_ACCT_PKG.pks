CREATE OR REPLACE 
PACKAGE      APPS.XXWC_AR_CONV_CUST_ACCT_PKG
   AUTHID CURRENT_USER
AS
   /******************************************************************************************************************************************************
        $Header XXWC_AR_CONV_CUST_ACCT_PKG $
        Module Name: XXWC_AR_CONV_CUST_ACCT_PKG.pks

        PURPOSE:   EQUIFAX Project

        REVISIONS:
        Ver        Date        Author             Description
        ---------  ----------  --------------- ----------------------------------------------------------------------------------------------------------
        1.0        07/03/2018  Ashwin Sridhar     TMS#20180308-00290 -AH HARRIS Customer Conversion
   ******************************************************************************************************************************************************/

   PROCEDURE checking_mandatory_fields (p_record_id   IN     NUMBER,
                                        p_row_id      IN     VARCHAR2,
                                        x_msg_count      OUT NUMBER,
                                        x_ret_mess       OUT VARCHAR2);

   PROCEDURE validate_inbound_data (p_record_id   IN     NUMBER,
                                    p_row_id      IN     VARCHAR2,
                                    x_msg_count      OUT NUMBER,
                                    x_ret_mess       OUT VARCHAR2);

   PROCEDURE create_customer (P_RECORD_ID         IN     NUMBER,
                              x_cust_account_id      OUT NUMBER,
                              x_acct_number          OUT VARCHAR2,
                              x_party_id             OUT NUMBER,
                              x_party_number         OUT VARCHAR2,
                              x_profile_id           OUT NUMBER,
                              x_ret_status           OUT VARCHAR2,
                              x_msg_count            OUT NUMBER,
                              x_msg_data             OUT VARCHAR2);

   PROCEDURE update_customer (P_RECORD_ID         IN     NUMBER,
                              x_cust_account_id      OUT NUMBER,
                              x_acct_number          OUT VARCHAR2,
                              x_party_id             OUT NUMBER,
                              x_party_number         OUT VARCHAR2,
                              x_profile_id           OUT NUMBER,
                              x_ret_status           OUT VARCHAR2,
                              x_msg_count            OUT NUMBER,
                              x_msg_data             OUT VARCHAR2);

   PROCEDURE inactivate_party_sites (p_record_id    IN     NUMBER,
                                     x_ret_status      OUT VARCHAR2,
                                     x_msg_count       OUT NUMBER,
                                     x_msg_data        OUT VARCHAR2);

   PROCEDURE inactivate_acct_sites (p_record_id    IN     NUMBER,
                                    x_ret_status      OUT VARCHAR2,
                                    x_msg_count       OUT NUMBER,
                                    x_msg_data        OUT VARCHAR2);

   PROCEDURE create_multiple_sites (p_record_id      IN     NUMBER,
                                    p_party_id       IN     NUMBER,
                                    p_cust_acct_id   IN     NUMBER,
                                    x_ret_status        OUT VARCHAR2,
                                    x_msg_count         OUT NUMBER,
                                    x_msg_data          OUT VARCHAR2);

   PROCEDURE main (p_errbuf             OUT VARCHAR2,
                   p_retcode            OUT VARCHAR2,
                   p_import_option   IN     VARCHAR2);

   PROCEDURE CUST_ACCT_UPDATE (P_CUST_ACCOUNT_ID   IN NUMBER,
                               P_STATUS            IN VARCHAR2);
END XXWC_AR_CONV_CUST_ACCT_PKG;
/