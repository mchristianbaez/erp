CREATE OR REPLACE PACKAGE APPS.XXWC_ACCT_MGR_UPD_FILE_PKG
AS

  /**************************************************************************
     $Header XXWC_ACCT_MGR_UPD_FILE_PKG $
     Module Name: XXWC_ACCT_MGR_UPD_FILE_PKG.pks

     PURPOSE:   This package is called by the concurrent programs
                XXWC Credit-Account Manager Update
     REVISIONS:
     Ver        Date        Author             Description
     ---------  ----------  ---------------   -------------------------
      1.0       05/12/2016  Neha Saini         Initial Build - Task ID: 20170616-00292
   /*************************************************************************

   /*************************************************************************
     Procedure : generate_file

     PURPOSE   :This procedure creates file for XXWC Credit-Account Manager Update
     PARAMETERS:  IN - None
                  OUT - errbuf,retcode
     REVISIONS:
     Ver        Date        Author             Description
     ---------  ----------  ---------------   -------------------------
      1.0       05/12/2016  Neha Saini         Initial Build - Task ID: 20170616-00292
   ************************************************************************/
   PROCEDURE generate_file (errbuf OUT VARCHAR2, retcode OUT VARCHAR2);

END XXWC_ACCT_MGR_UPD_FILE_PKG;
/