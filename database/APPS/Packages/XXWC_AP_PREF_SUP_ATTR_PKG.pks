CREATE OR REPLACE PACKAGE APPS.XXWC_AP_PREF_SUP_ATTR_PKG
/*************************************************************************
  $Header XXWC_AP_PREF_SUP_ATTR_PKG.pks $
  Module Name: XXWC_AP_PREF_SUP_ATTR_PKG

  PURPOSE: Package to upload the Preferred Supplier Attributes though WEB ADI

  TMS Task Id :  20141104-00115

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    ----------------
  1.0        29-Oct-2014  Manjula Chellappan    Initial Version


**************************************************************************/
IS
   PROCEDURE upload_main (p_supplier_name    VARCHAR2,
                          p_supplier_num     VARCHAR2,
                          p_supplier_tier    VARCHAR2,
                          p_supplier_sba     VARCHAR2);
END XXWC_AP_PREF_SUP_ATTR_PKG;