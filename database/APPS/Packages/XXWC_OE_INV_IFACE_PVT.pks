--
-- XXWC_OE_INV_IFACE_PVT  (Package) 
--
CREATE OR REPLACE PACKAGE APPS.XXWC_OE_Inv_Iface_PVT
   AUTHID CURRENT_USER
AS
   /* $Header: OEXVIIFS.pls 120.0.12010000.1 2008/07/25 08:03:05 appldev ship $ */

   --  Start of Comments
   --  API name    OE_Inv_Iface_PVT
   --  Type        Private
   --  Version     Current version = 1.0
   --              Initial version = 1.0

   /*************************************************************************
   *  Copyright (c) 2011 Lucidity Consulting Group
   *  All rights reserved.
   **************************************************************************
   *   $Header XXWC_OE_Inv_Iface_PVT.pks $
   *   Module Name: XXWC_OE_Inv_Iface_PVT.pks
   *
   *   PURPOSE:   This package is used by the XXWC_OE_INV_IFACE_WF.INVENTORY_INTERFACE
   *              which is used in WC Inventory Interface in OEOL Workflow.
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        09/29/2011  Srinivas Malkapuram             Initial Version
   * ***************************************************************************/

   g_level_unexpected   CONSTANT NUMBER := 6;
   g_level_error        CONSTANT NUMBER := 5;
   g_level_exception    CONSTANT NUMBER := 4;
   g_LEVEL_EVENT        CONSTANT NUMBER := 3;
   g_LEVEL_PROCEDURE    CONSTANT NUMBER := 2;
   g_LEVEL_STATEMENT    CONSTANT NUMBER := 1;

   TYPE serial_type IS RECORD
   (
      transaction_interface_id   NUMBER
     ,source_code                VARCHAR2 (30)
     ,source_line_id             NUMBER
     ,fm_serial_number           VARCHAR2 (30)
     ,to_serial_number           VARCHAR2 (30)
     ,last_update_date           DATE
     ,last_updated_by            NUMBER
     ,creation_date              DATE
     ,created_by                 NUMBER
   );

   --***************************************************************************
   -- Procedure Inventory_Interface
   -- Purpose : Inserts records into MTL_TRANSACTIONS_INTERFACE Table
   --  Original API is Oracle Seeded API : To support serial and lot controlled items this API is modified.
   --****************************************************************************

   PROCEDURE Inventory_Interface (p_line_id         IN            NUMBER
                                 ,x_return_status      OUT NOCOPY VARCHAR2
                                 ,x_result_out         OUT NOCOPY VARCHAR2);

   --***************************************************************************
   -- Procedure Serial_Interface
   -- Purose : API Inserts a record into Serial Numbers interface table.
   -- If item is serical Controlled then Serial Number inforrmation is popluate using this API
   --*******************************************************************************

   PROCEDURE Serial_Interface (serial_rec        IN            serial_type
                              ,x_return_status      OUT NOCOPY VARCHAR2
                              ,x_error_msg          OUT NOCOPY VARCHAR2);

   PROCEDURE LOG_MSG (p_debug_level   IN NUMBER
                     ,p_mod_name      IN VARCHAR2
                     ,p_debug_msg     IN VARCHAR2);
END XXWC_OE_Inv_Iface_PVT;
/

