CREATE OR REPLACE PACKAGE APPS.xxwc_om_be_pkg
AS
   /********************************************************************************************************************************
      $Header XXWC_OM_BE_PKG.PKG $
      Module Name: XXWC_OM_BE_PKG.PKS

      PURPOSE:   This package is used for OM custom business events

      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------------------------------------------------------------
      1.0        09-Jun-2016  Rakesh Patel           TMS#20160511-00208-IT - Improvements to AR Credit Management backend workflow
   *********************************************************************************************************************************/

   FUNCTION Start_AR_CMG_Process(          p_subscription_guid   IN     RAW,
          p_event               IN OUT wf_event_t
       ) RETURN VARCHAR2;
            
   PROCEDURE raise_ar_cmg_process(p_credit_request_id  IN NUMBER,
                                  p_review_type IN VARCHAR2,
                                  p_error_msg  OUT VARCHAR2);   
END xxwc_om_be_pkg;
/