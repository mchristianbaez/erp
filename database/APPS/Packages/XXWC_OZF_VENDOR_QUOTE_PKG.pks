CREATE OR REPLACE PACKAGE XXWC_OZF_VENDOR_QUOTE_PKG AS


   --Define variables for logging debug messages
   G_LEVEL_UNEXPECTED CONSTANT    NUMBER := 6;
   G_LEVEL_ERROR      CONSTANT    NUMBER := 5;
   G_LEVEL_EXCEPTION  CONSTANT    NUMBER := 4;
   G_LEVEL_EVENT      CONSTANT    NUMBER := 3;
   G_LEVEL_PROCEDURE  CONSTANT    NUMBER := 2;
   G_LEVEL_STATEMENT  CONSTANT    NUMBER := 1;
  
   G_CURRENT_RUNTIME_LEVEL          CONSTANT NUMBER := FND_LOG.G_CURRENT_RUNTIME_LEVEL ; 
   G_MODULE_NAME                    CONSTANT VARCHAR2(60) := 'PLSQL.XXWC_OZF_VENDOR_QUOTE_PKG';
   
   
   
   G_DEBUG_FLAG        VARCHAR2(1) := 'Y'; 
   
   
   G_ORGANIZATION_ID       NUMBER ; 
   G_COMMIT_RECORDS_COUNT  CONSTANT  NUMBER := 1000; 
   G_WHOUSE_MODE           VARCHAR2(30); 
   G_RETURN_STATUS         Varchar2(1) ; 
   G_ORGANIZATION_CODE     Varchar2(3); 
   
        
    
    --*********************************************************************************************************
    -- Procedure  MAIN
    -- THis API is called to convert Quotes into SSD Product Lines. 
    --*********************************************************************************************************
   
    Procedure  MAIN (Errbuf                       OUT NOCOPY VARCHAR2,
                     Retcode                      OUT NOCOPY NUMBER,
                     p_OM_Header_ID               IN Number,
                     p_SDR_Req_Header_ID          IN Number ) ;    
                     
     --*********************************************************************************************************
    -- Function  Check_SSD_Modifier_Exists
    -- Satish U: 15-MAR-2012 : THis Procedure checks if Modifier Exists with Supplier Ship And Debit Request Number 
    --  For a Given Inventory Item ID and Sales ORder Line Id and Price List ID 
    --*********************************************************************************************************
   
    
                     
    Function  Check_SSD_Modifier_Exists(p_Inventory_Item_ID  In Number, 
                                        P_Price_List_ID      In Number, 
                                        P_SO_Line_ID         In Number ) Return  Varchar2 ; 
                                        
    --*********************************************************************************************************
    -- Function  Get_Vendor_Item_Cost
    -- Satish U: 15-MAR-2012 : This PRocedure will check get the Vendor Cost of the Item , for  a given Item, SO Line and Organization
    --  For a Given Inventory Item ID and Sales ORder Line Id and Price List ID 
    --*********************************************************************************************************

    Function Get_Vendor_Item_Cost(p_Inventory_Item_ID IN Number , 
                                  P_SO_Line_ID        IN Number , 
                                  P_Organization_ID   In Number ) Return Number ;
                           
    --*********************************************************************************************************
    -- Function  Get_Vendor_Item_Cost
    -- Satish U: 15-MAR-2012 : This PRocedure will check get the Vendor Cost of the Item , for  a given Item, SO Line and Organization
    --  For a Given Inventory Item ID and Sales ORder Line Id and Price List ID 
    --*********************************************************************************************************
                                        
    Function Get_Vendor_Item_Cost02(p_Inventory_Item_ID IN Number , 
                                  P_SO_Line_ID        IN Number , 
                                  P_Organization_ID   In Number ) Return Varchar2 ;
                                  
     --*********************************************************************************************************
    -- Function  get_item_cost
    -- Satish U: 15-MAR-2012 : T-- Following API is initially copied from OZF_QP_QUAL_PVT.Get_Item_Cost 
    -- THis API is modifier to get Item Cost in WC way. 
    --*********************************************************************************************************
   
                                  
    FUNCTION get_item_cost
      (
       p_line_id NUMBER, p_resale_line_tbl ozf_order_price_pvt.resale_line_tbl_type -- OZF_ORDER_PRICE_PVT.G_RESALE_LINE_TBL
      ) RETURN VARCHAR2;
      
      
    -- Procedure  To add  Warehouse to Global Temporary Table 
    -- XXWC_PA_WAREHOUSE_GT                              
    Procedure Add_Warehouse(P_WHSE_CODE In Varchar2 ) ; 
END XXWC_OZF_VENDOR_QUOTE_PKG;
/
show errors; 
commit;
exit;