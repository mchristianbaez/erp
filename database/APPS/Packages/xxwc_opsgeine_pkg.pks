CREATE OR REPLACE PACKAGE APPS.xxwc_opsgeine_pkg AS
  /*************************************************************************
    $Header xxwc_opsgeine_pkg $
    Module Name: xxwc_opsgeine_pkg.pkb
  
    PURPOSE:   integrate with grid to send emails to right groups
  
    REVISIONS:
    Ver        Date        Author               Description
    ---------  ----------  ---------------      -------------------------
    1.0        03/16/2018  Nancy Pahwa          Initial Version TMS#20180319-00169
    1.1        05/10/2018  Pattabhi Avula       TMS#20181003-00001  - Create a UC4 Job
  **************************************************************************/

  procedure XXWC_OPSGEINE_PROC(p_request_id IN NUMBER);
  -- PROCEDURE XXWC_BSA_GRID_CONCURRENT; -- Ver#1.1
  PROCEDURE XXWC_BSA_GRID_CONCURRENT(ERRBUF OUT VARCHAR2, RETCODE OUT VARCHAR2); -- Ver#1.1
  PROCEDURE XXWC_BSA_CONCURRENT_WARNING;
END;
/