CREATE OR REPLACE PACKAGE xxwcar_misc_pkg IS

  /*******************************************************************************
  * Package:   XXWCAR_MISC_PKG
  * Description: 
  *              
  *              
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     2/22/2013    Kathy Poling    Initial creation 
  1.1     12/12/2013    Kathy Poling   Initial creation of the procedure
                                       uc4_submit_receipt_writeoff
                                       SR 222712 (Task ID: 20130908-00094)
  1.2     10/06/2014   Pattabhi        Changes made for TMS#  20141001-00058 
  ********************************************************************************/

  PROCEDURE uc4_autoadjustment(errbuf                OUT VARCHAR2
                              ,retcode               OUT NUMBER
                              ,p_user_name           IN VARCHAR2
                              ,p_responsibility_name IN VARCHAR2
                              ,p_amt_low             IN VARCHAR2
                              ,p_amt_high            IN VARCHAR2
                              ,p_pct_low             IN VARCHAR2
                              ,p_pct_hi              IN VARCHAR2
                              ,p_adj_type            IN VARCHAR2
                              ,p_act_code            IN VARCHAR2
                              ,p_reason_code         IN VARCHAR2
                              ,p_gl_date             IN DATE
                              ,p_currency            IN VARCHAR2
                              ,p_due_low             IN VARCHAR2
                              ,p_due_high            IN VARCHAR2
                              ,p_inv_type_low        IN VARCHAR2
                              ,p_inv_type_high       IN VARCHAR2
                              ,p_cust_name_low       IN VARCHAR2
                              ,p_cust_name_high      IN VARCHAR2
                              ,p_cust_num_low        IN VARCHAR2
                              ,p_cust_num_high       IN VARCHAR2
                              ,p_option_code         IN VARCHAR2);

  PROCEDURE writeoff_disc(errbuf                OUT VARCHAR2
                         ,retcode               OUT NUMBER
                         ,p_user_name           IN VARCHAR2
                         ,p_responsibility_name IN VARCHAR2
                         ,p_org_nm              IN VARCHAR2
                         ,p_activity            IN VARCHAR2
                         ,p_reason              IN VARCHAR2);

  PROCEDURE uc4_submit_writeoff(errbuf                OUT VARCHAR2
                               ,retcode               OUT NUMBER
                               ,p_user_name           IN VARCHAR2
                               ,p_responsibility_name IN VARCHAR2
                               ,p_org_nm              IN VARCHAR2
                               ,p_activity            IN VARCHAR2
                               ,p_reason              IN VARCHAR2
                               ,p_application         IN VARCHAR2
                               ,p_short_nm            IN VARCHAR2);

  --Version 1.1                             
  PROCEDURE uc4_submit_receipt_writeoff(errbuf                 OUT VARCHAR2
                                       ,retcode                OUT NUMBER
                                       ,p_user_name            IN VARCHAR2
                                       ,p_responsibility_name  IN VARCHAR2
                                       ,p_currency_code        IN VARCHAR2
                                       ,p_unapp_amount         IN VARCHAR2
                                       ,p_unapp_amount_percent IN VARCHAR2
                                       ,p_receipt_date_from    IN VARCHAR2
                                       ,p_receipt_date_to      IN VARCHAR2
                                       ,p_gl_date_from         IN VARCHAR2
                                       ,p_gl_date_to           IN VARCHAR2
                                       ,p_payment_mthd_id      IN VARCHAR2
                                       ,p_customer_number      IN VARCHAR2
                                       ,p_receipt_number       IN VARCHAR2
                                       ,p_method_id            IN VARCHAR2
                                       ,p_apply_date           IN VARCHAR2
                                       ,p_gl_date              IN VARCHAR2
                                       ,p_comments             IN VARCHAR2);

END xxwcar_misc_pkg;
/
