CREATE OR REPLACE PACKAGE APPS.xxwc_dbms_appl_info_pkg
AS
/***************************************************************************
  $Header xxwc_dbms_appl_info_pkg $
  Module Name: xxwc_dbms_appl_info_pkg.pkb

  PURPOSE:   This package is called by the concurrent programs,
             SQL scripts, reports and forms to set the module and action
             for a given session.

  REVISIONS:
  Ver        Date        Author             Description
  ---------  ----------  ---------------   ---------------------------------
   1.0       08/02/2017  Ashwin Sridhar    Initial Build - TMS#20170116-00178
****************************************************************************/
/*************************************************************************
  Procedure : set_module_action_p

  PURPOSE:   This procedure is used to set the module and action for a 
             given session.
  Parameter:
         IN
             p_module    -- Module Name
             p_action    -- Action Name
************************************************************************/
PROCEDURE set_module_action_p (p_module IN VARCHAR2
                              ,p_action IN VARCHAR2);

END xxwc_dbms_appl_info_pkg;
/