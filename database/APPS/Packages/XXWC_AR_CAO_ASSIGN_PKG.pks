--
-- XXWC_AR_CAO_ASSIGN_PKG  (Package) 
--
CREATE OR REPLACE PACKAGE APPS.xxwc_ar_cao_assign_pkg
   AUTHID CURRENT_USER
AS
   /* $Header: ARCAOAS.pls 120.0.12010000.2 2008/11/21 15:36:16 rmanikan noship $*/
   /*
   this package original seeded name is ar_cao_assign_pkg
   it was created to fix the bug with the Roles vs. direct Responsibilities in the cash application work queue:
   LOV for Defualt User Name and Result User Name contains no values.
     Select statements in the
     function ar_cao_assign_pkg.check_access changed to use to use the FND_USER_RESP_GROUPS_ALL
     view instead FND_USER_RESP_GROUPS_DIRECT to resolve the issue.
     xxwc_ar_cao_assign_pkg.check_access used  Value Set in Apps AR_CASH_APPLN_OWNER in
     where clause:
     trunc(sysdate) between nvl(start_date, trunc(sysdate)) and nvl(end_date, trunc(sysdate)) and xxwc_ar_cao_assign_pkg.check_access(f.USER_ID, 'Y') = 1
     order by f.USER_NAME
     changed by Rasikha Galimova -rasikha.galimova@hdsupply
   */
   PROCEDURE assign_work_items (errbuf                   OUT NOCOPY VARCHAR2
                               ,retcode                  OUT NOCOPY NUMBER
                               ,p_operating_unit      IN            NUMBER
                               ,p_receipt_date_from   IN            VARCHAR2
                               ,p_receipt_date_to     IN            VARCHAR2
                               ,p_cust_prof_class     IN            NUMBER
                               ,p_max_num_workers     IN            NUMBER
                               ,p_worker_no           IN            NUMBER);

   FUNCTION check_access (user_id IN NUMBER, valid_flag IN VARCHAR2)
      RETURN NUMBER;
END;
/

