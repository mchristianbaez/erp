CREATE OR REPLACE PACKAGE APPS.XXWCAR_UPDATE_CUSTOMER_PKG AS
/********************************************************************************

FILE NAME: APPS.XXWCAP_INV_INT_PKG.pkg

PROGRAM TYPE: PL/SQL Package

PURPOSE: API to update Cusotmer Information

HISTORY
===============================================================================
VERSION DATE          AUTHOR(S)       DESCRIPTION
------- -----------   --------------- -----------------------------------------
1.0     01/07/2013    Gopi Damuluri    Initial version.
1.1     05/15/2013    Gopi Damuluri    Uncommented update_cust_profile procedure
                                       ESMS : 203814
1.2     06/11/2013    Gopi Damuluri    Added a new procedure UPDATE_CUST_NOTES
                                       for WebADI loader. TMS:20130418-01738
1.3     11/11/2013    Gopi Damuluri    TMS# 20130808-00965
                                       Automation of Monthly Oracle Risk Update Process 
                                       TMS# 20130228-01478
                                       Update No Notice Customers credit limits from $0 to Null
1.4     01/31/2014 Maharajan Shunmugam  TMS#20131126-00046
1.5     09/17/2014    Pattabhi Avula	TMS#20141001-00029
********************************************************************************/

PROCEDURE update_cust_profile (p_errbuf            OUT      VARCHAR2
                             , p_retcode           OUT      NUMBER);

PROCEDURE update_cust_prof_amt(p_errbuf            OUT      VARCHAR2
                             , p_retcode           OUT      NUMBER);

PROCEDURE update_cust_notes(  p_cust_account_id               IN NUMBER
                            , p_account_number                IN VARCHAR2
                            , p_account_name                  IN VARCHAR2
                            , p_customer_account_status       IN VARCHAR2
                            , p_Total_AR                      IN NUMBER
                            , p_collector_name                IN VARCHAR2
                            , p_Note1                         IN VARCHAR2
                            , p_Note2                         IN VARCHAR2
                            , p_Note3                         IN VARCHAR2
                            , p_Note4                         IN VARCHAR2
                            , p_Note5                         IN VARCHAR2
                            , p_account_customer_source       IN VARCHAR2
                            , p_Account_Credit_Hold           IN VARCHAR2
                            , p_Account_Payment_Terms         IN VARCHAR2
                            , p_Account_Profile_Class_Name    IN VARCHAR2
                            , p_Account_Credit_limit          IN NUMBER
                            , p_PO_Required                   IN VARCHAR2);

-- Version# 1.3 > Start

PROCEDURE update_profile_class(p_cust_account_profile_id     IN NUMBER
                             , p_object_version_num          IN NUMBER
                             , p_profile_class_id            IN NUMBER);


PROCEDURE update_no_notice_cust_amt(p_errbuf                OUT VARCHAR2
                                  , p_retcode               OUT NUMBER
                                  ,p_resp                    IN VARCHAR2
                                  ,P_resp_status             IN VARCHAR2
                                  , p_report_only            IN VARCHAR2
				  , p_collector              IN NUMBER);

PROCEDURE update_cust_classification(p_errbuf               OUT VARCHAR2
                                   , p_retcode              OUT NUMBER);

PROCEDURE get_new_cust_classification(p_raw_score            IN NUMBER
                                    , p_new_classif         OUT VARCHAR2);

PROCEDURE update_cust_risk_info (p_errbuf                   OUT VARCHAR2
                               , p_retcode                  OUT NUMBER);
-- Version# 1.3 < End

END XXWCAR_UPDATE_CUSTOMER_PKG;
