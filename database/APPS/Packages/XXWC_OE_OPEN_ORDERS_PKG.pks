create or replace package APPS.XXWC_OE_OPEN_ORDERS_PKG is

/*************************************************************************
  $Header XXWC_OE_OPEN_ORDERS_PKG $
  Module Name: XXWC_OE_OPEN_ORDERS_PKG.pks

  PURPOSE:   This package is called by the concurrent programs
             to Load data to the custom table XXWC_OE_OPEN_ORDER_LINES

  REVISIONS:
  Ver        Date        Author                     Description
  ---------  ----------  ---------------         -------------------------  
  1.0        15-Mar-2015 Manjula Chellappan      TMS #20151023-00037 - Shipping Extension Modification
                                                     for Counter Order Removal and PUBD
**************************************************************************/
   g_dflt_email   fnd_user.email_address%TYPE
                     := 'HDSOracleDevelopers@hdsupply.com';
                     
  PROCEDURE load_open_order_lines 
    ( errbuf                  OUT VARCHAR2,
      retcode                 OUT VARCHAR2,
      p_organization_id       IN NUMBER,
      p_inventory_item_id     IN NUMBER);

end XXWC_OE_OPEN_ORDERS_PKG;
/
