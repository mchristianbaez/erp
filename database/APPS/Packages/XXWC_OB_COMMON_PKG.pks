--
-- XXWC_OB_COMMON_PKG  (Package) 
--
CREATE OR REPLACE PACKAGE APPS.XXWC_OB_COMMON_PKG
AS
   /********************************************************************************
   FILE NAME: XXWC_OB_COMMON_PKG.pkg

   PROGRAM TYPE: PL/SQL Package

   PURPOSE: Common API for Outbound Interfaces.

   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.0     10/20/2011    Gopi Damuluri    Initial creation of the procedure
   ********************************************************************************/

   PROCEDURE CREATE_FILE (p_errbuf              OUT VARCHAR2
                         ,p_retcode             OUT NUMBER
                         ,p_interface_name   IN     VARCHAR2
                         ,p_view_name        IN     VARCHAR2
                         ,p_directory_path   IN     VARCHAR2
                         ,p_file_name        IN     VARCHAR2
                         ,p_org_name         IN     VARCHAR2);

   PROCEDURE main (p_errbuf                   OUT VARCHAR2
                  ,p_retcode                  OUT NUMBER
                  ,p_interface_name        IN     VARCHAR2
                  ,p_view_name             IN     VARCHAR2
                  ,p_directory_path        IN     VARCHAR2
                  ,p_file_name             IN     VARCHAR2
                  ,p_user_name             IN     VARCHAR2
                  ,p_responsibility_name   IN     VARCHAR2
                  ,p_org_name              IN     VARCHAR2);

   TYPE xxcus_ob_file_rec IS RECORD
   (
      rec_line   VARCHAR2 (4000)
     ,org_id     NUMBER (15)
   );

   TYPE xxcus_ob_file_rec_tbl_type IS TABLE OF xxcus_ob_file_rec
      INDEX BY BINARY_INTEGER;
END xxwc_ob_common_pkg;
/

