CREATE OR REPLACE PACKAGE APPS.xxwc_inv_transit_all_org_pkg AS

  /********************************************************************************
  FILE NAME: XXWC_INV_TRANSIT_ALL_ORG_PKG.pkg
  
  PROGRAM TYPE: PL/SQL Package Body
  
  PURPOSE: To Run the XXWC Inventory Intransit Report for All Organizations
  
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     03/04/2015    Pattabhi Avual  Initial creation 
  ********************************************************************************/


PROCEDURE uc4_submit(  p_errbuf     OUT  VARCHAR2
                      ,p_retcode    OUT  VARCHAR2
                      ,p_resp_name  IN   VARCHAR2
                      ,p_user_name  IN   VARCHAR2
                      ,p_org_name   IN   VARCHAR2);

END xxwc_inv_transit_all_org_pkg;