
  CREATE OR REPLACE PACKAGE "APPS"."XXCUS_OZF_PAYMENTS_PKG" is

-----------------------------------------------------------------------------
-- Copyright 2012 HD Supply  Inc (Orlando, FL) - All rights reserved
--
--
--    NAME:      XXCUS_OZF_PAYMENTS_PKG.pck
--
--    REVISIONS:
--    Ver        Date        Author           Description
--    ---------  ----------  ---------------  -------------------------------
--    1.0        06/06/2012  Nithya Sampath   1. Created this package.
--    Notes:
--
--    Overview: This package contains procedure for creating the receipts
--              through the lockbox program.

------------------------------------------------------------------------------
------------------------------------------------------------------------------

PROCEDURE create_receipts (p_err_buf        OUT VARCHAR2,
					                 p_ret_code       OUT VARCHAR2,
                           p_org_id         IN NUMBER,
                           p_payment_Method IN VARCHAR2
                           );
                           
PROCEDURE load_interface_table(p_org_id         IN NUMBER,
                               p_payment_Method IN VARCHAR2);
                               
PROCEDURE submit_lockbox_pgm(p_transmission_name IN VARCHAR2,
                             p_transmission_id IN NUMBER,
                             p_lockbox_id IN NUMBER,
                             p_format_id  IN NUMBER,
                             p_org_id IN NUMBER                             
                             );
                           



END XXCUS_OZF_PAYMENTS_PKG;
;
