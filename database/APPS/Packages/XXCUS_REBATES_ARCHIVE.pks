CREATE OR REPLACE PACKAGE APPS.XXCUS_REBATES_ARCHIVE
-- ESMS TICKET HISTORY
-- Scope: Oracle Trade Management / Rebates accruals and gl journals extract
-- Used by concurrent jobs HDS Rebates: Archive rebates reporting table data
-- Tables to be archived: XXCUS.XXCUS_OZF_XLA_ACCRUALS_B and XXCUS.XXCUS_OZF_GL_JOURNALS_B
-- Change History:
-- Ticket#               Version  Date          Comment
-- ====================  =======  ============  ============================================
-- TMS 20160316-00196    1.0      04/15/2016    Created.
-- TMS 20160519-00078   1.1      Parameter name change and other fixes [ ESMS 327004 ]
--
as  
  --
  procedure tm_accruals
  (
    retcode                 out number
   ,errbuf                   out varchar2
   ,p_cal_year           in    number
  );
  --  
  procedure tm_journals
  (
    retcode                 out number
   ,errbuf                   out varchar2
   ,p_cal_year           in    number
  );
  -- Begin Ver 1.1
 procedure setup_gl_archive_partitions
  (
    retcode                 out number
   ,errbuf                   out varchar2
   ,p_cal_year           in    number
  );  
 procedure setup_accr_archive_partitions
  (
    retcode                 out number
   ,errbuf                   out varchar2
   ,p_cal_year           in    number
  );   
  -- End Ver 1.1
  --    
end XXCUS_REBATES_ARCHIVE;
/