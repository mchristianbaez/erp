create or replace 
PACKAGE      XXWC_COMP_DEMAND_HIST_PKG AS
/******************************************************************************
   NAME:       XXWC_COMP_DEMAND_HIST_PKG
   PURPOSE:
/* 
 -- Date: 24-APR-2013 
 -- Author: Dheeresh Chintala
 -- Scope: This package is to run Compile Demand History for all (multiple) inventory organizations.
 -- TMS ticket - 20121217-00830 
 -- Update History:
 --
 -- Parameters: None
 
   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        4/23/2013             1. Created this package.
******************************************************************************/

  PROCEDURE MAIN(
    retcode                 OUT NUMBER
   ,errbuf                  OUT VARCHAR2
   ,P_REGION                IN  VARCHAR2);

END XXWC_COMP_DEMAND_HIST_PKG; 