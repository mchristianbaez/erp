create or replace PACKAGE      APPS.XXWC_MWA_ROUTINES_PKG AS

   /*****************************************************************************************************************************************
   *   $Header XXWC_MWA_ROUTINES_PKG $                                                                                                      *
   *   Module Name: XXWC_MWA_ROUTINES_PKG                                                                                                   *
   *                                                                                                                                        *
   *   PURPOSE:   This package is used by the extensions in MSCA                                                                            *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        16-OCT-2014  Lee Spitzer               TMS Ticket 20141001-00198                                                          *
   *                                                     WC Canada: Enhancement: RFID / Mobile Supply Chain RFID / Mobile Supply Chain      *
   *   1.1        20-JUN-2015  Lee Spitzer               TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions                   *
   *              20-JUN-2015  Lee Spitzer               TMS Ticket Number 20150622-00004 RF - UPC Update                                   *
   *                                                                                                                                        *
   *   1.2        08-JUL-2015  Lee Spitzer               TMS Ticket 20150714-00142 RF - Implement fixes to include with release 2           *
   *****************************************************************************************************************************************/

  g_package                   VARCHAR2(30) := 'XXWC_MWA_ROUTINES_PKG';
  g_call_from                 VARCHAR2(175);
  g_call_point                VARCHAR2(175);
  g_distro_list               VARCHAR2 (80) := 'hdsoracledevelopers@hdsupply.com';
  g_module                    VARCHAR2 (80) := 'inv';
  g_sequence                  NUMBER;
  g_debug                     VARCHAR2(1) := nvl(FND_PROFILE.VALUE('AFLOG_ENABLED'),'N'); --Fnd Debug Log = Yes / No
  g_log_level                 NUMBER := nvl(FND_PROFILE.VALUE('AFLOG_LEVEL'),6); --Fnd Debug Log Level
  g_sqlerrm                   VARCHAR2(1000);
  g_sqlcode                   NUMBER;   
  g_message                   VARCHAR2(2000);
  g_exception                 EXCEPTION; 
  g_language                  VARCHAR2(10) := userenv('LANG');
   TYPE t_genref IS REF CURSOR;
  

   /*****************************************************************************************************************************************
   *   FUNCTION GET_OPEN_RCV_QTY                                                                                                            *
   *                                                                                                                                        *
   *   PURPOSE:   This function is used to get the default open quantity for receiving mobile forms                                         *
   *                                                                                                                                        *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        16-OCT-2014  Lee Spitzer               TMS Ticket 20141001-00198                                                          *
   *                                                     WC Canada: Enhancement: RFID / Mobile Supply Chain RFID / Mobile Supply Chain      *
   *****************************************************************************************************************************************/

    FUNCTION GET_OPEN_RCV_QTY  ( P_ORGANIZATION_ID  IN VARCHAR2        --2
                               , P_TRANSACTION_TYPE IN VARCHAR2        --3
                               , P_PO_HEADER_ID IN VARCHAR2            --4
                               , P_PO_LINE_ID IN VARCHAR2              --5
                               , P_INVENTORY_ITEM_ID IN VARCHAR2       --6
                               , P_UOM_CODE IN VARCHAR2                --7
                               , P_REQUISITION_HEADER_ID IN VARCHAR2   --8
                               , P_SHIPMENT_HEADER_ID IN VARCHAR2      --9
                               , P_ORDER_HEADER_ID IN VARCHAR2)         --0
                               
              RETURN VARCHAR2; --1
                               
   /*****************************************************************************************************************************************
   *   PROCEDURE GET_LOC_LOV_RCV                                                                                                            *
   *                                                                                                                                        *
   *   PURPOSE:   This function is used to get the default open quantity for receiving mobile forms                                         *
   *                                                                                                                                        *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        22-OCT-2014  Lee Spitzer               TMS Ticket 20141001-00198                                                          *
   *                                                     WC Canada: Enhancement: RFID / Mobile Supply Chain RFID / Mobile Supply Chain      *
   *****************************************************************************************************************************************/
  
    PROCEDURE GET_LOC_LOV_RCV ( x_locators               OUT    NOCOPY t_genref --0
                              , p_organization_id        IN     NUMBER    --1
                              , p_subinventory_code      IN     VARCHAR2  --2
                              , p_restrict_locators_code IN     NUMBER    --3
                              , p_inventory_item_id      IN     NUMBER    --4
                              , p_concatenated_segments  IN     VARCHAR2  --5
                              , p_transaction_type_id    IN     NUMBER    --6
                              , p_wms_installed          IN     VARCHAR2  --7
                              , p_project_id             IN     NUMBER    --8
                              , p_task_id                IN     NUMBER    --9
                              , p_locator_alias          IN     VARCHAR2); --10
    
    /*****************************************************************************************************************************************
   *   FUNCTION GET_ONHAND                                                                                                                 *
   *                                                                                                                                        *
   *   PURPOSE:   This function is used to get return the current on-hand, available to transact and available to reserve quantity          *
   *                                                                                                                                        *
   *    Return :  NUMBER
   *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        22-OCT-2014  Lee Spitzer               TMS Ticket 20141001-00198                                                          *
   *                                                     WC Canada: Enhancement: RFID / Mobile Supply Chain RFID / Mobile Supply Chain      *
   *****************************************************************************************************************************************/

                          
    FUNCTION GET_ONHAND ( p_inventory_item_id   IN   NUMBER
                        , p_organization_id     IN   NUMBER
                        , p_subinventory        IN   VARCHAR2
                        , p_locator_id          IN   NUMBER
                        , p_return_type         IN   VARCHAR2
                        , p_lot_number          IN   VARCHAR2 DEFAULT NULL
                        )
              RETURN NUMBER;


    /*****************************************************************************************************************************************
     *   FUNCTION GET_DEFAULT_LOCATOR_ID                                                                                                      *
     *                                                                                                                                        *
     *   PURPOSE:   This function is used to get return the primary bin assignment of an item aka primary bin                                 *
     *  
     *    Parameters: p_organization_id  IN NUMBER - organization_id
     *                p_inventory_item_id IN NUMBER - inventory_item_id
     *                p_subinventory_code IN VARCHAR2 - subinventory_code --if passed as null, will get subinventory from mtl_item_sub_defaults
     *                p_default_type  IN  NUMBER - 1 = Shipping, 2 = Receiving, 3 = Move Order Requisition, lookup type from MFG_LOOKUPS of 'MTL_DEFAULT_LOCATORS'
     *                
     *    Return :  NUMBER - locator_id from mtl_item_loc_defaults                                                                                                                   *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        22-OCT-2014  Lee Spitzer               TMS Ticket 20141001-00198                                                          *
     *                                                     WC Canada: Enhancement: RFID / Mobile Supply Chain RFID / Mobile Supply Chain      *
     *****************************************************************************************************************************************/

        FUNCTION GET_DEFAULT_LOCATOR_ID ( p_organization_id    IN NUMBER
                                        , p_inventory_item_id  IN NUMBER
                                        , p_subinventory_code  IN VARCHAR2
                                        , p_default_type       IN NUMBER)
                 RETURN NUMBER;
          
      
     /*****************************************************************************************************************************************
     *   FUNCTION GET_LOCATOR_NAME                                                                                                            *
     *                                                                                                                                        *
     *   PURPOSE:   This function is used to return the locator concatenated segments                                                         *
     *                                                                                                                                        *
     *    Parameters: p_organization_id  IN NUMBER - organization_id                                                                          *
     *                p_locator_id IN NUMBER - locator_id                                                                                     *
     *                                                                                                                                        *
     *    Return :  VARCHAR2 - concateated_segments from wms_item_locations_kfv                                                               *                                                   *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        22-OCT-2014  Lee Spitzer               TMS Ticket 20141001-00198                                                          *
     *                                                     WC Canada: Enhancement: RFID / Mobile Supply Chain RFID / Mobile Supply Chain      *
     *****************************************************************************************************************************************/
    
        FUNCTION GET_LOCATOR_NAME  ( p_organization_id IN NUMBER
                                   , p_locator_id IN NUMBER)
                 RETURN VARCHAR2;
      
     /*****************************************************************************************************************************************
     *   FUNCTION GET_ITEM_SUB_MAX_QTY                                                                                                        *
     *                                                                                                                                        *
     *   PURPOSE:   This function is used to return the max qty from subinventory of the item subinventories                                  *
     *                                                                                                                                        *
     *    Parameters: p_organization_id  IN NUMBER - organization_id                                                                          *
     *                p_inventory_item_id IN NUMBER -- inventory_item_id                                                                      *
     *                p_subinventory_code IN VARCHAR2 - subinventory_code                                                                     *
     *                                                                                                                                        *
     *    Return :  NUMBER - MAX_MINMAX_QTY from MTL_ITEM_SUB_INVENTORIES                                                                     *                                                   *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        22-OCT-2014  Lee Spitzer               TMS Ticket 20141001-00198                                                          *
     *                                                     WC Canada: Enhancement: RFID / Mobile Supply Chain RFID / Mobile Supply Chain      *
     *****************************************************************************************************************************************/

        FUNCTION GET_ITEM_SUB_MAX_QTY   ( p_organization_id    IN NUMBER
                                        , p_inventory_item_id  IN NUMBER
                                        , p_subinventory_code  IN VARCHAR2)
                 RETURN NUMBER;
              

     /*****************************************************************************************************************************************
     *   PROCEDURE PROCESS_ITEM_DEFAULT_LOCATOR                                                                                               *
     *                                                                                                                                        *
     *   PURPOSE:   This procedure is used to insert or update MTL_ITEM_SUB_DEFAULTS AND MTL_ITEM_LOC_DEFAULTS                                *
     *                                                                                                                                        *
     *    Parameters: p_organization_id  IN NUMBER - organization_id                                                                          *
     *                p_inventory_item_id IN NUMBER -- inventory_item_id                                                                      *
     *                p_subinventory_code IN VARCHAR2 - subinventory_code                                                                     *
     *                p_locator_id IN NUMBER --LOCATOR_ID                                                                                     *
     *                p_default_type  IN  NUMBER - 1 = Shipping, 2 = Receiving, 3 = Move Order Requisition, lookup type from MFG_LOOKUPS of 'MTL_DEFAULT_LOCATORS'
     *                                                                                                                                        *
     *    Out :  X_RETURN = 0 = Success, 1 = Error                                                                                            *
     *           X_MESSAGE = Return message                                                                                                   *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        22-OCT-2014  Lee Spitzer               TMS Ticket 20141001-00198                                                          *
     *                                                     WC Canada: Enhancement: RFID / Mobile Supply Chain RFID / Mobile Supply Chain      *
     *****************************************************************************************************************************************/

        PROCEDURE PROCESS_ITEM_DEFAULT_LOCATOR   ( p_organization_id    IN NUMBER
                                                 , p_inventory_item_id  IN NUMBER
                                                 , p_subinventory_code  IN VARCHAR2
                                                 , p_locator_id IN NUMBER
                                                 , p_default_type IN NUMBER
                                                 , x_return  OUT NUMBER
                                                 , x_message OUT VARCHAR2);
    
     /*****************************************************************************************************************************************
     *   PROCEDURE PROCESS_ITEM_SUB_MAX                                                                                                       *
     *                                                                                                                                        *
     *   PURPOSE:   This procedure is used to insert or update MTL_ITEM_SUB_INVENTORIES                                                       *
     *                                                                                                                                        *
     *    Parameters: p_organization_id  IN NUMBER - organization_id                                                                          *
     *                p_inventory_item_id IN NUMBER -- inventory_item_id                                                                      *
     *                p_subinventory_code IN VARCHAR2 - subinventory_code                                                                     *
     *                p_max_qty  IN  NUMBER                                                                                                   *
     *                                                                                                                                        *
     *    Out :  X_RETURN = 0 = Success, 1 = Error                                                                                            *
     *           X_MESSAGE = Return message                                                                                                   *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        22-OCT-2014  Lee Spitzer               TMS Ticket 20141001-00198                                                          *
     *                                                     WC Canada: Enhancement: RFID / Mobile Supply Chain RFID / Mobile Supply Chain      *
     *****************************************************************************************************************************************/

        PROCEDURE PROCESS_ITEM_SUB_MAX           ( p_organization_id    IN NUMBER
                                                 , p_inventory_item_id  IN NUMBER
                                                 , p_subinventory_code  IN VARCHAR2
                                                 , p_max_qty IN NUMBER
                                                 , x_return  OUT NUMBER
                                                 , x_message OUT VARCHAR2);
                                                 
     /*****************************************************************************************************************************************
     *   PROCEDURE PROCESS_ITEM_LOCATOR_TIE                                                                                                    *
     *                                                                                                                                        *
     *   PURPOSE:   This procedure is used to update or insert records into the MTL_SECONDARY_LOCATORS                                        *
     *                                                                                                                                        *
     *    Parameters: p_organization_id  IN NUMBER - organization_id                                                                          *
     *                p_inventory_item_id IN NUMBER -- inventory_item_id                                                                      *
     *                p_subinventory_code IN VARCHAR2 - subinventory_code                                                                     *
     *                p_locator_id IN NUMBER --LOCATOR_ID                                                                                     *
     *                                                                                                                                        *
     *    Out :  X_RETURN = 0 = Success, 1 = Error                                                                                            *
     *           X_MESSAGE = Return message                                                                                                   *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        22-OCT-2014  Lee Spitzer               TMS Ticket 20141001-00198                                                          *
     *                                                     WC Canada: Enhancement: RFID / Mobile Supply Chain RFID / Mobile Supply Chain      *
     *   1.1        08-JUL-2015  Lee Spitzer               TMS Ticket 20150714-00142 RF - Implement fixes to include with release 2           *
     *****************************************************************************************************************************************/

        PROCEDURE PROCESS_ITEM_LOCATOR_TIE       ( p_organization_id    IN NUMBER
                                                 , p_inventory_item_id  IN NUMBER
                                                 , p_subinventory_code  IN VARCHAR2
                                                 , p_locator_id IN NUMBER
                                                 , x_return  OUT NUMBER
                                                 , x_message OUT VARCHAR2);
     /*****************************************************************************************************************************************
     *   PROCEDURE PROCESS_ITEM_LOCATOR_TIE_CCR                                                                                               *
     *                                                                                                                                        *
     *   PURPOSE:   This procedure is the concurrent program to called by the trigger XXWC_MMT_ITEM_LOC_TIE_AIU used to update or insert records into the MTL_SECONDARY_LOCATORS                                        *
     *                                                                                                                                        *
     *    Parameters: p_organization_id  IN NUMBER - organization_id                                                                          *
     *                p_inventory_item_id IN NUMBER -- inventory_item_id                                                                      *
     *                p_subinventory_code IN VARCHAR2 - subinventory_code                                                                     *
     *                p_locator_id IN NUMBER --LOCATOR_ID                                                                                     *
     *                                                                                                                                        *
     *    Out :  errbuf = standard errbug for concurrent programs                                                                             *
     *           retcode = standard retcode for concurrent programs                                                                           *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        22-OCT-2014  Lee Spitzer               TMS Ticket 20141001-00198                                                          *
     *                                                     WC Canada: Enhancement: RFID / Mobile Supply Chain RFID / Mobile Supply Chain      *
     *****************************************************************************************************************************************/
                                                 
        PROCEDURE PROCESS_ITEM_LOCATOR_TIE_CCR
                                                 ( errbuf               OUT VARCHAR2
                                                 , retcode              OUT VARCHAR2
                                                 , p_organization_id    IN NUMBER
                                                 , p_inventory_item_id  IN NUMBER
                                                 , p_subinventory_code  IN VARCHAR2
                                                 , p_locator_id IN NUMBER);
                                                 


     /*****************************************************************************************************************************************
     *   PROCEDURE material_label                                                                                                             *
     *                                                                                                                                        *
     *   PURPOSE:   This procedure used to print material labels
     *                                                                                                                                        *
     *    Parameters: p_organization_id  IN NUMBER - organization_id                                                                          *
     *                p_inventory_item_id IN NUMBER -- inventory_item_id                                                                      *
     *                p_lot_number IN VARCHAR2 - subinventory_code                                                                            *
     *                p_quantity IN NUMBER                                                                                                    *
     *                p_copies  IN NUMBER                                                                                                     *
     *                p_user_id IN NUMBER --LOCATOR_ID                                                                                     *
     *                                                                                                                                        *
     *    Out :  errbuf = standard errbug for concurrent programs                                                                             *
     *           retcode = standard retcode for concurrent programs                                                                           *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        22-OCT-2014  Lee Spitzer               TMS Ticket 20141001-00198                                                          *
     *                                                     WC Canada: Enhancement: RFID / Mobile Supply Chain RFID / Mobile Supply Chain      *
     *****************************************************************************************************************************************/
       
        PROCEDURE material_label
              (   p_organization_id                 IN          VARCHAR2   DEFAULT NULL
              ,   p_inventory_item_id               IN          VARCHAR2   DEFAULT NULL
              ,   p_lot_number                      IN          VARCHAR2   DEFAULT NULL
              ,   p_quantity                        IN          VARCHAR2   DEFAULT NULL
              ,   p_printer_name                    IN          VARCHAR2   DEFAULT NULL
              ,   p_copies                          IN          VARCHAR2   DEFAULT 0
              ,   p_user_id                         IN          VARCHAR2   DEFAULT NULL
              ,   x_return                          OUT         NUMBER
              ,   x_message                         OUT         VARCHAR2
              );
              
     /*****************************************************************************************************************************************
     *  FUNCTION get_default_label_printer                                                                                                             *
     *                                                                                                                                        *
     *   PURPOSE:   This procedure used get default label printer profile
     *                                                                                                                                        *
     *    Parameters: None                                                                              *
     *    Return : XXWC_LABEL_PRINTER profile                                                                                                                                        *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        22-OCT-2014  Lee Spitzer               TMS Ticket 20141001-00198                                                          *
     *                                                     WC Canada: Enhancement: RFID / Mobile Supply Chain RFID / Mobile Supply Chain      *
     *****************************************************************************************************************************************/
              
    FUNCTION get_default_label_printer 
        RETURN VARCHAR2;
   

     /*****************************************************************************************************************************************
     *  PROCEDURE GET_RTV_LOV                                                                                                                  *
     *                                                                                                                                        *
     *   PURPOSE:   This procedure used for list of values for internal rtv on the mobile device, C or H                                      *
     *                                                                                                                                        *
     *    Parameters: None                                                                                                                    *
     *    Return : XXWC_LABEL_PRINTER profile                                                                                                 *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        22-OCT-2014  Lee Spitzer               TMS Ticket 20141001-00198                                                          *
     *                                                     WC Canada: Enhancement: RFID / Mobile Supply Chain RFID / Mobile Supply Chain      *
     *****************************************************************************************************************************************/


    PROCEDURE GET_RTV_LOV ( x_flexvalues             OUT    NOCOPY t_genref --0
                          , p_flex_value             IN     VARCHAR2
                         );
   
     /*****************************************************************************************************************************************
     *  PROCEDURE PROCESS_UPDATE_RTV_DFF                                                                                                      *
     *                                                                                                                                        *
     *   PURPOSE:   This procedure used to update the shipment_line_id list of values for internal rtv on the mobile device, C or H                                      *
     *                                                                                                                                        *
     *    Parameters: None                                                                                                                    *
     *    Return : XXWC_LABEL_PRINTER profile                                                                                                 *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        22-OCT-2014  Lee Spitzer               TMS Ticket 20141001-00198                                                          *
     *                                                     WC Canada: Enhancement: RFID / Mobile Supply Chain RFID / Mobile Supply Chain      *
     *****************************************************************************************************************************************/

    PROCEDURE PROCESS_UPDATE_RTV_DFF ( p_shipment_header_id    IN VARCHAR2
                                     , p_inventory_item_id      IN VARCHAR2
                                     , p_organization_id       IN VARCHAR2
                                     , p_value                 IN VARCHAR2
                                     , p_user_id               IN VARCHAR2
                                     , x_return                OUT NUMBER
                                     , x_message               OUT VARCHAR2);
                                     
                                     
    
     /*****************************************************************************************************************************************
     *  PROCEDURE PROCESS_UBD_CCR                                                                                                             *
     *                                                                                                                                        *
     *   PURPOSE:   This procedure used to submit the XXWC UBD Label Report from a RF Device                                                  *
     *                                                                                                                                        *
     *    Parameters: p_date    IN VARCHAR2 --Use by date                                                                                     *                                        *
     *                p_label_copies    IN VARCHAR2 --Number of Copies                                                                        *
     *                p_status_flag     IN VARCHAR2 --F or S -- parameters to determine label size                                            *
     *    Out : x_return -- 0 = Success, 1 = Warning, 2 = Error                                                                               *
     *          x_message --Message out                                                                                                       *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        06-FEB-201  Lee Spitzer               TMS Ticket 20141001-00198                                                           *
     *                                                     WC Canada: Enhancement: RFID / Mobile Supply Chain RFID / Mobile Supply Chain      *
     *****************************************************************************************************************************************/

     PROCEDURE PROCESS_UBD_CCR
                                                 ( p_date               IN  VARCHAR2
                                                 , p_label_copies       IN  VARCHAR2
                                                 , p_status_flag        IN VARCHAR2
                                                 , x_return             OUT NUMBER
                                                 , x_message            OUT VARCHAR2
                                                 );
      
     /*****************************************************************************************************************************************
     *  FUNCTION CHECK_ITEM_IS_TS                                                                                                           *
     *                                                                                                                                        *
     *   PURPOSE:   This function is to check if the item is assigned to the time sensitive category and if yes then return Y, otherwise N    *
     *                                                                                                                                        *
     *    Parameters: p_organization_id IN  VARCHAR2                                                                                          *
     *                p_inventory_item_id IN VARCHAR2                                                                                         *
     *    Return : Y or N                                                                                                                     *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        22-OCT-2014  Lee Spitzer               TMS Ticket 20141001-00198                                                          *
     *                                                     WC Canada: Enhancement: RFID / Mobile Supply Chain RFID / Mobile Supply Chain      *
     *   1.1        01-JUN-2015  Lee Spitzer               TMS Ticket 20150604-00154 Bundle 1                                                                     *
     *                                                      Change UBD label print logic to be based on shelf life days                       *
     *                                                       ( print if shelf life days <1000 and >0)                                         *
     *****************************************************************************************************************************************/


      FUNCTION CHECK_ITEM_IS_TS ( p_organization_id     IN VARCHAR2
                                , p_item                IN VARCHAR2)
              RETURN VARCHAR2;
      
      
     /*****************************************************************************************************************************************
     *  FUNCTION DEFAULT_ITEM_UBD                                                                                                             *
     *                                                                                                                                        *
     *   PURPOSE:   This function is to calculate the UBD is assigned to the time sensitive category and if yes then return Y, otherwise N    *
     *                                                                                                                                        *
     *    Parameters: p_organization_id IN  VARCHAR2                                                                                          *
     *                p_inventory_item_id IN VARCHAR2                                                                                         *
     *    Return : Y or N                                                                                                                     *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        22-OCT-2014  Lee Spitzer               TMS Ticket 20141001-00198                                                          *
     *                                                     WC Canada: Enhancement: RFID / Mobile Supply Chain RFID / Mobile Supply Chain      *
     *****************************************************************************************************************************************/


      FUNCTION DEFAULT_ITEM_UBD ( p_organization_id     IN VARCHAR2
                                , p_item                IN VARCHAR2)
              RETURN VARCHAR2;
      
 
     /*****************************************************************************************************************************************
     *  PROCEDURE GET_TS_LABEL_LOV                                                                                                            *
     *                                                                                                                                        *
     *   PURPOSE:   This procedure used for list of values for time sensitive label list of valus on the mobile device, C or H                *
     *                                                                                                                                        *
     *    Parameters: None                                                                                                                    *
     *    Return : x_flexvalus                                                                                                                *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        06-FEB-2015  Lee Spitzer               TMS Ticket 20141001-00198                                                          *
     *                                                     WC Canada: Enhancement: RFID / Mobile Supply Chain RFID / Mobile Supply Chain      *
     *****************************************************************************************************************************************/


    PROCEDURE GET_TS_LABEL_LOV ( x_flexvalues             OUT    NOCOPY t_genref --0
                               , p_flex_value             IN     VARCHAR2
                               );
                               
                               
    

     /*****************************************************************************************************************************************
     *  FUNCTION WC_BRANCH_LOCATOR_CONTROL                                                                                                    *
     *                                                                                                                                        *
     *   PURPOSE:   This function is to return if the subinventory has locators assigned to it                                                *
     *                                                                                                                                        *
     *    Parameters: p_organization_id IN  NUMBER                                                                                            *
     *                p_subinventory_code IN VARCHAR2                                                                                         *
     *    Return : Count of locators tied to subinventory for the organization                                                                *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        09-Feb-2014  Lee Spitzer               TMS Ticket 20141001-00198                                                          *
     *                                                     WC Canada: Enhancement: RFID / Mobile Supply Chain RFID / Mobile Supply Chain      *
     *****************************************************************************************************************************************/


      FUNCTION WC_BRANCH_LOCATOR_CONTROL ( p_organization_id      IN NUMBER
                                         , p_subinventory_code   IN VARCHAR2)
              RETURN NUMBER;



     /*****************************************************************************************************************************************
     *   PROCEDURE DELETE_ITEM_LOCATOR_TIE                                                                                                    *
     *                                                                                                                                        *
     *   PURPOSE:   This procedure is used to remove records into the MTL_SECONDARY_LOCATORS                                                  *
     *                                                                                                                                        *
     *    Parameters: p_organization_id  IN NUMBER - organization_id                                                                          *
     *                p_inventory_item_id IN NUMBER -- inventory_item_id                                                                      *
     *                p_subinventory_code IN VARCHAR2 - subinventory_code                                                                     *
     *                p_locator_id IN NUMBER --LOCATOR_ID                                                                                     *
     *                                                                                                                                        *
     *    Out :  X_RETURN = 0 = Success, 1 = Error                                                                                            *
     *           X_MESSAGE = Return message                                                                                                   *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        22-OCT-2014  Lee Spitzer               TMS Ticket 20141001-00198                                                          *
     *                                                     WC Canada: Enhancement: RFID / Mobile Supply Chain RFID / Mobile Supply Chain      *
     *********y********************************************************************************************************************************/

        PROCEDURE DELETE_ITEM_LOCATOR_TIE        ( p_organization_id    IN NUMBER
                                                 , p_inventory_item_id  IN NUMBER
                                                 , p_subinventory_code  IN VARCHAR2
                                                 , p_locator_id IN NUMBER
                                                 , x_return  OUT NUMBER
                                                 , x_message OUT VARCHAR2);

      
      
     /*****************************************************************************************************************************************
     *  FUNCTION WC_LOCATOR                                                                                                                   *
     *                                                                                                                                        *
     *   PURPOSE:   This function is to return if the locator exists                                                                          *
     *                                                                                                                                        *
     *    Parameters: p_organization_id IN  VARCHAR2                                                                                          *
     *                p_subinventory_code IN VARCHAR2                                                                                         *
     *    Return : Count of locators tied to subinventory for the organization                                                                *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        09-Feb-2014  Lee Spitzer               TMS Ticket 20141001-00198                                                          *
     *                                                     WC Canada: Enhancement: RFID / Mobile Supply Chain RFID / Mobile Supply Chain      *
     *****************************************************************************************************************************************/


      FUNCTION WC_LOCATOR ( p_locator              IN VARCHAR2)
              RETURN VARCHAR2;
              
     
     /*****************************************************************************************************************************************
     *  FUNCTION WC_LOCATOR                                                                                                                   *
     *                                                                                                                                        *
     *   PURPOSE:   This function is to return if the locator exists                                                                          *
     *                                                                                                                                        *
     *    Parameters: p_organization_id IN  VARCHAR2                                                                                          *
     *                p_subinventory_code IN VARCHAR2                                                                                         *
     *    Return : Count of locators tied to subinventory for the organization                                                                *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        09-Feb-2014  Lee Spitzer               TMS Ticket 20141001-00198                                                          *
     *                                                     WC Canada: Enhancement: RFID / Mobile Supply Chain RFID / Mobile Supply Chain      *
     *****************************************************************************************************************************************/


      FUNCTION WC_LOCATOR_PREFIX ( p_locator              IN VARCHAR2)
              RETURN VARCHAR2;
              
     /*****************************************************************************************************************************************
     *  PROCEDURE CREATE_WC_LOCATOR                                                                                                           *
     *                                                                                                                                        *
     *   PURPOSE:   This function is to return if the locator exists                                                                          *
     *                                                                                                                                        *
     *    Parameters: p_organization_id IN  VARCHAR2                                                                                          *
     *                p_subinventory_code IN VARCHAR2                                                                                         *
     *    Return : Count of locators tied to subinventory for the organization                                                                *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        09-Feb-2014  Lee Spitzer               TMS Ticket 20141001-00198                                                          *
     *                                                     WC Canada: Enhancement: RFID / Mobile Supply Chain RFID / Mobile Supply Chain      *
     *****************************************************************************************************************************************/


      PROCEDURE CREATE_WC_LOCATOR ( p_organization_id IN NUMBER
                                  , p_subinventory_code IN VARCHAR2
                                  , p_locator IN VARCHAR2
                                  , x_return  OUT NUMBER
                                  , x_message OUT VARCHAR2);
     


     /*****************************************************************************************************************************************
     *  PROCEDURE GET_WC_ITEM_LOV                                                                                                             *
     *                                                                                                                                        *
     *   PURPOSE:   This procedure used for items and upc code                                                                                *
     *                                                                                                                                        *
     *    Parameters: None                                                                                                                    *
     *    Return : XXWC_LABEL_PRINTER profile                                                                                                 *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        22-OCT-2014  Lee Spitzer               TMS Ticket 20141001-00198                                                          *
     *                                                     WC Canada: Enhancement: RFID / Mobile Supply Chain RFID / Mobile Supply Chain      *
     *****************************************************************************************************************************************/


    PROCEDURE GET_WC_ITEM_LOV ( x_item             OUT    NOCOPY t_genref --0
                              , p_organization_id  IN     VARCHAR2
                              , p_item             IN     VARCHAR2
                              );
   

     /*****************************************************************************************************************************************
     *  FUNCTION GET_PROFILE_VALUE                                                                                                            *
     *                                                                                                                                        *
     *   PURPOSE:   This function used to return the profile value from profile option                                                        *
     *                                                                                                                                        *
     *    Parameters: None                                                                                                                    *
     *    Return : XXWC_LABEL_PRINTER profile                                                                                                 *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        22-OCT-2014  Lee Spitzer               TMS Ticket 20141001-00198                                                          *
     *                                                     WC Canada: Enhancement: RFID / Mobile Supply Chain RFID / Mobile Supply Chain      *
     *****************************************************************************************************************************************/


    FUNCTION GET_PROFILE_VALUE (p_profile_option_name IN VARCHAR2)
      RETURN VARCHAR2;
 
      
     /*****************************************************************************************************************************************
     *  PROCEDURE GET_WC_SUBLOV                                                                                                               *
     *                                                                                                                                        *
     *   PURPOSE:   This procedure used for items and upc code                                                                                *
     *                                                                                                                                        *
     *    Parameters: None                                                                                                                    *
     *    Return : XXWC_LABEL_PRINTER profile                                                                                                 *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        22-OCT-2014  Lee Spitzer               TMS Ticket 20141001-00198                                                          *
     *                                                     WC Canada: Enhancement: RFID / Mobile Supply Chain RFID / Mobile Supply Chain      *
     *   1.1        20-JUN-2015  Lee Spitzer               TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions                   *
     *****************************************************************************************************************************************/


    PROCEDURE GET_WC_SUB_LOV ( x_subinventory           OUT    NOCOPY t_genref --0
                              , p_organization_id       IN     VARCHAR2
                              , p_subinventory_code     IN     VARCHAR2
                              , p_transfer_from_sub     IN     VARCHAR2
                              );
   
   /*****************************************************************************************************************************************
   *   PROCEDURE GET_OPEN_SALES_ORDERS                                                                                                      *
   *                                                                                                                                        *
   *   PURPOSE:   This procedure is used to get open_sales_orders                                                                           *
   *                                                                                                                                        *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        22-OCT-2014  Lee Spitzer               TMS Ticket 20141001-00198                                                          *
   *                                                     WC Canada: Enhancement: RFID / Mobile Supply Chain RFID / Mobile Supply Chain      *
   *****************************************************************************************************************************************/
   
   
   FUNCTION GET_OPEN_SALES_ORDERS ( p_organization_id     IN NUMBER
                                  , p_inventory_item_id   IN NUMBER
                                  , p_uom                 IN VARCHAR2
                                  )
      RETURN NUMBER;
   
   /*****************************************************************************************************************************************
   *   PROCEDURE GET_OPEN_COUNTER_ORDERS                                                                                                    *
   *                                                                                                                                        *
   *   PURPOSE:   This procedure is used to get open_counter_orders                                                                         *
   *                                                                                                                                        *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        22-OCT-2014  Lee Spitzer               TMS Ticket 20141001-00198                                                          *
   *                                                     WC Canada: Enhancement: RFID / Mobile Supply Chain RFID / Mobile Supply Chain      *
   *****************************************************************************************************************************************/
   
   FUNCTION GET_OPEN_COUNTER_ORDERS ( p_organization_id     IN NUMBER
                                     , p_inventory_item_id   IN NUMBER
                                     , p_uom                 IN VARCHAR2
                                  )
      RETURN NUMBER;
      
    
    
   /*****************************************************************************************************************************************
   *   PROCEDURE GET_WC_OH                                                                                                                  *
   *                                                                                                                                        *
   *   PURPOSE:   This procedure is used to get the on-hand inventroy                                                                       *
   *                                                                                                                                        *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        22-OCT-2014  Lee Spitzer               TMS Ticket 20141001-00198                                                          *
   *                                                     WC Canada: Enhancement: RFID / Mobile Supply Chain RFID / Mobile Supply Chain      *
   *****************************************************************************************************************************************/
  
    PROCEDURE GET_WC_OH             ( x_oh                     OUT    NOCOPY t_genref --0
                                    , p_organization_id        IN     NUMBER    --1
                                    , p_inventory_item_id      IN     VARCHAR2  --2
                                    , p_subinventory_code      IN     VARCHAR2  --3
                                    , p_locator_id             IN     VARCHAR2  --4
                                    , p_lot_number             IN     VARCHAR2  --5
                                    , p_project_id             IN     NUMBER    --6
                                    , p_task_id                IN     NUMBER    --7
                                    );

     /*****************************************************************************************************************************************
     *  PROCEDURE GET_ITEM_ONLY_LOV                                                                                                             *
     *                                                                                                                                        *
     *   PURPOSE:   This procedure used for items and upc code                                                                                *
     *                                                                                                                                        *
     *    Parameters: None                                                                                                                    *
     *    Return : XXWC_LABEL_PRINTER profile                                                                                                 *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        22-OCT-2014  Lee Spitzer               TMS Ticket 20141001-00198                                                          *
     *                                                     WC Canada: Enhancement: RFID / Mobile Supply Chain RFID / Mobile Supply Chain      *
     *****************************************************************************************************************************************/


    PROCEDURE GET_ITEM_ONLY_LOV ( x_item             OUT    NOCOPY t_genref --0
                                , p_organization_id  IN     VARCHAR2
                                , p_item             IN     VARCHAR2
                                );
                                
     /*****************************************************************************************************************************************
     *  PROCEDURE GET_ITEM_ONLY_LOV                                                                                                             *
     *                                                                                                                                        *
     *   PURPOSE:   This procedure used for items and upc code                                                                                *
     *                                                                                                                                        *
     *    Parameters: None                                                                                                                    *
     *    Return : XXWC_LABEL_PRINTER profile                                                                                                 *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        22-OCT-2014  Lee Spitzer               TMS Ticket 20141001-00198                                                          *
     *                                                     WC Canada: Enhancement: RFID / Mobile Supply Chain RFID / Mobile Supply Chain      *
     *****************************************************************************************************************************************/


    PROCEDURE GET_UPC_ONLY_LOV ( x_item             OUT    NOCOPY t_genref --0
                                , p_organization_id  IN     VARCHAR2
                                , p_item             IN     VARCHAR2
                                );

     /*****************************************************************************************************************************************
     *  PROCEDURE PROCESS_SUB_XFER                                                                                                            *
     *                                                                                                                                        *
     *   PURPOSE:   This procedure used to process sub xfer                                                                                   *
     *                                                                                                                                        *
     *    Parameters: None                                                                                                                    *
     *    Return : 0 = Sucess Otherwise errror                                                                                                *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        20-JUN-2015  Lee Spitzer               TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions                   *
     *****************************************************************************************************************************************/


  PROCEDURE PROCESS_SUB_XFER   ( p_organization_id       IN NUMBER       --1
                               , p_inventory_item_id     IN NUMBER       --2
                               , p_subinventory          IN VARCHAR2     --3
                               , p_locator_id            IN NUMBER       --4
                               , p_transfer_subinventory IN VARCHAR2     --5
                               , p_transfer_locator      IN NUMBER       --6
                               , p_lot_number            IN VARCHAR2     --7
                               , p_uom                   IN VARCHAR2     --8
                               , p_qty                   IN NUMBER       --9
                               , p_user_id               IN NUMBER       --10
                               , x_return                OUT NUMBER      --11
                               , x_message               OUT VARCHAR2);  --12
                                


     /*****************************************************************************************************************************************
     *  PROCEDURE GET_ACCOUNT_ALIAS_LOV                                                                                                       *
     *                                                                                                                                        *
     *   PURPOSE:   This procedure used for account alias                                                                                     *
     *                                                                                                                                        *
     *    Parameters: None                                                                                                                    *
     *    Return : Cursor for Account Alias LOV                                                                                               *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        20-JUN-2015  Lee Spitzer               TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions                   *
     *****************************************************************************************************************************************/


    PROCEDURE GET_ACCOUNT_ALIAS_LOV ( x_alias            OUT    NOCOPY t_genref --0
                                    , p_organization_id  IN     VARCHAR2
                                    , p_alias            IN     VARCHAR2
                                    );
                                    
     /*****************************************************************************************************************************************
     *  PROCEDURE GET_TRANSACTION_TYPE_LOV                                                                                                    *
     *                                                                                                                                        *
     *   PURPOSE:   This procedure used for tranasction type                                                                                  *
     *                                                                                                                                        *
     *    Parameters: None                                                                                                                    *
     *    Return : Cursor for Transaction Type LOV                                                                                            *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        20-JUN-2015  Lee Spitzer               TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions                   *
     *****************************************************************************************************************************************/


    PROCEDURE GET_TRANSACTION_TYPE_LOV ( x_transaction            OUT    NOCOPY t_genref --0
                                        , p_transaction_type      IN     VARCHAR2
                                       );                                  
                                
    
    
     /*****************************************************************************************************************************************
     *  PROCEDURE PROCESS_ALIAS_TXN                                                                                                           *
     *                                                                                                                                        *
     *   PURPOSE:   This procedure used to process account alias                                                                              *
     *                                                                                                                                        *
     *    Parameters: None                                                                                                                    *
     *    Return : 0 = Sucess Otherwise errror                                                                                                *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        20-JUN-2015  Lee Spitzer               TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions                   *
     *****************************************************************************************************************************************/


    PROCEDURE PROCESS_ALIAS_TXN  ( p_organization_id       IN NUMBER       --1
                                 , p_inventory_item_id     IN NUMBER       --2
                                 , p_subinventory          IN VARCHAR2     --3
                                 , p_locator_id            IN NUMBER       --4
                                 , p_lot_number            IN VARCHAR2     --5
                                 , p_uom                   IN VARCHAR2     --6
                                 , p_qty                   IN NUMBER       --7
                                 , p_disposition_id        IN NUMBER       --8
                                 , p_transaction_type_id   IN NUMBER       --9
                                 , p_transaction_action_id IN NUMBER       --10
                                 , p_transaction_source_type_id IN NUMBER  --11
                                 , p_user_id               IN NUMBER       --12
                                 , x_return                OUT NUMBER      --13
                                 , x_message               OUT VARCHAR2);  --14
                                  

     /*****************************************************************************************************************************************
     *  PROCEDURE GET_VENDOR_LOV                                                                                                              *
     *                                                                                                                                        *
     *   PURPOSE:   This procedure used for vendors                                                                                           *
     *                                                                                                                                        *
     *    Parameters: None                                                                                                                    *
     *    Return : Cursor for Vendor LOV                                                                                                      *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        20-JUN-2015  Lee Spitzer               TMS Ticket Number 20150622-00004 RF - UPC Update                                   *
     *****************************************************************************************************************************************/


    PROCEDURE GET_VENDOR_LOV ( x_vendor            OUT    NOCOPY t_genref --0
                              , p_vendor_name      IN     VARCHAR2
                              );                                  
                                


     /*****************************************************************************************************************************************
     *  PROCEDURE PROCESS_XREF                                                                                                                *
     *                                                                                                                                        *
     *   PURPOSE:   This procedure used for process cross reference                                                                           *
     *                                                                                                                                        *
     *    Parameters: None                                                                                                                    *
     *    Return : 0 = Sucess Otherwise errror                                                                                                *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        20-JUN-2015  Lee Spitzer               TMS Ticket Number 20150622-00004 RF - UPC Update                                   *
     *****************************************************************************************************************************************/
                             

    PROCEDURE PROCESS_XREF( p_organization_id       IN VARCHAR2 --1
                          , p_inventory_item_id     IN VARCHAR2 --2
                          , p_cross_reference       IN VARCHAR2 --3
                          , p_description           IN VARCHAR2 --4
                          , p_user_id               IN VARCHAR2 --5
                          , x_return                OUT NUMBER --6
                          , x_message               OUT VARCHAR2); --7
                    



     /*****************************************************************************************************************************************
     *  PROCEDURE GET_SALES_ORDER_LOV                                                                                                         *
     *                                                                                                                                        *
     *   PURPOSE:   This procedure used for STANDARD and INTERNAL Orders                                                                      *
     *                                                                                                                                        *
     *    Parameters: None                                                                                                                    *
     *    Return : Cursor for Sales Order                                                                                                     *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        20-JUN-2015  Lee Spitzer               TMS Ticket Number 20150622-00006 RF - Load Check                                   *
     *****************************************************************************************************************************************/


    PROCEDURE GET_SALES_ORDER_LOV ( x_sales_order       OUT    NOCOPY t_genref --0
                                  , p_organization_id   IN     VARCHAR2
                                  , p_order_number      IN     VARCHAR2
                                  );                                  
                                
                          
 END XXWC_MWA_ROUTINES_PKG;