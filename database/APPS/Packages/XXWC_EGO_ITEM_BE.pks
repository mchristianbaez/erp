CREATE OR REPLACE PACKAGE APPS.XXWC_EGO_ITEM_BE
AS
   /******************************************************************************************************
   -- File Name: XXWC_EGO_ITEM_BE.pks
   --
   -- PROGRAM TYPE: PL/SQL Script   <API>
   --
   -- PURPOSE:
   -- HISTORY
   -- ==========================================================================================================
   -- ==========================================================================================================
   -- VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- --------------------------------------------------------------------
   -- 1.0     01-MAR-2015   Shankar Vanga   TMS#20150126-00044  - Create Single Item Workflow Tool

   -- 2.0     26-May-2015   P.vamshidhar    TMS#20150519-00091 - Single Item UI Enhancements
                                            Added to function to create link in Workflow monitor
                                            if function return 0 user not eligible to access link else yes.

   --3.0      17-Jun-2015   P.Vamshidhar    TMS#20150609-00025 added purge_temp_data declaration part in spec.

   --4.0      11-Aug-2015   P.Vamshidhar    TMS#20141006-00021 - Description and Shelf Life changes not consistently
                                            propagating to lower level Orgs from Item Master
                                            TMS#20150807-00170 -- PDH - Add UDA in PDH for Last Verified Date
                                            Procedure: update_uda_attributes                                               
   ************************************************************************************************************/

   FUNCTION handle_CO_Status_change (
      P_SUBSCRIPTION_GUID   IN            RAW,
      P_EVENT               IN OUT NOCOPY WF_EVENT_T)
      RETURN VARCHAR2;

   PROCEDURE update_uda_attributes (i_change_order    IN            VARCHAR2,
                                    i_change_type     IN            VARCHAR2,    -- Added by Vamshi in rev 4.0
                                    i_change_id       IN            NUMBER,
                                    i_inv_item_id     IN            NUMBER,
                                    i_org_id          IN            NUMBER,
                                    o_error_code         OUT NOCOPY VARCHAR2,
                                    o_error_message      OUT NOCOPY VARCHAR2);

   PROCEDURE Apply_Changes (i_change_id IN NUMBER);


   FUNCTION XXWC_EGOGROUP_USR_VALID_FUNC (P_USER_NAME    IN VARCHAR2,
                                          P_GROUP_NAME   IN VARCHAR2)
      RETURN NUMBER;


   -- Added below declaration part into code in TMS#20150609-00025

   PROCEDURE purge_temp_data (i_change_id           IN NUMBER,
                              i_item_id                NUMBER,
                              i_change_order_type      VARCHAR2);


   -- Added below declaration part into code in TMS#20141006-00021
   PROCEDURE XXWC_WEIGHT_UOM_CODE_UPDATE (p_change_notice       IN VARCHAR2,
                                          p_inventory_item_id   IN NUMBER);


 END XXWC_EGO_ITEM_BE;
/
