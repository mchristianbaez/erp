CREATE OR REPLACE PACKAGE XXWC_ASL_CONV_PKG
/********************************************************************************

FILE NAME: APPS.XXWC_ASL_CONV_PKG.pkg

PROGRAM TYPE: PL/SQL Package

PURPOSE: API to load Approved Supplier List into Oracle.

HISTORY
===============================================================================
VERSION DATE          AUTHOR(S)       DESCRIPTION
------- -----------   --------------- -----------------------------------------
1.0     02/10/2013    Gopi Damuluri    Initial version.
********************************************************************************/
AS

PROCEDURE main (p_errbuf       OUT VARCHAR2,
                p_retcode      OUT VARCHAR2,
                p_validate_only IN VARCHAR2);

END XXWC_ASL_CONV_PKG;

/


