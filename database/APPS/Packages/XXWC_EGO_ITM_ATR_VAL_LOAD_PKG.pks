CREATE OR REPLACE PACKAGE APPS.XXWC_EGO_ITM_ATR_VAL_LOAD_PKG AS
/* *******************************************************************************
File Name				: XXWC_EGO_ITM_ATR_VAL_LOAD_PKG
PROGRAM TYPE			: PL/SQL Package spec 
PURPOSE					: Procedures and functions for uploading the item attribute values mass load via Web ADI to EBS Default Interface tables. As of now, this will work only for single row AG
-- Dependencies Tables 	: ego_itm_usr_attr_intrfc
--Pre Processing		: Related item and ICCs should be already exist

-- Post Processing 		: Call the CP: Import Catalog Items(EGOICICP) to load the data from Interface table to Main tables


HISTORY		:
==================================================================================
VERSION DATE          AUTHOR(S)       		  DESCRIPTION
------- -----------   ----------------------  -----------------------------------------
1.0     13-JUN-2013   Rajasekar Gunasekaran	  Initial Version
2.0     10-JUL-2013   Rajasekar Gunasekaran	  Added the new Proc LOAD_ITEM_ATTR_VAL_TO_MAIN CP proc 
2.1		19-JUL-2013   Rajasekar Gunasekaran	  Added the new Proc strToList for IN list conversion
*********************************************************************************/


FUNCTION LOAD_ITEM_ATTR_VAL_TO_INTRF 
	(
	P_DATA_SET_ID                  			NUMBER DEFAULT 99999                                                                                                                                                                                    
	,P_ORG_CODE                    			VARCHAR2                                                                                                                                                                                   
	,P_ITEM_NUMBER                          VARCHAR2                                                                                                                                                                                  
	,P_REVISION                             VARCHAR2 DEFAULT NULL                                                                                                                                                                                  
	,P_AG_INT_NAME            				VARCHAR2
	,P_AG_DISP_NAME            				VARCHAR2	
	,P_AG_TYPE                      		VARCHAR2 DEFAULT 'EGO_ITEMMGMT_GROUP'
	,P_ICC_NAME								VARCHAR2	
	,P_ATTR_INT_NAME                  		VARCHAR2 
	,P_ATTR_DISP_NAME                  		VARCHAR2	
	,P_ATTR_VALUE_STR                       VARCHAR2 DEFAULT NULL                                                                                                                                                                               
	,P_ATTR_VALUE_NUM                       NUMBER DEFAULT NULL                                                                                                                                                                                       
	,P_ATTR_VALUE_DATE                      DATE DEFAULT NULL                                                                                                                                                                                         
	,P_ATTR_DISP_VALUE                      VARCHAR2                                                                                                                                                                                
	,P_TRANSACTION_TYPE                     VARCHAR2 DEFAULT 'SYNC'
	,P_DATA_LEVEL_NAME                      VARCHAR2 DEFAULT 'ITEM_LEVEL'		                                                                                                                                                                                
	,P_SOURCE_SYSTEM_ID                     NUMBER DEFAULT NULL                                                                                                                                                                                       
	,P_SOURCE_SYSTEM_REFERENCE              VARCHAR2 DEFAULT NULL                                                                                                                                                                                
	,P_ATTR_UOM_DISP_VALUE                  VARCHAR2 DEFAULT NULL                                                                                                                                                                                 
	,P_ATTR_VALUE_UOM                       VARCHAR2 DEFAULT NULL                                                                                                                                                                                  
	                                                                                                                                                                                 
	) RETURN VARCHAR2;

	
PROCEDURE LOAD_ITEM_ATTR_VAL_TO_MAIN(errbuf OUT NOCOPY VARCHAR2, retcode OUT NOCOPY VARCHAR2,p_data_set_id IN VARCHAR2);

FUNCTION strToList (p_str_list  IN  VARCHAR2) RETURN xxwc_ego_list_tab;

END XXWC_EGO_ITM_ATR_VAL_LOAD_PKG;
