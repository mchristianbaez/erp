--
-- XXWC_POSOURCINGCONV_PKG  (Package) 
--
CREATE OR REPLACE PACKAGE APPS.xxwc_posourcingconv_pkg
IS
   /***************************************************************************
               *    script name: xxwc_posourcingconv_pkg.pks
               *
               *    interface / conversion name: PO Sourcing Rules and
                                                 assignments conversion.
               *
               *    functional purpose: convert Sourcing Rules and assignments
               *
               *    history:
               *
               *    version    date              author             description
               ****************************************************************
               *    1.0        04-sep-2011   k.Tavva     initial development.
               ***************************************************************/
   PROCEDURE dup_src_validation;

   PROCEDURE dup_assign_validation;

   PROCEDURE validations;

   PROCEDURE process_records;

   PROCEDURE sorce_rule_validate;

   PROCEDURE assignment_rec_exists;

   PROCEDURE xx_sourcing_rules_transfer (l_vendor_num VARCHAR2);

   PROCEDURE posourcing_conv_proc (errbuf               OUT VARCHAR2
                                  ,retcode              OUT NUMBER
                                  ,p_validate_only   IN     VARCHAR2 --,p_submit IN VARCHAR2
                                                                    );

   PROCEDURE print_debug (p_print_str IN VARCHAR2);
END xxwc_posourcingconv_pkg;
/

