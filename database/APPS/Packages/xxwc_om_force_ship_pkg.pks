/* Formatted on 11/6/2013 2:48:10 PM (QP5 v5.206) */
-- Start of DDL Script for Package APPS.XXWC_OM_FORCE_SHIP_PKG
-- Generated 11/6/2013 2:48:09 PM from APPS@EBIZFQA

CREATE OR REPLACE PACKAGE apps.xxwc_om_force_ship_pkg
    AUTHID CURRENT_USER
AS
    /*************************************************************************
      $Header: xxwc_om_force_ship_pkg $
      Module Name: xxwc_om_force_ship_pkg.pks

      PURPOSE:   This package is called by the "Force Ship" extension

      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------
      1.0        07/23/2012  Shankar Hariharan       Initial Version
      2.0        10/26/2012  Lee Spitzer             Update Process Force Ship to Restrict by Organization
      3.0       3/11/2014    Ram Talluri            Added procedure to submit interface trip stop for TMS #20140310-00017
    **************************************************************************/
    -- Global variable declaration
    g_request_id    NUMBER;
    g_subinv        VARCHAR2 (15) := 'General';
    g_pick_subinv   VARCHAR2 (15) := 'GeneralPCK';

    PROCEDURE write_log (p_debug_msg IN VARCHAR2);

    PROCEDURE write_error (p_debug_msg IN VARCHAR2, p_call_point IN VARCHAR2);

    PROCEDURE process_subinv_transfer (p_organization_id     IN     NUMBER
                                      ,p_inventory_item_id   IN     NUMBER
                                      ,p_transfer_qty        IN     NUMBER
                                      ,p_transfer_uom        IN     VARCHAR2
                                      ,p_header_id           IN     NUMBER
                                      ,p_line_id             IN     NUMBER
                                      ,p_lot_control         IN     VARCHAR2
                                      ,p_lot_number          IN     VARCHAR2
                                      ,p_return_status          OUT VARCHAR2
                                      ,p_return_msg             OUT VARCHAR2);

    PROCEDURE query_reservation (p_inventory_item_id   IN     NUMBER
                                ,p_organization_id     IN     NUMBER
                                ,p_line_id             IN     NUMBER
                                ,p_header_id           IN     NUMBER
                                ,p_rsv_tbl                OUT inv_reservation_global.mtl_reservation_tbl_type
                                ,p_return_status          OUT VARCHAR2
                                ,p_return_msg             OUT VARCHAR2);

    PROCEDURE create_reservation (p_inventory_item_id   IN     NUMBER
                                 ,p_organization_id     IN     NUMBER
                                 ,p_reservation_qty     IN     NUMBER
                                 ,p_reservation_uom     IN     VARCHAR2
                                 ,p_line_id             IN     NUMBER
                                 ,p_header_id           IN     NUMBER
                                 ,p_serial_control      IN     VARCHAR2
                                 ,p_lot_control         IN     VARCHAR2
                                 ,p_lot_number          IN     VARCHAR2
                                 ,p_return_status          OUT VARCHAR2
                                 ,p_return_msg             OUT VARCHAR2);

    PROCEDURE delete_reservation (p_inventory_item_id   IN     NUMBER
                                 ,p_organization_id     IN     NUMBER
                                 ,p_line_id             IN     NUMBER
                                 ,p_header_id           IN     NUMBER
                                 ,p_reservation_id      IN     NUMBER
                                 ,p_lot_number          IN     VARCHAR2
                                 ,p_return_status          OUT VARCHAR2
                                 ,p_return_msg             OUT VARCHAR2);

    FUNCTION get_on_hand (i_inventory_item_id IN NUMBER, i_organization_id IN NUMBER)
        RETURN NUMBER;

    PROCEDURE update_order_line (p_header_id       IN     NUMBER
                                ,p_line_id         IN     NUMBER
                                ,p_return_status      OUT VARCHAR2
                                ,p_return_msg         OUT VARCHAR2);

    PROCEDURE process_force_ship (i_header_id         IN     NUMBER
                                 ,i_organization_id   IN     NUMBER                                   --added 10/26/2012
                                 ,p_return_status        OUT VARCHAR2
                                 ,p_return_msg           OUT VARCHAR2);

    PROCEDURE update_line_subinv (errbuf OUT VARCHAR2, retcode OUT NUMBER);

    --Added by Rasikha- to check that row may locked by another process
    --*************************************************************************
    FUNCTION is_row_locked (v_rowid ROWID, table_name VARCHAR2)
        RETURN VARCHAR2;

    --added by Rasikha overload
    PROCEDURE update_line_subinv_catchup (errbuf OUT VARCHAR2, retcode OUT NUMBER, p_last_delta_date VARCHAR2);
    --Added by Ram Talluri on 3/11/2014 for TMS #20140310-00017
    PROCEDURE submit_its_stuck_deliveries (errbuf OUT VARCHAR2, retcode OUT NUMBER, p_number_of_days IN NUMBER, p_header_id IN NUMBER);
END xxwc_om_force_ship_pkg;
/

-- End of DDL Script for Package APPS.XXWC_OM_FORCE_SHIP_PKG

