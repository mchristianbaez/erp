CREATE OR REPLACE PACKAGE APPS.XXCUS_COSTAR_AP_EXPORT AS
/**************************************************************************
   $Header XXCUS_COSTAR_AP_EXPORT $
   Module Name: XXCUS_COSTAR_AP_EXPORT.pks

   PURPOSE:   This package is called by the concurrent programs
              XXCUS CoStar AP Invoices Import for importing AP invoices into EBS system.
   REVISIONS:
   Ver        Date        Author             	Description
   ---------  ----------  ---------------   	-------------------------
    1.0       25/06/2018  Ashwin Sridhar /   	Initial Build - Task ID: 20180709-00059
						  Vamshi Singirikonda	Adding GL External ID
/*************************************************************************/
p_run_id       NUMBER;
p_group_id     VARCHAR2(240);

PROCEDURE Process_Invoices(p_errbuf    OUT VARCHAR2
                          ,p_retcode   OUT VARCHAR2
                          ,p_file_name IN  VARCHAR2
                          ,p_run_id    IN  NUMBER
                          ,p_group_id  IN  VARCHAR2);

PROCEDURE LOAD_INVOICES_DATA(p_run_id IN NUMBER);

FUNCTION get_field (v_delimiter  IN VARCHAR2
                   ,n_field_no   IN NUMBER 
                   ,v_line_read  IN VARCHAR2
                   ,p_which_line IN NUMBER) RETURN VARCHAR2;

FUNCTION get_run_id RETURN NUMBER;

FUNCTION get_group_id RETURN VARCHAR2;

PROCEDURE LOAD_AUDIT_DATA(p_run_id   IN NUMBER
                         ,p_group_id IN VARCHAR2
                         ,p_stage    IN VARCHAR2);

PROCEDURE GENERATE_AUDIT_REPORT(p_errbuf   OUT VARCHAR2
                               ,p_retcode  OUT VARCHAR2
                               ,p_run_id   IN NUMBER
                               ,p_group_id IN VARCHAR2
                               );

FUNCTION afterReport RETURN BOOLEAN;

FUNCTION beforereport RETURN BOOLEAN;

PROCEDURE uc4_process_invoices(p_errbuf               OUT VARCHAR2
							   ,p_retcode             OUT NUMBER
							   ,p_data_file           IN VARCHAR2
							   ,p_run_id              IN VARCHAR2
							   ,p_group_id            IN VARCHAR2
							   ,p_user_name           IN VARCHAR2
							   ,p_responsibility_name IN VARCHAR2) ;
							   
PROCEDURE uc4_Payables_interface(p_errbuf             OUT VARCHAR2
							   ,p_retcode             OUT NUMBER							   
							   ,p_group_id            IN VARCHAR2
							   ,p_user_name           IN VARCHAR2
							   ,p_responsibility_name IN VARCHAR2) ;
							   
PROCEDURE uc4_invoice_Validation(p_errbuf             OUT VARCHAR2
							   ,p_retcode             OUT NUMBER							   
							   ,p_group_id            IN VARCHAR2
							   ,p_user_name           IN VARCHAR2
							   ,p_responsibility_name IN VARCHAR2) ;	
							   
PROCEDURE uc4_Generate_Audit   (p_errbuf              OUT VARCHAR2
							   ,p_retcode             OUT NUMBER	
                               ,p_run_id              IN VARCHAR2							   
							   ,p_group_id            IN VARCHAR2
							   ,p_user_name           IN VARCHAR2
							   ,p_responsibility_name IN VARCHAR2) ;
							   
PROCEDURE UC4_Submit_Audit_Report(p_errbuf               OUT VARCHAR2
								   ,p_retcode             OUT NUMBER   
								   ,p_run_id              IN VARCHAR2
								   ,p_group_id            IN VARCHAR2
								   ,p_user_name           IN VARCHAR2
								   ,p_responsibility_name IN VARCHAR2) ;							   

END XXCUS_COSTAR_AP_EXPORT;
/