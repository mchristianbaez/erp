CREATE OR REPLACE PACKAGE APPS.xxcus_ar_geocode_pkg
-- ESMS TICKET
-- used by OEXOEINL.pll before calling create_locationroutine to plug in the sales tax geocode value required for site creation
AS

  function get_ship_to_geocode (
    l_state         in  varchar2
   ,l_cityname      in  varchar2   
   ,l_zipcode       in  varchar2
   ,l_county        in  varchar2
  ) return varchar2;   
   
end xxcus_ar_geocode_pkg;
/