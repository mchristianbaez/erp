CREATE OR REPLACE PACKAGE APPS.xxwc_po_req_qty_update_pkg
--//============================================================================
--//
--// Object Name         :: xxwc_po_req_qty_update_pkg
--//
--// Object Type         :: Package Specification
--//
--// Object Description  :: This is used to fix Zero Requisition Qty.
--//
--// Version Control
--//============================================================================
--// Vers    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Mahesh Kudle     02/09/2014    Initial Build - TMS#20140904-00170
--//============================================================================
AS

--//============================================================================
--//
--// Object Name         :: xxwc_req_zero_qty
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This is used to fix Zero Requisition Qty.
--//
--// Version Control
--//============================================================================
--// Vers    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Mahesh Kudle     02/09/2014    Initial Build - TMS#20140904-00170
--//============================================================================
PROCEDURE xxwc_req_zero_qty (RETCODE      OUT VARCHAR2
                            ,ERRMSG       OUT VARCHAR2
                            ,P_REQ_NUMBER IN VARCHAR2
							,P_DAYS       IN NUMBER);

end xxwc_po_req_qty_update_pkg;
/