CREATE OR REPLACE PACKAGE APPS.xxwc_ar_lien_release_pkg
AS
   /******************************************************************************
      NAME:       apps.xxwc_ar_lien_release_pkg
      PURPOSE:    Package to create a case folder for lien release.

      REVISIONS:
      Ver        Date        Author           Description
      ---------  ----------  ---------------  ------------------------------------
      1.0        11/22/2013      shariharan    Initial Version
   ******************************************************************************/

   FUNCTION get_rem_order_total (i_header_id IN NUMBER)
      RETURN NUMBER;
      
   PROCEDURE create_case_folder (i_customer_id in number,i_request_id in number,error_message out varchar2);      
END xxwc_ar_lien_release_pkg;
/