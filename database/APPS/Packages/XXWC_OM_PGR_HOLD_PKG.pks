CREATE OR REPLACE PACKAGE APPS.XXWC_OM_PGR_HOLD_PKG
IS
   /*************************************************************************
   *   $Header XXXWC_OM_PGR_HOLD_PKG.pks $
   *   Module Name: XXWC_OM_PGR_HOLD_PKG.pks
   *
   *   PURPOSE:   This package is used for Applying Pricing Guard Rail Hold
   *
   *   REVISIONS:
   *   Ver        Date         Author                Description
   *   ---------  ----------   ---------------       -------------------------
   *   1.0        4-Nov-2015   Manjula Chellappan    Initial Version
   *                                                  TMS# 20151006-00045 Pricing guard rail project (Removal of pricing guard rail holds)
   * ***************************************************************************/

   g_distro_list   VARCHAR2 (75) DEFAULT 'hdsoracledevelopers@hdsupply.com';

   /* **************************************************************************
      *   Procedure Name: Apply_hold_program
      *
      *   PURPOSE:   This procedure called from a conc prog to Apply Pricing Guard Rail Hold
   *              for the Sales Order/Lines with erroraneous Margin
      *
      *   REVISIONS:
      *   Ver        Date         Author                Description
      *   ---------  ----------   ---------------       -------------------------
      *   1.0        4-Nov-2015   Manjula Chellappan    Initial Version
      *                                                  TMS# 20151006-00045 Pricing guard rail project (Removal of pricing guard rail holds)
      * ***************************************************************************/
   PROCEDURE Apply_hold_program (errbuf           OUT VARCHAR2,
                                 retcode          OUT VARCHAR2,
                                 p_from_date 	  IN VARCHAR2,
								 p_emailid   	  IN VARCHAR2,
								 p_filename  	  IN VARCHAR2);
END XXWC_OM_PGR_HOLD_PKG;
/