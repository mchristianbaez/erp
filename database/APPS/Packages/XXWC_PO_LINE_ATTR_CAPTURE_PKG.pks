CREATE OR REPLACE PACKAGE APPS.XXWC_PO_LINE_ATTR_CAPTURE_PKG
IS
   /*****************************************************************************************************************************************
   -- File Name: XXWC_PO_LINE_ATTR_CAPTURE_PKG.pks
   --
   -- PROGRAM TYPE: PL/SQL Script   <API>
   --
   -- PURPOSE: To track and update invalid buyer information in PO Requisition interface table error records.
   --
   -- HISTORY
   -- ========================================================================================================================================
   -- ========================================================================================================================================
   -- VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- --------------------------------------------------------------------------------------------------
   -- 1.0     25-AUG-2015   P.vamshidhar    TMS#20150515-00063 - PO - Stocking Flag at time of PO Creation
   --                                       Initial Version.

   ******************************************************************************************************************************************/

   PROCEDURE CAPTURE_ADDITIONAL_ATTR (x_err_buf        OUT VARCHAR2,
                                      x_retcode        OUT VARCHAR2,
                                      p_from_date      IN  VARCHAR2);
END XXWC_PO_LINE_ATTR_CAPTURE_PKG;
/
