--
-- XXWCINV_ECO_APPROVAL_WF_PKG  (Package) 
--
CREATE OR REPLACE PACKAGE APPS.XXWCINV_ECO_APPROVAL_WF_PKG
IS
   /**************************************************************************
    *
    * PROCEDURE
    *  Get_Attributes
    *
    * DESCRIPTION
    *  This procedure populates the custom elements within the workflow process
    *
    * PARAMETERS
    * ==========
    * NAME              TYPE     IN/OUT  DESCRIPTION
   .* ----------------- -------- ------  ---------------------------------------
    * p_itemtype        VARCHAR2 IN      workflow key
    * p_itemkey         VARCHAR2 IN      workflow instance identifier
    * p_actid           NUMBER   IN      action id
    * p_funcmode        VARCHAR2 IN      function mode (RUN, etc.)
    * x_result          VARCHAR2 OUT     result code
    *
    * CALLED BY
    *  XXWC Standard Approval process within the ECO Approval workflow
    *
    *************************************************************************/
   PROCEDURE Get_Attributes (p_itemtype   IN            VARCHAR2
                            ,p_itemkey    IN            VARCHAR2
                            ,p_actid      IN            NUMBER
                            ,p_funcmode   IN            VARCHAR2
                            ,x_result        OUT NOCOPY VARCHAR2);
END XXWCINV_ECO_APPROVAL_WF_PKG;
/

