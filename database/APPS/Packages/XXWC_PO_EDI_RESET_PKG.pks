CREATE OR REPLACE PACKAGE apps.xxwc_po_edi_reset_pkg
/*************************************************************************
  $Header XXWC_PO_EDI_FLAG_RESET_PKG.pks $
  Module Name : XXWC_PO_EDI_FLAG_RESET_PKG

  PURPOSE     : Package to Reset the Fields for Retriggering POO and POCO documents
                POO  - PO Outbound
                POCO - PO change outbound
  TMS Task Id : 20140516-00168

  REVISIONS:
  Ver        Date          Author                Description
  ---------  ----------    ------------------    ----------------
  1.0        18-Feb-2015   Manjula Chellappan    Initial Version

**************************************************************************/
IS
   PROCEDURE main (errbuf                     OUT VARCHAR2,
                   retcode                    OUT NUMBER,
                   p_trx_type              IN     VARCHAR2,
                   p_enable_attr           IN     VARCHAR2,
                   p_po_number             IN     VARCHAR2,
                   p_header_vendor_note    IN     VARCHAR2,
                   p_freight_acct_number   IN     VARCHAR2,
                   p_line_number           IN     NUMBER,
                   p_enable_item_desc      IN     VARCHAR2,
                   p_item_desc             IN     VARCHAR2);

   PROCEDURE validate_poo (p_po_number   IN     VARCHAR2,
                           p_retcode        OUT VARCHAR2,
                           p_error_msg      OUT VARCHAR2);

   PROCEDURE validate_poco (p_po_number   IN     VARCHAR2,
                            p_retcode        OUT VARCHAR2,
                            p_error_msg      OUT VARCHAR2);

   PROCEDURE update_attributes (p_po_number             IN     VARCHAR2,
                                p_header_vendor_note    IN     VARCHAR2,
                                p_freight_acct_number   IN     VARCHAR2,
                                p_line_number           IN     NUMBER,
                                p_item_desc             IN     VARCHAR2,
                                p_retcode                  OUT VARCHAR2,
                                p_error_msg                OUT VARCHAR2);

   PROCEDURE update_poo_flag (p_po_number   IN     VARCHAR2,
                              p_retcode        OUT VARCHAR2,
                              p_error_msg      OUT VARCHAR2);

   PROCEDURE update_poco_flag (p_po_number   IN     VARCHAR2,
                               p_retcode        OUT VARCHAR2,
                               p_error_msg      OUT VARCHAR2);
END xxwc_po_edi_reset_pkg;