CREATE OR REPLACE PACKAGE APPS.xxwc_ar_inv_int_pkg AS
 /******************************************************************************************************************************************************
        $Header xxwc_ar_inv_int_pkg $
        Module Name: xxwc_ar_inv_int_pkg.pks

        PURPOSE:   AHH Receipt Conversion

        REVISIONS:
        Ver        Date        Author           Description
        ---------  ----------  ---------------  ------------------------------------------------------------------------------------------------
        1.0        07/08/2018  P.Vamshidhar     TMS#20180708-00002 - AH HARRIS AR Debit Memo/Invoice Interface
******************************************************************************************************************************************************/

  PROCEDURE submit_interface(p_batch_source IN VARCHAR2);

  PROCEDURE load_interface;

  PROCEDURE main(errbuf          OUT VARCHAR2
                ,retcode         OUT NUMBER
                ,p_conc_prg_name IN VARCHAR2);

  PROCEDURE uc4_call(p_errbuf              OUT VARCHAR2
                    ,p_retcode             OUT NUMBER
                    ,p_conc_prg_name       IN VARCHAR2
                    ,p_conc_prg_arg1       IN VARCHAR2
                    ,p_user_name           IN VARCHAR2
                    ,p_responsibility_name IN VARCHAR2
                    ,p_org_name            IN VARCHAR2
                    ,p_threshold_value     IN NUMBER);

  PROCEDURE load_debit_memos(errbuf          OUT VARCHAR2
                            ,retcode         OUT NUMBER
                            ,p_conc_prg_name IN VARCHAR2);

  PROCEDURE update_acct_global_var;

  PROCEDURE update_dm_acct_global_var;

  PROCEDURE update_rev_amt(p_errbuf     OUT VARCHAR2
                          ,p_retcode    OUT NUMBER
                          ,p_trx_number IN VARCHAR2);

  PROCEDURE uc4_update_rev(p_errbuf              OUT VARCHAR2
                          ,p_retcode             OUT NUMBER
                          ,p_conc_prg_name       IN VARCHAR2
                          ,p_user_name           IN VARCHAR2
                          ,p_responsibility_name IN VARCHAR2
                          ,p_org_name            IN VARCHAR2);

END xxwc_ar_inv_int_pkg;
/