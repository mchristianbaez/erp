--
-- XXWC_AR_PRISM_CUST_IFACE_PKG  (Package) 
--
CREATE OR REPLACE PACKAGE APPS.xxwc_ar_prism_cust_iface_pkg
AS
   /*************************************************************************
    Copyright (c) 2012 Lucidity Consulting Group
    All rights reserved.
   **************************************************************************
     $Header xxwc_ar_prism_cust_iface_pkg.pls 120.1.12010000.1 $
     Module Name: xxwc_ar_prism_cust_iface_pkg.pks

     PURPOSE:   This package is called by a UC4 scheduler and controller
         program that will then initiate Concurrent Program
                XXWC Prism-EBS Customer Interface Processing
                to validate and process cash accounts, job sites and contacts

     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.0        03/13/2012  Consuelo Gonzalez      Initial Version as copy
                 of customer conversion code
   **************************************************************************/

   -- Global Variable Definition
   g_parameter_prefix   VARCHAR2 (30);
   g_org_id    CONSTANT NUMBER := fnd_global.org_id;
   g_request_id         NUMBER;

   /*************************************************************************
     Procedure :  populate_collectors

     Purpose:    Procedure called by validate_data that will repopulate the
         xref staging table for collectors with their matching
                   acting EBS Collector ID
   ************************************************************************/
   PROCEDURE populate_collectors;

   /*************************************************************************
     Procedure :  populate_credit_analyst

     Purpose:    Procedure called by validate_data that will repopulate
         the xref staging table for credit analysts with a matching
                   EBS Credit Analyst
   ************************************************************************/
   PROCEDURE populate_credit_analyst;

   /*************************************************************************
     Procedure :  validate_data

     Purpose:    Procedure called by the Process File procedure to validate
         data sent from prism
   ************************************************************************/
   PROCEDURE validate_data;

   /*************************************************************************
     Procedure :  customer_interface

     Purpose:    Procedure called by the Process File to load the valid
         data in the staging tables into Oracle
   ************************************************************************/
   PROCEDURE customer_interface (errbuf       OUT VARCHAR2
                                ,retcode      OUT VARCHAR2
                                ,p_mode           VARCHAR2
                                ,prefix    IN     VARCHAR2 DEFAULT NULL);

   /*************************************************************************
     Procedure :  pop_contact_info

     Purpose:    Procedure used by Customer_Interface to create contact
         information when applicable.
                   Not being used, inherited from conversion
   ************************************************************************/
   PROCEDURE pop_contact_info (p_site_use_type       IN     VARCHAR2
                              ,p_party_id            IN     NUMBER
                              ,p_cust_acct_id        IN     NUMBER
                              ,p_cust_acct_site_id   IN     NUMBER
                              ,p_rowid               IN     ROWID
                              ,p_out                    OUT VARCHAR2);

   /*************************************************************************
     Procedure :  pop_org_communication

     Purpose:    Procedure used by Customer_Interface to create communication
         points at the applicable level
   ************************************************************************/
   PROCEDURE pop_org_communication (p_level             IN     VARCHAR2
                                   ,p_party_id          IN     NUMBER
                                   ,p_phone_area_code   IN     VARCHAR2
                                   ,p_contact_phone     IN     VARCHAR2
                                   ,p_fax_area_code     IN     VARCHAR2
                                   ,p_contact_fax       IN     VARCHAR2
                                   ,p_out                  OUT VARCHAR2
                                   ,p_err_msg              OUT VARCHAR2);

   /*************************************************************************
     Procedure :  get_cust_conversion_stats

     Purpose:    Procedure used to determine customer stats in validation
         mode
   ************************************************************************/
   PROCEDURE get_cust_conversion_stats;

   /*************************************************************************
     Procedure :  populate_site_locations

     Purpose:    Procedure used to populate Customer Site Locations. Identify
          duplicate addresses within a party and assign oracle location Id,
                   Legacy Site Type column value in Customer Sites Staging table.
   ************************************************************************/
   PROCEDURE populate_site_locations;

   /*************************************************************************
     Procedure :  update_customer_without_sites

     Purpose:    Identifies Customer Records with out any sites and updates
         their status as Inactive
   ************************************************************************/
   PROCEDURE update_customer_without_sites;

   /*************************************************************************
     Procedure :  update_contacts_without_sites

     Purpose:    Procedure to identify Orphan Customer Contacts,
         Records with out corresponding Customer are updated
                   to a status of Error
   ************************************************************************/
   PROCEDURE update_contacts_without_sites;

   /*************************************************************************
     Procedure :  oe_ship_to

     Purpose:    Another Concurrent Program to process OE SHIP TO sites.
         This program should be called only after main customer
                   Conversion program is processed successfully. API Processes
                   all Customer Sites that are validated and whose customers
                   are already processed through main program and creates
                   Customer Sites and its contacts

                   Procedure inherited from conversion. Currently not used
                   by the Prism Customer Interface
   ************************************************************************/
   PROCEDURE oe_ship_to (errbuf       OUT VARCHAR2
                        ,retcode      OUT VARCHAR2
                        ,prefix    IN     VARCHAR2 DEFAULT NULL);

   /*************************************************************************
     Procedure:  pop_org_web_contact

     Purpose:    Procedure to create WEB Contact at Party or Party Site Level.
         Procedure called by customer_inteface
   ************************************************************************/
   PROCEDURE pop_org_web_contact (p_level         IN     VARCHAR2
                                 ,p_party_id      IN     NUMBER
                                 ,p_cod_comment   IN     VARCHAR2
                                 ,p_keyword       IN     VARCHAR2
                                 ,p_out              OUT VARCHAR2
                                 ,p_err_msg          OUT VARCHAR2);

   /*************************************************************************
     Procedure:  process_files

     Purpose:    Main procedure that sequences the customer data validation
         and then the load of the valid data
   ************************************************************************/
   PROCEDURE process_files (errbuf                        OUT VARCHAR2
                           ,retcode                       OUT VARCHAR2
                           ,p_process_type             IN     VARCHAR2
                           ,p_customer_file_name       IN     VARCHAR2
                           ,p_cust_site_file_name      IN     VARCHAR2
                           ,p_cust_contact_file_name   IN     VARCHAR2);

   /*************************************************************************
     Procedure:  submit_job

     Purpose:    Procedure called by UC4 to initiate the customer
         interface code. Starts with validation and moves on
                   to actual load of customer/site records
   ************************************************************************/
   PROCEDURE submit_job (errbuf                     OUT VARCHAR2
                        ,retcode                    OUT VARCHAR2
                        ,p_process_type          IN     VARCHAR2
                        ,p_user_name             IN     VARCHAR2
                        ,p_responsibility_name   IN     VARCHAR2);

   /*************************************************************************
     Procedure:  archive_data

     Purpose:    Procedure used to archive processed records from the
         main staging tables to the archiving tables
   ************************************************************************/
   PROCEDURE archive_data (errbuf                OUT VARCHAR2
                          ,retcode               OUT VARCHAR2
                          ,p_archive_type     IN     VARCHAR2
                          ,p_statuses         IN     VARCHAR2
                          ,p_from_proc_date   IN     VARCHAR2
                          ,p_to_proc_date     IN     VARCHAR2);
END xxwc_ar_prism_cust_iface_pkg;
/

