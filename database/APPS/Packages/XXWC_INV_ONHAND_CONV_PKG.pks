CREATE OR REPLACE PACKAGE XXWC_INV_ONHAND_CONV_PKG
AS
  /***************************************************************************
  *    script name: xxwc_inv_onhand_conv_pkg.pks
  *
  *    interface / conversion name: Item On-hand conversion.
  *
  *    functional purpose: convert On-hand Qty using Interface
  *
  *    history:
  *
  *    version    date              author             description
  ********************************************************************
  *    1.0        04-sep-2011      k.Tavva           initial development.
  *    1.10        4-MAR-2013      S. Spivey         BOD update should only effect acct alias receipts
  *******************************************************************/
  PROCEDURE print_debug(
      p_print_str IN VARCHAR2);
  PROCEDURE validations;
  PROCEDURE process_statements;
  PROCEDURE itemonhand_conv_proc(
      errbuf OUT VARCHAR2,
      retcode OUT VARCHAR2,
      p_validate_only IN VARCHAR2);
  PROCEDURE update_born_on_date(
      errbuf OUT VARCHAR2 ,
      retcode OUT VARCHAR2 ,
      i_trx_date        IN VARCHAR2 ,
      i_organization_id IN NUMBER);
END xxwc_inv_onhand_conv_pkg;
/
