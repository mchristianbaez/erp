CREATE OR REPLACE PACKAGE apps.xxwc_ahh_conversion_pkg AUTHID CURRENT_USER AS
  /*************************************************************************
    *   PROCEDURE Name: xxwc_cash_file_upload_prc
    *
    *   PURPOSE:   AR Receipt file upload from apex

  HISTORY
    ===============================================================================
    VERSION DATE          AUTHOR(S)              DESCRIPTION
    ------- -----------   ---------------  -----------------------------------------
    1.0     05/07/2018    Nancy Pahwa      TMS#20180308-00301   AHH conversion
   *****************************************************************************/
  PROCEDURE xxwc_ahh_conv_upload_prc(p_filename     IN VARCHAR2,
                                      p_directory    IN VARCHAR2,
                                      p_location     IN VARCHAR2 DEFAULT NULL,
                                      p_new_filename OUT VARCHAR2);


END xxwc_ahh_conversion_pkg;
/