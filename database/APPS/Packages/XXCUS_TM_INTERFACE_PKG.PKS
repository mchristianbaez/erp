CREATE OR REPLACE PACKAGE APPS.xxcus_tm_interface_pkg IS

  /*******************************************************************************
  * Procedure:
  * Description: Package will load data for APEX Common for Location Directory
  *              in Oracle to create the Branches as an internal Customer.
  *              A Party relationship will be created betwenn the LOB and the FRU.
  *
  *              Suppliers, Customers, Products and Purchase Receipts will be loaded
  *              from the Data Warehouse.  DW will load the data into staging table
  *              where Oracle will verify data and load into Oracle to be used for
  *              Vendor Rebates
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     01/27/2010    Kathy Poling    Initial creation of the procedure
  1.1     07/22/2010    Kathy Poling    Revised to get DW data from APXCMMN and
                                        the tables will be updated by delta feeds
                                        instead of a snapshot.
  1.2     01/13/2013    Kathy Poling    RFC 36582 added procedures
  1.3     04/30/2013    Kathy Poling    SR202000 Creating procedure to item and
                                        category interface.
  1.4     08/26/2013    Kathy Poling
  1.5     10/16/2014    Bala Seshadri   Add routine sla_exceptions [ ESMS 265235 ]
  1.6     09/26/2016    Bala Seshadri   Add routine beforereport and afterreport [ ESMS 449491 / TMS 20160711-00227 ]   
  ********************************************************************************/
  --
  g_audit_email_id varchar2(60) :=Null; --Ver 1.6
  g_audit_report_file varchar2(60) :=Null; --Ver 1.6
  g_week_dates varchar2(60) :=Null; --Ver 1.6
  --
  PROCEDURE get_gl_segments(p_bu_nm        IN VARCHAR2
                           ,p_brnch_cd     IN VARCHAR2
                           ,x_prod_segment OUT VARCHAR2
                           ,x_loc_segment  OUT VARCHAR2);

  PROCEDURE load_branch_location(errbuf OUT VARCHAR2, retcode OUT NUMBER);

  PROCEDURE load_rebate_vndr(errbuf OUT VARCHAR2, retcode OUT NUMBER);

  PROCEDURE load_rebate_product(errbuf    OUT VARCHAR2
                               ,retcode   OUT NUMBER
                               ,p_fperiod IN VARCHAR2
                               ,p_bu_nm   IN VARCHAR2);

  /* PROCEDURE load_rebate_receipts_grp(errbuf    OUT VARCHAR2
                                      ,retcode   OUT NUMBER
                                      ,p_fperiod IN VARCHAR2);
  */

  PROCEDURE load_categories_apex(errbuf OUT VARCHAR2, retcode OUT NUMBER);

  PROCEDURE oneoff_rebate_products(errbuf OUT VARCHAR2, retcode OUT NUMBER);

  PROCEDURE oneoff_rebate_receipts(errbuf      OUT VARCHAR2
                                  ,retcode     OUT NUMBER
                                  ,p_rebt_vndr IN VARCHAR2 DEFAULT '%'
                                  ,p_bu_nm     IN VARCHAR2 DEFAULT '%'
                                  ,p_fperiod   IN VARCHAR2);

  PROCEDURE create_valueset;

  PROCEDURE uom_exceptions(p_bu_nm IN VARCHAR2); --8/26/13 added parm

  --RFC 36582
  PROCEDURE receipt_exceptions(errbuf       OUT NOCOPY VARCHAR2
                              ,retcode      OUT NOCOPY NUMBER
                              ,p_start_date IN DATE
                              ,p_end_date   IN DATE
                              ,p_low        IN NUMBER
                              ,p_high       IN NUMBER
                              ,p_bu_nm      IN VARCHAR2);

  PROCEDURE product_exceptions(p_bu_nm IN VARCHAR2);

  PROCEDURE load_rebate_receipts(errbuf    OUT VARCHAR2
                                ,retcode   OUT NUMBER
                                ,p_fperiod IN VARCHAR2
                                ,p_bu_nm   IN VARCHAR2);

  PROCEDURE uc4_branch(errbuf OUT VARCHAR2, retcode OUT NUMBER);

  PROCEDURE uc4_vendors(errbuf OUT VARCHAR2, retcode OUT NUMBER);

  PROCEDURE uc4_products(errbuf    OUT VARCHAR2
                        ,retcode   OUT NUMBER
                        ,p_fperiod IN VARCHAR2
                        ,p_bu_nm   IN VARCHAR2);

  PROCEDURE uc4_receipts(errbuf    OUT VARCHAR2
                        ,retcode   OUT NUMBER
                        ,p_fperiod IN VARCHAR2
                        ,p_bu_nm   IN VARCHAR2);

  --RFC 36582
  PROCEDURE load_uom_mapping(p_bu_nm IN VARCHAR2); --RFC 38031 9/5/13 added parm

  PROCEDURE item_cat_iface_delete(errbuf OUT VARCHAR2, retcode OUT NUMBER);

  PROCEDURE update_site_use(p_party_site_use_id IN NUMBER
                           ,p_status            IN VARCHAR2
                           ,p_party_site_id     IN NUMBER
                           ,p_party_site_number IN VARCHAR2);

  --9/27/13
  FUNCTION get_branch(p_bu_nm    IN VARCHAR2
                     ,p_brnch_cd IN VARCHAR2
                     ,p_lob_nm   IN VARCHAR2) RETURN NUMBER;

  --10/28/13
  FUNCTION get_offer_type(p_cust_acct_id IN NUMBER) RETURN VARCHAR2;

  PROCEDURE submit_receipt_exceptions(errbuf           OUT VARCHAR2
                                     ,retcode          OUT NUMBER
                                     ,p_start_date     IN DATE
                                     ,p_end_date       IN DATE
                                     ,p_low            IN NUMBER
                                     ,p_high           IN NUMBER
                                     ,p_bu_nm          IN VARCHAR2
                                     ,p_user           IN NUMBER
                                     ,p_responsibility IN VARCHAR2);

  --01/27/14
  PROCEDURE pull_sdw(errbuf    OUT VARCHAR2
                    ,retcode   OUT NUMBER
                    ,p_load    IN VARCHAR2
                    ,p_fperiod IN VARCHAR2
                    ,p_bu_nm   IN VARCHAR2);
  PROCEDURE sla_exceptions 
   (
     retcode         out varchar2
    ,errbuf          out varchar2
    ,p_fiscal_period in  varchar2
   );
  --1/21/2014
  PROCEDURE submit_request_set(errbuf    OUT VARCHAR2
                              ,retcode   OUT NUMBER
                              ,p_fperiod IN VARCHAR2
                              ,p_user_id IN NUMBER);
 --
 function afterreport return boolean; --Ver 1.6
 --
 function beforereport return boolean; --Ver 1.6
 --
END xxcus_tm_interface_pkg;
/