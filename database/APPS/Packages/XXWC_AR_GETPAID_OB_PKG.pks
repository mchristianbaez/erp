CREATE OR REPLACE PACKAGE XXWC_AR_GETPAID_OB_PKG AS

  /********************************************************************************
  FILE NAME: XXWC_AR_GETPAID_OB_PKG.pkg

  PROGRAM TYPE: PL/SQL Package

  PURPOSE: Package to create Log Files for GetPaid interfaces

  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     10/20/2012    Gopi Damuluri    Initial creation of the procedure
  1.1     12/10/2012    Gopi Damuluri    Added new func - GET_BILL_TO_ADDR
  ********************************************************************************/

PROCEDURE create_log_file (p_errbuf           OUT     VARCHAR2,
               p_retcode          OUT     NUMBER,
               p_view_name         IN     VARCHAR2,
               p_directory_path    IN     VARCHAR2,
               p_file_name         IN     VARCHAR2,
               p_org_name          IN     VARCHAR2
              );

FUNCTION get_inv_source(p_customer_trx_id IN NUMBER
                      , p_org_id          IN NUMBER)
                 RETURN VARCHAR2;

FUNCTION get_credit_limit(p_customer_trx_id IN NUMBER
                        , p_org_id          IN NUMBER)
                 RETURN NUMBER;

FUNCTION get_flexnum3(p_org_id          IN NUMBER
                    , p_trx_class       IN VARCHAR2
                    , p_party_site_number IN VARCHAR2
                    , p_ship_to_site_use_id IN NUMBER
                    , p_location_id     IN NUMBER
                    , p_hdr_attr2       IN VARCHAR2
                    , p_hdr_attr8       IN VARCHAR2)
                   RETURN NUMBER;

FUNCTION get_bill_to_addr(p_org_id                  IN NUMBER
                        , p_cust_acct_id            IN NUMBER
                        , p_bill_to_site_use_id     IN NUMBER)
                   RETURN VARCHAR2;

FUNCTION get_lien_by_state(p_bill_to_site_use_id    IN NUMBER)
                   RETURN VARCHAR2;

END xxwc_ar_getpaid_ob_pkg;
/