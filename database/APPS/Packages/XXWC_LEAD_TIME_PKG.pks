create or replace 
PACKAGE      xxwc_lead_time_pkg
IS
   /*************************************************************************
   *  Copyright (c) 2011 Lucidity Consulting Group
   *  All rights reserved.
   **************************************************************************
   *   $Header xxwc_lead_time_pkg.pks $
   *   Module Name: xxwc_lead_time_pkg.pks
   *
   *   PURPOSE:   This package is used by the conc program WC Calculate and Update Lead time
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        09/01/2011  Vivek Lakaman             Initial Version
   * ************************************************************************/

   -- Main Procedure to calculate and update lead time
   PROCEDURE calculate_update (errbuff OUT VARCHAR2, retcode OUT VARCHAR2);
END xxwc_lead_time_pkg;