CREATE OR REPLACE PACKAGE APPS.XXWC_INV_PUBD_ONHAND_CONV_PKG
AS
/*************************************************************************
  $Header xxwc_inv_pubd_onhand_conv_pkg $
  Module Name: xxwc_inv_pubd_onhand_conv_pkg.pks

  PURPOSE:   TMS#20160107-00119 -convert PUBD On-hand Qty using Interface
             

  REVISIONS:
  Ver        Date        Author                     Description
  ---------  ----------  ---------------         -------------------------
  1.0        03/30/2016  Rakesh Patel            Initial Version
**************************************************************************/
    PROCEDURE print_debug (p_print_str IN VARCHAR2);

    PROCEDURE validations;

    PROCEDURE process_statements;

    PROCEDURE itemonhand_conv_proc (errbuf    OUT VARCHAR2,
                                    retcode   OUT VARCHAR2,
                                    p_validate_only IN VARCHAR2);

    PROCEDURE update_born_on_date (errbuf          OUT VARCHAR2
                             , retcode         OUT VARCHAR2
                             , i_trx_date   IN     VARCHAR2
                             , i_organization_id IN NUMBER);
END XXWC_INV_PUBD_ONHAND_CONV_PKG;
/