CREATE OR REPLACE PACKAGE apps.xxwc_ar_new_account_pkg
AS
/*************************************************************************
  $Header xxwc_ar_new_account_pkg $
  Module Name: xxwc_ar_new_account_pkg.pks

  PURPOSE:   This package is used in the new account creation extension

  REVISIONS:
  Ver        Date        Author                     Description
  ---------  ----------  ---------------         -------------------------
  1.0        03/21/2013  Shankar Hariharan       Initial Version
  1.1        09/23/2014  Pattabhi Avula          TMS# 20141001-00031
**************************************************************************/
   FUNCTION return_phone_number (i_party_id IN NUMBER)
      RETURN VARCHAR;

   PROCEDURE create_account (i_record_id       IN     NUMBER
                           , o_return_status      OUT VARCHAR2
                           , o_return_msg         OUT VARCHAR2);
END xxwc_ar_new_account_pkg;
/


