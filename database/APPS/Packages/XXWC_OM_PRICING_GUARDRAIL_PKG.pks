CREATE OR REPLACE PACKAGE APPS.xxwc_om_pricing_guardrail_pkg
AS

    --Define variables for logging debug messages
   C_LEVEL_UNEXPECTED CONSTANT    NUMBER := 6;
   C_LEVEL_ERROR      CONSTANT    NUMBER := 5;
   C_LEVEL_EXCEPTION  CONSTANT    NUMBER := 4;
   C_LEVEL_EVENT      CONSTANT    NUMBER := 3;
   C_LEVEL_PROCEDURE  CONSTANT    NUMBER := 2;
   C_LEVEL_STATEMENT  CONSTANT    NUMBER := 1;
   C_CURRENT_RUNTIME_LEVEL CONSTANT NUMBER := FND_LOG.G_CURRENT_RUNTIME_LEVEL ;
   C_MODULE_NAME      VARCHAR2(240):= 'XXWC.PLSQL.XXWC_OM_PRICING_GUARDRAIL_PKG';
   C_PGR_STATUS_CURRENT  CONSTANT VARCHAR2(30) := 'CURRENT';
   C_PGR_STATUS_HISTORY  CONSTANT VARCHAR2(30) := 'HISTORY';

   -- Satish U: Defined Global variables to capture Profile values.
   G_OM_PG_HOLD_LU_BOOK    Number  := fnd_profile.VALUE ('XXWC_OM_PG_HOLD_LU_BOOK');
   --G_OM_PG_HOLD_LU_ENTER   Number ;
   G_OM_PG_ACCESS          Varchar2(3) ;
   G_OM_PG_DB_TRIGGER_FLAG Varchar2(1) ;
   G_PRE_BOOK_FLAG         Varchar2(1) := 'N';

   --TYPE Modifier_List_Val_Rec_Type IS RECORD
   G_Pricing_GuardRail_Rec   XXWC_OM_PRICING_GUARDRAIL%ROWTYPE;



PROCEDURE process_om_header (iv_header_id          IN     NUMBER);

PROCEDURE process_om_line (   iv_line_id          IN     NUMBER,
                              iv_item_id          IN     NUMBER,
                              iv_unit_sp          IN     NUMBER,
                              iv_ship_from_org_id IN     NUMBER,
                              iv_pricing_date     IN     VARCHAR,
                              iv_header_id        IN     NUMBER,
                              iv_flow_code        IN     VARCHAR,
                              iv_unit_cost        IN     NUMBER,
                              iv_list_price       IN     NUMBER,
                              iv_order_type_id    IN     NUMBER
   );

PROCEDURE DBTRigger_PGR_Check (iv_line_id        IN     NUMBER,
                              iv_item_id          IN     NUMBER,
                              iv_unit_sp          IN     NUMBER,
                              iv_ship_from_org_id IN     NUMBER,
                              iv_pricing_date     IN     VARCHAR,
                              iv_header_id        IN     NUMBER,
                              iv_flow_code        IN     VARCHAR,
                              iv_unit_cost        IN     NUMBER,
                              iv_list_price       IN     NUMBER,
                              iv_order_type_id    IN     NUMBER,
                              iv_Line_Number      IN     VARCHAR2,
                              iv_Org_ID           IN     NUMBER,
                              iv_Manual_Adj_Exists   IN  Varchar2,
                              iv_Auto_Adj_Exists     IN  Varchar2,
                              x_Return_Status     OUT    Varchar2,
                              x_Msg_Data          OUT    Varchar2
   );

   FUNCTION get_margin_disc_info (iv_info_type           VARCHAR2
                                , iv_item_id             NUMBER
                                , iv_ship_from_org_id    NUMBER
                                , iv_price_zone          VARCHAR2
                                , iv_ordered_date        DATE)
      RETURN NUMBER;

   FUNCTION get_price_zone (iv_org_id NUMBER)
      RETURN VARCHAR;

   FUNCTION get_average_cost (iv_item_id NUMBER, iv_ship_from_org_id NUMBER)
      RETURN NUMBER;

   FUNCTION get_inventory_category (iv_item_id             NUMBER
                                  , iv_ship_from_org_id    NUMBER)
      RETURN NUMBER;

   FUNCTION get_item_price (iv_item_id         NUMBER
                          , iv_national_pl     NUMBER
                          , iv_category_pl     NUMBER
                          , iv_ordered_date    DATE)
      RETURN NUMBER;

   PROCEDURE apply_holds (iv_header_id            NUMBER
                        , iv_line_id              NUMBER
                        , iv_guardrail_hold       NUMBER
                        , ov_return_status    OUT VARCHAR2
                        , ov_return_message   OUT VARCHAR2);

   FUNCTION get_temp_info (iv_info_type VARCHAR2)
      RETURN VARCHAR;

   PROCEDURE import_guardrail_data (iv_pricing_GuardRail_ID In NUMBER,
                                    iv_price_zone           IN VARCHAR2,
                                    iv_item_category        IN VARCHAR2,
                                    iv_item_number          IN VARCHAR2,
                                    iv_min_margin           IN NUMBER,
                                    iv_max_discount         IN NUMBER,
                                    iv_start_date           IN DATE,
                                    iv_end_date             IN DATE );



   Procedure RELEASE_HOLD(p_header_id            IN   NUMBER    ,
                          p_Line_ID              In   Number    ,
                          p_hold_id              IN   NUMBER    ,
                          x_Return_Status        OUT  VARCHAR2  ,
                          x_Msg_Data             OUT VARCHAR2);

   Procedure Update_Status(p_Header_ID   In Number ,
                           P_Line_ID     In Number,
                           p_Return_Status In Varchar2,
                           p_Return_Message in Varchar2) ;

   FUNCTION get_temp_info_Gt(p_Header_ID  In Number,
                           p_Line_ID    In Number,
                           p_info_type VARCHAR2)  RETURN VARCHAR;

   Procedure Delete_temp_info (p_Header_ID  In Number,
                           p_Line_ID    In Number);
   Function  Get_PRicing_Guard_Rail_ID (p_Inventory_Item_ID  In Number,
                                        p_organization_ID    In Number ) Return Varchar2;

   Procedure  Insert_Record ( P_Pricing_GuardRail_Rec  In XXWC_OM_PRICING_GUARDRAIL%ROWTYPE,
                              x_Return_Status          Out NoCopy Varchar2,
                              x_Msg_Data               Out NoCopy Varchar2) ;



   PROCEDURE DB_Trigger_Apply_holds (
                 iv_header_id         IN   NUMBER
               , iv_line_id           IN   NUMBER
               , iv_Org_ID            IN   NUMBER
               , iv_guardrail_hold    IN   NUMBER
               , ov_return_status    OUT VARCHAR2
               , ov_return_message   OUT VARCHAR2);

   PROCEDURE DB_TRIGGER_RELEASE_HOLD(p_header_id IN   NUMBER    ,
                          p_Line_ID              In   Number    ,
                          p_hold_id              IN   NUMBER    ,
                          P_Org_ID               IN   NUMBER    ,
                          x_Return_Status        OUT  VARCHAR2  ,
                          x_Msg_Data             OUT VARCHAR2);

   FUNCTION get_PGR_Message(p_Header_ID  In Number,
                           p_Line_ID    In Number,
                           p_info_type VARCHAR2)  RETURN VARCHAR;

   Procedure Delete_Message (p_Header_ID  In Number,
                           p_Line_ID      In Number);

   Procedure Update_Message(p_Header_ID     In Number ,
                           P_Line_ID        In Number,
                           p_Return_Status  In Varchar2,
                           p_Return_Message in Varchar2) ;

   Function IS_PREBOOK_HOLD_EXIST(p_Header_ID In Number) Return Varchar2;

   PROCEDURE DB_TRIGGER_RELEASE_HOLD2(p_header_id IN   NUMBER    ,
                          p_hold_id              IN   NUMBER    ,
                          P_Org_ID               IN   NUMBER    ,
                          x_Return_Status        OUT  VARCHAR2  ,
                          x_Msg_Data             OUT VARCHAR2);

-- 06/24/2013 CG: TMS 20130618-01074: Added new procedure to programatically release PGR Holds
    PROCEDURE release_pgr_holds (RETCODE     OUT NUMBER
                               , ERRMSG    OUT VARCHAR2
                               , P_APPLIED_AGE  IN NUMBER);
                               
--12/29/2014 SH: TMS 20140723-00285: Added new procedure to programatically release PGR Holds                               
  PROCEDURE PGR_LINE_HOLD_RELEASE(RETCODE     OUT NUMBER
                               , ERRMSG    OUT VARCHAR2);                               

END xxwc_om_pricing_guardrail_pkg;
/