/* Formatted on 7/13/2016 1:34:12 PM (QP5 v5.265.14096.38000) */
CREATE OR REPLACE PACKAGE apps.xxwc_mv_routines_add_pkg
IS
   /********************************************************************************************************************************
        $Header xxwc_mv_routines_add_pkg
        Module Name: xxwc_mv_routines_add_pkg

        PURPOSE: This Package is created for EDW Invoice Line Extract

        REVISIONS:

        Ver        Date         Author          Description
        ---------  ----------   ----------      ------------------------------------------------------------------------------------
        1.0        07/31/2014   Veera C         Modified for TMS#20140710-00426 EDW Invoice Line
                                                Extract Change - Promo SKUs
        2.1        19-APR-2016  Rakesh Patel    TMS-20160407-00197 -Remove Parallelism
        3.0        26-OCT-2015  Kishorebabu V   TMS# 20151023-00102 Create Function XXWC_INV_AVAILABLE
        4.0        13-JUL-2016  P.vamshidhar    TMS#20160708-00172  - Batch improvements for XXWC OM Print Allocations Process
        5.0        11-OCT-2016  P.Vamshidhar    TMS#20160926-00235  - Improving error handing in 'XXWC_MV_ROUTINES_ADD_PKG' Package.

       ******************************************************************************************************************************/
   -- created by Rasikha 3/13/2013
   --used in apps.xxwc_ar_invoice_line_vw

   FUNCTION get_original_modifier (p_header_id NUMBER, p_line_id NUMBER)
      RETURN VARCHAR2;

   --used in apps.xxwc_ar_invoice_line_vw
   FUNCTION get_last_modifier (p_header_id NUMBER, p_line_id NUMBER)
      RETURN VARCHAR2;

   --used in apps.xxwc_ar_invoice_line_vw
   FUNCTION get_overridden_sell_price (p_header_id NUMBER, p_line_id NUMBER)
      RETURN NUMBER;

   --used in apps.xxwc_ar_invoice_line_vw
   FUNCTION get_price_type (p_header_id NUMBER, p_line_id NUMBER)
      RETURN VARCHAR2;

   PROCEDURE populate_xxwc_ar_inv_line_temp (p_from_date    DATE,
                                             p_to_date      DATE);

   PROCEDURE alter_table_temp (p_owner VARCHAR2, p_table_name VARCHAR2);

   PROCEDURE table_create_from_old (p_owner             VARCHAR2,
                                    p_old_table_name    VARCHAR2,
                                    p_new_table_name    VARCHAR2);

   PROCEDURE drop_temp_table (p_owner VARCHAR2, p_table_name VARCHAR2);

   PROCEDURE populate_onhand_mv (p_errbuf    OUT VARCHAR2,
                                 p_retcode   OUT VARCHAR2);

   PROCEDURE populate_get_onhand_qty (p_table_name         VARCHAR2,
                                      p_organization_id    NUMBER);

   PROCEDURE wait_for_jobs;

   /*************************************************************************
     $Header get_sales_unit_price $
     Module Name: get_sales_unit_price

     PURPOSE: This Function created for EDW Invoice Line Extract Chage - Promo SKU's.

     ESMS Task Id : 20140710-00426 EDW Invoice Line Extract Change - Promo SKUs


     REVISIONS:
     Ver        Date        Author          Description
     ---------  ----------  ----------      ----------------
     1.0        07/31/2014  Veera C         TMS#20140710-00426 EDW Invoice Line Extract Change - Promo SKUs
    **************************************************************************/
   FUNCTION get_sales_unit_price (p_cost_type   IN VARCHAR2,
                                  p_line_id     IN NUMBER)
      RETURN NUMBER;

   -- Added below 2 procedures by Rakesh Patel in 2.1 Version.

   PROCEDURE enable_parallelism(p_package_name IN VARCHAR2, p_comments IN VARCHAR2);  -- Added parameters by Vamshi in Rev 5.0

   PROCEDURE disable_parallelism(p_package_name IN VARCHAR2, p_comments IN VARCHAR2); -- Added parameters by Vamshi in Rev 5.0

   /* Ver# 3.0  20151023-00102 Create Function XXWC_INV_AVAILABLE  */
   /* Function is created to retrieve the ONHAND QUANTITY in the 'GENERAL' subinv*/
   FUNCTION xxwc_inv_available (p_organization_id     IN NUMBER,
                                p_inventory_item_id   IN NUMBER,
                                p_subinventory        IN VARCHAR2)
      RETURN NUMBER;

   /*Ver# 4.0  Added new Procedure to submit dbms job by Vamshi.*/
   PROCEDURE SUBMIT_PROGRAM (p_statement       IN VARCHAR2,
                             p_schedule_date   IN DATE);
END;                                                           -- Package spec
/

-- Grants for Package
GRANT EXECUTE ON apps.xxwc_mv_routines_add_pkg TO interface_prism
/
GRANT EXECUTE ON apps.xxwc_mv_routines_add_pkg TO xxeis
/
GRANT EXECUTE ON apps.xxwc_mv_routines_add_pkg TO interface_xxcus
/
GRANT EXECUTE ON apps.xxwc_mv_routines_add_pkg TO xxwc
/

-- End of DDL Script for Package APPS.XXWC_MV_ROUTINES_ADD_PKG