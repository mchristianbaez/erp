CREATE OR REPLACE PACKAGE APPS.xxwc_rental_engine_pkg
AS
--Define variables for logging debug messages
   g_level_unexpected        CONSTANT NUMBER         := 6;
   g_level_error             CONSTANT NUMBER         := 5;
   g_level_exception         CONSTANT NUMBER         := 4;
   g_level_event             CONSTANT NUMBER         := 3;
   g_level_procedure         CONSTANT NUMBER         := 2;
   g_level_statement         CONSTANT NUMBER         := 1;
   g_resp_name                        VARCHAR2 (100)
                                           := fnd_profile.VALUE ('RESP_NAME');
   g_resp_id                          NUMBER := fnd_profile.VALUE ('RESP_ID');
   g_user_id                          NUMBER := fnd_profile.VALUE ('USER_ID');
   g_current_runtime_level   CONSTANT NUMBER
                                           := fnd_log.g_current_runtime_level;
   g_module_name             CONSTANT VARCHAR2 (60)
                                            := 'PLSQL.XXWC_RENTAL_ENGINE_PKG';
   g_oeh_attr1_long_term              VARCHAR2 (40)  := 'LONG TERM';
   g_oeh_attr1_short_term             VARCHAR2 (40)  := 'SHORT TERM';
   g_oel_bill_only                    VARCHAR2 (40)  := 'BILL ONLY';
   g_oel_wc_bill_only                 VARCHAR2 (40)  := 'WC BILL ONLY';
   g_oel_credit_only                  VARCHAR2 (40)  := 'WC CREDIT ONLY';
   g_return_reason_code               VARCHAR2 (40)  := 'RETURN_RETURN';
   g_ret_with_receipt                 VARCHAR2 (40)  := 'RETURN WITH RECEIPT';
   g_mtl_transaction_type             VARCHAR2 (40)  := 'SALES ORDER ISSUE';
   g_mtl_subinventory_code            VARCHAR2 (40)  := 'STAGING';
   g_mtl_source_code                  VARCHAR2 (40)  := 'ORDER ENTRY';
   g_rental_hold                      VARCHAR2 (200)
                                                := 'XXWC RENTAL INVOICE HOLD';
   g_rma_trxn_type                    VARCHAR2 (40)  := 'RMA RECEIPT';
   g_rma_source_code                  VARCHAR2 (40)  := 'RCV';
   g_lost_sub_inv                     VARCHAR2 (40)  := 'LOSTRENTAL';
   g_dfr_sub_inv                      VARCHAR2 (40)  := 'DRRENTAL';
   -- 07/24/2012 CG: added new rental sale subinv
   g_rent_sale_sub_inv                VARCHAR2 (40)  := 'RENTALSALE';
   /* Damaged but Repairable */
   g_dbr_sub_inv                      VARCHAR2 (40)  := 'DBRENTAL';
   /* Damaged beyond Repair */
   g_rental_sub_inv                   VARCHAR2 (40)  := 'RENTAL';
   g_rental_nr_sub_inv                VARCHAR2 (40)  := 'RENTAL N/R';
   -- SatishU: 29-JUN-2012
   g_zero_unit_price_flag             VARCHAR2 (1)   := 'N';

   TYPE reservation_int_rec IS RECORD (
      v_reservation_interface_id   NUMBER,
      v_reservation_batch_id       NUMBER,
      v_requirement_date           DATE,
      v_organization_id            NUMBER,
      v_inventory_item_id          NUMBER,
      v_demand_source_type_id      NUMBER,
      v_demand_source_header_id    NUMBER,
      v_demand_source_line_id      NUMBER,
      v_reservation_uom_code       VARCHAR2 (3),
      v_reservation_quantity       NUMBER,
      v_subinventory_code          VARCHAR2 (10),
      v_serial_number              VARCHAR2 (30),
      v_supply_source_type_id      NUMBER,
      v_row_status_code            NUMBER,
      v_reservation_action_code    NUMBER,
      v_transaction_mode           NUMBER,
      v_last_update_date           DATE,
      v_last_updated_by            NUMBER,
      v_creation_date              DATE,
      v_created_by                 NUMBER
   );

   PROCEDURE main (
      errbuf           OUT      VARCHAR2,
      retcode          OUT      NUMBER,
      p_order_number   IN       VARCHAR2,
      p_log_enabled    IN       VARCHAR2 DEFAULT 'N'
   );

   PROCEDURE create_rma_line (
      p_header_id            IN              NUMBER,
      p_rma_line_id          IN              NUMBER,
      p_unit_selling_price   IN              NUMBER,
      p_inventory_item_id    IN              NUMBER,
      p_rma_qty              IN              NUMBER,
      p_ship_from_org_id     IN              NUMBER,
      --  p_Log_Enabled        IN VARCHAR2,
      x_return_status        OUT NOCOPY      VARCHAR2
   );

   PROCEDURE create_charge_line (
      p_header_id                IN              NUMBER,
      p_charge_line_id           IN              NUMBER,
      p_inventory_item_id        IN              NUMBER,
      p_qty                      IN              NUMBER,
      p_bill_cycle               IN              NUMBER,
      p_billing_days             IN              NUMBER,
      p_ship_from_org_id         IN              NUMBER,
      p_rental_billing_term      IN              VARCHAR2,
      p_override_rental_charge   IN              NUMBER,
      p_unit_list_price          IN              NUMBER,
      p_return_subinv            IN              VARCHAR2,
      p_rcv_quantity             IN              NUMBER,
      p_rma_line_id              IN              NUMBER,
      p_rma_status               IN              VARCHAR2,
      p_apply_hold_flag          IN              VARCHAR2,
      lx_return_status           OUT NOCOPY      VARCHAR2,
      lx_charge_line_id          OUT NOCOPY      NUMBER
   );

   PROCEDURE get_rma_details (
      p_header_id     IN              NUMBER,
      p_line_id       IN              NUMBER,
      p_log_enabled   IN              VARCHAR2,
      x_rma           OUT NOCOPY      NUMBER,
      x_rma_status    OUT NOCOPY      VARCHAR2
   );

   PROCEDURE get_billing_cycle (
      p_actual_shipment_date   IN              DATE,
      p_line_id                IN              NUMBER,
      p_rma_status             IN              VARCHAR2,
      p_rental_terms           IN              VARCHAR2,
      x_billing_cycle          OUT NOCOPY      NUMBER,
      x_billing_days           OUT NOCOPY      NUMBER,
      x_bill                   OUT NOCOPY      VARCHAR2
   );

   PROCEDURE get_return_subinventory (
      p_header_id              IN              NUMBER,
      p_line_id                IN              NUMBER,
      p_actual_shipment_date   IN              DATE,
      p_any_chr_bill_cycle     IN              NUMBER,
      x_return_subinv          OUT NOCOPY      VARCHAR2,
      x_return_date            OUT NOCOPY      DATE,
      x_rental_days            OUT             NUMBER,
      x_rcv_quantity           OUT NOCOPY      NUMBER
   );

   PROCEDURE get_return_subinventory (
      p_header_id              IN              NUMBER,
      p_line_id                IN              NUMBER,
      p_actual_shipment_date   IN              DATE,
      x_return_subinv          OUT NOCOPY      VARCHAR2,
      x_return_date            OUT NOCOPY      DATE,
      x_rental_days            OUT NOCOPY      NUMBER,
      x_rcv_quantity           OUT NOCOPY      NUMBER
   );

   FUNCTION get_return_unit_price (
      p_header_id          IN   NUMBER,
      p_line_id            IN   NUMBER,
      p_billing_terms      IN   VARCHAR2,
      p_charge_item        IN   NUMBER,
      p_rental_item        IN   NUMBER,
      p_unit_price         IN   NUMBER,
      p_ship_from_org_id   IN   NUMBER,
      p_rentaldays         IN   NUMBER,
      p_return_subinv      IN   VARCHAR2
   )
      RETURN NUMBER;

   PROCEDURE apply_hold_charge_line (
      p_header_id            IN              NUMBER,
      p_charge_line_id       IN              NUMBER,
      l_hold_return_status   OUT NOCOPY      VARCHAR2
   );

   PROCEDURE log_msg (
      p_debug_level   IN   NUMBER,
      p_mod_name      IN   VARCHAR2,
      p_debug_msg     IN   VARCHAR2
   );

   FUNCTION get_wc_trxn_id (p_trxtype IN VARCHAR2)
      RETURN NUMBER;

   FUNCTION is_item_serialized (
      p_line_id                 IN   NUMBER,
      p_inventory_item_id       IN   NUMBER,
      p_ship_from_org_id        IN   NUMBER,
      p_mtl_transaction_type    IN   VARCHAR2,
      p_mtl_subinventory_code   IN   VARCHAR2,
      p_mtl_source_code         IN   VARCHAR2
   )
      RETURN BOOLEAN;

   FUNCTION get_charge_item (p_inventory_item_id IN NUMBER)
      RETURN NUMBER;

   FUNCTION get_charge_item (
      p_inventory_item_id   IN   NUMBER,
      p_ship_from_org_id    IN   NUMBER
   )
      RETURN NUMBER;

   FUNCTION get_charge_item_unit_price (
      p_charge_item      IN   NUMBER,
      p_charge_line_id   IN   NUMBER
   )
      RETURN NUMBER;

   FUNCTION get_rma_qty (p_header_id IN NUMBER, p_charge_line_id IN NUMBER)
      RETURN NUMBER;

   FUNCTION get_chr_line_attr9_split_rma (p_line_id IN NUMBER)
      RETURN NUMBER;

   FUNCTION get_item_description (p_line_id IN NUMBER)
      RETURN VARCHAR;

   PROCEDURE create_reservation_line (
      v_reservation_record   IN              reservation_int_rec,
      p_rma_line_id          IN              NUMBER,
      p_inventory_item_id    IN              NUMBER,
      p_return_subinv        IN              VARCHAR2,
      lx_return_status       OUT NOCOPY      VARCHAR2
   );

   FUNCTION get_mtl_sales_order_id (p_order_num IN NUMBER)
      RETURN NUMBER;
END xxwc_rental_engine_pkg;
/


