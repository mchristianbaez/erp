--
-- XXWCAR_TAXWARE_PKG  (Package) 
--
CREATE OR REPLACE PACKAGE apps.xxwcar_taxware_pkg
IS
   /**************************************************************************
   File Name: XXWCAR_TAXWARE_PKG

   PROGRAM TYPE: SQL Script

   PURPOSE:      package Specification is used to load invoice transaction
                 data to capture the original customer attributes for tax
                 audit

   HISTORY
   =============================================================================
          Last Update Date : 07/17/2012
   =============================================================================
   =============================================================================
   VERSION DATE          AUTHOR(S)          DESCRIPTION
   ------- -----------   -----------------  ------------------------------------
   1.0     17-Jul-2012   Kathy Poling       Initial creation of the procedure
                                            Service Ticket 151718 RFC 34124
   1.1     09-Aug-2012   Kathy Poling       Enhancement adding columns for DocLink
                                            Service Ticket 160659
   =============================================================================
   *****************************************************************************/

   PROCEDURE load_audit (errbuf OUT VARCHAR2, retcode OUT NUMBER);

   FUNCTION get_trx_salesrep_num (p_trx_salesrep_id       IN NUMBER
                                 ,p_bill_to_salesrep_id   IN NUMBER
                                 ,p_org_id                IN NUMBER)
      RETURN VARCHAR2;
END xxwcar_taxware_pkg;
/

