/* Formatted on 3/6/2017 1:27:57 PM (QP5 v5.256.13226.35538) */
CREATE OR REPLACE PACKAGE APPS.XXWC_TAXWARE_TAX_PKG
AS
   /********************************************************************************************************************************
   -- Package XXWC_TAXWARE_TAX_PKG
   -- ******************************************************************************************************************************
   --
   -- PURPOSE: Package is to caliculate tax
   -- HISTORY
   -- ==============================================================================================================================
   -- ==============================================================================================================================
   -- VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- ----------------------------------------------------------------------------------------
   -- 1.0     05-Mar-2017   P.Vamshidhar    TMS#20170306-00204  - Tax  - Custom Wrapper Package to Caliculate Order Line tax .
                                            Package Created.
   *********************************************************************************************************************************/
   PROCEDURE caliculate_tax (p_tax40_grossamt          IN     NUMBER, --  := '123.00';
                             p_tax40_calctype          IN     VARCHAR2, --  := 'G';
                             p_tax40_numitems          IN     NUMBER, --  := 1;
                             p_tax40_prodcode          IN     VARCHAR2, --  := 'PCODE';
                             p_tax40_invoicedate       IN     DATE, --  := '02-MAR-2017';
                             p_tax40_companyid         IN     VARCHAR2, --  := '01';
                             p_tax40_custno            IN     VARCHAR2, --  := '11381000';
                             p_tax40_invoiceno         IN     VARCHAR2, --  := 'ABCDE';
                             p_tax40_usenexproind      IN     VARCHAR2, --  := 'Y';
                             p_tax40_critflg           IN     VARCHAR2, --  := 'R';
                             p_tax40_usestep           IN     VARCHAR2, --  := 'Y';
                             p_tax40_stepprocflg       IN     NUMBER, --  := 1;
                             p_Jur40_ShipFr_State      IN     VARCHAR2, --  := 'NY';
                             p_Jur40_ShipFr_City       IN     VARCHAR2, --  := 'NEW YORK';
                             p_Jur40_ShipFr_Zip        IN     VARCHAR2, --  := '10001';
                             p_Jur40_ShipFr_Geo        IN     VARCHAR2, --  := '00';
                             p_Jur40_ShipTo_State      IN     VARCHAR2, --  := 'NY';
                             p_Jur40_ShipTo_City       IN     VARCHAR2, --  := 'NEW YORK';
                             p_Jur40_ShipTo_Zip        IN     VARCHAR2, --  := '10001';
                             p_Jur40_ShipTo_Geo        IN     VARCHAR2, --  := '00';
                             p_Jur40_POA_State         IN     VARCHAR2, --  := 'NY';
                             p_Jur40_POA_City          IN     VARCHAR2, --  := 'NEW YORK';
                             p_Jur40_POA_Zip           IN     VARCHAR2, --  := '10001';
                             p_Jur40_POA_Geo           IN     VARCHAR2, --  := '00';
                             p_Jur40_POO_State         IN     VARCHAR2, --  := 'NY';
                             p_Jur40_POO_City          IN     VARCHAR2, --  := 'NEW YORK';
                             p_Jur40_POO_Zip           IN     VARCHAR2, --  := '10001';
                             p_Jur40_POO_Geo           IN     VARCHAR2, --  := '00';
                             x_return_status              OUT VARCHAR2,
                             x_return_mess                OUT VARCHAR2,
                             x_federal_tax_amt            OUT NUMBER,
                             x_state_tax_amt              OUT NUMBER,
                             x_county_tax_amt             OUT NUMBER,
                             x_local_tax_amt              OUT NUMBER,
                             x_second_st_tax_amt          OUT NUMBER,
                             x_second_county_tax_amt      OUT NUMBER,
                             X_second_local_tax_amt       OUT NUMBER,
                             x_fedtxrate                  OUT NUMBER,
                             x_sttx_rate                  OUT NUMBER,
                             x_cntxrate                   OUT NUMBER,
                             x_lotxrate                   OUT NUMBER,
                             x_scsttxrate                 OUT NUMBER,
                             x_sccntxrate                 OUT NUMBER,
                             x_sclotxrate                 OUT NUMBER);
END XXWC_TAXWARE_TAX_PKG;
/
GRANT EXECUTE ON APPS.XXWC_TAXWARE_TAX_PKG TO INTERFACE_DF;
/