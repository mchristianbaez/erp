CREATE OR REPLACE PACKAGE APPS.XXWC_MISC_RECEIPT_IFACE_PKG
AS
  /*************************************************************************
  *  Copyright (c) 2011 Lucidity Consulting Group
  *  All rights reserved.
  **************************************************************************
  *   $Header XXXWC_MISC_RECEIPT_IFACE_PKGpks $
  *   Module Name: XXXWC_MISC_RECEIPT_IFACE_PKG.sql
  *
  *   PURPOSE:   This package is for Creating Interface records for Misc Receipts
  *
  *   REVISIONS:
  *   Ver        Date        Author                     Description
  *   ---------  ----------  ---------------         -------------------------
  *   1.0        12/09/2011  Srinivas Malkapuram      Initial Version
  *   2.0       4/29/2013   Ram Talluri              Made code changes to support rental mass transfer process TMS #20130218-01185
  * ***************************************************************************/
  --G_ISSUE_TRANSACTION_TYPE   VARCHAR2(100)         := FND_PROFILE.VALUE('XXWC_RENTAL_TRX_TYPE_NAME'); --'Rental Asset Miscellaneous issue'; changed on 1/18/12 due to DFF naming convention
  G_RENTAL_SUBINV_CODE       VARCHAR2(40)          := FND_PROFILE.VALUE('XXWC_RENTAL_SUBINV_CODE');
  G_TRXN_ACTION_LOOKUP       VARCHAR2(100)         := 'MTL_TRANSACTION_ACTION';
  G_TRANSACTION_ACTION       VARCHAR2(100)         := 'SUBINVENTORY TRANSFER';
  G_RECEIPT_TRANSACTION_TYPE VARCHAR2(100)         := 'Miscellaneous receipt';
  G_TRANSACTION_SOURCE       VARCHAR2(100)         := 'INVENTORY';
  G_USER_ID                  NUMBER                := FND_PROFILE.VALUE('USER_ID');
  G_MODULE_NAME              CONSTANT VARCHAR2(60) := 'PLSQL.XXWC_MISC_RECEIPT_IFACE_PKG';
  G_LEVEL_PROCEDURE          NUMBER DEFAULT Fnd_Log.LEVEL_PROCEDURE;
  -- Satish U : Defined new Variable to capture Transaction Type ID
  G_ISSUE_TRANSACTION_TYPE_ID   Number         := FND_PROFILE.VALUE('XXWC_RENTAL_TRX_TYPE_NAME'); --'Rental Asset Miscellaneous issue'; changed on 1/18/12 due to DFF naming convention


Type serial_type
IS
  Record
  (
    transaction_interface_id NUMBER,
    source_code              VARCHAR2(30),
    fm_serial_number         VARCHAR2(30),
    to_serial_number         VARCHAR2(30) ,
    last_update_date DATE,
    last_updated_by NUMBER,
    creation_date DATE,
    created_by NUMBER);
PROCEDURE MAIN
  (
    errbuf OUT VARCHAR2,
    retcode OUT NUMBER,
    p_Log_Enabled IN VARCHAR2 DEFAULT 'N' );
PROCEDURE Serial_Interface
  (
    serial_rec IN serial_type,
    x_return_status OUT nocopy VARCHAR2,
    x_result_out OUT nocopy    VARCHAR2 );
  FUNCTION GET_RECEIPT_TRAN_TYPE_ID
    RETURN NUMBER;
  FUNCTION GET_TRANSACTION_SOURCE_ID
    RETURN NUMBER;
  FUNCTION GET_TRANSACTION_ACTION_ID
    RETURN NUMBER;
  FUNCTION GET_SERIAL_NUMBER
    (
      p_Transfer_Trx_ID IN NUMBER )
    RETURN VARCHAR2;
  FUNCTION IS_EXIST
    (
      P_TRANSACTION_ID    IN NUMBER,
      P_INVENTORY_ITEM_ID IN NUMBER,
      P_ORGANIZATION_ID   IN NUMBER )
    RETURN BOOLEAN;
  FUNCTION IS_ITEM_SERIALIZED
    (
      --P_INVENTORY_ITEM_ID IN NUMBER, --Commented by Ram Talluri 4/29/2013 TMS 20130218-01185
      P_ATTRIBUTE12 IN VARCHAR2,--Added by Ram Talluri 4/29/2013 TMS 20130218-01185. This function will now read the rental part number value from DFF.
      P_ORGANIZATION_ID   IN NUMBER)
    RETURN BOOLEAN;

  FUNCTION GET_RENTAL_INV_ITEM_ID ( --P_INVENTORY_ITEM_ID IN NUMBER, --Commented by Ram Talluri 4/29/2013 TMS 20130218-01185
                                    p_attribute12 IN VARCHAR2,--Added by Ram Talluri 4/29/2013 TMS 20130218-01185. This function will now read the rental part number value from DFF.
                                    p_organization_id IN NUMBER,
                                    p_transaction_id IN NUMBER)
  RETURN NUMBER;

    RETURN BOOLEAN;

  END ;
/