CREATE OR REPLACE PACKAGE APPS.xxwc_safety_stock_all_orgs_pkg
AS
/******************************************************************************
   $Header XXWC_SAFETY_STOCK_ALL_ORGS_PKG$
   NAME:       XXWC_SAFETY_STOCK_ALL_ORGS_PKG

   PURPOSE:    Automate Generate Safety Stock program for all organizations - TMS# 20131120-00056


   REVISIONS:
   Ver        Date         Author            Description
   ---------  -----------  ---------------   ------------------------------------
   1.0        23-APR-2013  Dheeresh Chintala  Initial version TMS# 20131120-00056
   1.1        03-SEP-2014  Manjula Chellappan Added parameter Horizon_date in Main procedure
					      for TMS # 20140903-00150

******************************************************************************/

   /*************************************************************************
          Procedure : Main

          PURPOSE:   Automate Generate Safety Stock program for all organizations
          Parameter:

     REVISIONS:
     Ver        Date         Author            Description
     ---------  -----------  ---------------   ------------------------------------
     1.0        23-APR-2013  Dheeresh Chintala  Initial version TMS# 20131120-00056
     1.1        03-SEP-2014  Manjula Chellappan Added parameter Horizon_date for TMS # 20140903-00150
    ************************************************************************/
   PROCEDURE main (
      retcode          OUT      NUMBER,
      errbuf           OUT      VARCHAR2,
      p_region         IN       VARCHAR2,
--Added the parameter by Manjula on 03-Sep-14 for TMS # 20140903-00150
      p_horizon_date   IN       VARCHAR2
   );
END xxwc_safety_stock_all_orgs_pkg;
/
