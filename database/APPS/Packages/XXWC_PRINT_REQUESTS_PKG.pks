CREATE OR REPLACE 
PACKAGE        APPS.XXWC_PRINT_REQUESTS_PKG AS

/******************************************************************************
   NAME:       XXWC_PRINT_REQUESTS_PKG

   PURPOSE:    Package to truncate data from the Printer Form tables that are no longer needed.  
   

   REVISIONS:   
   Ver        Date         Author           Description
   ---------  -----------  ---------------  ------------------------------------
   1.0        29-JUL-2013  Lee Spitzer       1. Create the PL/SQL Package
******************************************************************************/
   --Define variables for logging debug messages
   g_level_unexpected CONSTANT   NUMBER := 6;
   g_level_error CONSTANT        NUMBER := 5;
   g_level_exception CONSTANT    NUMBER := 4;
   g_LEVEL_EVENT CONSTANT        NUMBER := 3;
   g_LEVEL_PROCEDURE CONSTANT    NUMBER := 2;
   g_LEVEL_STATEMENT CONSTANT    NUMBER := 1;
   
   g_package_name CONSTANT       VARCHAR2(30) := 'XXWC_PRINT_REQUESTS_PKG';
   g_name                        VARCHAR2(30);
   g_program_name                VARCHAR2(60);
   g_exception                   EXCEPTION;
   g_user_id                     NUMBER := FND_GLOBAL.USER_ID;
   g_resp_id                     NUMBER := FND_GLOBAL.RESP_ID;
   g_resp_appl_id                NUMBER := FND_GLOBAL.RESP_APPL_ID;
   g_message                     VARCHAR2(2000);
   g_count                       NUMBER DEFAULT 0;
   
/*

  PROCEDURE DELETE_TEMP_TABLES
    No parameters required
    This program truncates the tables xxwc.xxwc_print_requests_temp and xxwc.xxwc_print_requests_arg_temp tables
    Tables should be purged weekly on Sundays
*/
PROCEDURE  DELETE_TEMP_TABLES
              (retbuf                         OUT VARCHAR2
              ,retcode                        OUT NUMBER
              );

END XXWC_PRINT_REQUESTS_PKG;