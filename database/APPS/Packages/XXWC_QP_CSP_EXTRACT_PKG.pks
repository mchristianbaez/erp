CREATE OR REPLACE PACKAGE XXWC_QP_CSP_EXTRACT_PKG AS
   /*************************************************************************
   *   $Header xxwc_qp_csp_extract_pkg.PKG $
   *   Module Name: xxwc_qp_csp_extract_pkg.PKG
   *
   *   PURPOSE:   This package is used to extract CSP Information
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        04/09/2014  Gopi Damuluri             Initial Version   
   * ***************************************************************************/
   PROCEDURE LOAD_STAGING (p_errbuf                 OUT  VARCHAR2,
                           p_retcode                OUT  NUMBER,
                           p_directory_path          IN  VARCHAR2,
                           p_file_name               IN  VARCHAR2,
                           p_user_name               IN  VARCHAR2);

PROCEDURE CREATE_FILE (p_errbuf           OUT     VARCHAR2,
               p_retcode          OUT     NUMBER,
               p_interface_name    IN     VARCHAR2,
               p_view_name         IN     VARCHAR2,
               p_directory_path    IN     VARCHAR2,
               p_file_name         IN     VARCHAR2,
               p_org_name          IN     VARCHAR2
              );

END xxwc_qp_csp_extract_pkg;
/