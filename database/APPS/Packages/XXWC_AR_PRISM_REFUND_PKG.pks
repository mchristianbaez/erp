create or replace package apps.xxwc_ar_prism_refund_pkg
as
    /***************************************************************************
       *    script name: xxwc_prism_refund_pkg
       *
       *    interface name: Prism Refunds
       *
       *    functional purpose:Match the refund debit memo to return credit memo
       *
       *    history:
       *
       *    version    date              author             description
       *************************************************************************
       *    1.0        25-apr-2012  Shankar Hariharan     initial development
       ************************************************************************/
    /**************************************************************************/

procedure process_refund (errbuf out varchar2, retcode out number,i_dummy in number, i_dm_bs_id in number,
                          i_cm_bs_id in number,i_receipt_method_id in number);
end xxwc_ar_prism_refund_pkg; 
/
