create or replace PACKAGE      XXWC_INV_SALES_VELOCITY_PKG AS


   --Define variables for logging debug messages
   G_LEVEL_UNEXPECTED CONSTANT    NUMBER := 6;
   G_LEVEL_ERROR      CONSTANT    NUMBER := 5;
   G_LEVEL_EXCEPTION  CONSTANT    NUMBER := 4;
   G_LEVEL_EVENT      CONSTANT    NUMBER := 3;
   G_LEVEL_PROCEDURE  CONSTANT    NUMBER := 2;
   G_LEVEL_STATEMENT  CONSTANT    NUMBER := 1;

   G_CURRENT_RUNTIME_LEVEL          CONSTANT NUMBER := FND_LOG.G_CURRENT_RUNTIME_LEVEL ;
   G_MODULE_NAME                    CONSTANT VARCHAR2(60) := 'PLSQL.XXWC_INV_SALES_VELOCITY_PKG';
   G_CATEGORY_SET_NAME              CONSTANT VARCHAR2(30) := 'Inventory Category';
   -- Declare Local Constants
   G_ITEM_STATUS_DISCONTINUED       CONSTANT Varchar2(30):= 'Discontinu';
   G_ITEM_STATUS_NON_STOCK          CONSTANT Varchar2(30):= 'Non-Stock';
   G_ITEM_STATUS_NOT_REPLENISHED    CONSTANT Varchar2(30):= 'No Repl';
   G_CATEGORY_DISCONTINUED          CONSTANT Varchar2(1) := 'Z';
   G_CATEGORY_NON_STOCK             CONSTANT Varchar2(1) := 'N';
   G_CATEGORY_NOT_REPLENISHED       CONSTANT varchar2(1) := 'B';
   G_CATEGORY_NEW                   CONSTANT Varchar2(1) := 'E';
   --05/07/13 TMS 20130502-01289: CG Added to consider new SV Classes D and V
   G_CATEGORY_WC_DISCONTINUED       CONSTANT Varchar2(1) := 'D';
   G_CATEGORY_SUP_DISCONTINUED      CONSTANT Varchar2(1) := 'V';
   --05/07/13 TMS 20130502-01289
   G_WHOUSE_MODE_STORE              CONSTANT Varchar2(30):= 'Store';
   G_WHOUSE_MODE_DC                 Constant Varchar2(30) := 'D/C';


   G_DEBUG_FLAG        VARCHAR2(1);

   G_NUMBER_OF_WEEKS       NUMBER := To_Number(Fnd_Profile.Value('XXWC_NUMBER_OF_WEEKS'));
   G_CATEGORY_SET_ID       NUMBER := To_Number(Fnd_Profile.Value('XXWC_SALES_VELOCITY_CATEOGRY_SET'));
   G_DATE_ONE_YEAR_BACK    DATE   := ADD_MONTHS(SYSDATE, -12) ;
   G_ORGANIZATION_ID       NUMBER ;
   G_COMMIT_RECORDS_COUNT  CONSTANT  NUMBER := 1000;
   G_WHOUSE_MODE           VARCHAR2(30);
   G_RETURN_STATUS         Varchar2(1) ;
   G_ORGANIZATION_CODE     Varchar2(3);

    -- 12/26/12 CG: Added funtion to pull EiSR Sales Hits count
    procedure get_eisr_sales_hits (p_inventory_item_id IN NUMBER
                                , p_organization_id IN NUMBER
                                , x_sales_hits out NUMBER
                                , x_dc_sales_hits out NUMBER);
    
    function get_annual_sales (p_inventory_item_id IN NUMBER
                                , p_organization_id IN NUMBER)
    return number;                                
    
    -- Procedure  Populate_GT (p_Organization_ID Number ) ;
    -- Procedure will read all the Items assigned to a given
    Procedure  Populate_GT(P_Organization_ID Number) ;

    -- PRocedure Sales_Velocity_Weeks
    -- Procedure will calculate Sales Velocity Counts for N weeks for all the items and Update GT table
    Procedure Sales_Velocity_Weeks(p_ORganization_ID Number);

    -- Procedure RMA_WEEKS
    -- Procedure Calcuatates count of RMA for all the items and Updte GT Table.
    Procedure RMA_WEEKS(P_Organization_Id   IN Number,
                        p_Inventory_Item_ID IN  Number,
                        x_RMA_Count         Out NOCOPY Number);

    -- Procedure will calculate Sales Velocity Counts for N weeks for all the items and Update GT table
    Procedure Sales_Velocity_Annual(p_ORganization_ID Number);

    -- Procedure RMA_WEEKS
    -- Procedure Calcuatates count of RMA for all the items and Updte GT Table.
    Procedure RMA_Annual(P_Organization_Id   IN Number,
                         p_Inventory_Item_ID IN  Number,
                         x_RMA_Count         Out NOCOPY Number);
    -- Get Sales Velocity Count
    Procedure Get_DC_Sales_Count(p_Inventory_Item_ID     IN Number,
                                 p_DC_Organization_ID    IN Number,
                                 x_DC_Sales_Count        OUT NOCOPY Number,
                                 X_DC_Annual_Sales_Count OUT NOCOPY Number);
   -- Following Procedure will Create Item Cateogyr if it does not exist.
   -- If Exists it updates Item Category

   Procedure  Update_Item_Category(p_Organization_Id   Number,
                                   p_Inventory_Item_ID Number ) ;

    -- Procedure to Update Sales Velocity values
    Procedure  Update_Sales_Velocity(P_Organization_ID Number) ;


    -- 11/28/12 CG: Procedure to generate report of before and after value for sales velocity
    Procedure  Sales_Velocity_Report (P_Organization_ID Number) ;

    --*********************************************************
    --*********************************************************

    Procedure  MAIN (Errbuf                       OUT NOCOPY VARCHAR2,
                     Retcode                      OUT NOCOPY NUMBER,
                     p_Organization_Code          IN VARCHAR2,
                     p_Debug_Flag                 IN VARCHAR2 DEFAULT 'N', 
                     p_print_detail               IN VARCHAR2 DEFAULT 'N') ;





END XXWC_INV_SALES_VELOCITY_PKG; 