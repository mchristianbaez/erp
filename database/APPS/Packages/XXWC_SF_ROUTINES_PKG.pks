CREATE OR REPLACE PACKAGE apps.xxwc_sf_routines_pkg
AS
   /*************************************************************************
     $Header xxwc_sf_routines_pkg $
     Module Name: xxwc_sf_routines_pkg.pks

     PURPOSE:   This package holds common routines used by Sales and Fulfilment jobs.

     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.0        07/23/2015  Gopi Damuluri           Initial Version
                                                    TMS# 20150722-00086
   **************************************************************************/

   PROCEDURE process_picked_so_lines (p_errbuf      OUT VARCHAR2
                                    , p_retcode     OUT NUMBER
                                    , p_order_num    IN VARCHAR2);

END xxwc_sf_routines_pkg;
/