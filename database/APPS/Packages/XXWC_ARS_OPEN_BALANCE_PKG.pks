CREATE OR REPLACE PACKAGE APPS.XXWC_ARS_OPEN_BALANCE_PKG
AS

  /**************************************************************************
     $Header XXWC_ARS_OPEN_BALANCE_PKG $
     Module Name: XXWC_ARS_OPEN_BALANCE_PKG.pks

     PURPOSE:   This package is called by the concurrent programs
                XXWC ARS Outbound Open Balance

     REVISIONS:
     Ver        Date        Author             Description
     ---------  ----------  ---------------   -------------------------
      1.0       11/12/2016  Neha Saini         Initial Build - TMS#20160126-00238

   /*************************************************************************



   /*************************************************************************
     Procedure : generate_ars_file

     PURPOSE   :This procedure creates file for the ARS Outbound Open Balance
     PARAMETERS:  IN - None
                  OUT - errbuf,retcode
     REVISIONS:
     Ver        Date        Author             Description
     ---------  ----------  ---------------   -------------------------
      1.0       11/12/2016  Neha Saini         Initial Build - TMS#20160126-00238
   ************************************************************************/
   PROCEDURE generate_ars_file (errbuf OUT VARCHAR2, retcode OUT VARCHAR2);
   
END XXWC_ARS_OPEN_BALANCE_PKG;
/