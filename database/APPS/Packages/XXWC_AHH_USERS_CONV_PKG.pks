CREATE OR REPLACE PACKAGE XXWC_AHH_USERS_CONV_PKG
IS
   /******************************************************************************************************************************************************
        $Header XXWC_AHH_USERS_CONV_PKG $
        Module Name: XXWC_AHH_USERS_CONV_PKG.pks

        PURPOSE:   AHH Users Conversion

        REVISIONS:
        Ver        Date        Author           Description
        ---------  ----------  ---------------  ------------------------------------------------------------------------------------------------
        1.0        07/18/2018  Sundaramoorthy R   TMS#20180718-00046
   ******************************************************************************************************************************************************/

   PROCEDURE debug (P_RECORD_ID IN NUMBER, p_msg IN VARCHAR2);
   PROCEDURE main (x_errbuf          OUT VARCHAR2,
                   x_retcode         OUT VARCHAR2,
                   p_user_name        IN VARCHAR2
                   );
END;
/