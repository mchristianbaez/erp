create or replace
PACKAGE       APPS.XXWC_PO_PURGE_REQ_PKG
/*************************************************************************
   *   $Header PO_PURGE_REQ $
   *   Module Name: XXWC_PO_PURGE_REQ_PKG
   *
   *   PURPOSE:   delete records from PO_PURGE_REQ_LIST tables 
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        10/05/2015  Hari Prasad M           Initial Version
   *   
   * ******************************************************************************/
AS 
 PROCEDURE XXWC_PO_PURGE_REQ             (errbuf    OUT VARCHAR2,
                                     retcode   OUT VARCHAR2);
   
End XXWC_PO_PURGE_REQ_PKG;
/