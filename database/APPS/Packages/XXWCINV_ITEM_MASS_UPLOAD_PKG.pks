CREATE OR REPLACE Package APPS.XXWCINV_ITEM_MASS_UPLOAD_PKG As

/**************************************************************************
 *
 * PROCEDURE
 *  Load_Xref
 *
 * DESCRIPTION
 *  This procedure collects the UPC cross-reference information in the XXWCINV_ITEM_XREF_IFACE table
 *    and submits it thru an API to generate UPC cross-reference information in the base tables
 *
 * PARAMETERS
 * ==========
 * NAME              TYPE     IN/OUT  DESCRIPTION
.* ----------------- -------- ------  ---------------------------------------
 * errbuf            VARCHAR2 OUT     error message returned to concurrent request
 * retcode           VARCHAR2 OUT     return code  (0=success  1=warning  2=error)
 *
 *
 * CALLED BY
 *  XXWC Item Cross-Reference Upload concurrent program
 *
 *************************************************************************/
  Procedure Load_Xref(errbuf  OUT  VARCHAR2
                     ,retcode OUT  VARCHAR2);

/**************************************************************************
 *
 * PROCEDURE
 *  Xref_Upload
 *
 * DESCRIPTION
 *  This procedure is used to upload the user-supplied data from the Excel spreadsheet into the custom interface table
 *
 * PARAMETERS
 * ==========
 * NAME                   TYPE     IN/OUT  DESCRIPTION
.* ---------------------- -------- ------  ---------------------------------------
 * P_ORG_CODE             VARCHAR2 IN      inventory organizatin code
 * P_ITEM_NUMBER          VARCHAR2 IN      inventory item number
 * P_XREF_TYPE            VARCAHR2 IN      cross-reference type
 * P_XREF_CODE            VARCHAR2 IN      old cross-reference
 * P_NEW_XREF_CODE        VARCHAR2 IN      new cross-reference
 * P_DESCRIPTION          VARCHAR2 IN      description for cross-reference
 *
 * CALLED BY
 *  XXWC Item Cross-Reference Integrator during the WebADI upload process
 *
 *************************************************************************/
  Procedure Xref_Upload(p_org_code               IN  VARCHAR2
                       ,p_item_number            IN  VARCHAR2
                       ,p_xref_type              IN  VARCHAR2
                       ,p_xref_code              IN  VARCHAR2 DEFAULT NULL
                       ,p_new_xref_code          IN  VARCHAR2
                       ,p_description            IN  VARCHAR2 DEFAULT NULL);


/**************************************************************************
 *
 * PROCEDURE
 *  Bin_Upload
 *
 * DESCRIPTION
 *  This procedure is used to upload the user-supplied data from the Excel spreadsheet into the custom interface table
 *
 * PARAMETERS
 * ==========
 * NAME                   TYPE     IN/OUT  DESCRIPTION
.* ---------------------- -------- ------  ---------------------------------------
 * P_ORG_CODE             VARCHAR2 IN      inventory organizatin code
 * P_ITEM_NUMBER          VARCHAR2 IN      inventory item number
 * P_OLD_LOCATION         VARCAHR2 IN      old stock locator
 * P_NEW_LOCATION         VARCAHR2 IN      new stock locator
 *
 * CALLED BY
 *  XXWC Item Location Upload Integrator during the WebADI upload process
 *
 *************************************************************************/
  Procedure Bin_Upload(p_org_code    IN VARCHAR2
                      ,p_item_number IN VARCHAR2
                      ,p_Old_location  IN VARCHAR2 Default NULL
                      ,p_new_location  IN VARCHAR2);

/**************************************************************************
 *
 * PROCEDURE
 *  Load_Location
 *
 * DESCRIPTION
 *  This procedure collects the item / location assignment information in the XXWCINV_STOCK_LOCATOR_IFACE table
 *    and submits it thru an API to generate item / location assignment information in the base tables
 *
 * PARAMETERS
 * ==========
 * NAME              TYPE     IN/OUT  DESCRIPTION
.* ----------------- -------- ------  ---------------------------------------
 * errbuf            VARCHAR2 OUT     error message returned to concurrent request
 * retcode           VARCHAR2 OUT     return code  (0=success  1=warning  2=error)
 *
 *
 * CALLED BY
 *  XXWC Item / Location Assignment Upload concurrent program
 *
 *************************************************************************/
  Procedure Load_Location(errbuf   OUT  VARCHAR2
                         ,retcode  OUT  VARCHAR2);

/**************************************************************************
 *
 * PROCEDURE
 *  Src_Rule_Upload
 *
 * DESCRIPTION
 *  This procedure is used to upload the user-supplied data from the Excel spreadsheet into the custom interface table
 *
 * PARAMETERS
 * ==========
 * NAME                   TYPE     IN/OUT  DESCRIPTION
.* ---------------------- -------- ------  ---------------------------------------
 * P_ORG_CODE             VARCHAR2 IN      inventory organizatin code
 * P_ITEM_NUMBER          VARCHAR2 IN      inventory item number
 * P_OLD_SRC_RULE         VARCHAR2 IN      old purchasing source rule
 * P_NEW_SRC_RULE         VARCHAR2 IN      new purchasing source rule
 *
 * CALLED BY
 *  XXWC Sourcing Rule Upload Integrator during the WebADI upload process
 *
 *************************************************************************/
  Procedure Src_Rule_Upload(p_org_code      IN  VARCHAR2
                           ,p_item_number   IN  VARCHAR2
                           ,p_old_src_rule  IN  VARCHAR2
                           ,p_new_src_rule  IN  VARCHAR2);

/**************************************************************************
 *
 * PROCEDURE
 *  Load_Sourcing_Assignments
 *
 * DESCRIPTION
 *  This procedure collects the sourcing rule assignment information in the  table
 *    and submits it thru an API to generate item sourcing rule assignment information in the base tables
 *
 * PARAMETERS
 * ==========
 * NAME              TYPE     IN/OUT  DESCRIPTION
.* ----------------- -------- ------  ---------------------------------------
 * errbuf            VARCHAR2 OUT     error message returned to concurrent request
 * retcode           VARCHAR2 OUT     return code  (0=success  1=warning  2=error)
 *
 *
 * CALLED BY
 *  XXWC Item / Location Assignment Upload concurrent program
 *
 *************************************************************************/
  Procedure Load_Sourcing_Assignments(errbuf   OUT  VARCHAR2
                                     ,retcode  OUT  VARCHAR2);

/**************************************************************************
 *
 * PROCEDURE
 *  BPA_Upload
 *
 * DESCRIPTION
 *  This procedure is used to upload the user-supplied data from the Excel spreadsheet into the custom interface table
 *
 * PARAMETERS
 * ==========
 * NAME                   TYPE     IN/OUT  DESCRIPTION
.* ---------------------- -------- ------  ---------------------------------------
 * P_OU_ID                NUMBER   IN      operating unit id
 * P_DOCUMENT_NUMBER      VARCHAR2 IN      purchasing document number
 * P_LINE_NUMBER          NUMBER   IN      line number
 * p_shipment_number      NUMBER   IN      shipment number / price break line
 * p_org_code             VARCHAR2 IN      ship-to inventory organization
 * p_unit_price           NUMBER   IN      line-level unit price
 * p_quantity             NUMBER   IN      price break quantity
 * p_price_Override       NUMBER   IN      price break unit price
 * p_discount_percent     NUMBER   IN      price break discount percent
 * p_start_date           DATE     IN      price break start date
 * p_end_date             DATE     IN      price break end date
 *
 * CALLED BY
 *  XXWC BPA - Price Break Upload Integrator during the WebADI upload process
 *
 *************************************************************************/
  Procedure BPA_Upload(p_ou_id            IN  NUMBER,
                       p_document_number  IN  VARCHAR2,
                       p_supplier         IN  VARCHAR2 DEFAULT NULL,
                       p_line_number      IN  NUMBER,
                       p_item_number      IN  VARCHAR2 DEFAULT NULL,
                       p_description      IN  VARCHAR2 DEFAULT NULL,
                       p_supplier_item    IN  VARCHAR2 DEFAULT NULL,
                       p_shipment_number  IN  NUMBER DEFAULT NULL,
                       p_org_code         IN  VARCHAR2,
                       p_unit_price       IN  NUMBER,
                       p_quantity         IN  NUMBER,
                       p_price_Override   IN  NUMBER,
                       p_start_date       IN  DATE,
                       p_end_date         IN  DATE, 
                       P_ALLOW_PRICE_OVERRIDE_FLAG IN Varchar2,
                       P_NOT_TO_EXCEED_PRICE IN NUMBER,
                       P_CASCADE_FLAG IN VARCHAR2 );

/**************************************************************************
 *
 * PROCEDURE
 *  Load_BPA
 *
 * DESCRIPTION
 *  This procedure collects the blanket purchasing agreement price break information in the  table
 *    and submits it thru an API to generate BPA price break information in the base tables
 *
 * PARAMETERS
 * ==========
 * NAME              TYPE     IN/OUT  DESCRIPTION
.* ----------------- -------- ------  ---------------------------------------
 * errbuf            VARCHAR2 OUT     error message returned to concurrent request
 * retcode           VARCHAR2 OUT     return code  (0=success  1=warning  2=error)
 * i_doc_number      VARCHAR2 OUT    BPA Number to be imported-- Added by Shankar TMS Task 20121217-00557
 *
 * CALLED BY
 *  XXWC BPA - Price Break Upload concurrent program
 *
 *************************************************************************/
  Procedure Load_BPA(errbuf   OUT  VARCHAR2,
                     retcode  OUT  VARCHAR2,
                     i_doc_number in VARCHAR2);

END XXWCINV_ITEM_MASS_UPLOAD_PKG;
/