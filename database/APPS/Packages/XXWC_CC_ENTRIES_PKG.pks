CREATE OR REPLACE PACKAGE APPS.XXWC_CC_ENTRIES_PKG IS

    g_user_id          NUMBER := fnd_global.user_id;
    g_login_id         NUMBER := fnd_profile.value('LOGIN_ID');
    g_err_callfrom     VARCHAR2(75) DEFAULT 'XXWC_CC_ENTRIES_PKG';
    g_err_callpoint    VARCHAR2(75) DEFAULT 'START';
    g_distro_list      VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
    g_err_msg          VARCHAR2(2000);
    g_message          VARCHAR2(2000);
    g_date             DATE := SYSDATE;
  
 /******************************************************************************
      NAME:       XXWC_CC_ENTRIES_PKG

      PURPOSE:    To process records from the XXWC_CC_ENTRY Form to the cycle count API

      Logic:     1) Execute the parameters passed through to the cycle count API
      
      REVISIONS:
      Ver        Date        Author           Description
      ---------  ----------  ---------------  ------------------------------------
      1.0        01-JUL-13   Lee Spitzer       1. Created this package spec  TMS Ticket - 20130214-01096 Implement Physical Inventory enhancements 
@SC @FIN / Implement Physical Inventory enhancements ... 
      
   ******************************************************************************/

  /******************************************************************************
    PROCEDURE process_api 
    Parameters 
    (p_cycle_count_header_id  IN NUMBER     Cycle Count Header ID
    ,p_cycle_count_entry_id   IN NUMBER     Cycle Count Entry Id
    ,p_count_list_sequence    IN NUMBER     Cycle Count List Sequence
    ,p_organization_id        IN NUMBER     Organization ID
    ,p_inventory_item_id      IN NUMBER     Inventory Item Id
    ,p_revision               IN VARCHAR2   Item Revision
    ,p_count_quantity         IN NUMBER     Cycle Count Quantity
    ,p_count_uom              IN VARCHAR2   Cycle Count Unit of Measure Code
    ,p_subinventory           IN VARCHAR2   Subinventory
    ,p_lot_number             IN VARCHAR2   Lot Number
    ,p_locator_id             IN NUMBER     Stock Locator Id
    ,p_cost_group_id          IN NUMBER     Cost Group Id
    ,p_user_id                IN NUMBER     User ID
    ,p_employee_id            IN NUMBER     Employee ID
    ,p_date                   IN DATE       Count Date
    ,x_interface_id           OUT NUMBER    Interface ID created and inserted into MTL_CC_ENTRIES_INTERFACE
    ,x_status                 OUT VARCHAR2  Return Status - S Means Success, E - Error, U - User-Defined
    ,x_error_code             OUT VARCHAR2  Error Code returned from the API
    ,x_msg_count              OUT NUMBER    Message Count returned from the API
    ,x_msg                    OUT VARCHAR2  Return Message from the API
   ******************************************************************************/

  PROCEDURE process_api
    (p_cycle_count_header_id  IN NUMBER
    ,p_cycle_count_entry_id   IN NUMBER
    ,p_count_list_sequence    IN NUMBER
    ,p_organization_id        IN NUMBER
    ,p_inventory_item_id      IN NUMBER
    ,p_revision               IN VARCHAR2
    ,p_count_quantity         IN NUMBER
    ,p_count_uom              IN VARCHAR2
    ,p_subinventory           IN VARCHAR2
    ,p_lot_number             IN VARCHAR2
    ,p_locator_id             IN NUMBER
    ,p_cost_group_id          IN NUMBER
    ,p_user_id                IN NUMBER
    ,p_employee_id            IN NUMBER
    ,p_date                   IN DATE
    ,x_interface_id           OUT NUMBER
    ,x_status                 OUT VARCHAR2
    ,x_error_code             OUT VARCHAR2
    ,x_msg_count              OUT NUMBER
    ,x_msg                    OUT VARCHAR2
    );
  
  
  FUNCTION wc_count_list_sequence
    (p_count_list_sequence    NUMBER
    ,p_wc_stock_locator       VARCHAR2)
    RETURN NUMBER;
    
END  XXWC_CC_ENTRIES_PKG;
/
