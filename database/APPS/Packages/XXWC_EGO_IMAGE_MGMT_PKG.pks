CREATE OR REPLACE PACKAGE apps.xxwc_ego_image_mgmt_pkg
/*********************************************************************************
File Name:    XXWC_EGO_IMAGE_MGMT_PKG
PROGRAM TYPE: PL/SQL Package specification
HISTORY
PURPOSE: Procedures and functions for maintaining the image exists attribute
==================================================================================
VERSION DATE          AUTHOR(S)               DESCRIPTION
------- -----------   --------------- --------------------------------------------
1.0     25-Feb-2014   Praveen Pawar           Initial creation of the package spec
*********************************************************************************/
AS
   PROCEDURE sync_image_exists_attr (
      errbuf    OUT NOCOPY   VARCHAR2,
      retcode   OUT NOCOPY   VARCHAR2
   );
END xxwc_ego_image_mgmt_pkg;