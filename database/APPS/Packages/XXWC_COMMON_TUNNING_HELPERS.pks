/* Formatted on 16-Oct-2013 18:50:17 (QP5 v5.206) */
-- Start of DDL Script for Package APPS.XXWC_COMMON_TUNNING_HELPERS
-- Generated 16-Oct-2013 18:50:11 from APPS@EBIZFQA

CREATE OR REPLACE PACKAGE apps.xxwc_common_tunning_helpers
IS
    -- Purpose: these procedures and functions are created in each XXWC tuning package
    -- to be able to use one copy of these objects
    -- MODIFICATION HISTORY
    -- Person      Date    Comments
    -- ---------   ------  ------------------------------------------
    --Rasikha Galimova 09/25/2013
    -- Enter package declarations as shown below

    PROCEDURE elapsed_time (p_interface_name VARCHAR2, p_called_from VARCHAR2, p_start IN OUT NUMBER);

    FUNCTION add_tuning_parameter (p_schema_name VARCHAR2, p_table_name VARCHAR2, p_tuning_factor NUMBER)
        RETURN NUMBER;

    PROCEDURE remove_tuning_factor_columns (p_schema_name VARCHAR2, p_table_name VARCHAR2);

    PROCEDURE drop_temp_table (p_owner VARCHAR2, p_table_name VARCHAR2);

    PROCEDURE alter_table_temp (p_owner VARCHAR2, p_table_name VARCHAR2);

    FUNCTION create_copy_table (p_owner VARCHAR2, p_table_name VARCHAR2, p_new_table_name VARCHAR2)
        RETURN NUMBER;

    PROCEDURE create_table_from_other_table (p_owner VARCHAR2, p_old_table_name VARCHAR2, p_new_table_name VARCHAR2);

    PROCEDURE format_temp_table (p_owner VARCHAR2, p_table_name VARCHAR2);

    PROCEDURE write_log (p_text IN VARCHAR2);

    PROCEDURE deleteduplicates (p_table_name     IN VARCHAR2
                               ,p_column_name    IN VARCHAR2
                               ,p_where_clause   IN VARCHAR2 DEFAULT NULL);

    FUNCTION remove_control_nonascii (p_in_string VARCHAR2)
        RETURN VARCHAR2;

    FUNCTION remove_control (p_in_string VARCHAR2)
        RETURN VARCHAR2;

    FUNCTION remove_nonascii (p_in_string VARCHAR2)
        RETURN VARCHAR2;

    PROCEDURE create_temp_table_data (p_owner         VARCHAR2
                                     ,p_table_name    VARCHAR2
                                     ,p_ddl_sql       CLOB
                                     ,p_insert_sql    CLOB);

    PROCEDURE send_mail_with_attachement (p_from        IN VARCHAR2
                                         ,p_to          IN VARCHAR2
                                         ,p_file_name   IN VARCHAR2
                                         ,p_subject     IN VARCHAR2);
END;                                                                                                     -- Package spec
/

-- Grants for Package
GRANT EXECUTE ON apps.xxwc_common_tunning_helpers TO xxeis
/
GRANT EXECUTE ON apps.xxwc_common_tunning_helpers TO interface_xxcus
/

-- End of DDL Script for Package APPS.XXWC_COMMON_TUNNING_HELPERS
