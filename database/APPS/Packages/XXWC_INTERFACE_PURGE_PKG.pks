CREATE OR REPLACE PACKAGE APPS.xxwc_interface_purge_pkg
IS
   /********************************************************************************
   -- File Name: XXWC_INTERFACE_PURGE_PKG.pks
   --
   -- PROGRAM TYPE: PL/SQL Script   <API>
   --
   -- PURPOSE: Package is to purge processed records in tables listed
   --          in 'XXWC_INTF_PURGE_TABLES' value set.
   -- HISTORY
   -- ======================================================================================
   -- ======================================================================================
   -- VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- ------------------------------------------------
   -- 1.0     05-Feb-2015   P.Vamshidhar    Created this package.  TMS: 20141120-00077
                                            White Cap CandI Concurrent Program for
                                            Purging processed records from SC interfaces.
   -- 1.2     16-Oct-2015   P.Vamshidhar    TMS#20150915-00126
                                            Added new Procedure to Purge from custom table
                                            XXWC.XXWC_PO_LINE_ADDTIONAL_ATTR_T
   -- 1.3     09-Feb-2017   Pattabhi Avula  TMS#20170126-00289
                                            Added new Procedure to Purge data from 
											mtl_interface_errors table after taking the backup
   *****************************************************************************************/

   PROCEDURE Int_Data_cleanup (p_errbuf             OUT VARCHAR2,
                               p_retcode            OUT VARCHAR2,
                               p_tab_selection   IN     VARCHAR2);

   PROCEDURE MTL_SYSTEM_ITEMS_INTF_PRC (p_errbuf    OUT VARCHAR2,
                                        p_retcode   OUT VARCHAR2);

   PROCEDURE MTL_ITEM_REV_INTF_PRC (p_errbuf    OUT VARCHAR2,
                                    p_retcode   OUT VARCHAR2);

   PROCEDURE EGO_ITM_ATTR_INTF_PRC (p_errbuf    OUT VARCHAR2,
                                    p_retcode   OUT VARCHAR2);

   PROCEDURE EGO_BULKLOAD_INTF_PRC (p_errbuf    OUT VARCHAR2,
                                    p_retcode   OUT VARCHAR2);

-- Added below procedure in Rev 1.2
   PROCEDURE XXWC_PO_LINE_ATTRIBUTE_PRC (p_errbuf    OUT VARCHAR2,
                                         p_retcode   OUT VARCHAR2);

   FUNCTION table_check (p_table_name IN VARCHAR2)
      RETURN VARCHAR2;
/*	  
-- Added below procedure in Rev 1.3
   PROCEDURE XXWC_MTL_INTERFACE_ERRORS_PRC (p_errbuf    OUT VARCHAR2,
                                            p_retcode   OUT VARCHAR2); */

END xxwc_interface_purge_pkg;
/