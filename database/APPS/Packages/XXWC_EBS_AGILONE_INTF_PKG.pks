CREATE OR REPLACE PACKAGE APPS.xxwc_ebs_agilone_intf_pkg
--//============================================================================
--//
--// Object Name         :: xxwc_ebs_agilone_intf_pkg
--//
--// Object Type         :: Package Specification
--//
--// Object Description  :: This is an outbound interface from oracle ebs to agilone.
--//
--// Version Control
--//============================================================================
--// Vers    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Mahesh Kudle     26/05/2014    Initial Build - TMS#20140602-00295
--// 2.0     Maharajan S      09/23/2015    TMS#20150824-00143 Agile One Supplemental Customer Attribute File
--//============================================================================
AS

--//============================================================================
--//
--// Object Name         :: create_file
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This procedure creates file for the outbound extract
--//
--// Version Control
--//============================================================================
--// Vers    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Mahesh Kudle     26/05/2014    Initial Build - TMS#20140602-00295
--//============================================================================
PROCEDURE create_file (p_errbuf           OUT  VARCHAR2
                      ,p_retcode          OUT  NUMBER
                      ,p_operating_unit   IN   NUMBER
                      ,p_directory_name   IN   VARCHAR2
                      ,p_source_name      IN   VARCHAR2
                      ,p_view_name        IN   VARCHAR2);

TYPE xxwc_ob_file_rec IS RECORD (org_id   NUMBER
                                ,rec_line VARCHAR2 (32767) );

TYPE xxwc_ob_cust_file_rec IS RECORD (cust_attr VARCHAR2 (32767) );   --Added for ver#2.0

TYPE xxwc_ob_file_rec_tbl_type IS TABLE OF xxwc_ob_file_rec INDEX BY BINARY_INTEGER;


--//============================================================================
--//
--// Object Name         :: main
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This procedure is called by UC4 to initiate Outbound Interfaces
--//
--// Version Control
--//============================================================================
--// Vers    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Mahesh Kudle     26/05/2014    Initial Build - TMS#20140602-00295
--//============================================================================
PROCEDURE main (p_errbuf              OUT VARCHAR2
               ,p_retcode             OUT NUMBER
               ,p_operating_unit_name IN  VARCHAR2
               ,p_ob_directory_name   IN  VARCHAR2
               ,p_objects_source      IN  VARCHAR2
               ,p_view_name           IN  VARCHAR2);

--//============================================================================
--//
--// Object Name         :: xxwc_market_price_extract
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This procedure is used to generate national market 
--//                        extract to pricing team
--//
--// Version Control
--//============================================================================
--// Vers    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Mahesh Kudle     26/05/2014    Initial Build - TMS#20140602-00295
--//============================================================================
PROCEDURE  xxwc_market_price_extract (p_errbuf             OUT VARCHAR2
                                     ,p_retcode            OUT NUMBER
                                     ,p_inventory_category IN VARCHAR2
                                     ,p_include_specials   IN VARCHAR2
									 ,p_directory_name     IN VARCHAR2
									 ,p_source_name        IN VARCHAR2);

--//============================================================================
--//
--// Object Name         :: xxwc_national_market_price
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This procedure is used to generate national market 
--//                        extract to agilone
--//
--// Version Control
--//============================================================================
--// Vers    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Mahesh Kudle     26/05/2014    Initial Build - TMS#20140602-00295
--//============================================================================
PROCEDURE  xxwc_national_market_price(p_errbuf             OUT VARCHAR2
                                     ,p_retcode            OUT NUMBER
                                     ,p_inventory_category IN VARCHAR2
                                     ,p_include_specials   IN VARCHAR2
									 ,p_directory_name     IN VARCHAR2
									 ,p_source_name        IN VARCHAR2);

--//============================================================================
--//
--// Object Name         :: xxwc_customer_attribute
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This procedure is used to generate customer attribute 
--//                        extract to agilone
--//
--// Version Control
--//============================================================================
--// Vers    Author                  Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Maharajan Shunmugam     09/23/2015    TMS#20150824-00143 Agile One Supplemental Customer Attribute File
--//============================================================================
PROCEDURE  xxwc_customer_attribute   (p_errbuf             OUT VARCHAR2
                                     ,p_retcode            OUT NUMBER);
								   
								   
END xxwc_ebs_agilone_intf_pkg;
/