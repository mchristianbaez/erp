CREATE OR REPLACE PACKAGE APPS.xxwc_inv_item_cross_ref_pkg
/**************************************************************************
 *
 * HEADER
 *   XXWC_INV_ITEM_CROSS_REF_PKG
 *
 * PROGRAM NAME
 *  XXWC_INV_ITEM_CROSS_REF_PKG.pks
 *
 * DESCRIPTION
 *  This package spec will upload the item cross reference in to base table using the API
 *
 *
 * PARAMETERS
 * ==========
 * NAME              DESCRIPTION
.* ----------------- ------------------------------------------------------
    None
 *
 * DEPENDENCIES
 *   None
 *
 * CALLED BY
 *   Item Cross reference Upload WebADI
 *
 * LAST UPDATE DATE   12-FEB-2015
 *
 * HISTORY
 * =======
 *
 * VERSION DATE        AUTHOR(S)       DESCRIPTION
 * ------- ----------- --------------- ------------------------------------
 * 1.0    12-FEB-2015 Gajendra M      Created for Item Cross reference Upload WebADI TMS # 20141212-00129
 * 8.0    25-Jun-2017 P.Vamshidhar          TMS#20170421-00201 - UPC Validation Master Items Edit Form  
 *************************************************************************/
 AS
  /**************************************************************************
   *
   * PROCEDURE
   * upload_xref
   *
   * DESCRIPTION
   *  This procedure will upload the cross reference in to base table thorough Web ADI
   *
   * PARAMETERS
   * ==========
   * NAME                   TYPE                  DESCRIPTION
  .* ----------------- -------- ---------------------------------------------
   * P_TRANSACTION_TYPE     VARCHAR2 IN      Transaction Type   
   * P_ITEM_NUMBER          VARCHAR2 IN      inventory item number
   * P_TRANSACTION_TYPE     VARCHAR2 IN      transaction type for cross-reference
   * P_CROSS_TYPE           VARCAHR2 IN      cross-reference type
   * XREF_CODE              VARCHAR2 IN      ross-reference   
   * P_NEW_XREF_CODE        VARCHAR2 IN      new cross-reference
   * P_MST_VEN_NO           VARCHAR2 IN      Master Vendor Number
   * P_MST_VEN_NAME         VARCHAR2 IN      Master Vendor Name
   * P_DESCRIPTION          VARCHAR2 IN      ITem Description
   * P_MST_VEN_NO_NAME      VARCHAR2 IN      Master Vendor Name Read Only
   *
   * RETURN VALUE
   *  None
   *
   *************************************************************************/
  PROCEDURE upload_xref(p_item_number      IN VARCHAR2
                       ,p_transaction_type IN VARCHAR2
                       ,p_cross_type       IN VARCHAR2
                       ,p_xref_code        IN VARCHAR2 DEFAULT NULL
                       ,p_new_xref_code    IN VARCHAR2
                       ,p_mst_ven_no       IN VARCHAR2 DEFAULT NULL
                       ,p_mst_ven_name     IN VARCHAR2 DEFAULT NULL
                       ,p_description      IN VARCHAR2 DEFAULT NULL
                       ,p_mst_ven_no_name  IN VARCHAR2 DEFAULT NULL);
   -- Added below function in Rev 8.0
   FUNCTION master_item_upc_validation (P_CROSS_REF   IN     VARCHAR2) RETURN VARCHAR2;					   
END xxwc_inv_item_cross_ref_pkg;
/