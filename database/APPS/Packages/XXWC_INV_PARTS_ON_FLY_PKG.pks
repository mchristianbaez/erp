CREATE OR REPLACE PACKAGE APPS.XXWC_INV_PARTS_ON_FLY_PKG
AS
   /*****************************************************************************************************************************************
   *   $Header XXWC_INV_PARTS_ON_FLY_PKG.pks $                                                                                              *
   *   Module Name: XXWC_INV_PARTS_ON_FLY_PKG                                                                                               *
   *                                                                                                                                        *
   *   PURPOSE:   This package is used by Parts on Fly form calling from sales order form                                                   *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0                                             Initial Version                                                                      *
   *                                                                                                                                        *
   *                                                                                                                                        *
   *   2.0        03-SEP-2015  P.Vamshidhar            TMS#20150629-00046 - 2015 PLM enhancement - Specials Reduction                       *
   *                                                                                                                                        *
   *                                                                                                                                        *
   *****************************************************************************************************************************************/

   --Define variables for logging debug messages
   G_LEVEL_UNEXPECTED           CONSTANT NUMBER := 6;
   G_LEVEL_ERROR                CONSTANT NUMBER := 5;
   G_LEVEL_EXCEPTION            CONSTANT NUMBER := 4;
   G_LEVEL_EVENT                CONSTANT NUMBER := 3;
   G_LEVEL_PROCEDURE            CONSTANT NUMBER := 2;
   G_LEVEL_STATEMENT            CONSTANT NUMBER := 1;

   G_CURRENT_RUNTIME_LEVEL      CONSTANT NUMBER
                                            := FND_LOG.G_CURRENT_RUNTIME_LEVEL ;
   G_MODULE_NAME                CONSTANT VARCHAR2 (60)
                                            := 'PLSQL.XXWC_INV_PARTS_ON_FLY_PKG' ;
   G_CATEGORY_SET_NAME          CONSTANT VARCHAR2 (30) := 'Inventory Category';
   G_TRX_TYPE_PARTS_ON_FLY_SO   CONSTANT VARCHAR2 (30) := 'PARTS_ON_FLY_SO';
   G_TRX_TYPE_PARTS_ON_FLY_RO   CONSTANT VARCHAR2 (30) := 'PARTS_ON_FLY_RO';


   G_COST_GROUP_ID                       NUMBER;
   G_ITEM_NUMBER                         VARCHAR2 (40);
   G_INVENTORY_ITEM_ID                   NUMBER;


   -- Create A Record Structure and capture all the UI Attributes


   TYPE PARTS_ON_FLY_REC IS RECORD
   (
      ITEM_NUMBER                VARCHAR2 (40),
      ITEM_DESCRIPTION           VARCHAR2 (240),
      INVENTORY_ITEM_ID          NUMBER,
      MST_ORGANIZATION_ID        NUMBER,
      ASSGN_ORGANIZATION_ID      NUMBER,
      PRICE_LIST_HEADER_ID       NUMBER,
      ITEM_TYPE_CODE             VARCHAR2 (30),
      ITEM_TEMPLATE_ID           NUMBER,
      PRIMARY_UOM_CODE           VARCHAR2 (10),
      WEIGHT_UOM_CODE            VARCHAR2 (10),
      UNIT_WEIGHT                NUMBER,
      BUYER_ID                   NUMBER,
      VENDOR_ID                  NUMBER,
      VENDOR_SITE_ID             NUMBER,
      MANUFACTURER_PART_NUMBER   VARCHAR2 (30),
      ORG_ID                     NUMBER,
      CATEGORY_ID                NUMBER,
      HAZARDOUS_MATERIAL_FLAG    VARCHAR2 (1),
      HAZARD_CLASS_ID            NUMBER,
      LIST_PRICE                 NUMBER (15, 5),
      MARKET_PRICE               NUMBER (10, 2),
      ITEM_COST                  NUMBER (15, 5),
      RADIO_GROUP_VALUE          VARCHAR2 (1),
      TRANSACTION_ID             NUMBER,
      ORGANIZATION_CODE          VARCHAR2 (30)
   );


   -- *********************************************************************************
   -- PROCEDURE CREATE_CROSS_REF : Creates Cross Reference for the new product
   --  Calls API to created Cross Reference : mtl_cross_references_pkg.insert_row
   --  Commits Transaction once cross reference is successfully created
   -- *********************************************************************************

   PROCEDURE CREATE_CROSS_REF (p_segment1               IN VARCHAR2,
                               p_cross_reference_type   IN VARCHAR2,
                               p_cross_reference        IN VARCHAR2,
                               p_description            IN VARCHAR2);

   -- *********************************************************************************
   -- PROCEDURE ASSIGN_SOURCING_RULE: API to create Sorucing Rules
   -- Public API MRP_SOURCING_RULE_PUB.Process_Sourcing_Rule is called to process sourcing rules.
   -- Upon successfully processing sourcing rules a record is created in Sourcing Rule Assignments table.
   -- *********************************************************************************


   PROCEDURE ASSIGN_SOURCING_RULE (
      p_INVENTORY_ITEM_ID   IN            NUMBER,
      p_vendor_id           IN            NUMBER,
      p_vendor_site_id      IN            NUMBER,
      p_Organization_ID     IN            NUMBER,
      X_Return_Status          OUT NOCOPY VARCHAR2,
      x_Msg_Data               OUT NOCOPY VARCHAR2,
      x_Msg_Count              OUT NOCOPY NUMBER);


   -- *********************************************************************************
   -- PROCEDURE CREATE_ASL: API to create Approved Supplier List
   --  API creates a record in Approved Supplier List
   -- *********************************************************************************

   PROCEDURE CREATE_ASL (p_segment1         IN VARCHAR2,
                         p_vendor_id        IN NUMBER,
                         p_vendor_site_id   IN NUMBER);



   -- *********************************************************************************
   -- PROCEDURE CREATE_VENDOR_CATEGORY: API to Vendor Category :
   --  API mtl_categories_pkg.insert_row is called to create Vendor Category
   -- *********************************************************************************

   PROCEDURE CREATE_VENDOR_CATEGORY (
      p_category_set_id   IN            NUMBER,
      p_vendor_id         IN            NUMBER,
      p_vendor_site_id    IN            NUMBER,
      x_category_id          OUT NOCOPY NUMBER);

   -- *********************************************************************************
   -- PROCEDURE ASSIGN_CATEGORY: API to  Assign a Item Category if it is different from the default Category :
   --
   -- *********************************************************************************

   PROCEDURE ASSIGN_CATEGORY (p_category_id         IN            NUMBER,
                              p_organization_id     IN            NUMBER,
                              p_Inventory_Item_ID   IN            NUMBER,
                              x_Return_Status          OUT NOCOPY VARCHAR2,
                              x_Msg_Data               OUT NOCOPY VARCHAR2,
                              x_Msg_Count              OUT NOCOPY NUMBER);


   -- *********************************************************************************
   -- PROCEDURE Assign_to_price_list: API to  assign to a price List
   --
   -- *********************************************************************************

   PROCEDURE ASSIGN_TO_PRICE_LIST (p_inventory_item_id     IN NUMBER,
                                   P_List_Header_ID        IN NUMBER,
                                   p_list_price_per_unit   IN NUMBER,
                                   p_primary_uom_code      IN VARCHAR2);


   -- *********************************************************************************
   -- PROCEDURE CREATE_ITEM_COST: API to  create Item Cost : But API is more used in the function
   --
   -- *********************************************************************************


   PROCEDURE CREATE_ITEM_COST (p_Inventory_Item_ID   IN            NUMBER,
                               p_Organization_ID     IN            NUMBER,
                               p_Item_Cost           IN            NUMBER,
                               x_Return_Status          OUT NOCOPY VARCHAR2,
                               x_Msg_Data               OUT NOCOPY VARCHAR2,
                               x_Msg_Count              OUT NOCOPY NUMBER);

   -- *********************************************************************************
   -- PROCEDURE SET_ITEM_NUMBER_NULL_SO: API to  assign NULL value  Global Temp Table when parts
   --  created from Sales ORder
   --
   -- *********************************************************************************

   PROCEDURE SET_ITEM_NUMBER_NULL_SO (P_TRANSACTION_ID IN NUMBER);

   -- *********************************************************************************
   -- PROCEDURE SET_ITEM_NUMBER_NULL_RO: API to  assign NULL value  Global Temp Table when parts
   --  created from Repair ORder
   --
   -- *********************************************************************************


   PROCEDURE SET_ITEM_NUMBER_NULL_RO (P_TRANSACTION_ID IN NUMBER);


   -- *********************************************************************************
   -- PROCEDURE SET_ITEM_NUMBER: API to  Updates values to Shared Variabls
   -- *********************************************************************************

   PROCEDURE SET_ITEM_NUMBER (P_INVENTORY_ITEM_ID   IN NUMBER,
                              P_ITEM_NUMBER         IN VARCHAR2,
                              P_TRANSACTION_TYPE    IN VARCHAR2,
                              P_ORGANIZATION_ID     IN NUMBER,
                              P_ORGANIZATION_CODE   IN VARCHAR2,
                              P_TRANSACTION_ID      IN NUMBER);

   -- *********************************************************************************
   -- FUNCTION GET_ITEM_NUMBER: Gets Item Number from Shared Variables table
   -- *********************************************************************************

   FUNCTION GET_ITEM_NUMBER (P_TRANSACTION_ID     IN NUMBER,
                             P_TRANSACTION_TYPE   IN VARCHAR2)
      RETURN VARCHAR2;


   -- *********************************************************************************
   -- FUNCTION GET_INVENTORY_ITEM_ID: Gets Item Item Id from Shared Variables table
   -- *********************************************************************************

   FUNCTION GET_INVENTORY_ITEM_ID (P_TRANSACTION_ID     IN NUMBER,
                                   P_TRANSACTION_TYPE   IN VARCHAR2)
      RETURN NUMBER;

   -- *********************************************************************************
   -- FUNCTION GET_ITEM_UOM: Gets Item UOM Code for Item Master Record
   -- *********************************************************************************


   FUNCTION GET_ITEM_UOM (P_INVENTORY_ITEM_ID IN NUMBER)
      RETURN VARCHAR2;

   -- *********************************************************************************
   -- FUNCTION GET_ITEM_UOM_DESC: Gets Item Description  for Item Master Record
   -- *********************************************************************************


   FUNCTION GET_ITEM_UOM_DESC (P_INVENTORY_ITEM_ID IN NUMBER)
      RETURN VARCHAR2;

   -- *********************************************************************************
   -- PROCEDURE CREATE_ITEM : Procedure to create Item On fly
   -- *********************************************************************************


   PROCEDURE CREATE_ITEM (
      p_Parts_On_Fly_REc   IN OUT NOCOPY Parts_On_Fly_Rec,
      x_Return_Status         OUT NOCOPY VARCHAR2,
      x_Msg_Data              OUT NOCOPY VARCHAR2,
      x_Msg_Count             OUT NOCOPY NUMBER);

   --*****************************************************************************************
   -- Program to Inactivate  Items whose ITEM_TYPE IS SPECIALS and that are created 90 ( P_DAYS_AFTER_CREATION )
   --*****************************************************************************************
   PROCEDURE Inactivate_Items (errbuf                     OUT VARCHAR2,
                               retcode                    OUT NUMBER,
                               P_DAYS_AFTER_CREATION   IN     NUMBER);

   --*****************************************************************************************
   -- Program to Move items from Manufacturer Cross Reference to Vendor Cross Reference
   --*****************************************************************************************
   PROCEDURE Update_Cross_Reference;

END XXWC_INV_PARTS_ON_FLY_PKG;
/
