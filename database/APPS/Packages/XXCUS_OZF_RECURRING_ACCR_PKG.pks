/**************************************************************************
   $Header xxcus_ozf_recurring_accr_pkg $
   Module Name: ORACLE TRADE MANAGEMENT

   PURPOSE:   RECURRING ACCRUAL PROCESS.
   REVISIONS:
   Ver        Date        Author             	Description
   ---------  ----------  ---------------   	-------------------------
    1.0       10/23/2018  Bala Seshadri   	Initial Build - Task ID: 20180116-00033
/*************************************************************************/
CREATE OR REPLACE PACKAGE APPS.xxcus_ozf_recurring_accr_pkg is
 --
 procedure Validate (p_ok OUT boolean, p_rec_seq IN number);
 --
 procedure Main
   ( 
      errbuff           out      varchar2,
      retcode          out      number,
      p_mode         in varchar2,
      p_record_seq in number default null,
      p_debug in varchar2 default 'N'      
   );
 --
end xxcus_ozf_recurring_accr_pkg;
/