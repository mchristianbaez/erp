CREATE OR REPLACE PACKAGE APPS.XXWC_INV_EXP_CORRECT_PKG
AS
   /*************************************************************************
     $Header XXWC_INV_EXP_CORRECT_PKG.pks $
     Module Name: XXWC_INV_EXP_CORRECT_PKG

     PURPOSE: This Package will create the Reversal Miscelleneious
                 transaction receipts
     ESMS Task Id :   20140717-00061

     REVISIONS:
     Ver        Date        Author                Description
     ---------  ----------  ------------------    ----------------
     1.0        07/21/2014  Pattabhi Avula        Initial Version
	
   **************************************************************************/

   PROCEDURE create_correction_txn (
       errbuff                 OUT  NOCOPY   VARCHAR2,
	   retcode                 OUT  NOCOPY   VARCHAR2,
       p_requisition_num       IN     po_requisition_headers_all.segment1%TYPE,
	   p_iso_num               IN     oe_order_headers_all.order_number%TYPE,
       p_dest_org              IN     mtl_parameters.organization_code%TYPE,
       p_item_num              IN     mtl_system_items_b.segment1%TYPE
                                     );

END XXWC_INV_EXP_CORRECT_PKG;
/
