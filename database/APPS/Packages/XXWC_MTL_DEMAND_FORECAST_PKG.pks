CREATE OR REPLACE PACKAGE APPS.XXWC_MTL_DEMAND_FORECAST_PKG
/*************************************************************************************************************************************
  $Header XXWC_MTL_DEMAND_FORECAST_PKG.pks $
  Module Name: XXWC_MTL_DEMAND_FORECAST_PKG

  PURPOSE: Generate Demand Forecast

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    ------------------------------------------------------------------------------------
  1.0        31-Jul-2015  Manjula Chellappan    Initial Version TMS # 20150701-00047
  1.1        26-Mar-2017  P.Vamshidhar          TMS#20170228-00077 - Performance issue with XXWC MTL Generate Demand Forecast Program
*************************************************************************************************************************************/
AS
   FUNCTION Get_History_Start_Date (p_calendar_code   IN VARCHAR2,
                                    p_start_date      IN DATE,
                                    p_bucket_type     IN NUMBER,
                                    p_prev_periods    IN NUMBER)
      RETURN DATE;

   PROCEDURE load_forecast_items (p_retcode OUT NUMBER);

   PROCEDURE load_avg_demand (p_retcode OUT NUMBER);

   PROCEDURE generate_forecast (errbuf                 OUT VARCHAR2,
                                retcode                OUT VARCHAR2,
                                p_organization_id   IN     NUMBER,
                                p_forecast          IN     VARCHAR2);

   PROCEDURE generate_forecast_main (errbuf    OUT VARCHAR2,
                                     retcode   OUT VARCHAR2);

   PROCEDURE Submit_forecast (
      errbuf                      OUT VARCHAR2,
      retcode                     OUT VARCHAR2,
      p_organization_id        IN     NUMBER,
      p_forecast_designator    IN     VARCHAR2,
      p_branch_dc_mode         IN     VARCHAR2,
      p_item_range             IN     VARCHAR2,
      p_item_category          IN     VARCHAR2,
      p_inventory_item_id      IN     NUMBER DEFAULT NULL,
      p_bucket_type            IN     NUMBER DEFAULT 3,
      p_start_date             IN     VARCHAR2,
      p_forecast_periods       IN     NUMBER DEFAULT 12,
      p_history_periods        IN     NUMBER DEFAULT 12,
      p_constant_seasonality   IN     NUMBER DEFAULT 0,
      p_seasonality_1          IN     NUMBER DEFAULT 0,
      p_seasonality_2          IN     NUMBER DEFAULT 0,
      p_seasonality_3          IN     NUMBER DEFAULT 0,
      p_seasonality_4          IN     NUMBER DEFAULT 0,
      p_seasonality_5          IN     NUMBER DEFAULT 0,
      p_seasonality_6          IN     NUMBER DEFAULT 0,
      p_seasonality_7          IN     NUMBER DEFAULT 0,
      p_seasonality_8          IN     NUMBER DEFAULT 0,
      p_seasonality_9          IN     NUMBER DEFAULT 0,
      p_seasonality_10         IN     NUMBER DEFAULT 0,
      p_seasonality_11         IN     NUMBER DEFAULT 0,
      p_seasonality_12         IN     NUMBER DEFAULT 0);
	  
-- Below Procedure added in Rev 1.1
PROCEDURE create_drop_indexes (p_event IN VARCHAR2); 
	  
END XXWC_MTL_DEMAND_FORECAST_PKG;
/