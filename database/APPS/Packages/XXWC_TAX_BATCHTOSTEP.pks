--
-- XXWC_TAX_BATCHTOSTEP  (Package) 
--
CREATE OR REPLACE PACKAGE APPS."XXWC_TAX_BATCHTOSTEP"
AS
   --XXWC_TAX_BATCHTOSTEP
   --=================================================================
   --  Copyright (c) 2012 HD SUPLY
   --=================================================================
   -- File Name: XXWC_TAX_BATCHTOSTEP
   --
   -- PROGRAM TYPE: PL/SQL Script   <API>
   --
   -- PURPOSE: Package for updating of cusotmers in step from AR
   --
   -- HISTORY
   -- ================================================================
   --         Last Update Date : 03/08/2012
   -- ================================================================
   -- ================================================================
   -- VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- ---------------------------------
   -- 1.0     04-Mar-2012   Manny         Created this package.

   /**************************************************************************/



   PROCEDURE stepaside (errbuf OUT VARCHAR2, retcode OUT NUMBER);
-- -----------------------------------------------------------------------------
-- |----------------------------< stepaside >------------------------------|
-- -----------------------------------------------------------------------------
--
-- {Start Of Comments}
--
-- Description:
--   This is a procedure to ensure we monitor the products assigned to the
--   tax excemption segment values/child ranges and pass the new values out to the
--   step system.



END XXWC_TAX_BATCHTOSTEP;
/

