CREATE OR REPLACE PACKAGE APPS.XXWC_EGO_REJECTITEMS_PURGE_PKG
IS
   /******************************************************************************************************
   -- File Name: XXWC_REJECTED_ITEMS_PURGE_PKG.pks
   --
   -- PROGRAM TYPE: PL/SQL Script   <API>
   --
   -- PURPOSE:
   -- HISTORY
   -- ======================================================================================
   -- ======================================================================================
   -- VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- ------------------------------------------------
   -- 1.0     12-Jun-2015   P.Vamshidhar    TMS#20150608-00031  - Single Item UI Enhancements
                                            Initial version.
    *******************************************************************************************************/

   PROCEDURE Main (x_errbuf           OUT VARCHAR2,
                   x_retcode          OUT NUMBER,
                   p_item_number   IN     VARCHAR2,
                   p_noof_days     IN     NUMBER);


   FUNCTION xxwc_delete_item (p_item_number     IN VARCHAR2,
                              p_change_notice   IN VARCHAR2,
                              p_change_id       IN NUMBER,
                              p_change_type     IN VARCHAR2,
							  p_item_id         IN NUMBER)
      RETURN VARCHAR2;
      
END XXWC_EGO_REJECTITEMS_PURGE_PKG;