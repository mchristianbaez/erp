CREATE OR REPLACE PACKAGE APPS.XXWC_ECE_POCO_PKG
AS
   /*************************************************************************
     $Header XXWC_ECE_POCO_PKG $
     Module Name: XXWC_ECE_POCO_PKG.pks

     PURPOSE: EDI - Add ability to bypass EDI when transmitting a canceled PO, and EDI Overriden PO

     REVISIONS:
     Ver        Date        Author                Description
     ---------  ----------  ------------------    ----------------
     1.0        12-Mar-15   Manjula Chellappan    Initial Version TMS # 20141113-00118
   **************************************************************************/

   PROCEDURE bypass_poco (p_po_header_id   IN NUMBER,
                          p_po_number   IN NUMBER);

   PROCEDURE extract_poco_outbound (
      errbuf                  OUT VARCHAR2,
      retcode                 OUT VARCHAR2,
      p_output_path        IN     VARCHAR2,      
      p_po_number_from     IN     VARCHAR2,
      p_po_number_to       IN     VARCHAR2,
      p_crdate_from        IN     VARCHAR2,
      p_crdate_to          IN     VARCHAR2,
      p_pc_type            IN     VARCHAR2,
      p_vendor_name        IN     VARCHAR2,
      p_vendor_site_code   IN     VARCHAR2,
      p_debug_mode         IN     NUMBER DEFAULT 0);
END;