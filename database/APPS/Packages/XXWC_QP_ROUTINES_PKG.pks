CREATE OR REPLACE PACKAGE APPS.XXWC_QP_ROUTINES_PKG
AS
   /*************************************************************************
        *   Procedure : GET_ORDER_SUB_TOTAL
        *
        *   PURPOSE:   This procedure return order sub total excluding Freight and Tzxes
        *  Author: Raghav Velicheti (TMS # 20130121-00460)
        * Creation_date 4/3/2013
        *Last update Date 4/3/2013
        *   Parameter:
        *          IN
        *              p_Header_id
        * ************************************************************************/
   FUNCTION XXWC_GET_TOT_MINUS (p_header_id IN NUMBER)
      RETURN NUMBER;

   /*************************************************************************
        *   Function : is_row_locked
        *
        *   PURPOSE:   This function identifies if a record has ben locked by user
        *  Author: Ram Talluri- TMS #20131008-00446
        * Creation_date 11/4/2013
        * Last update Date 11/4/2013
        *   Parameter:
        *          IN rowid, table_name
        *
        * ************************************************************************/
   FUNCTION is_row_locked (v_rowid ROWID, table_name VARCHAR2)
      RETURN VARCHAR2;

   /*************************************************************************
        *   Procedure : upd_calc_price_flag
        *
        *   PURPOSE:   This procedure sets calculate freeze flag of orde_line to Freeze Price.
        *  Author: Ram Talluri- TMS #20131008-00446
        * Creation_date 11/4/2013
        * Last update Date 11/4/2013
        *   Parameter:
        *          IN  p_delta_date
        *
        * ************************************************************************/
   PROCEDURE upd_calc_price_flag (errbuf              OUT VARCHAR2,
                                  retcode             OUT NUMBER,
                                  p_last_delta_date       VARCHAR2,
                                  p_header_id             NUMBER);

   /*************************************************************************
        *   Procedure : write_log
        *
        *   PURPOSE:   This procedure prints the string in log file.
        *  Author: Ram Talluri- TMS #20131008-00446
        * Creation_date 11/4/2013
        * Last update Date 11/4/2013
        *   Parameter:
        *          IN p_debug_msg
        *
        * ************************************************************************/
   PROCEDURE write_log (p_debug_msg IN VARCHAR2);

   /*************************************************************************
        *   Procedure : write_error
        *
        *   PURPOSE:   This procedure reports the error.
        *  Author: Ram Talluri- TMS #20131008-00446
        * Creation_date 11/4/2013
        * Last update Date 11/4/2013
        *   Parameter:
        *          IN p_debug_msg
        *
        * ************************************************************************/
   PROCEDURE write_error (p_debug_msg IN VARCHAR2, p_call_point IN VARCHAR2);

    /*************************************************************************
     *   Procedure : upd_calc_price_flag
     *
     *   PURPOSE:   This procedure sets calculate freeze flag of orde_line to Freeze Price.
     *  Author: Ram Talluri- TMS #20131008-00446
     * Creation_date 11/4/2013
     * Last update Date 11/4/2013
     *   Parameter:
                IN p_header_id
     *
     * ************************************************************************/
   PROCEDURE upd_calc_price_flag_proc (p_header_id        NUMBER,
                                       p_order_type_id    NUMBER);

   /*************************************************************************
        *   Procedure : webber_product_catalog
        *
        *   PURPOSE:   This procedure generates Product catalog for customer Webber. .
        *  Author: Ram Talluri- TMS #20140225-00208
        * Creation_date 5/9/2014
        * Last update Date 5/9/2014
        *
        * ************************************************************************/
PROCEDURE webber_product_catalog (errbuf              OUT VARCHAR2,
                                  retcode             OUT NUMBER);
   /*************************************************************************
        *   Procedure : get_price_break
        *
        *   PURPOSE:   This procedure gets the quantity price break information for order line.
        *  Author: Ram Talluri- TMS #20140528-00018
        * Creation_date 6/9/2014
        * Last update Date 6/9/2014
        *
        * ************************************************************************/
FUNCTION get_price_break(p_line_id in NUMBER)
    RETURN VARCHAR2;

                                  
END XXWC_QP_ROUTINES_PKG;
/