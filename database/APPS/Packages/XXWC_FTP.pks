-- Start of DDL Script for Package APPS.XXWC_FTP
-- Generated 6/20/2012 5:31:26 PM from APPS@EBIZCON

CREATE OR REPLACE 
PACKAGE apps.xxwc_ftp AS
/**************************************************************************<br/>
 * xxwc                                       All rights reserved.<br/>
                              <br/>
 * Desc.  : Provides FTP functionality to PL/SQL programs                  <br/>
 **************************************************************************<br/>

 ******************************************************************************/

/**
 * Disconnects FTP session
 */
PROCEDURE close_ftp
AS
  LANGUAGE java
    NAME 'XXWC_Ftp.close()';

/**
 * Opens FTP server and logs in
 */
PROCEDURE ftpConnect(Server VARCHAR2, User VARCHAR2, Password VARCHAR2)

AS
  LANGUAGE java
    NAME 'XXWC_Ftp.ftpConnect(java.lang.String, java.lang.String, java.lang.String)  ';

/**
 * Retrieves the specified file from the server in ASCII mode.
 *
 * @param file_to_get name of the file to be retrieved
 * @param local_file_name name of file on local machine can be a full path.
 *                        If only filename specified then will be stored in
 *                        current directory on local machine (not what you
 *                        want when calling from inside Oracle).
 * @param separator value used to terminate lines. Pass chr(10).
 */
PROCEDURE getAsciiFile(file_to_get VARCHAR2, local_file_name VARCHAR2, separator VARCHAR2)
AS
  LANGUAGE java
    NAME 'XXWC_Ftp.getAsciiFile(java.lang.String, java.lang.String, java.lang.String)';

/**
 * Retrieves the specified file from the server in binary mode.
 *
 * @param file_to_get name of the file to be retrieved
 * @param local_file_name name of file on local machine can be a full path.
 *                        If only filename specified then will be stored in
 *                        current directory on local machine (not what you
 *                        want when calling from inside Oracle).
 * @param separator value used to terminate lines. Pass chr(10).
 */
PROCEDURE getBinaryFile(file_to_get VARCHAR2, local_file_name VARCHAR2)
AS
  LANGUAGE java
    NAME 'XXWC_Ftp.getBinaryFile(java.lang.String, java.lang.String)';

/**
 * Returns a list of the files in the current directory on the FTP server.
 *
 * @param r1 Table of type "file_list" which can be found in the OracleCommon
 *           types folder.
 * @return The number of files in the directory?
 */
FUNCTION  getDirectoryContent(r1 out xxwc.xxwc_file_list)
 RETURN NUMBER
AS
  LANGUAGE java
    NAME 'XXWC_Ftp.getDirectoryContent(oracle.sql.ARRAY[]) return int';

/**
 * Changes to the specified directory on the FTP server.
 */
PROCEDURE setDirectory(directory_name VARCHAR2)
AS
  LANGUAGE java
    NAME 'XXWC_Ftp.setDirectory(java.lang.String)';

/**
 * Deletes the requested file from the FTP server.
 */
PROCEDURE fileDelete(filename VARCHAR2)
AS
  LANGUAGE java
    NAME 'XXWC_Ftp.fileDelete(java.lang.String)';

/**
 * Renames the requested file on the FTP server.
 */
PROCEDURE fileRename(oldfilename VARCHAR2, newfilename VARCHAR2)
AS
  LANGUAGE java
    NAME 'XXWC_Ftp.fileRename(java.lang.String, java.lang.String)';

/**
 * Creates the requested directory on the FTP server.
 */

PROCEDURE makeDirectory(directoryname VARCHAR2)
AS
  LANGUAGE java
    NAME 'XXWC_Ftp.makeDirectory(java.lang.String)';

/**
 * Uploads the specified data to the server in ASCII mode.
 *
 * @param filename Name of the file to be created on the server.
 * @param content The content to be put into the file on the server.
 * @param separator Value used to terminate lines. Pass chr(10).
 */
PROCEDURE putAsciiFile(filename VARCHAR2, content VARCHAR2,separator VARCHAR2)
AS
  LANGUAGE java
    NAME 'XXWC_Ftp.putAsciiFile(java.lang.String, java.lang.String, java.lang.String)';

/**
 * Uploads the specified file to the server in ASCII mode.
 *
 * @param local_file Name of the file to be uploaded to the server.
 * @param remote_file Name of the file to be created on the server.
 */
PROCEDURE putBinaryFile(local_file VARCHAR2, remote_file VARCHAR2)
AS
  LANGUAGE java
    NAME 'XXWC_Ftp.putBinaryFile(java.lang.String, java.lang.String)';


END;
/



-- End of DDL Script for Package APPS.XXWC_FTP

