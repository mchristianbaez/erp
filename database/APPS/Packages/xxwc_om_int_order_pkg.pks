CREATE OR REPLACE PACKAGE APPS.xxwc_om_int_order_pkg
AS
   /*************************************************************************
     $Header xxwc_om_int_orders_pkg $
     Module Name: xxwc_om_int_orders_pkg.pks

     PURPOSE:   This package is used for changes to internal orders

     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.0        08/07/2012  Shankar Hariharan        Initial Version
     2.0       6/4/2014    Ram Talluri              Added ISO_ACCEPTED_BY 
     3.0       02/03/2016    Rakesh Patel           TMS# 20150316-00051 internal order pick report performance issue
   **************************************************************************/
   PROCEDURE cancel_bo_line (errbuf OUT VARCHAR2, retcode OUT NUMBER);
   /*************************************************************************
        *   Procedure : iso_accepted_by
        *
        *   PURPOSE:   This procedure updates accepted by and accepted date to internal order lines. .
        *  
        *   REVISIONS:
        *    Ver        Date        Author                     Description
        *     ---------  ----------  ---------------         -------------------------
        *    1.0        7/24/2014  Ram Talluri               Initial Version - TMS#20140130-00144 
        *       
        * ************************************************************************/
PROCEDURE iso_accepted_by  (errbuf              OUT VARCHAR2,
                            retcode             OUT NUMBER,
                            p_last_delta_date       VARCHAR2,
                            p_header_id             NUMBER);

-- Version# 3.0 > Start
 /*************************************************************************
*   Function : submit_internal_order_pick_rpt
*
*   PURPOSE:   This procedure is called from Standard Header WF Node at header Level.
*   Checks if it is interneal order then submit the report else do nothing.
*  Ver        Date        Author                     Description
*  ---------  ----------  ---------------         -------------------------
* 3.0         02/03/2016  Rakesh Patel            Initial Version MS Ticket 20150316-00051
* ************************************************************************/
   PROCEDURE submit_internal_order_pick_rpt (
      itemtype    IN              VARCHAR2,
      itemkey     IN              VARCHAR2,
      actid       IN              NUMBER,
      funcmode    IN              VARCHAR2,
      resultout   IN OUT NOCOPY   VARCHAR2
   ); 
-- Version# 3.0 < End   
END xxwc_om_int_order_pkg;

/