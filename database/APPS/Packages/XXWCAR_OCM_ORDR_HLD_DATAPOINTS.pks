CREATE OR REPLACE PACKAGE APPS.xxwcar_ocm_ordr_hld_datapoints
AS
   /***************************************************************************
           *    script name: xxwcar_ocm_ordr_hld_datapoints
           *
           *    functional purpose: Used to Create additional data points in
           *                       Oracle credit management
           *    history:
           *
           *    version    date              author             description
           ********************************************************************
           *    1.0        31-May-2012  Steve Griffin     initial development.
           *    1.1        05-Jun-2012  Shankar Hariharan Bug fixes
           *    1.2        11-Jun-2012  Shankar Hariharan Additional functions per
           *                                              Steve's request
           *    1.3        25-Mar-2013  Shankar Hariharan Additional functions per
           *                                              Steve's request
           *    1.4        20-May-2013 Shankar Hariharan  Additional data points per tms ticket
           *                                              for simplifying credit holds
           *    1.5        15-Oct-2015 Kishorebabu V      TMS# 20150819-00054 AR - Credit Holds causing issues with billing Rentals
           *******************************************************************/

   TYPE get_nto_data_rec IS RECORD
   (
      attribute_category   VARCHAR2 (30)
    ,                                                               --NTO flag
     attribute14           VARCHAR2 (30)
    ,                                                  --Job info on file flag
     attribute20           VARCHAR2 (30)
    ,                                                               --NTO date
     attribute19           VARCHAR2 (30)
    ,                                                             --NTO number
     attribute18           VARCHAR2 (30)               --Job info on file flag
   );

   FUNCTION xxocm_NTO_flag (x_resultout      OUT NOCOPY VARCHAR2
                          , x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN VARCHAR2;

   FUNCTION xxocm_job_info_flag (x_resultout      OUT NOCOPY VARCHAR2
                               , x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN VARCHAR2;

   FUNCTION xxocm_NTO_date (x_resultout      OUT NOCOPY VARCHAR2
                          , x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN VARCHAR2;

   FUNCTION xxocm_NTO_number (x_resultout      OUT NOCOPY VARCHAR2
                            , x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN VARCHAR2;

   FUNCTION xxocm_NTO_job_total (x_resultout      OUT NOCOPY VARCHAR2
                               , x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN VARCHAR2;


   FUNCTION xxocm_AM_site (x_resultout      OUT NOCOPY VARCHAR2
                         , x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN VARCHAR2;


   FUNCTION xxocm_total_ar_site (x_resultout      OUT NOCOPY VARCHAR2
                               , x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN NUMBER;

   FUNCTION xxocm_current_site (x_resultout      OUT NOCOPY VARCHAR2
                              , x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN NUMBER;

   FUNCTION xxocm_1_to_30_site (x_resultout      OUT NOCOPY VARCHAR2
                              , x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN NUMBER;

   FUNCTION xxocm_31_to_60_site (x_resultout      OUT NOCOPY VARCHAR2
                               , x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN NUMBER;

   FUNCTION xxocm_61_to_90_site (x_resultout      OUT NOCOPY VARCHAR2
                               , x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN NUMBER;

   FUNCTION xxocm_91_to_180_site (x_resultout      OUT NOCOPY VARCHAR2
                                , x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN NUMBER;

   FUNCTION xxocm_181_to_360_site (x_resultout      OUT NOCOPY VARCHAR2
                                 , x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN NUMBER;

   FUNCTION xxocm_360_plus_site (x_resultout      OUT NOCOPY VARCHAR2
                               , x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN NUMBER;



   FUNCTION xxocm_total_ar_acct (x_resultout      OUT NOCOPY VARCHAR2
                               , x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN NUMBER;

   FUNCTION xxocm_current_acct (x_resultout      OUT NOCOPY VARCHAR2
                              , x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN NUMBER;

   FUNCTION xxocm_1_to_30_acct (x_resultout      OUT NOCOPY VARCHAR2
                              , x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN NUMBER;

   FUNCTION xxocm_31_to_60_acct (x_resultout      OUT NOCOPY VARCHAR2
                               , x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN NUMBER;

   FUNCTION xxocm_61_to_90_acct (x_resultout      OUT NOCOPY VARCHAR2
                               , x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN NUMBER;

   FUNCTION xxocm_91_to_180_acct (x_resultout      OUT NOCOPY VARCHAR2
                                , x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN NUMBER;

   FUNCTION xxocm_181_to_360_acct (x_resultout      OUT NOCOPY VARCHAR2
                                 , x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN NUMBER;

   FUNCTION xxocm_360_plus_acct (x_resultout      OUT NOCOPY VARCHAR2
                               , x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN NUMBER;


   FUNCTION xxocm_cust_since_acct (x_resultout      OUT NOCOPY VARCHAR2
                                 , x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN VARCHAR2;

   FUNCTION xxocm_location (x_resultout      OUT NOCOPY VARCHAR2
                          , x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN VARCHAR2;

   FUNCTION xxocm_site_number (x_resultout      OUT NOCOPY VARCHAR2
                             , x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN VARCHAR2;

   FUNCTION xxocm_requestor (x_resultout      OUT NOCOPY VARCHAR2
                           , x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN VARCHAR2;


   FUNCTION xxocm_last_pmt_date_site (x_resultout      OUT NOCOPY VARCHAR2
                                    , x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN VARCHAR2;

   FUNCTION xxocm_last_pmt_amt_site (x_resultout      OUT NOCOPY VARCHAR2
                                   , x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN NUMBER;

   FUNCTION xxocm_last_pmt_date_acct (x_resultout      OUT NOCOPY VARCHAR2
                                    , x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN VARCHAR2;

   FUNCTION xxocm_last_pmt_amt_acct (x_resultout      OUT NOCOPY VARCHAR2
                                   , x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN NUMBER;

   FUNCTION xxocm_dso_acct (x_resultout      OUT NOCOPY VARCHAR2
                                   , x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN NUMBER;

   FUNCTION xxocm_acct_credit_hold (x_resultout      OUT NOCOPY VARCHAR2
                                   , x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN VARCHAR2;

   FUNCTION xxocm_site_credit_hold (x_resultout      OUT NOCOPY VARCHAR2
                                   , x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN VARCHAR2;

   FUNCTION xxocm_credit_rating  (x_resultout      OUT NOCOPY VARCHAR2
                                   , x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN VARCHAR2;

   FUNCTION xxocm_order_amount      (x_resultout      OUT NOCOPY VARCHAR2
                                   , x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN NUMBER;

/* Ver#1.6  20150819-00054 --- AR - Credit Holds causing issues with billing Rentals */   --Start
   FUNCTION xxocm_rental_order_type  (x_resultout      OUT NOCOPY VARCHAR2
                                     ,x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN VARCHAR2;

   FUNCTION xxocm_payment_on_account  (x_resultout      OUT NOCOPY VARCHAR2
                                      ,x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN VARCHAR2;
/* Ver#1.6  20150819-00054 --- AR - Credit Holds causing issues with billing Rentals */   --End

END xxwcar_ocm_ordr_hld_datapoints;
/