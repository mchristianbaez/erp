CREATE OR REPLACE PACKAGE APPS.xxwc_ar_silverpop_intf_pkg
AS
   /*************************************************************************

   **************************************************************************
     $Header xxwc_ar_silverpop_intf_pkg $
     Module Name: xxwc_ar_silverpop_intf_pkg.pks

     PURPOSE:   This package is called by the concurrent programs
                XXWC Silver Pop Invoice Outbound Interface

     REVISIONS:
     Ver        Date        Author             Description
     ---------  ----------  ---------------   -------------------------
      1.0       29/01/2016  Neha Saini         Initial Build - TMS# 20160211-00160

   /*************************************************************************



   /*************************************************************************
     Procedure : generate_invoice_file

     PURPOSE   :This procedure creates file for the invoice data extract 
                for Silver Pop


   ************************************************************************/
   PROCEDURE generate_invoice_file (
      errbuf                OUT      VARCHAR2,
      retcode               OUT      VARCHAR2,
      p_directory_name      IN       VARCHAR2,
      p_from_date           IN       VARCHAR2,
      p_to_date             IN       VARCHAR2
   );
END xxwc_ar_silverpop_intf_pkg;
/