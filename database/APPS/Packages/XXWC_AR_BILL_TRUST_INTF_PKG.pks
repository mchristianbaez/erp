--
-- XXWC_AR_BILL_TRUST_INTF_PKG  (Package) 
--
CREATE OR REPLACE PACKAGE APPS.xxwc_ar_bill_trust_intf_pkg
AS
   /*************************************************************************
    Copyright (c) 2012 Lucidity Consulting Group
    All rights reserved.
   **************************************************************************
     $Header xxwc_ar_bill_trust_intf_pkg $
     Module Name: xxwc_ar_bill_trust_intf_pkg.pks

     PURPOSE:   This package is called by the concurrent programs
                XXWC Bill Trust Open Invoice Outbound Interface
                XXWC Bill Trust Monthly Statements Outbound Interface

     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.0        01/11/2012  Consuelo Gonzalez      Initial Version
     2.0        05/15/2012  Consuelo Gonzalez      Modified to integrate
                                                   viveks initial code
     3.0        06/06/2012  Consuelo Gonzalez      Altered to loop prism notes
                                                   with prism original line number
                                                   Changes to pull the PRISM
                                                   Rental dates from new stg tbls
     4.0        06/08/2012  Consuelo Gonzalez      Altered to pull individual
                                                   PRISM notes per line instead
                                                   of grouping and single record
                                                   Modify number format for dollar
                                                   amounts to be 999,999,999.00
     5.0        06/18/2012  Consuelo Gonzalez      Modified gen_open_invoice_file to:
                                                   * Pull notes from OM directly from
                                                     order lines
                                                   * Altered header level where clause
                                                     to exclude source for late charges
     6.0        07/03/2012  Consuelo Gonzalez      Modified gen_open_balance to alter
                                                   invoice numbers created prior to 06/14
                                                   to BT, they were sent by Prism and number
                                                   is different from the one in EBS.
     7.0        07/11/2012  Consuelo Gonzalez      Modified gen_open_balance to alter
                                                   invoice number created prior to 06/14
                                                   only removing -00
     8.0        08/08/2012  Consuelo Gonzalez      Changed currency format to 999,999,999,000
     9.0        08/15/2012  Consuelo Gonzalez      Changed unit price rounding to be dependent
                                                   on decimal places with a minimum of 2 and
                                                   a max of 5. Trimming trailing 0s.
    10.0        09/06/2012  Consuelo Gonzalez      gen_open_invoice_file: Added section to OM 
                                                   Line notes to pull in serial numbers in the 
                                                   WDD Attribute1 field                                                   
    11.0        09/28/2012  Consuelo Gonzalez      gen_open_invoice_file: changed inclusion
                                                   exclusion of CM transactions
    12.0        10/11/2012  Consuelo Gonzalez      Changes for CP# 872/ TMS 20121217-00785: to show CM Application
                                                   Changes for CP# 923/ TMS 20121217-00710: for repair order tool detail
    13.0        12/20/2012  Consuelo Gonzalez      Changes for CP# 2001/ TMS 20121217-00744: Added GSA Notes from the order line
                                                   Changes for CP# 2030/ TMS 20121217-00640: Changes for C! 1% Surcharge in Comm
    14.0        02/18/2013  Consuelo Gonzalez      Changes for CP# 872/ TMS 20121217-00785: to dispaly CM Remaining balance instead
                                                            of original amounts as with regular invoices.
                                                   Changes for TMS 20130214-01693: To remove line feed from item description                                                   
    15.0        12/20/2013  Gopi Damuluri          TMS# 20131016-00409
                                                   -- Add CountryOfOrigin
                                                   -- Add DeliveryId
                                                   -- Add GSA Compliance and TAA Compliance
   **************************************************************************/

   /*************************************************************************
     Procedure : gen_open_invoice_file

     PURPOSE:   This procedure creates file for the open invoices to
                Bill Trust
     Parameter:

   ************************************************************************/
   PROCEDURE gen_open_invoice_file (
      errbuf             OUT      VARCHAR2,
      retcode            OUT      VARCHAR2,
      p_directory_name   IN OUT   VARCHAR2,
      p_file_name        IN OUT   VARCHAR2,
      p_from_date        IN       VARCHAR2,
      p_to_date          IN       VARCHAR2
   );

   /*************************************************************************
     Procedure : gen_statements_file

     PURPOSE:   This procedure creates file for the Monthly Statements to
                Bill Trust
     Parameter:

   ************************************************************************/
   PROCEDURE gen_statements_file (
      errbuf             OUT      VARCHAR2,
      retcode            OUT      VARCHAR2,
      p_directory_name   IN OUT   VARCHAR2,
      p_file_name        IN OUT   VARCHAR2
   );

   /**************************************************************************
   *   $Header xxwc_billtrust_interface_pkg.pks $
   *   Module Name: xxwc_billtrust_interface_pkg.pks
   *
   *   PURPOSE:   This package is called by the concurrent program XXWC Billtrust Open Balance Outbound Interface
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        11/30/2011  Vivek Lakaman             Initial Version
   * ************************************************************************/

   /*************************************************************************
   *   Procedure : open_balance
   *
   *   PURPOSE:   This procedure creates file for the open balance
   *   Parameter:
   *          IN
   *              p_debug_msg      -- Debug Message
   * ************************************************************************/
   PROCEDURE open_balance (
      errbuf              OUT      VARCHAR2,
      retcode             OUT      VARCHAR2,
      px_directory_name   IN OUT   VARCHAR2,
      px_file_name        IN OUT   VARCHAR2
   );

     /*************************************************************************
   *   Procedure : get_open_balance
   *
   *   PURPOSE:   This procedure get the amount remaining for the invoice/receipts
   *   Parameter:
   *          IN
   *              p_debug_msg      -- Debug Message
   * ************************************************************************/
   FUNCTION get_open_balance (p_trx_type IN VARCHAR2, p_trx_id IN NUMBER)
      RETURN NUMBER;

     /*************************************************************************
   *   Procedure : submit_job
   *
   *   PURPOSE:   This procedure is called by UC4 to initiate the BT Interfaces
   *   Parameter:
   *          IN
   *              p_debug_msg      -- Debug Message
   * ************************************************************************/
   PROCEDURE submit_job (
      errbuf                  OUT      VARCHAR2,
      retcode                 OUT      VARCHAR2,
      p_user_name             IN       VARCHAR2,
      p_responsibility_name   IN       VARCHAR2,
      p_program_name          IN       VARCHAR2,
      p_ob_directory_name     IN       VARCHAR2,
      p_operating_unit_name   IN       VARCHAR2,
      p_rcp_file_name         IN       VARCHAR2
   );

-- Version# 15.0 > Start
   -- Function to derive DeliveryId detals
   FUNCTION get_delivery_id (p_cust_trx_id IN NUMBER) return VARCHAR2;
-- Version# 15.0 < End

END xxwc_ar_bill_trust_intf_pkg;
/


GRANT EXECUTE, DEBUG ON APPS.XXWC_AR_BILL_TRUST_INTF_PKG TO INTERFACE_PRISM;

GRANT EXECUTE, DEBUG ON APPS.XXWC_AR_BILL_TRUST_INTF_PKG TO INTERFACE_XXCUS;

