/* Formatted on 01-Mar-2013 17:24:49 (QP5 v5.206) */
-- Start of DDL Script for Package APPS.XXWC_HELPERS
-- Generated 01-Mar-2013 17:24:47 from APPS@EBIZFQA

CREATE OR REPLACE PACKAGE apps.xxwc_helpers
IS
    --Package will keep usefull XXWC function and procedures ;
    --Started By Rasikha Galimova at 1-march-2013
    --********************************************
    --********************************************
    -- FUNCTION remove_special_chars will remove all characters from string based on
    -- ASCII value starting from 33-124(keep oly ASCII in this interval);
    FUNCTION remove_special_chars (p_string IN VARCHAR2)
        RETURN VARCHAR2;

    --********************************************
    --if called from apps will put to log file, from database to the spool- by Rasikha, 1-march-2013.
    --********************************************
    PROCEDURE xxwc_log_debug (p_message VARCHAR2);

    --********************************************
    --log errors to    XXWC error system
    --*******************************************
    PROCEDURE xxwc_log_error (p_message           CLOB
                             ,p_package_name      VARCHAR2
                             ,p_procedure_name    VARCHAR2
                             ,p_module_name       VARCHAR2);
END;                                                                                                     -- Package spec
/

-- End of DDL Script for Package APPS.XXWC_HELPERS
