CREATE OR REPLACE 
PACKAGE      APPS.XXWC_INTANGBLE_ITEM_CORRECTION
AS
   /*************************************************************************
   *   $Header XXWC_INTANGBLE_ITEM_CORRECTION.PKG $
   *   Module Name: XXWC_INTANGBLE_ITEM_CORRECTION.PKG
   *
   *   PURPOSE:   This package is used update the intangible items status
   *              and corrected the SO and PO open line items
   *
   *   REVISIONS:
   *   Ver     Date        Author                Description
   *   ------  ----------  ---------------       -------------------------
   *   1.0     08/09/2018  Pattabhi Avula        Initial Version -- TMS#20180808-00080
   * ***************************************************************************/
   
PROCEDURE MAIN(p_errbuf                  OUT VARCHAR2,
               p_retcode                 OUT VARCHAR2,
			   p_item_id                  IN NUMBER,
			   p_organization_id          IN NUMBER,
			   p_days_old                 IN NUMBER
				  );
   
PROCEDURE SO_CANCEL_PROCESS(p_errbuf                  OUT VARCHAR2,
                              p_retcode                 OUT VARCHAR2,
							  p_item_id                  IN NUMBER,
							  p_organization_id          IN NUMBER,
							  p_days_old                 IN NUMBER
							 );
							 
PROCEDURE SO_LINE_ADD(p_errbuf                  OUT VARCHAR2,
                      p_retcode                 OUT VARCHAR2
					 );
							 
PROCEDURE PURCHASE_ORD_PROCESS(p_errbuf                  OUT VARCHAR2,
                                 p_retcode                 OUT VARCHAR2,
								 p_item_id                  IN NUMBER,
								 p_organization_id          IN NUMBER,
								 p_days_old                 IN NUMBER
							 );
							 
PROCEDURE add_lines_to_po(p_errbuf                  OUT VARCHAR2,
                          p_retcode                 OUT VARCHAR2
						  );
						  
PROCEDURE item_status_udpate(p_errbuf                  OUT VARCHAR2,
                             p_retcode                 OUT VARCHAR2,
							 p_item_id                  IN NUMBER,
							 p_organization_id          IN NUMBER
							 );
   
END XXWC_INTANGBLE_ITEM_CORRECTION;
/