CREATE OR REPLACE PACKAGE apps.xxwc_customer_extract_pkg
AS
/**************************************************************************
 *
 * HEADER  
 *   Organization / Role / Member Group Interface to Commerce
 *
 * PROGRAM NAME
 *  XXWC_CUSTOMER_EXTRACT_PKG.pks
 * 
 * DESCRIPTION
 *  this package contains all of the procedures/functions used to generate organization, organization role 
 *    and organization member group extract files for the Commerce interface
 *
 * HISTORY
 * =======
 *
 * VERSION DATE        AUTHOR(S)       DESCRIPTION
 * ------- ----------- --------------- ------------------------------------
 * 1.00    10-OCT-2012 Scott Spivey    Lucidity Consulting Group - Creation
 * 1.10    22-OCT-2012 Scott Spivey    removed address book data file
 *                                     added organization role data file
 * 1.20    29-OCT-2012 Scott Spivey    added organization member group data file
 * 1.30    14-DEC-2012 Scott Spivey    split full extract logic and delta extract logic to simplfy queries
 *                                     handle scenario when inactivating primary bill-to from site level
 * 1.40    16-JAN-2013 Scott Spivey    correct logic for start/end active date range validation on price lists
 *
 * 1.50    18-JAN-2017 Pattabhi Avula  Added new procedure(ecomm_acct_request) for Ecomm Account link 
 * <additional records for modifications>
 *
 *************************************************************************/

   /**************************************************************************
    *
    * PROCEDURE
    *  Ecomm_Extract
    *
    * DESCRIPTION
    *  This procedure is called from a concurrent request to generate the organization/role
    *    extract files for the Commerce interface
    *
    * PARAMETERS
    * ==========
    * NAME                 TYPE      DESCRIPTION
   .* -----------------    --------  ---------------------------------------------
    * ERRBUF               VARCAHR2  completion comment
    * RETCODE              VARCHAR2  completion code (0=success  1=warning  2=error)
    * P_EXTRACT_METHOD     VARCHAR2  type of extract : FULL or DELTA
    * p_LAST_EXTRACT_DATE  VARCHAR2  canonical date format of last extract
    *
    *
    * CALLED BY
    *  XXWC Price Lists Extract to ECOMM concurrent program
    *
    *************************************************************************/
   PROCEDURE ecomm_extract (
      errbuf                   OUT VARCHAR2
     ,retcode                  OUT VARCHAR2
     ,p_extract_method      IN     VARCHAR2 DEFAULT 'DELTA'
     ,p_last_extract_date   IN     VARCHAR2);


   /**************************************************************************
    *
    * PROCEDURE
    *  Extract_Wrapper
    *
    * DESCRIPTION
    *  This procedure is called from UC4 to submit the concurrent request to generate the organization/role
    *    extract files for the Commerce interface
    *
    * PARAMETERS
    * ==========
    * NAME                 TYPE      DESCRIPTION
   .* -----------------    --------  ---------------------------------------------
    * ERRBUF               VARCAHR2  completion comment
    * RETCODE              VARCHAR2  completion code (0=success  1=warning  2=error)
    * P_EXTRACT_METHOD     VARCHAR2  type of extract : FULL or DELTA
    * p_LAST_EXTRACT_DATE  VARCHAR2  canonical date format of last extract
    *
    *
    * CALLED BY
    *  UC4 scheduling program
    *
    *************************************************************************/
   PROCEDURE extract_wrapper (
      errbuf                   OUT VARCHAR2
     ,retcode                  OUT VARCHAR2
     ,p_extract_method      IN     VARCHAR2 DEFAULT 'DELTA'
     ,p_last_extract_date   IN     VARCHAR2);
	 
	
   /**************************************************************************
    *
    * PROCEDURE
    *  ecomm_acct_request
    *
    * DESCRIPTION
    *  Procedure will use to Implement Automated Account Linking through File Upload Process
    *          for APEX
    *
    * PARAMETERS
    * ==========
    * NAME                 TYPE      DESCRIPTION
   .* -----------------    --------  ---------------------------------------------
    * ERRBUF               VARCAHR2  completion comment
    * RETCODE              VARCHAR2  completion code (0=success  1=warning  2=error)
    * p_session_id         VARCHAR2  type of extract Ecomm Link Account
    * 
    *
    *
    * CALLED BY
    *  XXWC EComm Account Linking Program
    *
    *************************************************************************/
  PROCEDURE ecomm_acct_request(X_ERRBUF      OUT VARCHAR2,
                               X_RETCODE     OUT VARCHAR2,
							   p_session_id  IN VARCHAR2);
 /**************************************************************************
    *
    * PROCEDURE
    *  ecomm_acct_request
    *
    * DESCRIPTION
    *  Procedure will call from APEX to Implement Automated Account Linking through File Upload Process
    *          for APEX
    *
    * PARAMETERS
    * ==========
    * NAME                 TYPE      DESCRIPTION
   .* -----------------    --------  ---------------------------------------------
    * ERRBUF               VARCAHR2  completion comment
    * RETCODE              VARCHAR2  completion code (0=success  1=warning  2=error)
    * p_session_id         VARCHAR2  type of extract Ecomm Link Account
    * 
    *
    *
    * CALLED BY
    *  XXWC EComm Account Linking Program from APEX
    *
    *************************************************************************/
							   
  PROCEDURE submit_job (p_session_id             IN     VARCHAR2);

END xxwc_customer_extract_pkg;
/

GRANT EXECUTE ON APPS.XXWC_CUSTOMER_EXTRACT_PKG TO INTERFACE_PRISM;

GRANT EXECUTE ON XXWC_CUSTOMER_EXTRACT_PKG TO INTERFACE_APEXWC;

GRANT SELECT ON APPS.HZ_CUST_ACCOUNTS_ALL TO INTERFACE_APEXWC;