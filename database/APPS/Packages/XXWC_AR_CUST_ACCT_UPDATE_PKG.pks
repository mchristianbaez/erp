CREATE OR REPLACE PACKAGE APPS.XXWC_AR_CUST_ACCT_UPDATE_PKG
AS
   /**************************************************************************
    *
    * PACKAGE
    * XXWC_AR_CUST_ACCT_UPDATE_PKG
    *
    * DESCRIPTION
    *  FIN / Mass update of accounts that haven't had activity within the last XX month
    *
    * PARAMETERS
    * ==========
    * NAME              TYPE     DESCRIPTION
    * ----------------- -------- ---------------------------------------------
    *
    *
    *
    *
    *
    * CALLED BY
    *   Which program, if any, calls this one
     * HISTORY
    * =======
    *
    * VERSION DATE           AUTHOR(S)               DESCRIPTION
    * ------- ----------- ---------------------- ------------------------------------
    * 1.00    12/17/2013   HARSHAVARDHAN YEDLA            Creation
	* 1.01    09/17/2014   PATTABHI AVULA                 Modified
    *
    *************************************************************************/



   PROCEDURE UPDATE_CACCT_PTRADE_CUSTCLASS (v_errbuf     NUMBER,
                                            v_retcode    VARCHAR2);
END XXWC_AR_CUST_ACCT_UPDATE_PKG;
/