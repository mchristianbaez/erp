CREATE OR REPLACE PACKAGE APPS.XXCUS_IBY_ACH_POST_PROCESS
AS
   /* ************************************************************************
    HD Supply
    All rights reserved.
   **************************************************************************
     PURPOSE: Rename the temp ACH file created to the one that UC4 looks to FTP out.
                         Without this fix UC4 picks up the file half way and not when complete.
                         The temp file is created by Oracle Payments job "Format Payment Instructions with Text Output".
     REVISIONS:
     Ticket                          Ver             Date               Author                            Description
     ----------------------    ----------   ----------          ------------------------      -------------------------
     20150812-00198   1.0           08/18/2015    Bala Seshadri              Created.
   **************************************************************************/
   --
   Function rename_file
                     (
                        P_Subscription_Guid In Raw
                       ,P_Event             In Out Nocopy Wf_Event_T
                     ) Return Varchar2;                                          
END XXCUS_IBY_ACH_POST_PROCESS;
/