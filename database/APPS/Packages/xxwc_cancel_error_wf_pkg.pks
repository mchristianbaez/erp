CREATE OR REPLACE PACKAGE xxwc_cancel_error_wf_pkg AUTHID CURRENT_USER
AS
   /********************************************************************************************************************************
      $Header XXWC_CANCEL_ERROR_WF_PKG.PKG $
      Module Name: XXWC_CANCEL_ERROR_WF_PKG.PKG

      PURPOSE:   This package is used for data fix of order management

      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------------------------------------------------------------
      1.0        28-Sep-2016 Niraj K Ranjan          Initial Version - TMS#20160804-00335   Create a concurrent program to Cancel 
	                                                                 --Errored Workflows so they may be purged by the purge job
   *********************************************************************************************************************************/
   
   PROCEDURE cancel_error_wf ( p_errbuf         OUT VARCHAR2
                              ,p_retcode        OUT NUMBER
                              ,p_num_of_days    IN  NUMBER
							  ,p_item_type      IN  VARCHAR2
							  ,p_commit_count   IN  NUMBER
							  ,p_order_by       IN  VARCHAR2
                             );
								 
END xxwc_cancel_error_wf_pkg;
/
