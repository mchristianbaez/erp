CREATE OR REPLACE PACKAGE apps.xxwc_po_sel_onhand_pkg IS 
   /*************************************************************************
      $Header XXWC_PO_SEL_ONHAND_PKG $
      Module Name: XXWC_PO_SEL_ONHAND_PKG.PKS
   
      PURPOSE:   Package used for loading on-hand information
   
      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------
      1.0        02/12/2015  Gopi Damuluri           TMS# 20150209-00077 Initial Version
   ****************************************************************************/

   PROCEDURE load_onhand_info (p_errbuf                 OUT  VARCHAR2,
                               p_retcode                OUT  NUMBER);

END xxwc_po_sel_onhand_pkg;
/