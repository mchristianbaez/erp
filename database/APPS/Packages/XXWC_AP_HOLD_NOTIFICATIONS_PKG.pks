/* Formatted on 3/4/2014 1:19:57 AM (QP5 v5.206) */


CREATE OR REPLACE PACKAGE apps.xxwc_ap_hold_notifications_pkg
IS
     /*************************************************************************
   *   $Header XXWC_AP_HOLD_NOTIFICATIONS_PKG $
   *   Module Name: xxwc OM Automatic Receipt allocation package
   *
   *   Purpose: To send AP hold notification to recipient
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        2/15/2014  Rasikha Galimova       Initial Version
   *   1.1        11/24/2014 Maharajan Shunmugam    TMS#20141001-00163  Canada Multi Org changes
   * ***************************************************************************/


    PROCEDURE process_sent_to_ap_button (pl_hold_id              NUMBER
                                        ,p_note                  VARCHAR2
                                        ,p_resolution_code       VARCHAR2
                                        ,p_notification_id       NUMBER
                                        ,p_responder             VARCHAR2
                                        ,p_return_status     OUT VARCHAR2);

    PROCEDURE resend_combined_notification;

    PROCEDURE delete_notification (p_notification_id NUMBER);

    PROCEDURE main;

    PROCEDURE transfer (p_notification_id NUMBER, p_new_role VARCHAR2, p_comments VARCHAR2);

    PROCEDURE refresh_main(p_errbuf OUT varchar2, p_retcode OUT varchar2);                            --Added parameters for ver 1.1

    PROCEDURE get_approver (itemtype    IN            VARCHAR2
                           ,itemkey     IN            VARCHAR2
                           ,actid       IN            NUMBER
                           ,funcmode    IN            VARCHAR2
                           ,resultout      OUT NOCOPY VARCHAR2);

    PROCEDURE assign_form_attribute (p_notification_id NUMBER, p_recipient VARCHAR2);

    PROCEDURE watch_the_job;

    PROCEDURE xxwc_resent_notif_one_email (p_nid IN NUMBER);

    PROCEDURE xxwc_resent_all_notif_email (p_errbuf OUT NOCOPY VARCHAR2, p_retcode OUT NOCOPY VARCHAR2);
END;                                                                                                     -- Package spec
/

-- End of DDL Script for Package APPS.XXWC_AP_HOLD_NOTIFICATIONS_PKG
