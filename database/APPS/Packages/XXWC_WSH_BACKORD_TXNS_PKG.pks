--
-- XXWC_WSH_BACKORD_TXNS_PKG  (Package) 
--
CREATE OR REPLACE PACKAGE APPS.xxwc_wsh_backord_txns_pkg
AS
   /*************************************************************************
    Copyright (c) 2012 Lucidity Consulting Group
    All rights reserved.
   **************************************************************************
     $Header xxwc_wsh_backord_txns_pkg $
     Module Name: xxwc_wsh_backord_txns_pkg.pks

     PURPOSE:   This package is called by the concurrent programs
                XXWC WSH Backordered Transactions Processing

     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.0        01/18/2012  Consuelo Gonzalez      Initial Version
     2.0        03/05/2012  Consuelo Gonzalez      Added code for batch processing
   **************************************************************************/

   -- Global variable declaration
   g_request_id   NUMBER;

   /*************************************************************************
     Procedure : main_process

     PURPOSE:   receive and initiate the processing of backordered lines
                by delivery or sales order number
     Parameter:

   ************************************************************************/
   PROCEDURE main_process (errbuf              OUT VARCHAR2
                          ,retcode             OUT VARCHAR2
                          ,p_order_number   IN     VARCHAR2);

   /*************************************************************************
     Procedure : batch_process

     PURPOSE:   receive and initiate the batch processing of sales orders
                with the ability to cancel or reschedule full orders
     Parameter:

   ************************************************************************/
   PROCEDURE batch_process (errbuf                     OUT VARCHAR2
                           ,retcode                    OUT VARCHAR2
                           ,p_process_type          IN     VARCHAR2
                           ,p_order_number          IN     VARCHAR2
                           ,p_new_sched_ship_date   IN     VARCHAR2);
END xxwc_wsh_backord_txns_pkg;
/

