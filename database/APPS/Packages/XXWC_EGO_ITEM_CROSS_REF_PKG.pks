CREATE OR REPLACE PACKAGE APPS.xxwc_ego_item_cross_ref_pkg
/**************************************************************************
 *
 * HEADER  
 *   XXWC_EGO_ITEM_CROSS_REF_PKG
 *
 * PROGRAM NAME
 *  XXWC_EGO_ITEM_CROSS_REF_PKG.pks
 * 
 * DESCRIPTION
 *  This package spec will upload the item cross reference in to base table using the API
 *
 *
 * PARAMETERS
 * ==========
 * NAME              DESCRIPTION
.* ----------------- ------------------------------------------------------
    None
 *
 * DEPENDENCIES
 *   None
 *
 * CALLED BY
 *   Item Cross reference Upload WebADI
 *
 * LAST UPDATE DATE   19-AUG-2014
 *
 * HISTORY
 * =======
 *
 * VERSION DATE        AUTHOR(S)       DESCRIPTION
 * ------- ----------- --------------- ------------------------------------
 * 1.00    19-AUG-2014 KPIT            Created for Item Cross reference Upload WebADI TMS # 20140210-00116
 *************************************************************************/
AS
/**************************************************************************
 *
 * PROCEDURE
 * upload_xref
 *
 * DESCRIPTION
 *  This procedure will upload the cross reference in to base table thorough Web ADI
 *
 * PARAMETERS
 * ==========
 * NAME                   TYPE                  DESCRIPTION
.* ----------------- -------- ---------------------------------------------
 * P_ITEM_NUMBER          VARCHAR2 IN      inventory item number
 * P_TRANSACTION_TYPE     VARCHAR2 IN      transaction type for cross-reference
 * P_CROSS_TYPE            VARCAHR2 IN      cross-reference type
 * P_XREF_CODE            VARCHAR2 IN      old cross-reference
 * P_NEW_XREF_CODE        VARCHAR2 IN      new cross-reference
 * P_MST_VEN_NO           VARCHAR2 IN      Master Vendor Number
 *
 * RETURN VALUE
 *  None
 *
 * CALLED BY
 *   Upload Item Cross reference Web ADI
 * HISTORY
 * =======
 *
 * VERSION DATE        AUTHOR(S)       DESCRIPTION
 * ------- ----------- --------------- ------------------------------------
 * 1.00    19-AUG-2014 KPIT            Created for Item Cross reference Upload WebADI
 *************************************************************************/
   PROCEDURE upload_xref (
      p_item_number        IN   VARCHAR2,
      p_transaction_type   IN   VARCHAR2,
      P_CROSS_TYPE          IN   VARCHAR2,
      p_xref_code          IN   VARCHAR2 DEFAULT NULL,
      p_new_xref_code      IN   VARCHAR2,
      p_mst_ven_no         IN   VARCHAR2 DEFAULT NULL
   );
END xxwc_ego_item_cross_ref_pkg;
/
