CREATE OR REPLACE PACKAGE APPS.XXCUS_PO_POST_PROCESS
AS
   /* ************************************************************************
    HD Supply
    All rights reserved.
   **************************************************************************
     PURPOSE: Rename the oracle pdf output to a different file name and email it to the requisition preparer.
                         Original file is created by Oracle  job "HDS PO Output for Communication".
     REVISIONS:
     Ticket                          Ver             Date               Author                            Description
     ----------------------    ----------   ----------          ------------------------      -------------------------
     20171004-00368  1.0           10/04/2017    Bala Seshadri              Created.
   **************************************************************************/
   --
   Function rename_and_email
                     (
                        P_Subscription_Guid In Raw
                       ,P_Event             In Out Nocopy Wf_Event_T
                     ) Return Varchar2;
END XXCUS_PO_POST_PROCESS;
/