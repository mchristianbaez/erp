--
-- XXWC_CREDIT_COPY_TRX_UTIL_PKG  (Package) 
--
CREATE OR REPLACE PACKAGE APPS.XXWC_CREDIT_COPY_TRX_UTIL_PKG
IS
   /*************************************************************************
   *  Copyright (c) 2011 Lucidity Consulting Group
   *  All rights reserved.
   **************************************************************************
   *   $Header XXWC_CREDIT_COPY_TRX_UTIL_PKG.pks $
   *   Module Name: XXWC_CREDIT_COPY_TRX_UTIL_PKG.pks
   *
   *   PURPOSE:   This package is used by the utility package for HDS Copy Rebill
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        09/01/2011  Vivek Lakaman             Initial Version
   * ***************************************************************************/

   /*************************************************************************
    *   Procedure : Get_Balance_due
    *
    *   PURPOSE:   This function return balance due for the invoice
    *   Parameter:
    *          IN
    *              p_customer_trx_id    -- Customer Trx id
    * ************************************************************************/
   FUNCTION Get_Balance_due (p_customer_trx_id IN NUMBER)
      RETURN NUMBER;

   /*************************************************************************
    *   Procedure : Get_Batch_source_Id
    *
    *   PURPOSE:   This function return batch_source_id
    *   Parameter:
    *          IN
    *              p_customer_trx_id    -- Customer Trx id
    * ************************************************************************/
   FUNCTION Get_Batch_source_Id (p_customer_trx_id IN NUMBER)
      RETURN NUMBER;

   /*************************************************************************
    *   Procedure : Get_Batch_Source_Name
    *
    *   PURPOSE:   This function return batch_source_name
    *   Parameter:
    *          IN
    *              p_customer_trx_id    -- Customer Trx id
    * ************************************************************************/
   FUNCTION Get_Batch_Source_Name (p_customer_trx_id IN NUMBER)
      RETURN VARCHAR2;

   /*************************************************************************
    *   Procedure : Get_Invoice_Total
    *
    *   PURPOSE:   This function return invoice total for the invoice
    *   Parameter:
    *          IN
    *              p_customer_trx_id    -- Customer Trx id
    * ************************************************************************/

   FUNCTION Get_Invoice_Total (p_customer_trx_id IN NUMBER)
      RETURN NUMBER;

   /*************************************************************************
    *   Procedure : Get_GL_Date
    *
    *   PURPOSE:   This function return GL date for the invoice
    *   Parameter:
    *          IN
    *              p_customer_trx_id    -- Customer Trx id
    * ************************************************************************/


   FUNCTION Get_GL_Date (p_customer_trx_id IN NUMBER)
      RETURN DATE;

   /*************************************************************************
    *   Procedure : Get_Ship_To_Site__Number
    *
    *   PURPOSE:   This function return Site Number for the site use id
    *   Parameter:
    *          IN
    *              p_customer_trx_id    -- Customer Trx id
    * ************************************************************************/


   FUNCTION Get_Ship_To_Site__Number (p_SHIP_TO_SITE_USE_ID   IN NUMBER
                                     ,p_CUSTOMER_ID           IN NUMBER)
      RETURN VARCHAR2;


   /*************************************************************************
    *   Procedure : Get_Bill_To_Site__Number
    *
    *   PURPOSE:   This function return Site Number for the site use id
    *   Parameter:
    *          IN
    *              p_customer_trx_id    -- Customer Trx id
    * ************************************************************************/


   FUNCTION Get_Bill_To_Site__Number (p_BILL_TO_SITE_USE_ID   IN NUMBER
                                     ,p_CUSTOMER_ID           IN NUMBER)
      RETURN VARCHAR2;
END XXWC_CREDIT_COPY_TRX_UTIL_PKG;
/

