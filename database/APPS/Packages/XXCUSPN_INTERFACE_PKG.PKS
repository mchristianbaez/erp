
  CREATE OR REPLACE PACKAGE "APPS"."XXCUSPN_INTERFACE_PKG" AS

  --=================================================================
  --  Copyright (c) 2007 HD SUPLY
  --=================================================================
  -- File Name: xxcus_PN_INTERFACE_PKG
  --
  -- PROGRAM TYPE: PL/SQL Script   <API>
  --
  -- PURPOSE: Package for loading properties.
  --
  -- HISTORY
  -- ================================================================
  --         Last Update Date : 05/19/2011
  -- ================================================================
  -- ================================================================
  -- VERSION DATE          AUTHOR(S)       DESCRIPTION
  -- ------- -----------   --------------- ---------------------------------
  -- 1.0     19-May-2011   Kathy Poling    Copied package from R11 and 
  --                                       converted for R12 .
  -- 
  /**************************************************************************/

  PROCEDURE pn_admin_process(errbuf  OUT VARCHAR2
                            ,retcode OUT NUMBER);

END xxcuspn_interface_pkg;
;
