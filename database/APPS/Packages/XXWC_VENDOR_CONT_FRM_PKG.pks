CREATE OR REPLACE PACKAGE APPS.XXWC_VENDOR_CONT_FRM_PKG
AS
/*************************************************************************
 Copyright (c) 2013 HD Supply
 All rights reserved.
**************************************************************************
  $Header XXWC_VENDOR_CONT_FRM_PKG $
  Module Name: XXWC_VENDOR_CONT_FRM_PKG.pks

  PURPOSE:

  REVISIONS:
  Ver        Date        Author                     Description
  ---------  ----------  ---------------         -------------------------
  1.0        11/19/2013  HARSHAVARDHAN YEDLA      Initial Version
 
**************************************************************************/

   /*************************************************************************
     Function : xxwc_edi_doctype

     PURPOSE:   This function concatenates all the edi enabled document types 
     Parameter:
            IN
                p_tp_hdr_id      -- Header Id
   ************************************************************************/

  Function xxwc_edi_doctype(p_tp_hdr_id number)
  RETURN varchar2;

END XXWC_VENDOR_CONT_FRM_PKG;
/