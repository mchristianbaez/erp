--
-- XXWC_OE_INV_IFACE_WF  (Package) 
--
CREATE OR REPLACE PACKAGE APPS.XXWC_OE_Inv_Iface_WF
   AUTHID CURRENT_USER
AS
   /* $Header: OEXWIIFS.pls 120.0 2005/06/01 02:57:23 appldev noship $  */
   -- This Procedure gets called from the Workflow.
   -- This procedure is modified from Seeded procedure to support serial controlled transactions.

   PROCEDURE Inventory_Interface (itemtype    IN            VARCHAR2
                                 ,itemkey     IN            VARCHAR2
                                 ,actid       IN            NUMBER
                                 ,funcmode    IN            VARCHAR2
                                 ,resultout   IN OUT NOCOPY /* file.sql.39 change */
                                                            VARCHAR2);
END XXWC_OE_Inv_Iface_WF;
/

