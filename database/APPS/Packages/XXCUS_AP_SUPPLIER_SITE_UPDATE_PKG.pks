CREATE OR REPLACE PACKAGE APPS.XXCUS_AP_SUPP_SITE_UPDATE_PKG AS
 /**************************************************************************
    *
    * PACKAGE
    * XXCUS_AP_SUPP_SITE_UPDATE_PKG
    *
    * DESCRIPTION
    *  program to assign 'N' or 'Y' to AP_SUPPLIER_SITES_ALL.create_debit_memo_flag field.
    *
    * PARAMETERS
    * ==========
    * NAME              TYPE     DESCRIPTION
   .* ----------------- -------- ---------------------------------------------
    * Operating Unit
    * Debit Memo(Y/N)
    *
    *
    *
    *
    * CALLED BY
    *   HDS AP GSC: Update Supplier Site - iProcurement Debit Memo Flag
    * 
    * HISTORY
    * =======
    *
    * VERSION DATE        AUTHOR(S)       DESCRIPTION
    * ------- ----------- --------------- ------------------------------------
    * 1.00    01/6/2015   Maharajan S     Initial creation ESMS# 273959
    *
    *************************************************************************/
PROCEDURE UPDATE_DEBIT_MEMO_FLAG(errbuf                  OUT VARCHAR2
                                ,retcode                 OUT VARCHAR2
                                ,p_org_id                IN NUMBER
                                ,p_update_flag 		 IN VARCHAR2); 
END;
/
