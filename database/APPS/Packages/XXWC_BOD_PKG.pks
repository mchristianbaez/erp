create or replace 
PACKAGE XXWC_BOD_PKG IS

/******************************************************************************
   NAME:       XXWC_BOD_PKG

   PURPOSE:    Update Items where BOD did not update correctly between shipping extension and new BOD logic
                 transaction_date between '28-MAR-2013' and '05-SEP-2013'
   
   REVISIONS:   
   Ver        Date         Author           Description
   ---------  -----------  ---------------  ------------------------------------
   1.0        18-NOV-2013  Lee Spitzer       1. Create the PL/SQL Package  TMS Ticke # - 20130904-00878  
                                                                           Cleanup BOD dates on internally transferred items 
******************************************************************************/

   --Define variables for logging debug messages
   g_level_unexpected CONSTANT   NUMBER := 6;
   g_level_error CONSTANT        NUMBER := 5;
   g_level_exception CONSTANT    NUMBER := 4;
   g_LEVEL_EVENT CONSTANT        NUMBER := 3;
   g_LEVEL_PROCEDURE CONSTANT    NUMBER := 2;
   g_LEVEL_STATEMENT CONSTANT    NUMBER := 1;
   
   g_package_name CONSTANT       VARCHAR2(30) := 'XXWC_BOD_PKG';
   g_name VARCHAR2(30);
   g_program_name VARCHAR2(60);
   g_exception EXCEPTION;
   g_user_id NUMBER := FND_GLOBAL.USER_ID;
   g_resp_id NUMBER := FND_GLOBAL.RESP_ID;
   g_resp_appl_id NUMBER := FND_GLOBAL.RESP_APPL_ID;
   g_message VARCHAR2(2000);
   
   g_write_log VARCHAR2(1);

/*
PROCEDURE master_program 
      (p_inventory_item_id  IN NUMBER
      ,p_organization_id    IN NUMBER
      ,p_transaction_id     IN NUMBER
      ,p_onhand_quantities_id  IN NUMBER
      ,p_transaction_date   IN OUT DATE
      ,p_onhand_quantity    IN OUT NUMBER
      ,p_row_id             IN VARCHAR2);
      
The Master Program is used to start looking at the on-hand record passed through it.  It will pass the values to the transaction history procedure.

*/  
  PROCEDURE master_program
      (p_inventory_item_id  IN NUMBER
      ,p_organization_id    IN NUMBER
      ,p_transaction_id     IN NUMBER
      ,p_onhand_quantities_id  IN NUMBER
      ,p_transaction_date   IN OUT DATE
      ,p_onhand_quantity    IN OUT NUMBER
      ,p_row_id             IN VARCHAR2);
  
/*
PROCEDURE transaction_history
      (p_inventory_item_id  IN NUMBER
      ,p_organization_id    IN NUMBER
      ,p_transaction_id     IN NUMBER
      ,p_transaction_date   IN DATE
      ,p_quantity           IN NUMBER
      ,x_rem_quantity       OUT NUMBER
      ,x_date               OUT DATE
      ,x_transaction_type_id  OUT NUMBER
      ,x_transaction_id     OUT NUMBER
      ,x_transfer_organization_id OUT NUMBER);

Transaction History procedure looks for the most recent positive transaction that can cover the on-hand balance.  

*/

  PROCEDURE transaction_history
      (p_inventory_item_id  IN NUMBER
      ,p_organization_id    IN NUMBER
      ,p_transaction_id     IN NUMBER
      ,p_transaction_date   IN DATE
      ,p_quantity           IN NUMBER
      ,x_rem_quantity       OUT NUMBER
      ,x_date               OUT DATE
      ,x_transaction_type_id  OUT NUMBER
      ,x_transaction_id     OUT NUMBER
      ,x_transfer_organization_id OUT NUMBER);

/*
  PROCEDURE review_output
      (p_quantity    IN NUMBER
      ,p_date        IN DATE
      ,p_transaction_type_id  IN NUMBER
      ,p_transaction_id IN NUMBER
      ,x_result OUT VARCHAR2
      ,x_lot_count OUT NUMBER
      ,x_txn_count OUT NUMBER);

    Review output procedure examines the output from transaction history to see if this is a transfer and if we should continue or stop looking through the transaction history
*/
  PROCEDURE review_output
      (p_quantity    IN NUMBER
      ,p_date        IN DATE
      ,p_transaction_type_id  IN NUMBER
      ,p_transaction_id IN NUMBER
      ,x_result OUT VARCHAR2
      ,x_lot_count OUT NUMBER
      ,x_txn_count OUT NUMBER);

/*
  PROCEDURE get_receipt
      (p_transaction_id IN NUMBER
      ,x_bod OUT DATE
      ,x_quantity OUT NUMBER);

Get receipt procedure pulls the calculated receipt date
*/
  
  PROCEDURE get_receipt
      (p_transaction_id IN NUMBER
      ,x_bod OUT DATE
      ,x_quantity OUT NUMBER);

/*/

  PROCEDURE get_item_org
      (p_inventory_item_id IN NUMBER
      ,p_organization_id   IN NUMBER
      );
      

    Get Item Org procedure retrieves the item and organization information, used for the log and output file
*/

  PROCEDURE get_item_org
      (p_inventory_item_id IN NUMBER
      ,p_organization_id   IN NUMBER
      );
      
/*
  PROCEDURE insert_into_moqd_temp
      (p_inventory_item_id IN NUMBER
      ,p_organization_id   IN NUMBER
      ,p_onhand_quantities_id IN NUMBER
      --,p_sequence_number IN NUMBER
      ,p_row_id IN VARCHAR2);

Insert INTO MOQD TEMP inserts the record from MOQD Table into a temp table used to compare the calculated BOD vs the current BOD 

*/
      
  PROCEDURE insert_into_moqd_temp
      (p_inventory_item_id IN NUMBER
      ,p_organization_id   IN NUMBER
      ,p_onhand_quantities_id IN NUMBER
      --,p_sequence_number IN NUMBER
      ,p_row_id IN VARCHAR2);

/*
  PROCEDURE main_ccr
      (retbuf         OUT  VARCHAR2
      ,retcode        OUT  NUMBER
      ,p_organization_id IN NUMBER
      ,p_inventory_item_id IN NUMBER
      ,p_update IN VARCHAR2
      ,p_calculate_method IN VARCHAR2
      ,p_write_log   IN VARCHAR2);
      
  
 Procedure main_ccr is the concurrent program request procedure
 */  
      
  PROCEDURE main_ccr
      (retbuf         OUT  VARCHAR2
      ,retcode        OUT  NUMBER
      ,p_organization_id IN NUMBER
      ,p_inventory_item_id IN NUMBER
      ,p_update IN VARCHAR2
      ,p_calculate_method IN VARCHAR2
      ,p_write_log   IN VARCHAR2);
  
/*  PROCEDURE QUERY_RESULTS
        (p_organization_id IN NUMBER
        ,p_inventory_item_id IN NUMBER
        ,p_onhand_quantities_id IN NUMBER
        ,p_update IN VARCHAR2);
  
   Procedure Query results retrieves and compares calculated BOD with MOQD BOD and updates MOQD BOD if the records meets update criteria 
  */   
  
  PROCEDURE QUERY_RESULTS
        (p_organization_id IN NUMBER
        ,p_inventory_item_id IN NUMBER
        ,p_onhand_quantities_id IN NUMBER
        ,p_update IN VARCHAR2);
 
/*  PROCEDURE CREATE_BACKUP_TABLE
        (p_organization_id IN NUMBER
        ,p_inventory_item_id IN NUMBER
        ,p_table_name   OUT VARCHAR2);
  
    
    Create Backup Table creates a backup table of MOQD before updating.  Will create a table in the format XXWC.XXWC_MOQD_<REQUEST_ID> 
  */
  
  PROCEDURE CREATE_BACKUP_TABLE
        (p_organization_id IN NUMBER
        ,p_inventory_item_id IN NUMBER
        ,p_table_name   OUT VARCHAR2);
  
  /*
  PROCEDURE UPDATE_ORIG_RECEIPT_DATE
        (p_table_name in VARCHAR2);
      
  UPDATE ORIG RECEIPT DATE IS NO LONGER USED.  Code has been moved to QUERY_RESULTS
 
  */
 -- PROCEDURE UPDATE_ORIG_RECEIPT_DATE
 --       (p_table_name in VARCHAR2);
   
 /*
  PROCEDURE FIND_QTY
        (p_transaction_id in out NUMBER
        ,p_inventory_item_id IN NUMBER
        ,p_organization_id in out NUMBER
        ,p_onhand_quantities_id IN NUMBER
        ,p_onhand_quantity IN NUMBER
        ,p_row_id     IN VARCHAR2
        );

    Find QTY uses the calculate BOD method instead of the last positive quantity.
*/     
  PROCEDURE FIND_QTY
        (p_transaction_id in out NUMBER
        ,p_inventory_item_id IN NUMBER
        ,p_organization_id in out NUMBER
        ,p_onhand_quantities_id IN NUMBER
        ,p_onhand_quantity IN NUMBER
        ,p_row_id     IN VARCHAR2
        );

             
END;