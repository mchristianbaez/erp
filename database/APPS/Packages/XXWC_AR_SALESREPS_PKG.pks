CREATE OR REPLACE PACKAGE apps.xxwc_ar_salesreps_pkg
AS
   /******************************************************************************
      NAME:       xxwc.xxwc_ar_salesreps_pkg
      PURPOSE:    Package related to SR Hierarchy enhancement.

      REVISIONS:
      Ver        Date        Author           Description
      ---------  ----------  ---------------  ------------------------------------
      1.0        04/02/2013      shariharan    Initial Version
   ******************************************************************************/
   PROCEDURE sync_seed_data (errbuf OUT VARCHAR2, retcode OUT VARCHAR2);

   PROCEDURE sync_sr_hierarchy (i_dst_code IN VARCHAR2, i_sr_id IN NUMBER);

   PROCEDURE update_snapshot (errbuf OUT VARCHAR2, retcode OUT VARCHAR2, i_period_name IN VARCHAR2);

   PROCEDURE sync_salesreps (errbuf OUT VARCHAR2, retcode OUT VARCHAR2);
END xxwc_ar_salesreps_pkg;
/