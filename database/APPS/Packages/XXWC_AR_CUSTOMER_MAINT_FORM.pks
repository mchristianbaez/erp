CREATE OR REPLACE PACKAGE APPS.xxwc_ar_customer_maint_form
/**************************************************************************
*
* HEADER
 *   Source control header
*
* PROGRAM NAME
*  XXWC_AR_CUSTOMER_MAINT_FORM
*
 * DESCRIPTION
*  used for WC customer maintenance form
*
*
* PARAMETERS
* ==========
* NAME              DESCRIPTION
.* ----------------- ------------------------------------------------------
*
*
* DEPENDENCIES
* XXWC_CUSTOMER_MAINT_ARCH table - keep history of updates for TCA using the custom form
* XXWC_CUSTOMER_MAINT_CURR table  - keep recent updates for TCA using the custom form
* XXWC_CUSTOMER_MAINT_CURR_S -sequence for XXWC_CUSTOMER_MAINT_ARCH table
* XXWC_CUSTOMER_MAINT_FORM -view for query in the form
* CALLED BY
* XXWC_CUSTOMER_ACC_MTNS form
*
* LAST UPDATE DATE   23-aug-2012
*
*
* HISTORY
* =======
*
* VERSION DATE        AUTHOR(S)       DESCRIPTION
* ------- ----------- --------------- ------------------------------------
* 1.00    9-aug-2012 Rasikha Galimova   Creation
* 1.10   23-aug-2012 Rasikha Galimova   added changes to bring house account and  profile update changes
* 1.11   26-nov-2012 added procedure keep_profile_amounts_asbefore- will keep credit amount the same to prevent on hold existing orders.
* 1.12   23-jan-2013 Rasikha Galimova added FUNCTION get_site_purposes to bring site use purpose to the XXWC custom form
* 1.13   22-oct-2013 Harsha Yedla      TMS#20130909-00777 
*                                      Added Parameters: Credit check, Mandatory Po#, Print Price and Joint Check,Category to Procedure xxwc_update_customer_profile
*                                      Added a new function - XXWC_UPDATE_CUST_SITES
* 1.15   30-Mar-2015 Maharajan 
*                    Shunmugam         TMS#20140516-00043 Additional fields to Customer Maintenance Form
* 1.17   06/24/2015  Maharajan         TMS#20150604-00146 Billtrust - Automatic Update of Credit Balance Statement Field
*                    Shunmugam
* 1.18   07/15/2015  Maharajan         TMS#20150123-00197 AR - Customer Flag as indicator to auto apply credits
*                    Shunmugam
*************************************************************************/

IS
    FUNCTION get_sales_rep (p_org_id NUMBER, p_salesrep_id NUMBER)
        RETURN VARCHAR2;

    FUNCTION credit_analyst (p_credit_analyst_id NUMBER)
        RETURN VARCHAR2;

    FUNCTION get_acct_site_purposes (p_acct_site_id IN NUMBER)
        RETURN VARCHAR2;

    PROCEDURE xxwc_update_account (pr_cust_rowid             VARCHAR2
                                  ,pr_primary_salesrep_id    NUMBER
 				  ,p_predominant_trade    VARCHAR2     --Added for Ver 1.15<Start
                   		  ,p_legal_col_indicator  VARCHAR2
				  ,p_legal_placement      VARCHAR2   
				  ,p_classification       VARCHAR2 
                                  ,p_auto_apply_cm        VARCHAR2    --Added for ver 1.18
 				  ,p_mandatory_note       VARCHAR2
                                  ,p_mandatory_note1      VARCHAR2
                                  ,p_mandatory_note2      VARCHAR2
                                  ,p_mandatory_note3      VARCHAR2
                                  ,p_mandatory_note4	   VARCHAR2
                                  ,p_mandatory_note5 	   VARCHAR2   --Added for Ver 1.15<End
                                  ,p_last_updated_by         NUMBER
                                  ,p_last_update_date        DATE
                                  ,p_last_update_login       NUMBER
                                  ,p_responsibility_id       NUMBER
                                  ,p_org_id                  NUMBER);                                       --public api

    PROCEDURE xxwc_update_cust_site_uses (p_site_rowid           VARCHAR2
                                          ,p_salesrep_id          NUMBER
                                          ,p_freight_term         VARCHAR2
                                          ,p_last_updated_by      NUMBER
                                          ,p_last_update_date     DATE
                                          ,p_last_update_login    NUMBER
                                          ,p_responsibility_id    NUMBER
                                          ,p_org_id               NUMBER);                                   --public api

    PROCEDURE xxwc_update_customer_profile (p_prof_rowid           VARCHAR2
                                           ,p_salesrep_id          NUMBER
                                           ,p_collector_id         NUMBER
                                           ,p_remmit_to_code       VARCHAR2
                                           ,p_freight_term         VARCHAR2
                                           ,p_analyst_id           NUMBER
                                           ,p_credit_checking      VARCHAR2
                                           ,p_credit_hold          VARCHAR2
                                           ,p_payment_term_id      NUMBER
                                           ,p_profile_class_id     NUMBER
                                           ,p_account_status       VARCHAR2
                                           ,p_credit_class_code    VARCHAR2
                                           ,p_cust_rowid           VARCHAR2
                                           ,p_cust_acct_st_rowid   VARCHAR2     -- Version# 1.13
                                           ,p_site_uses_rowid      VARCHAR2
                                           ,p_mand_po              VARCHAR2     -- Version# 1.13
                                           ,p_print_price          VARCHAR2     -- Version# 1.13
                                           ,p_joint_check          VARCHAR2     -- Version# 1.13
                                           ,p_category             VARCHAR2     -- Version# 1.13
					   ,p_predominant_trade    VARCHAR2     --Added below for Ver 1.15<Start
                   			   ,p_legal_col_indicator  VARCHAR2
					   ,p_legal_placement      VARCHAR2
					   ,p_tax_exempt          VARCHAR2
                                           ,p_tax_exempt_type      VARCHAR2
					   ,p_classification       VARCHAR2
					   ,p_send_stmnt           VARCHAR2
                                           ,p_send_cr_balance      VARCHAR2       --Added for Ver 1.17
                                           ,p_auto_apply_cm        VARCHAR2       --Added for Ver 1.18
  					   ,p_geocode_override 	   VARCHAR2	  --Added for Ver 1.18
                                          -- ,p_credit_limit         NUMBER
                                           ,p_mandatory_note       VARCHAR2
                                           ,p_mandatory_note1      VARCHAR2
                                           ,p_mandatory_note2      VARCHAR2
                                           ,p_mandatory_note3      VARCHAR2
                                           ,p_mandatory_note4	   VARCHAR2
                                           ,p_mandatory_note5 	   VARCHAR2   --Added for Ver 1.15<End
                                           ,p_last_updated_by      NUMBER
                                           ,p_last_update_date     DATE
                                           ,p_last_update_login    NUMBER
                                           ,p_responsibility_id    NUMBER
                                           ,p_org_id               NUMBER
                                           ,p_level_flag           VARCHAR2);

    PROCEDURE changes_log (pl_prof_rowid          VARCHAR2
                          ,pl_site_uses_rowid     VARCHAR2
                          ,pl_cust_rowid          VARCHAR2
                          ,p_last_updated_by      NUMBER
                          ,p_last_update_date     DATE
                          ,p_last_update_login    NUMBER
                          ,p_stage                VARCHAR2);

    PROCEDURE xxwc_update_prifile_class (px_prof_rowid           VARCHAR2
                                        ,px_profile_class_id     NUMBER
                                        ,px_cust_rowid           VARCHAR2
                                        ,px_site_uses_rowid      VARCHAR2
				        ,px_send_statement	 VARCHAR2
                                        ,px_last_updated_by      NUMBER
                                        ,px_last_update_date     DATE
                                        ,px_last_update_login    NUMBER
                                        ,px_responsibility_id    NUMBER
                                        ,px_org_id               NUMBER);

    PROCEDURE delete_dups_log (log_id NUMBER);

-- Version# 1.13 > Start

   PROCEDURE xxwc_update_cust_siteS (p_cust_acct_site_rowid        VARCHAR2
                                  ,p_man_Po                VARCHAR2
                                  ,p_print_price           VARCHAR2
                                  ,p_joint_check           VARCHAR2
                                  ,p_category              VARCHAR2
				  ,p_tax_exempt           VARCHAR2     --Added for Ver#1.15
                                  ,p_tax_exempt_type       VARCHAR2     --Added for Ver#1.15
 				  ,p_geocode_override 	   VARCHAR2	 --Added for Ver 1.18
                                  ,p_last_updated_by       NUMBER
                                  ,p_last_update_date      DATE
                                  ,p_last_update_login     NUMBER
                                  ,p_responsibility_id     NUMBER
                                  ,p_org_id                NUMBER);

-- Version# 1.13 < End

    FUNCTION get_site_purposes (p_site_use_id IN NUMBER)
        RETURN VARCHAR2;
END;                                                                                                     -- Package spec
/
