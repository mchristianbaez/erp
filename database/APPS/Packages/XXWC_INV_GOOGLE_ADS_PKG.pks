CREATE OR REPLACE PACKAGE APPS.xxwc_inv_google_ads_pkg
AS
   /*****************************************************************************************************
    Copyright (c) 2017 HD Supply Group
    All rights reserved.
   ******************************************************************************************************
     $Header xxwc_inv_google_ads_pkg $
     Module Name: xxwc_inv_google_ads_pkg.pks

     PURPOSE:

     REVISIONS:
     Ver        Date        Author               Description
     ---------  ----------  ---------------      -------------------------
     1.0        01/31/2017 P.Vamshidhar          Initial Version(TMS#20161104-00076)
	 1.1        05/26/2017  Pattabhi Avula       Initial Version(TMS#20161017-00224)
   *****************************************************************************************************/

   PROCEDURE item_extract (p_on_hand_qty IN VARCHAR2, p_web_item IN VARCHAR2);

   PROCEDURE inventory_extract (p_on_hand_qty   IN VARCHAR2,
                                p_web_item      IN VARCHAR2);


   PROCEDURE main (x_errbuf            OUT VARCHAR2,
                   x_retcode           OUT VARCHAR2,
                   p_extract_type   IN     VARCHAR2,
                   p_on_hand_qty    IN     VARCHAR2,
                   p_web_item       IN     VARCHAR2);

   FUNCTION google_prod_category (p_inventory_item_id   IN NUMBER,
                                  p_organization_id     IN NUMBER)
      RETURN VARCHAR2;

   FUNCTION UNLOAD_DATA (p_my_cursor   IN SYS_REFCURSOR,
                         p_file_name   IN VARCHAR2,
                         p_file_path   IN VARCHAR2,
                         p_file_extn   IN VARCHAR2)
      RETURN XXWC_SPOOL_FILE_TBL_NTT
      PIPELINED;

   PROCEDURE xxwc_item_pricing_data (x_errbuf                OUT VARCHAR2,
                                     x_retcode               OUT VARCHAR2,
                                     p_organization_id    IN     NUMBER,
                                     p_cust_account_id    IN     NUMBER,
                                     p_cust_acct_use_id   IN     NUMBER);
-- Ver#1.1
	  
   PROCEDURE PLA_FEED (p_onhnd_qty IN NUMBER, p_web_item IN VARCHAR2);

END xxwc_inv_google_ads_pkg;
/