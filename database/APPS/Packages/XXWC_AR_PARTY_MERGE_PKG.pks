CREATE OR REPLACE PACKAGE APPS.XXWC_AR_PARTY_MERGE_PKG
AS
   /**************************************************************************
    *
    * PACKAGE NAME: XXWC_AR_PARTY_MERGE_PKG
    *
    * DESCRIPTION : Program to Merge Customer Parties
    *
    * HISTORY
    * =======
    *
    * VERSION DATE           AUTHOR(S)               DESCRIPTION
    * ------- ----------- ---------------------- ------------------------------------
    * 1.00    12/17/2013   HARSHAVARDHAN YEDLA       Creation
    *                                                TMS# 20130228-01277
    *************************************************************************/

   V_BATCH_ID   NUMBER;
   V_ERRBUF     VARCHAR2 (3000);

   PROCEDURE XXWC_AR_PARTY_MERGE_PROC (p_from_partyno   IN VARCHAR2,
                                       p_to_partyno     IN VARCHAR2,
                                       P_RID               VARCHAR2);

   PROCEDURE XXWC_AR_PARTY_MERGE_MAIN (ERRBUF    OUT VARCHAR2,
                                       RETCODE   OUT NUMBER);
END XXWC_AR_PARTY_MERGE_PKG;
/
