create or replace 
package xxwc_po_freight_burden_pkg
AS
   /*************************************************************************
   *   $Header xxwc_po_freight_burden_pkg $
   *   Module Name: xxwc_po_freight_burden_pkg
   *
   *   PURPOSE:   This package is used by the XXWC PO Freight Burden Upload ADI
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        04/01/2012  Shankar Hariharan             Initial Version
   *   2.0        05/13/2013  Lee Spitzer                   Updates to for HI Freight Burden
   * ***************************************************************************/

   PROCEDURE load_freight_burden (i_org_code IN VARCHAR2,
                                  i_vendor_num   IN VARCHAR2,
                                  i_freight_duty IN NUMBER,
                                  i_import_flag  IN VARCHAR2,
                                  --Added for HI Freight Burden
                                  i_freight_per_lb IN NUMBER,
                                  i_freight_basis IN VARCHAR2,
                                  i_vendor_type   IN VARCHAR2,
                                  i_source_org_code in VARCHAR2);
END xxwc_po_freight_burden_pkg;