CREATE OR REPLACE PACKAGE APPS.XXWC_RTV_SHIPMENT_PKG
   AUTHID CURRENT_USER
AS
   /********************************************************************************************************************************
      $Header XXWC_RTV_SHIPMENT_PKG.PKS $

      PURPOSE:   This package created to process RTV duplicate items allocations.

      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         ----------------------------------------------------------------------------------------
      1.0        14-Apr-2018 P.Vamshidhar            Initial Version -
                                                      TMS# 20170817-00112 - Receipt Traveler Enhancement Request

   ******************************************************************************************************************************************/

   PROCEDURE SHIPMENTS_DATA_POPULATE (P_CONC_REQUEST_ID   IN     NUMBER,
                                      P_SHIP_HDR_ID       IN     NUMBER,
                                      P_RET_STATUS           OUT VARCHAR2);
END XXWC_RTV_SHIPMENT_PKG;
/