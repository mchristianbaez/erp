CREATE OR REPLACE PACKAGE      xxwc_ascp_scwb_pkg
AS
    /*************************************************************************
   *   $Header xxwc_ascp_scwb_pkg $
   *   Module Name: xxwc_ascp_scwb_pkg
   *
   *   PURPOSE:   Package used in DB views for Supply Chain Workbench
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        04/01/2012  Shankar Hariharan       Initial Version
   *   1.1        05/27/2014  Lee Spitzer             20130917-00676 - Vendor Cost Improvements - Added Function get_vendor_contact_minimum
   *   1.2        08/13/2014  Lee Spitzer             20140726-00008  Update Vendor min Web ADI to download/ update default contact @SC Update Vendor min Web ADI to download/ update default contact
   *   1.3        12/01/2015  Kishorebabu V           TMS# 20151111-00201   Vendor Minimum notes not populating on PO hover over or Purch Info form
   *   1.4        03/25/2016  Rakesh Patel            TMS 20151023-00037/20160328-00211 - Shipping Extension Modification 
   ****************************************************************************/



     FUNCTION get_sales_velocity (i_inventory_item_id   IN NUMBER
                              , i_organization_id     IN NUMBER)
      RETURN VARCHAR;

   FUNCTION get_po_cost (i_inventory_item_id   IN NUMBER
                       , i_organization_id     IN NUMBER
                       , i_vendor_id           IN NUMBER
                       , i_vendor_site_id      IN NUMBER)
      RETURN NUMBER;

   FUNCTION get_vendor_minimum (i_vendor_id         IN NUMBER
                              , i_vendor_site_id    IN NUMBER
                              , i_organization_id   IN NUMBER
							  , i_vendor_contact_id IN NUMBER DEFAULT NULL)   --- Ver 1.3 as per TMS# 20151111-00201 Added new Column i_vendor_contact_id
      RETURN VARCHAR;

   FUNCTION get_vendor_info (i_return_type       IN VARCHAR
                           , i_vendor_id         IN NUMBER
                           , i_vendor_site_id    IN NUMBER
                           , i_organization_id   IN NUMBER
						   , i_vendor_contact_id IN NUMBER DEFAULT NULL)   --- Ver 1.3 as per TMS# 20151111-00201 Added new Column i_vendor_contact_id
      RETURN NUMBER;

   FUNCTION get_on_hand (i_inventory_item_id   IN NUMBER
                       , i_organization_id     IN NUMBER
                       , i_return_type         IN VARCHAR)
      RETURN NUMBER;

   FUNCTION get_on_hand_gt_n (i_inventory_item_id   IN NUMBER
                             , i_organization_id     IN NUMBER
                             , i_no_of_days          IN NUMBER)
      RETURN NUMBER;

   FUNCTION get_inventory_level (i_level_type          IN VARCHAR
                               , i_inventory_item_id   IN NUMBER
                               , i_organization_id     IN NUMBER)
      RETURN NUMBER;

   -- 12/09/CG: TMS 20130904-00654: Added 2 new Columns i_ppd_freight_units and i_notes
   PROCEDURE load_vendor_minimum (i_vendor_num   IN VARCHAR2,
                                  -- i_org_code     IN NUMBER,
                                  i_org_code     IN VARCHAR2,
                                  i_vendor_min   IN NUMBER,
                                  i_freight_min  IN NUMBER,
                                  i_freight_min_uom IN NUMBER,
                                  i_ppd_freight_units IN NUMBER,
                                  i_notes IN VARCHAR2
                                  );

   FUNCTION get_wc_org_id return number;
   PROCEDURE proccommit;

-- 08/14/2013 CG: TMS 20130801-01196: Added to pull internal req quantities
   FUNCTION get_internal_req_qty (i_inventory_item_id   IN NUMBER
                              , i_organization_id     IN NUMBER)
   RETURN NUMBER;

-- 12/03/2013 CG: TMS 20131121-00164: Added new function to pull concatenated cross ref
    FUNCTION xxwc_inv_get_concat_cross_ref (
      p_inventory_item_id      IN   NUMBER,
      p_cross_reference_type   IN   VARCHAR2
   ) RETURN VARCHAR2;

-- 12/10/2013 CG: TMS 20130904-00654: New function to retrieve vendor min notes
    FUNCTION get_vendor_minimum_notes (i_vendor_id         IN NUMBER
                                      ,i_vendor_site_id    IN NUMBER
                                      ,i_organization_id   IN NUMBER
                                      ,i_vendor_contact_id IN NUMBER DEFAULT NULL)   --- Ver 1.3 as per TMS# 20151111-00201 Added new Column i_vendor_contact_id
      RETURN VARCHAR;

   /*************************************************************************************************
   *   Function get_vendor_contact_minimums                                                         *
   *   Purpose : Used in the PO Form to display the Vendor Minimums for specific Vendor Contact     *
   *                                                                                                *
   *                                                                                                *
   *    FUNCTION get_vendor_contact_minimum (i_vendor_id         IN NUMBER                          *
   *                                    , i_vendor_site_id    IN NUMBER                             *
   *                                    , i_vendor_contact_id IN NUMBER                             *
   *                                    , i_organization_id   IN NUMBER)                            *
   *   REVISIONS:                                                                                   *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   1.1        05/27/2014  Lee Spitzer                TMS 20130917-00676                         *
   *                                                         Vendor Cost Management Improvements    *
   *                                                      Added Function get_vendor_contact_minimum *
   /************************************************************************************************/



    FUNCTION get_vendor_contact_minimum (i_vendor_id         IN NUMBER
                                        , i_vendor_site_id    IN NUMBER
                                        , i_vendor_contact_id IN NUMBER
                                        , i_organization_id   IN NUMBER)
      RETURN VARCHAR;

   /*************************************************************************************************
   *   PROCEDURE load_vendor_minimum_contact                                                        *
   *                              (i_vendor_min   IN NUMBER,                                        *
   *                               i_freight_min  IN NUMBER,                                        *
   *                               i_freight_min_uom IN NUMBER,                                     *
   *                               i_ppd_freight_units IN NUMBER,                                   *
   *                               i_notes IN VARCHAR2,                                             *
   *                               i_default_flag IN VARCHAR2,                                      *
   *                               i_vendor_id IN NUMBER,                                           *
   *                               i_vendor_site_code IN VARCHAR2,                                  *
   *                               i_vendor_contact  IN VARCHAR2,                                   *
   *                               i_organization_id IN NUMBER                                      *
   *                               );                                                               *
   *                                                                                                *
   *   REVISIONS:                                                                                   *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   1.2        08/13/2014  Lee Spitzer                20140726-00008  Update Vendor min          *
   *                                                         Web ADI to download                    *
   *                                                                                                *
   /************************************************************************************************/
     -- 12/09/CG: TMS 20130904-00654: Added 2 new Columns i_ppd_freight_units and i_notes

   PROCEDURE load_vendor_minimum_contact
                                 (i_vendor_min   IN NUMBER,
                                  i_freight_min  IN NUMBER,
                                  i_freight_min_uom IN NUMBER,
                                  i_ppd_freight_units IN NUMBER,
                                  i_notes IN VARCHAR2,
                                  i_default_flag IN VARCHAR2,
                                  i_vendor_id IN VARCHAR2,
                                  i_vendor_name in VARCHAR2,
                                  --i_vendor_site_id IN NUMBER,
                                  i_vendor_site_code IN VARCHAR2,
                                  --i_vendor_contact_id  IN NUMBER,
                                  i_vendor_contact in VARCHAR2,
                                  i_organization_id IN VARCHAR2
                                  );
								  
    /*************************************************************************
    Procedure name: xxwc_ascp_scwb_pkg.get_open_orders_qty

    PURPOSE:   This procedure calculate open order qty

    REVISIONS:
    Ver        Date        Author                     Description
    ---------  ----------  ---------------         -------------------------
    1.0        03/25/2016  Rakesh Patel            TMS #20151023-00037 - Shipping Extension Modification
    **************************************************************************/
	FUNCTION get_open_orders_qty (p_inventory_item_id   IN NUMBER
                                 ,p_organization_id     IN NUMBER
    							) RETURN NUMBER;
	
END xxwc_ascp_scwb_pkg;
/