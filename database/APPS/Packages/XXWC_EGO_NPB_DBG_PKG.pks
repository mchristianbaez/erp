/* Formatted on 5/25/2013 8:51:13 AM (QP5 v5.240.12305.39446) */
CREATE OR REPLACE PACKAGE APPS.XXWC_EGO_NPB_DBG_PKG
AS
   /**************************************************************************
    File Name:XXWC_EGO_NPB_DBG_PKG
    PROGRAM TYPE: PL/SQL Package spec and body
    PURPOSE: Captures debug information on the NPB form code
    HISTORY
    -- Description   : Called from the trigger XXWC_ENG_NIR_TRG

    VERSION DATE          AUTHOR(S)       DESCRIPTION
    ------- -----------   --------------- ---------------------------------
    1.0     07-Aug-2012    Santhosh Louis    Initial creation of the package
   **************************************************************************/

   p_profile_option_value   VARCHAR2 (10) := ''; -- := FND_PROFILE.VALUE('XXWC_EGO_NPB_DEBUG_PROFILE');

   PROCEDURE DEBUG_PROC (param_name VARCHAR2, param_value VARCHAR2);
END;
/