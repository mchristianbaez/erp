create or replace package APPS.XXWC_AR_FREIGHT_TERMS is
  /*************************************************************************
  *   $Header XXWC_AR_FREIGHT_TERMS $
  *   Module Name: XXWC_AR_FREIGHT_TERMS
  *
  *   PURPOSE:   This package will put the Account
  *   and Primary BT site flags in sync so that the JOTF's will be flagged correctly.
  *
  *   REVISIONS:
  *   Ver        Date        Author                     Description
  *   ---------  ----------  ---------------         -------------------------
  *   1.0        07/31/2015  Nancy Pahwa            Initial Version Task ID: 20150710-00080
    * ***************************************************************************/
  procedure XXWC_AR_EXEMPT_FLAG(errbuf OUT VARCHAR2, retcode OUT NUMBER);
end XXWC_AR_FREIGHT_TERMS;
/
