CREATE OR REPLACE PACKAGE apps.xxwc_om_order_holds_pkg
AS
   /******************************************************************************
      NAME:       apps.xxwc_om_order_holds_pkg
      PURPOSE:    Package to create a case folder whenever a hard hold is placed.

      REVISIONS:
      Ver        Date        Author           Description
      ---------  ----------  ---------------  ------------------------------------
      1.0        6/26/2013      shariharan    Initial Version


   ******************************************************************************/
   PROCEDURE create_case_folder (errbuf OUT VARCHAR2, retcode OUT VARCHAR2);

   PROCEDURE release_order_hold (i_order_number   IN     NUMBER
                               , i_review_type    IN     VARCHAR2
                               , o_ret_msg           OUT VARCHAR2);
END xxwc_om_order_holds_pkg;
/
