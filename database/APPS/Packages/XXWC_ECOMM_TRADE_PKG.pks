CREATE OR REPLACE PACKAGE APPS.XXWC_ECOMM_TRADE_PKG
AS
   /**************************************************************************************************
    Copyright (c) HD Supply Group
    All rights reserved.
   ***************************************************************************************************
     $Header XXWC_ECOMM_TRADE_PKG $
     Module Name: XXWC_ECOMM_TRADE_PKG.pks

     PURPOSE:

     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.0        05/23/2017  Pattabhi Avula             Initial Version(TMS#20160915-00028)
    *************************************************************************************************/
PROCEDURE write_log (p_debug_msg IN VARCHAR2);

PROCEDURE write_error (p_debug_msg IN VARCHAR2);

PROCEDURE generate_csv_files(errbuf     OUT VARCHAR2,
                             retcode    OUT VARCHAR2);
								  
END XXWC_ECOMM_TRADE_PKG;
/