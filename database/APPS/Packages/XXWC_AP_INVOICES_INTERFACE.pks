
create or replace package XXWC_AP_INVOICES_INTERFACE as
/*************************************************************************
     $Header XXWC_AP_INVOICES_INTERFACE $
     Module Name: XXWC_AP_INVOICES_INTERFACE
	 TMS # : 20140714-00052

     PURPOSE:   This package records the choices made in the PO Communication Form so that subsequent visits to the form defaults the correct values.

     REVISIONS:
     Ver        Date        Author                             Description
     ---------  ----------  -------------------------------    -------------------------
     1.0        05/19/2014  Jason Price (Focused E-commerce)   Initial Version
    
   **************************************************************************/ 
procedure UPDATE_EDI_INVOICES (p_errbuf OUT varchar2, p_retcode OUT varchar2);


end;

