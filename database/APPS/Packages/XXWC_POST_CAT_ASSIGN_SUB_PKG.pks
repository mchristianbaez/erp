CREATE OR REPLACE PACKAGE APPS.XXWC_POST_CAT_ASSIGN_SUB_PKG
AS
   /**************************************************************************
    File Name:XXWC_POST_CAT_ASSIGN_SUB_PKG
    PROGRAM TYPE: PL/SQL Package spec and body
    PURPOSE: 
    HISTORY
    -- Description   : Called from business event 
    --                 oracle.apps.ego.item.postCatalogAssignmentChange
    --
    -- Dependencies Tables        : 
    -- Dependencies Views         : None
    -- Dependencies Sequences     : 
    -- Dependencies Procedures    : 
    -- Dependencies Functions     : None
    -- Dependencies Packages      : None
    -- Dependencies Types         : None
    -- Dependencies Database Links: None
    ================================================================
           Last Update Date : 01/08/2014
    ================================================================
    ================================================================
    VERSION DATE          AUTHOR(S)       DESCRIPTION
    ------- -----------   --------------- ---------------------------------
    1.0     08-Jan-2014   Praveen Pawar    Initial creation of the package						   
   **************************************************************************/
	FUNCTION process_event (p_subscription_guid IN RAW
					                 		,p_event             IN OUT NOCOPY WF_EVENT_T
			)RETURN VARCHAR2;
	  
   END XXWC_POST_CAT_ASSIGN_SUB_PKG;
/