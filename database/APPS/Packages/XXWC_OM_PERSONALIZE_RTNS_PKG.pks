CREATE OR REPLACE PACKAGE APPS.XXWC_OM_PERSONALIZE_RTNS_PKG
AS
/*************************************************************************
        *   Function : is_row_locked
        *
        *   PURPOSE:   This function identifies if a record has ben locked by user
        *  Author: Ram Talluri- TMS #20131212-00053 
        * Creation_date 12/12/2013
        * Last update Date 12/12/2013
        *   Parameter:
        *          IN rowid, table_name
        *
        * ************************************************************************/
   FUNCTION is_row_locked (v_rowid ROWID, table_name VARCHAR2)
      RETURN VARCHAR2;

   /*************************************************************************
        *   Procedure : write_log
        *
        *   PURPOSE:   This procedure prints the string in log file.
        *  Author: Ram Talluri- TMS #20131212-00053 
        * Creation_date 12/12/2013
        * Last update Date 12/12/2013
        *   Parameter:
        *          IN p_debug_msg
        *
        * ************************************************************************/
   PROCEDURE write_log (p_debug_msg IN VARCHAR2);

   /*************************************************************************
        *   Procedure : write_error
        *
        *   PURPOSE:   This procedure reports the error.
        *  Author: Ram Talluri- TMS #20131212-00053 
        * Creation_date 12/12/2013
        * Last update Date 12/12/2013
        *   Parameter:
        *          IN p_debug_msg
        *
        * ************************************************************************/
   PROCEDURE write_error (p_debug_msg IN VARCHAR2, p_call_point IN VARCHAR2);
   /*************************************************************************
        *   Procedure : XXWC_OM_RENTAL_PROCESSING
        *
        *   PURPOSE:   This procedure is used in personalization to ship rental items on internal orders..
        *  Author: Ram Talluri- TMS #20131212-00053 
        * Creation_date 12/12/2013
        * Last update Date 12/12/2013
        *   Parameter:
        *          IN p_debug_msg
        *
        * ************************************************************************/
   
   PROCEDURE XXWC_OM_RENTAL_PROCESSING(
      p_header_id        IN   NUMBER,
      p_user_id        IN   NUMBER,
      p_resp_id        IN   NUMBER,
      p_resp_appl_id   IN   NUMBER,
      p_user_name      IN   VARCHAR2
   );
  /*************************************************************************
        *   Procedure : XXWC_GET_USER_PROF_VAL
        *
        *   PURPOSE:   This procedure is used in personalization to ship rental items on internal orders..
        *  Author: Ram Talluri- TMS #20131212-00053 
        * Creation_date 12/12/2013
        * Last update Date 12/12/2013
        *   Parameter:
        *          IN p_debug_msg
        *
        * ************************************************************************/
 
FUNCTION XXWC_GET_USER_PROF_VAL(V_PROFILE_USER VARCHAR2)
 RETURN number;

END XXWC_OM_PERSONALIZE_RTNS_PKG;
/