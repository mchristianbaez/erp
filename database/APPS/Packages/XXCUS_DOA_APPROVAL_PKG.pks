CREATE OR REPLACE PACKAGE APPS.XXCUS_DOA_APPROVAL_PKG AS
/**************************************************************************
   $Header XXCUS_DOA_APPROVAL_PKG $
   Module Name: XXCUS_DOA_APPROVAL_PKG.pks

   PURPOSE:   This package is called by the concurrent programs
              XXCUS DOA Approval Automation program
              for updating the job name for per employees.
   REVISIONS:
   Ver        Date        Author             Description
   ---------  ----------  ---------------   -------------------------
    1.0       10/07/2018  Ashwin Sridhar    Initial Build - Task ID: 20180410-00199
/*************************************************************************/

PROCEDURE UPDATE_JOB_NAME(p_errbuf  OUT VARCHAR2
                         ,p_retcode OUT VARCHAR2);

PROCEDURE CALL_HR_API(p_assignment_id IN NUMBER
                     ,p_job_id        IN NUMBER
                     ,p_object_ver    IN NUMBER
                     ,p_success       OUT VARCHAR2);

END XXCUS_DOA_APPROVAL_PKG;
/