CREATE OR REPLACE PACKAGE APPS.XXWC_PRICING_SEGMENT_DCOM_PKG AS
   /*************************************************************************
      $Header XXWC_PRICING_SEGMENT_DCOM_PKG.PKB $
      Module Name: XXWC_PRICING_SEGMENT_DCOM_PKG.PKB

      PURPOSE:   This package is used to decomission Pricing Segmentation process

      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------
      1.0        10/09/2016  Gopi Damuluri           Initial Version TMS# 20160922-00037
   ****************************************************************************/

PROCEDURE matrix_dcom(p_errbuf              OUT VARCHAR2
                    , p_retcode             OUT NUMBER
                    , p_name                 IN VARCHAR2
                    , p_list_header_id_from  IN  NUMBER
                    , p_list_header_id_to    IN  NUMBER);

PROCEDURE delete_qualifiers(p_list_header_id IN NUMBER);

PROCEDURE delete_qualifier_rules(p_list_header_id IN NUMBER);

PROCEDURE delete_mtrx_excluders(p_list_header_id IN NUMBER);

PROCEDURE upd_mtrx_incomp(p_list_header_id IN NUMBER);

PROCEDURE mtrx_item_cat_excl(p_errbuf         OUT VARCHAR2
                           , p_retcode        OUT NUMBER
                           , p_name            IN VARCHAR2);

PROCEDURE csp_bpw_dcom(p_errbuf    OUT VARCHAR2
                     , p_retcode   OUT NUMBER
                     , p_bpw_name  IN  VARCHAR2
                     , p_row_count IN  NUMBER);

END XXWC_PRICING_SEGMENT_DCOM_PKG;
/