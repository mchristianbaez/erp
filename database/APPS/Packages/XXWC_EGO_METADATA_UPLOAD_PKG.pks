CREATE OR REPLACE
PACKAGE APPS.XXWC_EGO_METADATA_UPLOAD_PKG AS
/********************************************************************************
File Name				: XXWC_EGO_METADATA_UPLOAD_PKG
PROGRAM TYPE			: PL/SQL Package spec 
PURPOSE					: Procedures and functions for uploading the AG metadata load via Web ADI to EBS Default Interface tables
-- Dependencies Tables 	: EGO_ATTR_GROUPS_INTERFACE,
						  EGO_ATTR_GROUPS_DL_INTERFACE,
						  EGO_ATTR_GROUP_COLS_INTF,
						  EGO_ATTR_GRPS_ASSOC_INTERFACE,
						  EGO_PAGES_INTERFACE,
						  EGO_PAGE_ENTRIES_INTERFACE
--Pre Processing		: Related VS and ICCs should be already populated in Interface tables or should be available in EBS main tables

-- Post Processing 		: Call the CP: EGO Import Metadata(EGOIMDCP) to load the data from Interface table to Main tables


HISTORY		:
==================================================================================
VERSION DATE          AUTHOR(S)       		  DESCRIPTION
------- -----------   ----------------------  -----------------------------------------
1.0     07-JUN-2013   Rajasekar Gunasekaran	  Initial Version

*********************************************************************************/


   /********************************************************************************
      PROGRAM TYPE: FUNCTION
   NAME: LOAD_AG_TO_INTRF
   PURPOSE: Function to load attribute group related informations to EBS Default Interface tables
   
   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       		DESCRIPTION
   ------- -----------   ---------------------	-----------------------------------
	1.0    07-JUN-2013   Rajasekar Gunasekaran	Initial Version
	1.1    12-JUN-2013   Rajasekar Gunasekaran	Updated with unit testing Fixes and few enhancements	
    ********************************************************************************/
	FUNCTION LOAD_AG_TO_INTRF 
	(
	--AG Data Params
	P_AG_TYPE					VARCHAR2 DEFAULT 'EGO_ITEMMGMT_GROUP'
	,P_AG_NAME					VARCHAR2-- not null
	,P_AG_DISP_NAME				VARCHAR2-- not null
	,P_AG_DESCRIPTION			VARCHAR2 DEFAULT NULL
	,P_AG_MULTI_ROW_FLAG		VARCHAR2 DEFAULT 'N' -- Y/N
	,P_AG_VARIANT_FLAG			VARCHAR2 DEFAULT 'N' -- Y/N
	,P_AG_NUM_OF_ROWS 			NUMBER DEFAULT NULL-- null for SR
	,P_AG_NUM_OF_COLS			NUMBER DEFAULT 2 
	--AG Data Level Params
	,P_AG_DATA_LEVEL_NAME		VARCHAR2 DEFAULT 'ITEM_LEVEL' --ITEM_LEVEL, ITEM_ORG, COMPONENTS_LEVEL
	,P_AG_USER_VIEW_PRIV_NAME	VARCHAR2 DEFAULT NULL
	,P_AG_USER_EDIT_PRIV_NAME	VARCHAR2 DEFAULT NULL
	,P_AG_PRE_EVENT_FLAG		VARCHAR2 DEFAULT 'N' -- Y/N
	,P_AG_POST_EVENT_FLAG		VARCHAR2 DEFAULT 'N'-- Y/N
	-- ICC Level Params
	,P_ICC_NAME					VARCHAR2 -- not null
	,P_ICC_PAGE_NAME			VARCHAR2 DEFAULT NULL
	,P_ICC_PAGE_SEQ				NUMBER 
	,P_ICC_PAGE_AG_SEQ			NUMBER
	--Attribute Params
	,P_ATTR_INTERNAL_NAME		VARCHAR2-- not null
	,P_ATTR_DISPLAY_NAME		VARCHAR2-- not null
	,P_ATTR_DESCRIPTION			VARCHAR2 DEFAULT NULL
	,P_ATTR_SEQUENCE			NUMBER
	,P_ATTR_DB_COLUMN_NAME 		VARCHAR2 -- not null --C_EXT_ATTR1 to 40, N_EXT_ATTR1 to 20, D_EXT_ATTR1 to 10, TL_EXT_ATTR1 to 40
	,P_ATTR_DATA_TYPE			VARCHAR2 DEFAULT 'C' -- C-Char, X-Standard Date, Y- Standard DateTime, A- Translatable Text, N- Number
	,P_ATTR_UNIQUE_KEY_FLAG		VARCHAR2 DEFAULT 'N'--Flag to indicate if this attribute is a part of Unique Key in case of Multi Row Attribute group
	,P_ATTR_INFO_1				VARCHAR2 DEFAULT NULL--URL incase of attribute type Dynamic URL
	,P_ATTR_UOM_CLASS			VARCHAR2 DEFAULT NULL-- Unit Of Measure in case of Number Attribute
	,P_ATTR_ENABLED_FLAG		VARCHAR2 DEFAULT 'Y' -- Y/N - Flag to indicate if the descriptive flexfield segment is enabled
	,P_ATTR_REQUIRED_FLAG		VARCHAR2 DEFAULT 'N' --Y/N - Flag to indicate whether a value must be entered for this segment
	,P_ATTR_DISPLAY_CODE		VARCHAR2 DEFAULT 'T' -- Values correspond to the lookup_type = EGO_EF_DISPLAY_TYPE Allowed values are 
													/* C - Checkbox,D - Dynamic URL,H - Hidden,L - Text Area,R - Radio Group,S - Static URL,T - Text field"*/
	,P_ATTR_DEFAULT_VALUE		VARCHAR2 DEFAULT NULL
	,P_ATTR_FLEX_VALUE_SET_NAME VARCHAR2 DEFAULT NULL
	--general Params
	,P_TRANSACTION_TYPE			VARCHAR2 DEFAULT 'SYNC' -- CREATE, UPDATE, SYNC, DELETE
	,P_INT_NAME_AUTO_GEN_VAL	VARCHAR2 DEFAULT NULL -- if internal names are not provided and this val is not null, then it will auto generate it
	) 
	RETURN VARCHAR2;
	
   PROCEDURE submit_job (
      errbuf                  OUT NOCOPY VARCHAR2,
      retcode                 OUT NOCOPY VARCHAR2,
      p_user_name             IN         VARCHAR2
   );
   
    FUNCTION submit_metadata_cp(p_user_name VARCHAR2) RETURN VARCHAR2;
	
	FUNCTION generate_db_column_name(L_AG_NAME VARCHAR2,L_AG_TYPE VARCHAR2,L_ATTR_DATA_TYPE VARCHAR2,L_SET_PROCESS_ID NUMBER) RETURN VARCHAR2;
	
	
END XXWC_EGO_METADATA_UPLOAD_PKG;	