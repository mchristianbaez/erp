CREATE OR REPLACE PACKAGE APPS.XXWCAP_POSITIVE_PAY_PKG AS
/********************************************************************************

FILE NAME: APPS.XXWCAP_POSITIVE_PAY_PKG.pkb

PROGRAM TYPE: PL/SQL Package

PURPOSE: API to create WC Positive Pay File

HISTORY
===============================================================================
VERSION DATE          AUTHOR(S)       DESCRIPTION
------- -----------   --------------- -----------------------------------------
1.0     09/16/2013    Gopi Damuluri    Initial version. (TMS# 20130823-00455)
1.1     10/06/2014    Pattabhi Avula   Hardcoded Org_id Initialization commented 
									   for TMS# 20141001-00058 
********************************************************************************/



/********************************************************************************
Procedure Name: UC4_CALL 

Description : API to call "Positive Pay File with Additional Parameters"
********************************************************************************/

PROCEDURE uc4_call (p_errbuf                           OUT VARCHAR2,
                    p_retcode                          OUT NUMBER,
                    p_user_name                        IN VARCHAR2,
                    p_responsibility_name              IN VARCHAR2,
                    p_conc_prg_name                    IN VARCHAR2,
                    p_arg_format                       IN VARCHAR2,
                    p_arg_int_bank_acct_name           IN VARCHAR2,
                    p_arg_pay_status                   IN VARCHAR2,
                    p_arg_reselect                     IN VARCHAR2,
                    p_arg_ob_pay_file_prefix           IN VARCHAR2,
                    p_arg_ob_pay_file_extn             IN VARCHAR2,
                    p_arg_ob_pay_file_dir              IN VARCHAR2);

END XXWCAP_POSITIVE_PAY_PKG;