CREATE OR REPLACE PACKAGE APPS.XXWC_COSTAR_GL_EXPORT AS
/**************************************************************************
   $Header XXWC_COSTAR_GL_EXPORT $
   Module Name: XXWC_COSTAR_GL_EXPORT.pks

   PURPOSE:   This package is called by the concurrent programs
              XXWC CoStar GL Journal Import for importing Journals into EBS system.
   REVISIONS:
   Ver        Date        Author             	  Description
   ---------  ----------  ---------------   	  -------------------------
    1.0       08/07/2018  Vamshi Singirikonda  	Initial Build - Task ID: 20180709-00115						  
/*************************************************************************/
p_run_id       NUMBER;
p_group_id     VARCHAR2(240);

PROCEDURE Process_Journals(p_errbuf    OUT VARCHAR2
                          ,p_retcode   OUT VARCHAR2
                          ,p_file_name IN  VARCHAR2
                          ,p_run_id    IN  NUMBER
                          );

PROCEDURE Load_Journal_Data(p_run_id IN NUMBER);

FUNCTION get_field (v_delimiter  IN VARCHAR2
                   ,n_field_no   IN NUMBER 
                   ,v_line_read  IN VARCHAR2
                   ,p_which_line IN NUMBER) RETURN VARCHAR2;

FUNCTION get_run_id RETURN NUMBER;

PROCEDURE LOAD_AUDIT_DATA(p_run_id   IN NUMBER                         
                         ,p_stage    IN VARCHAR2);


END XXWC_COSTAR_GL_EXPORT;
/