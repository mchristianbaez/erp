CREATE OR REPLACE PACKAGE APPS.XXWC_DB_GATHER_STATS_PKG
AS
   /*************************************************************************
     $Header XXWC_DB_GATHER_STATS $
     Module Name: XXWC_DB_GATHER_STATS.pks

     PURPOSE:   Gather schema stats in DB for non-application schemas

     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.0        09/12/2016  Pattabhi Avula           Initial Version TMS#20160829-00243

   **************************************************************************/

   PROCEDURE DB_GATHER_STATS (errbuf              OUT VARCHAR2,
                              retcode             OUT VARCHAR2,
                              p_schema_value   IN     VARCHAR2);
END;
/