CREATE OR REPLACE PACKAGE APPS.XXWC_SPEEDBUILD_OB_PKG
AS
   /********************************************************************************
   FILE NAME: XXWC_SPEEDBUILD_OB_PKG.pks

   PROGRAM TYPE: PL/SQL Package Body

   PURPOSE: SpeedBuild Outbound Interfaces.

   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.0     11/20/2013    Gopi Damuluri    Initial creation of the procedure
   1.1     01/27/2014    Gopi Damuluri    TMS# 20140116-00071
                                          Added a new procedure - ORDER_HDR_LINE_ORG
   1.2     01/31/2014    Gopi Damuluri    TMS# 20140211-00049
                                          Added a new procedure - CREATE_CATALOG_FILE
   1.3     05/27/2014    Gopi Damuluri    TMS# 20140529-00334
                                          Catalog Filter at Master Party Level
   ********************************************************************************/


   PROCEDURE load_global_temp_tbl(p_date_from            IN VARCHAR2
                                , p_date_to              IN VARCHAR2);
   
   PROCEDURE create_file (p_errbuf              OUT VARCHAR2
                         ,p_retcode             OUT NUMBER
                         ,p_directory_path       IN VARCHAR2
                        , p_date_from            IN VARCHAR2
                        , p_date_to              IN VARCHAR2);

   PROCEDURE pc_duplicate_line(p_application_id                IN NUMBER,
                            p_entity_short_name             IN VARCHAR2,
                            p_validation_entity_short_name  IN VARCHAR2,
                            p_validation_tmplt_short_name   IN VARCHAR2,
                            p_record_set_short_name         IN VARCHAR2,
                            p_scope                         IN VARCHAR2,
                            x_result                       OUT NOCOPY NUMBER);

   FUNCTION csp_exists(p_item_id            IN NUMBER)
            RETURN NUMBER;

-- Verson# 1.1 > Start
   PROCEDURE order_hdr_line_org(p_application_id                IN NUMBER,
                            p_entity_short_name             IN VARCHAR2,
                            p_validation_entity_short_name  IN VARCHAR2,
                            p_validation_tmplt_short_name   IN VARCHAR2,
                            p_record_set_short_name         IN VARCHAR2,
                            p_scope                         IN VARCHAR2,
                            x_result                       OUT NOCOPY NUMBER );
-- Verson# 1.1 < End

-- Verson# 1.2 > Start
   PROCEDURE CREATE_CATALOG_FILE;
-- Verson# 1.2 < End

-- Verson# 1.3 > Start
   FUNCTION get_parent_party_num (p_party_id IN NUMBER) return VARCHAR2;
-- Verson# 1.3 < End

END XXWC_SPEEDBUILD_OB_PKG;