CREATE OR REPLACE PACKAGE   APPS.XXWC_INV_MONTH_END_REPORTS_PKG AS

/******************************************************************************
   NAME:       XXWC_INV_MONTH_END_REPORTS_PKG

   PURPOSE:    Create Inventory Financial Month End Data Exports
   

   REVISIONS:   
   Ver        Date         Author           Description
   ---------  -----------  ---------------  ------------------------------------
   1.0        13-MAR-2013  Lee Spitzer       1. Create the PL/SQL Package
******************************************************************************/
   --Define variables for logging debug messages
   g_level_unexpected CONSTANT   NUMBER := 6;
   g_level_error CONSTANT        NUMBER := 5;
   g_level_exception CONSTANT    NUMBER := 4;
   g_LEVEL_EVENT CONSTANT        NUMBER := 3;
   g_LEVEL_PROCEDURE CONSTANT    NUMBER := 2;
   g_LEVEL_STATEMENT CONSTANT    NUMBER := 1;
   
   g_package_name CONSTANT       VARCHAR2(30) := 'XXWC_INV_MONTH_END_REPORTS_PKG';
   g_name VARCHAR2(30);
   g_program_name VARCHAR2(60);
   g_exception EXCEPTION;
   g_user_id NUMBER := FND_GLOBAL.USER_ID;
   g_resp_id NUMBER := FND_GLOBAL.RESP_ID;
   g_resp_appl_id NUMBER := FND_GLOBAL.RESP_APPL_ID;
   g_message VARCHAR2(2000);
   g_count NUMBER DEFAULT 0;
   
/******************************************************************************

Procedure ALL_INVENTORIES_VALUES_CCR used to populate temp table XXWC.XXWC_ALL_INVENTORIES_VALUE_TMP

PROCEDURE  ALL_INVENTORY_VALUES_CCR
              (retbuf                         OUT VARCHAR2
              ,retcode                        OUT NUMBER
              ,p_date                         IN VARCHAR2);

******************************************************************************/

PROCEDURE  ALL_INVENTORY_VALUES_CCR
              (retbuf                         OUT VARCHAR2
              ,retcode                        OUT NUMBER
              ,p_date                         IN VARCHAR2);
 
/******************************************************************************

Procedure PERIOD_RECON_CCR used to populate temp table XXWC.XXWC_PERIOD_RECON_TEMP

PROCEDURE  PERIOD_RECON_CCR
              (retbuf                         OUT VARCHAR2
              ,retcode                        OUT NUMBER
              ,p_period                       IN VARCHAR2);

******************************************************************************/

PROCEDURE  PERIOD_RECON_CCR
              (retbuf                         OUT VARCHAR2
              ,retcode                        OUT NUMBER
              ,p_period                       IN VARCHAR2);

/******************************************************************************

Procedure RECEIVING_VALUES_CCR used to populate temp table XXWC.XXWC_RECEIVING_VALUE_TEMP

PROCEDURE  RECEIVING_VALUES_CCR
              (retbuf                         OUT VARCHAR2
              ,retcode                        OUT NUMBER
              ,p_date                         IN VARCHAR2);

******************************************************************************/

PROCEDURE  RECEIVING_VALUES_CCR
              (retbuf                         OUT VARCHAR2
              ,retcode                        OUT NUMBER
              ,p_date                         IN VARCHAR2);

/******************************************************************************

Procedure INTRANSIT_VALUES_CCR used to populate temp table XXWC.XXWC_INTRANSIT_VALUE_TEMP

PROCEDURE  INTRANSIT_VALUES_CCR
              (retbuf                         OUT VARCHAR2
              ,retcode                        OUT NUMBER
              ,p_date                         IN VARCHAR2);

******************************************************************************/

PROCEDURE  INTRANSIT_VALUES_CCR
              (retbuf                         OUT VARCHAR2
              ,retcode                        OUT NUMBER
              ,p_date                         IN VARCHAR2);
  
END XXWC_INV_MONTH_END_REPORTS_PKG;