CREATE OR REPLACE PACKAGE APPS.xxwc_edi_tp_extract_pkg
AS
/****************************************************************************************************
 *FUNCTION | PROCEDURE | CURSOR
 *XXWC_EDI_TP_EXTRACT_PKG
 *
 * DESCRIPTION
 *  TMS # 20140418-00209 EDI - Create automated extract and email of XXWC_EDI_TP_V to Liaison
 *
 * PARAMETERS
 * ==========
 * NAME              TYPE     DESCRIPTION
 * ----------------- -------- ------------------------------------------------------------------------
 * p_file_path       <IN>     User Needs to pass the P_file_path for generating the files 
 * p_file_name1      <IN>     EDI extract first file name 
 * p_file_name2      <IN>     EDI extract Second file name 
 * 
 * CALLED BY
 *   Which program, if any, calls this one
 *
 * HISTORY
 * =======
 *
 * VERSION DATE        AUTHOR(S)       DESCRIPTION
 * ------- ----------- --------------- ----------------------------------------------------------------
 * 1.0    19/08/2014   Veera C	       TMS # 20140418-00209 
 ******************************************************************************************************/
PROCEDURE generate_files(errbuf       OUT  VARCHAR2
                        ,retcode      OUT  NUMBER
                        ,p_file_path  IN   VARCHAR2
						,p_file_name1 IN   VARCHAR2
						,p_file_name2 IN   VARCHAR2);
								
PROCEDURE submit_job(errbuf       OUT  VARCHAR2
                    ,retcode      OUT  NUMBER
                    ,p_file_path  IN   VARCHAR2
					,p_file_name1 IN   VARCHAR2
					,p_file_name2 IN   VARCHAR2
				    ,p_resp_name  IN   VARCHAR2
					,p_user_name  IN   VARCHAR2
					,p_org_name   IN   VARCHAR2);
								
END xxwc_edi_tp_extract_pkg;
/
GRANT EXECUTE ON apps.xxwc_edi_tp_extract_pkg TO interface_prism
/
GRANT EXECUTE ON apps.xxwc_edi_tp_extract_pkg TO interface_xxcus
/