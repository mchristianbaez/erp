CREATE OR REPLACE PACKAGE APPS.xxwc_obiee_etl_pre_process
IS
   /*************************************************************************
     $Header xxwc_obiee_etl_pre_process.pks $
     Module Name: xxwc_obiee_etl_pre_process

     PURPOSE: Package for Loading Inventory Dimension and Inventory Snapshot

     TMS Task Id :  20140425-00055

     REVISIONS:
     Ver        Date         Author                Description
     -----  -----------  ------------------    ----------------
     1.0    01-May-2014     Rasikha Galimova      Initial Version
     1.1    15-May-2014     Manjula Chellappan    Added the objects related to Inventory Snapshot
     1.2    27-May-2014     Manjula Chellappan    Added procedure submit_inv_dimension
     1.3    21-Jul-2014     Manjula Chellappan    Added procedure populate_special_item_vendor	
						  TMS# : 20140716-00161

   **************************************************************************/

   PROCEDURE populate_etl_inv_stage (errbuf OUT VARCHAR2, retcode OUT NUMBER);

   --PROCEDURE drop_table (p_owner VARCHAR2, p_table_name VARCHAR2);

   PROCEDURE alter_table_temp (p_owner VARCHAR2, p_table_name VARCHAR2);

   PROCEDURE create_copy_table (p_owner             VARCHAR2,
                                p_table_name        VARCHAR2,
                                p_new_table_name    VARCHAR2);

   PROCEDURE populate_inv_data_from_eisr;

   PROCEDURE get_inv_last_pay_site;

   PROCEDURE delete_dups_in_driver_table;

   PROCEDURE populate_obiee_etl_pre_oel;

   -- Version 1.1

   PROCEDURE load_snapshot_main (errbuf OUT VARCHAR2, retcode OUT NUMBER);

   PROCEDURE load_inv_item (p_retcode OUT NUMBER);

   PROCEDURE load_po_open (p_retcode OUT NUMBER);

   PROCEDURE load_inv_onhand (p_retcode OUT NUMBER);

   PROCEDURE load_inv_vndrcons (p_retcode OUT NUMBER);

   PROCEDURE load_inv_category (p_retcode OUT NUMBER);

   PROCEDURE load_so_backorder (p_retcode OUT NUMBER);

   PROCEDURE load_inv_intransit (p_retcode OUT NUMBER);

   PROCEDURE load_inv_cyclecount (p_retcode OUT NUMBER);

   PROCEDURE load_inv_receipt (p_retcode OUT NUMBER);

   PROCEDURE load_so_sales (p_retcode OUT NUMBER);

   PROCEDURE load_inv_snapshot (p_retcode OUT NUMBER);

   PROCEDURE submit_inv_snapshot (errbuf                     OUT VARCHAR2,
                                  retcode                    OUT NUMBER,
                                  p_user_name             IN     VARCHAR2,
                                  p_responsibility_name   IN     VARCHAR2);                                

   PROCEDURE send_mail (p_procedure_name   IN     VARCHAR2,
                        p_section          IN     VARCHAR2,
                        p_request_id       IN     NUMBER,
                        p_error_msg        IN     VARCHAR2,
                        p_retcode             OUT NUMBER);
-- Version 1.1

-- Version 1.2                                 
   PROCEDURE submit_inv_dimension(errbuf                     OUT VARCHAR2,
                                  retcode                    OUT NUMBER,
                                  p_user_name             IN     VARCHAR2,
                                  p_responsibility_name   IN     VARCHAR2);  
-- Version 1.2  
-- Version 1.3                                
   PROCEDURE populate_special_item_vendor;  
-- Version 1.3                                 
END XXWC_OBIEE_ETL_PRE_PROCESS;
/
