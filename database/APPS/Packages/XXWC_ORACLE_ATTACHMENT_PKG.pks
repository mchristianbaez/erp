create or replace package APPS.XXWC_ORACLE_ATTACHMENT_PKG as
  -----------------------------------------------------------------------------
  -- � Copyright 2008, Nancy Pahwa
  -- All Rights Reserved
  --
  -- Name           : XXWC_ORACLE_ATTACHMENT_PKG- Cleanup Orphan attachment records in Oracle EBS
  -- Date Written   : 11-May-2016
  -- Author         : Nancy Pahwa
  --
  -- Modification History:
  --
  -- Version When         Who        Did what
  -- ------- -----------  --------   -----------------------------------------------------
  -- 1.1    11-July-2017  nancypahwa   Initially Created TMS# 20170616-00237
  ---------------------------------------------------------------------------------
  procedure attachment_wrapper(errbuf           OUT VARCHAR2,
                               retcode          OUT NUMBER,
                               p_entity_name    varchar2,
                               p_from_date      varchar2,
                               p_to_date        varchar2,
                               p_operating_unit varchar2);

  procedure add_new_attachment(errbuf OUT VARCHAR2, retcode OUT NUMBER);
  procedure add_data_to_archive(errbuf OUT VARCHAR2, retcode OUT NUMBER);
end;