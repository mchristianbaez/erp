CREATE OR REPLACE PACKAGE APPS.XXWC_RCV_INTORG_PKG AS

/******************************************************************************
   NAME:       XXWC_RVC_INTORG_PKG

   PURPOSE:    Process Interorg Shipments That Were Received Short And Intorg Transfer It Back to the Shipping Organization
   

   REVISIONS:   
   Ver        Date         Author           Description
   ---------  -----------  ---------------  ------------------------------------
   1.0        03-NOV-2012  Lee Spitzer       1. Create the PL/SQL Package
   1.1        28-AUG-2013  Lee Spitzer       1.1.  Update Package for TMS Ticket - 20130826-00733 BOD Issue on Transfers
******************************************************************************/
   --Define variables for logging debug messages
   g_level_unexpected CONSTANT   NUMBER := 6;
   g_level_error CONSTANT        NUMBER := 5;
   g_level_exception CONSTANT    NUMBER := 4;
   g_LEVEL_EVENT CONSTANT        NUMBER := 3;
   g_LEVEL_PROCEDURE CONSTANT    NUMBER := 2;
   g_LEVEL_STATEMENT CONSTANT    NUMBER := 1;
   
   g_package_name CONSTANT       VARCHAR2(30) := 'XXWC_RCV_INTORG_PKG';
   g_name VARCHAR2(30);
   g_program_name VARCHAR2(60);
   g_exception EXCEPTION;
   g_user_id NUMBER := FND_GLOBAL.USER_ID;
   g_resp_id NUMBER := FND_GLOBAL.RESP_ID;
   g_resp_appl_id NUMBER := FND_GLOBAL.RESP_APPL_ID;
   g_message VARCHAR2(2000);
   g_count NUMBER DEFAULT 0;
   g_lot_control_code NUMBER;
   g_suppress_traveler varchar2(1);
   g_rcv_subinventory varchar2(10);
   g_rtv_subinventory varchar2(10);
   g_process_flag NUMBER;
   g_reason_id NUMBER;
   g_shipment_line_id NUMBER; -- Added 3/21/13 TMS Ticket # 20130228-01300
   g_proc_c_dff VARCHAR2(1); -- Added 4/05/13 CG
   
 
/*PROCEDURE INSERT_INTO_RCV_STAGE Used to query Interg Org Shipments that were received short and insert into XXWC_RCV_INTORG_TBL
          (P_ORG_ID    IN NUMBEr --Operating Unit
           P_DATE     IN DATE, --Date
           X_BATCH_ID OUT NUMBER, --Return the Batch Id to process for the interface table
           X_RETCODE OUT NUMBER) --Return output from the procudure
*/

PROCEDURE INSERT_INTO_RCV_STAGE
          (P_ORG_ID IN NUMBER,
           P_DATE     IN DATE,
           P_TRANSACTION_TYPE IN VARCHAR2,
           P_GROUP_ID IN NUMBER,
           P_BATCH_ID IN NUMBER,
           X_RETCODE OUT NUMBER);
           
/*PROCEDURE INSERT_INTO_RCV_INTERFACE Used to insert records from XXWC_RCV_INTORG_TBL to Receiving Interface Tables
          (P_BATCH_ID   IN NUMBER, --Batch ID
           P_SUBINVENTORY IN VARCHAR2 -- Subinventory to Preform the Receiving Transaction
           X_RETCODE    OUT NUMBER) --Return output from the procedure
*/

PROCEDURE INSERT_INTO_RCV_INTERFACE
          (P_ORG_ID IN NUMBER,
           P_BATCH_ID IN NUMBER,
           P_SUBINVENTORY IN VARCHAR2,
           P_TRANSACTION_TYPE IN VARCHAR2,
           X_RETCODE OUT NUMBER,
           X_RCV_GROUP_ID OUT NUMBER);

/*PROCEDURE process_rcv_mgr  Used to Call the RCV Transaction Manager   
                              (p_rcv_group_id  IN   NUMBER,  RCV Batch Id 
                               p_org_id        IN   NUMBER,  ORG ID
                               x_trans_count   OUT  NUMBER,  Out Count
                               x_return_status OUT  VARCHAR2  Out Status
                              )

*/
PROCEDURE PROCESS_RCV_MGR    ( P_RCV_GROUP_ID  IN   NUMBER,
                               P_ORG_ID        IN   NUMBER,
                               X_RETCODE       OUT  NUMBER
                              );
                              

PROCEDURE INSERT_INTO_MT_INTERFACE
          (P_ORG_ID IN NUMBER,
           P_BATCH_ID IN NUMBER,
           P_SUBINVENTORY IN VARCHAR2,
           P_TRANSACTION_TYPE IN VARCHAR2,
           X_RETCODE OUT NUMBER,
           X_TXN_HEADER_ID OUT NUMBER,
           X_TRANSACTION_DATE OUT DATE);
           
PROCEDURE PROCESS_TXN_MGR
            (P_TXN_HDR_ID   IN   NUMBER,
             X_TRANS_COUNT  OUT  NUMBER,
             X_RETCODE      OUT  NUMBER);

/*FUNCTION LOT_CONTROL_CODE 
        (P_INVENTORY_ITEM_ID IN NUMBER, --inventory item id
         P_ORGANIZATION_ID IN NUMBER organization_id) --organization_id
        RETURN NUMBER --1 No Control Code and 2 Lot contorl
*/

FUNCTION  LOT_CONTROL_CODE
            (P_INVENTORY_ITEM_ID IN NUMBER,
             P_ORGANIZATION_ID IN NUMBER)
    RETURN NUMBER;
    
    
PROCEDURE  MAIN_PROGRAM_CCR
              (retbuf         OUT  VARCHAR2,
               retcode        OUT  NUMBER,
               P_ORG_ID IN NUMBER,
               P_DATE     IN VARCHAR2,
               P_RCV_SUBINVENTORY IN VARCHAR2,
               P_RTV_SUBINVENTORY IN VARCHAR2,
               P_PROCESS_FLAG IN NUMBER,
               P_BATCH_ID IN NUMBER,
               P_SUPPRESS_RECEIPT_TRAVELER IN NUMBER,
               P_REASON_ID IN NUMBER,
               P_SHIPMENT_LINE_ID IN NUMBER, --Added 3/21/13 TMS Ticket # 20130228-01300
               P_PROC_C_DFF IN VARCHAR2); -- Added 04/05/13 CG 


FUNCTION  GET_ORGANIZATION_CODE
            (P_ORGANIZATION_ID IN NUMBER)
    RETURN VARCHAR2;              
           
FUNCTION GET_LOT_NUMBER
            (P_SHIPMENT_LINE_ID IN NUMBER)
    RETURN VARCHAR2; 
           

END XXWC_RCV_INTORG_PKG;
/
