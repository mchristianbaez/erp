CREATE OR REPLACE PACKAGE APPS.xxwcap_vendor_upd_pkg IS

  /*******************************************************************************
  -- Procedure: xxwcap_vendor_upd_pkg
  -- Description:  Program to update vendors site code/name based on the data 
  --               loaded provided by the user.
  --
  -- HISTORY
  ===============================================================================
  --         Last Update Date : 05/Mar/2013
  ===============================================================================
  ===============================================================================
  -- VERSION  DATE          AUTHOR(S)       DESCRIPTION
  -- ------- -----------   --------------- --------------------------------------
  -- 1.0     05-Mar-2013   Luong Vu    Created this package.
  *******************************************************************************/


  PROCEDURE update_ven_site_code(errbuf  OUT VARCHAR2
                                ,retcode OUT VARCHAR2);

  PROCEDURE update_party_site_name(errbuf  OUT VARCHAR2
                                  ,retcode OUT VARCHAR2);


END xxwcap_vendor_upd_pkg;
/
