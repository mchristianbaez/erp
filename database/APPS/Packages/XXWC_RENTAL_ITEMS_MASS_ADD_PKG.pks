--
-- XXWC_RENTAL_ITEMS_MASS_ADD_PKG  (Package) 
--
CREATE OR REPLACE PACKAGE APPS.XXWC_RENTAL_ITEMS_MASS_ADD_PKG
AS
   /*************************************************************************
   *  Copyright (c) 2011 Lucidity Consulting Group
   *  All rights reserved.
   **************************************************************************
   *   $Header XXWC_RENTAL_ITEM_MASS_ADD_PKG.pks $
   *   Module Name: XXWC_RENTAL_ITEM_MASS_ADD_PKG.sql
   *
   *   PURPOSE:   This package is for Categorizing Rental Items to FA MASS ADDITIONS
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        09/07/2011  Srinivas Malkapuram      Initial Version
   *   1.1        10/22/2014  Maharajan Shunmugam      TMS# 20141001-00248 Canada Multi Org changes
   * ***************************************************************************/

   --Declaration of Variables for Error/Warning Messages

   C_LEVEL_UNEXPECTED                    NUMBER DEFAULT Fnd_Log.LEVEL_UNEXPECTED;
   C_LEVEL_ERROR                         NUMBER DEFAULT Fnd_Log.LEVEL_ERROR;
   C_LEVEL_EXCEPTION                     NUMBER DEFAULT Fnd_Log.LEVEL_EXCEPTION;
   C_LEVEL_EVENT                         NUMBER DEFAULT Fnd_Log.LEVEL_EVENT;
   C_LEVEL_PROCEDURE                     NUMBER DEFAULT Fnd_Log.LEVEL_PROCEDURE;
   C_LEVEL_STATEMENT                     NUMBER DEFAULT Fnd_Log.LEVEL_STATEMENT;
   C_YES                        CONSTANT VARCHAR2 (1) := 'Y';
   C_NO                         CONSTANT VARCHAR2 (1) := 'N';
   C_RETURN_STATUS_SUCCESS      CONSTANT VARCHAR2 (1) := 'S';
   C_RETURN_STATUS_ERROR        CONSTANT VARCHAR2 (1) := 'E';
   C_RETURN_STATUS_UNEXPECTED   CONSTANT VARCHAR2 (1) := 'U';

   --G_RESP_NAME                           VARCHAR2 (100)   := FND_PROFILE.VALUE ('RESP_NAME');
   --G_RESP_ID                             NUMBER  := FND_PROFILE.VALUE ('RESP_ID');
   --G_USER_ID                             NUMBER  := FND_PROFILE.VALUE ('USER_ID');
--commented and added below for ver#1.1

   G_RESP_NAME                           VARCHAR2 (100)   := fnd_global.resp_name;
   G_RESP_ID                             NUMBER           := fnd_global.resp_id;
   G_USER_ID                             NUMBER           := fnd_global.user_id;

G_MODULE_NAME                CONSTANT VARCHAR2 (60)
      := 'PLSQL.XWC_RENTAL_ITEM_MASS_ADD_PKG' ;
   G_LEVEL_STATEMENT                     NUMBER
                                            DEFAULT Fnd_Log.LEVEL_STATEMENT;
   G_LEVEL_PROCEDURE                     NUMBER
                                            DEFAULT Fnd_Log.LEVEL_PROCEDURE;

   G_TRANSACTION_TYPE                    VARCHAR2 (100)
                                            := 'MISCELLANEOUS RECEIPT'; --'ISSUE TO RENTALS';
   G_RENTAL_SUBINV_CODE                  VARCHAR2 (40)
      := FND_PROFILE.VALUE ('XXWC_RENTAL_SUBINV_CODE');            --'RENTAL';
   G_TRXN_ACTION_LOOKUP                  VARCHAR2 (100)
                                            := 'MTL_TRANSACTION_ACTION';
   G_TRANSACTION_ACTION                  VARCHAR2 (100)
                                            := 'RECEIPT INTO STORES'; --'SUBINVENTORY TRANSFER';
   G_TRANSACTION_SOURCE                  VARCHAR2 (100) := 'SUBINVENTORY';
   G_ASSET_TYPE                          VARCHAR2 (100) := 'CAPITALIZED';
   G_POSTING_STATUS                      VARCHAR2 (40) := 'NEW';
   G_QUEUE_NAME                          VARCHAR2 (40) := 'NEW';
   G_BOOKTYPE_CODE                       VARCHAR2 (15)
      := FND_PROFILE.VALUE ('XXWC_BOOKTYPE_CODE');          --'HDS WHITE CAP';      --All below profiles exended to BRF for ver 1.1
   G_LOC_SEGMENT3                        VARCHAR (100)
      := FND_PROFILE.VALUE ('XXWC_LOCATION_SEG3'); --'HDS CONSTR SUPPLY GROUP';

   G_EXP_SEGMENT1                        VARCHAR2 (20)
      := FND_PROFILE.VALUE ('XXWC_EXP_SEGMENT1');               --'0W'       ;
   G_EXP_SEGMENT2                        VARCHAR2 (20)
      := FND_PROFILE.VALUE ('XXWC_EXP_SEGMENT2');               --'BW001'    ;
   G_EXP_SEGMENT3                        VARCHAR2 (20)
      := FND_PROFILE.VALUE ('XXWC_EXP_SEGMENT3');               --'0000'     ;
   G_EXP_SEGMENT4                        VARCHAR2 (20)
      := FND_PROFILE.VALUE ('XXWC_EXP_SEGMENT4');               --'701100'   ;
   G_EXP_SEGMENT5                        VARCHAR2 (20)
      := FND_PROFILE.VALUE ('XXWC_EXP_SEGMENT5');               --'00000'    ;
   G_EXP_SEGMENT6                        VARCHAR2 (20)
      := FND_PROFILE.VALUE ('XXWC_EXP_SEGMENT6');               --'00000'    ;
   G_EXP_SEGMENT7                        VARCHAR2 (20)
      := FND_PROFILE.VALUE ('XXWC_EXP_SEGMENT7');               --'00000'    ;

   /*
   G_ASSET_SEGMENT1                VARCHAR2(20)  := '0W'       ;
   G_ASSET_SEGMENT2                VARCHAR2(20)  := 'BW001'    ;
   G_ASSET_SEGMENT3                VARCHAR2(20)  := '0000'     ;
   G_ASSET_SEGMENT4                VARCHAR2(20)  := '165100'   ;
   G_ASSET_SEGMENT5                VARCHAR2(20)  := '00000'    ;
   G_ASSET_SEGMENT6                VARCHAR2(20)  := '00000'    ;
   G_ASSET_SEGMENT7                VARCHAR2(20)  := '00000'    ;
   */

   PROCEDURE MAIN (errbuf             OUT VARCHAR2
                  ,retcode            OUT NUMBER
                  ,p_Log_Enabled   IN     VARCHAR2 DEFAULT 'N');



   FUNCTION GET_CATEGORY_ID (p_segment1 IN VARCHAR2, p_segment2 IN VARCHAR2)
      RETURN NUMBER;


   FUNCTION GET_LOCATION_ID (p_segment1   IN VARCHAR2
                            ,p_segment2   IN VARCHAR2
                            ,p_segment3   IN VARCHAR2
                            ,p_segment4   IN VARCHAR2
                            ,p_segment5   IN VARCHAR2)
      RETURN NUMBER;

   FUNCTION GET_CODE_COMBINATION_ID (p_segment1   IN VARCHAR2
                                    ,p_segment2   IN VARCHAR2
                                    ,p_segment3   IN VARCHAR2
                                    ,p_segment4   IN VARCHAR2
                                    ,p_segment5   IN VARCHAR2
                                    ,p_segment6   IN VARCHAR2
                                    ,p_segment7   IN VARCHAR2)
      RETURN NUMBER;

   FUNCTION GET_SERIAL_NUMBER (p_Transfer_Trx_ID IN NUMBER)
      RETURN VARCHAR2;

   --- FUNCTION GET_LOCATION_SEGMENT (Organization_ID Numnber )  Return  Varchar2
   --- Function Returns  Expnese Account Location Segment for a given Organization
   FUNCTION GET_LOCATION_SEGMENT (P_Organization_ID IN NUMBER)
      RETURN VARCHAR2;



   FUNCTION IS_ITEM_SERIALIZED (p_Transfer_Trx_ID     IN NUMBER
                               ,p_Inventory_Item_ID   IN NUMBER)
      RETURN BOOLEAN;

   FUNCTION GET_VENDOR_NUMBER (p_VendorName IN VARCHAR2)
      RETURN NUMBER;
END XXWC_RENTAL_ITEMS_MASS_ADD_PKG;
/

