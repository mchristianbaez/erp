CREATE OR REPLACE PACKAGE apps.xxwc_ego_uda_mass_upload_pkg
/*********************************************************************************
File Name:    xxwc_ego_uda_mass_upload_pkg
PROGRAM TYPE: PL/SQL Package specification
PURPOSE:      Procedure for validating and processing UDAs
==================================================================================
VERSION DATE          AUTHOR(S)               DESCRIPTION
------- -----------   --------------- --------------------------------------------
1.0     18-Apr-2014   Praveen Pawar           Initial creation of the package spec
1.1     06-May-2014   Praveen Pawar           Added a new parameter file name
*********************************************************************************/
AS
   PROCEDURE process_udas (
      errbuf    OUT NOCOPY   VARCHAR2,
      retcode   OUT NOCOPY   VARCHAR2,
						pv_file_name  IN       VARCHAR2
   );
END xxwc_ego_uda_mass_upload_pkg;