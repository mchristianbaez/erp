
  CREATE OR REPLACE PACKAGE "APPS"."XXCUS_MISC_PKG" 
AS
   /**************************************************************************
    File Name:xxhsi_misc_pkg
    PROGRAM TYPE: PL/SQL
    PURPOSE: Miscellaneous Packages
    HISTORY
    ================================================================
           Last Update Date : 06/15/2004
    ================================================================
    ================================================================
    VERSION DATE          AUTHOR(S)       DESCRIPTION
    ------- -----------   --------------- ---------------------------------
     1.0    06-JUN-2010   Kathy Poling     Creation taken from Edric and Jason
     1.1    02-JAN-2013   Luong Vu         Added function create_ccid_generic
     1.3    19-JUN-2014   Praveen Pawar    Added procedure send_email_attachment TMS # 20140507-00218
     1.4    27-APR-2016   Rakesh Patel     Added a new default parameter to email_attachment for TMS#20151023-00047
   **************************************************************************/

   --13-NOV-2006   Edric Dearmas Add Function set_responsibility Begin
   FUNCTION set_responsibility (
      p_user   IN   VARCHAR2 DEFAULT NULL,
      p_resp   IN   VARCHAR2 DEFAULT NULL
   )
      RETURN BOOLEAN;

   PROCEDURE html_email (
      p_to              IN   VARCHAR2,
      p_from            IN   VARCHAR2,
      p_subject         IN   VARCHAR2,
      p_text            IN   VARCHAR2 DEFAULT NULL,
      p_html            IN   VARCHAR2 DEFAULT NULL,
      p_smtp_hostname   IN   VARCHAR2,
      p_smtp_portnum    IN   VARCHAR2
   );

   FUNCTION email_attachment (
      p_from                   IN   VARCHAR2,
      p_to                     IN   VARCHAR2,
      p_cc                     IN   VARCHAR2,
      p_bcc                    IN   VARCHAR2,
      p_subject                IN   VARCHAR2,
      p_body                   IN   VARCHAR2,
      p_smtp_host              IN   VARCHAR2,
      p_attachment_data        IN   BLOB,
      p_attachment_type        IN   VARCHAR2,
      p_attachment_file_name   IN   VARCHAR2
   )
      RETURN NUMBER;

   PROCEDURE send_mail (
      p_sender      IN   VARCHAR2,
      p_recipient   IN   VARCHAR2,
      p_message     IN   VARCHAR2
   );

   FUNCTION get_ccid (p_branch VARCHAR2, p_cost_center VARCHAR2)
      RETURN NUMBER;

   PROCEDURE purge_data (errbuf OUT VARCHAR2, retcode OUT NUMBER);

   FUNCTION get_description (p_proc_name VARCHAR2, p_parm_name VARCHAR2)
      RETURN VARCHAR2;

   FUNCTION get_ccid_create (p_branch VARCHAR2, p_cost_center VARCHAR2)
      RETURN NUMBER;

   PROCEDURE hr_assign_detail_fix (
      errbuf      OUT      VARCHAR2,
      retcode     OUT      NUMBER,
      p_emp_num   IN       VARCHAR2
   );

   FUNCTION create_ccid_generic (
      p_entrp_entity   VARCHAR2,
      p_entrp_loc      VARCHAR2,
      p_entpr_cc       VARCHAR2,
      p_account        VARCHAR2,
      p_segment5       VARCHAR2,
      p_segment6       VARCHAR2,
      p_segment7       VARCHAR2
   )
      RETURN NUMBER;

-------------------------
-- Added w.r.t ver 1.3 --
              /**************************************************************************
               *
               * PROCEDURE
               *  send_email_attachment
               *
               * DESCRIPTION
               *  This procedure will accept sender, receipient, subject and message as input parameters in addition to .xls attachment
               *
               * PARAMETERS
               * ==========
               * NAME                          TYPE     DESCRIPTION
               * -----------------             -------- ---------------------------------------------
               * p_sender        IN OUT   VARCHAR2      email sender
               * p_recipients    IN       VARCHAR2      email recepient
               * p_subject       IN       VARCHAR2      email subject
               * p_message       IN       VARCHAR2      email body
               * p_attachments   IN       VARCHAR2      email attachment
               * x_result        OUT      VARCHAR2      procedure result
               * x_result_msg    OUT      VARCHAR2      procedure result message
               * p_directory     IN       VARCHAR2      to pass directory name 
				    ================================================================
    VERSION DATE          AUTHOR(S)       DESCRIPTION
    ------- -----------   --------------- ---------------------------------
     1.3    19-JUN-2014   Praveen Pawar    Added procedure send_email_attachment TMS # 20140507-00218
     1.4    27-APR-2016   Rakesh Patel     Added a new default parameter to email_attachment for TMS # TMS#20151023-00047
               *************************************************************************/

   -------------------------
   PROCEDURE send_email_attachment (
      p_sender        IN       VARCHAR2,
      p_recipients    IN       VARCHAR2,
      p_subject       IN       VARCHAR2,
      p_message       IN       VARCHAR2,
      p_attachments   IN       VARCHAR2,
      x_result        OUT      VARCHAR2,
      x_result_msg    OUT      VARCHAR2,
      p_directory     IN       VARCHAR2 DEFAULT NULL -- Added for version 1.4
   );

---------------------------------------------
-- End of code modification w.r.t. ver 1.3 --
---------------------------------------------
   PROCEDURE sleep (seconds IN NUMBER);

   /******************************************************************************
   -- |-----------------------< uc4_xx_iface_purge_files >------------------------|
   -- ----------------------------------------------------------------------------
   --   VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- --------------------------------------
   -- 1.0     09-Apr-2014   Kathy Poling     Created this procedure SR 475294
   -- ****************************************************************************/
   PROCEDURE uc4_xx_iface_purge_files (
      errbuf                  OUT      VARCHAR2,
      retcode                 OUT      NUMBER,
      p_user_name             IN       VARCHAR2,
      p_responsibility_name   IN       VARCHAR2,
      p_intf_name             IN       VARCHAR2,
      p_dir_path              IN       VARCHAR2,
      p_retention_days        IN       VARCHAR2,
      p_file_prefix           IN       VARCHAR2
   );
END xxcus_misc_pkg;
/

