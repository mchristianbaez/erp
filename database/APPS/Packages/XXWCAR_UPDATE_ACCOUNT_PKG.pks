CREATE OR REPLACE PACKAGE APPS.XXWCAR_UPDATE_ACCOUNT_PKG AS
/********************************************************************************

FILE NAME: APPS.XXWCAR_UPDATE_ACCOUNT_PKG.pkb

PROGRAM TYPE: PL/SQL Package

PURPOSE: API to update Cusotmer Information

HISTORY
===============================================================================
VERSION DATE          AUTHOR(S)           DESCRIPTION
------- -----------   --------------- -----------------------------------------
1.0     02/25/2014   Maharajan Shunmugam  Initial version
********************************************************************************/

PROCEDURE UPDATE_CASH_ACCOUNT      (p_errbuf            OUT      VARCHAR2
                                  , p_retcode           OUT      NUMBER);

PROCEDURE update_profile_class(p_cust_account_profile_id     IN NUMBER
                             , p_object_version_num          IN NUMBER
                             , p_profile_class_id            IN NUMBER);

END XXWCAR_UPDATE_ACCOUNT_PKG;
