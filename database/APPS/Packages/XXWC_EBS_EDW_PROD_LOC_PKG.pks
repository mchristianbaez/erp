CREATE OR REPLACE PACKAGE APPS.xxwc_ebs_edw_prod_loc_pkg
--//============================================================================
--//
--// Object Name         :: xxwc_ebs_edw_prod_loc_pkg
--//
--// Object Type         :: Package Specification
--//
--// Object Description  :: This is an outbound interface from oracle ebs to edw.
--//                        This interface will populate the base table for VIEW
--//                        xxwc.xxwc_inv_product_loc_ext_vw
--//
--// Version Control
--//============================================================================
--// Vers    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Mahesh Kudle     15/10/2013    Initial Build - TMS#20130626-01200
--// 2.0     Maharajan Shunmugam  07/10/2014    TMS# 20140102-00072 Necessary Update to list_price 
--//                                            logic in EDW Product Location Extract
--//============================================================================
IS

--//============================================================================
--//
--// Object Name         :: populate xxwc_ebs_order_lines_p
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This is used to populate oe_order_lines_all data into
--//                        xxwc.xxwc_oe_order_lines_prod_loc table
--//
--// Version Control
--//============================================================================
--// Vers    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Mahesh Kudle     15/10/2013    Initial Build - TMS#20130626-01200
--//============================================================================
PROCEDURE xxwc_ebs_order_lines_p;
                                   
--//============================================================================
--// Procedure Name     :: populate_inv_prod_loc_temp
--//
--// Description        :: This is an outbound interface from oracle ebs to edw.
--//                       This interface will populate the base table for VIEW
--//                       xxwc.xxwc_inv_product_loc_ext_vw.
--//
--// Parameters         :: errbuf   OUT VARCHAR2
--//                       retcode  OUT NUMBER
--//
--// Change Notes
--//----------------------------------------------------------------------------
--// Vers    Author               Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Mahesh Kudle         15/10/2013    Initial Build - TMS#20130626-01200
--// 1.1     Harsha Yedla         14/01/2014    Changed function for list price TMS#20140102-00072 
--// 2.0     Maharajan Shunmugam  07/10/2014    TMS# 20140102-00072 Necessary Update to list_price 
--//                                            logic in EDW Product Location Extract
--//============================================================================
PROCEDURE populate_inv_prod_loc_temp (p_errbuf  OUT VARCHAR2
                                     ,p_retcode OUT NUMBER);
--Added below variables by Maha for ver 2.0
G_MARKET_NTL_PRL_ID     NUMBER := fnd_profile.value ('XXWC_QP_MARKET_NATIONAL_PRL');
G_MARKET_NTL_STP_PRL_ID NUMBER := fnd_profile.value ('XXWC_QP_MARKET_NTL_SETUP_PRL');
G_MARKET_CAT_PRL_ID     NUMBER := fnd_profile.value ('XXWC_QP_MARKET_CATEGORY_PRL');

END;
/
