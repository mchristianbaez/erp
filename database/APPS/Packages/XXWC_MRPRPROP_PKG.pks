--
-- XXWC_MRPRPROP_PKG  (Package) 
--
CREATE OR REPLACE PACKAGE APPS.XXWC_MRPRPROP_PKG
AS
   /******************************************************************************
      NAME:       xxwc_mrprprop_pkg

      PURPOSE:    Update Min and Max Levels after Reorder Point Planning and truncate staging table

      Logic:     1) Reorder Point Planning Report will insert records into xxwc_mrprprop that holds min and max quantities
                 2) Process Records on xxwc_mrprprop
                 3) If user select truncate temp table, the purge the records

      REVISIONS:
      Ver        Date        Author           Description
      ---------  ----------  ---------------  ------------------------------------
      1.0        29-Dec-11   Lee Spitzer       1. Created this trigger.
   ******************************************************************************/

   /******************************************************************************
     update_items_ccr
       Concurrent Request Wrapper to process records on XXWC_MRPRPROP Staging Table and Purge Table
           Parameters:
               P_Batch_Id = Batch_ID to process from XXWC_MRPROP
               P_USER_ID  = User Id from fnd_user
               P_PURGE  = Yes = 1, No = 2

   ******************************************************************************/
   PROCEDURE UPDATE_ITEMS_CCR (ERRBUF          OUT NOCOPY VARCHAR2
                              ,RETCODE         OUT        NUMBER
                              ,P_BATCH_ID   IN            NUMBER
                              ,P_USER_ID    IN            NUMBER
                              ,P_PURGE      IN            NUMBER);

   /******************************************************************************
       Procedure  update_items
        Used to call the API to update items and update the min_minmax_quantity and max_minmax_quantity
           Parameters:
               P_Batch_Id = Batch_ID to process from XXWC_MRPROP
               P_USER_ID  = User Id from fnd_user

   ******************************************************************************/
   PROCEDURE update_items (P_BATCH_ID        IN     NUMBER
                          ,P_USER_ID         IN     NUMBER
                          ,P_RETURN_STATUS      OUT NUMBER);


   /******************************************************************************
       Procedure  purge_table
        Used to delete records with corresponding batch_id
           Parameters:
               P_Batch_Id = Batch_ID to process from XXWC_MRPROP

   ******************************************************************************/

   PROCEDURE purge_table (P_BATCH_ID IN NUMBER, P_RETURN_STATUS OUT NUMBER);
END;
/

