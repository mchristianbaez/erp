CREATE OR REPLACE 
PACKAGE      APPS.XXWC_AHH_CONV_SCRIPTS_PKG
   AUTHID CURRENT_USER
AS
   /******************************************************************************************************************************************************
        $Header XXWC_AHH_CONV_SCRIPTS_PKG $
        Module Name: XXWC_AHH_CONV_SCRIPTS_PKG.pks

        PURPOSE:   AH Harries Conversion Project

        REVISIONS:
        Ver        Date        Author             Description
        ---------  ----------  --------------- ----------------------------------------------------------------------------------------------------------
        1.0        07/03/2018  Pattabhi Avula     TMS#20180703-00044 - XXWC AHH Script – Prism Site Number
   ******************************************************************************************************************************************************/

   PROCEDURE Update_Prism_Site_DFF (errbuff       OUT VARCHAR2,
                                    retcode       OUT VARCHAR2,
									p_to_date     IN  VARCHAR2);
									
   PROCEDURE update_dup_cust_locations (errbuff       OUT VARCHAR2,
                                        retcode       OUT VARCHAR2,
									    p_to_date     IN  VARCHAR2);
END;
/