/* Formatted on 3/12/2015 10:06:01 AM (QP5 v5.254.13202.43190) */
CREATE OR REPLACE PACKAGE apps.xxwc_qp_extracts_pkg
AS
   /**********************************************************************************************************************************************
    *
    * HEADER
    *   Price List / Offer Interface to Commerce
    *
    * PROGRAM NAME
    *  XXWC_QP_EXTRACTS_PKG.sql
    *
    * DESCRIPTION
    *  this package contains all of the procedures/functions used to generate price list and offer extract files for Commerce interface
    *
    * LAST UPDATE DATE   10-OCT-2012
    *   Date the program has been modified for the last time
    *
    * HISTORY
    * =======
    *
    * VERSION DATE        AUTHOR(S)       DESCRIPTION
    * ------- ----------- --------------- ------------------------------------
    * 1.00    10-OCT-2012 Scott Spivey    Lucidity Consulting Group - Creation
    * 1.10    21-NOV-2012 Scott Spivey    resolve duplicate offer records when duplicate rows in QP_MODIFIER_SUMMARY_V
    *                                     resolve duplicate offer records when item-specific pricing and category-based pricing
    *                                     resolve duplicate indicator field value when item-specific princing and category-based pricing
    * 1.20    17-DEC-2012 Scott Spivey    add logic to generate additional WhiteCapList price list
    * 1.30    08-Jan-2013 Scott Spivey    alter logic to ignore active_flag on modifier list header
    *                                     modularize price list logic for easier calls
    *                                     performance improvements
    *                                     add tag column from FND_LOOKUP_VALUES to determine whether to create price list extract record
    * 1.40    14-Jan-2013 Scott Spivey    added materialized view and temporary table to improve performance
    *                                     add call-for-price logic to price lists (already exist for modifiers)
    *                                     add call-for-price logic to list price function
    *                                     do not send call-for-price record for WhiteCapList (tag is not null)
    * 1.50   01-DEC-2013  Gopi Damuluri   TMS# 20131121-00230
    *                                     SpeedBuild Changes: CSP Information is now sent for all the Customers who have CSP setup.
    *                                     Added a new procedure - LOAD_SB_CSP_TEMP_TBL
    * 1.51   01/21/2014   Gopi Damuluri   TMS# 20140116-00071
    *                                     Added a new procedure - GET_CONTRACTLIST_PRICE to calculate CSP even if
    *                                     Call For Price flag is set at National Price.
    * 1.52   04/24/2014   Gopi Damuluri   TMS# 20140421-00080
    *                                     Added a new parameter P_END_DATE_ACTIVE to the function GET_CONTRACTLIST_PRICE
    * 1.60   03/11/2015   P.vamshidhar    TMS#20150309-00325 - PDH Catalog extracts for Ecom - performance issue
    *                                     Added new input parameter to 'Price_Lists' & 'extract_wrapper' procedures to refresh MV.
    * <additional records for modifications>
    *
    ***********************************************************************************************************************************************/

   /**************************************************************************
    *
    * PROCEDURE
    *  Price_Lists
    *
    * DESCRIPTION
    *  This procedure is called from a concurrent request to generate the price list/offer extract files
    *    for the Commerce interface
    *
    * PARAMETERS
    * ==========
    * NAME                 TYPE      DESCRIPTION
   .* -----------------    --------  ---------------------------------------------
    * ERRBUF               VARCAHR2  completion comment
    * RETCODE              VARCHAR2  completion code (0=success  1=warning  2=error)
    * P_EXTRACT_METHOD     VARCHAR2  type of extract : FULL or DELTA
    * p_LAST_EXTRACT_DATE  VARCHAR2  canonical date format of last extract
    * P_MV_REFRESH         VARCHAR2  'Y' to refresh MV and 'N' for not refresh.
    *
    * CALLED BY
    *  XXWC Price Lists Extract to ECOMM concurrent program
    *
    *************************************************************************/
   PROCEDURE price_lists (errbuf                   OUT VARCHAR2,
                          retcode                  OUT VARCHAR2,
                          p_extract_method      IN     VARCHAR2 DEFAULT 'DELTA',
                          p_last_extract_date   IN     VARCHAR2,
                          p_mv_refresh          IN     VARCHAR2 DEFAULT 'Y'); -- Added new parameter in TMS#20150309-00325 - 1.60 v

   /**************************************************************************
    *
    * PROCEDURE
    *  Extract_Wrapper
    *
    * DESCRIPTION
    *  This procedure is called from UC4 to submit the concurrent request to generate the price list/offer
    *    extract files for the Commerce interface
    *
    * PARAMETERS
    * ==========
    * NAME                 TYPE      DESCRIPTION
   .* -----------------    --------  ---------------------------------------------
    * ERRBUF               VARCAHR2  completion comment
    * RETCODE              VARCHAR2  completion code (0=success  1=warning  2=error)
    * P_EXTRACT_METHOD     VARCHAR2  type of extract : FULL or DELTA
    * p_LAST_EXTRACT_DATE  VARCHAR2  canonical date format of last extract
    * p_mv_refresh         VARCHAR2  'Y' to refresh MV and 'N' for not refresh.
    *
    * CALLED BY
    *  UC4 scheduling program
    *
    *************************************************************************/
   PROCEDURE extract_wrapper (errbuf                   OUT VARCHAR2,
                              retcode                  OUT VARCHAR2,
                              p_extract_method      IN     VARCHAR2,
                              p_last_extract_date   IN     VARCHAR2,
                              p_mv_refresh          IN     VARCHAR2 -- Added in TMS#20150309-00325 by Vamshi
                                                                   );

   PROCEDURE load_sb_csp_temp_tbl (p_extract_date IN DATE);   -- Version# 1.50

   -- Version# 1.51 > Start
   FUNCTION get_contractlist_price (p_item_num          IN VARCHAR2,
                                    p_price_list_name      VARCHAR2,
                                    p_end_date_active      VARCHAR2) -- Version# 1.52
      RETURN NUMBER;
-- Version# 1.51 < End

END xxwc_qp_extracts_pkg;
/