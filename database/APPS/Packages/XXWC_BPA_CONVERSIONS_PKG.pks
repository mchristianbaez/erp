create or replace package      APPS.XXWC_BPA_CONVERSIONS_PKG as

   /*************************************************************************************************   
   *    $Header XXWC_BPA_CONVERSIONS_PKG $                                                          *
   *   Module Name: XXWC_BPA_CONVERSIONS_PKG                                                        *
   *                                                                                                *
   *   PURPOSE:   This package is used for the data conversions for suppliers and supply chain data *
   *                                                                                                * 
   *   REVISIONS:                                                                                   *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   1.0        02/08/2014  Lee Spitzer                Initial Version 20130917-00676             *
   *                                                         Vendor Cost Management Improvements    *
   *   1.1        08/05/2014  Lee Spitzer                TMS Ticket 20140726-00007                  *
   *                                                         Wave 1 of Vendor Cost Improvements     *
   *   1.2        09/08/2014  Lee Spitzer                TMS Ticket 20140909-00030                  *
   *                                                        Fix Address display on PO document for  *
   *                                                         0PURCHASING site                       *
   *   1.3        01/19/2015  Lee Spitzer                TMS Ticket 20140909-00029                  *
   *                                                        Create 1 SEE NOTES contact              *
   /************************************************************************************************/
 
    
   
   /*************************************************************************************************   
   *   Procedure add_purchasing_supplier_site                                                       *
   *   Purpose : Procedure adds the PURCHASING Vendor Site Code to existing Suppliers               *
   *                                                                                                *
   *                                                                                                *
   *     procedure add_purchasing_supplier_site                                                     *
   *             (p_org_id IN NUMBER,                                                               *
   *              p_vendor_id IN NUMBER);                                                           *
   *                                                                                                *
   *   REVISIONS:                                                                                   *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   1.0        02/08/2014   Lee Spitzer              Initial Versions TMS 20130917-00676         *
   *                                                         Vendor Cost Management Improvements    *
   *                                                                                                *
   /************************************************************************************************/
   
   
    
    procedure add_purchasing_supplier_site
                (p_org_id IN NUMBER,
                 p_vendor_id IN NUMBER);
    
   
   /*************************************************************************************************   
   *   Procedure add_purchasing_supplier_site                                                       *
   *   Purpose : Procedure adds the Contact to new PURCHASING Site                                  *
   *                                                                                                *
   *                                                                                                *
   *     procedure add_purchasing_site_contacts                                                     *
   *             (p_org_id IN NUMBER,                                                               *
   *              p_vendor_id IN NUMBER);                                                           *
   *                                                                                                *
   *   REVISIONS:                                                                                   *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   1.0        02/08/2014   Lee Spitzer              Initial Versions TMS 20130917-00676         *
   *                                                         Vendor Cost Management Improvements    *
   *                                                                                                *
   /************************************************************************************************/
   
    
    procedure add_purchasing_site_contacts
                (p_org_id IN NUMBER,
                 p_vendor_id IN NUMBER);
                 
   
   /*************************************************************************************************   
   *   Procedure end_old_purchasing_sites                                                           *
   *   Purpose : This updates the inactive date on Purchaisng Sites that are not PURCHASING         *
   *                                                                                                *
   *                                                                                                *
   *     procedure end_old_purchasing_sites                                                         *
   *             (p_org_id IN NUMBER,                                                               *
   *              p_vendor_id IN NUMBER);                                                           *
   *                                                                                                *
   *   REVISIONS:                                                                                   *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   1.0        02/08/2014   Lee Spitzer              Initial Versions TMS 20130917-00676         *
   *                                                         Vendor Cost Management Improvements    *
   *                                                                                                *
   /************************************************************************************************/
   
   
   
    procedure end_old_purchasing_sites
                (p_org_id IN NUMBER,
                 p_vendor_id IN NUMBER);
                 
   
   /*************************************************************************************************   
   *   Procedure create_purchasing_site_sr                                                          *
   *   Purpose : Creates the Purchasing Sourcing Rules and Assignments                              *
   *                                                                                                *
   *                                                                                                *
   *     procedure add_purchasing_site_contacts                                                     *
   *             (p_vendor_id IN NUMBER);                                                           *
   *                                                                                                *
   *   REVISIONS:                                                                                   *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   1.0        02/08/2014   Lee Spitzer              Initial Versions TMS 20130917-00676         *
   *                                                         Vendor Cost Management Improvements    *
   *                                                                                                *
   /************************************************************************************************/
   
    procedure create_purchasing_site_sr
                (p_vendor_id in NUMBER);
                
   
   
   /*************************************************************************************************   
   *   Function get_contact_id_from_legacy                                                          *
   *   Purpose : returns the new contact id from the legacy vendor site id                          *
   *                                                                                                *
   *                                                                                                *
   *     function  get_contact_id_from_legacy                                                       *
   *             (p_vendor_id IN NUMBER,                                                            *
   *              p_vendor_site_id IN NUMBER);                                                      *
   *                RETURN NUMBER                                                                   *
   *                                                                                                *
   *   REVISIONS:                                                                                   *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   1.0        02/08/2014   Lee Spitzer              Initial Versions TMS 20130917-00676         *
   *                                                         Vendor Cost Management Improvements    *
   *                                                                                                *
   /************************************************************************************************/
    
    
    function  get_contact_id_from_legacy
                (p_vendor_id in NUMBER,
                 p_vendor_site_id IN NUMBER)
                 RETURN NUMBER;
    
   /*************************************************************************************************   
   *   Function get_legacy_id_from_contact_id                                                       *
   *   Purpose : returns the new old vendor site id from the new vendor contact id                  *
   *                                                                                                *
   *                                                                                                *
   *     function  get_legacy_id_from_contact_id                                                    *
   *             (p_vendor_id IN NUMBER,                                                            *
   *              p_vendor_site_id IN NUMBER,                                                       *
   *              p_vendor_contact_id IN NUMBER)                                                    *
   *                RETURN NUMBER             ;                                                     *
   *                                                                                                *
   *   REVISIONS:                                                                                   *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   1.0        02/08/2014   Lee Spitzer              Initial Versions TMS 20130917-00676         *
   *                                                         Vendor Cost Management Improvements    *
   *                                                                                                *
   /************************************************************************************************/
   
    
    function  get_legacy_id_from_contact_id
                (p_vendor_id in NUMBER,
                 p_vendor_site_id in NUMBER,
                 p_vendor_contact_id IN NUMBER)
                 RETURN NUMBER;
    
    
   /*************************************************************************************************   
   *   Function get_purchasing_ven_site_id                                                          *
   *   Purpose : returns the vendor site id based on the default global vendor site code            *
   *                                                                                                *
   *                                                                                                *
   *     function  get_purchasing_ven_site_id                                                       *
   *             (p_vendor_id IN NUMBER)                                                            *
   *                RETURN NUMBER             ;                                                     *
   *                                                                                                *
   *   REVISIONS:                                                                                   *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   1.0        02/08/2014   Lee Spitzer              Initial Versions TMS 20130917-00676         *
   *                                                         Vendor Cost Management Improvements    *
   *                                                                                                *
   /************************************************************************************************/
   
    
    function  get_purchasing_ven_site_id
                (p_vendor_id in NUMBER)
                 RETURN NUMBER;
    
    
    
   /*************************************************************************************************   
   *   Procedure update_vendor_minimum_contacts                                                     *
   *   Purpose : updates the XXWC PO Vendor Minimum table with with contact ids                     *
   *                                                                                                *
   *                                                                                                *
   *     Procedure  update_vendor_minimum_contacts                                                  *
   *             (p_vendor_id IN NUMBER);                                                            *
   *                                                                                                *
   *   REVISIONS:                                                                                   *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   1.0        02/08/2014   Lee Spitzer              Initial Versions TMS 20130917-00676         *
   *                                                         Vendor Cost Management Improvements    *
   *                                                                                                *
   /************************************************************************************************/
   
    
   
    procedure update_vendor_minimum_contacts
                (p_vendor_id in NUMBER);
   
   
   /*************************************************************************************************   
   *   procedure update_vendor_minimum_default                                                      *
   *   Purpose : updates the vendor minimum default flag on the XXWC PO Vendor Minimum table        *
   *                                                                                                *
   *                                                                                                *
   *     procedure update_vendor_minimum_default                                                    *
   *             (p_vendor_id IN NUMBER);                                                           *
   *                                                                                                *
   *   REVISIONS:                                                                                   *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   1.0        02/08/2014   Lee Spitzer              Initial Versions TMS 20130917-00676         *
   *                                                         Vendor Cost Management Improvements    *
   *                                                                                                *
   /************************************************************************************************/
    
    procedure update_vendor_minimum_default
                (p_vendor_id in NUMBER); 


   /*************************************************************************************************   
   *   procedure create_freight_burden_pur_site                                                     *
   *   Purpose - Creates a freight burden entry for the global vendor site code                     *
   *                                                                                                *
   *                                                                                                *
   *    procedure  update_vendor_minimum_default                                                    *
   *             (p_vendor_id IN NUMBER)                                                            *
   *                RETURN NUMBER             ;                                                     *
   *                                                                                                *
   *   REVISIONS:                                                                                   *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   1.0        02/08/2014   Lee Spitzer              Initial Versions TMS 20130917-00676         *
   *                                                         Vendor Cost Management Improvements    *
   *                                                                                                *
   /************************************************************************************************/

    
    procedure create_freight_burden_pur_site
                (p_vendor_id in NUMBER); 
                

   /*************************************************************************************************   
   *   procedure launch_ccr                                                                         *
   *   Purpose - Main Concurrent program for XXWC BPA Price Zone Import                             *
   *                                                                                                *
   *   procedure launch_ccr (errbuf   OUT VARCHAR2                                                  *
   *                      ,retcode  OUT NUMBER                                                      *
   *                      ,p_org_id  IN NUMBER                                                      *
   *                      ,p_program IN VARCHAR2                                                    *
   *                      ,P_VENDOR_ID IN NUMBER                                                    *
   *                      ,p_vendor_site_code IN VARCHAR2                                           *
   *                      ,p_vendor_param_list_id IN NUMBER); --Added Version 1.1 20140726-00007    *
   *                                                                                                *
   *                                                                                                *
   *                                                                                                *
   *   REVISIONS:                                                                                   *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   1.0        02/08/2014   Lee Spitzer              Initial Versions TMS 20130917-00676         *
   *                                                         Vendor Cost Management Improvements    *
   *   1.1        08/05/2014   Lee Spitzer              TMS Ticket 20140726-00007                   *
   *                                                       Upgrade program to use Supplier list     *
   *   1.3        01/19/2015  Lee Spitzer                TMS Ticket 20140909-00029                  *
   *                                                        Create 1 SEE NOTES contact              *
   *                                                                                                *
   /************************************************************************************************/
    
    procedure launch_ccr (errbuf   OUT VARCHAR2
                         ,retcode  OUT NUMBER
                         ,p_org_id  IN NUMBER
                         ,p_program IN VARCHAR2
                         ,P_VENDOR_ID IN NUMBER
                         ,p_vendor_site_code IN VARCHAR2
                         ,p_vendor_param_list_id IN NUMBER); --Added Version 1.1 20140726-00007
                         
  
   /*************************************************************************************************   
   *   Function get_default_vendor_site_id                                                          *
   *   Purpose : Function used in the conversion process to determine which legacy vendor site      *
   *       had the most sourcing assignments                                                        *
   *                                                                                                *
   *                                                                                                *
   *     function  get_default_vendor_site_id                                                       *
   *             (p_vendor_id IN NUMBER,                                                            *
   *              p_organization_id IN NUMBER) --Can be NULL                                        *       
   *                RETURN NUMBER             ;                                                     *
   *                                                                                                *
   *   REVISIONS:                                                                                   *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   1.0        02/08/2014   Lee Spitzer              Initial Versions TMS 20130917-00676         *
   *                                                         Vendor Cost Management Improvements    *
   *   1.1        08/05/2014   Lee Spitzer              TMS Ticket 20140726-00007                   *
   *                                                       Filter only for stock items              *
   *                                                                                                *
   *                                                                                                *
   /************************************************************************************************/
   
    function get_default_vendor_site_id
                        (p_vendor_id IN NUMBER
                        ,p_organization_id IN NUMBER)
          RETURN NUMBER;
                        
  
   /*************************************************************************************************   
   *   Procedure update_vendor_address                                                              *
   *   Purpose : Update Address Line 2 to Address Line 1 from Vendor Conversion; Address Line 1     *
   *       is a bogus address to help convert vendor site correctly                                 *
   *                                                                                                *
   *                                                                                                *
   *     procudure update_vendor_address                                                            *
   *             (p_org_id    IN NUMBER                                                             *
   *             ,p_vendor_id IN NUMBER                                                             *
   *             ,p_vendor_site_code IN VARCHAR2)                                                   *
   *                                                                                                *
   *   REVISIONS:                                                                                   *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   1.2        09/08/2014  Lee Spitzer                TMS Ticket 20140909-00030                  *
   *                                                        Fix Address display on PO document for  *
   *                                                         0PURCHASING site                       *
                                                                                                   *
   *                                                                                                *
   /************************************************************************************************/
    
    PROCEDURE update_vendor_address
                (p_org_id    IN NUMBER
                ,p_vendor_id IN NUMBER
                ,p_vendor_site_code in VARCHAR2);
                         
   
   /*************************************************************************************************   
   *   Procedure add_1_see_notes_contact                                                            *
   *   Purpose : This adds 1 SEE NOTES Contact to the 0PURCHASING Site                              *
   *                                                                                                *
   *                                                                                                *
   *     procedure end_old_purchasing_sites                                                         *
   *             (p_org_id IN NUMBER,                                                               *
   *              p_vendor_id IN NUMBER);                                                           *
   *                                                                                                *
   *   REVISIONS:                                                                                   *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   1.3        01/19/2015  Lee Spitzer                TMS Ticket 20140909-00029                  *
   *                                                        Create 1 SEE NOTES contact              *
   *                                                                                                *
   /************************************************************************************************/
   
  
    PROCEDURE add_1_see_notes_contact
                      (p_org_id IN NUMBER,
                       p_vendor_id IN NUMBER);

   
   /*************************************************************************************************   
   *   Procedure add_1_see_notes_site                                                               *
   *   Purpose : This adds 1 SEE NOTES Vendor Sitehe 0PURCHASING Site                               *
   *                                                                                                *
   *                                                                                                *
   *     procedure end_old_purchasing_sites                                                         *
   *             (p_org_id IN NUMBER,                                                               *
   *              p_vendor_id IN NUMBER);                                                           *
   *                                                                                                *
   *   REVISIONS:                                                                                   *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   1.3        01/19/2015  Lee Spitzer                TMS Ticket 20140909-00029                  *
   *                                                        Create 1 SEE NOTES contact              *
   *                                                                                                *
   /************************************************************************************************/
   
  
    PROCEDURE add_1_see_notes_supplier_site
                      (p_org_id IN NUMBER,
                       p_vendor_id IN NUMBER);
          
end; 