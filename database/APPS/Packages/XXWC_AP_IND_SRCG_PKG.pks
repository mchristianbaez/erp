--
-- XXWC_AP_IND_SRCG_PKG  (Package) 
--
CREATE OR REPLACE PACKAGE apps.xxwc_ap_ind_srcg_pkg
AS
   /********************************************************************************

   FILE NAME: xxwc_ap_ind_srcg_pkg.pks

   PROGRAM TYPE: PL/SQL Package Specification

   PURPOSE: Extract Oracle Account Payables Sourcing data for the given input parameters and
            generate a Pipe delimited file. The file will be FTPed to UC4 and
            finally to Sourcing Data Mart.

   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.0     10/20/2011    Gopi Damuluri    Initial creation of the procedure
   ********************************************************************************/

   FUNCTION inv_amt_remaining (p_invoice_id IN NUMBER)
      RETURN NUMBER;

   FUNCTION inv_min_due_date (p_invoice_id IN NUMBER)
      RETURN VARCHAR2;

   FUNCTION inv_payment_date (p_invoice_id IN NUMBER)
      RETURN VARCHAR2;

   FUNCTION disc_mrc_frt_amnt (p_invoice_id IN NUMBER)
      RETURN VARCHAR2;

   PROCEDURE create_payables_file (errbuf             OUT VARCHAR2
                                  ,retcode            OUT NUMBER
                                  ,p_period_name   IN     VARCHAR2);

   PROCEDURE create_payment_terms_file (errbuf    OUT VARCHAR2
                                       ,retcode   OUT NUMBER);

   PROCEDURE create_pay_to_vendor_file (errbuf    OUT VARCHAR2
                                       ,retcode   OUT NUMBER);

   PROCEDURE create_khalix_file (errbuf OUT VARCHAR2, retcode OUT NUMBER);

   PROCEDURE uc4_call (p_errbuf                   OUT VARCHAR2
                      ,p_retcode                  OUT NUMBER
                      ,p_conc_prg_name         IN     VARCHAR2
                      ,p_conc_prg_arg1         IN     VARCHAR2
                      ,p_user_name             IN     VARCHAR2
                      ,p_responsibility_name   IN     VARCHAR2
                      ,p_org_name              IN     VARCHAR2);

   FUNCTION get_curr_period
      RETURN VARCHAR2;
--  PROCEDURE uc4_payables_info(errbuf OUT VARCHAR2, retcode OUT NUMBER)

END xxwc_ap_ind_srcg_pkg;
/

