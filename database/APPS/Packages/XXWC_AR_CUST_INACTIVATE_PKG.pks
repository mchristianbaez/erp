CREATE OR REPLACE PACKAGE APPS.XXWC_AR_CUST_INACTIVATE_PKG
AS
  /****************************************************************************************************
    *
    * FUNCTION | PROCEDURE | CURSOR
    *  XXWC_AR_CUST_INACTIVATE_PKG
    *
    * DESCRIPTION
    *  FIN / Mass update of accounts that haven't had activity within the last XX month
    *
    * PARAMETERS
    * ==========
    * NAME              TYPE     DESCRIPTION
   .* ----------------- -------- ------------------------------------------------------------------------
    * P_MODE           <IN>       Report Only shows only report and commit will inactivate sites
    * P_CLASS          <IN>       Class name
    * P_MONTH          <IN>       No of months
    * P_AS_OF_DATE     <IN>       Report run date
    * P_EMAIL          <IN>       Email report output needs to be send
    *
    *
    * CALLED BY
    *   Which program, if any, calls this one
    *
    * HISTORY
    * =======
    *
    * VERSION DATE        AUTHOR(S)       DESCRIPTION
    * ------- ----------- --------------- ----------------------------------------------------------------
    * 1.0    05/21/2015   Maharajan S     Initial Creation - TMS#20130909-00768 Inactivate accounts that 
    *                                     haven't had activity within the last XX months
    ******************************************************************************************************/
   PROCEDURE XXWC_CUST_INACTIVATE(errbuf        OUT VARCHAR2,
                                  retcode       OUT NUMBER,
                                  p_resp        IN  VARCHAR2,
                                  P_resp_status IN  VARCHAR2,
                                  p_mode        IN  VARCHAR2,
                                  p_month       IN  NUMBER,
                                  p_date        IN  DATE,
                                  p_email       IN  VARCHAR2);
END;
/
