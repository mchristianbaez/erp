CREATE OR REPLACE PACKAGE APPS.xxwc_om_csp_interface_pkg
AS

/********************************************************************************
FILE NAME: APPS.XXWC_OM_CSP_INTERFACE_PKG.pkg

PROGRAM TYPE: PL/SQL Package

PURPOSE: This contains contains the need procedures for the Contract Pricing 
         Maintenance Extension. This includes Customer Pricing Attribute for 
         Item Category, ability to import modifiers from the CSP Maintenance 
         Form to Advanced Pricing, aonvert a Quote to a CSP Agreement, and 
         process  a mass CSP upload into the custom form.

HISTORY
===============================================================================
VERSION DATE          AUTHOR(S)              DESCRIPTION
------- -----------   --------------- -----------------------------------------
1.0     01/09/2013    Lucidity Consulting    Initial Release
1.1     12/17/2015    Pattabhi Avula         TMS# 20151030-00213 -- Parameter Name changed
5.8     5/19/2017     Niraj K ranjan         TMS#20150625-00057   CSP Enhance bundle - Item #4  CSP Approval Workflow
5.9     7/4/2017      Niraj K ranjan         TMS#20170703-00038  CSP data fix script to clean up old records and Revert Logic update
********************************************************************************/


  G_RETURN_SUCCESS      CONSTANT    VARCHAR2(1) := 'S' ;
  G_RETURN_ERROR        CONSTANT    VARCHAR2(1) := 'E' ;
  G_RETURN_UNEXP_ERROR  CONSTANT    VARCHAR2(1) := 'U' ;
  
  -- 10/09/2013 CG: TMS 20131009-00322: SQL*Loader Path Profile definition
  G_FILE_DIR_PATH       VARCHAR2(400) := Fnd_Profile.Value('XXWC_CSP_LOADER_PATH');
  
  g_debug_msg           VARCHAR2(4000); --ver 5.8
  TYPE xxwc_csp_agreement_rec IS RECORD(agreement_number NUMBER,revision_number NUMBER); --ver 5.8
  TYPE xxwc_agreement_number_tab IS TABLE OF xxwc_csp_agreement_rec INDEX BY BINARY_INTEGER; --ver 5.8
  TYPE l_cat_class_tbl IS RECORD(
         CATEGORY_ID mtl_categories_b.category_id%TYPE,
         ITEM_ID mtl_system_items_b.inventory_item_id%TYPE,
         CAT_CLASS VARCHAR2(100),
         SKU mtl_system_items_b.segment1%TYPE,
         SKU_DESCRIPTION mtl_system_items_b.description%TYPE,
         UOM mtl_system_items_b.primary_unit_of_measure%TYPE
       );
  TYPE l_cat_class_rc IS REF CURSOR RETURN l_cat_class_tbl;
  PROCEDURE create_header (i_agreement_id IN NUMBER);
  
  PROCEDURE create_lines (i_agreement_id IN NUMBER);
  
  PROCEDURE create_exclusions (i_agreement_id IN NUMBER);
   
  PROCEDURE update_lines (i_agreement_id IN NUMBER);
  
  PROCEDURE import_modifiers (errbuf              OUT VARCHAR2
                             , retcode             OUT VARCHAR2
                            -- , i_agreement_id   IN     NUMBER);  -- Version# 1.1
							 , p_agreement_id   IN     NUMBER);  -- Version# 1.1
                             
  PROCEDURE csp_quote (errbuf          OUT VARCHAR2, 
                        retcode         OUT VARCHAR2,
                        i_quotenumber   IN NUMBER,
                        i_price_type    IN VARCHAR2,
                        i_incomp_group  IN varchar2,
                        i_vq_number     IN VARCHAR2,
                        i_vendor_number IN NUMBER,
                        i_start_date IN VARCHAR2);
                   
                          
  FUNCTION get_item_category (i_inv_item_id IN NUMBER)
  RETURN CHAR;
      
  PROCEDURE csp_conversion (errbuf OUT VARCHAR2, 
                              retcode OUT VARCHAR2);
                              
  PROCEDURE csp_headers ;
  
  PROCEDURE csp_lines ; 
  
  PROCEDURE csp_exclusions ; 
                      
  PROCEDURE csp_mass_conversion (errbuf   OUT VARCHAR2, 
                                retcode  OUT VARCHAR2);
                                
  PROCEDURE csp_cust_quote (errbuf          OUT VARCHAR2, 
    retcode         OUT VARCHAR2,
    i_quotenumber   IN NUMBER,
    i_price_type    IN VARCHAR2,
    i_incomp_group  IN varchar2,
    i_vq_number     IN VARCHAR2,
    i_vendor_number IN NUMBER,
    i_start_date IN VARCHAR2);
    
  PROCEDURE csp_end_date (errbuf OUT VARCHAR2, 
                              retcode OUT VARCHAR2);
  PROCEDURE csp_delta (errbuf OUT VARCHAR2, 
                          retcode OUT VARCHAR2) ; 
                          
                          
  PROCEDURE update_delta_lines ;

  PROCEDURE create_delta_lines ; 

  PROCEDURE mass_update (errbuf              OUT VARCHAR2
                             , retcode             OUT VARCHAR2);
                             
                               -- 04/09/13 CG: TMS# : Procedure to remove inactive CSPs
  PROCEDURE cleanup_csp_quotes (errbuf OUT VARCHAR2, 
                              retcode OUT VARCHAR2,
                              i_agreement_id IN NUMBER);

-- 08/28/2013 CG: TMS 20130828-00775: New procedure to be used from maintenance from for self service submission
  PROCEDURE xxwc_csp_maintenance (RETCODE         OUT NUMBER
                                   , ERRMSG         OUT VARCHAR2
                                   , P_FILE_NAME    IN VARCHAR2
                                   , P_FILE_ID      IN NUMBER);
                                   
PROCEDURE csp_maintenance_process
(errbuf OUT VARCHAR2, 
retcode OUT VARCHAR2);

PROCEDURE csp_maintenance_exclusions
(errbuf OUT VARCHAR2, 
retcode OUT VARCHAR2);

   /*************************************************************************
    *   PROCEDURE Name: insert_csp_agreement
    *
    *   PURPOSE:   insert into table XXWC_OM_CSP_NOTIFICATIONS_TBL
	
	HISTORY
    ===============================================================================
    VERSION DATE          AUTHOR(S)              DESCRIPTION
    ------- -----------   --------------- -----------------------------------------
    5.8     5/19/2017     Niraj K ranjan         TMS#20150625-00057   CSP Enhance bundle - Item #4  CSP Approval Workflow
   *****************************************************************************/
   PROCEDURE insert_csp_agreement(p_agreement_rec   IN  XXWC_OM_CSP_NOTIFICATIONS_TBL%ROWTYPE
								 ,p_retmsg          OUT VARCHAR2);
   /*************************************************************************
    *   PROCEDURE Name: submit_csp_agreement
    *
    *   PURPOSE: Do the same functionality whatever submit button does of CSP Maintenance screen
	
	HISTORY
    ===============================================================================
    VERSION DATE          AUTHOR(S)              DESCRIPTION
    ------- -----------   --------------- -----------------------------------------
    5.8     5/19/2017     Niraj K ranjan         TMS#20150625-00057   CSP Enhance bundle - Item #4  CSP Approval Workflow
   *****************************************************************************/
   PROCEDURE submit_csp_agreement(p_agreement_id   IN  NUMBER
								 ,p_retmsg         OUT VARCHAR2);
   /*************************************************************************
    *   PROCEDURE Name: get_salesrep_detail
    *
    *   PURPOSE:   get sales rep name and email id
	
	HISTORY
    ===============================================================================
    VERSION DATE          AUTHOR(S)              DESCRIPTION
    ------- -----------   --------------- -----------------------------------------
    5.8     5/19/2017     Niraj K ranjan         TMS#20150625-00057   CSP Enhance bundle - Item #4  CSP Approval Workflow
   *****************************************************************************/
   PROCEDURE get_salesrep_detail(p_agreement_num   IN  NUMBER
                                ,p_salesrep_name   OUT VARCHAR2
                                ,p_salesrep_email  OUT VARCHAR2
								,p_sr_designation  OUT VARCHAR2
								,p_resource_name   OUT VARCHAR2
								,p_retmsg          OUT VARCHAR2);

   /*************************************************************************
    *   PROCEDURE Name: get_dm_detail
    *
    *   PURPOSE:   get district manager name and email id
	
	HISTORY
    ===============================================================================
    VERSION DATE          AUTHOR(S)              DESCRIPTION
    ------- -----------   --------------- -----------------------------------------
    5.8     5/19/2017     Niraj K ranjan         TMS#20150625-00057   CSP Enhance bundle - Item #4  CSP Approval Workflow
   *****************************************************************************/
   PROCEDURE get_dm_detail(p_agreement_num   IN NUMBER
                          ,p_dm_name        OUT VARCHAR2
                          ,p_dm_email       OUT VARCHAR2
						  ,p_dm_ntid        OUT VARCHAR2
						  ,p_dm_region      OUT VARCHAR2
						  ,p_dm_district    OUT VARCHAR2
						  ,p_retmsg         OUT VARCHAR2);
   /*************************************************************************
    *   FUNCTION Name: get_email_subject
    *
    *   PURPOSE:   get email subject line
	
	HISTORY
    ===============================================================================
    VERSION DATE          AUTHOR(S)              DESCRIPTION
    ------- -----------   --------------- -----------------------------------------
    5.8     5/19/2017     Niraj K ranjan         TMS#20150625-00057   CSP Enhance bundle - Item #4  CSP Approval Workflow
   *****************************************************************************/
   FUNCTION get_email_subject(p_agreement_number IN NUMBER,
                              p_email_context    IN VARCHAR2) --p_email_context = SUBMITTED/APPROVED/REJECTED
   RETURN VARCHAR2;
   /*************************************************************************
    *   PROCEDURE Name: get_csp_lines_email_body
    *
    *   PURPOSE:   get csp lines detail in html tabular format to append in email body
	
	HISTORY
    ===============================================================================
    VERSION DATE          AUTHOR(S)              DESCRIPTION
    ------- -----------   --------------- -----------------------------------------
    5.8     7/06/2017     Niraj K ranjan         TMS#20150625-00057   CSP Enhance bundle - Item #4  CSP Approval Workflow
   *****************************************************************************/
   PROCEDURE get_csp_lines_email_body(p_agreement_num     IN  NUMBER,
                                      p_dm_region_name    IN  VARCHAR2,
									  p_csp_line_msg_body OUT VARCHAR2,
									  p_retmsg            OUT VARCHAR2
									 );
   /*************************************************************************
    *   FUNCTION Name: get_email_body
    *
    *   PURPOSE:   get email body
	
	HISTORY
    ===============================================================================
    VERSION DATE          AUTHOR(S)              DESCRIPTION
    ------- -----------   --------------- -----------------------------------------
    5.8     5/19/2017     Niraj K ranjan         TMS#20150625-00057   CSP Enhance bundle - Item #4  CSP Approval Workflow
   *****************************************************************************/
   FUNCTION get_csp_header_email_body(p_agreement_num    IN NUMBER,
                                      p_revision_number  IN NUMBER) 
   RETURN VARCHAR2;
   /*************************************************************************
    *   PROCEDURE Name: send_html_email
    *
    *   PURPOSE:   send html email to multiple recepient.
	
	HISTORY
    ===============================================================================
    VERSION DATE          AUTHOR(S)              DESCRIPTION
    ------- -----------   --------------- -----------------------------------------
    5.8     5/19/2017     Niraj K ranjan         TMS#20150625-00057   CSP Enhance bundle - Item #4  CSP Approval Workflow
	5.9     7/4/2017      Niraj K ranjan         TMS#20170703-00038  CSP data fix script to clean up old records and Revert Logic update
   *****************************************************************************/
   PROCEDURE send_html_email (p_to              IN   VARCHAR2,
	                          p_cc              IN   VARCHAR2 DEFAULT NULL,
                              p_bcc             IN   VARCHAR2 DEFAULT NULL,
                              p_from            IN   VARCHAR2,
                              p_subject         IN   VARCHAR2,
                              p_text            IN   VARCHAR2 DEFAULT NULL,
                              p_html            IN   VARCHAR2 DEFAULT NULL,
                              p_smtp_hostname   IN   VARCHAR2,
                              p_smtp_portnum    IN   VARCHAR2,
							  p_retmsg          OUT  VARCHAR2 --Ver 5.9
							 );
   /*************************************************************************
    *   PROCEDURE Name: send_fyi_notification
    *
    *   PURPOSE:   send FYI notifications when csp is submitted / approved / rejected.
	
	HISTORY
    ===============================================================================
    VERSION DATE          AUTHOR(S)              DESCRIPTION
    ------- -----------   --------------- -----------------------------------------
    5.8     5/19/2017     Niraj K ranjan         TMS#20150625-00057   CSP Enhance bundle - Item #4  CSP Approval Workflow
   *****************************************************************************/
   PROCEDURE send_fyi_notification(p_agreement_number IN NUMBER,
                                   p_revision_number  IN NUMBER,
                                   p_email_context    IN VARCHAR2,
                                   p_retmsg           OUT VARCHAR2);
   /*************************************************************************
    *   PROCEDURE Name: send_csp_aprv_rej_ntf
    *
    *   PURPOSE:   send notifications when csp is approved or rejected from apex page.
	
	HISTORY
    ===============================================================================
    VERSION DATE          AUTHOR(S)              DESCRIPTION
    ------- -----------   --------------- -----------------------------------------
    5.8     5/19/2017     Niraj K ranjan         TMS#20150625-00057   CSP Enhance bundle - Item #4  CSP Approval Workflow
   *****************************************************************************/
   PROCEDURE send_csp_aprv_rej_ntf( p_agreement_number_list IN xxwc_agreement_number_tab
                                   ,p_ntf_type              IN VARCHAR2
								   ,p_retmsg                OUT VARCHAR2);
   /*************************************************************************
      PROCEDURE Name: get_cat_class_items

      PURPOSE:   This procedure will be called from CSP form to display cat class items detail in pop up screen.

      REVISIONS:
      Ver        Date          Author             Description
      ---------  ------------  ------------       ------------------
      7.0        02/22/2018  Niraj K Ranjan          TMS#20170201-00276   CSP Form Should be modified to accommodate Matrix modifiers--Matrix Maintenance
   ****************************************************************************/
    PROCEDURE get_cat_class_items(p_cat_items      IN OUT l_cat_class_rc,
	                              p_category_id        IN NUMBER,
                                  p_organization_id    IN NUMBER,
								  P_SKU                IN VARCHAR2,
								  p_SKU_DESCRIPTION    IN VARCHAR2,
								  P_UOM                IN VARCHAR2
                                 );
    /*************************************************************************
      PROCEDURE Name: load_csp_header

      PURPOSE:   This procedure is used to create CSP header and modifier record in bulk

      REVISIONS:
      Ver        Date          Author             Description
      ---------  ------------  ------------       ------------------
      8.0        05/31/2018    Niraj K Ranjan     TMS#20180523-00003   AH Conversion CSP Headers
   ****************************************************************************/
    PROCEDURE load_csp_header (RETCODE         OUT NUMBER
                            , ERRMSG         OUT VARCHAR2);
							
   /*************************************************************************
      PROCEDURE Name: xxwc_csp_header_mass_upload

      PURPOSE:   This procedure is used to load CSP header record in bulk through file upload and create modifier

      REVISIONS:
      Ver        Date          Author             Description
      ---------  ------------  ------------       ------------------
      8.0        05/31/2018    Niraj K Ranjan     TMS#20180523-00003   AH Conversion CSP Headers
   ****************************************************************************/
   PROCEDURE xxwc_csp_header_mass_upload (RETCODE         OUT NUMBER
                                   , ERRMSG         OUT VARCHAR2
                                   , P_FILE_NAME    IN VARCHAR2
                                   , P_FILE_ID      IN NUMBER);



END;
/


