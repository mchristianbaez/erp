--
-- XXWC_AP_INV_ADI_PKG  (Package) 
--
CREATE OR REPLACE PACKAGE apps.xxwc_ap_inv_adi_pkg
IS
   /********************************************************************************
   FILE NAME: XXWC_AP_INV_ADI_PKG.pkg

   PROGRAM TYPE: PL/SQL Package

   PURPOSE: Package to load AP Invoice Interface Tables through ADI

   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.0     07/25/2012    Gopi Damuluri    Initial creation of the procedure
   ********************************************************************************/

   PROCEDURE load_interface_tbl (p_invoice_num                  IN VARCHAR2
                                ,p_invoice_type_lookup_code     IN VARCHAR2
                                ,p_invoice_date                 IN DATE
                                ,p_po_number                    IN VARCHAR2
                                ,p_vendor_num                   IN VARCHAR2
                                ,p_vendor_name                  IN VARCHAR2
                                ,p_vendor_site_code             IN VARCHAR2
                                ,p_invoice_amount               IN NUMBER
                                ,p_invoice_currency_code        IN VARCHAR2
                                ,p_exchange_rate                IN NUMBER
                                ,p_exchange_rate_type           IN VARCHAR2
                                ,p_exchange_date                IN DATE
                                ,p_terms_name                   IN VARCHAR2
                                ,p_description                  IN VARCHAR2
                                ,p_awt_group_name               IN VARCHAR2
                                ,p_attribute1                   IN VARCHAR2
                                ,p_attribute2                   IN VARCHAR2
                                ,p_attribute3                   IN VARCHAR2
                                ,p_attribute4                   IN VARCHAR2
                                ,p_attribute5                   IN VARCHAR2
                                ,p_attribute6                   IN VARCHAR2
                                ,p_attribute7                   IN VARCHAR2
                                ,p_attribute8                   IN VARCHAR2
                                ,p_attribute9                   IN VARCHAR2
                                ,p_attribute10                  IN VARCHAR2
                                ,p_attribute11                  IN VARCHAR2
                                ,p_attribute12                  IN VARCHAR2
                                ,p_attribute13                  IN VARCHAR2
                                ,p_attribute14                  IN VARCHAR2
                                ,p_attribute15                  IN VARCHAR2
                                ,p_source                       IN VARCHAR2
                                ,p_group_id                     IN VARCHAR2
                                ,p_payment_method_lookup_code   IN VARCHAR2
                                ,p_pay_group_lookup_code        IN VARCHAR2
                                ,p_gl_date                      IN DATE
                                ,p_line_type_lookup_code        IN VARCHAR2
                                ,p_amount                       IN NUMBER
                                ,p_accounting_date              IN DATE
                                ,p_line_description             IN VARCHAR2
                                ,p_distribution_set_name        IN VARCHAR2
                                ,p_dist_code_concatenated       IN VARCHAR2
                                ,p_line_attribute1              IN VARCHAR2
                                ,p_line_attribute2              IN VARCHAR2
                                ,p_line_attribute3              IN VARCHAR2
                                ,p_line_attribute4              IN VARCHAR2
                                ,p_line_attribute5              IN VARCHAR2
                                ,p_line_attribute6              IN VARCHAR2
                                ,p_line_attribute7              IN VARCHAR2
                                ,p_line_attribute8              IN VARCHAR2
                                ,p_line_attribute9              IN VARCHAR2
                                ,p_line_attribute10             IN VARCHAR2
                                ,p_line_attribute11             IN VARCHAR2
                                ,p_line_attribute12             IN VARCHAR2
                                ,p_line_attribute13             IN VARCHAR2
                                ,p_line_attribute14             IN VARCHAR2
                                ,p_line_attribute15             IN VARCHAR2
                                ,p_asset_book_type_code         IN VARCHAR2
                                ,p_tax_classification_code      IN VARCHAR2);
END xxwc_ap_inv_adi_pkg;
/

