CREATE OR REPLACE PACKAGE xxwc_inv_update_dff_pkg AS
  /*************************************************************************
  *   $Header xxwc_inv_update_dff_pkg $
  *   Module Name: xxwc_inv_update_dff_pkg
  *
  *   PURPOSE:   Package used in Update Item PO and EHS DFF ADI
  *
  *   REVISIONS:
  *   Ver        Date        Author                     Description
  *   ---------  ----------  ---------------         -------------------------
  *   1.0        04/01/2012  Shankar Hariharan             Initial Version
  *   2.0        05/13/2013  Lee Spitzer                   Updates to for HI Freight Burden
  *   3.0        01/20/2015  Gajendra M                    Added UN Number,Hazard Class,Cas Number and Hazmat Materail Flag for TMS#20141229-00015
  *   4.0        04/06/2018  Naveen Kalidindi        TMS#20171011-00160 - Update EHS DFF attributes as part of Prop65 Compliance changes  
  *   5.0        23/08/2018  Naveen Kalidindi        TMS#20180824-00004 - Prop65 OAF Changes  
  * ***************************************************************************/

  -- Version 5.0
  PROCEDURE update_prop65_attributes(p_item_number         IN VARCHAR2
                                    ,p_inventory_item_id   IN NUMBER
                                    ,p_ca_prop65           IN VARCHAR2
                                    ,p_prop65_cancer       IN VARCHAR2 DEFAULT NULL
                                    ,p_prop65_reproductive IN VARCHAR2 DEFAULT NULL
                                    ,p_prop65_short_form   IN VARCHAR2 DEFAULT NULL
                                    ,o_ret_flag            OUT VARCHAR2
                                    ,o_ret_msg             OUT VARCHAR2);
									
  PROCEDURE update_ehs_attributes(p_prop65_reproductive_msg IN VARCHAR2 DEFAULT NULL --Added Prop65 Reproductive Msg, version 4.0
                                 ,p_prop65_cancer_msg       IN VARCHAR2 DEFAULT NULL --Added Prop65 Cancer Msg, version 4.0
                                 ,p_prop65_short_form       IN VARCHAR2 DEFAULT NULL --Added Prop65 Short Form, version 4.0
                                 ,p_hazmat_material_flag    IN VARCHAR2 DEFAULT NULL --Added Hazmat Materail Flag for TMS#20141229-00015
                                 ,p_cas_number              IN VARCHAR2 DEFAULT NULL --Added Cas Number for TMS#20141229-00015
                                 ,p_hazard_class            IN VARCHAR2 DEFAULT NULL --Added Hazard Class for TMS#20141229-00015
                                 ,p_un_number               IN VARCHAR2 DEFAULT NULL --Added UN Number for TMS#20141229-00015
                                 ,p_voc_sub_category        IN VARCHAR2 DEFAULT NULL
                                 ,p_voc_category            IN VARCHAR2 DEFAULT NULL
                                 ,p_voc_gl                  IN VARCHAR2 DEFAULT NULL
                                 ,p_ca_prop65               IN VARCHAR2 DEFAULT NULL
                                 ,p_pesticide_flag_state    IN VARCHAR2 DEFAULT NULL
                                 ,p_pesticide_flag          IN VARCHAR2 DEFAULT NULL
                                 ,p_package_group           IN VARCHAR2 DEFAULT NULL
                                 ,p_ormd_flag               IN VARCHAR2 DEFAULT NULL
                                 ,p_msds_number             IN VARCHAR2 DEFAULT NULL
                                 ,p_container_type          IN VARCHAR2 DEFAULT NULL
                                 ,p_hazmat_desc             IN VARCHAR2 DEFAULT NULL
                                 ,p_item_number             IN VARCHAR2
                                 ,p_org_code                IN VARCHAR2);
  /*
  PROCEDURE update_po_attributes (i_organization_id in number,
                                  i_inventory_item_id in number,
                                  i_reserve_stock in number,
                                  i_import_duty in number);
                                  */
  PROCEDURE update_po_attributes(p_org_code      IN VARCHAR2
                                ,p_item_number   IN VARCHAR2
                                ,p_reserve_stock IN NUMBER
                                ,p_import_duty   IN NUMBER
                                ,p_coo           IN VARCHAR2
                                ,p_taxware_code  IN VARCHAR2);

  PROCEDURE update_hi_ship_weight(p_org_code         IN VARCHAR2
                                 ,p_item_number      IN VARCHAR2
                                 ,p_item_description IN VARCHAR2
                                 ,p_unit_weight      IN NUMBER
                                 ,p_ship_weight      IN NUMBER);
END xxwc_inv_update_dff_pkg;
/
