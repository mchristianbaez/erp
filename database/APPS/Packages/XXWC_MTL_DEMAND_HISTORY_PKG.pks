CREATE OR REPLACE PACKAGE APPS.XXWC_MTL_DEMAND_HISTORY_PKG
/*************************************************************************
  $Header XXWC_MTL_DEMAND_HISTORY_PKG.pks $
  Module Name: XXWC_MTL_DEMAND_HISTORY_PKG

  PURPOSE: To update mtl_demand_histories for specific transactions defined

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    ----------------
  1.0        30-Apr-2015  Manjula Chellappan    Initial Version TMS # 20150302-00057

**************************************************************************/
AS
   /*************************************************************************

       1.  Load_Ou

    **************************************************************************/
   PROCEDURE Load_Ou (p_retcode OUT VARCHAR2);


   /*************************************************************************

       2.  Load_Inv_Orgs

    **************************************************************************/
   PROCEDURE Load_Inv_Orgs (p_retcode              OUT VARCHAR2,
                            p_organization_id   IN     NUMBER,
                            p_last_run_date     IN     VARCHAR2);

   /*************************************************************************

       3.  Load_Periods

    **************************************************************************/
   PROCEDURE Load_Periods (p_retcode              OUT VARCHAR2);

   /*************************************************************************

       4.  Load_Transactions

       **************************************************************************/
   PROCEDURE Load_Transactions (p_retcode              OUT VARCHAR2,
                                p_organization_id   IN     NUMBER);

   /*************************************************************************

       5.  Gen_Demand_Update

       **************************************************************************/
   PROCEDURE Gen_Demand_Update (p_retcode OUT VARCHAR2);

   /*************************************************************************

       6.  Demand_Update

       **************************************************************************/
   PROCEDURE Demand_Update (p_retcode              OUT VARCHAR2,
                            p_organization_id   IN     NUMBER);

   /*************************************************************************

       7.  Demand_Update_Main

      **************************************************************************/
   PROCEDURE Demand_Update_Main (errbuf                 OUT VARCHAR2,
                                 retcode                OUT NUMBER,
                                 p_organization_id   IN     NUMBER,
                                 p_last_run_date     IN     VARCHAR2);
END XXWC_MTL_DEMAND_HISTORY_PKG;
/