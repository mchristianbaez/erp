CREATE OR REPLACE PACKAGE APPS.xxwc_om_poa_pkg
AS
   /*************************************************************************
   *   $Header xxwc_om_poa_pkg.pks $
   *   Module Name: xxwc_om_poa_pkg.pks
   *
   *   PURPOSE:   This package is used by the XXWC OM POA unapply prepayments conc program
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        09/06/2013  Shankar Hariharan        Initial Version
   * ***************************************************************************/
   PROCEDURE unapply_prepayment (errbuf OUT VARCHAR2, retcode OUT VARCHAR2);
END xxwc_om_poa_pkg;
/