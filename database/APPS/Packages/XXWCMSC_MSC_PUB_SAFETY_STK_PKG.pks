CREATE OR REPLACE PACKAGE APPS.XXWCMSC_MSC_PUB_SAFETY_STK_PKG 
          AUTHID CURRENT_USER AS
/* $Header: MSCXPSSS.pls 120.1 2005/09/09 00:21:31 shwmathu noship $ */

  /* PL/SQL table types */
  TYPE COMPANYNAMELIST                 IS TABLE OF MSC_COMPANIES.company_name%TYPE;
  TYPE COMPANYSITELIST                 IS TABLE OF MSC_COMPANY_SITES.company_site_name%TYPE;
  TYPE ITEMNAMELIST                    IS TABLE OF MSC_SYSTEM_ITEMS.item_name%TYPE;
  TYPE ITEMDESCLIST                    IS TABLE OF MSC_SYSTEM_ITEMS.description%TYPE;
  TYPE ITEMUOMLIST                     IS TABLE OF MSC_SYSTEM_ITEMS.uom_code%TYPE;
  TYPE FNDMEANINGLIST                  IS TABLE OF FND_LOOKUP_VALUES.meaning%TYPE;
  TYPE PLANNERCODELIST                 IS TABLE OF MSC_SYSTEM_ITEMS.planner_code%TYPE;
  TYPE PLANNINGGROUPLIST               IS TABLE OF MSC_SUPPLIES.planning_group%TYPE;
  TYPE NUMBERLIST                      IS TABLE OF NUMBER;
  TYPE DATELIST                        IS TABLE OF DATE;


  PURCHASE_ORDER                       CONSTANT INTEGER := 1;   /* order type lookup - lookup_type = MRP_ORDER_TYPE  */
  PURCH_REQ                            CONSTANT INTEGER := 2;
  WORK_ORDER                           CONSTANT INTEGER := 3;
  REPETITIVE_SCHEDULE                  CONSTANT INTEGER := 4;
  PLANNED_ORDER                        CONSTANT INTEGER := 5;
  MATERIAL_TRANSFER                    CONSTANT INTEGER := 6;
  NONSTD_JOB                           CONSTANT INTEGER := 7;
  RECEIPT_PURCH_ORDER                  CONSTANT INTEGER := 8;
  REQUIREMENT                          CONSTANT INTEGER := 9;
  FPO_SUPPLY                           CONSTANT INTEGER := 10;
  SHIPMENT                             CONSTANT INTEGER := 11;
  RECEIPT_SHIPMENT                     CONSTANT INTEGER := 12;
  AGG_REP_SCHEDULE                     CONSTANT INTEGER := 13;
  DIS_JOB_BY                           CONSTANT INTEGER := 14;
  NON_ST_JOB_BY                        CONSTANT INTEGER := 15;
  REP_SCHED_BY                         CONSTANT INTEGER := 16;
  PLANNED_BY                           CONSTANT INTEGER := 17;
  ON_HAND_QTY                          CONSTANT INTEGER := 18;
  FLOW_SCHED                           CONSTANT INTEGER := 27;
  FLOW_SCHED_BY                        CONSTANT INTEGER := 28;
  PAYBACK_SUPPLY                       CONSTANT INTEGER :=29;


  DEMAND_PAYBACK                       CONSTANT INTEGER := 27; /* lookup_type = 'MSC_DEMAND_ORIGINATION' */
  /*in the package body, the query to the msc_demands will be
  using the lookup_type = 'MSC_DEMAND_ORIGINATION' for the origination_type*/

  PAB_SUPPLY                           CONSTANT INTEGER := 1;
  PAB_DEMAND                           CONSTANT INTEGER := 2;
  PAB_SCRAP_DEMAND                     CONSTANT INTEGER := 3;
  PAB_EXP_LOT                          CONSTANT INTEGER := 4;
  PAB_ONHAND                           CONSTANT INTEGER := 5;
 
  SAFETY_STOCK                         CONSTANT INTEGER := 7;
  PROJECTED_AVAILABLE_BALANCE          CONSTANT INTEGER := 27;

/**************************************************************************
 *
 * PROCEDURE
 *  Publish_Safety_Stocks
 *
 * DESCRIPTION
 *  This procedure collects the MRP safety stock information and submits it thru an API 
 *   to generate MTL safety stock information
 *
 * PARAMETERS
 * ==========
 * NAME              TYPE     IN/OUT  DESCRIPTION
.* ----------------- -------- ------  ---------------------------------------
 * p_errbuf          VARCHAR2 OUT     error message returned to concurrent request
 * p_retcode         VARCHAR2 OUT     return code  (0=success  1=warning  2=error)
 * p_plan_id         NUMBER   IN      MRP plan
 * p_org_code        VARCHAR2 IN      inventory organization
 * p_planner_code    VARCHAR2 IN      planner
 * p_abc_class       VARCHAR2 IN      ABC class
 * p_item_id         NUMBER   IN      inventory item id
 * p_planning_gp     VARCHAR2 IN
 * P_project_Id      NUMBER   IN      project id
 * p_task_id         NUMBER   IN      task id
 * p_item_category   VARCHAR2 IN      item category code
 * p_horizon_start   VARCHAR2 IN
 * p_horizon_end     VARCHAR2 IN
 * p_overwrite       VARCHAR2 IN      determine whether to replace existing safety stock information
 *
 * CALLED BY
 *  XXWC Publish Safety Stock concurrent program
 *
 *************************************************************************/
  PROCEDURE Publish_Safety_Stocks(p_errbuf            OUT NOCOPY VARCHAR2,
                                  p_retcode           OUT NOCOPY NUMBER,
                                  p_plan_id           IN NUMBER,
                                  p_org_code          IN VARCHAR2,
                                  p_planner_code      IN VARCHAR2,
                                  p_abc_class         IN VARCHAR2,
                                  p_item_id           IN NUMBER,
                                  p_planning_gp       IN VARCHAR2,
                                  p_project_id        IN NUMBER,
                                  p_task_id           IN NUMBER,
                                  p_item_category     IN VARCHAR2,
                                  p_horizon_start     IN VARCHAR2,
                                  p_horizon_end       IN VARCHAR2,
                                  p_overwrite         IN VARCHAR2
                                 );

/**************************************************************************
 *
 * PROCEDURE
 *  Generate_Safety_Stock
 *
 * DESCRIPTION
 *  This procedure collects the MRP forecast information and submits it thru an API 
 *   to generate MTL safety stock information
 *
 * PARAMETERS
 * ==========
 * NAME              TYPE     IN/OUT  DESCRIPTION
.* ----------------- -------- ------  ---------------------------------------
 * p_errbuf          VARCHAR2 OUT     error message returned to concurrent request
 * p_retcode         VARCHAR2 OUT     return code  (0=success  1=warning  2=error)
 * p_org_id          NUMBER   IN      inventory organization
 * p_forecast_set    VARCHAR2 IN      forecast set (which could include multiple forecast designators)
 * p_item_category   VARCHAR2 IN      item category
 * p_horizon_date    VARCHAR2 IN
 * p_overwrite       VARCHAR2 IN      determine whether to replace existing safety stock information
 *
 * CALLED BY
 *  XXWC Publish Safety Stock concurrent program
 *
 *************************************************************************
  Version 1.1       Updated 1/30/2013 - Pull Processing Lead Time instead of Safety Stock Bucket Days and remove MRP Planned % Restriction
  Version 1.2       Updated 8/14/2013 - Remove round order quantity logic and always round safety stock calculation to 2 decimals - TMS Ticket 20130814-02113 
  Version 1.3       Updated 2/6/2014 - Parameter for log file and output file - TMS Ticket 20140110-00053
 *************************************************************************/
  Procedure Generate_Safety_Stock(p_errbuf            OUT NOCOPY VARCHAR2,
                                  p_retcode           OUT NOCOPY NUMBER,
                                  p_org_id            IN  NUMBER,
																	p_forecast_set      IN  VARCHAR2,
																	p_item_category     IN  VARCHAR2,
																	p_horizon_date      IN  VARCHAR2,
																	p_overwrite         IN  VARCHAR2,
                                  p_write_log         IN  VARCHAR2,
                                  p_write_output      IN  VARCHAR2);


  PROCEDURE Log_Message(p_string IN VARCHAR2);

  FUNCTION Get_Message(p_app  IN VARCHAR2,
                       p_name IN VARCHAR2,
                       p_lang IN VARCHAR2
                      ) RETURN VARCHAR2;

END XXWCMSC_MSC_PUB_SAFETY_STK_PKG;
/
