CREATE OR REPLACE PACKAGE APPS.XXCUS_OZF_PROMO_OFFERS_PVT IS

  /*******************************************************************************
  * Procedure:
  * Description: Package will check all HDS custom validations well before oracle
  *             oracle starts checking the standard offer setup validations.
  *             This custom package is called within the oracle standard package
  *             OZF_Promotional_Offers_PVT.create_offers procedure.
  *
  * Dependencies: Oracle standard package OZF_Promotional_Offers_PVT.create_offers
  *
  * Note: How the function works?
  *
  *  Validations are performed in a sequence. When a failure occurs
  *  at each level, we raise the error message until there are no more failed
  *  HDS custom validations. We will call the function CHECK_HDS_VALIDATIONS
  *  only once.The function inturn can invoke as many validations as required. 
  *  For each validation we give user the ability to turn the validation on and off 
  *  using a custom profile created for each validation.
  * 
  *
  * Custom error messages: Error codes are setup using Application Developer responsibility
  *                       / Application / Messages for each validation that starts with XXCUS_OZF.
  *
  *                      Message description can be changed at any time without modifying the code.
  *
  *                      Example: XXCUS_OZF_CHECK_UNTIL_YEAR_DFF.
  *
  * WARNING: If for any reason the oracle package OZF_Promotional_Offers_PVT.create_offers   
  *         is recompiled using a patch or something else, please scan the package body
  *         script to make sure the custom XXCUS_OZF_PROMOTIONAL_OFFERS_PVT.CHECK_HDS_VALIDATIONS
  *         is invoked otherwise the offer entry will be missed altogether. 
  *
  *  
  HISTORY
  ===============================================================================
  ESMS / RFC      VERSION DATE          AUTHOR(S)        DESCRIPTION
  -------------   ------- -----------   ---------------  -----------------------------------------
  ESMS 273702      1.0   04/28/2015   Balaguru Seshadri Initial creation of the procedure
  ********************************************************************************/
  --
  PROCEDURE error_message
    (   p_message_name VARCHAR2,
        p_token_name   VARCHAR2 := NULL,
        P_token_value  VARCHAR2 := NULL
    );
  --
  PROCEDURE display_messages;
  --  
  PROCEDURE CHECK_HDS_VALIDATIONS_LEVEL1 
             (
               p_list_header_id  IN  NUMBER              
              ,p_vol_offer_type  IN  VARCHAR2
              ,p_status          OUT VARCHAR2
             );             
  --
  PROCEDURE CHECK_HDS_VALIDATIONS_LEVEL2 
             (
               p_list_header_id  IN  NUMBER
              ,p_status          OUT VARCHAR2
             );
  --  
END XXCUS_OZF_PROMO_OFFERS_PVT;
/