CREATE OR REPLACE PACKAGE APPS.XXWC_PO_HELPERS
/* Formatted on 04-Oct-2012 23:22:52 (QP5 v5.206) */
IS

   /*******************************************************************************************************
    $Header XXWC_PO_HELPERS.PKS $
     Module Name: XXWC_PO_HELPERS.PKS
   
     PURPOSE:   This package is used to hold routines used in Purchasing Module

     REVISIONS:
     Ver        Date        Author                             Description
     ---------  ----------  -------------------------------    ---------------------------------------------
     1.0        11/04/2014  Vijaysrinivasan                    TMS#20141002-00050 Multi org changes 
     2.0        04/04/2015  Gopi Damuluri                      TMS#20140715-00036 RF - PO Acceptances
     8.0        12/13/2016   P.Vamshidhar                      TMS#20150219-00093 -  Performance Tuning  															   	 
	                                                           Added new Procedure and function  
   **********************************************************************************************************/
   
    /*
     White Cap has the business need to run planning processes (Min-Max and Reorder Point Planning)
     on a nightly basis. This helps the Purchasing team to respond more quickly to unplanned demand.
     However, the nightly run of Min-ax planning creates a large number of requisitions which may not
      be actioned (i.e. converted to PO's) in one day. This may result in the creation of duplicate
       requisitions for the same item with different need by dates.
     Purging the requisitions on a nightly basis will eliminate the issue of duplicate requisitions
      and will also help reduce the volume of requisitions to be reviewed by Purchasing on a daily basis.

     Basic Business Needs
     The following are the functional requirements for the nightly auto-purging of requisitions:

         Create nightly process to purge requisitions based on the criteria below:
        Requisitions created by Min-Max or Reorder Point Planning (Import Source=INV)
        These requisitions will include Approved External requisitions or UnApproved Internal
        requisitions (created with Import Source=INV).
        The nightly process must be scheduled prior to running min-max or Reorder point planning every night.
     */
    PROCEDURE DELETE_INV_REQUISITIONS (P_ERRBUF OUT VARCHAR2, P_RETCODE OUT VARCHAR2, P_ORG_ID NUMBER DEFAULT NULL);

    ----used in XXWC_OEXOETEL_CUSTOM.pll
    FUNCTION XXWC_FIND_USER_NAME (P_USER_ID NUMBER)
        RETURN VARCHAR2;

    ----used in XXWC_OEXOETEL_CUSTOM.pll
    FUNCTION GET_DELIVERY_STATUS (P_LINE_ID NUMBER)
        RETURN VARCHAR2;

    ----used in XXWC_OEXOETEL_CUSTOM.pll
    FUNCTION DELIVERY_RUN (P_HEADER_ID NUMBER)
        RETURN VARCHAR2;

    ----used in XXWC_OEXOETEL_CUSTOM.pll
    PROCEDURE UPDATE_DELIVERY_RUN (P_HEADER_ID NUMBER);
    

    ----used in XXWC_OEXOETEL_CUSTOM.pll @ Rev 8.0
    FUNCTION OM_DELIVERY_RUN (P_HEADER_ID NUMBER, P_LINE_ID NUMBER)
        RETURN VARCHAR2;

    ----used in XXWC_OEXOETEL_CUSTOM.pll @ Rev 8.0
    PROCEDURE OM_UPDATE_DELIVERY_RUN (P_HEADER_ID NUMBER, P_LINE_ID NUMBER);

    -- 11/22/2013 CG: TMS 20131120-00028: New function to determine Doc Creation Method since the from
    -- Field does not display it. Used in Personalization 71.1 on PO Form (POXPOEPO)
    FUNCTION get_po_creation_method (p_po_header_id NUMBER) return VARCHAR2;
    
    -- 11/22/2013 CG: TMS 20131120-00028: New function to determine if PO has drop ship lines. 
    -- Used in Personalization 71.1 on PO Form (POXPOEPO)
    FUNCTION has_drop_ship_lines (p_po_header_id NUMBER) return VARCHAR2;
    
    -- 11/22/2013 CG: TMS 20131120-00028: New function to determine PO Ship to Location. 
    -- Used in Personalization 71.1 on PO Form (POXPOEPO)
    FUNCTION get_po_ship_to_location (p_po_header_id NUMBER) return VARCHAR2;
    
    -- 12/05/2013 CG: TMS 20131120-00026: New function to determine if PO Line is Open for Receipt/Billing
    -- to be able to default Promised Date. Used in Personalization 61 on PO Form (POXPOEPO)
    FUNCTION is_po_line_open (p_po_line_id NUMBER) RETURN varchar2;

    -- Version# 2.0 > Start
    FUNCTION get_po_acceptance_notes_cnt(p_po_header_id NUMBER) RETURN NUMBER;
    
    FUNCTION get_po_acceptance_due_date (p_in_date DATE) RETURN VARCHAR2;
    -- Version# 2.0 < End

END;
/


