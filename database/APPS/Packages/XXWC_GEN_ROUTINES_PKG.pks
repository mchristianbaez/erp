CREATE OR REPLACE PACKAGE APPS.xxwc_gen_routines_pkg
AS
   -- This package will contain all generic routines which are used by all Workstreams
   --Define variables for logging debug messages
   c_level_unexpected        CONSTANT NUMBER := 6;
   c_level_error             CONSTANT NUMBER := 5;
   c_level_exception         CONSTANT NUMBER := 4;
   c_level_event             CONSTANT NUMBER := 3;
   c_level_procedure         CONSTANT NUMBER := 2;
   c_level_statement         CONSTANT NUMBER := 1;
   c_current_runtime_level   CONSTANT NUMBER
                                           := fnd_log.g_current_runtime_level;
   c_site_level              CONSTANT NUMBER := 10004;

   -- Producre Log Message will log messages in Oracle Fnd_log_Messages
   PROCEDURE log_msg (
      p_debug_level   IN   NUMBER,
      p_mod_name      IN   VARCHAR2,
      p_debug_msg     IN   VARCHAR2
   );

   FUNCTION get_profile_value (p_profile_option_name IN VARCHAR2)
      RETURN VARCHAR2;

   -- Satish U: Nov 29 2011
   FUNCTION takenby (p_user_id NUMBER, p_name_type VARCHAR2 DEFAULT 'LONG')
      RETURN VARCHAR2;

   FUNCTION salesman (
      p_salesrep_id   NUMBER,
      p_name_type     VARCHAR2 DEFAULT 'LONG'
   )
      RETURN VARCHAR2;

   FUNCTION payment_term (p_term_id IN NUMBER)
      RETURN VARCHAR2;

--Kiran T :Dec 7 2011
--To caliculate the unit list price for Rental Items based on their Related Charge Items list price
   FUNCTION unit_list_price_calc (p_line_id NUMBER, p_inventory_item_id NUMBER)
      RETURN NUMBER;

--Kiran T :Dec 7 2011
--To caluculate the Tax in header level
   FUNCTION tax_calc (p_header_id NUMBER)
      RETURN NUMBER;

-- To display the White CAP Company Address and Phone numbers
   PROCEDURE branch_address (
      p_ship_from_org_id   IN       NUMBER,
      p_branch             OUT      VARCHAR2,
      p_street             OUT      VARCHAR2,
      p_city               OUT      VARCHAR2,
      p_phone              OUT      VARCHAR2,
      p_fax                OUT      VARCHAR2,
      p_ret_code           OUT      NUMBER,
      p_ret_msg            OUT      VARCHAR2
   );

   PROCEDURE site_address (
      p_site_use_code   IN       VARCHAR2,
      p_site_use_id     IN       NUMBER,
      p_addr1           OUT      VARCHAR2,
      p_loc             OUT      VARCHAR2,
      p_addr2           OUT      VARCHAR2,
      p_addr3           OUT      VARCHAR2,
      p_addr5           OUT      VARCHAR2,
      p_code            OUT      VARCHAR2,
      p_phone           OUT      VARCHAR2,
      p_ret_code        OUT      NUMBER,
      p_ret_msg         OUT      VARCHAR2,
      p_site_number     OUT      VARCHAR2
   );

   FUNCTION technician (
      p_resource_id   NUMBER,
      p_name_type     VARCHAR2 DEFAULT 'LONG'
   )
      RETURN VARCHAR2;

    /****************************************************************
   --Function Get_TOtal_Days_ONRent
   --Returns number of days on Rent
   -- Parameters Line_ID  ( Sales Order Line ID
   -- Description : This Function is used in Re-Rent Invoice Report
   -- History :  12-13-2011 Satish Upadhyayula
   *****************************************************************/
   FUNCTION get_total_days_onrent (p_so_line_id NUMBER)
      RETURN NUMBER;

   /****************************************************************
   --- Function Get_Qty_OnRent
   -- Returns Total Qty on Rent for a given Sales Order Line ID
   -- Description : THis function is used in Re-Rent Invoice Report
   -- History : 12-13-2011 : Satish Upadhyayula
   *****************************************************************/
   FUNCTION get_qty_returned (p_so_line_id NUMBER)
      RETURN NUMBER;

   /****************************************************************
   --- Function Get_Item_Cost
   -- Returns Item Cost from a PO Line  for a given Sales Order Line ID
   -- Description : THis function is used in Re-Rent Invoice Report
   -- History : 12-13-2011 : Satish Upadhyayula
   *****************************************************************/
   FUNCTION get_item_cost (p_so_line_id NUMBER)
      RETURN NUMBER;

   /****************************************************************
   --- Function Get_Item_UOM
   -- Returns Item UOM from a PO Line  for a given Sales Order Line ID
   -- Description : THis function is used in Re-Rent Invoice Report
   -- History : 12-13-2011 : Satish Upadhyayula
   *****************************************************************/
   FUNCTION get_item_uom (p_so_line_id NUMBER)
      RETURN VARCHAR2;

    /****************************************************************
   --- Function Curr_Period_End_Date
   -- Returns Current Period End Date for a given Sales Order Line ID
   -- Description : THis function is used in Re-Rent Invoice Report
   -- History : 12-13-2011 : Satish Upadhyayula
   *****************************************************************/
   FUNCTION curr_period_end_date (p_so_line_id NUMBER)
      RETURN DATE;

/****************************************************************
    --- Function Get_Trx_Type_ID
    -- Returns Transaction Type ID for given Transaction Name
    -- Description : THis function is used in Rental Mass Additions (1503)
    -- History : 01-19-2012 : Srinivas Malkapuram
    *****************************************************************/
   FUNCTION get_trx_type_id (p_trx_type_name VARCHAR2)
      RETURN NUMBER;

   /****************************************************************
   --- Function Get_Item_Serial_Code
   -- Checks whether inventory item has Serial Control Code
   -- Description : THis function is used in Personalizations for Rental Mass Additions (1503)
   -- History : 01-19-2012 : Srinivas Malkapuram
   *****************************************************************/
   FUNCTION get_item_serial_code (
      p_inventory_item_id   NUMBER,
      p_organization_id     NUMBER
   )
      RETURN NUMBER;

   /****************************************************************
    --- Function Is_Rental_Item_Exists
    -- Checks if Equivalent Rental Item Exists in the Same Org
    -- Description : THis function is used in Personalizations for Rental Mass Additions (1503)
    -- History : 01-19-2012 : Srinivas Malkapuram
    *****************************************************************/
   FUNCTION is_rental_item_exists (
      p_item_number       VARCHAR2,
      p_organization_id   NUMBER
   )
      RETURN NUMBER;

   /****************************************************************
    --- Function Get_NonSerial_Asset_Number
    -- Gets Asset Number for NonSerialized Items within inventory Org
    -- Description : THis function is used in Rental Asset Transfer Report (1504)
    -- History : 01-20-2012 : Srinivas Malkapuram
    *****************************************************************/
   FUNCTION get_nonserial_asset_number (
      p_segment1          VARCHAR2,
      p_organization_id   NUMBER
   )
      RETURN VARCHAR2;

/****************************************************************
    --- Function Get_Serial_Asset_Number
    -- Gets Asset Number for NonSerialized Items within inventory Org
    -- Description : THis function is used in Rental Asset Transfer Report (1504)
    -- History : 01-20-2012 : Srinivas Malkapuram
    *****************************************************************/
   FUNCTION get_serial_asset_number (
      p_transaction_id      NUMBER,
      p_inventory_item_id   NUMBER,
      p_organization_id     NUMBER
   )
      RETURN VARCHAR2;

    /****************************************************************
   --- Function Get_Asset_Type
   -- Gets Asset Type
   -- Description : THis function is used in Rental Asset Transfer Report (1504)
   -- History : 01-20-2012 : Srinivas Malkapuram
   *****************************************************************/
   FUNCTION get_asset_type (
      p_serial_control_code   NUMBER,
      p_asset_number          VARCHAR2
   )
      RETURN VARCHAR2;

    /****************************************************************
   --- Function Get_Asset_Number
   -- Gets Asset Type
   -- Description : THis function is used in Rental Asset Transfer Report (1504)
   -- History : 01-20-2012 : Srinivas Malkapuram
   *****************************************************************/
   FUNCTION get_asset_number (
      p_serial_control_code   NUMBER,
      p_asset_number          VARCHAR2
   )
      RETURN VARCHAR2;

        /****************************************************************
   --- Function Get_Asset_Type
   -- Gets Asset Type
   -- Description : THis function is used in Rental Asset Transfer Report (1504)
   -- History : 01-20-2012 : Srinivas Malkapuram
   *****************************************************************/
   FUNCTION get_asset_segments (p_asset_number VARCHAR2)
      RETURN VARCHAR2;

   PROCEDURE job_site_contact (
      p_ship_to_contact_id         NUMBER,
      p_contact_name         OUT   VARCHAR2,
      p_contact_number       OUT   VARCHAR2
   );

-- To display the White CAP map numbers      ---  Developed By Thiru
   FUNCTION MAP (p_ship_to_org_id NUMBER)
      RETURN VARCHAR2;

--TAVVA   Returns the Vendor Locations Which is used in PO REPORTS
   PROCEDURE vendor_locations (
      p_location_id                NUMBER,
      x_location_code        OUT   VARCHAR2,
      x_address_line_1       OUT   VARCHAR2,
      x_address_line_2       OUT   VARCHAR2,
      x_town_or_city         OUT   VARCHAR2,
      x_country              OUT   VARCHAR2,
      x_postax_code          OUT   VARCHAR2,
      x_telephone_number_1   OUT   VARCHAR2
   );

--TAVVA   Returns the Vendor Details Which is used in PO REPORTS
   PROCEDURE vendordetails_po (
      p_vendor_id                NUMBER,
      p_vendor_site_id           NUMBER,
      x_country            OUT   VARCHAR2,
      x_area_code          OUT   VARCHAR2,
      x_phone              OUT   VARCHAR2,
      x_fax                OUT   VARCHAR2,
      x_vendor_site_code   OUT   VARCHAR2,
      x_address_line1      OUT   VARCHAR2,
      x_address_line2      OUT   VARCHAR2,
      x_city               OUT   VARCHAR2,
      x_state              OUT   VARCHAR2,
      x_zip                OUT   VARCHAR2
   );

---Required for white cap reports   ---  Developed By Thiru
--Used in Backorder Report
   FUNCTION on_hand_qty (p_item_id NUMBER, p_org_id NUMBER)
      RETURN NUMBER;

---Required for white cap CFD reports   ---  Developed By Tavva
   FUNCTION verify_state_as_ca (p_site_use_id IN NUMBER)
      RETURN NUMBER;

--Required fo white cap cancel order lines personalization --Develope by harsha for tms#20130806-00698 

FUNCTION XXWC_GET_USER_PROF_VAL(V_PROFILE_USER VARCHAR2)
 RETURN number;


END xxwc_gen_routines_pkg;
/
