--
-- XXWC_BOM_CONVERSION_PKG  (Package) 
--
CREATE OR REPLACE PACKAGE apps.xxwc_bom_conversion_pkg
IS
   /********************************************************************************
   FILE NAME: APPS.XXWC_BOM_CONVERSION_PKG.pkg

   PROGRAM TYPE: PL/SQL Package

   PURPOSE: Validates and loads BOM and Routing data from staging to interface tables.

   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.0     08/24/2012    Gopi Damuluri    Initial version.
   ********************************************************************************/

   PROCEDURE validate_bom (p_errbuf OUT VARCHAR2, p_retcode OUT NUMBER);

   PROCEDURE bom_conversion (p_errbuf             OUT VARCHAR2
                            ,p_retcode            OUT NUMBER
                            ,p_validate_only   IN     VARCHAR2);

   PROCEDURE routing_conversion (p_errbuf            OUT VARCHAR2
                                ,p_retcode           OUT NUMBER
                                ,p_mfg_org_code   IN     VARCHAR2);
END xxwc_bom_conversion_pkg;
/

