/* Formatted on 6/8/2014 4:29:42 PM (QP5 v5.254.13202.43190) */
CREATE OR REPLACE PACKAGE XXWC_PO_RTV_PKG
AS
   /*************************************************************************
     $Header XXWC_PO_RTV_PKG.pks $
     Module Name: XXWC_PO_RTV_PKG

     PURPOSE: Package to do the database activities for the RTV Improvements
     ESMS Task Id :  20130911-00538

     REVISIONS:
     Ver        Date        Author                Description
     ---------  ----------  ------------------    ----------------
     1.0        04/21/2014  Manjula Chellappan    Initial Version

   **************************************************************************/

   PROCEDURE confirm_rtv (p_rtv_header_id   IN     NUMBER,
                          p_user_id         IN     NUMBER,
                          p_result             OUT BOOLEAN,
                          p_error_message      OUT VARCHAR2);

   PROCEDURE print_document (p_wc_return_number   IN     VARCHAR2,
                             p_program_name       IN     VARCHAR2,
                             p_user_id            IN     NUMBER,
                             p_resp_id            IN     NUMBER,
                             p_request_id            OUT NUMBER,
                             p_error_msg             OUT VARCHAR2);

   FUNCTION get_vendor_contact_id (p_po_header_id IN NUMBER)
      RETURN NUMBER;
END XXWC_PO_RTV_PKG;
/