CREATE OR REPLACE PACKAGE APPS.XXWC_GENERATE_CR_NUMBER
AS
   /**************************************************************************
    File Name:XXWC_GENERATE_CR_NUMBER.pks
    PROGRAM TYPE: PL/SQL Package specification
    PURPOSE: Create change request number
    HISTORY
    -- Description   :
    --
    --
    -- Dependencies Tables        : None
    -- Dependencies Views         : None
    -- Dependencies Sequences     : XXWC_GENERATE_CR_NUM_SEQ
    -- Dependencies Procedures    : None
    -- Dependencies Functions     : None
    -- Dependencies Packages      : None
    -- Dependencies Types         : None
    -- Dependencies Database Links: None
    ================================================================
           Last Update Date : 12/20/2013
    ================================================================
    ================================================================
    VERSION DATE          AUTHOR(S)       DESCRIPTION
    ------- -----------   --------------- ---------------------------------
    1.0     20-Dec-2013   Praveen Pawar   Initial creation of the package
    1.1     22-Jan-2014   Geoff Caesar    Add Apps. prefix and fix typeO Nummber to Number    
   **************************************************************************/

   FUNCTION GENERATE_CR_NUMBER (pv_param1  IN   VARCHAR2 
			                             ) RETURN VARCHAR2;

END XXWC_GENERATE_CR_NUMBER;
