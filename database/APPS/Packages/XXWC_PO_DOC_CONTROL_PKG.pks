CREATE OR REPLACE PACKAGE APPS.xxwc_po_doc_control_pkg
AS
   /******************************************************************************
      NAME:       apps.xxwc_po_doc_control_pkg
      PURPOSE:    Package to perform actions on Purchasing documents.

      REVISIONS:
      Ver        Date        Author           Description
      ---------  ----------  ---------------  ------------------------------------
      1.0        12/10/2013      shariharan    Initial Version
      1.1        12/27/2013      shariharan    BPA Ticket Task ID: 20130815-00900
      1.2        06/02/2014      lspitzer      Vendor Cost Improvements : 20130917-00676  - Added New Function get_pz_price_break
   ******************************************************************************/
   PROCEDURE cancel_bpa_lines (errbuf              OUT VARCHAR2,
                               retcode             OUT VARCHAR2,
                               i_po_header_id   IN     NUMBER);
                               
   FUNCTION get_price_break(i_header_id in number,i_ship_to_org_id in number, i_line_id in number, i_line_location_id in number,i_qty in number,i_price in number)
    return varchar;        
   
   /*************************************************************************************************   
   *   Function get_pz_price_break                                                                  *
   *   Purpose : Used for PO Tool Tip Personalization to show buyers price breaks for price zones   *
   *                                                                                                *
   *                                                                                                *
   *   FUNCTION get_pz_price_break(i_vendor_id in number, i_vendor_site_id in number, i_header_id in number,i_ship_to_org_id in number, i_inventory_item_id in number, i_qty in number,i_price in number)
   *    return varchar2;                              
   *                                                                                                *
   *   REVISIONS:                                                                                   *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   1.2        06/02/2014   Lee Spitzer              Initial Versions TMS 20130917-00676         *
   *                                                         Vendor Cost Management Improvements    *
   *                                                                                                *
   /************************************************************************************************/
   

   FUNCTION get_pz_price_break(i_vendor_id in number, i_vendor_site_id in number, i_header_id in number,i_ship_to_org_id in number, i_inventory_item_id in number, i_qty in number,i_price in number)
    return varchar2;                              

END xxwc_po_doc_control_pkg;
/
