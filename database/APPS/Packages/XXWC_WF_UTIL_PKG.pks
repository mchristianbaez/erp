-- Start of DDL Script for Package APPS.XXWC_WF_UTIL_PKG
-- Generated 13-Jun-2012 16:09:20 from APPS@EBIZCON

CREATE OR REPLACE 
PACKAGE apps.xxwc_wf_util_pkg
/* Formatted on 11-Jun-2012 18:48:54 (QP5 v5.206) */
IS
    -- This procedure will assign the WC Role - Associate
    -- role to all White Cap associates (GL segment1 = 0W) who
    -- do not already have it via an indirect assignment.
    -- It will also revoke it from anyone who has received it
    -- through an indirect assignment.
    -- The intent is to ensure all White Cap associates have
    -- this role while also ensuring we do not double-assign
    -- the role for a long period of time (that can cause odd behavior
    -- in the apps, like responsibilities showing up twice or three times in lists).
    -- This can be run ad-hoc or be scheduled as a daily concurrent program
    -- MODIFICATION HISTORY
    -- Person      Date    Comments
    -- ---------   ------  ------------------------------------------
    -- Andre Rivas 24-MAY-2004

    PROCEDURE xxwc_assign_default_role (p_errbuf               OUT VARCHAR2,
                                        p_retcode              OUT VARCHAR2,
                                        p_expiration_date          DATE DEFAULT NULL,
                                        p_admin_user        IN     VARCHAR2,
                                        p_start_date        IN     DATE);

    PROCEDURE xxwc_assign_role (p_user_name          VARCHAR2,
                                p_role_name          VARCHAR2,
                                p_start_date         DATE,
                                p_expiration_date    DATE DEFAULT NULL);

    PROCEDURE xxwc_log (p_message VARCHAR2);

    --******************************************************************************
    --******************************************************************************
    --******************************************************************************
    -- PROCEDURE xxwc_security_roles_report;
    --******************************************************************************
    --   Include one row for every active User record that meets these criteria
    --    User name
    --    First name and Last name
    --    User Email from User record
    --    Start date / End Date
    --    Supervisor Name from employee
    --    Employee Number
    --    A column named "Alert"
    -- Criteria
    --   The user is either
    --   a. In the White Cap organization based on the employee record's GL segment1 = 0W
    --   b. Assigned any XXWC_ROLE_% role or XXWC_% responsibility directly or indirectly
    --        that is active (end/start date) because some users might not even have
    --         an Employee record or might not be assigned to the WC organization
    --     Then any of the following can be true
    --    1. The user has been assigned more than one XXWC_ROLE% role directly,
    --       excluding the XXWC_ROLE_ASSOCIATE role (Alert = "Multiple Roles")
    --    2. The user has been assigned any responsibility directly other than
    --      iExpense (Alert = "Direct Responsibility"")
    --    The purpose is to run this report every day to see if any support staff
    --    has more rights than in WC Role and responsibility assignment policy.
    --   After executing:  begin xxwc_wf_util_pkg.xxwc_security_roles_report; end;
    --    ,run below select for the Report, it can be a APPS Concurrent Program:
    /*   SELECT DISTINCT user_name_oracle,
                        first_name,
                        last_name,
                        email_address,
                        employee_number,
                        full_name,
                        assignment_start_date,
                        assignment_end_date,
                        manager_first_name,
                        manager_last_name,
                        manager_email,
                        segment1,
                        alert1,
                        alert2
          FROM xxwc.xxwc_security_roles_report
        WHERE alert1 IS NOT NULL OR alert2 IS NOT NULL;


    */
    PROCEDURE refresh_security_role_report;

    TYPE xxwc_wf_role_report IS REF CURSOR;

    --******************************************************************************
    --******************************************************************************
    --******************************************************************************
    -- PROCEDURE xxwc_format_role_report (p_report OUT xxwc_wf_role_report)
    --will return cursor which can be used by Oracle CP  with sql executable
    --VARIABLE s_role_rep  REFCURSOR
    --EXECUTE xxwc_wf_util_pkg.xxwc_security_roles_report;
    --EXECUTE xxwc_wf_util_pkg.xxwc_wf_util_pkg(:s_role_rep);
    --put here some formatting
    --print s_role_rep
    --******************************************************************************
    PROCEDURE xxwc_format_role_report (p_report OUT xxwc_wf_role_report);
END;                                                                                                     -- Package spec
/



-- End of DDL Script for Package APPS.XXWC_WF_UTIL_PKG

