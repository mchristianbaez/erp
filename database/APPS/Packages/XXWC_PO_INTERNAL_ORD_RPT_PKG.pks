CREATE OR REPLACE PACKAGE APPS.xxwc_po_internal_ord_rpt_pkg
AS
   /*************************************************************************
    Copyright (c) 2012 Lucidity Consulting Group
    All rights reserved.
   **************************************************************************
     $Header xxwc_po_internal_ord_rpt_pkg $
     Module Name: xxwc_po_internal_ord_rpt_pkg.pks

     PURPOSE:   

     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.0        09/08/2012  Consuelo Gonzalez      Initial Version
     2.0        09/04/2012  Consuelo Gonzalez      TMS 20130904-00914: Fix for UOM Conversion Error
     3.0        11/22/2013  Consuelo Gonzalez      TMS 20131120-00029: Removing reservable field and 
                                                   adding On-Order
   **************************************************************************/

   /*************************************************************************
     Procedure : gen_internal_ord_rpt

     PURPOSE:   This procedure will generate the internal orders onhand report
     Parameter:

   ************************************************************************/
   PROCEDURE gen_internal_ord_rpt (
      errbuf             OUT      VARCHAR2,
      retcode            OUT      VARCHAR2,
      p_org_code         IN       VARCHAR2,
      p_req_number       IN       VARCHAR2
   );

   
END xxwc_po_internal_ord_rpt_pkg;
/


