CREATE OR REPLACE PACKAGE APPS.xxwc_mv_routines_pkg
AS
   /*************************************************************************
    Copyright (c) 2012 Lucidity Consulting Group
    All rights reserved.
   **************************************************************************
     $Header xxwc_mv_routines_pkg $
     Module Name: xxwc_mv_routines_pkg.pks

     PURPOSE:   This package is used by the BT Interfaces, EBS to Prism

     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.0        01/11/2012  Consuelo Gonzalez      Initial Version
     3.0        05/29/2015  Maharajan Shunmugam    TMS#20150527-00031 OM Rental Receipt Bug Fix

   **************************************************************************/
   FUNCTION get_phone_fax_number (
      p_contact_point_type   IN   VARCHAR2,
      p_party_id             IN   NUMBER,
      p_party_site_id        IN   NUMBER,
      p_phone_line_type      IN   VARCHAR2
   )
      RETURN VARCHAR2;

   FUNCTION get_open_po_qty (
      p_inventory_item_id   IN   NUMBER,
      p_organization_id     IN   NUMBER
   )
      RETURN NUMBER;

   FUNCTION get_safety_stock_qty (
      p_inventory_item_id   IN   NUMBER,
      p_organization_id     IN   NUMBER
   )
      RETURN NUMBER;

   FUNCTION get_open_so_ln_qty (
      p_inventory_item_id   IN   NUMBER,
      p_organization_id     IN   NUMBER
   )
      RETURN NUMBER;

   FUNCTION get_transit_qty (
      p_inventory_item_id   IN   NUMBER,
      p_organization_id     IN   NUMBER
   )
      RETURN NUMBER;

   FUNCTION get_oldest_receipt_date (
      p_inventory_item_id   IN   NUMBER,
      p_organization_id     IN   NUMBER
   )
      RETURN DATE;

   FUNCTION get_last_sales_date (
      p_inventory_item_id   IN   NUMBER,
      p_organization_id     IN   NUMBER
   )
      RETURN DATE;

   FUNCTION get_last_count_date (
      p_inventory_item_id   IN   NUMBER,
      p_organization_id     IN   NUMBER
   )
      RETURN DATE;

   FUNCTION get_last_count_qty (
      p_inventory_item_id   IN   NUMBER,
      p_organization_id     IN   NUMBER
   )
      RETURN NUMBER;

   FUNCTION get_last_counted_by (
      p_inventory_item_id   IN   NUMBER,
      p_organization_id     IN   NUMBER
   )
      RETURN VARCHAR2;

   FUNCTION get_cat_segment_label (
      p_structure_id   IN   NUMBER,
      p_cat_segment    IN   VARCHAR2,
      p_cat_value      IN   VARCHAR2
   )
      RETURN VARCHAR2;

   FUNCTION get_cat_segment_desc (
      p_structure_id   IN   NUMBER,
      p_cat_segment    IN   VARCHAR2,
      p_cat_value      IN   VARCHAR2
   )
      RETURN VARCHAR2;

   FUNCTION get_cust_credit_limit (
      p_cust_account_id   IN   NUMBER,
      p_site_use_id       IN   NUMBER
   )
      RETURN NUMBER;

   FUNCTION get_credit_manager (
      p_cust_account_id   IN   NUMBER,
      p_site_use_id       IN   NUMBER
   )
      RETURN NUMBER;

   FUNCTION get_contact_name (p_contact_id IN NUMBER)
      RETURN VARCHAR2;

   FUNCTION get_last_po_price (
      p_inventory_item_id   IN   NUMBER,
      p_organization_id     IN   NUMBER
   )
      RETURN NUMBER;

   FUNCTION get_first_sale_date (p_cust_account_id IN NUMBER)
      RETURN DATE;

   FUNCTION get_last_sale_date (p_cust_account_id IN NUMBER)
      RETURN DATE;

   FUNCTION get_ar_term_desc (p_term_id IN NUMBER)
      RETURN VARCHAR2;

   /*function get_high_credit_ytd(p_party_id IN NUMBER
                               , p_cust_account_id IN NUMBER
                               , p_customer_site_use_id IN NUMBER)
   RETURN VARCHAR2;*/
   FUNCTION get_high_credit_ytd (p_cust_account_id IN NUMBER)
      RETURN NUMBER;

   /*function get_high_credit_date_ytd(p_party_id IN NUMBER
                                   , p_cust_account_id IN NUMBER
                                   , p_customer_site_use_id IN NUMBER)
   RETURN date;*/
   FUNCTION get_high_credit_date_ytd (p_cust_account_id IN NUMBER)
      RETURN DATE;

   FUNCTION get_last_ar_trx_date (p_cust_account_id IN NUMBER)
      RETURN DATE;

   FUNCTION get_cust_sales (p_cust_account_id IN NUMBER)
      RETURN NUMBER;

   FUNCTION get_avg_days_late (p_cust_account_id IN NUMBER)
      RETURN NUMBER;

   FUNCTION get_order_shp_from_org_id (p_order_number IN VARCHAR2)
      RETURN NUMBER;

   FUNCTION get_tax_rate (
      p_customer_trx_id            IN   NUMBER,
      p_link_to_cust_trx_line_id   IN   NUMBER
   )
      RETURN NUMBER;

   FUNCTION get_tax_amount (
      p_customer_trx_id            IN   NUMBER,
      p_link_to_cust_trx_line_id   IN   NUMBER
   )
      RETURN NUMBER;

   FUNCTION get_user_employee (p_user_id IN NUMBER)
      RETURN VARCHAR;

   FUNCTION get_rental_ship_date (p_line_id IN NUMBER,p_link_to_line_id IN NUMBER)    --<<Added new parameter p_link_to_line_id for Ver#3.0
      RETURN DATE;

   FUNCTION get_rental_return_date (p_line_id IN NUMBER)
      RETURN DATE;

   FUNCTION get_bt_trx_salesrep (
      p_trx_salesrep_id       IN   NUMBER,
      p_bill_to_salesrep_id   IN   NUMBER,
      p_org_id                IN   NUMBER
   )
      RETURN VARCHAR2;

   FUNCTION get_inv_adj_amount (
      p_customer_trx_id        IN   NUMBER,
      p_customer_trx_line_id   IN   NUMBER,
      p_org_id                 IN   NUMBER
   )
      RETURN NUMBER;

   FUNCTION get_mst_organization_id (p_operating_unit_id IN NUMBER)
      RETURN NUMBER;

   FUNCTION get_salesrep_info (p_salesrep_num IN VARCHAR2)
      RETURN VARCHAR2;

   FUNCTION get_ap_invoice_num (
      p_po_header_id          IN   NUMBER,
      p_po_line_id            IN   NUMBER,
      p_po_line_location_id   IN   NUMBER,
      p_shipment_line_id      IN   NUMBER,
      p_rcv_transaction_id    IN   NUMBER
   )
      RETURN VARCHAR2;

   FUNCTION get_ap_invoice_tax (p_invoice_id IN NUMBER, p_org_id IN NUMBER)
      RETURN NUMBER;

   FUNCTION format_phone_number (p_phone_number IN VARCHAR2)
      RETURN VARCHAR2;

   FUNCTION get_bt_trx_salesrep_phone (
      p_trx_salesrep_id       IN   NUMBER,
      p_bill_to_salesrep_id   IN   NUMBER,
      p_org_id                IN   NUMBER
   )
      RETURN VARCHAR2;

   FUNCTION get_prism_rental_date (
      p_return_date_type   IN   VARCHAR2,
      p_trx_number         IN   VARCHAR2
   )
      RETURN DATE;

   FUNCTION get_prism_rental_type (p_trx_number IN VARCHAR2)
      RETURN VARCHAR2;

   FUNCTION get_prism_inv_line_uom (
      p_trx_number    IN   VARCHAR2,
      p_line_number   IN   VARCHAR2
   )
      RETURN VARCHAR2;

   FUNCTION get_uom_conv_rate_to_base (p_rcv_uom IN VARCHAR2
                                       , p_trx_uom IN VARCHAR2)
      RETURN NUMBER;

   FUNCTION get_item_cross_reference (
      p_inventory_item_id      IN   NUMBER,
      p_cross_reference_type   IN   VARCHAR2,
      p_rownum                 IN   NUMBER
   )
      RETURN VARCHAR2;

   FUNCTION get_salesrep_branch (p_fru_number IN VARCHAR2)
      RETURN VARCHAR2;

   FUNCTION get_item_category (
      p_inventory_item_id   IN   NUMBER,
      p_organization_id     IN   NUMBER,
      p_category_set        IN   VARCHAR2,
      p_segment_num         IN   NUMBER
   )
      RETURN VARCHAR2;

   FUNCTION get_item_sr_vendor (
      p_inventory_item_id   IN   NUMBER,
      p_organization_id     IN   NUMBER
   )
      RETURN VARCHAR2;

   FUNCTION get_last_receipt_date (
      p_inventory_item_id   IN   NUMBER,
      p_organization_id     IN   NUMBER,
      p_type                IN   VARCHAR2
   )
      RETURN DATE;

   FUNCTION get_last_transf_out_date (
      p_inventory_item_id   IN   NUMBER,
      p_organization_id     IN   NUMBER
   )
      RETURN DATE;

   FUNCTION get_onhand_qty (
      p_inventory_item_id   IN   NUMBER,
      p_organization_id     IN   NUMBER,
      p_subinventory_name   IN   VARCHAR2 DEFAULT NULL,
      p_return_value_type   IN   VARCHAR2
   )
      RETURN NUMBER;

   FUNCTION get_vendor_consigned_qty (
      p_inventory_item_id   IN   NUMBER,
      p_organization_id     IN   NUMBER
   )
      RETURN NUMBER;

   FUNCTION get_source_mgr_rep_num (p_source_id IN NUMBER)
      RETURN VARCHAR2;

   FUNCTION get_item_qp_list_price (p_inventory_item_id IN NUMBER)
      RETURN NUMBER;

   FUNCTION get_order_has_dropships (p_order_header_id IN NUMBER)
      RETURN VARCHAR2;

   FUNCTION get_oe_ds_line_vendor (p_order_line_id IN NUMBER)
      RETURN VARCHAR2;

   FUNCTION get_backordered_so_item_qty (
      p_inventory_item_id   IN   NUMBER,
      p_organization_id     IN   NUMBER
   )
      RETURN NUMBER;

   FUNCTION get_customer_open_balance (p_customer_id IN NUMBER)
      RETURN NUMBER;

   FUNCTION get_customer_last_payment_date (p_customer_id IN NUMBER)
      RETURN DATE;

   FUNCTION get_gl_code_segment (
      p_code_combination_id   IN   NUMBER,
      p_return_segment        IN   NUMBER
   )
      RETURN VARCHAR2;

   FUNCTION get_om_rental_item (p_line_id IN NUMBER, p_return_type IN VARCHAR2)
      RETURN VARCHAR2;

   FUNCTION get_item_dflt_loc (
      p_inventory_item_id   IN   NUMBER,
      p_organization_id     IN   NUMBER,
      p_subinventory        IN   VARCHAR2,
      p_default_loc_lvl     IN   NUMBER
   )
      RETURN VARCHAR2;

   FUNCTION get_order_line_cost (p_line_id  IN NUMBER)
   RETURN NUMBER;

   FUNCTION is_item_branch_source (p_inventory_item_id IN NUMBER
                                    , p_organization_id IN NUMBER)
   RETURN VARCHAR2;

   FUNCTION get_prism_line_unit_price (p_invoice_number IN VARCHAR2
                                        , p_invoice_line_no IN VARCHAR2)
   RETURN NUMBER;

   FUNCTION get_prism_line_ext_price (p_invoice_number IN VARCHAR2
                                        , p_invoice_line_no IN VARCHAR2)
   RETURN NUMBER;

   FUNCTION get_last_counted_by_ntid (
      p_inventory_item_id   IN   NUMBER,
      p_organization_id     IN   NUMBER
   )
      RETURN VARCHAR2;

   FUNCTION get_username (p_user_id IN NUMBER)
   RETURN VARCHAR2;

   FUNCTION get_vendor_quote_cost (p_line_id NUMBER)
   RETURN NUMBER;

   FUNCTION get_aged_inventory_rcvd (p_inventory_item_id IN NUMBER
                                    , p_organization_id IN NUMBER
                                    , p_from_age IN NUMBER
                                    , p_to_age IN NUMBER
                                    , p_above_age IN NUMBER)
    RETURN NUMBER;

   FUNCTION text_to_number (
      iv_character_value   IN   VARCHAR2
   )
   RETURN NUMBER;

   FUNCTION get_customer_name (
      in_customer_id   IN   NUMBER
   )
   RETURN VARCHAR2;

   FUNCTION get_master_inv (in_org_id IN NUMBER DEFAULT NULL)
   RETURN NUMBER;

   FUNCTION get_lookup_value (p_lookup_type in VARCHAR2
                             , p_lookup_code in VARCHAR2
                             , p_view_application_id in NUMBER)
   RETURN VARCHAR2;

   FUNCTION get_prev_periods_sales (p_customer_id   in NUMBER
                                    , p_prior_months_count in NUMBER
                                    , p_org_id  in NUMBER)
   RETURN NUMBER;

   FUNCTION get_site_type_credit_limit  (p_cust_account_id in NUMBER
                                        , p_site_use_id in NUMBER
                                        , p_site_type in VARCHAR2)
   RETURN NUMBER;

   FUNCTION get_default_supplier (p_inventory_item_id in NUMBER
                                , p_organization_id in NUMBER
                                , p_return_type in VARCHAR2)
   RETURN VARCHAR2;

   FUNCTION get_branch_num_from_lob (p_branch_lob IN VARCHAR2)
   RETURN VARCHAR2;

   FUNCTION get_prism_inv_house_rep (p_invoice_number IN VARCHAR2)
   RETURN VARCHAR2;

   procedure exec_dynamic_sql (sql_query    IN  VARCHAR2,
                                char_qty    OUT VARCHAR2);

   PROCEDURE XXWC_AR_CUST_BAL_MV_REFRESH (p_errbuf OUT VARCHAR2, p_retcode OUT VARCHAR2);

   FUNCTION get_cust_high_balance (p_cust_account_id IN NUMBER)
   RETURN NUMBER;

   FUNCTION get_cust_in_proc_bal (p_cust_account_id IN NUMBER)
   RETURN NUMBER;
 
   FUNCTION get_edw_cust_pb_site (p_cust_account_id IN NUMBER
                                  , p_org_id    IN NUMBER)
   RETURN VARCHAR2;
 
   FUNCTION get_ap_invoice_payment_amts (p_invoice_id   IN NUMBER
                                        , p_org_id     IN NUMBER
                                        , p_return_type IN VARCHAR2)
   RETURN NUMBER;                                        
 
   FUNCTION get_ap_invoice_payment_info (p_invoice_id   IN NUMBER
                                        , p_org_id      IN NUMBER
                                        , p_return_type IN VARCHAR2)
   RETURN VARCHAR2;

   FUNCTION get_edw_supp_prim_pay_site (p_vendor_num IN VARCHAR2
                                        , p_org_id IN NUMBER)
   RETURN VARCHAR2;                                        

   -- 01/30/13 CG: TMS 20130122-01578 Retrieve employees branch location
   FUNCTION get_emp_inv_branch_loc (p_employee_id IN NUMBER)
   RETURN VARCHAR2;

   -- 02/18/13 CG: TMS 20130218-01098: Added function to pull last supplier for SPECIAL items
   FUNCTION get_special_last_pay_site (p_inventory_item_id IN NUMBER
                                       , p_organization_id IN NUMBER
                                       , p_operating_unit_id IN NUMBER)
   RETURN VARCHAR2;

-- 03/26/13 CG: TMS 20130121-00481: Added function to retrieve a contact number (person/site/party) based on it's position or number in a full pull
-- Used by the new custom contact from ONLY
FUNCTION XXWC_CONTACT_POINT_FNC (
    P_OWNER_TABLE_NAME VARCHAR2
    , P_OWNER_TABLE_ID NUMBER
    , P_CONTACT_TYPE VARCHAR2
    , P_ROWNUM    NUMBER
) RETURN VARCHAR2;
--02/04/2013:Harsha--TMS 20140130-00206 : Added function to retrieve order method details

FUNCTION XXWC_order_method_FNC (
    P_Source_id number,
P_Attribute2 varchar2
) RETURN VARCHAR2;

--03/07/2014:Harsha--TMS 20140306-00133  --EDW - Incorrect RVP Reporting for DMs

 FUNCTION get_salesrep_areamgr_info (p_salesrep_num IN VARCHAR2)
      RETURN VARCHAR2;


END xxwc_mv_routines_pkg;
/
