CREATE OR REPLACE
PACKAGE XXWC_OM_COF_PKG
  /********************************************************************************
  FILE NAME: APPS.XXWCAP_INV_INT_PKG.pkg
  PROGRAM TYPE: PL/SQL Package Body
  PURPOSE: APIs for Order Management Cash On Fly
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     01/08/2013    Gopi Damuluri    Initial version.
  ********************************************************************************/
IS
  l_package_name VARCHAR2(100) := 'XXWC_OM_COF_PKG';
  l_distro_list  VARCHAR2(100) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
  -- ********************************************************************************
  -- Function to get SalesPerson for a given InventoryOrg
  -- *******************************************************************************
  FUNCTION get_sales_rep(
      p_ship_from_org IN VARCHAR2 ,
      p_return_type   IN VARCHAR2)
    RETURN VARCHAR2;
  PROCEDURE jof_update_cust(
      p_site_use_id IN NUMBER);
  PROCEDURE update_cust_acct_name(
      p_customer_id  IN NUMBER ,
      p_account_name IN VARCHAR2 ,
      p_salesrep_id  IN NUMBER);
END XXWC_OM_COF_PKG;
