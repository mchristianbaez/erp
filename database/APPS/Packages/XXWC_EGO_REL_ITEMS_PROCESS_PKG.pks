CREATE OR REPLACE PACKAGE apps.xxwc_ego_rel_items_process_pkg
/*********************************************************************************
File Name:    xxwc_ego_rel_items_process_pkg
PROGRAM TYPE: PL/SQL Package specification
HISTORY
PURPOSE: Procedures and functions for maintaining the image exists attribute
==================================================================================
VERSION DATE          AUTHOR(S)               DESCRIPTION
------- -----------   --------------- --------------------------------------------
1.0     13-May-2014   Praveen Pawar           Initial creation of the package spec
*********************************************************************************/
AS
   FUNCTION load_and_process_rel_items (
      pv_transaction_type        IN   VARCHAR2,
      pv_org_code                IN   VARCHAR2,
      pv_item_number             IN   VARCHAR2,
      pv_related_item_number     IN   VARCHAR2,
      pv_relationship_type       IN   VARCHAR2,
      pv_reciprocal_flag         IN   VARCHAR2,
      pv_planning_enabled_flag   IN   VARCHAR2,
      pd_start_date              IN   DATE,
      pd_end_date                IN   DATE
   )
      RETURN VARCHAR2;
END xxwc_ego_rel_items_process_pkg;
/