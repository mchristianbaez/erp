CREATE OR REPLACE PACKAGE XXWC_AR_ADJUSTMENTS_WEBADI_PKG 
/********************************************************************************
FILE NAME: APPS.XXWC_AR_ADJUSTMENTS_WEBADI_PKG.pkg

PROGRAM TYPE: PL/SQL Package

PURPOSE: API to load AR Invoice Adjustments using Web ADI.

HISTORY
===============================================================================
VERSION DATE          AUTHOR(S)       DESCRIPTION
------- -----------   --------------- -----------------------------------------
1.0     01/08/2013    Gopi Damuluri    Initial version.
********************************************************************************/
AS

 PROCEDURE IMPORT( p_customer_number       IN VARCHAR2
                 , p_customer_name         IN VARCHAR2
                 , p_customer_site_number  IN VARCHAR2
                 , p_invoice_source        IN VARCHAR2
                 , p_trx_number            IN VARCHAR2
                 , p_amount                IN NUMBER
                 , p_apply_date            IN DATE
                 , p_type                  IN VARCHAR2
                 , p_adjustment_type       IN VARCHAR2
                 , p_reason                IN VARCHAR2
                 , p_comments              IN VARCHAR2
--                 , p_code_combination_id   IN NUMBER
                 , p_created_from          IN VARCHAR2
                 , p_receivables_trx       IN VARCHAR2
                 );

END XXWC_AR_ADJUSTMENTS_WEBADI_PKG;
/