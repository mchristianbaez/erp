CREATE OR REPLACE PACKAGE XXWC_AR_EQUIFAX_CUST_ACCT_PKG
AS
   /* $Header: XXWC_AR_EQUIFAX_CUST_ACCT_PKG.pls $*/
   /*#
   * This package contains the public APIs for customer accounts and related entities.
   * @rep:scope public
   * @rep:product XXWC
   * @rep:displayname Create Equifax Customer Account
   * @rep:category BUSINESS_ENTITY HZ_CUSTOMER_ACCOUNT
   * @rep:category BUSINESS_ENTITY HZ_ORGANIZATION
   * @rep:category BUSINESS_ENTITY HZ_PERSON
   * @rep:lifecycle active
   * @rep:compatibility S
   * @rep:doccd 120hztig.pdf Customer Account APIs,  Oracle Trading Community Architecture Technical Implementation Guide
   */

   /*#
   * Returns Location Information
   * @param  SalesforceId                IN         VARCHAR2
   * @param  CustomerNumber              IN         VARCHAR2
   * @param  BusinessName                IN         VARCHAR2
   * @param  Phone                       IN         NUMBER
   * @param  Fax                         IN         NUMBER
   * @param  EmailAddress                IN         VARCHAR2
   * @param  SalesRepId                  IN         VARCHAR2
   * @param  TypeOfBusiness              IN         VARCHAR2
   * @param  ProfileClass                IN         VARCHAR2
   * @param  CreditManager               IN         VARCHAR2
   * @param  CreditClassification        IN         VARCHAR2
   * @param  AccountStatus               IN         VARCHAR2
   * @param  PredominantTrade            IN         VARCHAR2
   * @param  Status                      IN         VARCHAR2
   * @param  BillingAddress              IN         VARCHAR2
   * @param  BillingCity                 IN         VARCHAR2
   * @param  BillingCounty               IN         VARCHAR2
   * @param  BillingState                IN         VARCHAR2
   * @param  BillingZipCode              IN         VARCHAR2
   * @param  BillingCountry              IN         VARCHAR2
   * @param  ShippingAddress             IN         VARCHAR2
   * @param  ShippingCity                IN         VARCHAR2
   * @param  ShippingCounty              IN         VARCHAR2
   * @param  ShippingState               IN         VARCHAR2
   * @param  ShippingZipCode             IN         VARCHAR2
   * @param  ShippingCountry             IN         VARCHAR2
   * @param  YardJobAccountProj          IN         VARCHAR2
   * @param  CreditLimit                 IN         VARCHAR2
   * @param  DefaultJobCreditLimit       IN         VARCHAR2
   * @param  CreditDecisioning           IN         VARCHAR2
   * @param  APContactFirstName          IN         VARCHAR2
   * @param  APContactLastName           IN         VARCHAR2
   * @param  APPhone                     IN         VARCHAR2
   * @param  APEmail                     IN         VARCHAR2
   * @param  InvoiceEmailAddress         IN         VARCHAR2
   * @param  PODEmailAddress             IN         VARCHAR2
   * @param  SOAEmailAddress             IN         VARCHAR2
   * @param  PurchaseOrderRequired       IN         VARCHAR2
   * @param  DUNSNumber                  IN         VARCHAR2
   * @param  x_cust_account_id           OUT NOCOPY NUMBER
   * @param  x_account_number            OUT NOCOPY VARCHAR2
   * @param  x_party_id                  OUT NOCOPY NUMBER
   * @param  x_party_number              OUT NOCOPY VARCHAR2
   * @param  x_profile_id                OUT NOCOPY NUMBER
   * @param  x_return_status             OUT NOCOPY VARCHAR2
   * @param  x_msg_count                 OUT NOCOPY NUMBER
   * @param  x_msg_data                  OUT NOCOPY VARCHAR2
   * @rep:scope public
   * @rep:lifecycle active
   * @rep:displayname Create Customer Account
   * @rep:doccd 120hztig.pdf Customer Account APIs,  Oracle Trading Community Architecture Technical Implementation Guide
   */
   PROCEDURE equifax_create_customer (
      SalesforceId            IN            VARCHAR2,
      CustomerNumber          IN            VARCHAR2,
      BusinessName            IN            VARCHAR2,
      Phone                   IN            NUMBER,
      Fax                     IN            NUMBER,
      EmailAddress            IN            VARCHAR2,
      SalesRepId              IN            VARCHAR2,
      TypeOfBusiness          IN            VARCHAR2,
      ProfileClass            IN            VARCHAR2,
      CreditManager           IN            VARCHAR2,
      CreditClassification    IN            VARCHAR2,
      AccountStatus           IN            VARCHAR2,
      PredominantTrade        IN            VARCHAR2,
      Status                  IN            VARCHAR2,
      BillingAddress          IN            VARCHAR2,
      BillingCity             IN            VARCHAR2,
      BillingCounty           IN            VARCHAR2,
      BillingState            IN            VARCHAR2,
      BillingZipCode          IN            VARCHAR2,
      BillingCountry          IN            VARCHAR2,
      ShippingAddress         IN            VARCHAR2,
      ShippingCity            IN            VARCHAR2,
      ShippingCounty          IN            VARCHAR2,
      ShippingState           IN            VARCHAR2,
      ShippingZipCode         IN            VARCHAR2,
      ShippingCountry         IN            VARCHAR2,
      YardJobAccountProj      IN            VARCHAR2,
      CreditLimit             IN            VARCHAR2,
      DefaultJobCreditLimit   IN            VARCHAR2,
      CreditDecisioning       IN            VARCHAR2,
      APContactFirstName      IN            VARCHAR2,
      APContactLastName       IN            VARCHAR2,
      APPhone                 IN            VARCHAR2,
      APEmail                 IN            VARCHAR2,
      InvoiceEmailAddress     IN            VARCHAR2,
      PODEmailAddress         IN            VARCHAR2,
      SOAEmailAddress         IN            VARCHAR2,
      PurchaseOrderRequired   IN            VARCHAR2,
      DUNSNumber              IN            VARCHAR2,
      x_cust_account_id          OUT NOCOPY NUMBER,
      x_account_number           OUT NOCOPY VARCHAR2,
      x_party_id                 OUT NOCOPY NUMBER,
      x_party_number             OUT NOCOPY VARCHAR2,
      x_profile_id               OUT NOCOPY NUMBER,
      x_return_status            OUT NOCOPY VARCHAR2,
      x_msg_count                OUT NOCOPY NUMBER,
      x_msg_data                 OUT NOCOPY VARCHAR2);
END XXWC_AR_EQUIFAX_CUST_ACCT_PKG;
/