--
-- XXWC_INV_CC_BLOCKOUT_DATES_PKG  (Package) 
--
CREATE OR REPLACE PACKAGE APPS.XXWC_INV_CC_BLOCKOUT_DATES_PKG
AS
   /*************************************************************************
   *  Copyright (c) 2011 Lucidity Consulting Group
   *  All rights reserved.
   **************************************************************************
   *   $Header XXWC_INV_CC_BLOCKOUT_DATES_PKG.pks $
   *   Module Name: XXWC_INV_CC_BLOCKOUT_DATES_PKG.pks
   *
   *   PURPOSE:   This package is called by the concurrent program xxwc cyclecount
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        09/30/2011  Vivek Lakaman             Initial Version
   * ************************************************************************/

   --Define variables for logging debug messages
   g_level_unexpected   CONSTANT NUMBER := 6;
   g_level_error        CONSTANT NUMBER := 5;
   g_level_exception    CONSTANT NUMBER := 4;
   g_LEVEL_EVENT        CONSTANT NUMBER := 3;
   g_LEVEL_PROCEDURE    CONSTANT NUMBER := 2;
   g_LEVEL_STATEMENT    CONSTANT NUMBER := 1;

   PROCEDURE INSERT_ROW (PX_BLOCKOUT_DATE_ID       IN OUT NOCOPY NUMBER
                        ,P_INV_ORG_ID              IN            NUMBER
                        ,P_START_DATE              IN            DATE
                        ,P_END_DATE                IN            DATE
                        ,P_TYPE                    IN            VARCHAR2
                        ,P_OBJECT_VERSION_NUMBER   IN            NUMBER);

   PROCEDURE LOCK_ROW (P_BLOCKOUT_DATE_ID        IN NUMBER
                      ,P_OBJECT_VERSION_NUMBER   IN NUMBER);


   PROCEDURE UPDATE_ROW (P_BLOCKOUT_DATE_ID        IN NUMBER
                        ,P_INV_ORG_ID              IN NUMBER
                        ,P_START_DATE              IN DATE
                        ,P_END_DATE                IN DATE
                        ,P_TYPE                    IN VARCHAR2
                        ,P_OBJECT_VERSION_NUMBER   IN NUMBER);


   PROCEDURE DELETE_ROW (P_BLOCKOUT_DATE_ID IN NUMBER);
END XXWC_INV_CC_BLOCKOUT_DATES_PKG;
/

