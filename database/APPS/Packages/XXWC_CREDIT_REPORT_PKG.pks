CREATE OR REPLACE PACKAGE APPS.XXWC_CREDIT_REPORT_PKG
AS
   /*************************************************************************
     $Header xxwc_credit_report_pkg $
     Module Name: xxwc_credit_report_pkg.pks

     PURPOSE:   This package is to derive Open Balance information for Customers

     REVISIONS:
     Ver        Date        Author             Description
     ---------  ----------  ---------------    -------------------------
     1.0        01/11/2013  Gopi Damuluri      Initial Version
     1.1        02/01/2014  Gopi Damuluri      Added a procedure - CALL_CONC_PROG
   **************************************************************************/

   PROCEDURE create_credit_bureau_file (p_errbuf OUT VARCHAR2, p_retcode OUT VARCHAR2);
   
   FUNCTION total_sales(p_customer_id  IN NUMBER) RETURN NUMBER;

-- Version# 1.1 > Start
   PROCEDURE call_conc_prog (p_errbuf                OUT VARCHAR2
                           , p_retcode               OUT NUMBER
                           , p_user_name             IN  VARCHAR2
                           , p_resp_name             IN  VARCHAR2);
-- Version# 1.1 < End
   
END XXWC_CREDIT_REPORT_PKG;