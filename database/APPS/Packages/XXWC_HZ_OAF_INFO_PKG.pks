CREATE OR REPLACE PACKAGE APPS.XXWC_HZ_OAF_INFO_PKG AS

   /*************************************************************************************************
   *   $Header XXWC_HZ_OAF_INFO_PKG $                                                               *
   *   Module Name: XXWC_HZ_OAF_INFO_PKG                                                            *
   *                                                                                                *
   *   PURPOSE:   This package is used by the Customer Account Site OAF page                        *
   *                                                                                                *
   *   REVISIONS:                                                                                   *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   1.0        01/14/2015  Lee Spitzer                Initial Version 20121217-00636             *
   *                                                         OAF Customer Page Extension            *
   * ************************************************************************************************/

   /*************************************************************************************************   
 
   /*************************************************************************************************   
   *   PROCEDURE party_site_update                                                                  *
   *   Purpose : Procedure is a concurrent program to call the hz_party_site update api             *
   *                  The trigger XXWC_HZ_CUST_SITE_USES_BU will fire when there is a location      *
   *                      update on HZ_CUST_SITE_USES_ALL table and pass the party site id to the   *
   *                        procedure to execute.  No data on hz party site changes but will spawn  *
   *                        the dqm sync concurrent request to populate the HZ_STAGED_PARTY_SITE.   *
   *                        The object version will increment on HZ_CUST_SITES_USES after submitting* 
   *                        this request.                                                           *
   *   Parameters:                                                                                  *
   *      In :    p_party_site_id  -- party_site_id from HZ_PARTY_SITES                             *
   *      OUT :   ERRBUF   VARCHAR2 --Standard Oracle call for concurrent request procedures        *
   *              RETCODE OUT VARCHAR2 --Standrd Oracle call for concurrent reuqest procedures      *
   *                                                                                                *
   *      PROCEDURE party_site_update ( errbuf               OUT VARCHAR2                           *
   *                                  , retcode              OUT VARCHAR2                           *
   *                                  , p_party_site_id      IN NUMBER);                            *
   *                                                                                                *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   1.0        01/21/2015  Lee Spitzer                Initial Version 20121217-00636             *
   *                                                         OAF Customer Page DQM Extension        *
   *************************************************************************************************/
      
  PROCEDURE party_site_update ( errbuf               OUT VARCHAR2
                              , retcode              OUT VARCHAR2
                              , p_party_site_id      IN NUMBER);
END;
/