--
-- XXWC_AR_GETPAID_FILES_V3  (Package) 
--
--  Dependencies: 
--   STANDARD (Package)
--
CREATE OR REPLACE PACKAGE APPS.xxwc_ar_getpaid_files_V3
IS
    /********************************************************************************
    FILE NAME: xxwc_ar_getpaid_files_V3.pkg

    PROGRAM TYPE: PL/SQL Package Body

    PURPOSE: Package to create AREXTI and ARMAST files for GetPaid interfaces

    HISTORY
    ===============================================================================
    VERSION DATE          AUTHOR(S)       DESCRIPTION
    ------- -----------   --------------- -----------------------------------------
    1.0     4/3/2013    Rasikha Galimova    Initial creation of the procedure

    ********************************************************************************/

    --Email Defaults

    /********************************************************************************
    Procedure: create_arexti_file

    PURPOSE: API to create arexti and armast files for GetPaid interfaces, both log
    and data files.

    Replaces the following views:
    TODO: ADD VIEWS

    This is called from the UC4 GetPaid process flow by two jobs, one for each file.
    The process must be followed as:

    create_arexti_file() THEN create_armast_file()

    This is because the first file creates tables and the second reuses them.

    HISTORY
    ===============================================================================
    VERSION DATE          AUTHOR(S)       DESCRIPTION
    ------- -----------   --------------- -----------------------------------------
    1.0      4/3/2013    Rasikha Galimova    Initial creation of the procedure
    1522     5/22/2013  Rasikha              added artran file creation

    ********************************************************************************/

    PROCEDURE create_arexti_file (p_errbuf              OUT VARCHAR2
                                 ,p_retcode             OUT NUMBER
                                 ,p_directory_name          VARCHAR2
                                 ,p_file_name               VARCHAR2
                                 ,p_log_file_name           VARCHAR2
                                 ,p_org_name         IN     VARCHAR2);

    PROCEDURE create_armast_file (p_errbuf              OUT VARCHAR2
                                 ,p_retcode             OUT NUMBER
                                 ,p_directory_name          VARCHAR2
                                 ,p_file_name               VARCHAR2
                                 ,p_log_file_name           VARCHAR2
                                 ,p_org_name         IN     VARCHAR2);

    PROCEDURE populate_armast_fields;

    PROCEDURE armast_file;

    PROCEDURE create_file (p_view_name        IN VARCHAR2
                          ,p_directory_path   IN VARCHAR2
                          ,p_file_name        IN VARCHAR2
                          ,p_org_id           IN NUMBER);

    PROCEDURE create_artran_file (p_errbuf              OUT VARCHAR2
                                 ,p_retcode             OUT NUMBER
                                 ,p_directory_name          VARCHAR2
                                 ,p_file_name               VARCHAR2
                                 ,p_log_file_name           VARCHAR2
                                 ,p_org_name         IN     VARCHAR2
                                 ,p_tuning_factor           NUMBER);
END;                                                                                                     -- Package spec
/
