/* Formatted on 2014/07/22 22:23 (Formatter Plus v4.8.8) */
CREATE OR REPLACE PACKAGE apps.xxwc_cycle_count_sch_all_orgs
AS
/******************************************************************************
   $Header XXWC_CYCLE_COUNT_SCH_ALL_ORGS $
   NAME:       XXWC_CYCLE_COUNT_SCH_ALL_ORGS
   PURPOSE:    Automate Cycle Count Scheduler program to run for all organizations

   REVISIONS:
   Ver        Date        Author            Description
   ---------  ----------  ---------------   ------------------------------------
   1.0        6/24/2014   Dheeresh Chintala Initial Version TMS# 20131120-00056
******************************************************************************/

   /*************************************************************************
     Procedure : Main

     PURPOSE:   This procedure spawns XXWC Cycle Count Scheduler program for each org
     Parameter:

     REVISIONS:
      Ver        Date        Author            Description
      ---------  ----------  ---------------   ------------------------------------
      1.0        6/24/2014   Dheeresh Chintala Initial Version TMS# 20131120-00056

   ************************************************************************/
   PROCEDURE main (retcode OUT NUMBER, errbuf OUT VARCHAR2);
END xxwc_cycle_count_sch_all_orgs;
/