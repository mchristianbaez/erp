CREATE OR REPLACE PACKAGE xxwc_ar_cash_refund_pkg
AS
   /*************************************************************************
   *   $Header xxwc_om_cash_refund_ddl.sql $
   *   Module Name: xxwc OM Cash Refund DDL Creation Script
   *
   *   PURPOSE:   Used in extension Cash Refunds process
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        04/01/2012  Shankar Hariharan             Initial Version
   * ***************************************************************************/
   PROCEDURE cash_refund_process (i_header_id           IN     NUMBER
                                , i_return_header_id    IN     NUMBER
                                , i_cash_receipt_id     IN     NUMBER
                                , i_refund_amount       IN     NUMBER
                                , i_check_refund_amount IN     NUMBER
                                , i_payment_type_code   IN     VARCHAR2
                                , o_return_status          OUT VARCHAR2
                                , o_return_message         OUT VARCHAR2);
                                
PROCEDURE check_rec_in_doubt(p_cash_receipt_id IN NUMBER,
                             x_rec_in_doubt OUT NOCOPY VARCHAR2,
                             x_rid_reason OUT NOCOPY VARCHAR2);   
                             
PROCEDURE launch_auto_invoice(i_sales_order in VARCHAR2, o_return_message out varchar2);                                                          

PROCEDURE launch_auto_remittance (l_cash_receipt_id IN NUMBER,o_return_message out varchar2);

FUNCTION  prepayment_exists (i_header_id in number)
RETURN VARCHAR;
END xxwc_ar_cash_refund_pkg;
/

