CREATE OR REPLACE PACKAGE   XXWC_SHIPPING_NETWORKS_PKG AS

/******************************************************************************
   NAME:       XXWC_SHIPPING_NETWORK_PKG

   PURPOSE:    Create Shipping Networks Concurrent Request when new inventory organizations are enabled
   

   REVISIONS:   
   Ver        Date         Author           Description
   ---------  -----------  ---------------  ------------------------------------
   1.0        18-FEB-2013  Lee Spitzer       1. Create the PL/SQL Package
******************************************************************************/
   --Define variables for logging debug messages
   g_level_unexpected CONSTANT   NUMBER := 6;
   g_level_error CONSTANT        NUMBER := 5;
   g_level_exception CONSTANT    NUMBER := 4;
   g_LEVEL_EVENT CONSTANT        NUMBER := 3;
   g_LEVEL_PROCEDURE CONSTANT    NUMBER := 2;
   g_LEVEL_STATEMENT CONSTANT    NUMBER := 1;
   
   g_package_name CONSTANT       VARCHAR2(30) := 'XXWC_SHIPPING_NETWORKS_PKG';
   g_name VARCHAR2(30);
   g_program_name VARCHAR2(60);
   g_exception EXCEPTION;
   g_user_id NUMBER := FND_GLOBAL.USER_ID;
   g_resp_id NUMBER := FND_GLOBAL.RESP_ID;
   g_resp_appl_id NUMBER := FND_GLOBAL.RESP_APPL_ID;
   g_message VARCHAR2(2000);
   g_count NUMBER DEFAULT 0;
   

PROCEDURE  MAIN_PROGRAM_CCR
              (retbuf                         OUT VARCHAR2
              ,retcode                        OUT NUMBER
              ,p_user_id                      IN NUMBER
              ,p_chart_of_account_id          IN NUMBER
              ,p_intransit_type               IN NUMBER
              ,p_fob_point                    IN NUMBER
              ,p_matl_interorg_transfer_code  IN NUMBER
              ,p_elemental_visibility_enabled IN VARCHAR2
              ,p_manual_receipt_expense       IN VARCHAR2
              ,p_routing_header_id            IN NUMBER
              ,p_transfer_charge              IN NUMBER
              ,p_internal_order_required      IN NUMBER
              ,p_interorg_receivable          IN NUMBER
              ,p_interorg_payable             IN NUMBER
              );

END XXWC_SHIPPING_NETWORKS_PKG;