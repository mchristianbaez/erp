CREATE OR REPLACE PACKAGE APPS.XXWC_EGO_CUSTOMER_CATALOG_PKG
/******************************************************************************************************
-- File Name: XXWC_EGO_CUSTOMER_CATALOG_PKG.pks
--
-- PROGRAM TYPE: PL/SQL Script   <API>
--
-- PURPOSE: Developed to Extract PDH Catalog data for required customer
-- HISTORY
-- ==========================================================================================================
-- ==========================================================================================================
-- VERSION DATE          AUTHOR(S)       DESCRIPTION
-- ------- -----------   --------------- --------------------------------------------------------------------
-- 1.0     17-FEB-2016   P.Vamshidhar   TMS#20160219-00063 - External Customer Catalog Extract
--1.2      19-Aug-2016   P.Vamshidhar   TMS#20160603-00067 - Kiewit Phase 2 - Upload Data to PDH
--1.5      01-Jun-2017   P.Vamshidhar   TMS#20170421-00199 - UPC Validation XXWC B2B Customer Catalog
--1.6      26-APR-2018   Nancy Pahwa    TMS#20160218-00198   AR Writeoff and AR unapply reapply
--1.7      6-June-2018   Nancy Pahwa    TMS#20180308-00301   AR AR_CUSTOMER_INTERFACE 
************************************************************************************************************/
IS
   PROCEDURE Generate_txt (x_err_buf               OUT VARCHAR2,
                           x_ret_code              OUT VARCHAR2,
                           p_export_type        IN     VARCHAR2,
                           p_customer_id        IN     NUMBER,
                           p_customer_catalog   IN     VARCHAR2,
                           p_extract_type       IN     VARCHAR2,
                           p_from_date          IN     VARCHAR2);


   PROCEDURE CUST_CATALOG_STG_EXTRACT (x_err_buf               OUT VARCHAR2,
                                       x_ret_code              OUT VARCHAR2,
                                       p_export_type        IN     VARCHAR2,
                                       p_customer_id        IN     NUMBER,
                                       p_customer_catalog   IN     VARCHAR2,
                                       p_extract_type       IN     VARCHAR2);

   -- Added below Procedures by Vamshi in Rev. 1.2
   -- DATA VALIDATION FUNTIONS --------------------------------------------
   FUNCTION MASTER_DATA_VALIDATION (P_ITEM_NUMBER IN VARCHAR2)
      RETURN VARCHAR2;

   FUNCTION PDH_DATA_VALIDATION (P_ITEM_NUMBER IN VARCHAR2)
      RETURN VARCHAR2;

   FUNCTION validate_date (p_string IN VARCHAR2)
      RETURN DATE;

   FUNCTION Validate_value_set (p_value_set_id   IN NUMBER,
                                p_attr_value     IN VARCHAR)
      RETURN VARCHAR2;



   -- DATA UPDATION FUNCTIONS----------------------------------------------
   PROCEDURE PDH_ATTRIBUTES_UPDATE (xerrbuf             OUT VARCHAR2,
                                    xretcode            OUT VARCHAR2,
                                    p_batch_number   IN     VARCHAR2);
    --1.7 start
   PROCEDURE AR_CUSTOMER_INTERFACE(p_user_NAME    IN VARCHAR2,
                               P_RESP_ID      IN NUMBER,
                               P_RESP_APPL_ID IN NUMBER);
   PROCEDURE AR_INVOICE_INTERFACE(p_user_NAME    IN VARCHAR2,
                               P_RESP_ID      IN NUMBER,
                               P_RESP_APPL_ID IN NUMBER);
   PROCEDURE AR_DEBIT_MEMO_INTERFACE(p_user_NAME    IN VARCHAR2,
                               P_RESP_ID      IN NUMBER,
                               P_RESP_APPL_ID IN NUMBER);
   PROCEDURE AR_LOCKBOX_INTERFACE(p_user_NAME    IN VARCHAR2,
                               P_RESP_ID      IN NUMBER,
                               P_RESP_APPL_ID IN NUMBER);
   PROCEDURE AP_INVOICE_INTERFACE(p_user_NAME    IN VARCHAR2,
                               P_RESP_ID      IN NUMBER,
                               P_RESP_APPL_ID IN NUMBER);
    --1.7 end
   PROCEDURE AR_WRITEOFF_UPDATE(P_BATCH_NUMBER IN NUMBER,
                                P_STATUS IN VARCHAR2,
                               p_user_NAME    IN VARCHAR2,
                               P_RESP_ID      IN NUMBER,
                               P_RESP_APPL_ID IN NUMBER);
   PROCEDURE AR_UNAPPLY_REAPPLY_UPDATE(P_BATCH_NUMBER IN NUMBER,
                             P_STATUS IN VARCHAR2,
                               p_user_NAME    IN VARCHAR2,
                               P_RESP_ID      IN NUMBER,
                               P_RESP_APPL_ID IN NUMBER);
   PROCEDURE PDH_UPDATE (P_BATCH_NUMBER   IN NUMBER,
                         p_user_NAME      IN VARCHAR2,
                         P_RESP_ID        IN NUMBER,
                         P_RESP_APPL_ID   IN NUMBER);

   FUNCTION UPD_BEFORE_PROCESS
      RETURN VARCHAR2;

   FUNCTION MASTER_DATA_UPDATE (P_ITEM_NUMBER   IN     VARCHAR2,
                                X_ERR_MESS         OUT VARCHAR2)
      RETURN VARCHAR2;

   FUNCTION XREF_DATA_UPDATE (P_ITEM_NUMBER    IN     VARCHAR2,
                              X_UPC_ERR_MESS      OUT VARCHAR2,
                              X_VEN_ERR_MESS      OUT VARCHAR2)
      RETURN VARCHAR2;

   FUNCTION PDH_DATA_UPDATE (P_ITEM_NUMBER    IN     VARCHAR2,
                             X_PDH_ERR_MESS      OUT VARCHAR2,
                             X_FUT_ERR_MESS      OUT VARCHAR2)
      RETURN VARCHAR2;

   -- LOGING PROCEDURES-----------------------------------------------------

   PROCEDURE write_log (p_log_msg VARCHAR2);

   PROCEDURE cleanup_log;

   -- Added below procedure in Rev 1.5
   PROCEDURE MTL_UPC_VALIDATION;

END XXWC_EGO_CUSTOMER_CATALOG_PKG;