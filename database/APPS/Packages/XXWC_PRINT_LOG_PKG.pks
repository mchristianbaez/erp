create or replace 
PACKAGE        XXWC_PRINT_LOG_PKG AS

/******************************************************************************
   NAME:       XXWC_PRINT_LOG_PKG

   PURPOSE:    Package to insert concurrent request records into XXWC.XXWC_PRINT_LOG_TBL used for the Print Log Form
   

   REVISIONS:   
   Ver        Date         Author           Description
   ---------  -----------  ---------------  ------------------------------------
   1.0        27-FEB-2013  Lee Spitzer       1. Create the PL/SQL Package
******************************************************************************/
   --Define variables for logging debug messages
   g_level_unexpected CONSTANT   NUMBER := 6;
   g_level_error CONSTANT        NUMBER := 5;
   g_level_exception CONSTANT    NUMBER := 4;
   g_LEVEL_EVENT CONSTANT        NUMBER := 3;
   g_LEVEL_PROCEDURE CONSTANT    NUMBER := 2;
   g_LEVEL_STATEMENT CONSTANT    NUMBER := 1;
   
   g_package_name CONSTANT       VARCHAR2(30) := 'XXWC_SHIPPING_NETWORKS_PKG';
   g_name VARCHAR2(30);
   g_program_name VARCHAR2(60);
   g_exception EXCEPTION;
   g_user_id NUMBER := FND_GLOBAL.USER_ID;
   g_resp_id NUMBER := FND_GLOBAL.RESP_ID;
   g_resp_appl_id NUMBER := FND_GLOBAL.RESP_APPL_ID;
   g_message VARCHAR2(2000);
   g_count NUMBER DEFAULT 0;
   
/*

  PROCEDURE INSERT_CCR
    No parameters required
    This program inserts records into XXWC.XXWC_PRINT_LOG_TBL that currently do not exist
*/
PROCEDURE  INSERT_CCR
              (retbuf                         OUT VARCHAR2
              ,retcode                        OUT NUMBER
              );


/*

  PROCEDURE DELETE_CCR
    No parameters required
    This program delete records into XXWC.XXWC_PRINT_LOG_TBL that currently do not exist
*/
PROCEDURE  DELETE_CCR
              (retbuf                         OUT VARCHAR2
              ,retcode                        OUT NUMBER
              ,p_date                         IN VARCHAR2
              );

END XXWC_PRINT_LOG_PKG;