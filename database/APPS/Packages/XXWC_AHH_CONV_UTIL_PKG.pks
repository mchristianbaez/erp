CREATE OR REPLACE PACKAGE xxwc_ahh_conv_util_pkg IS
  ---
  -- PROCEDURE: Cleanup_Tables, cleans New Line, Tab and Space characters.
  ---
  PROCEDURE cleanup_tables(errbuf     OUT VARCHAR2
                          ,retcode    OUT NUMBER
                          ,p_table1   IN VARCHAR2
                          ,p_table2   IN VARCHAR2
                          ,p_table3   IN VARCHAR2
                          ,p_table4   IN VARCHAR2
                          ,p_table5   IN VARCHAR2
                          ,p_valueset IN VARCHAR2);
END xxwc_ahh_conv_util_pkg;
/
