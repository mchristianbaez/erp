CREATE OR REPLACE package APPS.XXWC_REORDER_POINT_PKG as


/******************************************************************************
   NAME:       XXWC_REORDER_POINT_PKG 

   PURPOSE:    Automate Whitecap Reorder Point Planning for all organizations - TMS Ticket 20121217-00830 
   

   REVISIONS:   
   Ver        Date         Author           Description
   ---------  -----------  ---------------  ------------------------------------
   1.0        23-APR-2013  Lee Spitzer       1. Create the PL/SQL Package
   1.1        30-JUL-2014  Lee Spitzer       2. 20140730-00181  Create wrapper program for requisition Import 
                                                        @SC create wrapper program to run requisition import for all orgs. ... 

******************************************************************************/


/******************************************************************************
   NAME:       MAIN

   PURPOSE:    Main Procedure to launch the White Cap Reorder Point Planning Request for all Orgs Ticket 20121217-00830 
   

   REVISIONS:   
   Ver        Date         Author           Description
   ---------  -----------  ---------------  ------------------------------------
   1.0        23-APR-2013  Lee Spitzer       1. Create the PL/SQL Package
   1.1        30-JUL-2014  Lee Spitzer       2. 20140730-00181  Create wrapper program for requisition Import 
                                                        @SC create wrapper program to run requisition import for all orgs. ... 

******************************************************************************/

  PROCEDURE MAIN(
    retcode               out number
   ,errbuf                out VARCHAR2
   ,p_region              IN VARCHAR2
   ,p_supply_date         IN VARCHAR2
   ,p_demand_date         IN VARCHAR2
   ,p_restock             IN NUMBER
   ,p_update_min_max      IN NUMBER
   ,p_supply_type         IN NUMBER
  );  
  
  
/******************************************************************************
   NAME:       PLANNING_REQUISITION_IMPORT

   PURPOSE:    Launch Requisition Import for All Orgs after Min-Max and Reorder Point Planning
               This will pass the organization_id in the batch id parameter
   

   REVISIONS:   
   Ver        Date         Author           Description
   ---------  -----------  ---------------  ------------------------------------
   1.1        30-JUL-2014  Lee Spitzer       2. 20140730-00181  Create wrapper program for requisition Import 
                                                        @SC create wrapper program to run requisition import for all orgs. ... 

******************************************************************************/
  
  PROCEDURE PLANNING_REQUSITION_IMPORT
   (retcode               OUT NUMBER
   ,errbuf                OUT VARCHAR2
   ,p_supply_type         IN NUMBER
 );


end XXWC_REORDER_POINT_PKG;
/
