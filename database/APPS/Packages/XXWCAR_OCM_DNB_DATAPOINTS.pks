--
-- XXWCAR_OCM_DNB_DATAPOINTS  (Package) 
--
CREATE OR REPLACE PACKAGE APPS.xxwcar_ocm_dnb_datapoints
AS
   /********************************************************************************
   *  Script Name: XXWCAR_OCM_DNB_DATAPOINTS.pks
   *
   *  Description: Package to derive D&B data points.
   *
   *  History:
   *
   *  Version  Date          Author          Description
   *********************************************************************************
   *  1.0      07-Nov-2011   Gopi Damuluri     Initial development.
   *********************************************************************************/

   TYPE get_cr_data_rec IS RECORD
   (
      failure_score_class   NUMBER (30)
     ,                                                               --scoring
      credit_score_class    NUMBER (30)
     ,                                                            --additional
      paydex_current        NUMBER (30)
     ,                                                            --additional
      suit_ind              VARCHAR2 (1)
     ,                                                       --reject override
      lien_ind              VARCHAR2 (1)
     ,                                                       --reject override
      judgement_ind         VARCHAR2 (1)
     ,                                                       --reject override
      suit_judge_ind        VARCHAR2 (30)
     ,                                                       --reject override
      claims_ind            VARCHAR2 (30)
     ,bankruptcy_ind        VARCHAR2 (30)
     ,                                                       --reject override
      high_credit           NUMBER (30)
     ,                                                       --reject override
      avg_high_credit       NUMBER (30)                              --scoring
   );

   TYPE get_org_profile_data_rec IS RECORD
   (
      sic_code           VARCHAR2 (30)
     ,                                                     --approval override
      oob_ind            VARCHAR2 (30)
     ,                                                       --reject override
      YEAR_ESTABLISHED   NUMBER (30)
     ,                                                       --reject override
      total_payments     NUMBER
   );

   FUNCTION xxocm_fin_stress_score_class (
      x_resultout      OUT NOCOPY VARCHAR2
     ,x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN NUMBER;


   FUNCTION xxocm_credit_score_class (x_resultout      OUT NOCOPY VARCHAR2
                                     ,x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN NUMBER;


   FUNCTION xxocm_paydex_current (x_resultout      OUT NOCOPY VARCHAR2
                                 ,x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN NUMBER;


   FUNCTION xxocm_suit_ind (x_resultout      OUT NOCOPY VARCHAR2
                           ,x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN VARCHAR2;

   FUNCTION xxocm_lien_ind (x_resultout      OUT NOCOPY VARCHAR2
                           ,x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN VARCHAR2;


   FUNCTION xxocm_judgement_ind (x_resultout      OUT NOCOPY VARCHAR2
                                ,x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN VARCHAR2;


   FUNCTION xxocm_suit_judge_ind (x_resultout      OUT NOCOPY VARCHAR2
                                 ,x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN VARCHAR2;

   FUNCTION xxocm_claims_ind (x_resultout      OUT NOCOPY VARCHAR2
                             ,x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN VARCHAR2;


   FUNCTION xxocm_bankruptcy_ind (x_resultout      OUT NOCOPY VARCHAR2
                                 ,x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN VARCHAR2;

   FUNCTION xxocm_high_credit (x_resultout      OUT NOCOPY VARCHAR2
                              ,x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN NUMBER;

   FUNCTION xxocm_avg_high_credit (x_resultout      OUT NOCOPY VARCHAR2
                                  ,x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN NUMBER;


   FUNCTION xxocm_num_trade_exp (x_resultout      OUT NOCOPY VARCHAR2
                                ,x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN NUMBER;


   FUNCTION xxocm_sic_code (x_resultout      OUT NOCOPY VARCHAR2
                           ,x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN NUMBER;

   FUNCTION xxocm_oob_ind (x_resultout      OUT NOCOPY VARCHAR2
                          ,x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN VARCHAR2;

   FUNCTION xxocm_year_est (x_resultout      OUT NOCOPY VARCHAR2
                           ,x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN NUMBER;

   FUNCTION xxocm_yib (x_resultout      OUT NOCOPY VARCHAR2
                      ,x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN VARCHAR2;


   FUNCTION xxocm_government_entity (x_resultout      OUT NOCOPY VARCHAR2
                                    ,x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN VARCHAR2;



   FUNCTION xxocm_auto_decline (x_resultout      OUT NOCOPY VARCHAR2
                               ,x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN VARCHAR2;


   FUNCTION xxocm_rec_limit (x_resultout      OUT NOCOPY VARCHAR2
                            ,x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN NUMBER;


   FUNCTION xxocm_rec_credit_class (x_resultout      OUT NOCOPY VARCHAR2
                                   ,x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN VARCHAR2;

   FUNCTION xxocm_rec_skip_approval (x_resultout      OUT NOCOPY VARCHAR2
                                    ,x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN VARCHAR2;


   FUNCTION xxocm_rec_code (x_resultout      OUT NOCOPY VARCHAR2
                           ,x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN NUMBER;
END xxwcar_ocm_dnb_datapoints;
/

