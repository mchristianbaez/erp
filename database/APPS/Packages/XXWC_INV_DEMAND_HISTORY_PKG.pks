CREATE OR REPLACE PACKAGE APPS.XXWC_INV_DEMAND_HISTORY_PKG
IS
   /*************************************************************************
     $Header XXWC_INV_DEMAND_HISTORY_PKG $
     Module Name: XXWC_INV_DEMAND_HISTORY_PKG

     PURPOSE: To update mtl_demand_histories for specific transactions defined
              in custom table xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES.
              Any new Additional transactions types should be inserted into xxwc.xxwc_mtl_demand_history_types
             To avoid to update multiple times table xxwc.XXWC_MTL_DEMAND_HIST_LAST_RUN and xxwc.XXWC_MTL_DEMAND_HISTORY_TXN  will keep history

     REVISIONS:
     Ver        Date        Author                Description
     ---------  ----------  ------------------    ----------------
     1.0        04/29/2014  Rasikha Galimova      Initial Version TMS # 20140203-00283
   **************************************************************************/
   --

   g_distribution_list VARCHAR2(240) := 'HDSOracleDevelopers@hdsupply.com';
   
   PROCEDURE get_demand_history (
      p_period_type            IN  VARCHAR2,
      p_organization_id        IN  VARCHAR2,
      p_specific_category_set  IN  VARCHAR2,
      p_error_msg              OUT CLOB);

   PROCEDURE correct_demand_history(
      p_errbuf               OUT VARCHAR2,
      p_retcode              OUT NUMBER,
      p_organization_id   IN     VARCHAR2 DEFAULT NULL);
      
END;
/