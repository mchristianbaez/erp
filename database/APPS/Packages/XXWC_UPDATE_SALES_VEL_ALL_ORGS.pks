/* Formatted on 2014/07/22 22:33 (Formatter Plus v4.8.8) */
CREATE OR REPLACE PACKAGE apps.xxwc_update_sales_vel_all_orgs
AS
/******************************************************************************
   NAME:       XXWC_UPDATE_SALES_VEL_ALL_ORGS
   PURPOSE:
/*
 -- Date: 24-JUN-2014
 -- Author: Dheeresh Chintala
 -- Scope: This is a wrapper program to run XXWC Calculate and Update Sales Velocity - Program
           for all branches
 --
 -- Update History:
 --
 -- Parameters: None

   REVISIONS:
   Ver        Date        Author            Description
   ---------  ----------  ---------------   ------------------------------------
   1.0        6/24/2014   Dheeresh Chintala Created this package.TMS ticket - 20131120-00056
******************************************************************************/

   /*************************************************************************
        Procedure : Main

        PURPOSE:   This procedure spawns XWC Calculate and Update Sales Velocity - Program for each org
        Parameter: p_region

        REVISIONS:
   Ver        Date        Author            Description
   ---------  ----------  ---------------   ------------------------------------
   1.0        6/24/2014   Dheeresh Chintala Created this package.TMS ticket - 20131120-00056

      ************************************************************************/
   PROCEDURE main (
      retcode    OUT      NUMBER,
      errbuf     OUT      VARCHAR2,
      p_region   IN       VARCHAR2
   );
END xxwc_update_sales_vel_all_orgs;
/