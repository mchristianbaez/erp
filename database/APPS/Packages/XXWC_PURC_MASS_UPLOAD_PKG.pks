CREATE OR REPLACE PACKAGE APPS.xxwc_purc_mass_upload_pkg
IS
/******************************************************************************
   NAME:       xxwc_purc_mass_upload_pkg

   PURPOSE:    Package to upload purchasing data
   
   REVISIONS:   
   Ver        Date         Author           Description
   ---------  -----------  ---------------  ------------------------------------
   1.0        15-Aug-2012  Rasikha Galimova 1. Create the PL/SQL Package
   1.4        02-Oct-2018  P.Vamshidhar      TMS#20180806-00122 - Purchasing Mass 
	                                         upload taking more time these days   
************************************************************************************/

    PROCEDURE process_item_category (p_inventory_item_id                 NUMBER
                                    ,p_organization_id                   NUMBER
                                    ,p_segment1                          VARCHAR
                                    ,p_category_set_name                 VARCHAR2
                                    ,x_return_status          OUT NOCOPY VARCHAR2
                                    ,x_errorcode              OUT NOCOPY NUMBER
                                    ,x_msg_count              OUT NOCOPY NUMBER
                                    ,x_msg_data               OUT NOCOPY VARCHAR2
                                    ,x_rowid                             VARCHAR2);

    PROCEDURE assign_item_sourcing_rule (p_organization_id     IN            NUMBER
                                        ,p_inventory_item_id   IN            NUMBER
                                        ,p_sourcing_rule                     VARCHAR2
                                        ,x_return_status          OUT NOCOPY VARCHAR2
                                        ,x_msg_count              OUT NOCOPY NUMBER
                                        ,x_msg_data               OUT NOCOPY VARCHAR2
                                        ,p_rowid                             VARCHAR2);

    FUNCTION find_item_cat (p_inventory_item_id NUMBER, p_organization_id NUMBER, p_category_set_name VARCHAR2)
        RETURN VARCHAR2;

    FUNCTION get_sourcing_rule (p_inventory_item_id NUMBER, p_organization_id NUMBER)
        RETURN VARCHAR2;

    PROCEDURE process_records;

    PROCEDURE purc_mass_upload_driver (p_org_code          IN VARCHAR2
                                      ,                                                             --used in find query
                                       p_item_number       IN VARCHAR2
                                      ,                                                             --used in find query
                                       p_sourcing_rule     IN VARCHAR2
                                      ,                                                           --update sourcing rule
                                       p_source_org        IN VARCHAR2
                                      ,                                --update item attributes SOURCE_ORGANIZATION_ID--
                                       p_source_type       IN VARCHAR2
                                      ,                                           --update item attributes SOURCE_TYPE--
                                       p_pplt              IN VARCHAR2
                                      ,                               --update item attributes PREPROCESSING_LEAD_TIME--
                                       p_plt               IN VARCHAR2
                                      ,                                        --update item attributes FULL_LEAD_TIME--
                                       p_flm               IN VARCHAR2
                                      ,                                  --update item attributes FIXED_LOT_MULTIPLIER--
                                       p_classification    IN VARCHAR2
                                      ,                               --*** update category_set_name  --'Sales Velocity'
                                       p_planning_method   IN VARCHAR2
                                      ,                               --update item attributes INVENTORY_PLANNING_CODE--
                                       p_min               IN VARCHAR2
                                      ,                                   --update item attributes MIN_MINMAX_QUANTITY--
                                       p_max               IN VARCHAR2
                                      ,                                   --update item attributes MAX_MINMAX_QUANTITY--
                                       p_p_flag            IN VARCHAR2
                                      ,                                --*** update category_set_name  --'Purchase Flag'
                                       p_reserve_stock     IN VARCHAR2
                                      ,                                           --update item attributes attribute12--
                                       p_buyer             IN VARCHAR2
                                      ,                                             --update item attributes  BUYER_ID--
                                       p_amu               IN VARCHAR2);

    PROCEDURE purc_mass_upload_dr_inner (pl_org_code         IN VARCHAR2
                                        ,p_item_number       IN VARCHAR2
                                        ,p_sourcing_rule     IN VARCHAR2
                                        ,p_source_org        IN VARCHAR2
                                        ,p_source_type       IN VARCHAR2
                                        ,p_pplt              IN VARCHAR2
                                        ,p_plt               IN VARCHAR2
                                        ,p_flm               IN VARCHAR2
                                        ,p_classification    IN VARCHAR2
                                        ,p_planning_method   IN VARCHAR2
                                        ,p_min               IN VARCHAR2
                                        ,p_max               IN VARCHAR2
                                        ,p_p_flag            IN VARCHAR2
                                        ,p_reserve_stock     IN VARCHAR2
                                        ,p_buyer             IN VARCHAR2
                                        ,p_amu               IN VARCHAR2
                                        ,p_rowid             IN VARCHAR2);

    PROCEDURE send_error_message (p_unique_string VARCHAR2);
	
	-- Added below procedures in Rev 1.4
    PROCEDURE submit(x_errbuf OUT VARCHAR2, x_retcode OUT varchar2);

    procedure debug (P_LOG_MSG IN VARCHAR2);	
END;
/
