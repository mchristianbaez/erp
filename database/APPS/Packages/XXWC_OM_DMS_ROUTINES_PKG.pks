CREATE OR REPLACE PACKAGE apps.xxwc_om_dms_routines_pkg AS

  /*********************************************************************************
  -- Package XXWC_OM_DMS_ROUTINES_PKG
  -- *******************************************************************************
  --
  -- PURPOSE: Package is for support routines used for DMS Process.
  --
  -- HISTORY
  -- ===========================================================================
  -- ===========================================================================
  -- VERSION DATE          AUTHOR(S)       DESCRIPTION
  -- ------- -----------   --------------- -------------------------------------
  -- 1.0     19-May-2015   Gopi Damuluri   Created this package. TMS 20150518-00036
  *******************************************************************************/

  /********************************************************************************
  -- PROCEDURE: GENERATE_POD
  --
  -- PURPOSE: Procedure to Generate POD files for DocLink
  --
  -- HISTORY
  -- ===========================================================================
  -- ===========================================================================
  -- VERSION DATE          AUTHOR(S)       DESCRIPTION
  -- ------- -----------   --------------- -------------------------------------
  -- 1.0     19-May-2015   Gopi Damuluri   TMS 20150518-00036
  *******************************************************************************/

  PROCEDURE generate_pod(p_errbuf             OUT VARCHAR2
                       , p_retcode            OUT VARCHAR2
                       , p_run_date            IN  VARCHAR2
                       , p_user_name           IN VARCHAR2
                       , p_responsibility_name IN VARCHAR2
                       );
  
  END xxwc_om_dms_routines_pkg;