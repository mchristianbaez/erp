/* Formatted on 11/15/2013 1:09:57 PM (QP5 v5.206) */
-- Start of DDL Script for Package APPS.XXWC_AR_GETPAID_ARCUST_V2
-- Generated 11/15/2013 1:09:56 PM from APPS@EBIZFQA

CREATE OR REPLACE PACKAGE apps.xxwc_ar_getpaid_arcust_v2
IS
    --
    -- To modify this template, edit file PKGSPEC.TXT in TEMPLATE
    -- directory of SQL Navigator
    --
    -- Purpose: Briefly explain the functionality of the package
    --
    -- MODIFICATION HISTORY
    -- Person      Date    Comments
    -- ---------   ------  ------------------------------------------
    --Rasikha Galimova 09/27/2013 will replace getpaid arcust file creation;
    -- Enter package declarations as shown below

    PROCEDURE populate_arcust_fields;

    PROCEDURE create_arcust_file (p_errbuf              OUT VARCHAR2
                                 ,p_retcode             OUT NUMBER
                                 ,p_directory_name          VARCHAR2
                                 ,p_file_name               VARCHAR2
                                 ,p_log_file_name           VARCHAR2
                                 ,p_org_name         IN     VARCHAR2);

    FUNCTION find_parent_party (p_child_party_id NUMBER)
        RETURN VARCHAR2;

    PROCEDURE populate_parent_records;
END;                                                                                                     -- Package spec
/

-- Grants for Package
GRANT EXECUTE ON apps.xxwc_ar_getpaid_arcust_v2 TO interface_xxcus
/

-- End of DDL Script for Package APPS.XXWC_AR_GETPAID_ARCUST_V2
