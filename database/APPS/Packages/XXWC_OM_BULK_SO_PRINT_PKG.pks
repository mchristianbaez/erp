CREATE OR REPLACE PACKAGE APPS.xxwc_om_bulk_so_print_pkg
as
/*************************************************************************
 Copyright (c) 2013 Lucidity Consulting Group
 All rights reserved.
**************************************************************************
  $Header xxwc_om_bulk_so_print_pkg$
  Module Name: xxwc_om_bulk_so_print_pkg

  PURPOSE:   This package is used for salesrep specific Bulk SO Printing

  REVISIONS:
  Ver        Date        Author                     Description
  ---------  ----------  ---------------         -------------------------
  1.0        06/06/2013  Consuelo Gonzalez       Initial Version TMS 20130604-00933
  1.1       1/02/2014    Raghav Velichetti      Modified package to include return order (TMS # 20131230-00151) 
  
**************************************************************************/

procedure   xxwc_om_bulk_so_print (RETCODE              OUT NUMBER
                                   , ERRMSG             OUT VARCHAR2
                                   , P_SALESREP_ID      IN  NUMBER
                                   , P_CREATION_DATE    IN VARCHAR2
                                   , P_DEFAULT_PRINTER  IN VARCHAR2);

end xxwc_om_bulk_so_print_pkg;
/