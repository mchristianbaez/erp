CREATE OR REPLACE PACKAGE APPS.XXWC_INV_LOT_CONV_PKG AS
/*************************************************************************
  $Header XXWC_INV_LOT_CONV_PKG $
  Module Name: XXWC_INV_LOT_CONV_PKG.pks

  PURPOSE: Remove Lot Control from Oralce

  REVISIONS:
  Ver        Date        Author                     Description
  ---------  ----------  ---------------    -------------------------
  1.0        08/16/2012  Gopi Damuluri      Initial Version
**************************************************************************/

   PROCEDURE create_misc_issue (p_item_id IN NUMBER, p_consigned_flag IN NUMBER);
   
   PROCEDURE create_misc_receipt (p_item_id IN NUMBER, p_consigned_flag IN NUMBER);
   
   PROCEDURE update_lot_div_flag (p_item_id IN NUMBER);
   
   PROCEDURE update_rsv_lot_ctrl_flag(p_item_id IN NUMBER);

   PROCEDURE main(p_errbuf           OUT VARCHAR2,
                  p_retcode          OUT VARCHAR2,
                  p_process_flag      IN VARCHAR2,
                  p_item_number       IN VARCHAR2,
                  p_consigned_flag    IN NUMBER);
                  
   FUNCTION check_reservations (p_item_id IN NUMBER) RETURN VARCHAR2;

   PROCEDURE update_born_on_date;
   
END XXWC_INV_LOT_CONV_PKG;
/
