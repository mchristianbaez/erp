--
-- XXWC_ARS_OUTBOUND_PKG  (Package) 
--
CREATE OR REPLACE PACKAGE apps.xxwc_ars_outbound_pkg
IS
 /********************************************************************************
  FILE NAME: XXWCAR_ARS_OUTBOUND_PKG.pkg
  
  PROGRAM TYPE: PL/SQL Package Body
  
  PURPOSE: ARS Outbound Interfaces.
  
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     07/16/2012    Luong Vu        Initial creation 
                                          Branch Cash Account
  1.3     09/17/2014    Maharajan
                        Shunmugam       TMS# 20141001-00153 Canada Multi-org Changes     
  ********************************************************************************/


   -- Author  : LV029752
   -- Created : 16-Jul-2012 1:17:57 AM
   -- Purpose : Handle various tasks related to ARS OB extract

   PROCEDURE create_ars_extract (p_errbuf OUT VARCHAR2, p_retcode OUT NUMBER);

   FUNCTION get_balance (p_site IN VARCHAR2)
      RETURN NUMBER;
 --Added below procedure for ver 1.3
PROCEDURE submit_job(errbuf       OUT  VARCHAR2
                    ,retcode      OUT  NUMBER
  	            ,p_resp_name  IN   VARCHAR2
		    ,p_user_name  IN   VARCHAR2
   		    ,p_org_name   IN   VARCHAR2);

END xxwc_ars_outbound_pkg;
/
GRANT EXECUTE ON apps.xxwc_ars_outbound_pkg TO interface_prism
/
GRANT EXECUTE ON apps.xxwc_ars_outbound_pkg TO interface_xxcus
