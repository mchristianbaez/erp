CREATE OR REPLACE PACKAGE APPS.XXWC_GET_REFUND_ACC_PKG
AS
   /******************************************************************************
       NAME:       XXWC_GET_REFUND_ACC_PKG

       PURPOSE:    To retrieve the refund branch code for Receipt w/o transaction
                   and credit card refund (misc receipt) 

       REVISIONS:
       Ver        Date        Author           Description
       ---------  ----------  ---------------  ------------------------------------
       1.0        12-JUL-13   Shankar Hariharan   1. Created the function
    ******************************************************************************/

   FUNCTION GET_REFUND_BRANCH_CODE (P_REC_APP_ID IN NUMBER)
      RETURN VARCHAR2;

   FUNCTION GET_CC_REFUND_BRANCH_CODE (P_RECEIPT_ID IN NUMBER)
      RETURN VARCHAR2;
END XXWC_GET_REFUND_ACC_PKG;
/