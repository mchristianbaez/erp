CREATE OR REPLACE 
Package           APPS.XXWCMRP_DEMAND_FORECAST_PKG As

/**************************************************************************
 *
 * PROCEDURE
 *  Load_Forecast
 *
 * DESCRIPTION
 *  This procedure collects the user-supplied demand forecast information in the XXWCINV_DEMAND_FORECAST_IFACE table
 *    and submits it thru an API to generate MRP demand forecast information in the base tables
 *
 * PARAMETERS
 * ==========
 * NAME              TYPE     IN/OUT  DESCRIPTION
.* ----------------- -------- ------  ---------------------------------------
 * ERRBUF            VARCHAR2 OUT     completion message
 * RETCODE           NUMBER   OUT     completion code
 *
 *
 * CALLED BY
 *  XXWC Demand Forecast Extension concurrent program
 *
 *************************************************************************/

  Procedure Load_Forecast(errbuf                   OUT VARCHAR2
                         ,retcode                  OUT NUMBER);

/**************************************************************************
 *
 * PROCEDURE
 *  Direct_Upload
 *
 * DESCRIPTION
 *  This procedure is used to upload the user-supplied data from the Excel spreadsheet into the custom interface table
 *
 * PARAMETERS
 * ==========
 * NAME                   TYPE     IN/OUT  DESCRIPTION
.* ---------------------- -------- ------  ---------------------------------------
 * P_ORG_CODE             VARCHAR2 IN      inventory organizatin code
 * P_FORECAST_DESIGNATOR  VARCHAR2 IN      MRP forecast designator
 * P_ITEM_NUMBER          VARCHAR2 IN      inventory item number
 * P_BUCKET_TyPE          VARCHAR2 IN      forecast bucket type  1=Days  2=Weeks  3=Periods
 * P_FORECAST_DATE        DATE     IN      forecast bucket start date
 * P_FORECAST_END_DATE    DATE     IN      forecast bucket end date
 * P_NUMBER_BUCKETS       NUMBER   IN      number of forecast buckets included in date range
 * P_QUANTITY             NUMBER   IN      forecast quantity for the period
 *
 * CALLED BY
 *  XXWC Demand Forecast Integrator during the WebADI upoad process
 *
 *************************************************************************/
  Procedure Direct_Upload(p_org_code               IN  VARCHAR2
                         ,p_forecast_designator    IN  VARCHAR2
                         ,p_item_number            IN  VARCHAR2
                         ,p_bucket_type            IN  VARCHAR2
                         ,p_forecast_date          IN  DATE
                         ,p_forecast_end_date      IN  DATE DEFAULT NULL
                         ,p_number_buckets         IN  NUMBER DEFAULT 1
                         ,p_quantity               IN  NUMBER);

/**************************************************************************
 *
 * PROCEDURE
 *  Generate_Forecast
 *
 * DESCRIPTION
 *  This procedure is used to generate demand forecast based upon historical demand
 *
 * PARAMETERS
 * ==========
 * NAME                   TYPE     IN/OUT  DESCRIPTION
.* ---------------------- -------- ------  ---------------------------------------
 * ERRBUF                 VARCHAR2 OUT     completion message
 * RETCODE                VARCHAR2 OUT     return code
 * P_ORG_ID               NUMBER   IN      inventory organization id
 * P_FORECAST             VARCHAR2 IN      MRP forecast designator
 * P_DC_MODE              VARCHAR2 IN      indicates whether demand history includes sourced org/items
 * P_ITEM_CATEGORY        VARCHAR2 IN      category within "Inventory Category" category set
 * P_ITEM_RANGE           VARCHAR2 IN      indicates whether items limited to forecast or to organization
 * P_ITEM_ID              NUMBER   IN      inventory item id
 * P_PERIOD_TyPE          NUMBER   IN      forecast period type  1=Days  2=Weeks  3=Periods
 * P_START_DATE           DATE     IN      forecast period start date
 * P_FORECAST_PERIODS     NUMBER   IN      number of forecast periods to generate, max 12 periods
 * P_PREV_PERIODS         NUMBER   IN      previous periods to use for average demand
 * P_SEAS_FACTOR1         NUMBER   IN      seasonal factor adjustment for demand historical average for period 1
 *  ......
 * P_SEAS_FACTOR12        NUMBER   IN      seasonal factor adjustment for demand historical average for period 12
 *
 * CALLED BY
 *  XXWC Generate Forecast concurrent program
 *
 *************************************************************************/
  Procedure Generate_Forecast(errbuf            OUT VARCHAR2,
                              retcode           OUT VARCHAR2,
                              p_org_id          IN  NUMBER,
                              p_forecast        IN  VARCHAR2,
                              p_dc_mode         IN  VARCHAR2,
                              p_item_range      IN  VARCHAR2,
                              p_item_category   IN  VARCHAR2,
                              p_item_id         IN  NUMBER DEFAULT NULL,
                              p_period_type     IN  NUMBER DEFAULT 3,   -- 3=PERIODS
                              p_start_date      IN  VARCHAR2,
                              p_forecast_periods IN  NUMBER DEFAULT 12,
                              p_prev_periods    IN  NUMBER DEFAULT 12,
                              p_seas_constant   IN  NUMBER DEFAULT 0, --Only Used Parameters to help default Seas_fact 1 through 12
                              p_seas_factor1    IN  NUMBER DEFAULT 0,
                              p_seas_factor2    IN  NUMBER DEFAULT 0,
                              p_seas_factor3    IN  NUMBER DEFAULT 0,
                              p_seas_factor4    IN  NUMBER DEFAULT 0,
                              p_seas_factor5    IN  NUMBER DEFAULT 0,
                              p_seas_factor6    IN  NUMBER DEFAULT 0,
                              p_seas_factor7    IN  NUMBER DEFAULT 0,
                              p_seas_factor8    IN  NUMBER DEFAULT 0,
                              p_seas_factor9    IN  NUMBER DEFAULT 0,
                              p_seas_factor10   IN  NUMBER DEFAULT 0,
                              p_seas_factor11   IN  NUMBER DEFAULT 0,
                              p_seas_factor12   IN  NUMBER DEFAULT 0);

/**************************************************************************
 *
 * PROCEDURE
 *  Copy_Forecast
 *
 * DESCRIPTION
 *  This procedure is used to generate demand forecast for one item based upon historical demand for another item
 *
 * PARAMETERS
 * ==========
 * NAME                   TYPE     IN/OUT  DESCRIPTION
.* ---------------------- -------- ------  ---------------------------------------
 * ERRBUF                 VARCHAR2 OUT     completion message
 * RETCODE                VARCHAR2 OUT     return code
 * P_ORG_ID               NUMBER   IN      inventory organization id
 * P_FORECAST             VARCHAR2 IN      MRP forecast designator
 * P_DC_MODE              VARCHAR2 IN      indicates whether demand history includes sourced org/items
 * P_SRC_ITEM_ID          NUMBER   IN      source inventory item id
 * P_DEST_ITEM_ID         NUMBER   IN      destination inventory item id
 * P_PERIOD_TyPE          NUMBER   IN      forecast period type  1=Days  2=Weeks  3=Periods
 * P_START_DATE           DATE     IN      forecast period start date
 * P_FORECAST_PERIODS     NUMBER   IN      number of forecast periods to generate
 * P_PREV_PERIODS         NUMBER   IN      previous periods to use for average demand
 * P_SEAS_FACTOR1         NUMBER   IN      seasonal factor adjustment for demand historical average for period 1
 *  ......
 * P_SEAS_FACTOR12        NUMBER   IN      seasonal factor adjustment for demand historical average for period 12
 *
 * CALLED BY
 *  XXWC Generate Forecast Based on Another Item concurrent program
 *
 *************************************************************************/
Procedure Copy_Forecast(errbuf            OUT VARCHAR2,
                        retcode           OUT VARCHAR2,
                        p_org_id          IN  NUMBER,
                        p_forecast        IN  VARCHAR2,
                        p_dc_mode         IN  VARCHAR2,
                        p_src_item_id     IN  NUMBER,
                        p_dest_item_id    IN  NUMBER,
                        p_period_type     IN  NUMBER DEFAULT 3,   -- 3=PERIODS
                        p_start_date      IN  VARCHAR2,
                        p_forecast_periods IN  NUMBER DEFAULT 12,
                        p_prev_periods    IN  NUMBER DEFAULT 12,
                        p_seas_factor1    IN  NUMBER DEFAULT 0,
                        p_seas_factor2    IN  NUMBER DEFAULT 0,
                        p_seas_factor3    IN  NUMBER DEFAULT 0,
                        p_seas_factor4    IN  NUMBER DEFAULT 0,
                        p_seas_factor5    IN  NUMBER DEFAULT 0,
                        p_seas_factor6    IN  NUMBER DEFAULT 0,
                        p_seas_factor7    IN  NUMBER DEFAULT 0,
                        p_seas_factor8    IN  NUMBER DEFAULT 0,
                        p_seas_factor9    IN  NUMBER DEFAULT 0,
                        p_seas_factor10   IN  NUMBER DEFAULT 0,
                        p_seas_factor11   IN  NUMBER DEFAULT 0,
                        p_seas_factor12   IN  NUMBER DEFAULT 0);
                        
Function AMU_FILTER_FACTOR
                       (p_org_id          IN  NUMBER,
                        p_item_id         IN  NUMBER,
                        p_start_date      IN  DATE,
                        p_end_date        IN  DATE,
                        p_dc_mode         IN  VARCHAR2,
                        p_amu             IN NUMBER
                        ) RETURN NUMBER;

End XXWCMRP_DEMAND_FORECAST_PKG;