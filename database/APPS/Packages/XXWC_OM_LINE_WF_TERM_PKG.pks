CREATE OR REPLACE package APPS.xxwc_om_line_wf_term_pkg as
/*
 -- Implementation Date:09/09/2013
 -- Author: Bala Seashadri
 -- Scope: Created this data fix script in refrence to the SR 3-6445032651 
 --Description of this package: When this package gets executed, the workflow gets terminated for the cancelled lines which is stuck in "Ship" Activity
 -- TMS TICKET # 20130909-00647 
  
*/
procedure om_line_wf_term (retcode out varchar2, errbuf out varchar2);

end xxwc_om_line_wf_term_pkg;
/