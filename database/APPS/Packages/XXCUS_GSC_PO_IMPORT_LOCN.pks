CREATE OR REPLACE PACKAGE APPS.xxcus_gsc_po_import_locn as
/*
 -- Author: Balaguru Seshadri
 -- Concurrent Job: HDS GSC PO: Import OPN Locations
 -- Author: Balaguru Seshadri
 -- Scope: This package imports all Oracle Property Manager active locations that belong to a building, floor or office as hr_locations for use
 --        by the HDS GSC Requisition process using iPricurement application. 
 -- ESMS ticket: 
 -- Modification History
 -- ESMS         RFC       Date           Version   Comments
 -- =========== =========  ============   ========  =====================================================================
 -- 264411                 10-DEC-2014    1.0       Initial version
*/
 --
 g_debug varchar2(1) :=Null;
 --
 procedure main 
   (
      retcode          out varchar2
     ,errbuf           out varchar2
   );
 --
end xxcus_gsc_po_import_locn;
/