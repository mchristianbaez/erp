CREATE OR REPLACE PACKAGE APPS.XXWCAR_UPDATE_ARS_JOB_SITE_PKG
AS
   /********************************************************************************

   FILE NAME: XXWCAR_UPDATE_ARS_JOB_SITE_PKG

   PROGRAM TYPE: PL/SQL Package

   PURPOSE: WebADI package to uppload Cusotmer Information

   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)         DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.1     02/10/2014 Maharajan Shunmugam  Initial version.TMS# 20131016-00418




   ********************************************************************************/

   PROCEDURE update_ars_job (p_document_num             IN VARCHAR2,
                             p_rws_number               IN VARCHAR2,
                             p_cli_number               IN VARCHAR2,
                             p_credit_group             IN VARCHAR2,
                             p_date_entered             IN DATE,
                             p_cust_number              IN VARCHAR2,
                             p_cust_name                IN VARCHAR2,
                             p_cust_street              IN VARCHAR2,
                             p_cust_city                IN VARCHAR2,
                             p_cust_state               IN VARCHAR2,
                             p_cust_zip                 IN VARCHAR2,
                             p_cust_phone               IN VARCHAR2,
                             p_gtr_name                 IN VARCHAR2,
                             p_gtr_street               IN VARCHAR2,
                             p_gtr_city                 IN VARCHAR2,
                             p_gtr_state                IN VARCHAR2,
                             p_gtr_zip                  IN VARCHAR2,
                             p_gtr_phone                IN VARCHAR2,
                             p_own_name                 IN VARCHAR2,
                             p_own_street               IN VARCHAR2,
                             p_own_city                 IN VARCHAR2,
                             p_own_state                IN VARCHAR2,
                             p_own_zip                  IN VARCHAR2,
                             p_own_phone                IN VARCHAR2,
                             p_bank_name                IN VARCHAR2,
                             p_bank_street              IN VARCHAR2,
                             p_bank_city                IN VARCHAR2,
                             p_bank_state               IN VARCHAR2,
                             p_bank_zip                 IN VARCHAR2,
                             p_bank_phone               IN VARCHAR2,
                             p_notice_type              IN VARCHAR2,
                             p_bond_number              IN VARCHAR2,
                             p_site_number              IN VARCHAR2,
                             p_site_street              IN VARCHAR2,
                             p_site_city                IN VARCHAR2,
                             p_site_state               IN VARCHAR2,
                             p_site_zip                 IN VARCHAR2,
                             p_site_county              IN VARCHAR2,
                             p_site_project_name        IN VARCHAR2,
                             p_site_legal_description   IN VARCHAR2,
                             p_sup_material_date        IN VARCHAR2,
                             p_est_cost                 IN VARCHAR2,
                             p_completed_date           IN VARCHAR2,
                             p_parties_served_count     IN VARCHAR2,
                             p_an1_type                 IN VARCHAR2,
                             p_an1_name                 IN VARCHAR2,
                             p_an1_branch               IN VARCHAR2,
                             p_an1_street               IN VARCHAR2,
                             p_an1_city                 IN VARCHAR2,
                             p_an1_state                IN VARCHAR2,
                             p_an1_zip                  IN VARCHAR2,
                             p_an1_phone                IN VARCHAR2,
                             p_an2_type                 IN VARCHAR2,
                             p_an2_name                 IN VARCHAR2,
                             p_an2_branch               IN VARCHAR2,
                             p_an2_street               IN VARCHAR2,
                             p_an2_city                 IN VARCHAR2,
                             p_an2_state                IN VARCHAR2,
                             p_an2_zip                  IN VARCHAR2,
                             p_an2_phone                IN VARCHAR2,
                             p_an3_type                 IN VARCHAR2,
                             p_an3_name                 IN VARCHAR2,
                             p_an3_branch               IN VARCHAR2,
                             p_an3_street               IN VARCHAR2,
                             p_an3_city                 IN VARCHAR2,
                             p_an3_state                IN VARCHAR2,
                             p_an3_zip                  IN VARCHAR2,
                             p_an3_phone                IN VARCHAR2);



   PROCEDURE create_contact (p_acc_number      IN     VARCHAR2,
                             p_party_site_id   IN     NUMBER,
                             p_contact_num     IN     VARCHAR2,
                             p_resp_role       IN     VARCHAR2,
                             p_contact_name    IN     VARCHAR2,
                             p_middle_name     IN     VARCHAR2,
                             p_cont_street     IN     VARCHAR2,
                             p_cont_city       IN     VARCHAR2,
                             p_cont_state      IN     VARCHAR2,
                             p_cont_zip        IN     VARCHAR2,
                             p_error_message      OUT VARCHAR2);
END XXWCAR_UPDATE_ARS_JOB_SITE_PKG;
/