--
-- XXWC_MSTR_PARTS_EXCL_LIST_PKG  (Package) 
--
CREATE OR REPLACE PACKAGE APPS.XXWC_MSTR_PARTS_EXCL_LIST_PKG
AS
   /*************************************************************************
   *  Copyright (c) 2011 Lucidity Consulting Group
   *  All rights reserved.
   **************************************************************************
   *   $Header XXWC_MSTR_PARTS_EXCL_LIST_PKG.pks $
   *   Module Name: XXWC_MSTR_PARTS_EXCL_LIST_PKG.pks
   *
   *   PURPOSE:   This package is called by the concurrent program xxwc cyclecount
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        09/30/2011  Vivek Lakaman             Initial Version
   * ************************************************************************/

   --Define variables for logging debug messages
   g_level_unexpected   CONSTANT NUMBER := 6;
   g_level_error        CONSTANT NUMBER := 5;
   g_level_exception    CONSTANT NUMBER := 4;
   g_LEVEL_EVENT        CONSTANT NUMBER := 3;
   g_LEVEL_PROCEDURE    CONSTANT NUMBER := 2;
   g_LEVEL_STATEMENT    CONSTANT NUMBER := 1;

   --Global variable to store the org id
   g_org_id                      NUMBER;    --:= fnd_profile.VALUE ('ORG_ID');

   /*****************************************************************************
   *   Procedure Name: Insert_Row
   *
   *   PURPOSE:   This procedure is the table handler called by the On-Insert Trigger
   * ***************************************************************************/

   PROCEDURE Insert_Row (PX_PARTS_EXCL_LIST_ID     IN OUT NOCOPY NUMBER
                        ,P_ITEM_ID                               NUMBER
                        ,P_ITEM_NUMBER                           VARCHAR2
                        ,P_LAST_UPDATE_DATE                      DATE
                        ,P_LAST_UPDATED_BY                       NUMBER
                        ,P_CREATION_DATE                         DATE
                        ,P_CREATED_BY                            NUMBER
                        ,P_LAST_UPDATE_LOGIN                     NUMBER
                        ,P_OBJECT_VERSION_NUMBER                 NUMBER);

   /*****************************************************************************
   *   Procedure Name: Update_Row
   *
   *   PURPOSE:   This procedure is the table handler called by the On-Update Trigger
   * ***************************************************************************/
   PROCEDURE Update_Row (P_PARTS_EXCL_LIST_ID       NUMBER
                        ,P_ITEM_ID                  NUMBER
                        ,P_ITEM_NUMBER              VARCHAR2
                        ,P_LAST_UPDATE_DATE         DATE
                        ,P_LAST_UPDATED_BY          NUMBER
                        ,P_LAST_UPDATE_LOGIN        NUMBER
                        ,P_OBJECT_VERSION_NUMBER    NUMBER);

   /*****************************************************************************
   *   Procedure Name: Lock_Row
   *
   *   PURPOSE:   This procedure is the table handler called by the On-Lock Trigger
   * ***************************************************************************/

   PROCEDURE Lock_Row (P_PARTS_EXCL_LIST_ID       NUMBER
                      ,p_OBJECT_VERSION_NUMBER    NUMBER);

   /*****************************************************************************
   *   Procedure Name: Delete_Row
   *
   *   PURPOSE:   This procedure is the table handler called by the On-Delete Trigger
   * ***************************************************************************/

   PROCEDURE Delete_Row (P_PARTS_EXCL_LIST_ID NUMBER);
END XXWC_MSTR_PARTS_EXCL_LIST_PKG;
/

