CREATE OR REPLACE PACKAGE APPS.XXWC_QP_MODIFIER_WEB_ADI_PKG
IS
   /*************************************************************************
   *  Copyright (c) 2011 Lucidity Consulting Group
   *  All rights reserved.
   **************************************************************************
   *   $Header xxwc_price_list_web_adi_pkg.pkb $
   *   Module Name: xxwc_price_list_web_adi_pkg.pkb
   *
   *   PURPOSE:   This package is used by the Price List Conversion
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        09/01/2011  Vivek Lakaman             Initial Version
   *   2.1        22-MAR-2013 Satish Upadhyayula        TMS# 20130322-01259 :  Performance Tuning. 
   *                                                    Modified API's Upload and Import 
   *                                                    Upload API and Import API are modified 
   * ***************************************************************************/
   --Define variables for logging debug messages
   G_LEVEL_UNEXPECTED CONSTANT    NUMBER := 6;
   G_LEVEL_ERROR      CONSTANT    NUMBER := 5;
   G_LEVEL_EXCEPTION  CONSTANT    NUMBER := 4;
   G_LEVEL_EVENT      CONSTANT    NUMBER := 3;
   G_LEVEL_PROCEDURE  CONSTANT    NUMBER := 2;
   G_LEVEL_STATEMENT  CONSTANT    NUMBER := 1;

   -- Define variables to capture error messages
   G_RETURN_SUCCESS      CONSTANT    VARCHAR2(1) := 'S' ;
   G_RETURN_ERROR        CONSTANT    VARCHAR2(1) := 'E' ;
   G_RETURN_UNEXP_ERROR  CONSTANT    VARCHAR2(1) := 'U' ;


   g_org_id                      NUMBER;

   G_BATCH_ID                   Number ;
   G_MODULE_NAME                Varchar2(80) := 'XXWC_QP_MODIFIER_WEB_ADI_PKG';


   /*************************************************************************
    *   Procedure : LOG_MSG
    *
    *   PURPOSE:   This procedure is used to log the debug message
    *   Parameter:
    *          IN
    *              p_debug_level    -- Debug Level
    *              p_mod_name       -- Module Name
    *              p_debug_msg      -- Debug Message
    * ************************************************************************/

   PROCEDURE Log_msg (p_debug_level   IN NUMBER,
                      p_mod_name      IN VARCHAR2,
                      p_debug_msg     IN VARCHAR2);

   /*************************************************************************
    *   Procedure : Get_product_attr_value
    *
    *   PURPOSE:   This procedure is used to get the item number or the category
    *   Parameter:
    *          IN
    *              p_product_attribute  -- product_attribute
    *              p_product_attr_value -- product_attr_value
    * ************************************************************************/

   FUNCTION Get_product_attr_value (p_product_attribute    IN VARCHAR2,
                                    p_product_attr_value   IN VARCHAR2)   RETURN VARCHAR2;


   /*************************************************************************
    *   Procedure : Get_Formula_name
    *
    *   PURPOSE:   This procedure is used to get formula name
    *   Parameter:
    *          IN
    *              p_formula_id  -- product_attribute
    * ************************************************************************/

   FUNCTION Get_Formula_name (p_formula_id IN NUMBER)    RETURN VARCHAR2;

  /*************************************************************************
    *   Procedure : VAL_MOD_LIST_NUMBER
    *
    *   PURPOSE:   This procedure is used to Validate Modifer List Name
    *   Parameter:
    *          IN
    *               P_List_Header_ID        --  Modifier List Header Id
    *               P_MODIFIER_LIST_NUMBER  -- Modifer List Name
    * ************************************************************************/

   PROCEDURE VAL_MOD_LIST_NUMBER ( P_List_Header_ID       In  Number,
                          P_MODIFIER_LIST_NUMBER IN  Varchar2,
                          x_Return_Status        OUT Varchar2 );

   /*************************************************************************
    *   Procedure : Get_Lookup_Code
    *
    *   PURPOSE:   This Function will return Lookup Code for given Lookup TYpe and Meaning
    *   Parameter:
    *          IN
    *               p_Lookup_Type       --  Lookup Type
    *              p_Meaning            -- Lookup Meaning
    * ************************************************************************/

   FUNCTION Get_Lookup_Code ( p_Lookup_Type   Varchar2,
                              p_Meaning       Varchar2 ) RETURN Varchar2;



-- SatishU : 30-MAR_2013 :  TO improve performance we will be removing some of the columns 
-- from WEB ADI Template  These columns will have Constant values for new List Lines and 
-- for update we will be using G_MISS_CHAR, G_MISS_NUM, G_MISS_DATE values. 
--  Here are the list of columns that are taken out. 
-- P_LN_ORIG_SYS_LINE_REF, P_LN_LIST_LINE_NO, P_LN_MODIFIER_LEVEL, P_LN_LIST_LINE_TYPE
-- P_LN_AUTOMATIC_FLAG, P_LN_OVERRIDE_FLAG, P_LN_PRODUCT_PRECEDENCE, P_LN_INCLUDE_ON_RETURNS_FLAG
--
-- Following Columns are not removed 
-- P_LN_LIST_LINE_ID, P_PA_PRICING_ATTRIBUTE_ID, P_LN_INCOMPATIBILITY_GRP_CODE

  PROCEDURE UPLOAD (P_LIST_HEADER_ID   NUMBER,
        P_MODIFIER_LIST_NAME           VARCHAR2,
        P_MODIFIER_LIST_NUMBER         VARCHAR2,
        P_CURRENCY_CODE                VARCHAR2,
        P_LIST_TYPE_CODE               VARCHAR2,
        P_START_DATE_ACTIVE            DATE ,
        P_END_DATE_ACTIVE              DATE,
        P_GLOBAL_FLAG                  VARCHAR2,
        P_AUTOMATIC_FLAG               VARCHAR2,
        P_LN_LIST_LINE_ID              NUMBER,
        P_LN_START_DATE_ACTIVE         DATE,
        P_LN_END_DATE_ACTIVE           DATE,
        P_LN_PRICE_BREAK_TYPE_CODE     VARCHAR2,
        P_LN_ARITHMETIC_OPERATOR_TYPE  VARCHAR2,
        P_LN_OPERAND                   NUMBER,
        P_LN_INCOMPATIBILITY_GRP_CODE  VARCHAR2,
        P_LN_FORMULA                   VARCHAR2,
        P_PA_PRICING_ATTRIBUTE_ID      NUMBER ,    
        P_PA_PRODUCT_ATTRIBUTE_CONTEXT VARCHAR2 ,
        P_PA_PRODUCT_ATTRIBUTE         VARCHAR2,
        P_PA_PRODUCT_ATTR_VALUE_ITEM   VARCHAR2,
        P_PA_PRODUCT_ATTR_VALUE_CAT    VARCHAR2 ,
        P_PA_PRODUCT_UOM_CODE          VARCHAR2,
        P_PA_PRICING_ATTRIBUTE         VARCHAR2 ,
        P_PA_COMPARISON_OPERATOR_CODE  VARCHAR2,
        P_PA_PRICING_ATTRIBUTE_CONTEXT VARCHAR2 ,
        P_PA_PRICING_ATTR_VALUE_FROM   VARCHAR2,
        P_PA_PRICING_ATTR_VALUE_TO     VARCHAR2
        ) ; 
        
        -- Prameters that are removed, 
        -- P_LN_ORIG_SYS_LINE_REF         VARCHAR2 Default NULL,
        -- P_LN_LIST_LINE_NO              VARCHAR2 Default NULL,
        -- P_LN_MODIFIER_LEVEL            VARCHAR2 Default NULL,
        -- P_LN_LIST_LINE_TYPE            VARCHAR2 Default NULL, 
        --P_LN_AUTOMATIC_FLAG            VARCHAR2 Default NULL,
        --P_LN_OVERRIDE_FLAG             VARCHAR2 Default NULL,
        --P_LN_PRICING_PHASE             VARCHAR2 Default NULL,
        --P_LN_PRODUCT_PRECEDENCE        NUMBER   Default NULL,
        --P_LN_ACCRUAL_FLAG              VARCHAR2 Default NULL,
        --P_LN_PRICING_GROUP_SEQUENCE    NUMBER   Default NULL,
        --P_LN_INCLUDE_ON_RETURNS_FLAG   VARCHAR2 Default NULL,
        --P_LN_QUALIFICATION_IND         NUMBER   Default NULL,
        --P_PA_ORIG_SYS_PRICING_ATTR_REF VARCHAR2 Default NULL,
         --P_PA_EXCLUDER_FLAG             VARCHAR2 Default NULL,
        --P_PA_ATTRIBUTE_GROUPING_NO     VARCHAR2 Default NULL
      
        --x_Batch_ID               OUT   NUMBER);



   Procedure  Import( Errbuf            OUT NOCOPY VARCHAR2,
                      Retcode           OUT NOCOPY NUMBER,
                      P_Batch_ID        IN   Number ,
                      p_LIST_HEADER_ID  IN   Number  );


   Function Get_Product_Attr_Code (
        p_attribute_name         IN VARCHAR2,
        p_attr_value             IN VARCHAR2 ) Return  Number ;

   Function Get_Product_Attr_Value (
        p_attribute_name         IN VARCHAR2,
        p_attr_Code             IN Number ) Return Varchar2 ;

   Procedure  Bulk_Import( Errbuf            OUT NOCOPY VARCHAR2,
                           Retcode           OUT NOCOPY NUMBER ) ;
                           
  -- Procedure to validate List Lines  values. from Modifier Import Process
  -- Satish U: 20130104-02019  : 03-JAN-2013
                           
   Procedure Validate_list_Lines (p_Header_Id In NUMBER ,
                                  X_Return_Status  OUT Varchar2 ); 
/*************************************************************************
    *   Function: Get_Formula_ID
    *
    *   PURPOSE:   This procedure is used to get formula ID
    *   Parameter:
    *          IN
    *              p_formula_name  
    * ************************************************************************/

   FUNCTION Get_Formula_id (p_formula_name IN VARCHAR2)
      RETURN NUMBER;                                  
                                  

END XXWC_QP_MODIFIER_WEB_ADI_PKG;
/
exit;