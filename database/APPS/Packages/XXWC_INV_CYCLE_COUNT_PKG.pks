CREATE OR REPLACE PACKAGE APPS.XXWC_INV_CYCLE_COUNT_PKG
AS
   /*************************************************************************
   *  Copyright (c) 2011 Lucidity Consulting Group
   *  All rights reserved.
   **************************************************************************
   *   $Header XXWC_CYCLE_COUNT_PKG.pks $
   *   Module Name: XXWC_CYCLE_COUNT_PKG.pks
   *
   *   PURPOSE:   This package is called by the concurrent program xxwc cyclecount
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        09/30/2011  Vivek Lakaman             Initial Version
   *   1.1        07/05/2013  Lee Spitzer               Added Procedure Process PI TMS Ticket - 20130214-01096 Implement Physical Inventory enhancements
                                                        @SC @FIN / Implement Physical Inventory enhancements ...
   *   1.2        10/24/2013  Lee Spitzer               Added Not Exists Clause to PI Procedure TMS Ticket # 20131016-00470 Exclude items assigned to Cat/Class SP.SPPP from PI�s
       1.3        12/18/2013  Lee Spitzer               Update insert_store_locations procedure to only insert active organizations TMS Ticket # 131115-00103
       1.4        12/19/2013  Lee Spitzer               Added Procedure for compiling ABC Compile for all orgs. TMS Ticket # 20131118-00080 Automate the ABC Compiling for cycle counts on a weekly basis @SC / Automate the ABC Compiling for cycle counts on a weekly basis ...
       1.6        06/17/2016  Pattabhi                  TMS#20160708-00169 - Batch improvements for XXWC Cycle Count Scheduler for All Orgs
   * *****************************************************************************************************************************************/

   --Define variables for logging debug messages
   g_level_unexpected   CONSTANT NUMBER := 6;
   g_level_error        CONSTANT NUMBER := 5;
   g_level_exception    CONSTANT NUMBER := 4;
   g_LEVEL_EVENT        CONSTANT NUMBER := 3;
   g_LEVEL_PROCEDURE    CONSTANT NUMBER := 2;
   g_LEVEL_STATEMENT    CONSTANT NUMBER := 1;

   --Global variable to store the org id
   g_org_id                      NUMBER;    --:= fnd_profile.VALUE ('ORG_ID');


   --Define the record type for the excluded parts
   TYPE excl_parts_rec IS RECORD
   (
      inv_item_id   NUMBER,
      inv_org_id    NUMBER,
      item_number   VARCHAR2 (200)
   );

   TYPE excl_parts_tbl IS TABLE OF excl_parts_rec
      INDEX BY BINARY_INTEGER;


   --Define the record type for the excluded Bin Locations
   TYPE excl_bin_rec IS RECORD
   (
      inv_location_id   NUMBER,
      bin               VARCHAR2 (100)
   );

   TYPE excl_bin_tbl IS TABLE OF excl_bin_rec
      INDEX BY BINARY_INTEGER;

   --Define the record type for the cycle count details
   TYPE cyclecount_rec IS RECORD
   (
      cycle_count_header_id   NUMBER,
      inv_org_id              NUMBER
   );

   TYPE cyclecount_tbl IS TABLE OF cyclecount_rec
      INDEX BY BINARY_INTEGER;

   --Define the record type for the store location details
   TYPE store_location_rec IS RECORD
   (
      inv_org_id     NUMBER,
      inv_org_name   VARCHAR2 (100)
   );

   TYPE store_location_tbl IS TABLE OF store_location_rec
      INDEX BY BINARY_INTEGER;


   --Define the record type for the item category
   TYPE category_rec IS RECORD
   (
      category      VARCHAR2 (240),
      category_id   NUMBER,
      inv_org_id    NUMBER
   );

   TYPE category_tbl IS TABLE OF category_rec
      INDEX BY BINARY_INTEGER;

   --Define the record type for the item number
   TYPE item_number_rec IS RECORD
   (
      item_number   VARCHAR2 (240),
      inv_item_id   NUMBER,
      inv_org_id    NUMBER
   );

   TYPE item_number_tbl IS TABLE OF item_number_rec
      INDEX BY BINARY_INTEGER;


   --Define the record type for the store location details
   TYPE store_sub_rec IS RECORD (subinventory VARCHAR2 (10));

   TYPE store_sub_tbl IS TABLE OF store_sub_rec
      INDEX BY BINARY_INTEGER;


   /*************************************************************************
 *   Function/Procedure : insert_store_locations
 *
 *   PURPOSE:   This procedure to insert store location
 *   Parameter:
 *          IN
 * ************************************************************************/
   PROCEDURE INSERT_STORE_LOCATIONS;

   /*************************************************************************
    *   Function/Procedure : Get_Process_Date
    *
    *   PURPOSE:   This procedure to get the process date
    *   Parameter:
    *          IN
    *              p_process_now_flag -- Process Now Flag
    *              p_schedule_date   -- Schedule Date
    *              p_inv_org_id      -- Inventory org
    * ************************************************************************/
   PROCEDURE Get_Process_Date (p_inv_org_id      IN     NUMBER,
                               px_process_date   IN OUT DATE,
                               px_days_to_add    IN OUT NUMBER);

   /*************************************************************************
    *   Function/Procedure : Process_automatic
    *
    *   PURPOSE:   This procedure to process the automatic selection type
    *   Parameter:
    *          IN
    *              p_cycle_count -- Cycle Coutn Name
    *              p_notes -- Notes
    *              p_inv_org_id -- Inv Org/Locations
    *              p_start_bin -- Start Bin
    *              p_end_bin -- End Bin
    *              p_part_num_excl -- Excluded Part Num
    *              p_bin_loc_excl -- Excluded Bin Loc
    *              p_mast_parts_excl_flag -- Include Master parts Exclusion
    *              p_incl_parts_onhand -- Include On Hand items Only Flag
    *              p_schedule_date -- Schedule Date
    *              p_process_now_flag -- Process Now Flag
    *          OUT
    *              x_return_status -- Return Status
    *              x_error_message -- Error Message
    * ************************************************************************/
   PROCEDURE Process_automatic (p_cycle_count            IN     VARCHAR2,
                                p_notes                  IN     VARCHAR2,
                                p_inv_org_id             IN     NUMBER,
                                p_start_bin              IN     VARCHAR2,
                                p_end_bin                IN     VARCHAR2,
                                p_part_num_excl          IN     VARCHAR2,
                                p_bin_loc_excl           IN     VARCHAR2,
                                p_mast_parts_excl_flag   IN     VARCHAR2,
                                p_incl_parts_onhand      IN     VARCHAR2,
                                p_no_specials            IN     VARCHAR2,
                                p_schedule_date          IN     DATE,
                                p_process_now_flag       IN     VARCHAR2,
                                x_num_of_days_to_count      OUT NUMBER,
                                x_return_status             OUT VARCHAR2,
                                x_error_message             OUT VARCHAR2);

   /*************************************************************************
    *   Function/Procedure : Process_Specific
    *
    *   PURPOSE:   This procedure to process the Specific Parts/category selection type
    *   Parameter:
    *          IN
    *              p_cycle_count -- Cycle Coutn Name
    *              p_notes -- Notes
    *              p_store_loc_tbl -- List of Inv Org/Locations
    *              p_type -- Parts/category
    *              p_part_category -- parts/category string
    *              p_schedule_date -- Schedule Date
    *              p_process_now_flag -- Process Now Flag
    *          OUT
    *              x_return_status -- Return Status
    *              x_error_message -- Error Message
    * ************************************************************************/
   PROCEDURE Process_Specific (p_cycle_count        IN     VARCHAR2,
                               p_notes              IN     VARCHAR2,
                               p_store_loc_tbl      IN     store_location_tbl,
                               p_type               IN     VARCHAR2, --P Parts  C Category
                               p_part_category      IN     VARCHAR2,
                               p_schedule_date      IN     DATE,
                               p_process_now_flag   IN     VARCHAR2,
                               x_return_status         OUT VARCHAR2,
                               x_error_message         OUT VARCHAR2);

   /*************************************************************************
    *   Function/Procedure : Calculate_days_to_count
    *
    *   PURPOSE:   This procedure to get the number of days to cycle count
    *   Parameter:
    *          IN
    *              p_cyclecount_name -- Cycle Count Name
    *              p_commit_flag -- Commit Flag
    *              p_inv_org_id -- Inv Org Id
    *              p_start_bin -- Start Bin
    *              p_end_bin -- End Bin
    *          OUT
    *              x_num_of_days_to_count -- Number of Days to Count
    *              x_return_status -- Return Status
    *              x_error_message -- Error Message
    * ************************************************************************/

   PROCEDURE Calculate_days_to_count (
      p_cycle_count            IN     VARCHAR2,
      p_notes                  IN     VARCHAR2,
      p_bin_loc_excl           IN     VARCHAR2,
      p_part_num_excl          IN     VARCHAR2,
      p_incl_parts_onhand      IN     VARCHAR2,
      p_mast_parts_excl_flag   IN     VARCHAR2,
      p_called_from            IN     VARCHAR2,
      p_inv_org_id             IN     NUMBER,
      p_start_bin              IN     VARCHAR2,
      p_end_bin                IN     VARCHAR2,
      p_schedule_date          IN     DATE,
      p_process_now_flag       IN     VARCHAR2,
      x_num_of_days_to_count      OUT NUMBER,
      x_cyclecount_hdr_id         OUT NUMBER,
      x_return_status             OUT VARCHAR2,
      x_error_message             OUT VARCHAR2);



   /*************************************************************************
    *   Function/Procedure : Delete_auto_lines
    *
    *   PURPOSE:   This procedure to delete cycle count Lines and Details
    *   Parameter:
    *          IN
    *              p_cyclecount_line_id -- Cycle Count Line Id
    *          OUT
    *              x_return_status -- Return Status
    *              x_error_msg -- Error Message
    * ************************************************************************/
   PROCEDURE Delete_auto_lines (p_cyclecount_line_id   IN     NUMBER,
                                x_return_status           OUT VARCHAR2,
                                x_error_msg               OUT VARCHAR2);

   /*************************************************************************
    *   Function/Procedure : Delete_Line_Dtls
    *
    *   PURPOSE:   This procedure to delete cycle count Lines Details
    *   Parameter:
    *          IN
    *              p_cyclecount_line_dtl_id -- Cycle Count Line Detail Id
    *          OUT
    *              x_return_status -- Return Status
    *              x_error_msg -- Error Message
    * ************************************************************************/
   PROCEDURE Delete_Line_Dtls (p_cyclecount_line_dtl_id IN NUMBER);

   /*************************************************************************
    *   Function/Procedure : Update_Line_Dtls
    *
    *   PURPOSE:   This procedure to Update  cycle count Lines Details
    *   Parameter:
    *          IN
    *              p_cyclecount_line_dtl_id -- Cycle Count Line Detail Id
    *              p_process_status -- Process Status
    *              p_process_date -- Process Date
    *              p_object_version_number -- Object Version Number
    * ************************************************************************/
   PROCEDURE update_line_dtls (p_cyclecount_line_dtl_id    NUMBER,
                               p_process_status            VARCHAR2,
                               p_process_date              DATE,
                               p_object_version_number     NUMBER);

   /*************************************************************************
    *   Function/Procedure : Delete_Specific_lines
    *
    *   PURPOSE:   This procedure to delete  cycle count specific Lines
    *   Parameter:
    *          IN
    *              p_cc_specific_line_id -- Cycle Count Specific Line Id
    * ************************************************************************/
   PROCEDURE Delete_Specific_lines (p_cc_specific_line_id IN NUMBER);

   /*************************************************************************
    *   Function/Procedure : Submit_Scheduler
    *
    *   PURPOSE:   This procedure to kick off the scheduler
    *   Parameter:
    *          IN
    *              p_inv_org_id -- Inv Org (Optional)
    * ************************************************************************/
   PROCEDURE Submit_Scheduler (errbuf            OUT VARCHAR2,
                               retcode           OUT VARCHAR2,
                               p_inv_org_id   IN     NUMBER);



   /*PROCEDURE Submit (errbuf OUT VARCHAR2, retcode OUT VARCHAR2);*/

   /*************************************************************************
 *   Function/Procedure : Update_Item_id
 *
 *   PURPOSE:   This procedure to update the item Id
 *   Parameter:
 *          OUT
 *              errbuf -- Error Buffer
 *              retcode -- Return Code
 * ************************************************************************/
   PROCEDURE Update_Item_id (errbuf OUT VARCHAR2, retcode OUT VARCHAR2);

   /*************************************************************************
    *   Procedure : Process_PI
    *
    *   PURPOSE:   This procedure used to create the physical inventory counts
    *   Parameters:
    *        p_cycle_count            IN     VARCHAR2,
    *        p_notes                  IN     VARCHAR2,
    *        p_inv_org_id             IN     NUMBER,
    *        p_subinventories         IN     store_sub_tbl,
    *        p_schedule_date          IN     DATE,
    *        p_process_now            IN     VARCHAR2,  --Added TMS Ticket 20130809-01349 - 9/6/2013
    *        x_return_status          OUT    VARCHAR2,
    *        x_error_message          OUT    VARCHAR2);
    *
    * ************************************************************************/

   PROCEDURE Process_PI (p_cycle_count        IN     VARCHAR2,
                         p_notes              IN     VARCHAR2,
                         p_inv_org_id         IN     NUMBER,
                         p_subinventories     IN     store_sub_tbl,
                         p_schedule_date      IN     DATE,
                         p_process_now_flag   IN     VARCHAR2, --Added TMS Ticket 20130809-01349 - 9/6/2013
                         x_return_status         OUT VARCHAR2,
                         x_error_message         OUT VARCHAR2);


   /*
   PROCEDURE INCACI_ALL_ORGS_CCR
                (retbuf                         OUT VARCHAR2
                ,retcode                        OUT NUMBER
                ,p_org_id                       IN NUMBER) --Operating Unit

      Procedure added for TMS Ticket # 20131118-0008
      INCACI_ALL_ORGS_CCR launches Initialize cycle count items for all organizations.  This will recompile the ABC Class and add missing items to the ABC compile.
   */

   PROCEDURE INCACI_ALL_ORGS_CCR (retbuf        OUT VARCHAR2,
                                  retcode       OUT NUMBER,
                                  p_org_id   IN     NUMBER);


   /****************************************************************************************************************************
    *   Procedure : cycle_count_single_submit
    *
    *   PURPOSE:   This procedure used to run the inventory counts for all orgs or passed organization
    *   Parameters:
    *        errbuf            OUT VARCHAR2,
    *        retcode           OUT VARCHAR2,
    *        p_inv_org_id      IN     NUMBER);
    *
    *  1.5   06/17/2016  Pattabhi  TMS#20160708-00169 - Batch improvements for XXWC Cycle Count Scheduler for All Orgs
    * ******************************************************************************************************************/

   PROCEDURE CYCLE_COUNT_SINGLE_SUBMIT (errbuf            OUT VARCHAR2,
                                        retcode           OUT VARCHAR2,
                                        p_inv_org_id   IN     NUMBER);
END XXWC_INV_CYCLE_COUNT_PKG;
/