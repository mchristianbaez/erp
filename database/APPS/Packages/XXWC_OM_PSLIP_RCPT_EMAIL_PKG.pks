CREATE OR REPLACE PACKAGE XXWC_OM_PSLIP_RCPT_EMAIL_PKG
   AUTHID CURRENT_USER
AS
   /********************************************************************************************************************************
      $Header XXWC_OM_PSLIP_RCPT_EMAIL_PKG.PKS $

      PURPOSE:   This package is used to get distinct emails.

      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         ----------------------------------------------------------------------------------------
      1.0        15-Apr-2018 P.Vamshidhar             Initial Version -
                                                      TMS# 20180414-00014- Add logic to remove duplicate emails being sent for SOAs and ASNs

   ******************************************************************************************************************************************/

   PROCEDURE DISTINCT_EMAIL (p_to_email_add   IN     VARCHAR2,
                             p_cc_email_add   IN     VARCHAR2,
                             x_to_email_add      OUT VARCHAR2,
                             x_cc_email_add      OUT VARCHAR2);
							 
END XXWC_OM_PSLIP_RCPT_EMAIL_PKG;
/