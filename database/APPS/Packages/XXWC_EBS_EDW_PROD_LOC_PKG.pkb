CREATE OR REPLACE PACKAGE BODY apps.xxwc_ebs_edw_prod_loc_pkg
--//============================================================================
--//
--// Object Name         :: xxwc_ebs_edw_prod_loc_pkg
--//
--// Object Type         :: Package Body
--//
--// Object Description  :: This is an outbound interface from oracle ebs to edw.
--//                        This interface will populate the base table for VIEW
--//                        xxwc.xxwc_inv_product_loc_ext_vw.
--//
--// Version Control
--//============================================================================
--// Vers    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Mahesh Kudle     15/10/2013    Initial Build - TMS#20130626-01200
--//============================================================================
IS

--//============================================================================
--// Local variable declaration
--//============================================================================
l_errbuf        CLOB;
g_start         NUMBER;

PROCEDURE xxwc_ebs_order_lines_p
--//============================================================================
--//
--// Object Name         :: populate xxwc_ebs_order_lines_p
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This is used to populate oe_order_lines_all data into
--//                        xxwc.xxwc_oe_order_lines_prod_loc table
--//
--// Version Control
--//============================================================================
--// Vers    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Mahesh Kudle     15/10/2013    Initial Build - TMS#20130626-01200
--//============================================================================
IS

l_errbuf   CLOB;
v_found    NUMBER;

BEGIN

  BEGIN
    SELECT 1
    INTO v_found
    FROM all_objects
    WHERE object_name = 'XXWC_OE_ORDER_LINES_PROD_LOC' AND owner = 'XXWC';
    EXCEPTION
      WHEN OTHERS
      THEN
        v_found := NULL;
      END;

      IF v_found IS NULL
      THEN
        EXECUTE IMMEDIATE 'CREATE TABLE xxwc.xxwc_oe_order_lines_prod_loc
                          ( line_id                NUMBER
                          , ship_from_org_id       NUMBER
                          , inventory_item_id      NUMBER
                          , actual_shipment_date   DATE
                          , cancelled_flag         VARCHAR2(12)
                          , order_source_id        NUMBER
                          , trunc_last_update_date DATE)';

        EXECUTE IMMEDIATE 'alter table xxwc.xxwc_oe_order_lines_prod_loc nologging';

        EXECUTE IMMEDIATE 'INSERT /*+APPEND*/
                          INTO   xxwc.xxwc_oe_order_lines_prod_loc
                          SELECT oola.line_id
                          ,      oola.ship_from_org_id
                          ,      oola.inventory_item_id
                          ,      oola.actual_shipment_date
                          ,      oola.cancelled_flag
                          ,      oola.order_source_id
                          ,      TRUNC(last_update_date) trunc_last_update_date
                          FROM   apps.oe_order_lines_all oola';

        COMMIT;

      ELSE
        DBMS_STATS.gather_index_stats (ownname => 'XXWC', indname => 'XXWC_OE_ORDER_LINES_LUD1');

        xxwc_common_tunning_helpers.create_table_from_other_table ('XXWC', 'XXWC_OE_ORDER_LINES_PROD_LOC','XXWC_OE_ORDER_PROD_LOC_TEMP');
        xxwc_common_tunning_helpers.alter_table_temp('XXWC', 'XXWC_OE_ORDER_PROD_LOC_TEMP');

        EXECUTE IMMEDIATE 'INSERT /*+APPEND*/
                          INTO xxwc.xxwc_oe_order_prod_loc_temp
                          SELECT oolp.line_id
                          ,      oolp.ship_from_org_id
                          ,      oolp.inventory_item_id
                          ,      oolp.actual_shipment_date
                          ,      oolp.cancelled_flag
                          ,      oolp.order_source_id
                          ,      oolp.trunc_last_update_date
                          FROM   xxwc.xxwc_oe_order_lines_prod_loc oolp
                          WHERE  oolp.line_id IN
                          (SELECT ool.line_id FROM xxwc.xxwc_oe_order_lines_prod_loc ool
                           MINUS
                           SELECT oola.line_id FROM apps.oe_order_lines_all oola
                           WHERE  TRUNC(oola.last_update_date) >= TRUNC (SYSDATE) - 30)';

        COMMIT;

        EXECUTE IMMEDIATE 'INSERT /*+APPEND*/
                          INTO xxwc.xxwc_oe_order_prod_loc_temp
                          SELECT oola.line_id
                          ,      oola.ship_from_org_id
                          ,      oola.inventory_item_id
                          ,      oola.actual_shipment_date
                          ,      oola.cancelled_flag
                          ,      oola.order_source_id
                          ,      TRUNC(last_update_date)    trunc_last_update_date
                          FROM   apps.oe_order_lines_all    oola
                          WHERE  TRUNC(last_update_date) >= TRUNC(SYSDATE)-30';

        COMMIT;

        xxwc_common_tunning_helpers.drop_temp_table ('XXWC','XXWC_OE_ORDER_LINES_PROD_LOC');

        EXECUTE IMMEDIATE 'CREATE TABLE xxwc.xxwc_oe_order_lines_prod_loc
                          ( line_id                NUMBER
                          , ship_from_org_id       NUMBER
                          , inventory_item_id      NUMBER
                          , actual_shipment_date   DATE
                          , cancelled_flag         VARCHAR2(12)
                          , order_source_id        NUMBER
                          , trunc_last_update_date DATE)';

        EXECUTE IMMEDIATE 'alter table xxwc.xxwc_oe_order_lines_prod_loc nologging';

        EXECUTE IMMEDIATE 'INSERT /*+APPEND*/
                          INTO   xxwc.xxwc_oe_order_lines_prod_loc
                          SELECT ool.line_id
                          ,      ool.ship_from_org_id
                          ,      ool.inventory_item_id
                          ,      ool.actual_shipment_date
                          ,      ool.cancelled_flag
                          ,      ool.order_source_id
                          ,      ool.trunc_last_update_date
                          FROM   xxwc.xxwc_oe_order_prod_loc_temp ool';
        COMMIT;

        xxwc_common_tunning_helpers.drop_temp_table ('XXWC','XXWC_OE_ORDER_PROD_LOC_TEMP');

      END IF;

EXCEPTION
    WHEN OTHERS
    THEN
        l_errbuf :=
               ' Error_Stack...'
            || CHR (10)
            || DBMS_UTILITY.format_error_stack ()
            || CHR (10)
            || ' Error_Backtrace...'
            || CHR (10)
            || DBMS_UTILITY.format_error_backtrace ();

       xxwc_common_tunning_helpers.write_log ('error executing procedure xxwc_ebs_order_lines_p ' || l_errbuf);
        RAISE;
END;

PROCEDURE populate_inv_prod_loc_temp (p_errbuf  OUT VARCHAR2
                                     ,p_retcode OUT NUMBER)
--//============================================================================
--// Procedure Name     :: populate_inv_prod_loc_temp
--//
--// Description        :: This is an outbound interface from oracle ebs to edw.
--//                       This interface will populate the base table for VIEW
--//                       xxwc.xxwc_inv_product_loc_ext_vw.
--//
--// Parameters         :: errbuf   OUT VARCHAR2
--//                       retcode  OUT NUMBER
--//
--// Change Notes
--//----------------------------------------------------------------------------
--// Vers    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Mahesh Kudle     15/10/2013    Initial Build - TMS#20130626-01200
--//============================================================================
IS

BEGIN

  xxwc_common_tunning_helpers.elapsed_time ('EDW_PROD_LOCATION'
                                           ,'Start Product Location'
                                           ,g_start);
  p_retcode := 0;

    --//========================================================================
    --// Create temp table xxwc.xxwc_inv_prod_loc##_x
    --//========================================================================
    BEGIN

      xxwc_common_tunning_helpers.drop_temp_table ('XXWC'
                                                  ,'XXWC_INV_PROD_LOC##_X');

        EXECUTE IMMEDIATE 'CREATE TABLE XXWC.XXWC_INV_PROD_LOC##_X
        ( branch_number           VARCHAR2(250)
        , su_sku                  VARCHAR2(250)
        , interface_date          DATE
        , sku_start_date          DATE
        , prod_status_code        VARCHAR2(250)
        , po_replacement_cost     NUMBER
        , prod_velocity_code      VARCHAR2(250)
        , min_qty                 NUMBER
        , max_qty                 NUMBER
        , forecast_hits           VARCHAR2(250)
        , twelve_month_hits       VARCHAR2(250)
        , price_sheet_effe_date   DATE
        , primary_unit_of_measure VARCHAR2(250)
        , organization_id         NUMBER
        , inventory_item_id       NUMBER
        , source_organization_id  NUMBER
        , buyer_id                NUMBER
        , source_type             NUMBER
        , item_type               VARCHAR2(250)
        , order_lead_days         NUMBER
        , attribute6              VARCHAR2(250)
        , branch_name             VARCHAR2(250)
        , operating_unit_id       NUMBER
        , operating_unit_name     VARCHAR2(250)
        , std_cost_amount         NUMBER
        , buyer_number            VARCHAR2(250))';

         xxwc_common_tunning_helpers.alter_table_temp ('XXWC'
                                                      ,'XXWC_INV_PROD_LOC##_X');


        EXECUTE IMMEDIATE 'INSERT /*+APPEND*/
        INTO   xxwc.xxwc_inv_prod_loc##_x
        SELECT mp.organization_code                                 branch_number
        ,      msib.segment1                                        su_sku
        ,      msib.creation_date                                   interface_date
        ,      msib.creation_date                                   sku_start_date
        ,      msib.inventory_item_status_code                      prod_status_code
        ,      msib.list_price_per_unit                             po_replacement_cost
        ,      msib.attribute12                                     prod_velocity_code
        ,      msib.min_minmax_quantity                             min_qty
        ,      msib.max_minmax_quantity                             max_qty
        ,      msib.attribute13                                     forecast_hits
        ,      msib.attribute15                                     twelve_month_hits
        ,      msib.creation_date                                   price_sheet_effe_date
        ,      msib.primary_unit_of_measure                         primary_unit_of_measure
        ,      msib.organization_id                                 organization_id
        ,      msib.inventory_item_id                               inventory_item_id
        ,      msib.source_organization_id                          source_organization_id
        ,      msib.buyer_id                                        buyer_id
        ,      msib.source_type                                     source_type
        ,      msib.item_type                                       item_type
        ,      (msib.preprocessing_lead_time + msib.full_lead_time) order_lead_days
        ,      mp.attribute6                                        attribute6
        ,      ood.organization_name                                branch_name
        ,      hou.organization_id                                  operating_unit_id
        ,      hou.name                                             operating_unit_name
        ,      apps.cst_cost_api.get_item_cost(''1.0''
                                             , msib.inventory_item_id
                                             , msib.organization_id) std_cost_amount
        ,      papf.employee_number                                 buyer_number
        FROM   apps.mtl_parameters                mp
        ,      apps.mtl_system_items_b            msib
        ,      apps.org_organization_definitions  ood
        ,      apps.hr_operating_units            hou
        ,      apps.per_all_people_f              papf
        WHERE  mp.organization_id               = msib.organization_id
        AND    mp.organization_id               = ood.organization_id
        AND    msib.organization_id             = ood.organization_id
        AND    ood.operating_unit               = hou.organization_id
        AND    msib.buyer_id                    = papf.person_id(+)
        AND    TRUNC (SYSDATE) BETWEEN papf.effective_start_date(+) AND  papf.effective_end_date(+)
        AND    hou.organization_id              = 162';

        COMMIT;

    END;

    xxwc_common_tunning_helpers.elapsed_time ('EDW_PROD_LOCATION'
                                             ,'Create temp table structure'
                                             ,g_start);

    --//========================================================================
    --// Populate stock_indicator
    --//========================================================================
    BEGIN

      xxwc_common_tunning_helpers.drop_temp_table ('XXWC','XXWC_INV_PROD_LOC##_XX');

        EXECUTE IMMEDIATE 'CREATE TABLE xxwc.xxwc_inv_prod_loc##_xx(inventory_item_id NUMBER
                                                                   ,organization_id   NUMBER
                                                                   ,segment1          VARCHAR2(250))';

        xxwc_common_tunning_helpers.alter_table_temp('XXWC', 'XXWC_INV_PROD_LOC##_XX');

        EXECUTE IMMEDIATE 'INSERT /*+APPEND*/
                          INTO   xxwc.xxwc_inv_prod_loc##_xx
                          SELECT mic.inventory_item_id
                          ,      mic.organization_id
                          ,      mic.segment1
                          FROM   apps.mtl_item_categories_v  mic
                          WHERE  mic.category_set_name     = ''Sales Velocity''';

        COMMIT;

        xxwc_common_tunning_helpers.create_table_from_other_table ('XXWC'
                                                                  ,'XXWC_INV_PROD_LOC##_X'
                                                                  ,'XXWC_INV_PROD_LOC##_Y');

        EXECUTE IMMEDIATE 'alter table  XXWC.XXWC_INV_PROD_LOC##_Y add(stock_indicator VARCHAR2(250))';

        EXECUTE IMMEDIATE 'INSERT /*+APPEND*/
                           INTO   xxwc.xxwc_inv_prod_loc##_y
                           SELECT x.*
                           ,      xx.segment1
                           FROM   xxwc.xxwc_inv_prod_loc##_x  x
                           ,      xxwc.xxwc_inv_prod_loc##_xx xx
                           WHERE  x.inventory_item_id       = xx.inventory_item_id(+)
                           AND    x.organization_id         = xx.organization_id(+)';

        COMMIT;

        xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'XXWC_INV_PROD_LOC##_XX');

    END;

    xxwc_common_tunning_helpers.elapsed_time ('EDW_PROD_LOCATION'
                                             ,'populate stock_indicator'
                                             , g_start);

    --//========================================================================
    --// Populate warehouse_branch
    --//========================================================================
    BEGIN

      xxwc_common_tunning_helpers.create_table_from_other_table ('XXWC'
                                                                ,'XXWC_INV_PROD_LOC##_Y'
                                                                ,'XXWC_INV_PROD_LOC##_X');

        EXECUTE IMMEDIATE 'alter table  XXWC.XXWC_INV_PROD_LOC##_X add (warehouse_branch VARCHAR2(250))';

        EXECUTE IMMEDIATE 'INSERT /*+APPEND*/
                           INTO   xxwc.xxwc_inv_prod_loc##_x
                           SELECT y.*
                           ,      mp_source.organization_code
                           FROM   xxwc.xxwc_inv_prod_loc##_y  y
                           ,      apps.mtl_parameters         mp_source
                           WHERE  y.source_organization_id  = mp_source.organization_id(+)';

        COMMIT;

    END;

    xxwc_common_tunning_helpers.elapsed_time ('EDW_PROD_LOCATION'
                                             ,'populate warehouse_branch'
                                             ,g_start);

    --//========================================================================
    --// Populate list_price
    --//========================================================================
    BEGIN

      xxwc_common_tunning_helpers.create_table_from_other_table ('XXWC'
                                                                ,'XXWC_INV_PROD_LOC##_X'
                                                                ,'XXWC_INV_PROD_LOC##_Y');

        EXECUTE IMMEDIATE 'alter table  XXWC.XXWC_INV_PROD_LOC##_Y add (list_price number)';

        EXECUTE IMMEDIATE 'INSERT /*+APPEND*/
                          INTO   xxwc.xxwc_inv_prod_loc##_y
                          SELECT x.*
                          ,      apps.xxwc_mv_routines_pkg.get_item_qp_list_price(x.inventory_item_id) list_price
                          FROM   xxwc.xxwc_inv_prod_loc##_x x';

        COMMIT;
    END;

    xxwc_common_tunning_helpers.elapsed_time ('EDW_PROD_LOCATION'
                                             ,'populate list_price'
                                             ,g_start);

    --//========================================================================
    --// Populate last_counted columns
    --//========================================================================
    BEGIN

      xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'XXWC_INV_PROD_LOC##_YY');
      xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'XXWC_INV_PROD_LOC##_YY1');
      xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'XXWC_INV_PROD_LOC##_YY2');

        EXECUTE IMMEDIATE 'CREATE TABLE xxwc.xxwc_inv_prod_loc##_yy
                           ( organization_id                NUMBER
                           , inventory_item_id              NUMBER
                           , counted_by_employee_id_current NUMBER
                           , last_count_date                DATE
                           , last_count_qty                 NUMBER)';

        xxwc_common_tunning_helpers.alter_table_temp('XXWC', 'XXWC_INV_PROD_LOC##_YY');

        EXECUTE IMMEDIATE 'INSERT /*+APPEND*/
                           INTO    xxwc.xxwc_inv_prod_loc##_yy
                           SELECT  mcce.organization_id
                           ,       mcce.inventory_item_id
                           ,       mcce.counted_by_employee_id_current
                           ,       mcce.count_date_current             last_count_date
                           ,       NVL(mcce.count_quantity_current,0)  last_count_qty
                           FROM    apps.mtl_cycle_count_entries mcce
                           WHERE   (mcce.count_date_current IS NULL
                                     OR mcce.count_date_current =
                                        (SELECT max(mcce2.count_date_current)
                                         FROM   apps.mtl_cycle_count_entries mcce2
                                         WHERE  mcce2.inventory_item_id    = mcce.inventory_item_id
                                         AND    mcce2.organization_id      = mcce.organization_id))
                           ORDER BY mcce.count_date_current DESC';

        COMMIT;

        EXECUTE IMMEDIATE 'CREATE TABLE xxwc.xxwc_inv_prod_loc##_yy1
                          ( organization_id    NUMBER
                          , inventory_item_id  NUMBER
                          , last_counted_by    VARCHAR2(250))';

        xxwc_common_tunning_helpers.alter_table_temp('XXWC', 'XXWC_INV_PROD_LOC##_YY1');

        EXECUTE IMMEDIATE 'INSERT /*+APPEND*/
                          INTO   xxwc.xxwc_inv_prod_loc##_yy1
                          SELECT organization_id
                          ,      inventory_item_id
                          ,      MIN(fu.user_name)            last_counted_by
                          FROM   xxwc.xxwc_inv_prod_loc##_yy  yy
                          ,      apps.per_all_people_f        papf
                          ,      apps.fnd_user                fu
                          WHERE  yy.counted_by_employee_id_current = papf.person_id
                          AND    papf.person_id                    = fu.employee_id
                          GROUP BY  organization_id
                          ,         inventory_item_id';

        COMMIT;

        EXECUTE IMMEDIATE 'CREATE TABLE xxwc.xxwc_inv_prod_loc##_yy2
                          ( organization_id   NUMBER
                          , inventory_item_id NUMBER
                          , last_count_date   DATE)';

        xxwc_common_tunning_helpers.alter_table_temp('XXWC', 'XXWC_INV_PROD_LOC##_YY2');

        EXECUTE IMMEDIATE 'INSERT /*+APPEND*/
                          INTO   xxwc.xxwc_inv_prod_loc##_yy2
                          SELECT organization_id
                          ,      inventory_item_id
                          ,      MAX(last_count_date) last_count_date
                          FROM   xxwc.xxwc_inv_prod_loc##_yy
                          GROUP BY  organization_id
                          ,         inventory_item_id';

        COMMIT;

        EXECUTE IMMEDIATE 'DELETE FROM xxwc.xxwc_inv_prod_loc##_yy
                           WHERE (organization_id,inventory_item_id) IN
                           (SELECT  organization_id,inventory_item_id
                           FROM     xxwc.xxwc_inv_prod_loc##_yy
                           GROUP BY organization_id,inventory_item_id
                           HAVING count(1) > 1)';

        COMMIT;

        xxwc_common_tunning_helpers.create_table_from_other_table ('XXWC'
                                                                  ,'XXWC_INV_PROD_LOC##_Y'
                                                                  ,'XXWC_INV_PROD_LOC##_X');

        EXECUTE IMMEDIATE 'alter table  xxwc.xxwc_inv_prod_loc##_x add (last_count_qty  NUMBER
                                                                       ,last_counted_by VARCHAR2(250)
                                                                       ,last_count_date DATE)';

        EXECUTE IMMEDIATE 'INSERT /*+APPEND*/
                           INTO  xxwc.xxwc_inv_prod_loc##_x
                           SELECT y.*
                           ,      NVL(yy.last_count_qty,0) last_count_qty
                           ,      yy1.last_counted_by
                           ,      yy2.last_count_date
                           FROM   xxwc.xxwc_inv_prod_loc##_y   y
                           ,      xxwc.xxwc_inv_prod_loc##_yy  yy
                           ,      xxwc.xxwc_inv_prod_loc##_yy1 yy1
                           ,      xxwc.xxwc_inv_prod_loc##_yy2 yy2
                           WHERE  y.inventory_item_id        = yy.inventory_item_id(+)
                           AND    y.organization_id          = yy.organization_id(+)
                           AND    y.inventory_item_id        = yy1.inventory_item_id(+)
                           AND    y.organization_id          = yy1.organization_id(+)
                           AND    y.inventory_item_id        = yy2.inventory_item_id(+)
                           AND    y.organization_id          = yy2.organization_id(+)';

        COMMIT;

        xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'XXWC_INV_PROD_LOC##_YY');
        xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'XXWC_INV_PROD_LOC##_YY1');
        xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'XXWC_INV_PROD_LOC##_YY2');

    END;

    xxwc_common_tunning_helpers.elapsed_time ('EDW_PROD_LOCATION'
                                             ,'populate last_counted'
                                             ,g_start);

    --//========================================================================
    --// Calling Procedure xxwc_ebs_order_lines_p
    --//========================================================================
    BEGIN
      xxwc_ebs_order_lines_p;
    END;

    xxwc_common_tunning_helpers.elapsed_time ('EDW_PROD_LOCATION'
                                             ,'populate xxwc_ebs_order_lines_p'
                                             ,g_start);

    --//========================================================================
    --// Populate last_transfer_out_date
    --//========================================================================
    BEGIN

         xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'XXWC_INV_PROD_LOC##_XX');

            EXECUTE IMMEDIATE 'CREATE TABLE xxwc.xxwc_inv_prod_loc##_xx
                   ( ship_from_org_id     NUMBER
                   , inventory_item_id    NUMBER
                   , actual_shipment_date DATE)';

            xxwc_common_tunning_helpers.alter_table_temp ('XXWC', 'XXWC_INV_PROD_LOC##_XX');

            EXECUTE IMMEDIATE 'INSERT /*+APPEND*/
                               INTO   xxwc.xxwc_inv_prod_loc##_xx
                               SELECT ship_from_org_id
                               ,      inventory_item_id
                               ,      MAX (oolp.actual_shipment_date) actual_shipment_date
                               FROM   xxwc.xxwc_oe_order_lines_prod_loc oolp
                               ,      apps.oe_order_sources       oos
                               WHERE  NVL (oolp.cancelled_flag, ''N'') = ''N''
                               AND    oolp.actual_shipment_date IS NOT NULL
                               AND    oolp.order_source_id             = oos.order_source_id
                               AND    oos.NAME                         = ''Internal''
                               GROUP BY ship_from_org_id
                               ,        inventory_item_id';

            COMMIT;

        xxwc_common_tunning_helpers.create_table_from_other_table ('XXWC'
                                                                  ,'XXWC_INV_PROD_LOC##_X'
                                                                  ,'XXWC_INV_PROD_LOC##_Y');

        EXECUTE IMMEDIATE 'alter table  xxwc.xxwc_inv_prod_loc##_y add (last_transfer_out_date DATE)';

        EXECUTE IMMEDIATE 'INSERT /*+APPEND*/
                           INTO   xxwc.xxwc_inv_prod_loc##_y
                           SELECT x.*
                           ,      xx.actual_shipment_date
                           FROM   xxwc.xxwc_inv_prod_loc##_x   x
                           ,      xxwc.xxwc_inv_prod_loc##_xx  xx
                           WHERE  x.inventory_item_id        = xx.inventory_item_id(+)
                           AND    x.organization_id          = xx.ship_from_org_id(+)';

        COMMIT;

        xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'XXWC_INV_PROD_LOC##_XX');

    END;

    --//========================================================================
    --// Print log table xxwc.xxwc_interfaces##log
    --//========================================================================
    xxwc_common_tunning_helpers.elapsed_time ('EDW_PROD_LOCATION'
                                             ,'populate last_transfer_out_date'
                                             ,g_start);

    --//========================================================================
    --// Populate last_po_receipt_date
    --//========================================================================
    BEGIN

      xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'XXWC_INV_PROD_LOC##_YY');

        EXECUTE IMMEDIATE 'CREATE TABLE xxwc.xxwc_inv_prod_loc##_yy
                            ( to_organization_id   NUMBER
                            , item_id              NUMBER
                            , last_po_receipt_date DATE )';

        xxwc_common_tunning_helpers.alter_table_temp('XXWC', 'XXWC_INV_PROD_LOC##_YY');

        EXECUTE IMMEDIATE 'INSERT /*+APPEND*/
                           INTO   xxwc.xxwc_inv_prod_loc##_yy
                           SELECT rsl.to_organization_id
                           ,      rsl.item_id
                           ,      MAX (rt.transaction_date) last_po_receipt_date
                           FROM   apps.rcv_transactions      rt
                           ,      apps.rcv_shipment_lines    rsl
                           WHERE  rt.transaction_type      = ''RECEIVE''
                           AND    rt.source_document_code  = ''PO''
                           AND    rt.shipment_line_id      = rsl.shipment_line_id
                           GROUP BY rsl.to_organization_id
                           ,        rsl.item_id';

        COMMIT;

        xxwc_common_tunning_helpers.create_table_from_other_table ('XXWC'
                                                                  ,'XXWC_INV_PROD_LOC##_Y'
                                                                  ,'XXWC_INV_PROD_LOC##_X');

        EXECUTE IMMEDIATE
            'alter table  XXWC.XXWC_INV_PROD_LOC##_X add ( last_po_receipt_date DATE)';

        EXECUTE IMMEDIATE 'INSERT /*+APPEND*/
                           INTO   xxwc.xxwc_inv_prod_loc##_x
                           SELECT y.*
                           ,      yy.last_po_receipt_date
                           FROM   xxwc.xxwc_inv_prod_loc##_y   y
                           ,      xxwc.xxwc_inv_prod_loc##_yy  yy
                           WHERE  y.inventory_item_id        = yy.item_id(+)
                           AND    y.organization_id          = yy.to_organization_id(+)';

        COMMIT;

      xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'XXWC_INV_PROD_LOC##_YY');

    END;

    xxwc_common_tunning_helpers.elapsed_time ('EDW_PROD_LOCATION'
                                             ,'populate last_po_receipt_date'
                                             ,g_start);

    --//========================================================================
    --// Populate last_sales_date
    --//========================================================================
    BEGIN

      xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'XXWC_INV_PROD_LOC##_XX');

            EXECUTE IMMEDIATE 'CREATE TABLE xxwc.xxwc_inv_prod_loc##_xx
                   ( ship_from_org_id     NUMBER
                   , inventory_item_id    NUMBER
                   , actual_shipment_date DATE)';

            xxwc_common_tunning_helpers.alter_table_temp ('XXWC', 'XXWC_INV_PROD_LOC##_XX');

            EXECUTE IMMEDIATE 'INSERT /*+APPEND*/
                               INTO   xxwc.xxwc_inv_prod_loc##_xx
                               SELECT ship_from_org_id
                               ,      inventory_item_id
                               ,      MAX (oolp.actual_shipment_date)    actual_shipment_date
                               FROM   xxwc.xxwc_oe_order_lines_prod_loc  oolp
                               WHERE  NVL (oolp.cancelled_flag, ''N'') = ''N''
                               AND    oolp.actual_shipment_date IS NOT NULL
                               GROUP BY ship_from_org_id
                               ,        inventory_item_id';

            COMMIT;

        xxwc_common_tunning_helpers.create_table_from_other_table ('XXWC'
                                                 ,'XXWC_INV_PROD_LOC##_X'
                                                 ,'XXWC_INV_PROD_LOC##_Y');

        EXECUTE IMMEDIATE
            'alter table  XXWC.XXWC_INV_PROD_LOC##_Y add ( last_sales_date DATE)';

        EXECUTE IMMEDIATE 'INSERT /*+APPEND*/
                           INTO  xxwc.xxwc_inv_prod_loc##_y
                           SELECT x.*
                           ,      xx.actual_shipment_date
                           FROM   xxwc.xxwc_inv_prod_loc##_x   x
                           ,      xxwc.xxwc_inv_prod_loc##_xx  xx
                           WHERE  x.inventory_item_id        = xx.inventory_item_id(+)
                           AND    x.organization_id          = xx.ship_from_org_id(+)';

        COMMIT;

      xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'XXWC_INV_PROD_LOC##_XX');

    END;

    xxwc_common_tunning_helpers.elapsed_time ('EDW_PROD_LOCATION'
                                             ,'populate last_sales_date'
                                             ,g_start);

    --//========================================================================
    --// Populate last_transfer_in_date
    --//========================================================================
    BEGIN

      xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'XXWC_INV_PROD_LOC##_YY');

        EXECUTE IMMEDIATE 'CREATE TABLE xxwc.xxwc_inv_prod_loc##_yy
                           ( to_organization_id NUMBER
                           , item_id            NUMBER
                           , transaction_date   DATE)';

        xxwc_common_tunning_helpers.alter_table_temp('XXWC', 'XXWC_INV_PROD_LOC##_YY');

        EXECUTE IMMEDIATE 'INSERT /*+APPEND*/
                           INTO   xxwc.xxwc_inv_prod_loc##_yy
                           SELECT rsl.to_organization_id
                           ,      rsl.item_id
                           ,      MAX (rt.transaction_date) transaction_date
                           FROM   apps.rcv_transactions     rt
                           ,      apps.rcv_shipment_lines   rsl
                           WHERE  rt.transaction_type     = ''RECEIVE''
                           AND    rt.source_document_code IN (''REQ'', ''INVENTORY'')
                           AND    rt.shipment_line_id     = rsl.shipment_line_id
                           GROUP BY rsl.to_organization_id
                           ,        rsl.item_id';

        COMMIT;

        xxwc_common_tunning_helpers.create_table_from_other_table ('XXWC'
                                                                  ,'XXWC_INV_PROD_LOC##_Y'
                                                                  ,'XXWC_INV_PROD_LOC##_X');

        EXECUTE IMMEDIATE 'alter table  XXWC.XXWC_INV_PROD_LOC##_X add ( last_transfer_in_date DATE)';

        EXECUTE IMMEDIATE 'INSERT /*+APPEND*/
                           INTO   xxwc.xxwc_inv_prod_loc##_x
                           SELECT y.*
                           ,      yy.transaction_date
                           FROM   xxwc.xxwc_inv_prod_loc##_y  y
                           ,      xxwc.xxwc_inv_prod_loc##_yy yy
                           WHERE  y.inventory_item_id       = yy.item_id(+)
                           AND    y.organization_id         = yy.to_organization_id(+)';

        COMMIT;

        xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'XXWC_INV_PROD_LOC##_YY');

    END;

    xxwc_common_tunning_helpers.elapsed_time ('EDW_PROD_LOCATION'
                                             ,'populate last_transfer_in_date'
                                             ,g_start);

    --//========================================================================
    --// Populate stock_qty
    --//========================================================================
    BEGIN

      xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'XXWC_INV_PROD_LOC##_XX');

        EXECUTE IMMEDIATE 'CREATE TABLE xxwc.xxwc_inv_prod_loc##_xx
                           ( inventory_item_id     NUMBER
                           , organization_id       NUMBER
                           , safety_stock_quantity NUMBER)';

        xxwc_common_tunning_helpers.alter_table_temp('XXWC', 'XXWC_INV_PROD_LOC##_XX');

        EXECUTE IMMEDIATE 'INSERT /*+APPEND*/
                           INTO   xxwc.xxwc_inv_prod_loc##_xx
                           SELECT DISTINCT inventory_item_id
                           ,      mst.organization_id
                           ,      mst.safety_stock_quantity
                           FROM   apps.mtl_safety_stocks_view       mst
                           ,      apps.org_acct_periods             oap
                           ,      apps.org_organization_definitions ood
                           WHERE  ood.organization_id             = oap.organization_id
                           AND    mst.organization_id             = oap.organization_id
                           AND    TRUNC (mst.effectivity_date) BETWEEN oap.period_start_date AND NVL (oap.period_close_date, SYSDATE)
                           AND    TRUNC (SYSDATE) BETWEEN period_start_date AND NVL (oap.period_close_date, SYSDATE)';

        COMMIT;

        xxwc_common_tunning_helpers.create_table_from_other_table ('XXWC'
                                                                  ,'XXWC_INV_PROD_LOC##_X'
                                                                  ,'XXWC_INV_PROD_LOC##_Y');

        EXECUTE IMMEDIATE
            'alter table  xxwc.xxwc_inv_prod_loc##_y add ( stock_qty NUMBER )';

        EXECUTE IMMEDIATE 'INSERT /*+APPEND*/
                           INTO  XXWC.XXWC_INV_PROD_LOC##_Y
                           SELECT x.*
                           ,      NVL(xx.safety_stock_quantity,0)   stock_qty
                           FROM   xxwc.xxwc_inv_prod_loc##_x   x
                           ,      xxwc.xxwc_inv_prod_loc##_xx  xx
                           WHERE  x.inventory_item_id        = xx.inventory_item_id(+)
                           AND    x.organization_id          = xx.organization_id(+)';

        COMMIT;

        xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'XXWC_INV_PROD_LOC##_XX');

    END;

    xxwc_common_tunning_helpers.elapsed_time ('EDW_PROD_LOCATION'
                                             ,'populate stock_qty'
                                             ,g_start);

    --//========================================================================
    --// Populate warehouse_branch_flag
    --//========================================================================
    BEGIN

      xxwc_common_tunning_helpers.create_table_from_other_table ('XXWC'
                                                                ,'XXWC_INV_PROD_LOC##_Y'
                                                                ,'XXWC_INV_PROD_LOC##_X');

        EXECUTE IMMEDIATE
            'alter table  XXWC.XXWC_INV_PROD_LOC##_X add ( warehouse_branch_flag VARCHAR2(250) )';

        EXECUTE IMMEDIATE 'INSERT /*+APPEND*/
                           INTO  xxwc.xxwc_inv_prod_loc##_x
                           SELECT y.*
                           ,      apps.xxwc_mv_routines_pkg.is_item_branch_source (y.inventory_item_id
                                                                                 , y.organization_id)   warehouse_branch_flag
                           FROM   xxwc.xxwc_inv_prod_loc##_y y';

        COMMIT;

    END;

    xxwc_common_tunning_helpers.elapsed_time ('EDW_PROD_LOCATION'
                                             ,'populate warehouse_branch_flag'
                                             ,g_start);

    --//========================================================================
    --// Populate seasonal_flag
    --//========================================================================
    BEGIN

      xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'XXWC_INV_PROD_LOC##_XX');

        EXECUTE IMMEDIATE 'CREATE TABLE xxwc.xxwc_inv_prod_loc##_xx
                           (inventory_item_id NUMBER
                           ,organization_id   NUMBER
                           ,segment1          VARCHAR2(250))';

        xxwc_common_tunning_helpers.alter_table_temp('XXWC', 'XXWC_INV_PROD_LOC##_XX');

         EXECUTE IMMEDIATE 'INSERT /*+APPEND*/
                           INTO   xxwc.xxwc_inv_prod_loc##_xx
                           SELECT mic.inventory_item_id
                           ,      mic.organization_id
                           ,      mic.segment1
                           FROM   apps.mtl_item_categories_v     mic
                           WHERE  mic.category_set_name      = ''Purchase Flag''';

        COMMIT;

        xxwc_common_tunning_helpers.create_table_from_other_table ('XXWC'
                                                                  ,'XXWC_INV_PROD_LOC##_X'
                                                                  ,'XXWC_INV_PROD_LOC##_Y');

        EXECUTE IMMEDIATE 'alter table  XXWC.XXWC_INV_PROD_LOC##_Y add ( seasonal_flag VARCHAR2(250) )';

        EXECUTE IMMEDIATE 'INSERT /*+APPEND*/
                           INTO   xxwc.xxwc_inv_prod_loc##_y
                           SELECT x.*
                           ,      xx.segment1
                           FROM   xxwc.xxwc_inv_prod_loc##_x  x
                           ,      xxwc.xxwc_inv_prod_loc##_xx xx
                           WHERE  x.inventory_item_id       = xx.inventory_item_id(+)
                           AND    x.organization_id         = xx.organization_id(+)';

        COMMIT;

        xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'XXWC_INV_PROD_LOC##_XX');

    END;

    xxwc_common_tunning_helpers.elapsed_time ('EDW_PROD_LOCATION'
                                             ,'populate seasonal_flag'
                                             ,g_start);

    --//========================================================================
    --// Populate primary_bin_location
    --//========================================================================
    BEGIN

      xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'XXWC_INV_PROD_LOC##_YY');

        EXECUTE IMMEDIATE 'CREATE TABLE xxwc.xxwc_inv_prod_loc##_yy
                           (organization_id   NUMBER
                           ,inventory_item_id NUMBER
                           ,segment1          VARCHAR2(250))';

        xxwc_common_tunning_helpers.alter_table_temp('XXWC', 'XXWC_INV_PROD_LOC##_YY');

        EXECUTE IMMEDIATE 'INSERT /*+APPEND*/
                           INTO   xxwc.xxwc_inv_prod_loc##_yy
                           SELECT s.organization_id
                           ,      s.inventory_item_id
                           ,      l.segment1
                           FROM   apps.mtl_secondary_locators s
                           ,      apps.mtl_item_locations     l
                           WHERE  s.secondary_locator       = l.inventory_location_id
                           AND    s.organization_id         = l.organization_id
                           AND    l.segment1 LIKE (TO_CHAR (''1'') || ''-%'')';

        COMMIT;

        xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'XXWC_INV_PROD_LOC##_YY1');

        EXECUTE IMMEDIATE 'CREATE TABLE xxwc.xxwc_inv_prod_loc##_yy1
                           (organization_id   NUMBER
                           ,inventory_item_id NUMBER)';

        xxwc_common_tunning_helpers.alter_table_temp('XXWC', 'XXWC_INV_PROD_LOC##_YY1');

        EXECUTE IMMEDIATE 'INSERT /*+APPEND*/
                           INTO   xxwc.xxwc_inv_prod_loc##_yy1
                           SELECT organization_id
                           ,      inventory_item_id
                           FROM   xxwc.xxwc_inv_prod_loc##_yy
                           GROUP BY organization_id,inventory_item_id
                           HAVING count(1) > 1';

                           COMMIT;

        EXECUTE IMMEDIATE 'DELETE FROM xxwc.xxwc_inv_prod_loc##_yy
                           WHERE (organization_id,inventory_item_id) IN
                           (SELECT  organization_id,inventory_item_id
                           FROM     xxwc.xxwc_inv_prod_loc##_yy
                           GROUP BY organization_id,inventory_item_id
                           HAVING count(1) > 1)';

        COMMIT;

        xxwc_common_tunning_helpers.create_table_from_other_table ('XXWC'
                                                                  ,'XXWC_INV_PROD_LOC##_Y'
                                                                  ,'XXWC_INV_PROD_LOC##_X');

        EXECUTE IMMEDIATE
            'alter table  XXWC.XXWC_INV_PROD_LOC##_X add ( primary_bin_location VARCHAR2(250) )';

        EXECUTE IMMEDIATE 'INSERT /*+APPEND*/
                          INTO   xxwc.xxwc_inv_prod_loc##_x
                          SELECT y.*
                          ,      NVL(yy.segment1,(apps.xxwc_mv_routines_pkg.get_item_dflt_loc (yy1.inventory_item_id
                                                                                             , yy1.organization_id
                                                                                             , NULL
                                                                                             , 1)))
                          FROM   xxwc.xxwc_inv_prod_loc##_y   y
                          ,      xxwc.xxwc_inv_prod_loc##_yy  yy
                          ,      xxwc.xxwc_inv_prod_loc##_yy1 yy1
                          WHERE  y.inventory_item_id        = yy.inventory_item_id(+)
                          AND    y.organization_id          = yy.organization_id(+)
                          AND    y.inventory_item_id        = yy1.inventory_item_id(+)
                          AND    y.organization_id          = yy1.organization_id(+)';

        COMMIT;

        xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'XXWC_INV_PROD_LOC##_YY');
        xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'XXWC_INV_PROD_LOC##_YY1');

    END;

    xxwc_common_tunning_helpers.elapsed_time ('EDW_PROD_LOCATION'
                                             ,'populate primary_bin_location'
                                             ,g_start);

    --//========================================================================
    --// Populate minimum_gross_margin
    --//========================================================================
    BEGIN

      xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'XXWC_INV_PROD_LOC##_XX');
      xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'XXWC_INV_PROD_LOC##_XX1');
      xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'XXWC_INV_PROD_LOC##_XX2');

        EXECUTE IMMEDIATE 'CREATE TABLE xxwc.xxwc_inv_prod_loc##_xx
                           (category_id       NUMBER
                           ,organization_id   NUMBER
                           ,inventory_item_id NUMBER)';

        xxwc_common_tunning_helpers.alter_table_temp('XXWC', 'XXWC_INV_PROD_LOC##_XX');

        EXECUTE IMMEDIATE 'INSERT /*+APPEND*/
                          INTO xxwc.xxwc_inv_prod_loc##_xx
                          SELECT c.category_id
                          ,      c.organization_id
                          ,      c.inventory_item_id
                          FROM   apps.mtl_category_sets_v            a
                          ,      apps.mtl_default_category_sets_fk_v b
                          ,      apps.mtl_item_categories            c
                          WHERE  a.category_set_id                 = b.category_set_id
                          AND    b.functional_area_id              = 1
                          AND    a.category_set_id                 = c.category_set_id';


        COMMIT;

        EXECUTE IMMEDIATE 'CREATE TABLE xxwc.xxwc_inv_prod_loc##_xx1
                           (organization_id   NUMBER
                           ,inventory_item_id NUMBER
                           ,price_zone        VARCHAR2(250)
                           ,min_margin        NUMBER)';

        xxwc_common_tunning_helpers.alter_table_temp('XXWC', 'XXWC_INV_PROD_LOC##_XX1');

        EXECUTE IMMEDIATE 'INSERT /*+APPEND*/
                          INTO xxwc.xxwc_inv_prod_loc##_xx1
                          SELECT xx.organization_id
                          ,      xx.inventory_item_id
                          ,      y.price_zone
                          ,      y.min_margin
                          FROM   xxwc.xxwc_inv_prod_loc##_xx xx
                          ,      xxwc_om_pricing_guardrail   y
                          WHERE  xx.category_id            = y.item_category_id
                          AND    status                    = ''CURRENT''
                          AND    SYSDATE BETWEEN start_date AND NVL (end_date , SYSDATE + 1)';

        COMMIT;

        EXECUTE IMMEDIATE 'CREATE TABLE xxwc.xxwc_inv_prod_loc##_xx2
                           (inventory_item_id NUMBER
                           ,price_zone        VARCHAR2(250)
                           ,min_margin        NUMBER)';

        xxwc_common_tunning_helpers.alter_table_temp('XXWC', 'XXWC_INV_PROD_LOC##_XX2');

        EXECUTE IMMEDIATE 'INSERT /*+APPEND*/
                          INTO   xxwc.xxwc_inv_prod_loc##_xx2
                          SELECT inventory_item_id
                          ,      price_zone
                          ,      min_margin
                          FROM   XXWC_OM_PRICING_GUARDRAIL
                          WHERE  status                    = ''CURRENT''
                          AND    SYSDATE BETWEEN start_date AND NVL (end_date , SYSDATE + 1)';

                          COMMIT;

        xxwc_common_tunning_helpers.create_table_from_other_table ('XXWC'
                                                                  ,'XXWC_INV_PROD_LOC##_X'
                                                                  ,'XXWC_INV_PROD_LOC##_Y');

        EXECUTE IMMEDIATE
            'alter table  XXWC.XXWC_INV_PROD_LOC##_Y add ( minimum_gross_margin NUMBER )';

        EXECUTE IMMEDIATE 'INSERT /*+APPEND*/
                           INTO   XXWC.XXWC_INV_PROD_LOC##_Y
                           SELECT x.*
                           ,      NVL(xx2.min_margin,xx1.min_margin)
                           FROM   xxwc.xxwc_inv_prod_loc##_x   x
                           ,      xxwc.xxwc_inv_prod_loc##_xx1 xx1
                           ,      xxwc.xxwc_inv_prod_loc##_xx2 xx2
                           WHERE  x.inventory_item_id        = xx1.inventory_item_id(+)
                           AND    x.organization_id          = xx1.organization_id(+)
                           AND    x.attribute6               = xx1.price_zone(+)
                           AND    x.inventory_item_id        = xx2.inventory_item_id(+)
                           AND    x.attribute6               = xx2.price_zone(+)';

        COMMIT;

        xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'XXWC_INV_PROD_LOC##_XX');
        xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'XXWC_INV_PROD_LOC##_XX1');
        xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'XXWC_INV_PROD_LOC##_XX2');

    END;

    xxwc_common_tunning_helpers.elapsed_time ('EDW_PROD_LOCATION'
                                             ,'populate minimum_gross_margin'
                                             ,g_start);

    BEGIN

      xxwc_common_tunning_helpers.create_table_from_other_table ('XXWC'
                                                                ,'XXWC_INV_PROD_LOC##_Y'
                                                                ,'XXWC_INV_PROD_LOC##_X');

        EXECUTE IMMEDIATE
            'alter table  XXWC.XXWC_INV_PROD_LOC##_X add ( part_vendor_code VARCHAR2(250) )';

        EXECUTE IMMEDIATE 'INSERT /*+APPEND*/
                          INTO  XXWC.XXWC_INV_PROD_LOC##_X
                          SELECT y.*
                          ,      xxwc_mv_routines_pkg.get_default_supplier (
                                    y.inventory_item_id
                                  , (CASE
                                        WHEN y.source_type = 1
                                             AND y.source_organization_id IS NOT NULL
                                        THEN
                                           y.source_organization_id
                                        ELSE
                                           y.organization_id
                                     END)
                                  , ''IDENTIFIER''
                                 ) segment1
                          FROM   xxwc.xxwc_inv_prod_loc##_y y';

        COMMIT;

    END;

    xxwc_common_tunning_helpers.elapsed_time ('EDW_PROD_LOCATION'
                                             ,'populate part_vendor_code'
                                             ,g_start);

    --//========================================================================
    --// Populate source_sup_prim_pay_site
    --//========================================================================
    BEGIN

      xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'XXWC_INV_PROD_LOC##_XX');
      xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'XXWC_INV_PROD_LOC##_XX1');

        EXECUTE IMMEDIATE 'CREATE TABLE xxwc.xxwc_inv_prod_loc##_xx
                           ( org_id                  NUMBER
                           , ship_to_organization_id NUMBER
                           , item_id                 NUMBER
                           , po_header_id            NUMBER)';

        xxwc_common_tunning_helpers.alter_table_temp('XXWC', 'XXWC_INV_PROD_LOC##_XX');

        EXECUTE IMMEDIATE 'INSERT /*+APPEND*/
                           INTO  xxwc.xxwc_inv_prod_loc##_xx
                           SELECT pla.org_id
                           ,      plla.ship_to_organization_id
                           ,      pla.item_id
                           ,      MAX(pla.po_header_id)          po_header_id
                           FROM   apps.po_lines_all              pla
                           ,      apps.po_line_locations_all     plla
                           WHERE  NVL(pla.cancel_flag, ''N'')  = ''N''
                           AND    pla.org_id                   = 162
                           AND    pla.po_line_id               = plla.po_line_id
                           AND    NVL(plla.cancel_flag, ''N'') = ''N''
                           GROUP BY pla.org_id
                           ,        plla.ship_to_organization_id
                           ,        pla.item_id';

        COMMIT;

        EXECUTE IMMEDIATE 'CREATE TABLE xxwc.xxwc_inv_prod_loc##_xx1
                           ( org_id                  NUMBER
                           , ship_to_organization_id NUMBER
                           , item_id                 NUMBER
                           , pay_site_code           VARCHAR2(250))';

        xxwc_common_tunning_helpers.alter_table_temp('XXWC', 'XXWC_INV_PROD_LOC##_XX1');

        EXECUTE IMMEDIATE 'INSERT /*+APPEND*/
                           INTO  xxwc.xxwc_inv_prod_loc##_xx1
                           SELECT xx.org_id
                           ,      xx.ship_to_organization_id
                           ,      xx.item_id
                           ,      (asa.segment1 || ''-'' || assa.vendor_site_id) pay_site_code
                           FROM   apps.po_headers_all              pha
                           ,      apps.ap_suppliers                asa
                           ,      apps.ap_supplier_sites_all       assa
                           ,      xxwc.xxwc_inv_prod_loc##_xx      xx
                           WHERE  pha.po_header_id               = xx.po_header_id
                           AND    pha.vendor_id                  = asa.vendor_id
                           AND    asa.vendor_id                  = assa.vendor_id
                           AND    assa.org_id                    = xx.org_id
                           AND    NVL(assa.pay_site_flag, ''N'') = ''Y''
                           ORDER BY assa.primary_pay_site_flag DESC';

        COMMIT;

        xxwc_common_tunning_helpers.create_table_from_other_table ('XXWC'
                                                 ,'XXWC_INV_PROD_LOC##_X'
                                                 ,'XXWC_INV_PROD_LOC##_Y');

        EXECUTE IMMEDIATE
            'alter table  XXWC.XXWC_INV_PROD_LOC##_Y add ( source_sup_prim_pay_site VARCHAR2(250) )';

        EXECUTE IMMEDIATE 'INSERT /*+APPEND*/
                           INTO  xxwc.xxwc_inv_prod_loc##_y
                           SELECT x.*,(CASE WHEN x.item_type = ''SPECIAL'' OR x.su_sku like ''SP/%''
                                            THEN xx1.pay_site_code
                                       ELSE SUBSTR ( apps.xxwc_mv_routines_pkg.get_edw_supp_prim_pay_site (x.part_vendor_code
                                                                                                         , x.operating_unit_id) , 1 , 80)
                                       END ) source_sup_prim_pay_site
                           FROM xxwc.xxwc_inv_prod_loc##_x    x
                           ,    xxwc.xxwc_inv_prod_loc##_xx1  xx1
                           where x.inventory_item_id        = xx1.item_id(+)
                           and x.organization_id            = xx1.ship_to_organization_id(+)
                           and x.operating_unit_id          = xx1.org_id(+)';

        COMMIT;

        xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'XXWC_INV_PROD_LOC##_XX');
        xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'XXWC_INV_PROD_LOC##_XX1');

    END;

    xxwc_common_tunning_helpers.elapsed_time ('EDW_PROD_LOCATION'
                                             ,'populate source_sup_prim_pay_site'
                                             ,g_start);

    --//========================================================================
    --// Create Final Temp table staructure for base view
    --//========================================================================
    BEGIN

      xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'XXWC_INV_PROD_LOC##_Z');

        EXECUTE IMMEDIATE 'CREATE TABLE XXWC.XXWC_INV_PROD_LOC##_Z
           ( BUSINESS_UNIT                  VARCHAR2 (250 BYTE)
            ,SOURCE_SYSTEM                  VARCHAR2 (250 BYTE)
            ,OPERATING_UNIT_ID              NUMBER
            ,OPERATING_UNIT_NAME            VARCHAR2 (250 BYTE)
            ,INTERFACE_DATE                 DATE
            ,BRANCH_NUMBER                  VARCHAR2 (250 BYTE)
            ,BRANCH_NAME                    VARCHAR2 (250 BYTE)
            ,SU_SKU                         VARCHAR2 (250 BYTE)
            ,SKU_START_DATE                 DATE
            ,STOCK_INDICATOR                VARCHAR2 (250 BYTE)
            ,PROD_RANK                      VARCHAR2 (250 BYTE)
            ,DEPT_NUMBER                    VARCHAR2 (250 BYTE)
            ,PROD_STATUS_CODE               VARCHAR2 (250 BYTE)
            ,BUYER_NUMBER                   VARCHAR2 (250 BYTE)
            ,PO_REPLACEMENT_COST            NUMBER
            ,PO_REPL_COST_EFFECT_DATE       VARCHAR2 (250 BYTE)
            ,LIST_PRICE                     NUMBER
            ,LIST_PRICE_EFF_DATE            VARCHAR2 (250 BYTE)
            ,STD_COST_AMOUNT                NUMBER
            ,STD_COST_EFFE_DATE             VARCHAR2 (250 BYTE)
            ,BRANCH_COST                    VARCHAR2 (250 BYTE)
            ,BRANCH_COST_EFFE_DATE          VARCHAR2 (250 BYTE)
            ,PROD_VELOCITY_CODE             VARCHAR2 (250 BYTE)
            ,PROD_LINE_STATUS_CODE          VARCHAR2 (250 BYTE)
            ,PROD_LINE_STATUS_DESC          VARCHAR2 (250 BYTE)
            ,PART_VENDOR_CODE               VARCHAR2 (250 BYTE)
            ,MIN_QTY                        NUMBER
            ,MAX_QTY                        NUMBER
            ,SAFETY_STOCK                   NUMBER
            ,ORDER_LEAD_DAYS                NUMBER
            ,MINIMUM_GROSS_MARGIN           NUMBER
            ,MIN_SELL_PRICE_AMT             NUMBER
            ,LAST_COUNT_QTY                 NUMBER
            ,LAST_COUNTED_BY                VARCHAR2 (250 BYTE)
            ,LAST_COUNT_DATE                DATE
            ,LEAD_TIME_EXPIRATION_DATE      VARCHAR2 (250 BYTE)
            ,LAST_PO_RECEIPT_DATE           DATE
            ,LAST_TRANSFER_OUT_DATE         DATE
            ,LAST_TRANSFER_IN_DATE          DATE
            ,LAST_WORK_ORDER_IN_DATE        VARCHAR2 (250 BYTE)
            ,LAST_WORK_ORDER_OUT_DATE       VARCHAR2 (250 BYTE)
            ,LAST_SALES_DATE                DATE
            ,TWELVE_MONTH_WO_OUT_QTY        VARCHAR2 (250 BYTE)
            ,TWELVE_MONTH_TRX_OUT_QTY       VARCHAR2 (250 BYTE)
            ,TWELVE_MONTH_STOCK_SALE_QTY    VARCHAR2 (250 BYTE)
            ,TWELVE_MONTH_DIRECT_SALE_QTY   VARCHAR2 (250 BYTE)
            ,FORECAST_HITS                  VARCHAR2 (250 BYTE)
            ,FORECAST_PERIOD                VARCHAR2 (250 BYTE)
            ,OVERRIDE_BRANCH_HITS           VARCHAR2 (250 BYTE)
            ,TWELVE_MONTH_HITS              VARCHAR2 (250 BYTE)
            ,PRICE_SHEET                    VARCHAR2 (250 BYTE)
            ,PRICE_SHEET_EFFE_DATE          DATE
            ,WAREHOUSE_BRANCH               VARCHAR2 (250 BYTE)
            ,WAREHOUSE_BRANCH_FLAG          VARCHAR2 (250 BYTE)
            ,SEASONAL_FLAG                  VARCHAR2 (250 BYTE)
            ,TREND_PERCENT                  VARCHAR2 (250 BYTE)
            ,PRIMARY_BIN_LOCATION           VARCHAR2 (250 BYTE)
            ,PRIMARY_UNIT_OF_MEASURE        VARCHAR2 (250 BYTE)
            ,ORGANIZATION_ID                NUMBER
            ,INVENTORY_ITEM_ID              NUMBER
            ,SOURCE_SUP_PRIM_PAY_SITE       VARCHAR2 (250 BYTE))';

        xxwc_common_tunning_helpers.alter_table_temp ('XXWC', 'XXWC_INV_PROD_LOC##_Z');

        EXECUTE IMMEDIATE
            'INSERT/*+append*/ into XXWC.XXWC_INV_PROD_LOC##_Z
             SELECT ''WHITECAP''
                   ,''Oracle EBS''
                   ,y.OPERATING_UNIT_ID
                   ,y.OPERATING_UNIT_NAME
                   ,y.INTERFACE_DATE
                   ,y.BRANCH_NUMBER
                   ,y.BRANCH_NAME
                   ,y.SU_SKU
                   ,y.SKU_START_DATE
                   ,(CASE WHEN SUBSTR (y.STOCK_INDICATOR, ''1'', ''40''  ) IN
                                 (''1'', ''2'', ''3'', ''4'', ''5'', ''6'', ''7'', ''8'', ''9'', ''E'')
                          THEN ''S''
                     ELSE ''N'' END)
                   ,SUBSTR (y.STOCK_INDICATOR, ''1'', ''163'')
                   ,NULL
                   ,y.PROD_STATUS_CODE
                   ,y.BUYER_NUMBER
                   ,y.PO_REPLACEMENT_COST
                   ,NULL
                   ,y.LIST_PRICE
                   ,NULL
                   ,y.STD_COST_AMOUNT
                   ,NULL
                   ,NULL
                   ,NULL
                   ,y.PROD_VELOCITY_CODE
                   ,NULL
                   ,NULL
                   ,Y.PART_VENDOR_CODE
                   ,y.MIN_QTY
                   ,y.MAX_QTY
                   ,y.STOCK_QTY
                   ,y.ORDER_LEAD_DAYS
                   ,y.MINIMUM_GROSS_MARGIN
                   ,(y.STD_COST_AMOUNT*(y.MINIMUM_GROSS_MARGIN/''100''))
                   ,y.LAST_COUNT_QTY
                   ,SUBSTR(y.LAST_COUNTED_BY,''1'', ''100'')
                   ,y.LAST_COUNT_DATE
                   ,NULL
                   ,y.LAST_PO_RECEIPT_DATE
                   ,y.LAST_TRANSFER_OUT_DATE
                   ,y.LAST_TRANSFER_IN_DATE
                   ,NULL
                   ,NULL
                   ,y.LAST_SALES_DATE
                   ,NULL
                   ,NULL
                   ,NULL
                   ,NULL
                   ,y.FORECAST_HITS
                   ,''160''
                   ,NULL
                   ,y.TWELVE_MONTH_HITS
                   ,NULL
                   ,y.PRICE_SHEET_EFFE_DATE
                   ,y.WAREHOUSE_BRANCH
                   ,SUBSTR(y.WAREHOUSE_BRANCH_FLAG,''1'', ''1'')
                   ,SUBSTR(y.SEASONAL_FLAG, ''1'',''163'')
                   ,NULL
                   ,SUBSTR(y.PRIMARY_BIN_LOCATION, ''1'', ''40'')
                   ,y.PRIMARY_UNIT_OF_MEASURE
                   ,y.ORGANIZATION_ID
                   ,y.INVENTORY_ITEM_ID
                   ,y.SOURCE_SUP_PRIM_PAY_SITE
               FROM xxwc.xxwc_inv_prod_loc##_y y';

        COMMIT;

    END;

    xxwc_common_tunning_helpers.elapsed_time ('EDW_PROD_LOCATION'
                                             ,'populate Z table'
                                             ,g_start);

    --//========================================================================
    --// Drop temp table
    --//========================================================================
    xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'XXWC_INV_PROD_LOC##_X');
    xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'XXWC_INV_PROD_LOC##_Y');

    --//========================================================================
    --// Print log table xxwc.xxwc_interfaces##log
    --//========================================================================
    xxwc_common_tunning_helpers.elapsed_time ('EDW_PROD_LOCATION'
                                             ,'END Product Location'
                                             ,g_start);

--//============================================================================
--// Handling unknown exceptions
--//============================================================================
EXCEPTION
WHEN others THEN
  l_errbuf :=
         ' Error_Stack...'
      || CHR (10)
      || DBMS_UTILITY.format_error_stack ()
      || CHR (10)
      || ' Error_Backtrace...'
      || CHR (10)
      || DBMS_UTILITY.format_error_backtrace ();

  xxwc_common_tunning_helpers.write_log ('error executing procedure xxwc_ebs_edw_prod_loc_pkg' || l_errbuf);
  p_retcode := 2;
  p_errbuf := l_errbuf;
END;
END;