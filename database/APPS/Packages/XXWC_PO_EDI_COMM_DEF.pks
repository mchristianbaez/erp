/* Formatted on 5/21/2014 2:48:14 PM (QP5 v5.254.13202.43190) */
CREATE OR REPLACE PACKAGE APPS.XXWC_PO_EDI_COMM_DEF
AS
   /*************************************************************************
        $Header XXWC_PO_EDI_COMM_DEF $
        Module Name: XXWC_PO_EDI_COMM_DEF

        PURPOSE:   This package records the choices made in the PO Communication Form so that subsequent visits to the form defaults the correct values.

        REVISIONS:
        Ver        Date        Author                             Description
        ---------  ----------  -------------------------------    -------------------------
        1.0        05/19/2014  Jason Price (Focused E-commerce)   Initial Version

      **************************************************************************/
   PROCEDURE set_history (P_po_header_id       NUMBER,
                          P_TRIGGER_TYPE    IN VARCHAR2,
                          P_CREATION_DATE   IN DATE,
                          P_XML_OR_EDI         VARCHAR2,
                          P_PRINT_CHECK        VARCHAR2,
                          P_FAX_NUMBER         VARCHAR2,
                          P_EMAIL_ADDRESS      VARCHAR2);
END XXWC_PO_EDI_COMM_DEF;