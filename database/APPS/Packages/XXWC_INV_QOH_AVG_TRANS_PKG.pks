CREATE OR REPLACE PACKAGE xxwc_inv_qoh_avg_trans_pkg AS
  /*******************************************************************************
  * Package:   XXWC_INV_QOH_AVG_TRANS_PKG
  * Description: This package is calling from TRANSFER_BUTTON in
                 XXWC_INV_ITEM_AVG_ONH_TRANS.fmb.
  HISTORY
  ===============================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     10-Mar-2018        P.Vamshidhar    Initial creation of the procedure
                                             TMS#20171024-00015
  ********************************************************************************/

  TYPE t_xxwc_inv_item_avg_onh_trans IS RECORD(
     process_id       NUMBER
    ,branch           VARCHAR2(5)
    ,from_item        VARCHAR2(30)
    ,from_item_desc   VARCHAR2(240)
    ,from_primary_uom VARCHAR2(10)
    ,from_avg_cost    NUMBER(10, 5)
    ,from_onhand_qty  NUMBER
    ,to_item          VARCHAR2(30)
    ,to_item_desc     VARCHAR2(240)
    ,to_primary_uom   VARCHAR2(10)
    ,to_avg_cost      NUMBER(10, 5)
    ,to_onhand_qty    NUMBER
    ,SOURCE           VARCHAR2(100)
    ,last_update_date DATE
    ,last_updated_by  NUMBER
    ,creation_date    DATE
    ,created_by       NUMBER
    ,from_flag        VARCHAR2(1));

  PROCEDURE process_request(p_process_id       IN NUMBER
                           ,p_from_item_id     IN NUMBER
                           ,p_from_item        IN VARCHAR2
                           ,p_from_description IN VARCHAR2
                           ,p_from_primary_uom IN VARCHAR2
                           ,p_from_std_rate    IN NUMBER
                           ,p_from_std_uom     IN VARCHAR2
                           ,p_to_item_id       IN NUMBER
                           ,p_to_item          IN VARCHAR2
                           ,p_to_description   IN VARCHAR2
                           ,p_to_primary_uom   IN VARCHAR2
                           ,p_to_std_rate      IN NUMBER
                           ,p_to_std_uom       IN VARCHAR2
                           ,p_cust_frm_rate    IN NUMBER
                           ,p_org_or_branchs   IN VARCHAR2
                           ,p_user_id          IN NUMBER
                           ,p_mode             IN VARCHAR2
                           ,o_ret_msg          OUT VARCHAR2
                           ,o_err_flag         OUT VARCHAR2);
  PROCEDURE onhand_transfer(p_from_item_id         IN NUMBER
                           ,p_to_item_id           IN NUMBER
                           ,p_to_item              IN VARCHAR2
                           ,p_org_code             IN VARCHAR2
                           ,p_organization_id      IN NUMBER
                           ,p_frm_uom              IN VARCHAR2
                           ,p_to_uom               IN VARCHAR2
                           ,p_from_item_cost       IN VARCHAR2
                           ,p_avg_cost             IN NUMBER
                           ,p_transaction_quantity IN NUMBER
                           ,p_to_item_qty          IN NUMBER
                           ,p_user_id              IN NUMBER
                           ,o_ret_msg              OUT VARCHAR2);
END;
/
