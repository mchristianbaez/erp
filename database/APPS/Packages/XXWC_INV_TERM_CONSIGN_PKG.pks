CREATE OR REPLACE PACKAGE APPS.XXWC_INV_TERM_CONSIGN_PKG
/*************************************************************************
  $Header XXWC_INV_TERM_CONSIGN_PKG.pks $
  Module Name: XXWC_INV_TERM_CONSIGN_PKG

  PURPOSE: Terminate Supplier Consignments

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    ----------------
  1.0        22-Feb-2016  Manjula Chellappan    Initial Version TMS # 20160219-00081
**************************************************************************/
AS
   PROCEDURE TransferToRegular (p_errbuf                 OUT VARCHAR2,
                                p_retcode                OUT NUMBER,
                                p_supplier_id         IN     NUMBER,
                                p_organization_id     IN     NUMBER,
                                p_inventory_item_id   IN     NUMBER);

   PROCEDURE DisableASL (p_errbuf                 OUT VARCHAR2,
                         p_retcode                OUT NUMBER,
                         p_supplier_id         IN     NUMBER,
                         p_organization_id     IN     NUMBER,
                         p_inventory_item_id   IN     NUMBER);

   PROCEDURE UpdateItems (p_errbuf                 OUT VARCHAR2,
                          p_retcode                OUT NUMBER,
                          p_organization_id     IN     NUMBER,
                          p_inventory_item_id   IN     NUMBER);

   PROCEDURE main (errbuf                   OUT VARCHAR2,
                   retcode                  OUT NUMBER,
                   p_supplier_id         IN     NUMBER,
                   p_organization_id     IN     NUMBER,
                   p_inventory_item_id   IN     NUMBER,
                   p_action_type         IN     VARCHAR2);
END XXWC_INV_TERM_CONSIGN_PKG;
/