CREATE OR REPLACE PACKAGE XXWC_OM_PRICING_CONV_PKG AS

  /****************************************************************************************************************
      $Header XXXWC_OM_PRICING_CONV_PKG $
      Module Name: XXWC_OM_PRICING_CONV_PKG.pks
  
      PURPOSE:    Convert existing modifiers to new pricing form
  
      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------    -------------------------------------------------------------------
      1.0        01/15/2018  Nancy Pahwa                 Initial Version TMS # 20170201-00276
  
  *****************************************************************************************************************/
  procedure pricing_conv(errbuf      OUT VARCHAR2,
                         retcode     OUT NUMBER,
                         p_from_date varchar2,
                         p_to_date   varchar2);
end XXWC_OM_PRICING_CONV_PKG;
/