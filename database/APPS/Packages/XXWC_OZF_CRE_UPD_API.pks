CREATE OR REPLACE PACKAGE APPS.XXWC_OZF_CRE_UPD_API AS
/**************************************************************************
   $Header XXWC_OZF_CRE_UPD_API $
   Module Name: XXWC_OZF_CRE_UPD_API.pks

   PURPOSE:   This package is called by the concurrent programs
              XXWC Create/Update OZF FND Objects for creating and updating the request group 
              used for Trade Management module in EBS system.
   REVISIONS:
   Ver        Date        Author             Description
   ---------  ----------  ---------------   -------------------------
    1.0       03/08/2018  Ashwin Sridhar    Initial Build - Task ID: TMS#20180713-00060
/*************************************************************************/

PROCEDURE CREATE_NEW_REQGROUP_P(p_errbuf  OUT VARCHAR2
                               ,p_retcode OUT VARCHAR2
                                );

PROCEDURE REMOVE_REQSET_FROM_REQGROUP_P(p_group_name IN VARCHAR2);

PROCEDURE UPDATE_RESP_WITH_REQGROUP_P(p_group_name IN VARCHAR2);

END XXWC_OZF_CRE_UPD_API;
/