/* Formatted on 3/4/2014 1:29:18 AM (QP5 v5.206) */
-- Start of DDL Script for Package APPS.XXWC_AP_INVOICE_HOLD_NOTIFY
-- Generated 3/4/2014 1:29:17 AM from APPS@EBIZFQA

CREATE OR REPLACE PACKAGE apps.xxwc_ap_invoice_hold_notify
IS
    --
    -- Purpose: This supports the AP Invoice Hold Notification
    -- workflow customizations and the WC DIRT (Discrepant Invoice Resolution
    -- and tracking) project. Some of the included procedures are brand new and
    -- some are copies of seeded procedures for use by the Workflow.
    --
    -- MODIFICATION HISTORY
    -- Person      Date    Comments
    -- ---------   ------  ------------------------------------------
    -- Raskiha G            Author
    -- Andre R     9/20/12  Added these comments

    FUNCTION get_branch_manager (p_location_id NUMBER)
        RETURN NUMBER;

    FUNCTION get_user_id (p_person_id NUMBER)
        RETURN NUMBER;

    FUNCTION get_user_name (p_person_id NUMBER)
        RETURN VARCHAR2;

    FUNCTION get_approver (p_transaction_id NUMBER)
        RETURN VARCHAR2;

    FUNCTION get_ap_person
        RETURN VARCHAR2;

    PROCEDURE get_next_approver (itemtype    IN            VARCHAR2
                                ,itemkey     IN            VARCHAR2
                                ,actid       IN            NUMBER
                                ,funcmode    IN            VARCHAR2
                                ,resultout      OUT NOCOPY VARCHAR2);

    PROCEDURE xxwc_send_email_to_ap (itemtype    IN            VARCHAR2
                                    ,itemkey     IN            VARCHAR2
                                    ,actid       IN            NUMBER
                                    ,funcmode    IN            VARCHAR2
                                    ,resultout      OUT NOCOPY VARCHAR2);

    PROCEDURE xxwc_process_ack_pounmatched (itemtype    IN            VARCHAR2
                                           ,itemkey     IN            VARCHAR2
                                           ,actid       IN            NUMBER
                                           ,funcmode    IN            VARCHAR2
                                           ,resultout      OUT NOCOPY VARCHAR2);

    PROCEDURE xxwc_process_ack_pomatched (itemtype    IN            VARCHAR2
                                         ,itemkey     IN            VARCHAR2
                                         ,actid       IN            NUMBER
                                         ,funcmode    IN            VARCHAR2
                                         ,resultout      OUT NOCOPY VARCHAR2);

    PROCEDURE populate_add_attributes (itemtype    IN            VARCHAR2
                                      ,itemkey     IN            VARCHAR2
                                      ,actid       IN            NUMBER
                                      ,funcmode    IN            VARCHAR2
                                      ,resultout      OUT NOCOPY VARCHAR2);

    FUNCTION get_ship_to_location (p_hold_id NUMBER)
        RETURN VARCHAR2;

    FUNCTION get_po_number (p_hold_id NUMBER)
        RETURN VARCHAR2;
END;                                                                                                     -- Package spec
/

-- End of DDL Script for Package APPS.XXWC_AP_INVOICE_HOLD_NOTIFY
