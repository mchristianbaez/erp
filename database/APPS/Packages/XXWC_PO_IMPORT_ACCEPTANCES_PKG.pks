/* Formatted on 07-Mar-2014 15:48:09 (QP5 v5.206) */
-- Start of DDL Script for Package APPS.XXWC_PO_IMPORT_ACCEPTANCES_PKG
-- Generated 07-Mar-2014 15:48:08 from APPS@EBIZFQA

CREATE OR REPLACE PACKAGE apps.xxwc_po_import_acceptances_pkg
AS
    /*************************************************************************
       $Header xxwc_po_acceptances_pkg $
       Module Name: xxwc_po_acceptances_pkg.pks

       PURPOSE:   This package imports inbound EDI 855 PO Acceptances, calls XML Publisher, bursts files, attaches and emails them to Buyers.

       REVISIONS:
       Ver        Date        Author                             Description
       ---------  ----------  -------------------------------    -------------------------
       1.0        03/05/2014  Jason Price (Focused E-commerce)   Initial Version

     **************************************************************************/

    PROCEDURE process_po_acceptances (errbuf OUT VARCHAR2, retcode OUT VARCHAR2);

    PROCEDURE wwxc_notif_attach_procedure (p_document_id     IN     VARCHAR2
                                          ,p_display_type    IN     VARCHAR2
                                          ,p_document        IN OUT BLOB
                                          ,p_document_type   IN OUT VARCHAR2);
END xxwc_po_import_acceptances_pkg;
/

-- End of DDL Script for Package APPS.XXWC_PO_IMPORT_ACCEPTANCES_PKG
