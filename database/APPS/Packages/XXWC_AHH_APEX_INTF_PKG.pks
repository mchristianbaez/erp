CREATE OR REPLACE PACKAGE APPS.XXWC_AHH_APEX_INTF_PKG
IS
  /******************************************************************************************************************************************************
        $Header XXWC_AHH_APEX_INTF_PKG $
        Module Name: XXWC_AHH_APEX_INTF_PKG.pkb

        PURPOSE:   AHH Customer Conversion

        REVISIONS:
        Ver        Date          Author           Description
        ---------  ----------   ---------------  ------------------------------------------------------------------------------------------------
        1.0        06/06/2018   Nancy Pahwa       AH HARRIS Interface TMS 20180716-00021
   ******************************************************************************************************************************************************/

   PROCEDURE write_log (p_log_msg VARCHAR2);

   PROCEDURE AR_INVOICE_INTERFACE(p_user_NAME    IN VARCHAR2,
                               P_RESP_ID      IN NUMBER,
                               P_RESP_APPL_ID IN NUMBER,
                                x_ret_msg OUT VARCHAR2);
    PROCEDURE AR_LOCKBOX_INTERFACE(p_user_NAME    IN VARCHAR2,
                               P_RESP_ID      IN NUMBER,
                               P_RESP_APPL_ID IN NUMBER,
                                x_ret_msg OUT VARCHAR2);
     PROCEDURE AP_INVOICE_INTERFACE(p_user_NAME    IN VARCHAR2,
                               P_RESP_ID      IN NUMBER,
                               P_RESP_APPL_ID IN NUMBER,
                                x_ret_msg OUT VARCHAR2);
  PROCEDURE AR_DEBIT_MEMO_INTERFACE(p_user_NAME    IN VARCHAR2,
                               P_RESP_ID      IN NUMBER,
                               P_RESP_APPL_ID IN NUMBER,
                                x_ret_msg OUT VARCHAR2);
END;
/