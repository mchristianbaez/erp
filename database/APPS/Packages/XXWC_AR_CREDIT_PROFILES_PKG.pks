CREATE OR REPLACE PACKAGE xxwc_ar_credit_profiles_pkg
AS
   /*************************************************************************
   *   $Header xxwc_ar_credit_profiles_pkg.pks $
   *   Module Name: xxwc update credit profiles
   *
   *   PURPOSE:   Procedure used to update profile classes and customer account
   *              for cash on the fly customers
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        10/09/2012  Shankar Hariharan            Initial Version
   * ***************************************************************************/
   PROCEDURE update_customer_profile (errbuf                  OUT VARCHAR2
                                    , retcode                 OUT VARCHAR2
                                    , i_profile_class_id   IN     NUMBER
                                    , i_customer_class_code IN    VARCHAR2
                                    , i_remit_to_code       IN    VARCHAR2);
END xxwc_ar_credit_profiles_pkg;
/

