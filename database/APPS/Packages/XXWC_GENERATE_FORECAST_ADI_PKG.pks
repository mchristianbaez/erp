CREATE OR REPLACE 
PACKAGE APPS.XXWC_GENERATE_FORECAST_ADI_PKG
AS
   /*************************************************************************
   *   $Header xxwc_generate_forecast_adi_pkg $
   *   Module Name: xxwc_generate_forecast_adi_pkg
   *
   *   PURPOSE:   Package used to submit XXWC Generate Demand Forecast via Web ADI Upload - TMS Ticket # 20130201-01386 
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        08/12/2013  Lee Spitzer                Initial Version
   * ***************************************************************************/

PROCEDURE submit_ccp           (p_org_code in varchar2
                               ,p_forecast_designator in varchar2
                               ,p_branch_dc_mode in varchar2
                               ,p_item_range    in varchar2
                               ,p_item_category in varchar2
                               ,p_item_number   in varchar2
                               ,p_bucket_type   in number
                               ,p_start_date    in date
                               ,p_num_of_fc_periods    in number
                               ,p_num_of_prev_periods in number
                               ,p_constant_seasonality in number
                               ,p_seasonality1 in number
                               ,p_seasonality2 in number
                               ,p_seasonality3 in number
                               ,p_seasonality4 in number
                               ,p_seasonality5 in number
                               ,p_seasonality6 in number
                               ,p_seasonality7 in number
                               ,p_seasonality8 in number
                               ,p_seasonality9 in number
                               ,p_seasonality10 in number
                               ,p_seasonality11 in number
                               ,p_seasonality12 in number);

END XXWC_GENERATE_FORECAST_ADI_PKG;