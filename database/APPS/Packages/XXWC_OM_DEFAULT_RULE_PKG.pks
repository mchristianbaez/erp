CREATE OR REPLACE PACKAGE APPS.XXWC_OM_DEFAULT_RULE_PKG
/*************************************************************************
 Copyright (c) 2014 HD Supply
 All rights reserved.
**************************************************************************
  $Header XXWC_OM_DEFAULT_RULE_PKG $
  Module Name: XXWC_OM_DEFAULT_RULE_PKG.pkb

  PURPOSE: To Customize Defaulting Rules in Oracle.

  REVISIONS:
  Ver        Date        Author              Description
  ---------  ----------  ---------------     -------------------------
  1.0        03/05/2014  Gopi Damuluri       Initial Version
                                             TMS# 20140124-00027 
                                             
   2.0        07/31/2014  Gopi Damuluri      TMS # 20140730-00271 (Restirct warehouse change 
                                              when the ship to changed)  
3.0         8/5/2014      Ram Talluri       TMS #20140805-00024 -Added function Get_subinv_iso_line-to return correct subinventory for ISO line.                                                                                        
**************************************************************************/
AS
FUNCTION Get_Return_Ship_Code( p_database_object_name     IN  VARCHAR2
                             , p_attribute_code           IN  VARCHAR2)
RETURN VARCHAR2;

FUNCTION Get_Ship_From_Org( p_database_object_name     IN  VARCHAR2
                             , p_attribute_code           IN  VARCHAR2)
RETURN NUMBER;
/**************************************************************************
 FUNCTION NAME:Get_subinv_iso_line
  PURPOSE: To get applicable subinventory for internal order line.

  REVISIONS:
  Ver        Date        Author              Description
  ---------  ----------  ---------------     -------------------------
  3.0        8/5/2014      Ram Talluri       TMS #20140805-00024 -Added function Get_subinv_iso_line-to return correct subinventory for ISO line.                                                                                        
**************************************************************************/
FUNCTION Get_subinv_iso_line( p_database_object_name     IN  VARCHAR2
                             , p_attribute_code           IN  VARCHAR2)
RETURN VARCHAR2;


END XXWC_OM_DEFAULT_RULE_PKG;
/