CREATE OR REPLACE PACKAGE apps.xxwc_qp_test_pkg
AS
   TYPE item_rec_type IS RECORD
   (
      inventory_item_id   NUMBER
     ,order_qty           NUMBER
   );

   TYPE item_tbl_type IS TABLE OF item_rec_type
      INDEX BY BINARY_INTEGER;

   /*************************************************************************
        *   Procedure : GET_ORDER_SUB_TOTAL
        *
        *   PURPOSE:   This procedure return order sub total excluding Freight and Tzxes
        *  Author: Raghav Velicheti (TMS # 20130121-00460)
        * Creation_date 4/3/2013
        *Last update Date 4/3/2013
        *   Parameter:
        *          IN
        *              p_Header_id
        * ************************************************************************/
   FUNCTION xxwc_get_tot_minus (p_header_id IN NUMBER)
      RETURN NUMBER;

   /*************************************************************************
   *   Function : XXWC_PRICE_REQUEST
   *
   *   PURPOSE:   This function is used to derive the system delivered price..
   *   Author: Ram Talluri
   *   Creation_date 8/20/2013
   *   Last update Date 8/20/2013
   *   Parameter:
   *          IN
   *              p_ship_from_org_id
   *             p_inventory_item_id
   *            p_customer_id
   *            p_cust_location_id
   * ************************************************************************/
   PROCEDURE xxwc_price_request (errbuf                  OUT VARCHAR2
                                ,retcode                 OUT VARCHAR2
                                ,p_ship_from_org_id   IN     NUMBER
                                ,p_item_tbl           IN     item_tbl_type
                                ,p_customer_id        IN     NUMBER
                                ,p_cust_site_use_id   IN     NUMBER);



   /*************************************************************************
    *   Function : XXWC_PRICE_REQUEST
    *
    *   PURPOSE:   This function is used to derive the system delivered price.
    *              It is an overloaded version that return the prices in the
    *              price table parameters. It does not DBMS_Output the resuts.
    *   Author: Andre Rivas
    *   Creation_date 8/20/2013
    *   Last update Date 6/6/2014
    *   Parameter:
    *          IN
    *              p_ship_from_org_id
    *             p_inventory_item_id
    *            p_customer_id
    *            p_cust_location_id
                 x_line_tbl         OUT qp_preq_grp.line_tbl_type
                 x_line_attr_tbl    OUT qp_preq_grp.line_tbl_type
    * ************************************************************************/
   PROCEDURE xxwc_price_request (errbuf                  OUT VARCHAR2
                                ,retcode                 OUT VARCHAR2
                                ,p_ship_from_org_id   IN     NUMBER
                                ,p_item_tbl           IN     item_tbl_type
                                ,p_customer_id        IN     NUMBER
                                ,p_cust_site_use_id   IN     NUMBER
                                ,x_line_tbl              OUT qp_preq_grp.line_tbl_type
                                ,x_line_attr_tbl         OUT qp_preq_grp.line_attr_tbl_type);


   /* Test procedure for the QP Price Request API. This will run through
           a large number of price lists and will output the timing for each
           order. A comma separated IN list can be passed in as a parameter,
           or the parameter may be left null to use the standard benchmark lists
           of items. The parameter list should be a list of order header IDs.
           */
   PROCEDURE price_request_test_run (p_order_header_id_list IN VARCHAR2 := NULL);
END xxwc_qp_test_pkg;
/