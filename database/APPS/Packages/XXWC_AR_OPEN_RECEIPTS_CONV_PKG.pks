CREATE OR REPLACE PACKAGE APPS.XXWC_AR_OPEN_RECEIPTS_CONV_PKG
AS
   /*************************************************************************
   *  Copyright (c) 2011 Lucidity Consulting Group
   *  All rights reserved.
   **************************************************************************
   *   $Header XXWC_AR_OPEN_RECEIPTS_PKG.pks $
   *   Module Name: XXWC_AR_OPEN_RECEIPTS_PKG.pks
   *
   *   PURPOSE:   This package is called by the concurrent program XXWC AR Receipts Conversion
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        10/11/2011  Vivek Lakaman             Initial Version
   *   1.1        03/04/2018  Ashwin Sridhar       Added for TMS#20180319-00242
   * ************************************************************************/

   -- ***************************************************************************************************************
   -- Procedure SUBMIT : Main API to process AR Open Receipts from Staging table and populate them into Oracle Tables.
   --
   PROCEDURE Submit (errbuf OUT VARCHAR2, retcode OUT VARCHAR2);
End Xxwc_Ar_Open_Receipts_Conv_Pkg;
/
