CREATE OR REPLACE PACKAGE xxwc_om_chg_ord_wf_pkg AS
  /*************************************************************************
  *   $Header XXWC_OM_CHG_ORD_WF_PKG.PKG $
  *   Module Name: XXWC_OM_CHG_ORD_WF_PKG.PKG
  *
  *   PURPOSE:   This package is used for customization in OM Change order
  *              Workflow.     
  *
  *   REVISIONS:
  *   Ver        Date         Author                     Description
  *   ---------  -----------  ---------------         -------------------------
  *   1.0        26-Apr-2016  Naveen Kalidindi        Initial Version.
  *													  TMS# 20171113-00018.
  * ***************************************************************************/
  
  /*************************************************************************
     PROCEDURE Name: Enable_Customization
  
     PURPOSE:   Validation criteria or Control mechanism to choose Customi-
                -zation or use Standard flow.
  ****************************************************************************/
  PROCEDURE enable_customization(p_itemtype IN VARCHAR2
                                ,p_itemkey  IN VARCHAR2
                                ,p_actid    IN NUMBER
                                ,p_funcmode IN VARCHAR2
                                ,resultout  IN OUT VARCHAR2);
  
  
  /*************************************************************************
     PROCEDURE Name: populate_req_first_approver
  
     PURPOSE:   Derive first active approver from Internal Requisition Appr-
                -oval/Action history.
  ****************************************************************************/
  PROCEDURE populate_req_first_approver(p_itemtype IN VARCHAR2
                                       ,p_itemkey  IN VARCHAR2
                                       ,p_actid    IN NUMBER
                                       ,p_funcmode IN VARCHAR2
                                       ,resultout  IN OUT VARCHAR2);

END xxwc_om_chg_ord_wf_pkg;
/
