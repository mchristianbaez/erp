CREATE OR REPLACE PACKAGE APPS.XXWCINV_INV_AGING_PKG
/* Formatted on 27-Jun-2012 11:12:04 (QP5 v5.206) */
/***************************************************************************
    $Header XXWCINV_INV_AGING_PKG.pks $
    Module Name: XXWCINV_INV_AGING_PKG
    PURPOSE: Package used for Aging Report
 
    REVISIONS:
    Ver    Date       	Author                Description
    ------ ---------  	------------------    ----------------
    1.0    Unknown    	Unknown               Initial Version
 -- There are multiple versions in SVN. To Follow the standards adding the history from Version 2.0
    2.0    19-Jun-2015	Manjula Chellappan    TMS# 20150618-00095 Inventory Aging Report - Redesign concurrent program
    3.0    16-DEC-2015  Lee Spitzer           TMS# 20150928-00193 Branch Aging Report
   
****************************************************************************/
AS
    p_region           VARCHAR2 (80);
    p_branch           NUMBER;                                                                        -- organization_id
    --P_MARKET                             VARCHAR2(80);
    p_item_category    VARCHAR2 (80);
    p_supplier_id      NUMBER;                                                                              -- vendor_id
    p_reserve_group    VARCHAR2 (80);
    p_days_low         NUMBER := 1;
    p_days_high        NUMBER := 9999;
    --P_RSV_AMT_LIMIT                      NUMBER := 0;

    cp_org_name        VARCHAR2 (80);
    cp_supplier_name   VARCHAR2 (80);
    

    /**************************************************************************
     *
     * FUNCTION
     *  BeforeReport
     *
     * DESCRIPTION
     *  This function is called at the beginning of the HD Supply Inventory Aging Report XML Publisher Data Template
     *   to calculate the inventory age, amounts and reserve amounts and populate the temp table used by the report logic
     *
     * PARAMETERS
     * ==========
     * NAME              TYPE     DESCRIPTION
    .* ----------------- -------- ---------------------------------------------
     * P_LOCATION        VARCHAR2 Inventory organization / store
     * P_REGION          VARCHAR2 Geographical region of stores
     * P_ITEM_CATEGORY   VARCHAR2 Inventory item category usng the "Inventory" category set
     * P_SUPPLIER_ID     NUMBER   Supplier ID
     * P_RESERVE_GROUP   VARCHAR2 Reserve type, associated with item category (expected values are Chemicals/Time Sensitive; Specials ; All Other Inventory)
     * P_REPORT_TYPE     VARCHAR2 Determine whether to use current data or month-end data in report
     *
     * RETURN VALUE
     *  BOOLEAN - True for success or False for error
     *
     *
     * CALLED BY
     *  HD Supply Inventory Aging Report XML Publisher Data template
     *
     *************************************************************************/
    FUNCTION beforereport
        RETURN BOOLEAN;

    /**************************************************************************
     *
     * FUNCTION
     *  CP_Org_Name_P
     *
     * DESCRIPTION
     *  This function is called at the beginning of the HD Supply Inventory Aging Report XML Publisher Data Template
     *   to translate the organization id into the organization name
     *
     * PARAMETERS
     * ==========
     * NAME              TYPE     DESCRIPTION
    .* ----------------- -------- ---------------------------------------------
     *
     * RETURN VALUE
     *  VARCHAR2 - organization name
     *
     *
     * CALLED BY
     *  HD Supply Inventory Aging Report XML Publisher Data template
     *
     *************************************************************************/
    FUNCTION cp_org_name_p
        RETURN VARCHAR2;

    /**************************************************************************
     *
     * FUNCTION
     *  CP_Supplier_Name_P
     *
     * DESCRIPTION
     *  This function is called at the beginning of the HD Supply Inventory Aging Report XML Publisher Data Template
     *   to translate the vendor id into a supplier name
     *
     * PARAMETERS
     * ==========
     * NAME              TYPE     DESCRIPTION
    .* ----------------- -------- ---------------------------------------------
     *
     * RETURN VALUE
     *  VARCHAR2 - supplier name
     *
     *
     * CALLED BY
     *  HD Supply Inventory Aging Report XML Publisher Data template
     *
     *************************************************************************/
    FUNCTION cp_supplier_name_p
        RETURN VARCHAR2;

     /**************************************************************************
     *
     * FUNCTION
     *  CP_vendor_name_p
     *
     * DESCRIPTION
     *  This function is used to derive the vendor source based on the source type
     *
     * PARAMETERS
     * ==========
     * NAME              TYPE     DESCRIPTION
    .* ----------------- -------- ---------------------------------------------
     *
     * RETURN VALUE
     *  VARCHAR2 - vendor Name or organization code
     *
     *
     * CALLED BY
     *  HD Supply Inventory Aging Report 
     * HISTORY
     * =======
     *
     * VERSION DATE        AUTHOR(S)       DESCRIPTION
     * ------- ----------- --------------- ------------------------------------
     * 1.00    04/23/2014   Maharajan S     Creation ESMS# 247548
     *
     *************************************************************************/

    FUNCTION cp_vendor_name_p(p_inventory_item_id NUMBER, p_organization_id NUMBER)
        RETURN VARCHAR2;

    FUNCTION afterreport
        RETURN BOOLEAN;

    /**************************************************************************
     *
     * PROCEDURE
     *  Update_Orig_Date_Rcvd
     *
     * DESCRIPTION
     *  This procedure is called from a database trigger on the MTL_ONHAND_QUANTITIES_DETAIL table for intercompany receipts
     *    to apply the appropriate orig_date_received value based upon the transfer transaction
     *
     * PARAMETERS
     * ==========
     * NAME              TYPE     DESCRIPTION
    .* ----------------- -------- ---------------------------------------------
     * P_TRX_ID          NUMBER   material transaction id that created the onhand details record
     * P_XFER_TRX_ID     NUMBER   transfer material transaction id used to ship the intercompany sales order
     *
     *
     * CALLED BY
     *  XXWC_INV_ORIG_DATE_RCVD event alert on insert into MTL_ONHAND_QUANTITIES_DETAIL table
     *
     *************************************************************************/
    PROCEDURE update_orig_date_rcvd (errbuf             OUT VARCHAR2,
                                     retcode            OUT VARCHAR2,
                                     p_trx_id        IN     NUMBER,
                                     p_xfer_trx_id   IN     NUMBER);

    /**************************************************************************
     *
     * PROCEDURE
     *  Purge_Interco
     *
     * DESCRIPTION
     *  This procedure is called from a concurrent program to clean-up the intercompany transfer table
     *
     * PARAMETERS
     * ==========
     * NAME              TYPE     DESCRIPTION
    .* ----------------- -------- ---------------------------------------------
     * P_days            NUMBER   days old for a shipment that has not been associated with an intercompany sales order
     *
     * CALLED BY
     *  XXWC_PURGE_INTERCO concurrent program
     *
     *************************************************************************/
    PROCEDURE purge_interco (errbuf OUT VARCHAR2, retcode OUT VARCHAR2, p_days IN NUMBER);

    /**************************************************************************
     *
     * PROCEDURE
     *  Generate_CSV
     *
     * DESCRIPTION
     *  This procedure is called from a concurrent program to generate a .CSV version of the inventory aging report
     *
     * PARAMETERS
     * ==========
     * NAME              TYPE     DESCRIPTION
    .* ----------------- -------- ---------------------------------------------
     * ERRBUF            VARCHAR2 output parameter
     * RETCODE           VARCHAR2 output parameter
     * P_LOCATION        VARCHAR2 Inventory organization / store
     * P_REGION          VARCHAR2 Geographical region of stores
     * P_ITEM_CATEGORY   VARCHAR2 Inventory item category usng the "Inventory" category set
     * P_SUPPLIER_ID     NUMBER   Supplier ID
     * P_RESERVE_GROUP   VARCHAR2 Reserve type, associated with item category (expected values are Chemicals/Time Sensitive; Specials ; All Other Inventory)
     * P_REPORT_TYPE     VARCHAR2 Determine whether to use current data or month-end data in report
     *
     * CALLED BY
     *  XXWC_INV_AGING_CSV concurrent program
     *
     *  Added filter for P_RESERVE_GROUP on 5-25-2012
     *  P_FTP_FLAG       VARCHAR2 Value 'N' will write output to concurrent Out file. 'Y' will generate flat file in /xx_iface -- Added for Revision 2.0 
     *************************************************************************/
    PROCEDURE generate_csv (errbuf               OUT VARCHAR2,
                            retcode              OUT VARCHAR2,
                            p_region          IN     VARCHAR2,
                            p_branch          IN     NUMBER,
                            --p_market           IN   VARCHAR2, Satis U: 06-MAR-2012
                            p_item_category   IN     VARCHAR2,
                            p_reserve_group   IN     VARCHAR2,
                            p_supplier_id     IN     NUMBER,
                            p_days_low        IN     NUMBER,
                            p_days_high       IN     NUMBER,
                            p_ftp_flag        IN     VARCHAR2 -- Added for Revision 2.0 
                            );

    -- p_rsv_amt_limit    IN   NUMBER); -- Satish U: 06-MAR-2012
    --*************************************************************************
    --*************************************************************************
    --generate_csv_utl_file
    --added by Rasikha 15-JUN-2012 to call from plsql to be able to put file into xx_iface directory, then ftp it
    --*************************************************************************
    PROCEDURE generate_csv_utl_file (errbuf               OUT VARCHAR2,
                                     retcode              OUT VARCHAR2,
                                     p_region          IN     VARCHAR2,
                                     p_branch          IN     NUMBER,
                                     p_item_category   IN     VARCHAR2,
                                     p_reserve_group   IN     VARCHAR2,
                                     p_supplier_id     IN     NUMBER,
                                     p_days_low        IN     NUMBER,
                                     p_days_high       IN     NUMBER,
                                     p_file_name          OUT VARCHAR2);

    --*************************************************************************
    --run_report_weekly
    --added by Rasikha 15-JUN-2012 to call from plsql to be able to put file into xx_iface directory, then ftp it
    --*************************************************************************
    PROCEDURE run_report_weekly;

    FUNCTION xxwc_next_month_end_aging
        RETURN VARCHAR2;
        
    PROCEDURE Update_Orig_Date_Rcvd_API (
                                p_trx_id       IN  NUMBER,
                                p_xfer_trx_id  IN  NUMBER);

    FUNCTION GET_INTRANSIT_BOD (
                                        p_inventory_item_id IN NUMBER,
                                        p_from_organization_id IN NUMBER,
                                        p_to_organization_id IN NUMBER,
                                        p_receipt_source_code in VARCHAR2,
                                        p_quantity in NUMBER)
        RETURN DATE;
        
   /*
   Function CHECK_TIME_SENSITIVE Added TMS 20130827-00676 on 8/26/2013 by Lee Spitzer to validate if item is considered Time Sensitive, returns  Yes or No
     (p_inventory_item_id IN NUMBER  -- Inventory Item Id
     ,p_organization_id IN NUMBER)   -- Organization Id
        RETURN VARCHAR2;
   */ 
   FUNCTION CHECK_TIME_SENSITIVE
                             (p_inventory_item_id IN NUMBER
                             ,p_organization_id IN NUMBER)
        RETURN VARCHAR2;
   
   /*Function CHECK_SPECIAL Added TMS 20130827-00676 on 8/26/2013 by Lee Spitzer if item is considered to be a special, return Yes or No
     (p_inventory_item_id IN NUMBER  -- Inventory Item Id
     ,p_organization_id IN NUMBER)   -- Organization Id
        RETURN VARCHAR2;
   */
        
   FUNCTION CHECK_SPECIAL     (p_inventory_item_id IN NUMBER
                             ,p_organization_id IN NUMBER)
        RETURN VARCHAR2;
        


    /**************************************************************************
     *
     * FUNCTION
     *  BeforeReport_branch
     *
     * DESCRIPTION
     *  Function added for TMS Ticket 20150928-00193
     *  This function is called at the beginning of the HD Supply Inventory Aging Branch Report
     *   to calculate the inventory age, amounts and reserve amounts and populate the temp table used by the report logic
     *
     * PARAMETERS
     * ==========
     * NAME              TYPE     DESCRIPTION
    .* ----------------- -------- ---------------------------------------------
     * P_LOCATION        VARCHAR2 Inventory organization / store
     * P_REGION          VARCHAR2 Geographical region of stores
     * P_ITEM_CATEGORY   VARCHAR2 Inventory item category usng the "Inventory" category set
     * P_SUPPLIER_ID     NUMBER   Supplier ID
     * P_RESERVE_GROUP   VARCHAR2 Reserve type, associated with item category (expected values are Chemicals/Time Sensitive; Specials ; All Other Inventory)
     * P_REPORT_TYPE     VARCHAR2 Determine whether to use current data or month-end data in report
     *
     * RETURN VALUE
     *  BOOLEAN - True for success or False for error
     *
     *
     * CALLED BY
     *  HD Supply Inventory Aging Report XML Publisher Data template
     *
     *************************************************************************/

    FUNCTION beforereport_branch
        RETURN BOOLEAN;


    /**************************************************************************
     *
     * PROCEDURE
     *  Generate_Branch_CSV
     *
     * DESCRIPTION
     *  This procedure is called from a concurrent program to generate a .CSV version of the inventory aging branch report 
     *  for TMS Ticket 20150928-00193
     *
     * PARAMETERS
     * ==========
     * NAME              TYPE     DESCRIPTION
    .* ----------------- -------- ---------------------------------------------
     * ERRBUF            VARCHAR2 output parameter
     * RETCODE           VARCHAR2 output parameter
     * P_LOCATION        VARCHAR2 Inventory organization / store
     * P_REGION          VARCHAR2 Geographical region of stores
     * P_ITEM_CATEGORY   VARCHAR2 Inventory item category usng the "Inventory" category set
     * P_SUPPLIER_ID     NUMBER   Supplier ID
     * P_RESERVE_GROUP   VARCHAR2 Reserve type, associated with item category (expected values are Chemicals/Time Sensitive; Specials ; All Other Inventory)
     * P_REPORT_TYPE     VARCHAR2 Determine whether to use current data or month-end data in report
     *
     * CALLED BY
     *  XXWC_INV_AGING_BRANCH_CSV concurrent program
     *
     *  Added filter for P_RESERVE_GROUP on 5-25-2012
     *  P_FTP_FLAG       VARCHAR2 Value 'N' will write output to concurrent Out file. 'Y' will generate flat file in /xx_iface -- Added for Revision 2.0 
     *************************************************************************/
    
    PROCEDURE generate_branch_csv (errbuf               OUT VARCHAR2,
                                   retcode              OUT VARCHAR2,
                                   p_region          IN     VARCHAR2,
                                   p_branch          IN     NUMBER,
                                   p_item_category   IN     VARCHAR2,
                                   p_reserve_group   IN     VARCHAR2,
                                   p_supplier_id     IN     NUMBER,
                                   p_days_low        IN     NUMBER,
                                   p_days_high       IN     NUMBER,
                                   p_ftp_flag        IN     VARCHAR2  
                                  );

                              
END xxwcinv_inv_aging_pkg;
/


