-- Start of DDL Script for Package APPS.XXWC_AR_CASH_RECEIPT_LB_UTIL
-- Generated 6/25/2012 9:03:16 PM from APPS@EBIZCON

CREATE OR REPLACE 
PACKAGE apps.xxwc_ar_cash_receipt_lb_util
/* Formatted on 25-Jun-2012 10:26:47 (QP5 v5.206) */
/* Object versions(SVN) located in \wc2020\integration\wc\ebs\prism_cash_receipts */
IS
    PROCEDURE xxwc_log (p_message VARCHAR2);

    PROCEDURE xxwc_prism_lb_driver (p_errbuf OUT VARCHAR2, p_retcode OUT VARCHAR2, p_org_id NUMBER DEFAULT NULL);

    PROCEDURE xxwc_prism_lb_import_validate (p_org_id                    NUMBER,
                                             p_trans_name                VARCHAR2,
                                             p_data_file                 VARCHAR2,
                                             p_control_file              VARCHAR2,
                                             p_transmission_format       VARCHAR2,
                                             p_lockbox_name              VARCHAR2,
                                             p_report_format             VARCHAR2,
                                             p_request_id            OUT NUMBER);

    PROCEDURE reverse_receipt (p_errbuf OUT VARCHAR2, p_retcode OUT VARCHAR2, p_org_id NUMBER);

    PROCEDURE prism_lockbox_process (errbuf                OUT VARCHAR2,
                                     retcode               OUT NUMBER,
                                     p_status           IN     VARCHAR2,
                                     p_lockbox_number   IN     VARCHAR2,
                                     p_request_id          OUT NUMBER);

    PROCEDURE xxwc_validate_prism_account (p_org_id NUMBER);

    PROCEDURE xxwc_verify_receipts (p_org_id NUMBER);

    PROCEDURE assign_cash_appl_work_items (p_errbuf OUT VARCHAR2, p_retcode OUT VARCHAR2, p_request_id OUT NUMBER);
END;
/

-- Grants for Package
GRANT EXECUTE ON apps.xxwc_ar_cash_receipt_lb_util TO xxwc_prism_execute_role
/


-- End of DDL Script for Package APPS.XXWC_AR_CASH_RECEIPT_LB_UTIL

