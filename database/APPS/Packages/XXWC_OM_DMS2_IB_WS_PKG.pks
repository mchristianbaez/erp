CREATE OR REPLACE PACKAGE APPS.XXWC_OM_DMS2_IB_WS_PKG AUTHID CURRENT_USER AS
/* $Header: XXWC_OM_DMS2_IB_WS_PKG.pls 120.16.12010000.1 2017/11/27 17:22:04 appldev ship $ */
/*#
* Table Handler to insert data in XXWC_DMS2_XML_DATA table.
* @rep:scope public
* @rep:product XXWC
* @rep:displayname Order Shipment Confirmation
* @rep:lifecycle active
* @rep:compatibility S
* @rep:category BUSINESS_ENTITY HZ_PARTIES
* @rep:ihelp FND/@o_funcsec#o_funcsec See the related online help
*/
----------------------------------------------------------------------
--
-- RECEIVE_SHIPMENT (PRIVATE)
--   Receive Order Ship Confirmation.
--   Only for use by FNDLOAD.
--
/*#
 * Receives the order ship confirmation
 * @param MLW                   IN    CLOB
 * @param X_RETURN_STATUS       OUT   NOCOPY VARCHAR2
 * @param X_MSG_DATA            OUT   NOCOPY VARCHAR2  
 * @rep:scope public
 * @rep:lifecycle active
 * @rep:displayname Receive Shipment
 * @rep:compatibility S
 * @rep:ihelp FND/@mesgdict#mesgdict See the related online help
 */
PROCEDURE RECEIVE_SHIPMENT(
 MLW                IN CLOB
,X_RETURN_STATUS    OUT   NOCOPY VARCHAR2
,X_MSG_DATA         OUT   NOCOPY VARCHAR2
);

END XXWC_OM_DMS2_IB_WS_PKG;
/