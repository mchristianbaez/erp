CREATE OR REPLACE PACKAGE APPS.XXWC_CUST_CONTACT_CLEANUP_PKG AS
/**************************************************************************
 *
 * FUNCTION | PROCEDURE | CURSOR
 *  XXWC_CUST_CONTACT_CLEANUP_PKG
 *
 * DESCRIPTION
 *  customer contact clean up (remove contacts where first and last name are the same, duplicate
 *
 * PARAMETERS
 * ==========
 * NAME              TYPE     DESCRIPTION
.* ----------------- -------- ---------------------------------------------
 * 
 *  
 *
 *
 * CALLED BY
 *   Which program, if any, calls this one
 *
 * HISTORY
 * =======
 *
 * VERSION DATE        AUTHOR(S)       DESCRIPTION
 * ------- ----------- --------------- ------------------------------------
 * 1.00    10/16/2013   Maharajan S            Creation
 *
 *************************************************************************/
 PROCEDURE XXWC_CUST_CONTACT_CLEANUP_PRC (errbuf    OUT VARCHAR2,
                                            retcode   OUT NUMBER,
                                            p_acc_number IN VARCHAR2);
END;
/
