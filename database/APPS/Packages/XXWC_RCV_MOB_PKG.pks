create or replace PACKAGE           APPS.XXWC_RCV_MOB_PKG AS

   /*****************************************************************************************************************************************
   *   $Header XXWC_RCV_MOB_PKG $                                                                                                           *
   *   Module Name: XXWC_RCV_MOB_PKG                                                                                                        *
   *                                                                                                                                        *
   *   PURPOSE:   This package is used by the extensions in MSCA                                                                            *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        09-FEB-2015  Lee Spitzer               TMS Ticket 20150302-00152                                                          *
   *                                                     RF - Receiving                                                                     *
   *****************************************************************************************************************************************/

  g_package                   VARCHAR2(30) := 'XXWC_RCV_MOB_PKG';
  g_call_from                 VARCHAR2(175);
  g_call_point                VARCHAR2(175);
  g_distro_list               VARCHAR2 (80) := 'hdsoracledevelopers@hdsupply.com';
  g_module                    VARCHAR2 (80) := 'inv';
  g_sequence                  NUMBER;
  g_debug                     VARCHAR2(1) := nvl(FND_PROFILE.VALUE('AFLOG_ENABLED'),'N'); --Fnd Debug Log = Yes / No
  g_log_level                 NUMBER := nvl(FND_PROFILE.VALUE('AFLOG_LEVEL'),6); --Fnd Debug Log Level
  g_sqlerrm                   VARCHAR2(1000);
  g_sqlcode                   NUMBER;   
  g_message                   VARCHAR2(2000);
  g_exception                 EXCEPTION; 
  g_language                  VARCHAR2(10) := userenv('LANG');
  TYPE t_genref IS REF CURSOR;
  

   /*****************************************************************************************************************************************
   *   PROCEDURE GET_PO_LOV                                                                                                                 *
   *                                                                                                                                        *
   *   PURPOSE:   This function is used to get the default open quantity for receiving mobile forms                                         *
   *                                                                                                                                        *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        09-FEB-2015  Lee Spitzer               TMS Ticket 20150302-00152                                                          *
   *                                                     RF - Receiving                                                                     *
   *****************************************************************************************************************************************/
  
    PROCEDURE GET_PO_LOV      ( x_po                     OUT    NOCOPY t_genref --0
                              , p_organization_id        IN     VARCHAR2    --1
                              , p_po_number              IN     VARCHAR2
                              );


   /*****************************************************************************************************************************************
   *   PROCEDURE GET_ITEM_LOV                                                                                                               *
   *                                                                                                                                        *
   *   PURPOSE:   This function is used to get the default open quantity for receiving mobile forms                                         *
   *                                                                                                                                        *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        09-FEB-2015  Lee Spitzer               TMS Ticket 20150302-00152                                                          *
   *                                                     RF - Receiving                                                                     *
   *****************************************************************************************************************************************/
  
    PROCEDURE GET_ITEM_LOV    ( x_item                   OUT    NOCOPY t_genref --0
                              , p_organization_id        IN     VARCHAR2 --1
                              , p_item                   IN     VARCHAR2 --2
                              , P_TRANSACTION_TYPE       IN     VARCHAR2 --3
                              , P_PO_HEADER_ID           IN     VARCHAR2 --4
                              , P_REQUISITION_HEADER_ID  IN     VARCHAR2 --5
                              , P_SHIPMENT_HEADER_ID     IN     VARCHAR2 --6
                              , P_ORDER_HEADER_ID        IN     VARCHAR2 --7
                              , p_ship_from_org_id       IN     VARCHAR2 --8
                              , p_group_id               IN     VARCHAR2 --9
                              );

   /*****************************************************************************************************************************************
   *   PROCEDURE validate_rcv_qty_val                                                                                                       *
   *                                                                                                                                        *
   *   PURPOSE:   This procedure is used to validate the receiving qty value                                                                *
   *                                                                                                                                        *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        09-FEB-2015  Lee Spitzer               TMS Ticket 20150302-00152                                                          *
   *                                                     RF - Receiving                                                                     *
   *****************************************************************************************************************************************/
  
    PROCEDURE validate_rcv_qty_val  ( p_qty         IN VARCHAR2
                                    , p_running_qty IN VARCHAR2
                                    , x_return  OUT NUMBER
                                    , x_message OUT VARCHAR2
                                    );
                                    
    

   /*****************************************************************************************************************************************
   *   PROCEDURE INSERT_INTO_RCV_INTERFACE                                                                                                  *
   *                                                                                                                                        *
   *   PURPOSE:   This procedure is used to insert records into the receiving interface table                                               *
   *                                                                                                                                        *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        09-FEB-2015  Lee Spitzer               TMS Ticket 20150302-00152                                                          *
   *                                                     RF - Receiving                                                                     *
   *****************************************************************************************************************************************/
    
    
    PROCEDURE INSERT_INTO_RCV_INTERFACE ( P_GROUP_ID IN NUMBER                        --1
                                        , P_ORGANIZATION_ID IN NUMBER                 --2
                                        , P_INVENTORY_ITEM_ID IN NUMBER               --3
                                        , P_ITEM_DESCRIPTION IN VARCHAR2              --4
                                        , P_UOM_CODE IN VARCHAR2                      --5
                                        , P_QUANTITY IN NUMBER                        --6
                                        , P_SOURCE_HEADER_ID IN NUMBER                --7
                                        , P_SUBINVENTORY_CODE IN VARCHAR2             --8
                                        , P_LOCATOR_ID IN NUMBER                      --9
                                        , P_PROJECT_ID IN NUMBER                      --10
                                        , P_TASK_ID IN NUMBER                         --11
                                        , P_TRANSACTION_TYPE IN VARCHAR2              --12
                                        , P_DOCTYPE IN VARCHAR2                       --13
                                        , P_SUPPRESS_TRAVELER IN VARCHAR2             --14
                                        , P_USER_ID IN NUMBER                         --15
                                        , P_VENDOR_ID IN NUMBER                       --16
                                        , P_VENDOR_SITE_ID IN NUMBER                  --17
                                        , X_RETURN OUT NUMBER                         --18
                                        , X_MESSAGE OUT VARCHAR2);                    --19
     


   /*****************************************************************************************************************************************
   *   PROCEDURE UPDATE_RCV_INTERFACE                                                                                                       *
   *                                                                                                                                        *
   *   PURPOSE:   This procedure is used to insert records into the receiving interface table                                               *
   *                                                                                                                                        *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        09-FEB-2015  Lee Spitzer               TMS Ticket 20150302-00152                                                          *
   *                                                     RF - Receiving                                                                     *
   *****************************************************************************************************************************************/
    
    
    PROCEDURE UPDATE_RCV_INTERFACE      ( P_GROUP_ID IN NUMBER                        --1
                                        , P_ORGANIZATION_ID IN NUMBER                 --2
                                        , P_TRANSACTION_TYPE IN VARCHAR2              --3
                                        , P_DOCTYPE IN VARCHAR2                       --4
                                        , P_FREIGHT_CARRIER_CODE IN VARCHAR2          --5
                                        , P_PACKING_SLIP IN VARCHAR2                  --6
                                        , P_BILL_OF_LADING IN VARCHAR2                --7
                                        , P_USER_ID IN NUMBER                         --8
                                        , P_ATTRIBUTE2 IN VARCHAR2                    --9
                                        , X_RETURN OUT NUMBER                         --10
                                        , X_MESSAGE OUT VARCHAR2);                    --11

   /*****************************************************************************************************************************************
   *   PROCEDURE PROCESS_RCV_MGR                                                                                                            *
   *                                                                                                                                        *
   *   PURPOSE:   This procedure is used to insert records into the receiving interface table                                               *
   *                                                                                                                                        *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        09-FEB-2015  Lee Spitzer               TMS Ticket 20150302-00152                                                          *
   *                                                     RF - Receiving                                                                     *
   *****************************************************************************************************************************************/
   
      PROCEDURE PROCESS_RCV_MGR    ( P_RCV_GROUP_ID  IN   NUMBER
                                   , X_RETURN        OUT  NUMBER
                                   , X_MESSAGE       OUT  VARCHAR2);



   /*****************************************************************************************************************************************
   *   FUNCTION  get_next_group_id                                                                                                          *
   *                                                                                                                                        *
   *   PURPOSE:   This function returns the next sequence number from rcv_interface_groups_s                                                *
   *                                                                                                                                        *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        09-FEB-2015  Lee Spitzer               TMS Ticket 20150302-00152                                                          *
   *                                                     RF - Receiving                                                                     *
   *****************************************************************************************************************************************/


      FUNCTION get_next_group_id RETURN NUMBER;
      

   /*****************************************************************************************************************************************
   *   FUNCTION  get_default_carrier                                                                                                        *
   *                                                                                                                                        *
   *   PURPOSE:   This function returns the next sequence number from rcv_interface_groups_s                                                *
   *                                                                                                                                        *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        09-FEB-2015  Lee Spitzer               TMS Ticket 20150302-00152                                                          *
   *                                                     RF - Receiving      *
   *****************************************************************************************************************************************/


      FUNCTION get_default_carrier ( P_TRANSACTION_TYPE IN VARCHAR2              --1
                                   , P_DOCTYPE IN VARCHAR2                       --2
                                   , P_SOURCE_HEADER_ID IN NUMBER)             --3
              RETURN VARCHAR2;
                                    
   /*****************************************************************************************************************************************
   *   PROCEDURE GET_REQ_LOV                                                                                                                *
   *                                                                                                                                        *
   *   PURPOSE:   This function is used to get the default open quantity for receiving mobile forms                                         *
   *                                                                                                                                        *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        09-FEB-2015  Lee Spitzer               TMS Ticket 20150302-00152                                                          *
   *                                                     RF - Receiving                                                                     *
   *****************************************************************************************************************************************/
  
    PROCEDURE GET_REQ_LOV     ( x_po                     OUT    NOCOPY t_genref --0
                              , p_organization_id        IN     VARCHAR2    --1
                              , p_req_number             IN     VARCHAR2
                              );


   /*****************************************************************************************************************************************
   *   PROCEDURE GET_RMA_LOV                                                                                                                *
   *                                                                                                                                        *
   *   PURPOSE:   This function is used to get the default open quantity for receiving mobile forms                                         *
   *                                                                                                                                        *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        09-FEB-2015  Lee Spitzer               TMS Ticket 20150302-00152                                                          *
   *                                                     RF - Receiving                                                                     *
   *****************************************************************************************************************************************/
  
    PROCEDURE GET_RMA_LOV     ( x_po                     OUT    NOCOPY t_genref --0
                              , p_organization_id        IN     VARCHAR2    --1
                              , p_rma_number             IN     VARCHAR2
                              );


   /*****************************************************************************************************************************************
   *   PROCEDURE EXECUTE_ROLLBACK                                                                                                           *
   *                                                                                                                                        *
   *   PURPOSE:   This function is used to get the default open quantity for receiving mobile forms                                         *
   *                                                                                                                                        *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        09-FEB-2015  Lee Spitzer               TMS Ticket 20150302-00152                                                          *
   *                                                     RF - Receiving                                                                     *
   *****************************************************************************************************************************************/
  
    PROCEDURE EXECUTE_ROLLBACK;
    
    
     /*****************************************************************************************************************************************
     *  PROCEDURE GET_RTV_LOV                                                                                                                 *
     *                                                                                                                                        *
     *   PURPOSE:   This procedure used for list of values for internal rtv on the mobile device, C or H                                      *
     *                                                                                                                                        *
     *    Parameters: None                                                                                                                    *
     *    Return : XXWC_LABEL_PRINTER profile                                                                                                 *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        09-FEB-2015  Lee Spitzer               TMS Ticket 20150302-00152                                                          *
     *                                                     RF - Receiving                                                                     *
     *****************************************************************************************************************************************/


    PROCEDURE GET_RTV_LOV ( x_flexvalues             OUT    NOCOPY t_genref --0
                          , p_flex_value             IN     VARCHAR2
                         );


     /*****************************************************************************************************************************************
     *  PROCEDURE PROCESS_UPDATE_RTV_DFF                                                                                                      *
     *                                                                                                                                        *
     *   PURPOSE:   This procedure used to update the shipment_line_id list of values for internal rtv on the mobile device, C or H                                      *
     *                                                                                                                                        *
     *    Parameters: None                                                                                                                    *
     *    Return : XXWC_LABEL_PRINTER profile                                                                                                 *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        09-FEB-2015  Lee Spitzer               TMS Ticket 20150302-00152                                                          *
     *                                                     RF - Receiving                                                                     *
     *****************************************************************************************************************************************/

    PROCEDURE PROCESS_UPDATE_RTV_DFF ( p_shipment_header_id    IN VARCHAR2
                                     , p_inventory_item_id      IN VARCHAR2
                                     , p_organization_id       IN VARCHAR2
                                     , p_value                 IN VARCHAR2
                                     , p_user_id               IN VARCHAR2
                                     , x_return                OUT NUMBER
                                     , x_message               OUT VARCHAR2);
                                     


     /*****************************************************************************************************************************************
     *  PROCEDURE get_po_acceptance_notes_cnt                                                                                                 *
     *                                                                                                                                        *
     *   PURPOSE:   This function calls the XXWC_PO_HELPERS.get_po_acceptance_notes_cnt                                                       *
     *                                                                                                                                        *
     *    Parameters: None                                                                                                                    *
     *    Return : 0 - No Accetpances, 1 Acceptance                                                                                           *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        09-FEB-2015  Lee Spitzer               TMS Ticket 20150302-00152                                                          *
     *                                                     RF - Receiving                                                                     *
     *****************************************************************************************************************************************/


      FUNCTION get_po_acceptance_notes_cnt  (p_po_header_id IN VARCHAR2) 
        RETURN NUMBER;


     /*****************************************************************************************************************************************
     *  PROCEDURE GET_PO_DEFER_ACCEPTANCE_LOV                                                                                                 *
     *                                                                                                                                        *
     *   PURPOSE:   This procedure used for list of values for internal rtv on the mobile device, C or H                                      *
     *                                                                                                                                        *
     *    Parameters: None                                                                                                                    *
     *    Return : XXWC_LABEL_PRINTER profile                                                                                                 *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        09-FEB-2015  Lee Spitzer               TMS Ticket 20150302-00152                                                          *
     *                                                     RF - Receiving                                                                     *
     *****************************************************************************************************************************************/


    PROCEDURE GET_PO_DEFER_ACCEPTANCE_LOV ( x_flexvalues             OUT    NOCOPY t_genref --0
                          , p_flex_value             IN     VARCHAR2
                         );


     /*****************************************************************************************************************************************
     *  PROCEDURE GET_RCV_TOTAL                                                                                                               *
     *                                                                                                                                        *
     *   PURPOSE:   This procedure used for list of values for internal rtv on the mobile device, C or H                                      *
     *                                                                                                                                        *
     *    Parameters: None                                                                                                                    *
     *    Return : Return Current Receiving Total on Interface Table                                                                          *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        09-FEB-2015  Lee Spitzer               TMS Ticket 20150302-00152                                                          *
     *                                                     RF - Receiving                                                                     *
     *****************************************************************************************************************************************/


    FUNCTION GET_RCV_TOTAL ( p_group_id  IN NUMBER
                           , p_inventory_item_id IN NUMBER
                           , p_organization_id IN NUMBER
                           , p_transaction_type IN VARCHAR2
                           )
        RETURN NUMBER;
        

     /*****************************************************************************************************************************************
     *  PROCEDURE GET_RCV_TOLERANCE                                                                                                           *
     *                                                                                                                                        *
     *   PURPOSE:   This procedure used for to get the receipt tolerance                                                                      *
     *                                                                                                                                        *
     *    Parameters: None                                                                                                                    *
     *    Return : Return Current Receiving Total on Interface Table                                                                          *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        09-FEB-2015  Lee Spitzer               TMS Ticket 20150302-00152                                                          *
     *                                                     RF - Receiving                                                                     *
     *****************************************************************************************************************************************/
        
     PROCEDURE GET_RCV_TOLERANCE ( P_ORGANIZATION_ID IN NUMBER                 --1
                                 , P_INVENTORY_ITEM_ID IN NUMBER               --2
                                 , P_SOURCE_HEADER_ID IN NUMBER                --3
                                 , P_TRANSACTION_TYPE IN VARCHAR2              --4
                                 , P_DOCTYPE IN VARCHAR2                       --5
                                 , P_QTY IN NUMBER                             --6
                                 , X_RETURN OUT NUMBER                         --7
                                 , X_MESSAGE OUT VARCHAR2);                    --8
                                 
     /*****************************************************************************************************************************************
     *  PROCEDURE GET_RCV_TXN_IFACE_ID                                                                                                        *
     *                                                                                                                                        *
     *   PURPOSE:   This procedure used for list of values for internal rtv on the mobile device, C or H                                      *
     *                                                                                                                                        *
     *    Parameters: None                                                                                                                    *
     *    Return : Return Current Receiving Total on Interface Table                                                                          *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        09-FEB-2015  Lee Spitzer               TMS Ticket 20150302-00152                                                          *
     *                                                     RF - Receiving                                                                     *
     *****************************************************************************************************************************************/


    FUNCTION GET_RCV_TXN_IFACE_ID ( p_group_id  IN NUMBER
                                  , p_inventory_item_id IN NUMBER
                                  , p_organization_id IN NUMBER
                                  , p_transaction_type IN VARCHAR2
                                  )
        RETURN NUMBER;
    
    
      /*****************************************************************************************************************************************
     *  PROCEDURE DELETE_RCV_IFACE                                                                                                            *
     *                                                                                                                                        *
     *   PURPOSE:   This procedure used for list of values for internal rtv on the mobile device, C or H                                      *
     *                                                                                                                                        *
     *    Parameters: None                                                                                                                    *
     *    Return : Return Current Receiving Total on Interface Table                                                                          *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        09-FEB-2015  Lee Spitzer               TMS Ticket 20150302-00152                                                          *
     *                                                     RF - Receiving                                                                     *
     *****************************************************************************************************************************************/

      PROCEDURE DELETE_RCV_IFACE ( p_group_id  IN NUMBER
                               , x_return    OUT NUMBER
                               , x_message   OUT VARCHAR2);
                               


     /*****************************************************************************************************************************************
     *  PROCEDURE CHECK_STUCK_GROUP_ID                                                                                                        *
     *                                                                                                                                        *
     *   PURPOSE:   This procedure used to check existing group id's on RCV Transactions Interface stuck                                      *
     *                                                                                                                                        *
     *    Parameters: None                                                                                                                    *
     *    Return : Return Current Receiving Total on Interface Table                                                                          *
     *                          > 0 means existing group_id                                                                                   *
     *                          = 0 no existing group id                                                                                      *
     *                          < 0 means multiple group_ids and reach out to IT                                                              *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        01-JUN-2015  Lee Spitzer               TMS Ticket 20150601-00248                                                          *
     *                                                     RF Break/fix - Timeout during receiving causes stuck transactions                  *
     *****************************************************************************************************************************************/

      PROCEDURE CHECK_STUCK_GROUP_ID ( p_organization_id  IN VARCHAR2     --1
                                     , p_source_header_id IN VARCHAR2     --2
                                     , p_doctype          IN VARCHAR2     --3
                                     , p_transaction_type IN VARCHAR2     --4
                                     , p_user_id          IN VARCHAR2     --5
                                     , x_return           OUT NUMBER      --6
                                     , x_message          OUT VARCHAR2);  --7
                                      
                               
                               
END XXWC_RCV_MOB_PKG;