CREATE OR REPLACE PACKAGE apps.xxwc_ahh_item_onhand_conv_pkg AS
  /***************************************************************************
  *    Script Name: Xxwc_Ahh_Item_Onhand_Conv_Pkg.Pkb
  *
  *    Interface / Conversion Name: Item On-Hand Conversion.
  *
  *    Functional Purpose: Convert On-Hand Qty Using Interface
  *
  *    History:
  *
  *    Version    Date              Author             Description
  ***************************************************************************
  *    1.0        14-Apr-2018       Naveen K           Initial development.
  *    2.0        17-Aug-2018       Rakesh Patel       TMS#20180817-00025-AHH- Minor change to Onhand conversion 
                                                       program to create decimal quantities to support UOM conversion.
                                                       Reset Error and unprocessed Records.
  ***************************************************************************/
  PROCEDURE print_debug(p_print_str IN VARCHAR2);

  FUNCTION derive_orcl_org(ahh_org_code IN VARCHAR2) RETURN VARCHAR2;

  FUNCTION convert_number(p_value IN VARCHAR2) RETURN NUMBER;

  FUNCTION get_oh_conv_factor(p_oracle_item_number IN VARCHAR2
                             ,p_ahh_item_number    IN VARCHAR2) RETURN NUMBER;
  PROCEDURE validations;

  PROCEDURE process_onhand;

  PROCEDURE itemonhand_conv_proc(errbuf          OUT VARCHAR2
                                ,retcode         OUT VARCHAR2
                                ,p_validate_only IN VARCHAR2);

END xxwc_ahh_item_onhand_conv_pkg;
/