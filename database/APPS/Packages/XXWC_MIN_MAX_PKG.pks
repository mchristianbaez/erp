create or replace 
package           XXWC_MIN_MAX_PKG as


/******************************************************************************
   NAME:       XXWC_MIN_MAX_PKG

   PURPOSE:    Automate XXWC Min-Max Planning For All Organizations - TMS Ticket 20121217-00830
   

   REVISIONS:   
   Ver        Date         Author           Description
   ---------  -----------  ---------------  ------------------------------------
   1.0        19-APR-2013  Lee Spitzer       1. Create the PL/SQL Package
   1.1        05-JUN-2013  Lee Spitzer       2. Removed Req Import Concurrent Request Launch - TMS Ticket 20130605-00994
******************************************************************************/

  PROCEDURE MAIN(
    retcode               out number
   ,errbuf                out VARCHAR2
   ,p_region              IN VARCHAR2
   ,p_supply_date         IN VARCHAR2
   ,p_demand_date         IN VARCHAR2
   ,p_restock             IN NUMBER
   ,p_supply_type         IN NUMBER
  );  
  
end XXWC_MIN_MAX_PKG;