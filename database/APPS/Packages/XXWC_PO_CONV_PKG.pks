CREATE OR REPLACE PACKAGE xxwc_po_conv_pkg
AS
/**************************************************************************
 *
 * HEADER
 *   ASL PO Conversion package
 *
 * PROGRAM NAME
 *  XXWC_PO_CONV_PKG.pks
 *
 * DESCRIPTION
 *  this package contains all of the procedures/functions used to convert the ASL-related purchase orders
 *   and to generate the receiving information for these purchase orders
 *
 * LAST UPDATE DATE   28-FEB-2013
 *   Date the program has been modified for the last time
 *
 * HISTORY
 * =======
 *
 * VERSION DATE        AUTHOR(S)       DESCRIPTION
 * ------- ----------- --------------- ------------------------------------
 * 1.00    28-FEB-2013                 Creation
 *
 * <additional records for modifications>
 *
 *************************************************************************/

PROCEDURE main(p_errbuf       OUT VARCHAR2,
               p_retcode      OUT VARCHAR2,
               p_validate_only IN VARCHAR2);

PROCEDURE receive_po(p_errbuf       OUT VARCHAR2
                    , p_retcode      OUT VARCHAR2
                    , p_po_num IN NUMBER);

END xxwc_po_conv_pkg;
/

