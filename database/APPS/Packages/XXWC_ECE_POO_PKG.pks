/* Formatted on 10/9/2014 9:16:28 AM (QP5 v5.254.13202.43190) */
CREATE OR REPLACE PACKAGE APPS.XXWC_ECE_POO_PKG
AS
   /*************************************************************************
     $Header XXWC_ECE_POO_PKG.pks $
     Module Name: XXWC_ECE_POO_PKG

     PURPOSE: Wrapper for the PO Outbound EDI program
     TMS Task Id :  20141002-00016

     REVISIONS:
     Ver        Date        Author                Description
     ---------  ----------  ------------------    ----------------
     1.0        6-Oct-2014  Manjula Chellappan    Initial Version

   **************************************************************************/

   PROCEDURE Extract_POO_Outbound (
      errbuf                OUT VARCHAR2,
      retcode               OUT VARCHAR2,
      p_output_path      IN     VARCHAR2,
      p_po_number_from   IN     VARCHAR2,
      p_po_number_to     IN     VARCHAR2,
      p_cdate_from       IN     VARCHAR2,
      p_cdate_to         IN     VARCHAR2,
      p_pc_type          IN     VARCHAR2,
      p_vendor_number    IN     VARCHAR2,
      p_vendor_site_id   IN     VARCHAR2,
      p_debug_mode       IN     NUMBER DEFAULT 0);
END XXWC_ECE_POO_PKG;