CREATE OR REPLACE PACKAGE apps.XXWC_QP_MS_EXTRACTS_ECOMM_PKG
AS
/*******************************************************************************
  * Procedure:   Price_list
  * Description: This procedure to fetch the microsites price lists
                 for loaded customers in XXWC.XXWC_QP_MS_EXTRCT_ECCOM table
  
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     11/21/2017    Pattabhi Avula  TMS#20171129-00069 Initial Version
  
********************************************************************************/

PROCEDURE price_list(retcode out VARCHAR2, errbuff out VARCHAR2);

PROCEDURE main(retcode out varchar2, errbuff out varchar2);

END; -- package spec
/