--
-- XXWC_PRICE_LIST_WEB_ADI_PKG  (Package) 
--
CREATE OR REPLACE PACKAGE APPS.xxwc_price_list_web_adi_pkg
IS
   /*************************************************************************
   *  Copyright (c) 2011 Lucidity Consulting Group
   *  All rights reserved.
   **************************************************************************
   *   $Header xxwc_price_list_web_adi_pkg.pkb $
   *   Module Name: xxwc_price_list_web_adi_pkg.pkb
   *
   *   PURPOSE:   This package is used by the Price List Conversion
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        09/01/2011  Vivek Lakaman             Initial Version
   * ***************************************************************************/
   --Define variables for logging debug messages
   g_level_unexpected   CONSTANT NUMBER := 6;
   g_level_error        CONSTANT NUMBER := 5;
   g_level_exception    CONSTANT NUMBER := 4;
   g_LEVEL_EVENT        CONSTANT NUMBER := 3;
   g_LEVEL_PROCEDURE    CONSTANT NUMBER := 2;
   g_LEVEL_STATEMENT    CONSTANT NUMBER := 1;

   g_org_id                      NUMBER;

   /*************************************************************************
    *   Procedure : Update_function
    *
    *   PURPOSE:   This procedure is used to log the debug message
    *   Parameter:
    *          IN
    *              p_list_header_id    -- List Header Id
    * ************************************************************************/

   PROCEDURE Update_function (p_list_header_id IN NUMBER);

   /*************************************************************************
    *   Procedure : LOG_MSG
    *
    *   PURPOSE:   This procedure is used to log the debug message
    *   Parameter:
    *          IN
    *              p_debug_level    -- Debug Level
    *              p_mod_name       -- Module Name
    *              p_debug_msg      -- Debug Message
    * ************************************************************************/

   PROCEDURE Log_msg (p_debug_level   IN NUMBER
                     ,p_mod_name      IN VARCHAR2
                     ,p_debug_msg     IN VARCHAR2);

   /*************************************************************************
    *   Procedure : Get_product_attr_value
    *
    *   PURPOSE:   This procedure is used to get the item number or the category
    *   Parameter:
    *          IN
    *              p_product_attribute  -- product_attribute
    *              p_product_attr_value -- product_attr_value
    * ************************************************************************/

   FUNCTION Get_product_attr_value (p_product_attribute    IN VARCHAR2
                                   ,p_product_attr_value   IN VARCHAR2)
      RETURN VARCHAR2;


   /*************************************************************************
    *   Procedure : Get_Formula_name
    *
    *   PURPOSE:   This procedure is used to get formula name
    *   Parameter:
    *          IN
    *              p_formula_id  -- product_attribute
    * ************************************************************************/

   FUNCTION Get_Formula_name (p_formula_id IN NUMBER)
      RETURN VARCHAR2;


   /*************************************************************************
    *   Procedure : import
    *
    *   PURPOSE:   This procedure is called from the WEDADI excel spreadsheet
    *   Parameter:
    *          IN
    *              p_name                -- Price List Name
    *              p_list_line_id        -- Price Line Id
    *              p_product_attr_value  -- Item Num
    *              p_product_uom_code    -- UOM Code
    *              p_operand             -- Unit Selling Price
    *              p_start_date_active   -- Start Date
    *              p_end_date_active     -- End date
    *              p_product_attribute   -- Product attribute either ITEM or ITEM_CATEGORY
    *              p_dynamic_formula_id  -- Dynamic formula Id
    *              p_static_formula_id   -- Static Formula Id
    *              p_escalator_dff       -- Escalator DFF Column
    * ************************************************************************/

   PROCEDURE import (p_list_header_id        NUMBER
                    ,p_price_list_name       VARCHAR2
                    ,p_list_line_id          NUMBER
                    ,p_product_attribute     VARCHAR2
                    ,p_product_attr_value    VARCHAR2
                    ,p_product_uom_code      VARCHAR2
                    ,p_operand               NUMBER
                    ,p_start_date_active     DATE
                    ,p_end_date_active       DATE
                    ,p_dynamic_formula_id    NUMBER
                    ,p_static_formula_id     NUMBER
                    ,p_escalator_dff         VARCHAR2
                    ,P_Precedence            NUMBER);
END xxwc_price_list_web_adi_pkg;
/

