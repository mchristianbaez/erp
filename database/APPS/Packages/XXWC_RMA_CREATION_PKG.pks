CREATE OR REPLACE PACKAGE APPS.XXWC_RMA_CREATION_PKG
AS
   /*************************************************************************
      $Header XXWC_RMA_CREATION_PKG.PKG $
      Module Name: XXWC_RMA_CREATION_PKG.PKG

      PURPOSE:   This package is used for return order creation

      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------
      1.0        04/12/2016  Niraj K Ranjan          Initial Version for TMS#20160323-00165
      
   ****************************************************************************/
   TYPE line_rec IS RECORD(line_id NUMBER, quantity NUMBER, return_reason_code VARCHAR2(30));
   TYPE line_tab IS TABLE OF line_rec INDEX BY BINARY_INTEGER;
   
   TYPE api_err_msg_rec IS RECORD(msg_index NUMBER, errmsg VARCHAR2(2000));
   TYPE api_err_msg_tab IS TABLE OF api_err_msg_rec INDEX BY BINARY_INTEGER;
    /*************************************************************************
      PROCEDURE Name: create_return_order

      PURPOSE:   To create return order

      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------
      1.0        04/12/2016  Niraj K Ranjan          Initial Version for TMS#20160323-00165
   ****************************************************************************/
   PROCEDURE create_return_order (p_order_header_id       IN     NUMBER,
                                  p_order_line_rec        IN     line_tab,
                                  p_credit_only           IN     VARCHAR2,
                                  p_credit_rebill         IN     VARCHAR2,
                                  p_org_id                IN     NUMBER,
                                  P_user_id               IN     NUMBER,
                                  P_resp_id               IN     NUMBER,
                                  P_resp_appl_id          IN     NUMBER,
                                  p_new_sales_order       OUT    NUMBER,
                                  p_new_order_hdr_id      OUT    NUMBER,
                                  p_status                OUT    VARCHAR2,
                                  p_retmsg                OUT    api_err_msg_tab
                                 );
   
END XXWC_RMA_CREATION_PKG;
/
