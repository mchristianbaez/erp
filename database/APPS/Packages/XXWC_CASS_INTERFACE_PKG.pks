CREATE OR REPLACE PACKAGE APPS.XXWC_CASS_INTERFACE_PKG
AS
   /*************************************************************************

   **************************************************************************
     $Header XXWC_CASS_INTERFACE_PKG $
     Module Name: XXWC_CASS_INTERFACE_PKG.pks

     PURPOSE:   This package is called by the concurrent programs
                XXWC CASS Detail (Weekly) Interface
                AND XXWC CASS Accrual (Monthly) Interface

     REVISIONS:
     Ver        Date        Author             Description
     ---------  ----------  ---------------   -------------------------
      1.0       29/03/2016  Neha Saini         Initial Build - TMS#  20160328-00016

   /*************************************************************************
   /*************************************************************************
     Procedure : main

     PURPOSE   :This package populate datafor CASS interface from file provided by CASS
                into staging table and then to Gl interface after validating.
                Journal import program will finally import these transaction to GL in oracle.
   ************************************************************************/
   PROCEDURE main_detail (
      errbuf                OUT      VARCHAR2,
      retcode               OUT      VARCHAR2
   );
      PROCEDURE main_accrual (
      errbuf                OUT      VARCHAR2,
      retcode               OUT      VARCHAR2
   );
END XXWC_CASS_INTERFACE_PKG;
/