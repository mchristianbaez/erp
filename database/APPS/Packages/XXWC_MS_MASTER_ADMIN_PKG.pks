create or replace package APPS.XXWC_MS_MASTER_ADMIN_PKG is
/*************************************************************************
  $Header XXWC_MS_MASTER_ADMIN_PKG.pks $
  Module Name: XXWC_MS_MASTER_ADMIN_PKG

  PURPOSE: To support the data extract for Master Administration Application

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    ----------------
  1.0        21-August-2015  Pahwa, Nancy    Initial Version TMS # 20150901-00129 

**************************************************************************/

  function get_user_name(nt_id in varchar2) return varchar2;
  function get_responsibility_name(p_resp_id in number, p_app_id in number)
    return varchar2;
  function get_role_name(p_role_id in number) return varchar2;
end XXWC_MS_MASTER_ADMIN_PKG;
/
