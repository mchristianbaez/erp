/* Formatted on 20-Mar-2014 18:58:57 (QP5 v5.206) */
-- Start of DDL Script for Package APPS.XXWC_EDI_IFACE_PKG
-- Generated 20-Mar-2014 18:58:56 from APPS@EBIZFQA

CREATE OR REPLACE PACKAGE apps.xxwc_edi_iface_pkg
IS
    PROCEDURE xxwc_check_for_new_edi_files (p_err_code OUT VARCHAR2);

    FUNCTION xxwc_edi_810_request (p_file_name IN VARCHAR2)
        RETURN NUMBER;

    FUNCTION xxwc_edi_856_request (p_file_name IN VARCHAR2)
        RETURN NUMBER;

    PROCEDURE get_concurrent_req_log_files;

    PROCEDURE send_error_email (p_request_id IN VARCHAR2, p_file_name IN VARCHAR2, p_email IN VARCHAR2);

    PROCEDURE xxwc_edi_process_driver (p_errbuf                         OUT NOCOPY VARCHAR2
                                      ,p_retcode                        OUT NOCOPY VARCHAR2
                                      ,p_process_files_with_errors                 VARCHAR2 DEFAULT NULL
                                      ,p_delete_files_with_errors                  VARCHAR2 DEFAULT NULL);

    PROCEDURE xxwc_log (p_message VARCHAR2);

    FUNCTION xxwc_oscommand_run (command IN STRING)
        RETURN VARCHAR2
    IS
        LANGUAGE JAVA
        NAME 'XXWC_OSCommand.Run(java.lang.String) return String';

    PROCEDURE run_edi_driver_dbms_job;

    PROCEDURE start_edi_dbms_job;

    FUNCTION get_invoice_date (p_file_name IN VARCHAR2)
        RETURN DATE;

    PROCEDURE populate_apex_fields (p_file_name IN VARCHAR2);

    PROCEDURE rename_edi_working_out_files (p_errbuf OUT NOCOPY VARCHAR2, p_retcode OUT NOCOPY VARCHAR2);

    FUNCTION check_cp_is_running_now (p_concurrent_program_id NUMBER)
        RETURN NUMBER;

    PROCEDURE delete_log_files;
END;
/

-- Grants for Package
GRANT EXECUTE ON apps.xxwc_edi_iface_pkg TO xxwc_prism_execute_role
/
GRANT EXECUTE ON apps.xxwc_edi_iface_pkg TO bs006141
/
GRANT EXECUTE ON apps.xxwc_edi_iface_pkg TO xxwc_dev_admin WITH GRANT OPTION
/
GRANT EXECUTE ON apps.xxwc_edi_iface_pkg TO interface_prism
/
GRANT EXECUTE ON apps.xxwc_edi_iface_pkg TO interface_xxcus
/

-- End of DDL Script for Package APPS.XXWC_EDI_IFACE_PKG
