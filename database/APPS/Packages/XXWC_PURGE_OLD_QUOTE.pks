CREATE OR REPLACE PACKAGE APPS.XXWC_PURGE_OLD_QUOTE
AS
/*******************************************************************************
  $Header XXWC_PURGE_OLD_QUOTE.pkb $
  Module Name: OM  
  
  * PURPOSE  : Package specification for TMS#20180216-00273
  * Description: This package will delete the old custom and standard quotes
                 based on parameter values
  
  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        06-MAR-2018  Pattabhi Avula        purge std and custom quotes.  
             16-APR-2018  Rakesh Patel          Added procedure to convert quote
********************************************************************************/

PROCEDURE main(ERRBUFF OUT VARCHAR2,RETCODE OUT VARCHAR2, P_QUOTE_TYPE  IN VARCHAR2, P_DATE IN VARCHAR2, P_BATCH_SIZE IN NUMBER);

PROCEDURE standard_quote(P_DATE IN DATE, P_BATCH_SIZE IN NUMBER);

PROCEDURE custom_quote(P_DATE IN DATE);

PROCEDURE std_to_wc_quote_conv( ERRBUFF       OUT VARCHAR2 
			                   ,RETCODE       OUT VARCHAR2
                               ,P_QUOTE_NUMBER IN NUMBER
							   );
							   
PROCEDURE std_to_wc_quote_conv_main( ERRBUFF       OUT VARCHAR2 
			                        ,RETCODE       OUT VARCHAR2
			                        ,P_QUOTE_NUMBER IN NUMBER
                                    ,P_FROM_DATE    IN VARCHAR2
							        ,P_TO_DATE      IN VARCHAR2
							        ,P_BATCH_SIZE   IN NUMBER
     							   );
END; -- package spec
/