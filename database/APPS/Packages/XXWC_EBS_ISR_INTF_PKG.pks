CREATE OR REPLACE PACKAGE APPS.xxwc_ebs_isr_intf_pkg
--//============================================================================
--//
--// Object Name         :: xxwc_ebs_isr_intf_pkg
--//
--// Object Type         :: Package Specification
--//
--// Object Description  :: This is an automation for Weekly ISR report..
--//
--// Version Control
--//============================================================================
--// Vers    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Krishna     01/07/2014          Initial Build - TMS#20130709-01006
--//============================================================================
AS

--//============================================================================
--//
--// Object Name         :: create_file
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This procedure creates file for the ISR report
--//
--// Version Control
--//============================================================================
--// Vers    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Krishna    01/07/2014          Initial Build - TMS#20130709-01006
--//============================================================================
PROCEDURE create_file (p_errbuf           OUT  VARCHAR2
                      ,p_retcode          OUT  NUMBER
                      ,p_directory_name   IN   VARCHAR2
                      );


--//============================================================================
--//
--// Object Name         :: main
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This procedure is called by UC4 to initiate file creation.
--//
--// Version Control
--//============================================================================
--// Vers    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Krishna    01/07/2014    Initial Build - TMS#20130709-01006 
--//============================================================================
PROCEDURE main (p_errbuf              OUT VARCHAR2
               ,p_retcode             OUT NUMBER
               ,p_ob_directory_name   IN  VARCHAR2
               );

END xxwc_ebs_isr_intf_pkg;
/