CREATE OR REPLACE package APPS.XXWC_PER_CLOSE_PEN_TRANS_PKG
/*
 --- Date: 26-FEB-2013 
 -- Author: Dheeresh Chintala 
 -- Scope: TThis package is to run Period Close Pending Transactions Report for multiple inventory organizations.
 -- TMS ticket 
 -- Update History: 
 -- ESMS ticket:224510   Date:10/22/2013 
 -- Version     Updated by          Update Date     Task (TMS or ESMS) 
 -- -------     ------------        ------------    ------------------  
 -- Ver 1.1     Dheeresh Chintala   22-OCT-2013     TMS-20131029-00408 - Period Close Pending Transactions Report - not sending email attachments
 --
 --
 -- Parameters
*/
as
  procedure main(
   retcode               out number
   ,errbuf                out VARCHAR2,
   --p_period              in  NUMBER --removed LHS 
   p_region               in VARCHAR2,  --added Ver 1.1
   p_period               in VARCHAR2
  );  
end XXWC_PER_CLOSE_PEN_TRANS_PKG;
/