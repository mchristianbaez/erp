CREATE OR REPLACE PACKAGE APPS.XXWC_PO_HAZMAT_SHIP_PKG
AS
   /*************************************************************************
     $Header XXWC_PO_HAZMAT_SHIP_PKG.pks $
     Module Name: XXWC_PO_HAZMAT_SHIP_PKG

     PURPOSE: This Package will display the Hazard item details which is
              used in po shipping goods
              
     TMS Task Id :   20140715-00037

     REVISIONS:
     Ver        Date        Author                Description
     ---------  ----------  ------------------    ----------------
     1.0        07/30/2014  Pattabhi Avula        Initial Version

   **************************************************************************/

   PROCEDURE ship_details_report(
                        errbuff           OUT    VARCHAR2,
                        retcode           OUT    VARCHAR2,
                        p_po_number       IN     po_headers_all.segment1%TYPE                     
                                   );
                                   
   PROCEDURE po_hazmat_ship_submit(p_po_num  IN  VARCHAR2
                                   ); 
END XXWC_PO_HAZMAT_SHIP_PKG;
/
