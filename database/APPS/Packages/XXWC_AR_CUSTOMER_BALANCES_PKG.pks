--
-- XXWC_AR_CUSTOMER_BALANCES_PKG  (Package) 
--
CREATE OR REPLACE PACKAGE APPS.xxwc_ar_customer_balances_pkg
IS
   /*************************************************************************
   *   Module Name: xxwc_ar_customer_balances_pkg.pkg
   *
   *   PURPOSE:   Customer open balances outbound interface. This package
   *              is called by the UC4 project WC.CUSTOMER_CREDIT.PRISM.
   *
   *   REVISIONS:
   *   Version    Date        Author                  Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        12/19/2011  Srini Gutha             Initial Version
   *   1.1        09/24/2014  Pattabhi Avula		  TMS# 20141001-00031  - 
   *												  WC Canada: MultiOrg: Week 
   *												  of Sep 22 (Pattabhi)
   * ************************************************************************/

   /*************************************************************************
    Procedure : open_balance

    PURPOSE:   Create file for customer open balances
    Parameter:
           IN
                p_directory_name      -- Directory Name
                p_file_name           -- File Name
                p_org_id              -- Operating Unit
  **************************************************************************/
   PROCEDURE open_balance (p_errbuf              OUT VARCHAR2
                          ,p_retcode             OUT VARCHAR2
                          ,p_directory_name   IN OUT VARCHAR2
                          ,p_file_name        IN OUT VARCHAR2
                          ,p_org_id           IN     NUMBER);

   /*************************************************************************
     Procedure : submit_request

     PURPOSE:   Submits the concurrent request
     Parameter:
            IN
                 p_directory_name       -- Directory Name
                 p_file_name            -- File Name
                 p_user_name            -- User Name
                 p_responsibility_name  -- Responsibility Name
                 p_org_name             -- Operating Unit Name
   **************************************************************************/
   PROCEDURE submit_request (p_directory_name        IN VARCHAR2
                            ,p_file_name             IN VARCHAR2
                            ,p_user_name             IN VARCHAR2
                            ,p_responsibility_name   IN VARCHAR2
                            ,p_org_name              IN VARCHAR2);
END xxwc_ar_customer_balances_pkg;
/

