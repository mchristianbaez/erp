CREATE OR REPLACE PACKAGE APPS.XXWC_INV_NEW_PROD_REQ_PKG
AS
   /*************************************************************************
   *  Copyright (c) 2011 Lucidity Consulting Group
   *  All rights reserved.
   **************************************************************************
   *   $Header XXWC_INV_NEW_PROD_REQ_PKG.pks $
   *   Module Name: XXWC_INV_NEW_PROD_REQ_PKG.pks
   *
   *   PURPOSE:   This package is used by the XXWC New product request workflow
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        12/01/2011  Shankar Hariharan        Initial Version
   *  1.1         03/13/2012 Shankar Hariharan        Added a procedure for cloning parts
   * 1.2          06/06/2012 Shankar Hariharan       Added Inv Category assignment
   * 1.3          04/01/2013 Shankar Hariharan       TMS Task 20130401-00537
   * 1.4          04/28/2013 Manjula Chellappan	     TMS # 20140407-00238 - New procedure POPULATE_PRODUCT_UDA
   * 1.5          06/18/2014 Praveen Pawar           Changed the data type of UPC Number field.
   * ***************************************************************************/

   --Define variables for logging debug messages
   G_LEVEL_UNEXPECTED           CONSTANT NUMBER := 6;
   G_LEVEL_ERROR                CONSTANT NUMBER := 5;
   G_LEVEL_EXCEPTION            CONSTANT NUMBER := 4;
   G_LEVEL_EVENT                CONSTANT NUMBER := 3;
   G_LEVEL_PROCEDURE            CONSTANT NUMBER := 2;
   G_LEVEL_STATEMENT            CONSTANT NUMBER := 1;

   G_CURRENT_RUNTIME_LEVEL      CONSTANT NUMBER
                                            := FND_LOG.G_CURRENT_RUNTIME_LEVEL ;
   G_MODULE_NAME                CONSTANT VARCHAR2 (60)
                                            := 'PLSQL.XXWC_INV_NEW_PROD_REQ_PKG' ;
   G_CATEGORY_SET_NAME          CONSTANT VARCHAR2 (30) := 'Inventory Category';
   G_TRX_TYPE_PARTS_ON_FLY_SO   CONSTANT VARCHAR2 (30) := 'PARTS_ON_FLY_SO';
   G_TRX_TYPE_PARTS_ON_FLY_RO   CONSTANT VARCHAR2 (30) := 'PARTS_ON_FLY_RO';


   G_COST_GROUP_ID                       NUMBER;
   G_ITEM_NUMBER                         VARCHAR2 (40);
   G_INVENTORY_ITEM_ID                   NUMBER;
   G_ORGANIZATION_ID                     NUMBER := fnd_profile.value('XXWC_ITEM_MASTER_ORG');
   G_CLONE_PARTS_SO                      VARCHAR2(30):= 'CLONE_PARTS_SO';


   -- Create A Record Structure and capture all the UI Attributes

   TYPE PARTS_ON_FLY_REC IS RECORD
   (
      ITEM_NUMBER               VARCHAR2 (40)
    , ITEM_DESCRIPTION          VARCHAR2 (240)
    , sp_item_number            VARCHAR2 (40)
    , mfg_part_number           VARCHAR2 (100)
    , INVENTORY_ITEM_ID         NUMBER
    , MST_ORGANIZATION_ID       NUMBER
    , ITEM_TEMPLATE_ID          NUMBER
    , PRIMARY_UOM_CODE          VARCHAR2 (10)
    , long_description          VARCHAR2 (4000)
    , shelf_life_days           NUMBER
    , unit_weight               NUMBER
    , tax_code                  VARCHAR2 (30)
    , HAZARDOUS_MATERIAL_FLAG   VARCHAR2 (1)
    , hazard_class              VARCHAR2 (30)
    , packaging_group           VARCHAR2 (30)
    , UN_Number                 VARCHAR2 (30)
    , container_type            VARCHAR2 (30)
    , package_labels            VARCHAR2 (30)
    , msds_sheets               VARCHAR2 (100)
    , ca_prop65_value           VARCHAR2 (100)
    , pesticide_flag            VARCHAR2 (1)
    , pesticide_flag_state      VARCHAR2 (30)
    , voc_value                 VARCHAR2 (30)
    , voc_category              VARCHAR2 (30)
    , voc_sub_category          VARCHAR2 (30)
    , po_cost                   NUMBER
    , fixed_lot_multiplier      NUMBER
    , default_buyer             NUMBER
    , country_of_origin         VARCHAR2 (30)
    , replaced_item_id          NUMBER
    , VENDOR_ID                 NUMBER
    , VENDOR_SITE_ID            NUMBER
    , CATEGORY_ID               NUMBER
    , TRANSACTION_ID            NUMBER
    , lead_time                 NUMBER
    --, upc_number                NUMBER
				, upc_number                VARCHAR2 (30)
    , sourced_from              VARCHAR2 (30)
    , sugg_list_price           NUMBER
    , request_id                NUMBER
   );

   FUNCTION get_gl_account (i_inventory_item_id in number,
                            i_organization_id in number, 
                            i_acc_type in varchar2)
   return number;

   PROCEDURE CREATE_CROSS_REF (p_segment1               IN VARCHAR2
                             , p_cross_reference_type   IN VARCHAR2
                             , p_cross_reference        IN VARCHAR2
                             , p_description            IN VARCHAR2);

   PROCEDURE CREATE_ITEM_REL (
            p_item_id              in number
          , p_rel_item_id          in number);

   PROCEDURE ASSIGN_CATEGORY (p_category_id         IN            NUMBER
                            , p_organization_id     IN            NUMBER
                            , p_Inventory_Item_ID   IN            NUMBER
                            , x_Return_Status          OUT NOCOPY VARCHAR2
                            , x_Msg_Data               OUT NOCOPY VARCHAR2
                            , x_Msg_Count              OUT NOCOPY NUMBER);
                            
                            
   PROCEDURE ASSIGN_INV_CATEGORY (p_category_id         IN            NUMBER
                            , p_organization_id     IN            NUMBER
                            , p_Inventory_Item_ID   IN            NUMBER
                            , x_Return_Status          OUT NOCOPY VARCHAR2
                            , x_Msg_Data               OUT NOCOPY VARCHAR2
                            , x_Msg_Count              OUT NOCOPY NUMBER);

   PROCEDURE CREATE_ITEM (p_Parts_On_Fly_REc   IN OUT NOCOPY Parts_On_Fly_Rec
                        , x_Return_Status         OUT NOCOPY VARCHAR2
                        , x_Msg_Data              OUT NOCOPY VARCHAR2
                        , x_Msg_Count             OUT NOCOPY NUMBER);

   PROCEDURE Launch_CreateItem_WF (itemtype    IN            VARCHAR2
                                 , itemkey     IN            VARCHAR2
                                 , actid       IN            NUMBER
                                 , funcmode    IN            VARCHAR2
                                 , resultout      OUT NOCOPY VARCHAR2);

   PROCEDURE data_Steward_reject (itemtype    IN            VARCHAR2
                                , itemkey     IN            VARCHAR2
                                , actid       IN            NUMBER
                                , funcmode    IN            VARCHAR2
                                , resultout      OUT NOCOPY VARCHAR2);


   PROCEDURE ehs_Review (itemtype    IN            VARCHAR2
                       , itemkey     IN            VARCHAR2
                       , actid       IN            NUMBER
                       , funcmode    IN            VARCHAR2
                       , resultout      OUT NOCOPY VARCHAR2);

   PROCEDURE po_Review (itemtype    IN            VARCHAR2
                      , itemkey     IN            VARCHAR2
                      , actid       IN            NUMBER
                      , funcmode    IN            VARCHAR2
                      , resultout      OUT NOCOPY VARCHAR2);

   PROCEDURE pricing_Review (itemtype    IN            VARCHAR2
                           , itemkey     IN            VARCHAR2
                           , actid       IN            NUMBER
                           , funcmode    IN            VARCHAR2
                           , resultout      OUT NOCOPY VARCHAR2);

   PROCEDURE activate_item (itemtype    IN            VARCHAR2
                          , itemkey     IN            VARCHAR2
                          , actid       IN            NUMBER
                          , funcmode    IN            VARCHAR2
                          , resultout      OUT NOCOPY VARCHAR2);
                          
   PROCEDURE process_item_org_assignments (l_trx_id      in number,
                                           l_inv_item_id in number,
                                           l_org_id      in number,
                                           l_uom_code    in varchar2,
                                           l_item_cost   in number,
                                           l_from_org_id in number, 
                                           o_ret_status  out varchar2,
                                           o_ret_msg     out varchar2);
   PROCEDURE SET_ITEM_NUMBER_NULL(P_TRANSACTION_ID   IN NUMBER) ; 
      
    
    PROCEDURE SET_ITEM_NUMBER (P_INVENTORY_ITEM_ID  IN NUMBER , 
                               P_ITEM_NUMBER        IN VARCHAR2,
                               P_TRANSACTION_TYPE   IN VARCHAR2, 
                               P_ORGANIZATION_ID    IN NUMBER, 
                               P_ORGANIZATION_CODE  IN VARCHAR2,
                               P_TRANSACTION_ID       IN NUMBER) ;
    
    FUNCTION GET_ITEM_NUMBER (P_TRANSACTION_ID  IN NUMBER, 
                              P_TRANSACTION_TYPE IN VARCHAR2) RETURN VARCHAR2; 
    
    FUNCTION GET_INVENTORY_ITEM_ID (P_TRANSACTION_ID  IN NUMBER, 
                              P_TRANSACTION_TYPE IN VARCHAR2) RETURN NUMBER;  
                              
    PROCEDURE update_unit_cost_on_so (errbuf OUT varchar2,
                                      retcode OUT varchar2);  

    PROCEDURE POPULATE_PRODUCT_UDA(p_inv_item_id IN NUMBER, p_error_msg OUT VARCHAR2);
	
END XXWC_INV_NEW_PROD_REQ_PKG;
/
sho err
/
