CREATE OR REPLACE PACKAGE APPS.xxwcar_dctm_pkg IS

  /**************************************************************************
  File Name: XXWCAR_DCTM_PKG
  
  PROGRAM TYPE: SQL Script
  
  PURPOSE:      package is used to create files to be used in AnyDoc (OCR) system
                as a lookup tool
  
  HISTORY
  =============================================================================
         Last Update Date : 11/23/2011
  =============================================================================
  =============================================================================
  VERSION DATE          AUTHOR(S)          DESCRIPTION
  ------- -----------   -----------------  ------------------------------------
  1.0     23-Nov-2011   Kathy Poling       Creation this package 
  1.1     28-May-2012   Kathy Poling       Added record count and dms_output 
                                           for being displayed in UC4
  1.2     10-Mar-2014   Maharajan          TMS# 20130908-00094
            Shunmugam          Added new procedure update_short_code
  =============================================================================
  *****************************************************************************/

  PROCEDURE open_trns(errbuf  OUT VARCHAR2,
                      retcode OUT NUMBER,
                      p_count OUT NUMBER);

  PROCEDURE customer(errbuf  OUT VARCHAR2,
                     retcode OUT NUMBER,
                     p_count OUT NUMBER);

  FUNCTION get_disc_date(p_term     NUMBER,
                         p_trx_date DATE,
                         p_due_date IN DATE) RETURN DATE;

  FUNCTION get_disc_amt(p_term IN NUMBER, p_trx_id IN NUMBER) RETURN NUMBER;

  PROCEDURE dctm_lockbox(errbuf OUT VARCHAR2, retcode OUT NUMBER);

  PROCEDURE uc4_load_dctm(errbuf           OUT VARCHAR2,
                          retcode          OUT NUMBER,
                          p_directory      IN VARCHAR2,
                          p_user           IN VARCHAR2,
                          p_responsibility IN VARCHAR2);

  PROCEDURE update_short_code(p_payment_schedule_id IN NUMBER,
                              p_short_code          IN VARCHAR2);

  PROCEDURE inv_short_code(errbuf OUT VARCHAR2, retcode OUT NUMBER);

  PROCEDURE uc4_submit_inv_short_code(errbuf                OUT VARCHAR2,
                                      retcode               OUT NUMBER,
                                      p_user_name           IN VARCHAR2,
                                      p_responsibility_name IN VARCHAR2);

END xxwcar_dctm_pkg;
/
