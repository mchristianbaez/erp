CREATE OR REPLACE PACKAGE APPS.XXWC_INV_PRIMARY_BIN_PKG AS

   /*****************************************************************************************************************************************
   *   $Header XXWC_INV_PRIMARY_BIN_PKG $                                                                                                      *
   *   Module Name: XXWC_INV_PRIMARY_BIN_PKG                                                                                                   *
   *                                                                                                                                        *
   *   PURPOSE:   This package is used by the extensions in MSCA                                                                            *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        16-OCT-2014  Lee Spitzer               TMS Ticket 20141001-00198                                                          *
   *                                                     WC Canada: Enhancement: RFID / Mobile Supply Chain RFID / Mobile Supply Chain      *
   *****************************************************************************************************************************************/

  g_package                   VARCHAR2(30) := 'XXWC_INV_PRIMARY_BIN_PKG';
  g_call_from                 VARCHAR2(175);
  g_call_point                VARCHAR2(175);
  g_distro_list               VARCHAR2 (80) := 'hdsoracledevelopers@hdsupply.com';
  g_module                    VARCHAR2 (80) := 'inv';
  g_sequence                  NUMBER;
  g_debug                     VARCHAR2(1) := nvl(FND_PROFILE.VALUE('AFLOG_ENABLED'),'N'); --Fnd Debug Log = Yes / No
  g_log_level                 NUMBER := nvl(FND_PROFILE.VALUE('AFLOG_LEVEL'),6); --Fnd Debug Log Level
  g_sqlerrm                   VARCHAR2(1000);
  g_sqlcode                   NUMBER;   
  g_message                   VARCHAR2(2000);
  g_exception                 EXCEPTION; 
  g_language                  VARCHAR2(10) := userenv('LANG');
  g_gtin_cross_ref_type VARCHAR2(25) := fnd_profile.VALUE('INV:GTIN_CROSS_REFERENCE_TYPE');
  g_gtin_code_length NUMBER := 14;
  TYPE t_genref IS REF CURSOR;
  
                              
   /*****************************************************************************************************************************************
   *   PROCEDURE GET_DEFAULT_TYPE_LOCAOTRS_LOV                                                                                                           *
   *                                                                                                                                        *
   *   PURPOSE:   This procedure is used to get the default type for item transaction defaults                                              *
   *                                                                                                                                        *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        22-OCT-2014  Lee Spitzer               TMS Ticket 20141001-00198                                                          *
   *                                                     WC Canada: Enhancement: RFID / Mobile Supply Chain RFID / Mobile Supply Chain      *
   *****************************************************************************************************************************************/
  
    PROCEDURE GET_DEFAULT_TYPE_LOCATORS_LOV  ( x_default                   OUT    NOCOPY t_genref --0
                                             ,  p_description               IN      VARCHAR2
                                             );
                                             
      /*****************************************************************************************************************************************
     *   PROCEDURE PROCESS_ITEM_DEFAULT_LOCATOR                                                                                               *
     *                                                                                                                                        *
     *   PURPOSE:   This procedure is used to insert or update MTL_ITEM_SUB_DEFAULTS AND MTL_ITEM_LOC_DEFAULTS                                *
     *                                                                                                                                        *
     *    Parameters: p_organization_id  IN NUMBER - organization_id                                                                          *
     *                p_inventory_item_id IN NUMBER -- inventory_item_id                                                                      *
     *                p_subinventory_code IN VARCHAR2 - subinventory_code                                                                     *
     *                p_locator_id IN NUMBER --LOCATOR_ID                                                                                     *
     *                p_default_type  IN  NUMBER - 1 = Shipping, 2 = Receiving, 3 = Move Order Requisition, lookup type from MFG_LOOKUPS of 'MTL_DEFAULT_LOCATORS'
     *                                                                                                                                        *
     *    Out :  X_RETURN = 0 = Success, 1 = Error                                                                                            *
     *           X_MESSAGE = Return message                                                                                                   *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        22-OCT-2014  Lee Spitzer               TMS Ticket 20141001-00198                                                          *
     *                                                     WC Canada: Enhancement: RFID / Mobile Supply Chain RFID / Mobile Supply Chain      *
     *********y********************************************************************************************************************************/

        PROCEDURE PROCESS_ITEM_DEFAULT_LOCATOR   ( p_organization_id    IN VARCHAR2
                                                 , p_inventory_item_id  IN VARCHAR2
                                                 , p_subinventory_code  IN VARCHAR2
                                                 , p_locator_id IN VARCHAR2
                                                 , p_default_type IN VARCHAR2
                                                 , x_return  OUT NUMBER
                                                 , x_message OUT VARCHAR2);

  /*****************************************************************************************************************************************
     *   PROCEDURE PROCESS_ITEM_SUB_MAX                                                                                                       *
     *                                                                                                                                        *
     *   PURPOSE:   This procedure is used to insert or update MTL_ITEM_SUB_INVENTORIES                                                       *
     *                                                                                                                                        *
     *    Parameters: p_organization_id  IN NUMBER - organization_id                                                                          *
     *                p_inventory_item_id IN NUMBER -- inventory_item_id                                                                      *
     *                p_subinventory_code IN VARCHAR2 - subinventory_code                                                                     *
     *                p_max_qty  IN  NUMBER                                                                                                   *
     *                                                                                                                                        *
     *    Out :  X_RETURN = 0 = Success, 1 = Error                                                                                            *
     *           X_MESSAGE = Return message                                                                                                   *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        22-OCT-2014  Lee Spitzer               TMS Ticket 20141001-00198                                                          *
     *                                                     WC Canada: Enhancement: RFID / Mobile Supply Chain RFID / Mobile Supply Chain      *
     *********y********************************************************************************************************************************/

        PROCEDURE PROCESS_ITEM_SUB_MAX           ( p_organization_id    IN VARCHAR2
                                                 , p_inventory_item_id  IN VARCHAR2
                                                 , p_subinventory_code  IN VARCHAR2
                                                 , p_max_qty IN VARCHAR2
                                                 , x_return  OUT NUMBER
                                                 , x_message OUT VARCHAR2);

     /*****************************************************************************************************************************************
     *   PROCEDURE PROCESS_ITEM_LOCATOR_TIE                                                                                                    *
     *                                                                                                                                        *
     *   PURPOSE:   This procedure is used to update or insert records into the MTL_SECONDARY_LOCATORS                                        *
     *                                                                                                                                        *
     *    Parameters: p_organization_id  IN NUMBER - organization_id                                                                          *
     *                p_inventory_item_id IN NUMBER -- inventory_item_id                                                                      *
     *                p_subinventory_code IN VARCHAR2 - subinventory_code                                                                     *
     *                p_locator_id IN NUMBER --LOCATOR_ID                                                                                     *
     *                                                                                                                                        *
     *    Out :  X_RETURN = 0 = Success, 1 = Error                                                                                            *
     *           X_MESSAGE = Return message                                                                                                   *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        22-OCT-2014  Lee Spitzer               TMS Ticket 20141001-00198                                                          *
     *                                                     WC Canada: Enhancement: RFID / Mobile Supply Chain RFID / Mobile Supply Chain      *
     *********y********************************************************************************************************************************/

        PROCEDURE PROCESS_ITEM_LOCATOR_TIE       ( p_organization_id    IN VARCHAR2
                                                 , p_inventory_item_id  IN VARCHAR2
                                                 , p_subinventory_code  IN VARCHAR2
                                                 , p_locator_id IN VARCHAR2
                                                 , x_return  OUT NUMBER
                                                 , x_message OUT VARCHAR2);
         
    
    /*****************************************************************************************************************************************
   *   PROCEDURE GET_MILD_INQUIRY                                                
   *   PURPOSE:   This procedure is used to get the results from MTL_ITEM_LOC_DEFAULTS                                                      *
   *                                                                                                                                        *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        22-OCT-2014  Lee Spitzer               TMS Ticket 20141001-00198                                                          *
   *                                                     WC Canada: Enhancement: RFID / Mobile Supply Chain RFID / Mobile Supply Chain      *
   *****************************************************************************************************************************************/
  
    PROCEDURE GET_MILD_INQUIRY  ( x_default                   OUT    NOCOPY t_genref --0
                                , p_organization_id           IN     VARCHAR2
                                , p_inventory_item_id         IN     VARCHAR2
                                , p_subinventory_code         IN     VARCHAR2
                                , p_locator_id                IN     VARCHAR2
                                , p_default_type              IN     VARCHAR2
                                );
                     
    
END XXWC_INV_PRIMARY_BIN_PKG;
