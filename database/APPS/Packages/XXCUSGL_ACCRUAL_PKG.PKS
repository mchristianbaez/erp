
  CREATE OR REPLACE PACKAGE "APPS"."XXCUSGL_ACCRUAL_PKG" IS

  /*******************************************************************************
  * Procedure:   LOAD_TE_ACCRUAL
  * Description: Create accrual entry for unprocessed credit card transactions
  *              during month end close
  *
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     08/12/2009    Kathy Poling    Initial creation of the procedure
   
  ********************************************************************************/

  PROCEDURE load_te_accrual(p_errbuf  OUT VARCHAR2,
                            p_retcode OUT NUMBER,
                            p_fperiod IN VARCHAR2);

  PROCEDURE load_payroll_accrual(p_errbuf        OUT VARCHAR2,
                                 p_retcode       OUT NUMBER,
                                 p_fperiod       IN VARCHAR2,
                                 p_lob           IN VARCHAR2,
                                 p_parent        IN VARCHAR2,
                                 p_batch_name    IN VARCHAR2,
                                 p_type_acct     IN VARCHAR2,
                                 p_time          IN VARCHAR2,
                                 p_accrue_branch IN VARCHAR2);

  PROCEDURE load_headcount(p_errbuf  OUT VARCHAR2,
                           p_retcode OUT NUMBER,
                           p_fperiod IN VARCHAR2);

END xxcusgl_accrual_pkg;
;
