create or replace 
PACKAGE      XXWC_AUTHBUYER_DACT_WEBADI_PKG
/********************************************************************************
FILE NAME: APPS.XXWC_AUTHBUYER_DACT_WEBADI_PKG.pkg

PROGRAM TYPE: PL/SQL Package

PURPOSE: API to de-active the customer cotact roles for auth buyers in Oracle.

HISTORY
===============================================================================
VERSION DATE          AUTHOR(S)         DESCRIPTION
------- -----------   --------------- -----------------------------------------
1.0     10/23/2017    Pattabhi Avula    TMS#20171003-00065 Initial version
********************************************************************************/
AS
   PROCEDURE IMPORT_CUST_ACCOUNTS (p_role             IN VARCHAR2,
                                   p_account_number   IN VARCHAR2,
                                   p_cust_acct_id     IN NUMBER
                                   );
END XXWC_AUTHBUYER_DACT_WEBADI_PKG;
/