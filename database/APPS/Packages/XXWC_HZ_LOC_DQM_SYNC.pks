CREATE OR REPLACE PACKAGE APPS.XXWC_HZ_LOC_DQM_SYNC
AS
   /* ************************************************************************
    HD Supply
    All rights reserved.
   **************************************************************************

     PURPOSE:Invoke party site update and DQM Sync Serial Index automagically so that end users can search the 
             customers using the location name from the customers UI, credit management and the iReceivables UI.   

     REVISIONS:
     Ticket                 Ver         Date         Author                     Description
     ---------              ----------  ----------   ------------------------   -------------------------
     TMS 20121217-00636     1.0         06/05/2015    Balaguru Seshadri          Invoke party site update and DQM Sync Serial Index automagically
   ************************************************************************* */
   --
   Function update_party_site
                     (
                        P_Subscription_Guid In Raw
                       ,P_Event             In Out Nocopy Wf_Event_T
                     ) Return Varchar2;                                          
END XXWC_HZ_LOC_DQM_SYNC;
/