CREATE OR REPLACE PACKAGE APPS.XXWC_OM_REPAIRS_PKG
   /*************************************************************************
     $Header XXWC_OM_REPAIRS_PKG $
     Object Name: XXWC_OM_REPAIRS_PKG.pks

     PURPOSE:   This package is used to create RepairType setups

     REVISIONS:
     Ver        Date        Author              Description
     ---------  ----------  ---------------     -------------------------
     1.0        07/01/2013  Gopi Damuluri       Initial Version
   /*************************************************************************/
AS

  PROCEDURE create_rep_type_trans(p_errbuf   OUT   VARCHAR2,
                                  p_retcode  OUT   VARCHAR2);
END XXWC_OM_REPAIRS_PKG;
/