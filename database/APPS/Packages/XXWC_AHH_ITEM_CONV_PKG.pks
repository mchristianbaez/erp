CREATE OR REPLACE PACKAGE apps.xxwc_ahh_item_conv_pkg IS
  /***************************************************************************
  *    Script Name: xxwc_ahh_item_conv_pkg.pks
  *
  *    Interface / Conversion Name: Item conversion.
  *
  *    Functional Purpose: Convert Items using Interface for A.H Harris
  *
  *    History:
  *
  *    Version    Date             Author      Description
  *****************************************************************************
  *    1.0        06/04/2018       Naveen K    Initial Development.
  *****************************************************************************/

  -- Prints Debug/Log Message - Checks for Debug Flag.
  PROCEDURE print_debug(p_print_str IN VARCHAR2);

  -- Print Log Message - Ignores Debug Flag.
  PROCEDURE print_log(p_print_str IN VARCHAR2);

  -- Convert Varchar2 into Number function
  FUNCTION convert_number(pid     IN ROWID
                         ,p_col   IN VARCHAR2
                         ,p_value IN VARCHAR2) RETURN NUMBER;

  -- Cleans New Line, Tab and Space characters.
  PROCEDURE cleanup_tables;

  -- Validations
  PROCEDURE validations;

  -- Derive UN Number for Oracle Item Number
  FUNCTION derive_unnumber(p_item     IN VARCHAR2
                          ,p_unnumber IN VARCHAR2) RETURN po_un_numbers.un_number_id%TYPE;

  -- Derive Hazard Class for Oracle Item Number
  FUNCTION derive_hazard_class_id(p_item         IN VARCHAR2
                                 ,p_hazard_class IN VARCHAR2)
    RETURN po_hazard_classes.hazard_class_id%TYPE;

  -- Get Item Description from Base Table.
  FUNCTION get_mst_desc(p_item VARCHAR2
                       ,p_desc VARCHAR2) RETURN VARCHAR2;

  -- Get Item Long Description from Base Table.
  FUNCTION get_mst_longdesc(p_item     VARCHAR2
                           ,p_longdesc VARCHAR2) RETURN VARCHAR2;

  -- Replace criteria for AVP Code.
  FUNCTION derive_avp_code(p_avp_code IN VARCHAR2) RETURN VARCHAR2;

  -- Get Item Type from User Item Type.
  FUNCTION derive_item_type(p_item_type IN VARCHAR2) RETURN VARCHAR2;

  -- Get User Item Type from Item Type.
  FUNCTION derive_user_item_type(p_internal_item_type IN VARCHAR2) RETURN VARCHAR2;

  -- Get Template ID from Item Type and Status
  FUNCTION derive_template_id(p_item_type   IN VARCHAR2
                             ,p_item_status IN VARCHAR2) RETURN NUMBER;

  -- Get Template ID for Master Item from Item Type and Status
  FUNCTION derive_mst_template_id(p_item_type   IN VARCHAR2
                                 ,p_item_status IN VARCHAR2) RETURN NUMBER;

  -- Get UOM Code from Unit Of Measure.
  FUNCTION derive_uom_code(p_uom IN VARCHAR2) RETURN VARCHAR2;

  -- Get Category ID from Concatenated segment
  FUNCTION derive_category_id(p_category_concat IN VARCHAR2
                             ,p_category_set    IN VARCHAR2) RETURN VARCHAR2;

  -- Get Oracle Org Code from AHH Org Code.
  FUNCTION derive_orcl_org(ahh_org_code IN VARCHAR2) RETURN VARCHAR2;

  -- Get Oracle Organization ID from AHH Org Code.
  FUNCTION derive_orcl_org_id(p_inv_org IN VARCHAR2) RETURN VARCHAR2;

  -- Get COGS and Sales CCID at Org Level. * No Creation of CCID
  FUNCTION derive_ccid(p_inv_org   IN VARCHAR2
                      ,p_ccid_name IN VARCHAR2) RETURN NUMBER;
  -- FUNCTION: check_and_create_ccid, Create and Check CCIDs for COGS, Sales and Expense Account.
  FUNCTION check_and_create_ccid(p_segments apps.fnd_flex_ext.segmentarray)
  RETURN NUMBER;
  -- FUNCTION: Populate_Ccid, populate CCIDs for COGS, Sales and Expense Account.
  PROCEDURE populate_ccid(p_segment1     IN VARCHAR2,
                        p_inv_org      IN VARCHAR2,
                        x_cogs_ccid    OUT NUMBER,
                        x_sales_ccid   OUT NUMBER,
                        x_expense_ccid OUT NUMBER);
  -- Get Oracle Item Type from AHH Org Ref Table.
  FUNCTION derive_ref_item_type(p_icsw_statusty IN VARCHAR2) RETURN VARCHAR2;

  -- Get SalesVelocity from velocityclassification referring AHH Org Ref Table.
  FUNCTION derive_salesvelocity(p_sv        IN VARCHAR2
                               ,p_item_type IN VARCHAR2) RETURN VARCHAR2;


  -- Get Item Source Type - Supplier or Inventory
  FUNCTION derive_source_type(p_src_from VARCHAR2) RETURN NUMBER;

  -- Get Oracle Source Organization ID from Source Type and AHH Org Code
  FUNCTION derive_source_org(p_src_type IN NUMBER
                            ,p_inv_org  IN VARCHAR2) RETURN NUMBER;

  -- Default Master controlled attributes to Interface table.
  PROCEDURE post_run_org_interface;

  -- Main Item Conversion Program
  PROCEDURE process_item(errbuf               OUT VARCHAR2
                        ,retcode              OUT NUMBER
                        ,p_validation_only    IN VARCHAR2
                        ,p_include_mst_items  IN VARCHAR2
                        ,p_include_org_items  IN VARCHAR2
                        ,p_include_categories IN VARCHAR2
                        ,p_item1              IN VARCHAR2
                        ,p_item2              IN VARCHAR2
                        ,p_item3              IN VARCHAR2);

  -- Main Item Locators Conversion Program
  PROCEDURE process_item_locators(errbuf  OUT VARCHAR2
                                 ,retcode OUT VARCHAR2
                                 ,p_item1 IN VARCHAR2
                                 ,p_item2 IN VARCHAR2
                                 ,p_item3 IN VARCHAR2);

  -- Main Item CrossReferences Conversion Program
  PROCEDURE process_itemxref(errbuf      OUT VARCHAR2
                            ,retcode     OUT VARCHAR2
                            ,p_ahh_item1 IN VARCHAR2
                            ,p_ahh_item2 IN VARCHAR2
                            ,p_ahh_item3 IN VARCHAR2);

  -- Create EGO Master Vendor Number Attributes
  PROCEDURE process_mst_vendor_attrs(p_submit_import_flag VARCHAR2);

  -- Derive Vendor ID, Number & Site ID for Sourcing Rule.
  PROCEDURE get_vendor_info(p_msib_organization_id     IN NUMBER
                           ,p_mref_icsp_prod           IN VARCHAR2
                           ,p_mref_vendor_owner_number IN VARCHAR2
                           ,x_vendor_id                OUT NUMBER
                           ,x_vendor_num               OUT VARCHAR2
                           ,x_vendor_name              OUT VARCHAR2
                           ,x_vendor_site_id           OUT NUMBER);

  -- Create Source Rule and Assignments
  PROCEDURE assign_sourcing_rule(x_return_status OUT NOCOPY VARCHAR2
                                ,x_msg_data      OUT NOCOPY VARCHAR2
                                ,x_msg_count     OUT NOCOPY NUMBER);

  -- Main Post Processor program: Sourcing Rules and Master Vendor Attrs
  PROCEDURE process_item_post_processor(errbuf                     OUT VARCHAR2
                                       ,retcode                    OUT NUMBER
                                       ,p_taxware_code_update_flag IN VARCHAR2
                                       ,p_master_vendor_num_attr   IN VARCHAR2
                                       ,p_assign_sourcing_rule     IN VARCHAR2
                                       ,p_create_subinv_defaults   IN VARCHAR2);
END xxwc_ahh_item_conv_pkg;