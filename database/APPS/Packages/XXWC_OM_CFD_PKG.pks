CREATE OR REPLACE PACKAGE xxwc_om_cfd_pkg IS
  /********************************************************************************
  
  FILE NAME: APPS.XXWCAP_INV_INT_PKG.pks
  
  PROGRAM TYPE: PL/SQL Package
  
  PURPOSE: APIs for Order Management Customer Facing Documents.
  
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     01/08/2013    Gopi Damuluri    Initial version.
  1.1     02/18/2013    Gopi Damuluri    Added a new function - secondary_tax
  1.2     06/05/2013    Ram Talluri      Added secondary_tax_wc_quote - TMS #20130529-01469
  1.3     07/18/2013    Gopi Damuluri    TMS# 20130618-01287
                                         * Time stamp based on the branch's timezone
  1.4     12/20/2013    Gopi Damuluri    TMS# 20131010-00323
                                         -- Display Country of Origin(COO) 
                                         -- GSA TAA Compliance
  1.10    02/14/2018    Pattabhi Avula    TMS#20170314-00267 Auth Buyer CFD Enhancement
  1.11    04/12/2018    Naveen Kalidindi TMS# 20171011-00160,20180723-00096  - Prop65 Oracle Compliance Changes.
  ********************************************************************************/
  FUNCTION notify_gsa(p_order_header_id IN NUMBER
                     ,p_order_line_id   IN NUMBER) RETURN VARCHAR2;

  PROCEDURE notify_gsa_hdr(p_errbuf          OUT VARCHAR2
                          ,p_retcode         OUT VARCHAR2
                          ,p_order_header_id IN NUMBER DEFAULT NULL);

  FUNCTION vendor_part_num(p_inventory_item_id IN NUMBER) RETURN VARCHAR2;

  FUNCTION unit_selling_price(p_unit_selling_price IN NUMBER) RETURN NUMBER;

  FUNCTION secondary_tax(p_order_header_id  IN NUMBER
                        ,p_st_site_use_id   IN NUMBER
                        ,p_ship_from_org_id IN NUMBER
                        ,p_organization_id  IN NUMBER
                        ,p_delivery_id      IN NUMBER) -- Version# 1.4
   RETURN NUMBER;

  --Ram Talluri TMS #TMS #20130529-01469 --START 
  FUNCTION secondary_tax_wc_quote(p_order_header_id  IN NUMBER
                                 ,p_st_site_use_id   IN NUMBER
                                 ,p_ship_from_org_id IN NUMBER
                                 ,p_organization_id  IN NUMBER) RETURN NUMBER;
  --Ram Talluri TMS #TMS #20130529-01469 --END    

  -- Version 1.3 > Start
  FUNCTION get_branch_dts(p_branch_name IN VARCHAR2) RETURN VARCHAR2;
  -- Version 1.3 < End

  -- Version# 1.4 > Start
  FUNCTION get_country_of_origin(p_inv_to_org_id IN NUMBER
                                ,p_inv_item_id   IN NUMBER) RETURN VARCHAR2;
  -- Version# 1.4 < End

  -- Version# 1.10 > Start
  FUNCTION get_auth_buyer(p_auth_buyer_flag IN VARCHAR2
                         ,p_auth_buyer_id   IN NUMBER) RETURN VARCHAR2;
  -- Version# 1.10 < End

  -- Version# 1.11 > Start
  FUNCTION get_prop65_message(p_inventory_item_id IN NUMBER
                             ,p_remove_warning    IN VARCHAR2 DEFAULT 'N'
                             ,p_remove_link       IN VARCHAR2 DEFAULT 'N') RETURN VARCHAR2;
  
  FUNCTION get_prop65_message_ecomm(p_inventory_item_id IN NUMBER) RETURN VARCHAR2;
  -- Version# 1.11 < End


END xxwc_om_cfd_pkg;
/
