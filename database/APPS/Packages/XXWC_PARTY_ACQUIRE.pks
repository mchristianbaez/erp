CREATE OR REPLACE PACKAGE APPS.XXWC_PARTY_ACQUIRE AUTHID CURRENT_USER  AS

----------------------------------------------------------------------------------------------
-- Filename:     XXWC_PARTY_ACQUIRE.pks  
--
-- Purpose:      This package is developed to support search functionality from DQM.
--
-- Source Table: 1. NA 
--
-- Destination Table: 1. NA  
--
-- Developer: Sandeep Surapaneni 
--
-- Date:       05/23/2013   
--
-- Modification History
--
--  Version    Person                Date            Comments
-- --------  -------------------  -----------    -----------------------------------------------
--  1.0      Sandeep Surapaneni    05/23/2013      Program written  
------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------
-- F U N C T I O N  # 1 
--
-- Procedure: get_prism_account  
--
-- Purpose:  1.To derive customer record based on  Prism Account
------------------------------------------------------------------------------------------------
  
      
      FUNCTION get_prism_account(
                                 p_party_id      IN      NUMBER,
                                 p_entity        IN      VARCHAR2,
                                 p_attribute     IN      VARCHAR2,
                                 p_context       IN      VARCHAR2   DEFAULT NULL
                                )
        RETURN VARCHAR2;


------------------------------------------------------------------------------------------------
-- F U N C T I O N  # 2 
--
-- Procedure: get_location_info   
--
-- Purpose:  1.To derive customer record based on Location 
------------------------------------------------------------------------------------------------

      FUNCTION get_location_info(
                                 p_party_site_id    IN    NUMBER,
                                 p_entity           IN    VARCHAR2,
                                 p_attribute        IN    VARCHAR2,
                                 p_context          IN    VARCHAR2 DEFAULT NULL
                                )
        RETURN VARCHAR2;


------------------------------------------------------------------------------------------------
-- F U N C T I O N  # 3 
--
-- Procedure: get_prism_site_info  
--
-- Purpose:  1.To derive customer record based on Prism Site  
------------------------------------------------------------------------------------------------

      FUNCTION get_prism_site_info(
                                   p_party_site_id    IN    NUMBER,
                                   p_entity           IN    VARCHAR2,
                                   p_attribute        IN    VARCHAR2,
                                   p_context          IN    VARCHAR2 DEFAULT NULL
                                  )
        RETURN VARCHAR2;
        


END;
/
