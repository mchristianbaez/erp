CREATE OR REPLACE PACKAGE APPS.xxcus_currentday_cash_pkg as
   /*
      ************************************************************************
    HD Supply
    All rights reserved.
   **************************************************************************
     PURPOSE:   GSC Current day cash positioning BAI file process 

     REVISIONS:
     Ver        Date                Author                                         Description
     ---------  ----------         ---------------                                 ------------------------------------------------------------------------------------------------------------
     1.0       09/21/2016   Balaguru Seshadri                   TMS:  20161026-00104 / ESMS 322411 - GSC Current day cash positioning BAI file process
     1.1       08/15/2018	Vamshi Singirikonda               TMS:  20180815-00062 / HDS Treasury: Remove OEBS uc4 job item and replace with SQL wrapper
     1.2       08/30/2018   Balaguru Seshadri                  TMS:  20180731-00086 / HDS Treasury: Fix to remove the payroll wires from WellsFargo File     
   ************************************************************************* 
  */
 -- 
  procedure setup_prereqs
   (
     retcode out varchar2
    ,errbuf  out varchar2
    ,p_max_files_per_day in number
    ,p_email in varchar2
    ,p_cc_email in varchar2
   );
 --
 procedure backup_bankfile
   (
     retcode out varchar2
    ,errbuf  out varchar2
   ); 
 --
 procedure kickoff_sql_loader
   (
     retcode out varchar2
    ,errbuf  out varchar2    
   );
   -- 
 procedure WELLSFARGO
   (
     retcode out varchar2
    ,errbuf  out varchar2
    ,p_bank_name in varchar2    
   );
   --  
 procedure BOFA_US
   (
     retcode out varchar2
    ,errbuf  out varchar2
    ,p_bank_name in varchar2    
   );
   --   
 procedure BOFA_CA
   (
     retcode out varchar2
    ,errbuf  out varchar2
    ,p_bank_name in varchar2    
   );
   --        
 procedure create_files
   (
     retcode out varchar2
    ,errbuf  out varchar2    
   );
   --      
  procedure submit_request_set
    (
        retcode out varchar2
       ,errbuf  out varchar2 
       ,p_to_email in varchar2  
       ,p_cc_email in varchar2
       ,p_send_attachments_to in varchar2 
    );
  --               
-- Start Version 1.1
PROCEDURE uc4_Cash_Positioning 
    (
	    p_errbuf               OUT VARCHAR2
	   ,p_retcode              OUT NUMBER							   
	   ,P_To_Email             IN VARCHAR2
	   ,P_CC_Email             IN VARCHAR2
	   ,P_Attach_Email         IN VARCHAR2
	   ,p_user_name            IN VARCHAR2
	   ,p_responsibility_name  IN VARCHAR2
	);
-- Start Version 1.2
procedure rewrite_wf_file
   (
     retcode out varchar2
    ,errbuf  out varchar2
   )
  ;  
-- End Version 1.2    
end xxcus_currentday_cash_pkg;
-- END Version 1.1
/