CREATE OR REPLACE PACKAGE BODY APPS.XXWC_MISC_RECEIPT_IFACE_PKG
AS
  /*************************************************************************
  *  Copyright (c) 2011 Lucidity Consulting Group
  *  All rights reserved.
  **************************************************************************
  *   $Header XXXWC_MISC_RECEIPT_IFACE_PKGpks $
  *   Module Name: XXXWC_MISC_RECEIPT_IFACE_PKG.sql
  *
  *   PURPOSE:   This package is for Creating Interface records for Misc Receipts
  *
  *   REVISIONS:
  *   Ver        Date        Author                     Description
  *   ---------  ----------  ---------------         -------------------------
  *   1.0        12/16/2011  Srinivas Malkapuram      Initial Version
  *   2.0       4/29/2013   Ram Talluri              Made code changes to support rental mass transfer process TMS #20130218-01185
  * ***************************************************************************/
PROCEDURE MAIN
  (
    errbuf OUT VARCHAR2,
    retcode OUT NUMBER,
    p_Log_Enabled IN VARCHAR2 DEFAULT 'N' )
                  IS
  /* Cursor to get the Misc Issue Transactions that has Attribute9 of Null */
  -- Satish U:  get Transactions whose Attribute9 value is Not equal to 'Y'

      CURSOR Item_Select
      IS
         SELECT mmt.transaction_id,
                mmt.inventory_item_id   ,
                mmt.organization_id     ,
                mmt.transaction_quantity,
                mmt.transaction_uom     ,
                mmt.revision,
                mmt.distribution_account_id,
                mmt.attribute_category,
                mmt.attribute1,
                mmt.attribute2,
                mmt.attribute3,
                mmt.attribute4,
                mmt.attribute5,
                mmt.attribute6,
                mmt.attribute10,
                mmt.attribute11,
                mmt.attribute12,
                mmt.attribute13,
                mmt.actual_cost*ABS(transaction_quantity)  TRANSACTION_VALUE
           FROM mtl_material_transactions mmt,
                mtl_transaction_types mtt
          WHERE mmt.transaction_type_id          = mtt.transaction_type_id
          and mtt.Transaction_TYpe_ID  = G_ISSUE_TRANSACTION_TYPE_ID
         --   AND UPPER(mtt.transaction_type_name) = UPPER(G_ISSUE_TRANSACTION_TYPE)
            AND ( UPPER(mmt.attribute9)         != 'Y'
                        OR
                   mmt.attribute9                   IS NULL
                ) ;
            --AND TRUNC(mmt.creation_date)  =TRUNC(sysdate);

  v_receipt                    NUMBER;
  v_flag                       CHAR;
  v_transaction_type_id        NUMBER;
  v_source_line_id             NUMBER;
  v_source_header_id           NUMBER;
  v_transaction_source_type_id NUMBER;
  v_transaction_interface_id   NUMBER;
  v_user_id                    NUMBER;
  v_dist_account_id            NUMBER;
  v_transaction_action_id      NUMBER;
  l_API_NAME                   VARCHAR2(30) := 'MISC_RECEIPT_INTERFACE';
  l_Module_Name                VARCHAR2(120);
  l_Debug_Msg                  VARCHAR2(2000);
  x_return_status              VARCHAR2(50);
  x_result_out                 VARCHAR2(100);
  v_Serial_number              VARCHAR2(100);
  serial_value                 serial_type;
  v_subinv_code                mtl_secondary_inventories.secondary_inventory_name%type;
  l_rental_item_id             NUMBER;

--Define user defined exception
    SKIP_RECORD            EXCEPTION;
    VALIDATION_ERROR       EXCEPTION;


BEGIN

  l_Module_Name := G_MODULE_NAME ||'.'|| l_API_NAME ;

  FOR Item_Rec  IN Item_Select
  LOOP

    BEGIN

        IF p_Log_Enabled                              IN ('y', 'Y') THEN
          l_Debug_Msg                                 := ('*********Processing for Transaction ID *********'|| Item_Rec.Transaction_ID );
          XXWC_RENTAL_ENGINE_PKG.LOG_MSG(p_debug_level => G_LEVEL_PROCEDURE, p_mod_name => l_Module_Name, P_DEBUG_MSG => l_Debug_Msg );
        END IF;

        v_flag           :='Y';
        v_user_id        :=G_USER_ID;

           l_rental_item_id := GET_RENTAL_INV_ITEM_ID ( --Item_Rec.inventory_item_id,--Commented by Ram Talluri 4/29/2013 TMS 20130218-01185
                                                        Item_Rec.attribute12,--Added by Ram Talluri 4/29/2013 TMS 20130218-01185. This function will now read the rental part number value from DFF.
                                                        Item_Rec.organization_id,
                                                        item_rec.transaction_id);--Added by Ram Talluri 4/29/2013 TMS 20130218-01185. This function will now accept the transaction_id as input param

            IF l_rental_item_id IS NULL  THEN

                  IF p_Log_Enabled IN ('y', 'Y') THEN
                      l_Debug_Msg    := ('Corresponding Rental Item is not Defined. Pls contact Inventory Manager' );
                      XXWC_RENTAL_ENGINE_PKG.LOG_MSG(p_debug_level => G_LEVEL_PROCEDURE,
                                                     p_mod_name => l_Module_Name,
                                                     P_DEBUG_MSG => l_Debug_Msg );

                 END IF;
                    v_flag :='N';
                    RAISE SKIP_RECORD;
            END IF;


           BEGIN


              IF IS_EXIST (Item_Rec.transaction_id,Item_Rec.inventory_item_id,Item_Rec.organization_id) THEN
                 IF p_Log_Enabled IN ('y', 'Y') THEN
                      l_Debug_Msg    := ('   Transaction ID already exists in interface '|| Item_Rec.Transaction_ID );
                      XXWC_RENTAL_ENGINE_PKG.LOG_MSG(p_debug_level => G_LEVEL_PROCEDURE,
                                                     p_mod_name => l_Module_Name,
                                                     P_DEBUG_MSG => l_Debug_Msg );

                 END IF;
                    v_flag :='N';
                    RAISE SKIP_RECORD;
              ELSE
                v_flag :='Y';
              END IF;

           EXCEPTION
                WHEN OTHERS THEN
                  IF p_Log_Enabled IN ('y', 'Y') THEN
                          l_Debug_Msg     := 'Error while retrieving records from Interface Table for Transaction ID '||Item_rec.transaction_id;
                          XXWC_RENTAL_ENGINE_PKG.LOG_MSG(p_debug_level => G_LEVEL_PROCEDURE,
                                                         p_mod_name => l_Module_Name,
                                                         P_DEBUG_MSG => l_Debug_Msg );

                  END IF;
                    v_flag      :='N';
                    RAISE SKIP_RECORD;
           END;


            BEGIN

              v_transaction_type_id           :=GET_RECEIPT_TRAN_TYPE_ID;
              v_transaction_source_type_id    :=GET_TRANSACTION_SOURCE_ID;
              v_transaction_action_id         :=GET_TRANSACTION_ACTION_ID;

               SELECT mtl_material_transactions_s.nextval
                 INTO v_transaction_interface_id
                 FROM dual;

            EXCEPTION
            WHEN OTHERS THEN
              l_Debug_Msg                                 := 'Error retriving Transaction IDs.....';
              XXWC_RENTAL_ENGINE_PKG.LOG_MSG(p_debug_level => G_LEVEL_PROCEDURE, p_mod_name => l_Module_Name, P_DEBUG_MSG => l_Debug_Msg );
              v_flag                                      :='N';
              RAISE SKIP_RECORD;
            END;


            BEGIN
                  IF 
                  
                  -- IS_ITEM_SERIALIZED(Item_Rec.inventory_item_id,Item_Rec.organization_id ) THEN--Commented by Ram Talluri 4/29/2013 TMS 20130218-01185
                  
                  IS_ITEM_SERIALIZED(Item_Rec.attribute12,Item_Rec.organization_id ) THEN--Added by Ram Talluri 4/29/2013 TMS 20130218-01185.

                        IF Item_Rec.transaction_quantity*-1 =1 THEN

                            --v_Serial_number                       := GET_SERIAL_NUMBER(Item_Rec.transaction_id);--Commented by Ram Talluri 4/29/2013 TMS 20130218-01185
                            v_Serial_number                       :=item_rec.attribute13;--Added by Ram Talluri 4/29/2013 TMS 20130218-01185. assigning serial number value from DFF
                            serial_value.transaction_interface_id :=v_transaction_interface_id;
                            serial_value.source_code              := 'INV';
                            serial_value.fm_serial_number         := v_Serial_number;
                            serial_value.to_serial_number         := v_Serial_number;
                            serial_value.Last_update_date         := sysdate;
                            serial_value.last_updated_by          := v_user_id;
                            serial_value.Creation_date            := sysdate;
                            serial_value.Created_by               := v_user_id;
                            Serial_Interface(serial_value ,x_return_status,x_result_out);

                        ELSE

                            l_Debug_Msg     := 'Serialized Items cannot be transferred more than 1 at a time '||Item_rec.transaction_id;
                            XXWC_RENTAL_ENGINE_PKG.LOG_MSG(p_debug_level => G_LEVEL_PROCEDURE,
                                                           p_mod_name => l_Module_Name,
                                                           P_DEBUG_MSG => l_Debug_Msg );
                            v_flag := 'N';
                            RAISE SKIP_RECORD;
                        END IF;

                  END IF;

                      IF x_return_status = FND_API.G_RET_STS_ERROR THEN
                            l_Debug_Msg     := 'EXCEPTION in calling Serial_Interface for '||Item_rec.transaction_id;
                            XXWC_RENTAL_ENGINE_PKG.LOG_MSG(p_debug_level => G_LEVEL_PROCEDURE,
                                                           p_mod_name => l_Module_Name,
                                                           P_DEBUG_MSG => l_Debug_Msg );

                            v_flag          :='N';
                            RAISE SKIP_RECORD;
                      END IF;

            EXCEPTION
            WHEN OTHERS THEN
              l_Debug_Msg   := 'ERROR: while trying to insert the record in Serial Interface table for Item ID ' ||Item_rec.INVENTORY_ITEM_ID || ' for Organization ID ' || Item_rec.ORGANIZATION_ID;
              XXWC_RENTAL_ENGINE_PKG.LOG_MSG(p_debug_level => G_LEVEL_PROCEDURE,
                                                           p_mod_name => l_Module_Name,
                                                           P_DEBUG_MSG => l_Debug_Msg );
              v_flag                                      :='N';
              RAISE SKIP_RECORD;
            END;

            BEGIN
                SELECT secondary_inventory_name
                  INTO v_subinv_code
                  FROM mtl_secondary_inventories
                 WHERE UPPER(secondary_inventory_name) = UPPER(G_RENTAL_SUBINV_CODE)
                   AND organization_id             =Item_Rec.organization_id;

            EXCEPTION
                WHEN OTHERS THEN
                    l_Debug_Msg  := 'Error retrieving SubInventory Code '||G_RENTAL_SUBINV_CODE;
                    XXWC_RENTAL_ENGINE_PKG.LOG_MSG(p_debug_level => G_LEVEL_PROCEDURE, p_mod_name => l_Module_Name, P_DEBUG_MSG => l_Debug_Msg );
                    v_flag := 'N';
                    RAISE SKIP_RECORD;
            END;


            BEGIN
               -- Satish U: Update Attribute9 Value with 'N'.  To make sure that Item Converted will not picked up for FA Mass Additions.
               -- 05-APR-2012

              IF v_flag='Y' THEN



                 INSERT
                   INTO mtl_transactions_interface
                  (
                    source_code               ,
                    source_line_id            ,
                    source_header_id          ,
                    transaction_interface_id  ,
                    subinventory_code         ,
                    process_flag              ,
                    transaction_mode          ,
                    lock_flag                 ,
                    created_by                ,
                    creation_date             ,
                    last_updated_by           ,
                    last_update_date          ,
                    organization_id           ,
                    inventory_item_id         ,
                    revision                  ,
                    transaction_quantity      ,
                    transaction_uom           ,
                    transaction_date          ,
                    transaction_type_id       ,
                    distribution_account_id   ,
                    transaction_source_type_id,
                    transaction_action_id     ,
                    attribute_category,
                    attribute1,
                    attribute2,
                    attribute3,
                    attribute4,
                    attribute5,
                    attribute6,
                    attribute7, -- Transaction Value since Rental Subinventory is an Expense account.
                    attribute9,
                    attribute10,
                    attribute11,
                    attribute12,
                    attribute13,
                    attribute14
                  )
                  VALUES
                  (
                    'INV'                           ,
                    v_transaction_interface_id      ,
                    v_transaction_interface_id      ,
                    v_transaction_interface_id      ,
                    v_subinv_code                   ,
                    '1'                             ,
                    '3'                             ,
                    '2'                             ,
                    v_user_id                       ,
                    sysdate                         ,
                    v_user_id                       ,
                    sysdate                         ,
                    Item_Rec.organization_id        ,
                    l_rental_item_id, --Item_Rec.inventory_item_id      ,
                    Item_Rec.revision               ,
                    ABS(Item_Rec.transaction_quantity),
                    Item_Rec.transaction_uom        ,
                    sysdate                         ,
                    v_transaction_type_id           ,
                    Item_rec.distribution_account_id ,
                    v_transaction_source_type_id    ,
                    v_transaction_action_id         ,
                    Item_rec.attribute_category,
                    Item_rec.attribute1,
                    Item_rec.attribute2,
                    Item_rec.attribute3,
                    Item_rec.attribute4,
                    Item_rec.attribute5,
                    Item_rec.attribute6,
                    Item_rec.TRANSACTION_VALUE,
                    'N',
                    Item_rec.attribute10,
                    Item_rec.attribute11,
                    Item_rec.attribute12,
                    Item_rec.attribute13,
                    Item_rec.transaction_id
                  );


                 UPDATE MTL_MATERIAL_TRANSACTIONS
                    SET ATTRIBUTE9         = 'Y',
                        last_update_date   =  SYSDATE,
                        last_updated_by    =  v_user_id
                  WHERE TRANSACTION_ID = Item_Rec.Transaction_ID;

                COMMIT;

                 l_Debug_Msg  := 'Successfully Inserted Transaction Interface ID in MTL_TRANSACTIONS_INTERFACE ' ||v_transaction_interface_id;
                    XXWC_RENTAL_ENGINE_PKG.LOG_MSG ( p_debug_level => G_LEVEL_PROCEDURE,
                                                     p_mod_name => l_Module_Name,
                                                     P_DEBUG_MSG => l_Debug_Msg ) ;
                    ROLLBACK;

              END IF;

            EXCEPTION
                WHEN OTHERS THEN
                    l_Debug_Msg  := 'Error inserting Record in mtl_transactions_interface '||Item_Rec.inventory_item_id || ' For ' ||Item_Rec.organization_id;
                    XXWC_RENTAL_ENGINE_PKG.LOG_MSG ( p_debug_level => G_LEVEL_PROCEDURE,
                                                     p_mod_name => l_Module_Name,
                                                     P_DEBUG_MSG => l_Debug_Msg ) ;
                    ROLLBACK;
                    RAISE SKIP_RECORD;
            END;


    EXCEPTION
        WHEN SKIP_RECORD THEN
                l_Debug_Msg    := ('*********Skipping further Processing of Transaction ID *********'||  Item_Rec.Transaction_ID );
                XXWC_RENTAL_ENGINE_PKG.LOG_MSG(p_debug_level   => G_LEVEL_PROCEDURE,
                                               p_mod_name      => l_Module_Name,
                                               P_DEBUG_MSG     => l_Debug_Msg );
                retcode := '1';
         WHEN NO_DATA_FOUND THEN
                 l_Debug_Msg := ('*********Skipping further Processing of Transaction ID *********'|| Item_Rec.Transaction_ID);
                  XXWC_RENTAL_ENGINE_PKG.LOG_MSG(p_debug_level   => G_LEVEL_PROCEDURE,
                                                   p_mod_name      => l_Module_Name,
                                                   P_DEBUG_MSG     => l_Debug_Msg );
                  retcode  := '1';
                  errbuf   := l_Debug_Msg;
          WHEN OTHERS THEN
              l_Debug_Msg := sqlerrm;
              XXWC_RENTAL_ENGINE_PKG.LOG_MSG(p_debug_level   => G_LEVEL_PROCEDURE,
                                               p_mod_name      => l_Module_Name,
                                               P_DEBUG_MSG     => l_Debug_Msg  );
              retcode  := '1';
              errbuf   := sqlerrm;
             ROLLBACK;
    END;
  END LOOP;

END;
PROCEDURE Serial_Interface
  (
    serial_rec IN serial_type,
    x_return_status OUT nocopy VARCHAR2,
    x_result_out OUT nocopy    VARCHAR2 )
IS
BEGIN
   INSERT
     INTO mtl_serial_numbers_interface
    (
      transaction_interface_id ,
      source_code              ,
      fm_serial_number         ,
      to_serial_number         ,
      last_update_date         ,
      last_updated_by          ,
      creation_date            ,
      created_by               ,
      process_flag
    )
    VALUES
    (
      serial_rec.transaction_interface_id ,
      serial_rec.source_code              ,
      serial_rec.fm_serial_number         ,
      serial_rec.to_serial_number         ,
      serial_rec.last_update_date         ,
      serial_rec.last_updated_by          ,
      serial_rec.creation_date            ,
      serial_rec.created_by               ,
      1
    );

  x_return_status:=FND_API.G_RET_STS_SUCCESS;

EXCEPTION
WHEN OTHERS THEN
  x_return_status:=FND_API.G_RET_STS_ERROR;
  x_result_out   :='Exception in Inserting into MTL Serial Interface';
END Serial_Interface;
FUNCTION GET_RECEIPT_TRAN_TYPE_ID
  RETURN NUMBER
IS
  v_transaction_type_id NUMBER;
  l_API_NAME            VARCHAR2
  (
    30
  )
  := 'GET_RECEIPT_TRAN_TYPE_ID';
  l_Module_Name VARCHAR2
  (
    120
  )
  ;
  l_Debug_Msg VARCHAR2
  (
    2000
  )
  ;
BEGIN
  l_Module_Name := G_MODULE_NAME ||'.'|| l_API_NAME ;
   SELECT transaction_type_id
     INTO v_transaction_type_id
     FROM mtl_transaction_types
    WHERE transaction_type_name = G_RECEIPT_TRANSACTION_TYPE;
  RETURN v_transaction_type_id;
EXCEPTION
WHEN OTHERS THEN
  l_Debug_Msg                                 := 'ERROR: while trying to Obtain Receipt Transaction Type ID ';
  XXWC_RENTAL_ENGINE_PKG.LOG_MSG(p_debug_level => G_LEVEL_PROCEDURE, p_mod_name => l_Module_Name, P_DEBUG_MSG => l_Debug_Msg );
  RETURN NULL;
END GET_RECEIPT_TRAN_TYPE_ID ;
FUNCTION GET_TRANSACTION_SOURCE_ID
  RETURN NUMBER
IS
  v_transaction_source_type_id NUMBER;
  l_API_NAME                   VARCHAR2(30) := 'GET_TRANSACTION_SOURCE_ID';
  l_Module_Name                VARCHAR2(120);
  l_Debug_Msg                  VARCHAR2(2000);
BEGIN
  l_Module_Name := G_MODULE_NAME ||'.'|| l_API_NAME ;
   SELECT transaction_source_type_id
     INTO v_transaction_source_type_id
     FROM mtl_txn_source_types
    WHERE UPPER(transaction_source_type_name) =G_TRANSACTION_SOURCE;
  RETURN v_transaction_source_type_id;
EXCEPTION
WHEN OTHERS THEN
  l_Debug_Msg                                 := 'ERROR: while trying to Obtain Transaction Source Type ID ';
  XXWC_RENTAL_ENGINE_PKG.LOG_MSG(p_debug_level => G_LEVEL_PROCEDURE, p_mod_name => l_Module_Name, P_DEBUG_MSG => l_Debug_Msg );
  RETURN NULL;
END GET_TRANSACTION_SOURCE_ID ;
FUNCTION GET_TRANSACTION_ACTION_ID
  RETURN NUMBER
IS
  v_transaction_action_id NUMBER;
  l_API_NAME              VARCHAR2(30) := 'GET_TRANSACTION_ACTION_ID';
  l_Module_Name           VARCHAR2(120);
  l_Debug_Msg             VARCHAR2(2000);
BEGIN
  l_Module_Name := G_MODULE_NAME ||'.'|| l_API_NAME ;
   SELECT lookup_code
     INTO v_transaction_action_id
     FROM mfg_lookups
    WHERE UPPER(meaning) = G_TRANSACTION_ACTION
  AND lookup_type        =G_TRXN_ACTION_LOOKUP;
  RETURN v_transaction_action_id;
EXCEPTION
WHEN OTHERS THEN
  l_Debug_Msg                                 := 'ERROR: while trying to Obtain Transaction Action ID ';
  XXWC_RENTAL_ENGINE_PKG.LOG_MSG(p_debug_level => G_LEVEL_PROCEDURE, p_mod_name => l_Module_Name, P_DEBUG_MSG => l_Debug_Msg );
  RETURN NULL;
END GET_TRANSACTION_ACTION_ID ;
FUNCTION IS_ITEM_SERIALIZED
  (
    --P_INVENTORY_ITEM_ID IN NUMBER,--Commented by Ram Talluri 4/29/2013 TMS 20130218-01185
    P_ATTRIBUTE12 IN VARCHAR2,--Added by Ram Talluri 4/29/2013 TMS 20130218-01185. This function will now read the rental part number value from DFF.
    P_ORGANIZATION_ID   IN NUMBER)
  RETURN BOOLEAN
IS
  v_serial_code NUMBER;
  l_API_NAME    VARCHAR2(30) := 'IS_ITEM_SERIALIZED';
  l_Module_Name VARCHAR2(120);
  l_Debug_Msg   VARCHAR2(2000);
BEGIN
  l_Module_Name := G_MODULE_NAME ||'.'|| l_API_NAME ;
   SELECT serial_number_control_code
     INTO v_serial_code
     FROM mtl_system_items
    WHERE 1=1
    --and inventory_item_id =P_INVENTORY_ITEM_ID--Commented by Ram Talluri 4/29/2013 TMS 20130218-01185
    and segment1=P_ATTRIBUTE12--Added by Ram Talluri 4/29/2013 TMS 20130218-01185. This function will now read the rental part number value from DFF.
  AND organization_id       = P_ORGANIZATION_ID;
  IF v_serial_code          > 1 THEN
    RETURN TRUE;
  ELSE
    RETURN FALSE;
  END IF;
EXCEPTION
WHEN OTHERS THEN
  --l_Debug_Msg                                 := 'ERROR: while trying to check the Item Serialialized for item ID ' ||P_INVENTORY_ITEM_ID || ' for Organization ID ' || P_ORGANIZATION_ID;--Commented by Ram Talluri 4/29/2013 TMS 20130218-01185
  l_Debug_Msg                                 := 'ERROR: while trying to check the Item Serialialized for item ID ' ||P_ATTRIBUTE12 || ' for Organization ID ' || P_ORGANIZATION_ID;--Added by Ram Talluri 4/29/2013 TMS 20130218-01185.
  XXWC_RENTAL_ENGINE_PKG.LOG_MSG(p_debug_level => G_LEVEL_PROCEDURE, p_mod_name => l_Module_Name, P_DEBUG_MSG => l_Debug_Msg );
  RETURN FALSE;
END IS_ITEM_SERIALIZED;
FUNCTION IS_EXIST
  (
    P_TRANSACTION_ID    IN NUMBER,
    P_INVENTORY_ITEM_ID IN NUMBER,
    P_ORGANIZATION_ID   IN NUMBER )
  RETURN BOOLEAN
IS
  v_receipt     NUMBER;
  l_API_NAME    VARCHAR2(30) := 'IS_EXIST';
  l_Module_Name VARCHAR2(120);
  l_Debug_Msg   VARCHAR2(2000);
BEGIN
  l_Module_Name := G_MODULE_NAME ||'.'|| l_API_NAME ;
   SELECT COUNT(*)
     INTO v_receipt
     FROM mtl_transactions_interface mti,
    mtl_transaction_types mtt
    WHERE 1                     =1
  AND mti.transaction_type_id   = mtt.transaction_type_id
  AND mtt.transaction_type_name = G_RECEIPT_TRANSACTION_TYPE
    --AND TRUNC(mti.creation_date)  =TRUNC(sysdate)
  AND mti.attribute14       = P_TRANSACTION_ID
  AND mti.inventory_item_id =P_INVENTORY_ITEM_ID
  AND mti.organization_id   =P_ORGANIZATION_ID;
  IF v_receipt              >0 THEN
    RETURN TRUE;
  ELSE
    RETURN FALSE;
  END IF;
EXCEPTION
WHEN OTHERS THEN
  l_Debug_Msg                                 := 'ERROR: while trying to check the Item EXIST in Transaction interface for item ID ' ||P_INVENTORY_ITEM_ID || ' for Organization ID ' || P_ORGANIZATION_ID;
  XXWC_RENTAL_ENGINE_PKG.LOG_MSG(p_debug_level => G_LEVEL_PROCEDURE, p_mod_name => l_Module_Name, P_DEBUG_MSG => l_Debug_Msg );
  RETURN FALSE;
END IS_EXIST;
FUNCTION GET_SERIAL_NUMBER
  (
    p_Transfer_Trx_ID IN NUMBER )
  RETURN VARCHAR2
IS
  l_serial_number VARCHAR2(20);
  l_API_NAME      VARCHAR2(30) := 'GET_SERIAL_NUMBER';
  l_Module_Name   VARCHAR2(120);
  l_Debug_Msg     VARCHAR2(2000);
BEGIN
  l_Module_Name := G_MODULE_NAME ||'.'|| l_API_NAME ;
   SELECT serial_number
     INTO l_serial_number
     FROM MTL_SERIAL_NUMBERS msn
    WHERE msn.last_transaction_id = p_Transfer_Trx_ID;
  RETURN l_serial_number;
EXCEPTION
WHEN NO_DATA_FOUND THEN
  l_Debug_Msg                                 := 'No Serial Number found for Transaction SET ID '||p_Transfer_Trx_ID;
  XXWC_RENTAL_ENGINE_PKG.LOG_MSG(p_debug_level => G_LEVEL_PROCEDURE, p_mod_name => l_Module_Name, P_DEBUG_MSG => l_Debug_Msg );
  RETURN NULL;
WHEN TOO_MANY_ROWS THEN
  l_Debug_Msg                                 := 'Duplicate Serial Numbers found for Transaction SET ID '||p_Transfer_Trx_ID;
  XXWC_RENTAL_ENGINE_PKG.LOG_MSG(p_debug_level => G_LEVEL_PROCEDURE, p_mod_name => l_Module_Name, P_DEBUG_MSG => l_Debug_Msg );
  RETURN NULL;
WHEN OTHERS THEN
  l_Debug_Msg                                 := 'ERROR: while trying to Obtain Serial Number Transaction SET ID '||p_Transfer_Trx_ID;
  XXWC_RENTAL_ENGINE_PKG.LOG_MSG(p_debug_level => G_LEVEL_PROCEDURE, p_mod_name => l_Module_Name, P_DEBUG_MSG => l_Debug_Msg );
  RETURN NULL;
END;

FUNCTION GET_RENTAL_INV_ITEM_ID(--p_inventory_item_id IN NUMBER,--Commented by Ram Talluri 4/29/2013 TMS 20130218-01185
                                p_attribute12 IN VARCHAR2,--Added by Ram Talluri 4/29/2013 TMS 20130218-01185.
                                p_organization_id IN NUMBER,
                                p_transaction_id  IN NUMBER)--Added by Ram Talluri 4/29/2013 TMS 20130218-01185.
  RETURN NUMBER
IS
  l_rental_inv_item_id NUMBER;
  l_API_NAME      VARCHAR2(30) := 'GET_RENTAL_INV_ITEM_ID';
  l_Module_Name   VARCHAR2(120);
  l_Debug_Msg     VARCHAR2(2000);
BEGIN
  l_Module_Name := G_MODULE_NAME ||'.'|| l_API_NAME ;
  
  /*Commented by Ram Talluri 4/29/2013 TMS 20130218-01185- Start
   SELECT inventory_item_id
     INTO l_rental_inv_item_id
     FROM MTL_SYSTEM_ITEMS_B MSI
    WHERE segment1 like 'R'||(select segment1 from mtl_system_items_b
                              where inventory_item_id = p_inventory_item_id
                                and organization_id = p_organization_id)
      AND organization_id = p_organization_id;
      
      Commented by Ram Talluri 4/29/2013 TMS 20130218-01185- End*/
      
      --Added by Ram Talluri 4/29/2013 TMS 20130218-01185-Start
   SELECT inventory_item_id
     INTO l_rental_inv_item_id
     FROM MTL_SYSTEM_ITEMS_B MSI
    WHERE segment1 =(select attribute12 from mtl_material_transactions
                              where attribute12 = p_attribute12
                                and organization_id = p_organization_id
                                and transaction_id=p_transaction_id)
      AND organization_id = p_organization_id;
      
      --Added by Ram Talluri 4/29/2013 TMS 20130218-01185-End*/

  RETURN l_rental_inv_item_id;

EXCEPTION
WHEN NO_DATA_FOUND THEN
--  l_Debug_Msg                                 := 'No Rental corresponding item found '||p_inventory_item_id;--Commented by Ram Talluri 4/29/2013 TMS 20130218-01185
  l_Debug_Msg                                 := 'No Rental corresponding item found '||p_attribute12;--Added by Ram Talluri 4/29/2013 TMS 20130218-01185.
  XXWC_RENTAL_ENGINE_PKG.LOG_MSG(p_debug_level => G_LEVEL_PROCEDURE, p_mod_name => l_Module_Name, P_DEBUG_MSG => l_Debug_Msg );
  RETURN NULL;
WHEN TOO_MANY_ROWS THEN
-- l_Debug_Msg                                 := 'Duplicate Rental Items found  '||p_inventory_item_id;--Commented by Ram Talluri 4/29/2013 TMS 20130218-01185
  l_Debug_Msg                                 := 'Duplicate Rental Items found  '||p_attribute12;--Added by Ram Talluri 4/29/2013 TMS 20130218-01185.
  XXWC_RENTAL_ENGINE_PKG.LOG_MSG(p_debug_level => G_LEVEL_PROCEDURE, p_mod_name => l_Module_Name, P_DEBUG_MSG => l_Debug_Msg );
  RETURN NULL;
WHEN OTHERS THEN
 --l_Debug_Msg                                 := 'ERROR: while trying to Obtain Rental Items '||p_inventory_item_id;--Commented by Ram Talluri 4/29/2013 TMS 20130218-01185
  l_Debug_Msg                                 := 'ERROR: while trying to Obtain Rental Items '||p_attribute12;--Added by Ram Talluri 4/29/2013 TMS 20130218-01185.
  XXWC_RENTAL_ENGINE_PKG.LOG_MSG(p_debug_level => G_LEVEL_PROCEDURE, p_mod_name => l_Module_Name, P_DEBUG_MSG => l_Debug_Msg );
  RETURN NULL;
END;

END XXWC_MISC_RECEIPT_IFACE_PKG;
/