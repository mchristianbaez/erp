CREATE OR REPLACE PACKAGE BODY APPS.xxwc_customer_conversion_pkg
AS
       /***************************************************************************************
    *  Script Name: XXWC_CUSTOMER_CONVERSION_PKG.pks
    *
    *  Script Owners: Lucidity Consulting Group.
    *
    *  Client: White Cap.
    *
    *  Interface / Conversion Name: Customers Conversion.
    *
    *  Functional Purpose: Convert customers using HZ APIs.
    *
    *  History:
    *
    *  Version  Date          Author          Description
    *****************************************************************************************
    *  1.0      04-Sep-2011   Karun Puri     Initial development.
    *  1.1                    Vasanth S      Added logic for OE Shipments : Concurrent Program 
    *  1.2                    Satish U       Performance Turning 
    *  1.3                    Satish U       Added Concurrent Program to  Inactive Customers 
	*  1.4      13-Oct-2014   Pattabhi Avula Changes made for TMS#  20141001-00170
    *****************************************************************************************/

    
    -- PROCEDURE populate_collectors 
    --  Satish U: This procedure will populate Collector Id in the XREF Table xxwc_collector_cr_analyst_xref
    -- Populate_Collectors API is called during validation process XXWC_VALIDATIONS 
    
    
    PROCEDURE populate_collectors
    IS
        -- Define a cursor to get distinct and active collectors from Oracle Table. 
        CURSOR collector_cur
        IS
            SELECT   xca.oracle_collector, ac.collector_id
            FROM   (SELECT DISTINCT oracle_collector
                    FROM   xxwc_collector_cr_analyst_xref
                    WHERE   collector_id IS NULL) xca, ar_collectors ac
            WHERE   UPPER (ac.name) = UPPER (oracle_collector)
            AND ac.status = 'A';
    BEGIN
        --Satish U: Update Collector ID to NULL For each run : 
        -- TO ensure we are not getting COllector_ID from Other Instances : 29-FEB-2012 
        UPDATE   xxwc_collector_cr_analyst_xref
               SET   collector_id = NULL
        Where Collector_ID Is NOt Null ; 
        
        FOR collector_rec IN collector_cur
        LOOP
            --- Loop through all the Collectors and update Collector ID in XREF Table. 

            UPDATE   xxwc_collector_cr_analyst_xref
               SET   collector_id = collector_rec.collector_id
            WHERE   UPPER (oracle_collector) =   UPPER (collector_rec.oracle_collector);
        END LOOP;

        COMMIT;
    END populate_collectors;

    --- **************************************************************************************
    -- PROCEDURE populate_credit_analyst
    -- Satish U : This Procedure will populate Credit Analyst ID :
    --            In the XREF Table xxwc_collector_cr_analyst_xref. 
    PROCEDURE populate_credit_analyst
    IS
        CURSOR crdt_analyst_cur
        IS
            SELECT   DISTINCT
                     c.resource_id, x.oracle_credit_analyst, x.prism_code
              FROM   jtf_rs_role_relations a,
                     jtf_rs_roles_vl b,
                     jtf_rs_resource_extns_vl c,
                     per_all_people_f per,
                     xxwc_collector_cr_analyst_xref x
             WHERE       a.role_resource_type = 'RS_INDIVIDUAL'
                     AND a.role_resource_id = c.resource_id
                     AND a.role_id = b.role_id
                     AND b.role_code = 'CREDIT_ANALYST'
                     AND c.category = 'EMPLOYEE'
                     AND c.source_id = per.person_id
                     AND SYSDATE BETWEEN per.effective_start_date
                                     AND  per.effective_end_date
                     AND per.current_employee_flag = 'Y'
                     AND NVL (a.delete_flag, 'N') <> 'Y'
                     AND UPPER (c.resource_name) =
                            UPPER (x.oracle_credit_analyst)
                     AND x.credit_analyst_id IS NULL;
    BEGIN
        --Satish U: Update credit_analyst_id to NULL For each run : 
        -- TO ensure we are not getting credit_analyst_id from Other Instances : 29-FEB-2012 
        UPDATE   xxwc_collector_cr_analyst_xref
            SET   credit_analyst_id = NULL
        Where credit_analyst_id Is NOt Null ; 
        
        FOR crdt_analyst_rec IN crdt_analyst_cur
        LOOP
            UPDATE   xxwc_collector_cr_analyst_xref
               SET   credit_analyst_id = crdt_analyst_rec.resource_id
             WHERE   prism_code = crdt_analyst_rec.prism_code;
        END LOOP;

        COMMIT;
    END populate_credit_analyst;

    --- **************************************************************************************
    -- Procedure POP_ERR :  API to capture Validation Errors and program errors for all three 
    -- entities : Customes, Customer Sites, Customer Contacts :  THis is an pragma autonomus transactions 

    PROCEDURE pop_err (p_entity_name        VARCHAR2, --Customer,Site,Contact ect.                                                    ,
                       p_entity_no          VARCHAR2,
                       p_validation_rule    VARCHAR2,
                       p_error_msg          VARCHAR2)
    IS
        PRAGMA AUTONOMOUS_TRANSACTION;
    BEGIN
        INSERT INTO xxwc_customer_int_errors (request_id,
                                              entity_name,
                                              entity_no,
                                              validation_rule,
                                              error_msg)
          VALUES   (fnd_global.conc_request_id,
                    p_entity_name,
                    p_entity_no,
                    p_validation_rule,
                    p_error_msg);

        COMMIT;
    END pop_err;

    --- **************************************************************************************
    -- Procedure xxwc_validations :  API does Custoemr Conversion program validation and also calls APIS to populate
    --  Oracle IDs in some of the Staging Tables. 

    PROCEDURE xxwc_validations
    IS
        -- Satish U: 10/25/2011 :    Included NULL value for PRocess Status as Records are loaded with Null Value
        --  Add Prefix
        CURSOR get_cust
        IS
            SELECT   *
            FROM   xxwc_ar_customers_cnv
            WHERE   orig_system_reference IS NOT NULL
            AND (process_status IS NULL
              OR process_status IN ('R', 'E')) ; 
             
             

        -- Satish U: 10/25/2011 :    Included NULL value for PRocess Status as Records are loaded with Null Value
        CURSOR get_sites (
            p_customer VARCHAR2)
        IS
            SELECT   *
            FROM   xxwc_ar_sites_cnv
            WHERE   orig_system_reference = p_customer
            AND (process_status IS NULL
               OR process_status IN ('R', 'E'));

        -- Satish U: 10/25/2011 :    Included NULL value for PRocess Status as Records are loaded with Null Value
        CURSOR get_contacts (
            p_customer VARCHAR2)
        IS
           SELECT   con.ROWID rid, con.*
           FROM   xxwc_ar_contact_points_cnv con
           WHERE       1 = 1
           AND con.legacy_party_site_number = p_customer
           AND orig_system_reference = legacy_party_site_number
           AND (con.process_status IS NULL
              OR process_status IN ('R', 'E'));

        -- Satish U: 10/25/2011 :    Included NULL value for PRocess Status as Records are loaded with Null Value
        CURSOR get_site_contacts (
            p_customer                               VARCHAR2,
            p_legacy_customer_number                 VARCHAR2)
        IS
            SELECT   con.ROWID rid, con.*
            FROM   xxwc_ar_contact_points_cnv con
            WHERE   1 = 1 AND con.orig_system_reference = p_customer
            AND con.legacy_party_site_number = p_legacy_customer_number
            AND (con.process_status IS NULL
               OR process_status IN ('R', 'E'));

        l_rec_found              NUMBER;
        l_err                    VARCHAR2 (1);
        l_customer_exists_flag   VARCHAR2 (1);
        l_rec_count              NUMBER;
        c_max_commit_count       CONSTANT NUMBER := 1000;
    BEGIN
        --Satish U : Populate Collector IDs in XREF Table. 
        populate_collectors;
        -- Populate Credit Analyst ID in XREF Table 
        populate_credit_analyst;
        populate_site_locations;
        update_customer_without_sites;
        -- Satish U: 10-JAN-2011 
        update_Sites_without_Customer;
        update_contacts_without_sites;
        
        -- Update Year Established Date 
        -- Satish U: 28-FEB-2012 : 
        Update  xxwc_ar_customers_cnv 
           Set YEAR_ESTABLISHED = '20070101'
        Where  NVL(YEAR_ESTABLISHED,'0') = '0' ;

        l_rec_count := 0;

        FOR rec_cust IN get_cust
        LOOP
            -- Initialize Variables before starting of the LOOP
            l_err := 'N';
            l_customer_exists_flag := 'N';

            --VR1 :Orig_System_Reference and Orig_System Should be Unique for each granular entity.

            --VR2 :Orig_System and Orig_System_Reference should both be Null or both should have value.
            IF (rec_cust.orig_system IS NULL
                AND rec_cust.orig_system_reference IS NOT NULL)
               OR (rec_cust.orig_system IS NOT NULL
                   AND rec_cust.orig_system_reference IS NULL)
            THEN
                pop_err (
                    'Customer',
                    g_parameter_prefix || rec_cust.orig_system_reference,
                    'VR2',
                    'Invalid ORIG SYSTEM,ORIG SYSTEM REFERENCE Combination');
                l_err := 'Y';
            END IF;

            --VR3  Check if Customer already exists in the system
            -- Changing table From Hz_parties to Hz_Cust_Accounts
            BEGIN
                SELECT   'Y'
                INTO   l_customer_exists_flag
                FROM   hz_cust_accounts cus
                WHERE   cus.orig_system_reference = 
                             g_parameter_prefix
                             || rec_cust.orig_system_reference; -- added by v.sankar
              --  AND cus.org_id = g_org_id;  -- commented by Pattabhi for TMS#20141001-00170

                pop_err (
                    'Customer',
                    g_parameter_prefix || rec_cust.orig_system_reference,
                    'VR3',
                    'Customer Already Exists');
                l_err := 'Y';
            EXCEPTION
                WHEN OTHERS THEN
                    l_customer_exists_flag := 'N';
            --- AND created_by_module <> 'ap_suppliers_api')
            END;

           

            --VR42 : Customer_Profile_Class_Name Validation: If value is passed then it is validated against HZ_CUST_PROFILE_CLASSES
            IF rec_cust.customer_profile_class_name IS NOT NULL THEN
                BEGIN
                    SELECT   1
                    INTO   l_rec_found
                    FROM   hz_cust_profile_classes cpc,
                             xxwc_cust_profile_class_xref x
                    WHERE   UPPER (cpc.name) = UPPER (x.profile_class)
                    AND x.prism_code = TO_NUMBER ( rec_cust.customer_profile_class_name)
                    AND cpc.status = 'A';
                EXCEPTION
                    WHEN NO_DATA_FOUND THEN
                        pop_err ('Customer',
                                 rec_cust.orig_system_reference,
                                 'VR42',
                                 'Invalid Customer Profile');
                -- l_err := 'Y';
                END;
            END IF;
            fnd_file.put_line (fnd_file.LOG, 'Before SIC_CODE Validation');
            --VR5 SIC Code validation
            IF rec_cust.sic_code IS NOT NULL AND rec_cust.sic_code <> 0 THEN
                -- Satish U: 12-MAR-2012 Changed it from 1972 SIC to 1987 SIC 
                BEGIN
                    SELECT   1
                    INTO   l_rec_found
                    FROM   ar_lookups b
                    WHERE   b.lookup_code = rec_cust.sic_code
                    AND b.lookup_type = '1987 SIC' ; --'1972 SIC';
                EXCEPTION
                    WHEN NO_DATA_FOUND  THEN
                        pop_err ('Customer',
                                 rec_cust.orig_system_reference,
                                 'VR5',
                                 'Invalid SIC Code');
                END;
            END IF;
            fnd_file.put_line (fnd_file.LOG, 'Before Account Status Validation');
            -- VR43 profile cust account status
            IF rec_cust.credit_analyst_name IS NOT NULL THEN
                BEGIN
                    SELECT   1
                    INTO   l_rec_found
                    FROM   xxwc_account_status_xref a, ar_lookups b
                    WHERE   a.account_status = b.meaning
                    AND b.lookup_type = 'ACCOUNT_STATUS'
                    AND a.prism_credit_group =  rec_cust.credit_analyst_name;
                EXCEPTION
                    WHEN NO_DATA_FOUND THEN
                        pop_err ('Customer',
                                 rec_cust.orig_system_reference,
                                 'VR43',
                                 'Invalid Profile Account Status');
                END;
            END IF;
            fnd_file.put_line (fnd_file.LOG, 'Before Collector or Credit Analyst Validation');
            --VR44 : Collector Name : Should exist in AR_COLLECTORS
            IF rec_cust.collector_name IS NOT NULL
            THEN
                BEGIN
                    SELECT   1
                    INTO   l_rec_found
                    FROM                                 --ar_collectors ac,
                          xxwc_collector_cr_analyst_xref x
                    WHERE   1 = 1
                    AND UPPER (x.prism_code) = UPPER (rec_cust.collector_name);
                EXCEPTION
                    WHEN NO_DATA_FOUND    THEN
                        pop_err ('Customer',
                                 rec_cust.orig_system_reference,
                                 'VR44',
                                 'Invalid Collector');
                --l_err := 'Y';
                END;
            END IF;
            
            --VR45 : Credit Hold Validation : Should have values �Y� or �N�
            IF rec_cust.credit_hold NOT IN ('Y', 'N')   THEN
                pop_err ('Customer',
                         rec_cust.orig_system_reference,
                         'VR45',
                         'Invalid Credit Hold');
                l_err := 'Y';
            END IF;
            fnd_file.put_line (fnd_file.LOG, 'Before Customer TYpe Validation');
            --VR46: Customer_Type : Validated against AR Lookup Type CUSTOMER_TYPE
            IF rec_cust.customer_type IS NOT NULL   THEN
                BEGIN
                    SELECT   1
                    INTO   l_rec_found
                    FROM   ar_lookups
                    WHERE   meaning = rec_cust.customer_type
                    AND lookup_type = 'CUSTOMER_TYPE'
                    AND TRUNC (SYSDATE) BETWEEN start_date_active
                    AND  NVL (end_date_active, TRUNC (SYSDATE)  + 1)
                    AND enabled_flag = 'Y';
                EXCEPTION
                    WHEN NO_DATA_FOUND THEN
                        pop_err ('Customer',
                                 rec_cust.orig_system_reference,
                                 'VR46',
                                 'Invalid CUSTOMER TYPE');
                        l_err := 'Y';
                END;
            END IF;
             fnd_file.put_line (fnd_file.LOG, 'Before Customer Class Validation');
            --VR47 :Customer Class Code : Validated against AR Lookup Type CUSTOMER CLASS
            IF rec_cust.customer_class_code IS NOT NULL
            THEN
                BEGIN
                    SELECT   1
                    INTO   l_rec_found
                    FROM   fnd_lookup_values_vl fl,
                             xxwc_cust_classification_xref x
                    WHERE   fl.lookup_type = 'CUSTOMER CLASS'
                    AND UPPER (fl.lookup_code) = UPPER (x.cust_classification)
                    AND UPPER (x.prism_code) = UPPER (rec_cust.customer_class_code)
                    AND TRUNC (SYSDATE) BETWEEN fl.start_date_active
                    AND  NVL (fl.end_date_active,TRUNC (SYSDATE)  + 1)
                    AND fl.enabled_flag = 'Y';
                EXCEPTION
                    WHEN NO_DATA_FOUND THEN
                        pop_err ('Customer',
                                 rec_cust.orig_system_reference,
                                 'VR47',
                                 'Invalid CUSTOMER CLASS');
                --l_err := 'Y';
                END;
            END IF;

            --VR51 : Send Statements Validation : Should have value �Y� or �N�
            IF rec_cust.send_statements NOT IN ('Y', 'N', 'C') THEN
                pop_err ('Customer',
                         rec_cust.orig_system_reference,
                         'VR51',
                         'Invalid Send Statements Flag');
            --l_err := 'Y';
            END IF;

            --VR53 : Credit Balance Statements : Validation Can have value if Send_Statements is �Y�. Should have values �Y� or �N�
            IF rec_cust.send_statements IN ('Y', 'C')  THEN
                IF rec_cust.credit_balance_statements NOT IN ('Y', 'N', 'C')  THEN
                    pop_err ('Customer',
                             rec_cust.orig_system_reference,
                             'VR53',
                             'Invalid Credit Balance Statement');
                    l_err := 'Y';
                END IF;
            END IF;

             fnd_file.put_line (fnd_file.LOG, 'Before AR Terms Validation');
            --VR62 : Standard_Term_Name : Must be valid term in AR_TERMS
            --ra_terms_vl
            IF rec_cust.standard_term_name IS NOT NULL
            THEN
                BEGIN
                    SELECT   1
                    INTO   l_rec_found
                    FROM   xxwc_payment_terms_xref xpt, ra_terms rt
                    WHERE   UPPER (rt.name) = UPPER (xpt.oracle_term)
                    AND xpt.prism_code = TO_NUMBER (rec_cust.standard_term_name)
                    AND TRUNC (SYSDATE) BETWEEN start_date_active
                         AND  NVL ( end_date_active, TRUNC (SYSDATE)  + 1);
                EXCEPTION
                    WHEN NO_DATA_FOUND THEN
                        pop_err ('Customer',
                                 rec_cust.payment_grace_days,
                                 'VR62',
                                 'Invalid Payment Term');
                --l_err := 'Y';
                END;
            END IF;
             fnd_file.put_line (fnd_file.LOG, 'Before Currency Code Validation');
            --VR66: Currency_Code : Validated against Fnd_Currencies
            IF rec_cust.currency_code IS NOT NULL THEN
                BEGIN
                    SELECT   1
                    INTO   l_rec_found
                    FROM   fnd_currencies
                    WHERE   currency_code = rec_cust.currency_code
                    AND TRUNC (SYSDATE) BETWEEN start_date_active
                          AND  NVL ( end_date_active, TRUNC (SYSDATE)  + 1);
                EXCEPTION
                    WHEN NO_DATA_FOUND THEN
                        pop_err ('Customer',
                                 rec_cust.orig_system_reference,
                                 'VR66',
                                 'Invalid Currency Code');
                        l_err := 'Y';
                END;
            END IF;
             fnd_file.put_line (fnd_file.LOG, 'Before AR CMGT Credit Classification Validation');
            --Satish U: Added Validation
            IF rec_cust.credit_classification IS NOT NULL THEN
                BEGIN
                    --- Check this Query for performance and in the exception  add message to OUT
                    -- Mention in the output that Credit Classification is defaulting
                    SELECT   1
                    INTO   l_rec_found
                    FROM   ar_lookups al, xxwc_cr_classification_xref x
                    WHERE   al.lookup_code = x.oracle_code
                    AND UPPER (x.prism_code) =  UPPER (rec_cust.credit_classification)
                    AND al.lookup_type =  'AR_CMGT_CREDIT_CLASSIFICATION'
                    AND al.enabled_flag = 'Y'
                    AND TRUNC (SYSDATE) BETWEEN TRUNC(al.start_date_active)
                           AND  TRUNC(NVL ( al.end_date_active, SYSDATE  + 1));
                EXCEPTION
                    WHEN OTHERS THEN
                        pop_err ('Customer',
                                 rec_cust.orig_system_reference,
                                 'VR71',
                                 'Invalid Credit Classification');
                --l_err := 'Y';

                END;
            END IF;


            --Open Sites
            FOR rec_sites IN get_sites (rec_cust.orig_system_reference)
            LOOP
                --Check address1
                IF rec_sites.address1 IS NULL THEN
                    pop_err ('Sites',
                             rec_cust.orig_system_reference,
                             'VV1',
                             'Address1 IS NULL');
                --l_err := 'Y';
                END IF;

                --Check Country
                IF rec_sites.country IS NULL THEN
                    pop_err ('Sites',
                             rec_cust.orig_system_reference,
                             'VV2',
                             'Country Value Is NULL');
                --l_err := 'Y';
                END IF;

                --Check County

                --Check State
                IF rec_sites.state IS NULL AND rec_sites.country = 'US'  THEN
                    pop_err ('Sites',
                             rec_cust.orig_system_reference,
                             'VV3',
                             'Invalid State');
                -- Satish U: Revisit at later point
                --l_err := 'Y';
                END IF;

                --Check postal code
                IF rec_sites.postal_code IS NULL AND rec_sites.country = 'US'  THEN
                    pop_err ('Postal Code',
                             rec_cust.orig_system_reference,
                             'VV4',
                             'Invalid Postal Code');
                -- Satish U: Revisit at later point
                --l_err := 'Y';
                END IF;

                --Check City
                IF rec_sites.city IS NULL AND rec_sites.country = 'US'  THEN
                    pop_err ('City',
                             rec_cust.orig_system_reference,
                             'VV5',
                             'Invalid City');
                -- Satish U: Revisit at later point
                --l_err := 'Y';
                END IF;

                --VR2 :Orig_System and Orig_System_Reference should both be Null or both should have value.
                IF (rec_sites.orig_system IS NULL
                    AND rec_sites.orig_system_reference IS NOT NULL)
                   OR (rec_sites.orig_system IS NOT NULL
                       AND rec_sites.orig_system_reference IS NULL)
                THEN
                    pop_err (
                        'Sites',
                        rec_cust.orig_system_reference,
                        'VR2',
                        'Invalid ORIG SYSTEM,ORIG SYSTEM REFERENCE Combination');
                    l_err := 'Y';
                END IF;

               fnd_file.put_line (fnd_file.LOG, 'Before Orig_System Validation');
                --VR3 :Validate Orig_System
                IF rec_sites.orig_system IS NOT NULL THEN
                    BEGIN
                        -- Satish U: Changing to Hz_Party_Sites
                        SELECT   1
                        INTO   l_rec_found
                        FROM   hz_party_sites hps, hz_parties hp
                         --hz_orig_systems_b
                        WHERE   hps.orig_system_reference =
                                     g_parameter_prefix
                                     || rec_sites.orig_system_reference
                        AND hps.party_id = hp.party_id
                        AND hp.orig_system_reference =
                                        g_parameter_prefix
                                        || rec_sites.orig_system_reference
                        AND ROWNUM =1;

                        pop_err ('Sites',
                                 rec_sites.orig_system_reference,
                                 'VR3',
                                 'Duplicate ORIG SYSTEM REFERENCE');
                        l_err := 'Y';
                    EXCEPTION
                        WHEN NO_DATA_FOUND  THEN
                            NULL;
                    END;
                    fnd_file.put_line (fnd_file.LOG, 'After Orig_System Validation');
                END IF;

                --VR18 :Country : Validation TERRITORY_CODE column in the FND_TERRITORY table.
                --ra_territories_kfv a
                IF rec_sites.country IS NOT NULL
                THEN
                    BEGIN
                        SELECT   1
                        INTO   l_rec_found
                        FROM   fnd_territories_vl
                        WHERE   territory_short_name = rec_sites.country;
                    EXCEPTION 
                        WHEN NO_DATA_FOUND THEN
                            pop_err ('Sites',
                                     rec_sites.orig_system_reference,
                                     'VR18',
                                     'Invalid COUNTRY');
                    --l_err := 'Y';
                    END;
                END IF;
                 fnd_file.put_line (fnd_file.LOG, 'Before Site Use Code Validation');
                --VR22 :Site_Use_Type Validation against AR Lookup Types
                IF rec_sites.site_use_type IS NOT NULL
                THEN
                    BEGIN
                        SELECT   1
                          INTO   l_rec_found
                          FROM   ar_lookups
                         WHERE   meaning = rec_sites.site_use_type
                                 AND lookup_type = 'PARTY_SITE_USE_CODE'
                                 AND TRUNC (SYSDATE) BETWEEN start_date_active
                                                         AND  NVL (
                                                                  end_date_active,
                                                                  TRUNC(SYSDATE)
                                                                  + 1)
                                 AND enabled_flag = 'Y';
                    EXCEPTION
                        WHEN NO_DATA_FOUND
                        THEN
                            pop_err ('Sites',
                                     rec_sites.orig_system_reference,
                                     'VR22',
                                     'Invalid Site Use Type');
                            l_err := 'Y';
                    END;
                END IF;

                --VR23 :Legal Collection on MSTR OR MISC
                IF rec_sites.attribute6 IS NOT NULL 
                AND rec_sites.hds_site_flag IN ('MISC','MSTR')
                THEN
                    BEGIN
                       
                       Select 1
                       Into l_Rec_Found 
                       From XXWC_LEGAL_COLLECTION_IND_XREF 
                       Where Prism_Code = Rec_Sites.Attribute6 
                       AND rec_sites.hds_site_flag IN ('MISC','MSTR')
                       AND ROWNUM =1; 
                       
                    EXCEPTION
                        WHEN NO_DATA_FOUND
                        THEN
                            pop_err ('Sites',
                                     rec_sites.orig_system_reference,
                                     'VR23',
                                     'Invalid attribute6 - legal collection');
                            --l_err := 'Y';
                    END;
                END IF;

                --VR24 :Legal Collection on MSTR OR MISC
                -- Changed XREF Table name : Satish U: 23-FEB-2012 
                -- Both Attribute6 and Attribute8 are validated against same XREF table 
                IF rec_sites.attribute8 IS NOT NULL 
                AND rec_sites.hds_site_flag IN ('MISC','MSTR')
                THEN
                    BEGIN
                       Select 1
                       Into l_Rec_Found 
                       From XXWC_LEGAL_COLLECTION_IND_XREF 
                       Where Prism_Code = Rec_Sites.Attribute8 
                       AND rec_sites.hds_site_flag IN ('MISC','MSTR')
                       AND ROWNUM =1; 
                       
                    EXCEPTION
                        WHEN NO_DATA_FOUND   THEN
                            pop_err ('Sites',
                                     rec_sites.orig_system_reference,
                                     'VR24',
                                     'Invalid attribute8 - collection agency');
                            --l_err := 'Y';
                    END;
                END IF;

                --VR25 :Bill TO Site Use Code� can not be null for �Ship To� Site Use.
                -- Satish U: Revisit this Validation
                IF UPPER (rec_sites.site_use_type) = 'SHIP TO'
                   AND rec_sites.bill_to_location IS NULL
                THEN
                    pop_err ('Sites',
                             rec_sites.orig_system_reference,
                             'VR25',
                             'Invalid Bill To Location For Ship To Site');
                --l_err := 'Y';
                END IF;


                --VR27 :GSA_Indicator : Should have values �Y� or �N�
                IF rec_sites.gsa_indicator NOT IN ('Y', 'N')
                THEN
                    pop_err ('Sites',
                             rec_sites.orig_system_reference,
                             'VR27',
                             'Invalid GAS Indicator');
                    l_err := 'Y';
                END IF;

                --VR28 :Salesrep Number Validation
                IF rec_sites.primary_salesrep_number IS NOT NULL  THEN
                    BEGIN
                        SELECT   1
                        INTO   l_rec_found
                        FROM   ra_salesreps
                        WHERE   salesrep_number = rec_sites.primary_salesrep_number;
                    EXCEPTION
                        WHEN NO_DATA_FOUND THEN
                            pop_err ('Sites',
                                     rec_sites.orig_system_reference,
                                     'VR28',
                                     'Invalid Sales Rep');
                    --Satish U: Currently Salesrep is Defaulted
                    --l_err := 'Y';
                    END;
                END IF;
            
                -- VR29 Site profile cust account status
                IF rec_sites.credit_analyst_name IS NOT NULL THEN
                    BEGIN
                       SELECT   1
                       INTO   l_rec_found
                       FROM   xxwc_account_status_xref a, ar_lookups b
                       WHERE   a.account_status = b.meaning
                       AND b.lookup_type = 'ACCOUNT_STATUS'
                       AND a.prism_credit_group =  rec_sites.credit_analyst_name;
                    EXCEPTION
                        WHEN NO_DATA_FOUND
                        THEN
                            pop_err ('Sites',
                                     rec_sites.orig_system_reference,
                                     'VR29',
                                     'Invalid Site Profile Account Status');
                    END;
                END IF;

                --VR30 :Validate Freight Terms
                IF rec_sites.fob_point IS NOT NULL
                THEN
                    BEGIN
                        SELECT   1
                        INTO   l_rec_found
                        FROM   fnd_lookup_values_vl
                        WHERE   meaning = rec_sites.fob_point
                        AND lookup_type = 'FREIGHT_TERMS'
                        AND TRUNC (SYSDATE) BETWEEN start_date_active
                           AND  NVL ( end_date_active, TRUNC(SYSDATE) + 1)
                        AND enabled_flag = 'Y';
                    EXCEPTION
                        WHEN NO_DATA_FOUND THEN
                            pop_err ('Sites',
                                     rec_sites.orig_system_reference,
                                     'VR30',
                                     'Invalid Freight Terms');
                    --l_err := 'Y';
                    END;
                END IF;

                --Party Contacts Loop
                FOR rec_contacts
                IN get_contacts (rec_sites.orig_system_reference)
                LOOP
                    --VR40 : Primary_Flag : Validation : Can have values �Y� /�N�.
                    IF rec_contacts.primary_flag NOT IN ('Y', 'N')
                    THEN
                        pop_err ('Contacts',
                                 rec_contacts.orig_system_reference,
                                 'VR40',
                                 'Invalid Primary Flag');
                        l_err := 'Y';
                    END IF;
                --VR41 : Contact point Purpose Validation: Validated against
                --AR Lookup Type CONTACT_POINT_PURPOSE when contact_type_purpose is not WEB.
                --Validated against AR Lookup Type CONTACT_POINT_PURPOSE_WEB when contact_type_purpose is WEB.
                END LOOP;                                       --get_contacts

                --Site Contacts Loop
                FOR rec_site_contacts
                IN get_site_contacts (rec_sites.orig_system_reference,
                                      rec_sites.legacy_customer_number)
                LOOP
                    --VR40 : Primary_Flag : Validation : Can have values �Y� /�N�.
                    IF rec_site_contacts.primary_flag NOT IN ('Y', 'N')
                    THEN
                        pop_err ('Site Contacts',
                                 rec_site_contacts.orig_system_reference,
                                 'VR41',
                                 'Invalid Primary Flag');
                        l_err := 'Y';
                    END IF;
                --VR41 : Contact point Purpose Validation: Validated against
                --AR Lookup Type CONTACT_POINT_PURPOSE when contact_type_purpose is not WEB.
                --Validated against AR Lookup Type CONTACT_POINT_PURPOSE_WEB when contact_type_purpose is WEB.
                END LOOP;                                  --get_site_contacts
            END LOOP;                                              --get_sites

            --Update the staging tables
            IF l_err = 'Y'
            THEN
                fnd_file.put_line (
                    fnd_file.LOG,
                    'Validation Failed : ' || rec_cust.orig_system_reference);

                UPDATE   xxwc_ar_customers_cnv
                   SET   process_status = 'E'
                 WHERE   orig_system_reference =
                             rec_cust.orig_system_reference;

                UPDATE   xxwc_ar_sites_cnv
                   SET   process_status = 'E'
                 WHERE   orig_system_reference =
                             rec_cust.orig_system_reference;

                UPDATE   xxwc_ar_contact_points_cnv
                   SET   process_status = 'E'
                 WHERE   orig_system_reference =
                             rec_cust.orig_system_reference;
            ELSE
                fnd_file.put_line (
                    fnd_file.LOG,
                    'Validation Successful : '
                    || rec_cust.orig_system_reference);

                UPDATE   xxwc_ar_customers_cnv
                   SET   process_status = 'V'
                        ,process_error = NULL
                 WHERE   orig_system_reference =
                             rec_cust.orig_system_reference;

                UPDATE   xxwc_ar_sites_cnv
                   SET   process_status = 'V'
                        ,process_error = NULL
                 WHERE   orig_system_reference =
                             rec_cust.orig_system_reference;

                UPDATE   xxwc_ar_contact_points_cnv
                   SET   process_status = 'V'
                        ,process_error = NULL
                 WHERE   orig_system_reference =
                             rec_cust.orig_system_reference;
            END IF;

            l_rec_count := l_rec_count + 1;

            IF l_rec_count >= c_max_commit_count
            THEN
                COMMIT;
            END IF;
        END LOOP;

        COMMIT;                                                     --get_cust
        
    END xxwc_validations;


    --- **************************************************************************************
    -- Procedure Create_Delivery_Contact :  API creates Delivery Contacts either at Party or Party Site Level. 
    
    
    Procedure  Create_Delivery_Contact(
            p_Party_ID         in Number ,
            p_Party_Site_ID    In Number ,
            p_Delivery_Contact In Varchar2 ,
            x_Return_Status    Out  Varchar2, 
            x_Msg_Data         Out  Varchar2 ) IS 
            
       -- party person recoprd variables
        ppersonrec               hz_party_v2pub.person_rec_type;
        oppartyid                hz_parties.party_id%TYPE;
        oppartynumber            hz_parties.party_number%TYPE;
        opprofileid              NUMBER;
        --party site contact
        porgcontactrec           hz_party_contact_v2pub.org_contact_rec_type;
        oocpartyid               hz_parties.party_id%TYPE;
        oocpartynumber           hz_parties.party_number%TYPE;
        oocpartyrelnumber        hz_org_contacts.contact_number%TYPE;
        oocpartyrelid            hz_party_relationships.party_relationship_id%TYPE;
        oocorgcontactid          hz_org_contacts.org_contact_id%TYPE;
        -- contact role
        pcustacctrolerec         hz_cust_account_role_v2pub.cust_account_role_rec_type;
        ocustacctroleid          hz_cust_account_roles.cust_account_role_id%TYPE;
        --contact variables
        pcontactpointrec         hz_contact_point_v2pub.contact_point_rec_type;
        pedirec                  hz_contact_point_v2pub.edi_rec_type;
        pemailrec                hz_contact_point_v2pub.email_rec_type;
        pphonerec                hz_contact_point_v2pub.phone_rec_type;
        ptelexrec                hz_contact_point_v2pub.telex_rec_type;
        pwebrec                  hz_contact_point_v2pub.web_rec_type;
        ocontactpointid          NUMBER;
        -- responsibility
        proleresponsibilityrec   hz_cust_account_role_v2pub.role_responsibility_rec_type;
        oresponsibilityid        NUMBER;
        l_proceed                VARCHAR2 (1);
        --
        ostatus                  VARCHAR2 (1);
        omsgcount                NUMBER;
        omsgdata                 VARCHAR2 (2000);

        v_dup_roll               NUMBER;

    Begin 
        X_Return_Status := 'S' ; 
        x_Msg_Data      := NULL; 
        ppersonrec.person_first_name := p_Delivery_Contact ;
        -- Create Party Id for Delivery Contact Person 
        select hz_party_number_s.nextval
        into ppersonrec.party_rec.party_number
        from dual;
        
        ppersonrec.party_rec.party_number := NULL; 
        ppersonrec.created_by_module:='TCA_V1_API';
        hz_party_v2pub.create_person (p_init_msg_list => 'T',
                                      p_person_rec    => ppersonrec,
                                      x_return_status => ostatus,
                                      x_msg_count     => omsgcount,
                                      x_msg_data      => omsgdata,
                                      x_party_id      => oppartyid,
                                      x_party_number  => oppartynumber,
                                      x_profile_id    => opprofileid);
        fnd_file.put_line (fnd_file.output,omsgdata);
        if ostatus <> 'S' then
           if omsgcount >1 then
              X_Msg_Data   := 'Person Creation API Failed:' ; 
              for i in 1..omsgcount  loop
                 X_Msg_Data := x_Msg_Data || substr(fnd_msg_pub.get(p_msg_index => i, p_encoded => fnd_api.g_false ), 1, 255);
              end loop;
              fnd_file.put_line (fnd_file.LOG,  x_Msg_Data);
           else
              x_Msg_Data := omsgdata ; 
              fnd_file.put_line (fnd_file.LOG,'Person Creation API Failed : ' || x_Msg_Data);
           end if;
           X_Return_Status:='E';
           return;
           l_proceed := 'N';
        else
           fnd_file.put_line (fnd_file.LOG,'Person Creation API Successful : ' );
           l_proceed := 'Y';
        end if; -- person creation
        
        If l_Proceed  = 'Y' Then 
           ostatus   := null;
           omsgcount := null;
           omsgdata  := null;
           porgcontactrec.created_by_module                := 'TCA_V1_API';
           porgcontactrec.job_title                        := null;
           porgcontactrec.Party_Site_ID                    := p_Party_Site_ID ; 
           porgcontactrec.party_rel_rec.subject_id         := oppartyid ;
           porgcontactrec.party_rel_rec.subject_type       := 'PERSON';
           porgcontactrec.party_rel_rec.subject_table_name := 'HZ_PARTIES';
           porgcontactrec.party_rel_rec.object_id          := p_party_id ;
           porgcontactrec.party_rel_rec.object_type        := 'ORGANIZATION';
           porgcontactrec.party_rel_rec.object_table_name  := 'HZ_PARTIES';
           porgcontactrec.party_rel_rec.relationship_code  := 'CONTACT_OF';
           porgcontactrec.party_rel_rec.relationship_type  := 'CONTACT';
           porgcontactrec.party_rel_rec.start_date         := sysdate;
           hz_party_contact_v2pub.create_org_contact(
                p_init_msg_list      => 'T',
                p_org_contact_rec    =>  porgcontactrec ,
                x_org_contact_id     =>  oocorgcontactid ,
                x_party_rel_id       =>  oocpartyrelid ,
                x_party_id           =>  oocpartyid ,
                x_party_number       =>  oocpartyrelnumber ,
                x_return_status      =>  ostatus,
                x_msg_count          =>  omsgcount,
                x_msg_data           =>  omsgdata);
           
           fnd_file.put_line (fnd_file.output,omsgdata);
           If ostatus <> 'S' Then
              If omsgcount >1 then
                 X_Msg_Data   := 'Create Org Contact API Error :' ; 
                 For i In 1..omsgcount    Loop
                    X_Msg_Data := x_Msg_Data || substr(fnd_msg_pub.get(p_msg_index => i, p_encoded => fnd_api.g_false ), 1, 255);
                 End Loop;
                 fnd_file.put_line (fnd_file.LOG, x_Msg_Data);
              Else
                 x_Msg_Data  := omsgdata ;
                 fnd_file.put_line (fnd_file.LOG,'Create Org API Failed : ' || x_Msg_Data);
              End If;
              X_Return_Status :='E';
              Return;
           Else
              fnd_file.put_line (fnd_file.LOG,'Create org API Successful : ' );
           End If; 
        End If; 
                                      
    End  Create_Delivery_Contact ; 
    
    
    
    --- ***************************************************************************************************
    -- Procedure upd_stagin_table_err :  API to update Process_Error value for all three staging tables. 
    --  THis is a private and Autonomous API 

    PROCEDURE upd_stagin_table_err (p_cus_err     VARCHAR2,
                                    p_site_err    VARCHAR2,
                                    p_con_err     VARCHAR2,
                                    p_rid         VARCHAR2)
    IS
        PRAGMA AUTONOMOUS_TRANSACTION;
    BEGIN
        IF p_cus_err IS NOT NULL
        THEN
            UPDATE   xxwc_ar_customers_cnv
               SET   process_error = p_cus_err
             WHERE   ROWID = p_rid;
        ELSIF p_site_err IS NOT NULL
        THEN
            UPDATE   xxwc_ar_sites_cnv
               SET   process_error = p_site_err
             WHERE   ROWID = p_rid;
        ELSIF p_con_err IS NOT NULL
        THEN
            UPDATE   xxwc_ar_contact_points_cnv
               SET   process_error = p_con_err
             WHERE   ROWID = p_rid;
        END IF;

        COMMIT;
    END;
    
    
    --- ***************************************************************************************************
    -- Procedure upd_stagin_table:  API to update Process_Statusvalue for all three staging tables. 
    --  THis is a private and Autonomous API 

    PROCEDURE upd_stagin_table (p_orig_system_reference IN VARCHAR2)
    IS
        PRAGMA AUTONOMOUS_TRANSACTION;
    BEGIN
        UPDATE   xxwc_ar_customers_cnv
           SET   process_status = 'E'
         WHERE   orig_system_reference = p_orig_system_reference;

        UPDATE   xxwc_ar_sites_cnv
           SET   process_status = 'E'
         WHERE   orig_system_reference = p_orig_system_reference;

        UPDATE   xxwc_ar_contact_points_cnv
           SET   process_status = 'E'
         WHERE   orig_system_reference = p_orig_system_reference;

        COMMIT;
    END upd_stagin_table;

    --- ***************************************************************************************************
    -- Procedure xxwc_customer_conv:  Main API called from Concurrent Program Customer Conversion Program  
    --  
    
    PROCEDURE xxwc_customer_conv (errbuf       OUT VARCHAR2,
                                  retcode      OUT VARCHAR2,
                                  p_mode           VARCHAR2,
                                  prefix    IN     VARCHAR2 DEFAULT NULL)
    AS
        -- cursor for selecting the party level information.
        CURSOR get_party
        IS
            SELECT   cus.ROWID rid, cus.*
              FROM   xxwc_ar_customers_cnv cus
             WHERE   1 = 1 
             AND process_status = 'V' ; 
             
             -- AND  Orig_System_Reference = '1040' ; 
             
             

        -- selecting multiple accounts for a party
        CURSOR get_account (
            p_cust IN VARCHAR2)
        IS
            SELECT   acc.ROWID rid, acc.*
              FROM   xxwc_ar_customers_cnv acc
             WHERE       1 = 1
                     AND orig_system_reference = p_cust
                     AND acc.process_status = 'V';

        -- selecting sites for the customer account
        
        
        CURSOR get_sites (
            p_cust IN VARCHAR2)
        IS
              SELECT   stg.ROWID rid, stg.*
              FROM   xxwc_ar_sites_cnv stg
              WHERE       1 = 1
              AND orig_system_reference = p_cust
              AND stg.process_status = 'V'
              AND stg.hds_site_flag <> 'SHIP'
              ORDER BY   legacy_site_type DESC;
         
         

        cust_party                get_party%ROWTYPE;

        --Get party contact point
        CURSOR get_contact (
            p_cust_site IN VARCHAR2)
        IS
            SELECT   stg.ROWID rid, stg.*
              FROM   xxwc_ar_contact_points_cnv stg
             WHERE       1 = 1
                     AND orig_system_reference = p_cust_site
                     AND legacy_party_site_number = orig_system_reference
                     AND stg.process_status = 'V';

        --Get site contact point
        CURSOR get_site_contact (
            p_cust_site                IN            VARCHAR2,
            p_legacy_customer_number   IN            VARCHAR2)
        IS
            SELECT   stg.ROWID rid, stg.*
              FROM   xxwc_ar_contact_points_cnv stg
             WHERE       1 = 1
                     AND orig_system_reference = p_cust_site
                     AND legacy_party_site_number = p_legacy_customer_number
                     AND stg.process_status = 'V';
                     
                     
       -- Satish U: 09-JAN-2012 Define a Cursor to get Frieght Terms from MISC or MAST and if value exists 
        Cursor  Freight_Terms_Cur (p_Orig_System_Reference  Varchar2 ) Is 
           Select Freight_Term, Legacy_party_Site_Number, HDS_SITE_FLAG
           From xxwc_ar_sites_cnv
           Where HDS_SITE_FLAG in ('MISC','MSTR')
           And   Orig_System_Reference = p_Orig_System_Reference 
           and Freight_Term is Not Null
           and rownum = 1 ; 
           
        l_Freight_Terms           Varchar2(40) ; 

        --
        ostatus                   VARCHAR2 (1);
        omsgcount                 NUMBER;
        omsgdata                  VARCHAR2 (2000);
        v_count                   NUMBER;
        l_count                   NUMBER;
        l_proceed                 VARCHAR2 (1);
        l_party                   VARCHAR2 (1);
        l_profile_id              NUMBER;
        -- party variables
        porganizationrec          hz_party_v2pub.organization_rec_type;
        opartyid                  hz_parties.party_id%TYPE;
        opartynumber              hz_parties.party_number%TYPE;
        oprofileid                NUMBER;
        osprofile_id              NUMBER;
        -- party account variables
        pcustaccountrec           hz_cust_account_v2pub.cust_account_rec_type;
        ppartyrec                 hz_party_v2pub.party_rec_type;
        pprofilerec               hz_customer_profile_v2pub.customer_profile_rec_type;
        ocustaccountid            hz_cust_accounts.cust_account_id%TYPE;
        ocustaccountno            hz_cust_accounts.account_number%TYPE;
        --location variable
        plocationrec              hz_location_v2pub.location_rec_type;
        olocationid               hz_locations.location_id%TYPE;
        --site variables
        ppartysiterec             hz_party_site_v2pub.party_site_rec_type;
        opartysiteid              hz_party_sites.party_site_id%TYPE;
        opartysiteno              hz_party_sites.party_site_number%TYPE;
        --site account avriables
        pcustacctsiterec          hz_cust_account_site_v2pub.cust_acct_site_rec_type;
        ocustacctsiteid           hz_cust_acct_sites.cust_acct_site_id%TYPE;
        --site use variables
        pcustacctsiteuserec       hz_cust_account_site_v2pub.cust_site_use_rec_type;
        pcustomerprofile          hz_customer_profile_v2pub.customer_profile_rec_type;
        ocustacctsiteuseid        NUMBER;
        pbilltositeuseid          NUMBER;
        pcustacctid               NUMBER;
        l_terms_id                NUMBER;
        l_sales_person_id         NUMBER;
        l_bp                      NUMBER;
        l_territory               NUMBER;
        ln_partysiteid_misc_bill_to NUMBER;
        oalias                    VARCHAR2 (240);

        -- amount profile variables
        pcustproamtrec            hz_customer_profile_v2pub.cust_profile_amt_rec_type;
        ocustacctprofileamtid     NUMBER;
        pcurrencycode             VARCHAR2 (10);
        ---------
        whereami                  NUMBER := 0;
        e_validation_exception exception;
        p_result                  VARCHAR2 (1);
        p_mstr_counter            NUMBER;
        p_yard_counter            NUMBER;
        p_loc_count               NUMBER;
        p_mstr_loc_count          NUMBER;
        p_yard_loc_count          NUMBER;
        p_job_loc_count           NUMBER;

        req_status                BOOLEAN;
        v_version_no              NUMBER;

        p_collector_id            NUMBER;
        p_credit_analyst_id       NUMBER;
        p_credit_classification   ar_lookups.lookup_code%TYPE;
        p_payment_term_id         NUMBER;
        p_object_version_number   NUMBER;
        v_err                     VARCHAR2 (1) := 'N';
        p_cus_err                 VARCHAR2 (4000);
        p_site_err                VARCHAR2 (4000);
        p_con_err                 VARCHAR2 (4000);
        p_rid                     VARCHAR2 (100);
        l_rec_count               NUMBER;
        c_max_commit_count        CONSTANT NUMBER := 100;
        l_api_name                VARCHAR2 (30) := 'XXWC_CUSTOMER_CONV';
        lv_primary_ship_to_flag   VARCHAR2 (2);
    BEGIN
        --If mode Validate the validate the records
        g_parameter_prefix := prefix;
        l_rec_count := 0;

        fnd_file.put_line (fnd_file.LOG,
                           'Begining of Customer Validation Process');
        xxwc_validations;
        fnd_file.put_line (fnd_file.LOG,
                           'Customer Validation Process is Complete');

        IF p_mode = 'Validate'
        THEN
            get_cust_conversion_stats;
            RETURN;
        END IF;

        fnd_file.put_line (fnd_file.LOG, 'CUSTOMER CONVERSION');
        fnd_file.put_line (fnd_file.LOG, '********************');

        

        v_count := 0;

        FOR cust_party IN get_party
        LOOP
            SAVEPOINT customer_record;
            l_rec_count := l_rec_count + 1;

            -- fnd_file.put_line(fnd_file.log,'1111-Inside Customer Party Loop'||Cust_party.Orig_System_Reference);
            BEGIN
                v_count := v_count + 1;
                l_party := 'N';
                --fnd_file.put_line (fnd_file.LOG, '1110-');

                IF l_party = 'N'
                THEN
                    -- start create party
                    opartyid := NULL;
                    porganizationrec := NULL;
                    --initialize the values
                    ostatus := NULL;
                    omsgcount := NULL;
                    omsgdata := NULL;
                    -- create the party organization record
                    porganizationrec.organization_name :=
                        prefix || cust_party.organization_name;
                    porganizationrec.party_rec.orig_system_reference :=
                        prefix || cust_party.orig_system_reference;
                    --porganizationrec.year_established :=
                    --  TO_NUMBER (to_char(to_date(cust_party.year_established,'YYYY-MM-DD'),'YYYY'));
                    porganizationrec.duns_number_c := cust_party.duns_number_c;
                    porganizationrec.gsa_indicator_flag :=
                        cust_party.gsa_indicator_flag;
                    fnd_file.put_line (fnd_file.LOG, '200 Building Organization Record');
                    --Sic code and sic code type
                    IF cust_party.sic_code IS NOT NULL
                       AND cust_party.sic_code <> 0
                    THEN
                        BEGIN
                            whereami := 95;
                            -- Satish U: 12-MAR-2012 : Changed from 1972 SIC to 1987 SIC 
                            SELECT   lookup_code, lookup_type
                              INTO   porganizationrec.sic_code,
                                     porganizationrec.sic_code_type
                              FROM   ar_lookups b
                             WHERE   b.lookup_code = cust_party.sic_code
                                     AND b.lookup_type = '1987 SIC';
                        EXCEPTION
                            WHEN NO_DATA_FOUND
                            THEN
                                porganizationrec.sic_code_type := NULL;
                                porganizationrec.sic_code := NULL;
                        END;
                    END IF;

                    -- switch this value to url field after conversion
                    porganizationrec.attribute20 :=
                        cust_party.cod_comment || '-' || cust_party.keyword;

                    --porganizationrec.party_rec.status:=cust_party.status;
                    /*porganizationrec.party_rec.party_number:= cust_party.cust_num;
                    porganizationrec.tax_reference := cust_party.tax_reg_no; (will be added back when decided how to handle multpile tax numbers)
                  porganizationrec.jgzz_fiscal_code:= cust_party.taxpayer_id; --tax payer id*/
                    porganizationrec.created_by_module := 'TCA_V1_API';

                    --porganizationrec.known_as:=oalias;
                    whereami := 100;

                    SELECT   hz_parties_s.NEXTVAL
                      INTO   ppartyrec.party_id
                      FROM   DUAL;

                    --
                    -- party api
                    --fnd_file.put_line(fnd_file.log,'900-Before Calling API Create Organization'||Cust_party.Orig_System_Reference);
                    hz_party_v2pub.create_organization (
                        p_init_msg_list      => 'T',
                        p_organization_rec   => porganizationrec,
                        x_return_status      => ostatus,
                        x_msg_count          => omsgcount,
                        x_msg_data           => omsgdata,
                        x_party_id           => opartyid,
                        x_party_number       => opartynumber,
                        x_profile_id         => oprofileid);
                END IF;


                IF ostatus <> 'S'
                THEN
                    whereami := 110;

                    --fnd_file.put_line(fnd_file.log,'1111-Create Organization Errored Out : Msg Count :'||omsgcount);
                    IF omsgcount > 1
                    THEN
                        whereami := 115;

                        FOR i IN 1 .. omsgcount
                        LOOP
                            p_cus_err :=
                                p_cus_err
                                || SUBSTR (
                                       fnd_msg_pub.get (
                                           i,
                                           p_encoded   => fnd_api.g_false),
                                       1,
                                       255);
                        END LOOP;

                        --Satish U: 11/01/2011 : FOllwoing Statement Moved outside the Loop
                        fnd_file.put_line (
                            fnd_file.LOG,
                               'PARTY ORGANIZATION RECORD '
                            || opartynumber
                            || SUBSTR (p_cus_err, 1, 255));
                    ELSE
                        whereami := 120;
                        fnd_file.put_line (
                            fnd_file.LOG,
                               'PARTY ORGANIZATION RECORD '
                            || omsgdata
                            || opartynumber);
                        p_cus_err := omsgdata;
                    END IF;

                    p_rid := cust_party.rid;
                    whereami := 130;
                    RAISE e_validation_exception;
                ELSE
                    --initialize the values
                    ostatus := NULL;
                    omsgcount := NULL;
                    omsgdata := NULL;

                    -- call the customer account api
                    -- get orig system refrence value
                    FOR cust_acc
                    IN get_account (cust_party.orig_system_reference)
                    LOOP
                        pcustproamtrec := NULL;
                        pcustaccountrec := NULL;
                        pprofilerec := NULL;
                        lv_primary_ship_to_flag := NULL;

                        --create org level communication
                        --xxpop_org_communication(opartyid,cust_acc.contact_phone,cust_acc.contact_fax,p_result);
                        --     if p_result='e' then
                        --               raise e_validation_exception;
                        --     end if;

                        porganizationrec.party_rec.party_id := opartyid;
                        pcustaccountrec.account_name :=  prefix || cust_acc.account_name;
                        pcustaccountrec.account_number :=  prefix || cust_acc.orig_system_reference;
                        -- Satish U: 30-JAN-2012 : Account Should always be created in Active Status 
                        pcustaccountrec.status := 'A' ; --cust_acc.status;
                        pcustaccountrec.orig_system_reference :=     prefix || cust_acc.orig_system_reference;
                        pcustaccountrec.tax_code := NULL;

                        --account level get profile class
                        whereami := 140;

                        BEGIN
                            SELECT   profile_class_id
                              --customer_profile_class_id
                            INTO   l_profile_id
                            FROM   hz_cust_profile_classes cpc, --ar_customer_profile_classes_v av,
                                     xxwc_cust_profile_class_xref x
                            WHERE   UPPER (cpc.name) = UPPER (x.profile_class)
                            AND x.prism_code =  TO_NUMBER(cust_acc.customer_profile_class_name)
                            AND cpc.status = 'A';
                        EXCEPTION
                            WHEN OTHERS THEN
                                l_profile_id := 0;                --is default
                        END;


                        pprofilerec.profile_class_id := l_profile_id;

                        --pprofilerec. credit_checking:=cust_acc.credit_check;
                        IF cust_acc.send_statements IN ('Y', 'N', 'C')
                        THEN
                            IF cust_acc.send_statements IN ('Y', 'C')  THEN
                                pprofilerec.send_statements := 'Y';
                            ELSE
                                pprofilerec.send_statements := 'N';
                            END IF;
                        ELSE
                            pprofilerec.send_statements := 'N';
                        END IF;

                        IF cust_acc.send_statements NOT IN ('Y', 'N', 'C')
                        THEN
                            pprofilerec.credit_balance_statements := 'N';
                        ELSIF cust_acc.send_statements IN ('Y', 'C', 'N') THEN
                            IF cust_acc.credit_balance_statements IN
                                       ('Y', 'C')
                            THEN
                                pprofilerec.credit_balance_statements := 'Y';
                            ELSIF cust_acc.credit_balance_statements = 'N'
                            THEN
                                pprofilerec.credit_balance_statements := 'N';
                            END IF;
                        END IF;

                        IF cust_acc.attribute3 IN ('Y', 'N', 'C')
                           AND cust_acc.send_statements IN ('Y', 'C')
                        THEN
                            IF cust_acc.attribute3 = 'C'       --IN ('Y', 'N')
                            THEN
                                pprofilerec.attribute3 := 'Y'; --cust_acc.attribute3; --Y N null Statement by Job
                            ELSE
                                pprofilerec.attribute3 := 'N';
                            END IF;
                        END IF;

                        pprofilerec.credit_hold := cust_acc.credit_hold;

                        BEGIN
                            SELECT   x.collector_id          --ac.collector_id
                              INTO   p_collector_id
                              FROM                         --ar_collectors ac,
                                  xxwc_collector_cr_analyst_xref x
                             WHERE   1 = 1
                                     --UPPER (ac.name) = UPPER (oracle_collector)
                                     AND UPPER (x.prism_code) =
                                            UPPER (cust_acc.collector_name);
                        --AND ac.status = 'A';
                        EXCEPTION
                            WHEN OTHERS
                            THEN
                                p_collector_id := 1;       --Default collector
                        END;

                        pprofilerec.collector_id := p_collector_id;

                        BEGIN
                            whereami := 150;

                            --- Check this Query for performance and in the exception  add message to OUT
                            SELECT   x.credit_analyst_id
                              INTO   p_credit_analyst_id
                              FROM   xxwc_collector_cr_analyst_xref x
                             WHERE   1 = 1
                                     AND UPPER (x.prism_code) =
                                            UPPER (cust_acc.collector_name);
                        EXCEPTION
                            WHEN OTHERS
                            THEN
                                p_credit_analyst_id := NULL;
                        END;

                        pprofilerec.credit_analyst_id := p_credit_analyst_id;

                        BEGIN
                            whereami := 160;

                            --- Check this Query for performance and in the exception  add message to OUT
                            -- Mention in the output that Credit Classification is defaulting
                            SELECT   al.lookup_code
                              INTO   p_credit_classification
                              FROM   ar_lookups al,
                                     xxwc_cr_classification_xref x
                             WHERE   al.lookup_code = x.oracle_code
                                     AND UPPER (x.prism_code) =
                                            UPPER(cust_acc.credit_classification)
                                     AND al.lookup_type =
                                            'AR_CMGT_CREDIT_CLASSIFICATION'
                                     AND al.enabled_flag = 'Y'
                                     AND TRUNC (SYSDATE) BETWEEN TRUNC(al.start_date_active)
                                                             AND  TRUNC(NVL (
                                                                            al.end_date_active,
                                                                            SYSDATE
                                                                            + 1));
                        EXCEPTION
                            WHEN OTHERS
                            THEN
                                p_credit_classification := 'VHIGH';
                        END;

                        pprofilerec.credit_classification := p_credit_classification;

                        /*DFF Name ========
                          Customer Information (Account level)*/

                        --CHECK CREDIT CLASS CODE WITH
                        IF UPPER (cust_acc.attribute1) IN ('Y', 'YES')
                        THEN
                            pcustaccountrec.attribute_category := 'Yes'; --'Yes';
                        END IF;

                        --Mandatory Notes
                        --pcustaccountrec.attribute6:='Old Mandatory Note';  --Old Mandatory Note';
                        pcustaccountrec.attribute17 := cust_acc.attribute17;
                        pcustaccountrec.attribute18 := cust_acc.attribute18;
                        pcustaccountrec.attribute19 := cust_acc.attribute19;
                        pcustaccountrec.attribute20 := cust_acc.attribute20;
                        pcustaccountrec.attribute16 := cust_acc.attribute21; -- added by v.sankar on 10/24/2011

                        --added by v.sankar on 11/22/2011
                        --customer source
                        pcustaccountrec.attribute4 := cust_acc.customer_source;
                        --account date established
                        pcustaccountrec.account_established_date :=  TO_DATE (cust_acc.year_established, 'YYYY-MM-DD');

                        --added by v.sankar on 12/14/2011
                        -- attribute6 -- Legal Agency                     
                        BEGIN
                            -- Satish U 02-21-2-12 : SHould be Populating Oracle Legal Indicator column value
                            -- Satish U 02-23-12 
                           SELECT  Oracle_Legal_Indicator
                           INTO   pcustaccountrec.attribute5
                           FROM   XXWC_LEGAL_COLLECTION_IND_XREF a, 
                                  xxwc_ar_sites_cnv sites
                           WHERE   a.PRISM_CODE = sites.attribute8
                           AND sites.orig_system_reference = cust_acc.orig_system_reference
                           AND sites.hds_site_flag IN ('MSTR', 'MISC')
                           AND ROWNUM = 1;
                           fnd_file.put_line (fnd_file.LOG,'Legal Collection Indicator Found Attribute8: ' );
                           
                        
                        EXCEPTION
                           WHEN OTHERS   THEN
                              pcustaccountrec.attribute5 := NULL;
                              fnd_file.put_line (fnd_file.LOG,' Legacy Agency : First When Others : NULL value: ' );
                        END;

                        --collection agency attribute5
                        -- Satish U: 24-JAN-2012 :  Should be Attribute6 not Attribute8 on Sites Record 
                        
                        BEGIN
                          
                           --SELECT   Prism_code
                           -- Satish U 02-21-2-12 : SHould be Populating Oracle Legal Indicator column value
                           If pcustaccountrec.attribute5 Is Null Then 
                               SELECT  Oracle_Legal_Indicator
                               INTO   pcustaccountrec.attribute5
                               FROM   XXWC_LEGAL_COLLECTION_IND_XREF a, 
                                      xxwc_ar_sites_cnv sites
                               WHERE   a.PRISM_CODE = sites.attribute6
                               AND sites.orig_system_reference = cust_acc.orig_system_reference
                               AND sites.hds_site_flag IN ('MSTR', 'MISC')
                               AND ROWNUM = 1;
                               fnd_file.put_line (fnd_file.LOG,'Collection Agency: found Attribyte6: ' );
                           
                           End If; 
                        EXCEPTION
                           WHEN OTHERS  THEN
                              Begin 
                                  -- Check If Site Record Attribute6 has value 
                                  -- If so then assign default value 'AGENCY' to Attribute5 column 
                                 Select 'AGENCY'
                                 Into pcustaccountrec.attribute5
                                 From xxwc_ar_sites_cnv sites 
                                 Where sites.orig_system_reference = cust_acc.orig_system_reference
                                 AND sites.hds_site_flag IN ('MSTR', 'MISC')
                                 AND Sites.Attribute6 is NOT NULL
                                 AND ROWNUM = 1;
                                 fnd_file.put_line (fnd_file.LOG,'Default Value Agency: ' );
                              Exception 
                                 When Others THen 
                                    pcustaccountrec.attribute5 := NULL;
                                    fnd_file.put_line (fnd_file.LOG,' Legacy Agency : Final When Others : NULL value: ' );
                              End ; 
                            
                        END;
                        --end added by v.sankar

                        pcustaccountrec.created_by_module := 'TCA_V1_API';

                        --account level customer classification
                        BEGIN
                            whereami := 170;

                            SELECT   fl.lookup_code
                            INTO   pcustaccountrec.customer_class_code
                            FROM   fnd_lookup_values_vl fl,
                                   xxwc_cust_classification_xref x
                            WHERE   fl.lookup_type = 'CUSTOMER CLASS'
                            AND UPPER (fl.lookup_code) = UPPER (x.cust_classification)
                            AND UPPER (x.prism_code) = UPPER ( cust_acc.customer_class_code)
                            AND TRUNC (SYSDATE) BETWEEN fl.start_date_active
                            AND  NVL (fl.end_date_active,TRUNC(SYSDATE) + 1)
                            AND fl.enabled_flag = 'Y';
                        EXCEPTION
                            WHEN OTHERS  THEN
                                pcustaccountrec.customer_class_code :=  'GEN_CONTRACT';
                        END;

                        -- account level payment terms account
                        BEGIN
                            whereami := 180;

                            SELECT   rt.term_id
                            INTO   p_payment_term_id
                            FROM   xxwc_payment_terms_xref xpt, ra_terms rt
                            WHERE   UPPER (rt.name) = UPPER (xpt.oracle_term)
                            AND xpt.prism_code = cust_acc.standard_term_name
                            AND TRUNC (SYSDATE) BETWEEN start_date_active
                            AND  NVL (end_date_active,TRUNC(SYSDATE)  + 1);
                        EXCEPTION
                            WHEN OTHERS
                            THEN
                                p_payment_term_id := NULL;
                        END;

                        pprofilerec.standard_terms := p_payment_term_id;

                        
                        BEGIN
                            whereami := 190;

                            SELECT   oracle_code
                              INTO   pprofilerec.attribute2
                              FROM   xxwc_remit_to_addr_xref x
                             WHERE   x.prism_code = cust_acc.attribute2;
                        EXCEPTION
                            WHEN NO_DATA_FOUND
                            THEN
                                pprofilerec.attribute2 := 2;
                        END;

                        --account status . added by v.sankar on 11/22/2011
                        BEGIN
                            whereami := 195;

                            SELECT   lookup_code
                              INTO   pprofilerec.account_status
                              FROM   xxwc_account_status_xref a, ar_lookups b
                             WHERE   a.account_status = b.meaning
                                     AND b.lookup_type = 'ACCOUNT_STATUS'
                                     AND a.prism_credit_group =
                                            cust_acc.credit_analyst_name;
                        EXCEPTION
                            WHEN NO_DATA_FOUND
                            THEN
                                pprofilerec.account_status := NULL;
                        END;
                        -- Satish U : Udpate Customer Account : Frieght Terms : 09JAN-2012 
                        For Freight_Terms_Rec In Freight_Terms_Cur(cust_acc.Orig_System_Reference )  Loop 
                           l_Freight_Terms := NULL; 
                           
                           IF UPPER( Freight_Terms_Rec.Freight_Term) =    'FREIGHT EXEMPT'  THEN
                              pcustaccountrec.Freight_Term :=  'EXEMPT';
                           ELSE
                              pcustaccountrec.Freight_Term :=   NULL;
                           END IF;

                           --l_Freight_Terms := Freight_Terms_Rec.Freight_Term; 
                           --pcustaccountrec.Freight_Term := l_Freight_Terms ; 
                        End Loop ; 
                        
                        fnd_file.put_line (
                            fnd_file.LOG,
                            'Before Calling Create Customer Account Record :'
                            || pcustaccountrec.account_name);
                        hz_cust_account_v2pub.create_cust_account (
                            p_init_msg_list          => 'T',
                            p_cust_account_rec       => pcustaccountrec, -- customer account record
                            p_organization_rec       => porganizationrec, -- party organization record
                            p_customer_profile_rec   => pprofilerec,
                            p_create_profile_amt     => fnd_api.g_true, --fnd_api.g_true
                            x_cust_account_id        => ocustaccountid,
                            x_account_number         => ocustaccountno,
                            x_party_id               => opartyid,
                            x_party_number           => opartynumber,
                            x_profile_id             => oprofileid,
                            x_return_status          => ostatus,
                            x_msg_count              => omsgcount,
                            x_msg_data               => omsgdata);

                        IF ostatus <> 'S'
                        THEN
                            whereami := 200;

                            --fnd_file.put_line(fnd_file.log,'1112-Create Customer Account API Errored OUt'||omsgcount);
                            IF omsgcount > 1
                            THEN
                                whereami := 205;

                                FOR i IN 1 .. omsgcount
                                LOOP
                                    p_cus_err :=
                                        NVL (p_cus_err, ' ')
                                        || SUBSTR (
                                               fnd_msg_pub.get (
                                                   i,
                                                   p_encoded   => fnd_api.g_false),
                                               1,
                                               255);
                                END LOOP;

                                --SatishU: 11/01/11 : Following SQL Statement Moved outside the Loop
                                fnd_file.put_line (
                                    fnd_file.LOG,
                                       'CUSTOMER ACCOUNT RECORD '
                                    || cust_acc.account_name
                                    || ' '
                                    || SUBSTR (p_cus_err, 1, 255));
                            ELSE
                                whereami := 210;
                                fnd_file.put_line (
                                    fnd_file.LOG,
                                       'CUSTOMER ACCOUNT RECORD '
                                    || cust_acc.account_name
                                    || ' '
                                    || omsgdata);
                                p_cus_err := omsgdata;
                            END IF;

                            p_rid := cust_party.rid;
                            whereami := 220;
                            RAISE e_validation_exception;
                        ELSE
                            IF NVL (cust_acc.trx_credit_limit, 0) != 0
                               OR NVL (cust_acc.overall_credit_limit, 0) != 0
                            THEN
                                pcustproamtrec := NULL;

                                IF oprofileid IS NULL
                                THEN
                                    whereami := 230;

                                    SELECT   cust_account_profile_id
                                      INTO   oprofileid
                                      FROM   hz_customer_profiles
                                     WHERE   cust_account_id = ocustaccountid;
                                END IF;

                                IF oprofileid IS NOT NULL
                                THEN
                                    BEGIN
                                        whereami := 240;

                                        SELECT   cust_acct_profile_amt_id,
                                                 object_version_number
                                          INTO   ocustacctprofileamtid,
                                                 p_object_version_number
                                          FROM   hz_cust_profile_amts
                                         WHERE   cust_account_profile_id =
                                                     oprofileid
                                                 AND currency_code = 'USD';
                                    EXCEPTION
                                        WHEN NO_DATA_FOUND
                                        THEN
                                            ocustacctprofileamtid := NULL;
                                            p_object_version_number := NULL;
                                    END;

                                    IF ocustacctprofileamtid IS NOT NULL
                                    THEN
                                        --Update amount profile
                                        pcustproamtrec.cust_acct_profile_amt_id :=
                                            ocustacctprofileamtid;
                                        pcustproamtrec.trx_credit_limit :=
                                            cust_acc.trx_credit_limit;
                                        pcustproamtrec.overall_credit_limit :=
                                            cust_acc.overall_credit_limit;
                                        pcustproamtrec.created_by_module :=
                                            'TCA_V1_API';
                                        fnd_file.put_line (
                                            fnd_file.LOG,
                                            ' Before Calling API Update Customer Profile Amt : ');
                                        hz_customer_profile_v2pub.update_cust_profile_amt (
                                            p_init_msg_list           => 'T',
                                            p_cust_profile_amt_rec    => pcustproamtrec,
                                            p_object_version_number   => p_object_version_number,
                                            x_return_status           => ostatus,
                                            x_msg_count               => omsgcount,
                                            x_msg_data                => omsgdata);

                                        IF ostatus <> 'S'
                                        THEN
                                            whereami := 230;

                                            -- fnd_file.put_line(fnd_file.log,'1113-Update Profile Amount API Errored Out'||omsgcount);
                                            IF omsgcount > 1
                                            THEN
                                                FOR i IN 1 .. omsgcount
                                                LOOP
                                                    p_cus_err :=
                                                        NVL (p_cus_err, ' ')
                                                        || SUBSTR (
                                                               fnd_msg_pub.get (
                                                                   i,
                                                                   p_encoded   => fnd_api.g_false),
                                                               1,
                                                               255);
                                                END LOOP;

                                                -- Satish U: 11/01/11 Moved Outside the Loop
                                                fnd_file.put_line (
                                                    fnd_file.LOG,
                                                    ' CUSTOMER Amout Profile Updation '
                                                    || SUBSTR (p_cus_err,
                                                               1,
                                                               255));
                                            ELSE
                                                whereami := 240;
                                                fnd_file.put_line (
                                                    fnd_file.LOG,
                                                    ' CUSTOMER Amount Profile Updation '
                                                    || omsgdata);
                                                p_cus_err := omsgdata;
                                            END IF;

                                            p_rid := cust_party.rid;
                                            RAISE e_validation_exception;
                                        END IF;
                                    ELSE
                                        --Create Profile Amount
                                        pcustproamtrec.cust_account_profile_id :=
                                            oprofileid; -- customer profile id from above api
                                        pcustproamtrec.cust_account_id :=
                                            ocustaccountid;
                                        pcustproamtrec.currency_code := 'USD';
                                        pcustproamtrec.trx_credit_limit :=
                                            cust_acc.trx_credit_limit;
                                        pcustproamtrec.overall_credit_limit :=
                                            cust_acc.overall_credit_limit;
                                        pcustproamtrec.created_by_module :=
                                            'TCA_V1_API';
                                        fnd_file.put_line (
                                            fnd_file.LOG,
                                            ' Before Calling Create Customer Profile Amount API ');
                                        hz_customer_profile_v2pub.create_cust_profile_amt (
                                            p_init_msg_list              => 'T',
                                            p_check_foreign_key          => 'T',
                                            p_cust_profile_amt_rec       => pcustproamtrec,
                                            x_cust_acct_profile_amt_id   => ocustacctprofileamtid,
                                            x_return_status              => ostatus,
                                            x_msg_count                  => omsgcount,
                                            x_msg_data                   => omsgdata);

                                        IF ostatus <> 'S'
                                        THEN
                                            whereami := 250;

                                            -- fnd_file.put_line(fnd_file.log,'1114- Create Customer Profile AMpount API Errored Out'||omsgcount);
                                            IF omsgcount > 1
                                            THEN
                                                FOR i IN 1 .. omsgcount
                                                LOOP
                                                    p_cus_err :=
                                                        NVL (p_cus_err, ' ')
                                                        || SUBSTR (
                                                               fnd_msg_pub.get (
                                                                   i,
                                                                   p_encoded   => fnd_api.g_false),
                                                               1,
                                                               255);
                                                END LOOP;

                                                -- Satish U: Move Outside the Loop
                                                fnd_file.put_line (
                                                    fnd_file.LOG,
                                                    ' CUSTOMER Amout Profile Creation '
                                                    || SUBSTR (p_cus_err,
                                                               1,
                                                               255));
                                            ELSE
                                                fnd_file.put_line (
                                                    fnd_file.LOG,
                                                    ' CUSTOMER Amount Profile Creation '
                                                    || omsgdata);
                                                p_cus_err := omsgdata;
                                            END IF;

                                            p_rid := cust_party.rid;
                                            whereami := 260;
                                            RAISE e_validation_exception;
                                        END IF;
                                    END IF;
                                END IF;
                            END IF;                           --amount profile

                            

                            IF p_result = 'E'
                            THEN
                                whereami := 270;
                                RAISE e_validation_exception;
                            END IF;

                            --set the yard counter
                            p_yard_counter := 1;
                            p_mstr_counter := 1;
                            p_loc_count := 1;
                            p_mstr_loc_count := 1;
                            p_yard_loc_count := 1;
                            p_job_loc_count := 1;

                            --Open Sites
                            FOR cust_sites
                            IN get_sites (cust_acc.orig_system_reference)
                            LOOP
                                --fnd_file.put_line (fnd_file.output,'CREATING SITE ' || cust_sites.location || 'FOR CUSTOMER ' || cust_party.account_name);
                                plocationrec := NULL;
                                ppartysiterec := NULL;
                                pcustacctsiterec := NULL;
                                pcustacctsiteuserec := NULL;
                                pcustomerprofile := NULL;
                                ostatus := NULL;
                                omsgcount := NULL;
                                omsgdata := NULL;
                                ln_partysiteid_misc_bill_to := NULL;

                                l_count := 0;
                                olocationid := NULL;
                                ostatus := NULL;

                                BEGIN
                                    SELECT   location_id
                                      INTO   olocationid
                                      FROM   hz_locations
                                     WHERE   location_id =
                                                 cust_sites.location_id;

                                    l_count := 1;
                                    --olocationid := Add_Rec.Location_ID;
                                    ostatus := 'S';
                                EXCEPTION
                                    WHEN NO_DATA_FOUND
                                    THEN
                                        l_count := 0;
                                        ostatus := NULL;
                                END;

                                


                                IF l_count = 0
                                THEN
                                    -- location creation
                                    IF NVL (cust_sites.country, 'USA') IN
                                               ('USA', 'US')
                                    THEN
                                        plocationrec.country := 'US';
                                    
                                    ELSE
                                        plocationrec.country :=
                                            cust_sites.country;
                                    END IF;

                                    --SatishU : 10/26/2011
                                    plocationrec.location_id :=
                                        cust_sites.location_id; -- added MISC v.sankar 10/31/2011
                                    plocationrec.postal_code :=
                                        (cust_sites.postal_code);
                                    plocationrec.address1 :=
                                        NVL (cust_sites.address1,
                                             'NO ADDRESS FOUND');
                                    plocationrec.address2 :=
                                        cust_sites.address2;
                                    plocationrec.address3 :=
                                        cust_sites.address3;
                                    plocationrec.address4 :=
                                        cust_sites.address4; 
                                    plocationrec.state := cust_sites.state; -- is mandatory
                                    plocationrec.city := cust_sites.city;
                                    plocationrec.county := cust_sites.county; --nvl(c2.county,c2.city); --for the time being (will be updated for sales tax later)
                                    plocationrec.created_by_module :=
                                        'TCA_V1_API';
                                    -- Added  Attribute6 PRISM Customer#  Satish U : 02/21/2012 
                                    --plocationrec.attribute17 := cust_sites.Legacy_Party_Site_Number ;
                                    whereami := 280;
                                    fnd_file.put_line (
                                        fnd_file.LOG,
                                        ' Before Calling Create Location API ');
                                    hz_location_v2pub.create_location (
                                        p_init_msg_list   => 'T',
                                        p_location_rec    => plocationrec,
                                        x_location_id     => olocationid,
                                        x_return_status   => ostatus,
                                        x_msg_count       => omsgcount,
                                        x_msg_data        => omsgdata);
                                END IF;

                                IF ostatus <> 'S'
                                THEN
                                    whereami := 290;

                                    --fnd_file.put_line(fnd_file.log,'1115- Create Location API Errored Out'||omsgcount);
                                    IF omsgcount > 1
                                    THEN
                                        FOR i IN 1 .. omsgcount
                                        LOOP
                                            p_site_err :=
                                                NVL (p_site_err, ' ')
                                                || SUBSTR (
                                                       fnd_msg_pub.get (
                                                           i,
                                                           p_encoded   => fnd_api.g_false),
                                                       1,
                                                       255);
                                        END LOOP;

                                        --Satish U: Moving Outside the Loop
                                        fnd_file.put_line (
                                            fnd_file.LOG,
                                            ' CUSTOMER LOCATION RECORD '
                                            || SUBSTR (p_site_err, 1, 255));
                                    ELSE
                                        whereami := 300;
                                        fnd_file.put_line (
                                            fnd_file.LOG,
                                               ' CUSTOMER LOCATION RECORD '
                                            || omsgdata
                                            || '-'
                                            || cust_sites.legacy_customer_number
                                            || '-'
                                            || cust_sites.country
                                            || '-'
                                            || cust_sites.address1
                                            || '-'
                                            || cust_sites.address2);
                                        p_site_err := omsgdata;
                                    END IF;

                                    p_rid := cust_sites.rid;
                                    whereami := 310;
                                    RAISE e_validation_exception;
                                ELSE
                                    --fnd_file.put_line (fnd_file.output,'CUSTOMER LOCATION CREATED:- ' ||   cust_sites.address1 || '  '  ||cust_sites.address2);
                                    --initialize the values
                                    ostatus := NULL;
                                    omsgcount := NULL;
                                    omsgdata := NULL;
                                    opartysiteid := NULL;
                                    opartysiteno := NULL;
                                    -- create a party site now
                                    ppartysiterec.party_id := opartyid;
                                    ppartysiterec.location_id := olocationid;

                                    --v.sankar 11/22/2011. needs to be fixed next round.
                                    ppartysiterec.party_site_number :=
                                        SUBSTR (
                                            cust_sites.legacy_party_site_number,
                                            1,
                                            25);
                                       

                                    

                                    IF cust_sites.hds_site_flag = 'MISC'
                                    THEN
                                        ppartysiterec.party_site_name :=
                                            cust_sites.hds_site_flag || '-'
                                            || cust_sites.legacy_customer_number;
                                    ELSIF cust_sites.legacy_site_type = 1000
                                    THEN
                                        ppartysiterec.party_site_name :=
                                            UPPER (cust_sites.hds_site_flag)
                                            || '-'
                                            || LPAD (p_mstr_counter, 2, '0');
                                        p_mstr_counter := p_mstr_counter + 1;
                                    ELSIF cust_sites.legacy_site_type BETWEEN 990
                                                                          AND  999
                                    THEN
                                        ppartysiterec.party_site_name :=
                                               'YARD'
                                            || '-'
                                            || LPAD (p_yard_counter, 2, '0');
                                        p_yard_counter := p_yard_counter + 1;
                                    ELSIF cust_sites.legacy_site_type BETWEEN 001
                                                                          AND  989
                                    THEN
                                        ppartysiterec.party_site_name :=
                                            cust_sites.hds_site_flag || '-'
                                            || cust_sites.legacy_customer_number;
                                    END IF;


                                    osprofile_id := NULL;
                                    --ppartysiterec.orig_system_reference :=    prefix||cust_sites.location || '-' ||prefix||cust_acc.account_name; --c1.reference ;
                                    ppartysiterec.orig_system_reference :=
                                        prefix
                                        || cust_sites.orig_system_reference;
                                    --ppartysiterec.orig_system_reference :=    prefix||cust_sites.location||'-'||nvl(rtrim(cust_sites.address1),cust_sites.legacy_customer_number);
                                     ppartysiterec.created_by_module :=
                                        'TCA_V1_API';
                                    whereami := 320;
                                    fnd_file.put_line (
                                        fnd_file.LOG,
                                        ' Before Calling Create Party SIte API ');
                                    hz_party_site_v2pub.create_party_site (
                                        p_init_msg_list       => 'T',
                                        p_party_site_rec      => ppartysiterec,
                                        x_party_site_id       => opartysiteid,
                                        x_party_site_number   => opartysiteno,
                                        x_return_status       => ostatus,
                                        x_msg_count           => omsgcount,
                                        x_msg_data            => omsgdata);

                                    --fnd_file.put_line (fnd_file.output,'CUSTOMER PARTY SITE CREATED FOR :- ' ||   cust_sites.address1 );
                                    IF ostatus <> 'S'     THEN
                                        whereami := 330;

                                        --fnd_file.put_line(fnd_file.log,'1116- Create Party Site API Errored Out'||omsgcount);
                                        IF omsgcount > 1 THEN
                                            FOR i IN 1 .. omsgcount
                                            LOOP
                                                p_site_err :=
                                                    p_site_err
                                                    || SUBSTR (
                                                           fnd_msg_pub.get (
                                                               i,
                                                               p_encoded   => fnd_api.g_false),
                                                           1,
                                                           255);
                                            END LOOP;

                                            -- Satish U: Moving Outside the Loop
                                            fnd_file.put_line (
                                                fnd_file.LOG,
                                                ' CUSTOMER PARTY SITE RECORD '
                                                || SUBSTR (p_site_err,
                                                           1,
                                                           255));
                                        ELSE
                                            whereami := 340;
                                            fnd_file.put_line (
                                                fnd_file.LOG,
                                                ' CUSTOMER  PARTY SITE RECORD '
                                                || omsgdata);
                                            p_site_err := omsgdata;
                                        END IF;

                                        p_rid := cust_sites.rid;
                                        RAISE e_validation_exception;
                                    ELSE
                                        -- Satish U: Create Delivery Contact 23-FEB-2012 
                                       
                                        whereami := 350;
                                        --initialize the values
                                        ostatus := NULL;
                                        omsgcount := NULL;
                                        omsgdata := NULL;
                                        ocustacctsiteid := NULL;
                                        -- create the customer account site now
                                        -- get cust account levelterriorities
                                      

                                        --fnd_file.put_line (fnd_file.output,' in here 1 '||omsgdata);
                                        pcustacctsiterec.cust_account_id :=
                                            ocustaccountid;
                                        pcustacctsiterec.party_site_id :=
                                            opartysiteid;
                                        pcustacctsiterec.created_by_module :=
                                            'TCA_V1_API';
                                        --pcustacctsiterec.orig_system_reference :=prefix||cust_sites.location || '-' || prefix||cust_sites.account_name;
                                        -- Satish U : 28-FEB-2012 : 
                                        -- Changed  Orig_System_Reference value to Legacy_Party_SIte_Number 
                                        pcustacctsiterec.orig_system_reference :=
                                            prefix
                                            || cust_sites.legacy_Party_Site_number;
                                        /**********************************
                                        pcustacctsiterec.orig_system_reference :=
                                            prefix
                                            || cust_sites.legacy_customer_number;
                                        *****************************/
                                        pcustacctsiterec.org_id := g_org_id; 
                                        /*DFF Name ========
                                        Address Information (Account Site level)*/

                                        --Notice to Owner
                                        pcustacctsiterec.attribute_category :=
                                            cust_sites.attribute4;    --'Yes';

                                        pcustacctsiterec.attribute1 := cust_sites.attribute16;
                                        
                                    
                                        --pcustacctsiterec.attribute2:='Mandatory Site Level Note'; --Mandatory Site Level Note

                                        IF UPPER (cust_sites.attribute15) =
                                               'YES'
                                        THEN
                                            pcustacctsiterec.attribute3 := 'Y';                                                                                                                                                                                                                                                                                                                         --Y N NULL Mandatory PO Number
                                        ELSIF UPPER (cust_sites.attribute15) =
                                                  'NO'
                                        THEN
                                            pcustacctsiterec.attribute3 := 'N';
                                        END IF;

                                        IF cust_sites.attribute5 IS NOT NULL
                                        THEN
                                            pcustacctsiterec.attribute5 :=
                                                TO_CHAR (
                                                    TO_DATE (
                                                        cust_sites.attribute5,
                                                        'YYYYMMDD'),
                                                    'YYYY/MM/DD HH24:MI:SS');
                                        --pcustacctsiterec.attribute5:=null; --cust_sites.attribute5; --to_char(sysdate,'YYYY/MM/DD HH24:MI:SS');  --Lien Release Date
                                        END IF;
                                        
                                        whereami := 360;
                                        -- Added  Attribute17 PRISM Customer#  Satish U : 02/21/2012 
                                        pcustacctsiterec.attribute17 := cust_sites.Legacy_Party_Site_Number ;
                                        --Mandatory Notes
                                        pcustacctsiterec.attribute18 :=
                                            cust_sites.attribute18; --Notice to Owner-Job Total
                                        pcustacctsiterec.attribute19 :=
                                            cust_sites.attribute19; --'Notice to Owner-Prelim Notice';  --Notice to Owner-Prelim Notice

                                        IF cust_sites.attribute20 IS NOT NULL
                                        THEN
                                            pcustacctsiterec.attribute20 :=
                                                TO_CHAR (
                                                    TO_DATE (
                                                        cust_sites.attribute20,
                                                        'YYYYMMDD'),
                                                    'YYYY/MM/DD HH24:MI:SS');
                                        --pcustacctsiterec.attribute20:=null; --cust_sites.attribute20;--to_char(sysdate,'YYYY/MM/DD HH24:MI:SS');--Notice to Owner-Prelim Date
                                        END IF;
                                       
                                        whereami := 370;
                                        fnd_file.put_line (
                                            fnd_file.LOG,
                                            ' Before Calling Create Customer Account SIte API ');
                                        hz_cust_account_site_v2pub.create_cust_acct_site (
                                            p_init_msg_list        => 'T',
                                            p_cust_acct_site_rec   => pcustacctsiterec,
                                            x_cust_acct_site_id    => ocustacctsiteid,
                                            x_return_status        => ostatus,
                                            x_msg_count            => omsgcount,
                                            x_msg_data             => omsgdata);

                                        IF ostatus <> 'S'
                                        THEN
                                            whereami := 380;

                                            --fnd_file.put_line(fnd_file.log,'1117- Create Customer Account Site'||omsgcount);
                                            IF omsgcount > 1
                                            THEN
                                                FOR i IN 1 .. omsgcount
                                                LOOP
                                                    p_site_err :=
                                                        NVL (p_site_err, ' ')
                                                        || SUBSTR (
                                                               fnd_msg_pub.get (
                                                                   i,
                                                                   p_encoded   => fnd_api.g_false),
                                                               1,
                                                               255);
                                                END LOOP;

                                                -- Satish U: Moving it outside the Loop
                                                fnd_file.put_line (
                                                    fnd_file.LOG,
                                                    ' CUSTOMER ACCOUNT SITE RECORD '
                                                    || cust_party.account_name
                                                    || ' '
                                                    || SUBSTR (p_site_err,
                                                               1,
                                                               255));
                                            ELSE
                                                whereami := 390;
                                                fnd_file.put_line (
                                                    fnd_file.LOG,
                                                    ' CUSTOMER ACCOUNT SITE RECORD '
                                                    || cust_party.account_name
                                                    || ' '
                                                    || omsgdata);
                                                p_site_err := omsgdata;
                                            END IF;

                                            p_rid := cust_sites.rid;
                                            RAISE e_validation_exception;
                                        ELSE
                                            whereami := 400;
                                            --fnd_file.put_line (fnd_file.output,'CUSTOMER PARTY ACCOUNT SITE CREATED FOR:- ' ||   cust_sites.address1);
                                            --initialize the values
                                            pcustacctsiteuserec := NULL;
                                            ostatus := NULL;
                                            omsgcount := NULL;
                                            omsgdata := NULL;
                                            ocustacctsiteuseid := NULL;

                                            --*********create bill to site use
                                            --if cust_sites.bill_to = 'Y' then
                                            --if upper(cust_sites.site_use_type)='BILL_TO' then
                                            IF UPPER (
                                                   cust_sites.hds_site_flag) IN
                                                       ('YARD',
                                                        'SITE',
                                                        'MSTR',
                                                        'JOB',
                                                        'MISC') -- added MISC v.sankar 10/31/2011
                                            THEN
                                                pcustacctsiteuserec := NULL;
                                                pcustomerprofile := NULL; -- 01/14/09
                                                pcustacctsiteuserec.site_use_code := 'BILL_TO';

                                                --pcustacctsiteuserec.primary_flag := cust_sites.primary_bill_to_flag;
                                                IF UPPER(cust_sites.hds_site_flag) IN
                                                           ('MSTR', 'MISC') -- added MISC v.sankar 10/31/2011
                                                THEN
                                                    pcustacctsiteuserec.primary_flag :=
                                                        'Y';
                                                ELSE
                                                    pcustacctsiteuserec.primary_flag :=
                                                        'N';
                                                END IF;

                                                --order type bill to site
                                                                                               --pcustacctsiteuserec.order_type := 'ppi standard';
                                                pcustacctsiteuserec.cust_acct_site_id :=
                                                    ocustacctsiteid;

                                                --Populate Location
                                               

                                                IF cust_sites.job_name IS NOT NULL
                                                   AND cust_sites.job_number IS NOT NULL
                                                THEN
                                                    pcustacctsiteuserec.location :=
                                                        SUBSTR (
                                                            UPPER(cust_sites.job_name)
                                                            || '-'
                                                            || cust_sites.job_number,
                                                            1,
                                                            40);
                                                ELSIF cust_sites.legacy_party_site_name IS NOT NULL
                                                      AND cust_sites.legacy_customer_number IS NOT NULL
                                                THEN
                                                    pcustacctsiteuserec.location :=
                                                        SUBSTR (
                                                            UPPER(cust_sites.legacy_party_site_name)
                                                            || '-'
                                                            || UPPER(cust_sites.legacy_customer_number),
                                                            1,
                                                            40);
                                                ELSIF cust_sites.legacy_party_site_name IS NULL
                                                      AND cust_sites.legacy_customer_number IS NOT NULL
                                                THEN
                                                    --IF TO_NUMBER(SUBSTR (
                                                    --                      TRIM(cust_sites.legacy_party_site_number),
                                                    --                      -3,
                                                    --                      3)) =
                                                    --         '000'
                                                    IF cust_sites.legacy_site_type =
                                                           1000
                                                    THEN
                                                        pcustacctsiteuserec.location :=
                                                            'MSTR-'
                                                            || UPPER(cust_sites.legacy_customer_number);
                                                    --ELSIF TO_NUMBER(SUBSTR (
                                                    --                          TRIM(cust_sites.legacy_party_site_number),
                                                    --                          -3,
                                                    --                          3))
                                                    ELSIF cust_sites.legacy_site_type BETWEEN 990
                                                                                          AND  999
                                                    THEN
                                                        pcustacctsiteuserec.location :=
                                                            'YARD-'
                                                            || UPPER(cust_sites.legacy_customer_number);
                                                    --ELSIF TO_NUMBER(SUBSTR (
                                                    --                          TRIM(cust_sites.legacy_party_site_number),
                                                    --                          -3,
                                                    --                          3))
                                                    ELSIF cust_sites.legacy_site_type BETWEEN 001
                                                                                          AND  989
                                                    THEN
                                                        pcustacctsiteuserec.location :=
                                                            'JOB-'
                                                            || UPPER(cust_sites.legacy_customer_number);
                                                    END IF;
                                                ELSIF cust_sites.legacy_party_site_name IS NOT NULL
                                                      AND cust_sites.legacy_customer_number IS NULL
                                                THEN
                                                    pcustacctsiteuserec.location :=
                                                        SUBSTR (
                                                            UPPER(cust_sites.legacy_party_site_name)
                                                            || '-'
                                                            || LPAD (
                                                                   p_loc_count,
                                                                   2,
                                                                   '0'),
                                                            1,
                                                            40);
                                                ELSIF cust_sites.legacy_party_site_name IS NULL
                                                      AND cust_sites.legacy_customer_number IS NULL
                                                THEN
                                                    --IF TO_NUMBER(SUBSTR (
                                                    --                      TRIM(cust_sites.legacy_party_site_number),
                                                    --                      -3,
                                                    --                      3)) =
                                                    --         '000'
                                                    IF cust_sites.legacy_site_type =
                                                           1000
                                                    THEN
                                                        pcustacctsiteuserec.location :=
                                                            'MSTR-'
                                                            || LPAD (
                                                                   p_mstr_loc_count,
                                                                   2,
                                                                   '0');
                                                        p_mstr_loc_count :=
                                                            p_mstr_loc_count
                                                            + 1;
                                                    --ELSIF TO_NUMBER(SUBSTR (
                                                    --                          TRIM(cust_sites.legacy_party_site_number),
                                                    --                          -3,
                                                    --                          3))
                                                    ELSIF cust_sites.legacy_site_type BETWEEN 990
                                                                                          AND  999
                                                    THEN
                                                        pcustacctsiteuserec.location :=
                                                            'YARD-'
                                                            || LPAD (
                                                                   p_yard_loc_count,
                                                                   2,
                                                                   '0');
                                                    --ELSIF TO_NUMBER(SUBSTR (
                                                    --                          TRIM(cust_sites.legacy_party_site_number),
                                                    --                          -3,
                                                    --                          3))
                                                    ELSIF cust_sites.legacy_site_type BETWEEN 001
                                                                                          AND  989
                                                    THEN
                                                        pcustacctsiteuserec.location :=
                                                            'JOB-'
                                                            || LPAD (
                                                                   p_job_loc_count,
                                                                   2,
                                                                   '0');
                                                    END IF;
                                                END IF;

                                                
                                                IF UPPER(cust_sites.hds_site_flag) =
                                                       'MSTR'
                                                THEN
                                                    pcustacctsiteuserec.attribute1 :=
                                                        'MSTR';
                                                    pcustacctsiteuserec.attribute5 :=
                                                        'MSTR';
                                                ELSIF UPPER(cust_sites.hds_site_flag) =
                                                          'YARD'
                                                THEN
                                                    pcustacctsiteuserec.attribute1 :=
                                                        'YARD';
                                                    pcustacctsiteuserec.attribute5 :=
                                                        'YARD';
                                                ELSIF UPPER(cust_sites.hds_site_flag) =
                                                          'MISC'
                                                THEN -- added MISC v.sankar 10/31/2011
                                                    pcustacctsiteuserec.attribute1 :=
                                                        'MISC';
                                                    pcustacctsiteuserec.attribute5 :=
                                                        'MISC';
                                                ELSIF UPPER(cust_sites.hds_site_flag) IN
                                                              ('SITE', 'JOB')
                                                THEN
                                                    pcustacctsiteuserec.attribute1 :=
                                                        'JOB';
                                                    pcustacctsiteuserec.attribute5 :=
                                                        'JOB';
                                                END IF;

                                                whereami := 410;

                                                --thomas guide Page
                                                pcustacctsiteuserec.attribute3 := cust_sites.attribute11;

                                                pcustacctsiteuserec.created_by_module :=
                                                    'TCA_V1_API';
                                                --profile class bill to site
                                              
                                                pcustomerprofile.profile_class_id :=
                                                    l_profile_id; --setting at cust account level

                                                --pcustomerprofile.credit_checking:=cust_acc.credit_check;
                                                --payment terms bill to site
                                             
                                              
                                                IF UPPER(cust_sites.freight_term) =
                                                       'FREIGHT EXEMPT'
                                                THEN
                                                    pcustacctsiteuserec.freight_term :=
                                                        'EXEMPT';
                                                ELSE
                                                    pcustacctsiteuserec.freight_term :=
                                                        NULL;
                                                END IF;

                                                whereami := 420;

                                                --warehouse site
                                            
                                                -- 23-FEB-2012 
                                                --07-MAR-2012  : First Check if this is a House Account 
                                                -- Satish U: 18-MAR-2012 : Changed it to Base Table from View
                                                BEGIN 
                                                    l_sales_person_id := NULL; 
                                                    
                                                    Select rs.SALESREP_ID 
                                                    Into l_sales_person_id 
                                                    From XXWC_AR_HOUSE_ACCOUNTS_XREF ha , 
                                                         JTF_RS_SALESREPS_MO_V rs -- jtf_rs_salesreps rs -- Table Replaced by  pattabhi for TMS#  20141001-00170 
                                                    Where ha.Salesrep_ID = cust_sites.Salesrep_ID
                                                    And   To_Char(ha.EMPLID)  = rs.Salesrep_number
                                                    And   ha.Salesrep_ID is Not Null ; 
                                                EXCEPTION 
                                                   WHEN OTHERS THEN 
                                                      l_sales_person_id := NULL; 
                                                END;
                                                -- 07-MAR-2012 : IF  House Account Sales Person is not found, Check Primary Salesrep Exists 
                                                --  Changed it to Base Table jtf_rs_salesreps from View. 
                                                IF l_sales_person_id IS NULL THEN 
                                                    BEGIN
                                                        SELECT   salesrep_id
                                                        INTO   l_sales_person_id
                                                        FROM   JTF_RS_SALESREPS_MO_V  --jtf_rs_salesreps  -- Table Replaced by pattabhi for TMS#  20141001-00170 
                                                        WHERE   1 = 1
                                                        AND salesrep_number =  cust_sites.primary_salesrep_number
                                                        AND ROWNUM = 1;
                                                    EXCEPTION
                                                        WHEN NO_DATA_FOUND   THEN
                                                            l_sales_person_id := NULL ; 
                                                            whereami := 430;
                                                              
                                                            If l_Sales_Person_Id is NULL Then 
                                                               -- Get Default Sales Person 
                                                               SELECT   salesrep_id
                                                               INTO   l_sales_person_id
                                                               FROM   apps.ra_salesreps
                                                               WHERE   name = 'No Sales Credit'
                                                               AND ROWNUM =    1;
                                                            End If; 
                                                    End;
                                                End IF; 

                                                        
                                                
                                                  

                                                pcustacctsiteuserec.primary_salesrep_id :=  l_sales_person_id;

                                                --added by v.sankar on 11/22/2011
                                                pcustacctsiteuserec.gsa_indicator :=   cust_sites.gsa_indicator;

                                                -- bill to terriorities
                                             
                                             

                                                BEGIN
                                                    SELECT   oracle_code
                                                      INTO   pcustomerprofile.attribute2 --Invoice Remit To Address Code
                                                      FROM   xxwc_remit_to_addr_xref x
                                                     WHERE   x.prism_code =
                                                                 cust_sites.attribute2;
                                                EXCEPTION
                                                    WHEN NO_DATA_FOUND
                                                    THEN
                                                        pcustomerprofile.attribute2 :=
                                                            2;
                                                END;

                                                pcustomerprofile.collector_id :=
                                                    p_collector_id; --p_collector_id value is setting at account level
                                                pcustomerprofile.credit_analyst_id :=
                                                    p_credit_analyst_id; --p_credit_analyst_id value is setting at account level
                                                pcustomerprofile.credit_classification :=
                                                    p_credit_classification; --p_credit_classification alue is setting at account level
                                                pcustomerprofile.standard_terms :=
                                                    p_payment_term_id; --Set at account level

                                                IF UPPER(cust_sites.hds_site_flag) IN
                                                           ('MSTR', 'MISC')
                                                THEN
                                                    IF cust_acc.attribute3 IN
                                                               ('Y', 'N', 'C')
                                                       AND cust_acc.send_statements IN
                                                                  ('Y', 'C')
                                                    THEN
                                                        IF cust_acc.attribute3 =
                                                               'C' --IN ('Y', 'N')
                                                        THEN
                                                            pcustomerprofile.attribute3 :=
                                                                'Y';
                                                        --cust_acc.attribute3;                                                                                                                                                                   --Y N null Statement by Job
                                                        ELSE
                                                            pcustomerprofile.attribute3 :=
                                                                'N';
                                                        END IF;
                                                    END IF;

                                                    --defaulted from account level
                                                    IF cust_acc.send_statements IN
                                                               ('Y', 'N', 'C')
                                                    THEN
                                                        IF cust_acc.send_statements IN
                                                                   ('Y', 'C')
                                                        THEN
                                                            pcustomerprofile.send_statements :=
                                                                'Y';
                                                        ELSE
                                                            pcustomerprofile.send_statements :=
                                                                'N';
                                                        END IF;
                                                    ELSE
                                                        pcustomerprofile.send_statements :=
                                                            'N';
                                                    END IF;

                                                    IF cust_acc.send_statements NOT IN
                                                               ('Y', 'N', 'C')
                                                    THEN
                                                        pcustomerprofile.credit_balance_statements :=
                                                            'N';
                                                    ELSIF cust_acc.send_statements IN
                                                                  ('Y',
                                                                   'C',
                                                                   'N')
                                                    THEN
                                                        IF cust_acc.credit_balance_statements IN
                                                                   ('Y', 'C')
                                                        THEN
                                                            pcustomerprofile.credit_balance_statements :=
                                                                'Y';
                                                        ELSIF cust_acc.credit_balance_statements =
                                                                  'N'
                                                        THEN
                                                            pcustomerprofile.credit_balance_statements :=
                                                                'N';
                                                        END IF;
                                                    END IF;
                                                END IF;

                                                pcustomerprofile.credit_hold :=
                                                    cust_sites.credit_hold; --defaulted from account leve profile

                                                 --account status . added by v.sankar on 12/14/2011
                                                BEGIN
                                                    whereami := 429;

                                                    SELECT   lookup_code
                                                    INTO   pcustomerprofile.account_status
                                                    FROM   xxwc_account_status_xref a, ar_lookups b
                                                    WHERE   a.account_status = b.meaning
                                                    AND b.lookup_type = 'ACCOUNT_STATUS'
                                                    AND a.prism_credit_group =  cust_sites.credit_analyst_name;
                                                EXCEPTION
                                                    WHEN NO_DATA_FOUND
                                                    THEN
                                                        pcustomerprofile.account_status := NULL;
                                                END;

                                                whereami := 430;
                                                fnd_file.put_line (
                                                    fnd_file.LOG,
                                                    ' Before Calling Create Customer Site USe API ');
                                                hz_cust_account_site_v2pub.create_cust_site_use (
                                                    p_init_msg_list          => 'T',
                                                    p_cust_site_use_rec      => pcustacctsiteuserec,
                                                    p_customer_profile_rec   => pcustomerprofile,
                                                    p_create_profile         => 'T',
                                                    p_create_profile_amt     => 'T',
                                                    x_site_use_id            => ocustacctsiteuseid,
                                                    x_return_status          => ostatus,
                                                    x_msg_count              => omsgcount,
                                                    x_msg_data               => omsgdata);

                                                IF UPPER(cust_sites.hds_site_flag) IN
                                                           ('YARD',
                                                            'MSTR',
                                                            'MISC',
                                                            'JOB',
                                                            'SITE')
                                                THEN
                                                    pbilltositeuseid :=  ocustacctsiteuseid;
                                                END IF;

                                                pcustacctid := ocustaccountid;

                                                IF ostatus <> 'S'
                                                THEN
                                                    whereami := 440;
                                                    l_proceed := 'N';

                                                    --fnd_file.put_line ( fnd_file.LOG  , ' 1120-Create SIte USe TYpe API errored out: Msg Count' || omsgcount);
                                                    IF omsgcount > 1
                                                    THEN
                                                        FOR i IN 1 .. omsgcount
                                                        LOOP
                                                            p_site_err :=
                                                                p_site_err
                                                                || SUBSTR (
                                                                       fnd_msg_pub.get (
                                                                           p_encoded => fnd_api.g_false),
                                                                       1,
                                                                       255);
                                                        END LOOP;

                                                        -- Satish U: 11/01/11 Moving Outside the Loop
                                                        fnd_file.put_line (
                                                            fnd_file.LOG,
                                                            'CUSTOMER BILL_TO SITE USE RECORD '
                                                            || SUBSTR (
                                                                   p_site_err,
                                                                   1,
                                                                   255));
                                                    ELSE
                                                        whereami := 450;
                                                        fnd_file.put_line (
                                                            fnd_file.LOG,
                                                            'CUSTOMER BILL_TO SITE USE RECORD '
                                                            || omsgdata);
                                                        p_site_err := omsgdata;
                                                    END IF;

                                                    p_rid := cust_sites.rid;
                                                    RAISE e_validation_exception;
                                                ELSE
                                                    whereami := 450;

                                                    IF NVL (
                                                           cust_sites.trx_credit_limit,
                                                           0) != 0
                                                       OR NVL (
                                                             cust_sites.overall_credit_limit,
                                                             0) != 0
                                                    THEN
                                                        pcustproamtrec := NULL;

                                                        BEGIN
                                                            SELECT   cust_account_profile_id
                                                              INTO   osprofile_id
                                                              FROM   hz_customer_profiles
                                                             WHERE   cust_account_id =
                                                                         ocustaccountid
                                                                     AND site_use_id =
                                                                            ocustacctsiteuseid;
                                                        EXCEPTION
                                                            WHEN NO_DATA_FOUND
                                                            THEN
                                                                whereami :=
                                                                    460;
                                                                osprofile_id :=
                                                                    NULL;
                                                                fnd_file.put_line (
                                                                    fnd_file.LOG,
                                                                    'No Customer Profile Found for site use id '
                                                                    || ocustacctsiteuseid);
                                                        END;

                                                        IF osprofile_id IS NOT NULL
                                                        THEN
                                                            BEGIN
                                                                whereami :=
                                                                    470;

                                                                SELECT   cust_acct_profile_amt_id,
                                                                         object_version_number
                                                                  INTO   ocustacctprofileamtid,
                                                                         p_object_version_number
                                                                  FROM   hz_cust_profile_amts
                                                                 WHERE   cust_account_profile_id =
                                                                             osprofile_id
                                                                         AND currency_code =
                                                                                'USD';
                                                            EXCEPTION
                                                                WHEN NO_DATA_FOUND
                                                                THEN
                                                                    ocustacctprofileamtid :=
                                                                        NULL;
                                                                    p_object_version_number :=
                                                                        NULL;
                                                            END;

                                                            IF ocustacctprofileamtid IS NOT NULL
                                                            THEN
                                                                whereami :=
                                                                    480;
                                                                --Update amount profile
                                                                pcustproamtrec.cust_acct_profile_amt_id :=
                                                                    ocustacctprofileamtid;
                                                                pcustproamtrec.trx_credit_limit :=
                                                                    cust_sites.trx_credit_limit;
                                                                pcustproamtrec.overall_credit_limit :=
                                                                    cust_sites.overall_credit_limit;
                                                                pcustproamtrec.created_by_module :=
                                                                    'TCA_V1_API';
                                                                fnd_file.put_line (
                                                                    fnd_file.LOG,
                                                                    ' Before Calling Update Profile Amt API ');
                                                                hz_customer_profile_v2pub.update_cust_profile_amt (
                                                                    p_init_msg_list           => 'T',
                                                                    p_cust_profile_amt_rec    => pcustproamtrec,
                                                                    p_object_version_number   => p_object_version_number,
                                                                    x_return_status           => ostatus,
                                                                    x_msg_count               => omsgcount,
                                                                    x_msg_data                => omsgdata);

                                                                IF ostatus <>
                                                                       'S'
                                                                THEN
                                                                    whereami :=
                                                                        490;

                                                                    --fnd_file.put_line ( fnd_file.LOG  , ' 1121 -Update Profile Amt API Errored Out : Msg Count' || omsgcount);
                                                                    IF omsgcount >
                                                                           1
                                                                    THEN
                                                                        FOR i IN 1 .. omsgcount
                                                                        LOOP
                                                                            p_site_err :=
                                                                                NVL (
                                                                                    p_site_err,
                                                                                    ' ')
                                                                                || SUBSTR (
                                                                                       fnd_msg_pub.get (
                                                                                           i,
                                                                                           p_encoded   => fnd_api.g_false),
                                                                                       1,
                                                                                       255);
                                                                        END LOOP;
                                                                        -- Satish U: 11/01/11 Moving Code Outside the Loop
                                                                        fnd_file.put_line (
                                                                            fnd_file.LOG,
                                                                            ' CUSTOMER Site Amout Profile Updation '
                                                                            || SUBSTR (
                                                                                   p_site_err,
                                                                                   1,
                                                                                   255));
                                                                    ELSE
                                                                        fnd_file.put_line (
                                                                            fnd_file.LOG,
                                                                            ' CUSTOMER Site Amount Profile Updation '
                                                                            || omsgdata);
                                                                        p_site_err :=
                                                                            omsgdata;
                                                                    END IF;

                                                                    p_rid :=
                                                                        cust_sites.rid;
                                                                    RAISE e_validation_exception;
                                                                END IF;
                                                            ELSE
                                                                whereami :=
                                                                    500;
                                                                --Create Profile Amount
                                                                pcustproamtrec.cust_account_profile_id :=
                                                                    osprofile_id;
                                                                pcustproamtrec.cust_account_id :=
                                                                    ocustaccountid;
                                                                pcustproamtrec.site_use_id :=
                                                                    ocustacctsiteuseid;
                                                                pcustproamtrec.currency_code :=
                                                                    'USD';
                                                                pcustproamtrec.trx_credit_limit :=
                                                                    cust_sites.trx_credit_limit;
                                                                pcustproamtrec.overall_credit_limit :=
                                                                    cust_sites.overall_credit_limit;
                                                                pcustproamtrec.created_by_module :=
                                                                    'TCA_V1_API';
                                                                fnd_file.put_line (
                                                                    fnd_file.LOG,
                                                                    ' Before Calling Create Profile Amt API ');
                                                                hz_customer_profile_v2pub.create_cust_profile_amt (
                                                                    p_init_msg_list              => 'T',
                                                                    p_check_foreign_key          => 'T',
                                                                    p_cust_profile_amt_rec       => pcustproamtrec,
                                                                    x_cust_acct_profile_amt_id   => ocustacctprofileamtid,
                                                                    x_return_status              => ostatus,
                                                                    x_msg_count                  => omsgcount,
                                                                    x_msg_data                   => omsgdata);

                                                                IF ostatus <>
                                                                       'S'
                                                                THEN
                                                                    whereami :=
                                                                        510;

                                                                    --fnd_file.put_line ( fnd_file.LOG  , ' Create Profile Amt API Errored Out- : Msg Count' || omsgcount);
                                                                    IF omsgcount >
                                                                           1
                                                                    THEN
                                                                        FOR i IN 1 .. omsgcount
                                                                        LOOP
                                                                            p_site_err :=
                                                                                NVL (
                                                                                    p_site_err,
                                                                                    ' ')
                                                                                || SUBSTR (
                                                                                       fnd_msg_pub.get (
                                                                                           i,
                                                                                           p_encoded   => fnd_api.g_false),
                                                                                       1,
                                                                                       255);
                                                                        END LOOP;
                                                                        -- SatishU: 11/02/11 : MOved the code out from LOOP
                                                                        fnd_file.put_line (
                                                                            fnd_file.LOG,
                                                                            ' CUSTOMER Site Use Level Amout Profile Creation '
                                                                            || SUBSTR (
                                                                                   p_site_err,
                                                                                   1,
                                                                                   255));
                                                                    ELSE
                                                                        fnd_file.put_line (
                                                                            fnd_file.LOG,
                                                                            ' CUSTOMER Site Use Level Amount Profile Creation '
                                                                            || omsgdata);
                                                                        p_site_err :=
                                                                            omsgdata;
                                                                    END IF;

                                                                    p_rid :=
                                                                        cust_sites.rid;
                                                                    RAISE e_validation_exception;
                                                                END IF;
                                                            END IF;
                                                        END IF;
                                                    END IF; --site amount profile
                                                END IF;

                                                --fnd_file.put_line (fnd_file.output,'CUSTOMER BILL TO SITE CREATED FOR THE SITE :- ' || cust_party.account_name);
                                                --fnd_file.put_line (fnd_file.output,'****************************');
                                                l_proceed := 'Y';
                                            --
                                         

                                            END IF;            -- bill to site

                                            whereami := 520;

                                            --*********create ship to site use
                                            IF UPPER (
                                                   cust_sites.hds_site_flag) IN
                                                       ('YARD',
                                                        'SITE',
                                                        'JOB',
                                                        'MISC') -- added MISC v.sankar 10/31/2011
                                            THEN
                                                --if upper(cust_sites.site_use_type) = 'SHIP_TO' then
                                                pcustacctsiteuserec := NULL;
                                                pcustomerprofile := NULL; -- 01/14/09
                                                pcustacctsiteuserec.site_use_code := 'SHIP_TO';
                                                --get bill to site use id for ship to site
                                                pcustacctsiteuserec.bill_to_site_use_id :=  pbilltositeuseid;

                                                IF UPPER(cust_sites.hds_site_flag) IN
                                                           ('YARD', 'MISC') -- added MISC v.sankar 10/31/2011
                                                   AND lv_primary_ship_to_flag IS NULL
                                                THEN
                                                    pcustacctsiteuserec.primary_flag :=
                                                        'Y';
                                                    lv_primary_ship_to_flag :=
                                                        'Y';
                                                ELSE
                                                    pcustacctsiteuserec.primary_flag :=
                                                        'N';
                                                END IF;                                               

                                                --added by v.sankar on 11/18/2011 to create a new party and cust acct site for MISC customers
                                                IF cust_sites.hds_site_flag =
                                                       'MISC'
                                                THEN
                                                    ostatus := NULL;
                                                    omsgcount := NULL;
                                                    omsgdata := NULL;
                                                    ln_partysiteid_misc_bill_to := opartysiteid;
                                                    opartysiteid := NULL;
                                                    opartysiteno := NULL;
                                                    -- create a party site now
                                                    ppartysiterec.party_id    :=   opartyid;
                                                    ppartysiterec.location_id :=   olocationid;

                                                    --v.sankar 11/22/2011. needs to be fixed next round.
                                                    ppartysiterec.party_site_number :=
                                                        SUBSTR (
                                                            cust_sites.legacy_party_site_number,
                                                            1,
                                                            23)
                                                        || '-S';
                                                    ppartysiterec.party_site_name :=
                                                        cust_sites.hds_site_flag
                                                        || '-S-'
                                                        || cust_sites.legacy_customer_number;
                                                    osprofile_id := NULL;
                                                    --ppartysiterec.orig_system_reference :=    prefix||cust_sites.location || '-' ||prefix||cust_acc.account_name; --c1.reference ;
                                                    ppartysiterec.orig_system_reference :=
                                                        prefix
                                                        || cust_sites.orig_system_reference;


                                                    ppartysiterec.created_by_module :=
                                                        'TCA_V1_API';
                                                    whereami := 521;
                                                    fnd_file.put_line (
                                                        fnd_file.LOG,
                                                        ' Before Calling Create Party SIte API for MISC Ship To');
                                                    hz_party_site_v2pub.create_party_site (
                                                        p_init_msg_list       => 'T',
                                                        p_party_site_rec      => ppartysiterec,
                                                        x_party_site_id       => opartysiteid,
                                                        x_party_site_number   => opartysiteno,
                                                        x_return_status       => ostatus,
                                                        x_msg_count           => omsgcount,
                                                        x_msg_data            => omsgdata);

                                                    --fnd_file.put_line (fnd_file.output,'CUSTOMER PARTY SITE CREATED FOR :- ' ||   cust_sites.address1 );
                                                    IF ostatus <> 'S'
                                                    THEN
                                                        whereami := 522;

                                                        --fnd_file.put_line(fnd_file.log,'1116- Create Party Site API Errored Out'||omsgcount);
                                                        IF omsgcount > 1
                                                        THEN
                                                            FOR i IN 1 .. omsgcount
                                                            LOOP
                                                                p_site_err :=
                                                                    p_site_err
                                                                    || SUBSTR (
                                                                           fnd_msg_pub.get (
                                                                               i,
                                                                               p_encoded   => fnd_api.g_false),
                                                                           1,
                                                                           255);
                                                            END LOOP;

                                                            -- Satish U: Moving Outside the Loop
                                                            fnd_file.put_line (
                                                                fnd_file.LOG,
                                                                ' CUSTOMER PARTY SITE RECORD FOR MISC SHIP-TO '
                                                                || SUBSTR (
                                                                       p_site_err,
                                                                       1,
                                                                       255));
                                                        ELSE
                                                            whereami := 523;
                                                            fnd_file.put_line (
                                                                fnd_file.LOG,
                                                                ' CUSTOMER  PARTY SITE RECORD FOR MISC SHIP-TO'
                                                                || omsgdata);
                                                            p_site_err :=
                                                                omsgdata;
                                                        END IF;

                                                        p_rid :=
                                                            cust_sites.rid;
                                                        RAISE e_validation_exception;
                                                    ELSE
                                                     
                                                       whereami := 524;
                                                        --initialize the values
                                                        ostatus := NULL;
                                                        omsgcount := NULL;
                                                        omsgdata := NULL;
                                                        ocustacctsiteid :=
                                                            NULL;
                                                        pcustacctsiterec.cust_account_id :=
                                                            ocustaccountid;
                                                        pcustacctsiterec.party_site_id :=
                                                            opartysiteid;
                                                        pcustacctsiterec.created_by_module :=
                                                            'TCA_V1_API';
                                                            
                                                        /**************Satish U: 28-FEB-2012 
                                                        pcustacctsiterec.orig_system_reference :=
                                                            prefix || 'S-'
                                                            || cust_sites.legacy_customer_number;
                                                        ***************************/
                                                        pcustacctsiterec.orig_system_reference := prefix || 'S-'    || cust_sites.legacy_Party_Site_number;
                                                        pcustacctsiterec.org_id :=  g_org_id;
                                                        --cust_sites.location||'-'||opartysiteid||nvl(rtrim(cust_sites.address1),cust_sites.legacy_customer_number);

                                                        /*DFF Name ========
                                                            Address Information (Account Site level)*/

                                                        --Notice to Owner
                                                        pcustacctsiterec.attribute_category :=
                                                            cust_sites.attribute4;       

                                                       pcustacctsiterec.attribute1 := cust_sites.attribute16;                                                                                                                                                             --'Yes';
                                                    
                                                        --pcustacctsiterec.attribute2:='Mandatory Site Level Note'; --Mandatory Site Level Note

                                                        IF UPPER(cust_sites.attribute15) =
                                                               'YES'
                                                        THEN
                                                            pcustacctsiterec.attribute3 :=
                                                                'Y'; --Y N NULL Mandatory PO Number
                                                        ELSIF UPPER(cust_sites.attribute15) =
                                                                  'NO'
                                                        THEN
                                                            pcustacctsiterec.attribute3 :=
                                                                'N';
                                                        END IF;

                                                        IF cust_sites.attribute5 IS NOT NULL
                                                        THEN
                                                            pcustacctsiterec.attribute5 :=
                                                                TO_CHAR (
                                                                    TO_DATE (
                                                                        cust_sites.attribute5,
                                                                        'YYYYMMDD'),
                                                                    'YYYY/MM/DD HH24:MI:SS');
                                                        --pcustacctsiterec.attribute5:=null; --cust_sites.attribute5; --to_char(sysdate,'YYYY/MM/DD HH24:MI:SS');  --Lien Release Date
                                                        END IF;

                                                        whereami := 525;
                                                        --Mandatory Notes
                                                        pcustacctsiterec.attribute18 :=
                                                            cust_sites.attribute18;                                                                                                                                                                  --Notice to Owner-Job Total
                                                        pcustacctsiterec.attribute19 :=
                                                            cust_sites.attribute19;                                                                                                                                                                  --'Notice to Owner-Prelim Notice';  --Notice to Owner-Prelim Notice

                                                        IF cust_sites.attribute20 IS NOT NULL
                                                        THEN
                                                            pcustacctsiterec.attribute20 :=
                                                                TO_CHAR (
                                                                    TO_DATE (
                                                                        cust_sites.attribute20,
                                                                        'YYYYMMDD'),
                                                                    'YYYY/MM/DD HH24:MI:SS');
                                                        --pcustacctsiterec.attribute20:=null; --cust_sites.attribute20;--to_char(sysdate,'YYYY/MM/DD HH24:MI:SS');--Notice to Owner-Prelim Date
                                                        END IF;

                                                        whereami := 526;
                                                        fnd_file.put_line (
                                                            fnd_file.LOG,
                                                            ' Before Calling Create Customer Account Site API for MISC Ship to');
                                                        hz_cust_account_site_v2pub.create_cust_acct_site (
                                                            p_init_msg_list        => 'T',
                                                            p_cust_acct_site_rec   => pcustacctsiterec,
                                                            x_cust_acct_site_id    => ocustacctsiteid,
                                                            x_return_status        => ostatus,
                                                            x_msg_count            => omsgcount,
                                                            x_msg_data             => omsgdata);

                                                        IF ostatus <> 'S'
                                                        THEN
                                                            whereami := 527;

                                                            IF omsgcount > 1
                                                            THEN
                                                                FOR i IN 1 .. omsgcount
                                                                LOOP
                                                                    p_site_err :=
                                                                        NVL (
                                                                            p_site_err,
                                                                            ' ')
                                                                        || SUBSTR (
                                                                               fnd_msg_pub.get (
                                                                                   i,
                                                                                   p_encoded   => fnd_api.g_false),
                                                                               1,
                                                                               255);
                                                                END LOOP;

                                                                -- Satish U: Moving it outside the Loop
                                                                fnd_file.put_line (
                                                                    fnd_file.LOG,
                                                                    ' CUSTOMER ACCOUNT SITE RECORD '
                                                                    || cust_party.account_name
                                                                    || ' '
                                                                    || SUBSTR (
                                                                           p_site_err,
                                                                           1,
                                                                           255));
                                                            ELSE
                                                                whereami :=
                                                                    528;
                                                                fnd_file.put_line (
                                                                    fnd_file.LOG,
                                                                    ' CUSTOMER ACCOUNT SITE RECORD '
                                                                    || cust_party.account_name
                                                                    || ' '
                                                                    || omsgdata);
                                                                p_site_err :=
                                                                    omsgdata;
                                                            END IF;

                                                            p_rid :=
                                                                cust_sites.rid;
                                                            RAISE e_validation_exception;
                                                        END IF;
                                                    END IF;
                                                END IF;

                                                --end added by v.sankar on 11/18/2011 to create a new site for MISC customers.

                                                pcustacctsiteuserec.cust_acct_site_id :=
                                                    ocustacctsiteid;

                                                --populate location
                                                IF cust_sites.job_name IS NOT NULL
                                                   AND cust_sites.job_number IS NOT NULL
                                                THEN
                                                    pcustacctsiteuserec.location :=
                                                        SUBSTR (
                                                            UPPER(cust_sites.job_name)
                                                            || '-'
                                                            || cust_sites.job_number,
                                                            1,
                                                            40);
                                                ELSIF cust_sites.legacy_party_site_name IS NOT NULL
                                                      AND cust_sites.legacy_customer_number IS NOT NULL
                                                THEN
                                                    pcustacctsiteuserec.location :=
                                                        SUBSTR (
                                                            UPPER(cust_sites.legacy_party_site_name)
                                                            || '-'
                                                            || UPPER(cust_sites.legacy_customer_number),
                                                            1,
                                                            40);
                                                ELSIF cust_sites.legacy_party_site_name IS NULL
                                                      AND cust_sites.legacy_customer_number IS NOT NULL
                                                THEN
                                                    IF cust_sites.legacy_site_type =
                                                           1000
                                                    THEN
                                                        pcustacctsiteuserec.location :=
                                                            'MSTR-'
                                                            || UPPER(cust_sites.legacy_customer_number);
                                                    ELSIF cust_sites.legacy_site_type BETWEEN 990
                                                                                          AND  999
                                                    THEN
                                                        pcustacctsiteuserec.location :=
                                                            'YARD-'
                                                            || UPPER(cust_sites.legacy_customer_number);
                                                    ELSIF cust_sites.legacy_site_type BETWEEN 001
                                                                                          AND  989
                                                    THEN
                                                        pcustacctsiteuserec.location :=
                                                            'JOB-'
                                                            || UPPER(cust_sites.legacy_customer_number);
                                                    END IF;
                                                ELSIF cust_sites.legacy_party_site_name IS NOT NULL
                                                      AND cust_sites.legacy_customer_number IS NULL
                                                THEN
                                                    pcustacctsiteuserec.location :=
                                                        SUBSTR (
                                                            UPPER(cust_sites.legacy_party_site_name)
                                                            || '-'
                                                            || LPAD (
                                                                   p_loc_count,
                                                                   2,
                                                                   '0'),
                                                            1,
                                                            40);
                                                    p_loc_count :=
                                                        p_loc_count + 1;
                                                ELSIF cust_sites.legacy_party_site_name IS NULL
                                                      AND cust_sites.legacy_customer_number IS NULL
                                                THEN
                                                    IF cust_sites.legacy_site_type =
                                                           1000
                                                    THEN
                                                        pcustacctsiteuserec.location :=
                                                            'MSTR-'
                                                            || LPAD (
                                                                   p_mstr_loc_count,
                                                                   2,
                                                                   '0');
                                                    ELSIF cust_sites.legacy_site_type BETWEEN 990
                                                                                          AND  999
                                                    THEN
                                                        pcustacctsiteuserec.location :=
                                                            'YARD-'
                                                            || LPAD (
                                                                   p_yard_loc_count,
                                                                   2,
                                                                   '0');
                                                        p_yard_loc_count :=
                                                            p_yard_loc_count
                                                            + 1;
                                                    ELSIF cust_sites.legacy_site_type BETWEEN 001
                                                                                          AND  989
                                                    THEN
                                                        pcustacctsiteuserec.location :=
                                                            'JOB-'
                                                            || LPAD (
                                                                   p_job_loc_count,
                                                                   2,
                                                                   '0');
                                                        p_job_loc_count :=
                                                            p_job_loc_count
                                                            + 1;
                                                    END IF;
                                                END IF;

                                                whereami := 530;

                                                --Customer Site Classification
                                                IF UPPER(cust_sites.hds_site_flag) =
                                                       'MSTR'
                                                THEN
                                                    pcustacctsiteuserec.attribute1 :=
                                                        'MSTR';
                                                    pcustacctsiteuserec.attribute5 :=
                                                        'MSTR';
                                                ELSIF UPPER(cust_sites.hds_site_flag) =
                                                          'YARD'
                                                THEN
                                                    pcustacctsiteuserec.attribute1 :=
                                                        'YARD';
                                                    pcustacctsiteuserec.attribute5 :=
                                                        'YARD';
                                                ELSIF UPPER(cust_sites.hds_site_flag) =
                                                          'MISC'
                                                THEN -- added MISC v.sankar 10/31/2011
                                                    pcustacctsiteuserec.attribute1 :=
                                                        'MISC';
                                                    pcustacctsiteuserec.attribute5 :=
                                                        'MISC';
                                                ELSIF UPPER(cust_sites.hds_site_flag) IN
                                                              ('SITE', 'JOB')
                                                THEN
                                                    pcustacctsiteuserec.attribute1 :=
                                                        'JOB';
                                                    pcustacctsiteuserec.attribute5 :=
                                                        'JOB';
                                                END IF;
    
                                                --thomas guide Page
                                                pcustacctsiteuserec.attribute3 := cust_sites.attribute11;

                                                pcustacctsiteuserec.created_by_module :=
                                                    'TCA_V1_API';
                                                --order type ship to site
                                           
                                                -- warehouse ship to site
                                                

                                                pcustacctsiteuserec.primary_salesrep_id :=  l_sales_person_id;

                                                --added by v.sankar on 11/22/2011
                                                pcustacctsiteuserec.gsa_indicator :=  cust_sites.gsa_indicator;

                                                

                                                IF UPPER(cust_sites.freight_term) =
                                                       'FREIGHT EXEMPT'
                                                THEN
                                                    pcustacctsiteuserec.freight_term :=
                                                        'EXEMPT';
                                                ELSE
                                                    pcustacctsiteuserec.freight_term :=
                                                        NULL;
                                                END IF;

                                                fnd_file.put_line (
                                                    fnd_file.LOG,
                                                    ' Before Calling Create Customer Site Use  API ');

                                                hz_cust_account_site_v2pub.create_cust_site_use (
                                                    p_init_msg_list          => 'T',
                                                    p_cust_site_use_rec      => pcustacctsiteuserec,
                                                    p_customer_profile_rec   => pcustomerprofile,
                                                    p_create_profile         => 'F',
                                                    p_create_profile_amt     => 'F',
                                                    x_site_use_id            => ocustacctsiteuseid,
                                                    x_return_status          => ostatus,
                                                    x_msg_count              => omsgcount,
                                                    x_msg_data               => omsgdata);
                                                --pbilltositeuseid := ocustacctsiteuseid ;
                                                pcustacctid := ocustaccountid;

                                                IF ostatus <> 'S'
                                                THEN
                                                    whereami := 540;
                                                    l_proceed := 'N';
                                                    fnd_file.put_line (
                                                        fnd_file.LOG,
                                                        ' Create Customer Site Use API Errored Out- Msg Count :'
                                                        || omsgcount);

                                                    IF omsgcount > 1
                                                    THEN
                                                        FOR i IN 1 .. omsgcount
                                                        LOOP
                                                            p_site_err :=
                                                                NVL (
                                                                    p_site_err,
                                                                    ' ')
                                                                || SUBSTR (
                                                                       fnd_msg_pub.get (
                                                                           i,
                                                                           p_encoded   => fnd_api.g_false),
                                                                       1,
                                                                       255);
                                                        END LOOP;

                                                        -- Satish U: 11/02/11 : Moved code out of the loop
                                                        fnd_file.put_line (
                                                            fnd_file.LOG,
                                                            'CUSTOMER SHIP_TO SITE USE RECORD '
                                                            || SUBSTR (
                                                                   p_site_err,
                                                                   1,
                                                                   255));
                                                    ELSE
                                                        whereami := 550;
                                                        fnd_file.put_line (
                                                            fnd_file.LOG,
                                                            'CUSTOMER SHIP_TO SITE USE RECORD '
                                                            || omsgdata
                                                            || '-'
                                                            || cust_party.account_name);
                                                        p_site_err := omsgdata;
                                                    END IF;

                                                    p_rid := cust_sites.rid;
                                                    RAISE e_validation_exception;
                                                END IF;


                                                l_proceed := 'Y';
                                            END IF;            -- ship to site
                                        END IF;          -- party site account

                                        --create party site level communication
                                        whereami := 550;

                                        FOR rec_site_contact
                                        IN get_site_contact (
                                               cust_sites.orig_system_reference,
                                               cust_sites.legacy_customer_number)
                                        LOOP
                                            xxpop_org_communication (
                                                'SITE',
                                                opartysiteid,
                                                rec_site_contact.phone_area_code,
                                                rec_site_contact.phone_number,
                                                rec_site_contact.fax_area_code,
                                                rec_site_contact.fax_number,
                                                p_result,
                                                omsgdata);

                                            IF p_result = 'E'
                                            THEN
                                                whereami := 560;
                                                fnd_file.put_line (
                                                    fnd_file.LOG,
                                                    'ERROR IN PARTY SITE LEVEL COMMUNICATION '
                                                    || TO_CHAR (whereami));
                                                p_con_err := omsgdata;
                                                p_rid := rec_site_contact.rid;
                                                RAISE e_validation_exception;
                                            END IF;

                                        -- contacts for MISC billto. since MISC site is created twice, we need to attach
                                        -- the same contact at bill to and ship to
                                            IF ln_partysiteid_misc_bill_to IS NOT NULL
                                            THEN
                                                xxpop_org_communication (
                                                'SITE',
                                                ln_partysiteid_misc_bill_to,
                                                rec_site_contact.phone_area_code,
                                                rec_site_contact.phone_number,
                                                rec_site_contact.fax_area_code,
                                                rec_site_contact.fax_number,
                                                p_result,
                                                omsgdata);

                                                IF p_result = 'E'
                                                THEN
                                                    whereami := 561;
                                                    fnd_file.put_line (
                                                        fnd_file.LOG,
                                                        'ERROR IN PARTY SITE LEVEL COMMUNICATION FOR MISC BILLTO'
                                                        || TO_CHAR (whereami));
                                                    p_con_err := omsgdata;
                                                    p_rid := rec_site_contact.rid;
                                                    RAISE e_validation_exception;
                                                END IF;
                                            END IF;

                                            IF cust_sites.hds_site_flag IN ('MISC', 'MSTR')
                                            THEN
                                                xxpop_org_communication (
                                                'PARTY',
                                                opartyid,
                                                rec_site_contact.phone_area_code,
                                                rec_site_contact.phone_number,
                                                rec_site_contact.fax_area_code,
                                                rec_site_contact.fax_number,
                                                p_result,
                                                omsgdata);

                                                IF p_result = 'E'
                                                THEN
                                                    whereami := 562;
                                                    fnd_file.put_line (
                                                        fnd_file.LOG,
                                                        'ERROR IN PARTY LEVEL COMMUNICATION FOR MISC OR MSTR'
                                                        || TO_CHAR (whereami));
                                                    p_con_err := omsgdata;
                                                    p_rid := rec_site_contact.rid;
                                                    RAISE e_validation_exception;
                                                END IF;
                                            END IF;

                                            UPDATE   xxwc_ar_contact_points_cnv
                                               SET   process_status = 'P'
                                             WHERE   ROWID =
                                                         rec_site_contact.rid;
                                        END LOOP;
                                    END IF;                      -- party site
                                END IF;                             --location

                                UPDATE   xxwc_ar_sites_cnv
                                   SET   process_status = 'P'
                                 WHERE   ROWID = cust_sites.rid;
                            END LOOP;                                  --Sites
                            

                            UPDATE   xxwc_ar_customers_cnv
                               SET   process_status = 'P'
                             WHERE   ROWID = cust_acc.rid;
                        END IF;                            -- for the account                        
                    END LOOP;                                        --Account

                    --create party level communication
                    whereami := 569;

                    FOR rec_contact
                    IN get_contact (cust_party.orig_system_reference)
                    LOOP
                        xxpop_org_communication ('PARTY',
                                                 opartyid,
                                                 rec_contact.phone_area_code,
                                                 rec_contact.phone_number,
                                                 rec_contact.fax_area_code,
                                                 rec_contact.fax_number,
                                                 p_result,
                                                 omsgdata);

                        IF p_result = 'E'
                        THEN
                            whereami := 570;
                            fnd_file.put_line (
                                fnd_file.LOG,
                                'ERROR IN PARTY LEVEL COMMUNICATION '
                                || TO_CHAR (whereami));
                            p_con_err := omsgdata;
                            p_rid := rec_contact.rid;
                            RAISE e_validation_exception;
                        END IF;

                        UPDATE   xxwc_ar_contact_points_cnv
                           SET   process_status = 'P'
                         WHERE   ROWID = rec_contact.rid;
                    END LOOP;
                END IF;                                  -- for the party

                --Inactivate the party
                --commented on 12/14/2011
                
               IF cust_party.cod_comment IS NOT NULL OR
                 cust_party.keyword IS NOT NULL
               THEN
                          xxpop_org_web_contact ('PARTY',
                                                 opartyid,
                                                 cust_party.cod_comment,
                                                 cust_party.keyword,
                                                 p_result,
                                                 omsgdata);

                        IF p_result = 'E'
                        THEN
                            whereami := 580;
                            fnd_file.put_line (
                                fnd_file.LOG,
                                'ERROR IN PARTY LEVEL WEB CONTACT'
                                || TO_CHAR (whereami));
                            p_con_err := omsgdata;
                            p_rid := cust_party.rid;
                            RAISE e_validation_exception;
                        END IF;
               END IF;

                UPDATE   xxwc_ar_customers_cnv
                   SET   process_status = 'P'
                 WHERE   orig_system_reference =
                             cust_party.orig_system_reference;
            EXCEPTION
                WHEN e_validation_exception
                THEN
                    --whereami    := 590;

                    v_err := 'Y';
                    ROLLBACK TO customer_record;
                    upd_stagin_table_err (p_cus_err,
                                          p_site_err,
                                          p_con_err,
                                          p_rid);
                    p_cus_err := NULL;
                    p_site_err := NULL;
                    p_con_err := NULL;
                    upd_stagin_table (cust_party.orig_system_reference);
                    fnd_file.put_line (
                        fnd_file.LOG,
                        'e_Validation_Exception IN PROCEDURE for customer: '
                        || cust_party.orig_system_reference
                        || '---'
                        || l_api_name
                        || '---'
                        || TO_CHAR (whereami));
                WHEN OTHERS
                THEN
                    -- whereami  := 600;
                    ROLLBACK TO customer_record;
                    v_err := 'Y';
                    --SatishU: 11/01/11  Added Following SQL Statement
                    upd_stagin_table_err (p_cus_err,
                                          p_site_err,
                                          p_con_err,
                                          p_rid);
                    p_cus_err := NULL;
                    p_site_err := NULL;
                    p_con_err := NULL;
                    upd_stagin_table (cust_party.orig_system_reference);
                    fnd_file.put_line (
                        fnd_file.LOG,
                           'When Others Error in PROCEDURE for customer: '
                        || cust_party.orig_system_reference
                        || '---'
                        || l_api_name
                        || SQLERRM
                        || '---'
                        || TO_CHAR (whereami));
            END;

            IF l_rec_count >= c_max_commit_count
            THEN
                COMMIT;                           --Commit the single customer
                l_rec_count := 0;
            END IF;
        END LOOP;

        COMMIT; -- COmmit after the program completes.          --close get_party;
        get_cust_conversion_stats;

        IF v_err = 'Y'
        THEN
            req_status :=
                fnd_concurrent.set_completion_status ('WARNING', NULL);
        END IF;
    END xxwc_customer_conv;
    
    --- **************************************************************************************
    -- Procedure xxpop_contact_info :  Currently this API is not used.  
    
    PROCEDURE xxpop_contact_info (p_site_use_type       IN     VARCHAR2,
                                  p_party_id            IN     NUMBER,
                                  p_cust_acct_id        IN     NUMBER,
                                  p_cust_acct_site_id   IN     NUMBER,
                                  p_rowid               IN     ROWID,
                                  p_out                    OUT VARCHAR2)
    IS
        -- party person recoprd variables
        ppersonrec               hz_party_v2pub.person_rec_type;
        oppartyid                hz_parties.party_id%TYPE;
        oppartynumber            hz_parties.party_number%TYPE;
        opprofileid              NUMBER;
        --party site contact
        porgcontactrec           hz_party_contact_v2pub.org_contact_rec_type;
        oocpartyid               hz_parties.party_id%TYPE;
        oocpartynumber           hz_parties.party_number%TYPE;
        oocpartyrelnumber        hz_org_contacts.contact_number%TYPE;
        oocpartyrelid            hz_party_relationships.party_relationship_id%TYPE;
        oocorgcontactid          hz_org_contacts.org_contact_id%TYPE;
        -- contact role
        pcustacctrolerec         hz_cust_account_role_v2pub.cust_account_role_rec_type;
        ocustacctroleid          hz_cust_account_roles.cust_account_role_id%TYPE;
        --contact variables
        pcontactpointrec         hz_contact_point_v2pub.contact_point_rec_type;
        pedirec                  hz_contact_point_v2pub.edi_rec_type;
        pemailrec                hz_contact_point_v2pub.email_rec_type;
        pphonerec                hz_contact_point_v2pub.phone_rec_type;
        ptelexrec                hz_contact_point_v2pub.telex_rec_type;
        pwebrec                  hz_contact_point_v2pub.web_rec_type;
        ocontactpointid          NUMBER;
        -- responsibility
        proleresponsibilityrec   hz_cust_account_role_v2pub.role_responsibility_rec_type;
        oresponsibilityid        NUMBER;
        l_proceed                VARCHAR2 (1);
        --
        ostatus                  VARCHAR2 (1);
        omsgcount                NUMBER;
        omsgdata                 VARCHAR2 (2000);

        v_dup_roll               NUMBER;

        CURSOR c2
        IS
            SELECT   *
              FROM   xxwc_ar_contact_points_cnv
             WHERE   ROWID = p_rowid AND process_status = 'V';
    BEGIN
        NULL;
    
    END xxpop_contact_info;

    --- ***************************************************************************************************
    -- Procedure xxpop_org_communication :  Organization Communication like Phone Number, Fax Number, Email 
    -- Information is created using this API  : Contact Points are created either at Party or Party Site Level
    
    
    PROCEDURE xxpop_org_communication (p_level             IN     VARCHAR2,
                                       p_party_id          IN     NUMBER,
                                       p_phone_area_code   IN     VARCHAR2,
                                       p_contact_phone     IN     VARCHAR2,
                                       p_fax_area_code     IN     VARCHAR2,
                                       p_contact_fax       IN     VARCHAR2,
                                       p_out                  OUT VARCHAR2,
                                       p_err_msg              OUT VARCHAR2)
    IS
        --contact variables
        pcontactpointrec   hz_contact_point_v2pub.contact_point_rec_type;
        pedirec            hz_contact_point_v2pub.edi_rec_type;
        pemailrec          hz_contact_point_v2pub.email_rec_type;
        pphonerec          hz_contact_point_v2pub.phone_rec_type;
        ptelexrec          hz_contact_point_v2pub.telex_rec_type;
        pwebrec            hz_contact_point_v2pub.web_rec_type;
        ocontactpointid    NUMBER;

        ostatus            VARCHAR2 (1);
        omsgcount          NUMBER;
        omsgdata           VARCHAR2 (2000);
    BEGIN
        IF p_contact_phone IS NOT NULL
        THEN
            ostatus := NULL;
            omsgcount := NULL;
            omsgdata := NULL;
            pcontactpointrec := NULL;
            pcontactpointrec.contact_point_type := 'PHONE';

            IF p_level = 'PARTY'
            THEN
                pcontactpointrec.owner_table_name := 'HZ_PARTIES';
            ELSE
                pcontactpointrec.owner_table_name := 'HZ_PARTY_SITES';
            END IF;

            pcontactpointrec.owner_table_id := p_party_id;
            pcontactpointrec.primary_flag := 'Y';
            pcontactpointrec.contact_point_purpose := 'BUSINESS';
            pphonerec.phone_country_code := NULL;
            pphonerec.phone_area_code := p_phone_area_code;
            pphonerec.phone_number := p_contact_phone;
            pphonerec.phone_extension := NULL;
            pphonerec.phone_line_type := 'GEN';
            pcontactpointrec.created_by_module := 'TCA_V1_API';

            hz_contact_point_v2pub.create_contact_point (
                p_init_msg_list       => 'T',
                p_contact_point_rec   => pcontactpointrec,
                p_phone_rec           => pphonerec,
                x_contact_point_id    => ocontactpointid,
                x_return_status       => ostatus,
                x_msg_count           => omsgcount,
                x_msg_data            => omsgdata);

            IF ostatus <> 'S'
            THEN
                IF omsgcount > 1
                THEN
                    FOR i IN 1 .. omsgcount
                    LOOP
                        fnd_file.put_line (
                            fnd_file.output,
                            'Org Phone contact point '
                            || SUBSTR (
                                   fnd_msg_pub.get (
                                       p_encoded => fnd_api.g_false),
                                   1,
                                   255));
                        p_err_msg :=
                            p_err_msg
                            || SUBSTR (
                                   fnd_msg_pub.get (
                                       p_encoded => fnd_api.g_false),
                                   1,
                                   255);
                    END LOOP;
                ELSE
                    fnd_file.put_line (
                        fnd_file.output,
                        'Org Phone contact point ' || omsgdata);
                    p_err_msg := omsgdata;
                END IF;

                p_out := 'E';
                RETURN;
            END IF;
        END IF;

        --Org level fax
        IF p_contact_fax IS NOT NULL
        THEN
            ostatus := NULL;
            omsgcount := NULL;
            omsgdata := NULL;
            pcontactpointrec := NULL;
            pcontactpointrec.contact_point_type := 'PHONE';

            IF p_level = 'PARTY'
            THEN
                pcontactpointrec.owner_table_name := 'HZ_PARTIES';
            ELSE
                pcontactpointrec.owner_table_name := 'HZ_PARTY_SITES';
            END IF;

            pcontactpointrec.owner_table_id := p_party_id;
            pcontactpointrec.contact_point_purpose := 'BUSINESS';
            pphonerec.phone_country_code := NULL;
            pphonerec.phone_area_code := p_fax_area_code;
            pphonerec.phone_number := p_contact_fax;
            pphonerec.phone_line_type := 'FAX';
            pcontactpointrec.created_by_module := 'TCA_V1_API';

            hz_contact_point_v2pub.create_contact_point (
                p_init_msg_list       => 'T',
                p_contact_point_rec   => pcontactpointrec,
                p_phone_rec           => pphonerec,
                x_contact_point_id    => ocontactpointid,
                x_return_status       => ostatus,
                x_msg_count           => omsgcount,
                x_msg_data            => omsgdata);

            IF ostatus <> 'S'
            THEN
                IF omsgcount > 1
                THEN
                    FOR i IN 1 .. omsgcount
                    LOOP
                        fnd_file.put_line (
                            fnd_file.output,
                            'Org Fax contact point '
                            || SUBSTR (
                                   fnd_msg_pub.get (
                                       p_encoded => fnd_api.g_false),
                                   1,
                                   255));
                        p_err_msg :=
                            p_err_msg
                            || SUBSTR (
                                   fnd_msg_pub.get (
                                       p_encoded => fnd_api.g_false),
                                   1,
                                   255);
                    END LOOP;
                ELSE
                    fnd_file.put_line (fnd_file.output,
                                       'Org Fax contact point ' || omsgdata);
                    p_err_msg := omsgdata;
                END IF;

                p_out := 'E';
                RETURN;
            END IF;
        END IF;
    END xxpop_org_communication;

    --- **************************************************************************************
    -- Procedure get_cust_conversion_stats :  API that gets customer conversion statistics and displays them in outfile. 

    PROCEDURE get_cust_conversion_stats
    IS
        CURSOR cust_cur
        IS
              SELECT   NVL (process_status, 'N') process_status,
                       COUNT ( * ) rec_count
                FROM   xxwc_ar_customers_cnv
            GROUP BY   process_status;

        CURSOR sites_cur
        IS
              SELECT   NVL (process_status, 'N') process_status,
                       COUNT ( * ) rec_count
                FROM   xxwc_ar_sites_cnv
            GROUP BY   process_status;

        CURSOR contacts_cur
        IS
              SELECT   NVL (process_status, 'N') process_status,
                       COUNT ( * ) rec_count
                FROM   xxwc_ar_contact_points_cnv
            GROUP BY   process_status;

        ln_customer_count   NUMBER;
        ln_site_count       NUMBER;
    BEGIN
        fnd_file.put_line (
            fnd_file.output,
            '*******************Customer Conversion Statistics **************************');


        SELECT   COUNT ( * )
          INTO   ln_customer_count
          FROM   xxwc_ar_customers_cnv cus
         WHERE   NOT EXISTS
                     (SELECT   1
                        FROM   xxwc_ar_sites_cnv
                       WHERE   orig_system_reference =
                                   cus.orig_system_reference);

        fnd_file.put_line (
            fnd_file.output,
            'Total Customers without Related Site Information :'
            || ln_customer_count);

        SELECT   COUNT ( * )
          INTO   ln_site_count
          FROM   xxwc_ar_sites_cnv sites
         WHERE   NOT EXISTS
                     (SELECT   1
                        FROM   xxwc_ar_customers_cnv
                       WHERE   orig_system_reference =
                                   sites.orig_system_reference);

        fnd_file.put_line (
            fnd_file.output,
            'Total Customer Sites without Related Customer Information :'
            || ln_site_count);

        fnd_file.put_line (fnd_file.output, ' ');

        fnd_file.put_line (
            fnd_file.output,
            '----------------Customer Statistics --------------------------');

        FOR cust_rec IN cust_cur
        LOOP
            IF cust_rec.process_status = 'P'
            THEN
                fnd_file.put_line (
                    fnd_file.output,
                    'Number Of Customer Records Processed           : '
                    || cust_rec.rec_count);
            ELSIF cust_rec.process_status = 'V'
            THEN
                fnd_file.put_line (
                    fnd_file.output,
                    'Number Of Customer Records in Validated status : '
                    || cust_rec.rec_count);
            ELSIF cust_rec.process_status = 'E'
            THEN
                fnd_file.put_line (
                    fnd_file.output,
                    'Number Of Customer Records Errored Out         : '
                    || cust_rec.rec_count);
            ELSIF cust_rec.process_status = 'N'
            THEN
                fnd_file.put_line (
                    fnd_file.output,
                    'Number Of Customer Records with NULL Status    : '
                    || cust_rec.rec_count);
            END IF;
        END LOOP;

        fnd_file.put_line (
            fnd_file.output,
            '----------------Customer Sites Statistics --------------------------');

        FOR cust_rec IN sites_cur
        LOOP
            IF cust_rec.process_status = 'P'
            THEN
                fnd_file.put_line (
                    fnd_file.output,
                    'Number Of Customer Sites Records Processed           : '
                    || cust_rec.rec_count);
            ELSIF cust_rec.process_status = 'V'
            THEN
                fnd_file.put_line (
                    fnd_file.output,
                    'Number Of Customer Sites Records in Validated status : '
                    || cust_rec.rec_count);
            ELSIF cust_rec.process_status = 'E'
            THEN
                fnd_file.put_line (
                    fnd_file.output,
                    'Number Of Customer Sites Records Errored Out         : '
                    || cust_rec.rec_count);
            ELSIF cust_rec.process_status = 'N'
            THEN
                fnd_file.put_line (
                    fnd_file.output,
                    'Number Of Customer Sites Records with NULL Status    : '
                    || cust_rec.rec_count);
            ELSIF cust_rec.process_status = 'I'
            THEN
                fnd_file.put_line (
                    fnd_file.output,
                    'Number Of Orphan Customer Sites Records   : '
                    || cust_rec.rec_count);
            END IF;
        END LOOP;

        fnd_file.put_line (
            fnd_file.output,
            '----------------Contact Points Statistics --------------------------');

        FOR cust_rec IN contacts_cur
        LOOP
            IF cust_rec.process_status = 'P'
            THEN
                fnd_file.put_line (
                    fnd_file.output,
                    'Number Of Contact Points Records Processed           : '
                    || cust_rec.rec_count);
            ELSIF cust_rec.process_status = 'V'
            THEN
                fnd_file.put_line (
                    fnd_file.output,
                    'Number Of Contact Points Records in Validated status : '
                    || cust_rec.rec_count);
            ELSIF cust_rec.process_status = 'E'
            THEN
                fnd_file.put_line (
                    fnd_file.output,
                    'Number Of Contact Points Records Errored Out         : '
                    || cust_rec.rec_count);
            ELSIF cust_rec.process_status = 'N'
            THEN
                fnd_file.put_line (
                    fnd_file.output,
                    'Number Of Contact Points Records with NULL Status    : '
                    || cust_rec.rec_count);
            ELSIF cust_rec.process_status = 'I'
            THEN
                fnd_file.put_line (
                    fnd_file.output,
                    'Number Of Orphan Contact Points Records  : '
                    || cust_rec.rec_count);
            END IF;
        END LOOP;
    END get_cust_conversion_stats;
    
    --- **************************************************************************************
    -- Procedure populate_site_locations :  API to populate Customer Site Locations. It has the logic to identify 
    -- duplicate addresses with in a party and assign them oracle location Id, Legacy Site Type column value in Customer Sites Staging table. 


    PROCEDURE populate_site_locations
    IS
        -- Define a Cursor that gets
        CURSOR cust_cur
        IS
            SELECT   orig_system_reference
              FROM   xxwc_ar_customers_cnv
             WHERE   1=1;
             
            

        CURSOR site_cur (
            p_customer_number VARCHAR2)
        IS
              SELECT   orig_system_reference,
                       address1,
                       address2,
                       address3,
                       address4,
                       city,
                       state,
                       postal_code,
                       county,
                       country,
                       province,
                       legacy_party_site_number,
                       legacy_site_type,
                       location_id
                FROM   xxwc_ar_sites_cnv
               WHERE   orig_system_reference = p_customer_number
                       AND location_id IS NULL
            ORDER BY   postal_code,
                       state,
                       city,
                       address1,
                       address2;



        -- Define Local Variables
        l_curr_postal_code   xxwc_ar_sites_cnv.postal_code%TYPE;
        l_curr_state         xxwc_ar_sites_cnv.state%TYPE;
        l_curr_city          xxwc_ar_sites_cnv.city%TYPE;
        l_curr_address1      xxwc_ar_sites_cnv.address1%TYPE;
        l_curr_address2      xxwc_ar_sites_cnv.address2%TYPE;
        l_curr_location_id   xxwc_ar_sites_cnv.location_id%TYPE;
        l_rec_count          NUMBER;
        l_legacy_site_type   NUMBER;
        c_max_commit_count   CONSTANT NUMBER := 1000;
        -- Satish U: 27-FEB-2012  : Added following  Variables 
        l_hyphen_pos    Number ;
        l_Str_length    Number ;
        l_substr        Varchar2(10);
    BEGIN
        -- Open the Customer Cursor
        l_rec_count := 0;
        fnd_file.put_line (fnd_file.LOG,'Begining of populate_site_locations');
        FOR cust_rec IN cust_cur
        LOOP
            FOR sites_rec IN site_cur (cust_rec.orig_system_reference)
            LOOP
                l_rec_count := l_rec_count + 1;
                --fnd_file.put_line (fnd_file.LOG,'301 Inside Sites Rec Cursor ');
                IF (NVL (sites_rec.postal_code, 'NULL') <>
                        NVL (l_curr_postal_code, 'NULL'))
                   OR (NVL (sites_rec.city, 'NULL') <>
                           NVL (l_curr_city, 'NULL'))
                   OR (NVL (sites_rec.state, 'NULL') <>
                           NVL (l_curr_state, 'NULL'))
                   OR (NVL (sites_rec.address1, 'NULL') <>
                           NVL (l_curr_address1, 'NULL'))
                   OR (NVL (sites_rec.address2, 'NULL') <>
                           NVL (l_curr_address2, 'NULL'))
                THEN
                    SELECT   hr_locations_s.NEXTVAL
                      INTO   l_curr_location_id
                      FROM   DUAL;
                   --fnd_file.put_line (fnd_file.LOG,'302  Generated Location ID ');
                ELSE
                    -- Do not Get New Location ID : Use THe Old One
                    NULL;
                END IF;

                l_curr_postal_code := sites_rec.postal_code;
                l_curr_city := sites_rec.city;
                l_curr_state := sites_rec.state;
                l_curr_address1 := sites_rec.address1;
                l_curr_address2 := sites_rec.address2;

                IF sites_rec.legacy_site_type IS NULL
                   OR sites_rec.location_id IS NULL
                THEN
                   
                    l_hyphen_pos := Instr(sites_rec.legacy_party_site_number , '-' , 1)  ;
                    l_Str_length := Length(sites_rec.legacy_party_site_number);  
                    l_substr     := Substr(sites_rec.legacy_party_site_number, l_hyphen_pos, l_Str_Length - l_Hyphen_Pos + 1) ; 
                   
                    If l_Hyphen_Pos > 0 Then 
                        IF TO_NUMBER(SUBSTR (RTRIM (sites_rec.legacy_party_site_number, l_substr), -3, 3)) = 0   THEN
                           l_legacy_site_type := 1000;
                        ELSE
                           l_legacy_site_type :=  TO_NUMBER(SUBSTR (RTRIM(sites_rec.legacy_party_site_number,l_substr ), -3, 3));
                        END IF;
                    Else
                       -- 20-APR-2012 : Satish U: 
                       Begin
                          l_legacy_site_type :=  TO_NUMBER(SUBSTR (TRIM(sites_rec.legacy_party_site_number), -3, 3));
                       Exception 
                          When Others Then 
                             l_legacy_site_type := 0; 
                       End;
                    End If; 
                   
                   -- Satish U: 20-APR-2012 
                   If sites_rec.legacy_party_site_number = sites_rec.orig_system_reference Then 
                      l_legacy_site_type := 1000;
                   End If; 
                    
                    --fnd_file.put_line (fnd_file.LOG,'304 Before updating xxwc_ar_Sites_cnv ');
                    UPDATE   xxwc_ar_sites_cnv
                       SET   location_id =
                                 NVL (location_id, l_curr_location_id),
                             legacy_site_type = l_legacy_site_type
                     WHERE   orig_system_reference =    sites_rec.orig_system_reference
                     AND legacy_party_site_number =     sites_rec.legacy_party_site_number;
                     
                     --fnd_file.put_line (fnd_file.LOG,'305 After updating xxwc_ar_Sites_cnv ');
                END IF;
            END LOOP;

            IF l_rec_count >= c_max_commit_count
            THEN
                l_rec_count := 0;
                COMMIT;
            END IF;
        END LOOP;
        fnd_file.put_line (fnd_file.LOG,'306 End of the API Populate Site Locations ');
        COMMIT;
    EXCEPTION
        WHEN OTHERS
        THEN
            fnd_file.put_line (fnd_file.LOG,'307 When Others Error : API Populate_Site_Locations ');
            fnd_file.put_line (
                fnd_file.output,
                ' When Others Error : API Populate_Site_Locations :'
                || SQLERRM);
            RAISE;
    END populate_site_locations;


    --- ********************************************************************************************************************
    -- Procedure update_customer_without_sites :  API Identifies Customer Records with out any sites and updates their status as Inactive' 
    
    PROCEDURE update_customer_without_sites
    IS
        CURSOR c1
        IS
            SELECT   orig_system_reference
            FROM   xxwc_ar_customers_cnv cus
            WHERE   NOT EXISTS
                 (SELECT   1
                  FROM   xxwc_ar_sites_cnv
                  WHERE   orig_system_reference =
                                  cus.orig_system_reference)
            AND Process_status <> 'I';
            
       
                     
    BEGIN
        FOR crec IN c1
        LOOP
            UPDATE   xxwc_ar_customers_cnv
               SET   Status = 'I'
            WHERE   orig_system_reference = crec.orig_system_reference;
        END LOOP;

        COMMIT;
    EXCEPTION
        WHEN OTHERS 
        THEN
            fnd_file.put_line (
                fnd_file.output,
                ' When Others Error : API update_customer_without_sites :'
                || SQLERRM);
            RAISE;
    END update_customer_without_sites;
    
    
    --- ********************************************************************************************************************
    -- Procedure update_Sites_without_Customer :  API Identifies Orphan Customer  Sites Records 
    -- Sites with out corresponding Customer Records and updates status as 'E'  
    
    PROCEDURE update_Sites_without_Customer Is
       Cursor Sites_Cur is (SELECT   Rowid, Sites.Orig_System_Reference, 
              Sites.Legacy_Party_Site_Number, sites.Hds_site_Flag
            FROM   xxwc_ar_sites_cnv sites
            WHERE   not exists 
                (Select 1 
                 From xxwc_ar_customers_cnv cust
                 Where cust.orig_system_reference = sites.orig_system_reference)) ;
    Begin 
       For Sites_Rec in Sites_Cur Loop 
          UPDATE   xxwc_ar_sites_cnv Sites
               SET   Process_status = 'E',
                     Process_Error  = 'Orphan Sites Record' 
          WHERE   rowid  = Sites_Rec.RowId;
       End Loop; 
       Commit; 
    Exception 
       When Others THen 
          Fnd_File.put_line (fnd_file.output, ' When Others Error : API update_Sites_without_Customer :' || SQLERRM);
          RAISE;
    End update_Sites_without_Customer; 
    
    
    --- ********************************************************************************************************************
    -- Procedure update_contacts_without_sites :  API Identifies Orphan Customer  Contacts, Records 
    -- ith out corresponding Customer  or Customer Customer Records and updates those records status as 'E'  
    
    PROCEDURE update_contacts_without_sites IS
        Cursor Contact_Cur Is ( 
           Select rowId, Orig_System_Reference, Legacy_party_Site_Number, hds_Site_flag
           From xxwc_ar_contact_points_cnv  cont 
           Where 1 =1 
        and not exists ( 
          Select 1 
          From  xxwc_ar_sites_cnv sites 
          Where sites.Orig_System_Reference   = cont.Orig_System_Reference 
          And (  Cont.legacy_Party_Site_Number =  Sites.Orig_System_Reference Or
                 Cont.Legacy_Party_Site_Number =  Sites.Legacy_Party_Site_Number ) ) ); 
                 
    Begin 
       For Contact_Rec in Contact_Cur Loop 
          UPDATE   xxwc_ar_contact_points_cnv
               SET   Process_status = 'E',
                     Process_Error  = 'Orphan Contact Record' 
          WHERE   rowid  = Contact_Rec.RowId;
       End Loop; 
       Commit; 
    Exception 
       When Others THen 
          Fnd_File.put_line (fnd_file.output, ' When Others Error : API update_Contacts_without_sites :' || SQLERRM);
          RAISE;
    End update_contacts_without_sites;
    
   
    --- ********************************************************************************************************************
    -- Procedure oe_ship_to :  Another Concurrent Program to process OE SHIP TO sites. This program should be called only 
    -- after main customer Conversion program is processed successfully. 
    -- API Processes all Customer Sites that are validated and whose customers are already processed through main program and 
    -- creates Customer Sites and its contacts 


    PROCEDURE oe_ship_to (errbuf       OUT VARCHAR2,
                          retcode      OUT VARCHAR2,
                          prefix    IN     VARCHAR2 DEFAULT NULL)
    IS
        CURSOR get_oe_ship_to_cust
        IS
            SELECT   DISTINCT sites.orig_system_reference, cust.account_name
            FROM   xxwc_ar_sites_cnv sites, xxwc_ar_customers_cnv cust
            WHERE  hds_site_flag = 'SHIP'
            AND sites.process_status = 'V'                     
            AND sites.orig_system_reference = cust.orig_system_reference
            AND cust.process_status = 'P'
            AND EXISTS (SELECT   1
                        FROM   hz_cust_accounts
                        WHERE   account_number = cust.orig_system_reference);

        CURSOR get_oe_ship_to_sites (
            p_customer VARCHAR2)
        IS
            SELECT   sites.ROWID rid,
                     sites.*,
                     hcsu.site_use_id,
                     hca.cust_account_id,
                     hca.party_id
              FROM   xxwc_ar_sites_cnv sites,
                     hz_cust_accounts hca,
                     apps.hz_cust_acct_sites hcas,
                     apps.hz_cust_site_uses hcsu
             WHERE       hds_site_flag = 'SHIP'
             AND process_status = 'V'
             AND sites.orig_system_reference = p_customer
             AND sites.orig_system_reference = hca.account_number
             AND hca.cust_account_id = hcas.cust_account_id
             AND hcas.cust_acct_site_id = hcsu.cust_acct_site_id
             AND hcsu.attribute1 IN ('MSTR', 'MISC')
             AND hcsu.site_use_code = 'BILL_TO'
             AND hcsu.primary_flag = 'Y';

        --Get site contact point
        CURSOR get_site_contact (
            p_cust_site                IN            VARCHAR2,
            p_legacy_customer_number   IN            VARCHAR2)
        IS
            SELECT   stg.ROWID rid, stg.*
            FROM   xxwc_ar_contact_points_cnv stg
            WHERE       1 = 1
            AND orig_system_reference = p_cust_site
            AND legacy_party_site_number = p_legacy_customer_number
            AND stg.process_status IN ('V');

        ostatus                   VARCHAR2 (1);
        omsgcount                 NUMBER;
        omsgdata                  VARCHAR2 (2000);
        v_count                   NUMBER;
        l_count                   NUMBER;
        l_proceed                 VARCHAR2 (1);
        l_party                   VARCHAR2 (1);
        l_profile_id              NUMBER;
        -- party variables
        porganizationrec          hz_party_v2pub.organization_rec_type;
        opartyid                  hz_parties.party_id%TYPE;
        opartynumber              hz_parties.party_number%TYPE;
        oprofileid                NUMBER;
        osprofile_id              NUMBER;
        -- party account variables
        pcustaccountrec           hz_cust_account_v2pub.cust_account_rec_type;
        ppartyrec                 hz_party_v2pub.party_rec_type;
        pprofilerec               hz_customer_profile_v2pub.customer_profile_rec_type;
        ocustaccountid            hz_cust_accounts.cust_account_id%TYPE;
        ocustaccountno            hz_cust_accounts.account_number%TYPE;
        --location variable
        plocationrec              hz_location_v2pub.location_rec_type;
        olocationid               hz_locations.location_id%TYPE;
        --site variables
        ppartysiterec             hz_party_site_v2pub.party_site_rec_type;
        opartysiteid              hz_party_sites.party_site_id%TYPE;
        opartysiteno              hz_party_sites.party_site_number%TYPE;
        --site account avriables
        pcustacctsiterec          hz_cust_account_site_v2pub.cust_acct_site_rec_type;
        ocustacctsiteid           hz_cust_acct_sites.cust_acct_site_id%TYPE;
        --site use variables
        pcustacctsiteuserec       hz_cust_account_site_v2pub.cust_site_use_rec_type;
        pcustomerprofile          hz_customer_profile_v2pub.customer_profile_rec_type;
        ocustacctsiteuseid        NUMBER;
        pbilltositeuseid          NUMBER;
        pcustacctid               NUMBER;
        l_terms_id                NUMBER;
        l_sales_person_id         NUMBER;
        l_bp                      NUMBER;
        l_territory               NUMBER;
        oalias                    VARCHAR2 (240);

        -- amount profile variables
        pcustproamtrec            hz_customer_profile_v2pub.cust_profile_amt_rec_type;
        ocustacctprofileamtid     NUMBER;
        pcurrencycode             VARCHAR2 (10);
        ---------
        whereami                  NUMBER := 0;
        e_validation_exception exception;
        p_result                  VARCHAR2 (1);
        p_mstr_counter            NUMBER;
        p_yard_counter            NUMBER;
        p_ship_counter            NUMBER;

        p_loc_count               NUMBER;
        p_mstr_loc_count          NUMBER;
        p_yard_loc_count          NUMBER;
        p_ship_loc_count          NUMBER;
        p_job_loc_count           NUMBER;
        p_site_counter            NUMBER;

        req_status                BOOLEAN;
        v_version_no              NUMBER;

        p_collector_id            NUMBER;
        p_credit_analyst_id       NUMBER;
        p_credit_classification   ar_lookups.lookup_code%TYPE;
        p_payment_term_id         NUMBER;
        p_object_version_number   NUMBER;
        v_err                     VARCHAR2 (1) := 'N';
        p_cus_err                 VARCHAR2 (4000);
        p_site_err                VARCHAR2 (4000);
        p_con_err                 VARCHAR2 (4000);
        p_rid                     VARCHAR2 (100);
        l_rec_count               NUMBER;
        c_max_commit_count        CONSTANT NUMBER := 100;
        l_api_name                VARCHAR2 (30) := 'XXWC_CUSTOMER_OE_SHIP_TO';
        lv_primary_ship_to_flag   VARCHAR2 (2);
    BEGIN
        g_parameter_prefix := prefix;
        l_rec_count := 0;

        FOR cust_acc IN get_oe_ship_to_cust
        LOOP
            BEGIN
                SAVEPOINT customer_record;
                l_rec_count := l_rec_count + 1;
                --set the counter

                p_ship_counter := 1;
                p_loc_count := 1;
                p_ship_loc_count := 1;
                p_site_counter := 1;

                FOR cust_sites
                IN get_oe_ship_to_sites (cust_acc.orig_system_reference)
                LOOP
                    plocationrec := NULL;
                    ppartysiterec := NULL;
                    pcustacctsiterec := NULL;
                    pcustacctsiteuserec := NULL;
                    pcustomerprofile := NULL;
                    ostatus := NULL;
                    omsgcount := NULL;
                    omsgdata := NULL;

                    l_count := 0;
                    olocationid := NULL;
                    ostatus := NULL;

                    BEGIN
                        SELECT   location_id
                          INTO   olocationid
                          FROM   hz_locations
                         WHERE   location_id = cust_sites.location_id;

                        l_count := 1;
                        ostatus := 'S';
                    EXCEPTION
                        WHEN NO_DATA_FOUND
                        THEN
                            l_count := 0;
                            ostatus := NULL;
                    END;


                    IF l_count = 0
                    THEN
                        -- location creation
                        IF NVL (cust_sites.country, 'USA') IN ('USA', 'US')
                        THEN
                            plocationrec.country := 'US';
                        ELSE
                            plocationrec.country := cust_sites.country;
                        END IF;

                        --SatishU : 10/26/2011
                        plocationrec.location_id := cust_sites.location_id; -- added MISC v.sankar 10/31/2011
                        plocationrec.postal_code := (cust_sites.postal_code);
                        plocationrec.address1 :=
                            NVL (cust_sites.address1, 'NO ADDRESS FOUND');
                        plocationrec.address2 := cust_sites.address2;
                        plocationrec.address3 := cust_sites.address3;
                        plocationrec.address4 := cust_sites.address4;
                        plocationrec.state := cust_sites.state;
                        plocationrec.city := cust_sites.city;
                        plocationrec.county := cust_sites.county;
                        plocationrec.created_by_module := 'TCA_V1_API';

                        whereami := 1001;
                        fnd_file.put_line (
                            fnd_file.LOG,
                            ' Before Calling Create Location API for OE SHIP TO');
                        hz_location_v2pub.create_location (
                            p_init_msg_list   => 'T',
                            p_location_rec    => plocationrec,
                            x_location_id     => olocationid,
                            x_return_status   => ostatus,
                            x_msg_count       => omsgcount,
                            x_msg_data        => omsgdata);
                    END IF;

                    IF ostatus <> 'S'
                    THEN
                        whereami := 1002;

                        IF omsgcount > 1
                        THEN
                            FOR i IN 1 .. omsgcount
                            LOOP
                                p_site_err :=
                                    NVL (p_site_err, ' ')
                                    || SUBSTR (
                                           fnd_msg_pub.get (
                                               i,
                                               p_encoded   => fnd_api.g_false),
                                           1,
                                           255);
                            END LOOP;

                            --Satish U: Moving Outside the Loop
                            fnd_file.put_line (
                                fnd_file.LOG,
                                ' CUSTOMER LOCATION RECORD FOR OE SHIP TO '
                                || SUBSTR (p_site_err, 1, 255));
                        ELSE
                            whereami := 1003;
                            fnd_file.put_line (
                                fnd_file.LOG,
                                   ' CUSTOMER LOCATION RECORD FOR OE SHIP TO'
                                || omsgdata
                                || '-'
                                || cust_sites.legacy_customer_number
                                || '-'
                                || cust_sites.country
                                || '-'
                                || cust_sites.address1
                                || '-'
                                || cust_sites.address2);
                            p_site_err := omsgdata;
                        END IF;

                        p_rid := cust_sites.rid;
                        whereami := 1004;
                        RAISE e_validation_exception;
                    ELSE
                        --initialize the values
                        ostatus := NULL;
                        omsgcount := NULL;
                        omsgdata := NULL;
                        opartysiteid := NULL;
                        opartysiteno := NULL;
                        -- create a party site now
                        ppartysiterec.party_id := cust_sites.party_id;
                        ppartysiterec.location_id := olocationid;

                        --v.sankar 11/22/2011. needs to be fixed next round.
                        ppartysiterec.party_site_number :=
                            SUBSTR (cust_sites.legacy_party_site_number,
                                    1,
                                    25);
                            

                        ppartysiterec.party_site_name :=
                               cust_sites.hds_site_flag
                            || '-'
                            || LPAD (p_ship_counter, 2, '0');
                        p_ship_counter := p_ship_counter + 1;



                        osprofile_id := NULL;
                        ppartysiterec.orig_system_reference :=
                            prefix || cust_sites.orig_system_reference;
                        ppartysiterec.created_by_module := 'TCA_V1_API';
                        whereami := 1005;
                        fnd_file.put_line (
                            fnd_file.LOG,
                            ' Before Calling Create Party Site API for OE Ship To');
                        hz_party_site_v2pub.create_party_site (
                            p_init_msg_list       => 'T',
                            p_party_site_rec      => ppartysiterec,
                            x_party_site_id       => opartysiteid,
                            x_party_site_number   => opartysiteno,
                            x_return_status       => ostatus,
                            x_msg_count           => omsgcount,
                            x_msg_data            => omsgdata);


                        IF ostatus <> 'S'
                        THEN
                            whereami := 1006;

                            IF omsgcount > 1
                            THEN
                                FOR i IN 1 .. omsgcount
                                LOOP
                                    p_site_err :=
                                        p_site_err
                                        || SUBSTR (
                                               fnd_msg_pub.get (
                                                   i,
                                                   p_encoded   => fnd_api.g_false),
                                               1,
                                               255);
                                END LOOP;

                                -- Satish U: Moving Outside the Loop
                                fnd_file.put_line (
                                    fnd_file.LOG,
                                    ' CUSTOMER PARTY SITE RECORD FOR OE SHIP TO'
                                    || SUBSTR (p_site_err, 1, 255));
                            ELSE
                                whereami := 1007;
                                fnd_file.put_line (
                                    fnd_file.LOG,
                                    ' CUSTOMER  PARTY SITE RECORD FOR OE SHIP TO'
                                    || omsgdata);
                                p_site_err := omsgdata;
                            END IF;

                            p_rid := cust_sites.rid;
                            RAISE e_validation_exception;
                        ELSE
                            
                            whereami := 1008;
                            --initialize the values
                            ostatus := NULL;
                            omsgcount := NULL;
                            omsgdata := NULL;
                            ocustacctsiteid := NULL;

                            pcustacctsiterec.cust_account_id :=     cust_sites.cust_account_id;
                            pcustacctsiterec.party_site_id := opartysiteid;
                            pcustacctsiterec.created_by_module := 'TCA_V1_API';
                            -- Satish U: 28-FEB-2012 : Changed Legacy_Customer_Number to Legacy_party_Site_Number 
                            pcustacctsiterec.orig_system_reference :=
                                   prefix
                                || cust_sites.legacy_Party_Site_number
                                || '-'
                                || p_site_counter;
                            p_site_counter := p_site_counter + 1;
                            pcustacctsiterec.org_id := g_org_id;

                            /*DFF Name ========
                            Address Information (Account Site level)*/

                            --Notice to Owner
                            pcustacctsiterec.attribute_category :=
                                cust_sites.attribute4;                --'Yes';
    
                            pcustacctsiterec.attribute1 := cust_sites.attribute16;
                            
                            --pcustacctsiterec.attribute2:='Mandatory Site Level Note'; --Mandatory Site Level Note

                            IF UPPER (cust_sites.attribute15) = 'YES'
                            THEN
                                pcustacctsiterec.attribute3 := 'Y'; --Y N NULL Mandatory PO Number
                            ELSIF UPPER (cust_sites.attribute15) = 'NO'
                            THEN
                                pcustacctsiterec.attribute3 := 'N';
                            END IF;

                            IF cust_sites.attribute5 IS NOT NULL
                            THEN
                                pcustacctsiterec.attribute5 :=
                                    TO_CHAR (
                                        TO_DATE (cust_sites.attribute5,
                                                 'YYYYMMDD'),
                                        'YYYY/MM/DD HH24:MI:SS');
                            --pcustacctsiterec.attribute5:=null; --cust_sites.attribute5; --to_char(sysdate,'YYYY/MM/DD HH24:MI:SS');  --Lien Release Date
                            END IF;

                            whereami := 1009;
                            --Mandatory Notes
                            pcustacctsiterec.attribute18 :=
                                cust_sites.attribute18; --Notice to Owner-Job Total
                            pcustacctsiterec.attribute19 :=
                                cust_sites.attribute19; --'Notice to Owner-Prelim Notice';  --Notice to Owner-Prelim Notice

                            IF cust_sites.attribute20 IS NOT NULL
                            THEN
                                pcustacctsiterec.attribute20 :=
                                    TO_CHAR (
                                        TO_DATE (cust_sites.attribute20,
                                                 'YYYYMMDD'),
                                        'YYYY/MM/DD HH24:MI:SS');
                            --pcustacctsiterec.attribute20:=null; --cust_sites.attribute20;--to_char(sysdate,'YYYY/MM/DD HH24:MI:SS');--Notice to Owner-Prelim Date
                            END IF;

                            whereami := 1010;
                            fnd_file.put_line (
                                fnd_file.LOG,
                                ' Before Calling Create Customer Account Site API for OE Ship To');
                            hz_cust_account_site_v2pub.create_cust_acct_site (
                                p_init_msg_list        => 'T',
                                p_cust_acct_site_rec   => pcustacctsiterec,
                                x_cust_acct_site_id    => ocustacctsiteid,
                                x_return_status        => ostatus,
                                x_msg_count            => omsgcount,
                                x_msg_data             => omsgdata);

                            IF ostatus <> 'S'
                            THEN
                                whereami := 1011;

                                --fnd_file.put_line(fnd_file.log,'1117- Create Customer Account Site'||omsgcount);
                                IF omsgcount > 1
                                THEN
                                    FOR i IN 1 .. omsgcount
                                    LOOP
                                        p_site_err :=
                                            NVL (p_site_err, ' ')
                                            || SUBSTR (
                                                   fnd_msg_pub.get (
                                                       i,
                                                       p_encoded   => fnd_api.g_false),
                                                   1,
                                                   255);
                                    END LOOP;

                                    -- Satish U: Moving it outside the Loop
                                    fnd_file.put_line (
                                        fnd_file.LOG,
                                        ' CUSTOMER ACCOUNT SITE RECORD FOR OE SHIP TO'
                                        || cust_acc.account_name
                                        || ' '
                                        || SUBSTR (p_site_err, 1, 255));
                                ELSE
                                    whereami := 1012;
                                    fnd_file.put_line (
                                        fnd_file.LOG,
                                        ' CUSTOMER ACCOUNT SITE RECORD FOR OE SHIP TO'
                                        || cust_acc.account_name
                                        || ' '
                                        || omsgdata);
                                    p_site_err := omsgdata;
                                END IF;

                                p_rid := cust_sites.rid;
                                RAISE e_validation_exception;
                            ELSE
                                whereami := 1013;

                                --initialize the values
                                pcustacctsiteuserec := NULL;
                                ostatus := NULL;
                                omsgcount := NULL;
                                omsgdata := NULL;
                                ocustacctsiteuseid := NULL;

                                pcustacctsiteuserec := NULL;
                                pcustomerprofile := NULL;          -- 01/14/09
                                pcustacctsiteuserec.site_use_code := 'SHIP_TO';
                                --get bill to site use id for ship to site
                                pcustacctsiteuserec.bill_to_site_use_id :=  cust_sites.site_use_id;
                                pcustacctsiteuserec.primary_flag := 'N';

                                pcustacctsiteuserec.cust_acct_site_id :=  ocustacctsiteid;

                                --populate location
                                IF cust_sites.job_name IS NOT NULL
                                   AND cust_sites.job_number IS NOT NULL
                                THEN
                                    pcustacctsiteuserec.location :=
                                        SUBSTR (
                                               UPPER (cust_sites.job_name)
                                            || '-'
                                            || cust_sites.job_number,
                                            1,
                                            40);
                                ELSIF cust_sites.legacy_party_site_name IS NOT NULL
                                      AND cust_sites.legacy_customer_number IS NOT NULL
                                THEN
                                    pcustacctsiteuserec.location :=
                                        SUBSTR (
                                            UPPER(cust_sites.legacy_party_site_name)
                                            || '-'
                                            || UPPER(cust_sites.legacy_customer_number),
                                            1,
                                            40);
                                ELSIF cust_sites.legacy_party_site_name IS NULL
                                      AND cust_sites.legacy_customer_number IS NOT NULL
                                THEN
                                    pcustacctsiteuserec.location :=
                                        'SHIP-'
                                        || UPPER(cust_sites.legacy_customer_number);
                                ELSIF cust_sites.legacy_party_site_name IS NOT NULL
                                      AND cust_sites.legacy_customer_number IS NULL
                                THEN
                                    pcustacctsiteuserec.location :=
                                        SUBSTR (
                                            UPPER(cust_sites.legacy_party_site_name)
                                            || '-'
                                            || LPAD (p_loc_count, 2, '0'),
                                            1,
                                            40);
                                    p_loc_count := p_loc_count + 1;
                                ELSIF cust_sites.legacy_party_site_name IS NULL
                                      AND cust_sites.legacy_customer_number IS NULL
                                THEN
                                    pcustacctsiteuserec.location :=
                                        'SHIP-'
                                        || LPAD (p_ship_loc_count, 2, '0');
                                END IF;

                                --whereami := 1014;

                                --Customer Site Classification

                                pcustacctsiteuserec.attribute1 := 'SHIP';
                                pcustacctsiteuserec.attribute5 := 'SHIP';
                                pcustacctsiteuserec.attribute3 := cust_sites.attribute11;


                                pcustacctsiteuserec.created_by_module :=
                                    'TCA_V1_API';
                                -- Check if Customer Site has House Account Salesrep ID Information 
                                -- Satish U: 23-FEB-2012 
                                If Cust_Sites.Salesrep_ID Is Not Null Then 
                                   BEGIN
                                      SELECT   salesrep_id
                                      INTO   l_sales_person_id
                                      FROM   ra_salesreps
                                      WHERE   1 = 1
                                      AND salesrep_number =  To_Char(cust_sites.Salesrep_ID)
                                      AND ROWNUM = 1; 
                                   EXCEPTION
                                      WHEN NO_DATA_FOUND   THen 
                                        l_sales_person_id := NULL ; 
                                        whereami := 430;
                                   End; 
                                End If;
                                
                                -- Find Regular Salesperson ID only when l_Sales_Person ID has NULL Value
                                If l_Sales_Person_ID IS NULL Then 
                                BEGIN
                                    SELECT   salesrep_id
                                    INTO   l_sales_person_id
                                    FROM   ra_salesreps
                                    WHERE   1 = 1
                                    AND salesrep_number = cust_sites.primary_salesrep_number
                                    AND ROWNUM = 1;
                                EXCEPTION
                                    WHEN NO_DATA_FOUND THEN
                                        whereami := 1014;

                                        SELECT   salesrep_id
                                        INTO   l_sales_person_id
                                        FROM   ra_salesreps
                                        WHERE   name = 'No Sales Credit' 
                                        AND ROWNUM = 1;
                                END;
                                End If; 

                                pcustacctsiteuserec.primary_salesrep_id :=  l_sales_person_id;

                                --added by v.sankar on 11/22/2011
                                pcustacctsiteuserec.gsa_indicator :=  cust_sites.gsa_indicator;



                                IF UPPER (cust_sites.freight_term) =
                                       'FREIGHT EXEMPT'
                                THEN
                                    pcustacctsiteuserec.freight_term :=
                                        'EXEMPT';
                                ELSE
                                    pcustacctsiteuserec.freight_term := NULL;
                                END IF;


                                fnd_file.put_line (
                                    fnd_file.LOG,
                                    ' Before Calling Create Customer Site Use API for OE SHIP TO');

                                hz_cust_account_site_v2pub.create_cust_site_use (
                                    p_init_msg_list          => 'T',
                                    p_cust_site_use_rec      => pcustacctsiteuserec,
                                    p_customer_profile_rec   => pcustomerprofile,
                                    p_create_profile         => 'F',
                                    p_create_profile_amt     => 'F',
                                    x_site_use_id            => ocustacctsiteuseid,
                                    x_return_status          => ostatus,
                                    x_msg_count              => omsgcount,
                                    x_msg_data               => omsgdata);
                                --pbilltositeuseid := ocustacctsiteuseid ;
                                pcustacctid := ocustaccountid;

                                IF ostatus <> 'S'
                                THEN
                                    whereami := 1015;
                                    l_proceed := 'N';
                                    fnd_file.put_line (
                                        fnd_file.LOG,
                                        ' Create Customer Site Use API Errored Out for OE SHIP TO- Msg Count :'
                                        || omsgcount);

                                    IF omsgcount > 1
                                    THEN
                                        FOR i IN 1 .. omsgcount
                                        LOOP
                                            p_site_err :=
                                                NVL (p_site_err, ' ')
                                                || SUBSTR (
                                                       fnd_msg_pub.get (
                                                           i,
                                                           p_encoded   => fnd_api.g_false),
                                                       1,
                                                       255);
                                        END LOOP;

                                        -- Satish U: 11/02/11 : Moved code out of the loop
                                        fnd_file.put_line (
                                            fnd_file.LOG,
                                            'CUSTOMER SHIP_TO SITE USE RECORD FOR OE SHIP TO'
                                            || SUBSTR (p_site_err, 1, 255));
                                    ELSE
                                        whereami := 1016;
                                        fnd_file.put_line (
                                            fnd_file.LOG,
                                            'CUSTOMER SHIP_TO SITE USE RECORD FOR OE SHIP TO '
                                            || omsgdata
                                            || '-'
                                            || cust_acc.account_name);
                                        p_site_err := omsgdata;
                                    END IF;

                                    p_rid := cust_sites.rid;
                                    RAISE e_validation_exception;
                                END IF;


                                l_proceed := 'Y';
                            END IF;                      -- party site account

                            --create party site level communication
                            whereami := 1017;
                    
                        END IF;                                  -- party site
                    END IF;                                         --location

                    UPDATE   xxwc_ar_sites_cnv
                       SET   process_status = 'P'
                     WHERE   ROWID = cust_sites.rid;
                END LOOP;

                --Sites
                IF l_rec_count >= c_max_commit_count
                THEN
                    COMMIT;                       --Commit the single customer
                    l_rec_count := 0;
                END IF;
            EXCEPTION
                WHEN e_validation_exception
                THEN
                    --whereami      := 590;
                    v_err := 'Y';
                    ROLLBACK TO customer_record;
                    upd_stagin_table_err (p_cus_err,
                                          p_site_err,
                                          p_con_err,
                                          p_rid);
                    p_cus_err := NULL;
                    p_site_err := NULL;
                    p_con_err := NULL;

                    --upd_stagin_table (cust_acc.orig_system_reference);
                    UPDATE   xxwc_ar_sites_cnv
                       SET   process_status = 'E'
                     WHERE   orig_system_reference =
                                 cust_acc.orig_system_reference
                             AND hds_site_flag = 'SHIP';

                    fnd_file.put_line (
                        fnd_file.LOG,
                        'e_Validation_Exception IN PROCEDURE for customer OE Ship To: '
                        || cust_acc.orig_system_reference
                        || '---'
                        || l_api_name
                        || '---'
                        || TO_CHAR (whereami));
                WHEN OTHERS
                THEN
                    -- whereami  := 600;
                    ROLLBACK TO customer_record;
                    v_err := 'Y';
                    --SatishU: 11/01/11    Added Following SQL Statement
                    upd_stagin_table_err (p_cus_err,
                                          p_site_err,
                                          p_con_err,
                                          p_rid);
                    p_cus_err := NULL;
                    p_site_err := NULL;
                    p_con_err := NULL;

                    --upd_stagin_table (cust_acc.orig_system_reference);
                    UPDATE   xxwc_ar_sites_cnv
                       SET   process_status = 'E'
                    WHERE   orig_system_reference =   cust_acc.orig_system_reference
                    AND hds_site_flag = 'SHIP';


                    fnd_file.put_line (
                        fnd_file.LOG,
                        'When Others Error in PROCEDURE for customer OE Ship To: '
                        || cust_acc.orig_system_reference
                        || '---'
                        || l_api_name
                        || SQLERRM
                        || '---'
                        || TO_CHAR (whereami));
            END;
        END LOOP;                                             --close cust acc

        COMMIT;                         -- Commit after the program completes.
        get_cust_conversion_stats;

        IF v_err = 'Y'    THEN
            req_status :=    fnd_concurrent.set_completion_status ('WARNING', NULL);
        END IF;
    EXCEPTION
        WHEN OTHERS     THEN
            RAISE;
    END oe_ship_to;


    --- ********************************************************************************************************************
    -- Procedure xxpop_org_web_contact :  API to create WEB Contact at Party or Party Site Level . This API is called from 
    -- the main Customer Conversion program 

    PROCEDURE xxpop_org_web_contact   (p_level             IN     VARCHAR2,
                                       p_party_id          IN     NUMBER,
                                       p_cod_comment       IN     VARCHAR2,
                                       p_keyword           IN     VARCHAR2, 
                                       p_out                  OUT VARCHAR2,
                                       p_err_msg              OUT VARCHAR2)
    IS
        --contact variables
        pcontactpointrec   hz_contact_point_v2pub.contact_point_rec_type;
        pedirec            hz_contact_point_v2pub.edi_rec_type;
        pemailrec          hz_contact_point_v2pub.email_rec_type;
        pphonerec          hz_contact_point_v2pub.phone_rec_type;
        ptelexrec          hz_contact_point_v2pub.telex_rec_type;
        pwebrec            hz_contact_point_v2pub.web_rec_type;
        ocontactpointid    NUMBER;

        ostatus            VARCHAR2 (1);
        omsgcount          NUMBER;
        omsgdata           VARCHAR2 (2000);
    BEGIN
        
            ostatus := NULL;
            omsgcount := NULL;
            omsgdata := NULL;
            pcontactpointrec := NULL;
            pcontactpointrec.contact_point_type := 'WEB';

            IF p_level = 'PARTY'
            THEN
                pcontactpointrec.owner_table_name := 'HZ_PARTIES';
            ELSE
                pcontactpointrec.owner_table_name := 'HZ_PARTY_SITES';
            END IF;

            pcontactpointrec.owner_table_id := p_party_id;
            pcontactpointrec.primary_flag := 'Y';
            pcontactpointrec.contact_point_purpose := 'HOMEPAGE';
            pwebrec.web_type := 'HTTP';
            IF p_cod_comment IS NULL AND p_keyword IS NOT NULL
            THEN
                pwebrec.url := p_keyword;
            ELSIF p_cod_comment IS NOT NULL AND p_keyword IS NULL
            THEN
                pwebrec.url := p_cod_comment;
            ELSE
                pwebrec.url := p_cod_comment || ' - ' || p_keyword;
            END IF;
            pcontactpointrec.created_by_module := 'TCA_V1_API';

            hz_contact_point_v2pub.create_contact_point (
                p_init_msg_list       => 'T',
                p_contact_point_rec   => pcontactpointrec,
                p_web_rec             => pwebrec,
                x_contact_point_id    => ocontactpointid,
                x_return_status       => ostatus,
                x_msg_count           => omsgcount,
                x_msg_data            => omsgdata);

            IF ostatus <> 'S'
            THEN
                IF omsgcount > 1
                THEN
                    FOR i IN 1 .. omsgcount
                    LOOP
                        fnd_file.put_line (
                            fnd_file.output,
                            'Org Web contact point '
                            || SUBSTR (
                                   fnd_msg_pub.get (
                                       p_encoded => fnd_api.g_false),
                                   1,
                                   255));
                        p_err_msg :=
                            p_err_msg
                            || SUBSTR (
                                   fnd_msg_pub.get (
                                       p_encoded => fnd_api.g_false),
                                   1,
                                   255);
                    END LOOP;
                ELSE
                    fnd_file.put_line (
                        fnd_file.output,
                        'Org Web contact point ' || omsgdata);
                    p_err_msg := omsgdata;
                END IF;

                p_out := 'E';
                RETURN;
            END IF;           
        
    END xxpop_org_web_contact;
    
    ---**************************************************************************************-------------
    -- ***** Procedure Inactivate_Cust_Accounts : to Inactive Cust Accounts that are in Inactive Status in Staging Table*************---- 
    --    Initially all Customers are created in active status. Upon creation of AR transactions this procedure should be called 
    -- to inactivate customers that are supposed to be based on Staging table information. 
    
    
    PROCEDURE  Inactivate_Cust_Accounts (Errbuf   Out Varchar2, 
                                        Retcode  Out Varchar2 ) IS 
    
      -- Define a Cursor to get the List of Active Customers IN Oracle that are Inactive in Customer COnversion Staging Table. 
      Cursor  Cust_Cur  IS 
          Select Cust_Account_ID, Account_Number , object_Version_Number 
          From Hz_Cust_Accounts hca
          Where Status = 'A'
          And  Exists  ( Select 1
          From xxwc_ar_customers_cnv  conv 
          Where  Conv.Orig_System_Reference = hca.Orig_System_Reference 
          And    Conv.Status = 'I' ) ;
          
      l_return_status         Varchar2(1) ; 
      l_msg_count             Number ;
      l_msg_data              Varchar2(2000); 
      l_Version_Number        Number ; 
      l_Total_Records         Number := 0; 
      l_Success_Count         Number := 0; 
      l_Failure_Count         Number := 0; 
      l_Max_Rec_Commit        Number := 0; 
      l_Cust_Account_Rec      hz_cust_account_v2pub.CUST_ACCOUNT_REC_TYPE ; 
      
    Begin
       For Cust_Rec In Cust_Cur Loop 
          Begin 
             l_Total_Records   := l_Total_Records + 1; 
             l_Max_Rec_Commit  := l_Max_Rec_Commit + 1; 
             fnd_file.put_line (fnd_file.LOG, ' Before Calling Update Account API ');
             l_return_status := NULL;
             l_msg_data := NULL;
             l_msg_count := NULL;                            
             l_cust_account_rec.cust_account_id := Cust_Rec.cust_account_id;
             l_cust_account_rec.Account_Number  := Cust_Rec.Account_Number;
             l_cust_account_rec.status := 'I';
             l_Version_Number          := Cust_Rec.object_Version_Number ;
             
             hz_cust_account_v2pub.update_cust_account (
                    p_init_msg_list          => 'T',
                    p_cust_account_rec       => l_cust_account_rec, -- customer account record
                    p_object_version_number  => l_Version_Number,                          
                    x_return_status          => l_return_status,
                    x_msg_count              => l_msg_count,
                    x_msg_data               => l_msg_data);
              IF l_return_status <> 'S'  THEN
                  l_Failure_Count  := l_Failure_Count + 1; 
                  IF l_msg_count > 1 THEN
                     l_msg_data  := ''; 
                     FOR i IN 1 .. l_msg_count LOOP
                        l_msg_data := l_msg_data  || SUBSTR (fnd_msg_pub.get ( i,  p_encoded   => fnd_api.g_false),1,255);
                     END LOOP;
                     fnd_file.put_line (fnd_file.LOG,'UPDATE CUSTOMER API Error out for : '
                                            || l_cust_account_rec.account_name
                                            || ' Error message : '
                                            || l_Msg_Data);
                     fnd_file.put_line (fnd_file.output, l_cust_account_rec.account_Number || ' Update status failed.'); 
                                              
                  ELSE
                     fnd_file.put_line (fnd_file.LOG, 'UPDATE CUSTOMER API Error out for : '
                                            || l_cust_account_rec.account_name
                                            || ' Error message : '
                                            || l_Msg_Data);
                     fnd_file.put_line (fnd_file.output, l_cust_account_rec.account_Number || ' Update status failed.'); 
                  END IF;
               ELSE
                  l_Success_Count  := l_Success_Count + 1; 
                  fnd_file.put_line (fnd_file.output, l_cust_account_rec.account_Number || '  Update status successful.'); 
               END IF; 
               If l_Max_Rec_Commit >= 500 Then 
                  Commit; 
                  l_Max_Rec_Commit := 0; 
               End If; 
          Exception 
             When Others Then 
                fnd_file.put_line (fnd_file.LOG, ' When Others Error 01 : UPDATE CUSTOMER API for : '
                                            || l_cust_account_rec.account_name
                                            || ' Error message : '
                                            || SQLERRM);
                RetCode := 1; 
          End ; 
       End Loop ; 
       fnd_file.put_line (fnd_file.output, 'Total Number of Customers Processed                :' ||l_Total_Records); 
       fnd_file.put_line (fnd_file.output, 'Total Number of Customers successfully inactivated :' ||l_Success_Count); 
       fnd_file.put_line (fnd_file.output, 'Total Number of Customers failed to inactivate     :' ||l_Failure_Count); 
       Commit; 
    Exception 
       When Others Then 
          fnd_file.put_line (fnd_file.LOG, ' When Others Error 02 : UPDATE CUSTOMER API for : '
                                            || l_cust_account_rec.account_name
                                            || ' Error message : '
                                            || SQLERRM);
         RetCode := 1; 
    END Inactivate_Cust_Accounts ; 
    
    
  ---**************************************************************************************-------------
   -- ***** Procedure Update_Party_Site_Locations:  Creates new Locations that are shared between party sites. 
   --******* If User Pass Customer Number as input parameter then the program looks for all party Sites for a given 
   --****** Customer else it process all party Sites that share Locations. 
   --*******************************************************************************************-------------------
                                       
   PROCEDURE  Update_Party_Site_Locations(Errbuf   Out Varchar2, 
                                          Retcode  Out Varchar2, 
                                          p_Cust_Account_Id IN Number,
                                          p_Number_Of_Sites In Number) IS 
   
      -- Define a Cursor that gets all Locations that are assigned to multiple Location Id 
      Cursor Get_Dup_Loc_Cur IS
        Select count(*), location_Id 
        From hz_party_Sites hps1
        Where Exists ( 
            Select 1
            From Hz_Cust_Accounts hca , 
                 Hz_party_Sites hps 
            Where hca.party_id = hps.party_Id 
            and   hps.location_Id = hps1.Location_ID  
            and  (p_cust_Account_ID is Null Or hca.Cust_Account_ID = p_Cust_Account_ID ))
        group by location_Id
        having Count(*) > 1  ; 
        
     
      -- Define a Cursor that gets Location record information for a Lcoation ID    
      Cursor Get_Loc_Cur (p_Location_ID Number ) IS
         Select *
         From Hz_Locations 
         Where Location_ID = p_Location_ID ; 
      
      
      -- Define a Cursor that gets list of Party Sites where location_ID needs to be updated 
      Cursor Get_Party_Sites_Cur (p_Location_ID Number ) IS
         Select  hps1.Party_Site_ID, hps1.Party_Site_Number, hps1.Location_ID, hps1.Party_ID, hps1.Object_Version_Number, hca.Account_Number, hca.Status
         From Hz_party_Sites hps1, 
              Hz_Cust_Accounts hca
         Where hps1.Location_ID = p_Location_ID 
         And   hps1.Party_Id    = hca.Party_ID 
         AND hps1.Party_Site_ID > (  Select Min(Party_Site_Id ) 
           From hz_party_Sites hps2 
           Where hps2.Location_Id = p_location_ID
           and hps2.Party_Id = hps1.Party_ID ) ;
           
       l_Record_Count   Number := 0; 
       l_location_id    Number := 0;
       l_Return_Status  Varchar2(1) ; 
       l_Msg_Count      Number ; 
       l_Msg_Data       Varchar2(2000) ; 
       l_location_rec hz_location_v2pub.location_rec_type;
       l_party_site_rec hz_party_site_v2pub.party_site_rec_type;
       l_Object_Version_Number   Number ; 
       l_Party_Site_Rec_Count    Number := 0 ; 
       
       Create_Loc_excep          Exception  ;
       Update_Party_Site_Excep   Exception  ; 
       Max_Party_Sites_Exp       Exception ; 
       
    Begin
       fnd_file.put_line( fnd_file.LOG,' Begning of Concurrent Program');
       fnd_file.put_line(fnd_file.output, '<HTML><BODY>');
       fnd_file.put_line(fnd_file.output, '<PRE>');
       fnd_file.new_line(fnd_file.output, 1);
       fnd_file.put_line(fnd_file.output, '********************************************************************************************<BR>');
       fnd_file.put_line(fnd_file.output, '<H3>  List of Party Sites whose Location Id is Updated with new Location ID                     </H3>');
       fnd_file.put_line(fnd_file.output,'********************************************************************************************<BR>');
       fnd_file.put_line(fnd_file.output,'<H4> 1. Program Name    : '|| ' XXWC Assign distinct locations to Customer Sites');
       fnd_file.put_line(fnd_file.output,    ' 2. Start Date      : '|| TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));
       fnd_file.put_line(fnd_file.output,    ' 3. P_Cust_Account_ID  : '|| p_Cust_Account_ID);
       fnd_file.put_line(fnd_file.output,    ' 4. P_Number_Of_Sites  : '|| P_Number_Of_Sites);
       fnd_file.put_line(fnd_file.output, '</H4>');
       fnd_file.put_line(fnd_file.output, '********************************************************************************************<BR>');
       fnd_file.put_line(fnd_file.output, '<BR>');
       fnd_file.put_line(fnd_file.output, '</PRE>');
       fnd_file.put_line(fnd_file.output, '<BR>');
       fnd_file.put_line(fnd_file.output, '</PRE>');
       fnd_file.put_line(fnd_file.output, '<TABLE BORDER=1>');
       fnd_file.put_line(fnd_file.output,'<TR><TH> S.No </TH><TH>Customer Number # </TH>  <TH>Status </TH><TH>Party Site Number # </TH> <TH> Old Location ID# </TH> <TH> New Location ID# </TH></TR>');
       For Dup_Loc_Rec in Get_Dup_Loc_Cur Loop 
           fnd_file.put_line ( fnd_file.LOG,' Processing Duplicate Location Rec : ' || Dup_Loc_Rec.Location_ID);
           -- Get List of Party Sites whose Location ID needs to be updated 
           For Party_Sites_Rec In Get_Party_Sites_Cur (Dup_Loc_Rec.Location_ID ) Loop 
              Begin
                 
                 fnd_file.put_line ( fnd_file.LOG,' Processing Party Site Rec : ' || Dup_Loc_Rec.Location_ID);
                 -- Open Location Cursor and create New Location 
                 For  Loc_Rec in Get_Loc_Cur(Dup_Loc_Rec.Location_ID)  Loop 
                    Begin
                       Savepoint Create_Location ; 
                       l_Location_Rec             := Null; 
                       l_Location_ID              := Null; 
                       l_Return_Status           := 'S' ; 
                       l_location_Rec.Country     := Loc_Rec.Country ;
                       l_location_Rec.postal_code := Loc_Rec.Postal_Code ;
                       l_location_Rec.Address1    := Loc_Rec.Address1; 
                       l_location_Rec.Address2    := Loc_Rec.Address2;
                       l_location_Rec.Address3    := Loc_Rec.Address3;
                       l_location_Rec.Address4    := Loc_Rec.Address4;
                       l_location_Rec.state       := Loc_Rec.state;
                       l_location_Rec.city        := Loc_Rec.city;
                       l_location_Rec.county      := Loc_Rec.county;
                       l_location_Rec.country     := Loc_Rec.country;
                       l_location_Rec.created_by_module := Loc_Rec.created_by_module;
                       --l_Location_Rec.Created_BY        := Fnd_Global.User_ID ; 
                       --l_Location_Rec.Last_Updated_BY   := Fnd_Global.User_ID ; 
                       --l_Location_Rec.Creation_Date     := SYSDATE ; 
                       -- l_Location_Rec.Last_Update_Date  := SYSDATE ; 
                       --l_Location_Rec.Last_Update_Login := Fnd_Global.Login_ID ; 
                       -- l_Location_Rec.Request_ID        := Fnd_Global.Conc_Request_Id ;
                       --l_Location_Rec.Program_Id        := Fnd_Global.Conc_Program_ID ; 
                       --l_Location_Rec.Program_Application_ID := Fnd_Global.Prog_Appl_ID ; 
                       --l_Location_Rec.Program_Update_Date := SYSDATE; 
                       -- Call Oracle API to Create Location 
                       -- This Location will be used to update HZ Party Sites 
                         
                       hz_location_v2pub.create_location (
                                p_init_msg_list   => 'T',
                                p_location_rec    => l_location_Rec,
                                x_location_id     => l_location_id,
                                x_return_status   => l_Return_Status,
                                x_msg_count       => l_msg_count,
                                x_msg_data        => l_msg_data);
                                
                        IF l_Return_Status <> 'S'  THEN
                           IF l_msg_count > 1  THEN
                               FOR i IN 1 .. l_msg_count LOOP
                                   l_msg_data :=  NVL (l_msg_data, ' ') || SUBSTR (fnd_msg_pub.get(i,  p_encoded   => fnd_api.g_false), 1, 255);
                               END LOOP;
                               --Satish U: Moving Outside the Loop
                               fnd_file.put_line ( fnd_file.LOG,' Error : CUSTOMER LOCATION RECORD  ' || SUBSTR (l_msg_data, 1, 255));
                           ELSE
                              fnd_file.put_line ( fnd_file.LOG,' Error : CUSTOMER LOCATION RECORD  ' || SUBSTR (l_msg_data, 1, 255));
                           END IF;
                           RAISE Create_Loc_excep;
                        ELSE
                           fnd_file.put_line ( fnd_file.LOG,' Customer Location Record Successfully Created :' || l_location_id );
                        END IF; 
                    End; --Loc_Rec           
                 End Loop; 
                   
                 --l_party_site_rec.identifying_address_flag := Party_Sites_Rec.identifying_address_flag; 
                 --l_party_site_rec.party_site_id      := Party_Sites_Rec.PARTY_SITE_ID ;
                 --l_party_site_rec.Location_id        := l_location_ID ;
                 --l_party_site_rec.party_id           := Party_Sites_Rec.party_id;
                 --l_party_site_rec.status             := Party_Sites_Rec.status;
                 --l_party_site_rec.party_site_name    := Party_Sites_Rec.party_site_name;
                 --l_party_site_rec.attribute_category := Party_Sites_Rec.attribute_category;          
                 --l_party_site_rec.addressee          := Party_Sites_Rec.addressee;               
                 --l_party_site_rec.created_by_module  := Party_Sites_Rec.created_by_module; 
                 
                 --l_party_site_rec.Last_Updated_BY   := Fnd_Global.User_ID ; 
                 --l_party_site_rec.Last_Update_Date  := SYSDATE ; 
                 --l_party_site_rec.Last_Update_Login := Fnd_Global.Login_ID ; 
                 --l_party_site_rec.Request_ID        := Fnd_Global.Conc_Request_Id ;
                 --l_party_site_rec.Program_Id        := Fnd_Global.Conc_Program_ID ; 
                 --l_party_site_rec.Program_Application_ID := Fnd_Global.Prog_Appl_ID ; 
                 --l_party_site_rec.Program_Update_Date := SYSDATE; 
                 l_Object_Version_Number := Party_Sites_Rec.OBJECT_VERSION_NUMBER; 
                 
                 Update Hz_Party_Sites 
                 Set  Location_ID = l_location_ID, 
                      Request_Id  = Fnd_Global.Conc_Request_Id , 
                      Program_id  = Fnd_Global.Conc_Program_ID, 
                      Last_Updated_By = Fnd_Global.User_ID ,
                      Last_Update_Date = Sysdate ,
                      Object_Version_Number = Object_Version_Number + 1 
                 Where Party_Site_ID =  Party_Sites_Rec.PARTY_SITE_ID ;
                 
                 l_Party_Site_Rec_Count  := l_Party_Site_Rec_Count + 1; 
                 
                 If Mod(l_party_Site_Rec_Count, 1000 ) = 0 Then 
                    -- Commit Transactions every 1000 records 
                    Commit; 
                 End If; 
                 If l_Party_Site_Rec_Count > p_Number_Of_Sites  Then 
                    Raise Max_Party_Sites_Exp ; 
                 End If; 
                 fnd_file.put_line (
                    fnd_file.output,
                       '<TR><TD>'
                    || l_Party_Site_Rec_Count 
                    || '</TD><TD>'
                    || Party_Sites_Rec.Account_Number
                    || '</TD><TD>'
                    || Party_Sites_Rec.Status
                    || '</TD><TD>'
                    || Party_Sites_Rec.Party_Site_Number 
                    || '</TD><TD>'
                    || Party_Sites_Rec.Location_ID 
                    || '</TD><TD>'
                    || l_Location_ID
                    || '</TD></TR>');
                 
                 /**********************
                 -- Check if Address is creatted Successfully If So then 
                 hz_party_site_v2pub.update_party_site (
                        p_init_msg_list         => FND_API.G_TRUE,
                        p_party_site_rec        => l_party_site_rec,
                        p_object_version_number => l_Object_Version_Number,
                        x_return_status         => l_Return_Status,
                        x_msg_count             => l_Msg_Count,
                        x_msg_data              => l_Msg_Data ) ; 
                   
                 IF l_Return_Status <> 'S'  THEN
                     IF l_msg_count > 1  THEN
                        FOR i IN 1 .. l_msg_count LOOP
                            l_msg_data :=  NVL (l_msg_data, ' ') || SUBSTR (fnd_msg_pub.get(i,  p_encoded   => fnd_api.g_false), 1, 255);
                        END LOOP;
                        --Satish U: Moving Outside the Loop
                        fnd_file.put_line ( fnd_file.LOG,' Error : Update Party Site Rec  ' || SUBSTR (l_msg_data, 1, 255));
                     ELSE
                        fnd_file.put_line ( fnd_file.LOG,' Error : Update Party Site Rec  ' || SUBSTR (l_msg_data, 1, 255));
                     END IF;
                     RAISE Update_Party_Site_Excep;
                 ELSE
                     fnd_file.put_line ( fnd_file.LOG,' Update Party Site Rec Successfully Created :' || l_party_site_rec.Party_Site_Number );
                     Commit; 
                 END IF;
                 **************/
              Exception 
                  When Create_Loc_excep Then 
                      Rollback TO Create_Location ; 
                  When Update_Party_Site_Excep Then 
                      Rollback To Create_Location ;
                     
              End; 
           End Loop; 
       End Loop; 
       
       If l_Return_Status = 'S'  Then 
          Commit; 
          fnd_file.put_line (fnd_file.output, '</TABLE>');
          fnd_file.put_line (fnd_file.output, '</BODY></HTML>');
       End If; 
    Exception 
       When Max_Party_Sites_Exp Then 
            Commit; 
            fnd_file.put_line (fnd_file.output, '</TABLE>');
            fnd_file.put_line (fnd_file.output, '</BODY></HTML>');
       When Others  Then 
          fnd_file.put_line (fnd_file.Log, ' When Others Exception : ' || SQLERRM ); 
          fnd_file.put_line (fnd_file.output, ' When Others Exception : ' || SQLERRM ); 
          Retcode := 2; 
    End Update_Party_Site_Locations;
  
                                      
END xxwc_customer_conversion_pkg;
/
show errors;
commit;
exit;