CREATE OR REPLACE PACKAGE BODY APPS.XXWC_ARS_OPEN_BALANCE_PKG
AS
   /*************************************************************************
     $Header XXWC_ARS_OPEN_BALANCE_PKG $
     Module Name: XXWC_ARS_OPEN_BALANCE_PKG.pkb

     PURPOSE:   This package is called by the concurrent programs
                XXWC ARS Outbound Open Balance

     REVISIONS:
     Ver        Date        Author             Description
     ---------  ----------  ---------------   -------------------------
      1.0       11/12/2016  Neha Saini         Initial Build - TMS#20160126-00238
   **************************************************************************/
   g_loc   VARCHAR2 (20000);

   /*************************************************************************
     Procedure : Write_Log

     PURPOSE:   This procedure logs debug message in Concurrent Log file
     Parameter:
            IN
                p_debug_msg      -- Debug Message
    REVISIONS:
       Ver        Date        Author             Description
     ---------  ----------  ---------------   -------------------------
      1.0       11/12/2016  Neha Saini         Initial Build - TMS#20160126-00238
   ************************************************************************/
   PROCEDURE write_log (p_debug_msg IN VARCHAR2)
   IS
   BEGIN
      fnd_file.put_line (fnd_file.LOG, p_debug_msg);
      DBMS_OUTPUT.put_line (p_debug_msg);
   END write_log;

   /*************************************************************************
      Procedure : Write_Error

     PURPOSE:   This procedure logs error message
     Parameter:
            IN
                p_debug_msg      -- Debug Message
    REVISIONS:
       Ver        Date        Author             Description
     ---------  ----------  ---------------   -------------------------
      1.0       11/12/2016  Neha Saini         Initial Build - TMS#20160126-00238
   ************************************************************************/

   --add message to concurrent output file
   PROCEDURE write_error (p_debug_msg IN VARCHAR2)
   IS
      l_req_id          NUMBER := fnd_global.conc_request_id;
      l_err_callfrom    VARCHAR2 (75) DEFAULT 'XXWC_ARS_OPEN_BALANCE_PKG';
      l_err_callpoint   VARCHAR2 (75) DEFAULT 'START';
      l_distro_list     VARCHAR2 (75)
                           DEFAULT 'HDSOracleDevelopers@hdsupply.com';
   BEGIN
      xxcus_error_pkg.xxcus_error_main_api (
         p_called_from         => l_err_callfrom,
         p_calling             => l_err_callpoint,
         p_request_id          => l_req_id,
         p_ora_error_msg       => SUBSTR (p_debug_msg, 1, 2000),
         p_error_desc          => 'Error running XXWC_ARS_OPEN_BALANCE_PKG with PROGRAM ERROR',
         p_distribution_list   => l_distro_list,
         p_module              => 'AR');
   END write_error;

   /*************************************************************************
     Procedure : generate_ars_file
     PARAMETERS:  IN - None
                  OUT - errbuf,retcode
     PURPOSE:  This procedure creates file for the ARS Outbound Open Balance
     REVISIONS:
       Ver        Date        Author             Description
     ---------  ----------  ---------------   -------------------------
      1.0       11/12/2016  Neha Saini         Initial Build - TMS#20160126-00238
   ************************************************************************/
   PROCEDURE generate_ars_file (errbuf OUT VARCHAR2, retcode OUT VARCHAR2)
   IS
      CURSOR cur_open_bal
      IS
           SELECT SUM (transaction_remaining_balance) open_balance,
                  trx_party_site_number party_site_number
             FROM xxeis.xxwc_ar_customer_balance_mv
         GROUP BY trx_party_site_number;

      l_filename         VARCHAR2 (200);
      l_count            NUMBER;
      g_prog_exception   EXCEPTION;
      v_file             UTL_FILE.file_type;
      l_err_msg          VARCHAR2 (4000);
      l_directory_name   VARCHAR2 (5000) := 'XXWC_ARS_OB_DIR';
   BEGIN
      -- printing in Parameters
      g_loc := 'Begining ARS Open balance Extract  ';
      write_log ('Begining ARS Open balance Extract  ');
      write_log ('========================================================');


      --Initialize the Out Parameter
      errbuf := NULL;
      retcode := '0';



      -- Get the file name
      SELECT    'ARSOpenBal_'
             || TO_CHAR (SYSDATE, 'RRRRMMDD_HH_MI_SS')
             || '.csv'
        INTO l_filename
        FROM DUAL;

      g_loc := 'getting file name';
      write_log ('l_filename:  ' || l_filename);

      --Open the file handler
      v_file :=
         UTL_FILE.fopen (LOCATION       => l_directory_name,
                         filename       => l_filename,
                         open_mode      => 'w',
                         max_linesize   => 32767);

      write_log ('Writing to the file ... Opening Cursor');
      l_count := 0;
      g_loc := 'now writing in file ';

      FOR cur_rec IN cur_open_bal
      LOOP
         EXIT WHEN cur_open_bal%NOTFOUND;


         -- Writing File Lines

         UTL_FILE.put_line (
            v_file,
            UPPER (
                  TRIM (cur_rec.party_site_number)
               || ','
               || TRIM (cur_rec.open_balance)
               || ','));

         l_count := l_count + 1;
      END LOOP;

      write_log ('Wrote ' || l_count || ' records to file');
      write_log ('Closing File Handler');
      g_loc := 'now closing in file ';
      --Closing the file handler
      UTL_FILE.fclose (v_file);

      retcode := '0';
      write_log ('Program Successfully completed');
   EXCEPTION
      WHEN UTL_FILE.invalid_path
      THEN
         UTL_FILE.fclose (v_file);
         errbuf := 'File Path is invalid.';
         retcode := '2';
         write_error (errbuf);
         write_log (errbuf);
      WHEN UTL_FILE.invalid_mode
      THEN
         UTL_FILE.fclose (v_file);
         errbuf := 'The open_mode parameter in FOPEN is invalid.';
         retcode := '2';
         write_error (errbuf);
         write_log (errbuf);
      WHEN UTL_FILE.invalid_filehandle
      THEN
         UTL_FILE.fclose (v_file);
         errbuf := 'File handle is invalid..';
         retcode := '2';
         write_error (errbuf);
         write_log (errbuf);
      WHEN UTL_FILE.invalid_operation
      THEN
         UTL_FILE.fclose (v_file);
         errbuf := 'File could not be opened or operated on as requested';
         retcode := '2';
         write_error (errbuf);
         write_log (errbuf);
      WHEN UTL_FILE.write_error
      THEN
         UTL_FILE.fclose (v_file);
         errbuf :=
            'Operating system error occurred during the write operation';
         retcode := '2';
         write_error (errbuf);
         write_log (errbuf);
      WHEN UTL_FILE.internal_error
      THEN
         UTL_FILE.fclose (v_file);
         write_log ('Unspecified PL/SQL error.');
      WHEN UTL_FILE.file_open
      THEN
         UTL_FILE.fclose (v_file);
         errbuf := 'The requested operation failed because the file is open.';
         retcode := '2';
         write_error (errbuf);
         write_log (errbuf);
      WHEN UTL_FILE.invalid_filename
      THEN
         UTL_FILE.fclose (v_file);
         errbuf := 'The filename parameter is invalid.';
         retcode := '2';
         write_error (errbuf);
         write_log (errbuf);
      WHEN UTL_FILE.access_denied
      THEN
         UTL_FILE.fclose (v_file);
         errbuf := 'Permission to access to the file location is denied.';
         retcode := '2';
         write_error (errbuf);
         write_log (errbuf);
      WHEN g_prog_exception
      THEN
         errbuf := l_err_msg;
         retcode := '2';
         write_error (l_err_msg);
         write_log (l_err_msg);
      WHEN OTHERS
      THEN
         -- UTL_FILE.fclose (v_file);
         l_err_msg := 'Error Msg :' || SQLERRM;
         errbuf := l_err_msg;
         retcode := '2';
         write_error (l_err_msg);
         write_log (l_err_msg);
   END generate_ars_file;
END XXWC_ARS_OPEN_BALANCE_PKG;
/