CREATE OR REPLACE PACKAGE BODY APPS.xxwc_om_bulk_so_print_pkg
as
/*************************************************************************
 Copyright (c) 2013 Lucidity Consulting Group
 All rights reserved.
**************************************************************************
  $Header xxwc_om_bulk_so_print_pkg$
  Module Name: xxwc_om_bulk_so_print_pkg.pkb

  PURPOSE:   This package is used for salesrep specific Bulk SO Printing

  REVISIONS:
  Ver        Date        Author                     Description
  ---------  ----------  ---------------         -------------------------
  1.0        06/06/2013  Consuelo Gonzalez       Initial Version TMS 20130604-00933
  2.0        01/03/2013  Raghavendra Velichetti return orders to print for XXWC OM Print Bulk print program--TMS # 20131230-00151 
  2.1        10/31/2014  Pattabhi Avula          TMS# 20141002-00064 Canada OU changes
**************************************************************************/

PROCEDURE   write_log   (p_message in varchar2)
IS
BEGIN
    dbms_output.put_line (p_message);
    fnd_file.put_line(fnd_file.log, p_message);
EXCEPTION
WHEN OTHERS THEN
    null;
END write_log;

PROCEDURE   write_output   (p_message in varchar2)
IS
BEGIN
    dbms_output.put_line (p_message);
    fnd_file.put_line(fnd_file.output, p_message);
EXCEPTION
WHEN OTHERS THEN
    null;
END write_output;


procedure   xxwc_om_bulk_so_print (RETCODE              OUT NUMBER
                                   , ERRMSG             OUT VARCHAR2
                                   , P_SALESREP_ID      IN  NUMBER
                                   , P_CREATION_DATE    IN VARCHAR2
                                   , P_DEFAULT_PRINTER  IN VARCHAR2)
IS
    l_conc_req_id       number := fnd_global.conc_request_id;
    l_error_message     clob;
    l_date              date;
    l_salesrep_name     varchar2(240);
    l_batch_id          number;
    l_group_id          number;
    l_request_id        number;
    l_count             number;
    
    cursor cur_order_list (l_salesrep_id number, l_creation_date date) is
            select  distinct oh.order_number
                    , oh.header_id
                    , oh.source_document_id
                    , oh.creation_date ord_creation_date
                    , ottt.name order_type
                    , oh.salesrep_id order_salesrep
                    , hcsua_ship.primary_salesrep_id ship_salesrep
                    , hcsua_bill.primary_salesrep_id bill_salesrep
                    , ol.ship_from_org_id
                    , mp.organization_code ship_from_org
                    , oh.ship_to_org_id
                    , oh.invoice_to_org_id
            from    apps.oe_order_headers oh
                    , apps.oe_order_lines ol
                    , oe_transaction_types_tl ottt
                    , mtl_parameters mp
                    , apps.hz_cust_site_uses hcsua_ship
                    , apps.hz_cust_site_uses hcsua_bill
            where   nvl(nvl(hcsua_ship.primary_salesrep_id, oh.salesrep_id), hcsua_bill.primary_salesrep_id) = l_salesrep_id
            and     trunc(oh.creation_date) = trunc(l_creation_date)
            and     nvl(oh.booked_flag,'N') = 'Y'
            and     nvl(oh.cancelled_flag, 'N') = 'N'
            and     oh.order_type_id = ottt.transaction_type_id
            and     ottt.language = 'US'
            and     ottt.name not in ('COUNTER ORDER', 'REPAIR ORDER')
            and     oh.header_id = ol.header_id
            and     nvl(ol.cancelled_flag, 'N') = 'N'
            and     ol.ship_from_org_id = mp.organization_Id
            and     oh.ship_to_org_id = hcsua_ship.site_use_id
            and     oh.org_id = hcsua_ship.org_id
            and     oh.invoice_to_org_id = hcsua_bill.site_use_id
            and     oh.org_id = hcsua_bill.org_id
            UNION
            select  distinct oh.order_number
                    , oh.header_id
                    , oh.source_document_id
                    , oh.creation_date ord_creation_date
                    , ottt.name order_type
                    , oh.salesrep_id order_salesrep
                    , hcsua_ship.primary_salesrep_id ship_salesrep
                    , hcsua_bill.primary_salesrep_id bill_salesrep
                    , oh.ship_from_org_id
                    , mp.organization_code ship_from_org
                    , oh.ship_to_org_id
                    , oh.invoice_to_org_id
            from    apps.oe_order_headers oh
                    , oe_transaction_types_tl ottt
                    , mtl_parameters mp
                    , apps.hz_cust_site_uses hcsua_ship
                    , apps.hz_cust_site_uses hcsua_bill
            where   nvl(nvl(hcsua_ship.primary_salesrep_id, oh.salesrep_id), hcsua_bill.primary_salesrep_id) = l_salesrep_id
            and     trunc(oh.creation_date) = trunc(l_creation_date)
            and     nvl(oh.booked_flag,'N') = 'Y'
            and     nvl(oh.cancelled_flag, 'N') = 'N'
            and     oh.order_type_id = ottt.transaction_type_id
            and     ottt.language = 'US'            
            and     ottt.name in ('COUNTER ORDER', 'REPAIR ORDER')
            and     oh.ship_from_org_id = mp.organization_Id
            and     oh.ship_to_org_id = hcsua_ship.site_use_id
            and     oh.org_id = hcsua_ship.org_id
            and     oh.invoice_to_org_id = hcsua_bill.site_use_id
            and     oh.org_id = hcsua_bill.org_id
            order by 1 desc;
    
BEGIN
    write_output('Starting Program to Print SO Documents.');
    
    l_salesrep_name := null;
    begin
        SELECT NVL (NVL (jrs.NAME, papf.full_name), jrre.resource_name)
        INTO l_salesrep_name
        FROM jtf_rs_salesreps_mo_v jrs, --V 2.1 Replaced the table jtf_rs_salesreps by pattabhi on 10/31/2014 for TMS# 20141002-00064
             jtf_rs_defresources_v jrre,
             per_all_people_f papf
       WHERE jrs.salesrep_id = P_SALESREP_ID
         AND jrs.resource_id = jrre.resource_id(+)
         AND jrs.person_id = papf.person_id(+)
         AND TRUNC (papf.effective_start_date(+)) <= TRUNC (SYSDATE)
         AND NVL (TRUNC (papf.effective_end_date(+)), TRUNC (SYSDATE)) >= TRUNC (SYSDATE);
    exception
    when others then
        write_log('Invalid salesrep Id: '||P_SALESREP_ID);
        l_salesrep_name := null;
    end;
    
    l_date := TO_DATE (P_CREATION_DATE, 'YYYY/MM/DD HH24:MI:SS');
    
    write_output('***************************************************************');
    write_output('Salesrep:          '||l_salesrep_name);
    write_output('Orders Created on: '||P_CREATION_DATE);
    write_output('To Printer:        '||P_DEFAULT_PRINTER);
    write_output('***************************************************************');
    
    l_count := 0;
    for c1 in cur_order_list (p_salesrep_id, l_date)
    loop
        exit when cur_order_list%notfound;
        l_batch_id          := null;
        l_group_id          := null;
        l_request_id        := null;
        
        write_output('Order Number: '||c1.order_number||' - Order Type: '||c1.order_type||' - Whse: '||c1.ship_from_org);
        
        select  xxwc_print_requests_s.nextval
        into    l_batch_id
        from    dual;
        
        select  xxwc_print_request_groups_s.nextval
        into    l_group_id
        from    dual;
        
        --write_output('l_batch_id: '||l_batch_id);
        --write_output('l_group_id: '||l_group_id);
        
        if c1.order_type = 'STANDARD ORDER' then
            
            INSERT INTO xxwc.xxwc_print_requests_temp (created_by
                                                     , creation_date
                                                     , last_updated_by
                                                     , last_update_date
                                                     , batch_id
                                                     , process_flag
                                                     , GROUP_ID
                                                     , application
                                                     , program
                                                     , description
                                                     , start_time
                                                     , sub_request
                                                     , printer
                                                     , style
                                                     , copies
                                                     , save_output
                                                     , print_together
                                                     , validate_printer
                                                     , template_appl_name
                                                     , template_code
                                                     , template_language
                                                     , template_territory
                                                     , output_format
                                                     , nls_language)
              VALUES   (fnd_global.user_id                  --created_by
                      , SYSDATE
                      , fnd_global.user_id                  --last_udated_by
                      , SYSDATE
                      , l_batch_id                          --BATCH_ID
                      , 1                                   --PROCESS_FLAG
                      , l_group_id                          --GROUP_ID
                      , 'XXWC'                              --APPLICATION
                      , 'XXWC_OM_PACK_SLIP_ALL'             --'XXWC_OM_PACK_SLIP'                 --PROGRAM
                      , 'XXWC OM Packing Slip - All Lines'  --'XXWC OM Packing Slip'              --DESCRIPTION
                      , NULL                                --START_TIME
                      , 'FALSE'                             --SUB_REQUEST
                      , P_DEFAULT_PRINTER                   --PRINTER
                      , NULL                                --STYLE
                      , 1                                   --COPIES
                      , 'TRUE'                              --SAVE_OUTPUT
                      , 'N'                                 --PRINT_TOGETHER
                      , 'RESOLVE'                           --VALIDATE_PRINTER
                      , 'XXWC'                              --TEMPLATE_APPL_NAME
                      , 'XXWC_OM_PACK_SLIP_ALL'             -- 'XXWC_OM_PACK_SLIP'                 --TEMPLATE_CODE
                      , 'en'                                --TEMPLATE_LANGUAGE
                      , 'US'                                --TEMPLATE_TERRITORY
                      , 'PDF'                               --OUTPUT_FORMAT
                      , 'en'                                --NLS_LANGUAGE
              );
        
            INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP values (l_batch_id,1,l_group_id,1,c1.ship_from_org_id);    -- Organization
            INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP values (l_batch_id,1,l_group_id,2,c1.header_id);           -- Order Number
            INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP values (l_batch_id,1,l_group_id,3,1);                      -- Print Pricing
            INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP values (l_batch_id,1,l_group_id,4,NULL);                   -- Reprint
            INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP values (l_batch_id,1,l_group_id,5,NULL);                   -- Print Kit Details
            INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP values (l_batch_id,1,l_group_id,6,2);                      -- Send Right Fax
            INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP values (l_batch_id,1,l_group_id,7,NULL);                   -- Fax Number
            INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP values (l_batch_id,1,l_group_id,8,NULL);                   -- Right Fax Comment
            INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP values (l_batch_id,1,l_group_id,9,1);                      -- Print Hazmat
            INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP values (l_batch_id,1,l_group_id,10,NULL);                  -- Delivery ID
        
        elsif c1.order_type in ( 'COUNTER ORDER','RETURN ORDER') then --Modified pacakage to include RETURN ORDER TMS # 20131230-00151 by RVELICHETTI
            
            INSERT INTO xxwc.xxwc_print_requests_temp (created_by
                                                     , creation_date
                                                     , last_updated_by
                                                     , last_update_date
                                                     , batch_id
                                                     , process_flag
                                                     , GROUP_ID
                                                     , application
                                                     , program
                                                     , description
                                                     , start_time
                                                     , sub_request
                                                     , printer
                                                     , style
                                                     , copies
                                                     , save_output
                                                     , print_together
                                                     , validate_printer
                                                     , template_appl_name
                                                     , template_code
                                                     , template_language
                                                     , template_territory
                                                     , output_format
                                                     , nls_language)
              VALUES   (fnd_global.user_id              --created_by
                      , SYSDATE
                      , fnd_global.user_id              --last_udated_by
                      , SYSDATE
                      , l_batch_id                      --BATCH_ID
                      , 1                               --PROCESS_FLAG
                      , l_group_id                      --GROUP_ID
                      , 'XXWC'                          --APPLICATION
                      , 'XXWC_OM_SRECEIPT'     --PROGRAM
                      , 'XXWC OM Sales Receipt Report'  --DESCRIPTION
                      , NULL                            --START_TIME
                      , 'FALSE'                         --SUB_REQUEST
                      , P_DEFAULT_PRINTER               --PRINTER
                      , NULL                            --STYLE
                      , 1                               --COPIES
                      , 'TRUE'                          --SAVE_OUTPUT
                      , 'N'                             --PRINT_TOGETHER
                      , 'RESOLVE'                       --VALIDATE_PRINTER
                      , 'XXWC'                          --TEMPLATE_APPL_NAME
                      , 'XXWC_OM_SRECEIPT'              --TEMPLATE_CODE
                      , 'en'                            --TEMPLATE_LANGUAGE
                      , 'US'                            --TEMPLATE_TERRITORY
                      , 'PDF'                           --OUTPUT_FORMAT
                      , 'en'                            --NLS_LANGUAGE
              );
              
            INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP values (l_batch_id,1,l_group_id,1,c1.header_id);   -- Order Number
            INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP values (l_batch_id,1,l_group_id,2,NULL);           -- Delivery Nname
            INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP values (l_batch_id,1,l_group_id,3,NULL);           -- Reprint
            INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP values (l_batch_id,1,l_group_id,4,NULL);           -- Print Kit Details
            INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP values (l_batch_id,1,l_group_id,5,2);              -- Send to Right Fax
            INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP values (l_batch_id,1,l_group_id,6,NULL);           -- Fax Number/Email
            INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP values (l_batch_id,1,l_group_id,7,NULL);           -- Right Fax Comment
            INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP values (l_batch_id,1,l_group_id,8,1);              -- Print Hazmat
                       
        elsif c1.order_type = 'REPAIR ORDER' then
            
            INSERT INTO xxwc.xxwc_print_requests_temp (created_by
                                                     , creation_date
                                                     , last_updated_by
                                                     , last_update_date
                                                     , batch_id
                                                     , process_flag
                                                     , GROUP_ID
                                                     , application
                                                     , program
                                                     , description
                                                     , start_time
                                                     , sub_request
                                                     , printer
                                                     , style
                                                     , copies
                                                     , save_output
                                                     , print_together
                                                     , validate_printer
                                                     , template_appl_name
                                                     , template_code
                                                     , template_language
                                                     , template_territory
                                                     , output_format
                                                     , nls_language)
              VALUES   (fnd_global.user_id                  --created_by
                      , SYSDATE
                      , fnd_global.user_id                  --last_udated_by
                      , SYSDATE
                      , l_batch_id                          --BATCH_ID
                      , 1                                   --PROCESS_FLAG
                      , l_group_id                          --GROUP_ID
                      , 'XXWC'                              --APPLICATION
                      , 'XXWC_CSD_REPAIR_RECEIPT'           --PROGRAM
                      , 'XXWC Repair Receipt Report'        --DESCRIPTION
                      , NULL                                --START_TIME
                      , 'FALSE'                             --SUB_REQUEST
                      , P_DEFAULT_PRINTER                   --PRINTER
                      , NULL                                --STYLE
                      , 1                                   --COPIES
                      , 'TRUE'                              --SAVE_OUTPUT
                      , 'N'                                 --PRINT_TOGETHER
                      , 'RESOLVE'                           --VALIDATE_PRINTER
                      , 'XXWC'                              --TEMPLATE_APPL_NAME
                      , 'XXWC_CSD_REPAIR_RECEIPT'           --TEMPLATE_CODE
                      , 'en'                                --TEMPLATE_LANGUAGE
                      , 'US'                                --TEMPLATE_TERRITORY
                      , 'PDF'                               --OUTPUT_FORMAT
                      , 'en'                                --NLS_LANGUAGE
              );
        
            INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP values (l_batch_id,1,l_group_id,1,c1.source_document_id);  -- Service Request Number
            INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP values (l_batch_id,1,l_group_id,2,NULL);                   -- Repair Order Number
            INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP values (l_batch_id,1,l_group_id,3,NULL);                   -- Receipt Start Date
            INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP values (l_batch_id,1,l_group_id,4,NULL);                   -- Receipt End Date
            INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP values (l_batch_id,1,l_group_id,5,NULL);                   -- Reprint
            INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP values (l_batch_id,1,l_group_id,6,NULL);                   -- Static Message
            INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP values (l_batch_id,1,l_group_id,7,2);                      -- Send to Right Fax
            INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP values (l_batch_id,1,l_group_id,8,NULL);                   -- Fax Number
            INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP values (l_batch_id,1,l_group_id,9,NULL);                   -- Rightfax Comment
        
        elsif c1.order_type in ('WC SHORT TERM RENTAL','WC LONG TERM RENTAL') then
            
            INSERT INTO xxwc.xxwc_print_requests_temp (created_by
                                                     , creation_date
                                                     , last_updated_by
                                                     , last_update_date
                                                     , batch_id
                                                     , process_flag
                                                     , GROUP_ID
                                                     , application
                                                     , program
                                                     , description
                                                     , start_time
                                                     , sub_request
                                                     , printer
                                                     , style
                                                     , copies
                                                     , save_output
                                                     , print_together
                                                     , validate_printer
                                                     , template_appl_name
                                                     , template_code
                                                     , template_language
                                                     , template_territory
                                                     , output_format
                                                     , nls_language)
              VALUES   (fnd_global.user_id                  --created_by
                      , SYSDATE
                      , fnd_global.user_id                  --last_udated_by
                      , SYSDATE
                      , l_batch_id                          --BATCH_ID
                      , 1                                   --PROCESS_FLAG
                      , l_group_id                          --GROUP_ID
                      , 'XXWC'                              --APPLICATION
                      , 'XXWC_OM_RENTAL_PS_RCT1'            --PROGRAM
                      , 'XXWC Rental Receipt Initial Report'--DESCRIPTION
                      , NULL                                --START_TIME
                      , 'FALSE'                             --SUB_REQUEST
                      , P_DEFAULT_PRINTER                   --PRINTER
                      , NULL                                --STYLE
                      , 1                                   --COPIES
                      , 'TRUE'                              --SAVE_OUTPUT
                      , 'N'                                 --PRINT_TOGETHER
                      , 'RESOLVE'                           --VALIDATE_PRINTER
                      , 'XXWC'                              --TEMPLATE_APPL_NAME
                      , 'XXWC_OM_RENTAL_PS_RCT1'            --TEMPLATE_CODE
                      , 'en'                                --TEMPLATE_LANGUAGE
                      , 'US'                                --TEMPLATE_TERRITORY
                      , 'PDF'                               --OUTPUT_FORMAT
                      , 'en'                                --NLS_LANGUAGE
              );
        
            INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP values (l_batch_id,1,l_group_id,1,c1.ship_from_org_id);    -- Organization
            INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP values (l_batch_id,1,l_group_id,2,c1.header_id);           -- Order Number
            INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP values (l_batch_id,1,l_group_id,3,1);                      -- Print Pricing
            INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP values (l_batch_id,1,l_group_id,4,NULL);                   -- Reprint
            INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP values (l_batch_id,1,l_group_id,5,NULL);                   -- Print Kit Details
            INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP values (l_batch_id,1,l_group_id,6,2);                      -- Send Right Fax
            INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP values (l_batch_id,1,l_group_id,7,NULL);                   -- Fax Number
            INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP values (l_batch_id,1,l_group_id,8,NULL);                   -- Right Fax Comment
            INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP values (l_batch_id,1,l_group_id,9,1);                      -- Print Hazmat
            INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP values (l_batch_id,1,l_group_id,10,NULL);                  -- Start Date
        
        else
            write_output('Undefined Order Type for this process');  
        end if;
        
        if c1.order_type in ('STANDARD ORDER', 'COUNTER ORDER', 'REPAIR ORDER', 'WC SHORT TERM RENTAL', 'WC LONG TERM RENTAL','RETURN ORDER') then  --return orders to print for XXWC OM Print Bulk print program--TMS # 20131230-00151 
            commit;      
            l_request_id := XXWC_ONT_ROUTINES_PKG.SUBMIT_PRINT_BATCH(l_batch_id,l_group_id,1,fnd_global.user_id,fnd_global.resp_id,fnd_global.resp_appl_id);
            --write_output('Request ID: '||l_request_id);
            write_output(' ');
            l_count := l_count + 1;
        end if;
        
    end loop;
    
    commit;
    
    write_output('Submitted '||l_count||' SO Print Jobs');
    
EXCEPTION
WHEN OTHERS THEN
    l_error_message := 'xxwc_qp_modifier_iface '
                || 'Error_Stack...'
                || DBMS_UTILITY.format_error_stack ()
                || ' Error_Backtrace...'
                || DBMS_UTILITY.format_error_backtrace ();

    xxcus_error_pkg.xxcus_error_main_api (
        p_called_from         => 'xxwc_om_bulk_so_print_pkg.xxwc_om_bulk_so_print'
       ,p_calling             => 'xxwc_om_bulk_so_print_pkg.xxwc_om_bulk_so_print'
       ,p_request_id          => l_conc_req_id
       ,p_ora_error_msg       => l_error_message
       ,p_error_desc          => 'Error running Pricing Modifier Interface'
       ,p_distribution_list   => 'HDSOracleDevelopers@hdsupply.com'
       ,p_module              => 'QP');

    write_log('Error in main xxwc_om_bulk_so_print_pkg. Error: '||l_error_message);
    
    ERRMSG := l_error_message;
    RETCODE := '2';
END xxwc_om_bulk_so_print;

end xxwc_om_bulk_so_print_pkg;
/