CREATE OR REPLACE PACKAGE BODY APPS.xxcuspn_alerts_pkg IS

  -- -----------------------------------------------------------------------------
  -- |----------------------------< pn_alert_main >----------------------------|
  -- -----------------------------------------------------------------------------
  --
  -- {Start Of Comments}
  --
  -- Description:
  --   Package to send emails for milestones in Oracle Property Manager.
  --
  --   VERSION DATE          AUTHOR(S)       DESCRIPTION
  -- ------- -----------   --------------- ---------------------------------
  -- 1.0     02-SEP-2009   Manny Rodriguez  Created this procedure.
  -- 1.1     05-MAY-2010   Luong vu         Added mail_error, l_distro_list2
  --                                        and l_err_lease (FRC 16904)
  -- 1.2     08-JUN-2010   Luong Vu         Add l_distro_list3 (RFC 17809) for substitution
  --                                        when primary_email is not found.
  -- 1.3     16-Nov-2010   Luong Vu         Commented out all DBMS output.
  --                                        Added COMMIT to all UPDATE of attributes
  --                                        13, 14, and 15 in alerts procedures.
  -- 2.0     17-May-2011   Luong Vu         Retrofit for R12
  -- ------------------------------------------------------------------------------

  l_err_msg  VARCHAR2(3000);
  l_err_code NUMBER;
  l_sec      VARCHAR2(255);
  l_fulltext VARCHAR2(7500);

  -- Error DEBUG
  l_err_callfrom  VARCHAR2(175) DEFAULT 'xxcusPN_ALERTS_PKG';
  l_err_callpoint VARCHAR2(175) DEFAULT 'START';

  l_err_lease VARCHAR2(500) DEFAULT 'LEASE';

  l_distro_list  VARCHAR2(175) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
  l_distro_list2 VARCHAR2(175) DEFAULT 'locations@hdsupply.com';
  l_distro_list3 VARCHAR2(175) DEFAULT 'locationdirectory@hdsupply.com'; --v1.2

  /*  l_distro_list  VARCHAR2(175) DEFAULT 'luong.vu@hdsupply.com';
    l_distro_list2 VARCHAR2(175) DEFAULT 'luong.vu@hdsupply.com';
    l_distro_list3 VARCHAR2(175) DEFAULT 'luong.vu@hdsupply.com'; --v1.2
  */
  program_error EXCEPTION;
  mail_error EXCEPTION;

  l_sender VARCHAR2(100);
  l_sid    VARCHAR2(10);

  l_hostname   VARCHAR2(200);
  l_hostport   VARCHAR2(20);
  l_default_cc VARCHAR2(200);

  PROCEDURE pn_alert_main(errbuf  OUT VARCHAR2
                         ,retcode OUT NUMBER) IS
    -- -----------------------------------------------------------------------------
    -- |----------------------------< pn_alert_options >----------------------------|
    -- -----------------------------------------------------------------------------
    --
    -- {Start Of Comments}
    --
    -- Description:
    --   This is the main alert procedure.
    --
    --   VERSION DATE          AUTHOR(S)       DESCRIPTION
    -- ------- -----------   --------------- ---------------------------------
    -- 1.0     09-SEP-2009   Manny         Created this procedure.
    -- 1.1     03-FEB-2009   Luong Vu      Add Pending Completion SR27466
    -- 1.2     01-MAR-2009   Luong Vu      SR34153
    -- 1.3     27-FEB-2010   Luong Vu      Add new clean up old email date for pending - SR37847
    --                                     completion
    --                                     Add mail_error (FRC 16904)
  BEGIN
    -- Initialize emails
    l_err_callpoint := 'Starting Main Alert Program';
  
    SELECT lower(NAME)
      INTO l_sid
      FROM v$database;
    l_sender := 'Oracle.Applications_' || l_sid || '@hdsupply.com';
  
    SELECT lv.description
      INTO l_hostname
      FROM applsys.fnd_lookup_values lv
     WHERE lv.lookup_type = 'HDS_HDS_PN_ALERTS'
       AND lv.lookup_code = 'SMTP_HOST';
  
    SELECT lv.description
      INTO l_hostport
      FROM applsys.fnd_lookup_values lv
     WHERE lv.lookup_type = 'HDS_HDS_PN_ALERTS'
       AND lv.lookup_code = 'SMTP_PORT';
  
    SELECT description
      INTO l_default_cc
      FROM fnd_lookup_values
     WHERE lookup_type = 'HDS_OPN_NOTIFICATIONS'
       AND lookup_code = 'CCGEN';
  
    --DBMS_OUTPUT.ENABLE (100000);
    -- clean up old email dates trailing in mini loop
    l_err_callpoint := 'Update #1 in Main Alert Program';
    BEGIN
      UPDATE pn.pn_lease_milestones_all
         SET attribute15 = NULL
       WHERE lease_milestone_id IN
             (SELECT lease_milestone_id
                FROM pn.pn_lease_milestones_all
               WHERE to_date(attribute15, 'MM-DD-YYYY') + every_days <=
                     SYSDATE);
    
      COMMIT;
    
    EXCEPTION
      WHEN OTHERS THEN
        RAISE program_error;
    END;
  
    -- clean up old emails dates for pending commencement when no longer in pending commencement status.
    BEGIN
      l_err_callpoint := 'Update #2 in Main Alert Program';
      UPDATE pn.pn_lease_details_all
         SET attribute14 = NULL
       WHERE lease_id IN (SELECT lease.lease_id
                            FROM pn.pn_leases_all        lease
                                ,pn.pn_lease_details_all lse
                           WHERE lse.attribute14 IS NOT NULL
                             AND lease.lease_status <> 'LOF');
      --               AND lease.lease_status not in ('LOF','SGN'));   -- v1.2
      COMMIT;
    
    EXCEPTION
      WHEN OTHERS THEN
        RAISE program_error;
    END;
  
    -- clean up old emails dates for pending completion when no longer in pending completion status.
    -- v1.3
    BEGIN
      l_err_callpoint := 'Update #2 in Main Alert Program';
      UPDATE pn.pn_lease_details_all
         SET attribute13 = NULL
       WHERE lease_id IN (SELECT lease.lease_id
                            FROM pn.pn_leases_all        lease
                                ,pn.pn_lease_details_all lse
                           WHERE lse.attribute13 IS NOT NULL
                             AND lease.lease_status <> 'SGN');
    
      COMMIT;
    
    EXCEPTION
      WHEN OTHERS THEN
        RAISE program_error;
    END;
  
    -- clean up old email dates when action date is changed.  Basically the stored email
    -- date should always be between the milestone date+lead days and current date
    BEGIN
      l_err_callpoint := 'Update #3 in Main Alert Program';
      UPDATE pn.pn_lease_milestones_all
         SET attribute15 = NULL
       WHERE lease_milestone_id IN (
                                    
                                    SELECT lease_milestone_id
                                      FROM pn.pn_lease_milestones_all
                                     WHERE to_date(attribute15, 'MM-DD-YYYY') <=
                                           (milestone_date - lead_days));
      COMMIT;
    EXCEPTION
      WHEN OTHERS THEN
        RAISE program_error;
    END;
  
    -- Call the email procedures.
    pn_alert_options;
    pn_alert_pending;
    pn_alert_pending_completion; -- v1.1
    pn_alert_expir;
  
    -- EXCEPTIONS
  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;
      l_err_code := 2;
      l_sec      := 'Error in ' || l_err_callpoint;
      l_err_msg  := l_sec;
      l_err_msg  := l_err_msg || ' WITH  ' || substr(SQLERRM, 1, 1000);
      l_fulltext := l_err_msg;
      --dbms_output.put_line(l_fulltext);
      fnd_file.put_line(fnd_file.log, l_fulltext);
      fnd_file.put_line(fnd_file.output, l_fulltext);
      retcode := l_err_code;
      errbuf  := l_err_msg;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_err_callfrom,
                                           p_calling => l_err_callpoint,
                                           p_ora_error_msg => SQLERRM,
                                           p_error_desc => 'Error running Alerts package with PROGRAM ERROR',
                                           p_distribution_list => l_distro_list,
                                           p_module => 'PN');
    
    WHEN mail_error THEN
      --lv
      --ROLLBACK;
      l_err_code := 2;
      l_sec      := 'Error in ' || l_err_callpoint;
      l_err_msg  := l_sec;
      l_err_msg  := l_err_msg || ' WITH  ' || substr(SQLERRM, 1, 1000);
      l_fulltext := l_err_msg;
      --dbms_output.put_line(l_fulltext);
      fnd_file.put_line(fnd_file.log, l_fulltext);
      fnd_file.put_line(fnd_file.output, l_fulltext);
      retcode := l_err_code;
      errbuf  := l_err_msg;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_err_callfrom,
                                           p_calling => l_err_callpoint,
                                           p_ora_error_msg => SQLERRM,
                                           p_error_desc => l_err_lease,
                                           p_distribution_list => l_distro_list,
                                           p_module => 'PN');
      -- Also send to Property Distribution group
      l_err_code := 2;
      l_sec      := 'Error in ' || l_err_callpoint;
      l_err_msg  := l_sec;
      l_err_msg  := l_err_msg || ' WITH  ' || substr(SQLERRM, 1, 1000);
      l_fulltext := l_err_msg;
      --dbms_output.put_line(l_fulltext);
      fnd_file.put_line(fnd_file.log, l_fulltext);
      fnd_file.put_line(fnd_file.output, l_fulltext);
      retcode := l_err_code;
      errbuf  := l_err_msg;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_err_callfrom,
                                           p_calling => l_err_callpoint,
                                           p_ora_error_msg => SQLERRM,
                                           p_error_desc => l_err_lease,
                                           p_distribution_list => l_distro_list2,
                                           p_module => 'PN');
    
    WHEN OTHERS THEN
      ROLLBACK;
      l_err_code := 2;
      l_sec      := 'Error in ' || l_err_callpoint;
      l_err_msg  := l_sec;
      l_err_msg  := l_err_msg || ' WITH  ' || substr(SQLERRM, 1, 1000);
      l_fulltext := l_err_msg;
      --dbms_output.put_line(l_fulltext);
      fnd_file.put_line(fnd_file.log, l_fulltext);
      fnd_file.put_line(fnd_file.output, l_fulltext);
      retcode := l_err_code;
      errbuf  := l_err_msg;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_err_callfrom,
                                           p_calling => l_err_callpoint,
                                           p_ora_error_msg => SQLERRM,
                                           p_error_desc => 'Error running Alerts package with PROGRAM ERROR',
                                           p_distribution_list => l_distro_list,
                                           p_module => 'PN');
    
  END pn_alert_main;

  PROCEDURE pn_alert_options IS
    -- -----------------------------------------------------------------------------
    -- |----------------------------< pn_alert_options >----------------------------|
    -- -----------------------------------------------------------------------------
    --
    -- {Start Of Comments}
    --
    -- Description:
    --   This is the Alert to call options (insurance, payment, options) .
    --
    --   VERSION DATE          AUTHOR(S)       DESCRIPTION
    -- ------- -----------   --------------- ---------------------------------
    -- 1.0     09-SEP-2009   Manny         Created this procedure.
  
  BEGIN
  
    --****************************************************************************************************--
    --  START  MAIN PROGRAM
    --****************************************************************************************************--
  
    l_err_callpoint := 'pn_alerts_option - Starting Main Loop';
    <<main_loop>>
    FOR c_main IN (
                   --main sql
                   SELECT mile.lease_id
                          ,mile.milestone_type_code
                          ,lease.lease_num
                          ,look1.description opt_description
                          ,look2.description pay_description
                          ,look3.description ins_description
                          ,mile.option_id
                          ,mile.insurance_requirement_id
                          ,mile.payment_term_id
                          ,nvl(look1.description,
                               nvl(look2.description, look3.description)) option_name
                          ,opt.option_status_lookup_code
                          ,opt.option_type_code
                          ,lease.name rer_name
                          ,look.description rer_type
                          ,nvl(lsedt.attribute8, 'No document link found') documentum_link
                     FROM pn.pn_lease_milestones_all mile
                          ,pn.pn_leases_all lease
                          ,pn.pn_options_all opt
                          ,pn.pn_insurance_requirements_all ins
                          ,pn.pn_payment_terms_all pay
                          ,pn.pn_lease_details_all lsedt
                          ,(SELECT description
                                  ,lookup_code
                              FROM fnd_lookup_values
                             WHERE lookup_type = 'PN_LEASE_OPTION_TYPE') look1
                          ,(SELECT description
                                  ,lookup_code
                              FROM fnd_lookup_values
                             WHERE lookup_type = 'PN_PAYMENT_PURPOSE_TYPE') look2
                          ,(SELECT description
                                  ,lookup_code
                              FROM fnd_lookup_values
                             WHERE lookup_type = 'PN_INSURANCE_TYPE') look3
                          ,(SELECT description
                                  ,lookup_code
                              FROM fnd_lookup_values
                             WHERE lookup_type = 'PN_LEASE_TYPE') look
                    WHERE mile.lease_id = lease.lease_id
                      AND mile.option_id = opt.option_id(+)
                      AND mile.insurance_requirement_id =
                          ins.insurance_requirement_id(+)
                      AND mile.payment_term_id = pay.payment_term_id(+)
                      AND opt.option_type_code = look1.lookup_code(+)
                      AND ins.insurance_type_lookup_code =
                          look3.lookup_code(+)
                      AND pay.payment_purpose_code = look2.lookup_code(+)
                      AND lease.lease_id = lsedt.lease_id
                      AND lease.lease_type_code = look.lookup_code
                      AND nvl(look1.description,
                              nvl(look2.description, look3.description)) IS NOT NULL -- is null to find optionless milestones
                    GROUP BY mile.milestone_type_code
                             ,mile.lease_id
                             ,lease.lease_num
                             ,look1.description
                             ,look2.description
                             ,look3.description
                             ,mile.option_id
                             ,mile.insurance_requirement_id
                             ,mile.payment_term_id
                             ,nvl(look1.description,
                                  nvl(look2.description, look3.description))
                             ,opt.option_status_lookup_code
                             ,opt.option_type_code
                             ,lease.name
                             ,look.description
                             ,nvl(lsedt.attribute8, 'No document link found')
                    ORDER BY mile.lease_id
                             ,mile.milestone_type_code
                             ,lease.lease_num
                             ,look1.description
                             ,look2.description
                             ,look3.description
                             ,mile.option_id
                             ,mile.insurance_requirement_id
                             ,mile.payment_term_id)
    LOOP
    
      --Start Inner Loop
      l_err_callpoint := 'pn_alerts_option - Starting Option Loop';
      <<option_loop>>
      FOR c_option IN (SELECT lease_milestone_id
                             ,milestone_date
                             ,milestone_date - lead_days begin_date
                             ,decode(attribute15, NULL,
                                     (milestone_date - lead_days),
                                     to_date(attribute15, 'MM-DD-YYYY') +
                                      every_days) next_email_date
                             ,lead_days
                             ,every_days frequency
                             ,to_date(attribute15, 'MM-DD-YYYY') email_date
                         FROM pn.pn_lease_milestones_all
                        WHERE lease_id = c_main.lease_id
                          AND milestone_type_code =
                              c_main.milestone_type_code
                          AND nvl(option_id,
                                  nvl(payment_term_id,
                                       nvl(insurance_requirement_id, 1))) =
                              nvl(c_main.option_id,
                                  nvl(c_main.payment_term_id,
                                       nvl(c_main.insurance_requirement_id, 1)))
                        ORDER BY lead_days ASC)
      
      LOOP
      
        --POP OUT of lower loop if the following reasons occur
      
        --If this milestone date is in the past
        EXIT option_loop WHEN c_option.milestone_date < SYSDATE;
      
        --If this an OPTION milestone and no longer open
        EXIT option_loop WHEN c_main.option_type_code IN('RENEW',
                                                         'AUTORENEW', 'TERM') AND nvl(c_main.option_status_lookup_code,
                                                                                      'x') <> 'OPEN';
      
        EXIT option_loop WHEN SYSDATE BETWEEN c_option.email_date AND c_option.next_email_date;
      
        /* dbms_output.put_line('evaluating ' || c_option.lease_milestone_id ||
        ' for ' || c_main.lease_num);*/
      
        IF SYSDATE >= c_option.next_email_date THEN
          --send email function
          l_err_callpoint := 'pn_alerts_option - Sending email';
          /* dbms_output.put_line('sending email for ' ||
          c_option.lease_milestone_id || ' for ' ||
          c_main.lease_num);*/
          options_mail(p_sender => l_sender,
                       p_milestone_id => c_option.lease_milestone_id,
                       p_lease_number => c_main.lease_num,
                       p_optname => c_main.option_name,
                       p_rer_type => c_main.rer_type,
                       p_rer_name => c_main.rer_name,
                       p_documentum_link => c_main.documentum_link);
        
          -- update table attribute 15
          l_err_callpoint := 'pn_alerts_option - Updating email_date';
        
          UPDATE pn.pn_lease_milestones_all
             SET attribute15 = to_char(SYSDATE, 'MM-DD-YYYY')
           WHERE lease_milestone_id = c_option.lease_milestone_id;
          COMMIT;
          EXIT option_loop;
        
        END IF;
      END LOOP option_loop;
    END LOOP main_loop;
  END pn_alert_options;

  PROCEDURE pn_alert_pending IS
    -- -----------------------------------------------------------------------------
    -- |----------------------------< pn_alert_pending >----------------------------|
    -- -----------------------------------------------------------------------------
    --
    -- {Start Of Comments}
    --
    -- Description:
    --   This is the Alert to send emails when pending commencement status.
    --   That variable is set in the initialization just below.
    --
    --   VERSION DATE          AUTHOR(S)       DESCRIPTION
    -- ------- -----------   --------------- ---------------------------------
    -- 1.0     09-SEP-2009   Manny         Created this procedure.
  
    -- Intitalize
    l_status VARCHAR2(100) DEFAULT 'PENDING_COMMENCEMENT';
  
  BEGIN
  
    --****************************************************************************************************--
    --  START  MAIN PROGRAM
    --****************************************************************************************************--
  
    l_err_callpoint := 'pn_alert_pending - Starting Main Loop';
    <<main_loop>>
    FOR c_main IN (
                   --main sql
                   SELECT lease.lease_id
                          ,mile.milestone_type_code
                          ,lease.lease_num
                     FROM pn.pn_set_milestones mile
                          ,pn.pn_leases_all     lease
                    WHERE mile.milestone_type_code = l_status
                      AND lease.lease_status = 'LOF'
                    GROUP BY mile.milestone_type_code
                             ,lease.lease_id
                             ,lease.lease_num
                    ORDER BY lease.lease_id
                             ,mile.milestone_type_code
                             ,lease.lease_num
                   
                   )
    LOOP
    
      l_err_callpoint := 'pn_alert_pending - Starting Option Loop';
      <<option_loop>>
      FOR c_option IN (SELECT mile.milestones_set_id
                             ,SYSDATE milestone_date
                             ,SYSDATE - mile.lead_days begin_date
                             ,decode(lease.attribute14, NULL, (SYSDATE),
                                     to_date(lease.attribute14, 'MM-DD-YYYY') +
                                      mile.frequency) next_email_date
                             ,mile.lead_days
                             ,mile.frequency
                             ,to_date(lease.attribute14, 'MM-DD-YYYY') email_date
                             ,lse.name rer_name
                             ,nvl(lease.attribute8, 'No document link found') documentum_link
                         FROM pn.pn_set_milestones    mile
                             ,pn.pn_lease_details_all lease
                             ,pn.pn_leases_all        lse
                        WHERE lease.lease_id = c_main.lease_id
                          AND mile.milestone_type_code =
                              c_main.milestone_type_code
                          AND lease.lease_id = lse.lease_id
                        ORDER BY mile.lead_days ASC)
      
      LOOP
      
        EXIT option_loop WHEN SYSDATE BETWEEN c_option.email_date AND c_option.next_email_date;
      
        --dbms_output.put_line('evaluating '||c_main.lease_num);
        IF SYSDATE >= c_option.next_email_date THEN
        
          --send email function
          l_err_callpoint := 'pn_alert_pending - Sending email';
          --dbms_output.put_line('sending email for ' || c_main.lease_num);
        
          pend_mail(p_sender => l_sender,
                    p_lease_number => c_main.lease_num,
                    p_lease_id => c_main.lease_id,
                    p_milestone_type => c_main.milestone_type_code,
                    p_rer_name => c_option.rer_name,
                    p_documentum_link => c_option.documentum_link);
        
          -- update table attribute 14
          l_err_callpoint := 'pn_alert_pending - Updating email_date';
        
          UPDATE pn.pn_lease_details_all
             SET attribute14 = to_char(SYSDATE, 'MM-DD-YYYY')
           WHERE lease_id = c_main.lease_id;
          COMMIT;
          EXIT option_loop;
        
        END IF;
      END LOOP option_loop;
    END LOOP main_loop;
  END pn_alert_pending;

  PROCEDURE pn_alert_pending_completion IS
    -- -----------------------------------------------------------------------------
    -- |----------------------------< pn_alert_pending_completion >-----------------|
    -- -----------------------------------------------------------------------------
    --
    -- {Start Of Comments}
    --
    -- Description:
    --   This is the Alert to send emails when pending commencement status.
    --   That variable is set in the initialization just below.
    --
    --   VERSION DATE          AUTHOR(S)       DESCRIPTION
    -- ------- -----------   --------------- ---------------------------------
    -- 1.0     03-FEB-2009   Luong Vu         Created this procedure. SR27466
    -- 1.1     27-FEB-2010   Luong Vu         Change attribute14 to attribute13 - SR37847
    --                                        (FRC 16904)
  
    -- Intitalize
    l_status VARCHAR2(100) DEFAULT 'PENDING_COMPLETION';
  
  BEGIN
  
    --****************************************************************************************************--
    --  START  MAIN PROGRAM
    --****************************************************************************************************--
  
    l_err_callpoint := 'pn_alert_pending_completion - Starting Main Loop';
    <<main_loop>>
    FOR c_main IN (
                   --main sql
                   SELECT lease.lease_id
                          ,mile.milestone_type_code
                          ,lease.lease_num
                     FROM pn.pn_set_milestones mile
                          ,pn.pn_leases_all     lease
                    WHERE mile.milestone_type_code = l_status
                      AND lease.lease_status = 'SGN'
                    GROUP BY mile.milestone_type_code
                             ,lease.lease_id
                             ,lease.lease_num
                    ORDER BY lease.lease_id
                             ,mile.milestone_type_code
                             ,lease.lease_num
                   
                   )
    LOOP
    
      l_err_callpoint := 'pn_alert_pending_completion - Starting Option Loop';
      <<option_loop>>
      FOR c_option IN (SELECT mile.milestones_set_id
                             ,SYSDATE milestone_date
                             ,SYSDATE - mile.lead_days begin_date
                              --,decode(lease.attribute14
                             ,decode(lease.attribute13 -- SR37847
                                    , NULL, (SYSDATE),
                                     to_date(lease.attribute13, 'MM-DD-YYYY') +
                                      mile.frequency) next_email_date
                             ,mile.lead_days
                             ,mile.frequency
                              --,to_date(lease.attribute14, 'MM-DD-YYYY') email_date
                             ,to_date(lease.attribute13, 'MM-DD-YYYY') email_date -- SR37847
                             ,lse.name rer_name
                             ,nvl(lease.attribute8, 'No document link found') documentum_link
                         FROM pn.pn_set_milestones    mile
                             ,pn.pn_lease_details_all lease
                             ,pn.pn_leases_all        lse
                        WHERE lease.lease_id = c_main.lease_id
                          AND mile.milestone_type_code =
                              c_main.milestone_type_code
                          AND lease.lease_id = lse.lease_id
                        ORDER BY mile.lead_days ASC)
      
      LOOP
      
        EXIT option_loop WHEN SYSDATE BETWEEN c_option.email_date AND c_option.next_email_date;
      
        --dbms_output.put_line('evaluating '||c_main.lease_num);
        IF SYSDATE >= c_option.next_email_date THEN
        
          --send email function
          l_err_callpoint := 'pn_alert_pending_completion - Sending email';
          --dbms_output.put_line('sending email for ' || c_main.lease_num);
        
          pend_mail2(p_sender => l_sender,
                     p_lease_number => c_main.lease_num,
                     p_lease_id => c_main.lease_id,
                     p_milestone_type => c_main.milestone_type_code,
                     p_rer_name => c_option.rer_name,
                     p_documentum_link => c_option.documentum_link);
        
          -- update table attribute 14
          l_err_callpoint := 'pn_alert_pending_completion - Updating email_date';
        
          UPDATE pn.pn_lease_details_all
          --SET attribute14 = to_char(SYSDATE, 'MM-DD-YYYY')
             SET attribute13 = to_char(SYSDATE, 'MM-DD-YYYY') -- SR37847
           WHERE lease_id = c_main.lease_id;
          COMMIT;
          EXIT option_loop;
        
        END IF;
      END LOOP option_loop;
    END LOOP main_loop;
  END pn_alert_pending_completion;

  PROCEDURE pn_alert_expir IS
    -- -----------------------------------------------------------------------------
    -- |----------------------------< pn_alert_expir >----------------------------|
    -- -----------------------------------------------------------------------------
    --
    -- {Start Of Comments}
    --
    -- Description:
    --   This is the Alert to send emails when nearing expiration status.
    --
    --   VERSION DATE          AUTHOR(S)       DESCRIPTION
    -- ------- -----------   --------------- ---------------------------------
    -- 1.0     09-SEP-2009   Manny         Created this procedure.
    -- 1.1     05/05/10      Luong Vu      Mod decode to include the case where the expiration date
    --                                     changed after the initial alert was sent. (FRC 16904)
  
    -- Intitalize
    l_status VARCHAR2(100) DEFAULT 'EXPIRATION';
  
  BEGIN
  
    --****************************************************************************************************--
    --  START  MAIN PROGRAM
    --****************************************************************************************************--
  
    l_err_callpoint := 'pn_alert_expir - Starting Main Loop';
    <<main_loop>>
    FOR c_main IN (
                   --main sql
                   SELECT lease.lease_id
                          ,mile.milestone_type_code
                          ,leases.lease_num
                     FROM pn.pn_set_milestones    mile
                          ,pn.pn_leases_all        leases
                          ,pn.pn_lease_details_all lease
                    WHERE mile.milestone_type_code = l_status
                      AND leases.lease_id = lease.lease_id
                    GROUP BY mile.milestone_type_code
                             ,lease.lease_id
                             ,leases.lease_num
                    ORDER BY lease.lease_id
                             ,mile.milestone_type_code
                             ,leases.lease_num
                   
                   )
    LOOP
    
      l_err_callpoint := 'pn_alert_expir - Starting Option Loop';
      <<option_loop>>
      FOR c_option IN (SELECT mile.milestones_set_id
                             ,nvl(lease.lease_extension_end_date,
                                  lease.lease_termination_date) milestone_date
                             ,nvl(lease.lease_extension_end_date,
                                  lease.lease_termination_date) - SYSDATE begin_date
                              /*                            ,decode(lease.attribute15        v1.1
                              ,NULL
                              ,(nvl(lease.lease_extension_end_date
                                   ,lease.lease_termination_date) -
                               mile.lead_days)
                              ,to_date(lease.attribute15
                                      ,'MM-DD-YYYY') + mile.frequency) next_email_date*/
                             ,CASE
                                WHEN lease.attribute15 IS NULL THEN
                                 nvl(lease.lease_extension_end_date,
                                     lease.lease_termination_date) -
                                 mile.lead_days
                                WHEN lease.attribute15 IS NOT NULL
                                     AND nvl(lease.lease_extension_end_date,
                                             lease.lease_termination_date) -
                                     SYSDATE > mile.lead_days THEN
                                 nvl(lease.lease_extension_end_date,
                                     lease.lease_termination_date) -
                                 mile.lead_days
                                ELSE
                                 to_date(lease.attribute15, 'MM-DD-YYYY') +
                                 mile.frequency
                              END next_email_date
                             ,mile.lead_days
                             ,mile.frequency
                             ,to_date(lease.attribute15, 'MM-DD-YYYY') email_date
                             ,look.description rer_type
                             ,lse.name rer_name
                             ,nvl(lease.attribute8, 'No document link found') documentum_link
                         FROM pn.pn_set_milestones mile
                             ,pn.pn_lease_details_all lease
                             ,pn.pn_leases_all lse
                             ,(SELECT description
                                     ,lookup_code
                                 FROM fnd_lookup_values
                                WHERE lookup_type = 'PN_LEASE_TYPE') look
                        WHERE lease.lease_id = c_main.lease_id
                          AND mile.milestone_type_code =
                              c_main.milestone_type_code
                          AND lease.lease_id = lse.lease_id
                          AND lse.lease_type_code = look.lookup_code
                        ORDER BY mile.lead_days ASC)
      
      LOOP
      
        --POP out of lower loop if the following reasons occur
      
        --If this milestone date is in the past
        EXIT WHEN c_option.milestone_date <= SYSDATE;
      
        EXIT option_loop WHEN SYSDATE BETWEEN c_option.email_date AND c_option.next_email_date;
      
        --  dbms_output.put_line('evaluating for '||c_main.lease_num);
      
        IF SYSDATE >= c_option.next_email_date THEN
          --send email function
          l_err_callpoint := 'pn_alert_expir - Sending email';
          --dbms_output.put_line('sending email for ' || c_main.lease_num);
          expir_mail(p_sender => l_sender,
                     p_lease_number => c_main.lease_num,
                     p_milestone_type => c_main.milestone_type_code,
                     p_lease_id => c_main.lease_id,
                     p_rer_name => c_option.rer_name,
                     p_documentum_link => c_option.documentum_link,
                     p_rer_type => c_option.rer_type,
                     p_milestone_date => c_option.milestone_date);
        
          -- update table attribute 15
          l_err_callpoint := 'pn_alert_expir - Updating email_date';
        
          UPDATE pn.pn_lease_details_all
             SET attribute15 = to_char(SYSDATE, 'MM-DD-YYYY')
           WHERE lease_id = c_main.lease_id;
          COMMIT;
          EXIT option_loop;
        
        END IF;
      END LOOP option_loop;
    END LOOP main_loop;
  END pn_alert_expir;

  PROCEDURE options_mail(p_sender          VARCHAR2
                        ,p_milestone_id    NUMBER
                        ,p_lease_number    VARCHAR2
                        ,p_optname         VARCHAR2
                        ,p_rer_type        VARCHAR2
                        ,p_rer_name        VARCHAR2
                        ,p_documentum_link VARCHAR2) IS
    -- -----------------------------------------------------------------------------
    -- |----------------------------< options_mail >----------------------------|
    -- -----------------------------------------------------------------------------
    --
    -- {Start Of Comments}
    --
    -- Description:
    --   This is the email procedure called by pn_alert_options.
    --
    --   VERSION DATE          AUTHOR(S)       DESCRIPTION
    -- ------- -----------   --------------- ---------------------------------
    -- 1.0     09-SEP-2009   Manny         Created this procedure.
    -- 1.1     27-APR-2010   Luong Vu      Changed subject line SR-37847
    --                                     Added error handling for mailing failure - mail_error
    --                                     (FRC 16904)
    -- 1.2     08-JUN-2010   Luong Vu      Mod primary_email to default to locationdirectory
    --                                     (RFC 17809)
    -- 1.3     02-AUG-2010   Luong Vu      mod of v1.2
  
    -- Java Mailer
    l_subject     VARCHAR2(32767) DEFAULT NULL; -- v1.1
    l_body        VARCHAR2(32767) DEFAULT NULL;
    l_body_header VARCHAR2(32767) DEFAULT NULL;
    l_body_detail VARCHAR2(32767) DEFAULT NULL;
    l_body_footer VARCHAR2(32767) DEFAULT NULL;
  
    -- local values
    l_primary_email  VARCHAR2(250) DEFAULT NULL;
    l_cc_email       VARCHAR2(250) DEFAULT NULL;
    l_milestone_type VARCHAR2(250) DEFAULT NULL;
    l_milestone_date VARCHAR2(50) DEFAULT NULL;
    l_timeline       VARCHAR2(50) DEFAULT NULL;
  
  BEGIN
  
    BEGIN
    
      l_err_callpoint := 'options_mail - main query';
      SELECT /*nvl(look.description
                                                                            ,(SELECT look.description
                                                                             FROM pn.pn_lease_details_all lse
                                                                                 ,gl.gl_code_combinations glc
                                                                                 ,(SELECT description, lookup_code, enabled_flag
                                                                                   FROM fnd_lookup_values
                                                                                   WHERE lookup_type = 'HDS_OPN_NOTIFICATIONS') look
                                                                             WHERE lse.lease_id = lease.lease_id
                                                                             AND nvl(lse.receivable_account_id,lse.expense_account_id) = glc.code_combination_id
                                                                             AND look.lookup_code LIKE glc.segment1 || '%'
                                                                             AND substr(look.lookup_code
                                                                                      ,length(look.lookup_code) - 1
                                                                                      ,2) <> 'CC'
                                                                             AND look.enabled_flag = 'Y')) primary_email*/
      /*              nvl2(look.description                                          --v1.2
      ,(SELECT look.description
       FROM pn.pn_lease_details_all lse
           ,gl.gl_code_combinations glc
           ,(SELECT description, lookup_code, enabled_flag
             FROM fnd_lookup_values
             WHERE lookup_type = 'HDS_OPN_NOTIFICATIONS') look
       WHERE lse.lease_id = lease.lease_id
       AND nvl(lse.receivable_account_id,lse.expense_account_id) = glc.code_combination_id
       AND look.lookup_code LIKE glc.segment1 || '%'
       AND substr(look.lookup_code
                ,length(look.lookup_code) - 1
                ,2) <> 'CC'
       AND look.enabled_flag = 'Y'),l_distro_list3) primary_email       */
      
       nvl((SELECT look.description -- v1.3
             FROM pn.pn_lease_details_all lse
                 ,gl.gl_code_combinations glc
                 ,(SELECT description
                         ,lookup_code
                         ,enabled_flag
                     FROM apps.fnd_lookup_values
                    WHERE lookup_type = 'HDS_OPN_NOTIFICATIONS') look
            WHERE lse.lease_id = lease.lease_id
              AND nvl(lse.receivable_account_id, lse.expense_account_id) =
                  glc.code_combination_id
              AND look.lookup_code LIKE glc.segment1 || '%'
              AND substr(look.lookup_code, length(look.lookup_code) - 1, 2) <> 'CC'
              AND look.enabled_flag = 'Y'), l_distro_list3) primary_email
      ,nvl(look2.description, '') cc_email
      ,look3.description milestone_type
      ,initcap(to_char(milestone_date, 'MON-DD-YYYY'))
      ,to_char(trunc(milestone_date - SYSDATE)) || ' Days ' timeline
        INTO l_primary_email
            ,l_cc_email
            ,l_milestone_type
            ,l_milestone_date
            ,l_timeline
        FROM pn.pn_lease_milestones_all lease
            ,(SELECT description
                    ,lookup_code
                FROM fnd_lookup_values
               WHERE lookup_type = 'HDS_OPN_NOTIFICATIONS') look
            ,(SELECT description
                    ,lookup_code
                FROM fnd_lookup_values
               WHERE lookup_type = 'HDS_OPN_NOTIFICATIONS') look2
            ,(SELECT description
                    ,lookup_code
                FROM fnd_lookup_values
               WHERE lookup_type = 'PN_MILESTONES_TYPE') look3
       WHERE lease.lease_milestone_id = p_milestone_id
         AND lease.attribute1 = look.lookup_code(+)
         AND lease.attribute2 = look2.lookup_code(+)
         AND lease.milestone_type_code = look3.lookup_code(+);
    EXCEPTION
      WHEN OTHERS THEN
        RAISE program_error;
    END;
  
    --l_subject     :='*** RE Action_' || p_optname || '__' || p_rer_name ||' ***';
    CASE
      WHEN p_optname = 'Renewal' THEN
        l_subject := '*** RE Action_' || p_optname || ' Option__' ||
                     p_rer_name || ' ***';
      WHEN p_optname = 'Rent' THEN
        l_subject := '*** RE Action_' || p_optname || ' Change__' ||
                     p_rer_name || ' ***';
      WHEN p_optname = 'Property' THEN
        l_subject := '*** RE Action_' || p_optname || ' Expiration__' ||
                     p_rer_name || ' ***';
      ELSE
        l_subject := '*** RE Action_' || p_optname || '__' || p_rer_name ||
                     ' ***';
    END CASE;
    l_body_header := '<style type="text/css">.style1 {font-family: Arial, Helvetica, sans-serif;}</style><p class="style1">';
  
    l_body_detail := l_body_detail || '<BR>Action is required before  ' ||
                     htf.bold(l_milestone_date) || ' for your upcoming ' ||
                     htf.bold(p_optname);
    l_body_detail := l_body_detail || ' ' || htf.bold(l_milestone_type) || '.';
    l_body_detail := l_body_detail ||
                     '<BR><BR>For further information on the ' ||
                     htf.bold(p_rer_type) || ' premises known as ' ||
                     htf.bold(p_rer_name) || ' with Real Estate Record ' ||
                     htf.bold(p_lease_number) || '.';
  
    l_body_footer := l_body_footer ||
                     '<BR>Please refer to  the links below for further information, or to take action on this item.';
    l_body_footer := l_body_footer || '<BR><BR>Documentum ' ||
                     htf.anchor(p_documentum_link, p_documentum_link);
    l_body_footer := l_body_footer || '<BR>Oracle Property Manager ' ||
                     htf.anchor('http://ofebiz.hsi.hughessupply.com:8000/oa_servlets/AppsLogin',
                                'Oracle Applications');
  
    l_body := l_body_header || l_body_detail || l_body_footer;
  
    BEGIN
      l_err_callpoint := 'options_mail - calling html_email program';
      l_err_lease     := 'Error sending alert for lease ' || p_lease_number ||
                         ' Send To: ' || l_primary_email || ' Send From: ' ||
                         p_sender; -- v1.1
    
      xxcus_misc_pkg.html_email(p_to => l_primary_email, p_from => p_sender,
                                p_text => 'test'
                                --,p_subject       => '***Real Estate Action Required - Property Manager Alert***'
                               , p_subject => l_subject, p_html => l_body,
                                p_smtp_hostname => l_hostname,
                                p_smtp_portnum => l_hostport);
    
      -- general email sent to the real estate distribution group
      l_err_callpoint := 'options_mail - calling html_email program for cc email';
      xxcus_misc_pkg.html_email(p_to => l_default_cc, p_from => p_sender,
                                p_text => 'test'
                                --,p_subject       => '***Real Estate Action Required - Property Manager Alert***'
                               , p_subject => l_subject, p_html => l_body,
                                p_smtp_hostname => l_hostname,
                                p_smtp_portnum => l_hostport);
    
      l_err_callpoint := 'options_mail - calling html_email program for cc email if not null';
      IF l_cc_email IS NOT NULL THEN
        xxcus_misc_pkg.html_email(p_to => l_cc_email, p_from => p_sender,
                                  p_text => 'test'
                                  --,p_subject       => '***Real Estate Action Required - Property Manager Alert***'
                                 , p_subject => l_subject, p_html => l_body,
                                  p_smtp_hostname => l_hostname,
                                  p_smtp_portnum => l_hostport);
      END IF;
    EXCEPTION
      WHEN OTHERS THEN
        RAISE mail_error;
    END;
  
  END options_mail;

  PROCEDURE pend_mail(p_sender          VARCHAR2
                     ,p_lease_number    VARCHAR2
                     ,p_milestone_type  VARCHAR2
                     ,p_lease_id        NUMBER
                     ,p_rer_name        VARCHAR2
                     ,p_documentum_link VARCHAR2) IS
  
    -- -----------------------------------------------------------------------------
    -- |----------------------------< pend_email >----------------------------|
    -- -----------------------------------------------------------------------------
    --
    -- {Start Of Comments}
    --
    -- Description:
    --   This is the email procedure called by pn_alert_lsedtl.
    --
    --   VERSION DATE          AUTHOR(S)       DESCRIPTION
    -- ------- -----------   --------------- ---------------------------------
    -- 1.0     09-SEP-2009   Manny         Created this procedure.
    -- 1.1     27-APR-2010   Luong Vu      Changed subject line SR-37847
    --                                     Added error handling for mailing failure - mail_error
    --                                     (FRC 16904)
    -- 1.2     08-JUN-2010   Luong Vu      Mod primary_email to default to locationdirectory
    --                                     (RFC 17809)
    -- 1.3     02-AUG-2010   Luong Vu      mode of v1.2
  
    -- Java Mailer
    l_subject     VARCHAR2(32767) DEFAULT NULL; -- v1.1
    l_body        VARCHAR2(32767) DEFAULT NULL;
    l_body_header VARCHAR2(32767) DEFAULT NULL;
    l_body_detail VARCHAR2(32767) DEFAULT NULL;
    l_body_footer VARCHAR2(32767) DEFAULT NULL;
  
    -- local values
    l_primary_email  VARCHAR2(250) DEFAULT NULL;
    l_cc_email       VARCHAR2(250) DEFAULT NULL;
    l_milestone_type VARCHAR2(250) DEFAULT NULL;
  
  BEGIN
  
    BEGIN
      l_err_callpoint := 'pend_mail - main query';
      SELECT /*(SELECT look.description
                                                                             FROM pn.pn_lease_details_all lse
                                                                                 ,gl.gl_code_combinations glc
                                                                                 ,(SELECT description, lookup_code, enabled_flag
                                                                                   FROM fnd_lookup_values
                                                                                   WHERE lookup_type = 'HDS_OPN_NOTIFICATIONS') look
                                                                             WHERE lse.lease_id =p_lease_id
                                                                             AND  nvl(lse.receivable_account_id,lse.expense_account_id) = glc.code_combination_id
                                                                             AND look.lookup_code LIKE glc.segment1 || '%'
                                                                             AND substr(look.lookup_code
                                                                                      ,length(look.lookup_code) - 1
                                                                                      ,2) <> 'CC'
                                                                             AND look.enabled_flag = 'Y') primary_email*/
      /*              nvl2(look.description                                          --v1.2
      ,(SELECT look.description
       FROM pn.pn_lease_details_all lse
           ,gl.gl_code_combinations glc
           ,(SELECT description, lookup_code, enabled_flag
             FROM fnd_lookup_values
             WHERE lookup_type = 'HDS_OPN_NOTIFICATIONS') look
       WHERE lse.lease_id = p_lease_id
       AND nvl(lse.receivable_account_id,lse.expense_account_id) = glc.code_combination_id
       AND look.lookup_code LIKE glc.segment1 || '%'
       AND substr(look.lookup_code
                ,length(look.lookup_code) - 1
                ,2) <> 'CC'
       AND look.enabled_flag = 'Y'),l_distro_list3) primary_email       */
      
       nvl((SELECT look.description -- v1.3
             FROM pn.pn_lease_details_all lse
                 ,gl.gl_code_combinations glc
                 ,(SELECT description
                         ,lookup_code
                         ,enabled_flag
                     FROM apps.fnd_lookup_values
                    WHERE lookup_type = 'HDS_OPN_NOTIFICATIONS') look
            WHERE lse.lease_id = p_lease_id
              AND nvl(lse.receivable_account_id, lse.expense_account_id) =
                  glc.code_combination_id
              AND look.lookup_code LIKE glc.segment1 || '%'
              AND substr(look.lookup_code, length(look.lookup_code) - 1, 2) <> 'CC'
              AND look.enabled_flag = 'Y'), l_distro_list3) primary_email
      ,nvl(look2.description, '') cc_email
      ,look3.description milestone_type
        INTO l_primary_email
            ,l_cc_email
            ,l_milestone_type
        FROM pn.pn_set_milestones lease
            ,(SELECT description
                    ,lookup_code
                FROM fnd_lookup_values
               WHERE lookup_type = 'HDS_OPN_NOTIFICATIONS') look
            ,(SELECT description
                    ,lookup_code
                FROM fnd_lookup_values
               WHERE lookup_type = 'HDS_OPN_NOTIFICATIONS') look2
            ,(SELECT description
                    ,lookup_code
                FROM fnd_lookup_values
               WHERE lookup_type = 'PN_MILESTONES_TYPE') look3
       WHERE lease.milestones_set_id =
             (SELECT milestones_set_id
                FROM pn.pn_set_milestones
               WHERE milestone_type_code = p_milestone_type
                 AND rownum = 1)
         AND lease.attribute1 = look.lookup_code(+)
         AND lease.attribute2 = look2.lookup_code(+)
         AND lease.milestone_type_code = look3.lookup_code(+);
    EXCEPTION
      WHEN OTHERS THEN
        RAISE program_error;
    END;
  
    l_subject     := '*** RE Action_Pending Commencement__' || p_rer_name ||
                     ' ***'; -- v1.1
    l_body_header := '<style type="text/css">.style1 {font-family: Arial, Helvetica, sans-serif;}</style><p class="style1">';
  
    l_body_detail := l_body_detail || '<BR>The premises known as  ' ||
                     htf.bold(p_rer_name) || ' is ' ||
                     htf.bold('Pending Commencement.');
    l_body_detail := l_body_detail ||
                     '<BR><BR>The Real Estate Record (RER) ' ||
                     htf.bold(p_lease_number) ||
                     ' must be finalized to complete abstraction and activation of this record.';
    l_body_detail := l_body_detail ||
                     '<BR><BR>Please provide a Commencement Agreement or Certificate of Occupancy to Lease Administration at ' ||
                     htf.mailto('leaseadministration@hdsupply.com',
                                'leaseadministration@hdsupply.com') || '.';
  
    l_body_footer := '<p class="style1">';
    l_body_footer := l_body_footer ||
                     '<BR>Please refer to  the links below for further information, or to take action on this item.';
    l_body_footer := l_body_footer || '<BR><BR>Documentum ' ||
                     htf.anchor(p_documentum_link, p_documentum_link);
    l_body_footer := l_body_footer || '<BR>Oracle Property Manager ' ||
                     htf.anchor('http://ofebiz.hsi.hughessupply.com:8000/oa_servlets/AppsLogin',
                                'Oracle Applications');
  
    l_body := l_body_header || l_body_detail || l_body_footer;
  
    BEGIN
      l_err_callpoint := 'pend_mail - calling html_email program';
      l_err_lease     := 'Error sending alert for lease ' || p_lease_number ||
                         ' Send To: ' || l_primary_email || ' Send From: ' ||
                         p_sender; -- v1.1
      xxcus_misc_pkg.html_email(p_to => l_primary_email, p_from => p_sender,
                                p_text => 'test'
                                --,p_subject       => '***Real Estate Action Required - Property Manager Alert***'
                               , p_subject => l_subject
                                -- v1.1
                               , p_html => l_body,
                                p_smtp_hostname => l_hostname,
                                p_smtp_portnum => l_hostport);
    
      -- general email sent to the real estate distribution group
      xxcus_misc_pkg.html_email(p_to => l_default_cc, p_from => p_sender,
                                p_text => 'test'
                                --,p_subject       => '***Real Estate Action Required - Property Manager Alert***'
                               , p_subject => l_subject
                                -- v1.1
                               , p_html => l_body,
                                p_smtp_hostname => l_hostname,
                                p_smtp_portnum => l_hostport);
    
      IF l_cc_email IS NOT NULL THEN
        xxcus_misc_pkg.html_email(p_to => l_cc_email, p_from => p_sender,
                                  p_text => 'test'
                                  --,p_subject       => '***Real Estate Action Required - Property Manager Alert***'
                                 , p_subject => l_subject
                                  -- v1.1
                                 , p_html => l_body,
                                  p_smtp_hostname => l_hostname,
                                  p_smtp_portnum => l_hostport);
      END IF;
    
    EXCEPTION
      WHEN OTHERS THEN
        RAISE mail_error;
    END;
  
  END pend_mail;

  PROCEDURE pend_mail2(p_sender          VARCHAR2
                      ,p_lease_number    VARCHAR2
                      ,p_milestone_type  VARCHAR2
                      ,p_lease_id        NUMBER
                      ,p_rer_name        VARCHAR2
                      ,p_documentum_link VARCHAR2) IS
    -- -----------------------------------------------------------------------------
    -- |----------------------------< pend_email2 >----------------------------|
    -- -----------------------------------------------------------------------------
    --
    -- {Start Of Comments}
    --
    -- Description:
    --   This is the email procedure called by pn_alert_lsedtl.
    --
    --   VERSION DATE          AUTHOR(S)       DESCRIPTION
    -- ------- -----------   --------------- ---------------------------------
    -- 1.0     09-SEP-2009   Luong Vu         Created this procedure. SR27466
    -- 1.1     27-APR-2010   Luong Vu      Changed subject line SR-37847
    --                                     Added error handling for mailing failure - mail_error
    --                                     (FRC 16904)
    -- 1.2     08-JUN-2010   Luong Vu      Mod primary_email to default to locationdirectory
    --                                     (RFC 17809)
    -- 1.3     02-AUG-2010   Luong Vu      Mod of v1.2
  
    -- Java Mailer
    l_subject     VARCHAR2(32767) DEFAULT NULL; -- v1.1
    l_body        VARCHAR2(32767) DEFAULT NULL;
    l_body_header VARCHAR2(32767) DEFAULT NULL;
    l_body_detail VARCHAR2(32767) DEFAULT NULL;
    l_body_footer VARCHAR2(32767) DEFAULT NULL;
  
    -- local values
    l_primary_email  VARCHAR2(250) DEFAULT NULL;
    l_cc_email       VARCHAR2(250) DEFAULT NULL;
    l_milestone_type VARCHAR2(250) DEFAULT NULL;
  
  BEGIN
  
    BEGIN
      l_err_callpoint := 'pend_mail2 - main query';
      SELECT /*(SELECT look.description
                                                                             FROM pn.pn_lease_details_all lse
                                                                                 ,gl.gl_code_combinations glc
                                                                                 ,(SELECT description, lookup_code, enabled_flag
                                                                                   FROM fnd_lookup_values
                                                                                   WHERE lookup_type = 'HDS_OPN_NOTIFICATIONS') look
                                                                             WHERE lse.lease_id =p_lease_id
                                                                             AND  nvl(lse.receivable_account_id,lse.expense_account_id) = glc.code_combination_id
                                                                             AND look.lookup_code LIKE glc.segment1 || '%'
                                                                             AND substr(look.lookup_code
                                                                                      ,length(look.lookup_code) - 1
                                                                                      ,2) <> 'CC'
                                                                             AND look.enabled_flag = 'Y') primary_email*/
      /*              nvl2(look.description                                          --v1.2
      ,(SELECT look.description
       FROM pn.pn_lease_details_all lse
           ,gl.gl_code_combinations glc
           ,(SELECT description, lookup_code, enabled_flag
             FROM fnd_lookup_values
             WHERE lookup_type = 'HDS_OPN_NOTIFICATIONS') look
       WHERE lse.lease_id = p_lease_id
       AND nvl(lse.receivable_account_id,lse.expense_account_id) = glc.code_combination_id
       AND look.lookup_code LIKE glc.segment1 || '%'
       AND substr(look.lookup_code
                ,length(look.lookup_code) - 1
                ,2) <> 'CC'
       AND look.enabled_flag = 'Y'),l_distro_list3) primary_email */
      
       nvl((SELECT look.description -- v1.3
             FROM pn.pn_lease_details_all lse
                 ,gl.gl_code_combinations glc
                 ,(SELECT description
                         ,lookup_code
                         ,enabled_flag
                     FROM apps.fnd_lookup_values
                    WHERE lookup_type = 'HDS_OPN_NOTIFICATIONS') look
            WHERE lse.lease_id = p_lease_id
              AND nvl(lse.receivable_account_id, lse.expense_account_id) =
                  glc.code_combination_id
              AND look.lookup_code LIKE glc.segment1 || '%'
              AND substr(look.lookup_code, length(look.lookup_code) - 1, 2) <> 'CC'
              AND look.enabled_flag = 'Y'), l_distro_list3) primary_email
      ,nvl(look2.description, '') cc_email
      ,look3.description milestone_type
        INTO l_primary_email
            ,l_cc_email
            ,l_milestone_type
        FROM pn.pn_set_milestones lease
            ,(SELECT description
                    ,lookup_code
                FROM fnd_lookup_values
               WHERE lookup_type = 'HDS_OPN_NOTIFICATIONS') look
            ,(SELECT description
                    ,lookup_code
                FROM fnd_lookup_values
               WHERE lookup_type = 'HDS_OPN_NOTIFICATIONS') look2
            ,(SELECT description
                    ,lookup_code
                FROM fnd_lookup_values
               WHERE lookup_type = 'PN_MILESTONES_TYPE') look3
       WHERE lease.milestones_set_id =
             (SELECT milestones_set_id
                FROM pn.pn_set_milestones
               WHERE milestone_type_code = p_milestone_type
                 AND rownum = 1)
         AND lease.attribute1 = look.lookup_code(+)
         AND lease.attribute2 = look2.lookup_code(+)
         AND lease.milestone_type_code = look3.lookup_code(+);
    
    EXCEPTION
      WHEN OTHERS THEN
        RAISE program_error;
    END;
    l_subject     := '*** RE Action_Pending Completion__' || p_rer_name ||
                     ' ***'; -- v1.1
    l_body_header := '<style type="text/css">.style1 {font-family: Arial, Helvetica, sans-serif;}</style><p class="style1">';
  
    l_body_detail := l_body_detail || '<BR>The premises known as  ' ||
                     htf.bold(p_rer_name) || ' is' ||
                     htf.bold(' Pending Completion.');
    l_body_detail := l_body_detail ||
                     '<BR><BR>The termination of this obligation must be fully documented to complete Real Estate Record (RER) ' ||
                     htf.bold(p_lease_number);
    l_body_detail := l_body_detail ||
                     '<BR><BR>Please provide final close-out documentation to Lease Administration at ' ||
                     htf.mailto('leaseadministration@hdsupply.com',
                                'leaseadministration@hdsupply.com') || '.';
  
    l_body_footer := '<p class="style1">';
    l_body_footer := l_body_footer ||
                     '<BR>Please refer to  the links below for further information, or to take action on this item.';
    l_body_footer := l_body_footer || '<BR><BR>Documentum ' ||
                     htf.anchor(p_documentum_link, p_documentum_link);
    l_body_footer := l_body_footer || '<BR>Oracle Property Manager ' ||
                     htf.anchor('http://ofebiz.hsi.hughessupply.com:8000/oa_servlets/AppsLogin',
                                'Oracle Applications');
  
    l_body := l_body_header || l_body_detail || l_body_footer;
  
    BEGIN
      l_err_callpoint := 'pend_mail2 - calling html_email program';
      l_err_lease     := 'Error sending alert for lease ' || p_lease_number ||
                         ' Send To: ' || l_primary_email || ' Send From: ' ||
                         p_sender; -- v1.1
      xxcus_misc_pkg.html_email(p_to => l_primary_email, p_from => p_sender,
                                p_text => 'test'
                                --,p_subject       => '***Real Estate Action Required - Property Manager Alert***'
                               , p_subject => l_subject
                                -- v1.1
                               , p_html => l_body,
                                p_smtp_hostname => l_hostname,
                                p_smtp_portnum => l_hostport);
    
      -- general email sent to the real estate distribution group
      xxcus_misc_pkg.html_email(p_to => l_default_cc, p_from => p_sender,
                                p_text => 'test'
                                --,p_subject       => '***Real Estate Action Required - Property Manager Alert***'
                               , p_subject => l_subject
                                -- v1.1
                               , p_html => l_body,
                                p_smtp_hostname => l_hostname,
                                p_smtp_portnum => l_hostport);
    
      IF l_cc_email IS NOT NULL THEN
        xxcus_misc_pkg.html_email(p_to => l_cc_email, p_from => p_sender,
                                  p_text => 'test'
                                  --,p_subject       => '***Real Estate Action Required - Property Manager Alert***'
                                 , p_subject => l_subject
                                  -- v1.1
                                 , p_html => l_body,
                                  p_smtp_hostname => l_hostname,
                                  p_smtp_portnum => l_hostport);
      END IF;
    
    EXCEPTION
      WHEN OTHERS THEN
        RAISE mail_error;
    END;
  
  END pend_mail2;

  PROCEDURE expir_mail(p_sender          VARCHAR2
                      ,p_lease_number    VARCHAR2
                      ,p_milestone_type  VARCHAR2
                      ,p_lease_id        NUMBER
                      ,p_rer_name        VARCHAR2
                      ,p_documentum_link VARCHAR2
                      ,p_rer_type        VARCHAR2
                      ,p_milestone_date  DATE) IS
  
    -- -----------------------------------------------------------------------------
    -- |----------------------------< expir_email >----------------------------|
    -- -----------------------------------------------------------------------------
    --
    -- {Start Of Comments}
    --
    -- Description:
    --   This is the email procedure called by pn_alert_lsedtl.
    --
    --   VERSION DATE          AUTHOR(S)       DESCRIPTION
    -- ------- -----------   --------------- ---------------------------------
    -- 1.0     09-SEP-2009   Manny         Created this procedure.
    -- 1.1     27-APR-2010   Luong Vu      Changed subject line SR-37847
    --                                     Added error handling for mailing failure - mail_error
    --                                     (FRC 16904)
    -- 1.2     08-JUN-2010   Luong Vu      Mod primary_email to default to locationdirectory
    --                                     (RFC 17809)
    -- 1.3     02-AUG-2010   Luong Vu      Mod of 1.2
  
    -- Java Mailer
    l_subject     VARCHAR2(32767) DEFAULT NULL; -- v1.1
    l_body        VARCHAR2(32767) DEFAULT NULL;
    l_body_header VARCHAR2(32767) DEFAULT NULL;
    l_body_detail VARCHAR2(32767) DEFAULT NULL;
    l_body_footer VARCHAR2(32767) DEFAULT NULL;
  
    -- local values
    l_primary_email  VARCHAR2(250) DEFAULT NULL;
    l_cc_email       VARCHAR2(250) DEFAULT NULL;
    l_milestone_type VARCHAR2(250) DEFAULT NULL;
  
  BEGIN
  
    BEGIN
      l_err_callpoint := 'expir_mail - main query';
      SELECT /*(SELECT look.description
                                                                             FROM pn.pn_lease_details_all lse
                                                                                 ,gl.gl_code_combinations glc
                                                                                 ,(SELECT description, lookup_code, enabled_flag
                                                                                   FROM fnd_lookup_values
                                                                                   WHERE lookup_type = 'HDS_OPN_NOTIFICATIONS') look
                                                                             WHERE lse.lease_id =p_lease_id
                                                                             AND  nvl(lse.receivable_account_id,lse.expense_account_id) = glc.code_combination_id
                                                                             AND look.lookup_code LIKE glc.segment1 || '%'
                                                                             AND substr(look.lookup_code
                                                                                      ,length(look.lookup_code) - 1
                                                                                      ,2) <> 'CC'
                                                                             AND look.enabled_flag = 'Y') primary_email*/
      
      /*nvl2(look.description                                          --v1.2
      ,(SELECT look.description
       FROM pn.pn_lease_details_all lse
           ,gl.gl_code_combinations glc
           ,(SELECT description, lookup_code, enabled_flag
             FROM fnd_lookup_values
             WHERE lookup_type = 'HDS_OPN_NOTIFICATIONS') look
       WHERE lse.lease_id = p_lease_id
       AND nvl(lse.receivable_account_id,lse.expense_account_id) = glc.code_combination_id
       AND look.lookup_code LIKE glc.segment1 || '%'
       AND substr(look.lookup_code
                ,length(look.lookup_code) - 1
                ,2) <> 'CC'
       AND look.enabled_flag = 'Y'),l_distro_list3) primary_email   */
      
       nvl((SELECT look.description -- v1.3
             FROM pn.pn_lease_details_all lse
                 ,gl.gl_code_combinations glc
                 ,(SELECT description
                         ,lookup_code
                         ,enabled_flag
                     FROM apps.fnd_lookup_values
                    WHERE lookup_type = 'HDS_OPN_NOTIFICATIONS') look
            WHERE lse.lease_id = p_lease_id
              AND nvl(lse.receivable_account_id, lse.expense_account_id) =
                  glc.code_combination_id
              AND look.lookup_code LIKE glc.segment1 || '%'
              AND substr(look.lookup_code, length(look.lookup_code) - 1, 2) <> 'CC'
              AND look.enabled_flag = 'Y'), l_distro_list3) primary_email
      ,nvl(look2.description, '') cc_email
      ,look3.description milestone_type
        INTO l_primary_email
            ,l_cc_email
            ,l_milestone_type
        FROM pn.pn_set_milestones lease
            ,(SELECT description
                    ,lookup_code
                FROM fnd_lookup_values
               WHERE lookup_type = 'HDS_OPN_NOTIFICATIONS') look
            ,(SELECT description
                    ,lookup_code
                FROM fnd_lookup_values
               WHERE lookup_type = 'HDS_OPN_NOTIFICATIONS') look2
            ,(SELECT description
                    ,lookup_code
                FROM fnd_lookup_values
               WHERE lookup_type = 'PN_MILESTONES_TYPE') look3
       WHERE lease.milestones_set_id =
             (SELECT milestones_set_id
                FROM pn.pn_set_milestones
               WHERE milestone_type_code = p_milestone_type
                 AND rownum = 1)
         AND lease.attribute1 = look.lookup_code(+)
         AND lease.attribute2 = look2.lookup_code(+)
         AND lease.milestone_type_code = look3.lookup_code(+);
    EXCEPTION
      WHEN OTHERS THEN
        RAISE program_error;
    END;
    l_subject     := '*** RE Action_Upcoming Expiration__' || p_rer_name ||
                     ' ***'; -- v1.1
    l_body_header := '<style type="text/css">.style1 {font-family: Arial, Helvetica, sans-serif;}</style><p class="style1">';
  
    l_body_detail := l_body_detail || '<BR>Action is required before  ' ||
                     htf.bold(to_char(p_milestone_date, 'MON-DD-YYYY')) ||
                     ' for your upcoming ' || htf.bold('Expiration.');
    l_body_detail := l_body_detail ||
                     '<BR><BR>For further information on the ' ||
                     htf.bold(p_rer_type) || ' premises known as ' ||
                     htf.bold(p_rer_name) || ' with Real Estate Record ' ||
                     htf.bold(p_lease_number) || '. ';
  
    l_body_footer := l_body_footer ||
                     '<BR>Please refer to the links below for further information, or to take action on this item.';
    l_body_footer := l_body_footer || '<BR><BR>Documentum ' ||
                     htf.anchor(p_documentum_link, p_documentum_link);
    l_body_footer := l_body_footer || '<BR>Oracle Property Manager ' ||
                     htf.anchor('http://ofebiz.hsi.hughessupply.com:8000/oa_servlets/AppsLogin',
                                'Oracle Applications');
  
    l_body := l_body_header || l_body_detail || l_body_footer;
  
    BEGIN
      l_err_callpoint := 'expir_mail - calling html_email program';
      l_err_lease     := 'Error sending alert for lease ' || p_lease_number ||
                         ' Send To: ' || l_primary_email || ' Send From: ' ||
                         p_sender; -- v1.1
    
      xxcus_misc_pkg.html_email(p_to => l_primary_email, p_from => p_sender,
                                p_text => 'test'
                                --,p_subject       => '***Real Estate Action Required - Property Manager Alert***'
                               , p_subject => l_subject
                                -- v1.1
                               , p_html => l_body,
                                p_smtp_hostname => l_hostname,
                                p_smtp_portnum => l_hostport);
    
      -- general email sent to the real estate distribution group
      xxcus_misc_pkg.html_email(p_to => l_default_cc, p_from => p_sender,
                                p_text => 'test'
                                -- ,p_subject       => '***Real Estate Action Required - Property Manager Alert***'
                               , p_subject => l_subject, p_html => l_body,
                                p_smtp_hostname => l_hostname,
                                p_smtp_portnum => l_hostport);
    
      IF l_cc_email IS NOT NULL THEN
        xxcus_misc_pkg.html_email(p_to => l_cc_email, p_from => p_sender,
                                  p_text => 'test'
                                  --    ,p_subject       => '***Real Estate Action Required - Property Manager Alert***'
                                 , p_subject => l_subject, p_html => l_body,
                                  p_smtp_hostname => l_hostname,
                                  p_smtp_portnum => l_hostport);
      END IF;
    
    EXCEPTION
      WHEN OTHERS THEN
        RAISE mail_error;
    END;
  
  END expir_mail;



  PROCEDURE html_email(p_to            IN VARCHAR2
                      ,p_from          IN VARCHAR2
                      ,p_subject       IN VARCHAR2
                      ,p_text          IN VARCHAR2 DEFAULT NULL
                      ,p_html          IN VARCHAR2 DEFAULT NULL
                      ,p_smtp_hostname IN VARCHAR2
                      ,p_smtp_portnum  IN VARCHAR2) IS
  
    -- -----------------------------------------------------------------------------
    -- |----------------------------< html_email >----------------------------|
    -- -----------------------------------------------------------------------------
    --
    -- {Start Of Comments}
    --
    -- Description:
    --   This is the email procedure called by the invidual email programs.
    --   This is a straight copy from ASKTOM.  More info can be referenced :
    --   http://asktom.oracle.com/pls/asktom/f?p=100:11:3585877835963984::::P11_QUESTION_ID:1739411218448
    --
    --   VERSION DATE          AUTHOR(S)       DESCRIPTION
    -- ------- -----------   --------------- ---------------------------------
    -- 1.0     09-SEP-2009   Manny         Created this procedure.
    --
  
    l_boundary   VARCHAR2(255) DEFAULT 'a1b2c3d4e3f2g1';
    l_connection utl_smtp.connection;
    l_body_html  CLOB := empty_clob; --This LOB will be the email message
    l_offset     NUMBER;
    l_ammount    NUMBER;
    l_temp       VARCHAR2(32767) DEFAULT NULL;
  BEGIN
  
    l_connection := utl_smtp.open_connection(p_smtp_hostname, p_smtp_portnum);
    utl_smtp.helo(l_connection, p_smtp_hostname);
    utl_smtp.mail(l_connection, p_from);
    utl_smtp.rcpt(l_connection, p_to);
  
    l_temp := l_temp || 'MIME-Version: 1.0' || chr(13) || chr(10);
    l_temp := l_temp || 'To: ' || p_to || chr(13) || chr(10);
    l_temp := l_temp || 'From: ' || p_from || chr(13) || chr(10);
    l_temp := l_temp || 'Subject: ' || p_subject || chr(13) || chr(10);
    l_temp := l_temp || 'Reply-To: ' || p_from || chr(13) || chr(10);
    l_temp := l_temp || 'Content-Type: multipart/alternative; boundary=' ||
              chr(34) || l_boundary || chr(34) || chr(13) || chr(10);
  
    ----------------------------------------------------
    -- Write the headers
    dbms_lob.createtemporary(l_body_html, FALSE, 10);
    dbms_lob.write(l_body_html, length(l_temp), 1, l_temp);
  
    ----------------------------------------------------
    -- Write the text boundary
    l_offset := dbms_lob.getlength(l_body_html) + 1;
    l_temp   := '--' || l_boundary || chr(13) || chr(10);
    l_temp   := l_temp || 'content-type: text/plain; charset=us-ascii' ||
                chr(13) || chr(10) || chr(13) || chr(10);
    dbms_lob.write(l_body_html, length(l_temp), l_offset, l_temp);
  
    ----------------------------------------------------
    -- Write the plain text portion of the email
    l_offset := dbms_lob.getlength(l_body_html) + 1;
    dbms_lob.write(l_body_html, length(p_text), l_offset, p_text);
  
    ----------------------------------------------------
    -- Write the HTML boundary
    l_temp   := chr(13) || chr(10) || chr(13) || chr(10) || '--' ||
                l_boundary || chr(13) || chr(10);
    l_temp   := l_temp || 'content-type: text/html;' || chr(13) || chr(10) ||
                chr(13) || chr(10);
    l_offset := dbms_lob.getlength(l_body_html) + 1;
    dbms_lob.write(l_body_html, length(l_temp), l_offset, l_temp);
  
    ----------------------------------------------------
    -- Write the HTML portion of the message
    l_offset := dbms_lob.getlength(l_body_html) + 1;
    dbms_lob.write(l_body_html, length(p_html), l_offset, p_html);
  
    ----------------------------------------------------
    -- Write the final html boundary
    l_temp   := chr(13) || chr(10) || '--' || l_boundary || '--' || chr(13);
    l_offset := dbms_lob.getlength(l_body_html) + 1;
    dbms_lob.write(l_body_html, length(l_temp), l_offset, l_temp);
  
    ----------------------------------------------------
    -- Send the email in 1900 byte chunks to UTL_SMTP
    l_offset  := 1;
    l_ammount := 1900;
    utl_smtp.open_data(l_connection);
    WHILE l_offset < dbms_lob.getlength(l_body_html)
    LOOP
      utl_smtp.write_data(l_connection,
                          dbms_lob.substr(l_body_html, l_ammount, l_offset));
      l_offset  := l_offset + l_ammount;
      l_ammount := least(1900, dbms_lob.getlength(l_body_html) - l_ammount);
    END LOOP;
    utl_smtp.close_data(l_connection);
    utl_smtp.quit(l_connection);
    dbms_lob.freetemporary(l_body_html);
  
  END html_email;

END xxcuspn_alerts_pkg;
/
