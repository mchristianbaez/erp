CREATE OR REPLACE PACKAGE BODY APPS.XXHDS_GLSLA_AR_DRILLDOWN
--ESMS ticket --Add activity date to the custom table as well as the pseudo table at_dtls
--192082         06/MAR/2013
--200632         23-APR-2013
--219693         17-DEC-2013
AS
  PROCEDURE print_log(p_message in varchar2) IS
  BEGIN
   IF fnd_global.conc_request_id >0 THEN
    fnd_file.put_line(fnd_file.log, p_message);
   ELSE
    dbms_output.put_line(p_message);
   END IF;
  EXCEPTION
   WHEN OTHERS THEN
    FND_FILE.PUT_LINE(FND_FILE.LOG, 'Issue in print_log routine ='||sqlerrm);
  END print_log;
  PROCEDURE get_attributes
          (
            p_dtl_id in number
           ,p_line_id in number
           ,o_wc_branch out varchar2
           ,o_order_num out varchar2
           ,o_order_type out varchar2
           ,o_pmt_type_code out varchar2
          ) IS
   CURSOR PROPS IS
       SELECT o.organization_code wc_branch,
              a.application_ref_num order_number,
              t.name order_type,
              p.payment_type_code payment_type_code
              --r.name activity_type
         FROM ar_receivable_applications_all a,
              ar_distributions_all ard,
              oe_order_headers_all h,
              oe_payments p,
              oe_transaction_types_v t,
              org_organization_definitions o,
              ar_receivables_trx r
        WHERE     1 = 1
          AND ard.line_id =p_line_id
          AND a.cash_receipt_id =p_dtl_id
          AND a.cash_receipt_history_id = ard.source_id
          AND ard.source_table = 'CRH'
          AND a.application_ref_type = 'OM'
          AND r.receivables_trx_id(+) = a.receivables_trx_id
          AND p.header_id(+) = TO_NUMBER (a.application_ref_id)
          AND p.payment_set_id(+) = a.payment_set_id
          AND p.payment_type_code(+) IN ('CASH', 'CHECK')
          AND h.header_id(+) = p.header_id
          AND t.transaction_type_id(+) = h.order_type_id
          AND o.organization_id(+) = h.ship_from_org_id
          AND rownum <2
       UNION ALL
       SELECT o.organization_code,
              a.application_ref_num,
              t.name order_type,
              p.payment_type_code
              --r.name
         FROM ar_receivable_applications a,
              ar_cash_receipts b,
              ar_distributions_all ard,
              oe_order_headers_all h,
              oe_payments p,
              apps.xxhds_iby_trxn_extensions_v x,
              oe_transaction_types_v t,
              org_organization_definitions o,
              ar_receivables_trx r
        WHERE     1 = 1
              AND ard.line_id =p_line_id
              AND a.cash_receipt_id =p_dtl_id
              AND a.cash_receipt_history_id = ard.source_id
              AND b.cash_receipt_id = a.cash_receipt_id
              AND ard.source_table = 'CRH'
              AND a.payment_set_id IS NOT NULL
              AND a.application_ref_type = 'OM'
              AND a.receivables_trx_id IS NOT NULL
              AND r.receivables_trx_id = a.receivables_trx_id
              AND p.header_id = TO_NUMBER (a.application_ref_id)
              AND p.payment_set_id = a.payment_set_id
              AND p.payment_type_code = 'CREDIT_CARD'
              AND x.trxn_extension_id = b.payment_trxn_extension_id
              AND h.header_id = p.header_id
              AND t.transaction_type_id(+) = h.order_type_id
              AND o.organization_id(+) = h.ship_from_org_id
              AND rownum <2;
  BEGIN
   FOR REC IN PROPS LOOP
     o_wc_branch     :=rec.wc_branch;
     o_order_num     :=rec.order_number;
     o_order_type    :=rec.order_type;
     o_pmt_type_code :=rec.payment_type_code;
   END LOOP;
  EXCEPTION
   WHEN NO_DATA_FOUND THEN
     o_wc_branch     :=NULL;
     o_order_num     :=NULL;
     o_order_type    :=NULL;
     o_pmt_type_code :=NULL;
   WHEN OTHERS THEN
    PRINT_LOG('WHEN OTHERS @ GET_ATTRIBUTES, MESSAGE: '||SQLERRM);
    PRINT_LOG('P_LINE_ID ='||p_line_id);
    PRINT_LOG('P_DTL_ID ='||p_dtl_id);
     o_wc_branch     :=NULL;
     o_order_num     :=NULL;
     o_order_type    :=NULL;
     o_pmt_type_code :=NULL;
  END get_attributes;
  FUNCTION get_ps_trx_number (n_ps_id in number) return varchar2 IS
   l_trx_number ar_payment_schedules_all.trx_number%type :=null;
  BEGIN
   select /*+ RESULT_CACHE */ trx_number
   into   l_trx_number
   from   apps.xxcus_dflt_ar_pmt_schedules_v
   where  1 =1
     and  payment_schedule_id =n_ps_id;

   return l_trx_number;

  EXCEPTION
   WHEN NO_DATA_FOUND THEN
    RETURN NULL;
   WHEN OTHERS THEN
    FND_FILE.PUT_LINE(FND_FILE.LOG, 'Issue in get_ps_trx_number routine, n_ps_id ='||n_ps_id||', message ='||sqlerrm);
    RETURN NULL;
  END get_ps_trx_number;

  FUNCTION get_rec_trx_name (n_rec_trx_id in number) return varchar2 IS
   l_name ar_receivables_trx_all.name%type :=null;
  BEGIN
   select /*+ RESULT_CACHE */ name
   into   l_name
   from   ar_receivables_trx
   where  1 =1
     and  receivables_trx_id =n_rec_trx_id;

   return l_name;

  EXCEPTION
   WHEN NO_DATA_FOUND THEN
    RETURN NULL;
   WHEN OTHERS THEN
    FND_FILE.PUT_LINE(FND_FILE.LOG, 'Issue in get_rec_trx_name routine, n_rec_trx_id ='||n_rec_trx_id||', message ='||sqlerrm);
    RETURN NULL;
  END get_rec_trx_name;


  PROCEDURE main (
    retcode               out number
   ,errbuf                out varchar2
   ,p_ledger_id           in  number
   ,p_period_name         in  varchar2
   ,p_account             in  varchar2
  ) IS
  -- Cursor gl_sla_ar will pick SLA entries for other than je categories Receipts and Misc Receipts.
  CURSOR gl_sla_ar (p_req_id in number, p_gl_date_from in date, p_gl_date_to in date) IS
  SELECT  jh.je_source SOURCE,
          jh.je_category,
          jl.effective_date acc_date,
          jh.NAME entry,
          SUBSTR (jl.description, 1, 240) line_descr,
          jl.accounted_dr line_acctd_dr,
          jl.accounted_cr line_acctd_cr,
          jl.entered_dr line_ent_dr,
          jl.entered_cr line_ent_cr,
          ael.gl_sl_link_id ael_gl_sl_link_id,
          aeh.event_id aeh_event_id,
          xdl.ae_line_num xdl_ae_line_num,
          ael.ae_header_id ael_ae_header_id,
          aeh.entity_id aeh_entity_id,
          ar_dtls."DTL_ID",
          ar_dtls."TRANSACTION_NUMBER",
          ar_dtls."CUSTOMER_NUMBER",
          ar_dtls."CUSTOMER_OR_VENDOR",
          ar_dtls."DTL_ENTITY_ID",
          ar_dtls."RECEIPT_CLASS",
          ar_dtls."PAYMENT_METHOD",
          ar_dtls."RECEIPT_NUMBER",
          ar_dtls."RECEIPT_DATE",
          ar_dtls."RECEIPT_STATUS" RECEIPT_CURRENT_STATUS,
          ar_dtls."RECEIPT_STATE" RECEIPT_CURRENT_STATE,
         (select status
             from ar_cash_receipt_history_all crh
            where ar_dtls.receipt_number is not null
              and crh.cash_receipt_id=ar_dtls.dtl_id
              and crh.event_id=xlae.event_id
              and crh.cash_receipt_history_id =
              (select max(cash_receipt_history_id) from ar_cash_receipt_history_all crh1
               where crh.cash_receipt_id=crh1.cash_receipt_id
                 and crh.event_id=crh1.event_id)
          ) activity_status,
          (
            select xlk.meaning
            from   xla_lookups xlk
            where  1 =1
              and xlk.lookup_type ='XLA_ACCOUNTING_CLASS'
              and xlk.lookup_code =ael.accounting_class_code
          )  RECEIPT_ACTIVITY_STATUS,
          ar_dtls."RECEIPT_COMMENTS",
          ar_dtls."CUSTOMER_RECEIPT_REF",
          ar_dtls."REMIT_BANK_ACCOUNT",
          ar_dtls."REVERSAL_CATEGORY",
          ar_dtls."REVERSAL_COMMENTS",
          ar_dtls."REVERSAL_DATE",
          ar_dtls."REVERSAL_REASON",
          ar_dtls."REVERSAL_REASON_CODE",
          ar_dtls."ADJUSMENT_NAME",
          ar_dtls."ADJUSTMENT_NUMBER"
         ,ar_dtls.card_brand CARD_BRAND
         ,ar_dtls.identifying_number IDENTIFYING_NUMBER
         ,ar_dtls.identifying_name IDENTIFYING_NAME
         ,ar_dtls.order_type ORDER_TYPE
         ,ar_dtls.sales_order SALES_ORDER
         ,ar_dtls.wc_branch WC_BRANCH
         ,DECODE (
             ra_dist.cust_trx_line_gl_dist_id,
             NULL, dist.AMOUNT_DR,
             TO_NUMBER (
                DECODE (
                   ra_dist.account_class,
                   'REC', DECODE (SIGN (NVL (ra_dist.amount, 0)),
                                  -1, NULL,
                                  NVL (ra_dist.amount, 0)),
                   DECODE (SIGN (NVL (ra_dist.amount, 0)),
                           -1, -NVL (ra_dist.amount, 0),
                           NULL))))
             Entered_DR,
          DECODE (
             ra_dist.cust_trx_line_gl_dist_id,
             NULL, dist.AMOUNT_cR,
             TO_NUMBER (
                DECODE (
                   ra_dist.account_class,
                   'REC', DECODE (SIGN (NVL (ra_dist.amount, 0)),
                                  -1, -NVL (ra_dist.amount, 0),
                                  NULL),
                   DECODE (SIGN (NVL (ra_dist.amount, 0)),
                           -1, NULL,
                           NVL (ra_dist.amount, 0)))))
             Entered_CR,
          DECODE (
             ra_dist.cust_trx_line_gl_dist_id,
             NULL, dist.ACCTD_AMOUNT_DR,
             TO_NUMBER (
                DECODE (
                   ra_dist.account_class,
                   'REC', DECODE (SIGN (NVL (ra_dist.amount, 0)),
                                  -1, NULL,
                                  NVL (ra_dist.acctd_amount, 0)),
                   DECODE (SIGN (NVL (ra_dist.amount, 0)),
                           -1, -NVL (ra_dist.acctd_amount, 0),
                           NULL))))
             Accounted_DR,
          DECODE (
             ra_dist.cust_trx_line_gl_dist_id,
             NULL, dist.ACCTD_AMOUNT_CR,
             TO_NUMBER (
                DECODE (
                   ra_dist.account_class,
                   'REC', DECODE (SIGN (NVL (ra_dist.amount, 0)),
                                  -1, -NVL (ra_dist.acctd_amount, 0),
                                  NULL),
                   DECODE (SIGN (NVL (ra_dist.amount, 0)),
                           -1, NULL,
                           NVL (ra_DIST.acctd_amount, 0)))))
             Accounted_CR,
          ael.unrounded_accounted_dr sla_line_accounted_dr,
          ael.unrounded_accounted_cr sla_line_accounted_cr,
            NVL (ael.unrounded_accounted_dr, 0)
          - NVL (ael.unrounded_accounted_cr, 0)
             sla_line_accounted_net,
          ael.unrounded_entered_dr sla_line_entered_dr,
          ael.unrounded_entered_cr sla_line_entered_cr,
            NVL (ael.unrounded_entered_dr, 0)
          - NVL (ael.unrounded_entered_cr, 0)
             sla_line_entered_net,
          xdl.unrounded_entered_cr sla_dist_entered_cr,
          xdl.unrounded_entered_dr sla_dist_entered_dr,
            NVL (xdl.unrounded_entered_cr, 0)
          - NVL (xdl.unrounded_entered_dr, 0)
             sla_dist_entered_net,
          xdl.unrounded_accounted_cr sla_dist_accounted_cr,
          xdl.unrounded_accounted_dr sla_dist_accounted_dr,
            NVL (xdl.unrounded_accounted_cr, 0)
          - NVL (xdl.unrounded_accounted_dr, 0)
             sla_dist_accounted_net,
          gcc.CONCATENATED_SEGMENTS gl_account_string,
          jh.period_name,
          NVL (dist.source_type, NULL) TYPE,
          gle.name,
          gb.name batch_name,
          GCC.SEGMENT4 GCC#50328#ACCOUNT,
          GCC.SEGMENT3 GCC#50328#COST_CENTER,
          GCC.SEGMENT6 GCC#50328#FURTURE_USE,
          GCC.SEGMENT7 GCC#50328#FUTURE_USE_2,
          GCC.SEGMENT2 GCC#50328#LOCATION,
          GCC.SEGMENT1 GCC#50328#PRODUCT,
          GCC.SEGMENT5 GCC#50328#PROJECT_CODE,
          GCC.SEGMENT4 GCC#50368#ACCOUNT,
          GCC.SEGMENT3 GCC#50368#DEPARTMENT,
          GCC.SEGMENT2 GCC#50368#DIVISION,
          GCC.SEGMENT6 GCC#50368#FUTURE_USE,
          GCC.SEGMENT1 GCC#50368#PRODUCT,
          GCC.SEGMENT5 GCC#50368#SUBACCOUNT,
          p_req_id request_id,
          Null payment_type_code,
          ar_dtls.org_id org_id,
          ar_dtls.instrument_type,
          ar_dtls.adjustment_reason,
          ar_dtls.adjustment_comments,
          ar_dtls.prism_return_payment_type,
          ar_dtls.invoice_source,
          ar_dtls.activity_name,
          to_number(null) receivable_application_id,
          ar_dtls.receivables_trx_id,
          ar_dtls.activity_date,
          xdl.source_distribution_id_num_1 source_dist_id_num_1,
          ar_dtls.receipt_type receipt_type,
          ar_dtls.original_cash_receipt_id original_cash_receipt_id,
          ar_dtls.payment_trxn_extn_id payment_trxn_extn_id,
          to_char(null) output_type,
          to_char(null) fin_branch,
          case
           when ar_dtls.invoice_source ='PRISM REFUND' then 'PRISM'
           else to_char(null)
          end  payment_type
     FROM gl_ledgers gle,
          gl_je_headers jh,
          gl_je_lines jl,
          gl_code_combinations_kfv gcc,
          gl_import_references jir,
          xla_distribution_links xdl,
          xla_ae_lines ael,
          xla_ae_headers aeh,
          ar_distributions_all dist,
          ra_cust_trx_line_gl_dist_all ra_dist,
          gl_period_statuses gps,
          gl_je_batches gb,
          xla_events xlae,                                             --sbala
          (
            SELECT trx.customer_trx_id dtl_id,
                  trx.trx_number transaction_number,
                  hzca.account_number customer_number,
                  hzp.party_name customer_or_vendor,
                  xlate.entity_id dtl_entity_id,
                  TO_CHAR (NULL) receipt_class,
                  TO_CHAR (NULL) payment_method,
                  TO_CHAR (NULL) receipt_number,
                  trx.trx_date   receipt_date,
                  TO_CHAR (NULL) receipt_status,
                  TO_CHAR (NULL) receipt_state,
                  TO_CHAR (NULL) receipt_comments,
                  TO_CHAR (NULL) customer_receipt_ref,
                  TO_CHAR (NULL) remit_bank_account,
                  TO_CHAR (NULL) reversal_category,
                  TO_CHAR (NULL) reversal_comments,
                  TO_DATE (NULL) reversal_date,
                  TO_CHAR (NULL) reversal_reason,
                  TO_CHAR (NULL) reversal_reason_code,
                  TO_CHAR (NULL) adjusment_name,
                  TO_CHAR (NULL) adjustment_number
                 ,null card_brand
                 ,null identifying_number
                 ,null identifying_name
                 ,null order_type
                 ,trx.ct_reference sales_order
                 /*
                 ,case
                    when trx.interface_header_context ='ORDER ENTRY' then trx.interface_header_attribute1
                    else to_char(null)
                  end sales_order
                  */
                 ,null wc_branch
                 ,trx.org_id org_id
                 ,null instrument_type
                 ,TO_CHAR (NULL) adjustment_reason
                 ,TO_CHAR (NULL) adjustment_comments
                 ,case
                   when trx.interface_header_context ='ORDER ENTRY' then
                     (
                       select attribute8
                       from   oe_order_headers
                       where  1 =1
                         and  order_number =to_number(trx.interface_header_attribute1)
                     )
                   else to_char(null)
                  end prism_return_payment_type
                 ,rbs.name invoice_source
                 ,TO_CHAR (NULL) activity_name
                 ,TO_NUMBER (NULL) receivables_trx_id
                 ,trx.trx_date activity_date
                 ,to_char (null) receipt_type
                 ,to_number (null) original_cash_receipt_id
                 ,TO_NUMBER (NULL) payment_trxn_extn_id
             FROM ra_customer_trx_all trx,
                  hz_cust_accounts hzca,
                  hz_parties hzp,
                  xla_transaction_entities xlate,
                  fnd_application fndapp,
                  ra_batch_sources rbs
            WHERE 1 = 1
                  AND fndapp.application_short_name = 'AR'
                  AND xlate.application_id = fndapp.application_id --do not miss this condition at any cost
                  AND xlate.ledger_id   =p_ledger_id
                  AND xlate.entity_code = 'TRANSACTIONS'
                  AND xlate.source_id_int_1 = trx.customer_trx_id
                  AND hzca.cust_account_id = trx.bill_to_customer_id
                  AND hzp.party_id = hzca.party_id
                  AND rbs.batch_source_id =trx.batch_source_id
                  AND Exists
                       (
                         select 1
                         from   ra_cust_trx_line_gl_dist_all
                         where  1 =1
                           and  customer_trx_id =trx.customer_trx_id
                           and  gl_date between p_gl_date_from and p_gl_date_to
                       )
           UNION
           SELECT adj.adjustment_id dtl_id,
                  adj.trx_number transaction_number,
                  adj.customer_number customer_number,
                  adj.customer_name customer_or_vendor,
                  xlate.entity_id dtl_entity_id,
                  TO_CHAR (NULL) receipt_class,
                  TO_CHAR (NULL) payment_method,
                  TO_CHAR (NULL) receipt_number,
                  adj.trx_date   receipt_date,
                  TO_CHAR (NULL) receipt_status,
                  TO_CHAR (NULL) receipt_state,
                  TO_CHAR (NULL) receipt_comments,
                  TO_CHAR (NULL) customer_receipt_ref,
                  TO_CHAR (NULL) remit_bank_account,
                  TO_CHAR (NULL) reversal_category,
                  TO_CHAR (NULL) reversal_comments,
                  TO_DATE (NULL) reversal_date,
                  TO_CHAR (NULL) reversal_reason,
                  TO_CHAR (NULL) reversal_reason_code,
                  adj.activity_name adjusment_name,
                  adj.adjustment_number adjustment_number
                 ,null card_brand
                 ,null identifying_number
                 ,null identifying_name
                 ,null order_type
                 ,case
                   when (adj.customer_trx_id is not null) then
                    (
                     select ct_reference
                     from   ra_customer_trx_all
                     where  1 =1
                       and  customer_trx_id =adj.customer_trx_id
                    )
                   else to_char(null)
                  end sales_order
                 ,(
                   select organization_code
                   from   org_organization_definitions a
                         ,ra_customer_trx_all b
                   where  1 =1
                     and  b.customer_trx_id =adj.customer_trx_id
                     and  b.interface_header_context ='ORDER ENTRY'
                     and  to_char(a.organization_id) =b.interface_header_attribute10
                  ) wc_branch
                 ,adj.org_id org_id
                 ,null instrument_type
                 ,adj.reason_meaning adjustment_reason
                 ,UPPER(trim(replace(trim(regexp_replace(adj.comments, '([^[:print:]])',' ')), '.', ''))) adjustment_comments
                 --,adj.comments adjustment_comments
                 ,TO_CHAR (NULL) prism_return_payment_type
                 ,adj.source invoice_source
                 ,adj.activity_name activity_name
                 ,adj.receivables_trx_id
                 ,adj.trx_date activity_date
                 ,TO_CHAR (NULL) receipt_type
                 ,TO_NUMBER (NULL) original_cash_receipt_id
                 ,TO_NUMBER (NULL) payment_trxn_extn_id
             FROM ar_adjustments_v adj,
                  xla_transaction_entities xlate,
                  fnd_application fndapp
            WHERE 1 = 1
                  AND fndapp.application_short_name = 'AR'
                  AND xlate.application_id = fndapp.application_id --do not miss this condition at any cost
                  AND xlate.ledger_id   =p_ledger_id
                  AND xlate.entity_code = 'ADJUSTMENTS'
                  AND xlate.source_id_int_1 = adj.adjustment_id
                  AND adj.gl_date between p_gl_date_from and p_gl_date_to
        ) ar_dtls
    WHERE 1 =1
      AND jh.je_header_id = jl.je_header_id
      AND jl.code_combination_id = gcc.code_combination_id
      AND GCC.SEGMENT4 =nvl(p_account, gcc.segment4) --'256201' -- sbala
      AND jh.je_header_id = jir.je_header_id
      AND jl.je_line_num = jir.je_line_num
      AND jh.ledger_id = gle.ledger_id
      AND jh.je_source ='Receivables'
      AND jh.je_category Not In ('Receipts', 'Misc Receipts')
      AND jir.gl_sl_link_id = ael.gl_sl_link_id(+)
      AND jir.gl_sl_link_table = ael.gl_sl_link_table(+)
      AND ael.ae_line_num = xdl.ae_line_num(+)
      AND ael.ae_header_id = xdl.ae_header_id(+)
      AND ael.ae_header_id = aeh.ae_header_id(+)
      AND xlae.application_id(+) = aeh.application_id              --sbala
      AND xlae.event_id(+) = aeh.event_id                         -- sbala
      AND ar_dtls.dtl_entity_id(+) = xlae.entity_id                --sbala
      AND dist.line_id(+) =
             DECODE (
                xdl.source_distribution_type,
                'AR_DISTRIBUTIONS_ALL', xdl.source_distribution_id_num_1,
                NULL)
      AND ra_dist.cust_trx_line_gl_dist_id(+) =
             DECODE (xdl.source_distribution_type,
                     'AR_DISTRIBUTIONS_ALL', NULL,
                     xdl.source_distribution_id_num_1)
      AND gps.application_id = 101
      AND gps.period_name =nvl(p_period_name, gps.period_name) -- sbala
      AND gps.period_name = jh.period_name
      AND gps.ledger_id = gle.ledger_id
      AND gle.ledger_id = nvl(p_ledger_id, gle.ledger_id)   --sbala
      AND jh.je_batch_id = gb.je_batch_id
  ORDER by jh.je_category desc ,ar_dtls.dtl_id asc;
  -- Cursor gl_sla_ar_payments will pick SLA entries for je categories Receipts and Misc Receipts only.
  CURSOR gl_sla_ar_payments (p_req_id in number, p_gl_date_from in date, p_gl_date_to in date) is
  SELECT  jh.je_source SOURCE,
          jh.je_category, --field sequence 2
          jl.effective_date acc_date,
          jh.NAME entry,
          SUBSTR (jl.description, 1, 240) line_descr,
          jl.accounted_dr line_acctd_dr,
          jl.accounted_cr line_acctd_cr,
          jl.entered_dr line_ent_dr,
          jl.entered_cr line_ent_cr,
          ael.gl_sl_link_id ael_gl_sl_link_id,
          aeh.event_id aeh_event_id,
          xdl.ae_line_num xdl_ae_line_num,
          ael.ae_header_id ael_ae_header_id,
          aeh.entity_id aeh_entity_id,
          ar_dtls."DTL_ID",
          ar_dtls."TRANSACTION_NUMBER",
          ar_dtls."CUSTOMER_NUMBER",
          ar_dtls."CUSTOMER_OR_VENDOR",
          ar_dtls."DTL_ENTITY_ID",
          ar_dtls."RECEIPT_CLASS",
          ar_dtls."PAYMENT_METHOD",
          ar_dtls."RECEIPT_NUMBER",
          ar_dtls."RECEIPT_DATE",
          ar_dtls."RECEIPT_STATUS" RECEIPT_CURRENT_STATUS,
          ar_dtls."RECEIPT_STATE" RECEIPT_CURRENT_STATE,
         (select status
             from ar_cash_receipt_history_all crh
            where ar_dtls.receipt_number is not null
              and crh.cash_receipt_id=ar_dtls.dtl_id
              and crh.event_id=xlae.event_id
              and crh.cash_receipt_history_id =
              (select max(cash_receipt_history_id) from ar_cash_receipt_history_all crh1
               where crh.cash_receipt_id=crh1.cash_receipt_id
                 and crh.event_id=crh1.event_id)) activity_status,
          (
            select xlk.meaning
            from   xla_lookups xlk
            where  1 =1
              and xlk.lookup_type ='XLA_ACCOUNTING_CLASS'
              and xlk.lookup_code =ael.accounting_class_code
          )  RECEIPT_ACTIVITY_STATUS,
          ar_dtls."RECEIPT_COMMENTS",
          ar_dtls."CUSTOMER_RECEIPT_REF",
          ar_dtls."REMIT_BANK_ACCOUNT",
          ar_dtls."REVERSAL_CATEGORY",
          ar_dtls."REVERSAL_COMMENTS",
          ar_dtls."REVERSAL_DATE",
          ar_dtls."REVERSAL_REASON",
          ar_dtls."REVERSAL_REASON_CODE",
          ar_dtls."ADJUSMENT_NAME",
          ar_dtls."ADJUSTMENT_NUMBER"
         ,ar_dtls.card_brand CARD_BRAND
         ,ar_dtls.identifying_number IDENTIFYING_NUMBER
         ,ar_dtls.identifying_name IDENTIFYING_NAME
         ,ar_dtls.order_type ORDER_TYPE
         ,ar_dtls.sales_order SALES_ORDER
         ,ar_dtls.wc_branch WC_BRANCH
         ,DECODE (
             ra_dist.cust_trx_line_gl_dist_id,
             NULL, dist.AMOUNT_DR,
             TO_NUMBER (
                DECODE (
                   ra_dist.account_class,
                   'REC', DECODE (SIGN (NVL (ra_dist.amount, 0)),
                                  -1, NULL,
                                  NVL (ra_dist.amount, 0)),
                   DECODE (SIGN (NVL (ra_dist.amount, 0)),
                           -1, -NVL (ra_dist.amount, 0),
                           NULL))))
             Entered_DR,
          DECODE (
             ra_dist.cust_trx_line_gl_dist_id,
             NULL, dist.AMOUNT_cR,
             TO_NUMBER (
                DECODE (
                   ra_dist.account_class,
                   'REC', DECODE (SIGN (NVL (ra_dist.amount, 0)),
                                  -1, -NVL (ra_dist.amount, 0),
                                  NULL),
                   DECODE (SIGN (NVL (ra_dist.amount, 0)),
                           -1, NULL,
                           NVL (ra_dist.amount, 0)))))
             Entered_CR,
          DECODE (
             ra_dist.cust_trx_line_gl_dist_id,
             NULL, dist.ACCTD_AMOUNT_DR,
             TO_NUMBER (
                DECODE (
                   ra_dist.account_class,
                   'REC', DECODE (SIGN (NVL (ra_dist.amount, 0)),
                                  -1, NULL,
                                  NVL (ra_dist.acctd_amount, 0)),
                   DECODE (SIGN (NVL (ra_dist.amount, 0)),
                           -1, -NVL (ra_dist.acctd_amount, 0),
                           NULL))))
             Accounted_DR,
          DECODE (
             ra_dist.cust_trx_line_gl_dist_id,
             NULL, dist.ACCTD_AMOUNT_CR,
             TO_NUMBER (
                DECODE (
                   ra_dist.account_class,
                   'REC', DECODE (SIGN (NVL (ra_dist.amount, 0)),
                                  -1, -NVL (ra_dist.acctd_amount, 0),
                                  NULL),
                   DECODE (SIGN (NVL (ra_dist.amount, 0)),
                           -1, NULL,
                           NVL (ra_DIST.acctd_amount, 0)))))
             Accounted_CR,
          ael.unrounded_accounted_dr sla_line_accounted_dr,
          ael.unrounded_accounted_cr sla_line_accounted_cr,
            NVL (ael.unrounded_accounted_dr, 0)
          - NVL (ael.unrounded_accounted_cr, 0)
             sla_line_accounted_net,
          ael.unrounded_entered_dr sla_line_entered_dr,
          ael.unrounded_entered_cr sla_line_entered_cr,
            NVL (ael.unrounded_entered_dr, 0)
          - NVL (ael.unrounded_entered_cr, 0)
             sla_line_entered_net,
          xdl.unrounded_entered_cr sla_dist_entered_cr,
          xdl.unrounded_entered_dr sla_dist_entered_dr,
            NVL (xdl.unrounded_entered_cr, 0)
          - NVL (xdl.unrounded_entered_dr, 0)
             sla_dist_entered_net,
          xdl.unrounded_accounted_cr sla_dist_accounted_cr,
          xdl.unrounded_accounted_dr sla_dist_accounted_dr,
            NVL (xdl.unrounded_accounted_cr, 0)
          - NVL (xdl.unrounded_accounted_dr, 0)
             sla_dist_accounted_net,
          gcc.CONCATENATED_SEGMENTS gl_account_string,
          jh.period_name,
          NVL (dist.source_type, NULL) TYPE,
          gle.name,
          gb.name batch_name,
          GCC.SEGMENT4 GCC#50328#ACCOUNT,
          GCC.SEGMENT3 GCC#50328#COST_CENTER,
          GCC.SEGMENT6 GCC#50328#FURTURE_USE,
          GCC.SEGMENT7 GCC#50328#FUTURE_USE_2,
          GCC.SEGMENT2 GCC#50328#LOCATION,
          GCC.SEGMENT1 GCC#50328#PRODUCT,
          GCC.SEGMENT5 GCC#50328#PROJECT_CODE,
          GCC.SEGMENT4 GCC#50368#ACCOUNT,
          GCC.SEGMENT3 GCC#50368#DEPARTMENT,
          GCC.SEGMENT2 GCC#50368#DIVISION,
          GCC.SEGMENT6 GCC#50368#FUTURE_USE,
          GCC.SEGMENT1 GCC#50368#PRODUCT,
          GCC.SEGMENT5 GCC#50368#SUBACCOUNT,
          p_req_id request_id,
          Null payment_type_code,
          ar_dtls.org_id org_id,
          ar_dtls.instrument_type,
          ar_dtls.adjustment_reason,
          ar_dtls.adjustment_comments,
          ar_dtls.prism_return_payment_type,
          ar_dtls.invoice_source,
          ar_dtls.activity_name,
          to_number(null) receivable_application_id,
          ar_dtls.receivables_trx_id,
          ar_dtls.activity_date,
          xdl.source_distribution_id_num_1 source_dist_id_num_1,
          ar_dtls.receipt_type receipt_type,
          ar_dtls.original_cash_receipt_id original_cash_receipt_id,
          ar_dtls.payment_trxn_extn_id payment_trxn_extn_id,
          to_char(null) output_type,
          to_char(null) fin_branch,
          to_char(null) payment_type
     FROM gl_ledgers gle,
          gl_je_headers jh,
          gl_je_lines jl,
          gl_code_combinations_kfv gcc,
          gl_import_references jir,
          xla_distribution_links xdl,
          xla_ae_lines ael,
          xla_ae_headers aeh,
          ar_distributions_all dist,
          ra_cust_trx_line_gl_dist_all ra_dist,
          gl_period_statuses gps,
          gl_je_batches gb,
          xla_events xlae,                                             --sbala
          (
           SELECT crv.cash_receipt_id dtl_id,
                  TO_CHAR (NULL) transaction_number,
                  crv.customer_number customer_number,
                  crv.customer_name customer_or_vendor,
                  xlate.entity_id dtl_entity_id,
                  receipt_class_dsp receipt_class,
                  crv.payment_method_dsp payment_method,
                  crv.receipt_number receipt_number,
                  crv.receipt_date receipt_date,
                  crv.receipt_status_dsp receipt_status,
                  crv.state_dsp receipt_state,
                  crv.comments receipt_comments,
                  crv.customer_receipt_reference customer_receipt_ref,
                  crv.remit_bank_account remit_bank_account,
                  crv.reversal_category reversal_category,
                  crv.reversal_comments reversal_comments,
                  NVL (crv.reversal_date, TO_DATE (NULL)) reversal_date,
                  crv.reversal_reason reversal_reason,
                  crv.reversal_reason_code reversal_reason_code,
                  TO_CHAR (NULL) adjusment_name,
                  TO_CHAR (NULL) adjustment_number
                 ,null card_brand
                 ,null identifying_number
                 ,null identifying_name
                 ,null order_type
                 ,null sales_order
                 ,null wc_branch
                 ,crv.org_id org_id
                 ,null instrument_type
                 ,TO_CHAR (NULL) adjustment_reason
                 ,TO_CHAR (NULL) adjustment_comments
                 ,TO_CHAR (NULL) prism_return_payment_type
                 ,TO_CHAR (NULL) invoice_source
                 ,TO_CHAR (NULL) activity_name
                 ,TO_NUMBER (NULL) receivables_trx_id
                 ,crv.receipt_date activity_date
                 ,crv.type receipt_type
                 ,case
                   when (crv.type ='MISC' and nvl(crv.reference_type, 'NONE') ='RECEIPT' and crv.reference_id Is Not Null)  then crv.reference_id
                   else to_number(null)
                  end ORIGINAL_CASH_RECEIPT_ID
                 ,crv.payment_trxn_extension_id payment_trxn_extn_id
             FROM ar_cash_receipts_v crv,
                  xla_transaction_entities xlate,
                  fnd_application fndapp
            WHERE 1 = 1
                  AND fndapp.application_short_name = 'AR'
                  AND xlate.application_id = fndapp.application_id --do not miss this condition at any cost
                  AND xlate.ledger_id   =p_ledger_id
                  AND xlate.entity_code = 'RECEIPTS'
                  AND xlate.source_id_int_1 = crv.cash_receipt_id
                  AND (
                        ( crv.state_dsp <>'Reversed' AND crv.gl_date between p_gl_date_from AND p_gl_date_to )
                        OR
                            (
                             (crv.state_dsp ='Reversed' and 2 =2)
                            )
                      )
        ) ar_dtls
    WHERE 1 =1
      AND jh.je_header_id = jl.je_header_id
      AND jl.code_combination_id = gcc.code_combination_id
      AND GCC.SEGMENT4 =nvl(p_account, gcc.segment4) --'256201' -- sbala
      AND jh.je_header_id = jir.je_header_id
      AND jl.je_line_num = jir.je_line_num
      AND jh.ledger_id = gle.ledger_id
      AND jh.je_source ='Receivables'
      AND jh.je_category In ('Receipts', 'Misc Receipts')
      AND jir.gl_sl_link_id = ael.gl_sl_link_id(+)
      AND jir.gl_sl_link_table = ael.gl_sl_link_table(+)
      AND ael.ae_line_num = xdl.ae_line_num(+)
      AND ael.ae_header_id = xdl.ae_header_id(+)
      AND ael.ae_header_id = aeh.ae_header_id(+)
      AND xlae.application_id(+) = aeh.application_id              --sbala
      AND xlae.event_id(+) = aeh.event_id                         -- sbala
      AND ar_dtls.dtl_entity_id(+) = xlae.entity_id                --sbala
      AND dist.line_id(+) =
             DECODE (
                xdl.source_distribution_type,
                'AR_DISTRIBUTIONS_ALL', xdl.source_distribution_id_num_1,
                NULL)
      AND ra_dist.cust_trx_line_gl_dist_id(+) =
             DECODE (xdl.source_distribution_type,
                     'AR_DISTRIBUTIONS_ALL', NULL,
                     xdl.source_distribution_id_num_1)
      AND gps.application_id = 101
      AND gps.period_name =nvl(p_period_name, gps.period_name) -- sbala
      AND gps.period_name = jh.period_name
      AND gps.ledger_id = gle.ledger_id
      AND gle.ledger_id = nvl(p_ledger_id, gle.ledger_id)   --sbala
      AND jh.je_batch_id = gb.je_batch_id
  /*
   ORDER BY 2, 15, 62, 26, 21, 27, 91 IS SHOWN BELOW WITH FIELD NAMES.
   JE_CATEGORY
   DTL_ID
   TYPE
   ACTIVITY_STATUS
   PAYMENT_METHOD
   RECEIPT_ACTIVITY_STATUS
   RECEIPT_TYPE
  */
  ORDER by 2, 15, 62, 26, 21, 27, 91;
  --
  TYPE gl_sla_ar_tbl IS TABLE OF gl_sla_ar%ROWTYPE INDEX BY BINARY_INTEGER;
  gl_sla_ar_row gl_sla_ar_tbl;

  cursor transform_adj_jrnls is
    select source
    ,je_category
    ,acc_date
    ,entry
    ,line_descr
    ,line_acctd_dr
    ,line_acctd_cr
    ,line_ent_dr
    ,line_ent_cr
    ,ael_gl_sl_link_id
    ,aeh_event_id
    ,xdl_ae_line_num
    ,ael_ae_header_id
    ,aeh_entity_id
    ,dtl_id
    ,transaction_number
    ,customer_number
    ,customer_or_vendor
    ,dtl_entity_id
    ,to_char(null) receipt_class
    ,to_char(null) payment_method
    ,to_char(null) receipt_number
    ,to_date(null) receipt_date
    ,to_char(null) receipt_current_status
    ,to_char(null) receipt_current_state
    ,to_char(null) activity_status
    ,receipt_activity_status
    ,to_char(null) RECEIPT_COMMENTS
    ,to_char(null) CUSTOMER_RECEIPT_REF
    ,to_char(null) REMIT_BANK_ACCOUNT
    ,to_char(null) REVERSAL_CATEGORY
    ,to_char(null) REVERSAL_COMMENTS
    ,to_date(null) REVERSAL_DATE
    ,to_char(null) REVERSAL_REASON
    ,to_char(null) REVERSAL_REASON_CODE
    ,adjusment_name
    ,a.adjustment_number
    ,to_char(null) CARD_BRAND
    ,to_char(null) IDENTIFYING_NUMBER
    ,to_char(null) IDENTIFYING_NAME
    ,order_type
    ,sales_order
    ,wc_branch
    ,to_number(null) entered_dr
    ,to_number(null) entered_cr
    ,to_number(null) accounted_dr
    ,to_number(null) accounted_cr
    ,sla_line_accounted_dr
    ,sla_line_accounted_cr
    ,sla_line_accounted_net
    ,sla_line_entered_dr
    ,sla_line_entered_cr
    ,sla_line_entered_net
    ,to_number(null) sla_dist_entered_cr
    ,to_number(null) sla_dist_entered_dr
    ,to_number(null) sla_dist_entered_net
    ,to_number(null) sla_dist_accounted_cr
    ,to_number(null) sla_dist_accounted_dr
    ,to_number(null) sla_dist_accounted_net --we will calculate the sum for this field in the program logic.
    ,gl_account_string
    ,period_name
    ,type
    ,ledger_name
    ,batch_name
    ,gcc#50328#account
    ,gcc#50328#cost_center
    ,gcc#50328#furture_use
    ,gcc#50328#future_use_2
    ,gcc#50328#location
    ,gcc#50328#product
    ,gcc#50328#project_code
    ,gcc#50368#account
    ,gcc#50368#department
    ,gcc#50368#division
    ,gcc#50368#future_use
    ,gcc#50368#product
    ,gcc#50368#subaccount
    ,request_id
    ,payment_type_code
    ,org_id
    ,instrument_type
    ,adjustment_reason
    ,adjustment_comments
    ,prism_return_payment_type
    ,invoice_source
    ,activity_name
    ,receivable_application_id
    ,receivables_trx_id
    ,activity_date
    ,source_dist_id_num_1
    ,receipt_type
    ,original_cash_receipt_id
    ,payment_trxn_extn_id
    ,output_type
    ,fin_branch
    ,payment_type
    from xxcus.xxhds_glsla_ar_drilldown a
        ,(
           select min(a.source_dist_id_num_1) src_dist_id_num_1, adjustment_number
           from   xxcus.xxhds_glsla_ar_drilldown a
           where  1 =1
           group by adjustment_number
         ) unique_adj
    where 1 =1
      and a.je_category          ='Adjustment'
      and a.adjustment_number    =unique_adj.adjustment_number
      and a.source_dist_id_num_1 =unique_adj.src_dist_id_num_1;

  type transform_adj_jrnls_tbl is table of transform_adj_jrnls%rowtype index by binary_integer;
  transform_adj_jrnls_row transform_adj_jrnls_tbl;

  cursor Cash_Recon_Indexes is
    select 'ALTER INDEX '||OWNER||'.'||INDEX_NAME||' REBUILD' idx_sql
    from   all_indexes
    where  1 =1
      and  table_owner ='XXCUS'
      and  table_name ='XXHDS_GLSLA_AR_DRILLDOWN'
    order by index_name asc;

  lc_request_data VARCHAR2(20) :='';
  ln_request_id   NUMBER :=0;
  n_req_id NUMBER :=0;
  p_limit NUMBER :=5000;

  v_prev_sla_type varchar2(30);
  v_card_brand  XXCUS.XXHDS_GLSLA_AR_DRILLDOWN.card_brand%TYPE :=Null;
  v_id_number   XXCUS.XXHDS_GLSLA_AR_DRILLDOWN.identifying_number%TYPE :=Null;
  v_id_name     XXCUS.XXHDS_GLSLA_AR_DRILLDOWN.identifying_name%TYPE :=Null;
  v_order_type  XXCUS.XXHDS_GLSLA_AR_DRILLDOWN.order_type%TYPE :=Null;
  n_sales_order XXCUS.XXHDS_GLSLA_AR_DRILLDOWN.sales_order%TYPE :=Null;
  v_wc_branch   XXCUS.XXHDS_GLSLA_AR_DRILLDOWN.wc_branch%TYPE :=Null;
  v_pmt_type_code XXCUS.XXHDS_GLSLA_AR_DRILLDOWN.payment_type_code%TYPE :=Null;
  v_instr_type   XXCUS.XXHDS_GLSLA_AR_DRILLDOWN.instrument_type%TYPE :=Null;

  TYPE t_ar_lookups_table
   IS
      TABLE OF NUMBER(22)
         INDEX BY BINARY_INTEGER;

   pg_ar_lookups_rec   t_ar_lookups_table;
   l_hash_value        NUMBER;

   l_dollar_amount number;
   l_payment_amount number;
   n_prev_cash_receipt_id number :=null;
   n_rec_app_id number :=0;
   n_rec_trx_id number :=0;
   v_rcpt_wrt_off_name ar_receivable_applications_v.trx_number%type :=Null;
   v_rcpt_wrt_off_activity ar_receivable_applications_v.rec_activity_name%type :=Null;
   d_activity_date date;
   n_cr_id number :=0;
   p_gl_date_from date;
   p_gl_date_to   date;
   b_adj_proceed  boolean;
   current_sql varchar2(2000);
   n_cr_hist_id number :=0;
   n_app_ps_id number :=0;
   n_header_id number :=0;
   v_header_id    ar_receivable_applications_all.attribute1%type;
   v_order_number ar_receivable_applications_all.attribute2%type;
   v_source_table ar_distributions_all.source_table%type :=Null;
   --
begin

   -- Store the concurrent request id
   n_req_id :=fnd_global.conc_request_id;

   print_log('Parameter p_ledger_id ='||p_ledger_id);
   print_log('Parameter p_period_name ='||p_period_name);
   print_log('Parameter p_account ='||p_account);
   -- Get period start and end dates
   BEGIN
    select start_date
          ,end_date
    into   p_gl_date_from
          ,p_gl_date_to
    from   gl_period_statuses
    where  1=1
      and  application_id =222 --Owned by AR application only
      and  period_name    =p_period_name
      and  ledger_id      =p_ledger_id;
   EXCEPTION
    WHEN NO_DATA_FOUND THEN
     print_log('No row returned in fetching start and end dates for period ='||p_period_name);
     p_gl_date_from :=null;
     p_gl_date_to   :=null;
    WHEN OTHERS THEN
     print_log('Issue in fetching start and end dates for period ='||p_period_name);
     p_gl_date_from :=null;
     p_gl_date_to   :=null;
   END;
   print_log('Parameter p_gl_date_from ='||to_char(p_gl_date_from, 'mm/dd/yyyy'));
   print_log('Parameter p_gl_date_to ='||to_char(p_gl_date_to, 'mm/dd/yyyy'));
   print_log('');
   --
   --flush the table and lets repopulate them.
   BEGIN
    execute immediate 'TRUNCATE TABLE XXCUS.XXHDS_GLSLA_AR_DRILLDOWN';
   EXCEPTION
    WHEN OTHERS THEN
     print_log('Error in truncating table XXCUS.XXHDS_GLSLA_AR_DRILLDOWN '||sqlerrm);
   END;
   --Run the cursor and populate the table with chunks of data for je_category other than 'Receipts', 'Misc Receipts'
   OPEN gl_sla_ar(n_req_id, p_gl_date_from, p_gl_date_to);
     LOOP --endloop@100
      FETCH gl_sla_ar BULK COLLECT INTO gl_sla_ar_row LIMIT p_limit;
      EXIT WHEN gl_sla_ar_row.COUNT =0;

      IF gl_sla_ar_row.COUNT >0 THEN --if@100
        --
        print_log('@cursor copy [1], Records found, total ='||gl_sla_ar_row.COUNT);
        -- Insert into the xxcus table
        begin
         forall recs in gl_sla_ar_row.first .. gl_sla_ar_row.last
          insert into xxcus.xxhds_glsla_ar_drilldown values gl_sla_ar_row(recs);
        exception
         when others then
          print_log('Issue in forall insert, cursor copy [1], message ='||sqlerrm);
        end;
      ELSE
       print_log('No records found @gl_sla_ar cursor.');
      END IF; --end if@100

     END LOOP; --endloop@100
   CLOSE gl_sla_ar;
   -- free up the collection tables as we are going to populate it again
   if gl_sla_ar_row.count >0 then
       gl_sla_ar_row.delete;
   else
       null;
   end if;
   --
   --Run the cursor and populate the table with chunks of data for je_category 'Receipts', 'Misc Receipts'
   OPEN gl_sla_ar_payments (n_req_id, p_gl_date_from, p_gl_date_to);
     LOOP --endloop@100
      FETCH gl_sla_ar_payments BULK COLLECT INTO gl_sla_ar_row LIMIT p_limit;
      EXIT WHEN gl_sla_ar_row.COUNT =0;

      IF gl_sla_ar_row.COUNT >0 THEN --if@100
        --
        print_log('@cursor copy [2], Records found, total ='||gl_sla_ar_row.COUNT);
        --
        FOR Idx IN  gl_sla_ar_row.FIRST .. gl_sla_ar_row.LAST LOOP  --end loop @106
          --
          If gl_sla_ar_row(Idx).type != 'ACTIVITY' Then
             Null;
          Else
          --
           If gl_sla_ar_row(Idx).Dtl_Id Is Not Null then
            --
                 Begin
                  --
                  begin
                   --
                    Select b.source_id, b.source_table
                    Into   n_rec_app_id, v_source_table
                    From   ar_distributions_all b
                    Where 1 =1
                      And b.line_id      =gl_sla_ar_row(Idx).source_dist_id_num_1;
                   --
                  exception
                   when no_data_found then
                    v_source_table :=Null;
                   when others then
                    print_log('v_source_table returned null, message ='||sqlerrm);
                    v_source_table :=Null;
                  end;
                  --
                  if v_source_table ='RA' then
                    Select a.applied_payment_schedule_id
                          --,a.cash_receipt_id
                          ,a.cash_receipt_history_id
                          ,a.receivables_trx_id
                          ,a.apply_date
                          ,a.attribute1
                          ,a.attribute2
                    Into n_app_ps_id
                        --,gl_sla_ar_row(Idx).dtl_id
                        ,n_cr_hist_id
                        ,n_rec_trx_id
                        ,d_activity_date
                        ,v_header_id
                        ,v_order_number
                    From ar_receivable_applications_all a
                    Where 1 =1
                      And a.receivable_application_id =n_rec_app_id;
                   --
                  elsif v_source_table ='CRH' then
                   Null;
                  else
                   n_rec_app_id :=Null;
                   v_rcpt_wrt_off_name :=Null;
                   n_rec_trx_id :=Null;
                   v_rcpt_wrt_off_activity :=Null;
                   d_activity_date :=Null;
                   v_order_number :=Null;
                   n_app_ps_id :=0;
                  end if;
                   --
                   if sign(n_app_ps_id) >0 then
                    v_rcpt_wrt_off_activity  :=get_rec_trx_name(n_rec_trx_id);
                   elsif n_app_ps_id =0 then
                    v_rcpt_wrt_off_activity  :=Null;
                   else
                    -- <0 means we fetch the standard trx number from ar_payments_schedules_all
                    -- like write off, refund and OM prepayments
                    v_rcpt_wrt_off_name  :=get_ps_trx_number(n_app_ps_id);
                   end if;
                   --
                   gl_sla_ar_row(Idx).receivable_application_id :=n_rec_app_id;
                   gl_sla_ar_row(Idx).adjusment_name            :=v_rcpt_wrt_off_name;
                   gl_sla_ar_row(Idx).receivables_trx_id        :=n_rec_trx_id;
                   gl_sla_ar_row(Idx).activity_name             :=v_rcpt_wrt_off_activity;
                   gl_sla_ar_row(Idx).activity_date             :=d_activity_date;
                   gl_sla_ar_row(Idx).sales_order               :=v_order_number;
                   --
                   begin
                    select to_number(v_header_id)
                    into   n_header_id
                    from   dual;
                    if n_header_id >0 then
                        select c.order_type
                              ,c.ship_from
                        into
                               gl_sla_ar_row(Idx).order_type
                              ,gl_sla_ar_row(Idx).wc_branch
                        from   oe_order_headers_v c
                        where  1 =1
                          and  c.header_id =n_header_id;
                    end if;
                   exception
                    when no_data_found then
                     Null;
                    when too_many_rows then
                     print_log('error in fetching order info, ard_line_id ='||gl_sla_ar_row(Idx).source_dist_id_num_1);
                     print_log('TooManyRows error in fetching order info, v_header_id ='||v_header_id);
                    when others then
                     print_log('error in fetching order info, ard_line_id ='||gl_sla_ar_row(Idx).source_dist_id_num_1);
                     print_log('Other Errors in fetching order info, message ='||sqlerrm);
                   end;
                   --
                   --
                 Exception
                  When No_Data_Found Then
                    Null;
                  When Too_Many_Rows Then
                    print_log('@When Too Many Rows, Issue in fetching activity details for cash_receipt_id ='||gl_sla_ar_row(Idx).Dtl_Id
                                 ||', ar_distributions_all.line_id ='||gl_sla_ar_row(Idx).source_dist_id_num_1
                             );
                  When Others Then
                    print_log('@When Others, Issue in fetching activity details for cash_receipt_id ='||gl_sla_ar_row(Idx).Dtl_Id
                                 ||', ar_distributions_all.line_id ='||gl_sla_ar_row(Idx).source_dist_id_num_1
                             );
                    print_log('@When Others, Issue in fetching activity details for cash_receipt_id ='||gl_sla_ar_row(Idx).Dtl_Id
                                 ||', message ='||sqlerrm
                             );
                 End;
            --
           Else
            -- dtl_id is null bcoz the write off activity was initiated as a refund and the SLA record failed to fetch it
            -- in the first big SQL call. We just have to pull all the required data elements and put it back in the
            -- plsql collection table for use.
            --
            --
                 Begin
                    Select a.receivable_application_id
                          ,a.applied_payment_schedule_id
                          ,a.cash_receipt_id
                          ,a.cash_receipt_history_id
                          ,a.receivables_trx_id
                          ,a.apply_date
                          ,a.attribute1
                          ,a.attribute2
                    Into n_rec_app_id
                        ,n_app_ps_id
                        ,gl_sla_ar_row(Idx).dtl_id
                        ,n_cr_hist_id
                        ,n_rec_trx_id
                        ,d_activity_date
                        ,v_header_id
                        ,v_order_number
                    From ar_distributions_all b
                        ,ar_receivable_applications_all a
                    Where 1 =1
                      And b.line_id                     =gl_sla_ar_row(Idx).source_dist_id_num_1
                      And a.receivable_application_id   =b.source_id;

                   v_rcpt_wrt_off_name  :=get_ps_trx_number(n_app_ps_id);

                   v_rcpt_wrt_off_activity  :=get_rec_trx_name(n_rec_trx_id);

                   gl_sla_ar_row(Idx).receivable_application_id :=n_rec_app_id;
                   gl_sla_ar_row(Idx).adjusment_name            :=v_rcpt_wrt_off_name;
                   gl_sla_ar_row(Idx).receivables_trx_id        :=n_rec_trx_id;
                   gl_sla_ar_row(Idx).activity_name             :=v_rcpt_wrt_off_activity;
                   gl_sla_ar_row(Idx).activity_date             :=d_activity_date;
                   gl_sla_ar_row(Idx).sales_order               :=v_order_number;
                   --
                   begin
                    select to_number(v_header_id)
                    into   n_header_id
                    from   dual;
                    if n_header_id >0 then
                        select c.order_type
                              ,c.ship_from
                        into
                               gl_sla_ar_row(Idx).order_type
                              ,gl_sla_ar_row(Idx).wc_branch
                        from   oe_order_headers_v c
                        where  1 =1
                          and  c.header_id =n_header_id;
                    end if;
                   exception
                    when no_data_found then
                     Null;
                    when too_many_rows then
                     print_log('error in fetching order info, ard_line_id ='||gl_sla_ar_row(Idx).source_dist_id_num_1);
                     print_log('TooManyRows error in fetching order info, v_header_id ='||v_header_id);
                    when others then
                     print_log('error in fetching order info, ard_line_id ='||gl_sla_ar_row(Idx).source_dist_id_num_1);
                     print_log('Other Errors in fetching order info, message ='||sqlerrm);
                   end;
                   --
                   /*
                   begin
                    select status
                    into   gl_sla_ar_row(Idx).activity_status
                    from   ar_cash_receipt_history_all crh
                    where  1 =1
                      and  cash_receipt_history_id =n_cr_hist_id;
                   exception
                    when too_many_rows then
                     print_log('ar_dist_all.line_id fetched more than one cash receipt history id, line_id ='||gl_sla_ar_row(Idx).source_dist_id_num_1);
                     gl_sla_ar_row(Idx).activity_status :=null;
                    when others then
                     print_log('ar_dist_all.line_id critical error in fetching cash receipt history id, line_id ='||gl_sla_ar_row(Idx).source_dist_id_num_1);
                     gl_sla_ar_row(Idx).activity_status :=null;
                   end;
                   */
                   --
                   begin
                    select
                           crv.cash_receipt_id
                          ,crv.customer_number
                          ,crv.customer_name
                          ,crv.receipt_class_dsp
                          ,crv.payment_method_dsp
                          ,crv.receipt_number
                          ,crv.receipt_date
                          ,crv.comments
                          ,crv.customer_receipt_reference
                          ,crv.remit_bank_account
                          ,crv.reversal_category reversal_category
                          ,crv.reversal_comments reversal_comments
                          ,NVL(crv.reversal_date, TO_DATE (NULL)) reversal_date
                          ,crv.reversal_reason reversal_reason
                          ,crv.reversal_reason_code reversal_reason_code
                          ,crv.receipt_status_dsp
                          ,crv.state_dsp
                    into
                         gl_sla_ar_row(Idx).dtl_id
                        ,gl_sla_ar_row(Idx).customer_number
                        ,gl_sla_ar_row(Idx).customer_or_vendor
                        ,gl_sla_ar_row(Idx).receipt_class
                        ,gl_sla_ar_row(Idx).payment_method
                        ,gl_sla_ar_row(Idx).receipt_number
                        ,gl_sla_ar_row(Idx).receipt_date
                        ,gl_sla_ar_row(Idx).receipt_comments
                        ,gl_sla_ar_row(Idx).customer_receipt_ref
                        ,gl_sla_ar_row(Idx).remit_bank_account
                        ,gl_sla_ar_row(Idx).reversal_category
                        ,gl_sla_ar_row(Idx).reversal_comments
                        ,gl_sla_ar_row(Idx).reversal_date
                        ,gl_sla_ar_row(Idx).reversal_reason
                        ,gl_sla_ar_row(Idx).reversal_reason_code
                        ,gl_sla_ar_row(Idx).receipt_current_status
                        ,gl_sla_ar_row(Idx).receipt_current_state
                    from   ar_cash_receipts_v crv
                    where  1 =1
                      and  crv.cash_receipt_id =gl_sla_ar_row(Idx).dtl_id;
                   exception
                    when too_many_rows then
                     print_log('Issue in fetch of receipt details, cash_receipt_id ='||gl_sla_ar_row(Idx).dtl_id);
                    when others then
                     print_log('Other errors in fetch of receipt details, cash_receipt_id ='||gl_sla_ar_row(Idx).dtl_id||', message ='||sqlerrm);
                   end;
                   --
                 Exception
                  When No_Data_Found Then
                    Null;
                  When Too_Many_Rows Then
                    print_log('dtl_id is blank, ar_distributions_all.line_id ='||gl_sla_ar_row(Idx).source_dist_id_num_1
                             );
                  When Others Then
                    print_log('dtl_id is blank, ar_distributions_all.line_id ='||gl_sla_ar_row(Idx).source_dist_id_num_1
                             );
                    print_log('@When Others, dtl_id is blank, message ='||sqlerrm);
                 End;
            --
           End If;
           --
          End If;
         --
           If (
                gl_sla_ar_row(Idx).receipt_type ='MISC' and
                gl_sla_ar_row(Idx).type != 'ACTIVITY' and
                gl_sla_ar_row(Idx).payment_method NOT LIKE '%PRISM%'
              ) Then
             --
             Begin
                select a.card_issuer_code
                      ,a.card_number
                      ,a.card_holder_name
                      ,a.payment_channel_code
                      ,a.returntxn_instrtype
                Into v_card_brand
                    ,v_id_number
                    ,v_id_name
                    ,v_pmt_type_code
                    ,v_instr_type
                from apps.xxhds_iby_trxn_extensions_v a
                where 1 =1
                and a.trxn_extension_id =gl_sla_ar_row(Idx).payment_trxn_extn_id
                and a.origin_application_id =222
                and a.trxn_ref_number1 ='RECEIPT'
                and a.trxn_ref_number2 =gl_sla_ar_row(Idx).receipt_number
                and rownum <2;
             Exception
              When No_Data_Found Then
                v_card_brand    :=Null;
                v_id_number     :=Null;
                v_id_name       :=Null;
                v_pmt_type_code :=Null;
                v_instr_type    :=Null;
              When Too_Many_Rows Then
                v_card_brand    :=Null;
                v_id_number     :=Null;
                v_id_name       :=Null;
                v_pmt_type_code :=Null;
                v_instr_type    :=Null;
              When Others Then
                print_log('2. OTHER ERRORS, RECEIPT_TYPE =MISC / TYPE <>ACTIVITY /PAYMENT_METHOD <>%PRISM, TRXN_EXTENSION_ID =>'
                           ||gl_sla_ar_row(Idx).payment_trxn_extn_id
                           ||', RECEIPT_NUMBER =>'||gl_sla_ar_row(Idx).receipt_number
                         );
                v_card_brand    :=Null;
                v_id_number     :=Null;
                v_id_name       :=Null;
                v_pmt_type_code :=Null;
                v_instr_type    :=Null;
             End;
             --
             Begin
                select c.order_number return_sales_order
                      ,c.order_type
                      ,c.ship_from
                into   n_sales_order
                      ,v_order_type
                      ,v_wc_branch
                from   ar_cash_receipts_all    a
                      ,xxwc_om_cash_refund_tbl b
                      ,oe_order_headers_v      c
                where  1 =1
                  and  a.cash_receipt_id    =gl_sla_ar_row(Idx).Dtl_Id
                  and  a.reference_type     ='RECEIPT'
                  and  b.cash_receipt_id(+) =a.reference_id --orig_cash_receipt_id
                  and  c.header_id(+)       =b.return_header_id
                  and  abs(b.refund_amount) =abs(gl_sla_ar_row(Idx).sla_dist_accounted_net);
             Exception
              When No_Data_Found Then
                v_order_type    :=Null;
                n_sales_order   :=Null;
                v_wc_branch     :='*0*';
              When Too_Many_Rows Then
                v_order_type    :=Null;
                n_sales_order   :=Null;
                v_wc_branch     :='*1*';
              When Others Then
                print_log('2: OTHER ERRORS, RECEIPT_TYPE =MISC / TYPE <>ACTIVITY /PAYMENT_METHOD <>%PRISM, RETURN CASH_RECEIPT_ID ='||gl_sla_ar_row(Idx).Dtl_Id);
                v_order_type    :=Null;
                n_sales_order   :=Null;
                v_wc_branch     :='*2*';
             End;
            --
             --
             gl_sla_ar_row(Idx).card_brand :=v_card_brand;
             gl_sla_ar_row(Idx).identifying_number :=v_id_number;
             gl_sla_ar_row(Idx).identifying_name :=v_id_name;
             gl_sla_ar_row(Idx).order_type :=v_order_type;
             gl_sla_ar_row(Idx).sales_order :=n_sales_order;
             gl_sla_ar_row(Idx).wc_branch :=v_wc_branch;
             gl_sla_ar_row(Idx).payment_type_code :=v_pmt_type_code;
             gl_sla_ar_row(Idx).instrument_type   :=v_instr_type;
            --
             --
           Elsif (
                   gl_sla_ar_row(Idx).receipt_type <>'MISC' and
                   gl_sla_ar_row(Idx).type != 'ACTIVITY' and
                   gl_sla_ar_row(Idx).payment_method NOT LIKE '%PRISM%'
                 ) Then
             --print_log('@ 201');
             Begin
                Select om_dtls.cc_brand
                      ,om_dtls.identifying_number
                      ,om_dtls.identifying_name
                      ,om_dtls.order_type
                      ,om_dtls.order_number
                      ,om_dtls.wc_branch
                      ,om_dtls.payment_type_code
                      ,om_dtls.instrument_type
                Into v_card_brand
                    ,v_id_number
                    ,v_id_name
                    ,v_order_type
                    ,n_sales_order
                    ,v_wc_branch
                    ,v_pmt_type_code
                    ,v_instr_type
                From apps.xxhds_ar_om_pmt_details om_dtls
                Where 1 =1
                  And cash_receipt_id =gl_sla_ar_row(Idx).Dtl_Id
                  And rownum <2;
             Exception
              When NO_Data_Found Then
                get_attributes
                 (
                   gl_sla_ar_row(Idx).Dtl_Id --cash receipt id
                  ,gl_sla_ar_row(Idx).source_dist_id_num_1 --source dist id num 1
                  ,v_wc_branch
                  ,n_sales_order
                  ,v_order_type
                  ,v_pmt_type_code
                 );
                v_card_brand    :=Null;
                v_id_number     :=Null;
                v_id_name       :=Null;
                v_instr_type    :=Null;
              When Too_Many_Rows Then
                v_card_brand    :=Null;
                v_id_number     :=Null;
                v_id_name       :=Null;
                v_order_type    :=Null;
                n_sales_order   :=Null;
                v_wc_branch     :='*4*';
                v_pmt_type_code :=Null;
                v_instr_type    :=Null;
              When Others Then
                v_card_brand    :=Null;
                v_id_number     :=Null;
                v_id_name       :=Null;
                v_order_type    :=Null;
                n_sales_order   :=Null;
                v_wc_branch     :='*5*';
                v_pmt_type_code :=Null;
                v_instr_type    :=Null;
             End;
             --
             --
             gl_sla_ar_row(Idx).card_brand :=v_card_brand;
             gl_sla_ar_row(Idx).identifying_number :=v_id_number;
             gl_sla_ar_row(Idx).identifying_name :=v_id_name;
             gl_sla_ar_row(Idx).order_type :=v_order_type;
             gl_sla_ar_row(Idx).sales_order :=n_sales_order;
             gl_sla_ar_row(Idx).wc_branch :=v_wc_branch;
             gl_sla_ar_row(Idx).payment_type_code :=v_pmt_type_code;
             gl_sla_ar_row(Idx).instrument_type   :=v_instr_type;
            --
           Elsif gl_sla_ar_row(Idx).payment_method LIKE '%PRISM%' then
             Begin
                select substr(b.interface_header_attribute15, 1, 30)
                      ,substr(b.interface_header_attribute7, 1, 3)
                into   n_sales_order
                      ,v_wc_branch
                from   ar_receivable_applications_v a
                      ,ra_customer_trx_all          b
                where  1 =1
                  and  a.cash_receipt_id =gl_sla_ar_row(Idx).Dtl_Id
                  and  b.customer_trx_id =a.customer_trx_id;
             Exception
              When NO_Data_Found Then
                n_sales_order   :=Null;
                v_wc_branch     :=Null;
              When Too_Many_Rows Then
                n_sales_order   :=Null;
                v_wc_branch     :=Null;
              When Others Then
                n_sales_order   :=Null;
                v_wc_branch     :=Null;
             End;
             --
             gl_sla_ar_row(Idx).sales_order       :=n_sales_order;
             gl_sla_ar_row(Idx).wc_branch         :=v_wc_branch;
             gl_sla_ar_row(Idx).payment_type      :='PRISM';
             gl_sla_ar_row(Idx).payment_type_code :='PRISM';
            --
           Else
            Null;
           End If;
         --
        END LOOP; --end loop @106
        -- Insert into the xxcus table
        begin
         forall recs in gl_sla_ar_row.first .. gl_sla_ar_row.last
          insert into xxcus.xxhds_glsla_ar_drilldown values gl_sla_ar_row(recs);
        exception
         when others then
          print_log('Issue in forall insert, cursor copy [2], message ='||sqlerrm);
        end;
      ELSE
       print_log('No records found @gl_sla_ar_payments cursor.');
      END IF; -- gl_sla_ar_row.COUNT >0 THEN

     END LOOP; --endloop@100
   CLOSE gl_sla_ar_payments;
   --
   begin
    open  transform_adj_jrnls;
    fetch transform_adj_jrnls bulk collect into transform_adj_jrnls_row;
    close transform_adj_jrnls;
    if transform_adj_jrnls_row.count >0 then
        -- delete the existing duplicate adjustment journals for a given transaction
        begin
         for idx in transform_adj_jrnls_row.first .. transform_adj_jrnls_row.last loop
           begin
             select sum(sla_dist_accounted_net)
             into   transform_adj_jrnls_row(idx).sla_dist_accounted_net
             from   xxcus.xxhds_glsla_ar_drilldown
             where  1 =1
               and  je_category =transform_adj_jrnls_row(idx).je_category
               and  adjustment_number =transform_adj_jrnls_row(idx).adjustment_number;
           exception
            when others then
              print_log('Issue in fetching total SLA amount for adjustment number ='||transform_adj_jrnls_row(idx).adjustment_number||', message ='||sqlerrm);
           end;
         end loop;
         b_adj_proceed :=TRUE;
        exception
         when others then
           b_adj_proceed :=FALSE;
           print_log('Issue in fetching duplicate adjustment journals tagged for deletion, message ='||sqlerrm);
        end;
        -- delete all adjustment journals and insert the summary ones
        begin
        --
         if (b_adj_proceed) then
           --
           delete xxcus.xxhds_glsla_ar_drilldown
           where  1 =1
             and  je_category ='Adjustment';
            --
            -- Insert into the xxcus table
            begin
             forall recs in transform_adj_jrnls_row.first .. transform_adj_jrnls_row.last
              insert into xxcus.xxhds_glsla_ar_drilldown values transform_adj_jrnls_row(recs);
            exception
             when others then
              print_log('Issue in forall insert of summarized adjustment journals, message ='||sqlerrm);
            end;
           --
         end if;
         --
        exception
            when others then
              print_log('Issue in deleting duplicate adjustment journals, message ='||sqlerrm);
        end;
       --
    end if;
   exception
    when others then
     print_log('Issue in updating VISA/MASTER/DISCOVER credit card payment type code text, message ='||sqlerrm);
     rollback to update_pmt_type;
   end;
   -- free up the collection tables as we do not need it anymore
   if gl_sla_ar_row.count >0 then
       gl_sla_ar_row.delete;
   else
       null;
   end if;
   --
   -- REBUILD indexes
   begin
    for idx_rec in Cash_Recon_Indexes loop
     current_sql :=idx_rec.idx_sql;
     execute immediate idx_rec.idx_sql;
    end loop;
   exception
    when others then
     print_log('Current Index Rebuild SQL =>'||current_sql);
     print_log('Current Index Rebuild SQL Error =>'||sqlerrm);
   end;
   --
   -- Update OUTPUT_TYPE field
   begin
    savepoint set_output_type;
    update xxcus.xxhds_glsla_ar_drilldown
       set output_type =
        (
              case
               when (je_category ='Debit Memos' OR
                      (payment_method like 'WC%'
                        and (activity_status IS NOT NULL AND activity_status <>'REVERSED'))
                    )
                then ('PRISM')
            --
               when
                 (
                   (
                     PAYMENT_METHOD IS NOT NULL AND PAYMENT_METHOD NOT LIKE 'WC%' AND
                      (
                       ACTIVITY_STATUS IS NOT NULL AND ACTIVITY_STATUS <>'REVERSED'
                      )
                   )
                   OR
                   (
                     PAYMENT_METHOD IS NOT NULL AND PAYMENT_METHOD NOT LIKE 'WC%' AND
                      (
                       ACTIVITY_STATUS IS NULL AND ACTIVITY_NAME LIKE '%101001-Branch Refund%'
                      )
                   )                   
                   OR
                   (
                     je_category ='Adjustment' AND
                     adjustment_comments LIKE 'CLEARS CURRENT MONTH VAR%'
                   )
                 )
                then ('ORACLE')
            --
               else 'NSF / ADJUSTMENTS'
            --
              end
            );
    exception
    when others then
     rollback to set_output_type;
     print_log('Issue in update of output_type, message ='||sqlerrm);
   end;
   -- update FIN_BRANCH field
   begin
    savepoint update_branch;
        merge into xxcus.xxhds_glsla_ar_drilldown t
        using (
                select org.organization_code branch_code
                      ,whse_gl_loc.entrp_loc      fru_loc
                from   org_organization_definitions   org
                      ,xxcus.xxcus_location_code_tbl  whse_gl_loc
                where 1 = 1
                  and org.operating_unit     =mo_global.get_current_org_id
                  and whse_gl_loc.lob_branch =org.organization_code
                  and whse_gl_loc.entrp_loc like 'BW%'
                  and whse_gl_loc.inactive ='N'
                  and org.disable_date is null
                  and org.organization_code not in ('WCC', 'MST')
               ) s
        on (t.gcc#50328#location = s.fru_loc)
        when matched then update set t.fin_branch =s.branch_code;
   exception
    when others then
     print_log('Issue in updating fin_branch [1] , message ='||sqlerrm);
     rollback to update_branch;
   end;
   -- update FIN_BRANCH field where we still have blank values for fin_branch
   begin
    savepoint update_branch;
        update xxcus.xxhds_glsla_ar_drilldown t
          set t.fin_branch =
               (
                select whse_gl_loc.lob_branch     branch_code
                from   xxcus.xxcus_location_code_tbl  whse_gl_loc
                where 1 = 1
                  and whse_gl_loc.entrp_loc like 'BW%'
                  and whse_gl_loc.inactive  ='N'
                  and whse_gl_loc.entrp_loc =t.gcc#50328#location
               )
        where 1 =1
          and t.fin_branch is null;
   exception
    when others then
     print_log('Issue in updating fin_branch [2], message ='||sqlerrm);
     rollback to update_branch;
   end;
   -- update PAYMENT_TYPE field for CASH order entry payments
   begin
    savepoint update_pmt_type;
        merge into xxcus.xxhds_glsla_ar_drilldown t
        using (
                select 'CASH' cS_pmt_type
                from   dual
               ) s
        on (t.payment_type_code = s.cS_pmt_type)
        when matched then update set t.payment_type ='CS';
   exception
    when others then
     print_log('Issue in updating CASH payment type code text, message ='||sqlerrm);
     rollback to update_pmt_type;
   end;
   -- update PAYMENT_TYPE field for CHECK order entry payments
   begin
    savepoint update_pmt_type;
        merge into xxcus.xxhds_glsla_ar_drilldown t
        using (
                select 'CHECK' cK_pmt_type
                from   dual
               ) s
        on (t.payment_type_code = s.cK_pmt_type)
        when matched then update set t.payment_type ='CK';
   exception
    when others then
     print_log('Issue in updating CHECK payment type code text, message ='||sqlerrm);
     rollback to update_pmt_type;
   end;
  -- update PAYMENT_TYPE field for AMEX CREDIT CARD order entry payments/returns
   begin
    savepoint update_pmt_type;
        merge into xxcus.xxhds_glsla_ar_drilldown t
        using (
                select 'CREDIT_CARD' cR_pmt_type
                from   dual
               ) s
        on (t.payment_type_code = s.cR_pmt_type AND card_brand ='AMEX')
        when matched then update set t.payment_type ='AMX';
   exception
    when others then
     print_log('Issue in updating AMEX credit card payment type code text, message ='||sqlerrm);
     rollback to update_pmt_type;
   end;
  -- update PAYMENT_TYPE field for NON AMEX CREDIT CARD order entry payments/returns
   begin
    savepoint update_pmt_type;
        merge into xxcus.xxhds_glsla_ar_drilldown t
        using (
                select 'CREDIT_CARD' cR_pmt_type
                from   dual
               ) s
        on (t.payment_type_code = s.cR_pmt_type AND card_brand <>'AMEX')
        when matched then update set t.payment_type ='VMD';
   exception
    when others then
     print_log('Issue in updating VISA/MASTER/DISCOVER credit card payment type code text, message ='||sqlerrm);
     rollback to update_pmt_type;
   end;
   --
exception
   when others then
    fnd_file.put_line(fnd_file.log, 'Error in caller XXHDS_GLSLA_AR_DRILLDOWN.main ='||sqlerrm);
    rollback;
end main;

end XXHDS_GLSLA_AR_DRILLDOWN;
/
