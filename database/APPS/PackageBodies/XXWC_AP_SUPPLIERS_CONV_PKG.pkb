CREATE OR REPLACE PACKAGE BODY APPS.XXWC_AP_SUPPLIERS_CONV_PKG
AS
   /*********************************************************************************************
*    script name: XXWC_AP_SUPPLIERS_CONV_PKG.pkb
*
*    interface / conversion name: Suppliers conversion.
*
*    functional purpose: convert suppliers/banks using Open interface
*                         and Bank API's
*
*    history:
*
*    version    date              author             description
*************************************************************************************************
*    1.0        04-sep-2011   T.Rajaiah     initial development.
*    1.1        15-Feb-2012   S Hariharan   PO site payment terms defaulted from Pay sites
*    1.2        22-May-2018   P.Vamshidhar  Validating Suppliers with Cross over list
*                                           TMS#20180703-00089 - AH Harries Suppliers Conversion
*************************************************************************************************/
   g_vendor_number   NUMBER;

   PROCEDURE xxwc_ap_supplier_valid (p_vendor_name    VARCHAR2,
                                     p_type_1099      VARCHAR2,
                                     p_vendor_type    VARCHAR2,
                                     p_terms_name     VARCHAR2,
                                     p_pay_group      VARCHAR2)
   AS
      l_income_tax_type   VARCHAR2 (15);
      l_vendor_type       VARCHAR2 (30);
      l_vendor_id         NUMBER;
      v_errorcode         NUMBER;
      v_errormessage      VARCHAR2 (240);
      l_pay_group         VARCHAR2 (30);
      v_sql               NUMBER;
   BEGIN
      g_vendor_type := NULL;
      g_federal_reportable_flag := NULL;
      g_term_id := NULL;
      g_type_1099 := NULL;
      g_vendor_id := NULL;
      g_existing := 'N';

      /*Modified by Thiru 9/jan/2012*/
      --- Start
      BEGIN
         IF g_vendor_number IS NOT NULL
         THEN
            BEGIN
               SELECT VENDOR_ID
                 INTO g_vendor_id
                 FROM APPS.AP_SUPPLIERS
                WHERE segment1 = g_vendor_number;

               fnd_file.put_line (fnd_file.LOG,
                                  'g_vendor_id:' || g_vendor_id);
            EXCEPTION
               WHEN OTHERS
               THEN
                  SELECT vendor_id
                    INTO g_vendor_id
                    FROM ap_suppliers
                   WHERE TRIM (UPPER (vendor_name)) =
                            TRIM (UPPER (p_vendor_name));

                  fnd_file.put_line (fnd_file.LOG,
                                     'g_vendor_id(Ex):' || g_vendor_id);
            END;
         ELSE
            SELECT vendor_id
              INTO g_vendor_id
              FROM ap_suppliers
             WHERE TRIM (UPPER (vendor_name)) = TRIM (UPPER (p_vendor_name));
         END IF;

         SELECT ap_suppliers_int_s.NEXTVAL INTO v_sql FROM DUAL;

         g_existing := 'Y';
      --l_verify_flag := 'N';
      --l_error_message := 'Vendor exist in Oracle';
      EXCEPTION
         WHEN OTHERS
         THEN
            g_vendor_id := NULL;
            g_existing := 'N';
      END;

      /*Modified by Thiru 9/jan/2012*/
      --- End

      BEGIN
         IF p_type_1099 IS NULL
         THEN
            g_federal_reportable_flag := 'N';
            g_type_1099 := NULL;
         ELSE
            g_type_1099 := 'MISC' || p_type_1099;
            g_federal_reportable_flag := 'Y';
         END IF;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_verify_flag := 'N';
            l_error_message := l_error_message || 'Income Tax Type is Invalid';
      END;

      /* Vendor Type Validations */
      BEGIN
         SELECT description
           INTO g_vendor_type
           FROM fnd_lookup_values
          WHERE     lookup_type = 'XXWC_PO_VENDOR_TYPE_MAP_CONV'
                AND TRIM (UPPER (lookup_code)) = TRIM (UPPER (p_vendor_type));
      EXCEPTION
         WHEN OTHERS
         THEN
            l_verify_flag := 'N';
            l_error_message :=
               l_error_message || 'Vendor Type,Mapping From Legazy fails';
      END;

      BEGIN
         SELECT lookup_code
           INTO l_vendor_type
           FROM po_lookup_codes
          WHERE     lookup_type(+) = 'VENDOR TYPE'
                AND TRIM (UPPER (lookup_code)) = TRIM (UPPER (g_vendor_type));
      EXCEPTION
         WHEN OTHERS
         THEN
            l_verify_flag := 'N';
            l_error_message := l_error_message || 'Invalid Vendor Type';
      END;


      -- Deriving term id
      BEGIN
         SELECT a.oracle_terms_id
           INTO g_term_id
           FROM XXWC.XXWC_AP_PMT_TERMS_STG_T a
          WHERE TRIM (UPPER (a.ahh_terms_id)) = TRIM (UPPER (p_terms_name));
      EXCEPTION
         WHEN OTHERS
         THEN
            g_term_id := 10154;
      END;

      BEGIN
         SELECT name
           INTO g_terms_name
           FROM AP_TERMS
          WHERE term_id = g_term_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            g_terms_name := NULL;
      END;
   /*
    BEGIN
       SELECT description
         INTO g_terms_name
         FROM fnd_lookup_values
        WHERE     lookup_type = 'XXWC_AP_TERMS_MAPPING_CONV'
              AND TRIM (UPPER (lookup_code)) = TRIM (UPPER (p_terms_name));

       SELECT term_id
         INTO g_term_id
         FROM ap_terms
        WHERE TRIM (UPPER (NAME)) = TRIM (UPPER (g_terms_name));
    EXCEPTION
       WHEN OTHERS
       THEN
          g_term_id := 10017;
    --l_verify_flag := 'N';
    --l_error_message :=l_error_message || '-Payment Terms NA-';
    END;

    */
   EXCEPTION
      WHEN OTHERS
      THEN
         v_errorcode := SQLCODE;
         v_errormessage := SUBSTR (SQLERRM, 1, 200);
         fnd_file.put_line (
            fnd_file.LOG,
            'Exception Block of Supplier Header Validation Procedure');
         fnd_file.put_line (fnd_file.LOG,
                            'The Error Code Traced is : ' || v_errorcode);
         fnd_file.put_line (
            fnd_file.LOG,
            'The Error Message Traced is : ' || v_errormessage);
   END xxwc_ap_supplier_valid;

   PROCEDURE xxwc_ap_supp_sites_valid (p_site_code     VARCHAR2,
                                       p_country       VARCHAR2,
                                       p_state         VARCHAR2,
                                       p_terms_name    VARCHAR2)
   AS
      l_term_id        NUMBER;
      v_errorcode      NUMBER;
      v_errormessage   VARCHAR2 (240);
   BEGIN
      g_vendor_site_code := NULL;
      g_term_id := NULL;


      BEGIN
         SELECT a.oracle_terms_id
           INTO g_term_id
           FROM XXWC.XXWC_AP_PMT_TERMS_STG_T a
          WHERE TRIM (UPPER (a.ahh_terms_id)) = TRIM (UPPER (p_terms_name));
      EXCEPTION
         WHEN OTHERS
         THEN
            g_term_id := 10154;
      END;

      BEGIN
         SELECT name
           INTO g_terms_name
           FROM AP_TERMS
          WHERE term_id = g_term_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            g_terms_name := NULL;
      END;

      --Code Added on 23-OCT-2011
      /*

       BEGIN
          SELECT description
            INTO g_terms_name
            FROM fnd_lookup_values
           WHERE     lookup_type = 'XXWC_AP_TERMS_MAPPING_CONV'
                 AND TRIM (UPPER (lookup_code)) = TRIM (UPPER (p_terms_name));

          SELECT term_id
            INTO g_term_id
            FROM ap_terms
           WHERE TRIM (UPPER (NAME)) = TRIM (UPPER (g_terms_name));
       EXCEPTION
          WHEN OTHERS
          THEN
             g_term_id := 10154;
       --l_verify_flag := 'N';
       --l_error_message := l_error_message || '-Payment Term NA-';
       END;
       */

      IF p_site_code IS NULL
      THEN
         g_vendor_site_code :=
            p_country || '-' || p_state || '-' || ap_suppliers_int_s.CURRVAL;
         g_vendor_site_code := SUBSTR (g_vendor_site_code, 1, 15);
      ELSE
         g_vendor_site_code := SUBSTR (p_site_code, 1, 15);     --p_site_code;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         v_errorcode := SQLCODE;
         v_errormessage := SUBSTR (SQLERRM, 1, 200);
         fnd_file.put_line (
            fnd_file.LOG,
            'Exception Block of Supplier Sites Validation Procedure');
         fnd_file.put_line (fnd_file.LOG,
                            'The Error Code Traced is : ' || v_errorcode);
         fnd_file.put_line (
            fnd_file.LOG,
            'The Error Message Traced is : ' || v_errormessage);
   END xxwc_ap_supp_sites_valid;

   PROCEDURE main (errbuf               OUT VARCHAR2,
                   retcode              OUT NUMBER,
                   p_validate_only   IN     VARCHAR2,
                   p_submit          IN     VARCHAR2)
   AS
      l_flag           VARCHAR2 (10) DEFAULT 'Y';
      v_errorcode      NUMBER;
      v_errormessage   VARCHAR2 (240);
      l_vendor_name    VARCHAR2 (240);

      CURSOR cur_hdr2
      IS
         SELECT c.ROWID r_id, c.*
           FROM XXWC_AP_SUPPLIER_INT c
          WHERE NVL (status, 'XXXXX') <> 'PROCESSED';

      CURSOR cur_site2
      IS
         SELECT d.ROWID r_id, d.*
           FROM XXWC_AP_SUPPLIER_SITES_INT d
          WHERE NVL (status, 'XXXXX') <> 'PROCESSED';
   BEGIN
      /*Initializing Global Variables as per Parameters selected*/

      -- Update payment method to match data in Oracle
      UPDATE XXWC_AP_SUPPLIER_SITES_INT
         SET payment_method_lookup_code =
                DECODE (SUBSTR (payment_method_lookup_code, 1, 4),
                        'ELEC', 'ELECTRONIC',
                        'CHEC', 'CHECK');

      -- Update Payment terms to what is available in pay site if the terms are null
      UPDATE XXWC_AP_SUPPLIER_SITES_INT a
         SET a.TERMS_ID =
                NVL (
                   (SELECT terms_id
                      FROM XXWC.XXWC_AP_SUPPLIER_SITES_INT b
                     WHERE     a.vendor_number = b.vendor_number
                           AND b.pay_site_flag = 1
                           AND b.terms_id IS NOT NULL
                           AND ROWNUM = 1),
                   'N30 (G,-)')
       WHERE a.TERMS_ID IS NULL;

      UPDATE XXWC_AP_SUPPLIER_SITES_INT
         SET pay_group_lookup_code = 'DISC'
       WHERE pay_group_lookup_code LIKE 'DISC%';

      COMMIT;

      IF p_validate_only = 'Y'
      THEN
         UPDATE xxwc.XXWC_AP_SUPPLIER_INT
            SET vendor_name =
                   REPLACE (
                      REPLACE (
                         REPLACE (
                            REPLACE (REPLACE (vendor_name, '&', ' and '),
                                     '-',
                                     ' '),
                            ',',
                            ''),
                         '''',
                         ''),
                      '"',
                      '');

         COMMIT;

         xxwc_ap_suppliers_conv_pkg.g_validate_only := 'Y';
      ELSE
         xxwc_ap_suppliers_conv_pkg.g_validate_only := 'N';
      END IF;

      IF p_submit = 'Yes'
      THEN
         xxwc_ap_suppliers_conv_pkg.g_submit := 'Y';
      ELSE
         xxwc_ap_suppliers_conv_pkg.g_submit := 'N';
      END IF;

      BEGIN
         xxwc_ap_suppliers_conv_pkg.xxwc_stage_to_interface;
      EXCEPTION
         WHEN OTHERS
         THEN
            v_errorcode := SQLCODE;
            v_errormessage := SUBSTR (SQLERRM, 1, 200);
            fnd_file.put_line (
               fnd_file.LOG,
               'Exception Block of Call to Stage to Interface in Main');
            fnd_file.put_line (fnd_file.LOG,
                               'The Error Code Traced is : ' || v_errorcode);
            fnd_file.put_line (
               fnd_file.LOG,
               'The Error Message Traced is : ' || v_errormessage);
      END;

      BEGIN
         fnd_file.put_line (fnd_file.output, '<HTML><BODY>');
         fnd_file.put_line (fnd_file.output, '<PRE>');
         fnd_file.new_line (fnd_file.output, 1);
         fnd_file.put_line (
            fnd_file.output,
            '********************************************************************************************<BR>');
         fnd_file.put_line (
            fnd_file.output,
            '<H3>                Custom Supplier Open Interface Import ERROR Report                      </H3>');
         fnd_file.put_line (
            fnd_file.output,
            '********************************************************************************************<BR>');
         fnd_file.put_line (
            fnd_file.output,
               '<H4> 1. Program Name           : '
            || 'XXWC_AP_CUSTOM_SUPPLIER_IMPORT_PROGRAM');
         fnd_file.put_line (
            fnd_file.output,
               ' 2. Start Date             :  '
            || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));
         fnd_file.put_line (fnd_file.output, '</H4>');
         fnd_file.put_line (
            fnd_file.output,
            '********************************************************************************************<BR>');
         fnd_file.put_line (fnd_file.output, '<BR>');
         fnd_file.put_line (
            fnd_file.output,
            '<b>The following records may causing this failure</b> ');
         fnd_file.put_line (fnd_file.output, '<TABLE BORDER=1>');
         fnd_file.put_line (
            fnd_file.output,
            '<TR><TH>SupplierNumber # </TH><TH>Supplier Name # </TH><TH>Rejection Reason </TH></TR>');

         FOR rec_hdr2 IN cur_hdr2
         LOOP
            fnd_file.put_line (
               fnd_file.output,
                  '<TR><TD>'
               || rec_hdr2.vendor_name
               || '</TD><TD>'
               || rec_hdr2.attribute1
               || '</TD><TD>'
               || rec_hdr2.reject_code
               || '</TD></TR>');
         END LOOP;

         fnd_file.put_line (fnd_file.output, '');
         fnd_file.put_line (fnd_file.output, '');
         fnd_file.put_line (fnd_file.output, '');
         fnd_file.put_line (fnd_file.output, '');
         fnd_file.put_line (
            fnd_file.output,
            '********************************************************************************************<BR>');
         fnd_file.put_line (
            fnd_file.output,
            '<H3>                Custom Supplier Sites Open  Interface Import ERROR Report                      </H3>');
         fnd_file.put_line (
            fnd_file.output,
            '********************************************************************************************<BR>');
         fnd_file.put_line (
            fnd_file.output,
               '<H4> 1. Program Name           : '
            || 'XXWC_AP_CUSTOM_SUPPLIER_IMPORT_PROGRAM');
         fnd_file.put_line (
            fnd_file.output,
               ' 2. Start Date             :  '
            || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));
         fnd_file.put_line (fnd_file.output, '</H4>');
         fnd_file.put_line (
            fnd_file.output,
            '********************************************************************************************<BR>');
         fnd_file.put_line (fnd_file.output, '<BR>');
         fnd_file.put_line (
            fnd_file.output,
            '<b>The following records may causing this failure</b> ');
         fnd_file.put_line (fnd_file.output, '<TABLE BORDER=1>');
         fnd_file.put_line (
            fnd_file.output,
            '<TR><TH>SupplierNumber # </TH><TH>Supplier Name # </TH><TH>Supplier Site </TH><TH>Rejection Reason </TH></TR>');

         FOR rec_site2 IN cur_site2
         LOOP
            BEGIN
               SELECT vendor_name
                 INTO l_vendor_name
                 FROM XXWC_AP_SUPPLIER_INT
                WHERE attribute1 = rec_site2.vendor_number;

               fnd_file.put_line (
                  fnd_file.output,
                     '<TR><TD>'
                  || rec_site2.vendor_number
                  || '</TD><TD>'
                  || l_vendor_name
                  || '</TD><TD>'
                  || rec_site2.vendor_site_code
                  || '</TD><TD>'
                  || rec_site2.reject_code
                  || '</TD></TR>');
            EXCEPTION
               WHEN OTHERS
               THEN
                  v_errorcode := SQLCODE;
                  v_errormessage := SUBSTR (SQLERRM, 1, 200);
                  fnd_file.put_line (
                     fnd_file.LOG,
                        'Exception Block Of Supplier Site Error Report '
                     || rec_site2.vendor_number);
                  fnd_file.put_line (
                     fnd_file.LOG,
                     'The Error Code Traced is : ' || v_errorcode);
                  fnd_file.put_line (
                     fnd_file.LOG,
                     'The Error Message Traced is : ' || v_errormessage);
            END;
         END LOOP;

         fnd_file.put_line (fnd_file.output, '</TABLE>');
         fnd_file.put_line (fnd_file.output, '</BODY></HTML>');

         --
         UPDATE APPS.AP_SUPPLIERS a
            SET a.attribute1 =
                   (SELECT b.AHH_SUPPLIER_NUMBER
                      FROM XXWC.XXWC_AP_SUPPLIER_REF_TBL b
                     WHERE b.oracle_vendor_id = a.segment1 AND ROWNUM = 1)
          WHERE     a.attribute1 IS NULL
                AND a.SEGMENT1 IN (SELECT ORACLE_VENDOR_ID
                                   FROM XXWC.XXWC_AP_SUPPLIER_REF_TBL);
      EXCEPTION
         WHEN OTHERS
         THEN
            v_errorcode := SQLCODE;
            v_errormessage := SUBSTR (SQLERRM, 1, 200);
            fnd_file.put_line (fnd_file.LOG,
                               'Exception Block Error Report Code');
            fnd_file.put_line (fnd_file.LOG,
                               'The Error Code Traced is : ' || v_errorcode);
            fnd_file.put_line (
               fnd_file.LOG,
               'The Error Message Traced is : ' || v_errormessage);
      END;
   EXCEPTION
      WHEN OTHERS
      THEN
         v_errorcode := SQLCODE;
         v_errormessage := SUBSTR (SQLERRM, 1, 200);
         fnd_file.put_line (fnd_file.LOG,
                            'Exception Block of Main Procedure');
         fnd_file.put_line (fnd_file.LOG,
                            'The Error Code Traced is : ' || v_errorcode);
         fnd_file.put_line (
            fnd_file.LOG,
            'The Error Message Traced is : ' || v_errormessage);
   END main;

   PROCEDURE xxwc_stage_to_interface
   AS
      v_vendor_name           VARCHAR2 (60);
      v_count                 NUMBER (15) := 0;
      v_errorcode             NUMBER;
      v_errormessage          VARCHAR2 (240);
      l_bnkacct               VARCHAR2 (20);
      l_bnum                  VARCHAR2 (30);
      l_site_code             VARCHAR2 (15);
      l_created_by            NUMBER := fnd_global.user_id;
      l_creation_date         DATE := SYSDATE;
      lvc_cover_flag          VARCHAR2 (1);

      CURSOR cur_hdr
      IS
             SELECT c.ROWID r_id, c.*
               FROM XXWC_AP_SUPPLIER_INT c
              WHERE NVL (status, 'XXXXX') <> 'PROCESSED'
         --                     AND ATTRIBUTE1 IN ('2160','21600','33081','33092')
         ----                    AND C.ATTRIBUTE1 NOT IN ('22070')
         ----                    AND ATTRIBUTE1 IN (SELECT AHH_SUPPLIER_NUMBER FROM XXWC.XXWC_AP_SUPPLIER_REF_TBL)
         --         --                    AND ATTRIBUTE1 NOT IN (SELECT AHH_SUPPLIER_NUMBER
         --         --                                             FROM XXWC.XXWC_AP_SUPPLIER_REF_TBL)
         --         -- AND attribute1 IN ('21730','21770') --('2160','21600') --('33081','33092') --('33078', '430') -- ?????
         FOR UPDATE OF status, reject_code;


      CURSOR cur_site (
         p_vendor_number    VARCHAR2)
      IS
             SELECT d.ROWID r_id, d.*
               FROM XXWC_AP_SUPPLIER_SITES_INT d
              WHERE     vendor_number = p_vendor_number
                    AND NVL (status, 'XXXXX') <> 'PROCESSED'
         --AND vendor_number IN ('33078', '430') -- ?????
         FOR UPDATE OF status, reject_code;

      CURSOR cur_cont (
         p_vendor_number    VARCHAR2,
         p_site_code        VARCHAR2)
      IS
             SELECT b.ROWID r_id, b.*
               FROM XXWC.XXWC_AP_SUP_SITE_CONTACT_INT b
              WHERE     vendor_number = p_vendor_number
                    AND vendor_site_code = p_site_code
                    AND NVL (status, 'XXXXX') <> 'PROCESSED'
         -- AND vendor_number IN ('33078', '430') -- ?????
         FOR UPDATE OF status, reject_code;

      CURSOR cur_bank (p_vendor_number VARCHAR2)
      IS
         SELECT b.ROWID r_id, b.*
           FROM xxwc_iby_bank_accounts b
          WHERE vendor_number = p_vendor_number;

      lvc_pay_conc_segs       GL_CODE_COMBINATIONS_KFV.CONCATENATED_SEGMENTS%TYPE
                                 := '0W.BW080.0000.211100.00000.00000.00000';
      lvc_pre_pay_conc_segs   GL_CODE_COMBINATIONS_KFV.CONCATENATED_SEGMENTS%TYPE
         := '0W.BW080.0000.135200.00000.00000.00000';
      ln_pay_ccid             GL_CODE_COMBINATIONS.CODE_COMBINATION_ID%TYPE;
      ln_pre_pay_ccid         GL_CODE_COMBINATIONS.CODE_COMBINATION_ID%TYPE;
      lvc_pay_err_msg         VARCHAR2 (240);
      lvc_pre_pay_err_msg     VARCHAR2 (240);
   BEGIN
      COMMIT;

      FND_FILE.PUT_LINE (
         FND_FILE.LOG,
            'HDR level l_verify_flag:'
         || l_verify_flag
         || ' g_validate_only:'
         || g_validate_only);

      FOR rec_hdr IN cur_hdr
      LOOP
         l_verify_flag := 'Y';
         l_error_message := NULL;
         g_vendor_number := NULL;

         IF g_validate_only = 'Y'
         THEN
            FND_FILE.PUT_LINE (FND_FILE.LOG,
                               'rec_hdr.vendor_name:' || rec_hdr.vendor_name);

            BEGIN
               lvc_cover_flag := NULL;

               SELECT 'Y'
                 INTO lvc_cover_flag
                 FROM XXWC.XXWC_AP_SUPPLIER_REF_TBL
                WHERE     AHH_SUPPLIER_NUMBER = rec_hdr.ATTRIBUTE1
                      AND ORACLE_VENDOR_ID IS NOT NULL;

               IF lvc_cover_flag = 'Y'
               THEN
                  GOTO LAST_STEP;
               END IF;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  lvc_cover_flag := 'N';
            END;

            xxwc_ap_supplier_valid (
               rec_hdr.vendor_name,
               RTRIM (
                  REPLACE (REPLACE (rec_hdr.tax_type, CHR (13), ' '),
                           CHR (10),
                           ' ')),
               rec_hdr.vendor_type_lookup_code,
               RTRIM (
                  REPLACE (REPLACE (rec_hdr.terms_name, CHR (13), ' '),
                           CHR (10),
                           ' ')),
               rec_hdr.pay_group_lookup_code);

            IF l_verify_flag != 'N'
            THEN
               UPDATE XXWC_AP_SUPPLIER_INT
                  SET status = 'VALIDATED', reject_code = l_error_message
                WHERE ROWID = rec_hdr.r_id;
            ELSE
               UPDATE XXWC_AP_SUPPLIER_INT
                  SET status = 'REJECTED', reject_code = l_error_message
                WHERE ROWID = rec_hdr.r_id;
            END IF;

           <<LAST_STEP>>
            IF lvc_cover_flag = 'Y'
            THEN
               UPDATE XXWC_AP_SUPPLIER_INT
                  SET status = 'EXIST',
                      reject_code = 'Supplier exists in Cross Over'
                WHERE ROWID = rec_hdr.r_id;
            END IF;


            FND_FILE.PUT_LINE (
               FND_FILE.LOG,
               'rec_hdr.attribute1 (site before):' || rec_hdr.attribute1);

            FOR rec_site IN cur_site (rec_hdr.attribute1)
            LOOP
               FND_FILE.PUT_LINE (
                  FND_FILE.LOG,
                  'rec_hdr.attribute1 (site after):' || rec_hdr.attribute1);

               BEGIN
                  lvc_cover_flag := NULL;

                  SELECT 'Y'
                    INTO lvc_cover_flag
                    FROM XXWC.XXWC_AP_SUPPLIER_SITES_REF_TBL
                   WHERE     AHH_SUPPLIER_NUMBER = rec_hdr.ATTRIBUTE1
                         AND ORACLE_VENDOR_SITE_ID IS NOT NULL
                         AND ROWNUM=1;  --?

                  IF lvc_cover_flag = 'Y'
                  THEN
                     GOTO LAST_STEP1;
                  END IF;
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     lvc_cover_flag := 'N';
               END;


               l_verify_flag := 'Y';
               l_error_message := NULL;
               xxwc_ap_supp_sites_valid (
                  rec_site.vendor_site_code,
                  rec_site.country,
                  rec_site.state,
                  RTRIM (
                     REPLACE (REPLACE (rec_site.terms_id, CHR (13), ' '),
                              CHR (10),
                              ' ')));

               IF l_verify_flag != 'N'
               THEN
                  UPDATE XXWC_AP_SUPPLIER_SITES_INT
                     SET status = 'VALIDATED', reject_code = l_error_message
                   WHERE ROWID = rec_site.r_id;
               ELSE
                  UPDATE XXWC_AP_SUPPLIER_SITES_INT
                     SET status = 'REJECTED', reject_code = l_error_message
                   WHERE ROWID = rec_site.r_id;
               END IF;

              <<LAST_STEP1>>
               IF lvc_cover_flag = 'Y'
               THEN
                  UPDATE XXWC_AP_SUPPLIER_SITES_INT
                     SET status = 'EXIST',
                         reject_code = 'Supplier Site exists in Cross Over'
                   WHERE ROWID = rec_site.r_id;
               END IF;
            END LOOP;
         END IF;

         FND_FILE.PUT_LINE (
            FND_FILE.LOG,
            'Before insert g_validate_only:' || g_validate_only);

         IF g_validate_only != 'Y'
         THEN
            BEGIN
               g_vendor_number := NULL;

               SELECT ORACLE_VENDOR_ID
                 INTO g_vendor_number
                 FROM XXWC.XXWC_AP_SUPPLIER_REF_TBL
                WHERE     AHH_SUPPLIER_NUMBER = rec_hdr.ATTRIBUTE1
                      AND ORACLE_VENDOR_ID IS NOT NULL;

               fnd_file.put_line (fnd_file.LOG,
                                  'g_vendor_number:' || g_vendor_number);
            EXCEPTION
               WHEN OTHERS
               THEN
                  g_vendor_number := NULL;
            END;

            xxwc_ap_supplier_valid (
               rec_hdr.vendor_name,
               RTRIM (
                  REPLACE (REPLACE (rec_hdr.tax_type, CHR (13), ' '),
                           CHR (10),
                           ' ')),
               rec_hdr.vendor_type_lookup_code,
               RTRIM (
                  REPLACE (REPLACE (rec_hdr.terms_name, CHR (13), ' '),
                           CHR (10),
                           ' ')),
               rec_hdr.pay_group_lookup_code);
            fnd_file.put_line (
               fnd_file.LOG,
                  'L verify flag : '
               || l_verify_flag
               || ' g_existing:'
               || g_existing);



            IF l_verify_flag != 'N'
            THEN
               IF g_existing = 'N'
               THEN
                  fnd_file.put_line (fnd_file.LOG, 'supplier insert : ');

                  INSERT
                    INTO ap_suppliers_int (vendor_interface_id,
                                           vendor_name,
                                           attribute1,
                                           vendor_name_alt,
                                           num_1099,
                                           type_1099,
                                           federal_reportable_flag,
                                           payment_priority,
                                           vendor_type_lookup_code,
                                           days_early_receipt_allowed,
                                           days_late_receipt_allowed,
                                           receipt_days_exception_code,
                                           -- segment1,
                                           invoice_currency_code,
                                           payment_currency_code,
                                           ship_to_location_code,
                                           bill_to_location_code,
                                           enabled_flag,
                                           employee_id,
                                           terms_name,
                                           pay_group_lookup_code,
                                           attribute2,
                                           attribute3,
                                           attribute4,
                                           attribute5,
                                           attribute6,
                                           attribute7,
                                           attribute8,
                                           attribute9,
                                           attribute10,
                                           attribute11,
                                           attribute12,
                                           attribute13,
                                           attribute14,
                                           attribute15,
                                           start_date_active,
                                           end_date_active,
                                           set_of_books_id,
                                           status,
                                           created_by,
                                           creation_date,
                                           last_updated_by,
                                           last_update_date,
                                           organization_type_lookup_code)
                  VALUES (ap_suppliers_int_s.NEXTVAL,
                          rec_hdr.vendor_name,          --rec_hdr.vendor_name,
                          rec_hdr.attribute1,
                          rec_hdr.vendor_name_alt,
                          rec_hdr.num_1099,
                          g_type_1099,                   --rec_hdr.TYPE_1099 ,
                          g_federal_reportable_flag, --rec_hdr.FEDERAL_REPORTABLE_FLAG,
                          rec_hdr.payment_priority,
                          g_vendor_type,    --rec_hdr.vendor_type_lookup_code,
                          rec_hdr.days_early_receipt_allowed,
                          rec_hdr.days_late_receipt_allowed,
                          rec_hdr.receipt_days_exception_code,
                          --rec_hdr.attribute1,                      --segment1,
                          rec_hdr.invoice_currency_code,
                          rec_hdr.payment_currency_code,
                          rec_hdr.ship_to_location_code,
                          rec_hdr.bill_to_location_code,
                          SUBSTR (rec_hdr.enabled_flag, 1, 1), --rec_hdr.enabled_flag,
                          rec_hdr.employee_id,
                          g_terms_name,
                          NVL (rec_hdr.pay_group_lookup_code, 'NET'),
                          rec_hdr.attribute2,
                          rec_hdr.attribute3,
                          rec_hdr.attribute4,
                          rec_hdr.attribute5,
                          rec_hdr.attribute6,
                          rec_hdr.attribute7,
                          rec_hdr.attribute8,
                          rec_hdr.attribute9,
                          rec_hdr.attribute10,
                          rec_hdr.attribute11,
                          rec_hdr.attribute12,
                          rec_hdr.attribute13,
                          rec_hdr.attribute14,
                          rec_hdr.attribute15,
                          rec_hdr.start_date_active,
                          rec_hdr.end_date_active,
                          rec_hdr.set_of_books_id,
                          'NEW',
                          l_created_by,
                          l_creation_date,
                          l_created_by,
                          l_creation_date             --,rec_hdr.attribute15);
                                         ,
                          'CORPORATION');                -- Changed by Vamshi.

                  UPDATE XXWC_AP_SUPPLIER_INT
                     SET status = 'PROCESSED', reject_code = l_error_message
                   WHERE ROWID = rec_hdr.r_id;
               END IF;

               /*Modified by Thiru 9/Jan/2012*/
               -- Start

               IF g_existing = 'Y'
               THEN
                  UPDATE ap_suppliers
                     SET attribute1 = rec_hdr.attribute1
                   WHERE TRIM (UPPER (vendor_name)) =
                            TRIM (UPPER (rec_hdr.vendor_name));
               END IF;

               /* Modified by Thiru 9/Jan/2012 */
               --- End
               ----Supplier Sites------------

               ln_pay_ccid := derive_ccid (lvc_pay_conc_segs, lvc_pay_err_msg);
               ln_pre_pay_ccid :=
                  derive_ccid (lvc_pre_pay_conc_segs, lvc_pre_pay_err_msg);


               FOR rec_site IN cur_site (rec_hdr.attribute1)
               LOOP
                  l_verify_flag := 'Y';
                  l_error_message := NULL;
                  fnd_file.put_line (fnd_file.LOG, 'supplier site first  : ');
                  fnd_file.put_line (fnd_file.LOG,
                                     'vendor number:' || rec_hdr.attribute1);

                  IF g_validate_only != 'Y'
                  THEN
                     xxwc_ap_supp_sites_valid (
                        rec_site.vendor_site_code,
                        rec_site.country,
                        rec_site.state,
                        RTRIM (
                           REPLACE (
                              REPLACE (rec_site.terms_id, CHR (13), ' '),
                              CHR (10),
                              ' ')));

                     fnd_file.put_line (
                        fnd_file.LOG,
                           'vendor number:'
                        || rec_hdr.attribute1
                        || 'Site Validated');

                     IF l_verify_flag != 'N'
                     THEN
                        --l_site_code :=
                        --    SUBSTR (rec_site.vendor_site_code, 1, 15);
                        fnd_file.put_line (
                           fnd_file.LOG,
                              'g_term_id:'
                           || g_term_id
                           || ' g_vendor_id:'
                           || g_vendor_id);

                        INSERT
                          INTO ap_supplier_sites_int (
                                  vendor_interface_id,
                                  vendor_id,     --- Added by Thiru 9/Jan/2012
                                  org_id,
                                  purchasing_site_flag,
                                  pay_site_flag,
                                  primary_pay_site_flag,
                                  vendor_site_code,
                                  address_line1,
                                  address_line2,
                                  address_line3,
                                  city,
                                  state,
                                  zip,
                                  county,
                                  country,
                                  phone,
                                  inactive_date,
                                  fax,
                                  hold_unmatched_invoices_flag,
                                  accts_pay_code_combination_id,
                                  prepay_code_combination_id,
                                  payment_method_lookup_code,
                                  exclusive_payment_flag,
                                  terms_id,
                                  pay_group_lookup_code,
                                  match_option,
                                  vendor_site_interface_id,
                                  distribution_set_name,
                                  status,
                                  tax_reporting_site_flag,
                                  created_by,
                                  creation_date,
                                  last_updated_by,
                                  last_update_date)
                           VALUES (
                                     ap_suppliers_int_s.CURRVAL,
                                     g_vendor_id, ---- Added by Thiru 9/Jan/2012
                                     xxwc_ascp_scwb_pkg.get_wc_org_id, --fnd_global.org_id,
                                     --TO_NUMBER (rec_site.org_id),
                                     DECODE (rec_site.purchasing_site_flag,
                                             1, 'Y',
                                             0, 'N',
                                             'NULL'), --rec_site.purchasing_site_flag,
                                     DECODE (rec_site.pay_site_flag,
                                             1, 'Y',
                                             0, 'N',
                                             'NULL'), --rec_site.pay_site_flag,
                                     DECODE (rec_site.pay_site_flag,
                                             1, 'Y',
                                             0, 'N',
                                             'NULL'),
                                     g_vendor_site_code, --rec_site.vendor_site_code,
                                     rec_site.address_line1,
                                     rec_site.address_line2,
                                     rec_site.address_line3,
                                     rec_site.city,
                                     rec_site.state,
                                     rec_site.zip,
                                     rec_site.county,
                                     NVL (
                                        RTRIM (
                                           REPLACE (
                                              REPLACE (rec_site.country,
                                                       CHR (13),
                                                       ' '),
                                              CHR (10),
                                              ' ')),
                                        'US'),             --rec_site.country,
                                     rec_site.phone,
                                     DECODE (
                                        SUBSTR (g_vendor_site_code, 1, 4),
                                        '0PUR', NULL,
                                        'PAY-', NULL,
                                        SYSDATE),
                                     SUBSTR (rec_site.fax, 1, 15),
                                     rec_site.hold_unmatched_invoices_flag,
                                     ln_pay_ccid,
                                     --TO_NUMBER (rec_site.accts_pay_code_combination_id),
                                     ln_pre_pay_ccid,
                                     --TO_NUMBER (rec_site.prepay_code_combination_id),
                                     DECODE (
                                        UPPER (
                                           rec_site.payment_method_lookup_code),
                                        'ELECTRONIC', 'EFT',
                                        UPPER (
                                           rec_site.payment_method_lookup_code)),
                                     'N',                           --DECODE (
                                     --  rec_site.exclusive_payment_flag,
                                     --0,
                                     -- 'Y'), --rec_site.exclusive_payment_flag,
                                     g_term_id,      --NVL (g_term_id, 10017),
                                     --TO_NUMBER (rec_site.terms_id),
                                     NVL (rec_site.pay_group_lookup_code,
                                          'NET'),
                                     rec_site.match_option,
                                     ap_supplier_sites_int_s.NEXTVAL,
                                     rec_site.distribution_set_name,
                                     'NEW',
                                     DECODE (
                                        rec_site.tax_reporting_site_flag,
                                        '1', 'Y',
                                        'N'),
                                     l_created_by,
                                     l_creation_date,
                                     l_created_by,
                                     l_creation_date);

                        fnd_file.put_line (
                           fnd_file.LOG,
                              'g_term_id:'
                           || g_term_id
                           || ' inserted '
                           || SQL%ROWCOUNT);


                        UPDATE XXWC_AP_SUPPLIER_SITES_INT
                           SET status = 'PROCESSED',
                               reject_code = l_error_message
                         WHERE ROWID = rec_site.r_id;


                        fnd_file.put_line (fnd_file.LOG,
                                           'supplier site after insert : ');
                     ELSE
                        UPDATE XXWC_AP_SUPPLIER_SITES_INT
                           SET status = 'REJECTED',
                               reject_code = l_error_message
                         WHERE ROWID = rec_site.r_id;
                     END IF;
                  END IF;

                  FOR rec_cont
                     IN cur_cont (rec_hdr.attribute1,
                                  rec_site.vendor_site_code)
                  LOOP
                     FND_FILE.PUT_LINE (
                        FND_FILE.LOG,
                           'rec_hdr.attribute1:'
                        || rec_hdr.attribute1
                        || ' rec_site.vendor_site_code:'
                        || rec_site.vendor_site_code);

                     INSERT
                       INTO ap_sup_site_contact_int (
                               vendor_interface_id,
                               vendor_contact_interface_id,
                               vendor_id,
                               vendor_site_code,
                               first_name,
                               last_name,
                               area_code,
                               org_id,
                               phone,
                               fax,
                               email_address,
                               status,
                               created_by,
                               creation_date,
                               last_updated_by,
                               last_update_date)
                        VALUES (
                                  ap_suppliers_int_s.CURRVAL,
                                  ap_sup_site_contact_int_s.NEXTVAL,
                                  g_vendor_id, -- added by Shankar 09-Jan-2012
                                  SUBSTR (rec_cont.vendor_site_code, 1, 15), --rec_cont.vendor_site_code,
                                  SUBSTR (rec_cont.first_name, 1, 15),
                                  NVL (rec_cont.last_name, '.'),
                                  rec_cont.area_code,
                                  xxwc_ascp_scwb_pkg.get_wc_org_id, --fnd_global.org_id,
                                  rec_cont.phone,
                                  SUBSTR (rec_cont.fax, 1, 15),
                                  RTRIM (
                                     REPLACE (
                                        REPLACE (rec_cont.email_address,
                                                 CHR (13),
                                                 ' '),
                                        CHR (10),
                                        ' ')),       --rec_cont.email_address,
                                  'NEW',
                                  l_created_by,
                                  l_creation_date,
                                  l_created_by,
                                  l_creation_date);

                     UPDATE XXWC_AP_SUP_SITE_CONTACT_INT
                        SET status = 'PROCESSED'
                      WHERE ROWID = rec_cont.r_id;

                     fnd_file.put_line (fnd_file.LOG,
                                        'supplier contact insert : ');
                  END LOOP;
               END LOOP;
            -------Supplier Sites End---------
            ELSE
               UPDATE XXWC_AP_SUPPLIER_INT
                  SET status = 'REJECTED', reject_code = l_error_message
                WHERE ROWID = rec_hdr.r_id;
            END IF;
         END IF;

         v_count := v_count + 1;
      END LOOP;

      COMMIT;

      IF g_submit = 'Y'
      THEN
         xxwc_supp_import_stand;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         v_errorcode := SQLCODE;
         v_errormessage := SUBSTR (SQLERRM, 1, 200);
         fnd_file.put_line (
            fnd_file.LOG,
            'Exception Block of Stage To Interface Procedure');
         fnd_file.put_line (fnd_file.LOG,
                            'The Error Code Traced is : ' || v_errorcode);
         fnd_file.put_line (
            fnd_file.LOG,
            'The Error Message Traced is : ' || v_errormessage);
   END xxwc_stage_to_interface;

   PROCEDURE xxwc_supp_import_stand
   AS
      v_conc_request_id   NUMBER;
      l_request_id1       NUMBER;
      v_phase             VARCHAR2 (100);
      v_status            VARCHAR2 (100);
      v_dev_phase         VARCHAR2 (100);
      v_dev_status        VARCHAR2 (100);
      v_message           VARCHAR2 (2000);
      v_wait_outcome      BOOLEAN;
      v_errorcode         NUMBER;
      v_errormessage      VARCHAR2 (240);
   BEGIN
      -- fnd_file.put_line (fnd_file.LOG, 'Inside Standard Submit');
      -- Supplier Open Interface Import
      l_request_id1 :=
         fnd_request.submit_request (application   => 'SQLAP',
                                     program       => 'APXSUIMP',
                                     description   => NULL,
                                     start_time    => SYSDATE,
                                     sub_request   => FALSE,
                                     argument1     => 'NEW',
                                     argument2     => 1000,
                                     argument3     => 'N',
                                     argument4     => 'N',
                                     argument5     => 'N');

      IF l_request_id1 = 0
      THEN
         fnd_file.put_line (
            fnd_file.LOG,
            'ERROR: Supplier Open Interface Import :' || l_request_id1);
         RETURN;
      END IF;

      COMMIT;
      v_wait_outcome :=
         fnd_concurrent.wait_for_request (request_id   => l_request_id1,
                                          INTERVAL     => 10,
                                          phase        => v_phase,
                                          status       => v_status,
                                          dev_phase    => v_dev_phase,
                                          dev_status   => v_dev_status,
                                          MESSAGE      => v_message);
      fnd_file.put_line (
         fnd_file.LOG,
         'Suppliers Open Interface Import : Request ID ' || l_request_id1);
      fnd_file.put_line (
         fnd_file.LOG,
         'Program Status :' || v_dev_phase || ' / ' || v_dev_status);

      IF UPPER (v_dev_phase) = 'COMPLETE' OR UPPER (v_dev_status) = 'NORMAL'
      THEN
         l_request_id1 :=
            fnd_request.submit_request (application   => 'SQLAP',
                                        program       => 'APXSSIMP',
                                        description   => NULL,
                                        start_time    => SYSDATE,
                                        sub_request   => FALSE,
                                        argument1     => '162',
                                        argument2     => 'ALL',
                                        argument3     => '1000',
                                        argument4     => 'N',
                                        argument5     => 'N',
                                        argument6     => 'N');

         IF l_request_id1 = 0
         THEN
            fnd_file.put_line (
               fnd_file.LOG,
               'ERROR: Supplier Sites Open Interface Import' || l_request_id1);
            RETURN;
         END IF;

         COMMIT;
         v_wait_outcome :=
            fnd_concurrent.wait_for_request (request_id   => l_request_id1,
                                             INTERVAL     => 10,
                                             phase        => v_phase,
                                             status       => v_status,
                                             dev_phase    => v_dev_phase,
                                             dev_status   => v_dev_status,
                                             MESSAGE      => v_message);
         fnd_file.put_line (
            fnd_file.LOG,
               'Supplier Sites Open Interface Import : Request ID '
            || l_request_id1);
         fnd_file.put_line (
            fnd_file.LOG,
            'Program Status :' || v_dev_phase || ' / ' || v_dev_status);

         IF    UPPER (v_dev_phase) = 'COMPLETE'
            OR UPPER (v_dev_status) = 'NORMAL'
         THEN
            -- update site contacts interface with vendor site id..
            UPDATE ap_sup_site_contact_int a
               SET vendor_site_id =
                      (SELECT vendor_site_id
                         FROM ap_supplier_sites b
                        WHERE     b.vendor_id = a.vendor_id
                              AND a.vendor_site_code = b.vendor_site_code
                              AND a.org_id = b.org_id)
             WHERE TRUNC (creation_date) = TRUNC (SYSDATE) AND status = 'NEW';

            COMMIT;
            l_request_id1 :=
               fnd_request.submit_request (application   => 'SQLAP',
                                           program       => 'APXSCIMP',
                                           description   => NULL,
                                           start_time    => SYSDATE,
                                           sub_request   => FALSE,
                                           argument1     => 'NEW',
                                           argument2     => 1000,
                                           argument3     => 'N',
                                           argument4     => 'N',
                                           argument5     => 'N');

            IF l_request_id1 = 0
            THEN
               fnd_file.put_line (
                  fnd_file.LOG,
                     'ERROR: Supplier Site Contacts Open Interface Import : Contact Thiru '
                  || l_request_id1);
               RETURN;
            END IF;

            COMMIT;
            v_wait_outcome :=
               fnd_concurrent.wait_for_request (request_id   => l_request_id1,
                                                INTERVAL     => 10,
                                                phase        => v_phase,
                                                status       => v_status,
                                                dev_phase    => v_dev_phase,
                                                dev_status   => v_dev_status,
                                                MESSAGE      => v_message);
            fnd_file.put_line (
               fnd_file.LOG,
                  'Supplier Site Contacts Open Interface Import : Request ID '
               || l_request_id1);
            fnd_file.put_line (
               fnd_file.LOG,
               'Program Status :' || v_dev_phase || ' / ' || v_dev_status);
         END IF;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         v_errorcode := SQLCODE;
         v_errormessage := SUBSTR (SQLERRM, 1, 200);
         fnd_file.put_line (
            fnd_file.LOG,
            'Exception Block of Submitting Standard Procedure Procedure');
         fnd_file.put_line (fnd_file.LOG,
                            'The Error Code Traced is : ' || v_errorcode);
         fnd_file.put_line (
            fnd_file.LOG,
            'The Error Message Traced is : ' || v_errormessage);
   END xxwc_supp_import_stand;

   PROCEDURE xxwc_supp_bank_branch
   IS
      lv_result_rec_type          iby_fndcpt_common_pub.result_rec_type;
      lv_error_code               VARCHAR2 (32000) := NULL;
      lv_msg_data                 VARCHAR2 (1000) := NULL;
      lv_msg_count                NUMBER := NULL;
      lv_return_status            VARCHAR2 (100) := NULL;
      lv_extbank_rec_type         iby_ext_bankacct_pub.extbank_rec_type;
      lv_extbankbranch_rec_type   iby_ext_bankacct_pub.extbankbranch_rec_type;
      lv_extbankacct_rec_type     iby_ext_bankacct_pub.extbankacct_rec_type;
      lv_bank_id                  NUMBER;
      lv_bank_id1                 NUMBER;
      lv_branch_id                NUMBER;
      lv_branch_id1               NUMBER;
      lv_acct_id                  NUMBER;
      lv_acct_id1                 NUMBER;
      lv_error_flag               VARCHAR2 (1);
      lv_cnt                      NUMBER := 0;
      lv_branch                   NUMBER := 0;
      lv_msg_dummy                VARCHAR2 (2000);
      lv_error_reason             VARCHAR2 (4000);
      lv_error                    VARCHAR2 (4000);

      CURSOR cur_bank
      IS
         SELECT DISTINCT iby.bank_party_id,
                         iby.bank_name,
                         bank_account_name,
                         xx.branch_number
           FROM iby_ext_banks_v iby, xxwc_iby_bank_accounts xx
          WHERE     UPPER (iby.bank_name) = UPPER (xx.bank_account_name)
                AND xx.bank_party_id IS NULL;

      CURSOR cur_branch
      IS
         SELECT DISTINCT ieb.branch_party_id,
                         xib.bank_party_id,
                         ieb.bank_branch_name,
                         xib.branch_number
           FROM iby_ext_bank_branches_v ieb, xxwc_iby_bank_accounts xib
          WHERE     ieb.branch_number = xib.branch_number
                AND ieb.bank_party_id = xib.bank_party_id
                AND xib.bank_branch_id IS NULL;

      CURSOR cur_bank_creation
      IS
         SELECT DISTINCT bank_account_name                   --, branch_number
           FROM xxwc_iby_bank_accounts
          WHERE bank_party_id IS NULL;

      CURSOR cur_branch_creation
      IS
         SELECT DISTINCT branch_number, bank_party_id, bank_account_name
           FROM xxwc_iby_bank_accounts
          WHERE bank_branch_id IS NULL AND bank_party_id IS NOT NULL;
   BEGIN
      FOR bank_info IN cur_bank
      LOOP
         UPDATE xxwc_iby_bank_accounts
            SET bank_party_id = bank_info.bank_party_id
          WHERE     UPPER (bank_account_name) =
                       UPPER (bank_info.bank_account_name)
                AND bank_party_id IS NULL;
      END LOOP;

      COMMIT;

      FOR branch_info IN cur_branch
      LOOP
         UPDATE xxwc_iby_bank_accounts
            SET bank_branch_id = branch_info.branch_party_id
          WHERE     branch_number = branch_info.branch_number
                AND bank_party_id = branch_info.bank_party_id
                AND bank_branch_id IS NULL;
      END LOOP;

      COMMIT;

      FOR i IN cur_bank_creation
      LOOP
         lv_error_code := NULL;
         lv_return_status := NULL;
         lv_msg_count := NULL;
         lv_msg_data := NULL;
         lv_extbank_rec_type.object_version_number := 1.0;
         lv_extbank_rec_type.bank_name := UPPER (i.bank_account_name);
         lv_extbank_rec_type.country_code := 'US';

         BEGIN
            iby_ext_bankacct_pub.create_ext_bank (
               p_api_version     => 1.0,
               p_init_msg_list   => fnd_api.g_true,
               p_ext_bank_rec    => lv_extbank_rec_type,
               x_bank_id         => lv_bank_id,
               x_return_status   => lv_return_status,
               x_msg_count       => lv_msg_count,
               x_msg_data        => lv_msg_data,
               x_response        => lv_result_rec_type);

            IF lv_return_status <> fnd_api.g_ret_sts_success
            THEN
               fnd_file.put_line (fnd_file.LOG, ('ERROR ' || lv_msg_count));

               IF lv_msg_count >= 1
               THEN
                  fnd_file.put_line (fnd_file.LOG,
                                     ('ERROR DATA' || lv_msg_data));

                  FOR i IN 1 .. lv_msg_count
                  LOOP
                     apps.fnd_msg_pub.get (i,
                                           fnd_api.g_false,
                                           lv_msg_data,
                                           lv_msg_dummy);
                     lv_error_code :=
                           lv_error_code
                        || '-'
                        || ('Msg' || TO_CHAR (i) || ': ' || lv_msg_data);
                     fnd_file.put_line (fnd_file.LOG,
                                        (SUBSTR (lv_error_code, 1, 250)));
                     lv_error_reason := lv_error_code;
                     fnd_file.put_line (
                        fnd_file.LOG,
                        ('Error in Bank:' || lv_error_reason));
                  END LOOP;

                  UPDATE xxwc_iby_bank_accounts
                     SET status = 'E', error_reason = lv_error_reason     ----
                   WHERE     UPPER (bank_account_name) =
                                UPPER (i.bank_account_name)
                         AND bank_party_id IS NULL;
               --AND branch_number = i.branch_number;

               --COMMIT;
               END IF;
            ELSE
               lv_bank_id1 := lv_bank_id;
               fnd_file.put_line (fnd_file.LOG, ('SUCCESS ' || lv_bank_id1));

               --needs update staemnte for staging table
               UPDATE xxwc_iby_bank_accounts
                  SET bank_party_id = lv_bank_id1, status = 'B'
                WHERE     UPPER (bank_account_name) =
                             UPPER (i.bank_account_name)
                      AND bank_party_id IS NULL;
            --AND branch_number = i.branch_number;

            --COMMIT;
            END IF;
         END;
      END LOOP;

      FOR j IN cur_branch_creation
      LOOP
         lv_error_code := NULL;
         lv_error := NULL;
         lv_return_status := NULL;
         lv_msg_count := NULL;
         lv_msg_data := NULL;
         lv_extbankbranch_rec_type.bank_party_id := j.bank_party_id;
         lv_extbankbranch_rec_type.branch_name := j.branch_number;
         lv_extbankbranch_rec_type.branch_number := j.branch_number;
         lv_extbankbranch_rec_type.branch_type := 'ABA';
         iby_ext_bankacct_pub.create_ext_bank_branch (
            p_api_version           => 1.0,
            p_init_msg_list         => fnd_api.g_true,
            p_ext_bank_branch_rec   => lv_extbankbranch_rec_type,
            x_branch_id             => lv_branch_id,
            x_return_status         => lv_return_status,
            x_msg_count             => lv_msg_count,
            x_msg_data              => lv_msg_data,
            x_response              => lv_result_rec_type);

         IF lv_return_status <> fnd_api.g_ret_sts_success
         THEN
            fnd_file.put_line (fnd_file.LOG, 'BRANCH ERROR ' || lv_msg_count);

            IF lv_msg_count >= 1
            THEN
               FOR i IN 1 .. lv_msg_count
               LOOP
                  apps.fnd_msg_pub.get (i,
                                        fnd_api.g_false,
                                        lv_msg_data,
                                        lv_msg_dummy);
                  lv_error_code :=
                        lv_error_code
                     || '-'
                     || ('Msg' || TO_CHAR (i) || ': ' || lv_msg_data);
                  fnd_file.put_line (fnd_file.LOG,
                                     (SUBSTR (lv_error_code, 1, 250)));
                  lv_error := lv_error_code;
                  fnd_file.put_line (fnd_file.LOG,
                                     ('Error in Branch:' || lv_error));
               END LOOP;

               UPDATE xxwc_iby_bank_accounts
                  SET status = 'E', error_reason = lv_error
                WHERE     bank_party_id = j.bank_party_id
                      AND branch_number = j.branch_number
                      AND UPPER (bank_account_name) =
                             UPPER (j.bank_account_name);
            --COMMIT;
            END IF;
         ELSE                               --SUCCESS FUL FOR CREATED ON BRACH
            lv_branch_id1 := lv_branch_id;
            fnd_file.put_line (fnd_file.LOG,
                               ('BRANCH SUCCESS ' || lv_branch_id1));

            --update staging table
            UPDATE xxwc_iby_bank_accounts
               SET bank_branch_id = lv_branch_id1, status = 'C'
             WHERE     bank_party_id = j.bank_party_id
                   AND branch_number = j.branch_number
                   AND UPPER (bank_account_name) =
                          UPPER (j.bank_account_name);
         -- COMMIT;
         END IF;
      END LOOP;

      COMMIT;
   END xxwc_supp_bank_branch;

   PROCEDURE xxwc_supp_acct_assignment
   IS
      lv_bank_acct_rec         apps.iby_ext_bankacct_pub.extbankacct_rec_type;
      lv_out_mesg              apps.iby_fndcpt_common_pub.result_rec_type;
      lv_acct                  NUMBER;
      lv_assigns               apps.iby_fndcpt_setup_pub.pmtinstrassignment_tbl_type;
      lv_payee_rec             apps.iby_disbursement_setup_pub.payeecontext_rec_type;
      lv_return_status         VARCHAR2 (30);
      lv_output                VARCHAR2 (2000);
      lv_msg_count             NUMBER;
      lv_msg_data              VARCHAR2 (150) := NULL;
      lv_msg_dummy             VARCHAR2 (150) := NULL;
      lv_bank_id               NUMBER;
      lv_branch_id             NUMBER;
      lv_party_id              NUMBER;
      --   l_bank                              VARCHAR2 (100);
      lv_acct_owner_party_id   NUMBER;
      lv_supplier_site_id      NUMBER;
      lv_party_site_id         NUMBER;
      lv_process_assign        VARCHAR2 (1);
      exec_bank_acct           EXCEPTION;
      lv_rec                   iby_disbursement_setup_pub.payeecontext_rec_type;
      lv_assign                iby_fndcpt_setup_pub.pmtinstrassignment_rec_type;
      lv_assign_id             NUMBER;
      --lv_return_status             VARCHAR2 (100);
      --lv_msg_count                 NUMBER;
      --lv_msg_data                  VARCHAR2 (200);
      lv_response_rec          iby_fndcpt_common_pub.result_rec_type;
      lv_acc_id                NUMBER := 0;
      lv_error_output          VARCHAR2 (1000);
      lv_error_reason          VARCHAR2 (1000);

      -- lv_msg_dummy                   VARCHAR2 (1000);
      CURSOR cur_acc
      IS
         SELECT a.ROWID, a.*
           FROM xxwc_iby_bank_accounts a
          WHERE     bank_party_id IS NOT NULL
                AND bank_branch_id IS NOT NULL
                AND vendor_party_id IS NULL
                AND NVL (status, 'E') <> 'P';

      CURSOR cur_acc_assign
      IS
         SELECT a.ROWID, a.*
           FROM xxwc_iby_bank_accounts a
          WHERE     bank_party_id IS NOT NULL
                AND bank_branch_id IS NOT NULL
                AND vendor_party_id IS NOT NULL
                AND NVL (status, 'E') <> 'P';
   BEGIN
      FOR i IN cur_acc
      LOOP
         BEGIN
            SELECT party_id
              INTO lv_party_id
              FROM ap_suppliers
             WHERE attribute1 = i.vendor_number;

            UPDATE xxwc_iby_bank_accounts
               SET vendor_party_id = lv_party_id
             WHERE ROWID = i.ROWID;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               lv_error_output :=
                  ('party id is null for this vendor:' || i.vendor_number);

               UPDATE xxwc_iby_bank_accounts
                  SET status = 'E', error_reason = lv_error_output
                WHERE ROWID = i.ROWID;
            WHEN TOO_MANY_ROWS
            THEN
               lv_error_output :=
                  ('Too many party id for this vendor:' || i.vendor_number);

               UPDATE xxwc_iby_bank_accounts
                  SET status = 'E', error_reason = lv_error_output
                WHERE ROWID = i.ROWID;
         END;
      END LOOP;

      COMMIT;

      FOR i IN cur_acc_assign
      LOOP
         lv_error_output := NULL;
         lv_error_reason := NULL;
         lv_output := NULL;
         lv_return_status := NULL;
         lv_msg_count := NULL;
         lv_supplier_site_id := NULL;
         --l_msg_data := NULL;
         apps.fnd_msg_pub.delete_msg (NULL);
         apps.fnd_msg_pub.initialize ();
         ---------------------------------------------------------------------
         lv_bank_acct_rec.branch_id := NULL;
         lv_bank_acct_rec.bank_id := NULL;
         lv_party_id := NULL;
         lv_bank_acct_rec.acct_owner_party_id := NULL;
         lv_bank_acct_rec.bank_account_name := NULL;
         lv_bank_acct_rec.bank_account_num := NULL;
         lv_bank_acct_rec.iban := NULL;
         lv_bank_acct_rec.start_date := SYSDATE;
         lv_bank_acct_rec.country_code := NULL;
         lv_bank_acct_rec.currency := NULL;
         lv_bank_acct_rec.foreign_payment_use_flag := NULL;
         lv_bank_acct_rec.alternate_acct_name := NULL;
         lv_error_reason := NULL;
         lv_process_assign := NULL;
         lv_bank_acct_rec.branch_id := i.bank_branch_id;
         --l_bank_acct_rec.branch_name := i.branch_number;
         lv_bank_acct_rec.bank_id := i.bank_party_id;
         lv_bank_acct_rec.acct_owner_party_id := i.vendor_party_id;
         lv_bank_acct_rec.bank_account_name := UPPER (i.bank_account_name);
         lv_bank_acct_rec.bank_account_num := i.bank_account_num;
         lv_bank_acct_rec.country_code := 'US';
         lv_bank_acct_rec.currency := 'USD';
         lv_bank_acct_rec.foreign_payment_use_flag := 'N';
         lv_acct := NULL;
         lv_bank_acct_rec.object_version_number := NULL;

         BEGIN
            SELECT a.ext_bank_account_id
              INTO lv_acct
              FROM iby_ext_bank_accounts a --, IBY_PMT_INSTR_USES_ALL b, IBY_EXTERNAL_PAYEES_ALL c
             WHERE     a.bank_id = i.bank_party_id
                   AND a.branch_id = i.bank_branch_id
                   AND a.bank_account_num = i.bank_account_num;

            -- added by Shankar25-Feb-2012 for dup bank account issue
            /*
            and a.ext_bank_account_id=b.instrument_id
            and b.ext_pmt_party_id= c.ext_payee_id
            and c.payee_party_id=i.vendor_party_id
            and c.party_site_id is not null
            and c.default_payment_method_code is not null;
           */
            fnd_file.put_line (
               fnd_file.output,
               (' ext BANK ACCOUNT EXISTS ' || i.bank_account_num));
            lv_process_assign := 'Y';

            IF i.bank_ext_account_id IS NULL
            THEN
               lv_bank_acct_rec.object_version_number := 2;
               raise_application_error (20001, 'Existing account');
            END IF;
         EXCEPTION
            WHEN OTHERS
            THEN
               apps.iby_ext_bankacct_pub.create_ext_bank_acct (
                  p_api_version         => 1.0,
                  p_init_msg_list       => 'F',
                  p_ext_bank_acct_rec   => lv_bank_acct_rec,
                  x_acct_id             => lv_acct,
                  x_return_status       => lv_return_status,
                  x_msg_count           => lv_msg_count,
                  x_msg_data            => lv_msg_data,
                  x_response            => lv_out_mesg);

               -- l_output := ' ';
               IF lv_return_status = fnd_api.g_ret_sts_error
               THEN
                  fnd_file.put_line (
                     fnd_file.LOG,
                     (' ext assignment ERROR ' || lv_msg_count));

                  FOR i IN 1 .. lv_msg_count
                  LOOP
                     apps.fnd_msg_pub.get (i,
                                           fnd_api.g_false,
                                           lv_msg_data,
                                           lv_msg_dummy);
                     lv_error_reason :=
                           lv_error_reason
                        || '-'
                        || ('Msg' || TO_CHAR (i) || ': ' || lv_msg_data);
                     fnd_file.put_line (fnd_file.LOG,
                                        (SUBSTR (lv_error_reason, 1, 250)));
                     lv_error_output := lv_error_reason;
                     fnd_file.put_line (
                        fnd_file.LOG,
                        ('Error in Account:' || lv_error_output));
                  END LOOP;

                  UPDATE xxwc_iby_bank_accounts
                     SET status = 'E', error_reason = lv_error_output
                   WHERE ROWID = i.ROWID;

                  lv_process_assign := 'N';
               --                COMMIT;
               ELSE
                  lv_process_assign := 'Y';
               END IF;
         END;

         IF lv_process_assign = 'Y'
         THEN
            lv_acc_id := lv_acct;
            fnd_file.put_line (fnd_file.LOG, ('Success ' || lv_acc_id));

            --get the vendor site from the first found pay site
            BEGIN
               SELECT vendor_site_id, party_site_id
                 INTO lv_supplier_site_id, lv_party_site_id
                 FROM ap_supplier_sites
                WHERE     vendor_id = (SELECT vendor_id
                                         FROM ap_suppliers
                                        WHERE attribute1 = i.vendor_number)
                      AND pay_site_flag = 'Y'
                      AND vendor_site_code LIKE 'PAY%'
                      AND org_id = xxwc_ascp_scwb_pkg.get_wc_org_id --fnd_global.org_id
                      AND ROWNUM = 1;
            EXCEPTION
               WHEN OTHERS
               THEN
                  lv_supplier_site_id := NULL;
                  lv_party_site_id := NULL;
            END;

            UPDATE xxwc_iby_bank_accounts
               SET bank_ext_account_id = lv_acc_id, status = 'A'
             WHERE ROWID = i.ROWID;

            --COMMIT;
            --END LOOP;

            -- FOR J IN CUR_ACC_ASSIGN (I.BANK_EXT_ACCOUNT_ID)
            -- LOOP
            lv_assign.instrument.instrument_type := 'BANKACCOUNT';
            lv_assign.instrument.instrument_id := lv_acc_id;
            lv_rec.party_id := i.vendor_party_id;
            lv_rec.org_id := xxwc_ascp_scwb_pkg.get_wc_org_id; --fnd_global.org_id;
            lv_rec.org_type := 'OPERATING_UNIT';
            lv_rec.party_site_id := lv_party_site_id;
            lv_rec.supplier_site_id := lv_supplier_site_id;
            lv_rec.payment_function := 'PAYABLES_DISB';
            iby_disbursement_setup_pub.set_payee_instr_assignment (
               p_api_version          => 1.0,
               p_init_msg_list        => fnd_api.g_true,
               p_payee                => lv_rec,
               p_assignment_attribs   => lv_assign,
               x_assign_id            => lv_assign_id,
               x_return_status        => lv_return_status,
               x_msg_count            => lv_msg_count,
               x_msg_data             => lv_msg_data,
               x_response             => lv_response_rec);

            IF lv_return_status = fnd_api.g_ret_sts_error
            THEN
               fnd_file.put_line (fnd_file.LOG,
                                  (' Assignment ERROR ' || lv_msg_count));

               FOR i IN 1 .. lv_msg_count
               LOOP
                  apps.fnd_msg_pub.get (i,
                                        apps.fnd_api.g_false,
                                        lv_msg_data,
                                        lv_msg_dummy);
                  lv_output :=
                        lv_output
                     || (TO_CHAR (i) || ': ' || SUBSTR (lv_msg_data, 1, 250));
                  fnd_file.put_line (fnd_file.LOG,
                                     (SUBSTR (lv_output, 1, 250)));
                  fnd_file.put_line (fnd_file.LOG,
                                     ('Error in Assignment:' || lv_output));
               END LOOP;

               UPDATE xxwc_iby_bank_accounts
                  SET status = 'E',
                      error_reason = 'assignment error:' || lv_output
                WHERE ROWID = i.ROWID;
            ELSE
               UPDATE xxwc_iby_bank_accounts
                  SET status = 'P'
                WHERE ROWID = i.ROWID;
            --COMMIT;
            END IF;
         END IF;
      END LOOP;
   END xxwc_supp_acct_assignment;

   PROCEDURE xxwc_main (errbuf OUT VARCHAR2, retcode OUT VARCHAR2)
   IS
   BEGIN
      xxwc_supp_bank_branch;
      xxwc_supp_acct_assignment;
   END;

   /*********************************************************************************************
    *    Function Name: derive_ccid
    *    history:
    *    version    date              author             description
    ********************************************************************************************
    *    1.2        22-May-2018   P.Vamshidhar  TMS#20180308-00301 AH HARRIS Supplier Conversion
    ********************************************************************************************/


   FUNCTION derive_ccid (p_concat_segs IN VARCHAR2, x_ret_msg OUT VARCHAR2)
      RETURN NUMBER
   IS
      ln_ccid               GL_CODE_COMBINATIONS.CODE_COMBINATION_ID%TYPE;
      ln_structure_num      NUMBER;
      l_valid_combination   BOOLEAN;
      l_cr_combination      BOOLEAN;
      lvc_sec               VARCHAR2 (4000);
   BEGIN
      lvc_sec := 'Procedure Start';

      BEGIN
         lvc_sec := 'Deriving CCID';

         SELECT code_combination_id
           INTO ln_ccid
           FROM gl_code_combinations
          WHERE (   segment1
                 || '.'
                 || segment2
                 || '.'
                 || segment3
                 || '.'
                 || segment4
                 || '.'
                 || segment5
                 || '.'
                 || segment6
                 || '.'
                 || segment7) = p_concat_segs;
      EXCEPTION
         WHEN OTHERS
         THEN
            ln_ccid := NULL;
      END;

      -- Validating CVR rule if code combination not exists
      IF ln_ccid IS NULL
      THEN
         BEGIN
            lvc_sec := ' Deriving Chart Account';
            ln_structure_num := NULL;

            SELECT id_flex_num
              INTO ln_structure_num
              FROM apps.fnd_id_flex_structures
             WHERE     id_flex_code = 'GL#'
                   AND id_flex_structure_code = 'HDS Accounting Flexfield';
         EXCEPTION
            WHEN OTHERS
            THEN
               ln_structure_num := NULL;
         END;

         lvc_sec := 'Validating CVR for segments';
         l_valid_combination :=
            APPS.FND_FLEX_KEYVAL.VALIDATE_SEGS (
               operation          => 'CHECK_COMBINATION',
               appl_short_name    => 'SQLGL',
               key_flex_code      => 'GL#',
               structure_number   => ln_structure_num,
               concat_segments    => p_concat_segs);

         IF l_valid_combination
         THEN
            lvc_sec := 'Creating Code combination';
            l_cr_combination :=
               APPS.FND_FLEX_KEYVAL.VALIDATE_SEGS (
                  operation          => 'CREATE_COMBINATION',
                  appl_short_name    => 'SQLGL',
                  key_flex_code      => 'GL#',
                  structure_number   => ln_structure_num,
                  concat_segments    => p_concat_segs);

            BEGIN
               lvc_sec := 'Deriving CCID (After Create)';

               SELECT code_combination_id
                 INTO ln_ccid
                 FROM gl_code_combinations_kfv
                WHERE concatenated_segments = p_concat_segs;
            EXCEPTION
               WHEN OTHERS
               THEN
                  ln_ccid := NULL;
            END;
         END IF;
      END IF;

      RETURN ln_ccid;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         x_ret_msg := 'No Code Combination Exists';
      WHEN OTHERS
      THEN
         x_ret_msg := 'Error Msg:' || SUBSTR (SQLERRM, 1, 250);
   END;
END XXWC_AP_SUPPLIERS_CONV_PKG;
/