CREATE OR REPLACE PACKAGE BODY APPS.XXCUS_COSTAR_AP_EXPORT AS
/**************************************************************************
   $Header XXCUS_COSTAR_AP_EXPORT $
   Module Name: XXCUS_COSTAR_AP_EXPORT.pkb

   PURPOSE:   This package body is called by the concurrent programs
              XXCUS CoStar AP Invoices Import for importing AP invoices into EBS system.
   REVISIONS:
   Ver        Date        Author             	Description
   ---------  ----------  ---------------   	-------------------------
    1.0       25/06/2018  Ashwin Sridhar /    	Initial Build - Task ID: 20180709-00059
						  Vamshi Singirikonda	Adding GL External ID
/*************************************************************************/
--Global Variables...
g_loc                    VARCHAR2(20000);
g_qty_invoiced           NUMBER:=1;
g_line_type_lkup_code    VARCHAR2(10):='ITEM';
g_org_id                 NUMBER:=163;
g_user_id                NUMBER:=fnd_global.user_id;
g_request_id             NUMBER:=fnd_global.conc_request_id;
g_invoicw_source         VARCHAR2(15):='COSTAR';
g_attribute_cat          VARCHAR2(15):='COSTAR';
g_invoice_type_lkup_code VARCHAR2(10):='STANDARD';
g_currency               VARCHAR2(10):='USD';
g_paygroup_lkup_code     VARCHAR2(10):='RENT';
g_terms_name             VARCHAR2(20):='DUE UPON RECEIPT';
g_file_name              VARCHAR2(240):=NULL;
g_delimiter              VARCHAR2(1):='|';
g_inbound_loc            VARCHAR2(240):=NULL;
g_directory_path         VARCHAR2(150):='XXCUS_COSTAR_APINV_DIR_IB';
g_pkg_name               VARCHAR2(50):='XXCUS_COSTAR_AP_EXPORT';
g_run_id                 NUMBER;
g_group_id               VARCHAR2(50);
g_seq                    NUMBER:=0;
g_status1                VARCHAR2(240):='File Copied to custom staging';
g_status2                VARCHAR2(240):='Interfaced to AP Invoice Lines Interface';
g_status3                VARCHAR2(240):='Interfaced to AP Invoice Headers Interface';
g_fiscal_period          VARCHAR2(10):=TO_CHAR(SYSDATE,'Mon-YYYY');
g_invoice_source         VARCHAR2(10) := 'COSTAR' ;
                         
/*************************************************************************
  Procedure : Write_Log

  PURPOSE:   This procedure logs debug message in Concurrent Log file
  Parameter: IN  p_debug_msg      -- Debug Message
  REVISIONS:
  Ver        Date        Author             Description
  ---------  ----------  ---------------   -------------------------
  1.0        25/06/2018  Ashwin Sridhar     Initial Build - Task ID: 20180709-00059
************************************************************************/
PROCEDURE write_log (p_debug_msg IN VARCHAR2) IS

BEGIN
      
  fnd_file.put_line (fnd_file.LOG, p_debug_msg);
  DBMS_OUTPUT.put_line (p_debug_msg);

END write_log;

/*************************************************************************
  Procedure : write_error

  PURPOSE:   This procedure logs debug message in Concurrent Out file
  Parameter: IN  p_debug_msg      -- Debug Message
  REVISIONS:
  Ver        Date        Author             Description
  ---------  ----------  ---------------   -------------------------
  1.0        25/06/2018  Ashwin Sridhar     Initial Build - Task ID: 20180709-00059
************************************************************************/
--Add message to concurrent output file
PROCEDURE write_error (p_debug_msg IN VARCHAR2) IS

l_req_id          NUMBER := fnd_global.conc_request_id;
l_err_callfrom    VARCHAR2 (75) DEFAULT 'XXCUS_COSTAR_AP_EXPORT';
l_err_callpoint   VARCHAR2 (75) DEFAULT 'START';
l_distro_list     VARCHAR2 (75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';

BEGIN

  xxcus_error_pkg.xxcus_error_main_api (
      p_called_from         => l_err_callfrom,
      p_calling             => l_err_callpoint,
      p_request_id          => l_req_id,
      p_ora_error_msg       => SUBSTR (p_debug_msg, 1, 2000),
      p_error_desc          => 'Error running XXCUS_COSTAR_AP_EXPORT with PROGRAM ERROR',
      p_distribution_list   => l_distro_list,
      p_module              => 'HRMS');

END write_error;

/*************************************************************************
  Function : get_run_id

  PURPOSE:   This Function return the run id for the given program run.
  Parameter: NA
  REVISIONS:
  Ver        Date        Author             Description
  ---------  ----------  ---------------   -------------------------
  1.0        25/06/2018  Ashwin Sridhar     Initial Build - Task ID: 20180709-00059
************************************************************************/
FUNCTION get_run_id RETURN NUMBER IS

  ln_run_seq NUMBER;

BEGIN

  ln_run_seq:=XXCUS.XXCUS_COSTAR_AP_EXPORT_RUN_S.NEXTVAL;
  g_run_id:=ln_run_seq;
  
  RETURN ln_run_seq;

EXCEPTION 
WHEN others THEN

  RETURN 0;
  
END get_run_id;

/*************************************************************************
  Function : get_group_id

  PURPOSE:   This Function return the group id for the given program run.
  Parameter: NA
  REVISIONS:
  Ver        Date        Author             Description
  ---------  ----------  ---------------   -------------------------
  1.0        25/06/2018  Ashwin Sridhar     Initial Build - Task ID: 20180709-00059
************************************************************************/
FUNCTION get_group_id RETURN VARCHAR2 IS
BEGIN

  RETURN 'CORP.COSTAR.'||TO_CHAR(SYSDATE,'YYYYMMDD')||'.'||g_run_id;

EXCEPTION 
WHEN others THEN

  RETURN NULL;
  
END get_group_id;

/*************************************************************************
  Procedure : Process_Invoices

  PURPOSE:   This main procedure extracts the AP invoice data from data file and loads into EBS system.
             This also calls many subroutines in the process.
  Parameter: IN  p_errbuf 
             IN  p_retcode
             IN  p_file_name
             IN  p_run_id
             IN  p_group_id 
  REVISIONS:
  Ver        Date        Author             Description
  ---------  ----------  ---------------   -------------------------
  1.0        25/06/2018  Ashwin Sridhar     Initial Build - Task ID: 20180709-00059
************************************************************************/
PROCEDURE Process_Invoices(p_errbuf    OUT VARCHAR2
                          ,p_retcode   OUT VARCHAR2
                          ,p_file_name IN  VARCHAR2
                          ,p_run_id    IN  NUMBER
                          ,p_group_id  IN  VARCHAR2
                          ) IS

l_count            NUMBER;
g_prog_exception   EXCEPTION;
v_file             UTL_FILE.file_type;
l_err_msg          VARCHAR2 (4000);  
ln_ap_header_seq   NUMBER;  
ln_ap_line_seq     NUMBER;
ln_vendor_id       NUMBER;
ln_vendor_site_id  NUMBER;
lv_dist_glcode     VARCHAR2(100);
lv_ovr_costar_prf  VARCHAR2(1);
lv_line_error      VARCHAR2(1):='N'; 
lv_attribute1      VARCHAR2(240);
lv_line_desc       VARCHAR2(240);

--Invoice Header Records...
CURSOR cu_header IS
SELECT a.*,a.rowid row_id
FROM  XXCUS.XXCUS_COSTAR_AP_EXPORT_INTF a
WHERE RECORD_TYPE='HEADER'
AND   run_id=p_run_id;

--Invoice Line Records...
CURSOR cu_line(cp_invoice_number IN VARCHAR2
              ,cp_run_id         IN NUMBER) IS
SELECT b.*,b.rowid row_id
FROM  XXCUS.XXCUS_COSTAR_AP_EXPORT_INTF b
WHERE RECORD_TYPE='DETAIL'
AND   INVOICE_NUMBER=cp_invoice_number
AND   run_id=cp_run_id;

BEGIN

  g_file_name:=p_file_name;
  g_inbound_loc:=g_directory_path;
  g_group_id:=p_group_id;
  lv_ovr_costar_prf:=NVL(fnd_profile.value('XXCUS_OVERRIDE_COSTAR_AP_GL_ALL_CODE'),'Y');

  --Printing in Parameters
  g_loc := 'Begining sample file Extract  ';
  write_log (g_loc);
  write_log ('========================================================');
  write_log ('File Name -'||g_file_name);
  write_log ('Calling the Invoice Load procedure');
  
  --Calling Procedure to Load data from data file to staging table...
  LOAD_INVOICES_DATA(p_run_id);

  write_log ('Data Loading into staging completed successfully...');
  
  write_log ('Calling the Audit Table Load procedure...');
  
  --Inserting into Audit Table
  LOAD_AUDIT_DATA(p_run_id   
                 ,p_group_id 
                 ,'STAGING'
                 );
  
  --Looping for the records in Invoice Staging table...
  FOR rec_header IN cu_header LOOP
  
    ln_ap_header_seq:=AP_INVOICES_INTERFACE_S.NEXTVAL;

    write_log ('Header Loop Invoice Number...'||rec_header.invoice_number);
    write_log ('Header Sequence ID...'||ln_ap_header_seq);

    BEGIN

       SELECT vendor_id
       INTO  ln_vendor_id
       FROM  ap_suppliers
       WHERE segment1=TRIM(rec_header.vendor_id);
       --AND TRUNC(SYSDATE) BETWEEN TRUNC(START_DATE_ACTIVE) AND TRUNC(NVL(END_DATE_ACTIVE,SYSDATE));

    EXCEPTION
    WHEN NO_DATA_FOUND THEN
  
       ln_vendor_site_id:=NULL;

    WHEN TOO_MANY_ROWS THEN

      ln_vendor_site_id:=NULL;

    WHEN others THEN

      ln_vendor_id:=NULL;

    END;
/*
    BEGIN

      SELECT vendor_site_id
      INTO   ln_vendor_site_id                         
      FROM   ap_supplier_sites_all
      WHERE  vendor_site_code = TRIM(rec_header.address_id)
      AND    vendor_id = ln_vendor_id;
      --AND    NVL(inactive_date,SYSDATE)>=SYSDATE;
 
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
  
       ln_vendor_site_id:=NULL;

    WHEN TOO_MANY_ROWS THEN

      ln_vendor_site_id:=NULL;

    WHEN others THEN
    
      ln_vendor_site_id:=NULL;

    END; */
  
    FOR rec_line IN cu_line(rec_header.INVOICE_NUMBER
                           ,p_run_id) LOOP

      ln_ap_line_seq:=ap_invoice_lines_interface_s.NEXTVAL;

      write_log ('Line Loop Line Number...'||rec_line.LINE_NUMBER);
      write_log ('Line Sequence ID...'||ln_ap_line_seq);
	  
	  

      IF lv_ovr_costar_prf='Y' THEN
	    IF rec_line.GL_ALLOC_CODE1 = '163' THEN
		rec_line.GL_ALLOC_CODE1 := '48' ;
		END IF ;
		      
        lv_dist_glcode:=rec_line.GL_ALLOC_CODE1||'.'||rec_line.GL_ALLOC_CODE2||'.'||rec_line.GL_ALLOC_CODE3
                                               ||'.'||rec_line.GL_EXTERNAL_ID||'.'||'00000.00000.00000';
      
      ELSE
      
        lv_dist_glcode:='48.'||rec_line.GL_ALLOC_CODE2||'.0000.135300.00000.00000.00000';
      
      END IF;
     
     lv_attribute1:=SUBSTR(rec_line.line_description,1,INSTR(rec_line.line_description,'_',1)-1);
     lv_line_desc:=SUBSTR(rec_line.line_description,INSTR(rec_line.line_description,'_',1)+1,LENGTH(rec_line.line_description));

      BEGIN

        INSERT INTO ap_invoice_lines_interface (invoice_id
                                               ,invoice_line_id
                                               ,line_number
                                               ,line_type_lookup_code
                                               ,amount
                                               ,accounting_date
                                               ,dist_code_concatenated
                                               ,quantity_invoiced
                                               ,last_updated_by
                                               ,last_update_date
                                               ,created_by
                                               ,creation_date
                                               ,org_id
                                               ,attribute_category
                                               ,attribute1
                                               ,description
                                               )
                                        VALUES (ln_ap_header_seq
                                               ,ln_ap_line_seq
                                               ,rec_line.line_number
                                               ,g_line_type_lkup_code
                                               ,TO_NUMBER (rec_line.line_amount)
                                               ,SYSDATE
                                               ,lv_dist_glcode
                                               ,g_qty_invoiced
                                               ,g_user_id
                                               ,SYSDATE
                                               ,g_user_id
                                               ,SYSDATE
                                               ,g_org_id
                                               ,g_attribute_cat
                                               ,lv_attribute1
                                               ,lv_line_desc
                                                );

        UPDATE XXCUS.XXCUS_COSTAR_AP_EXPORT_INTF
        SET    status =g_status2
        WHERE  rowid  =rec_line.row_id;
        
      EXCEPTION
      WHEN others THEN

        lv_line_error:='E';   

      END;

    END LOOP;
  
    IF lv_line_error='N' THEN

      INSERT INTO ap_invoices_interface(invoice_id
                                       ,invoice_num
                                       ,invoice_type_lookup_code
                                       ,invoice_date
                                       ,vendor_id
                                       ,invoice_amount
                                       ,invoice_currency_code
                                       ,terms_name
                                       ,last_update_date
                                       ,last_updated_by
                                       ,creation_date
                                       ,created_by
                                       ,gl_date
                                       ,org_id
                                       ,source
                                       ,vendor_site_id
                                       ,GROUP_ID
                                       ,payment_currency_code
                                       ,exclusive_payment_flag
                                       ,terms_date
                                       ,pay_group_lookup_code
                                       ,description
                                       )
                                VALUES (
                                       ln_ap_header_seq
                                      ,TRIM(rec_header.invoice_number)
                                      ,g_invoice_type_lkup_code
                                      ,rec_header.invoice_date
                                      ,ln_vendor_id
                                      ,TO_NUMBER(rec_header.invoice_amount)
                                      ,g_currency
                                      ,g_terms_name
                                      ,SYSDATE
                                      ,g_user_id
                                      ,SYSDATE
                                      ,g_user_id
                                      ,SYSDATE
                                      ,g_org_id 
                                      ,g_invoicw_source
                                      ,rec_header.address_id
                                      ,p_group_id
                                      ,g_currency
                                      ,'N'
                                      ,rec_header.invoice_date
                                      ,g_paygroup_lkup_code
                                      ,rec_header.invoice_description
                                      );

      UPDATE XXCUS.XXCUS_COSTAR_AP_EXPORT_INTF
      SET    status =g_status3
      WHERE  rowid  =rec_header.row_id;

    END IF;
  
  END LOOP;
  
  COMMIT;
  
  --Inserting Interface into Audit Table
  LOAD_AUDIT_DATA(p_run_id   
                 ,p_group_id 
                 ,'INTERFACE'
                 );
  
  --Initialize the Out Parameter
  p_errbuf := NULL;
  p_retcode := '0';
  
  write_log ('XXCUS CoStar AP Invoices Import - Program Successfully completed');

EXCEPTION
WHEN g_prog_exception THEN

   p_errbuf := l_err_msg;
   p_retcode := '2';
   write_error (l_err_msg);
   write_log (l_err_msg);
   
WHEN others THEN

   l_err_msg := 'Error Msg :' || SQLERRM;
   p_errbuf := l_err_msg;
   p_retcode := '2';
   write_error (l_err_msg);
   write_log (l_err_msg);

END Process_Invoices;

/*************************************************************************
  Procedure : LOAD_INVOICES_DATA

  PURPOSE:   This procedure imports the AP invoice data from data file and insert into staging table.
  Parameter: IN  p_run_id
  REVISIONS:
  Ver        Date        Author             Description
  ---------  ----------  ---------------   -------------------------
  1.0        25/06/2018  Ashwin Sridhar     Initial Build - Task ID: 20180709-00059
************************************************************************/
PROCEDURE LOAD_INVOICES_DATA(p_run_id IN NUMBER) IS

l_err_msg       VARCHAR2 (4000);
input_file_id   UTL_FILE.FILE_TYPE;
line_read       VARCHAR2(32767):= NULL;
line_count      NUMBER :=1;
lines_processed NUMBER :=0;
l_sub_routine   VARCHAR2(30) :='LOAD_INVOICES_DATA';
b_move_fwd      BOOLEAN;
l_record_type         VARCHAR2(30);  
l_invoice_number      VARCHAR2(30);  
l_invoice_date        DATE;         
l_location_id         VARCHAR2(100); 
l_location_name       VARCHAR2(240); 
l_vendor_id           VARCHAR2(30);        
l_address_id          VARCHAR2(15);        
l_terms               VARCHAR2(30);  
l_invoice_description VARCHAR2(240); 
l_currency            VARCHAR2(10);  
l_invoice_amount      NUMBER;        
l_line_number         NUMBER;        
l_line_description    VARCHAR2(240); 
l_line_amount         NUMBER;   
ln_user_id            NUMBER:=fnd_global.user_id;  
ln_request_id         NUMBER:=fnd_global.conc_request_id; 
l_gl_acct_extrnl_id   VARCHAR2(150);   
l_gl_alloc_code1      VARCHAR2(150); 
l_gl_alloc_code2      VARCHAR2(150); 
l_gl_alloc_code3      VARCHAR2(150); 
l_gl_alloc_code4      VARCHAR2(150);
l_gl_account_name     VARCHAR2(150);
l_gl_account_code     VARCHAR2(150);
l_gl_event_name       VARCHAR2(150);
l_temp_vendor_id      VARCHAR2(50) ;

BEGIN

  input_file_id  := utl_file.fopen(g_inbound_loc, g_file_name, 'R', 32767);
  write_log ('File opened for reading, file handle succesfully initiated...');
  g_run_id:=p_run_id;
  
  LOOP
  
    BEGIN

      utl_file.get_line(input_file_id, line_read, 32767);

      IF (line_count =1) THEN
      
        NULL; --ignore the header line
        write_log('Header :'||line_count||', Ignore header record.');
        
        line_count:= line_count+1; --set the line number to the next one in the file

      ELSE

        write_log('Line :'||line_count);

        BEGIN

          l_record_type :=regexp_replace(get_field(XXCUS_COSTAR_AP_EXPORT.g_delimiter, 1, line_read, line_count), '([^[:print:]])','');
          write_log('l_record_type =>'||l_record_type);
          l_invoice_number :=regexp_replace(get_field(XXCUS_COSTAR_AP_EXPORT.g_delimiter, 2, line_read, line_count), '([^[:print:]])','');
          write_log('l_invoice_number =>'||l_invoice_number);
          l_invoice_date :=to_date(regexp_replace(get_field(XXCUS_COSTAR_AP_EXPORT.g_delimiter, 3, line_read, line_count), '([^[:print:]])',''),'YYYY-MM-DD');
          write_log('l_invoice_date =>'||l_invoice_date);
          l_location_id :=regexp_replace(get_field(XXCUS_COSTAR_AP_EXPORT.g_delimiter, 4, line_read, line_count), '([^[:print:]])','');
          write_log('l_location_id =>'||l_location_id);
          l_location_name :=regexp_replace(get_field(XXCUS_COSTAR_AP_EXPORT.g_delimiter, 5, line_read, line_count), '([^[:print:]])','');
          write_log('l_location_name =>'||l_location_name);
          l_temp_vendor_id :=regexp_replace(get_field(XXCUS_COSTAR_AP_EXPORT.g_delimiter, 6, line_read, line_count), '([^[:print:]])','');
          l_vendor_id := substr(l_temp_vendor_id,1,instr(l_temp_vendor_id,'-')-1) ;
		  write_log('l_vendor_id =>'||l_vendor_id);
          -- l_address_id :=regexp_replace(get_field(XXCUS_COSTAR_AP_EXPORT.g_delimiter, 7, line_read, line_count), '([^[:print:]])','');
		  l_address_id := substr(l_temp_vendor_id,instr(l_temp_vendor_id,'-')+1) ; 
          write_log('l_address_id =>'||l_address_id);
          l_terms :=regexp_replace(get_field(XXCUS_COSTAR_AP_EXPORT.g_delimiter, 8, line_read, line_count), '([^[:print:]])','');
          write_log('l_terms =>'||l_terms);
          l_invoice_amount :=regexp_replace(get_field(XXCUS_COSTAR_AP_EXPORT.g_delimiter, 9, line_read, line_count), '([^[:print:]])','');
          write_log('l_invoice_amount =>'||l_invoice_amount);
		  l_invoice_description :=regexp_replace(get_field(XXCUS_COSTAR_AP_EXPORT.g_delimiter, 10, line_read, line_count), '([^[:print:]])','');
          write_log('l_invoice_description =>'||l_invoice_description);
          l_currency :=regexp_replace(get_field(XXCUS_COSTAR_AP_EXPORT.g_delimiter, 11, line_read, line_count), '([^[:print:]])','');
          write_log('l_currency =>'||l_currency);          
          l_line_number :=regexp_replace(get_field(XXCUS_COSTAR_AP_EXPORT.g_delimiter, 12, line_read, line_count), '([^[:print:]])','');
          write_log('l_line_number =>'||l_line_number);
          l_gl_account_name :=regexp_replace(get_field(XXCUS_COSTAR_AP_EXPORT.g_delimiter, 13, line_read, line_count), '([^[:print:]])','');
          write_log('l_gl_account_name =>'||l_gl_account_name);
		  l_gl_acct_extrnl_id :=regexp_replace(get_field(XXCUS_COSTAR_AP_EXPORT.g_delimiter, 14, line_read, line_count), '([^[:print:]])',''); 
          write_log('l_gl_acct_extrnl_id =>'||l_gl_acct_extrnl_id); 
		  l_gl_account_code :=regexp_replace(get_field(XXCUS_COSTAR_AP_EXPORT.g_delimiter, 15, line_read, line_count), '([^[:print:]])','');
          write_log('l_gl_account_code =>'||l_gl_account_code);
		  l_gl_event_name :=regexp_replace(get_field(XXCUS_COSTAR_AP_EXPORT.g_delimiter, 16, line_read, line_count), '([^[:print:]])','');
          write_log('l_gl_event_name =>'||l_gl_event_name);
		  l_line_description :=regexp_replace(get_field(XXCUS_COSTAR_AP_EXPORT.g_delimiter, 17, line_read, line_count), '([^[:print:]])','');
          write_log('l_line_description =>'||l_line_description);
		  l_line_amount :=regexp_replace(get_field(XXCUS_COSTAR_AP_EXPORT.g_delimiter, 18, line_read, line_count), '([^[:print:]])','');
          write_log('l_line_amount =>'||l_line_amount);
          l_gl_alloc_code1 :=regexp_replace(get_field(XXCUS_COSTAR_AP_EXPORT.g_delimiter, 19, line_read, line_count), '([^[:print:]])','');
          write_log('l_gl_alloc_code1 =>'||l_gl_alloc_code1);
          l_gl_alloc_code2 :=regexp_replace(get_field(XXCUS_COSTAR_AP_EXPORT.g_delimiter, 20, line_read, line_count), '([^[:print:]])','');
          write_log('l_gl_alloc_code2 =>'||l_gl_alloc_code2);
          l_gl_alloc_code3 :=regexp_replace(get_field(XXCUS_COSTAR_AP_EXPORT.g_delimiter, 21, line_read, line_count), '([^[:print:]])','');
          write_log('l_gl_alloc_code3 =>'||l_gl_alloc_code3);
          l_gl_alloc_code4 :=regexp_replace(get_field(XXCUS_COSTAR_AP_EXPORT.g_delimiter, 22, line_read, line_count), '([^[:print:]])','');
          write_log('l_gl_alloc_code4 =>'||l_gl_alloc_code4);
          
		  b_move_fwd :=TRUE;

        EXCEPTION
        WHEN others THEN

          write_log ('Error in get fields, line_count ='||line_count||', msg  ='||sqlerrm);
          b_move_fwd :=FALSE;

        END;

        IF (b_move_fwd) THEN
        
          BEGIN
        
            INSERT INTO XXCUS.XXCUS_COSTAR_AP_EXPORT_INTF
                (RUN_ID
                ,GROUP_ID
                ,FILE_NAME
                ,FISCAL_PERIOD
                ,RECORD_TYPE         
                ,INVOICE_NUMBER      
                ,INVOICE_DATE        
                ,LOCATION_ID         
                ,LOCATION_NAME       
                ,VENDOR_ID           
                ,ADDRESS_ID          
                ,TERMS               
                ,INVOICE_DESCRIPTION 
                ,CURRENCY            
                ,INVOICE_AMOUNT      
                ,LINE_NUMBER         
                ,LINE_DESCRIPTION    
                ,LINE_AMOUNT      
                ,GL_EXTERNAL_ID						
                ,GL_ALLOC_CODE1      
                ,GL_ALLOC_CODE2      
                ,GL_ALLOC_CODE3      
                ,GL_ALLOC_CODE4
				,GL_ACCOUNT_NAME
				,GL_ACCOUNT_CODE
				,GL_EVENT_NAME				
                ,CREATED_BY          
                ,CREATION_DATE       
                ,LAST_UPDATED_BY     
                ,LAST_UPDATE_DATE    
                ,REQUEST_ID          
                ,STATUS              
                ,ORG_ID
                )
                VALUES
                (g_run_id
                ,g_group_id
                ,g_file_name
                ,g_fiscal_period
                ,l_record_type         
                ,l_invoice_number      
                ,l_invoice_date        
                ,l_location_id         
                ,l_location_name       
                ,l_vendor_id           
                ,l_address_id          
                ,l_terms               
                ,l_invoice_description 
                ,l_currency            
                ,l_invoice_amount      
                ,l_line_number         
                ,NVL(l_line_description,'NA')   
                ,NVL(l_line_amount,0)    
                ,l_gl_acct_extrnl_id					
                ,l_gl_alloc_code1      
                ,l_gl_alloc_code2      
                ,l_gl_alloc_code3      
                ,l_gl_alloc_code4
				,l_gl_account_name
				,l_gl_account_code
				,l_gl_event_name
                ,ln_user_id
                ,SYSDATE
                ,ln_user_id
                ,SYSDATE
                ,ln_request_id
                ,g_status1
                ,g_org_id);
            
          EXCEPTION
          WHEN others THEN

            write_log ('Error in Record Insertion ='||line_count||', msg  ='||sqlerrm);

          END;

        END IF;

        line_count:= line_count+1;

      END IF;

    EXCEPTION
    WHEN no_data_found THEN

      EXIT;
                 
    WHEN others THEN

      raise_application_error(-20010,'Line: '||line_read);
      raise_application_error(-20010,' Unknown Errors: '||sqlerrm);

      EXIT;

    END;
  
  END LOOP;  
  
  COMMIT;
  
  utl_file.fclose(input_file_id);
  write_log ('After file close.');

EXCEPTION
WHEN utl_file.invalid_path THEN

  write_log('File: '||g_file_name||' Invalid Path: '||sqlerrm);
  raise_application_error(-20010,'File: '||g_file_name||' Invalid Path: '||sqlerrm);
   
WHEN utl_file.invalid_mode THEN

  write_log('File: '||g_file_name||' Invalid Mode: '||sqlerrm);
  raise_application_error(-20010,'File: '||g_file_name||' Invalid Mode: '||sqlerrm);
   
WHEN utl_file.invalid_operation THEN

  write_log('File: '||g_file_name||' Invalid Operation: '||sqlerrm);
  raise_application_error(-20010,'File: '||g_file_name||' Invalid Operation: '||sqlerrm);
   
WHEN utl_file.invalid_filehandle THEN

  write_log('File: '||g_file_name||' nvalid File Handle: '||sqlerrm);
  raise_application_error(-20010,'File: '||g_file_name||' Invalid File Handle: '||sqlerrm);
   
WHEN utl_file.read_error THEN

  write_log('File: '||g_file_name||' File Read Error: '||sqlerrm);
  raise_application_error(-20010,'File: '||g_file_name||' File Read Error: '||sqlerrm);
   
WHEN utl_file.internal_error THEN

  write_log('File: '||g_file_name||' File Internal Error: '||sqlerrm);
  raise_application_error(-20010,'File: '||g_file_name||' File Internal Error: '||sqlerrm);

WHEN others THEN

  write_log('Issue in '||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);
  write_error('Issue in '||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);
  raise program_error;
  raise;

END LOAD_INVOICES_DATA;

/*************************************************************************
  FUNCTION : get_field

  PURPOSE:   This function is used to extract the data from utl_file.
  Parameter: IN  v_delimiter    
             IN  n_field_no
             IN  v_line_read
             IN  p_which_line
  REVISIONS:
  Ver        Date        Author             Description
  ---------  ----------  ---------------   -------------------------
  1.0        25/06/2018  Ashwin Sridhar     Initial Build - Task ID: 20180709-00059
************************************************************************/
FUNCTION get_field (v_delimiter  IN VARCHAR2
                   ,n_field_no   IN NUMBER 
                   ,v_line_read  IN VARCHAR2
                   ,p_which_line IN NUMBER) RETURN VARCHAR2 IS

  l_sub_routine     VARCHAR2(30) :='get_field';
  n_start_field_pos NUMBER;
  n_end_field_pos   NUMBER;
  v_get_field       VARCHAR2(2000);

BEGIN

  IF n_field_no = 1 THEN
  
    n_start_field_pos := 1;

  ELSE
  
    n_start_field_pos := INSTR(v_line_read,v_delimiter,1,n_field_no-1)+1;

  END IF;
  
  write_log('n_start_field_pos '||n_start_field_pos||', n_field_no '||n_field_no);
  n_end_field_pos   := instr(v_line_read,v_delimiter,1,n_field_no) -1;
  write_log('n_end_field_pos '||n_end_field_pos||', n_field_no '||n_field_no);

  IF n_end_field_pos > 0 THEN
  
    v_get_field := substr(v_line_read,n_start_field_pos,(n_end_field_pos - n_start_field_pos)+1);
    write_log('v_get_field when >0 is '||v_get_field||', n_field_no '||n_field_no);

  ELSE

    v_get_field := substr(v_line_read,n_start_field_pos);
    write_log('v_get_field when 0 is '||v_get_field||', n_field_no '||n_field_no);

  END IF;

  RETURN LTRIM(RTRIM(v_get_field));

EXCEPTION
WHEN others THEN
  
   write_log('Issue in '||g_pkg_name||'.'||l_sub_routine||', message =>'||SQLERRM);
   write_log ('Line# ='||p_which_line);
   write_log ('Line ='||v_line_read);
   write_log ('get field: '||SQLERRM);

END get_field;

/*************************************************************************
  Procedure : LOAD_AUDIT_DATA

  PURPOSE:   This procedure is used to load the audit data.
  Parameter: IN  p_run_id    
             IN  p_group_id
             IN  p_stage
  REVISIONS:
  Ver        Date        Author             Description
  ---------  ----------  ---------------   -------------------------
  1.0        25/06/2018  Ashwin Sridhar     Initial Build - Task ID: 20180709-00059
************************************************************************/
PROCEDURE LOAD_AUDIT_DATA(p_run_id   IN NUMBER
                         ,p_group_id IN VARCHAR2
                         ,p_stage    IN VARCHAR2) IS

--Invoice Staging Header Records...
CURSOR cu_stg_header IS
SELECT COUNT(INVOICE_NUMBER)   inv_count
,      'Total Invoices Header' str_dtls
FROM   XXCUS.XXCUS_COSTAR_AP_EXPORT_INTF
WHERE  RECORD_TYPE='HEADER'
AND    run_id=p_run_id
UNION ALL
SELECT SUM(INVOICE_AMOUNT)   inv_count
,     'Total Amount Header'  str_dtls
FROM   XXCUS.XXCUS_COSTAR_AP_EXPORT_INTF
WHERE  RECORD_TYPE='HEADER'
AND    run_id=p_run_id;

--Invoice Staging Line Records...
CURSOR cu_stg_line IS
SELECT COUNT(1) INV_COUNT
,      'Total Invoice Lines' str_dtls
FROM  XXCUS.XXCUS_COSTAR_AP_EXPORT_INTF
WHERE RECORD_TYPE='DETAIL'
AND   run_id   =p_run_id
UNION ALL
SELECT SUM(LINE_AMOUNT) INV_COUNT
,     'Total Line Amount'  str_dtls
FROM  XXCUS.XXCUS_COSTAR_AP_EXPORT_INTF
WHERE RECORD_TYPE='DETAIL'
AND   run_id   =p_run_id;

--Invoice Interface Header Records...
CURSOR cu_iface_header IS
SELECT COUNT(INVOICE_ID)      inv_count
,     'Total Invoices Header' str_dtls
FROM  AP_INVOICES_INTERFACE
WHERE GROUP_ID=p_group_id
UNION ALL
SELECT SUM(INVOICE_AMOUNT)    inv_count
,      'Total Amount Header'  str_dtls
FROM   AP_INVOICES_INTERFACE
WHERE  GROUP_ID=p_group_id;

--Invoice Interface Line Records...
CURSOR cu_iface_line IS
SELECT COUNT(INVOICE_LINE_ID)  INV_COUNT
,      'Total Invoice Lines'   str_dtls
FROM  AP_INVOICE_LINES_INTERFACE Ali
,     AP_INVOICES_INTERFACE      AI
WHERE AI.INVOICE_ID=ALI.INVOICE_ID
AND   GROUP_ID=p_group_id
UNION ALL
SELECT SUM(AMOUNT)              INV_COUNT
,     'Total Line Amount'       str_dtls
FROM  AP_INVOICE_LINES_INTERFACE Ali
,     AP_INVOICES_INTERFACE      AI
WHERE AI.INVOICE_ID=ALI.INVOICE_ID
AND   GROUP_ID=p_group_id;

--Processed Invoices...
CURSOR cu_inv_pro IS
SELECT COUNT(API.INVOICE_ID) INV_COUNT
,      'Total Invoices Processed' STR_DTLS
FROM  AP_INVOICES_ALL API
,     AP_BATCHES_ALL  APA
WHERE 1=1
AND   API.BATCH_ID=APA.BATCH_ID
AND   APA.BATCH_NAME=p_group_id
UNION ALL
SELECT SUM(API.INVOICE_AMOUNT) INV_COUNT
,     'Total Processed Invoice Amount'  str_dtls
FROM  AP_INVOICES_ALL API
,     AP_BATCHES_ALL  APA
WHERE 1=1
AND   API.BATCH_ID=APA.BATCH_ID
AND   APA.BATCH_NAME=p_group_id;

--Rejected Invoices...
CURSOR cu_inv_rej IS
SELECT COUNT(INVOICE_ID) inv_count
,     'Total Invoices Rejected'  str_dtls
FROM   AP_INVOICES_INTERFACE
WHERE  group_id=p_group_id
AND    status='REJECTED'
UNION ALL
SELECT NVL(SUM(INVOICE_AMOUNT),0) inv_count
,     'Total Rejected Invoice Amount'  str_dtls
FROM   AP_INVOICES_INTERFACE
WHERE  group_id=p_group_id
AND    status='REJECTED';

ln_seq        NUMBER:=0;
l_sub_routine VARCHAR2(50):='LOAD_AUDIT_DATA';
ln_max_seq    NUMBER;
lv_file_name  VARCHAR2(100);

BEGIN

  IF p_stage='STAGING' THEN
  
    write_log ('Loading the Staging Summary...');
  
    --Inserting Header Records...
    FOR rec_stg_header IN cu_stg_header LOOP
  
      BEGIN

        ln_seq:=ln_seq+1;

        INSERT INTO XXCUS.XXCUS_COSTAR_AP_EXPORT_AUDIT(RUN_ID               
                                                      ,GROUP_ID             
                                                      ,FILE_NAME            
                                                      ,RECORD_TYPE          
                                                      ,SEQ                  
                                                      ,LINE_ITEM            
                                                      ,LINE_VALUE           
                                                      ,CREATED_BY           
                                                      ,CREATION_DATE        
                                                      ,LAST_UPDATED_BY      
                                                      ,LAST_UPDATE_DATE     
                                                      ,REQUEST_ID           
                                                      ,STATUS               
                                                    )
                                                    VALUES
                                                    (p_run_id
                                                    ,p_group_id
                                                    ,g_file_name
                                                    ,'HEADER STAGING'
                                                    ,ln_seq
                                                    ,rec_stg_header.str_dtls 
                                                    ,rec_stg_header.inv_count
                                                    ,g_user_id
                                                    ,SYSDATE
                                                    ,g_user_id
                                                    ,SYSDATE
                                                    ,g_request_id
                                                    ,'FILE IMPORT');
      EXCEPTION
      WHEN others THEN

        write_log ('Error in Record Insertion for Header Staging Summary='||ln_seq||sqlerrm);

      END;      
    
    END LOOP;

    FOR rec_stg_line IN cu_stg_line LOOP
    
      BEGIN

        ln_seq:=ln_seq+1;
  
        INSERT INTO XXCUS.XXCUS_COSTAR_AP_EXPORT_AUDIT(RUN_ID               
                                                      ,GROUP_ID             
                                                      ,FILE_NAME            
                                                      ,RECORD_TYPE          
                                                      ,SEQ                  
                                                      ,LINE_ITEM            
                                                      ,LINE_VALUE           
                                                      ,CREATED_BY           
                                                      ,CREATION_DATE        
                                                      ,LAST_UPDATED_BY      
                                                      ,LAST_UPDATE_DATE     
                                                      ,REQUEST_ID           
                                                      ,STATUS               
                                                    )
                                                    VALUES
                                                    (p_run_id
                                                    ,p_group_id
                                                    ,g_file_name
                                                    ,'LINE STAGING'
                                                    ,ln_seq
                                                    ,rec_stg_line.str_dtls
                                                    ,rec_stg_line.inv_count 
                                                    ,g_user_id
                                                    ,SYSDATE
                                                    ,g_user_id
                                                    ,SYSDATE
                                                    ,g_request_id
                                                    ,'FILE IMPORT');

      EXCEPTION
      WHEN others THEN

        write_log ('Error in Record Insertion for Line Staging Summary='||ln_seq||sqlerrm);

      END;

    END LOOP;

   g_seq:=ln_seq;

  ELSIF p_stage='INTERFACE' THEN

    write_log('Loading the Interface Summary...');
  
    --Inserting Header Records...
    FOR rec_iface_header IN cu_iface_header LOOP
  
      BEGIN

        g_seq:=g_seq+1;

        INSERT INTO XXCUS.XXCUS_COSTAR_AP_EXPORT_AUDIT(RUN_ID               
                                                      ,GROUP_ID             
                                                      ,FILE_NAME            
                                                      ,RECORD_TYPE          
                                                      ,SEQ                  
                                                      ,LINE_ITEM            
                                                      ,LINE_VALUE           
                                                      ,CREATED_BY           
                                                      ,CREATION_DATE        
                                                      ,LAST_UPDATED_BY      
                                                      ,LAST_UPDATE_DATE     
                                                      ,REQUEST_ID           
                                                      ,STATUS               
                                                      )
                                                      VALUES
                                                      (p_run_id
                                                      ,p_group_id
                                                      ,g_file_name
                                                      ,'HEADER INTERFACE'
                                                      ,g_seq
                                                      ,rec_iface_header.str_dtls 
                                                      ,rec_iface_header.inv_count
                                                      ,g_user_id
                                                      ,SYSDATE
                                                      ,g_user_id
                                                      ,SYSDATE
                                                      ,g_request_id
                                                      ,'STAGED');
      EXCEPTION
      WHEN others THEN

        write_log ('Error in Record Insertion for Header Interface Summary='||g_seq||sqlerrm);

      END;      
    
    END LOOP;

    --Inserting Line Records...
    FOR rec_iface_line IN cu_iface_line LOOP
    
      BEGIN

        g_seq:=g_seq+1;
  
        INSERT INTO XXCUS.XXCUS_COSTAR_AP_EXPORT_AUDIT(RUN_ID               
                                                      ,GROUP_ID             
                                                      ,FILE_NAME            
                                                      ,RECORD_TYPE          
                                                      ,SEQ                  
                                                      ,LINE_ITEM            
                                                      ,LINE_VALUE           
                                                      ,CREATED_BY           
                                                      ,CREATION_DATE        
                                                      ,LAST_UPDATED_BY      
                                                      ,LAST_UPDATE_DATE     
                                                      ,REQUEST_ID           
                                                      ,STATUS               
                                                      )
                                                      VALUES
                                                      (p_run_id
                                                      ,p_group_id
                                                      ,g_file_name
                                                      ,'LINE INTERFACE'
                                                      ,g_seq
                                                      ,rec_iface_line.str_dtls 
                                                      ,rec_iface_line.inv_count
                                                      ,g_user_id
                                                      ,SYSDATE
                                                      ,g_user_id
                                                      ,SYSDATE
                                                      ,g_request_id
                                                      ,'STAGED');

      EXCEPTION
      WHEN others THEN

        write_log ('Error in Record Insertion for Line Interface Summary='||g_seq||sqlerrm);

      END;

    END LOOP;

  ELSIF p_stage='PROCESSED' THEN

    write_log ('Loading the Processed Summary...');

    BEGIN

      SELECT MAX(seq)
      ,      MAX(FILE_NAME)
      INTO   ln_max_seq
      ,      lv_file_name
      FROM   XXCUS.XXCUS_COSTAR_AP_EXPORT_AUDIT
      WHERE  group_id=p_group_id;

    EXCEPTION
    WHEN others THEN
    
      ln_max_seq:=0;
    
    END;
  
    g_seq:=ln_max_seq;
  
    --Inserting Header Processed Records...
    FOR rec_inv_pro IN cu_inv_pro LOOP
  
      BEGIN

        g_seq:=g_seq+1;

        INSERT INTO XXCUS.XXCUS_COSTAR_AP_EXPORT_AUDIT(RUN_ID               
                                                    ,GROUP_ID             
                                                    ,FILE_NAME            
                                                    ,RECORD_TYPE          
                                                    ,SEQ                  
                                                    ,LINE_ITEM            
                                                    ,LINE_VALUE           
                                                    ,CREATED_BY           
                                                    ,CREATION_DATE        
                                                    ,LAST_UPDATED_BY      
                                                    ,LAST_UPDATE_DATE     
                                                    ,REQUEST_ID           
                                                    ,STATUS               
                                                    )
                                                    VALUES
                                                    (p_run_id
                                                    ,p_group_id
                                                    ,lv_file_name
                                                    ,'PROCESSED INVOICES'
                                                    ,g_seq
                                                    ,rec_inv_pro.str_dtls 
                                                    ,NVL(rec_inv_pro.inv_count,0)
                                                    ,g_user_id
                                                    ,SYSDATE
                                                    ,g_user_id
                                                    ,SYSDATE
                                                    ,g_request_id
                                                    ,'PROCESSED');
      EXCEPTION
      WHEN others THEN

        write_log ('Error in Record Insertion for processed Summary='||g_seq||sqlerrm);

      END;      
    
    END LOOP;

  ELSIF p_stage='REJECTED' THEN

    write_log ('Loading the Rejected Summary...');

    BEGIN

      SELECT MAX(seq)
      ,      MAX(FILE_NAME)
      INTO   ln_max_seq
      ,      lv_file_name
      FROM   XXCUS.XXCUS_COSTAR_AP_EXPORT_AUDIT
      WHERE  group_id=p_group_id;

    EXCEPTION
    WHEN others THEN
    
      ln_max_seq:=0;
    
    END;
  
    g_seq:=ln_max_seq;
  
    --Inserting Header Rejected Records...
    FOR rec_inv_rej IN cu_inv_rej LOOP
  
      BEGIN

        g_seq:=g_seq+1;

        INSERT INTO XXCUS.XXCUS_COSTAR_AP_EXPORT_AUDIT(RUN_ID               
                                                      ,GROUP_ID             
                                                      ,FILE_NAME            
                                                      ,RECORD_TYPE          
                                                      ,SEQ                  
                                                      ,LINE_ITEM            
                                                      ,LINE_VALUE           
                                                      ,CREATED_BY           
                                                      ,CREATION_DATE        
                                                      ,LAST_UPDATED_BY      
                                                      ,LAST_UPDATE_DATE     
                                                      ,REQUEST_ID           
                                                      ,STATUS               
                                                      )
                                                      VALUES
                                                      (p_run_id
                                                      ,p_group_id
                                                      ,lv_file_name
                                                      ,'REJECTED INVOICES'
                                                      ,g_seq
                                                      ,rec_inv_rej.str_dtls
                                                      ,rec_inv_rej.inv_count
                                                      ,g_user_id
                                                      ,SYSDATE
                                                      ,g_user_id
                                                      ,SYSDATE
                                                      ,g_request_id
                                                      ,'REJECTED');
      EXCEPTION
      WHEN others THEN

        write_log ('Error in Record Insertion for rejection Summary='||g_seq||sqlerrm);

      END;      
    
    END LOOP;

  END IF;

  COMMIT;

EXCEPTION
WHEN others THEN

  write_log('Issue in '||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);
  write_error('Issue in '||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);
  raise program_error;
  raise;
  
END LOAD_AUDIT_DATA;

/*************************************************************************
  Procedure : GENERATE_AUDIT_REPORT

  PURPOSE:   This procedure is used to generate the audit report.
  Parameter: IN  p_run_id    
             IN  p_group_id
  REVISIONS:
  Ver        Date        Author             Description
  ---------  ----------  ---------------   -------------------------
  1.0        25/06/2018  Ashwin Sridhar     Initial Build - Task ID: 20180709-00059
************************************************************************/
PROCEDURE GENERATE_AUDIT_REPORT(p_errbuf   OUT VARCHAR2
                               ,p_retcode  OUT VARCHAR2
                               ,p_run_id   IN  NUMBER
                               ,p_group_id IN  VARCHAR2
                               ) IS

l_sub_routine VARCHAR2(50);

BEGIN

  write_log ('Calling load_audit_data for processed Summary');
  LOAD_AUDIT_DATA(p_run_id   
                 ,p_group_id 
                 ,'PROCESSED'    
                  );

  write_log ('Calling load_audit_data for rejected Summary');
  LOAD_AUDIT_DATA(p_run_id   
                 ,p_group_id 
                 ,'REJECTED'    
                  );

EXCEPTION
WHEN others THEN

  write_log('Issue in '||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);
  p_retcode:=1;

END GENERATE_AUDIT_REPORT;

FUNCTION afterReport RETURN BOOLEAN AS

l_sub_routine      VARCHAR2(30) :='afterReport';
g_notif_email_from VARCHAR2(240):='OracleEBS.Costar.Extracts@hdsupply.com';
l_email_subject    VARCHAR2(240):='Costar CORP AP Invoices Audit Report';
g_notif_email_cc   VARCHAR2(240) :='Balaguru.Seshadri@hdsupply.com';--'HDS.OracleAPSupport@hdsupply.com';
l_non_prod_email   VARCHAR2(240):='vamshi.singirikonda@hdsupply.com';
ln_request_id      NUMBER:=fnd_global.conc_request_id;
l_file_name        VARCHAR2(250):='XXCUS_COSTAR_AP_AUDIT_REPORT_'||ln_request_id||'_1.PDF';
l_req_id           NUMBER;
l_new_file         VARCHAR2(250):='HDS_COSTAR_CORP_AP_INV_AUDIT_REPORT.PDF';
lv_arg1            VARCHAR2(100);
lv_arg2            VARCHAR2(100);

BEGIN
  
  write_log('Parent Request ID '||ln_request_id);
  
  BEGIN
  
    SELECT ARGUMENT1
    ,      ARGUMENT2
    INTO   lv_arg1
    ,      lv_arg2
    FROM apps.FND_CONCURRENT_REQUESTS
    WHERE request_id=ln_request_id;
  
  EXCEPTION
  WHEN others THEN
  
    lv_arg1:=NULL;
    lv_arg2:=NULL;
  
  END;
  
  write_log('Parameter 1 '||lv_arg1);
  write_log('Parameter 2 '||lv_arg2);
  
  l_req_id := fnd_request.submit_request
                 (
                   'XXCUS'
                  ,'XXCUS_COSTAR_EMAIL_RPT'
                  ,NULL
                  ,NULL
                  ,FALSE
                  ,l_file_name --absolute file name. XXCUS_CONCUR_AUDIT_18900888_1.PDF
                  ,l_non_prod_email -- non production recipient email id
                  ,g_notif_email_from --sender email id
                  ,l_email_subject --email subject
                  ,l_new_file --new file name that goes to share point folder Concur Audit Reports
                  ,g_notif_email_cc
                 );
 
  COMMIT;

  IF l_req_id>0 THEN
  
    write_log('Request Submitted successfully with request ID '||l_req_id);

    UPDATE XXCUS.XXCUS_COSTAR_AP_EXPORT_AUDIT
    SET    email_flag='Email Sent'
    WHERE  run_id    =lv_arg1
    AND    group_id  =lv_arg2;

    COMMIT;

  ELSE
  
    write_log('Issue in submitting the request ');
  
  END IF;
  
  RETURN TRUE;
  
EXCEPTION
WHEN others THEN

  write_log('Issue in '||g_pkg_name||'.'||l_sub_routine||' msg ='||sqlerrm);

  RETURN FALSE;

END afterReport;

FUNCTION beforereport RETURN BOOLEAN AS

ln_request_id NUMBER :=0;
l_sub_routine VARCHAR2(30) :='beforereport';
p_run_id      NUMBER;
p_group_id    VARCHAR2(240);

BEGIN
  
  NULL;  
  return true;
  
EXCEPTION
WHEN others THEN

  write_log('Issue in '||g_pkg_name||'.'||l_sub_routine||' msg ='||sqlerrm);

  RETURN FALSE;

END beforereport;

/*************************************************************************
  Procedure : uc4_process_invoices

  PURPOSE:   This procedure is a UC4 wrapper to submit concurrent Program 
             XXCUS CoStar AP Invoices Import           
  Parameter: OUT  p_errbuf 
             OUT  p_retcode
             IN   p_data_file
             IN   p_run_id
             IN   p_group_id
             IN   p_user_name
             IN   p_responsibility_name			 
  REVISIONS:
  Ver        Date        Author                  Description
  ---------  ----------  ---------------         -------------------------
  1.0        08/16/2018  Vamshi Singirikonda     Initial Build - Task ID: 20180709-00059
************************************************************************/
PROCEDURE uc4_process_invoices(p_errbuf               OUT VARCHAR2
							   ,p_retcode             OUT NUMBER
							   ,p_data_file           IN VARCHAR2
							   ,p_run_id              IN VARCHAR2
							   ,p_group_id            IN VARCHAR2
							   ,p_user_name           IN VARCHAR2
							   ,p_responsibility_name IN VARCHAR2) IS
			--
    l_package     VARCHAR2(50) := 'XXCUS_COSTAR_AP_EXPORT.uc4_process_invoices';
    l_distro_list VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';

    l_req_id     NUMBER NULL;
    l_phase      VARCHAR2(50);
    l_status     VARCHAR2(50);
    l_dev_status VARCHAR2(50);
    l_dev_phase  VARCHAR2(50);
    l_supplier_id NUMBER;
    l_rec_cnt     NUMBER := 0;
    l_interval    NUMBER := 30; -- In seconds
    l_max_time    NUMBER := 15000; -- In seconds
    l_message     VARCHAR2(2000);
    l_can_submit_request  BOOLEAN := TRUE;    
    l_err_msg             VARCHAR2(3000);
    l_err_code            NUMBER;    
    l_statement           CLOB;    
    l_user_id             NUMBER;
    l_responsibility_id   NUMBER;
    l_resp_application_id NUMBER;
    l_program             VARCHAR2(30) := 'XXCUS_COSTAR_INVOICE_IMPORT'; -- Program to process recon
    l_application         VARCHAR2(30) := 'XXCUS';

  BEGIN

    p_retcode := 0;
    -- Deriving User Id
    BEGIN
      SELECT user_id
        INTO l_user_id
        FROM applsys.fnd_user
       WHERE user_name = upper(p_user_name)
         AND SYSDATE BETWEEN start_date AND
             nvl(end_date, trunc(SYSDATE) + 1);
    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := 'UserName - ' || p_user_name ||
                     ' not defined in Oracle';
        RAISE program_error;
      WHEN OTHERS THEN
        l_err_msg := 'Error deriving user_id for UserName - ' ||
                     p_user_name;
        RAISE program_error;
    END;

    -- Deriving Responsibility Id and Responsibility Application Id
    BEGIN
      SELECT responsibility_id, application_id
        INTO l_responsibility_id, l_resp_application_id
        FROM apps.fnd_responsibility_vl
       WHERE responsibility_name = p_responsibility_name
         AND SYSDATE BETWEEN start_date AND
             nvl(end_date, trunc(SYSDATE) + 1);
    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := 'Responsibility - ' || p_responsibility_name ||
                     ' not defined in Oracle';
        RAISE program_error;
      WHEN OTHERS THEN
        l_err_msg := 'Error deriving Responsibility_id for ResponsibilityName - ' ||
                     p_responsibility_name;
        RAISE program_error;
    END;

    --------------------------------------------------------------------------
    -- Apps Initialize
    --------------------------------------------------------------------------
    fnd_global.apps_initialize(l_user_id
                              ,l_responsibility_id
                              ,l_resp_application_id);

    l_req_id := fnd_request.submit_request(application => l_application
                                          ,program     => l_program
                                          ,description => NULL
                                          ,start_time  => SYSDATE
                                          ,sub_request => FALSE
                                          ,argument1   => p_data_file
                                          ,argument2   => p_run_id
                                          ,argument3   => p_group_id);
    COMMIT;

    IF (l_req_id != 0)
    THEN
      IF fnd_concurrent.wait_for_request(l_req_id
                                        ,l_interval
                                        ,l_max_time
                                        ,l_phase
                                        ,l_status
                                        ,l_dev_phase
                                        ,l_dev_status
                                        ,l_message)
      THEN
        l_err_msg := chr(10) || 'ReqID=' || l_req_id || ' DPhase ' ||
                     l_dev_phase || ' DStatus ' || l_dev_status || chr(10) ||
                     ' MSG - ' || l_message;
        -- Error Returned
        IF l_dev_phase != 'COMPLETE' OR l_dev_status != 'NORMAL'
        THEN
          l_statement := 'An error occured in the running of XXCUS CoStar AP Invoices Import' ||
                         l_err_msg || '.';
          fnd_file.put_line(fnd_file.log, l_statement);
          fnd_file.put_line(fnd_file.output, l_statement);
          RAISE program_error;
        END IF;
      END IF;

    ELSE
      l_statement := 'An error occured when trying to submit XXCUS CoStar AP Invoices Import';
      RAISE program_error;
    END IF;

    dbms_output.put_line(l_statement);
    dbms_output.put_line('Request ID:  ' || l_req_id);
    dbms_output.put_line('Responsibility Name:  ' || p_responsibility_name);
    dbms_output.put_line('User Name:  ' || p_user_name);
    dbms_output.put_line('File Name:  ' || p_data_file);
	dbms_output.put_line('Run ID:  ' || p_run_id);
	dbms_output.put_line('Group ID:  ' || p_group_id);

    --For UC4 success notification
    dbms_output.put_line('Success');

  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;
      l_err_msg := l_err_msg || '-' || l_statement || ' ...Error_Stack...' ||
                   dbms_utility.format_error_stack() ||
                   ' Error_Backtrace...' ||
                   dbms_utility.format_error_backtrace();
      dbms_output.put_line('Request ID:  ' || l_req_id);
      dbms_output.put_line('Concurrent short name :  ' || l_program);
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_package
                                          ,p_calling           => l_err_msg
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'AP');
      p_retcode := 2;
      p_errbuf  := l_err_msg;

    WHEN OTHERS THEN
      l_err_msg := l_err_msg || '-' || l_statement|| ' ...Error_Stack...' ||
                   dbms_utility.format_error_stack() ||
                   ' Error_Backtrace...' ||
                   dbms_utility.format_error_backtrace();
      dbms_output.put_line('Request ID:  ' || l_req_id);
      dbms_output.put_line('Concurrent short name :  ' || l_program);
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_package
                                          ,p_calling           => l_err_msg
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'AP');
      p_retcode := 2;
      p_errbuf  := l_err_msg;

  END uc4_process_invoices;
  /*************************************************************************
  Procedure : uc4_Payables_interface

  PURPOSE:  This procedure is a UC4 wrapper to submit concurrent Program 
            Payables Open Interface Import 
  Parameter: OUT  p_errbuf 
             OUT  p_retcode
             IN   p_group_id
             IN   p_user_name
             IN   p_responsibility_name			 
  REVISIONS:
  Ver        Date        Author                  Description
  ---------  ----------  ---------------         -------------------------
  1.0        08/16/2018  Vamshi Singirikonda     Initial Build - Task ID: 20180709-00059
************************************************************************/
  PROCEDURE uc4_Payables_interface(p_errbuf           OUT VARCHAR2
							   ,p_retcode             OUT NUMBER							   
							   ,p_group_id            IN VARCHAR2
							   ,p_user_name           IN VARCHAR2
							   ,p_responsibility_name IN VARCHAR2) IS
			--
    l_package     VARCHAR2(50) := 'XXCUS_COSTAR_AP_EXPORT.uc4_Payables_interface';
    l_distro_list VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';

    l_req_id     NUMBER NULL;
    l_phase      VARCHAR2(50);
    l_status     VARCHAR2(50);
    l_dev_status VARCHAR2(50);
    l_dev_phase  VARCHAR2(50);
    l_supplier_id NUMBER;
    l_rec_cnt     NUMBER := 0;
    l_interval    NUMBER := 30; -- In seconds
    l_max_time    NUMBER := 15000; -- In seconds
    l_message     VARCHAR2(2000);
    l_can_submit_request  BOOLEAN := TRUE;    
    l_err_msg             VARCHAR2(3000);
    l_err_code            NUMBER;    
    l_statement           CLOB;    
    l_user_id             NUMBER;
    l_responsibility_id   NUMBER;
    l_resp_application_id NUMBER;
    l_program             VARCHAR2(30) := 'APXIIMPT'; -- Program to process recon
    l_application         VARCHAR2(30) := 'SQLAP';	
    l_login_id            NUMBER := APPS.FND_PROFILE.VALUE('login_id');
  BEGIN

    p_retcode := 0;
    -- Deriving User Id
    BEGIN
      SELECT user_id
        INTO l_user_id
        FROM applsys.fnd_user
       WHERE user_name = upper(p_user_name)
         AND SYSDATE BETWEEN start_date AND
             nvl(end_date, trunc(SYSDATE) + 1);
    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := 'UserName - ' || p_user_name ||
                     ' not defined in Oracle';
        RAISE program_error;
      WHEN OTHERS THEN
        l_err_msg := 'Error deriving user_id for UserName - ' ||
                     p_user_name;
        RAISE program_error;
    END;

    -- Deriving Responsibility Id and Responsibility Application Id
    BEGIN
      SELECT responsibility_id, application_id
        INTO l_responsibility_id, l_resp_application_id
        FROM apps.fnd_responsibility_vl
       WHERE responsibility_name = p_responsibility_name
         AND SYSDATE BETWEEN start_date AND
             nvl(end_date, trunc(SYSDATE) + 1);
    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := 'Responsibility - ' || p_responsibility_name ||
                     ' not defined in Oracle';
        RAISE program_error;
      WHEN OTHERS THEN
        l_err_msg := 'Error deriving Responsibility_id for ResponsibilityName - ' ||
                     p_responsibility_name;
        RAISE program_error;
    END;

    --------------------------------------------------------------------------
    -- Apps Initialize
    --------------------------------------------------------------------------
    fnd_global.apps_initialize(l_user_id
                              ,l_responsibility_id
                              ,l_resp_application_id);

    l_req_id := fnd_request.submit_request(application => l_application
                                          ,program     => l_program
                                          ,description => NULL
                                          ,start_time  => SYSDATE
                                          ,sub_request => FALSE
                                          ,argument1   => g_org_id   -- Operating Unit
                                          ,argument2   => g_invoice_source -- Source
                                          ,argument3   => p_group_id -- Group
										  ,argument4   => p_group_id -- Batch Name
										  ,argument5   => '' --Hold Name
										  ,argument6   => '' -- Hold Reason
										  ,argument7   => '' -- GL Date
										  ,argument8   => 'Y' -- Purge
										  ,argument9   => 'N' -- Trace Switch
										  ,argument10  => 'N' -- Debug Switch
										  ,argument11  => 'N' -- Summarize Report
										  ,argument12  => '1000' -- Commit Batch Size										  
										  ,argument13  => l_user_id -- User_id
										  ,argument14  => l_login_id -- Login_id
										  );
    COMMIT;

    IF (l_req_id != 0)
    THEN
      IF fnd_concurrent.wait_for_request(l_req_id
                                        ,l_interval
                                        ,l_max_time
                                        ,l_phase
                                        ,l_status
                                        ,l_dev_phase
                                        ,l_dev_status
                                        ,l_message)
      THEN
        l_err_msg := chr(10) || 'ReqID=' || l_req_id || ' DPhase ' ||
                     l_dev_phase || ' DStatus ' || l_dev_status || chr(10) ||
                     ' MSG - ' || l_message;
        -- Error Returned
        IF l_dev_phase != 'COMPLETE' OR l_dev_status != 'NORMAL'
        THEN
          l_statement := 'An error occured in the running of Payables Open Interface Import' ||
                         l_err_msg || '.';
          fnd_file.put_line(fnd_file.log, l_statement);
          fnd_file.put_line(fnd_file.output, l_statement);
          RAISE program_error;
        END IF;
      END IF;

    ELSE
      l_statement := 'An error occured when trying to submit Payables Open Interface Import';
      RAISE program_error;
    END IF;

    dbms_output.put_line(l_statement);
    dbms_output.put_line('Request ID:  ' || l_req_id);
    dbms_output.put_line('Responsibility Name:  ' || p_responsibility_name);
    dbms_output.put_line('User Name:  ' || p_user_name);    
	dbms_output.put_line('Group ID:  ' || p_group_id);

    --For UC4 success notification
    dbms_output.put_line('Success');

  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;
      l_err_msg := l_err_msg || '-' || l_statement || ' ...Error_Stack...' ||
                   dbms_utility.format_error_stack() ||
                   ' Error_Backtrace...' ||
                   dbms_utility.format_error_backtrace();
      dbms_output.put_line('Request ID:  ' || l_req_id);
      dbms_output.put_line('Concurrent short name :  ' || l_program);
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_package
                                          ,p_calling           => l_err_msg
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'AP');
      p_retcode := 2;
      p_errbuf  := l_err_msg;

    WHEN OTHERS THEN
      l_err_msg := l_err_msg || '-' || l_statement|| ' ...Error_Stack...' ||
                   dbms_utility.format_error_stack() ||
                   ' Error_Backtrace...' ||
                   dbms_utility.format_error_backtrace();
      dbms_output.put_line('Request ID:  ' || l_req_id);
      dbms_output.put_line('Concurrent short name :  ' || l_program);
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_package
                                          ,p_calling           => l_err_msg
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'AP');
      p_retcode := 2;
      p_errbuf  := l_err_msg;

  END uc4_Payables_interface;
    /*************************************************************************
  Procedure : uc4_invoice_Validation

  PURPOSE:  This procedure is a UC4 wrapper to submit concurrent Program 
            Invoice Validation 
  Parameter: OUT  p_errbuf 
             OUT  p_retcode
             IN   p_group_id
             IN   p_user_name
             IN   p_responsibility_name			 
  REVISIONS:
  Ver        Date        Author                  Description
  ---------  ----------  ---------------         -------------------------
  1.0        08/16/2018  Vamshi Singirikonda     Initial Build - Task ID: 20180709-00059
************************************************************************/
  PROCEDURE uc4_invoice_Validation(p_errbuf           OUT VARCHAR2
							   ,p_retcode             OUT NUMBER							   
							   ,p_group_id            IN VARCHAR2
							   ,p_user_name           IN VARCHAR2
							   ,p_responsibility_name IN VARCHAR2) IS
			--
    l_package     VARCHAR2(50) := 'XXCUS_COSTAR_AP_EXPORT.uc4_invoice_Validation';
    l_distro_list VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';

    l_req_id     NUMBER NULL;
    l_phase      VARCHAR2(50);
    l_status     VARCHAR2(50);
    l_dev_status VARCHAR2(50);
    l_dev_phase  VARCHAR2(50);
    l_supplier_id NUMBER;
    l_rec_cnt     NUMBER := 0;
    l_interval    NUMBER := 30; -- In seconds
    l_max_time    NUMBER := 15000; -- In seconds
    l_message     VARCHAR2(2000);
    l_can_submit_request  BOOLEAN := TRUE;    
    l_err_msg             VARCHAR2(3000);
    l_err_code            NUMBER;    
    l_statement           CLOB;    
    l_user_id             NUMBER;
    l_responsibility_id   NUMBER;
    l_resp_application_id NUMBER;
    l_program             VARCHAR2(30) := 'APPRVL'; -- Program to process recon
    l_application         VARCHAR2(30) := 'SQLAP';	    
	l_batch_id            NUMBER ;
  BEGIN

    p_retcode := 0;
    -- Deriving User Id
    BEGIN
      SELECT user_id
        INTO l_user_id
        FROM applsys.fnd_user
       WHERE user_name = upper(p_user_name)
         AND SYSDATE BETWEEN start_date AND
             nvl(end_date, trunc(SYSDATE) + 1);
    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := 'UserName - ' || p_user_name ||
                     ' not defined in Oracle';
        RAISE program_error;
      WHEN OTHERS THEN
        l_err_msg := 'Error deriving user_id for UserName - ' ||
                     p_user_name;
        RAISE program_error;
    END;

    -- Deriving Responsibility Id and Responsibility Application Id
    BEGIN
      SELECT responsibility_id, application_id
        INTO l_responsibility_id, l_resp_application_id
        FROM apps.fnd_responsibility_vl
       WHERE responsibility_name = p_responsibility_name
         AND SYSDATE BETWEEN start_date AND
             nvl(end_date, trunc(SYSDATE) + 1);
    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := 'Responsibility - ' || p_responsibility_name ||
                     ' not defined in Oracle';
        RAISE program_error;
      WHEN OTHERS THEN
        l_err_msg := 'Error deriving Responsibility_id for ResponsibilityName - ' ||
                     p_responsibility_name;
        RAISE program_error;
    END;
	
	


    --------------------------------------------------------------------------
    -- Apps Initialize
    --------------------------------------------------------------------------
    fnd_global.apps_initialize(l_user_id
                              ,l_responsibility_id
                              ,l_resp_application_id);
							  
     -- Dereving batch ID
	
	 BEGIN
      SELECT batch_id
        INTO l_batch_id
        FROM ap_batches_all
       WHERE batch_name  = p_group_id   ;      
    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := 'Batch - ' || p_group_id ||
                     ' not defined in Oracle';
        RAISE program_error;
      WHEN OTHERS THEN
        l_err_msg := 'Error deriving batch_id for batchname - ' ||
                     p_group_id;
        RAISE program_error;
    END;							  

    l_req_id := fnd_request.submit_request(application => l_application
                                          ,program     => l_program
                                          ,description => NULL
                                          ,start_time  => SYSDATE
                                          ,sub_request => FALSE
                                          ,argument1   => g_org_id   -- Operating Unit
                                          ,argument2   => 'All' -- Option
                                          ,argument3   => l_batch_id -- Invoice Batch Id
										  ,argument4   => '' -- Start Invoice Date
										  ,argument5   => '' --End Invoice Date
										  ,argument6   => '' -- Vendor Id
										  ,argument7   => '' -- Pay Group
										  ,argument8   => '' -- Invoice Id
										  ,argument9   => '' -- Entered By Userid
										  ,argument10  => 'N' -- Trace option
										  ,argument11  => '1000' -- Commit Size										  
										  ,argument12  => '1' -- Num of trx
										  ,argument13  => 'N' -- Debug Switch
										  );
    COMMIT;

    IF (l_req_id != 0)
    THEN
      IF fnd_concurrent.wait_for_request(l_req_id
                                        ,l_interval
                                        ,l_max_time
                                        ,l_phase
                                        ,l_status
                                        ,l_dev_phase
                                        ,l_dev_status
                                        ,l_message)
      THEN
        l_err_msg := chr(10) || 'ReqID=' || l_req_id || ' DPhase ' ||
                     l_dev_phase || ' DStatus ' || l_dev_status || chr(10) ||
                     ' MSG - ' || l_message;
        -- Error Returned
        IF l_dev_phase != 'COMPLETE' OR l_dev_status != 'NORMAL'
        THEN
          l_statement := 'An error occured in the running  Invoice Validation Program' ||
                         l_err_msg || '.';
          fnd_file.put_line(fnd_file.log, l_statement);
          fnd_file.put_line(fnd_file.output, l_statement);
          RAISE program_error;
        END IF;
      END IF;

    ELSE
      l_statement := 'An error occured when trying to submit Invoice Validation Program';
      RAISE program_error;
    END IF;

    dbms_output.put_line(l_statement);
    dbms_output.put_line('Request ID:  ' || l_req_id);
    dbms_output.put_line('Responsibility Name:  ' || p_responsibility_name);
    dbms_output.put_line('User Name:  ' || p_user_name);
	dbms_output.put_line('Group ID:  ' || p_group_id);

    --For UC4 success notification
    dbms_output.put_line('Success');

  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;
      l_err_msg := l_err_msg || '-' || l_statement || ' ...Error_Stack...' ||
                   dbms_utility.format_error_stack() ||
                   ' Error_Backtrace...' ||
                   dbms_utility.format_error_backtrace();
      dbms_output.put_line('Request ID:  ' || l_req_id);
      dbms_output.put_line('Concurrent short name :  ' || l_program);
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_package
                                          ,p_calling           => l_err_msg
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'AP');
      p_retcode := 2;
      p_errbuf  := l_err_msg;

    WHEN OTHERS THEN
      l_err_msg := l_err_msg || '-' || l_statement|| ' ...Error_Stack...' ||
                   dbms_utility.format_error_stack() ||
                   ' Error_Backtrace...' ||
                   dbms_utility.format_error_backtrace();
      dbms_output.put_line('Request ID:  ' || l_req_id);
      dbms_output.put_line('Concurrent short name :  ' || l_program);
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_package
                                          ,p_calling           => l_err_msg
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'AP');
      p_retcode := 2;
      p_errbuf  := l_err_msg;

  END uc4_invoice_Validation;
      /*************************************************************************
  Procedure : uc4_Generate_Audit

  PURPOSE:  This procedure is a UC4 wrapper to submit concurrent Program 
            XXCUS Costar Generate Audit Details
  Parameter: OUT  p_errbuf 
             OUT  p_retcode
			 IN   p_run_id
             IN   p_group_id
             IN   p_user_name
             IN   p_responsibility_name			 
  REVISIONS:
  Ver        Date        Author                  Description
  ---------  ----------  ---------------         -------------------------
  1.0        08/16/2018  Vamshi Singirikonda     Initial Build - Task ID: 20180709-00059
************************************************************************/
  PROCEDURE uc4_Generate_Audit   (p_errbuf              OUT VARCHAR2
							   ,p_retcode             OUT NUMBER	
                               ,p_run_id              IN VARCHAR2							   
							   ,p_group_id            IN VARCHAR2
							   ,p_user_name           IN VARCHAR2
							   ,p_responsibility_name IN VARCHAR2) IS
			--
    l_package     VARCHAR2(50) := 'XXCUS_COSTAR_AP_EXPORT.uc4_Generate_Audit';
    l_distro_list VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';

    l_req_id     NUMBER NULL;
    l_phase      VARCHAR2(50);
    l_status     VARCHAR2(50);
    l_dev_status VARCHAR2(50);
    l_dev_phase  VARCHAR2(50);
    l_supplier_id NUMBER;
    l_rec_cnt     NUMBER := 0;
    l_interval    NUMBER := 30; -- In seconds
    l_max_time    NUMBER := 15000; -- In seconds
    l_message     VARCHAR2(2000);
    l_can_submit_request  BOOLEAN := TRUE;
    l_err_msg             VARCHAR2(3000);
    l_err_code            NUMBER;
    l_statement           CLOB;   
    l_user_id             NUMBER;
    l_responsibility_id   NUMBER;
    l_resp_application_id NUMBER;
    l_program             VARCHAR2(30) := 'XXCUS_COSTAR_GENERATE_AUDIT'; -- Program to process recon
    l_application         VARCHAR2(30) := 'XXCUS';	    	
  BEGIN

    p_retcode := 0;
    -- Deriving User Id
    BEGIN
      SELECT user_id
        INTO l_user_id
        FROM applsys.fnd_user
       WHERE user_name = upper(p_user_name)
         AND SYSDATE BETWEEN start_date AND
             nvl(end_date, trunc(SYSDATE) + 1);
    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := 'UserName - ' || p_user_name ||
                     ' not defined in Oracle';
        RAISE program_error;
      WHEN OTHERS THEN
        l_err_msg := 'Error deriving user_id for UserName - ' ||
                     p_user_name;
        RAISE program_error;
    END;

    -- Deriving Responsibility Id and Responsibility Application Id
    BEGIN
      SELECT responsibility_id, application_id
        INTO l_responsibility_id, l_resp_application_id
        FROM apps.fnd_responsibility_vl
       WHERE responsibility_name = p_responsibility_name
         AND SYSDATE BETWEEN start_date AND
             nvl(end_date, trunc(SYSDATE) + 1);
    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := 'Responsibility - ' || p_responsibility_name ||
                     ' not defined in Oracle';
        RAISE program_error;
      WHEN OTHERS THEN
        l_err_msg := 'Error deriving Responsibility_id for ResponsibilityName - ' ||
                     p_responsibility_name;
        RAISE program_error;
    END;
	

    --------------------------------------------------------------------------
    -- Apps Initialize
    --------------------------------------------------------------------------
    fnd_global.apps_initialize(l_user_id
                              ,l_responsibility_id
                              ,l_resp_application_id);

    l_req_id := fnd_request.submit_request(application => l_application
                                          ,program     => l_program
                                          ,description => NULL
                                          ,start_time  => SYSDATE
                                          ,sub_request => FALSE
                                          ,argument1   => p_run_id   -- P_RUN_ID
                                          ,argument2   => p_group_id -- p_group_id                                          
										  );
    COMMIT;

    IF (l_req_id != 0)
    THEN
      IF fnd_concurrent.wait_for_request(l_req_id
                                        ,l_interval
                                        ,l_max_time
                                        ,l_phase
                                        ,l_status
                                        ,l_dev_phase
                                        ,l_dev_status
                                        ,l_message)
      THEN
        l_err_msg := chr(10) || 'ReqID=' || l_req_id || ' DPhase ' ||
                     l_dev_phase || ' DStatus ' || l_dev_status || chr(10) ||
                     ' MSG - ' || l_message;
        -- Error Returned
        IF l_dev_phase != 'COMPLETE' OR l_dev_status != 'NORMAL'
        THEN
          l_statement := 'An error occured in the running of XXCUS Costar Generate Audit Details' ||
                         l_err_msg || '.';
          fnd_file.put_line(fnd_file.log, l_statement);
          fnd_file.put_line(fnd_file.output, l_statement);
          RAISE program_error;
        END IF;
      END IF;

    ELSE
      l_statement := 'An error occured when trying to submit XXCUS Costar Generate Audit Details';
      RAISE program_error;
    END IF;

    dbms_output.put_line(l_statement);
    dbms_output.put_line('Request ID:  ' || l_req_id);
    dbms_output.put_line('Responsibility Name:  ' || p_responsibility_name);
    dbms_output.put_line('User Name:  ' || p_user_name);    
	dbms_output.put_line('Run ID:  ' || p_run_id);
	dbms_output.put_line('Group ID:  ' || p_group_id);

    --For UC4 success notification
    dbms_output.put_line('Success');

  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;
      l_err_msg := l_err_msg || '-' || l_statement || ' ...Error_Stack...' ||
                   dbms_utility.format_error_stack() ||
                   ' Error_Backtrace...' ||
                   dbms_utility.format_error_backtrace();
      dbms_output.put_line('Request ID:  ' || l_req_id);
      dbms_output.put_line('Concurrent short name :  ' || l_program);
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_package
                                          ,p_calling           => l_err_msg
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'AP');
      p_retcode := 2;
      p_errbuf  := l_err_msg;

    WHEN OTHERS THEN
      l_err_msg := l_err_msg || '-' || l_statement|| ' ...Error_Stack...' ||
                   dbms_utility.format_error_stack() ||
                   ' Error_Backtrace...' ||
                   dbms_utility.format_error_backtrace();
      dbms_output.put_line('Request ID:  ' || l_req_id);
      dbms_output.put_line('Concurrent short name :  ' || l_program);
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_package
                                          ,p_calling           => l_err_msg
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'AP');
      p_retcode := 2;
      p_errbuf  := l_err_msg;

  END uc4_Generate_Audit;
/*************************************************************************
  Procedure : UC4_Submit_Audit_Report

  PURPOSE:  This procedure is a UC4 wrapper to submit concurrent Program 
            XXCUS Costar AP Audit Report 
             
  Parameter: OUT  p_errbuf 
             OUT  p_retcode
			 IN   p_run_id
             IN   p_group_id
             IN   p_user_name
             IN   p_responsibility_name			 
  REVISIONS:
  Ver        Date        Author                  Description
  ---------  ----------  ---------------         -------------------------
  1.0        08/16/2018  Vamshi Singirikonda     Initial Build - Task ID: 20180709-00059
************************************************************************/
  PROCEDURE UC4_Submit_Audit_Report(p_errbuf               OUT VARCHAR2
								   ,p_retcode             OUT NUMBER   
								   ,p_run_id              IN VARCHAR2
								   ,p_group_id            IN VARCHAR2
								   ,p_user_name           IN VARCHAR2
								   ,p_responsibility_name IN VARCHAR2) IS

l_package     VARCHAR2(50) := 'XXCUS_COSTAR_AP_EXPORT.UC4_Submit_Audit_Report';
    l_distro_list VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';

    l_req_id     NUMBER NULL;
    l_phase      VARCHAR2(50);
    l_status     VARCHAR2(50);
    l_dev_status VARCHAR2(50);
    l_dev_phase  VARCHAR2(50);
    l_supplier_id NUMBER;
    l_rec_cnt     NUMBER := 0;
    l_interval    NUMBER := 30; -- In seconds
    l_max_time    NUMBER := 15000; -- In seconds
    l_message     VARCHAR2(2000);
    l_can_submit_request  BOOLEAN := TRUE;    
    l_err_msg             VARCHAR2(3000);
    l_err_code            NUMBER;    
    l_statement           CLOB;    
    l_user_id             NUMBER;
    l_responsibility_id   NUMBER;
    l_resp_application_id NUMBER;
    l_program             VARCHAR2(30) := 'XXCUS_COSTAR_AP_AUDIT_REPORT'; -- Program to process recon
    l_application         VARCHAR2(30) := 'XXCUS';
	l_layout BOOLEAN ;
  BEGIN

    p_retcode := 0;
    -- Deriving User Id
    BEGIN
      SELECT user_id
        INTO l_user_id
        FROM applsys.fnd_user
       WHERE user_name = upper(p_user_name)
         AND SYSDATE BETWEEN start_date AND
             nvl(end_date, trunc(SYSDATE) + 1);
    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := 'UserName - ' || p_user_name ||
                     ' not defined in Oracle';
        RAISE program_error;
      WHEN OTHERS THEN
        l_err_msg := 'Error deriving user_id for UserName - ' ||
                     p_user_name;
        RAISE program_error;
    END;

    -- Deriving Responsibility Id and Responsibility Application Id
    BEGIN
      SELECT responsibility_id, application_id
        INTO l_responsibility_id, l_resp_application_id
        FROM apps.fnd_responsibility_vl
       WHERE responsibility_name = p_responsibility_name
         AND SYSDATE BETWEEN start_date AND
             nvl(end_date, trunc(SYSDATE) + 1);
    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := 'Responsibility - ' || p_responsibility_name ||
                     ' not defined in Oracle';
        RAISE program_error;
      WHEN OTHERS THEN
        l_err_msg := 'Error deriving Responsibility_id for ResponsibilityName - ' ||
                     p_responsibility_name;
        RAISE program_error;
    END;

    --------------------------------------------------------------------------
    -- Apps Initialize
    --------------------------------------------------------------------------
    fnd_global.apps_initialize(l_user_id
                              ,l_responsibility_id
                              ,l_resp_application_id);
                              
   l_layout := apps.fnd_request.add_layout(
                            template_appl_name => 'XXCUS',
                            template_code      => 'XXCUS_COSTAR_AP_AUDIT_REPORT',
                            template_language  => 'en',
                            template_territory => 'US',
                            output_format      => 'PDF');                              

    l_req_id := fnd_request.submit_request(application => l_application
                                          ,program     => l_program
                                          ,description => NULL
                                          ,start_time  => SYSDATE
                                          ,sub_request => FALSE
                                          ,argument1   => p_run_id
                                          ,argument2   => p_group_id
                                          );
    COMMIT;

    IF (l_req_id != 0)
    THEN
      IF fnd_concurrent.wait_for_request(l_req_id
                                        ,l_interval
                                        ,l_max_time
                                        ,l_phase
                                        ,l_status
                                        ,l_dev_phase
                                        ,l_dev_status
                                        ,l_message)
      THEN
        l_err_msg := chr(10) || 'ReqID=' || l_req_id || ' DPhase ' ||
                     l_dev_phase || ' DStatus ' || l_dev_status || chr(10) ||
                     ' MSG - ' || l_message;
        -- Error Returned
        IF l_dev_phase != 'COMPLETE' OR l_dev_status != 'NORMAL'
        THEN
          l_statement := 'An error occured in the running of XXCUS Costar AP Audit Report' ||
                         l_err_msg || '.';
          fnd_file.put_line(fnd_file.log, l_statement);
          fnd_file.put_line(fnd_file.output, l_statement);
          RAISE program_error;
        END IF;
      END IF;

    ELSE
      l_statement := 'An error occured when trying to submit XXCUS Costar AP Audit Report';
      RAISE program_error;
    END IF;

    dbms_output.put_line(l_statement);
    dbms_output.put_line('Request ID:  ' || l_req_id);
    dbms_output.put_line('Responsibility Name:  ' || p_responsibility_name);
    dbms_output.put_line('User Name:  ' || p_user_name);    
	dbms_output.put_line('Run ID:  ' || p_run_id);
	dbms_output.put_line('Group ID:  ' || p_group_id);

    --For UC4 success notification
    dbms_output.put_line('Success');

  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;
      l_err_msg := l_err_msg || '-' || l_statement || ' ...Error_Stack...' ||
                   dbms_utility.format_error_stack() ||
                   ' Error_Backtrace...' ||
                   dbms_utility.format_error_backtrace();
      dbms_output.put_line('Request ID:  ' || l_req_id);
      dbms_output.put_line('Concurrent short name :  ' || l_program);
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_package
                                          ,p_calling           => l_err_msg
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'AP');
      p_retcode := 2;
      p_errbuf  := l_err_msg;

    WHEN OTHERS THEN
      l_err_msg := l_err_msg || '-' || l_statement|| ' ...Error_Stack...' ||
                   dbms_utility.format_error_stack() ||
                   ' Error_Backtrace...' ||
                   dbms_utility.format_error_backtrace();
      dbms_output.put_line('Request ID:  ' || l_req_id);
      dbms_output.put_line('Concurrent short name :  ' || l_program);
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_package
                                          ,p_calling           => l_err_msg
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'AP');
      p_retcode := 2;
      p_errbuf  := l_err_msg;

  END UC4_Submit_Audit_Report;                            
  
END XXCUS_COSTAR_AP_EXPORT;
/