CREATE OR REPLACE PACKAGE BODY APPS.XXWC_ECE_POO_PKG
AS
   /*************************************************************************
     $Header XXWC_ECE_POO_PKG.pkb $
     Module Name: XXWC_ECE_POO_PKG

     PURPOSE: Wrapper for the PO Outbound EDI program
     TMS Task Id :  20141002-00016

     REVISIONS:
     Ver        Date        Author                Description
     ---------  ----------  ------------------    ----------------
     1.0        6-Oct-2014  Manjula Chellappan    Initial Version
     1.1        9-Mar-2015  Manjula Chellappan    TMS# 20150306-00229 - Avoid excessive check
	                                              for child requests completion
   **************************************************************************/

   g_distribution_list   fnd_user.email_address%TYPE
                            := 'HDSOracleDevelopers@hdsupply.com';


   PROCEDURE Extract_POO_Outbound (
      errbuf                OUT VARCHAR2,
      retcode               OUT VARCHAR2,
      p_output_path      IN     VARCHAR2,
      p_po_number_from   IN     VARCHAR2,
      p_po_number_to     IN     VARCHAR2,
      p_cdate_from       IN     VARCHAR2,
      p_cdate_to         IN     VARCHAR2,
      p_pc_type          IN     VARCHAR2,
      p_vendor_number    IN     VARCHAR2,
      p_vendor_site_id   IN     VARCHAR2,
      p_debug_mode       IN     NUMBER DEFAULT 0)
   /*************************************************************************

     $Header Extract_POO_Outbound $
     Module Name: XXWC_ECE_POO_PKG

     PURPOSE: Wrapper for the PO Outbound EDI program
     TMS Task Id :  20141002-00016

     REVISIONS:
     Ver        Date        Author                Description
     ---------  ----------  ------------------    ----------------
     1.0        6-Oct-2014  Manjula Chellappan    Initial Version
     1.1        9-Mar-2015  Manjula Chellappan    TMS# 20150306-00229 - Avoid excessive check
	                                              for child requests completion	 

   **************************************************************************/
   IS
      l_program_name          VARCHAR2 (100) := 'ECEPOO';
      l_request_id            NUMBER;
      l_sec                   VARCHAR2 (400);
      l_file_name             VARCHAR2 (100);
      l_error_flag            VARCHAR2 (1);
      l_request_count         NUMBER;
      l_total_request_count   NUMBER;
      l_parent_request_id     NUMBER;
      l_line                  VARCHAR2 (50)
                                 := '*-----------------------------------*';

      l_date                  VARCHAR2 (50);


      CURSOR cur_edi
      IS
           SELECT po_number
             FROM ECE_POO_HEADERS_V eph
            WHERE     1 = 1
                  AND po_number BETWEEN NVL (p_po_number_from, po_number)
                                    AND NVL (p_po_number_to, po_number)
                  AND creation_date BETWEEN NVL (
                                               TO_DATE (
                                                  p_cdate_from,
                                                  'YYYY/MM/DD HH24:MI:SS'),
                                               creation_date)
                                        AND NVL (
                                               TO_DATE (
                                                  p_cdate_to,
                                                  'YYYY/MM/DD HH24:MI:SS'),
                                               creation_date)
                  AND po_type = NVL (p_pc_type, po_type)
                  AND supplier_number = NVL (p_vendor_number, supplier_number)
                  AND vendor_site_id = NVL (p_vendor_site_id, vendor_site_id)
                  AND EXISTS
                         (SELECT 'X'
                            FROM XXWC.XXWC_PO_COMM_HIST
                           WHERE     po_header_id = eph.po_header_id
                                 AND creation_date =
                                        (SELECT MAX (creation_date)
                                           FROM XXWC.XXWC_PO_COMM_HIST
                                          WHERE po_header_id = eph.po_header_id)
                                 AND XML_OR_EDI = 'EDI')
         ORDER BY 1;
   BEGIN


      fnd_file.put_line (fnd_file.LOG, l_line);
      fnd_file.put_line (fnd_file.LOG, l_line);

      l_sec := 'Initialize Apps';

      fnd_file.put_line (fnd_file.LOG, l_sec);
      fnd_file.put_line (fnd_file.LOG, l_line);

      l_parent_request_id := fnd_global.conc_request_id;

      fnd_file.put_line (fnd_file.LOG, 'User Id ' || fnd_global.user_id);
      fnd_file.put_line (fnd_file.LOG, 'Org Id ' || fnd_global.org_id);
      fnd_file.put_line (fnd_file.LOG, 'Resp Id ' || fnd_global.resp_id);
      fnd_file.put_line (fnd_file.LOG,
                         'Resp Appl Id ' || fnd_global.resp_appl_id);

      fnd_global.apps_initialize (fnd_global.user_id,
                                  fnd_global.resp_id,
                                  fnd_global.resp_appl_id);

      -- Set the Operating unit for Multi Org

      l_sec := 'Set the Operating unit for Multi Org';

      fnd_file.put_line (fnd_file.LOG, l_sec);


      fnd_request.set_org_id (fnd_global.org_id);


      l_sec := 'Truncate Table XXWC.XXWC_ECE_POO_TMP_TBL';

      fnd_file.put_line (fnd_file.LOG, l_sec);
      fnd_file.put_line (fnd_file.LOG, 'p_cdate_from ' || p_cdate_from);
      fnd_file.put_line (
         fnd_file.LOG,
         'p_cdate_from ' || TO_DATE (p_cdate_from, 'YYYY/MM/DD HH24:MI:SS'));
      fnd_file.put_line (fnd_file.LOG, 'p_cdate_to ' || p_cdate_to);
      fnd_file.put_line (
         fnd_file.LOG,
         'p_cdate_from ' || TO_DATE (p_cdate_to, 'YYYY/MM/DD HH24:MI:SS'));



      fnd_file.put_line (fnd_file.LOG, l_line);



      EXECUTE IMMEDIATE 'TRUNCATE TABLE XXWC.XXWC_ECE_POO_TMP_TBL';


      FOR rec_edi IN CUR_EDI
      LOOP
         l_error_flag := 'N';

         fnd_file.put_line (fnd_file.LOG, l_line);

         l_sec := 'Process PO : ' || rec_edi.po_number;

         fnd_file.put_line (fnd_file.LOG, l_sec);

         l_sec := 'Generate File Name';


         BEGIN
            SELECT    'POO'
                   || LPAD (
                         SUBSTR (
                            TO_CHAR (ECE_OUTPUT_RUNS_S.NEXTVAL),
                            DECODE (LENGTH (ECE_OUTPUT_RUNS_S.NEXTVAL),
                                    1, -1,
                                    2, -2,
                                    3, -3,
                                    -4),
                            4),
                         4,
                         '0')
                   || '.tmp'
              INTO l_file_name
              FROM DUAL;

            l_sec := 'EDI File Name : ' || l_file_name;

            fnd_file.put_line (fnd_file.LOG, l_sec);
         EXCEPTION
            WHEN OTHERS
            THEN
               l_file_name := NULL;

               l_error_flag := 'Y';
         END;



         IF l_error_flag = 'N'
         THEN
            l_sec := 'Submit EDI Request ';

            fnd_file.put_line (fnd_file.LOG, l_sec);


            l_request_id :=
               fnd_request.submit_request ('EC',
                                           l_program_name,
                                           NULL,
                                           NULL,
                                           FALSE,
                                           p_output_path,
                                           l_file_name,
                                           rec_edi.po_number,
                                           rec_edi.po_number,
                                           p_cdate_from,
                                           p_cdate_to,
                                           p_pc_type,
                                           p_vendor_number,
                                           p_vendor_site_id,
                                           p_debug_mode);


            l_sec := 'Request Id : ' || l_request_id;

            fnd_file.put_line (fnd_file.LOG, l_sec);

            l_sec := 'Insert into XXWC.XXWC_ECE_POO_TMP_TBL';

            IF l_request_id = -1
            THEN
               l_error_flag := 'Y';
            ELSE
               BEGIN
                  INSERT INTO XXWC.XXWC_ECE_POO_TMP_TBL
                       VALUES (l_request_id,
                               l_parent_request_id,
                               SYSDATE,
                               rec_edi.po_number,
                               l_file_name);

                  COMMIT;

                  l_sec := 'Process Completed';

                  fnd_file.put_line (fnd_file.LOG, l_sec);
                  fnd_file.put_line (fnd_file.LOG, l_line);
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     l_error_flag := 'Y';
               END;
            END IF;
         END IF;
      END LOOP;



      SELECT COUNT (*)
        INTO l_total_request_count
        FROM XXWC.XXWC_ECE_POO_TMP_TBL;


      l_sec := 'No Of POs to Process : ' || l_total_request_count;

      fnd_file.put_line (fnd_file.LOG, l_line);
      fnd_file.put_line (fnd_file.LOG, l_sec);

/* Commented for Revision 1.1 
      IF l_total_request_count > 0
      THEN
         l_sec := 'Wait for the Child Requests to complete';


         LOOP
            SELECT COUNT (*)
              INTO l_request_count
              FROM fnd_concurrent_requests
             WHERE     request_id IN (SELECT request_id
                                        FROM XXWC.XXWC_ECE_POO_TMP_TBL)
                   AND actual_completion_date IS NULL;

            IF l_request_count = 0
            THEN
               EXIT;
            END IF;
         END LOOP;

         l_sec := 'All Child Requests Completed.';

         fnd_file.put_line (fnd_file.LOG, l_sec);
         fnd_file.put_line (fnd_file.LOG, l_line);
         fnd_file.put_line (fnd_file.LOG, l_line);
      ELSE
         l_sec := 'No Eligible POs to Process';

         fnd_file.put_line (fnd_file.LOG, l_sec);
         fnd_file.put_line (fnd_file.LOG, l_line);
         fnd_file.put_line (fnd_file.LOG, l_line);
      END IF;
	  
*/
	  	  
   EXCEPTION
      WHEN OTHERS
      THEN
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_ECE_POO_PKG.EXTRACT_POO_OUTBOUND',
            p_calling             => l_sec,
            p_request_id          => l_parent_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (SQLERRM, 1, 240),
            p_distribution_list   => g_distribution_list,
            p_module              => 'EC');

         fnd_file.put_line (fnd_file.LOG, SQLERRM);
   END Extract_POO_Outbound;
END XXWC_ECE_POO_PKG;
/