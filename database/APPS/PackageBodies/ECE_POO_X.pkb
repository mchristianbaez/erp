CREATE OR REPLACE PACKAGE BODY APPS.ece_poo_x AS
-- $Header: ECEPOOXB.pls 120.1 2005/06/30 11:22:16 appldev ship $

/* -----------------------------------------------------------------------
REM
REM    PROCEDURE POPULATE_EXT_HEADER
REM        This procedure can be modified by the user to utilize the EDI
REM        Extension Tables
REM
REM ------------------------------------------------------------------------
*/

   PROCEDURE populate_ext_header(
      l_fkey      IN NUMBER,
      l_plsql_tbl IN OUT NOCOPY ece_flatfile_pvt.interface_tbl_type) IS

/******************************************************************************
      NAME:       populate_ext_header

      PURPOSE:    Extend EDI850 Outbound to pull the branch organization code at the header level

      Logic:     1) Retrieve the SHIP_TO_LOCATION_CODE 
                 2) Query the ORGANIZATION_CODE based on the SHIP_TO_LOCATION_CODE
                 3) Set extended column ORGNAIZATION_CODE
                                  
      REVISIONS:
      Ver        Date        Author           Description
      ---------  ----------  ---------------  ------------------------------------
      1.0        15-FEB-12   Lee Spitzer       1. Created the procedure
      1.1        07-JUN-14   Lee Spitzer       2. Added Logic to Assign DIRECT_OR_STANDARD
   ******************************************************************************/
 
      l_organization_id NUMBER;
      l_organization_code VARCHAR2(3);
      l_position_id NUMBER;
      l_location_code vARCHAR2(60);
      l_temp_position INTEGER;
      l_old_value VARCHAR2(32767); 
      
      
      ---Added 6/7/2014
      l_drop_count NUMBER;
      l_po_number   VARCHAR2(40);
      l_po_header_id    NUMBER;
      l_direct_or_standard VARCHAR2(10);
      l_position_id2 NUMBER;
      l_temp_position2 INTEGER;
      l_old_value2 VARCHAR2(32767);
      l_direct_phone   VARCHAR2(240);
      l_position_id3 NUMBER;
      l_temp_position3 INTEGER;
      l_old_value3 VARCHAR2(32767);
      
   
      BEGIN

        /*Get the Ship to Location Code Name*/
        
         ece_flatfile_PVT.find_pos (l_plsql_tbl,
                                    'SHIP_TO_LOCATION_CODE',
                                    l_position_id );


        
        l_location_code := l_plsql_tbl(l_position_id).value;


        --Get the organization_id from hr_location from the ship to code
        BEGIN
          SELECT inventory_organization_id
          INTO   l_organization_id
          FROM   hr_locations
          WHERE  location_code = l_location_code;
        EXCEPTION
            WHEN OTHERS THEN
                 l_organization_id := NULL;
        END;
        
        --Get the organizataion_code from mtl_parameters
        
        IF l_organization_id IS NOT NULL THEN
        
              BEGIN
                    SELECT organization_code
                    INTO   l_organization_code
                    FROM   mtl_parameters
                    WHERE  organization_id = l_organization_id;
                    
              EXCEPTION
                    WHEN OTHERS THEN
                            l_organization_code := NULL;
              END;
              
        ELSE 
            
            l_organization_code := NULL;
        
        
        END IF;
        
        
        ece_extract_utils_pub.ext_get_value
            (l_plsql_tbl,
            'ORGANIZATION_CODE',
            l_temp_position,
            l_old_value);
        
        
        
        ece_extract_utils_pub.ext_insert_value
            (l_plsql_tbl,
            l_temp_position,
            l_organization_code);
        


      /* Added 6/7/2014 */
      
         ece_flatfile_PVT.find_pos (l_plsql_tbl,
                                    'PO_NUMBER',
                                    l_position_id2 );
                                    
                                    

         l_po_number := l_plsql_tbl(l_position_id2).value;
  
        BEGIN
        
            select po_header_id
            into   l_po_header_id
            from   po_headers_all
            where  segment1 = l_po_number;
        EXCEPTION
            WHEN others THEN
                    l_po_header_id := NULL;
        END;
        
        
        IF l_po_header_id is not null then
          
      
                 BEGIN
                    select count(*)
                    into   l_drop_count
                    from   po_line_locations_all
                    where  po_header_id = l_po_header_id
                    and    drop_ship_flag = 'Y';
                 EXCEPTION
                    when others then
                           l_drop_count := 0;
                 END;
              
         
        
                IF l_drop_count > 0 THEN
                
                        l_direct_or_standard := 'DIRECT';
                        
                        BEGIN
        
                            select xxwc_po_document_util.xxwc_get_ds_bill_cont_phone (to_number(l_po_header_id)) 
                            into   l_direct_phone
                            from dual;
                            
                        EXCEPTION
                        
                            when others then    
                                    l_direct_phone := NULL;
                        
                        END;
                        
                        
                ELSE
                
                        l_direct_or_standard := 'STANDARD';
                        l_direct_phone := NULL;
                        
                END IF;
                
                
        ELSE
        
            l_direct_or_standard := 'STANDARD';
            l_direct_phone := NULL;
        
        END IF;
         
         
        
        ece_extract_utils_pub.ext_get_value
            (l_plsql_tbl,
            'DIRECT_OR_STANDARD',
            l_temp_position2,
            l_old_value2);
        
        
        
        ece_extract_utils_pub.ext_insert_value
            (l_plsql_tbl,
            l_temp_position2,
            l_direct_or_standard);
        
        
        
        ece_extract_utils_pub.ext_get_value
            (l_plsql_tbl,
            'DIRECT_PHONE',
            l_temp_position3,
            l_old_value3);
        
        
        
        ece_extract_utils_pub.ext_insert_value
            (l_plsql_tbl,
            l_temp_position3,
            l_direct_phone);
        
        
        
        
         NULL;



      END populate_ext_header;

/* -----------------------------------------------------------------------
REM
REM    PROCEDURE POPULATE_EXT_LINE
REM        This procedure can be modified by the user to utilize the EDI
REM        Extension Tables
REM
REM ------------------------------------------------------------------------
*/

   PROCEDURE populate_ext_line(
      l_fkey      IN NUMBER,
      l_plsql_tbl IN OUT NOCOPY ece_flatfile_pvt.interface_tbl_type) IS

      BEGIN

         NULL;

      END populate_ext_line;

/* -----------------------------------------------------------------------
REM
REM    PROCEDURE POPULATE_EXT_SHIPMENT
REM        This procedure can be modified by the user to utilize the EDI
REM        Extension Tables
REM
REM ------------------------------------------------------------------------
*/

   PROCEDURE populate_ext_shipment(
      l_fkey      IN NUMBER,
      l_plsql_tbl IN OUT NOCOPY ece_flatfile_pvt.interface_tbl_type) IS

/******************************************************************************
      NAME:       populate_ext_shipment

      PURPOSE:    Extend EDI850 Outbound to pull the branch organization code at the shipment level

      Logic:     1) Retrieve the SHIP_TO_LOCATION_CODE 
                 2) Query the ORGANIZATION_CODE based on the SHIP_TO_LOCATION_CODE
                 3) Set extended column ORGNAIZATION_CODE
                                  
      REVISIONS:
      Ver        Date        Author           Description
      ---------  ----------  ---------------  ------------------------------------
      1.0        15-FEB-12   Lee Spitzer       1. Created the procedure
   ******************************************************************************/
      
      l_organization_id NUMBER;
      l_organization_code VARCHAR2(3);
      l_position_id NUMBER;
      l_location_code VARCHAR2(60);
      l_temp_position INTEGER;
      l_old_value VARCHAR2(32767); 
   
      BEGIN

        /*Get the Ship to Location Code Name*/
        
         ece_flatfile_PVT.find_pos (l_plsql_tbl,
                                    'SHIP_TO_LOCATION_CODE',
                                    l_position_id );


        
        l_location_code := l_plsql_tbl(l_position_id).value;


        --Get the organization_id from hr_location from the ship to code
        BEGIN
          SELECT inventory_organization_id
          INTO   l_organization_id
          FROM   hr_locations
          WHERE  location_code = l_location_code;
        EXCEPTION
            WHEN OTHERS THEN
                 l_organization_id := NULL;
        END;
        
        --Get the organizataion_code from mtl_parameters
        
        IF l_organization_id IS NOT NULL THEN
        
              BEGIN
                    SELECT organization_code
                    INTO   l_organization_code
                    FROM   mtl_parameters
                    WHERE  organization_id = l_organization_id;
                    
              EXCEPTION
                    WHEN OTHERS THEN
                            l_organization_code := NULL;
              END;
              
        ELSE 
            
            l_organization_code := NULL;
        
        
        END IF;
        
        
        ece_extract_utils_pub.ext_get_value
            (l_plsql_tbl,
            'ORGANIZATION_CODE',
            l_temp_position,
            l_old_value);
        
        
        
        ece_extract_utils_pub.ext_insert_value
            (l_plsql_tbl,
            l_temp_position,
            l_organization_code);
        
 


      END populate_ext_shipment;

   PROCEDURE populate_ext_project(
      l_fkey      IN NUMBER,
      l_plsql_tbl IN OUT NOCOPY ece_flatfile_pvt.interface_tbl_type) IS

      BEGIN

         NULL;

      END populate_ext_project;

END ece_poo_x;
/
