--
-- XXWCAR_OCM_DNB_DATAPOINTS  (Package Body) 
--
CREATE OR REPLACE PACKAGE BODY APPS.XXWCAR_OCM_DNB_DATAPOINTS
AS
   /********************************************************************************
   *  Script Name: XXWCAR_OCM_DNB_DATAPOINTS.pkb
   *
   *  Description: Package to derive D&B data points.
   *
   *  History:
   *
   *  Version  Date          Author          Description
   *********************************************************************************
   *  1.0      07-Nov-2011   Gopi Damuluri     Initial development.
   *********************************************************************************/

   l_party_id              NUMBER (30) := NULL;
   -- party_id
   l_fss                   NUMBER (30) := NULL;
   -- current financial stress score class
   l_csc                   NUMBER (30) := NULL;
   -- dnb credit score class
   l_paydex_current        NUMBER (30) := NULL;
   -- dnb current paydex value
   l_suit_ind              VARCHAR2 (30) := NULL;
   -- dnb old suit indicator
   l_lien_ind              VARCHAR2 (30) := NULL;
   -- dnb old lien indicator
   l_judgement_ind         VARCHAR2 (30) := NULL;
   -- dnb old judgement ind
   l_suit_judge_ind        VARCHAR2 (30) := NULL;
   -- dnb new suit judge ind
   l_claims_ind            VARCHAR2 (30) := NULL;
   --dnb claims ind
   l_bankruptcy_ind        VARCHAR2 (30) := NULL;
   -- dnb bankruptcy indicator
   l_high_credit           NUMBER (30) := NULL;
   -- dnb high credit
   l_avg_high_credit       NUMBER (30) := NULL;
   -- dnb average high credit
   l_num_trade_exp         NUMBER (30) := NULL;
   -- dnb number of trade experiences
   l_oob_ind               VARCHAR2 (30) := NULL;
   -- dnb out of business indicator
   l_sic_code              NUMBER (30) := NULL;
   -- dnb SIC code
   l_year_est              NUMBER (30) := NULL;
   -- dnb year established
   l_yib                   NUMBER (30) := NULL;
   --years in business
   l_government_entity     VARCHAR2 (30) := NULL;
   -- government entity indicator
   l_approval_override     VARCHAR2 (30) := NULL;
   -- approval override indicator
   l_auto_decline          VARCHAR2 (30) := NULL;
   -- auto decline indicator
   l_rec_limit             NUMBER (30) := NULL;
   --recommended limit returned from xxwcar_dnb_scoring
   l_rec_class             VARCHAR2 (30) := NULL;
   --recommended credit class from xxwcar_dnb_scoring
   l_skip_approval         VARCHAR2 (30) := NULL;
   --skip approval from xxwcar_dnb_scoring
   l_recommendation_code   VARCHAR2 (30) := NULL;
   --recommendation code used to set parameters in OCM
   vget_cr_data            get_cr_data_rec := NULL;
   -- credit data
   vget_org_profile_data   get_org_profile_data_rec := NULL;

   -- org data

   /* Cursors to return Dun and Bradstreet credit data */
   /* previously downloaded by users for a specific    */
   /* customer account                                 */
   CURSOR c_get_cr_data
   IS
        SELECT NVL (failure_score_class, 0) failure_score_class
              ,             --SJG Updated failure_score to failure_score_class
               NVL (credit_score_class, 0) credit_score_class
              ,DECODE (paydex_score
                      ,NULL, '0'
                      ,'UN', '0'
                      ,'UNDETERMINED', '0'
                      ,'NA', '0'
                      ,'N/A', '0'
                      ,paydex_score)
                  paydex_score
              ,NVL (suit_ind, 'N') suit_ind
              ,NVL (lien_ind, 'N') lien_ind
              ,NVL (judgement_ind, 'N') judgement_ind
              ,NVL (suit_judge_ind, 'N') suit_judge_ind
              ,NVL (claims_ind, 'N') claims_ind
              ,NVL (bankruptcy_ind, 'N') bankruptcy_ind
              ,NVL (high_credit, 0) high_credit
              ,NVL (avg_high_credit, 0) avg_high_credit --SJG Deleted num_trade_exp
          FROM hz_credit_ratings
         WHERE party_id = l_party_id
      ORDER BY creation_date DESC;

   CURSOR c_get_org_profile_data
   IS
        SELECT NVL (sic_code, '0') sic_code
              ,NVL (oob_ind, 'N') oob_ind
              ,NVL (year_established, EXTRACT (YEAR FROM SYSDATE))
                  YEAR_ESTABLISHED
              ,NVL (total_payments, 0) total_payments --SJG Added total payments
          FROM hz_organization_profiles
         WHERE     1 = 1
               AND party_id = l_party_id
               AND (effective_end_date IS NULL OR effective_end_date > SYSDATE)
               AND effective_start_date < SYSDATE
      ORDER BY creation_date DESC;

   /* The following functions are used to returndata from    */
   /* the cursors to create data points that can be called   */
   /* from Oracle Credit Management                          */

   /* Created by Steve Gribbin                         */
   /*                                                  */
   /* This function calls the c_get_cr_data cursor     */
   /* defined above, returning the financial stress    */
   /* score class*/
   FUNCTION xxocm_fin_stress_score_class (
      x_resultout      OUT NOCOPY VARCHAR2
     ,x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN NUMBER
   IS
      l_fss   NUMBER (30) := NULL;
   BEGIN
      x_resultout := fnd_api.g_ret_sts_success;
      l_party_id :=
         TO_NUMBER (ocm_add_data_points.pg_ocm_add_dp_param_rec.p_party_id);

      BEGIN
         OPEN c_get_cr_data;

         FETCH c_get_cr_data INTO vget_cr_data;

         CLOSE c_get_cr_data;

         l_fss := NVL (vget_cr_data.failure_score_class, 0);
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_fss := 0;                                                --NULL;
         WHEN OTHERS
         THEN
            x_resultout := fnd_api.g_ret_sts_unexp_error;
            x_errormsg := SQLERRM;
      END;

      RETURN (l_fss);
   END xxocm_fin_stress_score_class;

   /* Created by Steve Gribbin                         */
   /*                                                  */
   /* This function calls the c_get_cr_data cursor     */
   /* defined above, returning the credit score        */
   /* class*/
   FUNCTION xxocm_credit_score_class (x_resultout      OUT NOCOPY VARCHAR2
                                     ,x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN NUMBER
   IS
      l_csc   NUMBER (30) := NULL;
   BEGIN
      x_resultout := fnd_api.g_ret_sts_success;
      l_party_id :=
         TO_NUMBER (ocm_add_data_points.pg_ocm_add_dp_param_rec.p_party_id);

      BEGIN
         OPEN c_get_cr_data;

         FETCH c_get_cr_data INTO vget_cr_data;

         CLOSE c_get_cr_data;

         l_csc := NVL (vget_cr_data.credit_score_class, 0);
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_csc := 0;                                                --NULL;
         WHEN OTHERS
         THEN
            x_resultout := fnd_api.g_ret_sts_unexp_error;
            x_errormsg := SQLERRM;
      END;

      RETURN (l_csc);
   END xxocm_credit_score_class;

   /* Created by Steve Gribbin                         */
   /*                                                  */
   /* This function calls the c_get_cr_data cursor     */
   /* defined above, returning the current paydex      */
   FUNCTION xxocm_paydex_current (x_resultout      OUT NOCOPY VARCHAR2
                                 ,x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN NUMBER
   IS
      l_paydex_current   NUMBER (30) := NULL;
   BEGIN
      x_resultout := fnd_api.g_ret_sts_success;
      l_party_id :=
         TO_NUMBER (ocm_add_data_points.pg_ocm_add_dp_param_rec.p_party_id);

      BEGIN
         OPEN c_get_cr_data;

         FETCH c_get_cr_data INTO vget_cr_data;

         CLOSE c_get_cr_data;

         l_paydex_current := TO_NUMBER (NVL (vget_cr_data.paydex_current, 0));
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_paydex_current := 0;                                     --NULL;
         WHEN OTHERS
         THEN
            x_resultout := fnd_api.g_ret_sts_unexp_error;
            x_errormsg := SQLERRM;
      END;

      RETURN (l_paydex_current);
   END xxocm_paydex_current;

   /* Created by Steve Gribbin                         */
   /*                                                  */
   /* This function calls the c_get_cr_data cursor     */
   /* defined above, returning the suit indicator      */
   /*                                                  */
   /*                                                  */
   FUNCTION xxocm_suit_ind (x_resultout      OUT NOCOPY VARCHAR2
                           ,x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN VARCHAR2
   IS
      l_suit_ind   VARCHAR2 (30) := NULL;
   BEGIN
      x_resultout := fnd_api.g_ret_sts_success;
      l_party_id :=
         TO_NUMBER (ocm_add_data_points.pg_ocm_add_dp_param_rec.p_party_id);

      BEGIN
         OPEN c_get_cr_data;

         FETCH c_get_cr_data INTO vget_cr_data;

         CLOSE c_get_cr_data;

         l_suit_ind := NVL (vget_cr_data.suit_ind, 'N');
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_suit_ind := 'N';                                         --NULL;
         WHEN OTHERS
         THEN
            x_resultout := fnd_api.g_ret_sts_unexp_error;
            x_errormsg := SQLERRM;
      END;

      RETURN (l_suit_ind);
   END xxocm_suit_ind;

   /* Created by Steve Gribbin                         */
   /*                                                  */
   /* This function calls the c_get_cr_data cursor     */
   /* defined above, returning the lien indicator      */
   /*                                                  */
   /*                                                  */
   FUNCTION xxocm_lien_ind (x_resultout      OUT NOCOPY VARCHAR2
                           ,x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN VARCHAR2
   IS
      l_lien_ind   VARCHAR2 (30) := NULL;
   BEGIN
      x_resultout := fnd_api.g_ret_sts_success;
      l_party_id :=
         TO_NUMBER (ocm_add_data_points.pg_ocm_add_dp_param_rec.p_party_id);

      BEGIN
         OPEN c_get_cr_data;

         FETCH c_get_cr_data INTO vget_cr_data;

         CLOSE c_get_cr_data;

         l_lien_ind := NVL (vget_cr_data.lien_ind, 'N');
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_lien_ind := 'N';                                         --NULL;
         WHEN OTHERS
         THEN
            x_resultout := fnd_api.g_ret_sts_unexp_error;
            x_errormsg := SQLERRM;
      END;

      RETURN (l_lien_ind);
   END xxocm_lien_ind;

   /* Created by Steve Gribbin                         */
   /*                                                  */
   /* This function calls the c_get_cr_data cursor     */
   /* defined above, returning the judgement indicator */
   /*                                                  */
   /*                                                  */
   FUNCTION xxocm_judgement_ind (x_resultout      OUT NOCOPY VARCHAR2
                                ,x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN VARCHAR2
   IS
      l_judgement_ind   VARCHAR2 (30) := NULL;
   BEGIN
      x_resultout := fnd_api.g_ret_sts_success;
      l_party_id :=
         TO_NUMBER (ocm_add_data_points.pg_ocm_add_dp_param_rec.p_party_id);

      BEGIN
         OPEN c_get_cr_data;

         FETCH c_get_cr_data INTO vget_cr_data;

         CLOSE c_get_cr_data;

         l_judgement_ind := NVL (vget_cr_data.judgement_ind, 'N');
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_judgement_ind := 'N';                                    --NULL;
         WHEN OTHERS
         THEN
            x_resultout := fnd_api.g_ret_sts_unexp_error;
            x_errormsg := SQLERRM;
      END;

      RETURN (l_judgement_ind);
   END xxocm_judgement_ind;

   /* Created by Steve Gribbin                         */
   /*                                                  */
   /* This function calls the c_get_cr_data cursor     */
   /* defined above, returning the suit / judge indicator */
   /*                                                  */
   /*                                                  */
   FUNCTION xxocm_suit_judge_ind (x_resultout      OUT NOCOPY VARCHAR2
                                 ,x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN VARCHAR2
   IS
      l_suit_judge_ind   VARCHAR2 (30) := NULL;
   BEGIN
      x_resultout := fnd_api.g_ret_sts_success;
      l_party_id :=
         TO_NUMBER (ocm_add_data_points.pg_ocm_add_dp_param_rec.p_party_id);

      BEGIN
         OPEN c_get_cr_data;

         FETCH c_get_cr_data INTO vget_cr_data;

         CLOSE c_get_cr_data;

         CASE
            WHEN (vget_cr_data.suit_judge_ind) = 'N'
            -- if no suits or judgements
            THEN
               l_suit_judge_ind := 'N';        -- then indicator says N (none)
            WHEN (vget_cr_data.suit_judge_ind) = 'Y' -- if suits or judgements
            THEN
               l_suit_judge_ind := 'Y';         -- then indicator says Y (yes)
            ELSE
               l_suit_judge_ind := 'N';
         -- then nulls and default says N (no) for scoring
         END CASE;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_suit_judge_ind := 'N';                                   --NULL;
         WHEN OTHERS
         THEN
            x_resultout := fnd_api.g_ret_sts_unexp_error;
            x_errormsg := SQLERRM;
      END;

      RETURN (l_suit_judge_ind);
   END xxocm_suit_judge_ind;

   /* Created by Steve Gribbin                         */
   /*                                                  */
   /* This function calls the c_get_cr_data cursor     */
   /* defined above, returning the claims indicator */
   /*                                                  */
   /*                                                  */
   FUNCTION xxocm_claims_ind (x_resultout      OUT NOCOPY VARCHAR2
                             ,x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN VARCHAR2
   IS
      l_claims_ind   VARCHAR2 (30) := NULL;
   BEGIN
      x_resultout := fnd_api.g_ret_sts_success;
      l_party_id :=
         TO_NUMBER (ocm_add_data_points.pg_ocm_add_dp_param_rec.p_party_id);

      BEGIN
         OPEN c_get_cr_data;

         FETCH c_get_cr_data INTO vget_cr_data;

         CLOSE c_get_cr_data;

         CASE
            WHEN (vget_cr_data.claims_ind) = 'N'
            -- if no claims
            THEN
               l_claims_ind := 'N';            -- then indicator says N (none)
            WHEN (vget_cr_data.claims_ind) = 'Y'                  -- if claims
            THEN
               l_claims_ind := 'Y';             -- then indicator says Y (yes)
            ELSE
               l_claims_ind := 'N';
         -- then nulls and default says N (no) for scoring
         END CASE;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_claims_ind := 'N';                                       --NULL;
         WHEN OTHERS
         THEN
            x_resultout := fnd_api.g_ret_sts_unexp_error;
            x_errormsg := SQLERRM;
      END;

      RETURN (l_claims_ind);
   END xxocm_claims_ind;



   /* Created by Steve Gribbin                         */
   /*                                                  */
   /* This function calls the c_get_cr_data cursor     */
   /* defined above, returning the bankruptcy indicator */
   /*                                                  */
   /*                                                  */
   FUNCTION xxocm_bankruptcy_ind (x_resultout      OUT NOCOPY VARCHAR2
                                 ,x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN VARCHAR2
   IS
      l_bankruptcy_ind   VARCHAR2 (30) := NULL;
   BEGIN
      x_resultout := fnd_api.g_ret_sts_success;
      l_party_id :=
         TO_NUMBER (ocm_add_data_points.pg_ocm_add_dp_param_rec.p_party_id);

      BEGIN
         OPEN c_get_cr_data;

         FETCH c_get_cr_data INTO vget_cr_data;

         CLOSE c_get_cr_data;

         l_bankruptcy_ind := NVL (vget_cr_data.bankruptcy_ind, 'N');
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_bankruptcy_ind := 'N';                                   --NULL;
         WHEN OTHERS
         THEN
            x_resultout := fnd_api.g_ret_sts_unexp_error;
            x_errormsg := SQLERRM;
      END;

      RETURN (l_bankruptcy_ind);
   END xxocm_bankruptcy_ind;

   /* Created by Steve Gribbin                         */
   /*                                                  */
   /* This function calls the c_get_cr_data cursor    */
   /* defined above, returning the dnb high credit     */
   /*                                                  */
   FUNCTION xxocm_high_credit (x_resultout      OUT NOCOPY VARCHAR2
                              ,x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN NUMBER
   IS
      l_high_credit   NUMBER (30) := NULL;
   BEGIN
      x_resultout := fnd_api.g_ret_sts_success;
      l_party_id :=
         TO_NUMBER (ocm_add_data_points.pg_ocm_add_dp_param_rec.p_party_id);

      BEGIN
         OPEN c_get_cr_data;

         FETCH c_get_cr_data INTO vget_cr_data;

         CLOSE c_get_cr_data;

         l_high_credit := NVL (vget_cr_data.high_credit, 0);
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_high_credit := 0;                                        --NULL;
         WHEN OTHERS
         THEN
            x_resultout := fnd_api.g_ret_sts_unexp_error;
            x_errormsg := SQLERRM;
      END;

      RETURN (l_high_credit);
   END xxocm_high_credit;

   /* Created by Steve Gribbin                         */
   /*                                                  */
   /* This function calls the c_get_cr_data cursor    */
   /* defined above, returning the dnb avg high credit     */
   /*                                                  */
   FUNCTION xxocm_avg_high_credit (x_resultout      OUT NOCOPY VARCHAR2
                                  ,x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN NUMBER
   IS
      l_avg_high_credit   NUMBER (30) := NULL;
   BEGIN
      x_resultout := fnd_api.g_ret_sts_success;
      l_party_id :=
         TO_NUMBER (ocm_add_data_points.pg_ocm_add_dp_param_rec.p_party_id);

      BEGIN
         OPEN c_get_cr_data;

         FETCH c_get_cr_data INTO vget_cr_data;

         CLOSE c_get_cr_data;

         l_avg_high_credit := NVL (vget_cr_data.avg_high_credit, 0);
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_avg_high_credit := 0;                                    --NULL;
         WHEN OTHERS
         THEN
            x_resultout := fnd_api.g_ret_sts_unexp_error;
            x_errormsg := SQLERRM;
      END;

      RETURN (l_avg_high_credit);
   END xxocm_avg_high_credit;

   /* Created by Steve Gribbin                         */
   /*                                                  */
   /* This function calls the c_get_cr_data cursor    */
   /* defined above, returning the number of trade experiences     */
   /*                                                  */
   FUNCTION xxocm_num_trade_exp (x_resultout      OUT NOCOPY VARCHAR2
                                ,x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN NUMBER
   IS
      l_num_trade_exp   NUMBER (30) := NULL;
   BEGIN
      x_resultout := fnd_api.g_ret_sts_success;
      l_party_id :=
         TO_NUMBER (ocm_add_data_points.pg_ocm_add_dp_param_rec.p_party_id);

      BEGIN
         OPEN c_get_org_profile_data;

         FETCH c_get_org_profile_data INTO vget_org_profile_data;

         CLOSE c_get_org_profile_data;

         l_num_trade_exp := NVL (vget_org_profile_data.total_payments, 0); --SJG replace num_trade_payments w total_payments
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_num_trade_exp := 0;                                      --NULL;
         WHEN OTHERS
         THEN
            x_resultout := fnd_api.g_ret_sts_unexp_error;
            x_errormsg := SQLERRM;
      END;

      RETURN (l_num_trade_exp);
   END xxocm_num_trade_exp;

   /* Created by Steve Gribbin                         */
   /*                                                  */
   /* This function calls the c_get_cr_data cursor     */
   /* and the c_get_org_profile cursor and returns     */
   /* the SIC code                                     */
   /*                                                  */

   FUNCTION xxocm_sic_code (x_resultout      OUT NOCOPY VARCHAR2
                           ,x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN NUMBER
   IS
      l_sic_code   NUMBER (30) := NULL;
   BEGIN
      x_resultout := fnd_api.g_ret_sts_success;
      l_party_id :=
         TO_NUMBER (ocm_add_data_points.pg_ocm_add_dp_param_rec.p_party_id);

      BEGIN
         OPEN c_get_org_profile_data;

         FETCH c_get_org_profile_data INTO vget_org_profile_data;

         CLOSE c_get_org_profile_data;

         l_sic_code := TO_NUMBER (NVL (vget_org_profile_data.sic_code, 0));
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_sic_code := 0;                                           --NULL;
         WHEN OTHERS
         THEN
            x_resultout := fnd_api.g_ret_sts_unexp_error;
            x_errormsg := SQLERRM;
      END;


      RETURN (l_sic_code);
   END xxocm_sic_code;

   /* Created by Steve Gribbin                         */
   /*                                                  */
   /* This function calls the c_get_cr_data cursor     */
   /* and the c_get_org_profile cursor and returns     */
   /* the out of business indicator                    */
   /*                                                  */

   FUNCTION xxocm_oob_ind (x_resultout      OUT NOCOPY VARCHAR2
                          ,x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN VARCHAR2
   IS
      l_oob_ind   VARCHAR2 (30) := NULL;
   BEGIN
      x_resultout := fnd_api.g_ret_sts_success;
      l_party_id :=
         TO_NUMBER (ocm_add_data_points.pg_ocm_add_dp_param_rec.p_party_id);

      BEGIN
         OPEN c_get_org_profile_data;

         FETCH c_get_org_profile_data INTO vget_org_profile_data;

         CLOSE c_get_org_profile_data;

         l_oob_ind := NVL (vget_org_profile_data.oob_ind, 'N');
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_oob_ind := 'N';                                          --NULL;
         WHEN OTHERS
         THEN
            x_resultout := fnd_api.g_ret_sts_unexp_error;
            x_errormsg := SQLERRM;
      END;


      RETURN (l_oob_ind);
   END xxocm_oob_ind;

   /* Created by Steve Gribbin                         */
   /*                                                  */
   /* This function calls the c_get_cr_data cursor     */
   /* and the c_get_org_profile cursor and returns     */
   /* the year established                             */
   /*                                                  */

   FUNCTION xxocm_year_est (x_resultout      OUT NOCOPY VARCHAR2
                           ,x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN NUMBER
   IS
      l_year_est   NUMBER (30) := NULL;
   BEGIN
      x_resultout := fnd_api.g_ret_sts_success;
      l_party_id :=
         TO_NUMBER (ocm_add_data_points.pg_ocm_add_dp_param_rec.p_party_id);

      BEGIN
         OPEN c_get_org_profile_data;

         FETCH c_get_org_profile_data INTO vget_org_profile_data;

         CLOSE c_get_org_profile_data;

         l_year_est :=
            NVL (vget_org_profile_data.year_established
                ,EXTRACT (YEAR FROM SYSDATE));
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_year_est := EXTRACT (YEAR FROM SYSDATE);
         WHEN OTHERS
         THEN
            x_resultout := fnd_api.g_ret_sts_unexp_error;
            x_errormsg := SQLERRM;
      END;


      RETURN (l_year_est);
   END xxocm_year_est;

   /* The following functions are used to determine if an    */
   /* auto deline rule or an auto approve rule are met       */
   /*                                                        */

   /* Created by Steve Gribbin                         */
   /*                                                  */
   /* This function is used to determine the number     */
   /* of years in which the business has been in       */
   /* operation                                        */
   /*                                                  */

   FUNCTION xxocm_yib (x_resultout      OUT NOCOPY VARCHAR2
                      ,x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN VARCHAR2
   IS
      l_yib   NUMBER (30) := NULL;
   BEGIN
      x_resultout := fnd_api.g_ret_sts_success;
      l_party_id :=
         TO_NUMBER (ocm_add_data_points.pg_ocm_add_dp_param_rec.p_party_id);

      BEGIN
         OPEN c_get_org_profile_data;

         FETCH c_get_org_profile_data INTO vget_org_profile_data;

         CLOSE c_get_org_profile_data;

         l_year_est :=
            (xxwcar_ocm_dnb_datapoints.xxocm_year_est (x_resultout
                                                      ,x_errormsg));
         l_yib := EXTRACT (YEAR FROM SYSDATE) - l_year_est;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_yib := 0;                                                --NULL;
         WHEN OTHERS
         THEN
            x_resultout := fnd_api.g_ret_sts_unexp_error;
            x_errormsg := SQLERRM;
      END;

      RETURN l_yib;
   END xxocm_yib;

   /* Created by Steve Gribbin                         */
   /*                                                  */
   /* This function calls the c_get_cr_data cursor     */
   /* and the c_get_org_profile cursor and returns     */
   /* Y if the SIC code is between 8200 and 8299       */
   /* or if the SIC code is between 9100 and 9721      */
   /*                                                  */

   FUNCTION xxocm_government_entity (x_resultout      OUT NOCOPY VARCHAR2
                                    ,x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN VARCHAR2
   IS
      l_government_entity   VARCHAR2 (30) := NULL;
   BEGIN
      x_resultout := fnd_api.g_ret_sts_success;
      l_party_id :=
         TO_NUMBER (ocm_add_data_points.pg_ocm_add_dp_param_rec.p_party_id);

      BEGIN
         OPEN c_get_org_profile_data;

         FETCH c_get_org_profile_data INTO vget_org_profile_data;

         CLOSE c_get_org_profile_data;

         l_sic_code :=
            (xxwcar_ocm_dnb_datapoints.xxocm_sic_code (x_resultout
                                                      ,x_errormsg));
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_sic_code := 0;                                           --NULL;
         WHEN OTHERS
         THEN
            x_resultout := fnd_api.g_ret_sts_unexp_error;
            x_errormsg := SQLERRM;
      END;

      CASE
         WHEN l_sic_code >= 8200 AND l_sic_code <= 8299
         THEN
            l_government_entity := 'Y';
         WHEN l_sic_code >= 9100 AND l_sic_code <= 9721
         THEN
            l_government_entity := 'Y';
         ELSE
            l_government_entity := 'N';
      END CASE;

      RETURN (l_government_entity);
   END xxocm_government_entity;

   /* Created by Steve Gribbin                         */
   /*                                                  */
   /* This function determines whether there are any   */
   /* dnb variables present that would lead to and     */
   /* auto rejection of a credit applicant             */
   /*                                                  */


   FUNCTION xxocm_auto_decline (x_resultout      OUT NOCOPY VARCHAR2
                               ,x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN VARCHAR2
   IS
      l_fss              NUMBER (30) := NULL;
      l_csc              NUMBER (30) := NULL;
      l_paydex_current   NUMBER (30) := NULL;
      l_suit_ind         VARCHAR2 (30) := NULL;
      l_lien_ind         VARCHAR2 (30) := NULL;
      l_judgement_ind    VARCHAR2 (30) := NULL;
      l_suit_judge_ind   VARCHAR2 (30) := NULL;
      l_claims_ind       VARCHAR2 (30) := NULL;
      l_bankruptcy_ind   VARCHAR2 (30) := NULL;
      l_yib              NUMBER (30) := NULL;
      l_oob_ind          VARCHAR2 (30) := NULL;
      l_num_trade_exp    NUMBER (30) := NULL;
      l_result           VARCHAR2 (100) := NULL;
      l_errormsg         VARCHAR2 (100) := NULL;
   BEGIN
      x_resultout := fnd_api.g_ret_sts_success;
      l_party_id :=
         TO_NUMBER (ocm_add_data_points.pg_ocm_add_dp_param_rec.p_party_id);


      l_fss :=
         (xxwcar_ocm_dnb_datapoints.xxocm_fin_stress_score_class (l_result
                                                                 ,l_errormsg));
      l_csc :=
         (xxwcar_ocm_dnb_datapoints.xxocm_credit_score_class (l_result
                                                             ,l_errormsg));
      l_paydex_current :=
         (xxwcar_ocm_dnb_datapoints.xxocm_paydex_current (l_result
                                                         ,l_errormsg));
      l_suit_ind :=
         (xxwcar_ocm_dnb_datapoints.xxocm_suit_ind (l_result, l_errormsg));
      l_lien_ind :=
         (xxwcar_ocm_dnb_datapoints.xxocm_lien_ind (l_result, l_errormsg));
      l_judgement_ind :=
         (xxwcar_ocm_dnb_datapoints.xxocm_judgement_ind (l_result
                                                        ,l_errormsg));
      l_suit_judge_ind :=
         (xxwcar_ocm_dnb_datapoints.xxocm_suit_judge_ind (l_result
                                                         ,l_errormsg));
      l_claims_ind :=
         (xxwcar_ocm_dnb_datapoints.xxocm_claims_ind (l_result, l_errormsg));
      l_bankruptcy_ind :=
         (xxwcar_ocm_dnb_datapoints.xxocm_bankruptcy_ind (l_result
                                                         ,l_errormsg));
      l_yib := (xxwcar_ocm_dnb_datapoints.xxocm_yib (l_result, l_errormsg));
      l_oob_ind :=
         (xxwcar_ocm_dnb_datapoints.xxocm_oob_ind (l_result, l_errormsg));
      l_num_trade_exp :=
         (xxwcar_ocm_dnb_datapoints.xxocm_num_trade_exp (l_result
                                                        ,l_errormsg));

      CASE
         WHEN l_fss = 4
         THEN
            l_auto_decline := 'Y';
         WHEN l_fss = 5
         THEN
            l_auto_decline := 'Y';
         WHEN l_fss = 0
         THEN
            l_auto_decline := 'Y';
         WHEN l_csc = 5
         THEN
            l_auto_decline := 'Y';
         WHEN l_csc = 0
         THEN
            l_auto_decline := 'Y';
         WHEN l_paydex_current <= 40
         THEN
            l_auto_decline := 'Y';
         WHEN l_suit_ind = 'Y'
         THEN
            l_auto_decline := 'Y';
         WHEN l_lien_ind = 'Y'
         THEN
            l_auto_decline := 'Y';
         WHEN l_judgement_ind = 'Y'
         THEN
            l_auto_decline := 'Y';
         WHEN l_suit_judge_ind = 'Y'
         THEN
            l_auto_decline := 'Y';
         WHEN l_claims_ind = 'Y'
         THEN
            l_auto_decline := 'Y';
         WHEN l_bankruptcy_ind = 'Y'
         THEN
            l_auto_decline := 'Y';
         WHEN l_yib < 1
         THEN
            l_auto_decline := 'Y';
         WHEN l_oob_ind = 'Y'
         THEN
            l_auto_decline := 'Y';
         WHEN l_num_trade_exp <= 2
         THEN
            l_auto_decline := 'Y';
         ELSE
            l_auto_decline := 'N';
      END CASE;

      RETURN (l_auto_decline);
   END xxocm_auto_decline;

   /* Created by Steve Gribbin                              */
   /*                                                       */
   /* Use DNB scoring parameters to determine recommended   */
   /* credit limit, recommended credit class, and           */
   /* skip approval                                         */
   /*                                                       */

   /* Created by Steve Gribbin                         */
   /*                                                  */
   /* This function is used to return a recommended    */
   /* credit limit based on the average high credit,   */
   /* financial stress score, auto decline indicator,  */
   /* and government indicator                         */
   /*                                                  */

   FUNCTION xxocm_rec_limit (x_resultout      OUT NOCOPY VARCHAR2
                            ,x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN NUMBER
   IS
      l_rec_limit         NUMBER (30) := NULL;
      l_avg_high_credit   NUMBER (30) := NULL;
      l_fss               NUMBER (30) := NULL;
      l_gov_ind           VARCHAR2 (30) := NULL;
      l_auto_decline      VARCHAR2 (30) := NULL;
      l_result            VARCHAR2 (100) := NULL;
      l_errormsg          VARCHAR2 (100) := NULL;
   BEGIN
      x_resultout := fnd_api.g_ret_sts_success;
      l_party_id :=
         TO_NUMBER (ocm_add_data_points.pg_ocm_add_dp_param_rec.p_party_id);
      l_avg_high_credit :=
         (xxwcar_ocm_dnb_datapoints.xxocm_avg_high_credit (l_result
                                                          ,l_errormsg));
      l_fss :=
         (xxwcar_ocm_dnb_datapoints.xxocm_fin_stress_score_class (l_result
                                                                 ,l_errormsg));
      l_gov_ind :=
         -- (xxwcar_ocm_dnb_datapoints.xxocm_gov_ind (l_result, l_errormsg)); -- ?????
         (xxwcar_ocm_dnb_datapoints.xxocm_government_entity (l_result
                                                            ,l_errormsg));
      l_auto_decline :=
         (xxwcar_ocm_dnb_datapoints.xxocm_auto_decline (l_result, l_errormsg));

      --Get recommended limit based on avg high credit, fss, government indicator, and auto decline indicator
      BEGIN
         SELECT rec_limit
           INTO l_rec_limit
           FROM xxwc.xxwcar_dnb_scoring
          WHERE     l_avg_high_credit BETWEEN ahc_min AND ahc_max
                AND l_fss = fss
                AND l_auto_decline = auto_decline
                AND l_gov_ind = gov_ind;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_rec_limit := 0;
         WHEN OTHERS
         THEN
            x_resultout := fnd_api.g_ret_sts_unexp_error;
            x_errormsg := SQLERRM;
      END;

      RETURN (l_rec_limit);
   END xxocm_rec_limit;

   /* Created by Steve Gribbin                         */
   /*                                                  */
   /* This function is used to return a recommended    */
   /* credit class based on the average high credit,   */
   /* financial stress score, auto decline indicator,  */
   /* and government indicator                         */
   /*                                                  */

   FUNCTION xxocm_rec_credit_class (x_resultout      OUT NOCOPY VARCHAR2
                                   ,x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN VARCHAR2
   IS
      l_rec_class         VARCHAR2 (30) := NULL;
      l_avg_high_credit   NUMBER (30) := NULL;
      l_fss               NUMBER (30) := NULL;
      l_gov_ind           VARCHAR2 (30) := NULL;
      l_auto_decline      VARCHAR2 (30) := NULL;
      l_result            VARCHAR2 (100) := NULL;
      l_errormsg          VARCHAR2 (100) := NULL;
   BEGIN
      x_resultout := fnd_api.g_ret_sts_success;
      l_party_id :=
         TO_NUMBER (ocm_add_data_points.pg_ocm_add_dp_param_rec.p_party_id);
      l_avg_high_credit :=
         (xxwcar_ocm_dnb_datapoints.xxocm_avg_high_credit (l_result
                                                          ,l_errormsg));
      l_fss :=
         (xxwcar_ocm_dnb_datapoints.xxocm_fin_stress_score_class (l_result
                                                                 ,l_errormsg));
      l_gov_ind :=
         -- (xxwcar_ocm_dnb_datapoints.xxocm_gov_ind (l_result, l_errormsg));
         (xxwcar_ocm_dnb_datapoints.xxocm_government_entity (l_result
                                                            ,l_errormsg));
      l_auto_decline :=
         (xxwcar_ocm_dnb_datapoints.xxocm_auto_decline (l_result, l_errormsg));

      --Get recommended credit class based on avg high credit, fss, government indicator, and auto decline indicator
      BEGIN
         SELECT rec_credit_class
           INTO l_rec_class
           FROM xxwc.xxwcar_dnb_scoring
          WHERE     l_avg_high_credit BETWEEN ahc_min AND ahc_max
                AND l_fss = fss
                AND l_auto_decline = auto_decline
                AND l_gov_ind = gov_ind;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_rec_class := 'N/A';
         WHEN OTHERS
         THEN
            x_resultout := fnd_api.g_ret_sts_unexp_error;
            x_errormsg := SQLERRM;
      END;

      RETURN (l_rec_class);
   END xxocm_rec_credit_class;

   /* Created by Steve Gribbin                         */
   /*                                                  */
   /* This function is used to return a recommended    */
   /* skip approval based on the average high credit,  */
   /* financial stress score, auto decline indicator,  */
   /* and government indicator                         */
   /*                                                  */

   FUNCTION xxocm_rec_skip_approval (x_resultout      OUT NOCOPY VARCHAR2
                                    ,x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN VARCHAR2
   IS
      l_skip_approval     VARCHAR2 (30) := NULL;
      l_avg_high_credit   NUMBER (30) := NULL;
      l_fss               NUMBER (30) := NULL;
      l_gov_ind           VARCHAR2 (30) := NULL;
      l_auto_decline      VARCHAR2 (30) := NULL;
      l_result            VARCHAR2 (100) := NULL;
      l_errormsg          VARCHAR2 (100) := NULL;
   BEGIN
      x_resultout := fnd_api.g_ret_sts_success;
      l_party_id :=
         TO_NUMBER (ocm_add_data_points.pg_ocm_add_dp_param_rec.p_party_id);
      l_avg_high_credit :=
         (xxwcar_ocm_dnb_datapoints.xxocm_avg_high_credit (l_result
                                                          ,l_errormsg));
      l_fss :=
         (xxwcar_ocm_dnb_datapoints.xxocm_fin_stress_score_class (l_result
                                                                 ,l_errormsg));
      l_gov_ind :=
         -- (xxwcar_ocm_dnb_datapoints.xxocm_gov_ind (l_result, l_errormsg));
         (xxwcar_ocm_dnb_datapoints.xxocm_government_entity (l_result
                                                            ,l_errormsg));
      l_auto_decline :=
         (xxwcar_ocm_dnb_datapoints.xxocm_auto_decline (l_result, l_errormsg));


      --Get recommended limit based on avg high credit, fss, government indicator, and auto decline indicator
      BEGIN
         SELECT rec_skip_approval
           INTO l_skip_approval
           FROM xxwc.xxwcar_dnb_scoring
          WHERE     l_avg_high_credit BETWEEN ahc_min AND ahc_max
                AND l_fss = fss
                AND l_auto_decline = auto_decline
                AND l_gov_ind = gov_ind;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_skip_approval := 'N';
         WHEN OTHERS
         THEN
            x_resultout := fnd_api.g_ret_sts_unexp_error;
            x_errormsg := SQLERRM;
      END;

      RETURN (l_skip_approval);
   END xxocm_rec_skip_approval;

   /* Created by Steve Gribbin                          */
   /*                                                   */
   /* This function is used to return the recommendation*/
   /* code based on the average high credit,            */
   /* financial stress score, auto decline indicator,   */
   /* and government indicator                          */
   /*                                                   */


   FUNCTION xxocm_rec_code (x_resultout      OUT NOCOPY VARCHAR2
                           ,x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN NUMBER
   IS
      l_recommendation_code   NUMBER (30) := NULL;
      l_avg_high_credit       NUMBER (30) := NULL;
      l_fss                   NUMBER (30) := NULL;
      l_gov_ind               VARCHAR2 (30) := NULL;
      l_auto_decline          VARCHAR2 (30) := NULL;
      l_result                VARCHAR2 (100) := NULL;
      l_errormsg              VARCHAR2 (100) := NULL;
   BEGIN
      x_resultout := fnd_api.g_ret_sts_success;
      l_party_id :=
         TO_NUMBER (ocm_add_data_points.pg_ocm_add_dp_param_rec.p_party_id);
      l_avg_high_credit :=
         (xxwcar_ocm_dnb_datapoints.xxocm_avg_high_credit (l_result
                                                          ,l_errormsg));
      l_fss :=
         (xxwcar_ocm_dnb_datapoints.xxocm_fin_stress_score_class (l_result
                                                                 ,l_errormsg));
      l_gov_ind :=
         -- (xxwcar_ocm_dnb_datapoints.xxocm_gov_ind (l_result, l_errormsg));
         (xxwcar_ocm_dnb_datapoints.xxocm_government_entity (l_result
                                                            ,l_errormsg));
      l_auto_decline :=
         (xxwcar_ocm_dnb_datapoints.xxocm_auto_decline (l_result, l_errormsg));

      --Get recommendation code based on avg high credit, fss, government indicator, and auto decline indicator
      BEGIN
         SELECT rec_code
           INTO l_recommendation_code
           FROM xxwc.xxwcar_dnb_scoring
          WHERE     l_avg_high_credit BETWEEN ahc_min AND ahc_max
                AND l_fss = fss
                AND l_auto_decline = auto_decline
                AND l_gov_ind = gov_ind;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_recommendation_code := 0;
         WHEN OTHERS
         THEN
            x_resultout := fnd_api.g_ret_sts_unexp_error;
            x_errormsg := SQLERRM;
      END;

      RETURN (l_recommendation_code);
   END xxocm_rec_code;
END xxwcar_ocm_dnb_datapoints;
/

