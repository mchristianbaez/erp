CREATE OR REPLACE PACKAGE BODY xxwc_gl_ahh_intf_pkg AS
  /***************************************************************************************************************************************
   $Header XXWC_GL_AHH_INTF_PKG $
   Module Name: XXWC_GL_AHH_INTF_PKG.pkb
  
   PURPOSE:   This package will be used to import AHH journals
  
   REVISIONS:
   Ver        Date        Author                             Description
   ---------  ----------  -------------------------------    -------------------------
   1.0        17/05/2018  P.Vamshidhar                       TMS# 20180515-00049 - AH Harris Journals Conversion
                                                             Initial Version
   1.1        30/05/2018  Naveen Kalidindi                   TMS# 20180515-00049 - AH Harris Journals Conversion
                                                             Initial Version
  *************************************************************************************************************************************/

  g_err_callfrom VARCHAR2(100) := 'XXWC_GL_AHH_INTF_PKG';
  g_distro_list  VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';


  PROCEDURE main(x_errbuf       OUT VARCHAR2
                ,x_retcode      OUT VARCHAR2
                ,p_gl_source    IN VARCHAR2
                ,p_gl_category  IN VARCHAR2
                ,p_default_acct IN VARCHAR2
                ,p_gl_import    IN VARCHAR2
                ,p_mode         IN VARCHAR2) IS
    --
    g_je_source   gl_je_sources.je_source_name%TYPE := p_gl_source;
    g_je_category gl_je_categories.user_je_category_name%TYPE := p_gl_category;
    l_inserted    NUMBER := 0;
    ln_ledger_id  NUMBER := 2061; --HD Supply USD
    --
  
    e_skip EXCEPTION;
  
    CURSOR c_journals_iface IS
      SELECT status
            ,accounting_date
            ,currency_code
            ,SYSDATE             date_created
            ,fnd_global.user_id  created_by
            ,actual_flag
            ,g_je_category       user_je_category_name
            ,g_je_source         user_je_source_name
            ,product             segment1
            ,location            segment2
            ,cost_center         segment3
            ,account             segment4
            ,project_code        segment5
            ,future_use          segment6
            ,future_use2         segment7
            ,debit_amount        entered_dr
            ,credit_amount       entered_cr
            ,batch_number
            ,batch_description
            ,journal_name
            ,journal_description
            ,journal_line_desc
            ,reference_field1    reference1
            ,reference_field2    reference2
            ,reference_field3    reference4
            ,reference_field4    reference5
            ,reference_field5    reference10
            ,reference_field6    reference21
            ,reference_field7    reference22
            ,reference_field8    reference23
            ,reference_field9    reference24
            ,ROWID               stg_rowid
        FROM xxwc.xxwc_gl_ahh_journals_stg_tbl
       WHERE 1 = 1
         AND status = 'NEW';
  
    lvc_period_status   apps.gl_period_statuses.closing_status%TYPE;
    ln_err_count        NUMBER := 0;
    l_interfaced_cnt    NUMBER := 0;
    l_intf_err_cnt      NUMBER := 0;
    l_processed_cnt     NUMBER := 0;
    l_total_cnt         NUMBER := 0;
    lvc_conc_segs       gl_code_combinations_kfv.concatenated_segments%TYPE;
    lvc_err_msg         VARCHAR2(4000);
    ln_ora_acct         gl_code_combinations.segment4%TYPE;
    ln_structure_num    NUMBER;
    lvc_sec             VARCHAR2(32767);
    l_gl_data_access_id NUMBER;
    ln_request_id       NUMBER;
    ln_group_id         NUMBER;
    ln_int_run_id       NUMBER;
  
    ln_branch           xxwc.xxwc_ap_branch_list_stg_tbl.oracle_bw_number%TYPE;
    l_default_bw_number xxwc.xxwc_ap_branch_list_stg_tbl.oracle_bw_number%TYPE := 'BW080';
  
    l_log_msg VARCHAR2(1000);
  
    --
    l_application_short_name VARCHAR2(240) := 'SQLGL';
    l_key_flex_code          VARCHAR2(240) := 'GL#';
    l_validation_date        DATE := SYSDATE;
    n_segments               NUMBER := 7;
    segments                 apps.fnd_flex_ext.segmentarray;
    l_combination_id         NUMBER;
    l_data_set               NUMBER := NULL;
    l_return                 BOOLEAN;
    l_message                VARCHAR2(240);
    --
    l_batch_name   VARCHAR2(240);
    l_batch_desc   VARCHAR2(500);
    l_journal_name VARCHAR2(240);
    l_journal_desc VARCHAR2(500);
    e_vaidations_failed EXCEPTION;
  
    CURSOR c_batch_info IS
      SELECT batch_number
            ,SUM(tt.credit_amount) credit_amount
            ,SUM(tt.debit_amount) debit_amount
            ,COUNT(*) total_count
            ,MAX(tt.accounting_date) min_acc_date
            ,MIN(tt.accounting_date) max_acc_date
        FROM xxwc.xxwc_gl_ahh_journals_stg_tbl tt
       WHERE status = 'NEW'
       GROUP BY batch_number
               ,status
       ORDER BY 1;
    TYPE t_batch_info IS TABLE OF c_batch_info%ROWTYPE;
    lt_batch_info t_batch_info;
  
    CURSOR c_journal_info IS
      SELECT tt.journal_name
            ,SUM(tt.credit_amount) credit_amount
            ,SUM(tt.debit_amount) debit_amount
            ,COUNT(*) total_count
            ,MAX(tt.accounting_date) min_acc_date
            ,MIN(tt.accounting_date) max_acc_date
        FROM xxwc.xxwc_gl_ahh_journals_stg_tbl tt
       WHERE status = 'NEW'
       GROUP BY journal_name
       ORDER BY 1;
    TYPE t_journal_info IS TABLE OF c_journal_info%ROWTYPE;
    lt_journal_info t_journal_info;
  
  BEGIN
    lvc_sec := '01';
    fnd_file.put_line(fnd_file.log
                     ,'XXWC GL AHH Journals Import - Start');
    fnd_file.put_line(fnd_file.log
                     ,' -- ************************************');
    fnd_file.put_line(fnd_file.log
                     ,' -- p_gl_source:          ' || p_gl_source);
    fnd_file.put_line(fnd_file.log
                     ,' -- p_gl_category:        ' || p_gl_category);
    fnd_file.put_line(fnd_file.log
                     ,' -- p_default_acct:       ' || p_default_acct);
    fnd_file.put_line(fnd_file.log
                     ,' -- p_gl_import:          ' || p_gl_import);
    fnd_file.put_line(fnd_file.log
                     ,' -- p_mode:          ' || p_mode);
    fnd_file.put_line(fnd_file.log
                     ,' -- l_default_bw_number:  ' || l_default_bw_number);
    fnd_file.put_line(fnd_file.log
                     ,' -- ************************************');
    fnd_file.put_line(fnd_file.log
                     ,' -- Set Of Books ID: ' || ln_ledger_id);
  
    --
    ln_group_id := xxwc.xxwc_ahh_gl_import_grp_seq.nextval;
    fnd_file.put_line(fnd_file.log
                     ,' -- Group ID: ' || ln_group_id);
  
    -- Deriving Chart of account
    BEGIN
      lvc_sec := ' Deriving Chart Account';
      SELECT gls.chart_of_accounts_id
        INTO ln_structure_num
        FROM gl_sets_of_books gls
       WHERE gls.set_of_books_id = ln_ledger_id;
    EXCEPTION
      WHEN OTHERS THEN
        ln_structure_num := NULL;
    END;
  
    fnd_file.put_line(fnd_file.log
                     ,' -- Chart Of Accounts ID: ' || ln_structure_num);
  
    -- Deriving Batch Info
    BEGIN
      lvc_sec      := ' Update Batch Number';
      l_batch_name := 'AHH Interface Batch ' || to_char(SYSDATE
                                                       ,'MMDDYYYY') || ' - ' ||
                      fnd_global.conc_request_id;
      l_batch_desc := 'AHH Interface Batch ' || to_char(SYSDATE
                                                       ,'MMDDYYYY') || ' - ' ||
                      fnd_global.conc_request_id || 'Description';
      UPDATE xxwc.xxwc_gl_ahh_journals_stg_tbl
         SET batch_number      = l_batch_name
            ,batch_description = l_batch_desc
       WHERE status = 'NEW'
         AND batch_number IS NULL;
    
      COMMIT;
      fnd_file.put_line(fnd_file.log
                       ,' *** Batch info update done: ' || l_batch_name || ', ' || SQL%ROWCOUNT ||
                        ' ***');
    EXCEPTION
      WHEN OTHERS THEN
        ROLLBACK;
        fnd_file.put_line(fnd_file.log
                         ,' *** unknown error updating batch info: error:' ||
                          substr(SQLERRM
                                ,1
                                ,100) || '***');
        RAISE;
    END;
  
    IF p_mode = 'CONVERSION' THEN
      BEGIN
        lvc_sec        := ' Update Journal Number';
        l_journal_name := 'AHH Interface Journal ' ||
                          to_char(SYSDATE
                                 ,'MMDDYYYY') || ' - ' || fnd_global.conc_request_id;
        l_journal_desc := 'AHH Interface Journal ' ||
                          to_char(SYSDATE
                                 ,'MMDDYYYY') || ' - ' || fnd_global.conc_request_id ||
                          'Description';
        UPDATE xxwc.xxwc_gl_ahh_journals_stg_tbl
           SET journal_name        = l_batch_name
              ,journal_description = l_batch_desc
              ,journal_line_desc   = 'Line - ' || l_journal_name
         WHERE status = 'NEW'
           AND journal_name IS NULL;
      
        fnd_file.put_line(fnd_file.log
                         ,' *** Journal info update done: ' || l_journal_name || ', ' ||
                          SQL%ROWCOUNT || ' ***');
        COMMIT;
      EXCEPTION
        WHEN OTHERS THEN
          ROLLBACK;
          fnd_file.put_line(fnd_file.log
                           ,' *** unknown error updating Journal info: error:' ||
                            substr(SQLERRM
                                  ,1
                                  ,100) || '***');
          RAISE;
      END;
    END IF;
  
    -- Validations
    <<validations>>
    BEGIN
      fnd_file.put_line(fnd_file.log
                       ,' *** -----------Validations----------------- ***');
      --
      lvc_sec := 'Check Batch Level amounts';
      OPEN c_batch_info;
      FETCH c_batch_info BULK COLLECT
        INTO lt_batch_info;
      CLOSE c_batch_info;
    
      FOR br IN 1 .. lt_batch_info.count LOOP
        IF lt_batch_info(br).credit_amount <> lt_batch_info(br).debit_amount THEN
          fnd_file.put_line(fnd_file.log
                           ,' *** Journal Batch Amounts did not match: ' ||
                            rpad('Batch#'
                                ,45
                                ,'-') || '|' || rpad('Credit Total'
                                                    ,15
                                                    ,' ') || '|' ||
                            rpad('Debit Total'
                                ,15
                                ,' ') || '|' || rpad('Max AccDate'
                                                    ,15
                                                    ,' ') || '|' ||
                            rpad('Min AccDate'
                                ,15
                                ,' ') || '|' || rpad('RowCount'
                                                    ,15
                                                    ,' '));
          fnd_file.put_line(fnd_file.log
                           ,' ***                                    : ' ||
                            rpad(lt_batch_info(br).batch_number
                                ,45
                                ,' ') || '|' || rpad(lt_batch_info(br).credit_amount
                                                    ,15
                                                    ,' ') || '|' ||
                            rpad(lt_batch_info(br).debit_amount
                                ,15
                                ,' ') || '|' || rpad(lt_batch_info(br).min_acc_date
                                                    ,15
                                                    ,' ') || '|' ||
                            rpad(lt_batch_info(br).min_acc_date
                                ,15
                                ,' ') || '|' || rpad(lt_batch_info(br).total_count
                                                    ,15
                                                    ,' ') || ' ***');
          RAISE e_vaidations_failed;
        
        ELSE
          fnd_file.put_line(fnd_file.log
                           ,' *** Journal Batch Amounts Info: ' ||
                            rpad('Batch#'
                                ,45
                                ,' ') || '|' || rpad('Credit Total'
                                                    ,15
                                                    ,' ') || '|' ||
                            rpad('Debit Total'
                                ,15
                                ,' ') || '|' || rpad('Max AccDate'
                                                    ,15
                                                    ,' ') || '|' ||
                            rpad('Min AccDate'
                                ,15
                                ,' ') || '|' || rpad('RowCount'
                                                    ,15
                                                    ,' '));
          fnd_file.put_line(fnd_file.log
                           ,' ***                           : ' ||
                            rpad(lt_batch_info(br).batch_number
                                ,45
                                ,' ') || '|' || rpad(lt_batch_info(br).credit_amount
                                                    ,15
                                                    ,' ') || '|' ||
                            rpad(lt_batch_info(br).debit_amount
                                ,15
                                ,' ') || '|' || rpad(lt_batch_info(br).min_acc_date
                                                    ,15
                                                    ,' ') || '|' ||
                            rpad(lt_batch_info(br).min_acc_date
                                ,15
                                ,' ') || '|' || rpad(lt_batch_info(br).total_count
                                                    ,15
                                                    ,' ') || ' ***');
        END IF;
      
      END LOOP;
      --
    
    
      -- Check Journal Level amounts.
      lvc_sec := 'Check Journal Level amounts';
      OPEN c_journal_info;
      FETCH c_journal_info BULK COLLECT
        INTO lt_journal_info;
      CLOSE c_journal_info;
    
      FOR jr IN 1 .. lt_journal_info.count LOOP
        IF lt_journal_info(jr).credit_amount <> lt_journal_info(jr).debit_amount THEN
          /*fnd_file.put_line(fnd_file.log
          , ' *** Journal Amounts did not match: Journal | Credit Total | Debit Total | Max AccDate | Min AccDate | RowCount' || lt_journal_info(jr)
           .journal_name || '|' || lt_journal_info(jr).credit_amount || '|' || lt_journal_info(jr)
           .debit_amount || '|' || lt_journal_info(jr).min_acc_date || '|' || lt_journal_info(jr)
           .min_acc_date || '|' || lt_journal_info(jr).total_count || ' ***');*/
          fnd_file.put_line(fnd_file.log
                           ,' *** Journal Amounts DID NOT MATCH: ' ||
                            rpad('JournalName'
                                ,45
                                ,' ') || '|' || rpad('Credit Total'
                                                    ,15
                                                    ,' ') || '|' ||
                            rpad('Debit Total'
                                ,15
                                ,' ') || '|' || rpad('Max AccDate'
                                                    ,15
                                                    ,' ') || '|' ||
                            rpad('Min AccDate'
                                ,15
                                ,' ') || '|' || rpad('RowCount'
                                                    ,15
                                                    ,' '));
          fnd_file.put_line(fnd_file.log
                           ,' ***                           : ' ||
                            rpad(lt_journal_info(jr).journal_name
                                ,45
                                ,' ') || '|' || rpad(lt_journal_info(jr).credit_amount
                                                    ,15
                                                    ,' ') || '|' ||
                            rpad(lt_journal_info(jr).debit_amount
                                ,15
                                ,' ') || '|' || rpad(lt_journal_info(jr).min_acc_date
                                                    ,15
                                                    ,' ') || '|' ||
                            rpad(lt_journal_info(jr).min_acc_date
                                ,15
                                ,' ') || '|' || rpad(lt_journal_info(jr).total_count
                                                    ,15
                                                    ,' ') || ' ***');
          RAISE e_vaidations_failed;
        
        END IF;
      
        IF lt_journal_info(jr).journal_name IS NULL THEN
          fnd_file.put_line(fnd_file.log
                           ,' *** Journal Name Not Available: ' ||
                            rpad('Journal'
                                ,45
                                ,' ') || '|' || rpad('Credit Total'
                                                    ,15
                                                    ,' ') || '|' ||
                            rpad('Debit Total'
                                ,15
                                ,' ') || '|' || rpad('Max AccDate'
                                                    ,15
                                                    ,' ') || '|' ||
                            rpad('Min AccDate'
                                ,15
                                ,' ') || '|' || rpad('RowCount'
                                                    ,15
                                                    ,' '));
          fnd_file.put_line(fnd_file.log
                           ,' ***                           : ' ||
                            rpad(lt_journal_info(jr).journal_name
                                ,45
                                ,' ') || '|' || rpad(lt_journal_info(jr).credit_amount
                                                    ,15
                                                    ,' ') || '|' ||
                            rpad(lt_journal_info(jr).debit_amount
                                ,15
                                ,' ') || '|' || rpad(lt_journal_info(jr).min_acc_date
                                                    ,15
                                                    ,' ') || '|' ||
                            rpad(lt_journal_info(jr).min_acc_date
                                ,15
                                ,' ') || '|' || rpad(lt_journal_info(jr).total_count
                                                    ,15
                                                    ,' ') || ' ***');
          RAISE e_vaidations_failed;
        END IF;
      
      END LOOP;
      --
    
      fnd_file.put_line(fnd_file.log
                       ,' *** -----------Validations----------------- ***');
    EXCEPTION
      WHEN OTHERS THEN
        fnd_file.put_line(fnd_file.log
                         ,' *** Unknown error validations info: section: ' || lvc_sec ||
                          ', error:' || substr(SQLERRM
                                              ,1
                                              ,100) || '***');
        RAISE;
    END validations;
  
  
  
    -- Start records processing
    fnd_file.put_line(fnd_file.log
                     ,'--------------------------------------------------------------------------- ');
    fnd_file.put_line(fnd_file.log
                     ,' *** Start Processing Staging Table Records: ***');
  
    fnd_file.put_line(fnd_file.log
                     ,' *** --------------------------------------- ***');
  
    lvc_sec := 'Retriving Data from Staging table';
    FOR rec IN c_journals_iface LOOP
    
      BEGIN
        l_total_cnt       := l_total_cnt + 1;
        lvc_err_msg       := NULL;
        lvc_period_status := NULL;
        l_log_msg         := NULL;
        ln_ora_acct       := NULL;
        ln_branch         := NULL;
      
        l_log_msg := rec.segment1 || '.' || rec.segment2 || '.' || rec
                    .segment3 || '.' || rec.segment4 || '.' || rec
                    .segment5 || '.' || rec.segment6 || '.' || rec.segment7 || ' ->';
      
      
      
        -- Accounting date validation.
        lvc_sec := 'Validating Period';
        BEGIN
          SELECT 'Y'
            INTO lvc_period_status
            FROM apps.gl_period_statuses
           WHERE rec.accounting_date BETWEEN start_date AND end_date
             AND application_id = 101
             AND set_of_books_id = ln_ledger_id
             AND closing_status = 'O';
        EXCEPTION
          WHEN no_data_found THEN
            lvc_period_status := 'N';
            lvc_err_msg       := 'Accounting date is not in Open Period - date -' ||
                                 rec.accounting_date;
            l_log_msg         := l_log_msg || 'PrdSts-E';
          WHEN OTHERS THEN
            lvc_period_status := 'N';
            lvc_err_msg       := 'Accounting date in Open Period Verification-err-' || SQLERRM;
            l_log_msg         := l_log_msg || 'PrdSts-E';
        END;
      
        IF lvc_period_status = 'N' THEN
          lvc_sec := 'Updating Accounting Date Status in Staging table';
        
          UPDATE xxwc.xxwc_gl_ahh_journals_stg_tbl
             SET error_message = lvc_err_msg
                ,status        = 'ERROR'
           WHERE ROWID = rec.stg_rowid;
        
          ln_err_count := ln_err_count + 1;
          RAISE e_skip;
        ELSE
          l_log_msg := l_log_msg || '| ' || 'PrdSts-Y ';
        END IF;
      
        -- Deriving Oracle Mapping Account
        lvc_sec := 'Deriving Mapping Account';
        BEGIN
          SELECT oracle_gl_account
            INTO ln_ora_acct
            FROM xxwc.xxwc_ap_gl_map_stg_tbl
           WHERE ahh_gl_account = rec.segment4;
        
          -- Default Acct when NULL
          IF TRIM(ln_ora_acct) IS NULL THEN
            ln_ora_acct := p_default_acct;
          END IF;
        
        EXCEPTION
          WHEN no_data_found THEN
            ln_ora_acct := p_default_acct;
          WHEN OTHERS THEN
            l_log_msg    := l_log_msg || '| ' || 'GLAcc-E';
            ln_err_count := ln_err_count + 1;
            lvc_err_msg  := 'Error occured while deriving mapping acct ' ||
                            substr(SQLERRM
                                  ,1
                                  ,250);
          
            UPDATE xxwc.xxwc_gl_ahh_journals_stg_tbl
               SET error_message = lvc_err_msg
                  ,status        = 'ERROR'
             WHERE ROWID = rec.stg_rowid;
          
            RAISE e_skip;
        END;
        l_log_msg := l_log_msg || '| ' || 'GLAcc: ' || ln_ora_acct;
      
        -- Deriving Oracle Location
        BEGIN
        
          SELECT br.oracle_bw_number
            INTO ln_branch
            FROM xxwc.xxwc_ap_branch_list_stg_tbl br
           WHERE br.ahh_branch_number = ltrim(rec.segment2
                                             ,'0');
          --
          IF TRIM(ln_branch) IS NULL THEN
            ln_branch := l_default_bw_number;
          END IF;
          --
        EXCEPTION
          WHEN no_data_found THEN
            ln_branch := l_default_bw_number;
          WHEN OTHERS THEN
            ln_err_count := ln_err_count + 1;
            l_log_msg    := l_log_msg || '| ' || 'Br-E';
            lvc_err_msg  := 'Error occured while deriving Branch ' ||
                            substr(SQLERRM
                                  ,1
                                  ,250);
            UPDATE xxwc.xxwc_gl_ahh_journals_stg_tbl
               SET error_message = lvc_err_msg
                  ,status        = 'ERROR'
             WHERE ROWID = rec.stg_rowid;
          
            RAISE e_skip;
        END;
        l_log_msg := l_log_msg || '| ' || 'Br: ' || ln_branch;
      
        -- Code combination validation
        lvc_sec := 'Deriving Concatenated Segs';
      
        lvc_conc_segs := rec.segment1 || '.' || ln_branch || '.' || rec.segment3 || '.' ||
                         ln_ora_acct || '.' || rec.segment5 || '.' || rec
                        .segment6 || '.' || rec.segment7;
      
        l_log_msg := l_log_msg || '| ' || lvc_conc_segs;
      
        lvc_sec := 'Deriving Code Combination id';
        BEGIN
        
          --
          segments(1) := rec.segment1;
          segments(2) := ln_branch;
          segments(3) := rec.segment3;
          segments(4) := ln_ora_acct;
          segments(5) := rec.segment5;
          segments(6) := rec.segment6;
          segments(7) := rec.segment7;
          --
        
          l_return  := fnd_flex_ext.get_combination_id(application_short_name => l_application_short_name
                                                      ,key_flex_code          => l_key_flex_code
                                                      ,structure_number       => ln_structure_num
                                                      ,validation_date        => l_validation_date
                                                      ,n_segments             => n_segments
                                                      ,segments               => segments
                                                      ,combination_id         => l_combination_id
                                                      ,data_set               => l_data_set);
          l_message := fnd_flex_ext.get_message;
          IF l_return THEN
            l_log_msg := l_log_msg || '| ccid:' || l_combination_id;
            l_log_msg := l_log_msg || '| ' || 'CcId-Y';
          
            dbms_output.put_line('l_Return = TRUE');
            dbms_output.put_line('COMBINATION_ID = ' || l_combination_id);
          ELSE
          
          
            IF substr(ln_ora_acct
                     ,1
                     ,1) = '9' THEN
              --
              ln_branch := 'BW080';
              segments(1) := rec.segment1;
              segments(2) := ln_branch;
              segments(3) := rec.segment3;
              segments(4) := ln_ora_acct;
              segments(5) := rec.segment5;
              segments(6) := rec.segment6;
              segments(7) := rec.segment7;
              --
              l_return  := fnd_flex_ext.get_combination_id(application_short_name => l_application_short_name
                                                          ,key_flex_code          => l_key_flex_code
                                                          ,structure_number       => ln_structure_num
                                                          ,validation_date        => l_validation_date
                                                          ,n_segments             => n_segments
                                                          ,segments               => segments
                                                          ,combination_id         => l_combination_id
                                                          ,data_set               => l_data_set);
              l_message := fnd_flex_ext.get_message;
              IF l_return THEN
                l_log_msg := l_log_msg || '| ccid:' || l_combination_id;
                l_log_msg := l_log_msg || '| ' || 'CcIdR-Y';
              
              ELSE
                l_log_msg := l_log_msg || '| ' || 'CcId-F-' || l_message;
              
                UPDATE xxwc.xxwc_gl_ahh_journals_stg_tbl
                   SET error_message = 'Code Combination ID validation failed: ' || l_message
                      ,status        = 'ERROR'
                 WHERE ROWID = rec.stg_rowid;
                --
                IF nvl(rec.entered_dr
                      ,0) <> 0
                   OR nvl(rec.entered_cr
                         ,0) <> 0 THEN
                  ln_err_count := ln_err_count + 1;
                END IF;
                --
                RAISE e_skip;
              END IF;
            
            ELSE
              --- Not 9XXXXX
              l_log_msg := l_log_msg || '| ' || 'CcId-F-' || l_message;
              UPDATE xxwc.xxwc_gl_ahh_journals_stg_tbl
                 SET error_message = 'Code Combination ID validation failed: ' || l_message
                    ,status        = 'ERROR'
               WHERE ROWID = rec.stg_rowid;
              --
              IF nvl(rec.entered_dr
                    ,0) <> 0
                 OR nvl(rec.entered_cr
                       ,0) <> 0 THEN
                ln_err_count := ln_err_count + 1;
              END IF;
              --
              RAISE e_skip;
            END IF;
          
          END IF;
        
        EXCEPTION
          WHEN OTHERS THEN
            l_log_msg   := l_log_msg || '| ' || 'CcId-E';
            lvc_err_msg := 'Unknown Error: ' || lvc_sec || '-' || SQLERRM;
          
            UPDATE xxwc.xxwc_gl_ahh_journals_stg_tbl
               SET error_message = lvc_err_msg
                  ,status        = 'ERROR'
             WHERE ROWID = rec.stg_rowid;
          
            ln_err_count := ln_err_count + 1;
            RAISE e_skip;
        END;
      
        UPDATE xxwc.xxwc_gl_ahh_journals_stg_tbl
           SET status = 'PROCESSED'
         WHERE 1 = 1
           AND ROWID = rec.stg_rowid;
      
        IF p_gl_import = 'Y' THEN
          BEGIN
            lvc_sec := 'Inserting Data into Interface';
          
            INSERT INTO apps.gl_interface
              (status
              ,ledger_id
              ,accounting_date
              ,currency_code
              ,date_created
              ,created_by
              ,actual_flag
              ,user_je_category_name
              ,user_je_source_name
              ,segment1
              ,segment2
              ,segment3
              ,segment4
              ,segment5
              ,segment6
              ,segment7
              ,entered_dr
              ,entered_cr
              ,reference1
              ,reference2
              ,reference4
              ,reference5
              ,reference10
              ,reference21
              ,reference22
              ,reference23
              ,reference24
              ,group_id)
            VALUES
              (rec.status
              ,ln_ledger_id
              ,rec.accounting_date
              ,rec.currency_code
              ,rec.date_created
              ,rec.created_by
              ,rec.actual_flag
              ,rec.user_je_category_name
              ,rec.user_je_source_name
              ,rec.segment1
              ,ln_branch --rec.segment2
              ,rec.segment3
              ,ln_ora_acct
              ,rec.segment5
              ,rec.segment6
              ,rec.segment7
              ,rec.entered_dr
              ,rec.entered_cr
              ,rec.batch_number
              ,rec.batch_description
              ,rec.journal_name
              ,rec.journal_description
              ,rec.journal_line_desc
              ,rec.reference21
              ,rec.reference22
              ,rec.reference23
              ,rec.reference24
              ,ln_group_id);
          
            --
            l_inserted := l_inserted + SQL%ROWCOUNT;
          
            lvc_sec := 'Updating Status in Stating table';
          
            UPDATE xxwc.xxwc_gl_ahh_journals_stg_tbl
               SET status = 'INTERFACED'
             WHERE 1 = 1
               AND ROWID = rec.stg_rowid;
            --
            l_log_msg := l_log_msg || '| ' || 'Intf-Y';
            --
            l_interfaced_cnt := l_interfaced_cnt + 1;
            --
          
          EXCEPTION
            WHEN OTHERS THEN
              l_intf_err_cnt := l_intf_err_cnt + 1;
              l_log_msg      := l_log_msg || '| ' || 'Intf-E';
              lvc_err_msg    := 'In GL Interface: ' || SQLERRM;
              UPDATE xxwc.xxwc_gl_ahh_journals_stg_tbl
                 SET status        = 'ERROR'
                    ,error_message = lvc_err_msg
               WHERE 1 = 1
                 AND ROWID = rec.stg_rowid;
            
              RAISE e_skip;
          END;
        
        END IF;
        --
      
        fnd_file.put_line(fnd_file.log
                         ,l_log_msg);
        l_log_msg := NULL;
        --
        l_processed_cnt := l_processed_cnt + 1;
      
      EXCEPTION
        WHEN e_skip THEN
        
          fnd_file.put_line(fnd_file.log
                           ,l_log_msg || '-Skipped, Error:' || lvc_err_msg);
          l_log_msg := NULL;
        
        WHEN OTHERS THEN
          fnd_file.put_line(fnd_file.log
                           ,l_log_msg || lvc_sec || '(sec) - Unknown Error Occured ' || SQLERRM);
        
      END;
    END LOOP;
  
    fnd_file.put_line(fnd_file.log
                     ,' *** --------------------------------------- ***');
    fnd_file.put_line(fnd_file.log
                     ,' *** End Processing Staging Table Records: ***');
  
    fnd_file.put_line(fnd_file.log
                     ,'--------------------------------------------------------------------------- ');
  
    fnd_file.put_line(fnd_file.log
                     ,' *** Total Lines Count:      ' || l_total_cnt);
    fnd_file.put_line(fnd_file.log
                     ,' *** Processed Count:        ' || l_processed_cnt);
    fnd_file.put_line(fnd_file.log
                     ,' *** Errored Count(Valid):   ' || ln_err_count);
    fnd_file.put_line(fnd_file.log
                     ,' *** Interfaced Count:       ' || l_interfaced_cnt);
    fnd_file.put_line(fnd_file.log
                     ,' *** Interface Errored Count:' || l_intf_err_cnt);
  
    fnd_file.put_line(fnd_file.log
                     ,'--------------------------------------------------------------------------- ');
  
    IF l_inserted > 0
       AND substr(p_gl_import
                 ,1
                 ,1) = 'Y'
       AND nvl(ln_err_count
              ,0) = 0 THEN
    
      SELECT gl_journal_import_s.nextval
        INTO ln_int_run_id
        FROM dual;
    
    
      SELECT profile_value
        INTO l_gl_data_access_id
        FROM apps.xxcus_display_profile_values_v
       WHERE 1 = 1
         AND level_set = 'Responsibility'
         AND profile_name = 'GL: Data Access Set'
         AND CONTEXT = (SELECT responsibility_name
                          FROM apps.fnd_responsibility_tl
                         WHERE 1 = 1
                           AND responsibility_id = fnd_global.resp_id
                           AND LANGUAGE = 'US');
    
      INSERT INTO gl_interface_control
        (je_source_name
        ,interface_run_id
        ,status
        ,set_of_books_id
        ,group_id)
      VALUES
        (p_gl_source
        ,ln_int_run_id
        ,'S'
        ,ln_ledger_id
        ,ln_group_id);
    
      --
      COMMIT;
      --
      ln_request_id := fnd_request.submit_request(application => 'SQLGL'
                                                 ,program     => 'GLLEZL'
                                                 ,description => ''
                                                 ,start_time  => ''
                                                 ,sub_request => FALSE
                                                 ,argument1   => ln_int_run_id
                                                 ,argument2   => l_gl_data_access_id
                                                 ,argument3   => 'N' --post to suspense
                                                 ,argument4   => NULL --from date
                                                 ,argument5   => NULL --to date
                                                 ,argument6   => 'N' --summary mode
                                                 ,argument7   => 'N' --import DFF
                                                 ,argument8   => 'Y' --backward mode
                                                  );
    
      --
      COMMIT;
      --
    
      fnd_file.put_line(fnd_file.log
                       ,'Request id:' || ln_request_id);
      fnd_file.put_line(fnd_file.log
                       ,'Journal Import submitted for group' || ln_group_id || ' Request id:' ||
                        ln_request_id);
    ELSE
      fnd_file.put_line(fnd_file.log
                       ,'*** Skipping Journal Import Interface Concurrent Program*** --> ' ||
                        ' #OfRows :' || l_inserted || ', Import Flag: ' || p_gl_import ||
                        ', #Of Errs:' || nvl(ln_err_count
                                            ,0));
      x_retcode := 1;
      x_errbuf  := 'Skipping Journal import interface program';
    END IF;
  
  
    COMMIT;
    --
    fnd_file.put_line(fnd_file.log
                     ,'--------------------------------------------------------------------------- ');
  
    fnd_file.put_line(fnd_file.log
                     ,'XXWC GL AHH Journals Import - End');
  EXCEPTION
    WHEN e_vaidations_failed THEN
      fnd_file.put_line(fnd_file.log
                       ,'*** Skipping Journal Import Interface Concurrent Program*** --> ' ||
                        ' One of more Validations Failed');
      x_retcode := 1;
      x_errbuf  := 'Skipping Journal import interface program';
    
  
    WHEN OTHERS THEN
      fnd_file.put_line(fnd_file.log
                       ,'Error Occured ' || SQLERRM);
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => g_err_callfrom || '.' || ' MAIN'
                                          ,p_calling           => lvc_sec
                                          ,p_ora_error_msg     => substr(SQLERRM
                                                                        ,1
                                                                        ,250)
                                          ,p_error_desc        => lvc_err_msg
                                          ,p_distribution_list => g_distro_list
                                          ,p_module            => 'XXWC');
      fnd_file.put_line(fnd_file.log
                       ,'--------------------------------------------------------------------------- ');
      fnd_file.put_line(fnd_file.log
                       ,'XXWC GL AHH Journals Import - Aborted');
    
    
      x_retcode := 2;
      x_errbuf  := SQLERRM;
  END;
  --
END xxwc_gl_ahh_intf_pkg;
/
