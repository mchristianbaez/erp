/* Formatted on 7/16/2014 11:13:55 AM (QP5 v5.254.13202.43190) */
CREATE OR REPLACE PACKAGE BODY APPS.XXWC_PO_RTV_PKG
AS
   /*************************************************************************
     $Header XXWC_PO_RTV_PKG.pkb $
     Module Name: XXWC_PO_RTV_PKG

     PURPOSE: Package to do the database activities for the RTV Improvements
     ESMS Task Id :  20130911-00538

     REVISIONS:
     Ver        Date        Author                Description
     ---------  ----------  ------------------    ----------------
     1.0        04/21/2014  Manjula Chellappan    Initial Version
	 2.0        11/07/2014  Vijaysrinivasan       TMS#20141002-00050 Multi org changes 
	**************************************************************************/
   g_start               NUMBER;
   g_distribution_list   fnd_user.email_address%TYPE
                            := 'HDSOracleDevelopers@hdsupply.com';

   PROCEDURE confirm_rtv (p_rtv_header_id   IN     NUMBER,
                          p_user_id         IN     NUMBER,
                          p_result             OUT BOOLEAN,
                          p_error_message      OUT VARCHAR2)
   /*************************************************************************
     $Header confirm_rtv $
     Module Name: confirm_rtv

     PURPOSE: Procedure to do RTV confirmation via miscellaneous Transaction
     ESMS Task Id :  20130911-00538

     REVISIONS:
     Ver        Date        Author                Description
     ---------  ----------  ------------------    ----------------
     1.0        04/21/2014  Manjula Chellappan    Initial Version

   **************************************************************************/
   IS
      l_transaction_type_name        VARCHAR2 (50) := 'WC Return to Vendor';
      l_sec                          VARCHAR2 (100);
      l_transaction_source           VARCHAR2 (50) := 'White Cap Return to Vendor';
      l_transaction_type_id          NUMBER;
      l_transaction_source_type_id   NUMBER;
      l_transaction_source_id        NUMBER;
      l_transaction_source_code      VARCHAR2 (40);
      l_process_flag                 NUMBER := 1;
      l_transaction_mode             NUMBER := 2;
      l_lock_flag                    NUMBER := 3;
      l_sysdate                      DATE := SYSDATE;
      l_result                       BOOLEAN;
      l_start                        BOOLEAN;
      l_err_code                     VARCHAR2 (100);
      l_err_desc                     VARCHAR2 (4000);
      l_rtv_status                   VARCHAR2 (50) := 'Confirmed';
      l_err_msg                      VARCHAR2 (4000);
      l_rtv_status_code              VARCHAR2 (40);
      l_function_name                VARCHAR2 (50) := 'RCV_RCVTXERE';


      CURSOR cur_rtv_txn
      IS
         SELECT rtvh.rtv_header_id RTV_HEADER_ID,
                rtvh.ship_from_branch_id SHIP_FROM_BRANCH_ID,
                rtvh.buyer_id BUYER_ID,
                rtvh.supplier_id SUPPLIER_ID,
                (SELECT segment1
                   FROM ap_suppliers
                  WHERE vendor_id = rtvh.supplier_id)
                   SUPPLIER_NUMBER,
                rtvh.supplier_site_id,
                (SELECT vendor_site_code
             --Modified the below line for TMS ## 20141002-00050  by VijaySrinivasan  on 11/07/2014 
              FROM ap_supplier_sites
                  WHERE vendor_site_id = rtvh.supplier_site_id)
                   SUPPLIER_SITE_CODE,
                rtvh.po_header_id PO_HEADER_ID,
                rtvh.return_number RETURN_NUMBER,
                rtvh.wc_return_number WC_RETURN_NUMBER,
                rtvh.supplier_address SUPPLIER_ADDRESS,
                rtvh.restocking_fee RESTOCKING_FEE,
                rtvh.rtv_status_code RTV_STATUS_CODE,
                rtvh.created_by HEADER_CREATED_BY,
                rtvh.creation_date HEADER_CREATION_DATE,
                rtvh.last_updated_by HEADER_LAST_UPDATED_BY,
                rtvh.last_update_date HEADER_LAST_UPDATE_DATE,
                rtvh.last_update_login HEADER_LAST_UPDATE_LOGIN,
                rtvh.attribute1 HEADER_ATTRIBUTE1,
                rtvh.attribute2 HEADER_ATTRIBUTE2,
                rtvh.attribute3 HEADER_ATTRIBUTE3,
                rtvh.attribute4 HEADER_ATTRIBUTE4,
                rtvh.attribute5 HEADER_ATTRIBUTE5,
                rtvh.attribute6 HEADER_ATTRIBUTE6,
                rtvh.attribute7 HEADER_ATTRIBUTE7,
                rtvh.attribute8 HEADER_ATTRIBUTE8,
                rtvh.attribute9 HEADER_ATTRIBUTE9,
                rtvh.attribute10 HEADER_ATTRIBUTE10,
                rtvh.attribute11 HEADER_ATTRIBUTE11,
                rtvh.attribute12 HEADER_ATTRIBUTE12,
                rtvh.attribute13 HEADER_ATTRIBUTE13,
                rtvh.attribute14 HEADER_ATTRIBUTE14,
                rtvh.attribute15 HEADER_ATTRIBUTE15,
                rtvl.rtv_line_id RTV_LINE_ID,
                rtvl.line_number LINE_NUMBER,
                rtvl.inventory_item_id INVENTORY_ITEM_ID,
                rtvl.subinventory SUBINVENTORY,
                rtvl.return_quantity RETURN_QUANTITY,
                rtvl.uom_code UOM_CODE,
                rtvl.return_unit_price RETURN_UNIT_PRICE,
                rtvl.return_reason RETURN_REASON,
                rtvl.notes NOTES,
                rtvl.processed_flag PROCESSED_FLAG,
                rtvl.created_by LINE_CREATED_BY,
                rtvl.creation_date LINE_CREATION_DATE,
                rtvl.last_updated_by LINE_LAST_UPDATED_BY,
                rtvl.last_update_date LINE_LAST_UPDATE_DATE,
                rtvl.last_update_login LINE_LAST_UPDATE_LOGIN,
                rtvl.attribute1 LINE_ATTRIBUTE1,
                rtvl.attribute2 LINE_ATTRIBUTE2,
                rtvl.attribute3 LINE_ATTRIBUTE3,
                rtvl.attribute4 LINE_ATTRIBUTE4,
                rtvl.attribute5 LINE_ATTRIBUTE5,
                rtvl.attribute6 LINE_ATTRIBUTE6,
                rtvl.attribute7 LINE_ATTRIBUTE7,
                rtvl.attribute8 LINE_ATTRIBUTE8,
                rtvl.attribute9 LINE_ATTRIBUTE9,
                rtvl.attribute10 LINE_ATTRIBUTE10,
                rtvl.attribute11 LINE_ATTRIBUTE11,
                rtvl.attribute12 LINE_ATTRIBUTE12,
                rtvl.attribute13 LINE_ATTRIBUTE13,
                rtvl.attribute14 LINE_ATTRIBUTE14,
                rtvl.attribute15 LINE_ATTRIBUTE15
           FROM 
             --Modified the below line for TMS ## 20141002-00050  by VijaySrinivasan  on 11/07/2014 
                xxwc_po_rtv_headers_tbl rtvh,
              --Modified the below line for TMS ## 20141002-00050  by VijaySrinivasan  on 11/07/2014 
               xxwc_po_rtv_lines_tbl rtvl
          WHERE     rtvh.rtv_header_id = rtvl.rtv_header_id
                AND rtvh.rtv_status_code = 'Pending'
                AND rtvh.rtv_header_id = p_rtv_header_id;
   BEGIN
   
      l_sec := 'Check Eligibility to confirm RTV ';

      --l_function_name := 'RCV_RCVTXERE' for the standard Returns form using which Return is performed
      
      IF FND_FUNCTION.test (l_function_name)
      THEN
         BEGIN
            l_sec := 'Get Transaction Type';
            xxwc_common_tunning_helpers.elapsed_time ('RTV CONFIRM',
                                                      l_sec,
                                                      g_start);

            SELECT transaction_type_id, transaction_source_type_id
              INTO l_transaction_type_id, l_transaction_source_type_id
              FROM mtl_transaction_types
             WHERE transaction_type_name = l_transaction_type_name;

            BEGIN
               l_sec := 'Get Transaction Source';
               xxwc_common_tunning_helpers.elapsed_time ('RTV CONFIRM',
                                                         l_sec,
                                                         g_start);

               SELECT disposition_id, segment1
                 INTO l_transaction_source_id, l_transaction_source_code
                 FROM MTL_GENERIC_DISPOSITIONS a,
             --Modified the below line for TMS ## 20141002-00050  by VijaySrinivasan  on 11/07/2014 
                      xxwc_po_rtv_headers_tbl b
                WHERE     description = l_transaction_source
                      AND a.organization_id = b.ship_from_branch_id
                      AND b.rtv_header_id = p_rtv_header_id;
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_err_msg := l_err_msg || ' ' || l_sec || ' ' || SQLERRM;
                  DBMS_OUTPUT.put_line ('Exception : ' || l_err_msg);
            END;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_err_msg := l_err_msg || ' ' || l_sec || ' ' || SQLERRM;
               DBMS_OUTPUT.put_line ('Exception : ' || l_err_msg);
         END;

         l_sec := 'Insert into Interface';

         xxwc_common_tunning_helpers.elapsed_time ('RTV CONFIRM',
                                                   l_sec,
                                                   g_start);

         FOR rec_rtv_txn IN cur_rtv_txn
         LOOP
            BEGIN
               INSERT
                 INTO mtl_transactions_interface (transaction_header_id,
                                                  transaction_type_id,
                                                  transaction_source_type_id,
                                                  transaction_source_id,
                                                  source_code,
                                                  process_flag,
                                                  transaction_mode,
                                                  lock_flag,
                                                  created_by,
                                                  creation_date,
                                                  last_updated_by,
                                                  last_update_date,
                                                  source_header_id,
                                                  source_line_id,
                                                  inventory_item_id,
                                                  organization_id,
                                                  transaction_quantity,
                                                  transaction_uom,
                                                  transaction_date,
                                                  transaction_cost,
                                                  subinventory_code,
                                                  attribute_category,
                                                  attribute1,
                                                  attribute2,
                                                  attribute3,
                                                  attribute4,
                                                  attribute5,
                                                  --Added Attribute6 on 9-Jul-14 to populate WC Return Number to Miscellaneous transactions DFF
                                                  attribute6)
               VALUES (p_rtv_header_id,
                       l_transaction_type_id,
                       l_transaction_source_type_id,
                       l_transaction_source_id,
                       l_transaction_source_code,
                       l_process_flag,
                       l_transaction_mode,
                       l_lock_flag,
                       p_user_id,
                       l_sysdate,
                       p_user_id,
                       l_sysdate,
                       rec_rtv_txn.rtv_header_id,
                       rec_rtv_txn.rtv_line_id,
                       rec_rtv_txn.inventory_item_id,
                       rec_rtv_txn.ship_from_branch_id,
                       -rec_rtv_txn.return_quantity,
                       rec_rtv_txn.uom_code,
                       l_sysdate,
                       rec_rtv_txn.return_unit_price,
                       rec_rtv_txn.subinventory,
                       'WC Return to Vendor',
                       rec_rtv_txn.supplier_number,
                       rec_rtv_txn.return_number,
                       rec_rtv_txn.return_unit_price,
                       rec_rtv_txn.restocking_fee,
                       rec_rtv_txn.supplier_site_id,
                       rec_rtv_txn.wc_return_number);
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_err_msg := l_err_msg || ' ' || l_sec || ' ' || SQLERRM;
                  DBMS_OUTPUT.put_line ('Exception : ' || l_err_msg);
            END;
         END LOOP;

         COMMIT;

         BEGIN
            BEGIN
               l_sec := 'Process Miscellaneous Transaction';
               xxwc_common_tunning_helpers.elapsed_time ('RTV CONFIRM',
                                                         l_sec,
                                                         g_start);

               l_result :=
                  mtl_online_transaction_pub.process_online (p_rtv_header_id,
                                                             NULL,
                                                             l_err_code,
                                                             l_err_desc);
               --dbms_output.put_line('Result '||l_result);
               DBMS_OUTPUT.put_line ('Err code ' || l_err_code);
               DBMS_OUTPUT.put_line ('Err desc ' || l_err_desc);
            END;

            IF l_result = TRUE
            THEN
               l_sec := 'Update RTV Status';
               xxwc_common_tunning_helpers.elapsed_time ('RTV CONFIRM',
                                                         l_sec,
                                                         g_start);

               BEGIN
             --Modified the below line for TMS ## 20141002-00050  by VijaySrinivasan  on 11/07/2014 
                  UPDATE xxwc_po_rtv_headers_tbl
                     SET rtv_status_code = l_rtv_status,
                         error_message = NULL,
                         last_update_date = SYSDATE,
                         last_updated_by = p_user_id
                   WHERE rtv_header_id = p_rtv_header_id;

                  COMMIT;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     l_err_msg := l_err_msg || ' ' || l_sec || ' ' || SQLERRM;
                     DBMS_OUTPUT.put_line ('Exception : ' || l_err_msg);
               END;
            ELSE
               l_err_msg :=
                     l_err_desc
                  || ' '
                  || l_err_msg
                  || ' '
                  || l_sec
                  || ' '
                  || SQLERRM;
               DBMS_OUTPUT.put_line ('Exception : ' || l_err_msg);

               l_sec := 'Update RTV Error Message';
               xxwc_common_tunning_helpers.elapsed_time ('RTV CONFIRM',
                                                         l_sec,
                                                         g_start);

               BEGIN
             --Modified the below line for TMS ## 20141002-00050  by VijaySrinivasan  on 11/07/2014 
                  UPDATE xxwc_po_rtv_headers_tbl
                     SET error_message = l_err_msg,
                         last_update_date = SYSDATE,
                         last_updated_by = p_user_id
                   WHERE rtv_header_id = p_rtv_header_id;

                  COMMIT;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     l_result := FALSE;
                     p_error_message := l_sec || ' ' || SQLERRM;
               --  ROLLBACK;
               END;
            -- ROLLBACK;
            END IF;
         END;
      ELSE
         p_error_message :=
            l_sec || ' Current User Not Eligible to Confirm RTV ';
      END IF;

      IF l_result IS NOT NULL
      THEN
         p_result := l_result;
      ELSE
         p_result := FALSE;
      END IF;



      IF p_result = FALSE
      THEN
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_PO_RTV_PKG.CONFIRM_RTV',
            p_calling             => l_sec,
            p_request_id          => NULL,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (l_err_desc || p_error_message,
                                             1,
                                             240),
            p_distribution_list   => g_distribution_list,
            p_module              => 'PO');
      END IF;
   END confirm_rtv;


   PROCEDURE print_document (p_wc_return_number   IN     VARCHAR2,
                             p_program_name       IN     VARCHAR2,
                             p_user_id            IN     NUMBER,
                             p_resp_id            IN     NUMBER,
                             p_request_id            OUT NUMBER,
                             p_error_msg             OUT VARCHAR2)
   /*************************************************************************
     $Header print_document $
     Module Name: print_document

     PURPOSE: Procedure to print the RTV Pick Ticket and Packing Slip
     ESMS Task Id :  20130911-00538

     REVISIONS:
     Ver        Date        Author                Description
     ---------  ----------  ------------------    ----------------
     1.0        06/06/2014  Manjula Chellappan    Initial Version

   **************************************************************************/
   IS
      l_wc_return_number   VARCHAR2 (40) := p_wc_return_number;
      l_user_id            NUMBER := p_user_id;
      l_resp_id            NUMBER := p_resp_id;
      l_resp_appl_id       NUMBER;
      l_add_layout         BOOLEAN;
      l_program_name       VARCHAR2 (30) := p_program_name;
      l_error_msg          VARCHAR2 (2000) := NULL;
      l_request_id         NUMBER;
      l_sec                VARCHAR2 (100);
   BEGIN
      l_sec := 'Get Responsibility Application Id';

      DBMS_OUTPUT.put_line (l_sec);


      BEGIN
         SELECT application_id
           INTO l_resp_appl_id
           FROM fnd_responsibility
          WHERE responsibility_id = l_resp_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_resp_appl_id := NULL;
            l_error_msg :=
                  l_error_msg
               || ' '
               || l_sec
               || ' Error in getting Responsibility Application_id '
               || SQLERRM;
      END;

      DBMS_OUTPUT.put_line ('l_resp_appl_id' || l_resp_appl_id);


      l_sec := 'Initialize Apps';

      DBMS_OUTPUT.put_line (l_sec);

      DBMS_OUTPUT.put_line ('l_user_id' || l_user_id);

      DBMS_OUTPUT.put_line ('l_resp_id' || l_resp_id);

      DBMS_OUTPUT.put_line ('l_resp_appl_id' || l_resp_appl_id);

      DBMS_OUTPUT.put_line ('p_wc_return_number' || p_wc_return_number);

      DBMS_OUTPUT.put_line ('l_wc_return_number' || l_wc_return_number);

      DBMS_OUTPUT.put_line ('l_program_name' || l_program_name);

      fnd_global.apps_initialize (l_user_id, l_resp_id, l_resp_appl_id);


      l_sec := 'Add Template Layout';
      DBMS_OUTPUT.put_line (l_sec);


      l_add_layout :=
         fnd_request.add_layout (template_appl_name   => 'XXWC',
                                 template_code        => l_program_name,
                                 template_language    => 'en', --Use language from template definition
                                 template_territory   => 'US', --Use territory from template definition
                                 output_format        => 'PDF' --Use output format from template definition
                                                              );

      IF l_add_layout = TRUE
      THEN
         l_sec := 'Submit Pick Ticket';
         DBMS_OUTPUT.put_line (l_sec);


         l_request_id :=
            fnd_request.submit_request ('XXWC',
                                        l_program_name,
                                        NULL,
                                        NULL,
                                        FALSE,
                                        l_wc_return_number);

         IF l_request_id = 0
         THEN
            l_error_msg :=
                  l_error_msg
               || ' '
               || l_sec
               || ' Failed to submit the concurrent Program ';
            DBMS_OUTPUT.put_line (l_error_msg);
         ELSE
            COMMIT;
         END IF;
      ELSIF l_add_layout = FALSE
      THEN
         l_error_msg :=
               l_error_msg
            || ' '
            || l_sec
            || ' Failed to associate the Template to the concurrent Program ';
         DBMS_OUTPUT.put_line (l_error_msg);
      END IF;

      p_request_id := NVL (l_request_id, 0);
      p_error_msg := l_error_msg;
      DBMS_OUTPUT.put_line (l_error_msg);
   EXCEPTION
      WHEN OTHERS
      THEN
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_PO_RTV_PKG.PRINT_DOCUMENT',
            p_calling             => l_sec,
            p_request_id          => NULL,
            p_ora_error_msg       => SUBSTR (
                                          'Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || 'Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (p_error_msg || SQLERRM, 1, 240),
            p_distribution_list   => g_distribution_list,
            p_module              => 'PO');
   END print_document;

   FUNCTION get_vendor_contact_id (p_po_header_id IN NUMBER)
      RETURN NUMBER
   /*************************************************************************
     $Header get_vendor_contact_id  $
     Module Name: get_vendor_contact_id

     PURPOSE: Procedure to print the RTV Pick Ticket and Packing Slip
     ESMS Task Id :  20130911-00538

     REVISIONS:
     Ver        Date        Author                Description
     ---------  ----------  ------------------    ----------------
     1.0        06/06/2014  Manjula Chellappan    Initial Version

   **************************************************************************/
   IS
      l_vendor_id            NUMBER;
      l_vendor_site_id       NUMBER;
      l_vendor_contact_id    NUMBER;
      l_per_party_id         NUMBER;
      l_relationship_id      NUMBER;
      l_rel_party_id         NUMBER;
      l_vendor_contact_id2   NUMBER;
   BEGIN
      BEGIN
         SELECT vendor_id, vendor_site_id, vendor_contact_id
           INTO l_vendor_id, l_vendor_site_id, l_vendor_contact_id
         --Modified the below line for TMS ## 20141002-00050  by VijaySrinivasan  on 11/07/2014 
           FROM po_headers
          WHERE po_header_id = p_po_header_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_vendor_id := NULL;
            l_vendor_site_id := NULL;
            l_vendor_contact_id := NULL;
      END;

      IF     l_vendor_id IS NOT NULL
         AND l_vendor_site_id IS NOT NULL
         AND l_vendor_contact_id IS NOT NULL
      THEN
         BEGIN
            SELECT per_party_id, relationship_id, rel_party_id
              INTO l_per_party_id, l_relationship_id, l_rel_party_id
              FROM po_vendor_contacts
             WHERE     vendor_id = l_vendor_id
                   AND vendor_site_id = l_vendor_site_id
                   AND vendor_contact_id = l_vendor_contact_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_per_party_id := NULL;
               l_relationship_id := NULL;
               l_rel_party_id := NULL;
         END;
      ELSE
         l_per_party_id := NULL;
         l_relationship_id := NULL;
         l_rel_party_id := NULL;
      END IF;


      IF     l_per_party_id IS NOT NULL
         AND l_relationship_id IS NOT NULL
         AND l_rel_party_id IS NOT NULL
      THEN
         BEGIN
            SELECT vendor_contact_id
              INTO l_vendor_contact_id2
              FROM po_vendor_contacts
             WHERE     vendor_id = l_vendor_id
                   AND per_party_id = l_per_party_id
                   AND relationship_id = l_relationship_id
                   AND rel_party_id = l_rel_party_id
                   AND vendor_contact_id != l_vendor_contact_id
                   AND SYSDATE < inactive_date
                   AND ROWNUM = 1;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_vendor_contact_id2 := NULL;
         END;
      ELSE
         l_vendor_contact_id2 := NULL;
      END IF;


      RETURN l_vendor_contact_id2;
   END get_vendor_contact_id;
END XXWC_PO_RTV_PKG;
/