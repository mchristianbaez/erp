CREATE OR REPLACE PACKAGE BODY APPS.xxwc_purc_mass_upload_pkg
IS
 /****************************************************************************************************
    File Name:XXWC_PURC_MASS_UPLOAD_PKG
    PROGRAM TYPE: PL/SQL Package spec and body
    PURPOSE: created by Rasikha Galimova to use with web ADI process
    HISTORY
    -- Description   :

    VERSION DATE          AUTHOR(S)           DESCRIPTION
    ------- -----------   --------------- ---------------------------------
    1.0     15-Jan-2013   Rasikha Galimova    TMS#20130115-01018 Initial creation of the package
    1.1     10-Nov-2014   Maharajan Shunmugam TMS#20141002-00072 Canada Multi Org changes
    1.2     07-Apr-2016   Gopi Damuluri       TMS#20160407-00190 - Remove Parallelism
    1.3     12-May-2016   Gopi Damuluri       TMS#20160511-00186 - Remove Parallelism
	1.4     02-Oct-2018   P.Vamshidhar        TMS#20180806-00122 - Purchasing Mass 
	                                          upload taking more time these days
											  Added debug messages.
   *************************************************************************************************/
    --l_user_id                          NUMBER := NVL (TO_NUMBER (fnd_profile.VALUE ('USER_ID')), -1);      --commented and added below by Maha for ver 1.1
    l_user_id                          NUMBER := fnd_global.user_id;
    l_batch_number_min                 NUMBER;
    l_batch_number_max                 NUMBER;
    g_debug_flag                       VARCHAR2(1):= substr(FND_PROFILE.VALUE('XXWC_PURC_MASS_UPLOAD_LOG'),1,1);
    g_request_id                       NUMBER:=FND_GLOBAL.CONC_REQUEST_ID;	
    FUNCTION check_type (p_in_string VARCHAR2, p_type VARCHAR2)
        RETURN BOOLEAN;

    c_current_runtime_level   CONSTANT NUMBER := fnd_log.g_current_runtime_level;

    l_package_name                     VARCHAR2 (124) := UPPER ('xxwc_purc_mass_upload_pkg');
    l_procedure_name                   VARCHAR2 (124);
    l_distro_list                      VARCHAR2 (75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
    l_err_code                         VARCHAR2 (24) := 0;

    l_errbuf                           CLOB;
    l_error_message                    CLOB;

    --***************************************************************
    --***************************************************************

    --*****************

   /*************************************************************************
     PROCEDURE : purc_mass_upload_driver
     PURPOSE   : Used by WebADI - XXWC Purchasing Mass Update Tool

     REVISIONS:
     Ver    Date         Author                Description
     ------ -----------  ------------------    ----------------
     1.2    07-Apr-16    Gopi Damuluri         TMS#20160407-00190 - Remove Parallelism
     **************************************************************************/

    PROCEDURE alter_table (p_owner VARCHAR2, p_table_name VARCHAR2)
    IS
    BEGIN
        FOR r IN (SELECT owner || '.' || table_name otable_name
                    FROM all_tables
                   WHERE table_name = UPPER (p_table_name) AND owner = UPPER (p_owner))
        LOOP
            EXECUTE IMMEDIATE 'alter table ' || r.otable_name || ' nologging';

            -- EXECUTE IMMEDIATE 'alter table ' || r.otable_name || ' parallel(degree 6)'; -- Version# 1.2
        END LOOP;
    EXCEPTION
        WHEN OTHERS
        THEN
            NULL;
    END;

    --***************************************************************
    --***************************************************************
    FUNCTION check_type (p_in_string VARCHAR2, p_type VARCHAR2)
        RETURN BOOLEAN
    IS
        v_return_flag   BOOLEAN := FALSE;
        v_number        NUMBER;
    BEGIN
        IF UPPER (p_type) = 'NUMBER'
        THEN
            BEGIN
                SELECT TO_NUMBER (p_in_string) INTO v_number FROM DUAL;

                v_return_flag := TRUE;
            EXCEPTION
                WHEN OTHERS
                THEN
                    v_return_flag := FALSE;
            END;
        END IF;

        RETURN v_return_flag;
    END;

    --******************************************************************************
    PROCEDURE send_developer_error
    IS
        v_instance_name   VARCHAR2 (255);
    BEGIN
        SELECT instance_name
          INTO v_instance_name
          FROM v$instance
         WHERE ROWNUM = 1;

        UTL_MAIL.send_attach_varchar2 (sender         => 'donotreply@hdsupply.com'
                                      ,recipients     => 'HDSOracleDevelopers@hdsupply.com'   --'rasikha.galimova@hdsupply.com'  --Changed email id by Maha for ver1.1
                                      ,subject        => 'general Upload Errors from ' || v_instance_name
                                      ,MESSAGE        => 'see attached file for errors'
                                      ,attachment     => l_error_message
                                      ,att_filename   => 'other_errors.txt');
    END;

    --******************************************************************************
    FUNCTION select_count (p_session_id NUMBER)
        RETURN NUMBER
    IS
        v_count   NUMBER;
    BEGIN
        SELECT COUNT ('a')
          INTO v_count
          FROM xxwc.xxwc_purc_mass_upload_p
         WHERE session_id = p_session_id AND run_uniq IS NULL;

        RETURN v_count;
    EXCEPTION
        WHEN OTHERS
        THEN
            RETURN 0;
    END;

    --******************************************************************************
    --******************************************************************************
    --******************************************************************************
    --******************************************************************************

    --******************************************************************************
    --******************************************************************************
    PROCEDURE process_records
    IS
        v_date                TIMESTAMP WITH TIME ZONE;
        n_loop_number         NUMBER := 0;

        v_more_rows_process   NUMBER := 0;

        lr                    NUMBER := 0;
        v_session_id          NUMBER;
        v_unique_string       VARCHAR2 (64);
    BEGIN
        debug('Procedure Start'); 
        SELECT COUNT ('a') INTO v_more_rows_process FROM xxwc.xxwc_purc_mass_upload_p;
        debug('Staging Table Count:'||v_more_rows_process); 
        IF v_more_rows_process > 0
        THEN
            SELECT DBMS_RANDOM.string ('A', 20) INTO v_unique_string FROM DUAL;
            debug('Unique Sequence String:'||v_unique_string); 
            COMMIT;

            INSERT /*+ APPEND */
                  INTO  xxwc.xxwc_purc_mass_upload_t
                SELECT * FROM xxwc.xxwc_purc_mass_upload_p;
 
            COMMIT;

            DELETE FROM xxwc.xxwc_purc_mass_upload_p;

            COMMIT;

            UPDATE xxwc.xxwc_purc_mass_upload_t
               SET run_uniq = v_unique_string
             WHERE run_uniq IS NULL;

			 debug('Updated Records:'||sql%rowcount); 
			 
            DELETE FROM xxwc.xxwc_purc_mass_upl_arch
                  WHERE insert_date < SYSDATE - 15;
			 debug('Deleted Records (Arch. Table):'||sql%rowcount); 
            COMMIT;

            SELECT COUNT ('a')
              INTO v_more_rows_process
              FROM xxwc.xxwc_purc_mass_upload_t a
             WHERE a.process_flag = 'start' AND run_uniq = v_unique_string;
			 
			 debug('Temp Table Records:'||v_more_rows_process); 
			 
            FOR r IN (SELECT a.ROWID vrowid, a.*
                        FROM xxwc.xxwc_purc_mass_upload_t a
                       WHERE a.process_flag = 'start' AND run_uniq = v_unique_string)
            LOOP
                v_date := SYSTIMESTAMP;

                --create a apps session, because it runs new session from dbms_job
                IF n_loop_number = 0 AND r.resp_id IS NOT NULL AND r.resp_appl_id IS NOT NULL
                THEN
                    v_session_id := r.session_id;
                    fnd_global.apps_initialize (user_id        => r.user_id
                                               ,resp_id        => r.resp_id
                                               ,resp_appl_id   => r.resp_appl_id);
                END IF;

                n_loop_number := n_loop_number + 1;

                UPDATE xxwc.xxwc_purc_mass_upload_t
                   SET start_process_date = v_date
                 WHERE ROWID = r.vrowid;

                COMMIT;
			    debug('Item :'||r.p_item_number||' Process Start'); 				
                purc_mass_upload_dr_inner (r.p_org_code
                                          ,r.p_item_number
                                          ,r.p_sourcing_rule
                                          ,r.p_source_org
                                          ,r.p_source_type
                                          ,r.p_pplt
                                          ,r.p_plt
                                          ,r.p_flm
                                          ,r.p_classification
                                          ,r.p_planning_method
                                          ,r.p_min
                                          ,r.p_max
                                          ,r.p_p_flag
                                          ,r.p_reserve_stock
                                          ,r.p_buyer
                                          ,r.p_amu
                                          ,r.vrowid);
                v_date := SYSTIMESTAMP;

                UPDATE xxwc.xxwc_purc_mass_upload_t
                   SET end_process_date = v_date, process_flag = 'end'
                 WHERE ROWID = r.vrowid;
				 
			    debug('Item :'||r.p_item_number||' Process Completed'); 				 
				
            END LOOP;
            debug('send_error_message start'); 
            send_error_message (v_unique_string);
            debug('send_error_message complete'); 
            INSERT /*+ append */
                  INTO  xxwc.xxwc_purc_mass_upl_arch
                SELECT *
                  FROM xxwc.xxwc_purc_mass_upload_t
                 WHERE process_flag = 'end' AND run_uniq = v_unique_string;

            DELETE FROM xxwc.xxwc_purc_mass_upload_t
                  WHERE process_flag = 'end' AND run_uniq = v_unique_string;

            COMMIT;
        END IF;
    EXCEPTION
        WHEN OTHERS
        THEN
            l_error_message :=
                   'Error_Stack...'
                || DBMS_UTILITY.format_error_stack ()
                || ' Error_Backtrace...'
                || DBMS_UTILITY.format_error_backtrace ();

            send_developer_error;
    END;

    --******************************************************************************
    --******************************************************************************
    --******************************************************************************
    --******************************************************************************
    --******************************************************************************
    --******************************************************************************
    --******************************************************************************

    --******************************************************************************
    --******************************************************************************
    --******************************************************************************
    --******************************************************************************
    --******************************************************************************
    /*DECLARE
        v_repeat_string   VARCHAR2 (1024);
        v_end_date        TIMESTAMP WITH TIME ZONE;
    BEGIN
        v_repeat_string := 'FREQ=MINUTELY;INTERVAL=3;';

        -- DBMS_OUTPUT.put_line (v_repeat_string);

        BEGIN
            sys.DBMS_SCHEDULER.drop_job (job_name => 'XXWC_PURC_MASS_UPL_RECORDS');
        EXCEPTION
            WHEN OTHERS
            THEN
                NULL;
        END;

        DBMS_SCHEDULER.create_job (
            job_name          => 'XXWC_PURC_MASS_UPL_RECORDS'
           ,job_type          => 'PLSQL_BLOCK'
           ,job_action        => 'BEGIN xxwc_purc_mass_upload_pkg.process_records; END;'
           ,start_date        => CURRENT_TIMESTAMP
           ,repeat_interval   => v_repeat_string
           ,end_date          => NULL
           ,enabled           => TRUE
           ,comments          => 'Schedule to process record uploaded by ADI for xxwc_purc_mass_upload_pkg');
    END;

     BEGIN
         DBMS_SCHEDULER.drop_job (job_name => 'XXWC_PURC_MASS_UPL_RECORDS');
     END;


     SELECT owner, job_name, enabled FROM dba_scheduler_jobs;
     select table_name from dict where table_name like '%SCHEDULER%';

    SELECT *
      FROM dba_scheduler_jobs
     WHERE job_name = 'XXWC_PURC_MASS_UPL_RECORDS'*/
    --******************************************************************************
    --******************************************************************************
    --******************************************************************************
    --******************************************************************************

   /*************************************************************************
     PROCEDURE : purc_mass_upload_driver
     PURPOSE   : Used by WebADI - XXWC Purchasing Mass Update Tool

     REVISIONS:
     Ver    Date         Author                Description
     ------ -----------  ------------------    ----------------
     1.2    07-Apr-16    Gopi Damuluri         TMS#20160407-00190 - Remove Parallelism
     1.3    12-May-2016  Gopi Damuluri         TMS#20160511-00186 - Remove Parallelism
     **************************************************************************/

    PROCEDURE purc_mass_upload_driver (p_org_code          IN VARCHAR2
                                      ,                                                             --used in find query
                                       p_item_number       IN VARCHAR2
                                      ,                                                             --used in find query
										p_sourcing_rule     IN VARCHAR2
                                      ,                                                           --update sourcing rule
                                       p_source_org        IN VARCHAR2
                                      ,                                --update item attributes SOURCE_ORGANIZATION_ID--
                                       p_source_type       IN VARCHAR2
                                      ,                              --update item attributes SOURCE_TYPE--values"I","S"
                                       p_pplt              IN VARCHAR2
                                      ,                               --update item attributes PREPROCESSING_LEAD_TIME--
                                       p_plt               IN VARCHAR2
                                      ,                                        --update item attributes FULL_LEAD_TIME--
                                       p_flm               IN VARCHAR2
                                      ,                                  --update item attributes FIXED_LOT_MULTIPLIER--
                                       p_classification    IN VARCHAR2
                                      ,                               --*** update category_set_name  --'Sales Velocity'
                                       p_planning_method   IN VARCHAR2
                                      ,                               --update item attributes INVENTORY_PLANNING_CODE--
                                       p_min               IN VARCHAR2
                                      ,                                   --update item attributes MIN_MINMAX_QUANTITY--
                                       p_max               IN VARCHAR2
                                      ,                                   --update item attributes MAX_MINMAX_QUANTITY--
                                       p_p_flag            IN VARCHAR2
                                      ,                                --*** update category_set_name  --'Purchase Flag'
                                       p_reserve_stock     IN VARCHAR2
                                      ,                                           --update item attributes attribute21--
                                       p_buyer             IN VARCHAR2
                                      ,                                             --update item attributes  BUYER_ID--
                                       p_amu               IN VARCHAR2)
    IS
        v_row_count           NUMBER;
        l_jobno               INTEGER;
        v_already_running     NUMBER := 0;
        v_session_id          NUMBER := NVL (USERENV ('SESSIONID'), -1);
        v_execute_string      VARCHAR2 (1024);
        v_resp_appl_id        NUMBER;

        v_resp_id             NUMBER;
        v_uniq_number         NUMBER;
        v_old_chunks          NUMBER := 0;
        v_unique_string       VARCHAR2 (64);
        v_already_submitted   VARCHAR2 (64) := '0';
    BEGIN
       -- l_user_id := NVL (TO_NUMBER (fnd_profile.VALUE ('USER_ID')), -1); --commented and added below for ver 1.1
      --  v_resp_appl_id := fnd_profile.VALUE ('RESP_APPL_ID');

        --v_resp_id := fnd_profile.VALUE ('RESP_ID');

	  l_user_id 	 := 	fnd_global.user_id;         
          v_resp_appl_id := 	fnd_global.resp_appl_id;
          v_resp_id 	 := 	fnd_global.resp_id;

        -- apps.xxwc_mv_routines_add_pkg.enable_parallelism; -- Version# 1.2 -- Version# 1.3

        INSERT /*+ APPEND */
              INTO  xxwc.xxwc_purc_mass_upload_p (p_org_code
                                                 ,p_item_number
                                                 ,p_sourcing_rule
                                                 ,p_source_org
                                                 ,p_source_type
                                                 ,p_pplt
                                                 ,p_plt
                                                 ,p_flm
                                                 ,p_classification
                                                 ,p_planning_method
                                                 ,p_min
                                                 ,p_max
                                                 ,p_p_flag
                                                 ,p_reserve_stock
                                                 ,p_buyer
                                                 ,p_amu
                                                 ,insert_date
                                                 ,error_message
                                                 ,process_flag
                                                 ,organization_id
                                                 ,rows_inserted
                                                 ,user_id
                                                 ,session_id
                                                 ,resp_appl_id
                                                 ,resp_id)
             VALUES (p_org_code
                    ,p_item_number
                    ,p_sourcing_rule
                    ,p_source_org
                    ,p_source_type
                    ,p_pplt
                    ,p_plt
                    ,p_flm
                    ,p_classification
                    ,p_planning_method
                    ,p_min
                    ,p_max
                    ,p_p_flag
                    ,p_reserve_stock
                    ,p_buyer
                    ,p_amu
                    ,SYSDATE
                    ,NULL
                    ,'start'
                    ,NULL
                    ,0
                    ,l_user_id
                    ,v_session_id
                    ,v_resp_appl_id
                    ,v_resp_id);

        COMMIT;
        
        -- apps.xxwc_mv_routines_add_pkg.disable_parallelism; -- Version# 1.2 -- Version# 1.3
        
    EXCEPTION
        WHEN OTHERS
        THEN
            l_error_message :=
                   'Error_Stack...'
                || DBMS_UTILITY.format_error_stack ()
                || ' Error_Backtrace...'
                || DBMS_UTILITY.format_error_backtrace ();

            send_developer_error;
    END;

    --******************************************************************************
    --******************************************************************************
    --******************************************************************************

    --******************************************************************************
    --******************************************************************************
    --******************************************************************************
    --******************************************************************************

   /*************************************************************************
     PROCEDURE : purc_mass_upload_dr_inner
     PURPOSE   : Used by WebADI - XXWC Purchasing Mass Update Tool

     REVISIONS:
     Ver    Date         Author                Description
     ------ -----------  ------------------    ----------------
     1.2    07-Apr-16    Gopi Damuluri         TMS#20160407-00190 - Remove Parallelism
     1.3    12-May-2016  Gopi Damuluri         TMS#20160511-00186 - Remove Parallelism
     **************************************************************************/

    PROCEDURE purc_mass_upload_dr_inner (pl_org_code         IN VARCHAR2
                                        ,                                                           --used in find query
                                         p_item_number       IN VARCHAR2
                                        ,                                                           --used in find query
                                         p_sourcing_rule     IN VARCHAR2
                                        ,                                                         --update sourcing rule
                                         p_source_org        IN VARCHAR2
                                        ,                              --update item attributes SOURCE_ORGANIZATION_ID--
                                         p_source_type       IN VARCHAR2
                                        ,                            --update item attributes SOURCE_TYPE--values"I","S"
                                         p_pplt              IN VARCHAR2
                                        ,                             --update item attributes PREPROCESSING_LEAD_TIME--
                                         p_plt               IN VARCHAR2
                                        ,                                      --update item attributes FULL_LEAD_TIME--
                                         p_flm               IN VARCHAR2
                                        ,                                --update item attributes FIXED_LOT_MULTIPLIER--
                                         p_classification    IN VARCHAR2
                                        ,                             --*** update category_set_name  --'Sales Velocity'
                                         p_planning_method   IN VARCHAR2
                                        ,                             --update item attributes INVENTORY_PLANNING_CODE--
                                         p_min               IN VARCHAR2
                                        ,                                 --update item attributes MIN_MINMAX_QUANTITY--
                                         p_max               IN VARCHAR2
                                        ,                                 --update item attributes MAX_MINMAX_QUANTITY--
                                         p_p_flag            IN VARCHAR2
                                        ,                              --*** update category_set_name  --'Purchase Flag'
                                         p_reserve_stock     IN VARCHAR2
                                        ,                                         --update item attributes attribute21--
                                         p_buyer             IN VARCHAR2
                                        ,                                           --update item attributes  BUYER_ID--
                                         p_amu               IN VARCHAR2
                                        ,p_rowid             IN VARCHAR2)
    --update item attributes attribute20--
    IS
        x_inventory_item_id          NUMBER;
        x_organization_id            NUMBER;
        x_return_status              VARCHAR2 (300);
        x_msg_count                  NUMBER;
        x_msg_data                   VARCHAR2 (1024);
        x_errorcode                  NUMBER;

        x_rowid                      VARCHAR2 (124) := p_rowid;

        --**************************************
        l_inventory_item_id          NUMBER;
        v_inventory_planning_code    NUMBER;
        l_inventory_planning_code    NUMBER;
        v_source_type                NUMBER;
        l_source_type                NUMBER;
        v_organization_id            NUMBER;
        l_organization_id            NUMBER;
        v_source_organization_id     NUMBER;
        l_source_organization_id     NUMBER;
        l_attribute21                VARCHAR2 (240);
        l_attribute20                VARCHAR2 (240);
        l_min_minmax_quantity        NUMBER;
        l_max_minmax_quantity        NUMBER;

        v_buyer_id                   NUMBER;
        l_buyer_id                   NUMBER;
        l_preprocessing_lead_time    NUMBER;
        l_full_lead_time             NUMBER;
        l_fixed_lot_multiplier       NUMBER;
        l_rule_id                    NUMBER;

        --  v_error                     EXCEPTION;
        e_validation_error           EXCEPTION;
        v_error_message              CLOB;

        v_found_segment1             VARCHAR2 (124);
        v_found_item_number          VARCHAR2 (124);
        v_current_source_meaning     VARCHAR2 (124);
        v_running_message1           CLOB;

        vl_source_type               VARCHAR2 (24);
        p_org_code                   VARCHAR2 (24);
        pl_source_org                VARCHAR2 (24);
        v_inventory_source_code_lk   VARCHAR2 (24);
        v_supplier_source_code_lk    VARCHAR2 (24);
        x_message_list               error_handler.error_tbl_type;
    BEGIN

        -- apps.xxwc_mv_routines_add_pkg.enable_parallelism; -- Version# 1.2 -- Version# 1.3

        --find lookup values source code
        debug('Item :'||p_item_number||' Start child procedure'); 
        SELECT lookup_code
          INTO v_inventory_source_code_lk
          FROM mfg_lookups
         WHERE lookup_type = 'MTL_SOURCE_TYPES' AND enabled_flag = 'Y' AND meaning = 'Inventory';
		 
        debug('MTL_SOURCE_TYPES (INV) lookup code:'||v_inventory_source_code_lk); 
		
        SELECT lookup_code
          INTO v_supplier_source_code_lk
          FROM mfg_lookups
         WHERE lookup_type = 'MTL_SOURCE_TYPES' AND enabled_flag = 'Y' AND meaning = 'Supplier';

        debug('MTL_SOURCE_TYPES (Supplier) lookup code:'||v_supplier_source_code_lk); 		 
		 
        IF p_source_type = 'I'
        THEN
            vl_source_type := 'Inventory';
        ELSIF p_source_type = 'S'
        THEN
            vl_source_type := 'Supplier';
        ELSE
            vl_source_type := NULL;
        END IF;

        l_err_code := '0';
        fnd_message.clear;

        DELETE FROM xxwc.xxwc_adi_error
              WHERE insert_date < SYSDATE - 2;

        debug('xxwc.xxwc_adi_error deleted:'||SQL%ROWCOUNT); 		 
		
        --find item organization
        p_org_code := pl_org_code;

        IF LENGTH (REPLACE (pl_org_code, ' ')) = 2
        THEN
            p_org_code := '0' || pl_org_code;
        END IF;

        IF LENGTH (REPLACE (pl_org_code, ' ')) = 1
        THEN
            p_org_code := '00' || pl_org_code;
        END IF;

        BEGIN
            SELECT organization_id
              INTO v_organization_id
              FROM org_organization_definitions
             WHERE organization_code = p_org_code AND disable_date IS NULL;
        debug('v_organization_id:'||v_organization_id); 		 			 
        EXCEPTION
            WHEN OTHERS
            THEN
                fnd_message.set_name ('XXWC', 'XXWC_ORG_CODE_ERROR');                                                 --
                fnd_message.set_token ('ORG_CODE', p_org_code);

                v_error_message := 'Invalid Organization Code=' || p_org_code;

                RAISE e_validation_error;
        END;

        BEGIN
            SELECT segment1
              INTO v_found_item_number
              FROM mtl_system_items
             WHERE organization_id = v_organization_id AND segment1 = p_item_number;
        debug('v_found_item_number:'||v_found_item_number); 		 			 			 
        EXCEPTION
            WHEN OTHERS
            THEN
                v_error_message := 'Invalid Item=' || p_item_number || ' for org=' || p_org_code;
                RAISE e_validation_error;
        END;

        v_found_item_number := NULL;
        l_organization_id := v_organization_id;

        FOR r
            IN (SELECT inventory_item_id
                      ,source_organization_id
                      ,buyer_id
                      ,attribute_category
                      ,attribute21
                      ,source_type
                      ,inventory_planning_code
                      ,fixed_lot_multiplier
                      ,preprocessing_lead_time
                      ,full_lead_time
                      ,min_minmax_quantity
                      ,max_minmax_quantity
                      ,attribute20
                  FROM mtl_system_items_b
                 WHERE     organization_id = v_organization_id
                       AND segment1 = p_item_number
                       AND enabled_flag = 'Y'
                       AND end_date_active IS NULL)
        LOOP
            debug('inventory_item_id:'||r.inventory_item_id); 	
			
    		l_inventory_item_id := r.inventory_item_id;

            --validate source org id and source type
            DBMS_OUTPUT.put_line ('here Vl_SOURCE_TYPE=' || vl_source_type);

            IF vl_source_type IS NOT NULL
            THEN
                BEGIN
                    SELECT lookup_code
                      INTO v_source_type
                      FROM mfg_lookups
                     WHERE lookup_type = 'MTL_SOURCE_TYPES' AND enabled_flag = 'Y' AND meaning = vl_source_type;
                debug('v_source_type:'||v_source_type); 		 			 			 					 
                EXCEPTION
                    WHEN OTHERS
                    THEN
                        fnd_message.set_name ('XXWC', 'XXWC_SOURCE_TYPE_ERROR');                                      --
                        fnd_message.set_token ('SOURCE_TYPE', p_source_type);

                        v_error_message := 'Invalid Source Type  =' || p_source_type;
                        RAISE e_validation_error;
                END;

                IF r.source_type IS NOT NULL
                THEN
                    SELECT meaning
                      INTO v_current_source_meaning
                      FROM mfg_lookups
                     WHERE lookup_type = 'MTL_SOURCE_TYPES' AND enabled_flag = 'Y' AND lookup_code = r.source_type;
                END IF;

                debug('v_current_source_meaning:'||v_current_source_meaning); 		 			 			 					 
				
                IF v_source_type <> r.source_type OR (r.source_type IS NULL AND v_source_type IS NOT NULL)
                THEN
                    l_source_type := v_source_type;
                END IF;
            ELSIF vl_source_type IS NULL AND p_source_org IS NOT NULL AND v_current_source_meaning = 'Inventory' --when source_type in file is null -use current value
            THEN
                l_source_type := r.source_type;
            ELSE
                l_source_type := ego_item_pub.g_miss_num;                                  --will not change source type
            END IF;

            --validate source org id
            -- DBMS_OUTPUT.PUT_LINE ('p_source_type=' || P_SOURCE_TYPE);
            --  DBMS_OUTPUT.PUT_LINE ('p_source_org=' || P_SOURCE_ORG);
            --find source org id
            pl_source_org := p_source_org;

            IF p_source_org IS NOT NULL AND LENGTH (REPLACE (p_source_org, ' ')) = 2
            THEN
                pl_source_org := '0' || p_source_org;
            ELSIF p_source_org IS NOT NULL AND LENGTH (REPLACE (p_source_org, ' ')) = 1
            THEN
                pl_source_org := '00' || p_source_org;
            ELSE
                pl_source_org := p_source_org;
            END IF;

            -- IF NVL (vl_source_type, 'X') = 'Inventory' AND pl_source_org IS NOT NULL
            IF         -- (NVL (vl_source_type, 'X') = 'Inventory' OR NVL (v_current_source_meaning, 'X') = 'Inventory')
              pl_source_org IS NOT NULL
            THEN
                BEGIN
                    SELECT organization_id
                      INTO v_source_organization_id
                      FROM org_organization_definitions
                     WHERE organization_code = pl_source_org AND disable_date IS NULL;
                debug('v_source_organization_id:'||v_source_organization_id); 		 			 			 					 					 
                -- DBMS_OUTPUT.PUT_LINE ('V_SOURCE_ORGANIZATION_ID=' || V_SOURCE_ORGANIZATION_ID);
                EXCEPTION
                    WHEN OTHERS
                    THEN
                        fnd_message.set_name ('XXWC', 'XXWC_SOURCE_ORG_ERROR');                                       --
                        fnd_message.set_token ('SOURCE_ORG', pl_source_org);

                        v_error_message := 'Invalid Source Org Code =' || pl_source_org;
                        RAISE e_validation_error;
                END;

                IF v_source_organization_id IS NOT NULL                   -- AND NVL (vl_source_type, 'X') = 'Inventory'
                                                        --  AND (NVL (vl_source_type, 'X') = 'Inventory' OR NVL (v_current_source_meaning, 'X') = 'Inventory')
                   AND v_source_organization_id <> NVL (r.source_organization_id, '999999999')
                THEN
                    l_source_organization_id := v_source_organization_id;
                END IF;
            -- ELSIF pl_source_org = 'UPN'  --Current SO incorrect - update to null source_organization_id to source direct
            --  THEN
            --l_source_organization_id := NULL;
            -- l_source_type := v_supplier_source_code_lk;
            -- l_source_organization_id := ego_item_pub.g_miss_num;
            ELSE
                l_source_organization_id := ego_item_pub.g_miss_num;
            END IF;

            --verify buyer
            IF p_buyer IS NOT NULL AND UPPER (p_buyer) <> 'UPN'
            THEN
                BEGIN
                    SELECT ppf.person_id
                      INTO v_buyer_id
                      FROM per_people_f ppf, po_agents poa, hr_organization_units org
                     WHERE     ppf.person_id = poa.agent_id
                           AND ppf.business_group_id = org.business_group_id
                           AND SYSDATE BETWEEN NVL (poa.start_date_active, SYSDATE - 1)
                                           AND NVL (poa.end_date_active, SYSDATE + 1)
                           AND TRUNC (SYSDATE) BETWEEN ppf.effective_start_date AND ppf.effective_end_date
                           AND NVL (ppf.current_employee_flag, 'N') = 'Y'
                           AND LOWER (ppf.full_name) = LOWER (p_buyer)
                           AND ROWNUM = 1;
                debug('v_buyer_id:'||v_buyer_id); 		 			 			 					 					 						   
                EXCEPTION
                    WHEN OTHERS
                    THEN
                        fnd_message.set_name ('XXWC', 'XXWC_BUYER_ERROR');                                            --
                        fnd_message.set_token ('BUYER', p_buyer);

                        v_error_message := 'Invalid Buyer =' || p_buyer;
                        RAISE e_validation_error;
                END;

                --here put ntification if we have dup by buyer name

                IF NVL (v_buyer_id, 99999999) <> NVL (r.buyer_id, 99999999)
                THEN
                    l_buyer_id := v_buyer_id;
                END IF;
            ELSIF UPPER (p_buyer) = 'UPN'
            THEN
                l_buyer_id := NULL;                                                            --will wipe the old buyer
            ELSE
                l_buyer_id := ego_item_pub.g_miss_num;
            END IF;

            --validate planning method

            IF p_planning_method IS NOT NULL
            THEN
                BEGIN
                    SELECT lookup_code
                      INTO v_inventory_planning_code
                      FROM mfg_lookups
                     WHERE lookup_type = 'MTL_MATERIAL_PLANNING' AND enabled_flag = 'Y' AND meaning = p_planning_method;
                debug('v_inventory_planning_code:'||v_inventory_planning_code); 		 			 			 					 					 						   					 
                EXCEPTION
                    WHEN OTHERS
                    THEN
                        fnd_message.set_name ('XXWC', 'XXWC_PLAN_METHOD_ERROR');                                      --
                        fnd_message.set_token ('PLAN_METHOD', p_planning_method);

                        v_error_message := 'Invalid Planning method=' || p_planning_method;
                        RAISE e_validation_error;
                END;

                IF v_inventory_planning_code <> NVL (r.inventory_planning_code, -1000)
                THEN
                    l_inventory_planning_code := v_inventory_planning_code;
                END IF;
            ELSE
                l_inventory_planning_code := NULL;
            END IF;

            --validate amu
            IF p_amu IS NOT NULL
            THEN
                IF p_amu <> NVL (r.attribute20, 'XXXXXXX')
                THEN
                    l_attribute20 := p_amu;
                END IF;
            ELSE
                l_attribute20 := NULL;
            END IF;

            --validate reserve_stock
            IF p_reserve_stock IS NOT NULL
            THEN
                IF p_reserve_stock <> NVL (r.attribute21, 'XXXXXXX')
                THEN
                    l_attribute21 := p_reserve_stock;
                END IF;
            ELSE
                l_attribute21 := NULL;
            END IF;

            -- validate min
            IF p_min IS NOT NULL
            THEN
                IF NOT check_type (NVL (p_min, 0), 'NUMBER')
                THEN
                    v_error_message := 'Invalid Min Minmax Quantity =' || p_min || ' should be number';
                    RAISE e_validation_error;
                END IF;

                IF NVL (p_min, -1000) <> NVL (r.min_minmax_quantity, -1000)
                THEN
                    l_min_minmax_quantity := p_min;
                END IF;
            ELSE
                l_min_minmax_quantity := NULL;
            END IF;

            -- validate max
            IF p_max IS NOT NULL
            THEN
                IF NOT check_type (NVL (p_max, 0), 'NUMBER')
                THEN
                    v_error_message := 'Invalid Max Minmax Quantity =' || p_max || ' should be number';
                    RAISE e_validation_error;
                END IF;

                IF NVL (p_max, -1000) <> NVL (r.max_minmax_quantity, -1000)
                THEN
                    l_max_minmax_quantity := p_max;
                END IF;
            ELSE
                l_max_minmax_quantity := NULL;
            END IF;

            --validate Preprocessing Lead Time
            IF p_pplt IS NOT NULL
            THEN
                IF NOT check_type (NVL (p_pplt, 0), 'NUMBER')
                THEN
                    v_error_message := 'Invalid Preprocessing Lead Time =' || p_pplt || ' should be number';
                    RAISE e_validation_error;
                END IF;

                IF NVL (p_pplt, -1000) <> NVL (r.preprocessing_lead_time, -1000)
                THEN
                    l_preprocessing_lead_time := p_pplt;
                END IF;
            ELSE
                l_preprocessing_lead_time := NULL;
            END IF;

            --validate Full Lead Time
            IF p_plt IS NOT NULL
            THEN
                IF NOT check_type (NVL (p_plt, 0), 'NUMBER')
                THEN
                    v_error_message := 'Invalid Full Lead Time =' || p_plt || ' should be number';
                    RAISE e_validation_error;
                END IF;

                IF NVL (p_plt, -1000) <> NVL (r.full_lead_time, -1000)
                THEN
                    l_full_lead_time := p_plt;
                END IF;
            ELSE
                l_full_lead_time := NULL;
            END IF;

            --validate Fixed Lot Multiplier
            IF p_flm IS NOT NULL AND UPPER (p_flm) <> 'UPN'
            THEN
                --VALIDATION IT IS NUMBER
                IF NOT check_type (NVL (p_flm, 0), 'NUMBER')
                THEN
                    v_error_message := 'Invalid Fixed Lot Multiplier =' || p_flm || ' should be number';
                    RAISE e_validation_error;
                END IF;

                IF NVL (p_flm, -1000) <> NVL (r.fixed_lot_multiplier, -1000)
                THEN
                    l_fixed_lot_multiplier := p_flm;
                END IF;
            ELSIF UPPER (p_flm) = 'UPN'
            THEN
                l_fixed_lot_multiplier := NULL;                                                  --will wipe the old flm
            ELSE
                l_fixed_lot_multiplier := ego_item_pub.g_miss_num;
            END IF;
        END LOOP;

        IF vl_source_type = 'Supplier'
        THEN
            l_source_organization_id := NULL;
        ELSE
            l_source_organization_id := NVL (l_source_organization_id, ego_item_pub.g_miss_num);
        END IF;
                debug('ego_item_pub.process_item start'); 		 			 			 					 					 						   					 
		
        apps.ego_item_pub.process_item (
            p_api_version               => 1.0
           ,p_init_msg_list             => 'T'
           ,p_commit                    => 'T'
           ,p_transaction_type          => 'UPDATE'
           ,p_inventory_item_id         => l_inventory_item_id
           ,p_organization_id           => l_organization_id
           ,p_segment1                  => p_item_number
           ,p_attribute21               => NVL (l_attribute21, ego_item_pub.g_miss_char)
           ,p_attribute20               => NVL (l_attribute20, ego_item_pub.g_miss_char)
           ,p_source_organization_id    => l_source_organization_id
           ,p_buyer_id                  => l_buyer_id
           ,p_inventory_planning_code   => NVL (l_inventory_planning_code, ego_item_pub.g_miss_num)
           ,p_min_minmax_quantity       => NVL (l_min_minmax_quantity, ego_item_pub.g_miss_num)
           ,p_max_minmax_quantity       => NVL (l_max_minmax_quantity, ego_item_pub.g_miss_num)
           ,p_preprocessing_lead_time   => NVL (l_preprocessing_lead_time, ego_item_pub.g_miss_num)
           ,p_full_lead_time            => NVL (l_full_lead_time, ego_item_pub.g_miss_num)
           ,p_fixed_lot_multiplier      => l_fixed_lot_multiplier
           ,p_source_type               => NVL (l_source_type, ego_item_pub.g_miss_num)
           ,x_inventory_item_id         => x_inventory_item_id
           ,x_organization_id           => x_organization_id
           ,x_return_status             => x_return_status
           ,x_msg_count                 => x_msg_count
           ,x_msg_data                  => x_msg_data);

		   debug('ego_item_pub.process_item Completed'); 		 			 			 					 					 						   					 
		   
        v_running_message1 := NULL;

        IF x_return_status != fnd_api.g_ret_sts_success
        THEN
            error_handler.get_message_list (x_message_list);

            FOR i IN 1 .. x_message_list.COUNT
            LOOP
                -- DBMS_OUTPUT.put_line (x_message_list (i).MESSAGE_TEXT);
                v_running_message1 := v_running_message1 || x_message_list (i).MESSAGE_TEXT;
            END LOOP;

            v_error_message :=
                   'error calling apps.ego_item_pub.process_item for item '
                || l_inventory_item_id
                || ' error:'
                || v_running_message1;
            RAISE e_validation_error;
        END IF;

        --update soursing rule here
        IF p_sourcing_rule IS NOT NULL
        THEN
            BEGIN
                SELECT MAX (sourcing_rule_id)
                  INTO l_rule_id
                  FROM mrp_sourcing_rules
                 WHERE sourcing_rule_name = NVL (p_sourcing_rule, TO_CHAR (SYSDATE, 'DDMONYYYYHH24MISS'));
		   debug('l_rule_id:'||l_rule_id);				 
            --DBMS_OUTPUT.PUT_LINE ('p_sourcing_rule=' || P_SOURCING_RULE);
            EXCEPTION
                WHEN OTHERS
                THEN
                    -- DBMS_OUTPUT.PUT_LINE ('p_sourcing_rule in=' || P_SOURCING_RULE);

                    IF p_sourcing_rule IS NOT NULL
                    THEN
                        fnd_message.set_name ('XXWC', 'XXWC_SOURCING_MISSING');
                        fnd_message.set_token ('XXWC_SOURCING', p_sourcing_rule);
                        v_error_message := 'Invalid Sourcing Rule =' || p_sourcing_rule;
                        RAISE e_validation_error;
                    END IF;
            END;

            IF l_rule_id IS NULL
            THEN
                fnd_message.set_name ('XXWC', 'XXWC_SOURCING_MISSING');
                fnd_message.set_token ('XXWC_SOURCING', p_sourcing_rule);
                v_error_message := 'Invalid Sourcing Rule =' || p_sourcing_rule;
                RAISE e_validation_error;
            END IF;

            IF     p_sourcing_rule IS NOT NULL
               AND p_sourcing_rule <> NVL (get_sourcing_rule (l_inventory_item_id, l_organization_id), 'null34')
            THEN
     		   debug('assign_item_sourcing_rule start');				 			
                assign_item_sourcing_rule (l_organization_id
                                          ,l_inventory_item_id
                                          ,p_sourcing_rule
                                          ,x_return_status
                                          ,x_msg_count
                                          ,x_msg_data
                                          ,x_rowid);
     		   debug('assign_item_sourcing_rule Completed');				 													  
            END IF;

            IF x_return_status != fnd_api.g_ret_sts_success
            THEN
                FOR ln_index IN 1 .. x_msg_count
                LOOP
                    x_msg_data := fnd_msg_pub.get (p_msg_index => ln_index, p_encoded => fnd_api.g_false);

                    IF x_msg_data IS NOT NULL AND NVL (v_running_message1, 'null') <> NVL (x_msg_data, 'null')
                    THEN
                        v_running_message1 := v_running_message1 || ' ' || x_msg_data;
                    END IF;
                END LOOP;

                v_error_message :=
                       'error calling assign_item_sourcing_rule for item '
                    || l_inventory_item_id
                    || ' error:'
                    || v_running_message1;
                RAISE e_validation_error;
            END IF;
        END IF;

        --update category attributes
        -- validate item category  FOR  Sales Velocity
        IF p_classification IS NOT NULL
        THEN
            v_found_segment1 := NULL;

            BEGIN
                SELECT mcv.segment1
                  INTO v_found_segment1
                  FROM mtl_categories_kfv mcv, mtl_category_sets mcs
                 WHERE     mcs.category_set_name = 'Sales Velocity'
                       AND mcs.structure_id = mcv.structure_id
                       AND mcv.segment1 = p_classification
                       AND enabled_flag = 'Y';
     		   debug('v_found_segment1:'||v_found_segment1);				 													  					   
            EXCEPTION
                WHEN OTHERS
                THEN
                    NULL;
            END;

            IF v_found_segment1 IS NULL
            THEN
                fnd_message.set_name ('XXWC', 'XXWC_ITEM_CATEGORY_MISSING');
                fnd_message.set_token ('CATEGORY', 'Sales Velocity');
                fnd_message.set_token ('CAT_VALUE', p_classification);
                v_error_message := 'Invalid item category  FOR  Sales Velocity NEW VALUE=' || p_classification;
                RAISE e_validation_error;
            END IF;
     		   debug('process_item_category Start');				 													  					   
            process_item_category (l_inventory_item_id
                                  ,l_organization_id
                                  ,p_classification
                                  ,                                                                        -- p_segment1
                                   'Sales Velocity'
                                  ,x_return_status
                                  ,x_errorcode
                                  ,x_msg_count
                                  ,x_msg_data
                                  ,x_rowid);
								  
     		   debug('process_item_category Completed');
			   
            IF x_return_status != fnd_api.g_ret_sts_success
            THEN
                FOR ln_index IN 1 .. x_msg_count
                LOOP
                    x_msg_data := fnd_msg_pub.get (p_msg_index => ln_index, p_encoded => fnd_api.g_false);

                    IF x_msg_data IS NOT NULL AND NVL (v_running_message1, 'null') <> NVL (x_msg_data, 'null')
                    THEN
                        v_running_message1 := v_running_message1 || ' ' || x_msg_data;
                    END IF;
                END LOOP;

                v_error_message :=
                       'error calling process_item_category for item '
                    || l_inventory_item_id
                    || ' error:'
                    || v_running_message1;
                RAISE e_validation_error;
            END IF;
        END IF;

        -- validate   item category  FOR  Purchase Flag
        IF  p_p_flag IS NOT NULL
        THEN
            v_found_segment1 := NULL;

            BEGIN
                SELECT mcv.segment1
                  INTO v_found_segment1
                  FROM mtl_categories_kfv mcv, mtl_category_sets mcs
                 WHERE     mcs.category_set_name = 'Purchase Flag'
                       AND mcs.structure_id = mcv.structure_id
                       AND mcv.segment1 = p_p_flag
                       AND enabled_flag = 'Y';
     		   debug('v_found_segment1 (p_p_flag): '||v_found_segment1);					   
            EXCEPTION
                WHEN OTHERS
                THEN
                    NULL;
            END;

            IF v_found_segment1 IS NULL
            THEN
                fnd_message.set_name ('XXWC', 'XXWC_ITEM_CATEGORY_MISSING');
                fnd_message.set_token ('CATEGORY', 'Purchase Flag');
                fnd_message.set_token ('CAT_VALUE', p_p_flag);
                v_error_message := 'Invalid item category  FOR  Purchase Flag=' || p_p_flag;
                --DBMS_OUTPUT.PUT_LINE (V_ERROR_MESSAGE);
                RAISE e_validation_error;
            END IF;
     		   debug('process_item_category (p_p_flag) Start ');
            process_item_category (l_inventory_item_id
                                  ,l_organization_id
                                  ,p_p_flag
                                  ,                                                                        -- p_segment1
                                   'Purchase Flag'
                                  ,x_return_status
                                  ,x_errorcode
                                  ,x_msg_count
                                  ,x_msg_data
                                  ,x_rowid);
     		   debug('process_item_category (p_p_flag) Completed ');
            IF x_return_status != fnd_api.g_ret_sts_success
            THEN
                FOR ln_index IN 1 .. x_msg_count
                LOOP
                    x_msg_data := fnd_msg_pub.get (p_msg_index => ln_index, p_encoded => fnd_api.g_false);

                    IF x_msg_data IS NOT NULL AND NVL (v_running_message1, 'null') <> NVL (x_msg_data, 'null')
                    THEN
                        v_running_message1 := v_running_message1 || ' ' || x_msg_data;
                    END IF;
                END LOOP;

                v_error_message :=
                       'error calling process_item_category for item '
                    || l_inventory_item_id
                    || ' error:'
                    || v_running_message1;
                RAISE e_validation_error;
            END IF;
        END IF;
        
        -- apps.xxwc_mv_routines_add_pkg.disable_parallelism; -- Version# 1.2 -- Version# 1.3
        
    EXCEPTION
        WHEN e_validation_error
        THEN
            --  V_ERROR_MESSAGE := SUBSTR (V_ERROR_MESSAGE, 1, 1014);

            UPDATE xxwc.xxwc_purc_mass_upload_t
               SET error_message = error_message || ' ' || v_error_message
             WHERE ROWID = x_rowid;

            -- RAISE_APPLICATION_ERROR (-20001, V_ERROR_MESSAGE);
            NULL;
            -- apps.xxwc_mv_routines_add_pkg.disable_parallelism; -- Version# 1.2 -- Version# 1.3
        WHEN OTHERS
        THEN
            fnd_message.set_name ('XXWC', 'XXWC_WHEN_OTHERS_ERROR');
            fnd_message.set_token ('SQLERROR_MSG', SUBSTR (SQLERRM, 1, 60));

            v_error_message :=
                   v_error_message
                || 'Error_Stack...'
                || DBMS_UTILITY.format_error_stack ()
                || ' Error_Backtrace...'
                || DBMS_UTILITY.format_error_backtrace ();

            -- RAISE_APPLICATION_ERROR (-20001, V_ERROR_MESSAGE);

            UPDATE xxwc.xxwc_purc_mass_upload_t
               SET error_message = error_message || ' ' || v_error_message
             WHERE ROWID = x_rowid;
            
            -- apps.xxwc_mv_routines_add_pkg.disable_parallelism; -- Version# 1.2 -- Version# 1.3
    END;

    --******************************************************************************
    --******************************************************************************
    --******************************************************************************
    --******************************************************************************

    PROCEDURE process_item_category (p_inventory_item_id                 NUMBER
                                    ,p_organization_id                   NUMBER
                                    ,p_segment1                          VARCHAR
                                    ,p_category_set_name                 VARCHAR2
                                    ,x_return_status          OUT NOCOPY VARCHAR2
                                    ,x_errorcode              OUT NOCOPY NUMBER
                                    ,x_msg_count              OUT NOCOPY NUMBER
                                    ,x_msg_data               OUT NOCOPY VARCHAR2
                                    ,x_rowid                             VARCHAR2)
    IS
        l_new_category_set_id   NUMBER := 0;
        l_new_category_id       NUMBER := 0;
        l_old_category_set_id   NUMBER := 0;
        l_old_category_id       NUMBER := 0;
        e_validation_error      EXCEPTION;
        nothing_to_change       EXCEPTION;
        l_old_value             VARCHAR2 (400);
        l_new_value             VARCHAR2 (400);
        v_error_message         CLOB;
        v_found_segment1        VARCHAR2 (400);
        v_running_message1      CLOB;

        x_message_list          error_handler.error_tbl_type;
    BEGIN
	    debug('process_item_category procedure Start ');
        l_new_value := p_segment1;
        l_old_value := find_item_cat (p_inventory_item_id, p_organization_id, p_category_set_name);

        IF l_old_value IS NOT NULL AND l_new_value IS NOT NULL AND l_old_value = l_new_value
        THEN
            RAISE nothing_to_change;
        END IF;

        IF l_old_value IS NULL AND l_new_value IS NULL
        THEN
            RAISE nothing_to_change;
        END IF;

        BEGIN
            SELECT mcv.segment1
              INTO v_found_segment1
              FROM mtl_categories_kfv mcv, mtl_category_sets mcs
             WHERE     mcs.category_set_name = p_category_set_name
                   AND mcs.structure_id = mcv.structure_id
                   AND mcv.segment1 = l_new_value
                   AND enabled_flag = 'Y';
            debug('process_item_category v_found_segment1:'||v_found_segment1);				   
        EXCEPTION
            WHEN OTHERS
            THEN
                NULL;
        END;

        IF v_found_segment1 IS NULL
        THEN
            fnd_message.set_name ('XXWC', 'XXWC_ITEM_CATEGORY_MISSING');
            fnd_message.set_token ('CATEGORY', p_category_set_name);
            fnd_message.set_token ('CAT_VALUE', l_new_value);
            v_error_message := 'Invalid item category  FOR ' || p_category_set_name || ' NEW VALUE=' || l_new_value;
            RAISE e_validation_error;
        END IF;

        --delete item category
        IF l_old_value IS NOT NULL AND l_new_value IS NULL
        THEN
            FOR r1
                IN (SELECT mcs.category_set_id, mcv.category_id
                      FROM mtl_categories_kfv mcv, mtl_category_sets mcs
                     WHERE     mcs.category_set_name = p_category_set_name --'Sales Velocity' or 'Purchase Flag in our case
                           AND mcs.structure_id = mcv.structure_id
                           AND mcv.segment1 = l_old_value
                           AND enabled_flag = 'Y')
            LOOP
            debug('inv_item_category_pvt.delete_category_assignment Start');				   			
                inv_item_category_pvt.delete_category_assignment (p_api_version         => 1.0
                                                                 ,p_init_msg_list       => 'T'
                                                                 ,p_commit              => 'T'
                                                                 ,p_inventory_item_id   => p_inventory_item_id
                                                                 ,p_organization_id     => p_organization_id
                                                                 ,p_category_set_id     => r1.category_set_id
                                                                 ,p_category_id         => r1.category_id
                                                                 ,x_return_status       => x_return_status
                                                                 ,x_msg_count           => x_msg_count
                                                                 ,x_msg_data            => x_msg_data);
            debug('inv_item_category_pvt.delete_category_assignment completed');				   			
                IF x_return_status != fnd_api.g_ret_sts_success
                THEN
                    error_handler.get_message_list (x_message_list);

                    FOR i IN 1 .. x_message_list.COUNT
                    LOOP
                        -- DBMS_OUTPUT.put_line (x_message_list (i).MESSAGE_TEXT);
                        v_running_message1 := v_running_message1 || x_message_list (i).MESSAGE_TEXT;
                    END LOOP;

                    v_error_message :=
                           'error calling  inv_item_category_pvt.delete_category_assignment for item '
                        || p_inventory_item_id
                        || ' error:'
                        || v_running_message1;
                    RAISE e_validation_error;
                END IF;
            END LOOP;
        END IF;

        IF l_old_value IS NULL AND l_new_value IS NOT NULL
        THEN
            BEGIN
                SELECT mcs.category_set_id, mcv.category_id
                  INTO l_new_category_set_id, l_new_category_id
                  FROM mtl_categories_kfv mcv, mtl_category_sets mcs
                 WHERE     mcs.category_set_name = p_category_set_name  --'Sales Velocity' or 'Purchase Flag in our case
                       AND mcs.structure_id = mcv.structure_id
                       AND mcv.segment1 = l_new_value
                       AND enabled_flag = 'Y';
            debug('l_new_category_set_id:'||l_new_category_set_id||'l_new_category_id:'||l_new_category_id);				   								   
            EXCEPTION
                WHEN OTHERS
                THEN
                    fnd_message.set_name ('XXWC', 'XXWC_ITEM_CATEGORY_MISSING');
                    fnd_message.set_token ('CATEGORY', p_category_set_name);
                    fnd_message.set_token ('CAT_VALUE', l_new_value);
                    v_error_message := 'Invalid item category  =' || l_new_value;
                    RAISE e_validation_error;
            END;
            debug('inv_item_category_pub.create_category_assignment start');
            inv_item_category_pub.create_category_assignment (p_api_version         => 1.0
                                                             ,p_init_msg_list       => 'T'
                                                             ,p_commit              => 'T'
                                                             ,p_category_id         => l_new_category_id
                                                             ,p_category_set_id     => l_new_category_set_id
                                                             ,p_inventory_item_id   => p_inventory_item_id
                                                             ,p_organization_id     => p_organization_id
                                                             ,x_return_status       => x_return_status
                                                             ,x_errorcode           => x_errorcode
                                                             ,x_msg_count           => x_msg_count
                                                             ,x_msg_data            => x_msg_data);
            debug('inv_item_category_pub.create_category_assignment complete');
            IF x_return_status != fnd_api.g_ret_sts_success
            THEN
                error_handler.get_message_list (x_message_list);

                FOR i IN 1 .. x_message_list.COUNT
                LOOP
                    -- DBMS_OUTPUT.put_line (x_message_list (i).MESSAGE_TEXT);
                    v_running_message1 := v_running_message1 || x_message_list (i).MESSAGE_TEXT;
                END LOOP;

                v_error_message :=
                       'error calling  inv_item_category_pub.create_category_assignment for item '
                    || p_inventory_item_id
                    || ' error:'
                    || v_running_message1;
                RAISE e_validation_error;
            END IF;
        END IF;

        --UPDATE
        IF l_old_value IS NOT NULL AND l_new_value IS NOT NULL AND l_old_value <> l_new_value
        THEN
            BEGIN
                SELECT mcs.category_set_id, mcv.category_id
                  INTO l_new_category_set_id, l_new_category_id
                  FROM mtl_categories_kfv mcv, mtl_category_sets mcs
                 WHERE     mcs.category_set_name = p_category_set_name  --'Sales Velocity' or 'Purchase Flag in our case
                       AND mcs.structure_id = mcv.structure_id
                       AND mcv.segment1 = l_new_value
                       AND enabled_flag = 'Y';
            debug('l_new_category_set_id1:'||l_new_category_set_id||'l_new_category_id:'||l_new_category_id);					   
            EXCEPTION
                WHEN OTHERS
                THEN
                    fnd_message.set_name ('XXWC', 'XXWC_ITEM_CATEGORY_MISSING');
                    fnd_message.set_token ('CATEGORY', p_category_set_name);
                    fnd_message.set_token ('CAT_VALUE', l_new_value);
                    v_error_message := 'Invalid item category  =' || l_new_value;
                    RAISE e_validation_error;
            END;

            BEGIN
                SELECT mcs.category_set_id, mcv.category_id
                  INTO l_old_category_set_id, l_old_category_id
                  FROM mtl_categories_kfv mcv, mtl_category_sets mcs
                 WHERE     mcs.category_set_name = p_category_set_name  --'Sales Velocity' or 'Purchase Flag in our case
                       AND mcs.structure_id = mcv.structure_id
                       AND mcv.segment1 = l_old_value
                       AND enabled_flag = 'Y';
            debug('l_old_category_set_id:'||l_old_category_set_id||'l_old_category_id:'||l_old_category_id);					   
            EXCEPTION
                WHEN OTHERS
                THEN
                    fnd_message.set_name ('XXWC', 'XXWC_ITEM_CATEGORY_MISSING');
                    fnd_message.set_token ('CATEGORY', p_category_set_name);
                    fnd_message.set_token ('CAT_VALUE', l_old_value);
                    v_error_message := 'Invalid item category  =' || l_old_value;
                    RAISE e_validation_error;
            END;

            -- update Mtl_Item_Categories set category_id
            debug('inv_item_category_pub.update_category_assignment start');					   			
            inv_item_category_pub.update_category_assignment (p_api_version         => 1.0
                                                             ,p_init_msg_list       => 'T'
                                                             ,p_commit              => 'T'
                                                             ,p_category_id         => l_new_category_id
                                                             ,p_old_category_id     => l_old_category_id
                                                             ,p_category_set_id     => l_new_category_set_id
                                                             ,p_inventory_item_id   => p_inventory_item_id
                                                             ,p_organization_id     => p_organization_id
                                                             ,x_return_status       => x_return_status
                                                             ,x_errorcode           => x_errorcode
                                                             ,x_msg_count           => x_msg_count
                                                             ,x_msg_data            => x_msg_data);
            debug('inv_item_category_pub.update_category_assignment complete');
            IF x_return_status != fnd_api.g_ret_sts_success
            THEN
                error_handler.get_message_list (x_message_list);

                FOR i IN 1 .. x_message_list.COUNT
                LOOP
                    -- DBMS_OUTPUT.put_line (x_message_list (i).MESSAGE_TEXT);
                    v_running_message1 := v_running_message1 || x_message_list (i).MESSAGE_TEXT;
                END LOOP;

                v_error_message :=
                       'error calling inv_item_category_pub.update_category_assignment for item '
                    || p_inventory_item_id
                    || ' error:'
                    || v_running_message1;
                RAISE e_validation_error;
            END IF;
        END IF;
    EXCEPTION
        WHEN e_validation_error
        THEN
            --x_error_msg := fnd_message.get ();
            UPDATE xxwc.xxwc_purc_mass_upload_t
               SET error_message = error_message || ' ' || v_error_message
             WHERE ROWID = x_rowid;

            -- RAISE_APPLICATION_ERROR (-20001, V_ERROR_MESSAGE);
            NULL;
        WHEN nothing_to_change
        THEN
            --x_error_msg := fnd_message.get ();

            NULL;
        WHEN OTHERS
        THEN
            fnd_message.set_name ('XXWC', 'XXWC_WHEN_OTHERS_ERROR');
            fnd_message.set_token ('SQLERROR_MSG', SUBSTR (SQLERRM, 1, 60));

            --fnd_message.set_name
            -- v_error_message := SQLERRM;
            v_error_message :=
                   v_error_message
                || 'Error_Stack...'
                || DBMS_UTILITY.format_error_stack ()
                || ' Error_Backtrace...'
                || DBMS_UTILITY.format_error_backtrace ();

            --   RAISE_APPLICATION_ERROR (-20001, V_ERROR_MESSAGE);
            UPDATE xxwc.xxwc_purc_mass_upload_t
               SET error_message = error_message || ' ' || v_error_message
             WHERE ROWID = x_rowid;
    END process_item_category;

    --    ********************************************************************************
    --    ********************************************************************************
    --    ********************************************************************************
    --    ********************************************************************************
    --    ********************************************************************************
    --    ********************************************************************************

    PROCEDURE assign_item_sourcing_rule (p_organization_id     IN            NUMBER
                                        ,p_inventory_item_id   IN            NUMBER
                                        ,p_sourcing_rule                     VARCHAR2
                                        ,x_return_status          OUT NOCOPY VARCHAR2
                                        ,x_msg_count              OUT NOCOPY NUMBER
                                        ,x_msg_data               OUT NOCOPY VARCHAR2
                                        ,p_rowid                             VARCHAR2)
    IS
        --  l_return_status            VARCHAR2 (1);
        --  l_msg_count                NUMBER := 0;
        -- l_msg_data                 VARCHAR2 (1000);

        l_rowid                    VARCHAR2 (164) := p_rowid;

        l_assignment_set_rec       mrp_src_assignment_pub.assignment_set_rec_type;
        l_assignment_set_val_rec   mrp_src_assignment_pub.assignment_set_val_rec_type;
        l_assignment_tbl           mrp_src_assignment_pub.assignment_tbl_type;
        l_assignment_val_tbl       mrp_src_assignment_pub.assignment_val_tbl_type;
        o_assignment_set_rec       mrp_src_assignment_pub.assignment_set_rec_type;
        o_assignment_set_val_rec   mrp_src_assignment_pub.assignment_set_val_rec_type;
        o_assignment_tbl           mrp_src_assignment_pub.assignment_tbl_type;
        o_assignment_val_tbl       mrp_src_assignment_pub.assignment_val_tbl_type;
        l_assignment_set_id        NUMBER;
        l_inventory_item_id        NUMBER;

        l_organization_id          NUMBER;

        l_ndx                      NUMBER := 0;
        l_old_sourcing_rule        VARCHAR2 (164);
        l_old_asgn_id              NUMBER;
        l_new_asgn_id              NUMBER;
        l_rule_id                  NUMBER;
        l_old_rule_id              NUMBER;
        e_validation_error         EXCEPTION;
        nothing_to_change          EXCEPTION;
        l_new_sourcing_rule        VARCHAR2 (164);
        v_error_message            CLOB;
        v_running_message1         CLOB;
        x_message_list             error_handler.error_tbl_type;

    BEGIN
        debug('assign_item_sourcing_rule start');	
        l_new_sourcing_rule := p_sourcing_rule;
        l_inventory_item_id := p_inventory_item_id;
        l_organization_id := p_organization_id;
        l_old_sourcing_rule := get_sourcing_rule (l_inventory_item_id, l_organization_id);

        IF NVL (l_old_sourcing_rule, 'NULL97') = NVL (l_new_sourcing_rule, 'NULL97')
        THEN
            RAISE nothing_to_change;
        END IF;

        -- DBMS_OUTPUT.PUT_LINE ('l_new_sourcing_rule=' || P_SOURCING_RULE);
        --DBMS_OUTPUT.PUT_LINE ('l_old_sourcing_rule=' || L_OLD_SOURCING_RULE);

        SELECT assignment_set_id
          INTO l_assignment_set_id
          FROM mrp_assignment_sets
         WHERE assignment_set_name = 'WC Default';
        debug('l_assignment_set_id:'||l_assignment_set_id);	
        IF NVL (l_old_sourcing_rule, 'NULL97') <> NVL (l_new_sourcing_rule, 'NULL97')
        THEN
            --DBMS_OUTPUT.PUT_LINE (' l_new_sourcing_rule  <> l_old_sourcing_rule');

            BEGIN
                SELECT MAX (sourcing_rule_id)
                  INTO l_rule_id
                  FROM mrp_sourcing_rules
                 WHERE sourcing_rule_name = NVL (l_new_sourcing_rule, TO_CHAR (SYSDATE, 'DDMONYYYYHH24MISS'));
            debug('l_rule_id:'||l_rule_id);					 
            EXCEPTION
                WHEN OTHERS
                THEN
                    IF l_new_sourcing_rule IS NOT NULL
                    THEN
                        fnd_message.set_name ('XXWC', 'XXWC_SOURCING_MISSING');
                        fnd_message.set_token ('XXWC_SOURCING', l_new_sourcing_rule);
                        v_error_message := 'Invalid Sourcing Rule =' || l_new_sourcing_rule;
                        RAISE e_validation_error;
                    END IF;
            END;

            BEGIN
                SELECT MAX (sourcing_rule_id)
                  INTO l_old_rule_id
                  FROM mrp_sourcing_rules
                 WHERE sourcing_rule_name = l_old_sourcing_rule;
                 debug('l_old_rule_id:'||l_old_rule_id);					 				 
            EXCEPTION
                WHEN OTHERS
                THEN
                    NULL;
            END;

            BEGIN
                SELECT sra.assignment_id
                  INTO l_old_asgn_id
                  FROM mrp_sr_assignments sra
                 WHERE     sra.sourcing_rule_id = l_old_rule_id
                       AND sra.inventory_item_id = l_inventory_item_id
                       AND sra.organization_id = l_organization_id
                       AND sra.assignment_set_id = l_assignment_set_id
                       AND sra.assignment_type = 6;
                 debug('l_old_asgn_id:'||l_old_asgn_id);					   
            EXCEPTION
                WHEN OTHERS
                THEN
                    NULL;
            END;

            BEGIN
                SELECT sra.assignment_id
                  INTO l_new_asgn_id
                  FROM mrp_sr_assignments sra
                 WHERE     sra.sourcing_rule_id = l_rule_id
                       AND sra.inventory_item_id = l_inventory_item_id
                       AND sra.organization_id = l_organization_id
                       AND sra.assignment_set_id = l_assignment_set_id
                       AND sra.assignment_type = 6;
                 debug('l_new_asgn_id:'||l_new_asgn_id);					   
                fnd_file.put_line (fnd_file.LOG, 'Sourcing Rule Already Assigned : ');
                RAISE nothing_to_change;
            EXCEPTION
                WHEN NO_DATA_FOUND
                THEN
                    NULL;
            END;

            IF l_old_asgn_id IS NOT NULL
            THEN
                l_ndx := l_ndx + 1;
                l_assignment_tbl (l_ndx).assignment_id := l_old_asgn_id;
                l_assignment_tbl (l_ndx).operation := mrp_globals.g_opr_delete;
            END IF;

            l_ndx := l_ndx + 1;
            l_assignment_tbl (l_ndx).assignment_set_id := l_assignment_set_id;
            l_assignment_tbl (l_ndx).assignment_type := 6;
            l_assignment_tbl (l_ndx).operation := mrp_globals.g_opr_create;                                  --'CREATE';
            l_assignment_tbl (l_ndx).organization_id := p_organization_id;
            l_assignment_tbl (l_ndx).inventory_item_id := p_inventory_item_id;

            l_assignment_tbl (l_ndx).sourcing_rule_id := l_rule_id;
            l_assignment_tbl (l_ndx).sourcing_rule_type := 1;
            --Sourcing Rule
            debug('mrp_src_assignment_pub.process_assignment start');					   			
            mrp_src_assignment_pub.process_assignment (p_api_version_number       => 1.0
                                                      ,p_init_msg_list            => fnd_api.g_false
                                                      ,p_return_values            => fnd_api.g_false
                                                      ,p_commit                   => fnd_api.g_false
                                                      ,x_return_status            => x_return_status
                                                      ,x_msg_count                => x_msg_count
                                                      ,x_msg_data                 => x_msg_data
                                                      ,p_assignment_set_rec       => l_assignment_set_rec
                                                      ,p_assignment_set_val_rec   => l_assignment_set_val_rec
                                                      ,p_assignment_tbl           => l_assignment_tbl
                                                      ,p_assignment_val_tbl       => l_assignment_val_tbl
                                                      ,x_assignment_set_rec       => o_assignment_set_rec
                                                      ,x_assignment_set_val_rec   => o_assignment_set_val_rec
                                                      ,x_assignment_tbl           => o_assignment_tbl
                                                      ,x_assignment_val_tbl       => o_assignment_val_tbl);
            debug('mrp_src_assignment_pub.process_assignment complete');					   			
            IF x_return_status != fnd_api.g_ret_sts_success
            THEN
                error_handler.get_message_list (x_message_list);

                FOR i IN 1 .. x_message_list.COUNT
                LOOP
                    -- DBMS_OUTPUT.put_line (x_message_list (i).MESSAGE_TEXT);
                    v_running_message1 := v_running_message1 || x_message_list (i).MESSAGE_TEXT;
                END LOOP;

                v_error_message :=
                       'error calling  mrp_src_assignment_pub.process_assignment for item '
                    || p_inventory_item_id
                    || ' error:'
                    || v_running_message1;
                RAISE e_validation_error;
            END IF;
        END IF;
    EXCEPTION
        WHEN e_validation_error
        THEN
            UPDATE xxwc.xxwc_purc_mass_upload_t
               SET error_message = error_message || ' ' || v_error_message
             WHERE ROWID = l_rowid;
        -- RAISE_APPLICATION_ERROR (-20001, V_ERROR_MESSAGE);
        WHEN nothing_to_change
        THEN
            NULL;
        WHEN OTHERS
        THEN
            fnd_message.set_name ('XXWC', 'XXWC_WHEN_OTHERS_ERROR');
            fnd_message.set_token ('SQLERROR_MSG', SUBSTR (SQLERRM, 1, 60));

            --fnd_message.set_name
            -- v_error_message := SQLERRM;
            v_error_message :=
                   v_error_message
                || 'Error_Stack...'
                || DBMS_UTILITY.format_error_stack ()
                || ' Error_Backtrace...'
                || DBMS_UTILITY.format_error_backtrace ();

            -- RAISE_APPLICATION_ERROR (-20001, V_ERROR_MESSAGE);

            UPDATE xxwc.xxwc_purc_mass_upload_t
               SET error_message = error_message || ' ' || v_error_message
             WHERE ROWID = l_rowid;
    END;

    --    ********************************************************************************
    --    ********************************************************************************
    --    ********************************************************************************
    --    ********************************************************************************
    --    ********************************************************************************

    FUNCTION find_item_cat (p_inventory_item_id NUMBER, p_organization_id NUMBER, p_category_set_name VARCHAR2)
        RETURN VARCHAR2
    IS
        purchase_flag_cat_val   VARCHAR2 (400);
    BEGIN
        SELECT mcv.segment1
          INTO purchase_flag_cat_val
          FROM mtl_categories_kfv mcv, mtl_category_sets mcs, mtl_item_categories mic
         WHERE     mcs.category_set_name = p_category_set_name                       -- 'Purchase Flag','Sales Velocity'
               AND mcs.structure_id = mcv.structure_id
               AND mic.inventory_item_id = p_inventory_item_id
               AND mic.organization_id = p_organization_id
               AND mic.category_set_id = mcs.category_set_id
               AND mic.category_id = mcv.category_id;
            debug('purchase_flag_cat_val:'||purchase_flag_cat_val);					   			
        RETURN purchase_flag_cat_val;
    EXCEPTION
        WHEN OTHERS
        THEN
            NULL;
            RETURN NULL;
    END;

    --    ********************************************************************************
    --    ********************************************************************************
    --    ********************************************************************************
    --    ********************************************************************************
    --    ********************************************************************************

    FUNCTION get_sourcing_rule (p_inventory_item_id NUMBER, p_organization_id NUMBER)
        RETURN VARCHAR2
    IS
        l_sr_name   VARCHAR2 (500);
    BEGIN
        SELECT MAX (msr.sourcing_rule_name)
          INTO l_sr_name
          FROM mrp_sr_assignments ass
              ,mrp_sr_receipt_org rco
              ,mrp_sr_source_org sso
              ,mrp_sourcing_rules msr
              ,po_vendors pov
         WHERE     1 = 1
               AND ass.inventory_item_id(+) = p_inventory_item_id
               AND ass.organization_id(+) = p_organization_id
               AND rco.sourcing_rule_id(+) = ass.sourcing_rule_id
               AND sso.sr_receipt_id(+) = rco.sr_receipt_id
               AND msr.sourcing_rule_id = ass.sourcing_rule_id
               AND pov.vendor_id(+) = sso.vendor_id;
            debug('l_sr_name:'||l_sr_name);
        RETURN l_sr_name;
    EXCEPTION
        WHEN OTHERS
        THEN
            RETURN NULL;
    END;

    --    ********************************************************************************
    --    ********************************************************************************
    --    ********************************************************************************

    --    ********************************************************************************
    --    ********************************************************************************
    --    ********************************************************************************

    PROCEDURE send_error_message (p_unique_string VARCHAR2)
    IS
        v_file_data       CLOB;
        v_dat_length      NUMBER := 0;

        v_email           VARCHAR2 (124);
        nl                NUMBER := 0;
        vs_rows           NUMBER := 0;
        ve_rows           NUMBER := 0;
        v_subject         VARCHAR2 (512);
        v_instance_name   VARCHAR2 (512);
    BEGIN
        --  VUSER_ID := NVL (TO_NUMBER (FND_PROFILE.VALUE ('USER_ID')), -1);
        --number of processed rows
        SELECT COUNT (*)
          INTO vs_rows
          FROM xxwc.xxwc_purc_mass_upload_t
         WHERE error_message IS NULL AND run_uniq = p_unique_string;

        FOR r1 IN (SELECT user_id
                     FROM xxwc.xxwc_purc_mass_upload_t
                    WHERE ROWNUM = 1 AND run_uniq = p_unique_string)
        LOOP
            FOR t IN (SELECT email_address
                        FROM fnd_user
                       WHERE user_id = r1.user_id)
            LOOP
                v_email := t.email_address;
            END LOOP;

            IF v_email IS NULL
            THEN
                v_email := 'HDSOracleDevelopers@hdsupply.com';   --'rasikha.galimova@hdsupply.com'  --Changed email id by Maha for ver1.1
            END IF;
        END LOOP;

        SELECT COUNT (*)
          INTO ve_rows
          FROM xxwc.xxwc_purc_mass_upload_t
         WHERE error_message IS NOT NULL AND run_uniq = p_unique_string;

        SELECT instance_name
          INTO v_instance_name
          FROM v$instance
         WHERE ROWNUM = 1;

        v_subject :=
               'Adi from instance: '
            || v_instance_name
            || ' Upload processed '
            || vs_rows
            || ' successfully; '
            || ',rejected '
            || ve_rows
            || ' rows;';

        FOR r
            IN (SELECT    p_org_code
                       || ','
                       || p_item_number
                       || ','
                       || p_sourcing_rule
                       || ','
                       || p_source_org
                       || ','
                       || p_source_type
                       || ','
                       || p_pplt
                       || ','
                       || p_plt
                       || ','
                       || p_flm
                       || ','
                       || p_classification
                       || ','
                       || p_planning_method
                       || ','
                       || p_min
                       || ','
                       || p_max
                       || ','
                       || p_p_flag
                       || ','
                       || p_reserve_stock
                       || ','
                       || p_buyer
                       || ','
                       || p_amu
                       || ','
                       || error_message
                           dat
                      ,LENGTH (
                              p_org_code
                           || ','
                           || p_item_number
                           || ','
                           || p_sourcing_rule
                           || ','
                           || p_source_org
                           || ','
                           || p_source_type
                           || ','
                           || p_pplt
                           || ','
                           || p_plt
                           || ','
                           || p_flm
                           || ','
                           || p_classification
                           || ','
                           || p_planning_method
                           || ','
                           || p_min
                           || ','
                           || p_max
                           || ','
                           || p_p_flag
                           || ','
                           || p_reserve_stock
                           || ','
                           || p_buyer
                           || ','
                           || p_amu
                           || ','
                           || error_message)
                           row_length
                      ,user_id
                  FROM xxwc.xxwc_purc_mass_upload_t
                 WHERE error_message IS NOT NULL AND run_uniq = p_unique_string)
        LOOP
            nl := nl + 1;
            v_file_data := v_file_data || r.dat || CHR (10);
            v_dat_length := v_dat_length + r.row_length;

            IF v_dat_length >= '25000'
            THEN
                BEGIN
                    UTL_MAIL.send_attach_varchar2 (sender         => 'donotreply@hdsupply.com'
                                                  ,recipients     => v_email
                                                  ,subject        => v_subject
                                                  ,MESSAGE        => 'see attached file for errors'
                                                  ,                   -- mime_type      IN VARCHAR2 CHARACTER SET ANY_CS
                                                   --                   DEFAULT 'text/plain; charset=us-ascii',
                                                   --  priority       IN PLS_INTEGER DEFAULT 3,
                                                   attachment     => v_file_data
                                                  ,                   -- att_mime_type  IN VARCHAR2 CHARACTER SET ANY_CS
                                                   --             DEFAULT 'text/plain; charset=us-ascii',
                                                   att_filename   => 'adi_errors.txt');
                END;

                v_file_data := NULL;
                v_dat_length := 0;
            END IF;
        END LOOP;

        IF v_file_data IS NOT NULL
        THEN
            BEGIN
                UTL_MAIL.send_attach_varchar2 (sender         => 'donotreply@hdsupply.com'
                                              ,recipients     => v_email
                                              ,subject        => v_subject
                                              ,MESSAGE        => 'see attached file for errors'
                                              ,                       -- mime_type      IN VARCHAR2 CHARACTER SET ANY_CS
                                               --                   DEFAULT 'text/plain; charset=us-ascii',
                                               --  priority       IN PLS_INTEGER DEFAULT 3,
                                               attachment     => v_file_data
                                              ,                       -- att_mime_type  IN VARCHAR2 CHARACTER SET ANY_CS
                                               --             DEFAULT 'text/plain; charset=us-ascii',
                                               att_filename   => 'adi_errors.txt');
            END;
        END IF;

        IF ve_rows = 0
        THEN
            BEGIN
                UTL_MAIL.send (sender       => 'donotreply@hdsupply.com'
                              ,recipients   => v_email
                              ,subject      => v_subject
                              ,MESSAGE      => v_subject);
            END;
        END IF;
    EXCEPTION
        WHEN OTHERS
        THEN
            l_error_message :=
                   'Error_Stack...'
                || DBMS_UTILITY.format_error_stack ()
                || ' Error_Backtrace...'
                || DBMS_UTILITY.format_error_backtrace ();

            send_developer_error;
    END;

  /*******************************************************************************
     PROCEDURE : submit

     REVISIONS:
     Ver    Date         Author                Description
     ------ -----------  ------------------    ----------------
	1.4     02-Oct-2018   P.Vamshidhar        TMS#20180806-00122 - Purchasing Mass 
	                                          upload taking more time these days 
     ******************************************************************************/
	PROCEDURE submit(x_errbuf OUT VARCHAR2, x_retcode OUT varchar2) is
	begin
	process_records;
	end;	

  /*******************************************************************************
     PROCEDURE : debug

     REVISIONS:
     Ver    Date         Author                Description
     ------ -----------  ------------------    ----------------
	1.4     02-Oct-2018   P.Vamshidhar        TMS#20180806-00122 - Purchasing Mass 
	                                          upload taking more time these days 
     ******************************************************************************/	
    procedure debug (P_LOG_MSG IN VARCHAR2) IS
    PRAGMA AUTONOMOUS_TRANSACTION;					 
	BEGIN
	 IF g_debug_flag='Y' THEN
	     INSERT INTO XXWC.XXWC_CUSTOM_LOG_TBL (SNO,PROCESS_NAME,LOG_MSG,REQUEST_ID) 
		      VALUES (XXWC.XXWC_CUSTOM_LOG_SEQ.NEXTVAL,'PURCHASE_MASS_UPLOAD',P_LOG_MSG,g_request_id);
         COMMIT;       
	 END IF;
	END;	
END;
/
