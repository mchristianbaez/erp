CREATE OR REPLACE PACKAGE BODY xxwc_om_chg_ord_wf_pkg AS
  /*************************************************************************
     $Header XXWC_OM_CHG_ORD_WF_PKG.PKG $
     Module Name: XXWC_OM_CHG_ORD_WF_PKG.PKG
  
     PURPOSE:   This package is used for customizations in OM Change order
                Workflow.
  
     REVISIONS:
     Ver     Date         Author              Description
     ------  -----------  ----------------    -------------------------
     1.0     29-Mar-2018  Naveen Kalidindi    Initial Version. 
                                              TMS# 20171113-00018
  ****************************************************************************/

  --Email Defaults
  g_dflt_email fnd_user.email_address%TYPE := 'WC-ITDEVALERTS-U1@HDSupply.com';
  g_pkg_name   VARCHAR2(50) := 'XXWC_OM_CHG_ORD_WF_PKG';

  /*************************************************************************
     PROCEDURE Name: insert_api_log
  
     PURPOSE:   Insert log into table XXWC_OM_WF_LOG_TBL
  ****************************************************************************/
  PROCEDURE insert_api_log(p_order_line_id IN NUMBER
                          ,p_api_name      IN VARCHAR2
                          ,p_api_section   IN VARCHAR2) IS
  
    --PRAGMA AUTONOMOUS_TRANSACTION;
    l_log_enabled_flag VARCHAR2(3);
    --
  BEGIN
    -- Check if Logging is enabled.
    SELECT nvl(fnd_profile.value('XXWC_OM_CHG_ORD_WF_LOG')
              ,'N')
      INTO l_log_enabled_flag
      FROM dual;
    --
    IF l_log_enabled_flag = 'Y' THEN
      -- Insert into Log table.
      INSERT INTO xxwc.xxwc_om_wf_log_tbl
        (order_line_id
        ,api_name
        ,api_section
        ,creation_date
        ,created_by
        ,last_update_date
        ,last_updated_by)
      VALUES
        (p_order_line_id
        ,p_api_name
        ,p_api_section
        ,SYSDATE
        ,fnd_global.user_id
        ,SYSDATE
        ,fnd_global.user_id);
      --
      -- COMMIT;
    END IF;
    --
  END insert_api_log;

  /*************************************************************************
     PROCEDURE Name: Enable_Customization
  
     PURPOSE:   Validation criteria or Control mechanism to choose Customi-
                -zation or use Standard flow.
  ****************************************************************************/
  PROCEDURE enable_customization(p_itemtype IN VARCHAR2
                                ,p_itemkey  IN VARCHAR2
                                ,p_actid    IN NUMBER
                                ,p_funcmode IN VARCHAR2
                                ,resultout  IN OUT VARCHAR2) IS
    --
    l_sec       VARCHAR2(200);
    l_proc_name VARCHAR2(50) := 'enable_customization';
    l_custom    VARCHAR2(1) := 'N';
    --
  BEGIN
    l_sec := 'Start';
    insert_api_log(p_order_line_id => p_itemkey
                  ,p_api_name      => l_proc_name
                  ,p_api_section   => l_sec);
  
    l_sec := '001';
    -- RUN mode - normal process execution
    IF (p_funcmode = 'RUN') THEN
    
      l_sec := '002';
      SELECT nvl(fnd_profile.value('XXWC_OM_CHG_ORD_WF_ENABLE')
                ,'N')
        INTO l_custom
        FROM dual;
      --
      l_sec := '003';
      IF l_custom = 'Y' THEN
        --
        resultout := 'COMPLETE:Y';
      
        --  
      ELSE
        resultout := 'COMPLETE:N';
      END IF;
      l_sec := '004';
    END IF;
    l_sec := '005';
    --
    RETURN;
    --
  EXCEPTION
    WHEN OTHERS THEN
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => 'XXWC_OM_CHG_ORD_WF_PKG.ENABLE_CUSTOMIZATION'
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => substr(SQLERRM
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => 'Error in procedure enable_customization for Header ID ' ||
                                                                  p_itemkey
                                          ,p_distribution_list => g_dflt_email
                                          ,p_module            => 'XXWC');
      --
      wf_core.context('enable_customization'
                     ,'EnableCustomization'
                     ,p_itemtype
                     ,p_itemkey
                     ,to_char(p_actid)
                     ,p_funcmode);
      --
      RAISE;
      --
  END enable_customization;



  /*************************************************************************
     PROCEDURE Name: populate_req_first_approver
  
     PURPOSE:   Derive first active approver from Internal Requisition Appr-
                -oval/Action history.
  ****************************************************************************/
  PROCEDURE populate_req_first_approver(p_itemtype IN VARCHAR2
                                       ,p_itemkey  IN VARCHAR2
                                       ,p_actid    IN NUMBER
                                       ,p_funcmode IN VARCHAR2
                                       ,resultout  IN OUT VARCHAR2) IS
    --
    l_sec       VARCHAR2(200);
    l_proc_name VARCHAR2(50) := 'populate_req_first_approver';
    --
    -- Select First Active Approver
    CURSOR c_select_approver(p_req_hdr_num IN VARCHAR2) IS
      SELECT fu.user_name approver
        FROM po_action_history          pah
            ,po_requisition_headers_all req
            ,fnd_user                   fu
       WHERE req.segment1 = p_req_hdr_num --'16020806'
         AND req.requisition_header_id = object_id
         AND action_code = 'APPROVE'
         AND object_type_code = 'REQUISITION'
         AND object_sub_type_code = 'INTERNAL'
         AND fu.employee_id = pah.employee_id
            -- Active Approver
         AND EXISTS (SELECT 1
                FROM per_all_people_f ppf
               WHERE ppf.person_id = pah.employee_id
                 AND trunc(SYSDATE) BETWEEN trunc(ppf.effective_start_date) AND
                     nvl(trunc(ppf.effective_end_date)
                        ,SYSDATE + 1)
                 AND ppf.current_employee_flag = 'Y')
            -- Valid Dummy Preparers
         AND EXISTS (SELECT 1
                FROM fnd_user          fu1
                    ,fnd_lookup_values lkp
               WHERE fu1.employee_id = req.preparer_id
                 AND fu1.user_name = lkp.lookup_code
                 AND lkp.lookup_type = 'XXWC_OM_CHG_ORD_WF_DUMMY_USERS'
                 AND trunc(SYSDATE) BETWEEN trunc(lkp.start_date_active) AND
                     nvl(trunc(lkp.end_date_active)
                        ,SYSDATE)
                 AND lkp.enabled_flag = 'Y')
       ORDER BY sequence_num ASC;
  
    --
    l_req_number          po_requisition_headers_all.segment1%TYPE;
    l_orig_resolving_role fnd_user.user_name%TYPE;
    l_first_approver      fnd_user.user_name%TYPE DEFAULT NULL;
    --
  BEGIN
    l_sec := 'Start';
    insert_api_log(p_order_line_id => p_itemkey
                  ,p_api_name      => l_proc_name
                  ,p_api_section   => l_sec);
  
    l_sec := '001';
    -- RUN mode - normal process execution
    IF (p_funcmode = 'RUN') THEN
      --
      l_sec        := '002';
      l_req_number := wf_engine.getitemattrtext(itemtype => p_itemtype
                                               ,itemkey  => p_itemkey
                                               ,aname    => 'REQ_HDR_NUMBER');
    
      --
      l_sec                 := '003';
      l_orig_resolving_role := wf_engine.getitemattrtext(itemtype => p_itemtype
                                                        ,itemkey  => p_itemkey
                                                        ,aname    => 'RESOLVING_ROLE');
      -- Get Approver
      l_sec := '004';
      OPEN c_select_approver(l_req_number);
    
      l_sec := '005';
      FETCH c_select_approver
        INTO l_first_approver;
    
      -- skip role assignment
      l_sec := '006';
      IF c_select_approver%NOTFOUND THEN
        l_first_approver := NULL;
      END IF;
    
      l_sec := '007';
      -- close cursor
      IF c_select_approver%ISOPEN THEN
        CLOSE c_select_approver;
      END IF;
    
      -- Assign First Approver Or Skip
      l_sec := '008';
      IF l_first_approver IS NOT NULL THEN
        --
        l_sec := '009';
        wf_engine.setitemattrtext(itemtype => p_itemtype
                                 ,itemkey  => p_itemkey
                                 ,aname    => 'RESOLVING_ROLE'
                                 ,avalue   => l_first_approver);
        --
      END IF;
      --
      l_sec := '010';
    END IF;
    --    
    l_sec     := '011';
    resultout := 'COMPLETE';
    --  
    RETURN;
    --
  EXCEPTION
    WHEN OTHERS THEN
      --
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => 'XXWC_OM_CHG_ORD_WF_PKG.POPULATE_REQ_FIRST_APPROVER'
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => substr(SQLERRM
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => 'Error in procedure Populate_Req_First_Approver  for line id ' ||
                                                                  p_itemkey
                                          ,p_distribution_list => g_dflt_email
                                          ,p_module            => 'XXWC');
      --
      wf_core.context('populate_req_first_approver'
                     ,'PopulateReqFirstApprover'
                     ,p_itemtype
                     ,p_itemkey
                     ,to_char(p_actid)
                     ,p_funcmode);
      --
      RAISE;
      --
  END populate_req_first_approver;

END xxwc_om_chg_ord_wf_pkg;
/
