CREATE OR REPLACE Package Body APPS.XXWCINV_ITEM_MASS_UPLOAD_PKG As

   /************************************************************************************
     REVISIONS:
     Ver        Date        Author                Description
     ---------  ----------  ------------------    ----------------
     1.0        11/10/2014   Vijaysrinivasan      TMS#20141002-00054 Multi org changes
   ************************************************************************************/

  -- global exception
  XXWC_ERROR                           EXCEPTION;


  -- global variables for error processing
  l_return_status                      VARCHAR2(1) := FND_API.G_Ret_Sts_Success;
  l_msg_count                          NUMBER;
  l_msg_data                           VARCHAR2(4000);
  l_msg_index_out                      NUMBER;

  -- Error DEBUG
  l_err_msg                            VARCHAR2(280);
  l_err_callfrom                       VARCHAR2(175) := 'XXWCINV_ITEM_MASS_UPLOAD_PKG';
  l_err_callpoint                      VARCHAR2(175) := 'START';
  l_distro_list                        VARCHAR2(80) := 'OracleDevelopmentGroup@hdsupply.com';
  l_module                             VARCHAR2(80);

  -- rec types moved to package level
  TYPE PO_LINE_REC IS RECORD(
      PO_HEADER_ID              Number
     ,PO_LINE_ID                NUMBER
     ,PO_LINE_LOCATION_ID       NUMBER
     ,INTERFACE_HEADER_ID       Number
     ,INTERFACE_LINE_ID         Number
     ,LINE_NUMBER               Number
     ,NOT_TO_EXCEED_PRICE       Number
     ,ALLOW_PRICE_OVERRIDE_FLAG Varchar2(1)
    , VENDOR_PRODUCT_NUM        Varchar2(40)
    , ALLOW_PRICE_OVERRIDE_FLAG_OLD Varchar2(1)
    , SHIPMENT_NUMBER           Number
    , START_DATE                DATE
    , END_DATE                  DATE
    , PRICE_OVERRIDE            Number);

  TYPE PO_LINE_TAB_TYPE IS TABLE OF PO_LINE_REC
     INDEX BY BINARY_INTEGER ;

  PO_LINE_TAB                          PO_LINE_TAB_TYPE ;
  PO_LINE_TAB_OUT                      PO_LINE_TAB_TYPE ;
  PO_LINE_TAB_NULL                     PO_LINE_TAB_TYPE ;

/**********************************************************************************************/
/* Xref_Upload - validates item cross-reference information and populate staging table        */
/**********************************************************************************************/

-- Satish U: 29-MAR-2012 : Function to Return Line ID
Function Get_PO_LINE_ID( p_PO_Header_ID  In NUmber,
                         P_Line_Number   In Number ) Return Number  IS
  l_Line_ID  Number ;
Begin
   Select po_line_id
   Into l_line_id
   From 
         --Modified the below line for TMS ## 20141002-00054  by VijaySrinivasan  on 11/10/2014 
         po_lines_all
   Where po_header_id = p_PO_Header_ID
   And line_num = p_line_number
   and org_id=FND_PROFILE.VALUE('ORG_ID')  -- Added by Pattabhi on 19-Nov-2014 for WEB ADI testing
   And Rownum = 1;

   Return l_Line_ID ;
Exception
   When Others Then
        Return Null;
End  Get_PO_LINE_ID ;

Function Get_UOM( p_PO_Header_ID  In NUmber,
                  P_Line_Number   In Number ) Return Varchar2  IS
  l_UNIT_MEAS_LOOKUP_CODE  Varchar2(25)  ;
Begin
   Select UNIT_MEAS_LOOKUP_CODE
   Into l_UNIT_MEAS_LOOKUP_CODE
   From 
        --Modified the below line for TMS ## 20141002-00054  by VijaySrinivasan  on 11/10/2014 
        po_lines_all
   Where po_header_id = p_PO_Header_ID
   And line_num = p_line_number
   AND org_id=FND_PROFILE.VALUE('ORG_ID')  -- Added by Pattabhi on 19-Nov-2014 for WEB ADI testing
   And Rownum = 1;

   Return l_UNIT_MEAS_LOOKUP_CODE ;

Exception
   When Others Then
        Return Null;
End Get_UOM ;



Procedure Xref_Upload(p_org_code               IN  VARCHAR2
                     ,p_item_number            IN  VARCHAR2
                     ,p_xref_type              IN  VARCHAR2
                     ,p_xref_code              IN  VARCHAR2 DEFAULT NULL
                     ,p_new_xref_code          IN  VARCHAR2
                     ,p_description            IN  VARCHAR2 DEFAULT NULL) As

  l_org_id                             NUMBER;
  l_item_id                            NUMBER;
  l_upc_code                           VARCHAR2(255);
  l_xref_type                          VARCHAR2(25);

Begin

  FND_MESSAGE.Clear;

  -- validate branch
  If p_org_code Is Not NULL Then
    Begin
      Select organization_id
        Into l_org_id
        From MTL_PARAMETERS
       Where organization_code = p_org_code;
    Exception
      When NO_DATA_FOUND Then
        FND_MESSAGE.SET_NAME('INV', 'INV-NO ORG INFORMATION');
        RAISE XXWC_ERROR;
    End;
  Else
    Select organization_id
      Into l_org_id
      From MTL_PARAMETERS
     Where organization_code = 'MST';
  End If;

  -- validate inventory item number
  Begin
    Select inventory_item_id
      Into l_item_id
      From MTL_SYSTEM_ITEMS_B
     Where organization_id = l_org_id
       And segment1 = p_item_number;
  Exception
    When NO_DATA_FOUND Then
      FND_MESSAGE.SET_NAME('INV', 'INV_INVALID_ITEM_ORG');
      RAISE XXWC_ERROR;
  End;

  -- validate cross-reference type
  Begin
    Select cross_reference_type
      Into l_xref_type
      From MTL_CROSS_REFERENCE_TYPES
     Where cross_reference_type = p_xref_type
       And Trunc(SYSDATE) <= Nvl(disable_date, SYSDATE+1);
  Exception
    When NO_DATA_FOUND Then
      FND_MESSAGE.Set_Name('INV', 'INV_XREF_TYPE_INACTIVE');
      FND_MESSAGE.Set_Token('CROSS_REFERENCE_TYPE',p_xref_type);
  End;

  -- validate cross-reference code
  If p_new_xref_code Is NULL Then
    FND_MESSAGE.Set_Name('INV', 'INV_XREF_NULL_COLS');
    RAISE XXWC_ERROR;
  Else
    Begin
      Select cross_reference
        Into l_upc_code
        From MTL_CROSS_REFERENCES_B
       Where cross_Reference_type = p_xref_type
         And Decode(organization_id, NULL, org_independent_flag, organization_id) = Decode(p_org_code, NULL, 'Y', l_org_id)
         And inventory_item_id = l_item_id
         And cross_reference = p_new_xref_code;
      FND_MESSAGE.SET_NAME('INV', 'INV_CROSS_REF_EXISTS');
      RAISE XXWC_ERROR;
    Exception
      When NO_DATA_FOUND Then NULL;
    End;
  End If;

  -- remove existing cross-reference for item/new xref code in staging table
  Begin
    Delete From XXWC.XXWCINV_ITEM_XREF_IFACE
     Where cross_Reference_type = p_xref_type
       And Nvl(organization_code,'~') = Nvl(p_org_code,'~')
       And item_number = p_item_number
       And new_cross_reference = p_new_xref_code;
  Exception
    When NO_DATA_FOUND Then
      NULL;
  End;

  -- populate custom staging table
  Insert Into XXWC.XXWCINV_ITEM_XREF_IFACE
         (organization_code
         ,item_number
         ,cross_reference_type
         ,old_cross_reference
         ,new_cross_reference
         ,description
         )
  Values (p_org_code                             -- organization_code
         ,p_item_number                          -- item_number
         ,p_xref_type                            -- cross_reference_type
         ,p_xref_code                            -- old_cross_reference
         ,p_new_xref_code                        -- new_cross_reference
         ,p_description                          -- description
         );

Exception
  When XXWC_ERROR Then
    NULL;
End Xref_Upload;

/**********************************************************************************************/
/* Load_Xref - populates base tables from custom staging tables                               */
/**********************************************************************************************/
Procedure Load_Xref(errbuf  OUT  VARCHAR2
                   ,retcode OUT  VARCHAR2) As

  Cursor C_Xref Is
    Select XREF.organization_code
          ,XREF.item_number
          ,XREF.cross_reference_type
          ,XREF.old_cross_reference
          ,XREF.new_cross_reference
          ,XREF.description
          ,MSI.inventory_item_id
          ,MSI.organization_id
      From XXWC.XXWCINV_ITEM_XREF_IFACE XREF,
           MTL_SYSTEM_ITEMS_B MSI,
           MTL_PARAMETERS MP
     Where XREF.item_number = MSI.segment1
       And Nvl(XREF.organization_code,'MST') = MP.organization_code
       And MP.organization_id = MSI.organization_id
     Order By XREF.cross_reference_type, XREF.organization_code, XREF.item_number;

  l_xref_tbl                           MTL_CROSS_REFERENCES_PUB.XRef_Tbl_Type;
  l_xref_id                            NUMBER;

  l_indx                               NUMBER := 0;
  l_cnt                                NUMBER := 0;

  l_return_status                      VARCHAR2(1);
  l_msg_count                          NUMBER;
  l_message_list                       Error_Handler.Error_Tbl_Type;

Begin

  l_module := 'Load_Xref';

  l_err_callpoint := 'Before Report Headers';

  FND_FILE.Put_Line(FND_FILE.Output, Lpad('XXWC Item Cross-Reference Load',50));
  FND_FILE.Put_Line(FND_FILE.Output, 'Date: '||To_Char(SYSDATE,'DD-MON-YYYY  HH:MI AM'));
  FND_FILE.Put_Line(FND_FILE.Output, ' ');
  FND_FILE.Put_Line(FND_FILE.Output, Rpad('Cross-Reference Type',20)||' '||
                                     Rpad('Organization',12)||' '||
                                     Rpad('Item Number ',40)||' '||
                                     Rpad('Cross Reference',30)||' '||
                                     Rpad('Description',40)
                   );
  FND_FILE.Put_Line(FND_FILE.Output, Rpad('----',20,'-')||' '||
                                     Rpad('----',12,'-')||' '||
                                     Rpad('----',40,'-')||' '||
                                     Rpad('----',30,'-')||' '||
                                     Rpad('----',40,'-')
                   );

  l_err_callpoint := 'Before Loop';
  For R1 In C_Xref Loop
    Begin

      -- add check to ensure that cross-reference doesn't exist in base tables
      l_err_callpoint := 'Before cross-reference check for duplicates';
      Begin
        Select cross_reference_id
          Into l_xref_id
          From MTL_CROSS_REFERENCES_B
         Where cross_reference_type = R1.cross_reference_type
           And inventory_item_id = R1.inventory_item_id
           And Decode(organization_id, NULL, org_independent_flag, organization_id) = Decode(R1.organization_code, NULL, 'Y', R1.organization_id)
           And cross_reference = R1.new_cross_reference;

        FND_FILE.Put_Line(FND_FILE.Log, 'Skipping cross-reference '||R1.cross_reference_type||' '||
                                         Nvl(R1.organization_code,'N/A')||' '||
                                         R1.item_number||' '||
                                         R1.new_cross_reference
                         );
        Raise XXWC_ERROR;
      Exception
        When NO_DATA_FOUND Then NULL;
        When TOO_MANY_ROWS Then
          RAISE XXWC_ERROR;
      End;

      l_indx := l_indx + 1;
      l_cnt := l_cnt + 1;

      l_err_callpoint := 'Before old cross-reference check';
      If R1.old_cross_reference Is Not NULL Then
        Begin
          Select cross_reference_id
            Into l_xref_id
            From MTL_CROSS_REFERENCES_B
           Where cross_reference_type = R1.cross_reference_type
             And inventory_item_id = R1.inventory_item_id
             And Decode(organization_id, NULL, org_independent_flag, organization_id) = Decode(R1.organization_code, NULL, 'Y', R1.organization_id)
             And cross_reference = R1.old_cross_reference;
          l_xref_tbl(l_indx).transaction_type := 'UPDATE';                  -- CREATE, UPDATE, DELETE
          l_xref_tbl(l_indx).cross_reference_id := l_xref_id;
        Exception
          When NO_DATA_FOUND Then
            l_xref_tbl(l_indx).transaction_type := 'CREATE';                  -- CREATE, UPDATE, DELETE
            l_xref_tbl(l_indx).cross_reference_id := NULL;
        End;
      Else
        l_xref_tbl(l_indx).transaction_type := 'CREATE';                  -- CREATE, UPDATE, DELETE
        l_xref_tbl(l_indx).cross_reference_id := NULL;
      End If;

      -- UPC codes can be generic for item across orgs
      l_err_callpoint := 'Before Org independent flag logic';
      If R1.organization_code Is NULL Then
        l_xref_tbl(l_indx).organization_id := NULL;
        l_xref_tbl(l_indx).org_independent_flag := 'Y';
      Else
        l_xref_tbl(l_indx).organization_id := R1.organization_id;
        l_xref_tbl(l_indx).org_independent_flag := 'N';
      End If;

      l_err_callpoint := 'Before populating API variables';
      l_xref_tbl(l_indx).inventory_item_id := R1.inventory_item_id;
      l_xref_tbl(l_indx).cross_reference_type := R1.cross_reference_type;
      l_xref_tbl(l_indx).cross_reference := R1.new_cross_reference;
      l_xref_tbl(l_indx).description := R1.description;

      l_xref_tbl(l_indx).last_update_date := SYSDATE;
      l_xref_tbl(l_indx).last_updated_by := FND_GLOBAL.user_id;
      l_xref_tbl(l_indx).creation_date := SYSDATE;
      l_xref_tbl(l_indx).created_by := FND_GLOBAL.user_id;
      l_xref_tbl(l_indx).last_update_login := FND_GLOBAL.login_id;
      l_xref_tbl(l_indx).request_id := FND_GLOBAL.conc_request_id;
      l_xref_tbl(l_indx).program_application_id := FND_GLOBAL.prog_appl_id;
      l_xref_tbl(l_indx).program_id :=FND_GLOBAL.conc_program_id;
      l_xref_tbl(l_indx).program_update_date := SYSDATE;

      l_err_callpoint := 'Before detail output';
      FND_FILE.Put_Line(FND_FILE.Output, Rpad(R1.cross_reference_type,20)||' '||
                                         Rpad(Nvl(R1.organization_code,'   '),12)||' '||
                                         Rpad(R1.item_number,40)||' '||
                                         Rpad(R1.new_cross_reference,30)||' '||
                                         Rpad(R1.description,40)
                       );
    Exception
      When XXWC_ERROR Then NULL;
    End;
  End Loop;

  l_err_callpoint := 'Before call to MTL_CROSS_REFERENCES_PUB.Process_XRef';
  If l_cnt > 0 Then
    MTL_CROSS_REFERENCES_PUB.Process_XRef(p_api_version        => 1.0
                                         ,p_init_msg_list      => FND_API.G_TRUE
                                         ,p_commit             => FND_API.G_FALSE
                                         ,p_xref_tbl           => l_xref_tbl
                                         ,x_return_status      => l_return_status
                                         ,x_msg_count          => l_msg_count
                                         ,x_message_list       => l_message_list
                                         );

    If l_return_status <> 'S' Then
      For ndx In 1..l_message_list.COUNT Loop
        FND_FILE.Put_Line(FND_FILE.Log, l_message_list(ndx).message_text);
      End Loop;
      RAISE XXWC_ERROR;
    End If;
  End If;

  COMMIT;
  errbuf := 'Success';
  retcode := 0;

  l_err_callpoint := 'Before clean-up of XXWCINV_ITEM_XREF_IFACE';
  -- clean up staging table after successful upload
  Delete From XXWC.XXWCINV_ITEM_XREF_IFACE;
  COMMIT;

  l_err_callpoint := 'Before End';
  FND_FILE.Put_Line(FND_FILE.Log, l_cnt||' records processed');
  FND_FILE.Put_Line(FND_FILE.Output, l_cnt||' records processed');
Exception
  When XXWC_ERROR Then
    errbuf := 'Warning';
    retcode := 1;
    ROLLBACK;
  When OTHERS Then
    errbuf := SQLERRM;
    retcode := 2;
    ROLLBACK;
    l_err_msg  := 'Error in ' || l_err_callpoint|| ' WITH  ' || substr(SQLERRM, 1, 1000);
    --dbms_output.put_line(l_fulltext);
    FND_FILE.Put_Line(FND_FILE.Log, l_err_msg);
    FND_FILE.Put_Line(FND_FILE.Output, l_err_msg);

    XXCUS_ERROR_PKG.XXCUS_ERROR_MAIN_API(p_called_from       => l_err_callfrom,
                                         p_calling           => l_err_callpoint,
                                         p_ora_error_msg     => SQLERRM,
                                         p_error_desc        => 'Error running XXWCINV_ITEM_MASS_UPLOAD_PKG package with PROGRAM ERROR',
                                         p_distribution_list => l_distro_list,
                                         p_module            => l_module);
End Load_Xref;

/**********************************************************************************************/
/* Bin_Upload- validate stock locator information and populates custom staging tables         */
/**********************************************************************************************/
Procedure Bin_Upload(p_org_code      IN VARCHAR2
                    ,p_item_number   IN VARCHAR2
                    ,p_Old_location  IN VARCHAR2 Default NULL
                    ,p_new_location  IN VARCHAR2) As

  l_error_message                      VARCHAR2(80);
  l_org_id                             NUMBER;
  l_item_id                            NUMBER;
  l_old_loc_id                         NUMBER;
  l_new_loc_id                         NUMBER;
  l_dummy                              VARCHAR2(1);

Begin

  FND_MESSAGE.Clear;

  -- validate branch
  Begin
    Select organization_id
      Into l_org_id
      From MTL_PARAMETERS
     Where organization_code = p_org_code;
  Exception
    When NO_DATA_FOUND Then
      FND_MESSAGE.SET_NAME('INV', 'INV-NO ORG INFORMATION');
      RAISE XXWC_ERROR;
  End;

  -- validate inventory item number
  Begin
    Select inventory_item_id
      Into l_item_id
      From MTL_SYSTEM_ITEMS_B
     Where organization_id = l_org_id
       And segment1 = p_item_number;
  Exception
    When NO_DATA_FOUND Then
      FND_MESSAGE.SET_NAME('INV', 'INV_INVALID_ITEM_ORG');
      RAISE XXWC_ERROR;
  End;

  Begin
    Select inventory_location_id
      Into l_new_loc_id
      From MTL_ITEM_LOCATIONS_KFV
     Where organization_id = l_org_id
       And concatenated_segments = p_new_location;
  Exception
    When NO_DATA_FOUND Then
      FND_MESSAGE.SET_NAME('INV', 'INV_INT_LOCSEGEXP');
      RAISE XXWC_ERROR;
  End;

  If p_old_location Is Not NULL Then
    Begin
      Select inventory_location_id
        Into l_old_loc_id
        From MTL_ITEM_LOCATIONS_KFV
       Where organization_id = l_org_id
         And concatenated_segments = p_old_location;
    Exception
      When NO_DATA_FOUND Then
        FND_MESSAGE.SET_NAME('INV', 'INV_INT_LOCSEGEXP');
        RAISE XXWC_ERROR;
    End;
  End If;

  Begin
    Select 'x'
      Into l_dummy
      From MTL_SECONDARY_LOCATORS
     Where organization_id = l_org_id
       And inventory_item_id = l_item_id
       And secondary_locator = l_new_loc_id;
    FND_MESSAGE.SET_NAME('INV', 'INV_LOCATOR_ASSIGNED');
    RAISE XXWC_ERROR;
  Exception
    When NO_DATA_FOUND Then
      NULL;
  End;

  -- delete any duplicate records in interface table
  Begin
    -- delete when old location matches, updating existing records
    Delete From XXWC.XXWCINV_STOCK_LOCATOR_IFACE
     Where organization_code = p_org_code
       And item_number = p_item_number
       And old_location = p_old_location;
  Exception
    When NO_DATA_FOUND Then NULL;
  End;

  Begin
    -- delete when new location matches, creating new location assignments
    Delete From XXWC.XXWCINV_STOCK_LOCATOR_IFACE
     Where organization_code = p_org_code
       And item_number = p_item_number
       And old_location Is NULL
       And new_location = p_new_location;
  Exception
    When NO_DATA_FOUND Then NULL;
  End;

  -- after all checks have been passed, insert into staging table
  Insert Into XXWC.XXWCINV_STOCK_LOCATOR_IFACE
         (organization_code
         ,item_number
         ,old_location
         ,new_location
         )
  Values (p_org_code                   -- organization_code
         ,p_item_number                -- item_number
         ,p_old_location               -- itemlocation1
         ,p_new_location               -- itemlocation1
         );

Exception
  When XXWC_ERROR Then
    NULL;
End Bin_Upload;

/**********************************************************************************************/
/* Load_Location - populates base tables from custom staging tables                               */
/**********************************************************************************************/
Procedure Load_Location(errbuf   OUT  VARCHAR2
                       ,retcode  OUT  VARCHAR2) As

  -- retrieve item / location assignment data from custom interface table
  Cursor C_Item_Locations Is
    Select SLI.organization_code,
           SLI.item_number,
           SLI.old_location,
           SLI.new_location,
           MIL.inventory_location_id,
           MSI.inventory_item_id,
           MSI.organization_id
      From XXWC.XXWCINV_STOCK_LOCATOR_IFACE SLI,
           MTL_SYSTEM_ITEMS_B MSI,
           MTL_PARAMETERS MP,
           MTL_ITEM_LOCATIONS_KFV MIL
     Where SLI.organization_code = MP.organization_code
       And SLI.item_number = MSI.segment1
       And MP.organization_id = MSI.organization_id
       And SLI.old_location = MIL.concatenated_segments(+)
     Order By SLI.organization_code,
              SLI.item_number,
              SLI.new_location;

  l_cnt                                NUMBER := 0;
  l_del_cnt                            NUMBER := 0;


Begin

  l_module := 'Load_Location';

  -- loop thru all records in interface table
  l_err_callpoint := 'Before C_Item_Locations Loop';
  For R1 IN C_Item_Locations Loop

    -- if old location provided, remove it for item
    l_err_callpoint := 'Before Old Location Removal';
    If R1.old_location Is Not NULL Then
      If R1.inventory_location_id Is NULL Then
        FND_FILE.Put_Line(FND_FILE.Log, 'Unable to find location '||R1.old_location);
      Else
        -- delete existing item / location assignment, if provided
        Begin
          Delete From MTL_SECONDARY_LOCATORS
           Where organization_id = R1.organization_id
             And inventory_item_id = R1.inventory_item_id
             And secondary_locator = R1.inventory_location_id;
--          FND_FILE.Put_Line(FND_FILE.Log, 'Deleting item / location assignment for '||R1.organization_code||' '||R1.item_number||' '||R1.old_location);
        Exception
          When NO_DATA_FOUND Then NULL;
        End;

      End If;
    End If;

--    FND_FILE.Put_Line(FND_FILE.Log, 'Creating item / location assignment for '||R1.organization_code||' '||R1.item_number||' '||R1.new_location);
    -- create item / location assignment
    l_err_callpoint := 'Before call to INV_LOC_WMS_PUB.Create_Loc_Item_Tie';
    INV_LOC_WMS_PUB.Create_Loc_Item_Tie(x_return_status              => l_return_status
                                       ,x_msg_count                  => l_msg_count
                                       ,x_msg_data                   => l_msg_data
                                       ,p_inventory_item_id          => NULL
                                       ,p_item                       => R1.item_number
                                       ,p_organization_id            => NULL
                                       ,p_organization_code          => R1.organization_code
                                       ,p_subinventory_code          => 'General'
                                       ,p_inventory_location_id      => NULL
                                       ,p_locator                    => R1.new_location
                                       ,p_status_id                  => 1
                                       ,p_par_level                  => NULL
                                       );

    -- if problems, display messages and terminate processing
    If l_return_status != 'S' Then
      FND_FILE.Put_Line(FND_FILE.Log, 'error processing item / location assignment for '||R1.organization_code||' '||R1.item_number||' '||R1.new_location);
          For ln_index IN 1 .. l_msg_count Loop
        FND_MSG_PUB.Get(p_msg_index       => ln_index,
                        p_encoded         => FND_API.G_False,
                        p_data            => l_msg_data,
                        p_msg_index_out   => l_msg_index_out);
        FND_FILE.Put_Line(FND_FILE.Log, l_msg_data);
      End Loop;
      RAISE XXWC_ERROR;
    End If;
    l_cnt := l_cnt + 1;

  End Loop;

  -- clean-up interface table
  l_err_callpoint := 'Before clean-up of XXWCINV_STOCK_LOCATOR_IFACE';
  Delete From XXWC.XXWCINV_STOCK_LOCATOR_IFACE;

  -- all or nothing processing, as any errors will rollback changes
  COMMIT;
  FND_FILE.Put_Line(FND_FILE.Log, l_del_cnt||' Item / Location Assignments Deleted');
  FND_FILE.Put_Line(FND_FILE.Log, l_cnt||' Item / Location Assignments Created');

  l_err_callpoint := 'Before End';
  errbuf := 'Success';
  retcode := 0;
Exception
  When XXWC_ERROR Then
    errbuf := 'Warning';
    retcode := 1;
    ROLLBACK;
  When OTHERS Then
    errbuf := SQLERRM;
    retcode := 2;
    ROLLBACK;
    l_err_msg  := 'Error in ' || l_err_callpoint|| ' WITH  ' || substr(SQLERRM, 1, 1000);
    --dbms_output.put_line(l_fulltext);
    FND_FILE.Put_Line(FND_FILE.Log, l_err_msg);
    FND_FILE.Put_Line(FND_FILE.Output, l_err_msg);

    XXCUS_ERROR_PKG.XXCUS_ERROR_MAIN_API(p_called_from       => l_err_callfrom,
                                         p_calling           => l_err_callpoint,
                                         p_ora_error_msg     => SQLERRM,
                                         p_error_desc        => 'Error running XXWCINV_ITEM_MASS_UPLOAD_PKG package with PROGRAM ERROR',
                                         p_distribution_list => l_distro_list,
                                         p_module            => l_module);
End Load_Location;

/**********************************************************************************************/
/* Src_Rule_Upload - validates sourcing rule assignment and populates custom staging tables   */
/**********************************************************************************************/
Procedure Src_Rule_Upload(p_org_code      IN  VARCHAR2
                         ,p_item_number   IN  VARCHAR2
                         ,p_old_src_rule  IN  VARCHAR2
                         ,p_new_src_rule  IN  VARCHAR2) As

  l_error_message                      VARCHAR2(80);
  l_org_id                             NUMBER;
  l_item_id                            NUMBER;
  l_old_src_id                         NUMBER;
  l_new_src_id                         NUMBER;
  l_dummy                              VARCHAR2(1);

Begin

  FND_MESSAGE.Clear;

  -- validate branch
  Begin
    Select organization_id
      Into l_org_id
      From MTL_PARAMETERS
     Where organization_code = p_org_code;
  Exception
    When NO_DATA_FOUND Then
      FND_MESSAGE.SET_NAME('INV', 'INV-NO ORG INFORMATION');
      RAISE XXWC_ERROR;
  End;

  -- validate inventory item number
  Begin
    Select inventory_item_id
      Into l_item_id
      From MTL_SYSTEM_ITEMS_B
     Where organization_id = l_org_id
       And segment1 = p_item_number;
  Exception
    When NO_DATA_FOUND Then
      FND_MESSAGE.SET_NAME('INV', 'INV_INVALID_ITEM_ORG');
      RAISE XXWC_ERROR;
  End;

  If p_old_src_rule Is Not NULL Then
    Begin
      Select sourcing_rule_id
        Into l_old_src_id
        From MRP_SOURCING_RULES
       Where sourcing_rule_name = p_old_src_rule;

      Begin
        Select SR.sourcing_rule_id
          Into l_old_src_id
          from MRP_SOURCING_RULES SR,
               MRP_SR_ASSIGNMENTS_V SRA
         Where SRA.sourcing_rule_id = SR.sourcing_rule_id
           And SR.sourcing_rule_id = l_old_src_id
           And SRA.inventory_item_id = l_item_id
           And ((SRA.organization_id = l_org_id And SRA.assignment_type = 6) Or         -- 3=item  6=item/org
               (SRA.organization_id Is NULL And SRA.assignment_type = 3));
      Exception
        When NO_DATA_FOUND Then
          FND_MESSAGE.SET_NAME('XXWC', 'XXWC_OLD_SR_NOT_ASGN');
          RAISE XXWC_ERROR;
        When TOO_MANY_ROWS Then
          FND_MESSAGE.SET_NAME('XXWC', 'XXWC_OLD_SR_NOT_ASGN');
          RAISE XXWC_ERROR;
      End;
    Exception
      When NO_DATA_FOUND Then
        FND_MESSAGE.SET_NAME('XXWC', 'XXWC_INVALID_OLD_SRC_RULE');
        RAISE XXWC_ERROR;
    End;
  End If;

  Begin
    Select sourcing_rule_id
      Into l_new_src_id
      From MRP_SOURCING_RULES
     Where sourcing_rule_name = p_new_src_rule;
  Exception
    When NO_DATA_FOUND Then
      FND_MESSAGE.SET_NAME('XXWC', 'XXWC_INVALID_NEW_SRC_RULE');
      RAISE XXWC_ERROR;
  End;

  Begin
    Select SR.sourcing_rule_id
      Into l_new_src_id
      from MRP_SOURCING_RULES SR,
           MRP_SR_ASSIGNMENTS_V SRA
     Where SRA.sourcing_rule_id = SR.sourcing_rule_id
       And SR.sourcing_rule_id = l_new_src_id
       And SRA.inventory_item_id = l_item_Id
       And ((SRA.organization_id = l_org_id And SRA.assignment_type = 6) Or         -- 3=item  6=item/org
            (SRA.organization_id Is NULL And SRA.assignment_type = 3));

    FND_MESSAGE.SET_NAME('PO', 'PO_PDOI_RULE_NAME_UNIQ');
    FND_MESSAGE.Set_Token('VALUE',p_new_src_rule);
    FND_MESSAGE.Set_Token('VALUE',p_item_number);
    RAISE XXWC_ERROR;
  Exception
    When TOO_MANY_ROWS THen
      FND_MESSAGE.SET_NAME('PO', 'PO_PDOI_RULE_NAME_UNIQ');
      FND_MESSAGE.Set_Token('VALUE',p_new_src_rule);
      FND_MESSAGE.Set_Token('VALUE',p_item_number);
      RAISE XXWC_ERROR;
    When NO_DATA_FOUND Then NULL;
  End;

  -- delete any existing record in staging table
  Begin
    Delete From XXWC.XXWCMRP_SRC_RULE_ASSGN_IFACE
     Where organization_code = p_Org_code
       And item_number = p_item_number
       And old_sourcing_rule = p_old_src_rule;
  Exception
    When NO_DATA_FOUND Then NULL;
  End;

  -- populate staging table
  Insert Into XXWC.XXWCMRP_SRC_RULE_ASSGN_IFACE
         (organization_code
         ,item_number
         ,old_sourcing_rule
         ,new_sourcing_rule
         )
  Values (p_org_code                   -- organization_code
         ,p_item_number                -- item_number
         ,p_old_src_rule               -- old_sourcing_rule
         ,p_new_src_rule               -- new_sourcing_rule
         );

Exception
  When XXWC_ERROR Then
    NULL;
End Src_Rule_Upload;

/**********************************************************************************************/
/* Load_Sourcing_Assignment - populates base tables from custom staging tables                               */
/**********************************************************************************************/
Procedure Load_Sourcing_Assignments(errbuf   OUT  VARCHAR2
                                   ,retcode  OUT  VARCHAR2) As

  Cursor C1 Is
    Select SRI.organization_code
          ,SRI.item_number
          ,SRI.old_sourcing_rule
          ,SRI.new_sourcing_rule
          ,MSI.organization_id
          ,MSI.inventory_item_id
     From XXWC.XXWCMRP_SRC_RULE_ASSGN_IFACE SRI,
          MTL_PARAMETERS MP,
          MTL_SYSTEM_ITEMS_B MSI
    Where SRI.organization_code = MP.organization_code
      And SRI.item_number = MSI.segment1
      And MP.organization_id = MSI.organization_id
    Order by SRI.organization_code,
             SRI.item_number;

  l_assignment_set_rec                 MRP_SRC_ASSIGNMENT_PUB.Assignment_Set_Rec_Type := MRP_SRC_ASSIGNMENT_PUB.G_MISS_ASSIGNMENT_SET_REC;
  l_assignment_set_val_rec             MRP_SRC_ASSIGNMENT_PUB.Assignment_Set_Val_Rec_Type := MRP_SRC_ASSIGNMENT_PUB.G_MISS_ASSIGNMENT_SET_VAL_REC;
  l_assignment_tbl                     MRP_SRC_ASSIGNMENT_PUB.Assignment_Tbl_Type := MRP_SRC_ASSIGNMENT_PUB.G_MISS_ASSIGNMENT_TBL;
  l_assignment_val_tbl                 MRP_SRC_ASSIGNMENT_PUB.Assignment_Val_Tbl_Type := MRP_SRC_ASSIGNMENT_PUB.G_MISS_ASSIGNMENT_VAL_TBL;
  x_assignment_set_rec                 MRP_SRC_ASSIGNMENT_PUB.Assignment_Set_Rec_Type;
  x_assignment_set_val_rec             MRP_SRC_ASSIGNMENT_PUB.Assignment_Set_Val_Rec_Type;
  x_assignment_tbl                     MRP_SRC_ASSIGNMENT_PUB.Assignment_Tbl_Type;
  x_assignment_val_tbl                 MRP_SRC_ASSIGNMENT_PUB.Assignment_Val_Tbl_Type;

  l_assignment_set_id                  NUMBER;

  l_new_src_id                         NUMBER;
  l_old_src_id                         NUMBER;
  l_old_asgn_id                        NUMBER;

  l_ndx                                NUMBER := 0;

  l_cnt                                NUMBER := 0;

Begin

  l_module := 'Load_Sourcing_Assignments';

  errbuf := 'Success';
  retcode := 0;

  l_err_callpoint := 'Before lookup of assignment_set_name = WC Default';
  Begin
    Select assignment_set_id
      Into l_assignment_set_id
      From MRP_ASSIGNMENT_SETS
     Where assignment_set_name = 'WC Default';
  Exception
    When NO_DATA_FOUND Then
      FND_FILE.Put_Line(FND_FILE.Log, 'Unable to find assignment set WC Default');
      RAISE XXWC_ERROR;
  End;

  l_err_callpoint := 'Before Loop';
  For R1 In C1 Loop

    -- initialize input table
    l_assignment_tbl := MRP_SRC_ASSIGNMENT_PUB.G_MISS_ASSIGNMENT_TBL;
    l_old_asgn_id := NULL;
    l_old_src_id := NULL;
    l_ndx := 0;

    Begin

      -- validate sourcing rules
      l_err_callpoint := 'Before validation of sourcing rule';
      Begin
        Select sourcing_rule_id
          Into l_new_src_id
          From MRP_SOURCING_RULES
         Where sourcing_rule_name = R1.new_sourcing_rule;
      Exception
        When NO_DATA_FOUND Then
          FND_FILE.Put_Line(FND_FILE.Log, 'Invalid sourcing rule : '||R1.new_sourcing_rule);
          RAISE XXWC_ERROR;
      End;

      l_err_callpoint := 'Before validation of old sourcing rule assigned';
      If R1.old_sourcing_rule Is Not NULL Then
        Begin
          Select sourcing_rule_id
            Into l_old_src_id
            From MRP_SOURCING_RULES
           Where sourcing_rule_name = R1.old_sourcing_rule;

          -- verify that old sourcing rule does exist for item/org
          Begin
            Select SRA.assignment_id
              Into l_old_asgn_id
              From MRP_SR_ASSIGNMENTS SRA
             Where SRA.sourcing_rule_id = l_old_src_id
               And SRA.inventory_item_id = R1.inventory_item_id
               And SRA.organization_id = R1.organization_id
               And SRA.assignment_set_id = l_assignment_set_id
               And SRA.assignment_type = 6;

          Exception
            When NO_DATA_FOUND Then
              l_old_asgn_id := NULL;
          End;

        Exception
          When NO_DATA_FOUND Then
            FND_FILE.Put_Line(FND_FILE.Log, 'Invalid sourcing rule : '||R1.old_sourcing_rule);
            RAISE XXWC_ERROR;
        End;
      End If;

      -- verify that new sourcing rule doesn't already exist for item/org
      l_err_callpoint := 'Before new sourcing rule duplicate check';
      Begin
        Select SRA.sourcing_rule_id
          Into l_new_src_id
          From MRP_SR_ASSIGNMENTS SRA
         Where SRA.sourcing_rule_id = l_new_src_id
           And SRA.inventory_item_id = R1.inventory_item_id
           And SRA.organization_id = R1.organization_id
           And SRA.assignment_set_id = l_assignment_set_id
           And SRA.assignment_type = 6;

        FND_FILE.Put_Line(FND_FILE.Log, 'Sourcing Rule Already Assigned : '||R1.organization_code||' '||R1.item_number||' '||R1.new_sourcing_rule);
        RAISE XXWC_ERROR;
      Exception
        When NO_DATA_FOUND Then NULL;
      End;

      l_cnt := l_cnt + 1;

      l_err_callpoint := 'Before old sourcing rule Delete';
      If l_old_asgn_id Is Not NULL Then
        l_ndx := l_ndx + 1;
        l_assignment_tbl(l_ndx).assignment_id := l_old_asgn_id;
        l_assignment_tbl(l_ndx).operation := MRP_GLOBALS.G_OPR_DELETE;
      End If;


      -- define assignment
      l_err_callpoint := 'Before new sourcing rule creation';
      l_ndx := l_ndx + 1;
      l_assignment_tbl(l_ndx).assignment_set_id := l_assignment_set_id;
      l_assignment_tbl(l_ndx).inventory_item_id := R1.inventory_item_id;
      l_assignment_tbl(l_ndx).organization_id := R1.organization_id;
      l_assignment_tbl(l_ndx).assignment_type := 6;                         -- 6=item/org assignment  3=item  4=organization  1=global
      l_assignment_tbl(l_ndx).sourcing_rule_id := l_new_src_id;
      l_assignment_tbl(l_ndx).sourcing_rule_type := 1;                      -- 1=sourcing rule
      l_assignment_tbl(l_ndx).operation := MRP_GLOBALS.G_OPR_CREATE;


      FND_FILE.Put_Line(FND_FILE.Log, 'Processing '||R1.organization_code||' '||R1.item_number||' '||R1.old_sourcing_rule||' '||R1.new_sourcing_rule);

      -- update sourcing rule assignment
      l_err_callpoint := 'Before execution of MRP_SRC_ASSIGNMENT_PUB.Process_Assignment';
      MRP_SRC_ASSIGNMENT_PUB.Process_Assignment(p_api_version_number            => 1.0
                                               ,p_init_msg_list                 => FND_API.G_TRUE
                                               ,p_return_values                 => FND_API.G_FALSE
                                               ,p_commit                        => FND_API.G_FALSE
                                               ,x_return_status                 => l_return_status
                                               ,x_msg_count                     => l_msg_count
                                               ,x_msg_data                      => l_msg_data
                                               ,p_assignment_set_rec            => l_assignment_set_rec
                                               ,p_assignment_set_val_rec        => l_assignment_set_val_rec
                                               ,p_assignment_tbl                => l_assignment_tbl
                                               ,p_assignment_val_tbl            => l_assignment_val_tbl
                                               ,x_assignment_set_rec            => x_assignment_set_rec
                                               ,x_assignment_set_val_rec        => x_assignment_set_val_rec
                                               ,x_assignment_tbl                => x_assignment_tbl
                                               ,x_assignment_val_tbl            => x_assignment_val_tbl
                                               );

      -- if problems, display messages and terminate processing
      If l_return_status != FND_API.G_RET_STS_SUCCESS Then
        FND_FILE.Put_Line(FND_FILE.Log, 'Error Processing '||R1.organization_code||' '||R1.item_number||' '||R1.old_sourcing_rule||' '||R1.new_sourcing_rule);
            For ln_index IN 1 .. l_msg_count Loop
          FND_MSG_PUB.Get(p_msg_index       => ln_index,
                          p_encoded         => FND_API.G_False,
                          p_data            => l_msg_data,
                          p_msg_index_out   => l_msg_index_out);
          FND_FILE.Put_Line(FND_FILE.Log, l_msg_data);
        End Loop;
        RAISE XXWC_ERROR;
      End If;

    Exception
      When XXWC_ERROR Then
        errbuf := 'Warning';
              retcode := 1;
    End;
  End Loop;

  -- Delete records interface table
  l_err_callpoint := 'Before clean-up of XXWCMRP_SRC_RULE_ASSGN_IFACE';
  Delete From XXWC.XXWCMRP_SRC_RULE_ASSGN_IFACE;

  COMMIT;
  FND_FILE.Put_Line(FND_FILE.Log, l_cnt||' Sourcing Rule Assignments Processed');

Exception
  When XXWC_ERROR Then
    errbuf := 'Warning';
    retcode := 1;
    ROLLBACK;
  When OTHERS Then
    errbuf := SQLERRM;
    retcode := 2;
    ROLLBACK;
    l_err_msg  := 'Error in ' || l_err_callpoint|| ' WITH  ' || substr(SQLERRM, 1, 1000);
    --dbms_output.put_line(l_fulltext);
    FND_FILE.Put_Line(FND_FILE.Log, l_err_msg);
    FND_FILE.Put_Line(FND_FILE.Output, l_err_msg);

    XXCUS_ERROR_PKG.XXCUS_ERROR_MAIN_API(p_called_from       => l_err_callfrom,
                                         p_calling           => l_err_callpoint,
                                         p_ora_error_msg     => SQLERRM,
                                         p_error_desc        => 'Error running XXWCINV_ITEM_MASS_UPLOAD_PKG package with PROGRAM ERROR',
                                         p_distribution_list => l_distro_list,
                                         p_module            => l_module);

End Load_Sourcing_Assignments;

/**********************************************************************************************/
/* BPA_Upload- validates BPA / price breaks and populates custom staging tables               */
/**********************************************************************************************/
Procedure BPA_Upload(p_ou_id            IN  NUMBER,
                       p_document_number  IN  VARCHAR2,
                       p_supplier         IN  VARCHAR2 DEFAULT NULL,
                       p_line_number      IN  NUMBER,
                       p_item_number      IN  VARCHAR2 DEFAULT NULL,
                       p_description      IN  VARCHAR2 DEFAULT NULL,
                       p_supplier_item    IN  VARCHAR2 DEFAULT NULL,
                       p_shipment_number  IN  NUMBER DEFAULT NULL,
                       p_org_code         IN  VARCHAR2,
                       p_unit_price       IN  NUMBER,
                       p_quantity         IN  NUMBER,
                       p_price_Override   IN  NUMBER,
                       p_start_date       IN  DATE,
                       p_end_date         IN  DATE,
                       P_ALLOW_PRICE_OVERRIDE_FLAG IN Varchar2,
                       P_NOT_TO_EXCEED_PRICE IN NUMBER,
                       P_CASCADE_FLAG IN VARCHAR2 ) Is -- added new parameter Shankar 06-Aug-2013

  l_error_message                      VARCHAR2(100);
  l_dummy                              VARCHAR2(1);
  l_header_id                          NUMBER;
  l_line_id                            NUMBER;
  l_line_location_id                   NUMBER;
  l_OPERATION                          VARCHAR2(30) ;
  l_Row_Count                          Number ;
  l_Item_Number                        Varchar2(40) ;

Begin

  -- validate operating unit id
  Begin
    Select 'x'
      Into l_dummy
      From HR_OPERATING_UNITS
     Where organization_id = p_ou_id;
  Exception
    When NO_DATA_FOUND Then
      FND_MESSAGE.SET_NAME('INV', 'INV_NO_OU');
      RAISE XXWC_ERROR;
  End;

  -- validate document number
  Begin
     Select po_header_id
     Into l_header_id
     From 
          --Modified the below line for TMS ## 20141002-00054  by VijaySrinivasan  on 11/10/2014 
          po_headers_all
     Where segment1 = p_document_number
     And org_id = p_ou_id
     And type_lookup_code = 'BLANKET';
  Exception
    When NO_DATA_FOUND Then
      FND_MESSAGE.SET_NAME('GML', 'PO_NOT_BPO');
      RAISE XXWC_ERROR;
  End;

  -- validate line number
  -- Satish U: 04-APR-2012
  -- Check if Line Number is Null If so raise exception
  If P_Line_Number is NULL Then
     FND_MESSAGE.SET_NAME('XXWC', 'XXWC_NULL_ERROR');
     FND_MESSAGE.SET_TOKEN('ITEM_NAME', 'Line Number');
     RAISE XXWC_ERROR;
  End If ;

  If p_Line_Number is Not Null Then
      Begin
        Select po_line_id
        Into l_line_id
         From 
              --Modified the below line for TMS ## 20141002-00054  by VijaySrinivasan  on 11/10/2014 
              po_lines_all
         Where po_header_id = l_header_id
           And line_num = p_line_number
		   AND org_id=FND_PROFILE.VALUE('ORG_ID');  -- Added by Pattabhi on 19-Nov-2014 for WEB ADI testing;
         l_OPERATION := 'UPDATE';
      Exception
        When NO_DATA_FOUND Then
          l_line_id  := NULL;
          --FND_MESSAGE.SET_NAME('PO', 'PO_ALL_CHOOSE_BLANKET_LINE');
          --RAISE XXWC_ERROR;
      End;
  Else
     l_Line_ID := NULL;
     l_OPERATION := 'CREATE';
  End If;

  -- validate line number
  -- Satish U: 04-APR-2012
  -- Check if Line Number is Null If so raise exception


  -- Satish U: 28-MAR-2012 : Added l_Line_ID is Not Null
  If p_shipment_number Is Not NULL
     And l_line_id Is Not NULL Then
    -- validate shipment number
    Begin
      Select line_location_id
        Into l_line_location_id
        From 
             --Modified the below line for TMS ## 20141002-00054  by VijaySrinivasan  on 11/10/2014 
             po_line_locations_all
       Where po_header_id = l_header_id
         And po_line_id = l_line_id
         And po_release_id Is NULL
         And shipment_type = 'PRICE BREAK'
         And shipment_num = p_shipment_number
		 AND org_id=FND_PROFILE.VALUE('ORG_ID');  -- Added by Pattabhi on 19-Nov-2014 for WEB ADI testing;
    Exception
      When NO_DATA_FOUND Then
         Null;
        --FND_MESSAGE.SET_NAME('PO', 'PO_CHNG_INVALID_SHIPMENT_NUM');
        --RAISE XXWC_ERROR;
    End;
  End If;


  -- validate inventory organization
  If p_org_code Is Not NULL Then
    Begin
      Select 'x'
        Into l_dummy
        From MTL_PARAMETERS
       Where organization_code = p_org_code;
    Exception
      When NO_DATA_FOUND Then
        FND_MESSAGE.SET_NAME('INV', 'INV-NO ORG INFORMATION');
        RAISE XXWC_ERROR;
    End;
  End If;

  -- validate start and end dates
  If p_start_date Is Not NULL
  And p_end_date < p_start_date Then
    FND_MESSAGE.SET_NAME('PO', 'PO_END_DATE_GE_START_DATE');
    RAISE XXWC_ERROR;
  End If;

  -- validate line level unit price
  If Nvl(p_unit_price,0) < 0 Then
    FND_MESSAGE.SET_NAME('FND', 'FWK_TBX_T_UNITPRICE_MIN');
    RAISE XXWC_ERROR;
  End If;

  -- validate price break quantity
  -- Satish U:  Do not make Qty required 29-Mar-2012
  -- If Nvl(p_quantity,0) <=0 Then
  If Nvl(p_quantity,0) < 0 Then
    FND_MESSAGE.SET_NAME('PO', 'PO_RI_QUANTITY_LE_0');
    RAISE XXWC_ERROR;
  End If;

  -- validate price break unit price
  -- Default Unit_Price If PRice Override is NULL
  If NVl(p_price_override,p_unit_price) <= 0 Then
    FND_MESSAGE.SET_NAME('XXWC', 'XXWC_PRICE_BREAK_PRICE_GE_0');
    RAISE XXWC_ERROR;
  End If;

  -- Satish U: 05-APR-2012 : Check values of Not To Exceed Price Values.
  If P_NOT_TO_EXCEED_PRICE < p_unit_price  Then
     FND_MESSAGE.SET_NAME('XXWC', 'XXWC_NOT_TO_EXCEED_PRICE_ERROR');
     FND_MESSAGE.SET_TOKEN('ITEM_NAME', 'Unit Price');
     RAISE XXWC_ERROR;
  Elsif P_NOT_TO_EXCEED_PRICE < p_price_Override Then
     FND_MESSAGE.SET_NAME('XXWC', 'XXWC_NOT_TO_EXCEED_PRICE_ERROR');
     FND_MESSAGE.SET_TOKEN('ITEM_NAME', 'Price Override');
     RAISE XXWC_ERROR;
  End If;

  -- Satish U : 25-JUN-2012
  -- Not To Excced_Price must be NULL if Allow_price_Override_flag is 'N'
  If NVL(p_ALLOW_PRICE_OVERRIDE_FLAG,'N') = 'N' And p_NOT_TO_EXCEED_PRICE IS NOT NULL THEN
     FND_MESSAGE.SET_NAME('PO', 'PO_PDOI_EXCEED_PRICE_NULL');
     FND_MESSAGE.SET_TOKEN('VALUE', p_NOT_TO_EXCEED_PRICE);
     RAISE XXWC_ERROR;
  End If;



 IF P_ITEM_NUMBER is NOT NULL THEN
    Begin
        Select Segment1
        Into l_Item_Number
        From Mtl_System_Items_B
        Where Segment1 = p_item_number
        And Rownum = 1;
    Exception
       When Others Then
         FND_MESSAGE.SET_NAME('XXWC', 'XXWC_ITEM_NOT_FOUND');
         FND_MESSAGE.SET_TOKEN('ITEM_NUMBER', p_item_number);
         RAISE XXWC_ERROR;

    End ;

 END IF;

  -- delete any existing changes for same BPA line
  Begin
    Delete From 
                --Modified the below line for TMS ## 20141002-00054  by VijaySrinivasan  on 11/10/2014 
                xxwcpo_bpa_interface
     Where org_id          = p_ou_id
       And document_number = p_document_number
       And line_number     = p_line_number
       And NVL(shipment_number,0)  = NVL(p_shipment_number,0);
  Exception
    When NO_DATA_FOUND Then NULL;
  End;


  --Satish U: 26-MAR- Delete any new Lines From BPA_INTERFACE
  -- delete any existing changes for same BPA line
  Begin
     Delete From 
                 --Modified the below line for TMS ## 20141002-00054  by VijaySrinivasan  on 11/10/2014 
                 xxwcpo_bpa_interface
     Where org_id = p_ou_id
     And document_number = p_document_number
     And line_number is NULL
     --And shipment_number = p_shipment_number
     AND Item_Number =  p_Item_Number;
  Exception
    When NO_DATA_FOUND Then NULL;
  End;

  -- store changes in staging table
  Insert Into 
         --Modified the below line for TMS ## 20141002-00054  by VijaySrinivasan  on 11/10/2014 
         XXWCPO_BPA_INTERFACE
         (org_id
         ,document_number
         ,line_number
         ,shipment_number
         ,organization_code
         ,unit_price
         ,price_override
         ,quantity
         ,start_date
         ,end_date
         ,ALLOW_PRICE_OVERRIDE_FLAG
         ,NOT_TO_EXCEED_PRICE
         ,ITEM_NUMBER
         ,DESCRIPTION
         ,SUPPLIER_ITEM
         ,CASCADE_FLAG
         )
  Values (p_ou_id                      -- org_id
         ,p_document_number            -- document_number
         ,p_line_number                -- line_number
         ,p_shipment_number            -- shipment_number
         ,p_org_code                   -- organization_code
         ,p_unit_price                 -- unit_price
         ,NVl(p_price_override,p_unit_price) -- price_override -- Default Unit PRice If PRice Over Ride is NULL 11-APR-2011
         ,p_quantity                   -- quantity
         ,p_start_date                 -- start_date
         ,p_end_Date                   -- end_date
         ,Upper(p_ALLOW_PRICE_OVERRIDE_FLAG) -- 25-JUN-2012
         ,p_NOT_TO_EXCEED_PRICE
         ,P_ITEM_NUMBER
         ,P_DESCRIPTION
         ,P_SUPPLIER_ITEM
         ,P_CASCADE_FLAG
         );

Exception
  When XXWC_ERROR Then
    NULL;
End BPA_Upload;

/**********************************************************************************************/
/* Create_BPA - creates BPA header for Sites that dont have a global BPA                      */
  -- Shankar   06-Aug-2013   Ticket 20130716-00932 -- 
/**********************************************************************************************/
Procedure create_bpa (i_document_number in varchar2, i_org_id in number, i_item_number in varchar2, i_unit_price in number)
is
  l_batch_id                           NUMBER := FND_GLOBAL.CONC_REQUEST_ID;
  l_request_id                         NUMBER;
  l_conc_req_phase                     VARCHAR2(80);
  l_conc_req_status                    VARCHAR2(80);
  l_conc_req_dev_phase                 VARCHAR2(80);
  l_conc_req_dev_status                VARCHAR2(80);
  l_conc_req_message                   VARCHAR2(80);
  l_ship_to                            NUMBER;
  l_agent_id                           NUMBER;
  l_Inventory_item_ID                  NUMBER;
  l_UOM_code                           VARCHAR2(50);
  cursor c1
      is
  select a.vendor_id, a.vendor_site_id
    from 
         --Modified the below line for TMS ## 20141002-00054  by VijaySrinivasan  on 11/10/2014 
         ap_supplier_sites_all a, 
         --Modified the below line for TMS ## 20141002-00054  by VijaySrinivasan  on 11/10/2014 
         po_headers_all b
   where b.segment1=i_document_number
       and b.org_id=i_org_id
       and b.vendor_id=a.vendor_id
       and a.purchasing_site_flag='Y'
       and nvl(a.inactive_date,sysdate+1)>=sysdate
    and not exists
   (select 1 
      from 
           --Modified the below line for TMS ## 20141002-00054  by VijaySrinivasan  on 11/10/2014 
           po_headers_all c
     where c.org_id=i_org_id
       and c.type_lookup_code='BLANKET'
       and c.global_agreement_flag='Y'
       and c.vendor_site_id=a.vendor_site_id);    
begin
  IF i_item_number is not null then
  for c1_rec in c1
   loop
          select ship_to_location_id, agent_id
            into l_ship_to, l_agent_id
            from 
                 --Modified the below line for TMS ## 20141002-00054  by VijaySrinivasan  on 11/10/2014 
                 po_headers_all
           where segment1=i_document_number
             and org_id=i_org_id;
             
      FND_FILE.Put_Line(FND_FILE.Log, '151 : Creating interface header record '||' for '||c1_rec.vendor_site_id);
       insert into po_headers_interface
            (interface_header_id,
            action,
            org_id,
            document_type_code,
            vendor_id,
            vendor_site_id,
            ship_to_location_id,
            agent_id,
            batch_id)
            values
            (po_headers_interface_s.nextval,
            'ORIGINAL',
            i_org_id,
            'BLANKET',
            c1_rec.vendor_id,
            c1_rec.vendor_site_id,
            l_ship_to,
            l_agent_id,
            l_batch_id);

            Select msi.Inventory_Item_ID , mum.UNIT_OF_MEASURE
            Into l_Inventory_item_ID , l_UOM_code
            From Mtl_System_Items_B Msi,
                 MTL_UNITS_OF_MEASURE_TL mum
            Where msi.Segment1 = I_Item_Number
            And  msi.Primary_Uom_Code = mum.Uom_Code
            And msi.Organization_ID = 222 ;


             Insert Into PO_LINES_INTERFACE
               (interface_header_id
               ,interface_line_id
               --,po_header_id
               ,line_num
               --,po_line_id
               ,unit_price
               ,unit_of_measure
               ,price_break_lookup_code
               ,action
               ,process_code
               ,Item_ID
               ,Item
              -- ,ALLOW_PRICE_OVERRIDE_FLAG
              -- ,NOT_TO_EXCEED_PRICE
              --, VENDOR_PRODUCT_NUM
               )
        Values (po_headers_interface_s.currval                -- interface_header_id
                ,po_lines_interface_s.nextval                  -- interface_line_id
               --,line_rec1.po_header_id                      -- po_header_id
               ,1      -- line_num
               --,l_po_line_id                        -- po_line_id  Changed it from R1.PO_LINE_ID
               ,i_unit_price
               ,l_UOM_COde                           -- unit_of_measure -- R1.unit_meas_lookup_code
               ,'NON CUMULATIVE'                     -- price_break_lookup_code
               ,'ADD'                             -- 'UPDATE'    -- action
               ,'PENDING'                            -- process_code
               , l_Inventory_Item_ID
               ,i_ITEM_NUMBER                     -- Item Num
              -- , Line_Rec1.ALLOW_PRICE_OVERRIDE_FLAG
               --, Line_Rec1.NOT_TO_EXCEED_PRICE
               --, DECODE(l_Action, 'UPDATE', NULL, Line_Rec1.Supplier_Item)
               );
    end loop;     
       COMMIT;
    
   l_err_callpoint := 'Before execution of FND_REQUEST.Submit_Request for POXPDOI for missing headers';
   l_request_id := FND_REQUEST.Submit_Request(application => 'PO'
                                                  ,program     => 'POXPDOI'
                                                  ,description => NULL
                                                  ,start_time  => NULL
                                                  ,sub_request => FALSE
                                                  ,argument1   => NULL           -- default buyer
                                                  ,argument2   => 'Blanket'      -- document type
                                                  ,argument3   => NULL           -- document sub-type
                                                  ,argument4   => 'N'            -- create or update items
                                                  ,argument5   => 'N'            -- create sourcing rules
                                                --  ,argument6   => 'APPROVED'     -- approval status -- Shankar 12/12/2012
                                                  ,argument6   => 'INCOMPLETE'     -- approval status
                                                  ,argument7   => NULL           -- release generation method
                                                  ,argument8   => To_Char(l_batch_id, 'FM9999999999')     -- batch id
                                                  ,argument9   => i_ORG_ID    -- operating unit
                                                  ,argument10  => 'Y'  -- global agreement
                                                  ,argument11  => 'Y'            -- enable sourcing level
                                                  ,argument12  => NULL
                                                  ,argument13  => NULL
                                                  ,argument14  => NULL
                                                  );
    COMMIT;

    If Nvl(l_request_id,0) = 0 Then
       FND_FILE.Put_Line(FND_FILE.Log, '210 Unable to submit Import Pricing Catalog concurrent program');
       FND_FILE.Put_Line(FND_FILE.Log, '220 Manually submit Import Pricing Catalog concurrent program for batch-id'||l_batch_id);
       --errbuf := 'Warning';
       --retcode := 1;
    Else
       FND_FILE.Put_Line(FND_FILE.Log, '230 : Submitted Import Pricing Catalog concurrent program, request-id = '||l_request_id);
    End If;

    -- wait on program completion and report any error messages
    l_err_callpoint := 'Before FND_CONCURRENT.Wait_For_Request for POXPDOI';
    If FND_CONCURRENT.Wait_For_Request(request_id => l_request_id
                                        ,interval   => 30
                                        ,max_wait   => 0
                                        ,phase      => l_conc_req_phase
                                        ,status     => l_conc_req_status
                                        ,dev_phase  => l_conc_req_dev_phase
                                        ,dev_status => l_conc_req_dev_status
                                        ,message    => l_conc_req_message) Then

       FND_FILE.Put_Line(FND_FILE.Log, '240 : Checking for any error messages');

       l_err_callpoint := 'Before returning C_Errors cursor';
    End If;
  END IF;  
end create_bpa;

/**********************************************************************************************/
/* Cascade_BPA - populates base tables for Cascade flag is Y                                  */
  -- Shankar   06-Aug-2013   Ticket 20130716-00932 -- Add cascade flag and cascade changes to all BPA for the supplier
/**********************************************************************************************/
Procedure cascade_line (i_document_number in varchar2,
                        i_org_id in number,
                        i_item_number in varchar2,
                        i_organization_id in number,
                        i_batch_id in number,
                        o_po_line_tab out po_line_tab_type)
is
  l_organization_id                    NUMBER;

  l_interface_header_id                NUMBER;
  l_interface_line_id                  NUMBER;
  l_pb_interface_line_id               NUMBER;
  l_line_location_id                   NUMBER;
  l_interface_line_location_id         Number ;

  l_batch_id                           NUMBER := FND_GLOBAL.CONC_REQUEST_ID;
  l_global_agreement_flag              VARCHAR2(1);

  l_conc_req_phase                     VARCHAR2(80);
  l_conc_req_status                    VARCHAR2(80);
  l_conc_req_dev_phase                 VARCHAR2(80);
  l_conc_req_dev_status                VARCHAR2(80);
  l_conc_req_message                   VARCHAR2(80);

  l_dummy                              VARCHAR2(1);
  l_cnt                                NUMBER := 0;

  l_request_id                         NUMBER;
  l_Action                             VARCHAR2(30) ;
  l_Inventory_Item_ID                  Number ;
  l_Line_Num                           Number ;
  l_PO_Line_ID                         Number ;
  l_UOM_Code                           Varchar2(25) ;
  l_MST_ORG_ID                         Number := FND_PROFILE.VALUE('XXWC_ITEM_MASTER_ORG');
  C_SHIPMENT_TYPE      CONSTANT        VARCHAR2(30) := 'PRICE BREAK';
  l_PO_LINE_ROW_COUNT1                 Number := 0;
  l_ROWCOUNT                           Number := 0;
  L_OU_ORG_ID                          Number ;
  l_BPA_ROWS_FOUND                     Varchar2(1) := 'N';
  l_Primary_UOM                        Varchar2(30) ;
  l_Item_Description                   Varchar2(240);
  l_shipment_num                           Number;
  l_max_int_sn                         Number;
  l_max_ll_sn                          Number;

     Cursor Line_Cur1 
       IS
        Select ph1.ORG_ID, ph1.segment1 DOCUMENT_NUMBER, bpa.line_number int_line_number,
        (select pl.line_num 
           from po_lines_v pl 
          where pl.po_header_id=ph1.po_header_id
            and pl.item_number=bpa.item_number 
            and nvl(pl.closed_code,'X') not in ('CLOSED', 'FINALLY CLOSED') 
            and rownum=1) LINE_NUMBER, 
           bpa.ITEM_NUMBER, bpa.DESCRIPTION,bpa.shipment_number,
           bpa.SUPPLIER_ITEM, bpa.unit_price, bpa.NOT_TO_EXCEED_PRICE, bpa.ALLOW_PRICE_OVERRIDE_FLAG
           , bpa.ORGANIZATION_CODE, mp.Organization_ID, bpa.cascade_flag, ph1.po_header_id,BPA.price_override
        From 
             --Modified the below line for TMS ## 20141002-00054  by VijaySrinivasan  on 11/10/2014 
             XXWCPO_BPA_INTERFACE BPA ,
             mtl_Parameters mp,
             --Modified the below line for TMS ## 20141002-00054  by VijaySrinivasan  on 11/10/2014 
             po_headers_all ph,
             --Modified the below line for TMS ## 20141002-00054  by VijaySrinivasan  on 11/10/2014 
             po_headers_all ph1
        Where  bpa.DOCUMENT_NUMBER = i_DOCUMENT_NUMBER
        AND    bpa.Org_ID          = i_ORG_ID
		AND    bpa.org_id          = ph.org_id
        And    bpa.Organization_Code = mp.Organization_Code(+)
        And  bpa.rowid  = (
           Select Min(bpa2.ROWID )
           From   
                  --Modified the below line for TMS ## 20141002-00054  by VijaySrinivasan  on 11/10/2014 
                  XXWCPO_BPA_INTERFACE BPA2
           Where  BPA2.DOCUMENT_NUMBER = i_DOCUMENT_NUMBER
           AND    BPA2.Org_ID          = i_ORG_ID
           AND    BPA2.LINE_NUMBER     = BPA.LINE_NUMBER 
           AND    BPA2.item_number     = i_item_number) 
        And bpa.document_number=ph.segment1
        AND ph.vendor_id=ph1.vendor_id
        AND ph.type_lookup_code='BLANKET'
        AND ph1.type_lookup_code='BLANKET'
        AND nvl(ph1.closed_code,'X') not in ('CLOSED', 'FINALLY CLOSED')
        AND ph1.global_agreement_flag='Y'
        AND BPA.item_number=i_item_number;
        
     Cursor Ship_Cur ( P_DOCUMENT_NUMBER IN VARCHAR2,
                       P_ORG_ID          IN NUMBER ,
                       p_LINE_NUMBER     IN NUMBER) IS
        SELECT ORG_ID, DOCUMENT_NUMBER, LINE_NUMBER, SHIPMENT_NUMBER, ORGANIZATION_CODE,
           PRICE_OVERRIDE, QUANTITY,START_DATE, END_DATE, unit_price
        FROM 
             --Modified the below line for TMS ## 20141002-00054  by VijaySrinivasan  on 11/10/2014 
             XXWCPO_BPA_INTERFACE BPA
        WHERE  DOCUMENT_NUMBER = P_DOCUMENT_NUMBER
        AND    Org_ID          = P_ORG_ID
        AND    LINE_NUMBER     = P_LINE_NUMBER
        And    SHIPMENT_NUMBER is Not Null;        
        
begin
       FOR Line_Rec1 IN Line_Cur1 
        LOOP
         IF line_rec1.document_number <> i_document_number then 
         BEGIN
          select interface_header_id
            into l_interface_header_id
            from po_headers_interface
           where document_type_code='BLANKET'
             and document_num= line_rec1.document_number
             and process_code='PENDING'
             and rownum=1;
         EXCEPTION
            when others then
            l_global_agreement_flag := 'Y';
            l_BPA_ROWS_FOUND  := 'Y';

              l_cnt := l_cnt + 1;
              FND_FILE.Put_Line(FND_FILE.Log, '140 : Global Agreement Flag ');
              -- retrieve global agreement flag for use in Import Price Lists concurrent program
              l_err_callpoint := 'Before Global Agreement Flag retrieval';
              Begin
                 Select Nvl(global_agreement_flag,'N')
                 Into l_global_agreement_flag
                 From 
                      --Modified the below line for TMS ## 20141002-00054  by VijaySrinivasan  on 11/10/2014 
                      PO_HEADERS_all
                 Where po_header_Id = line_rec1.po_header_id
				 and org_id=FND_PROFILE.VALUE('ORG_ID');  -- Added by Pattabhi on 19-Nov-2014 for WEB ADI testing;
              End;

              -- check to see if BPA header exists in interface table, otherwise create record in interface table
              l_err_callpoint := 'Before creation of PDOI header record';
              FND_FILE.Put_Line(FND_FILE.Log, '150 : Generate Interface header ID ');
              Select PO_HEADERS_INTERFACE_S.nextval
              Into l_interface_header_id
              From DUAL;

              FND_FILE.Put_Line(FND_FILE.Log, '151 : Creating interface header record '||l_interface_header_id||' for '||line_rec1.document_number);
              FND_FILE.Put_Line(FND_FILE.Log, '152 : Insert into PO_HEADERS_INTERFACE: '||line_rec1.document_number);
              l_OU_ORG_ID := line_rec1.org_id ;
              Insert Into PO_HEADERS_INTERFACE
                (interface_header_id
                ,action
                ,document_type_code
                ,document_num
                ,po_header_id
                ,org_id
                ,process_code
                ,interface_source_code
                ,batch_id
                ,last_update_date
                ,last_updated_by
                )
               Values (l_interface_header_id            -- interface_header_id
                   ,'UPDATE'                         -- action
                   ,'BLANKET'                        -- document_type_code
                   ,line_rec1.document_number               -- document_number
                   ,line_rec1.po_header_id                  -- po_header_id
                   ,line_rec1.org_id                        -- org_id
                   ,'PENDING'                        -- process_code
                   ,'WebADI'                         -- interface_source_code
                   ,i_batch_id                       -- batch_id
                   ,SYSDATE                          -- last_update_date
                   ,FND_GLOBAL.User_Id               -- last_updated_by
                   );
               FND_FILE.Put_Line(FND_FILE.Log, '154 :  After Inserting into PO_HEADERS_INTERFACE ');
         END;
         
         l_Inventory_item_ID     := NULL;
         l_PO_Line_ID := Get_PO_LINE_ID(line_rec1.po_header_id, Line_Rec1.Line_Number ) ;
         l_UOM_COde   := Get_UOM(line_rec1.po_header_id, Line_Rec1.Line_Number) ;
         -- create price break line
         Select PO_LINES_INTERFACE_S.nextval
         Into l_interface_line_id
         From DUAL;

         FND_FILE.Put_Line(FND_FILE.Log, '163: Creating interface line price-break record '||l_interface_line_id||' for '||Line_Rec1.line_number);
         l_err_callpoint := ' Before Getting Inventory Item ID ';

         Begin
            Select msi.Inventory_Item_ID , mum.UNIT_OF_MEASURE, msi.description
            Into l_Inventory_item_ID , l_Primary_UOM, l_Item_Description
            From Mtl_System_Items_B Msi,
                 MTL_UNITS_OF_MEASURE_TL mum
            Where msi.Segment1 = Line_Rec1.Item_Number
            And  msi.Primary_Uom_Code = mum.Uom_Code
            And msi.Organization_ID = NVL(Line_Rec1.organization_id, l_MST_ORG_ID )  ;
            FND_FILE.Put_Line(FND_FILE.Log, '163.01: Inventory Item Id is  '||l_Inventory_item_ID);

            If l_UOM_Code is NULL Then
               l_UOM_COde := l_Primary_UOM ;
            End IF;
            FND_FILE.Put_Line(FND_FILE.Log, '163.02: UOM Code '|| l_UOM_COde);
            FND_FILE.Put_Line(FND_FILE.Log, '163.03: Item Description '|| Line_Rec1.Description);

         Exception
            When Others Then
               l_Inventory_Item_ID := NULL;
               l_err_msg  := '164 : Item Number ' || Line_Rec1.Item_Number || 'not found in Organization :' || Line_Rec1.organization_code ;
               Raise XXWC_ERROR  ;
         End;

         -- Satish U: 26-MAR-2012
         --l_Action := NULL;
         -- If l_line_number IS NULL Then
         l_err_callpoint := ' Before Getting Line Number ';
         If l_PO_Line_ID IS NULL   Then
            --SatishU: 25-JUN-2012  Changed Action Code to 'ADD'
            --l_Action := 'ORIGINAL';
            l_Action := 'ADD';
            l_Line_Num := Line_Rec1.Line_Number ;

         Elsif l_PO_Line_ID IS NOT NULL  AND Line_Rec1.Line_Number is NOT NULL Then
            l_Line_Num := Line_Rec1.Line_Number ;
            l_Action := 'UPDATE';
         End If;

         FND_FILE.Put_Line(FND_FILE.Log, '169 : Action Code on the Line ' || l_Action);
        l_err_callpoint := ' Before Inserting into PO_LINES_INTERFACE ';

        FND_FILE.Put_Line(FND_FILE.Log, '170 : Insert into PO_LINES_INTERFACE ');

        Insert Into PO_LINES_INTERFACE
               (interface_header_id
               ,interface_line_id
               ,po_header_id
               ,line_num
              -- ,po_line_id
               ,unit_price
               ,unit_of_measure
               ,price_break_lookup_code
               ,action
               ,process_code
               ,Item_ID
               ,Item
               ,ALLOW_PRICE_OVERRIDE_FLAG
               ,NOT_TO_EXCEED_PRICE
              , VENDOR_PRODUCT_NUM
               , line_loc_populated_flag
               )
        Values (l_interface_header_id                -- interface_header_id
               ,l_interface_line_id                  -- interface_line_id
               ,line_rec1.po_header_id                      -- po_header_id
               ,NVL(Line_Rec1.line_number, l_Line_Num)      -- line_num
               --,l_po_line_id                        -- po_line_id  Changed it from R1.PO_LINE_ID
               ,Line_Rec1.Unit_Price                    -- unit_price
               ,l_UOM_COde                           -- unit_of_measure -- R1.unit_meas_lookup_code
               ,'NON CUMULATIVE'                     -- price_break_lookup_code
               ,l_Action                             -- 'UPDATE'    -- action
               ,'PENDING'                            -- process_code
               , l_Inventory_Item_ID
               ,Line_Rec1.ITEM_NUMBER                     -- Item Num
               , Line_Rec1.ALLOW_PRICE_OVERRIDE_FLAG
               , Line_Rec1.NOT_TO_EXCEED_PRICE
               , DECODE(l_Action, 'UPDATE', NULL, Line_Rec1.Supplier_Item)
               , decode(l_po_Line_id, null,decode(line_rec1.shipment_number,NULL,NULL,'Y'),null)
               );
         FND_FILE.Put_Line(FND_FILE.Log, '171 : After Insert into PO_LINES_INTERFACE ');
         l_err_callpoint := ' Before Inserting Into Collection ';

          IF Line_Rec1.ALLOW_PRICE_OVERRIDE_FLAG IS NOT NULL Or Line_Rec1.NOT_TO_EXCEED_PRICE IS NOT NULL
           Or Line_Rec1.Supplier_Item Is Not NULL or line_rec1.price_override is not null Then
            -- Increment the PO_LINE ROW COUNT
            l_PO_LINE_ROW_COUNT1  := l_PO_LINE_ROW_COUNT1 + 1;
            FND_FILE.Put_Line(FND_FILE.Log, '171.5 : insert to collection PL:' || l_PO_LINE_ROW_COUNT1 ||':' ||line_rec1.po_header_id||':'||l_interface_Line_id);
            -- Insert a Record inTo PLSQL Table
            O_PO_LINE_TAB(l_PO_LINE_ROW_COUNT1).PO_HEADER_ID                := line_rec1.po_header_id ;
            O_PO_LINE_TAB(l_PO_LINE_ROW_COUNT1).INTERFACE_HEADER_ID         := l_interface_header_id;
            O_PO_LINE_TAB(l_PO_LINE_ROW_COUNT1).INTERFACE_LINE_ID           := l_interface_Line_id;
            O_PO_LINE_TAB(l_PO_LINE_ROW_COUNT1).LINE_NUMBER                 := Line_Rec1.line_number ;
           -- O_PO_LINE_TAB(l_PO_LINE_ROW_COUNT1).SHIPMENT_NUMBER             := Line_Rec1.SHIPMENT_NUMBER ;
            O_PO_LINE_TAB(l_PO_LINE_ROW_COUNT1).NOT_TO_EXCEED_PRICE         := Line_Rec1.NOT_TO_EXCEED_PRICE ;
            O_PO_LINE_TAB(l_PO_LINE_ROW_COUNT1).PRICE_OVERRIDE              := Line_Rec1.PRICE_OVERRIDE ;
            O_PO_LINE_TAB(l_PO_LINE_ROW_COUNT1).ALLOW_PRICE_OVERRIDE_FLAG   := Line_Rec1.ALLOW_PRICE_OVERRIDE_FLAG ;
            O_PO_LINE_TAB(l_PO_LINE_ROW_COUNT1).VENDOR_PRODUCT_NUM          := Line_Rec1.Supplier_Item ;
            O_PO_LINE_TAB(l_PO_LINE_ROW_COUNT1).PO_LINE_ID                  := l_po_line_id ;

            IF l_po_line_id  IS NOT NULL THen
               Begin
                  Select Allow_Price_Override_Flag
                  Into  O_PO_LINE_TAB(l_PO_LINE_ROW_COUNT1).ALLOW_PRICE_OVERRIDE_FLAG_OLD
                  From 
                       --Modified the below line for TMS ## 20141002-00054  by VijaySrinivasan  on 11/10/2014 
                       PO_LINES
                  Where PO_LINE_ID = l_po_line_id ;

                  -- Check if Old Value and New Values are Different : 25-JUN-2012
                  If NVL(O_PO_LINE_TAB(l_PO_LINE_ROW_COUNT1).ALLOW_PRICE_OVERRIDE_FLAG_OLD, 'NULL') <>
                     NVL(O_PO_LINE_TAB(l_PO_LINE_ROW_COUNT1).ALLOW_PRICE_OVERRIDE_FLAG, 'NULL') Then

                       Update 
                              --Modified the below line for TMS ## 20141002-00054  by VijaySrinivasan  on 11/10/2014 
                              PO_LINES
                          Set ALLOW_PRICE_OVERRIDE_FLAG = O_PO_LINE_TAB(l_PO_LINE_ROW_COUNT1).ALLOW_PRICE_OVERRIDE_FLAG
                       Where PO_LINE_ID = l_PO_LINE_ID ;

                  End If;
               Exception
                  When Others Then
                     FND_FILE.Put_Line(FND_FILE.Log, '179.1 : When others exception');
               End ;
            End If;
          End If;
              For Ship_Rec1 IN Ship_Cur ( i_DOCUMENT_NUMBER ,
                                   Line_Rec1.ORG_ID ,
                                   Line_Rec1.Int_Line_Number) Loop
                  FND_FILE.Put_Line(FND_FILE.Log, '180 : Looping Through Shipment Record');
                  --initialize Variables
                  l_line_location_id      := NULL;

                  -- create price break line
                  Select PO_LINE_LOCATIONS_INTERFACE_S.nextval
                  Into l_interface_line_location_id
                  From DUAL;


                  If Ship_Rec1.shipment_number Is Not NULL Then
                      Begin
                        IF ship_rec1.organization_code is not null then
                         select organization_id
                           into l_organization_id
                           from mtl_parameters
                          where organization_code=ship_rec1.organization_code;
                        ELSE
                         l_organization_id := null;
                        END IF;
                         IF l_po_line_id is not null then
                         -- Satish U: Changed R1.PO_Line_ID to l_PO_Line_Id
                         FND_FILE.Put_Line(FND_FILE.Log, '190 : Get Line Location ID for line Id ' || l_po_line_id);
                         Select line_location_id
                         Into l_line_location_id
                         From 
                              --Modified the below line for TMS ## 20141002-00054  by VijaySrinivasan  on 11/10/2014 
                              po_line_locations_all
                         Where po_line_id = l_po_line_id
						 AND org_id=FND_PROFILE.VALUE('ORG_ID')  -- Added by Pattabhi on 19-Nov-2014 for WEB ADI testing
                         and ((ship_to_organization_id=l_organization_id
                              and rownum=1)
                              or
                              (l_organization_id is null
                               And shipment_num = Ship_Rec1.shipment_number));
                         l_Action := 'UPDATE';
                         ELSE
                          l_line_location_id := null;
                          l_Action := 'ADD';
                         END IF;
                         --Ship_Rec.shipment_number := NULL;
                      Exception
                         When NO_DATA_FOUND Then
                            l_line_location_id := NULL;
                            --Satishu: 25-JUN-2012  Changed Action Code from Original to ADD
                            --l_Action := 'ORIGINAL';
                            l_Action := 'ADD';
                      End;
                      
                      
                      IF l_po_line_id is not null and l_action = 'ADD'  then
                       select nvl(max(shipment_num),0)
                         into l_max_int_sn
                         from po_line_locations_interface
                        where interface_header_id=l_interface_header_id
                          and interface_line_id=l_interface_line_id;
                          
                       select nvl(max(shipment_num),0)
                         into l_max_ll_sn
                         from 
                              --Modified the below line for TMS ## 20141002-00054  by VijaySrinivasan  on 11/10/2014 
                              po_line_locations_all
                        where po_line_id=l_po_line_id
						AND org_id=FND_PROFILE.VALUE('ORG_ID');  -- Added by Pattabhi on 19-Nov-2014 for WEB ADI testing;
                        
                        IF l_max_int_sn > l_max_ll_sn then
                          l_shipment_num := l_max_int_sn;
                        ELSE
                          l_shipment_num := l_max_ll_sn;
                        END IF;
                      ELSE
                          l_shipment_num := ship_rec1.shipment_number;                   
                      END IF;
                      
                  Else
                      l_line_location_id := NULL;
                  End If;
                  FND_FILE.Put_Line(FND_FILE.Log, '190.01 : Action Code on the shipment Line ' || l_Action);

                  FND_FILE.Put_Line(FND_FILE.Log, '191 : Inserting Record for shipments ');
                  FND_FILE.Put_Line(FND_FILE.Log, '192 : Interface Line ID' || l_interface_line_id );
                  FND_FILE.Put_Line(FND_FILE.Log, '193 : Line Location ID' || l_line_location_id );
                  FND_FILE.Put_Line(FND_FILE.Log, '194 : Organization Code' || Ship_Rec1.ORGANIZATION_CODE );
                  FND_FILE.Put_Line(FND_FILE.Log, '195 : Quantity' || Ship_Rec1.QUANTITY );
                 IF l_line_location_id is null then 
                 FND_FILE.Put_Line(FND_FILE.Log, '191 : Inserting Record for shipments POLL ');
                 Insert into PO.PO_LINE_LOCATIONS_INTERFACE
                   (INTERFACE_LINE_LOCATION_ID,
                    INTERFACE_HEADER_ID,
                    INTERFACE_LINE_ID, 
                    LINE_LOCATION_ID,             
                    SHIPMENT_TYPE,
                    SHIPMENT_NUM,
                    SHIP_TO_ORGANIZATION_code,               
                    PRICE_OVERRIDE,
                    quantity,
                    start_date,
                    end_date,
                    CREATION_DATE)
                 Values
                   (l_interface_line_location_id,---    INTERFACE_LINE_LOCATION_ID,
                    l_interface_header_id,    ---        INTERFACE_HEADER_ID,
                    l_interface_line_id,    ---        INTERFACE_LINE_ID,
                    l_line_location_id,             
                    'PRICE BREAK',    ---        SHIPMENT_TYPE,
                    l_shipment_num+1, --ship_rec1.shipment_number,    ---        SHIPMENT_NUM,
                    Ship_Rec1.organization_Code,
                    ship_Rec1.PRICE_OVERRIDE,
                    ship_rec1.quantity,
                    ship_rec1.start_date,
                    ship_rec1.end_date,
                    SYSDATE); 
                    
                      update po_lines_interface
                         set line_loc_populated_flag='Y'
                       where interface_line_id=l_interface_line_id
                         and line_location_id is null;

                  ELSE  
                   FND_FILE.Put_Line(FND_FILE.Log, '191 : Inserting Record for shipments POL ');
                  -- create price break line
                  Select PO_LINES_INTERFACE_S.nextval
                  Into l_pb_interface_line_id
                  From DUAL;

                  Insert Into PO_LINES_INTERFACE
                       (interface_header_id
                       ,interface_line_id
                       ,po_header_id
                       ,line_num
                       ,po_line_id
                       ,line_location_id
                       ,ship_to_organization_Code
                       ,shipment_type
                       ,quantity
                       ,unit_price
                       ,unit_of_measure
                       ,effective_date
                       ,expiration_date
                      -- ,price_break_flag
                       --,price_break_lookup_code
                       ,action
                       --,process_code
                      -- ,Item_ID
                       ,Item
                       --,ALLOW_PRICE_OVERRIDE_FLAG
                      ,NOT_TO_EXCEED_PRICE
                      -- , VENDOR_PRODUCT_NUM
                       ,Shipment_Num
                       )
                  Values (l_interface_header_id                -- interface_header_id
                       ,l_pb_interface_line_id                  -- interface_line_id
                       ,line_rec1.po_header_id                      -- po_header_id
                       ,NVL(Line_Rec1.line_number, l_Line_Num)      -- line_num
                       ,l_po_line_id                        -- po_line_id  Changed it from R1.PO_LINE_ID
                       ,l_line_location_id                   -- line_location_id
                       ,Ship_Rec1.organization_Code                 -- ship_to_organization_id
                       ,C_SHIPMENT_TYPE                        -- shipment_type
                       , Ship_Rec1.quantity                          -- quantity
                       , ship_rec1.unit_price--,Ship_Rec.PRICE_OVERRIDE               -- Line_Rec.Unit_Price                    -- unit_price
                       ,l_UOM_COde                           -- unit_of_measure -- R1.unit_meas_lookup_code
                       ,Ship_Rec1.start_date                        -- effective_date
                       ,Ship_Rec1.end_date                          -- expiration_date
                      -- ,'Y'
                       --,'NON CUMULATIVE'                     -- price_break_lookup_code
                       ,l_Action                             -- 'UPDATE'    -- action
                      -- ,'PENDING'                            -- process_code
                      -- , l_Inventory_Item_ID
                       , Line_Rec1.ITEM_NUMBER
                      -- , Line_Rec.ALLOW_PRICE_OVERRIDE_FLAG
                       , Line_Rec1.NOT_TO_EXCEED_PRICE
                      -- ,Line_Rec.Supplier_Item
                       ,decode(l_action,'ADD',Ship_Rec1.shipment_number,null)
                   );
                   
                l_PO_LINE_ROW_COUNT1  := l_PO_LINE_ROW_COUNT1 + 1;
                
                FND_FILE.Put_Line(FND_FILE.Log, '195.1 : insert to collection PL:' || l_PO_LINE_ROW_COUNT1||':' ||line_rec1.po_header_id||':'||l_pb_interface_Line_id||':'||l_line_location_id);
                -- Insert a Record inTo PLSQL Table
                O_PO_LINE_TAB(l_PO_LINE_ROW_COUNT1).PO_HEADER_ID                := line_rec1.po_header_id ;
                O_PO_LINE_TAB(l_PO_LINE_ROW_COUNT1).INTERFACE_HEADER_ID         := l_interface_header_id;
                O_PO_LINE_TAB(l_PO_LINE_ROW_COUNT1).INTERFACE_LINE_ID           := l_pb_interface_line_id;
                O_PO_LINE_TAB(l_PO_LINE_ROW_COUNT1).PO_LINE_LOCATION_ID         := l_line_location_id ;
                O_PO_LINE_TAB(l_PO_LINE_ROW_COUNT1).LINE_NUMBER                 := Line_Rec1.line_number ;
                O_PO_LINE_TAB(l_PO_LINE_ROW_COUNT1).SHIPMENT_NUMBER             := ship_Rec1.shipment_number ;
                O_PO_LINE_TAB(l_PO_LINE_ROW_COUNT1).PRICE_OVERRIDE              := ship_Rec1.PRICE_OVERRIDE ;
                O_PO_LINE_TAB(l_PO_LINE_ROW_COUNT1).NOT_TO_EXCEED_PRICE         := Line_Rec1.NOT_TO_EXCEED_PRICE ;
                O_PO_LINE_TAB(l_PO_LINE_ROW_COUNT1).ALLOW_PRICE_OVERRIDE_FLAG   := Line_Rec1.ALLOW_PRICE_OVERRIDE_FLAG ;
                O_PO_LINE_TAB(l_PO_LINE_ROW_COUNT1).VENDOR_PRODUCT_NUM          := Line_Rec1.Supplier_Item ;
                O_PO_LINE_TAB(l_PO_LINE_ROW_COUNT1).PO_LINE_ID                  := l_po_line_id ;
                O_PO_LINE_TAB(l_PO_LINE_ROW_COUNT1).start_date                  := Ship_Rec1.start_date ;    
                O_PO_LINE_TAB(l_PO_LINE_ROW_COUNT1).end_date                    := Ship_Rec1.end_date ;
                END IF;   
                End Loop ; -- PO Line Shipment Cursor
          END IF;
         End Loop ; -- PO Line Shipment Cursor
         
end cascade_line;
/**********************************************************************************************/
/* Load_BPA - populates base tables from custom staging tables                               */
/**********************************************************************************************/
Procedure Load_BPA(errbuf   OUT  VARCHAR2,
                   retcode  OUT  VARCHAR2,
                   i_doc_number in VARCHAR2) Is

  -- Satish U: 03-APR-2012 : Added Outer Joine on Warehouse, as warehouse is not required.
  -- Satish U: 05-APR_2012 : Commented Following SQL Query
  -- Shankar   13-Dec-2012 :  Added a doc parameter to run the process for a single BPA--TMS 20121217-00557
  -- Shankar   06-Aug-2013   Ticket 20130716-00932 -- Add cascade flag and cascade changes to all BPA for the supplier
   -- Define a cursor to get distinc BPA Number for  Staging table
   Cursor C1 Is
      Select Distinct BPA.org_id
          ,BPA.document_number
          ,PH.po_header_id
      From 
           --Modified the below line for TMS ## 20141002-00054  by VijaySrinivasan  on 11/10/2014 
           XXWCPO_BPA_INTERFACE BPA,
           --Modified the below line for TMS ## 20141002-00054  by VijaySrinivasan  on 11/10/2014 
           PO_HEADERS_all PH
      Where BPA.document_number = PH.segment1
      And BPA.org_id = PH.org_id
	  AND ph.org_id=FND_PROFILE.VALUE('ORG_ID')  -- Added by Pattabhi on 19-Nov-2014 for WEB ADI testing
      --AND BPA.document_number=nvl(i_doc_number,BPA.document_number) -- Shankar 12/12/2012
      AND BPA.document_number=decode(i_doc_number,'ALL',BPA.document_number,i_doc_number) -- Shankar 12/12/2012
      Order By 1,2, 3;

  -- retrive PO header id to clean out interface tables
  Cursor C2 Is
    Select PH.po_header_id
      From 
           --Modified the below line for TMS ## 20141002-00054  by VijaySrinivasan  on 11/10/2014 
           XXWCPO_BPA_INTERFACE BPA,
           --Modified the below line for TMS ## 20141002-00054  by VijaySrinivasan  on 11/10/2014 
           PO_HEADERS_all PH
     Where BPA.document_number = PH.segment1
       And BPA.org_id = PH.org_id
	   AND ph.org_id=FND_PROFILE.VALUE('ORG_ID')  -- Added by Pattabhi on 19-Nov-2014 for WEB ADI testing
       --AND BPA.document_number=nvl(i_doc_number,BPA.document_number)  -- Shankar 12/12/2012
       AND BPA.document_number=decode(i_doc_number,'ALL',BPA.document_number,i_doc_number) -- Shankar 12/12/2012
       --Added by Shankar 06-Aug-2013 20130716-00932
       /* commented by Shankar for TMS Task 20140114-00092
       UNION
      Select ph.po_header_id
        from po_headers ph
       where exists (select 1 from po_headers ph1 where ph1.segment1=i_doc_number
                        and ph1.vendor_id=ph.vendor_id)
         and type_lookup_code = 'BLANKET'                 */
     Group By PH.po_header_id;

  Cursor C_Errors (p_int_header_id NUMBER) Is
    Select table_name,
           error_message,
           NULL line_num
      From PO_INTERFACE_ERRORS PIE
     Where interface_header_id = p_int_header_id
       And interface_type = 'PO_DOCS_OPEN_INTERFACE'
       And interface_line_id Is NULL
    UNION
    Select PIE.table_name,
           PIE.error_message,
           PLI.line_num
      From PO_INTERFACE_ERRORS PIE,
           PO_LINES_INTERFACE PLI
     Where PIE.interface_header_id = p_int_header_id
       And PIE.interface_type = 'PO_DOCS_OPEN_INTERFACE'
       And PIE.interface_line_id Is Not NULL
       And PIE.interface_line_id = PLI.interface_line_id
     Order By 1, 3;

     -- Satish U: 04-APR-2012 : Cursor to get the Distnct Lines for a given BPA
     Cursor Line_Cur ( P_DOCUMENT_NUMBER IN VARCHAR2,
                          P_ORG_ID          IN NUMBER ) IS
        Select bpa.ORG_ID, bpa.DOCUMENT_NUMBER, bpa.LINE_NUMBER, bpa.ITEM_NUMBER, bpa.DESCRIPTION,
           bpa.SUPPLIER_ITEM, bpa.unit_price, bpa.NOT_TO_EXCEED_PRICE, bpa.ALLOW_PRICE_OVERRIDE_FLAG
           , bpa.ORGANIZATION_CODE, mp.Organization_ID, bpa.cascade_flag,bpa.shipment_number
        From 
             --Modified the below line for TMS ## 20141002-00054  by VijaySrinivasan  on 11/10/2014 
             XXWCPO_BPA_INTERFACE BPA ,
             mtl_Parameters mp
        Where  bpa.DOCUMENT_NUMBER = P_DOCUMENT_NUMBER
        AND    bpa.Org_ID          = P_ORG_ID
        And    bpa.Organization_Code = mp.Organization_Code(+)
        And  bpa.rowid  = (
           Select Min(bpa2.ROWID )
           From   
                  --Modified the below line for TMS ## 20141002-00054  by VijaySrinivasan  on 11/10/2014 
                  XXWCPO_BPA_INTERFACE BPA2
           Where  BPA2.DOCUMENT_NUMBER = P_DOCUMENT_NUMBER
           AND    BPA2.Org_ID          = P_ORG_ID
           AND    BPA2.LINE_NUMBER     = BPA.LINE_NUMBER ) ;



     -- Satish U: 04-APR-2012 : Cursor to get the Distnct Shipment Lines for a given BPA
     -- Satish U: 14-JUN-2012  : Added Condiiton Shipment_Number is Not Null
     Cursor Ship_Cur ( P_DOCUMENT_NUMBER IN VARCHAR2,
                       P_ORG_ID          IN NUMBER ,
                       p_LINE_NUMBER     IN NUMBER) IS
        SELECT ORG_ID, DOCUMENT_NUMBER, LINE_NUMBER, SHIPMENT_NUMBER, ORGANIZATION_CODE,
           PRICE_OVERRIDE, QUANTITY,START_DATE, END_DATE, unit_price
        FROM 
             --Modified the below line for TMS ## 20141002-00054  by VijaySrinivasan  on 11/10/2014 
             XXWCPO_BPA_INTERFACE BPA
        WHERE  DOCUMENT_NUMBER = P_DOCUMENT_NUMBER
        AND    Org_ID          = P_ORG_ID
        AND    LINE_NUMBER     = P_LINE_NUMBER
        And    SHIPMENT_NUMBER is Not Null;
/*
  TYPE PO_LINE_REC IS RECORD(
      PO_HEADER_ID              Number
     ,PO_LINE_ID                NUMBER
     ,INTERFACE_HEADER_ID       Number
     ,INTERFACE_LINE_ID         Number
     ,LINE_NUMBER               Number
     ,NOT_TO_EXCEED_PRICE       Number
     ,ALLOW_PRICE_OVERRIDE_FLAG Varchar2(1)
    , VENDOR_PRODUCT_NUM        Varchar2(40)
    , ALLOW_PRICE_OVERRIDE_FLAG_OLD Varchar2(1));

  TYPE PO_LINE_TAB_TYPE IS TABLE OF PO_LINE_REC
     INDEX BY BINARY_INTEGER ;

  PO_LINE_TAB                          PO_LINE_TAB_TYPE ;
  PO_LINE_TAB_OUT                      PO_LINE_TAB_TYPE ;
  PO_LINE_TAB_NULL                     PO_LINE_TAB_TYPE ;
  */
  --Shankar TMS 20130904-00655
  TYPE PO_DOC_REC IS RECORD (DOC_NUMBER VARCHAR2(30));
  
  TYPE PO_DOC_TAB_TYPE IS TABLE OF PO_DOC_REC
  INDEX BY BINARY_INTEGER;
  
  PO_DOC_TAB   PO_DOC_TAB_TYPE;
  l_po_doc_cnt NUMBER :=1;
  l_item_key VARCHAR2(100);
  --Shankar TMS 20130904-00655 End  
  l_organization_id                    NUMBER;

  l_interface_header_id                NUMBER;
  l_interface_line_id                  NUMBER;
  l_pb_interface_line_id               NUMBER;
  l_line_location_id                   NUMBER;
  l_interface_line_location_id         Number ;

  l_batch_id                           NUMBER := FND_GLOBAL.CONC_REQUEST_ID;
  l_global_agreement_flag              VARCHAR2(1);

  l_conc_req_phase                     VARCHAR2(80);
  l_conc_req_status                    VARCHAR2(80);
  l_conc_req_dev_phase                 VARCHAR2(80);
  l_conc_req_dev_status                VARCHAR2(80);
  l_conc_req_message                   VARCHAR2(80);

  l_dummy                              VARCHAR2(1);
  l_cnt                                NUMBER := 0;

  l_request_id                         NUMBER;
  -- Satish U : 26-MAR-2012
  l_Action                             VARCHAR2(30) ;
  l_Inventory_Item_ID                  Number ;
  l_Line_Num                           Number ;
  l_PO_Line_ID                         Number ;
  l_UOM_Code                           Varchar2(25) ;
  l_MST_ORG_ID                         Number := FND_PROFILE.VALUE('XXWC_ITEM_MASTER_ORG');
  C_SHIPMENT_TYPE      CONSTANT        VARCHAR2(30) := 'PRICE BREAK';
  l_PO_LINE_ROW_COUNT                  Number := 0;
  l_ROWCOUNT                           Number := 0;
  L_OU_ORG_ID                          Number ;
  l_BPA_ROWS_FOUND                     Varchar2(1) := 'N';
  l_Primary_UOM                        Varchar2(30) ;
  l_Item_Description                   Varchar2(240);
  l_item_number                        VARCHAR2(50);
  l_unit_price                         NUMBER;
Begin
   -- begin processing
   FND_FILE.Put_Line(FND_FILE.Log, '100 : Processing Batch-ID '||l_batch_id);
   l_module := 'Load_BPA';

   errbuf := 'Success';
   retcode := 0;

   -- clean-up PDOI interface tables
   l_err_callpoint := 'Before clean-up of PDOI tables';

   For R2 In C2 Loop

      FND_FILE.Put_Line(FND_FILE.Log, '110 : Deleting existing records from PO_LINE_LOCATIONS_INTERFACE ');
      FND_FILE.Put_Line(FND_FILE.Log, '111 : Deleting existing records from PO_LINE_LOCATIONS_INTERFACE for PO_HEADER_ID ' || R2.po_header_id);

      Delete From PO_LINE_LOCATIONS_INTERFACE
      Where interface_header_id In (Select interface_header_id From PO_HEADERS_INTERFACE
                                      Where po_header_id = R2.po_header_id);

      FND_FILE.Put_Line(FND_FILE.Log, '112 : Number of Records Deleted from PO_LINE_LOCATIONS_INTERFACE for PO_HEADER_ID :' || SQL%ROWCOUNT);


      FND_FILE.Put_Line(FND_FILE.Log, '120 : Deleting existing records from PO_LINES_INTERFACE ');
      FND_FILE.Put_Line(FND_FILE.Log, '121 : Deleting existing records from PO_LINES_INTERFACE for PO_HEADER_ID ' || R2.po_header_id);

      Delete From PO_LINES_INTERFACE
      Where PO_HEADER_ID = R2.po_header_id ;

      FND_FILE.Put_Line(FND_FILE.Log, '122 : Number of Records Deleted from PO_LINE_INTERFACE for PO_HEADER_ID :' || SQL%ROWCOUNT);
      --Where interface_header_id In (Select interface_header_id From PO_HEADERS_INTERFACE
      --                               Where po_header_id = R2.po_header_id);

      Delete From PO_INTERFACE_ERRORS PIE
      Where Exists (
      Select 1
      From PO_HEADERS_INTERFACE   PHI
      Where PHI.po_header_id = R2.po_header_id
      And PIE.INTERFACE_HEADER_ID = PHI.INTERFACE_HEADER_ID ) ;


      FND_FILE.Put_Line(FND_FILE.Log, '130 : Deleting existing records from PO_HEADRERS_INTERFACE ');
      Delete From PO_HEADERS_INTERFACE
      Where po_header_id = R2.po_header_id;

   End Loop;
   COMMIT;

   -- begin processing
   FND_FILE.Put_Line(FND_FILE.Log, '131 : Processing Batch-ID '||l_batch_id);

   l_err_callpoint := 'Before Loop';


   For R1 In C1 Loop
      begin
      select item_number,unit_price
        into l_item_number, l_unit_price
        from 
             --Modified the below line for TMS ## 20141002-00054  by VijaySrinivasan  on 11/10/2014 
             XXWCPO_BPA_INTERFACE
       where document_number=r1.document_number
	   AND org_id=FND_PROFILE.VALUE('ORG_ID')  -- Added by Pattabhi on 19-Nov-2014 for WEB ADI testing
         and rownum=1;
      exception
       when others then null;
      end;  
      --create_bpa(r1.document_number, r1.org_id, l_item_number, l_unit_price);
      l_global_agreement_flag := 'Y';
      l_BPA_ROWS_FOUND  := 'Y';

      l_cnt := l_cnt + 1;
      FND_FILE.Put_Line(FND_FILE.Log, '140 : Global Agreement Flag ');
      -- retrieve global agreement flag for use in Import Price Lists concurrent program
      l_err_callpoint := 'Before Global Agreement Flag retrieval';
      Begin
         Select Nvl(global_agreement_flag,'N')
         Into l_global_agreement_flag
         From 
              --Modified the below line for TMS ## 20141002-00054  by VijaySrinivasan  on 11/10/2014 
              po_headers_all
         Where po_header_Id = R1.po_header_id
		 AND org_id=FND_PROFILE.VALUE('ORG_ID');  -- Added by Pattabhi on 19-Nov-2014 for WEB ADI testing;
      End;

      -- check to see if BPA header exists in interface table, otherwise create record in interface table
      l_err_callpoint := 'Before creation of PDOI header record';
      FND_FILE.Put_Line(FND_FILE.Log, '150 : Generate Interface header ID ');
      Select PO_HEADERS_INTERFACE_S.nextval
      Into l_interface_header_id
      From DUAL;

      FND_FILE.Put_Line(FND_FILE.Log, '151 : Creating interface header record '||l_interface_header_id||' for '||R1.document_number);
      FND_FILE.Put_Line(FND_FILE.Log, '152 : Insert into PO_HEADERS_INTERFACE ');
      l_OU_ORG_ID := R1.org_id ;
      Insert Into PO_HEADERS_INTERFACE
        (interface_header_id
        ,action
        ,document_type_code
        ,document_num
        ,po_header_id
        ,org_id
        ,process_code
        ,interface_source_code
        ,batch_id
        ,last_update_date
        ,last_updated_by
        )
       Values (l_interface_header_id            -- interface_header_id
           ,'UPDATE'                         -- action
           ,'BLANKET'                        -- document_type_code
           ,R1.document_number               -- document_number
           ,R1.po_header_id                  -- po_header_id
           ,R1.org_id                        -- org_id
           ,'PENDING'                        -- process_code
           ,'WebADI'                         -- interface_source_code
           ,l_batch_id                       -- batch_id
           ,SYSDATE                          -- last_update_date
           ,FND_GLOBAL.User_Id               -- last_updated_by
           );

       FND_FILE.Put_Line(FND_FILE.Log, '154 :  After Inserting into PO_HEADERS_INTERFACE ');
       -- check to see if BPA line exists in interface, otherwise create record in interface table
       l_err_callpoint := 'Before creation of PDOI line record';

       --Satish U: 04-APR-2012 : Open Line Cursor
       FOR Line_Rec IN Line_Cur ( R1.DOCUMENT_NUMBER ,
                               R1.ORG_ID ) LOOP
         FND_FILE.Put_Line(FND_FILE.Log, '160 :  Looping Through the Line Cursor ');
         l_Inventory_item_ID     := NULL;
         FND_FILE.Put_Line(FND_FILE.Log, '161 : Get PO line ID ');
         --,Get_UOM(PH.po_header_id, BPA.Line_Number) unit_meas_lookup_code
         l_PO_Line_ID := Get_PO_LINE_ID(R1.po_header_id, Line_Rec.Line_Number ) ;
         FND_FILE.Put_Line(FND_FILE.Log, '162 : Get UOM Code ');
         l_UOM_COde   := Get_UOM(R1.po_header_id, Line_Rec.Line_Number) ;
          FND_FILE.Put_Line(FND_FILE.Log, '162.01 : UOM Code Is ' || l_UOM_COde);
         -- create price break line
         Select PO_LINES_INTERFACE_S.nextval
         Into l_interface_line_id
         From DUAL;

         FND_FILE.Put_Line(FND_FILE.Log, '163: Creating interface line price-break record '||l_interface_line_id||' for '||Line_Rec.line_number);
         l_err_callpoint := ' Before Getting Inventory Item ID ';
         -- Get Item_ID
         -- Satish U: Added  NVL Function Around Organization ID as Organization ID Is not required.

         Begin
            Select msi.Inventory_Item_ID , mum.UNIT_OF_MEASURE, msi.description
            Into l_Inventory_item_ID , l_Primary_UOM, l_Item_Description
            From Mtl_System_Items_B Msi,
                 MTL_UNITS_OF_MEASURE_TL mum
            Where msi.Segment1 = Line_Rec.Item_Number
            And  msi.Primary_Uom_Code = mum.Uom_Code
            And msi.Organization_ID = NVL(Line_Rec.organization_id, l_MST_ORG_ID )  ;
            FND_FILE.Put_Line(FND_FILE.Log, '163.01: Inventory Item Id is  '||l_Inventory_item_ID);

            If l_UOM_Code is NULL Then
               l_UOM_COde := l_Primary_UOM ;
            End IF;
            FND_FILE.Put_Line(FND_FILE.Log, '163.02: UOM Code '|| l_UOM_COde);
            FND_FILE.Put_Line(FND_FILE.Log, '163.03: Item Description '|| Line_Rec.Description);

         Exception
            When Others Then
               l_Inventory_Item_ID := NULL;
               l_err_msg  := '164 : Item Number ' || Line_Rec.Item_Number || 'not found in Organization :' || Line_Rec.organization_code ;
               Raise XXWC_ERROR  ;
         End;

         -- Satish U: 26-MAR-2012
         --l_Action := NULL;
         -- If l_line_number IS NULL Then
         l_err_callpoint := ' Before Getting Line Number ';
         If l_PO_Line_ID IS NULL   Then
            --SatishU: 25-JUN-2012  Changed Action Code to 'ADD'
            --l_Action := 'ORIGINAL';
            l_Action := 'ADD';
            l_Line_Num := Line_Rec.Line_Number ;

         Elsif l_PO_Line_ID IS NOT NULL  AND Line_Rec.Line_Number is NOT NULL Then
            l_Line_Num := Line_Rec.Line_Number ;
            l_Action := 'UPDATE';
         End If;

         FND_FILE.Put_Line(FND_FILE.Log, '169 : Action Code on the Line ' || l_Action);
        l_err_callpoint := ' Before Inserting into PO_LINES_INTERFACE ';

        FND_FILE.Put_Line(FND_FILE.Log, '170 : Insert into PO_LINES_INTERFACE ');
        -- Satish U: 15-JUN-2012 : Commented Shipment TYpe from Line Record info
        -- Added Item_Description and Supplier_Item Too

        Insert Into PO_LINES_INTERFACE
               (interface_header_id
               ,interface_line_id
               ,po_header_id
               ,line_num
               ,po_line_id
              -- ,line_location_id
              -- ,ship_to_organization_id
              -- ,shipment_type
              -- ,quantity
               ,unit_price
               ,unit_of_measure
               --,effective_date
               --,expiration_date
               ,price_break_lookup_code
               ,action
               ,process_code
               ,Item_ID
               ,Item
               --,Item_Description
               ,ALLOW_PRICE_OVERRIDE_FLAG
               --,NOT_TO_EXCEED_PRICE
              , VENDOR_PRODUCT_NUM
              , line_loc_populated_flag
               )
        Values (l_interface_header_id                -- interface_header_id
               ,l_interface_line_id                  -- interface_line_id
               ,R1.po_header_id                      -- po_header_id
               ,NVL(Line_Rec.line_number, l_Line_Num)      -- line_num
               ,l_po_line_id                        -- po_line_id  Changed it from R1.PO_LINE_ID
              -- ,l_line_location_id                   -- line_location_id
              -- ,R1.organization_id                   -- ship_to_organization_id
              -- ,C_SHIPMENT_TYPE                        -- shipment_type
               --,R1.quantity                          -- quantity
               ,Line_Rec.Unit_Price                    -- unit_price
               ,l_UOM_COde                           -- unit_of_measure -- R1.unit_meas_lookup_code
              -- ,R1.start_date                        -- effective_date
              -- ,R1.end_date                          -- expiration_date
               ,'NON CUMULATIVE'                     -- price_break_lookup_code
               ,nvl(l_Action,'UPDATE')                             -- 'UPDATE'    -- action
               ,'PENDING'                            -- process_code
               , l_Inventory_Item_ID
               ,Line_Rec.ITEM_NUMBER                     -- Item Num
             --  , NVL(Line_Rec.Description , l_Item_Description)
               , Line_Rec.ALLOW_PRICE_OVERRIDE_FLAG
               --, Line_Rec.NOT_TO_EXCEED_PRICE
               , DECODE(l_Action, 'UPDATE', NULL, Line_Rec.Supplier_Item)
               , decode(l_po_Line_id, null,decode(line_rec.shipment_number,NULL,NULL,'Y'),null)
               );
         FND_FILE.Put_Line(FND_FILE.Log, '171 : After Insert into PO_LINES_INTERFACE Main ');
         l_err_callpoint := ' Before Inserting Into Collection Main';

         IF Line_Rec.ALLOW_PRICE_OVERRIDE_FLAG IS NOT NULL Or Line_Rec.NOT_TO_EXCEED_PRICE IS NOT NULL
           Or Line_Rec.Supplier_Item Is Not NULL  Then
            -- Increment the PO_LINE ROW COUNT
            l_PO_LINE_ROW_COUNT  := l_PO_LINE_ROW_COUNT + 1;
            -- Insert a Record inTo PLSQL Table
            PO_LINE_TAB(l_PO_LINE_ROW_COUNT).PO_HEADER_ID                := R1.po_header_id ;
            PO_LINE_TAB(l_PO_LINE_ROW_COUNT).INTERFACE_HEADER_ID         := l_interface_header_id;
            PO_LINE_TAB(l_PO_LINE_ROW_COUNT).INTERFACE_LINE_ID           := l_interface_Line_id;
            PO_LINE_TAB(l_PO_LINE_ROW_COUNT).LINE_NUMBER                 := Line_Rec.line_number ;
            --PO_LINE_TAB(l_PO_LINE_ROW_COUNT).shipment_number                 := Line_Rec.shipment_number ;
            --IF line_rec.shipment_number is null then
            PO_LINE_TAB(l_PO_LINE_ROW_COUNT).NOT_TO_EXCEED_PRICE         := Line_Rec.NOT_TO_EXCEED_PRICE ;
            PO_LINE_TAB(l_PO_LINE_ROW_COUNT).ALLOW_PRICE_OVERRIDE_FLAG   := Line_Rec.ALLOW_PRICE_OVERRIDE_FLAG ;
            --END IF;
            PO_LINE_TAB(l_PO_LINE_ROW_COUNT).VENDOR_PRODUCT_NUM          := Line_Rec.Supplier_Item ;
            PO_LINE_TAB(l_PO_LINE_ROW_COUNT).PO_LINE_ID                  := l_po_line_id ;
            FND_FILE.Put_Line(FND_FILE.Log, '172 : After Insert into PO_LINES_INTERFACE collection Main');
             IF l_po_line_id  IS NOT NULL THen
               Begin
                  Select Allow_Price_Override_Flag
                  Into  PO_LINE_TAB(l_PO_LINE_ROW_COUNT).ALLOW_PRICE_OVERRIDE_FLAG_OLD
                  From 
                       --Modified the below line for TMS ## 20141002-00054  by VijaySrinivasan  on 11/10/2014 
                       PO_LINES_all
                  Where PO_LINE_ID = l_po_line_id
                  AND org_id=FND_PROFILE.VALUE('ORG_ID');  -- Added by Pattabhi on 19-Nov-2014 for WEB ADI testing				  ;

                  -- Check if Old Value and New Values are Different : 25-JUN-2012
                  If NVL(PO_LINE_TAB(l_PO_LINE_ROW_COUNT).ALLOW_PRICE_OVERRIDE_FLAG_OLD, 'NULL') <>
                     NVL(PO_LINE_TAB(l_PO_LINE_ROW_COUNT).ALLOW_PRICE_OVERRIDE_FLAG, 'NULL') Then

                       Update 
                              --Modified the below line for TMS ## 20141002-00054  by VijaySrinivasan  on 11/10/2014 
                              PO_LINES_all
                          Set ALLOW_PRICE_OVERRIDE_FLAG = PO_LINE_TAB(l_PO_LINE_ROW_COUNT).ALLOW_PRICE_OVERRIDE_FLAG
                       Where PO_LINE_ID = l_PO_LINE_ID ;

                  End If;
               Exception
                  When Others Then
                     Null;
               End ;
            ENd If;
         End If;
         -- 04-APR-2012 : For each of PO Line process all the Shipments lines
        --Satish U: 04-APR-2012 : Open Line Cursor
         For Ship_Rec IN Ship_Cur ( Line_Rec.DOCUMENT_NUMBER ,
                               Line_Rec.ORG_ID ,
                               Line_Rec.Line_Number) Loop
              FND_FILE.Put_Line(FND_FILE.Log, '180 : Looping Through Shipment Record Main ');
              --initialize Variables
              l_line_location_id      := NULL;

              -- create price break line
              Select PO_LINE_LOCATIONS_INTERFACE_S.nextval
              Into l_interface_line_location_id
              From DUAL;


              If Ship_Rec.shipment_number Is Not NULL Then
                  Begin
                     -- Satish U: Changed R1.PO_Line_ID to l_PO_Line_Id
                     FND_FILE.Put_Line(FND_FILE.Log, '190 : Get Line Location ID for line Id ' || l_po_line_id);
                     Select line_location_id
                     Into l_line_location_id
                     From 
                          --Modified the below line for TMS ## 20141002-00054  by VijaySrinivasan  on 11/10/2014 
                          po_line_locations_all
                     Where po_line_id = l_po_line_id
					 AND org_id=FND_PROFILE.VALUE('ORG_ID')  -- Added by Pattabhi on 19-Nov-2014 for WEB ADI testing
                     And shipment_num = Ship_Rec.shipment_number;
                     l_Action := 'UPDATE';
                     --Ship_Rec.shipment_number := NULL;
                  Exception
                     When NO_DATA_FOUND Then
                        l_line_location_id := NULL;
                        --Satishu: 25-JUN-2012  Changed Action Code from Original to ADD
                        --l_Action := 'ORIGINAL';
                        l_Action := 'ADD';
                        /*
                      IF l_po_line_id is not null then  
                      update po_lines_interface
                         set line_loc_populated_flag='Y'
                       where po_line_id=l_po_line_id;
                      END IF;   */
                  End;
              Else
                  l_line_location_id := NULL;
              End If;
              FND_FILE.Put_Line(FND_FILE.Log, '190.01 : Action Code on the shipment Line ' || l_Action);

              FND_FILE.Put_Line(FND_FILE.Log, '191 : Inserting Record into Line Location Interface Table ');
              FND_FILE.Put_Line(FND_FILE.Log, '192 : Interface Line ID' || l_interface_line_id );
              FND_FILE.Put_Line(FND_FILE.Log, '193 : Line Location ID' || l_line_location_id );
              FND_FILE.Put_Line(FND_FILE.Log, '194 : Organization Code' || Ship_Rec.ORGANIZATION_CODE );
              FND_FILE.Put_Line(FND_FILE.Log, '195 : Quantity' || Ship_Rec.QUANTITY );
             IF l_line_location_id is null then 
               FND_FILE.Put_Line(FND_FILE.Log, '195.5 : Insert into PLL');
             Insert into PO.PO_LINE_LOCATIONS_INTERFACE
               (INTERFACE_LINE_LOCATION_ID,
                INTERFACE_HEADER_ID,
                INTERFACE_LINE_ID, 
                LINE_LOCATION_ID,             
                SHIPMENT_TYPE,
                SHIPMENT_NUM,
                SHIP_TO_ORGANIZATION_code,               
                PRICE_OVERRIDE,
                quantity,
                start_date,
                end_date,
                CREATION_DATE)
             Values
               (l_interface_line_location_id,---    INTERFACE_LINE_LOCATION_ID,
                l_interface_header_id,    ---        INTERFACE_HEADER_ID,
                l_interface_line_id,    ---        INTERFACE_LINE_ID,
                l_line_location_id,             
                'PRICE BREAK',    ---        SHIPMENT_TYPE,
                ship_rec.shipment_number,    ---        SHIPMENT_NUM,
                Ship_Rec.organization_Code,
                ship_Rec.PRICE_OVERRIDE,
                ship_Rec.quantity,
                ship_rec.start_date,
                ship_rec.end_date,
                SYSDATE); 
 
                update po_lines_interface
                   set line_loc_populated_flag='Y'
                 where interface_line_id=l_interface_line_id
                   and line_location_id is null;

              ELSE  
             -- create price break line
              Select PO_LINES_INTERFACE_S.nextval
              Into l_pb_interface_line_id
              From DUAL;

              Insert Into PO_LINES_INTERFACE
                   (interface_header_id
                   ,interface_line_id
                   ,po_header_id
                   ,line_num
                   ,po_line_id
                   ,line_location_id
                   ,ship_to_organization_Code
                   ,shipment_type
                   ,quantity
                   ,unit_price
                   ,unit_of_measure
                   ,effective_date
                   ,expiration_date
                  -- ,price_break_flag
                   --,price_break_lookup_code
                   ,action
                   --,process_code
                  -- ,Item_ID
                   ,Item
                   --,ALLOW_PRICE_OVERRIDE_FLAG
                  ,NOT_TO_EXCEED_PRICE
                  -- , VENDOR_PRODUCT_NUM
                   ,Shipment_Num
                   ,creation_date
                   )
              Values (l_interface_header_id                -- interface_header_id
                   ,l_pb_interface_line_id                  -- interface_line_id
                   ,R1.po_header_id                      -- po_header_id
                   ,NVL(Line_Rec.line_number, l_Line_Num)      -- line_num
                   ,l_po_line_id                        -- po_line_id  Changed it from R1.PO_LINE_ID
                   ,l_line_location_id                   -- line_location_id
                   ,Ship_Rec.organization_Code                 -- ship_to_organization_id
                   ,C_SHIPMENT_TYPE                        -- shipment_type
                   , Ship_Rec.quantity                          -- quantity
                   , ship_rec.unit_price--,Ship_Rec.PRICE_OVERRIDE               -- Line_Rec.Unit_Price                    -- unit_price
                   ,l_UOM_COde                           -- unit_of_measure -- R1.unit_meas_lookup_code
                   ,Ship_Rec.start_date                        -- effective_date
                   ,Ship_Rec.end_date                          -- expiration_date
                  -- ,'Y'
                   --,'NON CUMULATIVE'                     -- price_break_lookup_code
                   ,l_Action                             -- 'UPDATE'    -- action
                  -- ,'PENDING'                            -- process_code
                  -- , l_Inventory_Item_ID
                   , Line_Rec.ITEM_NUMBER
                  -- , Line_Rec.ALLOW_PRICE_OVERRIDE_FLAG
                   , Line_Rec.NOT_TO_EXCEED_PRICE
                  -- ,Line_Rec.Supplier_Item
                   ,decode(l_action,'ADD',Ship_Rec.shipment_number,null)
                   ,sysdate
               );
               
            l_PO_LINE_ROW_COUNT  := l_PO_LINE_ROW_COUNT + 1;
            FND_FILE.Put_Line(FND_FILE.Log, '196.1 : insert to collection PLL:' || l_PO_LINE_ROW_COUNT||':' ||line_rec.document_number||':'||l_pb_interface_Line_id||':'||l_line_location_id);
            -- Insert a Record inTo PLSQL Table
            PO_LINE_TAB(l_PO_LINE_ROW_COUNT).PO_HEADER_ID                := R1.po_header_id ;
            PO_LINE_TAB(l_PO_LINE_ROW_COUNT).INTERFACE_HEADER_ID         := l_interface_header_id;
            PO_LINE_TAB(l_PO_LINE_ROW_COUNT).INTERFACE_LINE_ID           := l_pb_interface_line_id;
            PO_LINE_TAB(l_PO_LINE_ROW_COUNT).LINE_NUMBER                 := Line_Rec.line_number ;
            PO_LINE_TAB(l_PO_LINE_ROW_COUNT).SHIPMENT_NUMBER             := ship_Rec.shipment_number ;
            PO_LINE_TAB(l_PO_LINE_ROW_COUNT).PRICE_OVERRIDE              := ship_Rec.PRICE_OVERRIDE ;
            PO_LINE_TAB(l_PO_LINE_ROW_COUNT).NOT_TO_EXCEED_PRICE         := Line_Rec.NOT_TO_EXCEED_PRICE ;
            PO_LINE_TAB(l_PO_LINE_ROW_COUNT).ALLOW_PRICE_OVERRIDE_FLAG   := Line_Rec.ALLOW_PRICE_OVERRIDE_FLAG ;
            PO_LINE_TAB(l_PO_LINE_ROW_COUNT).VENDOR_PRODUCT_NUM          := Line_Rec.Supplier_Item ;
            PO_LINE_TAB(l_PO_LINE_ROW_COUNT).PO_LINE_ID                  := l_po_line_id ;    
            PO_LINE_TAB(l_PO_LINE_ROW_COUNT).PO_LINE_LOCATION_ID         := l_line_location_id ;
            PO_LINE_TAB(l_PO_LINE_ROW_COUNT).start_date                  := Ship_Rec.start_date ;    
            PO_LINE_TAB(l_PO_LINE_ROW_COUNT).end_date                    := Ship_Rec.end_date ;
            
            END IF;   
         End Loop ; -- PO Line Shipment Cursor

         --Added by Shankar 06-Aug-2013 20130716-00932
         /*
         IF nvl(line_rec.cascade_flag,'N')='Y' then
          PO_LINE_TAB_OUT := PO_LINE_TAB_NULL ;
          FND_FILE.Put_Line(FND_FILE.Log, '196 : Before Cascade');
          cascade_line(line_rec.document_number,line_rec.org_id,line_rec.item_number,line_rec.organization_id,l_batch_id,po_line_tab_out);
          --commit;
          FND_FILE.Put_Line(FND_FILE.Log, '197 : After Cascade:'||po_line_tab_out.count);
          IF po_line_tab_out.count > 0 then
          for i in 1..po_line_tab_out.count 
            loop
              BEGIN
                -- Insert a Record inTo PLSQL Table
                IF po_line_tab_out(i).po_header_id is not null then
                 l_PO_LINE_ROW_COUNT  := l_PO_LINE_ROW_COUNT + 1;
                 FND_FILE.Put_Line(FND_FILE.Log, '197 : After Cascade reading record:'||i||' into:'||l_PO_LINE_ROW_COUNT);
                 FND_FILE.Put_Line(FND_FILE.Log,'199:cascade collect info:'||po_line_tab_out(i).po_header_id||':'||po_line_tab_out(i).interface_header_id ||':'|| po_line_tab_out(i).interface_Line_id
                  ||':'|| po_line_tab_out(i).line_number||':'||po_line_tab_out(i).SHIPMENT_NUMBER ||':'||po_line_tab_out(i).NOT_TO_EXCEED_PRICE ||':'||po_line_tab_out(i).PRICE_OVERRIDE
                 ||':'||po_line_tab_out(i).ALLOW_PRICE_OVERRIDE_FLAG||':'||po_line_tab_out(i).VENDOR_PRODUCT_NUM||':'||po_line_tab_out(i).po_line_id
                 ||':'||po_line_tab_out(i).ALLOW_PRICE_OVERRIDE_FLAG_OLD||':'||po_line_tab_out(i).po_line_location_id);
                PO_LINE_TAB(l_PO_LINE_ROW_COUNT).PO_HEADER_ID                := po_line_tab_out(i).po_header_id ;
                PO_LINE_TAB(l_PO_LINE_ROW_COUNT).INTERFACE_HEADER_ID         := po_line_tab_out(i).interface_header_id;
                PO_LINE_TAB(l_PO_LINE_ROW_COUNT).INTERFACE_LINE_ID           := po_line_tab_out(i).interface_Line_id;
                PO_LINE_TAB(l_PO_LINE_ROW_COUNT).PO_LINE_LOCATION_ID         := po_line_tab_out(i).po_line_location_id ;
                PO_LINE_TAB(l_PO_LINE_ROW_COUNT).LINE_NUMBER                 := po_line_tab_out(i).line_number ;
                PO_LINE_TAB(l_PO_LINE_ROW_COUNT).SHIPMENT_NUMBER             := po_line_tab_out(i).SHIPMENT_NUMBER ;
                PO_LINE_TAB(l_PO_LINE_ROW_COUNT).NOT_TO_EXCEED_PRICE         := po_line_tab_out(i).NOT_TO_EXCEED_PRICE ;
                PO_LINE_TAB(l_PO_LINE_ROW_COUNT).PRICE_OVERRIDE              := po_line_tab_out(i).PRICE_OVERRIDE ;
                PO_LINE_TAB(l_PO_LINE_ROW_COUNT).ALLOW_PRICE_OVERRIDE_FLAG   := po_line_tab_out(i).ALLOW_PRICE_OVERRIDE_FLAG ;
                PO_LINE_TAB(l_PO_LINE_ROW_COUNT).VENDOR_PRODUCT_NUM          := po_line_tab_out(i).VENDOR_PRODUCT_NUM ;
                PO_LINE_TAB(l_PO_LINE_ROW_COUNT).PO_LINE_ID                  := po_line_tab_out(i).po_line_id ;
                PO_LINE_TAB(l_PO_LINE_ROW_COUNT).ALLOW_PRICE_OVERRIDE_FLAG_OLD:= po_line_tab_out(i).ALLOW_PRICE_OVERRIDE_FLAG_OLD;
                PO_LINE_TAB(l_PO_LINE_ROW_COUNT).START_DATE                  := po_line_tab_out(i).START_DATE;
                PO_LINE_TAB(l_PO_LINE_ROW_COUNT).END_DATE                    := po_line_tab_out(i).END_DATE;
                END IF;
              EXCEPTION
               when others then 
               FND_FILE.Put_Line(FND_FILE.Log, '198 : After Cascade exception:'||i||':'||substr(sqlerrm,1,200));
               --l_PO_LINE_ROW_COUNT  := l_PO_LINE_ROW_COUNT - 1;
              END;  
            end loop;
           END IF;
           
          -- add to the po_line_rec
         END IF; 
         */
        -- End --Added by Shankar 06-Aug-2013 20130716-00932
      End Loop; -- PO Line Cursor
       --Shankar TMS 20130904-00655
      PO_DOC_TAB(l_po_doc_cnt).doc_number := r1.document_number;
      l_po_doc_cnt := l_po_doc_cnt + 1;  
       --Shankar TMS 20130904-00655 End
      -- 04-APR-2012 : Moved the Transaction down
      -- Delete the Records from  Staging Table Now
      FND_FILE.Put_Line(FND_FILE.Log, 'Check complete');
      l_err_callpoint := 'Before clean-up of XXWCPO_BPA_INTERFACE';

      Delete From 
                  --Modified the below line for TMS ## 20141002-00054  by VijaySrinivasan  on 11/10/2014 
                  XXWCPO_BPA_INTERFACE
      Where  Document_Number = R1.Document_Number ;
      COMMIT;
      FND_FILE.Put_Line(FND_FILE.Log, l_cnt||' 200 : records created in PDOI interface tables');

   ENd Loop ;


   -- setup environment to Purchasing for submission of Import Pricing Catalog
   MO_GLOBAL.Init('PO');
   Begin
      Select Distinct ORG_ID
      Into L_OU_ORG_ID
      From PO_HEADERS_INTERFACE
      Where Rownum = 1;
   Exception
      When Others Then
          l_OU_ORG_ID := FND_PROFILE.VALUE('DEFAULT_ORG_ID');
   End;
   -- submit PDOI to import BPAs
   l_err_callpoint := 'Before execution of FND_REQUEST.Submit_Request for POXPDOI';
   l_request_id := FND_REQUEST.Submit_Request(application => 'PO'
                                                  ,program     => 'POXPDOI'
                                                  ,description => NULL
                                                  ,start_time  => NULL
                                                  ,sub_request => FALSE
                                                  ,argument1   => NULL           -- default buyer
                                                  ,argument2   => 'Blanket'      -- document type
                                                  ,argument3   => NULL           -- document sub-type
                                                  ,argument4   => 'N'            -- create or update items
                                                  ,argument5   => 'N'            -- create sourcing rules
                                                --  ,argument6   => 'APPROVED'     -- approval status -- Shankar 12/12/2012
                                                 -- ,argument6   => 'INITIATE APPROVAL'     -- approval status
                                                  ,argument6   => 'INCOMPLETE'     -- approval status --Shankar TMS 20130904-00655
                                                  ,argument7   => NULL           -- release generation method
                                                  ,argument8   => To_Char(l_batch_id, 'FM9999999999')     -- batch id
                                                  ,argument9   => l_OU_ORG_ID    -- operating unit
                                                  ,argument10  => l_global_agreement_flag  -- global agreement
                                                  ,argument11  => 'Y'            -- enable sourcing level
                                                  ,argument12  => NULL
                                                  ,argument13  => NULL
                                                  ,argument14  => NULL
                                                  );
    COMMIT;

    If Nvl(l_request_id,0) = 0 Then
       FND_FILE.Put_Line(FND_FILE.Log, '210 Unable to submit Import Pricing Catalog concurrent program');
       FND_FILE.Put_Line(FND_FILE.Log, '220 Manually submit Import Pricing Catalog concurrent program for batch-id'||l_batch_id);
       errbuf := 'Warning';
       retcode := 1;
    Else
       FND_FILE.Put_Line(FND_FILE.Log, '230 : Submitted Import Pricing Catalog concurrent program, request-id = '||l_request_id);
    End If;

    -- wait on program completion and report any error messages
    l_err_callpoint := 'Before FND_CONCURRENT.Wait_For_Request for POXPDOI';
    If FND_CONCURRENT.Wait_For_Request(request_id => l_request_id
                                        ,interval   => 30
                                        ,max_wait   => 0
                                        ,phase      => l_conc_req_phase
                                        ,status     => l_conc_req_status
                                        ,dev_phase  => l_conc_req_dev_phase
                                        ,dev_status => l_conc_req_dev_status
                                        ,message    => l_conc_req_message) Then

       FND_FILE.Put_Line(FND_FILE.Log, '240 : Checking for any error messages');

       l_err_callpoint := 'Before returning C_Errors cursor';
       For R_Error In C_Errors(l_interface_header_id) Loop
          FND_FILE.Put_Line(FND_FILE.Log, 'Table: '||R_Error.table_name||' Line: '||R_Error.line_num||' Error: '||R_Error.error_message);
          errbuf := 'Warning';
          retcode := 1;
       End Loop;

    End If;

    -- Satish U 05-APR-2012 :
    -- Check if There are PO_LINES needs to be Updated with
    FND_FILE.Put_Line(FND_FILE.Log, '250 :Direct Update PO Lines/Locations All Table:'||PO_LINE_TAB.count);
    l_err_callpoint := 'Before Updating P_COUNT';
    
    For i in 1..PO_LINE_TAB.count
      loop    
      FND_FILE.Put_Line(FND_FILE.Log, '251 : Update PO Lines All Table:'||i||':'||PO_LINE_TAB(i).PO_HEADER_ID||':'||PO_LINE_TAB(i).LINE_NUMBER||':'||
      PO_LINE_TAB(i).shipment_number||':'||PO_LINE_TAB(i).interface_line_id||':'||PO_LINE_TAB(i).PRICE_OVERRIDE||':'||PO_LINE_TAB(i).po_line_location_id);
      BEGIN
        IF PO_LINE_TAB(i).po_line_location_id is null then
            l_err_callpoint := 'Before Updating PL';
        Update 
               --Modified the below line for TMS ## 20141002-00054  by VijaySrinivasan  on 11/10/2014 
               PO_LINES
           Set NOT_TO_EXCEED_PRICE        = PO_LINE_TAB(i).NOT_TO_EXCEED_PRICE,
               --ALLOW_PRICE_OVERRIDE_FLAG  = PO_LINE_TAB(i).ALLOW_PRICE_OVERRIDE_FLAG,
               VENDOR_PRODUCT_NUM         = PO_LINE_TAB(i).VENDOR_PRODUCT_NUM
        Where PO_HEADER_ID = PO_LINE_TAB(i).PO_HEADER_ID
        AND   LINE_NUM  = nvl(PO_LINE_TAB(i).LINE_NUMBER,line_num)
        AND   item_id  = (select inventory_item_id from mtl_system_items a, po_lines_interface b
                           where a.organization_id=l_mst_org_id
                             and a.inventory_item_id=b.item_id
                             and b.interface_line_id=po_line_tab(i).interface_line_id)
        And Exists
            ( Select 1
              From PO_LINES_INTERFACE
              Where Process_Code in ('NOTIFIED','ACCEPTED')
              And INTERFACE_LINE_ID =  PO_LINE_TAB(i).INTERFACE_LINE_ID);
              FND_FILE.Put_Line(FND_FILE.Log, '251 : After Update PO Lines All Table'||':'||PO_LINE_TAB(i).PO_HEADER_ID||':'||PO_LINE_TAB(i).LINE_NUMBER||':'||
               PO_LINE_TAB(i).shipment_number||':'||po_line_tab(i).interface_line_id||':'||PO_LINE_TAB(i).PRICE_OVERRIDE);
        ELSE
         FND_FILE.Put_Line(FND_FILE.Log, '251 : Update PO Line Locations All Table'||PO_LINE_TAB(i).PO_HEADER_ID||':'||PO_LINE_TAB(i).shipment_number||':'||po_line_tab(i).po_line_location_id);
         l_err_callpoint := 'Before Updating POL';
        BEGIN 
        Update 
               --Modified the below line for TMS ## 20141002-00054  by VijaySrinivasan  on 11/10/2014 
               PO_LINE_locationS
           Set --NOT_TO_EXCEED_PRICE  = PO_LINE_TAB(i).NOT_TO_EXCEED_PRICE
               PRICE_OVERRIDE=PO_LINE_TAB(i).PRICE_OVERRIDE
              ,START_DATE=PO_LINE_TAB(i).START_DATE
              ,END_DATE=PO_LINE_TAB(i).END_DATE  
        Where  line_location_id= PO_LINE_TAB(i).po_line_location_id
        And Exists
            ( Select 1
              From PO_LINES_INTERFACE
              Where Process_Code in ('NOTIFIED','ACCEPTED')
              And INTERFACE_LINE_ID =  PO_LINE_TAB(i).INTERFACE_LINE_ID); 
              
        IF sql%rowcount > 0 then             
        FND_FILE.Put_Line(FND_FILE.Log, '252 : Update PO Lines Locations All Table:'||sql%rowcount);
        END IF; 
        EXCEPTION
         when others then 
           FND_FILE.Put_Line(FND_FILE.Log, '254 : Updating po_line_locations:'||substr(sqlerrm,1,200));
        END;      
        END IF;

       -- Check if PO Lines are rejected and
       IF po_line_tab(i).po_line_id  IS NOT NULL THen
          Begin
             -- Check if Old Value and New Values are Different : 25-JUN-2012
             If NVL(PO_LINE_TAB(i).ALLOW_PRICE_OVERRIDE_FLAG_OLD, 'NULL') <>
                     NVL(PO_LINE_TAB(i).ALLOW_PRICE_OVERRIDE_FLAG, 'NULL') Then
               l_err_callpoint := 'Before Updating PO_LINES end';      
              FND_FILE.Put_Line(FND_FILE.Log, '255 : Updating price override:'||po_line_tab(i).po_line_id);       
               BEGIN
                Update 
                       --Modified the below line for TMS ## 20141002-00054  by VijaySrinivasan  on 11/10/2014 
                       PO_LINES
                   Set ALLOW_PRICE_OVERRIDE_FLAG = PO_LINE_TAB(i).ALLOW_PRICE_OVERRIDE_FLAG_OLD
                Where PO_LINE_ID = po_line_tab(i).PO_LINE_ID
                And Exists
                    ( Select 1
                      From PO_LINES_INTERFACE
                      Where Process_Code Not in ('NOTIFIED','ACCEPTED')
                      And INTERFACE_LINE_ID =  PO_LINE_TAB(i).INTERFACE_LINE_ID);
               EXCEPTION
                when others then
                   FND_FILE.Put_Line(FND_FILE.Log, '256 : Updating price override:'||i||':'||po_line_tab(i).po_line_id||':'||substr(sqlerrm,1,200));  
               END;
             End If;

          End ;
       
      END IF; 
     EXCEPTION
      when no_data_found then
       FND_FILE.Put_Line(FND_FILE.Log,l_err_callpoint||':257 : Updating PL/POL:NDF:'||substr(sqlerrm,1,200));
      when others then
       FND_FILE.Put_Line(FND_FILE.Log,l_err_callpoint|| ':257 : Updating PL/POL:OTHERS:'||substr(sqlerrm,1,200));    
     END; 
    End Loop ;
    
     --Shankar TMS 20130904-00655
     FND_FILE.Put_Line(FND_FILE.LOG,'Calling Approval WF');
      FOR i IN po_doc_tab.FIRST..po_doc_tab.LAST
      LOOP
      FND_FILE.Put_Line(FND_FILE.LOG,'Call Approval for BPA:'||po_doc_tab(i).doc_number);
              FOR p_rec
              IN (SELECT pha.po_header_id,
                         pha.org_id,
                         pha.segment1,
                         pha.agent_id,
                         pdt.document_subtype,
                         pdt.document_type_code,
                         pha.authorization_status
                    FROM 
                         --Modified the below line for TMS ## 20141002-00054  by VijaySrinivasan  on 11/10/2014 
                         po_headers_all pha, 
                         --Modified the below line for TMS ## 20141002-00054  by VijaySrinivasan  on 11/10/2014 
                         PO_DOCUMENT_TYPES pdt
                   WHERE     pha.type_lookup_code = pdt.document_subtype
                         AND pha.org_id = pdt.org_id
						 and pha.org_id=FND_PROFILE.VALUE('ORG_ID')  -- Added by Pattabhi on 19-Nov-2014 for WEB ADI testing
                         AND pdt.document_type_code = 'PA'
                         AND pha.segment1 = po_doc_tab (i).doc_number
                         AND pha.authorization_status IN ('INCOMPLETE',
                                                          'REQUIRES REAPPROVAL'))
        LOOP

        SELECT p_rec.po_header_id || '-' || TO_CHAR (po_wf_itemkey_s.NEXTVAL)
          INTO l_item_key
          FROM DUAL;


        po_reqapproval_init1.start_wf_process(
        ItemType => 'POAPPRV'
        , ItemKey => l_item_key
        , WorkflowProcess => 'POAPPRV_TOP'
        , ActionOriginatedFrom => 'PO_FORM'
        , DocumentID => p_rec.po_header_id -- po_header_id
        , DocumentNumber => p_rec.segment1 -- BPA Number
        , PreparerID => p_rec.agent_id -- Buer/Preparer_id
        , DocumentTypeCode => p_rec.document_type_code--'PA'
        , DocumentSubtype => p_rec.document_subtype --'BLANKET'
        , SubmitterAction => 'APPROVE'
        , forwardToID => NULL
        , forwardFromID => NULL
        , DefaultApprovalPathID => NULL
        , Note => NULL
        , PrintFlag => 'N'
        , FaxFlag => 'N'
        , FaxNumber => NULL
        , EmailFlag => 'N'
        , EmailAddress => NULL
        , CreateSourcingRule => 'N'
        , ReleaseGenMethod => 'N'
        , UpdateSourcingRule => 'N'
        , MassUpdateReleases => 'N'
        , RetroactivePriceChange => 'N'
        , OrgAssignChange => 'N'
        , CommunicatePriceChange => 'N'
        , p_Background_Flag => 'N'
        , p_Initiator => NULL
        , p_xml_flag => NULL
        , FpdsngFlag => 'N'
        , p_source_type_code => NULL);
            COMMIT;
        FND_FILE.Put_Line(FND_FILE.LOG,'Submitted Approval for BPA:'||po_doc_tab(i).doc_number);
        END LOOP;
      END LOOP;--Shankar TMS 20130904-00655 END
    -- Check How Many records are there in Staging Table If there no records then Truncate the table.
    l_err_callpoint := 'Before truncate table';
    BEGIN
    Select Count(*)
    INTO l_ROWCOUNT
    From 
         --Modified the below line for TMS ## 20141002-00054  by VijaySrinivasan  on 11/10/2014 
         XXWCPO_BPA_INTERFACE BPA
        where org_id=FND_PROFILE.VALUE('ORG_ID');  -- Added by Pattabhi on 19-Nov-2014 for WEB ADI testing		 ;

    IF l_ROWCOUNT = 0 THEN
       NULL;
       EXECUTE IMMEDIATE 'TRUNCATE TABLE XXWC.XXWCPO_BPA_INTERFACE' ;
    END IF;
    EXCEPTION
    when others then 
       FND_FILE.Put_Line(FND_FILE.Log, '257 : Truncate table:'||substr(sqlerrm,1,200));
    END;

    Commit;


Exception
  When XXWC_ERROR Then
     --dbms_output.put_line(l_fulltext);
     FND_FILE.Put_Line(FND_FILE.Log, l_err_msg);
     FND_FILE.Put_Line(FND_FILE.Output, l_err_msg);
     errbuf := 'Warning';
     retcode := 1;
     ROLLBACK;

  When OTHERS Then
    errbuf := SQLERRM;
    retcode := 2;
    ROLLBACK;
    l_err_msg  := 'Error in ' || l_err_callpoint|| ' WITH  ' || substr(SQLERRM, 1, 1000);
    --dbms_output.put_line(l_fulltext);
    FND_FILE.Put_Line(FND_FILE.Log, l_err_msg);
    FND_FILE.Put_Line(FND_FILE.Output, l_err_msg);

    XXCUS_ERROR_PKG.XXCUS_ERROR_MAIN_API(p_called_from       => l_err_callfrom,
                                         p_calling           => l_err_callpoint,
                                         p_ora_error_msg     => SQLERRM,
                                         p_error_desc        => 'Error running XXWCINV_ITEM_MASS_UPLOAD_PKG package with PROGRAM ERROR',
                                         p_distribution_list => l_distro_list,
                                         p_module            => l_module);

End Load_BPA;

End XXWCINV_ITEM_MASS_UPLOAD_PKG;
/