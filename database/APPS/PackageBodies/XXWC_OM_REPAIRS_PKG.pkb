CREATE OR REPLACE PACKAGE BODY APPS.XXWC_OM_REPAIRS_PKG 
   /*************************************************************************
     $Header XXWC_OM_REPAIRS_PKG $
     Object Name: XXWC_OM_REPAIRS_PKG.pks

     PURPOSE:   This package is used to create RepairType setups

     REVISIONS:
     Ver        Date        Author              Description
     ---------  ----------  ---------------     -------------------------
     1.0        07/01/2013  Gopi Damuluri       Initial Version
   /*************************************************************************/
AS

   g_user_id                  NUMBER       := fnd_global.user_id;

PROCEDURE create_rep_type_trans(p_errbuf   OUT   VARCHAR2,
                                p_retcode  OUT   VARCHAR2)
   /*************************************************************************
     Procedure : CREATE_REP_TYPE_TRANS

     PURPOSE:   This procedure creates Repair Type Transitions.
   ************************************************************************/
IS

-----------------------------------------------------------
-- From Repair Type Cursor
-----------------------------------------------------------
CURSOR cur_frm_rt IS
    select repair_type_id
      from CSD_REPAIR_TYPES_B
     where 1 = 1 
       and res_dt_calc_point = 'RO_CREATION';

-----------------------------------------------------------
-- To Repair Type Cursor
-----------------------------------------------------------
CURSOR cur_to_rt (p_from_rt  IN NUMBER) IS
    select repair_type_id
      from CSD_REPAIR_TYPES_B csd_to
     where 1 = 1 
       and res_dt_calc_point = 'RO_CREATION'
       and repair_type_id    != p_from_rt
       and not exists (select '1'
                         from CSD_RT_TRANS_B csd_frm
                        where csd_frm.from_repair_type_id = p_from_rt
                          and csd_frm.to_repair_type_id   = csd_to.repair_type_id);

-----------------------------------------------------------
-- From Repair Type Cursor
-----------------------------------------------------------

    CURSOR cur_flw_sts (p_from_rt  IN NUMBER, p_to_rt  IN NUMBER) IS
    SELECT distinct common_flow_status_id
      FROM csd_rt_trans_b csd_sts
     WHERE 1 = 1 
       AND from_repair_type_id = 10007
       AND NOT EXISTS (SELECT '1'
                         FROM csd_rt_trans_b csd_sts2
                        WHERE csd_sts2.from_repair_type_id   = p_from_rt
                          AND csd_sts2.to_repair_type_id     = p_to_rt
                          AND csd_sts2.common_flow_status_id = csd_sts.common_flow_status_id);

l_rep_type_trans_seq          NUMBER;
l_frm_rt_id                   NUMBER;
l_to_rt_id                    NUMBER;

-- Error variables
l_err_callfrom          VARCHAR2 (75) DEFAULT 'XXWC_OM_REPAIRS_PKG';
l_err_callpoint         VARCHAR2 (75) DEFAULT 'CREATE_REP_TYPE_TRANS';
l_distro_list           VARCHAR2 (75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
l_err_msg               VARCHAR2 (3000);

BEGIN

FOR rec_frm_rt IN cur_frm_rt LOOP
    l_frm_rt_id := rec_frm_rt.repair_type_id;

  FOR rec_to_rt IN cur_to_rt(l_frm_rt_id) LOOP
    l_to_rt_id := rec_to_rt.repair_type_id;

    FOR rec_flw_sts IN cur_flw_sts(l_frm_rt_id, l_to_rt_id) LOOP

     l_rep_type_trans_seq := NULL;

     SELECT CSD_RT_TRANS_S1.NEXTVAL
       INTO l_rep_type_trans_seq
       FROM DUAL;

     CSD_RT_TRANS_PKG.INSERT_ROW ( l_rep_type_trans_seq                           -- PX_RT_TRAN_ID 
                                 , rec_frm_rt.REPAIR_TYPE_ID                      -- P_FROM_REPAIR_TYPE_ID 
                                 , rec_to_rt.REPAIR_TYPE_ID                       -- P_TO_REPAIR_TYPE_ID 
                                 , rec_flw_sts.COMMON_FLOW_STATUS_ID              -- P_COMMON_FLOW_STATUS_ID 
                                 , 'N'                                            -- P_REASON_REQUIRED_FLAG
                                 , 'N'                                            -- P_CAPTURE_ACTIVITY_FLAG
                                 , 'Y'                                            -- P_ALLOW_ALL_RESP_FLAG 
                                 , 1                                              -- P_OBJECT_VERSION_NUMBER
                                 , NULL                                           -- P_DESCRIPTION
                                 , SYSDATE                                        -- P_CREATION_DATE
                                 , g_user_id                                      -- P_CREATED_BY
                                 , SYSDATE                                        -- P_LAST_UPDATE_DATE
                                 , g_user_id                                      -- P_LAST_UPDATED_BY
                                 , -1                                             -- P_LAST_UPDATE_LOGIN
                                 );
    END LOOP; -- cur_flw_sts
   END LOOP; -- cur_to_rt
END LOOP; -- cur_frm_rt

COMMIT;

EXCEPTION
WHEN OTHERS THEN
         l_err_msg := SUBSTR ((l_err_msg || ' ERROR ' || SUBSTR (SQLERRM, 1, 2000)), 1,2000);

         DBMS_OUTPUT.put_line (l_err_msg);

         xxcus_error_pkg.xxcus_error_main_api
                                        (p_called_from            => l_err_callfrom,
                                         p_calling                => l_err_callpoint,
                                         p_request_id             => NULL,
                                         p_ora_error_msg          => SUBSTR
                                                                        (SQLERRM,
                                                                         1,
                                                                         2000
                                                                        ),
                                         p_error_desc             => SUBSTR
                                                                        (l_err_msg,
                                                                         1,
                                                                         2000
                                                                        ),
                                         p_distribution_list      => l_distro_list,
                                         p_module                 => 'OM'
                                        );
         p_retcode := 2;
         p_errbuf := l_err_msg;
END CREATE_REP_TYPE_TRANS;

END XXWC_OM_REPAIRS_PKG;