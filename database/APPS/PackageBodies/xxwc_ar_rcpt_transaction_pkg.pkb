CREATE OR REPLACE PACKAGE BODY xxwc_ar_rcpt_transaction_pkg 
AS
/*************************************************************************
   *   $Header xxwc_ar_rcpt_mass_writeoff_pkg.pkb $
   *   Module Name: xxwc_ar_rcpt_mass_writeoff_pkg
   *
   *   PURPOSE:   Used in receipt mass write off
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *  1.0        04/24/2018   Niraj K Ranjan          Initial Version
   * ***************************************************************************/
   
   /*************************************************************************
    *   PROCEDURE Name: update_writeoff_rec_status
    *
    *   PURPOSE:   update status and error message in table XXWC_AR_RCPT_MASS_WRITEOFF_TBL
	
	HISTORY
    ===============================================================================
    VERSION DATE          AUTHOR(S)              DESCRIPTION
    ------- -----------   --------------- -----------------------------------------
    1.0     04/24/2018    Niraj K Ranjan   TMS#20160218-00198   AR APEX project - Ability to apply payments in mass
   *****************************************************************************/
   PROCEDURE update_writeoff_rec_status(P_writeoff_rec  IN XXWC.XXWC_AR_RCPT_MASS_WRITEOFF_TBL%ROWTYPE) 
   IS
      PRAGMA AUTONOMOUS_TRANSACTION;
   BEGIN
      UPDATE XXWC_AR_RCPT_MASS_WRITEOFF_TBL
	    SET REQUEST_ID          = P_writeoff_rec.REQUEST_ID
		   ,CASH_RECEIPT_ID     = P_writeoff_rec.CASH_RECEIPT_ID
		   ,STATUS              = NVL(P_writeoff_rec.STATUS,STATUS)
		   ,ERROR_MESSAGE       = P_writeoff_rec.ERROR_MESSAGE
		   ,LAST_UPDATED_BY     = P_writeoff_rec.LAST_UPDATED_BY
		   ,LAST_UPDATE_DATE    = SYSDATE
      WHERE BATCH_ID         = P_writeoff_rec.BATCH_ID
      AND   CUSTOMER_NUMBER  = P_writeoff_rec.CUSTOMER_NUMBER
      AND   RECEIPT_NUMBER	 = P_writeoff_rec.RECEIPT_NUMBER	
      AND   RECEIPT_DATE	 = P_writeoff_rec.RECEIPT_DATE	
      AND   RECEIPT_AMOUNT   = P_writeoff_rec.RECEIPT_AMOUNT 
      AND   ACTIVITY_NAME	 = P_writeoff_rec.ACTIVITY_NAME	
      AND   WRITEOFF_AMOUNT  = P_writeoff_rec.WRITEOFF_AMOUNT ;
      COMMIT;
   EXCEPTION
      WHEN OTHERS THEN
		 ROLLBACK;
   END update_writeoff_rec_status;
   
   /*************************************************************************
    *   PROCEDURE Name: update_reapply_rec_status
    *
    *   PURPOSE:   update status and error message in table XXWC_AR_RCPT_MASS_REAPPLY_TBL
	
	HISTORY
    ===============================================================================
    VERSION DATE          AUTHOR(S)              DESCRIPTION
    ------- -----------   --------------- -----------------------------------------
    1.0     04/27/2018    Niraj K Ranjan   TMS#20160218-00198   AR APEX project - Ability to apply payments in mass
   *****************************************************************************/
   PROCEDURE update_reapply_rec_status(P_reapply_rec  IN XXWC.XXWC_AR_RCPT_MASS_REAPPLY_TBL%ROWTYPE) 
   IS
      PRAGMA AUTONOMOUS_TRANSACTION;
   BEGIN
      UPDATE XXWC_AR_RCPT_MASS_REAPPLY_TBL
	    SET REQUEST_ID          = P_reapply_rec.REQUEST_ID
		   ,CASH_RECEIPT_ID     = P_reapply_rec.CASH_RECEIPT_ID
		   ,STATUS              = NVL(P_reapply_rec.STATUS,STATUS)
		   ,ERROR_MESSAGE       = P_reapply_rec.ERROR_MESSAGE
		   ,LAST_UPDATED_BY     = P_reapply_rec.LAST_UPDATED_BY
		   ,LAST_UPDATE_DATE    = SYSDATE
      WHERE BATCH_ID            = P_reapply_rec.BATCH_ID
      AND   CUSTOMER_NUMBER     = P_reapply_rec.CUSTOMER_NUMBER
      AND   RECEIPT_NUMBER	    = P_reapply_rec.RECEIPT_NUMBER	
      AND   PROCESS_FLAG	    = P_reapply_rec.PROCESS_FLAG	
      AND   RECEIPT_AMOUNT      = P_reapply_rec.RECEIPT_AMOUNT 
      AND   TRANSACTION_NUMBER	= P_reapply_rec.TRANSACTION_NUMBER	
      AND   AMOUNT_APPLIED      = P_reapply_rec.AMOUNT_APPLIED ;
      COMMIT;
   EXCEPTION
      WHEN OTHERS THEN
		 ROLLBACK;
   END update_reapply_rec_status;
   
   /*************************************************************************
    *   PROCEDURE Name: receipt_writeoff
    *
    *   PURPOSE:   AR Receipt write off in mass
	
	HISTORY
    ===============================================================================
    VERSION DATE          AUTHOR(S)              DESCRIPTION
    ------- -----------   ---------------  -----------------------------------------
    1.0     04/24/2018    Niraj K Ranjan   TMS#20160218-00198   AR APEX project - Ability to apply payments in mass
   *****************************************************************************/
   PROCEDURE receipt_writeoff(errbuf              OUT VARCHAR2,
                              retcode             OUT NUMBER,
							  p_batch_id          IN  NUMBER,
							  p_rec_status        IN  VARCHAR2) 
   IS
       g_dflt_email   fnd_user.email_address%TYPE  := 'HDSOracleDevelopers@hdsupply.com';
	   l_writeoff_rec XXWC.XXWC_AR_RCPT_MASS_WRITEOFF_TBL%ROWTYPE;
	   l_request_id NUMBER := fnd_global.conc_request_id;
	   l_last_updated_by NUMBER := fnd_profile.value('USER_ID');
	   l_retcode NUMBER;
	   l_errbuf  CLOB;
       l_errormsg VARCHAR2(2000);
	   l_debug_stmt VARCHAR2(500);
       e_prc_exception     EXCEPTION;
	   
	   l_return_status                  VARCHAR2 (1);
       l_msg_count                      NUMBER;
       l_msg_data                       VARCHAR2 (240);
       l_cash_receipt_id                NUMBER;
       l_count                          NUMBER := 0;
       l_application_ref_type           ar_receivable_applications.application_ref_type%TYPE;
       l_application_ref_id             ar_receivable_applications.application_ref_id%TYPE;
       l_application_ref_num            ar_receivable_applications.application_ref_num%TYPE;
       l_secondary_application_ref_id   ar_receivable_applications.secondary_application_ref_id%TYPE;
       l_receivable_application_id      ar_receivable_applications.receivable_application_id%TYPE;
	   l_applied_payment_schedule_id    ar_receivable_applications_v.applied_payment_schedule_id%TYPE;
	   l_receipt_status                 ar_cash_receipts.status%TYPE;
	   l_receipt_amount                 ar_cash_receipts.amount%TYPE;
	   l_applied_amount                 ar_payment_schedules_all.amount_applied%TYPE;
	   l_receivables_trx_id             ar_receivables_trx_all.receivables_trx_id%TYPE;
	   l_payment_schedule_id            ar_payment_schedules.payment_schedule_id%TYPE;
	   
	   
	   CURSOR cr_rcpts
	   IS
	      SELECT
              BATCH_ID        
             ,REQUEST_ID       
             ,CASH_RECEIPT_ID  
             ,CUSTOMER_NUMBER 
             ,RECEIPT_NUMBER	  
             ,RECEIPT_DATE	  
             ,RECEIPT_AMOUNT 
             ,ACTIVITY_NAME	  
             ,WRITEOFF_AMOUNT  
             ,COMMENTS         
             ,STATUS	          
             ,ERROR_MESSAGE    
             ,CREATED_BY       
             ,CREATION_DATE    
             ,LAST_UPDATED_BY  
             ,LAST_UPDATE_DATE 
             ,LAST_UPDATE_LOGIN
          FROM  xxwc_ar_rcpt_mass_writeoff_tbl
		  where 1=1
		  and  batch_id = p_batch_id
          and status = p_rec_status; --NEW/ERROR
	      
   BEGIN
      --fnd_global.apps_initialize (l_last_updated_by, 50878, 222);
      fnd_file.put_line(fnd_file.log,'Start Program '||to_char(sysdate,'DD-MON-YYYY HH24:MI:SS'));
      l_retcode := 0;
	  l_errbuf  := NULL;
	  --mo_global.init ('AR');
      --mo_global.set_policy_context ('S', '162');
      --fnd_global.apps_initialize (42195, 50878, 222);
      l_debug_stmt := 'fetch list of eligible receipts';
	  FOR rec_rcpts IN cr_rcpts
	  LOOP
	     BEGIN
		    l_debug_stmt := 'Initialize variables';
		    l_errormsg        := null;
		    l_cash_receipt_id := NULL;
			l_receipt_status  := NULL;
			l_writeoff_rec    := NULL;
			--Fetch cash receipt id
			l_debug_stmt := 'Find cash receipt id';
			BEGIN
			   select acr.cash_receipt_id,acr.status,acr.amount
			   INTO l_cash_receipt_id,l_receipt_status,l_receipt_amount
               from AR_CASH_RECEIPTS acr,
                    hz_cust_accounts hca,
                    hz_cust_acct_sites hcas,
                    HZ_CUST_SITE_USES hcsu
               where 1=1
               AND hca.cust_account_id = hcas.cust_account_id
               and hcas.cust_acct_site_id = hcsu.cust_acct_site_id
               and acr.customer_site_use_id = hcsu.SITE_USE_ID
               AND hca.account_NUMBER = rec_rcpts.customer_number
               AND acr.RECEIPT_NUMBER = rec_rcpts.receipt_number
               and trunc(acr.RECEIPT_DATE) =  trunc(rec_rcpts.RECEIPT_DATE)
               AND acr.AMOUNT  = rec_rcpts.RECEIPT_AMOUNT ;
               --AND acr.STATUS = 'UNAPP';
			   IF l_receipt_status = 'REV' THEN
			      l_errormsg   := 'Receipt is in Reversal status';
				  raise e_prc_exception;
			   END IF;
			   
			EXCEPTION
			   WHEN NO_DATA_FOUND THEN
			      l_errormsg   := 'Receipt not found';
				  raise e_prc_exception;
			   WHEN TOO_MANY_ROWS THEN
			      l_errormsg   := 'More than one receipt found';
				  raise e_prc_exception;
			   WHEN OTHERS THEN
			      l_errormsg   := SUBSTR('Unexpected Error:'||l_debug_stmt||'=>'||sqlerrm,1,2000);
				  raise e_prc_exception;
			END;
			
			/*BEGIN
			   SELECT NVL(-(AMOUNT_APPLIED),0) INTO l_applied_amount
			   FROM AR_PAYMENT_SCHEDULES_ALL 
			   WHERE CASH_RECEIPT_ID = l_cash_receipt_id;
			   
			   IF (l_receipt_amount - l_applied_amount - rec_rcpts.writeoff_amount) < 0 THEN
			      l_errormsg   := 'Receipt remaining amount will become -ve after writeoff';
				  raise e_prc_exception;
			   END IF;
			EXCEPTION
			   WHEN NO_DATA_FOUND THEN
			      l_applied_amount := 0;
			   WHEN OTHERS THEN
			      l_errormsg   := 'Unexpected Error:'||sqlerrm;
				  raise e_prc_exception;
			END;*/
			l_debug_stmt := 'Validate activity name';
			BEGIN
			   SELECT receivables_trx_id
			   INTO l_receivables_trx_id
               FROM ar_receivables_trx_all 
			   WHERE name = rec_rcpts.activity_name;
			EXCEPTION
			   WHEN NO_DATA_FOUND THEN
			      l_errormsg   := 'Activity name not found';
				  raise e_prc_exception;
			   WHEN TOO_MANY_ROWS THEN
			      l_errormsg   := 'More than one Activity name found';
				  raise e_prc_exception;
			   WHEN OTHERS THEN
			      l_errormsg   := SUBSTR('Unexpected Error:'||l_debug_stmt||'=>'||sqlerrm,1,2000);
				  raise e_prc_exception;
			END;
			l_debug_stmt := 'Find payment_schedule_id';
			BEGIN
			   SELECT payment_schedule_id
			   INTO l_payment_schedule_id
               FROM ar_payment_schedules
               WHERE trx_number = 'Receipt Write-off';
			EXCEPTION
			   WHEN NO_DATA_FOUND THEN
			      l_errormsg   := 'Receipt Write-off transaction type not found';
				  raise e_prc_exception;
			   WHEN TOO_MANY_ROWS THEN
			      l_errormsg   := 'More than one transaction type for Receipt Write-off found';
				  raise e_prc_exception;
			   WHEN OTHERS THEN
			      l_errormsg   := SUBSTR('Unexpected Error:'||l_debug_stmt||'=>'||sqlerrm,1,2000);
				  raise e_prc_exception;
			END;
			l_debug_stmt := 'Call API for receipt write-off';
			ar_receipt_api_pub.activity_application
              (p_api_version                       => 1.0,
               p_init_msg_list                     => fnd_api.g_true,
               p_commit                            => fnd_api.g_true,
               p_validation_level                  => fnd_api.g_valid_level_full,
               x_return_status                     => l_return_status,
               x_msg_count                         => l_msg_count,
               x_msg_data                          => l_msg_data,
               p_cash_receipt_id                   => l_cash_receipt_id ,
			   p_amount_applied                    => rec_rcpts.writeoff_amount,
               p_applied_payment_schedule_id       => l_payment_schedule_id,   -- -3
               p_receivables_trx_id                => l_receivables_trx_id,
               p_receivable_application_id         => l_receivable_application_id,
			   p_apply_date                        => SYSDATE,
			   p_apply_gl_date                     => SYSDATE,
               p_application_ref_type              => l_application_ref_type,
               p_application_ref_id                => l_application_ref_id,
               p_application_ref_num               => l_application_ref_num,
               p_secondary_application_ref_id      => l_secondary_application_ref_id
              );
	        
	   	    IF l_return_status = 'S' THEN
		      COMMIT;
			  fnd_file.put_line(fnd_file.log,'Receipt write-off succeeded for cash receipt id:'||l_cash_receipt_id||
			                    ' application_ref_num:'||l_application_ref_num);
	        ELSE
	          IF l_msg_count = 1 THEN
                  l_errormsg := l_msg_data;
              ELSIF l_msg_count > 1 THEN
                 LOOP
                    l_count := l_count + 1;
					l_msg_data := fnd_msg_pub.get (fnd_msg_pub.g_next, fnd_api.g_false);
                    l_errormsg := substr(l_errormsg||'|'||l_msg_data,1,2000);
                    IF l_msg_data IS NULL THEN
                       EXIT;
                    END IF;
                 END LOOP;
              END IF;
			  fnd_file.put_line(fnd_file.log,'API error for cash receipt id:'||l_cash_receipt_id||'=>'||l_errormsg);
			  RAISE e_prc_exception;
	        END IF;
			
			l_writeoff_rec.BATCH_ID          := rec_rcpts.BATCH_ID         ;
            l_writeoff_rec.REQUEST_ID        := l_request_id               ;
            l_writeoff_rec.CASH_RECEIPT_ID   := l_cash_receipt_id          ;
            l_writeoff_rec.CUSTOMER_NUMBER   := rec_rcpts.CUSTOMER_NUMBER  ;
            l_writeoff_rec.RECEIPT_NUMBER	 := rec_rcpts.RECEIPT_NUMBER   ; 
            l_writeoff_rec.RECEIPT_DATE	     := rec_rcpts.RECEIPT_DATE	   ; 
            l_writeoff_rec.RECEIPT_AMOUNT    := rec_rcpts.RECEIPT_AMOUNT   ;
            l_writeoff_rec.ACTIVITY_NAME	 := rec_rcpts.ACTIVITY_NAME	   ; 
            l_writeoff_rec.WRITEOFF_AMOUNT   := rec_rcpts.WRITEOFF_AMOUNT  ;
            l_writeoff_rec.COMMENTS          := rec_rcpts.COMMENTS         ;
            l_writeoff_rec.STATUS	         := 'SUCCESS'	               ; 
            l_writeoff_rec.ERROR_MESSAGE     := l_errormsg                 ;
            l_writeoff_rec.CREATED_BY        := rec_rcpts.CREATED_BY       ;
            l_writeoff_rec.CREATION_DATE     := rec_rcpts.CREATION_DATE    ;
            l_writeoff_rec.LAST_UPDATED_BY   := l_last_updated_by          ;
            l_writeoff_rec.LAST_UPDATE_DATE  := rec_rcpts.LAST_UPDATE_DATE ;
            l_writeoff_rec.LAST_UPDATE_LOGIN := l_last_updated_by          ;
			  
			update_writeoff_rec_status(P_writeoff_rec  => l_writeoff_rec);
         EXCEPTION
		    WHEN e_prc_exception THEN
			  ROLLBACK;
			  l_writeoff_rec.BATCH_ID          := rec_rcpts.BATCH_ID         ;
              l_writeoff_rec.REQUEST_ID        := l_request_id               ;
              l_writeoff_rec.CASH_RECEIPT_ID   := l_cash_receipt_id          ;
              l_writeoff_rec.CUSTOMER_NUMBER   := rec_rcpts.CUSTOMER_NUMBER  ;
              l_writeoff_rec.RECEIPT_NUMBER	   := rec_rcpts.RECEIPT_NUMBER	 ; 
              l_writeoff_rec.RECEIPT_DATE	   := rec_rcpts.RECEIPT_DATE	 ; 
              l_writeoff_rec.RECEIPT_AMOUNT    := rec_rcpts.RECEIPT_AMOUNT   ;
              l_writeoff_rec.ACTIVITY_NAME	   := rec_rcpts.ACTIVITY_NAME	 ; 
              l_writeoff_rec.WRITEOFF_AMOUNT   := rec_rcpts.WRITEOFF_AMOUNT  ;
              l_writeoff_rec.COMMENTS          := rec_rcpts.COMMENTS         ;
              l_writeoff_rec.STATUS	           := 'ERROR'	                 ; 
              l_writeoff_rec.ERROR_MESSAGE     := l_errormsg                 ;
              l_writeoff_rec.CREATED_BY        := rec_rcpts.CREATED_BY       ;
              l_writeoff_rec.CREATION_DATE     := rec_rcpts.CREATION_DATE    ;
              l_writeoff_rec.LAST_UPDATED_BY   := l_last_updated_by          ;
              l_writeoff_rec.LAST_UPDATE_DATE  := rec_rcpts.LAST_UPDATE_DATE ;
              l_writeoff_rec.LAST_UPDATE_LOGIN := l_last_updated_by          ;
			  
			  update_writeoff_rec_status(P_writeoff_rec  => l_writeoff_rec);
		 END;
	  END LOOP;
	  fnd_file.put_line(fnd_file.log,'End Program '||to_char(sysdate,'DD-MON-YYYY HH24:MI:SS'));
	  errbuf  := l_errbuf;
	  retcode := l_retcode;
   EXCEPTION
       WHEN OTHERS THEN
	      l_retcode := 2;
		  l_errormsg := substr(l_debug_stmt||'=>'||SQLERRM,1,2000);
	      l_errbuf := l_errormsg;
		  errbuf  := l_errbuf;
	      retcode := l_retcode;
		  ROLLBACK;
		  xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_AR_RCPT_TRANSACTION_PKG.RECEIPT_WRITEOFF',
            p_calling             => l_debug_stmt,
            p_request_id          => l_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (l_errormsg, 1, 240),
            p_distribution_list   => g_dflt_email,
            p_module              => 'AR');
		  
   END receipt_writeoff;
   
   /*************************************************************************
    *   PROCEDURE Name: receipt_reapply
    *
    *   PURPOSE:   AR Receipt apply/unapply/both in mass
	
	HISTORY
    ===============================================================================
    VERSION DATE          AUTHOR(S)              DESCRIPTION
    ------- -----------   ---------------  -----------------------------------------
    1.0     04/24/2018    Niraj K Ranjan   TMS#20160218-00198   AR APEX project - Ability to apply payments in mass
   *****************************************************************************/
   PROCEDURE receipt_reapply(errbuf              OUT VARCHAR2,
                             retcode             OUT NUMBER,
							 p_batch_id          IN  NUMBER,
							 p_rec_status        IN  VARCHAR2) 
   IS
       g_dflt_email   fnd_user.email_address%TYPE  := 'HDSOracleDevelopers@hdsupply.com';
	   l_reapply_rec XXWC.XXWC_AR_RCPT_MASS_REAPPLY_TBL%ROWTYPE;
	   l_request_id NUMBER := fnd_global.conc_request_id;
	   l_last_updated_by NUMBER := fnd_profile.value('USER_ID');
	   l_retcode NUMBER;
	   l_errbuf  CLOB;
       l_errormsg VARCHAR2(2000);
	   l_debug_stmt VARCHAR2(500);
       e_prc_exception     EXCEPTION;
	   
	   l_return_status                  VARCHAR2 (1);
       l_msg_count                      NUMBER;
       l_msg_data                       VARCHAR2 (240);
       l_cash_receipt_id                NUMBER;
       l_count                          NUMBER := 0;
       l_application_ref_type           ar_receivable_applications.application_ref_type%TYPE;
       l_application_ref_id             ar_receivable_applications.application_ref_id%TYPE;
       l_application_ref_num            ar_receivable_applications.application_ref_num%TYPE;
       l_secondary_application_ref_id   ar_receivable_applications.secondary_application_ref_id%TYPE;
       l_receivable_application_id      ar_receivable_applications.receivable_application_id%TYPE;
	   l_applied_payment_schedule_id    ar_receivable_applications_v.applied_payment_schedule_id%TYPE;
	   l_receipt_status                 ar_cash_receipts.status%TYPE;
	   l_receipt_amount                 ar_cash_receipts.amount%TYPE;
	   l_applied_amount                 ar_payment_schedules_all.amount_applied%TYPE;
	   l_receivables_trx_id             ar_receivables_trx_all.receivables_trx_id%TYPE;
	   l_payment_schedule_id            ar_payment_schedules.payment_schedule_id%TYPE;
	   l_customer_trx_id                ra_customer_trx.customer_trx_id%TYPE;
	   l_stg_status                     xxwc_ar_rcpt_mass_reapply_tbl.status%TYPE;
	   	   
	   CURSOR cr_rcpts
	   IS
	      SELECT
              BATCH_ID           
             ,REQUEST_ID         
             ,CASH_RECEIPT_ID    
             ,PROCESS_FLAG       
             ,CUSTOMER_NUMBER 	  
             ,CUSTOMER_NAME      
             ,RECEIPT_NUMBER
             ,RECEIPT_DATE			 
             ,RECEIPT_AMOUNT 	
             ,TRANSACTION_NUMBER 
             ,AMOUNT_APPLIED     
             ,DISCOUNT_AMOUNT      
             ,STATUS	            
             ,ERROR_MESSAGE      
             ,CREATED_BY         
             ,CREATION_DATE      
             ,LAST_UPDATED_BY    
             ,LAST_UPDATE_DATE   
			 ,LAST_UPDATE_LOGIN
          FROM  XXWC_AR_RCPT_MASS_REAPPLY_TBL
		  where 1=1
		  and  batch_id = p_batch_id
          and status = p_rec_status; --NEW/ERROR
	      
   BEGIN
      --fnd_global.apps_initialize (l_last_updated_by, 50878, 222);
      fnd_file.put_line(fnd_file.log,'Start Program '||to_char(sysdate,'DD-MON-YYYY HH24:MI:SS'));
      l_retcode := 0;
	  l_errbuf  := NULL;
	  --mo_global.init ('AR');
      --mo_global.set_policy_context ('S', '162');
      --fnd_global.apps_initialize (42195, 50878, 222);
      l_debug_stmt := 'fetch list of eligible receipts';
	  FOR rec_rcpts IN cr_rcpts
	  LOOP
	     BEGIN
		    l_debug_stmt := 'Initialize variables';
		    l_errormsg        := null;
		    l_cash_receipt_id := NULL;
			l_receipt_status  := NULL;
			l_reapply_rec     := NULL;
			l_payment_schedule_id := null;
			l_receivable_application_id := null;
			l_return_status := null;
			l_stg_status := '';
			--Validate Process Flag
			IF rec_rcpts.process_flag NOT IN ('Apply','Unapply','Both')
			THEN
			   l_errormsg   := 'Process Flag is not valid';
               l_stg_status := 'ERROR';
               raise e_prc_exception;
			END IF;
			--Fetch cash receipt id
			l_debug_stmt := 'Find cash receipt id';
			BEGIN
			   select acr.cash_receipt_id,acr.status,acr.amount
			   INTO l_cash_receipt_id,l_receipt_status,l_receipt_amount
               from AR_CASH_RECEIPTS acr,
                    hz_cust_accounts hca,
                    hz_cust_acct_sites hcas,
                    HZ_CUST_SITE_USES hcsu
               where 1=1
               and hca.cust_account_id = hcas.cust_account_id
               and hcas.cust_acct_site_id = hcsu.cust_acct_site_id
               and acr.customer_site_use_id = hcsu.SITE_USE_ID
               AND hca.account_number = rec_rcpts.customer_number
               AND acr.RECEIPT_NUMBER = rec_rcpts.receipt_number
               and trunc(acr.RECEIPT_DATE) =  trunc(rec_rcpts.RECEIPT_DATE)
               AND acr.AMOUNT  = rec_rcpts.RECEIPT_AMOUNT ;
			
               --AND acr.STATUS = 'UNAPP';
			   IF l_receipt_status = 'REV' THEN
			      l_errormsg   := 'Receipt is in Reversal status';
				  l_stg_status     := 'ERROR';
				  raise e_prc_exception;
			   END IF;
			   
			EXCEPTION
			   WHEN NO_DATA_FOUND THEN
			      l_errormsg   := 'Receipt not found';
				  l_stg_status     := 'ERROR';
				  raise e_prc_exception;
			   WHEN TOO_MANY_ROWS THEN
			      l_errormsg   := 'More than one receipt found';
				  l_stg_status     := 'ERROR';
				  raise e_prc_exception;
			   WHEN OTHERS THEN
			      l_errormsg   := SUBSTR('Unexpected Error:'||l_debug_stmt||'=>'||sqlerrm,1,2000);
				  l_stg_status := 'ERROR';
				  raise e_prc_exception;
			END;
			l_debug_stmt := 'Validate transaction number';
			BEGIN
			   SELECT CUSTOMER_TRX_ID 
			   INTO l_customer_trx_id
			   FROM ra_customer_trx 
			   WHERE trx_Number = rec_rcpts.transaction_number;
			EXCEPTION
			   WHEN NO_DATA_FOUND THEN
			      l_customer_trx_id := 0;
				  l_errormsg   := 'Transaction Number not found';
				  l_stg_status := 'ERROR';
				  raise e_prc_exception;
			   WHEN OTHERS THEN
			      l_errormsg   := SUBSTR('Unexpected Error:'||l_debug_stmt||'=>'||sqlerrm,1,2000);
				  l_stg_status := 'ERROR';
				  raise e_prc_exception;
			END;
			/*l_debug_stmt := 'Validate activity name';
			BEGIN
			   SELECT receivables_trx_id
			   INTO l_receivables_trx_id
               FROM ar_receivables_trx_all 
			   WHERE name = rec_rcpts.activity_name;
			EXCEPTION
			   WHEN NO_DATA_FOUND THEN
			      l_errormsg   := 'Activity name not found';
				  raise e_prc_exception;
			   WHEN TOO_MANY_ROWS THEN
			      l_errormsg   := 'More than one Activity name found';
				  raise e_prc_exception;
			   WHEN OTHERS THEN
			      l_errormsg   := SUBSTR('Unexpected Error:'||l_debug_stmt||'=>'||sqlerrm,1,2000);
				  raise e_prc_exception;
			END;*/
			IF rec_rcpts.process_flag IN ('Unapply','Both') THEN
			   l_debug_stmt := 'Find payment_schedule_id';
			   BEGIN
			      select applied_payment_schedule_id, receivable_application_id
			      into l_payment_schedule_id,l_receivable_application_id
			      from ar_receivable_applications_v 
                  where cash_receipt_id = l_cash_receipt_id
                  and trx_Number = rec_rcpts.transaction_number;
			   EXCEPTION
			      WHEN NO_DATA_FOUND THEN
			         l_errormsg   := 'applied_payment_schedule_id not found';
					 l_stg_status := 'ERROR';
			   	     raise e_prc_exception;
			      WHEN TOO_MANY_ROWS THEN
			         l_errormsg   := 'More than one applied_payment_schedule_id found';
					 l_stg_status := 'ERROR';
			   	     raise e_prc_exception;
			      WHEN OTHERS THEN
			         l_errormsg   := SUBSTR('Unexpected Error:'||l_debug_stmt||'=>'||sqlerrm,1,2000);
			   	     l_stg_status := 'ERROR';
					 raise e_prc_exception;
			   END;
			END IF;
			
			IF rec_rcpts.process_flag IN ('Unapply','Both') THEN
			   l_debug_stmt := 'Call API to Unapply Receipt';
			   ar_receipt_api_pub.unapply
                  (p_api_version                      => 1.0,
                   p_init_msg_list                    => fnd_api.g_true,
                   p_commit                           => fnd_api.g_true,
                   p_validation_level                 => fnd_api.g_valid_level_full,
                   x_return_status                    => l_return_status,
                   x_msg_count                        => l_msg_count,
                   x_msg_data                         => l_msg_data,
                   p_cash_receipt_id                  => l_cash_receipt_id,
                   p_applied_payment_schedule_id      => l_payment_schedule_id,
			   	   p_receivable_application_id        => l_receivable_application_id
                   --p_reversal_gl_date                 => sysdate
                  );
		       IF l_return_status = 'S' THEN
		         COMMIT;
				 l_stg_status     := 'SUCCESS';
			     fnd_file.put_line(fnd_file.log,'Receipt Unapply succeeded for cash receipt id:'||l_cash_receipt_id||
			                    ' Trx Number:'||rec_rcpts.transaction_number);
	           ELSE
	             IF l_msg_count = 1 THEN
                    l_errormsg := l_msg_data;
                 ELSIF l_msg_count > 1 THEN
                    LOOP
                       l_count := l_count + 1;
				  	   l_msg_data := fnd_msg_pub.get (fnd_msg_pub.g_next, fnd_api.g_false);
                       l_errormsg := substr(l_errormsg||'|'||l_msg_data,1,2000);
                       IF l_msg_data IS NULL THEN
                          EXIT;
                       END IF;
                    END LOOP;
                 END IF;
			     fnd_file.put_line(fnd_file.log,'Unapply API error for cash receipt id:'||l_cash_receipt_id||'=>'||l_errormsg);
			     l_stg_status     := 'ERROR';
				 RAISE e_prc_exception;
	           END IF;
		    END IF;
			IF rec_rcpts.process_flag IN ('Apply','Both') THEN
			   l_debug_stmt := 'Call API to Apply Receipt';
			   ar_receipt_api_pub.apply( p_api_version     => 1.0,
                                 p_init_msg_list    => fnd_api.g_true,
                                 p_commit           => fnd_api.g_true, 
                                 p_validation_level => fnd_api.g_valid_level_full, 
                                 x_return_status    => l_return_status, 
                                 x_msg_count        => l_msg_count,
                                 x_msg_data         => l_msg_data,
                                 p_cash_receipt_id  => l_cash_receipt_id,
                                 p_customer_trx_id  => l_customer_trx_id,
                                 p_applied_payment_schedule_id => NULL ,
                                 p_amount_applied   => rec_rcpts.amount_applied, 
								 p_discount         => rec_rcpts.discount_amount,
                                 p_show_closed_invoices => 'Y', 
                                 p_apply_date       => SYSDATE, 
                                 p_apply_gl_date    => SYSDATE
                                 );
               IF l_return_status = 'S' THEN
		         COMMIT;
				 l_stg_status := 'SUCCESS';
			     fnd_file.put_line(fnd_file.log,'Receipt Apply succeeded for cash receipt id:'||l_cash_receipt_id||
			                    ' Trx Number:'||rec_rcpts.transaction_number);
	           ELSE
	             IF l_msg_count = 1 THEN
                    l_errormsg := l_msg_data;
                 ELSIF l_msg_count > 1 THEN
                    LOOP
                       l_count := l_count + 1;
				  	   l_msg_data := fnd_msg_pub.get (fnd_msg_pub.g_next, fnd_api.g_false);
                       l_errormsg := substr(l_errormsg||'|'||l_msg_data,1,2000);
                       IF l_msg_data IS NULL THEN
                          EXIT;
                       END IF;
                    END LOOP;
                 END IF;
			     fnd_file.put_line(fnd_file.log,'Apply API error for cash receipt id:'||l_cash_receipt_id||'=>'||l_errormsg);
			     l_stg_status := 'ERROR';
				 RAISE e_prc_exception;
	           END IF;
            END IF;
			
	   	    l_reapply_rec.BATCH_ID           := rec_rcpts.BATCH_ID          ;
            l_reapply_rec.REQUEST_ID         := l_request_id                ;
            l_reapply_rec.CASH_RECEIPT_ID    := l_cash_receipt_id           ;
            l_reapply_rec.PROCESS_FLAG       := rec_rcpts.PROCESS_FLAG      ;
            l_reapply_rec.CUSTOMER_NUMBER 	 := rec_rcpts.CUSTOMER_NUMBER   ; 
            l_reapply_rec.CUSTOMER_NAME      := rec_rcpts.CUSTOMER_NAME	    ; 
            l_reapply_rec.RECEIPT_NUMBER	 := rec_rcpts.RECEIPT_NUMBER    ;
			l_reapply_rec.RECEIPT_DATE	     := rec_rcpts.RECEIPT_DATE      ;
            l_reapply_rec.RECEIPT_AMOUNT 	 := rec_rcpts.RECEIPT_AMOUNT    ; 
            l_reapply_rec.TRANSACTION_NUMBER := rec_rcpts.TRANSACTION_NUMBER;
            l_reapply_rec.AMOUNT_APPLIED     := rec_rcpts.AMOUNT_APPLIED    ;
            l_reapply_rec.DISCOUNT_AMOUNT    := rec_rcpts.DISCOUNT_AMOUNT   ;
			l_reapply_rec.STATUS	         := l_stg_status                ;
			l_reapply_rec.ERROR_MESSAGE      := l_errormsg                  ;
            l_reapply_rec.CREATED_BY         := rec_rcpts.CREATED_BY        ;
            l_reapply_rec.CREATION_DATE      := rec_rcpts.CREATION_DATE     ;
            l_reapply_rec.LAST_UPDATED_BY    := l_last_updated_by           ;
            l_reapply_rec.LAST_UPDATE_DATE   := sysdate                     ;
			l_reapply_rec.LAST_UPDATE_LOGIN  := l_last_updated_by           ;
			update_reapply_rec_status(p_reapply_rec  => l_reapply_rec);
         EXCEPTION
		    WHEN e_prc_exception THEN
			  ROLLBACK;
			  l_reapply_rec.BATCH_ID           := rec_rcpts.BATCH_ID          ;
              l_reapply_rec.REQUEST_ID         := l_request_id                ;
              l_reapply_rec.CASH_RECEIPT_ID    := l_cash_receipt_id           ;
              l_reapply_rec.PROCESS_FLAG       := rec_rcpts.PROCESS_FLAG      ;
              l_reapply_rec.CUSTOMER_NUMBER    := rec_rcpts.CUSTOMER_NUMBER   ; 
              l_reapply_rec.CUSTOMER_NAME      := rec_rcpts.CUSTOMER_NAME	  ; 
              l_reapply_rec.RECEIPT_NUMBER	   := rec_rcpts.RECEIPT_NUMBER    ;
			  l_reapply_rec.RECEIPT_DATE	   := rec_rcpts.RECEIPT_DATE      ;
              l_reapply_rec.RECEIPT_AMOUNT 	   := rec_rcpts.RECEIPT_AMOUNT    ; 
              l_reapply_rec.TRANSACTION_NUMBER := rec_rcpts.TRANSACTION_NUMBER;
              l_reapply_rec.AMOUNT_APPLIED     := rec_rcpts.AMOUNT_APPLIED    ;
              l_reapply_rec.DISCOUNT_AMOUNT    := rec_rcpts.DISCOUNT_AMOUNT   ;
			  l_reapply_rec.STATUS	           := l_stg_status                ;
			  l_reapply_rec.ERROR_MESSAGE      := l_errormsg                  ;
              l_reapply_rec.CREATED_BY         := rec_rcpts.CREATED_BY        ;
              l_reapply_rec.CREATION_DATE      := rec_rcpts.CREATION_DATE     ;
              l_reapply_rec.LAST_UPDATED_BY    := l_last_updated_by           ;
              l_reapply_rec.LAST_UPDATE_DATE   := sysdate                     ;
			  l_reapply_rec.LAST_UPDATE_LOGIN  := l_last_updated_by           ;
			  
			  update_reapply_rec_status(p_reapply_rec  => l_reapply_rec);
		 END;
	  END LOOP;
	  fnd_file.put_line(fnd_file.log,'End Program '||to_char(sysdate,'DD-MON-YYYY HH24:MI:SS'));
	  errbuf  := l_errbuf;
	  retcode := l_retcode;
   EXCEPTION
       WHEN OTHERS THEN
	      l_retcode := 2;
		  l_errormsg := substr(l_debug_stmt||'=>'||SQLERRM,1,2000);
	      l_errbuf := l_errormsg;
		  errbuf  := l_errbuf;
	      retcode := l_retcode;
		  ROLLBACK;
		  xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_AR_RCPT_TRANSACTION_PKG.RECEIPT_REAPPLY',
            p_calling             => l_debug_stmt,
            p_request_id          => l_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (l_errormsg, 1, 240),
            p_distribution_list   => g_dflt_email,
            p_module              => 'AR');
		  
   END receipt_reapply;
   
   -- -----------------------------------------------------------------------------
    -- ¿ Copyright 2008, HD Supply
    -- All Rights Reserved
    --
    -- Name           : XXWC_CASH_FILE_UPLOAD_PRC
    -- Date Written   : 26-APRIL-2018
    -- Author         : Nancy Pahwa
    --
    -- Modification History: PACKAGE FOR UPLOAD OF CSV FILE FOR USE OF EXTERNAL TABLE
    --
    -- When         Who             TMS Task        Did what
    -- -----------  --------        --------        ---------------------------------------------
    -- 26-APR-2018  Nancy Pahwa  20160218-00198  Initially Created
    ---------------------------------------------------------------------------------
   PROCEDURE xxwc_cash_file_upload_prc(p_filename     IN VARCHAR2,
                                     p_directory    IN VARCHAR2,
                                     p_location     IN VARCHAR2 DEFAULT NULL,
                                     p_new_filename OUT VARCHAR2) AS
    l_file           UTL_FILE.file_type;
    l_blob_len       NUMBER;
    l_pos            INTEGER := 1;
    l_amount         BINARY_INTEGER := 32767;
    l_buffer         RAW(32767);
    l_directory      VARCHAR2(500);
    v_new_filename   VARCHAR2(500);
    l_count          NUMBER;
    l_bfilename      VARCHAR2(500);
    v_bfile          BFILE;
    l_year           NUMBER;
    l_timestamp      NUMBER;
    l_directory_path VARCHAR2(200);
    l_sql            VARCHAR2(200);
  BEGIN
    -- Checking if the Directory exists. Creating a directory if it doesn't exist.
    l_directory    := p_directory;
    v_new_filename := SUBSTR(p_filename,
                             INSTR(p_filename, '/') + 1,
                             LENGTH(p_filename));

    --Dbms_Output.Put_Line(l_directory||'  '||v_new_filename);
    v_bfile := BFILENAME(p_directory, v_new_filename);
    l_file  := UTL_FILE.fopen(p_directory, v_new_filename, 'WB', 32760);
    --      IF DBMS_LOB.FILEEXISTS (v_bfile) = 1
    --      THEN
    FOR rec IN (SELECT blob_content lblob
                  FROM apex_application_temp_files
                 WHERE name = p_filename
                   AND id = id) LOOP
      l_blob_len := DBMS_LOB.getlength(rec.lblob);
      WHILE l_pos < l_blob_len LOOP
        DBMS_LOB.read(rec.lblob, l_amount, l_pos, l_buffer);
        UTL_FILE.put_raw(l_file, l_buffer, FALSE);
        l_pos := l_pos + l_amount;
      END LOOP;
    END LOOP;
    UTL_FILE.fclose(l_file);
    --  END IF;
  EXCEPTION
    WHEN OTHERS THEN
      ROLLBACK;
      -- Close the file if something goes wrong.
      IF UTL_FILE.is_open(l_file) THEN
        UTL_FILE.fclose(l_file);
      END IF;

      RAISE;
  END;
END xxwc_ar_rcpt_transaction_pkg;
/
