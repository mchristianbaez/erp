CREATE OR REPLACE PACKAGE BODY APPS.xxcus_ap_ind_srcg_pkg AS

  /********************************************************************************

  FILE NAME: xxcus_AP_IND_SRCG_PKG.pkb

  PROGRAM TYPE: PL/SQL Package Body

  PURPOSE: Extract Oracle Account Payables Sourcing data for the given input parameters and
           generate a Pipe delimited file. The file will be FTPed to UC4 and
           finally to Sourcing Data Mart.

  SERVICE TICKET: XXXXX

  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     10/20/2011    Gopi Damuluri    Initial creation of the procedure
  2.0     06/24/2013    Gopi Damuluri    ESMS: 199317 Added a new procedure uc4_call which will
                                         be called from UC4.
  2.1     03/03/2014    Gopi Damuluri    TMS# 20140225-00075
                                         Process is modified to consider last 6 week 
                                         invoices irrespective whether GL Period is open/closed
  2.2     06/24/2014    Gopi Damuluri    TMS#  20140624-00015 
                                         To update TAX_ID column in PAY_TO_VENDOR 
                                         extract for both CRP and WC feed
  2.3     11/02/2016    TMS 20161102-00149 / ESMS 482591 - Error in GSC sourcing extract job XXCUS AP Indirect Sourcing Data Outbound Interface
  2.4     15/05/2018    Ashwin Sridhar   TMS#20180726-00107--CoStar Inbound and Outbound Integrations 
  2.5     08/06/2018    Vamshi Singirikonda  TMS# 20180806-00042 -- Sourcing extract for AP invoice line failure for BU AH Harris 
  ********************************************************************************/

  l_calling_proc_name     VARCHAR2(75) := 'xxcus_ap_ind_srcg_pkg.create_payables_file';
  l_pay_group_lookup_code VARCHAR2(100) := 'INVENTORY';
  l_org_id                NUMBER := 163;
  l_creation_date         VARCHAR2(15) := '01-JAN-2005';
  l_lookup_type           VARCHAR2(50) := 'XXCUSAP_BUSINESS_UNIT';
--  l_start_date            DATE := to_date('27-FEB-2012', 'DD-MON-YYYY'); -- TO_DATE('01-JAN-2009','DD-MON-YYYY'); -- Version# 2.1
--  l_end_date              DATE := to_date('28-OCT-2012', 'DD-MON-YYYY'); -- TO_DATE('30-SEP-2011','DD-MON-YYYY'); -- Version# 2.1
  l_start_date            DATE := TRUNC(SYSDATE) - 41; -- Version# 2.1
  l_end_date              DATE := TRUNC(SYSDATE);      -- Version# 2.1
  -- Begin 2.3
  --
    type t_varchar2_32767 is record (myrec varchar2(32767));
    type t_cur_ap_inv_dtl is table of t_varchar2_32767 index by binary_integer;  
  --
  l_file_name_dtl_temp varchar2(240) :=Null;
  l_file_name_dtl varchar2(240) :=Null;
  l_outbound_loc varchar2(240) :=Null; 
  --
  l_result varchar2(2000) :=Null; --2.3
  l_utl_max_linesize NUMBER :=32767;
  g_loc VARCHAR2(80) :=Null; 
  g_command varchar2(2000) :=Null;
  --
  -- End 2.3
  --Email Defaults
  pl_dflt_email fnd_user.email_address%TYPE := 'HDSOracleDevelopers@hdsupply.com';
-- Begin 2.3
  procedure print_log(p_message in varchar2) as
  begin 
   if fnd_global.conc_request_id >0 then
    fnd_file.put_line(fnd_file.log, p_message);
   else
    dbms_output.put_line(p_message);
   end if;
  end print_log;
  --
  function get_location (v_operation in varchar2, v_type  in varchar2) return varchar2 is
    v_loc varchar2(240) :=Null;
  begin
      --      
      if v_operation ='DBA_DIRECTORY' then
       --
           if v_type = 'INBOUND' then 
               -- 
               v_loc := Null;
               --
           elsif v_type = 'OUTBOUND' then
               -- 
               v_loc := 'ORACLE_INT_UC4_APSOURCING';
               --
           else
               -- 
               v_loc := Null;
               --
           end if;
       --
      elsif v_operation ='ABSOLUTE_PATH' then
       --
           if v_type = 'INBOUND' then 
               -- 
               v_loc :=Null;
               --
           elsif v_type = 'OUTBOUND' then
               -- 
               select '/xx_iface/'||lower(name)||'/outbound/uc4/ap/sourcing'
               into    v_loc
               from  v$database;
               --
           else
               -- 
               v_loc := Null;
               --
           end if;
       --
      else
               -- 
               v_loc := Null;
               --
      end if;
      --
      print_log('File location details: Operation :'||v_operation||', Type :'||v_type||', directory =>'||v_loc);
      --
       return v_loc;
      --
  exception
    when others then
     print_log ('get_location, error : '||sqlerrm);
     return null;
     raise program_error;
  end get_location;    
   --
-- End 2.3
  --------------------------------------------------------------------------------
  -- Function to derive Invoice Amount Remaining
  --------------------------------------------------------------------------------
  FUNCTION inv_amt_remaining(p_invoice_id IN NUMBER) RETURN NUMBER IS
    l_amount_remaining NUMBER;
  BEGIN
    SELECT SUM(ps.amount_remaining) amount_remaining
      INTO l_amount_remaining
      FROM ap.ap_payment_schedules_all ps
     WHERE 1 = 1
       AND ps.invoice_id = p_invoice_id
     GROUP BY ps.invoice_id;

    RETURN l_amount_remaining;

  EXCEPTION
    WHEN OTHERS THEN
      RETURN 0;
  END;

  --------------------------------------------------------------------------------
  -- Function to derive Minimum Invoice Date
  --------------------------------------------------------------------------------
  FUNCTION inv_min_due_date(p_invoice_id IN NUMBER) RETURN VARCHAR2 IS
    l_due_date VARCHAR2(10) := '01/01/0001';
  BEGIN
    SELECT to_char(MIN(ps.due_date), 'MM/DD/YYYY')
      INTO l_due_date
      FROM ap.ap_payment_schedules_all ps
     WHERE 1 = 1
       AND ps.invoice_id = p_invoice_id
     GROUP BY ps.invoice_id;

    RETURN l_due_date;

  EXCEPTION
    WHEN OTHERS THEN
      RETURN l_due_date;
  END;

  --------------------------------------------------------------------------------
  -- Function to derive Invoice Payment Date
  --------------------------------------------------------------------------------
  FUNCTION inv_payment_date(p_invoice_id IN NUMBER) RETURN VARCHAR2 IS
    l_payment_date VARCHAR2(10) := '01/01/0001';
  BEGIN
    SELECT to_char(MIN(b.creation_date), 'MM/DD/YYYY')
      INTO l_payment_date
      FROM ap.ap_invoice_payments_all b
          ,ap.ap_checks_all           ac
     WHERE b.invoice_id = p_invoice_id
       AND b.check_id = ac.check_id
       AND ac.status_lookup_code NOT IN ('VOIDED', 'OVERFLOW')
    --AND i.invoice_id = '5l_org_id9203'
     GROUP BY b.invoice_id;

    RETURN l_payment_date;

  EXCEPTION
    WHEN OTHERS THEN
      RETURN l_payment_date;
  END;

  --------------------------------------------------------------------------------
  -- Function to derive appended and pipe delimited value of
  -- InvoiceMerchendiseAmount, FreightAmount and TaxAmount
  --------------------------------------------------------------------------------
  FUNCTION disc_mrc_frt_amnt(p_invoice_id IN NUMBER) RETURN VARCHAR2 IS
    l_disc_mrc_frt_amnt VARCHAR2(200);
  BEGIN
    SELECT SUM(CASE
                 WHEN pd.line_type_lookup_code IN ('ITEM', 'MISCELLANEOUS') THEN
                  pd.amount
               END) || ' | ' || SUM(CASE
                                      WHEN pd.line_type_lookup_code = 'FREIGHT' THEN
                                       pd.amount
                                    END) || ' | ' ||
           SUM(CASE
                 WHEN pd.line_type_lookup_code = 'TAX' THEN
                  pd.amount
               END) || ' | ' || ''
      INTO l_disc_mrc_frt_amnt
      FROM ap.ap_invoice_distributions_all pd
     WHERE pd.invoice_id = p_invoice_id
     GROUP BY pd.invoice_id;

    RETURN l_disc_mrc_frt_amnt;

  EXCEPTION
    WHEN OTHERS THEN
      RETURN NULL;
  END disc_mrc_frt_amnt;

  --------------------------------------------------------------------------------
  -- Procedure to Payment Terms File
  --------------------------------------------------------------------------------
  -- Version   Date             Developer        Description                    --
  -- -------   -----            ----------       ---------------                --
  --  1.1      10-Apr-2013      Luong Vu         Change percentage to decimal   --
  --                                             Incident# 198070               --
  --                                             RFC 36566
  --------------------------------------------------------------------------------
  PROCEDURE create_payment_terms_file(errbuf  OUT VARCHAR2
                                     ,retcode OUT NUMBER) IS

    ----------------------------------------------------------------------
    -- Cursor to derive Payment Term Details
    ----------------------------------------------------------------------
    CURSOR cur_payment_terms IS
      SELECT 'CORPORATE' || ' | ' || -- "BUSINESS_UNIT"
              'ORACLE-CRP' || ' | ' || -- "SOURCE SYSTEMS"
              apt.term_id || ' | ' || -- "TERM ID"
              apt.description || ' | ' || -- "TERM DESCRIPTION"
              (aptl.discount_percent / 100) || ' | ' || -- "PERCENT DISCOUNT"
              aptl.discount_days || ' | ' || -- "DISCOUNT DAYS"
              aptl.due_days || ' | ' || -- "NET DAYS"
             --         aptl.discount_day_of_month  || ' | ' ||   -- "PROXY TERM"
             --         aptl.due_day_of_month       || ' | ' ||   -- "PROXY DAY"
              decode(aptl.discount_day_of_month, NULL, '', 'Y') || ' | ' || -- "PROXY FLAG"
              aptl.discount_day_of_month || ' | ' || -- "PROXY DAY"
             --         aptl.due_day_of_month       || ' | ' ||   -- "PROXY DAY"
              aptl.due_months_forward -- "PROXY MONTH"
              "PAYMENT_TERM_DTLS"
        FROM ap.ap_terms_tl    apt
            ,ap.ap_terms_lines aptl
       WHERE apt.term_id = aptl.term_id;

    --File Variables
    l_file_name      VARCHAR2(150);
    l_file_name_temp VARCHAR2(150);
    l_file_handle    utl_file.file_type;
    l_procedure_name VARCHAR2(75) := 'xxcus_ap_ind_srcg_pkg.create_payment_terms_file';

  BEGIN
    fnd_file.put_line(fnd_file.log,' ');  
    fnd_file.put_line(fnd_file.log,
                      '**********************************************************************************');
    fnd_file.put_line(fnd_file.log,
                      '----------------------------------------------------------------------------------');
    fnd_file.put_line(fnd_file.log,
                      'Start of Procedure : CREATE_PAYMENT_TERMS_FILE');
    fnd_file.put_line(fnd_file.log,
                      '----------------------------------------------------------------------------------');

    l_file_name := 'CRP_PAYMENTTERMS_' || to_char(SYSDATE, 'YYYYMMDD') ||
                   '.ld';
    -- Begin 2.3
    g_loc :='create_payment_terms_file.001'; 
    fnd_file.put_line(fnd_file.log,'@g_loc ='||g_loc||', create_payment_terms_file, l_file_name '||l_file_name);  
    -- End 2.3
    l_file_name_temp := 'TEMP_' || l_file_name;
    -- Begin 2.3
    g_loc :='create_payment_terms_file.002'; 
    fnd_file.put_line(fnd_file.log,'@g_loc ='||g_loc||', create_payment_terms_file, l_file_name_temp '||l_file_name_temp);  
    -- End 2.3
    -- Start creating the Payment Terms file
    -- Begin 2.3    
    l_file_handle := utl_file.fopen('ORACLE_INT_UC4_APSOURCING',
                                    l_file_name_temp, 'w', l_utl_max_linesize);
    g_loc :='create_payment_terms_file.003'; 
    fnd_file.put_line(fnd_file.log,'@g_loc ='||g_loc||', create_payment_terms_file, l_file_handle initalized');  
    -- End 2.3
    FOR rec_payment_terms IN cur_payment_terms
    LOOP
      -- Write Payment Terms info into file
      utl_file.put_line(l_file_handle, rec_payment_terms.payment_term_dtls);
    END LOOP;
    -- Begin 2.3
    g_loc :='create_payment_terms_file.004'; 
    fnd_file.put_line(fnd_file.log,'@g_loc ='||g_loc||', create_payment_terms_file, all lines written.');  
    -- End 2.3
    -- Close the file
    utl_file.fclose(l_file_handle);
    -- Begin 2.3
    g_loc :='create_payment_terms_file.005'; 
    fnd_file.put_line(fnd_file.log,'@g_loc ='||g_loc||', create_payment_terms_file, File '||l_file_name_temp||' closed.');  
    -- End 2.3
    -- 'Rename file for pickup';
    -- Begin 2.3    
    --utl_file.frename('ORACLE_INT_UC4_APSOURCING', l_file_name_temp, 'ORACLE_INT_UC4_APSOURCING', l_file_name, TRUE); --2.3
            begin
                        --
                         g_command := 'mv '
                                     ||' '
                                     ||l_outbound_loc
                                     ||'/'
                                     ||l_file_name_temp
                                     ||' '
                                     ||l_outbound_loc
                                     ||'/'
                                     ||l_file_name
                                     ; 
                         --
                         print_log('MOVE command:');
                         print_log('============');
                         print_log(g_command);
                         --
                         select xxwc_edi_iface_pkg.xxwc_oscommand_run (g_command)
                         into   l_result
                         from   dual;
                         print_log('v_result '||l_result);
                         --
                         if (l_result is null) then 
                                 g_command := 'chmod'
                                             ||' '
                                             ||'777'
                                             ||' '                                             
                                             ||l_outbound_loc
                                             ||'/'
                                             ||l_file_name            
                                             ; 
                                 --
                                 print_log('CHMOD command:');
                                 print_log('============');
                                 print_log(g_command);
                                 --
                                 select xxwc_edi_iface_pkg.xxwc_oscommand_run (g_command)
                                 into   l_result
                                 from   dual;
                                 print_log('v_result '||l_result);   
                                 --                         
                         end if;                        
                         --                           
            exception
                when others then
                 print_log('Error in renaming file: '||l_file_name_dtl_temp||' TO '||l_file_name_dtl||', message =>'||sqlerrm);
                 raise program_error;
            end;
            --        
    g_loc :='create_payment_terms_file.006'; 
    fnd_file.put_line(fnd_file.log,'@g_loc ='||g_loc||', create_payment_terms_file, renamed file '||l_file_name_temp||' TO '||l_file_name);  
    -- End 2.3
    fnd_file.put_line(fnd_file.log,
                      '----------------------------------------------------------------------------------');
    fnd_file.put_line(fnd_file.log,
                      'End of Procedure : CREATE_PAYMENT_TERMS_FILE');
    fnd_file.put_line(fnd_file.log,
                      '----------------------------------------------------------------------------------');
    fnd_file.put_line(fnd_file.log,
                      '**********************************************************************************');

  EXCEPTION
    WHEN OTHERS THEN
      errbuf := 'Error in procedure - CREATE_PAYMENT_TERMS_FILE. Error - ' ||
                SQLERRM;

      xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_calling_proc_name,
                                           p_calling => l_procedure_name,
                                           p_request_id => fnd_global.conc_request_id,
                                           p_ora_error_msg => SQLERRM,
                                           p_error_desc => errbuf,
                                           p_distribution_list => pl_dflt_email,
                                           p_module => 'AP');
  END create_payment_terms_file;


  /********************************************************************************
  PROCEDURE NAME: create_pay_to_vendor_file
  
  PURPOSE: Generate Pay To Vendor File
  
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     10/20/2011    Gopi Damuluri    Initial creation of the procedure
  2.2     06/24/2014    Gopi Damuluri    TMS#  20140624-00015 
                                         To update TAX_ID column in PAY_TO_VENDOR 
                                         extract for both CRP and WC feed
  ********************************************************************************/
  --------------------------------------------------------------------------------
  -- Procedure to create Pay To Vendor File
  --------------------------------------------------------------------------------
  PROCEDURE create_pay_to_vendor_file(errbuf  OUT VARCHAR2
                                     ,retcode OUT NUMBER) IS

    ----------------------------------------------------------------------
    -- Cursor to derive Pay To Vendor Details
    ----------------------------------------------------------------------
    CURSOR cur_pay_to_vendor IS
      SELECT 'CORPORATE' || ' | ' || -- "BUSINESS_UNIT"
             'ORACLE-CRP' || ' | ' || --"SOURCE SYSTEMS"
             --Commented the below CASE statement by Ashwin.S on 15-May-2018
             /*CASE povs.pay_group_lookup_code
               WHEN 'AP SUPPLIER' THEN
                ''
               WHEN 'ELECTRICAL' THEN
                ''
               WHEN 'PLANS SPECS' THEN
                ''
               WHEN 'FINAL APPROVED' THEN
                ''
               WHEN 'EXPENSE' THEN
                ''
               WHEN 'PLUMBING' THEN
                ''
               WHEN 'REAL ESTATE' THEN
                'REAL ESTATE' --Added the logic by Ashwin.S on 02-May-2018
               WHEN 'ASSET LEASE' THEN
                'REAL ESTATE' --Added the logic by Ashwin.S on 02-May-2018
               WHEN 'HDS INTERC' THEN
                ''
               WHEN 'LOCAL CHECKS' THEN
                ''
               WHEN 'INTERCOMPANY INVOICE' THEN
                ''
               WHEN 'AP WIRE TRANSFER' THEN
                ''
               WHEN 'RENT' THEN
                'REAL ESTATE' --Added the logic by Ashwin.S on 02-May-2018
               WHEN 'IWO' THEN
                ''
               WHEN 'HDSOOP' THEN
                ''
               WHEN 'CUSTOMER REFUND' THEN
                ''
               WHEN 'ZERO CHECK' THEN
                ''
               WHEN 'HDS INTERCO' THEN
                ''
               WHEN 'IWO' THEN
                ''
               WHEN 'REBATE' THEN
                ''
               WHEN 'UTILITIES' THEN
                ''
               WHEN 'TERM EMPLOYEE' THEN
                ''
               WHEN 'TAX' THEN
                'TAX'
               WHEN 'EMPLOYEE' THEN
                'EMPLOYEE'
               WHEN 'FREIGHT' THEN
                'FREIGHT'*/
--Added the Below case condition by Ashwin Sridhar on 15-May-2018 for #Version 2.4
CASE
 WHEN (
( ( pov.vendor_type_lookup_code IN ('REAL ESTATE','RENT','ASSET LEASE') OR povs.pay_group_lookup_code IN ('REAL ESTATE','RENT','ASSET LEASE')) AND
  (NVL(pov.END_DATE_ACTIVE,SYSDATE)>=SYSDATE OR NVL(povs.INACTIVE_DATE,SYSDATE)>=SYSDATE) )
 )  THEN 'REAL ESTATE'   
 WHEN povs.pay_group_lookup_code IN ('EMPLOYEE','FREIGHT','TAX') THEN povs.pay_group_lookup_code
  ELSE NULL 
END|| ' | ' ||  -- "VENDOR_TYPE" -- Added to clean up vendor type - Sbreland
             CASE
               WHEN povs.inactive_date IS NULL THEN
                'ACTIVE'
               WHEN povs.inactive_date IS NOT NULL THEN
                'INACTIVE'
             END || ' | ' || -- "VENDOR_STATUS"
             pov.segment1 || '-' || povs.vendor_site_id || ' | ' || -- "VENDOR_SITE_ID"
             pov.vendor_name || ' | ' || -- "Vendor Mailing Address Name"
             pov.vendor_name || ' | ' || -- "Pay to Vendor Name"
             REPLACE(REPLACE(NVL(pov.num_1099, pov.individual_1099), '-', ''), '/', '') || ' | ' || -- "TAX_ID" -- Version# 2.2
             povs.address_line1 || ' | ' || -- ADDRESS LINE 1 (Pay-to Mailing Address 1)
             povs.address_line2 || ' | ' || -- ADDRESS LINE 2 (Pay-to Mailing Address 2)
             povs.address_line3 || ' | ' || -- ADDRESS LINE 3 (Pay-to Mailing Address 3)
             povs.city || ' | ' || -- VENDOR CITY (Pay-to Maling City)
             povs.state || ' | ' || -- VENDOR STATE (Pay-to Maling State/Province)
             povs.zip || ' | ' || -- VENDOR ZIP CODE (Pay-to Maling Postal Code)
             decode(povs.fax_area_code, NULL, povs.fax,
                    povs.fax_area_code || '-' || substr(povs.fax, 1, 3) || '-' ||
                     substr(povs.fax, 4, 7)) || ' | ' || -- VENDOR FAX NUMBER (Pay-to Mailing Fax Number)
             povs.country || ' | ' || -- VENDOR COUNTRY (Pay-to Mailing Country)
             decode(povs.area_code, NULL, povs.phone,
                    povs.area_code || '-' || substr(povs.phone, 1, 3) || '-' ||
                     substr(povs.phone, 4, 7)) || ' | ' || -- VENDOR TELEPHONE NUMBER (Pay-to Maling Phone Number)
             '' || ' | ' || -- "Pay-To Physical Address Name"
             '' || ' | ' || -- "Pay-To Physical Address Line 1"
             '' || ' | ' || -- "Pay-To Physical Address Line 2"
             '' || ' | ' || -- "Pay-To Physical City"
             '' || ' | ' || -- "Pay-To Physical State/Province"
             '' || ' | ' || -- "Pay-To Physical Postal Code"
             '' || ' | ' || -- "Pay-To Physical Fax"
             '' || ' | ' || -- "Pay-To Physical Country"
             '' || ' | ' || -- "Pay-To Physical Phone Number"
             apt.term_id || ' | ' || -- "Payment Term ID"
             to_char(povs.creation_date, 'MM/DD/YYYY') -- "Creation Date"
             "PAY_TO_VENDOR_DTLS"
        FROM ap_suppliers          pov
            ,ap_supplier_sites_all povs
            ,ap.ap_terms_tl        apt
       WHERE pov.vendor_id = povs.vendor_id
         AND nvl(povs.terms_id, pov.terms_id) = apt.term_id
--         AND povs.org_id = l_org_id          -- Version# 2.1
         AND povs.org_id IN (163, 167)         -- Version# 2.1
         AND povs.creation_date >= l_creation_date
         AND povs.pay_group_lookup_code != l_pay_group_lookup_code
         AND povs.pay_site_flag = 'Y'
       ORDER BY povs.vendor_site_id;

    --File Variables
    l_file_name      VARCHAR2(150);
    l_file_name_temp VARCHAR2(150);
    l_file_handle    utl_file.file_type;
    l_procedure_name VARCHAR2(75) := 'xxcus_ap_ind_srcg_pkg.create_pay_to_vendor_file';

  BEGIN
    fnd_file.put_line(fnd_file.log,' ');  
    fnd_file.put_line(fnd_file.log,
                      '**********************************************************************************');
    fnd_file.put_line(fnd_file.log,
                      '----------------------------------------------------------------------------------');
    fnd_file.put_line(fnd_file.log,
                      'Start of Procedure : CREATE_PAY_TO_VENDOR_FILE');
    fnd_file.put_line(fnd_file.log,
                      '----------------------------------------------------------------------------------');

    l_file_name := 'CRP_PAYTOVENDOR_' || to_char(SYSDATE, 'YYYYMMDD') ||
                   '.ld';
    -- Begin 2.3
    g_loc :='create_pay_to_vendor_file.001'; 
    fnd_file.put_line(fnd_file.log,'@g_loc ='||g_loc||', create_pay_to_vendor_file, l_file_name  = '||l_file_name);  
    -- End 2.3
    l_file_name_temp := 'TEMP_' || l_file_name;
    -- Begin 2.3
    g_loc :='create_pay_to_vendor_file.002';
    fnd_file.put_line(fnd_file.log,'@g_loc ='||g_loc||', create_pay_to_vendor_file, l_file_name_temp  = '||l_file_name_temp);  
    -- End 2.3
    -- Start creating the Pay To Vendor file
    -- Begin 2.3
    l_file_handle := utl_file.fopen('ORACLE_INT_UC4_APSOURCING',
                                    l_file_name_temp, 'w', l_utl_max_linesize);
    g_loc :='create_pay_to_vendor_file.003';
    fnd_file.put_line(fnd_file.log,'@g_loc ='||g_loc||', create_pay_to_vendor_file, l_file_handle initialized');  
    -- End 2.3
    FOR rec_pay_to_vendor IN cur_pay_to_vendor
    LOOP
      -- Write Pay To Vendor info into file
      utl_file.put_line(l_file_handle, rec_pay_to_vendor.pay_to_vendor_dtls);
    END LOOP;
    -- Begin 2.3
    g_loc :='create_pay_to_vendor_file.004';
    fnd_file.put_line(fnd_file.log,'@g_loc ='||g_loc||', create_pay_to_vendor_file, all lines written.');  
    -- End 2.3
    -- Close the file
    utl_file.fclose(l_file_handle);
    -- Begin 2.3
    g_loc :='create_pay_to_vendor_file.005';
    fnd_file.put_line(fnd_file.log,'@g_loc ='||g_loc||', create_pay_to_vendor_file, file '||l_file_name||' closed.');  
    -- End 2.3
    -- 'Rename file for pickup';
    -- Begin 2.3    
    --utl_file.frename('ORACLE_INT_UC4_APSOURCING', l_file_name_temp,'ORACLE_INT_UC4_APSOURCING', l_file_name, TRUE);
            begin
                        --
                         g_command := 'mv '
                                     ||' '
                                     ||l_outbound_loc
                                     ||'/'
                                     ||l_file_name_temp
                                     ||' '
                                     ||l_outbound_loc
                                     ||'/'
                                     ||l_file_name
                                     ; 
                         --
                         print_log('MOVE command:');
                         print_log('============');
                         print_log(g_command);
                         --
                         select xxwc_edi_iface_pkg.xxwc_oscommand_run (g_command)
                         into   l_result
                         from   dual;
                         print_log('v_result '||l_result);   
                         --
                         if (l_result is null) then 
                                 g_command := 'chmod'
                                             ||' '
                                             ||'777'
                                             ||' '                                             
                                             ||l_outbound_loc
                                             ||'/'
                                             ||l_file_name          
                                             ; 
                                 --
                                 print_log('CHMOD command:');
                                 print_log('============');
                                 print_log(g_command);
                                 --
                                 select xxwc_edi_iface_pkg.xxwc_oscommand_run (g_command)
                                 into   l_result
                                 from   dual;
                                 print_log('v_result '||l_result);   
                                 --                         
                         end if;                        
                         -- 
            exception
                when others then
                 print_log('Error in renaming file: '||l_file_name_dtl_temp||' TO '||l_file_name_dtl||', message =>'||sqlerrm);
                 raise program_error;
            end;
            --       
    g_loc :='create_pay_to_vendor_file.006';
    fnd_file.put_line(fnd_file.log,'@g_loc ='||g_loc||', create_pay_to_vendor_file, renamed file '||l_file_name_temp||' TO '||l_file_name);  
    -- End 2.3
    fnd_file.put_line(fnd_file.log,
                      '----------------------------------------------------------------------------------');
    fnd_file.put_line(fnd_file.log,
                      'End of Procedure : CREATE_PAY_TO_VENDOR_FILE');
    fnd_file.put_line(fnd_file.log,
                      '----------------------------------------------------------------------------------');
    fnd_file.put_line(fnd_file.log,
                      '**********************************************************************************');

  EXCEPTION
    WHEN OTHERS THEN
      errbuf := 'Error in procedure - CREATE_PAY_TO_VENDOR_FILE. Error - ' ||
                SQLERRM;

      xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_calling_proc_name,
                                           p_calling => l_procedure_name,
                                           p_request_id => fnd_global.conc_request_id,
                                           p_ora_error_msg => SQLERRM,
                                           p_error_desc => errbuf,
                                           p_distribution_list => pl_dflt_email,
                                           p_module => 'AP');
  END create_pay_to_vendor_file;

  --------------------------------------------------------------------------------
  -- Procedure to Native GL To Khalix File
  --------------------------------------------------------------------------------
  PROCEDURE create_khalix_file(errbuf  OUT VARCHAR2
                              ,retcode OUT NUMBER) IS

    ----------------------------------------------------------------------
    -- Cursor to derive Native GL To Khalix Details
    ----------------------------------------------------------------------
    CURSOR cur_kahlix IS
      SELECT 'CORPORATE' || ' | ' || -- "BUSINESS_UNIT"
             'ORACLE-CRP' || ' | ' || -- "FINANCIAL_SOURCE_SYSTEM"
             fvv.flex_value || ' | ' || -- "NATIVE_GL_ACCT"
             fvv.description || ' | ' || 'A' || fvv.flex_value -- "KHALIX_ACCT"
             "KAHLIX_DETAILS"
        FROM apps.fnd_flex_values_vl  fvv
            ,apps.fnd_flex_value_sets fvs
       WHERE fvv.flex_value_set_id = fvs.flex_value_set_id
         AND fvs.flex_value_set_name = 'XXCUS_GL_ACCOUNT'
       ORDER BY fvv.flex_value;

    --File Variables
    l_file_name      VARCHAR2(150);
    l_file_name_temp VARCHAR2(150);
    l_file_handle    utl_file.file_type;
    l_procedure_name VARCHAR2(75) := 'xxcus_ap_ind_srcg_pkg.create_khalix_file';

  BEGIN
    fnd_file.put_line(fnd_file.log,' ');  
    fnd_file.put_line(fnd_file.log,
                      '**********************************************************************************');
    fnd_file.put_line(fnd_file.log,
                      '----------------------------------------------------------------------------------');
    fnd_file.put_line(fnd_file.log,
                      'Start of Procedure : CREATE_KHALIX_FILE');
    fnd_file.put_line(fnd_file.log,
                      '----------------------------------------------------------------------------------');

    l_file_name := 'CRP_NATIVEGLTOKHALIX_' || to_char(SYSDATE, 'YYYYMMDD') ||
                   '.ld';
    -- Begin 2.3
    g_loc :='create_khalix_file.001';
    fnd_file.put_line(fnd_file.log,'@g_loc ='||g_loc||', create_khalix_file, l_file_name ='||l_file_name);  
    -- End 2.3
    fnd_file.put_line(fnd_file.log, 'l_file_name          :' || l_file_name);

    l_file_name_temp := 'TEMP_' || l_file_name;
    -- Begin 2.3
    g_loc :='create_khalix_file.002';
    fnd_file.put_line(fnd_file.log,'@g_loc ='||g_loc||', create_khalix_file, l_file_name_temp = '||l_file_name_temp);  
    -- End 2.3
    -- Start creating the Native GL To Khalix file
    -- Begin 2.3    
    l_file_handle := utl_file.fopen('ORACLE_INT_UC4_APSOURCING',
                                    l_file_name_temp, 'w', l_utl_max_linesize);
    g_loc :='create_khalix_file.003';
    fnd_file.put_line(fnd_file.log,'@g_loc ='||g_loc||', create_khalix_file, l_file_handle initialized.');  
    -- End 2.3
    FOR rec_kahlix IN cur_kahlix
    LOOP
      -- Write Native GL To Khalix info into file
      utl_file.put_line(l_file_handle, rec_kahlix.kahlix_details);
    END LOOP;
    -- Begin 2.3
    g_loc :='create_khalix_file.004';
    fnd_file.put_line(fnd_file.log,'@g_loc ='||g_loc||', create_khalix_file, all lines written.');  
    -- End 2.3
    -- Close the file
    utl_file.fclose(l_file_handle);
    -- Begin 2.3
    g_loc :='create_khalix_file.005';
    fnd_file.put_line(fnd_file.log,'@g_loc ='||g_loc||', create_khalix_file, l_file_name_temp ='||l_file_name_temp||' closed.');  
    -- End 2.3
    -- 'Rename file for pickup';
    -- Begin 2.3    
    --utl_file.frename('ORACLE_INT_UC4_APSOURCING', l_file_name_temp, 'ORACLE_INT_UC4_APSOURCING', l_file_name, TRUE);
            begin
                        --
                         g_command := 'mv '
                                     ||' '
                                     ||l_outbound_loc
                                     ||'/'
                                     ||l_file_name_temp
                                     ||' '
                                     ||l_outbound_loc
                                     ||'/'
                                     ||l_file_name
                                     ; 
                         --
                         print_log('MOVE command:');
                         print_log('============');
                         print_log(g_command);
                         --
                         select xxwc_edi_iface_pkg.xxwc_oscommand_run (g_command)
                         into   l_result
                         from   dual;
                         print_log('v_result '||l_result);   
                         --
                         if (l_result is null) then 
                                 g_command := 'chmod'
                                             ||' '
                                             ||'777'
                                             ||' '                                             
                                             ||l_outbound_loc
                                             ||'/'
                                             ||l_file_name        
                                             ; 
                                 --
                                 print_log('CHMOD command:');
                                 print_log('============');
                                 print_log(g_command);
                                 --
                                 select xxwc_edi_iface_pkg.xxwc_oscommand_run (g_command)
                                 into   l_result
                                 from   dual;
                                 print_log('v_result '||l_result);   
                                 --                         
                         end if;                        
                         --       
            exception
                when others then
                 print_log('Error in renaming file: '||l_file_name_dtl_temp||' TO '||l_file_name_dtl||', message =>'||sqlerrm);
                 raise program_error;
            end;
            --        
    g_loc :='create_khalix_file.006';
    fnd_file.put_line(fnd_file.log,'@g_loc ='||g_loc||', create_khalix_file, renamed file '||l_file_name_temp||' TO '||l_file_name);  
    -- End 2.3
    fnd_file.put_line(fnd_file.log,
                      '----------------------------------------------------------------------------------');
    fnd_file.put_line(fnd_file.log, 'End of Procedure : CREATE_KHALIX_FILE');
    fnd_file.put_line(fnd_file.log,
                      '----------------------------------------------------------------------------------');
    fnd_file.put_line(fnd_file.log,
                      '**********************************************************************************');
    fnd_file.put_line(fnd_file.log,' ');

  EXCEPTION
    WHEN OTHERS THEN
      errbuf := 'Error in procedure - CREATE_KHALIX_FILE. Error - ' ||
                SQLERRM;

      xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_calling_proc_name,
                                           p_calling => l_procedure_name,
                                           p_request_id => fnd_global.conc_request_id,
                                           p_ora_error_msg => SQLERRM,
                                           p_error_desc => errbuf,
                                           p_distribution_list => pl_dflt_email,
                                           p_module => 'AP');

  END create_khalix_file;

  PROCEDURE create_payables_file(errbuf        OUT VARCHAR2
                                ,retcode       OUT NUMBER
                                ,p_period_name IN VARCHAR2) IS
    ----------------------------------------------------------------------
    -- Cursor to derive Payable Invoice Header details
    ----------------------------------------------------------------------
    CURSOR cur_ap_inv_hdr(p_period_name IN VARCHAR2) IS
      SELECT 'CORPORATE' || ' | ' || -- "BUSINESS_UNIT"
             'ORACLE-CRP' || ' | ' || -- "FINANCIAL SOURCE_SYSTEM"
             i.invoice_id || ' | ' || -- "Document Number"
             v.segment1 || '-' || vs.vendor_site_id || ' | ' ||
             i.invoice_num || ' | ' || -- "Vendor Invoice Number"
             'E' || ' | ' || -- "Invoice Type Code"
             to_char(i.invoice_date, 'MM/DD/YYYY') || ' | ' || -- "Vendor Invoice Date"
             to_char(i.creation_date, 'MM/DD/YYYY') || ' | ' || -- "Vendor Invoice Entered Date"
             to_char(i.last_update_date, 'MM/DD/YYYY') || ' | ' || -- "Invoice Last Updated Date"
             CASE
               WHEN i.cancelled_date IS NOT NULL THEN
                'CANCELLED'
               WHEN abs(i.amount_paid) > 0
                    AND (i.invoice_amount - i.amount_paid) != 0 THEN
                'PARTIAL PAID'
               WHEN i.payment_status_flag = 'Y' THEN
                'PAID'
               WHEN i.payment_status_flag = 'N' THEN
                'NOT PAID'
               WHEN xxcus_ap_ind_srcg_pkg.inv_amt_remaining(i.invoice_id) = 0 THEN
                'PAID'
             END || ' | ' || -- "Vendor Invoice Status"
             apt.term_id || ' | ' || -- "Payment Term ID"
             decode(xxcus_ap_ind_srcg_pkg.inv_min_due_date(i.invoice_id),
                    '01010001', '',
                    xxcus_ap_ind_srcg_pkg.inv_min_due_date(i.invoice_id)) ||
             ' | ' || -- "Payment Due Date"
             decode(xxcus_ap_ind_srcg_pkg.inv_payment_date(i.invoice_id),
                    '01010001', '',
                    xxcus_ap_ind_srcg_pkg.inv_payment_date(i.invoice_id)) ||
             ' | ' || -- "Payment Date"
             i.amount_paid || ' | ' || --"Payment Amount"
             i.discount_amount_taken || ' | ' || -- "Cash Discount Amount"
             xxcus_ap_ind_srcg_pkg.disc_mrc_frt_amnt(i.invoice_id) || ' | ' || -- "Inv_Misc_Disc_Chg_Adj"
             i.invoice_amount -- "Net Invoice Total"
             invoice_header
        FROM ap.ap_invoices_all    i
            ,ap_suppliers          v
            ,ap_supplier_sites_all vs
            ,ap.ap_terms_tl        apt
       WHERE 1 = 1
         AND i.vendor_id = v.vendor_id
         AND v.vendor_id = vs.vendor_id
         AND i.vendor_site_id = vs.vendor_site_id
         AND i.terms_id = apt.term_id
--         AND i.org_id = l_org_id
         AND i.org_id IN (163, 167)         -- Version# 2.1
         --AND nvl(i.attribute10, 'N') = 'N' --2.3
         AND vs.pay_group_lookup_code != l_pay_group_lookup_code
         AND i.invoice_id IN
             (SELECT pd.invoice_id
                FROM ap.ap_invoice_distributions_all pd
               WHERE pd.invoice_id = i.invoice_id
                 AND ((p_period_name IS NULL AND
                     pd.accounting_date BETWEEN l_start_date AND
                     l_end_date) OR p_period_name = pd.period_name));

    ----------------------------------------------------------------------
    -- Cursor to derive Payable Invoice Header Totals
    ----------------------------------------------------------------------
    CURSOR cur_ap_inv_hdr_ttl(p_period_name IN VARCHAR2) IS
      SELECT SUM(nvl(i.invoice_amount, 0)) "INVOICE_AMOUNT"
             ,SUM(nvl(i.amount_paid, 0)) "AMOUNT_PAID"
        FROM ap.ap_invoices_all    i
            ,ap_suppliers          v
            ,ap_supplier_sites_all vs
            ,ap.ap_terms_tl        apt
       WHERE 1 = 1
         AND i.vendor_id = v.vendor_id
         AND v.vendor_id = vs.vendor_id
         AND i.vendor_site_id = vs.vendor_site_id
         AND i.terms_id = apt.term_id
--         AND i.org_id = l_org_id
         AND i.org_id IN (163, 167)         -- Version# 2.1
         --AND nvl(i.attribute10, 'N') = 'N' --2.3
         AND vs.pay_group_lookup_code != l_pay_group_lookup_code
         AND i.invoice_id IN
             (SELECT pd.invoice_id
                FROM ap.ap_invoice_distributions_all pd
               WHERE pd.invoice_id = i.invoice_id
                 AND ((p_period_name IS NULL AND
                     pd.accounting_date BETWEEN l_start_date AND
                     l_end_date) OR p_period_name = pd.period_name));

    ----------------------------------------------------------------------
    -- Cursor to derive Payable Invoice Line and Distribution details
    ----------------------------------------------------------------------
    CURSOR cur_ap_inv_dtl(p_period_name IN VARCHAR2) IS
      SELECT DECODE(ffvl.attribute5,
	                'AH HARRIS','WHITECAP CONSTRUCTION',
					ffvl.attribute5) || ' | ' || -- "BUSINESS_UNIT"        -- Version# 2.1 , -- Version# 2.5
             -- flv.description || ' | ' || -- "BUSINESS_UNIT"     -- Version# 2.1
             'ORACLE-CRP' || ' | ' || -- "FINANCIAL SOURCE_SYSTEM"
             i.invoice_id || ' | ' || -- "DOCUMENT_NUMBER"
             pd.invoice_line_number ||
             lpad(pd.distribution_line_number, 4, '0') || ' | ' || -- "INVOICE_LINE_NUMBER"
             'E' || ' | ' || -- "INVOICE_TYPE"
             to_char(pd.accounting_date, 'MM/DD/YYYY') || ' | ' || -- "GL_DATE"
             CASE pd.line_type_lookup_code
               WHEN 'MISCELLANEOUS' THEN
                'M'
               WHEN 'ITEM' THEN
                'I'
               WHEN 'FREIGHT' THEN
                'F'
               WHEN 'TAX' THEN
                'T'
             END || ' | ' || -- "INVOICE_LINE_TYPE"
             pd.amount || ' | ' || -- "INVOICE_LINE_AMOUNT"
             to_char(i.last_update_date, 'MM/DD/YYYY') || ' | ' || -- "LAST_UPDT_DT"
             cc.segment4 || ' | ' || -- "ACCOUNT"
             i.attribute12 || ' | ' || -- "PO NUMBER"
             cc.segment2 -- "LOCATION"
             invoice_details
        FROM ap.ap_invoices_all              i
            ,ap_supplier_sites_all           vs
            ,ap.ap_invoice_distributions_all pd
            ,gl.gl_code_combinations         cc
--            ,apps.fnd_lookup_values_vl       flv                 -- Version# 2.1
            , fnd_flex_value_sets            ffvs                  -- Version# 2.1
            , fnd_flex_values_vl             ffvl                  -- Version# 2.1
       WHERE i.invoice_id = pd.invoice_id
         AND pd.dist_code_combination_id = cc.code_combination_id
         AND i.vendor_id =vs.vendor_id --2.3
         AND i.vendor_site_id = vs.vendor_site_id
--         AND i.org_id = l_org_id
         AND i.org_id IN (163, 167)         -- Version# 2.1
         AND vs.pay_group_lookup_code NOT IN(l_pay_group_lookup_code) --2.3
         --AND nvl(i.attribute10, 'N') = 'N' --2.3
--         AND cc.segment1 = flv.lookup_code                          -- Version# 2.1
--         AND flv.lookup_type = l_lookup_type                        -- Version# 2.1
           AND ffvs.flex_value_set_name  = 'XXCUS_GL_PRODUCT'         -- Version# 2.1
           AND ffvs.flex_value_set_id    = ffvl.flex_value_set_id     -- Version# 2.1
           AND ffvl.enabled_flag         = 'Y'                        -- Version# 2.1
           AND ffvl.flex_value           = cc.segment1                -- Version# 2.1
            --      and pd.period_name              = NVL(p_period_name, pd.period_name)
            --      AND (p_period_name IS NOT NULL OR pd.accounting_date BETWEEN l_start_date AND l_end_date)
         AND EXISTS (SELECT pd_s.invoice_id
                FROM ap.ap_invoice_distributions_all pd_s
               WHERE pd_s.invoice_id = i.invoice_id
                 AND ((p_period_name IS NULL AND pd_s.accounting_date BETWEEN
                     l_start_date AND l_end_date) OR
                     p_period_name = pd_s.period_name))
       ORDER BY vs.vendor_site_id DESC;

    ----------------------------------------------------------------------
    -- Cursor to derive Payable Invoice Line and Distribution details
    ----------------------------------------------------------------------
    CURSOR cur_ap_inv_dtl_ttl(p_period_name IN VARCHAR2) IS
      SELECT pd.period_name "PERIOD"
             ,cc.segment4 "GL_ACCOUNT"
             ,SUM(nvl(pd.amount, 0)) "INVOICE_LINE_AMOUNT"
        FROM ap.ap_invoices_all              i
            ,ap_supplier_sites_all           vs
            ,ap.ap_invoice_distributions_all pd
            ,gl.gl_code_combinations         cc
--            ,apps.fnd_lookup_values_vl       flv                    -- Version# 2.1
            , fnd_flex_value_sets            ffvs                     -- Version# 2.1
            , fnd_flex_values_vl             ffvl                     -- Version# 2.1
       WHERE i.invoice_id = pd.invoice_id
         AND pd.dist_code_combination_id = cc.code_combination_id
         AND i.vendor_id =vs.vendor_id --2.3         
         AND i.vendor_site_id = vs.vendor_site_id
--         AND i.org_id = l_org_id
         AND i.org_id IN (163, 167)         -- Version# 2.1
         AND vs.pay_group_lookup_code NOT IN(l_pay_group_lookup_code) --2.3
         --AND nvl(i.attribute10, 'N') = 'N' --2.3
--         AND cc.segment1 = flv.lookup_code                          -- Version# 2.1
--         AND flv.lookup_type = l_lookup_type                        -- Version# 2.1
           AND ffvs.flex_value_set_name  = 'XXCUS_GL_PRODUCT'         -- Version# 2.1
           AND ffvs.flex_value_set_id    = ffvl.flex_value_set_id     -- Version# 2.1
           AND ffvl.enabled_flag         = 'Y'                        -- Version# 2.1
           AND ffvl.flex_value           = cc.segment1                -- Version# 2.1
            --      and pd.period_name              = NVL(p_period_name, pd.period_name)
            --      AND (p_period_name IS NOT NULL OR pd.accounting_date BETWEEN l_start_date AND l_end_date)
         AND EXISTS (SELECT pd_s.invoice_id
                FROM ap.ap_invoice_distributions_all pd_s
               WHERE pd_s.invoice_id = i.invoice_id
                 AND ((p_period_name IS NULL AND pd_s.accounting_date BETWEEN
                     l_start_date AND l_end_date) OR
                     p_period_name = pd_s.period_name))
       GROUP BY cc.segment4
               ,pd.period_name
       ORDER BY pd.period_name;

    -- Intialize Variables
    l_err_msg          VARCHAR2(2000);
    l_err_code         NUMBER;
    l_sec              VARCHAR2(150);
    l_procedure_name   VARCHAR2(75) := 'xxcus_ap_ind_srcg_pkg.create_payables_file';
    l_from_date        DATE;
    l_to_date          DATE;
    l_inv_header_total NUMBER;
    l_inv_paid_total   NUMBER;
    l_inv_line_total   NUMBER;
    l_gl_account       VARCHAR2(30);
    l_period           VARCHAR2(10);
    l_errbuf           VARCHAR2(2000);
    l_retcode          NUMBER;

    --File Variables
    l_file_name_hdr      VARCHAR2(150);
    l_file_name_dtl      VARCHAR2(150);
    l_file_dir           VARCHAR2(100);
    l_file_handle_hdr    utl_file.file_type;
    l_file_handle_dtl    utl_file.file_type;
    l_file_name_hdr_temp VARCHAR2(150);
    l_file_name_dtl_temp VARCHAR2(150);

  BEGIN
    fnd_file.put_line(fnd_file.log,
                      '**********************************************************************************');
    fnd_file.put_line(fnd_file.log,
                      '----------------------------------------------------------------------------------');
    fnd_file.put_line(fnd_file.log,
                      'Start of Procedure : CREATE_PAYABLES_FILE');
    fnd_file.put_line(fnd_file.log,
                      '----------------------------------------------------------------------------------');
    fnd_file.put_line(fnd_file.log, 'Input Parameters   :');
    fnd_file.put_line(fnd_file.log,
                      'p_period_name          :' || p_period_name);
    fnd_file.put_line(fnd_file.log,
                      '----------------------------------------------------------------------------------');
    -- Begin 2.3
    g_loc :='create_payables_file.001';
    --
    l_outbound_loc :=get_location ( v_operation =>'ABSOLUTE_PATH', v_type  =>'OUTBOUND');
    --     
    --End 2.3
    l_file_name_hdr := 'CRP_APINVOICEHEADER_' ||
                       to_char(SYSDATE, 'YYYYMMDD') || '.ld';
    l_file_name_dtl := 'CRP_APINVOICELINE_' || to_char(SYSDATE, 'YYYYMMDD') ||
                       '.ld';
    g_loc :='create_payables_file.002'; --2.3
    l_file_name_hdr_temp := 'TEMP_' || l_file_name_hdr;
    l_file_name_dtl_temp := 'TEMP_' || l_file_name_dtl;
    --    
    -- Begin 2.3
    g_loc :='create_payables_file.003'; 
    fnd_file.put_line(fnd_file.log,' ');    
    fnd_file.put_line(fnd_file.log,'@g_loc ='||g_loc||', l_file_name_hdr_temp =>'||l_file_name_hdr_temp);    
    fnd_file.put_line(fnd_file.log,'@g_loc ='||g_loc||', l_file_name_dtl_temp =>'||l_file_name_dtl_temp);  
    fnd_file.put_line(fnd_file.log,' ');      
    -- End 2.3
    -- Delete the Temp and Data Files before generating the new files - have a flag(Free Text) for this

    -->Header Start
    -- Start creating the Invoice Header file
    l_file_handle_hdr := utl_file.fopen('ORACLE_INT_UC4_APSOURCING',
                                        l_file_name_hdr_temp, 'w', l_utl_max_linesize); --2.3
    -- Begin 2.3
    g_loc :='create_payables_file.004'; 
    fnd_file.put_line(fnd_file.log,'@g_loc ='||g_loc||', l_file_handle_hdr initialized ');  
    -- End 2.3          
    FOR rec_ap_inv_hdr IN cur_ap_inv_hdr(p_period_name)
    LOOP

      -- Write header info
      utl_file.put_line(l_file_handle_hdr, rec_ap_inv_hdr.invoice_header);
    END LOOP;
    -- Begin 2.3
    g_loc :='create_payables_file.005'; 
    fnd_file.put_line(fnd_file.log,'@g_loc ='||g_loc||', after copying all lines to file '||l_file_name_hdr_temp);  
    -- End 2.3
    -- Close the file
    utl_file.fclose(l_file_handle_hdr);
    -- Begin 2.3
    g_loc :='create_payables_file.006'; 
    fnd_file.put_line(fnd_file.log,'@g_loc ='||g_loc||', after file close of l_file_name_hdr_temp = '||l_file_name_hdr_temp);  
    -- End 2.3
    -- 'Rename file for pickup';
    -- Begin 2.3    
    g_loc :='create_payables_file.007.a'; 
    fnd_file.put_line(fnd_file.log,'@g_loc ='||g_loc||', before rename of file  '||l_file_name_hdr_temp||' TO '||l_file_name_hdr);      
    --utl_file.frename('ORACLE_INT_UC4_APSOURCING', l_file_name_hdr_temp,'ORACLE_INT_UC4_APSOURCING', l_file_name_hdr, TRUE); 
            begin
                        --
                         g_command := 'mv '
                                     ||' '
                                     ||l_outbound_loc
                                     ||'/'
                                     ||l_file_name_hdr_temp
                                     ||' '
                                     ||l_outbound_loc
                                     ||'/'
                                     ||l_file_name_hdr              
                                     ; 
                         --
                         print_log('MOVE command:');
                         print_log('============');
                         print_log(g_command);
                         --
                         select xxwc_edi_iface_pkg.xxwc_oscommand_run (g_command)
                         into   l_result
                         from   dual;
                         print_log('v_result '||l_result);   
                         --
                         if (l_result is null) then 
                                 g_command := 'chmod'
                                             ||' '
                                             ||'777'
                                             ||' '                                             
                                             ||l_outbound_loc
                                             ||'/'
                                             ||l_file_name_hdr            
                                             ; 
                                 --
                                 print_log('CHMOD command:');
                                 print_log('============');
                                 print_log(g_command);
                                 --
                                 select xxwc_edi_iface_pkg.xxwc_oscommand_run (g_command)
                                 into   l_result
                                 from   dual;
                                 print_log('v_result '||l_result);   
                                 --                         
                         end if;                        
                         --  
            exception
                when others then
                 print_log('Error in renaming file: '||l_file_name_dtl_temp||' TO '||l_file_name_dtl||', message =>'||sqlerrm);
                 raise program_error;
            end;
            --                          
    g_loc :='create_payables_file.007.b'; 
    fnd_file.put_line(fnd_file.log,'@g_loc ='||g_loc||', after rename of file  '||l_file_name_hdr_temp||' TO '||l_file_name_hdr);  
    l_file_handle_dtl := utl_file.fopen('ORACLE_INT_UC4_APSOURCING',l_file_name_dtl_temp, 'w',l_utl_max_linesize);       
    -- End 2.3
    --
    FOR rec_ap_inv_dtl IN cur_ap_inv_dtl(p_period_name)
    LOOP
      -- Write line info
      utl_file.put_line(l_file_handle_dtl, rec_ap_inv_dtl.invoice_details);
    END LOOP;
    --  
    -- Close the file
    utl_file.fclose(l_file_handle_dtl);
    --
    --utl_file.frename('ORACLE_INT_UC4_APSOURCING', l_file_name_dtl_temp,'ORACLE_INT_UC4_APSOURCING', l_file_name_dtl);     
    -- 'Rename file for pickup';   
            begin
                        --
                         g_command := 'mv '
                                     ||' '
                                     ||l_outbound_loc
                                     ||'/'
                                     ||l_file_name_dtl_temp
                                     ||' '
                                     ||l_outbound_loc
                                     ||'/'
                                     ||l_file_name_dtl              
                                     ; 
                         --
                         print_log('MOVE command:');
                         print_log('============');
                         print_log(g_command);
                         --
                         select xxwc_edi_iface_pkg.xxwc_oscommand_run (g_command)
                         into   l_result
                         from   dual;
                         print_log('v_result '||l_result);   
                         --
                         if (l_result is null) then 
                                 g_command := 'chmod'
                                             ||' '
                                             ||'777'
                                             ||' '                                             
                                             ||l_outbound_loc
                                             ||'/'
                                             ||l_file_name_dtl            
                                             ; 
                                 --
                                 print_log('CHMOD command:');
                                 print_log('============');
                                 print_log(g_command);
                                 --
                                 select xxwc_edi_iface_pkg.xxwc_oscommand_run (g_command)
                                 into   l_result
                                 from   dual;
                                 print_log('v_result '||l_result);   
                                 --                         
                         end if;                        
                         --  
            exception
                when others then
                 print_log('Error in renaming file: '||l_file_name_dtl_temp||' TO '||l_file_name_dtl||', message =>'||sqlerrm);
                 raise program_error;
            end;
            --                 
    --> Header Total
    FOR rec_ap_inv_hdr_ttl IN cur_ap_inv_hdr_ttl(p_period_name)
    LOOP
      l_inv_paid_total   := rec_ap_inv_hdr_ttl.amount_paid;
      l_inv_header_total := rec_ap_inv_hdr_ttl.invoice_amount;
    END LOOP;

    fnd_file.put_line(fnd_file.output,
                      '**********************************************************************************');
    fnd_file.put_line(fnd_file.output,
                      '----------------------------------------------------------------------------------');
    fnd_file.put_line(fnd_file.output, '*** Input Parameters ***');
    fnd_file.put_line(fnd_file.output,
                      'Period Name                         :' ||
                       p_period_name);
    fnd_file.put_line(fnd_file.output,
                      '----------------------------------------------------------------------------------');
    fnd_file.put_line(fnd_file.output,
                      '----------------------------------------------------------------------------------');
    fnd_file.put_line(fnd_file.output,
                      '*** Invoice Header Output Totals ***');
    fnd_file.put_line(fnd_file.output,
                      'Total Inovice Amount                :' ||
                       l_inv_header_total);
    fnd_file.put_line(fnd_file.output,
                      'Total Paid Amount Total             :' ||
                       l_inv_paid_total);
    fnd_file.put_line(fnd_file.output,
                      '----------------------------------------------------------------------------------');
   fnd_file.put_line(fnd_file.output,
                      '----------------------------------------------------------------------------------');
    fnd_file.put_line(fnd_file.output,
                      '*** Distribution Totals Per Account ***');
    fnd_file.put_line(fnd_file.output,
                      '----------------------------------------------------------------------------------');
    fnd_file.put_line(fnd_file.output,
                      'Period              GL Account                    Invoice Total');
    fnd_file.put_line(fnd_file.output,
                      '----------------------------------------------------------------------------------');

    --> Line Total
    FOR rec_ap_inv_dtl_ttl IN cur_ap_inv_dtl_ttl(p_period_name)
    LOOP
      l_period         := rec_ap_inv_dtl_ttl.period;
      l_gl_account     := rec_ap_inv_dtl_ttl.gl_account;
      l_inv_line_total := rec_ap_inv_dtl_ttl.invoice_line_amount;
      fnd_file.put_line(fnd_file.output,
                        rpad(l_period, 20, ' ') ||
                         rpad(l_gl_account, 30, ' ') || l_inv_line_total);
    END LOOP;     
    --                 
    COMMIT;
    --
    fnd_file.put_line(fnd_file.output,
                      '----------------------------------------------------------------------------------');
    fnd_file.put_line(fnd_file.output,
                      '**********************************************************************************');

    fnd_file.put_line(fnd_file.log,
                      '----------------------------------------------------------------------------------');
    fnd_file.put_line(fnd_file.log,
                      'End of Procedure : CREATE_PAYABLES_FILE');
    fnd_file.put_line(fnd_file.log,
                      '----------------------------------------------------------------------------------');

    --> Create Payment Terms File
    create_payment_terms_file(l_errbuf, l_retcode);

    --> Create Pay To Vendor File
    create_pay_to_vendor_file(l_errbuf, l_retcode);

    --> Create Khalix File
    create_khalix_file(l_errbuf, l_retcode);

  EXCEPTION
    -- this section traps my errors
    WHEN OTHERS THEN
      l_err_code := 2;
      l_err_msg  := l_sec || l_err_msg || ' ERROR ' || SQLCODE ||
                    substr(SQLERRM, 1, 2000);

      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);

      utl_file.fclose(l_file_handle_hdr);
      utl_file.fremove(l_file_dir, l_file_name_hdr);

      utl_file.fclose(l_file_handle_dtl);
      utl_file.fremove(l_file_dir, l_file_name_dtl);

      xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_procedure_name,
                                           p_calling => l_sec,
                                           p_request_id => fnd_global.conc_request_id,
                                           p_ora_error_msg => SQLERRM,
                                           p_error_desc => 'Error running Direct Sourcing package with Program Error Exception',
                                           p_distribution_list => pl_dflt_email,
                                           p_module => 'AP');
  END create_payables_file;

-- Version# 2.0 > Start
 /********************************************************************************
  ProcedureName : UC4_CALL
  Purpose       : API which is called from UC4 job. It submits the Concurrent
                  Program - "XXCUS AP Indirect Sourcing Data Outbound Interface"

  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     06/24/2013    Gopi Damuluri    Initial version.
  ********************************************************************************/
  PROCEDURE uc4_call(p_errbuf              OUT VARCHAR2
                    ,p_retcode             OUT NUMBER
                    ,p_conc_prg_name       IN VARCHAR2
                    ,p_user_name           IN VARCHAR2
                    ,p_responsibility_name IN VARCHAR2) IS

    --
    -- Package Variables
    --
    l_package VARCHAR2(50) := 'XXCUS_AP_IND_SRCG_PKG';
    l_email   fnd_user.email_address%TYPE;

    l_req_id              NUMBER NULL;
    v_phase               VARCHAR2(50);
    v_status              VARCHAR2(50);
    v_dev_status          VARCHAR2(50);
    v_dev_phase           VARCHAR2(50);
    v_message             VARCHAR2(250);
    v_error_message       VARCHAR2(3000);
    v_supplier_id         NUMBER;
    v_rec_cnt             NUMBER := 0;
    l_message             VARCHAR2(150);
    l_errormessage        VARCHAR2(3000);
    pl_errorstatus        NUMBER;
    l_can_submit_request  BOOLEAN := TRUE;
    l_globalset           VARCHAR2(100);
    l_err_msg             VARCHAR2(3000);
    l_err_code            NUMBER;
    l_sec                 VARCHAR2(255);
    l_statement           VARCHAR2(9000);
    l_user                fnd_user.user_id%TYPE;
    l_user_id             NUMBER;
    l_responsibility_id   NUMBER;
    l_resp_application_id NUMBER;
    l_rej_rec_cnt         NUMBER;
    l_totl_rec_cnt        NUMBER;
    l_rej_percent         NUMBER;
    l_prc_inv_amt         NUMBER;
    l_prc_tax_amt         NUMBER;
    l_prc_frt_amt         NUMBER;
    l_rej_inv_amt         NUMBER;
    l_rej_tax_amt         NUMBER;
    l_rej_frt_amt         NUMBER;
    l_totl_inv_amt        NUMBER;
    l_totl_tax_amt        NUMBER;
    l_totl_frt_amt        NUMBER;
    l_conc_prg_arg1       VARCHAR2(900);

    -- Error DEBUG
    l_err_callfrom  VARCHAR2(75) DEFAULT 'XXCUS_AP_IND_SRCG_PKG.UC4_CALL';
    l_err_callpoint VARCHAR2(75) DEFAULT 'START';
  BEGIN
    p_retcode := 0;
    --------------------------------------------------------------------------
    -- Deriving UserId
    --------------------------------------------------------------------------
    BEGIN
      SELECT user_id
        INTO l_user_id
        FROM fnd_user
       WHERE 1 = 1
         AND user_name = upper(p_user_name)
         AND SYSDATE BETWEEN start_date AND
             nvl(end_date, trunc(SYSDATE) + 1);
    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := 'UserName - ' || p_user_name ||
                     ' not defined in Oracle';
        RAISE program_error;
      WHEN OTHERS THEN
        l_err_msg := 'Error deriving user_id for UserName - ' ||
                     p_user_name;
        RAISE program_error;
    END;

    --------------------------------------------------------------------------
    -- Deriving ResponsibilityId and ResponsibilityApplicationId
    --------------------------------------------------------------------------
    BEGIN
      SELECT responsibility_id
            ,application_id
        INTO l_responsibility_id
            ,l_resp_application_id
        FROM fnd_responsibility_vl
       WHERE responsibility_name = p_responsibility_name
         AND SYSDATE BETWEEN start_date AND
             nvl(end_date, trunc(SYSDATE) + 1);
    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := 'Responsibility - ' || p_responsibility_name ||
                     ' not defined in Oracle';
        RAISE program_error;
      WHEN OTHERS THEN
        l_err_msg := 'Error deriving Responsibility_id for ResponsibilityName - ' ||
                     p_responsibility_name;
        RAISE program_error;
    END;

    fnd_file.put_line(fnd_file.log,
                      'Concurrent Program Name - ' || p_conc_prg_name);
    l_sec           := 'UC4 call to run concurrent request - XXCUS AP Indirect Sourcing Data Outbound Interface';
    l_conc_prg_arg1 := XXWC_AP_IND_SRCG_PKG.get_curr_period;

    --------------------------------------------------------------------------
    -- Apps Initialize
    --------------------------------------------------------------------------
    fnd_global.apps_initialize(l_user_id, l_responsibility_id,
                               l_resp_application_id);

    l_err_callpoint := 'Before submitting Concurrent request';
    --------------------------------------------------------------------------
    -- Submit "XXCUS AP Indirect Sourcing Data Outbound Interface" Program
    --------------------------------------------------------------------------
    l_req_id        := fnd_request.submit_request(application => 'XXCUS',
                                                  program => p_conc_prg_name,
                                                  description => NULL,
                                                  start_time => SYSDATE,
                                                  sub_request => FALSE,
                                                  -- argument1 => l_conc_prg_arg1 -- Version# 2.1
                                                  argument1 => NULL
                                                  );
    l_err_callpoint := 'After submitting Concurrent request';

    COMMIT;

    dbms_output.put_line('After fnd_request');
    IF (l_req_id != 0) THEN
      IF fnd_concurrent.wait_for_request(l_req_id, 30, 15000, v_phase,
                                         v_status, v_dev_phase, v_dev_status,
                                         v_message) THEN
        v_error_message := chr(10) || 'ReqID=' || l_req_id || ' DPhase ' ||
                           v_dev_phase || ' DStatus ' || v_dev_status ||
                           chr(10) || ' MSG - ' || v_message;
        -- Error Returned
        IF v_dev_phase != 'COMPLETE'
           OR v_dev_status != 'NORMAL' THEN
          l_statement := 'An error occured running the XXCUS_AP_IND_SRCG_PKG ' ||
                         v_error_message || '.';
          fnd_file.put_line(fnd_file.log, l_statement);
          fnd_file.put_line(fnd_file.output, l_statement);
          RAISE program_error;
        END IF;
        -- Then Success!
      ELSE
        l_statement := 'An error occured running the XXCUS_AP_IND_SRCG_PKG ' ||
                       v_error_message || '.';
        fnd_file.put_line(fnd_file.log, l_statement);
        fnd_file.put_line(fnd_file.output, l_statement);
        RAISE program_error;
      END IF;

    ELSE
      l_statement := 'An error occured running the XXCUS_AP_IND_SRCG_PKG ';
      fnd_file.put_line(fnd_file.log, l_statement);
      fnd_file.put_line(fnd_file.output, l_statement);
      RAISE program_error;
    END IF;

    dbms_output.put_line(l_sec);
    dbms_output.put_line('Request ID:  ' || l_req_id);
  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;
      l_err_code := 2;
      l_err_msg  := l_sec;
      l_err_msg  := l_err_msg || ' ERROR ' || substr(SQLERRM, 1, 2000);
      dbms_output.put_line(l_err_msg);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      --        l_err_callpoint := l_message;

      xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_err_callfrom,
                                           p_calling => l_err_callpoint,
                                           p_request_id => l_req_id,
                                           p_ora_error_msg => SQLERRM,
                                           p_error_desc => 'Error running XXCUS_AP_IND_SRCG_PKG package with PROGRAM ERROR',
                                           p_distribution_list => pl_dflt_email,
                                           p_module => 'AR');

      p_retcode := 2;
    WHEN OTHERS THEN
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := l_sec;
      l_err_msg  := l_sec || l_err_msg || ' ERROR ' || SQLCODE ||
                    substr(SQLERRM, 1, 2000);
      dbms_output.put_line(l_err_msg);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      --        l_err_callpoint := l_message;

      p_retcode := 2;

      xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_err_callfrom,
                                           p_calling => l_err_callpoint,
                                           p_request_id => l_req_id,
                                           p_ora_error_msg => substr(l_err_msg,
                                                                      1, 2000),
                                           p_error_desc => 'Error running XXCUS_AP_IND_SRCG_PKG package with OTHERS Exception',
                                           p_distribution_list => pl_dflt_email,
                                           p_module => 'AR');
  END uc4_call;
-- Version# 2.0 < End

END XXCUS_AP_IND_SRCG_PKG;
/