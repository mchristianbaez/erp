CREATE OR REPLACE PACKAGE BODY APPS.XXCUS_OZF_SUBMIT_CLAIMS_PKG as
/*
 -- ESMS ticket 229451
 -- Date:14-NOV-2013
 -- Author: Balaguru Seshadri
-- Scope: This package will enhance HD Supply to run the HDS Rebates Create Invoices Job by Agreement 
  --    REVISIONS:
  --    ESMS/RFC        Date        Author           Description
  --    ---------       ----------  ---------------  -------------------------------
  --    240298 /39944   04/02/2014  Balaguru Seshadri Modify main routine to include calendar year.
  -------------------------------------------------------------------------------
*/

  procedure print_log(p_message in varchar2) is
  begin 
   if fnd_global.conc_request_id >0 then
    fnd_file.put_line(fnd_file.log, p_message);
   else
    dbms_output.put_line(p_message);
   end if;
  exception
   when others then
    fnd_file.put_line(fnd_file.log, 'Issue in print_log routine ='||sqlerrm);
  end print_log;
  
  function get_field (v_delimiter in varchar2, n_field_no in number ,v_line_read in varchar2) return varchar2 is
       n_start_field_pos number;
       n_end_field_pos   number;
       v_get_field       varchar2(2000);
  begin
       if n_field_no = 1 then
          n_start_field_pos := 1;
       else
          n_start_field_pos := instr(v_line_read,v_delimiter,1,n_field_no-1)+1;
       end if;
      
       n_end_field_pos   := instr(v_line_read,v_delimiter,1,n_field_no) -1;
       if n_end_field_pos > 0 then
          v_get_field := substr(v_line_read,n_start_field_pos,(n_end_field_pos - n_start_field_pos)+1);
       else
          v_get_field := substr(v_line_read,n_start_field_pos); 
       end if;

       return v_get_field;

  exception
    when others then
     print_log ('Line ='||v_line_read);
     print_log ('get field: '||sqlerrm);
  end get_field;  

  procedure main (
    retcode               out number
   ,errbuf                out varchar2
   ,p_territory           in  varchar2
   ,p_cal_year            in  varchar2   
   ,p_frequency           in  varchar2
   ,p_agreement           in  varchar2 default Null
   ,p_amount              in  number
  ) is
    
    type lc_refcursor is ref cursor;
    my_offers             lc_refcursor; 
    REBATES_SQL           varchar2(20000); 
    
   cursor hds_agreements is
    select to_char(null)   offer_code 
          ,to_number(null) list_header_id
          ,to_char(null)   frequency
          ,to_char(null)   global_flag
          ,to_number(null) org_id
          ,to_char(null)   cal_year
    from   dual;   
    
    type my_offer_tbl is table of hds_agreements%rowtype index by binary_integer;
    my_offer_rec my_offer_tbl;          
  
   lc_request_data VARCHAR2(20) :='';
   ln_request_id   NUMBER :=0;
   n_req_id NUMBER :=0;
   l_org_id NUMBER :=0; 
   n_count_child_requests NUMBER :=0; 
   N_conc_pgm_id NUMBER :=0;
   N_running_count NUMBER :=0;
   b_keep_going BOOLEAN;
   n_program_app_id NUMBER :=0;
   N_user_id NUMBER :=0;
   v_email       fnd_user.email_address%type :=Null;
   v_cp_long_name   fnd_concurrent_programs_tl.user_concurrent_program_name%type :=Null; 
   v_cp_phase       Varchar2(40) :=Null;
   v_cp_status      Varchar2(40) :=Null; 
   v_child_requests Varchar2(240) :=Null;
   output_file_id   UTL_FILE.FILE_TYPE;
   v_path varchar2(80);
   p_period_id NUMBER :=0;
   n_high_priority NUMBER :=40;
   n_low_priority NUMBER :=50;
   
  begin  
  
   lc_request_data :=fnd_conc_global.request_data;
   
   n_req_id :=fnd_global.conc_request_id;
 
   if lc_request_data is null then     
       
       lc_request_data :='1';
  
       --l_org_id :=mo_global.get_current_org_id;    
       
       N_user_id :=fnd_global.user_id;         
       
       print_log('');
       print_log('Parameters: ');
       print_log('===========');              
       print_log('p_territory ='||p_territory);
       print_log('p_cal_year ='||p_cal_year);
       print_log('p_frequency ='||p_frequency);
       print_log('p_agreement [offer_code-list_header_id-frequency-global_flag-org_id ] ='||p_agreement);
       print_log(''); 
       
      Begin 
       -- 
       if p_agreement is null then
       --
         if p_territory ='US' then
            l_org_id :=101;
         elsif p_territory ='CA' then
            l_org_id :=102;        
         else
            l_org_id :=Null;
         end if;
       --
           REBATES_SQL :='Select distinct oo.offer_code  offer_code
                                ,qlhv.list_header_id     list_header_id
                                ,qlhv.attribute6         frequency
                                ,qlhv.global_flag        global_flag
                                ,oo.org_id               org_id
                                ,qlhv.attribute7         cal_year
                          From qp_list_headers_vl qlhv, ozf.ozf_offers oo 
                          Where 1 = 1
                            And oo.status_code       =''ACTIVE''
                            And oo.autopay_flag      =''Y''
                            And oo.qp_list_header_id =qlhv.list_header_id'||
                           ' And qlhv.attribute7 ='||''''||p_cal_year||''''||                           
                           ' And qlhv.attribute6 ='||''''||p_frequency||'''';
       -- 
             REBATES_SQL :=REBATES_SQL||
                           ' '||
                           'And qlhv.global_flag = ''N''';
             REBATES_SQL :=REBATES_SQL||
                           ' '||
                           'And org_id ='||''''||l_org_id||'''';
             /*              
             REBATES_SQL :=REBATES_SQL||
                           ' '||
                           ' And oo.offer_code IN (''AMER07130058FLA'',''AMER08130107FLA'')';
             */                              
        --
        --
         Begin 
          --
          Open my_offers FOR REBATES_SQL;
          Fetch my_offers Bulk Collect Into my_offer_rec;
          Close my_offers;
          --
          b_keep_going :=TRUE;
          --       
         Exception
          when others then
            b_keep_going :=FALSE;         
         End;   
         -- 
        print_log(''); 
         if p_territory ='US' then
           print_log('Rebates SQL [US Non Global]:');
           print_log('============================');            
         else
           print_log('Rebates SQL [Canada Non Global]:');
           print_log('================================');           
         end if;
         print_log(REBATES_SQL);
         print_log('');
         print_log('@All non global agreements, total offers: '||my_offer_rec.count); 
         print_log('');                 
         --                                         
       else
        --
        -- User wanted to run the claim process for a specific agreement.
        -- Just set the plsql table my_offer_rec with appropriate values and let the program flow through the logic
        --
        my_offer_rec(1).offer_code      :=get_field ('-', 1, p_agreement);       
        my_offer_rec(1).list_header_id  :=To_Number (get_field('-', 2, p_agreement));
        my_offer_rec(1).frequency       :=get_field ('-', 3, p_agreement);        
        my_offer_rec(1).global_flag     :=get_field ('-', 4, p_agreement);
        my_offer_rec(1).cal_year        :=get_field ('-', 5, p_agreement);        
        --
        if my_offer_rec(1).global_flag ='Y' then
         --
         my_offer_rec(1).org_id :=101; --Always run global offers with org_id 101
         --
        else
         --
         my_offer_rec(1).org_id :=To_Number (get_field('-', 5, p_agreement));
         --
        end if;
        -- 
        b_keep_going :=TRUE;
        --  
        print_log('');
        print_log('Offer_code ='||my_offer_rec(1).offer_code);
        print_log('List_header_id ='||my_offer_rec(1).list_header_id);        
        print_log('Frequency ='||my_offer_rec(1).frequency);        
        print_log('Calendar Year ='||my_offer_rec(1).cal_year);        
        print_log('Global_Flag ='||my_offer_rec(1).global_flag);        
        print_log('Org_id ='||my_offer_rec(1).org_id);        
        print_log('');
        print_log('@Non global specific agreement, total offers: '||my_offer_rec.count);                   
        print_log('');        
        --                             
       end if;
       --
      Exception
       When Others Then
       --
        print_log('@100, main: message ='||sqlerrm);
        b_keep_going :=FALSE;
       --
      End;     
      -- 
      if (b_keep_going) then
       --
          If my_offer_rec.count =0 Then
          
           print_log('No active offers for territory ='||p_territory||
                     ', frequency ='||p_frequency                       
                    );
           
          Else --my_offer_rec.count >0 
           --print_log('Total offers fetched ='||my_offer_rec.count);
           --  
           --  Update fnd_concurrent_programs table request_priority with value 90 as this will ensure the standard offers are processed before global ones
           --
           if (p_agreement is null) then
            --
            if p_territory ='US' then 
            --
             Begin 
              Update fnd_concurrent_programs
                 set request_priority =n_high_priority
               where 1 =1
                 and application_id =(select application_id from fnd_application where application_short_name ='XXCUS')
                 and concurrent_program_name ='XXCUSOZF_REB_INV';
             Exception
              When Others Then
               rollback;
               print_log('Error in updating request priority 90, message ='||sqlerrm);
             End;
            --            
            else
             -- for canada make sure we set it to default 50
             Begin 
              Update fnd_concurrent_programs
                 set request_priority =n_low_priority
               where 1 =1
                 and application_id =(select application_id from fnd_application where application_short_name ='XXCUS')
                 and concurrent_program_name ='XXCUSOZF_REB_INV';
             Exception
              When Others Then
               rollback;
               print_log('Error in updating request priority 50, message ='||sqlerrm);
             End;
            --              
             --            
            end if;
           else
            -- if the agreement number is not null then we don't worry about the order of execution as we are running claims for only one offer
             Null;
            --
           end if;
           --    
           for indx in 1 .. my_offer_rec.count loop                                   
            --                                   
            ln_request_id :=fnd_request.submit_request
                  (
                   application      =>'XXCUS',
                   program          =>'XXCUSOZF_REB_INV',
                   description      =>'',
                   start_time       =>'',
                   sub_request      =>TRUE,
                   argument1        =>TO_CHAR(my_offer_rec(indx).org_id),
                   argument2        =>'',
                   argument3        =>to_char(my_offer_rec(indx).list_header_id),
                   argument4        =>p_frequency,                       
                   argument5        =>'',
                   argument6        =>To_Char(p_amount),
                   argument7        =>'INV',
                   argument8        =>p_cal_year                                                                                                                                                                                                                                                                                                     
                  ); 
                          
              if ln_request_id >0 then 
                print_log('Submitted HDS Rebates: Create Claims for offer_code ='
                                                 ||my_offer_rec(indx).offer_code
                                                 ||', Request Id ='
                                                 ||ln_request_id
                         );
              else
                print_log('Failed to submit HDS Rebates: Create Claims for offer_code ='
                                                 ||my_offer_rec(indx).offer_code                                                    
                         );
              end if;     
                       
             fnd_conc_global.set_req_globals(conc_status =>'PAUSED' ,request_data =>lc_request_data);
               
             lc_request_data :=to_char(to_number(lc_request_data)+1);      
           end loop;            
           --
           -- if the agreement name is null and territory is US then submit claims job for global offers.
           --
           if (p_agreement is null and p_territory ='US') then
            --
           -- Update fnd_concurrent_programs table request_priority with value back to default [50] as this will ensure the global offers are processed
           -- after the standard US ones are done.
           --
             Begin 
              Update fnd_concurrent_programs
                 set request_priority =n_low_priority
               where 1 =1
                 and application_id =(select application_id from fnd_application where application_short_name ='XXCUS')
                 and concurrent_program_name ='XXCUSOZF_REB_INV';
             Exception
              When Others Then
               rollback;
               print_log('Error in updating request priority back to 50, message ='||sqlerrm);
             End;
           --            
            begin 
             -- clean up the plsql table bcoz we have standard offer rows populated.
               if my_offer_rec.count >0 then
                my_offer_rec.delete;             
               end if;
               --
                   REBATES_SQL :='Select distinct oo.offer_code  offer_code
                                        ,qlhv.list_header_id     list_header_id
                                        ,qlhv.attribute6         frequency
                                        ,qlhv.global_flag        global_flag
                                        ,oo.org_id               org_id
                                        ,qlhv.attribute7         cal_year
                                  From qp_list_headers_vl qlhv, ozf.ozf_offers oo 
                                  Where 1 = 1
                                    And oo.status_code       =''ACTIVE''
                                    And oo.autopay_flag      =''Y''
                                    And oo.qp_list_header_id =qlhv.list_header_id'||
                                   ' And qlhv.attribute7 ='||''''||p_cal_year||''''||                                    
                                   ' And qlhv.attribute6 ='||''''||p_frequency||'''';
               -- 
                   REBATES_SQL :=REBATES_SQL||
                                   ' '||
                                   'And qlhv.global_flag = ''Y''';                            
                --
                --                       
                print_log('');
                print_log('Rebates SQL [Global]:');
                print_log('========================');
                print_log(REBATES_SQL);
                print_log('');
             --
             -- Exec SQL to fetch all global offers and kickoff the claims process under org 101 [US]
                 --
                 Begin
                  --
                  l_org_id :=101; --Run global offers under US org  
                  --
                  Open my_offers FOR REBATES_SQL;
                  Fetch my_offers Bulk Collect Into my_offer_rec;
                  Close my_offers;
                  --
                  print_log('');
                  print_log('@All global agreements, total offers: '||my_offer_rec.count); 
                  print_log('');
                  --                  
                  b_keep_going :=TRUE;
                  --       
                 Exception
                  when others then
                  print_log('');
                  print_log('@Global agreements, error message ='||sqlerrm); 
                  print_log('');                  
                    b_keep_going :=FALSE;         
                 End;              
                 --
                 if (b_keep_going) then
                      -- no errors so far 
                      if my_offer_rec.count >0 then 
                       -- we have some global offers ready for kick off
                           for indx1 in 1 .. my_offer_rec.count loop                                   
                            --                                   
                            ln_request_id :=fnd_request.submit_request
                                  (
                                   application      =>'XXCUS',
                                   program          =>'XXCUSOZF_REB_INV',
                                   description      =>'Global ',
                                   start_time       =>'',
                                   sub_request      =>TRUE,
                                   argument1        =>To_Char(l_org_id),
                                   argument2        =>'',
                                   argument3        =>to_char(my_offer_rec(indx1).list_header_id),
                                   argument4        =>p_frequency,                       
                                   argument5        =>'',
                                   argument6        =>To_Char(p_amount),
                                   argument7        =>'INV',
                                   argument8        =>p_cal_year
                                  ); 
                                          
                              if ln_request_id >0 then 
                                print_log('Submitted HDS Rebates: Create Claims for global offer_code ='
                                                                 ||my_offer_rec(indx1).offer_code
                                                                 ||', Request Id ='
                                                                 ||ln_request_id
                                         );
                              else
                                print_log('Failed to submit HDS Rebates: Create Claims for global offer_code ='
                                                                 ||my_offer_rec(indx1).offer_code                                                    
                                         );
                              end if;
                                       
                             fnd_conc_global.set_req_globals(conc_status =>'PAUSED' ,request_data =>lc_request_data);
                               
                             lc_request_data :=to_char(to_number(lc_request_data)+1);      
                           end loop;               
                       --
                      end if;
                      --
                 end if;
                 --
            exception
             --
             when others then
              print_log('Error in delete of plsql collection table my_offer_rec, message ='||sqlerrm);
             -- 
            end;
            --
           end if;
           --
          End If; -- checking if my_offer_rec.count =0
       --      
      else
       print_log('Variable b_keep_going is set to FALSE, message ='||sqlerrm);
      end if;
      --                                   
   else     
    --           
      retcode :=0;
      errbuf :='Child request[s] completed. Exit HDS Rebates: Submit Claims Master';
      print_log(errbuf);
    --
   end if;
  exception
   when others then
    print_log('Error in caller Apps.XXCUS_OZF_SUBMIT_CLAIMS_PKG.Main ='||sqlerrm);
    rollback;
  end main;
  
end XXCUS_OZF_SUBMIT_CLAIMS_PKG;
/
