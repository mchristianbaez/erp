CREATE OR REPLACE PACKAGE BODY apps.xxwcar_taxware_pkg IS

  /**************************************************************************
  File Name: XXWCAR_TAXWARE_PKG
  
  PROGRAM TYPE: SQL Script
  
  PURPOSE:      package Specification is used to load invoice transaction
                data to capture the original customer attributes for tax
                audit.  Concurrent request 'WC AR TAXWARE AUDIT LOAD' will
                be run daily at 9:00 am to load transactions
  
  HISTORY
  =============================================================================
         Last Update Date : 07/17/2012
  =============================================================================
  =============================================================================
  VERSION DATE          AUTHOR(S)          DESCRIPTION
  ------- -----------   -----------------  ------------------------------------
  1.0     17-Jul-2012   Kathy Poling       Initial creation of the procedure
                                           Service Ticket 151718 RFC 34124
  1.1     10-Aug-2012   Kathy Poling       Service Ticket 160659 Enhancement
                                           adding columns for DocLink 
  1.2     27-Mar-2013   Kathy Poling       Service Ticket 194888 changing the batch name
                                           to the grouping rule for Order Management
  1.3     25-Sep-2014   Maharajan Shunmugam TMS#20141001-00155  Canada multi org changes 
                                                                                                                   
  =============================================================================
  *****************************************************************************/
  l_distro_list  VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';

  PROCEDURE load_audit(errbuf OUT VARCHAR2, retcode OUT NUMBER) IS
  
    l_todate             DATE;
    l_fromdate           DATE;
    l_count              NUMBER := 0;
    l_req_id             NUMBER := fnd_global.conc_request_id;
    l_sec                VARCHAR2(255);
 --   l_org CONSTANT hr_all_organization_units.organization_id%TYPE := 162; --HDS White Cap - Org --commented and added below for ver 1.3
    l_org CONSTANT hr_all_organization_units.organization_id%TYPE := MO_GLOBAL.GET_CURRENT_ORG_ID;
    l_grouping_rule      NUMBER := 1001;   --ORDER MANAGEMENT           --Version 1.2 
    l_procedure_name VARCHAR2(75) := 'XXWCAR_TAXWARE_PKG.LOAD_AUDIT';
  
  BEGIN
    mo_global.set_policy_context('S',l_org);                        --Added for version 1.3

    BEGIN
    
      SELECT trunc(SYSDATE - description)
        INTO l_fromdate
        FROM apps.fnd_lookup_values
       WHERE lookup_type = 'XXWCAR_TAXWARE_PKG'
         AND lookup_code = 'FDATE'
         AND enabled_flag = 'Y'
         AND nvl(end_date_active, SYSDATE) >= SYSDATE;
    
    EXCEPTION
      WHEN no_data_found THEN
        l_fromdate := trunc(SYSDATE - 2);
      
    END;
  
    BEGIN
    
      SELECT trunc(SYSDATE + description)
        INTO l_todate
        FROM apps.fnd_lookup_values
       WHERE lookup_type = 'XXWCAR_TAXWARE_PKG'
         AND lookup_code = 'TDATE'
         AND enabled_flag = 'Y'
         AND nvl(end_date_active, SYSDATE) >= SYSDATE;
    
    EXCEPTION
      WHEN no_data_found THEN
        l_todate := trunc(SYSDATE + 1);
      
    END;
  
    BEGIN
    
      SELECT COUNT(*)
        INTO l_count
        FROM ar.ra_customer_trx_all rcta
       WHERE creation_date BETWEEN l_fromdate AND l_todate
         AND rcta.complete_flag = 'Y'
         AND rcta.org_id = l_org
         AND customer_trx_id NOT IN
             (SELECT customer_trx_id
                FROM xxwc.xxwcar_taxware_audit_tbl xtat
               WHERE xtat.customer_trx_id = rcta.customer_trx_id);
    
      fnd_file.put_line(fnd_file.log
                       ,'Record Count to be inserted:  ' || l_count);
      fnd_file.put_line(fnd_file.output
                       ,'Record Count to be inserted:  ' || l_count);
    
    EXCEPTION
      WHEN no_data_found THEN
        l_count := 0;
      WHEN OTHERS THEN
        l_count := 0;
      
    END;
  
    IF l_count > 0
    THEN
    
      l_sec := 'Loading transactions for AR Taxware Audit creation Dates between:  ';
      fnd_file.put_line(fnd_file.log
                       ,'Start time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec || l_fromdate || ' and ' ||
                        l_todate);
      fnd_file.put_line(fnd_file.output
                       ,'Start time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec || l_fromdate || ' and ' ||
                        l_todate);
    
      INSERT /*+APPEND*/
      INTO xxwc.xxwcar_taxware_audit_tbl
        (SELECT rcta.customer_trx_id
               ,rcta.creation_date
               ,rcta.trx_number
               ,rcta.trx_date
               ,apsa.amount_line_items_original total_line_items
               ,apsa.tax_original               total_tax
               ,apsa.freight_original           total_freight
               ,apsa.amount_due_original        total_invoice
               ,rcta.bill_to_customer_id
               ,rcta.bill_to_site_use_id
               ,rcta.ship_to_customer_id
               ,rcta.ship_to_site_use_id
               ,rcta.complete_flag
               --,rcta.org_id
                -- Bill To Information
               ,hp_bill.party_name cust_bill_to_name
               ,hps_bill.party_site_number cust_bill_to_site_number
               ,hca_bill.account_name cust_bill_to_acct_name
               ,hca_bill.account_number cust_bill_to_number
               ,hcsu_bill.location cust_bill_to_location
               ,hl_bill.address1 cust_bill_to_address1
               ,hl_bill.address2 cust_bill_to_address2
               ,hl_bill.address3 cust_bill_to_address3
               ,hl_bill.address4 cust_bill_to_address4
               ,hl_bill.city cust_bill_to_city
               ,nvl(hl_bill.state, hl_bill.province) cust_bill_to_state_prov
               ,hl_bill.postal_code cust_bill_to_zip_code
               ,hl_bill.county cust_bill_to_county
               ,hl_bill.country cust_bill_to_country
               ,hps_bill.addressee bill_to_addressee
               ,xxwc_mv_routines_pkg.get_contact_name(rcta.bill_to_contact_id) bill_to_contact
               ,hl_bill.sales_tax_geocode bill_to_tax_geocode
                -- Ship To Information
               ,hp_ship.party_name cust_ship_to_name
               ,hps_ship.party_site_number cust_ship_to_site_number
               ,hca_ship.account_name cust_ship_to_acct_name
               ,hca_ship.account_number cust_ship_to_number
               ,hcsu_ship.location cust_ship_to_location
               ,hl_ship.address1 cust_ship_to_address1
               ,hl_ship.address2 cust_ship_to_address2
               ,hl_ship.address3 cust_ship_to_address3
               ,hl_ship.address4 cust_ship_to_address4
               ,hl_ship.city cust_ship_to_city
               ,nvl(hl_ship.state, hl_ship.province) cust_ship_to_state_prov
               ,hl_ship.postal_code cust_ship_to_zip_code
               ,hl_ship.county cust_ship_to_county
               ,hl_ship.country cust_ship_to_country
               ,hps_ship.addressee ship_to_addressee
               ,xxwc_mv_routines_pkg.get_contact_name(rcta.ship_to_contact_id) ship_to_contact
               ,hl_ship.sales_tax_geocode ship_to_tax_geocode
               ,SYSDATE
               --Version 1.1
               ,(CASE
                  WHEN rbsa.name LIKE 'PRISM%' AND
                       rcta.ct_reference = ltrim(rcta.purchase_order, '0') THEN
                   NULL
                  ELSE
                   rcta.purchase_order
                END) po_number
               ,xxwcar_taxware_pkg.get_trx_salesrep_num(rcta.primary_salesrep_id
                                                       ,hcsu_bill.primary_salesrep_id
                                                       ,rcta.org_id) account_manager_num
               ,xxwc_mv_routines_pkg.get_bt_trx_salesrep(rcta.primary_salesrep_id
                                                        ,hcsu_bill.primary_salesrep_id
                                                        ,rcta.org_id) account_manager
               ,(CASE
                  WHEN --rbsa.name IN ('ORDER MANAGEMENT', 'STANDARD OM SOURCE')   --Version 1.2 
                       rbsa.grouping_rule_id = l_grouping_rule                     --Version 1.2 
                    THEN
                   (SELECT xxwc_mv_routines_pkg.get_user_employee(ooha.created_by)
                      FROM oe_order_headers_all ooha
                     WHERE ooha.order_number =
                           rcta.interface_header_attribute1
                       AND rownum = 1)
                  WHEN rbsa.name LIKE 'PRISM%' THEN
                   rcta.interface_header_attribute10
                  ELSE
                   NULL
                END) taken_by
               ,hla.location_code whs_branch_name
               ,(CASE
                  WHEN --rbsa.name IN ('ORDER MANAGEMENT', 'STANDARD OM SOURCE')   --Version 1.2 
                       rbsa.grouping_rule_id = l_grouping_rule                     --Version 1.2 
                    THEN
                   rcta.interface_header_attribute1
                  ELSE
                   (SELECT sales_order
                      FROM ra_customer_trx_lines_all rctla
                     WHERE rctla.customer_trx_id = rcta.customer_trx_id
                       AND rctla.sales_order IS NOT NULL
                       AND rownum = 1)
                END) order_number
               ,(CASE
                  WHEN --rbsa.name IN ('ORDER MANAGEMENT', 'STANDARD OM SOURCE')   --Version 1.2 
                       rbsa.grouping_rule_id = l_grouping_rule                       --Version 1.2  
                     THEN
                   (SELECT ooha.ordered_date --to_char(ooha.ordered_date, 'MM/DD/YYYY')
                      FROM oe_order_headers_all ooha
                     WHERE ooha.order_number =
                           rcta.interface_header_attribute1
                       AND rownum = 1)
                  ELSE
                   (SELECT sales_order_date --to_char(sales_order_date, 'MM/DD/YYYY')
                      FROM ra_customer_trx_lines_all rctla
                     WHERE rctla.customer_trx_id = rcta.customer_trx_id
                       AND rctla.sales_order_date IS NOT NULL
                       AND rownum = 1)
                END) order_date
               ,(CASE
                  WHEN rctta.type = 'CM' THEN
                   'IMMEDIATE'
                  ELSE
                   rtt.description
                END) terms
               ,(CASE
                  WHEN --rbsa.name IN ('ORDER MANAGEMENT', 'STANDARD OM SOURCE')   --Version 1.2 
                       rbsa.grouping_rule_id = l_grouping_rule                     --Version 1.2  
                    THEN
                   (SELECT wcsm.ship_method_code_meaning
                      FROM oe_order_headers_all       ooha
                          ,wsh_carrier_ship_methods_v wcsm
                     WHERE ooha.order_number =
                           rcta.interface_header_attribute1
                       AND ooha.shipping_method_code = wcsm.ship_method_code
                       AND rownum = 1)
                  WHEN rbsa.name LIKE 'PRISM%' THEN
                   rcta.interface_header_attribute12
                  ELSE
                   rcta.ship_via
                END) ship_via
                 ,rcta.org_id
                --Version 1.1 ended
           FROM ar.ra_customer_trx_all      rcta
               ,ar.ar_payment_schedules_all apsa
                -- Bill To Data
               ,ar.hz_cust_accounts       hca_bill
               ,ar.hz_parties             hp_bill
               ,ar.hz_cust_site_uses_all  hcsu_bill
               ,ar.hz_cust_acct_sites_all hcasa_bill
               ,ar.hz_party_sites         hps_bill
               ,ar.hz_locations           hl_bill
                -- Ship To Data
               ,ar.hz_cust_accounts       hca_ship
               ,ar.hz_parties             hp_ship
               ,ar.hz_cust_site_uses_all  hcsu_ship
               ,ar.hz_cust_acct_sites_all hcasa_ship
               ,ar.hz_party_sites         hps_ship
               ,ar.hz_locations           hl_ship
                -- Invoice Data
               ,ar.ra_terms_tl            rtt
               ,ar.ra_cust_trx_types_all  rctta
               ,ra_batch_sources_all      rbsa
               ,hr.hr_locations_all       hla
               ,hr_all_organization_units haou
          WHERE rcta.complete_flag = 'Y'
            AND rcta.org_id = l_org
            AND rcta.creation_date BETWEEN l_fromdate AND l_todate
            AND rcta.customer_trx_id = apsa.customer_trx_id
               -- Bill to Data
            AND rcta.bill_to_customer_id = hca_bill.cust_account_id
            AND hca_bill.party_id = hp_bill.party_id
            AND rcta.bill_to_site_use_id = hcsu_bill.site_use_id
            AND hca_bill.cust_account_id = hcasa_bill.cust_account_id
            AND hcasa_bill.cust_acct_site_id = hcsu_bill.cust_acct_site_id
            AND hcsu_bill.site_use_code = 'BILL_TO'
            AND hcasa_bill.party_site_id = hps_bill.party_site_id
            AND hps_bill.location_id = hl_bill.location_id
               -- Ship To Data
            AND rcta.ship_to_customer_id = hca_ship.cust_account_id(+)
            AND hca_ship.party_id = hp_ship.party_id(+)
            AND rcta.ship_to_site_use_id = hcsu_ship.site_use_id(+)
            AND hcsu_ship.cust_acct_site_id =
                hcasa_ship.cust_acct_site_id(+)
            AND hcasa_ship.party_site_id = hps_ship.party_site_id(+)
            AND hps_ship.location_id = hl_ship.location_id(+)
               -- Invoice Data      --Version 1.1 ended  
            AND rcta.term_id = rtt.term_id(+)
            AND rtt.language(+) = 'US'
            AND rcta.cust_trx_type_id = rctta.cust_trx_type_id
            AND rcta.org_id = rctta.org_id
            AND rcta.batch_source_id = rbsa.batch_source_id
            AND rcta.org_id = rbsa.org_id
            AND ((rbsa.name NOT LIKE 'PRISM%' AND
                nvl(rcta.interface_header_attribute10
                     ,(SELECT to_char(organization_id)
                        FROM mtl_parameters
                      -- WHERE organization_code = 'WCC')) =
                         WHERE organization_code = fnd_profile.VALUE ('XXWC_CORPORATE_ORGANIZATION'))) =
                to_char(haou.organization_id) AND
                haou.location_id = hla.location_id) OR
                (rbsa.name LIKE 'PRISM%' AND
               -- nvl(lpad(rcta.interface_header_attribute7, 3, '0'), 'WCC') =
                  nvl(lpad(rcta.interface_header_attribute7, 3, '0'), fnd_profile.VALUE ('XXWC_CORPORATE_ORGANIZATION')) =
                substr(haou.name, 1, instr(haou.name, '-', 1, 1) - 2) AND
                haou.location_id = hla.location_id))  
                --Version 1.1 ended
            AND rcta.customer_trx_id NOT IN
                (SELECT customer_trx_id
                   FROM xxwc.xxwcar_taxware_audit_tbl xtat
                  WHERE xtat.customer_trx_id = rcta.customer_trx_id));
    
      COMMIT;
      l_sec := ' -  Completed inserting data into XXWC_TAXWARE_AUDIT_TBL';
    
      fnd_file.put_line(fnd_file.log
                       ,'End time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        l_sec);
      fnd_file.put_line(fnd_file.output
                       ,'End time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        l_sec);
    
    END IF;
  
  EXCEPTION
    WHEN OTHERS THEN
      retcode := SQLCODE;
      errbuf  := substr(SQLERRM, 1, 2000);
      fnd_file.put_line(fnd_file.log
                       ,'Error in ' || l_procedure_name || l_sec);
      fnd_file.put_line(fnd_file.log, SQLERRM || '-' || SQLCODE);
      fnd_file.put_line(fnd_file.output, SQLERRM || '-' || SQLCODE);
    
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_procedure_name
                                          ,p_calling           => 'Exception for xxwcar_taxware_pkg'
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => retcode
                                          ,p_error_desc        => errbuf
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'AR');
  END load_audit;

  /**************************************************************************
  File Name: XXWCAR_TAXWARE_PKG
  
  PROGRAM TYPE: SQL Script
  
  PURPOSE:      package Specification is used to load invoice transaction
                data to capture the original customer attributes for tax
                audit.  Concurrent request 'WC AR TAXWARE AUDIT LOAD' will
                be run daily at 9:00 am to load transactions
  
  HISTORY
  =============================================================================
         Last Update Date : 08/10/2012
  =============================================================================
  =============================================================================
  VERSION DATE          AUTHOR(S)          DESCRIPTION
  ------- -----------   -----------------  ------------------------------------
  1.0     10-Aug-2012   Kathy Poling       Initial creation of the function
                                           Service Ticket 160659
  =============================================================================
  *****************************************************************************/

  FUNCTION get_trx_salesrep_num(p_trx_salesrep_id     IN NUMBER
                               ,p_bill_to_salesrep_id IN NUMBER
                               ,p_org_id              IN NUMBER)
    RETURN VARCHAR2 IS
    l_salesrep_number VARCHAR2(240);
  BEGIN
    l_salesrep_number := NULL;
  
    -- Pull sales rep for transaction
    BEGIN
      SELECT nvl(jrs.salesrep_number, papf.employee_number)
        INTO l_salesrep_number
        FROM jtf.jtf_rs_salesreps jrs, hr.per_all_people_f papf
       WHERE jrs.salesrep_id = p_trx_salesrep_id
         AND jrs.org_id = p_org_id
         AND jrs.person_id = papf.person_id(+)
         AND trunc(papf.effective_start_date(+)) <= trunc(SYSDATE)
         AND nvl(trunc(papf.effective_end_date(+)), trunc(SYSDATE)) >=
             trunc(SYSDATE);
    EXCEPTION
      WHEN OTHERS THEN
        l_salesrep_number := NULL;
    END;
  
    IF nvl(l_salesrep_number, '-3') = '-3'
    THEN
      -- Pull sales rep for bill to
      BEGIN
        SELECT nvl(jrs.salesrep_number, papf.employee_number)
          INTO l_salesrep_number
          FROM jtf.jtf_rs_salesreps jrs, hr.per_all_people_f papf
         WHERE jrs.salesrep_id = p_bill_to_salesrep_id
           AND jrs.org_id = p_org_id
           AND jrs.person_id = papf.person_id(+)
           AND trunc(papf.effective_start_date(+)) <= trunc(SYSDATE)
           AND nvl(trunc(papf.effective_end_date(+)), trunc(SYSDATE)) >=
               trunc(SYSDATE);
      EXCEPTION
        WHEN OTHERS THEN
          l_salesrep_number := NULL;
      END;
    END IF;
  
    IF nvl(l_salesrep_number, '-3') = '-3'
    THEN
      l_salesrep_number := NULL;
    END IF;
  
    RETURN(l_salesrep_number);
  EXCEPTION
    WHEN OTHERS THEN
      RETURN(NULL);
  END get_trx_salesrep_num;

END xxwcar_taxware_pkg;
/
