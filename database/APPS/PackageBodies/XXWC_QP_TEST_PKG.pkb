CREATE OR REPLACE PACKAGE BODY APPS.xxwc_qp_test_pkg
AS
   /*************************************************************************
        *   Function : GET_ORDER_SUB_TOTAL
        *
        *   PURPOSE:   This procedure return order sub total excluding Freight and Tzxes
        *  Author: Raghav Velicheti (TMS # 20130121-00460)
        * Creation_date 4/3/2013
        *Last update Date 4/3/2013
        *   Parameter:
        *          IN
        *              p_Header_id
        * ************************************************************************/
   FUNCTION xxwc_get_tot_minus (p_header_id IN NUMBER)
      RETURN NUMBER
   IS
      p_order_sum   NUMBER;

      CURSOR get_sum_order_lines_cur (l_header_id NUMBER)
      IS
         SELECT SUM ( (unit_selling_price * ordered_quantity) * DECODE (line_category_code, 'ORDER', 1, -1))
           FROM oe_order_lines_all
          WHERE header_id = l_header_id;
   BEGIN
      OPEN get_sum_order_lines_cur (p_header_id);

      FETCH get_sum_order_lines_cur INTO p_order_sum;

      CLOSE get_sum_order_lines_cur;

      RETURN p_order_sum;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END xxwc_get_tot_minus;



   /*************************************************************************
   *   Function : XXWC_PRICE_REQUEST
   *
   *   PURPOSE:   This function is used to derive the system delivered price..
   *   Author: Ram Talluri
   *   Creation_date 8/20/2013
   *   Last update Date 8/20/2013
   *   Parameter:
   *          IN
   *              p_ship_from_org_id
   *             p_inventory_item_id
   *            p_customer_id
   *            p_cust_location_id
   * ************************************************************************/
   PROCEDURE xxwc_price_request (errbuf                  OUT VARCHAR2
                                ,retcode                 OUT VARCHAR2
                                ,p_ship_from_org_id   IN     NUMBER
                                ,p_item_tbl           IN     item_tbl_type
                                ,p_customer_id        IN     NUMBER
                                ,p_cust_site_use_id   IN     NUMBER)
   --p_quantity            IN     NUMBER)
   IS
      p_line_tbl                 qp_preq_grp.line_tbl_type;
      p_qual_tbl                 qp_preq_grp.qual_tbl_type;
      p_line_attr_tbl            qp_preq_grp.line_attr_tbl_type;
      p_line_detail_tbl          qp_preq_grp.line_detail_tbl_type;
      p_line_detail_qual_tbl     qp_preq_grp.line_detail_qual_tbl_type;
      p_line_detail_attr_tbl     qp_preq_grp.line_detail_attr_tbl_type;
      p_related_lines_tbl        qp_preq_grp.related_lines_tbl_type;
      p_control_rec              qp_preq_grp.control_record_type;
      x_line_tbl                 qp_preq_grp.line_tbl_type;
      x_line_qual                qp_preq_grp.qual_tbl_type;
      x_line_attr_tbl            qp_preq_grp.line_attr_tbl_type;
      x_line_detail_tbl          qp_preq_grp.line_detail_tbl_type;
      x_line_detail_qual_tbl     qp_preq_grp.line_detail_qual_tbl_type;
      x_line_detail_attr_tbl     qp_preq_grp.line_detail_attr_tbl_type;
      x_related_lines_tbl        qp_preq_grp.related_lines_tbl_type;
      x_return_status            VARCHAR2 (240);
      x_return_status_text       VARCHAR2 (240);
      qual_rec                   qp_preq_grp.qual_rec_type;
      line_attr_rec              qp_preq_grp.line_attr_rec_type;
      line_rec                   qp_preq_grp.line_rec_type;
      detail_rec                 qp_preq_grp.line_detail_rec_type;
      ldet_rec                   qp_preq_grp.line_detail_rec_type;
      rltd_rec                   qp_preq_grp.related_lines_rec_type;
      l_pricing_contexts_tbl     qp_attr_mapping_pub.contexts_result_tbl_type;
      l_qualifier_contexts_tbl   qp_attr_mapping_pub.contexts_result_tbl_type;
      v_line_tbl_cnt             INTEGER;
      l_item_tbl                 item_tbl_type;
      l_inv_item_id              NUMBER;

      -- i                          BINARY_INTEGER;
      l_version                  VARCHAR2 (240);
      l_file_val                 VARCHAR2 (60);
      l_modifier_name            VARCHAR2 (240);
      l_list_header_id           NUMBER;
      v_list_header_id           NUMBER;
      l_item_category            NUMBER;
      l_gm_selling_price         NUMBER;
      l_message_level            NUMBER;
      k                          BINARY_INTEGER := 1;
      j                          BINARY_INTEGER := 1;
      l                          BINARY_INTEGER := 1;
      /*
            CURSOR C_WEBBER_ITEMS
            IS
            SELECT DISTINCT msi.inventory_item_id,
                               msi.segment1,
                               msi.description,
                               cat.category_concat_segs item_category,
                               cat.category_set_id
                 FROM apps.mtl_system_items_b msi, apps.mtl_item_categories_v cat
                WHERE msi.organization_id = 250
                  AND msi.segment1 = '278STCRETE' --'278SW1002C' --'278SW1002C'
                  AND msi.inventory_item_id = cat.inventory_item_id
                  AND cat.category_id = 4261;
      */
      l_cust_account_id          NUMBER;                       --:= 82737;--Dummy Customer for Pricing Reports
      l_cust_site_use_id         NUMBER;                                                          --:= 523750;
      l_organization_id          NUMBER;                                                   --:= 222; --:= 250;
   BEGIN
      INSERT INTO xxwc.xxwc_pa_warehouse_gt (organization_id)
           VALUES (p_ship_from_org_id);

      /*
            Fnd_File.put_line (Fnd_File.LOG, 'Stage1');

            Fnd_File.put_line (Fnd_File.output,
                               'Pricing report for Webber product Catalog');

            Fnd_File.put_line (
               Fnd_File.output,
               'Barcode1 ,Part Number, Description, Manufacturer, Package Size, UOM, Price, Key Words, Modifier/Price list applied');
        */

      qp_attr_mapping_pub.build_contexts (p_request_type_code           => 'ONT'
                                         ,p_pricing_type                => 'L'
                                         ,x_price_contexts_result_tbl   => l_pricing_contexts_tbl
                                         ,x_qual_contexts_result_tbl    => l_qualifier_contexts_tbl);

      FOR i IN 1 .. p_item_tbl.LAST
      LOOP
         DBMS_OUTPUT.put_line ('Loop for ' || p_item_tbl (i).inventory_item_id);

         l_gm_selling_price := NULL;
         l_item_category := NULL;


         BEGIN
            SELECT category_id
              INTO l_item_category
              FROM mtl_item_categories_v
             WHERE     inventory_item_id = p_item_tbl (i).inventory_item_id
                   AND category_set_id = 1100000062
                   AND organization_id = p_ship_from_org_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_item_category := NULL;
         END;

         DBMS_OUTPUT.put_line ('Itemcat=' || l_item_category);
         /*
                  qp_attr_mapping_pub.build_contexts (
                     p_request_type_code           => 'ONT',
                     p_pricing_type                => 'L',
                     x_price_contexts_result_tbl   => l_pricing_contexts_tbl,
                     x_qual_contexts_result_tbl    => l_qualifier_contexts_tbl);
         */
         v_line_tbl_cnt := p_item_tbl.COUNT;

         ---- Control Record
         p_control_rec.pricing_event := 'BATCH';                                                   -- 'BATCH';
         p_control_rec.calculate_flag := 'Y';                              --QP_PREQ_GRP.G_SEARCH_N_CALCULATE;
         p_control_rec.simulation_flag := 'Y';
         p_control_rec.rounding_flag := 'Q';
         p_control_rec.manual_discount_flag := 'Y';
         p_control_rec.request_type_code := 'ONT';
         p_control_rec.temp_table_insert_flag := 'Y';

         ---- Line Records ---------
         line_rec.request_type_code := 'ONT';
         line_rec.line_id := i * -1;                   -- Order Line Id. This can be any thing for this script
         line_rec.line_index := i;                                                       -- Request Line Index
         line_rec.line_type_code := 'LINE';                                     -- LINE or ORDER(Summary Line)
         line_rec.pricing_effective_date := SYSDATE;                              -- Pricing as of what date ?
         line_rec.active_date_first := SYSDATE;                            -- Can be Ordered Date or Ship Date
         line_rec.active_date_second := SYSDATE;                           -- Can be Ordered Date or Ship Date
         line_rec.active_date_first_type := 'NO TYPE';                                             -- ORD/SHIP
         line_rec.active_date_second_type := 'NO TYPE';                                            -- ORD/SHIP
         line_rec.line_quantity := p_item_tbl (i).order_qty;                               -- Ordered Quantity
         DBMS_OUTPUT.put_line ('VAlue of i is ' || i);

         -- line_rec.line_uom_code := 'EA';                                                         -- Ordered UOM Code
         BEGIN
            SELECT msib.primary_uom_code
              INTO line_rec.line_uom_code
              FROM mtl_system_items_b msib
             WHERE     msib.inventory_item_id = p_item_tbl (i).inventory_item_id
                   AND msib.organization_id = p_ship_from_org_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               line_rec.line_uom_code := 'EA';
         END;


         --Fnd_File.put_line (Fnd_File.LOG, 'UOM-' || line_rec.line_uom_code);

         line_rec.currency_code := 'USD';                                                     -- Currency Code
         line_rec.price_flag := 'Y';                 -- Price Flag can have 'Y' , 'N'(No pricing) , 'P'(Phase)
         p_line_tbl (i) := line_rec;


         ---- Line Attribute Record
         line_attr_rec.line_index := i;
         line_attr_rec.pricing_context := 'ITEM';
         line_attr_rec.pricing_attribute := 'PRICING_ATTRIBUTE1';
         line_attr_rec.pricing_attr_value_from := TO_CHAR (p_item_tbl (i).inventory_item_id); -- INVENTORY ITEM ID
         line_attr_rec.validated_flag := 'N';
         p_line_attr_tbl (k) := line_attr_rec;
         k := k + 1;

         /*
                  Fnd_File.put_line (
                     Fnd_File.LOG,
                     'ITEM_ID-' || line_attr_rec.pricing_attr_value_from);
           */
         IF l_item_category IS NOT NULL
         THEN
            line_attr_rec.line_index := i;
            line_attr_rec.pricing_context := 'ITEM';                                                        --
            line_attr_rec.pricing_attribute := 'PRICING_ATTRIBUTE2';
            line_attr_rec.pricing_attr_value_from := TO_CHAR (l_item_category);                 -- Category ID
            line_attr_rec.validated_flag := 'N';
            p_line_attr_tbl (k) := line_attr_rec;
            k := k + 1;
         END IF;

         DBMS_OUTPUT.put_line (' line_attr_rec.pricing_context - ' || line_attr_rec.pricing_context);
         DBMS_OUTPUT.put_line (' line_attr_rec.pricing_attribute - ' || line_attr_rec.pricing_attribute);
         DBMS_OUTPUT.put_line (
            ' line_attr_rec.pricing_attr_value_from - ' || line_attr_rec.pricing_attr_value_from);

         DBMS_OUTPUT.put_line ('ITEM_CATEGORY_ID-' || line_attr_rec.pricing_attr_value_from);

         qual_rec.line_index := i;
         qual_rec.qualifier_context := 'ORDER';
         qual_rec.qualifier_attribute := 'QUALIFIER_ATTRIBUTE18';
         qual_rec.qualifier_attr_value_from := TO_CHAR (NVL (p_ship_from_org_id, l_organization_id)); -- SHIP_FROM_ORG_ID;
         qual_rec.comparison_operator_code := '=';
         qual_rec.validated_flag := 'Y';
         p_qual_tbl (j) := qual_rec;
         j := j + 1;

         DBMS_OUTPUT.put_line ('org_id-' || qual_rec.qualifier_attr_value_from);

         -- Wrap these two qualifiers so they only get added once
         IF i = 1
         THEN
         qual_rec.line_index := i;
         qual_rec.qualifier_context := 'CUSTOMER';
         qual_rec.qualifier_attribute := 'QUALIFIER_ATTRIBUTE32';
         qual_rec.qualifier_attr_value_from := TO_CHAR (NVL (p_customer_id, l_cust_account_id)); -- CUSTOMER ID;
         qual_rec.comparison_operator_code := '=';
         qual_rec.validated_flag := 'Y';
         p_qual_tbl (j) := qual_rec;
         j := j + 1;

         DBMS_OUTPUT.put_line ('cust_id-' || qual_rec.qualifier_attr_value_from);


         IF NVL (p_cust_site_use_id, l_cust_site_use_id) IS NOT NULL
         THEN
            qual_rec.line_index := i;
            qual_rec.qualifier_context := 'CUSTOMER';
            qual_rec.qualifier_attribute := 'QUALIFIER_ATTRIBUTE11';
            qual_rec.qualifier_attr_value_from := TO_CHAR (NVL (p_cust_site_use_id, l_cust_site_use_id)); -- Ship To Site Use ID;
            qual_rec.comparison_operator_code := '=';
            qual_rec.validated_flag := 'Y';
            p_qual_tbl (j) := qual_rec;
            j := j + 1;
         END IF;
         END IF;

         IF NVL (p_cust_site_use_id, l_cust_site_use_id) IS NOT NULL
         THEN
            fnd_file.put_line (fnd_file.LOG, 'cust_site_id-' || qual_rec.qualifier_attr_value_from);
            fnd_file.put_line (fnd_file.LOG, 'Stage3');
         END IF;

         DBMS_OUTPUT.put_line (' qual_rec.qualifier_context - ' || qual_rec.qualifier_context);
         DBMS_OUTPUT.put_line (' qual_rec.qualifier_attribute - ' || qual_rec.qualifier_attribute);
         DBMS_OUTPUT.put_line (
            ' qual_rec.qualifier_attr_value_from - ' || qual_rec.qualifier_attr_value_from);
      END LOOP;

      qp_preq_pub.price_request (p_line_tbl
                                ,p_qual_tbl
                                ,p_line_attr_tbl
                                ,p_line_detail_tbl
                                ,p_line_detail_qual_tbl
                                ,p_line_detail_attr_tbl
                                ,p_related_lines_tbl
                                ,p_control_rec
                                ,x_line_tbl
                                ,x_line_qual
                                ,x_line_attr_tbl
                                ,x_line_detail_tbl
                                ,x_line_detail_qual_tbl
                                ,x_line_detail_attr_tbl
                                ,x_related_lines_tbl
                                ,x_return_status
                                ,x_return_status_text);
      DBMS_OUTPUT.put_line ('Rs=' || x_return_status || ' rettxt=' || x_return_status_text);

      -- Getting new selling price
      l := x_line_tbl.FIRST;

      FOR i IN 1 .. x_line_tbl.COUNT
      LOOP
         FOR m IN 1 .. x_line_attr_tbl.COUNT
         LOOP
            IF     x_line_attr_tbl (m).line_index = x_line_tbl (i).line_index
               AND x_line_attr_tbl (m).pricing_attribute = 'PRICING_ATTRIBUTE1'
            THEN
               l_inv_item_id := x_line_attr_tbl (m).pricing_attr_value_from;
               DBMS_OUTPUT.put_line ('InvitemID=' || l_inv_item_id);
            END IF;
         END LOOP;

         DBMS_OUTPUT.put_line (
               'List Price='
            || x_line_tbl (i).adjusted_unit_price
            || ' '
            || x_line_tbl (i).unit_price
            || ' '
            || x_line_tbl (i).updated_adjusted_unit_price
            || ' '
            || x_line_tbl (i).extended_price);

         INSERT INTO xxwc_ecomm_order_line_tmp (customer_id
                                               ,cust_site_use_id
                                               ,inventory_item_id
                                               ,ship_from_org_id
                                               ,selling_price)
              VALUES (p_customer_id
                     ,p_cust_site_use_id
                     ,l_inv_item_id
                     ,p_ship_from_org_id
                     ,x_line_tbl (i).adjusted_unit_price);
      --l_gm_selling_price := x_line_tbl (l).adjusted_unit_price;
      END LOOP;
   /*
            IF l IS NOT NULL
            THEN
               LOOP
                    l_gm_selling_price := x_line_tbl (l).adjusted_unit_price;

   dbms_output.put_line('l_gm_selling_price - '||l_gm_selling_price);

                 EXIT WHEN l = x_line_tbl.LAST;
                   l := x_line_tbl.NEXT (l);
               END LOOP;
            END IF;


    dbms_output.put_line('l_gm_selling_price 2- '||l_gm_selling_price);

            l := x_line_detail_tbl.FIRST;

            IF l IS NOT NULL
            THEN
               LOOP

                  IF x_line_detail_tbl (l).automatic_flag = 'Y'
                  THEN
                     l_modifier_name := NULL;
                     l_list_header_id := NULL;

                     BEGIN
                        SELECT name
                          INTO l_modifier_name
                          FROM qp_list_headers_vl
                         WHERE list_header_id =
                                  x_line_detail_tbl (l).list_header_id;

                        Fnd_File.put_line (
                           Fnd_File.LOG,
                           'Modifier_applied-' || l_modifier_name);

                        l_list_header_id := x_line_detail_tbl (i).list_header_id;
                     EXCEPTION
                        WHEN OTHERS
                        THEN
                           l_modifier_name := NULL;
                           l_list_header_id := NULL;
                     END;
                  END IF;

                  EXIT WHEN i = x_line_detail_tbl.LAST;
                  i := x_line_detail_tbl.NEXT (i);

               END LOOP;
            END IF;


                  Fnd_File.put_line (
               Fnd_File.output,
                  r.segment1
               || ','
               || r.segment1
               || ','
               || r.Description
               || ','
               || '   '
               || ','
               || '1'
               || ','
               || line_rec.line_uom_code
               || ','
               || l_gm_selling_price
               || ','
               || '   '
               || ','
               || l_modifier_name);

                  dbms_output.put_line (r.segment1
               || ','
               || r.segment1
               || ','
               || r.Description
               || ','
               || '   '
               || ','
               || '1'
               || ','
               || line_rec.line_uom_code
               || ','
               || l_gm_selling_price
               || ','
               || '   '
               || ','
               || l_modifier_name);
   END LOOP;

   Fnd_File.put_line (
            Fnd_File.output,'End of Report');
*/
   END;


   /*************************************************************************
     *   Function : XXWC_PRICE_REQUEST
     *
     *   PURPOSE:   This function is used to derive the system delivered price.
     *              It is an overloaded version that return the prices in the
     *              price table parameters. It does not DBMS_Output the resuts.
     *   Author: Andre Rivas
     *   Creation_date 8/20/2013
     *   Last update Date 6/6/2014
     *   Parameter:
     *          IN
     *              p_ship_from_org_id
     *             p_inventory_item_id
     *            p_customer_id
     *            p_cust_location_id
                  x_line_tbl         OUT qp_preq_grp.line_tbl_type
                  x_line_attr_tbl    OUT qp_preq_grp.line_tbl_type
     * ************************************************************************/
   PROCEDURE xxwc_price_request (errbuf                  OUT VARCHAR2
                                ,retcode                 OUT VARCHAR2
                                ,p_ship_from_org_id   IN     NUMBER
                                ,p_item_tbl           IN     item_tbl_type
                                ,p_customer_id        IN     NUMBER
                                ,p_cust_site_use_id   IN     NUMBER
                                ,x_line_tbl              OUT qp_preq_grp.line_tbl_type
                                ,x_line_attr_tbl         OUT qp_preq_grp.line_attr_tbl_type)
   IS
      p_line_tbl                 qp_preq_grp.line_tbl_type;
      p_qual_tbl                 qp_preq_grp.qual_tbl_type;
      p_line_attr_tbl            qp_preq_grp.line_attr_tbl_type;
      p_line_detail_tbl          qp_preq_grp.line_detail_tbl_type;
      p_line_detail_qual_tbl     qp_preq_grp.line_detail_qual_tbl_type;
      p_line_detail_attr_tbl     qp_preq_grp.line_detail_attr_tbl_type;
      p_related_lines_tbl        qp_preq_grp.related_lines_tbl_type;
      p_control_rec              qp_preq_grp.control_record_type;
      -- x_line_tbl                 qp_preq_grp.line_tbl_type;
      x_line_qual                qp_preq_grp.qual_tbl_type;
      --x_line_attr_tbl            qp_preq_grp.line_attr_tbl_type;
      x_line_detail_tbl          qp_preq_grp.line_detail_tbl_type;
      x_line_detail_qual_tbl     qp_preq_grp.line_detail_qual_tbl_type;
      x_line_detail_attr_tbl     qp_preq_grp.line_detail_attr_tbl_type;
      x_related_lines_tbl        qp_preq_grp.related_lines_tbl_type;
      x_return_status            VARCHAR2 (240);
      x_return_status_text       VARCHAR2 (240);
      qual_rec                   qp_preq_grp.qual_rec_type;
      line_attr_rec              qp_preq_grp.line_attr_rec_type;
      line_rec                   qp_preq_grp.line_rec_type;
      l_pricing_contexts_tbl     qp_attr_mapping_pub.contexts_result_tbl_type;
      l_qualifier_contexts_tbl   qp_attr_mapping_pub.contexts_result_tbl_type;
      v_line_tbl_cnt             INTEGER;
      --l_inv_item_id              NUMBER;

      l_item_category            NUMBER;
      l_gm_selling_price         NUMBER;

      k                          BINARY_INTEGER := 1;
      j                          BINARY_INTEGER := 1;
      l                          BINARY_INTEGER := 1;


      l_cust_account_id          NUMBER;                       --:= 82737;--Dummy Customer for Pricing Reports
      l_cust_site_use_id         NUMBER;                                                          --:= 523750;
      l_organization_id          NUMBER;                                                   --:= 222; --:= 250;
   BEGIN
      INSERT INTO xxwc.xxwc_pa_warehouse_gt (organization_id)
           VALUES (p_ship_from_org_id);

      qp_attr_mapping_pub.build_contexts (p_request_type_code           => 'ONT'
                                         ,p_pricing_type                => 'L'
                                         ,x_price_contexts_result_tbl   => l_pricing_contexts_tbl
                                         ,x_qual_contexts_result_tbl    => l_qualifier_contexts_tbl);

      FOR i IN 1 .. p_item_tbl.LAST
      LOOP
         -- DBMS_OUTPUT.put_line ('Loop for ' || p_item_tbl (i).inventory_item_id);

         l_gm_selling_price := NULL;
         l_item_category := NULL;


         BEGIN
            SELECT category_id
              INTO l_item_category
              FROM mtl_item_categories_v
             WHERE     inventory_item_id = p_item_tbl (i).inventory_item_id
                   AND category_set_id = 1100000062
                   AND organization_id = p_ship_from_org_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_item_category := NULL;
         END;

         -- DBMS_OUTPUT.put_line ('Itemcat=' || l_item_category);

         v_line_tbl_cnt := p_item_tbl.COUNT;

         ---- Control Record
         p_control_rec.pricing_event := 'BATCH';                                                   -- 'BATCH';
         p_control_rec.calculate_flag := 'Y';                              --QP_PREQ_GRP.G_SEARCH_N_CALCULATE;
         p_control_rec.simulation_flag := 'Y';
         p_control_rec.rounding_flag := 'Q';
         p_control_rec.manual_discount_flag := 'Y';
         p_control_rec.request_type_code := 'ONT';
         p_control_rec.temp_table_insert_flag := 'Y';

         ---- Line Records ---------
         line_rec.request_type_code := 'ONT';
         line_rec.line_id := i * -1;                   -- Order Line Id. This can be any thing for this script
         line_rec.line_index := i;                                                       -- Request Line Index
         line_rec.line_type_code := 'LINE';                                     -- LINE or ORDER(Summary Line)
         line_rec.pricing_effective_date := SYSDATE;                              -- Pricing as of what date ?
         line_rec.active_date_first := SYSDATE;                            -- Can be Ordered Date or Ship Date
         line_rec.active_date_second := SYSDATE;                           -- Can be Ordered Date or Ship Date
         line_rec.active_date_first_type := 'NO TYPE';                                             -- ORD/SHIP
         line_rec.active_date_second_type := 'NO TYPE';                                            -- ORD/SHIP
         line_rec.line_quantity := p_item_tbl (i).order_qty;                               -- Ordered Quantity

         --DBMS_OUTPUT.put_line ('VAlue of i is ' || i);

         -- line_rec.line_uom_code := 'EA';                                                         -- Ordered UOM Code
         BEGIN
            SELECT msib.primary_uom_code
              INTO line_rec.line_uom_code
              FROM mtl_system_items_b msib
             WHERE     msib.inventory_item_id = p_item_tbl (i).inventory_item_id
                   AND msib.organization_id = p_ship_from_org_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               line_rec.line_uom_code := 'EA';
         END;


         --Fnd_File.put_line (Fnd_File.LOG, 'UOM-' || line_rec.line_uom_code);

         line_rec.currency_code := 'USD';                                                     -- Currency Code
         line_rec.price_flag := 'Y';                 -- Price Flag can have 'Y' , 'N'(No pricing) , 'P'(Phase)
         p_line_tbl (i) := line_rec;


         ---- Line Attribute Record
         line_attr_rec.line_index := i;
         line_attr_rec.pricing_context := 'ITEM';
         line_attr_rec.pricing_attribute := 'PRICING_ATTRIBUTE1';
         line_attr_rec.pricing_attr_value_from := TO_CHAR (p_item_tbl (i).inventory_item_id); -- INVENTORY ITEM ID
         line_attr_rec.validated_flag := 'N';
         p_line_attr_tbl (k) := line_attr_rec;
         k := k + 1;

         IF l_item_category IS NOT NULL
         THEN
            line_attr_rec.line_index := i;
            line_attr_rec.pricing_context := 'ITEM';                                                        --
            line_attr_rec.pricing_attribute := 'PRICING_ATTRIBUTE2';
            line_attr_rec.pricing_attr_value_from := TO_CHAR (l_item_category);                 -- Category ID
            line_attr_rec.validated_flag := 'N';
            p_line_attr_tbl (k) := line_attr_rec;
            k := k + 1;
         END IF;

         --DBMS_OUTPUT.put_line (' line_attr_rec.pricing_context - ' || line_attr_rec.pricing_context);
         --DBMS_OUTPUT.put_line (' line_attr_rec.pricing_attribute - ' || line_attr_rec.pricing_attribute);
         --DBMS_OUTPUT.put_line (' line_attr_rec.pricing_attr_value_from - ' || line_attr_rec.pricing_attr_value_from);

         --DBMS_OUTPUT.put_line ('ITEM_CATEGORY_ID-' || line_attr_rec.pricing_attr_value_from);

         qual_rec.line_index := i;
         qual_rec.qualifier_context := 'ORDER';
         qual_rec.qualifier_attribute := 'QUALIFIER_ATTRIBUTE18';
         qual_rec.qualifier_attr_value_from := TO_CHAR (NVL (p_ship_from_org_id, l_organization_id)); -- SHIP_FROM_ORG_ID;
         qual_rec.comparison_operator_code := '=';
         qual_rec.validated_flag := 'Y';
         p_qual_tbl (j) := qual_rec;
         j := j + 1;

         --DBMS_OUTPUT.put_line ('org_id-' || qual_rec.qualifier_attr_value_from);

         -- Wrap these two qualifiers so they only get added once
         IF i = 1
         THEN
            qual_rec.line_index := i;
            qual_rec.qualifier_context := 'CUSTOMER';
            qual_rec.qualifier_attribute := 'QUALIFIER_ATTRIBUTE32';
            qual_rec.qualifier_attr_value_from := TO_CHAR (NVL (p_customer_id, l_cust_account_id)); -- CUSTOMER ID;
            qual_rec.comparison_operator_code := '=';
            qual_rec.validated_flag := 'Y';
            p_qual_tbl (j) := qual_rec;
            j := j + 1;

            --DBMS_OUTPUT.put_line ('cust_id-' || qual_rec.qualifier_attr_value_from);


            IF NVL (p_cust_site_use_id, l_cust_site_use_id) IS NOT NULL
            THEN
               qual_rec.line_index := i;
               qual_rec.qualifier_context := 'CUSTOMER';
               qual_rec.qualifier_attribute := 'QUALIFIER_ATTRIBUTE11';
               qual_rec.qualifier_attr_value_from := TO_CHAR (NVL (p_cust_site_use_id, l_cust_site_use_id)); -- Ship To Site Use ID;
               qual_rec.comparison_operator_code := '=';
               qual_rec.validated_flag := 'Y';
               p_qual_tbl (j) := qual_rec;
               j := j + 1;
            END IF;
         END IF;

        /*
         IF NVL (p_cust_site_use_id, l_cust_site_use_id) IS NOT NULL
         THEN
            fnd_file.put_line (fnd_file.LOG, 'cust_site_id-' || qual_rec.qualifier_attr_value_from);
            fnd_file.put_line (fnd_file.LOG, 'Stage3');
         END IF; */
      --DBMS_OUTPUT.put_line (' qual_rec.qualifier_context - ' || qual_rec.qualifier_context);
      --DBMS_OUTPUT.put_line (' qual_rec.qualifier_attribute - ' || qual_rec.qualifier_attribute);
      --DBMS_OUTPUT.put_line (' qual_rec.qualifier_attr_value_from - ' || qual_rec.qualifier_attr_value_from);
      END LOOP;

      qp_preq_pub.price_request (p_line_tbl
                                ,p_qual_tbl
                                ,p_line_attr_tbl
                                ,p_line_detail_tbl
                                ,p_line_detail_qual_tbl
                                ,p_line_detail_attr_tbl
                                ,p_related_lines_tbl
                                ,p_control_rec
                                ,x_line_tbl
                                ,x_line_qual
                                ,x_line_attr_tbl
                                ,x_line_detail_tbl
                                ,x_line_detail_qual_tbl
                                ,x_line_detail_attr_tbl
                                ,x_related_lines_tbl
                                ,x_return_status
                                ,x_return_status_text);
   --DBMS_OUTPUT.put_line ('Rs=' || x_return_status || ' rettxt=' || x_return_status_text);

   -- Getting new selling price
   /*
   l := x_line_tbl.FIRST;

   FOR i IN 1 .. x_line_tbl.COUNT
   LOOP
      FOR m IN 1 .. x_line_attr_tbl.COUNT
      LOOP
         IF x_line_attr_tbl (m).line_index = x_line_tbl (i).line_index AND x_line_attr_tbl (m).pricing_attribute = 'PRICING_ATTRIBUTE1'
         THEN
            l_inv_item_id := x_line_attr_tbl (m).pricing_attr_value_from;
            DBMS_OUTPUT.put_line ('InvitemID=' || l_inv_item_id);
         END IF;
      END LOOP;

      DBMS_OUTPUT.put_line ('List Price=' || x_line_tbl (i).adjusted_unit_price || ' ' || x_line_tbl (i).unit_price || ' ' || x_line_tbl (i).updated_adjusted_unit_price || ' ' || x_line_tbl (i).extended_price);

      INSERT INTO xxwc_ecomm_order_line_tmp (customer_id
                                            ,cust_site_use_id
                                            ,inventory_item_id
                                            ,ship_from_org_id
                                            ,selling_price)
           VALUES (p_customer_id
                  ,p_cust_site_use_id
                  ,l_inv_item_id
                  ,p_ship_from_org_id
                  ,x_line_tbl (i).adjusted_unit_price);
   --l_gm_selling_price := x_line_tbl (l).adjusted_unit_price;
   END LOOP;*/

   END;

   /* Test procedure for the QP Price Request API. This will run through
       a large number of price lists and will output the timing for each
       order. A comma separated IN list can be passed in as a parameter,
       or the parameter may be left null to use the standard benchmark lists
       of items. The parameter list should be a list of order header IDs.
    */
   PROCEDURE price_request_test_run (p_order_header_id_list IN VARCHAR2 := NULL)
   IS
      TYPE item_rec_type IS RECORD (inventory_item_id   NUMBER
                                   ,order_qty           NUMBER);

      errbuf                   VARCHAR2 (240);
      retcode                  VARCHAR2 (240);

      TYPE item_tbl_type IS TABLE OF item_rec_type
         INDEX BY BINARY_INTEGER;

      l_item_tbl               apps.xxwc_qp_test_pkg.item_tbl_type;
      l_item_rec               apps.xxwc_qp_test_pkg.item_rec_type;

      i                        NUMBER;
      l_ship_from_org_id       NUMBER;
      l_customer_id            NUMBER;
      l_cust_site_use_id       NUMBER;


      CURSOR sample_order_cur (order_numb IN NUMBER)
      IS
         SELECT l.inventory_item_id
               ,l.ordered_quantity
               ,l.ship_from_org_id
               ,l.ship_to_org_id
               ,l.sold_to_org_id
           FROM apps.oe_order_lines_all l
          WHERE l.header_id = (SELECT header_id
                                 FROM apps.oe_order_headers_all oh
                                WHERE oh.order_number = order_numb);



      l_order_header_cursor    SYS_REFCURSOR;
      l_order_header_sql       VARCHAR2 (32767);

      order_header_rec         apps.oe_order_headers_all%ROWTYPE;

      tsstart                  TIMESTAMP;
      tsend                    TIMESTAMP;

      output                   VARCHAR2 (8000);

      l_order_header_id_list   VARCHAR (32676);

      -- Return Parameters
      x_line_tbl               qp_preq_grp.line_tbl_type;
      x_line_attr_tbl          qp_preq_grp.line_attr_tbl_type;
   BEGIN
      IF p_order_header_id_list IS NULL
      THEN
         l_order_header_id_list := '9444352
                             ,9443805
                             ,9436275
                             ,9444433
                             ,9443862
                             ,9444257
                             ,9442841
                             ,9438967
                             ,9443927
                             ,9444729
                             ,9442462
                             ,9429136
                             ,9438890
                             ,9442107
                             ,9438526
                             ,9435763
                             ,9436939
                             ,9429889
                             ,9435171
                             ,9444143
                             ,9444515
                             ,9432704
                             ,9433405
                             ,9439145
                             ,9424973
                             ,9394213
                             ,9413469
                             ,9435147
                             ,9435321
                             ,9435275
                             ,9399002
                             ,9435252
                             ,9347846
                             ,9352506
                             ,9430180
                             ,9417908
                             ,9416408
                             ,9355918
                             ,9396816
                             ,9391361
                             ,9442962
                             ,9409732
                             ,9324608
                             ,9416604
                             ,9402277
                             ,9391614
                             ,9434288
                             ,9431505
                             ,9348754
                             ,9414365';
      ELSE
         l_order_header_id_list := p_order_header_id_list;
      END IF;

      l_order_header_sql := 'SELECT *
        FROM apps.oe_order_headers_all oh
       WHERE oh.header_id IN (' || l_order_header_id_list || ')';

      OPEN l_order_header_cursor FOR l_order_header_sql;

      LOOP
         FETCH l_order_header_cursor INTO order_header_rec;

         EXIT WHEN l_order_header_cursor%NOTFOUND;

         i := 1;

         FOR order_line_rec IN sample_order_cur (order_header_rec.order_number)
         LOOP
            l_item_rec := NULL;
            l_item_rec.inventory_item_id := order_line_rec.inventory_item_id;
            l_item_rec.order_qty := order_line_rec.ordered_quantity;
            l_item_tbl (i) := l_item_rec;
            i := i + 1;

            l_ship_from_org_id := order_line_rec.ship_from_org_id;
            l_customer_id := order_line_rec.sold_to_org_id;
            l_cust_site_use_id := order_line_rec.ship_to_org_id;
         END LOOP;

         tsstart := SYSTIMESTAMP;
         apps.xxwc_qp_test_pkg.xxwc_price_request (errbuf
                                                  ,retcode
                                                  ,l_ship_from_org_id
                                                  ,l_item_tbl
                                                  ,l_customer_id
                                                  ,l_cust_site_use_id
                                                  ,x_line_tbl
                                                  ,x_line_attr_tbl);

         -- the two "x_" variables contain the output. currently we are not
         -- reading it for this test.

         tsend := SYSTIMESTAMP;

         output :=
               output
            || TO_CHAR (order_header_rec.order_number)
            || ', '
            || TO_CHAR (i)
            || ', '
            || TO_CHAR (tsend - tsstart, 'SSSSS')
            || CHR (13)
            || CHR (10);
      END LOOP;

      CLOSE l_order_header_cursor;

      DBMS_OUTPUT.put_line ('Time in API Wrapper');
      DBMS_OUTPUT.put_line ('Order,Items,Time');
      DBMS_OUTPUT.put_line (output);
   END;
END xxwc_qp_test_pkg;
/