CREATE OR REPLACE PACKAGE BODY APPS.XXWCAP_POSITIVE_PAY_PKG AS
/********************************************************************************

FILE NAME: APPS.XXWCAP_POSITIVE_PAY_PKG.pkb

PROGRAM TYPE: PL/SQL Package

PURPOSE: API to create WC Positive Pay File

HISTORY
===============================================================================
VERSION DATE          AUTHOR(S)       DESCRIPTION
------- -----------   --------------- -----------------------------------------
1.0     09/16/2013    Gopi Damuluri    Initial version. (TMS# 20130823-00455)
1.1     10/06/2014    Pattabhi Avula   Hardcoded Org_id Initialization commented 
									   for TMS# 20141001-00058 
********************************************************************************/



/********************************************************************************
Procedure Name: UC4_CALL 

Description : API to call "Positive Pay File with Additional Parameters"
********************************************************************************/

PROCEDURE uc4_call (p_errbuf                           OUT VARCHAR2,
                    p_retcode                          OUT NUMBER,
                    p_user_name                        IN VARCHAR2,
                    p_responsibility_name              IN VARCHAR2,
                    p_conc_prg_name                    IN VARCHAR2,
                    p_arg_format                       IN VARCHAR2,
                    p_arg_int_bank_acct_name           IN VARCHAR2,
                    p_arg_pay_status                   IN VARCHAR2,
                    p_arg_reselect                     IN VARCHAR2,
                    p_arg_ob_pay_file_prefix           IN VARCHAR2,
                    p_arg_ob_pay_file_extn             IN VARCHAR2,
                    p_arg_ob_pay_file_dir              IN VARCHAR2)
IS

  --
  -- Package Variables
  --

  l_dflt_email VARCHAR2(200) := 'HDSOracleDevelopers@hdsupply.com';

  l_req_id                NUMBER NULL;
  v_phase                 VARCHAR2(50);
  v_status                VARCHAR2(50);
  v_dev_status            VARCHAR2(50);
  v_dev_phase             VARCHAR2(50);
  v_message               VARCHAR2(250);
  v_error_message         VARCHAR2(3000);

  l_err_msg               VARCHAR2(3000);
  l_err_code              NUMBER;
  l_sec                   VARCHAR2(255);

  l_user_id               NUMBER;
  l_responsibility_id     NUMBER;
  l_resp_application_id   NUMBER;

  l_conc_prg_arg4            VARCHAR2(240)  := TO_CHAR(SYSDATE, 'YYYY-MON-DD');
  l_conc_prg_arg5            VARCHAR2(240)  := TO_CHAR(SYSDATE, 'YYYY-MON-DD');

  -- Error DEBUG
  l_err_callfrom  VARCHAR2(75) DEFAULT 'XXWCAP_POSITIVE_PAY_PKG';
  l_err_callpoint VARCHAR2(75) DEFAULT 'START';
  l_distro_list   VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';

BEGIN

--     mo_global.init ('AP');
   --  mo_global.set_policy_context ('S', 162); -- Commented by Pattabhi for TMS# 20141001-00058 
     fnd_global.set_nls_context ('AMERICAN');

  --------------------------------------------------------------------------
  -- Deriving UserId
  --------------------------------------------------------------------------
  BEGIN
    SELECT user_id
      INTO l_user_id
      FROM fnd_user
     WHERE 1 = 1
       AND user_name = UPPER(p_user_name)
       AND SYSDATE BETWEEN start_date AND NVL(end_date, TRUNC(SYSDATE) + 1);
  EXCEPTION
  WHEN NO_DATA_FOUND THEN
     l_err_msg := 'UserName - '||p_user_name||' not defined in Oracle';
     RAISE program_error;
  WHEN OTHERS THEN
     l_err_msg := 'Error deriving user_id for UserName - '||p_user_name;
     RAISE program_error;
  END;

  --------------------------------------------------------------------------
  -- Deriving ResponsibilityId and ResponsibilityApplicationId
  --------------------------------------------------------------------------
  BEGIN
    SELECT responsibility_id
         , application_id
      INTO l_responsibility_id
         , l_resp_application_id
      FROM fnd_responsibility_vl
     WHERE responsibility_name = p_responsibility_name
       AND SYSDATE BETWEEN start_date AND NVL(end_date, TRUNC(SYSDATE) + 1);
  EXCEPTION
  WHEN NO_DATA_FOUND THEN
     l_err_msg := 'Responsibility - '||p_responsibility_name||' not defined in Oracle';
     RAISE program_error;
  WHEN OTHERS THEN
     l_err_msg := 'Error deriving Responsibility_id for ResponsibilityName - '||p_responsibility_name;
     RAISE program_error;
  END;

  l_sec := 'UC4 call to run concurrent request - PRISM2EBS AP Invoice Interface';

  --------------------------------------------------------------------------
  -- Apps Initialize
  --------------------------------------------------------------------------
  FND_GLOBAL.APPS_INITIALIZE (l_user_id, l_responsibility_id, l_resp_application_id);

  --------------------------------------------------------------------------
  -- Submit "XXWC Create Outbound File Common Program"
  --------------------------------------------------------------------------
  l_req_id := fnd_request.submit_request(application  => 'IBY'
                                       , program      => p_conc_prg_name
                                       , description  => NULL
                                       , start_time   => SYSDATE
                                       , sub_request  => FALSE
                                       , argument1    => NULL
                                       , argument2    => p_arg_format
                                       , argument3    => p_arg_int_bank_acct_name
                                       , argument4    => l_conc_prg_arg4
                                       , argument5    => l_conc_prg_arg5
                                       , argument6    => p_arg_pay_status
                                       , argument7    => p_arg_reselect
                                       , argument8    => NULL
                                       , argument9    => NULL
                                       , argument10   => p_arg_ob_pay_file_prefix
                                       , argument11   => p_arg_ob_pay_file_extn
                                       , argument12   => p_arg_ob_pay_file_dir);

  COMMIT;

  dbms_output.put_line('After fnd_request');
  IF (l_req_id != 0)
  THEN
    IF fnd_concurrent.wait_for_request(l_req_id
                                      ,6
                                      ,15000
                                      ,v_phase
                                      ,v_status
                                      ,v_dev_phase
                                      ,v_dev_status
                                      ,v_message)
    THEN
      v_error_message := chr(10) || 'ReqID=' || l_req_id || ' DPhase ' ||
                         v_dev_phase || ' DStatus ' || v_dev_status ||
                         chr(10) || ' MSG - ' || v_message;
      -- Error Returned
      IF v_dev_phase != 'COMPLETE' OR v_dev_status != 'NORMAL'
      THEN
        l_err_msg := 'An error occured running the XXWCAP_POSITIVE_PAY_PKG ' ||
                       v_error_message || '.';
        fnd_file.put_line(fnd_file.log, l_err_msg);
        fnd_file.put_line(fnd_file.output, l_err_msg);
        RAISE program_error;
      END IF;
      -- Then Success!
    ELSE
      l_err_msg := 'An error occured running the XXWCAP_POSITIVE_PAY_PKG ' ||
                     v_error_message || '.';
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      RAISE program_error;
    END IF;

  ELSE
    l_err_msg := 'An error occured running the XXWCAP_POSITIVE_PAY_PKG ';
  dbms_output.put_line('l_err_msg:  ' || l_err_msg);
    fnd_file.put_line(fnd_file.log, l_err_msg);
    fnd_file.put_line(fnd_file.output, l_err_msg);
    RAISE program_error;
  END IF;

  p_retcode := 0;

EXCEPTION
  WHEN program_error THEN
    ROLLBACK;

    l_err_msg  := l_sec;
    l_err_msg  := l_err_msg || ' ERROR ' || substr(SQLERRM, 1, 2000);

    dbms_output.put_line(l_err_msg);
    fnd_file.put_line(fnd_file.log, l_err_msg);
    fnd_file.put_line(fnd_file.output, l_err_msg);

    xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                        ,p_calling           => l_err_callpoint
                                        ,p_request_id        => l_req_id
                                        ,p_ora_error_msg     => SQLERRM
                                        ,p_error_desc        => 'Error running XXWCAP_POSITIVE_PAY_PKG package with PROGRAM ERROR'
                                        ,p_distribution_list => l_distro_list
                                        ,p_module            => 'AP');

    fnd_file.put_line(fnd_file.output, 'Fix the error!');
    
    p_retcode := 2;
  WHEN OTHERS THEN
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);

    l_err_msg  := l_sec || l_err_msg || ' ERROR ' || SQLCODE ||
                  substr(SQLERRM, 1, 2000);
    dbms_output.put_line(l_err_msg);
    fnd_file.put_line(fnd_file.log, l_err_msg);
    fnd_file.put_line(fnd_file.output, l_err_msg);

    xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                        ,p_calling           => l_err_callpoint
                                        ,p_request_id        => l_req_id
                                        ,p_ora_error_msg     => substr(l_err_msg
                                                                      ,1
                                                                      ,2000)
                                        ,p_error_desc        => 'Error running XXWCAP_POSITIVE_PAY_PKG package with OTHERS Exception'
                                        ,p_distribution_list => l_distro_list
                                        ,p_module            => 'AP');

    p_retcode := 2;

END uc4_call;

END XXWCAP_POSITIVE_PAY_PKG;
/