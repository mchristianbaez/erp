CREATE OR REPLACE PACKAGE BODY APPS.xxcusgl_interface_pkg AS
  --/* $Header: XXCUSFGL_INTERFACE_PKG.pkg 12.1.1 2011/05/09 12:00:00 vjayasee ship $ */

  -- ****************************************************************
  -- ****************************************************************
  -- *
  -- * Application : XXCUS
  -- * Module      : GL Interface
  -- * Version     : 
  -- * Creation    : 09-May-2011  V.Jayaseelan
  -- *
  -- * Purpose     : Used for GL Interface
  -- *  
  -- *  Change History
  -- *
  -- *  Developer      Date         Description
  -- *  ------------  -----------  ----------------------------------
  -- *  V.Jayaseelan  09-May-2011   Initial Version
  -- *  KPoling       26-Jul-2011   Changed group_id for the import
  -- *                              correcting issues with the summary 
  -- *                              and detail load
  -- ****************************************************************
  PROCEDURE print_out(p_message IN VARCHAR2) IS
  BEGIN
    IF fnd_global.conc_request_id > -1
    THEN
      fnd_file.put_line(fnd_file.output, p_message);
    ELSE
      dbms_output.put_line(p_message);
    END IF;
  END print_out;
  PROCEDURE print_log(p_message IN VARCHAR2) IS
  BEGIN
    IF fnd_global.conc_request_id > -1
    THEN
      fnd_file.put_line(fnd_file.log, p_message);
    ELSE
      dbms_output.put_line(p_message);
    END IF;
  END print_log;

  PROCEDURE journal_import(n_retcode OUT NUMBER, c_errbuf OUT VARCHAR2) IS
  
    -- v_sob_id            VARCHAR2(200);
    v_interface_run_id NUMBER;
    v_request_id       NUMBER;
    import_rejection EXCEPTION;
    v_sysdate     DATE := SYSDATE;
    v_group_id    NUMBER;
    v_file_name   VARCHAR2(60);
    v_source_name VARCHAR2(25);
    --
    v_interval    NUMBER := 60; --Time to wait between checks. This is the number of seconds to sleep.
    v_max_wait    NUMBER := 1200; --The maximum time in seconds to wait for the requests completion
    v_rphase      VARCHAR2(30);
    v_rstatus     VARCHAR2(30);
    v_dstatus     VARCHAR2(30);
    v_dphase      VARCHAR2(30) := 'PENDING';
    v_message     VARCHAR2(240);
    v_wait_status BOOLEAN;
  
 /*   CURSOR source_name IS
      SELECT DISTINCT user_je_source_name, set_of_books_id, group_id
        FROM gl.gl_interface
       WHERE accounting_date = trunc(SYSDATE);
  */
  BEGIN
    BEGIN
      FOR lp_group IN (SELECT user_je_source_name, ledger_id, COUNT(*)
                         FROM gl.gl_interface
                        WHERE status = 'NEW'
                        GROUP BY user_je_source_name, ledger_id) LOOP
       
      SELECT gl.gl_interface_control_s.nextval INTO g_group_id FROM dual;                      
                            
       
        UPDATE gl.gl_interface
           SET group_id = g_group_id
         WHERE user_je_source_name = lp_group.user_je_source_name
           AND ledger_id = lp_group.ledger_id
           AND status = 'NEW';
      
        COMMIT;
      END LOOP;
    END;
  
    --v_file_name := p_file_name;
    --FOR lp_source_name IN source_name
    FOR lp_source_name in (SELECT DISTINCT user_je_source_name, ledger_id, group_id
        FROM gl.gl_interface
       WHERE status = 'NEW')
       
    LOOP
      print_log('***************************************************');
      print_log('Importing GL Journals for JE Source - ' ||
                lp_source_name.user_je_source_name);
      print_log('***************************************************');
    
      SELECT gl.gl_journal_import_s.nextval INTO v_interface_run_id FROM dual;
    
      SELECT je_source_name
        INTO v_source_name
        FROM gl.gl_je_sources_tl
       WHERE LANGUAGE = userenv('LANG')
         AND user_je_source_name = lp_source_name.user_je_source_name;
    
      /*UPDATE gl.gl_interface
       SET group_id = v_group_id
       WHERE attribute17 = v_file_name AND
               set_of_books_id = lp_source_name.set_of_books_id AND
            user_je_source_name = lp_source_name.user_je_source_name;
      
      COMMIT;*/
    
      INSERT INTO gl.gl_interface_control
        (je_source_name, status, interface_run_id, set_of_books_id, group_id)
      VALUES
        (v_source_name
        ,'S'
        ,v_interface_run_id
        ,lp_source_name.ledger_id
        ,lp_source_name.group_id);
      COMMIT;
      v_request_id := apps.fnd_request.submit_request('SQLGL'
                                                     ,'GLLEZL'
                                                     ,'Universal Journal IMP'
                                                     , -- Description
                                                      v_sysdate
                                                     , --:REQUEST.START_DATE)
                                                      FALSE
                                                     ,v_interface_run_id
                                                     ,lp_source_name.ledger_id
                                                     ,'N'
                                                     ,''
                                                     ,''
                                                     ,'N'
                                                     ,'O'
                                                     ,chr(0));
    
      IF (v_request_id = 0)
      THEN
        print_log('***************************************************');
        print_log('ERROR: Importing GL Journals FAILED FOR JE Source - ' ||
                  lp_source_name.user_je_source_name);
        print_log('***************************************************');
        RAISE import_rejection;
      ELSE
        COMMIT; -- Must do a commit in order for WAIT_FOR_REQUEST to work correctly.
        v_wait_status := fnd_concurrent.wait_for_request(request_id => v_request_id
                                                        ,INTERVAL   => v_interval
                                                        ,max_wait   => v_max_wait
                                                        ,phase      => v_rphase
                                                        ,status     => v_rstatus
                                                        ,dev_phase  => v_dphase
                                                        ,dev_status => v_dstatus
                                                        ,message    => v_message);
        print_log('***************************************************');
        print_log('SUCCESS: Importing GL Journals COMPLETED FOR JE Source - ' ||
                  lp_source_name.user_je_source_name);
        print_log('***************************************************');
      END IF;
    END LOOP;
   
  END journal_import;

  FUNCTION gl_ccid_valid(p_code_combo IN VARCHAR2) RETURN NUMBER IS
    CURSOR cur_get_ccid IS
      SELECT code_combination_id
        FROM apps.gl_code_combinations_kfv
       WHERE concatenated_segments = p_code_combo;
    v_ccid NUMBER;
  BEGIN
    v_ccid := -1;
    OPEN cur_get_ccid;
    FETCH cur_get_ccid
      INTO v_ccid;
    CLOSE cur_get_ccid;
    IF v_ccid < 0
    THEN
      print_log('GL Code Combination: ' || p_code_combo || ' is not valid.');
    END IF;
    RETURN v_ccid;
  END gl_ccid_valid;

  PROCEDURE insert_data(p_batch_name        IN VARCHAR2
                       ,p_period            IN VARCHAR2
                       ,p_batch_description IN VARCHAR2
                       ,p_reversal_flag     IN VARCHAR2
                       ,p_reversal_period   IN VARCHAR2
                       ,p_reversal_type     IN VARCHAR2
                       ,p_currency_code     IN VARCHAR2
                       ,p_seg1              IN VARCHAR2
                       ,p_seg2              IN VARCHAR2
                       ,p_seg3              IN VARCHAR2
                       ,p_seg4              IN VARCHAR2
                       ,p_seg5              IN VARCHAR2
                       ,p_seg6              IN VARCHAR2
                       ,p_usr_category      IN VARCHAR2
                       ,p_usr_source        IN VARCHAR2
                       ,p_conv_rate         IN VARCHAR2
                       ,p_entered_dr        IN VARCHAR2
                       ,p_entered_cr        IN VARCHAR2
                       ,p_acctd_dr          IN VARCHAR2
                       ,p_acctd_cr          IN VARCHAR2
                       ,p_transaction_date  IN DATE
                       ,p_je_name           IN VARCHAR2
                       ,p_je_description    IN VARCHAR2
                       ,p_line_description  IN VARCHAR2
                       ,p_line_ref1         IN VARCHAR2
                       ,p_line_ref2         IN VARCHAR2
                       ,p_line_ref3         IN VARCHAR2
                       ,p_line_ref4         IN VARCHAR2
                       ,p_line_ref5         IN VARCHAR2
                       ,p_line_ref6         IN VARCHAR2
                       ,p_line_ref7         IN VARCHAR2
                       ,p_line_ref8         IN VARCHAR2
                       ,p_line_ref9         IN VARCHAR2
                       ,p_line_ref10        IN VARCHAR2
                       ,p_line_num          IN NUMBER
                       ,p_line_attribute1   IN VARCHAR2
                       ,p_line_attribute2   IN VARCHAR2
                       ,p_line_attribute3   IN VARCHAR2
                       ,p_line_attribute4   IN VARCHAR2
                       ,p_line_attribute5   IN VARCHAR2
                       ,p_line_context      IN VARCHAR2) IS
    --
    CURSOR cur_period IS
      SELECT end_date
        FROM gl.gl_periods
       WHERE period_set_name = g_ldgr_per_set
         AND period_name = p_period;
    --
    v_ccid      NUMBER;
    v_acct_date DATE;
    --
  BEGIN
    v_ccid := NULL;
    --
    --v_ccid := gl_ccid_valid(p_code_combo);
    --IF v_ccid > 0 THEN
    IF nvl(g_batch, 'XXXXX') <> p_batch_name
    THEN
      --SELECT gl.gl_interface_control_s.nextval INTO g_group_id FROM dual;
      g_batch := p_batch_name;
      COMMIT;
    END IF;
  
    --
    OPEN cur_period;
    FETCH cur_period
      INTO v_acct_date;
    CLOSE cur_period;
    --
    INSERT INTO gl.gl_interface
      (status
      ,ledger_id
      ,accounting_date
      ,currency_code
      ,date_created
      ,created_by
      ,actual_flag
      ,user_je_category_name
      ,user_je_source_name
      ,currency_conversion_date
      ,encumbrance_type_id
      ,budget_version_id
      ,user_currency_conversion_type
      ,currency_conversion_rate
      ,average_journal_flag
      ,originating_bal_seg_value
      ,segment1
      ,segment2
      ,segment3
      ,segment4
      ,segment5
      ,segment6
      ,segment7
      ,
       --segment8, segment9, segment10,
       --segment11, segment12, segment13, segment14,
       --segment15, segment16, segment17, segment18,
       --segment19, segment20, segment21, segment22,
       --segment23, segment24, segment25, segment26,
       --segment27, segment28, segment29, segment30,
       entered_dr
      ,entered_cr
      ,accounted_dr
      ,accounted_cr
      ,transaction_date
      ,reference1
      ,reference2
      ,reference3
      ,reference4
      ,reference5
      ,reference6
      ,reference7
      ,reference8
      ,reference9
      ,reference10
      ,
       --reference11, reference12, reference13, reference14,
       --reference15, reference16, reference17, reference18,
       --reference19, reference20,
       reference21
      ,reference22
      ,reference23
      ,reference24
      ,reference25
      ,reference26
      ,reference27
      ,reference28
      ,reference29
      ,reference30
      ,je_batch_id
      ,period_name
      ,je_header_id
      ,je_line_num
      ,chart_of_accounts_id
      ,functional_currency_code
      ,
       --code_combination_id,
       date_created_in_gl
      ,warning_code
      ,status_description
      ,stat_amount
      ,group_id
      ,request_id
      ,subledger_doc_sequence_id
      ,subledger_doc_sequence_value
      ,attribute1
      ,attribute2
      ,gl_sl_link_id
      ,gl_sl_link_table
      ,attribute3
      ,attribute4
      ,attribute5
      ,
       --attribute6, attribute7, attribute8, attribute9,
       --attribute10, attribute11, attribute12, attribute13,
       --attribute14, attribute15, attribute16, attribute17,
       --attribute18, attribute19, attribute20,
       CONTEXT
      ,context2
      ,invoice_date
      ,tax_code
      ,invoice_identifier
      ,invoice_amount
      ,context3
      ,ussgl_transaction_code
      ,descr_flex_error_message
      ,jgzz_recon_ref
      ,reference_date
      ,set_of_books_id
      ,balancing_segment_value
      ,management_segment_value
      ,funds_reserved_flag
      --,code_combination_id_interim
      )
    VALUES
      ('NEW'
      ,g_ldgr_book_id
      ,v_acct_date
      ,p_currency_code
      ,SYSDATE
      ,fnd_global.user_id
      ,'A'
      ,p_usr_category
      ,p_usr_source
      ,NULL
      , -- Curr Conv Date
       NULL
      , -- Encum Type Id
       NULL
      , -- Budget Ver Id
       'User'
      , -- Cur. Conversion Type
       p_conv_rate
      ,NULL
      , -- Avg jrnl flg
       NULL
      , -- Orig bal Seg
       p_seg1
      ,p_seg2
      ,p_seg3
      ,p_seg4
      ,p_seg5
      ,p_seg6
      ,'00000'
      ,p_entered_dr
      ,p_entered_cr
      ,p_acctd_dr
      ,p_acctd_cr
      ,p_transaction_date
      ,p_batch_name
      ,p_batch_description
      ,NULL
      , -- Not Used
       ltrim(rtrim(p_je_name))
      ,ltrim(rtrim(p_je_description))
      ,NULL
      , -- JE Header Reference
       p_reversal_flag
      , --'YES',              -- JE Reversal Flag
       p_reversal_period
      , --v_period,
       p_reversal_type
      , --'NO',               -- Reversale through Debit/Credit
       ltrim(rtrim(p_line_description))
      ,ltrim(rtrim(p_line_ref1))
      ,ltrim(rtrim(p_line_ref2))
      ,ltrim(rtrim(p_line_ref3))
      ,ltrim(rtrim(p_line_ref4))
      ,ltrim(rtrim(p_line_ref5))
      ,ltrim(rtrim(p_line_ref6))
      ,ltrim(rtrim(p_line_ref7))
      ,ltrim(rtrim(p_line_ref8))
      ,ltrim(rtrim(p_line_ref9))
      ,ltrim(rtrim(p_line_ref10))
      ,NULL
      , -- Je Bactch Id
       p_period
      , ---v_period,
       NULL
      , -- Je Hdr Id
       p_line_num
      ,NULL
      , -- COA Id
       g_ldgr_cur_code
      ,
       --v_ccid,
       trunc(SYSDATE)
      ,NULL
      , -- Warning Code
       NULL
      , -- Status Desc
       NULL
      ,NULL
       
      ,NULL
      , -- Req id
       NULL
      , -- Sub Lgr Seq Id
       NULL
      , -- Sub Lgr Seq Val
       ltrim(rtrim(p_line_attribute1))
      ,ltrim(rtrim(p_line_attribute2))
      ,NULL
      , -- GL SL Link Id
       NULL
      , -- GL SL Link Table
       ltrim(rtrim(p_line_attribute3))
      ,ltrim(rtrim(p_line_attribute4))
      ,ltrim(rtrim(p_line_attribute5))
      ,p_line_context
      ,NULL
      , -- Contxt2
       NULL
      , -- Inv Date
       NULL
      , -- Tax Code
       NULL
      , -- Inv Identifier
       NULL
      , -- Inv Amt
       NULL
      , -- Contxt3
       NULL
      , -- USSGL Trx Code
       NULL
      , -- Desc Flex Err Msg
       NULL
      , -- jgzz recon ref
       NULL
      , -- Ref Date
       -1
      , -- SOB ID
       NULL
      , -- Bal Seg Val
       NULL
      , -- Mgmt Seg Val
       NULL -- Funds Res Flag
      --,NULL  -- CCID Interim
       ); 
    --END IF;
  END insert_data;
  /******************************************************************************
   **************************** Not Used Any More *******************************
   ******************************************************************************
  PROCEDURE gl_get_first_period(tset_of_books_id IN  NUMBER,
                                tperiod_name     IN  VARCHAR2,
                                tfirst_period    OUT VARCHAR2,
                                errbuf           OUT VARCHAR2) IS
  
  BEGIN
  
     SELECT a.period_name
        INTO tfirst_period
     FROM gl_period_statuses a, 
          gl_period_statuses b
     WHERE a.application_id  = 101              AND     
           b.application_id  = 101              AND
           a.set_of_books_id = tset_of_books_id AND
           b.set_of_books_id = tset_of_books_id AND
           a.period_type     = b.period_type    AND
           a.period_year     = b.period_year    AND
           b.period_name     = tperiod_name     AND     
           a.period_num      = (SELECT min(c.period_num)
                                FROM gl_period_statuses c
                                WHERE c.application_id  = 101              AND
                                      c.set_of_books_id = tset_of_books_id AND
                                      c.period_year     = a.period_year    AND
                                      c.period_type     = a.period_type
                                GROUP BY c.period_year);
  
  EXCEPTION
     WHEN NO_DATA_FOUND THEN
  
        errbuf := gl_message.get_message('GL_PLL_INVALID_FIRST_PERIOD', 
                                         'Y',
                                         'PERIOD', 
                                         tperiod_name,
                                         'SOBID', 
                                         tset_of_books_id);
  
     WHEN OTHERS THEN
        errbuf := SQLERRM;
  
  END gl_get_first_period;
   ******************************************************************************
   **************************** Not Used Any More *******************************
   ******************************************************************************/
  PROCEDURE gl_summary_load(p_sob_id    IN NUMBER
                           ,p_period    IN VARCHAR2
                           ,p_curr_code IN VARCHAR2
                           ,p_segment1  IN VARCHAR2
                           ,p_errbuf    OUT VARCHAR2) IS
    CURSOR cur_data(p_ytd_ptd IN VARCHAR2) IS
      SELECT /*+RULE*/
       bal.code_combination_id ccid
      ,bal.currency_code currency_code
      ,'Conversion' user_je_source_name
      ,'Other' usr_category_name
      ,1 cur_conv_rate
      ,SUM(CASE
             WHEN p_ytd_ptd = 'YTD' THEN
              (nvl(bal.begin_balance_dr, 0) - nvl(bal.begin_balance_cr, 0)) +
              (nvl(bal.period_net_dr, 0) - nvl(bal.period_net_cr, 0))
             ELSE
              nvl(bal.period_net_dr, 0) - nvl(bal.period_net_cr, 0)
           END) amount
      ,trunc(bal.last_update_date) trx_date
        FROM gl.gl_balances@r12_to_fin bal
       WHERE bal.set_of_books_id = p_sob_id
         AND bal.period_name = p_period
         AND bal.currency_code = p_curr_code
         AND bal.actual_flag = 'A'
         AND CASE
               WHEN p_ytd_ptd = 'YTD' THEN
                (nvl(bal.begin_balance_dr, 0) - nvl(bal.begin_balance_cr, 0)) +
                (nvl(bal.period_net_dr, 0) - nvl(bal.period_net_cr, 0))
               ELSE
                nvl(bal.period_net_dr, 0) - nvl(bal.period_net_cr, 0)
             END <> 0
       GROUP BY bal.code_combination_id
               ,bal.currency_code
               ,trunc(bal.last_update_date);
  
    CURSOR cur_get_ccinfo(p_ccid IN NUMBER) IS
      SELECT gcc.segment1 seg1
            ,gcc.segment2 seg2
            ,gcc.segment3 seg3
            ,gcc.segment4 seg4
            ,gcc.segment5 seg5
            ,gcc.segment6 seg6
        FROM apps.gl_code_combinations@r12_to_fin gcc
       WHERE gcc.code_combination_id = p_ccid;
  
    v_batch_name VARCHAR2(100);
    v_batch_desc VARCHAR2(240);
    v_entered_cr NUMBER;
    v_entered_dr NUMBER;
    --
    v_seg1 VARCHAR2(150);
    v_seg2 VARCHAR2(150);
    v_seg3 VARCHAR2(150);
    v_seg4 VARCHAR2(150);
    v_seg5 VARCHAR2(150);
    v_seg6 VARCHAR2(150);
    --
    v_first_period VARCHAR2(30);
    --
    p_ytd_ptd VARCHAR2(30) := 'PTD';
    --
    v_insert NUMBER;
  BEGIN
    v_batch_name := 'Summary Load: ' ||
                    to_char(SYSDATE, 'RRRR-DD-MM HH24:MI:SS');
    v_batch_desc := v_batch_name;
  
    --   gl_get_first_period(tset_of_books_id => p_sob_id,
    --                       tperiod_name     => p_period,
    --                       tfirst_period    => v_first_period,
    --                       errbuf           => p_errbuf);
  
    FOR lp_data IN cur_data(v_first_period)
    LOOP
      v_entered_dr := 0;
      v_entered_cr := 0;
      --
      v_seg1 := NULL;
      v_seg2 := NULL;
      v_seg3 := NULL;
      v_seg4 := NULL;
      v_seg5 := NULL;
      v_seg6 := NULL;
      --
    
      OPEN cur_get_ccinfo(lp_data.ccid);
      FETCH cur_get_ccinfo
        INTO v_seg1, v_seg2, v_seg3, v_seg4, v_seg5, v_seg6;
      CLOSE cur_get_ccinfo;
      IF p_ytd_ptd = 'PTD'
      THEN
        IF sign(lp_data.amount) < 0
        THEN
          v_entered_cr := abs(lp_data.amount);
        ELSE
          v_entered_dr := lp_data.amount;
        END IF;
      END IF;
    
      v_insert := -1;
    
      IF nvl(p_segment1, v_seg1) = v_seg1
      THEN
        v_insert := 1;
      END IF;
    
      IF v_insert > 0
      THEN
        insert_data(p_batch_name        => v_batch_name
                   ,p_period            => p_period
                   ,p_batch_description => v_batch_desc
                   ,p_reversal_flag     => NULL
                   ,p_reversal_period   => NULL
                   ,p_reversal_type     => NULL
                   ,p_currency_code     => lp_data.currency_code
                   ,p_seg1              => v_seg1
                   ,p_seg2              => v_seg2
                   ,p_seg3              => v_seg3
                   ,p_seg4              => v_seg4
                   ,p_seg5              => v_seg5
                   ,p_seg6              => v_seg6
                   ,p_usr_category      => lp_data.usr_category_name
                   ,p_usr_source        => lp_data.user_je_source_name
                   ,p_conv_rate         => lp_data.cur_conv_rate
                   ,p_entered_dr        => v_entered_dr
                   ,p_entered_cr        => v_entered_cr
                   ,p_acctd_dr          => NULL
                   ,p_acctd_cr          => NULL
                   ,p_transaction_date  => lp_data.trx_date
                   ,p_je_name           => NULL
                   ,p_je_description    => NULL
                   ,p_line_description  => NULL
                   ,p_line_ref1         => NULL
                   ,p_line_ref2         => NULL
                   ,p_line_ref3         => NULL
                   ,p_line_ref4         => NULL
                   ,p_line_ref5         => NULL
                   ,p_line_ref6         => NULL
                   ,p_line_ref7         => NULL
                   ,p_line_ref8         => NULL
                   ,p_line_ref9         => NULL
                   ,p_line_ref10        => NULL
                   ,p_line_num          => 1
                   ,p_line_attribute1   => NULL
                   ,p_line_attribute2   => NULL
                   ,p_line_attribute3   => NULL
                   ,p_line_attribute4   => NULL
                   ,p_line_attribute5   => NULL
                   ,p_line_context      => NULL);
      END IF;
    END LOOP;
  END gl_summary_load;
  
  PROCEDURE gl_detail_load(p_sob_id IN NUMBER, p_period IN VARCHAR2) IS
  
    CURSOR cur_grp_by_src IS
      SELECT DISTINCT src.je_source_name      je_source_name
                     ,src.user_je_source_name user_je_source_name
        FROM gl.gl_je_headers@r12_to_fin      hed
            ,apps.gl_je_sources_vl@r12_to_fin src
       WHERE hed.je_source = src.je_source_name
         AND hed.set_of_books_id = p_sob_id
         AND hed.period_name = p_period;
  
    CURSOR cur_data(p_src_name IN VARCHAR2) IS
      SELECT bat.name                         batch_name
            ,bat.description                  batch_description
            ,gcc.segment1                     seg1
            ,gcc.segment2                     seg2
            ,gcc.segment3                     seg3
            ,gcc.segment4                     seg4
            ,gcc.segment5                     seg5
            ,gcc.segment6                     seg6
            ,hed.accrual_rev_flag             reversal_flag
            ,hed.accrual_rev_period_name      reversal_period
            ,hed.accrual_rev_change_sign_flag reversal_type
            ,hed.currency_code                currency_code
            ,cat.user_je_category_name        usr_category_name
            ,hed.currency_conversion_rate     cur_conv_rate
            ,lin.entered_dr                   entered_dr
            ,lin.entered_cr                   entered_cr
            ,lin.accounted_dr                 accounted_dr
            ,lin.accounted_cr                 accounted_cr
            ,hed.date_created                 trx_date
            ,hed.name                         je_head_name
            ,hed.description                  je_head_desc
            ,lin.description                  line_description
            ,lin.reference_1                  line_ref1
            ,lin.reference_2                  line_ref2
            ,lin.reference_3                  line_ref3
            ,lin.reference_4                  line_ref4
            ,lin.reference_5                  line_ref5
            ,lin.reference_6                  line_ref6
            ,lin.reference_7                  line_ref7
            ,lin.reference_8                  line_ref8
            ,lin.reference_9                  line_ref9
            ,lin.reference_10                 line_ref10
            ,lin.je_line_num                  line_num
            ,lin.attribute1                   line_attribute1
            ,lin.attribute2                   line_attribute2
            ,lin.attribute3                   line_attribute3
            ,lin.attribute4                   line_attribute4
            ,lin.attribute5                   line_attribute5
            ,lin.context                      line_context
        FROM gl.gl_je_headers@r12_to_fin              hed
            ,gl.gl_je_lines@r12_to_fin                lin
            ,gl.gl_je_batches@r12_to_fin              bat
            ,apps.gl_je_categories_vl@r12_to_fin      cat
            ,apps.gl_code_combinations_kfv@r12_to_fin gcc
       WHERE hed.je_header_id = lin.je_header_id
         AND hed.je_batch_id = bat.je_batch_id(+)
         AND hed.je_category = cat.je_category_name
         AND gcc.code_combination_id = lin.code_combination_id
         AND hed.je_source = p_src_name
         AND hed.set_of_books_id = p_sob_id
         AND hed.period_name = p_period
         AND hed.actual_flag = 'A';
         
    --
    v_batch_name VARCHAR2(100);
    v_batch_desc VARCHAR2(240);
    v_acct_date DATE;
    --
  BEGIN
    SELECT end_date
    into v_acct_date
        FROM gl.gl_periods
       WHERE period_set_name = g_ldgr_per_set
         AND period_name = p_period;
  
    FOR lp_grp_src IN cur_grp_by_src
    LOOP
      v_batch_name := 'REL11i Conv. for Source: ' || lp_grp_src.je_source_name ||
                      to_char(SYSDATE, 'RRRR-DD-MM HH24:MI:SS');
      v_batch_desc := '11i to 12i GL Detail Conversion for source ' ||
                      lp_grp_src.je_source_name;
      FOR lp_data IN cur_data(lp_grp_src.je_source_name)
      LOOP
        insert_data(p_batch_name        => nvl(lp_data.batch_name, v_batch_name)
                   ,p_batch_description => nvl(lp_data.batch_description
                                              ,v_batch_desc)
                   ,p_period            => p_period
                   ,p_reversal_flag     => lp_data.reversal_flag
                   ,p_reversal_period   => lp_data.reversal_period
                   ,p_reversal_type     => lp_data.reversal_type
                   ,p_currency_code     => lp_data.currency_code
                   ,p_seg1              => lp_data.seg1
                   ,p_seg2              => lp_data.seg2
                   ,p_seg3              => lp_data.seg3
                   ,p_seg4              => lp_data.seg4
                   ,p_seg5              => lp_data.seg5
                   ,p_seg6              => lp_data.seg6
                   ,p_usr_category      => lp_data.usr_category_name
                   ,p_usr_source        => lp_grp_src.user_je_source_name
                   ,p_conv_rate         => lp_data.cur_conv_rate
                   ,p_entered_dr        => lp_data.entered_dr
                   ,p_entered_cr        => lp_data.entered_cr
                   ,p_acctd_dr          => lp_data.accounted_dr
                   ,p_acctd_cr          => lp_data.accounted_cr
                   ,p_transaction_date  => lp_data.trx_date
                   ,p_je_name           => lp_data.je_head_name
                   ,p_je_description    => lp_data.je_head_desc
                   ,p_line_description  => lp_data.line_description
                   ,p_line_ref1         => lp_data.line_ref1
                   ,p_line_ref2         => lp_data.line_ref2
                   ,p_line_ref3         => lp_data.line_ref3
                   ,p_line_ref4         => lp_data.line_ref4
                   ,p_line_ref5         => lp_data.line_ref5
                   ,p_line_ref6         => lp_data.line_ref6
                   ,p_line_ref7         => lp_data.line_ref7
                   ,p_line_ref8         => lp_data.line_ref8
                   ,p_line_ref9         => lp_data.line_ref9
                   ,p_line_ref10        => lp_data.line_ref10
                   ,p_line_num          => lp_data.line_num
                   ,p_line_attribute1   => lp_data.line_attribute1
                   ,p_line_attribute2   => lp_data.line_attribute2
                   ,p_line_attribute3   => lp_data.line_attribute3
                   ,p_line_attribute4   => lp_data.line_attribute4
                   ,p_line_attribute5   => lp_data.line_attribute5
                   ,p_line_context      => lp_data.line_context);

      END LOOP;
    END LOOP;
    
    
  END gl_detail_load;
  
  PROCEDURE gl_interface(p_errbuf    OUT VARCHAR2
                        ,p_retcode   OUT VARCHAR2
                        ,p_sob       IN VARCHAR2
                        ,p_period    IN VARCHAR2
                        ,p_summ_detl IN VARCHAR2
                        ,p_segment1  IN VARCHAR2) IS
  
    CURSOR cur_sob_info IS
      SELECT set_of_books_id, period_set_name, currency_code
        FROM gl.gl_sets_of_books@r12_to_fin
       WHERE NAME = p_sob;
  
    CURSOR cur_get_period(p_period_set IN VARCHAR2, p_period_date IN DATE) IS
      SELECT period_name
        FROM gl.gl_periods
       WHERE p_period_date BETWEEN start_date AND end_date
         AND period_set_name = p_period_set;
  
    CURSOR cur_sob_mapping IS
      SELECT description
        FROM applsys.fnd_lookup_values
       WHERE lookup_type = 'HDS_GL_SOB_MAPPING'
         AND enabled_flag = 'Y'
         AND meaning = p_sob
         AND SYSDATE BETWEEN start_date_active AND
             nvl(end_date_active, SYSDATE + 1);
  
    CURSOR cur_get_ledger_info(p_ledger_book IN VARCHAR2) IS
      SELECT ledger_id, currency_code, period_set_name
        FROM gl.gl_ledgers
       WHERE NAME = p_ledger_book;
  
    v_ledger_book VARCHAR2(30);
    v_sob_id      NUMBER;
    v_period_set  VARCHAR2(30);
    v_period      VARCHAR2(15);
    v_period_date DATE;
    v_curr_code   VARCHAR2(15);
  BEGIN
    BEGIN
    v_ledger_book := NULL;
    v_sob_id      := NULL;
    v_period_set  := NULL;
    v_period      := NULL;
    v_period_date := NULL;
    --
    OPEN cur_sob_mapping;
    FETCH cur_sob_mapping
      INTO v_ledger_book;
    IF cur_sob_mapping%NOTFOUND
    THEN
      print_log('Verify if Lookup ''HDS_GL_SOB_MAPPING'' Exists:');
      print_log('OR Check if mapping exists for 11i SOB: ' || p_sob);
    ELSE
      OPEN cur_get_ledger_info(v_ledger_book);
      FETCH cur_get_ledger_info
        INTO g_ldgr_book_id, g_ldgr_cur_code, g_ldgr_per_set;
      IF cur_get_ledger_info%NOTFOUND
      THEN
        print_log('SOB Mapping to Ledger Books Not Found: ' || v_ledger_book);
      ELSE
        OPEN cur_sob_info;
        FETCH cur_sob_info
          INTO v_sob_id, v_period_set, v_curr_code;
        IF cur_sob_info%NOTFOUND
        THEN
          print_log('SOB Name ' || p_sob ||
                    ' defined in the lookup type HDS_GL_SOB_MAPPING is incorrect.');
        ELSE
          --
          -- Assumtion being that the Period Name is always going to be
          -- in the format of 'MON-YYYY' in both 11i and 12i
          -------------------------------------------------------------
          -- BEGIN
          --    v_period_date := TO_DATE('15-'||UPPER(p_period), 'DD-MON-RRRR');
          -- EXCEPTION
          --    WHEN OTHERS THEN
          --       print_log('Error Converting V_PERIOD_DATE: '||'15-'||UPPER(p_period));
          -- END;
          -----------------------------------------------------------
          --
          -- IF v_period_date IS NOT NULL THEN
          --    OPEN cur_get_period(v_period_set,
          --                        v_period_date);
          --       FETCH cur_get_period
          --          INTO v_period;
          --    CLOSE cur_get_period;
          v_period := initcap(p_period);
        
          IF p_summ_detl = 'DETAIL'
          THEN
            gl_detail_load(p_sob_id => v_sob_id, p_period => v_period);
          ELSE
            gl_summary_load(p_sob_id    => v_sob_id
                           ,p_period    => v_period
                           ,p_curr_code => v_curr_code
                           ,p_segment1  => p_segment1
                           ,p_errbuf    => p_errbuf);
          END IF;
          -- END IF;
        END IF;
        CLOSE cur_sob_info;
      
      END IF;
      CLOSE cur_get_ledger_info;
    END IF;
    CLOSE cur_sob_mapping;
    
    END;
    --
    --
        
       journal_import (n_retcode => p_retcode,
                       c_errbuf  => p_errbuf);
                       
                          
  END gl_interface;

END xxcusgl_interface_pkg;
/
