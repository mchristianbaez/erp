CREATE OR REPLACE PACKAGE BODY APPS.XXWC_CREDIT_REPORT_PKG
AS
   /*************************************************************************
     $Header xxwc_credit_report_pkg $
     Module Name: xxwc_credit_report_pkg.pkb

     PURPOSE:   This package is to derive Open Balance information for Customers

     REVISIONS:
     Ver        Date        Author             Description
     ---------  ----------  ---------------    -------------------------
     1.0        01/11/2012  Gopi Damuluri      Initial Version
     1.1        02/01/2014  Gopi Damuluri      Added the procedure - CALL_CONC_PROG
	 1.2	    9/17/2014   Pattabhi Avula     Modified code(Removed _all) for TMS#20141001-00031 OU Test
   **************************************************************************/

      g_req_id          NUMBER        := fnd_global.conc_request_id;
      g_err_callfrom    VARCHAR2 (75) DEFAULT 'XXWC_CREDIT_REPORT_PKG';
      g_distro_list     VARCHAR2 (75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';

   PROCEDURE create_credit_bureau_file (p_errbuf OUT VARCHAR2, p_retcode OUT VARCHAR2)
   IS
      l_sec        VARCHAR2(2000);
   BEGIN
     l_sec := 'START';

    /* --------------------------------------------------------------------
     -- Setting the Context 
     --------------------------------------------------------------------
     mo_global.init('AR');
     fnd_global.apps_initialize ( user_id      => 1296
                                , resp_id      => 20678
                                , resp_appl_id => 222);

     mo_global.set_policy_context('S',162);
     fnd_global.set_nls_context('AMERICAN');  */ -- 26/09/2014 Commented by pattabhi for TMS#20141001-00031 & US OU Testing 
    
     --------------------------------------------------------------------
     -- Truncate Temporary Tables
     --------------------------------------------------------------------
     EXECUTE IMMEDIATE 'TRUNCATE TABLE xxwc.xxwc_cb_ps_age_gtt_tbl';
     EXECUTE IMMEDIATE 'TRUNCATE TABLE xxwc.xxwc_cb_ps_gtt_tbl';
     EXECUTE IMMEDIATE 'TRUNCATE TABLE xxwc.xxwc_cb_cust_gtt_tbl';
     EXECUTE IMMEDIATE 'TRUNCATE TABLE xxwc.xxwc_cb_ps_bal_gtt_tbl';
     EXECUTE IMMEDIATE 'TRUNCATE TABLE xxwc.xxwc_cb_ps_bkt_gtt_tbl';
     EXECUTE IMMEDIATE 'TRUNCATE TABLE xxwc.xxwc_cb_addr_gtt_tbl';
     EXECUTE IMMEDIATE 'TRUNCATE TABLE xxwc.xxwc_cb_final_tbl';

     --------------------------------------------------------------------
     -- 1. Derive all the Open Transactions
     --------------------------------------------------------------------
     l_sec := '1. Insert into XXWC_CB_PS_AGE_GTT_TBL';
     fnd_file.put_line (fnd_file.LOG,'l_sec - '||l_sec||'Date - '||to_char(SYSDATE,'DD-MON-YYYY HH24:MI:SS'));

      INSERT INTO xxwc.xxwc_cb_ps_age_gtt_tbl(customer_id
                                            , age
                                            , amount_due_remaining
                                            , dup_flag)
                          SELECT apsa.customer_id
                               , TRUNC(SYSDATE) - TRUNC (apsa.due_date)
                               , apsa.amount_due_remaining
                               , '1'
                            FROM AR_PAYMENT_SCHEDULES  apsa
                           WHERE 1 = 1 
                             AND apsa.status = 'OP'
                             AND apsa.amount_due_remaining <> 0;  
                           --  AND apsa.customer_trx_id IS NOT NULL
                           --  AND apsa.org_id = 162;  -- 26/09/2014 Commented by Pattabhi for TMS#20141001-00031 Canada OU Testing 

     --------------------------------------------------------------------
     -- 2. Derive distinct customers with Open Balances
     --------------------------------------------------------------------
      l_sec := '2. Insert into XXWC_CB_PS_GTT_TBL';
      fnd_file.put_line (fnd_file.LOG,'l_sec - '||l_sec||'Date - '||to_char(SYSDATE,'DD-MON-YYYY HH24:MI:SS'));
      COMMIT;

      INSERT INTO xxwc.xxwc_cb_ps_gtt_tbl( customer_id
                                         , dup_flag)
                         SELECT DISTINCT customer_id
                              , '2'
                           FROM xxwc.xxwc_cb_ps_age_gtt_tbl;

     --------------------------------------------------------------------
     -- 3. Derive Customers information
     --------------------------------------------------------------------
      l_sec := '3. Insert into XXWC_CB_CUST_GTT_TBL';
      fnd_file.put_line (fnd_file.LOG,'l_sec - '||l_sec||'Date - '||to_char(SYSDATE,'DD-MON-YYYY HH24:MI:SS'));
      COMMIT;

      INSERT INTO xxwc.xxwc_cb_cust_gtt_tbl (customer_id
                                           , customer_number
                                           , party_id
                                           , customer_name
                                           , cust_acct_site_id
                                           , party_site_id
                                           , site_use_id
                                           , terms
                                           , org_id)
                                      SELECT hca.cust_account_id
                                           , hca.account_number
                                           , hca.party_id
                                           , hp.party_name
                                           , hcasa.cust_acct_site_id
                                           , hcasa.party_site_id
                                           , hcsua.site_use_id
                                           , ( SELECT xxwc_mv_routines_pkg.get_ar_term_desc (NVL (hcp.standard_terms, hcsua.payment_term_id))
                                                 FROM ar.hz_customer_profiles hcp
                                                WHERE 1 = 1
                                                  AND hcp.cust_account_id = hca.cust_account_id
                                                  AND hcp.site_use_id IS NULL
                                             ) terms
                                           , hcasa.org_id
                                        FROM apps.hz_cust_accounts         hca
                                           , apps.hz_cust_acct_sites    hcasa
                                           , apps.hz_cust_site_uses     hcsua  
                                           , xxwc.xxwc_cb_ps_gtt_tbl      gtt
                                           , hz_parties                   hp
                                      WHERE 1 = 1
                                        AND hca.cust_account_id     = gtt.customer_id
                                        AND hp.party_id             = hca.party_id
                                        AND hcasa.cust_account_id   = hca.cust_account_id
                                        AND hcsua.cust_acct_site_id = hcasa.cust_acct_site_id
                                       -- AND hcasa.org_id            = 162
                                       -- AND hcsua.org_id            = 162
                                        AND hcsua.site_use_code     = 'BILL_TO'  
                                        AND hcsua.primary_flag      = 'Y';

     --------------------------------------------------------------------
     -- 4. Derive Customer Address information
     --------------------------------------------------------------------
      l_sec := '4. Insert into XXWC_CB_ADDR_GTT_TBL';
      fnd_file.put_line (fnd_file.LOG,'l_sec - '||l_sec||'Date - '||to_char(SYSDATE,'DD-MON-YYYY HH24:MI:SS'));
      COMMIT;

      INSERT INTO xxwc.xxwc_cb_addr_gtt_tbl(org_id
                                     , customer_id
                                     , party_id
                                     , customer_number
                                     , customer_name
                                     , customer_address_1
                                     , customer_address_2
                                     , customer_address_3
                                     , customer_address_4
                                     , terms
                                     , site_use_id)
                SELECT gtt.org_id
                     , gtt.customer_id
                     , gtt.party_id
                     , gtt.customer_number
                     , gtt.customer_name
                     , hl.address1                              customer_address_1
                     , (hl.address2 || ' ' || hl.address3)      customer_address_2
                     , (hl.city || ', ' || hl.state)            customer_address_3
                     ,  hl.postal_code                          customer_address_4
                     , gtt.terms
                     , gtt.site_use_id
                  FROM apps.hz_party_sites            hps
                     , apps.hz_locations              hl
                     , xxwc.xxwc_cb_cust_gtt_tbl    gtt
                 WHERE 1 = 1
                   AND hps.party_site_id = gtt.party_site_id
                   AND hps.location_id = hl.location_id;

     --------------------------------------------------------------------
     -- 5. Derive Customer Credit Analyst
     --------------------------------------------------------------------
      l_sec := '5. Update XXWC_CB_ADDR_GTT_TBL';
      fnd_file.put_line (fnd_file.LOG,'l_sec - '||l_sec||'Date - '||to_char(SYSDATE,'DD-MON-YYYY HH24:MI:SS'));
      COMMIT;
      
              UPDATE xxwc.xxwc_cb_addr_gtt_tbl gtt
                 SET credit_analyst = (SELECT c.resource_name
                                         FROM jtf_rs_role_relations        a
                                            , jtf_rs_roles_vl              b
                                            , jtf_rs_resource_extns_vl     c
                                            , hz_customer_profiles         hcp
                                        WHERE a.role_resource_type       = 'RS_INDIVIDUAL'
                                          AND a.role_resource_id         = c.resource_id
                                          AND a.role_id                  = b.role_id
                                          AND b.role_code                = 'CREDIT_ANALYST'
                                          AND c.category                 = 'EMPLOYEE'
                                          AND NVL(a.delete_flag, 'N')   <> 'Y'
                                          AND c.resource_id              = hcp.credit_analyst_id
                                          AND hcp.cust_account_id        = gtt.customer_id
                                          AND hcp.site_use_id            = gtt.site_use_id
                                          AND ROWNUM                     = 1);

     --------------------------------------------------------------------
     -- 6. Derive Customer Balance Information
     --------------------------------------------------------------------
     l_sec := '6. Insert into XXWC_CB_PS_BAL_GTT_TBL';
     fnd_file.put_line (fnd_file.LOG,'l_sec - '||l_sec||'Date - '||to_char(SYSDATE,'DD-MON-YYYY HH24:MI:SS'));
      COMMIT;
     
     
        INSERT INTO xxwc.xxwc_cb_ps_bal_gtt_tbl (customer_id
                                               , bucket
                                               , balance)
        SELECT DISTINCT customer_id
             , descr
             , SUM(amount_due_remaining) OVER (PARTITION BY customer_id, descr) balance
          FROM xxwc.xxwc_cb_ps_age_gtt_tbl
          INNER JOIN (SELECT 'current'   descr, -9999 rng_start,  0    rng_stop FROM dual
               UNION (SELECT '1_30'      descr, 1    rng_start,  30    rng_stop FROM dual)
               UNION (SELECT '31_60'     descr, 31   rng_start,  60    rng_stop FROM dual) 
               UNION (SELECT '61_90'     descr, 61   rng_start,  90    rng_stop FROM dual) 
               UNION (SELECT '91_plus'   descr, 91   rng_start,  9999  rng_stop FROM dual) 
               UNION (SELECT '31_plus'   descr, 31   rng_start,  9999  rng_stop FROM dual)  
               UNION (SELECT 'all'       descr, -9999    rng_start,  9999  rng_stop FROM dual)) ON age BETWEEN nvl(rng_start, age) AND nvl(rng_stop, age)
         WHERE 1 =1
        --  and customer_id = 45690
        -- ORDER BY customer_id
         ;

       --------------------------------------------------------------------
       -- 7. Align Customer Balance Information into Buckets
       --------------------------------------------------------------------
       l_sec := '7. Insert into XXWC_CB_PS_BKT_GTT_TBL';
       fnd_file.put_line (fnd_file.LOG,'l_sec - '||l_sec||'Date - '||to_char(SYSDATE,'DD-MON-YYYY HH24:MI:SS'));
       COMMIT;

       DBMS_STATS.gather_index_stats (ownname => 'XXWC', indname => 'XXWC_CB_PS_BAL_GTT_N1');
       DBMS_STATS.gather_index_stats (ownname => 'XXWC', indname => 'XXWC_CB_PS_BAL_GTT_N2');

       INSERT INTO  xxwc.xxwc_cb_ps_bkt_gtt_tbl (customer_id 
                                               , "current"
                                               , "1_30"        
                                               , "31_60"       
                                               , "61_90"       
                                               , "91_plus"     
                                               , "31_plus"     
                                               , "all"         
                                               )
             SELECT gtt.customer_id
                  , (select gtt_1.balance from xxwc.xxwc_cb_ps_bal_gtt_tbl gtt_1 where gtt_1.customer_id = gtt.customer_id and gtt_1.bucket = 'current')
                  , (select gtt_1.balance from xxwc.xxwc_cb_ps_bal_gtt_tbl gtt_1 where gtt_1.customer_id = gtt.customer_id and gtt_1.bucket = '1_30')
                  , (select gtt_1.balance from xxwc.xxwc_cb_ps_bal_gtt_tbl gtt_1 where gtt_1.customer_id = gtt.customer_id and gtt_1.bucket = '31_60')
                  , (select gtt_1.balance from xxwc.xxwc_cb_ps_bal_gtt_tbl gtt_1 where gtt_1.customer_id = gtt.customer_id and gtt_1.bucket = '61_90')
                  , (select gtt_1.balance from xxwc.xxwc_cb_ps_bal_gtt_tbl gtt_1 where gtt_1.customer_id = gtt.customer_id and gtt_1.bucket = '91_plus')
                  , (select gtt_1.balance from xxwc.xxwc_cb_ps_bal_gtt_tbl gtt_1 where gtt_1.customer_id = gtt.customer_id and gtt_1.bucket = '31_plus')
                  , gtt.balance
               FROM xxwc.xxwc_cb_ps_bal_gtt_tbl gtt
              WHERE 1 = 1
                AND gtt.bucket = 'all';

       --------------------------------------------------------------------
       -- 8. Combine the above derived information into CreditFile format
       --------------------------------------------------------------------
       l_sec := '8. Insert into XXWC_CB_FINAL_TBL';
       fnd_file.put_line (fnd_file.LOG,'l_sec - '||l_sec||'Date - '||to_char(SYSDATE,'DD-MON-YYYY HH24:MI:SS'));
       COMMIT;

       DBMS_STATS.gather_index_stats (ownname => 'XXWC', indname => 'XXWC_CB_ADDR_GTT_N1');
       DBMS_STATS.gather_index_stats (ownname => 'XXWC', indname => 'XXWC_CB_PS_BKT_GTT_N1');

        INSERT INTO XXWC.XXWC_CB_FINAL_TBL(  ORG_ID 
        ,  CUSTOMER_NUMBER      
        ,  CUSTOMER_NAME        
        ,  CUSTOMER_ADDRESS_1   
        ,  CUSTOMER_ADDRESS_2   
        ,  CUSTOMER_ADDRESS_3   
        ,  CUSTOMER_ADDRESS_4   
        ,  TELEPHONE_NUMBER     
        ,  TERMS                
        ,  PAYMENT_EXPERIENCE   
        ,  DAYS_SLOW            
        ,  DATE_OF_LAST_INVOICE 
        ,  HIGH_CREDIT_DATE     
        ,  AVERAGE_DAYS_TO_PAY  
        ,  HIGH_CREDIT          
        ,  AGING_CATEGORY_1     
        ,  AGING_CATEGORY_2     
        ,  AGING_CATEGORY_3     
        ,  AGING_CATEGORY_4     
        ,  AGING_CATEGORY_5     
        ,  TOTAL_AMT_31_OR_MORE 
        ,  TOTAL_AR             
        ,  BRANCH_NUMBER        
        ,  CREDIT_MANAGER_NAME  
        ,  LINE_OF_BUSINESS     
        ,  TOTAL_SALES          
        )
          SELECT addr.org_id
               , addr.customer_number 
               , addr.customer_name
               , addr.customer_address_1
               , addr.customer_address_2
               , addr.customer_address_3
               , addr.customer_address_4
               , xxwc_mv_routines_pkg.get_phone_fax_number ('PHONE' ,addr.party_id ,NULL ,'GEN')    telephone_number
               , addr.terms
               , NULL payment_experience
               , NULL days_slow                                                        
               , xxwc_mv_routines_pkg.get_last_ar_trx_date (addr.customer_id)      date_of_last_invoice
               , xxwc_mv_routines_pkg.get_high_credit_date_ytd (addr.customer_id)  high_credit_date 
               , xxwc_mv_routines_pkg.get_avg_days_late (addr.customer_id)         average_days_to_pay
               , xxwc_mv_routines_pkg.get_high_credit_ytd ( addr.customer_id)      high_credit
               , ps_bkt."current"                                                  aging_category_1
               , ps_bkt."1_30"                                                     aging_category_2
               , ps_bkt."31_60"                                                    aging_category_3
               , ps_bkt."61_90"                                                    aging_category_4
               , ps_bkt."91_plus"                                                  aging_category_5
               , ps_bkt."31_plus"                                                  total_amt_31_or_more
               , ps_bkt."all"                                                      total_ar
               , NULL                                                              branch_number
               , addr.credit_analyst                                               credit_manager_name
               , 'White Cap'                                                       line_of_business
               , total_sales(ps_bkt.customer_id)                                   total_sales
            FROM xxwc.xxwc_cb_addr_gtt_tbl     addr
               , xxwc.xxwc_cb_ps_bkt_gtt_tbl   ps_bkt
           WHERE 1 = 1
             AND ps_bkt.customer_id     = addr.customer_id;

      COMMIT;
      fnd_file.put_line (fnd_file.LOG,'l_sec - final '||l_sec||'Date - '||to_char(SYSDATE,'DD-MON-YYYY HH24:MI:SS'));

   EXCEPTION
   WHEN OTHERS THEN
     fnd_file.put_line (fnd_file.LOG,'l_sec - '||l_sec);
     fnd_file.put_line (fnd_file.LOG, 'Err - '||SQLERRM);

      xxcus_error_pkg.xxcus_error_main_api
         (p_called_from            => 'XXWC_CREDIT_REPORT_PKG.CREATE_CREDIT_BUREAU_FILE',
          p_calling                => l_sec,
          p_request_id             => g_req_id,
          p_ora_error_msg          => SUBSTR (SQLERRM, 1, 2000),
          p_error_desc             => 'Error running XXWC_CREDIT_REPORT_PKG with PROGRAM ERROR',
          p_distribution_list      => g_distro_list,
          p_module                 => 'AR'
         );

   END create_credit_bureau_file;

   FUNCTION total_sales(p_customer_id  IN NUMBER) RETURN NUMBER
      IS
      l_sec               VARCHAR2(2000);
      l_cust_sales        NUMBER := 0;
   BEGIN
      SELECT SUM (ps.amount_due_original
                 + NVL (
                       (SELECT SUM (NVL (a.acctd_amount, 0))
                          FROM ar_adjustments a
                         WHERE     a.payment_schedule_id = ps.payment_schedule_id
                               AND a.status = 'A'
                               AND a.gl_date <= TRUNC (SYSDATE))
                      ,0))
        INTO l_cust_sales
        FROM ar_payment_schedules ps
       WHERE     ps.class IN ('INV', 'DM', 'CB', 'DEP')
             AND ps.payment_schedule_id <> -1
             AND ps.gl_date BETWEEN TRUNC (ADD_MONTHS (SYSDATE, -12)) AND TRUNC (SYSDATE)
        AND ps.customer_id = p_customer_id;

      RETURN l_cust_sales;
     
   EXCEPTION
   WHEN OTHERS THEN
     fnd_file.put_line (fnd_file.LOG,'l_sec - '||l_sec);
     fnd_file.put_line (fnd_file.LOG, 'Err - '||SQLERRM);
     l_cust_sales := 0;

      xxcus_error_pkg.xxcus_error_main_api
         (p_called_from            => 'XXWC_CREDIT_REPORT_PKG.TOTAL_SALES',
          p_calling                => l_sec,
          p_request_id             => g_req_id,
          p_ora_error_msg          => SUBSTR (SQLERRM, 1, 2000),
          p_error_desc             => 'Error running XXWC_CREDIT_REPORT_PKG with PROGRAM ERROR',
          p_distribution_list      => g_distro_list,
          p_module                 => 'AR'
         );
   RETURN l_cust_sales;

   END total_sales;

-- Version# 1.1 > Start
   /********************************************************************************
   ProcedureName : call_conc_prog
   Purpose       : Call Concurrent Program - XXWC Credit Bureau Refresh Process

   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.0     02/20/2014    Gopi Damuluri    Initial creation of the procedure
   ********************************************************************************/
   PROCEDURE call_conc_prog (p_errbuf                OUT VARCHAR2
                           , p_retcode               OUT NUMBER
                           , p_user_name             IN  VARCHAR2
                           , p_resp_name             IN  VARCHAR2)
   IS
      --
      -- Package Variables
      --

      l_req_id                NUMBER        NULL;
      v_phase                 VARCHAR2 (50);
      v_status                VARCHAR2 (50);
      v_dev_status            VARCHAR2 (50);
      v_dev_phase             VARCHAR2 (50);
      v_message               VARCHAR2 (250);
      v_error_message         VARCHAR2 (3000);

      l_sec                   VARCHAR2 (255);
      l_user_id               NUMBER;
      l_responsibility_id     NUMBER;
      l_resp_application_id   NUMBER;

   BEGIN

      l_sec := 'Deriving UserId';
      --------------------------------------------------------------------------
      -- Deriving UserId
      --------------------------------------------------------------------------
      BEGIN
         SELECT user_id
           INTO l_user_id
           FROM fnd_user
          WHERE 1 = 1
            AND user_name = UPPER (p_user_name)
            AND SYSDATE BETWEEN start_date AND NVL (end_date, TRUNC (SYSDATE) + 1);
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
         v_error_message := 'UserName - ' || p_user_name || ' not defined in Oracle';
         RAISE PROGRAM_ERROR;
      WHEN OTHERS THEN
         v_error_message := 'Error deriving user_id for UserName - ' || p_user_name;
         RAISE PROGRAM_ERROR;
      END;

      l_sec := 'Deriving ResponsibilityId and ResponsibilityApplicationId';
      --------------------------------------------------------------------------
      -- Deriving ResponsibilityId and ResponsibilityApplicationId
      --------------------------------------------------------------------------
      BEGIN
        SELECT responsibility_id
             , application_id
          INTO l_responsibility_id
             , l_resp_application_id
          FROM fnd_responsibility_vl
         WHERE responsibility_name = p_resp_name
           AND SYSDATE BETWEEN start_date AND NVL (end_date, TRUNC (SYSDATE) + 1);
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
         v_error_message :=  'Responsibility - '|| p_resp_name|| ' not defined in Oracle';
         RAISE PROGRAM_ERROR;
      WHEN OTHERS THEN
         v_error_message := 'Error deriving Responsibility_id for ResponsibilityName - ' || p_resp_name;
         RAISE PROGRAM_ERROR;
      END;

      l_sec := 'UC4 call to run concurrent request - XXWC Credit Bureau Refresh Process ';
      --------------------------------------------------------------------------
      -- Apps Initialize
      --------------------------------------------------------------------------
      FND_GLOBAL.APPS_INITIALIZE (l_user_id
                                , l_responsibility_id
                                , l_resp_application_id);

      --------------------------------------------------------------------------
      -- Submit "XXWC Credit Bureau Refresh Process"
      --------------------------------------------------------------------------
      l_req_id := fnd_request.submit_request (application   => 'XXWC'
                                            , program       => 'XXWC_CREDIT_BUREAU_REFRESH_PRC'
                                            , description   => NULL
                                            , start_time    => SYSDATE
                                            , sub_request   => FALSE);

      COMMIT;

      IF (l_req_id != 0)
      THEN
         IF fnd_concurrent.wait_for_request (l_req_id
                                           , 6
                                           , 15000
                                           , v_phase
                                           , v_status
                                           , v_dev_phase
                                           , v_dev_status
                                           , v_message)
         THEN
            v_error_message := CHR (10)
               || 'ReqID='
               || l_req_id
               || ' DPhase '
               || v_dev_phase
               || ' DStatus '
               || v_dev_status
               || CHR (10)
               || ' MSG - '
               || v_message;

            -- Error Returned
            IF v_dev_phase != 'COMPLETE' OR v_dev_status != 'NORMAL'
            THEN
               v_error_message := 'An error occured running the xxwc_ob_common_pkg' 
                  || v_error_message
                  || '.';
               fnd_file.put_line (fnd_file.LOG, v_error_message);

               RAISE PROGRAM_ERROR;
            END IF;
         -- Then Success!
         END IF;
      ELSE
         RAISE PROGRAM_ERROR;
      END IF;

      p_retcode := 0;
   EXCEPTION
      WHEN PROGRAM_ERROR
      THEN
         ROLLBACK;

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_OB_COMMON_PKG.CALL_CONC_PROG'
           ,p_calling             => l_sec
           ,p_request_id          => l_req_id
           ,p_ora_error_msg       => SUBSTR (SQLERRM, 1, 2000)
           ,p_error_desc          => 'PROGRAM ERROR'
           ,p_distribution_list   => g_distro_list
           ,p_module              => 'XXCUS');

         p_retcode := 2;
      WHEN OTHERS
      THEN

         fnd_file.put_line (fnd_file.LOG, v_error_message);

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_OB_COMMON_PKG.CALL_CONC_PROG'
           ,p_calling             => l_sec
           ,p_request_id          => l_req_id
           ,p_ora_error_msg       => SUBSTR (SQLERRM, 1, 2000)
           ,p_error_desc          => 'OTHERS Exception'
           ,p_distribution_list   => g_distro_list
           ,p_module              => 'XXCUS');
         p_retcode := 2;
   END call_conc_prog;
-- Version# 1.1 < End
   
END XXWC_CREDIT_REPORT_PKG;