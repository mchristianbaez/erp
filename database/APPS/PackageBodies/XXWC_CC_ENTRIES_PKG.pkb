CREATE OR REPLACE PACKAGE BODY APPS.XXWC_CC_ENTRIES_PKG IS
 /******************************************************************************
      NAME:       XXWC_CC_ENTRIES_PKG

      PURPOSE:    To process records from the XXWC_CC_ENTRY Form to the cycle count API

      Logic:     1) Execute the parameters passed through to the cycle count API
      
      REVISIONS:
      Ver        Date        Author           Description
      ---------  ----------  ---------------  ------------------------------------
      1.0        01-JUL-13   Lee Spitzer       1. Created this package body  TMS Ticket - 20130214-01096 Implement Physical Inventory enhancements 
                                                @SC @FIN / Implement Physical Inventory enhancements ...
      1.1        16-Nov-16   P.Vamshidhar     TMS#20160303-00105 - Error handling message fix.                                                 
      
   ******************************************************************************/
/******************************************************************************
    PROCEDURE process_api 
    Parameters 
    (p_cycle_count_header_id  IN NUMBER     Cycle Count Header ID
    ,p_cycle_count_entry_id   IN NUMBER     Cycle Count Entry Id
    ,p_count_list_sequence    IN NUMBER     Cycle Count List Sequence
    ,p_organization_id        IN NUMBER     Organization ID
    ,p_inventory_item_id      IN NUMBER     Inventory Item Id
    ,p_revision               IN VARCHAR2   Item Revision
    ,p_count_quantity         IN NUMBER     Cycle Count Quantity
    ,p_count_uom              IN VARCHAR2   Cycle Count Unit of Measure Code
    ,p_subinventory           IN VARCHAR2   Subinventory
    ,p_lot_number             IN VARCHAR2   Lot Number
    ,p_locator_id             IN NUMBER     Stock Locator Id
    ,p_cost_group_id          IN NUMBER     Cost Group Id
    ,p_user_id                IN NUMBER     User ID
    ,p_employee_id            IN NUMBER     Employee ID
    ,p_date                   IN DATE       Count Date
    ,x_interface_id           OUT NUMBER    Interface ID created and inserted into MTL_CC_ENTRIES_INTERFACE
    ,x_status                 OUT VARCHAR2  Return Status - S Means Success, E - Error, U - User-Defined
    ,x_error_code             OUT VARCHAR2  Error Code returned from the API
    ,x_msg_count              OUT NUMBER    Message Count returned from the API
    ,x_msg                    OUT VARCHAR2  Return Message from the API
   ******************************************************************************/

  PROCEDURE process_api 
    (p_cycle_count_header_id  IN NUMBER
    ,p_cycle_count_entry_id   IN NUMBER
    ,p_count_list_sequence    IN NUMBER
    ,p_organization_id        IN NUMBER
    ,p_inventory_item_id      IN NUMBER
    ,p_revision               IN VARCHAR2
    ,p_count_quantity         IN NUMBER
    ,p_count_uom              IN VARCHAR2
    ,p_subinventory           IN VARCHAR2
    ,p_lot_number             IN VARCHAR2
    ,p_locator_id             IN NUMBER
    ,p_cost_group_id          IN NUMBER
    ,p_user_id                IN NUMBER
    ,p_employee_id            IN NUMBER
    ,p_date                   IN DATE
    ,x_interface_id           OUT NUMBER
    ,x_status                 OUT VARCHAR2
    ,x_error_code             OUT VARCHAR2
    ,x_msg_count              OUT NUMBER
    ,x_msg                    OUT VARCHAR2
    ) IS
  
    lrec mtl_cceoi_var_pvt.inv_cceoi_type; -- cycle counting record. 
    i                         NUMBER;
    j                         NUMBER;
    l_msg_data                VARCHAR2(2000);
    l_ss_qty                  NUMBER;
  BEGIN
  
    --Constants
    lrec.PROCESS_MODE           := 1 ; --1 Means On-Line Procesing
    lrec.ACTION_CODE            := MTL_CCEOI_VAR_PVT.G_PROCESS;
    lrec.delete_flag            := 1;  --1 Means Delete Records
    
    --Variables
    lrec.cycle_count_header_id  := p_cycle_count_header_id;
    lrec.cycle_count_entry_id   := p_cycle_count_entry_id;
    lrec.count_list_sequence    := p_count_list_sequence;
    lrec.count_uom              := p_count_uom;
    lrec.primary_uom_quantity   := p_count_quantity;
    lrec.subinventory           := p_subinventory;
    lrec.cost_group_id          := p_cost_group_id;
    lrec.inventory_item_id      := p_inventory_item_id;
    lrec.organization_id        := p_organization_id;
    lrec.lot_number             := p_lot_number;
    lrec.revision               := p_revision;
    lrec.locator_id             := p_locator_id;
    lrec.count_date             := p_date;
    lrec.employee_id            := p_employee_id;
    lrec.created_by             := p_user_id;
    lrec.last_updated_by        := p_user_id;
    
    --Getting Snap Shot On-Hand Quantity from custom snap shot On-Hand Table when records where inserted when the cycle count record was created 
    
        BEGIN
        
                SELECT sum(primary_transaction_quantity)
                INTO   l_ss_qty
                FROM   XXWC.XXWC_CC_QUANTITIES_DETAIL_TBL
                WHERE  inventory_item_id = p_inventory_item_id
                AND    organization_id =     p_organization_id
                AND    cycle_count_header_id = p_cycle_count_header_id
                AND    cycle_count_entry_id = p_cycle_count_entry_id
                AND    subinventory_code = p_subinventory
                and    nvl(lot_number,'-99999') = nvl(p_lot_number,'-99999');
                
        EXCEPTION
                WHEN OTHERS THEN
                    l_ss_qty := NULL;
        
        END;
        
        --Assign the SS Quantity as the system quantity
        --This will validate the count quantity against the snapshot quantity and not the current on-hand quantity
        
        IF l_ss_qty is not null then
        
            lrec.system_quantity := l_ss_qty;
        
        END IF;
        
         
    --Calling Cycle Count Entries API
    mtl_cceoi_action_pub.import_CountRequest
      (p_api_version     => 0.9
      ,P_COMMIT          => FND_API.G_TRUE
      ,P_INIT_MSG_LIST   => FND_API.G_TRUE
      ,p_validation_level => FND_API.G_VALID_LEVEL_FULL
      ,x_return_status   => x_status
      ,x_errorcode       => x_error_code
      ,x_msg_count       => x_msg_count
      ,x_msg_data        => l_msg_data
      ,p_interface_rec   => lrec
      ,x_interface_id    => x_interface_id);
   
    
    IF (x_status != fnd_api.g_ret_sts_success) THEN
    
        IF ( x_msg_count > 0 ) THEN  -- Changed from 1 to 0 in Rev 1.1
          FOR i IN 1 .. x_msg_count
            LOOP
               fnd_msg_pub.get ( p_msg_index => i , p_encoded =>FND_API.G_FALSE,
               p_data => x_msg , p_msg_index_out => j ) ;
              
               
            END LOOP ;
        END IF;
        
    ELSE
    
      x_msg := NULL;
      
      
    END IF;
  
  EXCEPTION
    
    when others then
      
        g_message := 'Encountered when others exception on process_api';
        g_err_msg := SQLCODE || SQLERRM;
      
       xxcus_error_pkg.xxcus_error_main_api(p_called_from      => g_err_callfrom
                                          ,p_calling           => g_err_callpoint
                                          ,p_ora_error_msg     => g_err_msg
                                          ,p_error_desc        => g_message
                                          ,p_distribution_list => g_distro_list
                                          ,p_module            => 'INV');    
    
  
  
  END process_api;
  
  FUNCTION wc_count_list_sequence
    (p_count_list_sequence    NUMBER
    ,p_wc_stock_locator       VARCHAR2)
    RETURN NUMBER IS

  l_wc_count_list_sequence number;
    
  BEGIN

    IF p_count_list_sequence is not null and p_wc_stock_locator IS NOT NULL THEN
    
      l_wc_count_list_sequence := p_count_list_sequence ||'.'|| substr(p_wc_stock_locator,1,1);
      
    ELSE
    
      l_wc_count_list_sequence := p_count_list_sequence;
      
    END IF;
  
  
    RETURN l_wc_count_list_sequence;
  
  EXCEPTION 
    WHEN OTHERS THEN
  
    RETURN p_count_list_sequence;
    
  END wc_count_list_sequence;
END  XXWC_CC_ENTRIES_PKG;
/