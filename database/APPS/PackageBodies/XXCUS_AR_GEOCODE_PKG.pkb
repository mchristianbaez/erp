CREATE OR REPLACE package body APPS.xxcus_ar_geocode_pkg
-- ESMS TICKET 190433
-- Used by HDS AR Tax Reports concurrent program.
AS
 
  function get_ship_to_geocode (
    l_state         in  varchar2
   ,l_cityname      in  varchar2   
   ,l_zipcode       in  varchar2
   ,l_county        in  varchar2
  ) return varchar2 is  
     
  cursor get_txw_info (p_state in varchar2, p_cityname in varchar2, p_zipcode in varchar2, p_county in varchar2) is
  select min(geo_code) txw_geo, combined_rate*100 max_tax_rate, STNAME
    from (
    SELECT h.stalpha as stcode, h.stname AS STNAME, s.stname AS STATE, c.cntyname AS COUNTY, l.cityname AS CITY, 
      l.zipcode AS "ZIP CODE", l.geocode AS "GEO_CODE", x.stalphacode||l.zipcode||l.geocode as "OVERRIDE GEO",
      (s.cursalerate + s.curspecrate) AS "STATE RATE", 
      (c.cursalerate + c.curspecrate) AS "COUNTY RATE", 
      (l.cursalerate + l.curspecrate) AS "LOCAL RATE",
      (s.cursalerate + s.curspecrate + c.cursalerate + 
      c.curspecrate + l.cursalerate + l.curspecrate ) AS "COMBINED_RATE"
    FROM taxsttax s
      JOIN taxware.taxcntytax c USING (stcode)
      JOIN taxlocltax l USING (stcode, cntycode)
      join taxware.stepstcd_tbl x using(stcode)
      join TAXSTINFO h using (stcode)
       WHERE  1 = 1 
        and h.stalpha    =p_state
        and cityname     =p_cityname
        and l.zipcode    =nvl(p_zipcode, l.zipcode)        
        and c.cntyname   =nvl(p_county, c.cntyname) 
        order by s.stname, c.cntyname,l.cityname,l.zipcode,l.geocode
    )
    group by combined_rate*100, stname
    order by to_number(min(geo_code));
    
   type my_geo_tbl is table of get_txw_info%rowtype;
   my_geo_rec my_geo_tbl; 
   
   v_geocode varchar2(10) :=Null;   
   
  begin
    open get_txw_info
            (
              p_state    =>l_state 
             ,p_cityname =>l_cityname
             ,p_zipcode  =>l_zipcode
             ,p_county   =>l_county
            );
    fetch get_txw_info bulk collect into my_geo_rec;
    close get_txw_info;
    
    if my_geo_rec.count >0 then   
      for indx in 1 .. my_geo_rec.count loop 
        if indx =1 then
         v_geocode :=( l_state || substr(l_zipcode, 1, 5) || my_geo_rec(indx).txw_geo );
        else
         null;
        end if;
           if indx =1 then
            exit;
           end if;
      end loop;
    else 
     v_geocode :=Null;
    end if;
    
    return v_geocode;
    
  exception
   when others then
    v_geocode :=Null;
    return v_geocode; 
  end get_ship_to_geocode; 

end xxcus_ar_geocode_pkg;
/