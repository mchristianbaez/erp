CREATE OR REPLACE PACKAGE BODY APPS.XXWC_PO_EDI_COMM_DEF
AS
   /*************************************************************************
        $Header XXWC_PO_EDI_COMM_DEF $
        Module Name: XXWC_PO_EDI_COMM_DEF

        PURPOSE:   This package records the choices made in the PO Communication Form so that subsequent visits to the form defaults the correct values.

        REVISIONS:
        Ver        Date        Author                             Description
        ---------  ----------  -------------------------------    -------------------------
        1.0        05/19/2014  Jason Price (Focused E-commerce)   Initial Version
        1.1        10/08/2014  Lee Spitzer                           TMS # 20141001-00254 Multiorg
        1.2        06/05/2014  Manjula Chellappan                 TMS # 20150521-00029 Fix unique constraint error
      **************************************************************************/

   PROCEDURE set_history (P_po_header_id    IN NUMBER,
                          P_TRIGGER_TYPE    IN VARCHAR2,
                          P_CREATION_DATE   IN DATE,
                          P_XML_OR_EDI      IN VARCHAR2,
                          P_PRINT_CHECK     IN VARCHAR2,
                          P_FAX_NUMBER      IN VARCHAR2,
                          P_EMAIL_ADDRESS   IN VARCHAR2)
   /*************************************************************************
        $Header XXWC_PO_EDI_COMM_DEF $
        Module Name: XXWC_PO_EDI_COMM_DEF.set_history

        PURPOSE:   This procedure records the choices made in the PO Communication Form so that subsequent visits to the form defaults the correct values.

        REVISIONS:
        Ver        Date        Author                             Description
        ---------  ----------  -------------------------------    -------------------------
        1.0        05/19/2014  Jason Price (Focused E-commerce)   Initial Version
        1.1        10/08/2014  Lee Spitzer                           TMS # 20141001-00254 Multiorg
        1.2        06/05/2014  Manjula Chellappan                 TMS # 20150521-00029 Fix unique constraint error
      **************************************************************************/
   IS
      PRAGMA AUTONOMOUS_TRANSACTION;
      g_distro_list   VARCHAR2 (75)
                         DEFAULT 'HDSOracleDevelopers@hdsupply.com';
      -- Added the variable for Ver 1.2
      l_log_msg       VARCHAR2 (512);
      l_start         NUMBER;	  
   BEGIN
      /* removed 10/08/2014 by Lee Spitzer for TMS # 20141001-00254 Multiorg
      INSERT INTO XXWC.XXWC_PO_COMM_HIST -- values (P_po_header_id, P_XML_OR_EDI, P_PRINT_CHECK, P_FAX_NUMBER, P_EMAIL_ADDRESS)
                P_po_header_id,
                P_TRIGGER_TYPE,
                P_CREATION_DATE,
                P_XML_OR_EDI,
                P_PRINT_CHECK,
                P_FAX_NUMBER,
                P_EMAIL_ADDRESS
        */
      --added 10/08/2014 for org_id by Lee Spitzer for TMS # 20141001-00254 Multiorg
      INSERT INTO XXWC.XXWC_PO_COMM_HIST -- values (P_po_header_id, P_XML_OR_EDI, P_PRINT_CHECK, P_FAX_NUMBER, P_EMAIL_ADDRESS)
                                         (po_header_id,
                                          trigger_type,
                                          creation_date,
                                          xml_or_edi,
                                          print_check,
                                          fax_number,
                                          email_address)
         SELECT P_po_header_id,
                P_TRIGGER_TYPE,
                P_CREATION_DATE,
                P_XML_OR_EDI,
                P_PRINT_CHECK,
                P_FAX_NUMBER,
                P_EMAIL_ADDRESS
           FROM DUAL
          WHERE NOT EXISTS
                       (SELECT 'EXISTS'
                          FROM XXWC.XXWC_PO_COMM_HIST POCH
                         WHERE     P_po_header_id = POCH.po_header_id
                               AND NVL (P_XML_OR_EDI, 'NULL') =
                                      NVL (POCH.XML_OR_EDI, 'NULL')
                               AND NVL (P_PRINT_CHECK, 'NULL') =
                                      NVL (POCH.PRINT_CHECK, 'NULL')
                               AND NVL (P_FAX_NUMBER, 'NULL') =
                                      NVL (POCH.FAX_NUMBER, 'NULL')
                               AND NVL (P_EMAIL_ADDRESS, 'NULL') =
                                      NVL (POCH.EMAIL_ADDRESS, 'NULL'));

      COMMIT;
   EXCEPTION
      -- << Added for Ver 1.2 Begin

      WHEN DUP_VAL_ON_INDEX
      THEN	  
         l_log_msg :=
            SUBSTR (
                  'p_po_header_id:'
               || p_po_header_id
               || '-p_creation_date:'
               || TO_CHAR(p_creation_date,'DD-MON-YYYY HH:MI:SS AM')
               || '-p_xml_or_edi:'
               || p_xml_or_edi
               || '-p_print_check:'
               || p_print_check
               || '-p_fax_number:'
               || p_fax_number
               || '-p_email_address:'
               || p_email_address,
               1,
               512);

         UPDATE XXWC.XXWC_PO_COMM_HIST
            SET trigger_type = p_trigger_type,
                xml_or_edi = P_XML_OR_EDI,
                print_check = P_PRINT_CHECK,
                fax_number = P_FAX_NUMBER,
                email_address = P_EMAIL_ADDRESS
          WHERE     po_header_id = P_po_header_id
                AND creation_date = p_creation_date;

         xxwc_common_tunning_helpers.elapsed_time ('PO_COMM_HIST',
                                                   l_log_msg,
                                                   l_start);
         COMMIT;
      --  Added for Ver 1.2 End>>
      WHEN OTHERS
      THEN
         ROLLBACK;
     --  << Added for Ver 1.2 Begin		 
         l_log_msg :=
            SUBSTR (
                  'p_po_header_id:'
               || p_po_header_id
               || '-p_creation_date:'
               || TO_CHAR(p_creation_date,'DD-MON-YYYY HH:MI:SS AM')
               || '-p_xml_or_edi:'
               || p_xml_or_edi
               || '-p_print_check:'
               || p_print_check
               || '-p_fax_number:'
               || p_fax_number
               || '-p_email_address:'
               || p_email_address,
               1,
               512);
     --  Added for Ver 1.2 End>>
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_PO_EDI_COMM_DEF.set_history',
            p_calling             => 'Package called from PO Communication Form',
            p_request_id          => NULL,
            p_ora_error_msg       => SUBSTR (SQLERRM, 1, 2000),
            --p_error_desc          => 'OTHERS Exception', --  Commented for Rev 1.3
            p_error_desc          => SUBSTR (l_log_msg, 1, 240), -- Added for Rev 1.3
            p_distribution_list   => g_distro_list,
            p_module              => 'XXWC');
         COMMIT;
   END;
END XXWC_PO_EDI_COMM_DEF;
/