CREATE OR REPLACE PACKAGE BODY apps.XXWC_COMMON_UTILITIES_PKG
/***********************************************************************************************************
FILE NAME: APPS.XXWC_COMMON_UTILITIES_PKG.pkb

PROGRAM TYPE: PL/SQL Package body

PURPOSE: To Changing the file permission to particular directory file

HISTORY
=============================================================================================================
VERSION DATE          AUTHOR(S)       DESCRIPTION
------- -----------   --------------- -----------------------------------------------------------------------
1.0     03/27/2018    Pattabhi Avula  TMS#20171101-00037 Initial version.
1.1     03/29/2018    P.Vamshidhar    TMS#20180320-00096 Add logic to remove duplicate emails being sent for SOAs and ASNs
1.2     10/19/2018    P.Vamshidhar    TMS#20181019-00001 - Custom Tables High Water Mark Reset
**************************************************************************************************************/
AS
   l_package_name   VARCHAR2 (100)
                       := 'XXWC_COMMON_UTILITIES_PKG.FILE_PERMISSIONS_CHNG';
   l_distro_list    VARCHAR2 (100) DEFAULT 'WC-ITDevelopment-U1@HDSupply.com';

   /*****************************************************************************************************************
   Procedure Name: WRITE_LOG

   PROGRAM TYPE: Procedure name

   PURPOSE:To write log

   HISTORY
   ==================================================================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- ----------------------------------------------------------------------------
   1.2     10/19/2018    P.Vamshidhar    TMS#20181019-00001 - Custom Tables High Water Mark Reset
   ******************************************************************************************************************/   
   PROCEDURE write_log (p_log_msg IN VARCHAR2) IS
   BEGIN
     FND_FILE.PUT_LINE(FND_FILE.LOG,p_log_msg);
   END;
   
   
   PROCEDURE FILE_PERMISSIONS_CHNG (p_dir_path    IN VARCHAR2,
                                    p_file_name   IN VARCHAR2)
   IS
      lvc_command   VARCHAR2 (500);
      l_result      VARCHAR2 (500);
   BEGIN
      lvc_command := 'chmod 777' || ' ' || p_dir_path || '/' || p_file_name;

      BEGIN
         SELECT xxwc_edi_iface_pkg.xxwc_oscommand_run (lvc_command)
           INTO l_result
           FROM DUAL;
      EXCEPTION
         WHEN OTHERS
         THEN
            fnd_file.put_line (fnd_file.LOG,
                               'File permission Result is ' || l_result);
      END;
   EXCEPTION
      WHEN OTHERS
      THEN
         fnd_file.put_line (fnd_file.LOG, 'Error is ' || SQLERRM);
   END;

   /*****************************************************************************************************************
   Procedure Name: XXWC_OM_SOA_EMAIL_PRC

   PROGRAM TYPE: Procedure name

   PURPOSE: To remove duplicate email addresses.  This procedure being used in sales order receipt and pack slip rdfs

   HISTORY
   ==================================================================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- ----------------------------------------------------------------------------
   1.1     03/29/2018    P.Vamshidhar    Initial Version
                                         TMS#Add logic to remove duplicate emails being sent for SOAs and ASNs
   ******************************************************************************************************************/
   PROCEDURE XXWC_OM_SOA_EMAIL_PRC (p_to_email_addr   IN     VARCHAR2,
                                    p_cc_email_addr   IN     VARCHAR2,
                                    p_request_id      IN     NUMBER,
                                    x_to_email_addr      OUT VARCHAR2,
                                    x_cc_email_addr      OUT VARCHAR2)
   IS
      PROCEDURE email (p_email_add   IN     VARCHAR2,
                       x_email_add      OUT VARCHAR2,
                       p_flag               VARCHAR2)
      IS
         CURSOR CUR_EMAIL
         IS
            SELECT DISTINCT a.EMAIL_ADDRESS email_address
              FROM XXWC.XXWC_OM_SOA_EMAIL_GTT a
             WHERE a.FLAG = p_flag AND p_flag = 'TO'
            UNION ALL
            SELECT DISTINCT a.EMAIL_ADDRESS email_address
              FROM XXWC.XXWC_OM_SOA_EMAIL_GTT a
             WHERE     FLAG = p_flag
                   AND p_flag = 'CC'
                   AND NOT EXISTS
                          (SELECT 1
                             FROM XXWC.XXWC_OM_SOA_EMAIL_GTT b
                            WHERE     b.EMAIL_ADDRESS = a.email_address
                                  AND b.flag = 'TO');


         from_email   VARCHAR2 (1000);
         new_email    VARCHAR2 (1000);
         l_email      VARCHAR2 (1000);
         ln_cnt       NUMBER := 1;
      BEGIN
         from_email := p_email_add;
         from_email := REPLACE (from_email, ';', ',');

         LOOP
            EXIT WHEN ln_cnt = 0;
            l_email := NULL;
            l_email := SUBSTR (from_email, 1, INSTR (from_email, ',', 1) - 1);

            INSERT INTO XXWC.XXWC_OM_SOA_EMAIL_GTT (EMAIL_ADDRESS, FLAG)
                 VALUES (l_email, P_FLAG);

            ln_cnt := ln_cnt + 1;
            from_email := SUBSTR (from_email, INSTR (from_email, ',', 1) + 1);

            IF INSTR (from_email, ',', 1) = 0
            THEN
               INSERT INTO XXWC.XXWC_OM_SOA_EMAIL_GTT (EMAIL_ADDRESS, flag)
                    VALUES (from_email, p_flag);

               ln_cnt := 0;
            END IF;
         END LOOP;

         new_email := NULL;

         FOR REC_EMAIL IN CUR_EMAIL
         LOOP
            IF new_email IS NOT NULL
            THEN
               new_email := new_email || ',' || REC_EMAIL.EMAIL_ADDRESS;
            ELSE
               new_email := REC_EMAIL.EMAIL_ADDRESS;
            END IF;
         END LOOP;

         x_email_add := new_email;
      EXCEPTION
         WHEN OTHERS
         THEN
            x_email_add := p_email_add;
            xxcus_error_pkg.xxcus_error_main_api (
               p_called_from         => 'xxwc_om_soa_email_prc',
               p_calling             => 'xxwc_om_soa_email_prc child procedure',
               p_request_id          => p_request_id,
               p_ora_error_msg       => SQLERRM,
               p_error_desc          => 'p_email_add:' || p_email_add,
               p_distribution_list   => 'WC-ITDEVALERTS-U1@HDSupply.com',
               p_module              => 'XXWC');
      END;
   BEGIN
      IF p_to_email_addr IS NOT NULL
      THEN
         email (LOWER (p_to_email_addr), x_to_email_addr, 'TO');
      END IF;

      IF p_cc_email_addr IS NOT NULL
      THEN
         email (LOWER (p_cc_email_addr), x_cc_email_addr, 'CC');
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_COMMON_UTILITIES_PKG.XXWC_OM_SOA_EMAIL_PRC',
            p_calling             => 'xxwc_om_soa_email_prc',
            p_request_id          => p_request_id,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          =>    'To email:'
                                     || p_to_email_addr
                                     || ' cc Email:'
                                     || p_cc_email_addr,
            p_distribution_list   => 'WC-ITDEVALERTS-U1@HDSupply.com',
            p_module              => 'XXWC');
         x_to_email_addr := p_to_email_addr;
         x_cc_email_addr := p_cc_email_addr;
   END;

   /*****************************************************************************************************************
   Procedure Name: HWM_RESET

   PROGRAM TYPE: Procedure name

   PURPOSE: To Table Reset High Water Mark.

   HISTORY
   ==================================================================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- ----------------------------------------------------------------------------
   1.2     10/19/2018    P.Vamshidhar    TMS#20181019-00001 - Custom Tables High Water Mark Reset
   ******************************************************************************************************************/   
	PROCEDURE HWM_RESET ( X_ERRBUF     OUT VARCHAR2,
	                      X_RETCODE    OUT VARCHAR2,									  
	                      P_TABLE_NAME IN VARCHAR2,
						  P_RESET_TYPE IN VARCHAR2,
						  P_EXE_STATS  IN VARCHAR2) IS
   
    lvc_table_name  VARCHAR2(32767);
	lvc_owner       VARCHAR2(100);
	
    CURSOR CUR_TAB IS
    SELECT LOOKUP_CODE TABLE_NAME,
           ATTRIBUTE1 RESET_OPTION
      FROM FND_LOOKUP_VALUES FLV
     WHERE FLV.LOOKUP_TYPE              ='XXWC_CUSTOM_TABLES_HWM_RESET'
       AND FLV.ENABLED_FLAG               ='Y'
       AND NVL(END_DATE_ACTIVE,SYSDATE+1)>=SYSDATE; 
    BEGIN
	    WRITE_LOG('Procedure Start');
		IF P_TABLE_NAME IS NOT NULL THEN
		   BEGIN
		     SELECT OWNER, OWNER||'.'||TABLE_NAME  INTO lvc_owner,lvc_table_name
			 FROM DBA_TABLES WHERE TABLE_NAME = P_TABLE_NAME;
                			 
		   EXCEPTION
           WHEN OTHERS THEN
            WRITE_LOG('Error Occurred '||SUBSTR(SQLERRM,1,250));
			lvc_table_name:=NULL;		
           END;		   
		   WRITE_LOG('lvc_table_name: '||lvc_table_name);
           		
            IF P_RESET_TYPE ='SHRINK' THEN
                SHRINK_TABLE(lvc_table_name);
            ELSIF P_RESET_TYPE='TRUNCATE' THEN
			    TRUNC_TABLE(lvc_table_name);
            END IF;
		    IF P_EXE_STATS ='Y' THEN
			PROGRAM_SUBMIT(lvc_owner,P_TABLE_NAME);
			END IF;			
		ELSE
		FOR REC_TAB IN CUR_TAB LOOP

		    BEGIN
			lvc_table_name := NULL;		
            lvc_owner := NULL;			
		     SELECT OWNER, OWNER||'.'||TABLE_NAME  INTO lvc_owner, lvc_table_name
			 FROM DBA_TABLES WHERE TABLE_NAME = REC_TAB.TABLE_NAME;                			 
		   EXCEPTION
           WHEN OTHERS THEN
            WRITE_LOG('Error Occurred '||SUBSTR(SQLERRM,1,250));
			lvc_table_name:=NULL;		
           END;		
		   
		   WRITE_LOG('lvc_table_name: '||lvc_table_name);
           		
            IF REC_TAB.RESET_OPTION ='SHRINK' THEN
                SHRINK_TABLE(lvc_table_name);
            ELSIF REC_TAB.RESET_OPTION='TRUNCATE' THEN
			    TRUNC_TABLE(lvc_table_name);
            END IF;

		    IF P_EXE_STATS = 'Y' THEN
			PROGRAM_SUBMIT(lvc_owner,REC_TAB.TABLE_NAME);
			END IF;
		END LOOP;
		END IF;
	   
   EXCEPTION
      WHEN OTHERS
      THEN
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_COMMON_UTILITIES_PKG.HWM_RESET',
            p_calling             => 'HWM_RESET',
            p_request_id          => FND_GLOBAL.CONC_REQUEST_ID,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          => SQLERRM ,
            p_distribution_list   => 'WC-ITDEVALERTS-U1@HDSupply.com',
            p_module              => 'XXWC');
	END;

   /*****************************************************************************************************************
   Procedure Name: SHRINK_TABLE

   PROGRAM TYPE: Procedure name

   PURPOSE: To Table Reset High Water Mark.

   HISTORY
   ==================================================================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- ----------------------------------------------------------------------------
   1.2     10/19/2018    P.Vamshidhar    TMS#20181019-00001 - Custom Tables High Water Mark Reset
   ******************************************************************************************************************/	
	
	PROCEDURE SHRINK_TABLE (P_TABLE_NAME IN VARCHAR2) IS
	lvc_command VARCHAR2(32767);
	BEGIN	
      lvc_command:='ALTER TABLE '||P_TABLE_NAME||' ENABLE ROW MOVEMENT';
      write_log(lvc_command);	  
	  EXECUTE IMMEDIATE lvc_command;
	  
      lvc_command:='ALTER TABLE '||P_TABLE_NAME||' SHRINK SPACE';
      write_log(lvc_command);	  
	  EXECUTE IMMEDIATE lvc_command;

   EXCEPTION
      WHEN OTHERS
      THEN
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_COMMON_UTILITIES_PKG.SHRINK_TABLE',
            p_calling             => 'SHRINK_TABLE',
            p_request_id          => FND_GLOBAL.CONC_REQUEST_ID,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          => SQLERRM ,
            p_distribution_list   => 'WC-ITDEVALERTS-U1@HDSupply.com',
            p_module              => 'XXWC');	  
    END;

   /*****************************************************************************************************************
   Procedure Name: TRUNC_TABLE

   PROGRAM TYPE: Procedure

   PURPOSE: To Table Reset High Water Mark.

   HISTORY
   ==================================================================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- ----------------------------------------------------------------------------
   1.2     10/19/2018    P.Vamshidhar    TMS#20181019-00001 - Custom Tables High Water Mark Reset
   ******************************************************************************************************************/	
	
	PROCEDURE TRUNC_TABLE (P_TABLE_NAME IN VARCHAR2) IS	
	lvc_table_bkp_name VARCHAR2(30);
    lvc_command VARCHAR2(32767);	
	ln_tab_count NUMBER:=0;
	ln_bkp_count NUMBER:=0;
	BEGIN
    write_log('	TRUNC_TABLE Start for '||P_TABLE_NAME);
	
	lvc_table_bkp_name := SUBSTR(p_table_name,1,20)||'_'||TO_CHAR(SYSDATE,'HH24MISS');
	write_log('Backup Table Name:'||lvc_table_bkp_name);
	lvc_command := 'CREATE TABLE '||lvc_table_bkp_name||' AS ( SELECT * FROM '||P_TABLE_NAME||' )';
	write_log(lvc_command);
	EXECUTE IMMEDIATE lvc_command;
	
	lvc_command:='TRUNCATE TABLE '||p_table_name;
	write_log(lvc_command);
	EXECUTE IMMEDIATE lvc_command;
	lvc_command :='INSERT INTO '||P_TABLE_NAME||' (SELECT * FROM '||lvc_table_bkp_name||')';
	write_log(lvc_command);
	EXECUTE IMMEDIATE lvc_command;
	COMMIT;
	
	lvc_command:= 'SELECT COUNT(1) FROM '||lvc_table_bkp_name;
	write_log(lvc_command);
	EXECUTE IMMEDIATE lvc_command INTO ln_bkp_count;
	write_log('Backup Table Count :'||ln_bkp_count);
	
	lvc_command:= 'SELECT COUNT(1) FROM '||p_table_name;
	write_log(lvc_command);
	EXECUTE IMMEDIATE lvc_command INTO ln_tab_count;
	write_log('Table Count :'||ln_tab_count);
	
    IF NVL(ln_bkp_count,0) = NVL(ln_tab_count,0) THEN
	    lvc_command :='DROP TABLE '||lvc_table_bkp_name;
		write_log(lvc_command);
        EXECUTE IMMEDIATE lvc_command;
    ELSE
	    write_log('Rows not equal for backup and original table');
    END IF;
    write_log('	TRUNC_TABLE complete for '||P_TABLE_NAME);
  EXCEPTION
      WHEN OTHERS
      THEN
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_COMMON_UTILITIES_PKG.TRUNC_TABLE',
            p_calling             => 'TRUNC_TABLE',
            p_request_id          => FND_GLOBAL.CONC_REQUEST_ID,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          => SQLERRM ,
            p_distribution_list   => 'WC-ITDEVALERTS-U1@HDSupply.com',
            p_module              => 'XXWC');		
    END;

   /*****************************************************************************************************************
   Procedure Name: PROGRAM_SUBMIT

   PROGRAM TYPE: Procedure

   PURPOSE: To Table Reset High Water Mark.

   HISTORY
   ==================================================================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- ----------------------------------------------------------------------------
   1.2     10/19/2018    P.Vamshidhar    TMS#20181019-00001 - Custom Tables High Water Mark Reset
   ******************************************************************************************************************/	
	PROCEDURE PROGRAM_SUBMIT (P_OWNER      IN VARCHAR2,
	                          P_TABLE_NAME IN VARCHAR2) IS
							  
    ln_request_id   FND_CONCURRENT_REQUESTS.REQUEST_ID%TYPE;
	BEGIN
	    write_log('Table Name:'||P_OWNER||'.'||P_TABLE_NAME);
	    
		ln_request_id := fnd_request.submit_request (application   => 'FND',  
                                                     program       => 'FNDGTST',  
                                                     description   => NULL,
                                                     start_time    => NULL,
                                                     sub_request   => NULL,
                                                     argument1     => P_OWNER,
                                                     argument2     => P_TABLE_NAME, 
                                                     argument3     => NULL,
                                                     argument4     => NULL,
                                                     argument5     => NULL,
                                                     argument6     => 'NOBACKUP',
								                     argument7     => 'DEFAULT',
								                     argument8     => 'LASTRUN',
								                     argument9     => 'Y');
	            write_log('Request Id:'||ln_request_id);
	     COMMIT;
  EXCEPTION
      WHEN OTHERS
      THEN
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_COMMON_UTILITIES_PKG.PROGRAM_SUBMIT',
            p_calling             => 'PROGRAM_SUBMIT',
            p_request_id          => FND_GLOBAL.CONC_REQUEST_ID,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          => SQLERRM ,
            p_distribution_list   => 'WC-ITDEVALERTS-U1@HDSupply.com',
            p_module              => 'XXWC');		 
	END;
END XXWC_COMMON_UTILITIES_PKG;
/