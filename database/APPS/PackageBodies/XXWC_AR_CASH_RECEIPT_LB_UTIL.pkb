-- Start of DDL Script for Package Body APPS.XXWC_AR_CASH_RECEIPT_LB_UTIL
-- Generated 6/25/2012 9:01:40 PM from APPS@EBIZCON

CREATE OR REPLACE 
PACKAGE BODY apps.xxwc_ar_cash_receipt_lb_util
/* Formatted on 6/25/2012 8:59:13 PM (QP5 v5.206) */
IS
    l_err_code            VARCHAR2 (24) := 0;

    l_errbuf              CLOB;
    l_distro_list         VARCHAR2 (75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
    l_org_name   CONSTANT hr_all_organization_units.name%TYPE := 'HDS White Cap - Org';
    l_package_name        VARCHAR2 (128) := 'XXWC_AR_CASH_RECEIPT_LB_UTL';
    l_procedure_name      VARCHAR2 (128);
    l_req_id              NUMBER := fnd_global.conc_request_id;
    l_program_error       EXCEPTION;
    PRAGMA EXCEPTION_INIT (l_program_error, -2323);
    l_temp_table          VARCHAR2 (128);
    l_org_id              NUMBER;
    l_prismfh999_status   VARCHAR2 (64);
    l_prism99999_status   VARCHAR2 (64);
    l_prism99poa_status   VARCHAR2 (64);
    l_prismfhpoa_status   VARCHAR2 (64);
    l_rev_status          VARCHAR2 (64);

    --create sequence xxwc.xxwc_prizm_lockbox_run start with 100000
    --************************************************************
    --************************************************************
    --************************************************************
    --************************************************************
    --************************************************************
    --************************************************************
    PROCEDURE xxwc_wait_for_cp (p_request_id NUMBER, p_data_file VARCHAR2)
    IS
        vr_result               BOOLEAN;
        v_request_id            NUMBER;
        vr_phase                VARCHAR2 (64);
        vr_status               VARCHAR2 (64);
        vr_dev_phase            VARCHAR2 (64);
        vr_dev_status           VARCHAR2 (64);
        vr_debugging_message1   VARCHAR2 (264);
        v_data_file             VARCHAR2 (128);
        v_file_dir              VARCHAR2 (128) := 'XXWCAR_LOCKBOX_DIR';
    BEGIN
        v_request_id := p_request_id;
        v_data_file := p_data_file;
        vr_result :=
            fnd_concurrent.wait_for_request (v_request_id,
                                             10,
                                             0,
                                             vr_phase,
                                             vr_status,
                                             vr_dev_phase,
                                             vr_dev_status,
                                             vr_debugging_message1);
        l_errbuf := 'waiting on submitted requests';
        xxwc_log (l_errbuf);

        IF vr_result AND vr_dev_phase = 'COMPLETE' AND vr_dev_status <> 'NORMAL'
        THEN
            l_errbuf :=
                   l_errbuf
                || ' An error  when submit the  XXWCAR_LOCKBOX_PRISM  for file:'
                || v_data_file
                || '; Phase: '
                || vr_dev_phase
                || '; Status: '
                || vr_dev_status
                || '; request_id ='
                || v_request_id;
            xxwc_log (l_errbuf);
        END IF;

        BEGIN
            UTL_FILE.frename (v_file_dir,
                              v_data_file,
                              v_file_dir,
                              'P_' || v_data_file);
        EXCEPTION
            WHEN OTHERS
            THEN
                NULL;
        -- xxwc_log ('Exception...' || l_file_name || ' file does not exist to rename/archive...');
        END;
    END;

    PROCEDURE xxwc_log (p_message VARCHAR2)
    IS
    BEGIN
        DBMS_OUTPUT.put_line ('*Message from ' || l_procedure_name || ' ' || p_message);

        -- fnd_file.put_line (fnd_file.LOG, '*Message from ' || l_procedure_name || ' ' || p_message);

        IF NVL (l_err_code, '0') <> '0'
        THEN
            DBMS_OUTPUT.put_line ('*l_err_code= ' || l_err_code);
            xxcus_error_pkg.xxcus_error_main_api (
                p_called_from         => l_package_name,
                p_calling             => l_procedure_name,
                p_request_id          => l_req_id,
                p_ora_error_msg       => SUBSTR (p_message, 1, 2000),
                p_error_desc          => 'Error running ' || l_package_name || '.' || l_procedure_name,
                p_distribution_list   => l_distro_list,
                p_module              => 'AR');
        END IF;

        l_errbuf := NULL;
    END;

    -- ALTER TABLE xxwc.xxwcar_cash_rcpts_tbl ADD(oracle_account_nbr VARCHAR2(164),cust_account_id NUMBER ,oracle_flag VARCHAR2(128))
    --************************************************************
    --************************************************************
    --************************************************************
    --************************************************************
    --************************************************************
    --************************************************************

    PROCEDURE xxwc_prism_lb_driver (p_errbuf OUT VARCHAR2, p_retcode OUT VARCHAR2, p_org_id NUMBER DEFAULT NULL)
    IS
        v_file_suffix         VARCHAR2 (64);
        v_org_id              NUMBER;

        v_prismfh999_status   VARCHAR2 (64);
        v_prism99999_status   VARCHAR2 (64);
        v_prism99poa_status   VARCHAR2 (64);
        v_prismfhpoa_status   VARCHAR2 (64);
        v_trans_count         NUMBER;
        vr1_req_id            NUMBER;
        vr2_req_id            NUMBER;
        vr3_req_id            NUMBER;
        vr4_req_id            NUMBER;
        v_exit_exception      EXCEPTION;
    BEGIN
        l_errbuf := NULL;
        l_err_code := '0';
        l_procedure_name := UPPER ('xxwc_prism_lb_driver');

        BEGIN
            SELECT organization_id
              INTO l_org_id
              FROM hr_all_organization_units
             WHERE name = l_org_name;
        EXCEPTION
            WHEN OTHERS
            THEN
                NULL;
        END;

        IF p_org_id IS NULL
        THEN
            v_org_id := 162;
        ELSE
            v_org_id := l_org_id;
        END IF;

        --populate xxwc.xxwcar_cash_rcpts_tbl with right oracle account
        xxwc_ar_cash_receipt_lb_util.xxwc_validate_prism_account (v_org_id);

        IF l_err_code <> '0'
        THEN
            RAISE v_exit_exception;
        END IF;

        --update status to remove from processing - all rows with prism account errors
        BEGIN
            FOR r IN (SELECT ROWID v_rid, oracle_flag
                        FROM xxwc.xxwcar_cash_rcpts_tbl
                       WHERE cust_account_id = 0 AND status = 'NEW')
            LOOP
                UPDATE xxwc.xxwcar_cash_rcpts_tbl
                   SET status = 'NEXT',
                       oracle_flag = NVL (r.oracle_flag, 'customer ' || customer_nbr || ' is not in Oracle')
                 WHERE ROWID = r.v_rid;
            END LOOP;

            COMMIT;
        EXCEPTION
            WHEN OTHERS
            THEN
                NULL;
        END;

        --validate that new receipt in oracle;
        xxwc_ar_cash_receipt_lb_util.xxwc_verify_receipts (v_org_id);

        IF l_err_code <> '0'
        THEN
            RAISE v_exit_exception;
        END IF;

        --  v_org_id := 162;
        p_retcode := '0';
        l_err_code := '0';

        --xxwc_ar_cash_receipt_lb_util.xxwc_populate_temp_tables (l_temp_table);
        l_procedure_name := UPPER ('xxwc_prism_lb_driver');

        --alter table xxwc.xxwcar_cash_rcpts_tbl modify(status varchar2(64))

        v_file_suffix := xxwc.xxwc_prism_lockbox_run_s.NEXTVAL;
        v_prismfh999_status := 'PRISMFH999' || '_' || v_file_suffix;
        v_prism99999_status := 'PRISM99999' || '_' || v_file_suffix;
        v_prism99poa_status := 'PRISM99POA' || '_' || v_file_suffix;
        v_prismfhpoa_status := 'PRISMFHPOA' || '_' || v_file_suffix;

        l_rev_status := 'REV' || '_' || v_file_suffix;
        l_prismfh999_status := v_prismfh999_status;
        l_prism99999_status := v_prism99999_status;
        l_prism99poa_status := v_prism99poa_status;
        l_prismfhpoa_status := v_prismfhpoa_status;

        --********************************
        FOR r
            IN (SELECT ROWID v_rid, bill_to_location, receipt_type
                  FROM xxwc.xxwcar_cash_rcpts_tbl
                 WHERE     status = 'NEW'
                       AND receipt_type IN ('CASH', 'DEPOSIT', 'POA')
                       AND NVL (oracle_flag, 'null') <> 'in Cash Receipt table')
        LOOP
            IF r.receipt_type IN ('CASH', 'DEPOSIT') AND r.bill_to_location NOT IN ('401', '402', '403')
            THEN
                UPDATE xxwc.xxwcar_cash_rcpts_tbl
                   SET status = v_prism99999_status,
                       operator_code =
                           (SELECT lookup_code
                              FROM apps.fnd_lookup_values hi
                             WHERE     meaning = 'PRISM99999'
                                   AND enabled_flag = 'Y'
                                   AND lookup_type = 'XXWC_AR_LOCKBOX_DCTM'
                                   AND NVL (end_date_active, SYSDATE + 1) > SYSDATE)
                 WHERE ROWID = r.v_rid;
            ELSIF r.receipt_type IN ('CASH', 'DEPOSIT') AND r.bill_to_location IN ('401', '402', '403')
            THEN
                UPDATE xxwc.xxwcar_cash_rcpts_tbl
                   SET status = v_prismfh999_status,
                       operator_code =
                           (SELECT lookup_code
                              FROM apps.fnd_lookup_values hi
                             WHERE     meaning = 'PRISMFH999'
                                   AND enabled_flag = 'Y'
                                   AND lookup_type = 'XXWC_AR_LOCKBOX_DCTM'
                                   AND NVL (end_date_active, SYSDATE + 1) > SYSDATE)
                 WHERE ROWID = r.v_rid;
            ELSIF r.receipt_type IN ('POA') AND r.bill_to_location IN ('401', '402', '403')
            THEN
                UPDATE xxwc.xxwcar_cash_rcpts_tbl
                   SET status = v_prismfhpoa_status,
                       operator_code =
                           (SELECT lookup_code
                              FROM apps.fnd_lookup_values hi
                             WHERE     meaning = 'PRISMFHPOA'
                                   AND enabled_flag = 'Y'
                                   AND lookup_type = 'XXWC_AR_LOCKBOX_DCTM'
                                   AND NVL (end_date_active, SYSDATE + 1) > SYSDATE)
                 WHERE ROWID = r.v_rid;
            ELSIF r.receipt_type IN ('POA') AND r.bill_to_location NOT IN ('401', '402', '403')
            THEN
                UPDATE xxwc.xxwcar_cash_rcpts_tbl
                   SET status = v_prism99poa_status,
                       operator_code =
                           (SELECT lookup_code
                              FROM apps.fnd_lookup_values hi
                             WHERE     meaning = 'PRISM99POA'
                                   AND enabled_flag = 'Y'
                                   AND lookup_type = 'XXWC_AR_LOCKBOX_DCTM'
                                   AND NVL (end_date_active, SYSDATE + 1) > SYSDATE)
                 WHERE ROWID = r.v_rid;
            END IF;
        END LOOP;

        COMMIT;

        IF l_err_code <> '0'
        THEN
            RAISE v_exit_exception;
        END IF;

        l_errbuf := NULL;
        l_err_code := '0';

        SELECT COUNT ('q')
          INTO v_trans_count
          FROM xxwc.xxwcar_cash_rcpts_tbl
         WHERE status = v_prism99999_status;

        IF v_trans_count > 0
        THEN
            xxwc_log ('creating file for ' || v_prism99999_status);
            prism_lockbox_process (l_errbuf,
                                   l_err_code,
                                   v_prism99999_status,
                                   'PRISM99999',
                                   vr1_req_id);
        END IF;

        IF l_err_code <> '0'
        THEN
            RAISE v_exit_exception;
        END IF;

        l_errbuf := NULL;
        l_err_code := '0';

        SELECT COUNT ('q')
          INTO v_trans_count
          FROM xxwc.xxwcar_cash_rcpts_tbl
         WHERE status = v_prismfh999_status;

        IF v_trans_count > 0
        THEN
            xxwc_log ('creating file for ' || v_prismfh999_status);
            prism_lockbox_process (l_errbuf,
                                   l_err_code,
                                   v_prismfh999_status,
                                   'PRISMFH999',
                                   vr2_req_id);
        END IF;

        IF l_err_code <> '0'
        THEN
            RAISE v_exit_exception;
        END IF;

        l_errbuf := NULL;
        l_err_code := '0';

        SELECT COUNT ('q')
          INTO v_trans_count
          FROM xxwc.xxwcar_cash_rcpts_tbl
         WHERE status = v_prism99poa_status;

        IF v_trans_count > 0
        THEN
            xxwc_log ('creating file for ' || v_prism99poa_status);
            prism_lockbox_process (l_errbuf,
                                   l_err_code,
                                   v_prism99poa_status,
                                   'PRISM99POA',
                                   vr3_req_id);
        END IF;

        IF l_err_code <> '0'
        THEN
            RAISE v_exit_exception;
        END IF;

        l_errbuf := NULL;
        l_err_code := '0';

        SELECT COUNT ('q')
          INTO v_trans_count
          FROM xxwc.xxwcar_cash_rcpts_tbl
         WHERE status = v_prismfhpoa_status;

        IF v_trans_count > 0
        THEN
            xxwc_log ('creating file for ' || v_prismfhpoa_status);
            prism_lockbox_process (l_errbuf,
                                   l_err_code,
                                   v_prismfhpoa_status,
                                   'PRISMFHPOA',
                                   vr4_req_id);
        END IF;

        IF l_err_code <> '0'
        THEN
            RAISE v_exit_exception;
        END IF;

        xxwc_wait_for_cp (vr1_req_id, v_prism99999_status || '.txt');
        xxwc_wait_for_cp (vr2_req_id, v_prismfh999_status || '.txt');
        xxwc_wait_for_cp (vr3_req_id, v_prism99poa_status || '.txt');
        xxwc_wait_for_cp (vr4_req_id, v_prismfhpoa_status || '.txt');

        l_errbuf := NULL;
        l_err_code := '0';

        xxwc_log ('reversing');

        reverse_receipt (l_errbuf, l_err_code, v_org_id);
        l_procedure_name := UPPER ('xxwc_prism_lb_driver');
        p_retcode := '0';
        l_errbuf := NULL;
        p_errbuf := NULL;
        xxwc_log ('return code is  ' || p_retcode);

        --validate that new receipt in oracle;
        xxwc_ar_cash_receipt_lb_util.xxwc_verify_receipts (v_org_id);

        BEGIN
            FOR r IN (SELECT ROWID v_rid
                        FROM xxwc.xxwcar_cash_rcpts_tbl
                       WHERE status = 'NEXT')
            LOOP
                UPDATE xxwc.xxwcar_cash_rcpts_tbl
                   SET status = 'NEW'
                 WHERE ROWID = r.v_rid;
            END LOOP;

            COMMIT;
        EXCEPTION
            WHEN OTHERS
            THEN
                NULL;
        END;

        UPDATE xxwc.xxwcar_cash_rcpts_tbl
           SET status = 'NEW'
         WHERE     status IN (l_prismfh999_status, l_prism99999_status, l_prism99poa_status, l_prismfhpoa_status)
               AND oracle_flag IS NULL;

        COMMIT;
    EXCEPTION
        WHEN OTHERS
        THEN
            UPDATE xxwc.xxwcar_cash_rcpts_tbl
               SET status = 'NEW'
             WHERE     status IN (l_prismfh999_status, l_prism99999_status, l_prism99poa_status, l_prismfhpoa_status)
                   AND oracle_flag IS NULL;

            COMMIT;
            l_err_code := '2';
            p_retcode := l_err_code;
            l_procedure_name := UPPER ('xxwc_prism_lb_driver');
            l_errbuf :=
                   l_errbuf
                || 'Error_Stack...'
                || CHR (10)
                || DBMS_UTILITY.format_error_stack ()
                || CHR (10)
                || ' Error_Backtrace...'
                || CHR (10)
                || DBMS_UTILITY.format_error_backtrace ();

            xxwc_log (l_errbuf);
    END;

    --************************************************************
    --************************************************************
    --************************************************************
    --************************************************************
    --************************************************************
    --************************************************************

    PROCEDURE xxwc_prism_lb_import_validate (p_org_id                    NUMBER,
                                             p_trans_name                VARCHAR2,
                                             p_data_file                 VARCHAR2,
                                             p_control_file              VARCHAR2,
                                             p_transmission_format       VARCHAR2,
                                             p_lockbox_name              VARCHAR2,
                                             p_report_format             VARCHAR2,
                                             p_request_id            OUT NUMBER)
    IS
        /* TO TEST
    DECLARE
        v_request_id   NUMBER;
    BEGIN
        xxwc_ar_cash_receipt_lb_util.xxwc_prism_lb_import_validate (
            162,
            'test98',
            'WC_PRISM_LOCKBOX_FILE_03272012.txt',
            'XXWCAR_LOCKBOX_PRISM',
            'XXWC_LOCKBOX_PRISM',
            'PRISM99999',
            'Rejects Only',
            v_request_id);
        xxwc_log ('v_request_id=' || v_request_id);
    END;
        */

        v_new_trans_flag               VARCHAR2 (16) := 'Y';
        v_run_import                   VARCHAR2 (16) := 'Y';
        v_run_validation               VARCHAR2 (16) := 'Y';
        v_invalid_trans_num_handling   VARCHAR2 (16) := 'Y';                          -- post partial amout as unapplied
        v_run_appl                     VARCHAR2 (16) := 'Y';                                    --submit post quick cash
        v_transmission_format_id       NUMBER;
        v_lockbox_id                   NUMBER;
        v_code_report_format           VARCHAR2 (16) := 'R';                                --R 'Rejects Only', 'A' -all
        v_user_name                    VARCHAR2 (164) := 'xxwc_int_finance';
        v_user_id                      NUMBER;
        v_prism_lockbox_directory      VARCHAR2 (512);
        v_req_id                       NUMBER;
        v_control_file                 VARCHAR2 (164);
        v_transmission_format          VARCHAR2 (164);
        v_report_format                VARCHAR2 (164);
        v_data_file                    VARCHAR2 (512);

        v_exit_exception               EXCEPTION;

        v_resp_id                      NUMBER;
        v_resp_appl_id                 NUMBER;
    BEGIN
        l_errbuf := NULL;
        l_err_code := '0';
        l_procedure_name := UPPER ('xxwc_prism_lb_import_validate');
        xxwc_log ('running file' || p_data_file);
        v_control_file := COALESCE (p_control_file, 'XXWCAR_LOCKBOX_PRISM');
        v_transmission_format := COALESCE (p_transmission_format, 'XXWC_LOCKBOX_PRISM');
        v_report_format := COALESCE (p_report_format, 'Rejects Only');

        BEGIN
            SELECT directory_path
              INTO v_prism_lockbox_directory
              FROM all_directories
             WHERE directory_name = 'XXWCAR_LOCKBOX_DIR';
        EXCEPTION
            WHEN OTHERS
            THEN
                l_errbuf := 'DIRECTORY XXWCAR_LOCKBOX_DIR is not set up in Oracle';
                RAISE v_exit_exception;
        END;

        FOR r IN (SELECT transmission_format_id
                    FROM ar_transmission_formats
                   WHERE format_name = v_transmission_format)                                    --'XXWC_LOCKBOX_PRISM')
        LOOP
            v_transmission_format_id := r.transmission_format_id;
        END LOOP;

        v_data_file := v_prism_lockbox_directory || '/' || p_data_file;

        FOR r IN (SELECT b.lockbox_id
                    FROM ar.ar_lockboxes_all b
                   WHERE b.lockbox_number = p_lockbox_name)                                               --'PRISM99POA'
        LOOP
            v_lockbox_id := r.lockbox_id;
        END LOOP;

        FOR r IN (SELECT lookup_code, meaning
                    FROM ar_lookups
                   WHERE lookup_type = 'LB_REPORT_FORMAT' AND meaning = v_report_format)
        LOOP
            v_code_report_format := r.lookup_code;
        END LOOP;

        BEGIN
            SELECT user_id
              INTO v_user_id
              FROM fnd_user
             WHERE user_name = UPPER (v_user_name);
        EXCEPTION
            WHEN OTHERS
            THEN
                v_user_id := 0;
        END;

        BEGIN
            SELECT responsibility_id, application_id
              INTO v_resp_id, v_resp_appl_id
              FROM fnd_responsibility_vl
             WHERE     responsibility_name = 'HDS Credit Assoc Cash App Mgr - WC'
                   AND SYSDATE BETWEEN start_date AND NVL (end_date, TRUNC (SYSDATE) + 1);
        EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
                l_errbuf := 'Responsibility - ' || 'HDS Credit Assoc Cash App Mgr - WC' || ' not defined in Oracle';
                RAISE l_program_error;
            WHEN OTHERS
            THEN
                l_errbuf :=
                       'Error deriving Responsibility_id for ResponsibilityName - '
                    || 'HDS Credit Assoc Cash App Mgr - WC';
                RAISE l_program_error;
        END;

        fnd_global.apps_initialize (user_id => v_user_id, resp_id => v_resp_id,   --'HDS Credit Assoc Cash App Mgr - WC'
                                                                               resp_appl_id => v_resp_appl_id);

        mo_global.init ('AR');
        mo_global.set_policy_context ('S', p_org_id);
        fnd_request.set_org_id (fnd_global.org_id);
        --Y, , , SG-NON-POA-0429170501, Y, /xx_iface/ebiztrn/inbound/ar/lockbox/WC_PRISM_LOCKBOX_FILE0429170501.txt, XXWCAR_LOCKBOX_PRISM, 2000, Y, N, 1000, , R, N, Y, N, Y, , 162, L
        --Y, , , SG-04232012-POA, Y,             /xx_iface/ebiztrn/inbound/ar/lockbox/WC_PRISM_POA_LOCKBOX_FILE.txt, XXWCAR_LOCKBOX_PRISM, 2000, Y, N, 2000, , R, N, Y, N, Y, , 162, L,
        --N, 3014, 3736239, SG-NON-POA-0429170501, N, , ,                                                                                  2000, Y, N, 1000, , R, N, Y, N, Y, , 162, L,
        v_req_id :=
            fnd_request.submit_request ('AR',
                                        'ARLPLB',
                                        NULL,
                                        NULL,
                                        FALSE,
                                        v_new_trans_flag,         --              VARCHAR2 := 'Y';  -- New Transmission?
                                        NULL,                                                         -- Transmission Id
                                        NULL,                                                     -- Original Request Id
                                        p_trans_name,                                               -- Transmission Name
                                        v_run_import, --                   VARCHAR2 := 'Y';                  -- Submit Import?
                                        v_data_file,                                                        -- Data File
                                        v_control_file,                                                  -- Control File
                                        v_transmission_format_id,                              -- Transmission Format Id
                                        v_run_validation,                                          -- Submit Validation?
                                        'N',                                                  -- Pay Unrelated Invoices?
                                        v_lockbox_id,                                                      -- Lockbox Id
                                        -- Note that the GL Date MUST be sent in the format 'DD-MON-YY', otherwise the concurrent

                                        NULL,                                                                 -- GL Date
                                        v_code_report_format,                                           -- Report Format
                                        'N',                                                   -- Complete Batches Only?
                                        v_run_appl,                                                 -- Submit Postbatch?
                                        'N',                                             -- Alternate name search option
                                        v_invalid_trans_num_handling, -- 'Y' Post Partial Amount or Reject Entire Receipt
                                        NULL,                                                  -- USSGL transaction code
                                        p_org_id,                                                              -- org_id
                                        'L',                                                          -- submission type
                                        NULL,
                                        CHR (0),
                                        CHR (0));
        COMMIT;
        p_request_id := v_req_id;
        xxwc_log ('submitted request for lockbox ' || p_lockbox_name || 'request_id=' || v_req_id);
    EXCEPTION
        WHEN l_program_error
        THEN
            ROLLBACK;
            l_err_code := '2';
            xxwc_log ('PROGRAM_ERROR ' || l_errbuf);
        WHEN OTHERS
        THEN
            ROLLBACK;
            l_err_code := '2';

            l_errbuf :=
                   l_errbuf
                || 'Error_Stack...'
                || CHR (10)
                || DBMS_UTILITY.format_error_stack ()
                || CHR (10)
                || ' Error_Backtrace...'
                || CHR (10)
                || DBMS_UTILITY.format_error_backtrace ();

            xxwc_log (l_errbuf);

            p_request_id := 0;
    END;

    --************************************************************
    --************************************************************
    --************************************************************
    --************************************************************
    --************************************************************
    --************************************************************

    PROCEDURE reverse_receipt (p_errbuf OUT VARCHAR2, p_retcode OUT VARCHAR2, p_org_id NUMBER)
    IS
        l_return_status            VARCHAR2 (240);
        l_msg_count                NUMBER;
        l_msg_data                 VARCHAR2 (1000);
        l_msg_dummy                VARCHAR2 (1000);
        l_receipt_number           ar_cash_receipts.receipt_number%TYPE;
        l_reversal_category_code   ar_cash_receipts.reversal_category%TYPE;
        l_reversal_gl_date         ar_cash_receipt_history.reversal_gl_date%TYPE;
        l_reversal_date            ar_cash_receipts.reversal_date%TYPE;
        l_reversal_reason_code     ar_cash_receipts.reversal_reason_code%TYPE;
        l_reversal_comments        ar_cash_receipts.reversal_comments%TYPE;
        l_attribute_rec            ar_receipt_api_pub.attribute_rec_type;
        l_global_attribute_rec     ar_receipt_api_pub.global_attribute_rec_type;
        v_user_name                VARCHAR2 (164) := 'xxwc_int_finance';
        v_user_id                  NUMBER;
        v_resp_id                  NUMBER;
        v_resp_appl_id             NUMBER;
        v_running_message1         VARCHAR2 (240);

        l_cash_receipt_id          NUMBER;

        CURSOR c_cash_receipts
        IS
            SELECT rt.cash_receipt_id cash_receipt_id, st.ROWID vrowid, st.check_nbr prism_check_nbr
              FROM xxwc.xxwcar_cash_rcpts_tbl st, ar_cash_receipts_all rt
             WHERE                                                            --  st.operator_code IN ('X', 'Y','H','J')
                  st   .receipt_type = 'DEBIT'
                   AND rt.pay_from_customer = st.cust_account_id
                   AND rt.amount = st.check_amt
                   AND st.status = 'NEW'
                   AND rt.receipt_number = st.check_nbr
                   AND rt.reversal_category IS NULL;
    BEGIN
        l_errbuf := NULL;
        l_err_code := '0';
        l_procedure_name := UPPER ('reverse_receipt');
        xxwc_log ('running reversal');
        l_reversal_category_code := 'REV';
        l_reversal_reason_code := 'PAYMENT REVERSAL';
        l_reversal_gl_date := SYSDATE;
        l_reversal_date := SYSDATE;
        l_reversal_comments := 'prism';

        BEGIN
            SELECT user_id
              INTO v_user_id
              FROM fnd_user
             WHERE user_name = UPPER (v_user_name);
        EXCEPTION
            WHEN OTHERS
            THEN
                v_user_id := 0;
        END;

        BEGIN
            SELECT responsibility_id, application_id
              INTO v_resp_id, v_resp_appl_id
              FROM fnd_responsibility_vl
             WHERE     responsibility_name = 'HDS Credit Assoc Cash App Mgr - WC'
                   AND SYSDATE BETWEEN start_date AND NVL (end_date, TRUNC (SYSDATE) + 1);
        EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
                l_errbuf := 'Responsibility - ' || 'HDS Credit Assoc Cash App Mgr - WC' || ' not defined in Oracle';
                RAISE l_program_error;
            WHEN OTHERS
            THEN
                l_errbuf :=
                       'Error deriving Responsibility_id for ResponsibilityName - '
                    || 'HDS Credit Assoc Cash App Mgr - WC';
                xxwc_log (l_errbuf);
                RAISE l_program_error;
        END;

        fnd_global.apps_initialize (user_id => v_user_id, resp_id => v_resp_id,   --'HDS Credit Assoc Cash App Mgr - WC'
                                                                               resp_appl_id => v_resp_appl_id);

        mo_global.init ('AR');
        mo_global.set_policy_context ('S', p_org_id);
        fnd_request.set_org_id (fnd_global.org_id);

        FOR receipts IN c_cash_receipts
        LOOP
            BEGIN
                l_cash_receipt_id := receipts.cash_receipt_id;
                xxwc_log ('Processing (reverse) receipt_number :' || l_receipt_number);
                l_return_status := '';
                l_msg_count := '';
                l_msg_data := '';
                ar_receipt_api_pub.reverse (p_api_version              => 1.0,
                                            p_init_msg_list            => fnd_api.g_false,
                                            p_commit                   => fnd_api.g_false,
                                            p_validation_level         => fnd_api.g_valid_level_full,
                                            x_return_status            => l_return_status,
                                            x_msg_count                => l_msg_count,
                                            x_msg_data                 => l_msg_data,
                                            -- p_receipt_number           => l_receipt_number, --removed by Rasikha
                                            p_cash_receipt_id          => l_cash_receipt_id,
                                            p_reversal_category_code   => l_reversal_category_code,
                                            p_reversal_gl_date         => l_reversal_gl_date,
                                            p_reversal_date            => l_reversal_date,
                                            p_reversal_reason_code     => l_reversal_reason_code,
                                            p_reversal_comments        => l_reversal_comments,
                                            p_attribute_rec            => l_attribute_rec,
                                            p_global_attribute_rec     => l_global_attribute_rec);

                IF (l_return_status = 'S')
                THEN
                    UPDATE xxwc.xxwcar_cash_rcpts_tbl
                       SET status = l_rev_status                                                          -- 'PROCESSED'
                     WHERE ROWID = receipts.vrowid;

                    COMMIT;
                    xxwc_log ('Success processing (reverse) receipt_number :' || l_receipt_number);
                    xxwc_log ('Success');
                    xxwc_log ('Reversed receipt number: ' || l_receipt_number);
                    xxwc_log ('Return Status = ' || SUBSTR (l_return_status, 1, 255));
                    xxwc_log ('Message Count = ' || l_msg_count);
                    xxwc_log ('Message Data = ' || l_msg_data);
                ELSE
                    ROLLBACK;
                    xxwc_log ('error processing (reverse) receipt_number :' || l_receipt_number);
                    xxwc_log ('Return Status = ' || SUBSTR (l_return_status, 1, 255));
                    xxwc_log ('Message Count = ' || TO_CHAR (l_msg_count));
                    xxwc_log ('Message Data = ' || SUBSTR (l_msg_data, 1, 255));
                    xxwc_log (
                        apps.fnd_msg_pub.get (p_msg_index => apps.fnd_msg_pub.g_last, p_encoded => apps.fnd_api.g_false));

                    /*IF l_msg_count >=0 THEN
                      FOR I IN 1..10 LOOP
                             dbms_output.put_line(I||'. '|| SUBSTR (FND_MSG_PUB.Get(p_encoded => FND_API.G_FALSE ), 1, 255));
                      END LOOP;
                    END IF;*/
                    IF l_msg_count > 0
                    THEN
                        v_running_message1 := '';

                        FOR i IN 1 .. l_msg_count
                        LOOP
                            fnd_msg_pub.get (i,
                                             fnd_api.g_false,
                                             l_msg_data,
                                             l_msg_dummy);

                            IF NVL (v_running_message1, 'null') <> l_msg_data
                            THEN
                                xxwc_log (SUBSTR ('Msg' || TO_CHAR (i) || ': ' || l_msg_data, 1, 255));
                            END IF;

                            v_running_message1 := l_msg_data;
                        END LOOP;
                    END IF;
                END IF;
            EXCEPTION
                WHEN OTHERS
                THEN
                    l_errbuf :=
                           'reversal error for cash_receipt_id '
                        || receipts.cash_receipt_id
                        || ' prism check number '
                        || receipts.prism_check_nbr
                        || ' Error_Stack...'
                        || CHR (10)
                        || DBMS_UTILITY.format_error_stack ()
                        || CHR (10)
                        || ' Error_Backtrace...'
                        || CHR (10)
                        || DBMS_UTILITY.format_error_backtrace ();

                    xxwc_log (l_errbuf);
            END;
        END LOOP;

        --UPDATE oracle flag for debit transaction which were already reversed
        FOR r
            IN (SELECT rt.cash_receipt_id cash_receipt_id,
                       st.ROWID vrowid,
                       st.check_nbr prism_check_nbr,
                       rt.reversal_category,
                       st.oracle_flag
                  FROM ar_cash_receipts_all rt,
                       (SELECT *
                          FROM xxwc.xxwcar_cash_rcpts_tbl
                         WHERE     SUBSTR (status, 1, 3) = 'REV'
                               AND (oracle_flag IS NULL OR NVL (oracle_flag, 'null') <> 'in Cash Receipt table')) st
                 WHERE                                                        --  st.operator_code IN ('X', 'Y','H','J')
                      st   .receipt_type = 'DEBIT'
                       AND rt.pay_from_customer = st.cust_account_id
                       AND rt.amount = st.check_amt
                       AND rt.receipt_number = st.check_nbr
                       AND rt.reversal_category = 'REV')
        LOOP
            UPDATE xxwc.xxwcar_cash_rcpts_tbl
               SET oracle_flag = 'in Cash Receipt table'
             WHERE ROWID = r.vrowid;
        END LOOP;

        COMMIT;
    EXCEPTION
        WHEN l_program_error
        THEN
            ROLLBACK;
            l_err_code := '2';
            xxwc_log ('PROGRAM_ERROR ' || l_errbuf);
        WHEN OTHERS
        THEN
            ROLLBACK;
            l_err_code := '2';

            l_errbuf :=
                   'Error_Stack...'
                || CHR (10)
                || DBMS_UTILITY.format_error_stack ()
                || CHR (10)
                || ' Error_Backtrace...'
                || CHR (10)
                || DBMS_UTILITY.format_error_backtrace ();

            xxwc_log (l_errbuf);
    END reverse_receipt;

    --************************************************************
    --************************************************************
    --************************************************************
    --************************************************************
    --************************************************************
    --************************************************************

    PROCEDURE prism_lockbox_process (errbuf                OUT VARCHAR2,
                                     retcode               OUT NUMBER,
                                     p_status           IN     VARCHAR2,
                                     p_lockbox_number   IN     VARCHAR2,
                                     p_request_id          OUT NUMBER)
    IS
        l_file_name        VARCHAR2 (100);
        l_file_dir         VARCHAR2 (100);
        l_file_handle      UTL_FILE.file_type;

        l_header_string    VARCHAR2 (1000);
        l_pmt_string       VARCHAR2 (5000);
        l_ovrflw_string    VARCHAR2 (5000);
        l_ck_cnt           NUMBER := 0;

        l_cnt_ovrflw       NUMBER;

        l_org              hr_all_organization_units.organization_id%TYPE;
        v_status           VARCHAR2 (128);

        v_lockbox_number   VARCHAR2 (64);
    BEGIN
        p_request_id := NULL;
        l_errbuf := NULL;
        l_err_code := '0';
        l_procedure_name := UPPER ('prism_lockbox_process');

        v_status := p_status;
        v_lockbox_number := p_lockbox_number;

        SELECT organization_id
          INTO l_org
          FROM hr_all_organization_units
         WHERE name = l_org_name;

        l_file_dir := 'XXWCAR_LOCKBOX_DIR';                                       --/xx_iface/ebiztrn/inbound/ar/lockbox

        l_file_name := v_status || '.txt';

        xxwc_log ('Filename generated : ' || l_file_name);

        l_file_handle := UTL_FILE.fopen (l_file_dir, l_file_name, 'w');

        FOR r_header
            IN (  SELECT DISTINCT l.lockbox_number,
                                  r.deposit_date,
                                  '4100067610' dest_acct,
                                  l.bank_origination_number,
                                  r.operator_code operator_code
                    FROM xxwc.xxwcar_cash_rcpts_tbl r,
                         (SELECT li.lockbox_number, li.bank_origination_number
                            FROM ar.ar_lockboxes_all li
                           WHERE li.status = 'A' AND li.org_id = l_org AND li.lockbox_number = v_lockbox_number) l,
                         (SELECT hi.lookup_code, hi.end_date_active, hi.meaning
                            FROM apps.fnd_lookup_values hi
                           WHERE hi.lookup_type = 'XXWC_AR_LOCKBOX_DCTM' AND hi.enabled_flag = 'Y') h
                   WHERE     UPPER (r.operator_code) = h.lookup_code
                         AND NVL (h.end_date_active, SYSDATE) >= SYSDATE
                         AND h.meaning = l.lockbox_number
                         AND r.status = p_status
                ORDER BY 1, 2)
        LOOP
            l_header_string :=
                   '2'
                || RPAD (r_header.lockbox_number, 10, ' ')
                || TO_CHAR (r_header.deposit_date, 'DD-MON-YY')
                || RPAD (r_header.dest_acct, 24, ' ')
                || r_header.bank_origination_number;
            UTL_FILE.put_line (l_file_handle, l_header_string);

            FOR r_line
                IN (  SELECT stag_rec.ROWID,
                             stag_rec.*,
                             NVL (stag_rec.oracle_account_nbr, stag_rec.customer_nbr) account_number
                        FROM xxwc.xxwcar_cash_rcpts_tbl stag_rec
                       WHERE                              -- UPPER (stag_rec.operator_code) = r_header.operator_code AND
                            stag_rec.status = p_status AND stag_rec.deposit_date = r_header.deposit_date
                    ORDER BY stag_rec.check_nbr, stag_rec.invoice_amt)
            LOOP
                l_ck_cnt := l_ck_cnt + 1;                                                       --line nmber in the file
                l_pmt_string :=
                       '3'
                    || RPAD (r_header.lockbox_number, 10, ' ')
                    || LPAD (l_ck_cnt, 4, '0')
                    || LPAD (r_line.check_amt, 16, ' ')
                    || RPAD (r_line.check_nbr, 20, ' ')
                    || 'USD'
                    || RPAD (NVL (r_line.account_number, ' '), 14, ' ')
                    || RPAD (TO_CHAR (r_header.deposit_date, 'DD-MON-YY'), 10, ' ')
                    || r_line.comments;

                UTL_FILE.put_line (l_file_handle, l_pmt_string);
                l_cnt_ovrflw := 9;                                                                          --one record
                l_ovrflw_string :=
                       '4'
                    || RPAD (r_header.lockbox_number, 10, ' ')
                    || LPAD (l_ck_cnt, 4, '0')
                    || LPAD ('1', 3, '0')
                    || l_cnt_ovrflw
                    || RPAD (NVL (r_line.invoice_nbr, 'ZZZZZ'), 19, ' ')
                    || LPAD (NVL (r_line.invoice_amt, r_line.check_amt), 17, ' ');
                UTL_FILE.put_line (l_file_handle, l_ovrflw_string);
            END LOOP;
        END LOOP;

        UTL_FILE.fclose (l_file_handle);

        DECLARE
            v_request_id   NUMBER;
            v_trans_name   VARCHAR2 (128);
        BEGIN
            v_trans_name := 'SG_' || v_status;
            xxwc_ar_cash_receipt_lb_util.xxwc_prism_lb_import_validate (l_org_id,
                                                                        v_trans_name,
                                                                        l_file_name,
                                                                        'XXWCAR_LOCKBOX_PRISM',
                                                                        'XXWC_LOCKBOX_PRISM',
                                                                        v_lockbox_number,
                                                                        'Rejects Only',
                                                                        v_request_id);
            p_request_id := v_request_id;
        END;
    EXCEPTION
        WHEN OTHERS
        THEN
            ROLLBACK;

            BEGIN
                UTL_FILE.fclose (l_file_handle);
                UTL_FILE.fremove (l_file_dir, l_file_name);
            EXCEPTION
                WHEN OTHERS
                THEN
                    NULL;
            END;

            l_err_code := '2';
            l_errbuf := 'error when creating file for lockbox:' || p_lockbox_number || ' status:' || p_status;
            l_errbuf :=
                   l_errbuf
                || ' Error_Stack...'
                || CHR (10)
                || DBMS_UTILITY.format_error_stack ()
                || CHR (10)
                || ' Error_Backtrace...'
                || CHR (10)
                || DBMS_UTILITY.format_error_backtrace ();
            retcode := l_err_code;
            errbuf := l_errbuf;
            xxwc_log (l_errbuf);
    END;

    --************************************************************
    --************************************************************
    --************************************************************
    --************************************************************
    --************************************************************
    --************************************************************
    PROCEDURE xxwc_validate_prism_account (p_org_id NUMBER)
    IS
        v_org_id         NUMBER;

        v_create_table   VARCHAR2 (1);
    BEGIN
        v_org_id := p_org_id;

        BEGIN
            SELECT '1'
              INTO v_create_table
              FROM all_tables
             WHERE table_name = 'PRISM_CUST_MAP' AND owner = 'XXWC';
        EXCEPTION
            WHEN OTHERS
            THEN
                EXECUTE IMMEDIATE '
CREATE TABLE xxwc.prism_cust_map
(
    prism_customer_number   VARCHAR2 (256),
    hca_cust_account_id     NUMBER,
    hcas_attribute17        VARCHAR2 (512),
    hca_account_number      VARCHAR2 (256),
    hcas_cust_account_id    NUMBER,
    hca_attribute6          VARCHAR2 (512),
    status  VARCHAR2 (1024)
)';
        END;

        EXECUTE IMMEDIATE 'alter table  xxwc.prism_cust_map nologging';

        EXECUTE IMMEDIATE 'truncate table xxwc.prism_cust_map';

        INSERT /*+append*/
              INTO  xxwc.prism_cust_map (prism_customer_number)
            SELECT DISTINCT customer_nbr
              FROM xxwc.xxwcar_cash_rcpts_tbl
             WHERE receipt_type IN ('CASH', 'DEPOSIT', 'POA', 'DEBIT') AND status = 'NEW';

        COMMIT;

        --select * from xxwc.prism_cust_map
        --find dup attribute17 on site level

        BEGIN
            FOR r IN (SELECT ROWID r_id
                        FROM xxwc.prism_cust_map
                       WHERE prism_customer_number IN (SELECT attribute17
                                                         FROM (  SELECT COUNT (cust_account_id), attribute17
                                                                   FROM (SELECT DISTINCT cust_account_id, attribute17
                                                                           FROM hz_cust_acct_sites_all
                                                                          WHERE org_id = v_org_id AND status = 'A')
                                                               GROUP BY attribute17
                                                                 HAVING COUNT (cust_account_id) > 1)))
            LOOP
                UPDATE xxwc.prism_cust_map
                   SET status = 'Found duplicate attribute17;'
                 WHERE ROWID = r.r_id;
            END LOOP;

            COMMIT;
        END;

        --select * from xxwc.prism_cust_map
        BEGIN
            FOR r
                IN (SELECT hcas.cust_account_id, hcas.attribute17, pr.ROWID r_id
                      FROM hz_cust_acct_sites_all hcas, xxwc.prism_cust_map pr
                     WHERE     hcas.org_id = v_org_id
                           AND hcas.status = 'A'
                           AND hcas.attribute17 = pr.prism_customer_number
                           AND NVL (pr.status, 'x') NOT LIKE '%duplicate%')
            LOOP
                UPDATE xxwc.prism_cust_map
                   SET hcas_attribute17 = r.attribute17, hcas_cust_account_id = r.cust_account_id
                 WHERE ROWID = r.r_id;
            END LOOP;

            COMMIT;
        END;

        UPDATE xxwc.prism_cust_map
           SET status = status || ' Prism_customer_number has not matching attribute17 in hz_cust_acct_sites_all table;'
         WHERE hcas_attribute17 IS NULL AND NVL (status, 'x') NOT LIKE '%duplicate%';

        COMMIT;

        BEGIN
            FOR r
                IN (SELECT hca.account_number,
                           hca.attribute6,
                           hca.cust_account_id,
                           pr.ROWID r_id
                      FROM hz_cust_accounts hca, xxwc.prism_cust_map pr
                     WHERE     NVL (hcas_attribute17, pr.prism_customer_number) = hca.account_number
                           AND hca.status = 'A'
                           AND NVL (pr.status, 'x') NOT LIKE '%duplicate%')
            LOOP
                UPDATE xxwc.prism_cust_map
                   SET hca_cust_account_id = r.cust_account_id,
                       hca_account_number = r.account_number,
                       hca_attribute6 = r.attribute6
                 WHERE ROWID = r.r_id;
            END LOOP;

            COMMIT;
        END;

        UPDATE xxwc.prism_cust_map
           SET status = status || ' hca_account_number found by prism_customer_number in hz_cust_accounts;'
         WHERE hca_account_number IS NOT NULL AND NVL (status, 'x') NOT LIKE '%duplicate%';

        COMMIT;

        UPDATE xxwc.prism_cust_map
           SET status = status || 'hca_cust_account_id<>hcas_cust_account_id;'
         WHERE     hca_account_number IS NOT NULL
               AND hca_cust_account_id <> hcas_cust_account_id
               AND NVL (status, 'x') NOT LIKE '%duplicate%';

        COMMIT;

        BEGIN
            FOR r
                IN (SELECT hca.account_number,
                           hca.attribute6,
                           hca.cust_account_id,
                           pr.ROWID r_id
                      FROM hz_cust_accounts hca, xxwc.prism_cust_map pr
                     WHERE     pr.hcas_cust_account_id = hca.cust_account_id
                           AND hca.status = 'A'
                           AND NVL (pr.status, 'x') NOT LIKE '%duplicate%')
            LOOP
                UPDATE xxwc.prism_cust_map
                   SET hca_cust_account_id = r.cust_account_id,
                       hca_account_number = r.account_number,
                       hca_attribute6 = r.attribute6,
                       status =
                              status
                           || ' hca_account_number found by hcas_cust_account_id(prism_cust_map) in hz_cust_accounts;'
                 WHERE ROWID = r.r_id;
            END LOOP;

            COMMIT;
        END;

        BEGIN
            FOR r IN (SELECT m.ROWID rt, m.*
                        FROM xxwc.xxwcar_cash_rcpts_tbl m
                       WHERE m.receipt_type IN ('CASH', 'DEPOSIT', 'POA', 'DEBIT') AND status = 'NEW')
            LOOP
                FOR r1
                    IN (SELECT NVL (pr.hca_cust_account_id, 0) cust_account_id, pr.hca_account_number
                          FROM xxwc.prism_cust_map pr
                         WHERE     pr.prism_customer_number = r.customer_nbr
                               AND NVL (pr.status, 'x') NOT LIKE '%duplicate%')
                LOOP
                    UPDATE xxwc.xxwcar_cash_rcpts_tbl
                       SET oracle_account_nbr = r1.hca_account_number, cust_account_id = r1.cust_account_id
                     WHERE ROWID = r.rt;
                END LOOP;

                FOR r1
                    IN (SELECT 'x'
                          FROM xxwc.prism_cust_map pr
                         WHERE pr.prism_customer_number = r.customer_nbr AND NVL (pr.status, 'x') LIKE '%duplicate%')
                LOOP
                    UPDATE xxwc.xxwcar_cash_rcpts_tbl
                       SET oracle_flag = 'Found duplicate attribute17;', cust_account_id = 0
                     WHERE ROWID = r.rt;
                END LOOP;
            END LOOP;

            COMMIT;
        END;
    EXCEPTION
        WHEN OTHERS
        THEN
            l_procedure_name := 'xxwc_validate_prism_account';
            l_err_code := '2';

            l_errbuf :=
                   ' Error_Stack...'
                || CHR (10)
                || DBMS_UTILITY.format_error_stack ()
                || CHR (10)
                || ' Error_Backtrace...'
                || CHR (10)
                || DBMS_UTILITY.format_error_backtrace ();

            xxwc_log (l_errbuf);
    END;

    --************************************************************
    --************************************************************
    --************************************************************
    --************************************************************
    --************************************************************
    --************************************************************
    PROCEDURE xxwc_verify_receipts (p_org_id NUMBER)
    IS
        v_org_id   NUMBER;
    BEGIN
        v_org_id := p_org_id;

        BEGIN
            FOR r IN (SELECT k.ROWID rd, k.*
                        FROM xxwc.xxwcar_cash_rcpts_tbl k
                       WHERE k.receipt_type IN ('CASH', 'DEPOSIT', 'POA', 'DEBIT') AND k.oracle_flag IS NULL)
            LOOP
                FOR r1
                    IN (SELECT acr.receipt_date deposit_date,
                               acr.amount,
                               acr.receipt_number check_nbr,                                           --receipt_amount,
                               cust.account_number,                                                   --customer_number,
                               ps.trx_number,
                               ps.trx_date,
                               ara.amount_applied,
                               ps.amount_due_original,
                               ps.amount_due_remaining,
                               ps.gl_date,
                               hro.name org,
                               hro.organization_id
                          FROM ar.ar_cash_receipts_all acr,
                               ar.ar_receivable_applications_all ara,
                               ar.ar_payment_schedules_all ps,
                               apps.hz_parties rc,
                               apps.hz_cust_accounts cust,
                               hr.hr_all_organization_units hro
                         WHERE     ps.customer_id = cust.cust_account_id
                               AND cust.party_id = rc.party_id
                               AND acr.cash_receipt_id = ara.cash_receipt_id
                               AND ara.payment_schedule_id = ps.payment_schedule_id
                               AND acr.org_id = hro.organization_id
                               AND acr.receipt_date = r.deposit_date
                               AND acr.receipt_number = r.check_nbr
                               AND cust.account_number = r.oracle_account_nbr
                               AND cust.cust_account_id = r.cust_account_id
                               AND acr.amount = r.check_amt
                               AND ps.trx_number IN (r.invoice_nbr, r.check_nbr)
                               -- AND ps.trx_number = r.invoice_nbr
                               AND acr.org_id = v_org_id)
                LOOP
                    UPDATE xxwc.xxwcar_cash_rcpts_tbl
                       SET oracle_flag = 'in Cash Receipt table'
                     WHERE ROWID = r.rd;
                END LOOP;
            END LOOP;

            COMMIT;
        END;

        BEGIN
            FOR r IN (SELECT k.ROWID rd, k.*
                        FROM xxwc.xxwcar_cash_rcpts_tbl k
                       WHERE k.receipt_type IN ('CASH', 'DEPOSIT', 'POA', 'DEBIT') AND k.oracle_flag IS NULL)
            LOOP
                FOR r1
                    IN (SELECT record_type,
                               status,
                               check_number,
                               customer_number,
                               receipt_date,
                               org_id
                          FROM ar_payments_interface_all
                         WHERE     source_type_flag = 'L'
                               AND org_id = v_org_id
                               AND receipt_date = r.deposit_date
                               AND check_number = r.check_nbr
                               AND customer_number = r.oracle_account_nbr
                               AND remittance_amount = r.check_amt)
                LOOP
                    UPDATE xxwc.xxwcar_cash_rcpts_tbl
                       SET oracle_flag = 'in Interface Table status=' || r1.status
                     WHERE ROWID = r.rd;
                END LOOP;
            END LOOP;

            COMMIT;
        END;
    EXCEPTION
        WHEN OTHERS
        THEN
            l_procedure_name := 'xxwc_verify_receipts';
            l_err_code := '2';

            l_errbuf :=
                   ' Error_Stack...'
                || CHR (10)
                || DBMS_UTILITY.format_error_stack ()
                || CHR (10)
                || ' Error_Backtrace...'
                || CHR (10)
                || DBMS_UTILITY.format_error_backtrace ();

            xxwc_log (l_errbuf);
    END;

    --************************************************************
    --************************************************************
    --************************************************************
    PROCEDURE assign_cash_appl_work_items (p_errbuf OUT VARCHAR2, p_retcode OUT VARCHAR2, p_request_id OUT NUMBER)
    IS
        v_resp_id             NUMBER;
        v_resp_appl_id        NUMBER;
        v_errbuf              CLOB;
        v_org_id              NUMBER;
        v_org_name   CONSTANT hr_all_organization_units.name%TYPE := 'HDS White Cap - Org';
        v_exit_exception      EXCEPTION;
        v_package_name        VARCHAR2 (256) := 'call for request  Assign Cash Application Work Items';
        v_procedure_name      VARCHAR2 (256) := 'parameters 162,,,,,1';
        v_err_code            VARCHAR2 (12) := '0';
        v_req_id              NUMBER;

        v_distro_list         VARCHAR2 (256) := 'HDSOracleDevelopers@hdsupply.com';
        v_user_name           VARCHAR2 (164) := 'xxwc_int_finance';
        v_user_id             NUMBER;
    BEGIN
        BEGIN
            SELECT organization_id
              INTO v_org_id
              FROM hr_all_organization_units
             WHERE name = v_org_name;
        EXCEPTION
            WHEN OTHERS
            THEN
                v_errbuf := 'org_id not found for =' || v_org_name;
                RAISE v_exit_exception;
        END;

        BEGIN
            SELECT user_id
              INTO v_user_id
              FROM fnd_user
             WHERE user_name = UPPER (v_user_name);
        EXCEPTION
            WHEN OTHERS
            THEN
                v_user_id := 0;
        END;

        BEGIN
            SELECT responsibility_id, application_id
              INTO v_resp_id, v_resp_appl_id
              FROM fnd_responsibility_vl
             WHERE     responsibility_name = 'HDS Credit Manager Credit & Collections - WC'
                   AND SYSDATE BETWEEN start_date AND NVL (end_date, TRUNC (SYSDATE) + 1);
        EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
                v_errbuf :=
                    'Responsibility - ' || 'HDS Credit Manager Credit & Collections - WC' || ' not defined in Oracle';
                RAISE v_exit_exception;
            WHEN OTHERS
            THEN
                v_errbuf :=
                       'Error deriving Responsibility_id for ResponsibilityName - '
                    || 'HDS Credit Assoc Cash App Mgr - WC';
                RAISE v_exit_exception;
        END;

        fnd_global.apps_initialize (user_id => v_user_id, resp_id => v_resp_id, --'HDS Credit Manager Credit & Collections - WC'
                                                                               resp_appl_id => v_resp_appl_id);

        mo_global.init ('AR');
        mo_global.set_policy_context ('S', v_org_id);
        fnd_request.set_org_id (fnd_global.org_id);

        v_req_id :=
            fnd_request.submit_request ('AR',                                                              --application
                                        'ARCAOAB',                                                            -- program
                                        '',                                                                --description
                                        '',                                                                 --start_time
                                        FALSE,                                                             --sub_request
                                        v_org_id,                                                       --operating unit
                                        '',                                                          --receipt date from
                                        '',                                                            --receipt date to
                                        '',                                                     --customer profile class
                                        1,
                                        '',                                                        --number of instances
                                        CHR (0),
                                        CHR (0));
        COMMIT;
        p_request_id := v_req_id;
        COMMIT;
        DBMS_OUTPUT.put_line ('request_id for Assign Cash Application Work Items is ' || v_req_id);

        DBMS_OUTPUT.put_line (' v_err_code ' || v_err_code);
        p_errbuf := v_errbuf;
        p_retcode := v_err_code;
    EXCEPTION
        WHEN OTHERS
        THEN
            v_errbuf :=
                   v_errbuf
                || 'Error_Stack...'
                || CHR (10)
                || DBMS_UTILITY.format_error_stack ()
                || CHR (10)
                || ' Error_Backtrace...'
                || CHR (10)
                || DBMS_UTILITY.format_error_backtrace ();

            v_err_code := '2';
            DBMS_OUTPUT.put_line (' v_err_code ' || v_err_code);
            xxcus_error_pkg.xxcus_error_main_api (
                p_called_from         => v_package_name,
                p_calling             => v_procedure_name,
                p_request_id          => NULL,
                p_ora_error_msg       => SUBSTR (v_errbuf, 1, 2000),
                p_error_desc          => 'Error running ' || v_package_name || '.' || v_procedure_name,
                p_distribution_list   => v_distro_list,
                p_module              => 'XXWC');
            p_errbuf := v_errbuf;
            p_retcode := v_err_code;
    END assign_cash_appl_work_items;
--************************************************************
--************************************************************
--************************************************************

END;
/

-- Grants for Package Body
GRANT EXECUTE ON apps.xxwc_ar_cash_receipt_lb_util TO xxwc_prism_execute_role
/


-- End of DDL Script for Package Body APPS.XXWC_AR_CASH_RECEIPT_LB_UTIL

