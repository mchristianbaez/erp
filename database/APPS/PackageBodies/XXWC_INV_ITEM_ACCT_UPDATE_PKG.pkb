CREATE OR REPLACE PACKAGE BODY APPS.XXWC_INV_ITEM_ACCT_UPDATE_PKG
IS
   /******************************************************************************************************
   -- File Name: XXWC_INV_ITEM_ACCT_UPDATE_PKG.pkb
   --
   -- PROGRAM TYPE: Package body
   --
   -- PURPOSE: Package created to update accounts, create ccid.
   -- HISTORY
   -- ==========================================================================================================
   -- ==========================================================================================================
   -- VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- --------------------------------------------------------------------
   -- 1.0     21-OCT-2016   P.Vamshidhar    TMS#20160122-00091  GL Code Enhancements to leverage GL API
   --                                       Initial version
   -- 1.1     01-Feb-2017   P. Vamshidhar   TMS#20170116-00091  - @ PERF  -  XXWC INV Item GL Codes Update
   -- 1.2     04-May-2017   P. Vamshidhar   TMS#20170119-00013 - Need One time data fix to update Expense 4th segment
   -- 1.3     11-10-2017    S.Neha          TMS#Task ID: 20160921-00268  - schedyling add new prd in bpa concurrent prog
   ************************************************************************************************************/

   g_distro_list   VARCHAR2 (100) := 'HDSOracleDevelopers@hdsupply.com';
   g_user_id       PLS_INTEGER := fnd_global.user_id;


   FUNCTION item_accts_update (p_item_id          IN NUMBER,
                               p_org_id           IN NUMBER,
                               p_old_sales_id     IN NUMBER,
                               p_old_cogs_id      IN NUMBER,
                               p_old_expn_id      IN NUMBER,
                               p_new_sales_ccid   IN NUMBER,
                               p_new_cogs_ccid    IN NUMBER,
                               p_new_expn_ccid    IN NUMBER)
      RETURN BOOLEAN
   IS
      /******************************************************************************************************
      -- Object Name: item_accts_update
      --
      -- Object Type: Function
      --
      -- PURPOSE: Created function to update account for item
      -- HISTORY
      -- ==========================================================================================================
      -- ==========================================================================================================
      -- VERSION DATE          AUTHOR(S)       DESCRIPTION
      -- ------- -----------   --------------- --------------------------------------------------------------------
      -- 1.0     21-OCT-2016   P.Vamshidhar    TMS#20160122-00091  GL Code Enhancements to leverage GL API
      --                                       Initial version
      ************************************************************************************************************/

      x_return_status   VARCHAR2 (1000);
      x_msg_count       NUMBER;
      l_item_table      EGO_Item_PUB.Item_Tbl_Type;
      x_item_table      EGO_Item_PUB.Item_Tbl_Type;
   BEGIN
      l_item_table (1).Transaction_Type := 'UPDATE';
      l_item_table (1).inventory_item_id := p_item_id;
      l_item_table (1).organization_id := p_org_id;

      IF p_new_cogs_ccid IS NOT NULL
      THEN
         l_item_table (1).cost_of_sales_account := p_new_cogs_ccid;
         addlogtable (p_item_id,
                      p_org_id,
                      'COGS',
                      p_old_cogs_id,
                      p_new_cogs_ccid,
                      'UPDATE',
                      NULL);
      END IF;

      IF p_new_sales_ccid IS NOT NULL
      THEN
         l_item_table (1).sales_account := p_new_sales_ccid;
         addlogtable (p_item_id,
                      p_org_id,
                      'SALES',
                      p_old_sales_id,
                      p_new_sales_ccid,
                      'UPDATE',
                      NULL);
      END IF;

      IF p_new_expn_ccid IS NOT NULL
      THEN
         l_item_table (1).expense_account := p_new_expn_ccid;
         addlogtable (p_item_id,
                      p_org_id,
                      'EXPENSE',
                      p_old_expn_id,
                      p_new_expn_ccid,
                      'UPDATE',
                      NULL);
      END IF;


      EGO_ITEM_PUB.Process_Items (
         p_api_version      => 1.0,
         p_init_msg_list    => FND_API.g_TRUE,
         p_commit           => FND_API.g_TRUE,
         p_Item_Tbl         => l_item_table,
         x_Item_Tbl         => x_item_table,
         P_ROLE_GRANT_TBL   => EGO_ITEM_PUB.G_MISS_ROLE_GRANT_TBL,
         x_return_status    => x_return_status,
         x_msg_count        => x_msg_count);

      IF x_return_status = 'S'
      THEN
         COMMIT;
         RETURN TRUE;
      ELSE
         ROLLBACK;
         RETURN FALSE;
      END IF;
   END;


   PROCEDURE printlog (p_message IN VARCHAR2)
   IS
   /******************************************************************************************************
   -- Object Name: printlog
   --
   -- Object Type: Procedure
   --
   -- PURPOSE: Procedure to log messages (For concurrent Program purpose)
   -- HISTORY
   -- ==========================================================================================================
   -- ==========================================================================================================
   -- VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- --------------------------------------------------------------------
   -- 1.0     21-OCT-2016   P.Vamshidhar    TMS#20160122-00091  GL Code Enhancements to leverage GL API
   --                                       Initial version
   ************************************************************************************************************/

   BEGIN
      apps.fnd_file.put_line (apps.fnd_file.LOG, p_message);
      DBMS_OUTPUT.put_line (p_message);
   END printlog;

   PROCEDURE addlogtable (p_item_id         IN NUMBER,
                          p_org_id          IN NUMBER,
                          p_acct_type       IN VARCHAR2,
                          p_old_acct_ccid   IN NUMBER,
                          p_new_acct_ccid   IN NUMBER,
                          p_status          IN VARCHAR2,
                          p_comments        IN VARCHAR2)
   IS
   /******************************************************************************************************
   -- Object Name: addlogtable
   --
   -- Object Type: Procedure
   --
   -- PURPOSE: Procedure to log account updates into log table.
   -- HISTORY
   -- ==========================================================================================================
   -- ==========================================================================================================
   -- VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- --------------------------------------------------------------------
   -- 1.0     21-OCT-2016   P.Vamshidhar    TMS#20160122-00091  GL Code Enhancements to leverage GL API
   --                                       Initial version
   ************************************************************************************************************/
   BEGIN
      INSERT INTO XXWC.XXWC_INV_ITEM_ACCT_UPDATE_LOG (INVENTORY_ITEM_ID,
                                                      ORGANIZATION_ID,
                                                      ACCOUNT_TYPE,
                                                      OLD_CCID,
                                                      NEW_CCID,
                                                      STATUS,
                                                      CREATED_BY,
                                                      COMMENTS)
           VALUES (p_item_id,
                   p_org_id,
                   p_acct_type,
                   p_old_acct_ccid,
                   p_new_acct_ccid,
                   p_status,
                   g_user_id,
                   p_comments);
   END;



   FUNCTION create_ccid (p_concat_segs IN VARCHAR2)
      RETURN NUMBER
   IS
      /******************************************************************************************************
      -- Object Name: create_ccid
      --
      -- Object Type: Function
      --
      -- PURPOSE: Function to create ccid.
      -- HISTORY
      -- ==========================================================================================================
      -- ==========================================================================================================
      -- VERSION DATE          AUTHOR(S)       DESCRIPTION
      -- ------- -----------   --------------- --------------------------------------------------------------------
      -- 1.0     21-OCT-2016   P.Vamshidhar    TMS#20160122-00091  GL Code Enhancements to leverage GL API
      --                                       Initial version
      ************************************************************************************************************/
      lvc_concat_segs     GL_CODE_COMBINATIONS_KFV.CONCATENATED_SEGMENTS%TYPE
                             := p_concat_segs;
      l_keyval_status     BOOLEAN;
      ln_coa_id           GL_LEDGERS.CHART_OF_ACCOUNTS_ID%TYPE;
      ln_ccid             GL_CODE_COMBINATIONS.CODE_COMBINATION_ID%TYPE;
      lvc_enabled_flag    VARCHAR (1);
      ln_ledger_id        GL_LEDGERS.LEDGER_ID%TYPE := 2061;
      xx_exp_data_error   EXCEPTION;
   BEGIN
      BEGIN
         SELECT code_combination_id, enabled_flag
           INTO ln_ccid, lvc_enabled_flag
           FROM gl_code_combinations
          WHERE    segment1
                || '.'
                || segment2
                || '.'
                || segment3
                || '.'
                || segment4
                || '.'
                || segment5
                || '.'
                || segment6
                || '.'
                || segment7 = p_concat_segs;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            printlog (
               p_concat_segs || ' Code Combination Does Not Exist in GL.');
            ln_ccid := NULL;
      END;

      IF ln_ccid IS NOT NULL
      THEN
         IF lvc_enabled_flag = 'Y'
         THEN
            RETURN ln_ccid;
         ELSE
            RETURN NULL;
         END IF;
      ELSE
         -- validating set_of_books_id
         IF NVL (ln_ledger_id, 0) = 0
         THEN
            printlog (ln_ledger_id || ' Chart Of Accounts Id Not Found');
            RAISE xx_exp_data_error;
         ELSE
            BEGIN
               SELECT chart_of_accounts_id
                 INTO ln_coa_id
                 FROM gl_sets_of_books
                WHERE set_of_books_id = ln_ledger_id;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  printlog (
                     'Chart of Accounts ID not found from profile option GL_SET_OF_BKS_ID');
                  RAISE;
            END;
         END IF;


         ln_ccid := NULL;
         -- keyval_mode can be one of CREATE_COMBINATION CHECK_COMBINATION FIND_COMBINATION
         l_keyval_status :=
            fnd_flex_keyval.validate_segs (
               operation          => 'CHECK_COMBINATION',
               appl_short_name    => 'SQLGL',
               key_flex_code      => 'GL#',
               structure_number   => ln_coa_id,
               concat_segments    => lvc_concat_segs);

         IF l_keyval_status
         THEN
            -- create will only work if dynamic inserts on and cross validation rules not broken
            l_keyval_status :=
               fnd_flex_keyval.validate_segs (
                  operation          => 'CREATE_COMBINATION',
                  appl_short_name    => 'SQLGL',
                  key_flex_code      => 'GL#',
                  structure_number   => ln_coa_id,
                  concat_segments    => lvc_concat_segs);

            IF l_keyval_status
            THEN
               BEGIN
                  SELECT code_combination_id
                    INTO ln_ccid
                    FROM gl_code_combinations
                   WHERE        segment1
                             || '.'
                             || segment2
                             || '.'
                             || segment3
                             || '.'
                             || segment4
                             || '.'
                             || segment5
                             || '.'
                             || segment6
                             || '.'
                             || segment7 = p_concat_segs
                         AND ENABLED_FLAG = 'Y';
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     ln_ccid := NULL;
                  WHEN OTHERS
                  THEN
                     ln_ccid := NULL;
                     printlog ('Error Occured ' || SUBSTR (SQLERRM, 1, 250));
               END;
            ELSE
               ln_ccid := NULL;
            END IF;
         ELSE
            ln_ccid := NULL;
         END IF;
      END IF;

      RETURN ln_ccid;
   EXCEPTION
      WHEN XX_EXP_DATA_ERROR
      THEN
         ln_ccid := NULL;
         RETURN ln_ccid;
      WHEN NO_DATA_FOUND
      THEN
         printlog ('Unexpected Error');
         ln_ccid := NULL;
         RETURN ln_ccid;
   END create_ccid;

   FUNCTION accts_update (p_org_id IN NUMBER, p_item_id NUMBER)
      RETURN BOOLEAN
   IS
      /******************************************************************************************************
      -- Object Name: accts_update
      --
      -- Object Type: Function
      --
      -- PURPOSE: Function to update accounts item
      -- HISTORY
      -- ==========================================================================================================
      -- ==========================================================================================================
      -- VERSION DATE          AUTHOR(S)       DESCRIPTION
      -- ------- -----------   --------------- --------------------------------------------------------------------
      -- 1.0     21-OCT-2016   P.Vamshidhar    TMS#20160122-00091  GL Code Enhancements to leverage GL API
      --                                       Initial version
      ************************************************************************************************************/
      item_cogs_segs     VARCHAR2 (240);
      item_sales_segs    VARCHAR2 (240);
      item_expn_segs     VARCHAR2 (240);
      cat_cogs           NUMBER;
      cat_sales          NUMBER;
      cat_expn           NUMBER;
      cat_sales_segs     VARCHAR2 (240);
      cat_cogs_segs      VARCHAR2 (240);
      cat_expn_segs      VARCHAR2 (240);
      org_cogs_ccid      GL_CODE_COMBINATIONS.CODE_COMBINATION_ID%TYPE;
      org_sales_ccid     GL_CODE_COMBINATIONS.CODE_COMBINATION_ID%TYPE;
      org_expn_ccid      GL_CODE_COMBINATIONS.CODE_COMBINATION_ID%TYPE;
      item_cogs_ccid     GL_CODE_COMBINATIONS.CODE_COMBINATION_ID%TYPE;
      item_sales_ccid    GL_CODE_COMBINATIONS.CODE_COMBINATION_ID%TYPE;
      item_expn_ccid     GL_CODE_COMBINATIONS.CODE_COMBINATION_ID%TYPE;
      new_sales_ccid     GL_CODE_COMBINATIONS.CODE_COMBINATION_ID%TYPE;
      new_cogs_ccid      GL_CODE_COMBINATIONS.CODE_COMBINATION_ID%TYPE;
      new_expn_ccid      GL_CODE_COMBINATIONS.CODE_COMBINATION_ID%TYPE;
      lvc_acct_updated   BOOLEAN;
   BEGIN
      BEGIN
         SELECT cogs_gcc.concatenated_segments,
                msib.cost_of_sales_account,
                sales_gcc.concatenated_segments,
                msib.sales_account,
                expn_gcc.concatenated_segments,
                msib.expense_account
           INTO item_cogs_segs,
                item_cogs_ccid,
                item_sales_segs,
                item_sales_ccid,
                item_expn_segs,
                item_expn_ccid
           FROM APPS.MTL_SYSTEM_ITEMS_B msib,
                apps.gl_code_combinations_kfv cogs_gcc,
                apps.gl_code_combinations_kfv sales_gcc,
                apps.gl_code_combinations_kfv expn_gcc
          WHERE     msib.organization_id = p_org_id
                AND msib.inventory_item_id = p_item_id
                AND msib.cost_of_sales_account =
                       cogs_gcc.code_combination_id(+)
                AND cogs_gcc.enabled_flag = 'Y'
                AND msib.sales_account = sales_gcc.code_combination_id(+)
                AND sales_gcc.enabled_flag = 'Y'
                AND msib.expense_account = expn_gcc.code_combination_id(+)
                AND expn_gcc.enabled_flag = 'Y';
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            item_cogs_segs := NULL;
            item_sales_segs := NULL;
            item_expn_segs := NULL;
            item_cogs_ccid := NULL;
            item_sales_ccid := NULL;
            item_expn_ccid := NULL;
      END;

      BEGIN
         SELECT ATTRIBUTE1, ATTRIBUTE2, ATTRIBUTE8
           INTO cat_cogs, cat_sales, cat_expn
           FROM APPS.MTL_ITEM_CATEGORIES MIC, APPS.MTL_CATEGORIES_B MC
          WHERE     MIC.CATEGORY_SET_ID = 1100000062
                AND MIC.INVENTORY_ITEM_ID = p_item_id
                AND MIC.CATEGORY_ID = MC.CATEGORY_ID
                AND MIC.ORGANIZATION_ID = 222;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            cat_cogs := NULL;
            cat_sales := NULL;
            cat_expn := NULL;
      END;

      IF    cat_cogs IS NOT NULL
         OR cat_sales IS NOT NULL
         OR cat_expn IS NOT NULL
      THEN
         BEGIN
            SELECT                                           -- Sales Segments
                  sales_gcc.segment1
                   || '.'
                   || sales_gcc.segment2
                   || '.'
                   || sales_gcc.segment3
                   || '.'
                   || cat_sales
                   || '.'
                   || sales_gcc.segment5
                   || '.'
                   || sales_gcc.segment6
                   || '.'
                   || sales_gcc.segment7,
                      -- COGS Segments
                      cogs_gcc.segment1
                   || '.'
                   || cogs_gcc.segment2
                   || '.'
                   || cogs_gcc.segment3
                   || '.'
                   || cat_cogs
                   || '.'
                   || cogs_gcc.segment5
                   || '.'
                   || cogs_gcc.segment6
                   || '.'
                   || cogs_gcc.segment7,
                      -- Expn Segments
                      expn_gcc.segment1
                   || '.'
                   || expn_gcc.segment2
                   || '.'
                   || expn_gcc.segment3
                   || '.'
                   || cat_expn
                   || '.'
                   || expn_gcc.segment5
                   || '.'
                   || expn_gcc.segment6
                   || '.'
                   || expn_gcc.segment7,
                   mp.cost_of_sales_account,
                   mp.sales_account,
                   mp.expense_account
              INTO                                               --sales_ccid,
                  cat_sales_segs,                                 --cogs_ccid,
                   cat_cogs_segs,
                   cat_expn_segs,
                   org_cogs_ccid,
                   org_sales_ccid,
                   org_expn_ccid
              FROM APPS.MTL_PARAMETERS mp,
                   APPS.GL_CODE_COMBINATIONS cogs_gcc,
                   APPS.GL_CODE_COMBINATIONS sales_gcc,
                   APPS.GL_CODE_COMBINATIONS expn_gcc
             WHERE     mp.organization_id = p_org_id
                   AND mp.cost_of_sales_account =
                          cogs_gcc.code_combination_id(+)
                   AND cogs_gcc.enabled_flag = 'Y'
                   AND mp.sales_account = sales_gcc.code_combination_id(+)
                   AND sales_gcc.enabled_flag = 'Y'
                   AND mp.expense_account = expn_gcc.code_combination_id(+)
                   AND expn_gcc.enabled_flag = 'Y';
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               cat_sales_segs := NULL;
               cat_cogs_segs := NULL;
               cat_expn_segs := NULL;
               org_cogs_ccid := NULL;
               org_sales_ccid := NULL;
               org_expn_ccid := NULL;
         END;

         IF     (    item_cogs_segs = cat_cogs_segs
                 AND item_sales_segs = cat_sales_segs
                 AND item_expn_segs = cat_expn_segs)
            AND (    item_cogs_segs IS NOT NULL
                 AND cat_cogs_segs IS NOT NULL
                 AND item_sales_segs IS NOT NULL
                 AND cat_sales_segs IS NOT NULL
                 AND item_expn_segs IS NOT NULL
                 AND cat_expn_segs IS NOT NULL)
         THEN
            RETURN TRUE;
         ELSE
            new_sales_ccid := NULL;
            new_cogs_ccid := NULL;
            new_expn_ccid := NULL;

            IF     (item_cogs_segs <> cat_cogs_segs)
               AND (item_cogs_segs IS NOT NULL AND cat_cogs_segs IS NOT NULL)
            THEN
               new_cogs_ccid := create_ccid (cat_cogs_segs);

               IF new_cogs_ccid IS NULL
               THEN
                  IF     (org_cogs_ccid <> item_cogs_ccid)
                     AND (    org_cogs_ccid IS NOT NULL
                          AND item_cogs_ccid IS NOT NULL)
                  THEN
                     new_cogs_ccid := org_cogs_ccid;
                  END IF;
               END IF;
            END IF;

            IF     (item_sales_segs <> cat_sales_segs)
               AND (    item_sales_segs IS NOT NULL
                    AND cat_sales_segs IS NOT NULL)
            THEN
               new_sales_ccid := create_ccid (cat_sales_segs);

               IF new_sales_ccid IS NULL
               THEN
                  IF     (org_sales_ccid <> item_sales_ccid)
                     AND (    org_sales_ccid IS NOT NULL
                          AND item_sales_ccid IS NOT NULL)
                  THEN
                     new_sales_ccid := org_sales_ccid;
                  END IF;
               END IF;
            END IF;

            IF     (item_expn_segs <> cat_expn_segs)
               AND (item_expn_segs IS NOT NULL AND cat_expn_segs IS NOT NULL)
            THEN
               new_expn_ccid := create_ccid (cat_expn_segs);

               IF new_expn_ccid IS NULL
               THEN
                  IF     (org_expn_ccid <> item_expn_ccid)
                     AND (    org_expn_ccid IS NOT NULL
                          AND item_expn_ccid IS NOT NULL)
                  THEN
                     new_expn_ccid := org_expn_ccid;
                  END IF;
               END IF;
            END IF;
         END IF;

         -- ITEM_API calling
         IF    new_expn_ccid IS NOT NULL
            OR new_sales_ccid IS NOT NULL
            OR new_cogs_ccid IS NOT NULL
         THEN
            lvc_acct_updated :=
               item_accts_update (p_item_id,
                                  p_org_id,
                                  item_sales_ccid,
                                  item_cogs_ccid,
                                  item_expn_ccid,
                                  new_sales_ccid,
                                  new_cogs_ccid,
                                  new_expn_ccid);
         END IF;
      END IF;

      RETURN TRUE;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN FALSE;
   END;


   FUNCTION process_event (p_subscription_guid   IN            RAW,
                           p_event               IN OUT NOCOPY wf_event_t)
      RETURN VARCHAR2
   IS
      /******************************************************************************************************
      -- Object Name: process_event
      --
      -- Object Type: Function
      --
      -- PURPOSE: Functin calling from business event to update accounts.
      -- HISTORY
      -- ==========================================================================================================
      -- ==========================================================================================================
      -- VERSION DATE          AUTHOR(S)       DESCRIPTION
      -- ------- -----------   --------------- --------------------------------------------------------------------
      -- 1.0     21-OCT-2016   P.Vamshidhar    TMS#20160122-00091  GL Code Enhancements to leverage GL API
      --                                       Initial version
      ************************************************************************************************************/
      l_param_name          VARCHAR2 (240);
      l_param_value         VARCHAR2 (2000);
      l_event_name          VARCHAR2 (2000);
      l_err_text            VARCHAR2 (3000);
      l_param_list          wf_parameter_list_t;
      ln_request_id         NUMBER;
      ln_master_org         NUMBER;
      l_category_id         NUMBER;
      ln_category_set_id    NUMBER;
      ln_cat_intf_rec_cnt   NUMBER;
      ---------------------------------------------
      -- End of code modification w.r.t. ver 2.1 --
      ---------------------------------------------
      lv_error_message      VARCHAR2 (4000);
      l_error               VARCHAR2 (4000);
      lv_org_code           mtl_parameters.organization_code%TYPE := NULL;
      lv_item_number        mtl_system_items_b.segment1%TYPE := NULL;
      lv_category           mtl_categories_b.segment1%TYPE := NULL;
      cv_website_item       mtl_category_sets_tl.category_set_name%TYPE
                               := 'Website Item';
      no_event_exception    EXCEPTION;
      l_num_msg_cnt         NUMBER := NULL;
      l_chr_return_status   VARCHAR2 (100) := NULL;
      x_message_list        error_handler.error_tbl_type;
      x_tbl_item_table      ego_item_pub.item_tbl_type;
      l_tbl_item_table      ego_item_pub.item_tbl_type;
      l_calling             VARCHAR2 (4000) := NULL;

      ln_inv_item_id        mtl_system_items_b.inventory_item_id%TYPE;
      ln_org_id             mtl_system_items_b.organization_id%TYPE;
      lb_result             BOOLEAN;
      lvc_item_status       mtl_system_items_b.inventory_item_status_code%TYPE;
      lvc_item_mast         mtl_parameters.master_organization_id%TYPE;
      --new variables for ver 1.3
      l_item_number         mtl_system_items_b.segment1%TYPE; --changes for ver 1.3
      l_item_type           mtl_system_items_b.item_type%TYPE; --changes for ver1.3
      lv_request_id         NUMBER;                       --changes for ver1.3
      lvc_vendor_number     EGO_MTL_SY_ITEMS_EXT_B.C_EXT_ATTR1%type; --changes for ver1.3
      lvc_item_type         MTL_SYSTEM_ITEMS_B.INVENTORY_ITEM_STATUS_CODE%TYPE;
   BEGIN
      IF (p_event IS NULL)
      THEN
         RAISE no_event_exception;
      END IF;


      l_event_name := p_event.geteventname ();
      l_param_list := p_event.getparameterlist;
      ln_master_org := TO_NUMBER (fnd_profile.VALUE ('XXWC_ITEM_MASTER_ORG'));

      l_calling := 'Checking event name and fetching parameter value...';

      IF     l_param_list IS NOT NULL
         AND l_event_name = 'oracle.apps.ego.item.postItemCreate'
      THEN
         l_calling := 'Event Parameters Reading...';

         FOR i IN l_param_list.FIRST .. l_param_list.LAST
         LOOP
            l_param_name := l_param_list (i).getname;
            l_param_value := l_param_list (i).getvalue;

            IF l_param_name = 'INVENTORY_ITEM_ID'
            THEN
               ln_inv_item_id := l_param_value;
            ELSIF l_param_name = 'ORGANIZATION_ID'
            THEN
               ln_org_id := l_param_value;
            END IF;
         END LOOP;

         COMMIT;

         -- ver 1.3 calling concurrent program  XXWC BPA New Product Build Program changes for TMS Task ID: 20160921-00268

         BEGIN
            --get item number

            printlog ('gettign item number ');

            BEGIN
               SELECT segment1, item_type,inventory_item_status_code 
                 INTO l_item_number, l_item_type, lvc_item_type
                 FROM mtl_system_items_b
                WHERE     inventory_item_id = ln_inv_item_id
                      AND organization_id = ln_org_id;
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_item_number := NULL;
                  l_item_type := NULL;
            END;

/*            BEGIN
               SELECT C_EXT_ATTR1
                 INTO lvc_vendor_number
                 FROM APPS.EGO_MTL_SY_ITEMS_EXT_B
                WHERE     C_EXT_ATTR1 IS NOT NULL
                      AND organization_id = ln_org_id
                      AND ATTR_GROUP_ID IN (861)
                      AND inventory_item_id = ln_inv_item_id;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  BEGIN
                     SELECT C_EXT_ATTR1
                       INTO lvc_vendor_number
                       FROM XXWC.XXWC_CHG_EGO_ITEM_ATTRS
                      WHERE     C_EXT_ATTR1 IS NOT NULL
                            AND organization_id = ln_org_id
                            AND ATTR_GROUP_ID IN (861)
                            AND inventory_item_id = ln_inv_item_id;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        printlog ('Vendor123 ' || SUBSTR (SQLERRM, 1, 250));
                  END;
            END;

*/

            printlog ('gettign item number l_item_number ' || l_item_number);
            printlog ('gettign item type l_item_type ' || l_item_type);

            IF l_item_type <> 'SPECIAL' AND  lvc_item_type= 'Active'
            THEN
            
         
               --Submitting Concurrent Request
               --
               printlog (
                  'calling XXWC_BPA_NEW_PRD_PKG for  ' || l_item_number);
                  
  
               lv_request_id :=
                  fnd_request.submit_request (
                     application   => 'XXWC',        -- Application Short Name
                     program       => 'XXWC_BPA_NEW_PRD_PKG', -- Program Short Name
                     description   => NULL,      -- Any Meaningful Description
                     start_time    => TO_CHAR (SYSDATE + 5 / 1440,
                                               'DD-MON-YYYY HH24:MI:SS'), -- Start Time
                     sub_request   => FALSE,       -- Subrequest Default False
                     argument1     => l_item_number     -- Parameters Starting
                                                   );
               --
               COMMIT;
            
            END IF;
         END;


         -------------- ver 1.3 changes for TMS Task ID: 20160921-00268 ends


         l_calling :=
               'Checking Item status Item Id '
            || ln_inv_item_id
            || ' Org Id '
            || ln_org_id;

         -- Item status validation
         SELECT inventory_item_status_code
           INTO lvc_item_status
           FROM APPS.MTL_SYSTEM_ITEMS_B
          WHERE     organization_id = ln_org_id
                AND inventory_item_id = ln_inv_item_id;

         l_calling := 'Checking Organization_id';

         -- White cap organization validation.
         SELECT master_organization_id
           INTO lvc_item_mast
           FROM mtl_parameters
          WHERE organization_id = ln_org_id;


         IF lvc_item_status NOT IN ('Inactive', 'New Item')
         THEN
            IF     NVL (ln_inv_item_id, 0) > 0
               AND NVL (ln_org_id, 0) > 0
               AND ln_master_org = lvc_item_mast
            THEN
               l_calling := 'Accounts Update Process Start';

               lb_result := accts_update (ln_org_id, ln_inv_item_id);

               l_calling := 'Accounts Update Results Validation';

               COMMIT;
            END IF;
         END IF;
      END IF;

      RETURN 'SUCCESS';
   EXCEPTION
      WHEN no_event_exception
      THEN
         RETURN 'ERROR: WF_EVENT_MSG IS NULL';
      WHEN OTHERS
      THEN
         l_error := SQLERRM;
         l_err_text := 'Error : ' || TO_CHAR (SQLCODE) || '---' || l_error;
         ROLLBACK;
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_INV_ITEM_ACCT_UPDATE_PKG.PROCESS_EVENT',
            p_calling             => l_calling,
            p_request_id          => -1,
            p_ora_error_msg       => SUBSTR (
                                          'Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || 'Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (l_err_text, 1, 240),
            p_distribution_list   => g_distro_list,
            p_module              => 'XXWC');

         RETURN 'SUCCESS';
   END process_event;

   PROCEDURE CATE_ITEM_ACCT_UPDATE (X_ERRBUF    OUT VARCHAR2,
                                    X_RETCODE   OUT VARCHAR2)
   IS
      /******************************************************************************************************
      -- Object Name: CATE_ITEM_ACCT_UPDATE
      --
      -- Object Type: Procedure
      --
      -- PURPOSE: Functin calling from business event to update accounts.
      -- HISTORY
      -- ==========================================================================================================
      -- ==========================================================================================================
      -- VERSION DATE          AUTHOR(S)       DESCRIPTION
      -- ------- -----------   --------------- --------------------------------------------------------------------
      -- 1.0     21-OCT-2016   P.Vamshidhar    TMS#20160122-00091  GL Code Enhancements to leverage GL API
      --                                       Initial version
      ************************************************************************************************************/
      CURSOR CUR_CUST_CAT
      IS
           SELECT CATEGORY_ID
             FROM XXWC.XXWC_MTL_INV_CATE_UPDATE_LOG XMIC
            WHERE PROCESS_STATUS = 'NEW'
         GROUP BY category_id;

      CURSOR CUR_CATE (
         P_CATEGORY_ID    NUMBER)
      IS
         SELECT mic.INVENTORY_ITEM_ID, MSIB.INVENTORY_ITEM_STATUS_CODE
           FROM apps.mtl_item_categories mic, apps.mtl_system_items_b msib
          WHERE     mic.inventory_item_id = msib.inventory_item_id
                AND mic.organization_id = msib.organization_Id
                AND MSIB.ORGANIZATION_ID = 222
                AND mic.category_set_id = 1100000062
                AND mic.category_id = P_CATEGORY_ID;

      CURSOR CUR_ITEM_DATA (
         P_ITEM_ID    NUMBER)
      IS
         SELECT A.ORGANIZATION_ID,
                INVENTORY_ITEM_ID,
                A.inventory_item_status_code
           FROM APPS.MTL_SYSTEM_ITEMS_B A, APPS.MTL_PARAMETERS B
          WHERE     A.INVENTORY_ITEM_ID = P_ITEM_ID
                AND A.ORGANIZATION_ID = B.ORGANIZATION_ID;

      l_process_status   BOOLEAN;
      -- Changes Rev 1.1  - Start
      ln_count           NUMBER := 0;
      v_dev_phase        VARCHAR2 (40);
      v_dev_status       VARCHAR2 (40);
      ln_request_id      NUMBER;
      lvc_statement      VARCHAR2 (1000);
      v_rec_cnt          NUMBER := 0;
      v_interval         NUMBER := 30;
      v_max_time         NUMBER := 15000;
      v_status           VARCHAR2 (50);
      v_phase            VARCHAR2 (50);
      v_message          VARCHAR2 (250);
   -- Changes Rev 1.1 - End;
   BEGIN
      FND_FILE.PUT_LINE (FND_FILE.LOG, 'Procedure Begin');

      FOR REC_CUST_CAT IN CUR_CUST_CAT
      LOOP
         FND_FILE.PUT_LINE (
            FND_FILE.LOG,
            'Process begin for Category Id ' || REC_CUST_CAT.CATEGORY_ID);

         FOR REC_CATE IN CUR_CATE (REC_CUST_CAT.CATEGORY_ID)
         LOOP
            IF REC_CATE.INVENTORY_ITEM_STATUS_CODE = 'Inactive'
            THEN
               CONTINUE;
            END IF;

            FND_FILE.PUT_LINE (
               FND_FILE.LOG,
                  'Inventory Item Id '
               || REC_CATE.INVENTORY_ITEM_ID
               || ' '
               || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS')
               || ' Begin ');

            -- Change begin - Rev 1.1
            --l_process_status :=
            -- XXWC_INV_ITEM_ACCT_UPDATE_PKG.INV_ITEM_ACCTS_UPD (
            --    REC_CATE.INVENTORY_ITEM_ID);

            l_process_status :=
               XXWC_INV_ITEM_ACCT_UPDATE_PKG.INV_ITEM_ACCTS_UPD_CP (
                  REC_CATE.INVENTORY_ITEM_ID);

            IF (l_process_status)
            THEN
               ln_count := ln_count + 1;
            END IF;

            -- Changes End - Rev 1.1

            FND_FILE.PUT_LINE (
               FND_FILE.LOG,
                  'Inventory Item Id '
               || REC_CATE.INVENTORY_ITEM_ID
               || ' '
               || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS')
               || ' End ');
         END LOOP;

         UPDATE XXWC.XXWC_MTL_INV_CATE_UPDATE_LOG
            SET PROCESS_STATUS = 'SUCCESS'
          WHERE CATEGORY_ID = REC_CUST_CAT.CATEGORY_ID;

         COMMIT;

         FND_FILE.PUT_LINE (
            FND_FILE.LOG,
            'Process end for Category Id ' || REC_CUST_CAT.CATEGORY_ID);
      END LOOP;

      -- Begin Changes Rev - 1.1

      FND_FILE.PUT_LINE (
         FND_FILE.LOG,
         'Number of Items Inserted into Interface table ' || ln_count);

      IF NVL (ln_count, 0) > 0
      THEN
         ln_request_id :=
            fnd_request.submit_request (application   => 'INV',
                                        program       => 'INCOIN',
                                        description   => NULL,
                                        start_time    => SYSDATE,
                                        sub_request   => FALSE,
                                        argument1     => 222,
                                        -- Organization id
                                        argument2     => 1,
                                        -- All organizations
                                        argument3     => 1,
                                        -- Validate Items
                                        argument4     => 1,
                                        -- Process Items
                                        argument5     => 1,
                                        -- Delete Processed Rows
                                        argument6     => NULL,
                                        -- Process Set (Null for All)
                                        argument7     => 2,
                                        -- Create or Update Items
                                        argument8     => 1 -- Gather Statistics
                                                          );
         COMMIT;

         IF (ln_request_id != 0)
         THEN
            IF fnd_concurrent.wait_for_request (ln_request_id,
                                                v_interval,
                                                v_max_time,
                                                v_phase,
                                                v_status,
                                                v_dev_phase,
                                                v_dev_status,
                                                v_message)
            THEN
               -- Error Returned
               IF v_dev_phase != 'COMPLETE' OR v_dev_status != 'NORMAL'
               THEN
                  lvc_statement :=
                        'Error occured in the running of Item import Prgram'
                     || v_message
                     || '.';
                  fnd_file.put_line (fnd_file.LOG, lvc_statement);
               END IF;
            END IF;
         ELSE
            lvc_statement :=
               'Error occured when submitting Item Import Program';
            fnd_file.put_line (fnd_file.LOG, lvc_statement);
         END IF;

         COMMIT;
      END IF;

      -- End Changes Rev - 1.1

      FND_FILE.PUT_LINE (FND_FILE.LOG, 'Procedure End');
   END;


   FUNCTION INV_ITEM_ACCTS_UPD (p_item_id IN NUMBER)
      RETURN BOOLEAN
   IS
      /******************************************************************************************************
      -- Object Name: accts_update
      --
      -- Object Type: Function
      --
      -- PURPOSE: Function to update accounts item
      -- HISTORY
      -- ==========================================================================================================
      -- ==========================================================================================================
      -- VERSION DATE          AUTHOR(S)       DESCRIPTION
      -- ------- -----------   --------------- --------------------------------------------------------------------
      -- 1.0     21-OCT-2016   P.Vamshidhar    TMS#20160122-00091  GL Code Enhancements to leverage GL API
      --                                       Initial version
      ************************************************************************************************************/
      item_cogs_segs      VARCHAR2 (240);
      item_sales_segs     VARCHAR2 (240);
      item_expn_segs      VARCHAR2 (240);
      cat_cogs            NUMBER;
      cat_sales           NUMBER;
      cat_expn            NUMBER;
      cat_sales_segs      VARCHAR2 (240);
      cat_cogs_segs       VARCHAR2 (240);
      cat_expn_segs       VARCHAR2 (240);
      org_cogs_ccid       GL_CODE_COMBINATIONS.CODE_COMBINATION_ID%TYPE;
      org_sales_ccid      GL_CODE_COMBINATIONS.CODE_COMBINATION_ID%TYPE;
      org_expn_ccid       GL_CODE_COMBINATIONS.CODE_COMBINATION_ID%TYPE;
      item_cogs_ccid      GL_CODE_COMBINATIONS.CODE_COMBINATION_ID%TYPE;
      item_sales_ccid     GL_CODE_COMBINATIONS.CODE_COMBINATION_ID%TYPE;
      item_expn_ccid      GL_CODE_COMBINATIONS.CODE_COMBINATION_ID%TYPE;
      new_sales_ccid      GL_CODE_COMBINATIONS.CODE_COMBINATION_ID%TYPE;
      new_cogs_ccid       GL_CODE_COMBINATIONS.CODE_COMBINATION_ID%TYPE;
      new_expn_ccid       GL_CODE_COMBINATIONS.CODE_COMBINATION_ID%TYPE;
      lvc_acct_updated    BOOLEAN;

      l_item_table        EGO_Item_PUB.Item_Tbl_Type;
      x_item_table        EGO_Item_PUB.Item_Tbl_Type;
      ln_count            NUMBER := 0;
      l_msg_data          VARCHAR2 (32000);
      lvc_return_status   VARCHAR2 (1);
      ln_msg_count        NUMBER;

      CURSOR cur_org_id (p_inventory_item_id NUMBER)
      IS
         SELECT ORGANIZATION_ID, INVENTORY_ITEM_STATUS_CODE
           FROM APPS.MTL_SYSTEM_ITEMS_B
          WHERE INVENTORY_ITEM_ID = P_inventory_item_id;
   BEGIN
      BEGIN
         SELECT ATTRIBUTE1, ATTRIBUTE2, ATTRIBUTE8
           INTO cat_cogs, cat_sales, cat_expn
           FROM APPS.MTL_ITEM_CATEGORIES MIC, APPS.MTL_CATEGORIES_B MC
          WHERE     MIC.CATEGORY_SET_ID = 1100000062
                AND MIC.INVENTORY_ITEM_ID = p_item_id
                AND MIC.CATEGORY_ID = MC.CATEGORY_ID
                AND MIC.ORGANIZATION_ID = 222;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            cat_cogs := NULL;
            cat_sales := NULL;
            cat_expn := NULL;
      END;

      FND_FILE.PUT_LINE (FND_FILE.LOG, ' Inventory_Item_id ' || p_item_id);

      FOR rec_org_id IN cur_org_id (p_item_id)
      LOOP
         IF rec_org_id.inventory_item_status_code = 'Inactive'
         THEN
            CONTINUE;
         END IF;

         BEGIN
            item_cogs_segs := NULL;
            item_cogs_ccid := NULL;
            item_sales_segs := NULL;
            item_sales_ccid := NULL;
            item_expn_segs := NULL;
            item_expn_ccid := NULL;

            SELECT cogs_gcc.concatenated_segments,
                   msib.cost_of_sales_account,
                   sales_gcc.concatenated_segments,
                   msib.sales_account,
                   expn_gcc.concatenated_segments,
                   msib.expense_account
              INTO item_cogs_segs,
                   item_cogs_ccid,
                   item_sales_segs,
                   item_sales_ccid,
                   item_expn_segs,
                   item_expn_ccid
              FROM APPS.MTL_SYSTEM_ITEMS_B msib,
                   apps.gl_code_combinations_kfv cogs_gcc,
                   apps.gl_code_combinations_kfv sales_gcc,
                   apps.gl_code_combinations_kfv expn_gcc
             WHERE     msib.organization_id = rec_org_id.organization_id
                   AND msib.inventory_item_id = p_item_id
                   AND msib.cost_of_sales_account =
                          cogs_gcc.code_combination_id(+)
                   AND cogs_gcc.enabled_flag = 'Y'
                   AND msib.sales_account = sales_gcc.code_combination_id(+)
                   AND sales_gcc.enabled_flag = 'Y'
                   AND msib.expense_account = expn_gcc.code_combination_id(+)
                   AND expn_gcc.enabled_flag = 'Y';
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               item_cogs_segs := NULL;
               item_sales_segs := NULL;
               item_expn_segs := NULL;
               item_cogs_ccid := NULL;
               item_sales_ccid := NULL;
               item_expn_ccid := NULL;
         END;


         IF    cat_cogs IS NOT NULL
            OR cat_sales IS NOT NULL
            OR cat_expn IS NOT NULL
         THEN
            BEGIN
               cat_sales_segs := NULL;                            --cogs_ccid,
               cat_cogs_segs := NULL;
               cat_expn_segs := NULL;
               org_cogs_ccid := NULL;
               org_sales_ccid := NULL;
               org_expn_ccid := NULL;

               SELECT                                        -- Sales Segments
                     sales_gcc.segment1
                      || '.'
                      || sales_gcc.segment2
                      || '.'
                      || sales_gcc.segment3
                      || '.'
                      || cat_sales
                      || '.'
                      || sales_gcc.segment5
                      || '.'
                      || sales_gcc.segment6
                      || '.'
                      || sales_gcc.segment7,
                         -- COGS Segments
                         cogs_gcc.segment1
                      || '.'
                      || cogs_gcc.segment2
                      || '.'
                      || cogs_gcc.segment3
                      || '.'
                      || cat_cogs
                      || '.'
                      || cogs_gcc.segment5
                      || '.'
                      || cogs_gcc.segment6
                      || '.'
                      || cogs_gcc.segment7,
                         -- Expn Segments
                         expn_gcc.segment1
                      || '.'
                      || expn_gcc.segment2
                      || '.'
                      || expn_gcc.segment3
                      || '.'
                      || cat_expn
                      || '.'
                      || expn_gcc.segment5
                      || '.'
                      || expn_gcc.segment6
                      || '.'
                      || expn_gcc.segment7,
                      mp.cost_of_sales_account,
                      mp.sales_account,
                      mp.expense_account
                 INTO                                            --sales_ccid,
                     cat_sales_segs,                              --cogs_ccid,
                      cat_cogs_segs,
                      cat_expn_segs,
                      org_cogs_ccid,
                      org_sales_ccid,
                      org_expn_ccid
                 FROM APPS.MTL_PARAMETERS mp,
                      APPS.GL_CODE_COMBINATIONS cogs_gcc,
                      APPS.GL_CODE_COMBINATIONS sales_gcc,
                      APPS.GL_CODE_COMBINATIONS expn_gcc
                WHERE     mp.organization_id = rec_org_id.organization_id
                      AND mp.cost_of_sales_account =
                             cogs_gcc.code_combination_id(+)
                      AND cogs_gcc.enabled_flag = 'Y'
                      AND mp.sales_account = sales_gcc.code_combination_id(+)
                      AND sales_gcc.enabled_flag = 'Y'
                      AND mp.expense_account =
                             expn_gcc.code_combination_id(+)
                      AND expn_gcc.enabled_flag = 'Y';
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  cat_sales_segs := NULL;
                  cat_cogs_segs := NULL;
                  cat_expn_segs := NULL;
                  org_cogs_ccid := NULL;
                  org_sales_ccid := NULL;
                  org_expn_ccid := NULL;
            END;
         END IF;

         IF     (    item_cogs_segs = cat_cogs_segs
                 AND item_sales_segs = cat_sales_segs
                 AND item_expn_segs = cat_expn_segs)
            AND (    item_cogs_segs IS NOT NULL
                 AND cat_cogs_segs IS NOT NULL
                 AND item_sales_segs IS NOT NULL
                 AND cat_sales_segs IS NOT NULL
                 AND item_expn_segs IS NOT NULL
                 AND cat_expn_segs IS NOT NULL)
         THEN
            RETURN TRUE;
         ELSE
            new_sales_ccid := NULL;
            new_cogs_ccid := NULL;
            new_expn_ccid := NULL;

            IF     (item_cogs_segs <> cat_cogs_segs)
               AND (item_cogs_segs IS NOT NULL AND cat_cogs_segs IS NOT NULL)
            THEN
               new_cogs_ccid := create_ccid (cat_cogs_segs);

               IF new_cogs_ccid IS NULL
               THEN
                  IF     (org_cogs_ccid <> item_cogs_ccid)
                     AND (    org_cogs_ccid IS NOT NULL
                          AND item_cogs_ccid IS NOT NULL)
                  THEN
                     new_cogs_ccid := org_cogs_ccid;
                  END IF;
               END IF;
            END IF;

            IF     (item_sales_segs <> cat_sales_segs)
               AND (    item_sales_segs IS NOT NULL
                    AND cat_sales_segs IS NOT NULL)
            THEN
               new_sales_ccid := create_ccid (cat_sales_segs);

               IF new_sales_ccid IS NULL
               THEN
                  IF     (org_sales_ccid <> item_sales_ccid)
                     AND (    org_sales_ccid IS NOT NULL
                          AND item_sales_ccid IS NOT NULL)
                  THEN
                     new_sales_ccid := org_sales_ccid;
                  END IF;
               END IF;
            END IF;

            IF     (item_expn_segs <> cat_expn_segs)
               AND (item_expn_segs IS NOT NULL AND cat_expn_segs IS NOT NULL)
            THEN
               new_expn_ccid := create_ccid (cat_expn_segs);

               IF new_expn_ccid IS NULL
               THEN
                  IF     (org_expn_ccid <> item_expn_ccid)
                     AND (    org_expn_ccid IS NOT NULL
                          AND item_expn_ccid IS NOT NULL)
                  THEN
                     new_expn_ccid := org_expn_ccid;
                  END IF;
               END IF;
            END IF;
         END IF;


         IF    new_expn_ccid IS NOT NULL
            OR new_sales_ccid IS NOT NULL
            OR new_cogs_ccid IS NOT NULL
         THEN
            ln_count := ln_count + 1;
            l_item_table (ln_count).Transaction_Type := 'UPDATE';
            l_item_table (ln_count).inventory_item_id := p_item_id;
            l_item_table (ln_count).organization_id :=
               rec_org_id.organization_id;

            IF new_cogs_ccid IS NOT NULL
            THEN
               l_item_table (ln_count).cost_of_sales_account := new_cogs_ccid;
            END IF;

            IF new_sales_ccid IS NOT NULL
            THEN
               l_item_table (ln_count).sales_account := new_sales_ccid;
            END IF;

            IF new_expn_ccid IS NOT NULL
            THEN
               l_item_table (ln_count).expense_account := new_expn_ccid;
            END IF;
         END IF;
      END LOOP;

      IF l_item_table.COUNT > 0
      THEN
         FND_FILE.PUT_LINE (
            FND_FILE.LOG,
               ' API Time '
            || TO_CHAR (SYSDATE, 'DD-MM-YYYY HH24:MI:SS')
            || ' l_item_table '
            || l_item_table.COUNT);
         EGO_ITEM_PUB.Process_Items (
            p_api_version      => 1.0,
            p_init_msg_list    => FND_API.g_TRUE,
            p_commit           => FND_API.g_TRUE,
            p_Item_Tbl         => l_item_table,
            x_Item_Tbl         => x_item_table,
            P_ROLE_GRANT_TBL   => EGO_ITEM_PUB.G_MISS_ROLE_GRANT_TBL,
            x_return_status    => lvc_return_status,
            x_msg_count        => ln_msg_count);

         FND_FILE.PUT_LINE (
            FND_FILE.LOG,
               ' API Time 1  '
            || TO_CHAR (SYSDATE, 'DD-MM-YYYY HH24:MI:SS')
            || ' lvc_return_status '
            || lvc_return_status);


         IF lvc_return_status = 'S'
         THEN
            COMMIT;
         ELSE
            ROLLBACK;

            IF ln_msg_count > 0
            THEN
               FOR i IN 1 .. ln_msg_count
               LOOP
                  l_msg_data :=
                        l_msg_Data
                     || SUBSTR (
                           fnd_msg_pub.get (p_msg_index   => i,
                                            p_encoded     => fnd_api.g_false),
                           1,
                           255);
               END LOOP;
            END IF;

            FND_FILE.PUT_LINE (
               FND_FILE.LOG,
               ' Item Id: ' || p_item_id || ' Error :' || l_msg_data);
         END IF;
      END IF;

      l_item_table.DELETE;


      RETURN TRUE;
   EXCEPTION
      WHEN OTHERS
      THEN
         FND_FILE.PUT_LINE (FND_FILE.LOG,
                            'Error Occured ' || SUBSTR (SQLERRM, 1, 250));
         RETURN FALSE;
   END;

   FUNCTION INV_ITEM_ACCTS_UPD_CP (p_item_id IN NUMBER)
      RETURN BOOLEAN
   IS
      /******************************************************************************************************
      -- Object Name: INV_ITEM_ACCTS_UPD_CP
      --
      -- PROGRAM TYPE: Function
      --
      -- PURPOSE: Function created to insert date into MTL_SYSTEM_ITEMS_INTERFACE table.
      -- HISTORY
      -- ==========================================================================================================
      -- ==========================================================================================================
      -- VERSION DATE          AUTHOR(S)       DESCRIPTION
      -- ------- -----------   --------------- --------------------------------------------------------------------
      -- 1.1     01-Feb-2017   P. Vamshidhar   TMS#20170116-00091  - @ PERF  -  XXWC INV Item GL Codes Update
      --                                       Intial Version
   -- 1.2     04-May-2017   P. Vamshidhar   TMS#20170119-00013 - Need One time data fix to update Expense 4th segment
      *******************************************************************************************************************/
      item_cogs_segs    VARCHAR2 (240);
      item_sales_segs   VARCHAR2 (240);
      item_expn_segs    VARCHAR2 (240);
      cat_cogs          NUMBER;
      cat_sales         NUMBER;
      cat_expn          NUMBER;
      cat_sales_segs    VARCHAR2 (240);
      cat_cogs_segs     VARCHAR2 (240);
      cat_expn_segs     VARCHAR2 (240);
      org_cogs_ccid     GL_CODE_COMBINATIONS.CODE_COMBINATION_ID%TYPE;
      org_sales_ccid    GL_CODE_COMBINATIONS.CODE_COMBINATION_ID%TYPE;
      org_expn_ccid     GL_CODE_COMBINATIONS.CODE_COMBINATION_ID%TYPE;
      item_cogs_ccid    GL_CODE_COMBINATIONS.CODE_COMBINATION_ID%TYPE;
      item_sales_ccid   GL_CODE_COMBINATIONS.CODE_COMBINATION_ID%TYPE;
      item_expn_ccid    GL_CODE_COMBINATIONS.CODE_COMBINATION_ID%TYPE;
      new_sales_ccid    GL_CODE_COMBINATIONS.CODE_COMBINATION_ID%TYPE;
      new_cogs_ccid     GL_CODE_COMBINATIONS.CODE_COMBINATION_ID%TYPE;
      new_expn_ccid     GL_CODE_COMBINATIONS.CODE_COMBINATION_ID%TYPE;
      ln_count          NUMBER := 0;


      CURSOR cur_org_id (p_inventory_item_id NUMBER)
      IS
         SELECT ORGANIZATION_ID, INVENTORY_ITEM_STATUS_CODE
           FROM APPS.MTL_SYSTEM_ITEMS_B
          WHERE INVENTORY_ITEM_ID = P_inventory_item_id;
   BEGIN
      BEGIN
         SELECT ATTRIBUTE1, ATTRIBUTE2, ATTRIBUTE8
           INTO cat_cogs, cat_sales, cat_expn
           FROM APPS.MTL_ITEM_CATEGORIES MIC, APPS.MTL_CATEGORIES_B MC
          WHERE     MIC.CATEGORY_SET_ID = 1100000062
                AND MIC.INVENTORY_ITEM_ID = p_item_id
                AND MIC.CATEGORY_ID = MC.CATEGORY_ID
                AND MIC.ORGANIZATION_ID = 222;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            cat_cogs := NULL;
            cat_sales := NULL;
            cat_expn := NULL;
      END;

      FND_FILE.PUT_LINE (FND_FILE.LOG, 'Inventory Item id ' || p_item_id);


      FOR rec_org_id IN cur_org_id (p_item_id)
      LOOP
         IF rec_org_id.inventory_item_status_code = 'Inactive'
         THEN
            CONTINUE;
         END IF;

         BEGIN
            item_cogs_segs := NULL;
            item_cogs_ccid := NULL;
            item_sales_segs := NULL;
            item_sales_ccid := NULL;
            item_expn_segs := NULL;
            item_expn_ccid := NULL;

            SELECT cogs_gcc.concatenated_segments,
                   msib.cost_of_sales_account,
                   sales_gcc.concatenated_segments,
                   msib.sales_account,
                   expn_gcc.concatenated_segments,
                   msib.expense_account
              INTO item_cogs_segs,
                   item_cogs_ccid,
                   item_sales_segs,
                   item_sales_ccid,
                   item_expn_segs,
                   item_expn_ccid
              FROM APPS.MTL_SYSTEM_ITEMS_B msib,
                   apps.gl_code_combinations_kfv cogs_gcc,
                   apps.gl_code_combinations_kfv sales_gcc,
                   apps.gl_code_combinations_kfv expn_gcc
             WHERE     msib.organization_id = rec_org_id.organization_id
                   AND msib.inventory_item_id = p_item_id
                   AND msib.cost_of_sales_account =
                          cogs_gcc.code_combination_id(+)
                   AND cogs_gcc.enabled_flag = 'Y'
                   AND msib.sales_account = sales_gcc.code_combination_id(+)
                   AND sales_gcc.enabled_flag = 'Y'
                   AND msib.expense_account = expn_gcc.code_combination_id(+)
                   AND expn_gcc.enabled_flag = 'Y';
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               item_cogs_segs := NULL;
               item_sales_segs := NULL;
               item_expn_segs := NULL;
               item_cogs_ccid := NULL;
               item_sales_ccid := NULL;
               item_expn_ccid := NULL;
         END;


         IF    cat_cogs IS NOT NULL
            OR cat_sales IS NOT NULL
            OR cat_expn IS NOT NULL
         THEN
            BEGIN
               cat_sales_segs := NULL;                            --cogs_ccid,
               cat_cogs_segs := NULL;
               cat_expn_segs := NULL;
               org_cogs_ccid := NULL;
               org_sales_ccid := NULL;
               org_expn_ccid := NULL;

               SELECT                                        -- Sales Segments
                     sales_gcc.segment1
                      || '.'
                      || sales_gcc.segment2
                      || '.'
                      || sales_gcc.segment3
                      || '.'
                      || cat_sales
                      || '.'
                      || sales_gcc.segment5
                      || '.'
                      || sales_gcc.segment6
                      || '.'
                      || sales_gcc.segment7,
                         -- COGS Segments
                         cogs_gcc.segment1
                      || '.'
                      || cogs_gcc.segment2
                      || '.'
                      || cogs_gcc.segment3
                      || '.'
                      || cat_cogs
                      || '.'
                      || cogs_gcc.segment5
                      || '.'
                      || cogs_gcc.segment6
                      || '.'
                      || cogs_gcc.segment7,
                         -- Expn Segments
                         expn_gcc.segment1
                      || '.'
                      || expn_gcc.segment2
                      || '.'
                      || expn_gcc.segment3
                      || '.'
                      || cat_expn
                      || '.'
                      || expn_gcc.segment5
                      || '.'
                      || expn_gcc.segment6
                      || '.'
                      || expn_gcc.segment7,
                      mp.cost_of_sales_account,
                      mp.sales_account,
                      mp.expense_account
                 INTO cat_sales_segs,
                      cat_cogs_segs,
                      cat_expn_segs,
                      org_cogs_ccid,
                      org_sales_ccid,
                      org_expn_ccid
                 FROM APPS.MTL_PARAMETERS mp,
                      APPS.GL_CODE_COMBINATIONS cogs_gcc,
                      APPS.GL_CODE_COMBINATIONS sales_gcc,
                      APPS.GL_CODE_COMBINATIONS expn_gcc
                WHERE     mp.organization_id = rec_org_id.organization_id
                      AND mp.cost_of_sales_account =
                             cogs_gcc.code_combination_id(+)
                      AND cogs_gcc.enabled_flag = 'Y'
                      AND mp.sales_account = sales_gcc.code_combination_id(+)
                      AND sales_gcc.enabled_flag = 'Y'
                      AND mp.expense_account =
                             expn_gcc.code_combination_id(+)
                      AND expn_gcc.enabled_flag = 'Y';
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  cat_sales_segs := NULL;
                  cat_cogs_segs := NULL;
                  cat_expn_segs := NULL;
                  org_cogs_ccid := NULL;
                  org_sales_ccid := NULL;
                  org_expn_ccid := NULL;
            END;
         END IF;

         IF     (    item_cogs_segs = cat_cogs_segs
                 AND item_sales_segs = cat_sales_segs
                 AND item_expn_segs = cat_expn_segs)
            AND (    item_cogs_segs IS NOT NULL
                 AND cat_cogs_segs IS NOT NULL
                 AND item_sales_segs IS NOT NULL
                 AND cat_sales_segs IS NOT NULL
                 AND item_expn_segs IS NOT NULL
                 AND cat_expn_segs IS NOT NULL)
         THEN
            --RETURN TRUE; Commented in Rev. 1.2
            CONTINUE;                                      -- Added in Rev 1.2
         ELSE
            new_sales_ccid := NULL;
            new_cogs_ccid := NULL;
            new_expn_ccid := NULL;

            IF     (item_cogs_segs <> cat_cogs_segs)
               AND (item_cogs_segs IS NOT NULL AND cat_cogs_segs IS NOT NULL)
            THEN
               new_cogs_ccid := create_ccid (cat_cogs_segs);

               IF new_cogs_ccid IS NULL
               THEN
                  IF     (org_cogs_ccid <> item_cogs_ccid)
                     AND (    org_cogs_ccid IS NOT NULL
                          AND item_cogs_ccid IS NOT NULL)
                  THEN
                     new_cogs_ccid := org_cogs_ccid;
                  END IF;
               END IF;
            END IF;

            IF     (item_sales_segs <> cat_sales_segs)
               AND (    item_sales_segs IS NOT NULL
                    AND cat_sales_segs IS NOT NULL)
            THEN
               new_sales_ccid := create_ccid (cat_sales_segs);

               IF new_sales_ccid IS NULL
               THEN
                  IF     (org_sales_ccid <> item_sales_ccid)
                     AND (    org_sales_ccid IS NOT NULL
                          AND item_sales_ccid IS NOT NULL)
                  THEN
                     new_sales_ccid := org_sales_ccid;
                  END IF;
               END IF;
            END IF;

            IF     (item_expn_segs <> cat_expn_segs)
               AND (item_expn_segs IS NOT NULL AND cat_expn_segs IS NOT NULL)
            THEN
               new_expn_ccid := create_ccid (cat_expn_segs);

               IF new_expn_ccid IS NULL
               THEN
                  IF     (org_expn_ccid <> item_expn_ccid)
                     AND (    org_expn_ccid IS NOT NULL
                          AND item_expn_ccid IS NOT NULL)
                  THEN
                     new_expn_ccid := org_expn_ccid;
                  END IF;
               END IF;
            END IF;
         END IF;


         IF    new_expn_ccid IS NOT NULL
            OR new_sales_ccid IS NOT NULL
            OR new_cogs_ccid IS NOT NULL
         THEN
            ln_count := ln_count + 1;

            INSERT
              INTO APPS.MTL_SYSTEM_ITEMS_INTERFACE (INVENTORY_ITEM_ID,
                                                    ORGANIZATION_ID,
                                                    Transaction_Type,
                                                    process_flag,
                                                    COST_OF_SALES_ACCOUNT,
                                                    SALES_ACCOUNT,
                                                    EXPENSE_ACCOUNT)
            VALUES (p_item_id,
                    rec_org_id.organization_id,
                    'UPDATE',
                    1,
                    new_cogs_ccid,
                    new_sales_ccid,
                    new_expn_ccid);
         END IF;
      END LOOP;

      FND_FILE.PUT_LINE (
         FND_FILE.LOG,
            ln_count
         || ' Records inserted into Interface for item '
         || p_item_id);


      RETURN TRUE;
   EXCEPTION
      WHEN OTHERS
      THEN
         FND_FILE.PUT_LINE (FND_FILE.LOG,
                            'Error Occured ' || SUBSTR (SQLERRM, 1, 250));
         RETURN FALSE;
   END;
END;
/
