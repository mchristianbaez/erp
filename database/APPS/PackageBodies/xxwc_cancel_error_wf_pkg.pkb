CREATE OR REPLACE PACKAGE BODY xxwc_cancel_error_wf_pkg
AS
   /********************************************************************************************************************************
      $Header XXWC_CANCEL_ERROR_WF_PKG.PKG $
      Module Name: XXWC_CANCEL_ERROR_WF_PKG.PKG

      PURPOSE:   This package is used to Cancel Errored Workflows so they may be purged by the purge job

      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------------------------------------------------------------
      1.0        28-Sep-2016 Niraj K Ranjan          Initial Version - TMS#20160804-00335   Create a concurrent program to Cancel 
	                                                                 --Errored Workflows so they may be purged by the purge job
   *********************************************************************************************************************************/

   --Email Defaults
   g_dflt_email   fnd_user.email_address%TYPE := 'WC-ITDEVALERTS-U1@HDSupply.com';
   g_pkg_name     VARCHAR2 (50) := 'xxwc_cancel_error_wf_pkg';

    /*************************************************************************
      PROCEDURE Name: cancel_error_wf

      PURPOSE:   Cancel Errored Workflows

      REVISIONS:
      Ver        Date          Author             Description
      ---------  ------------  ------------       ------------------
      1.0        28-Sep-2016    Niraj             Initial Version
   ****************************************************************************/
   PROCEDURE cancel_error_wf ( p_errbuf         OUT VARCHAR2
                              ,p_retcode        OUT NUMBER
                              ,p_num_of_days    IN  NUMBER
							  ,p_item_type      IN  VARCHAR2
							  ,p_commit_count   IN  NUMBER
							  ,p_order_by       IN  VARCHAR2
                             )
   IS
     e_user_exception         EXCEPTION;
     l_err_msg                VARCHAR2 (2000);
     l_err_code               VARCHAR2(200);
     l_sec                    VARCHAR2 (150);
	 l_request_id             NUMBER;
	 l_count                  NUMBER := 0;
	 TYPE cur_typ             IS REF CURSOR;
	 l_cur_type               cur_typ;
	 l_query                  VARCHAR2(4000);
	 l_item_key               wf_items.item_key%TYPE; 

   BEGIN
      l_sec := 'Start of procedure cancel_error_wf';
      fnd_file.put_line (fnd_file.LOG ,'=========================================================================================');
      fnd_file.put_line (fnd_file.LOG , 'Start of Procedure : cancel_error_wf'||' @ '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
      l_request_id := fnd_global.conc_request_id;
	  fnd_file.put_line (fnd_file.LOG , 'l_request_id  :'||l_request_id);
	  fnd_file.put_line (fnd_file.LOG , 'p_num_of_days :'||p_num_of_days);
	  fnd_file.put_line (fnd_file.LOG , 'p_item_type   :'||p_item_type);
	  fnd_file.put_line (fnd_file.LOG , 'p_commit_count:'||p_commit_count);
	  fnd_file.put_line (fnd_file.LOG , 'p_order_by    :'||p_order_by);
	  
	  IF p_order_by = 'N' THEN
	     l_query :=   'SELECT item_key FROM wf_items'||
                      ' WHERE  end_date is null'||
	   			      ' AND TRUNC(begin_date) <= (TRUNC(SYSDATE) - '||p_num_of_days||')'||
	   			      ' AND item_type = NVL('||''''||p_item_type||''''||','|| '''WFERROR'''||')'||
	   			      ' AND parent_item_type is null';
      ELSIF p_order_by = 'Y' THEN
         l_query :=   'SELECT item_key FROM wf_items'||
                      ' WHERE  end_date is null'||
	   			      ' AND TRUNC(begin_date) <= (TRUNC(SYSDATE) - '||p_num_of_days||')'||
	   			      ' AND item_type = NVL('||''''||p_item_type||''''||','|| '''WFERROR'''||')'||
	   			      ' AND parent_item_type is null'||
					  ' ORDER BY begin_date asc';
	  END IF;
	  fnd_file.put_line (fnd_file.LOG ,'--------------------DYNAMIC QUERY--------------------');
	  fnd_file.put_line (fnd_file.LOG ,l_query);
	  fnd_file.put_line (fnd_file.LOG ,'-----------------------------------------------------');

	  fnd_file.put_line (fnd_file.LOG ,'Starting abort script');
	  l_sec := 'Start of Loop l_cur_type';
	  OPEN l_cur_type FOR l_query;
      LOOP
        FETCH l_cur_type INTO l_item_key;
        EXIT WHEN l_cur_type%NOTFOUND;
		l_count := l_count + 1;
        l_sec := 'Calling api wf_engine.abortProcess for item key: '||l_item_key;
		--fnd_file.put_line (fnd_file.LOG ,l_sec);
        wf_engine.abortProcess(p_item_type, l_item_key);
		IF MOD(l_count,p_commit_count) = 0 THEN
           fnd_file.put_line(fnd_file.LOG ,'Processed '||p_commit_count||' rows. Committing Current count: ' || l_count||' @ '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
           COMMIT;
        END IF;
      END LOOP;
      CLOSE l_cur_type;

      fnd_file.put_line (fnd_file.LOG ,'Finished abort script. Final commit. Processed this many lines: ' || l_count);
      COMMIT;

	  fnd_file.put_line (fnd_file.LOG , 'End of Procedure : cancel_error_wf'||' @ '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
      fnd_file.put_line (fnd_file.LOG ,'==========================================================================================');
   EXCEPTION
       WHEN OTHERS THEN
       p_errbuf := 'Error';
       p_retcode := '2';
	   l_err_code := SQLCODE;
       l_err_msg  := SUBSTR(SQLERRM,1,2000);
       fnd_file.put_line (fnd_file.LOG, l_sec||'=>'||l_err_code||'=>'||l_err_msg);
       fnd_file.put_line (fnd_file.output, l_sec||'=>'||l_err_code||'=>'||l_err_msg);
	   ROLLBACK;
       xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_CANCEL_ERROR_WF_PKG.CANCEL_ERROR_WF'
           ,p_calling             => l_sec
           ,p_request_id          => l_request_id
           ,p_ora_error_msg       => SQLERRM
           ,p_error_desc          => 'Error while canceling errored workflows'
           ,p_distribution_list   => g_dflt_email
           ,p_module              => 'XXWC');
   END cancel_error_wf;
END xxwc_cancel_error_wf_pkg;
/