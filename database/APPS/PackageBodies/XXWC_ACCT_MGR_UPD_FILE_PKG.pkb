CREATE OR REPLACE PACKAGE BODY APPS.XXWC_ACCT_MGR_UPD_FILE_PKG
AS
   /**************************************************************************
      $Header XXWC_ACCT_MGR_UPD_FILE_PKG $
      Module Name: XXWC_ACCT_MGR_UPD_FILE_PKG.pks

      PURPOSE:   This package is called by the concurrent programs
                 XXWC Credit-Account Manager Update
      REVISIONS:
      Ver        Date        Author             Description
      ---------  ----------  ---------------   ---------------------------------------------
       1.0       05/12/2016  Neha Saini         Initial Build - Task ID: 20170616-00292
       1.1       01/08/2018  P.Vamshidhar       TMS#20180105-00272 - Customer - Account Manager Hierarchy File update
    /*************************************************************************/

   g_loc   VARCHAR2 (20000);

   /*************************************************************************
     Procedure : Write_Log

     PURPOSE:   This procedure logs debug message in Concurrent Log file
     Parameter:
            IN
                p_debug_msg      -- Debug Message
    REVISIONS:
       Ver        Date        Author             Description
     ---------  ----------  ---------------   -------------------------
      1.0       05/12/2016  Neha Saini         Initial Build - Task ID: 20170616-00292
   ************************************************************************/
   PROCEDURE write_log (p_debug_msg IN VARCHAR2)
   IS
   BEGIN
      fnd_file.put_line (fnd_file.LOG, p_debug_msg);
      DBMS_OUTPUT.put_line (p_debug_msg);
   END write_log;

   /*************************************************************************
      Procedure : Write_Error

     PURPOSE:   This procedure logs error message
     Parameter:
            IN
                p_debug_msg      -- Debug Message
    REVISIONS:
       Ver        Date        Author             Description
     ---------  ----------  ---------------   -------------------------
     1.0       05/12/2016  Neha Saini         Initial Build - Task ID: 20170616-00292
   ************************************************************************/

   --add message to concurrent output file
   PROCEDURE write_error (p_debug_msg IN VARCHAR2)
   IS
      l_req_id          NUMBER := fnd_global.conc_request_id;
      l_err_callfrom    VARCHAR2 (75) DEFAULT 'XXWC_ACCT_MGR_UPD_FILE_PKG';
      l_err_callpoint   VARCHAR2 (75) DEFAULT 'START';
      l_distro_list     VARCHAR2 (75)
                           DEFAULT 'HDSOracleDevelopers@hdsupply.com';
   BEGIN
      xxcus_error_pkg.xxcus_error_main_api (
         p_called_from         => l_err_callfrom,
         p_calling             => l_err_callpoint,
         p_request_id          => l_req_id,
         p_ora_error_msg       => SUBSTR (p_debug_msg, 1, 2000),
         p_error_desc          => 'Error running XXWC_ACCT_MGR_UPD_FILE_PKG with PROGRAM ERROR',
         p_distribution_list   => l_distro_list,
         p_module              => 'AR');
   END write_error;

   /********************************************************************************************************************
     Procedure : generate_file
     PARAMETERS:  IN - None
                  OUT - errbuf,retcode
     PURPOSE:  This procedure creates file for the concurrent program  XXWC Credit-Account Manager Update
     REVISIONS:
       Ver        Date        Author             Description
     ---------  ----------  ---------------   -------------------------
     1.0       05/12/2016  Neha Saini         Initial Build - Task ID: 20170616-
     1.1       01/08/2018  P.Vamshidhar       TMS#20180105-00272 - Customer - Account Manager Hierarchy File update
   *********************************************************************************************************************/
   PROCEDURE generate_file (errbuf OUT VARCHAR2, retcode OUT VARCHAR2)
   IS
      -- Below customer query modified in Rev 1.1
      CURSOR cur_open_bal
      IS
         SELECT SALESREP_ID,
                resource_id,
                resource_name,
                resource_email,
                bm,
                (SELECT JRR.resource_id
                   FROM JTF.JTF_RS_RESOURCE_EXTNS_TL JRR
                  WHERE RESOURCE_NAME = bm)
                   bm_mgr_id,
                (SELECT jrs.SOURCE_EMAIL
                   FROM apps.JTF_RS_RESOURCE_EXTNS_VL jrs
                  WHERE RESOURCE_NAME = bm)
                   bm_email,
                rvp,
                (SELECT JRR.resource_id
                   FROM JTF.JTF_RS_RESOURCE_EXTNS_TL JRR
                  WHERE RESOURCE_NAME = rvp)
                   rvp_mgr_id,
                (SELECT jrs.SOURCE_EMAIL
                   FROM apps.JTF_RS_RESOURCE_EXTNS_VL jrs
                  WHERE RESOURCE_NAME = rvp)
                   rvp_email,
                dsm,
                (SELECT JRR.resource_id
                   FROM JTF.JTF_RS_RESOURCE_EXTNS_TL JRR
                  WHERE RESOURCE_NAME = dsm)
                   dsm_mgr_id,
                (SELECT jrs.SOURCE_EMAIL
                   FROM apps.JTF_RS_RESOURCE_EXTNS_VL jrs
                  WHERE RESOURCE_NAME = dsm)
                   dsm_email,
                rsm,
                (SELECT JRR.resource_id
                   FROM JTF.JTF_RS_RESOURCE_EXTNS_TL JRR
                  WHERE RESOURCE_NAME = rsm)
                   rsm_mgr_id,
                (SELECT jrs.SOURCE_EMAIL
                   FROM apps.JTF_RS_RESOURCE_EXTNS_VL jrs
                  WHERE RESOURCE_NAME = rsm)
                   rsm_email,
                dm,
                (SELECT JRR.resource_id
                   FROM JTF.JTF_RS_RESOURCE_EXTNS_TL JRR
                  WHERE RESOURCE_NAME = DM)
                   dm_mgr_id,
                (SELECT jrs.SOURCE_EMAIL
                   FROM apps.JTF_RS_RESOURCE_EXTNS_VL jrs
                  WHERE RESOURCE_NAME = dm)
                   dm_email,
                (SELECT REPLACE (loc.BRANCH_NICKNAME, 'HD Supply White Cap ')
                   FROM hdscmmn.locdirplus_vw@EAAPXPRD_REBATES_LNK.HSI.HUGHESSUPPLY.COM LOC,
                        apps.PER_ALL_PEOPLE_F PAPF,
                        APPS.FND_USER FU
                  WHERE     UPPER (loc.BRMGR_NTID) = UPPER (FU.USER_NAME)
                        AND FU.EMPLOYEE_ID = PAPF.PERSON_ID
                        AND SYSDATE BETWEEN PAPF.EFFECTIVE_START_DATE
                                        AND PAPF.EFFECTIVE_END_DATE
                        AND TO_CHAR (NVL (BM_MGR_EEID, 0)) =
                               PAPF.EMPLOYEE_NUMBER
                        AND LOC.SPACE_TYPE = 'Wholesale Outlet'
                        AND LOC.LOB_DBA LIKE 'Construction%Industrial'
                        AND ROWNUM = 1)
                   BRANCH_NICKNAME                            -- Added Rev 1.1
           FROM (SELECT DISTINCT
                        jrs.SALESREP_ID,
                        jrs.RESOURCE_ID,
                        jrrev.RESOURCE_NAME,
                        jrrev.SOURCE_EMAIL resource_email,
                        jrrev.user_name,
                        jrs.person_id,
                        CASE
                           WHEN jrrev.category = 'EMPLOYEE'
                           THEN
                              (SELECT MGR_NAME
                                 FROM XXWC.XXWC_SR_SALESREP_HIERARCHY a
                                WHERE a.mgt_lvl = 1 AND a.sr_id = xssh.sr_id)
                           WHEN jrrev.category = 'OTHER'
                           THEN
                              (SELECT MGR_NAME
                                 FROM XXWC.XXWC_SR_SALESREP_HIERARCHY a
                                WHERE a.mgt_lvl = 1 AND a.sr_id = xssh.sr_id)
                        END
                           BM,
                        CASE
                           WHEN jrrev.category = 'EMPLOYEE'
                           THEN
                              (SELECT MGR_EEID
                                 FROM XXWC.XXWC_SR_SALESREP_HIERARCHY a
                                WHERE a.mgt_lvl = 1 AND a.sr_id = xssh.sr_id)
                           WHEN jrrev.category = 'OTHER'
                           THEN
                              (SELECT MGR_EEID
                                 FROM XXWC.XXWC_SR_SALESREP_HIERARCHY a
                                WHERE a.mgt_lvl = 1 AND a.sr_id = xssh.sr_id)
                        END
                           BM_MGR_EEID,                       -- Added Rev 1.1
                        CASE
                           WHEN jrrev.category = 'EMPLOYEE'
                           THEN
                              (SELECT MGR_NAME
                                 FROM XXWC.XXWC_SR_SALESREP_HIERARCHY a
                                WHERE a.mgt_lvl = 3 AND a.sr_id = xssh.sr_id)
                           WHEN jrrev.category = 'OTHER'
                           THEN
                              (SELECT MGR_NAME
                                 FROM XXWC.XXWC_SR_SALESREP_HIERARCHY a
                                WHERE a.mgt_lvl = 3 AND a.sr_id = xssh.sr_id)
                        END
                           DSM,
                        CASE
                           WHEN jrrev.category = 'EMPLOYEE'
                           THEN
                              (SELECT MGR_NAME
                                 FROM XXWC.XXWC_SR_SALESREP_HIERARCHY a
                                WHERE a.mgt_lvl = 5 AND a.sr_id = xssh.sr_id)
                           WHEN jrrev.category = 'OTHER'
                           THEN
                              (SELECT MGR_NAME
                                 FROM XXWC.XXWC_SR_SALESREP_HIERARCHY a
                                WHERE a.mgt_lvl = 5 AND a.sr_id = xssh.sr_id)
                        END
                           DM,
                        CASE
                           WHEN jrrev.category = 'EMPLOYEE'
                           THEN
                              (SELECT MGR_NAME
                                 FROM XXWC.XXWC_SR_SALESREP_HIERARCHY a
                                WHERE a.mgt_lvl = 7 AND a.sr_id = xssh.sr_id)
                           WHEN jrrev.category = 'OTHER'
                           THEN
                              (SELECT MGR_NAME
                                 FROM XXWC.XXWC_SR_SALESREP_HIERARCHY a
                                WHERE a.mgt_lvl = 7 AND a.sr_id = xssh.sr_id)
                        END
                           RSM,
                        CASE
                           WHEN jrrev.category = 'EMPLOYEE'
                           THEN
                              (SELECT MGR_NAME
                                 FROM XXWC.XXWC_SR_SALESREP_HIERARCHY a
                                WHERE a.mgt_lvl = 9 AND a.sr_id = xssh.sr_id)
                           WHEN jrrev.category = 'OTHER'
                           THEN
                              (SELECT MGR_NAME
                                 FROM XXWC.XXWC_SR_SALESREP_HIERARCHY a
                                WHERE a.mgt_lvl = 9 AND a.sr_id = xssh.sr_id)
                        END
                           RVP
                   FROM XXWC.XXWC_SR_SALESREP_HIERARCHY xssh,
                        apps.JTF_RS_SALESREPS jrs,
                        APPS.JTF_RS_RESOURCE_EXTNS_VL jrrev
                  WHERE     1 = 1
                        AND xssh.SR_ID = jrs.salesrep_id
                        AND jrs.RESOURCE_ID = jrrev.RESOURCE_ID);


      l_filename         VARCHAR2 (200);
      l_count            NUMBER;
      g_prog_exception   EXCEPTION;
      v_file             UTL_FILE.file_type;
      l_err_msg          VARCHAR2 (5000);
      l_directory_name   VARCHAR2 (5000) := 'XXWC_CREDIT_ACCT_MGR_FILE';
   BEGIN
      --setting context
      BEGIN
         apps.mo_global.set_policy_context ('S', 162);
      END;

      -- printing in Parameters
      g_loc := 'Begining sample file Extract  ';
      write_log (g_loc);
      write_log ('========================================================');


      --Initialize the Out Parameter
      errbuf := NULL;
      retcode := '0';



      -- Get the file name
      SELECT 'AcctMgrHeirarchy' || TO_CHAR (SYSDATE, 'MMDDRRRR') || '.csv'
        INTO l_filename
        FROM DUAL;

      g_loc := 'getting file name ';
      write_log (g_loc || 'l_filename:  ' || l_filename);

      --Open the file handler
      g_loc := 'Open File handler ';
      write_log (g_loc);
      v_file :=
         UTL_FILE.fopen (LOCATION       => l_directory_name,
                         filename       => l_filename,
                         open_mode      => 'w',
                         max_linesize   => 32767);

      g_loc := 'Writing to the file ... Opening Cursor ';
      write_log (g_loc);

      l_count := 0;
      g_loc := 'Resetting the counter and now writing in file first line  ';
      write_log (g_loc);

      -- BM_BR column added in Rev 1.1
      UTL_FILE.put_line (
         v_file,
         UPPER (
               'SALESREP_ID'
            || ','
            || 'RESOURCE_ID'
            || ','
            || 'RESOURCE_NAME'
            || ','
            || 'RESOURCE_EMAIL'
            || ','
            || 'BM_NAME'
            || ','
            || 'BM_EMAIL'
            || ','
            || 'BM_ID'
            || ','
            || 'DSM_NAME'
            || ','
            || 'DSM_EMAIL'
            || ','
            || 'DSM_ID'
            || ','
            || 'RSM_NAME'
            || ','
            || 'RSM_EMAIL'
            || ','
            || 'RSM_ID'
            || ','
            || 'RVP_NAME'
            || ','
            || 'RVP_EMAIL'
            || ','
            || 'RVP_ID'
            || ','
            || 'DM_NAME'
            || ','
            || 'DM_EMAIL'
            || ','
            || 'DM_ID'
            || ','
            || 'BM_BR'                                     -- Added in Rev 1.1
            || ','));
      g_loc := 'now writing in file whole data ';
      write_log (g_loc);

      FOR cur_rec IN cur_open_bal
      LOOP
         EXIT WHEN cur_open_bal%NOTFOUND;

         BEGIN
            -- Writing File Lines

            UTL_FILE.put_line (
               v_file,
               UPPER (
                     TRIM (cur_rec.SALESREP_ID)
                  || ','
                  || TRIM (cur_rec.RESOURCE_ID)
                  || ','
                  || REPLACE (TRIM (cur_rec.RESOURCE_NAME), ',', ' ')
                  || ','
                  || TRIM (cur_rec.RESOURCE_EMAIL)
                  || ','
                  || REPLACE (TRIM (cur_rec.bm), ',', ' ')
                  || ','
                  || TRIM (cur_rec.bm_email)
                  || ','
                  || TRIM (cur_rec.bm_mgr_id)
                  || ','
                  || REPLACE (TRIM (cur_rec.dsm), ',', ' ')
                  || ','
                  || TRIM (cur_rec.dsm_email)
                  || ','
                  || TRIM (cur_rec.dsm_mgr_id)
                  || ','
                  || REPLACE (TRIM (cur_rec.rsm), ',', ' ')
                  || ','
                  || TRIM (cur_rec.rsm_email)
                  || ','
                  || TRIM (cur_rec.rsm_mgr_id)
                  || ','
                  || REPLACE (TRIM (cur_rec.rvp), ',', ' ')
                  || ','
                  || TRIM (cur_rec.rvp_email)
                  || ','
                  || TRIM (cur_rec.rvp_mgr_id)
                  || ','
                  || REPLACE (TRIM (cur_rec.dm), ',', ' ')
                  || ','
                  || TRIM (cur_rec.dm_email)
                  || ','
                  || TRIM (cur_rec.dm_mgr_id)
                  || ','
                  || TRIM (cur_rec.BRANCH_NICKNAME) -- Added column in Rev 1.1
                  || ','));

            l_count := l_count + 1;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_err_msg :=
                     'Error at --'
                  || cur_rec.salesrep_id
                  || '---- '
                  || SUBSTR (SQLERRM, 1, 2000);
               write_error (l_err_msg);
               write_log (l_err_msg);
         --   if one record has an issue whole program should not fail on capture error and send email
         END;
      END LOOP;

      write_log ('Wrote ' || l_count || ' records to file');
      write_log ('Closing File Handler');
      g_loc := 'now closing in file ';
      --Closing the file handler
      UTL_FILE.fclose (v_file);

      retcode := '0';
      write_log (
         'XXWC Credit-Account Manager Update- Program Successfully completed');
   EXCEPTION
      WHEN UTL_FILE.invalid_path
      THEN
         UTL_FILE.fclose (v_file);
         errbuf := 'File Path is invalid.';
         retcode := '2';
         write_error (errbuf);
         write_log (errbuf);
      WHEN UTL_FILE.invalid_mode
      THEN
         UTL_FILE.fclose (v_file);
         errbuf := 'The open_mode parameter in FOPEN is invalid.';
         retcode := '2';
         write_error (errbuf);
         write_log (errbuf);
      WHEN UTL_FILE.invalid_filehandle
      THEN
         UTL_FILE.fclose (v_file);
         errbuf := 'File handle is invalid..';
         retcode := '2';
         write_error (errbuf);
         write_log (errbuf);
      WHEN UTL_FILE.invalid_operation
      THEN
         UTL_FILE.fclose (v_file);
         errbuf := 'File could not be opened or operated on as requested';
         retcode := '2';
         write_error (errbuf);
         write_log (errbuf);
      WHEN UTL_FILE.write_error
      THEN
         UTL_FILE.fclose (v_file);
         errbuf :=
            'Operating system error occurred during the write operation';
         retcode := '2';
         write_error (errbuf);
         write_log (errbuf);
      WHEN UTL_FILE.internal_error
      THEN
         UTL_FILE.fclose (v_file);
         write_log ('Unspecified PL/SQL error.');
      WHEN UTL_FILE.file_open
      THEN
         UTL_FILE.fclose (v_file);
         errbuf := 'The requested operation failed because the file is open.';
         retcode := '2';
         write_error (errbuf);
         write_log (errbuf);
      WHEN UTL_FILE.invalid_filename
      THEN
         UTL_FILE.fclose (v_file);
         errbuf := 'The filename parameter is invalid.';
         retcode := '2';
         write_error (errbuf);
         write_log (errbuf);
      WHEN UTL_FILE.access_denied
      THEN
         UTL_FILE.fclose (v_file);
         errbuf := 'Permission to access to the file location is denied.';
         retcode := '2';
         write_error (errbuf);
         write_log (errbuf);
      WHEN g_prog_exception
      THEN
         errbuf := l_err_msg;
         retcode := '2';
         write_error (l_err_msg);
         write_log (l_err_msg);
      WHEN OTHERS
      THEN
         -- UTL_FILE.fclose (v_file);
         l_err_msg := 'Error Msg :' || SQLERRM;
         errbuf := l_err_msg;
         retcode := '2';
         write_error (l_err_msg);
         write_log (l_err_msg);
   END generate_file;
END XXWC_ACCT_MGR_UPD_FILE_PKG;
/