CREATE OR REPLACE PACKAGE BODY APPS.XXWC_OM_EMAIL_CUSTOMER_PKG
IS
   -- ==========================================================================================================
   -- Package: XXWC_OM_EMAIL_CUSTOMER_PKG.pkb
   -- ==========================================================================================================
   -- VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- --------------------------------------------------------------------
   -- 1.0    11-Sep-2017    P.Vamshidhar    TMS#20170207-00206 - Research the issue where users are able to enter
   --                                       invalid email address and Fax numbers while app
   --                                       Initial version
   -- ==========================================================================================================

   PROCEDURE MAIN (x_errbuf              OUT VARCHAR2,
                   x_retcode             OUT NUMBER,
                   p_request_id       IN     NUMBER,
                   p_transaction_id   IN     VARCHAR2,
                   p_receiver_email   IN     VARCHAR2,
                   p_customer_name    IN     VARCHAR2,
                   p_contact          IN     VARCHAR2)
   IS
      -- ==========================================================================================================
      -- Procedure: Main
      -- ==========================================================================================================
      -- VERSION DATE          AUTHOR(S)       DESCRIPTION
      -- ------- -----------   --------------- --------------------------------------------------------------------
      -- 1.0    11-Sep-2017    P.Vamshidhar    TMS#20170207-00206 - Research the issue where users are able to enter
      --                                       invalid email address and Fax numbers while app
      --                                       Initial version
      -- ==========================================================================================================
      lvc_email_subject      VARCHAR2 (32767);
      lvc_email_dir          VARCHAR2 (32767) := 'XXWC_OM_CUST_EMAIL_PDF_DIR';
      lvc_utl_file           UTL_FILE.file_type;
      lvc_email_body_file    VARCHAR2 (1000);
      lvc_cp_out_pdf_name    VARCHAR2 (1000);
      lvc_email_attachment   VARCHAR2 (1000);
      lvc_sec                VARCHAR2 (4000);
      lvc_email_sender       VARCHAR2 (1000);
      lvc_return_status      VARCHAR2 (1000);
      ln_user_id             FND_USER.USER_ID%TYPE := FND_GLOBAL.USER_ID;
      lvc_sender_name        VARCHAR2 (200);
      lvc_org_fax_num        VARCHAR2 (20);
      lvc_order_number       OE_ORDER_HEADERS_ALL.ORDER_NUMBER%TYPE;
      lvc_comments           VARCHAR2 (240);
      lvc_email_unix_dir     VARCHAR2 (1000);
      l_err_callfrom2        VARCHAR2 (1000)
                                := 'XXWC_OM_EMAIL_CUSTOMER_PKG.MAIN';
      lvc_prog_short_name    FND_CONCURRENT_PROGRAMS.CONCURRENT_PROGRAM_NAME%TYPE;
      cust_exp               EXCEPTION;
   BEGIN
      lvc_sec := 'Deriving Senders email address and name';

      BEGIN
         SELECT NVL (NVL (papf.email_address, fu.email_address),
                     'noreply@hdsupply.com'),
                papf.first_name || ' ' || papf.last_name
           INTO lvc_email_sender, lvc_sender_name
           FROM apps.per_all_people_f papf, apps.fnd_user fu
          WHERE     papf.person_id = fu.employee_id
                AND SYSDATE BETWEEN EFFECTIVE_START_DATE
                                AND EFFECTIVE_END_DATE
                AND fu.user_id = ln_user_id
                AND NVL (papf.email_address, fu.email_address) IS NOT NULL;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            lvc_email_sender := 'noreply@hdsupply.com';
         WHEN OTHERS
         THEN
            lvc_email_sender := 'noreply@hdsupply.com';
      END;


      lvc_sec := 'Deriving email attachment file name from lookup';

      BEGIN
         SELECT a.DESCRIPTION, B.PROGRAM_SHORT_NAME
           INTO lvc_email_attachment, lvc_prog_short_name
           FROM APPS.FND_LOOKUP_VALUES A, APPS.FND_CONC_REQ_SUMMARY_V B
          WHERE     A.LOOKUP_TYPE = 'XXWC_OM_CFD_EMAIL_ATTACHMENTS'
                AND A.LOOKUP_CODE = B.PROGRAM_SHORT_NAME
                AND B.REQUEST_ID = P_REQUEST_ID;
      EXCEPTION
         WHEN OTHERS
         THEN
            FND_FILE.PUT_LINE (FND_FILE.LOG,
                               'Error ocurred ' || SUBSTR (SQLERRM, 1, 250));

            SELECT PROGRAM_SHORT_NAME
              INTO lvc_email_attachment
              FROM APPS.FND_CONC_REQ_SUMMARY_V B
             WHERE REQUEST_ID = P_REQUEST_ID;
      END;



      lvc_sec := 'Deriving Organization Fax Number and Order Number';

      IF lvc_prog_short_name IN ('XXWC_OM_QUOTE_NEW')
      THEN
         BEGIN
            SELECT b.TELEPHONE_NUMBER_2, c.quote_number
              INTO lvc_org_fax_num, lvc_order_number
              FROM apps.HR_ORGANIZATION_UNITS_V a,
                   apps.hr_locations b,
                   xxwc.xxwc_om_quote_headers c
             WHERE     a.organization_id = c.ORGANIZATION_ID
                   AND a.location_id = b.location_id
                   AND c.quote_number = p_transaction_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               lvc_org_fax_num := NULL;
               lvc_order_number := NULL;
         END;
      ELSE
         BEGIN
            SELECT b.TELEPHONE_NUMBER_2, c.order_number
              INTO lvc_org_fax_num, lvc_order_number
              FROM apps.HR_ORGANIZATION_UNITS_V a,
                   apps.hr_locations b,
                   APPS.OE_ORDER_HEADERS_ALL c
             WHERE     a.organization_id = c.ship_from_org_id
                   AND a.location_id = b.location_id
                   AND c.header_id = p_transaction_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               lvc_org_fax_num := NULL;
               lvc_order_number := NULL;
         END;
      END IF;

      lvc_sec := 'Deriving Order Notes to Customer';

      BEGIN
         SELECT DECODE (concurrent_program_name,
                        'XXWC_OM_SRECEIPT', ARGUMENT7,
                        'XXWC_OM_SRECEIPT_CUSTOMER', ARGUMENT7,
                        ARGUMENT8)
           INTO lvc_comments
           FROM XXWC_PRINT_LOG_VW
          WHERE     HEADER_ID = p_transaction_id
                AND TABLE_NAME = 'OE_ORDER_HEADERS_ALL'
                AND REQUEST_ID = p_request_id;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            lvc_comments := NULL;
         WHEN OTHERS
         THEN
            lvc_comments := NULL;
      END;


      lvc_sec := 'Generating email attachment file name';

      BEGIN
         SELECT    directory_path
                || '/'
                || lvc_email_attachment
                || '_'
                || lvc_order_number
                || '.pdf',
                directory_path
           INTO lvc_email_attachment, lvc_email_unix_dir
           FROM dba_directories
          WHERE directory_name = lvc_email_dir;
      EXCEPTION
         WHEN OTHERS
         THEN
            FND_FILE.PUT_LINE (FND_FILE.LOG,
                               'Error occured ' || SUBSTR (SQLERRM, 1, 250));
      END;

      lvc_sec := 'Deriving concurrent Program output pdf file name ';

      BEGIN
         SELECT    SUBSTR (a.OUTFILE_NAME,
                           1,
                           INSTR (a.OUTFILE_NAME, 'out/', 1) + 3)
                || b.PROGRAM_SHORT_NAME
                || '_'
                || A.REQUEST_ID
                || '_1.PDF'
           INTO lvc_cp_out_pdf_name
           FROM apps.fnd_concurrent_requests a, apps.fnd_conc_req_summary_v b
          WHERE a.request_id = b.request_id AND a.request_id = P_REQUEST_ID;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            lvc_cp_out_pdf_name := NULL;
         WHEN OTHERS
         THEN
            RAISE cust_exp;
      END;

      -- Email body.
      lvc_sec := 'Generating email body';
      FND_MESSAGE.clear;
      fnd_message.set_name ('XXWC', 'XXWC_OM_PDF_EMAIL_TEXT');
      fnd_message.set_token ('P_CUSTOMER_NAME', p_customer_name);
      --fnd_message.set_token ('P_ORDER_BILL_TO_CONTACT', P_CONTACT);
      fnd_message.set_token ('P_USER_NAME', lvc_sender_name);
      fnd_message.set_token ('P_FAX_NUM', lvc_org_fax_num);
      fnd_message.set_token ('P_COMMENTS', lvc_comments);

      lvc_sec := 'Generating email body file name';

      lvc_email_body_file := 'OM' || '_' || p_request_id || '_US.txt';

      lvc_sec := 'Generating email body file in unix';

      lvc_utl_file :=
         UTL_FILE.fopen (lvc_email_dir,
                         lvc_email_body_file,
                         'W',
                         32767);

      UTL_FILE.put_line (lvc_utl_file, FND_MESSAGE.GET);

      UTL_FILE.fclose (lvc_utl_file);


      lvc_sec := ' ';


      -- Email subject.
      lvc_sec := 'Generating email subject';
      FND_MESSAGE.clear;
      fnd_message.set_name ('XXWC', 'XXWC_OM_CFD_EMAIL_SUB');
      fnd_message.set_token ('ORDER_NUMBER', TO_CHAR (lvc_order_number));
      lvc_email_subject := FND_MESSAGE.GET;


      FND_FILE.PUT_LINE (FND_FILE.LOG, 'email_subject ' || lvc_email_subject);
      FND_FILE.PUT_LINE (
         FND_FILE.LOG,
         'email_body    ' || lvc_email_unix_dir || '/' || lvc_email_body_file);
      FND_FILE.PUT_LINE (FND_FILE.LOG, 'email_sender ' || lvc_email_sender);
      FND_FILE.PUT_LINE (FND_FILE.LOG, 'email_receiver ' || p_receiver_email);
      FND_FILE.PUT_LINE (FND_FILE.LOG,
                         'email_attachment ' || lvc_email_attachment);
      FND_FILE.PUT_LINE (FND_FILE.LOG,
                         'CP Output pdf file ' || lvc_cp_out_pdf_name);

      INSERT INTO XXWC.XXWC_OM_EMAIL_COMM_HIST_TBL (ORD_HEADER_ID,
                                                    SENDER_EMAIL_ADD,
                                                    RCV_EMAIL_ADD,
                                                    EMAIL_SUBJECT,
                                                    EMAIL_BODY,
                                                    EMAIL_ATTACH,
                                                    CONC_REQUEST_ID,
                                                    CREATION_DATE)
           VALUES (p_transaction_id,
                   lvc_email_sender,
                   p_receiver_email,
                   lvc_email_subject,
                   lvc_email_unix_dir || '/' || lvc_email_body_file,
                   lvc_email_attachment,
                   FND_GLOBAL.CONC_REQUEST_ID,
                   SYSDATE);



      SENDING_EMAIL (
         p_email_subject        => lvc_email_subject,
         p_email_body_file      =>    lvc_email_unix_dir
                                   || '/'
                                   || lvc_email_body_file,
         p_email_sender         => lvc_email_sender,
         p_email_receiver       => p_receiver_email,
         p_email_attachment     => lvc_email_attachment,
         p_cp_output_pdf_file   => lvc_cp_out_pdf_name,
         p_attachment_file      => lvc_email_attachment);
   EXCEPTION
      WHEN CUST_EXP
      THEN
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => l_err_callfrom2,
            p_calling             => lvc_sec,
            p_ora_error_msg       => SUBSTR (SQLERRM, 1, 250),
            p_error_desc          => 'Error running XXWC_OM_EMAIL_CUSTOMER_PKG package with PROGRAM ERROR',
            p_distribution_list   => 'OracleDevelopmentGroup@hdsupply.com',
            p_module              => 'XXWC');
      WHEN OTHERS
      THEN
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => l_err_callfrom2,
            p_calling             => lvc_sec,
            p_ora_error_msg       => SUBSTR (SQLERRM, 1, 250),
            p_error_desc          => 'Error running XXWC_OM_EMAIL_CUSTOMER_PKG package with PROGRAM ERROR',
            p_distribution_list   => 'OracleDevelopmentGroup@hdsupply.com',
            p_module              => 'XXWC');
   END;

   -- ==========================================================================================================
   -- Procedure: SENDING_EMAIL
   -- ==========================================================================================================
   -- VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- --------------------------------------------------------------------
   -- 1.0    11-Sep-2017    P.Vamshidhar    TMS#20170207-00206 - Research the issue where users are able to enter
   --                                       invalid email address and Fax numbers while app
   --                                       Initial version
   -- ==========================================================================================================

   PROCEDURE SENDING_EMAIL (p_email_subject        IN VARCHAR2,
                            p_email_body_file      IN VARCHAR2,
                            p_email_sender         IN VARCHAR2,
                            p_email_receiver       IN VARCHAR2,
                            p_email_attachment     IN VARCHAR2,
                            p_cp_output_pdf_file   IN VARCHAR2,
                            p_attachment_file      IN VARCHAR2)
   IS
      lvc_cc_email_id   VARCHAR2 (1000);
      lvc_sec           VARCHAR2 (1000);
      l_err_callfrom2   VARCHAR2 (1000)
                           := 'XXWC_OM_EMAIL_CUSTOMER_PKG.SENDING_EMAIL';
      ln_request_id     FND_CONCURRENT_REQUESTS.REQUEST_ID%TYPE;
   BEGIN
      lvc_sec := 'Procedure start';
      ln_request_id :=
         fnd_request.submit_request (
            application   => 'XXWC',
            program       => 'XXWC_OM_EMAIL_CUSTOMER',
            description   => '',
            start_time    => '',
            sub_request   => FALSE,
            argument1     => p_email_subject,
            argument2     => p_email_body_file,
            argument3     => p_email_sender,
            argument4     => p_email_receiver,
            argument5     => p_email_attachment,
            argument6     => lvc_cc_email_id,
            argument7     => p_cp_output_pdf_file,
            argument8     => p_attachment_file);

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => l_err_callfrom2,
            p_calling             => lvc_sec,
            p_ora_error_msg       => SUBSTR (SQLERRM, 1, 250),
            p_error_desc          => 'Error running XXWC_OM_EMAIL_CUSTOMER_PKG package with PROGRAM ERROR',
            p_distribution_list   => 'OracleDevelopmentGroup@hdsupply.com',
            p_module              => 'XXWC');
   END;
END XXWC_OM_EMAIL_CUSTOMER_PKG;
/