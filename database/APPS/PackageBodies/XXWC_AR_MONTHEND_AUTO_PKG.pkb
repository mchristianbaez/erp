CREATE OR REPLACE PACKAGE BODY APPS.XXWC_AR_MONTHEND_AUTO_PKG
IS
   /*************************************************************************
   *  Copyright (c) 2016 HD Supply Inc.--ver1.2
   *  All rights reserved.
   **************************************************************************
   *   $Header XXWC_AR_MONTHEND_AUTO_PKG.pkb $
   *   Module Name: XXWC_AR_MONTHEND_AUTO_PKG.pkb
   *
   *   PURPOSE:   This package is used to automate the AR month end process
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0       6/1/2016     Neha Saini             TMS# 20150716-00173 AR month end process
   *   1.1       9/26/2016    Neha Saini             TMS# 20150716-00173 Added new procedure and changed MV name for snapshot
   *   1.2       10/18/2016   Neha Saini             TMS# 20161018-00116 Added period name paramters in poen and close period and changed future period logic in get current period.
   *   1.3       11/16/2016   Neha Saini             TMS# TMS# 20160930-00015  adding future period start date and end parameter
   * ************************************************************************************************************************************************************************************************************/

   /*****************************
   -- GLOBAL VARIABLES
   ******************************/

   g_error_message   VARCHAR2 (32000);
   g_location        VARCHAR2 (5000);



   /*************************************************************************
   Procedure : Write_Error

  PURPOSE:   This procedure logs error message
  Parameter:
         IN
             p_debug_msg      -- Debug Message
************************************************************************/



   --add message to concurrent output file
   PROCEDURE write_error (p_debug_msg IN VARCHAR2)
   IS
      l_err_callfrom    VARCHAR2 (75) DEFAULT 'XXWC_AR_MONTHEND_AUTO_PKG';
      l_err_callpoint   VARCHAR2 (75) DEFAULT 'START';
      l_distro_list     VARCHAR2 (75)
                           DEFAULT 'HDSOracleDevelopers@hdsupply.com';
   BEGIN
      xxcus_error_pkg.xxcus_error_main_api (
         p_called_from         => l_err_callfrom,
         p_calling             => l_err_callpoint,
         p_request_id          => g_conc_request_id,
         p_ora_error_msg       => SUBSTR (p_debug_msg, 1, 2000),
         p_error_desc          => 'Error running XXWC_AR_MONTHEND_AUTO_PKG with PROGRAM ERROR ',
         p_distribution_list   => l_distro_list,
         p_module              => 'AR');
   END write_error;

   /*************************************************************************
   *   Procedure : Write_output
   *
   *   PURPOSE:   This procedure is the table handler called by the On-Insert Trigger
   *   Parameter:
   *          IN
   *              p_debug_msg      -- Debug Message
   * ************************************************************************/

   --add message to concurrent output file
   PROCEDURE Write_output (p_debug_msg IN VARCHAR2)
   IS
   BEGIN
      IF g_conc_request_id = 0
      THEN
         DBMS_OUTPUT.PUT_LINE (p_debug_msg);
      ELSE
         fnd_file.put_line (fnd_file.output, p_debug_msg);
         fnd_file.put_line (fnd_file.LOG, p_debug_msg);
      END IF;
   END Write_output;

   --ver 1.1 --starts
   /*************************************************************************
 *   Procedure : get_amount_due
 *
 *   PURPOSE:   This procedure is to get amount due remaining comparison
 *   Parameter:
 *          IN  NONE
 *          OUT   ERRBUF,RETCODE
 *   REVISIONS:
 *   Ver        Date        Author                     Description
 *   ---------  ----------  ---------------         -------------------------
 *   1.0       6/1/2016     Neha Saini             TMS# 20150716-00173 AR month end process
 * ************************************************************************/

   PROCEDURE get_amount_due (ERRBUF OUT VARCHAR2, RETCODE OUT NUMBER)
   IS
      l_amt_due_pay_schedule   NUMBER;
      l_amt_due_mv_refresh     NUMBER;
      PROGRAM_ERROR            EXCEPTION;
   BEGIN
      g_location := 'starting get_amount_due';
      write_output (g_location);
      g_error_message := NULL;
      ERRBUF := NULL;
      RETCODE := 0;

      -- GET Amount due remaining from ap_payment_schedule
      BEGIN
         SELECT SUM (amount_due_remaining)
           INTO l_amt_due_pay_schedule
           FROM apps.ar_payment_schedules_all
          WHERE org_id = 162;
      EXCEPTION
         WHEN OTHERS
         THEN
            g_error_message := SQLERRM;
      END;

      g_location :=
            'l_amt_due_pay_schedule amount due for AR payment schedule: '
         || l_amt_due_pay_schedule;
      write_output (g_location);

      -- GET Amount due remaining from AR customer BALance MV refresh
      BEGIN
         SELECT SUM (transaction_remaining_balance)
           INTO l_amt_due_mv_refresh
           FROM XXWC.XXWC_AR_CUST_BAL_MV_MONTHLY;
      EXCEPTION
         WHEN OTHERS
         THEN
            g_error_message := SQLERRM;
      END;

      g_location :=
            'l_amt_due_mv_refresh amoutn due for AR customer Balance MV refresh : '
         || l_amt_due_mv_refresh;
      write_output (g_location);

      -- Now compare both the Amount and decide if program is SUCCESS or FAILURE
      IF l_amt_due_mv_refresh = l_amt_due_pay_schedule
      THEN
         ERRBUF := 'Amount Matched';
         RETCODE := 0;
      ELSE
         ERRBUF := 'Amount Does not Matched';
         RETCODE := 2;
      END IF;


      g_location := 'completed get_amount_due';
      write_output (g_location);
   EXCEPTION
      WHEN PROGRAM_ERROR
      THEN
         g_error_message :=
            'Error at get_amount_due in g_exception: ' || g_error_message;
         write_output (g_error_message);
         write_error (g_error_message);
         ERRBUF := g_error_message;
         RETCODE := 2;
      WHEN OTHERS
      THEN
         g_error_message :=
               g_error_message
            || 'Error in main exception for get_amount_due : '
            || SQLERRM;
         write_output (g_error_message);
         write_error (g_error_message);
         ERRBUF := g_error_message;
         RETCODE := 2;
   END;

   --ver 1.1 Ended
   /*************************************************************************
   *   Procedure : pending_close_period
   *
   *   PURPOSE:   This procedure is to pending close the current period.
   *   Parameter:
   *          IN    p_period_name
   *          OUT   ERRBUF,RETCODE
   *   0 – Concurrent Program is successful.
   *   1 – Concurrent program completed with warning.
   *   2 – Concurrent Program has some Error.
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0       6/1/2016     Neha Saini             TMS# 20150716-00173 AR month end process
   *   1.1       9/26/2016    Neha Saini             TMS# 20150716-00173 Added new procedure and changed MV name for snapshot
   *   1.2       10/18/2016   Neha Saini             TMS# 20161018-00116 Added period name paramters in poen and close period and changed future period logic in get current period.
   * **********************************************************************************************************************************/

   PROCEDURE pending_close_period (ERRBUF          OUT VARCHAR2,
                                   RETCODE         OUT NUMBER,
                                   p_period_name       VARCHAR2) --ver 1.2 changes starts
   IS
      l_access_set_id    NUMBER;
      l_ledger_id        NUMBER;
      l_application_id   NUMBER;
      l_row_count        NUMBER;
      g_exception        EXCEPTION;
   BEGIN
      g_error_message := NULL;
      ERRBUF := NULL;
      RETCODE := 0;
      --ver 1.2 changes starts
      g_location := 'starting pending_close_period';
      write_output (g_location);

      l_access_set_id := apps.FND_PROFILE.VALUE ('GL_ACCESS_SET_ID');
      l_ledger_id :=
         apps.GL_ACCESS_SET_SECURITY_PKG.get_default_ledger_id (
            l_access_set_id,
            'R');
      g_location := 'got ledger id -> ' || l_ledger_id;
      write_output (g_location);

      SELECT application_id
        INTO l_application_id
        FROM apps.fnd_application
       WHERE APPLICATION_SHORT_NAME = 'AR';

      g_location := 'got application id -> ' || l_application_id;
      write_output (g_location);

      BEGIN
         UPDATE GL_PERIOD_STATUSES
            SET closing_status = 'W'
          WHERE     SET_OF_BOOKS_ID = NVL (l_ledger_id, 2061)         --ver1.1
                AND application_id = l_application_id
                AND closing_status = 'O'
                AND period_name = p_period_name;

         g_location :=
            ' No of rows updated to open next period ' || SQL%ROWCOUNT;
         write_output (g_location);
         l_row_count := SQL%ROWCOUNT;
         COMMIT;
      EXCEPTION
         WHEN OTHERS
         THEN
            g_error_message :=
                  'Error: While closing current period ==='
               || SUBSTR (SQLERRM, 1, 1000);
            RAISE g_EXCEPTION;
      END;

      IF l_row_count = 0
      THEN
         g_error_message :=
               'Error: Period --->  '
            || p_period_name
            || ' --- is not closed.Please check the period Name  ';
         RAISE g_EXCEPTION;
      END IF;

      --ver 1.2 changes ends
      g_location :=
            ' Completed successfully pending close of period -> '
         || p_period_name;
      write_output (g_location);
      ERRBUF := g_error_message;
   EXCEPTION
      WHEN G_EXCEPTION
      THEN
         write_output (g_error_message);
         write_error (g_error_message);
         ERRBUF := g_error_message;
         RETCODE := 2;
      WHEN OTHERS
      THEN
         g_location :=
               'ending in Main exception of pending_close_period conc_request_id-> '
            || g_conc_request_id
            || ' Error: '
            || SUBSTR (SQLERRM, 1, 1000);
         write_output (g_location);
         write_error (g_location);
         ERRBUF := g_location;
         RETCODE := 2;
   END pending_close_period;

   /*************************************************************************
   *   Procedure : open_next_period
   *
   *   PURPOSE:   This procedure is to open new period .
   *   Parameter:
   *          IN    p_period_name
   *          OUT   ERRBUF , RETCODE
   *   0 – Concurrent Program is successful.
   *   1 – Concurrent program completed with warning.
   *   2 – Concurrent Program has some Error.
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0       6/1/2016     Neha Saini             TMS# 20150716-00173 AR month end process
   *   1.1       9/26/2016    Neha Saini             TMS# 20150716-00173 Added new procedure and changed MV name for snapshot
   *   1.2       10/18/2016   Neha Saini             TMS# 20161018-00116 Added period name paramters in poen and close period and changed future period logic in get current period.
   * **********************************************************************************************************************************/

   PROCEDURE open_next_period (ERRBUF          OUT VARCHAR2,
                               RETCODE         OUT NUMBER,
                               p_period_name       VARCHAR2) ----ver 1.2 changes starts
   IS
      l_access_set_id    NUMBER;
      l_ledger_id        NUMBER;
      l_application_id   NUMBER;
      l_row_count        NUMBER;
      g_exception        EXCEPTION;
   BEGIN
      g_error_message := NULL;
      g_location := 'starting open_next_period';
      write_output (g_location);

      ERRBUF := NULL;
      RETCODE := 0;                                   --ver 1.2 changes starts

      l_access_set_id := apps.FND_PROFILE.VALUE ('GL_ACCESS_SET_ID');
      l_ledger_id :=
         apps.GL_ACCESS_SET_SECURITY_PKG.get_default_ledger_id (
            l_access_set_id,
            'R');
      g_location := 'got ledger id ->' || l_ledger_id;
      write_output (g_location);

      SELECT application_id
        INTO l_application_id
        FROM apps.fnd_application
       WHERE APPLICATION_SHORT_NAME = 'AR';

      g_location := 'got application id -> ' || l_application_id;
      write_output (g_location);

      BEGIN
         UPDATE GL_PERIOD_STATUSES
            SET closing_status = 'O'
          WHERE     SET_OF_BOOKS_ID = NVL (l_ledger_id, 2061)         --ver1.1
                AND application_id = l_application_id
                AND period_name = p_period_name;

         g_location :=
            ' No of rows updated to open next period ' || SQL%ROWCOUNT;
         write_output (g_location);
         l_row_count := SQL%ROWCOUNT;
         COMMIT;
      EXCEPTION
         WHEN OTHERS
         THEN
            g_error_message :=
                  'Error: while opening next period=== '
               || SUBSTR (SQLERRM, 1, 1000);
            RAISE G_EXCEPTION;
      END;

      IF l_row_count = 0
      THEN
         g_error_message :=
               'Error: Period --->  '
            || p_period_name
            || ' --- is not Open. Please check the period Name ';
         RAISE g_EXCEPTION;
      END IF;

      --ver 1.2 changes ends
      ERRBUF := g_error_message;
      RETCODE := 0;
      g_location := ' future period which is  open ' || p_period_name;
      write_output (g_location);
   EXCEPTION
      WHEN G_EXCEPTION
      THEN
         write_output (g_error_message);
         write_error (g_error_message);
         ERRBUF := g_error_message;
         RETCODE := 2;
      WHEN OTHERS
      THEN
         g_location :=
            'ending in Main exception of open_next_period ' || SQLERRM;
         write_output (g_location);
         write_error (g_location);
         g_error_message :=
               'When others Main open_next_period Conc request Id: -> '
            || g_conc_request_id
            || ' Error: '
            || SQLERRM;
         ERRBUF := g_error_message;
         RETCODE := 2;
   END open_next_period;

   /*************************************************************************
   *   Procedure : get_current_period
   *
   *   PURPOSE:   This procedure is to get the current and future period name to clos eand open respectively.
   *   Parameter:
   *          IN  NONE
   *          OUT   p_current_period_name,p_future_period_name ,p_error_message
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0       6/1/2016     Neha Saini             TMS# 20150716-00173 AR month end process
   *   1.1       9/26/2016    Neha Saini             TMS# 20150716-00173 Added new procedure and changed MV name for snapshot
   *   1.2       10/18/2016   Neha Saini             TMS# 20161018-00116 Added period name paramters in poen and close period and changed future period logic in get current period.
   *   1.3       11/16/2016   Neha Saini              TMS# TMS# 20160930-00015  adding future period start date and end parameter
   * *********************************************************************************************************************************/

   PROCEDURE get_current_period (p_current_period_name   OUT VARCHAR2,
                                 p_future_period_name    OUT VARCHAR2,
                                 p_start_date            OUT DATE,
                                 p_end_date              OUT DATE,
                                 p_oem_sunday            OUT DATE,
                                 p_oem_sunday_check      OUT VARCHAR2,
                                 p_future_start_date     OUT DATE,--ver1.3
                                 p_future_end_date       OUT DATE,--ver1.3
                                 p_error_message         OUT VARCHAR2)
   IS
      l_access_set_id    NUMBER;
      l_ledger_id        NUMBER;
      l_application_id   NUMBER;
      g_exception        EXCEPTION;
   BEGIN
      --ver 1.2 changes starts
      g_error_message := NULL;
      g_location := 'starting get_current_period';
      write_output (g_location);


      l_access_set_id := apps.FND_PROFILE.VALUE ('GL_ACCESS_SET_ID');
      l_ledger_id :=
         apps.GL_ACCESS_SET_SECURITY_PKG.get_default_ledger_id (
            l_access_set_id,
            'R');
      g_location := 'got ledger id ' || l_ledger_id;
      write_output (g_location);

      SELECT application_id
        INTO l_application_id
        FROM apps.fnd_application
       WHERE APPLICATION_SHORT_NAME = 'AR';

      g_location := 'got application id  ' || l_application_id;
      write_output (g_location);

      BEGIN
         SELECT start_date, end_date, period_name
           INTO p_start_date, p_end_date, p_current_period_name
           FROM apps.gl_period_statuses glp
          WHERE     1 = 1
                AND closing_status = 'O'
                AND glp.ledger_id = NVL (l_ledger_id, 2061)           --ver1.1
                AND glp.adjustment_period_flag = 'N'
                AND glp.application_id = l_application_id;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            g_error_message :=
                  'Error: OPEN period does not Exists.  '
               || SUBSTR (SQLERRM, 1, 1000);
            RAISE G_EXCEPTION;
         WHEN OTHERS
         THEN
            g_error_message :=
                  'Error: Getting current open period and period dates  '
               || SUBSTR (SQLERRM, 1, 1000);
            RAISE G_EXCEPTION;
      END;

      g_location := 'Noe trying to get future period name ';
      write_output (g_location);

      g_location :=
            'getting end date for current period- '
         || p_current_period_name
         || ' end date --'
         || p_end_date;
      write_output (g_location);

      BEGIN
         SELECT period_name,start_date,end_date
           INTO p_future_period_name,p_future_start_date,p_future_end_date--ver1.3
           FROM apps.gl_period_statuses glp
          WHERE     1 = 1
                AND closing_status = 'N'
                AND glp.ledger_id = NVL (2061, 2061)                  --ver1.1
                AND glp.adjustment_period_flag = 'N'
                AND glp.application_id = 222
                AND glp.start_date = p_end_date + 1;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            g_error_message :=
                  'Error: No future period with status not opened where start date equals  '
               || p_end_date
               || ' '
               || SUBSTR (SQLERRM, 1, 1000);
            RAISE G_EXCEPTION;
         WHEN OTHERS
         THEN
            g_error_message :=
                  'Error: Getting future period and period dates  '
               || SUBSTR (SQLERRM, 1, 1000);
            RAISE G_EXCEPTION;
      END;

      g_location :=
         'Here we got the future period name ' || p_future_period_name;
      write_output (g_location);

      g_location := 'Now getting OEM Check';
      write_output (g_location);

      g_location :=
            ' end of the date period this month  '
         || TO_DATE (TRUNC (p_end_date));
      write_output (g_location);

      IF TO_CHAR (TRUNC (SYSDATE), 'DD-MON-RR') = TRUNC (p_end_date)
      THEN
         p_oem_sunday_check := 'YES';
         g_location := 'today is end of period date' || p_oem_sunday_check;
         write_output (g_location);
      ELSE
         p_oem_sunday_check := 'NO';
         g_location :=
            'Today is not end of the period date' || p_oem_sunday_check;
         write_output (g_location);
      END IF;

      p_oem_sunday := TRUNC (p_end_date);

      p_error_message := g_error_message;
      g_location :=
            'ending get_current_period  returning value for current period name '
         || p_current_period_name
         || ' and future period name to open '
         || p_future_period_name;

      write_output (g_location);
   --ver 1.2 changes ends
   EXCEPTION
      WHEN G_EXCEPTION
      THEN
         g_location := g_error_message;
         write_output (g_location);
         write_error (g_location);
         p_error_message := g_error_message;
      WHEN OTHERS
      THEN
         g_location :=
            'ending in Main exception of get_current_period ' || SQLERRM;
         write_output (g_location);
         write_error (g_location);
         g_error_message :=
            'When others Main get_current_period Error: ' || SQLERRM;
         p_error_message := g_error_message;
   END get_current_period;

   /*************************************************************************
    *   Function : check_unprocessed_lines
    *
    *   PURPOSE:   This procedure is to check if there are any unprocessed lines
    *              in ra_interface_lines_all.
    *              --This procedure is to check if there are any unprocessed lines
    *                in ra_interface_lines_all.
    *   Parameter:
    *          IN
    *             None
    *          OUT
    *             p_retVal OUT varchar2
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
    *   1.0       6/1/2016     Neha Saini             TMS# 20150716-00173 AR month end process
    * ************************************************************************/


   PROCEDURE check_unprocessed_lines (p_retVal OUT VARCHAR2)
   IS
      l_orders_total           NUMBER;
      l_order_totals           NUMBER;
      l_orderstotalthreshold   NUMBER;
      l_ordertotalsthreshold   NUMBER;
   BEGIN
      g_error_message := NULL;
      g_location := 'starting check_unprocessed_lines';
      write_output (g_location);

      BEGIN
         g_location := 'getting orders total threshold';
         write_output (g_location);

         SELECT meaning
           INTO l_orderstotalthreshold
           FROM apps.fnd_lookup_values
          WHERE     lookup_type = 'XXWC_AR_MONTHEND_THRESHOLD'
                AND TRUNC (SYSDATE) BETWEEN TRUNC (START_DATE_ACTIVE)
                                        AND TRUNC (
                                               NVL (END_DATE_ACTIVE,
                                                    SYSDATE + 1))
                AND enabled_flag = 'Y'
                AND lookup_code = 'THRESHOLD';

         g_location :=
            'getting order totals threshold: ' || l_orderstotalthreshold;
         write_output (g_location);

         SELECT meaning
           INTO l_ordertotalsthreshold
           FROM apps.fnd_lookup_values
          WHERE     lookup_type = 'XXWC_AR_MONTHEND_THRESHOLD'
                AND TRUNC (SYSDATE) BETWEEN TRUNC (START_DATE_ACTIVE)
                                        AND TRUNC (
                                               NVL (END_DATE_ACTIVE,
                                                    SYSDATE + 1))
                AND enabled_flag = 'Y'
                AND lookup_code = 'ORDER_TOTAL';

         g_location :=
            'getting orders total threshold: ' || l_ordertotalsthreshold;
         write_output (g_location);
         p_retVal := 'SUCCESS';
      EXCEPTION
         WHEN OTHERS
         THEN
            g_error_message :=
               'When others while getting threshold value ' || SQLERRM;
            p_retVal := 'FAILURE';
            g_location :=
               'error while getting threshold value from lookups XXWC_AR_MONTHEND_THRESHOLD';
            write_output (g_location);
            write_error (g_location);
      END;

      BEGIN
         SELECT SUM (AMOUNT)
           INTO l_orders_total
           FROM apps.RA_INTERFACE_LINES_all
          WHERE org_id = 162;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_orders_total := 0;
      END;

      g_location :=
         'getting order totals for unprocessed lines ' || l_orders_total;
      write_output (g_location);

      BEGIN
         SELECT COUNT (1)
           INTO l_order_totals
           FROM (  SELECT INTERFACE_LINE_ATTRIBUTE1
                     FROM apps.RA_INTERFACE_LINES_all
                    WHERE ORG_ID = 162
                 GROUP BY INTERFACE_LINE_ATTRIBUTE1
                   HAVING SUM (AMOUNT) > l_ordertotalsthreshold);
      EXCEPTION
         WHEN OTHERS
         THEN
            l_order_totals := 0;
      END;

      g_location :=
            'getting no of orders who have totals > threshold '
         || l_order_totals;
      write_output (g_location);

      IF l_orders_total > l_orderstotalthreshold OR l_order_totals > 0
      THEN
         g_location := 'orders total/order totals  exceed threshold ';

         write_output (g_location);
         p_retVal := 'FAILURE';
      ELSE
         p_retVal := 'SUCCESS';
      END IF;

      g_location := 'ending check_unprocessed_lines ' || p_retVal;

      write_output (g_location);
   EXCEPTION
      WHEN OTHERS
      THEN
         g_location :=
            'ending in exception of check_unprocessed_lines ' || SQLERRM;
         write_output (g_location);
         write_error (g_location);
         g_error_message :=
            'When others Main check_unprocessed_lines Wrror: ' || SQLERRM;
         p_retVal := 'FAILURE';
   END check_unprocessed_lines;

   /*************************************************************************
   *   Procedure : get_snap_shot_mv
   *
   *   PURPOSE:   This procedure is to take snapshot for AR CUSTOMER BAL MV.
   *   Parameter:
   *          IN  NONE
   *          OUT   p_error_message
   VERSION:
   /**************************************************************************
   VERSION        DATE            AUTHOR                     Comments
   -------------------------------------------------------------------------------------
   ver 1.0     6/1/2016       Neha Saini             TMS# 20150716-00173 AR month end process
   ver 1.1     9/26/2016      Neha Saini             changed the MV name from which snapshot need to be taken
   * ************************************************************************/

   PROCEDURE get_snap_shot_mv (ERRBUF OUT VARCHAR2, RETCODE OUT NUMBER)
   IS
      l_count   NUMBER := 0;
   BEGIN
      g_error_message := NULL;
      ERRBUF := NULL;
      RETCODE := 0;

      g_location := 'starting get_snap_shot_mv ';
      write_output (g_location);

      BEGIN
         SELECT COUNT (1)
           INTO L_COUNT
           FROM DBA_OBJECTS
          WHERE UPPER (OBJECT_NAME) LIKE 'XXWC_AR_CUST_BAL_MV_MONTHLY'; --ver1.1
      EXCEPTION
         WHEN OTHERS
         THEN
            l_count := 0;
      END;

      g_location := 'l_count' || l_count;
      write_output (g_location);

      IF l_count <> 0
      THEN
         g_location := 'dropping records' || l_count;
         write_output (g_location);

         EXECUTE IMMEDIATE 'DROP table XXWC.XXWC_AR_CUST_BAL_MV_MONTHLY'; --ver1.1
      END IF;

      BEGIN
         SELECT COUNT (1)
           INTO L_COUNT
           FROM DBA_OBJECTS
          WHERE UPPER (OBJECT_NAME) LIKE 'XXWC_AR_CUST_BAL_MV_MONTHLY'; --ver1.1
      EXCEPTION
         WHEN OTHERS
         THEN
            l_count := 0;
      END;

      g_location := 'l_count' || l_count;
      write_output (g_location);

      IF l_count = 0
      THEN
         g_location := 'creating table' || l_count;
         write_output (g_location);

         EXECUTE IMMEDIATE
            'create table XXWC.XXWC_AR_CUST_BAL_MV_MONTHLY as  ( select * from  XXEIS.XXWC_AR_CUSTOMER_BALANCE_MV)'; --ver1.1
      ELSE
         g_location := 'dropping and  creating table in ELSE ' || l_count;
         write_output (g_location);

         EXECUTE IMMEDIATE 'DROP table XXWC.XXWC_AR_CUST_BAL_MV_MONTHLY'; --ver1.1



         EXECUTE IMMEDIATE
            'create table XXWC.XXWC_AR_CUST_BAL_MV_MONTHLY as  ( select * from  XXEIS.XXWC_AR_CUSTOMER_BALANCE_MV)'; --ver1.1
      END IF;

      COMMIT;
      g_location := 'completed get_snap_shot_mv successfully ';
      write_output (g_location);
   EXCEPTION
      WHEN OTHERS
      THEN
         g_location :=
               'error in exception while taking snapshot in get_snap_shot_mv -> '
            || g_conc_request_id
            || ' Error: '
            || SQLERRM;
         write_output (g_location);
         write_error (g_location);
         g_error_message := g_location;
         ERRBUF := g_error_message;
         RETCODE := 2;
   END;

   /*************************************************************************
  *   Procedure : unposted_items_report
  *
  *   PURPOSE:   This procedure is to submit unposted items report.
  *   Parameter:
  *          IN  period_start_date,period_end_date
  *          OUT  NONE
  *   REVISIONS:
  *   Ver        Date        Author                     Description
  *   ---------  ----------  ---------------         -------------------------
  *   1.0       6/1/2016     Neha Saini             TMS# 20150716-00173 AR month end process
  *   1.2       10/18/2016   Neha Saini             TMS# 20161018-00116 Added period name paramters 
  *                                                   in poen and close period and changed future period 
  *                                                   logic in get current period.Added headers for all procedures which are changed.
  * ************************************************************************/ 

   PROCEDURE unposted_items_report (period_start_date IN date,period_end_date IN date)--ver1.2 starts
   IS
      v_user_id             NUMBER;
      v_app_id              NUMBER;
      v_resp_id             NUMBER;
      v_resp_name           VARCHAR2 (100);
      v_request_id          NUMBER;
      v_app_name            VARCHAR2 (20);
      v_req_phase           VARCHAR2 (100);
      v_req_status          VARCHAR2 (100);
      v_req_dev_phase       VARCHAR2 (100);
      v_req_dev_status      VARCHAR2 (100);
      v_req_message         VARCHAR2 (100);
      v_req_return_status   BOOLEAN;
      l_ledger_id           NUMBER;
      l_access_set_id       NUMBER;
      l_currency_code       VARCHAR2 (5);
   BEGIN
      /*First we need to initialize the apps, this will automatically done when we submit the program from application directly*/
      g_location := 'start unposted report ';
      write_output (g_location);

      BEGIN
         SELECT user_id
           INTO v_user_id
           FROM fnd_user
          WHERE user_name = 'XXWC_INT_FINANCE';
      EXCEPTION
         WHEN OTHERS
         THEN
            v_user_id := NULL;
      END;

      g_location := 'get user id ' || v_user_id;
      write_output (g_location);

      BEGIN
         -- mo_global.init('AR');
         mo_global.set_policy_context ('S', 162);

         SELECT fa.application_id,
                fr.responsibility_id,
                fr.responsibility_name,
                fa.application_short_name
           INTO v_app_id,
                v_resp_id,
                v_resp_name,
                v_app_name
           FROM fnd_responsibility_vl fr, fnd_application fa
          WHERE     responsibility_name LIKE 'HDS Receivables Manager - WC'
                AND fr.application_id = fa.application_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            v_app_id := NULL;
            v_resp_id := NULL;
            v_resp_name := NULL;
            v_app_name := NULL;
      END;


      BEGIN
         SELECT g.currency_code
           INTO l_currency_code
           FROM gl_sets_of_books g, ar_system_parameters p
          WHERE g.set_of_books_id = p.set_of_books_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_currency_code := 'USD';
      END;

      -- Initializing apps
      fnd_global.apps_initialize (user_id        => v_user_id,
                                  resp_id        => v_resp_id,
                                  resp_appl_id   => v_app_id);


      l_access_set_id := apps.FND_PROFILE.VALUE ('GL_ACCESS_SET_ID');
      g_location := 'get application id ' || v_app_id;
      write_output (g_location);
      l_ledger_id :=
         apps.GL_ACCESS_SET_SECURITY_PKG.get_default_ledger_id (
            l_access_set_id,
            'R');
      g_location := 'got ledger id ' || l_ledger_id;
      write_output (g_location);
      --ver1.2 changes 

      g_location :=
            'period start date and period end date '
         || TO_CHAR (period_start_date, 'yyyy/mm/dd hh:mm:ss');
      write_output (g_location);
      g_location :=
            'period start date and period end date '
         || TO_CHAR (period_end_date, 'yyyy/mm/dd hh:mm:ss');
         
      write_output (g_location);
     --ver1.2 changes ends
      g_location := 'submitting request';
      write_output (g_location);

      FND_REQUEST.SET_ORG_ID (162);

      v_request_id :=
         fnd_request.submit_request (
            v_app_name, /* Short name of the application under which the program is registered in this it will be ONT since the Reserve Order program is registered under order management*/
            'ARXGER', -- Short name of the concurrent program needs to submitted
            NULL,         -- concurrent program description OPTIONAL parameter
            SYSDATE, /* Start time parameter which can be used to mention the actual start of the concurrent program which once again OPTIONAL*/
            FALSE, /* Sub request parameters if this program is need to be submitted from another running request then TRUE needs to passed, but default is FALSE and an OPTIONAL parameter*/
            argument1    => NVL (l_ledger_id, 2061),                  --ver1.1
            argument2    => '',
            argument3    => TO_CHAR (period_start_date, 'yyyy/mm/dd hh:mm:ss'),--ver1.2
            argument4    => TO_CHAR (period_end_date, 'yyyy/mm/dd hh:mm:ss'),--ver1.2
            argument5    => -999,
            argument6    => -999,
            argument7    => -999,
            argument8    => -999,
            argument9    => -999,
            argument10   => -999);



      COMMIT;

      g_location := 'Concurrent Program Req ID' || v_request_id;
      write_output (g_location);



      -- Concurrent Program Status
      IF v_request_id <> 0 AND v_request_id IS NOT NULL
      THEN
         LOOP
            v_req_return_status :=
               fnd_concurrent.wait_for_request (v_request_id, -- request ID for which results need to be known
                                                60, --interval parameter  time between checks default is 60 Seconds
                                                0, -- Max wait default is 0 , this is the maximum amount of time a program should wait for it to get completed
                                                v_req_phase,
                                                v_req_status,
                                                v_req_dev_phase,
                                                v_req_dev_status,
                                                v_req_message);
            /*Others are output parameters to get the phase and status of the submitted concurrent program*/
            EXIT WHEN    UPPER (v_req_phase) = 'COMPLETED'
                      OR UPPER (v_req_status) IN ('CANCELLED',
                                                  'ERROR',
                                                  'TERMINATED');
         END LOOP;
      END IF;

      g_location := 'Concurrent Program Status' || v_req_status;
      write_output (g_location);
   EXCEPTION
      WHEN OTHERS
      THEN
         g_location :=
               'Main Exception -> conc request ID->'
            || g_conc_request_id
            || ' Error: '
            || SQLERRM
            || DBMS_UTILITY.format_error_backtrace;
         write_output (g_location);
         write_error (g_location);
   END;
END XXWC_AR_MONTHEND_AUTO_PKG;
/
GRANT EXECUTE ON APPS.XXWC_AR_MONTHEND_AUTO_PKG TO interface_xxcus;