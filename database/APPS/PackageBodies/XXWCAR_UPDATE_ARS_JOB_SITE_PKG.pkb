CREATE OR REPLACE PACKAGE BODY APPS.xxwcar_update_ars_job_site_pkg
AS
   /********************************************************************************

   FILE NAME: XXWCAR_UPDATE_ARS_JOB_SITE_PKG

   PROGRAM TYPE: PL/SQL Package

   PURPOSE: WebADI package to upload Cusotmer Information

   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)         DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.1     02/10/2014 Maharajan Shunmugam  Initial version.TMS# 20131016-00418

   1.2	   08/10/2014 Veera C			    Modified for TMS #20141001-00166 


   ********************************************************************************/

   PROCEDURE update_ars_job (p_document_num             IN VARCHAR2,
                             p_rws_number               IN VARCHAR2,
                             p_cli_number               IN VARCHAR2,
                             p_credit_group             IN VARCHAR2,
                             p_date_entered             IN DATE,
                             p_cust_number              IN VARCHAR2,
                             p_cust_name                IN VARCHAR2,
                             p_cust_street              IN VARCHAR2,
                             p_cust_city                IN VARCHAR2,
                             p_cust_state               IN VARCHAR2,
                             p_cust_zip                 IN VARCHAR2,
                             p_cust_phone               IN VARCHAR2,
                             p_gtr_name                 IN VARCHAR2,
                             p_gtr_street               IN VARCHAR2,
                             p_gtr_city                 IN VARCHAR2,
                             p_gtr_state                IN VARCHAR2,
                             p_gtr_zip                  IN VARCHAR2,
                             p_gtr_phone                IN VARCHAR2,
                             p_own_name                 IN VARCHAR2,
                             p_own_street               IN VARCHAR2,
                             p_own_city                 IN VARCHAR2,
                             p_own_state                IN VARCHAR2,
                             p_own_zip                  IN VARCHAR2,
                             p_own_phone                IN VARCHAR2,
                             p_bank_name                IN VARCHAR2,
                             p_bank_street              IN VARCHAR2,
                             p_bank_city                IN VARCHAR2,
                             p_bank_state               IN VARCHAR2,
                             p_bank_zip                 IN VARCHAR2,
                             p_bank_phone               IN VARCHAR2,
                             p_notice_type              IN VARCHAR2,
                             p_bond_number              IN VARCHAR2,
                             p_site_number              IN VARCHAR2,
                             p_site_street              IN VARCHAR2,
                             p_site_city                IN VARCHAR2,
                             p_site_state               IN VARCHAR2,
                             p_site_zip                 IN VARCHAR2,
                             p_site_county              IN VARCHAR2,
                             p_site_project_name        IN VARCHAR2,
                             p_site_legal_description   IN VARCHAR2,
                             p_sup_material_date        IN VARCHAR2,
                             p_est_cost                 IN VARCHAR2,
                             p_completed_date           IN VARCHAR2,
                             p_parties_served_count     IN VARCHAR2,
                             p_an1_type                 IN VARCHAR2,
                             p_an1_name                 IN VARCHAR2,
                             p_an1_branch               IN VARCHAR2,
                             p_an1_street               IN VARCHAR2,
                             p_an1_city                 IN VARCHAR2,
                             p_an1_state                IN VARCHAR2,
                             p_an1_zip                  IN VARCHAR2,
                             p_an1_phone                IN VARCHAR2,
                             p_an2_type                 IN VARCHAR2,
                             p_an2_name                 IN VARCHAR2,
                             p_an2_branch               IN VARCHAR2,
                             p_an2_street               IN VARCHAR2,
                             p_an2_city                 IN VARCHAR2,
                             p_an2_state                IN VARCHAR2,
                             p_an2_zip                  IN VARCHAR2,
                             p_an2_phone                IN VARCHAR2,
                             p_an3_type                 IN VARCHAR2,
                             p_an3_name                 IN VARCHAR2,
                             p_an3_branch               IN VARCHAR2,
                             p_an3_street               IN VARCHAR2,
                             p_an3_city                 IN VARCHAR2,
                             p_an3_state                IN VARCHAR2,
                             p_an3_zip                  IN VARCHAR2,
                             p_an3_phone                IN VARCHAR2)
   IS
      l_error_message              VARCHAR2 (1000);
      l_err_message                VARCHAR2 (1000) := NULL;
      l_attribute_category         VARCHAR2 (150);
      l_party_id                   NUMBER;
      l_party_site                 NUMBER;
      l_cust_account_id            NUMBER;
      l_contact_role               VARCHAR2 (30);
      l_exception                  EXCEPTION;
      l_init_msg_list              VARCHAR2 (1000) := FND_API.G_TRUE;
      l_cust_acct_site_rec         HZ_CUST_ACCOUNT_SITE_V2PUB.cust_acct_site_rec_type;
      l_prof_amt_rec               HZ_CUSTOMER_PROFILE_V2PUB.cust_profile_amt_rec_type;
      l_return_status              VARCHAR2 (1000);
      l_msg_count                  NUMBER;
      l_msg_data                   VARCHAR2 (1000);
      l_site_use_id                NUMBER;
      l_cust_acct_site_id          NUMBER;
      l_object_version_number      NUMBER;
      l_cust_acct_profile_amt_id   NUMBER;
      l_obj_version                NUMBER;
      l_credit_amount              NUMBER;
      l_call_profile_api           VARCHAR2 (1) := 'N';
      l_call_site_api              VARCHAR2(1)  := 'N';
      l_duplicate VARCHAR2(1);
      l_interest_type              VARCHAR2(50);
      l_interest_rate 		   NUMBER;
      
      l_msgdata                    VARCHAR2(32000);
      l_msgname                    VARCHAR2(30);
      l_msgapp                     VARCHAR2(50);
      l_msgencoded                 VARCHAR2(32100);
      l_msgencodedlen              NUMBER(6);
      l_msgnameloc                 NUMBER(6);
      l_msgtextloc                 NUMBER(6);
	  l_org_id                     NUMBER:= mo_global.get_current_org_id;  --08/10/2014 Added by Veera for Canada OU Test

   BEGIN


      --------------------------------------------------------------------
      -- Initialize user, application and responsibility
      --------------------------------------------------------------------
      mo_global.init ('AR');
     -- mo_global.set_policy_context ('S', 162);    --08/10/2014 Commented by Veera for Canada OU Test
	  mo_global.set_policy_context ('S', l_org_id);  --08/10/2014 Added by Veera for Canada OU Test
	 

    BEGIN
     SELECT 'Y'
       INTO l_duplicate
     FROM XXWC.XXWC_ARS_JOB_GLOB_TEMP
      WHERE document_number = p_document_num;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
    l_duplicate := 'N';
    WHEN OTHERS THEN
    l_duplicate := 'Y';
    END;

    IF l_duplicate = 'Y' THEN
      l_error_message := ' ~ Duplicate document number found. Please correct data manually';
     RAISE l_exception;
    ELSE
    INSERT INTO XXWC.XXWC_ARS_JOB_GLOB_TEMP
     VALUES (p_document_num,p_cust_number);
   END IF;   
      --------------------------------------------------------------------
      -- Check if account number is valid
      --------------------------------------------------------------------
      BEGIN
         SELECT party_id,
                cust_account_id
           INTO l_party_id,
                l_cust_account_id
           FROM AR.hz_cust_accounts
          WHERE account_number =  p_cust_number 
            AND status = 'A';
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_error_message := ' ~ Account number is not valid';
            DBMS_OUTPUT.PUT_LINE (
               'Account number is invalid :' || p_site_number);
            fnd_file.put_line (fnd_file.LOG,
                               'Account number is invalid :' || p_site_number);
            RAISE l_exception;
         WHEN OTHERS
         THEN
            l_error_message :=
                  ' ~ Unidentified exception - validating Account number '
               || SQLERRM;
            DBMS_OUTPUT.PUT_LINE (
               'Account number is invalid :' || p_site_number || SQLERRM);
            fnd_file.put_line (
               fnd_file.LOG,
               'Account number is invalid :' || p_site_number || SQLERRM);
           RAISE l_exception;
      END;


      --------------------------------------------------------------------
      -- Check if acount and site number is valid
      --------------------------------------------------------------------
      BEGIN
         SELECT party_site_id
           INTO l_party_site
           FROM AR.hz_party_sites
          WHERE party_site_number = p_site_number
            AND party_id = l_party_id 
            AND status = 'A';
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_error_message := ' ~ Site number is not valid or not related to account number';
            DBMS_OUTPUT.PUT_LINE (
               'Site number is invalid :' || p_site_number);
            fnd_file.put_line (fnd_file.LOG,
                               'Site number is not valid or not related to account number :' || p_site_number);
            RAISE l_exception;
         WHEN OTHERS
         THEN
            l_error_message :=
                  ' ~ Unidentified exception - Site number is not valid or not related to account number '
               || SQLERRM;
            DBMS_OUTPUT.PUT_LINE (
               'Site number is not valid or not related to account number:' || p_site_number || SQLERRM);
            fnd_file.put_line (
               fnd_file.LOG,
               'Site number is not valid or not related to account number :' || p_site_number || SQLERRM);
           RAISE l_exception;
      END;

      --------------------------------------------------------------------
      -- Get cust acct site id and object version number
      --------------------------------------------------------------------
      BEGIN
         SELECT cust_acct_site_id, 
                object_version_number
           INTO l_cust_acct_site_id, 
                l_object_version_number
           FROM apps.hz_cust_acct_sites
          WHERE party_site_id = l_party_site 
            AND cust_account_id = l_cust_account_id
            AND status = 'A';
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_error_message := ' ~ Account site is not valid';
            DBMS_OUTPUT.PUT_LINE (
               'Site number is invalid :' || p_site_number);
            fnd_file.put_line (fnd_file.LOG,
                               'Account site is not valid :' || p_site_number);
            RAISE l_exception;
         WHEN OTHERS
         THEN
            l_error_message :=
                  ' ~ Unidentified exception - validating site number '
               || SQLERRM;
            DBMS_OUTPUT.PUT_LINE (
               'Account site is not valid:' || p_site_number || SQLERRM);
            fnd_file.put_line (
               fnd_file.LOG,
               'Account site is not valid :' || p_site_number || SQLERRM);
           RAISE l_exception;
      END;

      DBMS_OUTPUT.PUT_LINE ('Cust acct site id' || l_cust_acct_site_id);
      fnd_file.put_line (
         fnd_file.LOG,
         'Getting cust acct site id :' || l_cust_acct_site_id);

      --------------------------------------------------------------------
      -- Get cust acct profile amount id and obj version number
      --------------------------------------------------------------------

      BEGIN
         SELECT hcpa.cust_acct_profile_amt_id,
                hcpa.object_version_number,
                hcpa.overall_credit_limit,
                hcpa.interest_type,
                hcpa.interest_rate
           INTO l_cust_acct_profile_amt_id, 
                l_obj_version, 
                l_credit_amount,
                l_interest_type,
                l_interest_rate
           FROM hz_cust_profile_amts hcpa, 
                apps.hz_cust_site_uses hcsu
          WHERE hcpa.cust_account_id = l_cust_account_id
            AND hcsu.cust_acct_site_id = l_cust_acct_site_id
            AND hcpa.site_use_id = hcsu.site_use_id;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_error_message := ' ~ Error getting customer profile amount details. Please check site and site use';
            RAISE l_exception;
         WHEN OTHERS
         THEN
            l_error_message :=
                  ' ~ Unidentified exception - validating account site profile '
               || SQLERRM;
            RAISE l_exception;
      END;

      DBMS_OUTPUT.PUT_LINE ('Profile amnt id' || l_cust_acct_profile_amt_id);
      fnd_file.put_line (
         fnd_file.LOG,
         'Getting cust acct profile amnt id :' || l_cust_acct_profile_amt_id);
      DBMS_OUTPUT.PUT_LINE ('Profile amnt ' || l_credit_amount);
      fnd_file.put_line (fnd_file.LOG,
                         'Getting profile amount :' || l_credit_amount);


      DBMS_OUTPUT.PUT_LINE ('Notice Type ' || p_notice_type);
      fnd_file.put_line (fnd_file.LOG, 'Notice Type ' || p_notice_type);


      ----------------------------------------------------------------
      -- If notice type is NULL
      ----------------------------------------------------------------

      IF p_notice_type IS NULL
      THEN
         l_cust_acct_site_rec.cust_acct_site_id     := l_cust_acct_site_id;
         l_cust_acct_site_rec.attribute_category    := 'Yes';
         l_cust_acct_site_rec.attribute14           := 'Y';
         l_cust_acct_site_rec.attribute19           := p_rws_number;
         l_cust_acct_site_rec.attribute18           := p_est_cost;
         l_cust_acct_site_rec.attribute20           := TO_CHAR(TO_DATE(p_completed_date,'MM/DD/YYYY'),'YYYY/MM/DD HH24:MI:SS');
         l_call_profile_api                         := 'Y';
         l_call_site_api                            := 'Y';

      ----------------------------------------------------------------
      -- If notice type is RS_ONLY
      ----------------------------------------------------------------

      ELSIF p_notice_type = 'RS_ONLY'
      THEN
         l_cust_acct_site_rec.cust_acct_site_id      := l_cust_acct_site_id;
         --l_cust_acct_site_rec.attribute_category     := 'Yes';
         l_cust_acct_site_rec.attribute14            := 'Y';
         l_call_profile_api                          := 'Y';
         l_call_site_api                             := 'Y';

      --------------------------------------------------------------------
      -- If notice type is UPGRADE
      --------------------------------------------------------------------

      ELSIF p_notice_type = 'UPGRADE'
      THEN
         l_call_profile_api := 'Y';    
         l_return_status    := 'S'    ;
      END IF;

        --------------------------------------------------------------------
      --Set owner financed as Yes if name is OWNER FINANCED
      --------------------------------------------------------------------

       IF UPPER(p_bank_name) LIKE '%OWNER%FINANCED%' OR
          UPPER(p_an1_name)  LIKE '%OWNER%FINANCED%' OR
          UPPER(p_an2_name)  LIKE '%OWNER%FINANCED%' OR
          UPPER(p_an3_name)  LIKE '%OWNER%FINANCED%' THEN  
          
       l_call_site_api                             := 'Y'; 
       l_cust_acct_site_rec.cust_acct_site_id      := l_cust_acct_site_id;         
       l_cust_acct_site_rec.attribute13         := 'Y';
       
       END IF;

      IF l_call_site_api = 'Y' THEN 
      --------------------------------------------------------------------
      -- API Call to update Customer site rec
      --------------------------------------------------------------------
      hz_cust_account_site_v2pub.update_cust_acct_site (
         p_init_msg_list           => FND_API.G_TRUE,
         p_cust_acct_site_rec      => l_cust_acct_site_rec,
         p_object_version_number   => l_object_version_number,
         x_return_status           => l_return_status,
         x_msg_count               => l_msg_count,
         x_msg_data                => l_msg_data);
      --------------------------------------------------------------------
      -- Validate the status of API output
      --------------------------------------------------------------------

      DBMS_OUTPUT.PUT_LINE ('Cust acct site API status: ' || l_return_status);
      fnd_file.put_line (fnd_file.LOG,
                         'Cust acct site API status: ' || l_return_status);
      DBMS_OUTPUT.PUT_LINE ('Cust acct site API status: ' || l_msg_data);
      fnd_file.put_line (fnd_file.LOG,
                         'Cust acct site API status: ' || l_msg_data);
                         
      IF l_return_status = 'E'
      THEN
      ROLLBACK;
      l_error_message := 'Update acct site API Failure: '||l_msg_data;
      RAISE l_exception;
      ELSE
      COMMIT;
      END IF;

      IF l_msg_count > 1
      THEN
         FOR I IN 1 .. l_msg_count
         LOOP
            fnd_file.put_line (
               fnd_file.LOG,
               SUBSTR (FND_MSG_PUB.Get (p_encoded => FND_API.G_FALSE), 1, 255));
         END LOOP;
      END IF;
  
    END IF;

      IF (p_credit_group = 'C' OR p_credit_group = 'E')
      THEN
         IF l_credit_amount >= 50000
         THEN
            l_credit_amount := l_credit_amount;
         ELSIF l_credit_amount < 50000
         THEN
            l_credit_amount := 50000;
         END IF;
      ELSIF p_credit_group = 'N'
      THEN
         l_credit_amount := p_est_cost;
      END IF;


      DBMS_OUTPUT.PUT_LINE ('Credit Group: ' || p_credit_group);
      fnd_file.put_line (fnd_file.LOG, 'Credit Group: ' || p_credit_group);

      IF l_return_status = 'S'
      THEN
         IF     (   p_credit_group = 'C'
                 OR p_credit_group = 'E'
                 OR p_credit_group = 'N')
            AND l_call_profile_api = 'Y'
         THEN
            l_prof_amt_rec.cust_acct_profile_amt_id     :=  l_cust_acct_profile_amt_id;
            l_prof_amt_rec.OVERALL_CREDIT_LIMIT         :=  l_credit_amount;

            IF ( l_interest_type = 'FIXED_RATE' AND l_interest_rate IS NULL) THEN
            l_prof_amt_rec.interest_rate                :=  1.5; 
            END IF;

            --------------------------------------------------------------------
            -- API Call to update CustomerProfileAmount
            --------------------------------------------------------------------

            HZ_CUSTOMER_PROFILE_V2PUB.update_cust_profile_amt (
               p_init_msg_list           => FND_API.G_TRUE,
               p_cust_profile_amt_rec    => l_prof_amt_rec,
               p_object_version_number   => l_obj_version,
               x_return_status           => l_return_status,
               x_msg_count               => l_msg_count,
               x_msg_data                => l_msg_data);
               
               COMMIT;

      IF l_return_status = 'E'
      THEN
      l_error_message := 'Update profile amount API failure: '||l_msg_data;
      RAISE l_exception;
      END IF;
            --------------------------------------------------------------------
            -- Validate the status of API output
            --------------------------------------------------------------------

            IF l_msg_count > 1
            THEN
               FOR I IN 1 .. l_msg_count
               LOOP
                  fnd_file.put_line (
                     fnd_file.LOG,
                     SUBSTR (FND_MSG_PUB.Get (p_encoded => FND_API.G_FALSE),
                             1,
                             255));
               END LOOP;
            END IF;
         END IF;


      --------------------------------------------------------------------
      -- Calling Create contact procedure for GTR
      --------------------------------------------------------------------

         DBMS_OUTPUT.PUT_LINE ('Calling Create contact');

         IF p_gtr_name IS NOT NULL
         THEN
            l_err_message := NULL;
            fnd_file.put_line (fnd_file.LOG,
                               'Calling Create contact for GTR ');
            create_contact (p_cust_number,
                            l_party_site,
                            p_gtr_phone,
                            'GENERAL',
                            p_gtr_name,
                            p_bond_number,
                            p_gtr_street,
                            p_gtr_city,
                            p_gtr_state,
                            p_gtr_zip,
                            l_err_message);
            DBMS_OUTPUT.PUT_LINE ('Error :' || l_err_message);
            fnd_file.put_line (fnd_file.LOG,
                               'Error Message: ' || l_err_message);
           IF l_err_message IS NOT NULL THEN
            l_error_message := 'FOR GTR Name: '||l_err_message; 
            RAISE l_exception;
           END IF;
           
         END IF;

    --------------------------------------------------------------------
      -- Calling Create contact procedure for Owner
    --------------------------------------------------------------------
         IF p_own_name IS NOT NULL
         THEN
            l_err_message := NULL;
            fnd_file.put_line (fnd_file.LOG,
                               'Calling Create contact for OWN ');
            create_contact (p_cust_number,
                            l_party_site,
                            p_own_phone,
                            'OWNER',
                            p_own_name,
                            p_bond_number,
                            p_own_street,
                             p_own_city ,
                             p_own_state,
                             p_own_zip ,
                            l_err_message);
            DBMS_OUTPUT.PUT_LINE ('Error :' || l_err_message);
            fnd_file.put_line (fnd_file.LOG,
                               'Error Message: ' || l_err_message);
         
          IF l_err_message IS NOT NULL THEN
            l_error_message := 'For OWN Name: '||l_err_message; 
            RAISE l_exception;
           END IF;                      
                               
         END IF;

    --------------------------------------------------------------------
      -- Calling Create contact procedure for bank
    --------------------------------------------------------------------

    IF p_bank_name IS NOT NULL 
    AND 
    (UPPER(p_bank_name) NOT LIKE '%OWNER%FINANCED%' AND UPPER(p_bank_name) NOT LIKE '%UNABLE%TO%DETERMINE%' AND UPPER(p_bank_name) NOT LIKE '%BONDING%INFORMATION%' 
    AND UPPER(p_bank_name) NOT LIKE '%*%')
    THEN
            l_err_message := null;
            fnd_file.put_line (fnd_file.LOG,
                               'Calling Create contact for BANK ');
            create_contact (p_cust_number,
                            l_party_site,
                            p_bank_phone,
                            'FINANCIAL',
                             p_bank_name,
                             p_bond_number,
                             p_bank_street,
                             p_bank_city ,
                             p_bank_state,
                             p_bank_zip ,
                            l_err_message);
            DBMS_OUTPUT.PUT_LINE ('Error :' || l_err_message);
            fnd_file.put_line (fnd_file.LOG,
                               'Error Message: ' || l_err_message);
          IF l_err_message IS NOT NULL THEN
            l_error_message := 'For Bank Name : '||p_bank_name||'-'||l_err_message; 
            RAISE l_exception;
           END IF;
         END IF;

    --------------------------------------------------------------------
      -- Calling Create contact procedure for AN1
    --------------------------------------------------------------------

         IF (p_an1_name IS NOT NULL AND UPPER(p_an1_name) NOT LIKE '%OWNER%FINANCED%' AND UPPER(p_an1_name) NOT LIKE '%*%' )
         THEN
            BEGIN
               SELECT tag
                 INTO l_contact_role
                 FROM fnd_lookup_values
                WHERE     lookup_type = 'XXCUS_ARS_CONTACT_ROLE'
                      AND UPPER (meaning) = UPPER (p_an1_type)
                      AND enabled_flag = 'Y';
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_contact_role := NULL;
                   l_error_message :=
                  ' ~ Unidentified exception - Getting contact role from XXCUS_ARS_CONTACT_ROLE lookup'
               || SQLERRM;
                  RAISE l_exception;
            END;
            
            
            fnd_file.put_line (fnd_file.LOG,
                               'Calling Create contact for AN1 ');
            l_err_message := NULL;
            create_contact (p_cust_number,
                            l_party_site,
                            p_an1_phone,
                            l_contact_role,
                             p_an1_name,
                             p_an1_branch,
                             p_an1_street,
                             p_an1_city,
                             p_an1_state,
                             p_an1_zip ,
                            l_err_message);
            DBMS_OUTPUT.PUT_LINE ('Error :' || l_err_message);
            fnd_file.put_line (fnd_file.LOG,
                               'Error Message: ' || l_err_message);
            IF l_err_message IS NOT NULL THEN
            l_error_message := 'For AN1 : '||l_err_message; 
            RAISE l_exception;
           END IF;
         END IF;

    --------------------------------------------------------------------
      -- Calling Create contact procedure for AN2
    --------------------------------------------------------------------
         IF (p_an2_name IS NOT NULL AND UPPER(p_an2_name) NOT LIKE '%OWNER%FINANCED%' AND UPPER(p_an2_name) NOT LIKE '%*%' )
         THEN
            BEGIN
               SELECT tag
                 INTO l_contact_role
                 FROM fnd_lookup_values
                WHERE     lookup_type = 'XXCUS_ARS_CONTACT_ROLE'
                      AND UPPER (meaning) = UPPER (p_an2_type)
                      AND enabled_flag = 'Y';
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_contact_role := NULL;
                                     l_error_message :=
                  ' ~ Unidentified exception - Getting contact role from XXCUS_ARS_CONTACT_ROLE lookup'
               || SQLERRM;
                  RAISE l_exception;

            END;

            fnd_file.put_line (fnd_file.LOG,
                               'Calling Create contact for AN2 ');
             l_err_message := NULL;
            create_contact (p_cust_number,
                            l_party_site,
                            p_an2_phone,
                            l_contact_role,
                            p_an2_name,
                            p_an2_branch,
                             p_an2_street,
                             p_an2_city,
                             p_an2_state,
                             p_an2_zip ,
                            l_err_message);
            DBMS_OUTPUT.PUT_LINE ('Error :' || l_err_message);
            fnd_file.put_line (fnd_file.LOG,
                               'Error Message: ' || l_err_message);
                               
            IF l_err_message IS NOT NULL THEN
            l_error_message := 'For AN2: '||l_err_message; 
            RAISE l_exception;
           END IF;
         END IF;

    --------------------------------------------------------------------
      -- Calling Create contact procedure for AN3
    --------------------------------------------------------------------

         IF (p_an3_name IS NOT NULL AND UPPER(p_an3_name) NOT LIKE '%OWNER%FINANCED%' AND UPPER(p_an3_name) NOT LIKE '%*%')
         THEN
            BEGIN
               SELECT tag
                 INTO l_contact_role
                 FROM fnd_lookup_values
                WHERE     lookup_type = 'XXCUS_ARS_CONTACT_ROLE'
                      AND UPPER (meaning) = UPPER (p_an3_type)
                      AND enabled_flag = 'Y';
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_contact_role := NULL;
                                     l_error_message :=
                  ' ~ Unidentified exception - Getting contact role from XXCUS_ARS_CONTACT_ROLE lookup'
               || SQLERRM;
                  RAISE l_exception;
            END;

            fnd_file.put_line (fnd_file.LOG,
                               'Calling Create contact for AN3 ');
            l_err_message := NULL;
            create_contact (p_cust_number,
                            l_party_site,
                            p_an3_phone,
                            l_contact_role,
                            p_an3_name,
                            p_an3_branch,
                             p_an3_street,
                             p_an3_city,
                             p_an3_state,
                             p_an3_zip ,
                            l_err_message);
            DBMS_OUTPUT.PUT_LINE ('Error :' || l_err_message);
            fnd_file.put_line (fnd_file.LOG,
                               'Error Message: ' || l_err_message);
                               
            IF l_err_message IS NOT NULL THEN
            l_error_message := 'For AN3 :'||l_err_message; 
            RAISE l_exception;
           END IF;
           
         END IF;
      END IF;
      
      
        l_msgencoded    := fnd_message.get_encoded();
        l_msgencodedlen := LENGTH(l_msgencoded);
        l_msgnameloc    := INSTR(l_msgencoded, chr(0));
        l_msgapp        := SUBSTR(l_msgencoded, 1, l_msgnameloc-1);
        l_msgencoded    := SUBSTR(l_msgencoded, l_msgnameloc+1, l_msgencodedlen);      
        l_msgencodedlen := LENGTH(l_msgencoded);
        l_msgtextloc    := INSTR(l_msgencoded, chr(0));
        l_msgname       := SUBSTR(l_msgencoded, 1, l_msgtextloc-1);

    IF (l_msgname <> 'CONC-SINGLE PENDING REQUEST') THEN
        fnd_message.set_name(l_msgapp, l_msgname);
    END IF;
 
  EXCEPTION
  WHEN l_exception  THEN
       FND_MESSAGE.SET_NAME('XXWC','XXWC_ADI_ERROR_MSG');
       FND_MESSAGE.SET_TOKEN('ERROR_MESSAGE',l_error_message);      
  WHEN OTHERS THEN
       ROLLBACK;
       l_error_message := 'Unidentified error - '||SQLERRM;
       FND_MESSAGE.SET_NAME('XXWC','XXWC_ADI_ERROR_MSG');
       FND_MESSAGE.SET_TOKEN('ERROR_MESSAGE',l_error_message);
      
   END update_ars_job;

   /********************************************************************************

   NAME: CREATE_CONTACT

   PROGRAM TYPE: PL/SQL Procedure

   PURPOSE: Procedure to create customer contact

   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)         DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.1     02/10/2014 Maharajan Shunmugam  Initial version.TMS# 20131016-00418


   ********************************************************************************/


   PROCEDURE create_contact (p_acc_number      IN     VARCHAR2,
                             p_party_site_id   IN     NUMBER,
                             p_contact_num     IN     VARCHAR2,
                             p_resp_role       IN     VARCHAR2,
                             p_contact_name    IN     VARCHAR2,
                             p_middle_name     IN     VARCHAR2,
                             p_cont_street     IN     VARCHAR2,
                             p_cont_city       IN     VARCHAR2,
                             p_cont_state      IN     VARCHAR2,
                             p_cont_zip        IN     VARCHAR2,  
                             p_error_message      OUT VARCHAR2)
   IS
   -- X_ variable is to differntiate the OUT parameter from API
      l_party_id               NUMBER;
      l_customer_id            NUMBER;
      l_create_person_rec      HZ_PARTY_V2PUB.person_rec_type;
      x_party_id               NUMBER;
      x_party_number           VARCHAR2 (2000);
      x_profile_id             NUMBER;
      x_return_status          VARCHAR2 (2000);
      x_msg_count              NUMBER;
      x_msg_data               VARCHAR2 (2000);

      l_org_contact_rec        HZ_PARTY_CONTACT_V2PUB.ORG_CONTACT_REC_TYPE;
      x_org_contact_id         NUMBER;
      x_party_rel_id           NUMBER;
      x_party_id2              NUMBER;
      x_party_number2          VARCHAR2 (2000);
      l_cust_acc_site_id       NUMBER;

      l_cr_cust_acc_role_rec   HZ_CUST_ACCOUNT_ROLE_V2PUB.cust_account_role_rec_type;
      x_cust_account_role_id   NUMBER;
      l_exception              EXCEPTION;

      l_contact_point_rec      HZ_CONTACT_POINT_V2PUB.CONTACT_POINT_REC_TYPE;

      l_edi_rec                HZ_CONTACT_POINT_V2PUB.EDI_REC_TYPE;
      l_email_rec              HZ_CONTACT_POINT_V2PUB.EMAIL_REC_TYPE;
      l_phone_rec              HZ_CONTACT_POINT_V2PUB.PHONE_REC_TYPE;
      l_telex_rec              HZ_CONTACT_POINT_V2PUB.TELEX_REC_TYPE;
      l_web_rec                HZ_CONTACT_POINT_V2PUB.WEB_REC_TYPE;

      x_contact_point_id       NUMBER;


      --For Contact role
      l_rec                    hz_cust_account_role_v2pub.role_responsibility_rec_type;
      x_responsibility_id      NUMBER;
      l_responsibility_type    VARCHAR2 (100);



      l_location_rec           HZ_LOCATION_V2PUB.LOCATION_REC_TYPE;
      l_location_id            NUMBER;

      l_party_site_rec         HZ_PARTY_SITE_V2PUB.PARTY_SITE_REC_TYPE;
      l_party_site_id          NUMBER;
      l_party_site_number      VARCHAR2 (2000);
	  l_org_id                 NUMBER:= mo_global.get_current_org_id;  --08/10/2014 Added by Veera for Canada OU Test

   BEGIN
   
    mo_global.init ('AR');
     -- mo_global.set_policy_context ('S', 162);    --08/10/2014 Commented by Veera for Canada OU Test
	  mo_global.set_policy_context ('S', l_org_id);  --08/10/2014 Added by Veera for Canada OU Test
   
      ------------------------------------
      -- Derive Customer Party Id - Step 0
      ------------------------------------
      SELECT party_id, 
             cust_account_id
        INTO l_party_id, 
             l_customer_id
        FROM apps.hz_cust_accounts
       WHERE account_number = p_acc_number;

      ------------------------------------
      -- 1. Create a GTR contact - Step 1
      ------------------------------------
      l_create_person_rec.created_by_module := 'TCA_V1_API';
      l_create_person_rec.person_first_name := p_contact_name;
      
      IF p_resp_role = 'BONDING' THEN
        l_create_person_rec.person_middle_name := p_middle_name;
      END IF;

      HZ_PARTY_V2PUB.create_person ('T',
                                    l_create_person_rec,
                                    x_party_id,
                                    x_party_number,
                                    x_profile_id,
                                    x_return_status,
                                    x_msg_count,
                                    x_msg_data);
      COMMIT;

      IF x_return_status != 'S'
      THEN
         p_error_message := 'Error Creating Person: '||x_msg_data;
         RAISE l_exception;
      END IF;

      DBMS_OUTPUT.PUT_LINE ('Party id  Created :' || x_party_id);

      -------------------------------------------------------------
      -- 2. Create a relation cont-org using party_id from step 1
      --------------------------------------------------------------
      l_org_contact_rec.created_by_module := 'TCA_V1_API';
      l_org_contact_rec.party_rel_rec.subject_id := x_party_id; --<<value for party_id from step 1>
      l_org_contact_rec.party_rel_rec.subject_type := 'PERSON';
      l_org_contact_rec.party_rel_rec.subject_table_name := 'HZ_PARTIES';
      l_org_contact_rec.party_rel_rec.object_id := l_party_id; --<<value for party_id from step 0>
      l_org_contact_rec.party_site_id := p_party_site_id;
      l_org_contact_rec.party_rel_rec.object_type := 'ORGANIZATION';
      l_org_contact_rec.party_rel_rec.object_table_name := 'HZ_PARTIES';
      l_org_contact_rec.party_rel_rec.relationship_code := 'CONTACT_OF';
      l_org_contact_rec.party_rel_rec.relationship_type := 'CONTACT';
      l_org_contact_rec.party_rel_rec.start_date := SYSDATE;
      l_org_contact_rec.party_rel_rec.status := 'A';
      --         p_org_contact_rec.contact_number                   := p_contact_num;

      hz_party_contact_v2pub.create_org_contact ('T',
                                                 l_org_contact_rec,
                                                 x_org_contact_id,
                                                 x_party_rel_id,
                                                 x_party_id2,
                                                 x_party_number,
                                                 x_return_status,
                                                 x_msg_count,
                                                 x_msg_data);
      COMMIT;

      IF x_return_status != 'S'
      THEN
         p_error_message := 'Error creating Org contact: '||x_msg_data;
         RAISE l_exception;
      END IF;

      DBMS_OUTPUT.PUT_LINE ('Party relation  Created :' || x_party_id2);

      ------------------------------------------------------------------------------------------------------------
      -- 3. Create a location for contact
      ------------------------------------------------------------------------------------------------------------
      BEGIN
         l_location_rec.country     := 'US';
         l_location_rec.address1    := p_cont_street;
         l_location_rec.address2    := NULL;
         l_location_rec.city        := p_cont_city;
         l_location_rec.postal_code := p_cont_zip;
         l_location_rec.state       := p_cont_state;
         l_location_rec.created_by_module := 'TCA_V1_API';

         HZ_LOCATION_V2PUB.CREATE_LOCATION (
            p_init_msg_list   => FND_API.G_TRUE,
            p_location_rec    => l_location_rec,
            x_location_id     => l_location_id,
            x_return_status   => x_return_status,
            x_msg_count       => x_msg_count,
            x_msg_data        => x_msg_data);
            
            COMMIT;

         IF x_return_status != 'S'
         THEN          
            p_error_message := 'Error creating location: '||x_msg_data;
            RAISE l_exception;
         END IF;
      END;

      ------------------------------------------------------------------------------------------------------------
      -- 4. Assign location to the contact
      ------------------------------------------------------------------------------------------------------------
      BEGIN
         l_party_site_rec.party_id := x_party_id2;
         l_party_site_rec.location_id := l_location_id;
         l_party_site_rec.identifying_address_flag := 'Y';
         l_party_site_rec.created_by_module := 'TCA_V1_API';

         HZ_PARTY_SITE_V2PUB.CREATE_PARTY_SITE (
            p_init_msg_list       => FND_API.G_TRUE,
            p_party_site_rec      => l_party_site_rec,
            x_party_site_id       => l_party_site_id,
            x_party_site_number   => l_party_site_number,
            x_return_status       => x_return_status,
            x_msg_count           => x_msg_count,
            x_msg_data            => x_msg_data);

COMMIT;

         IF x_return_status = 'E'
         THEN
            p_error_message := 'Error creating party site: '||'Party_id--'||x_party_id2||'location_id--'||l_location_id||
             'party site is--'||l_party_site_id||'Party site number--'||l_party_site_number||x_msg_data;
            RAISE l_exception;
         END IF;
      END;


      ------------------------------------------------------------------------------------------------------------
      -- 5. Create a Phone Contact Point using party_id
      ------------------------------------------------------------------------------------------------------------
      
      IF p_contact_num IS NOT NULL 
      THEN
      -- Initializing the Mandatory API parameters
      l_contact_point_rec.contact_point_type := 'PHONE';
      l_contact_point_rec.owner_table_name := 'HZ_PARTIES';
      l_contact_point_rec.owner_table_id := x_party_id2;         --l_party_id;
      l_contact_point_rec.primary_flag := 'Y';
      l_contact_point_rec.contact_point_purpose := 'BUSINESS';
      l_contact_point_rec.created_by_module := 'BO_API';

      IF p_contact_num IS NOT NULL THEN
      l_phone_rec.phone_area_code := SUBSTR(p_contact_num,1,3);
      l_phone_rec.phone_country_code := '1';
      l_phone_rec.phone_number := SUBSTR(p_contact_num,4);
      l_phone_rec.phone_line_type := 'MOBILE';    
      END IF;

      DBMS_OUTPUT.PUT_LINE (
         'Calling the API hz_contact_point_v2pub.create_contact_point');

      HZ_CONTACT_POINT_V2PUB.CREATE_CONTACT_POINT (
         p_init_msg_list       => FND_API.G_TRUE,
         p_contact_point_rec   => l_contact_point_rec,
         p_edi_rec             => l_edi_rec,
         p_email_rec           => l_email_rec,
         p_phone_rec           => l_phone_rec,
         p_telex_rec           => l_telex_rec,
         p_web_rec             => l_web_rec,
         x_contact_point_id    => x_contact_point_id,
         x_return_status       => x_return_status,
         x_msg_count           => x_msg_count,
         x_msg_data            => x_msg_data);

COMMIT;

      IF x_return_status != 'S'
      THEN
         p_error_message := 'Status: '||x_return_status||' Error creating contact point: '||x_msg_data;
         RAISE l_exception;
      END IF;
   END IF;

      DBMS_OUTPUT.PUT_LINE ('Contact point created :' || x_contact_point_id);


      SELECT cust_acct_site_id
        INTO l_cust_acc_site_id
        FROM apps.hz_cust_acct_sites
       WHERE party_site_id = p_party_site_id;

      ------------------------------------------------------------------------------------------------------------
      -- 6. Create a contact using party_id you get in step 3
      ------------------------------------------------------------------------------------------------------------
     l_cr_cust_acc_role_rec.party_id := x_party_id2; --<<value for party_id from step 2>
     l_cr_cust_acc_role_rec.cust_account_id := l_customer_id; --<<value for cust_account_id from step 0>
      --p_cr_cust_acc_role_rec.primary_flag    := 'Y';
      l_cr_cust_acc_role_rec.role_type := 'CONTACT';
      l_cr_cust_acc_role_rec.created_by_module := 'TCA_V1_API';
      l_cr_cust_acc_role_rec.cust_acct_site_id := l_cust_acc_site_id; --<< Pass cust acct site id to create contact at site level >>

      HZ_CUST_ACCOUNT_ROLE_V2PUB.CREATE_CUST_ACCOUNT_ROLE (
         'T',
         l_cr_cust_acc_role_rec,
         x_cust_account_role_id,
         x_return_status,
         x_msg_count,
         x_msg_data);
         
         COMMIT;

      IF x_return_status != 'S'
      THEN
         p_error_message := 'Error creating cust account role: '||x_msg_data;
         RAISE l_exception;
      END IF;


      ------------------------------------------------------------------------------------------------------------
      -- 7. Create a role responsibility
      ------------------------------------------------------------------------------------------------------------


      l_rec.cust_account_role_id := x_cust_account_role_id;
      l_rec.responsibility_type := p_resp_role;       --l_responsibility_type;
      l_rec.created_by_module := 'TCA_V1_API';


      hz_cust_account_role_v2pub.create_role_responsibility (
         p_init_msg_list             => FND_API.G_TRUE,
         p_role_responsibility_rec   => l_rec,
         x_responsibility_id         => x_responsibility_id,
         x_return_status             => x_return_status,
         x_msg_count                 => x_msg_count,
         x_msg_data                  => x_msg_data);
         
         COMMIT;

      IF x_return_status != 'S'
      THEN
         p_error_message := 'Error creating Role Responsibility: '||x_msg_data;
         DBMS_OUTPUT.PUT_LINE ('Error in creating role :' || p_error_message);
         RAISE l_exception;
      END IF;
  EXCEPTION
      WHEN l_exception
      THEN
         fnd_file.put_line (
            fnd_file.LOG,
               'p_customer_id - '
            || l_customer_id
            || ' in L_EXCEPTION, p_error_message - '
            || p_error_message);
      WHEN OTHERS
      THEN
         ROLLBACK;
         fnd_file.put_line (
            fnd_file.LOG,
               'p_customer_id - '
            || l_customer_id
            || ' error in  CREATE_CONTACT procedure - '
            || SQLERRM);
   END create_contact;
END XXWCAR_UPDATE_ARS_JOB_SITE_PKG;
/
