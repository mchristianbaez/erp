create or replace PACKAGE BODY xxwc_posourcingconv_pkg
IS
/***************************************************************************
*    script name: xxwc_posourcingconv_pkg.pkb
*
*    client: white cap.
*
*    interface / conversion name: PO Sourcing Rules and
				  assignments conversion.
*
*    functional purpose: convert Sourcing Rules
			and assignments
*
*    history:
*
*    version    date          author      description
*****************************************************************************
*    1.0        04-sep-2011   k.Tavva    initial development.
*    1.1        19-Feb-2013   G.Damuluri TMS - 20130227-01069 Consider
*                                        Vendor Sites from PRISM
****************************************************************************/
/**************************************************************************
*    List Of Validations
*  1   ORGANIZATION VALIDATION
*  2   DUPLICATE RECORD VALIDATION
**************************************************************************/
    PROCEDURE print_debug (p_print_str IN VARCHAR2)
    IS
    BEGIN
        --  IF g_debug = 'Y' THEN
        fnd_file.put_line (fnd_file.LOG, p_print_str);
        -- END IF;
        DBMS_OUTPUT.put_line (p_print_str);
        NULL;
    END print_debug;

    PROCEDURE xx_sourcing_rules_transfer (l_vendor_num VARCHAR2)
    IS
    --Not used in the main procedure
        l_organization_id   NUMBER := 0;
        l_cnt               NUMBER := 0;

        CURSOR c_transfer_rec
        IS
            SELECT   ROWID, xxwcsr.*
              FROM   xxwc_sourcing_rules_conv xxwcsr                       --;
             WHERE                           --xxwcsr.process_status = 'V' AND
                  UPPER (source_type) = UPPER ('Transfer From')
                     AND UPPER (vendor_num) = l_vendor_num;
    BEGIN
        FOR rc_transfer_rec IN c_transfer_rec
        LOOP
            SELECT   COUNT (1), organization_id
              INTO   l_cnt, l_organization_id
              FROM   mtl_parameters
             WHERE   UPPER (organization_code) =
                         UPPER (TO_CHAR (rc_transfer_rec.vendor_num));

            IF l_cnt > 0
            THEN
                UPDATE   xxwc_sourcing_rules_conv
                   SET   status = 'V',
                         source_organization_id = l_organization_id
                 WHERE   ROWID = rc_transfer_rec.ROWID;
            ELSE
                UPDATE   xxwc_sourcing_rules_conv
                   SET   status = 'E',
                         error_message =
                             'Invalid Vendor / Org Code For Transfer From'
                 WHERE   ROWID = rc_transfer_rec.ROWID;
            END IF;

            COMMIT;
        END LOOP;
    END xx_sourcing_rules_transfer;

    PROCEDURE assignment_rec_exists
    IS
        l_sr_count               NUMBER := 0;
        l_assign_error_message   VARCHAR2 (1000) := NULL;

        CURSOR cur_assign_stg_rec
        IS
            SELECT   ROWID, xxwcsrac.*
              FROM   xxwc_sr_assingments_conv xxwcsrac
             WHERE   NVL (process_status, 'R') = 'V';
    BEGIN
        FOR r_cur_assign_stg_rec IN cur_assign_stg_rec
        LOOP
            l_sr_count := 0;
            l_assign_error_message := NULL;

            IF r_cur_assign_stg_rec.sourcing_rule_name IS NOT NULL
            THEN
                SELECT   COUNT (1)
                  INTO   l_sr_count
                  FROM   mrp_sr_assignments
                 WHERE   sourcing_rule_id =
                             (SELECT   sourcing_rule_id
                                FROM   mrp_sourcing_rules
                               WHERE   sourcing_rule_name =
                                           r_cur_assign_stg_rec.sourcing_rule_name)
                         AND assignment_set_id = fnd_profile.VALUE ('MRP_DEFAULT_ASSIGNMENT_SET')
                         AND organization_id =
                                (SELECT   organization_id
                                   FROM   org_organization_definitions
                                  WHERE   UPPER (organization_code) =
                                              r_cur_assign_stg_rec.organization_code)
                         AND inventory_item_id =
                                (SELECT   inventory_item_id
                                   FROM   mtl_system_items_b
                                  WHERE   segment1 =
                                              r_cur_assign_stg_rec.item_num
                                          AND organization_id =
                                                 (SELECT   organization_id
                                                    FROM   org_organization_definitions
                                                   WHERE   organization_code =
                                                               r_cur_assign_stg_rec.organization_code));

                IF l_sr_count > 0
                THEN
                    l_assign_error_message :=
                           l_assign_error_message
                        || '-'
                        || 'Assignment Exists '
                        || r_cur_assign_stg_rec.sourcing_rule_name;
                -- print_debug (l_assign_error_message);
                END IF;
            ELSE
                l_assign_error_message :=
                    l_assign_error_message
                    || ' Sourcing Rule Name is null..! ';
            -- print_debug (l_assign_error_message);
            END IF;

            IF l_assign_error_message IS NULL
            THEN
                UPDATE   xxwc_sr_assingments_conv xxwcsrac
                   SET   process_status = 'V'
                 WHERE   ROWID = r_cur_assign_stg_rec.ROWID;
            ELSE
                UPDATE   xxwc_sr_assingments_conv xxwcsrac
                   SET   error_message = l_assign_error_message,
                         process_status = 'E'
                 WHERE   ROWID = r_cur_assign_stg_rec.ROWID;
            END IF;

            COMMIT;
        END LOOP;
    END assignment_rec_exists;

    PROCEDURE sorce_rule_validate
    IS
        l_sr_count               NUMBER := 0;
        l_assign_error_message   VARCHAR2 (1000) := NULL;

        CURSOR cur_assign_stg_rec
        IS
            SELECT   ROWID, xxwcsrac.*
              FROM   xxwc_sr_assingments_conv xxwcsrac                     --;
             WHERE                       --ASSIGNMENT_SET_NAME is not null and
                  NVL (process_status, 'R') = 'V';
    BEGIN
        FOR r_cur_assign_stg_rec IN cur_assign_stg_rec
        LOOP
            l_sr_count := 0;
            l_assign_error_message := NULL;

            IF r_cur_assign_stg_rec.sourcing_rule_name IS NOT NULL
            THEN
                --print_debug ('SOURCING RULE NAME VALIDATION Process....');
                SELECT   COUNT ( * )
                  INTO   l_sr_count
                  FROM   mrp_sourcing_rules msr,
                         mrp_sr_receipt_org msro,
                         mrp_sr_source_org msmso
                 WHERE   msr.sourcing_rule_id = msro.sourcing_rule_id
                         AND msro.sr_receipt_id = msmso.sr_receipt_id
                         AND msr.sourcing_rule_name =
                                r_cur_assign_stg_rec.sourcing_rule_name
                         AND SYSDATE < NVL (disable_date, SYSDATE + 1);

                IF l_sr_count = 0
                THEN
                    l_assign_error_message :=
                           l_assign_error_message
                        || '-'
                        || 'Sourcing Rule Not Exist '
                        || r_cur_assign_stg_rec.sourcing_rule_name;
                    print_debug (l_assign_error_message);
                END IF;
            ELSE
                l_assign_error_message :=
                    l_assign_error_message
                    || ' Sourcing Rule Name is null..! ';
            --print_debug (l_assign_error_message);
            END IF;

            IF l_assign_error_message IS NULL
            THEN
                UPDATE   xxwc_sr_assingments_conv xxwcsrac
                   SET   process_status = 'V'
                 WHERE   ROWID = r_cur_assign_stg_rec.ROWID;
            ELSE
                UPDATE   xxwc_sr_assingments_conv xxwcsrac
                   SET   error_message = l_assign_error_message,
                         process_status = 'E'
                 WHERE   ROWID = r_cur_assign_stg_rec.ROWID;
            END IF;

            COMMIT;
        END LOOP;
    END sorce_rule_validate;

    PROCEDURE dup_src_validation
    IS
        CURSOR cur_sourcerec_dup_validate
        IS
              --        SELECT DISTINCT sourcing_rule_name, source_organization_code
              --                       FROM xxwc_sourcing_rules_conv
              --                      WHERE process_status = 'R'
              --                  GROUP BY sourcing_rule_name, source_organization_code
              --                    HAVING COUNT (*) > 1;
              SELECT   DISTINCT
                       sourcing_rule_name,
                       receipt_organization_code,
                       source_organization_code
                FROM   xxwc_sourcing_rules_conv
               WHERE   process_status = 'R'
            GROUP BY   sourcing_rule_name,
                       receipt_organization_code,
                       source_organization_code
              HAVING   COUNT ( * ) > 1;

        l_sr_error_message   VARCHAR2 (1000);
    BEGIN
        FOR r_cur_sourc_dup_val IN cur_sourcerec_dup_validate
        LOOP
            l_sr_error_message := NULL;

            BEGIN
                l_sr_error_message := 'Duplicate Source Record. ';
                DBMS_OUTPUT.put_line (l_sr_error_message);

                UPDATE   xxwc_sourcing_rules_conv
                   SET   process_status = 'E',
                         error_message = l_sr_error_message
                 WHERE   source_organization_code =
                             r_cur_sourc_dup_val.source_organization_code
                         AND sourcing_rule_name =
                                r_cur_sourc_dup_val.sourcing_rule_name
                         AND ROWID >
                                (  SELECT   MIN (ROWID)
                                     FROM   xxwc_sourcing_rules_conv
                                    WHERE   source_organization_code =
                                                r_cur_sourc_dup_val.source_organization_code
                                            AND sourcing_rule_name =
                                                   r_cur_sourc_dup_val.sourcing_rule_name
                                            AND process_status = 'R'
                                 GROUP BY   sourcing_rule_name,
                                            source_organization_code
                                   HAVING   COUNT ( * ) > 1)
                         AND process_status = 'R';

                COMMIT;
            END;
        END LOOP;
    END dup_src_validation;

    PROCEDURE dup_assign_validation
    IS
        CURSOR cur_assingerec_dup_validate
        IS
              SELECT   DISTINCT assignment_set_name,
                                organization_code,
                                item_num,
                                sourcing_rule_name
                FROM   xxwc_sr_assingments_conv
               WHERE   process_status = 'R'
            GROUP BY   assignment_set_name,
                       organization_code,
                       item_num,
                       sourcing_rule_name
              HAVING   COUNT ( * ) > 1;

        l_assign_error_message   VARCHAR2 (1000);
    BEGIN
        FOR r_cur_assign_dup_val IN cur_assingerec_dup_validate
        LOOP
            l_assign_error_message := NULL;

            BEGIN
                l_assign_error_message := ' Duplicate Assignment Record. ';
                DBMS_OUTPUT.put_line (l_assign_error_message);

                UPDATE   xxwc_sr_assingments_conv
                   SET   process_status = 'E',
                         error_message = l_assign_error_message
                 WHERE   organization_code =
                             r_cur_assign_dup_val.organization_code
                         AND assignment_set_name =
                                r_cur_assign_dup_val.assignment_set_name
                         AND ROWID >
                                (  SELECT   MIN (ROWID)
                                     FROM   xxwc_sr_assingments_conv
                                    WHERE   organization_code =
                                                r_cur_assign_dup_val.organization_code
                                            AND assignment_set_name =
                                                   r_cur_assign_dup_val.assignment_set_name
                                            AND process_status = 'R'
                                 GROUP BY   assignment_set_name,
                                            organization_code
                                   HAVING   COUNT ( * ) > 1)
                         AND process_status = 'R';

                COMMIT;
            END;
        END LOOP;
    END dup_assign_validation;

    PROCEDURE validations
    IS
        l_sr_count               NUMBER;
        l_sr_error_message       VARCHAR2 (4000);
        l_assign_count           NUMBER;
        l_assign_error_message   VARCHAR2 (4000);
        l_cnt                    NUMBER := 0;
        l_organization_id        NUMBER := 0;
        l_vendor_id              NUMBER;
        l_vendor_site_id         NUMBER;
        l_org_id                 NUMBER := xxwc_ascp_scwb_pkg.get_wc_org_id;

        CURSOR cur_sr_stg_rec
        IS
            SELECT   ROWID, xxwcsrc.*
              FROM   xxwc_sourcing_rules_conv xxwcsrc                     ---;
             WHERE   NVL (process_status, 'R') IN ('R', 'E');

        --OR NVL (process_status, 'R') = ;
        CURSOR cur_assign_stg_rec
        IS
            SELECT   ROWID, xxwcsrac.*
              FROM   xxwc_sr_assingments_conv xxwcsrac                     --;
             WHERE                       --ASSIGNMENT_SET_NAME is not null and
                  NVL (process_status, 'R') IN ('R', 'E');
    --   OR NVL (process_status, 'R') = 'E';
    BEGIN
        FOR r_cur_stg IN cur_sr_stg_rec
        LOOP
            l_sr_count := 0;
            l_sr_error_message := NULL;
            l_organization_id := NULL;
            l_cnt := 0;

            IF UPPER (r_cur_stg.source_type) = 'BUY FROM'
            THEN
                IF r_cur_stg.vendor_num IS NULL
                --or r_cur_stg.VENDOR_SITE_CODE IS NOT NULL
                THEN
                    l_sr_error_message := 'Vendor Number Required';
                END IF;
            ELSIF UPPER (r_cur_stg.source_type) = UPPER ('Transfer From')
            THEN
                IF r_cur_stg.ship_method IS NULL
                   OR r_cur_stg.source_organization_code IS NULL
                THEN
                    l_sr_error_message :=
                        'Ship Method / Source Organization Id Required';
                END IF;
            ELSE
                NULL;
            END IF;

            -------------------------------------- 1     Source ORGANIZATION VALIDATION and receipt org validations
            IF (r_cur_stg.source_organization_code IS NOT NULL
                OR r_cur_stg.receipt_organization_code IS NOT NULL)
            THEN
                IF r_cur_stg.source_organization_code IS NOT NULL
                THEN
                    BEGIN
                        SELECT   COUNT ( * )
                          INTO   l_sr_count
                          FROM   org_organization_definitions
                         WHERE   organization_code = r_cur_stg.source_organization_code
                                 AND SYSDATE < NVL (disable_date, SYSDATE + 1);

                        -- print_debug ('Source ORGANIZATION VALIDATION Process....');

                        -- Checking Active organization
                        IF l_sr_count = 0
                        THEN
                            l_sr_error_message :=
                                   l_sr_error_message
                                || '-'
                                || 'Invalid Source Organization code: '
                                || r_cur_stg.source_organization_code;
                        --print_debug (l_sr_error_message);
                        END IF;
                    END;
                END IF;

                -- 2     Receipt ORGANIZATION VALIDATION
                IF r_cur_stg.receipt_organization_code IS NOT NULL
                THEN
                    BEGIN
                        SELECT   COUNT ( * )
                          INTO   l_sr_count
                          FROM   org_organization_definitions
                         WHERE   organization_code =
                                     r_cur_stg.receipt_organization_code
                                 AND SYSDATE <
                                        NVL (disable_date, SYSDATE + 1);

                        -- print_debug ('Receipt ORGANIZATION VALIDATION Process....');

                        -- Checking Active organization
                        IF l_sr_count = 0
                        THEN
                            l_sr_error_message :=
                                   l_sr_error_message
                                || '-'
                                || 'Invalid Receipt Organization code: '
                                || r_cur_stg.receipt_organization_code;
                        -- print_debug (l_sr_error_message);
                        END IF;
                    END;
                END IF;
            ELSE
                NULL;
            END IF;

            --        --3   SOURCING RULE NAME VALIDATION
            --        IF r_cur_stg.sourcing_rule_name IS NOT NULL
            --        THEN
            ----             SELECT COUNT (*)
            ----                INTO l_sr_count
            ----                FROM mrp_sourcing_rules
            ----              WHERE sourcing_rule_name = r_cur_stg.sourcing_rule_name;
            --            print_debug ('SOURCING RULE NAME VALIDATION Process....');

            --            SELECT COUNT (*)
            --               INTO l_sr_count
            --               FROM mrp_sourcing_rules msr,
            --                      mrp_sr_receipt_org msro,
            --                      mrp_sr_source_org msmso
            --              WHERE msr.sourcing_rule_id = msro.sourcing_rule_id
            --                AND msro.sr_receipt_id = msmso.sr_receipt_id
            --                AND msr.sourcing_rule_name = r_cur_stg.sourcing_rule_name
            --                AND SYSDATE < NVL (disable_date, SYSDATE + 1);

            --            print_debug ('SOURCING RULE NAME VALIDATION Process....');

            --            IF l_sr_count > 0
            --            THEN
            --                l_sr_error_message :=
            --                        l_sr_error_message
            --                    || '-'
            --                    || 'Sourcing Rule Already Existed '
            --                    || r_cur_stg.sourcing_rule_name;
            ----                     print_debug (l_sr_error_message);
            --            END IF;
            --        ELSE
            --            l_sr_error_message :=
            --                          l_sr_error_message || ' Sourcing Rule Name is null..! ';
            ----                 print_debug (l_sr_error_message);
            --        END IF;

            -----------------------vendor id validation
            IF (r_cur_stg.source_type) LIKE ('Transfe%')
            THEN
                --call procedure
                print_debug ('Vendorid Validation');

                --xx_sorucing_rules_transfer (UPPER (TO_CHAR (r_cur_stg.vendor_num)));
                SELECT   COUNT (1)                         --, organization_id
                  INTO   l_cnt                           --, l_organization_id
                  FROM   mtl_parameters
                 WHERE   (organization_code) = (TO_CHAR (r_cur_stg.vendor_num));

                BEGIN
                    IF l_cnt = 0
                    THEN
                        l_sr_error_message :=
                               l_sr_error_message
                            || '-'
                            || 'Invalid Vendor / Org Code For Transfer From';
                    ELSE
                        SELECT   organization_id
                          INTO   l_organization_id
                          FROM   mtl_parameters
                         WHERE   (organization_code) = (TO_CHAR (r_cur_stg.vendor_num));
                    END IF;
                END;
            ELSIF (r_cur_stg.source_type) LIKE ('Buy%Fro%')
            THEN
               BEGIN
                SELECT   vendor_id
                  INTO   l_vendor_id
                  FROM   ap_suppliers
                 /*WHERE   attribute1 IS NOT NULL
                   AND   attribute1 = TO_CHAR (r_cur_stg.vendor_num);/*20130917-00676 */
                 WHERE  segment1 = TO_CHAR(r_cur_stg.vendor_num);  /*20130917-00676 */
                  l_sr_count := 1;       
               EXCEPTION
                 when others then
                   l_sr_count := 0;
               END;

                IF l_sr_count = 0
                THEN
                    l_sr_error_message :=
                           l_sr_error_message
                        || '-'
                        || 'Vendor Name does not Exist '
                        || r_cur_stg.sourcing_rule_name;
                --                print_debug (l_sr_error_message);
                
                ELSE
                  BEGIN
                    SELECT vendor_site_id
                      INTO l_vendor_site_id
                      FROM ap_supplier_sites_all
                     WHERE vendor_id=l_vendor_id
                       AND org_id=l_org_id
                       AND purchasing_site_flag='Y'
                       AND vendor_site_code = r_cur_stg.vendor_site_code -- Added as per TMS - 20130227-01069
                       AND rownum=1;
                  EXCEPTION
                   when others then 
                    l_sr_error_message :=
                           l_sr_error_message
                        || '-'
                        || 'Vendor Site does not Exist '
                        || r_cur_stg.sourcing_rule_name;
                  END;  
                END IF;
                
                
            ELSE
                l_sr_error_message :=
                    l_sr_error_message || ' Vendor Name is null..! ';
            --            print_debug (l_sr_error_message);
            END IF;

            --Sourcing Records Updation Proecss
            IF l_sr_error_message IS NULL
            THEN
                --            print_debug ('Sourcing Records Updation Process....');
                UPDATE   xxwc_sourcing_rules_conv xxwcsrc
                   SET   process_status = 'V',
                         source_organization_id = l_organization_id
                 WHERE   ROWID = r_cur_stg.ROWID;
            ELSE
                UPDATE   xxwc_sourcing_rules_conv xxwcsrc
                   SET   error_message = l_sr_error_message,
                         process_status = 'E'
                 WHERE   ROWID = r_cur_stg.ROWID;
            END IF;

            COMMIT;
        END LOOP;

        print_debug ('Duplicate Sourcing  Record Validaton Process....');
        xxwc_posourcingconv_pkg.dup_src_validation;
        print_debug ('Duplicate Sourcing Record Validaton End....');
        print_debug ('Assignment Organizationn Validation');

        -- 4     Assignment Organization Validation
        FOR r_cur_assign_stg IN cur_assign_stg_rec
        LOOP
            l_assign_error_message := NULL;

            BEGIN
                SELECT   COUNT ( * )
                  INTO   l_assign_count
                  FROM   org_organization_definitions
                 WHERE   UPPER (organization_code) =
                             UPPER (r_cur_assign_stg.organization_code)
                         AND SYSDATE < NVL (disable_date, SYSDATE + 1);

                --  print_debug ('Assignment Organizationn Validation');

                -- Checking Active organization
                IF l_assign_count = 0
                THEN
                    l_assign_error_message :=
                        'Invalid Assignment Organization Id '
                        || r_cur_assign_stg.organization_code;
                    print_debug (l_assign_error_message);
                END IF;
            END;

            ---ITEM VALIDATION
            BEGIN
                SELECT   COUNT (1)
                  INTO   l_assign_count
                  FROM   mtl_system_items_b
                 WHERE   segment1 = r_cur_assign_stg.item_num
                         AND organization_id =
                                (SELECT   organization_id
                                   FROM   org_organization_definitions
                                  WHERE   organization_code =
                                              r_cur_assign_stg.organization_code);

                --  print_debug ('Assignment Organizationn Validation');

                -- Checking Active organization
                IF l_assign_count = 0
                THEN
                    l_assign_error_message :=
                        'Invalid Item Number '||r_cur_assign_stg.item_num ||' for '
                        || r_cur_assign_stg.organization_code;
                    print_debug (l_assign_error_message);
                END IF;
            END;

            --        --5   Assignment Set Name Validation
            --        BEGIN
            --            SELECT COUNT (*)
            --               INTO l_assign_count
            --               FROM mrp_sourcing_rules msr,
            --                      mrp_sr_assignments msa,
            --                      mrp_assignment_sets mas
            --              WHERE msr.sourcing_rule_id = msa.sourcing_rule_id
            --                AND mas.assignment_set_id = msa.assignment_set_id
            --                AND msr.organization_id = msa.organization_id
            --                AND mas.assignment_set_name =
            --                                                    r_cur_assign_stg.assignment_set_name;

            --            IF l_assign_count > 0
            --            THEN
            --                l_assign_error_message :=
            --                        ' Duplicate Assignment Set Name'
            --                    || r_cur_assign_stg.organization_code;
            --                print_debug (l_assign_error_message);
            --            END IF;
            --        END;

            --Sourcing Assignment Records Updation Proecss
            IF l_assign_error_message IS NULL
            THEN
                UPDATE   xxwc_sr_assingments_conv xxwcsrac
                   SET   process_status = 'V'
                 WHERE   ROWID = r_cur_assign_stg.ROWID;
            ELSE
                UPDATE   xxwc_sr_assingments_conv xxwcsrac
                   SET   error_message = l_assign_error_message,
                         process_status = 'E'
                 WHERE   ROWID = r_cur_assign_stg.ROWID;
            END IF;

            COMMIT;
        END LOOP;

        xxwc_posourcingconv_pkg.dup_assign_validation;
    END validations;

    PROCEDURE process_records
    IS
        -----------------------------------------------------------------------------------------
        l_session_id                NUMBER;
        l_msg_index_out             NUMBER;
        l_count                     NUMBER;
        l_org_cnt                   NUMBER;
        l_vendor_cnt                NUMBER;
        l_org_class                 VARCHAR2 (3);
        l_org_num                   NUMBER;
        l_org_id                    NUMBER := xxwc_ascp_scwb_pkg.get_wc_org_id; --fnd_profile.value('DEFAULT_ORG_ID');
        l_line_num                  NUMBER := 0;
        l_err_count                 NUMBER := 0;
        -----------------------------------------------------------------------------------------
        l_return_status             VARCHAR2 (1);
        l_msg_count                 NUMBER := 0;
        l_msg_data                  VARCHAR2 (1000);
        --l_msg_index_out            NUMBER;
        --l_count                        NUMBER;
        --l_err_count                    NUMBER                                                 := 0;
        l_sourcing_rule_rec         mrp_sourcing_rule_pub.sourcing_rule_rec_type;
        l_sourcing_rule_val_rec     mrp_sourcing_rule_pub.sourcing_rule_val_rec_type;
        l_receiving_org_tbl         mrp_sourcing_rule_pub.receiving_org_tbl_type;
        l_receiving_org_val_tbl     mrp_sourcing_rule_pub.receiving_org_val_tbl_type;
        l_shipping_org_tbl          mrp_sourcing_rule_pub.shipping_org_tbl_type;
        l_shipping_org_val_tbl      mrp_sourcing_rule_pub.shipping_org_val_tbl_type;
        o_sourcing_rule_rec         mrp_sourcing_rule_pub.sourcing_rule_rec_type;
        o_sourcing_rule_val_rec     mrp_sourcing_rule_pub.sourcing_rule_val_rec_type;
        o_receiving_org_tbl         mrp_sourcing_rule_pub.receiving_org_tbl_type;
        o_receiving_org_val_tbl     mrp_sourcing_rule_pub.receiving_org_val_tbl_type;
        o_shipping_org_tbl          mrp_sourcing_rule_pub.shipping_org_tbl_type;
        o_shipping_org_val_tbl      mrp_sourcing_rule_pub.shipping_org_val_tbl_type;
        -----------------------------------------------------------------------------------------
        -- l_assign_ession_id           NUMBER;
        l_assign_return_status      VARCHAR2 (1);
        l_assign_msg_count          NUMBER := 0;
        l_assign_msg_data           VARCHAR2 (1000);
        --l_assign_msg_index_out      NUMBER;
        --l_count                         NUMBER;
        --l_org_cnt                       NUMBER;
        --l_vendor_cnt                NUMBER;
        --   l_org_class                      VARCHAR2 (3);
        --   l_org_num                    NUMBER;
        --   l_line_num                   NUMBER                                                := 0;
        --   l_err_count                      NUMBER                                                := 0;
        l_assignment_set_rec        mrp_src_assignment_pub.assignment_set_rec_type;
        l_assignment_set_val_rec    mrp_src_assignment_pub.assignment_set_val_rec_type;
        l_assignment_tbl            mrp_src_assignment_pub.assignment_tbl_type;
        l_assignment_val_tbl        mrp_src_assignment_pub.assignment_val_tbl_type;
        o_assignment_set_rec        mrp_src_assignment_pub.assignment_set_rec_type;
        o_assignment_set_val_rec    mrp_src_assignment_pub.assignment_set_val_rec_type;
        o_assignment_tbl            mrp_src_assignment_pub.assignment_tbl_type;
        o_assignment_val_tbl        mrp_src_assignment_pub.assignment_val_tbl_type;
        l_error_message             VARCHAR2 (1000);
        l_source_organization_id    NUMBER := NULL;
        l_source_type               NUMBER;
        l_receipt_organization_id   NUMBER := NULL;
        l_vendor_id                 NUMBER;
        l_vendor_site_id            NUMBER;
        --assignments
        l_assignment_set_id         NUMBER := fnd_profile.VALUE ('MRP_DEFAULT_ASSIGNMENT_SET');
        l_inventory_item_id         NUMBER;
        l_sourcing_rule_id          NUMBER;
        l_category_set_id           NUMBER;
        l_caegory_id                NUMBER;
        l_customer_id               NUMBER;
        l_cust_site_id              NUMBER;
        l_organization_id           NUMBER;
        --l_vendor_site_id            NUMBER;
        p                           NUMBER := 1;
        --WHO COLUMNS
        l_user                      NUMBER := fnd_global.user_id;
        l_creation_date             DATE := SYSDATE;

        CURSOR c_rec_sr_stg
        IS
            SELECT   ROWID, xxwcsr.*
              FROM   xxwc_sourcing_rules_conv xxwcsr                       --;
             WHERE   xxwcsr.process_status = 'V';

        TYPE xxwcr_table IS TABLE OF c_rec_sr_stg%ROWTYPE;

        process_source_rec          xxwcr_table;

        ---------------------------------------
        CURSOR c_rec_sra_stg                            --(p_cur_org VARCHAR2)
        IS
            SELECT   ROWID, xxwcsra.*
              FROM   xxwc_sr_assingments_conv xxwcsra
             WHERE   xxwcsra.process_status = 'V';

        TYPE xxwcsra_table IS TABLE OF c_rec_sra_stg%ROWTYPE;

        process_assignment_rec      xxwcsra_table;

        --vendor id
        CURSOR c_vendor (p_vendor_num VARCHAR2)
        IS
            SELECT   DISTINCT vendor_id
              FROM   po_vendors
             --WHERE   NVL (attribute1, 1) = p_vendor_num; --20130917-00676 
             WHERE nvl(segment1,1) = p_vendor_num; --20130917-00676 

        --list of cusrors for assignment sets
        --orgid retrival
        CURSOR c_org (p_org VARCHAR2)
        IS
            SELECT   organization_id
              FROM   org_organization_definitions
             WHERE   organization_code = p_org;

        --item
        CURSOR c_item (p_item_num VARCHAR2, p_orgid NUMBER)
        IS
            SELECT   inventory_item_id
              FROM   mtl_system_items_b
             WHERE   segment1 = p_item_num AND organization_id = p_orgid;

        --sourcing rule
        CURSOR c_sourcing (p_sourcing_rule_name VARCHAR2)
        IS
            SELECT   sourcing_rule_id
              FROM   mrp_sourcing_rules
             WHERE   sourcing_rule_name = p_sourcing_rule_name;

    ---------------------------------------
    BEGIN
        --------Validating source and Assignment reocrds
        print_debug (' Validating Sourcing and Assignment Records Starts');
        print_debug ('-----------------------------------------------------');
        xxwc_posourcingconv_pkg.validations;
        print_debug ('End of  Validating Sourcing and Assignment Records ');
        print_debug ('-----------------------------------------------------');
        print_debug (' Sourcing Records Insertion....');

        --call the Transfer From Procedure
        -- xx_sorucing_rules_transfer;
        FOR r_rec_sr_stg IN c_rec_sr_stg
        LOOP
            l_error_message := NULL;
            l_vendor_id := NULL;

            IF r_rec_sr_stg.source_organization_code IS NOT NULL
            THEN
                SELECT   organization_id
                  INTO   l_source_organization_id
                  FROM   org_organization_definitions
                 WHERE   organization_code = r_rec_sr_stg.source_organization_code;
            ELSE
                l_source_organization_id := NULL;
            END IF;

            IF r_rec_sr_stg.receipt_organization_code IS NOT NULL
            THEN
                SELECT   organization_id
                  INTO   l_receipt_organization_id
                  FROM   org_organization_definitions
                 WHERE   organization_code = r_rec_sr_stg.receipt_organization_code;
            ELSE
                l_receipt_organization_id := NULL;
            END IF;

            IF UPPER (r_rec_sr_stg.source_type) = UPPER ('Buy From')
            THEN
                l_source_type := 3;

                BEGIN
                    FOR r_vendor IN c_vendor (r_rec_sr_stg.vendor_num)
                    LOOP
                        l_vendor_id := r_vendor.vendor_id;
                    END LOOP;
                    
                  BEGIN
                    select vendor_site_id
                      into l_vendor_site_id
                      from ap_supplier_sites_all
                     where vendor_id=l_vendor_id
                       and org_id=l_org_id
                       and purchasing_site_flag='Y'
                       AND vendor_site_code = r_rec_sr_stg.vendor_site_code -- Added as per TMS - 20130227-01069
                       and rownum=1;
                  EXCEPTION
                   when others then l_vendor_site_id := null;
                  END;  
                    
                EXCEPTION
                    WHEN NO_DATA_FOUND
                    THEN
                        UPDATE   xxwc_sourcing_rules_conv
                           SET   process_status = 'E',
                                 error_message = 'Invalid Vendor Number '
                         WHERE   ROWID = r_rec_sr_stg.ROWID;

                        COMMIT;
                    WHEN OTHERS
                    THEN
                        UPDATE   xxwc_sourcing_rules_conv
                           SET   process_status = 'E',
                                 error_message = 'Invalid Vendor Number '
                         WHERE   ROWID = r_rec_sr_stg.ROWID;

                        COMMIT;
                END;
            ELSIF UPPER (r_rec_sr_stg.source_type) = UPPER ('Transfer')
            THEN
                l_source_type := 1;
                l_vendor_id := NULL;
                l_vendor_site_id := NULL;
            ELSE
                l_source_type := 2;
            END IF;

            fnd_message.clear;
            l_sourcing_rule_rec := mrp_sourcing_rule_pub.g_miss_sourcing_rule_rec;
            l_sourcing_rule_rec.sourcing_rule_name := r_rec_sr_stg.sourcing_rule_name; --SR Name
            l_sourcing_rule_rec.description := r_rec_sr_stg.sourcing_rule_desc; -- Description
            --Enabled by Shankar to create local rule Nov-2011
            -- Disable the below assignment to create global rule--shankar--21-Feb-2012
            --l_sourcing_rule_rec.organization_id := l_receipt_organization_id;
            l_sourcing_rule_rec.planning_active := r_rec_sr_stg.planning_active;
            l_sourcing_rule_rec.status := r_rec_sr_stg.status;  -- Create New record
            l_sourcing_rule_rec.sourcing_rule_type := r_rec_sr_stg.sourcing_rule_type;
            l_sourcing_rule_rec.operation := r_rec_sr_stg.operation;
            l_sourcing_rule_rec.attribute1 := r_rec_sr_stg.attribute1;
            l_sourcing_rule_rec.attribute2 := r_rec_sr_stg.attribute2;
            l_sourcing_rule_rec.attribute3 := r_rec_sr_stg.attribute3;
            l_sourcing_rule_rec.attribute4 := r_rec_sr_stg.attribute4;


            l_receiving_org_tbl := mrp_sourcing_rule_pub.g_miss_receiving_org_tbl;
            l_shipping_org_tbl := mrp_sourcing_rule_pub.g_miss_shipping_org_tbl;
            -- Disable the below assignment to create global rule--shankar--21-Feb-2012            
            --l_receiving_org_tbl (p).receipt_organization_id := l_receipt_organization_id;
            l_receiving_org_tbl (1).effective_date := TRUNC (SYSDATE);
            l_receiving_org_tbl (1).operation := r_rec_sr_stg.operation;
            -- Create or Update
            l_shipping_org_tbl (1).RANK := 1;
            l_shipping_org_tbl (1).allocation_percent := 100;
            l_shipping_org_tbl (1).source_type := l_source_type;
            l_shipping_org_tbl (1).source_organization_id := r_rec_sr_stg.source_organization_id;
            l_shipping_org_tbl (1).receiving_org_index := 1;
            l_shipping_org_tbl (1).operation := r_rec_sr_stg.operation;
            l_shipping_org_tbl (1).ship_method := r_rec_sr_stg.ship_method;
            l_shipping_org_tbl (1).secondary_inventory := r_rec_sr_stg.secondary_inventory;
            l_shipping_org_tbl (1).vendor_id := l_vendor_id;          --52238;
            l_shipping_org_tbl (1).vendor_site_id := l_vendor_site_id;          --

            --------who columns
            l_receiving_org_tbl (1).creation_date := l_creation_date;
            l_receiving_org_tbl (1).last_update_date := l_creation_date;
            l_receiving_org_tbl (1).last_updated_by := l_user;
            l_receiving_org_tbl (1).created_by := l_user;
            l_sourcing_rule_rec.creation_date := l_creation_date;
            l_sourcing_rule_rec.last_update_date := l_creation_date;
            l_sourcing_rule_rec.last_updated_by := l_user;
            l_sourcing_rule_rec.created_by := l_user;
            l_shipping_org_tbl (1).creation_date := l_creation_date;
            l_shipping_org_tbl (1).last_update_date := l_creation_date;
            l_shipping_org_tbl (1).last_updated_by := l_user;
            l_shipping_org_tbl (1).created_by := l_user;
            ------------
            mrp_sourcing_rule_pub.process_sourcing_rule (
                p_api_version_number      => 1.0,
                p_init_msg_list           => fnd_api.g_true --,p_return_values =>
                                                           ,
                p_commit                  => fnd_api.g_true,
                x_return_status           => l_return_status,
                x_msg_count               => l_msg_count,
                x_msg_data                => l_msg_data,
                p_sourcing_rule_rec       => l_sourcing_rule_rec,
                p_sourcing_rule_val_rec   => l_sourcing_rule_val_rec,
                p_receiving_org_tbl       => l_receiving_org_tbl,
                p_receiving_org_val_tbl   => l_receiving_org_val_tbl,
                p_shipping_org_tbl        => l_shipping_org_tbl,
                p_shipping_org_val_tbl    => l_shipping_org_val_tbl,
                x_sourcing_rule_rec       => o_sourcing_rule_rec,
                x_sourcing_rule_val_rec   => o_sourcing_rule_val_rec,
                x_receiving_org_tbl       => o_receiving_org_tbl,
                x_receiving_org_val_tbl   => o_receiving_org_val_tbl,
                x_shipping_org_tbl        => o_shipping_org_tbl,
                x_shipping_org_val_tbl    => o_shipping_org_val_tbl);
            print_debug ('insertion after sourcing');

            IF l_return_status = fnd_api.g_ret_sts_success
            THEN
                --            print_debug ('Success!');

                --update stmt
                UPDATE   xxwc_sourcing_rules_conv
                   SET   process_status = 'P'
                 WHERE   ROWID = r_rec_sr_stg.ROWID;
            ELSE
                print_debug ('count:' || l_msg_count);

                IF l_msg_count > 0
                THEN
                    FOR l_index IN 1 .. l_msg_count
                    LOOP
                        l_msg_data :=
                            fnd_msg_pub.get (p_msg_index   => l_index,
                                             p_encoded     => fnd_api.g_false);
                        print_debug (SUBSTR (l_msg_data, 1, 250));
                        l_error_message := l_error_message || l_msg_data;
                    END LOOP;

                    --update stmt
                    UPDATE   xxwc_sourcing_rules_conv
                       SET   process_status = 'E',
                             error_message =
                                 'Sourcing Error' || '-' || l_error_message
                     -- WHERE ROWID = process_source_rec (i).ROWID;
                     WHERE   ROWID = r_rec_sr_stg.ROWID;
                --COMMIT;
                --                print_debug ('MSG:' || o_sourcing_rule_rec.return_status);
                END IF;

                print_debug ('Failure!');
            END IF;

            COMMIT;
        END LOOP;

        ------------------------------------------------------------------------------------------------
        print_debug (' processeing Assignment records');

        BEGIN
            --        print_debug ('Before insert');

            --calling the sourcing rule name validation
            xxwc_posourcingconv_pkg.sorce_rule_validate;
            xxwc_posourcingconv_pkg.assignment_rec_exists;
            p := 1;

            FOR r_rec_sra_stg IN c_rec_sra_stg
            LOOP
                fnd_message.clear;
                l_assignment_tbl.delete;

                BEGIN
                    FOR r_org IN c_org (r_rec_sra_stg.organization_code)
                    LOOP
                        l_organization_id := r_org.organization_id;
                    END LOOP;

                    FOR r_item
                    IN c_item (r_rec_sra_stg.item_num, l_organization_id)
                    LOOP
                        l_inventory_item_id := r_item.inventory_item_id;
                    END LOOP;

                    FOR r_sourcing
                    IN c_sourcing (r_rec_sra_stg.sourcing_rule_name)
                    LOOP
                        l_sourcing_rule_id := r_sourcing.sourcing_rule_id;
                    END LOOP;
                EXCEPTION
                    WHEN OTHERS
                    THEN
                        --updat stging table
                        UPDATE   xxwc_sr_assingments_conv
                           SET   process_status = 'E',
                                 error_message =
                                        'Assignment Error'
                                     || '-'
                                     || 'Sourcing / Item Error'
                         WHERE   ROWID = r_rec_sra_stg.ROWID;
                END;

                l_assignment_tbl (p).assignment_set_id :=
                    fnd_profile.VALUE ('MRP_DEFAULT_ASSIGNMENT_SET');
                --BY USING DEFAULT PROFILE VALUES--33045;
                -- WC Default - New --2001; --WC Default
                l_assignment_tbl (p).assignment_type := 6;
                l_assignment_tbl (p).operation := r_rec_sra_stg.operation;
                l_assignment_tbl (p).organization_id := l_organization_id;
                l_assignment_tbl (p).inventory_item_id := l_inventory_item_id;
                l_assignment_tbl (p).sourcing_rule_id := l_sourcing_rule_id;
                l_assignment_tbl (p).sourcing_rule_type :=
                    r_rec_sra_stg.source_rule_type;
                --------who columns
                l_assignment_tbl (p).creation_date := l_creation_date;
                l_assignment_tbl (p).last_update_date := l_creation_date;
                l_assignment_tbl (p).last_updated_by := l_user;
                l_assignment_tbl (p).created_by := l_user;
                -- p := p + 1;
                --  END LOOP;
                mrp_src_assignment_pub.process_assignment (
                    p_api_version_number       => 1.0,
                    p_init_msg_list            => fnd_api.g_false,
                    p_return_values            => fnd_api.g_false,
                    p_commit                   => fnd_api.g_false,
                    x_return_status            => l_assign_return_status,
                    x_msg_count                => l_assign_msg_count,
                    x_msg_data                 => l_assign_msg_data,
                    p_assignment_set_rec       => l_assignment_set_rec,
                    p_assignment_set_val_rec   => l_assignment_set_val_rec,
                    p_assignment_tbl           => l_assignment_tbl,
                    p_assignment_val_tbl       => l_assignment_val_tbl,
                    x_assignment_set_rec       => o_assignment_set_rec,
                    x_assignment_set_val_rec   => o_assignment_set_val_rec,
                    x_assignment_tbl           => o_assignment_tbl,
                    x_assignment_val_tbl       => o_assignment_val_tbl);
                print_debug ('After insert Assignments');

                IF l_assign_return_status = fnd_api.g_ret_sts_success
                THEN
                    print_debug ('Success!');

                    UPDATE   xxwc_sr_assingments_conv
                       SET   process_status = 'P'
                     WHERE   ROWID = r_rec_sra_stg.ROWID;
                --            COMMIT;
                ELSE
                    print_debug ('count:' || l_assign_msg_count);

                    IF l_assign_msg_count > 0
                    THEN
                        FOR l_index IN 1 .. l_assign_msg_count
                        LOOP
                            l_assign_msg_data :=
                                fnd_msg_pub.get (
                                    p_msg_index   => l_index,
                                    p_encoded     => fnd_api.g_false);
                            print_debug (SUBSTR (l_assign_msg_data, 1, 250));
                            l_error_message := l_assign_msg_data;
                        END LOOP;

                        UPDATE   xxwc_sr_assingments_conv
                           SET   process_status = 'E',
                                 error_message = l_error_message
                         WHERE   ROWID = r_rec_sra_stg.ROWID;

                        --                COMMIT;
                        print_debug ('MSG:' || o_assignment_set_rec.return_status);
                    END IF;

                    print_debug ('Failure!');
                END IF;

                COMMIT;
            END LOOP;
        END;

        print_debug (' processeing ends sourcing');
    END process_records;

    PROCEDURE posourcing_conv_proc (errbuf               OUT VARCHAR2,
                                    retcode              OUT NUMBER,
                                    p_validate_only   IN     VARCHAR2 --,p_submit  IN  VARCHAR2
                                                                     )
    IS
        l_count                NUMBER;
        l_count_valid          NUMBER;
        l_assign_count         NUMBER;
        l_assign_count_valid   NUMBER;
        v_errorcode            NUMBER;
        v_errormessage         VARCHAR2 (240);
    ----------------------------------------------------------------------
    BEGIN
        IF p_validate_only = 'Y'
        THEN
            ---------------
            print_debug ('Performing .........!');

            SELECT   COUNT (1)
              INTO   l_count
              FROM   xxwc_sourcing_rules_conv
             WHERE   NVL (process_status, 'R') IN ('R', 'E');

            SELECT   COUNT (1)
              INTO   l_assign_count
              FROM   xxwc_sr_assingments_conv
             WHERE   NVL (process_status, 'R') IN ('R', 'E');

            IF l_count > 0 OR l_assign_count > 0
            THEN
                xxwc_posourcingconv_pkg.print_debug (
                    'Performing Validations');
                xxwc_posourcingconv_pkg.validations;
            ELSE
                xxwc_posourcingconv_pkg.print_debug ('No Records To Process');
            END IF;
        ELSE
            SELECT   COUNT (1)
              INTO   l_count
              FROM   xxwc_sourcing_rules_conv
             WHERE   NVL (process_status, 'R') IN ('R', 'E');

            SELECT   COUNT (1)
              INTO   l_assign_count
              FROM   xxwc_sr_assingments_conv
             WHERE   NVL (process_status, 'R') IN ('R', 'E');

            IF l_count > 0 OR l_assign_count > 0
            THEN
                xxwc_posourcingconv_pkg.print_debug (
                    'Performing Validations');
                xxwc_posourcingconv_pkg.validations;
            ELSE
                xxwc_posourcingconv_pkg.print_debug ('No Records To Process');
            END IF;

            SELECT   COUNT (1)
              INTO   l_count_valid
              FROM   xxwc_sourcing_rules_conv
             WHERE   process_status = 'V';

            SELECT   COUNT (1)
              INTO   l_assign_count_valid
              FROM   xxwc_sr_assingments_conv
             WHERE   process_status = 'V';

            IF l_count_valid > 0 OR l_assign_count_valid > 0
            THEN
                xxwc_posourcingconv_pkg.print_debug ('Performing Insert');
                xxwc_posourcingconv_pkg.process_records;
            ELSE
                xxwc_posourcingconv_pkg.print_debug ('No Records To Insert');
            END IF;
        END IF;
    END posourcing_conv_proc;
END XXWC_POSOURCINGCONV_PKG;