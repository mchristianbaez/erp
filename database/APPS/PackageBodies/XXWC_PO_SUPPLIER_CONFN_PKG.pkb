CREATE OR REPLACE PACKAGE BODY XXWC_PO_SUPPLIER_CONFN_PKG AS
 /*************************************************************************************
   $Header XXWC_PO_SUPPLIER_CONFN_PKG $
   Module Name: XXWC_PO_SUPPLIER_CONFN_PKG.pks

   PURPOSE:   This package attaches the URL Information to PO header. 
              Mails the supplier approval confirmation on the same to Buyers.

   REVISIONS:
   Ver        Date        Author                             Description
   ---------  ----------  -------------------------------    -------------------------
   1.0        18/04/2018  Ashwin Sridhar                     Initial Version-TMS#20180305-00300--Automate PO Confirmations - Oracle EBS
  *************************************************************************************/

FUNCTION XXWC_SUPP_CONFIRM_BE_PROC(p_subscription_guid   IN RAW
                                  ,p_event               IN OUT wf_event_t)
  RETURN VARCHAR2 IS 

--Cursor to check for the PO URL details...
CURSOR cu_po_comm(cp_po_number IN VARCHAR2) IS
SELECT DISTINCT XPSC.CONFIRMATION_URL
,      PHA.PO_HEADER_ID
,      PHA.VENDOR_ID
,      PHA.VENDOR_SITE_ID
,      PHA.AGENT_ID
FROM   XXWC.XXWC_PO_SUPPLIER_CONFIRMATIONS XPSC
,      PO_HEADERS_ALL PHA
WHERE  1=1
AND    XPSC.PO_NUMBER = PHA.SEGMENT1
AND    XPSC.PO_NUMBER = cp_po_number
AND    XPSC.PROCESSED_FLAG IS NULL;

ln_user_id        NUMBER:=fnd_global.user_id;
ln_category_id    NUMBER;
ln_seq_num        NUMBER;
l_sec             VARCHAR2 (2000);
l_req_id          NUMBER:= fnd_global.conc_request_id;
g_distro_list     VARCHAR2 (75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
ln_vendor_id      NUMBER;
ln_vendor_site_id NUMBER;
ln_po_header_id   NUMBER;
lv_url            VARCHAR2(500);
lv_err_flag       VARCHAR2(1):='N';
lv_email          VARCHAR2(100);
ln_person_id      NUMBER;
l_plist           wf_parameter_list_t := p_event.getparameterlist ();
ln_po_number      VARCHAR2(50);
l_error_msg       VARCHAR2(2000);

BEGIN
  l_sec:='Start of the package...';
  FND_FILE.PUT_LINE (FND_FILE.LOG, 'Start of the package...');
  
  ln_po_number := wf_event.getvalueforparameter ('P_PO_NUMBER', l_plist);
  
  FOR rec_po_comm IN cu_po_comm(ln_po_number) LOOP
    COMMIT;
  
    l_sec:='Inside Loop for PO Number '||rec_po_comm.po_header_id;
    FND_FILE.PUT_LINE (FND_FILE.LOG, 'Inside Loop for PO Number '||rec_po_comm.po_header_id);
    ln_vendor_id:=rec_po_comm.VENDOR_ID;
    ln_vendor_site_id:=rec_po_comm.VENDOR_SITE_ID;
    lv_url:= rec_po_comm.CONFIRMATION_URL;
    ln_po_header_id:=rec_po_comm.PO_HEADER_ID;
    ln_person_id:=rec_po_comm.AGENT_ID;
  
    --Getting the document category
    BEGIN
    
      SELECT CATEGORY_ID 
      INTO   ln_category_id
      FROM   FND_DOCUMENT_CATEGORIES_TL
      WHERE  user_name='Documents';
    
    EXCEPTION
    WHEN others THEN
    
      ln_category_id:=NULL;
    
    END;

    --Getting the Max Sequence Value for attachments for the given PO Number...
    BEGIN

      SELECT MAX(SEQ_NUM)
      INTO   ln_seq_num
      FROM   FND_ATTACHED_DOCUMENTS
      WHERE  ENTITY_NAME IN ('PO_HEAD','PO_HEADERS')
      AND    PK1_VALUE  =rec_po_comm.po_header_id;

    EXCEPTION
    WHEN others THEN 

      ln_seq_num:=0; 

    END;

    IF ln_category_id IS NOT NULL THEN

      l_sec:='Inside Category ID NOT NULL ';
      FND_FILE.PUT_LINE (FND_FILE.LOG, 'Inside Category ID NOT NULL');

      BEGIN
      
        fnd_webattch.add_attachment(seq_num              => ln_seq_num+10,
                                    category_id          => ln_category_id,
                                    document_description => 'URL Details',
                                    datatype_id          => 5, --Web Page
                                    text                 => NULL,
                                    file_name            => NULL,
                                    url                  => rec_po_comm.CONFIRMATION_URL,
                                    function_name        => null,
                                    entity_name          => 'PO_HEADERS',
                                    pk1_value            => rec_po_comm.po_header_id,
                                    pk2_value            => NULL,
                                    pk3_value            => NULL,
                                    pk4_value            => NULL,
                                    pk5_value            => NULL,
                                    media_id             => NULL,
                                    user_id              => ln_user_id,
                                    usage_type           => 'O',
                                    title                => 'Confirmation'
                                    );
      
      EXCEPTION
      WHEN others THEN

        l_sec:='Inside Exception for URL attachment'; 
        FND_FILE.PUT_LINE (FND_FILE.LOG, 'Inside Exception for URL attachment');

        lv_err_flag:='W';

        xxcus_error_pkg.xxcus_error_main_api (
           p_called_from         => 'XXWC_PO_SUPPLIER_CONFN_PKG.XXWC_SUPP_CONFIRM_PROC',
           p_calling             => l_sec,
           p_request_id          => l_req_id,
           p_ora_error_msg       => SUBSTR (l_sec, 1, 2000),
           p_error_desc          => 'OTHERS Exception',
           p_distribution_list   => g_distro_list,
           p_module              => 'XXCUS');

        l_error_msg:=SQLERRM;

        UPDATE XXWC.XXWC_PO_SUPPLIER_CONFIRMATIONS
        SET    PROCESSED_FLAG ='W'
        ,      VENDOR_ID      = ln_vendor_id 
        ,      VENDOR_SITE_ID = ln_vendor_site_id
        ,      PO_HEADER      = ln_po_header_id
        ,      ERROR_MESSAGE  = l_sec
        WHERE  PO_NUMBER      = ln_po_number
        AND    PROCESSED_FLAG IS NULL;
  
        COMMIT;
            
      END;

    END IF;

  END LOOP;
  
  FND_FILE.PUT_LINE (FND_FILE.LOG, 'URL Attachment for PO Completed...');
  
  IF lv_err_flag='N' THEN
    --Updating OK transactions in po supplier confirmation table...
    UPDATE XXWC.XXWC_PO_SUPPLIER_CONFIRMATIONS
    SET    PROCESSED_FLAG ='Y'
    ,      VENDOR_ID      = ln_vendor_id 
    ,      VENDOR_SITE_ID = ln_vendor_site_id
    ,      PO_HEADER      = ln_po_header_id
    WHERE  PO_NUMBER      = ln_po_number
    AND    PROCESSED_FLAG IS NULL;

    COMMIT;

    BEGIN
    
      SELECT EMAIL_ADDRESS
      INTO   lv_email
      FROM   per_all_people_f 
      WHERE  1=1
      AND    SYSDATE BETWEEN effective_start_date AND effective_end_date
      AND    person_id = ln_person_id;
     
    EXCEPTION
    WHEN others THEN

      lv_email:=NULL;

    END;    

    IF lv_email IS NOT NULL THEN

      --Calling Email procedure to Email the Buyer...
      XXWC_SEND_BUYER_EMAIL_PROC(ln_po_number  
                                ,lv_email      
                                ,lv_url        
                                 );

    END IF;

  END IF;
  
  RETURN ('SUCCESS');
  
EXCEPTION
WHEN others THEN

  FND_FILE.PUT_LINE (FND_FILE.LOG, ' ');
  FND_FILE.PUT_LINE (fnd_file.LOG, 'ERROR in ' || l_sec);
  FND_FILE.PUT_LINE (FND_FILE.LOG, ' ');
 
  l_error_msg := SUBSTR (SQLERRM, 1, 240);

  wf_core.context ('XXWC_PO_SUPPLIER_CONFN_PKG',
                   'XXWC_SUPP_CONFIRM_PROC',
                   p_event.geteventname (),
                   p_subscription_guid);

  wf_event.seterrorinfo (p_event, SUBSTR (SQLERRM, 1, 240));

  RETURN 'ERROR';

  xxcus_error_pkg.xxcus_error_main_api (
     p_called_from         => 'XXWC_PO_SUPPLIER_CONFN_PKG.XXWC_SUPP_CONFIRM_PROC',
     p_calling             => l_sec,
     p_request_id          => l_req_id,
     p_ora_error_msg       => SUBSTR (l_sec, 1, 2000),
     p_error_desc          => 'OTHERS Exception',
     p_distribution_list   => g_distro_list,
     p_module              => 'XXCUS');

  --Updating OK transactions in po supplier confirmation table...
  UPDATE XXWC.XXWC_PO_SUPPLIER_CONFIRMATIONS
  SET    PROCESSED_FLAG ='E'
  ,      VENDOR_ID      = ln_vendor_id 
  ,      VENDOR_SITE_ID = ln_vendor_site_id
  ,      PO_HEADER      = ln_po_header_id
  ,      ERROR_MESSAGE  = l_sec
  WHERE  PO_NUMBER      = ln_po_number
  AND    PROCESSED_FLAG IS NULL;

  COMMIT;
  
END XXWC_SUPP_CONFIRM_BE_PROC;

PROCEDURE XXWC_SEND_BUYER_EMAIL_PROC(p_po_number  IN NUMBER
                                    ,p_email      IN VARCHAR2
                                    ,p_url        IN VARCHAR2
                                    )IS

lv_subject VARCHAR2(1000);
lv_body    VARCHAR2(4000); 
l_connection   UTL_SMTP.connection;
l_sec          VARCHAR2(200); 
l_host         VARCHAR2(256) := 'mailoutrelay.hdsupply.net';
l_hostport     VARCHAR2(20)  := '25';
l_email_from   VARCHAR2(240) := 'no-reply@whitecap.net ';
l_boundary     VARCHAR2 (255)      DEFAULT 'a1b2c3d4e3f2g1';
l_body_html    CLOB                := EMPTY_CLOB;
--This will be the email message
l_offset       NUMBER;
l_ammount      NUMBER;
l_temp         VARCHAR2 (32767)    DEFAULT NULL;

BEGIN

  FND_FILE.PUT_LINE (FND_FILE.LOG, 'Inside procedure XXWC_SEND_BUYER_EMAIL_PROC');
  l_sec :='Inside procedure XXWC_SEND_BUYER_EMAIL_PROC';

  fnd_message.set_name ('XXWC', 'XXWC_SUPPLIER_PO_CON_SUBJECT');
  fnd_message.set_token ('PO_NUMBER', TO_CHAR (p_po_number));
  
  lv_subject := FND_MESSAGE.GET;
  
  fnd_message.set_name ('XXWC', 'XXWC_SUPPLIER_PO_CON_BODY');
  fnd_message.set_token ('PO_NUMBER', TO_CHAR (p_po_number));
  fnd_message.set_token ('PO_URL', TO_CHAR (p_url));
  
  lv_body := FND_MESSAGE.GET;
  
   l_sec := 'Open Connection'; 
   FND_FILE.PUT_LINE (FND_FILE.LOG,'Open Connection');
   l_connection := UTL_SMTP.open_connection (l_host, l_hostport);
   UTL_SMTP.helo (l_connection, l_host);
   UTL_SMTP.mail (l_connection, l_email_from);
   UTL_SMTP.rcpt (l_connection, p_email);
   
   l_sec := 'Set UTL_SMTP rcpt email id';
   
      l_temp := l_temp || 'MIME-Version: 1.0' || CHR (13) || CHR (10);
      l_temp := l_temp || 'To: ' || p_email || CHR (13) || CHR (10);
      l_temp := l_temp || 'From: ' || l_email_from || CHR (13) || CHR (10);
      l_temp := l_temp || 'Subject: ' || lv_subject || CHR (13) || CHR (10);
      l_temp := l_temp || 'Reply-To: ' || l_email_from || CHR (13) || CHR (10);
      l_temp :=
            l_temp
         || 'Content-Type: multipart/alternative; boundary='
         || CHR (34)
         || l_boundary
         || CHR (34)
         || CHR (13)
         || CHR (10);
      ----------------------------------------------------
      -- Write the headers
      l_sec := 'call dbms_lob.createtemporary'; 
      DBMS_LOB.createtemporary (l_body_html, FALSE, 10);
      l_sec := 'call dbms_lob.write 1'; 
      DBMS_LOB.WRITE (l_body_html, LENGTH (l_temp), 1, l_temp);
      ----------------------------------------------------
      -- Write the text boundary
      l_offset := DBMS_LOB.getlength (l_body_html) + 1;
      l_temp := '--' || l_boundary || CHR (13) || CHR (10);
      l_temp :=
            l_temp
         || 'content-type: text/plain; charset=us-ascii'
         || CHR (13)
         || CHR (10)
         || CHR (13)
         || CHR (10);
      l_sec := 'call dbms_lob.write 2'; 
      DBMS_LOB.WRITE (l_body_html, LENGTH (l_temp), l_offset, l_temp);
      ----------------------------------------------------
      -- Write the plain text portion of the email
      l_offset := DBMS_LOB.getlength (l_body_html) + 1;
      l_sec := 'call dbms_lob.write 3'; 
      DBMS_LOB.WRITE (l_body_html, LENGTH (lv_body), l_offset, lv_body);
      ----------------------------------------------------
      -- Write the HTML boundary
      l_temp :=
            CHR (13)
         || CHR (10)
         || CHR (13)
         || CHR (10)
         || '--'
         || l_boundary
         || CHR (13)
         || CHR (10);
      l_temp :=
            l_temp
         || 'content-type: text/html;'
         || CHR (13)
         || CHR (10)
         || CHR (13)
         || CHR (10);
      l_offset := DBMS_LOB.getlength (l_body_html) + 1;
      l_sec := 'call dbms_lob.write 4'; 
      DBMS_LOB.WRITE (l_body_html, LENGTH (l_temp), l_offset, l_temp);
      ----------------------------------------------------
      -- Write the HTML portion of the message
      l_offset := DBMS_LOB.getlength (l_body_html) + 1;
      l_sec := 'call dbms_lob.write 5'; 
      DBMS_LOB.WRITE (l_body_html, LENGTH (lv_body), l_offset, lv_body);
      ----------------------------------------------------
      -- Write the final html boundary
      l_temp := CHR (13) || CHR (10) || '--' || l_boundary || '--' || CHR (13);
      l_offset := DBMS_LOB.getlength (l_body_html) + 1;
      l_sec := 'call dbms_lob.write 6'; 
      DBMS_LOB.WRITE (l_body_html, LENGTH (l_temp), l_offset, l_temp);
      ----------------------------------------------------
       -- Send the email in 1900 byte chunks to UTL_SMTP
      l_offset := 1;
      l_ammount := 1900;
      UTL_SMTP.open_data (l_connection);
      l_sec := 'call utl_smtp.write_data'; 
      WHILE l_offset < DBMS_LOB.getlength (l_body_html)
      LOOP
         UTL_SMTP.write_data (l_connection,
                              DBMS_LOB.SUBSTR (l_body_html,
                                               l_ammount,
                                               l_offset
                                              )
                             );
         l_offset := l_offset + l_ammount;
         l_ammount :=
                     LEAST (1900, DBMS_LOB.getlength (l_body_html) - l_ammount);
      END LOOP;
   
   l_sec := 'Close connection'; 
   UTL_SMTP.close_data (l_connection);
   UTL_SMTP.quit (l_connection);
   DBMS_LOB.freetemporary (l_body_html);

   FND_FILE.PUT_LINE (FND_FILE.LOG,'Email Sent Successfully');
   
EXCEPTION
WHEN others THEN

  l_sec:='Inside Exception for procedure XXWC_SEND_BUYER_EMAIL_PROC';
  FND_FILE.PUT_LINE (FND_FILE.LOG,l_sec);

END XXWC_SEND_BUYER_EMAIL_PROC;

PROCEDURE XXWC_RAISE_SUPP_CONFIRMATION(p_po_number IN NUMBER
                                      ,p_error_msg OUT VARCHAR2) IS PRAGMA AUTONOMOUS_TRANSACTION;

l_error_msg  VARCHAR2 (4000);
l_parameter_list  wf_parameter_list_t;
l_event_data      CLOB;
l_sec             VARCHAR2 (200);
g_distro_list     VARCHAR2 (75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';

BEGIN
   
  l_sec := 'Set values for Parameter List';
           
  l_parameter_list :=wf_parameter_list_t(wf_parameter_t ('P_PO_NUMBER', p_po_number));

  l_sec := 'Raise business Event';
        
  wf_event.raise (
                  p_event_name   => 'xxwc.oracle.apps.po.supp.confirm_event',
                  p_event_key    => SYS_GUID (),
                  p_event_data   => l_event_data,
                  p_parameters   => l_parameter_list);

  COMMIT;

EXCEPTION
WHEN others THEN

  xxcus_error_pkg.xxcus_error_main_api (
     p_called_from         => 'XXWC_PO_SUPPLIER_CONFN_PKG.XXWC_RAISE_SUPP_CONFIRMATION',
     p_calling             => l_sec,
     p_request_id          => fnd_global.conc_request_id,
     p_ora_error_msg       => SUBSTR (
                                   ' Error_Stack...'
                                || DBMS_UTILITY.format_error_stack ()
                                || ' Error_Backtrace...'
                                || DBMS_UTILITY.format_error_backtrace (),
                                1,
                                2000),
     p_error_desc          => SUBSTR (SQLERRM, 1, 240),
     p_distribution_list   => g_distro_list,
     p_module              => 'PO');
     
  p_error_msg := 'Failed for calling supplier confirmation '||SUBSTR (SQLERRM, 1, 240);

END XXWC_RAISE_SUPP_CONFIRMATION;

PROCEDURE XXWC_SUPP_CONFIRM_PROC(p_errbuf    OUT VARCHAR2
                                ,p_retcode   OUT VARCHAR2
                                ,p_po_number IN NUMBER) IS

--Cursor to check for the PO URL details...
CURSOR cu_po_comm(cp_po_number IN VARCHAR2) IS
SELECT DISTINCT XPSC.CONFIRMATION_URL
,      PHA.PO_HEADER_ID
,      PHA.VENDOR_ID
,      PHA.VENDOR_SITE_ID
,      PHA.AGENT_ID
FROM   XXWC.XXWC_PO_SUPPLIER_CONFIRMATIONS XPSC
,      PO_HEADERS_ALL PHA
WHERE  1=1
AND    XPSC.PO_NUMBER = PHA.SEGMENT1
AND    XPSC.PO_NUMBER = cp_po_number
AND    XPSC.PROCESSED_FLAG IS NULL;

ln_user_id        NUMBER:=fnd_global.user_id;
ln_category_id    NUMBER;
ln_seq_num        NUMBER;
l_sec             VARCHAR2 (2000);
l_req_id          NUMBER:= fnd_global.conc_request_id;
g_distro_list     VARCHAR2 (75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
ln_vendor_id      NUMBER;
ln_vendor_site_id NUMBER;
ln_po_header_id   NUMBER;
lv_url            VARCHAR2(500);
lv_err_flag       VARCHAR2(1):='N';
lv_email          VARCHAR2(100);
ln_person_id      NUMBER;
l_error_msg       VARCHAR2(2000);

BEGIN

  l_sec:='Start of the package...';
  FND_FILE.PUT_LINE (FND_FILE.LOG, 'Start of the package...');
  
  FOR rec_po_comm IN cu_po_comm(p_po_number) LOOP
  
    COMMIT;
  
    l_sec:='Inside Loop for PO Number '||rec_po_comm.po_header_id;
    FND_FILE.PUT_LINE (FND_FILE.LOG, 'Inside Loop for PO Number '||rec_po_comm.po_header_id);
    ln_vendor_id:=rec_po_comm.VENDOR_ID;
    ln_vendor_site_id:=rec_po_comm.VENDOR_SITE_ID;
    lv_url:= rec_po_comm.CONFIRMATION_URL;
    ln_po_header_id:=rec_po_comm.PO_HEADER_ID;
    ln_person_id:=rec_po_comm.AGENT_ID;
  
    --Getting the document category
    BEGIN
    
      SELECT CATEGORY_ID 
      INTO   ln_category_id
      FROM   FND_DOCUMENT_CATEGORIES_TL
      WHERE  user_name='Documents';
    
    EXCEPTION
    WHEN others THEN
    
      ln_category_id:=NULL;
    
    END;

    --Getting the Max Sequence Value for attachments for the given PO Number...
    BEGIN

      SELECT NVL(MAX(SEQ_NUM),0)
      INTO   ln_seq_num
      FROM   FND_ATTACHED_DOCUMENTS
      WHERE  ENTITY_NAME IN ('PO_HEAD','PO_HEADERS')
      AND    PK1_VALUE  =rec_po_comm.po_header_id;

    EXCEPTION
    WHEN others THEN 

      ln_seq_num:=0; 

    END;

    IF ln_category_id IS NOT NULL THEN

      l_sec:='Inside Category ID NOT NULL ';
      FND_FILE.PUT_LINE (FND_FILE.LOG, 'Inside Category ID NOT NULL');

      BEGIN
      
        fnd_webattch.add_attachment(seq_num              => ln_seq_num+10,
                                    category_id          => ln_category_id,
                                    document_description => 'URL Details',
                                    datatype_id          => 5, --Web Page
                                    text                 => NULL,
                                    file_name            => NULL,
                                    url                  => rec_po_comm.CONFIRMATION_URL,
                                    function_name        => null,
                                    entity_name          => 'PO_HEADERS',
                                    pk1_value            => rec_po_comm.po_header_id,
                                    pk2_value            => NULL,
                                    pk3_value            => NULL,
                                    pk4_value            => NULL,
                                    pk5_value            => NULL,
                                    media_id             => NULL,
                                    user_id              => ln_user_id,
                                    usage_type           => 'O',
                                    title                => 'Confirmation'
                                    );
      
      EXCEPTION
      WHEN others THEN

        l_sec:='Inside Exception for URL attachment'; 
        FND_FILE.PUT_LINE (FND_FILE.LOG, 'Inside Exception for URL attachment');

        lv_err_flag:='W';

        xxcus_error_pkg.xxcus_error_main_api (
           p_called_from         => 'XXWC_PO_SUPPLIER_CONFN_PKG.XXWC_SUPP_CONFIRM_PROC',
           p_calling             => l_sec,
           p_request_id          => l_req_id,
           p_ora_error_msg       => SUBSTR (l_sec, 1, 2000),
           p_error_desc          => 'OTHERS Exception',
           p_distribution_list   => g_distro_list,
           p_module              => 'XXCUS');

        l_error_msg:=SQLERRM;

        UPDATE XXWC.XXWC_PO_SUPPLIER_CONFIRMATIONS
        SET    PROCESSED_FLAG ='W'
        ,      VENDOR_ID      = ln_vendor_id 
        ,      VENDOR_SITE_ID = ln_vendor_site_id
        ,      PO_HEADER      = ln_po_header_id
        ,      ERROR_MESSAGE  = l_sec
        WHERE  PO_NUMBER      = p_po_number
        AND    PROCESSED_FLAG IS NULL;
  
        COMMIT;
            
      END;

    END IF;

  END LOOP;
  
  FND_FILE.PUT_LINE (FND_FILE.LOG, 'URL Attachment for PO Completed...');
  
  IF lv_err_flag='N' THEN
  
    --Updating OK transactions in po supplier confirmation table...
    UPDATE XXWC.XXWC_PO_SUPPLIER_CONFIRMATIONS
    SET    PROCESSED_FLAG ='Y'
    ,      VENDOR_ID      = ln_vendor_id 
    ,      VENDOR_SITE_ID = ln_vendor_site_id
    ,      PO_HEADER      = ln_po_header_id
    WHERE  PO_NUMBER      = p_po_number
    AND    PROCESSED_FLAG IS NULL;

    COMMIT;

    BEGIN
    
      SELECT EMAIL_ADDRESS
      INTO   lv_email
      FROM   per_all_people_f 
      WHERE  1=1
      AND    SYSDATE BETWEEN effective_start_date AND effective_end_date
      AND    person_id = ln_person_id;
     
    EXCEPTION
    WHEN others THEN

      lv_email:=NULL;

    END;    

    IF lv_email IS NOT NULL THEN

      --Calling Email procedure to Email the Buyer...
      XXWC_SEND_BUYER_EMAIL_PROC(p_po_number  
                                ,lv_email      
                                ,lv_url        
                                 );

    END IF;

  END IF;
  
EXCEPTION
WHEN others THEN

  FND_FILE.PUT_LINE (FND_FILE.LOG, ' ');
  FND_FILE.PUT_LINE (fnd_file.LOG, 'ERROR in ' || l_sec);
  FND_FILE.PUT_LINE (FND_FILE.LOG, ' ');
 
  l_error_msg := SUBSTR (SQLERRM, 1, 240);

  xxcus_error_pkg.xxcus_error_main_api (
     p_called_from         => 'XXWC_PO_SUPPLIER_CONFN_PKG.XXWC_SUPP_CONFIRM_PROC',
     p_calling             => l_sec,
     p_request_id          => l_req_id,
     p_ora_error_msg       => SUBSTR (l_sec, 1, 2000),
     p_error_desc          => 'OTHERS Exception',
     p_distribution_list   => g_distro_list,
     p_module              => 'XXCUS');

  --Updating OK transactions in po supplier confirmation table...
  UPDATE XXWC.XXWC_PO_SUPPLIER_CONFIRMATIONS
  SET    PROCESSED_FLAG ='E'
  ,      VENDOR_ID      = ln_vendor_id 
  ,      VENDOR_SITE_ID = ln_vendor_site_id
  ,      PO_HEADER      = ln_po_header_id
  ,      ERROR_MESSAGE  = l_sec
  WHERE  PO_NUMBER      = p_po_number
  AND    PROCESSED_FLAG IS NULL;

  COMMIT;
  
END XXWC_SUPP_CONFIRM_PROC;

END XXWC_PO_SUPPLIER_CONFN_PKG;
/