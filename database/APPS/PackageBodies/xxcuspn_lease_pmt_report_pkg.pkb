CREATE OR REPLACE PACKAGE BODY APPS.xxcuspn_lease_pmt_report_pkg AS

  /**************************************************************************
  ===========================================================================
    Program Name: xxcuspn_lease_pmt_report_pkg
    PROGRAM TYPE: PL/SQL Package
    PURPOSE: denormalize lease data for reporting purpose
    HISTORY
  ===========================================================================
  ===========================================================================
      VERSION DATE          AUTHOR(S)          DESCRIPTION                              TICKET
      ------- -----------   ---------------    ------------------------------           ---------------
      1.0     03-Jun-2013   Luong Vu           Created this package.
      1.1     29-Jan-2014   Balaguru Seshadri  add truncate logic in the main routine.  ESMS 237440 / RFC 39233
      1.2     15-Aug-2016   Balaguru Seshadri  Add new columns                          TMS 20160815-00072 OR ESMS 438366]
      1.3     12-Dec-2017   Balaguru Seshadri   TMS 20171212-00018 / RITM0017849 
  ***************************************************************************/


  /*  ===========================================================================
    Procedure Name: create_pmt_headers
    PURPOSE: create denormalized lease header data for reporting purpose
    HISTORY
  ===========================================================================
    VERSION DATE          AUTHOR(S)          DESCRIPTION
    ------- -----------   ---------------    ------------------------------
    1.0     03-Jun-2013   Luong Vu           Created this package.
  ===========================================================================*/
  -- Begin Ver 1.3
  g_email_mime_type  varchar2(150) :='text; charset=us-ascii';    
  g_notif_email_from varchar2(240) :='Oracle.EBS.OPN.Rent.Repop@hdsupply.com';
  g_email_subject varchar2(240) :='HDS OPN Rent Extract - Status Update';
  g_notif_email_body varchar2(240) :=Null;
  g_notif_email_to varchar2(240) :='leaseadministration@hdsupply.com';
  -- End Ver 1.3  
  procedure print_log(p_message in varchar2) is
  begin
   if fnd_global.conc_request_id >0 then
    fnd_file.put_line(fnd_file.log, p_message);
   else
    dbms_output.put_line(p_message);
   end if;
  exception
   when others then
    fnd_file.put_line(fnd_file.log, 'Issue in print_log routine ='||sqlerrm);
  end print_log;
  -- Begin Ver 1.3
 procedure SEND_EMAIL_NOTIF
   (
        p_email_from in varchar2,
        p_email_to in varchar2,
        p_email_subject in varchar2,
        p_email_body in varchar2
   ) is
    l_sub_routine varchar2(30) :='send_email_notif';    
  begin   
      --
      UTL_MAIL.send
          (
            sender          =>p_email_from,
            recipients    =>p_email_to,            
            subject        =>p_email_subject,
            message     =>p_email_body,
            mime_type =>g_email_mime_type
         );
      --
  exception
   when others then
    print_log('Issue in xxcuspn_lease_pmt_report_pkg.send_email_notif, message =>'||sqlerrm);   
    print_log('Issue in sending email notification : message =>'||sqlerrm);
  end SEND_EMAIL_NOTIF;
  --    
  -- End Ver 1.3
  PROCEDURE create_pmt_headers(errbuf  OUT VARCHAR2
                              ,retcode OUT VARCHAR2
                              ) IS

    l_procedure_name VARCHAR2(75) DEFAULT 'create_pmt_headers';

  BEGIN
    -- Insert Lease Header Rec
    INSERT INTO xxcus.xxcuspn_lease_headers_tbl
      SELECT le.lease_id
            ,le.lease_num
            ,loc_bld.location_code
            ,le.name
            ,ad.address_line1 || ' ' || ad.address_line2 address
            ,ad.city
            ,ad.state
            ,ad.province
            ,ad.country
            ,ad.zip_code
            ,le.status
            ,flv2.meaning lease_status
            ,ld.lease_commencement_date
            ,ld.lease_execution_date
            ,ld.lease_termination_date
            ,ld.lease_extension_end_date
            ,loc_bld.location_id
            ,SYSDATE
            ,le.last_update_date lease_upd_dt
            ,ld.last_update_date rer_detail_upd_dt
            ,flv.meaning lease_type
            ,le.lease_class_code
            ,decode(ld.attribute9, 'Y', 'Yes', NULL) capital_lease
            ,CASE gcc.segment1
               WHEN '06' THEN
                'PLB'
               WHEN '07' THEN
                'PLB'
               ELSE
                llr.bu_id
             END lob
            ,gcc.segment1 gl_product
        FROM pn.pn_leases_all              le
            ,pn.pn_lease_details_all       ld
            ,pn.pn_locations_all           loc_bld
            ,pn.pn_addresses_all           ad
            ,gl.gl_code_combinations       gcc
            ,apps.xxcuspn_ld_lob_rollup_vw llr
            ,apps.fnd_lookups              flv
            ,apps.fnd_lookups              flv2
       WHERE 1 =1
         AND ad.address_id = loc_bld.address_id
         AND loc_bld.location_id = le.location_id
         AND le.lease_id = ld.lease_id
         AND ld.expense_account_id = gcc.code_combination_id(+)
         AND gcc.segment1 = llr.fps_id(+)
         AND le.lease_type_code = flv.lookup_code
         AND flv.lookup_type = 'PN_LEASE_TYPE'
         AND le.lease_status = flv2.lookup_code
         AND flv2.lookup_type = 'PN_LEASESTATUS_TYPE'
         AND loc_bld.active_end_date =
             (SELECT MAX(active_end_date)
                FROM pn.pn_locations_all
               WHERE location_id = loc_bld.location_id)
       ORDER BY le.lease_id;
    print_log('Total lease header records inserted ='||sql%rowcount);
    COMMIT;
  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;

      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := 'Error in program ' || l_procedure_name || ' Error: ' ||
                    l_sec;
      l_err_msg  := l_err_msg || ' ERROR ' || substr(SQLERRM, 1, 2000);

      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode := l_err_code;
      errbuf  := l_err_msg;

      xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_procedure_name,
                                           p_calling => l_sec,
                                           p_request_id => fnd_global.conc_request_id,
                                           p_ora_error_msg => SQLERRM,
                                           p_error_desc => 'eRROR',
                                           p_distribution_list => l_distro_list,
                                           p_module => 'PN');

    WHEN OTHERS THEN

      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := l_sec || l_err_msg || ' ERROR ' || SQLCODE ||
                    substr(SQLERRM, 1, 2000);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode := l_err_code;
      errbuf  := l_err_msg;

      xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_procedure_name,
                                           p_calling => l_sec,
                                           p_request_id => fnd_global.conc_request_id,
                                           p_ora_error_msg => SQLERRM,
                                           p_error_desc => 'Error running Eclipse Acct Extract Process in GL_ACCT_RECON_PKG.CREATE_GLBAL_TWO_SEGS package with OTHERS Exception',
                                           p_distribution_list => l_distro_list,
                                           p_module => 'PN');

  END create_pmt_headers;

  ---test lease_id 1627, 1002, 1607 (dist line), 1042 (end_date 4712), 1051(both end dates 4712)
  -- 1804 - multiple terms with mix dist.
  -- 1971 - insurance
  -- 1118 - CPI
  -- 1024 - QTR
  -- 3533 - INCOME

  /*  ===========================================================================
    Procedure Name: create_pmt_detail
    PURPOSE: create denormalized lease detail data for reporting purpose
    HISTORY
  ===========================================================================
    VERSION DATE          AUTHOR(S)          DESCRIPTION
    ------- -----------   ---------------    ------------------------------
    1.0     03-Jun-2013   Luong Vu           Created this package.
  ===========================================================================*/

  PROCEDURE create_pmt_detail(errbuf  OUT VARCHAR2
                             ,retcode OUT VARCHAR2) IS

    l_base_rent  NUMBER;
    l_first_rent NUMBER;
    l_last_rent  NUMBER;
    l_year_start NUMBER;
    l_year_end   NUMBER;
    l_duration   NUMBER;
    l_exists     NUMBER;
    l_init       NUMBER;
    l_perdiem    NUMBER;
    l_days       NUMBER;
    l_start_date DATE;
    l_end_date   DATE;
    l_rent       NUMBER;
    l_payment    NUMBER;
    l_month      NUMBER;
    l_year       NUMBER;
    l_extend     NUMBER;
    l_count      NUMBER;
    l_amount     NUMBER;
    l_annual_amt NUMBER;
    l_line_id    NUMBER;

    l_procedure_name VARCHAR2(75) DEFAULT 'CREATE_PMT_DETAIL';
    l_lease_id       NUMBER := 1801; --5980; ------------remove after testing......................

    CURSOR lease_cur IS
      SELECT le.lease_id
            ,le.lease_status
            ,le.lease_class_code
            ,le.lease_termination_date
        FROM xxcus.xxcuspn_lease_headers_tbl le
        WHERE 1 =1
       ORDER BY le.lease_id
      --
      ;

    lease_rec lease_cur%ROWTYPE;

  BEGIN
    --dbms_output.enable(10000000);

    l_sec := 'START';
    ----------------------------------
    -- STEP 1 - Create YEAR layout  --
    ----------------------------------

    -- Get number of month to extend lease for end date 31-Dec-4712
    SELECT meaning
      INTO l_extend
      FROM apps.fnd_lookups
     WHERE lookup_type = 'HDS_OPN_FINANCIAL_REPORT'
       AND lookup_code = 'EXTEND';

    IF NOT lease_cur%ISOPEN THEN
      OPEN lease_cur;
    END IF;
    LOOP
      FETCH lease_cur
        INTO lease_rec;
      EXIT WHEN lease_cur%NOTFOUND;

      l_sec := 'Start Account Distribution Cursor';
      print_log('Lease Id ='||lease_rec.lease_id);
      FOR c_distribution IN (SELECT dis.distribution_id
                               FROM pn.pn_distributions_all         dis
                                   ,xxcus.xxcuspn_lease_headers_tbl le
                                   ,pn.pn_payment_terms_all         pt
                              WHERE le.lease_id = pt.lease_id
                                AND pt.payment_term_id =
                                    dis.payment_term_id(+)
                                AND le.lease_id = lease_rec.lease_id
                                AND dis.account_class NOT IN ('LIA', 'REC'))
      LOOP
             l_sec := 'Inside Cursor c_distribution';
        FOR c_payment IN (SELECT pt.payment_term_id
                                ,flv2.meaning payment_term_type
                                ,flv.meaning frequency_code
                                ,pt.start_date
                                ,pt.end_date
                                ,pt.estimated_amount
                                ,pt.vendor_id
                                ,pt.vendor_site_id
                                ,pt.lease_id
                                ,dis.percentage
                                ,lc.fru
                                ,gcc.segment1
                                ,gcc.segment2
                                ,gcc.segment4
                                ,CASE
                                   WHEN lease_rec.lease_class_code =
                                        'SUB_LEASE' THEN
                                    cus.customer_name
                                   ELSE
                                    sup.vendor_name
                                 END customer_name
                                ,CASE
                                   WHEN lease_rec.lease_class_code =
                                        'SUB_LEASE' THEN
                                    cus.customer_number
                                   ELSE
                                    sup.attribute1
                                 END party_number
                                ,lc.lob_branch    lob_branch
                                ,flv3.meaning     payment_purpose
                                ,pt.attribute1    term_rate
                                ,pt.attribute2    tax_payer
                                ,pt.term_comments
                                ,pt.currency_code
                            FROM pn.pn_payment_terms_all       pt
                                ,pn.pn_distributions_all       dis
                                ,gl.gl_code_combinations       gcc
                                ,apps.ar_customers             cus
                                ,ap.ap_suppliers               sup
                                ,apps.fnd_lookups              flv
                                ,apps.fnd_lookups              flv2
                                ,apps.fnd_lookups              flv3
                                ,xxcus.xxcus_location_code_tbl lc
                           WHERE sup.vendor_id(+) = pt.vendor_id
                             AND cus.customer_id(+) = pt.customer_id
                             AND pt.payment_term_id = dis.payment_term_id(+)
                             AND dis.account_id = gcc.code_combination_id
                             AND gcc.segment2 = lc.entrp_loc
                             AND pt.frequency_code = flv.lookup_code(+)
                             AND pt.payment_term_type_code =
                                 flv2.lookup_code(+)
                             AND pt.payment_purpose_code =
                                 flv3.lookup_code(+)
                             AND lease_id = lease_rec.lease_id
                             AND dis.distribution_id =
                                 c_distribution.distribution_id
                             AND dis.account_class NOT IN ('LIA', 'REC')
                             AND flv.lookup_type =
                                 'PN_PAYMENT_FREQUENCY_TYPE'
                             AND flv2.lookup_type = 'PN_PAYMENT_TERM_TYPE'
                             AND flv3.lookup_type =
                                 'PN_PAYMENT_PURPOSE_TYPE'
                             AND lc.inactive = 'N'
                           ORDER BY start_date
                                   ,end_date)
        LOOP
           l_sec := 'Inside Cursor c_payment';
          --print_log(l_sec);
          -- get lease start year
          IF extract(MONTH FROM c_payment.start_date) = 1 THEN
            l_year_start := extract(YEAR FROM c_payment.start_date) - 1;
          ELSE
            l_year_start := extract(YEAR FROM c_payment.start_date);
          END IF;
          l_sec := 'l_year_start ='||l_year_start;
          --print_log(l_sec);
          -- get lease end year
          IF c_payment.end_date = '31-Dec-4712'
             AND lease_rec.lease_termination_date = '31-Dec-4712' THEN
            l_year_end := extract(YEAR FROM
                                  add_months(trunc(SYSDATE), l_extend));
          ELSIF c_payment.end_date = '31-Dec-4712'
                AND lease_rec.lease_termination_date <> '31-Dec-4712' THEN
            l_year_end := extract(YEAR FROM
                                  lease_rec.lease_termination_date);
          ELSE
            l_year_end := extract(YEAR FROM c_payment.end_date);
          END IF;
          l_sec := 'l_year_end ='||l_year_end;
          --print_log(l_sec);
          --  Build out year rows for the duration of the lease base on available payment terms.

          WHILE l_year_start <= l_year_end
          LOOP
            -- Get line_id
            l_line_id := xxcuspn_rent_rpt_s.nextval;
            l_sec := 'After checking l_year_start <= l_year_end, l_line_id ='||l_line_id;

            SELECT CASE
                     WHEN EXISTS
                      (SELECT 1
                             FROM xxcus.xxcuspn_lease_payments_tbl
                            WHERE year_ca = l_year_start
                              AND lease_id = lease_rec.lease_id
                              AND percentage = c_payment.percentage
                              AND frequency_code = c_payment.frequency_code
                              AND payment_purpose = c_payment.payment_purpose
                              AND payment_term_type =
                                  c_payment.payment_term_type
                              AND nvl(vendor_id, 0) =
                                  nvl(c_payment.vendor_id, 0)) THEN
                      1
                     ELSE
                      0
                   END
              INTO l_exists
              FROM dual;

            IF (l_exists = 1) THEN
              l_year_start := l_year_start + 1;
               l_sec := 'After checking l_exists = 1';
            ELSE
              INSERT INTO xxcus.xxcuspn_lease_payments_tbl
                (lease_id
                ,vendor_id
                ,creation_date
                ,lease_class_code
                ,lease_status
                ,percentage
                ,fru
                ,gl_product
                ,gl_location
                ,gl_account
                ,lob_branch
                ,payment_purpose
                ,payment_term_type
                ,frequency_code
                ,term_rate
                ,tax_payer
                ,start_date
                ,end_date
                ,currency_code
                ,estimated_amount
                ,vendor_name
                ,party_number
                ,year_ca
                ,term_comments
                ,line_id)
              VALUES
                (c_payment.lease_id
                ,c_payment.vendor_id
                ,SYSDATE
                ,lease_rec.lease_class_code
                ,NULL
                ,c_payment.percentage
                ,c_payment.fru
                ,c_payment.segment1
                ,c_payment.segment2
                ,c_payment.segment4
                ,c_payment.lob_branch
                ,c_payment.payment_purpose
                ,c_payment.payment_term_type
                ,c_payment.frequency_code
                ,c_payment.term_rate
                ,c_payment.tax_payer
                ,c_payment.start_date
                ,c_payment.end_date
                ,c_payment.currency_code
                ,c_payment.estimated_amount
                ,c_payment.customer_name
                ,c_payment.party_number
                ,l_year_start
                ,c_payment.term_comments
                ,l_line_id);
              l_sec := 'After inserting xxcus.xxcuspn_lease_payments_tbl';
              COMMIT;
            END IF;
          END LOOP; -- WHILE
        END LOOP c_payment;
      END LOOP c_distribution;
    END LOOP lease_cur;
    CLOSE lease_cur;
    ------------------------------
    -- STEP 2 - Build payments  --
    ------------------------------

    IF NOT lease_cur%ISOPEN THEN
      OPEN lease_cur;
    END IF;
    LOOP
      FETCH lease_cur
        INTO lease_rec;
      EXIT WHEN lease_cur%NOTFOUND;

      l_last_rent := 0;
      l_perdiem   := 0;
      l_base_rent := 0;
      l_count     := 0;

      FOR c_payment IN (SELECT pt.payment_term_id
                              ,pt.start_date
                              ,pt.end_date
                              ,pt.estimated_amount
                              ,pt.lease_id
                              ,flv.meaning         frequency_code
                              ,flv2.meaning        payment_term_type_code
                              ,dis.percentage
                              ,flv3.meaning        payment_purpose
                              ,pt.vendor_id
                          FROM pn.pn_payment_terms_all pt
                              ,pn.pn_distributions_all dis
                              ,apps.fnd_lookups        flv
                              ,apps.fnd_lookups        flv2
                              ,apps.fnd_lookups        flv3
                         WHERE flv.lookup_code = pt.frequency_code
                           AND flv3.lookup_code = pt.payment_purpose_code
                           AND flv2.lookup_code = pt.payment_term_type_code
                           AND pt.payment_term_id = dis.payment_term_id
                           AND pt.lease_id = lease_rec.lease_id
                           AND dis.account_class NOT IN ('LIA', 'REC')
                           AND flv.lookup_type = 'PN_PAYMENT_FREQUENCY_TYPE'
                           AND flv2.lookup_type = 'PN_PAYMENT_TERM_TYPE'
                           AND flv3.lookup_type = 'PN_PAYMENT_PURPOSE_TYPE'
                         ORDER BY pt.payment_term_id
                                 ,start_date
                                 ,end_date)
      LOOP

        -- ANNUAL PAYMENT
        IF c_payment.frequency_code = 'Annually' THEN
          FOR c_year IN (SELECT lease_id
                               ,vendor_id
                               ,year_ca
                               ,percentage
                                --,frequency_code
                               ,flv.meaning frequency_code
                               ,TRIM(to_char(c_payment.start_date, 'Month')) months
                               ,estimated_amount
                               ,payment_term_type
                           FROM xxcus.xxcuspn_lease_payments_tbl
                               ,apps.fnd_lookups flv
                          WHERE flv.meaning = frequency_code
                            AND lease_id = c_payment.lease_id
                            AND payment_term_type =
                                c_payment.payment_term_type_code
                            AND flv.meaning = c_payment.frequency_code
                            AND flv.lookup_type =
                                'PN_PAYMENT_FREQUENCY_TYPE')
          LOOP

            IF lease_rec.lease_class_code = 'SUB_LEASE'
               OR lease_rec.lease_class_code = 'THIRD_PARTY' THEN
              l_rent := c_year.estimated_amount * (-1);
            ELSE
              l_rent := c_year.estimated_amount;
            END IF;

            -- determine payment amount if the remaining months are less then 12
            l_count := round(months_between(l_end_date, l_start_date), 0);

            IF l_count < 12 THEN
              l_amount  := l_payment / 12;
              l_payment := trunc((l_amount * l_count), 2);
            END IF;

            IF c_year.months = 'January' THEN
              UPDATE xxcus.xxcuspn_lease_payments_tbl
                 SET january = l_rent
               WHERE lease_id = c_year.lease_id
                 AND vendor_id = c_year.vendor_id
                 AND year_ca = c_year.year_ca
                 AND percentage = c_year.percentage
                 AND frequency_code = c_year.frequency_code
                 AND payment_purpose = c_payment.payment_purpose
                 AND payment_term_type = c_year.payment_term_type;
              --END IF;

            ELSIF c_year.months = 'February' THEN
              UPDATE xxcus.xxcuspn_lease_payments_tbl
                 SET february = l_rent
               WHERE lease_id = c_year.lease_id
                 AND vendor_id = c_year.vendor_id
                 AND year_ca = c_year.year_ca
                 AND percentage = c_year.percentage
                 AND frequency_code = c_year.frequency_code
                 AND payment_purpose = c_payment.payment_purpose
                 AND payment_term_type = c_year.payment_term_type;
              --END IF;

            ELSIF c_year.months = 'March' THEN
              UPDATE xxcus.xxcuspn_lease_payments_tbl
                 SET march = l_rent
               WHERE lease_id = c_year.lease_id
                 AND vendor_id = c_year.vendor_id
                 AND year_ca = c_year.year_ca
                 AND percentage = c_year.percentage
                 AND frequency_code = c_year.frequency_code
                 AND payment_purpose = c_payment.payment_purpose
                 AND payment_term_type = c_year.payment_term_type;
              --END IF;

            ELSIF c_year.months = 'April' THEN
              UPDATE xxcus.xxcuspn_lease_payments_tbl
                 SET april = l_rent
               WHERE lease_id = c_year.lease_id
                 AND vendor_id = c_year.vendor_id
                 AND year_ca = c_year.year_ca
                 AND percentage = c_year.percentage
                 AND frequency_code = c_year.frequency_code
                 AND payment_purpose = c_payment.payment_purpose
                 AND payment_term_type = c_year.payment_term_type;
              --END IF;

            ELSIF c_year.months = 'May' THEN
              UPDATE xxcus.xxcuspn_lease_payments_tbl
                 SET may = l_rent
               WHERE lease_id = c_year.lease_id
                 AND vendor_id = c_year.vendor_id
                 AND year_ca = c_year.year_ca
                 AND percentage = c_year.percentage
                 AND frequency_code = c_year.frequency_code
                 AND payment_purpose = c_payment.payment_purpose
                 AND payment_term_type = c_year.payment_term_type;
              --END IF;

            ELSIF c_year.months = 'June' THEN
              UPDATE xxcus.xxcuspn_lease_payments_tbl
                 SET july = l_rent
               WHERE lease_id = c_year.lease_id
                 AND vendor_id = c_year.vendor_id
                 AND year_ca = c_year.year_ca
                 AND percentage = c_year.percentage
                 AND frequency_code = c_year.frequency_code
                 AND payment_purpose = c_payment.payment_purpose
                 AND payment_term_type = c_year.payment_term_type;
              --END IF;

            ELSIF c_year.months = 'July' THEN
              UPDATE xxcus.xxcuspn_lease_payments_tbl
                 SET july = l_rent
               WHERE lease_id = c_year.lease_id
                 AND vendor_id = c_year.vendor_id
                 AND year_ca = c_year.year_ca
                 AND percentage = c_year.percentage
                 AND frequency_code = c_year.frequency_code
                 AND payment_purpose = c_payment.payment_purpose
                 AND payment_term_type = c_year.payment_term_type;
              --END IF;

            ELSIF c_year.months = 'August' THEN
              UPDATE xxcus.xxcuspn_lease_payments_tbl
                 SET august = l_rent
               WHERE lease_id = c_year.lease_id
                 AND vendor_id = c_year.vendor_id
                 AND year_ca = c_year.year_ca
                 AND percentage = c_year.percentage
                 AND frequency_code = c_year.frequency_code
                 AND payment_purpose = c_payment.payment_purpose
                 AND payment_term_type = c_year.payment_term_type;
              --END IF;

            ELSIF c_year.months = 'September' THEN
              UPDATE xxcus.xxcuspn_lease_payments_tbl
                 SET september = l_rent
               WHERE lease_id = c_year.lease_id
                 AND vendor_id = c_year.vendor_id
                 AND year_ca = c_year.year_ca
                 AND percentage = c_year.percentage
                 AND frequency_code = c_year.frequency_code
                 AND payment_purpose = c_payment.payment_purpose
                 AND payment_term_type = c_year.payment_term_type;
              --END IF;

            ELSIF c_year.months = 'October' THEN
              UPDATE xxcus.xxcuspn_lease_payments_tbl
                 SET october = l_rent
               WHERE lease_id = c_year.lease_id
                 AND vendor_id = c_year.vendor_id
                 AND year_ca = c_year.year_ca
                 AND percentage = c_year.percentage
                 AND frequency_code = c_year.frequency_code
                 AND payment_purpose = c_payment.payment_purpose
                 AND payment_term_type = c_year.payment_term_type;
              --END IF;

            ELSIF c_year.months = 'November' THEN
              UPDATE xxcus.xxcuspn_lease_payments_tbl
                 SET november = l_rent
               WHERE lease_id = c_year.lease_id
                 AND vendor_id = c_year.vendor_id
                 AND year_ca = c_year.year_ca
                 AND percentage = c_year.percentage
                 AND frequency_code = c_year.frequency_code
                 AND payment_purpose = c_payment.payment_purpose
                 AND payment_term_type = c_year.payment_term_type;
              --END IF;

            ELSIF c_year.months = 'December' THEN
              UPDATE xxcus.xxcuspn_lease_payments_tbl
                 SET december = l_rent
               WHERE lease_id = c_year.lease_id
                 AND vendor_id = c_year.vendor_id
                 AND year_ca = c_year.year_ca
                 AND percentage = c_year.percentage
                 AND frequency_code = c_year.frequency_code
                 AND payment_purpose = c_payment.payment_purpose
                 AND payment_term_type = c_year.payment_term_type;
            END IF;
            COMMIT;
          END LOOP; -- year loop

          -- QUARTERLY PAYMENT
        ELSIF c_payment.frequency_code = 'Quarterly' THEN
          FOR c_qtr IN (SELECT lease_id
                              ,year_ca
                              ,percentage
                               --,frequency_code
                              ,flv.meaning frequency_code
                              ,TRIM(to_char(c_payment.start_date, 'Month')) months
                              ,estimated_amount
                              ,payment_term_type
                              ,start_date
                              ,end_date
                          FROM xxcus.xxcuspn_lease_payments_tbl
                              ,apps.fnd_lookups flv
                         WHERE flv.meaning = frequency_code
                           AND lease_id = c_payment.lease_id
                           AND payment_term_type =
                               c_payment.payment_term_type_code
                           AND flv.meaning = c_payment.frequency_code
                           AND flv.lookup_type = 'PN_PAYMENT_FREQUENCY_TYPE')
          LOOP

            IF lease_rec.lease_class_code = 'SUB_LEASE'
               OR lease_rec.lease_class_code = 'THIRD_PARTY' THEN
              l_payment := c_qtr.estimated_amount * (-1);
            ELSE
              l_payment := c_qtr.estimated_amount;
            END IF;
            l_start_date := c_qtr.start_date;
            l_end_date   := c_qtr.end_date;

            WHILE l_start_date <= l_end_date
            LOOP
              l_month := extract(MONTH FROM l_start_date);
              l_year  := extract(YEAR FROM l_start_date);

              -- determine payment amount if the remaining months are less then 3
              l_count := round(months_between(l_end_date, l_start_date), 0);

              IF l_count < 3 THEN
                l_amount  := l_payment / 3;
                l_payment := trunc((l_amount * l_count), 2);
              END IF;

              IF l_month = 1 THEN
                UPDATE xxcus.xxcuspn_lease_payments_tbl
                   SET january = l_payment
                 WHERE lease_id = c_qtr.lease_id
                   AND year_ca = l_year
                   AND percentage = c_qtr.percentage
                   AND frequency_code = c_qtr.frequency_code
                   AND payment_purpose = c_payment.payment_purpose
                   AND payment_term_type = c_qtr.payment_term_type;
                --END IF;

              ELSIF l_month = 2 THEN
                UPDATE xxcus.xxcuspn_lease_payments_tbl
                   SET february = l_payment
                 WHERE lease_id = c_qtr.lease_id
                   AND year_ca = l_year
                   AND percentage = c_qtr.percentage
                   AND frequency_code = c_qtr.frequency_code
                   AND payment_purpose = c_payment.payment_purpose
                   AND payment_term_type = c_qtr.payment_term_type;
                --END IF;

              ELSIF l_month = 3 THEN
                UPDATE xxcus.xxcuspn_lease_payments_tbl
                   SET march = l_payment
                 WHERE lease_id = c_qtr.lease_id
                   AND year_ca = l_year
                   AND percentage = c_qtr.percentage
                   AND frequency_code = c_qtr.frequency_code
                   AND payment_purpose = c_payment.payment_purpose
                   AND payment_term_type = c_qtr.payment_term_type;
                --END IF;

              ELSIF l_month = 4 THEN
                UPDATE xxcus.xxcuspn_lease_payments_tbl
                   SET april = l_payment
                 WHERE lease_id = c_qtr.lease_id
                   AND year_ca = l_year
                   AND percentage = c_qtr.percentage
                   AND frequency_code = c_qtr.frequency_code
                   AND payment_purpose = c_payment.payment_purpose
                   AND payment_term_type = c_qtr.payment_term_type;
                --END IF;

              ELSIF l_month = 5 THEN
                UPDATE xxcus.xxcuspn_lease_payments_tbl
                   SET may = l_payment
                 WHERE lease_id = c_qtr.lease_id
                   AND year_ca = l_year
                   AND percentage = c_qtr.percentage
                   AND frequency_code = c_qtr.frequency_code
                   AND payment_purpose = c_payment.payment_purpose
                   AND payment_term_type = c_qtr.payment_term_type;
                --END IF;

              ELSIF l_month = 6 THEN
                UPDATE xxcus.xxcuspn_lease_payments_tbl
                   SET july = l_payment
                 WHERE lease_id = c_qtr.lease_id
                   AND year_ca = l_year
                   AND percentage = c_qtr.percentage
                   AND frequency_code = c_qtr.frequency_code
                   AND payment_purpose = c_payment.payment_purpose
                   AND payment_term_type = c_qtr.payment_term_type;
                --END IF;

              ELSIF l_month = 7 THEN
                UPDATE xxcus.xxcuspn_lease_payments_tbl
                   SET july = l_payment
                 WHERE lease_id = c_qtr.lease_id
                   AND year_ca = l_year
                   AND percentage = c_qtr.percentage
                   AND frequency_code = c_qtr.frequency_code
                   AND payment_purpose = c_payment.payment_purpose
                   AND payment_term_type = c_qtr.payment_term_type;
                --END IF;

              ELSIF l_month = 8 THEN
                UPDATE xxcus.xxcuspn_lease_payments_tbl
                   SET august = l_payment
                 WHERE lease_id = c_qtr.lease_id
                   AND year_ca = l_year
                   AND percentage = c_qtr.percentage
                   AND frequency_code = c_qtr.frequency_code
                   AND payment_purpose = c_payment.payment_purpose
                   AND payment_term_type = c_qtr.payment_term_type;
                --END IF;

              ELSIF l_month = 9 THEN
                UPDATE xxcus.xxcuspn_lease_payments_tbl
                   SET september = l_payment
                 WHERE lease_id = c_qtr.lease_id
                   AND year_ca = l_year
                   AND percentage = c_qtr.percentage
                   AND frequency_code = c_qtr.frequency_code
                   AND payment_purpose = c_payment.payment_purpose
                   AND payment_term_type = c_qtr.payment_term_type;
                --END IF;

              ELSIF l_month = 10 THEN
                UPDATE xxcus.xxcuspn_lease_payments_tbl
                   SET october = l_payment
                 WHERE lease_id = c_qtr.lease_id
                   AND year_ca = l_year
                   AND percentage = c_qtr.percentage
                   AND frequency_code = c_qtr.frequency_code
                   AND payment_purpose = c_payment.payment_purpose
                   AND payment_term_type = c_qtr.payment_term_type;
                --END IF;

              ELSIF l_month = 11 THEN
                UPDATE xxcus.xxcuspn_lease_payments_tbl
                   SET november = l_payment
                 WHERE lease_id = c_qtr.lease_id
                   AND year_ca = l_year
                   AND percentage = c_qtr.percentage
                   AND frequency_code = c_qtr.frequency_code
                   AND payment_purpose = c_payment.payment_purpose
                   AND payment_term_type = c_qtr.payment_term_type;
                --END IF;

              ELSIF l_month = 12 THEN
                UPDATE xxcus.xxcuspn_lease_payments_tbl
                   SET december = l_payment
                 WHERE lease_id = c_qtr.lease_id
                   AND year_ca = l_year
                   AND percentage = c_qtr.percentage
                   AND frequency_code = c_qtr.frequency_code
                   AND payment_purpose = c_payment.payment_purpose
                   AND payment_term_type = c_qtr.payment_term_type;
              END IF;
              COMMIT;
              l_start_date := add_months(l_start_date, 3);
            END LOOP; -- while c_qtr
          END LOOP; -- quarter loop

        ELSIF c_payment.frequency_code = 'Semi-Annually' THEN
          FOR c_sa IN (SELECT lease_id
                             ,year_ca
                             ,percentage
                              --,frequency_code
                             ,flv.meaning frequency_code
                             ,TRIM(to_char(c_payment.start_date, 'Month')) months
                             ,estimated_amount
                             ,payment_term_type
                             ,start_date
                             ,end_date
                         FROM xxcus.xxcuspn_lease_payments_tbl
                             ,apps.fnd_lookups flv
                        WHERE flv.meaning = frequency_code
                          AND lease_id = c_payment.lease_id
                          AND payment_term_type =
                              c_payment.payment_term_type_code
                          AND flv.meaning = c_payment.frequency_code
                          AND flv.lookup_type = 'PN_PAYMENT_FREQUENCY_TYPE')
          LOOP

            IF lease_rec.lease_class_code = 'SUB_LEASE'
               OR lease_rec.lease_class_code = 'THIRD_PARTY' THEN
              l_payment := c_sa.estimated_amount * (-1);
            ELSE
              l_payment := c_sa.estimated_amount;
            END IF;
            l_start_date := c_sa.start_date;
            l_end_date   := c_sa.end_date;

            WHILE l_start_date <= l_end_date
            LOOP
              l_month := extract(MONTH FROM l_start_date);
              l_year  := extract(YEAR FROM l_start_date);

              -- determine payment amount if the remaining months are less then 3
              l_count := round(months_between(l_end_date, l_start_date), 0);

              IF l_count < 6 THEN
                l_amount  := l_payment / 6;
                l_payment := trunc((l_amount * l_count), 2);
              END IF;

              IF l_month = 1 THEN
                UPDATE xxcus.xxcuspn_lease_payments_tbl
                   SET january = l_payment
                 WHERE lease_id = c_sa.lease_id
                   AND year_ca = l_year
                   AND percentage = c_sa.percentage
                   AND frequency_code = c_sa.frequency_code
                   AND payment_purpose = c_payment.payment_purpose
                   AND payment_term_type = c_sa.payment_term_type;
                --END IF;

              ELSIF l_month = 2 THEN
                UPDATE xxcus.xxcuspn_lease_payments_tbl
                   SET february = l_payment
                 WHERE lease_id = c_sa.lease_id
                   AND year_ca = l_year
                   AND percentage = c_sa.percentage
                   AND frequency_code = c_sa.frequency_code
                   AND payment_purpose = c_payment.payment_purpose
                   AND payment_term_type = c_sa.payment_term_type;
                --END IF;

              ELSIF l_month = 3 THEN
                UPDATE xxcus.xxcuspn_lease_payments_tbl
                   SET march = l_payment
                 WHERE lease_id = c_sa.lease_id
                   AND year_ca = l_year
                   AND percentage = c_sa.percentage
                   AND frequency_code = c_sa.frequency_code
                   AND payment_purpose = c_payment.payment_purpose
                   AND payment_term_type = c_sa.payment_term_type;
                --END IF;

              ELSIF l_month = 4 THEN
                UPDATE xxcus.xxcuspn_lease_payments_tbl
                   SET april = l_payment
                 WHERE lease_id = c_sa.lease_id
                   AND year_ca = l_year
                   AND percentage = c_sa.percentage
                   AND frequency_code = c_sa.frequency_code
                   AND payment_purpose = c_payment.payment_purpose
                   AND payment_term_type = c_sa.payment_term_type;
                --END IF;

              ELSIF l_month = 5 THEN
                UPDATE xxcus.xxcuspn_lease_payments_tbl
                   SET may = l_payment
                 WHERE lease_id = c_sa.lease_id
                   AND year_ca = l_year
                   AND percentage = c_sa.percentage
                   AND frequency_code = c_sa.frequency_code
                   AND payment_purpose = c_payment.payment_purpose
                   AND payment_term_type = c_sa.payment_term_type;
                --END IF;

              ELSIF l_month = 6 THEN
                UPDATE xxcus.xxcuspn_lease_payments_tbl
                   SET july = l_payment
                 WHERE lease_id = c_sa.lease_id
                   AND year_ca = l_year
                   AND percentage = c_sa.percentage
                   AND frequency_code = c_sa.frequency_code
                   AND payment_purpose = c_payment.payment_purpose
                   AND payment_term_type = c_sa.payment_term_type;
                --END IF;

              ELSIF l_month = 7 THEN
                UPDATE xxcus.xxcuspn_lease_payments_tbl
                   SET july = l_payment
                 WHERE lease_id = c_sa.lease_id
                   AND year_ca = l_year
                   AND percentage = c_sa.percentage
                   AND frequency_code = c_sa.frequency_code
                   AND payment_purpose = c_payment.payment_purpose
                   AND payment_term_type = c_sa.payment_term_type;
                --END IF;

              ELSIF l_month = 8 THEN
                UPDATE xxcus.xxcuspn_lease_payments_tbl
                   SET august = l_payment
                 WHERE lease_id = c_sa.lease_id
                   AND year_ca = l_year
                   AND percentage = c_sa.percentage
                   AND frequency_code = c_sa.frequency_code
                   AND payment_purpose = c_payment.payment_purpose
                   AND payment_term_type = c_sa.payment_term_type;
                --END IF;

              ELSIF l_month = 9 THEN
                UPDATE xxcus.xxcuspn_lease_payments_tbl
                   SET september = l_payment
                 WHERE lease_id = c_sa.lease_id
                   AND year_ca = l_year
                   AND percentage = c_sa.percentage
                   AND frequency_code = c_sa.frequency_code
                   AND payment_purpose = c_payment.payment_purpose
                   AND payment_term_type = c_sa.payment_term_type;
                --END IF;

              ELSIF l_month = 10 THEN
                UPDATE xxcus.xxcuspn_lease_payments_tbl
                   SET october = l_payment
                 WHERE lease_id = c_sa.lease_id
                   AND year_ca = l_year
                   AND percentage = c_sa.percentage
                   AND frequency_code = c_sa.frequency_code
                   AND payment_purpose = c_payment.payment_purpose
                   AND payment_term_type = c_sa.payment_term_type;
                --END IF;

              ELSIF l_month = 11 THEN
                UPDATE xxcus.xxcuspn_lease_payments_tbl
                   SET november = l_payment
                 WHERE lease_id = c_sa.lease_id
                   AND year_ca = l_year
                   AND percentage = c_sa.percentage
                   AND frequency_code = c_sa.frequency_code
                   AND payment_purpose = c_payment.payment_purpose
                   AND payment_term_type = c_sa.payment_term_type;
                --END IF;

              ELSIF l_month = 12 THEN
                UPDATE xxcus.xxcuspn_lease_payments_tbl
                   SET december = l_payment
                 WHERE lease_id = c_sa.lease_id
                   AND year_ca = l_year
                   AND percentage = c_sa.percentage
                   AND frequency_code = c_sa.frequency_code
                   AND payment_purpose = c_payment.payment_purpose
                   AND payment_term_type = c_sa.payment_term_type;
              END IF;
              COMMIT;
              l_start_date := to_char(add_months(l_start_date, 6),
                                      'DD-Mon-YYYY');
            END LOOP; -- while c_sa
          END LOOP; -- semi annual loop

          --MONTHLY PAYMENT

        ELSE

          l_base_rent := c_payment.estimated_amount *
                         (c_payment.percentage / 100);

          l_init       := 1;
          l_year_start := extract(YEAR FROM c_payment.start_date);
          -- get lease end year
          IF lease_rec.lease_status = 'TER' THEN
            l_year_end := extract(YEAR FROM
                                  lease_rec.lease_termination_date);
          ELSE
            IF c_payment.end_date = '31-Dec-4712'
               AND lease_rec.lease_termination_date = '31-Dec-4712' THEN
              l_year_end := extract(YEAR FROM
                                    add_months(trunc(SYSDATE), l_extend)); -- EXTENSION.
            ELSIF c_payment.end_date = '31-Dec-4712'
                  AND lease_rec.lease_termination_date <> '31-Dec-4712' THEN
              l_year_end := extract(YEAR FROM
                                    lease_rec.lease_termination_date);
            ELSE
              l_year_end := extract(YEAR FROM c_payment.end_date);
            END IF;
          END IF;

          l_start_date := c_payment.start_date;

          -- l_end_date
          IF c_payment.end_date = '31-Dec-4712'
             AND lease_rec.lease_termination_date = '31-Dec-4712' THEN
            l_end_date := add_months(last_day(trunc(SYSDATE, 'month')),
                                     l_extend);
          ELSIF c_payment.end_date = '31-Dec-4712'
                AND lease_rec.lease_termination_date <> '31-Dec-4712' THEN
            l_end_date := lease_rec.lease_termination_date;
          ELSE
            l_end_date := c_payment.end_date;
          END IF;

          --- get number of months
          SELECT round(months_between(l_end_date, l_start_date) + 1)
            INTO l_duration
            FROM dual;

          FOR c_cur IN (SELECT to_char(add_months(start_date, LEVEL - 1),
                                       'fmMonth') months
                          FROM (SELECT l_start_date start_date
                                      ,l_end_date   end_date
                                  FROM pn.pn_payment_terms_all pt
                                 WHERE payment_term_id =
                                       c_payment.payment_term_id)
                        CONNECT BY LEVEL <=
                                   months_between(trunc(l_end_date, 'MM'),
                                                  trunc(l_start_date, 'MM')) + 1)
          LOOP

            -- Calculate month rent amount for first and last month
            -- rent per day
            IF l_init = 1 THEN
              SELECT extract(DAY FROM last_day(start_date))
                INTO l_days
                FROM pn.pn_payment_terms_all pt
               WHERE payment_term_id = c_payment.payment_term_id;
              l_perdiem := trunc((l_base_rent / l_days), 2);

            ELSIF l_init = l_duration THEN
              SELECT extract(DAY FROM last_day(end_date))
                INTO l_days
                FROM pn.pn_payment_terms_all pt
               WHERE payment_term_id = c_payment.payment_term_id;
              l_perdiem := trunc((l_base_rent / l_days), 2);

            ELSE
              NULL;
            END IF;

            -- first month rent

            IF l_init = 1 THEN
              IF trunc(l_start_date, 'month') = l_start_date THEN
                l_first_rent := l_base_rent;
              ELSE
                l_first_rent := trunc(((l_days -
                                      extract(DAY FROM l_start_date) + 1) *
                                      l_perdiem) + l_last_rent, 2);

              END IF;
            END IF;
            -- last month rent
            IF l_init = l_duration THEN
              IF last_day(l_end_date) = l_end_date THEN
                l_last_rent := l_base_rent;
              ELSE
                l_last_rent := trunc(((extract(DAY FROM l_end_date)) *
                                     l_perdiem), 2);
              END IF;
            END IF;
            -- Start insert rent
            IF l_init = 1
               AND c_payment.frequency_code <> 'One Time' THEN
              l_rent := trunc(l_first_rent, 2);
            ELSIF l_init = l_duration
                  AND c_payment.frequency_code <> 'One Time' THEN
              l_rent := trunc(l_last_rent, 2);
            ELSE
              l_rent := trunc(l_base_rent, 2);
            END IF;

            IF lease_rec.lease_class_code = 'SUB_LEASE'
               OR lease_rec.lease_class_code = 'THIRD_PARTY' THEN
              l_rent := l_rent * (-1);
            END IF;

            IF c_cur.months = 'January' THEN
              UPDATE xxcus.xxcuspn_lease_payments_tbl
                 SET january = l_rent
               WHERE lease_id = c_payment.lease_id
                 AND year_ca = l_year_start
                 AND percentage = c_payment.percentage
                 AND frequency_code = c_payment.frequency_code
                 AND payment_purpose = c_payment.payment_purpose
                 AND payment_term_type = c_payment.payment_term_type_code
                 AND nvl(vendor_id, 0) = nvl(c_payment.vendor_id, 0);
              --END IF;

            ELSIF c_cur.months = 'February' THEN
              UPDATE xxcus.xxcuspn_lease_payments_tbl
                 SET february = l_rent
               WHERE lease_id = c_payment.lease_id
                 AND year_ca = l_year_start
                 AND percentage = c_payment.percentage
                 AND frequency_code = c_payment.frequency_code
                 AND payment_purpose = c_payment.payment_purpose
                 AND payment_term_type = c_payment.payment_term_type_code
                 AND nvl(vendor_id, 0) = nvl(c_payment.vendor_id, 0);
              --END IF;

            ELSIF c_cur.months = 'March' THEN
              UPDATE xxcus.xxcuspn_lease_payments_tbl
                 SET march = l_rent
               WHERE lease_id = c_payment.lease_id
                 AND year_ca = l_year_start
                 AND percentage = c_payment.percentage
                 AND frequency_code = c_payment.frequency_code
                 AND payment_purpose = c_payment.payment_purpose
                 AND payment_term_type = c_payment.payment_term_type_code
                 AND nvl(vendor_id, 0) = nvl(c_payment.vendor_id, 0);
              --END IF;

            ELSIF c_cur.months = 'April' THEN
              UPDATE xxcus.xxcuspn_lease_payments_tbl
                 SET april = l_rent
               WHERE lease_id = c_payment.lease_id
                 AND year_ca = l_year_start
                 AND percentage = c_payment.percentage
                 AND frequency_code = c_payment.frequency_code
                 AND payment_purpose = c_payment.payment_purpose
                 AND payment_term_type = c_payment.payment_term_type_code
                 AND nvl(vendor_id, 0) = nvl(c_payment.vendor_id, 0);
              --END IF;

            ELSIF c_cur.months = 'May' THEN
              UPDATE xxcus.xxcuspn_lease_payments_tbl
                 SET may = l_rent
               WHERE lease_id = c_payment.lease_id
                 AND year_ca = l_year_start
                 AND percentage = c_payment.percentage
                 AND frequency_code = c_payment.frequency_code
                 AND payment_purpose = c_payment.payment_purpose
                 AND payment_term_type = c_payment.payment_term_type_code
                 AND nvl(vendor_id, 0) = nvl(c_payment.vendor_id, 0);
              --END IF;

            ELSIF c_cur.months = 'June' THEN
              UPDATE xxcus.xxcuspn_lease_payments_tbl
                 SET june = l_rent
               WHERE lease_id = c_payment.lease_id
                 AND year_ca = l_year_start
                 AND percentage = c_payment.percentage
                 AND frequency_code = c_payment.frequency_code
                 AND payment_purpose = c_payment.payment_purpose
                 AND payment_term_type = c_payment.payment_term_type_code
                 AND nvl(vendor_id, 0) = nvl(c_payment.vendor_id, 0);
              --END IF;

            ELSIF c_cur.months = 'July' THEN
              UPDATE xxcus.xxcuspn_lease_payments_tbl
                 SET july = l_rent
               WHERE lease_id = c_payment.lease_id
                 AND year_ca = l_year_start
                 AND percentage = c_payment.percentage
                 AND frequency_code = c_payment.frequency_code
                 AND payment_purpose = c_payment.payment_purpose
                 AND payment_term_type = c_payment.payment_term_type_code
                 AND nvl(vendor_id, 0) = nvl(c_payment.vendor_id, 0);
              --END IF;

            ELSIF c_cur.months = 'August' THEN
              UPDATE xxcus.xxcuspn_lease_payments_tbl
                 SET august = l_rent
               WHERE lease_id = c_payment.lease_id
                 AND year_ca = l_year_start
                 AND percentage = c_payment.percentage
                 AND frequency_code = c_payment.frequency_code
                 AND payment_purpose = c_payment.payment_purpose
                 AND payment_term_type = c_payment.payment_term_type_code
                 AND nvl(vendor_id, 0) = nvl(c_payment.vendor_id, 0);
              --END IF;

            ELSIF c_cur.months = 'September' THEN
              UPDATE xxcus.xxcuspn_lease_payments_tbl
                 SET september = l_rent
               WHERE lease_id = c_payment.lease_id
                 AND year_ca = l_year_start
                 AND percentage = c_payment.percentage
                 AND frequency_code = c_payment.frequency_code
                 AND payment_purpose = c_payment.payment_purpose
                 AND payment_term_type = c_payment.payment_term_type_code
                 AND nvl(vendor_id, 0) = nvl(c_payment.vendor_id, 0);
              --END IF;

            ELSIF c_cur.months = 'October' THEN
              UPDATE xxcus.xxcuspn_lease_payments_tbl
                 SET october = l_rent
               WHERE lease_id = c_payment.lease_id
                 AND year_ca = l_year_start
                 AND percentage = c_payment.percentage
                 AND frequency_code = c_payment.frequency_code
                 AND payment_purpose = c_payment.payment_purpose
                 AND payment_term_type = c_payment.payment_term_type_code
                 AND nvl(vendor_id, 0) = nvl(c_payment.vendor_id, 0);
              --END IF;

            ELSIF c_cur.months = 'November' THEN
              UPDATE xxcus.xxcuspn_lease_payments_tbl
                 SET november = l_rent
               WHERE lease_id = c_payment.lease_id
                 AND year_ca = l_year_start
                 AND percentage = c_payment.percentage
                 AND frequency_code = c_payment.frequency_code
                 AND payment_purpose = c_payment.payment_purpose
                 AND payment_term_type = c_payment.payment_term_type_code
                 AND nvl(vendor_id, 0) = nvl(c_payment.vendor_id, 0);
              --END IF;

            ELSIF c_cur.months = 'December' THEN
              UPDATE xxcus.xxcuspn_lease_payments_tbl
                 SET december = l_rent
               WHERE lease_id = c_payment.lease_id
                 AND year_ca = l_year_start
                 AND percentage = c_payment.percentage
                 AND frequency_code = c_payment.frequency_code
                 AND payment_purpose = c_payment.payment_purpose
                 AND payment_term_type = c_payment.payment_term_type_code
                 AND nvl(vendor_id, 0) = nvl(c_payment.vendor_id, 0);
            END IF;
            COMMIT;

            IF c_cur.months = 'December' THEN
              l_year_start := l_year_start + 1;
            END IF;
            l_init := l_init + 1;
          END LOOP; -- month iteration
        END IF; --c_payment.frequency_code
      END LOOP; --lease
    END LOOP; --lease

    -- Calculate annual amount for each row

    FOR c_rec IN (SELECT *
                    FROM xxcus.xxcuspn_lease_payments_tbl lpt)
    LOOP

      SELECT nvl(lp.january, 0) + nvl(lp.february, 0) + nvl(lp.march, 0) +
             nvl(lp.april, 0) + nvl(lp.may, 0) + nvl(lp.june, 0) +
             nvl(lp.july, 0) + nvl(lp.august, 0) + nvl(lp.september, 0) +
             nvl(lp.october, 0) + nvl(lp.november, 0) + nvl(lp.december, 0)
        INTO l_annual_amt
        FROM xxcus.xxcuspn_lease_payments_tbl lp
       WHERE lp.line_id = c_rec.line_id;

      --   dbms_output.put_line('ANNUAL AMOUNT: ' || l_annual_amt);

      UPDATE xxcus.xxcuspn_lease_payments_tbl
         SET annual_total = l_annual_amt
       WHERE line_id = c_rec.line_id;
      COMMIT;
    END LOOP; -- annual amount
  --
  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;

      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := 'Error in program ' || l_procedure_name || ' Error: ' ||
                    l_sec;
      l_err_msg  := l_err_msg || ' ERROR ' || substr(SQLERRM, 1, 2000);

      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode := l_err_code;
      errbuf  := l_err_msg;

      xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_procedure_name,
                                           p_calling => l_sec,
                                           p_request_id => fnd_global.conc_request_id,
                                           p_ora_error_msg => SQLERRM,
                                           p_error_desc => 'Error running xxcuspn_lease_pmt_report_pkg.create_pmt_detail with PROGRAM exeception',
                                           p_distribution_list => l_distro_list,
                                           p_module => 'PN');

    WHEN OTHERS THEN

      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := l_sec || l_err_msg || ' ERROR ' || SQLCODE ||
                    substr(SQLERRM, 1, 2000);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode := l_err_code;
      errbuf  := l_err_msg;

      xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_procedure_name,
                                           p_calling => l_sec,
                                           p_request_id => fnd_global.conc_request_id,
                                           p_ora_error_msg => SQLERRM,
                                           p_error_desc => 'Error running xxcuspn_lease_pmt_report_pkg.create_pmt_detail with OTHERS Exception',
                                           p_distribution_list => l_distro_list,
                                           p_module => 'PN');

  END create_pmt_detail;

  /*  ===========================================================================
    Procedure Name: main
    PURPOSE: execute procedures to populate denormalized lease data.
    HISTORY
  ===========================================================================
    VERSION DATE          AUTHOR(S)          DESCRIPTION
    ------- -----------   ---------------    ------------------------------
    1.0     03-Jun-2013   Luong Vu           Created this package.
    1.1     29-Jan-2014   Balaguru Seshadri Truncate old data before the extract
                                            ESMS  237440.
  ===========================================================================*/

  PROCEDURE main(errbuf  OUT VARCHAR2
                ,retcode OUT VARCHAR2) IS

    l_procedure_name VARCHAR2(75) DEFAULT 'main';

  BEGIN
    --
    mo_global.set_policy_context('S', 163);
    print_log('After set operating unit to 163');
    --
    execute immediate 'truncate table xxcus.xxcuspn_lease_headers_tbl';
    print_log('Removed old data from table xxcus.xxcuspn_lease_headers_tbl ');
    --
    execute immediate 'truncate table xxcus.xxcuspn_lease_payments_tbl';
    print_log('Removed old data from table xxcus.xxcuspn_lease_payments_tbl');
    --
    xxcuspn_lease_pmt_report_pkg.create_pmt_headers
         (
            errbuf  =>xxcuspn_lease_pmt_report_pkg.l_err_msg
           ,retcode =>xxcuspn_lease_pmt_report_pkg.l_err_code
         );
    --
    print_log('After calling xxcuspn_lease_pmt_report_pkg.create_pmt_headers');
    --
    xxcuspn_lease_pmt_report_pkg.create_pmt_detail
         (
            errbuf  =>xxcuspn_lease_pmt_report_pkg.l_err_msg
           ,retcode =>xxcuspn_lease_pmt_report_pkg.l_err_code
         );
    --
    print_log('After calling xxcuspn_lease_pmt_report_pkg.create_pmt_detail');
    --
  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;
      dbms_output.put_line('@main when program error, msg ='||l_sec);
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := 'Error in program ' || l_procedure_name || ' Error: ' ||
                    l_sec;
      l_err_msg  := l_err_msg || ' ERROR ' || substr(SQLERRM, 1, 2000);

      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode := l_err_code;
      errbuf  := l_err_msg;

      xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_procedure_name,
                                           p_calling => l_sec,
                                           p_request_id => fnd_global.conc_request_id,
                                           p_ora_error_msg => SQLERRM,
                                           p_error_desc => 'Error running xxcuspn_lease_pmt_report_pkg.main with PROGRAM exeception',
                                           p_distribution_list => l_distro_list,
                                           p_module => 'PN');

    WHEN OTHERS THEN
      dbms_output.put_line('@main when others, msg ='||l_sec);
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := l_sec || l_err_msg || ' ERROR ' || SQLCODE ||
                    substr(SQLERRM, 1, 2000);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode := l_err_code;
      errbuf  := l_err_msg;

      xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_procedure_name,
                                           p_calling => l_sec,
                                           p_request_id => fnd_global.conc_request_id,
                                           p_ora_error_msg => SQLERRM,
                                           p_error_desc => 'Error running xxcuspn_lease_pmt_report_pkg.main with OTHERS Exception',
                                           p_distribution_list => l_distro_list,
                                           p_module => 'PN');

  END;


  /*===========================================================================
    Function Name: data_ref_cur
    PURPOSE: function to create select query and return data as sys_refcursor
             for EIS report
    HISTORY
  ===========================================================================
    VERSION DATE          AUTHOR(S)          DESCRIPTION
    ------- -----------   ---------------    ------------------------------
    1.0     03-Jun-2013   Luong Vu           Created this package.
  ===========================================================================*/

  FUNCTION rolling12_ref_cur(p_month IN VARCHAR2
                            ,p_year  IN NUMBER
                            ,retcode OUT NUMBER
                            ,errbuf  OUT VARCHAR2) RETURN SYS_REFCURSOR IS
    l_rent_data xxcuspn_rent_tbl_ntt := xxcuspn_rent_tbl_ntt();
    cur         SYS_REFCURSOR;
    l_query     VARCHAR2(4000);
    l_month     VARCHAR2(25);
    l_count     NUMBER;
    l_year      NUMBER;

    l_rer_id    VARCHAR(50);
    l_january   NUMBER;
    l_february  VARCHAR2(25);
    l_march     VARCHAR2(25);
    l_april     VARCHAR2(25);
    l_may       VARCHAR2(25);
    l_june      VARCHAR2(25);
    l_july      VARCHAR2(25);
    l_august    VARCHAR2(25);
    l_september VARCHAR2(25);
    l_october   VARCHAR2(25);
    l_november  VARCHAR2(25);
    l_december  VARCHAR2(25);

    l_procedure_name VARCHAR2(75) DEFAULT 'rolling12_ref_cur';

  BEGIN

    l_sec := 'create query base on input';

    IF lower(p_month) = 'january' THEN

      l_query := 'select LOB, RER_ID, PRIME_LOC, PRIME_ADDRESS, PRIME_CITY, PRIME_ST_PRV, POSTAL_CODE
                 ,RER_TYPE, COMM_DATE, EXP_DATE, RER_STATUS, ORACLE_ID, FRU_ID, BR_#,PAYMENT_TERM_TYPE, YEAR
                 ,JANUARY, FEBRUARY, MARCH, APRIL, MAY ,JUNE, JULY, AUGUST, SEPTEMBER, OCTOBER
                 ,NOVEMBER, DECEMBER, vendor_code, vendor_name,  frequency,  gl_code, annual_total, term_comments' || --Ver 1.2
                 ' from apps.xxcuspn_lease_rent_vw where YEAR = ' || p_year ||
                 'ORDER BY RER_id';

    ELSIF lower(p_month) = 'february' THEN
      l_year  := p_year + 1;
      l_query := 'select LOB, RER_ID, PRIME_LOC, PRIME_ADDRESS, PRIME_CITY, PRIME_ST_PRV, POSTAL_CODE
                 ,RER_TYPE, COMM_DATE, EXP_DATE, RER_STATUS, ORACLE_ID, FRU_ID, BR_#,PAYMENT_TERM_TYPE, YEAR,NULL,' ||
                 p_month || ', MARCH, APRIL, MAY ,JUNE, JULY, AUGUST, SEPTEMBER, OCTOBER
                 ,NOVEMBER, DECEMBER, vendor_code, vendor_name,  frequency,  gl_code, annual_total, term_comments' || --Ver 1.2
                 ' from apps.xxcuspn_lease_rent_vw where YEAR = ' || p_year ||
                 ' union ' ||
                 'select LOB, RER_ID, PRIME_LOC, PRIME_ADDRESS, PRIME_CITY, PRIME_ST_PRV, POSTAL_CODE
                 ,RER_TYPE, COMM_DATE, EXP_DATE, RER_STATUS, ORACLE_ID, FRU_ID, BR_#,PAYMENT_TERM_TYPE, YEAR,JANUARY,
                 NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL, vendor_code, vendor_name,  frequency,  gl_code, annual_total, term_comments' || --Ver 1.2
                 ' from apps.xxcuspn_lease_rent_vw where YEAR = ' || l_year ||
                 ' ORDER BY RER_id';

    ELSIF lower(p_month) = 'march' THEN
      l_year  := p_year + 1;
      l_query := 'select LOB, RER_ID, PRIME_LOC, PRIME_ADDRESS, PRIME_CITY, PRIME_ST_PRV, POSTAL_CODE
                 ,RER_TYPE, COMM_DATE, EXP_DATE, RER_STATUS, ORACLE_ID, FRU_ID, BR_#,PAYMENT_TERM_TYPE, YEAR,NULL,NULL,' ||
                 p_month || ', APRIL, MAY ,JUNE, JULY, AUGUST, SEPTEMBER, OCTOBER
                 ,NOVEMBER, DECEMBER, vendor_code, vendor_name,  frequency,  gl_code, annual_total, term_comments' || --Ver 1.2
                 ' from apps.xxcuspn_lease_rent_vw where YEAR = ' || p_year ||
                 ' union ' ||
                 'select LOB, RER_ID, PRIME_LOC, PRIME_ADDRESS, PRIME_CITY, PRIME_ST_PRV, POSTAL_CODE
                 ,RER_TYPE, COMM_DATE, EXP_DATE, RER_STATUS, ORACLE_ID, FRU_ID, BR_#,PAYMENT_TERM_TYPE, YEAR, JANUARY, FEBRUARY,
                 NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL, vendor_code, vendor_name,  frequency,  gl_code, annual_total, term_comments' || --Ver 1.2
                 ' from apps.xxcuspn_lease_rent_vw where YEAR = ' || l_year ||
                 ' ORDER BY RER_id';

    ELSIF lower(p_month) = 'april' THEN
      l_year  := p_year + 1;
      l_query := 'select LOB, RER_ID, PRIME_LOC, PRIME_ADDRESS, PRIME_CITY, PRIME_ST_PRV, POSTAL_CODE
                 ,RER_TYPE, COMM_DATE, EXP_DATE, RER_STATUS, ORACLE_ID, FRU_ID, BR_#,PAYMENT_TERM_TYPE, YEAR,NULL,NULL,NULL,' ||
                 p_month || ', MAY ,JUNE, JULY, AUGUST, SEPTEMBER, OCTOBER
                 ,NOVEMBER, DECEMBER, vendor_code, vendor_name,  frequency,  gl_code, annual_total, term_comments' || --Ver 1.2
                 ' from apps.xxcuspn_lease_rent_vw where YEAR = ' || p_year ||
                 ' union ' ||
                 'select LOB, RER_ID, PRIME_LOC, PRIME_ADDRESS, PRIME_CITY, PRIME_ST_PRV, POSTAL_CODE
                 ,RER_TYPE, COMM_DATE, EXP_DATE, RER_STATUS, ORACLE_ID, FRU_ID, BR_#,PAYMENT_TERM_TYPE, YEAR, JANUARY, FEBRUARY,
                 MARCH,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL, vendor_code, vendor_name,  frequency,  gl_code, annual_total, term_comments' || --Ver 1.2
                 ' from apps.xxcuspn_lease_rent_vw where YEAR = ' || l_year ||
                 ' ORDER BY RER_id';

    ELSIF lower(p_month) = 'may' THEN
      l_year  := p_year + 1;
      l_query := 'select LOB, RER_ID, PRIME_LOC, PRIME_ADDRESS, PRIME_CITY, PRIME_ST_PRV, POSTAL_CODE
                 ,RER_TYPE, COMM_DATE, EXP_DATE, RER_STATUS, ORACLE_ID, FRU_ID, BR_#,PAYMENT_TERM_TYPE, YEAR,NULL,NULL,
                 NULL,NULL,' || p_month ||
                 ',JUNE, JULY, AUGUST, SEPTEMBER, OCTOBER
                 ,NOVEMBER, DECEMBER, vendor_code, vendor_name,  frequency,  gl_code, annual_total, term_comments' || --Ver 1.2
                 ' from apps.xxcuspn_lease_rent_vw where YEAR = ' || p_year ||
                 ' union ' ||
                 'select LOB, RER_ID, PRIME_LOC, PRIME_ADDRESS, PRIME_CITY, PRIME_ST_PRV, POSTAL_CODE
                 ,RER_TYPE, COMM_DATE, EXP_DATE, RER_STATUS, ORACLE_ID, FRU_ID, BR_#,PAYMENT_TERM_TYPE, YEAR, JANUARY, FEBRUARY,
                 MARCH, APRIL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL, vendor_code, vendor_name,  frequency,  gl_code, annual_total, term_comments' || --Ver 1.2
                 ' from apps.xxcuspn_lease_rent_vw where YEAR = ' || l_year ||
                 ' ORDER BY RER_id';

    ELSIF lower(p_month) = 'june' THEN
      l_year  := p_year + 1;
      l_query := 'select LOB, RER_ID, PRIME_LOC, PRIME_ADDRESS, PRIME_CITY, PRIME_ST_PRV, POSTAL_CODE
                 ,RER_TYPE, COMM_DATE, EXP_DATE, RER_STATUS, ORACLE_ID, FRU_ID, BR_#,PAYMENT_TERM_TYPE, YEAR,NULL,NULL,
                 NULL,NULL,NULL,' || p_month ||
                 ', JULY, AUGUST, SEPTEMBER, OCTOBER
                 ,NOVEMBER, DECEMBER, vendor_code, vendor_name,  frequency,  gl_code, annual_total, term_comments' || --Ver 1.2
                 ' from apps.xxcuspn_lease_rent_vw where YEAR = ' || p_year ||
                 ' union ' ||
                 'select LOB, RER_ID, PRIME_LOC, PRIME_ADDRESS, PRIME_CITY, PRIME_ST_PRV, POSTAL_CODE
                 ,RER_TYPE, COMM_DATE, EXP_DATE, RER_STATUS, ORACLE_ID, FRU_ID, BR_#,PAYMENT_TERM_TYPE, YEAR, JANUARY, FEBRUARY,
                 MARCH, APRIL, MAY,NULL,NULL,NULL,NULL,NULL,NULL,NULL, vendor_code, vendor_name,  frequency,  gl_code, annual_total, term_comments' || --Ver 1.2
                 ' from apps.xxcuspn_lease_rent_vw where YEAR = ' || l_year ||
                 ' ORDER BY RER_id';

    ELSIF lower(p_month) = 'july' THEN
      l_year  := p_year + 1;
      l_query := 'select LOB, RER_ID, PRIME_LOC, PRIME_ADDRESS, PRIME_CITY, PRIME_ST_PRV, POSTAL_CODE
                 ,RER_TYPE, COMM_DATE, EXP_DATE, RER_STATUS, ORACLE_ID, FRU_ID, BR_#,PAYMENT_TERM_TYPE, YEAR,NULL,NULL,
                 NULL,NULL,NULL,NULL,' || p_month ||
                 ', AUGUST, SEPTEMBER, OCTOBER
                 ,NOVEMBER, DECEMBER, vendor_code, vendor_name,  frequency,  gl_code, annual_total, term_comments' || --Ver 1.2
                 ' from apps.xxcuspn_lease_rent_vw where YEAR = ' || p_year ||
                 ' union ' ||
                 'select LOB, RER_ID, PRIME_LOC, PRIME_ADDRESS, PRIME_CITY, PRIME_ST_PRV, POSTAL_CODE
                 ,RER_TYPE, COMM_DATE, EXP_DATE, RER_STATUS, ORACLE_ID, FRU_ID, BR_#,PAYMENT_TERM_TYPE, YEAR, JANUARY, FEBRUARY,
                 MARCH, APRIL, MAY, JUNE,NULL,NULL,NULL,NULL,NULL,NULL, vendor_code, vendor_name,  frequency,  gl_code, annual_total, term_comments' || --Ver 1.2
                 ' from apps.xxcuspn_lease_rent_vw where YEAR = ' || l_year ||
                 ' ORDER BY RER_id';

    ELSIF lower(p_month) = 'august' THEN
      l_year  := p_year + 1;
      l_query := 'select LOB, RER_ID, PRIME_LOC, PRIME_ADDRESS, PRIME_CITY, PRIME_ST_PRV, POSTAL_CODE
                 ,RER_TYPE, COMM_DATE, EXP_DATE, RER_STATUS, ORACLE_ID, FRU_ID, BR_#,PAYMENT_TERM_TYPE, YEAR,NULL,NULL,
                 NULL,NULL,NULL,NULL,NULL,' || p_month ||
                 ', SEPTEMBER, OCTOBER
                 ,NOVEMBER, DECEMBER, vendor_code, vendor_name,  frequency,  gl_code, annual_total, term_comments' || --Ver 1.2
                 ' from apps.xxcuspn_lease_rent_vw where YEAR = ' || p_year ||
                 ' union ' ||
                 'select LOB, RER_ID, PRIME_LOC, PRIME_ADDRESS, PRIME_CITY, PRIME_ST_PRV, POSTAL_CODE
                 ,RER_TYPE, COMM_DATE, EXP_DATE, RER_STATUS, ORACLE_ID, FRU_ID, BR_#,PAYMENT_TERM_TYPE, YEAR, JANUARY, FEBRUARY,
                 MARCH, APRIL, MAY, JUNE, JULY,NULL,NULL,NULL,NULL,NULL, vendor_code, vendor_name,  frequency,  gl_code, annual_total, term_comments' || --Ver 1.2
                 ' from apps.xxcuspn_lease_rent_vw where YEAR = ' || l_year ||
                 ' ORDER BY RER_id';

    ELSIF lower(p_month) = 'september' THEN
      l_year  := p_year + 1;
      l_query := 'select LOB, RER_ID, PRIME_LOC, PRIME_ADDRESS, PRIME_CITY, PRIME_ST_PRV, POSTAL_CODE
                 ,RER_TYPE, COMM_DATE, EXP_DATE, RER_STATUS, ORACLE_ID, FRU_ID, BR_#,PAYMENT_TERM_TYPE, YEAR,NULL,NULL,
                 NULL,NULL,NULL,NULL,NULL,NULL,' || p_month ||
                 ', OCTOBER
                 ,NOVEMBER, DECEMBER, vendor_code, vendor_name,  frequency,  gl_code, annual_total, term_comments' || --Ver 1.2
                 ' from apps.xxcuspn_lease_rent_vw where YEAR = ' || p_year ||
                 ' union ' ||
                 'select LOB, RER_ID, PRIME_LOC, PRIME_ADDRESS, PRIME_CITY, PRIME_ST_PRV, POSTAL_CODE
                 ,RER_TYPE, COMM_DATE, EXP_DATE, RER_STATUS, ORACLE_ID, FRU_ID, BR_#,PAYMENT_TERM_TYPE, YEAR, JANUARY, FEBRUARY,
                 MARCH, APRIL, MAY, JUNE, JULY, AUGUST,NULL,NULL,NULL,NULL, vendor_code, vendor_name,  frequency,  gl_code, annual_total, term_comments' || --Ver 1.2
                 ' from apps.xxcuspn_lease_rent_vw where YEAR = ' || l_year ||
                 ' ORDER BY RER_id';

    ELSIF lower(p_month) = 'october' THEN
      l_year  := p_year + 1;
      l_query := 'select LOB, RER_ID, PRIME_LOC, PRIME_ADDRESS, PRIME_CITY, PRIME_ST_PRV, POSTAL_CODE
                 ,RER_TYPE, COMM_DATE, EXP_DATE, RER_STATUS, ORACLE_ID, FRU_ID, BR_#,PAYMENT_TERM_TYPE, YEAR,NULL,NULL,
                 NULL,NULL,NULL,NULL,NULL,NULL,NULL,' ||
                 p_month || ',NOVEMBER, DECEMBER, vendor_code, vendor_name,  frequency,  gl_code, annual_total, term_comments ' || --Ver 1.2
                 ' from apps.xxcuspn_lease_rent_vw where YEAR = ' || p_year ||
                 ' union ' ||
                 'select LOB, RER_ID, PRIME_LOC, PRIME_ADDRESS, PRIME_CITY, PRIME_ST_PRV, POSTAL_CODE
                 ,RER_TYPE, COMM_DATE, EXP_DATE, RER_STATUS, ORACLE_ID, FRU_ID, BR_#,PAYMENT_TERM_TYPE, YEAR, JANUARY, FEBRUARY,
                 MARCH, APRIL, MAY, JUNE, JULY, AUGUST, SEPTEMBER,NULL,NULL,NULL, vendor_code, vendor_name,  frequency,  gl_code, annual_total, term_comments' || --Ver 1.2
                 ' from apps.xxcuspn_lease_rent_vw where YEAR = ' || l_year ||
                 ' ORDER BY RER_id';

    ELSIF lower(p_month) = 'november' THEN
      l_year  := p_year + 1;
      l_query := 'select LOB, RER_ID, PRIME_LOC, PRIME_ADDRESS, PRIME_CITY, PRIME_ST_PRV, POSTAL_CODE
                 ,RER_TYPE, COMM_DATE, EXP_DATE, RER_STATUS, ORACLE_ID, FRU_ID, BR_#,PAYMENT_TERM_TYPE, YEAR,NULL,NULL,
                 NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,' ||
                 p_month || ', DECEMBER, vendor_code, vendor_name,  frequency,  gl_code, annual_total, term_comments' || --Ver 1.2'
                 ' from apps.xxcuspn_lease_rent_vw where YEAR = ' || p_year ||
                 ' union ' ||
                 'select LOB, RER_ID, PRIME_LOC, PRIME_ADDRESS, PRIME_CITY, PRIME_ST_PRV, POSTAL_CODE
                 ,RER_TYPE, COMM_DATE, EXP_DATE, RER_STATUS, ORACLE_ID, FRU_ID, BR_#,PAYMENT_TERM_TYPE, YEAR, JANUARY, FEBRUARY,
                 MARCH, APRIL, MAY, JUNE, JULY, AUGUST, SEPTEMBER, OCTOBER,NULL,NULL, vendor_code, vendor_name,  frequency,  gl_code, annual_total, term_comments' || --Ver 1.2
                 ' from apps.xxcuspn_lease_rent_vw where YEAR = ' || l_year ||
                 ' ORDER BY RER_id';

    ELSIF lower(p_month) = 'december' THEN
      l_year  := p_year + 1;
      l_query := 'select LOB, RER_ID, PRIME_LOC, PRIME_ADDRESS, PRIME_CITY, PRIME_ST_PRV, POSTAL_CODE
                 ,RER_TYPE, COMM_DATE, EXP_DATE, RER_STATUS, ORACLE_ID, FRU_ID, BR_#,PAYMENT_TERM_TYPE, YEAR,NULL,NULL,NULL
                 ,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,' ||
                 p_month ||', vendor_code, vendor_name,  frequency,  gl_code, annual_total, term_comments ' || --Ver 1.2
                 ' from apps.xxcuspn_lease_rent_vw where YEAR = ' || p_year ||
                 ' union ' ||
                 'select LOB, RER_ID, PRIME_LOC, PRIME_ADDRESS, PRIME_CITY, PRIME_ST_PRV, POSTAL_CODE
                 ,RER_TYPE, COMM_DATE, EXP_DATE, RER_STATUS, ORACLE_ID, FRU_ID, BR_#,PAYMENT_TERM_TYPE, YEAR, JANUARY, FEBRUARY,
                 MARCH, APRIL, MAY, JUNE, JULY, AUGUST, SEPTEMBER, OCTOBER, NOVEMBER,NULL, vendor_code, vendor_name,  frequency,  gl_code, annual_total, term_comments ' || --Ver 1.2
                 ' from apps.xxcuspn_lease_rent_vw where YEAR = ' || l_year ||
                 ' ORDER BY RER_id';

    END IF;

    l_sec := 'open cursor cur';

    OPEN cur FOR l_query;
    RETURN cur;

  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;

      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := 'Error in program ' || l_procedure_name || ' Error: ' ||
                    l_sec;
      l_err_msg  := l_err_msg || ' ERROR ' || substr(SQLERRM, 1, 2000);

      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode := l_err_code;
      errbuf  := l_err_msg;

      xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_procedure_name,
                                           p_calling => l_sec,
                                           p_request_id => fnd_global.conc_request_id,
                                           p_ora_error_msg => SQLERRM,
                                           p_error_desc => 'Error running xxcuspn_lease_pmt_report_pkg.data_ref_cur with PROGRAM exeception',
                                           p_distribution_list => l_distro_list,
                                           p_module => 'PN');

    WHEN OTHERS THEN

      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := l_sec || l_err_msg || ' ERROR ' || SQLCODE ||
                    substr(SQLERRM, 1, 2000);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode := l_err_code;
      errbuf  := l_err_msg;

      xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_procedure_name,
                                           p_calling => l_sec,
                                           p_request_id => fnd_global.conc_request_id,
                                           p_ora_error_msg => SQLERRM,
                                           p_error_desc => 'Error running xxcuspn_lease_pmt_report_pkg.data_ref_cur with OTHERS Exception',
                                           p_distribution_list => l_distro_list,
                                           p_module => 'PN');

  END;

  ----------------------------------------------------------------------------------

  /*FUNCTION get_data(p_month IN VARCHAR2
                   ,p_year  IN NUMBER) RETURN xxcuspn_rent_tbl_ntt IS
    --  RETURN SYS_REFCURSOR IS
    l_rent_data     xxcuspn_rent_tbl_ntt := xxcuspn_rent_tbl_ntt();
    cur             SYS_REFCURSOR;
    l_query         VARCHAR2(255);
    l_lob           VARCHAR2(10);
    l_rer_id        VARCHAR(50);
    l_prime_loc     VARCHAR2(25);
    l_prime_address VARCHAR2(150);
    l_prime_city    VARCHAR2(100);
    l_prime_st_prv  VARCHAR2(60);
    l_postal_code   VARCHAR2(10);
    l_rer_type      VARCHAR2(30);
    l_comm_date     DATE;
    l_exp_date      DATE;
    l_rer_status    VARCHAR2(30);
    l_oracle_id     VARCHAR2(25);
    l_fru_id        VARCHAR2(4);
    l_br_#          VARCHAR2(6);
    l_year          NUMBER;
    l_month1        NUMBER;
    l_month2        NUMBER;
    l_month3        NUMBER;
    l_month4        NUMBER;
    l_month5        NUMBER;
    l_month6        NUMBER;
    l_month7        NUMBER;
    l_month8        NUMBER;
    l_month9        NUMBER;
    l_month10       NUMBER;
    l_month11       NUMBER;
    l_month12       NUMBER;

    l_count NUMBER := 0;

  BEGIN

    cur := data_ref_cur(p_month, p_year);
    LOOP
      FETCH cur
        INTO l_lob
            ,l_rer_id
            ,l_prime_loc
            ,l_prime_address
            ,l_prime_city
            ,l_prime_st_prv
            ,l_postal_code
            ,l_rer_type
            ,l_comm_date
            ,l_exp_date
            ,l_rer_status
            ,l_oracle_id
            ,l_fru_id
            ,l_br_#
            ,l_year
            ,l_month1
            ,l_month2
            ,l_month3
            ,l_month4
            ,l_month5
            ,l_month6
            ,l_month7
            ,l_month8
            ,l_month9
            ,l_month10
            ,l_month11
            ,l_month12;

      INSERT INTO xxcus.xxcuspn_rent_tbl
      VALUES
        (l_lob
        ,l_rer_id
        ,l_prime_loc
        ,l_prime_address
        ,l_prime_city
        ,l_prime_st_prv
        ,l_postal_code
        ,l_rer_type
        ,l_comm_date
        ,l_exp_date
        ,l_rer_status
        ,l_oracle_id
        ,l_fru_id
        ,l_br_#
        ,l_year
        ,l_month1
        ,l_month2
        ,l_month3
        ,l_month4
        ,l_month5
        ,l_month6
        ,l_month7
        ,l_month8
        ,l_month9
        ,l_month10
        ,l_month11
        ,l_month12
        ,NULL
        ,NULL);

      EXIT WHEN cur%NOTFOUND;
      \*      l_rent_data.extend;    -- REMOVE TO USE INSERT METHOD ABOVE
      l_count := l_count + 1;
      l_rent_data(l_count) := xxcuspn_rent_type(l_lob, l_rer_id, l_prime_loc,
                                                l_prime_address, l_prime_city,
                                                l_prime_st_prv, l_postal_code,
                                                l_rer_type, l_comm_date,
                                                l_exp_date, l_rer_status,
                                                l_oracle_id, l_fru_id, l_br_#,
                                                l_year, l_month1, l_month2,
                                                l_month3, l_month4, l_month5,
                                                l_month6, l_month7, l_month8,
                                                l_month9, l_month10,
                                                l_month11, l_month12);*\
    END LOOP;
    COMMIT;
    CLOSE cur;
    RETURN l_rent_data;
  END;*/

  /*===========================================================================
    Function Name: get_rolling12_data
    PURPOSE: populate rolling 12 month data to a table for EIS report
    HISTORY
  ===========================================================================
    VERSION DATE          AUTHOR(S)          DESCRIPTION
    ------- -----------   ---------------    ------------------------------
    1.0     03-Jun-2013   Luong Vu           Created this package.
  ===========================================================================*/
  PROCEDURE get_rolling12_data(p_retcode out varchar2, p_errbuf out varchar2, p_month IN VARCHAR2 -- Ver 1.3
                              ,p_year  IN NUMBER) IS
    --  RETURN SYS_REFCURSOR IS
    l_rent_data     xxcuspn_rent_tbl_ntt := xxcuspn_rent_tbl_ntt();
    cur             SYS_REFCURSOR;
    l_query         VARCHAR2(255);
    l_lob           VARCHAR2(10);
    l_rer_id        VARCHAR(50);
    l_prime_loc     VARCHAR2(25);
    l_prime_address VARCHAR2(150);
    l_prime_city    VARCHAR2(100);
    l_prime_st_prv  VARCHAR2(60);
    l_postal_code   VARCHAR2(10);
    l_rer_type      VARCHAR2(30);
    l_comm_date     DATE;
    l_exp_date      DATE;
    l_rer_status    VARCHAR2(30);
    l_oracle_id     VARCHAR2(25);
    l_fru_id        VARCHAR2(4);
    l_br_#          VARCHAR2(6);
    l_term_type     VARCHAR2(50);
    l_year          NUMBER;
    l_month1        NUMBER;
    l_month2        NUMBER;
    l_month3        NUMBER;
    l_month4        NUMBER;
    l_month5        NUMBER;
    l_month6        NUMBER;
    l_month7        NUMBER;
    l_month8        NUMBER;
    l_month9        NUMBER;
    l_month10       NUMBER;
    l_month11       NUMBER;
    l_month12       NUMBER;
    l_count         NUMBER := 0;
   l_vendor_code xxcus.xxcuspn_rent_tbl.vendor_code%type :=Null;--Ver 1.2
   l_vendor_name xxcus.xxcuspn_rent_tbl.vendor_name%type :=Null; --Ver 1.2
   l_frequency xxcus.xxcuspn_rent_tbl.frequency%type :=Null; --Ver 1.2
   l_gl_code xxcus.xxcuspn_rent_tbl.gl_code%type :=Null; --Ver 1.2
   l_total xxcus.xxcuspn_rent_tbl.annual_total%type :=Null;--Ver 1.2
   l_term_comments xxcus.xxcuspn_rent_tbl.term_comments%type :=Null; --Ver 1.2
    l_procedure_name VARCHAR2(75) DEFAULT 'get_rolling12_data';
    l_db_name varchar2(40) :=Null; -- Ver 1.3
  BEGIN 
    --
    -- Begin Ver 1.3       
    select upper(name) into l_db_name from v$database;
    if l_db_name ='EBSPRD' then
     l_db_name :=' ( Oracle EBS PROD )';
    else
     l_db_name :=' ( Oracle EBS NON PROD )';    
    end if;
    --
    --
    print_log('100: Instance :'||l_db_name);
    --    
    print_log('101: Before clean up of rent table');
    -- End Ver 1.3    
    l_sec := 'truncate table xxcus.xxcuspn_rent_tbl';

    EXECUTE IMMEDIATE 'truncate table xxcus.xxcuspn_rent_tbl';
    print_log('102: Before clean up of rent table'); -- Ver 1.3
    l_sec := 'insert data into table xxcus.xxcuspn_rent_tbl';
    print_log('103: Before fetch of data set'); -- Ver 1.3
    cur := rolling12_ref_cur(p_month, p_year, retcode => l_err_code,
                             errbuf => l_err_msg);
    print_log('104: After fetch of data set'); -- Ver 1.3                             
    LOOP
      FETCH cur
        INTO l_lob
            ,l_rer_id
            ,l_prime_loc
            ,l_prime_address
            ,l_prime_city
            ,l_prime_st_prv
            ,l_postal_code
            ,l_rer_type
            ,l_comm_date
            ,l_exp_date
            ,l_rer_status
            ,l_oracle_id
            ,l_fru_id
            ,l_br_#
            ,l_term_type
            ,l_year
            ,l_month1
            ,l_month2
            ,l_month3
            ,l_month4
            ,l_month5
            ,l_month6
            ,l_month7
            ,l_month8
            ,l_month9
            ,l_month10
            ,l_month11
            ,l_month12
           ,l_vendor_code --Ver 1.2
           ,l_vendor_name --Ver 1.2
           ,l_frequency --Ver 1.2
           ,l_gl_code --Ver 1.2
           ,l_total --Ver 1.2
           ,l_term_comments --Ver 1.2
              ;    
      INSERT INTO xxcus.xxcuspn_rent_tbl
      VALUES
        (l_lob
        ,l_rer_id
        ,l_prime_loc
        ,l_prime_address
        ,l_prime_city
        ,l_prime_st_prv
        ,l_postal_code
        ,l_rer_type
        ,l_comm_date
        ,l_exp_date
        ,l_rer_status
        ,l_oracle_id
        ,l_fru_id
        ,l_br_#
        ,l_term_type
        ,l_year
        ,l_month1
        ,l_month2
        ,l_month3
        ,l_month4
        ,l_month5
        ,l_month6
        ,l_month7
        ,l_month8
        ,l_month9
        ,l_month10
        ,l_month11
        ,l_month12
        --,NULL --Ver 1.2
        ,l_total --Ver 1.2
        ,NULL
       ,l_vendor_code --Ver 1.2
       ,l_vendor_name --Ver 1.2
       ,l_frequency --Ver 1.2
       ,l_gl_code --Ver 1.2
       ,l_term_comments --Ver 1.2
        );        
      EXIT WHEN cur%NOTFOUND;
    END LOOP;
    COMMIT;
    CLOSE cur;
    --
    -- Begin Ver 1.3
    --
    select count(1) into l_count from xxcus.xxcuspn_rent_tbl; 
    --
    print_log('Rent table count ='||l_count); 
    --
    begin
        --                          
        print_log('Before sending email notification about OPN rent extract status update');                                                                                                
        --  
        g_notif_email_body :='The OPN rent repopulate extract is complete. Please check table xxcus.xxcuspn_rent_tbl. Total records :'||l_count;
        --
        send_email_notif
           (
                p_email_from      =>g_notif_email_from,
                p_email_to           =>g_notif_email_to,
                p_email_subject =>g_email_subject||l_db_name, 
                p_email_body     =>g_notif_email_body
           )
          ; 
        --                          
        print_log('After sending email notification about OPN rent extract status update');                                                                                                
        --      
    exception
     when others then
      print_log('Error in sending email alert, message =>'||sqlerrm);
    end;
    --
  EXCEPTION 
    WHEN OTHERS THEN 
       print_log('199: ERROR IN GET _ROLLING12_DATA, MSG =>'||SQLERRM);    
       -- End Ver 1.3
  END;
END xxcuspn_lease_pmt_report_pkg;
/