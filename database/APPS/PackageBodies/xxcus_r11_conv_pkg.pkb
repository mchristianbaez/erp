CREATE OR REPLACE PACKAGE BODY APPS.xxcus_r11_conv_pkg IS

  /*******************************************************************************
  * Procedure:   
  * Description: Will be used to load Property Manager data into R12 from R11
  *              
  *
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     03/02/2011    Kathy Poling    Initial creation  
  
  ********************************************************************************/
  PROCEDURE pn_data(errbuf OUT VARCHAR2, retcode OUT NUMBER) IS
  
    -- Package Variables
    l_count_r11 NUMBER;
    l_count_r12 NUMBER;
    l_table     VARCHAR(50);
    l_sequence  VARCHAR(50);
    l_seq_r11   NUMBER;
    l_seq_r12   NUMBER;
    l_org    CONSTANT hr_all_organization_units.organization_id%TYPE := 163; --HD Supply Corp USD - Org
    l_ledger CONSTANT gl_ledgers.ledger_id%TYPE := 2061; --HD Supply USD
    l_cust_id      NUMBER := 6051; --cust_account_id table hz_cust_accounts for customer HDS REAL ESTATE SUBLESSEE
    l_cust_site_id NUMBER := 7337; --site use id table HZ_CUST_SITE_USES_ALL
    l_trx_type_id  NUMBER := 1; --INVOICE                  
  
    l_err_code NUMBER;
    l_err_msg  VARCHAR2(3000);
    l_sec      VARCHAR2(255);
  
    l_err_callfrom  VARCHAR2(75) DEFAULT 'xxcus_pn_conv_pkg';
    l_err_callpoint VARCHAR2(75) DEFAULT 'START';
    l_distro_list   VARCHAR2(75) DEFAULT 'kathy.poling@hdsupply.com';
  
  BEGIN
    BEGIN
      l_table     := 'PN.PN_ADDRESSES_ALL';
      l_sequence  := 'PN.PN_ADDRESSES_S';
      l_sec       := 'Loading  table ' || l_table;
      l_count_r11 := 0;
      l_count_r12 := 0;
    
      EXECUTE IMMEDIATE 'SELECT COUNT(*) FROM ' || l_table ||
                        '@R12_TO_FIN.HSI.HUGHESSUPPLY.COM'
        INTO l_count_r11;
    
      EXECUTE IMMEDIATE 'SELECT MAX(address_id) FROM ' || l_table ||
                        '@R12_TO_FIN.HSI.HUGHESSUPPLY.COM'
        INTO l_seq_r11;
    
      INSERT /*+ APPEND */
      INTO pn_addresses_all
        SELECT * FROM pn.pn_addresses_all@r12_to_fin.hsi.hughessupply.com;
      COMMIT;
    
      BEGIN
        FOR c_update IN (SELECT address_id
                               ,created_by
                               ,last_updated_by
                               ,nvl(c.user_id, a.created_by) created_user
                               ,nvl(u.user_id, a.last_updated_by) updated_user
                           FROM pn.pn_addresses_all            a
                               ,xxcus.xxcus_employee_conv_temp c
                               ,xxcus.xxcus_employee_conv_temp u
                          WHERE created_by = c.user_id_r11(+)
                            AND last_updated_by = u.user_id_r11(+))
        LOOP
        
          UPDATE pn.pn_addresses_all
             SET org_id          = l_org
                ,created_by      = c_update.created_user
                ,last_updated_by = c_update.updated_user
           WHERE address_id = c_update.address_id;
        
        END LOOP;
      END;
    
      EXECUTE IMMEDIATE 'alter sequence ' || l_sequence || ' increment by ' ||
                        l_seq_r11;
    
      EXECUTE IMMEDIATE 'select ' || l_sequence || '.nextval from dual '
        INTO l_seq_r12;
    
      EXECUTE IMMEDIATE 'alter sequence ' || l_sequence || ' increment by 1 ';
    
      EXECUTE IMMEDIATE 'SELECT COUNT(*) FROM ' || l_table
        INTO l_count_r12;
    
      INSERT INTO xxcus.xxcus_pn_conv_tbl
        (table_name
        ,r11_count
        ,r12_count
        ,sequence
        ,seq_r11_num
        ,seq_r12_num
        ,creation_date)
      VALUES
        (l_table
        ,l_count_r11
        ,l_count_r12
        ,l_sequence
        ,l_seq_r11
        ,l_seq_r12
        ,SYSDATE);
    
      COMMIT;
    
    END;
  
    ----------------------------------------------------------
    BEGIN
      l_table     := 'PN.PN_COMPANIES_ALL';
      l_sequence  := 'PN.PN_COMPANIES_S';
      l_sec       := 'Loading table ' || l_table;
      l_count_r11 := 0;
      l_count_r12 := 0;
    
      EXECUTE IMMEDIATE 'SELECT COUNT(*) FROM ' || l_table ||
                        '@R12_TO_FIN.HSI.HUGHESSUPPLY.COM'
        INTO l_count_r11;
    
      EXECUTE IMMEDIATE 'SELECT MAX(company_id) FROM ' || l_table ||
                        '@R12_TO_FIN.HSI.HUGHESSUPPLY.COM'
        INTO l_seq_r11;
    
      INSERT /*+ APPEND */
      INTO pn.pn_companies_all
        SELECT * FROM pn.pn_companies_all@r12_to_fin.hsi.hughessupply.com;
      COMMIT;
    
      BEGIN
        FOR c_update IN (SELECT company_id
                               ,created_by
                               ,last_updated_by
                               ,nvl(c.user_id, a.created_by) created_user
                               ,nvl(u.user_id, a.last_updated_by) updated_user
                           FROM pn.pn_companies_all            a
                               ,xxcus.xxcus_employee_conv_temp c
                               ,xxcus.xxcus_employee_conv_temp u
                          WHERE created_by = c.user_id_r11(+)
                            AND last_updated_by = u.user_id_r11(+))
        LOOP
        
          UPDATE pn.pn_companies_all
             SET org_id          = l_org
                ,created_by      = c_update.created_user
                ,last_updated_by = c_update.updated_user
           WHERE company_id = c_update.company_id;
        
        END LOOP;
      END;
    
      EXECUTE IMMEDIATE 'alter sequence ' || l_sequence || ' increment by ' ||
                        l_seq_r11;
    
      EXECUTE IMMEDIATE 'select ' || l_sequence || '.nextval from dual '
        INTO l_seq_r12;
    
      EXECUTE IMMEDIATE 'alter sequence ' || l_sequence || ' increment by 1 ';
    
      EXECUTE IMMEDIATE 'SELECT COUNT(*) FROM ' || l_table
        INTO l_count_r12;
    
      INSERT INTO xxcus.xxcus_pn_conv_tbl
        (table_name
        ,r11_count
        ,r12_count
        ,sequence
        ,seq_r11_num
        ,seq_r12_num
        ,creation_date)
      VALUES
        (l_table
        ,l_count_r11
        ,l_count_r12
        ,l_sequence
        ,l_seq_r11
        ,l_seq_r12
        ,SYSDATE);
    
      COMMIT;
    
    END;
  
    --------------------------------------------------------------
    BEGIN
      l_table     := 'PN.PN_COMPANY_SITES_ALL';
      l_sequence  := 'PN.PN_COMPANY_SITES_S';
      l_sec       := 'Loading table ' || l_table;
      l_count_r11 := 0;
      l_count_r12 := 0;
    
      EXECUTE IMMEDIATE 'SELECT COUNT(*) FROM ' || l_table ||
                        '@R12_TO_FIN.HSI.HUGHESSUPPLY.COM'
        INTO l_count_r11;
    
      EXECUTE IMMEDIATE 'SELECT MAX(company_site_id) FROM ' || l_table ||
                        '@R12_TO_FIN.HSI.HUGHESSUPPLY.COM'
        INTO l_seq_r11;
    
      INSERT /*+ APPEND */
      INTO pn.pn_company_sites_all
        SELECT * FROM pn.pn_company_sites_all@r12_to_fin.hsi.hughessupply.com;
      COMMIT;
    
      BEGIN
        FOR c_update IN (SELECT company_site_id
                               ,created_by
                               ,last_updated_by
                               ,nvl(c.user_id, a.created_by) created_user
                               ,nvl(u.user_id, a.last_updated_by) updated_user
                           FROM pn.pn_company_sites_all        a
                               ,xxcus.xxcus_employee_conv_temp c
                               ,xxcus.xxcus_employee_conv_temp u
                          WHERE created_by = c.user_id_r11(+)
                            AND last_updated_by = u.user_id_r11(+))
        LOOP
        
          UPDATE pn.pn_company_sites_all
             SET org_id          = l_org
                ,created_by      = c_update.created_user
                ,last_updated_by = c_update.updated_user
           WHERE company_site_id = c_update.company_site_id;
        
        END LOOP;
      END;
    
      EXECUTE IMMEDIATE 'alter sequence ' || l_sequence || ' increment by ' ||
                        l_seq_r11;
    
      EXECUTE IMMEDIATE 'select ' || l_sequence || '.nextval from dual '
        INTO l_seq_r12;
    
      EXECUTE IMMEDIATE 'alter sequence ' || l_sequence || ' increment by 1 ';
    
      EXECUTE IMMEDIATE 'SELECT COUNT(*) FROM ' || l_table
        INTO l_count_r12;
    
      INSERT INTO xxcus.xxcus_pn_conv_tbl
        (table_name
        ,r11_count
        ,r12_count
        ,sequence
        ,seq_r11_num
        ,seq_r12_num
        ,creation_date)
      VALUES
        (l_table
        ,l_count_r11
        ,l_count_r12
        ,l_sequence
        ,l_seq_r11
        ,l_seq_r12
        ,SYSDATE);
    
      COMMIT;
    
    END;
  
    --------------------------------------------------------------
    BEGIN
      l_table     := 'PN.PN_CONTACTS_ALL';
      l_sequence  := 'PN.PN_CONTACTS_S';
      l_sec       := 'Loading table ' || l_table;
      l_count_r11 := 0;
      l_count_r12 := 0;
    
      EXECUTE IMMEDIATE 'SELECT COUNT(*) FROM ' || l_table ||
                        '@R12_TO_FIN.HSI.HUGHESSUPPLY.COM'
        INTO l_count_r11;
    
      EXECUTE IMMEDIATE 'SELECT MAX(contact_id) FROM ' || l_table ||
                        '@R12_TO_FIN.HSI.HUGHESSUPPLY.COM'
        INTO l_seq_r11;
    
      INSERT /*+ APPEND */
      INTO pn.pn_contacts_all
        SELECT * FROM pn.pn_contacts_all@r12_to_fin.hsi.hughessupply.com;
      COMMIT;
    
      BEGIN
        FOR c_update IN (SELECT contact_id
                               ,created_by
                               ,last_updated_by
                               ,nvl(c.user_id, a.created_by) created_user
                               ,nvl(u.user_id, a.last_updated_by) updated_user
                           FROM pn.pn_contacts_all             a
                               ,xxcus.xxcus_employee_conv_temp c
                               ,xxcus.xxcus_employee_conv_temp u
                          WHERE created_by = c.user_id_r11(+)
                            AND last_updated_by = u.user_id_r11(+))
        LOOP
        
          UPDATE pn.pn_contacts_all
             SET org_id          = l_org
                ,created_by      = c_update.created_user
                ,last_updated_by = c_update.updated_user
           WHERE contact_id = c_update.contact_id;
        
        END LOOP;
      END;
    
      EXECUTE IMMEDIATE 'alter sequence ' || l_sequence || ' increment by ' ||
                        l_seq_r11;
    
      EXECUTE IMMEDIATE 'select ' || l_sequence || '.nextval from dual '
        INTO l_seq_r12;
    
      EXECUTE IMMEDIATE 'alter sequence ' || l_sequence || ' increment by 1 ';
    
      EXECUTE IMMEDIATE 'SELECT COUNT(*) FROM ' || l_table
        INTO l_count_r12;
    
      INSERT INTO xxcus.xxcus_pn_conv_tbl
        (table_name
        ,r11_count
        ,r12_count
        ,sequence
        ,seq_r11_num
        ,seq_r12_num
        ,creation_date)
      VALUES
        (l_table
        ,l_count_r11
        ,l_count_r12
        ,l_sequence
        ,l_seq_r11
        ,l_seq_r12
        ,SYSDATE);
    
      COMMIT;
    
    END;
  
    --------------------------------------------------------------
    BEGIN
      l_table     := 'PN.PN_CONTACT_ASSIGNMENTS_ALL';
      l_sequence  := 'PN.PN_CONTACT_ASSIGNMENTS_S';
      l_sec       := 'Loading table ' || l_table;
      l_count_r11 := 0;
      l_count_r12 := 0;
    
      EXECUTE IMMEDIATE 'SELECT COUNT(*) FROM ' || l_table ||
                        '@R12_TO_FIN.HSI.HUGHESSUPPLY.COM'
        INTO l_count_r11;
    
      EXECUTE IMMEDIATE 'SELECT MAX(contact_assignment_id) FROM ' || l_table ||
                        '@R12_TO_FIN.HSI.HUGHESSUPPLY.COM'
        INTO l_seq_r11;
    
      INSERT /*+ APPEND */
      INTO pn.pn_contact_assignments_all
        SELECT *
          FROM pn.pn_contact_assignments_all@r12_to_fin.hsi.hughessupply.com;
      COMMIT;
    
      BEGIN
        FOR c_update IN (SELECT contact_assignment_id
                               ,created_by
                               ,last_updated_by
                               ,nvl(c.user_id, a.created_by) created_user
                               ,nvl(u.user_id, a.last_updated_by) updated_user
                           FROM pn.pn_contact_assignments_all  a
                               ,xxcus.xxcus_employee_conv_temp c
                               ,xxcus.xxcus_employee_conv_temp u
                          WHERE created_by = c.user_id_r11(+)
                            AND last_updated_by = u.user_id_r11(+))
        LOOP
        
          UPDATE pn.pn_contact_assignments_all
             SET org_id          = l_org
                ,created_by      = c_update.created_user
                ,last_updated_by = c_update.updated_user
           WHERE contact_assignment_id = c_update.contact_assignment_id;
        
        END LOOP;
      END;
    
      EXECUTE IMMEDIATE 'alter sequence ' || l_sequence || ' increment by ' ||
                        l_seq_r11;
    
      EXECUTE IMMEDIATE 'select ' || l_sequence || '.nextval from dual '
        INTO l_seq_r12;
    
      EXECUTE IMMEDIATE 'alter sequence ' || l_sequence || ' increment by 1 ';
    
      EXECUTE IMMEDIATE 'SELECT COUNT(*) FROM ' || l_table
        INTO l_count_r12;
    
      INSERT INTO xxcus.xxcus_pn_conv_tbl
        (table_name
        ,r11_count
        ,r12_count
        ,sequence
        ,seq_r11_num
        ,seq_r12_num
        ,creation_date)
      VALUES
        (l_table
        ,l_count_r11
        ,l_count_r12
        ,l_sequence
        ,l_seq_r11
        ,l_seq_r12
        ,SYSDATE);
    
      COMMIT;
    
    END;
  
    --------------------------------------------------------------
    BEGIN
      l_table     := 'PN.PN_CONTACT_ASSIGN_HISTORY';
      l_sequence  := 'PN.pn_contact_assign_history_s';
      l_sec       := 'Loading table ' || l_table;
      l_count_r11 := 0;
      l_count_r12 := 0;
    
      EXECUTE IMMEDIATE 'SELECT COUNT(*) FROM ' || l_table ||
                        '@R12_TO_FIN.HSI.HUGHESSUPPLY.COM'
        INTO l_count_r11;
    
      EXECUTE IMMEDIATE 'SELECT MAX(contact_assign_history_id) FROM ' ||
                        l_table || '@R12_TO_FIN.HSI.HUGHESSUPPLY.COM'
        INTO l_seq_r11;
    
      INSERT /*+ APPEND */
      INTO pn.pn_contact_assign_history
        SELECT *
          FROM pn.pn_contact_assign_history@r12_to_fin.hsi.hughessupply.com;
      COMMIT;
    
      BEGIN
        FOR c_update IN (SELECT contact_assign_history_id
                               ,created_by
                               ,last_updated_by
                               ,nvl(c.user_id, a.created_by) created_user
                               ,nvl(u.user_id, a.last_updated_by) updated_user
                           FROM pn.pn_contact_assign_history   a
                               ,xxcus.xxcus_employee_conv_temp c
                               ,xxcus.xxcus_employee_conv_temp u
                          WHERE created_by = c.user_id_r11(+)
                            AND last_updated_by = u.user_id_r11(+))
        LOOP
        
          UPDATE pn.pn_contact_assign_history
             SET created_by      = c_update.created_user
                ,last_updated_by = c_update.updated_user
           WHERE contact_assign_history_id = c_update.contact_assign_history_id;
        
        END LOOP;
      END;
    
      EXECUTE IMMEDIATE 'alter sequence ' || l_sequence || ' increment by ' ||
                        l_seq_r11;
    
      EXECUTE IMMEDIATE 'select ' || l_sequence || '.nextval from dual '
        INTO l_seq_r12;
    
      EXECUTE IMMEDIATE 'alter sequence ' || l_sequence || ' increment by 1 ';
    
      EXECUTE IMMEDIATE 'SELECT COUNT(*) FROM ' || l_table
        INTO l_count_r12;
    
      INSERT INTO xxcus.xxcus_pn_conv_tbl
        (table_name
        ,r11_count
        ,r12_count
        ,sequence
        ,seq_r11_num
        ,seq_r12_num
        ,creation_date)
      VALUES
        (l_table
        ,l_count_r11
        ,l_count_r12
        ,l_sequence
        ,l_seq_r11
        ,l_seq_r12
        ,SYSDATE);
    
      COMMIT;
    
    END;
  
    ------------------------------------------------------------------------     
    BEGIN
      l_table     := 'PN.PN_DISTRIBUTIONS_ALL';
      l_sequence  := 'PN.PN_DISTRIBUTIONS_S';
      l_sec       := 'Loading table ' || l_table;
      l_count_r11 := 0;
      l_count_r12 := 0;
    
      EXECUTE IMMEDIATE 'SELECT COUNT(*) FROM ' || l_table ||
                        '@R12_TO_FIN.HSI.HUGHESSUPPLY.COM'
        INTO l_count_r11;
    
      EXECUTE IMMEDIATE 'SELECT MAX(distribution_id) FROM ' || l_table ||
                        '@R12_TO_FIN.HSI.HUGHESSUPPLY.COM'
        INTO l_seq_r11;
    
      INSERT /*+ APPEND */
      INTO pn.pn_distributions_all
        SELECT * FROM pn.pn_distributions_all@r12_to_fin.hsi.hughessupply.com;
      COMMIT;
    
      BEGIN
        FOR c_update IN (SELECT distribution_id
                               ,a.created_by
                               ,a.last_updated_by
                               ,nvl(c.user_id, a.created_by) created_user
                               ,nvl(u.user_id, a.last_updated_by) updated_user
                               ,(SELECT code_combination_id
                                   FROM gl.gl_code_combinations cc
                                  WHERE cc.segment1 = (CASE
                                          WHEN g.segment1 IN ('08', '31') THEN
                                           '0W'
                                          WHEN g.segment1 IN
                                               ('43', '44', '45', '46') THEN
                                           '42'
                                          WHEN g.segment1 = '12' THEN
                                           '11'
                                          ELSE
                                           g.segment1
                                        END)
                                    AND cc.segment2 = (CASE
                                          WHEN g.segment2 LIKE 'B0%' THEN
                                           'BW' || substr(g.segment2, 3, 3)
                                          ELSE
                                           g.segment2
                                        END)
                                    AND cc.segment3 = (CASE
                                          WHEN g.segment1 = '11' AND
                                               substr(g.segment2, 1, 1) = 'M' AND
                                               g.segment2 <> 'M0196' THEN
                                           '0000'
                                          WHEN g.segment1 IN ('01', '32') THEN
                                           'W010'
                                          WHEN g.segment1 = '42' THEN
                                           'C950'
                                          WHEN g.segment1 IN
                                               ('17', '18', '19', '37', '38') THEN
                                           '0000'
                                          ELSE
                                           g.segment3
                                        END)
                                    AND cc.segment4 = g.segment4
                                    AND cc.segment5 = g.segment5
                                    AND cc.segment6 = g.segment6
                                    AND cc.segment7 = '00000') acct_ccid
                               ,a.account_id
                           FROM pn.pn_distributions_all                                 a
                               ,xxcus.xxcus_employee_conv_temp                          c
                               ,xxcus.xxcus_employee_conv_temp                          u
                               ,gl.gl_code_combinations@r12_to_fin.hsi.hughessupply.com g
                          WHERE a.created_by = c.user_id_r11(+)
                            AND a.last_updated_by = u.user_id_r11(+)
                            AND account_id = g.code_combination_id)
        LOOP
        
          UPDATE pn.pn_distributions_all
             SET org_id          = l_org
                ,created_by      = c_update.created_user
                ,last_updated_by = c_update.updated_user
                ,account_id      = nvl(c_update.acct_ccid, c_update.account_id)
           WHERE distribution_id = c_update.distribution_id;
        
        END LOOP;
      END;
    
      EXECUTE IMMEDIATE 'alter sequence ' || l_sequence || ' increment by ' ||
                        l_seq_r11;
    
      EXECUTE IMMEDIATE 'select ' || l_sequence || '.nextval from dual '
        INTO l_seq_r12;
    
      EXECUTE IMMEDIATE 'alter sequence ' || l_sequence || ' increment by 1 ';
    
      EXECUTE IMMEDIATE 'SELECT COUNT(*) FROM ' || l_table
        INTO l_count_r12;
    
      INSERT INTO xxcus.xxcus_pn_conv_tbl
        (table_name
        ,r11_count
        ,r12_count
        ,sequence
        ,seq_r11_num
        ,seq_r12_num
        ,creation_date)
      VALUES
        (l_table
        ,l_count_r11
        ,l_count_r12
        ,l_sequence
        ,l_seq_r11
        ,l_seq_r12
        ,SYSDATE);
    
      COMMIT;
    
    END;
  
    ------------------------------------------------------------------------     
    BEGIN
      l_table     := 'PN.PN_INSURANCE_REQUIREMENTS_ALL';
      l_sequence  := 'PN.PN_INSURANCE_REQUIREMENTS_S';
      l_sec       := 'Loading table ' || l_table;
      l_count_r11 := 0;
      l_count_r12 := 0;
    
      EXECUTE IMMEDIATE 'SELECT COUNT(*) FROM ' || l_table ||
                        '@R12_TO_FIN.HSI.HUGHESSUPPLY.COM'
        INTO l_count_r11;
    
      EXECUTE IMMEDIATE 'SELECT MAX(insurance_requirement_id) FROM ' || l_table ||
                        '@R12_TO_FIN.HSI.HUGHESSUPPLY.COM'
        INTO l_seq_r11;
    
      INSERT /*+ APPEND */
      INTO pn.pn_insurance_requirements_all
        SELECT *
          FROM pn.pn_insurance_requirements_all@r12_to_fin.hsi.hughessupply.com;
      COMMIT;
    
      BEGIN
        FOR c_update IN (SELECT insurance_requirement_id
                               ,created_by
                               ,last_updated_by
                               ,nvl(c.user_id, a.created_by) created_user
                               ,nvl(u.user_id, a.last_updated_by) updated_user
                           FROM pn.pn_insurance_requirements_all a
                               ,xxcus.xxcus_employee_conv_temp   c
                               ,xxcus.xxcus_employee_conv_temp   u
                          WHERE created_by = c.user_id_r11(+)
                            AND last_updated_by = u.user_id_r11(+))
        LOOP
        
          UPDATE pn.pn_insurance_requirements_all
             SET org_id          = l_org
                ,created_by      = c_update.created_user
                ,last_updated_by = c_update.updated_user
           WHERE insurance_requirement_id = c_update.insurance_requirement_id;
        
        END LOOP;
      END;
    
      EXECUTE IMMEDIATE 'alter sequence ' || l_sequence || ' increment by ' ||
                        l_seq_r11;
    
      EXECUTE IMMEDIATE 'select ' || l_sequence || '.nextval from dual '
        INTO l_seq_r12;
    
      EXECUTE IMMEDIATE 'alter sequence ' || l_sequence || ' increment by 1 ';
    
      EXECUTE IMMEDIATE 'SELECT COUNT(*) FROM ' || l_table
        INTO l_count_r12;
    
      INSERT INTO xxcus.xxcus_pn_conv_tbl
        (table_name
        ,r11_count
        ,r12_count
        ,sequence
        ,seq_r11_num
        ,seq_r12_num
        ,creation_date)
      VALUES
        (l_table
        ,l_count_r11
        ,l_count_r12
        ,l_sequence
        ,l_seq_r11
        ,l_seq_r12
        ,SYSDATE);
    
      COMMIT;
    
    END;
  
    ------------------------------------------------------------------------     
    BEGIN
      l_table     := 'PN.PN_INSUR_REQUIRE_HISTORY';
      l_sequence  := 'PN.PN_INSUR_REQUIRE_HISTORY_S';
      l_sec       := 'Loading table ' || l_table;
      l_count_r11 := 0;
      l_count_r12 := 0;
    
      EXECUTE IMMEDIATE 'SELECT COUNT(*) FROM ' || l_table ||
                        '@R12_TO_FIN.HSI.HUGHESSUPPLY.COM'
        INTO l_count_r11;
    
      EXECUTE IMMEDIATE 'SELECT MAX(insurance_history_id) FROM ' || l_table ||
                        '@R12_TO_FIN.HSI.HUGHESSUPPLY.COM'
        INTO l_seq_r11;
    
      INSERT /*+ APPEND */
      INTO pn.pn_insur_require_history
        SELECT *
          FROM pn.pn_insur_require_history@r12_to_fin.hsi.hughessupply.com;
      COMMIT;
    
      BEGIN
        FOR c_update IN (SELECT insurance_history_id
                               ,created_by
                               ,last_updated_by
                               ,nvl(c.user_id, a.created_by) created_user
                               ,nvl(u.user_id, a.last_updated_by) updated_user
                           FROM pn.pn_insur_require_history    a
                               ,xxcus.xxcus_employee_conv_temp c
                               ,xxcus.xxcus_employee_conv_temp u
                          WHERE created_by = c.user_id_r11(+)
                            AND last_updated_by = u.user_id_r11(+))
        LOOP
        
          UPDATE pn.pn_insur_require_history
             SET created_by      = c_update.created_user
                ,last_updated_by = c_update.updated_user
           WHERE insurance_history_id = c_update.insurance_history_id;
        
        END LOOP;
      END;
    
      EXECUTE IMMEDIATE 'alter sequence ' || l_sequence || ' increment by ' ||
                        l_seq_r11;
    
      EXECUTE IMMEDIATE 'select ' || l_sequence || '.nextval from dual '
        INTO l_seq_r12;
    
      EXECUTE IMMEDIATE 'alter sequence ' || l_sequence || ' increment by 1 ';
    
      EXECUTE IMMEDIATE 'SELECT COUNT(*) FROM ' || l_table
        INTO l_count_r12;
    
      INSERT INTO xxcus.xxcus_pn_conv_tbl
        (table_name
        ,r11_count
        ,r12_count
        ,sequence
        ,seq_r11_num
        ,seq_r12_num
        ,creation_date)
      VALUES
        (l_table
        ,l_count_r11
        ,l_count_r12
        ,l_sequence
        ,l_seq_r11
        ,l_seq_r12
        ,SYSDATE);
    
      COMMIT;
    
    END;
  
    ------------------------------------------------------------------------     
    BEGIN
      l_table     := 'PN.PN_LANDLORD_SERVICES_ALL';
      l_sequence  := 'PN.PN_LANDLORD_SERVICES_S';
      l_sec       := 'Loading table ' || l_table;
      l_count_r11 := 0;
      l_count_r12 := 0;
    
      EXECUTE IMMEDIATE 'SELECT COUNT(*) FROM ' || l_table ||
                        '@R12_TO_FIN.HSI.HUGHESSUPPLY.COM'
        INTO l_count_r11;
    
      EXECUTE IMMEDIATE 'SELECT MAX(landlord_service_id) FROM ' || l_table ||
                        '@R12_TO_FIN.HSI.HUGHESSUPPLY.COM'
        INTO l_seq_r11;
    
      INSERT /*+ APPEND */
      INTO pn.pn_landlord_services_all
        SELECT *
          FROM pn.pn_landlord_services_all@r12_to_fin.hsi.hughessupply.com;
      COMMIT;
    
      BEGIN
        FOR c_update IN (SELECT landlord_service_id
                               ,created_by
                               ,last_updated_by
                               ,nvl(c.user_id, a.created_by) created_user
                               ,nvl(u.user_id, a.last_updated_by) updated_user
                           FROM pn.pn_landlord_services_all    a
                               ,xxcus.xxcus_employee_conv_temp c
                               ,xxcus.xxcus_employee_conv_temp u
                          WHERE created_by = c.user_id_r11(+)
                            AND last_updated_by = u.user_id_r11(+))
        LOOP
        
          UPDATE pn.pn_landlord_services_all
             SET org_id          = l_org
                ,created_by      = c_update.created_user
                ,last_updated_by = c_update.updated_user
           WHERE landlord_service_id = c_update.landlord_service_id;
        
        END LOOP;
      END;
    
      EXECUTE IMMEDIATE 'alter sequence ' || l_sequence || ' increment by ' ||
                        l_seq_r11;
    
      EXECUTE IMMEDIATE 'select ' || l_sequence || '.nextval from dual '
        INTO l_seq_r12;
    
      EXECUTE IMMEDIATE 'alter sequence ' || l_sequence || ' increment by 1 ';
    
      EXECUTE IMMEDIATE 'SELECT COUNT(*) FROM ' || l_table
        INTO l_count_r12;
    
      INSERT INTO xxcus.xxcus_pn_conv_tbl
        (table_name
        ,r11_count
        ,r12_count
        ,sequence
        ,seq_r11_num
        ,seq_r12_num
        ,creation_date)
      VALUES
        (l_table
        ,l_count_r11
        ,l_count_r12
        ,l_sequence
        ,l_seq_r11
        ,l_seq_r12
        ,SYSDATE);
    
      COMMIT;
    
    END;
  
    ------------------------------------------------------------------------     
    BEGIN
      l_table     := 'PN.PN_LANDLORD_SERVICE_HISTORY';
      l_sequence  := 'PN.PN_LANDLORD_SERVICE_HISTORY_S';
      l_sec       := 'Loading table ' || l_table;
      l_count_r11 := 0;
      l_count_r12 := 0;
    
      EXECUTE IMMEDIATE 'SELECT COUNT(*) FROM ' || l_table ||
                        '@R12_TO_FIN.HSI.HUGHESSUPPLY.COM'
        INTO l_count_r11;
    
      EXECUTE IMMEDIATE 'SELECT MAX(landlord_service_history_id) FROM ' ||
                        l_table || '@R12_TO_FIN.HSI.HUGHESSUPPLY.COM'
        INTO l_seq_r11;
    
      INSERT /*+ APPEND */
      INTO pn.pn_landlord_service_history
        SELECT *
          FROM pn.pn_landlord_service_history@r12_to_fin.hsi.hughessupply.com;
      COMMIT;
    
      BEGIN
        FOR c_update IN (SELECT landlord_service_history_id
                               ,created_by
                               ,last_updated_by
                               ,nvl(c.user_id, a.created_by) created_user
                               ,nvl(u.user_id, a.last_updated_by) updated_user
                           FROM pn.pn_landlord_service_history a
                               ,xxcus.xxcus_employee_conv_temp c
                               ,xxcus.xxcus_employee_conv_temp u
                          WHERE created_by = c.user_id_r11(+)
                            AND last_updated_by = u.user_id_r11(+))
        LOOP
        
          UPDATE pn.pn_landlord_service_history
             SET created_by      = c_update.created_user
                ,last_updated_by = c_update.updated_user
           WHERE landlord_service_history_id =
                 c_update.landlord_service_history_id;
        
        END LOOP;
      END;
    
      EXECUTE IMMEDIATE 'alter sequence ' || l_sequence || ' increment by ' ||
                        l_seq_r11;
    
      EXECUTE IMMEDIATE 'select ' || l_sequence || '.nextval from dual '
        INTO l_seq_r12;
    
      EXECUTE IMMEDIATE 'alter sequence ' || l_sequence || ' increment by 1 ';
    
      EXECUTE IMMEDIATE 'SELECT COUNT(*) FROM ' || l_table
        INTO l_count_r12;
    
      INSERT INTO xxcus.xxcus_pn_conv_tbl
        (table_name
        ,r11_count
        ,r12_count
        ,sequence
        ,seq_r11_num
        ,seq_r12_num
        ,creation_date)
      VALUES
        (l_table
        ,l_count_r11
        ,l_count_r12
        ,l_sequence
        ,l_seq_r11
        ,l_seq_r12
        ,SYSDATE);
    
      COMMIT;
    
    END;
  
    ------------------------------------------------------------------------     
    BEGIN
      l_table     := 'PN.PN_LEASES_ALL';
      l_sequence  := 'PN.PN_LEASES_S';
      l_sec       := 'Loading table ' || l_table;
      l_count_r11 := 0;
      l_count_r12 := 0;
    
      EXECUTE IMMEDIATE 'SELECT COUNT(*) FROM ' || l_table ||
                        '@R12_TO_FIN.HSI.HUGHESSUPPLY.COM'
        INTO l_count_r11;
    
      EXECUTE IMMEDIATE 'SELECT MAX(lease_id) FROM ' || l_table ||
                        '@R12_TO_FIN.HSI.HUGHESSUPPLY.COM'
        INTO l_seq_r11;
    
      BEGIN
      
        FOR c_lease IN (SELECT lease_id
                              ,last_update_date
                              ,nvl(u.user_id, l.last_updated_by) last_updated_by
                              ,creation_date
                              ,nvl(c.user_id, l.created_by) created_by
                              ,last_update_login
                              ,NAME
                              ,lease_num
                              ,parent_lease_id
                              ,address_location_id
                              ,lease_type_code
                              ,payment_term_proration_rule
                              ,nvl(a.user_id, l.abstracted_by_user) abstracted_by_user
                              ,comments
                              ,status
                              ,org_id
                              ,lease_class_code
                              ,lease_status
                              ,location_id
                              ,customer_id
                          FROM pn.pn_leases_all@r12_to_fin.hsi.hughessupply.com l
                              ,xxcus.xxcus_employee_conv_temp                   c
                              ,xxcus.xxcus_employee_conv_temp                   u
                              ,xxcus.xxcus_employee_conv_temp                   a
                         WHERE l.created_by = c.user_id_r11(+)
                           AND l.last_updated_by = u.user_id_r11(+)
                           AND l.abstracted_by_user = a.user_id_r11(+))
        LOOP
        
          INSERT INTO pn.pn_leases_all
            (lease_id
            ,last_update_date
            ,last_updated_by
            ,creation_date
            ,created_by
            ,last_update_login
            ,NAME
            ,lease_num
            ,parent_lease_id
            ,address_location_id
            ,lease_type_code
            ,payment_term_proration_rule
            ,abstracted_by_user
            ,comments
            ,status
            ,org_id
            ,lease_class_code
            ,lease_status
            ,location_id
            ,customer_id)
          VALUES
            (c_lease.lease_id
            ,c_lease.last_update_date
            ,c_lease.last_updated_by
            ,c_lease.creation_date
            ,c_lease.created_by
            ,c_lease.last_update_login
            ,c_lease.name
            ,c_lease.lease_num
            ,c_lease.parent_lease_id
            ,c_lease.address_location_id
            ,c_lease.lease_type_code
            ,c_lease.payment_term_proration_rule
            ,c_lease.abstracted_by_user
            ,c_lease.comments
            ,c_lease.status
            ,l_org
            ,c_lease.lease_class_code
            ,c_lease.lease_status
            ,c_lease.location_id
            ,c_lease.customer_id);
        
        END LOOP;
      END;
    
      EXECUTE IMMEDIATE 'alter sequence ' || l_sequence || ' increment by ' ||
                        l_seq_r11;
    
      EXECUTE IMMEDIATE 'select ' || l_sequence || '.nextval from dual '
        INTO l_seq_r12;
    
      EXECUTE IMMEDIATE 'alter sequence ' || l_sequence || ' increment by 1 ';
    
      EXECUTE IMMEDIATE 'SELECT COUNT(*) FROM ' || l_table
        INTO l_count_r12;
    
      INSERT INTO xxcus.xxcus_pn_conv_tbl
        (table_name
        ,r11_count
        ,r12_count
        ,sequence
        ,seq_r11_num
        ,seq_r12_num
        ,creation_date)
      VALUES
        (l_table
        ,l_count_r11
        ,l_count_r12
        ,l_sequence
        ,l_seq_r11
        ,l_seq_r12
        ,SYSDATE);
    
      COMMIT;
    
    END;
  
    ------------------------------------------------------------------------     
    BEGIN
      l_table     := 'PN.PN_LEASE_CHANGES_ALL';
      l_sequence  := 'PN.PN_LEASE_CHANGES_S';
      l_sec       := 'Loading table ' || l_table;
      l_count_r11 := 0;
      l_count_r12 := 0;
    
      EXECUTE IMMEDIATE 'SELECT COUNT(*) FROM ' || l_table ||
                        '@R12_TO_FIN.HSI.HUGHESSUPPLY.COM'
        INTO l_count_r11;
    
      EXECUTE IMMEDIATE 'SELECT MAX(lease_change_id) FROM ' || l_table ||
                        '@R12_TO_FIN.HSI.HUGHESSUPPLY.COM'
        INTO l_seq_r11;
    
      INSERT /*+ APPEND */
      INTO pn.pn_lease_changes_all
        SELECT * FROM pn.pn_lease_changes_all@r12_to_fin.hsi.hughessupply.com;
      COMMIT;
    
      BEGIN
        FOR c_update IN (SELECT lease_change_id
                               ,created_by
                               ,last_updated_by
                               ,nvl(c.user_id, a.created_by) created_user
                               ,nvl(u.user_id, a.last_updated_by) updated_user
                               ,b.user_id abstracted_by_user
                               ,r.user_id responsible_user
                           FROM pn.pn_lease_changes_all        a
                               ,xxcus.xxcus_employee_conv_temp c
                               ,xxcus.xxcus_employee_conv_temp u
                               ,xxcus.xxcus_employee_conv_temp b
                               ,xxcus.xxcus_employee_conv_temp r
                          WHERE created_by = c.user_id_r11(+)
                            AND last_updated_by = u.user_id_r11(+)
                            AND abstracted_by_user = b.user_id_r11(+)
                            AND responsible_user = r.user_id_r11(+))
        LOOP
        
          UPDATE pn.pn_lease_changes_all
             SET org_id             = l_org
                ,created_by         = c_update.created_user
                ,last_updated_by    = c_update.updated_user
                ,abstracted_by_user = c_update.abstracted_by_user
                ,responsible_user   = c_update.responsible_user
           WHERE lease_change_id = c_update.lease_change_id;
        
        END LOOP;
      END;
    
      EXECUTE IMMEDIATE 'alter sequence ' || l_sequence || ' increment by ' ||
                        l_seq_r11;
    
      EXECUTE IMMEDIATE 'select ' || l_sequence || '.nextval from dual '
        INTO l_seq_r12;
    
      EXECUTE IMMEDIATE 'alter sequence ' || l_sequence || ' increment by 1 ';
    
      EXECUTE IMMEDIATE 'SELECT COUNT(*) FROM ' || l_table
        INTO l_count_r12;
    
      INSERT INTO xxcus.xxcus_pn_conv_tbl
        (table_name
        ,r11_count
        ,r12_count
        ,sequence
        ,seq_r11_num
        ,seq_r12_num
        ,creation_date)
      VALUES
        (l_table
        ,l_count_r11
        ,l_count_r12
        ,l_sequence
        ,l_seq_r11
        ,l_seq_r12
        ,SYSDATE);
    
      COMMIT;
    
    END;
  
    ------------------------------------------------------------------------     
    BEGIN
      l_table     := 'PN.PN_LEASE_DETAILS_ALL';
      l_sequence  := 'PN.PN_LEASE_DETAILS_S';
      l_sec       := 'Loading table ' || l_table;
      l_count_r11 := 0;
      l_count_r12 := 0;
    
      EXECUTE IMMEDIATE 'SELECT COUNT(*) FROM ' || l_table ||
                        '@R12_TO_FIN.HSI.HUGHESSUPPLY.COM'
        INTO l_count_r11;
    
      EXECUTE IMMEDIATE 'SELECT MAX(lease_detail_id) FROM ' || l_table ||
                        '@R12_TO_FIN.HSI.HUGHESSUPPLY.COM'
        INTO l_seq_r11;
    
      INSERT /*+ APPEND */
      INTO pn.pn_lease_details_all
        SELECT * FROM pn.pn_lease_details_all@r12_to_fin.hsi.hughessupply.com;
      COMMIT;
    
      BEGIN
        FOR c_details IN (SELECT lease_detail_id
                                ,d.responsible_user
                                ,d.created_by
                                ,d.last_updated_by
                                ,nvl(r.user_id, d.responsible_user) resp_user
                                ,nvl(c.user_id, d.created_by) created_user
                                ,nvl(u.user_id, d.last_updated_by) updated_user
                                ,(SELECT code_combination_id
                                    FROM gl.gl_code_combinations cc
                                   WHERE cc.segment1 = (CASE
                                           WHEN g.segment1 IN ('08', '31') THEN
                                            '0W'
                                           WHEN g.segment1 IN
                                                ('43', '44', '45', '46') THEN
                                            '42'
                                           WHEN g.segment1 = '12' THEN
                                            '11'
                                           ELSE
                                            g.segment1
                                         END)
                                     AND cc.segment2 = (CASE
                                           WHEN g.segment2 LIKE 'B0%' THEN
                                            'BW' || substr(g.segment2, 3, 3)
                                           ELSE
                                            g.segment2
                                         END)
                                     AND cc.segment3 = (CASE
                                           WHEN g.segment1 = '11' AND
                                                substr(g.segment2, 1, 1) = 'M' AND
                                                g.segment2 <> 'M0196' THEN
                                            '0000'
                                           WHEN g.segment1 IN ('01', '32') THEN
                                            'W010'
                                           WHEN g.segment1 = '42' THEN
                                            'C950'
                                           WHEN g.segment1 IN
                                                ('17', '18', '19', '37', '38') THEN
                                            '0000'
                                           ELSE
                                            g.segment3
                                         END)
                                     AND cc.segment4 = g.segment4
                                     AND cc.segment5 = g.segment5
                                     AND cc.segment6 = g.segment6
                                     AND cc.segment7 = '00000') acct_ccid
                                ,(SELECT code_combination_id
                                    FROM gl.gl_code_combinations cc
                                   WHERE cc.segment1 = (CASE
                                           WHEN ac.segment1 IN ('08', '31') THEN
                                            '0W'
                                           WHEN ac.segment1 IN
                                                ('43', '44', '45', '46') THEN
                                            '42'
                                           WHEN ac.segment1 = '12' THEN
                                            '11'
                                           ELSE
                                            ac.segment1
                                         END)
                                     AND cc.segment2 = (CASE
                                           WHEN ac.segment2 LIKE 'B0%' THEN
                                            'BW' || substr(ac.segment2, 3, 3)
                                           ELSE
                                            ac.segment2
                                         END)
                                     AND cc.segment3 = (CASE
                                           WHEN ac.segment1 = '11' AND
                                                substr(ac.segment2, 1, 1) = 'M' AND
                                                ac.segment2 <> 'M0196' THEN
                                            '0000'
                                           WHEN g.segment1 IN ('01', '32') THEN
                                            'W010'
                                           WHEN g.segment1 = '42' THEN
                                            'C950'
                                           WHEN g.segment1 IN
                                                ('17', '18', '19', '37', '38') THEN
                                            '0000'
                                           ELSE
                                            ac.segment3
                                         END)
                                     AND cc.segment4 = ac.segment4
                                     AND cc.segment5 = ac.segment5
                                     AND cc.segment6 = ac.segment6
                                     AND cc.segment7 = '00000') accrual_ccid
                                ,(SELECT code_combination_id
                                    FROM gl.gl_code_combinations cc
                                   WHERE cc.segment1 = (CASE
                                           WHEN rc.segment1 IN ('08', '31') THEN
                                            '0W'
                                           WHEN rc.segment1 IN
                                                ('43', '44', '45', '46') THEN
                                            '42'
                                           WHEN rc.segment1 = '12' THEN
                                            '11'
                                           ELSE
                                            rc.segment1
                                         END)
                                     AND cc.segment2 = (CASE
                                           WHEN rc.segment2 LIKE 'B0%' THEN
                                            'BW' || substr(rc.segment2, 3, 3)
                                           ELSE
                                            rc.segment2
                                         END)
                                     AND cc.segment3 = (CASE
                                           WHEN rc.segment1 = '11' AND
                                                substr(rc.segment2, 1, 1) = 'M' AND
                                                rc.segment2 <> 'M0196' THEN
                                            '0000'
                                           WHEN g.segment1 IN ('01', '32') THEN
                                            'W010'
                                           WHEN g.segment1 = '42' THEN
                                            'C950'
                                           WHEN g.segment1 IN
                                                ('17', '18', '19', '37', '38') THEN
                                            '0000'
                                           ELSE
                                            rc.segment3
                                         END)
                                     AND cc.segment4 = rc.segment4
                                     AND cc.segment5 = rc.segment5
                                     AND cc.segment6 = rc.segment6
                                     AND cc.segment7 = '00000') recv_ccid
                            FROM pn.pn_lease_details_all                                 d
                                ,xxcus.xxcus_employee_conv_temp                          r
                                ,xxcus.xxcus_employee_conv_temp                          c
                                ,xxcus.xxcus_employee_conv_temp                          u
                                ,gl.gl_code_combinations@r12_to_fin.hsi.hughessupply.com g
                                ,gl.gl_code_combinations@r12_to_fin.hsi.hughessupply.com ac
                                ,gl.gl_code_combinations@r12_to_fin.hsi.hughessupply.com rc
                           WHERE d.created_by = c.user_id_r11(+)
                             AND d.last_updated_by = u.user_id_r11(+)
                             AND d.responsible_user = r.user_id_r11(+)
                             AND d.expense_account_id = g.code_combination_id(+)
                             AND d.accrual_account_id =
                                 ac.code_combination_id(+)
                             AND d.receivable_account_id =
                                 rc.code_combination_id(+))
        LOOP
        
          UPDATE pn.pn_lease_details_all
             SET org_id                = l_org
                ,responsible_user      = c_details.resp_user
                ,created_by            = c_details.created_user
                ,last_updated_by       = c_details.updated_user
                ,expense_account_id    = c_details.acct_ccid
                ,accrual_account_id    = c_details.accrual_ccid
                ,receivable_account_id = c_details.recv_ccid
           WHERE lease_detail_id = c_details.lease_detail_id;
        
        END LOOP;
      END;
    
      EXECUTE IMMEDIATE 'alter sequence ' || l_sequence || ' increment by ' ||
                        l_seq_r11;
    
      EXECUTE IMMEDIATE 'select ' || l_sequence || '.nextval from dual '
        INTO l_seq_r12;
    
      EXECUTE IMMEDIATE 'alter sequence ' || l_sequence || ' increment by 1 ';
    
      EXECUTE IMMEDIATE 'SELECT COUNT(*) FROM ' || l_table
        INTO l_count_r12;
    
      INSERT INTO xxcus.xxcus_pn_conv_tbl
        (table_name
        ,r11_count
        ,r12_count
        ,sequence
        ,seq_r11_num
        ,seq_r12_num
        ,creation_date)
      VALUES
        (l_table
        ,l_count_r11
        ,l_count_r12
        ,l_sequence
        ,l_seq_r11
        ,l_seq_r12
        ,SYSDATE);
    
      COMMIT;
    
    END;
  
    ------------------------------------------------------------------------     
    BEGIN
      l_table     := 'PN.PN_LEASE_DETAILS_HISTORY';
      l_sequence  := 'PN.PN_LEASE_DETAILS_HISTORY_S';
      l_sec       := 'Loading table ' || l_table;
      l_count_r11 := 0;
      l_count_r12 := 0;
    
      EXECUTE IMMEDIATE 'SELECT COUNT(*) FROM ' || l_table ||
                        '@R12_TO_FIN.HSI.HUGHESSUPPLY.COM'
        INTO l_count_r11;
    
      EXECUTE IMMEDIATE 'SELECT MAX(detail_history_id) FROM ' || l_table ||
                        '@R12_TO_FIN.HSI.HUGHESSUPPLY.COM'
        INTO l_seq_r11;
    
      INSERT /*+ APPEND */
      INTO pn.pn_lease_details_history
        SELECT *
          FROM pn.pn_lease_details_history@r12_to_fin.hsi.hughessupply.com;
      COMMIT;
    
      BEGIN
        FOR c_update IN (SELECT detail_history_id
                               ,a.created_by
                               ,a.last_updated_by
                               ,nvl(c.user_id, a.created_by) created_user
                               ,nvl(u.user_id, a.last_updated_by) updated_user
                               ,r.user_id responsible_user
                               ,(SELECT code_combination_id
                                   FROM gl.gl_code_combinations cc
                                  WHERE cc.segment1 = (CASE
                                          WHEN g.segment1 IN ('08', '31') THEN
                                           '0W'
                                          WHEN g.segment1 IN
                                               ('43', '44', '45', '46') THEN
                                           '42'
                                          WHEN g.segment1 = '12' THEN
                                           '11'
                                          ELSE
                                           g.segment1
                                        END)
                                    AND cc.segment2 = (CASE
                                          WHEN g.segment2 LIKE 'B0%' THEN
                                           'BW' || substr(g.segment2, 3, 3)
                                          ELSE
                                           g.segment2
                                        END)
                                    AND cc.segment3 = (CASE
                                          WHEN g.segment1 = '11' AND
                                               substr(g.segment2, 1, 1) = 'M' AND
                                               g.segment2 <> 'M0196' THEN
                                           '0000'
                                          WHEN g.segment1 IN ('01', '32') THEN
                                           'W010'
                                          WHEN g.segment1 = '42' THEN
                                           'C950'
                                          WHEN g.segment1 IN
                                               ('17', '18', '19', '37', '38') THEN
                                           '0000'
                                          ELSE
                                           g.segment3
                                        END)
                                    AND cc.segment4 = g.segment4
                                    AND cc.segment5 = g.segment5
                                    AND cc.segment6 = g.segment6
                                    AND cc.segment7 = '00000') acct_ccid
                               ,(SELECT code_combination_id
                                   FROM gl.gl_code_combinations cc
                                  WHERE cc.segment1 = (CASE
                                          WHEN ac.segment1 IN ('08', '31') THEN
                                           '0W'
                                          WHEN ac.segment1 IN
                                               ('43', '44', '45', '46') THEN
                                           '42'
                                          WHEN ac.segment1 = '12' THEN
                                           '11'
                                          ELSE
                                           ac.segment1
                                        END)
                                    AND cc.segment2 = (CASE
                                          WHEN ac.segment2 LIKE 'B0%' THEN
                                           'BW' || substr(ac.segment2, 3, 3)
                                          ELSE
                                           ac.segment2
                                        END)
                                    AND cc.segment3 = (CASE
                                          WHEN ac.segment1 = '11' AND
                                               substr(ac.segment2, 1, 1) = 'M' AND
                                               ac.segment2 <> 'M0196' THEN
                                           '0000'
                                          WHEN g.segment1 IN ('01', '32') THEN
                                           'W010'
                                          WHEN g.segment1 = '42' THEN
                                           'C950'
                                          WHEN g.segment1 IN
                                               ('17', '18', '19', '37', '38') THEN
                                           '0000'
                                          ELSE
                                           ac.segment3
                                        END)
                                    AND cc.segment4 = ac.segment4
                                    AND cc.segment5 = ac.segment5
                                    AND cc.segment6 = ac.segment6
                                    AND cc.segment7 = '00000') accrual_ccid
                               ,(SELECT code_combination_id
                                   FROM gl.gl_code_combinations cc
                                  WHERE cc.segment1 = (CASE
                                          WHEN rc.segment1 IN ('08', '31') THEN
                                           '0W'
                                          WHEN rc.segment1 IN
                                               ('43', '44', '45', '46') THEN
                                           '42'
                                          WHEN rc.segment1 = '12' THEN
                                           '11'
                                          ELSE
                                           rc.segment1
                                        END)
                                    AND cc.segment2 = (CASE
                                          WHEN rc.segment2 LIKE 'B0%' THEN
                                           'BW' || substr(rc.segment2, 3, 3)
                                          ELSE
                                           rc.segment2
                                        END)
                                    AND cc.segment3 = (CASE
                                          WHEN rc.segment1 = '11' AND
                                               substr(rc.segment2, 1, 1) = 'M' AND
                                               rc.segment2 <> 'M0196' THEN
                                           '0000'
                                          WHEN g.segment1 IN ('01', '32') THEN
                                           'W010'
                                          WHEN g.segment1 = '42' THEN
                                           'C950'
                                          WHEN g.segment1 IN
                                               ('17', '18', '19', '37', '38') THEN
                                           '0000'
                                          ELSE
                                           rc.segment3
                                        END)
                                    AND cc.segment4 = rc.segment4
                                    AND cc.segment5 = rc.segment5
                                    AND cc.segment6 = rc.segment6
                                    AND cc.segment7 = '00000') recv_ccid
                           FROM pn.pn_lease_details_history                             a
                               ,xxcus.xxcus_employee_conv_temp                          c
                               ,xxcus.xxcus_employee_conv_temp                          u
                               ,xxcus.xxcus_employee_conv_temp                          r
                               ,gl.gl_code_combinations@r12_to_fin.hsi.hughessupply.com g
                               ,gl.gl_code_combinations@r12_to_fin.hsi.hughessupply.com ac
                               ,gl.gl_code_combinations@r12_to_fin.hsi.hughessupply.com rc
                          WHERE a.created_by = c.user_id_r11(+)
                            AND a.last_updated_by = u.user_id_r11(+)
                            AND a.responsible_user = r.user_id_r11(+)
                            AND a.expense_account_id = g.code_combination_id(+)
                            AND a.accrual_account_id = ac.code_combination_id(+)
                            AND a.receivable_account_id =
                                rc.code_combination_id(+))
        LOOP
        
          UPDATE pn.pn_lease_details_history
             SET created_by            = c_update.created_user
                ,last_updated_by       = c_update.updated_user
                ,responsible_user      = c_update.responsible_user
                ,expense_account_id    = c_update.acct_ccid
                ,accrual_account_id    = c_update.accrual_ccid
                ,receivable_account_id = c_update.recv_ccid
           WHERE detail_history_id = c_update.detail_history_id;
        
        END LOOP;
      END;
    
      EXECUTE IMMEDIATE 'alter sequence ' || l_sequence || ' increment by ' ||
                        l_seq_r11;
    
      EXECUTE IMMEDIATE 'select ' || l_sequence || '.nextval from dual '
        INTO l_seq_r12;
    
      EXECUTE IMMEDIATE 'alter sequence ' || l_sequence || ' increment by 1 ';
    
      EXECUTE IMMEDIATE 'SELECT COUNT(*) FROM ' || l_table
        INTO l_count_r12;
    
      INSERT INTO xxcus.xxcus_pn_conv_tbl
        (table_name
        ,r11_count
        ,r12_count
        ,sequence
        ,seq_r11_num
        ,seq_r12_num
        ,creation_date)
      VALUES
        (l_table
        ,l_count_r11
        ,l_count_r12
        ,l_sequence
        ,l_seq_r11
        ,l_seq_r12
        ,SYSDATE);
    
      COMMIT;
    
    END;
  
    ------------------------------------------------------------------------     
    BEGIN
      l_table     := 'PN.PN_LEASE_MILESTONES_ALL';
      l_sequence  := 'PN.PN_LEASE_MILESTONES_S';
      l_sec       := 'Loading table ' || l_table;
      l_count_r11 := 0;
      l_count_r12 := 0;
    
      EXECUTE IMMEDIATE 'SELECT COUNT(*) FROM ' || l_table ||
                        '@R12_TO_FIN.HSI.HUGHESSUPPLY.COM'
        INTO l_count_r11;
    
      EXECUTE IMMEDIATE 'SELECT MAX(lease_milestone_id) FROM ' || l_table ||
                        '@R12_TO_FIN.HSI.HUGHESSUPPLY.COM'
        INTO l_seq_r11;
    
      INSERT /*+ APPEND */
      INTO pn.pn_lease_milestones_all
        SELECT *
          FROM pn.pn_lease_milestones_all@r12_to_fin.hsi.hughessupply.com;
      COMMIT;
    
      BEGIN
        FOR c_details IN (SELECT lease_milestone_id
                                ,nvl(r.user_id, d.user_id) user_id
                                ,nvl(c.user_id, d.created_by) created_user
                                ,nvl(u.user_id, d.last_updated_by) updated_user
                            FROM pn.pn_lease_milestones_all     d
                                ,xxcus.xxcus_employee_conv_temp r
                                ,xxcus.xxcus_employee_conv_temp c
                                ,xxcus.xxcus_employee_conv_temp u
                           WHERE created_by = c.user_id_r11(+)
                             AND last_updated_by = u.user_id_r11(+)
                             AND d.user_id = r.user_id_r11(+))
        LOOP
        
          UPDATE pn.pn_lease_milestones_all
             SET org_id          = l_org
                ,user_id         = c_details.user_id
                ,created_by      = c_details.created_user
                ,last_updated_by = c_details.updated_user
           WHERE lease_milestone_id = c_details.lease_milestone_id;
        
        END LOOP;
      END;
    
      EXECUTE IMMEDIATE 'alter sequence ' || l_sequence || ' increment by ' ||
                        l_seq_r11;
    
      EXECUTE IMMEDIATE 'select ' || l_sequence || '.nextval from dual '
        INTO l_seq_r12;
    
      EXECUTE IMMEDIATE 'alter sequence ' || l_sequence || ' increment by 1 ';
    
      EXECUTE IMMEDIATE 'SELECT COUNT(*) FROM ' || l_table
        INTO l_count_r12;
    
      INSERT INTO xxcus.xxcus_pn_conv_tbl
        (table_name
        ,r11_count
        ,r12_count
        ,sequence
        ,seq_r11_num
        ,seq_r12_num
        ,creation_date)
      VALUES
        (l_table
        ,l_count_r11
        ,l_count_r12
        ,l_sequence
        ,l_seq_r11
        ,l_seq_r12
        ,SYSDATE);
    
      COMMIT;
    
    END;
  
    ------------------------------------------------------------------------     
    BEGIN
      l_table     := 'PN.PN_LEASE_TRANSACTIONS_ALL';
      l_sequence  := 'PN.PN_LEASE_TRANSACTIONS_S';
      l_sec       := 'Loading table ' || l_table;
      l_count_r11 := 0;
      l_count_r12 := 0;
    
      EXECUTE IMMEDIATE 'SELECT COUNT(*) FROM ' || l_table ||
                        '@R12_TO_FIN.HSI.HUGHESSUPPLY.COM'
        INTO l_count_r11;
    
      EXECUTE IMMEDIATE 'SELECT MAX(lease_transaction_id) FROM ' || l_table ||
                        '@R12_TO_FIN.HSI.HUGHESSUPPLY.COM'
        INTO l_seq_r11;
    
      INSERT /*+ APPEND */
      INTO pn.pn_lease_transactions_all
        SELECT *
          FROM pn.pn_lease_transactions_all@r12_to_fin.hsi.hughessupply.com;
      COMMIT;
    
      BEGIN
        FOR c_details IN (SELECT lease_transaction_id
                                ,nvl(c.user_id, created_by) created_user
                                ,nvl(u.user_id, last_updated_by) updated_user
                            FROM pn.pn_lease_transactions_all   d
                                ,xxcus.xxcus_employee_conv_temp c
                                ,xxcus.xxcus_employee_conv_temp u
                           WHERE created_by = c.user_id_r11(+)
                             AND last_updated_by = u.user_id_r11(+))
        LOOP
        
          UPDATE pn.pn_lease_transactions_all
             SET org_id          = l_org
                ,created_by      = c_details.created_user
                ,last_updated_by = c_details.updated_user
           WHERE lease_transaction_id = c_details.lease_transaction_id;
        
        END LOOP;
      END;
    
      EXECUTE IMMEDIATE 'alter sequence ' || l_sequence || ' increment by ' ||
                        l_seq_r11;
    
      EXECUTE IMMEDIATE 'select ' || l_sequence || '.nextval from dual '
        INTO l_seq_r12;
    
      EXECUTE IMMEDIATE 'alter sequence ' || l_sequence || ' increment by 1 ';
    
      EXECUTE IMMEDIATE 'SELECT COUNT(*) FROM ' || l_table
        INTO l_count_r12;
    
      INSERT INTO xxcus.xxcus_pn_conv_tbl
        (table_name
        ,r11_count
        ,r12_count
        ,sequence
        ,seq_r11_num
        ,seq_r12_num
        ,creation_date)
      VALUES
        (l_table
        ,l_count_r11
        ,l_count_r12
        ,l_sequence
        ,l_seq_r11
        ,l_seq_r12
        ,SYSDATE);
    
      COMMIT;
    
    END;
  
    ------------------------------------------------------------------------     
    BEGIN
      l_table     := 'PN.PN_LOCATIONS_ALL';
      l_sequence  := 'PN.PN_LOCATIONS_S';
      l_sec       := 'Loading table ' || l_table;
      l_count_r11 := 0;
      l_count_r12 := 0;
    
      EXECUTE IMMEDIATE 'SELECT COUNT(*) FROM ' || l_table ||
                        '@R12_TO_FIN.HSI.HUGHESSUPPLY.COM'
        INTO l_count_r11;
    
      EXECUTE IMMEDIATE 'SELECT MAX(location_id) FROM ' || l_table ||
                        '@R12_TO_FIN.HSI.HUGHESSUPPLY.COM'
        INTO l_seq_r11;
    
      INSERT /*+ APPEND */
      INTO pn.pn_locations_all
        SELECT * FROM pn.pn_locations_all@r12_to_fin.hsi.hughessupply.com;
      COMMIT;
    
      BEGIN
        FOR c_details IN (SELECT location_id
                                ,nvl(c.user_id, created_by) created_user
                                ,nvl(u.user_id, last_updated_by) updated_user
                            FROM pn.pn_locations_all            d
                                ,xxcus.xxcus_employee_conv_temp c
                                ,xxcus.xxcus_employee_conv_temp u
                           WHERE created_by = c.user_id_r11(+)
                             AND last_updated_by = u.user_id_r11(+))
        LOOP
        
          UPDATE pn.pn_locations_all
             SET org_id          = l_org
                ,created_by      = c_details.created_user
                ,last_updated_by = c_details.updated_user
           WHERE location_id = c_details.location_id;
        
        END LOOP;
      END;
    
      EXECUTE IMMEDIATE 'alter sequence ' || l_sequence || ' increment by ' ||
                        l_seq_r11;
    
      EXECUTE IMMEDIATE 'select ' || l_sequence || '.nextval from dual '
        INTO l_seq_r12;
    
      EXECUTE IMMEDIATE 'alter sequence ' || l_sequence || ' increment by 1 ';
    
      EXECUTE IMMEDIATE 'SELECT COUNT(*) FROM ' || l_table
        INTO l_count_r12;
    
      INSERT INTO xxcus.xxcus_pn_conv_tbl
        (table_name
        ,r11_count
        ,r12_count
        ,sequence
        ,seq_r11_num
        ,seq_r12_num
        ,creation_date)
      VALUES
        (l_table
        ,l_count_r11
        ,l_count_r12
        ,l_sequence
        ,l_seq_r11
        ,l_seq_r12
        ,SYSDATE);
    
      COMMIT;
    
    END;
  
    ------------------------------------------------------------------------     
    BEGIN
      l_table     := 'PN.PN_LOCATION_FEATURES_ALL';
      l_sequence  := 'PN.PN_LOCATION_FEATURES_S';
      l_sec       := 'Loading table ' || l_table;
      l_count_r11 := 0;
      l_count_r12 := 0;
    
      EXECUTE IMMEDIATE 'SELECT COUNT(*) FROM ' || l_table ||
                        '@R12_TO_FIN.HSI.HUGHESSUPPLY.COM'
        INTO l_count_r11;
    
      EXECUTE IMMEDIATE 'SELECT MAX(location_feature_id) FROM ' || l_table ||
                        '@R12_TO_FIN.HSI.HUGHESSUPPLY.COM'
        INTO l_seq_r11;
    
      INSERT /*+ APPEND */
      INTO pn.pn_location_features_all
        SELECT *
          FROM pn.pn_location_features_all@r12_to_fin.hsi.hughessupply.com;
      COMMIT;
    
      BEGIN
        FOR c_details IN (SELECT location_feature_id
                                ,nvl(c.user_id, created_by) created_user
                                ,nvl(u.user_id, last_updated_by) updated_user
                            FROM pn.pn_location_features_all    d
                                ,xxcus.xxcus_employee_conv_temp c
                                ,xxcus.xxcus_employee_conv_temp u
                           WHERE created_by = c.user_id_r11(+)
                             AND last_updated_by = u.user_id_r11(+))
        LOOP
        
          UPDATE pn.pn_location_features_all
             SET org_id          = l_org
                ,created_by      = c_details.created_user
                ,last_updated_by = c_details.updated_user
           WHERE location_feature_id = c_details.location_feature_id;
        
        END LOOP;
      END;
    
      EXECUTE IMMEDIATE 'alter sequence ' || l_sequence || ' increment by ' ||
                        l_seq_r11;
    
      EXECUTE IMMEDIATE 'select ' || l_sequence || '.nextval from dual '
        INTO l_seq_r12;
    
      EXECUTE IMMEDIATE 'alter sequence ' || l_sequence || ' increment by 1 ';
    
      EXECUTE IMMEDIATE 'SELECT COUNT(*) FROM ' || l_table
        INTO l_count_r12;
    
      INSERT INTO xxcus.xxcus_pn_conv_tbl
        (table_name
        ,r11_count
        ,r12_count
        ,sequence
        ,seq_r11_num
        ,seq_r12_num
        ,creation_date)
      VALUES
        (l_table
        ,l_count_r11
        ,l_count_r12
        ,l_sequence
        ,l_seq_r11
        ,l_seq_r12
        ,SYSDATE);
    
      COMMIT;
    
    END;
  
    ------------------------------------------------------------------------     
    BEGIN
      l_table     := 'PN.PN_NOTE_DETAILS';
      l_sequence  := 'PN.PN_NOTE_DETAILS_S';
      l_sec       := 'Loading table ' || l_table;
      l_count_r11 := 0;
      l_count_r12 := 0;
    
      EXECUTE IMMEDIATE 'SELECT COUNT(*) FROM ' || l_table ||
                        '@R12_TO_FIN.HSI.HUGHESSUPPLY.COM'
        INTO l_count_r11;
    
      EXECUTE IMMEDIATE 'SELECT MAX(note_detail_id) FROM ' || l_table ||
                        '@R12_TO_FIN.HSI.HUGHESSUPPLY.COM'
        INTO l_seq_r11;
    
      INSERT /*+ APPEND */
      INTO pn.pn_note_details
        SELECT * FROM pn.pn_note_details@r12_to_fin.hsi.hughessupply.com;
      COMMIT;
    
      BEGIN
        FOR c_details IN (SELECT note_detail_id
                                ,nvl(c.user_id, created_by) created_user
                                ,nvl(u.user_id, last_updated_by) updated_user
                            FROM pn.pn_note_details             d
                                ,xxcus.xxcus_employee_conv_temp c
                                ,xxcus.xxcus_employee_conv_temp u
                           WHERE created_by = c.user_id_r11(+)
                             AND last_updated_by = u.user_id_r11(+))
        LOOP
        
          UPDATE pn.pn_note_details
             SET created_by      = c_details.created_user
                ,last_updated_by = c_details.updated_user
           WHERE note_detail_id = c_details.note_detail_id;
        
        END LOOP;
      END;
    
      EXECUTE IMMEDIATE 'alter sequence ' || l_sequence || ' increment by ' ||
                        l_seq_r11;
    
      EXECUTE IMMEDIATE 'select ' || l_sequence || '.nextval from dual '
        INTO l_seq_r12;
    
      EXECUTE IMMEDIATE 'alter sequence ' || l_sequence || ' increment by 1 ';
    
      EXECUTE IMMEDIATE 'SELECT COUNT(*) FROM ' || l_table
        INTO l_count_r12;
    
      INSERT INTO xxcus.xxcus_pn_conv_tbl
        (table_name
        ,r11_count
        ,r12_count
        ,sequence
        ,seq_r11_num
        ,seq_r12_num
        ,creation_date)
      VALUES
        (l_table
        ,l_count_r11
        ,l_count_r12
        ,l_sequence
        ,l_seq_r11
        ,l_seq_r12
        ,SYSDATE);
    
      COMMIT;
    
    END;
  
    ------------------------------------------------------------------------     
    BEGIN
      l_table     := 'PN.PN_NOTE_HEADERS';
      l_sequence  := 'PN.PN_NOTE_HEADERS_S';
      l_sec       := 'Loading table ' || l_table;
      l_count_r11 := 0;
      l_count_r12 := 0;
    
      EXECUTE IMMEDIATE 'SELECT COUNT(*) FROM ' || l_table ||
                        '@R12_TO_FIN.HSI.HUGHESSUPPLY.COM'
        INTO l_count_r11;
    
      EXECUTE IMMEDIATE 'SELECT MAX(note_header_id) FROM ' || l_table ||
                        '@R12_TO_FIN.HSI.HUGHESSUPPLY.COM'
        INTO l_seq_r11;
    
      INSERT /*+ APPEND */
      INTO pn.pn_note_headers
        SELECT * FROM pn.pn_note_headers@r12_to_fin.hsi.hughessupply.com;
      COMMIT;
    
      BEGIN
        FOR c_details IN (SELECT note_header_id
                                ,nvl(c.user_id, created_by) created_user
                                ,nvl(u.user_id, last_updated_by) updated_user
                            FROM pn.pn_note_headers             d
                                ,xxcus.xxcus_employee_conv_temp c
                                ,xxcus.xxcus_employee_conv_temp u
                           WHERE created_by = c.user_id_r11(+)
                             AND last_updated_by = u.user_id_r11(+))
        LOOP
        
          UPDATE pn.pn_note_headers
             SET created_by      = c_details.created_user
                ,last_updated_by = c_details.updated_user
           WHERE note_header_id = c_details.note_header_id;
        
        END LOOP;
      END;
    
      EXECUTE IMMEDIATE 'alter sequence ' || l_sequence || ' increment by ' ||
                        l_seq_r11;
    
      EXECUTE IMMEDIATE 'select ' || l_sequence || '.nextval from dual '
        INTO l_seq_r12;
    
      EXECUTE IMMEDIATE 'alter sequence ' || l_sequence || ' increment by 1 ';
    
      EXECUTE IMMEDIATE 'SELECT COUNT(*) FROM ' || l_table
        INTO l_count_r12;
    
      INSERT INTO xxcus.xxcus_pn_conv_tbl
        (table_name
        ,r11_count
        ,r12_count
        ,sequence
        ,seq_r11_num
        ,seq_r12_num
        ,creation_date)
      VALUES
        (l_table
        ,l_count_r11
        ,l_count_r12
        ,l_sequence
        ,l_seq_r11
        ,l_seq_r12
        ,SYSDATE);
    
      COMMIT;
    
    END;
  
    ------------------------------------------------------------------------     
    BEGIN
      l_table     := 'PN.PN_OPTIONS_ALL';
      l_sequence  := 'PN.PN_OPTIONS_S';
      l_sec       := 'Loading table ' || l_table;
      l_count_r11 := 0;
      l_count_r12 := 0;
    
      EXECUTE IMMEDIATE 'SELECT COUNT(*) FROM ' || l_table ||
                        '@R12_TO_FIN.HSI.HUGHESSUPPLY.COM'
        INTO l_count_r11;
    
      EXECUTE IMMEDIATE 'SELECT MAX(option_id) FROM ' || l_table ||
                        '@R12_TO_FIN.HSI.HUGHESSUPPLY.COM'
        INTO l_seq_r11;
    
      INSERT /*+ APPEND */
      INTO pn.pn_options_all
        SELECT * FROM pn.pn_options_all@r12_to_fin.hsi.hughessupply.com;
      COMMIT;
    
      BEGIN
        FOR c_details IN (SELECT option_id
                                ,nvl(c.user_id, created_by) created_user
                                ,nvl(u.user_id, last_updated_by) updated_user
                            FROM pn.pn_options_all              d
                                ,xxcus.xxcus_employee_conv_temp c
                                ,xxcus.xxcus_employee_conv_temp u
                           WHERE created_by = c.user_id_r11(+)
                             AND last_updated_by = u.user_id_r11(+))
        LOOP
        
          UPDATE pn.pn_options_all
             SET org_id          = l_org
                ,created_by      = c_details.created_user
                ,last_updated_by = c_details.updated_user
           WHERE option_id = c_details.option_id;
        
        END LOOP;
      END;
    
      EXECUTE IMMEDIATE 'alter sequence ' || l_sequence || ' increment by ' ||
                        l_seq_r11;
    
      EXECUTE IMMEDIATE 'select ' || l_sequence || '.nextval from dual '
        INTO l_seq_r12;
    
      EXECUTE IMMEDIATE 'alter sequence ' || l_sequence || ' increment by 1 ';
    
      EXECUTE IMMEDIATE 'SELECT COUNT(*) FROM ' || l_table
        INTO l_count_r12;
    
      INSERT INTO xxcus.xxcus_pn_conv_tbl
        (table_name
        ,r11_count
        ,r12_count
        ,sequence
        ,seq_r11_num
        ,seq_r12_num
        ,creation_date)
      VALUES
        (l_table
        ,l_count_r11
        ,l_count_r12
        ,l_sequence
        ,l_seq_r11
        ,l_seq_r12
        ,SYSDATE);
    
      COMMIT;
    
    END;
  
    ------------------------------------------------------------------------     
    BEGIN
      l_table     := 'PN.PN_OPTIONS_HISTORY';
      l_sequence  := 'PN.PN_OPTIONS_HISTORY_S';
      l_sec       := 'Loading table ' || l_table;
      l_count_r11 := 0;
      l_count_r12 := 0;
    
      EXECUTE IMMEDIATE 'SELECT COUNT(*) FROM ' || l_table ||
                        '@R12_TO_FIN.HSI.HUGHESSUPPLY.COM'
        INTO l_count_r11;
    
      EXECUTE IMMEDIATE 'SELECT MAX(option_history_id) FROM ' || l_table ||
                        '@R12_TO_FIN.HSI.HUGHESSUPPLY.COM'
        INTO l_seq_r11;
    
      INSERT /*+ APPEND */
      INTO pn.pn_options_history
        SELECT * FROM pn.pn_options_history@r12_to_fin.hsi.hughessupply.com;
      COMMIT;
    
      BEGIN
        FOR c_details IN (SELECT option_history_id
                                ,nvl(c.user_id, created_by) created_user
                                ,nvl(u.user_id, last_updated_by) updated_user
                            FROM pn.pn_options_history          d
                                ,xxcus.xxcus_employee_conv_temp c
                                ,xxcus.xxcus_employee_conv_temp u
                           WHERE created_by = c.user_id_r11(+)
                             AND last_updated_by = u.user_id_r11(+))
        LOOP
        
          UPDATE pn.pn_options_history
             SET created_by      = c_details.created_user
                ,last_updated_by = c_details.updated_user
           WHERE option_history_id = c_details.option_history_id;
        
        END LOOP;
      END;
    
      EXECUTE IMMEDIATE 'alter sequence ' || l_sequence || ' increment by ' ||
                        l_seq_r11;
    
      EXECUTE IMMEDIATE 'select ' || l_sequence || '.nextval from dual '
        INTO l_seq_r12;
    
      EXECUTE IMMEDIATE 'alter sequence ' || l_sequence || ' increment by 1 ';
    
      EXECUTE IMMEDIATE 'SELECT COUNT(*) FROM ' || l_table
        INTO l_count_r12;
    
      INSERT INTO xxcus.xxcus_pn_conv_tbl
        (table_name
        ,r11_count
        ,r12_count
        ,sequence
        ,seq_r11_num
        ,seq_r12_num
        ,creation_date)
      VALUES
        (l_table
        ,l_count_r11
        ,l_count_r12
        ,l_sequence
        ,l_seq_r11
        ,l_seq_r12
        ,SYSDATE);
    
      COMMIT;
    
    END;
  
    ------------------------------------------------------------------------     
    BEGIN
      l_table     := 'PN.PN_PAYMENT_ITEMS_ALL';
      l_sequence  := 'PN.PN_PAYMENT_ITEMS_S';
      l_sec       := 'Loading table ' || l_table;
      l_count_r11 := 0;
      l_count_r12 := 0;
    
      EXECUTE IMMEDIATE 'SELECT COUNT(*) FROM ' || l_table ||
                        '@R12_TO_FIN.HSI.HUGHESSUPPLY.COM'
        INTO l_count_r11;
    
      EXECUTE IMMEDIATE 'SELECT MAX(payment_item_id) FROM ' || l_table ||
                        '@R12_TO_FIN.HSI.HUGHESSUPPLY.COM'
        INTO l_seq_r11;
    
      INSERT /*+ APPEND */
      INTO pn.pn_payment_items_all
        SELECT * FROM pn.pn_payment_items_all@r12_to_fin.hsi.hughessupply.com;
      COMMIT;
    
      BEGIN
        FOR c_details IN (SELECT payment_item_id
                                ,nvl(c.user_id, d.created_by) created_user
                                ,nvl(u.user_id, d.last_updated_by) updated_user
                                ,s.vendor_id
                                ,s.vendor_site_id
                                ,(CASE
                                   WHEN customer_id IS NOT NULL THEN
                                    l_cust_id
                                   ELSE
                                    NULL
                                 END) customer_id
                                ,(CASE
                                   WHEN customer_site_use_id IS NOT NULL THEN
                                    l_cust_site_id
                                   ELSE
                                    NULL
                                 END) customer_site_use_id
                            FROM pn.pn_payment_items_all        d
                                ,xxcus.xxcus_employee_conv_temp c
                                ,xxcus.xxcus_employee_conv_temp u
                                ,ap.ap_supplier_sites_all       s
                           WHERE d.created_by = c.user_id_r11(+)
                             AND d.last_updated_by = u.user_id_r11(+)
                             AND d.vendor_id = s.attribute15(+)
                             AND d.vendor_site_id = s.attribute14(+))
        LOOP
        
          UPDATE pn.pn_payment_items_all
             SET org_id               = l_org
                ,set_of_books_id      = l_ledger
                ,created_by           = c_details.created_user
                ,last_updated_by      = c_details.updated_user
                ,vendor_site_id       = c_details.vendor_site_id
                ,vendor_id            = c_details.vendor_id
                ,customer_id          = c_details.customer_id
                ,customer_site_use_id = c_details.customer_site_use_id
           WHERE payment_item_id = c_details.payment_item_id;
        
        END LOOP;
      END;
    
      EXECUTE IMMEDIATE 'alter sequence ' || l_sequence || ' increment by ' ||
                        l_seq_r11;
    
      EXECUTE IMMEDIATE 'select ' || l_sequence || '.nextval from dual '
        INTO l_seq_r12;
    
      EXECUTE IMMEDIATE 'alter sequence ' || l_sequence || ' increment by 1 ';
    
      EXECUTE IMMEDIATE 'SELECT COUNT(*) FROM ' || l_table
        INTO l_count_r12;
    
      INSERT INTO xxcus.xxcus_pn_conv_tbl
        (table_name
        ,r11_count
        ,r12_count
        ,sequence
        ,seq_r11_num
        ,seq_r12_num
        ,creation_date)
      VALUES
        (l_table
        ,l_count_r11
        ,l_count_r12
        ,l_sequence
        ,l_seq_r11
        ,l_seq_r12
        ,SYSDATE);
    
      COMMIT;
    
    END;
  
    ------------------------------------------------------------------------     
    BEGIN
      l_table     := 'PN.PN_PAYMENT_SCHEDULES_ALL';
      l_sequence  := 'PN.PN_PAYMENT_SCHEDULES_S';
      l_sec       := 'Loading table ' || l_table;
      l_count_r11 := 0;
      l_count_r12 := 0;
    
      EXECUTE IMMEDIATE 'SELECT COUNT(*) FROM ' || l_table ||
                        '@R12_TO_FIN.HSI.HUGHESSUPPLY.COM'
        INTO l_count_r11;
    
      EXECUTE IMMEDIATE 'SELECT MAX(payment_schedule_id) FROM ' || l_table ||
                        '@R12_TO_FIN.HSI.HUGHESSUPPLY.COM'
        INTO l_seq_r11;
    
      INSERT /*+ APPEND */
      INTO pn.pn_payment_schedules_all
        SELECT *
          FROM pn.pn_payment_schedules_all@r12_to_fin.hsi.hughessupply.com;
      COMMIT;
    
      BEGIN
        FOR c_details IN (SELECT payment_schedule_id
                                ,nvl(c.user_id, d.created_by) created_user
                                ,nvl(u.user_id, d.last_updated_by) updated_user
                            FROM pn.pn_payment_schedules_all    d
                                ,xxcus.xxcus_employee_conv_temp c
                                ,xxcus.xxcus_employee_conv_temp u
                           WHERE created_by = c.user_id_r11(+)
                             AND last_updated_by = u.user_id_r11(+))
        LOOP
        
          UPDATE pn.pn_payment_schedules_all
             SET org_id          = l_org
                ,created_by      = c_details.created_user
                ,last_updated_by = c_details.updated_user
           WHERE payment_schedule_id = c_details.payment_schedule_id;
        
        END LOOP;
      END;
    
      EXECUTE IMMEDIATE 'alter sequence ' || l_sequence || ' increment by ' ||
                        l_seq_r11;
    
      EXECUTE IMMEDIATE 'select ' || l_sequence || '.nextval from dual '
        INTO l_seq_r12;
    
      EXECUTE IMMEDIATE 'alter sequence ' || l_sequence || ' increment by 1 ';
    
      EXECUTE IMMEDIATE 'SELECT COUNT(*) FROM ' || l_table
        INTO l_count_r12;
    
      INSERT INTO xxcus.xxcus_pn_conv_tbl
        (table_name
        ,r11_count
        ,r12_count
        ,sequence
        ,seq_r11_num
        ,seq_r12_num
        ,creation_date)
      VALUES
        (l_table
        ,l_count_r11
        ,l_count_r12
        ,l_sequence
        ,l_seq_r11
        ,l_seq_r12
        ,SYSDATE);
    
      COMMIT;
    
    END;
  
    ------------------------------------------------------------------------  
  
    --Error in ebizdev prior to 12.1.3
    --Error in START WITH  ORA-01400: cannot insert NULL into ("PN"."PN_PAYMENT_TERMS_ALL"."OPEX_RECON_ID")  
    BEGIN
      l_table     := 'PN.PN_PAYMENT_TERMS_ALL';
      l_sequence  := 'PN.PN_PAYMENT_TERMS_S';
      l_sec       := 'Loading table ' || l_table;
      l_count_r11 := 0;
      l_count_r12 := 0;
    
      EXECUTE IMMEDIATE 'SELECT COUNT(*) FROM ' || l_table ||
                        '@R12_TO_FIN.HSI.HUGHESSUPPLY.COM'
        INTO l_count_r11;
    
      EXECUTE IMMEDIATE 'SELECT MAX(payment_term_id) FROM ' || l_table ||
                        '@R12_TO_FIN.HSI.HUGHESSUPPLY.COM'
        INTO l_seq_r11;
    
      INSERT /*+ APPEND */
      INTO pn.pn_payment_terms_all
        SELECT * FROM pn.pn_payment_terms_all@r12_to_fin.hsi.hughessupply.com;
      COMMIT;
    
      BEGIN
        FOR c_details IN (SELECT payment_term_id
                                ,nvl(c.user_id, d.created_by) created_user
                                ,nvl(u.user_id, d.last_updated_by) updated_user
                                ,s.vendor_id
                                ,s.vendor_site_id
                                ,(CASE
                                   WHEN customer_id IS NOT NULL THEN
                                    l_cust_id
                                   ELSE
                                    NULL
                                 END) customer_id
                                ,(CASE
                                   WHEN customer_site_use_id IS NOT NULL THEN
                                    l_cust_site_id
                                   ELSE
                                    NULL
                                 END) customer_site_use_id
                                ,(CASE
                                   WHEN cust_trx_type_id IS NOT NULL THEN
                                    l_trx_type_id
                                   ELSE
                                    NULL
                                 END) cust_trx_type_id
                            FROM pn.pn_payment_terms_all        d
                                ,xxcus.xxcus_employee_conv_temp c
                                ,xxcus.xxcus_employee_conv_temp u
                                ,ap.ap_supplier_sites_all       s
                           WHERE d.created_by = c.user_id_r11(+)
                             AND d.last_updated_by = u.user_id_r11(+)
                             AND d.vendor_id = s.attribute15(+)
                             AND d.vendor_site_id = s.attribute14(+))
        LOOP
        
          UPDATE pn.pn_payment_terms_all
             SET org_id               = l_org
                ,set_of_books_id      = l_ledger
                ,created_by           = c_details.created_user
                ,last_updated_by      = c_details.updated_user
                ,vendor_site_id       = c_details.vendor_site_id
                ,vendor_id            = c_details.vendor_id
                ,customer_id          = c_details.customer_id
                ,customer_site_use_id = c_details.customer_site_use_id
                ,cust_trx_type_id     = c_details.cust_trx_type_id
           WHERE payment_term_id = c_details.payment_term_id;
        
        END LOOP;
      END;
    
      EXECUTE IMMEDIATE 'alter sequence ' || l_sequence || ' increment by ' ||
                        l_seq_r11;
    
      EXECUTE IMMEDIATE 'select ' || l_sequence || '.nextval from dual '
        INTO l_seq_r12;
    
      EXECUTE IMMEDIATE 'alter sequence ' || l_sequence || ' increment by 1 ';
    
      EXECUTE IMMEDIATE 'SELECT COUNT(*) FROM ' || l_table
        INTO l_count_r12;
    
      INSERT INTO xxcus.xxcus_pn_conv_tbl
        (table_name
        ,r11_count
        ,r12_count
        ,sequence
        ,seq_r11_num
        ,seq_r12_num
        ,creation_date)
      VALUES
        (l_table
        ,l_count_r11
        ,l_count_r12
        ,l_sequence
        ,l_seq_r11
        ,l_seq_r12
        ,SYSDATE);
    
      COMMIT;
    
    END;
  
    ------------------------------------------------------------------------     
    BEGIN
      l_table     := 'PN.PN_PHONES_ALL';
      l_sequence  := 'PN.PN_PHONES_S';
      l_sec       := 'Loading table ' || l_table;
      l_count_r11 := 0;
      l_count_r12 := 0;
    
      EXECUTE IMMEDIATE 'SELECT COUNT(*) FROM ' || l_table ||
                        '@R12_TO_FIN.HSI.HUGHESSUPPLY.COM'
        INTO l_count_r11;
    
      EXECUTE IMMEDIATE 'SELECT MAX(phone_id) FROM ' || l_table ||
                        '@R12_TO_FIN.HSI.HUGHESSUPPLY.COM'
        INTO l_seq_r11;
    
      INSERT /*+ APPEND */
      INTO pn.pn_phones_all
        SELECT * FROM pn.pn_phones_all@r12_to_fin.hsi.hughessupply.com;
      COMMIT;
    
      BEGIN
        FOR c_details IN (SELECT contact_id
                                ,nvl(c.user_id, d.created_by) created_user
                                ,nvl(u.user_id, d.last_updated_by) updated_user
                            FROM pn.pn_phones_all               d
                                ,xxcus.xxcus_employee_conv_temp c
                                ,xxcus.xxcus_employee_conv_temp u
                           WHERE d.created_by = c.user_id_r11(+)
                             AND d.last_updated_by = u.user_id_r11(+))
        LOOP
        
          UPDATE pn.pn_phones_all
             SET org_id          = l_org
                ,created_by      = c_details.created_user
                ,last_updated_by = c_details.updated_user
           WHERE contact_id = c_details.contact_id;
        
        END LOOP;
      END;
    
      EXECUTE IMMEDIATE 'alter sequence ' || l_sequence || ' increment by ' ||
                        l_seq_r11;
    
      EXECUTE IMMEDIATE 'select ' || l_sequence || '.nextval from dual '
        INTO l_seq_r12;
    
      EXECUTE IMMEDIATE 'alter sequence ' || l_sequence || ' increment by 1 ';
    
      EXECUTE IMMEDIATE 'SELECT COUNT(*) FROM ' || l_table
        INTO l_count_r12;
    
      INSERT INTO xxcus.xxcus_pn_conv_tbl
        (table_name
        ,r11_count
        ,r12_count
        ,sequence
        ,seq_r11_num
        ,seq_r12_num
        ,creation_date)
      VALUES
        (l_table
        ,l_count_r11
        ,l_count_r12
        ,l_sequence
        ,l_seq_r11
        ,l_seq_r12
        ,SYSDATE);
    
      COMMIT;
    
    END;
  
    ------------------------------------------------------------------------     
    BEGIN
      l_table     := 'PN.PN_PROPERTIES_ALL';
      l_sequence  := 'PN.PN_PROPERTIES_S';
      l_sec       := 'Loading table ' || l_table;
      l_count_r11 := 0;
      l_count_r12 := 0;
    
      EXECUTE IMMEDIATE 'SELECT COUNT(*) FROM ' || l_table ||
                        '@R12_TO_FIN.HSI.HUGHESSUPPLY.COM'
        INTO l_count_r11;
    
      EXECUTE IMMEDIATE 'SELECT MAX(property_id) FROM ' || l_table ||
                        '@R12_TO_FIN.HSI.HUGHESSUPPLY.COM'
        INTO l_seq_r11;
    
      INSERT /*+ APPEND */
      INTO pn.pn_properties_all
        SELECT * FROM pn.pn_properties_all@r12_to_fin.hsi.hughessupply.com;
      COMMIT;
    
      BEGIN
        FOR c_details IN (SELECT property_id
                                ,nvl(c.user_id, d.created_by) created_user
                                ,nvl(u.user_id, d.last_updated_by) updated_user
                            FROM pn.pn_properties_all           d
                                ,xxcus.xxcus_employee_conv_temp c
                                ,xxcus.xxcus_employee_conv_temp u
                           WHERE d.created_by = c.user_id_r11(+)
                             AND d.last_updated_by = u.user_id_r11(+))
        LOOP
        
          UPDATE pn.pn_properties_all
             SET org_id          = l_org
                ,created_by      = c_details.created_user
                ,last_updated_by = c_details.updated_user
           WHERE property_id = c_details.property_id;
        
        END LOOP;
      END;
    
      EXECUTE IMMEDIATE 'alter sequence ' || l_sequence || ' increment by ' ||
                        l_seq_r11;
    
      EXECUTE IMMEDIATE 'select ' || l_sequence || '.nextval from dual '
        INTO l_seq_r12;
    
      EXECUTE IMMEDIATE 'alter sequence ' || l_sequence || ' increment by 1 ';
    
      EXECUTE IMMEDIATE 'SELECT COUNT(*) FROM ' || l_table
        INTO l_count_r12;
    
      INSERT INTO xxcus.xxcus_pn_conv_tbl
        (table_name
        ,r11_count
        ,r12_count
        ,sequence
        ,seq_r11_num
        ,seq_r12_num
        ,creation_date)
      VALUES
        (l_table
        ,l_count_r11
        ,l_count_r12
        ,l_sequence
        ,l_seq_r11
        ,l_seq_r12
        ,SYSDATE);
    
      COMMIT;
    
    END;
  
    ------------------------------------------------------------------------     
    BEGIN
      l_table     := 'PN.PN_RIGHTS_ALL';
      l_sequence  := 'PN.PN_RIGHTS_S';
      l_sec       := 'Loading table ' || l_table;
      l_count_r11 := 0;
      l_count_r12 := 0;
    
      EXECUTE IMMEDIATE 'SELECT COUNT(*) FROM ' || l_table ||
                        '@R12_TO_FIN.HSI.HUGHESSUPPLY.COM'
        INTO l_count_r11;
    
      EXECUTE IMMEDIATE 'SELECT MAX(right_id) FROM ' || l_table ||
                        '@R12_TO_FIN.HSI.HUGHESSUPPLY.COM'
        INTO l_seq_r11;
    
      INSERT /*+ APPEND */
      INTO pn.pn_rights_all
        SELECT * FROM pn.pn_rights_all@r12_to_fin.hsi.hughessupply.com;
      COMMIT;
    
      BEGIN
        FOR c_details IN (SELECT right_id
                                ,nvl(c.user_id, d.created_by) created_user
                                ,nvl(u.user_id, d.last_updated_by) updated_user
                            FROM pn.pn_rights_all               d
                                ,xxcus.xxcus_employee_conv_temp c
                                ,xxcus.xxcus_employee_conv_temp u
                           WHERE d.created_by = c.user_id_r11(+)
                             AND d.last_updated_by = u.user_id_r11(+))
        LOOP
        
          UPDATE pn.pn_rights_all
             SET org_id          = l_org
                ,created_by      = c_details.created_user
                ,last_updated_by = c_details.updated_user
           WHERE right_id = c_details.right_id;
        
        END LOOP;
      END;
    
      EXECUTE IMMEDIATE 'alter sequence ' || l_sequence || ' increment by ' ||
                        l_seq_r11;
    
      EXECUTE IMMEDIATE 'select ' || l_sequence || '.nextval from dual '
        INTO l_seq_r12;
    
      EXECUTE IMMEDIATE 'alter sequence ' || l_sequence || ' increment by 1 ';
    
      EXECUTE IMMEDIATE 'SELECT COUNT(*) FROM ' || l_table
        INTO l_count_r12;
    
      INSERT INTO xxcus.xxcus_pn_conv_tbl
        (table_name
        ,r11_count
        ,r12_count
        ,sequence
        ,seq_r11_num
        ,seq_r12_num
        ,creation_date)
      VALUES
        (l_table
        ,l_count_r11
        ,l_count_r12
        ,l_sequence
        ,l_seq_r11
        ,l_seq_r12
        ,SYSDATE);
    
      COMMIT;
    
    END;
  
    ------------------------------------------------------------------------     
    BEGIN
      l_table     := 'PN.PN_RIGHTS_HISTORY';
      l_sequence  := 'PN.PN_RIGHTS_HISTORY_S';
      l_sec       := 'Loading table ' || l_table;
      l_count_r11 := 0;
      l_count_r12 := 0;
    
      EXECUTE IMMEDIATE 'SELECT COUNT(*) FROM ' || l_table ||
                        '@R12_TO_FIN.HSI.HUGHESSUPPLY.COM'
        INTO l_count_r11;
    
      EXECUTE IMMEDIATE 'SELECT MAX(right_id) FROM ' || l_table ||
                        '@R12_TO_FIN.HSI.HUGHESSUPPLY.COM'
        INTO l_seq_r11;
    
      INSERT /*+ APPEND */
      INTO pn.pn_rights_history
        SELECT * FROM pn.pn_rights_history@r12_to_fin.hsi.hughessupply.com;
      COMMIT;
    
      BEGIN
        FOR c_details IN (SELECT right_history_id
                                ,nvl(c.user_id, d.created_by) created_user
                                ,nvl(u.user_id, d.last_updated_by) updated_user
                            FROM pn.pn_rights_history           d
                                ,xxcus.xxcus_employee_conv_temp c
                                ,xxcus.xxcus_employee_conv_temp u
                           WHERE d.created_by = c.user_id_r11(+)
                             AND d.last_updated_by = u.user_id_r11(+))
        LOOP
        
          UPDATE pn.pn_rights_history
             SET created_by      = c_details.created_user
                ,last_updated_by = c_details.updated_user
           WHERE right_history_id = c_details.right_history_id;
        
        END LOOP;
      END;
    
      EXECUTE IMMEDIATE 'alter sequence ' || l_sequence || ' increment by ' ||
                        l_seq_r11;
    
      EXECUTE IMMEDIATE 'select ' || l_sequence || '.nextval from dual '
        INTO l_seq_r12;
    
      EXECUTE IMMEDIATE 'alter sequence ' || l_sequence || ' increment by 1 ';
    
      EXECUTE IMMEDIATE 'SELECT COUNT(*) FROM ' || l_table
        INTO l_count_r12;
    
      INSERT INTO xxcus.xxcus_pn_conv_tbl
        (table_name
        ,r11_count
        ,r12_count
        ,sequence
        ,seq_r11_num
        ,seq_r12_num
        ,creation_date)
      VALUES
        (l_table
        ,l_count_r11
        ,l_count_r12
        ,l_sequence
        ,l_seq_r11
        ,l_seq_r12
        ,SYSDATE);
    
      COMMIT;
    
    END;
  
    ------------------------------------------------------------------------     
    BEGIN
      l_table     := 'PN.PN_SET_MILESTONES';
      l_sequence  := 'PN.PN_SET_MILESTONES_S';
      l_sec       := 'Loading table ' || l_table;
      l_count_r11 := 0;
      l_count_r12 := 0;
    
      EXECUTE IMMEDIATE 'SELECT COUNT(*) FROM ' || l_table ||
                        '@R12_TO_FIN.HSI.HUGHESSUPPLY.COM'
        INTO l_count_r11;
    
      EXECUTE IMMEDIATE 'SELECT MAX(milestones_set_id) FROM ' || l_table ||
                        '@R12_TO_FIN.HSI.HUGHESSUPPLY.COM'
        INTO l_seq_r11;
    
      INSERT /*+ APPEND */
      INTO pn.pn_set_milestones
        SELECT * FROM pn.pn_set_milestones@r12_to_fin.hsi.hughessupply.com;
      COMMIT;
    
      BEGIN
        FOR c_details IN (SELECT milestones_set_id
                                ,nvl(c.user_id, d.created_by) created_user
                                ,nvl(u.user_id, d.last_updated_by) updated_user
                                ,nvl(a.user_id, d.user_id) user_id
                            FROM pn.pn_set_milestones           d
                                ,xxcus.xxcus_employee_conv_temp c
                                ,xxcus.xxcus_employee_conv_temp u
                                ,xxcus.xxcus_employee_conv_temp a
                           WHERE d.created_by = c.user_id_r11(+)
                             AND d.last_updated_by = u.user_id_r11(+)
                             AND d.last_updated_by = a.user_id_r11(+))
        LOOP
        
          UPDATE pn.pn_set_milestones
             SET created_by      = c_details.created_user
                ,last_updated_by = c_details.updated_user
                ,user_id         = c_details.user_id
           WHERE milestones_set_id = c_details.milestones_set_id;
        
        END LOOP;
      END;
    
      EXECUTE IMMEDIATE 'alter sequence ' || l_sequence || ' increment by ' ||
                        l_seq_r11;
    
      EXECUTE IMMEDIATE 'select ' || l_sequence || '.nextval from dual '
        INTO l_seq_r12;
    
      EXECUTE IMMEDIATE 'alter sequence ' || l_sequence || ' increment by 1 ';
    
      EXECUTE IMMEDIATE 'SELECT COUNT(*) FROM ' || l_table
        INTO l_count_r12;
    
      INSERT INTO xxcus.xxcus_pn_conv_tbl
        (table_name
        ,r11_count
        ,r12_count
        ,sequence
        ,seq_r11_num
        ,seq_r12_num
        ,creation_date)
      VALUES
        (l_table
        ,l_count_r11
        ,l_count_r12
        ,l_sequence
        ,l_seq_r11
        ,l_seq_r12
        ,SYSDATE);
    
      COMMIT;
    
    END;
  
    ------------------------------------------------------------------------     
    BEGIN
      l_table     := 'PN.PN_SET_TYPES';
      l_sequence  := 'PN.PN_SET_TYPES_S';
      l_sec       := 'Loading table ' || l_table;
      l_count_r11 := 0;
      l_count_r12 := 0;
    
      EXECUTE IMMEDIATE 'SELECT COUNT(*) FROM ' || l_table ||
                        '@R12_TO_FIN.HSI.HUGHESSUPPLY.COM'
        INTO l_count_r11;
    
      EXECUTE IMMEDIATE 'SELECT MAX(set_id) FROM ' || l_table ||
                        '@R12_TO_FIN.HSI.HUGHESSUPPLY.COM'
        INTO l_seq_r11;
    
      INSERT /*+ APPEND */
      INTO pn.pn_set_types
        SELECT * FROM pn.pn_set_types@r12_to_fin.hsi.hughessupply.com;
      COMMIT;
    
      BEGIN
        FOR c_details IN (SELECT set_id
                                ,nvl(c.user_id, d.created_by) created_user
                                ,nvl(u.user_id, d.last_updated_by) updated_user
                            FROM pn.pn_set_types                d
                                ,xxcus.xxcus_employee_conv_temp c
                                ,xxcus.xxcus_employee_conv_temp u
                           WHERE d.created_by = c.user_id_r11(+)
                             AND d.last_updated_by = u.user_id_r11(+))
        LOOP
        
          UPDATE pn.pn_set_types
             SET created_by      = c_details.created_user
                ,last_updated_by = c_details.updated_user
           WHERE set_id = c_details.set_id;
        
        END LOOP;
      END;
    
      EXECUTE IMMEDIATE 'alter sequence ' || l_sequence || ' increment by ' ||
                        l_seq_r11;
    
      EXECUTE IMMEDIATE 'select ' || l_sequence || '.nextval from dual '
        INTO l_seq_r12;
    
      EXECUTE IMMEDIATE 'alter sequence ' || l_sequence || ' increment by 1 ';
    
      EXECUTE IMMEDIATE 'SELECT COUNT(*) FROM ' || l_table
        INTO l_count_r12;
    
      INSERT INTO xxcus.xxcus_pn_conv_tbl
        (table_name
        ,r11_count
        ,r12_count
        ,sequence
        ,seq_r11_num
        ,seq_r12_num
        ,creation_date)
      VALUES
        (l_table
        ,l_count_r11
        ,l_count_r12
        ,l_sequence
        ,l_seq_r11
        ,l_seq_r12
        ,SYSDATE);
    
      COMMIT;
    
    END;
  
    ------------------------------------------------------------------------ 
  
    BEGIN
      l_table     := 'PN.PN_SPACE_ASSIGN_CUST_ALL';
      l_sequence  := 'PN.PN_SPACE_ASSIGN_CUST_S';
      l_sec       := 'Loading table ' || l_table;
      l_count_r11 := 0;
      l_count_r12 := 0;
    
      EXECUTE IMMEDIATE 'SELECT COUNT(*) FROM ' || l_table ||
                        '@R12_TO_FIN.HSI.HUGHESSUPPLY.COM'
        INTO l_count_r11;
    
      EXECUTE IMMEDIATE 'SELECT MAX(cust_space_assign_id) FROM ' || l_table ||
                        '@R12_TO_FIN.HSI.HUGHESSUPPLY.COM'
        INTO l_seq_r11;
    
      INSERT /*+ APPEND */
      INTO pn.pn_space_assign_cust_all
        SELECT *
          FROM pn.pn_space_assign_cust_all@r12_to_fin.hsi.hughessupply.com;
      COMMIT;
    
      BEGIN
        FOR c_details IN (SELECT cust_space_assign_id
                                ,nvl(c.user_id, d.created_by) created_user
                                ,nvl(u.user_id, d.last_updated_by) updated_user
                                ,(CASE
                                   WHEN cust_account_id IS NOT NULL THEN
                                    l_cust_id
                                   ELSE
                                    NULL
                                 END) cust_account_id
                                ,(CASE
                                   WHEN site_use_id IS NOT NULL THEN
                                    l_cust_site_id
                                   ELSE
                                    NULL
                                 END) site_use_id
                                ,(SELECT code_combination_id
                                    FROM gl.gl_code_combinations cc
                                   WHERE cc.segment1 = (CASE
                                           WHEN g.segment1 IN ('08', '31') THEN
                                            '0W'
                                           WHEN g.segment1 IN
                                                ('43', '44', '45', '46') THEN
                                            '42'
                                           WHEN g.segment1 = '12' THEN
                                            '11'
                                           ELSE
                                            g.segment1
                                         END)
                                     AND cc.segment2 = (CASE
                                           WHEN g.segment2 LIKE 'B0%' THEN
                                            'BW' || substr(g.segment2, 3, 3)
                                           ELSE
                                            g.segment2
                                         END)
                                     AND cc.segment3 = (CASE
                                           WHEN g.segment1 = '11' AND
                                                substr(g.segment2, 1, 1) = 'M' AND
                                                g.segment2 <> 'M0196' THEN
                                            '0000'
                                           WHEN g.segment1 IN ('01', '32') THEN
                                            'W010'
                                           WHEN g.segment1 = '42' THEN
                                            'C950'
                                           WHEN g.segment1 IN
                                                ('17', '18', '19', '37', '38') THEN
                                            '0000'
                                           ELSE
                                            g.segment3
                                         END)
                                     AND cc.segment4 = g.segment4
                                     AND cc.segment5 = g.segment5
                                     AND cc.segment6 = g.segment6
                                     AND cc.segment7 = '00000') acct_ccid
                            FROM pn.pn_space_assign_cust_all                             d
                                ,xxcus.xxcus_employee_conv_temp                          c
                                ,xxcus.xxcus_employee_conv_temp                          u
                                ,gl.gl_code_combinations@r12_to_fin.hsi.hughessupply.com g
                           WHERE d.created_by = c.user_id_r11(+)
                             AND d.last_updated_by = u.user_id_r11(+)
                             AND d.expense_account_id = g.code_combination_id(+))
        LOOP
        
          UPDATE pn.pn_space_assign_cust_all
             SET org_id             = l_org
                ,created_by         = c_details.created_user
                ,last_updated_by    = c_details.updated_user
                ,expense_account_id = c_details.acct_ccid
                ,cust_account_id    = c_details.cust_account_id
                ,site_use_id        = c_details.site_use_id
           WHERE cust_space_assign_id = c_details.cust_space_assign_id;
        
        END LOOP;
      END;
    
      EXECUTE IMMEDIATE 'alter sequence ' || l_sequence || ' increment by ' ||
                        l_seq_r11;
    
      EXECUTE IMMEDIATE 'select ' || l_sequence || '.nextval from dual '
        INTO l_seq_r12;
    
      EXECUTE IMMEDIATE 'alter sequence ' || l_sequence || ' increment by 1 ';
    
      EXECUTE IMMEDIATE 'SELECT COUNT(*) FROM ' || l_table
        INTO l_count_r12;
    
      INSERT INTO xxcus.xxcus_pn_conv_tbl
        (table_name
        ,r11_count
        ,r12_count
        ,sequence
        ,seq_r11_num
        ,seq_r12_num
        ,creation_date)
      VALUES
        (l_table
        ,l_count_r11
        ,l_count_r12
        ,l_sequence
        ,l_seq_r11
        ,l_seq_r12
        ,SYSDATE);
    
      COMMIT;
    
    END;
  
    ------------------------------------------------------------------------     
    BEGIN
      l_table     := 'PN.PN_SPACE_ASSIGN_EMP_ALL';
      l_sequence  := 'PN.PN_SPACE_ASSIGN_EMP_S';
      l_sec       := 'Loading table ' || l_table;
      l_count_r11 := 0;
      l_count_r12 := 0;
    
      EXECUTE IMMEDIATE 'SELECT COUNT(*) FROM ' || l_table ||
                        '@R12_TO_FIN.HSI.HUGHESSUPPLY.COM'
        INTO l_count_r11;
    
      EXECUTE IMMEDIATE 'SELECT MAX(emp_space_assign_id) FROM ' || l_table ||
                        '@R12_TO_FIN.HSI.HUGHESSUPPLY.COM'
        INTO l_seq_r11;
    
      INSERT /*+ APPEND */
      INTO pn.pn_space_assign_emp_all
        SELECT *
          FROM pn.pn_space_assign_emp_all@r12_to_fin.hsi.hughessupply.com;
      COMMIT;
    
      BEGIN
        FOR c_details IN (SELECT emp_space_assign_id
                                ,nvl(c.user_id, d.created_by) created_user
                                ,nvl(u.user_id, d.last_updated_by) updated_user
                            FROM pn.pn_space_assign_emp_all     d
                                ,xxcus.xxcus_employee_conv_temp c
                                ,xxcus.xxcus_employee_conv_temp u
                           WHERE d.created_by = c.user_id_r11(+)
                             AND d.last_updated_by = u.user_id_r11(+))
        LOOP
        
          UPDATE pn.pn_space_assign_emp_all
             SET org_id          = l_org
                ,created_by      = c_details.created_user
                ,last_updated_by = c_details.updated_user
           WHERE emp_space_assign_id = c_details.emp_space_assign_id;
        
        END LOOP;
      END;
    
      EXECUTE IMMEDIATE 'alter sequence ' || l_sequence || ' increment by ' ||
                        l_seq_r11;
    
      EXECUTE IMMEDIATE 'select ' || l_sequence || '.nextval from dual '
        INTO l_seq_r12;
    
      EXECUTE IMMEDIATE 'alter sequence ' || l_sequence || ' increment by 1 ';
    
      EXECUTE IMMEDIATE 'SELECT COUNT(*) FROM ' || l_table
        INTO l_count_r12;
    
      INSERT INTO xxcus.xxcus_pn_conv_tbl
        (table_name
        ,r11_count
        ,r12_count
        ,sequence
        ,seq_r11_num
        ,seq_r12_num
        ,creation_date)
      VALUES
        (l_table
        ,l_count_r11
        ,l_count_r12
        ,l_sequence
        ,l_seq_r11
        ,l_seq_r12
        ,SYSDATE);
    
      COMMIT;
    
    END;
  
    ------------------------------------------------------------------------ 
  
    BEGIN
      l_table     := 'PN.PN_TENANCIES_ALL';
      l_sequence  := 'PN.PN_TENANCIES_S';
      l_sec       := 'Loading table ' || l_table;
      l_count_r11 := 0;
      l_count_r12 := 0;
    
      EXECUTE IMMEDIATE 'SELECT COUNT(*) FROM ' || l_table ||
                        '@R12_TO_FIN.HSI.HUGHESSUPPLY.COM'
        INTO l_count_r11;
    
      EXECUTE IMMEDIATE 'SELECT MAX(tenancy_id) FROM ' || l_table ||
                        '@R12_TO_FIN.HSI.HUGHESSUPPLY.COM'
        INTO l_seq_r11;
    
      INSERT /*+ APPEND */
      INTO pn.pn_tenancies_all
        SELECT * FROM pn.pn_tenancies_all@r12_to_fin.hsi.hughessupply.com;
      COMMIT;
    
      BEGIN
        FOR c_details IN (SELECT tenancy_id
                                ,nvl(c.user_id, d.created_by) created_user
                                ,nvl(u.user_id, d.last_updated_by) updated_user
                                ,(CASE
                                   WHEN customer_id IS NOT NULL THEN
                                    l_cust_id
                                   ELSE
                                    NULL
                                 END) customer_id
                                ,(CASE
                                   WHEN customer_site_use_id IS NOT NULL THEN
                                    l_cust_site_id
                                   ELSE
                                    NULL
                                 END) customer_site_use_id
                            FROM pn.pn_tenancies_all            d
                                ,xxcus.xxcus_employee_conv_temp c
                                ,xxcus.xxcus_employee_conv_temp u
                           WHERE d.created_by = c.user_id_r11(+)
                             AND d.last_updated_by = u.user_id_r11(+))
        LOOP
        
          UPDATE pn.pn_tenancies_all
             SET org_id               = l_org
                ,created_by           = c_details.created_user
                ,last_updated_by      = c_details.updated_user
                ,customer_id          = c_details.customer_id
                ,customer_site_use_id = c_details.customer_site_use_id
           WHERE tenancy_id = c_details.tenancy_id;
        
        END LOOP;
      END;
    
      EXECUTE IMMEDIATE 'alter sequence ' || l_sequence || ' increment by ' ||
                        l_seq_r11;
    
      EXECUTE IMMEDIATE 'select ' || l_sequence || '.nextval from dual '
        INTO l_seq_r12;
    
      EXECUTE IMMEDIATE 'alter sequence ' || l_sequence || ' increment by 1 ';
    
      EXECUTE IMMEDIATE 'SELECT COUNT(*) FROM ' || l_table
        INTO l_count_r12;
    
      INSERT INTO xxcus.xxcus_pn_conv_tbl
        (table_name
        ,r11_count
        ,r12_count
        ,sequence
        ,seq_r11_num
        ,seq_r12_num
        ,creation_date)
      VALUES
        (l_table
        ,l_count_r11
        ,l_count_r12
        ,l_sequence
        ,l_seq_r11
        ,l_seq_r12
        ,SYSDATE);
    
      COMMIT;
    
    END;
  
    ------------------------------------------------------------------------ 
  
    BEGIN
      l_table     := 'PN.PN_TENANCIES_HISTORY';
      l_sequence  := 'PN.PN_TENANCIES_HISTORY_S';
      l_sec       := 'Loading table ' || l_table;
      l_count_r11 := 0;
      l_count_r12 := 0;
    
      EXECUTE IMMEDIATE 'SELECT COUNT(*) FROM ' || l_table ||
                        '@R12_TO_FIN.HSI.HUGHESSUPPLY.COM'
        INTO l_count_r11;
    
      EXECUTE IMMEDIATE 'SELECT MAX(tenancy_history_id) FROM ' || l_table ||
                        '@R12_TO_FIN.HSI.HUGHESSUPPLY.COM'
        INTO l_seq_r11;
    
      INSERT /*+ APPEND */
      INTO pn.pn_tenancies_history
        SELECT * FROM pn.pn_tenancies_history@r12_to_fin.hsi.hughessupply.com;
      COMMIT;
    
      BEGIN
        FOR c_details IN (SELECT tenancy_history_id
                                ,nvl(c.user_id, d.created_by) created_user
                                ,nvl(u.user_id, d.last_updated_by) updated_user
                                ,(CASE
                                   WHEN customer_id IS NOT NULL THEN
                                    l_cust_id
                                   ELSE
                                    NULL
                                 END) customer_id
                                ,(CASE
                                   WHEN customer_site_use_id IS NOT NULL THEN
                                    l_cust_site_id
                                   ELSE
                                    NULL
                                 END) customer_site_use_id
                            FROM pn.pn_tenancies_history        d
                                ,xxcus.xxcus_employee_conv_temp c
                                ,xxcus.xxcus_employee_conv_temp u
                           WHERE d.created_by = c.user_id_r11(+)
                             AND d.last_updated_by = u.user_id_r11(+))
        LOOP
        
          UPDATE pn.pn_tenancies_history
             SET org_id               = l_org
                ,created_by           = c_details.created_user
                ,last_updated_by      = c_details.updated_user
                ,customer_id          = c_details.customer_id
                ,customer_site_use_id = c_details.customer_site_use_id
           WHERE tenancy_history_id = c_details.tenancy_history_id;
        
        END LOOP;
      END;
    
      EXECUTE IMMEDIATE 'alter sequence ' || l_sequence || ' increment by ' ||
                        l_seq_r11;
    
      EXECUTE IMMEDIATE 'select ' || l_sequence || '.nextval from dual '
        INTO l_seq_r12;
    
      EXECUTE IMMEDIATE 'alter sequence ' || l_sequence || ' increment by 1 ';
    
      EXECUTE IMMEDIATE 'SELECT COUNT(*) FROM ' || l_table
        INTO l_count_r12;
    
      INSERT INTO xxcus.xxcus_pn_conv_tbl
        (table_name
        ,r11_count
        ,r12_count
        ,sequence
        ,seq_r11_num
        ,seq_r12_num
        ,creation_date)
      VALUES
        (l_table
        ,l_count_r11
        ,l_count_r12
        ,l_sequence
        ,l_seq_r11
        ,l_seq_r12
        ,SYSDATE);
    
      COMMIT;
    
    END;
  
    -- EXCEPTIONS
  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;
      l_err_code := 2;
      l_sec      := 'Error in ' || l_err_callpoint;
      l_err_msg  := l_sec;
      l_err_msg  := l_err_msg || ' WITH  ' || substr(SQLERRM, 1, 1000);
      dbms_output.put_line(l_err_msg);
      retcode := l_err_code;
      errbuf  := l_err_msg;
    
    WHEN OTHERS THEN
      ROLLBACK;
      l_err_code := 2;
      l_sec      := 'Error in ' || l_err_callpoint;
      l_err_msg  := l_sec;
      l_err_msg  := l_err_msg || ' WITH  ' || substr(SQLERRM, 1, 1000);
      dbms_output.put_line(l_err_msg);
      retcode := l_err_code;
      errbuf  := l_err_msg;
    
  END pn_data;
  /*******************************************************************************
  * Procedure:   
  * Description: Will be used to load AP Supplier Data into R12 from R11
  *              Supplier, Supplier Sites, Supplier Contacts and Supplier 
  *              Bank Account.
  *              Active Supplier and Supplier sites will be converted.
  *              Inactive Suppliers and Supplier sites that had paid historical 
  *              invoices associated to them that were converted over for duplicate 
  *              invoice processing validations.
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     03/15/2011    Kathy Poling    Initial creation  
  1.1     08/18/2011    Kathy Poling    Changed to pull all active vendors
  1.2     10/05/2011    Kathy Poling    Added bank mapping table
  ********************************************************************************/

  PROCEDURE xxcus_ap_vendor(errbuf OUT VARCHAR2, retcode OUT NUMBER) IS
  
    l_user_name           VARCHAR2(100) := 'KP027916';
    l_user                NUMBER;
    l_can_submit_request  BOOLEAN := TRUE;
    l_globalset           VARCHAR2(100);
    l_statement           VARCHAR2(100);
    l_err_msg             VARCHAR2(2000);
    l_sec                 VARCHAR2(2000);
    nl                    VARCHAR2(20) := fnd_global.newline;
    v_supplier_id         NUMBER;
    v_supplier_site_id    NUMBER;
    v_supplier_contact_id NUMBER;
    v_site                NUMBER;
    v_rec_cnt             NUMBER := 0;
    l_org                 NUMBER;
    l_date                DATE := '01-Aug-2010'; --cut over date for history moving to R12
    l_org_gsc CONSTANT hr_all_organization_units.organization_id%TYPE := 163; --HD Supply US GSC
    l_org_pr  CONSTANT hr_all_organization_units.organization_id%TYPE := 166; --HD Supply US Garnishment
  
    --   v_pay_group VARCHAR2(30) := 'REAL ESTATE';
    --  v_pay_term CONSTANT ap_terms_tl.term_id%TYPE := 10015; --AP NET 30
    v_ledger CONSTANT gl_ledgers.ledger_id%TYPE := 2061; --HD Supply Corp
  
    v_req_id            NUMBER;
    v_call_status       BOOLEAN;
    v_set_req_status    BOOLEAN;
    v_req_phase         VARCHAR2(80);
    v_req_status        VARCHAR2(80);
    v_dev_phase         VARCHAR2(30);
    v_dev_status        VARCHAR2(30);
    v_req_message       VARCHAR2(255);
    v_interval          NUMBER := 10; -- In seconds
    v_max_time          NUMBER := 15000; -- In seconds
    v_completion_status VARCHAR2(30) := 'NORMAL';
    v_error_msg         VARCHAR2(150);
  
    TYPE t_rec IS RECORD(
      req_id      NUMBER(15),
      req_phase   VARCHAR2(80),
      req_status  VARCHAR2(80),
      dev_phase   VARCHAR2(30),
      dev_status  VARCHAR2(30),
      req_message VARCHAR2(240));
    TYPE t_req_tab_type IS TABLE OF t_rec INDEX BY BINARY_INTEGER;
  
    req_table t_req_tab_type;
    i         NUMBER := 0;
  
    CURSOR c1 IS
      SELECT DISTINCT v.vendor_id --use attribute field for mapping invoices
                     ,v.vendor_name
                     ,v.vendor_name_alt
                     ,v.segment1
                     ,v.summary_flag
                     ,v.enabled_flag
                     ,e.person_id12 employee_id
                     ,(CASE
                        WHEN v.vendor_type_lookup_code IN
                             ('EXPENSE'
                             ,'FREIGHT'
                             ,'IWO'
                             ,'IWO EXP'
                             ,'REAL ESTATE'
                             ,'EMPLOYEE') THEN
                         v.vendor_type_lookup_code
                        WHEN v.vendor_type_lookup_code IS NULL THEN
                         v.vendor_type_lookup_code
                        ELSE
                         'VENDOR'
                      END) vendor_type_lookup_code
                     ,v.customer_num --What are they using customer number for???
                     ,v.one_time_flag
                     ,v.parent_vendor_id --use attribute field to update after import
                     ,pt.term_id --payterm in R12
                     ,v.set_of_books_id
                     ,v.always_take_disc_flag
                     ,v.pay_date_basis_lookup_code
                     ,(CASE
                        WHEN v.pay_group_lookup_code IN
                             ('AP WIRE TRANSFER'
                             ,'EMPLOYEE'
                             ,'EXPENSE'
                             ,'FREIGHT'
                             ,'IWO'
                             ,'LOCAL CHECKS'
                             ,'REAL ESTATE'
                             ,'RENT'
                             ,'TAX'
                             ,'UTILITIES'
                             ,'VENDOR'
                             ,'ZERO CHECK') THEN
                         v.pay_group_lookup_code
                        ELSE
                         'VENDOR'
                      END) pay_group_lookup_code
                     ,v.payment_priority
                     ,v.invoice_currency_code
                     ,v.payment_currency_code
                     ,v.hold_all_payments_flag
                     ,v.hold_future_payments_flag
                     ,v.hold_reason
                     ,v.num_1099
                     ,v.type_1099
                     ,v.organization_type_lookup_code
                     ,v.start_date_active
                     ,v.end_date_active
                      --,v.minority_group_lookup_code
                     ,(CASE
                        WHEN v.payment_method_lookup_code = 'ACH' THEN
                         'EFT'
                        ELSE
                         v.payment_method_lookup_code
                      END) payment_method_lookup_code
                     ,v.women_owned_flag
                     ,v.small_business_flag
                     ,v.standard_industry_class
                     ,v.terms_date_basis
                     ,v.exclusive_payment_flag
                     ,v.tax_verification_date
                     ,v.name_control
                     ,v.state_reportable_flag
                     ,v.federal_reportable_flag
                      --,attribute15 --What is this and do we need?????
                     ,v.vat_registration_num --What is this being used for and do we need????
                     ,v.auto_calculate_interest_flag
                     ,v.exclude_freight_from_discount
                     ,tax_reporting_name
        FROM po.po_vendors@r12_to_fin.hsi.hughessupply.com          v
            ,po.po_vendor_sites_all@r12_to_fin.hsi.hughessupply.com s
            ,xxcus.xxcus_employee_conv_temp                         e
            ,ap.ap_terms_tl@r12_to_fin.hsi.hughessupply.com         t
            ,ap.ap_terms_tl                                         pt
       WHERE v.employee_id = e.person_id11(+)
         AND v.vendor_id = s.vendor_id
         AND s.pay_site_flag = 'Y'
         AND v.terms_id = t.term_id
         AND t.name = pt.name(+)
            --AND v.vendor_id IN (869617, 817464, 328995, 32267, 32267, 817769, 279913, 568036)  --used for testing
         AND (s.inactive_date IS NULL AND v.end_date_active IS NULL --all version 1.1
             AND v.enabled_flag = 'Y' AND v.employee_id IS NULL OR
             v.vendor_id IN (SELECT vendor_id
                                FROM xxcus.xxcus_ap_invoices_conv i
                               WHERE i.vendor_id = v.vendor_id) OR
             --vendor_type_lookup_code = 'REAL ESTATE'
             vendor_name IN
             (SELECT description
                 FROM apps.fnd_lookup_values@r12_to_fin.hsi.hughessupply.com f
                WHERE lookup_type = 'HDS_OPN_VENDORS'
                  AND description = v.vendor_name));
    /*          AND i.vendor_id = v.vendor_id
                    AND invoice_amount <> 0
                    AND (invoice_date BETWEEN l_date AND SYSDATE OR
                        payment_status_flag IN ('N', 'P')
                        --AND cancelled_date IS NULL 
                        OR i.invoice_id IN
                        (SELECT invoice_id
                               FROM ap.ap_invoice_payments_all@R12_TO_FIN.HSI.HUGHESSUPPLY.COM ip
                                   ,ap.ap_checks_all@R12_TO_FIN.HSI.HUGHESSUPPLY.COM           c
                              WHERE ip.invoice_id = i.invoice_id
                                AND ip.check_id = c.check_id
                                AND c.status_lookup_code <> 'VOIDED'
                                AND cleared_amount IS NULL
                                AND cleared_date IS NULL
                                AND c.payment_method_lookup_code <> 'ACH'
                                AND c.bank_account_name NOT IN
                                    ('WT Clearing Document'
                                    ,'IEXPENSE PAYMENT CLEARING'
                                    ,'CONVERSION BANK ACCOUNT')
                                AND c.amount <> 0))) 
    */
  BEGIN
  
    --SELECT user_id INTO l_user FROM fnd_user WHERE user_name = l_user_name;
  
    --  Setup parameters for running FND JOBS!
    l_can_submit_request := xxcus_misc_pkg.set_responsibility(l_user_name
                                                             ,'XXCUS_CON');
    IF l_can_submit_request
    THEN
      l_globalset := 'Global Variables are set.';
    
    ELSE
    
      l_globalset := 'Global Variables are not set.';
      l_sec       := 'Global Variables are not set for the Responsibility of XXCUS_CON and the User.';
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      RAISE program_error;
    END IF;
  
    BEGIN
    
      l_sec := 'Trucate data from xxcus_ap_invoices_conv.';
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      EXECUTE IMMEDIATE 'truncate table xxcus.xxcus_ap_invoices_conv';
    
      COMMIT;
      --Load invoices from R11 will be used for vendors and loading invoices 
      INSERT /*+ APPEND */
      INTO xxcus.xxcus_ap_invoices_conv
        (SELECT *
           FROM ap.ap_invoices_all@r12_to_fin.hsi.hughessupply.com i
          WHERE i.invoice_id NOT IN (18889
                                    ,67777
                                    ,67789
                                    ,67790
                                    ,67791
                                    ,67904
                                    ,633589
                                    ,643768
                                    ,796165
                                    ,802725
                                    ,1033634
                                    ,1060408
                                    ,1237864
                                    ,1354081
                                    ,1371148
                                    ,1370500
                                    ,1380679
                                    ,1380889
                                    ,1410633
                                    ,1432594
                                    ,1417721
                                    ,1417940
                                    ,1430541
                                    ,1438024
                                    ,1429115
                                    ,1475636
                                    ,1550366
                                    ,1571291
                                    ,1613970
                                    ,1617725
                                    ,1625453
                                    ,1684598
                                    ,1752369
                                    ,1819259
                                    ,1826963
                                    ,1841039
                                    ,1894282
                                    ,1892969
                                    ,1893256
                                    ,1983219
                                    ,1982866
                                    ,2218903
                                    ,2237361
                                    ,2239091
                                    ,2265678
                                    ,2361966
                                    ,2507351
                                    ,2495923
                                    ,2567228
                                    ,2723304
                                    ,2820446
                                    ,2864621
                                    ,2938310
                                    ,3103418
                                    ,3229471
                                    ,3223379
                                    ,3398080
                                    ,3389002
                                    ,3466133
                                    ,3549183
                                    ,3613054
                                    ,4963135
                                    ,590668
                                    ,3813028
                                    ,3813000
                                    ,3813112
                                    ,3813031)
               --AND i.vendor_id = v.vendor_id
            AND invoice_type_lookup_code <> 'EXPENSE_REPORT' --added with version 1.1
            AND invoice_amount <> 0
            AND invoice_date BETWEEN '31-Jul-2010' AND SYSDATE + 1
         UNION
         SELECT *
           FROM ap.ap_invoices_all@r12_to_fin.hsi.hughessupply.com i
          WHERE i.invoice_id NOT IN (18889
                                    ,67777
                                    ,67789
                                    ,67790
                                    ,67791
                                    ,67904
                                    ,633589
                                    ,643768
                                    ,796165
                                    ,802725
                                    ,1033634
                                    ,1060408
                                    ,1237864
                                    ,1354081
                                    ,1371148
                                    ,1370500
                                    ,1380679
                                    ,1380889
                                    ,1410633
                                    ,1432594
                                    ,1417721
                                    ,1417940
                                    ,1430541
                                    ,1438024
                                    ,1429115
                                    ,1475636
                                    ,1550366
                                    ,1571291
                                    ,1613970
                                    ,1617725
                                    ,1625453
                                    ,1684598
                                    ,1752369
                                    ,1819259
                                    ,1826963
                                    ,1841039
                                    ,1894282
                                    ,1892969
                                    ,1893256
                                    ,1983219
                                    ,1982866
                                    ,2218903
                                    ,2237361
                                    ,2239091
                                    ,2265678
                                    ,2361966
                                    ,2507351
                                    ,2495923
                                    ,2567228
                                    ,2723304
                                    ,2820446
                                    ,2864621
                                    ,2938310
                                    ,3103418
                                    ,3229471
                                    ,3223379
                                    ,3398080
                                    ,3389002
                                    ,3466133
                                    ,3549183
                                    ,3613054
                                    ,4963135
                                    ,590668
                                    ,3813028
                                    ,3813000
                                    ,3813112
                                    ,3813031)
               --AND i.vendor_id = v.vendor_id
            AND invoice_amount <> 0
               --AND invoice_date BETWEEN '01-Aug-2010' AND SYSDATE 
            AND invoice_amount <> amount_paid
            AND payment_status_flag IN ('N', 'P')
               --AND cancelled_date IS NULL 
             OR i.invoice_id IN (SELECT invoice_id
                                   FROM ap.ap_invoice_payments_all@r12_to_fin.hsi.hughessupply.com ip
                                       ,ap.ap_checks_all@r12_to_fin.hsi.hughessupply.com           c
                                  WHERE ip.invoice_id = i.invoice_id
                                    AND ip.check_id = c.check_id
                                    AND c.status_lookup_code <> 'VOIDED'
                                    AND cleared_amount IS NULL
                                    AND cleared_date IS NULL
                                    AND c.payment_method_lookup_code <> 'ACH'
                                    AND c.bank_account_name NOT IN
                                        ('WT Clearing Document'
                                        ,'IEXPENSE PAYMENT CLEARING'
                                        ,'CONVERSION BANK ACCOUNT')
                                    AND c.amount <> 0));
    
      COMMIT;
    
      l_sec := 'Loaded the invoices from R11.';
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
    
    END;
  
    BEGIN
    
      FOR cc_supplier IN c1
      LOOP
      
        --SELECT apps.ap_suppliers_int_s.nextval INTO v_supplier_id FROM dual;
      
        v_rec_cnt := v_rec_cnt + 1;
      
        -- Supplier header Open Interface Import
        INSERT INTO ap.ap_suppliers_int
          (vendor_interface_id
          ,last_update_date
          ,last_updated_by
          ,vendor_name
          ,vendor_name_alt
          ,creation_date
          ,created_by
          ,segment1
          ,summary_flag
          ,enabled_flag
          ,employee_id
          ,vendor_type_lookup_code
          ,customer_num --What is this being used for and do we need????
          ,one_time_flag
           --,parent_vendor_id
          ,terms_id
          ,set_of_books_id
          ,always_take_disc_flag
          ,pay_date_basis_lookup_code
          ,pay_group_lookup_code
          ,payment_priority
          ,invoice_currency_code
          ,payment_currency_code
          ,hold_all_payments_flag
          ,hold_future_payments_flag
          ,hold_reason
          ,num_1099
          ,type_1099
          ,organization_type_lookup_code
          ,start_date_active
          ,end_date_active
           --,minority_group_lookup_code
          ,payment_method_lookup_code
          ,women_owned_flag
          ,small_business_flag
          ,standard_industry_class
          ,terms_date_basis
          ,exclusive_payment_flag
          ,tax_verification_date
          ,name_control
          ,state_reportable_flag
          ,federal_reportable_flag
          ,attribute15 --R11 Vendor id to be used for mapping invoices
          ,vat_registration_num --What is this being used for and do we need????
          ,auto_calculate_interest_flag
          ,exclude_freight_from_discount
          ,tax_reporting_name
          ,status)
        VALUES
          (ap_suppliers_int_s.nextval
          ,SYSDATE
          ,0
          ,cc_supplier.vendor_name
          ,cc_supplier.vendor_name_alt
          ,SYSDATE
          ,0
          ,cc_supplier.segment1
          ,cc_supplier.summary_flag
          ,cc_supplier.enabled_flag
          ,cc_supplier.employee_id
          ,cc_supplier.vendor_type_lookup_code
          ,cc_supplier.customer_num
          ,cc_supplier.one_time_flag
           --,cc_supplier.parent_vendor_id
          ,cc_supplier.term_id
          ,v_ledger
          ,cc_supplier.always_take_disc_flag
          ,cc_supplier.pay_date_basis_lookup_code
          ,cc_supplier.pay_group_lookup_code
          ,cc_supplier.payment_priority
          ,cc_supplier.invoice_currency_code
          ,cc_supplier.payment_currency_code
          ,cc_supplier.hold_all_payments_flag
          ,cc_supplier.hold_future_payments_flag
          ,cc_supplier.hold_reason
          ,cc_supplier.num_1099
          ,cc_supplier.type_1099
          ,cc_supplier.organization_type_lookup_code
          ,cc_supplier.start_date_active
          ,cc_supplier.end_date_active
           --,cc_supplier.minority_group_lookup_code
          ,cc_supplier.payment_method_lookup_code
          ,cc_supplier.women_owned_flag
          ,cc_supplier.small_business_flag
          ,cc_supplier.standard_industry_class
          ,cc_supplier.terms_date_basis
          ,cc_supplier.exclusive_payment_flag
          ,cc_supplier.tax_verification_date
          ,cc_supplier.name_control
          ,cc_supplier.state_reportable_flag
          ,cc_supplier.federal_reportable_flag
          ,cc_supplier.vendor_id --R11 Vendor id to be used for mapping invoices
          ,cc_supplier.vat_registration_num --What is this being used for and do we need????
          ,cc_supplier.auto_calculate_interest_flag
          ,cc_supplier.exclude_freight_from_discount
          ,cc_supplier.tax_reporting_name
          ,'NEW');
      
        --Write detail Line Log rows
        l_sec := 'Inserting supplier:  ' || cc_supplier.vendor_name ||
                 ' Supplier ID:  ' || ap_suppliers_int_s.currval;
        fnd_file.put_line(fnd_file.log, l_sec);
        fnd_file.put_line(fnd_file.output, l_sec);
      
        --dbms_output.put_line(l_sec);
      
        -- Bank at Vendor
        /*     FOR cc_vndr_bnk IN (SELECT DISTINCT a.r12_bank_name     bank_name
                                           ,b.bank_party_id
                                           ,a.r12_branch_name   bank_branch_name
                                           ,b.branch_party_id
                                           ,ba.account_type
                                           ,ba.bank_account_num
                                           ,ba.currency_code
                                           ,ba.description
                                           ,br.institution_type
                                           ,bau.primary_flag
                                           ,bau.vendor_id
                                           ,v.vendor_name
                                           ,bau.org_id
                                           ,b.bank_home_country
                              FROM ap.ap_bank_accounts_all@R12_TO_FIN.HSI.HUGHESSUPPLY.COM     ba
                                  ,ap.ap_bank_account_uses_all@R12_TO_FIN.HSI.HUGHESSUPPLY.COM bau
                                  ,ap.ap_bank_branches@R12_TO_FIN.HSI.HUGHESSUPPLY.COM         br
                                  ,po.po_vendors@R12_TO_FIN.HSI.HUGHESSUPPLY.COM               v
                                  ,apps.ce_bank_branches_v                b
                                  ,xxcus.xxcusap_bank_map_tbl             a
                             WHERE bau.external_bank_account_id =
                                   ba.bank_account_id
                               AND ba.bank_branch_id = br.bank_branch_id
                               AND upper(br.bank_name) = upper(r11_bank_name)
                               AND br.country = a.country
                               AND ba.account_type = 'SUPPLIER'
                               AND ba.inactive_date IS NULL
                               AND bau.end_date IS NULL
                               AND ba.attribute2 IS NULL
                               AND bau.vendor_id = v.vendor_id
                               AND bau.vendor_site_id IS NULL
                               AND v.enabled_flag = 'Y'
                               AND nvl(v.end_date_active, SYSDATE) >= SYSDATE
                               AND upper(r12_bank_name) = b.bank_name
                               AND a.r12_branch_name = b.bank_branch_name
                               AND a.country = b.bank_home_country
                               AND bau.vendor_id = cc_supplier.vendor_id)
        LOOP
        
          INSERT INTO iby_temp_ext_bank_accts
            (bank_account_num
            ,country_code
            ,temp_ext_bank_acct_id
            ,last_update_date
            ,last_updated_by
            ,creation_date
            ,created_by
            ,object_version_number
            ,calling_app_unique_ref1 --vendor_interface_id
            ,foreign_payment_use_flag
            ,bank_id
            ,branch_id
            ,status)
          VALUES
            (cc_vndr_bnk.bank_account_num
            ,cc_vndr_bnk.bank_home_country
            ,iby_temp_ext_bank_accts_s.nextval
            ,SYSDATE
            ,0
            ,SYSDATE
            ,0
            ,1
            ,ap_suppliers_int_s.currval
            ,'N'
            ,cc_vndr_bnk.bank_party_id
            ,cc_vndr_bnk.branch_party_id
            ,'NEW');
        
          --Write detail Line Log rows
          l_sec := 'Inserting supplier bank:  ' || cc_supplier.vendor_name ||
                   ' Bank Acct ID:  ' || iby_temp_ext_bank_accts_s.currval;
          fnd_file.put_line(fnd_file.log, l_sec);
          fnd_file.put_line(fnd_file.output, l_sec);
        
        --dbms_output.put_line(l_sec);
        END LOOP;
        */
        v_site := 1;
        -- Supplier Sites Open Interface Import
        FOR cc_site IN (SELECT vendor_site_id
                              ,vendor_id
                              ,vendor_site_code
                              ,vendor_site_code_alt
                              ,purchasing_site_flag
                              ,rfq_only_site_flag
                              ,pay_site_flag
                              ,attention_ar_flag
                              ,nvl(address_line1, 'X') address_line1
                              ,address_lines_alt
                              ,address_line2
                              ,address_line3
                              ,city
                              ,state
                              ,zip
                              ,province
                              ,country
                              ,area_code
                              ,phone
                              ,customer_num
                              ,inactive_date
                              ,fax
                              ,fax_area_code
                              ,telex
                              ,(CASE
                                 WHEN payment_method_lookup_code = 'ACH' THEN
                                  'EFT'
                                 ELSE
                                  payment_method_lookup_code
                               END) payment_method_lookup_code
                              ,terms_date_basis
                              ,(CASE
                                 WHEN pay_group_lookup_code IN
                                      ('AP WIRE TRANSFER'
                                      ,'EMPLOYEE'
                                      ,'EXPENSE'
                                      ,'FREIGHT'
                                      ,'IWO'
                                      ,'LOCAL CHECKS'
                                      ,'REAL ESTATE'
                                      ,'RENT'
                                      ,'TAX'
                                      ,'UTILITIES'
                                      ,'VENDOR'
                                      ,'ZERO CHECK') THEN
                                  pay_group_lookup_code
                                 ELSE
                                  'VENDOR'
                               END) pay_group_lookup_code
                              ,payment_priority
                              ,pt.term_id --payterm in R12
                              ,pay_date_basis_lookup_code
                              ,always_take_disc_flag
                              ,invoice_currency_code
                              ,payment_currency_code
                              ,hold_all_payments_flag
                              ,hold_future_payments_flag
                              ,hold_reason
                              ,hold_unmatched_invoices_flag
                              ,exclusive_payment_flag
                              ,tax_reporting_site_flag
                              ,(CASE
                                 WHEN org_id = 262 AND s.attribute1 IS NOT NULL THEN
                                  l_org_pr
                                 ELSE
                                  NULL
                               END) attribute_category
                              ,(CASE
                                 WHEN org_id = 262 AND s.attribute1 IS NOT NULL THEN
                                  s.attribute1
                                 ELSE
                                  NULL
                               END) attribute1
                               --,attribute2
                               --,ATTRIBUTE3
                               --,attribute4
                               --,ATTRIBUTE5
                               --,ATTRIBUTE6
                               --,attribute7
                               --,ATTRIBUTE8
                               --,ATTRIBUTE9
                               --,ATTRIBUTE10
                               --,ATTRIBUTE11
                               --,ATTRIBUTE12
                               --,ATTRIBUTE13
                              ,s.attribute14
                              ,s.attribute15
                              ,validation_number
                              ,exclude_freight_from_discount
                              ,vat_registration_num
                              ,org_id
                              ,bank_number
                              ,address_line4
                              ,nvl(county, 'X') county
                              ,address_style
                              ,s.language
                              ,allow_awt_flag
                              ,edi_transaction_handling
                              ,pcard_site_flag
                              ,create_debit_memo_flag
                              ,offset_tax_flag
                              ,supplier_notif_method
                              ,email_address
                              ,remittance_email
                              ,primary_pay_site_flag
                        --,TOLERANCE_ID  --what is this???
                          FROM po.po_vendor_sites_all@r12_to_fin.hsi.hughessupply.com s
                              ,ap.ap_terms_tl@r12_to_fin.hsi.hughessupply.com         t
                              ,ap.ap_terms_tl                                         pt
                         WHERE s.vendor_id = cc_supplier.vendor_id
                           AND s.terms_id = t.term_id
                           AND s.pay_site_flag = 'Y'
                           AND t.name = pt.name(+)
                           AND (inactive_date IS NULL OR
                               s.vendor_site_id IN
                               (SELECT vendor_site_id
                                   FROM xxcus.xxcus_ap_invoices_conv i
                                  WHERE i.vendor_site_id = s.vendor_site_id) OR
                               s.vendor_site_id IN
                               (SELECT to_number(attribute1)
                                   FROM apps.fnd_lookup_values@r12_to_fin.hsi.hughessupply.com
                                  WHERE lookup_type = 'HDS_OPN_VENDORS'
                                    AND to_number(attribute1) = s.vendor_site_id)))
        LOOP
        
          IF cc_site.org_id = 81
          THEN
            l_org := l_org_gsc;
          ELSE
            l_org := l_org_pr;
          END IF;
        
          --    SELECT apps.ap_supplier_sites_int_s.nextval
          --      INTO v_supplier_site_id
          --      FROM dual;
        
          INSERT INTO ap.ap_supplier_sites_int
            (vendor_interface_id
            ,last_update_date
            ,last_updated_by
            ,vendor_site_code
            ,vendor_site_code_alt
            ,creation_date
            ,created_by
            ,purchasing_site_flag
            ,rfq_only_site_flag
            ,pay_site_flag
            ,attention_ar_flag
            ,address_line1
            ,address_lines_alt
            ,address_line2
            ,address_line3
            ,city
            ,state
            ,zip
            ,province
            ,country
            ,area_code
            ,phone
            ,customer_num
            ,inactive_date
            ,fax
            ,fax_area_code
            ,telex
            ,payment_method_lookup_code
            ,terms_date_basis
            ,pay_group_lookup_code
            ,payment_priority
            ,terms_id --need to find payterm in R12
            ,pay_date_basis_lookup_code
            ,always_take_disc_flag
            ,invoice_currency_code
            ,payment_currency_code
            ,hold_all_payments_flag
            ,hold_future_payments_flag
            ,hold_reason
            ,hold_unmatched_invoices_flag
            ,exclusive_payment_flag
            ,tax_reporting_site_flag
            ,attribute_category
            ,attribute1
             --,attribute2
             --,attribute4
             --,attribute7
            ,attribute14
            ,attribute15
            ,exclude_freight_from_discount
            ,vat_registration_num
            ,org_id
            ,address_line4
            ,county
            ,address_style
            ,LANGUAGE
            ,allow_awt_flag
            ,edi_transaction_handling
            ,pcard_site_flag
            ,create_debit_memo_flag
            ,offset_tax_flag
            ,supplier_notif_method
            ,email_address
            ,remittance_email
            ,primary_pay_site_flag
             --TOLERANCE_ID  --what is this???
            ,vendor_site_interface_id
            ,status
            ,auto_tax_calc_flag)
          VALUES
            (ap_suppliers_int_s.currval
            ,SYSDATE
            ,0
            ,cc_site.vendor_site_code
            ,cc_site.vendor_site_code_alt
            ,SYSDATE
            ,0
            ,cc_site.purchasing_site_flag
            ,cc_site.rfq_only_site_flag
            ,cc_site.pay_site_flag
            ,cc_site.attention_ar_flag
            ,cc_site.address_line1
            ,cc_site.address_lines_alt
            ,cc_site.address_line2
            ,cc_site.address_line3
            ,cc_site.city
            ,cc_site.state
            ,cc_site.zip
            ,cc_site.province
            ,cc_site.country
            ,cc_site.area_code
            ,cc_site.phone
            ,cc_site.customer_num
            ,cc_site.inactive_date
            ,cc_site.fax
            ,cc_site.fax_area_code
            ,cc_site.telex
            ,cc_site.payment_method_lookup_code
            ,cc_site.terms_date_basis
            ,cc_site.pay_group_lookup_code
            ,cc_site.payment_priority
            ,cc_site.term_id --need to find payterm in R12
            ,cc_site.pay_date_basis_lookup_code
            ,cc_site.always_take_disc_flag
            ,cc_site.invoice_currency_code
            ,cc_site.payment_currency_code
            ,cc_site.hold_all_payments_flag
            ,cc_site.hold_future_payments_flag
            ,cc_site.hold_reason
            ,cc_site.hold_unmatched_invoices_flag
            ,cc_site.exclusive_payment_flag
            ,cc_site.tax_reporting_site_flag
            ,cc_site.attribute_category
            ,cc_site.attribute1
             --,cc_site.attribute2
             --,cc_site.attribute4
             --,cc_site.attribute7
            ,cc_site.vendor_site_id
            ,cc_site.vendor_id
            ,cc_site.exclude_freight_from_discount
            ,cc_site.vat_registration_num
            ,l_org
            ,cc_site.address_line4
            ,cc_site.county
            ,cc_site.address_style
            ,cc_site.language
            ,cc_site.allow_awt_flag
            ,cc_site.edi_transaction_handling
            ,cc_site.pcard_site_flag
            ,cc_site.create_debit_memo_flag
            ,cc_site.offset_tax_flag
            ,cc_site.supplier_notif_method
            ,cc_site.email_address
            ,cc_site.remittance_email
            ,cc_site.primary_pay_site_flag
             --,cc_site.tolerance_id --what is this???
            ,ap_supplier_sites_int_s.nextval --v_supplier_site_id
            ,'NEW'
            ,'N');
        
          v_site := v_site + 1;
        
          --Write detail Line Log rows
          l_sec := 'Inserting supplier site:  ' || cc_supplier.vendor_name ||
                   ' Site Code:  ' || cc_site.vendor_site_code ||
                   'Record Count: ' || v_site;
          fnd_file.put_line(fnd_file.log, l_sec);
          fnd_file.put_line(fnd_file.output, l_sec);
        
          --dbms_output.put_line(l_sec);
        
          --Supplier Site Contacts Open Interface Import
        
          FOR cc_contact IN (SELECT vendor_contact_id
                                   ,vendor_site_id
                                   ,inactive_date
                                   ,first_name
                                   ,middle_name
                                   ,last_name
                                   ,prefix
                                   ,title
                                   ,mail_stop
                                   ,area_code
                                   ,phone
                                   ,contact_name_alt
                                   ,department
                                   ,email_address
                                   ,url
                                   ,alt_area_code
                                   ,alt_phone
                                   ,fax_area_code
                                   ,fax
                               FROM po.po_vendor_contacts@r12_to_fin.hsi.hughessupply.com
                              WHERE vendor_site_id = cc_site.vendor_site_id
                                AND nvl(inactive_date, SYSDATE + 1) > SYSDATE)
          LOOP
          
            --   SELECT apps.ap_sup_site_contact_int_s.nextval
            --     INTO v_supplier_contact_id
            --     FROM dual;
          
            INSERT INTO ap.ap_sup_site_contact_int
              (last_update_date
              ,last_updated_by
              ,vendor_site_code
              ,org_id
              ,creation_date
              ,created_by
              ,first_name
              ,middle_name
              ,last_name
              ,prefix
              ,title
              ,mail_stop
              ,area_code
              ,phone
              ,contact_name_alt
              ,department
              ,status
              ,email_address
              ,url
              ,alt_area_code
              ,alt_phone
              ,fax_area_code
              ,fax
              ,vendor_interface_id
              ,vendor_contact_interface_id
              ,org_contact_id)
            VALUES
              (SYSDATE
              ,0
              ,cc_site.vendor_site_code
              ,l_org
              ,SYSDATE
              ,0
              ,cc_contact.first_name
              ,cc_contact.middle_name
              ,cc_contact.last_name
              ,cc_contact.prefix
              ,cc_contact.title
              ,cc_contact.mail_stop
              ,cc_contact.area_code
              ,cc_contact.phone
              ,cc_contact.contact_name_alt
              ,cc_contact.department
              ,'NEW'
              ,cc_contact.email_address
              ,cc_contact.url
              ,cc_contact.alt_area_code
              ,cc_contact.alt_phone
              ,cc_contact.fax_area_code
              ,cc_contact.fax
              ,ap_suppliers_int_s.currval
              ,ap_sup_site_contact_int_s.nextval --v_supplier_contact_id
              ,l_org);
          
            --Write detail Line Log rows
            l_sec := 'Inserting supplier contact:  ' || cc_supplier.vendor_name ||
                     ' Site Code:  ' || cc_site.vendor_site_code;
            fnd_file.put_line(fnd_file.log, l_sec);
            fnd_file.put_line(fnd_file.output, l_sec);
          
          --dbms_output.put_line(l_sec);
          END LOOP;
          --supplier site bank interfaced
        /*
                                  FOR cc_site_bank IN (SELECT DISTINCT a.r12_bank_name     bank_name
                                                                      ,b.bank_party_id
                                                                      ,a.r12_branch_name   bank_branch_name
                                                                      ,b.branch_party_id
                                                                      ,ba.account_type
                                                                      ,ba.bank_account_num
                                                                      ,ba.currency_code
                                                                      ,ba.description
                                                                      ,br.institution_type
                                                                      ,bau.primary_flag
                                                                      ,bau.vendor_id
                                                                      ,v.vendor_name
                                                                      ,bau.org_id
                                                                      ,bau.vendor_site_id
                                                                      ,vs.vendor_site_code
                                                                      ,b.bank_home_country
                                                         FROM ap.ap_bank_accounts_all@R12_TO_FIN.HSI.HUGHESSUPPLY.COM     ba
                                                             ,ap.ap_bank_account_uses_all@R12_TO_FIN.HSI.HUGHESSUPPLY.COM bau
                                                             ,ap.ap_bank_branches@R12_TO_FIN.HSI.HUGHESSUPPLY.COM         br
                                                             ,po.po_vendors@R12_TO_FIN.HSI.HUGHESSUPPLY.COM               v
                                                             ,po.po_vendor_sites_all@R12_TO_FIN.HSI.HUGHESSUPPLY.COM      vs
                                                             ,apps.ce_bank_branches_v                b
                                                             ,xxcus.xxcusap_bank_map_tbl             a
                                                        WHERE bau.external_bank_account_id =
                                                              ba.bank_account_id
                                                          AND ba.bank_branch_id = br.bank_branch_id
                                                             --AND ba.account_type NOT IN ('CUSTOMER', 'INTERNAL')
                                                          AND ba.account_type = 'SUPPLIER'
                                                          AND ba.inactive_date IS NULL
                                                          AND bau.end_date IS NULL
                                                          AND ba.attribute2 IS NULL
                                                          AND bau.vendor_id = v.vendor_id
                                                          AND v.enabled_flag = 'Y'
                                                          AND nvl(v.end_date_active, SYSDATE) >= SYSDATE
                                                          AND bau.vendor_site_id = vs.vendor_site_id
                                                          AND nvl(vs.inactive_date, SYSDATE) >= SYSDATE
                                                          AND upper(br.bank_name) = upper(r11_bank_name)
                                                          AND upper(br.bank_branch_name) =
                                                              upper(r11_branch_name)
                                                          AND a.country = br.country
                                                          AND upper(r12_bank_name) = upper(b.bank_name)
                                                          AND upper(r12_branch_name) =
                                                              b.bank_branch_name
                                                          AND a.country = b.bank_home_country
                                                          AND bau.vendor_site_id =
                                                              cc_site.vendor_site_id)
                                  LOOP
                                  
                                    INSERT INTO iby_temp_ext_bank_accts
                                      (bank_account_num
                                      ,country_code
                                      ,temp_ext_bank_acct_id
                                      ,last_update_date
                                      ,last_updated_by
                                      ,creation_date
                                      ,created_by
                                      ,object_version_number
                                      ,calling_app_unique_ref2 --vendor_site_interface_id
                                      ,foreign_payment_use_flag
                                      ,bank_id
                                       --, bank_name
                                       --,BANK_NUMBER
                                      ,branch_id
                                       --, branch_name 
                                      ,status)
                                    VALUES
                                      (cc_site_bank.bank_account_num
                                      ,cc_site_bank.bank_home_country
                                      ,iby_temp_ext_bank_accts_s.nextval
                                      ,SYSDATE
                                      ,0
                                      ,SYSDATE
                                      ,0
                                      ,1
                                      ,ap_supplier_sites_int_s.currval
                                      ,'N'
                                      ,cc_site_bank.bank_party_id
                                      ,cc_site_bank.branch_party_id
                                      ,'NEW');
                                  
                                    --Write detail Line Log rows
                                    l_sec := 'Inserting supplier site bank:  ' ||
                                             cc_supplier.vendor_name || ' Supplier site ID:  ' ||
                                             ap_supplier_sites_int_s.currval;
                                    fnd_file.put_line(fnd_file.log, l_sec);
                                    fnd_file.put_line(fnd_file.output, l_sec);
                                  
                                  --dbms_output.put_line(l_sec);
                                  
                                  END LOOP;
                                  */
        END LOOP;
      END LOOP;
    END;
    fnd_file.put_line(fnd_file.log, 'Vendors created...= ' || v_rec_cnt);
    fnd_file.put_line(fnd_file.log, 'Supplier Open Interface Import....:');
  
    -- Submit Supplier Open Interface Import to vendors from the interface table into Oracle
    v_req_id := fnd_request.submit_request('SQLAP'
                                          ,'APXSUIMP'
                                          ,NULL
                                          ,''
                                          ,FALSE
                                          ,'NEW'
                                          ,1000
                                          ,'N'
                                          ,'N'
                                          ,'N');
    COMMIT; -- The Commit is Important here. Only after the Commit, FND_CONCURRENT.WAIT_FOR_REQUEST can
    -- get the status of the child process.
  
    IF v_req_id != 0
    THEN
    
      -- Wait till the above request is completed.
      v_call_status := fnd_concurrent.wait_for_request(v_req_id
                                                      ,v_interval
                                                      ,v_max_time
                                                      ,v_req_phase
                                                      ,v_req_status
                                                      ,v_dev_phase
                                                      ,v_dev_status
                                                      ,v_req_message);
      IF v_dev_phase != 'COMPLETE' OR v_dev_status != 'NORMAL'
      THEN
        l_statement := 'An error occured in the Supplier Open Interface Import, please review the Log for concurrent request ' ||
                       v_req_id || ' - ' || v_error_msg || '.';
        fnd_file.put_line(fnd_file.log, l_statement);
        fnd_file.put_line(fnd_file.output, l_statement);
        RAISE program_error;
      ELSE
        retcode := 0;
      
      END IF;
    END IF;
    --end supplier import
  
    fnd_file.put_line(fnd_file.log
                     ,'Supplier Sites Open Interface Import....:');
    fnd_file.put_line(fnd_file.log, 'Supplier Open Interface Import....:');
  
    -- Submit Supplier Sites Open Interface Import supplier sites from the interface table into Oracle
    v_req_id := fnd_request.submit_request('SQLAP'
                                          ,'APXSSIMP'
                                          ,NULL
                                          ,''
                                          ,FALSE
                                          ,'NEW'
                                          ,1000
                                          ,'N'
                                          ,'N'
                                          ,'N');
    COMMIT; -- The Commit is Important here. Only after the Commit, FND_CONCURRENT.WAIT_FOR_REQUEST can
    -- get the status of the child process.
  
    IF v_req_id != 0
    THEN
    
      -- Wait till the above request is completed.
      v_call_status := fnd_concurrent.wait_for_request(v_req_id
                                                      ,v_interval
                                                      ,v_max_time
                                                      ,v_req_phase
                                                      ,v_req_status
                                                      ,v_dev_phase
                                                      ,v_dev_status
                                                      ,v_req_message);
      IF v_dev_phase != 'COMPLETE' OR v_dev_status != 'NORMAL'
      THEN
        l_statement := 'An error occured in the Supplier Site Open Interface Import, please review the Log for concurrent request ' ||
                       v_req_id || ' - ' || v_error_msg || '.';
        fnd_file.put_line(fnd_file.log, l_statement);
        fnd_file.put_line(fnd_file.output, l_statement);
        RAISE program_error;
      ELSE
        retcode := 0;
      
      END IF;
    END IF;
    --end supplier site import
  
    fnd_file.put_line(fnd_file.log
                     ,'Supplier Site Contacts Open Interface Import....:');
    fnd_file.put_line(fnd_file.log
                     ,'Supplier Site Contacts Open Interface Import....:');
  
    -- Submit Supplier Sites Contracts Interface Import supplier sites from the interface table into Oracle
    v_req_id := fnd_request.submit_request('SQLAP'
                                          ,'APXSCIMP'
                                          ,NULL
                                          ,''
                                          ,FALSE
                                          ,'NEW'
                                          ,1000
                                          ,'N'
                                          ,'N'
                                          ,'N');
    COMMIT;
  
    IF v_req_id != 0
    THEN
    
      -- Wait till the above request is completed.
      v_call_status := fnd_concurrent.wait_for_request(v_req_id
                                                      ,v_interval
                                                      ,v_max_time
                                                      ,v_req_phase
                                                      ,v_req_status
                                                      ,v_dev_phase
                                                      ,v_dev_status
                                                      ,v_req_message);
      IF v_dev_phase != 'COMPLETE' OR v_dev_status != 'NORMAL'
      THEN
        l_statement := 'An error occured in the Supplier Site Contact Interface Import, please review the Log for concurrent request ' ||
                       v_req_id || ' - ' || v_error_msg || '.';
        fnd_file.put_line(fnd_file.log, l_statement);
        fnd_file.put_line(fnd_file.output, l_statement);
        RAISE program_error;
      ELSE
        retcode := 0;
      
      END IF;
    END IF;
    --end supplier contact import
  
  EXCEPTION
    WHEN OTHERS THEN
      ROLLBACK;
      l_err_msg := l_err_msg || 'Unexpected Error Occured in Section :' ||
                   '::ERROR :' || SQLERRM || nl;
      fnd_file.put_line(fnd_file.log, l_err_msg);
  END;

  /*******************************************************************************
  * Procedure:   
  * Description: Will be used to load Bank and Branch related for Supplier Data 
  *              into R12 from R11 Bank and Branch information 
  *              Bank Account.
  *              Active Bank and Branch being used by active Supplier and Supplier sites 
  *              will be converted. If Branch has anything in the address line it will
  *              load the address which has to create a party site.  
  *              Bank and Branch will load after Suppliers.
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     03/31/2011    Kathy Poling    Initial creation  
  1.1     10/05/2011    Kathy Poling    Added bank mapping table
  ********************************************************************************/
  PROCEDURE xxcus_bank_load(errbuf OUT VARCHAR2, retcode OUT NUMBER) IS
  
    x_result_rec_type iby_fndcpt_common_pub.result_rec_type;
    v_error_reason    VARCHAR2(4000) := NULL;
    v_msg_data        VARCHAR2(2000) := NULL;
    v_error_code      VARCHAR2(6000) := NULL;
    v_msg             VARCHAR2(2000);
    v_msg_count       NUMBER := NULL;
    v_return_status   VARCHAR2(100) := NULL;
  
    --Bank API
    v_extbank_rec_type iby_ext_bankacct_pub.extbank_rec_type;
  
    --Branch API
    v_extbankbranch_rec_type iby_ext_bankacct_pub.extbankbranch_rec_type;
  
    --Location API
    v_location_rec hz_location_v2pub.location_rec_type;
    x_location_id  NUMBER;
  
    --Party Site API
    v_party_site_rec    hz_party_site_v2pub.party_site_rec_type;
    x_party_site_id     NUMBER;
    x_party_site_number VARCHAR2(2000);
  
    l_err_code NUMBER;
    l_err_msg  VARCHAR2(3000);
    l_sec      VARCHAR2(255);
  
    v_bank_id       NUMBER;
    x_bank_id       NUMBER;
    v_branch_id     NUMBER;
    x_branch_id     NUMBER;
    v_location_id   NUMBER;
    v_party_site_id NUMBER;
  
    CURSOR c_bank IS
    --distinct supplier bank
      SELECT DISTINCT upper(TRIM(a.r12_bank_name)) bank_name
                     ,TRIM(a.country) country
                     ,br.institution_type
        FROM ap.ap_bank_branches@r12_to_fin.hsi.hughessupply.com br
            ,xxcus.xxcusap_bank_map_tbl                          a
       WHERE nvl(bank_status, 'X') <> 'P'
         AND upper(br.bank_name) = upper(TRIM(r11_bank_name))
         AND TRIM(a.branch_num) = br.bank_num
         AND TRIM(a.country) = br.country
         AND br.bank_branch_id IN
             (SELECT ba.bank_branch_id
                FROM ap.ap_bank_accounts_all@r12_to_fin.hsi.hughessupply.com ba
               WHERE ba.bank_branch_id = br.bank_branch_id
                 AND ba.account_type = 'SUPPLIER'
                 AND ba.inactive_date IS NULL
                 AND ba.attribute2 IS NULL
                 AND ba.bank_account_id IN
                     (SELECT bank_account_id
                        FROM ap.ap_bank_account_uses_all@r12_to_fin.hsi.hughessupply.com bau
                       WHERE bau.external_bank_account_id = ba.bank_account_id
                         AND bau.end_date IS NULL
                         AND bau.vendor_id IN
                             (SELECT v.vendor_id
                                FROM po.po_vendors@r12_to_fin.hsi.hughessupply.com v
                               WHERE v.vendor_id = bau.vendor_id
                                 AND v.enabled_flag = 'Y'
                                 AND nvl(v.end_date_active, SYSDATE) >= SYSDATE)))
      --AND upper(TRIM(r12_bank_name)) NOT IN('DEUTSCHE BANK', 'INTESA SANPAOLO SPA')
      MINUS
      SELECT upper(bankorgprofile.organization_name)
            ,bankorgprofile.home_country
            ,bankca.class_code
        FROM hz_organization_profiles bankorgprofile
            ,hz_code_assignments      bankca
       WHERE bankca.class_category = 'BANK_INSTITUTION_TYPE'
         AND bankca.class_code IN ('BANK', 'CLEARINGHOUSE')
         AND bankca.owner_table_name = 'HZ_PARTIES'
         AND bankca.owner_table_id = bankorgprofile.party_id;
  
    CURSOR c_branch IS
      SELECT DISTINCT upper(TRIM(a.r12_bank_name)) bank_name
                     ,upper(TRIM(a.r12_branch_name)) bank_branch_name
                     ,br.bank_num
                     ,TRIM(a.country) country
                     ,cbb.party_id bank_party_id
                     ,nvl(br.bank_branch_type, 'ABA') bank_branch_type
                     ,TRIM(a.address_line1) address_line1
                     ,TRIM(a.address_line2) address_line2
                     ,TRIM(a.city) city
                     ,TRIM(a.state) state
                     ,TRIM(a.zip) zip
        FROM ap.ap_bank_branches@r12_to_fin.hsi.hughessupply.com br
            ,xxcus.xxcusap_bank_map_tbl                          a
            ,ar.hz_parties                                       cbb
      --,ce_bank_branches_v                                  cbb
       WHERE nvl(branch_status, 'X') <> 'P'
            --AND bank_status = 'P'
            --AND a.bank_id IS NOT NULL
         AND upper(TRIM(a.r12_bank_name)) = upper(cbb.party_name)
            --AND TRIM(a.country) = cbb.bank_home_country
         AND upper(br.bank_name) = upper(TRIM(r11_bank_name))
         AND TRIM(a.branch_num) = br.bank_num
         AND TRIM(a.country) = br.country
            --AND upper(TRIM(r12_bank_name)) <> 'CITIBANK'
         AND br.bank_branch_id IN
             (SELECT ba.bank_branch_id
                FROM ap.ap_bank_accounts_all@r12_to_fin.hsi.hughessupply.com ba
               WHERE ba.bank_branch_id = br.bank_branch_id
                 AND ba.account_type = 'SUPPLIER'
                 AND ba.inactive_date IS NULL
                 AND ba.attribute2 IS NULL
                 AND ba.bank_account_id IN
                     (SELECT bank_account_id
                        FROM ap.ap_bank_account_uses_all@r12_to_fin.hsi.hughessupply.com bau
                       WHERE bau.external_bank_account_id = ba.bank_account_id
                         AND bau.end_date IS NULL
                         AND bau.vendor_id IN
                             (SELECT v.vendor_id
                                FROM po.po_vendors@r12_to_fin.hsi.hughessupply.com v
                               WHERE v.vendor_id = bau.vendor_id
                                 AND v.enabled_flag = 'Y'
                                 AND nvl(v.end_date_active, SYSDATE) >= SYSDATE)))
         AND (upper(TRIM(a.r12_bank_name))
             ,br.bank_num
             ,TRIM(a.country)
             ,cbb.party_id) NOT IN
             (SELECT upper(bank_name)
                    ,branch_number
                    ,bank_home_country
                    ,bank_party_id
                FROM ce_bank_branches_v z
               WHERE z.bank_party_id = cbb.party_id
                 AND z.branch_number = br.bank_num
                 AND z.bank_home_country = TRIM(a.country));
  
  BEGIN
  
    fnd_file.put_line(fnd_file.log, 'BEFORE BANK API');
  
    FOR bank IN c_bank
    LOOP
      BEGIN
      
        v_error_code    := NULL;
        v_return_status := NULL;
        v_msg_count     := NULL;
        v_msg_data      := NULL;
      
        v_extbank_rec_type.object_version_number := 1.0;
        v_extbank_rec_type.bank_name             := upper(bank.bank_name);
        v_extbank_rec_type.institution_type      := bank.institution_type;
        v_extbank_rec_type.country_code          := nvl(bank.country, 'US');
      
        iby_ext_bankacct_pub.create_ext_bank(p_api_version   => 1.0
                                            ,p_init_msg_list => fnd_api.g_true
                                            ,p_ext_bank_rec  => v_extbank_rec_type
                                            ,x_bank_id       => x_bank_id
                                            ,x_return_status => v_return_status
                                            ,x_msg_count     => v_msg_count
                                            ,x_msg_data      => v_msg_data
                                            ,x_response      => x_result_rec_type);
      
        l_sec := 'Bank Name:  ' || bank.bank_name || ' Country:  ' ||
                 bank.country;
      
        IF v_return_status <> fnd_api.g_ret_sts_success
        THEN
          --fnd_file.put_line(fnd_file.log ,('ERROR ' || l_sec || ' ~ ' || v_msg_count));
          IF v_msg_count >= 1
          
          THEN
            --fnd_file.put_line(fnd_file.log,('ERROR DATA' || l_sec || ' ~ ' || v_msg_data));
          
            FOR i IN 1 .. v_msg_count
            LOOP
            
              apps.fnd_msg_pub.get(i, fnd_api.g_false, v_msg_data, v_msg);
            
              v_error_code := v_error_code || '-' ||
                              ('Msg' || to_char(i) || ': ' || v_msg_data);
            
              --fnd_file.put_line(fnd_file.log, (substr(v_error_code, 1, 250)));
            
              v_error_reason := v_error_code;
              fnd_file.put_line(fnd_file.log
                               ,('Error in Bank:' || l_sec || ' ~ ' ||
                                v_error_reason));
            
            END LOOP;
          
            UPDATE xxcus.xxcusap_bank_map_tbl
               SET bank_status = 'E'
                  ,bank_error  = v_error_reason
                  ,bank_date   = SYSDATE
             WHERE r12_bank_name = upper(bank.bank_name);
          
          END IF;
          --ROLLBACK;
        ELSE
          v_bank_id := x_bank_id;
          fnd_file.put_line(fnd_file.log
                           ,('SUCCESS ' || l_sec || ' Party ID ' || v_bank_id));
          --update mapping table
          UPDATE xxcus.xxcusap_bank_map_tbl
             SET bank_status = 'P', bank_id = x_bank_id, bank_date = SYSDATE
           WHERE upper(TRIM(r12_bank_name)) = upper(bank.bank_name)
             AND upper(TRIM(country)) = bank.country;
          --COMMIT;
        END IF;
      END;
    END LOOP;
  
    --loading branch 
    BEGIN
    
      fnd_file.put_line(fnd_file.log, '4687 Starting Branch API ');
    
      FOR branch IN c_branch
      LOOP
        fnd_file.put_line(fnd_file.log
                         ,'Bank ID: ' || branch.bank_party_id || ' Branch: ' ||
                          upper(branch.bank_num));
      
        v_error_code    := NULL;
        v_return_status := NULL;
        v_msg_count     := NULL;
        v_msg_data      := NULL;
      
        v_extbankbranch_rec_type.bank_party_id := branch.bank_party_id;
        v_extbankbranch_rec_type.branch_name   := upper(branch.bank_branch_name);
        v_extbankbranch_rec_type.branch_number := CASE
                                                    WHEN branch.bank_branch_type =
                                                         'ABA' THEN
                                                     branch.bank_num
                                                    ELSE
                                                     NULL
                                                  END;
        v_extbankbranch_rec_type.bic := CASE
                                          WHEN branch.bank_branch_type = 'SWIFT' THEN
                                           branch.bank_num
                                          ELSE
                                           NULL
                                        END;
        v_extbankbranch_rec_type.branch_type   := branch.bank_branch_type;
      
        iby_ext_bankacct_pub.create_ext_bank_branch(p_api_version         => 1.0
                                                   ,p_init_msg_list       => fnd_api.g_true
                                                   ,p_ext_bank_branch_rec => v_extbankbranch_rec_type
                                                   ,x_branch_id           => x_branch_id
                                                   ,x_return_status       => v_return_status
                                                   ,x_msg_count           => v_msg_count
                                                   ,x_msg_data            => v_msg_data
                                                   ,x_response            => x_result_rec_type);
      
        l_sec := 'Party ID:  ' || branch.bank_party_id || ' Branch Name:  ' ||
                 branch.bank_branch_name || ' Branch Num:  ' || branch.bank_num;
      
        IF v_return_status <> fnd_api.g_ret_sts_success
        THEN
          fnd_file.put_line(fnd_file.log, 'BRANCH ERROR ' || v_msg_count);
        
          IF v_msg_count >= 1
          THEN
            FOR i IN 1 .. v_msg_count
            LOOP
              apps.fnd_msg_pub.get(i, fnd_api.g_false, v_msg_data, v_msg);
              v_error_code := v_error_code || '-' ||
                              ('Msg' || to_char(i) || ': ' || v_msg_data);
            
              v_error_reason := v_error_code;
              fnd_file.put_line(fnd_file.log
                               ,('Error in Branch: ' || l_sec || ' ~ ' ||
                                v_error_reason));
            
            END LOOP;
          
            UPDATE xxcus.xxcusap_bank_map_tbl
               SET branch_status  = 'E'
                  ,branch_error   = v_error_reason
                  ,branch_created = SYSDATE
             WHERE upper(TRIM(r12_bank_name)) = upper(branch.bank_name)
               AND upper(TRIM(r12_branch_name)) =
                   upper(branch.bank_branch_name)
               AND upper(TRIM(country)) = branch.country;
          
          END IF;
          --ROLLBACK;
        ELSE
          v_branch_id := x_branch_id;
        
          fnd_file.put_line(fnd_file.output
                           ,'BRANCH ID Created -' || v_branch_id);
        
          UPDATE xxcus.xxcusap_bank_map_tbl
             SET branch_status  = 'P'
                ,branch_created = SYSDATE
                ,branch_id      = v_branch_id
           WHERE upper(TRIM(r12_bank_name)) = upper(branch.bank_name)
             AND upper(TRIM(r12_branch_name)) = upper(branch.bank_branch_name)
             AND upper(TRIM(country)) = branch.country;
        
        END IF;
      
        IF v_return_status = 'S' AND branch.address_line1 IS NOT NULL
        THEN
        
          v_error_reason  := NULL;
          v_return_status := NULL;
          v_msg_count     := NULL;
          v_msg_data      := NULL;
        
          v_location_rec.country               := nvl(branch.country, 'US');
          v_location_rec.address1              := branch.address_line1;
          v_location_rec.address2              := branch.address_line2;
          v_location_rec.city                  := branch.city;
          v_location_rec.postal_code           := branch.zip;
          v_location_rec.state                 := branch.state;
          v_location_rec.county                := 'X';
          v_location_rec.orig_system_reference := 'BRANCH~' || v_branch_id;
          v_location_rec.created_by_module     := 'XXCUS_INTERFACE';
        
          hz_location_v2pub.create_location(p_init_msg_list => fnd_api.g_true
                                           ,p_location_rec  => v_location_rec
                                           ,x_location_id   => x_location_id
                                           ,x_return_status => v_return_status
                                           ,x_msg_count     => v_msg_count
                                           ,x_msg_data      => v_msg_data);
        
          v_location_id := x_location_id;
          l_sec         := 'Created Location ID :  ' || x_location_id ||
                           ' Branch ID : ' || v_branch_id || ' Status = ' ||
                           v_return_status || ' Reason = ' || v_msg_data;
          --fnd_file.put_line(fnd_file.output, l_sec);
        
          IF v_return_status <> fnd_api.g_ret_sts_success
          THEN
            fnd_file.put_line(fnd_file.log, 'LOCATION ERROR ' || v_msg_count);
          
            IF v_msg_count >= 1
            THEN
              FOR i IN 1 .. v_msg_count
              LOOP
                apps.fnd_msg_pub.get(i, fnd_api.g_false, v_msg_data, v_msg);
                v_error_code := v_error_code || '-' ||
                                ('Msg' || to_char(i) || ': ' || v_msg_data);
              
                v_error_reason := v_error_code;
                fnd_file.put_line(fnd_file.log
                                 ,('Error in Location: ' || l_sec || ' ~ ' ||
                                  v_error_reason));
              
              END LOOP;
            
            END IF;
            --ROLLBACK;
          ELSE
            v_location_id := x_location_id;
          
            fnd_file.put_line(fnd_file.output
                             ,'Location ID Created -' || v_location_id);
          
          END IF;
        
          --COMMIT;
        
          --End Location API for the Bank Branch
        
          IF v_return_status = 'S'
          THEN
            --Start Party Site API for the pay to vendor sites
          
            v_error_reason  := NULL;
            v_return_status := NULL;
            v_msg_count     := NULL;
            v_msg_data      := NULL;
          
            v_party_site_rec.party_id                 := v_branch_id;
            v_party_site_rec.party_site_number        := branch.bank_branch_name;
            v_party_site_rec.location_id              := x_location_id;
            v_party_site_rec.party_site_name          := branch.bank_name || '~' ||
                                                         branch.bank_branch_name;
            v_party_site_rec.orig_system_reference    := 'BRANCH~' ||
                                                         v_branch_id;
            v_party_site_rec.identifying_address_flag := 'N';
            v_party_site_rec.created_by_module        := 'XXCUS_INTERFACE';
          
            hz_party_site_v2pub.create_party_site(p_init_msg_list     => fnd_api.g_true
                                                 ,p_party_site_rec    => v_party_site_rec
                                                 ,x_party_site_id     => x_party_site_id
                                                 ,x_party_site_number => x_party_site_number
                                                 ,x_return_status     => v_return_status
                                                 ,x_msg_count         => v_msg_count
                                                 ,x_msg_data          => v_msg_data);
          
            l_sec := 'Branch Name Site loaded : ' || branch.bank_branch_name ||
                     ' - Party Site ID:  ' || x_party_site_id || ' Status = ' ||
                     v_return_status || ' Reason = ' || v_msg_data;
            --fnd_file.put_line(fnd_file.log, l_sec);
            fnd_file.put_line(fnd_file.output, l_sec);
          
            IF v_return_status <> fnd_api.g_ret_sts_success
            THEN
              fnd_file.put_line(fnd_file.log
                               ,'BRANCH SITE ERROR ' || v_msg_count);
            
              IF v_msg_count >= 1
              THEN
                FOR i IN 1 .. v_msg_count
                LOOP
                  apps.fnd_msg_pub.get(i, fnd_api.g_false, v_msg_data, v_msg);
                  v_error_code := v_error_code || '-' ||
                                  ('Msg' || to_char(i) || ': ' || v_msg_data);
                
                  v_error_reason := v_error_code;
                  fnd_file.put_line(fnd_file.log
                                   ,('Error in : ' || l_sec || ' ~ ' ||
                                    v_error_reason));
                
                END LOOP;
              
              END IF;
              --ROLLBACK;
            ELSE
              v_party_site_id := x_party_site_id;
            
              fnd_file.put_line(fnd_file.output
                               ,'Location ID Created -' || v_party_site_id);
            
            END IF;
          
          END IF;
        END IF;
      END LOOP;
    
    EXCEPTION
      WHEN OTHERS THEN
        fnd_file.put_line(fnd_file.log, 'ERROR Loading Branch - ' || SQLERRM);
        fnd_file.put_line(fnd_file.output
                         ,'ERROR Loading Branch - ' || SQLERRM);
      
    END;
  
  EXCEPTION
    WHEN OTHERS THEN
      fnd_file.put_line(fnd_file.log, SQLERRM || '-' || SQLCODE);
      fnd_file.put_line(fnd_file.output, SQLERRM || '-' || SQLCODE);
    
  END xxcus_bank_load;

  /*******************************************************************************
  * Procedure:   
  * Description: Will be used to load Bank Accounts for Suppliers
  *              into R12 from R11   
  *              Active supplier bank accounts will be converted  
  *              Supplier bank accounts will load after Suppliers and Bank/Branch.
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     03/31/2011    Kathy Poling    Initial creation  
  ********************************************************************************/
  PROCEDURE xxcus_supp_bank_acct(errbuf OUT VARCHAR2, retcode OUT NUMBER) IS
    v_bank_acct_rec apps.iby_ext_bankacct_pub.extbankacct_rec_type;
    v_out_mesg      apps.iby_fndcpt_common_pub.result_rec_type;
    v_acct          NUMBER;
    v_assigns       apps.iby_fndcpt_setup_pub.pmtinstrassignment_tbl_type;
    v_payee_rec     apps.iby_disbursement_setup_pub.payeecontext_rec_type;
    v_return_status VARCHAR2(30);
    v_output        VARCHAR2(1000);
    v_msg_count     NUMBER;
    v_msg_data      VARCHAR2(150) := NULL;
    v_msg_dummy     VARCHAR2(150) := NULL;
    v_bank_id       NUMBER;
    v_branch_id     NUMBER;
    v_party_id      NUMBER;
    --   l_bank                              VARCHAR2 (100);
    v_acct_owner_party_id NUMBER;
    v_supplier_site_id    NUMBER;
    v_party_site_id       NUMBER;
    v_process_assign      VARCHAR2(1);
    exec_bank_acct EXCEPTION;
    v_rec       iby_disbursement_setup_pub.payeecontext_rec_type;
    v_assign    iby_fndcpt_setup_pub.pmtinstrassignment_rec_type;
    v_assign_id NUMBER;
    --v_return_status             VARCHAR2 (100);
    --v_msg_count                 NUMBER;
    --v_msg_data                  VARCHAR2 (200);
    v_response_rec iby_fndcpt_common_pub.result_rec_type;
    v_acc_id       NUMBER := 0;
    v_error_output VARCHAR2(1000);
    v_error_reason VARCHAR2(1000);
  
    l_sec VARCHAR2(255);
  
    -- v_msg_dummy                   VARCHAR2 (1000);
  
    CURSOR cur_acc_assign IS
      SELECT DISTINCT a.r12_bank_name     bank_name
                     ,b.bank_party_id
                     ,a.r12_branch_name   bank_branch_name
                     ,b.branch_party_id
                     ,ba.account_type
                     ,ba.bank_account_num
                     ,ba.currency_code
                     ,ba.description
                     ,br.institution_type
                     ,bau.primary_flag
                     ,aps.vendor_id
                     ,aps.party_id        vendor_party_id
                     ,aps.vendor_name
                      --,bau.org_id
                     ,apss.party_site_id
                     ,apss.vendor_site_id
                     ,apss.vendor_site_code
                     ,b.bank_home_country
                     ,apss.org_id
                     ,a.rowid
        FROM ap.ap_bank_accounts_all@r12_to_fin.hsi.hughessupply.com     ba
            ,ap.ap_bank_account_uses_all@r12_to_fin.hsi.hughessupply.com bau
            ,ap.ap_bank_branches@r12_to_fin.hsi.hughessupply.com         br
            ,po.po_vendors@r12_to_fin.hsi.hughessupply.com               v
            ,po.po_vendor_sites_all@r12_to_fin.hsi.hughessupply.com      vs
            ,apps.ce_bank_branches_v                                     b
            ,xxcus.xxcusap_bank_map_tbl                                  a
            ,ap.ap_suppliers                                             aps
            ,ap.ap_supplier_sites_all                                    apss
       WHERE nvl(a.acct_status, 'X') <> 'P'
            --AND branch_status = 'P'
         AND bau.external_bank_account_id = ba.bank_account_id
         AND ba.bank_branch_id = br.bank_branch_id
            --AND ba.account_type NOT IN ('CUSTOMER', 'INTERNAL')
         AND ba.account_type = 'SUPPLIER'
         AND ba.inactive_date IS NULL
         AND bau.end_date IS NULL
         AND ba.attribute2 IS NULL
         AND bau.vendor_id = v.vendor_id
         AND v.enabled_flag = 'Y'
         AND nvl(v.end_date_active, SYSDATE) >= SYSDATE
         AND bau.vendor_site_id = vs.vendor_site_id
         AND nvl(vs.inactive_date, SYSDATE) >= SYSDATE
         AND upper(br.bank_name) = upper(r11_bank_name)
         AND upper(br.bank_branch_name) = upper(r11_branch_name)
         AND a.country = br.country
         AND upper(r12_bank_name) = upper(b.bank_name)
         AND upper(r12_branch_name) = b.bank_branch_name
         AND a.country = b.bank_home_country
         AND to_char(vs.vendor_site_id) = apss.attribute14
         AND to_char(vs.vendor_id) = apss.attribute15
         AND apss.vendor_id = aps.vendor_id
         AND (b.branch_party_id, ba.bank_account_num) NOT IN
             (SELECT branch_id, bank_account_num
                FROM iby_ext_bank_accounts ieba
               WHERE ieba.branch_id = b.branch_party_id
                 AND ieba.bank_account_num = ba.bank_account_num);
  
  BEGIN
  
    FOR i IN cur_acc_assign
    LOOP
    
      l_sec := 'Vendor Name: ' || i.vendor_name || ' ~ Site Code: ' ||
               i.vendor_site_code || ' ~ Account:  ' || i.bank_account_num;
    
      v_error_output  := NULL;
      v_return_status := NULL;
      v_msg_count     := NULL;
      --l_msg_data := NULL;
      apps.fnd_msg_pub.delete_msg(NULL);
      apps.fnd_msg_pub.initialize();
      ---------------------------------------------------------------------
      --v_bank_acct_rec.branch_id := NULL;
      --v_bank_acct_rec.bank_id := NULL;
      v_party_id := NULL;
      --v_bank_acct_rec.acct_owner_party_id := NULL;
      --v_bank_acct_rec.bank_account_name := NULL;
      --v_bank_acct_rec.bank_account_num := NULL;
      v_bank_acct_rec.iban       := NULL;
      v_bank_acct_rec.start_date := SYSDATE;
      --v_bank_acct_rec.country_code := NULL;
      --v_bank_acct_rec.currency := NULL;
      --v_bank_acct_rec.foreign_payment_use_flag := NULL;
      v_bank_acct_rec.alternate_acct_name := NULL;
      v_error_reason                      := NULL;
      v_process_assign                    := NULL;
      v_bank_acct_rec.branch_id           := i.branch_party_id;
      --l_bank_acct_rec.branch_name := i.branch_number;
      v_bank_acct_rec.bank_id                  := i.bank_party_id;
      v_bank_acct_rec.acct_owner_party_id      := i.vendor_party_id;
      v_bank_acct_rec.bank_account_name        := i.bank_name;
      v_bank_acct_rec.bank_account_num         := i.bank_account_num;
      v_bank_acct_rec.country_code             := i.bank_home_country;
      v_bank_acct_rec.currency                 := i.currency_code;
      v_bank_acct_rec.foreign_payment_use_flag := 'N';
    
      BEGIN
        SELECT ext_bank_account_id
          INTO v_acct
          FROM iby_ext_bank_accounts
         WHERE bank_id = i.bank_party_id
           AND branch_id = i.branch_party_id
           AND bank_account_num = i.bank_account_num;
      
        fnd_file.put_line(fnd_file.output
                         ,(' ext BANK ACCOUNT EXISTS ' || i.bank_account_num));
        v_process_assign := 'Y';
      EXCEPTION
        WHEN OTHERS THEN
          apps.iby_ext_bankacct_pub.create_ext_bank_acct(p_api_version       => 1.0
                                                        ,p_init_msg_list     => 'F'
                                                        ,p_ext_bank_acct_rec => v_bank_acct_rec
                                                        ,x_acct_id           => v_acct
                                                        ,x_return_status     => v_return_status
                                                        ,x_msg_count         => v_msg_count
                                                        ,x_msg_data          => v_msg_data
                                                        ,x_response          => v_out_mesg);
        
          -- l_output := ' ';
          IF v_return_status = fnd_api.g_ret_sts_error
          THEN
            fnd_file.put_line(fnd_file.log
                             ,(' ext assignment ERROR ' || v_msg_count));
          
            FOR i IN 1 .. v_msg_count
            LOOP
              apps.fnd_msg_pub.get(i, fnd_api.g_false, v_msg_data, v_msg_dummy);
              v_error_reason := v_error_reason || '-' ||
                                ('Msg' || to_char(i) || ': ' || v_msg_data);
              fnd_file.put_line(fnd_file.log, (substr(v_error_reason, 1, 250)));
              v_error_output := v_error_reason;
              fnd_file.put_line(fnd_file.log
                               ,('Error in Account: ' || l_sec || ' : ' ||
                                v_error_output));
            END LOOP;
          
            UPDATE xxcus.xxcusap_bank_map_tbl
               SET acct_status = 'E', acct_error = v_error_output
             WHERE ROWID = i.rowid;
          
            v_process_assign := 'N';
            --                COMMIT;
          ELSE
            v_process_assign := 'Y';
          END IF;
      END;
    
      IF v_process_assign = 'Y'
      THEN
        v_acc_id := v_acct;
        fnd_file.put_line(fnd_file.log, ('Success ' || v_acc_id));
      
        UPDATE xxcus.xxcusap_bank_map_tbl
           SET bank_acct_id = v_acc_id, acct_status = 'A'
         WHERE ROWID = i.rowid;
      
        --COMMIT;
        --END LOOP;
      
        -- FOR J IN CUR_ACC_ASSIGN (I.BANK_EXT_ACCOUNT_ID)
        -- LOOP
        v_assign.instrument.instrument_type := 'BANKACCOUNT';
        v_assign.instrument.instrument_id   := v_acc_id;
        v_rec.party_id                      := i.vendor_party_id;
        v_rec.org_id                        := i.org_id;
        v_rec.org_type                      := 'OPERATING_UNIT';
        v_rec.party_site_id                 := i.party_site_id;
        v_rec.supplier_site_id              := i.vendor_site_id;
        v_rec.payment_function              := 'PAYABLES_DISB';
      
        iby_disbursement_setup_pub.set_payee_instr_assignment(p_api_version        => 1.0
                                                             ,p_init_msg_list      => fnd_api.g_true
                                                             ,p_payee              => v_rec
                                                             ,p_assignment_attribs => v_assign
                                                             ,x_assign_id          => v_assign_id
                                                             ,x_return_status      => v_return_status
                                                             ,x_msg_count          => v_msg_count
                                                             ,x_msg_data           => v_msg_data
                                                             ,x_response           => v_response_rec);
      
        IF v_return_status = fnd_api.g_ret_sts_error
        THEN
          fnd_file.put_line(fnd_file.log
                           ,(' Assignment ERROR ' || v_msg_count));
        
          FOR i IN 1 .. v_msg_count
          LOOP
            apps.fnd_msg_pub.get(i
                                ,apps.fnd_api.g_false
                                ,v_msg_data
                                ,v_msg_dummy);
            v_output := v_output ||
                        (to_char(i) || ': ' || substr(v_msg_data, 1, 250));
            fnd_file.put_line(fnd_file.log, (substr(v_output, 1, 250)));
            fnd_file.put_line(fnd_file.log
                             ,('Error in Assignment:' || v_output));
          END LOOP;
        
          UPDATE xxcus.xxcusap_bank_map_tbl
             SET acct_status = 'E', acct_error = v_output
           WHERE ROWID = i.rowid;
        ELSE
          UPDATE xxcus.xxcusap_bank_map_tbl
             SET acct_status = 'P'
           WHERE ROWID = i.rowid;
          --COMMIT;
        END IF;
      END IF;
    END LOOP;
  END xxcus_supp_bank_acct;

  /*******************************************************************************
  * Procedure:   
  * Description: Will be used to load AP Paid Invoice History Data 
  *              into R12 from R11.  The paid invoices will be loaded  
  *              to the history tables in R12 and used only for duplicate checking
  *              Only the history of invoices dated 8/1/2010 and later 
  *              
  *              
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     05/02/2011    Kathy Poling    Initial creation  
  
  ********************************************************************************/
  PROCEDURE xxcus_ap_inv_history(errbuf         OUT VARCHAR2
                                ,retcode        OUT NUMBER
                                ,p_history_name IN VARCHAR2) IS
    --'R11 Invoice Paid History' 
  
    l_err_code NUMBER;
    l_err_msg  VARCHAR2(3000);
    l_sec      VARCHAR2(255);
    l_org      NUMBER;
    l_org_gsc CONSTANT hr_all_organization_units.organization_id%TYPE := 163; --HD Supply US GSC
    l_org_pr  CONSTANT hr_all_organization_units.organization_id%TYPE := 166; --HD Supply US Garnishment
  
    l_err_callfrom  VARCHAR2(75) DEFAULT 'xxcus_r11_conv_pkg';
    l_err_callpoint VARCHAR2(75) DEFAULT 'START';
    l_distro_list   VARCHAR2(75) DEFAULT 'kathy.poling@hdsupply.com';
  
    --  
    CURSOR c_hist_inv IS
      SELECT i.invoice_id
            ,s.vendor_id
            ,s.vendor_site_code
            ,i.invoice_num
            ,i.invoice_date
            ,i.invoice_amount
            ,NULL batch_name
             --,'R11 Invoice Paid History'
            ,i.doc_sequence_id
            ,i.doc_sequence_value
            ,i.org_id
        FROM xxcus.xxcus_ap_invoices_conv i, ap.ap_supplier_sites_all s
       WHERE i.vendor_site_id = to_number(s.attribute14)
         AND i.vendor_id = to_number(s.attribute15)
         AND i.invoice_type_lookup_code <> 'EXPENSE REPORT'
         AND i.payment_status_flag = 'Y'
         AND i.invoice_date >= '01-Aug-2010';
  
  BEGIN
  
    FOR c_inv IN c_hist_inv
    LOOP
    
      IF c_inv.org_id = 81
      THEN
        l_org := l_org_gsc;
      ELSE
        l_org := l_org_pr;
      END IF;
    
      INSERT INTO ap.ap_history_invoices_all
        (invoice_id
        ,vendor_id
        ,vendor_site_code
        ,invoice_num
        ,invoice_date
        ,invoice_amount
        ,purge_name
        ,doc_sequence_id
        ,doc_sequence_value
        ,org_id)
      VALUES
        (apps.ap_invoices_s.nextval
        ,c_inv.vendor_id
        ,c_inv.vendor_site_code
        ,c_inv.invoice_num
        ,c_inv.invoice_date
        ,c_inv.invoice_amount
        ,p_history_name
        ,c_inv.doc_sequence_id
        ,c_inv.doc_sequence_value
        ,l_org);
    
      FOR c_check_hist IN (SELECT ac.check_id
                                 ,cb.bank_account_id
                                 ,ac.check_number
                                 ,ac.check_date
                                 ,ac.amount
                                 ,ac.currency_code
                                 ,decode(void_date, NULL, NULL, 'Y') void_date
                                 ,ac.doc_sequence_id
                                 ,ac.doc_sequence_value
                                 ,ac.org_id
                             FROM ap.ap_checks_all@r12_to_fin.hsi.hughessupply.com           ac
                                 ,ap.ap_invoice_payments_all@r12_to_fin.hsi.hughessupply.com ip
                                 ,ce.ce_bank_accounts                                        cb
                            WHERE ip.invoice_id = c_inv.invoice_id
                              AND ip.check_id = ac.check_id
                              AND upper(ac.bank_account_name) =
                                  upper(cb.bank_account_name)
                            GROUP BY ac.check_id
                                    ,cb.bank_account_id
                                    ,ac.check_number
                                    ,ac.check_date
                                    ,ac.amount
                                    ,ac.currency_code
                                    ,decode(void_date, NULL, NULL, 'Y')
                                    ,ac.doc_sequence_id
                                    ,ac.doc_sequence_value
                                    ,ac.org_id)
      LOOP
      
        INSERT INTO ap_history_checks_all
          (check_id
          ,bank_account_id
          ,check_number
          ,check_date
          ,amount
          ,currency_code
          ,void_flag
          ,purge_name
          ,doc_sequence_id
          ,doc_sequence_value
          ,payment_id
          ,org_id)
        VALUES
          (apps.ap_checks_s.nextval
          ,c_check_hist.bank_account_id
          ,c_check_hist.check_number
          ,c_check_hist.check_date
          ,c_check_hist.amount
          ,c_check_hist.currency_code
          ,c_check_hist.void_date
          ,p_history_name
          ,c_check_hist.doc_sequence_id
          ,c_check_hist.doc_sequence_value
          ,apps.iby_pay_payments_all_s.nextval
          ,l_org);
      
        FOR c_payment IN (SELECT ip.invoice_id
                                ,ip.check_id
                                ,SUM(ip.amount) amount
                                ,ip.org_id
                            FROM ap.ap_invoice_payments_all@r12_to_fin.hsi.hughessupply.com ip
                           WHERE ip.invoice_id = c_inv.invoice_id
                             AND ip.check_id = c_check_hist.check_id
                           GROUP BY ip.invoice_id, ip.check_id, ip.org_id)
        LOOP
        
          INSERT INTO ap_history_inv_payments_all
            (invoice_id, check_id, amount, org_id)
          VALUES
            (apps.ap_invoices_s.currval
            ,apps.ap_checks_s.currval
            ,c_payment.amount
            ,l_org);
        
        END LOOP;
      
      END LOOP;
    
    END LOOP;
  
  EXCEPTION
    WHEN OTHERS THEN
      fnd_file.put_line(fnd_file.log, SQLERRM || '-' || SQLCODE);
      fnd_file.put_line(fnd_file.output, SQLERRM || '-' || SQLCODE);
    
  END;

  /*******************************************************************************
  * Procedure:   
  * Description: Will be used to load AP Open Invoice Data into R12 from R11. 
  *              The invoices will be loaded into the interface tables in R12
  *              All open invoices (expense and freight and expense reports) 
  *              which have not been paid or are partially paid should be included 
  *              in the extract.
  *              
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     05/04/2011    Kathy Poling    Initial creation  
  
  ********************************************************************************/

  PROCEDURE xxcus_ap_invoice_open(errbuf OUT VARCHAR2, retcode OUT NUMBER) IS
  
    l_err_code NUMBER;
    l_err_msg  VARCHAR2(3000);
    l_sec      VARCHAR2(255);
    l_org      NUMBER;
    l_org_gsc CONSTANT hr_all_organization_units.organization_id%TYPE := 163; --HD Supply US GSC
    l_org_pr  CONSTANT hr_all_organization_units.organization_id%TYPE := 166; --HD Supply US Garnishment
    l_group_id           VARCHAR2(80) := 'CONVERSION R11';
    l_count              NUMBER;
    l_user_name          VARCHAR2(100) := 'KP027916';
    l_can_submit_request BOOLEAN := TRUE;
    l_globalset          VARCHAR2(100);
  
    v_req_id            NUMBER;
    v_call_status       BOOLEAN;
    v_set_req_status    BOOLEAN;
    v_req_phase         VARCHAR2(80);
    v_req_status        VARCHAR2(80);
    v_dev_phase         VARCHAR2(30);
    v_dev_status        VARCHAR2(30);
    v_req_message       VARCHAR2(255);
    v_interval          NUMBER := 10; -- In seconds
    v_max_time          NUMBER := 15000; -- In seconds
    v_completion_status VARCHAR2(30) := 'NORMAL';
    v_error_msg         VARCHAR2(150);
  
    l_err_callfrom  VARCHAR2(75) DEFAULT 'xxcus_r11_conv_pkg';
    l_err_callpoint VARCHAR2(75) DEFAULT 'START';
    l_distro_list   VARCHAR2(75) DEFAULT 'kathy.poling@hdsupply.com';
  
    CURSOR c_open_inv IS
      SELECT i.invoice_id
            ,v.segment1 vendor_num
            ,s.vendor_site_code
            ,i.invoice_num
            ,i.set_of_books_id
            ,i.invoice_currency_code
            ,i.invoice_amount
            ,i.amount_paid
            ,i.discount_amount_taken
            ,i.invoice_date
            ,'CONVERSION R11' SOURCE
            ,i.invoice_type_lookup_code
            ,i.description
            ,i.terms_id
            ,i.terms_date
            ,i.payment_method_lookup_code
            ,i.pay_group_lookup_code
            ,i.payment_status_flag
            ,i.creation_date
            ,(CASE
               WHEN i.org_id = 262 AND i.attribute1 IS NOT NULL THEN
                i.attribute1
               ELSE
                NULL
             END) attribute1 -- case_identifier  for org 262 using value set 20 characters
            ,(CASE
               WHEN i.org_id = 262 AND i.attribute2 IS NOT NULL THEN
                i.attribute2
               ELSE
                NULL
             END) attribute2 --deduction_code  for org 262 using value set 4 characters
            ,(CASE
               WHEN i.org_id = 262 AND i.attribute3 IS NOT NULL THEN
                i.attribute3
               ELSE
                NULL
             END) attribute3 --employee  for org 262 using value set 10 characters
            ,(CASE
               WHEN i.org_id = 81 AND i.attribute11 IS NOT NULL THEN
                i.attribute11
               ELSE
                NULL
             END) attribute11 --invoice_type  for org 81 using value set HSI_DCTM_INV_TYPES
            ,(CASE
               WHEN i.attribute_category = 262 THEN
                l_org_pr
               WHEN i.attribute_category = 81 AND i.attribute11 IS NOT NULL THEN
                l_org_gsc
               ELSE
                NULL
             END) attribute_category
            ,i.org_id
            ,i.pay_curr_invoice_amount
            ,i.gl_date
            ,i.validated_tax_amount
        FROM xxcus.xxcus_ap_invoices_conv                           i
            ,po.po_vendors@r12_to_fin.hsi.hughessupply.com          v
            ,po.po_vendor_sites_all@r12_to_fin.hsi.hughessupply.com s
       WHERE i.vendor_id = v.vendor_id
         AND i.vendor_site_id = s.vendor_site_id
         AND invoice_amount <> 0
         AND invoice_amount <> nvl(amount_paid, 0)
         AND payment_status_flag <> 'Y'
         AND cancelled_amount IS NULL;
  
  BEGIN
  
    --  Setup parameters for running FND JOBS!
    l_can_submit_request := xxcus_misc_pkg.set_responsibility(l_user_name
                                                             ,'XXCUS_CON');
    IF l_can_submit_request
    THEN
      l_globalset := 'Global Variables are set.';
    
    ELSE
    
      l_globalset := 'Global Variables are not set.';
      l_sec       := 'Global Variables are not set for the Responsibility of XXCUS_CON and the User.';
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      RAISE program_error;
    END IF;
  
    --Start loading invoices
    FOR c_inv IN c_open_inv
    LOOP
    
      IF c_inv.org_id = 81
      THEN
        l_org := l_org_gsc;
      ELSE
        l_org := l_org_pr;
      END IF;
    
      INSERT INTO ap.ap_invoices_interface
        (invoice_id
        ,invoice_num
        ,description
        ,invoice_type_lookup_code
        ,invoice_date
        ,vendor_num
        ,vendor_site_code
        ,invoice_amount
        ,invoice_currency_code
        ,last_update_date
        ,last_updated_by
        ,creation_date
        ,created_by
        ,attribute1
        ,attribute2
        ,attribute3
        ,attribute11
        ,attribute15 --R11 invoice_id used for conv to attach documents
        ,SOURCE
        ,group_id
        ,org_id
        ,attribute_category)
      VALUES
        (ap_invoices_interface_s.nextval
        ,c_inv.invoice_num
        ,c_inv.description
        ,c_inv.invoice_type_lookup_code
        ,c_inv.invoice_date
        ,c_inv.vendor_num
        ,c_inv.vendor_site_code
        ,c_inv.invoice_amount
        ,c_inv.invoice_currency_code
        ,SYSDATE
        ,3
        ,SYSDATE
        ,3
        ,c_inv.attribute1
        ,c_inv.attribute2
        ,c_inv.attribute3
        ,c_inv.attribute11
        ,c_inv.invoice_id --R11 invoice id used for conv to attach documents
        ,c_inv.source
        ,l_group_id
        ,l_org
        ,c_inv.attribute_category);
    
      FOR c_line IN (SELECT d.invoice_id
                           ,d.invoice_distribution_id
                           ,d.amount
                           ,d.distribution_line_number
                           ,d.dist_code_combination_id
                           ,(CASE
                              WHEN g.segment1 IN ('08', '31') THEN
                               '0W'
                              WHEN g.segment1 IN ('43', '44', '45', '46') THEN
                               '42'
                              WHEN g.segment1 = '12' THEN
                               '11'
                              ELSE
                               g.segment1
                            END) || '.' || (CASE
                              WHEN g.segment2 LIKE 'B0%' THEN
                               'BW' || substr(g.segment2, 3, 3)
                              ELSE
                               g.segment2
                            END) || '.' || g.segment3 || '.' || g.segment4 || '.' ||
                            g.segment5 || '.' || g.segment6 || '.00000' gl_code
                            /*,(CASE
                              WHEN d.line_type_lookup_code = 'TAX' THEN
                               'MISCELLANEOUS'
                              ELSE
                               d.line_type_lookup_code
                            END) line_type_lookup_code */
                           ,d.line_type_lookup_code
                           ,(CASE
                              WHEN d.line_type_lookup_code = 'TAX' THEN
                               'XXCUS_TAX_RATE'
                              ELSE
                               NULL
                            END) tax_code
                           ,(CASE
                              WHEN d.line_type_lookup_code = 'TAX' THEN
                               'N'
                              ELSE
                               NULL
                            END) tax_recoverable_flag
                           ,d.description
                           ,d.set_of_books_id
                           ,d.creation_date
                           ,d.org_id
                       FROM ap.ap_invoice_distributions_all@r12_to_fin.hsi.hughessupply.com d
                           ,gl.gl_code_combinations@r12_to_fin.hsi.hughessupply.com         g
                      WHERE invoice_id = c_inv.invoice_id
                        AND dist_code_combination_id = g.code_combination_id)
      LOOP
      
        INSERT INTO ap_invoice_lines_interface
          (invoice_id
          ,invoice_line_id
          ,line_number
          ,line_type_lookup_code
          ,amount
          ,description
          ,dist_code_concatenated
          ,last_updated_by
          ,last_update_date
          ,created_by
          ,creation_date
          ,org_id
          ,tax_code
          ,tax_recoverable_flag)
        VALUES
          (ap_invoices_interface_s.currval
          ,ap_invoice_lines_interface_s.nextval
          ,c_line.distribution_line_number
          ,c_line.line_type_lookup_code
          ,c_line.amount
          ,c_line.description
          ,c_line.gl_code
          ,3
          ,SYSDATE
          ,3
          ,SYSDATE
          ,l_org
          ,c_line.tax_code
          ,c_line.tax_recoverable_flag);
      END LOOP;
      COMMIT;
    END LOOP;
    --start import of invoices
    BEGIN
      SELECT COUNT(*) INTO l_count FROM ap.ap_invoices_interface;
    
      IF l_count >= 1
      THEN
      
        fnd_file.put_line(fnd_file.log
                         ,'Payables Open Interface Import starting.  ');
        fnd_file.put_line(fnd_file.log
                         ,'Payables Open Interface Import starting.  ');
      
        FOR c_import IN (SELECT DISTINCT org_id FROM ap.ap_invoices_interface)
        LOOP
        
          -- Submit Payables Open Interface Import 
          v_req_id := fnd_request.submit_request('SQLAP'
                                                ,'APXIIMPT'
                                                ,NULL
                                                ,''
                                                ,FALSE
                                                ,c_import.org_id --Org ID
                                                ,l_group_id --Source
                                                ,l_group_id --Group
                                                ,NULL --Batch Name
                                                ,NULL --Hold Name
                                                ,NULL --Hold Reason
                                                ,NULL --GL date
                                                ,'N' --Purge
                                                ,'N' --Trace Switch
                                                ,'N' --Debug Switch
                                                ,'N' --Summarize Report
                                                ,1000); --Commit Batch Size
          --User ID
          --Login ID
        
          COMMIT;
        
          IF v_req_id != 0
          THEN
          
            -- Wait till the above request is completed.
            v_call_status := fnd_concurrent.wait_for_request(v_req_id
                                                            ,v_interval
                                                            ,v_max_time
                                                            ,v_req_phase
                                                            ,v_req_status
                                                            ,v_dev_phase
                                                            ,v_dev_status
                                                            ,v_req_message);
            IF v_dev_phase != 'COMPLETE' OR v_dev_status != 'NORMAL'
            THEN
              l_sec := 'An error occured in the Payables Open Interface Import, please review the Log for concurrent request ' ||
                       v_req_id || ' - ' || v_error_msg || '.';
              fnd_file.put_line(fnd_file.log, l_sec);
              fnd_file.put_line(fnd_file.output, l_sec);
              RAISE program_error;
            ELSE
              retcode := 0;
            
            END IF;
          END IF;
        END LOOP;
      END IF;
    END;
  
  EXCEPTION
    WHEN OTHERS THEN
      fnd_file.put_line(fnd_file.log, SQLERRM || '-' || SQLCODE);
      fnd_file.put_line(fnd_file.output, SQLERRM || '-' || SQLCODE);
    
  END xxcus_ap_invoice_open;

  /*******************************************************************************
  * Procedure:   
  * Description: Will be used to load attachment for the open invoices loaded 
  *              into R12 from R11. 
  *              
  *            
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     05/04/2011    Kathy Poling    Initial creation  
  
  ********************************************************************************/

  /*******************************************************************************
  * Procedure:   
  * Description: Will be used to load AP TE Accrual History Data into R12 from R11. 
  *              Concurrent program:  HDS R11 iExpenses TE Accrual History
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     07/26/2011    Mani Kumar      TE Accrual History Table Conversion 
  
  ********************************************************************************/

  PROCEDURE xxcusoie_te_accrual_hist_conv(errbuf  OUT VARCHAR2
                                         ,retcode OUT NUMBER) IS
    --Intialize Variables    
    l_sec            VARCHAR2(200);
    l_procedure_name VARCHAR2(75) := 'xxcus_r11_conv_pkg.xxcusoie_te_accrual_hist_conv';
    l_bef_cnt        NUMBER := 0;
  
  BEGIN
    l_sec := 'Start inserting data from PRD into XXCUSOIE_TE_ACCRUAL_HIST_TBL ';
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
  
    BEGIN
      SELECT COUNT(*)
        INTO l_bef_cnt
        FROM xxhsi.xxhsi_te_accrual_hist_tbl@r12_to_fin.hsi.hughessupply.com;
    END;
    l_sec := 'Count of Records from xxhsi.xxhsi_te_accrual_hist_tbl: ' ||
             l_bef_cnt;
    fnd_file.put_line(fnd_file.log, l_sec);
    INSERT /*+APPEND*/
    INTO xxcus.xxcusoie_te_accrual_hist_tbl
      (SELECT oracle_accounts
             ,oracle_product
             ,oracle_location
             ,oracle_cost_center
             ,oracle_account
             ,segment5
             ,segment6
             ,'00000' segment7
             ,item_description
             ,line_amount
             ,currency_code
             ,full_name
             ,employee_number
             ,emp_default_prod
             ,emp_default_loc
             ,emp_default_costctr
             ,fru
             ,merchant_name
             ,credit_card_trx_id
             ,transaction_date
             ,card_program_id
             ,card_program_name
             ,mcc_code_no
             ,mcc_code
             ,imported_to_ap
             ,imported_to_gl
             ,query_num
             ,query_descr
             ,parm_value
             ,expense_report_status
             ,start_expense_date
             ,end_expense_date
             ,accrual_period
             ,expense_report_number
             ,distribution_line_number
         FROM xxhsi.xxhsi_te_accrual_hist_tbl@r12_to_fin.hsi.hughessupply.com);
  
    l_sec := 'Committing transactions in XXCUSOIE_TE_ACCRUAL_HIST_TBL';
    COMMIT;
    l_bef_cnt := 0;
    BEGIN
      SELECT COUNT(*) INTO l_bef_cnt FROM xxcus.xxcusoie_te_accrual_hist_tbl;
    END;
    l_sec := 'Count of Records from XXCUS.XXCUSOIE_TE_ACCRUAL_HIST_TBL: ' ||
             l_bef_cnt;
    fnd_file.put_line(fnd_file.log, l_sec);
    l_sec := 'Completed inserting data from SQA into XXCUSOIE_TE_ACCRUAL_HIST_TBL';
  
  EXCEPTION
    WHEN OTHERS THEN
      retcode := SQLCODE;
      errbuf  := substr(SQLERRM, 1, 2000);
      fnd_file.put_line(fnd_file.log, 'Error in ' || l_procedure_name || l_sec);
      fnd_file.put_line(fnd_file.log, SQLERRM || '-' || SQLCODE);
      fnd_file.put_line(fnd_file.output, SQLERRM || '-' || SQLCODE);
  END xxcusoie_te_accrual_hist_conv;

  /*******************************************************************************
  * Procedure:   
  * Description: Will be used to load IEXP Bullet Train Data into R12 from R11. 
  *              Concurrent Program:  HDS R11 iExpenses Bullet Train History
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     08/01/2011    Mani Kumar      Bullet Train Table Conversion 
  
  ********************************************************************************/

  PROCEDURE xxcusoie_bullettrain_conv(errbuf OUT VARCHAR2, retcode OUT NUMBER) IS
    --Intialize Variables    
    l_sec            VARCHAR2(200);
    l_procedure_name VARCHAR2(75) := 'xxcus_r11_conv_pkg.xxcusoie_bullettrain_conv';
    l_count          NUMBER;
  BEGIN
    l_sec := 'Start inserting data from DVA into XXCUS_BULLET_IEXP_TBL ';
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
    --Code to insert data into Bullet Train Tables
    BEGIN
      SELECT COUNT(*)
        INTO l_count
        FROM xxhsi.xxhsi_bullet_iexp_tbl@r12_to_fin.hsi.hughessupply.com
       WHERE to_date(period_name, 'MON-YY') >= '01-JAN-2010'
         AND query_num IN (1, 4);
    END;
    l_sec := 'No.of Records selected to insert for query_num 1 and 4 from xxhsi.xxhsi_bullet_iexp_tbl: ' ||
             l_count;
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
  
    INSERT /*+APPEND*/
    INTO xxcus.xxcus_bullet_iexp_tbl
      (SELECT period_name
             ,cc_fiscal_period
             ,oracle_product
             ,product_descr
             ,oracle_location
             ,location_descr
             ,oracle_cost_center
             ,cost_center_descr
             ,oracle_account
             ,account_descr
             ,oracle_accounts
             ,item_description
             ,category
             ,sub_category
             ,line_amount
             ,currency_code
             ,invoice_number
             ,distribution_line_number
             ,full_name
             ,employee_number
             ,emp_default_prod
             ,emp_default_loc
             ,emp_default_costctr
             ,fru
             ,creation_date
             ,report_submitted_date
             ,description
             ,vendor_name
             ,merchant_name
             ,business_purpose
             ,justification
             ,start_expense_date
             ,end_expense_date
             ,remarks
             ,attendees
             ,attendees_emp
             ,attendees_te
             ,attendees_cti1
             ,attendees_cti2
             ,credit_card_trx_id
             ,cardmember_name
             ,transaction_date
             ,posted_date
             ,merchant_location
             ,destination_from
             ,destination_to
             ,distance_unit_code
             ,daily_distance
             ,trip_distance
             ,avg_mileage_rate
             ,dt_sentto_gl
             ,expense_report_number
             ,card_program_id
             ,card_program_name
             ,mcc_code_no
             ,mcc_code
             ,accounting_date
             ,report_total
             ,miles
             ,rate_per_mile
             ,attribute_category
             ,category_code
             ,transaction_amount
             ,receipt_currency_amount
             ,amt_due_ccard_company
             ,amt_due_employee
             ,imported_to_ap
             ,imported_to_gl
             ,workflow_approved_flag
             ,expense_current_approver_id
             ,approver_id
             ,approver_name
             ,report_type
             ,advance_invoice_to_apply
             ,advance_distribution_number
             ,advance_flag
             ,advance_gl_date
             ,advance_number
             ,report_reject_code
             ,app_post_flag
             ,approved_in_gl
             ,vouchno
             ,query_num
             ,query_descr
             ,expense_report_status
             ,reference_number
             ,project_number
             ,project_name
             ,'00000'
             ,'00000'
         FROM xxhsi.xxhsi_bullet_iexp_tbl@r12_to_fin.hsi.hughessupply.com
        WHERE to_date(period_name, 'MON-YY') >= '01-JAN-2010'
          AND query_num IN (1, 4));
  
    l_sec := 'Committing Records for query num 1 and 4 into XXCUS_BULLET_IEXP_TBL ';
    COMMIT;
  
    BEGIN
      l_count := 0;
      SELECT COUNT(*)
        INTO l_count
        FROM xxcus.xxcus_bullet_iexp_tbl
       WHERE to_date(period_name, 'MON-YY') >= '01-JAN-2010'
         AND query_num IN (1, 4);
    END;
    l_sec := 'No.of Records inserted for query_num 1 and 4 into XXCUS.XXCUS_BULLET_IEXP_TBL: ' ||
             l_count;
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
  
    l_count := 0;
    BEGIN
      SELECT COUNT(*)
        INTO l_count
        FROM xxhsi.xxhsi_bullet_iexp_tbl@r12_to_fin.hsi.hughessupply.com
       WHERE query_num IN (5);
    END;
    l_sec := 'No.of Records selected to insert for query_num 5 from xxhsi.xxhsi_bullet_iexp_tbl: ' ||
             l_count;
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
  
    INSERT /*+APPEND*/
    INTO xxcus.xxcus_bullet_iexp_tbl
      (SELECT period_name
             ,cc_fiscal_period
             ,oracle_product
             ,product_descr
             ,oracle_location
             ,location_descr
             ,oracle_cost_center
             ,cost_center_descr
             ,oracle_account
             ,account_descr
             ,oracle_accounts
             ,item_description
             ,category
             ,sub_category
             ,line_amount
             ,currency_code
             ,invoice_number
             ,distribution_line_number
             ,full_name
             ,employee_number
             ,emp_default_prod
             ,emp_default_loc
             ,emp_default_costctr
             ,fru
             ,creation_date
             ,report_submitted_date
             ,description
             ,vendor_name
             ,merchant_name
             ,business_purpose
             ,justification
             ,start_expense_date
             ,end_expense_date
             ,remarks
             ,attendees
             ,attendees_emp
             ,attendees_te
             ,attendees_cti1
             ,attendees_cti2
             ,credit_card_trx_id
             ,cardmember_name
             ,transaction_date
             ,posted_date
             ,merchant_location
             ,destination_from
             ,destination_to
             ,distance_unit_code
             ,daily_distance
             ,trip_distance
             ,avg_mileage_rate
             ,dt_sentto_gl
             ,expense_report_number
             ,card_program_id
             ,card_program_name
             ,mcc_code_no
             ,mcc_code
             ,accounting_date
             ,report_total
             ,miles
             ,rate_per_mile
             ,attribute_category
             ,category_code
             ,transaction_amount
             ,receipt_currency_amount
             ,amt_due_ccard_company
             ,amt_due_employee
             ,imported_to_ap
             ,imported_to_gl
             ,workflow_approved_flag
             ,expense_current_approver_id
             ,approver_id
             ,approver_name
             ,report_type
             ,advance_invoice_to_apply
             ,advance_distribution_number
             ,advance_flag
             ,advance_gl_date
             ,advance_number
             ,report_reject_code
             ,app_post_flag
             ,approved_in_gl
             ,vouchno
             ,query_num
             ,query_descr
             ,expense_report_status
             ,reference_number
             ,project_number
             ,project_name
             ,'00000'
             ,'00000'
         FROM xxhsi.xxhsi_bullet_iexp_tbl@r12_to_fin.hsi.hughessupply.com
        WHERE query_num IN (5));
  
    l_sec := 'Committing Records for query num 5 into XXCUS_BULLET_IEXP_TBL ';
    COMMIT;
  
    BEGIN
      l_count := 0;
      SELECT COUNT(*)
        INTO l_count
        FROM xxcus.xxcus_bullet_iexp_tbl
       WHERE query_num IN (5);
    END;
    l_sec := 'No.of Records inserted for query_num 5 into XXCUS.XXCUS_BULLET_IEXP_TBL: ' ||
             l_count;
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
  
  EXCEPTION
    WHEN OTHERS THEN
      retcode := SQLCODE;
      errbuf  := substr(SQLERRM, 1, 2000);
      fnd_file.put_line(fnd_file.log, 'Error in ' || l_procedure_name || l_sec);
      fnd_file.put_line(fnd_file.log, SQLERRM || '-' || SQLCODE);
      fnd_file.put_line(fnd_file.output, SQLERRM || '-' || SQLCODE);
  END xxcusoie_bullettrain_conv;

  /*******************************************************************************
  * Procedure:   
  * Description: Add delegates from R11 to R12. 
  *              
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     02/13/2012    Manny Rodriguez Delegates conversion 
  
  ********************************************************************************/

  PROCEDURE iexp_conv_delegate(errbuf OUT VARCHAR2, retcode OUT NUMBER) IS
  
    --Intialize Variables
    l_err_msg     VARCHAR2(4000);
    l_err_code    NUMBER;
    l_sec         VARCHAR2(4000);
    l_r12_person  NUMBER;
    l_r12_userid  NUMBER;
    l_r12_empid   NUMBER;
    l_r12_empname VARCHAR2(200);
    l_r12_delname VARCHAR2(200);
    l_skip        BOOLEAN DEFAULT FALSE;
    l_dummy       VARCHAR2(1);
  
    l_procedure_name VARCHAR2(100) := 'XXCUSIE_PKG.IEXP_CB_DELEGATE';
    pl_dflt_email    fnd_user.email_address%TYPE := 'manny.rodriguez@hdsupply.com';
  
  BEGIN
    --Version 1.1  start of changes
    -- EXECUTE IMMEDIATE 'TRUNCATE TABLE XXCUS.XXCUSIE_CB_DELEGATE_TBL';
  
    --Insert Crown Bolt employees into table
    l_sec := 'Loading existing delegates; ';
    dbms_output.put_line(l_sec);
  
    FOR sel_delegate IN (SELECT *
                           FROM ak.ak_web_user_sec_attr_values@r12_to_fin.hsi.hughessupply.com a
                          WHERE web_user_id !=
                                (SELECT user_id
                                   FROM apps.fnd_user@r12_to_fin.hsi.hughessupply.com
                                  WHERE employee_id = a.number_value
                                    AND employee_id IS NOT NULL
                                    AND rownum = 1)
                            AND attribute_code = 'ICX_HR_PERSON_ID'
                            AND attribute_application_id = 178
                            AND (SELECT DISTINCT c.person_type_id
                                   FROM hr.per_all_people_f@r12_to_fin.hsi.hughessupply.com c
                                  WHERE c.person_id = a.number_value
                                    AND c.object_version_number =
                                        (SELECT MAX(object_version_number)
                                           FROM hr.per_all_people_f@r12_to_fin.hsi.hughessupply.com
                                          WHERE person_id = c.person_id)
                                    AND c.person_type_id = 6) = 6)
    LOOP
    
      -- Get person  
      l_sec  := 'Getting person';
      l_skip := FALSE;
      BEGIN
        SELECT person_id12, name12
          INTO l_r12_person, l_r12_empname
          FROM xxcus.xxcus_employee_conv_temp
         WHERE person_id11 = sel_delegate.number_value
           AND rownum = 1; --Joe D is in the conversion table twice;
      EXCEPTION
        WHEN no_data_found THEN
          l_sec := 'Skipping delegate creation for person ID ' ||
                   sel_delegate.number_value ||
                   '.  Could not find a cross reference';
          dbms_output.put_line(l_sec);
          continue;
      END;
    
      -- Get Delegate  
      l_sec := 'Getting delegate';
      BEGIN
        SELECT user_id, name12
          INTO l_r12_userid, l_r12_delname
          FROM xxcus.xxcus_employee_conv_temp
         WHERE user_id_r11 = sel_delegate.web_user_id
           AND rownum = 1;
      EXCEPTION
        WHEN no_data_found THEN
          l_sec := 'Skipping delegate UserID ' || sel_delegate.web_user_id ||
                   '.  Could not find a cross reference';
          dbms_output.put_line(l_sec);
          continue;
      END;
    
      --  look to see if delegates exist for this user
      BEGIN
        SELECT 'x'
          INTO l_dummy
          FROM ak_web_user_sec_attr_values b
         WHERE b.web_user_id = l_r12_userid
           AND b.number_value = l_r12_person
           AND rownum = 1;
        l_skip := TRUE;
        dbms_output.put_line('employee ' || l_r12_empname ||
                             ' exists with delegate ' || l_r12_delname);
      EXCEPTION
        WHEN OTHERS THEN
          l_skip := FALSE;
      END;
    
      IF l_skip = FALSE
      THEN
        dbms_output.put_line('creating delegate association for ' ||
                             l_r12_empname || ' with delegate ' ||
                             l_r12_delname);
        INSERT INTO ak_web_user_sec_attr_values
          (web_user_id --FND_USER_ID of delegate
          ,attribute_code
          ,attribute_application_id
          ,varchar2_value
          ,date_value
          ,number_value --PESON_ID of USER
          ,created_by
          ,creation_date
          ,last_updated_by
          ,last_update_date
          ,last_update_login)
        VALUES
          (l_r12_userid --FND_USER_ID of delegate
          ,sel_delegate.attribute_code
          ,sel_delegate.attribute_application_id
          ,sel_delegate.varchar2_value
          ,sel_delegate.date_value
          ,l_r12_person
          ,0
          ,SYSDATE
          ,0
          ,SYSDATE
          ,0);
      
      END IF;
    
    END LOOP;
  
  END iexp_conv_delegate;

END xxcus_r11_conv_pkg;
/
