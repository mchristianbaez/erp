/* Formatted on 3/12/2015 4:28:38 PM (QP5 v5.254.13202.43190) */
CREATE OR REPLACE PACKAGE BODY APPS.XXWC_QP_EXTRACTS_PKG
AS
   /********************************************************************************************************************************************************************************************
    *
    * HEADER
    *   Price List / Offer Interface to Commerce
    *
    * PROGRAM NAME
    *  XXWC_QP_EXTRACTS_PKG.sql
    *
    * DESCRIPTION
    *  this package contains all of the procedures/functions used to generate price list and offer extract files for Commerce interface
    *
    * LAST UPDATE DATE   10-OCT-2012
    *   Date the program has been modified for the last time
    *
    * HISTORY
    * =======
    *
    * VERSION DATE        AUTHOR(S)       DESCRIPTION
    * ------- ----------- --------------- ------------------------------------
    * 1.00    10-OCT-2012 Scott Spivey    Lucidity Consulting Group - Creation
    * 1.10    21-NOV-2012 Scott Spivey    resolve duplicate offer records when duplicate rows in QP_MODIFIER_SUMMARY_V
    *                                     resolve duplicate offer records when item-specific pricing and category-based pricing
    *                                     resolve duplicate indicator field value when item-specific princing and category-based pricing
    * 1.20    17-DEC-2012 Scott Spivey    add logic to generate additional WhiteCapList price list
    * 1.30    08-Jan-2013 Scott Spivey    alter logic to ignore active_flag on modifier list header
    *                                     modularize price list logic for easier calls
    *                                     performance improvements
    *                                     add tag column from FND_LOOKUP_VALUES to determine whether to create price list extract record
    * 1.40    14-Jan-2013 Scott Spivey    added materialized view and temporary table to improve performance
    * 1.50    27-Jan-2013 Scott Spivey    add call-for-price logic to price lists (already exist for modifiers)
    *                                     add call-for-price logic to list price function
    * 1.60    21-Feb-2013 Scott Spivey    modify call-for-price logic to override newprice logic
    * 1.70    22-May-2013 Andre Rivas     Remove "White Cap Products" price list from "List Price" and "Price" attributes in wc_catalogentries.csv (attributes should no longer be published)
    *                                          20130503-01243 Request for Change to Pricing Interface Extract for eComm
    * 1.80   20-june-2013 Rasikha          The "P_" records created for items listed for the "WhiteCap ProductsPriceList" price list,  20130503-01243 ,see attachment to it
    * 1.90   15-AUG-2013  Ram Talluri     Price list/Modifier header condition commented as it is causing extract incorrect records in DELTA file - TMS #20130813-01179 8/15/2013
    * 2.00   01-DEC-2013  Gopi Damuluri   TMS# 20131121-00230
    *                                     SpeedBuild Changes: CSP Information is now sent for all the Customers who have CSP setup.
    *                                     Added a new procedure - LOAD_SB_CSP_TEMP_TBL
    * 2.01   01/21/2014   Gopi Damuluri   TMS# 20140116-00071
    *                                     Added a new procedure - GET_CONTRACTLIST_PRICE to calculate CSP even if
    *                                     Call For Price flag is set at National Price.
    * 2.02   04/24/2014   Gopi Damuluri   TMS# 20140421-00080
    *                                     Added a new parameter P_END_DATE_ACTIVE to the function GET_CONTRACTLIST_PRICE
    * 2.03   07/01/2014   Gopi Damuluri   TMS# 20140710-00142
    *                                     Resolve missing Pricelists when a Customer has Site Level CSP but no Master level CSP.
    *                                     Also not all Sites have the CSP defined
    * 2.04   22/10/2014  Veera C           TMS#20141002-00037 Canada Multi Org Changes
    * <additional records for modifications>
    *
    * 2.05   03/11/2015   P.vamshidhar    TMS#20150309-00325 - PDH Catalog extracts for Ecom - performance issue
    *                                     Added new input parameter to 'Price_Lists' & 'extract_wrapper' procedures to refresh MV.
    *
    ****************************************************************************************************************************************************************************************************/

   xxwc_error              EXCEPTION;

   -- global error handling variables
   g_err_callfrom          VARCHAR2 (75) := 'XXWC_QP_EXTRACTS_PKG.Price_Lists';
   g_err_callpoint         VARCHAR2 (75) := 'START';
   g_distro_list           VARCHAR2 (75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
   g_message               VARCHAR2 (2000);

   -- global file name variables
   l_directory             VARCHAR2 (80) := 'XXWC_WCS_QP_EXTRACTS_DIR';
   l_plh_outfile           UTL_FILE.file_type;
   l_plh_file_name         VARCHAR2 (80) := 'wc_pricelist.csv';
   l_plh_tmp_file_name     VARCHAR2 (80) := 'tmp_wc_pricelist.csv';
   l_pll_outfile           UTL_FILE.file_type;
   l_pll_file_name         VARCHAR2 (80) := 'wc_offer.csv';
   l_pll_tmp_file_name     VARCHAR2 (80) := 'tmp_wc_offer.csv';

   -- global table for list prices
   TYPE l_list_price_tbl IS TABLE OF NUMBER
      INDEX BY BINARY_INTEGER;

   l_price_lists           l_list_price_tbl;

   l_cnt                   NUMBER := 0;

   -- this controls which lookup type's values are used to determine list price for an item
   g_master_price_list     VARCHAR2 (80) := 'XXWC_NATIONAL_PRICING';
   g_sb_req_line_type_id   NUMBER := 1161;                    -- Version# 2.00

   /*
    This procedure is called by UC4 to execute the extract operation.;
    Parameters:
    errbuf: Error message or result message
    retcode: Success / failure code, 0 = Success, 1 = Warning, 2 = Error
    p_extract_method: DELTA or FULL
    p_mv_refresh   : Y or N
    -------------------------------------------------------------------------------------------------------------------------
    * VERSION DATE        AUTHOR(S)       DESCRIPTION
    * ------- ----------- --------------- ------------------------------------
    * 2.05    03/11/2015   P.vamshidhar    TMS#20150309-00325 - PDH Catalog extracts for Ecom - performance issue
    *                                     Added new parameter to 'extract_wrapper' procedure to refresh MV.
    * ------------------------------------------------------------------------------------------------------------------------
     */

   PROCEDURE extract_wrapper (errbuf                   OUT VARCHAR2,
                              retcode                  OUT VARCHAR2,
                              p_extract_method      IN     VARCHAR2,
                              p_last_extract_date   IN     VARCHAR2,
                              p_mv_refresh          IN     VARCHAR2) -- Added in TMS#20150309-00325 by Vamshi
   AS
      l_request_id   NUMBER;
      l_phase        VARCHAR2 (80);
      l_status       VARCHAR2 (80);
      l_dev_phase    VARCHAR2 (80);
      l_dev_status   VARCHAR2 (80);
      l_message      VARCHAR2 (4000);

      l_user_id      NUMBER;
      l_resp_id      NUMBER;
      l_appl_id      NUMBER := 661;                        -- Advanced Pricing
      --l_ou_id        NUMBER := 162;   --TMS#20141002-00037 Commented by Veera                                                          -- HDS White Cap - Org
      l_ou_id        NUMBER;               --TMS#20141002-00037 Added by Veera
   BEGIN
      errbuf := 'Success';
      retcode := '0';

      IF NVL (p_extract_method, 'DELTA') NOT IN ('FULL', 'DELTA')
      THEN
         DBMS_OUTPUT.put_line (
            'P_EXTRACT_METHOD has an invalid value : ' || p_extract_method);
         RAISE xxwc_error;
      ELSE
         DBMS_OUTPUT.put_line ('P_EXTRACT_METHOD = ' || p_extract_method);
         DBMS_OUTPUT.put_line (
            'P_LAST_EXTRACT_DATE = ' || p_last_extract_date);
      END IF;

      BEGIN
         SELECT user_id
           INTO l_user_id
           FROM fnd_user
          WHERE user_name = 'XXWC_INT_SALESFULFILLMENT';
      EXCEPTION
         WHEN OTHERS
         THEN
            l_user_id := 3;
      END;

      BEGIN
         SELECT responsibility_id
           INTO l_resp_id
           FROM fnd_responsibility_vl
          WHERE     responsibility_key = 'XXWC_PRICING_MANAGER'
                AND application_id = l_appl_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_resp_id := 50891;         -- XXWC_PRICING_MANAGER responsibility
      END;

      /* BEGIN
           SELECT organization_id
             INTO l_ou_id
             FROM hr_operating_units
            WHERE short_code = 'HDSWCORG';
       EXCEPTION
           WHEN OTHERS
           THEN
               l_ou_id := 162;                                                                   -- HDS White Cap - Org
       END;*/
      --TMS#20141002-00037 Commented by Veera
      l_ou_id := mo_global.get_current_org_id; --TMS#20141002-00037 Added by Veera
      -- set Oracle Applications environment
      apps.fnd_global.apps_initialize (user_id        => l_user_id,
                                       resp_id        => l_resp_id,
                                       resp_appl_id   => l_appl_id);

      -- 161 = HDS White Cap operating unit
      apps.mo_global.set_policy_context ('S', l_ou_id);
      apps.mo_global.init ('QP');

      -- don't show concurrent request in user's queue unless status=WARNING or ERROR
      --  l_success := FND_REQUEST.Set_Options(implicit  => 'WARNING');
      --  FND_REQUEST.Set_Org_Id(org_id IN number default NULL);

      l_request_id :=
         fnd_request.submit_request (
            application   => 'XXWC',
            program       => 'XXWC_QP_PRICE_EXTRACT',
            description   => NULL,
            start_time    => NULL,
            sub_request   => FALSE,
            argument1     => NVL (p_extract_method, 'DELTA'),
            argument2     => p_last_extract_date,
            argument3     => NVL (p_mv_refresh, 'Y')); -- Added in TMS#20150309-00325 by Vamshi

      COMMIT;

      IF NVL (l_request_id, 0) = 0
      THEN
         -- submission failed
         DBMS_OUTPUT.put_line ('Unable to submit concurrent program');
         RAISE xxwc_error;
      ELSE
         DBMS_OUTPUT.put_line ('Concurrent Request = ' || l_request_id);
      END IF;

      IF fnd_concurrent.wait_for_request (request_id   => l_request_id,
                                          interval     => 60,
                                          max_wait     => 0,
                                          phase        => l_phase,
                                          status       => l_status,
                                          dev_phase    => l_dev_phase,
                                          dev_status   => l_dev_status,
                                          MESSAGE      => l_message)
      THEN
         DBMS_OUTPUT.put_line (
            'Phase = ' || l_phase || ' and status = ' || l_status);
         DBMS_OUTPUT.put_line ('Request Completion Message = ' || l_message);

         -- return completion information
         IF l_dev_phase = 'COMPLETE'
         THEN
            CASE l_dev_status
               WHEN 'NORMAL'
               THEN
                  errbuf := l_status;
                  retcode := '0';
               WHEN 'ERROR'
               THEN
                  errbuf := l_status;
                  retcode := '2';
               WHEN 'WARNING'
               THEN
                  errbuf := l_status;
                  retcode := '1';
               ELSE
                  errbuf := l_status;
                  retcode := '2';
            END CASE;
         ELSE
            DBMS_OUTPUT.put_line (
               'Request Has Not Completed prior to wait process max_wait');
            DBMS_OUTPUT.put_line (
                  'DEV Phase = '
               || l_dev_phase
               || ' and DEV status = '
               || l_dev_status);
            errbuf := l_status;
            retcode := '1';
         END IF;
      ELSE
         DBMS_OUTPUT.put_line (
            'Unable to determine completion status of concurrent program');
         errbuf := 'Unable to determine completion status';
         retcode := '2';
      END IF;
   EXCEPTION
      WHEN xxwc_error
      THEN
         errbuf := 'Error';
         retcode := '2';
      WHEN OTHERS
      THEN
         errbuf := 'Error';
         retcode := '2';
         DBMS_OUTPUT.put_line (SQLERRM);
   END extract_wrapper;

   /**************************************************************************
    *
    * FUNCTION
    *  Get_List_Price
    *
    * DESCRIPTION
    *  private function to retrieve the list price for an inventory item
    *
    * PARAMETERS
    * ==========
    * NAME              TYPE     DESCRIPTION
   .* ----------------- -------- ---------------------------------------------
    * P_ITEM_ID         NUMBER   inventory item id
    *
    * RETURN VALUE
    *  NUMBER - list price
    *
    * CALLED BY
    *  Process_Modifiers procedure before computing unit price
    *
    *************************************************************************/
   FUNCTION get_list_price (p_item_id IN NUMBER)
      RETURN NUMBER
   IS
      CURSOR c_list_price
      IS
           SELECT DECODE (
                     NVL (qll.attribute1, 'N'),
                     'Y', -1,
                     DECODE (qll.arithmetic_operator,
                             'UNIT_PRICE', qll.operand))
                     unit_price            -- 27-Jan-2013  ver 1.5 enhancement
             FROM qp_secu_list_headers_v qlh,
                  fnd_lookup_values flv,
                  qp_list_lines_v qll
            WHERE     flv.lookup_type = g_master_price_list
                  AND flv.view_application_id = 661
                  AND flv.enabled_flag = 'Y'
                  AND TRUNC (SYSDATE) BETWEEN NVL (flv.start_date_active,
                                                   SYSDATE - 1)
                                          AND NVL (flv.end_date_active,
                                                   SYSDATE + 1)
                  AND flv.meaning = qlh.name
                  AND qlh.list_type_code = 'PRL'
                  AND qlh.active_flag = 'Y'
                  AND TRUNC (SYSDATE) BETWEEN NVL (qlh.start_date_active,
                                                   SYSDATE - 1)
                                          AND NVL (qlh.end_date_active,
                                                   SYSDATE + 1)
                  AND qll.list_header_id = qlh.list_header_id
                  AND qll.product_attribute = 'PRICING_ATTRIBUTE1'     -- item
                  AND qll.product_attr_value = TO_CHAR (p_item_id)
                  AND qll.list_line_type_code = 'PLL'
                  AND TRUNC (SYSDATE) BETWEEN NVL (qll.start_date_active,
                                                   SYSDATE - 1)
                                          AND NVL (qll.end_date_active,
                                                   SYSDATE + 1)
         ORDER BY qlh.name;

      l_list_price   NUMBER := 0;
   BEGIN
      -- setup array of list prices for each item
      IF l_price_lists.EXISTS (p_item_id)
      THEN
         RETURN l_price_lists (p_item_id);
      END IF;

      OPEN c_list_price;

      FETCH c_list_price INTO l_list_price;

      IF c_list_price%NOTFOUND
      THEN
         l_list_price := -99;                    -- flag as 'call for pricing'
      END IF;

      CLOSE c_list_price;

      l_price_lists (p_item_id) := l_list_price;
      RETURN l_list_price;
   END get_list_price;

   /**************************************************************************
    *
    * PROCEDURE
    *  Process_Modifiers
    *
    * DESCRIPTION
    *  private procedure generate data for pricing modifiers
    *
    * PARAMETERS
    * ==========
    * NAME                 TYPE      DESCRIPTION
   .* -----------------    --------  ---------------------------------------------
    * P_MODIFIER_TYPE      VARCHAR2  name of price modifier set
    * P_EXTRACT_METHOD     VARCHAR2  type of extract : FULL or DELTA
    * p_EXTRACT_DATE       DATE      last extract date
    *
    *
    * CALLED BY      Price Lists procedure
    *
    *************************************************************************/
   PROCEDURE process_modifiers (p_modifier_type    IN VARCHAR2,
                                p_extract_method   IN VARCHAR2,
                                p_extract_date     IN DATE)
   IS
      -- cursor to handle modifiers
      CURSOR c_modifiers
      IS
         SELECT qlh.name price_list_name,
                NVL (flv.description, flv.meaning) ecomm_price_list_name,
                qlh.currency_code,
                msi.item_number,
                DECODE (INSTR (msi.description, '"'),
                        0, msi.description,
                        '"' || REPLACE (msi.description, '"', '""') || '"')
                   item_description,
                msi.inventory_item_id,
                qms.list_line_id || '' list_line_id,
                qms.arithmetic_operator,
                qms.operand,
                TO_CHAR (
                   DECODE (
                      qms.start_date_active,
                      NULL, qlh.start_date_active,
                      DECODE (
                         qlh.start_date_active,
                         NULL, qms.start_date_active,
                         GREATEST (qms.start_date_active,
                                   qlh.start_date_active))),
                   'YYYY-MM-DD HH24:MI:SS')
                   start_date_active,
                TO_CHAR (
                   DECODE (
                      qms.end_date_active,
                      NULL, qlh.end_date_active,
                      DECODE (
                         qlh.end_date_active,
                         NULL, qms.end_date_active,
                         LEAST (qms.end_date_active, qlh.end_date_active))),
                   'YYYY-MM-DD HH24:MI:SS')
                   end_date_active,
                qms.product_precedence,
                qms.price_break_type_code,                            -- POINT
                qms.comparison_operator_code,                       -- BETWEEN
                qms.pricing_attr_value_from,            -- price break qty low
                qms.pricing_attr_value_to,             -- price break qty high
                NVL (qms.attribute1, 'N') call_for_price,
                DECODE (flv.enabled_flag, 'Y', '0', '1') active_flag,
                flv.tag     -- determine whether to generate price list record
                       ,
                qms.price_by_formula_id /*  TMS: 20131013-00045 Multi Decimal Rounding, Toby Rave, added to query modifier line operand during GCP call */
           FROM qp_secu_list_headers_vl qlh,
                fnd_lookup_values flv,
                qp_modifier_summary_v qms,
                xxwc_qp_ecommerce_items_mv msi
          WHERE     flv.lookup_type = p_modifier_type
                AND flv.view_application_id = 661
                AND flv.enabled_flag = 'Y'
                AND TRUNC (SYSDATE) BETWEEN NVL (flv.start_date_active,
                                                 SYSDATE - 1)
                                        AND NVL (flv.end_date_active,
                                                 SYSDATE + 1)
                AND qlh.list_type_code = 'DLT'
                AND qlh.name = flv.meaning
                AND TRUNC (SYSDATE) BETWEEN NVL (qlh.start_date_active,
                                                 SYSDATE - 1)
                                        AND NVL (qlh.end_date_active,
                                                 SYSDATE + 1)
                AND qms.list_header_id = qlh.list_header_id
                AND qms.product_attr = 'PRICING_ATTRIBUTE1' -- 1=item  2=category 3=all
                AND TO_NUMBER (qms.product_attr_val) = msi.inventory_item_id
                AND qms.excluder_flag = 'N'                   -- Version# 2.01
                AND qms.list_line_type_code = 'DIS'
                AND qms.modifier_level_code = 'LINE'
                AND (   (    p_extract_method = 'FULL'
                         AND flv.enabled_flag = 'Y'
                         AND TRUNC (SYSDATE) BETWEEN NVL (
                                                        qms.start_date_active,
                                                        SYSDATE - 1)
                                                 AND NVL (
                                                        qms.end_date_active,
                                                        SYSDATE + 1))
                     OR (    p_extract_method = 'DELTA'
                         AND (   qms.last_update_date >= p_extract_date
                              OR -- handle scenario where modifer line created/updated
                                msi.last_update_date >= p_extract_date -- handle scenario where item flagged as website item
                              -- OR
                              --   qlh.last_update_date >= p_extract_date-- handle scenario where price list header updated
                              -- --Commented by Ram Talluri per TMS #20130813-01179 8/15/2013
                              OR flv.last_update_date >= p_extract_date
                              OR -- handle scenario where modifier added to lookup type
                                EXISTS
                                    (SELECT 'x' -- check for updated list price that would affect modifier
                                       FROM xxwc.xxwc_qp_unit_price_temp qp
                                      WHERE     msi.inventory_item_id =
                                                   qp.inventory_item_id
                                            AND qms.arithmetic_operator !=
                                                   'NEWPRICE'))))
         UNION
         -- retrieve items by categories
         SELECT qlh.name price_list_name,
                NVL (flv.description, flv.meaning) ecomm_price_list_name,
                qlh.currency_code,
                msi.item_number,
                DECODE (INSTR (msi.description, '"'),
                        0, msi.description,
                        '"' || REPLACE (msi.description, '"', '""') || '"')
                   item_description,
                msi.inventory_item_id,
                mic.category_id || msi.inventory_item_id list_line_id --               ,NVL (qll.product_uom_code, msi.primary_uom_code) uom_code
                                                                     ,
                qll.arithmetic_operator,
                qll.operand,
                TO_CHAR (
                   DECODE (
                      qll.start_date_active,
                      NULL, qlh.start_date_active,
                      DECODE (
                         qlh.start_date_active,
                         NULL, qll.start_date_active,
                         GREATEST (qll.start_date_active,
                                   qlh.start_date_active))),
                   'YYYY-MM-DD HH24:MI:SS')
                   start_date_active,
                TO_CHAR (
                   DECODE (
                      qll.end_date_active,
                      NULL, qlh.end_date_active,
                      DECODE (
                         qlh.end_date_active,
                         NULL, qll.end_date_active,
                         LEAST (qll.end_date_active, qlh.end_date_active))),
                   'YYYY-MM-DD HH24:MI:SS')
                   end_date_active,
                qll.product_precedence,
                qll.price_break_type_code,                            -- POINT
                qll.comparison_operator_code,                       -- BETWEEN
                qll.pricing_attr_value_from,            -- price break qty low
                qll.pricing_attr_value_to,             -- price break qty high
                NVL (qll.attribute1, 'N') call_for_price,
                DECODE (flv.enabled_flag, 'Y', '0', '1') active_flag,
                flv.tag     -- determine whether to generate price list record
                       ,
                qll.price_by_formula_id /*  TMS: 20131013-00045 Multi Decimal Rounding, Toby Rave, added to query modifier line operand during GCP call */
           FROM qp_secu_list_headers_vl qlh,
                fnd_lookup_values flv,
                qp_modifier_summary_v qll,
                mtl_item_categories mic,
                xxwc_qp_ecommerce_items_mv msi
          WHERE     flv.lookup_type = p_modifier_type
                AND flv.enabled_flag = 'Y'
                AND flv.view_application_id = 661
                AND TRUNC (SYSDATE) BETWEEN NVL (flv.start_date_active,
                                                 SYSDATE - 1)
                                        AND NVL (flv.end_date_active,
                                                 SYSDATE + 1)
                AND flv.meaning = qlh.name
                AND qlh.list_type_code = 'DLT'
                AND TRUNC (SYSDATE) BETWEEN NVL (qlh.start_date_active,
                                                 SYSDATE - 1)
                                        AND NVL (qlh.end_date_active,
                                                 SYSDATE + 1)
                AND qll.list_header_id = qlh.list_header_id
                AND qll.product_attr = 'PRICING_ATTRIBUTE2' -- 1=item  2=category 3=all
                AND qll.excluder_flag = 'N'                   -- Version# 2.01
                AND NOT EXISTS
                           (SELECT '1'
                              FROM qp_pricing_attributes qpa
                             WHERE     qpa.list_line_id = qll.list_line_id
                                   AND qpa.PRODUCT_ATTRIBUTE =
                                          'PRICING_ATTRIBUTE1'
                                   AND qpa.PRODUCT_ATTR_VALUE =
                                          TO_CHAR (msi.inventory_item_id)
                                   AND qpa.EXCLUDER_FLAG = 'Y') -- -- Version# 2.01
                AND TO_CHAR (mic.category_id) = qll.product_attr_val
                AND mic.inventory_item_id = msi.inventory_item_id
                AND mic.organization_id = msi.organization_id
                AND qll.list_line_type_code = 'DIS'
                AND qll.modifier_level_code = 'LINE'
                -- exclude when item has SKU-specific pricing
                AND NOT EXISTS
                           (SELECT 'x'
                              FROM qp_modifier_summary_v qms
                             WHERE     qms.list_header_id =
                                          qll.list_header_id
                                   AND qms.product_attr =
                                          'PRICING_ATTRIBUTE1'         -- item
                                   AND TO_CHAR (msi.inventory_item_id) =
                                          qms.product_attr_val
                                   AND qms.modifier_level_code = 'LINE'
                                   AND TRUNC (SYSDATE) BETWEEN NVL (
                                                                  qms.start_date_active,
                                                                  SYSDATE - 1)
                                                           AND NVL (
                                                                  qms.end_date_active,
                                                                  SYSDATE + 1))
                AND (   (    p_extract_method = 'FULL'
                         --                         AND qlh.active_flag = 'Y'
                         AND TRUNC (SYSDATE) BETWEEN NVL (
                                                        qll.start_date_active,
                                                        SYSDATE - 1)
                                                 AND NVL (
                                                        qll.end_date_active,
                                                        SYSDATE + 1))
                     OR (    p_extract_method = 'DELTA'
                         AND (   qll.last_update_date >= p_extract_date
                              OR -- handle scenario where modifer line created/updated
                                msi.last_update_date >= p_extract_date -- handle scenario where item flagged as website item
                              -- OR
                              --   qlh.last_update_date >= p_extract_date-- handle scenario where price list header updated
                              ----Commented by Ram Talluri per TMS #20130813-01179 8/15/2013
                              OR flv.last_update_date >= p_extract_date
                              OR -- handle scenario where modifier added to lookup type
                                EXISTS
                                    (SELECT 'x' -- check for updated list price that would affect modifier
                                       FROM xxwc.xxwc_qp_unit_price_temp qp
                                      WHERE     msi.inventory_item_id =
                                                   qp.inventory_item_id
                                            AND qll.arithmetic_operator !=
                                                   'NEWPRICE'))))
         UNION                                     -- -- Version# 2.00 > Start
         SELECT qlh.name price_list_name --                 , NVL (flv.description, flv.meaning) ecomm_price_list_name
                                        ,
                qlh.name ecomm_price_list_name,
                qlh.currency_code,
                msi.item_number,
                DECODE (INSTR (msi.description, '"'),
                        0, msi.description,
                        '"' || REPLACE (msi.description, '"', '""') || '"')
                   item_description,
                msi.inventory_item_id,
                qll.list_line_id || '' list_line_id,
                qll.arithmetic_operator,
                qll.operand,
                TO_CHAR (
                   DECODE (
                      qll.start_date_active,
                      NULL, qlh.start_date_active,
                      DECODE (
                         qlh.start_date_active,
                         NULL, qll.start_date_active,
                         GREATEST (qll.start_date_active,
                                   qlh.start_date_active))),
                   'YYYY-MM-DD HH24:MI:SS')
                   start_date_active,
                TO_CHAR (
                   DECODE (
                      qll.end_date_active,
                      NULL, qlh.end_date_active,
                      DECODE (
                         qlh.end_date_active,
                         NULL, qll.end_date_active,
                         LEAST (qll.end_date_active, qlh.end_date_active))),
                   'YYYY-MM-DD HH24:MI:SS')
                   end_date_active,
                qll.product_precedence,
                qll.price_break_type_code,                            -- POINT
                qll.comparison_operator_code,                       -- BETWEEN
                qll.pricing_attr_value_from,            -- price break qty low
                qll.pricing_attr_value_to,             -- price break qty high
                NVL (qll.attribute1, 'N') call_for_price,
                '0' active_flag,
                NULL tag    -- determine whether to generate price list record
                        --                 , DECODE (flv.enabled_flag, 'Y', '0', '1') active_flag
                        --                 , flv.tag                                            -- determine whether to generate price list record
                ,
                qll.price_by_formula_id /*  TMS: 20131013-00045 Multi Decimal Rounding, Toby Rave, added to query modifier line operand during GCP call */
           FROM qp_secu_list_headers_vl qlh,
                qp_modifier_summary_v qll,
                apps.xxwc_sb_qp_temp_tbl stg,
                xxwc_qp_ecommerce_items_mv msi
          WHERE     1 = 1
                AND p_modifier_type = 'XXWC_CONTRACT_PRICING'
                AND qlh.attribute10 = 'Contract Pricing'
                AND qll.list_header_id = stg.list_header_id
                AND qlh.list_header_id = stg.list_header_id
                AND qlh.list_type_code = 'DLT'
                AND qll.list_line_type_code = 'DIS'
                AND qll.modifier_level_code = 'LINE'
                AND qll.product_attr = 'PRICING_ATTRIBUTE1' -- 1=item  2=category 3=all
                AND qll.excluder_flag = 'N'                -- -- Version# 2.01
                AND TO_NUMBER (qll.product_attr_val) = msi.inventory_item_id
                AND qlh.active_flag = 'Y'
                AND TRUNC (SYSDATE) BETWEEN NVL (qlh.start_date_active,
                                                 SYSDATE - 1)
                                        AND NVL (qlh.end_date_active,
                                                 SYSDATE + 1)
                --               AND TRUNC (SYSDATE) BETWEEN NVL (qll.start_date_active, SYSDATE - 1)
                --                                           AND NVL (qll.end_date_active, SYSDATE + 1)
                AND EXISTS
                       (SELECT '1'
                          FROM apps.xxwc_om_contract_pricing_hdr xx_csp
                         WHERE     1 = 1
                               AND TO_CHAR (xx_csp.agreement_id) =
                                      qlh.attribute14
                               AND AGREEMENT_STATUS = 'APPROVED')
         UNION
         SELECT qlh.name price_list_name --                 , NVL (flv.description, flv.meaning) ecomm_price_list_name
                                        ,
                qlh.name ecomm_price_list_name,
                qlh.currency_code,
                msi.item_number,
                DECODE (INSTR (msi.description, '"'),
                        0, msi.description,
                        '"' || REPLACE (msi.description, '"', '""') || '"')
                   item_description,
                msi.inventory_item_id,
                mic.category_id || msi.inventory_item_id list_line_id,
                qll.arithmetic_operator,
                qll.operand,
                TO_CHAR (
                   DECODE (
                      qll.start_date_active,
                      NULL, qlh.start_date_active,
                      DECODE (
                         qlh.start_date_active,
                         NULL, qll.start_date_active,
                         GREATEST (qll.start_date_active,
                                   qlh.start_date_active))),
                   'YYYY-MM-DD HH24:MI:SS')
                   start_date_active,
                TO_CHAR (
                   DECODE (
                      qll.end_date_active,
                      NULL, qlh.end_date_active,
                      DECODE (
                         qlh.end_date_active,
                         NULL, qll.end_date_active,
                         LEAST (qll.end_date_active, qlh.end_date_active))),
                   'YYYY-MM-DD HH24:MI:SS')
                   end_date_active,
                qll.product_precedence,
                qll.price_break_type_code,                            -- POINT
                qll.comparison_operator_code,                       -- BETWEEN
                qll.pricing_attr_value_from,            -- price break qty low
                qll.pricing_attr_value_to,             -- price break qty high
                NVL (qll.attribute1, 'N') call_for_price,
                '0' active_flag,
                NULL tag    -- determine whether to generate price list record
                        --                 , DECODE (flv.enabled_flag, 'Y', '0', '1') active_flag
                        --                 , flv.tag                                            -- determine whether to generate price list record
                ,
                qll.price_by_formula_id /*  TMS: 20131013-00045 Multi Decimal Rounding, Toby Rave, added to query modifier line operand during GCP call */
           FROM qp_secu_list_headers_vl qlh,
                qp_modifier_summary_v qll,
                apps.xxwc_sb_qp_temp_tbl stg,
                xxwc_qp_ecommerce_items_mv msi,
                mtl_item_categories mic
          WHERE     1 = 1
                AND p_modifier_type = 'XXWC_CONTRACT_PRICING'
                AND qlh.attribute10 = 'Contract Pricing'
                AND qll.list_header_id = stg.list_header_id
                AND qlh.list_header_id = stg.list_header_id
                AND qlh.list_type_code = 'DLT'
                AND qll.list_line_type_code = 'DIS'
                AND qll.modifier_level_code = 'LINE'
                AND qll.product_attr = 'PRICING_ATTRIBUTE2' -- 1=item  2=category 3=all
                AND qll.excluder_flag = 'N'                   -- Version# 2.01
                AND NOT EXISTS
                           (SELECT '1'
                              FROM qp_pricing_attributes qpa
                             WHERE     qpa.list_line_id = qll.list_line_id
                                   AND qpa.PRODUCT_ATTRIBUTE =
                                          'PRICING_ATTRIBUTE1'
                                   AND qpa.PRODUCT_ATTR_VALUE =
                                          TO_CHAR (msi.inventory_item_id)
                                   AND qpa.EXCLUDER_FLAG = 'Y') -- Version# 2.01
                AND TO_CHAR (mic.category_id) = qll.product_attr_val
                AND mic.inventory_item_id = msi.inventory_item_id
                AND mic.organization_id = msi.organization_id
                AND qlh.active_flag = 'Y'
                AND TRUNC (SYSDATE) BETWEEN NVL (qlh.start_date_active,
                                                 SYSDATE - 1)
                                        AND NVL (qlh.end_date_active,
                                                 SYSDATE + 1)
                --               AND TRUNC (SYSDATE) BETWEEN NVL (qll.start_date_active, SYSDATE - 1)
                --                                           AND NVL (qll.end_date_active, SYSDATE + 1)
                AND EXISTS
                       (SELECT '1'
                          FROM apps.xxwc_om_contract_pricing_hdr xx_csp
                         WHERE     1 = 1
                               AND TO_CHAR (xx_csp.agreement_id) =
                                      qlh.attribute14
                               AND agreement_status = 'APPROVED')
                AND NOT EXISTS
                           (SELECT 'x'
                              FROM qp_modifier_summary_v qms
                             WHERE     qms.list_header_id =
                                          qll.list_header_id
                                   AND qms.product_attr =
                                          'PRICING_ATTRIBUTE1'         -- item
                                   AND TO_CHAR (msi.inventory_item_id) =
                                          qms.product_attr_val
                                   AND qms.modifier_level_code = 'LINE'
                                   AND TRUNC (SYSDATE) BETWEEN NVL (
                                                                  qms.start_date_active,
                                                                  SYSDATE - 1)
                                                           AND NVL (
                                                                  qms.end_date_active,
                                                                  SYSDATE + 1)
                                   AND EXISTS
                                          (SELECT '1'
                                             FROM apps.xxwc_om_contract_pricing_hdr xx_csp
                                            WHERE     1 = 1
                                                  AND TO_CHAR (
                                                         xx_csp.agreement_id) =
                                                         qms.attribute14
                                                  AND AGREEMENT_STATUS =
                                                         'APPROVED'))
         ORDER BY 2, 4;

      -- Version# 2.02 > Start
      CURSOR cur_catalog_pl
      IS
         SELECT DISTINCT
                hp.party_name || '-' || hca.account_number
                   ecomm_price_list_name
           FROM apps.xxwc_sb_qp_temp_tbl stg,
                apps.hz_cust_accounts hca,
                hz_parties hp
          WHERE     1 = 1
                AND p_modifier_type = 'XXWC_CONTRACT_PRICING'
                AND stg.customer_id = hca.cust_account_id
                AND hp.party_id = hca.party_id
                AND stg.list_header_id IS NULL;

      -- Version# 2.02 < End

      TYPE rec_type IS TABLE OF c_modifiers%ROWTYPE
         INDEX BY BINARY_INTEGER;

      r1                  rec_type;

      l_last_price_list   VARCHAR2 (240) := 'NONE';
      l_list_price        NUMBER;
      l_unit_price        NUMBER;
      l_csp_cnt           NUMBER;                             -- Version# 2.01
   BEGIN
      -- Load CSP information into Temp table -- Version# 2.00
      load_sb_csp_temp_tbl (p_extract_date);

      OPEN c_modifiers;

      LOOP
         FETCH c_modifiers BULK COLLECT INTO r1 LIMIT 1000;

         FOR i IN 1 .. r1.COUNT
         LOOP
            -- generate price list record for each new price list name
            IF     l_last_price_list != r1 (i).ecomm_price_list_name
               AND r1 (i).tag IS NULL
            THEN
               l_last_price_list := r1 (i).ecomm_price_list_name;
               UTL_FILE.put_line (
                  l_plh_outfile,
                     NULL
                  || '|'
                  ||                                               -- UniqueId
                    r1 (i).ecomm_price_list_name
                  || '|'
                  ||                                                   -- Name
                    r1 (i).ecomm_price_list_name
                  || '|'
                  ||                                            -- Description
                    NULL
                  || '|'
                  ||                                             -- Precedence
                    'C'
                  || '|'
                  ||                                                   -- Type
                     --                 r1.active_flag                                      -- Delete
                     0                         -- Delete  0=active  1=inactive
                      );
            END IF;

            -- Version# 2.01 > Start
            l_csp_cnt := 0;

            BEGIN
               SELECT COUNT (1)
                 INTO l_csp_cnt
                 FROM fnd_lookup_values flv
                WHERE     flv.lookup_type IN ('XXWC_CONTRACT_PRICING')
                      AND flv.meaning = r1 (i).ecomm_price_list_name;
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_csp_cnt := 0;
            END;

            -- Version# 2.01 < End

            IF p_modifier_type != 'XXWC_CONTRACT_PRICING' OR l_csp_cnt > 0
            THEN
               l_list_price := get_list_price (r1 (i).inventory_item_id);

               IF l_list_price > 0 -- negative list price indicates call-for-price flag
               THEN
                  CASE                            --r1 (i).arithmetic_operator
                     /*  TMS: 20131013-00045 Multi Decimal Rounding, Toby Rave, added to query modifier line operand during GCP call */
                     WHEN     r1 (i).arithmetic_operator = 'NEWPRICE'
                          AND r1 (i).price_by_formula_id IS NULL
                     THEN
                        l_unit_price := NVL (r1 (i).operand, -99); -- flag as call-for-price if unit price is null
                     WHEN r1 (i).arithmetic_operator = 'AMT'
                     THEN
                        IF r1 (i).operand > 0      -- ignore negative discount
                        THEN
                           l_unit_price := l_list_price - r1 (i).operand;
                        ELSE
                           l_unit_price := l_list_price;
                        END IF;
                     /*  TMS: 20131013-00045 Multi Decimal Rounding, Toby Rave, added to query modifier line operand during GCP call */
                     WHEN     r1 (i).arithmetic_operator = 'NEWPRICE'
                          AND r1 (i).price_by_formula_id = 52060
                     THEN
                        IF NVL (r1 (i).operand, 0) > 0 -- ignore negative discount
                        THEN
                           l_unit_price := l_list_price * r1 (i).operand;
                        ELSE
                           l_unit_price := l_list_price;
                        END IF;
                     ELSE
                        l_unit_price :=
                           get_list_price (r1 (i).inventory_item_id);
                  END CASE;


                  -- ensure that modified price below zero becomes call-for-price
                  IF l_unit_price < 0
                  THEN
                     l_unit_price := -1;
                  END IF;
               ELSE                                          -- call for price
                  l_unit_price := -1;
               END IF;
            -- Version# 2.01 > Start
            ELSIF p_modifier_type = 'XXWC_CONTRACT_PRICING' AND l_csp_cnt = 0
            THEN
               -- l_unit_price := get_contractlist_price(r1(i).item_number, r1(i).ecomm_price_list_name ); -- Version# 2.02
               l_unit_price :=
                  get_contractlist_price (r1 (i).item_number,
                                          r1 (i).ecomm_price_list_name,
                                          r1 (i).end_date_active); -- Version# 2.02
            ELSE
               l_unit_price := -1;
            END IF;

            -- Version# 2.01 < End

            -- end 27-Jan-2013  ver 1.5 enhancement

            UTL_FILE.put_line (
               l_pll_outfile,
                  NULL
               || '|'
               ||                                         -- PriceListUniqueId
                 r1 (i).ecomm_price_list_name
               || '|'
               ||                                             -- PriceListName
                 NULL
               || '|'
               ||                                          -- CatentryUniqueId
                 r1 (i).item_number
               || '|'
               ||                                        -- CatentryPartNumber
                 r1 (i).list_line_id
               || '|'
               ||                                                -- Identifier
                 r1 (i).product_precedence
               || '|'
               ||                                                -- Precedence
                 r1 (i).start_date_active
               || '|'
               ||                                                 -- StartDate
                 r1 (i).end_date_active
               || '|'
               ||                                                   -- EndDate
                  --                                     R1.uom_code||'|'||                         -- QuantityUnitIdentifier
                  'C62'
               || '|'
               ||                                    -- QuantityUnitIdentifier
                 r1 (i).pricing_attr_value_from
               || '|'
               ||                                           -- MinimumQuantity
                 r1 (i).pricing_attr_value_to
               || '|'
               ||                                           -- MaximumQuantity
                 r1 (i).item_description
               || '|'
               ||                                               -- Description
                 ROUND (l_unit_price, 2)
               || '|'
               ||                                                     -- Price
                 r1 (i).currency_code
               || '|'
               ||                                                  -- Currency
                  --              r1.active_flag              -- Delete  0=do not delete  1=delete
                  0                       -- Delete  0=do not delete  1=delete
                   );

            l_cnt := l_cnt + 1;
         END LOOP;

         EXIT WHEN r1.COUNT = 0;
      END LOOP;

      CLOSE c_modifiers;

      -- Version# 2.02 > Start
      FOR rec_catalog_pl IN cur_catalog_pl
      LOOP
         UTL_FILE.put_line (
            l_plh_outfile,
               NULL                                                -- UniqueId
            || '|'
            || rec_catalog_pl.ecomm_price_list_name                    -- Name
            || '|'
            || rec_catalog_pl.ecomm_price_list_name             -- Description
            || '|'
            || NULL                                              -- Precedence
            || '|'
            || 'C'                                                     -- Type
            || '|'
            || 0                                -- Delete 0=active  1=inactive
                );
      END LOOP;
   -- Version# 2.02 < End

   END process_modifiers;

   /**************************************************************************
    *
    * PROCEDURE
    *  Process_Modifiers
    *
    * DESCRIPTION
    *  private procedure generate data for pricing modifiers
    *
    * PARAMETERS
    * ==========
    * NAME                 TYPE      DESCRIPTION
   .* -----------------    --------  ---------------------------------------------
    * P_MODIFIER_TYPE      VARCHAR2  name of price modifier set
    * P_EXTRACT_METHOD     VARCHAR2  type of extract : FULL or DELTA
    * p_EXTRACT_DATE       DATE      last extract date
    *
    *
    * CALLED BY
    *  Price Lists procedure
    *
    *************************************************************************/
   PROCEDURE process_price_lists (
      p_modifier_type         IN VARCHAR2,
      p_extract_method        IN VARCHAR2,
      p_extract_date          IN DATE,
      p_product_list_prefix   IN VARCHAR2 DEFAULT NULL)
   IS
      -- cursors to handle price lists
      CURSOR c_price_lists
      IS
           SELECT qlh.name price_list_name,
                  NVL (flv.description, flv.meaning) ecomm_price_list_name,
                  qlh.currency_code,
                  p_product_list_prefix || msi.item_number item_number,
                  DECODE (INSTR (msi.description, '"'),
                          0, msi.description,
                          '"' || REPLACE (msi.description, '"', '""') || '"')
                     item_description,
                  msi.inventory_item_id,
                  msi.organization_id,
                  DECODE (qll.arithmetic_operator, 'UNIT_PRICE', qll.operand)
                     unit_price,
                  TO_CHAR (
                     DECODE (
                        qll.start_date_active,
                        NULL, qlh.start_date_active,
                        DECODE (
                           qlh.start_date_active,
                           NULL, qll.start_date_active,
                           GREATEST (qll.start_date_active,
                                     qlh.start_date_active))),
                     'YYYY-MM-DD HH24:MI:SS')
                     start_date_active,
                  TO_CHAR (
                     DECODE (
                        qll.end_date_active,
                        NULL, qlh.end_date_active,
                        DECODE (
                           qlh.end_date_active,
                           NULL, qll.end_date_active,
                           LEAST (qll.end_date_active, qlh.end_date_active))),
                     'YYYY-MM-DD HH24:MI:SS')
                     end_date_active,
                  qll.product_precedence,
                  qll.list_line_id,
                  NVL (qll.attribute1, 'N') call_for_price,
                  flv.tag   -- determine whether to generate price list record
             FROM qp_secu_list_headers_v qlh,
                  qp_list_lines_v qll,
                  fnd_lookup_values flv,
                  xxwc_qp_ecommerce_items_mv msi
            WHERE     flv.lookup_type = p_modifier_type
                  AND flv.view_application_id = 661
                  AND flv.enabled_flag = 'Y'
                  AND TRUNC (SYSDATE) BETWEEN NVL (flv.start_date_active,
                                                   SYSDATE - 1)
                                          AND NVL (flv.end_date_active,
                                                   SYSDATE + 1)
                  AND flv.meaning = qlh.name
                  AND qlh.list_type_code = 'PRL'
                  AND TRUNC (SYSDATE) BETWEEN NVL (qlh.start_date_active,
                                                   SYSDATE - 1)
                                          AND NVL (qlh.end_date_active,
                                                   SYSDATE + 1)
                  AND qlh.list_header_id = qll.list_header_id
                  AND qll.product_attribute = 'PRICING_ATTRIBUTE1'     -- item
                  AND msi.inventory_item_id =
                         TO_NUMBER (qll.product_attr_value)
                  AND qll.list_line_type_code = 'PLL'
                  AND (   (    p_extract_method = 'FULL'
                           AND qlh.active_flag = 'Y'
                           AND TRUNC (SYSDATE) BETWEEN NVL (
                                                          qll.start_date_active,
                                                          SYSDATE - 1)
                                                   AND NVL (
                                                          qll.end_date_active,
                                                          SYSDATE + 1))
                       OR (    p_extract_method = 'DELTA'
                           AND (   qll.last_update_date >= p_extract_date
                                OR             -- price list line was modified
                                  msi.last_update_date >= p_extract_date -- item added to web item category
                                /* OR
                                   qlh.last_update_date >= p_extract_date  -- price list header was modified
                                 */
                                --Commented by Ram Talluri per TMS #20130813-01179 8/15/2013
                                OR flv.last_update_date >= p_extract_date -- price list header added to extract group
                                                                         )))
         ORDER BY flv.description, msi.item_number;

      TYPE rec_type IS TABLE OF c_price_lists%ROWTYPE
         INDEX BY BINARY_INTEGER;

      r1                  rec_type;

      --      l_unit_price          NUMBER;
      l_last_price_list   VARCHAR2 (240) := 'NONE';
      l_unit_price        NUMBER;
   BEGIN
      OPEN c_price_lists;

      LOOP
         FETCH c_price_lists BULK COLLECT INTO r1 LIMIT 100;

         FOR i IN 1 .. r1.COUNT
         LOOP
            -- generate price list record for each new price list name
            IF     l_last_price_list != r1 (i).ecomm_price_list_name
               AND r1 (i).tag IS NULL -- use this field to determine whether to create price list record
            THEN
               UTL_FILE.put_line (
                  l_plh_outfile,
                     NULL
                  || '|'
                  ||                                               -- UniqueId
                    r1 (i).ecomm_price_list_name
                  || '|'
                  ||                                                   -- Name
                    r1 (i).ecomm_price_list_name
                  || '|'
                  ||                                            -- Description
                    NULL
                  || '|'
                  ||                                             -- Precedence
                    'C'
                  || '|'
                  ||                                                   -- Type
                    0                                                -- Delete
                     );
            END IF;

            l_last_price_list := r1 (i).ecomm_price_list_name;

            -- begin 27-Jan-2013  ver 1.5 enhancement
            -- handle call-for-price scenario
            IF r1 (i).call_for_price = 'Y'
            THEN
               l_unit_price := -1;
            ELSE
               l_unit_price := r1 (i).unit_price;
            END IF;

            -- end 27-Jan-2013  ver 1.5 enhancement

            UTL_FILE.put_line (
               l_pll_outfile,
                  NULL
               || '|'
               ||                                         -- PriceListUniqueId
                 r1 (i).ecomm_price_list_name
               || '|'
               ||                                             -- PriceListName
                 NULL
               || '|'
               ||                                          -- CatentryUniqueId
                 r1 (i).item_number
               || '|'
               ||                                        -- CatentryPartNumber
                 r1 (i).list_line_id
               || '|'
               ||                                                -- Identifier
                 r1 (i).product_precedence
               || '|'
               ||                                                -- Precedence
                 r1 (i).start_date_active
               || '|'
               ||                                                 -- StartDate
                 r1 (i).end_date_active
               || '|'
               ||                                                   -- EndDate
                 'C62'
               || '|'
               ||                                    -- QuantityUnitIdentifier
                 NULL
               || '|'
               ||                                           -- MinimumQuantity
                 NULL
               || '|'
               ||                                           -- MaximumQuantity
                 r1 (i).item_description
               || '|'
               ||                                               -- Description
                 l_unit_price              -- 27-Jan-2013  ver 1.5 enhancement
               || '|'
               ||                                                -- PriceinUSD
                 r1 (i).currency_code
               || '|'
               ||                                                  -- Currency
                 0                        -- Delete  0=do not delete  1=delete
                  );

            -- populate item array with list price for use in modifier logic
            l_price_lists (r1 (i).inventory_item_id) := l_unit_price;

            l_cnt := l_cnt + 1;

            -- generate temp table of FULL/DELTA extract for NATIONAL price list
            -- for use in modifier-based query
            IF p_modifier_type = g_master_price_list
            THEN
               INSERT INTO xxwc.xxwc_qp_unit_price_temp (inventory_item_id,
                                                         organization_id,
                                                         description,
                                                         unit_price)
                    VALUES (r1 (i).inventory_item_id,
                            r1 (i).organization_id,
                            r1 (i).item_description,
                            l_unit_price);
            END IF;
         END LOOP;

         EXIT WHEN r1.COUNT = 0;
      END LOOP;

      CLOSE c_price_lists;
   END process_price_lists;


   /*******************************************************************************************************************/
   /* Price_Lists : main procedure for price list/offer interface to Commerce
   /*
   /* 2.05  03/11/2015   P.vamshidhar    TMS#20150309-00325 - PDH Catalog extracts for Ecom - performance issue
   /*                                    Added new input parameter to 'Price_Lists' procedure to refresh MV.
   /*                                    Added if clause to refresh MV.
   /*
   /*******************************************************************************************************************/

   PROCEDURE price_lists (errbuf                   OUT VARCHAR2,
                          retcode                  OUT VARCHAR2,
                          p_extract_method      IN     VARCHAR2 DEFAULT 'DELTA',
                          p_last_extract_date   IN     VARCHAR2,
                          p_mv_refresh          IN     VARCHAR2 DEFAULT 'Y') -- Added new parameter in TMS#20150309-00325
   IS
      l_last_extract_date   DATE;

      --  check for improper price list names in custom lookup types
      CURSOR c_lookup_values
      IS
         SELECT lookup_type, meaning, description
           FROM fnd_lookup_values flv
          WHERE     lookup_type IN ('XXWC_NATIONAL_PRICING',
                                    'XXWC_WHITECAPLIST',
                                    'XXWC_WHITECAPPRODUCTSPRICELIST')
                AND NOT EXISTS
                           (SELECT 'x'
                              FROM qp_secu_list_headers_v qlh
                             WHERE     flv.meaning = qlh.name
                                   AND qlh.list_type_code = 'PRL')
         UNION
         SELECT lookup_type, meaning, description
           FROM fnd_lookup_values flv
          WHERE     lookup_type IN ('XXWC_CONTRACT_PRICING',
                                    'XXWC_NATIONAL_TRADE_PRICE',
                                    'XXWC_TRADER_PRICE')
                AND NOT EXISTS
                           (SELECT 'x'
                              FROM qp_secu_list_headers_vl qlh
                             WHERE     flv.meaning = qlh.name
                                   AND qlh.list_type_code = 'DLT')
         ORDER BY lookup_type, meaning, description;

      l_extract_days        NUMBER
         := TO_NUMBER (fnd_profile.VALUE ('XXWC_EXTRACT_DAYS'));
   BEGIN
      errbuf := 'Success';
      retcode := 0;

      -- output parameter values
      fnd_file.put_line (fnd_file.LOG,
                         'p_extract_method = ' || p_extract_method);

      IF p_last_extract_date IS NOT NULL
      THEN
         l_last_extract_date :=
            TRUNC (fnd_date.canonical_to_date (p_last_extract_date));
      ELSE
         l_last_extract_date := TRUNC (SYSDATE - NVL (l_extract_days, 3));
      END IF;

      fnd_file.put_line (fnd_file.LOG,
                         'p_last_extract_date = ' || l_last_extract_date);

      -- refresh XXWC_QP_ECOMMERCE_ITEMS_MV materialized view
      fnd_file.put_line (
         fnd_file.LOG,
         'Refreshing XXWC_QP_ECOMMERCE_ITEMS_MV materialized view');


      -- added if clause in TMS#20150309-00325
      IF p_mv_refresh = 'Y'
      THEN
         DBMS_MVIEW.refresh (list     => 'XXWC_QP_ECOMMERCE_ITEMS_MV',
                             method   => 'C'); -- C=complete  F=fast  ?=force  A=always
      END IF;


      -- perform validation that lookup values are valid price list or modifier list names
      -- no validation in
      FOR r_lookup IN c_lookup_values
      LOOP
         fnd_file.put_line (
            fnd_file.LOG,
               'Can not find valid price list for '
            || r_lookup.meaning
            || ' defined in '
            || r_lookup.lookup_type);
         errbuf := 'Warning';
         retcode := 1;       -- set as warning message and continue processing
      END LOOP;

      l_plh_outfile := UTL_FILE.fopen (l_directory, l_plh_tmp_file_name, 'w'); -- write new price lists interface file
      l_pll_outfile := UTL_FILE.fopen (l_directory, l_pll_tmp_file_name, 'w'); -- write new offers interface file

      -- generate file headers for both PriceLists and Offers
      UTL_FILE.put_line (l_plh_outfile, 'PriceLists');
      UTL_FILE.put_line (l_plh_outfile,
                         'UniqueId|Name|Description|Precedence|Type|Delete');

      UTL_FILE.put_line (l_pll_outfile, 'Offers');
      UTL_FILE.put_line (
         l_pll_outfile,
            'PriceListUniqueId|PriceListName|CatentryUniqueId|CatentryPartNumber|Identifier|Precedence|StartDate|'
         || 'EndDate|QuantityUnitIdentifier|MinimumQuantity|MaximumQuantity|Description|PriceInUSD|Currency|Delete');

      -- build list price extract / table
      -- replicate this logic for new Commerce price lists based upon QP price lists
      -- pass lookup type related to new eCommerce price list

      --------------------------------------------------------------------------
      fnd_file.put_line (fnd_file.LOG, 'Processing National List Price');
      fnd_file.put_line (
         fnd_file.LOG,
         'Start - ' || TO_CHAR (SYSDATE, 'YYYY-MM-DD HH24:MI:SS'));

      process_price_lists (p_modifier_type    => 'XXWC_NATIONAL_PRICING',
                           p_extract_method   => p_extract_method,
                           p_extract_date     => l_last_extract_date);

      fnd_file.put_line (
         fnd_file.LOG,
         'End - ' || TO_CHAR (SYSDATE, 'YYYY-MM-DD HH24:MI:SS'));
      fnd_file.put_line (fnd_file.LOG, '.');
      --------------------------------------------------------------------------

      --------------------------------------------------------------------------
      fnd_file.put_line (fnd_file.LOG, 'Processing WhiteCapList');
      fnd_file.put_line (
         fnd_file.LOG,
         'Start - ' || TO_CHAR (SYSDATE, 'YYYY-MM-DD HH24:MI:SS'));

      process_price_lists (p_modifier_type    => 'XXWC_WHITECAPLIST',
                           p_extract_method   => p_extract_method,
                           p_extract_date     => l_last_extract_date);

      fnd_file.put_line (
         fnd_file.LOG,
         'End - ' || TO_CHAR (SYSDATE, 'YYYY-MM-DD HH24:MI:SS'));
      fnd_file.put_line (fnd_file.LOG, '.');
      --------------------------------------------------------------------------

      --------------------------------------------------------------------------
      fnd_file.put_line (fnd_file.LOG,
                         'Processing WhiteCap ProductsPriceList');
      fnd_file.put_line (
         fnd_file.LOG,
         'Start - ' || TO_CHAR (SYSDATE, 'YYYY-MM-DD HH24:MI:SS'));

      process_price_lists (
         p_modifier_type    => 'XXWC_WHITECAPPRODUCTSPRICELIST',
         p_extract_method   => p_extract_method,
         p_extract_date     => l_last_extract_date);

      --added for TMS 20130503-01243,06/20/2013
      process_price_lists (
         p_modifier_type         => 'XXWC_WHITECAPPRODUCTSPRICELIST' -- here is call to change
                                                                    ,
         p_extract_method        => p_extract_method,
         p_extract_date          => l_last_extract_date,
         p_product_list_prefix   => 'P_');

      fnd_file.put_line (
         fnd_file.LOG,
         'End - ' || TO_CHAR (SYSDATE, 'YYYY-MM-DD HH24:MI:SS'));
      fnd_file.put_line (fnd_file.LOG, '.');
      --------------------------------------------------------------------------

      -- end logic for QP price lists

      -- generate National Trade Price List
      -- replicate this logic for modifier list based price lists
      -- pass lookup type associated with eCommerce price list
      fnd_file.put_line (fnd_file.LOG,
                         'Processing National Trade Price List');
      fnd_file.put_line (
         fnd_file.LOG,
         'Start - ' || TO_CHAR (SYSDATE, 'YYYY-MM-DD HH24:MI:SS'));
      process_modifiers ('XXWC_NATIONAL_TRADE_PRICE',
                         p_extract_method,
                         l_last_extract_date);
      fnd_file.put_line (
         fnd_file.LOG,
         'End - ' || TO_CHAR (SYSDATE, 'YYYY-MM-DD HH24:MI:SS'));
      fnd_file.put_line (fnd_file.LOG, '.');
      -- end logic for modifier list based price lists

      -- retrieve trader price lists
      fnd_file.put_line (fnd_file.LOG, 'Processing Trader Price Lists');
      fnd_file.put_line (
         fnd_file.LOG,
         'Start - ' || TO_CHAR (SYSDATE, 'YYYY-MM-DD HH24:MI:SS'));
      process_modifiers ('XXWC_TRADER_PRICE',
                         p_extract_method,
                         l_last_extract_date);
      fnd_file.put_line (
         fnd_file.LOG,
         'End - ' || TO_CHAR (SYSDATE, 'YYYY-MM-DD HH24:MI:SS'));
      fnd_file.put_line (fnd_file.LOG, '.');

      -- retrieve contract price lists
      fnd_file.put_line (fnd_file.LOG, 'Processing Contract Price Lists');
      fnd_file.put_line (
         fnd_file.LOG,
         'Start - ' || TO_CHAR (SYSDATE, 'YYYY-MM-DD HH24:MI:SS'));
      process_modifiers ('XXWC_CONTRACT_PRICING',
                         p_extract_method,
                         l_last_extract_date);
      fnd_file.put_line (
         fnd_file.LOG,
         'End - ' || TO_CHAR (SYSDATE, 'YYYY-MM-DD HH24:MI:SS'));
      fnd_file.put_line (fnd_file.LOG, '.');
      -- end of logic for QP modifier lists

      UTL_FILE.fclose (l_plh_outfile);
      UTL_FILE.fclose (l_pll_outfile);

      fnd_file.put_line (fnd_file.LOG, l_cnt || ' offer records generated');
      fnd_file.put_line (fnd_file.LOG, '.');

      fnd_file.put_line (
         fnd_file.LOG,
            'Renaming price list file '
         || l_plh_tmp_file_name
         || ' => '
         || l_plh_file_name);
      UTL_FILE.frename (src_location    => l_directory,
                        src_filename    => l_plh_tmp_file_name,
                        dest_location   => l_directory,
                        dest_filename   => l_plh_file_name,
                        overwrite       => TRUE);

      fnd_file.put_line (
         fnd_file.LOG,
            'Renaming price list file '
         || l_pll_tmp_file_name
         || ' => '
         || l_pll_file_name);
      UTL_FILE.frename (src_location    => l_directory,
                        src_filename    => l_pll_tmp_file_name,
                        dest_location   => l_directory,
                        dest_filename   => l_pll_file_name,
                        overwrite       => TRUE);
      fnd_file.put_line (fnd_file.LOG, '.');

      fnd_file.put_line (
         fnd_file.LOG,
            'Process Complete     '
         || TO_CHAR (SYSDATE, 'YYYY-MM-DD HH24:MI:SS'));
   EXCEPTION
      WHEN OTHERS
      THEN
         errbuf := 'Error';
         retcode := 2;
         fnd_file.put_line (fnd_file.LOG, SQLERRM);

         IF UTL_FILE.is_open (l_plh_outfile)
         THEN
            UTL_FILE.fclose (l_plh_outfile);
         END IF;

         IF UTL_FILE.is_open (l_pll_outfile)
         THEN
            UTL_FILE.fclose (l_pll_outfile);
         END IF;

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom,
            p_calling             => g_err_callpoint,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          => g_message,
            p_distribution_list   => g_distro_list,
            p_module              => 'XXWC');
   END price_lists;

   -- Version# 2.00 > Start
   /**************************************************************************
    *
    * PROCEDURE   LOAD_SB_CSP_TEMP_TBL
    *
    * DESCRIPTION Procedure to load CSP information into Temp table
    *
    * CALLED BY Price Lists procedure
    * HISTORY
    * =======
    *
    * VERSION DATE        AUTHOR(S)       DESCRIPTION
    * ------- ----------- --------------- ------------------------------------
    * 2.03   07/01/2014   Gopi Damuluri   TMS# 20140710-00142
    *                                     Resolve missing Pricelists when a Customer has Site Level CSP but no Master level CSP.
    *                                     Also not all Sites have the CSP defined
    *
    *************************************************************************/
   PROCEDURE load_sb_csp_temp_tbl (p_extract_date IN DATE)
   IS
      l_sec   VARCHAR2 (2000);
   BEGIN
      --------------------------------------------------------------------
      -- Truncate Temporary Tables
      --------------------------------------------------------------------
      EXECUTE IMMEDIATE 'TRUNCATE TABLE xxwc.xxwc_sb_qp_temp_tbl';

      l_sec := 'Inserting Customer Specific Pricing for Quotes created';

      -- ### CUSTOMER
      INSERT INTO XXWC_SB_QP_TEMP_TBL (LIST_HEADER_ID, CUSTOMER_ID)
         SELECT DISTINCT
                qq.list_header_id, TO_NUMBER (qq.qualifier_attr_value)
           FROM qp_qualifiers qq
          WHERE     1 = 1
                AND qq.qualifier_attribute = 'QUALIFIER_ATTRIBUTE32'
                AND qq.qualifier_context = 'CUSTOMER'
                AND NOT EXISTS
                       (SELECT '1'
                          FROM XXWC_SB_QP_TEMP_TBL stg
                         WHERE stg.list_header_id = qq.list_header_id)
                AND EXISTS
                       (SELECT '1'
                          FROM apps.oe_order_lines ool
                         WHERE     1 = 1
                               AND TRUNC (ool.last_update_date) >=
                                      p_extract_date
                               AND TRUNC (ool.last_update_date) < SYSDATE + 2
                               AND ool.sold_to_org_id =
                                      qq.qualifier_attr_value
                               AND ool.flow_status_code IN ('PENDING_CUSTOMER_ACCEPTANCE',
                                                            'CUSTOMER_ACCEPTED')
                               AND ool.line_type_id = g_sb_req_line_type_id);

      l_sec := 'Inserting Ship-To Specific Pricing for Quotes created';

      -- ### SHIP_TO SITE
      INSERT INTO XXWC_SB_QP_TEMP_TBL (LIST_HEADER_ID, SHIP_FROM_ORG_ID)
         SELECT DISTINCT
                qq.list_header_id, TO_NUMBER (qq.qualifier_attr_value)
           FROM qp_qualifiers qq
          WHERE     1 = 1
                AND qq.qualifier_attribute = 'QUALIFIER_ATTRIBUTE11'
                AND qq.qualifier_context = 'CUSTOMER'
                AND NOT EXISTS
                       (SELECT '1'
                          FROM apps.XXWC_SB_QP_TEMP_TBL stg
                         WHERE stg.list_header_id = qq.list_header_id)
                AND EXISTS
                       (SELECT '1'
                          FROM apps.oe_order_lines ool,
                               apps.hz_cust_acct_sites hcas   -- Version# 2.03
                                                           ,
                               apps.hz_cust_site_uses hcsu    -- Version# 2.03
                         WHERE     1 = 1
                               AND hcas.cust_acct_site_id =
                                      hcsu.cust_acct_site_id  -- Version# 2.03
                               AND hcsu.site_use_id = qq.qualifier_attr_value -- Version# 2.03
                               AND ool.sold_to_org_id = hcas.cust_account_id -- Version# 2.03
                               AND TRUNC (ool.last_update_date) >=
                                      p_extract_date
                               AND TRUNC (ool.last_update_date) < SYSDATE + 2
                               --                     AND ool.ship_to_org_id          = qq.qualifier_attr_value  -- Version# 2.03
                               AND ool.flow_status_code IN ('PENDING_CUSTOMER_ACCEPTANCE',
                                                            'CUSTOMER_ACCEPTED')
                               AND ool.line_type_id = g_sb_req_line_type_id);

      l_sec := 'Inserting CSPs corresponding to Past Quotes';

      -- ### CSP
      INSERT INTO XXWC_SB_QP_TEMP_TBL (LIST_HEADER_ID, CUSTOMER_ID)
         SELECT DISTINCT qlh.list_header_id, cph.customer_id
           FROM xxwc_om_contract_pricing_lines cpl,
                apps.xxwc_om_contract_pricing_hdr cph,
                qp_secu_list_headers_vl qlh
          WHERE     1 = 1
                AND cph.agreement_status = 'APPROVED'
                AND cph.agreement_id = cpl.agreement_id
                AND qlh.attribute14 = cph.agreement_id
                AND cpl.product_attribute = 'ITEM'
                AND TRUNC (cpl.last_update_date) >= p_extract_date
                AND TRUNC (cpl.last_update_date) < SYSDATE + 2
                AND NOT EXISTS
                       (SELECT '1'
                          FROM apps.XXWC_SB_QP_TEMP_TBL stg
                         WHERE stg.list_header_id = qlh.list_header_id)
                AND EXISTS
                       (SELECT '1'
                          FROM apps.oe_order_lines ool
                         WHERE     ool.line_type_id = g_sb_req_line_type_id
                               AND ool.sold_to_org_id = cph.customer_id
                               AND ool.flow_status_code IN ('PENDING_CUSTOMER_ACCEPTANCE',
                                                            'CUSTOMER_ACCEPTED')
                               AND ool.ordered_item = cpl.product_value);

      -- Version# 2.02 > Start
      UPDATE xxwc_sb_qp_temp_tbl stg
         SET stg.customer_id =
                (SELECT hcas.cust_account_id
                   FROM apps.hz_cust_acct_sites hcas,
                        apps.hz_cust_site_uses hcsu
                  WHERE     1 = 1
                        AND hcsu.site_use_id = stg.ship_from_org_id
                        AND hcas.cust_acct_site_id = hcsu.cust_acct_site_id)
       WHERE 1 = 1 AND stg.ship_from_org_id IS NOT NULL;


      INSERT INTO XXWC_SB_QP_TEMP_TBL (CUSTOMER_ID)
         SELECT DISTINCT ool.sold_to_org_id
           FROM apps.oe_order_lines ool
          WHERE     1 = 1
                AND TRUNC (ool.last_update_date) >= p_extract_date
                AND TRUNC (ool.last_update_date) < SYSDATE + 2
                AND ool.flow_status_code IN ('PENDING_CUSTOMER_ACCEPTANCE',
                                             'CUSTOMER_ACCEPTED')
                AND NOT EXISTS
                           (SELECT '1'
                              FROM apps.xxwc_sb_qp_temp_tbl stg
                             WHERE     stg.customer_id = ool.sold_to_org_id
                                   AND stg.ship_from_org_id IS NULL)
                AND ool.line_type_id = g_sb_req_line_type_id;

      -- Version# 2.02 < End

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'LOAD_SB_CSP_TEMP_TBL',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          => 'Error while loading SpeedBuild CSP Temp table',
            p_distribution_list   => g_distro_list,
            p_module              => 'XXWC');
   END load_sb_csp_temp_tbl;

   -- Version# 2.00 < End

   -- Version# 2.01 > Start
   FUNCTION get_contractlist_price (p_item_num          IN VARCHAR2,
                                    p_price_list_name      VARCHAR2,
                                    p_end_date_active      VARCHAR2) -- Version# 2.02
      RETURN NUMBER
   IS
      CURSOR c_list_price
      IS
         SELECT cpl.selling_price                                -- list_price
           FROM qp_list_headers qlh, xxwc_om_contract_pricing_lines cpl
          WHERE     1 = 1
                AND qlh.name = p_price_list_name
                AND qlh.active_flag = 'Y'
                AND qlh.attribute14 = cpl.agreement_id
                AND cpl.product_attribute = 'ITEM'
                AND cpl.product_value = p_item_num
                AND (   p_end_date_active IS NULL
                     OR TRUNC (cpl.end_date) =
                           TRUNC (
                              TO_DATE (p_end_date_active,
                                       'YYYY-MM-DD HH24:MI:SS'))) -- Version# 2.02
                --                 AND NVL(cpl.end_date, TRUNC(SYSDATE) +1)  >= TRUNC(SYSDATE) -- Version# 1.52
                AND TRUNC (SYSDATE) BETWEEN NVL (qlh.start_date_active,
                                                 SYSDATE - 1)
                                        AND NVL (qlh.end_date_active,
                                                 SYSDATE + 1);

      /*
              SELECT qll.operand unit_price
                      FROM qp_list_headers qlh
                         , qp_modifier_summary_v qll
                     WHERE    1=1
                       and qlh.name             = p_price_list_name
                       AND qlh.active_flag      = 'Y'
                       AND TRUNC (SYSDATE) BETWEEN NVL (qlh.start_date_active, SYSDATE - 1)
                                                   AND NVL (qlh.end_date_active, SYSDATE + 1)
                       AND qll.list_header_id   = qlh.list_header_id
                       AND qll.product_attr     = 'PRICING_ATTRIBUTE1'
                       AND qll.product_attr_val = TO_CHAR (p_item_id)
                       AND TRUNC (SYSDATE) BETWEEN NVL (qll.start_date_active, SYSDATE - 1)
                                                   AND NVL (qll.end_date_active, SYSDATE + 1);
      */

      l_list_price   NUMBER := -1;
   BEGIN
      FOR r_list_price IN c_list_price
      LOOP
         IF c_list_price%NOTFOUND
         THEN
            l_list_price := -1;
         ELSE
            l_list_price := r_list_price.selling_price;
         END IF;
      END LOOP;

      RETURN l_list_price;
   END get_contractlist_price;
-- Version# 2.01 < End

END XXWC_QP_EXTRACTS_PKG;
/