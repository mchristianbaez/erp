CREATE OR REPLACE PACKAGE BODY APPS.XXWC_PURGE_OLD_QUOTE
AS
/*************************************************************************
  $Header XXWC_PURGE_OLD_QUOTE.pkb $
  Module Name: OM

  * PURPOSE  : Package specification for TMS#20180216-00273
  * Description: This package will delete the old custom and standard quotes
                 based on parameter values

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        06-MAR-2018  Pattabhi Avula        purge std and custom quotes.  
             16-APR-2018  Rakesh Patel          Added procedure to convert quote
**************************************************************************/   
   -- global error handling variables
   g_err_callfrom          VARCHAR2 (75) := 'XXWC_PURGE_OLD_QUOTE.main';
   g_err_callpoint         VARCHAR2 (240) := 'START';
   g_distro_list           VARCHAR2 (75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
   g_message               VARCHAR2 (2000);
   
   
/*************************************************************************
  * Procedure  : main
  * Description: This procedure will call another two procedures based
                 on parameters.

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        06-MAR-2018  Pattabhi Avula         TMS#20180216-00273

**************************************************************************/ 
PROCEDURE main(ERRBUFF       OUT VARCHAR2, 
			   RETCODE       OUT VARCHAR2,
			   P_QUOTE_TYPE  IN VARCHAR2, 
               P_DATE        IN VARCHAR2, 
			   P_BATCH_SIZE  IN NUMBER
			   )
  IS
l_quote_date    DATE;  
BEGIN  
  DBMS_OUTPUT.ENABLE (100000);
  
  l_quote_date :=fnd_date.canonical_to_date(P_DATE);
  
  IF P_QUOTE_TYPE = 'STANDARD' THEN
  
     standard_quote(l_quote_date,P_BATCH_SIZE);
	 g_err_callpoint:='Calling from standard_quote procedure';
	 
  ELSIF  P_QUOTE_TYPE = 'CUSTOM' THEN
  
     custom_quote(l_quote_date);
	 g_err_callpoint:='Calling from custom_quote procedure';
  END IF;
  
EXCEPTION
   WHEN OTHERS
   THEN
   errbuff := 'Error';
   retcode := 2;  
   g_message:=SUBSTR(SQLERRM,1,240);   
xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom,
            p_calling             => g_err_callpoint,
            p_ora_error_msg       => SUBSTR(SQLERRM,1,2000),
            p_error_desc          => g_message,
            p_distribution_list   => g_distro_list,
            p_module              => 'XXWC');
END;


/*************************************************************************
  * Procedure  : standard_quote
  * Description: This procedure will purge all standard quotes based on
                 parameter values

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        06-MAR-2018  Pattabhi Avula         TMS#20180216-00273

**************************************************************************/

PROCEDURE standard_quote(P_DATE       IN DATE, 
                         P_BATCH_SIZE IN NUMBER)
  IS 
  
  x_header_rec            OE_ORDER_PUB.Header_Rec_Type;
  X_DEBUG_FILE            VARCHAR2(100); 
  l_msg_index_out         NUMBER(10); 
  l_org_id                NUMBER := 162;
  l_return_status         VARCHAR2(1000); 
  l_msg_count             NUMBER; 
  l_msg_data              VARCHAR2(1000); 
  l_user_name             VARCHAR2(150) := 'XXWC_INT_SALESFULFILLMENT';
  l_responsibility_name   VARCHAR2(150) := 'HDS Order Mgmt Super User - WC';
  l_user_id               NUMBER;
  l_responsibility_id     NUMBER;
  l_resp_application_id   NUMBER;

  
  CURSOR CUR_HDQ_QUOTES IS
    SELECT header_id,quote_number 
	  FROM APPS.OE_ORDER_HEADERS_ALL oh
     WHERE quote_number IS NOT NULL
	   AND flow_status_code IN('DRAFT','DRAFT_CUSTOMER_REJECTED','PENDING_CUSTOMER_ACCEPTANCE')						
	   AND TRUNC(CREATION_DATE)<= TRUNC(P_DATE)
	   AND ROWNUM<=P_BATCH_SIZE;
	   
BEGIN 
  dbms_output.enable(1000000); 
      ----------------------------------------------------------------------------------
      -- Derive UserId
      ----------------------------------------------------------------------------------
      BEGIN
         SELECT user_id
           INTO l_user_id
           FROM fnd_user
          WHERE     user_name = l_user_name
                AND SYSDATE BETWEEN start_date
                                AND NVL (end_date, TRUNC (SYSDATE) + 1);
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            g_message :=
               'UserName ' || l_user_name || ' not defined in Oracle';
         WHEN OTHERS
         THEN
            g_message :=
               'Error deriving user_id for UserName - ' || l_user_name;
      END;
    
      
      ----------------------------------------------------------------------------------
      -- Derive ResponsibilityId and ApplicationId
      ----------------------------------------------------------------------------------
      BEGIN
         SELECT responsibility_id, application_id
           INTO l_responsibility_id, l_resp_application_id
           FROM fnd_responsibility_vl
          WHERE     responsibility_name = l_responsibility_name
                AND SYSDATE BETWEEN start_date
                                AND NVL (end_date, TRUNC (SYSDATE) + 1);
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            g_message :=
                  'Responsibility '||
              l_responsibility_name
               || ' not defined in Oracle';           
         WHEN OTHERS
         THEN
            g_message :=
                  'Error deriving Responsibility_id for '
               || l_responsibility_name;          
      END;
      
      ----------------------------------------------------------------------------------
      -- Apps Initialize
      ----------------------------------------------------------------------------------
      FND_GLOBAL.APPS_INITIALIZE (l_user_id,
                                  l_responsibility_id,
                                  l_resp_application_id);
      mo_global.set_policy_context('S',l_org_id); 
      mo_global.init('ONT');

      oe_msg_pub.initialize; 
	  
	BEGIN 
     FOR I IN CUR_HDQ_QUOTES
      LOOP
	  
	   BEGIN
		  INSERT 
		    INTO XXWC.XXWC_OE_ORDER_HDR_QUOTE_BKP
		  SELECT * 
		    FROM APPS.oe_order_headers_all 
		   WHERE header_id=I.header_id;
		   
		   INSERT 
		    INTO XXWC.XXWC_OE_ORDER_LINES_QUOTE_BKP
		  SELECT * 
		    FROM APPS.oe_order_lines_all 
		   WHERE header_id=I.header_id;
		 END;		   

       x_header_rec := OE_ORDER_PUB.G_MISS_HEADER_REC; 
       x_header_rec.header_id :=I.HEADER_ID;  

       oe_order_pub.Delete_Order (
                     p_header_id        => x_header_rec.header_id,
                     p_org_id           => l_org_id,
                     p_operating_unit   => NULL,   -- MOAC,
                     x_return_status    => l_return_status,
                     x_msg_count        => l_msg_count,
                     x_msg_data         => l_msg_data);

        FOR j IN 1 .. l_msg_count 
        LOOP 
         Oe_Msg_Pub.get( p_msg_index => j 
          , p_encoded => Fnd_Api.G_FALSE 
          , p_data => l_msg_data 
          , p_msg_index_out => l_msg_index_out); 
          fnd_file.put_line (fnd_file.LOG,'message is: ' || l_msg_data); 
          fnd_file.put_line (fnd_file.LOG,'message index is: ' || l_msg_index_out); 
        END LOOP; 
        -- Check the return status 
        IF l_return_status = FND_API.G_RET_STS_SUCCESS 
        THEN 
         fnd_file.put_line (fnd_file.LOG,'Successfully deleted the Quote :'||i.quote_number); 		 
        ELSE
         fnd_file.put_line (fnd_file.LOG,' API Failed while deleting the Quote :'||i.quote_number); 
        END IF; 
		COMMIT;
     END LOOP;
    EXCEPTION
     WHEN OTHERS THEN
     fnd_file.put_line (fnd_file.LOG,'Error Message is '||SQLERRM); 
     END;
EXCEPTION
  WHEN OTHERS THEN
  fnd_file.put_line (fnd_file.LOG,'Error Message is '||SQLERRM); 
END; 


/*************************************************************************
  * Procedure  : custom_quote
  * Description: This procedure will purge all custom quotes based on
                 parameter values

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        06-MAR-2018  Pattabhi Avula         TMS#20180216-00273

**************************************************************************/

PROCEDURE custom_quote(P_DATE IN DATE)
  IS 

  CURSOR cur_ql
   IS 
       SELECT ROWID 
	     FROM xxwc.xxwc_om_quote_lines
          WHERE TRUNC(creation_date)<=TRUNC(P_DATE);



   CURSOR cur_qh
   IS
      SELECT ROWID FROM xxwc.xxwc_om_quote_headers
               WHERE  TRUNC(creation_date)<=TRUNC(P_DATE);
			     
	  
	  TYPE rec_Table1 IS TABLE OF cur_qh%ROWTYPE
      INDEX BY PLS_INTEGER;
	  
	   TYPE rec_Table2 IS TABLE OF cur_ql%ROWTYPE
      INDEX BY PLS_INTEGER;

	 
   WORKING_REC_TABLE_1   REC_TABLE1;
   working_rec_table_2   rec_Table2;
   
   ln_counter1 NUMBER:=0;
   ln_counter2 NUMBER:=0;

BEGIN
    dbms_output.enable(1000000);
   -- Taking the data backup
   BEGIN
     INSERT 
       INTO XXWC.XXWC_OM_QUOTE_HEADERS_BKP
     SELECT * 
       FROM xxwc.xxwc_om_quote_headers
      WHERE TRUNC(creation_date)<=TRUNC(P_DATE);
      
      INSERT 
       INTO XXWC.XXWC_OM_QUOTE_LINES_BKP
     SELECT * 
       FROM xxwc.xxwc_om_quote_lines 
      WHERE TRUNC(creation_date)<=TRUNC(P_DATE);
   END;		      
   
   OPEN cur_ql;

   LOOP
      FETCH cur_ql BULK COLLECT INTO working_rec_table_2 LIMIT 50000;
	  
	        fnd_file.put_line (fnd_file.LOG,'working_rec_table_2 count '||working_rec_table_2.COUNT);

           ln_counter2:=ln_counter2+working_rec_table_2.COUNT;

      FORALL i IN working_rec_table_2.FIRST .. working_rec_table_2.LAST
         DELETE   FROM xxwc.xxwc_om_quote_lines
               WHERE ROWID = working_rec_table_2 (i).ROWID;

      
    COMMIT;
      WORKING_REC_TABLE_2.DELETE;
	  
	  EXIT WHEN cur_ql%NOTFOUND;
   END LOOP;

   CLOSE cur_ql;
    fnd_file.put_line (fnd_file.LOG,'Total Records Deleted in table xxwc_om_quote_lines -'||ln_counter2);
    fnd_file.put_line (fnd_file.LOG,'Process xxwc_om_quote_lines end'||TO_CHAR(SYSDATE,'DD-MON-YYYY HH24:MI:SS'));   	  


      fnd_file.put_line (fnd_file.LOG,'Process MTL_INTERFACE TABLES Purge begin '||TO_CHAR(SYSDATE,'DD-MON-YYYY HH24:MI:SS'));

   OPEN cur_qh;

   LOOP
      FETCH cur_qh BULK COLLECT INTO working_rec_table_1 LIMIT 50000;
	  
	  fnd_file.put_line (fnd_file.LOG,'working_rec_table_1 count '||working_rec_table_1.COUNT);
	  ln_counter1:=ln_counter1+working_rec_table_1.COUNT;

      FORALL i IN working_rec_table_1.FIRST .. working_rec_table_1.LAST
         DELETE   FROM xxwc.xxwc_om_quote_headers
          WHERE ROWID = working_rec_table_1 (i).rowid;

    COMMIT;
      working_rec_table_1.delete;
	  EXIT WHEN cur_qh%NOTFOUND;
   END LOOP;


   CLOSE cur_qh;
   
      fnd_file.put_line (fnd_file.LOG,'Total Records Deleted in table xxwc_om_quote_headers -'||ln_counter1);
      fnd_file.put_line (fnd_file.LOG,'Process xxwc_om_quote_headers end '||TO_CHAR(SYSDATE,'DD-MON-YYYY HH24:MI:SS'));    
   COMMIT;
   EXCEPTION
   WHEN OTHERS
   THEN
      IF cur_qh%ISOPEN
      THEN
         CLOSE cur_qh;
      END IF;
	  
	  IF cur_ql%ISOPEN
      THEN
         CLOSE cur_ql;
      END IF;
	  
	    ROLLBACK;
      fnd_file.put_line (fnd_file.LOG,'Exception '||TO_CHAR(SYSDATE,'DD-MON-YYYY HH24:MI:SS'));   	  	  
      fnd_file.put_line (fnd_file.LOG,'Unable to delete the records ' || SQLERRM);
END;

PROCEDURE write_log (p_message IN VARCHAR2)
IS
BEGIN
   --DBMS_OUTPUT.PUT_LINE(p_message);
   fnd_file.put_line (fnd_file.LOG,p_message);
END;

/*************************************************************************
  * Procedure  : std_to_wc_quote_conv
  * Description: This procedure will convert all std quote to custom quotes based on
                 parameter values

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0       16-APR-2018  Rakesh Patel          Added procedure to convert quote
**************************************************************************/
PROCEDURE std_to_wc_quote_conv( ERRBUFF       OUT VARCHAR2 
			                   ,RETCODE       OUT VARCHAR2
			                   ,P_QUOTE_NUMBER IN NUMBER
							   )
IS		
   l_from_date    DATE;
   l_to_date    DATE; 
   l_header_rec        xxwc_order_header_rec;  --in
   l_order_lines_tbl   xxwc_order_lines_tbl;  --in 

   l_quote_total       NUMBER;
   l_quote_number      NUMBER;
   l_return_status     VARCHAR2(4000);
   l_return_msg        VARCHAR2(4000);
   l_line_seq          NUMBER :=0;
   l_line_tbl_idx      NUMBER :=0;
   
   CURSOR cur_standard_quotes_hdr ( P_QUOTE_NUMBER IN NUMBER )
      IS
         SELECT ooh.quote_number
               ,ooh.header_id 
               ,ooh.sales_document_name quote_name
               ,ooh.request_date--TO_CHAR (ooh.request_date, ''MM/DD/YYYY'') request_date
               ,ooh.quote_date--TO_CHAR (ooh.quote_date, ''MM/DD/YYYY'') quote_date
               ,TO_CHAR (trunc(ooh.expiration_date), 'YY-MM-DD') expiration_date--TRUNC(ooh.expiration_date) expiration_date
               ,ooh.creation_date order_date
               ,ooh.order_source_id
               ,ooh.salesrep_id
               ,ooh.sold_to_org_id
               ,ooh.ship_to_org_id
               ,ooh.invoice_to_org_id
               ,ooh.shipping_method_code
               ,ooh.invoice_to_contact_id
               ,ooh.shipping_instructions shipping_instructions
               ,ooh.created_by
               ,fu.user_name
               ,mp.organization_code
            FROM apps.oe_order_headers_all ooh
                ,apps.mtl_parameters mp
                ,apps.fnd_user fu
           WHERE 1=1
             AND mp.organization_id = ooh.ship_from_org_id
             AND fu.user_id = ooh.created_by
             AND ooh.quote_number IS NOT NULL
             AND ooh.quote_number = P_QUOTE_NUMBER
             AND ooh.flow_status_code IN('PENDING_CUSTOMER_ACCEPTANCE')
             AND (TRUNC(ooh.expiration_date) > TRUNC(SYSDATE) OR ooh.expiration_date IS NULL)
             AND NOT EXISTS( SELECT 1
                              FROM xxwc.xxwc_om_quote_headers xoqh
                             WHERE xoqh.attribute15 = TO_CHAR(ooh.quote_number)
                            );

    CURSOR cur_standard_quotes_lines(p_header_id IN NUMBER)
      IS
         SELECT ool.inventory_item_id
               ,ool.ordered_quantity
               ,ool.unit_selling_price
               ,ool.shipping_instructions
            FROM apps.oe_order_lines_all ool
           WHERE 1=1
             AND ool.header_id = p_header_id
        ORDER BY line_number;    					   
BEGIN
 
   fnd_file.put_line (fnd_file.LOG,'Parameter P_QUOTE_NUMBER: '||P_QUOTE_NUMBER);  	

   FND_GLOBAL.APPS_INITIALIZE (15986, 50880, 222);
    
   mo_global.set_policy_context('S',162);
     
   FOR cur_standard_quotes IN cur_standard_quotes_hdr ( P_QUOTE_NUMBER => P_QUOTE_NUMBER )
   LOOP
      BEGIN
           write_log ( 'Assign values to l_header_rec for quote_number: ' || cur_standard_quotes.quote_number );

           l_header_rec                       := xxwc_order_header_rec.g_miss_null;
           l_header_rec.sold_to_org_id        := cur_standard_quotes.sold_to_org_id;
           l_header_rec.ship_to_org_id        := cur_standard_quotes.ship_to_org_id;   
           l_header_rec.invoice_to_org_id     := cur_standard_quotes.invoice_to_org_id;
           l_header_rec.invoice_to_contact_id := cur_standard_quotes.invoice_to_contact_id;     
           l_header_rec.shipping_instructions := cur_standard_quotes.shipping_instructions;
           l_header_rec.expiration_date       := NVL(cur_standard_quotes.expiration_date, TO_CHAR (trunc(sysdate)+60, 'YY-MM-DD'));
           l_header_rec.salesrep_id           := cur_standard_quotes.salesrep_id;
           l_header_rec.org_id                := 162;
           l_header_rec.shipping_method_code  := cur_standard_quotes.shipping_method_code;
           l_header_rec.branch_code           := cur_standard_quotes.organization_code;
           l_header_rec.created_by            := cur_standard_quotes.user_name;
           l_header_rec.order_source_type     := 'ONLINE';
           l_header_rec.orig_sys_document_ref := cur_standard_quotes.quote_number;
           
           l_line_seq     := 0;
           l_line_tbl_idx := 0;
          -- l_order_lines_tbl.delete;  
           l_order_lines_tbl :=  XXWC_ORDER_LINES_TBL();
              
           FOR cur_standard_quote_lines IN cur_standard_quotes_lines (cur_standard_quotes.header_id)
           LOOP
              --write_log ( 'Assign values to l_order_lines_tbl for header_id: ' || cur_standard_quotes.header_id );
              l_line_seq     := l_line_seq +10;
              l_line_tbl_idx := l_line_tbl_idx+1;
              l_order_lines_tbl.EXTEND();

              l_order_lines_tbl(l_line_tbl_idx)                      := xxwc_order_line_rec.g_miss_null;
              l_order_lines_tbl(l_line_tbl_idx).line_id              := l_line_seq;
              l_order_lines_tbl(l_line_tbl_idx).inventory_item_id    := cur_standard_quote_lines.inventory_item_id;
              l_order_lines_tbl(l_line_tbl_idx).ordered_quantity     := cur_standard_quote_lines.ordered_quantity;
              l_order_lines_tbl(l_line_tbl_idx).unit_selling_price   := cur_standard_quote_lines.unit_selling_price;
              l_order_lines_tbl(l_line_tbl_idx).shipping_instructions:= cur_standard_quote_lines.shipping_instructions;
           END LOOP;
           
           IF l_order_lines_tbl.count > 0 THEN  
		      --------------------------------------------------------------------------
	          -- Call  XXWC_CHECKITAPP_PKG.CREATE_CUSTOM_QUOTE api to create custom quote
	          --------------------------------------------------------------------------
	          APPS.XXWC_CHECKITAPP_PKG.CREATE_CUSTOM_QUOTE
              (
                p_order_header_rec     => l_header_rec
               ,p_order_lines_tbl      => l_order_lines_tbl
               ,o_status               => l_return_status
               ,o_quote_number         => l_quote_number
               ,o_quote_total          => l_quote_total 
               ,o_message              => l_return_msg
               ,p_debug_mode           => 'T'
              );
 
              write_log( 'Std Quote # conv: '||cur_standard_quotes.quote_number||' Api return status : '||l_return_status || ' Custom Quote # : '||l_quote_number ||' l_quote_total  : '||l_quote_total);
              write_log( 'l_return_msg : '||l_return_msg);    
	          COMMIT;
	        ELSE
              write_log( 'Can not convert the quote to custom quote as there are no line exists for quote Number  : '||l_quote_number);
            END IF;  
      EXCEPTION
      WHEN OTHERS THEN
         write_log ( 'cur_standard_quotes- When Others Error: '|| SQLERRM);
      END;   
   END LOOP; 
    
EXCEPTION
WHEN OTHERS THEN
   fnd_file.put_line (fnd_file.LOG,'Error in std_to_wc_quote_conv '||SQLERRM);   
END;

PROCEDURE wait_for_running_request ( p_request_id IN NUMBER )
   IS
      CURSOR cu_running_requests ( p_request_id IN NUMBER)
      IS
         SELECT /*+ Index(fnd_concurrent_requests_N7) */ request_id
           FROM apps.fnd_concurrent_requests fcr
          WHERE fcr.concurrent_program_id IN ( SELECT CONCURRENT_PROGRAM_ID 
                                                 FROM FND_CONCURRENT_PROGRAMS_VL 
                                                WHERE CONCURRENT_PROGRAM_NAME ='XXWC_STD_TO_WC_QUOTE_CONV'
                                                AND ENABLED_FLAG = 'Y' )
            AND (status_code = 'R' OR status_code = 'I')
            AND (phase_code = 'R' OR phase_code ='P')
            AND request_id = p_request_id;

      l_request_id    NUMBER;
      lc_phase        VARCHAR2 (100);
      lc_status       VARCHAR2 (100);
      lc_dev_phase    VARCHAR2 (100);
      lc_dev_status   VARCHAR2 (100);
      lc_message      VARCHAR2 (100);
      lc_waited BOOLEAN;
   BEGIN
      LOOP
         OPEN cu_running_requests( p_request_id );

         FETCH cu_running_requests
          INTO l_request_id;
         
         IF cu_running_requests%NOTFOUND
         THEN
            CLOSE cu_running_requests;

            EXIT;
         END IF;

         CLOSE cu_running_requests;

         IF l_request_id > 0
         THEN
            lc_waited:=fnd_concurrent.wait_for_request (
                                             request_id      => l_request_id,
                                             INTERVAL        => 2,
                                             max_wait        => 180,
                                             phase           => lc_phase,
                                             status          => lc_status,
                                             dev_phase       => lc_dev_phase,
                                             dev_status      => lc_dev_status,
                                             MESSAGE         => lc_message
                                            );
         END IF;
      END LOOP;
   EXCEPTION
      WHEN OTHERS
      THEN
         write_log ('Error in wait_for_running_request order header id ' || p_request_id);
         write_log ('Error: ' || SQLERRM);
         RAISE;
   END wait_for_running_request;

/*************************************************************************
  * Procedure  : std_to_wc_quote_conv_main
  * Description: This procedure will convert all std quote to custom quotes based on
                 parameter values

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0       16-APR-2018  Rakesh Patel          Added procedure to convert quote
**************************************************************************/
PROCEDURE std_to_wc_quote_conv_main( ERRBUFF       OUT VARCHAR2 
			                        ,RETCODE       OUT VARCHAR2
			                        ,P_QUOTE_NUMBER IN NUMBER
                                    ,P_FROM_DATE    IN VARCHAR2
							        ,P_TO_DATE      IN VARCHAR2
							        ,P_BATCH_SIZE   IN NUMBER
     							   )
IS		
   l_from_date        DATE;
   l_to_date          DATE; 
   l_req_id           NUMBER :=0;
   l_request_id_count NUMBER :=0;
   l_return_status    VARCHAR2(4000);
  
   TYPE request_ids_tbl IS TABLE OF NUMBER INDEX BY PLS_INTEGER;
   l_request_ids_tbl   request_ids_tbl;
   
   CURSOR cur_standard_quotes_hdr ( P_QUOTE_NUMBER IN NUMBER
                                   ,P_FROM_DATE    IN DATE
							       ,P_TO_DATE      IN DATE   
                                   )
      IS
         SELECT ooh.quote_number,  (SELECT COUNT(1) 
                                      FROM apps.oe_order_lines_all ool 
                                     WHERE ool.header_id = ooh.header_id) lines_count
            FROM apps.oe_order_headers_all ooh
                ,apps.mtl_parameters mp
                ,apps.fnd_user fu
           WHERE 1=1
             AND mp.organization_id = ooh.ship_from_org_id
             AND fu.user_id = ooh.created_by
             AND ooh.quote_number IS NOT NULL
             AND ooh.quote_number = NVL(P_QUOTE_NUMBER , ooh.quote_number)
             AND TRUNC(ooh.creation_date) BETWEEN P_FROM_DATE AND P_TO_DATE
             AND ooh.flow_status_code IN('PENDING_CUSTOMER_ACCEPTANCE')
             AND (TRUNC(ooh.expiration_date) > TRUNC(SYSDATE) OR ooh.expiration_date IS NULL)
             AND NOT EXISTS( SELECT 1
                              FROM xxwc.xxwc_om_quote_headers xoqh
                             WHERE xoqh.attribute15 = TO_CHAR(ooh.quote_number)
                            )
             ORDER BY lines_count;
					   
BEGIN
   l_from_date :=fnd_date.canonical_to_date(P_FROM_DATE);
   
   l_to_date :=fnd_date.canonical_to_date(P_TO_DATE);
   
   fnd_file.put_line (fnd_file.LOG,'1. Parameter P_QUOTE_NUMBER: '||P_QUOTE_NUMBER);  	
   fnd_file.put_line (fnd_file.LOG,'2. Parameter P_FROM_DATE: '||l_from_date);  
   fnd_file.put_line (fnd_file.LOG,'3. Parameter P_TO_DATE: '||l_to_date); 
   fnd_file.put_line (fnd_file.LOG,'4. Parameter P_BATCH_SIZE: '||P_BATCH_SIZE);   
   
   FND_GLOBAL.APPS_INITIALIZE (15986, 50880, 222);
    
   mo_global.set_policy_context('S',162);
     
   FOR cur_standard_quotes IN cur_standard_quotes_hdr ( P_QUOTE_NUMBER => P_QUOTE_NUMBER
                                                       ,P_FROM_DATE    => l_from_date
							                           ,P_TO_DATE      => l_to_date
                                                       )
   LOOP
      BEGIN
         l_req_id := fnd_request.submit_request(application => 'XXWC'
	                                          , program     => 'XXWC_STD_TO_WC_QUOTE_CONV'
	                                          , description => NULL
	                                          , start_time  => SYSDATE
	                                          , sub_request => FALSE
	                                          , argument1   => cur_standard_quotes.quote_number --Quote Number
	                                          );
	
	     COMMIT;

         IF NVL (l_req_id, 0) > 0
         THEN
		    l_request_id_count := l_request_id_count+1;
		    l_request_ids_tbl (l_request_id_count) := l_req_id;
		    l_return_status := 'Concurrent request '  || l_req_id || ' has been submitted for QUOTE_NUMBER: ' || cur_standard_quotes.QUOTE_NUMBER; 
  		    write_log  (l_return_status); 
         ELSE
            l_return_status := 'Could not submit concurren request for QUOTE_NUMBER: ' || cur_standard_quotes.QUOTE_NUMBER;
		    write_log  (l_return_status); 
         END IF;
			 
		 IF l_request_ids_tbl.COUNT >= NVL(P_BATCH_SIZE, 50) THEN
	  	    FOR i IN l_request_ids_tbl.FIRST .. l_request_ids_tbl.LAST
			    LOOP
			       wait_for_running_request ( p_request_id => l_request_ids_tbl(i) );
				END LOOP;  
				l_request_ids_tbl.delete;
				l_request_id_count := 0;
	     END IF;
      EXCEPTION
      WHEN OTHERS THEN
         write_log ( 'cur_standard_quotes- When Others Error: '|| SQLERRM);
      END;   
   END LOOP; 
    
EXCEPTION
WHEN OTHERS THEN
   fnd_file.put_line (fnd_file.LOG,'Error in std_to_wc_quote_conv '||SQLERRM);   
END;
   							   
end XXWC_PURGE_OLD_QUOTE;
/