CREATE OR REPLACE PACKAGE BODY APPS.XXCUS_OZF_ADJUSTUPL_PKG
IS
-- ---------------------------------------------------------------------------
-- Copyright 2012 HD Supply  Inc (Orlando, FL) - All rights reserved
--
--
--    NAME:       XXCUS_OZF_ADJUSTUPL_PKG.pck
--
--    REVISIONS:
--    Ver        Date        Author             Description
--    ---------  ----------  ---------------    -------------------------------
--    1.0        04/23/2012  Chandra Gadge      1. Created this package.
--    1.1        05/22/2014  Balaguru Seshadri  2. New routines val_mvid,val_offers,val_currency,val_amount,val_LOB,val_Branch
--                                                  ,get_icx_date_format,get_def_currency,get_def_currency_len 
--                                                   ESMS 248697 / RFC 40565
--    1.2        12/05/2014 Balaguru Seshadri  1.Add REDUCTION type to the val_amount routine logic, ESMS: 263605
--    1.3        05/01/2015 Balaguru Seshadri  ESMS 270044, Add/Modify SQL filters in val_offers procedure
--    1.4        11/19/2015 Balaguru Seshadri  Child: TMS 20151217-00142 , Parent: TMS  20151008-00085, ESMS 301618
--    Notes:
--
--    Overview: This package gives the Adjustment upload web ADI feature
-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
PROCEDURE print_out(p_buffer varchar2)
IS
BEGIN
  fnd_file.put_line(fnd_file.output, p_buffer);
  --dbms_output.put_line(p_buffer);
END print_out;
--
PROCEDURE  print_log(p_buffer varchar2) is
BEGIN
   fnd_file.put_line(fnd_file.log, p_buffer);
END print_log;

  /*******************************************************************************
  * Procedure:   UPLOAD_ADJUST
  * Description: 
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0                                   Initial creation of the procedure
  1.1     11/07/2013    Kathy Poling    RFC 38600 remove cust_account_id validation  
  1.2     01/15/2014    Kathy Poling    SR 237089 remove adjust_reason validation 
                                        for DEDUCTION 
                                        RFC 39229         
  ********************************************************************************/
PROCEDURE UPLOAD_ADJUST(
      P_INTERFACE_ID           NUMBER,
      P_CUST_ACCOUNT_ID     NUMBER,
      P_LIST_HEADER_ID      NUMBER,
      P_OFFER_NAME          VARCHAR2,
      P_OFFER_CODE          VARCHAR2,
      P_AMOUNT              NUMBER,
      P_CURRENCY_CODE       VARCHAR2,
      P_ADJUST_REASON       VARCHAR2,
      P_LOB_PARTY_ID        NUMBER,
      P_LOCATION_ID         NUMBER,
      P_ITEM_TYPE           VARCHAR2,
      P_ITEM_ID             NUMBER,
      P_EFF_START_DATE      DATE,
      P_EFF_END_DATE        DATE,
      X_RETURN_STATUS OUT NOCOPY VARCHAR2)
IS

l_list_header_id           NUMBER;
l_payment_method           VARCHAR2(100);

CURSOR c_offer_id(p_offer_code IN VARCHAR2) IS
SELECT oo.qp_list_header_id,qlhv.attribute5 payment_method
  FROM ozf_offers oo,qp_list_headers_vl qlhv
 WHERE offer_code = p_offer_code
   AND oo.qp_list_header_id=qlhv.list_header_id;

BEGIN
        IF P_OFFER_NAME  IS NULL OR P_OFFER_CODE IS NULL THEN
           raise_application_error(-20000, 'Rebate Name is required');
          ELSE
           OPEN c_offer_id(p_offer_code);
             FETCH c_offer_id INTO l_list_header_id,l_payment_method;
           CLOSE c_offer_id;
        END IF;

        --
        IF P_AMOUNT IS NULL OR P_AMOUNT = 0 THEN
           raise_application_error(-20000, 'Amount is Required and cannot be Zero');
        END IF;

        IF P_CURRENCY_CODE IS NULL THEN
           raise_application_error(-20000, 'Currency Code is Required');
        END IF;

/* Version 1.1 11/6/2013  removing for testing for correcting PRD issue
        IF P_CUST_ACCOUNT_ID IS NULL THEN
           raise_application_error(-20000, 'Master Vendor is Required');
        END IF;
*/
        IF P_ADJUST_REASON IS NULL THEN
           raise_application_error(-20000, 'Adjustment Reason is Required');
        END IF;

        IF P_ADJUST_REASON ='REIMBURSEMENT' AND SIGN(P_AMOUNT) =  1  THEN
        raise_application_error(-20000, 'Enter Negative Amount for REIMBURSEMENT');
        END IF;

/* Version 1.2 remove DEDUCTION validation to allow reimburse overpayments made by vendors
        IF P_ADJUST_REASON ='REIMBURSEMENT' AND l_payment_method != 'DEDUCTION'  THEN
             raise_application_error(-20000, 'REIMBURSEMENT to be created only for Deduction Agreements');
        END IF;
*/
        IF P_EFF_START_DATE IS NOT NULL AND P_EFF_END_DATE IS NOT NULL AND P_EFF_START_DATE > P_EFF_END_DATE THEN
       raise_application_error(-20000, 'Start Date cannot be greater than End Date');
        END IF;

        --
        INSERT INTO XXCUS.XXCUSOZF_REBATE_ADJUST_TBL (
           INTERFACE_ID,
       LIST_HEADER_ID,
       OFFER_NAME,
       OFFER_CODE,
       AMOUNT,
       CURRENCY_CODE,
       CUST_ACCOUNT_ID,
       LOB_PARTY_ID,
       LOCATION_ID,
       ITEM_TYPE,
       ITEM_ID,
       EFF_START_DATE,
       EFF_END_DATE,
           ADJUST_REASON
          ,LAST_UPDATE_DATE
          ,LAST_UPDATED_BY
          ,CREATION_DATE
          ,CREATED_BY
          ,STATUS_CODE
        ) values (
           p_interface_id
        --,p_list_header_id
          ,l_list_header_id
          ,p_offer_name
          ,p_offer_code
          ,p_amount
          ,p_currency_code
          ,p_cust_account_id
          ,p_lob_party_id
          ,p_location_id
          ,p_item_type
          ,p_item_id
          ,p_eff_start_date
          ,p_eff_end_date
          ,p_adjust_reason
          ,SYSDATE --last_update_date
          ,FND_GLOBAL.USER_ID--last_updated_by
          ,SYSDATE --creation_date
          ,FND_GLOBAL.USER_ID--created_by
          ,'X'
        );

      ---
      EXCEPTION
          WHEN utl_file.invalid_path THEN
             raise_application_error(- 20100, 'Invalid Path');
          WHEN utl_file.invalid_mode THEN
             raise_application_error(- 20101, 'Invalid Mode');
          WHEN utl_file.invalid_operation THEN
             raise_application_error(- 20102, 'Invalid Operation');
          WHEN utl_file.invalid_filehandle THEN
             raise_application_error(- 20103, 'Invalid Filehandle');
          WHEN utl_file.write_error THEN
             raise_application_error(- 20104, 'Write Error');
          WHEN utl_file.read_error THEN
             raise_application_error(- 20105, 'Read Error');
          WHEN utl_file.internal_error THEN
             raise_application_error(- 20106, 'Internal Error');
          WHEN fnd_api.g_exc_unexpected_error THEN
             --utl_file.fclose(l_file);
             IF length(SQLERRM) > 30 THEN
                fnd_message.set_name( 'OZF', substr(SQLERRM, 12, 30));
             ELSE
                fnd_message.set_name( 'OZF', SQLERRM);
             END IF;
             x_return_status := fnd_api.g_ret_sts_unexp_error;
          WHEN OTHERS THEN
             --utl_file.fclose(l_file);
             IF length(SQLERRM) > 30 THEN
                fnd_message.set_name( 'OZF', substr(SQLERRM, 12, 30));
             ELSE
                fnd_message.set_name( 'OZF', SQLERRM);
             END IF;
         x_return_status := fnd_api.g_ret_sts_unexp_error;
END UPLOAD_ADJUST;

  /*******************************************************************************
  * Procedure:   Create_Rebate_Adjustment
  * Description: 
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0                                   Initial creation of the procedure
  1.1     11/07/2013    Kathy Poling    RFC 38600 add nvl p_vendor_id     
  1.4    11/19/2015     Balaguru Seshadri TMS 20151008-00085 / ESMS 301618          
  ********************************************************************************/
  
PROCEDURE Create_Rebate_Adjustment(p_adj_rec        IN  adj_rec_type,
                                   x_return_status  OUT VARCHAR2) IS

    l_return_status           VARCHAR2(1);
    l_msg_count               NUMBER;
    l_msg_data                VARCHAR2(2000);
    l_adj_rec                 ozf_fund_utilized_pub.adjustment_rec_type;
    l_adjustment_type         VARCHAR2(200);
    l_total_accrued_amt       NUMBER;
    l_total_spend             NUMBER;
    l_adj_amount              NUMBER;
    l_total_adj_amount        NUMBER;
    l_diff_adj_amount         NUMBER;


        CURSOR c_get_amount  (p_plan_id          IN NUMBER,
                              p_vendor_id         IN NUMBER,
                              p_branch_id        IN NUMBER,
                              p_lob_id            IN NUMBER,
                              p_product_id        IN NUMBER,
                              p_product_type            IN VARCHAR2,
                              p_start_date              IN DATE,
                              --p_end_date                IN DATE) IS --Ver 1.4 
                              p_end_date                IN DATE, --Ver 1.4 
                              p_division_id             IN NUMBER -- Ver 1.4
                              ) IS --Ver 1.4                              
        SELECT     SUM(ofu.acctd_amount) accrued_amount,
                   SUM(orl.selling_price * orl.quantity) total_spend
              FROM ozf_funds_utilized_all_b ofu,
                   ozf_resale_lines_all orl,
                   mtl_system_items_b mtl,
                   hz_cust_accounts hca
             WHERE 1=1
               AND ofu.product_id=orl.inventory_item_id
               AND orl.inventory_item_id=mtl.inventory_item_id
               AND mtl.organization_id=84
               AND ofu.org_id=orl.org_id
               AND ((p_product_type ='I' AND p_product_id = (SELECT inventory_item_id
                                                          FROM mtl_system_items_b
                                                             WHERE organization_id=84))
                    OR (p_product_type='C' AND p_product_id IN (SELECT mc.category_id
                                                               FROM mtl_categories mc,
                                                                     mtl_item_categories mic
                                                             WHERE mc.structure_id=50348
                                                                 AND mic.category_set_id=1100000041
                                                                 AND mic.category_id=mc.category_id
                                                                 AND mic.organization_id=84)) OR mtl.inventory_item_id=ofu.product_id)
               AND ofu.cust_account_id  = NVL(p_vendor_id, ofu.cust_account_id)  --Version 1.1 added nvl
               AND orl.bill_to_party_id = NVL(p_lob_id,orl.bill_to_party_id)
               AND orl.bill_to_cust_account_id = NVL(p_branch_id,orl.bill_to_cust_account_id)
               AND hca.cust_account_id=ofu.cust_account_id
               AND orl.date_ordered BETWEEN NVL(p_start_date,orl.date_ordered) AND NVL(p_end_date,orl.date_ordered)
               AND ofu.object_id=orl.resale_line_id
               AND ofu.utilization_type in ('ACCRUAL','ADJUSTMENT')
               --AND ofu.request_id=449819
               AND ofu.plan_id=p_plan_id
               AND nvl(orl.end_cust_party_id, 0) =nvl(p_division_id , orl.end_cust_party_id); --Ver 1.4


    CURSOR c_utilizations(p_plan_id          IN NUMBER,
                          p_vendor_id         IN NUMBER,
                          p_branch_id        IN NUMBER,
                          p_lob_id        IN NUMBER,
                          p_product_id        IN NUMBER,
                          p_product_type        IN VARCHAR2,
                          p_start_date          IN DATE,
                          --p_end_date            IN DATE) IS --Ver 1.4
                          p_end_date            IN DATE, --Ver 1.4
                          p_division_id              IN NUMBER --Ver 1.4
                        ) IS --Ver 1.4
    SELECT     ofu.fund_id,
               ofu.currency_code,
               ofu.bill_to_site_use_id,
               ofu.cust_account_id,
               ofu.object_type,
               ofu.object_id,
               ofu.product_level_type,
               ofu.product_id,
               orl.item_number,
               ofu.plan_id,
               ofu.component_type,
               ofu.org_id,
               SUM(ofu.acctd_amount) accrued_amount,
               SUM(orl.selling_price * orl.quantity) total_spend
          FROM ozf_funds_utilized_all_b ofu,
               ozf_resale_lines_all orl,
               mtl_system_items_b mtl,
               hz_cust_accounts hca
         WHERE 1=1
           AND ofu.product_id=orl.inventory_item_id
           AND orl.inventory_item_id=mtl.inventory_item_id
           AND mtl.organization_id=84
           AND ofu.org_id=orl.org_id
           AND ((p_product_type ='I' AND p_product_id = (SELECT inventory_item_id
                                                      FROM mtl_system_items_b
                                                         WHERE organization_id=84))
                    OR (p_product_type='C' AND p_product_id IN (SELECT mc.category_id
                                                           FROM mtl_categories mc,
                                                                     mtl_item_categories mic
                                                         WHERE mc.structure_id=50348
                                                             AND mic.category_set_id=1100000041
                                                             AND mic.category_id=mc.category_id
                                                             AND mic.organization_id=84)) OR mtl.inventory_item_id=ofu.product_id)
           AND ofu.cust_account_id  = NVL(p_vendor_id, ofu.cust_account_id)  --Version 1.1 added nvl
           AND orl.bill_to_party_id = NVL(p_lob_id,orl.bill_to_party_id)
           AND orl.bill_to_cust_account_id = NVL(p_branch_id,orl.bill_to_cust_account_id)
           AND hca.cust_account_id=ofu.cust_account_id
           AND orl.date_ordered BETWEEN NVL(p_start_date,orl.date_ordered) AND NVL(p_end_date,orl.date_ordered)
           AND ofu.object_id=orl.resale_line_id
           AND ofu.utilization_type in ('ACCRUAL','ADJUSTMENT')
           --AND ofu.request_id=449819
           AND ofu.plan_id=p_plan_id
           AND nvl(orl.end_cust_party_id, 0) =nvl(p_division_id , orl.end_cust_party_id) --Ver 1.4
           GROUP BY       ofu.fund_id,
                      ofu.currency_code,
                      ofu.bill_to_site_use_id,
                      ofu.cust_account_id,
                      ofu.object_type,
                      ofu.object_id,
                      ofu.product_level_type,
                      ofu.product_id,
                      orl.item_number,
                      ofu.plan_id,
                      ofu.component_type,
                         ofu.org_id;

     TYPE utilizations_cur_type is TABLE OF c_utilizations%rowtype INDEX BY BINARY_INTEGER;

     l_utilizations_rec   utilizations_cur_type;


  BEGIN

    SAVEPOINT Create_Rebate_Adjustment;

    --DBMS_PROFILER.START_PROFILER('CHANDRA_2_07092012');


    --l_return_status := 'S';

    Print_Out('Adjustment / GL Date : ' || SYSDATE);

              Print_Out(
                                '-----------------------------CreateAdj Start--------------------------------------');


    l_total_adj_amount := 0;
    l_diff_adj_amount  := 0;

    OPEN c_get_amount(p_adj_rec.list_header_id,p_adj_rec.cust_account_id,p_adj_rec.location_id,p_adj_rec.lob_party_id,
                        p_adj_rec.item_id,p_adj_rec.item_type,p_adj_rec.eff_start_date,p_adj_rec.eff_end_date, p_division_id =>p_adj_rec.end_cust_party_id); --Ver 1.4
      FETCH c_get_amount INTO l_total_accrued_amt,l_total_spend;
    CLOSE c_get_amount;


    OPEN c_utilizations(p_adj_rec.list_header_id,p_adj_rec.cust_account_id,p_adj_rec.location_id,p_adj_rec.lob_party_id,
                        p_adj_rec.item_id,p_adj_rec.item_type,p_adj_rec.eff_start_date,p_adj_rec.eff_end_date, p_division_id =>p_adj_rec.end_cust_party_id); --Ver 1.4
    LOOP
      FETCH c_utilizations BULK COLLECT INTO l_utilizations_rec;-- LIMIT 500;
      Print_Out('Utilizations Count:-'|| l_utilizations_rec.COUNT);

      FOR i in 1..l_utilizations_rec.COUNT
      LOOP
          l_adj_amount := 0;
          l_adj_rec := NULL;
          IF l_utilizations_rec(i).accrued_amount <> 0 THEN
              l_adj_amount := ROUND(p_adj_rec.amount *  l_utilizations_rec(i).accrued_amount / l_total_accrued_amt,2);
              --Print_Out('Adjustment Amount based on Accrual for-'|| l_utilizations_rec(i).object_id|| ' -is- '|| i ||l_adj_amount);

          ELSE
              l_adj_amount := ROUND(p_adj_rec.amount *  l_utilizations_rec(i).total_spend / l_total_spend,2);
              --Print_Out('Adjustment Amount based on Accrual for-'|| l_utilizations_rec(i).object_id|| ' -is- '|| i ||l_adj_amount);
          END IF;


            l_total_adj_amount := l_total_adj_amount + l_adj_amount;
          --Print_Out('Total Count is'|| l_utilizations_rec.COUNT);

            IF i = l_utilizations_rec.COUNT THEN
               Print_Out('Last Record Count is'|| l_utilizations_rec.COUNT);
               Print_Out('Total Adjusted Amount is--'|| l_total_adj_amount);
               Print_Out('Adjusted Amount is--'|| l_adj_amount);

               IF l_total_adj_amount >  p_adj_rec.amount THEN
                   l_diff_adj_amount :=  l_total_adj_amount-p_adj_rec.amount;
                   l_adj_amount := l_adj_amount - l_diff_adj_amount;
                   l_total_adj_amount := l_total_adj_amount - l_diff_adj_amount;
                   Print_Out('Final Adjusted Amount is--'|| l_adj_amount);
                 ELSE
                   l_diff_adj_amount :=  p_adj_rec.amount-l_total_adj_amount;
                   l_adj_amount := l_adj_amount + l_diff_adj_amount;
                   l_total_adj_amount := l_total_adj_amount + l_diff_adj_amount;
                   Print_Out('Final Adjusted Amount is--'|| l_adj_amount);
               END IF;
            END IF;

            --l_total_adj_amount := l_total_adj_amount + l_diff_adj_amount;

        IF l_adj_amount <> 0 THEN

          IF SIGN(l_adj_amount) = 1  THEN
         l_adjustment_type := 'STANDARD';
        ELSE
         l_adjustment_type := 'DECREASE_EARNED';
          END IF;

          l_adj_rec.fund_id             := l_utilizations_rec(i).fund_id;
         -- l_adj_rec.amount              := ABS(ROUND(l_adj_amount,2));
          l_adj_rec.amount              := ABS(l_adj_amount);
          l_adj_rec.currency_code       := l_utilizations_rec(i).currency_code;
          l_adj_rec.adjustment_date     := SYSDATE;
          l_adj_rec.gl_date             := SYSDATE;
          l_adj_rec.activity_type       := 'OFFR';
          l_adj_rec.cust_account_id     := l_utilizations_rec(i).cust_account_id;
          l_adj_rec.bill_to_site_use_id := l_utilizations_rec(i).bill_to_site_use_id;
          l_adj_rec.document_type       := 'TP_ORDER';
          l_adj_rec.document_number     := l_utilizations_rec(i).object_id;
          l_adj_rec.product_level_type  := 'PRODUCT';
          l_adj_rec.product_id          := l_utilizations_rec(i).product_id;
          l_adj_rec.product_name        := l_utilizations_rec(i).item_number;
          --l_adj_rec.justification       := 'Adjustment Upload'; -- comments
          l_adj_rec.adjustment_type     := l_adjustment_type;
          l_adj_rec.activity_id         := l_utilizations_rec(i).plan_id;
          --l_adj_rec.org_id              := l_utilizations_rec(i).org_id;
          l_adj_rec.attribute10         := p_adj_rec.adjust_reason;



          --dbms_output.put_line(CHR(10));
          --dbms_output.put_line('Before Calling OZF_Fund_Utilized_Pub.Create_Fund_Adjustment ');
          l_return_status := 'S';
          --mo_global.init('OZF');
          --mo_global.set_policy_context(p_access_mode => 'S', p_org_id => 101);

          ozf_fund_utilized_pub.create_fund_adjustment(p_api_version      => 1.0,
                                                       p_init_msg_list    => fnd_api.g_true,
                                                       p_commit           => fnd_api.g_false,
                                                       p_validation_level => fnd_api.g_valid_level_full,
                                                       p_adj_rec          => l_adj_rec,
                                                       x_return_status    => l_return_status,
                                                       x_msg_count        => l_msg_count,
                                                       x_msg_data         => l_msg_data);



          IF l_return_status = FND_API.g_ret_sts_error THEN
             Print_Log('Error in Create_Adjustment');
             RAISE FND_API.G_EXC_ERROR;
           ELSIF l_return_status = FND_API.g_ret_sts_unexp_error THEN
               Print_Log('Error In Create_Adjustment');
             RAISE FND_API.G_EXC_UNEXPECTED_ERROR;
          END IF;
       END IF;

          --dbms_output.put_line('After Calling OZF_Fund_Utilized_Pub.Create_Fund_Adjustment ');
          --dbms_output.put_line('l_return_status : ' || l_return_status);

          --printLog('l_msg_count : ' || l_msg_count);
          --printLog('l_msg_data : ' || l_msg_data);
       END LOOP;
       EXIT WHEN c_utilizations%NOTFOUND;
      END LOOP;
      CLOSE c_utilizations;

      Print_Out('Total Adjustment Amount is-'||p_adj_rec.amount);
      Print_Out('Total Adjusted   Amount is-'||l_total_adj_amount);


      x_return_status := l_return_status;

      --DBMS_PROFILER.STOP_PROFILER;

    --dbms_output.put_line('Return Status'||l_return_status);
    --for i in 0..l_msg_count loop
      --dbms_output.put_line(substr(fnd_msg_pub.get(p_msg_index => i, p_encoded => 'F'), 1, 254));
    --end loop;
    --p_adj_ret_status := l_return_status;

      Print_Out('-----------------------------CreateAdj Stop---------------------------------------');
  EXCEPTION
    WHEN FND_API.G_EXC_ERROR THEN
      x_return_status := FND_API.G_RET_STS_ERROR;
      -- Standard call to get message count and if count=1, get the message
      FND_MSG_PUB.Count_And_Get(p_encoded => FND_API.G_FALSE,
                                p_count   => l_msg_count,
                                p_data    => l_msg_data);
       FOR i in 0..l_msg_count loop
        Print_Out(substr(fnd_msg_pub.get(p_msg_index => i, p_encoded => 'F'), 1, 254));
       END LOOP;

    WHEN FND_API.G_EXC_UNEXPECTED_ERROR THEN
      x_return_status := FND_API.G_RET_STS_UNEXP_ERROR;
      -- Standard call to get message count and if count=1, get the message
      FND_MSG_PUB.Count_And_Get(p_encoded => FND_API.G_FALSE,
                                p_count   => l_msg_count,
                                p_data    => l_msg_data);
       FOR i in 0..l_msg_count loop
        Print_Out(substr(fnd_msg_pub.get(p_msg_index => i, p_encoded => 'F'), 1, 254));
       END LOOP;

    WHEN OTHERS THEN
      ROLLBACK TO Create_Rebate_Adjustment;
      x_return_status := FND_API.G_RET_STS_UNEXP_ERROR;
      Print_Out('Exception in CreateAdj Procedure ' || SQLERRM);

      FND_MSG_PUB.Count_And_Get(p_encoded => FND_API.G_FALSE,
                                p_count   => l_msg_count,
                                p_data    => l_msg_data);
       FOR i in 0..l_msg_count loop
        Print_Out(substr(fnd_msg_pub.get(p_msg_index => i, p_encoded => 'F'), 1, 254));
       END LOOP;
END Create_Rebate_Adjustment;
-- Begin Ver 1.4
  /*******************************************************************************
  * Procedure:   UPLOAD_ADJUST
  * Description: 
  HISTORY
  ===============================================================================
  VERSION DATE    AUTHOR(S)                 DESCRIPTION
  ------- -----------   ---------------                 -----------------------------------------
  1.4                          Balaguru Seshadri    TMS 20151008-00085 / ESMS 301618, Add end_cust_party_id [Division] to the process.  
  ********************************************************************************/
-- End Ver 1.4
PROCEDURE CREATE_ADJUSTMENTS(
      errbuf           IN OUT VARCHAR2
     ,retcode          IN OUT VARCHAR2
     ,p_interface_id   IN NUMBER) IS

  l_adj_tbl           XXCUS_OZF_ADJUSTUPL_PKG.adj_tbl;
  l_return_status     VARCHAR2(1);
  l_agreement_name      QP_LIST_HEADERS_VL.DESCRIPTION%TYPE;
  l_lob_name            HZ_PARTIES.PARTY_NAME%TYPE;
  l_mstr_vendor         HZ_PARTIES.PARTY_NAME%TYPE;
  l_branch_number       HZ_CUST_ACCOUNTS.ACCOUNT_NUMBER%TYPE;
  l_item_type           VARCHAR2(50);
  l_item               VARCHAR2(100);

  l_err_callfrom      VARCHAR2(75) DEFAULT 'XXCUS_OZF_ADJUSTUPL_PKG';
  l_err_callpoint     VARCHAR2(75) DEFAULT 'Rebate Adjustments';
  l_distro_list       VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';

  v_message          VARCHAR2(250);
  l_message          VARCHAR2(150);
  l_err_msg          VARCHAR2(3000);
  l_err_code         NUMBER;
  l_sec              VARCHAR2(500);
  l_req_id           NUMBER := FND_GLOBAL.CONC_REQUEST_ID;

   x_return_status             VARCHAR2(2000);
   x_msg_count                 NUMBER;
   x_msg_data                 VARCHAR2(2000);


  CURSOR c_agreement_name(p_list_header_id IN NUMBER) IS
  SELECT description
    FROM qp_list_headers_vl
   WHERE list_header_id=p_list_header_id;


  CURSOR c_lob_name(p_lob_id IN NUMBER) IS
  SELECT party_name
    FROM hz_parties
   WHERE party_id=p_lob_id;

  CURSOR c_branch_number(p_location_id IN NUMBER) IS
  SELECT account_number
    FROM hz_cust_accounts
   WHERE cust_account_id=p_location_id;

  CURSOR c_product_type(p_item_type IN VARCHAR2) IS
  SELECT meaning
   FROM  FND_LOOKUP_VALUES
   WHERE lookup_type ='XXCUS_REB_ITM_TYPE'
     AND lookup_code = p_item_type;

  CURSOR c_item_number(p_item_id IN NUMBER) IS
  SELECT segment1||' - '||description
   FROM  mtl_system_items_vl
  WHERE  organization_id=84
    AND  inventory_item_id=p_item_id;


  CURSOR c_supplier_category(p_category_id IN NUMBER) IS
  SELECT category_concat_segs||' - '||description
    FROM mtl_categories_v
   WHERE category_id=p_category_id;

  CURSOR c_master_vendor(p_cust_account_id IN NUMBER) IS
  SELECT hp.party_name
    FROM hz_parties hp , hz_cust_accounts hca
   WHERE hp.party_id=hca.party_id
     AND hca.cust_account_id=p_cust_account_id;
 -- Begin Ver 1.4
  CURSOR c_hds_division(p_division_id IN NUMBER) IS
  SELECT div.party_name
    FROM xxcus.xxcus_ozf_rebate_divisions_all div
   WHERE 1 =1
     AND party_id=p_division_id; 
   --
   l_division_name xxcus.xxcus_ozf_rebate_divisions_all.party_name%type :=Null;
   --
 -- End Ver 1.4
  BEGIN
      RETCODE := 0;

      SAVEPOINT Create_Adjustments;

      Print_Out('P_Interface_id:-'|| p_interface_id);


      Print_Out(rpad('***************', 25, ' ') || ':: ' || '**********************');

      l_adj_tbl.DELETE;
            --
            BEGIN
            SELECT  interface_id,
                    list_header_id,
                amount,
                currency_code,
                cust_account_id,
                lob_party_id,
                location_id,
                item_type,
                item_id,
                eff_start_date,
                eff_end_date,
                    adjust_reason
                    ,end_cust_party_id --Ver 1.4
                    ,null end_cust_party_name --Ver 1.4
                    BULK COLLECT
               INTO l_adj_tbl
               FROM XXCUS.XXCUSOZF_REBATE_ADJUST_TBL
              WHERE list_header_id IS NOT NULL
                AND interface_id = p_interface_id
                AND status_code='X';


            EXCEPTION
               WHEN OTHERS THEN
                  Print_Out('Unexpected Error ' || SQLERRM);
                  RAISE;
            END;

            Print_Out('Rebates Agreements Count-'|| l_adj_tbl.COUNT);

            FOR i IN 1 .. l_adj_tbl.COUNT  LOOP

               l_agreement_name := NULL;
               l_lob_name       := NULL;
               l_branch_number  := NULL;
               l_item_type      := NULL;
               l_item           := NULL;


               OPEN c_agreement_name(l_adj_tbl(i).list_header_id);
             FETCH c_agreement_name INTO l_agreement_name;
               CLOSE c_agreement_name;

               OPEN c_master_vendor(l_adj_tbl(i).cust_account_id);
                FETCH c_master_vendor INTO l_mstr_vendor;
               CLOSE c_master_vendor;

               OPEN c_lob_name(l_adj_tbl(i).lob_party_id);
                FETCH c_lob_name INTO l_lob_name;
           CLOSE c_lob_name;

               OPEN c_branch_number(l_adj_tbl(i).location_id);
                 FETCH c_branch_number INTO l_branch_number;
           CLOSE c_branch_number;

               OPEN c_product_type(l_adj_tbl(i).item_type);
                 FETCH c_product_type INTO l_item_type;
           CLOSE c_product_type;

               IF  l_adj_tbl(i).item_type = 'I' THEN
                   OPEN c_item_number(l_adj_tbl(i).item_id);
                 FETCH c_item_number INTO l_item;
               CLOSE c_item_number;

                 ELSIF l_adj_tbl(i).item_type = 'C' THEN
                   OPEN c_supplier_category(l_adj_tbl(i).item_id);
                 FETCH c_supplier_category INTO l_item;
               CLOSE c_supplier_category;
               END IF;
              --
              -- Begin Ver 1.4
                 OPEN c_hds_division(l_adj_tbl(i).end_cust_party_id);
                 FETCH c_hds_division INTO l_division_name;
                 CLOSE c_hds_division;              
                 --
                  l_adj_tbl(i).end_cust_party_name :=l_division_name;
                 --
              -- End Ver 1.4
              --
 
               Print_Out('*---------------------------------------------------------------------------------------------*');
               Print_Out(rpad('Agreement ID', 40, ' ') || ': ' || l_adj_tbl(i).list_header_id);
               Print_Out(rpad('Agreement Name', 40, ' ') || ': ' || l_agreement_name);
               Print_Out(rpad('Adjustment Amount', 40, ' ') || ': ' || l_adj_tbl(i).amount);
               Print_Out(rpad('Currency', 40, ' ') || ': ' || l_adj_tbl(i).currency_code);
               Print_Out(rpad('Master Vendor', 40, ' ') || ': ' || l_mstr_vendor);
           Print_Out(rpad('Line of Business', 40, ' ') || ': ' || l_lob_name);
           Print_Out(rpad('Branch Location', 40, ' ') || ': ' || l_branch_number);
           Print_Out(rpad('Item Type', 40, ' ') || ': ' || l_item_type);
           Print_Out(rpad('Item', 40, ' ') || ': ' || l_item);
           Print_Out(rpad('Effective Start Date', 40, ' ') || ': ' || l_adj_tbl(i).eff_start_date);
           Print_Out(rpad('Effective End Date', 40, ' ') || ': ' || l_adj_tbl(i).eff_end_date);
           Print_Out(rpad('Adjustment Reason', 40, ' ') || ': ' || l_adj_tbl(i).adjust_reason);
           Print_Out(rpad('End Cust Party ID [Division_ID]', 40, ' ') || ': ' || l_adj_tbl(i).end_cust_party_id||', End Cust Party Name [Division] :'||l_adj_tbl(i).end_cust_party_name); -- Ver 1.4

               Print_Out('*---------------------------------------------------------------------------------------------*');

               Print_Out('Agreement Name:-'|| l_agreement_name);




               l_return_status := 'S';

               XXCUS_OZF_ADJUSTUPL_PKG.Create_Rebate_Adjustment(p_adj_rec => l_adj_tbl(i),
                                                                x_return_status => l_return_status);


               Print_Out('Return Status is-'|| l_return_status);

               IF l_return_status = 'S' THEN
                  Print_Out('Agreement :- '||l_agreement_name||' Adjustment Successfull');

                  UPDATE XXCUSOZF_REBATE_ADJUST_TBL
                     SET STATUS_CODE='U'
                   WHERE interface_id=p_interface_id
                     AND list_header_id=l_adj_tbl(i).list_header_id;

                ELSIF l_return_status = FND_API.g_ret_sts_error THEN
                   Print_Out('Agreement :- '||l_agreement_name||' Adjustment Failed');
                   RAISE FND_API.G_EXC_ERROR;

                ELSIF l_return_status = FND_API.g_ret_sts_unexp_error THEN
                     Print_Out('Agreement :- '||l_agreement_name||' Adjustment Failed');
                   RAISE FND_API.G_EXC_UNEXPECTED_ERROR;
               END IF;

            END LOOP;

      EXCEPTION
                      WHEN fnd_api.g_exc_error THEN
                  x_return_status := FND_API.G_RET_STS_ERROR;
               -- Standard call to get message count and if count=1, get the message
                   FND_MSG_PUB.Count_And_Get(p_encoded => FND_API.G_FALSE,
                                     p_count   => x_msg_count,
                                     p_data    => x_msg_data);
               FOR i in 0..x_msg_count loop
              Print_Out(substr(fnd_msg_pub.get(p_msg_index => i, p_encoded => 'F'), 1, 254));
               END LOOP;

               RETCODE := 2;
               ERRBUF  := 'Create Adjustments Completed with Error';

               xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                              ,p_calling           => l_err_callpoint
                                              ,p_request_id        => l_req_id
                                              ,p_ora_error_msg     => SQLERRM
                                              ,p_error_desc        => 'Error Running XXCUS_OZF_UPLOADADJ_PKG.Create_Adjustments package with PROGRAM ERROR'
                                              ,p_distribution_list => l_distro_list
                                                           ,p_module            => 'TM');

                  WHEN fnd_api.g_exc_unexpected_error THEN
                    x_return_status := FND_API.G_RET_STS_UNEXP_ERROR;
                 -- Standard call to get message count and if count=1, get the message

            FND_MSG_PUB.Count_And_Get(p_encoded => FND_API.G_FALSE,
                                      p_count   => x_msg_count,
                                      p_data    => x_msg_data);
            FOR i in 0..x_msg_count loop
               Print_Out(substr(fnd_msg_pub.get(p_msg_index => i, p_encoded => 'F'), 1, 254));
            END LOOP;

            RETCODE := 2;
            ERRBUF  := 'Create Adjustments Completed with Error';

            xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                            ,p_calling           => l_err_callpoint
                                            ,p_request_id        => l_req_id
                                            ,p_ora_error_msg     => SQLERRM
                                            ,p_error_desc        => 'Error Running XXCUS_OZF_UPLOADADJ_PKG.Create_Adjustments package with PROGRAM ERROR'
                                            ,p_distribution_list => l_distro_list
                                                        ,p_module        => 'TM');
                  WHEN OTHERS THEN
                        ROLLBACK TO Create_Adjustments;
                x_return_status := FND_API.G_RET_STS_UNEXP_ERROR;
                Print_Out('Exception in Create Adjustments Procedure ' || SQLERRM);

                FND_MSG_PUB.Count_And_Get(p_encoded => FND_API.G_FALSE,
                                          p_count   => x_msg_count,
                                          p_data    => x_msg_data);
                FOR i in 0..x_msg_count loop
                   Print_Out(substr(fnd_msg_pub.get(p_msg_index => i, p_encoded => 'F'), 1, 254));
                END LOOP;

                RETCODE := 2;
                ERRBUF  := 'Create Adjustments Completed with Error';

                xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                              ,p_calling           => l_err_callpoint
                                              ,p_request_id        => l_req_id
                                              ,p_ora_error_msg     => SQLERRM
                                              ,p_error_desc        => 'Error Running XXCUS_OZF_UPLOADADJ_PKG.Create_Adjustments package with PROGRAM ERROR'
                                              ,p_distribution_list => l_distro_list
                                                         ,p_module            => 'TM');

  END CREATE_ADJUSTMENTS;
 --
 PROCEDURE val_mvid
 -- Used by the custom form HDS Rebates: Accrual Adjustments  
   (
     p_mvid in   varchar2
    ,p_id   out  number     
   ) IS
 BEGIN 
    select cust_account_id 
    into   p_id
    from   apps.xxcusozf_rbt_adj_mvid_v
    where  1 =1
      and  mvid =p_mvid;   
 EXCEPTION
  WHEN no_data_found then 
   p_id :=0;
  WHEN OTHERS THEN
   p_id :=0;
 END val_mvid;
 --
-- ---------------------------------------------------------------------------
--
--    REVISIONS:
--    Ver        Date        Author             Description
--    ---------  ----------  ---------------    -------------------------------
--    1.0        04/23/2012  Balaguru Seshadri  ESMS 248697
--    1.3        05/01/2015 Balaguru Seshadri   ESMS 270044
-- ---------------------------------------------------------------------------- 
 PROCEDURE val_offers 
 -- Used by the custom form HDS Rebates: Accrual Adjustments
   (
     p_customer_id     in  number
    ,p_offer_name      in  varchar2 
    ,p_list_header_id  out number
    ,p_offer_code      out varchar2 
    ,p_cust_account_id out number
    ,p_offer_customer  out varchar2 
    ,p_offer_currency  out varchar2      
   ) IS
 BEGIN 
    select list_header_id
          ,offer_code
          ,cust_account_id
          ,offer_customer 
          ,offer_currency
    into   p_list_header_id
          ,p_offer_code
          ,p_cust_account_id
          ,p_offer_customer
          ,p_offer_currency
    from   apps.xxcusozf_rbt_adj_mvid_offers_v
    where  1 =1
      --and  cust_account_id  =nvl(p_customer_id, cust_account_id) --ver 1.3
      --Begin ver 1.3      
      and  ( (
                 (NVL(p_customer_id, 0) >0) AND (cust_account_id =p_customer_id) 
             )
             OR
                 (2 =2)
           )  
      --End ver 1.3     
      and  offer_name       =p_offer_name;
 EXCEPTION
  WHEN no_data_found then 
   p_list_header_id :=0;
   p_cust_account_id :=0;
   p_offer_customer :=Null;   
   p_offer_code     :=Null;
   p_offer_currency :=Null;
  WHEN OTHERS THEN
   p_list_header_id :=0;
   p_cust_account_id :=0;
   p_offer_customer :=Null;   
   p_offer_code     :=Null;
   p_offer_currency :=Null;
 END val_offers;
 -- 
 PROCEDURE val_currency
 -- Used by the custom form HDS Rebates: Accrual Adjustments  
   (
     p_in_currency   in   varchar2
    ,p_out_currency  out  varchar2   
   ) IS
 BEGIN 
    select currency_code 
    into   p_out_currency
    from   fnd_currencies_tl
    where  1 =1
      and  currency_code =p_in_currency;   
 EXCEPTION
  WHEN no_data_found then 
   p_out_currency :=Null;
  WHEN OTHERS THEN
   p_out_currency :=Null;
 END val_currency;
 --
 PROCEDURE val_amount
 -- Used by the custom form HDS Rebates: Accrual Adjustments  
   (
     p_lookup_code in  varchar2
    ,p_amount      in  number
    ,p_text        out varchar2
    ,p_valid       out varchar2   
   ) IS
 BEGIN 
  --
  if p_lookup_code in ('REIMBURSEMENT', 'REDUCTION') then
   --
      if p_amount <0 then
       p_text :=Null;
       p_valid :='Y';
      elsif p_amount >0 then 
       p_text :='Amount is greater than zero. Please enter a negative amount.';
       p_valid :='N';       
      elsif p_amount =0 then 
       p_text :='Amount is zero. Please enter a negative amount.';
       p_valid :='N';    
      else
       p_text :='Amount is blank. Please enter a negative amount.';
       p_valid :='N';    
      end if;   
   --
  else
   --
      if p_amount <>0 then
       p_text :=Null;
       p_valid :='Y';
      elsif p_amount =0 then 
       p_text :='Amount is zero. Please enter a non zero amount.';
       p_valid :='N';    
      else
       p_text :='Amount is blank. Please enter a non zero amount.';
       p_valid :='N';    
      end if;   
   --
  end if;
  --
 EXCEPTION
  WHEN OTHERS THEN
   p_valid :='Z';
   p_text :=sqlerrm;
 END val_amount;
 -- 
 PROCEDURE val_LOB
 -- Used by the custom form HDS Rebates: Accrual Adjustments  
   (
     p_lob    in  varchar2
    ,p_lob_id out number 
    ,p_text   out varchar2  
   ) IS
 BEGIN 
  --
  select lob_party_id
  into   p_lob_id
  from   apps.xxcusozf_rbt_lob_v
  where  1 =1
    and  lob_party_name =p_lob;
   p_text :=Null;
  --
 EXCEPTION
  WHEN NO_DATA_FOUND THEN
   p_lob_id :=0;
   p_text   :='Not a valid LOB, please enter or choose from the list.';
  WHEN OTHERS THEN
   p_lob_id :=0;
   p_text :=substr(sqlerrm, 1, 2000);
 END val_LOB;
 -- 
 PROCEDURE val_Branch
 -- Used by the custom form HDS Rebates: Accrual Adjustments  
   (
     p_lob_party_id  in  number
    ,p_branch_name   in  varchar2     
    ,p_location_id   out number 
    ,p_text          out varchar2
   ) IS
 BEGIN 
  --
    select branch_id 
    into   p_location_id
    from   apps.xxcusozf_rbt_branch_v 
    where  lob_party_id =p_lob_party_id
      and  branch_name  =p_branch_name;
   p_text :=Null;
  --
 EXCEPTION
  WHEN NO_DATA_FOUND THEN
   p_location_id :=0;
   p_text   :='Not a valid Branch, please enter or choose from the list.';
  WHEN OTHERS THEN
   p_location_id :=0;
   p_text :=substr(sqlerrm, 1, 2000);
 END val_Branch;
 --
 FUNCTION get_icx_date_format
 -- Used by the custom form HDS Rebates: Accrual Adjustments  
   (
      p_user_name in varchar2
   ) RETURN VARCHAR2 IS
  --
   v_default_date_fmt varchar2(20) :='DD-MON-YYYY';
   v_date_format      varchar2(20) :=Null;
  --
 BEGIN 
  --
    SELECT case
            when (user_prof.profile_value is not null) then user_prof.profile_value
            else site_prof.profile_value          
           end
    INTO   v_date_format
    FROM   apps.xxcus_display_profile_values_v user_prof
          ,apps.xxcus_display_profile_values_v site_prof
    WHERE 1 =1
    and site_prof.profile_name ='ICX: Date format mask'
    and user_prof.profile_name(+) =site_prof.profile_name
    AND site_prof.level_set ='Site'
    AND site_prof.context is null
    AND user_prof.context(+) =p_user_name
    AND user_prof.level_set(+) ='User';
  --
   return nvl(v_date_format, v_default_date_fmt);
  --
 EXCEPTION
  WHEN NO_DATA_FOUND THEN
   return v_default_date_fmt;
  WHEN OTHERS THEN
   return v_default_date_fmt;
 END get_icx_date_format;
 --
 FUNCTION get_currency_format
 -- Used by the custom form HDS Rebates: Accrual Adjustments  
   (
      p_currency     in varchar2
     ,p_field_length in number
   ) RETURN VARCHAR2 IS
  --
   v_default_currency_fmt varchar2(20) :='FM999G990D00';
   v_currency_format      varchar2(20) :=Null;
  --
 BEGIN 
  --
    select fnd_currency.get_format_mask( currency_code =>p_currency ,field_length =>p_field_length)
    into   v_currency_format
    from   dual;
  --
   return nvl(v_currency_format, v_default_currency_fmt);
  --
 EXCEPTION
  WHEN NO_DATA_FOUND THEN
   return v_default_currency_fmt;
  WHEN OTHERS THEN
   return v_default_currency_fmt;
 END get_currency_format;
 --
 FUNCTION get_def_currency RETURN VARCHAR2 IS
  --
 BEGIN 
  --
   return xxcus_ozf_adjustupl_pkg.g_default_currency;
  --
 EXCEPTION
  WHEN OTHERS THEN
   return 'USD';
 END get_def_currency;
 -- 
 FUNCTION get_def_currency_len RETURN NUMBER IS
  --
 BEGIN 
  --
   return xxcus_ozf_adjustupl_pkg.g_currency_field_len;
  --
 EXCEPTION
  WHEN OTHERS THEN
   return 13;
 END get_def_currency_len;
 --        
END XXCUS_OZF_ADJUSTUPL_PKG;
/