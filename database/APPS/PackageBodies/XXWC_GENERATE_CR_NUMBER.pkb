CREATE OR REPLACE PACKAGE BODY APPS.XXWC_GENERATE_CR_NUMBER
AS
   /**************************************************************************
    File Name:XXWC_GENERATE_CR_NUMMBER.pkb
    PROGRAM TYPE: PL/SQL Package specification
    PURPOSE: Create change request number
    HISTORY
    -- Description   :
    --
    --
    -- Dependencies Tables        : None
    -- Dependencies Views         : None
    -- Dependencies Sequences     : XXWC_GENERATE_CR_NUM_SEQ
    -- Dependencies Procedures    : None
    -- Dependencies Functions     : None
    -- Dependencies Packages      : None
    -- Dependencies Types         : None
    -- Dependencies Database Links: None
    ================================================================
           Last Update Date : 12/20/2013
    ================================================================
    ================================================================
    VERSION DATE          AUTHOR(S)       DESCRIPTION
    ------- -----------   --------------- ---------------------------------
    1.0     20-Dec-2013   Praveen Pawar   Initial creation of the package
    1.1     22-Jan-2013   Geoff Caesar	  Add Apps. prefix and fix typeO Nummber to Number    
   **************************************************************************/
 
			FUNCTION GENERATE_CR_NUMBER (pv_param1  IN VARCHAR2 ) RETURN VARCHAR2
			IS
			
						ln_cr_num_part     NUMBER                                       := NULL;
						lv_return_param    ENG_ENGINEERING_CHANGES.CHANGE_NOTICE%TYPE   := NULL;
			
			BEGIN

						SELECT APPS.xxwc_generate_cr_num_seq.nextval
						  INTO ln_cr_num_part
						FROM DUAL;

						lv_return_param := 'NPR'||ln_cr_num_part;
						RETURN lv_return_param;

			   RETURN LV_RETURN_PARAM;
						
			EXCEPTION
					
					WHEN OTHERS
					THEN
					
								RETURN NULL;
								
			END GENERATE_CR_NUMBER;

END XXWC_GENERATE_CR_NUMBER;