create or replace package body apps.xxhds_ozf_tp_accrual_pkg as
/*+
 -- Module; HDS Oracle Trademanagement
 -- Date: 24-July-2012
 -- Author: Balaguru Seshadri
 -- Scope: This package will enhance HD Supply to run the Oracle standard third party accrual program in a mulithread fashion.
 -- Parameters:
 -- 1) Partner Account Id -Required
 -- 2) Begin Report Date --Required
 -- 3) End Report Date --Required
*/

 -- Global variables
 G_Batch_Status   Ozf_Resale_Batches_All.Status_Code%Type :='CLOSED';
 G_Application    Fnd_Application.Application_Short_Name%Type :='OZF';
 G_Cp_Short_Code  Fnd_Concurrent_Programs.Concurrent_Program_Name%Type :='OZFTPACCR';

  procedure main (
    retcode               out number
   ,errbuf                out varchar2
   ,p_partner_account_id  in  varchar2
   ,p_begin_date          in  varchar2
   ,p_end_date            in  varchar2
  ) is

  /*
   cursor hds_tp_accruals is
     select hzca.account_number         partner_account_number
           ,hzp.party_name              partner_account_name
           ,ozfrb.resale_batch_id       resale_batch_id
           ,ozfrb.batch_number          resale_batch_number
           ,ozfrb.report_start_date     batch_start_date
           ,ozfrb.report_end_date       batch_end_date
      -- end of fields
    from   hz_cust_accounts   hzca
          ,hz_parties         hzp
          ,ozf_resale_batches ozfrb
      -- end of tables
    where  1 =1
      and  hzca.cust_account_id          =p_partner_account_id
      and  hzp.party_id                  =hzca.party_id
      and  ozfrb.partner_cust_account_id =hzca.cust_account_id
      and  ozfrb.report_start_date between fnd_date.canonical_to_date(p_begin_date) and fnd_date.canonical_to_date(p_end_date)
      and  ozfrb.status_code =G_Batch_Status
      --and  ozfrb.resale_batch_id =10080
    order by hzca.account_number, ozfrb.batch_number asc;
  */

    cursor hds_tp_accruals(n_org_id in number) is
    SELECT
    hzca.account_number         partner_account_number
               ,hzp.party_name              partner_account_name
               ,ozfrb.resale_batch_id       resale_batch_id
               ,ozfrb.batch_number          resale_batch_number
               ,ozfrb.report_start_date     batch_start_date
               ,ozfrb.report_end_date       batch_end_date
               ,count (ozfrb.batch_count)   batch_header_count
               ,count (orl.resale_line_id)  batch_line_count
      from ozf_resale_lines orl,
           ozf_resale_batches_all ozfrb,
           ozf_resale_batch_line_maps_all orm,
           hz_cust_accounts hzca,
           hz_parties hzp
     where 1 =1
       and hzca.cust_account_id =p_partner_account_id
       and ozfrb.org_id =n_org_id
       and orl.org_id =ozfrb.org_id
       and orl.resale_line_id = orm.resale_line_id
       and ozfrb.resale_batch_id = orm.resale_batch_id
       and hzp.party_id = hzca.party_id
       and ozfrb.partner_cust_account_id = hzca.cust_account_id
       and orl.sold_from_cust_account_id =ozfrb.partner_cust_account_id
       and ozfrb.status_code = 'CLOSED'
       and orl.date_ordered between fnd_date.canonical_to_date(p_begin_date) and fnd_date.canonical_to_date(p_end_date)
    group by
    hzca.account_number
               ,hzp.party_name
               ,ozfrb.resale_batch_id
               ,ozfrb.batch_number
               ,ozfrb.report_start_date
               ,ozfrb.report_end_date;

    type hds_tp_accruals_tbl is table of hds_tp_accruals%rowtype index by binary_integer;
    hds_tp_accruals_rec hds_tp_accruals_tbl;

   lc_request_data VARCHAR2(20) :='';
   ln_request_id   NUMBER :=0;
   n_req_id NUMBER :=0;
   l_org_id NUMBER :=0;

  begin

   l_org_id :=mo_global.get_current_org_id;

   n_req_id :=fnd_global.conc_request_id;

   lc_request_data :=fnd_conc_global.request_data;

   if lc_request_data is null then

      lc_request_data :='1';

      Open hds_tp_accruals(l_org_id);
      Fetch hds_tp_accruals Bulk Collect Into hds_tp_accruals_rec;
      Close hds_tp_accruals;

      If hds_tp_accruals_rec.count =0 Then

       fnd_file.put_line(fnd_file.log,'No batches found in closed status for partner_cust_account_id ='||p_partner_account_id||', exit now.');

      Else --hds_tp_accruals_rec.count >0

         for indx in 1 .. hds_tp_accruals_rec.count loop

                fnd_request.set_org_id(mo_global.get_current_org_id);

                ln_request_id :=fnd_request.submit_request
                      (
                       application      =>G_Application,
                       program          =>G_Cp_Short_Code,
                       description      =>'',
                       start_time       =>'',
                       sub_request      =>TRUE,
                       argument1        =>hds_tp_accruals_rec(indx).resale_batch_id,
                       argument2        =>'',
                       argument3        =>'',
                       argument4        =>p_partner_account_id
                      );

                  if ln_request_id >0 then
                    fnd_file.put_line(fnd_file.log, 'Submitted oracle third party accrual program for Partner='
                                                     ||hds_tp_accruals_rec(indx).partner_account_number
                                                     ||', Batch Number='
                                                     ||hds_tp_accruals_rec(indx).resale_batch_number
                                                     ||', Request Id ='
                                                     ||ln_request_id
                                                    );
                  else
                    fnd_file.put_line(fnd_file.log, 'Failed to submit oracle third party accrual program for Partner='
                                                     ||hds_tp_accruals_rec(indx).partner_account_number
                                                     ||', Batch Number='
                                                     ||hds_tp_accruals_rec(indx).resale_batch_number
                                                    );
                  end if;

                 fnd_conc_global.set_req_globals(conc_status =>'PAUSED' ,request_data =>lc_request_data);

                 lc_request_data :=to_char(to_number(lc_request_data)+1);
          end loop;

      End If; -- checking if hds_tp_accruals_rec.count =0

   else
      retcode :=0;
      errbuf :='Child request completed. Exit HDS Kickoff Third Party Accrual -Master';
      fnd_file.put_line(fnd_file.log,errbuf);
      return;
   end if;
  exception
   when others then
    fnd_file.put_line(fnd_file.log, 'Error in caller xxhds_ozf_tp_accrual_pkg.main ='||sqlerrm);
    rollback;
  end main;

end xxhds_ozf_tp_accrual_pkg;
/
