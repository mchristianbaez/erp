CREATE OR REPLACE PACKAGE BODY xxwc_po_conv_pkg
/***************************************************************************
*    script name: XXWC_PO_CONV_PKG
*
*    conversion name: Purchase Orders for Consigned Items
*
*    history:
*
*    version    date              author             description
********************************************************************
*    1.0        01-Feb-2013   G.Damuluri     initial development.
*    1.1        16-May-2013   G.Damuluri     TMS - 20130508-01291 Modified to consider PO 
*                                            lines with status 'CLOSED FOR INVOICE'
*******************************************************************/

AS

PROCEDURE main(p_errbuf       OUT VARCHAR2,
               p_retcode      OUT VARCHAR2,
               p_validate_only IN VARCHAR2)
       IS

l_currency_code           fnd_currencies_vl.currency_code%type;
l_err_flag                Char(1);
l_err_message             varchar2(5000);
l_vendor_id               po_vendors.vendor_id%type;
l_agent_id                NUMBER;
l_vendor_site_id          po_vendor_sites_all.vendor_site_id%type;
l_ship_to                 hr_locations.location_id%type;
l_bill_to                 hr_locations.location_id%type;
l_inventory_item_id       mtl_system_items_b.inventory_item_id%type;
l_legacy_ponum            number(20):=0;
l_batch_id                number(3);

l_fob                     varchar2(25) := 'DESTINATION';
l_freight_carrier         varchar2(25) := 'VENDOR SELECT';
l_freight_terms           varchar2(25) := 'Prepaid';

   CURSOR c_po_header
       IS
   SELECT DISTINCT organization_code
        , vendor_name
        , vendor_number
        , vendor_site_code
     FROM xxwc.xxwc_po_conv_stg_tbl
    WHERE NVL(status,'N') != 'S';

   CURSOR c_po_lines (p_organization_code IN VARCHAR2)
       IS
   SELECT organization_code
        , partnumber
        , onhand_qty
        , avg_cost
     FROM xxwc.xxwc_po_conv_stg_tbl
    WHERE NVL(status,'N') != 'S'
      AND organization_code = RPAD(p_organization_code,3,'0');

   l_line_num             NUMBER;
   l_default_buyer        VARCHAR2(50) := 'Broadwell, Michael V';
   l_def_billto_location  VARCHAR2(50) := 'WCC - WC Headquarters';
   l_organization_code    VARCHAR2(3);

BEGIN

   FOR H1 IN C_PO_HEADER
   LOOP
        l_err_flag           := 'N';
        l_line_num           := 0;
        l_vendor_id          := NULL;
        l_vendor_site_id     := NULL;
        l_bill_to            := NULL;
        l_ship_to            := NULL;
        l_agent_id           := NULL;
        l_organization_code  := NULL;

        BEGIN
          SELECT vendor_id
            INTO l_vendor_id
            FROM po_vendors
           WHERE vendor_name = TRIM(H1.vendor_name);

        EXCEPTION
        WHEN OTHERS THEN
          l_err_flag := 'Y' ;
          l_err_message := l_err_message||'Vendor is not Existing...';
        END;

        BEGIN
          SELECT vendor_site_id
            INTO l_vendor_site_id
            FROM po_vendor_sites_all
           WHERE vendor_id = l_vendor_id
             AND vendor_site_code = TRIM(H1.vendor_site_code);
        EXCEPTION
        WHEN OTHERS THEN
          l_err_flag := 'Y' ;
          l_err_message := l_err_message||'Vendor Site is not Existing...';
        END;

        BEGIN
          SELECT location_id
            INTO l_ship_to
            FROM hr_locations
           WHERE substr(location_code,1,3) = LPAD(H1.organization_code,3,'0')
             AND office_site_flag = 'Y';
        EXCEPTION
        WHEN OTHERS THEN
          l_err_flag := 'Y' ;
          l_err_message := l_err_message||'Ship To Location is not Existing...';
        END;

        BEGIN
          SELECT location_id
            INTO l_bill_to
            FROM hr_locations
           WHERE location_code = l_def_billto_location;
        EXCEPTION
        WHEN OTHERS THEN
          l_err_flag := 'Y' ;
          l_err_message := l_err_message||'Bill To Location is not Existing...';
        END;

        BEGIN
          SELECT employee_id
            INTO l_agent_id
            FROM po_buyers_v
           WHERE full_name = l_default_buyer;
        EXCEPTION
        WHEN OTHERS THEN
          l_err_flag := 'Y' ;
          l_err_message := l_err_message||'Buyer is not Existing...';
        END;

/*
If H1.status = 'Approved' then
l_batch_id := 100 ;
elsif H1.status = 'Incomplete' then
l_batch_id := 101 ;
else
l_err_flag := 'N' ;
l_err_message := l_err_message||'Status is not valid...';
end if;
*/

        IF l_err_flag = 'N' THEN
           INSERT INTO po_headers_interface(interface_header_id,
                                        action,
                                        document_type_code,
                                        currency_code,
                                        agent_id,
                                        vendor_id,
                                        vendor_site_id,
                                        ship_to_location_id,
                                        bill_to_location_id,
                                        attribute_category,
                                        attribute1,
                                        fob,
                                        freight_carrier,
                                        freight_terms,
                                        org_id,
                                        created_by,
                                        creation_date,
                                        last_updated_by,
                                        last_update_date
                                        )
                                        values
                                        (po_headers_interface_s.nextval,
                                        'ORIGINAL',
                                        'STANDARD',
                                        'USD',
                                        l_agent_id,
                                        l_vendor_id,
                                        l_vendor_site_id,
                                        l_ship_to,
                                        l_bill_to,
                                        'Standard Purchase Order',
                                        TO_CHAR(TRUNC(SYSDATE),'YYYY/MM/DD HH24:MI:SS'),
                                        l_fob,
                                        l_freight_carrier,
                                        l_freight_terms,
                                        162,
                                        -1,
                                        TRUNC(SYSDATE),
                                        -1,
                                        TRUNC(SYSDATE)
                                        );

           l_organization_code  := LPAD(H1.organization_code,3,'0');
           FOR L1 IN C_PO_LINES(l_organization_code)
           LOOP
              BEGIN
                SELECT inventory_item_id
                  INTO l_inventory_item_id
                  FROM mtl_system_items_b
                 WHERE segment1        = l1.partnumber
                   AND organization_id = (SELECT inventory_organization_id
                                            FROM hr_locations
                                           WHERE location_id = l_ship_to ) ;
              EXCEPTION
              WHEN OTHERS THEN
                 l_err_flag := 'N' ;
                 l_err_message := l_err_message ||'Inventory Item is not Existing...';
              END;

              IF l1.avg_cost IS NULL THEN
                 l_err_flag := 'Y' ;
                 l_err_message := l_err_message ||'Unit Price is not Existing...';
              END IF;

              IF l1.onhand_qty IS NULL THEN
                 l_err_flag := 'N' ;
                 l_err_message := l_err_message ||'Quantity is not Existing...';
              END IF;

              IF l_err_flag = 'N' THEN
                 l_line_num := l_line_num + 1;
                 INSERT INTO po_lines_interface(interface_line_id,
                                                interface_header_id,
                                                action,
                                                line_num,
                                                item_id,
                                                unit_price,
                                                quantity,
                                                Need_By_Date,
                                                consigned_flag,
                                        created_by,
                                        creation_date,
                                        last_updated_by,
                                        last_update_date
                                                )
                                         VALUES
                                                (po_lines_interface_s.nextval,
                                                po_headers_interface_s.currval,
                                                'ORIGINAL',
                                                l_line_num ,
                                                l_inventory_item_id,
                                                l1.avg_cost,
                                                l1.onhand_qty,
                                                TRUNC(SYSDATE),
                                                'Y',
                                        -1,
                                        TRUNC(SYSDATE),
                                        -1,
                                        TRUNC(SYSDATE)
                                                );

          INSERT INTO po.po_distributions_interface
          (interface_header_id
          ,interface_line_id
          ,interface_distribution_id
          ,org_id
          ,distribution_num
          ,quantity_ordered
          ,destination_organization
          , deliver_to_location_id
          ,charge_account_id
          ,accrual_account_id
          ,variance_account_id
          ,creation_date
          ,created_by
          ,last_update_date
          ,last_updated_by
          )
          VALUES (po.po_headers_interface_s.CURRVAL
          ,  po.po_lines_interface_s.CURRVAL
          ,  po.po_distributions_interface_s.NEXTVAL
          ,  162
          ,  '1'
          ,  l1.onhand_qty
          ,  l_organization_code
          ,  l_ship_to
          ,  6311
          ,  7641
          ,  9539
          ,  SYSDATE
          ,  -1
          ,  SYSDATE
          ,  -1
          );



                 UPDATE xxwc.xxwc_po_conv_stg_tbl
                    SET status = 'S'
                  WHERE organization_code = L1.organization_code
                    AND partnumber = L1.partnumber;

              ELSE
                 UPDATE xxwc.xxwc_po_conv_stg_tbl
                    SET ERROR_MESSAGE = l_err_message,
                        status = 'E'
                  WHERE organization_code = L1.organization_code
                    AND partnumber = L1.partnumber;
              END IF;
           END LOOP;
        ELSE
          UPDATE xxwc.xxwc_po_conv_stg_tbl
             SET error_message = l_err_message,
                 status = 'E'
           WHERE organization_code = H1.organization_code;
        END IF;

   COMMIT;
   END LOOP;
EXCEPTION
  WHEN OTHERS THEN
    dbms_output.put_line('Error - '||SQLERRM);
END main;

PROCEDURE receive_po(p_errbuf       OUT VARCHAR2
                    , p_retcode      OUT VARCHAR2
                    , p_po_num IN NUMBER) IS

  CURSOR cur_po
      IS
  SELECT distinct poh.po_header_id
         , poh.vendor_id
         , poh.segment1
         , poh.org_id
    FROM xxwc.xxwc_po_conv_stg_tbl stg
       , po_line_locations_all pll
       , mtl_parameters mp
       , po_headers_all poh
   WHERE mp.organization_id        = pll.ship_to_organization_id 
     AND stg.organization_code     = mp.organization_code
     AND poh.po_header_id          = pll.po_header_id
     AND poh.approved_flag         = 'Y'
     AND NVL(poh.cancel_flag, 'N') = 'N'
  UNION
  SELECT po_header_id
       , vendor_id
       , segment1
       , org_id
    FROM po_headers_all
   WHERE 1                     = 1
     AND segment1              = p_po_num
     AND approved_flag         = 'Y'
     AND NVL(cancel_flag, 'N') = 'N';

  CURSOR cur_po_line (p_po_header_id IN NUMBER)
      IS
  SELECT pl.org_Id
       , pl.po_header_id
       , pl.item_id
       , pl.po_line_id
       , pl.line_num
       , pll.quantity
       , pl.unit_meas_lookup_code
       , mp.organization_code
       , pll.line_location_id
       , pll.closed_code
       , pll.quantity_received
       , pll.cancel_flag
       , pll.shipment_num
       , pda.destination_type_code
       , pda.deliver_to_person_id
       , pda.deliver_to_location_id
       , pda.destination_subinventory
       , pda.destination_organization_id
       , msib.segment1 item_number
    FROM po_lines_all              pl
       , po_line_locations_all     pll
       , mtl_parameters            mp
       , apps.po_distributions_all pda
       , mtl_system_items_b        msib
   WHERE pl.po_header_id             = p_po_header_id
     AND pl.po_line_id               = pll.po_line_id
     AND pll.line_location_id        = pda.line_location_id
     AND pll.ship_to_organization_id = mp.organization_id
     AND msib.organization_id = mp.organization_id
     AND msib.inventory_item_id = pl.item_id;

  CURSOR cur_lot(p_org_code IN VARCHAR2, p_item IN VARCHAR2)
  IS
  SELECT lot_number
       , transaction_quantity
       , lot_expiration_date
    FROM xxwc_onhand_lot_cnv
   WHERE organization_code=p_org_code
     AND item_segment= p_item
     -- AND rownum=1
     ;


  x_user_id              NUMBER := 1296;
  x_resp_id              NUMBER := 20678;
  x_appl_id              NUMBER := 222;
  x_po_header_id         NUMBER;
  x_vendor_id            NUMBER := 45135;
  x_segment1             VARCHAR2 (20);
  x_org_id               NUMBER;
  x_line_num             NUMBER;
  l_chr_lot_number       VARCHAR2 (50);
  l_po_line_bal_qty      NUMBER;
  l_chr_return_status    VARCHAR2 (2000);
  l_num_msg_count        NUMBER;
  l_chr_msg_data         VARCHAR2 (50);
  v_count                NUMBER;
  l_po_header_id         NUMBER;

BEGIN
  DBMS_OUTPUT.put_line ('RCV Sample Insert Script Starts');
  DBMS_OUTPUT.put_line ('**************************************');

  FOR rec_po IN cur_po LOOP -- PO Loop Start >

/*
    SELECT DISTINCT u.user_id,
           to_char(a.responsibility_id) responsibility_id,
           b.application_id
      INTO x_user_id
         , x_resp_id
         , x_appl_id
      FROM apps.fnd_user_resp_groups_direct a
         , apps.fnd_responsibility_vl b
         , apps.fnd_user u
         , apps.fnd_application fa
     WHERE a.user_id                       = u.user_id
       and a.responsibility_id             = b.responsibility_id
       and a.responsibility_application_id = b.application_id
       and sysdate between a.start_date and nvl(a.end_date,sysdate+1)
       and fa.application_id (+)           = b.application_id
       and upper(u.user_name)              = 'A42485' -- Enter the User_name
       and b.responsibility_name           = 'Inventory'; -- Enter The Responsibility Name
*/

   DBMS_OUTPUT.put_line ('Inserting the Record into Rcv_headers_interface');
   DBMS_OUTPUT.put_line ('*********************************************');

    INSERT INTO rcv_headers_interface(header_interface_id
                                    , GROUP_ID
                                    , processing_status_code
                                    , receipt_source_code
                                    , transaction_type
                                    , last_update_date
                                    , last_updated_by
                                    , last_update_login
                                    , creation_date
                                    , created_by
                                    , vendor_id
                                    , expected_receipt_date
                                    , validation_flag)
    SELECT rcv_headers_interface_s.NEXTVAL
         , rcv_interface_groups_s.NEXTVAL
         , 'PENDING'
         , 'VENDOR'
         , 'NEW'
         , SYSDATE
         , x_user_id
         , 0
         , SYSDATE
         , x_user_id
         , x_vendor_id
         , SYSDATE
         , 'Y'
      FROM DUAL;

    l_po_header_id := rec_po.po_header_id;
    FOR rec_det IN cur_po_line(l_po_header_id) LOOP -- PO Line Loop Start >
      IF rec_det.closed_code IN ('APPROVED', 'OPEN', 'CLOSED FOR INVOICE') -- AND rec_det.quantity_received > 0 -- Version 1.1
      THEN
        DBMS_OUTPUT.put_line ('Inserting the Record into Rcv_Transactions_Interface');
        DBMS_OUTPUT.put_line ('*********************************************');

        INSERT INTO rcv_transactions_interface(interface_transaction_id
                                             , GROUP_ID
                                             , last_update_date
                                             , last_updated_by
                                             , creation_date
                                             , created_by
                                             , last_update_login
                                             , transaction_type
                                             , transaction_date
                                             , processing_status_code
                                             , processing_mode_code
                                             , transaction_status_code
                                             , po_header_id
                                             , po_line_id
                                             , item_id
                                             , quantity
                                             , unit_of_measure
                                             , po_line_location_id
                                             , auto_transact_code
                                             , receipt_source_code
                                             , vendor_id
                                             , to_organization_code
                                             , source_document_code
                                             , document_num
                                             , destination_type_code
                                             , deliver_to_person_id
                                             , deliver_to_location_id
                                             , subinventory
                                             , header_interface_id
                                             , validation_flag)
                                        SELECT rcv_transactions_interface_s.NEXTVAL
                                             , rcv_interface_groups_s.CURRVAL
                                             , SYSDATE
                                             , x_user_id
                                             , SYSDATE
                                             , x_user_id
                                             , 0
                                             , 'RECEIVE'
                                             , SYSDATE
                                             , 'PENDING'
                                             , 'BATCH'
                                             , 'PENDING'
                                             , rec_det.po_header_id
                                             , rec_det.po_line_id
                                             , rec_det.item_id
                                             , rec_det.quantity
                                             , rec_det.unit_meas_lookup_code
                                             , rec_det.line_location_id
                                             , 'DELIVER'
                                             , 'VENDOR'
                                             , x_vendor_id
                                             , rec_det.organization_code
                                             , 'PO'
                                             , x_segment1
                                             , rec_det.destination_type_code
                                             , rec_det.deliver_to_person_id
                                             , rec_det.deliver_to_location_id
                                             , 'General'
                                             , rcv_headers_interface_s.CURRVAL
                                             , 'Y'
                                          FROM DUAL;

        DBMS_OUTPUT.put_line ('PO line:'||rec_det.line_num||' Shipment: '||rec_det.shipment_num||' has been inserted into ROI.');

        SELECT count(*)
          INTO v_count
          FROM mtl_system_items
         WHERE inventory_item_id = rec_det.item_id
           AND lot_control_code = 2 -- 2 - full_control, 1 - no control
           AND organization_id = rec_det.destination_organization_id;

        IF v_count > 0 THEN
           DBMS_OUTPUT.put_line ('The Ordered Item is Lot Controlled');
           DBMS_OUTPUT.put_line ('Generate the Lot Number for the Lot Controlled Item');

           BEGIN
           -- initialization required for R12
           mo_global.set_policy_context ('S', rec_det.org_id);
           mo_global.init ('INV');

           -- Initialization for Organization_id
           inv_globals.set_org_id (rec_det.destination_organization_id);
           -- initialize environment

           fnd_global.apps_initialize (user_id => x_user_id,
                                       resp_id => x_resp_id,
                                  resp_appl_id => x_appl_id);

           DBMS_OUTPUT.put_line ('Calling inv_lot_api_pub.auto_gen_lot API to Create Lot Numbers');
           DBMS_OUTPUT.put_line ('*********************************************');

           l_chr_lot_number := inv_lot_api_pub.auto_gen_lot(p_org_id => rec_det.destination_organization_id,
                                                            p_inventory_item_id => rec_det.item_id,
                                                            p_parent_lot_number => NULL,
                                                            p_subinventory_code => NULL,
                                                            p_locator_id => NULL,
                                                            p_api_version => 1.0,
                                                            p_init_msg_list => 'F',
                                                            p_commit => 'T',
                                                            p_validation_level => 100,
                                                            x_return_status => l_chr_return_status,
                                                            x_msg_count => l_num_msg_count,
                                                            x_msg_data => l_chr_msg_data);

           IF l_chr_return_status = 'S' THEN
              COMMIT;
           ELSE
              ROLLBACK;
           END IF;

           DBMS_OUTPUT.put_line ('Lot Number Created for the item is => '|| l_chr_lot_number);
           END;

           DBMS_OUTPUT.put_line ('Inserting the Record into mtl_transaction_lots_interface ');
           DBMS_OUTPUT.put_line ('*********************************************');
/*
           INSERT INTO mtl_transaction_lots_interface( transaction_interface_id,
                                                last_update_date,
                                                last_updated_by,
                                                creation_date,
                                                created_by,
                                                last_update_login,
                                                lot_number,
                                                transaction_quantity,
                                                primary_quantity,
                                                serial_transaction_temp_id,
                                                product_code,
                                                lot_expiration_date,
                                                product_transaction_id)
                                        (SELECT mtl_material_transactions_s.nextval,--transaction_interface_id
                                                sysdate, --last_update_date
                                                x_user_id, --last_updated_by
                                                sysdate, --creation_date
                                                x_user_id, --created_by
                                                -1, --last_update_login
                                                l_chr_lot_number, --lot_number
                                                rec_det.quantity, --transaction_quantity
                                                rec_det.quantity, --primary_quantity
                                                NULL, --serial_transaction_temp_id
                                                'RCV', --product_code
                                                SYSDATE + 365,
                                                rcv_transactions_interface_s.currval --product_transaction_id
                                           FROM dual);
*/
          l_po_line_bal_qty := rec_det.quantity;

          FOR rec_lot IN cur_lot(rec_det.organization_code, rec_det.item_number) LOOP
              IF l_po_line_bal_qty = 0 THEN
                EXIT;
              END IF;

              IF l_po_line_bal_qty > rec_lot.transaction_quantity THEN
                 l_po_line_bal_qty := l_po_line_bal_qty - rec_lot.transaction_quantity;
              ELSE
                 l_po_line_bal_qty := 0;
              END IF;
                 INSERT INTO mtl_transaction_lots_interface( transaction_interface_id,
                                                      last_update_date,
                                                      last_updated_by,
                                                      creation_date,
                                                      created_by,
                                                      last_update_login,
                                                      lot_number,
                                                      transaction_quantity,
                                                      primary_quantity,
                                                      serial_transaction_temp_id,
                                                      product_code,
                                                      lot_expiration_date,
                                                      product_transaction_id)
                                              (SELECT mtl_material_transactions_s.nextval,--transaction_interface_id
                                                      sysdate,                            --last_update_date
                                                      x_user_id,                          --last_updated_by
                                                      sysdate,                            --creation_date
                                                      x_user_id,                          --created_by
                                                      -1,                                 --last_update_login
                                                      rec_lot.lot_number,                 --lot_number
                                                      rec_lot.transaction_quantity,       --transaction_quantity
                                                      rec_lot.transaction_quantity,       --primary_quantity
                                                      NULL,                               --serial_transaction_temp_id
                                                      'RCV',                              --product_code
                                                      rec_lot.lot_expiration_date,        --lot_expiration_date
                                                      rcv_transactions_interface_s.currval --product_transaction_id
                                                 FROM dual);
          END LOOP;
        ELSE
          DBMS_OUTPUT.put_line ('The Ordered Item is Not Lot Controlled');
          DBMS_OUTPUT.put_line ('********************************************');
        END IF;
      ELSE
        DBMS_OUTPUT.put_line ( 'PO line ' ||rec_det.line_num||'-'|| rec_det.shipment_num|| ' is either closed, cancelled, received.');
        DBMS_OUTPUT.put_line ('*********************************************');
      END IF;
    END LOOP; -- PO Line Loop End <
    END LOOP; -- PO Loop End <
    DBMS_OUTPUT.put_line ('RCV Sample Insert Script Ends');
    DBMS_OUTPUT.put_line ('*****************************************');
    COMMIT;
END RECEIVE_PO;

END XXWC_PO_CONV_PKG;
