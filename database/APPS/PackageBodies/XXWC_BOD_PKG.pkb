create or replace 
PACKAGE BODY XXWC_BOD_PKG IS

  /*************************************************************************
 *   Procedure : Write_Log
 *
 *   PURPOSE:   This procedure logs error message
 *   Parameter:
 *          IN
 *              p_debug_msg      -- Debug Message
 * ************************************************************************/
   -- add debug message to log table and concurrent log file
   PROCEDURE Write_Log (p_debug_level   IN NUMBER,
                        p_mod_name      IN VARCHAR2,
                        p_debug_msg     IN VARCHAR2)
   IS
      PRAGMA AUTONOMOUS_TRANSACTION;
   BEGIN
      --IF (p_debug_level >= FND_LOG.G_CURRENT_RUNTIME_LEVEL)
      --THEN
      -- Call Oracle FND_LOG API to insert the debug message
      FND_LOG.STRING (p_debug_level, p_mod_name, P_DEBUG_MSG);
      -- END IF;
      fnd_file.put_line (fnd_file.LOG, P_DEBUG_MSG);
      --Write_output_OUTPUT.put_line (P_DEBUG_MSG);
      COMMIT;
   END Write_Log;

  /*************************************************************************
 *   Procedure : Write_Error
 *
 *   PURPOSE:   This procedure logs error message
 *   Parameter:
 *          IN
 *              p_debug_msg      -- Debug Message
 * ************************************************************************/

   --add message to concurrent output file
   PROCEDURE Write_Error (p_debug_msg IN VARCHAR2)
   IS
      l_req_id          NUMBER := fnd_global.conc_request_id;
      l_err_callfrom    VARCHAR2 (75) DEFAULT G_PACKAGE_NAME ;
      l_err_callpoint   VARCHAR2 (75) DEFAULT 'START' ;
      l_distro_list VARCHAR2 (75)
            DEFAULT 'HDSOracleDevelopers@hdsupply.com'  ;
   BEGIN
      xxcus_error_pkg.xxcus_error_main_api (
         p_called_from         => l_err_callfrom,
         p_calling             => l_err_callpoint,
         p_request_id          => l_req_id,
         p_ora_error_msg       => SUBSTR (p_debug_msg, 1, 2000),
         p_error_desc          => 'Error running '|| G_PACKAGE_NAME ||' with PROGRAM ERROR',
         p_distribution_list   => l_distro_list,
         p_module              => 'INV'
      );
   END Write_Error;

   /*************************************************************************
    *   Procedure : Write_output
    *
    *   PURPOSE:   This procedure logs message into concurrent output
    *   Parameter:
    *          IN
    *              p_debug_msg      -- Debug Message
    * ************************************************************************/
   PROCEDURE Write_output (p_debug_msg IN VARCHAR2)
   IS
   BEGIN
      fnd_file.put_line (fnd_file.output, p_debug_msg);
   END Write_output;

   /*************************************************************************
    *   Procedure : Write_log_output
    *
    *   PURPOSE:   This procedure logs message into concurrent logfile
    *   Parameter:
    *          IN
    *              p_debug_msg      -- Debug Message
    * ************************************************************************/

   PROCEDURE Write_log_output (p_debug_msg IN VARCHAR2)
   IS
   BEGIN
    
    IF g_write_log = 'Y' THEN
     fnd_file.put_line (fnd_file.log, p_debug_msg);
    END IF;
   END write_log_output;


  PROCEDURE master_program
      (p_inventory_item_id  IN NUMBER
      ,p_organization_id    IN NUMBER
      ,p_transaction_id     IN NUMBER
      ,p_onhand_quantities_id  IN NUMBER
      ,p_transaction_date   IN OUT DATE
      ,p_onhand_quantity    IN OUT NUMBER
      ,p_row_id             IN VARCHAR2) 
      
    IS

      x_rem_quantity NUMBER DEFAULT NULL;
      x_date         DATE;
      x_transaction_id  NUMBER;
      x_transaction_type_id NUMBER;
      x_result    VARCHAR2(10);
      x_transfer_organization_id NUMBER;
      
      x_bod DATE;
      x_quantity NUMBER;

      l_qty NUMBER DEFAULT 0;
      l_allocated_qty NUMBER DEFAULT 0;
      l_total_qty NUMBER DEFAULT 0;
      l_balance_qty NUMBER DEFAULT 0;
      l_organization_id NUMBER;
      l_transaction_date DATE;
      
      l_count NUMBER DEFAULT 0;
      l_loop_count NUMBER DEFAULT 0;
      l_lot_count NUMBER DEFAULT 0;
      l_lot_count_total NUMBER DEFAULT 0;
      l_txn_count NUMBER DEFAULT 0;
      l_txn_count_total NUMBER DEFAULT 0;
        
      l_original_date DATE;
      
      --l_sequence_number NUMBER;
      
      
      CURSOR c_item IS
        SELECT p_inventory_item_id inventory_item_Id,
               p_organization_id organization_id,
               p_transaction_id transaction_id,
               p_onhand_quantities_id onhand_quantities_id,
               p_transaction_date transaction_date,
               p_onhand_quantity onhand_quantity
        from dual;
        
      l_original_transaction_id NUMBER;
      l_original_quantity NUMBER;
      
    BEGIN
    
    /*
    BEGIN
      select xxwc.xxwc_bod_s.nextval into l_sequence_number from dual;
    END;
    */
    
    xxwc_bod_pkg.insert_into_moqd_temp
      (p_inventory_item_id => p_inventory_item_id
      ,p_organization_id   => p_organization_id
      ,p_onhand_quantities_id => p_onhand_quantities_id
      --,p_sequence_number => l_sequence_number
      ,p_row_id   => p_row_id);
    
    
    --set l_original_date to p_transaction_date parameter
    l_original_date := p_transaction_date;
    l_original_transaction_id := p_transaction_id;
    l_original_quantity := p_onhand_quantity;
    
    Write_log_output('--------------Starting------------------');
      
    xxwc_bod_pkg.get_item_org(p_inventory_item_id, p_organization_id);
    
    write_log_output('p_onhand_quantities_id : ' || p_onhand_quantities_id); 
    
    
    write_log_output('-------Start Transaction Search-------------------'); 
    
    l_count := 0;
    l_loop_count := 0;
    l_lot_count := 0;
    l_txn_count := 0;
    
    <<SECTION1>>
    
    
    FOR r_item IN c_item
    loop 
    
      
    write_log_output('---Loop '||l_loop_count||'----------'); 
    
     IF x_result = 'STOP'  THEN
        exit;
     ELSE 
    
      if x_rem_quantity is null then
      
        l_qty := p_onhand_quantity;
        
      ELSE 
      
        l_qty := l_allocated_qty;
        
      END IF;
     
    
      IF x_result = 'XFER' THEN
      
        l_organization_id := x_transfer_organization_id;
        l_transaction_date := x_date;
        l_count := l_count + 1;
          
      ELSE
      
        l_organization_id := p_organization_id;
        l_transaction_date := nvl(x_date, p_transaction_date);
        
      END IF;
    
    xxwc_bod_pkg.get_item_org(p_inventory_item_id, l_organization_id);
    
    
    xxwc_bod_pkg.transaction_history
      (p_inventory_item_id  => r_item.inventory_item_id
      ,p_organization_id    => l_organization_id
      ,p_transaction_id     => r_item.transaction_id
      ,p_transaction_date   => l_transaction_date
      ,p_quantity           => l_qty
      ,x_rem_quantity       => x_rem_quantity
      ,x_date               => x_date
      ,x_transaction_type_id => x_transaction_type_id
      ,x_transaction_id     => x_transaction_id
      ,x_transfer_organization_id => x_transfer_organization_id);

  
    review_output
          (p_quantity    => x_rem_quantity
          ,p_date        => x_date
          ,p_transaction_type_id  => x_transaction_type_id
          ,p_transaction_id => x_transaction_id
          ,x_result => x_result
          ,x_lot_count => l_lot_count
          ,x_txn_count => l_txn_count);

      l_lot_count_total := l_lot_count + l_lot_count_total;
      l_txn_count_total := l_txn_count + l_txn_count_total;
     
    END IF;
  
  
    IF x_rem_quantity >= 0 THEN
    
        l_allocated_qty := l_qty;
        
    ELSE
    
        l_allocated_qty := l_qty + x_rem_quantity;
        
    END IF;
    
       Write_log_output('l_allocated_qty : ' || l_allocated_qty);
  
    IF x_result != 'XFER' THEN
    
      l_total_qty := l_allocated_qty + l_total_qty;
  
      
      l_balance_qty := l_total_qty - p_onhand_quantity;
      
    ELSE
    
      l_total_qty := l_total_qty;
      
      l_balance_qty := l_total_qty - p_onhand_quantity;

    END IF;

      Write_log_output('l_total_qty : ' || l_total_qty);
    
      Write_log_output('l_balance_qty : ' || l_balance_qty);
  
  
    END LOOP;
  
    IF x_result = 'XFER' THEN
      
      
      IF l_count = 10 THEN
      
        NULL;
      
      ELSE
      
      p_transaction_date := x_date;
      l_loop_count := l_loop_count + 1;
      Write_log_output('l_loop_count : ' || l_loop_count);
      
      GOTO SECTION1;
    
      END IF;
    
    if l_balance_qty > 0 then
    
      p_onhand_quantity := l_balance_qty;
      p_transaction_date := x_date;
      
      IF l_count = 10 THEN
      
        NULL;
      
      ELSE
    
        Write_log_output('p_onhand_quantity : ' || l_balance_qty);
        
        --l_count := l_count + 1;
        
        GOTO SECTION1;
    
      END IF;
      
    END IF;
      
    END IF;
  
  
      Write_log_output('x_transaction_id : ' || x_transaction_id);
  
      xxwc_bod_pkg.get_receipt
      (p_transaction_id => x_transaction_id
      ,x_bod => x_bod
      ,x_quantity => x_quantity);

    Write_log_output('p_onhand_quantity : ' || p_onhand_quantity);
  
    Write_log_output('--------------End Transaction Search------------------');
    
  INSERT INTO xxwc.xxwc_bod_temp
      (inventory_item_id
      ,organization_Id
      ,update_transaction_id
      ,create_transaction_id
      ,primary_transaction_quantity
      ,onhand_quantities_id
      ,orig_date_received
      ,creation_date
      --,sequence_number
      ,moqd_rowid
      ,loop_count
      ,lot_count
      ,txn_count)
  VALUES
      (p_inventory_item_id
      ,p_organization_id
      ,NULL
      ,NULL
      ,p_onhand_quantity
      ,p_onhand_quantities_id
      ,x_bod
      ,l_original_date
      --,l_sequence_number
      ,p_row_id
      ,l_count
      ,l_lot_count_total
      ,l_txn_count_total);
        
     
    END;
    
  PROCEDURE transaction_history
      (p_inventory_item_id  IN NUMBER
      ,p_organization_id    IN NUMBER
      ,p_transaction_id     IN NUMBER
      ,p_transaction_date   IN DATE
      ,p_quantity           IN NUMBER
      ,x_rem_quantity       OUT NUMBER
      ,x_date               OUT DATE
      ,x_transaction_type_id  OUT NUMBER
      ,x_transaction_id     OUT NUMBER
      ,x_transfer_organization_id OUT NUMBER) IS
      
     l_transaction_id NUMBER;
     l_quantity       NUMBER;
     l_date           DATE;
     l_rem_quantity   NUMBER;
     l_transfer_organization_id NUMBER;
     l_transfer_transaction_id NUMBER;
     l_transaction_type_id NUMBER;
     
     l_receipt_transaction_id NUMBER;
     l_xfer_transaction_id NUMBER;
     l_alias_receipt_id NUMBER;
     l_wip_completion_id NUMBER;
     l_intransit_receipt_id NUMBER;
     l_cycle_count_adjust_id NUMBER;
     l_rma_receipt_id NUMBER;
     l_transaction_type_name VARCHAR2(80);

     l_transaction_date DATE;
     l_source_code VARCHAR2(30);
     l_count_transaction_id NUMBER;
     
BEGIN

    --PO Receipt Transaction Id      

    BEGIN
      SELECT transaction_type_id
      INTO   l_receipt_transaction_id
      FROM   mtl_transaction_types
      where  transaction_type_name = 'PO Receipt';
    END;


    --Int Req Intr Rcpt Transaction ID
    BEGIN
      SELECT transaction_type_id
      INTO   l_xfer_transaction_id
      FROM   mtl_transaction_types
      where  transaction_type_name = 'Int Req Intr Rcpt';
    END;

  --Account Alias Receipt
    BEGIN
      SELECT transaction_type_id
      INTO   l_alias_receipt_id
      FROM   mtl_transaction_types
      where  transaction_type_name = 'Account alias receipt (Inventory Type Adjustment In)';
    END;

  --WIP Completion
    BEGIN
      SELECT transaction_type_id
      INTO   l_wip_completion_id
      FROM   mtl_transaction_types
      where  transaction_type_name = 'WIP Completion';
    END;

  --Intransit Receipt

    BEGIN
      SELECT transaction_type_id
      INTO   l_intransit_receipt_id
      FROM   mtl_transaction_types
      WHERE  transaction_type_name = 'Intransit Receipt';
    END;


      BEGIN
        SELECT transaction_type_id 
        INTO   l_cycle_count_adjust_id
        FROM   mtl_transaction_types
        WHERE  transaction_type_name = 'Cycle Count Adjust';
     END;
     
     BEGIN
        SELECT transaction_type_id
        INTO   l_rma_receipt_id
        FROM   mtl_transaction_types
        WHERE  transaction_type_name = 'RMA Receipt';
      END;
  

    --Find Last PO or Transfer Transaction
    BEGIN
      SELECT primary_quantity,
             to_date(nvl(to_date(attribute15,'DD-MON-RR'),transaction_date)),
             transfer_transaction_id,
             transfer_organization_id,
             transaction_type_id,
             transaction_id
      INTO   l_quantity,
             l_date,
             l_transfer_transaction_id,
             l_transfer_organization_id,
             l_transaction_type_id,
             l_transaction_id
      FROM   mtl_material_transactions
      WHERE  inventory_item_id = p_inventory_item_id
      AND    organization_id = p_organization_id
      AND    transaction_date <= p_transaction_date
      AND    transaction_id = p_transaction_id
      AND    transaction_type_id IN (l_receipt_transaction_id, l_xfer_transaction_id,l_alias_receipt_id, l_wip_completion_id,l_intransit_receipt_id, l_cycle_count_adjust_id, l_rma_receipt_id)
      AND    nvl(to_date(attribute15,'DD-MON-RR'),transaction_date) = (SELECT MAX(nvl(to_date(attribute15,'DD-MON-RR'),transaction_date))
                                FROM   mtl_material_transactions 
                                WHERE  inventory_item_id = p_inventory_item_id
                                AND    organization_id = p_organization_id
                                AND    transaction_date <= p_transaction_date
                                AND    transaction_id <= p_transaction_id
                                AND    transaction_type_id IN (l_receipt_transaction_id, l_xfer_transaction_id,l_alias_receipt_id,  l_wip_completion_id,l_intransit_receipt_id, l_cycle_count_adjust_id, l_rma_receipt_id))
      AND   rownum = 1;
      
      EXCEPTION
          WHEN NO_DATA_FOUND THEN
            
      BEGIN          
        SELECT primary_quantity,
               to_date(nvl(to_date(attribute15,'DD-MON-RR'),transaction_date)),
               transfer_transaction_id,
               transfer_organization_id,
               transaction_type_id,
               transaction_id
        INTO   l_quantity,
               l_date,
               l_transfer_transaction_id,
               l_transfer_organization_id,
               l_transaction_type_id,
               l_transaction_id
        FROM   mtl_material_transactions
        WHERE  inventory_item_id = p_inventory_item_id
        AND    organization_id = p_organization_id
        AND    transaction_date <= p_transaction_date
        AND    transaction_id < p_transaction_id
        AND    transaction_type_id IN (l_receipt_transaction_id, l_xfer_transaction_id,l_alias_receipt_id,  l_wip_completion_id,l_intransit_receipt_id, l_cycle_count_adjust_id, l_rma_receipt_id)
        AND    nvl(to_date(attribute15,'DD-MON-RR'),transaction_date) = (SELECT MAX(nvl(to_date(attribute15,'DD-MON-RR'),transaction_date))
                                  FROM   mtl_material_transactions 
                                  WHERE  inventory_item_id = p_inventory_item_id
                                  AND    organization_id = p_organization_id
                                  AND    transaction_date <= p_transaction_date
                                  AND    transaction_id <= p_transaction_id
                                  AND    transaction_type_id IN (l_receipt_transaction_id, l_xfer_transaction_id,l_alias_receipt_id,l_wip_completion_id,l_intransit_receipt_id, l_cycle_count_adjust_id, l_rma_receipt_id))
        AND   ROWNUM = 1
        ;
      EXCEPTION
      
        WHEN OTHERS THEN
          
          x_rem_quantity :=  p_quantity;
          x_transaction_type_id := NULL;
          x_date  := p_transaction_date;
          x_transfer_organization_id := NULL;
          x_transaction_id := NULL;
              
      END;
    
    WHEN OTHERS THEN
      x_rem_quantity :=  l_quantity - p_quantity;
      x_transaction_type_id := l_transaction_type_id;
      x_date  := l_date;
      x_transfer_organization_id := l_transfer_organization_id;
      x_transaction_id := l_transaction_id;
    END;
    
      IF l_transfer_transaction_id IS NOT NULL THEN
      
          SELECT transaction_date
          INTO   l_date
          FROM   mtl_material_transactions
          WHERE  transaction_id = l_transfer_transaction_id;
          
      END IF;
      
      --IF Transaction Type ID is Account Alias Receipt and there a
 
     
      IF l_transaction_type_id = l_alias_receipt_id THEN
      
            --Get the transaction_date of the transaction_id
           
            BEGIN
            
                select source_code, transaction_date
                into   l_source_code, l_transaction_date
                from   mtl_material_transactions
                where  transaction_id = l_transaction_id;
            
            EXCEPTION
            
                when others then
                
                        l_source_code := NULL;
                        
             END;
            
            Write_log_output('l_source_code : ' || l_source_code);
            
            if l_source_code = 'CONVERSION' THEN
            
            Write_log_output('Current date is ' || l_date);
            
              BEGIN
               
               SELECT  primary_quantity,
                       to_date(nvl(to_date(attribute15,'DD-MON-RR'),transaction_date)),
                       transfer_transaction_id,
                       transfer_organization_id,
                       transaction_type_id,
                       transaction_id
                INTO   l_quantity,
                       l_date,
                       l_transfer_transaction_id,
                       l_transfer_organization_id,
                       l_transaction_type_id,
                       l_transaction_id
                FROM   mtl_material_transactions
                WHERE  inventory_item_id = p_inventory_item_id
                AND    organization_id = p_organization_id
                AND    source_code = l_source_code
                AND    transaction_type_id IN (l_alias_receipt_id)
                AND    nvl(to_date(attribute15,'DD-MON-RR'),transaction_date) = (SELECT MIN(nvl(to_date(attribute15,'DD-MON-RR'),transaction_date))
                                          FROM   mtl_material_transactions 
                                          WHERE  inventory_item_id = p_inventory_item_id
                                          AND    organization_id = p_organization_id
                                          and    source_code = l_source_code
                                          AND    transaction_type_id IN (l_alias_receipt_id));
                
               exception
               
                 when others then  
                      x_rem_quantity :=  p_quantity;
                      x_transaction_type_id := NULL;
                      x_date  := p_transaction_date;
                      x_transfer_organization_id := NULL;
                      x_transaction_id := NULL;
              END;               
                                                
            END IF;
                  
        
      END IF;
        
  
      x_rem_quantity :=  l_quantity - p_quantity;
      x_transaction_type_id := l_transaction_type_id;
      x_date  := l_date;
      x_transfer_organization_id := l_transfer_organization_id;
      x_transaction_id := l_transaction_id;
    
      Write_log_output('l_transaction_id : ' || l_transaction_id);
      Write_log_output('l_primary_transaction_quantity : ' || l_quantity);
      Write_log_output('l_date : ' || l_date);
      Write_log_output('l_transfer_transaction_id : ' || l_transfer_transaction_id);
      Write_log_output('l_transfer_organization_id : ' || l_transfer_organization_id);
      Write_log_output('l_transaction_type_id : ' || l_transaction_type_id);
      Write_log_output('x_rem_quantity : ' || x_rem_quantity);
    
    IF l_transaction_type_id is not null then 
      
      BEGIN
        select transaction_type_name
        INTO   l_transaction_type_name
        FROM   mtl_transaction_types
        WHERE  transaction_type_id = l_transaction_type_id;
      END;
      
      write_log_output('l_transaction_type_name : ' || l_transaction_type_name);
    
    END IF;

  END;

  PROCEDURE review_output
          (p_quantity    IN NUMBER
          ,p_date        IN DATE
          ,p_transaction_type_id  IN NUMBER
          ,p_transaction_id IN NUMBER
          ,x_result OUT VARCHAR2
          ,x_lot_count OUT NUMBER
          ,x_txn_count OUT NUMBER) IS
    
    l_result VARCHAR2(10);
    l_receipt_transaction_id NUMBER;
    l_xfer_transaction_id NUMBER;
    l_alias_receipt_id NUMBER;
    l_wip_completion_id NUMBER;
    l_intransit_receipt_id NUMBER;
    l_cycle_count_adjust_id NUMBER;
    l_rma_receipt_id NUMBER;
    l_lot_count number;
    l_txn_count number;
    
    
  BEGIN
    
    --PO Receipt Transaction Id      

    BEGIN
      SELECT transaction_type_id
      INTO   l_receipt_transaction_id
      FROM   mtl_transaction_types
      where  transaction_type_name = 'PO Receipt';
    END;


    --Int Req Intr Rcpt Transaction ID
    BEGIN
      SELECT transaction_type_id
      INTO   l_xfer_transaction_id
      FROM   mtl_transaction_types
      where  transaction_type_name = 'Int Req Intr Rcpt';
    END;

  --Account Alias Receipt
    BEGIN
      SELECT transaction_type_id
      INTO   l_alias_receipt_id
      FROM   mtl_transaction_types
      where  transaction_type_name = 'Account alias receipt (Inventory Type Adjustment In)';
    END;

  --WIP Completion
    BEGIN
      SELECT transaction_type_id
      INTO   l_wip_completion_id
      FROM   mtl_transaction_types
      where  transaction_type_name = 'WIP Completion';
    END;

  --Intransit Receipt

    BEGIN
      SELECT transaction_type_id
      INTO   l_intransit_receipt_id
      FROM   mtl_transaction_types
      WHERE  transaction_type_name = 'Intransit Receipt';
    END;

    BEGIN
      SELECT transaction_type_id
      INTO   l_cycle_count_adjust_id
      FROM   mtl_transaction_types
      WHERE  transaction_type_name = 'Cycle Count Adjust';
    END;
  
  
  BEGIN
    SELECT transaction_type_id
    INTO   l_rma_receipt_id
    FROM   mtl_transaction_types
    WHERE  transaction_type_name = 'RMA Receipt';
  END;
  
  
  IF p_transaction_type_id = l_receipt_transaction_id THEN
  
    IF p_quantity >= 0 THEN
    
       l_result := 'STOP';
       
    ELSE
    
        l_result := 'GO';
    
    END IF;
  
  ELSIF p_transaction_type_id =  l_alias_receipt_id THEN
    
    IF p_quantity >= 0 THEN
    
       l_result := 'STOP';
       
    ELSE
    
        l_result := 'GO';
    
    END IF;
    
  ELSIF p_transaction_type_id = l_xfer_transaction_id THEN
    
    IF p_quantity >= 0 THEN
    
       l_result := 'XFER';
       
    ELSE
    
        l_result := 'GO';
    
    END IF;
  
  ELSIF p_transaction_type_id = l_intransit_receipt_id THEN
    
    IF p_quantity >= 0 THEN
    
       l_result := 'XFER';
       
    ELSE
    
        l_result := 'GO';
    
    END IF;

  ELSIF p_transaction_type_id =  l_wip_completion_id THEN
    
    IF p_quantity >= 0 THEN
    
       l_result := 'STOP';
       
    ELSE
    
        l_result := 'GO';
    
    END IF;
  

  ELSIF p_transaction_type_id = l_cycle_count_adjust_id THEN
  
    IF p_quantity > 0 THEN
    
      l_result := 'STOP';
      
    ELSE
    
      l_result := 'GO';
      
    END IF;

  ELSIF p_transaction_type_id = l_rma_receipt_id THEN
  
    IF p_quantity > 0 THEN
    
      l_result := 'STOP';
      
    ELSE
    
      l_result := 'GO';
      
    END IF;


  ELSE
  
      l_result := 'STOP';
      
  END IF;
  
  BEGIN
    SELECT 1
    INTO   l_lot_count
    FROM   MTL_TRANSACTION_LOT_NUMBERS
    where  transaction_id = p_transaction_id;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
          L_LOT_COUNT := 0;
    WHEN OTHERS THEN
          L_LOT_COUNT := 0;
  END;
  
  
  BEGIN
    SELECT 1
    INTO   l_txn_count
    FROM   mtl_material_transactions
    where  transaction_id = p_transaction_id
    and    transaction_date between '28-MAR-2013' and '05-SEP-2013'; --Dates where BOD Extension didn't execute
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
          L_TXN_COUNT := 0;
    WHEN OTHERS THEN
          L_TXN_COUNT := 0;
  END;
  
  
  x_result := l_result;
  x_lot_count := l_lot_count;
  x_txn_count := l_txn_count;
    
  Write_log_output('l_result : ' || l_result);
  Write_log_output('x_lot_count : ' || l_lot_count);
  Write_log_output('x_txn_count :' || l_txn_count);
  END;
  
    PROCEDURE get_receipt
      (p_transaction_id IN NUMBER
      ,x_bod OUT DATE
      ,x_quantity OUT NUMBER) IS
      
      l_date DATE;
      l_quantity NUMBER;
      
    BEGIN
    
      BEGIN
        SELECT primary_quantity,
               to_date((nvl(to_date(attribute15,'DD-MON-RR'),transaction_date)))
        INTO   l_quantity,
               l_date
        FROM   mtl_material_transactions mmt
        WHERE  mmt.transaction_id = p_transaction_id;
    
      exception
          WHEN others THEN
              l_quantity := NULL;
              l_date := NULL;
      END;
    
    
    
      
        x_bod := l_date;
        x_quantity := l_quantity;
        
        Write_log_output('x_bod : ' || x_bod);
        Write_log_output('x_quantity : ' || x_quantity);
    
    END;

  PROCEDURE get_item_org
      (p_inventory_item_id IN NUMBER
      ,p_organization_id   IN NUMBER
      ) IS
    
    l_item_number VARCHAR2(40);
    l_org_code    VARCHAR2(3);
      
  BEGIN
  
    BEGIN
      SELECT mp.organization_code,
             msib.segment1
      INTO   l_org_code,
             l_item_number
      FROM   mtl_parameters mp,
             mtl_system_items_b msib
      WHERE  msib.organization_id = mp.organization_id
      AND    msib.inventory_item_id = p_inventory_item_id
      AND    mp.organization_id = p_organization_id;
    EXCEPTION
      WHEN OTHERS THEN
            l_org_code := NULL;
            l_item_number := NULL;
    END;
  
      Write_log_output('x_org_code : ' || l_org_code);
      Write_log_output('x_item_number : ' || l_item_number);
      Write_log_output('p_inventory_item_id : ' || p_inventory_item_id);
      Write_log_output('p_organization_id : ' || p_organization_id);
      Write_log_output('time_stamp : ' || to_char(sysdate,'DD-MON-YYYY HH:MM:SS'));
      
  END;
  
  PROCEDURE insert_into_moqd_temp
      (p_inventory_item_id IN NUMBER
      ,p_organization_id   IN NUMBER
      ,p_onhand_quantities_id IN NUMBER
      --,p_sequence_number  IN NUMBER
      ,p_row_id           IN VARCHAR2) IS
      
   BEGIN
   
   
    INSERT INTO xxwc.xxwc_moqd_temp
      (inventory_item_id
      ,organization_Id
      ,update_transaction_id
      ,create_transaction_id
      ,primary_transaction_quantity
      ,onhand_quantities_id
      ,orig_date_received
      ,creation_date
      --,sequence_number
      ,moqd_rowid)
      select inventory_item_id
            ,organization_Id
            ,update_transaction_id
            ,create_transaction_id
            ,primary_transaction_quantity
            ,onhand_quantities_id
            ,orig_date_received
            ,creation_date
            --,p_sequence_number
            ,p_row_id
      FROM  mtl_onhand_quantities_detail
      WHERE  inventory_item_id = p_inventory_item_id
      AND    organization_id = p_organization_id
      AND    onhand_quantities_id = p_onhand_quantities_id
      AND    rowid = p_row_id;

   END;
  
  
    PROCEDURE main_ccr
      (retbuf         OUT  VARCHAR2
      ,retcode        OUT  NUMBER
      ,p_organization_id IN NUMBER
      ,p_inventory_item_id IN NUMBER
      ,p_update IN VARCHAR2
      ,p_calculate_method IN VARCHAR2
      ,p_write_log IN VARCHAR2) IS
    
    
    
    CURSOR c_items IS
    SELECT inventory_item_id,
           organization_id,
           create_transaction_id transaction_id,
           onhand_quantities_id,
           creation_date transaction_date,
           primary_transaction_quantity quantity,
           ROWID row_id
     FROM  mtl_onhand_quantities_detail
     WHERE   organization_id = nvl(p_organization_id, organization_id)
     AND     inventory_item_id = nvl(p_inventory_item_id, inventory_item_id)
     ORDER BY organization_id, inventory_item_id;
  
  CURSOR c_distinct_items IS
    SELECT DISTINCT 
           inventory_item_id,
           organization_id,
           onhand_quantities_id
     FROM  mtl_onhand_quantities_detail
     WHERE   organization_id = nvl(p_organization_id, organization_id)
     AND     inventory_item_id = nvl(p_inventory_item_id, inventory_item_id);
  
      x_table_name VARCHAR2(30);
      
      BEGIN
      
        g_write_log := nvl(p_write_log,'N');  
      
        --Write_output('SEQUENCE_NUMBER|INVENTORY_ITEM_ID|ORGANIZATION_ID|ITEM_NUMBER|ORGANIZATION_CODE|ITEM_TYPE|ONHAND_QUANTITIES_ID|CALC_PRIMARY_TRANSACTION_QUANTITY|CAL_ORIG_RECEIPT_DATE|MOQD_PRIMARY_TRANSACTION_QUANTITY|MOQD_ORIG_RECEIPT_DATE');
        Write_output('INVENTORY_ITEM_ID|ORGANIZATION_ID|ITEM_NUMBER|ORGANIZATION_CODE|ITEM_TYPE|ONHAND_QUANTITIES_ID|CALC_PRIMARY_TRANSACTION_QUANTITY|CAL_ORIG_RECEIPT_DATE|MOQD_PRIMARY_TRANSACTION_QUANTITY|MOQD_ORIG_RECEIPT_DATE|LOOP_COUNT|LOT_COUNT|TXN_COUNT|GREATER_THAN|LESS_THAN|EQUAL|UPDATE');

        EXECUTE IMMEDIATE 'TRUNCATE TABLE XXWC.XXWC_MOQD_TEMP';
        EXECUTE IMMEDIATE 'TRUNCATE TABLE XXWC.XXWC_BOD_TEMP';
        EXECUTE IMMEDIATE 'TRUNCATE TABLE XXWC.XXWC_BOD_DETAIL_TEMP';                
          
      
        --If we are updating, create a backup table
        IF p_update = 'Y' THEN
        
                xxwc_bod_pkg.create_backup_table(p_organization_id,p_inventory_item_id,x_table_name);
        
        
        END IF;
        
      
       FOR r_items IN c_items
       
        LOOP
        
            IF NVL(P_CALCULATE_METHOD,'N') = 'N' THEN 
            
              xxwc_bod_pkg.master_program
              (p_inventory_item_id  => r_items.inventory_item_id
              ,p_organization_id    => r_items.organization_id
              ,p_transaction_id     => r_items.transaction_id
              ,p_onhand_quantities_id => r_items.onhand_quantities_id
              ,p_transaction_date   => r_items.transaction_date
              ,p_onhand_quantity    => r_items.quantity
              ,p_row_id => r_items.row_id);
             
            
             ELSE
                
                execute immediate 'TRUNCATE TABLE XXWC.XXWC_BOD_DETAIL_TEMP';                
          
                xxwc_bod_pkg.FIND_QTY
                (p_transaction_id => r_items.transaction_id
                ,p_inventory_item_id => r_items.inventory_item_id
                ,p_organization_id =>  r_items.organization_id
                ,p_onhand_quantities_id => r_items.onhand_quantities_id
                ,p_onhand_quantity => r_items.quantity
                ,p_row_id     => r_items.row_id);
            
            
                COMMIT;
            END IF;
                                
         END LOOP;
     
        FOR r_distinct_items IN c_distinct_items
        
        LOOP
        
          xxwc_bod_pkg.query_results (p_organization_id   => r_distinct_items.organization_id
                                         ,p_inventory_item_id => r_distinct_items.inventory_item_id
                                         ,p_onhand_quantities_id => r_distinct_items.onhand_quantities_id
                                         ,p_update => p_update);
    
       end loop;
    
        COMMIT;
        
        
      
          retcode := 0;
                
       END main_ccr;
  
  
      PROCEDURE QUERY_RESULTS
        (p_organization_id IN NUMBER
        ,p_inventory_item_id IN NUMBER
        ,p_onhand_quantities_id IN NUMBER
        ,p_update IN VARCHAR2)
      
      IS
      
      CURSOR c_results IS
      SELECT mp.organization_code,
             msib.segment1,
             msib.item_type,
             xbt.inventory_item_id, 
             xbt.organization_id,
             xbt.onhand_quantities_id, 
             --nvl(xbt.orig_date_received,xmt.orig_date_received) calc_orig_date_received,
             xbt.orig_date_received calc_orig_date_received,
             xbt.primary_transaction_quantity calc_transaction_quantity,
             --xbt.sequence_number,
             xbt.moqd_rowid,
             xbt.loop_count,
             xbt.lot_count,
             xbt.txn_count
      FROM  xxwc.xxwc_bod_temp xbt,
            mtl_system_items_b msib,
            mtl_parameters mp
      WHERE xbt.inventory_item_id = msib.inventory_item_id
      AND   xbt.organization_id = msib.organization_id
      AND   msib.organization_id = mp.organization_id
      AND   msib.inventory_item_id = p_inventory_item_id
      AND   msib.organization_id = p_organization_id
      AND   xbt.onhand_quantities_id = p_onhand_quantities_id
      ;
      
      l_moqd_transaction_quantity NUMBER;
      l_moqd_orig_date_received DATE;
      l_greater_than NUMBER;
      l_less_than NUMBER;
      l_equal NUMBER;
      l_update NUMBER;
      
      BEGIN
  
        
       FOR r_results IN c_results
       
        LOOP
      
          BEGIN
            SELECT primary_transaction_quantity,
                   orig_date_received
            INTO   l_moqd_transaction_quantity,
                   l_moqd_orig_date_received
            FROM   xxwc.xxwc_moqd_temp
            WHERE  inventory_item_id = r_results.inventory_item_id
            AND    organization_id = r_results.organization_id
            AND    onhand_quantities_id = r_results.onhand_quantities_id
            --AND    sequence_number = r_results.sequence_number
            AND    moqd_rowid = r_results.moqd_rowid;
          EXCEPTION
            WHEN OTHERS THEN
                l_moqd_transaction_quantity := NULL;
                l_moqd_orig_date_received := NULL;
                write_log_output('When others error getting xxwc_moqd_temp query ' || SQLCODE || SQLERRM);
          END;
          

          BEGIN
                      
            SELECT 
                       (case when 
                        (trunc(r_results.calc_orig_date_received)>trunc(l_moqd_orig_date_received)) 
                           THEN 
                              1
                           ELSE 
                              0 END),
                       (case when 
                        (trunc(r_results.calc_orig_date_received)<trunc(l_moqd_orig_date_received)) 
                           THEN 
                              1
                           ELSE 
                              0 END),
                       (case when
                         (trunc(r_results.calc_orig_date_received)=trunc(l_moqd_orig_date_received))
                            THEN
                                1
                             ELSE
                                0 END)      
            INTO    l_greater_than,
                    l_less_than,
                    l_equal
            FROM dual;
         
          EXCEPTION
            WHEN OTHERS THEN
                  l_greater_than := 0;
                  l_less_than := 0;
                  l_equal := 1;
            
        
          END;
          
          if l_less_than = 1 and r_results.loop_count > 0 and r_results.txn_count > 0 THEN
       
          
            l_update := 1;
            
          else
          
            l_update := 0;
            
          end if; 
                
          --Write_output(r_results.inventory_item_id || '|' ||r_results.organization_id || '|' ||r_results.segment1 || '|' || r_results.organization_code || '|' ||r_results.item_type ||'|'|| r_results.onhand_quantities_id || '|' || r_results.calc_transaction_quantity || '|' ||r_results.calc_orig_date_received || '|' || r_results.moqd_transaction_quantity || '|' || r_results.moqd_orig_date_received);
        --Write_output(r_results.sequence_number || '|' || r_results.inventory_item_id || '|' ||r_results.organization_id || '|' ||r_results.segment1 || '|' || r_results.organization_code || '|' ||r_results.item_type ||'|'|| r_results.onhand_quantities_id || '|' || r_results.calc_transaction_quantity || '|' ||r_results.calc_orig_date_received || '|' || l_moqd_transaction_quantity || '|' || l_moqd_orig_date_received);
        Write_output(r_results.inventory_item_id || '|' ||r_results.organization_id || '|' ||r_results.segment1 || '|' || r_results.organization_code || '|' ||r_results.item_type ||'|'|| r_results.onhand_quantities_id || '|' || r_results.calc_transaction_quantity || '|' ||r_results.calc_orig_date_received || '|' || l_moqd_transaction_quantity || '|' || l_moqd_orig_date_received || '|'||r_results.loop_count|| '|' || r_results.lot_count || '|' || r_results.txn_count || '|' || l_greater_than || '|' || l_less_than || '|' || l_equal || '|' || l_update );
        
        
           if l_update = 1 and nvl(p_update,'N') = 'Y' THEN
        
            BEGIN
                   update mtl_onhand_quantities_detail 
                          set   orig_date_received = r_results.calc_orig_date_received
                          where  inventory_item_id = r_results.inventory_item_id
                          and    organization_id  = r_results.organization_id
                          and    onhand_quantities_id = r_results.onhand_quantities_id
                          and    rowid = r_results.moqd_rowid;
            EXCEPTION
                WHEN OTHERS THEN
                    NULL;
            END;
          
            COMMIT;
          
            
            write_log_output('----------------------------');
            write_log_output('-----------UPDATE MTL_ONHAND_QUANTITIES_DETAIL---------------');
            write_log_output('updated r_results.moqd_rowid ' || r_results.moqd_rowid);
            write_log_output('updated r_reults.inventory_item_id ' || r_results.inventory_item_id);
            write_log_output('updated r_results.organization_id ' || r_results.organization_id);
            write_log_output('updated r_results.onhand_quantities_id ' || r_results.onhand_quantities_id);
            write_log_output('old orig_date_received ' || l_moqd_orig_date_received);
            write_log_output('new orig_date_received ' || r_results.calc_orig_date_received);
            
        
            END IF;
          
        END LOOP;
        
      END;

  PROCEDURE CREATE_BACKUP_TABLE
        (p_organization_id IN NUMBER
        ,p_inventory_item_id IN NUMBER
        ,p_table_name   OUT VARCHAR2)
  
    IS
  
    l_sql_statement VARCHAR2(1000);
    l_concurrent_request_id NUMBER := FND_GLOBAL.CONC_REQUEST_ID;
  
  BEGIN
  
    if p_organization_id IS NULL and p_inventory_item_id IS NULL THEN
    
        l_sql_statement := 'CREATE TABLE XXWC.XXWC_MOQD_'||l_concurrent_request_id||' as SELECT * FROM mtl_onhand_quantities_detail';
        
    elsif p_organization_id IS NOT NULL and p_inventory_item_id IS NULL THEN
    
        l_sql_statement := 'CREATE TABLE XXWC.XXWC_MOQD_'||l_concurrent_request_id||' as SELECT * FROM mtl_onhand_quantities_detail where organization_id = '||p_organization_id;
    
    elsif p_organization_id IS NOT NULL and p_inventory_item_id IS NOT NULL THEN
    
        l_sql_statement := 'CREATE TABLE XXWC.XXWC_MOQD_'||l_concurrent_request_id||' as SELECT * FROM mtl_onhand_quantities_detail where organization_id = '||p_organization_id||' and inventory_item_id = '||p_inventory_item_id;
    
    else
        
        l_sql_statement := 'CREATE TABLE XXWC.XXWC_MOQD_'||l_concurrent_request_id||' as SELECT * FROM mtl_onhand_quantities_detail';
    
    END IF;
  
    EXECUTE IMMEDIATE l_sql_statement;
    
    COMMIT;
 
    write_log_output(l_sql_statement);
    
    p_table_name := 'XXWC.XXWC_MOQD_'||l_concurrent_request_id;
    
  END CREATE_BACKUP_TABLE;
  
/*  
  PROCEDURE UPDATE_ORIG_RECEIPT_DATE
            (p_table_name in VARCHAR2)

    IS
  
    cursor c_bod_temp is
        select onhand_quantities_id,
               inventory_item_id,
               organization_id,
               moqd_rowid,
               orig_date_received,
               primary_transaction_quantity
         from   xxwc_bod_temp
         where  loop_count > 0
         and    txn_count > 0
         ;
         
     l_sql varchar2(2000);
     l_cursor integer;
     l_dummy  integer;
     l_orig_date_received DATE;
    
  BEGIN
  
 
    for r_bod_temp in c_bod_temp
    
        LOOP
        
            BEGIN
                select orig_date_received
                into   l_orig_date_received
                from   mtl_onhand_quantities_detail
                where  rowid = r_bod_temp.moqd_rowid;
             EXCEPTION
                when others then    
                    l_orig_date_received := NULL;
             END;
        
          if trunc(r_bod_temp.orig_date_received) > trunc(l_orig_date_received) then 
        
            BEGIN
                   update mtl_onhand_quantities_detail 
                          set   orig_date_received = r_bod_temp.orig_date_received
                          where  inventory_item_id = r_bod_temp.inventory_item_id
                          and    organization_id  = r_bod_temp.organization_id
                          and    onhand_quantities_id = r_bod_temp.onhand_quantities_id
                          and    rowid = r_bod_temp.moqd_rowid;
            EXCEPTION
                WHEN OTHERS THEN
                    NULL;
            END;
          
          
            write_log_output('updated r_bod_temp.moqd_rowid ' || r_bod_temp.moqd_rowid);
            write_log_output('updated r_bod_temp.inventory_item_id ' || r_bod_temp.inventory_item_id);
            write_log_output('updated r_bod_temp.organization_id ' || r_bod_temp.organization_id);
            write_log_output('updated r_bod_temp.onhand_quantities_id ' || r_bod_temp.onhand_quantities_id);
            
          
          else
          
            null;
            
          end if;
            
        /*
         begin
            
        
                        l_sql := 'update ' || p_table_name || ' 
                          set   orig_date_received = '''||r_bod_temp.orig_date_received||'''
                          where  inventory_item_id = '||r_bod_temp.inventory_item_id||'
                          and    organization_id  = '||r_bod_temp.organization_id||'
                          and    onhand_quantities_id = '||r_bod_temp.onhand_quantities_id||'
                          and    primary_transaction_quantity = '||r_bod_temp.primary_transaction_quantity||'
                          and    orig_date_received > '''||r_bod_temp.orig_date_received||'''';

                write_log_output(l_sql);
                
                l_cursor := dbms_sql.open_cursor;
                dbms_sql.parse(l_cursor, l_sql, dbms_sql.native);
                l_dummy := dbms_sql.execute(l_cursor);
                
                
                dbms_sql.close_cursor(l_cursor);      
        
            END;
        END LOOP;
         
  
     COMMIT;
    
  END UPDATE_ORIG_RECEIPT_DATE;

*/

  PROCEDURE FIND_QTY
        (p_transaction_id in out NUMBER
        ,p_inventory_item_id IN NUMBER
        ,p_organization_id in out NUMBER
        ,p_onhand_quantities_id in number
        ,p_onhand_quantity IN NUMBER
        ,p_row_id     IN VARCHAR2
        )
    IS
        
        l_neg_qty   number;
        l_pos_qty   number;
        l_transaction_id number;

     l_receipt_transaction_id NUMBER;
     l_xfer_transaction_id NUMBER;
     l_alias_receipt_id NUMBER;
     l_wip_completion_id NUMBER;
     l_intransit_receipt_id NUMBER;
     l_cycle_count_adjust_id NUMBER;
     l_rma_receipt_id NUMBER;
    
    
     p_transaction_type_id NUMBER;
     p_quantity number;
     l_organization_id NUMBER;
     l_result VARCHAR2(5);
     l_transfer_transaction_id NUMBER;
     l_transfer_organization_id NUMBER;
     l_count NUMBER DEFAULT 0;
     l_date  DATE;
     x_result VARCHAR2(5);
     l_txn_count NUMBER DEFAULT 0;
     l_txn_count_total NUMBER DEFAULT 0;
     l_lot_count NUMBER DEFAULT 0;
     l_lot_count_total NUMBER DEFAULT 0;
     l_allocated_qty number DEFAULT 0;
     l_sum_allocated_qty number DEFAULT 0;
     
    BEGIN

    
    l_organization_id := p_organization_id;


    xxwc_bod_pkg.insert_into_moqd_temp
      (p_inventory_item_id => p_inventory_item_id
      ,p_organization_id   => p_organization_id
      ,p_onhand_quantities_id => p_onhand_quantities_id
      --,p_sequence_number => l_sequence_number
      ,p_row_id   => p_row_id);
    
        
    l_count := 0;
    
    --PO Receipt Transaction Id      

    BEGIN
      SELECT transaction_type_id
      INTO   l_receipt_transaction_id
      FROM   mtl_transaction_types
      where  transaction_type_name = 'PO Receipt';
    END;


    --Int Req Intr Rcpt Transaction ID
    BEGIN
      SELECT transaction_type_id
      INTO   l_xfer_transaction_id
      FROM   mtl_transaction_types
      where  transaction_type_name = 'Int Req Intr Rcpt';
    END;

  --Account Alias Receipt
    BEGIN
      SELECT transaction_type_id
      INTO   l_alias_receipt_id
      FROM   mtl_transaction_types
      where  transaction_type_name = 'Account alias receipt (Inventory Type Adjustment In)';
    END;

  --WIP Completion
    BEGIN
      SELECT transaction_type_id
      INTO   l_wip_completion_id
      FROM   mtl_transaction_types
      where  transaction_type_name = 'WIP Completion';
    END;

  --Intransit Receipt

    BEGIN
      SELECT transaction_type_id
      INTO   l_intransit_receipt_id
      FROM   mtl_transaction_types
      WHERE  transaction_type_name = 'Intransit Receipt';
    END;

    BEGIN
      SELECT transaction_type_id
      INTO   l_cycle_count_adjust_id
      FROM   mtl_transaction_types
      WHERE  transaction_type_name = 'Cycle Count Adjust';
    END;
  
  
  BEGIN
    SELECT transaction_type_id
    INTO   l_rma_receipt_id
    FROM   mtl_transaction_types
    WHERE  transaction_type_name = 'RMA Receipt';
  END;

    Write_log_output('--------------Starting------------------');
      
    xxwc_bod_pkg.get_item_org(p_inventory_item_id, p_organization_id);
    
    write_log_output('p_onhand_quantities_id : ' || p_onhand_quantities_id); 
    
    
    write_log_output('-------Start Transaction Search-------------------'); 
    
  
    <<SECTION1>>
        Write_log_output('time_stamp : ' || to_char(sysdate,'DD-MON-YYYY HH:MM:SS'));
    
        write_log_output('-------LOOP '|| l_count || '-----------');
        write_log_output('p_inventorg_item_id = ' || p_inventory_item_id);
        write_log_output('p_organization_id = ' || p_organization_id);
        write_log_output('p_transaction_id = ' || p_transaction_id);
    
        --execute immediate 'TRUNCATE TABLE XXWC.XXWC_BOD_DETAIL_TEMP';

    
        BEGIN
        INSERT INTO XXWC.XXWC_BOD_DETAIL_TEMP
        select mmt.organization_id,
               mmt.inventory_item_id,
               mmt.transaction_id,
               mmt.transfer_transaction_id,
               mmt.transfer_organization_id,
               NVL(TO_DATE(MMT.ATTRIBUTE15),mmt.transaction_date),
               mmt.transaction_type_id,
               sum(CASE WHEN mmt.primary_quantity >= 0 AND mmt.logical_transaction IS NULL AND mmt.transaction_type_id NOT IN (2,52,53) THEN mmt.primary_quantity ELSE 0 END) OVER (PARTITION BY mmt.organization_id, mmt.inventory_item_id ORDER BY mmt.organization_id, mmt.inventory_item_id, nvl(to_date(mmt.attribute15,'DD-MON-RR'),mmt.transaction_date) ASC) sum_pos_qty,
               sum(CASE WHEN mmt.primary_quantity < 0 AND mmt.logical_transaction IS NULL AND mmt.transaction_type_id NOT IN (2,52,53) THEN mmt.primary_quantity ELSE 0 END) OVER (PARTITION BY mmt.organization_id, mmt.inventory_item_id ORDER BY mmt.organization_id, mmt.inventory_item_id, nvl(to_date(mmt.attribute15,'DD-MON-RR'),mmt.transaction_date) ASC) sum_neg_qty,
               NULL
        FROM   mtl_material_transactions mmt
        WHERE  mmt.inventory_item_id = p_inventory_item_id
        and    mmt.organization_id = p_organization_id
        AND    not exists (select *
                           from   xxwc.xxwc_bod_detail_temp
                           where  organization_id = p_organization_id
                           and    inventory_item_id = p_inventory_item_id)
        ORDER BY mmt.organization_id, mmt.inventory_item_id, nvl(to_date(mmt.attribute15,'DD-MON-RR'),mmt.transaction_date) ASC;
    
        exception
            when others then
                write_log_output('Could not insert into xxwc_bod_detail_temp ' || sqlcode || sqlerrm);
        END;
        
        COMMIT;
        
        BEGIN
            select sum_pos_qty, sum_neg_qty
            into   l_pos_qty, l_neg_qty
            from   xxwc.xxwc_bod_detail_temp
            where  transaction_id = p_transaction_id;   
        exception
            when others then
                write_log_output('could not find l_pos_qty or l_neg_qty' ||  sqlcode || sqlerrm);
                l_pos_qty :=0;
                l_neg_qty :=0;
        END;
    
        
        BEGIN
            select sum(nvl(allocated_qty,0))
            into   l_sum_allocated_qty
            from   xxwc.xxwc_bod_detail_temp
            where  transaction_id <= p_transaction_id
            and    inventory_item_id = p_inventory_item_id
            and    organization_id = p_organization_id;   
        exception
            when others then
                write_log_output('could not find l_sum_allocated_qty' ||  sqlcode || sqlerrm);
                l_sum_allocated_qty := 0;
        END;
    
        write_log_output('l_pos_qty ' || l_pos_qty);
        write_log_output('l_neg_qty ' || l_neg_qty);
        write_log_output('l_sum_allocated ' || l_sum_allocated_qty); 
        
    
        BEGIN
            select transaction_id
            into   l_transaction_id
            from   xxwc.xxwc_bod_detail_temp
            where  transaction_id = p_transaction_id
            and  transaction_type_id IN (l_receipt_transaction_id, l_xfer_transaction_id,l_alias_receipt_id,  l_wip_completion_id,l_intransit_receipt_id, l_cycle_count_adjust_id, l_rma_receipt_id);
        EXCEPTION 
            WHEN NO_DATA_FOUND THEN
            write_log_output('hit no data found exception, looking for earlier transaction');
            write_log_output('l_neg_qty :' || l_neg_qty);
            write_log_output('transaction_type_ids :' || l_receipt_transaction_id||','|| l_xfer_transaction_id||','||l_alias_receipt_id||','||l_wip_completion_id||','||l_intransit_receipt_id||','||l_cycle_count_adjust_id||','||l_rma_receipt_id);
            BEGIN
            
                /*if abs(l_neg_qty) > l_pos_qty THEN
                    
                    l_neg_qty := l_pos_qty;
                    
                else
                
                    l_neg_qty := l_neg_qty + l_sum_allocated_qty;
                
                end if;
            */
            
               if abs(l_neg_qty)-l_sum_allocated_qty > l_pos_qty THEN
                    
                    l_neg_qty := l_pos_qty-1;
                    
                else
                
                    l_neg_qty := l_neg_qty + l_sum_allocated_qty;
                
               end if;
        
        
               write_log_output('updated l_neg_qty ' || l_neg_qty); 
                   
                select min(transaction_id)
                into   l_transaction_id
                from   xxwc.xxwc_bod_detail_temp
                --where  sum_pos_qty >= abs(l_neg_qty)
                where  sum_pos_qty >= abs(l_neg_qty)
                and    inventory_item_id = p_inventory_item_id
                and    organization_id = p_organization_id
                and    transaction_type_id IN (l_receipt_transaction_id, l_xfer_transaction_id,l_alias_receipt_id,  l_wip_completion_id,l_intransit_receipt_id, l_cycle_count_adjust_id, l_rma_receipt_id);
            EXCEPTION
                when others then
                    write_log_output('could not find min transaction_id');
            END;           
           
            WHEN OTHERS THEN
                write_log_output('could not find transaction_id');
        END;
        
        write_log_output('-------FIND QTY--------');
        write_log_output('l_transaction_id ' || l_transaction_id);
        write_log_output('-------END FIND QTY--------'); 
  
  
            
        BEGIN
            select transaction_type_id, transfer_transaction_id, transfer_organization_id, primary_quantity, --nvl(to_date(attribute15,'DD-MON-RR'),transaction_date),
                   case when source_code = 'CONVERSION' and attribute15 is null then trunc(add_months(transaction_date,-12)) else nvl(to_date(attribute15,'DD-MON-RR'),transaction_date) end
            into   p_transaction_type_id, l_transfer_transaction_id, l_transfer_organization_id, p_quantity, l_date
            from   mtl_material_transactions
            where  transaction_id = l_transaction_id;
        EXCEPTION
            WHEN OTHERS THEN
                 p_transaction_type_id := NULL;
                 l_transfer_transaction_id := NULL;
                 l_transfer_organization_id := NULL;
                 p_quantity := NULL;
                 l_date := NULL;
            
        END;
        
        write_log_output('p_transaction_type_id ' || p_transaction_type_id);
        write_log_output('l_transfer_transaction_id ' || l_transfer_transaction_id);
        write_log_output('l_transfer_organization_id ' || l_transfer_organization_id);
        write_log_output('p_quantity ' || p_quantity);
        write_log_output('l_date ' || l_date);
        
        
         l_allocated_qty := p_onhand_quantity - l_sum_allocated_qty;
         
         write_log_output('l_allocated_qty ' || l_allocated_qty);

        BEGIN         
         update xxwc.xxwc_bod_detail_temp
         set    allocated_qty = nvl(l_allocated_qty,0) + nvl(allocated_qty,0)
         where  transaction_id = l_transaction_id
         and    inventory_item_id = inventory_item_id
         and    organization_id = organization_id; 
        END;

        COMMIT;
    
BEGIN
    SELECT 1
    INTO   l_lot_count
    FROM   MTL_TRANSACTION_LOT_NUMBERS
    where  transaction_id = p_transaction_id;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
          L_LOT_COUNT := 0;
    WHEN OTHERS THEN
          L_LOT_COUNT := 0;
  END;
  
  
  BEGIN
    SELECT 1
    INTO   l_txn_count
    FROM   mtl_material_transactions
    where  transaction_id = p_transaction_id
    and    transaction_date between '28-MAR-2013' and '05-SEP-2013'; --Dates where BOD Extension didn't execute
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
          L_TXN_COUNT := 0;
    WHEN OTHERS THEN
          L_TXN_COUNT := 0;
  END;

  
    l_txn_count_total := l_txn_count_total + l_txn_count;
    l_lot_count_total := l_lot_count_total + l_lot_count;
  
  
  IF p_transaction_type_id = l_receipt_transaction_id THEN
  
    IF p_quantity >= 0 THEN
    
       l_result := 'STOP';
       
    ELSE
    
        l_result := 'GO';
    
    END IF;
  
  ELSIF p_transaction_type_id =  l_alias_receipt_id THEN
    
    IF p_quantity >= 0 THEN
    
       l_result := 'STOP';
       
    ELSE
    
        l_result := 'GO';
    
    END IF;
    
  ELSIF p_transaction_type_id = l_xfer_transaction_id THEN
    
    IF p_quantity >= 0 THEN
    
       l_result := 'XFER';
       
    ELSE
    
        l_result := 'GO';
    
    END IF;
  
  ELSIF p_transaction_type_id = l_intransit_receipt_id THEN
    
    IF p_quantity >= 0 THEN
    
       l_result := 'XFER';
       
    ELSE
    
        l_result := 'GO';
    
    END IF;

  ELSIF p_transaction_type_id =  l_wip_completion_id THEN
    
    IF p_quantity >= 0 THEN
    
       l_result := 'STOP';
       
    ELSE
    
        l_result := 'GO';
    
    END IF;
  

  ELSIF p_transaction_type_id = l_cycle_count_adjust_id THEN
  
    IF p_quantity > 0 THEN
    
      l_result := 'STOP';
      
    ELSE
    
      l_result := 'GO';
      
    END IF;

  ELSIF p_transaction_type_id = l_rma_receipt_id THEN
  
    IF p_quantity > 0 THEN
    
      l_result := 'STOP';
      
    ELSE
    
      l_result := 'GO';
      
    END IF;


  ELSE
  
      l_result := 'STOP';
      
  END IF;
  
  
  write_log_output('l_result ' || l_result);
  
    IF l_result = 'XFER' THEN
      
      Write_log_output('l_count : ' || l_count);
      
      IF l_count = 10 THEN
      
        NULL;
      
      ELSE
      
      --p_transaction_date := x_date;
      
      
          l_count := l_count + 1;
          p_organization_id := l_transfer_organization_id;
          p_transaction_id := l_transfer_transaction_id;
           
      
      GOTO SECTION1;
    
      END IF;
    
     /*if l_balance_qty > 0 then
    
      p_onhand_quantity := l_balance_qty;
      p_transaction_date := x_date;
      
      IF l_count = 10 THEN
      
        NULL;
      
      ELSE
    
        Write_log_output('p_onhand_quantity : ' || l_balance_qty);
        
        GOTO SECTION1;
    
      END IF;
      
    END IF;
      */
      
    END IF;
    

  
      
--    Write_output(p_inventory_item_id || '|' ||p_organization_id || '|' || NULL /*r_results.segment1 */|| '|' || NULL /*r_results.organization_code */|| '|' || NULL /*r_results.item_type */ ||'|'|| p_onhand_quantities_id || '|' || p_quantity || '|' || l_date || '|' || NULL /*l_moqd_transaction_quantity */|| '|' || NULL /*l_moqd_orig_date_received */|| '|'|| l_count /*r_results.loop_count*/|| '|' || NULL /*r_results.lot_count*/);
   
  INSERT INTO xxwc.xxwc_bod_temp
      (inventory_item_id
      ,organization_Id
      ,update_transaction_id
      ,create_transaction_id
      ,primary_transaction_quantity
      ,onhand_quantities_id
      ,orig_date_received
      ,creation_date
      --,sequence_number
      ,moqd_rowid
      ,loop_count
      ,lot_count
      ,txn_count)
  VALUES
      (p_inventory_item_id
      ,l_organization_id
      ,NULL
      ,NULL
      ,p_onhand_quantity
      ,p_onhand_quantities_id
      ,l_date
      ,NULL
      --,l_sequence_number
      ,p_row_id
      ,l_count
      ,l_lot_count_total
      ,l_txn_count_total);

  
  
    END FIND_QTY;  


  
END;