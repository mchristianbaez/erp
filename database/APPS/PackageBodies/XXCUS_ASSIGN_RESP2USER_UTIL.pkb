CREATE OR REPLACE PACKAGE BODY APPS."XXCUS_ASSIGN_RESP2USER_UTIL" IS
  /**************************************************************************************
  The purpose of this utility package is to assign responsibility to a Application User acct

  Logic is as follows :
    Oracle Application User will be assigned with responsibilities.
  *************************************************************************************/

  -- Error DEBUG
  l_err_callfrom  VARCHAR2(75) DEFAULT 'XXCUS_ASSIGN_RESP2USER_UTIL';
  l_err_callpoint VARCHAR2(75) DEFAULT 'START';
  l_distro_list   VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';

  PROCEDURE process_users(errbuf_p  OUT VARCHAR2
                         ,retcode_p OUT NUMBER) 
                         IS

    pl_error_code  VARCHAR2(1);
    pl_error_msg   VARCHAR2(4000);
    pl_count       NUMBER;
    pl_error_count NUMBER;

    l_from_date       DATE := TRUNC(SYSDATE);
    l_to_date         DATE := TRUNC(SYSDATE) + 700;
    l_exists_flag     VARCHAR2(1);

     CURSOR cur_usr_resp
         IS
     SELECT tl.responsibility_id
          , tl.application_id
          , fu.user_id
          , fu.user_name
          , tl.responsibility_name
       FROM XXCUS_RESPONSIBILITY_XREF    xref
          , XXCUS_USER_RESPONSIBILTY_DTLS dtls
          , fnd_responsibility_tl         tl
          , fnd_user                      fu
      WHERE xref.old_responsibility_name = dtls.old_responsibility_name
        AND xref.new_responsibility_name = tl.responsibility_name
        AND fu.user_name                 = dtls.user_name
      ORDER BY fu.user_name;
  BEGIN
    fnd_file.put_line(fnd_file.output,'#####################################################################################');

    fnd_file.put_line(fnd_file.output,
                      ' Start Processing : ' ||
                       to_char(SYSDATE, 'DD-MON-RRRR HH24:MI:SS'));

         fnd_file.put_line(fnd_file.output,'------------------------------------------------------------------------------------');
         fnd_file.put_line(fnd_file.output,'EXISTS    USER_NAME                     RESPONSIBILITY NAME' ||
                       to_char(SYSDATE, 'DD-MON-RRRR HH24:MI:SS'));
         fnd_file.put_line(fnd_file.output,'------------------------------------------------------------------------------------');

    FOR rec_usr_resp IN cur_usr_resp LOOP
      BEGIN

         l_exists_flag    := 'N';

         BEGIN
           SELECT 'Y'
             INTO l_exists_flag
             FROM fnd_user_resp_groups_all
            WHERE user_id = rec_usr_resp.user_id
              AND responsibility_id = rec_usr_resp.responsibility_id
              AND SYSDATE between start_date AND NVL(end_date, TRUNC(SYSDATE) + 1);
         EXCEPTION
           WHEN OTHERS THEN
           l_exists_flag := 'N';
         END;

         IF l_exists_flag = 'N' THEN
         fnd_user_resp_groups_api.insert_assignment(user_id                       => rec_usr_resp.user_id,
                                                    responsibility_id             => rec_usr_resp.responsibility_id,
                                                    responsibility_application_id => rec_usr_resp.application_id,
                                                    security_group_id             => 0,
                                                    start_date                    => l_from_date,
                                                    end_date                      => NULL,
                                                    description                   => 'UPGRADE ASSIGNMENT');
         END IF;

         fnd_file.put_line(fnd_file.output,rpad(l_exists_flag, 10, ' ')||rpad(rec_usr_resp.user_name, 30, ' ')||rec_usr_resp.responsibility_name);

      EXCEPTION
      WHEN OTHERS THEN
         fnd_file.put_line(fnd_file.output, 'Error - '||SQLERRM);
      END;
    END LOOP;

    fnd_file.put_line(fnd_file.output,'------------------------------------------------------------------------------------');
    fnd_file.put_line(fnd_file.output,'#####################################################################################');

    COMMIT;

    EXCEPTION
      WHEN OTHERS THEN
        fnd_file.put_line(fnd_file.output, 'Error - '||SQLERRM);

    fnd_file.put_line(fnd_file.output,
                      ' Completed Processing : ' ||
                       to_char(SYSDATE, 'DD-MON-RRRR HH24:MI:SS'));

  END process_users;



  FUNCTION check_user_exists(p_user_number IN VARCHAR2) RETURN VARCHAR2 IS
    CURSOR cu_check_user IS
      SELECT '1'
        FROM apps.fnd_user        a
            ,apps.per_employees_x b
       WHERE b.employee_num = p_user_number
         AND a.employee_id = b.employee_id
         AND a.end_date IS NULL;


    pl_flag VARCHAR2(1) := 'Y';
  BEGIN
    IF cu_check_user%ISOPEN THEN
      CLOSE cu_check_user;
    END IF;

    OPEN cu_check_user;
    FETCH cu_check_user
      INTO pl_flag;
    IF cu_check_user%FOUND THEN
      pl_flag := 'Y';
    ELSE
      pl_flag := 'N';
    END IF;
    CLOSE cu_check_user;
    RETURN pl_flag;
  END check_user_exists;

END XXCUS_ASSIGN_RESP2USER_UTIL;
/
