--
-- XXWCINV_ECO_APPROVAL_WF_PKG  (Package Body) 
--
CREATE OR REPLACE PACKAGE BODY APPS.XXWCINV_ECO_APPROVAL_WF_PKG
AS
   /*******************************************************************************************/
   /* Get_Attributes : procedure to populate custom elements within the workflow process      */
   /*******************************************************************************************/
   PROCEDURE Get_Attributes (p_itemtype   IN            VARCHAR2
                            ,p_itemkey    IN            VARCHAR2
                            ,p_actid      IN            NUMBER
                            ,p_funcmode   IN            VARCHAR2
                            ,x_result        OUT NOCOPY VARCHAR2)
   AS
      CURSOR C_Approvers (
         p_apprv_list_id NUMBER)
      IS
           SELECT EA.attribute1
             FROM ENG_ECN_APPROVERS EA
            WHERE     EA.approval_list_id = p_apprv_list_id
                  AND EA.attribute1 IS NOT NULL
                  AND TRUNC (SYSDATE) <= NVL (EA.disable_date, SYSDATE + 1)
         ORDER BY EA.sequence1;

      l_approval_list      VARCHAR2 (80);
      l_approval_list_id   NUMBER := 100;
      l_contract_admin     VARCHAR2 (80);
   BEGIN
      -- retrieve approval list element value for workflow process
      l_approval_list :=
         WF_ENGINE.GetItemAttrText (itemtype   => p_itemtype
                                   ,itemkey    => p_itemkey
                                   ,aname      => 'APPROVAL_LIST');

      -- retrieve approval list id from within element value
      l_approval_list_id :=
         SUBSTR (l_approval_list
                ,  INSTR (l_approval_list
                         ,':'
                         ,1
                         ,1)
                 + 1);

      -- find contract admin based upon approval list approvers
      OPEN C_Approvers (l_approval_list_id);

      FETCH C_Approvers INTO l_contract_admin;

      IF C_Approvers%NOTFOUND
      THEN
         l_contract_admin := NULL;
      END IF;

      CLOSE C_Approvers;

      -- populate contract admin element within workflow process
      DBMS_OUTPUT.Put_Line ('Contract Admin = ' || l_contract_admin);
      WF_ENGINE.SetItemAttrText (itemtype   => p_itemtype
                                ,itemkey    => p_itemkey
                                ,aname      => 'XXWC_CONTRACT_ADMIN'
                                ,avalue     => l_contract_admin);

      -- return result based upon contract admin value
      IF l_contract_admin IS NULL
      THEN
         x_result := 'COMPLETE:FAILURE';
      ELSE
         x_result := 'COMPLETE:SUCCESS';
      END IF;
   END Get_Attributes;
END XXWCINV_ECO_APPROVAL_WF_PKG;
/

