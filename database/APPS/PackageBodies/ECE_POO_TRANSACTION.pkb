/* Formatted on 1/7/2014 6:30:39 PM (QP5 v5.206) */
-- Start of DDL Script for Package Body APPS.ECE_POO_TRANSACTION
-- Generated 1/7/2014 6:30:36 PM from APPS@EBIZDEV

CREATE OR REPLACE PACKAGE BODY apps.ece_poo_transaction
AS
    -- $Header: ECEPOOB.pls 120.10.12010000.1 2008/07/25 07:23:27 appldev ship $

    ioutput_width   INTEGER := 4000;                                                                          -- 2823215
    ikey_count      NUMBER := 0;
    xheadercount    NUMBER := 0;
    i_path          VARCHAR2 (1000);
    i_filename      VARCHAR2 (1000);

    PROCEDURE extract_poo_outbound (errbuf                 OUT NOCOPY VARCHAR2
                                   ,retcode                OUT NOCOPY VARCHAR2
                                   ,coutput_path        IN            VARCHAR2
                                   ,coutput_filename    IN            VARCHAR2
                                   ,cpo_number_from     IN            VARCHAR2
                                   ,cpo_number_to       IN            VARCHAR2
                                   ,ccdate_from         IN            VARCHAR2
                                   ,ccdate_to           IN            VARCHAR2
                                   ,cpc_type            IN            VARCHAR2
                                   ,cvendor_name        IN            VARCHAR2
                                   ,cvendor_site_code   IN            VARCHAR2
                                   ,v_debug_mode        IN            NUMBER DEFAULT 0)
    IS
        xprogress                  VARCHAR2 (80);
        irun_id                    NUMBER := 0;
        ioutput_width              INTEGER := 4000;
        ctransaction_type          VARCHAR2 (120) := 'POO';
        ccommunication_method      VARCHAR2 (120) := 'EDI';
        cheader_interface          VARCHAR2 (120) := 'ECE_PO_INTERFACE_HEADERS';
        cline_interface            VARCHAR2 (120) := 'ECE_PO_INTERFACE_LINES';
        cshipment_interface        VARCHAR2 (120) := 'ECE_PO_INTERFACE_SHIPMENTS';
        cdistribution_interface    VARCHAR2 (120) := 'ECE_PO_DISTRIBUTIONS';                               --Bug 1891291
        l_line_text                VARCHAR2 (2000);

        ccreat_date_from           DATE := TO_DATE (ccdate_from, 'YYYY/MM/DD HH24:MI:SS');
        ccreat_date_to             DATE := TO_DATE (ccdate_to, 'YYYY/MM/DD HH24:MI:SS') + 1;
        cenabled                   VARCHAR2 (1) := 'Y';
        ece_transaction_disabled   EXCEPTION;
        --xHeaderCount               NUMBER;
        cfilename                  VARCHAR2 (30) := NULL;                                                      --2430822

        CURSOR c_output
        IS
              SELECT text
                FROM ece_output
               WHERE run_id = irun_id
            ORDER BY line_id;
    BEGIN
        xprogress := 'POO-10-1000';
        ec_debug.enable_debug (v_debug_mode);

        IF ec_debug.g_debug_level >= 2
        THEN
            ec_debug.push ('ECE_POO.Extract_POO_Outbound');
            ec_debug.pl (3, 'cOutput_Path: ', coutput_path);
            ec_debug.pl (3, 'cOutput_Filename: ', coutput_filename);
            ec_debug.pl (3, 'cPO_Number_From: ', cpo_number_from);
            ec_debug.pl (3, 'cPO_Number_To: ', cpo_number_to);
            ec_debug.pl (3, 'cCDate_From: ', ccdate_from);
            ec_debug.pl (3, 'cCDate_To: ', ccdate_to);
            ec_debug.pl (3, 'cPC_Type: ', cpc_type);
            ec_debug.pl (3, 'cVendor_Name: ', cvendor_name);
            ec_debug.pl (3, 'cVendor_Site_Code: ', cvendor_site_code);
            ec_debug.pl (3, 'v_debug_mode: ', v_debug_mode);
        END IF;

        /* Check to see if the transaction is enabled. If not, abort */
        xprogress := 'POO-10-1005';
        fnd_profile.get ('ECE_' || ctransaction_type || '_ENABLED', cenabled);

        xprogress := 'POO-10-1010';

        IF cenabled = 'N'
        THEN
            xprogress := 'POO-10-1015';
            RAISE ece_transaction_disabled;
        END IF;

        xprogress := 'POO-10-1020';

        BEGIN
            SELECT ece_output_runs_s.NEXTVAL INTO irun_id FROM DUAL;
        EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
                ec_debug.pl (0
                            ,'EC'
                            ,'ECE_GET_NEXT_SEQ_FAILED'
                            ,'PROGRESS_LEVEL'
                            ,xprogress
                            ,'SEQ'
                            ,'ECE_OUTPUT_RUNS_S');
        END;

        xprogress := 'POO-10-1025';

        IF ec_debug.g_debug_level >= 1
        THEN
            ec_debug.pl (3, 'iRun_id: ', irun_id);
            ec_debug.pl (1
                        ,'EC'
                        ,'ECE_POO_START'
                        ,NULL);
            ec_debug.pl (1
                        ,'EC'
                        ,'ECE_RUN_ID'
                        ,'RUN_ID'
                        ,irun_id);
        END IF;

        xprogress := 'POO-10-1026';

        ece_poo_transaction.project_sel_c := 0;                                                            --Bug 2490109

        IF coutput_filename IS NULL
        THEN                                                                                               --Bug 2430822
           -- cfilename := 'POO' || irun_id || '.dat';
            cfilename := 'POO' || irun_id || '.tmp';-- changed by Rasikha 01/06/2014 TMS 20130415-00522, to prevent the file ftp before write will finish
            --another custom CP "XXWC EDI File extention renaming" will change it back to .dat after checking that original CP finished processing.
        ELSE
            cfilename := coutput_filename;
        END IF;

        -- Open the file for write.
        xprogress := 'POO-10-1030';

        IF ec_debug.g_debug_level = 1
        THEN
            ec_debug.pl (1, 'Output File:', cfilename);
            ec_debug.pl (1, 'Open Output file');                                                           --Bug 2034376
        END IF;

        i_path := coutput_path;
        i_filename := cfilename;
        --    ece_poo_transaction.uFile_type := utl_file.fopen(cOutput_Path,cFilename,'W',32767); --Bug 2887790

        xprogress := 'POO-10-1040';

        IF ec_debug.g_debug_level = 1
        THEN
            ec_debug.pl (1, 'Call Populate Poo Trx procedure');                                            --Bug 2034376
        END IF;

        ece_poo_transaction.populate_poo_trx (ccommunication_method
                                             ,ctransaction_type
                                             ,ioutput_width
                                             ,SYSDATE
                                             ,irun_id
                                             ,cheader_interface
                                             ,cline_interface
                                             ,cshipment_interface
                                             ,cdistribution_interface
                                             ,ccreat_date_from
                                             ,ccreat_date_to
                                             ,cvendor_name
                                             ,cvendor_site_code
                                             ,cpc_type
                                             ,cpo_number_from
                                             ,cpo_number_to);

        /*     xProgress := 'POO-10-1035';
         if ec_debug.G_debug_level = 1 then
              ec_debug.pl(1,'Call Put To Output Table procedure');   --Bug 2034376
             end if;

         select count(*)
             into xHeaderCount
             from ECE_PO_INTERFACE_HEADERS
             where run_id = iRun_id;
        */
        IF ec_debug.g_debug_level = 1
        THEN
            ec_debug.pl (1, 'NUMBER OF RECORDS PROCESSED IS ', xheadercount);
        END IF;

        /*
     xProgress := 'POO-10-1041';

          ece_poo_transaction.put_data_to_output_table(
                cCommunication_Method,
                cTransaction_Type,
                iOutput_width,
                iRun_id,
                cHeader_Interface,
                cLine_Interface,
                cShipment_Interface,
                cDistribution_Interface);
      */

        xprogress := 'POO-10-1042';

        IF (UTL_FILE.is_open (ece_poo_transaction.ufile_type))
        THEN
            UTL_FILE.fclose (ece_poo_transaction.ufile_type);
        END IF;

        IF ec_mapping_utils.ec_get_trans_upgrade_status (ctransaction_type) = 'U'
        THEN
            ec_debug.pl (0
                        ,'EC'
                        ,'ECE_REC_TRANS_PENDING'
                        ,NULL);
            retcode := 1;
        END IF;

        IF ec_debug.g_debug_level >= 2
        THEN
            ec_debug.pop ('ECE_POO.Extract_POO_Outbound');
        END IF;

        ec_debug.disable_debug;

        COMMIT;
    EXCEPTION
        WHEN ece_transaction_disabled
        THEN
            ec_debug.pl (0
                        ,'EC'
                        ,'ECE_TRANSACTION_DISABLED'
                        ,'TRANSACTION'
                        ,ctransaction_type);
            retcode := 1;
            ec_debug.disable_debug;
            ROLLBACK;
        WHEN UTL_FILE.write_error
        THEN
            ec_debug.pl (0
                        ,'EC'
                        ,'ECE_UTL_WRITE_ERROR'
                        ,NULL);
            ec_debug.pl (0
                        ,'EC'
                        ,'ECE_ERROR_MESSAGE'
                        ,'ERROR_MESSAGE'
                        ,SQLERRM);

            retcode := 2;
            ec_debug.disable_debug;

            IF (UTL_FILE.is_open (ece_poo_transaction.ufile_type))
            THEN
                UTL_FILE.fclose (ece_poo_transaction.ufile_type);
            END IF;

            ece_poo_transaction.ufile_type :=
                UTL_FILE.fopen (coutput_path
                               ,cfilename
                               ,'W'
                               ,32767);
            UTL_FILE.fclose (ece_poo_transaction.ufile_type);
            ROLLBACK;
        WHEN UTL_FILE.invalid_path
        THEN
            ec_debug.pl (0
                        ,'EC'
                        ,'ECE_UTIL_INVALID_PATH'
                        ,NULL);
            ec_debug.pl (0
                        ,'EC'
                        ,'ECE_ERROR_MESSAGE'
                        ,'ERROR_MESSAGE'
                        ,SQLERRM);

            retcode := 2;
            ec_debug.disable_debug;
            ROLLBACK;
        WHEN UTL_FILE.invalid_operation
        THEN
            ec_debug.pl (0
                        ,'EC'
                        ,'ECE_UTIL_INVALID_OPERATION'
                        ,NULL);
            ec_debug.pl (0
                        ,'EC'
                        ,'ECE_ERROR_MESSAGE'
                        ,'ERROR_MESSAGE'
                        ,SQLERRM);

            retcode := 2;
            ec_debug.disable_debug;

            IF (UTL_FILE.is_open (ece_poo_transaction.ufile_type))
            THEN
                UTL_FILE.fclose (ece_poo_transaction.ufile_type);
            END IF;

            ece_poo_transaction.ufile_type :=
                UTL_FILE.fopen (coutput_path
                               ,cfilename
                               ,'W'
                               ,32767);
            UTL_FILE.fclose (ece_poo_transaction.ufile_type);
            ROLLBACK;
        WHEN OTHERS
        THEN
            ec_debug.pl (0
                        ,'EC'
                        ,'ECE_PROGRAM_ERROR'
                        ,'PROGRESS_LEVEL'
                        ,xprogress);
            ec_debug.pl (0
                        ,'EC'
                        ,'ECE_ERROR_MESSAGE'
                        ,'ERROR_MESSAGE'
                        ,SQLERRM);

            retcode := 2;
            ec_debug.disable_debug;

            IF (UTL_FILE.is_open (ece_poo_transaction.ufile_type))
            THEN
                UTL_FILE.fclose (ece_poo_transaction.ufile_type);
            END IF;

            ece_poo_transaction.ufile_type :=
                UTL_FILE.fopen (coutput_path
                               ,cfilename
                               ,'W'
                               ,32767);
            UTL_FILE.fclose (ece_poo_transaction.ufile_type);
            ROLLBACK;
    END extract_poo_outbound;

    PROCEDURE populate_poo_trx (ccommunication_method     IN VARCHAR2
                               ,ctransaction_type         IN VARCHAR2
                               ,ioutput_width             IN INTEGER
                               ,dtransaction_date         IN DATE
                               ,irun_id                   IN INTEGER
                               ,cheader_interface         IN VARCHAR2
                               ,cline_interface           IN VARCHAR2
                               ,cshipment_interface       IN VARCHAR2
                               ,cdistribution_interface   IN VARCHAR2
                               ,ccreate_date_from         IN DATE
                               ,ccreate_date_to           IN DATE
                               ,csupplier_name            IN VARCHAR2
                               ,csupplier_site            IN VARCHAR2
                               ,cdocument_type            IN VARCHAR2
                               ,cpo_number_from           IN VARCHAR2
                               ,cpo_number_to             IN VARCHAR2)
    IS
        xprogress                    VARCHAR2 (30);
        v_levelprocessed             VARCHAR2 (40);

        catt_header_interface        VARCHAR2 (120) := 'ECE_ATTACHMENT_HEADERS';
        catt_detail_interface        VARCHAR2 (120) := 'ECE_ATTACHMENT_DETAILS';

        l_header_tbl                 ece_flatfile_pvt.interface_tbl_type;
        l_line_tbl                   ece_flatfile_pvt.interface_tbl_type;
        l_shipment_tbl               ece_flatfile_pvt.interface_tbl_type;
        l_key_tbl                    ece_flatfile_pvt.interface_tbl_type;

        l_hdr_att_hdr_tbl            ece_flatfile_pvt.interface_tbl_type;
        l_hdr_att_dtl_tbl            ece_flatfile_pvt.interface_tbl_type;
        l_ln_att_hdr_tbl             ece_flatfile_pvt.interface_tbl_type;
        l_ln_att_dtl_tbl             ece_flatfile_pvt.interface_tbl_type;
        l_mi_att_hdr_tbl             ece_flatfile_pvt.interface_tbl_type;
        l_mi_att_dtl_tbl             ece_flatfile_pvt.interface_tbl_type;
        l_msi_att_hdr_tbl            ece_flatfile_pvt.interface_tbl_type;
        l_msi_att_dtl_tbl            ece_flatfile_pvt.interface_tbl_type;
        l_shp_att_hdr_tbl            ece_flatfile_pvt.interface_tbl_type;
        l_shp_att_dtl_tbl            ece_flatfile_pvt.interface_tbl_type;

        iatt_hdr_pos                 NUMBER := 0;
        iatt_ln_pos                  NUMBER := 0;
        iatt_mi_pos                  NUMBER := 0;
        iatt_msi_pos                 NUMBER := 0;
        iatt_shp_pos                 NUMBER := 0;

        v_project_acct_installed     BOOLEAN;
        v_project_acct_short_name    VARCHAR2 (2) := 'PA';
        v_project_acct_status        VARCHAR2 (120);
        v_project_acct_industry      VARCHAR2 (120);
        v_project_acct_schema        VARCHAR2 (120);

        v_att_enabled                VARCHAR2 (10);
        v_header_att_enabled         VARCHAR2 (10);
        v_line_att_enabled           VARCHAR2 (10);
        v_mitem_att_enabled          VARCHAR2 (10);
        v_iitem_att_enabled          VARCHAR2 (10);
        v_ship_att_enabled           VARCHAR2 (10);
        n_att_seg_size               NUMBER;

        v_entity_name                VARCHAR2 (120);
        v_pk1_value                  VARCHAR2 (120);
        v_pk2_value                  VARCHAR2 (120);

        header_sel_c                 INTEGER;
        line_sel_c                   INTEGER;
        shipment_sel_c               INTEGER;

        cheader_select               VARCHAR2 (32000);
        cline_select                 VARCHAR2 (32000);
        cshipment_select             VARCHAR2 (32000);

        cheader_from                 VARCHAR2 (32000);
        cline_from                   VARCHAR2 (32000);
        cshipment_from               VARCHAR2 (32000);

        cheader_where                VARCHAR2 (32000);
        cline_where                  VARCHAR2 (32000);
        cshipment_where              VARCHAR2 (32000);

        iheader_count                NUMBER := 0;
        iline_count                  NUMBER := 0;
        ishipment_count              NUMBER := 0;
        --iKey_count                 NUMBER := 0;

        l_header_fkey                NUMBER;
        l_line_fkey                  NUMBER;
        l_shipment_fkey              NUMBER;

        nheader_key_pos              NUMBER;
        nline_key_pos                NUMBER;
        nshipment_key_pos            NUMBER;

        ntrans_code_pos              NUMBER;                                                                  -- 2823215

        c_file_common_key            VARCHAR2 (255);                                                          -- 2823215

        dummy                        INTEGER;
        n_trx_date_pos               NUMBER;
        ndocument_type_pos           NUMBER;
        npo_number_pos               NUMBER;
        npo_type_pos                 NUMBER;
        nrelease_num_pos             NUMBER;
        nrelease_id_pos              NUMBER;
        nline_num_pos                NUMBER;
        nline_location_id_pos        NUMBER;
        -- Bug 2823215
        nship_line_location_id_pos   NUMBER;
        nship_release_num_pos        NUMBER;
        nline_uom_code_pos           NUMBER;
        nline_location_uom_pos       NUMBER;
        nlp_att_cat_pos              NUMBER;
        nlp_att1_pos                 NUMBER;
        nlp_att2_pos                 NUMBER;
        nlp_att3_pos                 NUMBER;
        nlp_att4_pos                 NUMBER;
        nlp_att5_pos                 NUMBER;
        nlp_att6_pos                 NUMBER;
        nlp_att7_pos                 NUMBER;
        nlp_att8_pos                 NUMBER;
        nlp_att9_pos                 NUMBER;
        nlp_att10_pos                NUMBER;
        nlp_att11_pos                NUMBER;
        nlp_att12_pos                NUMBER;
        nlp_att13_pos                NUMBER;
        nlp_att14_pos                NUMBER;
        nlp_att15_pos                NUMBER;
        nst_cust_name_pos            NUMBER;
        nst_cont_name_pos            NUMBER;
        nst_cont_phone_pos           NUMBER;
        nst_cont_fax_pos             NUMBER;
        nst_cont_email_pos           NUMBER;
        nshipping_instruct_pos       NUMBER;
        npacking_instruct_pos        NUMBER;
        nshipping_method_pos         NUMBER;
        ncust_po_num_pos             NUMBER;
        ncust_po_line_num_pos        NUMBER;
        ncust_po_ship_num_pos        NUMBER;
        ncust_prod_desc_pos          NUMBER;
        ndeliv_cust_loc_pos          NUMBER;
        ndeliv_cust_name_pos         NUMBER;
        ndeliv_cont_name_pos         NUMBER;
        ndeliv_cont_phone_pos        NUMBER;
        ndeliv_cont_fax_pos          NUMBER;
        ndeliv_cust_addr_pos         NUMBER;
        ndeliv_cont_email_pos        NUMBER;
        --Bug 2823215
        nshp_uom_pos                 NUMBER;
        nline_uom_pos                NUMBER;
        l_document_type              VARCHAR2 (30);
        norganization_id             NUMBER;
        nitem_id_pos                 NUMBER;

        -- Timezone enhancement
        nrel_date_pos                PLS_INTEGER;
        nrel_dt_tz_pos               PLS_INTEGER;
        nrel_dt_off_pos              PLS_INTEGER;
        ncrtn_date_pos               PLS_INTEGER;
        ncrtn_dt_tz_pos              PLS_INTEGER;
        ncrtn_dt_off_pos             PLS_INTEGER;
        nrev_date_pos                PLS_INTEGER;
        nrev_dt_tz_pos               PLS_INTEGER;
        nrev_dt_off_pos              PLS_INTEGER;
        nacc_due_dt_pos              PLS_INTEGER;
        nacc_due_tz_pos              PLS_INTEGER;
        nacc_due_off_pos             PLS_INTEGER;
        nblkt_srt_dt_pos             PLS_INTEGER;
        nblkt_srt_tz_pos             PLS_INTEGER;
        nblkt_srt_off_pos            PLS_INTEGER;
        nblkt_end_dt_pos             PLS_INTEGER;
        nblkt_end_tz_pos             PLS_INTEGER;
        nblkt_end_off_pos            PLS_INTEGER;
        npcard_exp_dt_pos            PLS_INTEGER;
        npcard_exp_tz_pos            PLS_INTEGER;
        npcard_exp_off_pos           PLS_INTEGER;
        nline_can_dt_pos             PLS_INTEGER;
        nline_can_tz_pos             PLS_INTEGER;
        nline_can_off_pos            PLS_INTEGER;
        nexprn_dt_pos                PLS_INTEGER;
        nexprn_tz_pos                PLS_INTEGER;
        nexprn_off_pos               PLS_INTEGER;
        nship_need_dt_pos            PLS_INTEGER;
        nship_need_tz_pos            PLS_INTEGER;
        nship_need_off_pos           PLS_INTEGER;
        nship_prom_dt_pos            PLS_INTEGER;
        nship_prom_tz_pos            PLS_INTEGER;
        nship_prom_off_pos           PLS_INTEGER;
        nship_accept_dt_pos          PLS_INTEGER;
        nship_accept_tz_pos          PLS_INTEGER;
        nship_accept_off_pos         PLS_INTEGER;
        nshp_can_dt_pos              PLS_INTEGER;
        nshp_can_tz_pos              PLS_INTEGER;
        nshp_can_off_pos             PLS_INTEGER;
        nshp_strt_dt_pos             PLS_INTEGER;
        nshp_strt_tz_pos             PLS_INTEGER;
        nshp_strt_off_pos            PLS_INTEGER;
        nshp_end_dt_pos              PLS_INTEGER;
        nshp_end_tz_pos              PLS_INTEGER;
        nshp_end_off_pos             PLS_INTEGER;
        -- Timezone enhancement

        v_drop_ship_flag             NUMBER;
        rec_order_line_info          oe_drop_ship_grp.order_line_info_rec_type;                                --2887790
        nheader_cancel_flag_pos      NUMBER;
        nline_cancel_flag_pos        NUMBER;
        nshipment_cancel_flag_pos    NUMBER;
        v_header_cancel_flag         VARCHAR2 (10);
        v_line_cancel_flag           VARCHAR2 (10);
        v_shipment_cancel_flag       VARCHAR2 (10);

        init_msg_list                VARCHAR2 (20);
        simulate                     VARCHAR2 (20);
        validation_level             VARCHAR2 (20);
        commt                        VARCHAR2 (20);
        return_status                VARCHAR2 (20);
        msg_count                    NUMBER;
        msg_data                     VARCHAR2 (2000);                                                         -- 3650215

        line_part_number             VARCHAR2 (80);
        line_part_attrib_category    VARCHAR2 (80);

        -- bug 6511409
        line_part_attribute1         mtl_item_flexfields.attribute1%TYPE;
        line_part_attribute2         mtl_item_flexfields.attribute2%TYPE;
        line_part_attribute3         mtl_item_flexfields.attribute3%TYPE;
        line_part_attribute4         mtl_item_flexfields.attribute4%TYPE;
        line_part_attribute5         mtl_item_flexfields.attribute5%TYPE;
        line_part_attribute6         mtl_item_flexfields.attribute6%TYPE;
        line_part_attribute7         mtl_item_flexfields.attribute7%TYPE;
        line_part_attribute8         mtl_item_flexfields.attribute8%TYPE;
        line_part_attribute9         mtl_item_flexfields.attribute9%TYPE;
        line_part_attribute10        mtl_item_flexfields.attribute10%TYPE;
        line_part_attribute11        mtl_item_flexfields.attribute11%TYPE;
        line_part_attribute12        mtl_item_flexfields.attribute12%TYPE;
        line_part_attribute13        mtl_item_flexfields.attribute13%TYPE;
        line_part_attribute14        mtl_item_flexfields.attribute14%TYPE;
        line_part_attribute15        mtl_item_flexfields.attribute15%TYPE;

        d_dummy_date                 DATE;

        imap_id                      NUMBER;
        c_header_common_key_name     VARCHAR2 (40);
        c_line_common_key_name       VARCHAR2 (40);
        c_shipment_key_name          VARCHAR2 (40);
        n_header_common_key_pos      NUMBER;
        n_line_common_key_pos        NUMBER;
        n_ship_common_key_pos        NUMBER;

        fail_convert_to_ext          EXCEPTION;

        CURSOR c_org_id (p_line_id NUMBER)
        IS
            SELECT DISTINCT ship_to_organization_id
              FROM po_line_locations
             WHERE po_line_id = p_line_id;
    BEGIN
        IF ec_debug.g_debug_level >= 2
        THEN
            ec_debug.push ('ECE_POO_TRANSACTION.POPULATE_POO_TRX');
            ec_debug.pl (3, 'cCommunication_Method: ', ccommunication_method);
            ec_debug.pl (3, 'cTransaction_Type: ', ctransaction_type);
            ec_debug.pl (3, 'iOutput_width: ', ioutput_width);
            ec_debug.pl (3, 'dTransaction_date: ', dtransaction_date);
            ec_debug.pl (3, 'iRun_id: ', irun_id);
            ec_debug.pl (3, 'cHeader_Interface: ', cheader_interface);
            ec_debug.pl (3, 'cLine_Interface: ', cline_interface);
            ec_debug.pl (3, 'cShipment_Interface: ', cshipment_interface);
            ec_debug.pl (3, 'cDistribution_Interface: ', cdistribution_interface);
            ec_debug.pl (3, 'cCreate_Date_From: ', ccreate_date_from);
            ec_debug.pl (3, 'cCreate_Date_To: ', ccreate_date_to);
            ec_debug.pl (3, 'cSupplier_Name: ', csupplier_name);
            ec_debug.pl (3, 'cSupplier_Site: ', csupplier_site);
            ec_debug.pl (3, 'cDocument_Type: ', cdocument_type);
            ec_debug.pl (3, 'cPO_Number_From: ', cpo_number_from);
            ec_debug.pl (3, 'cPO_Number_To: ', cpo_number_to);
        END IF;

        xprogress := 'POOB-10-1000';

        BEGIN
            SELECT inventory_organization_id INTO norganization_id FROM financials_system_parameters;
        EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
                ec_debug.pl (0
                            ,'EC'
                            ,'ECE_NO_ROW_SELECTED'
                            ,'PROGRESS_LEVEL'
                            ,xprogress
                            ,'INFO'
                            ,'INVENTORY ORGANIZATION ID'
                            ,'TABLE_NAME'
                            ,'FINANCIALS_SYSTEM_PARAMETERS');
        END;

        IF ec_debug.g_debug_level = 3
        THEN
            ec_debug.pl (3, 'norganization_id: ', norganization_id);
        END IF;

        --Bug 1891291 begin
        -- Let's See if Project Accounting is Installed
        xprogress := 'POOB-10-1001';
        /*  v_project_acct_installed := fnd_installation.get_app_info(
                                        v_project_acct_short_name, -- i.e. 'PA'
                                        v_project_acct_status,     -- 'I' means it's installed
                                        v_project_acct_industry,
                                        v_project_acct_schema);

          v_project_acct_status := NVL(v_project_acct_status,'X');
          ec_debug.pl(3,'v_project_acct_status: '  ,v_project_acct_status);
          ec_debug.pl(3,'v_project_acct_industry: ',v_project_acct_industry);
          ec_debug.pl(3,'v_project_acct_schema: '  ,v_project_acct_schema);
        */
        --Bug 1891291 end
        -- Get Profile Option Values for Attachments
        xprogress := 'POOB-10-1002';
        fnd_profile.get ('ECE_' || ctransaction_type || '_HEAD_ATT', v_header_att_enabled);
        fnd_profile.get ('ECE_' || ctransaction_type || '_LINE_ATT', v_line_att_enabled);
        fnd_profile.get ('ECE_' || ctransaction_type || '_MITEM_ATT', v_mitem_att_enabled);
        fnd_profile.get ('ECE_' || ctransaction_type || '_IITEM_ATT', v_iitem_att_enabled);
        fnd_profile.get ('ECE_' || ctransaction_type || '_SHIP_ATT', v_ship_att_enabled);
        fnd_profile.get ('ECE_' || ctransaction_type || '_ATT_SEG_SIZE', n_att_seg_size);

        -- Check to see if any attachments are enabled
        xprogress := 'POOB-10-1004';

        IF    NVL (v_header_att_enabled, 'N') = 'Y'
           OR NVL (v_mitem_att_enabled, 'N') = 'Y'
           OR NVL (v_iitem_att_enabled, 'N') = 'Y'
           OR NVL (v_ship_att_enabled, 'N') = 'Y'
        THEN
            v_att_enabled := 'Y';
        END IF;

        IF v_att_enabled = 'Y'
        THEN
            BEGIN
                IF n_att_seg_size < 1 OR n_att_seg_size > g_max_att_seg_size OR n_att_seg_size IS NULL
                THEN
                    RAISE INVALID_NUMBER;
                END IF;
            EXCEPTION
                WHEN VALUE_ERROR OR INVALID_NUMBER
                THEN
                    ec_debug.pl (0
                                ,'EC'
                                ,'ECE_INVALID_SEGMENT_NUM'
                                ,'SEGMENT_VALUE'
                                ,n_att_seg_size
                                ,'SEGMENT_DEFAULT'
                                ,g_default_att_seg_size);
                    n_att_seg_size := g_default_att_seg_size;
            END;
        END IF;

        IF ec_debug.g_debug_level = 3
        THEN
            ec_debug.pl (3, 'v_header_att_enabled: ', v_header_att_enabled);
            ec_debug.pl (3, 'v_line_att_enabled: ', v_line_att_enabled);
            ec_debug.pl (3, 'v_mitem_att_enabled: ', v_mitem_att_enabled);
            ec_debug.pl (3, 'v_iitem_att_enabled: ', v_iitem_att_enabled);
            ec_debug.pl (3, 'v_ship_att_enabled: ', v_ship_att_enabled);
            ec_debug.pl (3, 'v_att_enabled: ', v_att_enabled);
            ec_debug.pl (3, 'n_att_seg_size: ', n_att_seg_size);
        END IF;

        xprogress := 'POOB-10-1010';
        ece_flatfile_pvt.init_table (ctransaction_type
                                    ,cheader_interface
                                    ,NULL
                                    ,FALSE
                                    ,l_header_tbl
                                    ,l_key_tbl);

        xprogress := 'POOB-10-1020';
        l_key_tbl := l_header_tbl;

        xprogress := 'POOB-10-1025';
        --iKey_count := l_header_tbl.COUNT;
        --ec_debug.pl(3,'iKey_count: ',iKey_count );

        xprogress := 'POOB-10-1030';
        ece_flatfile_pvt.init_table (ctransaction_type
                                    ,cline_interface
                                    ,NULL
                                    ,TRUE
                                    ,l_line_tbl
                                    ,l_key_tbl);

        xprogress := 'POOB-10-1040';
        ece_flatfile_pvt.init_table (ctransaction_type
                                    ,cshipment_interface
                                    ,NULL
                                    ,TRUE
                                    ,l_shipment_tbl
                                    ,l_key_tbl);

        -- ****************************************************************************
        -- Here, I am building the SELECT, FROM, and WHERE  clauses for the dynamic SQL
        -- call. The ece_extract_utils_pub.select_clause uses the EDI data dictionary
        -- for the build.
        -- ****************************************************************************

        BEGIN
            SELECT map_id
              INTO imap_id
              FROM ece_mappings
             WHERE map_code = 'EC_' || ctransaction_type || '_FF';
        EXCEPTION
            WHEN OTHERS
            THEN
                NULL;
        END;

        xprogress := 'POOB-10-1050';
        ece_extract_utils_pub.select_clause (ctransaction_type
                                            ,ccommunication_method
                                            ,cheader_interface
                                            ,l_header_tbl
                                            ,cheader_select
                                            ,cheader_from
                                            ,cheader_where);

        BEGIN
            SELECT eit.key_column_name
              INTO c_header_common_key_name
              FROM ece_interface_tables eit
             WHERE     eit.transaction_type = ctransaction_type
                   AND eit.interface_table_name = cheader_interface
                   AND eit.map_id = imap_id;
        EXCEPTION
            WHEN OTHERS
            THEN
                NULL;
        END;

        xprogress := 'POOB-10-1060';
        ece_extract_utils_pub.select_clause (ctransaction_type
                                            ,ccommunication_method
                                            ,cline_interface
                                            ,l_line_tbl
                                            ,cline_select
                                            ,cline_from
                                            ,cline_where);

        BEGIN
            SELECT eit.key_column_name
              INTO c_line_common_key_name
              FROM ece_interface_tables eit
             WHERE     eit.transaction_type = ctransaction_type
                   AND eit.interface_table_name = cline_interface
                   AND eit.map_id = imap_id;
        EXCEPTION
            WHEN OTHERS
            THEN
                NULL;
        END;

        xprogress := 'POOB-10-1070';
        ece_extract_utils_pub.select_clause (ctransaction_type
                                            ,ccommunication_method
                                            ,cshipment_interface
                                            ,l_shipment_tbl
                                            ,cshipment_select
                                            ,cshipment_from
                                            ,cshipment_where);

        BEGIN
            SELECT eit.key_column_name
              INTO c_shipment_key_name
              FROM ece_interface_tables eit
             WHERE     eit.transaction_type = ctransaction_type
                   AND eit.interface_table_name = cshipment_interface
                   AND eit.map_id = imap_id;
        EXCEPTION
            WHEN OTHERS
            THEN
                NULL;
        END;

        -- **************************************************************************
        -- Here, I am customizing the WHERE clause to join the Interface tables together.
        -- i.e. Headers -- Lines -- Line Details
        --
        -- Select   Data1, Data2, Data3...........
        -- From  Header_View
        -- Where A.Transaction_Record_ID = D.Transaction_Record_ID (+)
        -- and   B.Transaction_Record_ID = E.Transaction_Record_ID (+)
        -- and   C.Transaction_Record_ID = F.Transaction_Record_ID (+)
        -- ******* (Customization should be added here) ********
        -- and   A.Communication_Method = 'EDI'
        -- and   A.xxx = B.xxx   ........
        -- and   B.yyy = C.yyy   .......
        -- **************************************************************************
        -- **************************************************************************
        -- :po_header_id is a place holder for foreign key value.
        -- A PL/SQL table (list of values) will be used to store data.
        -- Procedure ece_flatfile.Find_pos will be used to locate the specific
        -- data value in the PL/SQL table.
        -- dbms_sql (Native Oracle db functions that come with every Oracle Apps)
        -- dbms_sql.bind_variable will be used to assign data value to :transaction_id.
        --
        -- Let's use the above example:
        --
        -- 1. Execute dynamic SQL 1 for headers (A) data
        --    Get value of A.xxx (foreign key to B)
        --
        -- 2. bind value A.xxx to variable B.xxx
        --
        -- 3. Execute dynamic SQL 2 for lines (B) data
        --    Get value of B.yyy (foreigh key to C)
        --
        -- 4. bind value B.yyy to variable C.yyy
        --
        -- 5. Execute dynamic SQL 3 for line_details (C) data
        -- **************************************************************************
        -- **************************************************************************
        --   Change the following few lines as needed
        -- **************************************************************************
        xprogress := 'POOB-10-1080';
        cheader_where := cheader_where || 'ece_poo_headers_v.communication_method =' || ':cComm_Method';

        xprogress := 'POOB-10-1090';

        IF ccreate_date_from IS NOT NULL
        THEN
            cheader_where := cheader_where || ' AND ' || 'ece_poo_headers_v.creation_date >=' || ':cCreate_Dt_From';
        END IF;

        xprogress := 'POOB-10-1100';

        IF ccreate_date_to IS NOT NULL
        THEN
            cheader_where := cheader_where || ' AND ' || 'ece_poo_headers_v.creation_date <=' || ':cCreate_Dt_To';
        END IF;

        xprogress := 'POOB-10-1110';

        IF csupplier_name IS NOT NULL
        THEN
            cheader_where := cheader_where || ' AND ' || 'ece_poo_headers_v.supplier_number =' || ':cSuppl_Name';
        END IF;

        xprogress := 'POOB-10-1120';

        IF csupplier_site IS NOT NULL
        THEN
            cheader_where := cheader_where || ' AND ' || 'ece_poo_headers_v.vendor_site_id =' || ':cSuppl_Site';
        END IF;

        xprogress := 'POOB-10-1130';

        IF cdocument_type IS NOT NULL
        THEN
            cheader_where := cheader_where || ' AND ' || 'ece_poo_headers_v.document_type =' || ':cDoc_Type';
        END IF;

        xprogress := 'POOB-10-1140';

        IF cpo_number_from IS NOT NULL
        THEN
            cheader_where := cheader_where || ' AND ' || 'ece_poo_headers_v.po_number >=' || ':cPO_Num_From';
        END IF;

        xprogress := 'POOB-10-1150';

        IF cpo_number_to IS NOT NULL
        THEN
            cheader_where := cheader_where || ' AND ' || 'ece_poo_headers_v.po_number <=' || ':cPO_Num_To';
        END IF;

        xprogress := 'POOB-10-1160';
        cheader_where := cheader_where || ' ORDER BY po_number, por_release_num';

        xprogress := 'POOB-10-1170';
        cline_where :=
               cline_where
            || ' ece_poo_lines_v.po_header_id = :po_header_id AND'
            || ' ece_poo_lines_v.por_release_num = :por_release_num '
            || ' ORDER BY line_num';

        xprogress := 'POOB-10-1180';

        cshipment_where :=
               cshipment_where
            || ' ece_poo_shipments_v.po_header_id = :po_header_id AND'
            || ' ece_poo_shipments_v.po_line_id = :po_line_id AND'
            || ' ece_poo_shipments_v.por_release_id = :por_release_id'
            || ' ORDER BY shipment_number';                                                                    --2823215

        -- 3957851
        --                   ' ece_poo_shipments_v.por_release_id = :por_release_id AND'   ||
        --                   ' ((ece_poo_shipments_v.por_release_id = 0) OR'               ||
        --                   ' (ece_poo_shipments_v.por_release_id <> 0 AND'               ||
        --                   ' ece_poo_shipments_v.shipment_number = :shipment_number))'   ||
        --                   ' ORDER BY shipment_number';   --2823215

        xprogress := 'POOB-10-1190';
        cheader_select := cheader_select || cheader_from || cheader_where;

        cline_select := cline_select || cline_from || cline_where;

        cshipment_select := cshipment_select || cshipment_from || cshipment_where;

        IF ec_debug.g_debug_level = 3
        THEN
            ec_debug.pl (3, 'cHeader_select: ', cheader_select);
            ec_debug.pl (3, 'cLine_select: ', cline_select);
            ec_debug.pl (3, 'cShipment_select: ', cshipment_select);
        END IF;

        -- ***************************************************
        -- ***   Get data setup for the dynamic SQL call.
        -- ***   Open a cursor for each of the SELECT call
        -- ***   This tells the database to reserve spaces
        -- ***   for the data returned by the SQL statement
        -- ***************************************************
        xprogress := 'POOB-10-1200';
        header_sel_c := DBMS_SQL.open_cursor;

        xprogress := 'POOB-10-1210';
        line_sel_c := DBMS_SQL.open_cursor;

        xprogress := 'POOB-10-1220';
        shipment_sel_c := DBMS_SQL.open_cursor;

        -- ***************************************************
        -- Parse each of the SELECT statement
        -- so the database understands the command
        -- ***************************************************
        xprogress := 'POOB-10-1230';

        BEGIN
            DBMS_SQL.parse (header_sel_c, cheader_select, DBMS_SQL.native);
        EXCEPTION
            WHEN OTHERS
            THEN
                ece_error_handling_pvt.print_parse_error (DBMS_SQL.last_error_position, cheader_select);
                app_exception.raise_exception;
        END;

        xprogress := 'POOB-10-1240';

        BEGIN
            DBMS_SQL.parse (line_sel_c, cline_select, DBMS_SQL.native);
        EXCEPTION
            WHEN OTHERS
            THEN
                ece_error_handling_pvt.print_parse_error (DBMS_SQL.last_error_position, cline_select);
                app_exception.raise_exception;
        END;

        xprogress := 'POOB-10-1250';

        BEGIN
            DBMS_SQL.parse (shipment_sel_c, cshipment_select, DBMS_SQL.native);
        EXCEPTION
            WHEN OTHERS
            THEN
                ece_error_handling_pvt.print_parse_error (DBMS_SQL.last_error_position, cshipment_select);
                app_exception.raise_exception;
        END;

        -- ************
        -- set counter
        -- ************
        xprogress := 'POOB-10-1260';
        iheader_count := l_header_tbl.COUNT;

        xprogress := 'POOB-10-1270';
        iline_count := l_line_tbl.COUNT;

        xprogress := 'POOB-10-1280';
        ishipment_count := l_shipment_tbl.COUNT;

        IF ec_debug.g_debug_level = 3
        THEN
            ec_debug.pl (3, 'iHeader_count: ', iheader_count);
            ec_debug.pl (3, 'iLine_count: ', iline_count);
            ec_debug.pl (3, 'iShipment_count: ', ishipment_count);
        END IF;

        -- ******************************************************
        --  Define TYPE for every columns in the SELECT statement
        --  For each piece of the data returns, we need to tell
        --  the database what type of information it will be.
        --  e.g. ID is NUMBER, due_date is DATE
        --  However, for simplicity, we will convert
        --  everything to varchar2.
        -- ******************************************************
        xprogress := 'POOB-10-1290';
        ece_flatfile_pvt.define_interface_column_type (header_sel_c
                                                      ,cheader_select
                                                      ,ece_extract_utils_pub.g_maxcolwidth
                                                      ,l_header_tbl);

        xprogress := 'POOB-10-1300';
        ece_flatfile_pvt.define_interface_column_type (line_sel_c
                                                      ,cline_select
                                                      ,ece_extract_utils_pub.g_maxcolwidth
                                                      ,l_line_tbl);

        xprogress := 'POOB-10-1310';
        ece_flatfile_pvt.define_interface_column_type (shipment_sel_c
                                                      ,cshipment_select
                                                      ,ece_extract_utils_pub.g_maxcolwidth
                                                      ,l_shipment_tbl);

        -- **************************************************************
        -- ***  The following is custom tailored for this transaction
        -- ***  It finds the values and use them in the WHERE clause to
        -- ***  join tables together.
        -- **************************************************************
        -- ***************************************************
        -- To complete the Line SELECT statement,
        --  we will need values for the join condition.
        -- ***************************************************
        -- Header Level Positions
        xprogress := 'POOB-10-1320';
        ece_extract_utils_pub.find_pos (l_header_tbl, ece_extract_utils_pub.g_transaction_date, n_trx_date_pos);

        xprogress := 'POOB-10-1330';
        ece_extract_utils_pub.find_pos (l_header_tbl, 'PO_HEADER_ID', nheader_key_pos);

        xprogress := 'POOB-10-1340';
        ece_extract_utils_pub.find_pos (l_header_tbl, 'DOCUMENT_TYPE', ndocument_type_pos);

        xprogress := 'POOB-10-1350';
        ece_extract_utils_pub.find_pos (l_header_tbl, 'PO_NUMBER', npo_number_pos);

        xprogress := 'POOB-10-1360';
        ece_extract_utils_pub.find_pos (l_header_tbl, 'PO_TYPE', npo_type_pos);

        xprogress := 'POOB-10-1370';
        ece_extract_utils_pub.find_pos (l_header_tbl, 'POR_RELEASE_NUM', nrelease_num_pos);

        xprogress := 'POOB-10-1380';
        ece_extract_utils_pub.find_pos (l_header_tbl, 'POR_RELEASE_ID', nrelease_id_pos);

        xprogress := 'POOB-10-1381';
        ece_extract_utils_pub.find_pos (l_header_tbl, 'CANCEL_FLAG', nheader_cancel_flag_pos);

        xprogress := 'POOB-10-1382';
        ece_flatfile_pvt.find_pos (l_header_tbl, ece_flatfile_pvt.g_translator_code, ntrans_code_pos);         --2823215

        -- Line Level Positions
        xprogress := 'POOB-10-1390';
        ece_extract_utils_pub.find_pos (l_line_tbl, 'PO_LINE_LOCATION_ID', nline_location_id_pos);

        xprogress := 'POOB-10-1400';
        ece_extract_utils_pub.find_pos (l_line_tbl, 'LINE_NUM', nline_num_pos);

        xprogress := 'POOB-10-1402';
        ece_extract_utils_pub.find_pos (l_line_tbl, 'PO_LINE_ID', nline_key_pos);

        xprogress := 'POOB-10-1404';
        ece_extract_utils_pub.find_pos (l_line_tbl, 'ITEM_ID', nitem_id_pos);

        xprogress := 'POOB-10-1405';
        ece_extract_utils_pub.find_pos (l_line_tbl, 'CANCEL_FLAG', nline_cancel_flag_pos);
        -- 2823215
        xprogress := 'POOB-10-1406';
        ece_extract_utils_pub.find_pos (l_line_tbl, 'UOM_CODE', nline_uom_code_pos);

        xprogress := 'POOB-10-1407';
        ece_extract_utils_pub.find_pos (l_line_tbl, 'LP_ATTRIBUTE_CATEGORY', nlp_att_cat_pos);

        xprogress := 'POOB-10-1408';
        ece_extract_utils_pub.find_pos (l_line_tbl, 'LP_ATTRIBUTE1', nlp_att1_pos);

        xprogress := 'POOB-10-1409';
        ece_extract_utils_pub.find_pos (l_line_tbl, 'LP_ATTRIBUTE2', nlp_att2_pos);

        xprogress := 'POOB-10-1410';
        ece_extract_utils_pub.find_pos (l_line_tbl, 'LP_ATTRIBUTE3', nlp_att3_pos);

        xprogress := 'POOB-10-1411';
        ece_extract_utils_pub.find_pos (l_line_tbl, 'LP_ATTRIBUTE4', nlp_att4_pos);

        xprogress := 'POOB-10-1412';
        ece_extract_utils_pub.find_pos (l_line_tbl, 'LP_ATTRIBUTE5', nlp_att5_pos);

        xprogress := 'POOB-10-1413';
        ece_extract_utils_pub.find_pos (l_line_tbl, 'LP_ATTRIBUTE6', nlp_att6_pos);

        xprogress := 'POOB-10-1414';
        ece_extract_utils_pub.find_pos (l_line_tbl, 'LP_ATTRIBUTE7', nlp_att7_pos);

        xprogress := 'POOB-10-1415';
        ece_extract_utils_pub.find_pos (l_line_tbl, 'LP_ATTRIBUTE8', nlp_att8_pos);

        xprogress := 'POOB-10-1416';
        ece_extract_utils_pub.find_pos (l_line_tbl, 'LP_ATTRIBUTE9', nlp_att9_pos);

        xprogress := 'POOB-10-1417';
        ece_extract_utils_pub.find_pos (l_line_tbl, 'LP_ATTRIBUTE10', nlp_att10_pos);

        xprogress := 'POOB-10-1418';
        ece_extract_utils_pub.find_pos (l_line_tbl, 'LP_ATTRIBUTE11', nlp_att11_pos);

        xprogress := 'POOB-10-1419';
        ece_extract_utils_pub.find_pos (l_line_tbl, 'LP_ATTRIBUTE12', nlp_att12_pos);

        xprogress := 'POOB-10-1420';
        ece_extract_utils_pub.find_pos (l_line_tbl, 'LP_ATTRIBUTE13', nlp_att13_pos);

        xprogress := 'POOB-10-1421';
        ece_extract_utils_pub.find_pos (l_line_tbl, 'LP_ATTRIBUTE14', nlp_att14_pos);

        xprogress := 'POOB-10-1422';
        ece_extract_utils_pub.find_pos (l_line_tbl, 'LP_ATTRIBUTE15', nlp_att15_pos);                         -- 2823215

        -- Shipment Level Positions
        xprogress := 'POOB-10-1423';
        ece_extract_utils_pub.find_pos (l_shipment_tbl, 'LINE_LOCATION_ID', nship_line_location_id_pos);

        xprogress := 'POOB-10-1424';
        ece_extract_utils_pub.find_pos (l_shipment_tbl, 'CANCELLED_FLAG', nshipment_cancel_flag_pos);

        xprogress := 'POOB-10-1025';
        ece_extract_utils_pub.find_pos (l_shipment_tbl, 'POR_RELEASE_NUM', nship_release_num_pos);

        xprogress := 'POOB-10-1026';
        ece_extract_utils_pub.find_pos (l_shipment_tbl, 'UOM_CODE', nline_location_uom_pos);

        xprogress := 'POOB-10-1427';
        ece_extract_utils_pub.find_pos (l_shipment_tbl, 'SHIPMENT_NUMBER', nshipment_key_pos);

        xprogress := 'POOB-10-1428';
        ece_flatfile_pvt.find_pos (l_shipment_tbl, 'SHIP_TO_CUSTOMER_NAME', nst_cust_name_pos);

        xprogress := 'POOB-10-1429';
        ece_flatfile_pvt.find_pos (l_shipment_tbl, 'SHIP_TO_CONTACT_NAME', nst_cont_name_pos);

        xprogress := 'POOB-10-1430';
        ece_flatfile_pvt.find_pos (l_shipment_tbl, 'SHIP_TO_CONTACT_PHONE', nst_cont_phone_pos);

        xprogress := 'POOB-10-1431';
        ece_flatfile_pvt.find_pos (l_shipment_tbl, 'SHIP_TO_CONTACT_FAX', nst_cont_fax_pos);

        xprogress := 'POOB-10-1432';
        ece_flatfile_pvt.find_pos (l_shipment_tbl, 'SHIP_TO_CONTACT_EMAIL', nst_cont_email_pos);

        xprogress := 'POOB-10-1433';
        ece_flatfile_pvt.find_pos (l_shipment_tbl, 'SHIPPING_INSTRUCTIONS', nshipping_instruct_pos);

        xprogress := 'POOB-10-1434';
        ece_flatfile_pvt.find_pos (l_shipment_tbl, 'PACKING_INSTRUCTIONS', npacking_instruct_pos);

        xprogress := 'POOB-10-1435';
        ece_flatfile_pvt.find_pos (l_shipment_tbl, 'SHIPPING_METHOD', nshipping_method_pos);

        xprogress := 'POOB-10-1437';
        ece_flatfile_pvt.find_pos (l_shipment_tbl, 'CUSTOMER_PO_NUMBER', ncust_po_num_pos);

        xprogress := 'POOB-10-1438';
        ece_flatfile_pvt.find_pos (l_shipment_tbl, 'CUSTOMER_PO_LINE_NUM', ncust_po_line_num_pos);

        xprogress := 'POOB-10-1439';
        ece_flatfile_pvt.find_pos (l_shipment_tbl, 'CUSTOMER_PO_SHIPMENT_NUM', ncust_po_ship_num_pos);

        xprogress := 'POOB-10-1440';
        ece_flatfile_pvt.find_pos (l_shipment_tbl, 'CUSTOMER_ITEM_DESCRIPTION', ncust_prod_desc_pos);

        xprogress := 'POOB-10-1441';
        ece_flatfile_pvt.find_pos (l_shipment_tbl, 'DELIVER_TO_LOCATION', ndeliv_cust_loc_pos);

        xprogress := 'POOB-10-1442';
        ece_flatfile_pvt.find_pos (l_shipment_tbl, 'DELIVER_TO_CUSTOMER_NAME', ndeliv_cust_name_pos);

        xprogress := 'POOB-10-1443';
        ece_flatfile_pvt.find_pos (l_shipment_tbl, 'DELIVER_TO_CONTACT_NAME', ndeliv_cont_name_pos);

        xprogress := 'POOB-10-1444';
        ece_flatfile_pvt.find_pos (l_shipment_tbl, 'DELIVER_TO_CONTACT_PHONE', ndeliv_cont_phone_pos);

        xprogress := 'POOB-10-1445';
        ece_flatfile_pvt.find_pos (l_shipment_tbl, 'DELIVER_TO_CONTACT_FAX', ndeliv_cont_fax_pos);

        xprogress := 'POOB-10-1446';
        ece_flatfile_pvt.find_pos (l_shipment_tbl, 'DELIVER_TO_CUSTOMER_ADDRESS', ndeliv_cust_addr_pos);

        xprogress := 'POOB-10-1447';
        ece_flatfile_pvt.find_pos (l_shipment_tbl, 'DELIVER_TO_CONTACT_EMAIL', ndeliv_cont_email_pos);

        -- 2823215
        xprogress := 'POOB-10-1448';
        ece_flatfile_pvt.find_pos (l_shipment_tbl, 'CODE_UOM', nshp_uom_pos);

        xprogress := 'POOB-10-1449';
        ece_flatfile_pvt.find_pos (l_line_tbl, 'UOM_CODE', nline_uom_pos);

        IF ec_debug.g_debug_level = 3
        THEN
            ec_debug.pl (3, 'n_trx_date_pos: ', n_trx_date_pos);
            ec_debug.pl (3, 'nHeader_key_pos: ', nheader_key_pos);
            ec_debug.pl (3, 'nDocument_type_pos: ', ndocument_type_pos);
            ec_debug.pl (3, 'nPO_Number_pos: ', npo_number_pos);
            ec_debug.pl (3, 'nPO_Type_pos: ', npo_type_pos);
            ec_debug.pl (3, 'nRelease_num_pos: ', nrelease_num_pos);
            ec_debug.pl (3, 'nRelease_id_pos: ', nrelease_id_pos);
            ec_debug.pl (3, 'nLine_Location_ID_pos: ', nline_location_id_pos);
            ec_debug.pl (3, 'nLine_num_pos: ', nline_num_pos);
            ec_debug.pl (3, 'nLine_key_pos: ', nline_key_pos);
            ec_debug.pl (3, 'nItem_id_pos: ', nitem_id_pos);
            ec_debug.pl (3, 'nShip_Line_Location_ID_pos: ', nship_line_location_id_pos);
        END IF;

        -- Timezone enhancement
        xprogress := 'POOB-TZ-1000';
        ece_flatfile_pvt.find_pos (l_header_tbl, 'RELEASE_DATE', nrel_date_pos);

        xprogress := 'POOB-TZ-1001';
        ece_flatfile_pvt.find_pos (l_header_tbl, 'RELEASE_DT_TZ_CODE', nrel_dt_tz_pos);

        xprogress := 'POOB-TZ-1002';
        ece_flatfile_pvt.find_pos (l_header_tbl, 'RELEASE_DT_OFF', nrel_dt_off_pos);

        xprogress := 'POOB-TZ-1003';
        ece_flatfile_pvt.find_pos (l_header_tbl, 'CREATION_DATE', ncrtn_date_pos);

        xprogress := 'POOB-TZ-1004';
        ece_flatfile_pvt.find_pos (l_header_tbl, 'CREATION_DT_TZ_CODE', ncrtn_dt_tz_pos);

        xprogress := 'POOB-TZ-1005';
        ece_flatfile_pvt.find_pos (l_header_tbl, 'CREATION_DT_OFF', ncrtn_dt_off_pos);

        xprogress := 'POOB-TZ-1006';
        ece_flatfile_pvt.find_pos (l_header_tbl, 'PO_REVISION_DATE', nrev_date_pos);

        xprogress := 'POOB-TZ-1007';
        ece_flatfile_pvt.find_pos (l_header_tbl, 'REVISION_DT_TZ_CODE', nrev_dt_tz_pos);

        xprogress := 'POOB-TZ-1008';
        ece_flatfile_pvt.find_pos (l_header_tbl, 'REVISION_DT_OFF', nrev_dt_off_pos);

        xprogress := 'POOB-TZ-1009';
        ece_flatfile_pvt.find_pos (l_header_tbl, 'PO_ACCEPTANCE_DUE_BY_DATE', nacc_due_dt_pos);

        xprogress := 'POOB-TZ-1010';
        ece_flatfile_pvt.find_pos (l_header_tbl, 'PO_ACCEPT_DUE_TZ_CODE', nacc_due_tz_pos);

        xprogress := 'POOB-TZ-1011';
        ece_flatfile_pvt.find_pos (l_header_tbl, 'PO_ACCEPT_DUE_OFF', nacc_due_off_pos);

        xprogress := 'POOB-TZ-1012';
        ece_flatfile_pvt.find_pos (l_header_tbl, 'BLANKET_START_DATE', nblkt_srt_dt_pos);

        xprogress := 'POOB-TZ-1013';
        ece_flatfile_pvt.find_pos (l_header_tbl, 'BLANKET_SRT_DT_TZ_CODE', nblkt_srt_tz_pos);

        xprogress := 'POOB-TZ-1014';
        ece_flatfile_pvt.find_pos (l_header_tbl, 'BLANKET_SRT_DT_OFF', nblkt_srt_off_pos);

        xprogress := 'POOB-TZ-1015';
        ece_flatfile_pvt.find_pos (l_header_tbl, 'BLANKET_END_DATE', nblkt_end_dt_pos);

        xprogress := 'POOB-TZ-1016';
        ece_flatfile_pvt.find_pos (l_header_tbl, 'BLANKET_END_DT_TZ_CODE', nblkt_end_tz_pos);

        xprogress := 'POOB-TZ-1017';
        ece_flatfile_pvt.find_pos (l_header_tbl, 'BLANKET_END_DT_OFF', nblkt_end_off_pos);

        xprogress := 'POOB-TZ-1018';
        ece_flatfile_pvt.find_pos (l_header_tbl, 'PCARD_EXPIRATION_DATE', npcard_exp_dt_pos);

        xprogress := 'POOB-TZ-1019';
        ece_flatfile_pvt.find_pos (l_header_tbl, 'PCARD_EXPRN_DT_TZ_CODE', npcard_exp_tz_pos);

        xprogress := 'POOB-TZ-1020';
        ece_flatfile_pvt.find_pos (l_header_tbl, 'PCARD_EXPRN_DT_OFF', npcard_exp_off_pos);

        xprogress := 'POOB-TZ-1021';
        ece_flatfile_pvt.find_pos (l_line_tbl, 'LINE_CANCELLED_DATE', nline_can_dt_pos);

        xprogress := 'POOB-TZ-1022';
        ece_flatfile_pvt.find_pos (l_line_tbl, 'LINE_CANCEL_DT_TZ_CODE', nline_can_tz_pos);

        xprogress := 'POOB-TZ-1023';
        ece_flatfile_pvt.find_pos (l_line_tbl, 'LINE_CANCEL_DT_OFF', nline_can_off_pos);

        xprogress := 'POOB-TZ-1024';
        ece_flatfile_pvt.find_pos (l_line_tbl, 'EXPIRATION_DATE', nexprn_dt_pos);

        xprogress := 'POOB-TZ-1025';
        ece_flatfile_pvt.find_pos (l_line_tbl, 'EXPIRATION_DT_TZ_CODE', nexprn_tz_pos);

        xprogress := 'POOB-TZ-1026';
        ece_flatfile_pvt.find_pos (l_line_tbl, 'EXPIRATION_DT_OFF', nexprn_off_pos);

        xprogress := 'POOB-TZ-1027';
        ece_flatfile_pvt.find_pos (l_shipment_tbl, 'SHIPMENT_NEED_BY_DATE', nship_need_dt_pos);

        xprogress := 'POOB-TZ-1028';
        ece_flatfile_pvt.find_pos (l_shipment_tbl, 'SHIPMENT_NEED_DT_TZ_CODE', nship_need_tz_pos);

        xprogress := 'POOB-TZ-1029';
        ece_flatfile_pvt.find_pos (l_shipment_tbl, 'SHIPMENT_NEED_DT_OFF', nship_need_off_pos);

        xprogress := 'POOB-TZ-1030';
        ece_flatfile_pvt.find_pos (l_shipment_tbl, 'SHIPMENT_PROMISED_DATE', nship_prom_dt_pos);

        xprogress := 'POOB-TZ-1031';
        ece_flatfile_pvt.find_pos (l_shipment_tbl, 'SHIPMENT_PROM_DT_TZ_CODE', nship_prom_tz_pos);

        xprogress := 'POOB-TZ-1032';
        ece_flatfile_pvt.find_pos (l_shipment_tbl, 'SHIPMENT_PROM_DT_OFF', nship_prom_off_pos);

        xprogress := 'POOB-TZ-1033';
        ece_flatfile_pvt.find_pos (l_shipment_tbl, 'SHIPMENT_LAST_ACCEPTABLE_DATE', nship_accept_dt_pos);

        xprogress := 'POOB-TZ-1034';
        ece_flatfile_pvt.find_pos (l_shipment_tbl, 'SHIPMENT_LAST_ACC_DT_TZ_CODE', nship_accept_tz_pos);

        xprogress := 'POOB-TZ-1035';
        ece_flatfile_pvt.find_pos (l_shipment_tbl, 'SHIPMENT_LAST_ACC_DT_OFF', nship_accept_off_pos);

        xprogress := 'POOB-TZ-1036';
        ece_flatfile_pvt.find_pos (l_shipment_tbl, 'CANCELLED_DATE', nshp_can_dt_pos);

        xprogress := 'POOB-TZ-1037';
        ece_flatfile_pvt.find_pos (l_shipment_tbl, 'CANCEL_DT_TZ_CODE', nshp_can_tz_pos);

        xprogress := 'POOB-TZ-1038';
        ece_flatfile_pvt.find_pos (l_shipment_tbl, 'CANCEL_DT_OFF', nshp_can_off_pos);

        xprogress := 'POOB-TZ-1039';
        ece_flatfile_pvt.find_pos (l_shipment_tbl, 'START_DATE', nshp_strt_dt_pos);

        xprogress := 'POOB-TZ-1040';
        ece_flatfile_pvt.find_pos (l_shipment_tbl, 'START_DT_TZ_CODE', nshp_strt_tz_pos);

        xprogress := 'POOB-TZ-1041';
        ece_flatfile_pvt.find_pos (l_shipment_tbl, 'START_DT_OFF', nshp_strt_off_pos);

        xprogress := 'POOB-TZ-1042';
        ece_flatfile_pvt.find_pos (l_shipment_tbl, 'END_DATE', nshp_end_dt_pos);

        xprogress := 'POOB-TZ-1043';
        ece_flatfile_pvt.find_pos (l_shipment_tbl, 'END_DT_TZ_CODE', nshp_end_tz_pos);

        xprogress := 'POOB-TZ-1044';
        ece_flatfile_pvt.find_pos (l_shipment_tbl, 'END_DT_OFF', nshp_end_off_pos);
        -- Timezone enhancement
        xprogress := 'POOB-09-1400';
        ece_flatfile_pvt.find_pos (l_header_tbl, c_header_common_key_name, n_header_common_key_pos);

        xprogress := 'POOB-09-1401';
        ece_flatfile_pvt.find_pos (l_line_tbl, c_line_common_key_name, n_line_common_key_pos);

        xprogress := 'POOB-09-1402';
        ece_flatfile_pvt.find_pos (l_shipment_tbl, c_shipment_key_name, n_ship_common_key_pos);

        xprogress := 'POOB-10-1407';
        -- bind variables
        DBMS_SQL.bind_variable (header_sel_c, 'cComm_Method', ccommunication_method);

        IF ccreate_date_from IS NOT NULL
        THEN
            DBMS_SQL.bind_variable (header_sel_c, 'cCreate_Dt_From', ccreate_date_from);
        END IF;

        IF ccreate_date_to IS NOT NULL
        THEN
            DBMS_SQL.bind_variable (header_sel_c, 'cCreate_Dt_To', ccreate_date_to);
        END IF;

        IF csupplier_name IS NOT NULL
        THEN
            DBMS_SQL.bind_variable (header_sel_c, 'cSuppl_Name', csupplier_name);
        END IF;

        IF csupplier_site IS NOT NULL
        THEN
            DBMS_SQL.bind_variable (header_sel_c, 'cSuppl_Site', csupplier_site);
        END IF;

        IF cdocument_type IS NOT NULL
        THEN
            DBMS_SQL.bind_variable (header_sel_c, 'cDoc_Type', cdocument_type);
        END IF;

        IF cpo_number_from IS NOT NULL
        THEN
            DBMS_SQL.bind_variable (header_sel_c, 'cPO_Num_From', cpo_number_from);
        END IF;

        IF cpo_number_to IS NOT NULL
        THEN
            DBMS_SQL.bind_variable (header_sel_c, 'cPO_Num_To', cpo_number_to);
        END IF;

        --  dbms_sql.bind_variable(line_sel_c,'l_nOrganization_ID',nOrganization_ID);

        ikey_count := 0;

        IF ec_debug.g_debug_level = 3
        THEN
            ec_debug.pl (3, 'iKey_count: ', ikey_count);
        END IF;

        -- EXECUTE the SELECT statement
        xprogress := 'POOB-10-1408';
        dummy := DBMS_SQL.execute (header_sel_c);

        -- ***************************************************
        -- The model is:
        -- HEADER - LINE - SHIPMENT ...
        -- With data for each HEADER line, populate the header
        -- interfacetable then get all LINES that belongs
        -- to the HEADER. Then get all
        -- SHIPMENTS that belongs to the LINE.
        -- ***************************************************
        xprogress := 'POOB-10-1430';

        WHILE DBMS_SQL.fetch_rows (header_sel_c) > 0
        LOOP                                                                                                   -- Header
            -- **************************************
            --  store internal values in pl/sql table
            -- **************************************
            IF (NOT UTL_FILE.is_open (ece_poo_transaction.ufile_type))
            THEN
                ece_poo_transaction.ufile_type :=
                    UTL_FILE.fopen (i_path
                                   ,i_filename
                                   ,'W'
                                   ,32767);
            END IF;

            xprogress := 'POOB-10-1431';
            ece_flatfile_pvt.assign_column_value_to_tbl (header_sel_c
                                                        ,0
                                                        ,l_header_tbl
                                                        ,l_key_tbl);

            -- ***************************************************
            --  also need to populate transaction_date and run_id
            -- ***************************************************
            xprogress := 'POOB-10-1432';
            l_header_tbl (n_trx_date_pos).VALUE := TO_CHAR (dtransaction_date, 'YYYYMMDD HH24MISS');

            --  The application specific feedback logic begins here.
            xprogress := 'POOB-10-1440';

            BEGIN
                /* Bug 2396394 Added the document type CONTRACT in SQL below */
                SELECT DECODE (l_header_tbl (ndocument_type_pos).VALUE
                              ,'BLANKET', 'NB'
                              ,'STANDARD', 'NS'
                              ,'PLANNED', 'NP'
                              ,'RELEASE', 'NR'
                              ,'BLANKET RELEASE', 'NR'
                              ,'CONTRACT', 'NC'
                              ,'NR')
                  INTO l_document_type
                  FROM DUAL;
            EXCEPTION
                WHEN NO_DATA_FOUND
                THEN
                    ec_debug.pl (0
                                ,'EC'
                                ,'ECE_DECODE_FAILED'
                                ,'PROGRESS_LEVEL'
                                ,xprogress
                                ,'CODE'
                                ,l_header_tbl (ndocument_type_pos).VALUE);
            END;

            IF ec_debug.g_debug_level = 3
            THEN
                ec_debug.pl (3, 'l_document_type: ', l_document_type);
            END IF;

            xprogress := 'POOB-10-1450';

            ece_poo_transaction.update_po (l_document_type
                                          ,l_header_tbl (npo_number_pos).VALUE
                                          ,l_header_tbl (npo_type_pos).VALUE
                                          ,l_header_tbl (nrelease_num_pos).VALUE);

            xprogress := 'POOB-TZ-1500';
            ece_timezone_api.get_server_timezone_details (
                TO_DATE (l_header_tbl (nrel_date_pos).VALUE, 'YYYYMMDD HH24MISS')
               ,l_header_tbl (nrel_dt_off_pos).VALUE
               ,l_header_tbl (nrel_dt_tz_pos).VALUE);

            xprogress := 'POOB-TZ-1510';

            ece_timezone_api.get_server_timezone_details (
                TO_DATE (l_header_tbl (ncrtn_date_pos).VALUE, 'YYYYMMDD HH24MISS')
               ,l_header_tbl (ncrtn_dt_off_pos).VALUE
               ,l_header_tbl (ncrtn_dt_tz_pos).VALUE);

            xprogress := 'POOB-TZ-1520';

            ece_timezone_api.get_server_timezone_details (
                TO_DATE (l_header_tbl (nrev_date_pos).VALUE, 'YYYYMMDD HH24MISS')
               ,l_header_tbl (nrev_dt_off_pos).VALUE
               ,l_header_tbl (nrev_dt_tz_pos).VALUE);

            xprogress := 'POOB-TZ-1530';
            ece_timezone_api.get_server_timezone_details (
                TO_DATE (l_header_tbl (nacc_due_dt_pos).VALUE, 'YYYYMMDD HH24MISS')
               ,l_header_tbl (nacc_due_off_pos).VALUE
               ,l_header_tbl (nacc_due_tz_pos).VALUE);

            xprogress := 'POOB-TZ-1540';
            ece_timezone_api.get_server_timezone_details (
                TO_DATE (l_header_tbl (nblkt_srt_dt_pos).VALUE, 'YYYYMMDD HH24MISS')
               ,l_header_tbl (nblkt_srt_off_pos).VALUE
               ,l_header_tbl (nblkt_srt_tz_pos).VALUE);

            xprogress := 'POOB-TZ-1550';
            ece_timezone_api.get_server_timezone_details (
                TO_DATE (l_header_tbl (nblkt_end_dt_pos).VALUE, 'YYYYMMDD HH24MISS')
               ,l_header_tbl (nblkt_end_off_pos).VALUE
               ,l_header_tbl (nblkt_end_tz_pos).VALUE);

            xprogress := 'POOB-TZ-1560';
            ece_timezone_api.get_server_timezone_details (
                TO_DATE (l_header_tbl (npcard_exp_dt_pos).VALUE, 'YYYYMMDD HH24MISS')
               ,l_header_tbl (npcard_exp_off_pos).VALUE
               ,l_header_tbl (npcard_exp_tz_pos).VALUE);

            -- pass the pl/sql table in for xref
            xprogress := 'POOB-10-1460';
            ec_code_conversion_pvt.populate_plsql_tbl_with_extval (p_api_version_number   => 1.0
                                                                  ,p_init_msg_list        => init_msg_list
                                                                  ,p_simulate             => simulate
                                                                  ,p_commit               => commt
                                                                  ,p_validation_level     => validation_level
                                                                  ,p_return_status        => return_status
                                                                  ,p_msg_count            => msg_count
                                                                  ,p_msg_data             => msg_data
                                                                  ,p_key_tbl              => l_key_tbl
                                                                  ,p_tbl                  => l_header_tbl);

            -- ***************************
            -- insert into interface table
            -- ***************************
            xprogress := 'POOB-10-1480';

            BEGIN
                SELECT ece_poo_header_s.NEXTVAL INTO l_header_fkey FROM DUAL;
            EXCEPTION
                WHEN NO_DATA_FOUND
                THEN
                    ec_debug.pl (0
                                ,'EC'
                                ,'ECE_GET_NEXT_SEQ_FAILED'
                                ,'PROGRESS_LEVEL'
                                ,xprogress
                                ,'SEQ'
                                ,'ECE_POO_HEADER_S');
            END;

            ec_debug.pl (3, 'l_header_fkey: ', l_header_fkey);
            --2823215
            xprogress := 'POOB-10-1490';

            c_file_common_key := RPAD (SUBSTRB (NVL (l_header_tbl (ntrans_code_pos).VALUE, ' '), 1, 25), 25);

            xprogress := 'POOB-10-1491';
            c_file_common_key :=
                   c_file_common_key
                || RPAD (SUBSTRB (NVL (l_header_tbl (n_header_common_key_pos).VALUE, ' '), 1, 22), 22)
                || RPAD (' ', 22)
                || RPAD (' ', 22);

            ec_debug.pl (3, 'c_file_common_key: ', c_file_common_key);

            /*  ece_extract_utils_pub.insert_into_interface_tbl(iRun_id,
                                                             cTransaction_Type,
                                                             cCommunication_Method,
                                                             cHeader_Interface,
                                                             l_header_tbl,
                                                             l_header_fkey);  */

            -- Now update the columns values of which have been obtained
            -- thru the procedure calls.

            -- ********************************************************
            -- Call custom program stub to populate the extension table
            -- ********************************************************
            xprogress := 'POOB-10-1492';
            ece_poo_x.populate_ext_header (l_header_fkey, l_header_tbl);
            -- 2823215
            ece_poo_transaction.write_to_file (ctransaction_type
                                              ,ccommunication_method
                                              ,cheader_interface
                                              ,l_header_tbl
                                              ,ioutput_width
                                              ,irun_id
                                              ,c_file_common_key
                                              ,l_header_fkey);

            -- 2823215
            -- Header Level Attachment Handler
            xprogress := 'POOB-10-1501';

            IF v_header_att_enabled = 'Y'
            THEN
                xprogress := 'POOB-10-1502';

                IF l_document_type = 'NR'
                THEN                                                                         -- If this is a Release PO.
                    xprogress := 'POOB-10-1503';
                    v_entity_name := 'PO_RELEASES';
                    v_pk1_value := l_header_tbl (nrelease_id_pos).VALUE;

                    IF ec_debug.g_debug_level = 3
                    THEN
                        ec_debug.pl (3, 'release_id: ', l_header_tbl (nrelease_id_pos).VALUE);
                    END IF;
                ELSE                                                                     -- If this is a non-Release PO.
                    xprogress := 'POOB-10-1504';
                    v_entity_name := 'PO_HEADERS';
                    v_pk1_value := l_header_tbl (nheader_key_pos).VALUE;

                    IF ec_debug.g_debug_level = 3
                    THEN
                        ec_debug.pl (3, 'po_header_id: ', l_header_tbl (nheader_key_pos).VALUE);
                    END IF;
                END IF;

                xprogress := 'POOB-10-1505';
                ece_poo_transaction.populate_text_attachment (ccommunication_method
                                                             ,ctransaction_type
                                                             ,irun_id
                                                             ,2
                                                             ,3
                                                             ,catt_header_interface
                                                             ,catt_detail_interface
                                                             ,v_entity_name
                                                             ,'VENDOR'
                                                             ,v_pk1_value
                                                             ,c_any_value
                                                             ,c_any_value
                                                             ,c_any_value
                                                             ,c_any_value
                                                             ,n_att_seg_size
                                                             ,l_key_tbl
                                                             ,c_file_common_key
                                                             ,l_hdr_att_hdr_tbl
                                                             ,l_hdr_att_dtl_tbl
                                                             ,iatt_hdr_pos);                                  -- 2823215
            END IF;

            -- ***************************************************
            -- From Header data, we can assign values to
            -- place holders (foreign keys) in Line_select and
            -- Line_detail_Select
            -- set values into binding variables
            -- ***************************************************

            -- use the following bind_variable feature as you see fit.
            xprogress := 'POOB-10-1510';
            DBMS_SQL.bind_variable (line_sel_c, 'po_header_id', l_header_tbl (nheader_key_pos).VALUE);

            xprogress := 'POOB-10-1515';
            DBMS_SQL.bind_variable (line_sel_c, 'por_release_num', l_header_tbl (nrelease_num_pos).VALUE);

            xprogress := 'POOB-10-1520';
            DBMS_SQL.bind_variable (shipment_sel_c, 'po_header_id', l_header_tbl (nheader_key_pos).VALUE);

            xprogress := 'POOB-10-1525';
            DBMS_SQL.bind_variable (shipment_sel_c, 'por_release_id', l_header_tbl (nrelease_id_pos).VALUE);   --2823215

            xprogress := 'POOB-10-1530';
            dummy := DBMS_SQL.execute (line_sel_c);

            -- ***************************
            -- Line Level Loop Starts Here
            -- ***************************
            xprogress := 'POOB-10-1540';

            WHILE DBMS_SQL.fetch_rows (line_sel_c) > 0
            LOOP                                                                                                --- Line
                -- ****************************
                -- store values in pl/sql table
                -- ****************************
                xprogress := 'POOB-10-1550';
                ece_flatfile_pvt.assign_column_value_to_tbl (line_sel_c
                                                            ,iheader_count
                                                            ,l_line_tbl
                                                            ,l_key_tbl);

                -- The following procedure gets the part number for the
                -- item ID returned
                xprogress := 'POOB-10-1560';
                ece_inventory.get_item_number (l_line_tbl (nitem_id_pos).VALUE
                                              ,norganization_id
                                              ,line_part_number
                                              ,line_part_attrib_category
                                              ,line_part_attribute1
                                              ,line_part_attribute2
                                              ,line_part_attribute3
                                              ,line_part_attribute4
                                              ,line_part_attribute5
                                              ,line_part_attribute6
                                              ,line_part_attribute7
                                              ,line_part_attribute8
                                              ,line_part_attribute9
                                              ,line_part_attribute10
                                              ,line_part_attribute11
                                              ,line_part_attribute12
                                              ,line_part_attribute13
                                              ,line_part_attribute14
                                              ,line_part_attribute15);

                BEGIN
                    SELECT uom_code
                      INTO l_line_tbl (nline_uom_pos).VALUE
                      FROM mtl_units_of_measure
                     WHERE unit_of_measure = l_line_tbl (nline_uom_code_pos).VALUE;
                EXCEPTION
                    WHEN OTHERS
                    THEN
                        NULL;
                END;

                xprogress := 'POOB-TZ-2500';
                ece_timezone_api.get_server_timezone_details (
                    TO_DATE (l_line_tbl (nline_can_dt_pos).VALUE, 'YYYYMMDD HH24MISS')
                   ,l_line_tbl (nline_can_off_pos).VALUE
                   ,l_line_tbl (nline_can_tz_pos).VALUE);

                xprogress := 'POOB-TZ-2510';

                ece_timezone_api.get_server_timezone_details (
                    TO_DATE (l_line_tbl (nexprn_dt_pos).VALUE, 'YYYYMMDD HH24MISS')
                   ,l_line_tbl (nexprn_off_pos).VALUE
                   ,l_line_tbl (nexprn_tz_pos).VALUE);

                -- pass the pl/sql table in for xref
                xprogress := 'POOB-10-1570';
                ec_code_conversion_pvt.populate_plsql_tbl_with_extval (p_api_version_number   => 1.0
                                                                      ,p_init_msg_list        => init_msg_list
                                                                      ,p_simulate             => simulate
                                                                      ,p_commit               => commt
                                                                      ,p_validation_level     => validation_level
                                                                      ,p_return_status        => return_status
                                                                      ,p_msg_count            => msg_count
                                                                      ,p_msg_data             => msg_data
                                                                      ,p_key_tbl              => l_key_tbl
                                                                      ,p_tbl                  => l_line_tbl);

                xprogress := 'POOB-10-1590';

                BEGIN
                    SELECT ece_poo_line_s.NEXTVAL INTO l_line_fkey FROM DUAL;
                EXCEPTION
                    WHEN NO_DATA_FOUND
                    THEN
                        ec_debug.pl (0
                                    ,'EC'
                                    ,'ECE_GET_NEXT_SEQ_FAILED'
                                    ,'PROGRESS_LEVEL'
                                    ,xprogress
                                    ,'SEQ'
                                    ,'ECE_POO_LINE_S');
                END;

                ec_debug.pl (3, 'l_line_fkey: ', l_line_fkey);

                IF ec_debug.g_debug_level = 3
                THEN
                    ec_debug.pl (3, 'line_part_number: ', line_part_number);
                    ec_debug.pl (3, 'line_part_attrib_category: ', line_part_attrib_category);
                    ec_debug.pl (3, 'line_part_attribute1: ', line_part_attribute1);
                    ec_debug.pl (3, 'line_part_attribute2: ', line_part_attribute2);
                    ec_debug.pl (3, 'line_part_attribute3: ', line_part_attribute3);
                    ec_debug.pl (3, 'line_part_attribute4: ', line_part_attribute4);
                    ec_debug.pl (3, 'line_part_attribute5: ', line_part_attribute5);
                    ec_debug.pl (3, 'line_part_attribute6: ', line_part_attribute6);
                    ec_debug.pl (3, 'line_part_attribute7: ', line_part_attribute7);
                    ec_debug.pl (3, 'line_part_attribute8: ', line_part_attribute8);
                    ec_debug.pl (3, 'line_part_attribute9: ', line_part_attribute9);
                    ec_debug.pl (3, 'line_part_attribute10: ', line_part_attribute10);
                    ec_debug.pl (3, 'line_part_attribute11: ', line_part_attribute11);
                    ec_debug.pl (3, 'line_part_attribute12: ', line_part_attribute12);
                    ec_debug.pl (3, 'line_part_attribute13: ', line_part_attribute13);
                    ec_debug.pl (3, 'line_part_attribute14: ', line_part_attribute14);
                    ec_debug.pl (3, 'line_part_attribute15: ', line_part_attribute15);
                END IF;

                xprogress := 'POOB-10-1591';
                l_line_tbl (nlp_att_cat_pos).VALUE := line_part_attrib_category;
                l_line_tbl (nlp_att1_pos).VALUE := line_part_attribute1;
                l_line_tbl (nlp_att2_pos).VALUE := line_part_attribute2;
                l_line_tbl (nlp_att3_pos).VALUE := line_part_attribute3;
                l_line_tbl (nlp_att4_pos).VALUE := line_part_attribute4;
                l_line_tbl (nlp_att5_pos).VALUE := line_part_attribute5;
                l_line_tbl (nlp_att6_pos).VALUE := line_part_attribute6;
                l_line_tbl (nlp_att7_pos).VALUE := line_part_attribute7;
                l_line_tbl (nlp_att8_pos).VALUE := line_part_attribute8;
                l_line_tbl (nlp_att9_pos).VALUE := line_part_attribute9;
                l_line_tbl (nlp_att10_pos).VALUE := line_part_attribute10;
                l_line_tbl (nlp_att11_pos).VALUE := line_part_attribute11;
                l_line_tbl (nlp_att12_pos).VALUE := line_part_attribute12;
                l_line_tbl (nlp_att13_pos).VALUE := line_part_attribute13;
                l_line_tbl (nlp_att14_pos).VALUE := line_part_attribute14;
                l_line_tbl (nlp_att15_pos).VALUE := line_part_attribute15;

                xprogress := 'POOB-10-1600';
                c_file_common_key :=
                       RPAD (SUBSTRB (NVL (l_header_tbl (ntrans_code_pos).VALUE, ' '), 1, 25), 25)
                    || RPAD (SUBSTRB (NVL (l_header_tbl (n_header_common_key_pos).VALUE, ' '), 1, 22), 22)
                    || RPAD (SUBSTRB (NVL (l_line_tbl (n_line_common_key_pos).VALUE, ' '), 1, 22), 22)
                    || RPAD (' ', 22);

                IF ec_debug.g_debug_level = 3
                THEN
                    ec_debug.pl (3, 'c_file_common_key: ', c_file_common_key);
                END IF;

                xprogress := 'POOB-10-1620';
                ece_poo_x.populate_ext_line (l_line_fkey, l_line_tbl);
                -- 2823215
                xprogress := 'POOB-10-1621';
                ece_poo_transaction.write_to_file (ctransaction_type
                                                  ,ccommunication_method
                                                  ,cline_interface
                                                  ,l_line_tbl
                                                  ,ioutput_width
                                                  ,irun_id
                                                  ,c_file_common_key
                                                  ,l_line_fkey);

                --2823215
                -- Insert into Interface Table
                /*             xProgress := 'POOB-10-1600';
                             ece_extract_utils_pub.insert_into_interface_tbl(iRun_id,cTransaction_Type,cCommunication_Method,cLine_Interface,l_line_tbl,l_line_fkey); */

                /***************************
                *  Line LEVEL Attachments  *
                ***************************/
                IF v_line_att_enabled = 'Y'
                THEN
                    xprogress := 'POOB-10-1621';
                    /*      IF l_document_type = 'NR' THEN -- If this is a Release PO.
                             xProgress := 'POOB-10-1622';
                             v_entity_name := 'PO_SHIPMENTS';
                             v_pk1_value := l_line_tbl(nLine_Location_ID_pos).value; -- LINE_LOCATION_ID
                           if ec_debug.G_debug_level = 3 then
                             ec_debug.pl(3,'PO_LINE_LOCATION_ID: ',l_line_tbl(nLine_Location_ID_pos).value);
                             end if;
                          ELSE -- If this is a non-Release PO. */
                    --Bug 2187958
                    xprogress := 'POOB-10-1623';
                    v_entity_name := 'PO_LINES';
                    v_pk1_value := l_line_tbl (nline_key_pos).VALUE;                                          -- LINE_ID

                    IF ec_debug.g_debug_level = 3
                    THEN
                        ec_debug.pl (3, 'PO_LINE_ID: ', l_line_tbl (nline_key_pos).VALUE);
                    --        end if;
                    END IF;

                    xprogress := 'POOB-10-1624';
                    ece_poo_transaction.populate_text_attachment (ccommunication_method
                                                                 ,ctransaction_type
                                                                 ,irun_id
                                                                 ,5
                                                                 ,6
                                                                 ,catt_header_interface
                                                                 ,catt_detail_interface
                                                                 ,v_entity_name
                                                                 ,'VENDOR'
                                                                 ,v_pk1_value
                                                                 ,c_any_value
                                                                 ,c_any_value
                                                                 ,c_any_value
                                                                 ,c_any_value
                                                                 ,n_att_seg_size
                                                                 ,l_key_tbl
                                                                 ,c_file_common_key
                                                                 ,l_ln_att_hdr_tbl
                                                                 ,l_ln_att_dtl_tbl
                                                                 ,iatt_ln_pos);
                END IF;

                /***************************
                *  Master Org Attachments  *
                ***************************/
                IF v_mitem_att_enabled = 'Y'
                THEN
                    xprogress := 'POOB-10-1625';
                    v_entity_name := 'MTL_SYSTEM_ITEMS';
                    v_pk1_value := norganization_id;                                          -- Master Inventory Org ID

                    v_pk2_value := l_line_tbl (nitem_id_pos).VALUE;                                           -- Item ID

                    IF ec_debug.g_debug_level = 3
                    THEN
                        ec_debug.pl (3, 'Master Org ID: ', v_pk1_value);
                        ec_debug.pl (3, 'Item ID: ', v_pk2_value);
                    END IF;

                    xprogress := 'POOB-10-1626';
                    ece_poo_transaction.populate_text_attachment (ccommunication_method
                                                                 ,ctransaction_type
                                                                 ,irun_id
                                                                 ,7
                                                                 ,8
                                                                 ,catt_header_interface
                                                                 ,catt_detail_interface
                                                                 ,v_entity_name
                                                                 ,'VENDOR'
                                                                 ,v_pk1_value
                                                                 ,v_pk2_value
                                                                 ,NULL
                                                                 ,NULL
                                                                 ,NULL
                                                                 ,n_att_seg_size
                                                                 ,l_key_tbl
                                                                 ,c_file_common_key
                                                                 ,l_mi_att_hdr_tbl
                                                                 ,l_mi_att_dtl_tbl
                                                                 ,iatt_mi_pos);
                END IF;

                /******************************
                *  Inventory Org Attachments  *
                ******************************/
                /* Bug 3550723
                    IF v_iitem_att_enabled = 'Y' THEN
                       xProgress := 'POOB-10-1627';
                       v_entity_name := 'MTL_SYSTEM_ITEMS';
                       v_pk2_value := l_line_tbl(nitem_id_pos).value; -- Item ID
                     if ec_debug.G_debug_level = 3 then
                        ec_debug.pl(3,'Item ID: ',v_pk2_value);
                       end if;

                       xProgress := 'POOB-10-1628';
                       FOR v_org_id IN c_org_id(l_line_tbl(nLine_key_pos).value) LOOP -- Value passed is the Line ID
                          IF v_org_id.ship_to_organization_id <> nOrganization_ID THEN -- Only do this if it is not the same as the Master Org ID
                             v_pk1_value := v_org_id.ship_to_organization_id;
                           if ec_debug.G_debug_level = 3 then
                             ec_debug.pl(3,'Inventory Org ID: ',v_pk1_value);
                             end if;

                             xProgress := 'POOB-10-1626';
                             ece_poo_transaction.populate_text_attachment(cCommunication_Method,
                                                                          cTransaction_Type,
                                                                          iRun_id,
                                                                          9,
                                                                          10,
                                                                          cAtt_Header_Interface,
                                                                          cAtt_Detail_Interface,
                                                                          v_entity_name,
                                                                          'VENDOR',
                                                                          v_pk1_value,
                                                                          v_pk2_value,
                                                                          NULL,
                                                                          NULL,
                                                                          NULL,
                                                                          n_att_seg_size,
                                                                          l_key_tbl,
                                          c_file_common_key,
                                                                          l_msi_att_hdr_tbl,
                                                                          l_msi_att_dtl_tbl,
                                                                          iAtt_msi_pos);
                          END IF;
                       END LOOP;
                    END IF;
                */

                -- **********************
                -- set LINE_NUMBER values
                -- **********************
                --  Removed based on bug:3957851
                --               xProgress := 'POOB-10-1627';
                --              dbms_sql.bind_variable(shipment_sel_c,'shipment_number',l_line_tbl(nLine_num_pos).value);

                xprogress := 'POOB-10-1630';
                DBMS_SQL.bind_variable (shipment_sel_c, 'po_line_id', l_line_tbl (nline_key_pos).VALUE);

                xprogress := 'POOB-10-1640';
                dummy := DBMS_SQL.execute (shipment_sel_c);

                -- *************************
                -- Shipment loop starts here
                -- *************************
                xprogress := 'POOB-10-1650';

                WHILE DBMS_SQL.fetch_rows (shipment_sel_c) > 0
                LOOP                                                                                        --- Shipment
                    -- ****************************
                    -- store values in pl/sql table
                    -- ****************************
                    xprogress := 'POOB-10-1660';
                    ece_flatfile_pvt.assign_column_value_to_tbl (shipment_sel_c
                                                                ,iheader_count + iline_count
                                                                ,l_shipment_tbl
                                                                ,l_key_tbl);

                    xprogress := 'POOB-10-1670';

                    BEGIN
                        SELECT ece_poo_shipment_s.NEXTVAL INTO l_shipment_fkey FROM DUAL;
                    EXCEPTION
                        WHEN NO_DATA_FOUND
                        THEN
                            ec_debug.pl (0
                                        ,'EC'
                                        ,'ECE_GET_NEXT_SEQ_FAILED'
                                        ,'PROGRESS_LEVEL'
                                        ,xprogress
                                        ,'SEQ'
                                        ,'ECE_POO_SHIPMENT_S');
                    END;

                    IF ec_debug.g_debug_level = 3
                    THEN
                        ec_debug.pl (3, 'l_shipment_fkey: ', l_shipment_fkey);
                    END IF;

                    l_shipment_tbl (nline_location_uom_pos).VALUE := l_line_tbl (nline_uom_code_pos).VALUE; -- bug 2823215

                    l_shipment_tbl (nship_release_num_pos).VALUE := l_header_tbl (nrelease_num_pos).VALUE; -- bug 2823215

                    l_shipment_tbl (nshp_uom_pos).VALUE := l_line_tbl (nline_uom_pos).VALUE;

                    xprogress := 'POOB-TZ-3500';
                    ece_timezone_api.get_server_timezone_details (
                        TO_DATE (l_shipment_tbl (nship_need_dt_pos).VALUE, 'YYYYMMDD HH24MISS')
                       ,l_shipment_tbl (nship_need_off_pos).VALUE
                       ,l_shipment_tbl (nship_need_tz_pos).VALUE);

                    xprogress := 'POOB-TZ-3510';
                    ece_timezone_api.get_server_timezone_details (
                        TO_DATE (l_shipment_tbl (nship_prom_dt_pos).VALUE, 'YYYYMMDD HH24MISS')
                       ,l_shipment_tbl (nship_prom_off_pos).VALUE
                       ,l_shipment_tbl (nship_prom_tz_pos).VALUE);

                    xprogress := 'POOB-TZ-3520';
                    ece_timezone_api.get_server_timezone_details (
                        TO_DATE (l_shipment_tbl (nship_accept_dt_pos).VALUE, 'YYYYMMDD HH24MISS')
                       ,l_shipment_tbl (nship_accept_off_pos).VALUE
                       ,l_shipment_tbl (nship_accept_tz_pos).VALUE);

                    xprogress := 'POOB-TZ-3530';
                    ece_timezone_api.get_server_timezone_details (
                        TO_DATE (l_shipment_tbl (nshp_can_dt_pos).VALUE, 'YYYYMMDD HH24MISS')
                       ,l_shipment_tbl (nshp_can_off_pos).VALUE
                       ,l_shipment_tbl (nshp_can_tz_pos).VALUE);

                    xprogress := 'POOB-TZ-3540';
                    ece_timezone_api.get_server_timezone_details (
                        TO_DATE (l_shipment_tbl (nshp_strt_dt_pos).VALUE, 'YYYYMMDD HH24MISS')
                       ,l_shipment_tbl (nshp_strt_off_pos).VALUE
                       ,l_shipment_tbl (nshp_strt_tz_pos).VALUE);

                    xprogress := 'POOB-TZ-3550';
                    ece_timezone_api.get_server_timezone_details (
                        TO_DATE (l_shipment_tbl (nshp_end_dt_pos).VALUE, 'YYYYMMDD HH24MISS')
                       ,l_shipment_tbl (nshp_end_off_pos).VALUE
                       ,l_shipment_tbl (nshp_end_tz_pos).VALUE);
                    -- pass the pl/sql table in for xref
                    xprogress := 'POOB-10-1680';
                    ec_code_conversion_pvt.populate_plsql_tbl_with_extval (p_api_version_number   => 1.0
                                                                          ,p_init_msg_list        => init_msg_list
                                                                          ,p_simulate             => simulate
                                                                          ,p_commit               => commt
                                                                          ,p_validation_level     => validation_level
                                                                          ,p_return_status        => return_status
                                                                          ,p_msg_count            => msg_count
                                                                          ,p_msg_data             => msg_data
                                                                          ,p_key_tbl              => l_key_tbl
                                                                          ,p_tbl                  => l_shipment_tbl);

                    /*        xProgress := 'POOB-10-1690';

                            ece_extract_utils_pub.insert_into_interface_tbl(iRun_id,cTransaction_Type,cCommunication_Method,cShipment_Interface,l_shipment_tbl,l_shipment_fkey); */

                    xprogress := 'POOB-10-1690';
                    c_file_common_key :=
                           RPAD (SUBSTRB (NVL (l_header_tbl (ntrans_code_pos).VALUE, ' '), 1, 25), 25)
                        || RPAD (SUBSTRB (NVL (l_header_tbl (n_header_common_key_pos).VALUE, ' '), 1, 22), 22)
                        || RPAD (SUBSTRB (NVL (l_line_tbl (n_line_common_key_pos).VALUE, ' '), 1, 22), 22)
                        || RPAD (SUBSTRB (NVL (l_shipment_tbl (n_ship_common_key_pos).VALUE, ' '), 1, 22), 22);

                    IF ec_debug.g_debug_level = 3
                    THEN
                        ec_debug.pl (3, 'c_file_common_key: ', c_file_common_key);
                    END IF;

                    xprogress := 'POOB-10-1700';
                    ece_poo_x.populate_ext_shipment (l_shipment_fkey, l_shipment_tbl);

                    -- Drop shipment
                    xprogress := 'POOB-10-1691';
                    v_drop_ship_flag :=
                        oe_drop_ship_grp.po_line_location_is_drop_ship (
                            l_shipment_tbl (nship_line_location_id_pos).VALUE);
                    xprogress := 'POOB-10-1692';

                    IF ec_debug.g_debug_level = 3
                    THEN
                        ec_debug.pl (3, 'Drop Ship Flag:', v_drop_ship_flag);
                    END IF;

                    IF (v_drop_ship_flag IS NOT NULL)
                    THEN
                        v_header_cancel_flag := l_header_tbl (nheader_cancel_flag_pos).VALUE;
                        v_line_cancel_flag := l_line_tbl (nline_cancel_flag_pos).VALUE;
                        v_shipment_cancel_flag := l_shipment_tbl (nshipment_cancel_flag_pos).VALUE;

                        IF ec_debug.g_debug_level = 3
                        THEN
                            ec_debug.pl (3, 'v_header_cancel_flag:', v_header_cancel_flag);
                            ec_debug.pl (3, 'v_line_cancel_flag:', v_line_cancel_flag);
                            ec_debug.pl (3, 'v_shipment_cancel_flag:', v_shipment_cancel_flag);
                        END IF;

                        IF (    (NVL (v_header_cancel_flag, 'N') <> 'Y')
                            AND (NVL (v_line_cancel_flag, 'N') <> 'Y')
                            AND (NVL (v_shipment_cancel_flag, 'N') <> 'Y'))
                        THEN
                            xprogress := 'POOB-10-1696';
                            oe_drop_ship_grp.get_order_line_info (1.0
                                                                 ,l_header_tbl (nheader_key_pos).VALUE
                                                                 ,l_line_tbl (nline_key_pos).VALUE
                                                                 ,l_shipment_tbl (nship_line_location_id_pos).VALUE
                                                                 ,l_header_tbl (nrelease_id_pos).VALUE
                                                                 ,2
                                                                 ,rec_order_line_info
                                                                 ,msg_data
                                                                 ,msg_count
                                                                 ,return_status);
                            xprogress := 'POOB-10-1670';

                            IF ec_debug.g_debug_level = 3
                            THEN
                                ec_debug.pl (3, 'Ship to Customer Name:', rec_order_line_info.ship_to_customer_name);
                                ec_debug.pl (3, 'Ship to Contact Name:', rec_order_line_info.ship_to_contact_name);
                                ec_debug.pl (3, 'Ship to Contact Phone:', rec_order_line_info.ship_to_contact_phone);
                                ec_debug.pl (3, 'Ship to Contact Fax:', rec_order_line_info.ship_to_contact_fax);
                                ec_debug.pl (3, 'Ship to Contact Email:', rec_order_line_info.ship_to_contact_email);
                                ec_debug.pl (3, 'Shipping Instructions:', rec_order_line_info.shipping_instructions);
                                ec_debug.pl (3, 'Packing Instructions:', rec_order_line_info.packing_instructions);
                                ec_debug.pl (3, 'Shipping Method:', rec_order_line_info.shipping_method);
                                ec_debug.pl (3, 'Customer PO Number:', rec_order_line_info.customer_po_number);
                                ec_debug.pl (3
                                            ,'Customer PO Line Number:'
                                            ,rec_order_line_info.customer_po_line_number);
                                ec_debug.pl (3
                                            ,'Customer PO Shipment Num:'
                                            ,rec_order_line_info.customer_po_shipment_number);
                                ec_debug.pl (3
                                            ,'Customer Item Description:'
                                            ,rec_order_line_info.customer_product_description);
                                ec_debug.pl (3
                                            ,'Deliver to Location:'
                                            ,rec_order_line_info.deliver_to_customer_location);
                                ec_debug.pl (3
                                            ,'Deliver to Customer Name:'
                                            ,rec_order_line_info.deliver_to_customer_name);
                                ec_debug.pl (3
                                            ,'Deliver to Contact Name:'
                                            ,rec_order_line_info.deliver_to_customer_name);
                                ec_debug.pl (3
                                            ,'Deliver to Contact Phone:'
                                            ,rec_order_line_info.deliver_to_contact_phone);
                                ec_debug.pl (3, 'Deliver to Contact Fax:', rec_order_line_info.deliver_to_contact_fax);
                                ec_debug.pl (3
                                            ,'Deliver to Customer Address:'
                                            ,rec_order_line_info.deliver_to_customer_address);
                                ec_debug.pl (3
                                            ,'Deliver to Contact Email:'
                                            ,rec_order_line_info.deliver_to_contact_email);
                            END IF;

                            -- 2823215
                            l_shipment_tbl (nst_cust_name_pos).VALUE := rec_order_line_info.ship_to_customer_name;
                            l_shipment_tbl (nst_cont_name_pos).VALUE := rec_order_line_info.ship_to_contact_name;
                            l_shipment_tbl (nst_cont_phone_pos).VALUE := rec_order_line_info.ship_to_contact_phone;
                            l_shipment_tbl (nst_cont_fax_pos).VALUE := rec_order_line_info.ship_to_contact_fax;
                            l_shipment_tbl (nst_cont_email_pos).VALUE := rec_order_line_info.ship_to_contact_email;
                            l_shipment_tbl (nshipping_instruct_pos).VALUE := rec_order_line_info.shipping_instructions;
                            l_shipment_tbl (npacking_instruct_pos).VALUE := rec_order_line_info.packing_instructions;
                            l_shipment_tbl (nshipping_method_pos).VALUE := rec_order_line_info.shipping_method;
                            l_shipment_tbl (ncust_po_num_pos).VALUE := rec_order_line_info.customer_po_number;
                            l_shipment_tbl (ncust_po_line_num_pos).VALUE := rec_order_line_info.customer_po_line_number;
                            l_shipment_tbl (ncust_po_ship_num_pos).VALUE :=
                                rec_order_line_info.customer_po_shipment_number;
                            l_shipment_tbl (ncust_prod_desc_pos).VALUE :=
                                rec_order_line_info.customer_product_description;
                            l_shipment_tbl (ndeliv_cust_loc_pos).VALUE :=
                                rec_order_line_info.deliver_to_customer_location;
                            l_shipment_tbl (ndeliv_cust_name_pos).VALUE := rec_order_line_info.deliver_to_customer_name;
                            l_shipment_tbl (ndeliv_cont_name_pos).VALUE := rec_order_line_info.deliver_to_contact_name;
                            l_shipment_tbl (ndeliv_cont_phone_pos).VALUE := rec_order_line_info.deliver_to_contact_phone;
                            l_shipment_tbl (ndeliv_cont_fax_pos).VALUE := rec_order_line_info.deliver_to_contact_fax;
                            l_shipment_tbl (ndeliv_cust_addr_pos).VALUE :=
                                rec_order_line_info.deliver_to_customer_address;
                            l_shipment_tbl (ndeliv_cont_email_pos).VALUE := rec_order_line_info.deliver_to_contact_email;
                        -- 2823215
                        END IF;
                    END IF;

                    -- 2823215
                    ece_poo_transaction.write_to_file (ctransaction_type
                                                      ,ccommunication_method
                                                      ,cshipment_interface
                                                      ,l_shipment_tbl
                                                      ,ioutput_width
                                                      ,irun_id
                                                      ,c_file_common_key
                                                      ,l_shipment_fkey);
                    -- 2823215
                    -- ********************************************************
                    -- Call custom program stub to populate the extension table
                    -- ********************************************************
                    /*     xProgress := 'POOB-10-1700';
                         ece_poo_x.populate_ext_shipment(l_shipment_fkey,l_shipment_tbl); */

                    -- Shipment Level Attachment Handler
                    xprogress := 'POOB-10-1710';

                    IF v_ship_att_enabled = 'Y'
                    THEN
                        v_entity_name := 'PO_SHIPMENTS';
                        v_pk1_value := l_shipment_tbl (nship_line_location_id_pos).VALUE;

                        IF ec_debug.g_debug_level = 3
                        THEN
                            ec_debug.pl (3
                                        ,'Ship Level Line Location ID: '
                                        ,l_shipment_tbl (nship_line_location_id_pos).VALUE);
                        END IF;

                        xprogress := 'POOB-10-1720';
                        ece_poo_transaction.populate_text_attachment (ccommunication_method
                                                                     ,ctransaction_type
                                                                     ,irun_id
                                                                     ,12
                                                                     ,13
                                                                     ,catt_header_interface
                                                                     ,catt_detail_interface
                                                                     ,v_entity_name
                                                                     ,'VENDOR'
                                                                     ,v_pk1_value
                                                                     ,c_any_value
                                                                     ,c_any_value
                                                                     ,c_any_value
                                                                     ,c_any_value
                                                                     ,n_att_seg_size
                                                                     ,l_key_tbl
                                                                     ,c_file_common_key
                                                                     ,l_shp_att_hdr_tbl
                                                                     ,l_shp_att_dtl_tbl
                                                                     ,iatt_shp_pos);                          -- 2823215
                    END IF;

                    -- Project Level Handler
                    xprogress := 'POOB-10-1730';
                    --     IF v_project_acct_status = 'I' THEN -- Project Accounting is Installed --Bug 1891291
                    ece_poo_transaction.populate_distribution_info (ccommunication_method
                                                                   ,ctransaction_type
                                                                   ,irun_id
                                                                   ,cdistribution_interface
                                                                   ,l_key_tbl
                                                                   ,l_header_tbl (nheader_key_pos).VALUE
                                                                   ,                                     -- PO_HEADER_ID
                                                                    l_header_tbl (nrelease_id_pos).VALUE
                                                                   ,                                    -- PO_RELEASE_ID
                                                                    l_line_tbl (nline_key_pos).VALUE
                                                                   ,                                       -- PO_LINE_ID
                                                                    l_shipment_tbl (nship_line_location_id_pos).VALUE
                                                                   ,                                 -- LINE_LOCATION_ID
                                                                    c_file_common_key);                        --2823215
                --    END IF;  --Bug 1891291

                END LOOP;                                                                         -- SHIPMENT Level Loop

                xprogress := 'POOB-10-1740';

                IF DBMS_SQL.last_row_count = 0
                THEN
                    v_levelprocessed := 'SHIPMENT';
                    ec_debug.pl (0
                                ,'EC'
                                ,'ECE_NO_DB_ROW_PROCESSED'
                                ,'PROGRESS_LEVEL'
                                ,xprogress
                                ,'LEVEL_PROCESSED'
                                ,v_levelprocessed
                                ,'TRANSACTION_TYPE'
                                ,ctransaction_type);
                END IF;
            END LOOP;                                                                                 -- LINE Level Loop

            xprogress := 'POOB-10-1750';

            IF (DBMS_SQL.last_row_count = 0)
            THEN
                v_levelprocessed := 'LINE';
                ec_debug.pl (0
                            ,'EC'
                            ,'ECE_NO_DB_ROW_PROCESSED'
                            ,'PROGRESS_LEVEL'
                            ,xprogress
                            ,'LEVEL_PROCESSED'
                            ,v_levelprocessed
                            ,'TRANSACTION_TYPE'
                            ,ctransaction_type);
            END IF;

            xheadercount := xheadercount + 1;
        END LOOP;                                                                                   -- HEADER Level Loop

        xprogress := 'POOB-10-1760';

        IF (DBMS_SQL.last_row_count = 0)
        THEN
            v_levelprocessed := 'HEADER';
            ec_debug.pl (0
                        ,'EC'
                        ,'ECE_NO_DB_ROW_PROCESSED'
                        ,'LEVEL_PROCESSED'
                        ,v_levelprocessed
                        ,'PROGRESS_LEVEL'
                        ,xprogress
                        ,'TRANSACTION_TYPE'
                        ,ctransaction_type);
        END IF;

        xprogress := 'POOB-10-1770';

        IF (ece_poo_transaction.project_sel_c > 0)
        THEN                                                                                               --Bug 2819176
            DBMS_SQL.close_cursor (ece_poo_transaction.project_sel_c);                                     --Bug 2490109
        END IF;

        xprogress := 'POOB-10-1780';
        DBMS_SQL.close_cursor (shipment_sel_c);

        xprogress := 'POOB-10-1790';
        DBMS_SQL.close_cursor (line_sel_c);

        xprogress := 'POOB-10-1800';
        DBMS_SQL.close_cursor (header_sel_c);
        ec_debug.pop ('ECE_POO_TRANSACTION.POPULATE_POO_TRX');
    EXCEPTION
        WHEN OTHERS
        THEN
            ec_debug.pl (0
                        ,'EC'
                        ,'ECE_PROGRAM_ERROR'
                        ,'PROGRESS_LEVEL'
                        ,xprogress);
            ec_debug.pl (0
                        ,'EC'
                        ,'ECE_ERROR_MESSAGE'
                        ,'ERROR_MESSAGE'
                        ,SQLERRM);

            app_exception.raise_exception;
    END populate_poo_trx;

    PROCEDURE put_data_to_output_table (ccommunication_method     IN VARCHAR2
                                       ,ctransaction_type         IN VARCHAR2
                                       ,ioutput_width             IN INTEGER
                                       ,irun_id                   IN INTEGER
                                       ,cheader_interface         IN VARCHAR2
                                       ,cline_interface           IN VARCHAR2
                                       ,cshipment_interface       IN VARCHAR2
                                       ,cdistribution_interface   IN VARCHAR2)
    IS
        xprogress                   VARCHAR2 (80);
        v_levelprocessed            VARCHAR2 (40);

        catt_header_interface       VARCHAR2 (120) := 'ECE_ATTACHMENT_HEADERS';
        catt_detail_interface       VARCHAR2 (120) := 'ECE_ATTACHMENT_DETAILS';

        l_header_tbl                ece_flatfile_pvt.interface_tbl_type;
        l_line_tbl                  ece_flatfile_pvt.interface_tbl_type;
        l_shipment_tbl              ece_flatfile_pvt.interface_tbl_type;

        l_document_type             VARCHAR2 (30);

        c_header_common_key_name    VARCHAR2 (40);
        c_line_common_key_name      VARCHAR2 (40);
        c_shipment_key_name         VARCHAR2 (40);
        c_file_common_key           VARCHAR2 (255);

        nheader_key_pos             NUMBER;
        nline_key_pos               NUMBER;
        nshipment_key_pos           NUMBER;
        ntrans_code_pos             NUMBER;

        header_sel_c                INTEGER;
        line_sel_c                  INTEGER;
        shipment_sel_c              INTEGER;

        header_del_c1               INTEGER;
        line_del_c1                 INTEGER;
        shipment_del_c1             INTEGER;

        header_del_c2               INTEGER;
        line_del_c2                 INTEGER;
        shipment_del_c2             INTEGER;

        cheader_select              VARCHAR2 (32000);
        cline_select                VARCHAR2 (32000);
        cshipment_select            VARCHAR2 (32000);

        cheader_from                VARCHAR2 (32000);
        cline_from                  VARCHAR2 (32000);
        cshipment_from              VARCHAR2 (32000);

        cheader_where               VARCHAR2 (32000);
        cline_where                 VARCHAR2 (32000);
        cshipment_where             VARCHAR2 (32000);

        cheader_delete1             VARCHAR2 (32000);
        cline_delete1               VARCHAR2 (32000);
        cshipment_delete1           VARCHAR2 (32000);

        cheader_delete2             VARCHAR2 (32000);
        cline_delete2               VARCHAR2 (32000);
        cshipment_delete2           VARCHAR2 (32000);

        iheader_count               NUMBER;
        iline_count                 NUMBER;
        ishipment_count             NUMBER;

        rheader_rowid               ROWID;
        rline_rowid                 ROWID;
        rshipment_rowid             ROWID;

        cheader_x_interface         VARCHAR2 (50);
        cline_x_interface           VARCHAR2 (50);
        cshipment_x_interface       VARCHAR2 (50);

        rheader_x_rowid             ROWID;
        rline_x_rowid               ROWID;
        rshipment_x_rowid           ROWID;

        iheader_start_num           INTEGER;
        iline_start_num             INTEGER;
        ishipment_start_num         INTEGER;
        dummy                       INTEGER;

        ndocument_type_pos          NUMBER;
        npos1                       NUMBER;
        ntrans_id                   NUMBER;
        n_po_header_id              NUMBER;
        nrelease_id                 NUMBER;
        nrelease_id_pos             NUMBER;
        n_po_line_id                NUMBER;
        npo_line_location_id_pos    NUMBER;
        npo_line_location_id        NUMBER;
        nline_location_id_pos       NUMBER;
        nline_location_id           NUMBER;
        nline_num_pos               NUMBER;
        nline_num                   NUMBER;
        nrelease_num                NUMBER;
        nrelease_num_pos            NUMBER;
        norganization_id            NUMBER;
        nitem_id_pos                NUMBER;
        nitem_id                    NUMBER;

        v_project_acct_installed    BOOLEAN;
        v_project_acct_short_name   VARCHAR2 (2) := 'PA';
        v_project_acct_status       VARCHAR2 (120);
        v_project_acct_industry     VARCHAR2 (120);
        v_project_acct_schema       VARCHAR2 (120);

        v_entity_name               VARCHAR2 (120);
        v_pk1_value                 VARCHAR2 (120);
        v_pk2_value                 VARCHAR2 (120);

        CURSOR c_org_id (p_line_id NUMBER)
        IS
            SELECT DISTINCT ship_to_organization_id
              FROM po_line_locations
             WHERE po_line_id = p_line_id;
    BEGIN
        IF ec_debug.g_debug_level = 3
        THEN
            ec_debug.push ('ECE_POO_TRANSACTION.PUT_DATA_TO_OUTPUT_TABLE');
            ec_debug.pl (3, 'cCommunication_Method: ', ccommunication_method);
            ec_debug.pl (3, 'cTransaction_Type: ', ctransaction_type);
            ec_debug.pl (3, 'iOutput_width: ', ioutput_width);
            ec_debug.pl (3, 'iRun_id: ', irun_id);
            ec_debug.pl (3, 'cHeader_Interface: ', cheader_interface);
            ec_debug.pl (3, 'cLine_Interface: ', cline_interface);
            ec_debug.pl (3, 'cShipment_Interface: ', cshipment_interface);
        END IF;

        BEGIN
            SELECT inventory_organization_id INTO norganization_id FROM financials_system_parameters;
        EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
                ec_debug.pl (0
                            ,'EC'
                            ,'ECE_NO_ROW_SELECTED'
                            ,'PROGRESS_LEVEL'
                            ,xprogress
                            ,'INFO'
                            ,'INVENTORY ORGANIZATION ID'
                            ,'TABLE_NAME'
                            ,'FINANCIALS_SYSTEM_PARAMETERS');
        END;

        IF ec_debug.g_debug_level = 3
        THEN
            ec_debug.pl (3, 'norganization_id: ', norganization_id);
        END IF;

        -- Let's See if Project Accounting is Installed
        /* xProgress := 'POOB-20-1000';
         v_project_acct_installed := fnd_installation.get_app_info(
                                       v_project_acct_short_name, -- i.e. 'PA'
                                       v_project_acct_status,     -- 'I' means it's installed
                                       v_project_acct_industry,
                                       v_project_acct_schema);

         v_project_acct_status := NVL(v_project_acct_status,'X');
         ec_debug.pl(3,'v_project_acct_status: '  ,v_project_acct_status);
         ec_debug.pl(3,'v_project_acct_industry: ',v_project_acct_industry);
         ec_debug.pl(3,'v_project_acct_schema: '  ,v_project_acct_schema);
*/
        xprogress := 'POOB-20-1005';
        ece_flatfile_pvt.select_clause (ctransaction_type
                                       ,ccommunication_method
                                       ,cheader_interface
                                       ,cheader_x_interface
                                       ,l_header_tbl
                                       ,c_header_common_key_name
                                       ,cheader_select
                                       ,cheader_from
                                       ,cheader_where);

        xprogress := 'POOB-20-1010';
        ece_flatfile_pvt.select_clause (ctransaction_type
                                       ,ccommunication_method
                                       ,cline_interface
                                       ,cline_x_interface
                                       ,l_line_tbl
                                       ,c_line_common_key_name
                                       ,cline_select
                                       ,cline_from
                                       ,cline_where);

        xprogress := 'POOB-20-1020';
        ece_flatfile_pvt.select_clause (ctransaction_type
                                       ,ccommunication_method
                                       ,cshipment_interface
                                       ,cshipment_x_interface
                                       ,l_shipment_tbl
                                       ,c_shipment_key_name
                                       ,cshipment_select
                                       ,cshipment_from
                                       ,cshipment_where);

        -- Header Level Find Positions
        xprogress := 'POOB-20-1021';
        ece_flatfile_pvt.find_pos (l_header_tbl, ece_flatfile_pvt.g_translator_code, ntrans_code_pos);

        xprogress := 'POOB-20-1022';
        ece_flatfile_pvt.find_pos (l_header_tbl, c_header_common_key_name, nheader_key_pos);

        xprogress := 'POOB-20-1023';
        ece_flatfile_pvt.find_pos (l_header_tbl, 'RELEASE_NUMBER', nrelease_num_pos);

        xprogress := 'POOB-20-1024';
        ece_flatfile_pvt.find_pos (l_header_tbl, 'PO_RELEASE_ID', nrelease_id_pos);

        xprogress := 'POOB-20-1025';
        ece_flatfile_pvt.find_pos (l_header_tbl, 'DOCUMENT_TYPE', ndocument_type_pos);

        -- Line Level Find Positions
        xprogress := 'POOB-20-1026';
        ece_flatfile_pvt.find_pos (l_line_tbl, c_line_common_key_name, nline_key_pos);

        xprogress := 'POOB-20-1027';
        ece_flatfile_pvt.find_pos (l_line_tbl, 'LINE_NUMBER', nline_num_pos);

        xprogress := 'POOB-20-1028';
        ece_flatfile_pvt.find_pos (l_line_tbl, 'PO_LINE_LOCATION_ID', npo_line_location_id_pos);

        xprogress := 'POOB-20-1029';
        ece_flatfile_pvt.find_pos (l_line_tbl, 'ITEM_ID', nitem_id_pos);

        -- Shipment Level Find Positions
        xprogress := 'POOB-20-1030';
        ece_flatfile_pvt.find_pos (l_shipment_tbl, 'LINE_LOCATION_ID', nline_location_id_pos);

        xprogress := 'POOB-20-1032';
        ece_flatfile_pvt.find_pos (l_shipment_tbl, c_shipment_key_name, nshipment_key_pos);

        IF ec_debug.g_debug_level = 3
        THEN
            ec_debug.pl (3, 'nTrans_code_pos: ', ntrans_code_pos);
            ec_debug.pl (3, 'nHeader_key_pos: ', nheader_key_pos);
            ec_debug.pl (3, 'nRelease_num_pos: ', nrelease_num_pos);
            ec_debug.pl (3, 'nRelease_ID_pos: ', nrelease_id_pos);
            ec_debug.pl (3, 'nDocument_type_pos: ', ndocument_type_pos);
            ec_debug.pl (3, 'nLine_key_pos: ', nline_key_pos);
            ec_debug.pl (3, 'nLine_num_pos: ', nline_num_pos);
            ec_debug.pl (3, 'nPO_Line_Location_ID_pos: ', npo_line_location_id_pos);
            ec_debug.pl (3, 'nItem_ID_pos: ', nitem_id_pos);
            ec_debug.pl (3, 'nLine_Location_ID_pos: ', nline_location_id_pos);
            ec_debug.pl (3, 'nShipment_key_pos: ', nshipment_key_pos);
        END IF;

        -- Build SELECT Statement
        xprogress := 'POOB-20-1035';
        cheader_where := cheader_where || ' AND ' || cheader_interface || '.run_id = ' || ':Run_id';

        cline_where :=
               cline_where
            || ' AND '
            || cline_interface
            || '.run_id = '
            || ':Run_id'
            || ' AND '
            || cline_interface
            || '.po_header_id = :po_header_id AND '
            || cline_interface
            || '.release_number = :por_release_num';

        cshipment_where :=
               cshipment_where
            || ' AND '
            || cshipment_interface
            || '.run_id ='
            || ':Run_id'
            || ' AND '
            || cshipment_interface
            || '.po_header_id = :po_header_id AND '
            || cshipment_interface
            || '.po_line_id = :po_line_id AND '
            || cshipment_interface
            || '.release_number = :por_release_num AND (('
            || cshipment_interface
            || '.release_number = 0) OR ('
            || cshipment_interface
            || '.release_number <> 0 AND '
            || cshipment_interface
            || '.shipment_number = :shipment_number))';

        xprogress := 'POOB-20-1040';
        cheader_select :=
               cheader_select
            || ','
            || cheader_interface
            || '.rowid,'
            || cheader_x_interface
            || '.rowid,'
            || cheader_interface
            || '.po_header_id,'
            || cheader_interface
            || '.release_number ';

        cline_select :=
               cline_select
            || ','
            || cline_interface
            || '.rowid,'
            || cline_x_interface
            || '.rowid,'
            || cline_interface
            || '.po_line_id,'
            || cline_interface
            || '.line_number ';

        cshipment_select :=
               cshipment_select
            || ','
            || cshipment_interface
            || '.rowid,'
            || cshipment_x_interface
            || '.rowid,'
            || cshipment_interface
            || '.shipment_number ';

        xprogress := 'POOB-20-1050';
        cheader_select :=
               cheader_select
            || cheader_from
            || cheader_where
            || ' ORDER BY '
            || cheader_interface
            || '.po_header_id,'
            || cheader_interface
            || '.release_number '
            || ' FOR UPDATE';
        ec_debug.pl (3, 'cHeader_select: ', cheader_select);

        cline_select :=
               cline_select
            || cline_from
            || cline_where
            || ' ORDER BY '
            || cline_interface
            || '.line_number '
            || ' FOR UPDATE';
        ec_debug.pl (3, 'cLine_select: ', cline_select);

        cshipment_select :=
               cshipment_select
            || cshipment_from
            || cshipment_where
            || ' ORDER BY '
            || cshipment_interface
            || '.shipment_number '
            || ' FOR UPDATE';
        ec_debug.pl (3, 'cShipment_select: ', cshipment_select);

        xprogress := 'POOB-20-1060';
        cheader_delete1 := 'DELETE FROM ' || cheader_interface || ' WHERE rowid = :col_rowid';

        cline_delete1 := 'DELETE FROM ' || cline_interface || ' WHERE rowid = :col_rowid';

        cshipment_delete1 := 'DELETE FROM ' || cshipment_interface || ' WHERE rowid = :col_rowid';

        xprogress := 'POOB-20-1070';
        cheader_delete2 := 'DELETE FROM ' || cheader_x_interface || ' WHERE rowid = :col_rowid';

        cline_delete2 := 'DELETE FROM ' || cline_x_interface || ' WHERE rowid = :col_rowid';

        cshipment_delete2 := 'DELETE FROM ' || cshipment_x_interface || ' WHERE rowid = :col_rowid';

        IF ec_debug.g_debug_level = 3
        THEN
            ec_debug.pl (3, 'cHeader_delete1: ', cheader_delete1);
            ec_debug.pl (3, 'cLine_delete1: ', cline_delete1);
            ec_debug.pl (3, 'cShipment_delete1: ', cshipment_delete1);
            ec_debug.pl (3, 'cHeader_delete2: ', cheader_delete2);
            ec_debug.pl (3, 'cLine_delete2: ', cline_delete2);
            ec_debug.pl (3, 'cShipment_delete2: ', cshipment_delete2);
        END IF;

        -- ***************************************************
        -- ***   Get data setup for the dynamic SQL call.
        -- ***   Open a cursor for each of the SELECT call
        -- ***   This tells the database to reserve spaces
        -- ***   for the data returned by the SQL statement
        -- ***************************************************
        xprogress := 'POOB-20-1080';
        header_sel_c := DBMS_SQL.open_cursor;

        xprogress := 'POOB-20-1090';
        line_sel_c := DBMS_SQL.open_cursor;

        xprogress := 'POOB-20-1100';
        shipment_sel_c := DBMS_SQL.open_cursor;

        xprogress := 'POOB-20-1110';
        header_del_c1 := DBMS_SQL.open_cursor;

        xprogress := 'POOB-20-1120';
        line_del_c1 := DBMS_SQL.open_cursor;

        xprogress := 'POOB-20-1130';
        shipment_del_c1 := DBMS_SQL.open_cursor;

        xprogress := 'POOB-20-1140';
        header_del_c2 := DBMS_SQL.open_cursor;

        xprogress := 'POOB-20-1150';
        line_del_c2 := DBMS_SQL.open_cursor;

        xprogress := 'POOB-20-1160';
        shipment_del_c2 := DBMS_SQL.open_cursor;

        -- *****************************************
        -- Parse each of the SELECT statement
        -- so the database understands the command
        -- *****************************************
        xprogress := 'POOB-20-1170';

        BEGIN
            DBMS_SQL.parse (header_sel_c, cheader_select, DBMS_SQL.native);
        EXCEPTION
            WHEN OTHERS
            THEN
                ece_error_handling_pvt.print_parse_error (DBMS_SQL.last_error_position, cheader_select);
                app_exception.raise_exception;
        END;

        xprogress := 'POOB-20-1180';

        BEGIN
            DBMS_SQL.parse (line_sel_c, cline_select, DBMS_SQL.native);
        EXCEPTION
            WHEN OTHERS
            THEN
                ece_error_handling_pvt.print_parse_error (DBMS_SQL.last_error_position, cline_select);
                app_exception.raise_exception;
        END;

        xprogress := 'POOB-20-1190';

        BEGIN
            DBMS_SQL.parse (shipment_sel_c, cshipment_select, DBMS_SQL.native);
        EXCEPTION
            WHEN OTHERS
            THEN
                ece_error_handling_pvt.print_parse_error (DBMS_SQL.last_error_position, cshipment_select);
                app_exception.raise_exception;
        END;

        xprogress := 'POOB-20-1200';

        BEGIN
            DBMS_SQL.parse (header_del_c1, cheader_delete1, DBMS_SQL.native);
        EXCEPTION
            WHEN OTHERS
            THEN
                ece_error_handling_pvt.print_parse_error (DBMS_SQL.last_error_position, cheader_delete1);
                app_exception.raise_exception;
        END;

        xprogress := 'POOB-20-1210';

        BEGIN
            DBMS_SQL.parse (line_del_c1, cline_delete1, DBMS_SQL.native);
        EXCEPTION
            WHEN OTHERS
            THEN
                ece_error_handling_pvt.print_parse_error (DBMS_SQL.last_error_position, cline_delete1);
                app_exception.raise_exception;
        END;

        xprogress := 'POOB-20-1220';

        BEGIN
            DBMS_SQL.parse (shipment_del_c1, cshipment_delete1, DBMS_SQL.native);
        EXCEPTION
            WHEN OTHERS
            THEN
                ece_error_handling_pvt.print_parse_error (DBMS_SQL.last_error_position, cshipment_delete1);
                app_exception.raise_exception;
        END;

        xprogress := 'POOB-20-1230';

        BEGIN
            DBMS_SQL.parse (header_del_c2, cheader_delete2, DBMS_SQL.native);
        EXCEPTION
            WHEN OTHERS
            THEN
                ece_error_handling_pvt.print_parse_error (DBMS_SQL.last_error_position, cheader_delete2);
                app_exception.raise_exception;
        END;

        xprogress := 'POOB-20-1240';

        BEGIN
            DBMS_SQL.parse (line_del_c2, cline_delete2, DBMS_SQL.native);
        EXCEPTION
            WHEN OTHERS
            THEN
                ece_error_handling_pvt.print_parse_error (DBMS_SQL.last_error_position, cline_delete2);
                app_exception.raise_exception;
        END;

        xprogress := 'POOB-20-1250';

        BEGIN
            DBMS_SQL.parse (shipment_del_c2, cshipment_delete2, DBMS_SQL.native);
        EXCEPTION
            WHEN OTHERS
            THEN
                ece_error_handling_pvt.print_parse_error (DBMS_SQL.last_error_position, cshipment_delete2);
                app_exception.raise_exception;
        END;

        -- *************
        -- set counter
        -- *************
        xprogress := 'POOB-20-1260';
        iheader_count := l_header_tbl.COUNT;
        iline_count := l_line_tbl.COUNT;
        ishipment_count := l_shipment_tbl.COUNT;

        IF ec_debug.g_debug_level = 3
        THEN
            ec_debug.pl (3, 'iHeader_count: ', iheader_count);
            ec_debug.pl (3, 'iLine_count: ', iline_count);
            ec_debug.pl (3, 'iShipment_count: ', ishipment_count);
        END IF;

        -- ******************************************************
        --  Define TYPE for every columns in the SELECT statement
        --  For each piece of the data returns, we need to tell
        --  the database what type of information it will be.
        --  e.g. ID is NUMBER, due_date is DATE
        --  However, for simplicity, we will convert
        --  everything to varchar2.
        -- ******************************************************
        xprogress := 'POOB-20-1270';
        ece_flatfile_pvt.define_interface_column_type (header_sel_c
                                                      ,cheader_select
                                                      ,ece_flatfile_pvt.g_maxcolwidth
                                                      ,l_header_tbl);

        -- ***************************************************
        -- Need rowid for delete (Header Level)
        -- ***************************************************
        xprogress := 'POOB-20-1280';
        DBMS_SQL.define_column_rowid (header_sel_c, iheader_count + 1, rheader_rowid);

        xprogress := 'POOB-20-1290';
        DBMS_SQL.define_column_rowid (header_sel_c, iheader_count + 2, rheader_x_rowid);

        xprogress := 'POOB-20-1300';
        DBMS_SQL.define_column (header_sel_c, iheader_count + 3, n_po_header_id);

        xprogress := 'POOB-20-1310';
        ece_flatfile_pvt.define_interface_column_type (line_sel_c
                                                      ,cline_select
                                                      ,ece_flatfile_pvt.g_maxcolwidth
                                                      ,l_line_tbl);

        -- ***************************************************
        -- Need rowid for delete (Line Level)
        -- ***************************************************
        xprogress := 'POOB-20-1320';
        DBMS_SQL.define_column_rowid (line_sel_c, iline_count + 1, rline_rowid);

        xprogress := 'POOB-20-1330';
        DBMS_SQL.define_column_rowid (line_sel_c, iline_count + 2, rline_x_rowid);

        xprogress := 'POOB-20-1340';
        DBMS_SQL.define_column (line_sel_c, iline_count + 3, n_po_line_id);

        xprogress := 'POOB-20-1350';
        ece_flatfile_pvt.define_interface_column_type (shipment_sel_c
                                                      ,cshipment_select
                                                      ,ece_flatfile_pvt.g_maxcolwidth
                                                      ,l_shipment_tbl);

        -- ***************************************************
        -- Need rowid for delete (Shipment Level)
        -- ***************************************************
        xprogress := 'POOB-20-1360';
        DBMS_SQL.define_column_rowid (shipment_sel_c, ishipment_count + 1, rshipment_rowid);

        xprogress := 'POOB-20-1370';
        DBMS_SQL.define_column_rowid (shipment_sel_c, ishipment_count + 2, rshipment_x_rowid);

        -- ************************************************************
        -- ***  The following is custom tailored for this transaction
        -- ***  It find the values and use them in the WHERE clause to
        -- ***  join tables together.
        -- ************************************************************
        -- *******************************************
        -- To complete the Line SELECT statement,
        -- we will need values for the join condition.
        -- *******************************************
        xprogress := 'POOB-20-1375';
        DBMS_SQL.bind_variable (header_sel_c, 'Run_id', irun_id);
        DBMS_SQL.bind_variable (line_sel_c, 'Run_id', irun_id);
        DBMS_SQL.bind_variable (shipment_sel_c, 'Run_id', irun_id);

        --- EXECUTE the SELECT statement
        xprogress := 'POOB-20-1380';
        dummy := DBMS_SQL.execute (header_sel_c);

        -- ********************************************************************
        -- ***   With data for each HEADER line, populate the ECE_OUTPUT table
        -- ***   then populate ECE_OUTPUT with data from all LINES that belongs
        -- ***   to the HEADER. Then populate ECE_OUTPUT with data from all
        -- ***   LINE TAX that belongs to the LINE.
        -- ********************************************************************

        -- HEADER - LINE - SHIPMENT ...
        xprogress := 'POOB-20-1390';

        WHILE DBMS_SQL.fetch_rows (header_sel_c) > 0
        LOOP                                                                                                   -- Header
            -- ******************************
            --   store values in pl/sql table
            -- ******************************
            xprogress := 'POOB-20-1400';
            ece_flatfile_pvt.assign_column_value_to_tbl (header_sel_c, l_header_tbl);

            xprogress := 'POOB-20-1410';
            DBMS_SQL.COLUMN_VALUE (header_sel_c, iheader_count + 1, rheader_rowid);

            xprogress := 'POOB-20-1420';
            DBMS_SQL.COLUMN_VALUE (header_sel_c, iheader_count + 2, rheader_x_rowid);

            xprogress := 'POOB-20-1430';
            DBMS_SQL.COLUMN_VALUE (header_sel_c, iheader_count + 3, n_po_header_id);

            xprogress := 'POOB-20-1440';
            nrelease_num := l_header_tbl (nrelease_num_pos).VALUE;

            xprogress := 'POOB-20-1450';
            nrelease_id := l_header_tbl (nrelease_id_pos).VALUE;

            IF ec_debug.g_debug_level = 3
            THEN
                ec_debug.pl (3, 'nRelease_num: ', nrelease_num);
                ec_debug.pl (3, 'nRelease_ID: ', nrelease_id);
            END IF;

            BEGIN
                xprogress := 'POOB-20-1455';

                /* Bug 2396394 Added the document type CONTRACT in SQL below */

                SELECT DECODE (l_header_tbl (ndocument_type_pos).VALUE
                              ,'BLANKET', 'NB'
                              ,'STANDARD', 'NS'
                              ,'PLANNED', 'NP'
                              ,'RELEASE', 'NR'
                              ,'BLANKET RELEASE', 'NR'
                              ,'CONTRACT', 'NC'
                              ,'NR')
                  INTO l_document_type
                  FROM DUAL;
            EXCEPTION
                WHEN NO_DATA_FOUND
                THEN
                    ec_debug.pl (0
                                ,'EC'
                                ,'ECE_DECODE_FAILED'
                                ,'PROGRESS_LEVEL'
                                ,xprogress
                                ,'CODE'
                                ,l_header_tbl (ndocument_type_pos).VALUE);
            END;

            ec_debug.pl (3, 'l_document_type: ', l_document_type);

            xprogress := 'POOB-20-1460';
            c_file_common_key := RPAD (SUBSTRB (NVL (l_header_tbl (ntrans_code_pos).VALUE, ' '), 1, 25), 25);

            xprogress := 'POOB-20-1470';
            c_file_common_key :=
                   c_file_common_key
                || RPAD (SUBSTRB (NVL (l_header_tbl (nheader_key_pos).VALUE, ' '), 1, 22), 22)
                || RPAD (' ', 22)
                || RPAD (' ', 22);

            ec_debug.pl (3, 'c_file_common_key: ', c_file_common_key);

            xprogress := 'POOB-20-1480';
            ece_poo_transaction.write_to_file (ctransaction_type
                                              ,ccommunication_method
                                              ,cheader_interface
                                              ,l_header_tbl
                                              ,ioutput_width
                                              ,irun_id
                                              ,c_file_common_key
                                              ,NULL);

            IF l_document_type = 'NR'
            THEN                                                                             -- If this is a Release PO.
                xprogress := 'POOB-20-1481';
                v_entity_name := 'PO_RELEASES';
                v_pk1_value := nrelease_id;

                IF ec_debug.g_debug_level = 3
                THEN
                    ec_debug.pl (3, 'release_id: ', nrelease_id);
                END IF;
            ELSE                                                                         -- If this is a non-Release PO.
                xprogress := 'POOB-20-1482';
                v_entity_name := 'PO_HEADERS';
                v_pk1_value := n_po_header_id;

                IF ec_debug.g_debug_level = 3
                THEN
                    ec_debug.pl (3, 'po_header_id: ', n_po_header_id);
                END IF;
            END IF;

            xprogress := 'POOB-20-1483';
            put_att_to_output_table (ccommunication_method
                                    ,ctransaction_type
                                    ,ioutput_width
                                    ,irun_id
                                    ,2
                                    ,3
                                    ,catt_header_interface
                                    ,catt_detail_interface
                                    ,v_entity_name
                                    ,'VENDOR'
                                    ,v_pk1_value
                                    ,NULL
                                    ,NULL
                                    ,NULL
                                    ,NULL
                                    ,c_file_common_key);

            -- ***************************************************
            -- With Header data at hand, we can assign values to
            -- place holders (foreign keys) in Line_select and
            -- Line_detail_Select
            -- ***************************************************
            -- ******************************************
            -- set values into binding variables
            -- ******************************************
            xprogress := 'POOB-20-1490';
            DBMS_SQL.bind_variable (line_sel_c, 'po_header_id', n_po_header_id);

            xprogress := 'POOB-20-1500';
            DBMS_SQL.bind_variable (shipment_sel_c, 'po_header_id', n_po_header_id);

            xprogress := 'POOB-20-1505';
            DBMS_SQL.bind_variable (line_sel_c, 'por_release_num', nrelease_num);

            xprogress := 'POOB-20-1506';
            DBMS_SQL.bind_variable (shipment_sel_c, 'por_release_num', nrelease_num);

            xprogress := 'POOB-20-1510';
            dummy := DBMS_SQL.execute (line_sel_c);

            -- ***************************************************
            -- line loop starts here
            -- ***************************************************
            xprogress := 'POOB-20-1520';

            WHILE DBMS_SQL.fetch_rows (line_sel_c) > 0
            LOOP                                                                                                --- Line
                -- ***************************************************
                --   store values in pl/sql table
                -- ***************************************************
                xprogress := 'POOB-20-1530';
                ece_flatfile_pvt.assign_column_value_to_tbl (line_sel_c, l_line_tbl);

                xprogress := 'POOB-20-1533';
                DBMS_SQL.COLUMN_VALUE (line_sel_c, iline_count + 1, rline_rowid);

                xprogress := 'POOB-20-1535';
                DBMS_SQL.COLUMN_VALUE (line_sel_c, iline_count + 2, rline_x_rowid);

                xprogress := 'POOB-20-1537';
                DBMS_SQL.COLUMN_VALUE (line_sel_c, iline_count + 3, n_po_line_id);

                xprogress := 'POOB-20-1540';
                nline_num := l_line_tbl (nline_num_pos).VALUE;

                xprogress := 'POOB-20-1544';
                npo_line_location_id := l_line_tbl (npo_line_location_id_pos).VALUE;

                xprogress := 'POOB-20-1545';
                nitem_id := l_line_tbl (nitem_id_pos).VALUE;

                IF ec_debug.g_debug_level = 3
                THEN
                    ec_debug.pl (3, 'n_po_line_id: ', n_po_line_id);
                    ec_debug.pl (3, 'nLine_num: ', nline_num);
                    ec_debug.pl (3, 'nPO_Line_Location_ID: ', npo_line_location_id);
                    ec_debug.pl (3, 'nItem_ID: ', nitem_id);
                END IF;

                xprogress := 'POOB-20-1550';
                c_file_common_key :=
                       RPAD (SUBSTRB (NVL (l_header_tbl (ntrans_code_pos).VALUE, ' '), 1, 25), 25)
                    || RPAD (SUBSTRB (NVL (l_header_tbl (nheader_key_pos).VALUE, ' '), 1, 22), 22)
                    || RPAD (SUBSTRB (NVL (l_line_tbl (nline_key_pos).VALUE, ' '), 1, 22), 22)
                    || RPAD (' ', 22);

                IF ec_debug.g_debug_level = 3
                THEN
                    ec_debug.pl (3, 'c_file_common_key: ', c_file_common_key);
                END IF;

                xprogress := 'POOB-20-1551';
                ece_poo_transaction.write_to_file (ctransaction_type
                                                  ,ccommunication_method
                                                  ,cline_interface
                                                  ,l_line_tbl
                                                  ,ioutput_width
                                                  ,irun_id
                                                  ,c_file_common_key
                                                  ,NULL);

                -- Line Level Attachment Handler
                /*  IF l_document_type = 'NR' THEN -- If this is a Release PO.
                     xProgress := 'POOB-20-1552';
                     v_entity_name := 'PO_SHIPMENTS';
                     v_pk1_value := nPO_Line_Location_ID; -- LINE_LOCATION_ID
                  ELSE -- If this is a non-Release PO.
                  END IF;
                  Bug 2187958
                */
                xprogress := 'POOB-20-1553';
                v_entity_name := 'PO_LINES';
                v_pk1_value := n_po_line_id;                                                                  -- LINE_ID

                xprogress := 'POOB-20-1554';
                put_att_to_output_table (ccommunication_method
                                        ,ctransaction_type
                                        ,ioutput_width
                                        ,irun_id
                                        ,5
                                        ,6
                                        ,catt_header_interface
                                        ,catt_detail_interface
                                        ,v_entity_name
                                        ,'VENDOR'
                                        ,v_pk1_value
                                        ,NULL
                                        ,NULL
                                        ,NULL
                                        ,NULL
                                        ,c_file_common_key);

                -- Master Item Attachment Handler
                xprogress := 'POOB-20-1555';
                v_entity_name := 'MTL_SYSTEM_ITEMS';
                v_pk1_value := norganization_id;                                              -- Master Inventory Org ID

                v_pk2_value := nitem_id;                                                                      -- Item ID

                IF ec_debug.g_debug_level = 3
                THEN
                    ec_debug.pl (3, 'Master Org ID: ', v_pk1_value);
                    ec_debug.pl (3, 'Item ID: ', v_pk2_value);
                END IF;

                xprogress := 'POOB-20-1556';
                put_att_to_output_table (ccommunication_method
                                        ,ctransaction_type
                                        ,ioutput_width
                                        ,irun_id
                                        ,7
                                        ,8
                                        ,catt_header_interface
                                        ,catt_detail_interface
                                        ,v_entity_name
                                        ,'VENDOR'
                                        ,v_pk1_value
                                        ,v_pk2_value
                                        ,NULL
                                        ,NULL
                                        ,NULL
                                        ,c_file_common_key);

                /* Bug 3550723
                    -- Inventory Item Attachment Handler
                    xProgress := 'POOB-20-1557';
                    FOR v_org_id IN c_org_id(n_po_line_id) LOOP -- Value passed is the Line ID
                       IF v_org_id.ship_to_organization_id <> nOrganization_ID THEN -- Only do this if it is not the same as the Master Org ID
                          v_pk1_value := v_org_id.ship_to_organization_id;

                         if ec_debug.G_debug_level = 3 then
                          ec_debug.pl(3,'Inventory Org ID: ',v_pk1_value);
                         end if;

                          xProgress := 'POOB-20-1558';
                          put_att_to_output_table(cCommunication_Method,
                                                  cTransaction_Type,
                                                  iOutput_width,
                                                  iRun_id,
                                                  9,
                                                  10,
                                                  cAtt_Header_Interface,
                                                  cAtt_Detail_Interface,
                                                  v_entity_name,
                                                  'VENDOR',
                                                  v_pk1_value,
                                                  v_pk2_value,
                                                  NULL,
                                                  NULL,
                                                  NULL,
                                                  c_file_common_key);
                       END IF;
                    END LOOP;
                */

                -- **************************
                --   set LINE_NUMBER values
                -- **************************
                xprogress := 'POOB-20-1560';
                DBMS_SQL.bind_variable (shipment_sel_c, 'po_line_id', n_po_line_id);

                xprogress := 'POOB-20-1575';
                DBMS_SQL.bind_variable (shipment_sel_c, 'shipment_number', nline_num);

                xprogress := 'POOB-20-1580';
                dummy := DBMS_SQL.execute (shipment_sel_c);

                -- ****************************
                --  Shipment loop starts here
                -- ****************************
                xprogress := 'POCOB-10-1590';

                WHILE DBMS_SQL.fetch_rows (shipment_sel_c) > 0
                LOOP                                                                                       --- Shipments
                    -- *********************************
                    --  store values in pl/sql table
                    -- *********************************
                    xprogress := 'POCOB-10-1600';
                    ece_flatfile_pvt.assign_column_value_to_tbl (shipment_sel_c, l_shipment_tbl);

                    xprogress := 'POCOB-10-1603';
                    DBMS_SQL.COLUMN_VALUE (shipment_sel_c, ishipment_count + 1, rshipment_rowid);

                    xprogress := 'POCOB-10-1606';
                    DBMS_SQL.COLUMN_VALUE (shipment_sel_c, ishipment_count + 2, rshipment_x_rowid);

                    xprogress := 'POCOB-10-1610';
                    nline_location_id := l_shipment_tbl (nline_location_id_pos).VALUE;

                    IF ec_debug.g_debug_level = 3
                    THEN
                        ec_debug.pl (3, 'Ship Level Line Location ID: ', nline_location_id);
                    END IF;

                    xprogress := 'POCOB-10-1620';
                    c_file_common_key :=
                           RPAD (SUBSTRB (NVL (l_header_tbl (ntrans_code_pos).VALUE, ' '), 1, 25), 25)
                        || RPAD (SUBSTRB (NVL (l_header_tbl (nheader_key_pos).VALUE, ' '), 1, 22), 22)
                        || RPAD (SUBSTRB (NVL (l_line_tbl (nline_key_pos).VALUE, ' '), 1, 22), 22)
                        || RPAD (SUBSTRB (NVL (l_shipment_tbl (nshipment_key_pos).VALUE, ' '), 1, 22), 22);

                    IF ec_debug.g_debug_level = 3
                    THEN
                        ec_debug.pl (3, 'c_file_common_key: ', c_file_common_key);
                    END IF;

                    xprogress := 'POOB-20-1630';
                    ece_poo_transaction.write_to_file (ctransaction_type
                                                      ,ccommunication_method
                                                      ,cshipment_interface
                                                      ,l_shipment_tbl
                                                      ,ioutput_width
                                                      ,irun_id
                                                      ,c_file_common_key
                                                      ,NULL);

                    -- Shipment Level Attachment Handler
                    v_entity_name := 'PO_SHIPMENTS';
                    v_pk1_value := nline_location_id;

                    xprogress := 'POOB-20-1632';
                    put_att_to_output_table (ccommunication_method
                                            ,ctransaction_type
                                            ,ioutput_width
                                            ,irun_id
                                            ,12
                                            ,13
                                            ,catt_header_interface
                                            ,catt_detail_interface
                                            ,v_entity_name
                                            ,'VENDOR'
                                            ,v_pk1_value
                                            ,NULL
                                            ,NULL
                                            ,NULL
                                            ,NULL
                                            ,c_file_common_key);

                    -- Project Level Handler
                    xprogress := 'POOB-20-1634';
                    --    IF v_project_acct_status = 'I' THEN -- Project Accounting is Installed --bug1891291
                    ece_poo_transaction.put_distdata_to_out_tbl (ccommunication_method
                                                                ,ctransaction_type
                                                                ,ioutput_width
                                                                ,irun_id
                                                                ,cdistribution_interface
                                                                ,n_po_header_id
                                                                ,                                        -- PO_HEADER_ID
                                                                 nrelease_id
                                                                ,                                       -- PO_RELEASE_ID
                                                                 n_po_line_id
                                                                ,                                          -- PO_LINE_ID
                                                                 nline_location_id
                                                                ,                                    -- LINE_LOCATION_ID
                                                                 c_file_common_key);
                    --   END IF;  --bug 1891291

                    xprogress := 'POOB-20-1640';
                    DBMS_SQL.bind_variable (shipment_del_c1, 'col_rowid', rshipment_rowid);

                    xprogress := 'POOB-20-1650';
                    DBMS_SQL.bind_variable (shipment_del_c2, 'col_rowid', rshipment_x_rowid);

                    xprogress := 'POOB-20-1660';
                    dummy := DBMS_SQL.execute (shipment_del_c1);

                    xprogress := 'POOB-20-1670';
                    dummy := DBMS_SQL.execute (shipment_del_c2);
                END LOOP;                                                                              -- Shipment Level

                xprogress := 'POOB-20-1674';

                IF DBMS_SQL.last_row_count = 0
                THEN
                    v_levelprocessed := 'SHIPMENT';
                    ec_debug.pl (0
                                ,'EC'
                                ,'ECE_NO_DB_ROW_PROCESSED'
                                ,'PROGRESS_LEVEL'
                                ,xprogress
                                ,'LEVEL_PROCESSED'
                                ,v_levelprocessed
                                ,'TRANSACTION_TYPE'
                                ,ctransaction_type);
                END IF;

                -- *********************
                -- Use rowid for delete
                -- *********************
                xprogress := 'POOB-20-1680';
                DBMS_SQL.bind_variable (line_del_c1, 'col_rowid', rline_rowid);

                xprogress := 'POOB-20-1690';
                DBMS_SQL.bind_variable (line_del_c2, 'col_rowid', rline_x_rowid);

                xprogress := 'POOB-20-1700';
                dummy := DBMS_SQL.execute (line_del_c1);

                xprogress := 'POOB-20-1710';
                dummy := DBMS_SQL.execute (line_del_c2);
            END LOOP;                                                                                      -- Line Level

            xprogress := 'POOB-20-1714';

            IF DBMS_SQL.last_row_count = 0
            THEN
                v_levelprocessed := 'LINE';
                ec_debug.pl (0
                            ,'EC'
                            ,'ECE_NO_DB_ROW_PROCESSED'
                            ,'PROGRESS_LEVEL'
                            ,xprogress
                            ,'LEVEL_PROCESSED'
                            ,v_levelprocessed
                            ,'TRANSACTION_TYPE'
                            ,ctransaction_type);
            END IF;

            xprogress := 'POOB-20-1720';
            DBMS_SQL.bind_variable (header_del_c1, 'col_rowid', rheader_rowid);

            xprogress := 'POOB-20-1730';
            DBMS_SQL.bind_variable (header_del_c2, 'col_rowid', rheader_x_rowid);

            xprogress := 'POOB-20-1740';
            dummy := DBMS_SQL.execute (header_del_c1);

            xprogress := 'POOB-20-1750';
            dummy := DBMS_SQL.execute (header_del_c2);
        END LOOP;                                                                                        -- Header Level

        xprogress := 'POOB-20-1754';

        IF DBMS_SQL.last_row_count = 0
        THEN
            v_levelprocessed := 'HEADER';
            ec_debug.pl (0
                        ,'EC'
                        ,'ECE_NO_DB_ROW_PROCESSED'
                        ,'PROGRESS_LEVEL'
                        ,xprogress
                        ,'LEVEL_PROCESSED'
                        ,v_levelprocessed
                        ,'TRANSACTION_TYPE'
                        ,ctransaction_type);
        END IF;

        xprogress := 'POOB-20-1760';
        DBMS_SQL.close_cursor (header_sel_c);

        xprogress := 'POOB-20-1770';
        DBMS_SQL.close_cursor (line_sel_c);

        xprogress := 'POOB-20-1780';
        DBMS_SQL.close_cursor (shipment_sel_c);

        xprogress := 'POOB-20-1790';
        DBMS_SQL.close_cursor (header_del_c1);

        xprogress := 'POOB-20-1800';
        DBMS_SQL.close_cursor (line_del_c1);

        xprogress := 'POOB-20-1812';
        DBMS_SQL.close_cursor (shipment_del_c1);

        xprogress := 'POOB-20-1814';
        DBMS_SQL.close_cursor (header_del_c2);

        xprogress := 'POOB-20-1816';
        DBMS_SQL.close_cursor (line_del_c2);

        xprogress := 'POOB-20-1818';
        DBMS_SQL.close_cursor (shipment_del_c2);

        -- Bug 2490109 Closing the distribution cursors.
        xprogress := 'POOB-50-1819';

        IF (ece_poo_transaction.project_sel_c > 0)
        THEN                                                                                               --Bug 2819176
            DBMS_SQL.close_cursor (ece_poo_transaction.project_sel_c);

            xprogress := 'POOB-50-1820';
            DBMS_SQL.close_cursor (ece_poo_transaction.project_del_c1);

            xprogress := 'POOB-50-1821';
            DBMS_SQL.close_cursor (ece_poo_transaction.project_del_c2);
        END IF;

        xprogress := 'POOB-20-1820';

        IF ec_debug.g_debug_level >= 2
        THEN
            ec_debug.pop ('ECE_POO_TRANSACTION.PUT_DATA_TO_OUTPUT_TABLE');
        END IF;
    EXCEPTION
        WHEN OTHERS
        THEN
            ec_debug.pl (0
                        ,'EC'
                        ,'ECE_PROGRAM_ERROR'
                        ,'PROGRESS_LEVEL'
                        ,xprogress);
            ec_debug.pl (0
                        ,'EC'
                        ,'ECE_ERROR_MESSAGE'
                        ,'ERROR_MESSAGE'
                        ,SQLERRM);

            app_exception.raise_exception;
    END put_data_to_output_table;

    PROCEDURE update_po (document_type    IN VARCHAR2
                        ,po_number        IN VARCHAR2
                        ,po_type          IN VARCHAR2
                        ,release_number   IN VARCHAR2)
    IS
        xprogress            VARCHAR2 (80);
        l_document_type      VARCHAR2 (25);
        l_document_subtype   VARCHAR2 (25);
        l_document_id        NUMBER;
        l_header_id          NUMBER;
        l_release_id         NUMBER;
        l_error_code         NUMBER;
        l_error_buf          VARCHAR2 (1000);
        l_error_stack        VARCHAR2 (2000);
    BEGIN
        IF ec_debug.g_debug_level >= 2
        THEN
            ec_debug.push ('ECE_POO_TRANSACTION.UPDATE_PO');
            ec_debug.pl (3, 'document_type: ', document_type);
            ec_debug.pl (3, 'po_number: ', po_number);
            ec_debug.pl (3, 'po_type: ', po_type);
            ec_debug.pl (3, 'release_number: ', release_number);
        END IF;

        xprogress := 'POOB-30-1000';

        BEGIN
            SELECT po_header_id
              INTO l_header_id
              FROM po_headers
             WHERE segment1 = po_number AND type_lookup_code = po_type;
        EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
                ec_debug.pl (0
                            ,'EC'
                            ,'ECE_NO_ROW_SELECTED'
                            ,'PROGRESS_LEVEL'
                            ,xprogress
                            ,'INFO'
                            ,'PO HEADER ID'
                            ,'TABLE_NAME'
                            ,'PO_HEADERS');
        END;

        IF ec_debug.g_debug_level = 3
        THEN
            ec_debug.pl (3, 'l_header_id: ', l_header_id);
        END IF;

        -- Perform the first update if this is a Standard or Blanket PO
        xprogress := 'POOB-30-1010';

        IF document_type NOT IN ('NR', 'CR')
        THEN
            xprogress := 'POOB-30-1020';

            UPDATE po_headers
               SET last_update_date = SYSDATE
                  ,printed_date = SYSDATE
                  ,print_count = NVL (print_count, 0) + 1
                  ,edi_processed_flag = 'Y'
             WHERE po_header_id = l_header_id;

            IF SQL%NOTFOUND
            THEN
                ec_debug.pl (0
                            ,'EC'
                            ,'ECE_NO_ROW_UPDATED'
                            ,'PROGRESS_LEVEL'
                            ,xprogress
                            ,'INFO'
                            ,'EDI PROCESSED'
                            ,'TABLE_NAME'
                            ,'PO_HEADERS');
            END IF;

            -- Perform the same update for the Archive Table.
            xprogress := 'POOB-30-1022';

            UPDATE po_headers_archive
               SET last_update_date = SYSDATE
                  ,printed_date = SYSDATE
                  ,print_count = NVL (print_count, 0) + 1
                  ,edi_processed_flag = 'Y'
             WHERE po_header_id = l_header_id AND latest_external_flag = 'Y';

            IF SQL%NOTFOUND
            THEN
                ec_debug.pl (0
                            ,'EC'
                            ,'ECE_NO_ROW_UPDATED'
                            ,'PROGRESS_LEVEL'
                            ,xprogress
                            ,'INFO'
                            ,'EDI PROCESSED'
                            ,'TABLE_NAME'
                            ,'PO_HEADERS_ARCHIVE');
            END IF;
        ELSE
            -- Get the po_release_id, as it is needed here for the update and
            -- later for the archive call
            xprogress := 'POOB-30-1024';

            BEGIN
                SELECT po_release_id
                  INTO l_release_id
                  FROM po_releases
                 WHERE release_num = release_number AND po_header_id = l_header_id;
            EXCEPTION
                WHEN NO_DATA_FOUND
                THEN
                    ec_debug.pl (0
                                ,'EC'
                                ,'ECE_NO_ROW_SELECTED'
                                ,'PROGRESS_LEVEL'
                                ,xprogress
                                ,'INFO'
                                ,'PO RELEASE ID'
                                ,'TABLE_NAME'
                                ,'PO_RELEASES');
            END;

            IF ec_debug.g_debug_level = 3
            THEN
                ec_debug.pl (3, 'l_release_id: ', l_release_id);
            END IF;

            -- Perform this update if this is a Release PO
            xprogress := 'POOB-30-1030';

            UPDATE po_releases
               SET last_update_date = SYSDATE
                  ,printed_date = SYSDATE
                  ,print_count = NVL (print_count, 0) + 1
                  ,edi_processed_flag = 'Y'
             WHERE po_release_id = l_release_id;

            IF SQL%NOTFOUND
            THEN
                ec_debug.pl (0
                            ,'EC'
                            ,'ECE_NO_ROW_UPDATED'
                            ,'PROGRESS_LEVEL'
                            ,xprogress
                            ,'INFO'
                            ,'EDI PROCESSED'
                            ,'TABLE_NAME'
                            ,'PO_RELEASES');
            END IF;

            -- Perform the same update for the Archive Table.
            xprogress := 'POOB-30-1040';

            UPDATE po_releases_archive
               SET last_update_date = SYSDATE
                  ,printed_date = SYSDATE
                  ,print_count = NVL (print_count, 0) + 1
                  ,edi_processed_flag = 'Y'
             WHERE po_release_id = l_release_id AND latest_external_flag = 'Y';

            IF SQL%NOTFOUND
            THEN
                ec_debug.pl (0
                            ,'EC'
                            ,'ECE_NO_ROW_UPDATED'
                            ,'PROGRESS_LEVEL'
                            ,xprogress
                            ,'INFO'
                            ,'EDI PROCESSED'
                            ,'TABLE_NAME'
                            ,'PO_RELEASES_ARCHIVE');
            END IF;
        END IF;

        -- Perform archiving by calling the archive proceedure.
        xprogress := 'POOB-30-1050';

        BEGIN
            /* Bug 2396394 Added the document type CONTRACT(NC) in SQL below */
            SELECT DECODE (document_type
                          ,'NS', 'PO'
                          ,'NC', 'PO'
                          ,'CS', 'PO'
                          ,'NB', 'PA'
                          ,'CB', 'PA'
                          ,'NP', 'PO'
                          ,'CP', 'PO'
                          ,'NR', 'RELEASE'
                          ,'CR', 'RELEASE')
                  ,DECODE (document_type
                          ,'NR', DECODE (po_type,  'PLANNED', 'SCHEDULED',  'BLANKET', 'BLANKET')
                          ,'CR', DECODE (po_type,  'PLANNED', 'SCHEDULED',  'BLANKET', 'BLANKET')
                          ,po_type)
              INTO l_document_type, l_document_subtype
              FROM DUAL;
        EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
                ec_debug.pl (0
                            ,'EC'
                            ,'ECE_DECODE_FAILED'
                            ,'PROGRESS_LEVEL'
                            ,xprogress
                            ,'CODE'
                            ,document_type);
        END;

        IF ec_debug.g_debug_level = 3
        THEN
            ec_debug.pl (3, 'l_document_type: ', l_document_type);
            ec_debug.pl (3, 'l_document_subtype: ', l_document_subtype);
        END IF;

        xprogress := 'POOB-30-1060';

        IF document_type NOT IN ('NR', 'CR')
        THEN
            l_document_id := l_header_id;
        ELSE
            l_document_id := l_release_id;
        END IF;

        xprogress := 'POOB-30-1090';
        ece_po_archive_pkg.porarchive (l_document_type
                                      ,l_document_subtype
                                      ,l_document_id
                                      ,'PRINT'
                                      ,l_error_code
                                      ,l_error_buf
                                      ,l_error_stack);

        xprogress := 'POOB-30-1100';

        -- IF l_error_code <> 0 THEN
        --    raise_application_error(-20000,l_error_buf || l_error_stack);
        -- END IF;

        IF ec_debug.g_debug_level = 3
        THEN
            ec_debug.pop ('ECE_POO_TRANSACTION.UPDATE_PO');
        END IF;
    EXCEPTION
        WHEN OTHERS
        THEN
            ec_debug.pl (0
                        ,'EC'
                        ,'ECE_PROGRAM_ERROR'
                        ,'PROGRESS_LEVEL'
                        ,xprogress);
            ec_debug.pl (0
                        ,'EC'
                        ,'ECE_ERROR_MESSAGE'
                        ,'ERROR_MESSAGE'
                        ,SQLERRM);

            app_exception.raise_exception;
    END update_po;

    PROCEDURE populate_distribution_info (ccommunication_method     IN            VARCHAR2
                                         ,ctransaction_type         IN            VARCHAR2
                                         ,irun_id                   IN            INTEGER
                                         ,cdistribution_interface   IN            VARCHAR2
                                         ,l_key_tbl                 IN OUT NOCOPY ece_flatfile_pvt.interface_tbl_type
                                         ,cpo_header_id             IN            NUMBER
                                         ,cpo_release_id            IN            NUMBER
                                         ,cpo_line_id               IN            NUMBER
                                         ,cpo_line_location_id      IN            NUMBER
                                         ,cfile_common_key          IN            VARCHAR2)
    IS
        xprogress                   VARCHAR2 (80);
        v_levelprocessed            VARCHAR (40);

        /* Bug 2490109
          l_project_tbl              ece_flatfile_pvt.Interface_tbl_type;
          project_sel_c              INTEGER;
        */
        v_project_view_name         VARCHAR2 (120) := 'ECE_PO_DISTRIBUTIONS_V';                           -- Bug 1891291

        cproject_select             VARCHAR2 (32000);
        cproject_from               VARCHAR2 (32000);
        cproject_where              VARCHAR2 (32000);

        iproject_output_level       NUMBER := 14;
        iproject_count              NUMBER := 0;
        --iKey_count                 NUMBER := 0;

        l_project_fkey              NUMBER;

        dummy                       INTEGER;

        init_msg_list               VARCHAR2 (20);
        simulate                    VARCHAR2 (20);
        validation_level            VARCHAR2 (20);
        commt                       VARCHAR2 (20);
        return_status               VARCHAR2 (20);
        msg_count                   NUMBER;
        msg_data                    VARCHAR2 (2000);                                                           --3650215
        v_project_acct_installed    BOOLEAN;
        v_project_acct_short_name   VARCHAR2 (2) := 'PA';
        v_project_acct_status       VARCHAR2 (120);
        v_project_acct_industry     VARCHAR2 (120);
        v_project_acct_schema       VARCHAR2 (120);
        c_project_number            VARCHAR2 (25);
        c_project_type              VARCHAR2 (20);
        c_task_number               VARCHAR2 (25);
        c_task_id                   NUMBER;                                                                --bug 1891291
        c_project_id                NUMBER;                                                                --bug 1891291
        ntask_id_pos                NUMBER;                                                                   -- 2823215
        nproject_id_pos             NUMBER;
        nproject_num_pos            NUMBER;
        nproject_type_pos           NUMBER;
        ntask_num_pos               NUMBER;                                                                   -- 2823215
        nconv_dt_pos                PLS_INTEGER;
        nconv_tz_pos                PLS_INTEGER;
        nconv_off_pos               PLS_INTEGER;
    BEGIN
        IF ec_debug.g_debug_level = 3
        THEN
            ec_debug.push ('ECE_POO_TRANSACTION.POPULATE_PROJECT_INFO');
            ec_debug.pl (3, 'cCommunication_Method: ', ccommunication_method);
            ec_debug.pl (3, 'cTransaction_Type: ', ctransaction_type);
            ec_debug.pl (3, 'iRun_id: ', irun_id);
            ec_debug.pl (3, 'cDistribution_Interface: ', cdistribution_interface);
            ec_debug.pl (3, 'cPO_Header_ID: ', cpo_header_id);
            ec_debug.pl (3, 'cPO_Release_ID: ', cpo_release_id);
            ec_debug.pl (3, 'cPO_Line_ID: ', cpo_line_id);
            ec_debug.pl (3, 'cPO_Line_Location_ID: ', cpo_line_location_id);
        END IF;

        -- Initialize the PL/SQL Table

        v_project_acct_installed :=
            fnd_installation.get_app_info (v_project_acct_short_name
                                          ,                                                                 -- i.e. 'PA'
                                           v_project_acct_status
                                          ,                                                  -- 'I' means it's installed
                                           v_project_acct_industry
                                          ,v_project_acct_schema);

        v_project_acct_status := NVL (v_project_acct_status, 'X');

        IF ece_poo_transaction.project_sel_c = 0
        THEN                                                                                               --Bug 2490109
            ece_poo_transaction.project_sel_c := -911;
        END IF;

        IF ece_poo_transaction.project_sel_c < 0
        THEN                                                                                               --Bug 2490109
            xprogress := 'POOB-40-1000';
            ikey_count := l_key_tbl.COUNT;

            IF ec_debug.g_debug_level = 3
            THEN
                ec_debug.pl (3, 'iKey_count: ', ikey_count);
            END IF;

            ece_flatfile_pvt.init_table (ctransaction_type
                                        ,cdistribution_interface
                                        ,iproject_output_level
                                        ,TRUE
                                        ,l_project_tbl
                                        ,l_key_tbl);

            xprogress := 'POOB-40-1010';
            ece_extract_utils_pub.select_clause (ctransaction_type
                                                ,ccommunication_method
                                                ,cdistribution_interface
                                                ,l_project_tbl
                                                ,cproject_select
                                                ,cproject_from
                                                ,cproject_where);

            -- Build the WHERE Clause
            xprogress := 'POOB-40-1020';
            cproject_where := cproject_where || 'po_header_id = :po_header_id';

            cproject_where := cproject_where || ' AND ' || 'nvl(po_release_id,0) = :po_release_id';

            cproject_where := cproject_where || ' AND ' || 'po_line_id = :po_line_id';

            cproject_where := cproject_where || ' AND ' || 'line_location_id = :line_location_id';

            xprogress := 'POOB-40-1030';
            cproject_where := cproject_where || ' ORDER BY distribution_num';

            -- Combine the SELECT, FROM and WHERE Clauses
            xprogress := 'POOB-40-1040';
            cproject_select := cproject_select || cproject_from || cproject_where;

            IF ec_debug.g_debug_level = 3
            THEN
                ec_debug.pl (3, 'cProject_select: ', cproject_select);
            END IF;

            -- Open the Cursor
            xprogress := 'POOB-40-1050';
            project_sel_c := DBMS_SQL.open_cursor;

            -- Parse the Cursor
            xprogress := 'POOB-40-1060';

            BEGIN
                DBMS_SQL.parse (project_sel_c, cproject_select, DBMS_SQL.native);
            EXCEPTION
                WHEN OTHERS
                THEN
                    ece_error_handling_pvt.print_parse_error (DBMS_SQL.last_error_position, cproject_select);
                    app_exception.raise_exception;
            END;

            -- Set Counter
            xprogress := 'POOB-40-1070';
            iproject_count := l_project_tbl.COUNT;
            ec_debug.pl (3, 'iProject_count: ', iproject_count);

            xprogress := 'POOB-40-1080';
            --iKey_count := l_key_tbl.COUNT;
            --ec_debug.pl(3,'iKey_count: ',iKey_count);

            -- Define Column Types
            xprogress := 'POOB-40-1090';
            ece_flatfile_pvt.define_interface_column_type (project_sel_c
                                                          ,cproject_select
                                                          ,ece_extract_utils_pub.g_maxcolwidth
                                                          ,l_project_tbl);
        -- Project Level Positions

        END IF;                                                                                            --Bug 2490109

        --2823215
        ece_flatfile_pvt.find_pos (l_project_tbl, 'TASK_ID', ntask_id_pos);
        ece_flatfile_pvt.find_pos (l_project_tbl, 'PROJECT_ID', nproject_id_pos);
        ece_flatfile_pvt.find_pos (l_project_tbl, 'PROJECT_NUMBER', nproject_num_pos);
        ece_flatfile_pvt.find_pos (l_project_tbl, 'PROJECT_TYPE', nproject_type_pos);
        ece_flatfile_pvt.find_pos (l_project_tbl, 'TASK_NUMBER', ntask_num_pos);

        --2823215
        ece_flatfile_pvt.find_pos (l_project_tbl, 'CONVERSION_DATE', nconv_dt_pos);
        ece_flatfile_pvt.find_pos (l_project_tbl, 'CONVERSION_DT_TZ_CODE', nconv_tz_pos);
        ece_flatfile_pvt.find_pos (l_project_tbl, 'CONVERSION_DT_OFF', nconv_off_pos);

        IF ece_poo_transaction.project_sel_c > 0
        THEN                                                                                               --Bug 2490109
            -- Bind Variables
            xprogress := 'POOB-40-1140';
            DBMS_SQL.bind_variable (project_sel_c, ':po_header_id', NVL (cpo_header_id, 0));

            xprogress := 'POOB-40-1150';
            DBMS_SQL.bind_variable (project_sel_c, ':po_release_id', NVL (cpo_release_id, 0));

            xprogress := 'POOB-40-1160';
            DBMS_SQL.bind_variable (project_sel_c, ':po_line_id', NVL (cpo_line_id, 0));

            xprogress := 'POOB-40-1170';
            DBMS_SQL.bind_variable (project_sel_c, ':line_location_id', NVL (cpo_line_location_id, 0));

            -- Execute the Cursor
            xprogress := 'POOB-40-1180';
            dummy := DBMS_SQL.execute (project_sel_c);

            -- Fetch Data
            xprogress := 'POOB-40-1190';

            WHILE DBMS_SQL.fetch_rows (project_sel_c) > 0
            LOOP                                                                                   -- Project Level Loop
                -- Store Internal Values in the PL/SQL Table
                xprogress := 'POOB-40-1200';
                ece_flatfile_pvt.assign_column_value_to_tbl (project_sel_c
                                                            ,ikey_count
                                                            ,l_project_tbl
                                                            ,l_key_tbl);

                xprogress := 'POOB-TZ-4500';

                ece_timezone_api.get_server_timezone_details (
                    TO_DATE (l_project_tbl (nconv_dt_pos).VALUE, 'YYYYMMDD HH24MISS')
                   ,l_project_tbl (nconv_off_pos).VALUE
                   ,l_project_tbl (nconv_tz_pos).VALUE);

                -- Convert Internal to External Values
                xprogress := 'POOB-40-1210';
                ec_code_conversion_pvt.populate_plsql_tbl_with_extval (p_api_version_number   => 1.0
                                                                      ,p_init_msg_list        => init_msg_list
                                                                      ,p_simulate             => simulate
                                                                      ,p_commit               => commt
                                                                      ,p_validation_level     => validation_level
                                                                      ,p_return_status        => return_status
                                                                      ,p_msg_count            => msg_count
                                                                      ,p_msg_data             => msg_data
                                                                      ,p_key_tbl              => l_key_tbl
                                                                      ,p_tbl                  => l_project_tbl);

                -- Get Project FKEY
                xprogress := 'POOB-40-1220';

                BEGIN
                    SELECT ece_po_project_info_s.NEXTVAL INTO l_project_fkey FROM DUAL;
                EXCEPTION
                    WHEN NO_DATA_FOUND
                    THEN
                        ec_debug.pl (0
                                    ,'EC'
                                    ,'ECE_GET_NEXT_SEQ_FAILED'
                                    ,'PROGRESS_LEVEL'
                                    ,xprogress
                                    ,'SEQ'
                                    ,'ECE_PO_PROJECT_INFO_S');
                END;

                IF ec_debug.g_debug_level = 3
                THEN
                    ec_debug.pl (3, 'l_project_fkey: ', l_project_fkey);
                END IF;

                -- Insert into Interface Table
                /*           xProgress := 'POOB-40-1230';
                           ece_extract_utils_pub.insert_into_interface_tbl(
                              iRun_id,
                              cTransaction_Type,
                              cCommunication_Method,
                              cDistribution_Interface,
                              l_project_tbl,
                              l_project_fkey);  */

                -- Bug 1891291 begin
                /* Update ECE_PO_DISTRIBUTIONS
                   with project related data
                   based on task_id and project_id
                */

                xprogress := 'POOB-40-1240';

                IF ctransaction_type = 'POO'
                THEN
                    ece_poo_x.populate_ext_project (l_project_fkey, l_project_tbl);
                ELSIF ctransaction_type = 'POCO'
                THEN
                    ece_poco_x.populate_ext_project (l_project_fkey, l_project_tbl);
                END IF;

                IF v_project_acct_status = 'I'
                THEN
                    BEGIN
                        c_task_id := l_project_tbl (ntask_id_pos).VALUE;
                        c_project_id := l_project_tbl (nproject_id_pos).VALUE;

                        /* select task_id,project_id into
                        c_task_id,c_project_id
                        from
                        ECE_PO_DISTRIBUTIONS EPID
                        where
                        EPID.TRANSACTION_RECORD_ID = l_project_fkey; */

                        IF (c_task_id IS NOT NULL AND c_project_id IS NOT NULL)
                        THEN
                            SELECT ppe.project_number, ppe.project_type, pat.task_number
                              INTO c_project_number, c_project_type, c_task_number
                              FROM pa_projects_expend_v ppe, pa_tasks pat, ece_po_distributions epid
                             WHERE     epid.task_id = pat.task_id(+)
                                   AND epid.project_id = pat.project_id(+)
                                   AND epid.project_id = ppe.project_id(+)
                                   AND epid.transaction_record_id = l_project_fkey;

                            --2823215
                            l_project_tbl (nproject_num_pos).VALUE := c_project_number;
                            l_project_tbl (nproject_type_pos).VALUE := c_project_type;
                            l_project_tbl (ntask_num_pos).VALUE := c_task_number;
                        --2823215

                        /* UPDATE ECE_PO_DISTRIBUTIONS EPID
                        SET  EPID.PROJECT_NUMBER = c_PROJECT_NUMBER,
                             EPID.PROJECT_TYPE =   c_PROJECT_TYPE,
                             EPID.TASK_NUMBER = c_TASK_NUMBER
                        WHERE EPID.TRANSACTION_RECORD_ID = l_project_fkey; */

                        END IF;
                    EXCEPTION
                        WHEN NO_DATA_FOUND
                        THEN
                            NULL;
                        WHEN OTHERS
                        THEN
                            NULL;
                    END;
                END IF;

                -- 2823215
                ece_poo_transaction.write_to_file (ctransaction_type
                                                  ,ccommunication_method
                                                  ,cdistribution_interface
                                                  ,l_project_tbl
                                                  ,ioutput_width
                                                  ,irun_id
                                                  ,cfile_common_key
                                                  ,l_project_fkey);
            -- 2823215
            -- Bug 1891291 end
            -- Call Custom Project Stub Depending on Transaction
            /*            xProgress := 'POOB-40-1240';
                        IF cTransaction_Type = 'POO' THEN
                           ece_poo_x.populate_ext_project(l_project_fkey,l_project_tbl);
                        ELSIF cTransaction_Type = 'POCO' THEN
                           ece_poco_x.populate_ext_project(l_project_fkey,l_project_tbl);
                        END IF;   */

            END LOOP;

            -- Check to see if anything was processed
            xprogress := 'POOB-40-1250';

            IF (DBMS_SQL.last_row_count = 0)
            THEN
                v_levelprocessed := 'PROJECT';
                ec_debug.pl (0
                            ,'EC'
                            ,'ECE_NO_DB_ROW_PROCESSED'
                            ,'LEVEL_PROCESSED'
                            ,v_levelprocessed
                            ,'PROGRESS_LEVEL'
                            ,xprogress
                            ,'TRANSACTION_TYPE'
                            ,ctransaction_type);
            END IF;
        END IF;                                                                                            --Bug 2490109

        -- Close the Cursor Bug 2490109
        -- xProgress := 'POOB-40-1260';
        -- dbms_sql.close_cursor(project_sel_c);

        -- Tell Debug that this Procedure is Done
        IF ec_debug.g_debug_level = 3
        THEN
            ec_debug.pop ('ECE_POO_TRANSACTION.POPULATE_PROJECT_INFO');
        END IF;
    EXCEPTION
        WHEN OTHERS
        THEN
            ec_debug.pl (0
                        ,'EC'
                        ,'ECE_PROGRAM_ERROR'
                        ,'PROGRESS_LEVEL'
                        ,xprogress);
            ec_debug.pl (0
                        ,'EC'
                        ,'ECE_ERROR_MESSAGE'
                        ,'ERROR_MESSAGE'
                        ,SQLERRM);

            app_exception.raise_exception;
    END populate_distribution_info;

    PROCEDURE put_distdata_to_out_tbl (ccommunication_method     IN VARCHAR2
                                      ,ctransaction_type         IN VARCHAR2
                                      ,ioutput_width             IN INTEGER
                                      ,irun_id                   IN INTEGER
                                      ,cdistribution_interface   IN VARCHAR2
                                      ,cpo_header_id             IN NUMBER
                                      ,cpo_release_id            IN NUMBER
                                      ,cpo_line_id               IN NUMBER
                                      ,cpo_line_location_id      IN NUMBER
                                      ,cfile_common_key          IN VARCHAR2)
    IS
        xprogress                   VARCHAR2 (80);
        v_levelprocessed            VARCHAR2 (40);
        c_project_common_key_name   VARCHAR2 (40);

        nproject_key_pos            NUMBER;

        /* Bug 2490109
         l_project_tbl              ece_flatfile_pvt.Interface_tbl_type;
         project_sel_c              INTEGER;
         project_del_c1             INTEGER;
         project_del_c2             INTEGER;
        */

        cproject_select             VARCHAR2 (32000);
        cproject_from               VARCHAR2 (32000);
        cproject_where              VARCHAR2 (32000);

        cproject_delete1            VARCHAR2 (32000);
        cproject_delete2            VARCHAR2 (32000);

        iproject_count              NUMBER;

        rproject_rowid              ROWID;
        rproject_x_rowid            ROWID;

        cproject_x_interface        VARCHAR2 (50);

        iproject_output_level       NUMBER := 14;
        iproject_start_num          INTEGER;
        dummy                       INTEGER;
    BEGIN
        IF ec_debug.g_debug_level >= 2
        THEN
            ec_debug.push ('ECE_POO_TRANSACTION.PUT_PROJECT_DATA_TO_OUTPUT_TBL');
            ec_debug.pl (3, 'cCommunication_Method: ', ccommunication_method);
            ec_debug.pl (3, 'cTransaction_Type: ', ctransaction_type);
            ec_debug.pl (3, 'iOutput_width: ', ioutput_width);
            ec_debug.pl (3, 'iRun_id: ', irun_id);
            ec_debug.pl (3, 'cDistribution_Interface: ', cdistribution_interface);
            ec_debug.pl (3, 'cPO_Header_ID: ', cpo_header_id);
            ec_debug.pl (3, 'cPO_Release_ID: ', cpo_release_id);
            ec_debug.pl (3, 'cPO_Line_ID: ', cpo_line_id);
            ec_debug.pl (3, 'cPO_Line_Location_ID: ', cpo_line_location_id);
            ec_debug.pl (3, 'cFile_Common_Key: ', cfile_common_key);
        END IF;

        IF ece_poo_transaction.project_sel_c = 0
        THEN                                                                                              -- Bug 2490109
            ece_poo_transaction.project_sel_c := -911;
        END IF;

        IF ece_poo_transaction.project_sel_c < 0
        THEN                                                                                              -- Bug 2490109
            ece_poo_transaction.l_project_tbl.delete;                                                     -- Bug 2490109

            -- Build the SELECT, FROM, and WHERE Clauses
            xprogress := 'POOB-50-1000';
            ece_flatfile_pvt.select_clause (ctransaction_type
                                           ,ccommunication_method
                                           ,cdistribution_interface
                                           ,cproject_x_interface
                                           ,l_project_tbl
                                           ,c_project_common_key_name
                                           ,cproject_select
                                           ,cproject_from
                                           ,cproject_where
                                           ,iproject_output_level);

            -- Customize the WHERE Clause
            xprogress := 'POOB-50-1010';
            cproject_where := cproject_where || ' AND ' || cdistribution_interface || '.run_id = :run_id';

            cproject_where :=
                cproject_where || ' AND NVL(' || cdistribution_interface || '.po_header_id,0) = :po_header_id';

            cproject_where :=
                cproject_where || ' AND NVL(' || cdistribution_interface || '.po_release_id,0) = :po_release_id';

            cproject_where := cproject_where || ' AND NVL(' || cdistribution_interface || '.po_line_id,0) = :po_line_id';

            cproject_where :=
                   cproject_where
                || ' AND NVL('
                || cdistribution_interface
                || '.line_location_id,0) = :po_line_location_id';

            cproject_where := cproject_where || ' ORDER BY ' || cdistribution_interface || '.distribution_num';

            -- Customize the SELECT Clause
            cproject_select :=
                   cproject_select
                || ','
                || cdistribution_interface
                || '.rowid'
                || ','
                || cproject_x_interface
                || '.rowid ';

            -- Build the Complete SQL Statement
            cproject_select := cproject_select || cproject_from || cproject_where || ' FOR UPDATE';
            ec_debug.pl (3, 'cProject_select: ', cproject_select);

            -- Build First DELETE SQL Statement
            cproject_delete1 := 'DELETE FROM ' || cdistribution_interface || ' WHERE rowid = :col_rowid';

            -- Build Second DELETE SQL Statement
            cproject_delete2 := 'DELETE FROM ' || cproject_x_interface || ' WHERE rowid = :col_rowid';

            IF ec_debug.g_debug_level = 3
            THEN
                ec_debug.pl (3, 'cProject_delete1: ', cproject_delete1);
                ec_debug.pl (3, 'cProject_delete2: ', cproject_delete2);
            END IF;

            -- Open the Cursors
            xprogress := 'POOB-50-1020';
            project_sel_c := DBMS_SQL.open_cursor;

            xprogress := 'POOB-50-1030';
            project_del_c1 := DBMS_SQL.open_cursor;

            xprogress := 'POOB-50-1040';
            project_del_c2 := DBMS_SQL.open_cursor;

            -- Parse the SQL Statements
            xprogress := 'POOB-50-1050';

            BEGIN
                xprogress := 'POOB-50-1060';
                DBMS_SQL.parse (project_sel_c, cproject_select, DBMS_SQL.native);
            EXCEPTION
                WHEN OTHERS
                THEN
                    ece_error_handling_pvt.print_parse_error (DBMS_SQL.last_error_position, cproject_select);
                    app_exception.raise_exception;
            END;

            xprogress := 'POOB-50-1070';

            BEGIN
                xprogress := 'POOB-50-1080';
                DBMS_SQL.parse (project_del_c1, cproject_delete1, DBMS_SQL.native);
            EXCEPTION
                WHEN OTHERS
                THEN
                    ece_error_handling_pvt.print_parse_error (DBMS_SQL.last_error_position, cproject_delete1);
                    app_exception.raise_exception;
            END;

            xprogress := 'POOB-50-1090';

            BEGIN
                xprogress := 'POOB-50-1100';
                DBMS_SQL.parse (project_del_c2, cproject_delete2, DBMS_SQL.native);
            EXCEPTION
                WHEN OTHERS
                THEN
                    ece_error_handling_pvt.print_parse_error (DBMS_SQL.last_error_position, cproject_delete2);
                    app_exception.raise_exception;
            END;
        END IF;                                                                                            --Bug 2490109

        IF ece_poo_transaction.project_sel_c > 0
        THEN                                                                                              -- Bug 2490109
            -- Set the Counter Variables
            xprogress := 'POOB-50-1110';
            iproject_count := l_project_tbl.COUNT;

            IF ec_debug.g_debug_level = 3
            THEN
                ec_debug.pl (3, 'iProject_count: ', iproject_count);
            END IF;

            -- Define Column Types
            xprogress := 'POOB-50-1120';
            ece_flatfile_pvt.define_interface_column_type (project_sel_c
                                                          ,cproject_select
                                                          ,ece_flatfile_pvt.g_maxcolwidth
                                                          ,l_project_tbl);

            -- Define Additional Column Types
            xprogress := 'POOB-50-1130';
            DBMS_SQL.define_column_rowid (project_sel_c, iproject_count + 1, rproject_rowid);

            xprogress := 'POOB-50-1140';
            DBMS_SQL.define_column_rowid (project_sel_c, iproject_count + 2, rproject_x_rowid);

            -- Bind Variables
            xprogress := 'POOB-50-1150';
            DBMS_SQL.bind_variable (project_sel_c, ':run_id', irun_id);

            xprogress := 'POOB-50-1160';
            DBMS_SQL.bind_variable (project_sel_c, ':po_header_id', NVL (cpo_header_id, 0));

            xprogress := 'POOB-50-1170';
            DBMS_SQL.bind_variable (project_sel_c, ':po_release_id', NVL (cpo_release_id, 0));

            xprogress := 'POOB-50-1180';
            DBMS_SQL.bind_variable (project_sel_c, ':po_line_id', NVL (cpo_line_id, 0));

            xprogress := 'POOB-50-1190';
            DBMS_SQL.bind_variable (project_sel_c, ':po_line_location_id', NVL (cpo_line_location_id, 0));

            -- Execute the SQL Statement
            xprogress := 'POOB-50-1200';
            dummy := DBMS_SQL.execute (project_sel_c);

            -- Fetch Data
            xprogress := 'POOB-50-1210';

            WHILE DBMS_SQL.fetch_rows (project_sel_c) > 0
            LOOP                                                                                   -- Project Level Loop
                -- Store the Fetched Data in the PL/SQL Table
                xprogress := 'POOB-50-1220';
                ece_flatfile_pvt.assign_column_value_to_tbl (project_sel_c, l_project_tbl);

                xprogress := 'POOB-50-1230';
                DBMS_SQL.COLUMN_VALUE (project_sel_c, iproject_count + 1, rproject_rowid);

                xprogress := 'POOB-50-1240';
                DBMS_SQL.COLUMN_VALUE (project_sel_c, iproject_count + 2, rproject_x_rowid);

                xprogress := 'POOB-50-1250';
                ece_poo_transaction.write_to_file (ctransaction_type
                                                  ,ccommunication_method
                                                  ,cdistribution_interface
                                                  ,l_project_tbl
                                                  ,ioutput_width
                                                  ,irun_id
                                                  ,cfile_common_key
                                                  ,NULL);

                -- Bind the ROWIDs for Deletion
                xprogress := 'POOB-50-1260';
                DBMS_SQL.bind_variable (project_del_c1, ':col_rowid', rproject_rowid);

                xprogress := 'POOB-50-1270';
                DBMS_SQL.bind_variable (project_del_c2, ':col_rowid', rproject_x_rowid);

                -- Execute the First Delete SQL
                xprogress := 'POOB-50-1280';
                dummy := DBMS_SQL.execute (project_del_c1);

                -- Execute the Second Delete SQL
                xprogress := 'POOB-50-1290';
                dummy := DBMS_SQL.execute (project_del_c2);
            END LOOP;                                                                                   -- Project Level

            -- Make a note if zero rows were processed
            xprogress := 'POOB-50-1300';

            IF DBMS_SQL.last_row_count = 0
            THEN
                xprogress := 'POOB-50-1310';
                v_levelprocessed := 'PROJECT';
                ec_debug.pl (0
                            ,'EC'
                            ,'ECE_NO_DB_ROW_PROCESSED'
                            ,'PROGRESS_LEVEL'
                            ,xprogress
                            ,'LEVEL_PROCESSED'
                            ,v_levelprocessed
                            ,'TRANSACTION_TYPE'
                            ,ctransaction_type);
            END IF;
        END IF;                                                                                           -- Bug 2490109

        /*  Bug 2490109
           -- Let's Close the Cursors
           xProgress := 'POOB-50-1320';
           dbms_sql.close_cursor(project_sel_c);

           xProgress := 'POOB-50-1330';
           dbms_sql.close_cursor(project_del_c1);

           xProgress := 'POOB-50-1340';
           dbms_sql.close_cursor(project_del_c2);
        */

        xprogress := 'POOB-50-1350';

        IF ec_debug.g_debug_level >= 2
        THEN
            ec_debug.pop ('ECE_POO_TRANSACTION.PUT_PROJECT_DATA_TO_OUTPUT_TBL');
        END IF;
    EXCEPTION
        WHEN OTHERS
        THEN
            ec_debug.pl (0
                        ,'EC'
                        ,'ECE_PROGRAM_ERROR'
                        ,'PROGRESS_LEVEL'
                        ,xprogress);
            ec_debug.pl (0
                        ,'EC'
                        ,'ECE_ERROR_MESSAGE'
                        ,'ERROR_MESSAGE'
                        ,SQLERRM);

            app_exception.raise_exception;
    END put_distdata_to_out_tbl;

    PROCEDURE populate_text_attachment (ccommunication_method   IN            VARCHAR2
                                       ,ctransaction_type       IN            VARCHAR2
                                       ,irun_id                 IN            INTEGER
                                       ,cheader_output_level    IN            NUMBER
                                       ,cdetail_output_level    IN            NUMBER
                                       ,catt_header_interface   IN            VARCHAR2
                                       ,catt_detail_interface   IN            VARCHAR2
                                       ,centity_name            IN            VARCHAR2
                                       ,cname                   IN            VARCHAR2
                                       ,cpk1_value              IN            VARCHAR2
                                       ,cpk2_value              IN            VARCHAR2
                                       ,cpk3_value              IN            VARCHAR2
                                       ,cpk4_value              IN            VARCHAR2
                                       ,cpk5_value              IN            VARCHAR2
                                       ,csegment_size           IN            NUMBER
                                       ,l_key_tbl               IN OUT NOCOPY ece_flatfile_pvt.interface_tbl_type
                                       ,cfile_common_key        IN            VARCHAR2
                                       ,l_att_header_tbl        IN OUT NOCOPY ece_flatfile_pvt.interface_tbl_type
                                       ,l_att_detail_tbl        IN OUT NOCOPY ece_flatfile_pvt.interface_tbl_type
                                       ,l_key_count             IN OUT NOCOPY NUMBER)
    IS
        xprogress             VARCHAR2 (80);

        --l_att_header_tbl     ece_flatfile_pvt.Interface_tbl_type;

        v_att_header_select   VARCHAR2 (32000);
        v_att_header_from     VARCHAR2 (32000);
        v_att_header_where    VARCHAR2 (32000);
        v_att_view_name       VARCHAR2 (120) := 'ECE_ATTACHMENT_V';

        n_att_header_sel_c    INTEGER;
        n_dummy               INTEGER;
        n_header_count        NUMBER := 0;
        n_header_fkey         NUMBER;

        n_datatype_id_pos     NUMBER;
        n_att_seq_num_pos     NUMBER;
        n_att_doc_id_pos      NUMBER;                                                                      --Bug 2187958
        n_att_hdr_count       NUMBER;
    BEGIN
        IF ec_debug.g_debug_level >= 2
        THEN
            ec_debug.push ('ECE_POO_TRANSACTION.POPULATE_TEXT_ATTACHMENT');
            ec_debug.pl (3, 'cEntity_Name: ', centity_name);
            ec_debug.pl (3, 'cName: ', cname);
            ec_debug.pl (3, 'cPK1_Value: ', cpk1_value);
            ec_debug.pl (3, 'cPK2_Value: ', cpk2_value);
            ec_debug.pl (3, 'cPK3_Value: ', cpk3_value);
            ec_debug.pl (3, 'cPK4_Value: ', cpk4_value);
            ec_debug.pl (3, 'cPK5_Value: ', cpk5_value);
            ec_debug.pl (3, 'cSegment_Size: ', csegment_size);
        END IF;

        xprogress := 'POOB-60-1000';

        IF (l_att_header_tbl.COUNT = 0)
        THEN
            l_key_count := l_key_tbl.COUNT;
            ece_flatfile_pvt.init_table (ctransaction_type
                                        ,catt_header_interface
                                        ,cheader_output_level
                                        ,TRUE
                                        ,l_att_header_tbl
                                        ,l_key_tbl);
        END IF;

        IF ec_debug.g_debug_level >= 3
        THEN
            ec_debug.pl (3, 'l_key_count: ', l_key_count);
        END IF;

        xprogress := 'POOB-60-1005';
        -- Build the SELECT Clause.
        ece_extract_utils_pub.select_clause (ctransaction_type
                                            ,ccommunication_method
                                            ,catt_header_interface
                                            ,l_att_header_tbl
                                            ,v_att_header_select
                                            ,v_att_header_from
                                            ,v_att_header_where);

        xprogress := 'POOB-60-1010';
        -- Build the WHERE and the ORDER BY Clause.
        -- Entity Name must not be NULL.
        v_att_header_where := v_att_header_where || v_att_view_name || '.entity_name = :cEntity_Name';

        xprogress := 'POOB-60-1020';
        -- Name must not be NULL.
        v_att_header_where :=
            v_att_header_where || ' AND UPPER(' || v_att_view_name || '.category_name) = UPPER(:cName)';

        xprogress := 'POOB-60-1030';
        -- cPK1 Value must not be NULL.
        v_att_header_where := v_att_header_where || ' AND ' || v_att_view_name || '.pk1_value = :cPK1_Value';

        xprogress := 'POOB-60-1040';

        /*         IF cPK2_Value IS NOT NULL THEN
                    v_att_header_where := v_att_header_where || ' AND ' ||
                                          v_att_view_name    || '.pk2_value = :cPK2_Value';
                 ELSE  -- cPK2_Value IS NULL.
                    v_att_header_where := v_att_header_where || ' AND ' ||
                                          v_att_view_name    || '.pk2_value IS NULL';
                 END IF; */
        -- BUG:5367903

        IF cpk2_value IS NULL
        THEN
            v_att_header_where := v_att_header_where || ' AND ' || v_att_view_name || '.pk2_value IS NULL';
        ELSE
            IF cpk2_value <> c_any_value
            THEN
                v_att_header_where := v_att_header_where || ' AND ' || v_att_view_name || '.pk2_value = :cPK2_Value';
            END IF;
        END IF;

        xprogress := 'POOB-60-1050';

        IF centity_name <> 'MTL_SYSTEM_ITEMS'
        THEN                                                                                                  -- 3550723
            /*           IF cPK3_Value IS NOT NULL THEN
                        v_att_header_where := v_att_header_where || ' AND ' ||
                                              v_att_view_name    || '.pk3_value = :cPK3_Value';
                       ELSE  -- cPK3_Value IS NULL.
                        v_att_header_where := v_att_header_where || ' AND ' ||
                                              v_att_view_name    || '.pk3_value IS NULL';
                       END IF;*/
            -- BUG:5367903

            IF cpk3_value IS NULL
            THEN
                v_att_header_where := v_att_header_where || ' AND ' || v_att_view_name || '.pk3_value IS NULL';
            ELSE
                IF cpk3_value <> c_any_value
                THEN
                    v_att_header_where := v_att_header_where || ' AND ' || v_att_view_name || '.pk3_value = :cPK3_Value';
                END IF;
            END IF;

            xprogress := 'POOB-60-1060';

            /* IF cPK4_Value IS NOT NULL THEN
             v_att_header_where := v_att_header_where || ' AND ' ||
                                   v_att_view_name    || '.pk4_value = :cPK4_Value';
            ELSE  -- cPK4_Value IS NULL.
             v_att_header_where := v_att_header_where || ' AND ' ||
                                   v_att_view_name    || '.pk4_value IS NULL';
            END IF;*/
            -- BUG:5367903

            IF cpk4_value IS NULL
            THEN
                v_att_header_where := v_att_header_where || ' AND ' || v_att_view_name || '.pk4_value IS NULL';
            ELSE
                IF cpk4_value <> c_any_value
                THEN
                    v_att_header_where := v_att_header_where || ' AND ' || v_att_view_name || '.pk4_value = :cPK4_Value';
                END IF;
            END IF;

            xprogress := 'POOB-60-1070';

            /* IF cPK5_Value IS NOT NULL THEN
             v_att_header_where := v_att_header_where || ' AND ' ||
                                   v_att_view_name    || '.pk5_value = :cPK5_Value';
            ELSE  -- cPK5_Value IS NULL.
             v_att_header_where := v_att_header_where || ' AND ' ||
                                   v_att_view_name    || '.pk5_value IS NULL';
            END IF;*/
            -- BUG:5367903

            IF cpk5_value IS NULL
            THEN
                v_att_header_where := v_att_header_where || ' AND ' || v_att_view_name || '.pk5_value IS NULL';
            ELSE
                IF cpk5_value <> c_any_value
                THEN
                    v_att_header_where := v_att_header_where || ' AND ' || v_att_view_name || '.pk5_value = :cPK5_Value';
                END IF;
            END IF;
        END IF;

        xprogress := 'POOB-60-1080';
        /*v_att_header_where := v_att_header_where || ' ORDER BY ' ||
                              v_att_view_name    || '.att_seq_num';*/

        --Bug 2187958
        v_att_header_where := v_att_header_where || ' ORDER BY ' || v_att_view_name || '.attached_document_id';

        xprogress := 'POOB-60-1090';
        -- Now we put all the clauses together.
        v_att_header_select := v_att_header_select || v_att_header_from || v_att_header_where;

        IF ec_debug.g_debug_level = 3
        THEN
            ec_debug.pl (3, 'v_att_header_select: ', v_att_header_select);
        END IF;

        xprogress := 'POOB-60-1092';
        ece_extract_utils_pub.find_pos (l_att_header_tbl, 'DATATYPE_ID', n_datatype_id_pos);

        xprogress := 'POOB-60-1094';
        ece_extract_utils_pub.find_pos (l_att_header_tbl, 'ATT_SEQ_NUM', n_att_seq_num_pos);

        IF ec_debug.g_debug_level = 3
        THEN
            ec_debug.pl (3, 'n_datatype_id_pos: ', n_datatype_id_pos);
            ec_debug.pl (3, 'n_att_seq_num_pos: ', n_att_seq_num_pos);
        END IF;

        xprogress := 'POOB-60-1096';
        ece_extract_utils_pub.find_pos (l_att_header_tbl, 'ATTACHED_DOCUMENT_ID', n_att_doc_id_pos);       --Bug 2187958

        IF ec_debug.g_debug_level = 3
        THEN
            ec_debug.pl (3, 'n_att_doc_id_pos: ', n_att_doc_id_pos);
        END IF;

        -- Open Cursor.
        xprogress := 'POOB-60-1100';
        n_att_header_sel_c := DBMS_SQL.open_cursor;

        -- Parse Cursor.
        xprogress := 'POOB-60-1110';

        BEGIN
            DBMS_SQL.parse (n_att_header_sel_c, v_att_header_select, DBMS_SQL.native);
        EXCEPTION
            WHEN OTHERS
            THEN
                ece_error_handling_pvt.print_parse_error (DBMS_SQL.last_error_position, v_att_header_select);
                app_exception.raise_exception;
        END;

        -- Set Counter
        xprogress := 'POOB-60-1120';
        n_header_count := l_att_header_tbl.COUNT;

        IF ec_debug.g_debug_level = 3
        THEN
            ec_debug.pl (3, 'n_header_count: ', n_header_count);
        END IF;

        xprogress := 'POOB-60-1130';
        ece_flatfile_pvt.define_interface_column_type (n_att_header_sel_c
                                                      ,v_att_header_select
                                                      ,ece_extract_utils_pub.g_maxcolwidth
                                                      ,l_att_header_tbl);

        -- Bind Variables
        xprogress := 'POOB-60-1132';
        DBMS_SQL.bind_variable (n_att_header_sel_c, ':cEntity_Name', centity_name);

        xprogress := 'POOB-60-1133';
        DBMS_SQL.bind_variable (n_att_header_sel_c, ':cName', cname);

        xprogress := 'POOB-60-1134';
        DBMS_SQL.bind_variable (n_att_header_sel_c, ':cPK1_Value', cpk1_value);

        IF cpk2_value IS NOT NULL AND cpk2_value <> c_any_value
        THEN
            xprogress := 'POOB-60-1135';
            DBMS_SQL.bind_variable (n_att_header_sel_c, ':cPK2_Value', cpk2_value);
        END IF;

        IF centity_name <> 'MTL_SYSTEM_ITEMS'
        THEN                                                                                                  -- 3550723
            /* IF cPK3_Value IS NOT NULL THEN */
                                                                                                          -- BUG:5367903
            IF cpk3_value IS NOT NULL AND cpk3_value <> c_any_value
            THEN
                xprogress := 'POOB-60-1136';
                DBMS_SQL.bind_variable (n_att_header_sel_c, ':cPK3_Value', cpk3_value);
            END IF;

            /* IF cPK4_Value IS NOT NULL THEN */
                                                                                                          -- BUG:5367903
            IF cpk4_value IS NOT NULL AND cpk4_value <> c_any_value
            THEN
                xprogress := 'POOB-60-1137';
                DBMS_SQL.bind_variable (n_att_header_sel_c, ':cPK4_Value', cpk4_value);
            END IF;

            /* IF cPK5_Value IS NOT NULL THEN */
                                                                                                          -- BUG:5367903
            IF cpk5_value IS NOT NULL AND cpk5_value <> c_any_value
            THEN
                xprogress := 'POOB-60-1138';
                DBMS_SQL.bind_variable (n_att_header_sel_c, ':cPK5_Value', cpk5_value);
            END IF;
        END IF;

        -- Execute Cursor
        xprogress := 'POOB-60-1140';
        n_dummy := DBMS_SQL.execute (n_att_header_sel_c);

        xprogress := 'POOB-60-1150';

        WHILE DBMS_SQL.fetch_rows (n_att_header_sel_c) > 0
        LOOP
            xprogress := 'POOB-60-1160';
            ece_flatfile_pvt.assign_column_value_to_tbl (n_att_header_sel_c
                                                        ,l_key_count
                                                        ,l_att_header_tbl
                                                        ,l_key_tbl);

            xprogress := 'POOB-60-1170';
            /* BEGIN
                SELECT   ece_attachment_headers_s.NEXTVAL INTO n_header_fkey
                FROM     DUAL;

             EXCEPTION
                WHEN NO_DATA_FOUND THEN
                   ec_debug.pl(0,'EC','ECE_GET_NEXT_SEQ_FAILED','PROGRESS_LEVEL',xProgress,'SEQ','ECE_ATTACHMENT_HEADERS_S');
             END; */

            xprogress := 'POOB-60-1180';
            /*  ece_extract_utils_pub.insert_into_interface_tbl(iRun_id,
                                                              cTransaction_Type,
                                                              cCommunication_Method,
                                                              cAtt_Header_Interface,
                                                              l_att_header_tbl,
                                                              n_header_fkey); */

            -- 2823215
            ece_poo_transaction.write_to_file (ctransaction_type
                                              ,ccommunication_method
                                              ,catt_header_interface
                                              ,l_att_header_tbl
                                              ,ioutput_width
                                              ,irun_id
                                              ,cfile_common_key
                                              ,n_header_fkey);
            -- 2823215
            xprogress := 'POOB-60-1190';
            -- populate_ext_att_header(n_header_fkey,l_att_header_tbl);

            xprogress := 'POOB-60-1200';
            populate_text_att_detail (ccommunication_method
                                     ,ctransaction_type
                                     ,irun_id
                                     ,cdetail_output_level
                                     ,catt_detail_interface
                                     ,l_att_header_tbl (n_att_seq_num_pos).VALUE
                                     ,centity_name
                                     ,cname
                                     ,cpk1_value
                                     ,cpk2_value
                                     ,cpk3_value
                                     ,cpk4_value
                                     ,cpk5_value
                                     ,l_att_header_tbl (n_datatype_id_pos).VALUE
                                     ,csegment_size
                                     ,l_key_tbl
                                     ,l_att_header_tbl (n_att_doc_id_pos).VALUE
                                     ,                                                                    -- Bug 2187958
                                      cfile_common_key
                                     ,l_att_detail_tbl);
        END LOOP;

        xprogress := 'POOB-60-1210';
        DBMS_SQL.close_cursor (n_att_header_sel_c);

        IF ec_debug.g_debug_level >= 2
        THEN
            ec_debug.pop ('ECE_POO_TRANSACTION.POPULATE_TEXT_ATTACHMENT');
        END IF;
    EXCEPTION
        WHEN OTHERS
        THEN
            ec_debug.pl (0
                        ,'EC'
                        ,'ECE_PROGRAM_ERROR'
                        ,'PROGRESS_LEVEL'
                        ,xprogress);
            ec_debug.pl (0
                        ,'EC'
                        ,'ECE_ERROR_MESSAGE'
                        ,'ERROR_MESSAGE'
                        ,SQLERRM);

            app_exception.raise_exception;
    END populate_text_attachment;

    PROCEDURE populate_text_att_detail (ccommunication_method   IN            VARCHAR2
                                       ,ctransaction_type       IN            VARCHAR2
                                       ,irun_id                 IN            INTEGER
                                       ,cdetail_output_level    IN            NUMBER
                                       ,catt_detail_interface   IN            VARCHAR2
                                       ,catt_seq_num            IN            NUMBER
                                       ,centity_name            IN            VARCHAR2
                                       ,cname                   IN            VARCHAR2
                                       ,cpk1_value              IN            VARCHAR2
                                       ,cpk2_value              IN            VARCHAR2
                                       ,cpk3_value              IN            VARCHAR2
                                       ,cpk4_value              IN            VARCHAR2
                                       ,cpk5_value              IN            VARCHAR2
                                       ,cdata_type_id           IN            NUMBER
                                       ,                                                              -- 1=Short, 2=Long
                                        csegment_size           IN            NUMBER
                                       ,l_key_tbl               IN OUT NOCOPY ece_flatfile_pvt.interface_tbl_type
                                       ,catt_doc_id             IN            NUMBER
                                       ,cfile_common_key        IN            VARCHAR2
                                       ,l_att_detail_tbl        IN OUT NOCOPY ece_flatfile_pvt.interface_tbl_type)
    IS
        xprogress                     VARCHAR2 (80);

        c_local_chr_10                VARCHAR2 (1) := fnd_global.local_chr (10);
        c_local_chr_13                VARCHAR2 (1) := fnd_global.local_chr (13);

        --l_att_detail_tbl              ece_flatfile_pvt.Interface_tbl_type;
        l_short_text                  VARCHAR2 (32000);
        l_data_chunk                  VARCHAR2 (32000);
        l_temp_text                   VARCHAR2 (32000);

        v_att_detail_select           VARCHAR2 (32000) := 'SELECT ';
        v_att_detail_from             VARCHAR2 (32000) := ' FROM ';
        v_att_detail_where            VARCHAR2 (32000) := ' WHERE ';
        v_att_view_name               VARCHAR2 (120) := 'ECE_ATTACHMENT_V';
        v_continue_flag               VARCHAR2 (1);
        v_last_char                   VARCHAR2 (32000);
        v_split_word                  VARCHAR2 (120);

        n_att_detail_sel_c            INTEGER;
        n_dummy                       INTEGER;

        n_cr_pos                      NUMBER;
        n_lf_pos                      NUMBER;
        n_detail_count                NUMBER := 0;
        n_detail_fkey                 NUMBER;
        n_new_chunk_size              NUMBER;
        n_return_size                 NUMBER;
        n_temp_return_size            NUMBER;
        n_segment_number              NUMBER;
        n_short_text_length           NUMBER;                                                             -- Bug 3310412
        n_cur_pos                     NUMBER;
        n_space_pos                   NUMBER;

        n_att_seq_num_pos             NUMBER;
        n_att_doc_id_pos              NUMBER;                                                              --Bug 2187958
        n_entity_name_pos             NUMBER;
        n_name_pos                    NUMBER;
        n_pk1_value_pos               NUMBER;
        n_pk2_value_pos               NUMBER;
        n_pk3_value_pos               NUMBER;
        n_pk4_value_pos               NUMBER;
        n_pk5_value_pos               NUMBER;
        n_seg_num_pos                 NUMBER;
        n_cont_flag_pos               NUMBER;
        n_att_seg_pos                 NUMBER;
        n_run_id_pos                  NUMBER;
        n_transaction_record_id_pos   NUMBER;
        n_pr_pos                      NUMBER;                                                                 -- 3618073
    BEGIN
        IF ec_debug.g_debug_level = 3
        THEN
            ec_debug.push ('ECE_POO_TRANSACTION.POPULATE_TEXT_ATT_DETAIL');
            ec_debug.pl (3, 'cDetail_Output_Level: ', cdetail_output_level);
            ec_debug.pl (3, 'cAtt_Detail_Interface: ', catt_detail_interface);
            ec_debug.pl (3, 'cAtt_Seq_Num: ', catt_seq_num);
            ec_debug.pl (3, 'cEntity_Name: ', centity_name);
            ec_debug.pl (3, 'cName: ', cname);
            ec_debug.pl (3, 'cPK1_Value: ', cpk1_value);
            ec_debug.pl (3, 'cPK2_Value: ', cpk2_value);
            ec_debug.pl (3, 'cPK3_Value: ', cpk3_value);
            ec_debug.pl (3, 'cPK4_Value: ', cpk4_value);
            ec_debug.pl (3, 'cPK5_Value: ', cpk5_value);
            ec_debug.pl (3, 'cData_Type_ID: ', cdata_type_id);
            ec_debug.pl (3, 'cSegment_Size: ', csegment_size);
            ec_debug.pl (3, 'cAtt_doc_id: ', catt_doc_id);
        END IF;

        fnd_profile.get ('ECE_ATT_SPLIT_WORD_ALLOWED', v_split_word);
        v_split_word := NVL (v_split_word, 'Y');
        ec_debug.pl (3, 'v_split_word: ', v_split_word);

        xprogress := 'POOB-70-1000';

        IF (l_att_detail_tbl.COUNT = 0)
        THEN
            ece_flatfile_pvt.init_table (ctransaction_type
                                        ,catt_detail_interface
                                        ,cdetail_output_level
                                        ,TRUE
                                        ,l_att_detail_tbl
                                        ,l_key_tbl);
        END IF;

        xprogress := 'POOB-70-1010';
        ece_flatfile_pvt.find_pos (l_att_detail_tbl, 'ATT_SEQ_NUM', n_att_seq_num_pos);
        ece_flatfile_pvt.find_pos (l_att_detail_tbl, 'ENTITY_NAME', n_entity_name_pos);
        ece_flatfile_pvt.find_pos (l_att_detail_tbl, 'NAME', n_name_pos);
        ece_flatfile_pvt.find_pos (l_att_detail_tbl, 'PK1_VALUE', n_pk1_value_pos);
        ece_flatfile_pvt.find_pos (l_att_detail_tbl, 'PK2_VALUE', n_pk2_value_pos);
        ece_flatfile_pvt.find_pos (l_att_detail_tbl, 'PK3_VALUE', n_pk3_value_pos);
        ece_flatfile_pvt.find_pos (l_att_detail_tbl, 'PK4_VALUE', n_pk4_value_pos);
        ece_flatfile_pvt.find_pos (l_att_detail_tbl, 'PK5_VALUE', n_pk5_value_pos);
        ece_flatfile_pvt.find_pos (l_att_detail_tbl, 'SEGMENT_NUMBER', n_seg_num_pos);
        ece_flatfile_pvt.find_pos (l_att_detail_tbl, 'CONTINUE_FLAG', n_cont_flag_pos);
        ece_flatfile_pvt.find_pos (l_att_detail_tbl, 'ATTACHMENT_SEGMENT', n_att_seg_pos);
        ece_flatfile_pvt.find_pos (l_att_detail_tbl, 'RUN_ID', n_run_id_pos);
        ece_flatfile_pvt.find_pos (l_att_detail_tbl, 'TRANSACTION_RECORD_ID', n_transaction_record_id_pos);
        ece_flatfile_pvt.find_pos (l_att_detail_tbl, 'ATTACHED_DOCUMENT_ID', n_att_doc_id_pos);             --Bug2187958
        xprogress := 'POOB-70-1020';
        -- Build the SELECT Clause.
        ece_extract_utils_pub.select_clause (ctransaction_type
                                            ,ccommunication_method
                                            ,catt_detail_interface
                                            ,l_att_detail_tbl
                                            ,v_att_detail_select
                                            ,v_att_detail_from
                                            ,v_att_detail_where);

        IF cdata_type_id = 1
        THEN                                                                                               -- Short Text
            v_att_detail_select := v_att_detail_select || ',short_text';
        ELSE
            v_att_detail_select := v_att_detail_select || ',long_text';
        END IF;

        xprogress := 'POOB-70-1030';
        -- Build the WHERE and the ORDER BY Clause.
        -- Entity Name must not be NULL.
        v_att_detail_where := v_att_detail_where || v_att_view_name || '.entity_name = :cEntity_name';

        xprogress := 'POOB-70-1040';
        -- Name must not be NULL.
        v_att_detail_where :=
            v_att_detail_where || ' AND UPPER(' || v_att_view_name || '.category_name) = UPPER(:cName)';

        xprogress := 'POOB-70-1045';
        -- Attachment Sequence must not be NULL.
        v_att_detail_where := v_att_detail_where || ' AND ' || v_att_view_name || '.att_seq_num = :cAtt_Seq_Num';

        --Attached document ID must not be null Bug 2187958
        v_att_detail_where := v_att_detail_where || ' AND ' || v_att_view_name || '.attached_document_id = :cAtt_doc_id';

        xprogress := 'POOB-70-1050';
        -- PK1 Value must not be NULL.
        v_att_detail_where := v_att_detail_where || ' AND ' || v_att_view_name || '.pk1_value = :cPK1_Value';

        xprogress := 'POOB-70-1060';

        /* IF cPK2_Value IS NOT NULL THEN
           xProgress := 'POOB-70-1070';
           v_att_detail_where := v_att_detail_where || ' AND ' ||
                                 v_att_view_name    || '.pk2_value = :cPK2_Value';
        ELSE  -- cPK2_Value IS NULL.
           xProgress := 'POOB-70-1080';
           v_att_detail_where := v_att_detail_where || ' AND ' ||
                                 v_att_view_name    || '.pk2_value IS NULL';
        END IF;*/
        -- BUG:5367903
        IF cpk2_value IS NULL
        THEN
            xprogress := 'POOB-70-1070';
            v_att_detail_where := v_att_detail_where || ' AND ' || v_att_view_name || '.pk2_value IS NULL';
        ELSE                                                                                  -- cPK2_Value IS NOT NULL.
            xprogress := 'POOB-70-1080';

            IF cpk2_value <> c_any_value
            THEN
                v_att_detail_where := v_att_detail_where || ' AND ' || v_att_view_name || '.pk2_value = :cPK2_Value';
            END IF;
        END IF;

        xprogress := 'POOB-70-1090';

        IF centity_name <> 'MTL_SYSTEM_ITEMS'
        THEN                                                                                                  -- 3550723
            /* IF cPK3_Value IS NOT NULL THEN
             xProgress := 'POOB-70-1100';
             v_att_detail_where := v_att_detail_where || ' AND ' ||
                                   v_att_view_name    || '.pk3_value = :cPK3_Value';
            ELSE  -- cPK3_Value IS NULL.
             xProgress := 'POOB-70-1110';
             v_att_detail_where := v_att_detail_where || ' AND ' ||
                                   v_att_view_name    || '.pk3_value IS NULL';
            END IF; */
            -- BUG:5367903

            IF cpk3_value IS NULL
            THEN
                xprogress := 'POOB-70-1100';
                v_att_detail_where := v_att_detail_where || ' AND ' || v_att_view_name || '.pk3_value IS NULL';
            ELSE                                                                              -- cPK3_Value IS NOT NULL.
                xprogress := 'POOB-70-1110';

                IF cpk3_value <> c_any_value
                THEN
                    v_att_detail_where := v_att_detail_where || ' AND ' || v_att_view_name || '.pk3_value = :cPK3_Value';
                END IF;
            END IF;

            xprogress := 'POOB-70-1120';

            /*IF cPK4_Value IS NOT NULL THEN
             xProgress := 'POOB-70-1130';
             v_att_detail_where := v_att_detail_where || ' AND ' ||
                                   v_att_view_name    || '.pk4_value = :cPK4_Value';
            ELSE  -- cPK4_Value IS NULL.
             xProgress := 'POOB-70-1140';
             v_att_detail_where := v_att_detail_where || ' AND ' ||
                                   v_att_view_name    || '.pk4_value IS NULL';
            END IF;*/
            -- BUG:5367903
            IF cpk4_value IS NULL
            THEN
                xprogress := 'POOB-70-1130';
                v_att_detail_where := v_att_detail_where || ' AND ' || v_att_view_name || '.pk4_value IS NULL';
            ELSE                                                                              -- cPK4_Value IS NOT NULL.
                xprogress := 'POOB-70-1140';

                IF cpk4_value <> c_any_value
                THEN
                    v_att_detail_where := v_att_detail_where || ' AND ' || v_att_view_name || '.pk4_value = :cPK4_Value';
                END IF;
            END IF;

            xprogress := 'POOB-70-1150';

            /* IF cPK5_Value IS NOT NULL THEN
             xProgress := 'POOB-70-1160';
             v_att_detail_where := v_att_detail_where || ' AND ' ||
                                   v_att_view_name    || '.pk5_value = :cPK5_Value';
            ELSE  -- cPK5_Value IS NULL.
             xProgress := 'POOB-70-1170';
             v_att_detail_where := v_att_detail_where || ' AND ' ||
                                   v_att_view_name    || '.pk5_value IS NULL';
            END IF; */
            -- BUG:5367903
            IF cpk5_value IS NULL
            THEN
                xprogress := 'POOB-70-1160';
                v_att_detail_where := v_att_detail_where || ' AND ' || v_att_view_name || '.pk5_value IS NULL';
            ELSE                                                                              -- cPK5_Value IS NOT NULL.
                xprogress := 'POOB-70-1170';

                IF cpk5_value <> c_any_value
                THEN
                    v_att_detail_where := v_att_detail_where || ' AND ' || v_att_view_name || '.pk5_value = :cPK5_Value';
                END IF;
            END IF;
        END IF;

        xprogress := 'POOB-70-1180';
        v_att_detail_where := v_att_detail_where || ' AND ' || v_att_view_name || '.usage_type <> ''T''';
        --Bug 2187958
        xprogress := 'POOB-70-1190';
        /* v_att_detail_where := v_att_detail_where || ' ORDER BY ' ||
                               v_att_view_name    || '.att_seq_num';*/

        v_att_detail_where := v_att_detail_where || ' ORDER BY ' || v_att_view_name || '.attached_document_id';

        -- Now we put all the clauses together.
        xprogress := 'POOB-70-1200';
        v_att_detail_select := v_att_detail_select || v_att_detail_from || v_att_detail_where;

        IF ec_debug.g_debug_level = 3
        THEN
            ec_debug.pl (3, 'v_att_detail_select: ', v_att_detail_select);
        END IF;

        -- Open Cursor.
        xprogress := 'POOB-70-1210';
        n_att_detail_sel_c := DBMS_SQL.open_cursor;

        -- Parse Cursor.
        xprogress := 'POOB-70-1220';

        BEGIN
            xprogress := 'POOB-70-1230';
            DBMS_SQL.parse (n_att_detail_sel_c, v_att_detail_select, DBMS_SQL.native);
        EXCEPTION
            WHEN OTHERS
            THEN
                ece_error_handling_pvt.print_parse_error (DBMS_SQL.last_error_position, v_att_detail_select);
                app_exception.raise_exception;
        END;

        xprogress := 'POOB-70-1240';
        DBMS_SQL.bind_variable (n_att_detail_sel_c, ':cEntity_name', centity_name);

        xprogress := 'POOB-70-1250';
        DBMS_SQL.bind_variable (n_att_detail_sel_c, ':cName', cname);

        xprogress := 'POOB-70-1255';
        DBMS_SQL.bind_variable (n_att_detail_sel_c, ':cAtt_Seq_Num', catt_seq_num);

        xprogress := 'POOB-70-1258';
        DBMS_SQL.bind_variable (n_att_detail_sel_c, ':cAtt_doc_id', catt_doc_id);

        xprogress := 'POOB-70-1260';
        DBMS_SQL.bind_variable (n_att_detail_sel_c, ':cPK1_Value', cpk1_value);

        xprogress := 'POOB-70-1270';

        /* IF cPK2_Value IS NOT NULL THEN */
                                                                                                          -- BUG:5367903
        IF cpk2_value IS NOT NULL AND cpk2_value <> c_any_value
        THEN
            DBMS_SQL.bind_variable (n_att_detail_sel_c, ':cPK2_Value', cpk2_value);
        END IF;

        IF centity_name <> 'MTL_SYSTEM_ITEMS'
        THEN                                                                                                  -- 3550723
            xprogress := 'POOB-70-1280';

            /* IF cPK3_Value IS NOT NULL THEN */
                                                                                                          -- BUG:5367903
            IF cpk3_value IS NOT NULL AND cpk3_value <> c_any_value
            THEN
                DBMS_SQL.bind_variable (n_att_detail_sel_c, ':cPK3_Value', cpk3_value);
            END IF;

            xprogress := 'POOB-70-1290';

            /* IF cPK4_Value IS NOT NULL THEN */
                                                                                                          -- BUG:5367903
            IF cpk4_value IS NOT NULL AND cpk4_value <> c_any_value
            THEN
                DBMS_SQL.bind_variable (n_att_detail_sel_c, ':cPK4_Value', cpk4_value);
            END IF;

            xprogress := 'POOB-70-1300';

            /* IF cPK5_Value IS NOT NULL THEN */
                                                                                                          -- BUG:5367903
            IF cpk5_value IS NOT NULL AND cpk5_value <> c_any_value
            THEN
                DBMS_SQL.bind_variable (n_att_detail_sel_c, ':cPK5_Value', cpk5_value);
            END IF;
        END IF;

        -- Set Counter
        xprogress := 'POOB-70-1310';
        n_detail_count := l_att_detail_tbl.COUNT;

        IF ec_debug.g_debug_level = 3
        THEN
            ec_debug.pl (3, 'n_detail_count: ', n_detail_count);
        END IF;

        -- Define Columns
        xprogress := 'POOB-70-1320';
        ece_flatfile_pvt.define_interface_column_type (n_att_detail_sel_c
                                                      ,v_att_detail_select
                                                      ,ece_extract_utils_pub.g_maxcolwidth
                                                      ,l_att_detail_tbl);

        IF cdata_type_id = 1
        THEN                                                                                               -- Short Text
            xprogress := 'POOB-70-1330';
            /* dbms_sql.define_column(n_att_detail_sel_c,n_detail_count + 1,v_att_detail_select,ece_extract_utils_pub.G_MAXCOLWIDTH);*/
            /*Bug 2153310.
              Increased the size to 2000 since the short text attachment can have
              2000 characters */
            DBMS_SQL.define_column (n_att_detail_sel_c
                                   ,n_detail_count + 1
                                   ,v_att_detail_select
                                   ,2000);
        ELSE                                                                                                -- Long Text
            xprogress := 'POOB-70-1340';
            DBMS_SQL.define_column_long (n_att_detail_sel_c, n_detail_count + 1);
        END IF;

        -- Execute Cursor
        xprogress := 'POOB-70-1350';
        n_dummy := DBMS_SQL.execute (n_att_detail_sel_c);

        xprogress := 'POOB-70-1360';

        WHILE DBMS_SQL.fetch_rows (n_att_detail_sel_c) > 0
        LOOP
            xprogress := 'POOB-70-1370';
            ec_debug.pl (3, 'xProgress: ', xprogress);

            ece_flatfile_pvt.assign_column_value_to_tbl (n_att_detail_sel_c, l_att_detail_tbl);

            n_segment_number := 1;
            n_cur_pos := 1;
            n_new_chunk_size := 0;

            xprogress := 'POOB-70-1380';

            IF ec_debug.g_debug_level = 3
            THEN
                ec_debug.pl (3, 'xProgress: ', xprogress);
            END IF;

            IF cdata_type_id = 1
            THEN                                                                                           -- Short Text
                xprogress := 'POOB-70-1390';

                IF ec_debug.g_debug_level = 3
                THEN
                    ec_debug.pl (3, 'xProgress: ', xprogress);
                END IF;

                DBMS_SQL.COLUMN_VALUE (n_att_detail_sel_c, n_detail_count + 1, l_short_text);
                -- l_short_text := LTRIM(RTRIM(l_short_text));     -- Not using this one because white spaces at front may be intentional
                l_short_text := RTRIM (l_short_text);
            END IF;

            xprogress := 'POOB-70-1400';
            ec_debug.pl (3, 'xProgress: ', xprogress);

            LOOP
                xprogress := 'POOB-70-1410';

                IF ec_debug.g_debug_level = 3
                THEN
                    ec_debug.pl (3, 'xProgress: ', xprogress);
                END IF;

                n_pr_pos := 0;                                                                                -- 3618073

                IF cdata_type_id = 1
                THEN                                                                                       -- Short Text
                    xprogress := 'POOB-70-1420';

                    IF ec_debug.g_debug_level = 3
                    THEN
                        ec_debug.pl (3, 'xProgress: ', xprogress);
                    END IF;

                    l_short_text := SUBSTRB (l_short_text, n_cur_pos);
                    n_short_text_length := LENGTH (l_short_text);                                         -- bug 3310412
                    -- l_data_chunk := SUBSTRB(l_short_text,n_cur_pos,cSegment_Size);
                    l_data_chunk := SUBSTRB (l_short_text, 1, csegment_size);

                    n_return_size := LENGTH (l_data_chunk);

                    IF ec_debug.g_debug_level = 3
                    THEN
                        ec_debug.pl (3, 'n_cur_pos: ', n_cur_pos);
                        ec_debug.pl (3, 'n_return_size: ', n_return_size);
                        ec_debug.pl (3, 'l_short_text: ', l_short_text);
                        ec_debug.pl (3, 'l_data_chunk: ', l_data_chunk);
                    END IF;
                ELSIF cdata_type_id = 2
                THEN                                                                                        -- Long Text
                    xprogress := 'POOB-70-1430';

                    IF ec_debug.g_debug_level = 3
                    THEN
                        ec_debug.pl (3, 'xProgress: ', xprogress);
                    END IF;

                    --                         Cursor(I)         ,Pos(I)            ,Len(I)       ,Offset(I)    ,Value(O)    ,Value Len(O)
                    DBMS_SQL.column_value_long (n_att_detail_sel_c
                                               ,n_detail_count + 1
                                               ,csegment_size
                                               ,n_cur_pos - 1
                                               ,l_data_chunk
                                               ,n_return_size);

                    IF ec_debug.g_debug_level = 3
                    THEN
                        ec_debug.pl (3, 'n_cur_pos: ', n_cur_pos);
                        ec_debug.pl (3, 'n_return_size: ', n_return_size);
                        ec_debug.pl (3, 'l_data_chunk: ', l_data_chunk);
                    END IF;
                END IF;

                xprogress := 'POOB-70-1440';

                IF ec_debug.g_debug_level = 3
                THEN
                    ec_debug.pl (3, 'xProgress: ', xprogress);
                END IF;

                EXIT WHEN (n_return_size = 0 AND cdata_type_id = 2) OR (l_short_text IS NULL AND cdata_type_id = 1);

                xprogress := 'POOB-70-1450';

                IF ec_debug.g_debug_level = 3
                THEN
                    ec_debug.pl (3, 'xProgress: ', xprogress);
                END IF;

                v_continue_flag := 'Y';
                n_cr_pos := INSTR (l_data_chunk, c_local_chr_13);
                n_lf_pos := INSTR (l_data_chunk, c_local_chr_10);

                IF ec_debug.g_debug_level = 3
                THEN
                    ec_debug.pl (3, 'n_cr_pos: ', n_cr_pos);
                    ec_debug.pl (3, 'n_lf_pos: ', n_lf_pos);
                END IF;

                IF n_cr_pos = 0 AND n_lf_pos = 0
                THEN                                                          -- There are no CR or CRLF in the chunk...
                    xprogress := 'POOB-70-1460';

                    IF ec_debug.g_debug_level = 3
                    THEN
                        ec_debug.pl (3, 'xProgress: ', xprogress);
                    END IF;

                    IF v_split_word = 'N'
                    THEN                                                         -- No I am not allowed to split a word.
                        xprogress := 'POOB-70-1470';

                        IF ec_debug.g_debug_level = 3
                        THEN
                            ec_debug.pl (3, 'xProgress: ', xprogress);
                        END IF;

                        v_last_char := SUBSTRB (l_data_chunk, n_return_size, 1);

                        IF v_last_char NOT IN ('.', ',', ';', '-', ' ')
                        THEN
                            xprogress := 'POOB-70-1480';

                            IF ec_debug.g_debug_level = 3
                            THEN
                                ec_debug.pl (3, 'xProgress: ', xprogress);
                            END IF;

                            n_space_pos :=
                                INSTR (l_data_chunk
                                      ,' '
                                      ,-1
                                      ,1);

                            IF n_space_pos <> 0
                            THEN                                                                 --Space Character Found
                                xprogress := 'POOB-70-1490';

                                -- bug 3310412
                                /*  if the return size is less than the allowed segment size*/
                                /*  return the whole text as word splitting is not possible */
                                IF (n_return_size < csegment_size)
                                THEN
                                    n_new_chunk_size := n_return_size;
                                ELSE
                                    /*  If remaining length of short text is equal to segment size */
                                    /*  return the whole text without splitting */
                                    IF (cdata_type_id = 2 AND (n_short_text_length = csegment_size))
                                    THEN
                                        n_new_chunk_size := n_return_size;
                                    /* Else remaining length is greater than segment size */
                                    /* The last word is split */
                                    /* Print upto the last space character */
                                    ELSE
                                        n_new_chunk_size := n_space_pos;
                                    END IF;
                                END IF;

                                -- bug 3310412
                                l_data_chunk := SUBSTRB (l_data_chunk, 1, n_new_chunk_size);

                                IF ec_debug.g_debug_level = 3
                                THEN
                                    ec_debug.pl (3, 'xProgress: ', xprogress);
                                    ec_debug.pl (3, 'n_new_chunk_size: ', n_new_chunk_size);
                                END IF;
                            ELSE                                                               --Space Chacter Not Found
                                xprogress := 'POOB-70-1492';
                                n_new_chunk_size := n_return_size;

                                IF ec_debug.g_debug_level = 3
                                THEN
                                    ec_debug.pl (3, 'xProgress: ', xprogress);
                                    ec_debug.pl (3, 'n_new_chunk_size: ', n_new_chunk_size);
                                END IF;
                            END IF;
                        ELSE                                         -- Whew, last character is a breakable character...
                            xprogress := 'POOB-70-1494';

                            n_new_chunk_size := n_return_size;

                            IF ec_debug.g_debug_level = 3
                            THEN
                                ec_debug.pl (3, 'xProgress: ', xprogress);
                                ec_debug.pl (3, 'n_new_chunk_size: ', n_new_chunk_size);
                            END IF;
                        END IF;
                    ELSE                                                            -- Yes I am allowed to split a word.
                        xprogress := 'POOB-70-1496';

                        n_new_chunk_size := n_return_size;

                        IF ec_debug.g_debug_level = 3
                        THEN
                            ec_debug.pl (3, 'xProgress: ', xprogress);
                            ec_debug.pl (3, 'n_new_chunk_size: ', n_new_chunk_size);
                        END IF;
                    END IF;
                ELSIF n_cr_pos = 1 OR n_lf_pos = 1
                THEN                                                                          -- This is a blank line...
                    xprogress := 'POOB-70-1500';

                    IF ec_debug.g_debug_level = 3
                    THEN
                        ec_debug.pl (3, 'xProgress: ', xprogress);
                    END IF;

                    IF n_cr_pos = 1 AND n_lf_pos = 2
                    THEN
                        xprogress := 'POOB-70-1510';

                        IF ec_debug.g_debug_level = 3
                        THEN
                            ec_debug.pl (3, 'xProgress: ', xprogress);
                        END IF;

                        n_new_chunk_size := 2;
                        n_pr_pos := 0;                                                                        -- 3618073
                    ELSE
                        xprogress := 'POOB-70-1520';

                        IF ec_debug.g_debug_level = 3
                        THEN
                            ec_debug.pl (3, 'xProgress: ', xprogress);
                        END IF;

                        n_new_chunk_size := 1;
                        n_pr_pos := 0;                                                                        -- 3618073
                    END IF;

                    xprogress := 'POOB-70-1530';

                    IF ec_debug.g_debug_level = 3
                    THEN
                        ec_debug.pl (3, 'xProgress: ', xprogress);
                    END IF;

                    l_data_chunk := '';
                ELSIF n_cr_pos > 1 OR n_lf_pos > 1
                THEN                                                              -- There is a CR or LF in the chunk...
                    xprogress := 'POOB-70-1540';

                    IF ec_debug.g_debug_level = 3
                    THEN
                        ec_debug.pl (3, 'xProgress: ', xprogress);
                    END IF;

                    IF n_cr_pos > 1
                    THEN
                        xprogress := 'POOB-70-1550';

                        IF ec_debug.g_debug_level = 3
                        THEN
                            ec_debug.pl (3, 'xProgress: ', xprogress);
                        END IF;

                        IF n_lf_pos = n_cr_pos + 1
                        THEN                                                                  -- This is a CRLF combo...
                            xprogress := 'POOB-70-1560';

                            IF ec_debug.g_debug_level = 3
                            THEN
                                ec_debug.pl (3, 'xProgress: ', xprogress);
                            END IF;

                            n_new_chunk_size := n_lf_pos;
                            n_pr_pos := 2;                                                                    -- 3618073
                        ELSE                                                                       -- This is CR only...
                            xprogress := 'POOB-70-1570';

                            IF ec_debug.g_debug_level = 3
                            THEN
                                ec_debug.pl (3, 'xProgress: ', xprogress);
                            END IF;

                            n_new_chunk_size := n_cr_pos;
                            n_pr_pos := 1;                                                                     --3618073
                        END IF;
                    ELSE                                                                              -- This is LF only
                        xprogress := 'POOB-70-1580';

                        IF ec_debug.g_debug_level = 3
                        THEN
                            ec_debug.pl (3, 'xProgress: ', xprogress);
                        END IF;

                        n_new_chunk_size := n_lf_pos;
                        n_pr_pos := 1;
                    END IF;

                    xprogress := 'POOB-70-1590';

                    l_data_chunk := SUBSTRB (l_data_chunk, 1, n_new_chunk_size - n_pr_pos);                   -- 3618073

                    IF ec_debug.g_debug_level = 3
                    THEN
                        ec_debug.pl (3, 'xProgress: ', xprogress);
                        ec_debug.pl (3, 'n_new_chunk_size: ', n_new_chunk_size);
                        ec_debug.pl (3, 'l_data_chunk: ', l_data_chunk);
                    END IF;
                END IF;

                xprogress := 'POOB-70-1600';

                l_att_detail_tbl (n_att_seg_pos).VALUE := l_data_chunk;

                l_att_detail_tbl (n_seg_num_pos).VALUE := n_segment_number;

                IF ec_debug.g_debug_level = 3
                THEN
                    ec_debug.pl (3, 'xProgress: ', xprogress);
                    ec_debug.pl (3, 'l_data_chunk: ', l_data_chunk);
                    ec_debug.pl (3, 'n_new_chunk_size: ', n_new_chunk_size);
                    ec_debug.pl (3, 'n_segment_number: ', n_segment_number);
                    ec_debug.pl (3, 'n_cur_pos: ', n_cur_pos);
                END IF;

                xprogress := 'POOB-70-1610';

                IF cdata_type_id = 1
                THEN                                                                                       -- Short Text
                    n_cur_pos := n_new_chunk_size + 1;
                ELSIF cdata_type_id = 2
                THEN                                                                                        -- Long Text
                    n_cur_pos := n_cur_pos + n_new_chunk_size;
                END IF;

                n_segment_number := n_segment_number + 1;

                IF ec_debug.g_debug_level = 3
                THEN
                    ec_debug.pl (3, 'New n_cur_pos: ', n_cur_pos);
                    ec_debug.pl (3, 'n_segment_number: ', n_segment_number);
                END IF;

                BEGIN
                    xprogress := 'POOB-70-1620';
                    ec_debug.pl (3, 'xProgress: ', xprogress);

                    SELECT ece_attachment_details_s.NEXTVAL INTO n_detail_fkey FROM DUAL;

                    IF ec_debug.g_debug_level = 3
                    THEN
                        ec_debug.pl (3, 'n_detail_fkey: ', n_detail_fkey);
                    END IF;
                EXCEPTION
                    WHEN NO_DATA_FOUND
                    THEN
                        ec_debug.pl (0
                                    ,'EC'
                                    ,'ECE_GET_NEXT_SEQ_FAILED'
                                    ,'PROGRESS_LEVEL'
                                    ,xprogress
                                    ,'SEQ'
                                    ,'ECE_ATTACHMENT_DETAILS_S');
                END;

                -- I have to execute the following few lines of code because, I have no way of knowing at this point
                -- whether to set the Continue Flag 'Y' or 'N' until I loop around to the top again. This is how I find
                -- out ahead of time what the answer will be.
                IF cdata_type_id = 1
                THEN                                                                                       -- Short Text
                    xprogress := 'POOB-70-1622';

                    IF ec_debug.g_debug_level = 3
                    THEN
                        ec_debug.pl (3, 'xProgress: ', xprogress);
                    END IF;

                    l_temp_text := SUBSTRB (l_short_text, n_cur_pos);
                ELSIF cdata_type_id = 2
                THEN                                                                                        -- Long Text
                    xprogress := 'POOB-70-1623';

                    IF ec_debug.g_debug_level = 3
                    THEN
                        ec_debug.pl (3, 'xProgress: ', xprogress);
                    END IF;

                    DBMS_SQL.column_value_long (n_att_detail_sel_c
                                               ,n_detail_count + 1
                                               ,csegment_size
                                               ,n_cur_pos - 1
                                               ,l_temp_text
                                               ,n_temp_return_size);
                END IF;

                IF (n_temp_return_size = 0 AND cdata_type_id = 2) OR (l_temp_text IS NULL AND cdata_type_id = 1)
                THEN
                    xprogress := 'POOB-70-1624';

                    IF ec_debug.g_debug_level = 3
                    THEN
                        ec_debug.pl (3, 'xProgress: ', xprogress);
                    END IF;

                    v_continue_flag := 'N';
                END IF;

                l_att_detail_tbl (n_cont_flag_pos).VALUE := v_continue_flag;

                IF ec_debug.g_debug_level = 3
                THEN
                    ec_debug.pl (3, 'v_continue_flag: ', v_continue_flag);
                END IF;

                xprogress := 'POOB-70-1625';
                ec_debug.pl (3, 'xProgress: ', xprogress);
                l_att_detail_tbl (n_att_seq_num_pos).VALUE := catt_seq_num;
                l_att_detail_tbl (n_entity_name_pos).VALUE := centity_name;
                l_att_detail_tbl (n_name_pos).VALUE := cname;
                l_att_detail_tbl (n_pk1_value_pos).VALUE := cpk1_value;
                l_att_detail_tbl (n_pk2_value_pos).VALUE := cpk2_value;
                l_att_detail_tbl (n_pk3_value_pos).VALUE := cpk3_value;
                l_att_detail_tbl (n_pk4_value_pos).VALUE := cpk4_value;
                l_att_detail_tbl (n_pk5_value_pos).VALUE := cpk5_value;
                l_att_detail_tbl (n_run_id_pos).VALUE := irun_id;
                l_att_detail_tbl (n_att_doc_id_pos).VALUE := catt_doc_id;                                  --Bug 2187958
                xprogress := 'POOB-70-1630';
                ec_debug.pl (3, 'xProgress: ', xprogress);
                /* ece_extract_utils_pub.insert_into_interface_tbl(
                    iRun_id,
                    cTransaction_Type,
                    cCommunication_Method,
                    cAtt_Detail_Interface,
                    l_att_detail_tbl,
                    n_detail_fkey); */
                -- 2823215
                ece_poo_transaction.write_to_file (ctransaction_type
                                                  ,ccommunication_method
                                                  ,catt_detail_interface
                                                  ,l_att_detail_tbl
                                                  ,ioutput_width
                                                  ,irun_id
                                                  ,cfile_common_key
                                                  ,n_detail_fkey);
            -- 2823215
            END LOOP;
        -- populate_ext_att_detail();

        END LOOP;

        xprogress := 'POOB-70-1640';
        DBMS_SQL.close_cursor (n_att_detail_sel_c);

        IF ec_debug.g_debug_level >= 2
        THEN
            ec_debug.pop ('ECE_POO_TRANSACTION.POPULATE_TEXT_ATT_DETAIL');
        END IF;
    EXCEPTION
        WHEN OTHERS
        THEN
            ec_debug.pl (0
                        ,'EC'
                        ,'ECE_PROGRAM_ERROR'
                        ,'PROGRESS_LEVEL'
                        ,xprogress);
            ec_debug.pl (0
                        ,'EC'
                        ,'ECE_ERROR_MESSAGE'
                        ,'ERROR_MESSAGE'
                        ,SQLERRM);

            app_exception.raise_exception;
    END populate_text_att_detail;

    PROCEDURE put_att_to_output_table (ccommunication_method   IN VARCHAR2
                                      ,ctransaction_type       IN VARCHAR2
                                      ,ioutput_width           IN INTEGER
                                      ,irun_id                 IN INTEGER
                                      ,cheader_output_level    IN NUMBER
                                      ,cdetail_output_level    IN NUMBER
                                      ,cheader_interface       IN VARCHAR2
                                      ,cdetail_interface       IN VARCHAR2
                                      ,centity_name            IN VARCHAR2
                                      ,cname                   IN VARCHAR2
                                      ,cpk1_value              IN VARCHAR2
                                      ,cpk2_value              IN VARCHAR2
                                      ,cpk3_value              IN VARCHAR2
                                      ,cpk4_value              IN VARCHAR2
                                      ,cpk5_value              IN VARCHAR2
                                      ,cfile_common_key        IN VARCHAR2)
    IS
        xprogress                  VARCHAR2 (80);

        l_header_tbl               ece_flatfile_pvt.interface_tbl_type;

        c_header_common_key_name   VARCHAR2 (40);

        nseq_num_pos               NUMBER;
        nseq_num                   NUMBER;
        ndoc_id_pos                NUMBER;
        ndoc_id                    NUMBER;
        natt_doc_id_pos            NUMBER;
        natt_doc_id                NUMBER;
        header_sel_c               INTEGER;
        header_del_c1              INTEGER;
        header_del_c2              INTEGER;
        cheader_select             VARCHAR2 (32000);
        cheader_from               VARCHAR2 (32000);
        cheader_where              VARCHAR2 (32000);
        cheader_delete1            VARCHAR2 (32000);
        cheader_delete2            VARCHAR2 (32000);

        cheader_x_interface        VARCHAR2 (50);

        iheader_count              NUMBER;
        dummy                      INTEGER;
        rheader_rowid              ROWID;
        rheader_x_rowid            ROWID;
        get_no_rows                INTEGER;
        get_no_lrows               INTEGER;
    BEGIN
        IF ec_debug.g_debug_level >= 2
        THEN
            ec_debug.push ('ECE_POO_TRANSACTION.PUT_ATT_TO_OUTPUT_TABLE');
            ec_debug.pl (3, 'cEntity_Name: ', centity_name);
            ec_debug.pl (3, 'cName: ', cname);
            ec_debug.pl (3, 'cPK1_Value: ', cpk1_value);
            ec_debug.pl (3, 'cPK2_Value: ', cpk2_value);
            ec_debug.pl (3, 'cPK3_Value: ', cpk3_value);
            ec_debug.pl (3, 'cPK4_Value: ', cpk4_value);
            ec_debug.pl (3, 'cPK5_Value: ', cpk5_value);
        END IF;

        xprogress := 'POOB-80-1000';
        ece_flatfile_pvt.select_clause (ctransaction_type
                                       ,ccommunication_method
                                       ,cheader_interface
                                       ,cheader_x_interface
                                       ,l_header_tbl
                                       ,c_header_common_key_name
                                       ,cheader_select
                                       ,cheader_from
                                       ,cheader_where
                                       ,cheader_output_level);

        -- Build the WHERE and the ORDER BY Clause.
        cheader_where := cheader_where || ' AND ' || cheader_interface || '.run_id = :Run_id';

        -- Entity Name must not be NULL.
        cheader_where := cheader_where || ' AND ' || cheader_interface || '.entity_name = :Entity_Name';

        -- Name must not be NULL.
        cheader_where := cheader_where || ' AND ' || 'UPPER(' || cheader_interface || '.name) =  UPPER(:Name)';

        -- PK1 Value must not be NULL.
        cheader_where := cheader_where || ' AND ' || cheader_interface || '.pk1_value = :PK1_Value';

        IF cpk2_value IS NOT NULL
        THEN
            xprogress := 'POOB-80-1010';
            cheader_where := cheader_where || ' AND ' || cheader_interface || '.pk2_value = :PK2_Value';
        ELSE                                                                                      -- cPK2_Value IS NULL.
            xprogress := 'POOB-80-1020';
            cheader_where := cheader_where || ' AND ' || cheader_interface || '.pk2_value IS NULL';
        END IF;

        IF cpk3_value IS NOT NULL
        THEN
            xprogress := 'POOB-80-1030';
            cheader_where := cheader_where || ' AND ' || cheader_interface || '.pk3_value = :PK3_Value';
        ELSE                                                                                      -- cPK3_Value IS NULL.
            xprogress := 'POOB-80-1040';
            cheader_where := cheader_where || ' AND ' || cheader_interface || '.pk3_value IS NULL';
        END IF;

        IF cpk4_value IS NOT NULL
        THEN
            xprogress := 'POOB-80-1050';
            cheader_where := cheader_where || ' AND ' || cheader_interface || '.pk4_value = :PK4_Value';
        ELSE                                                                                      -- cPK4_Value IS NULL.
            xprogress := 'POOB-80-1060';
            cheader_where := cheader_where || ' AND ' || cheader_interface || '.pk4_value IS NULL';
        END IF;

        IF cpk5_value IS NOT NULL
        THEN
            xprogress := 'POOB-80-1070';
            cheader_where := cheader_where || ' AND ' || cheader_interface || '.pk5_value = :PK5_Value';
        ELSE                                                                                      -- cPK5_Value IS NULL.
            xprogress := 'POOB-80-1080';
            cheader_where := cheader_where || ' AND ' || cheader_interface || '.pk5_value IS NULL';
        END IF;

        /* Bug 1892253
        If an item has an attachment  and if this
        item is used in more than 1 PO while
        extracting,then the attachment information
        of the item will be repeated in ece_attachment_headers
        n number of times if n number of POs with the same
        item are extracted.
        Added code to pick up no of rows having distinct
        attachment data from ece_attachment_headers and
        appended this number to the where condition.
        */

        IF centity_name = 'MTL_SYSTEM_ITEMS'
        THEN
            BEGIN
                get_no_rows := 0;

                SELECT COUNT (DISTINCT (att_seq_num))
                  INTO get_no_rows
                  FROM ece_attachment_headers
                 WHERE pk1_value = cpk1_value AND pk2_value = cpk2_value AND entity_name LIKE 'MTL_SYSTEM_ITEMS';

                get_no_rows := get_no_rows + 1;
            EXCEPTION
                WHEN OTHERS
                THEN
                    NULL;
            END;

            cheader_where := cheader_where || ' AND ' || ' rownum < ' || get_no_rows;
        END IF;

        /* 2279486
        Get the attachments(attached_document_id) pertaining only to the corrresponding
        release by using line id*/
        IF centity_name = 'PO_LINES'
        THEN
            BEGIN
                get_no_lrows := 0;

                SELECT COUNT (DISTINCT (attached_document_id))
                  INTO get_no_lrows
                  FROM ece_attachment_headers
                 WHERE pk1_value = cpk1_value AND entity_name LIKE 'PO_LINES';

                get_no_lrows := get_no_lrows + 1;
            EXCEPTION
                WHEN OTHERS
                THEN
                    NULL;
            END;

            cheader_where := cheader_where || ' AND ' || ' rownum < ' || get_no_lrows;
        END IF;

        -- Bug 2187958
        /*cHeader_where := cHeader_where        || ' ORDER BY ' ||
                             cHeader_Interface    || '.att_seq_num';*/

        cheader_where := cheader_where || ' ORDER BY ' || cheader_interface || '.attached_document_id';

        cheader_select := cheader_select || ',' || cheader_interface || '.rowid,' || cheader_x_interface || '.rowid ';

        -- Now form the complete SELECT SQL...
        cheader_select := cheader_select || cheader_from || cheader_where || ' FOR UPDATE';
        ec_debug.pl (3, 'cHeader_select: ', cheader_select);

        -- Form DELETE SQL...
        cheader_delete1 := 'DELETE FROM ' || cheader_interface || ' WHERE rowid = :col_rowid';

        -- Form DELETE SQL for the Extension Table...
        cheader_delete2 := 'DELETE FROM ' || cheader_x_interface || ' WHERE rowid = :col_rowid';

        IF ec_debug.g_debug_level = 3
        THEN
            ec_debug.pl (3, 'cHeader_delete1: ', cheader_delete1);
            ec_debug.pl (3, 'cHeader_delete2: ', cheader_delete2);
        END IF;

        -- Open Cursors
        xprogress := 'POOB-80-1090';
        header_sel_c := DBMS_SQL.open_cursor;

        xprogress := 'POOB-80-1100';
        header_del_c1 := DBMS_SQL.open_cursor;

        xprogress := 'POOB-80-1110';
        header_del_c2 := DBMS_SQL.open_cursor;

        -- Parse the SELECT SQL
        BEGIN
            xprogress := 'POOB-80-1120';
            DBMS_SQL.parse (header_sel_c, cheader_select, DBMS_SQL.native);
        EXCEPTION
            WHEN OTHERS
            THEN
                ece_error_handling_pvt.print_parse_error (DBMS_SQL.last_error_position, cheader_select);
                app_exception.raise_exception;
        END;

        -- Parse the DELETE1 SQL
        BEGIN
            xprogress := 'POOB-80-1130';
            DBMS_SQL.parse (header_del_c1, cheader_delete1, DBMS_SQL.native);
        EXCEPTION
            WHEN OTHERS
            THEN
                ece_error_handling_pvt.print_parse_error (DBMS_SQL.last_error_position, cheader_delete1);
                app_exception.raise_exception;
        END;

        -- Parse the DELETE2 SQL
        BEGIN
            xprogress := 'POOB-80-1140';
            DBMS_SQL.parse (header_del_c2, cheader_delete2, DBMS_SQL.native);
        EXCEPTION
            WHEN OTHERS
            THEN
                ece_error_handling_pvt.print_parse_error (DBMS_SQL.last_error_position, cheader_delete2);
                app_exception.raise_exception;
        END;

        --Set Counter
        xprogress := 'POOB-80-1150';
        iheader_count := l_header_tbl.COUNT;

        IF ec_debug.g_debug_level = 3
        THEN
            ec_debug.pl (3, 'iHeader_count: ', iheader_count);
        END IF;

        xprogress := 'POOB-80-1160';
        ece_flatfile_pvt.define_interface_column_type (header_sel_c
                                                      ,cheader_select
                                                      ,ece_flatfile_pvt.g_maxcolwidth
                                                      ,l_header_tbl);

        xprogress := 'POOB-80-1170';
        DBMS_SQL.define_column_rowid (header_sel_c, iheader_count + 1, rheader_rowid);

        xprogress := 'POOB-80-1180';
        DBMS_SQL.define_column_rowid (header_sel_c, iheader_count + 2, rheader_x_rowid);

        -- Bug 2198707
        DBMS_SQL.bind_variable (header_sel_c, 'Run_id', irun_id);
        DBMS_SQL.bind_variable (header_sel_c, 'Entity_Name', centity_name);
        DBMS_SQL.bind_variable (header_sel_c, 'Name', cname);
        DBMS_SQL.bind_variable (header_sel_c, 'PK1_Value', cpk1_value);

        IF cpk2_value IS NOT NULL
        THEN
            DBMS_SQL.bind_variable (header_sel_c, 'PK2_Value', cpk2_value);
        END IF;

        IF cpk3_value IS NOT NULL
        THEN
            DBMS_SQL.bind_variable (header_sel_c, 'PK3_Value', cpk3_value);
        END IF;

        IF cpk4_value IS NOT NULL
        THEN
            DBMS_SQL.bind_variable (header_sel_c, 'PK4_Value', cpk4_value);
        END IF;

        IF cpk5_value IS NOT NULL
        THEN
            DBMS_SQL.bind_variable (header_sel_c, 'PK5_Value', cpk5_value);
        END IF;

        --Execute the Cursor
        xprogress := 'POOB-80-1190';
        dummy := DBMS_SQL.execute (header_sel_c);

        xprogress := 'POOB-80-1200';

        WHILE DBMS_SQL.fetch_rows (header_sel_c) > 0
        LOOP
            xprogress := 'POOB-80-1210';
            ece_flatfile_pvt.assign_column_value_to_tbl (header_sel_c, l_header_tbl);

            xprogress := 'POOB-80-1220';
            DBMS_SQL.COLUMN_VALUE (header_sel_c, iheader_count + 1, rheader_rowid);

            xprogress := 'POOB-80-1230';
            DBMS_SQL.COLUMN_VALUE (header_sel_c, iheader_count + 2, rheader_x_rowid);

            -- Find the Position of the Attachemnt Doc ID
            xprogress := 'POOB-80-1232';
            ece_flatfile_pvt.find_pos (l_header_tbl, 'DOCUMENT_ID', ndoc_id_pos);
            ndoc_id := l_header_tbl (ndoc_id_pos).VALUE;

            -- Find the Position of the Attachment Sequence Number
            xprogress := 'POOB-80-1240';
            ece_flatfile_pvt.find_pos (l_header_tbl, 'ATT_SEQ_NUM', nseq_num_pos);
            -- Get the Attachment Sequence Number itself.
            nseq_num := l_header_tbl (nseq_num_pos).VALUE;
            -- Bug 2187958
            ece_flatfile_pvt.find_pos (l_header_tbl, 'ATTACHED_DOCUMENT_ID', natt_doc_id);
            -- Get the Attachment Document ID
            natt_doc_id := l_header_tbl (natt_doc_id).VALUE;

            IF ec_debug.g_debug_level = 3
            THEN
                ec_debug.pl (3, 'nDoc_ID_pos: ', ndoc_id_pos);
                ec_debug.pl (3, 'nDoc_ID: ', ndoc_id);
                ec_debug.pl (3, 'nSeq_Num: ', nseq_num);
                ec_debug.pl (3, 'nAtt_Doc_ID: ', natt_doc_id);
            END IF;

            xprogress := 'POOB-80-1250';
            ece_poo_transaction.write_to_file (ctransaction_type
                                              ,ccommunication_method
                                              ,cheader_interface
                                              ,l_header_tbl
                                              ,ioutput_width
                                              ,irun_id
                                              ,cfile_common_key
                                              ,NULL);

            xprogress := 'POOB-80-1260';
            DBMS_SQL.bind_variable (header_del_c1, 'col_rowid', rheader_rowid);

            xprogress := 'POOB-80-1270';
            DBMS_SQL.bind_variable (header_del_c2, 'col_rowid', rheader_x_rowid);

            -- Execute the Cursor that deletes from the Interface Table
            xprogress := 'POOB-80-1280';
            dummy := DBMS_SQL.execute (header_del_c1);

            -- Execute the Cursor that deletes from the Extension Table
            xprogress := 'POOB-80-1290';
            dummy := DBMS_SQL.execute (header_del_c2);

            xprogress := 'POOB-80-1300';
            put_att_detail_to_output_table (ccommunication_method
                                           ,ctransaction_type
                                           ,ioutput_width
                                           ,irun_id
                                           ,cdetail_output_level
                                           ,cheader_interface
                                           ,cdetail_interface
                                           ,nseq_num
                                           ,centity_name
                                           ,cname
                                           ,cpk1_value
                                           ,cpk2_value
                                           ,cpk3_value
                                           ,cpk4_value
                                           ,cpk5_value
                                           ,cfile_common_key
                                           ,natt_doc_id);
        END LOOP;

        -- Close the Cursors and Cleanup...
        xprogress := 'POOB-80-1310';
        DBMS_SQL.close_cursor (header_sel_c);
        DBMS_SQL.close_cursor (header_del_c1);
        DBMS_SQL.close_cursor (header_del_c2);

        IF ec_debug.g_debug_level >= 2
        THEN
            ec_debug.pop ('ECE_POO_TRANSACTION.PUT_ATT_TO_OUTPUT_TABLE');
        END IF;
    EXCEPTION
        WHEN OTHERS
        THEN
            ec_debug.pl (0
                        ,'EC'
                        ,'ECE_PROGRAM_ERROR'
                        ,'PROGRESS_LEVEL'
                        ,xprogress);
            ec_debug.pl (0
                        ,'EC'
                        ,'ECE_ERROR_MESSAGE'
                        ,'ERROR_MESSAGE'
                        ,SQLERRM);

            app_exception.raise_exception;
    END put_att_to_output_table;

    PROCEDURE put_att_detail_to_output_table (ccommunication_method   IN VARCHAR2
                                             ,ctransaction_type       IN VARCHAR2
                                             ,ioutput_width           IN INTEGER
                                             ,irun_id                 IN INTEGER
                                             ,cdetail_output_level    IN NUMBER
                                             ,cheader_interface       IN VARCHAR2
                                             ,cdetail_interface       IN VARCHAR2
                                             ,catt_seq_num            IN NUMBER
                                             ,centity_name            IN VARCHAR2
                                             ,cname                   IN VARCHAR2
                                             ,cpk1_value              IN VARCHAR2
                                             ,cpk2_value              IN VARCHAR2
                                             ,cpk3_value              IN VARCHAR2
                                             ,cpk4_value              IN VARCHAR2
                                             ,cpk5_value              IN VARCHAR2
                                             ,cfile_common_key        IN VARCHAR2
                                             ,catt_doc_id             IN NUMBER)
    IS
        xprogress                  VARCHAR2 (80);

        l_detail_tbl               ece_flatfile_pvt.interface_tbl_type;

        c_detail_common_key_name   VARCHAR2 (40);

        detail_sel_c               INTEGER;
        detail_del_c1              INTEGER;
        detail_del_c2              INTEGER;
        cdetail_select             VARCHAR2 (32000);
        cdetail_from               VARCHAR2 (32000);
        cdetail_where              VARCHAR2 (32000);
        cdetail_delete1            VARCHAR2 (32000);
        cdetail_delete2            VARCHAR2 (32000);

        cdetail_x_interface        VARCHAR2 (50);

        idetail_count              NUMBER;
        dummy                      INTEGER;
        rdetail_rowid              ROWID;
        rdetail_x_rowid            ROWID;
        get_no_rows                INTEGER;                                                               -- Bug 1892253
        get_no_lrows               INTEGER;
    BEGIN
        IF ec_debug.g_debug_level >= 2
        THEN
            ec_debug.push ('ECE_POO_TRANSACTION.PUT_ATT_DETAIL_TO_OUTPUT_TABLE');
            ec_debug.pl (3, 'cDetail_Output_Level: ', cdetail_output_level);
            ec_debug.pl (3, 'cAtt_Seq_Num: ', catt_seq_num);
            ec_debug.pl (3, 'cEntity_Name: ', centity_name);
            ec_debug.pl (3, 'cName: ', cname);
            ec_debug.pl (3, 'cPK1_Value: ', cpk1_value);
            ec_debug.pl (3, 'cPK2_Value: ', cpk2_value);
            ec_debug.pl (3, 'cPK3_Value: ', cpk3_value);
            ec_debug.pl (3, 'cPK4_Value: ', cpk4_value);
            ec_debug.pl (3, 'cPK5_Value: ', cpk5_value);
            ec_debug.pl (3, 'cFile_Common_Key: ', cfile_common_key);
            ec_debug.pl (3, 'cAtt_Doc_ID: ', catt_doc_id);
        END IF;

        xprogress := 'POOB-90-1000';
        ece_flatfile_pvt.select_clause (ctransaction_type
                                       ,ccommunication_method
                                       ,cdetail_interface
                                       ,cdetail_x_interface
                                       ,l_detail_tbl
                                       ,c_detail_common_key_name
                                       ,cdetail_select
                                       ,cdetail_from
                                       ,cdetail_where
                                       ,cdetail_output_level);

        -- Build the WHERE and the ORDER BY Clause.
        xprogress := 'POOB-90-1010';
        cdetail_where := cdetail_where || ' AND ' || cdetail_interface || '.run_id = :iRun_id';

        -- Get the right Attachment Sequence.
        xprogress := 'POOB-90-1012';
        cdetail_where := cdetail_where || ' AND ' || cdetail_interface || '.att_seq_num = :cAtt_Seq_Num';
        -- Bug 2187958
        -- Get the Attachment document ID.
        xprogress := 'POOB-90-1012A';
        cdetail_where := cdetail_where || ' AND ' || cdetail_interface || '.attached_document_id = :cAtt_Doc_ID';

        -- Entity Name must not be NULL.
        xprogress := 'POOB-90-1014';
        cdetail_where := cdetail_where || ' AND ' || cdetail_interface || '.entity_name = :cEntity_Name';

        -- Name must not be NULL.
        xprogress := 'POOB-90-1016';
        cdetail_where := cdetail_where || ' AND ' || 'UPPER(' || cdetail_interface || '.name) = UPPER(:cName)';

        -- PK1 Value must not be NULL.
        xprogress := 'POOB-90-1018';
        cdetail_where := cdetail_where || ' AND ' || cdetail_interface || '.pk1_value = :cPK1_Value';

        xprogress := 'POOB-90-1020';

        IF cpk2_value IS NOT NULL
        THEN
            xprogress := 'POOB-90-1030';
            cdetail_where := cdetail_where || ' AND ' || cdetail_interface || '.pk2_value = :cPK2_Value';
        ELSE                                                                                      -- cPK2_Value IS NULL.
            xprogress := 'POOB-90-1040';
            cdetail_where := cdetail_where || ' AND ' || cdetail_interface || '.pk2_value IS NULL';
        END IF;

        IF cpk3_value IS NOT NULL
        THEN
            xprogress := 'POOB-90-1050';
            cdetail_where := cdetail_where || ' AND ' || cdetail_interface || '.pk3_value = :cPK3_Value';
        ELSE                                                                                      -- cPK3_Value IS NULL.
            xprogress := 'POOB-90-1060';
            cdetail_where := cdetail_where || ' AND ' || cdetail_interface || '.pk3_value IS NULL';
        END IF;

        IF cpk4_value IS NOT NULL
        THEN
            xprogress := 'POOB-90-1070';
            cdetail_where := cdetail_where || ' AND ' || cdetail_interface || '.pk4_value = :cPK4_Value';
        ELSE                                                                                      -- cPK4_Value IS NULL.
            xprogress := 'POOB-90-1080';
            cdetail_where := cdetail_where || ' AND ' || cdetail_interface || '.pk4_value IS NULL';
        END IF;

        IF cpk5_value IS NOT NULL
        THEN
            xprogress := 'POOB-90-1090';
            cdetail_where := cdetail_where || ' AND ' || cdetail_interface || '.pk5_value = :cPK5_Value';
        ELSE                                                                                      -- cPK5_Value IS NULL.
            xprogress := 'POOB-90-1100';
            cdetail_where := cdetail_where || ' AND ' || cdetail_interface || '.pk5_value IS NULL';
        END IF;

        /* Bug 1892253.
           Added query to pick the number of segments
           based on pk2_value,pk1_value and sequence
           number and append the no of segments obtained
           to cDetail_where condition
        */

        IF centity_name = 'MTL_SYSTEM_ITEMS'
        THEN
            get_no_rows := 0;

            BEGIN
                SELECT MAX (segment_number)
                  INTO get_no_rows
                  FROM ece_attachment_details
                 WHERE     pk1_value = cpk1_value
                       AND pk2_value = cpk2_value
                       AND entity_name LIKE 'MTL_SYSTEM_ITEMS'
                       AND attached_document_id = catt_doc_id;

                get_no_rows := get_no_rows + 1;
            EXCEPTION
                WHEN OTHERS
                THEN
                    NULL;
            END;

            cdetail_where := cdetail_where || ' AND ' || ' rownum <  ' || get_no_rows;
        END IF;

        /* 2279486
        Get the no of detailed line attachments pertaining only to the corrresponding
        release by using line id and attached_document_id*/

        IF centity_name = 'PO_LINES'
        THEN
            BEGIN
                get_no_lrows := 0;

                SELECT COUNT (DISTINCT (segment_number))
                  INTO get_no_lrows
                  FROM ece_attachment_details
                 WHERE pk1_value = cpk1_value AND entity_name LIKE 'PO_LINES' AND attached_document_id = catt_doc_id;

                get_no_lrows := get_no_lrows + 1;
            EXCEPTION
                WHEN OTHERS
                THEN
                    NULL;
            END;

            cdetail_where := cdetail_where || ' AND ' || ' rownum < ' || get_no_lrows;
        END IF;

        cdetail_where := cdetail_where || ' ORDER BY ' || cdetail_interface || '.segment_number';

        cdetail_select := cdetail_select || ', ' || cdetail_interface || '.rowid, ' || cdetail_x_interface || '.rowid ';

        -- Now form the complete SELECT SQL...
        cdetail_select := cdetail_select || cdetail_from || cdetail_where || ' FOR UPDATE';

        -- Form DELETE SQL...
        cdetail_delete1 := 'DELETE FROM ' || cdetail_interface || ' WHERE rowid = :col_rowid';

        -- Form DELETE SQL for the Extension Table...
        cdetail_delete2 := 'DELETE FROM ' || cdetail_x_interface || ' WHERE rowid = :col_rowid';

        IF ec_debug.g_debug_level = 3
        THEN
            ec_debug.pl (3, 'cDetail_select: ', cdetail_select);
            ec_debug.pl (3, 'cDetail_delete1: ', cdetail_delete1);
            ec_debug.pl (3, 'cDetail_delete2: ', cdetail_delete2);
        END IF;

        -- Open Cursors
        xprogress := 'POOB-90-1110';
        detail_sel_c := DBMS_SQL.open_cursor;

        xprogress := 'POOB-90-1120';
        detail_del_c1 := DBMS_SQL.open_cursor;

        xprogress := 'POOB-90-1130';
        detail_del_c2 := DBMS_SQL.open_cursor;

        -- Parse the SELECT SQL
        BEGIN
            xprogress := 'POOB-90-1140';
            DBMS_SQL.parse (detail_sel_c, cdetail_select, DBMS_SQL.native);
        EXCEPTION
            WHEN OTHERS
            THEN
                ece_error_handling_pvt.print_parse_error (DBMS_SQL.last_error_position, cdetail_select);
                app_exception.raise_exception;
        END;

        -- Parse the DELETE1 SQL
        BEGIN
            xprogress := 'POOB-90-1150';
            DBMS_SQL.parse (detail_del_c1, cdetail_delete1, DBMS_SQL.native);
        EXCEPTION
            WHEN OTHERS
            THEN
                ece_error_handling_pvt.print_parse_error (DBMS_SQL.last_error_position, cdetail_delete1);
                app_exception.raise_exception;
        END;

        -- Parse the DELETE2 SQL
        BEGIN
            xprogress := 'POOB-90-1160';
            DBMS_SQL.parse (detail_del_c2, cdetail_delete2, DBMS_SQL.native);
        EXCEPTION
            WHEN OTHERS
            THEN
                ece_error_handling_pvt.print_parse_error (DBMS_SQL.last_error_position, cdetail_delete2);
                app_exception.raise_exception;
        END;

        --Set Counter
        xprogress := 'POOB-90-1170';
        idetail_count := l_detail_tbl.COUNT;

        IF ec_debug.g_debug_level = 3
        THEN
            ec_debug.pl (3, 'iDetail_count: ', idetail_count);
        END IF;

        xprogress := 'POOB-90-1180';
        ece_flatfile_pvt.define_interface_column_type (detail_sel_c
                                                      ,cdetail_select
                                                      ,ece_flatfile_pvt.g_maxcolwidth
                                                      ,l_detail_tbl);

        xprogress := 'POOB-90-1190';
        DBMS_SQL.define_column_rowid (detail_sel_c, idetail_count + 1, rdetail_rowid);

        xprogress := 'POOB-90-1200';
        DBMS_SQL.define_column_rowid (detail_sel_c, idetail_count + 2, rdetail_x_rowid);

        --Bind Variables
        xprogress := 'POOB-90-1201';
        DBMS_SQL.bind_variable (detail_sel_c, ':iRun_id', irun_id);

        xprogress := 'POOB-90-1202';
        DBMS_SQL.bind_variable (detail_sel_c, ':cAtt_Seq_Num', catt_seq_num);

        xprogress := 'POOB-90-1202a';
        DBMS_SQL.bind_variable (detail_sel_c, ':cAtt_Doc_ID', catt_doc_id);

        xprogress := 'POOB-90-1203';
        DBMS_SQL.bind_variable (detail_sel_c, ':cEntity_Name', centity_name);

        xprogress := 'POOB-90-1204';
        DBMS_SQL.bind_variable (detail_sel_c, ':cName', cname);

        xprogress := 'POOB-90-1205';
        DBMS_SQL.bind_variable (detail_sel_c, ':cPK1_Value', cpk1_value);

        IF cpk2_value IS NOT NULL
        THEN
            xprogress := 'POOB-90-1206';
            DBMS_SQL.bind_variable (detail_sel_c, ':cPK2_Value', cpk2_value);
        END IF;

        IF cpk3_value IS NOT NULL
        THEN
            xprogress := 'POOB-90-1207';
            DBMS_SQL.bind_variable (detail_sel_c, ':cPK3_Value', cpk3_value);
        END IF;

        IF cpk4_value IS NOT NULL
        THEN
            xprogress := 'POOB-90-1208';
            DBMS_SQL.bind_variable (detail_sel_c, ':cPK4_Value', cpk4_value);
        END IF;

        IF cpk5_value IS NOT NULL
        THEN
            xprogress := 'POOB-90-1209';
            DBMS_SQL.bind_variable (detail_sel_c, ':cPK5_Value', cpk5_value);
        END IF;

        --Execute the Cursor
        xprogress := 'POOB-90-1210';
        dummy := DBMS_SQL.execute (detail_sel_c);

        xprogress := 'POOB-90-1220';

        WHILE DBMS_SQL.fetch_rows (detail_sel_c) > 0
        LOOP
            xprogress := 'POOB-90-1230';
            ece_flatfile_pvt.assign_column_value_to_tbl (detail_sel_c, l_detail_tbl);

            xprogress := 'POOB-90-1240';
            DBMS_SQL.COLUMN_VALUE (detail_sel_c, idetail_count + 1, rdetail_rowid);

            xprogress := 'POOB-90-1250';
            DBMS_SQL.COLUMN_VALUE (detail_sel_c, idetail_count + 2, rdetail_x_rowid);

            xprogress := 'POOB-90-1260';
            ece_poo_transaction.write_to_file (ctransaction_type
                                              ,ccommunication_method
                                              ,cdetail_interface
                                              ,l_detail_tbl
                                              ,ioutput_width
                                              ,irun_id
                                              ,cfile_common_key
                                              ,NULL);

            xprogress := 'POOB-90-1270';
            DBMS_SQL.bind_variable (detail_del_c1, 'col_rowid', rdetail_rowid);

            xprogress := 'POOB-90-1280';
            DBMS_SQL.bind_variable (detail_del_c2, 'col_rowid', rdetail_x_rowid);

            -- Execute the Cursor that deletes from the Interface Table
            xprogress := 'POOB-90-1290';
            dummy := DBMS_SQL.execute (detail_del_c1);

            -- Execute the Cursor that deletes from the Extension Table
            xprogress := 'POOB-90-1300';
            dummy := DBMS_SQL.execute (detail_del_c2);
        END LOOP;

        -- Close the Cursors and Cleanup...
        xprogress := 'POOB-90-1310';
        DBMS_SQL.close_cursor (detail_sel_c);
        DBMS_SQL.close_cursor (detail_del_c1);
        DBMS_SQL.close_cursor (detail_del_c2);

        IF ec_debug.g_debug_level >= 2
        THEN
            ec_debug.pop ('ECE_POO_TRANSACTION.PUT_ATT_DETAIL_TO_OUTPUT_TABLE');
        END IF;
    EXCEPTION
        WHEN OTHERS
        THEN
            ec_debug.pl (0
                        ,'EC'
                        ,'ECE_PROGRAM_ERROR'
                        ,'PROGRESS_LEVEL'
                        ,xprogress);
            ec_debug.pl (0
                        ,'EC'
                        ,'ECE_ERROR_MESSAGE'
                        ,'ERROR_MESSAGE'
                        ,SQLERRM);

            app_exception.raise_exception;
    END put_att_detail_to_output_table;

    PROCEDURE write_to_file (ctransaction_type       IN VARCHAR2
                            ,ccommunication_method   IN VARCHAR2
                            ,cinterface_table        IN VARCHAR2
                            ,p_interface_tbl         IN ece_flatfile_pvt.interface_tbl_type
                            ,ioutput_width           IN INTEGER
                            ,irun_id                 IN INTEGER
                            ,p_common_key            IN VARCHAR2
                            ,p_foreign_key           IN NUMBER)
    IS
        xprogress              VARCHAR2 (30);
        coutput_path           VARCHAR2 (120);
        iline_pos              INTEGER;
        idata_count            INTEGER := p_interface_tbl.COUNT;
        istart_num             INTEGER;
        irow_num               INTEGER;
        cinsert_stmt           VARCHAR2 (32000);
        l_common_key           VARCHAR2 (255) := p_common_key;
        l_count                NUMBER;
        cvalue                 VARCHAR2 (32000);
        csrc_tbl_val_wo_newl   VARCHAR2 (32000);
        csrc_tbl_val_wo_frmf   VARCHAR2 (32000);
        csrc_tbl_val_wo_tab    VARCHAR2 (32000);
        c_local_chr_10         VARCHAR2 (1) := fnd_global.local_chr (10);
        c_local_chr_13         VARCHAR2 (1) := fnd_global.local_chr (13);
        c_local_chr_9          VARCHAR2 (1) := fnd_global.local_chr (9);
    BEGIN
        IF ec_debug.g_debug_level >= 2
        THEN
            ec_debug.push ('ECE_POO_TRANSACTION.WRITE_TO_FILE');
        END IF;

        xprogress := 'POOB-WR-1020';

        FOR i IN 1 .. idata_count
        LOOP
            xprogress := 'POOB-WR-1030';
            l_count := i;
            xprogress := 'POOB-WR-1040';

            IF     p_interface_tbl (i).record_num IS NOT NULL
               AND p_interface_tbl (i).position IS NOT NULL
               AND p_interface_tbl (i).data_length IS NOT NULL
            THEN
                xprogress := 'POOB-WR-1050';
                irow_num := i;

                IF ec_debug.g_debug_level >= 3
                THEN
                    ec_debug.pl (3, 'iRow_num : ', irow_num);
                END IF;

                xprogress := 'POOB-WR-1060';

                IF p_interface_tbl (i).interface_column_name = 'RUN_ID'
                THEN
                    cvalue := irun_id;
                ELSIF p_interface_tbl (i).interface_column_name = 'TRANSACTION_RECORD_ID'
                THEN
                    cvalue := p_foreign_key;
                ELSE
                    csrc_tbl_val_wo_newl := REPLACE (p_interface_tbl (i).VALUE, c_local_chr_10, '');
                    csrc_tbl_val_wo_frmf := REPLACE (csrc_tbl_val_wo_newl, c_local_chr_13, '');
                    csrc_tbl_val_wo_tab := REPLACE (csrc_tbl_val_wo_frmf, c_local_chr_9, '');
                    cvalue := csrc_tbl_val_wo_tab;
                END IF;

                cinsert_stmt :=
                       cinsert_stmt
                    || SUBSTRB (RPAD (NVL (cvalue, ' '), TO_CHAR (p_interface_tbl (i).data_length), ' ')
                               ,1
                               ,p_interface_tbl (i).data_length);
            -- ******************************************************
            -- the following two lines is for testing/debug purpose
            -- ******************************************************
            -- cInsert_stmt := cInsert_stmt || rpad(substrb(p_Interface_tbl(i).interface_column_name,1,p_Interface_tbl(i).data_length-2)||
            -- substrb(TO_CHAR(p_Interface_tbl(i).data_length),1,2), TO_CHAR(p_Interface_tbl(i).data_length),' ');
            END IF;

            xprogress := 'POOB-WR-1070';

            IF i < idata_count
            THEN
                xprogress := 'POOB-WR-1080';

                IF p_interface_tbl (i).record_num <> p_interface_tbl (i + 1).record_num
                THEN
                    xprogress := 'POOB-WR-1090';
                    cinsert_stmt :=
                           l_common_key
                        || LPAD (NVL (p_interface_tbl (irow_num).record_num, 0), 4, '0')
                        || RPAD (NVL (p_interface_tbl (irow_num).layout_code, ' '), 2)
                        || RPAD (NVL (p_interface_tbl (irow_num).record_qualifier, ' '), 3)
                        || cinsert_stmt;

                    xprogress := 'POOB-WR-1100';
                    UTL_FILE.put_line (ece_poo_transaction.ufile_type, cinsert_stmt);
                    xprogress := 'POOB-WR-1110';
                    cinsert_stmt := NULL;
                -- cInsert_stmt := '*' || TO_CHAR(p_Interface_tbl(i).Record_num);
                END IF;
            ELSE
                xprogress := 'POOB-WR-1120';

                /* Bug# 2108977 :- Added the following codition to prevent NULL records from causing
                                   erros */

                IF irow_num IS NOT NULL
                THEN
                    cinsert_stmt :=
                           l_common_key
                        || LPAD (NVL (p_interface_tbl (irow_num).record_num, 0), 4, '0')
                        || RPAD (NVL (p_interface_tbl (irow_num).layout_code, ' '), 2)
                        || RPAD (NVL (p_interface_tbl (irow_num).record_qualifier, ' '), 3)
                        || cinsert_stmt;

                    xprogress := 'POOB-WR-1130';
                    UTL_FILE.put_line (ece_poo_transaction.ufile_type, cinsert_stmt);
                END IF;
            END IF;
        END LOOP;

        IF ec_debug.g_debug_level >= 2
        THEN
            ec_debug.pop ('ECE_POO_TRANSACTION.WRITE_TO_FILE');
        END IF;
    EXCEPTION
        WHEN UTL_FILE.write_error
        THEN
            ec_debug.pl (0
                        ,'EC'
                        ,'ECE_UTL_WRITE_ERROR'
                        ,NULL);
            ec_debug.pl (0
                        ,'EC'
                        ,'ECE_ERROR_MESSAGE'
                        ,'ERROR_MESSAGE'
                        ,SQLERRM);
            app_exception.raise_exception;
        WHEN UTL_FILE.invalid_path
        THEN
            ec_debug.pl (0
                        ,'EC'
                        ,'ECE_UTIL_INVALID_PATH'
                        ,NULL);
            ec_debug.pl (0
                        ,'EC'
                        ,'ECE_ERROR_MESSAGE'
                        ,'ERROR_MESSAGE'
                        ,SQLERRM);
            app_exception.raise_exception;
        WHEN UTL_FILE.invalid_operation
        THEN
            ec_debug.pl (0
                        ,'EC'
                        ,'ECE_UTIL_INVALID_OPERATION'
                        ,NULL);
            ec_debug.pl (0
                        ,'EC'
                        ,'ECE_ERROR_MESSAGE'
                        ,'ERROR_MESSAGE'
                        ,SQLERRM);
            app_exception.raise_exception;
        WHEN OTHERS
        THEN
            ec_debug.pl (0
                        ,'EC'
                        ,'ECE_PROGRAM_ERROR'
                        ,'PROGRESS_LEVEL'
                        ,'ECE_POO_TRANSACTION.WRITE_TO_FILE');
            ec_debug.pl (0
                        ,'EC'
                        ,'ECE_PROGRAM_ERROR'
                        ,'PROGRESS_LEVEL'
                        ,xprogress);
            ec_debug.pl (0
                        ,'EC'
                        ,'ECE_ERROR_CODE'
                        ,'ERROR_CODE'
                        ,SQLCODE);
            ec_debug.pl (0
                        ,'EC'
                        ,'ECE_ERROR_MESSAGE'
                        ,'ERROR_MESSAGE'
                        ,SQLERRM);

            ec_debug.pl (0
                        ,'EC'
                        ,'ECE_PLSQL_POS_NOT_FOUND'
                        ,'COLUMN_NAME'
                        ,'to_char(l_count)');
            ec_debug.pl (0
                        ,'EC'
                        ,'ECE_PLSQL_VALUE'
                        ,'COLUMN_NAME'
                        ,'p_interface_tbl(l_count).value');
            ec_debug.pl (0
                        ,'EC'
                        ,'ECE_PLSQL_DATA_TYPE'
                        ,'COLUMN_NAME'
                        ,'p_interface_tbl(l_count).data_type');
            ec_debug.pl (0
                        ,'EC'
                        ,'ECE_PLSQL_COLUMN_NAME'
                        ,'COLUMN_NAME'
                        ,'p_interface_tbl(l_count).base_column');
            app_exception.raise_exception;
    END write_to_file;
END ece_poo_transaction;
/

-- End of DDL Script for Package Body APPS.ECE_POO_TRANSACTION
