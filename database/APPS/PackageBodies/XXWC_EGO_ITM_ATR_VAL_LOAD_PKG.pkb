CREATE OR REPLACE PACKAGE BODY APPS.XXWC_EGO_ITM_ATR_VAL_LOAD_PKG AS

/* *******************************************************************************
File Name				: XXWC_EGO_ITM_ATR_VAL_LOAD_PKG
PROGRAM TYPE			: PL/SQL Package spec 
PURPOSE					: Procedures and functions for uploading the item attribute values mass load via Web ADI to EBS Default Interface tables.
-- Dependencies Tables 	: ego_itm_usr_attr_intrfc
-- Dependencies Type 	: xxwc_ego_list_tab
--Pre Processing		: Related item and ICCs should be already exist

-- Post Processing 		: Call the Standard API : apps.ego_item_user_attrs_cp_pub.process_item_user_attrs_data to load the data from Interface table to Main tables
--XXWC EGO Item UDA Load - XXWC_EGO_ITEM_ATTR_VAL_LOAD
--VS: XXWC_EGO_ICC_NAME, XXWC_EGO_ITEM_STATUS

HISTORY		:
==================================================================================
VERSION DATE          AUTHOR(S)       		  DESCRIPTION
------- -----------   ----------------------  -----------------------------------------
1.0     13-JUN-2013   Rajasekar Gunasekaran	  Initial Version
1.0     20-JUN-2013   Rajasekar Gunasekaran	  Updated with unit testing fixes + validation enhancements
2.0     10-JUL-2013   Rajasekar Gunasekaran	  Added the new Proc LOAD_ITEM_ATTR_VAL_TO_MAIN CP proc 
2.1		19-JUL-2013   Rajasekar Gunasekaran	  Added the new Proc strToList for IN list conversion
2.2		24-JUL-2013   Rajasekar Gunasekaran	  Updated the Validation query to fix the validation issue - check for its parent ICC's AG as well
2.2		05-Aug-2013   Rajasekar Gunasekaran	  Enhancements:
												1. commented the Commit
2.3		12-Sep-2013   Rajasekar Gunasekaran	  Enhanced the code to work for Multi-Row AG - Features and Benefits										
*********************************************************************************/




	FUNCTION LOAD_ITEM_ATTR_VAL_TO_INTRF 
	(
	P_DATA_SET_ID                  			NUMBER DEFAULT 99999                                                                                                                                                                                    
	,P_ORG_CODE                    			VARCHAR2                                                                                                                                                                                   
	,P_ITEM_NUMBER                          VARCHAR2                                                                                                                                                                                  
	,P_REVISION                             VARCHAR2 DEFAULT NULL                                                                                                                                                                                  
	,P_AG_INT_NAME            				VARCHAR2
	,P_AG_DISP_NAME            				VARCHAR2	
	,P_AG_TYPE                      		VARCHAR2 DEFAULT 'EGO_ITEMMGMT_GROUP'
	,P_ICC_NAME								VARCHAR2	
	,P_ATTR_INT_NAME                  		VARCHAR2 
	,P_ATTR_DISP_NAME                  		VARCHAR2	
	,P_ATTR_VALUE_STR                       VARCHAR2 DEFAULT NULL                                                                                                                                                                               
	,P_ATTR_VALUE_NUM                       NUMBER DEFAULT NULL                                                                                                                                                                                       
	,P_ATTR_VALUE_DATE                      DATE DEFAULT NULL                                                                                                                                                                                         
	,P_ATTR_DISP_VALUE                      VARCHAR2                                                                                                                                                                                
	,P_TRANSACTION_TYPE                     VARCHAR2 DEFAULT 'SYNC'
	,P_DATA_LEVEL_NAME                      VARCHAR2 DEFAULT 'ITEM_LEVEL'		                                                                                                                                                                                
	,P_SOURCE_SYSTEM_ID                     NUMBER DEFAULT NULL                                                                                                                                                                                       
	,P_SOURCE_SYSTEM_REFERENCE              VARCHAR2 DEFAULT NULL                                                                                                                                                                                
	,P_ATTR_UOM_DISP_VALUE                  VARCHAR2 DEFAULT NULL                                                                                                                                                                                 
	,P_ATTR_VALUE_UOM                       VARCHAR2 DEFAULT NULL                                                                                                                                                                                  
	                                                                                                                                                                                 
	) RETURN VARCHAR2

	IS

	L_TRANSACTION_ID                        NUMBER(15);                                                                                                                                                                                  
	L_PROCESS_STATUS                        NUMBER := 1;                                                                                                                                                                                        
	L_DATA_SET_ID                  			NUMBER(15) := NVL(P_DATA_SET_ID,99999);                                                                                                                                                                                    
	L_ORG_CODE 			                    VARCHAR2(3) := NVL(TRIM(REPLACE(P_ORG_CODE,'"','')),'MST');                                                                                                                                                                                   
	L_ITEM_NUMBER                           VARCHAR2(81) := TRIM(REPLACE(P_ITEM_NUMBER,'"',''));                                                                                                                                                                                  
	L_REVISION                              VARCHAR2(3) := TRIM(REPLACE(P_REVISION,'"',''));                                                                                                                                                                                  
	L_ROW_IDENTIFIER                 		NUMBER(38) := 0;                                                                                                                                                                                   
	L_AG_INT_NAME            				VARCHAR2(30) := TRIM(REPLACE(P_AG_INT_NAME,'"','')); 
	L_AG_DISP_NAME            				VARCHAR2(30) := TRIM(REPLACE(P_AG_DISP_NAME,'"','')); 	
	L_AG_TYPE                         		VARCHAR2(30)  := NVL(TRIM(REPLACE(P_AG_TYPE,'"','')),'EGO_ITEMMGMT_GROUP'); 
	L_ATTR_INT_NAME                  		 VARCHAR2(30)  := TRIM(REPLACE(P_ATTR_INT_NAME,'"','')); 
	L_ATTR_DISP_NAME                  		 VARCHAR2(30) := TRIM(REPLACE(P_ATTR_DISP_NAME,'"','')); 	
	L_ATTR_VALUE_STR                          VARCHAR2(1000) := TRIM(REPLACE(P_ATTR_VALUE_STR,'"',''));                                                                                                                                                                                 
	L_ATTR_VALUE_NUM                          NUMBER  := P_ATTR_VALUE_NUM;                                                                                                                                                                                        
	L_ATTR_VALUE_DATE                         DATE  := P_ATTR_VALUE_DATE;                                                                                                                                                                                          
	L_ATTR_DISP_VALUE                         VARCHAR2(1000) := TRIM(REPLACE(P_ATTR_DISP_VALUE,'"',''));                                                                                                                                                                                 
	L_TRANSACTION_TYPE                        VARCHAR2(10) := NVL(TRIM(REPLACE(P_TRANSACTION_TYPE,'"','')),'SYNC'); 
	L_ICC_NAME								  VARCHAR2(100) := TRIM(REPLACE(P_ICC_NAME,'"','')); 
	l_icc_id								  NUMBER;
	L_SOURCE_SYSTEM_ID                        NUMBER := P_SOURCE_SYSTEM_ID;                                                                                                                                                                                       
	L_SOURCE_SYSTEM_REFERENCE                 VARCHAR2(255) := TRIM(REPLACE(P_SOURCE_SYSTEM_REFERENCE,'"',''));                                                                                                                                                                                  
	L_ATTR_UOM_DISP_VALUE                     VARCHAR2(25) := TRIM(REPLACE(P_ATTR_UOM_DISP_VALUE,'"',''));                                                                                                                                                                                  
	L_ATTR_VALUE_UOM                          VARCHAR2(3) := TRIM(REPLACE(P_ATTR_VALUE_UOM,'"','')); 
	L_DATA_LEVEL_NAME                         VARCHAR2(30) := NVL(TRIM(REPLACE(P_DATA_LEVEL_NAME,'"','')),'ITEM_LEVEL'); 
	L_ORGANIZATION_ID                         NUMBER(15); 
	l_ag_id									  NUMBER(15);
	l_attr_data_type						  VARCHAR2(3);
	l_ag_multi_row_flag						  VARCHAR2(5);
	--INVENTORY_ITEM_ID                       NUMBER(15)                                                                                                                                                                                    
	--ITEM_CATALOG_GROUP_ID                   NUMBER(15)                                                                                                                                                                                    
	--REVISION_ID                             NUMBER(15)                                                                                                                                                                                    
	--ATTR_GROUP_ID                           NUMBER(15)                                                                                                                                                                                    
	--REQUEST_ID                              NUMBER(15)                                                                                                                                                                                    
	--PROGRAM_APPLICATION_ID                  NUMBER(15)                                                                                                                                                                                    
	--PROGRAM_ID                              NUMBER(15)                                                                                                                                                                                    
	--PROGRAM_UPDATE_DATE                     DATE                                                                                                                                                                                          
	--CREATED_BY                              NUMBER(15)                                                                                                                                                                                    
	--CREATION_DATE                           DATE                                                                                                                                                                                          
	--LAST_UPDATED_BY                         NUMBER(15)                                                                                                                                                                                    
	--LAST_UPDATE_DATE                        DATE                                                                                                                                                                                          
	--LAST_UPDATE_LOGIN                       NUMBER(15)                                                                                                                                                                                    
	                                                                                                                                                                                 
	--CHANGE_ID                               NUMBER                                                                                                                                                                                        
	--CHANGE_LINE_ID                          NUMBER                                                                                                                                                                                        
		
	--INTERFACE_TABLE_UNIQUE_ID               NUMBER                                                                                                                                                                                        
	--PROG_INT_CHAR1                          VARCHAR2(255)                                                                                                                                                                                 
	--PROG_INT_CHAR2                          VARCHAR2(255)                                                                                                                                                                                 
	--PROG_INT_NUM1                           NUMBER                                                                                                                                                                                        
	--PROG_INT_NUM2                           NUMBER                                                                                                                                                                                        
	--PROG_INT_NUM3                           NUMBER                                                                                                                                                                                        
	--PROG_INT_NUM4                           NUMBER                                                                                                                                                                                        
	--DATA_LEVEL_ID                           NUMBER                                                                                                                                                                                                                                                                                                                                                                   
	--PK1_VALUE                               NUMBER                                                                                                                                                                                        
	--PK2_VALUE                               NUMBER                                                                                                                                                                                        
	--PK3_VALUE                               NUMBER                                                                                                                                                                                        
	--PK4_VALUE                               NUMBER                                                                                                                                                                                        
	--PK5_VALUE                               NUMBER                                                                                                                                                                                        
	--USER_DATA_LEVEL_NAME                    VARCHAR2(240)                                                                                                                                                                                 
	--BUNDLE_ID                               NUMBER      
	
	l_error_message							VARCHAR2(4000) := '';
	l_raise_error							VARCHAR2(2) := 'N';
	l_validation_num 						NUMBER := 0;
	l_validate_flag							NUMBER := 0;
	
	BEGIN

	 
	------------------------- validate the input param----------------------start-------------
	
	--validation for mandatory check
	IF(L_ITEM_NUMBER IS NULL OR L_AG_DISP_NAME IS NULL OR L_ATTR_DISP_NAME IS NULL) THEN
		l_validation_num := l_validation_num + 1;
		l_error_message := l_error_message || l_validation_num|| '. Mandatory columns missing ';
		l_raise_error := 'Y';
	END IF;
	
	--validation for mandatory value check
	IF(L_ATTR_DISP_VALUE IS NULL AND L_ATTR_VALUE_STR IS NULL AND L_ATTR_VALUE_NUM IS NULL AND L_ATTR_VALUE_DATE IS NULL) THEN
		l_validation_num := l_validation_num + 1;
		l_error_message := l_error_message || l_validation_num|| '. Attribute Value is mandatory in any one of the value column. ';
		l_raise_error := 'Y';
	END IF;
	
	--Validate whether the Org code is present in db
	BEGIN	
		SELECT organization_id INTO L_ORGANIZATION_ID
		FROM mtl_parameters 
		WHERE organization_code = L_ORG_CODE AND rownum=1;
	EXCEPTION
	WHEN NO_DATA_FOUND THEN
		l_validation_num := l_validation_num + 1;
		l_error_message := l_error_message || l_validation_num|| '. Organization Code does not exist in PDH. ';
		l_raise_error := 'Y';
	END;
	
	--Validate whether the item is present in db
	BEGIN	
		SELECT INVENTORY_ITEM_ID,item_catalog_group_id INTO L_TRANSACTION_ID,l_icc_id
			FROM MTL_SYSTEM_ITEMS_B 
			WHERE SEGMENT1 = L_ITEM_NUMBER
			AND organization_id = L_ORGANIZATION_ID;
	EXCEPTION
	WHEN NO_DATA_FOUND THEN
		l_validation_num := l_validation_num + 1;
		l_error_message := l_error_message || l_validation_num|| '. Item and Org combination does not exist in PDH. ';
		l_raise_error := 'Y';
	END;
	
	-- Generate the ICC name from Item number
	IF(l_icc_id IS NOT NULL) THEN
		BEGIN
			SELECT segment1 INTO L_ICC_NAME
			FROM mtl_item_catalog_groups
			WHERE item_catalog_group_id = l_icc_id;
		EXCEPTION
		WHEN OTHERS THEN
			l_validation_num := l_validation_num + 1;
			l_error_message := l_error_message || l_validation_num|| '. Error while generating the ICC Name. ';
			l_raise_error := 'Y';
		END;		
	END IF;
	
	-- validate whether the AG associated to the ICC
	--<Raj 24-Jul-2013> Updated the below query to fix the validation issue to check it for its parent ICC's AG as well
	BEGIN
		SELECT 1 INTO l_validate_flag
		FROM EGO_OBJ_ATTR_GRP_ASSOCS_V ag
		 -- ,mtl_item_catalog_groups icc
		WHERE --icc.item_catalog_group_id = ag.classification_code AND
		   attr_group_type = L_AG_TYPE--'EGO_ITEMMGMT_GROUP'
		  AND data_level_int_name = L_DATA_LEVEL_NAME--'ITEM_LEVEL'
		  AND enabled_code = 'Y'
		  --AND attr_group_name = 'XXWC_CATCH_BASIN_INSERTS_AG'
		  AND UPPER(attr_group_disp_name) = UPPER(L_AG_DISP_NAME)--'Catch Basin Inserts'
		 -- AND icc.segment1 = L_ICC_NAME--'CATCH BASIN INSERTS'
		  AND classification_code in (select item_catalog_group_id from mtl_item_catalog_groups_b connect by prior parent_catalog_group_id = item_catalog_group_id start with segment1 = L_ICC_NAME ) --'CATCH BASIN INSERTS'
		  AND ROWNUM = 1;
	
	EXCEPTION
	WHEN NO_DATA_FOUND THEN
		l_validation_num := l_validation_num + 1;
		l_error_message := l_error_message || l_validation_num|| '. The AG:'||L_AG_DISP_NAME||' is not associated to the ICC: ' ||L_ICC_NAME;
		l_raise_error := 'Y';
	END;	
	
/*	IF(l_validate_flag < 1) THEN
		l_validation_num := l_validation_num + 1;
		l_error_message := l_error_message || l_validation_num|| '. The AG:'||L_AG_DISP_NAME||' is not associated to the ICC: ' ||L_ICC_NAME;
		l_raise_error := 'Y';
	END IF;
*/
	
	--validate the item and icc combination exists in db
	  /* ' Item does not exist in ICC provided in Data file'
       WHERE   NOT EXISTS
                  (SELECT   1
                     FROM   mtl_system_items_b b, mtl_item_catalog_groups_b c
                    WHERE   b.segment1 = a.item_number
                            AND b.item_catalog_group_id =
                                  c.item_catalog_group_id
                            AND UPPER (icc) = c.segment1
                            AND ROWNUM = 1);
	*/
	
	-------------------------------- validate the input param----------------------end--------
	
	
	
	
	--------------------------- generate the mandatory params-----------------start-------------------
	
	-- generate the AG internal name and attr internal name
	--validate whether the attribute exist in the AG
	--IF (L_AG_INT_NAME IS NULL OR L_ATTR_INT_NAME IS NULL) THEN
		BEGIN
			SELECT ag.attr_group_id, attr.attr_group_name, attr.attr_name, attr.data_type_code, ag.multi_row_code 
				INTO l_ag_id, L_AG_INT_NAME, L_ATTR_INT_NAME, l_attr_data_type, l_ag_multi_row_flag
				FROM ego_attr_groups_v AG, ego_attrs_v attr
				WHERE ag.attr_group_name = attr.attr_group_name
					AND ag.attr_group_type = attr.attr_group_type
					AND UPPER(ag.attr_group_disp_name) = UPPER(L_AG_DISP_NAME)
					AND attr.ATTR_GROUP_TYPE = L_AG_TYPE
					AND attr.ATTR_DISPlay_NAME = L_ATTR_DISP_NAME
					AND ROWNUM = 1;
		EXCEPTION
		WHEN NO_DATA_FOUND THEN
			l_validation_num := l_validation_num + 1;
			l_error_message := l_error_message || l_validation_num|| '. AG Display name and Attribute Display name combination does not exist in PDH';
			l_raise_error := 'Y';
		END;	
	--END IF;
	
	--generate the row identifier
    --Design Constrain but not for this client - For any Multi-Row AG, user has to always enter all attributes for each row, even if it is null or empty. --alternate design to make it generic is to intro a new	row number field and make it mandatory for MultiRow AG
	IF (l_ag_multi_row_flag = 'Y') THEN
		BEGIN
		  SELECT NVL(MAX(ROW_IDENTIFIER)+1,l_ag_id||L_TRANSACTION_ID) INTO L_ROW_IDENTIFIER 
                FROM apps.ego_itm_usr_attr_intrfc
                WHERE ATTR_GROUP_INT_NAME = L_AG_INT_NAME
				and ATTR_INT_NAME = L_ATTR_INT_NAME
				and item_number = L_ITEM_NUMBER
				and organization_code = L_ORG_CODE
                AND data_set_id = L_DATA_SET_ID
                AND PROCESS_STATUS in (1,2);
		EXCEPTION
		WHEN OTHERS THEN
			l_validation_num := l_validation_num + 1;
			l_error_message := l_error_message || l_validation_num|| '. Error while generating the Row identifier for this Multi-row AG';
			l_raise_error := 'Y';
		END;
	ELSE -- if this AG is single row ag
		L_ROW_IDENTIFIER := l_ag_id||L_TRANSACTION_ID;
	END IF;

	--------------------------- generate the mandatory params-----------------end-------------------
	
	-- additional validation on item attribute value
	
	IF(l_attr_data_type = 'C') THEN -- C= Char attrs
	
		-- char length validation
		IF(LENGTHB(L_ATTR_DISP_VALUE) > 150 OR LENGTHB(L_ATTR_VALUE_STR) > 150) THEN
			l_validation_num := l_validation_num + 1;
			l_error_message := l_error_message || l_validation_num|| '. Lenght of the Attribute value is > 150 chars. ';
			l_raise_error := 'Y';
		END IF;
		
	ELSIF (l_attr_data_type = 'A') THEN

		-- char length validation
		IF(LENGTHB(L_ATTR_DISP_VALUE) > 1000) THEN
			l_validation_num := l_validation_num + 1;
			l_error_message := l_error_message || l_validation_num|| '. Lenght of the Attribute value is > 1000 chars. ';
			l_raise_error := 'Y';
		END IF;
		
	ELSIF (l_attr_data_type = 'N') THEN
	
		BEGIN
			IF(L_ATTR_DISP_VALUE IS NOT NULL) THEN
				L_ATTR_DISP_VALUE := TO_NUMBER(L_ATTR_DISP_VALUE);
			END IF;

			IF(L_ATTR_VALUE_NUM IS NOT NULL) THEN
				L_ATTR_VALUE_NUM := TO_NUMBER(L_ATTR_VALUE_NUM);
			END IF;
			
		EXCEPTION
		WHEN OTHERS THEN
			l_validation_num := l_validation_num + 1;
			l_error_message := l_error_message || l_validation_num|| '. The Attribute value given for this attribute is not an number. ';
			l_raise_error := 'Y';
		END;

	ELSIF (l_attr_data_type IN ('X','Y')) THEN
	
		BEGIN
			IF(L_ATTR_DISP_VALUE IS NOT NULL) THEN
				L_ATTR_DISP_VALUE := TO_DATE(L_ATTR_DISP_VALUE);
			END IF;

			IF(L_ATTR_VALUE_DATE IS NOT NULL) THEN
				L_ATTR_VALUE_DATE := TO_DATE(L_ATTR_VALUE_DATE);
			END IF;
			
		EXCEPTION
		WHEN OTHERS THEN
			l_validation_num := l_validation_num + 1;
			l_error_message := l_error_message || l_validation_num|| '. The Attribute value given for this attribute is not a Date value. ';
			l_raise_error := 'Y';
		END;
		
	END IF; -- end if of datatype validation
	
	
	-- raise error on any validation failure
	IF l_raise_error = 'Y' THEN
		RETURN l_error_message;
	END IF;
	
	
	-- insert into item attrbute val interface tables
	 BEGIN
               INSERT INTO ego.ego_itm_usr_attr_intrfc (
                                                           TRANSACTION_ID,
														   process_status,
														   ORGANIZATION_CODE,
                                                           data_set_id,
                                                           item_number,
                                                           row_identifier,
                                                           attr_group_int_name,
                                                           attr_int_name,
                                                           attr_value_str,
                                                           attr_value_num,
														   attr_value_date,
                                                           attr_disp_value,
                                                           transaction_type,
														   DATA_LEVEL_NAME,
                                                           organization_id,
                                                           created_by,
                                                           creation_date,
                                                           last_updated_by,
                                                           last_update_date,
                                                           last_update_login,
                                                           attr_group_type,
                                                           interface_table_unique_id
                          )
                 VALUES   (
                              L_TRANSACTION_ID,
							  L_PROCESS_STATUS,
							  L_ORG_CODE,
                              L_DATA_SET_ID,
                              L_ITEM_NUMBER,
                              L_ROW_IDENTIFIER,
                              L_AG_INT_NAME,
                              L_ATTR_INT_NAME,
                              L_ATTR_VALUE_STR, --val str
                              L_ATTR_VALUE_NUM, --val num,
							  L_ATTR_VALUE_DATE, --val date
                              L_ATTR_DISP_VALUE,
                              L_TRANSACTION_TYPE,
							  L_DATA_LEVEL_NAME,
                              null,
                              FND_PROFILE.VALUE ('USER_ID'),
                              SYSDATE,
                              FND_PROFILE.VALUE ('USER_ID'),
                              SYSDATE,
                              fnd_profile.VALUE ('LOGIN_ID'),
                              L_AG_TYPE,
                              mtl_system_items_interface_s.NEXTVAL --interface_table_unique_id
                          );
            EXCEPTION
               WHEN OTHERS
               THEN
				l_error_message := SUBSTR ( l_error_message || ' Error while inserting to interface table ' || SQLERRM ,1,2000);
				RETURN l_error_message;
            END;

           -- COMMIT;
	
	RETURN l_error_message;
	
    EXCEPTION
    WHEN OTHERS THEN
		l_error_message := SUBSTR ( l_error_message || ' --- ' || SQLERRM || ' Details: '||dbms_utility.format_error_backtrace(),1,4000);
		RETURN l_error_message;
	END LOAD_ITEM_ATTR_VAL_TO_INTRF;
 
/*
CREATE TYPE list_table AS TABLE OF VARCHAR2(32767);

CREATE OR REPLACE FUNCTION Str_to_list (p_str_list  IN  VARCHAR2)
RETURN list_table
AS
l_tab   list_table := list_table();
l_text  VARCHAR2(32767) := p_str_list || ',';
l_idx   NUMBER;
BEGIN
LOOP
l_idx := INSTR(l_text, ',');
EXIT WHEN NVL(l_idx, 0) = 0;
l_tab.extend;
l_tab(l_tab.last) := TRIM(SUBSTR(l_text, 1, l_idx - 1));
l_text := SUBSTR(l_text, l_idx + 1);
END LOOP;
RETURN l_tab;
END;
*/ 

   /********************************************************************************
   PROGRAM TYPE: PROCEDURE
   NAME: LOAD_ITEM_ATTR_VAL_TO_MAIN
   PURPOSE: Procedure to convert the item attributes
   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       		DESCRIPTION
   ------- -----------   --------------- 		-----------------------------------------
   2.0     10-JUL-2013   Rajasekar Gunasekaran	Added the new Proc LOAD_ITEM_ATTR_VAL_TO_MAIN for  
    ********************************************************************************/
PROCEDURE LOAD_ITEM_ATTR_VAL_TO_MAIN(errbuf OUT NOCOPY VARCHAR2, retcode OUT NOCOPY VARCHAR2,p_data_set_id IN VARCHAR2) 

IS

   l_user_id 		NUMBER;
   l_responsibility_id 	NUMBER;
   l_resp_application_id NUMBER;
   l_resp_name CONSTANT VARCHAR(40) := 'HDS Product Data Hub Admin - WC';
   l_user_name VARCHAR(50) := Apps.fnd_global.user_name();

BEGIN
	
	retcode := 0;
	errbuf := NULL;
	
	fnd_file.put_line(fnd_file.LOG, 'Starting the CP to load Item UDAs from Interface table to Main table for the batch id: ' ||p_data_set_id );
	 -- Deriving Ids from initalization variables
     -- to get the user id 
	 BEGIN
		IF (l_user_name IS NOT NULL) THEN
			l_user_id := Apps.FND_LOAD_UTIL.OWNER_ID(l_user_name);
			fnd_file.put_line(fnd_file.LOG, 'Initilizing the user Name: '||l_user_name);			
		ELSE
			l_user_id := Apps.FND_LOAD_UTIL.OWNER_ID('XXWC_INT_SUPPLYCHAIN');
			fnd_file.put_line(fnd_file.LOG, 'Initilizing the user Name: XXWC_INT_SUPPLYCHAIN');	
		END IF;
		
	 EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            errbuf := 'UserName ' || l_user_name || ' not defined in Oracle';
			fnd_file.put_line(fnd_file.LOG, errbuf);
            RAISE ;
         WHEN OTHERS
         THEN
            errbuf := 'Error deriving user_id for UserName - ' || l_user_name;
			fnd_file.put_line (fnd_file.LOG, errbuf);
	
            RAISE;
			
		 END;

	  -- to get the resp id and application id
      BEGIN
         SELECT responsibility_id, application_id
           INTO l_responsibility_id, l_resp_application_id
           FROM fnd_responsibility_vl
          WHERE responsibility_name = l_resp_name
            AND SYSDATE BETWEEN start_date AND NVL (end_date,
                                                    TRUNC (SYSDATE) + 1
                                                   );
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            errbuf := 'Responsibility '
               || l_resp_name
               || ' not defined in Oracle';
			   fnd_file.put_line (fnd_file.LOG, errbuf);
            RAISE ;
         WHEN OTHERS
         THEN
            errbuf :=
                  'Error deriving Responsibility_id for '
               || l_resp_name;
			   fnd_file.put_line (fnd_file.LOG, errbuf);
            RAISE ;
      END;	
		
        -- Environment Initialization
	apps.fnd_global.apps_initialize (user_id        => l_user_id, --Apps.FND_LOAD_UTIL.OWNER_ID('XXWC_INT_SUPPLYCHAIN'),
									resp_id        	=> l_responsibility_id, --EGO_PIM_DATA_LIBRARIAN, Product Information Management Data Librarian
									resp_appl_id   	=> l_resp_application_id);
                                  
	apps.ego_item_user_attrs_cp_pub.process_item_user_attrs_data (
            errbuf                           => errbuf,
            retcode                          => retcode,
            p_data_set_id                    => p_data_set_id,
            p_debug_level                    => 0,
            p_purge_successful_lines         => apps.fnd_api.g_false,
            p_initialize_error_handler       => apps.fnd_api.g_true,
            p_validate_only                  => apps.fnd_api.g_false,
            p_ignore_security_for_validate   => apps.fnd_api.g_false
         );
        -- COMMIT;
		 
        IF (retcode > 0 AND errbuf IS NOT NULL)
         THEN

            fnd_file.put_line (
               fnd_file.LOG,
                  ' Item Attributes value data load got Failed with the error :'
               || errbuf
            );
					 
          --dbms_output.put_line('errbuf: '||l_chr_errbuf);  
          --dbms_output.put_line('ret code: '||l_num_retcode); 
        END IF;
 
EXCEPTION
  WHEN OTHERS THEN
    retcode := 2;
	errbuf := SUBSTR ( errbuf || SQLCODE  || ' ERROR : ' || SQLERRM || ' Details: '||dbms_utility.format_error_backtrace(),  1,  2000);
	--DBMS_OUTPUT.put_line (errbuf);
END LOAD_ITEM_ATTR_VAL_TO_MAIN;

FUNCTION strToList(p_str_list  IN  VARCHAR2)
RETURN xxwc_ego_list_tab
AS
	l_tab   xxwc_ego_list_tab := xxwc_ego_list_tab();
	l_text  VARCHAR2(32767) := p_str_list || ',';
	l_idx   NUMBER;
BEGIN
	LOOP
	l_idx := INSTR(l_text, ',');
	EXIT WHEN NVL(l_idx, 0) = 0;
		l_tab.extend;
		l_tab(l_tab.last) := TRIM(SUBSTR(l_text, 1, l_idx - 1));
		l_text := SUBSTR(l_text, l_idx + 1);
	END LOOP;
	
RETURN l_tab;
END strToList;

END XXWC_EGO_ITM_ATR_VAL_LOAD_PKG; 