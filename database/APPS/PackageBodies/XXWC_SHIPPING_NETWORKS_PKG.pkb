CREATE OR REPLACE 
PACKAGE BODY      XXWC_SHIPPING_NETWORKS_PKG AS

/******************************************************************************
   NAME:       XXWC_SHIPPING_NETWORKS_PKG

   PURPOSE:    Process Interorg Shipments That Were Received Short And Intorg Transfer It Back to the Shipping Organization
   

   REVISIONS:
   Ver        Date         Author           Description
   ---------  -----------  ---------------  ------------------------------------
   1.0        18-FEB-2013  Lee Spitzer       1. Create the PL/SQL Package
******************************************************************************/


  /*************************************************************************
 *   Procedure : Write_Log
 *
 *   PURPOSE:   This procedure logs error message
 *   Parameter:
 *          IN
 *              p_debug_msg      -- Debug Message
 * ************************************************************************/
   -- add debug message to log table and concurrent log file
   PROCEDURE Write_Log (p_debug_level   IN NUMBER,
                        p_mod_name      IN VARCHAR2,
                        p_debug_msg     IN VARCHAR2)
   IS
      PRAGMA AUTONOMOUS_TRANSACTION;
   BEGIN
      --IF (p_debug_level >= FND_LOG.G_CURRENT_RUNTIME_LEVEL)
      --THEN
      -- Call Oracle FND_LOG API to insert the debug message
      FND_LOG.STRING (p_debug_level, p_mod_name, P_DEBUG_MSG);
      -- END IF;
      fnd_file.put_line (fnd_file.LOG, P_DEBUG_MSG);
      --DBMS_OUTPUT.put_line (P_DEBUG_MSG);
      COMMIT;
   END Write_Log;

  /*************************************************************************
 *   Procedure : Write_Error
 *
 *   PURPOSE:   This procedure logs error message
 *   Parameter:
 *          IN
 *              p_debug_msg      -- Debug Message
 * ************************************************************************/

   --add message to concurrent output file
   PROCEDURE Write_Error (p_debug_msg IN VARCHAR2)
   IS
      l_req_id          NUMBER := fnd_global.conc_request_id;
      l_err_callfrom    VARCHAR2 (75) DEFAULT G_PACKAGE_NAME ;
      l_err_callpoint   VARCHAR2 (75) DEFAULT 'START' ;
      l_distro_list VARCHAR2 (75)
            DEFAULT 'HDSOracleDevelopers@hdsupply.com'  ;
   BEGIN
      xxcus_error_pkg.xxcus_error_main_api (
         p_called_from         => l_err_callfrom,
         p_calling             => l_err_callpoint,
         p_request_id          => l_req_id,
         p_ora_error_msg       => SUBSTR (p_debug_msg, 1, 2000),
         p_error_desc          => 'Error running '|| G_PACKAGE_NAME ||' with PROGRAM ERROR',
         p_distribution_list   => l_distro_list,
         p_module              => 'INV'
      );
   END Write_Error;

   /*************************************************************************
    *   Procedure : Write_output
    *
    *   PURPOSE:   This procedure logs message into concurrent output
    *   Parameter:
    *          IN
    *              p_debug_msg      -- Debug Message
    * ************************************************************************/
   PROCEDURE Write_output (p_debug_msg IN VARCHAR2)
   IS
   BEGIN
      fnd_file.put_line (fnd_file.output, p_debug_msg);
   END Write_output;

   /*************************************************************************
    *   Procedure : Write_log_output
    *
    *   PURPOSE:   This procedure logs message into concurrent logfile
    *   Parameter:
    *          IN
    *              p_debug_msg      -- Debug Message
    * ************************************************************************/

   PROCEDURE Write_log_output (p_debug_msg IN VARCHAR2)
   IS
   BEGIN
      fnd_file.put_line (fnd_file.LOG, p_debug_msg);
   END write_log_output;
   
   PROCEDURE  MAIN_PROGRAM_CCR
              (retbuf                         OUT VARCHAR2
              ,retcode                        OUT NUMBER
              ,p_user_id                      IN NUMBER
              ,p_chart_of_account_id          IN NUMBER
              ,p_intransit_type               IN NUMBER
              ,p_fob_point                    IN NUMBER
              ,p_matl_interorg_transfer_code  IN NUMBER
              ,p_elemental_visibility_enabled IN VARCHAR2
              ,p_manual_receipt_expense       IN VARCHAR2
              ,p_routing_header_id            IN NUMBER
              ,p_transfer_charge              IN NUMBER
              ,p_internal_order_required      IN NUMBER
              ,p_interorg_receivable          IN NUMBER
              ,p_interorg_payable             IN NUMBER
              ) IS
    
    cursor c_shipping_networks is
    
        SELECT mp.organization_code from_organization_code,
               mp2.organization_code to_organization_code,
               mp.organization_id from_organization_id,
               mp2.organization_id to_organization_id,
               SYSDATE last_update_date,
               p_user_id last_updated_by,
               SYSDATE creation_date,
               p_user_id created_by,
               FND_GLOBAL.LOGIN_ID last_update_login,
               p_intransit_type intransit_type,  --1 = Direct Org , 2 = Intransit
               p_matl_interorg_transfer_code matl_interorg_transfer_code,
               p_fob_point fob_point,  --leave blank if intransit type = 1 ,otherwise intransit you need to have it as 1 or 2
               p_routing_header_id routing_header_id, --leave blank if intransity type = 1, otherewise for direct receipt = 3  
               p_internal_order_required interorg_required_flag,
               p_interorg_receivable interorg_receivables_account,
               p_interorg_payable interorg_payables_account,
               p_elemental_visibility_enabled elemental_visibilty_enabled,
               p_manual_receipt_expense manual_receipt_expense
        FROM   mtl_parameters mp,
               mtl_parameters mp2
        WHERE  (mp.attribute7 IS NOT NULL AND mp.attribute7 NOT IN ('N/A','Container'))
        AND    mp.organization_id != mp2.organization_id
        ---Only Active Orgs Based on Current System Date
        AND    EXISTS (SELECT *
                       FROM   hr_all_organization_units haou
                       WHERE  haou.organization_id = mp.organization_id
                       AND    nvl(haou.date_to, SYSDATE+1) > SYSDATE)
        AND    (mp2.attribute7 IS NOT NULL AND mp2.attribute7 NOT IN ('N/A','Container'))
        --Only Active Orgs Based on Current System Date
        AND    EXISTS (SELECT *
                       FROM   hr_all_organization_units haou
                       WHERE  haou.organization_id = mp2.organization_id
                       AND    nvl(haou.date_to, SYSDATE+1) > SYSDATE)
        and    not exists (select *   
                           from   mtl_interorg_parameters mip   
                           WHERE  mip.from_organization_id = mp.organization_id   
                           AND    mip.to_organization_id = mp2.organization_id)
        order by mp.organization_code, mp2.organization_code;
               
  
    l_intransit_inv_account NUMBER;     
    l_fob_point             NUMBER;
    l_routing_header_id   NUMBER;
    l_interorg_transfer_cr_account NUMBER;
    l_material_account NUMBER;
    l_segment1 VARCHAR2(30);
    l_segment2 VARCHAR2(30);
    l_segment3 VARCHAR2(30);
    l_segment4 VARCHAR2(30);
    l_segment5 VARCHAR2(30);
    l_segment6 VARCHAR2(30);
    l_segment7 VARCHAR2(30);
    l_status BOOLEAN;
    l_concat_segs		  VARCHAR2 (80);
    l_coa_id 			  NUMBER;
    
    BEGIN
    
      G_NAME := 'MAIN_PROGRAM_CCR';
      G_PROGRAM_NAME := G_PACKAGE_NAME||'.'||G_NAME;
  
      write_log_output('******************************************************************');
      write_log_output('Starting Program '||G_PROGRAM_NAME);
      write_log_output('******************************************************************');
      write_log_output(G_PROGRAM_NAME ||' Parameters');
      write_log_output('--------------------------------------------');
      write_log_output(G_PROGRAM_NAME ||' P_USER_ID = ' || P_USER_ID);
      write_log_output(G_PROGRAM_NAME ||' P_CHART_OF_ACCOUNT_ID = ' || P_CHART_OF_ACCOUNT_ID);
      write_log_output(G_PROGRAM_NAME ||' P_INTRANSIT_TYPE = ' || P_INTRANSIT_TYPE); 
      write_log_output(G_PROGRAM_NAME ||' P_FOB_POINT = ' || P_FOB_POINT);
      write_log_output(G_PROGRAM_NAME ||' P_MATL_INTERORG_TRANSFER_CODE = ' || P_MATL_INTERORG_TRANSFER_CODE);
      write_log_output(G_PROGRAM_NAME ||' P_ELEMENTAL_VISIBILITY_ENABLED = ' || P_ELEMENTAL_VISIBILITY_ENABLED);
      write_log_output(G_PROGRAM_NAME ||' P_MANUAL_RECEIPT_EXPENSE = ' || P_MANUAL_RECEIPT_EXPENSE);
      write_log_output(G_PROGRAM_NAME ||' P_ROUTING_HEADER_ID = ' || P_ROUTING_HEADER_ID);        
      write_log_output(G_PROGRAM_NAME ||' P_INTERNAL_ORDER_REQUIRED = ' || P_INTERNAL_ORDER_REQUIRED);        
      write_log_output(G_PROGRAM_NAME ||' P_TRANSFER_CHARGE = ' || P_TRANSFER_CHARGE);         
      write_log_output(G_PROGRAM_NAME ||' P_INTERORG_RECEIVABLE = ' || P_INTERORG_RECEIVABLE);
      write_log_output(G_PROGRAM_NAME ||' P_INTERORG_PAYABLE = ' || P_INTERORG_PAYABLE);               
              
  
      write_output('XXWC Create Shipping Network');
      write_output(' ');
      write_output('The following shipping networks were created');
      write_output(' ');
      write_output('FROM ORG |TO ORG ');
      write_output('-----------------');
      
    
      --If the intransit_type = 1 (Direct) then set fob point to NULL;
      IF p_intransit_type = 1 THEN 
            l_fob_point := NULL;
      ELSE
            l_fob_point := p_fob_point;
      END IF;
      
      write_log_output(G_PROGRAM_NAME ||' L_FOB_POINT = ' || L_FOB_POINT);
      
      --If the intransit type = 1 (Direct) then set receiving_header_id to NULL;
      IF p_intransit_type = 1 THEN
          
          l_routing_header_id := NULL;
          
      ELSE
      
          l_routing_header_id := p_routing_header_id;
          
      END IF;
      
      
      write_log_output(G_PROGRAM_NAME ||' L_ROUTING_HEADER_ID = ' || L_ROUTING_HEADER_ID);  
      
      
        FOR r_shipping_networks IN c_shipping_networks
        
        LOOP
        --If the FOB Point is null or 2 (Direct or Intransit at Shipment, then set the intransit inventory account to the shipping branch inventory account      
          IF nvl(l_fob_point,2) = 2 THEN
          
              BEGIN
              
                SELECT intransit_inv_account
                INTO   l_intransit_inv_account
                FROM   mtl_parameters
                WHERE  organization_id = r_shipping_networks.from_organization_id;
              
              EXCEPTION
              
                  WHEN others THEN
                      write_output('Error - check log file for more details');
                      g_message := SQLCODE || SQLERRM || 'error finding intransit value account for organization ' || r_shipping_networks.from_organization_code;
                      raise g_exception;
              
              END;
              
              
              if l_intransit_inv_account IS NULL then
              
                    write_output('Error - check log file for more details');
                    g_message := 'Please setup the intransit value account for organization ' || r_shipping_networks.from_organization_code;
                    raise g_exception;
               
               END IF;
               
          
          ELSE
          
          
              BEGIN
              
                SELECT intransit_inv_account
                INTO   l_intransit_inv_account
                FROM   mtl_parameters
                WHERE  organization_id = r_shipping_networks.to_organization_id;
              
              EXCEPTION
              
                WHEN OTHERS THEN
                      write_output('Error - check log file for more details');
                      g_message := SQLCODE || SQLERRM || ' error finding intransit value account for organization ' || r_shipping_networks.to_organization_code;
                      raise g_exception;
              
              END;
              
              
              if l_intransit_inv_account IS NULL then
                    
                    write_output('Error - check log file for more details');
                    g_message := 'Please setup the intransit value account for organization ' || r_shipping_networks.to_organization_code;
                    raise g_exception;
               
               END IF;
               
          END IF;
          
          --Get the Interorg_transfer_cr_account from the shipping org if it's null then build the string
          BEGIN
          
                SELECT interorg_transfer_cr_account, material_account
                INTO   l_interorg_transfer_cr_account, l_material_account
                FROM   mtl_parameters
                where  organization_id = r_shipping_networks.from_organization_id;
          
          EXCEPTION
          
               WHEN NO_DATA_FOUND THEN
               
                  l_interorg_transfer_cr_account := NULL;
                  
                WHEN OTHERS THEN
                
                    g_message := SQLCODE || SQLERRM || ' when others error message getting interorg_transfer_cr_account for org '|| r_shipping_networks.from_organization_code;
          END;
          
          
          --if the interog transfer cr account is null then get the material account from the shipping branch and find the CCID 
          IF l_interorg_transfer_cr_account IS NULL THEN
          
              BEGIN
                    SELECT segment1,
                           segment2,
                           segment3,
                           segment4,
                           segment5,
                           segment6,
                           segment7
                     INTO  l_segment1,
                           l_segment2,
                           l_segment3,
                           l_segment4,
                           l_segment5,
                           l_segment6,
                           l_segment7
                     FROM  gl_code_combinations
                     WHERE code_combination_id = l_material_account;
                     
              EXCEPTION
              
                  WHEN OTHERS THEN
                      write_output('Error - check log file for more details');
                      g_message := SQLCODE || SQLERRM || ' could not find the material account for org ' || r_shipping_networks.from_organization_code || ' CCID ' || l_material_account; 
                      raise g_exception;
              END;
              
        
        
              --Find the CCID 
              
            BEGIN
             l_concat_segs :=
                 l_segment1
              || '.'
              || l_segment2
              || '.'
              || l_segment3
              || '.'
              || p_transfer_charge
              || '.'
              || l_segment5
              || '.'
              || l_segment6
              || '.'
              || l_segment7;
        
             l_status	 :=
              fnd_flex_keyval.validate_segs ('CREATE_COMBINATION'    --operation
                             , 'SQLGL'
                             --appl_short_name
                             , 'GL#'
                             --key_flex_code
                             , p_chart_of_account_id
                             --structure_number
                             , l_concat_segs
                             --concat_segments
                             , 'V'
                             --values_or_ids
                             , SYSDATE
                             --validation_date
                             , 'ALL'
                             --displayable
                             , NULL
                             --data_set
                             , NULL
                             --vrule
                             , NULL
                             --where_clause
                             , NULL
                             --get_columns
                             , FALSE
                             --allow_nulls
                             , FALSE
                             --allow_orphans
                             , NULL
                             --allow_orphans
                             , NULL
                             --resp_id
                             , NULL
                             --user_id
                             , NULL
                             --select_comb_from_view
                             , NULL
                             --no_combmsg
                             , NULL--where_clause_msg
                               );
        
                IF l_status THEN
                    
                        l_interorg_transfer_cr_account	:= fnd_flex_keyval.combination_id ();
                END IF;
            
              EXCEPTION
              
                WHEN OTHERS THEN
                    write_output('Error - check log file for more details');
                    g_message := SQLCODE || SQLERRM || ' could not find the transfer charge account for org ' || r_shipping_networks.from_organization_code || ' transfer charge ' || p_transfer_charge; 
                    raise g_exception;
              
              END;
              
              
              IF  l_interorg_transfer_cr_account IS NULL THEN
                    
                    write_output('Error - check log file for more details');
                    g_message := 'Could not find or create the transfer charge account for org ' || r_shipping_networks.from_organization_code || ' transfer charge ' || p_transfer_charge; 
                    raise g_exception;
              
              END IF;

			
            
            END IF;
     
     
      --Insert into MTL_INTERORG_PARAMETERS TABLE
      BEGIN          
          INSERT INTO mtl_interorg_parameters 	
             (FROM_ORGANIZATION_ID	
             ,TO_ORGANIZATION_ID	
             ,LAST_UPDATE_DATE	
             ,LAST_UPDATED_BY	
             ,CREATION_DATE	
             ,CREATED_BY	
             ,LAST_UPDATE_LOGIN	
             ,INTRANSIT_TYPE	
             --,DISTANCE_UOM_CODE	
             ,MATL_INTERORG_TRANSFER_CODE	
             ,FOB_POINT	
             ,ROUTING_HEADER_ID	
             ,INTERNAL_ORDER_REQUIRED_FLAG	
             ,INTRANSIT_INV_ACCOUNT	
             --,INTERORG_TRNSFR_CHARGE_PERCENT	
             ,INTERORG_TRANSFER_CR_ACCOUNT	
             ,INTERORG_RECEIVABLES_ACCOUNT	
             ,INTERORG_PAYABLES_ACCOUNT	
             --,INTERORG_PRICE_VAR_ACCOUNT	
             ,ELEMENTAL_VISIBILITY_ENABLED	
             ,MANUAL_RECEIPT_EXPENSE)	
             VALUES
             (R_SHIPPING_NETWORKS.FROM_ORGANIZATION_ID--,	--FROM_ORGANIZATION_ID
             ,R_SHIPPING_NETWORKS.TO_ORGANIZATION_ID--,	 --TO_ORGANIZATION_ID
             ,SYSDATE ---LAST_UPDATE_DATE,	
             ,P_USER_ID --LAST_UPDATED_BY,	
             ,SYSDATE --CREATION_DATE,	
             ,P_USER_ID -- CREATED_BY,	
             ,-1 --LAST_UPDATE_LOGIN,	
             ,P_INTRANSIT_TYPE --INTRANSIT_TYPE,	
             --,NULL -- DISTANCE_UOM_CODE,	
             ,P_MATL_INTERORG_TRANSFER_CODE --MATL_INTERORG_TRANSFER_CODE,	
             ,L_FOB_POINT --FOB_POINT,	
             ,L_ROUTING_HEADER_ID -- ROUTING_HEADER_ID,	
             ,P_INTERNAL_ORDER_REQUIRED --INTERNAL_ORDER_REQUIRED_FLAG,	
             ,L_INTRANSIT_INV_ACCOUNT --INTRANSIT_INV_ACCOUNT,	
             --,NULL --INTERORG_TRNSFR_CHARGE_PERCENT,	
             ,L_INTERORG_TRANSFER_CR_ACCOUNT --INTERORG_TRANSFER_CR_ACCOUNT,	
             ,P_INTERORG_RECEIVABLE --INTERORG_RECEIVABLES_ACCOUNT,	
             ,P_INTERORG_PAYABLE --INTERORG_PAYABLES_ACCOUNT,	
             --,NULL --INTERORG_PRICE_VAR_ACCOUNT,	
             ,P_ELEMENTAL_VISIBILITY_ENABLED --ELEMENTAL_VISIBILITY_ENABLED,	
             ,P_MANUAL_RECEIPT_EXPENSE --MANUAL_RECEIPT_EXPENSE)	
             );
      
      EXCEPTION
      
          WHEN OTHERS THEN
            
              ROLLBACK;    
              write_output('Error creating shipping network between ' || R_SHIPPING_NETWORKS.FROM_ORGANIZATION_CODE||' and '||R_SHIPPING_NETWORKS.TO_ORGANIZATION_CODE);
              g_message := SQLCODE || SQLERRM || ' Could not insert into mtl_interorg_parameters';
              raise g_exception;
      
      

      END;
    
    
      write_output(R_SHIPPING_NETWORKS.FROM_ORGANIZATION_CODE||'      |'||R_SHIPPING_NETWORKS.TO_ORGANIZATION_CODE);
        
      COMMIT;
          
    END LOOP;        
    
    
    
    
    
    
    RETCODE := 0;
    
    
    EXCEPTION
    
        WHEN G_EXCEPTION THEN
        
         write_log_output('******************************************************************');
         write_log_output (g_message);
         write_log_output('Ending Program '||G_PROGRAM_NAME);
         write_log_output('******************************************************************');
          
         RETCODE := 2;
         
         
        WHEN OTHERS THEN
        
         g_message := SQLCODE || SQLERRM || ' When others error in program ' ||  G_PROGRAM_NAME;
         write_log_output('******************************************************************');
         write_log_output (g_message);
         write_log_output('Ending Program '||G_PROGRAM_NAME);
         write_log_output('******************************************************************');
        
         RETCODE := 2;
    
    
    END MAIN_PROGRAM_CCR;
    
END XXWC_SHIPPING_NETWORKS_PKG;