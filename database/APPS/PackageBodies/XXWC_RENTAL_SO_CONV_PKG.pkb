CREATE OR REPLACE PACKAGE BODY XXWC_RENTAL_SO_CONV_PKG AS
   /*************************************************************************
   *   $Header XXWC_RENTAL_SO_CONV_PKG.PKG $
   *   Module Name: XXWC_RENTAL_SO_CONV_PKG.PKG
   *
   *   PURPOSE:   This package is used by the Rental SalesOrder Conversion
   *
   *   REVISIONS:
   *   Ver        Date        Author                  Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        08/30/2012  Gopi Damuluri           Initial Version   
   *   2.0        01/11/2013  Gopi Damuluri           Added logic to ignore Authorized Buyer Contacts
   *   2.1        04/22/2013  Lucidity CG             Added Shipping Method to conversion logic (task 20130408-03457)
   *   2.2        04/13/2018  Ashwin Sridhar          TMS#20180328-00041--AH HARRIS Rental Conversion 
   * ***************************************************************************/
g_default_contact VARCHAR2(200) := 'SYSNAME';


   /*************************************************************************
   *   PROCEDURE Name: create_contact
   *
   *   PURPOSE:   To create contact when contact does not exist in Oracle
   *
   * ***************************************************************************/
   PROCEDURE create_contact (p_customer_id   IN NUMBER
                           , p_first_name    IN VARCHAR2
                           , p_last_name     IN VARCHAR2
                           , p_cust_contact_id OUT NUMBER
                           , p_error_message OUT VARCHAR2)
   IS
     l_party_id                   NUMBER;
     p_create_person_rec HZ_PARTY_V2PUB.person_rec_type;
     x_party_id NUMBER;
     x_party_number VARCHAR2(2000);
     x_profile_id NUMBER;
     x_return_status VARCHAR2(2000);
     x_msg_count NUMBER;
     x_msg_data VARCHAR2(2000);

     p_org_contact_rec HZ_PARTY_CONTACT_V2PUB.ORG_CONTACT_REC_TYPE;
     x_org_contact_id NUMBER;
     x_party_rel_id NUMBER;
     x_party_id2 NUMBER;
     x_party_number2 VARCHAR2(2000);

     p_cr_cust_acc_role_rec HZ_CUST_ACCOUNT_ROLE_V2PUB.cust_account_role_rec_type;
     x_cust_account_role_id NUMBER;
     l_exception            EXCEPTION;
   BEGIN
    ------------------------------------
    -- Derive Customer Party Id
    ------------------------------------
    SELECT party_id
      INTO l_party_id
      FROM hz_cust_accounts_all hca
     WHERE cust_account_id = p_customer_id;

    ------------------------------------
    -- 1. Create a definition contact
    ------------------------------------
--     p_create_person_rec.person_pre_name_adjunct := 'MR.';
     IF p_first_name IS NULL AND p_last_name IS NULL THEN
        p_create_person_rec.person_first_name := g_default_contact;
        p_create_person_rec.person_last_name  := g_default_contact;
     ELSE
        p_create_person_rec.person_first_name := p_first_name;
        p_create_person_rec.person_last_name  := p_last_name;
     END IF;
     p_create_person_rec.created_by_module := 'TCA_V1_API';

     HZ_PARTY_V2PUB.create_person('T',
     p_create_person_rec,
     x_party_id,
     x_party_number,
     x_profile_id,
     x_return_status,
     x_msg_count,
     x_msg_data);

     IF x_return_status != 'S' THEN
       p_error_message := x_msg_data;
       RAISE l_exception;
     END IF;

    ------------------------------------
    -- 2. Create a relation cont-org using party_id from step 7 and party_id from step 0
    ------------------------------------
        p_org_contact_rec.created_by_module                := 'TCA_V1_API';
        p_org_contact_rec.party_rel_rec.subject_id         := x_party_id; --<<value for party_id from step 1>
        p_org_contact_rec.party_rel_rec.subject_type       := 'PERSON';
        p_org_contact_rec.party_rel_rec.subject_table_name := 'HZ_PARTIES';
        p_org_contact_rec.party_rel_rec.object_id          := l_party_id; --<<value for party_id from step 0>
        p_org_contact_rec.party_rel_rec.object_type        := 'ORGANIZATION';
        p_org_contact_rec.party_rel_rec.object_table_name  := 'HZ_PARTIES';
        p_org_contact_rec.party_rel_rec.relationship_code  := 'CONTACT_OF';
        p_org_contact_rec.party_rel_rec.relationship_type  := 'CONTACT';
        p_org_contact_rec.party_rel_rec.start_date         := SYSDATE;

        hz_party_contact_v2pub.create_org_contact('T',
        p_org_contact_rec,
        x_org_contact_id,
        x_party_rel_id,
        x_party_id2,
        x_party_number,
        x_return_status,
        x_msg_count,
        x_msg_data);

     IF x_return_status != 'S' THEN
       p_error_message := x_msg_data;
       RAISE l_exception;
     END IF;

    ------------------------------------
    -- 3. Create a contact using party_id you get in step 8 and cust_account_id from step 0
    ------------------------------------
        p_cr_cust_acc_role_rec.party_id          := x_party_id2; --<<value for party_id from step 2>
        p_cr_cust_acc_role_rec.cust_account_id   := p_customer_id; --<<value for cust_account_id from step 0>
        --p_cr_cust_acc_role_rec.primary_flag    := 'Y';
        p_cr_cust_acc_role_rec.role_type         := 'CONTACT';
        p_cr_cust_acc_role_rec.created_by_module := 'TCA_V1_API';

        HZ_CUST_ACCOUNT_ROLE_V2PUB.create_cust_account_role(
        'T',
        p_cr_cust_acc_role_rec,
        x_cust_account_role_id,
        x_return_status,
        x_msg_count,
        x_msg_data);

     IF x_return_status != 'S' THEN
       p_error_message := x_msg_data;
       RAISE l_exception;
     ELSE
       p_cust_contact_id := x_cust_account_role_id;
     END IF;

   EXCEPTION
   WHEN l_exception THEN
     fnd_file.put_line(fnd_file.log,'p_customer_id - '||p_customer_id||' in L_EXCEPTION, p_error_message - '||p_error_message);
   WHEN OTHERS THEN
     ROLLBACK;
     fnd_file.put_line(fnd_file.log,'p_customer_id - '||p_customer_id||' error in  CREATE_CONTACT procedure - '||SQLERRM);
   END create_contact;

   /*************************************************************************
   *   PROCEDURE Name: load_interface
   *
   *   PURPOSE:   To validate Rental Staging data and load the same into 
   *              Interface table.
   * ***************************************************************************/
   PROCEDURE load_interface (p_errbuf                 OUT  VARCHAR2,
                             p_retcode                OUT  NUMBER,
                             p_validate_only           IN  VARCHAR2
                             )
   IS

   CURSOR cur_so_hdr
       IS
   SELECT DISTINCT stg.order_type
        , stg.legacy_party_site_number
        , stg.cust_po_number
         , lpad(stg.warehouse,3,'0') warehouse
        , ship_to_contact ship_to_contact_FN
        , ship_to_contact_LN
        , bill_to_contact bill_to_contact_FN
        , bill_to_contact_LN
        , prism_sales_order_num
        , shipping_method                           -- rev 2.1 4/22/2013  task 20130408-03457
     FROM xxwc.xxwc_rental_so_conv_tbl stg
    WHERE 1 = 1
      AND stg.status IN ('V', 'N');

   CURSOR cur_so_lines(p_prism_sales_order_num    VARCHAR2)
       IS
   SELECT DISTINCT stg.item_number
        , stg.quantity
        , stg.rental_charge
        , stg.rental_start_date
        , stg.rowid
     FROM xxwc.xxwc_rental_so_conv_tbl stg
    WHERE 1 = 1
--      AND status IN ('V', 'N')
      AND stg.prism_sales_order_num         = p_prism_sales_order_num;


   l_order_source_id            NUMBER          := 0; -- Online
   l_shipping_method_code       VARCHAR2(50)    := '000001_WILL CALL_P_LTL';
   l_err_flag                   VARCHAR2(1)     := 'N';
   g_org_id                     NUMBER          := 162; --FND_PROFILE.VALUE('ORG_ID');
   l_order_type                 oe_transaction_types_tl.name%TYPE;
   l_cust_contact_id            NUMBER;
   l_inv_org_id                 NUMBER;
   l_ship_to_site_use_id        NUMBER;
   l_customer_id                NUMBER;
   l_bill_to_site_use_id        NUMBER;
   l_party_site_id              NUMBER;
   l_inv_item_id                NUMBER;
   l_line_number                NUMBER;
   l_order_cnt                  NUMBER := 0;
   l_error_message              VARCHAR2 (2000);
   l_line_error_message         VARCHAR2 (2000);
   l_ship_to_found              VARCHAR2(1);
   l_tax_exempt_reason_code     VARCHAR2(50) := 'RESALE';
   l_attribute1                 VARCHAR2(240);
   l_attribute2                 VARCHAR2(240);
   l_exception                  EXCEPTION;
   ln_credit_chk                VARCHAR2(1); --Added by Ashwin.S on 16-Apr-2018 for TMS#20180328-00041

   BEGIN
     fnd_file.put_line(fnd_file.log,'----------------------------------------------------------------------------------');
     fnd_file.put_line(fnd_file.log,'Start of Procedure : LOAD_INTERFACE');
     fnd_file.put_line(fnd_file.log,'----------------------------------------------------------------------------------');

     ----------------------------------------------------------------------------------
     --
     ----------------------------------------------------------------------------------
     FOR rec_so_hdr IN cur_so_hdr LOOP

       --Added the debug messages by Ashwin.S on 13-Apr-2018 for TMS#20180328-00041 
       fnd_file.put_line(fnd_file.log,'Prism Sales Order Number '||rec_so_hdr.prism_sales_order_num);
       fnd_file.put_line(fnd_file.log,'Order Type '||rec_so_hdr.order_type);
       fnd_file.put_line(fnd_file.log,'Legacy Party Site number '||rec_so_hdr.legacy_party_site_number);
       fnd_file.put_line(fnd_file.log,'Shipping Method '||rec_so_hdr.shipping_method);
       fnd_file.put_line(fnd_file.log,'Warehouse '||rec_so_hdr.warehouse);
       fnd_file.put_line(fnd_file.log,'Customer PO Number '||rec_so_hdr.cust_po_number);
     
        l_error_message := '.';
        l_err_flag      := 'N';
        l_order_cnt     := l_order_cnt + 1;

        BEGIN -- rec_so_hdr LOOP BEGIN
        ----------------------------------------------------------------------------------
        -- Derive Order Type
        ----------------------------------------------------------------------------------
        l_order_type := NULL;
        l_attribute1 := NULL;
        IF rec_so_hdr.order_type = 'L' THEN
           l_order_type := 'WC LONG TERM RENTAL';
           l_attribute1 := 'Long Term';
        ELSIF rec_so_hdr.order_type = 'S' THEN
           l_order_type := 'WC SHORT TERM RENTAL';
           l_attribute1 := 'Short Term';
        ELSE
          l_err_flag   := 'Y';
          l_error_message := l_error_message || '-- Invalid Order Type';
        END IF;

        ----------------------------------------------------------------------------------
        -- Derive Inventory Org Id
        ----------------------------------------------------------------------------------
        l_inv_org_id := NULL;
        BEGIN
          SELECT organization_id
            INTO l_inv_org_id
            FROM org_organization_definitions  ood
           WHERE organization_code = LPAD (rec_so_hdr.warehouse, 3, '0');
        EXCEPTION
        WHEN OTHERS THEN
          l_err_flag   := 'Y';
          l_error_message := l_error_message || '-- Invalid Warehouse';
        END;

        l_ship_to_site_use_id     := NULL;
        l_customer_id             := NULL;
        l_bill_to_site_use_id     := NULL;
        l_party_site_id           := NULL;
        l_ship_to_found           := 'N';
        ----------------------------------------------------------------------------------
        -- Derive Ship-To Address using Prism PartySiteNumber - Validate Customer Site
        ----------------------------------------------------------------------------------
        BEGIN
          SELECT hcsu.site_use_id
               , hcas.cust_account_id
               , hcsu.bill_to_site_use_id
               , hcas.party_site_id
            INTO l_ship_to_site_use_id
               , l_customer_id
               , l_bill_to_site_use_id
               , l_party_site_id
            FROM hz_cust_acct_sites_all   hcas
               , hz_cust_site_uses_all    hcsu
           WHERE 1                           = 1
             AND hcas.status                 = 'A'
             AND hcsu.status                 = 'A'
             AND hcas.attribute17            = rec_so_hdr.legacy_party_site_number
             AND hcas.cust_acct_site_id      = hcsu.cust_acct_site_id
             AND hcsu.site_use_code          = 'SHIP_TO'
             AND hcas.org_id                 = g_org_id;

          l_ship_to_found := 'Y';
        EXCEPTION
        WHEN NO_DATA_FOUND THEN
          -- Check for the case with an appended Ship-To Site
          BEGIN
            SELECT hcsu.site_use_id
                 , hcas.cust_account_id
                 , hcsu.bill_to_site_use_id
                 , hcas.party_site_id
              INTO l_ship_to_site_use_id
                 , l_customer_id
                 , l_bill_to_site_use_id
                 , l_party_site_id
              FROM hz_cust_acct_sites_all   hcas
                 , hz_cust_site_uses_all    hcsu
             WHERE 1                           = 1
               AND hcas.status                 = 'A'
               AND hcsu.status                 = 'A'
               AND SUBSTR(hcas.attribute17,1,INSTR(hcas.attribute17,'-') - 1) = rec_so_hdr.legacy_party_site_number
               AND hcas.cust_acct_site_id      = hcsu.cust_acct_site_id
               AND hcsu.site_use_code          = 'SHIP_TO'
               AND hcas.org_id                 = g_org_id;

            l_ship_to_found := 'Y';
          EXCEPTION
          WHEN NO_DATA_FOUND THEN
              l_ship_to_found := 'N';
          WHEN TOO_MANY_ROWS THEN
            l_err_flag   := 'Y';
            l_error_message := l_error_message || '-- Multiple Oracle Sites have same PRISM Party Site';
          WHEN OTHERS THEN
            l_err_flag   := 'Y';
            l_error_message := l_error_message || '-- Error deriving SHIP-TO Site details';
          END;
        WHEN TOO_MANY_ROWS THEN
          l_err_flag   := 'Y';
          l_error_message := l_error_message || '-- Multiple Oracle Sites have same PRISM Party Site';
        WHEN OTHERS THEN
          l_err_flag   := 'Y';
          l_error_message := l_error_message || '-- Error deriving SHIP-TO Site details';
        END;

        ----------------------------------------------------------------------------------
        -- Derive Ship-To Address using Prism PartySiteNumber - Validate Party Site
        ----------------------------------------------------------------------------------
        IF l_ship_to_found = 'N' THEN
          BEGIN
            SELECT hcsu.site_use_id
                 , hcas.cust_account_id
                 , hcsu.bill_to_site_use_id
                 , hcas.party_site_id
              INTO l_ship_to_site_use_id
                 , l_customer_id
                 , l_bill_to_site_use_id
                 , l_party_site_id
              FROM hz_cust_acct_sites_all   hcas
                 , hz_cust_site_uses_all    hcsu
                 , hz_party_sites           hps
             WHERE 1                           = 1
               AND hcas.status                 = 'A'
               AND hcsu.status                 = 'A'
               AND hps.status                  = 'A'
               AND hps.party_site_number       = rec_so_hdr.legacy_party_site_number
               AND hcas.cust_acct_site_id      = hcsu.cust_acct_site_id
               AND hcas.party_site_id          = hps.party_site_id
               AND hcsu.site_use_code          = 'SHIP_TO'
               AND hcas.org_id                 = g_org_id;

            l_ship_to_found := 'Y';
          EXCEPTION
          WHEN NO_DATA_FOUND THEN
            -- Check for the case with an appended Ship-To Site
            BEGIN
            SELECT hcsu.site_use_id
                 , hcas.cust_account_id
                 , hcsu.bill_to_site_use_id
                 , hcas.party_site_id
              INTO l_ship_to_site_use_id
                 , l_customer_id
                 , l_bill_to_site_use_id
                 , l_party_site_id
              FROM hz_cust_acct_sites_all   hcas
                 , hz_cust_site_uses_all    hcsu
                 , hz_party_sites           hps
             WHERE 1                           = 1
               AND hcas.status                 = 'A'
               AND hcsu.status                 = 'A'
               AND hps.status                  = 'A'
               AND SUBSTR(hps.party_site_number,1,INSTR(hps.party_site_number,'-') - 1) = rec_so_hdr.legacy_party_site_number
               AND hcas.cust_acct_site_id      = hcsu.cust_acct_site_id
               AND hcas.party_site_id          = hps.party_site_id
               AND hcsu.site_use_code          = 'SHIP_TO'
               AND hcas.org_id                 = g_org_id;

            l_ship_to_found := 'Y';
            EXCEPTION
            WHEN NO_DATA_FOUND THEN
              l_err_flag   := 'Y';
              l_error_message := l_error_message || '-- PRISM Party Site not in Oracle';
            WHEN TOO_MANY_ROWS THEN
              l_err_flag   := 'Y';
              l_error_message := l_error_message || '-- Multiple Oracle Sites have same PRISM Party Site';
            WHEN OTHERS THEN
              l_err_flag   := 'Y';
              l_error_message := l_error_message || '-- Error deriving SHIP-TO Site details';
            END;
          WHEN TOO_MANY_ROWS THEN
            l_err_flag   := 'Y';
            l_error_message := l_error_message || '-- Multiple Oracle Sites have same PRISM Party Site';
          WHEN OTHERS THEN
            l_err_flag   := 'Y';
            l_error_message := l_error_message || '-- Error deriving SHIP-TO Site details';
          END;
        END IF;

        ----------------------------------------------------------------------------------
        -- Derive Contact Details (12-Jan-2013 Added a condition to ignore Contacts defined
        -- with RoleTypes Eg:Authorized Buyer Contacts)
        ----------------------------------------------------------------------------------
        l_cust_contact_id  := NULL;
        BEGIN
          SELECT cust_account_role_id
            INTO l_cust_contact_id
            FROM hz_cust_account_roles hcar
           WHERE 1 = 1
             AND hcar.cust_account_id     = l_customer_id
             AND hcar.role_type           = 'CONTACT'
             AND hcar.current_role_state = 'A'
             AND hcar.cust_acct_site_id IS NULL
             AND hcar.last_update_date IN (SELECT MAX(hcar_s.last_update_date)
                                             FROM hz_cust_account_roles hcar_s
                                            WHERE 1 = 1
                                              AND hcar_s.cust_account_id     = l_customer_id
                                              AND hcar_s.role_type           = 'CONTACT'
                                              AND hcar.cust_acct_site_id IS NULL
                                              AND hcar_s.current_role_state = 'A')
             AND not exists (select '1'
                              from ar_contacts_v cont
                                 , hz_role_responsibility cont_role
                              where 1 =1
                                and cont.contact_id = cont_role.cust_account_role_id
                                and cont.customer_id = l_customer_id)                                              
             AND rownum = 1;

        EXCEPTION
        WHEN NO_DATA_FOUND THEN
          create_contact(l_customer_id
                       , rec_so_hdr.ship_to_contact_FN
                       , rec_so_hdr.ship_to_contact_LN
                       , l_cust_contact_id
                       , l_error_message);

          IF l_error_message IS NOT NULL THEN
            l_err_flag   := 'Y';
            l_error_message := l_error_message || '-- Customer has no contacts';
          END IF;

        WHEN OTHERS THEN
          l_err_flag   := 'Y';
          l_error_message := l_error_message || '-- Error deriving contact details';
        END;

     -- start  -- rev 2.1 4/22/2013  task 20130408-03457
     -- validate shipping_method
     IF rec_so_hdr.shipping_method IS NOT NULL THEN
       BEGIN
         SELECT wcs.ship_method_code
           INTO l_shipping_method_code
           FROM wsh_carrier_services_v wcs
          WHERE wcs.ship_method_meaning = rec_so_hdr.shipping_method
            AND wcs.enabled_flag = 'Y';
       EXCEPTION
         WHEN NO_DATA_FOUND THEN
           l_shipping_method_code := NULL;
           l_err_flag   := 'Y';
           l_error_message := l_error_message || '-- Invalid shipping_method';
       END;
     ELSE
       l_shipping_method_code := NULL;
     END IF;
     -- end   -- rev 2.1 4/22/2013  task 20130408-03457

    --Added the logic by Ashwin.S on 16-Apr-2018 for TMS#20180328-00041
    BEGIN 

      SELECT hcp.credit_hold
      INTO  ln_credit_chk
      FROM  hz_cust_acct_sites_all   hcas
      ,     hz_cust_site_uses_all    hcsu
      ,     HZ_PARTY_SITES           HPS
      ,     HZ_CUSTOMER_PROFILES     hcp
      WHERE 1                           = 1
      AND   hcas.status                 = 'A'
      AND   hcsu.status                 = 'A'
      AND   HPS.STATUS                  = 'A'
      AND   hps.party_site_number       = rec_so_hdr.legacy_party_site_number
      AND   hcas.cust_acct_site_id      = hcsu.cust_acct_site_id
      AND   HCAS.PARTY_SITE_ID          = HPS.PARTY_SITE_ID
      AND   HCSU.SITE_USE_CODE          = 'BILL_TO'
      AND   HCSU.PRIMARY_FLAG           ='Y'
      AND   HCP.SITE_USE_ID             = HCSU.SITE_USE_ID
      AND   hcp.cust_account_id         = hcas.cust_account_id
      AND   hcas.org_id                 = g_org_id;

    EXCEPTION
    WHEN others THEN
    
      ln_credit_chk:='N';

    END;
     
    --Added the logic by Ashwin.S on 16-Apr-2018 for TMS#20180328-00041
    IF ln_credit_chk='Y' THEN
    
      l_err_flag:='Y';
      l_error_message := l_error_message || '-- Cannot create order for this customer as we have credit hold for this...';

    END IF;

      ---------------------------------------------------------------------------------
      -- Insert into OE_HEADERS_IFACE_ALL table
      ----------------------------------------------------------------------------------
     BEGIN

     IF p_validate_only = 'N' AND l_err_flag   != 'Y' THEN
     
       --Added the debug messages by Ashwin.S on 13-Apr-2018 for TMS#20180328-00041 
       fnd_file.put_line(fnd_file.log,'Inside Insert condition for headers...');
       fnd_file.put_line(fnd_file.log,'p_validate_only  '||p_validate_only);
       fnd_file.put_line(fnd_file.log,'l_err_flag '||l_err_flag);
       
        INSERT INTO OE_HEADERS_IFACE_ALL(ORDER_SOURCE_ID,
                 CUSTOMER_PO_NUMBER,
                 SHIPPING_METHOD_CODE,
                 INVOICE_TO_CONTACT_ID,
                 SHIP_TO_CONTACT_ID,
                 SOLD_TO_CONTACT_ID,
                 ORIG_SYS_DOCUMENT_REF,
                 ORDERED_DATE,
                 ORDER_TYPE,
                 CUSTOMER_ID,
                 CREATED_BY,
                 CREATION_DATE,
                 LAST_UPDATED_BY,
                 LAST_UPDATE_DATE,
                 SOLD_TO_ORG_ID,
                 SHIP_FROM_ORG_ID,
                 SHIP_TO_ORG_ID,
                 INVOICE_TO_ORG_ID,
                 BOOKED_FLAG ,
                 ATTRIBUTE1,
                 ATTRIBUTE7,
                 ORG_ID)
                 VALUES
                (l_order_source_id            -- ORDER_SOURCE_ID,
               , rec_so_hdr.cust_po_number    -- CUSTOMER_PO_NUMBER,
               , l_shipping_method_code       -- SHIPPING_METHOD_CODE,
               , l_cust_contact_id            --INVOICE_TO_CONTACT_ID,
               , l_cust_contact_id            --SHIP_TO_CONTACT_ID,
               , l_cust_contact_id            --SOLD_TO_CONTACT_ID,
               , rec_so_hdr.prism_sales_order_num   --ORIG_SYS_DOCUMENT_REF,
               , SYSDATE                      --ORDERED_DATE,
               , l_order_type                 --ORDER_TYPE,
               , l_bill_to_site_use_id        --CUSTOMER_ID,
               , Fnd_Global.user_id           --CREATED_BY,
               , SYSDATE                      --CREATION_DATE,
               , Fnd_Global.user_id           --LAST_UPDATED_BY,
               , SYSDATE                      --LAST_UPDATE_DATE,
               , l_customer_id                --SOLD_TO_ORG_ID,
               , l_inv_org_id                 --SHIP_FROM_ORG_ID,
               , l_ship_to_site_use_id        -- SHIP_TO_ORG_ID,
               , l_bill_to_site_use_id        --INVOICE_TO_ORG_ID,
               , 'Y'                          --BOOKED_FLAG ,
               , l_attribute1                 --ATTRIBUTE1,
               , 'XXWC_INT_SALESFULFILLMENT'  --ATTRIBUTE7,
               , g_org_id);

      ---------------------------------------------------------------------------------
      -- Insert into OE_ACTIONS_IFACE_ALL
      ----------------------------------------------------------------------------------
        INSERT INTO OE_ACTIONS_IFACE_ALL
        (ORDER_SOURCE_ID,
        ORIG_SYS_DOCUMENT_REF,
        OPERATION_CODE,
        ORG_ID)
        VALUES
        (l_order_source_id            -- ORDER_SOURCE_ID,
       , rec_so_hdr.prism_sales_order_num    -- ORIG_SYS_DOCUMENT_REF,
       , 'BOOK_ORDER'                 -- OPERATION_CODE
       , g_org_id );
      END IF; --      IF p_validate_only = 'N' THEN

   EXCEPTION
   WHEN OTHERS THEN
     l_err_flag := 'Y';
     l_error_message := l_error_message || '-- Error inserting into Header interface tables';
   END;

    IF l_err_flag = 'Y' THEN
      RAISE l_exception;
    END IF;

    l_line_number := 0;

    FOR rec_so_lines IN cur_so_lines(rec_so_hdr.prism_sales_order_num) LOOP

      --Added the debug messages by Ashwin.S on 13-Apr-2018 for TMS#20180328-00041 
      fnd_file.put_line(fnd_file.log,'Item Number '||rec_so_lines.item_number);
      fnd_file.put_line(fnd_file.log,'Quantity '||rec_so_lines.quantity);
      fnd_file.put_line(fnd_file.log,'Rental Charge '||rec_so_lines.rental_charge);
      fnd_file.put_line(fnd_file.log,'Rental Start Date '||rec_so_lines.rental_start_date);

        l_line_error_message := NULL;
        l_line_number := l_line_number + 1;

      ---------------------------------------------------------------------------------
      -- Derive Inventory Item details
      ----------------------------------------------------------------------------------
        l_inv_item_id := NULL;
        IF rec_so_lines.item_number IS NOT NULL THEN
          BEGIN
            SELECT inventory_item_id
              INTO l_inv_item_id
              FROM mtl_system_items_b  msib
             WHERE organization_id = l_inv_org_id
               AND enabled_flag    = 'Y'
               AND segment1        = rec_so_lines.item_number;
          EXCEPTION
          WHEN OTHERS THEN
            l_err_flag   := 'Y';
            l_line_error_message := '-- Invalid Item';
            fnd_file.put_line(fnd_file.log,'Error validating item - '||rec_so_lines.item_number||'   Error - '||SQLERRM);
          END;
        ELSE
          l_err_flag := 'Y';
          l_line_error_message := '-- Invalid Item';
        END IF;

      ---------------------------------------------------------------------------------
      -- ReRental Item DFF
      ----------------------------------------------------------------------------------
      l_attribute2 := NULL;
      IF SUBSTR(rec_so_lines.item_number,1,2) = 'RR' THEN
        l_attribute2 := '28 Day ReRental Billing';
      END IF;

      --Added by Ashwin.S on 13-Apr-2018 for TMS#20180328-00041 
      IF p_validate_only='Y' AND l_err_flag!= 'Y' THEN  

        UPDATE XXWC.xxwc_rental_so_conv_tbl
        SET    STATUS='V'
        WHERE  rowid = rec_so_lines.rowid;
      
      END IF;

      ---------------------------------------------------------------------------------
      -- Insert into OE_LINES_IFACE_ALL
      ----------------------------------------------------------------------------------
     IF p_validate_only = 'N' AND l_err_flag != 'Y' AND NVL(rec_so_lines.quantity,0) != 0 THEN

       --Added the debug messages by Ashwin.S on 13-Apr-2018 for TMS#20180328-00041 
       fnd_file.put_line(fnd_file.log,'Inside Insert condition for Lines...');
       fnd_file.put_line(fnd_file.log,'p_validate_only '||p_validate_only);
       fnd_file.put_line(fnd_file.log,'l_err_flag '||l_err_flag);  

        INSERT INTO OE_LINES_IFACE_ALL
            (ORDER_SOURCE_ID,
            INVOICE_TO_CONTACT_ID,
            SHIP_TO_CONTACT_ID,
            ORIG_SYS_DOCUMENT_REF,
            ORIG_SYS_LINE_REF,
            LINE_NUMBER,
            LINE_TYPE,
            INVENTORY_ITEM_ID,
            ORDERED_QUANTITY,
            UNIT_SELLING_PRICE,
            ATTRIBUTE4,
            UNIT_LIST_PRICE,
            CALCULATE_PRICE_FLAG,
            SOLD_TO_ORG_ID,
            SHIP_FROM_ORG_ID,
            SHIP_TO_ORG_ID,
            INVOICE_TO_ORG_ID,
            CREATED_BY,
            CREATION_DATE,
            LAST_UPDATED_BY,
            LAST_UPDATE_DATE,
            ATTRIBUTE2,
            ATTRIBUTE12,
            ORG_ID)
        VALUES(l_order_source_id                          --ORDER_SOURCE_ID,
           , l_cust_contact_id                            --INVOICE_TO_CONTACT_ID,
           , l_cust_contact_id                            --SHIP_TO_CONTACT_ID,
           , rec_so_hdr.prism_sales_order_num             --ORIG_SYS_DOCUMENT_REF,
           , rec_so_hdr.prism_sales_order_num||'_'||l_line_number  --ORIG_SYS_LINE_REF,
           , l_line_number                                --LINE_NUMBER,
           , 'RENTAL LINE'                                --LINE_TYPE,
           , l_inv_item_id                                --INVENTORY_ITEM_ID,
           , rec_so_lines.quantity                        --ORDERED_QUANTITY,
           , 0                                            --UNIT_SELLING_PRICE,
           , rec_so_lines.rental_charge                   --ATTRIBUTE4,
           , rec_so_lines.rental_charge                   --UNIT_LIST_PRICE
           ,'N'                                           --CALCULATE_PRICE_FLAG,
           , l_customer_id                                --SOLD_TO_ORG_ID,,
           , l_inv_org_id                                 --SHIP_FROM_ORG_ID,
           , l_ship_to_site_use_id                        -- SHIP_TO_ORG_ID,
           , l_bill_to_site_use_id                        --INVOICE_TO_ORG_ID,
           , Fnd_Global.user_id                           --CREATED_BY,
           , SYSDATE                                      --CREATION_DATE,
           , Fnd_Global.user_id                           --LAST_UPDATED_BY,
           , SYSDATE                                      --LAST_UPDATE_DATE,
           , l_attribute2                                 --ATTRIBUTE2
           , TO_CHAR(rec_so_lines.rental_start_date,'YYYY/MM/DD')   --ATTRIBUTE12
           , g_org_id);
     END IF; -- p_validate_only = 'N' THEN

    IF l_err_flag = 'Y' THEN
      BEGIN
      -- UPDATE staging table with Error Status
      UPDATE xxwc.xxwc_rental_so_conv_tbl stg
         SET status = 'E'
           , error_message  = l_line_error_message
       WHERE rowid = rec_so_lines.rowid;

      DELETE FROM OE_HEADERS_IFACE_ALL WHERE ORIG_SYS_DOCUMENT_REF = rec_so_hdr.prism_sales_order_num;
      DELETE FROM OE_ACTIONS_IFACE_ALL WHERE ORIG_SYS_DOCUMENT_REF = rec_so_hdr.prism_sales_order_num;
      DELETE FROM OE_LINES_IFACE_ALL WHERE ORIG_SYS_DOCUMENT_REF = rec_so_hdr.prism_sales_order_num;

      EXCEPTION
      WHEN OTHERS THEN
        NULL;
      END;

    ELSE
      BEGIN
      -- UPDATE staging table with Success status
      UPDATE xxwc.xxwc_rental_so_conv_tbl stg
         SET status = 'V'
       WHERE rowid          = rec_so_lines.rowid;
      EXCEPTION
      WHEN OTHERS THEN
        NULL;
      END;
    END IF;

   END LOOP; -- rec_so_lines

    IF l_err_flag = 'Y' THEN
      BEGIN
      IF l_error_message IS NOT NULL THEN
        -- UPDATE staging table with Error Status
        UPDATE xxwc.xxwc_rental_so_conv_tbl stg
           SET status = 'E'
             , error_message  = l_error_message
         WHERE cust_po_number = rec_so_hdr.cust_po_number;
      END IF;

      DELETE FROM OE_HEADERS_IFACE_ALL WHERE ORIG_SYS_DOCUMENT_REF = rec_so_hdr.prism_sales_order_num;
      DELETE FROM OE_ACTIONS_IFACE_ALL WHERE ORIG_SYS_DOCUMENT_REF = rec_so_hdr.prism_sales_order_num;
      DELETE FROM OE_LINES_IFACE_ALL WHERE ORIG_SYS_DOCUMENT_REF = rec_so_hdr.prism_sales_order_num;

      EXCEPTION
      WHEN OTHERS THEN
        NULL;
      END;

    --Added the ELSIF by Ashwin.S on 13-Apr-2018 for TMS#20180328-00041 
    ELSIF l_err_flag!='Y' AND p_validate_only = 'N' THEN

      BEGIN
      -- UPDATE staging table with Success status
      UPDATE xxwc.xxwc_rental_so_conv_tbl stg
         SET status = 'S'
       WHERE cust_po_number          = rec_so_hdr.cust_po_number;
      EXCEPTION
      WHEN OTHERS THEN
        NULL;
      END;

    ELSE

      NULL;

    END IF;

   EXCEPTION
   WHEN l_exception THEN
      BEGIN
      -- UPDATE staging table with Error Status
      UPDATE xxwc.xxwc_rental_so_conv_tbl stg
         SET status = 'E'
           , error_message  = l_error_message
       WHERE cust_po_number = rec_so_hdr.cust_po_number;
      EXCEPTION
      WHEN OTHERS THEN
        NULL;
      END;

      DELETE FROM OE_HEADERS_IFACE_ALL WHERE ORIG_SYS_DOCUMENT_REF = rec_so_hdr.prism_sales_order_num;
      DELETE FROM OE_ACTIONS_IFACE_ALL WHERE ORIG_SYS_DOCUMENT_REF = rec_so_hdr.prism_sales_order_num;
      DELETE FROM OE_LINES_IFACE_ALL WHERE ORIG_SYS_DOCUMENT_REF = rec_so_hdr.prism_sales_order_num;

   END; -- rec_so_hdr
   END LOOP; -- rec_so_hdr

   EXCEPTION
   WHEN l_exception THEN
     fnd_file.put_line(fnd_file.log,'In L_EXCEPTION, ErrorMessage - '||l_error_message);
   WHEN OTHERS THEN
     ROLLBACK;
     fnd_file.put_line(fnd_file.log,'Error in  LOAD_INTERFACE procedure - '||SQLERRM);
   END load_interface;

END XXWC_RENTAL_SO_CONV_PKG;
/