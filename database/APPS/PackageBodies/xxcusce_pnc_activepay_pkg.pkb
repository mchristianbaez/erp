CREATE OR REPLACE PACKAGE BODY APPS.xxcusce_pnc_activepay_pkg IS
  /*******************************************************************************
  * Procedure:   UC4_BANK_STMT
  * Description: This is for UC4 to start the concurrent request to submit
  *              Bank Statement Loader for PNC ActivePay Recon
  *
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     02/19/2013    Kathy Poling        Initial creation of the procedure
  1.1     04/08/2014   Maharajan Shunmugam  TMS#20140127-00190 WC & GSC PNC Inbound Recon file pocess
  1.2     06/02/2014   Maharajan Shunmugam  ESMS# 252149 WC Inbound file name changed as per PNC
  1.3     09/27/2014    Kathy Poling        ESMS# 264959 Enhancement for Power Solutions
  ********************************************************************************/

  ------------------------------------------------------------------------------------------------
  -- Below procedure is commented and logic is used within the new procedure UC4_BANK_STMT1
  -- By Maharajan S on 04/08/2014 for TMS#20140127-00190
  -------------------------------------------------------------------------------------------------

  /*PROCEDURE uc4_bank_stmt(errbuf                OUT VARCHAR2
                           ,retcode               OUT NUMBER
                           ,p_data_file           IN VARCHAR2
                           ,p_user_name           IN VARCHAR2
                           ,p_responsibility_name IN VARCHAR2) IS
      --
      -- Package Variables
      --
      l_package    VARCHAR2(50) := 'XXCUSCE_PNC_ACTIVEPAY_PKG.UC4_BANK_STMT';
      l_dflt_email VARCHAR2(200) := 'hdsoracleglsupport@hdsupply.com';
      l_email      fnd_user.email_address%TYPE;
      
      l_sid                VARCHAR2(8);
      l_req_id              NUMBER NULL;
      v_phase               VARCHAR2(50);
      v_status              VARCHAR2(50);
      v_dev_status          VARCHAR2(50);
      v_dev_phase           VARCHAR2(50);
      v_message             VARCHAR2(250);
      v_error_message       VARCHAR2(3000);
      v_supplier_id         NUMBER;
      v_rec_cnt             NUMBER := 0;
      v_interval            NUMBER := 30; -- In seconds
      v_max_time            NUMBER := 15000; -- In seconds
      l_message             VARCHAR2(150);
      l_errormessage        VARCHAR2(3000);
      pl_errorstatus        NUMBER;
      l_can_submit_request  BOOLEAN := TRUE;
      l_globalset           VARCHAR2(100);
      l_err_msg             VARCHAR2(3000);
      l_err_code            NUMBER;
      l_sec                 VARCHAR2(255);
      l_statement           VARCHAR2(9000);
      l_user                fnd_user.user_id%TYPE;
      l_user_id             NUMBER;
      l_responsibility_id   NUMBER;
      l_resp_application_id NUMBER;
      l_program             VARCHAR2(30) := 'CESQLLDR'; --Bank Statement Loader
      l_application         VARCHAR2(30) := 'CE';
      l_argument1           VARCHAR2(50) := 'IMPORT'; --'Load and Import'; --Process Option
      l_argument2           NUMBER := 3020; -- Map ID for PNC  'XXPNC.ctl'; 
      l_argument4           VARCHAR2(50); --directory path
      l_argument5           VARCHAR2(50) := NULL; --Bank Branch Name
      l_argument6           VARCHAR2(50) := NULL; --Bank Account Number
      l_argument7           VARCHAR2(50) := to_char(TRUNC(sysdate), 'YYYY/MM/DD HH24:MI:SS'); --GL Date
      l_argument8           VARCHAR2(50) := NULL; --Organization
      l_argument9           VARCHAR2(50) := NULL; --Receivables Activity
      l_argument10          VARCHAR2(50) := NULL; --Payment Method
      l_argument11          VARCHAR2(50) := NULL; --NSF Handling
      l_argument12          VARCHAR2(10) := 'N'; --Display Debug
      l_argument13          VARCHAR2(50):= NULL; --Debug Path
      l_argument14          VARCHAR2(50):= NULL; --Debug File
      v_timestamp          NUMBER;
      -- Error DEBUG
      l_err_callfrom  VARCHAR2(75) DEFAULT 'XXCUSCE_PNC_ACTIVEPAY_PKG.UC4_BANK_STMT';
      l_err_callpoint VARCHAR2(75) DEFAULT 'START';
      l_distro_list   VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
      
    
    BEGIN
      
    SELECT lower(NAME) INTO l_sid FROM v$database;
    
  --derive the timestamp
  
    BEGIN
    SELECT TO_CHAR(SYSDATE,'YYYYMMDDHHMMSS') 
      INTO v_timestamp
    FROM dual;
    END;
  
  
  
      -- Deriving User Id
      BEGIN
        SELECT user_id
          INTO l_user_id
          FROM fnd_user
         WHERE user_name = upper(p_user_name)
           AND SYSDATE BETWEEN start_date AND
               nvl(end_date, trunc(SYSDATE) + 1);
      EXCEPTION
        WHEN no_data_found THEN
          l_err_msg := 'UserName - ' || p_user_name ||
                       ' not defined in Oracle';
          RAISE program_error;
        WHEN OTHERS THEN
          l_err_msg := 'Error deriving user_id for UserName - ' ||
                       p_user_name;
          RAISE program_error;
      END;
    
      -- Deriving Responsibility Id and Responsibility Application Id
      BEGIN
        SELECT responsibility_id, application_id
          INTO l_responsibility_id, l_resp_application_id
          FROM fnd_responsibility_vl
         WHERE responsibility_name = p_responsibility_name
           AND SYSDATE BETWEEN start_date AND
               nvl(end_date, trunc(SYSDATE) + 1);
      EXCEPTION
        WHEN no_data_found THEN
          l_err_msg := 'Responsibility - ' || p_responsibility_name ||
                       ' not defined in Oracle';
          RAISE program_error;
        WHEN OTHERS THEN
          l_err_msg := 'Error deriving Responsibility_id for ResponsibilityName - ' ||
                       p_responsibility_name;
          RAISE program_error;
      END;
      
      --directory path
      l_argument4  := '/xx_iface/' || l_sid || '/xtr/inbound/'; 
    
      --------------------------------------------------------------------------
      -- Apps Initialize
      --------------------------------------------------------------------------
      fnd_global.apps_initialize(l_user_id
                                ,l_responsibility_id
                                ,l_resp_application_id);
    
      l_sec := 'UC4 call to run concurrent request Bank Statement Loader.';
    
      l_req_id := fnd_request.submit_request(application => l_application
                                            ,program     => l_program
                                            ,description => NULL
                                            ,start_time  => SYSDATE
                                            ,sub_request => FALSE
                                            ,argument1   => l_argument1
                                            ,argument2   => l_argument2
                                            ,argument3   => p_data_file
                                            ,argument4   => l_argument4
                                            ,argument5   => l_argument5
                                            ,argument6   => l_argument6
                                            ,argument7   => l_argument7
                                            ,argument8   => l_argument8
                                            ,argument9   => l_argument9
                                            ,argument10  => l_argument10
                                            ,argument11  => l_argument11
                                            ,argument12  => l_argument12
                                            ,argument13  => l_argument13
                                            ,argument14  => l_argument14);
      COMMIT;
    
      IF (l_req_id != 0)
      THEN
        IF fnd_concurrent.wait_for_request(l_req_id
                                          ,v_interval
                                          ,v_max_time
                                          ,v_phase
                                          ,v_status
                                          ,v_dev_phase
                                          ,v_dev_status
                                          ,v_message)
        THEN
          v_error_message := chr(10) || 'ReqID=' || l_req_id || ' DPhase ' ||
                             v_dev_phase || ' DStatus ' || v_dev_status ||
                             chr(10) || ' MSG - ' || v_message;
          -- Error Returned
          IF v_dev_phase != 'COMPLETE' OR v_dev_status != 'NORMAL'
          THEN
            l_statement := 'An error occured in the running of the Bank Statement Loader' ||
                           v_error_message || '.';
            fnd_file.put_line(fnd_file.log, l_statement);
            fnd_file.put_line(fnd_file.output, l_statement);
            RAISE program_error;
          
          END IF;
          -- Then Success!
          retcode := 0;
        ELSE
          l_statement := 'An error occured running the Bank Statement Loader' ||
                         v_error_message || '.';
          fnd_file.put_line(fnd_file.log, l_statement);
          fnd_file.put_line(fnd_file.output, l_statement);
          RAISE program_error;
        END IF;
      
      ELSE
        l_statement := 'An error occured when trying to submit the Bank Statement Loader';
        fnd_file.put_line(fnd_file.log, l_statement);
        fnd_file.put_line(fnd_file.output, l_statement);
        RAISE program_error;
      END IF;
    
      dbms_output.put_line(l_sec);
      dbms_output.put_line('Request ID:  ' || l_req_id);
    
    EXCEPTION
      WHEN program_error THEN
        ROLLBACK;
        retcode    := 2;
        l_err_code := 2;
        l_err_msg  := l_sec;
        l_err_msg  := l_err_msg || ' ERROR ' || substr(SQLERRM, 1, 2000);
        dbms_output.put_line(l_err_msg);
        fnd_file.put_line(fnd_file.log, l_err_msg);
        fnd_file.put_line(fnd_file.output, l_err_msg);
        l_err_callpoint := l_err_msg;
      
        xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                            ,p_calling           => l_err_callpoint
                                            ,p_request_id        => l_req_id
                                            ,p_ora_error_msg     => substr(SQLERRM
                                                                          ,1
                                                                          ,2000)
                                            ,p_error_desc        => substr(l_err_msg
                                                                          ,1
                                                                          ,2000)
                                            ,p_distribution_list => l_distro_list
                                            ,p_module            => 'CE');
      
      WHEN OTHERS THEN
        fnd_file.put_line(fnd_file.log, l_sec);
        fnd_file.put_line(fnd_file.output, l_sec);
        l_err_code := 2;
        retcode    := 2;
        l_err_msg  := l_sec;
        l_err_msg  := l_err_msg || ' ERROR ' || SQLCODE ||
                      substr(SQLERRM, 1, 2000);
        dbms_output.put_line(l_err_msg);
        fnd_file.put_line(fnd_file.log, l_err_msg);
        fnd_file.put_line(fnd_file.output, l_err_msg);
        l_err_callpoint := l_err_msg;
      
        xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                            ,p_calling           => l_err_callpoint
                                            ,p_request_id        => l_req_id
                                            ,p_ora_error_msg     => substr(SQLERRM
                                                                          ,1
                                                                          ,2000)
                                            ,p_error_desc        => substr(l_err_msg
                                                                          ,1
                                                                          ,2000)
                                            ,p_distribution_list => l_distro_list
                                            ,p_module            => 'CE');
      
    END uc4_bank_stmt;*/

  PROCEDURE uc4_bank_stmt1(p_errbuf              OUT VARCHAR2
                          ,p_retcode             OUT NUMBER
                          ,p_data_file           IN VARCHAR2
                          ,p_user_name           IN VARCHAR2
                          ,p_responsibility_name IN VARCHAR2) IS
    --
    -- Package Variables
    --
    l_package    VARCHAR2(50) := 'XXCUSCE_PNC_ACTIVEPAY_PKG.UC4_BANK_STMT1';
    l_dflt_email VARCHAR2(200) := 'hdsoracleglsupport@hdsupply.com';
    l_email      fnd_user.email_address%TYPE;
  
    l_sid        VARCHAR2(8);
    l_req_id     NUMBER NULL;
    l_phase      VARCHAR2(50);
    l_status     VARCHAR2(50);
    l_dev_status VARCHAR2(50);
    l_dev_phase  VARCHAR2(50);
    --l_error_message       VARCHAR2(3000);
    l_supplier_id NUMBER;
    l_rec_cnt     NUMBER := 0;
    l_interval    NUMBER := 30; -- In seconds
    l_max_time    NUMBER := 15000; -- In seconds
    l_message     CLOB;
    --l_errormessage        CLOB;
    l_errorstatus         NUMBER;
    l_can_submit_request  BOOLEAN := TRUE;
    l_globalset           VARCHAR2(100);
    l_err_msg             CLOB;
    l_err_code            NUMBER;
    l_sec                 VARCHAR2(110); 
    l_statement CLOB; --VARCHAR2(9000);
    l_user                fnd_user.user_id%TYPE;
    l_user_id             NUMBER;
    l_responsibility_id   NUMBER;
    l_resp_application_id NUMBER;
    l_program             VARCHAR2(30) := 'XXCUS_CE_PNC_RECON'; -- Loader program
    l_application         VARCHAR2(30) := 'XXCUS';
  
    --For CE load
  
    x_req_id             NUMBER NULL;
    x_phase              VARCHAR2(50);
    x_status             VARCHAR2(50);
    x_dev_status         VARCHAR2(50);
    x_dev_phase          VARCHAR2(50);
    x_error_message      VARCHAR2(3000);
    x_supplier_id        NUMBER;
    x_rec_cnt            NUMBER := 0;
    x_interval           NUMBER := 30; -- In seconds
    x_max_time           NUMBER := 15000; -- In seconds
    x_message            VARCHAR2(3000);
    x_errormessage       VARCHAR2(3000);
    x_errorstatus        NUMBER;
    x_can_submit_request BOOLEAN := TRUE;
    x_globalset          VARCHAR2(100);
    x_err_msg            VARCHAR2(3000);
    x_err_code           NUMBER;
    x_sec                VARCHAR2(3000);
    x_statement          VARCHAR2(9000);
    x_program            VARCHAR2(30) := 'CESQLLDR'; --Bank Statement Loader
    x_application        VARCHAR2(30) := 'CE';
    x_argument1          VARCHAR2(50) := 'IMPORT'; --'Load and Import'; --Process Option
    x_argument2          NUMBER;
    x_argument4          VARCHAR2(1000); --directory path
    x_argument5          VARCHAR2(50) := NULL; --Bank Branch Name
    x_argument6          VARCHAR2(50) := NULL; --Bank Account Number
    x_argument7          VARCHAR2(50) := to_char(trunc(SYSDATE)
                                                ,'YYYY/MM/DD HH24:MI:SS'); --GL Date
    x_argument8          VARCHAR2(50) := NULL; --Organization
    x_argument9          VARCHAR2(50) := NULL; --Receivables Activity
    x_argument10         VARCHAR2(50) := NULL; --Payment Method
    x_argument11         VARCHAR2(50) := NULL; --NSF Handling
    x_argument12         VARCHAR2(10) := 'N'; --Display Debug
    x_argument13         VARCHAR2(50) := NULL; --Debug Path
    x_argument14         VARCHAR2(50) := NULL; --Debug File
  
    -- Error DEBUG
    l_err_callfrom  VARCHAR2(1000) DEFAULT 'XXCUSCE_PNC_ACTIVEPAY_PKG.UC4_BANK_STMT1';
    l_err_callpoint VARCHAR2(1000) DEFAULT 'START';
    l_distro_list   VARCHAR2(100) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
  
    l_data_file_name VARCHAR2(1000);
    l_timestamp      NUMBER;
    l_data_file      VARCHAR2(1000);
    l_dat_file_path  VARCHAR2(1000);
    l_ctl_file       VARCHAR2(1000);
    l_ctl_file_path  VARCHAR2(1000);
    l_log_file_path  VARCHAR2(1000);
  
    l_map_id NUMBER; --Version 1.3 
    l_name   VARCHAR2(120); --Version 1.3  
  
  BEGIN
  
    l_sec := ' Starting procedure UC4_BANK_STMT1';
  
    BEGIN
      SELECT to_char(SYSDATE, 'YYYYMMDDHHMMSS') INTO l_timestamp FROM dual;
    END;
  
    --Version 1.3    9/26/14
    --lookup map id based on the file name and setup of the lookup with the control file to be used
    BEGIN
      SELECT map_id
        INTO l_map_id
        FROM ce_bank_stmt_int_map m
       WHERE control_file_name IN
             (SELECT attribute1
                FROM fnd_lookup_values flv
               WHERE 1 = 1
                 AND lookup_type = 'XXCUS_CE_PNC_ACTIVEPAY'
                 AND enabled_flag = 'Y'
                 AND SYSDATE BETWEEN start_date_active AND
                     nvl(end_date_active, SYSDATE + 1)
                 AND upper(meaning) =
                     upper(substr(p_data_file
                                 ,1
                                 ,instr(p_data_file, '_') - 1))
                 AND flv.attribute1 = m.control_file_name);
    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := 'Deriving Map ID from file- ' ||
                     substr(p_data_file, 1, instr(p_data_file, '_') - 1) ||
                     ' not valid';
        RAISE program_error;
    END;
  
    --get naming of file to pass on based off p_data_file and setup of the lookup to be used
    BEGIN
      SELECT attribute2
        INTO l_name
        FROM fnd_lookup_values flv
       WHERE 1 = 1
         AND lookup_type = 'XXCUS_CE_PNC_ACTIVEPAY'
         AND enabled_flag = 'Y'
         AND SYSDATE BETWEEN start_date_active AND
             nvl(end_date_active, SYSDATE + 1)
         AND upper(meaning) =
             upper(substr(p_data_file, 1, instr(p_data_file, '_') - 1));
    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := 'Deriving file name for processing- ' ||
                     substr(p_data_file, 1, instr(p_data_file, '_') - 1) ||
                     ' not valid';
        RAISE program_error;
    END;
    -- 9/26/2014
  
    --------------------------------------------------------------------------
    -- Deriving instance
    --------------------------------------------------------------------------
  
    SELECT lower(NAME) INTO l_sid FROM v$database;
  
    --------------------------------------------------------------------------
    -- Deriving User Id
    --------------------------------------------------------------------------
    BEGIN
      SELECT user_id
        INTO l_user_id
        FROM applsys.fnd_user
       WHERE user_name = upper(p_user_name)
         AND SYSDATE BETWEEN start_date AND
             nvl(end_date, trunc(SYSDATE) + 1);
    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := 'UserName - ' || p_user_name ||
                     ' not defined in Oracle';
        RAISE program_error;
      WHEN OTHERS THEN
        l_err_msg := 'Error deriving user_id for UserName - ' ||
                     p_user_name;
        RAISE program_error;
    END;
  
    --------------------------------------------------------------------------
    -- Deriving Responsibility Id and Responsibility Application Id
    --------------------------------------------------------------------------
    BEGIN
      SELECT responsibility_id, application_id
        INTO l_responsibility_id, l_resp_application_id
        FROM apps.fnd_responsibility_vl
       WHERE responsibility_name = p_responsibility_name
         AND SYSDATE BETWEEN start_date AND
             nvl(end_date, trunc(SYSDATE) + 1);
    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := 'Responsibility - ' || p_responsibility_name ||
                     ' not defined in Oracle';
        RAISE program_error;
      WHEN OTHERS THEN
        l_err_msg := 'Error deriving Responsibility_id for ResponsibilityName - ' ||
                     p_responsibility_name;
        RAISE program_error;
    END;
  
    --------------------------------------------------------------------------
    -- Apps Initialize
    --------------------------------------------------------------------------
    fnd_global.apps_initialize(l_user_id
                              ,l_responsibility_id
                              ,l_resp_application_id);
  
    l_sec := 'UC4 call to run Generic Loader Program.';
  
    BEGIN
      SELECT --attribute1 DAT_FILE,
       attribute2 dat_file_path
      ,attribute3 ctl_file
      ,attribute4 ctl_file_path
      ,attribute5 log_file_path
        INTO l_dat_file_path, l_ctl_file, l_ctl_file_path, l_log_file_path
        FROM applsys.fnd_lookup_values
       WHERE lookup_type = 'XXWC_LOADER_COMMON_LOOKUP'
         AND LANGUAGE = userenv('LANG')
         AND meaning = 'XXCUSCE_PNC_GEN_RECON'
         AND enabled_flag = 'Y'
         AND trunc(SYSDATE) BETWEEN trunc(start_date_active) AND
             trunc(nvl(end_date_active, SYSDATE));
    EXCEPTION
      WHEN OTHERS THEN
        l_err_msg := 'In Others Of Common Lookup..' || SQLERRM;
        fnd_file.put_line(fnd_file.log
                         ,'In Others Of Common Lookup..' || SQLERRM);
    END;
  
    fnd_file.put_line(fnd_file.log
                     ,'l_DAT_FILE_PATH,l_CTL_FILE,l_CTL_FILE_PATH,l_LOG_FILE_PATH..');
    fnd_file.put_line(fnd_file.log
                     ,l_dat_file_path || ',' || l_ctl_file || ',' ||
                      l_ctl_file_path || ',' || l_log_file_path);
  
    fnd_file.put_line(fnd_file.log, 'Calling Genreic Loader Program...');
  
    --------------------------------------------------------------------------
    --Remove file extension to pass to generic loader program
    --------------------------------------------------------------------------
  
    SELECT substr(p_data_file, 1, instr(p_data_file, '.') - 1)
      INTO l_data_file
      FROM dual;
  
    BEGIN
      l_req_id := fnd_request.submit_request(application => 'XXWC'
                                            ,program     => 'XXWCGLP'
                                            ,description => NULL
                                            ,start_time  => SYSDATE
                                            ,sub_request => FALSE
                                            ,argument1   => l_data_file
                                            ,argument2   => l_dat_file_path
                                            ,argument3   => l_ctl_file
                                            ,argument4   => l_ctl_file_path
                                            ,argument5   => l_log_file_path);
      COMMIT;
    
      IF (l_req_id != 0)
      THEN
        IF fnd_concurrent.wait_for_request(l_req_id
                                          ,l_interval
                                          ,l_max_time
                                          ,l_phase
                                          ,l_status
                                          ,l_dev_phase
                                          ,l_dev_status
                                          ,l_message)
        THEN
          l_err_msg := chr(10) || 'ReqID=' || l_req_id || ' DPhase ' ||
                       l_dev_phase || ' DStatus ' || l_dev_status ||
                       chr(10) || ' MSG - ' || l_message;
          -- Error Returned
          IF upper(l_dev_phase) != 'COMPLETE' --OR l_dev_status != 'NORMAL'
          THEN
            l_statement := 'An error occured in the running of XXCUS CE PNC Recon File Loader Program' ||
                           l_err_msg || '.';
            RAISE program_error;
          
          ELSE
            --submit Recon generate file program
            l_sec := 'UC4 call to run concurrent request XXCUS CE PNC Generate Recon file for CE.';
          
            /*  Version 1.3  changed to using lookup
                        --------------------------------------------------------------------------
                        -- Get data file name for GSC
                        --------------------------------------------------------------------------
                     
                        IF upper(p_data_file) LIKE 'HDSUPPLYRECON%'
                        THEN
                        
                          x_argument2 := 3020; -- Map ID for PNC  'XXPNC.ctl'
                        
                          SELECT 'HDSupplyRecon_' || l_timestamp || '.csv'
                            INTO l_data_file_name
                            FROM dual;
                        
                          --------------------------------------------------------------------------
                          -- Get data file name for WC
                          --------------------------------------------------------------------------
                        
                        ELSIF upper(p_data_file) LIKE 'HDSWHITECAPRECON%'   --Changed 'WHITECAPRECON% as HDSWHITECAPRECON% for ver 1.2
                        THEN
                        
                          x_argument2 := 4020; -- Map ID for PNC  'XXPNCv2.ctl'
                        
                          SELECT 'WhiteCapRecon_' || l_timestamp || '.csv'
                            INTO l_data_file_name
                            FROM dual;
                        
                        END IF;
            */
            --Version 1.3  9/26/2014  get file name for processing          
            SELECT l_name || '_' || l_timestamp || '.csv'
              INTO l_data_file_name
              FROM dual;
          
            x_argument2 := l_map_id;
            --9/26/2014
          
            l_req_id := fnd_request.submit_request(application => 'XXCUS'
                                                  ,program     => 'XXCUSCE_PNC_GEN_RECON'
                                                  ,description => NULL
                                                  ,start_time  => SYSDATE
                                                  ,sub_request => FALSE
                                                  ,argument1   => l_data_file_name);
            COMMIT;
          
            IF (l_req_id != 0)
            THEN
              IF fnd_concurrent.wait_for_request(l_req_id
                                                ,l_interval
                                                ,l_max_time
                                                ,l_phase
                                                ,l_status
                                                ,l_dev_phase
                                                ,l_dev_status
                                                ,l_message)
              THEN
                l_err_msg := chr(10) || 'ReqID=' || l_req_id || ' DPhase ' ||
                             l_dev_phase || ' DStatus ' || l_dev_status ||
                             chr(10) || ' MSG - ' || l_message;
                -- Error Returned
                IF l_dev_phase != 'COMPLETE' OR l_dev_status != 'NORMAL'
                THEN
                  l_statement := 'An error occured in the running of XXCUS CE PNC Generate Recon file for CE' ||
                                 l_err_msg || '.';
                  RAISE program_error;
                
                  --Start of CE Load program
                
                ELSE
                
                  BEGIN
                    --------------------------------------------------------------------------
                    --directory path
                    --------------------------------------------------------------------------
                  
                    x_argument4 := '/xx_iface/' || l_sid || '/xtr/inbound/';
                  
                    --------------------------------------------------------------------------
                    -- Apps Initialize
                    --------------------------------------------------------------------------
                    fnd_global.apps_initialize(l_user_id
                                              ,l_responsibility_id
                                              ,l_resp_application_id);
                  
                    x_sec := 'UC4 call to run concurrent request Bank Statement Loader.';
                  
                    x_req_id := fnd_request.submit_request(application => x_application
                                                          ,program     => x_program
                                                          ,description => NULL
                                                          ,start_time  => SYSDATE
                                                          ,sub_request => FALSE
                                                          ,argument1   => x_argument1
                                                          ,argument2   => x_argument2
                                                          ,argument3   => l_data_file_name
                                                          ,argument4   => x_argument4
                                                          ,argument5   => x_argument5
                                                          ,argument6   => x_argument6
                                                          ,argument7   => x_argument7
                                                          ,argument8   => x_argument8
                                                          ,argument9   => x_argument9
                                                          ,argument10  => x_argument10
                                                          ,argument11  => x_argument11
                                                          ,argument12  => x_argument12
                                                          ,argument13  => x_argument13
                                                          ,argument14  => x_argument14);
                    COMMIT;
                  
                    IF (x_req_id != 0)
                    THEN
                      IF fnd_concurrent.wait_for_request(x_req_id
                                                        ,x_interval
                                                        ,x_max_time
                                                        ,x_phase
                                                        ,x_status
                                                        ,x_dev_phase
                                                        ,x_dev_status
                                                        ,x_message)
                      THEN
                        x_error_message := chr(10) || 'ReqID=' || x_req_id ||
                                           ' DPhase ' || x_dev_phase ||
                                           ' DStatus ' || x_dev_status ||
                                           chr(10) || ' MSG - ' ||
                                           x_message;
                        -- Error Returned
                        IF x_dev_phase != 'COMPLETE' OR
                           x_dev_status != 'NORMAL'
                        THEN
                          x_statement := 'An error occured in the running of the Bank Statement Loader' ||
                                         x_error_message || '.';
                          RAISE program_error;
                        
                        END IF;
                        -- Then Success!
                        p_retcode := 0;
                      ELSE
                        x_statement := 'An error occured running the Bank Statement Loader' ||
                                       x_error_message || '.';
                        RAISE program_error;
                      END IF;
                    
                    ELSE
                      x_statement := 'An error occured when trying to submit the Bank Statement Loader';
                      RAISE program_error;
                    END IF;
                  
                    dbms_output.put_line(x_sec);
                    dbms_output.put_line('Request ID:  ' || x_req_id);
                  
                  EXCEPTION
                    WHEN program_error THEN
                      ROLLBACK;
                      x_err_msg := x_sec || x_statement || x_err_msg ||
                                   ' ...Error_Stack...' ||
                                   dbms_utility.format_error_stack() ||
                                   ' Error_Backtrace...' ||
                                   dbms_utility.format_error_backtrace();
                    
                      l_err_callpoint := x_sec;
                    
                      dbms_output.put_line('Request ID:  ' || x_req_id);
                      dbms_output.put_line('Concurrent short name :  ' ||
                                           x_program);
                    
                      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                                          ,p_calling           => l_err_callpoint
                                                          ,p_request_id        => x_req_id
                                                          ,p_ora_error_msg     => substr(x_err_msg
                                                                                        ,1
                                                                                        ,2000)
                                                          ,p_error_desc        => substr(x_err_msg
                                                                                        ,1
                                                                                        ,240)
                                                          ,p_distribution_list => l_distro_list
                                                          ,p_module            => 'CE');
                    
                      p_retcode := 2;
                      p_errbuf  := x_err_msg;
                    
                    WHEN OTHERS THEN
                    
                      fnd_file.put_line(fnd_file.log, x_sec);
                      fnd_file.put_line(fnd_file.output, x_sec);
                      x_err_msg       := x_sec || x_statement || x_err_msg ||
                                         ' ...Error_Stack...' ||
                                         dbms_utility.format_error_stack() ||
                                         ' Error_Backtrace...' ||
                                         dbms_utility.format_error_backtrace();
                      l_err_callpoint := x_sec;
                    
                      dbms_output.put_line('Request ID:  ' || x_req_id);
                      dbms_output.put_line('Concurrent short name :  ' ||
                                           x_program);
                    
                      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                                          ,p_calling           => l_err_callpoint
                                                          ,p_request_id        => x_req_id
                                                          ,p_ora_error_msg     => substr(x_err_msg
                                                                                        ,1
                                                                                        ,2000)
                                                          ,p_error_desc        => substr(x_err_msg
                                                                                        ,1
                                                                                        ,240)
                                                          ,p_distribution_list => l_distro_list
                                                          ,p_module            => 'CE');
                      p_retcode := 2;
                      p_errbuf  := x_err_msg;
                    
                  END;
                
                  --eND OF CE LOAD
                
                END IF;
                -- Then Success!
                p_retcode := 0;
              ELSE
                l_statement := 'An error occured running XXCUS CE PNC Generate Recon file for CE' ||
                               l_err_msg || '.';
                RAISE program_error;
              END IF;
            
            ELSE
              l_statement := 'An error occured when trying to submit XXCUS CE PNC Generate Recon file for CE';
              RAISE program_error;
            END IF;
          
            --End calling recon file program
          
          END IF;
          -- Then Success!
          p_retcode := 0;
        ELSE
          l_statement := 'An error occured running XXWC Genric Loader Program' ||
                         l_err_msg || '.';
          RAISE program_error;
        END IF;
      
      ELSE
        l_statement := 'An error occured when trying to submit XXWC Genric Loader Program';
        RAISE program_error;
      END IF;
    
      dbms_output.put_line(l_sec);
      dbms_output.put_line('Request ID:  ' || l_req_id);
    
    END;
  
  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;
      l_err_msg := l_statement || l_err_msg || ' ...Error_Stack...' ||
                   dbms_utility.format_error_stack() ||
                   ' Error_Backtrace...' ||
                   dbms_utility.format_error_backtrace();
      fnd_file.put_line(fnd_file.log, l_err_msg);
      l_err_callpoint := l_sec;
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'CE');
      p_retcode := 2;
      p_errbuf  := l_err_msg;
    
    WHEN OTHERS THEN
      l_err_msg := l_statement || l_err_msg || ' ...Error_Stack...' ||
                   dbms_utility.format_error_stack() ||
                   ' Error_Backtrace...' ||
                   dbms_utility.format_error_backtrace();
      fnd_file.put_line(fnd_file.log, l_err_msg);
      l_err_callpoint := l_sec;
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'CE');
      p_retcode := 2;
      p_errbuf  := l_err_msg;
    
  END uc4_bank_stmt1;

  /*******************************************************************************
  * Procedure:   UC4_BANK_STMT
  * Description: This is for UC4 to start the concurrent request to submit
  *              XXCUS AP Submit Reconciliation Program
  *
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.1     04/08/2014   Maharajan Shunmugam  TMS#20140127-00190 WC & GSC PNC Inbound Recon file pocess
  1.3     09/27/2014    Kathy Poling    ESMS# 264959 Enhancement for Power Solutions
  ********************************************************************************/

  PROCEDURE uc4_bank_stmt(p_errbuf              OUT VARCHAR2
                         ,p_retcode             OUT NUMBER
                         ,p_data_file           IN VARCHAR2
                         ,p_user_name           IN VARCHAR2
                         ,p_responsibility_name IN VARCHAR2) IS
    --
    l_package     VARCHAR2(50) := 'XXCUSCE_PNC_ACTIVEPAY_PKG.UC4_BANK_STMT';
    l_distro_list VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
  
    l_req_id     NUMBER NULL;
    l_phase      VARCHAR2(50);
    l_status     VARCHAR2(50);
    l_dev_status VARCHAR2(50);
    l_dev_phase  VARCHAR2(50);
    --l_error_message       VARCHAR2(3000);
    l_supplier_id NUMBER;
    l_rec_cnt     NUMBER := 0;
    l_interval    NUMBER := 30; -- In seconds
    l_max_time    NUMBER := 15000; -- In seconds
    l_message     VARCHAR2(2000);
    --l_errormessage        CLOB;
    l_can_submit_request  BOOLEAN := TRUE;
    l_globalset           VARCHAR2(100);
    l_err_msg             VARCHAR2(3000);
    l_err_code            NUMBER;
    l_sec                 VARCHAR2(110);
    l_statement           CLOB;
    l_user                fnd_user.user_id%TYPE;
    l_user_id             NUMBER;
    l_responsibility_id   NUMBER;
    l_resp_application_id NUMBER;
    l_program             VARCHAR2(30) := 'XXCUS_AP_SUBMIT_RECON'; -- Program to process recon
    l_application         VARCHAR2(30) := 'XXCUS';
  
  BEGIN
  
    p_retcode := 0;
    -- Deriving User Id
    BEGIN
      SELECT user_id
        INTO l_user_id
        FROM applsys.fnd_user
       WHERE user_name = upper(p_user_name)
         AND SYSDATE BETWEEN start_date AND
             nvl(end_date, trunc(SYSDATE) + 1);
    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := 'UserName - ' || p_user_name ||
                     ' not defined in Oracle';
        RAISE program_error;
      WHEN OTHERS THEN
        l_err_msg := 'Error deriving user_id for UserName - ' ||
                     p_user_name;
        RAISE program_error;
    END;
  
    -- Deriving Responsibility Id and Responsibility Application Id
    BEGIN
      SELECT responsibility_id, application_id
        INTO l_responsibility_id, l_resp_application_id
        FROM apps.fnd_responsibility_vl
       WHERE responsibility_name = p_responsibility_name
         AND SYSDATE BETWEEN start_date AND
             nvl(end_date, trunc(SYSDATE) + 1);
    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := 'Responsibility - ' || p_responsibility_name ||
                     ' not defined in Oracle';
        RAISE program_error;
      WHEN OTHERS THEN
        l_err_msg := 'Error deriving Responsibility_id for ResponsibilityName - ' ||
                     p_responsibility_name;
        RAISE program_error;
    END;
    
    l_sec := 'Truncate table xxcusce_pnc_inbound_stg_tbl';
    --Version 1.3 
    Begin
    EXECUTE IMMEDIATE 'truncate table xxcus.xxcusce_pnc_inbound_stg_tbl';
    EXCEPTION
      WHEN OTHERS THEN
        l_err_msg := 'Failed to truncate xxxcus.xxcusce_pnc_inbound_stg_tbl : ' ||
                     SQLERRM;
        fnd_file.put_line(fnd_file.log, l_err_msg);
        RAISE program_error;
    END;
  
    --------------------------------------------------------------------------
    -- Apps Initialize
    --------------------------------------------------------------------------
    fnd_global.apps_initialize(l_user_id
                              ,l_responsibility_id
                              ,l_resp_application_id);
  
    l_req_id := fnd_request.submit_request(application => l_application
                                          ,program     => l_program
                                          ,description => NULL
                                          ,start_time  => SYSDATE
                                          ,sub_request => FALSE
                                          ,argument1   => p_data_file
                                          ,argument2   => p_user_name
                                          ,argument3   => p_responsibility_name);
    COMMIT;
  
    IF (l_req_id != 0)
    THEN
      IF fnd_concurrent.wait_for_request(l_req_id
                                        ,l_interval
                                        ,l_max_time
                                        ,l_phase
                                        ,l_status
                                        ,l_dev_phase
                                        ,l_dev_status
                                        ,l_message)
      THEN
        l_err_msg := chr(10) || 'ReqID=' || l_req_id || ' DPhase ' ||
                     l_dev_phase || ' DStatus ' || l_dev_status || chr(10) ||
                     ' MSG - ' || l_message;
        -- Error Returned
        IF l_dev_phase != 'COMPLETE' OR l_dev_status != 'NORMAL'
        THEN
          l_statement := 'An error occured in the running of XXCUS AP Submit Reconciliation Program' ||
                         l_err_msg || '.';
          fnd_file.put_line(fnd_file.log, l_statement);
          fnd_file.put_line(fnd_file.output, l_statement);
          RAISE program_error;
        END IF;
      END IF;
    
    ELSE
      l_statement := 'An error occured when trying to submit XXCUS AP Submit Reconciliation Program';
      RAISE program_error;
    END IF;
  
    dbms_output.put_line(l_statement);
    dbms_output.put_line('Request ID:  ' || l_req_id);
    dbms_output.put_line('Responsibility Name:  ' || p_responsibility_name);
    dbms_output.put_line('User Name:  ' || p_user_name);
    dbms_output.put_line('File Name:  ' || p_data_file);
  
    --For UC4 success notification
    dbms_output.put_line('Success');
  
  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;
      l_err_msg := l_err_msg || '-' || l_statement || ' ...Error_Stack...' ||
                   dbms_utility.format_error_stack() ||
                   ' Error_Backtrace...' ||
                   dbms_utility.format_error_backtrace();
      dbms_output.put_line('Request ID:  ' || l_req_id);
      dbms_output.put_line('Concurrent short name :  ' || l_program);
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_package
                                          ,p_calling           => l_err_msg
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'CE');
      p_retcode := 2;
      p_errbuf  := l_err_msg;
    
    WHEN OTHERS THEN
      l_err_msg := l_err_msg || '-' || l_statement|| ' ...Error_Stack...' ||
                   dbms_utility.format_error_stack() ||
                   ' Error_Backtrace...' ||
                   dbms_utility.format_error_backtrace();
      dbms_output.put_line('Request ID:  ' || l_req_id);
      dbms_output.put_line('Concurrent short name :  ' || l_program);
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_package
                                          ,p_calling           => l_err_msg
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'CE');
      p_retcode := 2;
      p_errbuf  := l_err_msg;
    
  END uc4_bank_stmt;

  /*******************************************************************************
  * Procedure:   GENERATE_FILE
  * Description: This procedure will use the data from staging table and generate
  *              PNC ActivePay Recon file in desired format
  *
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     04/08/2014    Maharajan Shunmugam    TMS#20140127-00190
  1.3     09/27/2014    Kathy Poling    ESMS# 264959 Enhancement for Power Solutions
  ********************************************************************************/

  PROCEDURE generate_file(p_errbuf    OUT VARCHAR2
                         ,p_retcode   OUT NUMBER
                         ,p_data_file IN VARCHAR2) IS
  
    l_data_file_name VARCHAR2(1000);
    l_req_id         NUMBER := 0;
    l_path           VARCHAR2(200) := 'XXWC_PNC_RECON';
    l_request_id     NUMBER := 0;
    l_sum            NUMBER;
    l_footer         VARCHAR2(2000);
    l_output_file_id utl_file.file_type;
    l_header         VARCHAR2(2000);
    l_detail         VARCHAR2(3000);
    l_package        VARCHAR2(50) := 'XXCUSCE_PNC_ACTIVEPAY_PKG.GENERATE_FILE';
    l_distro_list    VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
    l_sec            VARCHAR2(2000);
    l_err_msg        CLOB; --VARCHAR2(2000);
  
  BEGIN
  
    l_sec    := 'Starting generating file procedure';
    l_req_id := fnd_global.conc_request_id;
  
    l_data_file_name := p_data_file;
  
    --Version 1.3 9/26/2014
    --get header record to pass 
    l_sec := 'Lookup header record for file';
    BEGIN
      SELECT attribute3 || to_char(SYSDATE, 'MMDDRRRR')
        INTO l_header
        FROM fnd_lookup_values flv
       WHERE 1 = 1
         AND lookup_type = 'XXCUS_CE_PNC_ACTIVEPAY'
         AND enabled_flag = 'Y'
         AND SYSDATE BETWEEN start_date_active AND
             nvl(end_date_active, SYSDATE + 1)
         AND upper(attribute2) =
             upper(substr(p_data_file, 1, instr(p_data_file, '_')));
    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := 'Deriving header record for file- ' ||
                     substr(p_data_file, 1, instr(p_data_file, '_')) ||
                     ' not valid';
        RAISE program_error;
    END;
  
    fnd_file.put_line(fnd_file.log, 'Header is  ' || l_header);
    fnd_file.put_line(fnd_file.log, 'Filename is  ' || l_data_file_name);
  
    /*  --remove hard coding put in a lookup
    --*****************************************************************
    -- hard code header account name and output file name for GSC
    --*****************************************************************
    
    IF upper(p_data_file) LIKE 'HDSUPPLYRECON%'
    THEN
    
      SELECT 'H' || ',' || 'PNCCOMMERCIALCARD' || ',' ||
             to_char(SYSDATE, 'MMDDRRRR')
        INTO l_header
        FROM dual;
    
      fnd_file.put_line(fnd_file.log, 'Header is  ' || l_header);
      fnd_file.put_line(fnd_file.log, 'Filename is  ' || l_data_file_name);
    
      --*****************************************************************
      -- hard code header account name and output file name for WC
      --*****************************************************************
    
    ELSIF upper(p_data_file) LIKE 'WHITECAPRECON%'
    THEN
    
      SELECT 'H' || ',' || 'WCPNCCOMMERCIALCARD' || ',' ||
             to_char(SYSDATE, 'MMDDRRRR')
        INTO l_header
        FROM dual;
    
      fnd_file.put_line(fnd_file.log, 'Header is  ' || l_header);
      fnd_file.put_line(fnd_file.log, 'Filename is  ' || l_data_file_name);
    
    END IF;
    */
    -- 9/26/2014
    --*****************************************************************
    -- Get the path to write the file
    --*****************************************************************
    l_sec := 'Getting path to write file';
  
    BEGIN
    
      fnd_file.put_line(fnd_file.log, 'Path is  ' || l_path);
    
      l_output_file_id := utl_file.fopen(l_path, l_data_file_name, 'w');
    EXCEPTION
      WHEN OTHERS THEN
        l_err_msg := 'Error in opening file using UTIL_FILE ';
        fnd_file.put_line(fnd_file.log
                         ,'Error in opening file using UTIL_FILE ');
    END;
  
    fnd_file.put_line(fnd_file.log
                     ,'After opening file id for file ' || l_path || '/' ||
                      l_data_file_name);
  
    utl_file.put_line(l_output_file_id, l_header);
  
    fnd_file.put_line(fnd_file.log
                     ,'Header record copied into the file ' || l_path || '/' ||
                      l_data_file_name);
  
    --*****************************************************************
    -- writing file
    --*****************************************************************
  
    l_sec := 'Writing recon file';
  
    FOR i IN (SELECT record_type
                    ,MAX(invoice_date) invoice_date
                    ,document_number
                    ,SUM(invoice_amount) invoice_amount
                FROM xxcus.xxcusce_pnc_inbound_stg_tbl
               GROUP BY record_type, document_number)
    LOOP
      l_detail := 'X' || ',' || i.record_type || ',' || ' ' || ',' ||
                  i.invoice_date || ',' || ' ' || ',' || i.document_number || ',' ||
                  i.invoice_amount;
    
      utl_file.put_line(l_output_file_id, l_detail);
    END LOOP;
  
    l_sec := 'End Writing recon file';
  
    --*****************************************************************
    -- Get total invoice amount
    --*****************************************************************
    l_sec := 'Calculating total invoice amount';
  
    BEGIN
      SELECT SUM(invoice_amount)
        INTO l_sum
        FROM xxcus.xxcusce_pnc_inbound_stg_tbl;
    EXCEPTION
      WHEN OTHERS THEN
        l_sum     := 0;
        l_err_msg := ' Exception while calculating the sum of invoice amount';
    END;
  
    --*****************************************************************
    -- Generate footer
    --****************************************************************
    l_sec := 'Generating footer';
  
    SELECT 'X' || ',' || 399 || ',' || '' || ',' || '' || ',' || '' || ',' || '' || ',' ||
           l_sum
      INTO l_footer
      FROM dual;
  
    utl_file.put_line(l_output_file_id, l_footer);
  
    fnd_file.put_line(fnd_file.log
                     ,'All detail records are copied into the file ' ||
                      l_path || '/' || l_data_file_name);
  
    utl_file.fclose(l_output_file_id);
  
    fnd_file.put_line(fnd_file.log
                     ,'After close of file ' || l_path || '/' ||
                      l_data_file_name);
  
  EXCEPTION
    WHEN OTHERS THEN
      fnd_file.put_line(fnd_file.log, 'Error in ');
      l_err_msg := l_err_msg || l_sec || ' ...Error_Stack...' ||
                   dbms_utility.format_error_stack() ||
                   ' Error_Backtrace...' ||
                   dbms_utility.format_error_backtrace();
      fnd_file.put_line(fnd_file.log, l_err_msg);
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_package
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => substr(' Error_Stack...' ||
                                                                         dbms_utility.format_error_stack() ||
                                                                         ' Error_Backtrace...' ||
                                                                         dbms_utility.format_error_backtrace()
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'CE');
      p_errbuf  := l_err_msg;
      p_retcode := 2;
    
  END;

END;
/
