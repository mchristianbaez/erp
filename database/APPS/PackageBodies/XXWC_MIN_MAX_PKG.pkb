CREATE OR REPLACE 
package body           APPS.XXWC_MIN_MAX_PKG as


/******************************************************************************
   NAME:       XXWC_MIN_MAX_PKG

   PURPOSE:    Automate XXWC Min-Max Planning For All Organizations - TMS Ticket 20121217-00830
   

   REVISIONS:   
   Ver        Date         Author           Description
   ---------  -----------  ---------------  ------------------------------------
   1.0        19-APR-2013  Lee Spitzer       1. Create the PL/SQL Package
   1.1        05-JUN-2013  Lee Spitzer       2. Removed Req Import Concurrent Request Launch - TMS Ticket 20130605-00994
   1.2        02-DEC-2014  Pattabhi Avula    3. TMS# 20141002-00048 Multi-org Changes donne
******************************************************************************/


 -- Global variables
 G_Application    Fnd_Application.Application_Short_Name%TYPE :='XXWC';
 G_Cp_Short_Code  Fnd_Concurrent_Programs.Concurrent_Program_Name%Type :='XXWC_INVISMMX'; --XXWC Min-Max Planning Report

  procedure main (
    retcode               out number
   ,errbuf                out VARCHAR2
   ,p_region              IN VARCHAR2
   ,p_supply_date         IN VARCHAR2
   ,p_demand_date         IN VARCHAR2
   ,p_restock             IN NUMBER
   ,p_supply_type         IN NUMBER
  ) is
    
    CURSOR wc_inv_orgs (n_org_id IN NUMBER) IS
      SELECT ood.organization_id,
             ood.organization_code,
             ood.organization_name,
             ood.set_of_books_id,
             ood.chart_of_accounts_id,
             ood.legal_entity,
             mp.attribute9 region
        FROM org_organization_definitions ood,
             mtl_parameters mp
       WHERE 1 = 1         
         AND ood.operating_unit = n_org_id 
         AND ood.disable_date IS NULL
         AND ood.organization_code NOT IN (FND_PROFILE.VALUE('XXWC_CORPORATE_ORGANIZATION'),'MST')  -- V 1.2 Replaced the WCC organization code with profile value by Pattabhi on 02-Dec-2014 for TMS# 20141002-00048Canada Changes
         AND ood.organization_id = mp.organization_id
         AND mp.attribute9 = nvl(p_region,mp.attribute9)
    ORDER BY ood.organization_code ASC;
    
    
    CURSOR c_location (p_organization_id IN NUMBER) IS 
      SELECT location_id
      FROM   hr_all_organization_units
      WHERE  organization_id = p_organization_id
      AND    p_supply_type = 2
      UNION
      select to_number(attribute15) location_id
      FROM   mtl_parameters
      WHERE  organization_id = p_organization_id
      AND    p_supply_type = 1;
      
    type wc_inv_orgs_tbl is table of wc_inv_orgs%rowtype index by binary_integer;
    wc_inv_orgs_rec wc_inv_orgs_tbl;        
  
   l_request_data VARCHAR2(20) :='';
   l_request_id   NUMBER :=0;
   l_req_id NUMBER :=0;
   l_org_id NUMBER :=0; 
   l_user_id NUMBER :=0;
   
   l_category_set_id NUMBER;
   l_structure_id NUMBER;
   l_exception EXCEPTION;
   l_req_approval VARCHAR2(1);
   
      
  begin  
  
   l_request_data :=fnd_conc_global.request_data;
   
   l_req_id :=fnd_global.conc_request_id;
   
   
   begin
   
    SELECT category_set_id, structure_id
    into   l_category_set_id, l_structure_id
    FROM   mtl_category_sets
    where  category_set_name = 'Inventory Category';
   
   exception
    
      WHEN others THEN
          raise l_exception;
   end;
   
 
   if l_request_data is null then     
       
       l_request_data :='1';
  
       l_org_id :=mo_global.get_current_org_id;
      
       --l_req_id :=fnd_global.conc_request_id;     
       
       l_user_id :=fnd_global.user_id; 
       
       fnd_file.put_line(fnd_file.log, '');              
       fnd_file.put_line(fnd_file.log, 'l_org_id ='||l_org_id);
       fnd_file.put_line(fnd_file.log, 'l_user_id ='||l_user_id);
       fnd_file.put_line(fnd_file.log, '');      
      
      Open wc_inv_orgs(l_org_id);
      Fetch wc_inv_orgs Bulk Collect Into wc_inv_orgs_rec;
      Close wc_inv_orgs;    
      
      If wc_inv_orgs_rec.count =0 Then
      
       fnd_file.put_line(fnd_file.log,'No active inventory organizations found for operating unit WhiteCap');
       
      Else --wc_inv_orgs_rec.count >0 
      
         -- We will kick off for every single inventory orgs and not wait to check how many are running at a time
          for indx in 1 .. wc_inv_orgs_rec.count loop  
          
              for r_location in c_location(wc_inv_orgs_rec(Indx).organization_id) loop
               
               fnd_file.put_line(fnd_file.log, 'org ='||wc_inv_orgs_rec(indx).organization_name);
    
                l_request_id :=fnd_request.submit_request
                      (application      =>G_Application
                      ,PROGRAM          =>G_Cp_Short_Code
                      ,description      =>''
                      ,start_time       =>''
                      ,sub_request      =>TRUE
                      ,argument1        =>wc_inv_orgs_rec(Indx).organization_id --Organization Id
                      ,argument2        =>1 --Planning Level
                      ,argument3        =>NULL --Flag to enable/disable subinventory parameter
                      ,argument4        =>NULL --Subinventory
                      ,argument5        =>1 --Selection
                      ,argument6        =>l_category_set_id --Category Set
                      ,argument7        =>l_structure_id --Category Structure
                      ,argument8        =>NULL --Category Low
                      ,argument9        =>NULL --Category High
                      ,argument10       =>NULL --Item Low
                      ,argument11       =>NULL --Item High
                      ,argument12       =>NULL --Planner Low
                      ,argument13       =>NULL --Planner High
                      ,argument14       =>NULL --Buyer Low
                      ,argument15       =>NULL --Buyer High
                      ,argument16       =>1    --Sort By
                      ,argument17       =>p_demand_date --Demand Cutoff Date
                      ,argument18       =>0 --Demand Cutoff Date Offset
                      ,argument19       =>p_supply_date --Supply Cutoff Date
                      ,argument20       =>0 --Supply Cutoff Date Offset
                      ,argument21       =>l_user_id --USER ID
                      ,argument22       =>p_restock --Restock
                      ,argument23       =>NULL --For Repetitive Item
                      ,argument24       =>r_location.location_id --Default Delivery To
                      ,argument25       =>1 --Net Unreserved Orders
                      ,argument26       =>1 --Net Reserved Orders
                      ,argument27       =>1 --Net WIP Demand
                      ,argument28       =>1 --Include PO Supply
                      ,argument29       =>1 --Include Move Order Supply
                      ,argument30       =>1 --Include WIP Supply
                      ,argument31       =>1 --Include Interface Supply
                      ,argument32       =>2 --Include Nonnetable
                      ,argument33       =>3 --Lot Control
                      ,argument34       =>1 --Display Format
                      ,argument35       =>2 --Display Item Description
                      ,argument36       =>2 --Purchasing by Revision
                      ,argument37       =>p_supply_type --Supply Type
                      ); 
                     
                  IF l_request_id >0 THEN 
                    fnd_file.put_line(fnd_file.log, 'Submitted XXWC Min Max Report for organization ='
                                                     ||wc_inv_orgs_rec(indx).organization_name
                                                     ||', Request Id ='
                                                     ||l_request_id
                                                    );
                  ELSE
                    fnd_file.put_line(fnd_file.log, 'Failed to submit XXWC Min Max Report for organization ='
                                                     ||wc_inv_orgs_rec(indx).organization_name                                                     
                                                    );
                  end if;     
                     
                 fnd_conc_global.set_req_globals(conc_status =>'PAUSED' ,request_data =>l_request_data);
           
                 l_request_data :=to_char(to_number(l_request_data)+1);      
              
            END loop;
            
          end loop;     
                              
      End If; -- checking if wc_inv_orgs_rec.count =0      
  
    COMMIT;
    
   /*--Removed on 6/5/2013 due to issues causing multiple internal sales order - TMS Ticket 20130605-00994 
   /*
   if p_restock = 1 THEN
     
      --If the Source Type is Supplier = 2 or 3, then initiate req approval
      IF p_supply_type != 1 THEN
      
        l_req_approval := 'Y';
        
      ELSE
      
      --If the Source Type is Inventory = 1 then No
      
        l_req_approval := 'N';
      
      END IF;
      
      l_request_id :=fnd_request.submit_request
              (
               application      =>'PO'
              ,PROGRAM          =>'REQIMPORT'
              ,description      =>'Requisition Import'
              ,start_time       =>''
              ,sub_request      => TRUE
              ,argument1        => 'INV' --Import Source
              ,argument2        => NULL  --Batch
              ,argument3        => 'VENDOR' --Group By               
              ,argument4        => NULL --Last Requisition Number
              ,argument5        => 'N' --Multiple Distributions
              ,argument6        => l_req_approval --Initiate Req Approval After Reqimport                                                                          
              ); 
     
          COMMIT;
      
     END IF;
   
   */ 

      retcode :=0;
      errbuf :='Child request[s] completed. Exit XXWC Min-Max For All Orgs';
      fnd_file.put_line(fnd_file.log,errbuf);

    
    ELSE
    
      retcode :=1;
      errbuf :='Child requests not submitted becuase no active orgs. Exit XXWC Min-Max For All Orgs';
      fnd_file.put_line(fnd_file.log,errbuf);

   END IF;
   
  exception
    WHEN l_exception THEN
      fnd_file.put_line(fnd_file.log, 'Error in XXWC_MIN_MAX_PKG count not find Inventory Category Set ='||sqlcode || sqlerrm);
   WHEN others THEN
    fnd_file.put_line(fnd_file.log, 'Error in caller apps.XXWC_MIN_MAX_PKG.main ='||sqlerrm);
    rollback;
  end main;
  
end XXWC_MIN_MAX_PKG;