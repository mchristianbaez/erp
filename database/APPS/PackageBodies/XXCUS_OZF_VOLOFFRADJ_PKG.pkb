CREATE OR REPLACE PACKAGE BODY APPS.XXCUS_OZF_VOLOFFRADJ_PKG IS
  
   g_pkg_name       CONSTANT VARCHAR2 (30) := 'XXCUS_OZF_VOLOFFRADJ_PKG';
   g_recal_flag     CONSTANT VARCHAR2(1) :=  NVL(fnd_profile.value('OZF_BUDGET_ADJ_ALLOW_RECAL'),'N');
   g_order_gl_phase CONSTANT VARCHAR2 (15) :=NVL(fnd_profile.VALUE ('OZF_ORDER_GLPOST_PHASE'), 'SHIPPED');
   g_debug_flag     VARCHAR2 (1) := 'N';
   G_DEBUG          BOOLEAN := FND_MSG_PUB.check_msg_level(FND_MSG_PUB.g_msg_lvl_debug_high);
   g_bulk_limit     CONSTANT NUMBER := 5000;

   TYPE amountTbl       IS TABLE OF ozf_funds_utilized_all_b.amount%TYPE;
   TYPE glDateTbl       IS TABLE OF ozf_funds_utilized_all_b.gl_date%TYPE;
   TYPE objectTypeTbl   IS TABLE OF ozf_funds_utilized_all_b.object_type%TYPE;
   TYPE objectIdTbl     IS TABLE OF ozf_funds_utilized_all_b.object_id%TYPE;
   TYPE priceAdjustmentIDTbl     IS TABLE OF ozf_funds_utilized_all_b.price_adjustment_id%TYPE;
   TYPE glPostedFlagTbl     IS TABLE OF ozf_funds_utilized_all_b.gl_posted_flag%TYPE;
   TYPE orderLineIdTbl     IS TABLE OF ozf_funds_utilized_all_b.order_line_id%TYPE;
   TYPE utilizationIdTbl IS TABLE OF ozf_funds_utilized_all_b.utilization_id%TYPE; --Added for bug 7030415

   TYPE order_line_rec_type IS RECORD(order_header_id               NUMBER
                                     ,order_line_id                 NUMBER
                                     ,inventory_item_id             NUMBER
                                     ,unit_list_price               NUMBER
                                     ,quantity                      NUMBER
                                     ,transactional_curr_code       oe_order_headers_all.transactional_curr_code%TYPE
                                     ,line_category_code            oe_order_lines_all.line_category_code%TYPE
                                     ,reference_line_id             NUMBER
                                     ,order_number                  NUMBER
                                     ,group_nos                     VARCHAR2(256)
                                     ,uom_ratio                     NUMBER
                                     );

    TYPE order_line_tbl_type IS TABLE OF order_line_rec_type INDEX BY BINARY_INTEGER;
    TYPE offer_id_tbl IS TABLE OF NUMBER index by binary_integer;
    TYPE product_attr_val_cursor_type is ref cursor;
    g_offer_id_tbl offer_id_tbl;
    
  PROCEDURE write_conc_log ( p_text IN VARCHAR2)
                           IS
   BEGIN
      --IF g_debug_flag = 'Y' THEN
         ozf_utility_pvt.write_conc_log (p_text);
        --ozf_utility_pvt.debug_message(p_text);
      --END IF;
   END;
   
   PROCEDURE process_accrual (
      p_earned_amt          IN       NUMBER,
      p_qp_list_header_id   IN       NUMBER,
      p_act_util_rec        IN       ozf_actbudgets_pvt.act_util_rec_type,
      p_act_budgets_rec     IN       ozf_actbudgets_pvt.act_budgets_rec_type := NULL,
      x_return_status       OUT NOCOPY      VARCHAR2,
      x_msg_count           OUT NOCOPY      NUMBER,
      x_msg_data            OUT NOCOPY      VARCHAR2
   ) IS
      l_fund_amt_tbl            ozf_accrual_engine.ozf_fund_amt_tbl_type;
      l_api_name                VARCHAR2(30):= 'process_accrual';
      l_full_name               VARCHAR2(60):= g_pkg_name ||'.'||l_api_name||' : ' ;
      l_act_budgets_rec         ozf_actbudgets_pvt.act_budgets_rec_type := p_act_budgets_rec;
      l_act_util_rec            ozf_actbudgets_pvt.act_util_rec_type    := p_act_util_rec;
      l_earned_amount           NUMBER;
      l_old_earned_amount       NUMBER;
      l_header_id               NUMBER; -- order or invoice id
      l_line_id                 NUMBER; -- order or invoice id
      l_remaining_amt           NUMBER;
      l_count                   NUMBER                                  := 1;
      l_rate                    NUMBER;
      l_util_curr               VARCHAR2 (30);
      l_adj_amount              NUMBER;
      l_converted_adj_amount    NUMBER;
      j                         NUMBER; --loop counter
      l_off_name                VARCHAR2(240);
      l_off_description         VARCHAR2(2000);
      l_act_budget_id           NUMBER;
      l_earned_amount           NUMBER;

      CURSOR c_offer_info (p_list_header_id IN NUMBER) IS
         ----- fix bug 5675871
         SELECT qp.description, qp.name ,nvl(ofr.transaction_currency_code, ofr.fund_request_curr_code)
           FROM qp_list_headers_vl qp, ozf_offers ofr
           WHERE qp.list_header_id = p_list_header_id
             AND qp.list_header_id = ofr.qp_list_header_id;
/*
         SELECT description, name ,currency_code
           FROM qp_list_headers_vl
           WHERE list_header_id = p_list_header_id;
*/
      CURSOR c_adj_info (p_price_adj_id IN NUMBER,p_object_type VARCHAR2,p_order_line_id IN NUMBER) IS
         SELECT distinct billto_cust_account_id, cust_account_id,product_id,object_id,object_type,org_id
                ,ship_to_site_use_id,bill_to_site_use_id,exchange_rate_type --Added for bug 7030415
           FROM ozf_funds_utilized_all_b
           WHERE price_adjustment_id = p_price_adj_id
           AND object_type = p_object_type
           AND order_line_id = p_order_line_id;

      CURSOR c_tp_adj_info (p_price_adj_id IN NUMBER,p_object_type VARCHAR2) IS
         SELECT distinct billto_cust_account_id, cust_account_id,product_id,object_id,object_type,org_id
                ,ship_to_site_use_id,bill_to_site_use_id,exchange_rate_type --Added for bug 7030415
           FROM ozf_funds_utilized_all_b
           WHERE price_adjustment_id = p_price_adj_id
           AND object_type = p_object_type;

 -- Added for bug 7030415, cursor for currency conversion type.
      CURSOR c_get_conversion_type( p_org_id   IN   NUMBER) IS
         SELECT exchange_rate_type
         FROM   ozf_sys_parameters_all
         WHERE  org_id = p_org_id;

        l_exchange_rate_type VARCHAR2(30) := FND_API.G_MISS_CHAR;



       l_adj_info  c_adj_info%ROWTYPE;

   BEGIN
      x_return_status            := fnd_api.g_ret_sts_success;
      SAVEPOINT process_accrual;
      IF G_DEBUG THEN
         ozf_utility_pvt.debug_message ('   Start'|| g_pkg_name||'.'||l_api_name);
      END IF;

      IF l_act_util_rec.object_type = 'TP_ORDER' THEN
         OPEN c_tp_adj_info (l_act_util_rec.price_adjustment_id,l_act_util_rec.object_type);
         FETCH c_tp_adj_info INTO l_adj_info;
         CLOSE c_tp_adj_info;
      ELSE
         OPEN c_adj_info (l_act_util_rec.price_adjustment_id,l_act_util_rec.object_type,l_act_util_rec.order_line_id);
         FETCH c_adj_info INTO l_adj_info;
         CLOSE c_adj_info;
      END IF;

      ozf_accrual_engine.calculate_accrual_amount (
         x_return_status=> x_return_status,
         p_src_id=> p_qp_list_header_id,
         p_earned_amt=> p_earned_amt,
         -- yzhao: 02/23/2004 11.5.10 added following 3 parameters to return customer-product qualified budgets only
         --        if none budget qualifies, then post to all budgets
         p_cust_account_type => 'BILL_TO',
         p_cust_account_id   => l_adj_info.billto_cust_account_id,
         p_product_item_id   => l_adj_info.product_id,
         x_fund_amt_tbl=> l_fund_amt_tbl
         );

      --dbms_output.put_line(' cal Status '||x_return_status);
      write_conc_log(l_full_name ||'Calculate Accrual Amt Return Status ' ||x_return_status);
      IF G_DEBUG THEN
         ozf_utility_pvt.debug_message (l_full_name ||'Return Status' ||x_return_status);
      END IF;
      -- fetch offer info
      OPEN c_offer_info ( p_qp_list_header_id);
      FETCH c_offer_info INTO l_off_description, l_off_name,l_util_curr ;
      CLOSE c_offer_info;

      --- if this is not funded by a parent campaign or any budget the error out saying no budgte found
      IF l_fund_amt_tbl.COUNT = 0 OR x_return_status <> 'S' THEN
         IF fnd_msg_pub.check_msg_level (fnd_msg_pub.g_msg_lvl_error) THEN
            fnd_message.set_name ('OZF', 'OZF_FUND_NO_BUDGET_FOUND');
            fnd_message.set_token ('OFFER_NAME', l_off_name);
            fnd_msg_pub.ADD;
         END IF;
         --dbms_output.put_line(' In error ');
         IF x_return_status = fnd_api.g_ret_sts_error THEN
            RAISE fnd_api.g_exc_error;
         ELSIF x_return_status = fnd_api.g_ret_sts_unexp_error THEN
            RAISE fnd_api.g_exc_unexpected_error;
         END IF;
      ELSE
         --if some row is returned to adjust then
         IF G_DEBUG THEN
            ozf_utility_pvt.debug_message ('Begin Processing For Offer Adjustment: '||l_off_name);
         END IF;
         IF  (l_fund_amt_tbl.COUNT > 0) AND x_return_status = fnd_api.g_ret_sts_success THEN
             l_adj_amount               := 0; -- in offer currency
             l_remaining_amt            :=  ozf_utility_pvt.currround (
                                            p_earned_amt,
              --nirprasa,12.1.1             l_util_curr ); -- in offer currency
                                            l_act_util_rec.plan_currency_code); -- in transaction currency

              --nirprasa,12.1.1
            <<earned_adj_loop>>
            FOR j IN l_fund_amt_tbl.FIRST .. l_fund_amt_tbl.LAST
            LOOP
               IF g_recal_flag = 'N' THEN
                  IF l_fund_amt_tbl (j).earned_amount = 0 THEN
                      IF G_DEBUG THEN
                         ozf_utility_pvt.debug_message ('    D: 0 earned amount' );
                      END IF;
                      GOTO l_endofearadjloop;
                  END IF;
               END IF;
               IF ABS(l_remaining_amt) >= l_fund_amt_tbl (j).earned_amount THEN
                   l_adj_amount               := l_fund_amt_tbl (j).earned_amount; -- this is in offer and order currency
               ELSE
                  l_adj_amount               := l_remaining_amt;
               END IF;
               l_adj_amount            :=  ozf_utility_pvt.currround (
                                    l_adj_amount,
               --nirprasa,12.1.1    l_util_curr  ); -- in offer currency
                                    l_act_util_rec.plan_currency_code); -- in transaction currency
               --nirprasa,12.1.1

               l_remaining_amt            := l_remaining_amt - l_adj_amount;
                  -- conver the adjustment amount from offer currency to fund currency
                  --use l_adj_info


               --nirprasa,12.1.1 IF l_util_curr <> l_fund_amt_tbl (j).budget_currency THEN
               IF l_act_util_rec.plan_currency_code <> l_fund_amt_tbl (j).budget_currency THEN
                        ozf_utility_pvt.convert_currency (
                           x_return_status=> x_return_status,
                           p_from_currency=> l_act_util_rec.plan_currency_code, --nirprasa,12.1.1 l_util_curr,
                           p_to_currency=> l_fund_amt_tbl (j).budget_currency,
                           p_conv_date=> l_act_util_rec.exchange_rate_date,
                           p_conv_type=> l_adj_info.exchange_rate_type, --Added for bug 7030415
                           p_from_amount=> l_adj_amount,
                           x_to_amount=> l_converted_adj_amount,
                           x_rate=> l_rate
                        );
               END IF;
               IF G_DEBUG THEN
                  ozf_utility_pvt.debug_message( '   Adj amount coverted '|| l_converted_adj_amount
                        || ' l_adj amount'   || l_adj_amount);
               END IF;

               IF x_return_status <> fnd_api.g_ret_sts_success THEN
                  IF G_DEBUG THEN
                     -- ozf_utility_pvt.error_message( '  Convert Currency '||x_return_status);
                     ozf_utility_pvt.debug_message( '  Convert Currency '||x_return_status);
                  END IF;
                  IF x_return_status = fnd_api.g_ret_sts_error THEN
                     RAISE fnd_api.g_exc_error;
                  ELSIF x_return_status = fnd_api.g_ret_sts_unexp_error THEN
                     RAISE fnd_api.g_exc_unexpected_error;
                  END IF;
               END IF;
               IF x_return_status = fnd_api.g_ret_sts_success THEN
                  IF l_act_util_rec.plan_currency_code = l_fund_amt_tbl (j).budget_currency THEN
                     l_act_budgets_rec.parent_src_apprvd_amt :=
                                 ozf_utility_pvt.currround (
                                    l_adj_amount,
                  --nirprasa,12.1.1 l_util_curr             );
                                    l_act_util_rec.plan_currency_code);
                  --nirprasa,12.1.1
                  ELSE
                     IF G_DEBUG THEN
                        ozf_utility_pvt.debug_message('in not equal currency');
                     END IF;
                     l_act_budgets_rec.parent_src_apprvd_amt :=
                                 ozf_utility_pvt.currround (
                                    l_converted_adj_amount,
                                    l_fund_amt_tbl (j).budget_currency );
                  END IF;

                  l_act_util_rec.product_id := l_adj_info.product_id;
                  l_act_util_rec.object_type := l_adj_info.object_type;
                  l_act_util_rec.object_id   := l_adj_info.object_id;
                  l_act_util_rec.product_level_type := 'PRODUCT';
                  -- yzhao: 11.5.10 02/23/2004 added billto_cust_account_id
                  l_act_util_rec.billto_cust_account_id := l_adj_info.billto_cust_account_id;
                  l_act_util_rec.cust_account_id := l_adj_info.cust_account_id;
                  l_act_util_rec.utilization_type := 'ADJUSTMENT';
                  l_act_util_rec.org_id := l_adj_info.org_id;
                  l_act_util_rec.ship_to_site_use_id := l_adj_info.ship_to_site_use_id;
                  l_act_util_rec.bill_to_site_use_id := l_adj_info.bill_to_site_use_id;

                  l_act_budgets_rec.justification := fnd_message.get_string ('OZF', 'OZF_ACR_VOL_BDADJ');
                  l_act_budgets_rec.transfer_type := 'UTILIZED';
                  l_act_budgets_rec.request_date := SYSDATE;
                  l_act_budgets_rec.status_code := 'APPROVED';
                  l_act_budgets_rec.user_status_id :=
                            ozf_utility_pvt.get_default_user_status (
                                'OZF_BUDGETSOURCE_STATUS',
                                l_act_budgets_rec.status_code  );
                  l_act_budgets_rec.budget_source_type := 'OFFR';
                  l_act_budgets_rec.budget_source_id := p_qp_list_header_id;
                  l_act_budgets_rec.arc_act_budget_used_by := 'OFFR';
                  l_act_budgets_rec.act_budget_used_by_id := p_qp_list_header_id;
                  l_act_budgets_rec.parent_src_curr := l_fund_amt_tbl (j).budget_currency;
                  l_act_budgets_rec.parent_source_id := l_fund_amt_tbl (j).ofr_src_id;
                  l_act_budgets_rec.request_amount :=
ozf_utility_pvt.currround (l_adj_amount, l_act_util_rec.plan_currency_code);
                  --nirprasa,12.2 ozf_utility_pvt.currround (l_adj_amount, l_util_curr);

                  l_act_budgets_rec.request_currency := l_act_util_rec.plan_currency_code; --l_util_curr;
                  l_act_budgets_rec.approved_amount := l_act_budgets_rec.request_amount;
                  l_act_budgets_rec.approved_in_currency := l_act_util_rec.plan_currency_code; --l_util_curr;
                  l_act_util_rec.fund_request_currency_code := l_util_curr;
                  --nirprasa,12.2 end.

                  IF l_adj_amount > 0 THEN
                     l_act_util_rec.adjustment_type :='STANDARD'; -- Seeded Data for Backdated Positive Adj
                     l_act_util_rec.adjustment_type_id := -7; -- Seeded Data for Backdated Positive Adj
                  ELSE
                     l_act_util_rec.adjustment_type :='DECREASE_EARNED'; -- Seeded Data for Backdated Negative Adj
                     l_act_util_rec.adjustment_type_id := -6; -- Seeded Data for Backdated Negative Adj
                  END IF;

                 IF l_act_budgets_rec.request_amount <> 0 THEN -- fix bug 4720113
                    ozf_fund_Adjustment_pvt.process_Act_budgets(
                                 x_return_status=> x_return_status,
                                 x_msg_count=> x_msg_count,
                                 x_msg_data=> x_msg_data,
                                 p_act_budgets_rec=> l_act_budgets_rec,
                                 p_act_util_rec=> l_act_util_rec,
                                 x_act_budget_id=> l_act_budget_id
                             );
                  write_conc_log(l_full_name ||'Process Act Budget' ||x_return_status);
                  IF G_DEBUG THEN
                     ozf_utility_pvt.debug_message('create utlization '|| x_return_status);
                  END IF;

                  IF x_return_status <> fnd_api.g_ret_sts_success THEN
                     IF x_return_status = fnd_api.g_ret_sts_error THEN
                        RAISE fnd_api.g_exc_error;
                     ELSIF x_return_status = fnd_api.g_ret_sts_unexp_error THEN
                        RAISE fnd_api.g_exc_unexpected_error;
                     END IF;
                  END IF;
                END IF;  --l_act_budgets_rec.request_amount <> 0
            --- quit when the total earned amount is adjusted
              END IF;
            <<l_endofearadjloop>>
            IF G_DEBUG THEN
               ozf_utility_pvt.debug_message ( 'l_remaining_amt ' || l_remaining_amt
                        || 'l_adj amount' || l_adj_amount || 'fund_id '|| l_fund_amt_tbl (j).ofr_src_id );
            END IF;
            EXIT WHEN l_remaining_amt = 0;
            END LOOP earned_adj_loop;
         END IF; --end of check for table count >0
      END IF; -- end of check for count
   EXCEPTION
      WHEN fnd_api.g_exc_error THEN
         ROLLBACK TO process_accrual;
         x_return_status            := fnd_api.g_ret_sts_error;
         fnd_msg_pub.count_and_get (p_count => x_msg_count, p_data => x_msg_data, p_encoded => fnd_api.g_false);
      WHEN fnd_api.g_exc_unexpected_error THEN
         ROLLBACK TO process_accrual;
         x_return_status            := fnd_api.g_ret_sts_unexp_error;
         fnd_msg_pub.count_and_get (p_count => x_msg_count, p_data => x_msg_data, p_encoded => fnd_api.g_false);
      WHEN OTHERS THEN
         ROLLBACK TO process_accrual;
         x_return_status            := fnd_api.g_ret_sts_unexp_error;
         IF fnd_msg_pub.check_msg_level (fnd_msg_pub.g_msg_lvl_unexp_error) THEN
            fnd_msg_pub.add_exc_msg (g_pkg_name, l_api_name);
         END IF;
         fnd_msg_pub.count_and_get (p_count => x_msg_count, p_data => x_msg_data, p_encoded => fnd_api.g_false);

   END process_accrual;




  PROCEDURE volume_offer_adjustment (
          p_qp_list_header_id     IN       NUMBER,
          p_vol_off_type          IN        VARCHAR2,
          p_init_msg_list         IN       VARCHAR2 := fnd_api.g_false,
          p_commit                IN       VARCHAR2 := fnd_api.g_false,
          x_return_status         OUT NOCOPY      VARCHAR2,
          x_msg_count             OUT NOCOPY      NUMBER,
          x_msg_data              OUT NOCOPY      VARCHAR2
       ) IS 
    
         CURSOR  c_old_price_Adj(p_list_header_id IN NUMBER)  IS
          SELECT old_Adj_amt,order_line_id
                ,price_adjustment_id
                ,gl_date
                ,object_type
                ,object_id
                ,gl_posted_flag
                ,utilization_id 
                FROM
          ( SELECT  sum(plan_curr_amount)  old_Adj_amt
                , order_line_id
                ,min(price_adjustment_id) price_adjustment_id
                ,min(gl_date) gl_date
                 ,object_type
                 ,object_id
                ,'Y' gl_posted_flag
                ,min(utilization_id) utilization_id
            FROM ozf_funds_utilized_all_b
            WHERE plan_id = p_list_header_id
             AND plan_type = 'OFFR'
            -- AND gl_date is not NULL -- only process shipped or invoiced order.
             AND gl_posted_flag IN('Y','F')
             AND utilization_type IN ( 'ACCRUAL','LEAD_ACCRUAL','UTILIZED', 'ADJUSTMENT', 'LEAD_ADJUSTMENT')
             AND price_adjustment_id IS NOT NULL
             GROUP BY order_line_id,object_type,object_id);
             
    
         CURSOR  c_resale_line_info(p_resale_line_id IN NUMBER, p_adj_id IN NUMBER)  IS
            SELECT line.quantity ordered_quantity ,
                 line.quantity shipped_quantity,
                 line.quantity invoiced_quantity,
                 adj.priced_unit_price unit_list_price,
                 line.resale_line_id line_id,
                 NVL(line.date_shipped, line.date_ordered) actual_shipment_date,
                 NVL(line.date_shipped, line.date_ordered) fulfillment_date,  -- invoiced date ?????
                 line.inventory_item_id,
                 line.currency_code --dummy column
            FROM OZF_RESALE_LINES_ALL line,ozf_resale_adjustments_all adj
            WHERE line.resale_line_id = p_resale_line_id
            AND adj.resale_adjustment_id = p_adj_id
            AND line.resale_line_id = adj.resale_line_id;
    
    
         CURSOR  c_prior_tiers(p_parent_discount_id  IN NUMBER, p_volume IN NUMBER ) IS
           SELECT  offer_discount_line_id ,volume_from ,volume_to, discount
             FROM  ozf_offer_discount_lines
             WHERE   parent_discount_line_id = p_parent_discount_id
             AND   p_volume >= volume_from
             ORDER BY volume_from  DESC;
    
    
         CURSOR c_discount_header(p_discount_line_id IN NUMBER) IS
             SELECT discount_type,volume_type
              FROM ozf_offer_discount_lines
              WHERE offer_discount_line_id = p_discount_line_id
              AND tier_type = 'PBH';
    
         CURSOR c_get_group(p_order_line_id IN NUMBER,p_list_header_id IN NUMBER) IS
           SELECT group_no,pbh_line_id,include_volume_flag
            FROM ozf_order_group_prod
            WHERE order_line_id = p_order_line_id
            AND qp_list_header_id = p_list_header_id;
    
         CURSOR c_market_option(p_list_header_id IN NUMBER, p_group_id IN NUMBER) IS
           SELECT opt.retroactive_flag
            FROM ozf_offr_market_options opt
            WHERE opt.GROUP_NUMBER= p_group_id
            AND opt.qp_list_header_id = p_list_header_id;
    
    --fix for bug 5975203
         CURSOR c_current_discount(p_volume IN NUMBER, p_parent_discount_id IN NUMBER) IS
             SELECT discount
            FROM ozf_offer_discount_lines
            WHERE p_volume > volume_from
                 AND p_volume <= volume_to
             AND parent_discount_line_id = p_parent_discount_id;
    
    /*    CURSOR c_max_volume(p_order_line_id IN NUMBER, p_qp_list_header_id IN NUMBER,p_source_code IN VARCHAR2) IS
           SELECT summ.individual_volume
           FROM ozf_volume_detail det,ozf_volume_summary summ
           WHERE det.order_line_id = p_order_line_id
           AND det.qp_list_header_id = p_qp_list_header_id
           AND det.volume_track_type = summ.individual_type
           AND det.qp_list_header_id = summ.qp_list_header_id
           AND det.source_code = p_source_code;
           
    */
         
         
           CURSOR  c_order_line_info(p_order_line_id IN NUMBER)  IS
	          SELECT DECODE(line.line_category_code,'ORDER',line.ordered_quantity,
	                                                                              'RETURN', -line.ordered_quantity) ordered_quantity,
	               DECODE(line.line_category_code,'ORDER',NVL(line.shipped_quantity,line.ordered_quantity),
	                                                                              'RETURN', line.invoiced_quantity,
	                                                                              line.ordered_quantity) shipped_quantity,
	               line.invoiced_quantity,
	               line.unit_list_price,
	               line.line_id,
	               line.actual_shipment_date,
	               line.fulfillment_date,  -- invoiced date ?????
	               line.inventory_item_id,
	               header.transactional_curr_code
	          FROM oe_order_lines_all line, oe_order_headers_all header
	          WHERE line.line_id = p_order_line_id
          AND line.header_id = header.header_id;
         
         CURSOR c_preset_tier(p_pbh_line_id IN NUMBER, p_qp_list_header_id IN NUMBER,p_group_id IN NUMBER) IS
           SELECT a.discount
           FROM   ozf_offer_discount_lines a, ozf_market_preset_tiers b, ozf_offr_market_options c
           WHERE  a.offer_discount_line_id = b.dis_offer_discount_id
           AND    b.pbh_offer_discount_id = p_pbh_line_id
           AND    b.offer_market_option_id = c.offer_market_option_id
           AND    c.qp_list_header_id = p_qp_list_header_id
           AND    c.group_number = p_group_id;
    
        CURSOR c_sales_accrual(p_list_header_id  IN NUMBER) IS
           SELECT 'X' from ozf_funds_all_b
           WHERE plan_id= p_list_header_id
           AND accrual_basis = 'SALES'
           UNION
           SELECT 'X' from ozf_funds_all_b
           WHERE plan_id = p_list_header_id
           AND accrual_basis = 'CUSTOMER'
           AND liability_flag = 'N';
    
        CURSOR c_unit_discount(p_order_line_id  IN NUMBER, p_price_adjust_id NUMBER) IS
           SELECT SUM(adjusted_amount_per_pqty)
           FROM oe_price_adjustments
           WHERE line_id = p_order_line_id
           AND accrual_flag = 'N'
           AND applied_flag = 'Y'
           AND list_line_type_code IN ('DIS', 'SUR', 'PBH', 'FREIGHT_CHARGE')
           and pricing_group_sequence <
           (SELECT pricing_group_sequence FROM oe_price_adjustments
             WHERE price_Adjustment_id = p_price_adjust_id) ;
    
        CURSOR c_discount(p_order_line_id  IN NUMBER, p_price_adjust_id NUMBER) IS
           SELECT SUM(adjusted_amount_per_pqty)
           FROM oe_price_adjustments
           WHERE line_id = p_order_line_id
           AND accrual_flag = 'N'
           AND applied_flag = 'Y'
           AND list_line_type_code IN ('DIS', 'SUR', 'PBH', 'FREIGHT_CHARGE');
    
        CURSOR  c_get_tier_limits (p_parent_discount_id IN NUMBER) IS
           SELECT MIN(volume_from),MAX(volume_to)
           FROM ozf_offer_discount_lines
           WHERE parent_discount_line_id = p_parent_discount_id;
    
         CURSOR  c_get_max_tier (p_max_volume_to IN NUMBER,p_parent_discount_id IN NUMBER)    IS
            SELECT  discount
            FROM ozf_offer_discount_lines
            WHERE volume_to =p_max_volume_to
            AND parent_discount_line_id = p_parent_discount_id;
    
         CURSOR c_offer_curr IS
          SELECT nvl(transaction_currency_code,fund_request_curr_code),
                 transaction_currency_code,
                 offer_id
            FROM ozf_offers
           WHERE qp_list_header_id = p_qp_list_header_id;
    
         --22-FEB-2007 kdass bug 5759350 - changed datatype of p_product_id from NUMBER to VARCHAR2 based on Feng's suggestion
         --fix for bug 5979971
       CURSOR c_apply_discount(p_offer_id IN NUMBER,p_line_id IN NUMBER) IS
            SELECT NVL(apply_discount_flag,'N')
            FROM ozf_order_group_prod
            WHERE offer_id = p_offer_id
              AND order_line_id = p_line_id;
    
    
         l_api_name                CONSTANT VARCHAR2(30)   := 'volume_offer_adjustment';
         l_full_name               VARCHAR2(70):= g_pkg_name ||'.'||l_api_name ||' : ';
         l_api_version             CONSTANT NUMBER                 := 1.0;
         l_return_status           VARCHAR2 (20) :=  fnd_api.g_ret_sts_success;
         l_msg_count               NUMBER;
         l_msg_data                VARCHAR2 (2000)        := NULL;
         l_volume_offer_tier_id    NUMBER;
         l_current_offer_tier_id   NUMBER;
         l_order_amount            NUMBER;
         l_old_discount            NUMBER;
         l_new_discount            NUMBER;--
         l_new_operator            VARCHAR2(30);
         l_old_operator            VARCHAR2(30);
         y1                        NUMBER; -- Initial Adjsutment
         l_current_max_tier        NUMBER;
         l_current_min_tier        NUMBER;
         l_act_util_rec            ozf_actbudgets_pvt.act_util_rec_type    ;
         l_adj_amount              NUMBER;
         l_volume_type             VARCHAR2(30);
         l_current_tier_value      NUMBER;
         l_total                   NUMBER;
         l_value                   NUMBER;
         l_previous_tier_max       NUMBER;
         l_new_utilization         NUMBER;
         l_total_order             NUMBER;
         l_total_amount            NUMBER;
         l_returned_flag           BOOLEAN := false;
         l_qp_list_header_id       NUMBER := p_qp_list_header_id;
         l_retroactive             VARCHAR2(1) ;
         l_trx_date                DATE;
         l_volume                  NUMBER;
         l_group_id                NUMBER;
         l_pbh_line_id             NUMBER;
         l_discount_type           VARCHAR2(30);
         l_source_code             VARCHAR2(30);
         l_preset_tier             NUMBER;
         l_order_line_info         c_order_line_info%ROWTYPE;
         l_order_line_id           NUMBER;
         l_order_type              VARCHAR2(30);
         l_sales_accrual_flag      VARCHAR2 (3);
         l_volume_offer_type       VARCHAR2(30) := p_vol_off_type;
         l_selling_price           NUMBER;
         l_unit_discount           NUMBER;
         l_min_tier                NUMBER;
         l_max_tier                NUMBER;
         l_offer_curr              VARCHAR2(30);
         l_conv_price              NUMBER;
         l_rate                    NUMBER;
         l_included_vol_flag       VARCHAR2(1);
         l_amountTbl               amountTbl ;
         l_glDateTbl               glDateTbl ;
         l_objectTypeTbl           objectTypeTbl ;
         l_objectIdTbl             objectIdTbl;
         l_priceAdjustmentIDTbl    priceAdjustmentIDTbl ;
         l_glPostedFlagTbl         glPostedFlagTbl;
         l_orderLineIdTbl          orderLineIdTbl;
    
    
         l_offer_id                NUMBER;
         l_apply_discount          VARCHAR2(1) ;
         l_transaction_currency_code  VARCHAR2(30);
    
         -- julou bug 6348078. cursor to get transaction_date for IDSM line.
         CURSOR c_trx_date(p_line_id NUMBER) IS
         SELECT transaction_date
         FROM   ozf_sales_transactions
         WHERE  source_code = 'IS'
         AND    line_id = p_line_id;
    
         --Added for bug 7030415
         l_utilizationIdTbl        utilizationIdTbl;
         CURSOR c_utilization_details(l_utilization_id IN NUMBER) IS
            SELECT exchange_rate_type, org_id
            FROM ozf_funds_utilized_all_b
            WHERE utilization_id=l_utilization_id;
    
         l_conv_type       ozf_funds_utilized_all_b.exchange_rate_type%TYPE;
         l_org_id          NUMBER;
         
         
         l_back_to_volume   NUMBER;
         l_offer_id1        NUMBER;
         l_min_discount     NUMBER;
         
         l_as_of_date_discount NUMBER;
         l_adjustment_amount   NUMBER;
         l_adj_line_amount     NUMBER;
         
              
          CURSOR c_back_to_vol(p_list_header_id IN NUMBER) IS  --CGADGE
          SELECT TO_NUMBER(attribute9)
            FROM qp_list_headers_b
           WHERE list_header_id=p_list_header_id; 
          
          CURSOR c_offer_id(p_list_header_id IN NUMBER) IS  --CGADGE
  	  SELECT offer_id
  	  FROM   ozf_offers
           WHERE qp_list_header_id=p_list_header_id; 
           
          CURSOR c_min_discount(p_amount IN NUMBER,p_offer_id IN NUMBER) IS
          SELECT discount 
            FROM ozf_offer_discount_lines 
           WHERE offer_id=p_offer_id
             AND p_amount BETWEEN volume_from and volume_to;
      
       BEGIN
          
          --IF G_DEBUG THEN
            -- ozf_utility_pvt.debug_message(' /*************************** DEBUG MESSAGE START *************************/' || l_api_name);
          --END IF;
            -- write_conc_log(' /*************************** DEBUG MESSAGE START *************************/' || l_api_name);
    
          SAVEPOINT volume_offer_adjustment;
          

          /*
          IF g_offer_id_tbl.FIRST IS NOT NULL THEN
             FOR i IN g_offer_id_tbl.FIRST .. g_offer_id_tbl.LAST
             LOOP
                IF g_offer_id_tbl(i) = l_qp_list_header_id THEN
                   write_conc_log (' no adjustment for offer: ' || l_qp_list_header_id);
                   GOTO l_endoffloop;
                END IF;
             END LOOP;
          END IF; */
          
           
          
          OPEN   c_back_to_vol(l_qp_list_header_id);
  	   FETCH c_back_to_vol INTO l_back_to_volume;
          CLOSE  c_back_to_vol;
          
          
          OPEN  c_offer_id(l_qp_list_header_id);
            FETCH c_offer_id INTO l_offer_id1;
  	  CLOSE c_offer_id;
  	  
          
          
          OPEN  c_min_discount(l_back_to_volume,l_offer_id1);
  	    FETCH c_min_discount INTO l_min_discount;
  	  CLOSE c_min_discount;
    
         
          OPEN c_sales_accrual(l_qp_list_header_id);
          FETCH c_sales_accrual INTO l_sales_accrual_flag;
          CLOSE c_sales_accrual;
    
          
          write_conc_log('CHECK5 '||l_sales_accrual_flag);
          IF l_sales_accrual_flag is NOT NULL THEN
             l_order_type := 'SHIPPED'; --'BOOKED'; -- set to shipped for sales accrual untill decision has been made.
          ELSIF g_order_gl_phase ='SHIPPED' AND l_volume_offer_type = 'ACCRUAL' THEN
             l_order_type := 'SHIPPED';
          ELSIF g_order_gl_phase ='INVOICED' AND l_volume_offer_type = 'ACCRUAL' THEN
             l_order_type := 'INVOICED';
          ELSIF  l_volume_offer_type = 'OFF_INVOICE' THEN
             l_order_type := 'INVOICED';
          END IF;
    
               l_total_order := 0;  -- total ordered amount for offer.
               l_total_amount := 0; --- total utilization amount for offer.
             
               OPEN c_old_price_adj(l_qp_list_header_id);
  	       LOOP
  	        FETCH c_old_price_adj BULK COLLECT INTO l_amountTbl, l_orderLineIdTbl
  	                                              , l_priceAdjustmentIDTbl, l_glDateTbl
  	                                              , l_objectTypeTbl, l_objectIdTbl, l_glPostedFlagTbl, l_utilizationIdTbl --Added for bug 7030415
  	                                             LIMIT g_bulk_limit;
  	                      
                    FOR i IN NVL(l_priceAdjustmentIDTbl.FIRST, 1) .. NVL(l_priceAdjustmentIDTbl.LAST, 0) LOOP
                       l_total_amount := l_total_amount + l_amountTbl(i);     
                    END LOOP;
                EXIT WHEN c_old_price_adj%NOTFOUND;   
                END LOOP;
               CLOSE c_old_price_adj; 
               
                      
               OPEN c_old_price_adj(l_qp_list_header_id);
               --FOR l_old_price_adj IN c_old_price_adj(l_qp_list_header_id)
               LOOP
                 FETCH c_old_price_adj BULK COLLECT INTO l_amountTbl, l_orderLineIdTbl
                                                           , l_priceAdjustmentIDTbl, l_glDateTbl
                                                           , l_objectTypeTbl, l_objectIdTbl, l_glPostedFlagTbl, l_utilizationIdTbl --Added for bug 7030415
                                                           LIMIT g_bulk_limit;
                   
                   FOR i IN NVL(l_priceAdjustmentIDTbl.FIRST, 1) .. NVL(l_priceAdjustmentIDTbl.LAST, 0) LOOP
                      IF l_objectTypeTbl(i) ='ORDER' THEN
                         IF G_DEBUG THEN
                            ozf_utility_pvt.debug_message(' order_line_id:  '|| l_orderLineIdTbl(i) );
                         END IF;
                         write_conc_log(' order_line_id:  '|| l_orderLineIdTbl(i) );
    
                         l_source_code := 'OM';
                         l_order_line_id := l_orderLineIdTbl(i);
                         OPEN c_order_line_info(l_order_line_id);
                         FETCH c_order_line_info INTO l_order_line_info;
                         CLOSE c_order_line_info;
    
                         IF l_priceAdjustmentIDTbl(i) = -1 THEN
                            OPEN c_discount(l_order_line_id,l_priceAdjustmentIDTbl(i));
                            FETCH c_discount INTO l_unit_discount;
                            CLOSE c_discount;
                         ELSE
                            OPEN c_unit_discount(l_order_line_id,l_priceAdjustmentIDTbl(i));
                            FETCH c_unit_discount INTO l_unit_discount;
                            CLOSE c_unit_discount;
                         END IF;
    
                         write_conc_log(' l_unit_discount:  '|| l_unit_discount);
    
                      ELSE
                         IF G_DEBUG THEN
                           ozf_utility_pvt.debug_message(' resale_line_id:  '|| l_objectIdTbl(i) );
                         END IF;
                         write_conc_log(' resale_line_id:  '|| l_objectIdTbl(i));
    
                         l_source_code := 'IS';
                         l_order_line_id := l_objectIdTbl(i);
                         OPEN c_resale_line_info(l_order_line_id,l_priceAdjustmentIDTbl(i));
                         FETCH c_resale_line_info INTO l_order_line_info;
                         CLOSE c_resale_line_info;
                      END IF;
    
                      l_selling_price := l_order_line_info.unit_list_price + NVL(l_unit_discount,0); -- discount is negative
                      write_conc_log(' l_selling_price:  '|| l_selling_price);
    
                      OPEN c_offer_curr;
                      FETCH c_offer_curr INTO l_offer_curr, l_transaction_currency_code, l_offer_id;
                      CLOSE c_offer_curr;
    
                      IF l_amountTbl(i) = 0 THEN -- fix bug 5689866
                         --21-MAY-07 kdass fixed bug 6059036 - added condition for direct and indirect orders
                         IF l_objectTypeTbl(i) ='ORDER' THEN
                            OPEN c_apply_discount(l_offer_id, l_orderLineIdTbl(i));
                            FETCH c_apply_discount INTO l_apply_discount;
                            CLOSE c_apply_discount;
                         ELSE
                            OPEN c_apply_discount(l_offer_id, l_objectIdTbl(i));
                            FETCH c_apply_discount INTO l_apply_discount;
                            CLOSE c_apply_discount;
                         END IF;
    
                         write_conc_log('l_apply_discount:  ' || l_apply_discount);
    
                         IF l_apply_discount ='N' THEN
                           IF G_DEBUG THEN
                              ozf_utility_pvt.debug_message('not apply discount:  ' || l_order_line_info.inventory_item_id);
                           END IF;
                           write_conc_log(' not apply discount:'|| l_order_line_info.inventory_item_id);
                           GOTO l_endoffloop;
                         END IF;
                      END IF; -- bug  5689866
    
                        --Added for bug 7030415
                         OPEN c_utilization_details(l_utilizationIdTbl(i));
                         FETCH c_utilization_details INTO l_conv_type,l_org_id;
                         CLOSE c_utilization_details;
    
                         l_act_util_rec.org_id := l_org_id;
    
              --12.2, multi-currency enhancement.
              IF l_transaction_currency_code IS NOT NULL
              AND l_transaction_currency_code <> l_order_line_info.transactional_curr_code THEN
    
                 ozf_utility_pvt.write_conc_log('order curr: ' || l_order_line_info.transactional_curr_code);
                 ozf_utility_pvt.write_conc_log('offer curr: ' || l_transaction_currency_code);
                 ozf_utility_pvt.write_conc_log('selling price: ' || l_selling_price);
    
    
    
                         ozf_utility_pvt.write_conc_log('l_conv_type: ' || l_conv_type);
    
    
                 ozf_utility_pvt.convert_currency (x_return_status => l_return_status
                                                  ,p_conv_type     => l_conv_type --7030415
                                                  ,p_conv_date     => OZF_ACCRUAL_ENGINE.G_FAE_START_DATE
                                                  --l_order_line_info.actual_shipment_date
                                                  ,p_from_currency => l_order_line_info.transactional_curr_code
                                                  ,p_to_currency   => l_transaction_currency_code
                                                  ,p_from_amount   => l_selling_price
                                                  ,x_to_amount     => l_conv_price
                                                  ,x_rate          => l_rate
                                                  );
    
                         IF l_return_status = fnd_api.g_ret_sts_error THEN
                            RAISE fnd_api.g_exc_error;
                         ELSIF l_return_status = fnd_api.g_ret_sts_unexp_error THEN
                            RAISE fnd_api.g_exc_unexpected_error;
                         END IF;
    
                         l_selling_price := l_conv_price;
                         write_conc_log ('selling price after currency conversion: ' || l_selling_price);
    
                      END IF;
    
    
                      OPEN c_get_group(l_order_line_id,l_qp_list_header_id);
                      FETCH c_get_group INTO l_group_id,l_pbh_line_id,l_included_vol_flag;
                      CLOSE c_get_group;
    
                      IF G_DEBUG THEN
                        ozf_utility_pvt.debug_message(' l_group_id:  '|| l_group_id );
                        ozf_utility_pvt.debug_message(' l_pbh_line_id:  '|| l_pbh_line_id );
                        ozf_utility_pvt.debug_message(' l_included_vol_flag:  '|| l_included_vol_flag );
                      END IF;
                      
                      write_conc_log(' l_group_id:  '|| l_group_id );
                      write_conc_log(' l_pbh_line_id:  '|| l_pbh_line_id );
                      write_conc_log(' l_included_vol_flag:  '|| l_included_vol_flag );
    
                      IF l_group_id is NULL OR l_pbh_line_id is NULL THEN
                         GOTO l_endoffloop;
                      END IF;
    
                      OPEN c_market_option(l_qp_list_header_id,l_group_id);
                      FETCH c_market_option INTO l_retroactive;
                      CLOSE c_market_option;
    
                      OPEN c_discount_header(l_pbh_line_id);
                      FETCH c_discount_header INTO l_discount_type,l_volume_type;
                      CLOSE c_discount_header;
    
    
                      IF l_retroactive = 'Y' THEN
                         ozf_volume_calculation_pub.get_volume
                                             (p_init_msg_list =>fnd_api.g_false
                                              ,p_api_version =>1.0
                                              ,p_commit  =>fnd_api.g_false
                                              ,x_return_status =>l_return_status
                                              ,x_msg_count => l_msg_count
                                              ,x_msg_data  => l_msg_data
                                              ,p_qp_list_header_id => l_qp_list_header_id
                                              ,p_order_line_id =>l_order_line_id
                                              ,p_trx_date   =>sysdate+1
                                              ,p_source_code => l_source_code
                                              ,x_acc_volume => l_volume
                                              );
                       END IF;
    
                      IF l_return_status = fnd_api.g_ret_sts_error THEN
                        RAISE fnd_api.g_exc_error;
                      ELSIF l_return_status = fnd_api.g_ret_sts_unexp_error THEN
                        RAISE fnd_api.g_exc_unexpected_error;
                      END IF;
        
    
                      IF G_DEBUG THEN
                         ozf_utility_pvt.debug_message(' l_volume:  '|| l_volume );
                      END IF;
                      write_conc_log(' l_volume:  '|| l_volume );
    
                     -- l_new_discount := 0;
                      
                      OPEN c_current_discount(l_volume,l_pbh_line_id);
                      FETCH c_current_discount INTO l_new_discount;
                      CLOSE c_current_discount;
    
                      -- fix bug 5055425 by feliu on 02/23/2006
                      
                      IF l_new_discount  is NULL THEN
                         OPEN c_get_tier_limits(l_pbh_line_id);
                         FETCH c_get_tier_limits INTO l_min_tier,l_max_tier;
                         CLOSE c_get_tier_limits;
                         IF l_volume < l_min_tier THEN
                            l_new_discount := 0;
                         ELSE
                            OPEN c_get_max_tier(l_max_tier,l_pbh_line_id);
                            FETCH c_get_max_tier INTO l_new_discount;
                            CLOSE c_get_max_tier;
                         END IF;
                         IF G_DEBUG THEN
                            ozf_utility_pvt.debug_message(' l_new_discount:  '|| l_new_discount );
                         END IF;
                         write_conc_log(' l_new_discount:  '|| l_new_discount );
                      END IF;
    
                      l_preset_tier := NULL;
                      OPEN c_preset_tier(l_pbh_line_id,l_qp_list_header_id,l_group_id);
                      FETCH c_preset_tier INTO l_preset_tier;
                      CLOSE c_preset_tier;
    
                       write_conc_log( ' l_preset_tier=' || l_preset_tier);
                       write_conc_log( ' l_new_discount=' || l_new_discount);
    
    
                      IF l_preset_tier is NOT NULL AND l_preset_tier > l_new_discount THEN
                      l_new_discount := l_preset_tier;
                        IF G_DEBUG THEN
                           ozf_utility_pvt.debug_message('not reach preset tier:  ');
                        END IF;
                        write_conc_log(' not reach preset tier:');
                      END IF;
                      
                      l_as_of_date_discount := 0;
                      l_adjustment_amount   := 0;
                      l_adj_line_amount     := 0;
                      
                      l_as_of_date_discount := l_new_discount * (l_volume - l_back_to_volume)/100 + l_back_to_volume * l_min_discount/100;
                      
			    IF l_as_of_date_discount < l_total_amount THEN
				 l_adjustment_amount := l_total_amount - l_as_of_date_discount;
				 l_adj_line_amount := l_adjustment_amount * l_amountTbl(i)/l_total_amount;
			    END IF;   

                        l_new_utilization := 0;
                        l_value :=0;
                        l_adj_amount := 0;
                        
                        l_adj_amount := -l_adj_line_amount;
    
                       IF G_DEBUG THEN
                             ozf_utility_pvt.debug_message(l_full_name ||' retroactive flag is Y. ' || ' l_volume_type=' || l_volume_type
                                         || ' l_new_discount='  || l_new_discount
                                         || ' l_new_utilization='  || l_new_utilization
                                         || ' l_amountTbl=' || l_amountTbl(i)
                                         || ' l_adj_amount='  || l_adj_amount);
                         END IF;
                         write_conc_log(l_full_name ||' retroactive flag is Y. ' || ' l_volume_type=' || l_volume_type
                                         || ' l_new_discount='  || l_new_discount
                                         || ' l_new_utilization='  || l_new_utilization
                                         || ' l_amountTbl=' || l_amountTbl(i)
                                         || ' l_adj_amount='  || l_adj_amount);
    
                     -- END IF;  --l_retroactive 
    
                      
                      l_act_util_rec.price_Adjustment_id     := l_priceAdjustmentIDTbl(i);
                      l_act_util_rec.order_line_id  := l_orderLineIdTbl(i);
                      l_act_util_rec.gl_posted_flag := l_glPostedFlagTbl(i);
                      l_act_util_rec.object_type := l_objectTypeTbl(i);
                      
                      --nirprasa, 12.2 assign the currencies.
                      
                      IF l_transaction_currency_code IS NULL THEN
                         l_act_util_rec.plan_currency_code := l_order_line_info.transactional_curr_code;
                      ELSE
                         l_act_util_rec.plan_currency_code := l_offer_curr;
                      END IF;
                      
                      l_act_util_rec.fund_request_currency_code := l_offer_curr;
                      l_act_util_rec.exchange_rate_date := OZF_ACCRUAL_ENGINE.G_FAE_START_DATE;
                      
                      --nirprasa, 12.2 end assign the currencies.
    
                      IF NVL(l_adj_amount,0) <> 0 THEN
                         process_accrual (
                           p_earned_amt          =>l_adj_amount,
                           p_qp_list_header_id   =>l_qp_list_header_id,
                           p_act_util_rec        =>l_act_util_rec,
                           x_return_status       =>l_return_status,
                           x_msg_count           =>l_msg_count,
                           x_msg_data            =>l_msg_data );
    
                         IF l_return_status = fnd_api.g_ret_sts_error THEN
                           RAISE fnd_api.g_exc_error;
                         ELSIF l_return_status = fnd_api.g_ret_sts_unexp_error THEN
                           RAISE fnd_api.g_exc_unexpected_error;
                         END IF;
                      END IF;
    
                      IF G_DEBUG THEN
                         ozf_utility_pvt.debug_message(
                             l_full_name ||' Process Accrual Msg count '||l_msg_count||' Msg data'||l_msg_data||' Return status'||l_return_status
                          );
                      END IF;
                      write_conc_log(
                         l_full_name ||' Process Accrual Msg count '||l_msg_count||' Msg data'||l_msg_data||' Return status'||l_return_status
                      );
    
                      <<l_endoffloop>>
                      NULL;
                   END LOOP; -- loop for For
                   EXIT WHEN c_old_price_adj%NOTFOUND;
               END LOOP; -- end price adj loop
               CLOSE c_old_price_adj;
    
               IF l_return_status = fnd_api.g_ret_sts_error THEN
                  RAISE fnd_api.g_exc_error;
               ELSIF l_return_status = fnd_api.g_ret_sts_unexp_error THEN
                  RAISE fnd_api.g_exc_unexpected_error;
               END IF; 
    
               <<l_endoffloop>>
               NULL;
    
               IF G_DEBUG THEN
                 ozf_utility_pvt.debug_message(' /*************************** DEBUG MESSAGE END *************************/' || l_api_name );
               END IF;
               
               write_conc_log(' /*************************** DEBUG MESSAGE END *************************/' || l_api_name );
    
               fnd_msg_pub.count_and_get (p_count => x_msg_count, p_data => x_msg_data, p_encoded => fnd_api.g_false);
    
               x_return_status := l_return_status;
    
            EXCEPTION
               WHEN fnd_api.g_exc_error THEN
                   ROLLBACK TO volume_offer_adjustment;
                   x_return_status            := fnd_api.g_ret_sts_error;
                   fnd_msg_pub.count_and_get (p_count => x_msg_count, p_data => x_msg_data, p_encoded => fnd_api.g_false);
               WHEN fnd_api.g_exc_unexpected_error THEN
                   ROLLBACK TO volume_offer_adjustment;
                   x_return_status            := fnd_api.g_ret_sts_unexp_error;
                   fnd_msg_pub.count_and_get (p_count => x_msg_count, p_data => x_msg_data, p_encoded => fnd_api.g_false);
               WHEN OTHERS THEN
                   ROLLBACK TO volume_offer_adjustment;
                   x_return_status            := fnd_api.g_ret_sts_unexp_error;
                   IF fnd_msg_pub.check_msg_level (fnd_msg_pub.g_msg_lvl_unexp_error) THEN
                      fnd_msg_pub.add_exc_msg (g_pkg_name, l_api_name);
                   END IF;
                   fnd_msg_pub.count_and_get (p_count => x_msg_count, p_data => x_msg_data, p_encoded => fnd_api.g_false);
    
    
  END volume_offer_adjustment;
  
  PROCEDURE volume_offer_adjustment(errbuf    	      IN OUT VARCHAR2
                                   ,retcode           IN OUT VARCHAR2
                                   ,p_list_header_id  IN NUMBER) IS
  
   l_return_status           VARCHAR2 (20) :=  fnd_api.g_ret_sts_success;
   l_msg_count               NUMBER;
   l_msg_data                VARCHAR2 (2000)        := NULL;
   
   l_err_callfrom  VARCHAR2(75) DEFAULT 'XXCUS_OZF_VOLOFFRADJ_PKG';
   l_err_callpoint VARCHAR2(75) DEFAULT 'Volume Offer Adjustment';
   l_distro_list   VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
   
   v_message          VARCHAR2(250);
   l_message          VARCHAR2(150);
   l_err_msg          VARCHAR2(3000);
   l_err_code         NUMBER;
   l_sec              VARCHAR2(500);
   l_req_id           NUMBER := FND_GLOBAL.CONC_REQUEST_ID;
   
   CURSOR c_get_rebates IS
    SELECT list_header_id
      FROM qp_list_headers_vl qlhv,
           ozf_offers oo,
           ams_media_vl med
     WHERE oo.qp_list_header_id=qlhv.list_header_id
       AND med.media_id=oo.activity_media_id
       AND med.MEDIA_TYPE_CODE ='DEAL'
       AND oo.status_code='ACTIVE'
       AND oo.qp_list_header_id=NVL(p_list_header_id,oo.qp_list_header_id)
       AND med.DESCRIPTION IN ('TIER_BACK_$','TIER_PS_BACK_$');
  BEGIN
     
     SAVEPOINT volume_offer_adjustment1; 
     retcode := 0;
     
     
    FOR i IN  c_get_rebates LOOP  
     
     volume_offer_adjustment (
             p_qp_list_header_id     => i.list_header_id,
             p_vol_off_type          => 'ACCRUAL',
             p_init_msg_list         => fnd_api.g_false,
             p_commit                => fnd_api.g_false,
             x_return_status         => l_return_status,
             x_msg_count             => l_msg_count,
             x_msg_data              => l_msg_data);
     
     END LOOP;
     
     
          write_conc_log('l_return_status = '||SUBSTR(l_return_status,1,255));
          write_conc_log('l_msg_count = '||TO_CHAR(l_msg_count));
          write_conc_log('l_msg_data = '|| SUBSTR (l_msg_data,1,255));
          
          IF l_return_status = FND_API.g_ret_sts_error THEN
             write_conc_log('Expected Error in Volume Offer Adjustment');
             RAISE FND_API.G_EXC_ERROR;
            ELSIF l_return_status = FND_API.g_ret_sts_unexp_error THEN
             write_conc_log('Unexpected Error in Volume Offer Adjustment');
             RAISE FND_API.G_EXC_UNEXPECTED_ERROR;
          END IF;
             
     EXCEPTION
           
           WHEN fnd_api.g_exc_error THEN
                 l_return_status := FND_API.G_RET_STS_ERROR;
              -- Standard call to get message count and if count=1, get the message
		 
		 FND_MSG_PUB.Count_And_Get(p_encoded => FND_API.G_FALSE,
		                           p_count   => l_msg_count,
		                           p_data    => l_msg_data);
		 FOR i in 0..l_msg_count loop
		    write_conc_log(substr(fnd_msg_pub.get(p_msg_index => i, p_encoded => 'F'), 1, 254));
		 END LOOP;    
		         
		 RETCODE := 2;
		 ERRBUF  := 'Volume Offer Adjustment Completed with Error';
		         
		 xxcus_error_pkg.xxcus_error_main_api(p_called_from 	  => l_err_callfrom
		 	                             ,p_calling           => l_err_callpoint
		 	                             ,p_request_id        => l_req_id
		 	                             ,p_ora_error_msg     => SQLERRM
		 	                             ,p_error_desc        => 'Error running XXCUS_OZF_VOLOFFRADJ_PKG.Volume Offer Adjustment package with PROGRAM ERROR'
		 	                             ,p_distribution_list => l_distro_list
                                                     ,p_module            => 'TM');
                
                --ROLLBACK TO volume_offer_adjustment1;
            
            WHEN fnd_api.g_exc_unexpected_error THEN
                 l_return_status := FND_API.G_RET_STS_UNEXP_ERROR;
              -- Standard call to get message count and if count=1, get the message
		 
		 FND_MSG_PUB.Count_And_Get(p_encoded => FND_API.G_FALSE,
		                           p_count   => l_msg_count,
		                           p_data    => l_msg_data);
		  FOR i in 0..l_msg_count loop
		     write_conc_log(substr(fnd_msg_pub.get(p_msg_index => i, p_encoded => 'F'), 1, 254));
		  END LOOP;                          
		          
		  RETCODE := 2;
		  ERRBUF  := 'Volume Offer Adjustment Completed with Error';
		 	         
		  xxcus_error_pkg.xxcus_error_main_api(p_called_from 	  => l_err_callfrom
		 		 	             ,p_calling           => l_err_callpoint
		 		 	             ,p_request_id        => l_req_id
		 		 	             ,p_ora_error_msg     => SQLERRM
		 		 	             ,p_error_desc        => 'Error running XXCUS_OZF_VOLOFFRADJ_PKG.Volume Offer Adjustment package with PROGRAM ERROR'
		 		 	             ,p_distribution_list => l_distro_list
		                                     ,p_module            => 'TM');
                

                --ROLLBACK TO volume_offer_adjustment1;
                
           WHEN OTHERS THEN
                 ROLLBACK TO volume_offer_adjustment1;
		 l_return_status := FND_API.G_RET_STS_UNEXP_ERROR;
		 write_conc_log('Exception in Volume Offer Adjustment ' || SQLERRM);
		         
		 FND_MSG_PUB.Count_And_Get(p_encoded => FND_API.G_FALSE,
		                           p_count   => l_msg_count,
		                           p_data    => l_msg_data);
		          
		 FOR i in 0..l_msg_count loop
		   write_conc_log(substr(fnd_msg_pub.get(p_msg_index => i, p_encoded => 'F'), 1, 254));
		 END LOOP;  
		          
		 RETCODE := 2;
		 ERRBUF  := 'Volume Offer Adjustment Completed with Error';
		 	 	         
		 xxcus_error_pkg.xxcus_error_main_api(p_called_from 	  => l_err_callfrom
		 	 	 	             ,p_calling           => l_err_callpoint
		 	 	 	             ,p_request_id        => l_req_id
		 	 	 	             ,p_ora_error_msg     => SQLERRM
		 	 	 	             ,p_error_desc        => 'Error running XXCUS_OZF_VOLOFFRADJ_PKG.Volume Offer Adjustment package with PROGRAM ERROR'
		 	 	 	             ,p_distribution_list => l_distro_list
	 	                             	     ,p_module            => 'TM');
 END volume_offer_adjustment ;
  

END XXCUS_OZF_VOLOFFRADJ_PKG;
/
