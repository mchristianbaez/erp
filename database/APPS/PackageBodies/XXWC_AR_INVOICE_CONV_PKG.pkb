CREATE OR REPLACE PACKAGE BODY APPS.XXWC_AR_INVOICE_CONV_PKG
IS
   /************************************************************************************************************************
      *    script name: xxwc_ar_invoice_conv_pkg.pkb
      *
      *    script owners: lucidity consulting group.
      *
      *    client: white cap.
      *
      *    interface / conversion name: Open AR Invoice Conversion.
      *
      *    functional purpose: convert AR Invoices using open interface tables.
      *
      *    history:
      *
      *    Version    Date          Author            Description
      **********************************************************************************************************************
      *    1.0        04-sep-2011   T.Rajaiah         Initial development.
      *    1.1        23-Mar-2018   Ashwin Sridhar    Added for TMS#20180319-00240-AH HARRIS AR Transactions Conversion
      **********************************************************************************************************************/

   g_memo_line_text   VARCHAR2 (2000)
      := 'WC : Line Memo For Legacy System Data Conversion';

   PROCEDURE xxwc_ar_invoice_hdr_valid (p_amount                NUMBER,
                                        p_cust_number           NUMBER,
                                        p_bill_to               NUMBER,
                                        p_sold_to               NUMBER,
                                        p_salesrep_number       VARCHAR2,
                                        p_terms_name            VARCHAR2,
                                        p_attribute2            VARCHAR2,
                                        p_cust_trx_type_name    VARCHAR2,
                                        p_trx_number            VARCHAR2,
                                        p_hds_site_flag         VARCHAR2,
                                        p_ship_to               VARCHAR2)
   AS
      l_branch_code           VARCHAR2 (40);
      l_terms_name            VARCHAR2 (240);
      v_trx_name              VARCHAR2 (30);
      l_trx_number            VARCHAR2 (30);
      lvc_conc_segs           GL_CODE_COMBINATIONS_KFV.CONCATENATED_SEGMENTS%TYPE;
      lvc_err_msg             VARCHAR2 (4000);
      ln_paying_customer_id   HZ_CUST_ACCOUNTS.CUST_ACCOUNT_ID%TYPE;

      -- Satish U: 23-JAN-2012  To capture Bill TO Site Use ID of Ship To Site
      l_Bill_To_Site_Use_ID   NUMBER;
   BEGIN
      fnd_file.put_line (fnd_file.LOG, 'Inside Validation Procedure');
      v_trx_name := NULL;
      FND_FILE.PUT_LINE (FND_FILE.LOG, 'STEP1');

      -- Get Set OF books ID associated with Operating Unit

      BEGIN
         SELECT set_of_books_id
           INTO g_set_of_books_id
           FROM hr_operating_units
          WHERE organization_id = 162;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_verify_flag := 'N';
            l_error_message := l_error_message || '--Set of books not defined';
            fnd_file.put_line (fnd_file.LOG, 'Set oF Books Not Defined');
      END;

      FND_FILE.PUT_LINE (FND_FILE.LOG,
                         'STEP2 - g_set_of_books_id:' || g_set_of_books_id);

      BEGIN
         IF p_cust_trx_type_name = 'I' OR p_cust_trx_type_name = 'Invoice'
         THEN
            v_trx_name := 'Invoice';
         ELSIF p_cust_trx_type_name = 'C' OR p_cust_trx_type_name = 'Credit'
         THEN
            v_trx_name := 'Credit Memo';
         ELSIF p_cust_trx_type_name = 'B' OR p_cust_trx_type_name = 'Debit'
         THEN
            v_trx_name := 'Debit Memo';
         ELSE
            v_trx_name := NULL;
         END IF;


         IF v_trx_name = 'Invoice' AND NVL (p_amount, 0) < 0
         THEN
            v_trx_name := 'Credit Memo';
         ELSIF v_trx_name = 'Credit Memo' AND NVL (p_amount, 0) > 0
         THEN
            v_trx_name := 'Debit Memo';
         ELSIF v_trx_name = 'Debit Memo' AND NVL (p_amount, 0) < 0
         THEN
            v_trx_name := 'Credit Memo';
         END IF;

         -- Derive Cust_Trx_Type_ID value based on Trx_Name for a given Operating Unit

         SELECT cust_trx_type_id
           INTO g_cus_trx_type_id
           FROM ra_cust_trx_types_all
          WHERE name = v_trx_name AND org_id = 162;                     --162;
      EXCEPTION
         WHEN OTHERS
         THEN
            fnd_file.put_line (fnd_file.LOG,
                               '--Invalid Transaction Type' || v_trx_name);
            l_verify_flag := 'N';
            l_error_message := l_error_message || '--Invalid Transaction Type';
      END;

      BEGIN
         l_Bill_To_Site_Use_ID := NULL;                    -- Initialize value

         SELECT hcas.cust_acct_site_id, hcas.cust_account_id
           --hcsu.SITE_USE_ID
           INTO g_bill_to_address_id, g_customer_id
           --, l_Bill_To_Site_Use_ID
           FROM hz_cust_acct_sites_all hcas, hz_cust_site_uses_all hcsu
          WHERE     hcas.BILL_TO_FLAG IN ('Y', 'P')                   -- ?????
                AND hcas.attribute17 = TO_CHAR (TRIM (p_ship_to))
                AND hcas.cust_acct_site_id = hcsu.cust_acct_site_id
                AND hcsu.site_use_code = 'BILL_TO'
                AND hcas.org_id = 162
                AND ROWNUM = 1;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            BEGIN
               SELECT hcas.cust_acct_site_id, hcas.cust_account_id
                 --   hcsu.SITE_USE_ID
                 INTO g_bill_to_address_id, g_customer_id
                 --- , l_Bill_To_Site_Use_ID
                 FROM hz_cust_acct_sites_all hcas, hz_cust_site_uses_all hcsu
                WHERE     hcas.BILL_TO_FLAG IN ('Y', 'P')             -- ?????
                      AND hcas.attribute17 = TO_CHAR (TRIM (p_bill_to))
                      AND hcas.cust_acct_site_id = hcsu.cust_acct_site_id
                      AND hcsu.site_use_code = 'BILL_TO'
                      AND hcas.org_id = 162
                      AND ROWNUM = 1;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  BEGIN
                     fnd_file.put_line (fnd_file.LOG, 'No Data Found');

                     SELECT hcas.cust_acct_site_id, hcas.cust_account_id
                       INTO g_bill_to_address_id, g_customer_id
                       FROM apps.hz_cust_acct_sites_all hcas,
                            apps.hz_cust_site_uses_all hcsu,
                            XXWC.XXWC_AHH_AR_CUST_CROSS_OVER_T A,
                            APPS.HZ_CUST_ACCOUNTS B
                      WHERE     hcas.BILL_TO_FLAG IN ('Y', 'P')
                            AND hcas.cust_acct_site_id =
                                   hcsu.cust_acct_site_id
                            AND hcsu.site_use_code = 'BILL_TO'
                            AND hcas.org_id = 162
                            AND A.CUST_NUM = p_cust_number
                            AND A.ORACLE_CUST_NUM = B.ACCOUNT_NUMBER
                            AND b.cust_account_id = hcas.cust_account_id
                            AND ROWNUM = 1;

                     fnd_file.put_line (
                        fnd_file.LOG,
                           'No Data Found g_bill_to_address_id:'
                        || g_bill_to_address_id
                        || ' g_customer_id:'
                        || g_customer_id);
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        fnd_file.put_line (fnd_file.LOG,
                                           'Error - ' || SQLERRM);

                        IF v_trx_name = 'Invoice'
                        THEN
                           IF     TO_CHAR (P_Bill_To) = P_Ship_To
                              AND INSTR (P_SHIP_TO, '000', 1) > 1
                           THEN
                              NULL;
                           ELSE
                              l_verify_flag := 'N';
                           END IF;
                        END IF;

                        l_error_message :=
                              l_error_message
                           || '--Customer Primary Bill to not Defined -'
                           || p_bill_to;
                        fnd_file.put_line (
                           fnd_file.LOG,
                           '--Customer Bill to not Defined :' || p_bill_to);
                  END;
               WHEN OTHERS
               THEN
                  fnd_file.put_line (fnd_file.LOG, 'Error - ' || SQLERRM);

                  IF v_trx_name = 'Invoice'
                  THEN
                     IF     TO_CHAR (P_Bill_To) = P_Ship_To
                        AND INSTR (P_SHIP_TO, '000', 1) > 1
                     THEN
                        NULL;
                     ELSE
                        l_verify_flag := 'N';
                     END IF;
                  END IF;

                  l_error_message :=
                        l_error_message
                     || '--Customer Primary Bill to not Defined -'
                     || p_bill_to;
                  fnd_file.put_line (
                     fnd_file.LOG,
                     '--Customer Bill to not Defined :' || p_bill_to);
            END;
         WHEN OTHERS
         THEN
            fnd_file.put_line (fnd_file.LOG, 'Error - ' || SQLERRM);

            IF v_trx_name = 'Invoice'
            THEN
               IF     TO_CHAR (P_Bill_To) = P_Ship_To
                  AND INSTR (P_SHIP_TO, '000', 1) > 1
               THEN
                  NULL;
               ELSE
                  l_verify_flag := 'N';
               END IF;
            END IF;

            l_error_message :=
                  l_error_message
               || '--Customer Bill to not Defined for ShipTo - '
               || p_ship_to;
            fnd_file.put_line (
               fnd_file.LOG,
               '--Customer Bill to not Defined for ShipTo :' || p_ship_to);
      END;

      -- Get Ship TO Infomration

      g_ship_to_address_id := NULL;

      BEGIN
         SELECT hcas.cust_acct_site_id
           INTO g_ship_to_address_id
           FROM hz_cust_acct_sites_all hcas, hz_cust_site_uses_all hcsu
          WHERE     hcas.cust_acct_site_id = hcsu.cust_acct_site_id
                AND hcsu.site_use_code = 'SHIP_TO'
                AND hcsu.cust_acct_site_id = g_bill_to_address_id;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            BEGIN
               SELECT hcas.cust_acct_site_id
                 INTO g_ship_to_address_id
                 FROM hz_cust_acct_sites_all hcas, hz_cust_site_uses_all hcsu
                WHERE     hcas.cust_acct_site_id = hcsu.cust_acct_site_id
                      AND hcsu.site_use_code = 'SHIP_TO'
                      AND hcas.cust_account_id = g_customer_id
                      AND ROWNUM = 1;
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_error_message :=
                        l_error_message
                     || '--Customer Ship-To not Defined for CustomerId-'
                     || g_customer_id;
                  fnd_file.put_line (
                     fnd_file.LOG,
                        '--Customer Ship-To not Defined for CustomerId-'
                     || g_customer_id);
            END;
         WHEN OTHERS
         THEN
            l_error_message :=
                  l_error_message
               || '--Customer Ship-To not Defined for Bill-To'
               || g_bill_to_address_id;
            fnd_file.put_line (
               fnd_file.LOG,
                  '--Customer Ship-To not Defined for Bill-To-'
               || g_bill_to_address_id);
      END;

      --Derive Salesrep ID from Salesrep Number
      -- Added below code in Rev 1.1
      BEGIN
         SELECT rs.SALESREP_ID
           INTO g_salesrep_id
           FROM XXWC.XXWC_AHH_AM_T xaat, apps.ra_salesreps_all rs
          WHERE     UPPER (TRIM (xaat.SLSREPOUT)) =
                       UPPER (TRIM (p_salesrep_number))
                AND xaat.SALESREP_ID = rs.SALESREP_NUMBER
                AND rs.org_id = 162;
      EXCEPTION
         WHEN OTHERS
         THEN
            -- Oracle Salesrep is not found then assign default Salesrep 'No Sales Credit'
            SELECT salesrep_id
              INTO g_salesrep_id
              FROM ra_salesreps_all
             WHERE name = 'No Sales Credit' AND org_id = 162;
      END;



      IF v_trx_name LIKE 'Credit%'
      THEN
         g_term_id := NULL;
      ELSE
         BEGIN
            SELECT TERM_NAME
              INTO l_terms_name
              FROM XXWC.XXWC_AHH_PAYMENT_TERMS_T
             WHERE UPPER (AHH_TERM_NAME) = TRIM (UPPER (p_terms_name));
         EXCEPTION
            WHEN OTHERS
            THEN
               l_terms_name := 'IMMEDIATE';
         END;

         BEGIN
            SELECT term_id
              INTO g_term_id
              FROM ra_terms
             WHERE     TRIM (UPPER (name)) = TRIM (UPPER (l_terms_name))
                   AND ROWNUM = 1;
         EXCEPTION
            WHEN OTHERS
            THEN
               g_term_id := 5;
         END;
      END IF;

      l_branch_code := p_attribute2;

      IF l_branch_code IS NOT NULL
      THEN
         BEGIN
            IF g_revenue_ccid IS NULL
            THEN
               lvc_conc_segs := NULL;
               lvc_err_msg := NULL;

               g_revenue_ccid :=
                  derive_ccid ('0W',
                               'BW087',                   -- changed by Vamshi
                               '0000',
                               '251100',                  -- changed by Vamshi
                               '00000',
                               '00000',
                               '00000',
                               lvc_err_msg);

               IF lvc_err_msg IS NOT NULL
               THEN
                  fnd_file.put_line (
                     fnd_file.LOG,
                        'Revenue Acct:'
                     || lvc_conc_segs
                     || ' Error Occured:'
                     || lvc_err_msg);
               END IF;
            END IF;

            lvc_conc_segs := NULL;
            lvc_err_msg := NULL;

            g_rec_ccid :=
               derive_ccid ('0W',
                            l_branch_code,
                            '0000',
                            '111100',
                            '00000',
                            '00000',
                            '00000',
                            lvc_err_msg);

            IF lvc_err_msg IS NOT NULL
            THEN
               fnd_file.put_line (
                  fnd_file.LOG,
                     'Receivable Acct:'
                  || lvc_conc_segs
                  || ' Error Occured:'
                  || lvc_err_msg);
            END IF;
         EXCEPTION
            WHEN TOO_MANY_ROWS
            THEN
               l_verify_flag := 'N';
               l_error_message :=
                     l_error_message
                  || '--Too many rows returned during account derivation';
            WHEN OTHERS
            THEN
               l_verify_flag := 'N';
               l_error_message :=
                  l_error_message || '--Rec and Rev account not Defined';
         END;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         v_errorcode := SQLCODE;
         v_errormessage := SUBSTR (SQLERRM, 1, 200);
         fnd_file.put_line (fnd_file.LOG,
                            'Exception Block of Validation Procedure');
         fnd_file.put_line (fnd_file.LOG,
                            'The Error Code Traced is : ' || v_errorcode);
         fnd_file.put_line (
            fnd_file.LOG,
            'The Error Message Traced is : ' || v_errormessage);
   END xxwc_ar_invoice_hdr_valid;


   --*************************************************************************************************
   --Procedure  xxwc_stage_to_interface :  Using this API, successfully validated AR Open Invoices records from Staging tables are inserted into AR
   -- Open Invoice Interface Tables. .

   PROCEDURE xxwc_stage_to_interface (p_run_date IN VARCHAR2) --Added the parameter by Ashwin.S for TMS#20180319-00240
   AS
      v_errorcode             NUMBER;
      v_errormessage          VARCHAR2 (240);
      v_recordcnt             NUMBER (6) := 0;
      l_branch_code           VARCHAR2 (20);
      ln_paying_customer_id   NUMBER;
      lvc_ship_to             VARCHAR2 (100);



      CURSOR cur_hdr
      IS
           SELECT c.ROWID r_id,
                  C.PAYING_CUSTOMER_ID,
                  C.INTERFACE_LINE_ATTRIBUTE1,
                  UPPER (C.ATTRIBUTE2) ATTRIBUTE2,
                  C.ATTRIBUTE3,
                  C.ACCTD_AMOUNT,
                  C.ORIG_SYSTEM_SOLD_CUSTOMER_REF,
                  C.AMOUNT,
                  UPPER (C.TERM_NAME) TERM_NAME,
                  C.ORIG_SYSTEM_BILL_CUSTOMER_REF,
                  C.TRX_DATE,
                  C.TRX_NUMBER,
                  C.UNIT_SELLING_PRICE,
                  C.PRIMARY_SALESREP_NUMBER,
                  C.SALES_ORDER,
                  C.PURCHASE_ORDER,
                  C.PROCESS_STATUS,
                  C.ERROR_MESSAGE,
                  C.CUST_TRX_TYPE_NAME,
                  C.SHIP_TO_PARTY_SITE_NUMBER SITE_ADDRESS,
                  C.HDS_SITE_FLAG,
                  UPPER (
                     DECODE (
                        C.SHIP_TO_PARTY_SITE_NUMBER,
                        NULL, C.ORIG_SYSTEM_SOLD_CUSTOMER_REF,
                        (   C.ORIG_SYSTEM_SOLD_CUSTOMER_REF
                         || '-'
                         || C.SHIP_TO_PARTY_SITE_NUMBER)))            -- ?????
                     SHIP_TO_PARTY_SITE_NUMBER,
                  C.ORG_ID,
                  C.COMMENTS
             FROM xxwc.xxwc_ar_invoices_conv c 
            WHERE     1 = 1
--                  and PROCESS_STATUS='REJECTED'
                  AND NVL (process_status, 'XXXXX') NOT IN ('PROCESSED',
                                                            'REJECTED')
         ORDER BY process_status DESC, trx_number;
   BEGIN
      fnd_file.put_line (fnd_file.LOG, 'Inside Stage to interface Procedure');


      FOR rec_hdr IN cur_hdr
      LOOP
         fnd_file.put_line (fnd_file.LOG,
                            'Trx Number Processing ' || rec_hdr.trx_number);
         l_verify_flag := 'Y';
         l_error_message := NULL;
         g_set_of_books_id := NULL;
         g_cus_trx_type_id := NULL;
         g_account_id := NULL;
         g_site_id := NULL;
         g_salesrep_id := NULL;
         g_revenue_ccid := NULL;
         g_rec_ccid := NULL;
         g_sold_to := NULL;
         g_customer_id := NULL;
         g_bill_to_address_id := NULL;
         g_ship_to := NULL;
         g_ship_to_address_id := NULL;
         ln_paying_customer_id := NULL;

         IF v_recordcnt = 1000
         THEN
            COMMIT;
            v_recordcnt := 0;
         END IF;

         l_branch_code := NULL;

         BEGIN
            SELECT ORACLE_BW_NUMBER
              INTO l_branch_code
              FROM XXWC.XXWC_AP_BRANCH_LIST_STG_TBL
             WHERE ahh_warehouse_number = UPPER (rec_hdr.attribute2);
         EXCEPTION
            WHEN OTHERS
            THEN
               l_branch_code := 'BW080';
         END;

         lvc_ship_to := NULL;

         IF rec_hdr.SITE_ADDRESS IS NULL
         THEN
            BEGIN
               SELECT hcas.attribute17
                 INTO lvc_ship_to
                 FROM APPS.HZ_CUST_SITE_USES_ALL HCSU,
                      APPS.HZ_CUST_ACCT_SITES_ALL HCAS,
                      APPS.HZ_CUST_ACCOUNTS hca
                WHERE     HCSU.ATTRIBUTE1 = 'YARD'
                      AND hcsu.cust_acct_site_id = hcas.cust_acct_site_id
                      AND hcas.attribute17 =
                             rec_hdr.ORIG_SYSTEM_SOLD_CUSTOMER_REF
                      AND hcas.cust_account_id = hca.cust_account_id
                      AND hca.attribute4 = 'AHH'
                      AND ROWNUM = 1;
            EXCEPTION
               WHEN OTHERS
               THEN
                  lvc_ship_to := NULL;
            END;
         ELSE
            lvc_ship_to := rec_hdr.ship_to_party_site_number;
         END IF;

         -- Deriving Paying_customer_id
         /*
         BEGIN
            -- Checking customer exists in cross Over list
            SELECT b.cust_account_id
              INTO ln_paying_customer_id
              FROM XXWC.XXWC_AHH_AR_CUST_CROSS_OVER_T A,
                   APPS.HZ_CUST_ACCOUNTS B
             WHERE     A.CUST_NUM = i.ORIG_SYSTEM_SOLD_CUSTOMER_REF
                   AND A.ORACLE_CUST_NUM = B.ACCOUNT_NUMBER
                   AND ROWNUM = 1;
         EXCEPTION
            WHEN OTHERS
            THEN
               ln_paying_customer_id := NULL;
         END;

            -- Checking customer exists oracle base tables if not exists in cross-over.
         IF NVL (ln_paying_customer_id, 0) = 0
         THEN
            BEGIN
               SELECT a.cust_account_id
                 INTO ln_paying_customer_id
                 FROM hz_cust_accounts a, hz_cust_acct_sites_all b
                WHERE     a.cust_account_id = b.cust_account_id
                      AND a.ATTRIBUTE4 = 'AHH'
                      AND NVL (b.attribute17, 'ABCXYZ') =
                             i.ORIG_SYSTEM_SOLD_CUSTOMER_REF
                      AND b.bill_to_flag = 'P';
            EXCEPTION
               WHEN OTHERS
               THEN
                  ln_paying_customer_id := NULL;
            END;
         END IF;
          */
         IF g_validate_only = 'Y'
         THEN
            fnd_file.put_line (fnd_file.LOG,
                               'Submitting API  xxwc_ar_invoice_hdr_valid');
            -- Valdiate Invocie REcord
            FND_FILE.PUT_LINE (FND_FILE.LOG,
                               'rec_hdr.amount:' || rec_hdr.amount);
            FND_FILE.PUT_LINE (
               FND_FILE.LOG,
               'rec_hdr.paying_customer_id:' || ln_paying_customer_id);
            FND_FILE.PUT_LINE (
               FND_FILE.LOG,
                  'orig_system_bill_customer_ref:'
               || rec_hdr.orig_system_bill_customer_ref);
            FND_FILE.PUT_LINE (
               FND_FILE.LOG,
                  'orig_system_sold_customer_ref:'
               || rec_hdr.orig_system_sold_customer_ref);
            FND_FILE.PUT_LINE (
               FND_FILE.LOG,
                  'rec_hdr.primary_salesrep_number:'
               || rec_hdr.primary_salesrep_number);
            FND_FILE.PUT_LINE (FND_FILE.LOG,
                               'rec_hdr.term_name:' || rec_hdr.term_name);
            FND_FILE.PUT_LINE (FND_FILE.LOG,
                               'rec_hdr.attribute2:' || rec_hdr.attribute2);
            FND_FILE.PUT_LINE (
               FND_FILE.LOG,
               'rec_hdr.cust_trx_type_name:' || rec_hdr.cust_trx_type_name);
            FND_FILE.PUT_LINE (FND_FILE.LOG,
                               'rec_hdr.trx_number:' || rec_hdr.trx_number);
            FND_FILE.PUT_LINE (
               FND_FILE.LOG,
               'rec_hdr.hds_site_flag:' || rec_hdr.hds_site_flag);
            FND_FILE.PUT_LINE (
               FND_FILE.LOG,
                  'rec_hdr.ship_to_party_site_number:'
               || rec_hdr.ship_to_party_site_number);
            xxwc_ar_invoice_hdr_valid (rec_hdr.amount,
                                       rec_hdr.paying_customer_id, -- commented by Vamshi
                                       --ln_paying_customer_id, -- Added by Vamshi.
                                       rec_hdr.orig_system_bill_customer_ref,
                                       rec_hdr.orig_system_sold_customer_ref,
                                       rec_hdr.primary_salesrep_number,
                                       rec_hdr.term_name,
                                       l_branch_code,
                                       --rec_hdr.attribute2,   commented by Vamshi
                                       rec_hdr.cust_trx_type_name,
                                       rec_hdr.trx_number,
                                       rec_hdr.hds_site_flag,
                                       --rec_hdr.ship_to_party_site_number  -- commented by Vamshi
                                       lvc_ship_to);

            IF l_verify_flag != 'N'
            THEN
               -- SInce Validation is successful : Update AR Open Invoice Transaction as VALIDATED
               UPDATE xxwc.xxwc_ar_invoices_conv --Prefixed xxwc by Ashwin.S on 21-Mar-2018 for TMS#20180319-00240
                  SET process_status = 'VALIDATED',
                      error_message = l_error_message
                WHERE ROWID = rec_hdr.r_id;
            ELSE
               -- update status and error message
               UPDATE xxwc.xxwc_ar_invoices_conv --Prefixed xxwc by Ashwin.S on 21-Mar-2018 for TMS#20180319-00240
                  SET process_status = 'REJECTED',
                      error_message = l_error_message
                WHERE ROWID = rec_hdr.r_id;
            END IF;
         END IF;

         IF g_validate_only != 'Y'
         THEN
            fnd_file.put_line (fnd_file.LOG,
                               ' Processing API xxwc_ar_invoice_hdr_valid ');
            FND_FILE.PUT_LINE (FND_FILE.LOG,
                               'rec_hdr.amount:' || rec_hdr.amount);
            FND_FILE.PUT_LINE (
               FND_FILE.LOG,
               'rec_hdr.paying_customer_id:' || ln_paying_customer_id);
            FND_FILE.PUT_LINE (
               FND_FILE.LOG,
                  'orig_system_bill_customer_ref:'
               || rec_hdr.orig_system_bill_customer_ref);
            FND_FILE.PUT_LINE (
               FND_FILE.LOG,
                  'orig_system_sold_customer_ref:'
               || rec_hdr.orig_system_sold_customer_ref);
            FND_FILE.PUT_LINE (
               FND_FILE.LOG,
                  'rec_hdr.primary_salesrep_number:'
               || rec_hdr.primary_salesrep_number);
            FND_FILE.PUT_LINE (FND_FILE.LOG,
                               'rec_hdr.term_name:' || rec_hdr.term_name);
            FND_FILE.PUT_LINE (FND_FILE.LOG,
                               'rec_hdr.attribute2:' || rec_hdr.attribute2);
            FND_FILE.PUT_LINE (
               FND_FILE.LOG,
               'rec_hdr.cust_trx_type_name:' || rec_hdr.cust_trx_type_name);
            FND_FILE.PUT_LINE (FND_FILE.LOG,
                               'rec_hdr.trx_number:' || rec_hdr.trx_number);
            FND_FILE.PUT_LINE (
               FND_FILE.LOG,
               'rec_hdr.hds_site_flag:' || rec_hdr.hds_site_flag);
            FND_FILE.PUT_LINE (
               FND_FILE.LOG,
                  'rec_hdr.ship_to_party_site_number:'
               || rec_hdr.ship_to_party_site_number);
            FND_FILE.PUT_LINE (FND_FILE.LOG, 'lvc_ship_to:' || lvc_ship_to);

            xxwc_ar_invoice_hdr_valid (rec_hdr.amount,
                                       rec_hdr.paying_customer_id, -- commented by Vamshi
                                       --ln_paying_customer_id, -- Added by Vamshi.
                                       rec_hdr.orig_system_bill_customer_ref,
                                       rec_hdr.orig_system_sold_customer_ref,
                                       rec_hdr.primary_salesrep_number,
                                       rec_hdr.term_name,
                                       l_branch_code,       -- Added by Vamshi
                                       -- rec_hdr.attribute2,  -- Commented by Vamshi.
                                       rec_hdr.cust_trx_type_name,
                                       rec_hdr.trx_number,
                                       rec_hdr.hds_site_flag,
                                       --rec_hdr.ship_to_party_site_number  -- commented by Vamshi
                                       lvc_ship_to);



            IF l_verify_flag != 'N'
            THEN
               INSERT
                 INTO ra_interface_lines_all (interface_line_context,
                                              interface_line_attribute1,
                                              interface_line_attribute2,
                                              attribute2,
                                              attribute3,
                                              batch_source_name,
                                              set_of_books_id,
                                              line_type,
                                              description,
                                              currency_code,
                                              amount,
                                              cust_trx_type_id, --paying_customer_id,
                                              --paying_site_use_id,
                                              orig_system_sold_customer_id,
                                              orig_system_bill_customer_id,
                                              orig_system_bill_address_id,
                                              orig_system_ship_customer_id,
                                              orig_system_ship_address_id,
                                              conversion_type, -- conversion_date,
                                              conversion_rate,
                                              trx_date,
                                              trx_number,          --uom_name,
                                              org_id,
                                              primary_salesrep_id,
                                              quantity_ordered,
                                              unit_selling_price,
                                              term_id,
                                              gl_date,
                                              sales_order,
                                              amount_includes_tax_flag,
                                              taxable_flag,
                                              purchase_order,
                                              HEADER_ATTRIBUTE_CATEGORY,
                                              HEADER_ATTRIBUTE10,
                                              HEADER_ATTRIBUTE12,
                                              HEADER_ATTRIBUTE15,
                                              COMMENTS)    -- Added in Rev 1.1
               VALUES ('CONVERSION',
                       rec_hdr.sales_order,
                       rec_hdr.trx_number,
                       l_branch_code,
                       rec_hdr.attribute3,
                       'CONVERSION-AHHARRIS',
                       g_set_of_books_id,
                       'LINE',
                       g_memo_line_text,                   -- added by Vamshi.
                       --'WC : Line Memo For Legacy System Data Conversion', --'Conversion of Transactions on '||SYSDATE,   commented by Vamshi
                       'USD',                           --'USD'  currency_code
                       rec_hdr.amount,
                       g_cus_trx_type_id,                      --g_account_id,
                       --g_site_id,
                       g_customer_id,
                       --g_sold_to,
                       g_customer_id,
                       g_bill_to_address_id,
                       g_customer_id,
                       g_ship_to_address_id,
                       'User',                          --inv_rec.invoicedate,
                       1,                                   -- Conversion Rate
                       rec_hdr.trx_date,
                       rec_hdr.trx_number, --prefix||inv_rec.invoiceno Trx_number
                       -- vuom,                             -- 'Each',
                       162,
                       g_salesrep_id,
                       1,
                       rec_hdr.unit_selling_price,
                       g_term_id,
                       G_GL_DATE, --TO_DATE ('11/27/2011', 'MM/DD/YYYY'), --5000, 10143, 'S',
                       rec_hdr.sales_order,
                       'Y',
                       'N',                                 --rec_hdr.trx_date
                       rec_hdr.purchase_order,
                       162,
                       p_run_date,
                       p_run_date,
                       p_run_date,
                       rec_hdr.comments                    -- Added in Rev 1.1
                                       );

               fnd_file.put_line (
                  fnd_file.LOG,
                  'Inserting Record into Ra Interface Sales Credits  All ');

               INSERT
                 INTO ra_interface_salescredits_all (
                         interface_line_context,
                         interface_line_attribute1,
                         interface_line_attribute2,
                         salesrep_id,
                         sales_credit_amount_split,
                         sales_credit_percent_split,
                         sales_credit_type_id,
                         org_id)
               VALUES ('CONVERSION',
                       rec_hdr.sales_order,
                       rec_hdr.trx_number,
                       g_salesrep_id,
                       rec_hdr.amount,
                       100,
                       1,
                       162);

               fnd_file.put_line (
                  fnd_file.LOG,
                  'Inserting Record into Ra Interface Distributions  All ');

               INSERT
                 INTO ra_interface_distributions_all (
                         interface_line_context,
                         interface_line_attribute1,
                         interface_line_attribute2,
                         account_class,
                         amount,
                         percent,
                         code_combination_id,
                         acctd_amount,
                         org_id)
               VALUES ('CONVERSION',
                       rec_hdr.sales_order,
                       rec_hdr.trx_number,
                       'REV',
                       rec_hdr.amount,
                       100,
                       g_revenue_ccid,                                 --?????
                       rec_hdr.amount,
                       162);

               INSERT
                 INTO ra_interface_distributions_all (
                         interface_line_context,
                         interface_line_attribute1,
                         interface_line_attribute2,
                         account_class,
                         amount,
                         percent,
                         code_combination_id,
                         acctd_amount,
                         org_id)
               VALUES ('CONVERSION',
                       rec_hdr.sales_order,
                       rec_hdr.trx_number,
                       'REC',
                       rec_hdr.amount,
                       100,
                       g_rec_ccid,                                     --?????
                       rec_hdr.amount,
                       162);


               IF SQL%FOUND
               THEN
                  fnd_file.put_line (fnd_file.LOG, 'Inserted successfully');
               END IF;

               UPDATE xxwc.xxwc_ar_invoices_conv --Prefixed xxwc by Ashwin.S on 21-Mar-2018 for TMS#20180319-00240
                  SET process_status = 'PROCESSED',
                      error_message = l_error_message
                WHERE ROWID = rec_hdr.r_id;
            ELSE
               -- update status and error message
               UPDATE xxwc.xxwc_ar_invoices_conv --Prefixed xxwc by Ashwin.S on 21-Mar-2018 for TMS#20180319-00240
                  SET process_status = 'REJECTED',
                      error_message = l_error_message
                WHERE ROWID = rec_hdr.r_id;
            END IF;
         END IF;

         v_recordcnt := v_recordcnt + 1;
      END LOOP;

      COMMIT;
      fnd_file.put_line (fnd_file.LOG, 'Before calling standard program');

      IF g_submit = 'Y'
      THEN
         fnd_file.put_line (fnd_file.LOG,
                            'Inside IF: Before calling standard program');
         UPDATE APPS.RA_INTERFACE_LINES_ALL SET TERM_ID=NULL WHERE CUST_TRX_TYPE_ID=2 AND TERM_ID IS NOT NULL; 							
         xxwc_ar_auto_invoice_prog;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         v_errorcode := SQLCODE;
         v_errormessage := SUBSTR (SQLERRM, 1, 200);
         fnd_file.put_line (
            fnd_file.LOG,
            'Exception Block of Stage to Interface Import Procedure');
         fnd_file.put_line (fnd_file.LOG,
                            'The Error Code Traced is : ' || v_errorcode);
         fnd_file.put_line (
            fnd_file.LOG,
            'The Error Message Traced is : ' || v_errormessage);
   END xxwc_stage_to_interface;

   --*************************************************************************************************
   --Procedure  xxwc_ar_auto_invoice_prog :  Auto Invoice Master Program is kicked off using this API.


   PROCEDURE xxwc_ar_auto_invoice_prog
   AS
      v_conc_request_id   NUMBER;
      l_request_id1       NUMBER;
      v_phase             VARCHAR2 (100);
      v_status            VARCHAR2 (100);
      v_dev_phase         VARCHAR2 (100);
      v_dev_status        VARCHAR2 (100);
      v_message           VARCHAR2 (2000);
      v_wait_outcome      BOOLEAN;
      v_errorcode         NUMBER;
      v_errormessage      VARCHAR2 (240);
   BEGIN
      l_request_id1 :=
         fnd_request.submit_request (
            application   => 'AR',
            program       => 'RAXMTR',
            description   => 'Autoinvoice Master Program',
            start_time    => SYSDATE,
            sub_request   => FALSE,
            argument1     => 8,
            argument2     => 162,
            argument3     => g_batch_source_id,
            argument4     => g_batch_source_name,
            argument5     => G_GL_DATE, -- TO_DATE ('11/27/2011', 'MM/DD/YYYY'),
            argument6     => NULL,
            argument7     => NULL,
            argument8     => NULL,
            argument9     => NULL,
            argument10    => NULL,
            argument11    => NULL,
            argument12    => NULL,
            argument13    => NULL,
            argument14    => NULL,
            argument15    => NULL,
            argument16    => NULL,
            argument17    => NULL,
            argument18    => NULL,
            argument19    => NULL,
            argument20    => NULL,
            argument21    => NULL,
            argument22    => NULL,
            argument23    => NULL,
            argument24    => NULL,
            argument25    => NULL,
            argument26    => 'Y',
            argument27    => NULL);
      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         v_errorcode := SQLCODE;
         v_errormessage := SUBSTR (SQLERRM, 1, 200);
         g_errorcode := v_errorcode;
         g_errormessage := v_errormessage;
         fnd_file.put_line (
            fnd_file.LOG,
            'Exception Block of Standard Program Call Procedure');
         fnd_file.put_line (fnd_file.LOG,
                            'The Error Code Traced is : ' || v_errorcode);
         fnd_file.put_line (
            fnd_file.LOG,
            'The Error Message Traced is : ' || v_errormessage);
   END xxwc_ar_auto_invoice_prog;


   --*************************************************************************************************
   -- Procedure Main :  This procedure gets called from AR Open Invoice Conversion program.  Program can be called to just validate the records
   -- or validation and importing Open Invoices to AR schema can be done in one process too.
   -- How to call the API just to validate and not import.
   -- P_Validate_Only => 'Y'
   -- p_Submit        => 'N'
   -- to Import records from staging into AR Schema pass the program parameters as follows.
   -- P_Validate_Only => 'Y'
   -- p_Submit        => 'N'
   -- When an Invoice Transaction is processed successfully, Status column is updated as PROCESSED else it is updated as REJECTED
   -- Only 20000 AR Open Invoice Transactions are processed every batch.
   -- IF a rejected record needs to be processed again then one should update Process_Status with NULL value.

   PROCEDURE main (errbuf               OUT VARCHAR2,
                   retcode              OUT NUMBER,
                   p_validate_only   IN     VARCHAR2,
                   p_submit          IN     VARCHAR2,
                   p_run_date        IN     VARCHAR2 --Added by Ashwin.S for TMS#20180319-00240
                                                    )
   AS
      l_flag        VARCHAR2 (10) DEFAULT 'Y';


      batch_error   EXCEPTION;
      GL_DATE_EXP   EXCEPTION;

      -- Define a Cursor that picks atmost 20000 records whose process_Status is NULL .
      --
      CURSOR stg_hdr
      IS
           SELECT c.ROWID r_id, c.*
             FROM xxwc.xxwc_ar_invoices_conv c --Prefixed xxwc by Ashwin.S on 21-Mar-2018 for TMS#20180319-00240
            WHERE NVL (process_status, 'XXXXX') NOT IN ('PROCESSED', 'REJECTED')
         --AND trx_number IN ('4126027-00')
         ORDER BY process_status DESC, trx_number;
   BEGIN
      fnd_file.put_line (fnd_file.LOG, 'Inside Main Procedure');

      --16-FEB-2012 : Check If GL Date exists if not then exit the program
      IF G_GL_DATE IS NULL
      THEN
         fnd_file.put_line (fnd_file.LOG, ' Please enter value for GL Date');
         Retcode := 1;
         RAISE GL_DATE_EXP;
      END IF;


      BEGIN
         -- Deriving Memo Line Text
         -- Added by Vamshi
         g_revenue_ccid := NULL;

         SELECT a.gl_id_rev
           INTO g_revenue_ccid
           FROM APPS.AR_MEMO_LINES_VL a
          WHERE 1 = 1 AND A.NAME = g_memo_line_text;
      EXCEPTION
         WHEN OTHERS
         THEN
            g_revenue_ccid := NULL;
            FND_FILE.PUT_LINE (
               FND_FILE.LOG,
               'Error Occured while deriving revenue account');
      END;


      --      BEGIN
      --         UPDATE xxwc.xxwc_ar_invoices_conv
      --            SET PROCESS_STATUS = 'REJECTED',
      --                ERROR_MESSAGE = 'Missing ship_to_party_site_number'
      --          WHERE SHIP_TO_PARTY_SITE_NUMBER IS NULL AND PROCESS_STATUS = 'NEW';
      --      END;


      BEGIN
         SELECT batch_source_id, name
           INTO g_batch_source_id, g_batch_source_name
           FROM ra_batch_sources_all
          WHERE name = 'CONVERSION-AHHARRIS';
      EXCEPTION
         WHEN OTHERS
         THEN
            RAISE batch_error;
      END;

      /*Initializing Global Variables as per Parameters selected*/
      IF p_validate_only = 'Y'
      THEN
         xxwc_ar_invoice_conv_pkg.g_validate_only := 'Y';
      ELSE
         xxwc_ar_invoice_conv_pkg.g_validate_only := 'N';
      END IF;

      IF p_submit = 'Yes'
      THEN
         xxwc_ar_invoice_conv_pkg.g_submit := 'Y';
      ELSE
         xxwc_ar_invoice_conv_pkg.g_submit := 'N';
      END IF;

      /*Calling Import Procedure Package*/
      fnd_file.put_line (fnd_file.LOG, 'Before stage to interface');
      xxwc_ar_invoice_conv_pkg.xxwc_stage_to_interface (p_run_date); --Added the parameter by Ashwin.S for TMS#20180319-00240
      fnd_file.put_line (fnd_file.LOG, 'After stage to interface');

      /*Calling Auto Invoice Import from Backend*/
      IF p_submit = 'Yes'
      THEN
         fnd_file.put_line (
            fnd_file.LOG,
               'Request ID for AR Invoice Import Program Is :'
            || xxwc_ap_invoice_conv_pkg.g_request_id);
         fnd_file.put_line (
            fnd_file.LOG,
               'Program Status :'
            || xxwc_ap_invoice_conv_pkg.g_dev_phase
            || ' / '
            || xxwc_ap_invoice_conv_pkg.g_dev_status);
      END IF;

      /*Error Report printing Statememnts*/
      fnd_file.put_line (fnd_file.output, '<HTML><BODY>');
      fnd_file.put_line (fnd_file.output, '<PRE>');
      fnd_file.new_line (fnd_file.output, 1);
      fnd_file.put_line (
         fnd_file.output,
         '********************************************************************************************<BR>');
      fnd_file.put_line (
         fnd_file.output,
         '<H3>                Custom AR Open Invoice Execution Report                      </H3>');
      fnd_file.put_line (
         fnd_file.output,
         '********************************************************************************************<BR>');
      fnd_file.put_line (
         fnd_file.output,
            '<H4> 1. Program Name           : '
         || ' XXWC AR Invoice Import Program ');
      fnd_file.put_line (
         fnd_file.output,
            ' 2. Start Date             :  '
         || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));
      fnd_file.put_line (fnd_file.output, '</H4>');
      fnd_file.put_line (
         fnd_file.output,
         '********************************************************************************************<BR>');
      fnd_file.put_line (fnd_file.output, '<BR>');
      fnd_file.put_line (
         fnd_file.output,
         '<b>The following records may cause this failure</b> ');
      fnd_file.put_line (fnd_file.output, '<TABLE BORDER=1>');
      fnd_file.put_line (
         fnd_file.output,
         '<TR><TH>Sales order Number # </TH><TH>Trx Number # </TH><TH>Amount </TH><TH>Currency </TH><TH>Rejection Reason # </TR>');

      FOR rec_stg IN stg_hdr
      LOOP
         fnd_file.put_line (
            fnd_file.output,
               '<TR><TD>'
            || rec_stg.sales_order
            || '</TD><TD>'
            || rec_stg.trx_number
            || '</TD><TD>'
            || rec_stg.amount
            || '</TD><TD>'
            || 'USD'
            || '</TD><TD>'
            || rec_stg.error_message
            || '</TD></TR>');
      END LOOP;

      fnd_file.put_line (fnd_file.output, '</TABLE>');
      fnd_file.put_line (fnd_file.output, '</BODY></HTML>');
   EXCEPTION
      WHEN batch_error
      THEN
         v_errormessage := 'CONVERSION Batch not defined in Oracle';
         fnd_file.put_line (fnd_file.LOG, 'Error: ' || v_errormessage);
         Retcode := 1;
      WHEN GL_DATE_EXP
      THEN
         v_errormessage := 'GL DATE not captured in Profile';
         fnd_file.put_line (fnd_file.LOG, 'Error: ' || v_errormessage);
         Retcode := 1;
      WHEN OTHERS
      THEN
         v_errorcode := SQLCODE;
         v_errormessage := SUBSTR (SQLERRM, 1, 200);
         fnd_file.put_line (fnd_file.LOG,
                            'Exception Block of Main Procedure');
         fnd_file.put_line (fnd_file.LOG,
                            'The Error Code Traced is : ' || v_errorcode);
         fnd_file.put_line (
            fnd_file.LOG,
            'The Error Message Traced is : ' || v_errormessage);
   END main;


   FUNCTION derive_ccid (p_segment1   IN     VARCHAR2,
                         p_segment2   IN     VARCHAR2,
                         p_segment3   IN     VARCHAR2,
                         p_segment4   IN     VARCHAR2,
                         p_segment5   IN     VARCHAR2,
                         p_segment6   IN     VARCHAR2,
                         p_segment7   IN     VARCHAR2,
                         x_ret_msg       OUT VARCHAR2)
      RETURN NUMBER
   IS
      ln_ccid               GL_CODE_COMBINATIONS.CODE_COMBINATION_ID%TYPE;
      ln_structure_num      VARCHAR2 (10);
      l_valid_combination   BOOLEAN;
      l_cr_combination      BOOLEAN;
      lvc_sec               VARCHAR2 (4000);
      lvc_ccid_status       BOOLEAN;
      p_concat_segs         VARCHAR2 (1000);
   BEGIN
      lvc_sec := 'Procedure Start';

      BEGIN
         lvc_sec := 'Deriving CCID';


         SELECT /*+ Index(gcc GL.XXCUS_GL_CODE_COMB_N1) */
                gcc.CODE_COMBINATION_ID
           INTO ln_ccid
           FROM apps.gl_code_combinations gcc
          WHERE     gcc.segment1 = p_segment1
                AND gcc.segment2 = p_segment2
                AND gcc.segment3 = p_segment3
                AND gcc.segment4 = p_segment4
                AND gcc.segment5 = p_segment5
                AND gcc.segment6 = p_segment6
                AND gcc.segment7 = p_segment7;
      EXCEPTION
         WHEN OTHERS
         THEN
            ln_ccid := NULL;
      END;

      -- Validating CVR rule if code combination not exists
      IF ln_ccid IS NULL
      THEN
         BEGIN
            lvc_sec := ' Deriving Chart Account';
            ln_structure_num := NULL;

            SELECT id_flex_num
              INTO ln_structure_num
              FROM apps.fnd_id_flex_structures
             WHERE     id_flex_code = 'GL#'
                   AND id_flex_structure_code = 'XXCUS Accounting Flexfield'; --'HDS Accounting Flexfield';
         EXCEPTION
            WHEN OTHERS
            THEN
               ln_structure_num := NULL;
         END;

         p_concat_segs :=
               p_segment1
            || '.'
            || p_segment2
            || '.'
            || p_segment3
            || '.'
            || p_segment4
            || '.'
            || p_segment5
            || '.'
            || p_segment6
            || '.'
            || p_segment7;


         lvc_sec := 'Validating CVR for segments';
         l_valid_combination :=
            APPS.FND_FLEX_KEYVAL.VALIDATE_SEGS (
               operation          => 'CHECK_COMBINATION',
               appl_short_name    => 'SQLGL',
               key_flex_code      => 'GL#',
               structure_number   => ln_structure_num,
               concat_segments    => p_concat_segs);

         IF l_valid_combination
         THEN
            lvc_sec := 'Creating Code combination';
            l_cr_combination :=
               APPS.FND_FLEX_KEYVAL.VALIDATE_SEGS (
                  operation          => 'CREATE_COMBINATION',
                  appl_short_name    => 'SQLGL',
                  key_flex_code      => 'GL#',
                  structure_number   => ln_structure_num,
                  concat_segments    => p_concat_segs);

            BEGIN
               lvc_sec := 'Deriving CCID (After Create)';

               SELECT /*+ Index(gcc GL.XXCUS_GL_CODE_COMB_N1) */
                      gcc.CODE_COMBINATION_ID
                 INTO ln_ccid
                 FROM apps.gl_code_combinations gcc
                WHERE     gcc.segment1 = p_segment1
                      AND gcc.segment2 = p_segment2
                      AND gcc.segment3 = p_segment3
                      AND gcc.segment4 = p_segment4
                      AND gcc.segment5 = p_segment5
                      AND gcc.segment6 = p_segment6
                      AND gcc.segment7 = p_segment7;
            EXCEPTION
               WHEN OTHERS
               THEN
                  BEGIN
                     SELECT /*+ Index(gcc GL.XXCUS_GL_CODE_COMB_N1) */
                            gcc.CODE_COMBINATION_ID
                       INTO ln_ccid
                       FROM apps.gl_code_combinations gcc
                      WHERE     gcc.segment1 = p_segment1
                            AND gcc.segment2 = p_segment2
                            --  AND gcc.segment3 = p_segment3
                            --  AND gcc.segment4 = p_segment4
                            AND gcc.segment5 = p_segment5
                            AND gcc.segment6 = p_segment6
                            AND gcc.segment7 = p_segment7
                            AND ROWNUM = 1;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        ln_ccid := 1789354;
                  END;
            END;
         END IF;
      END IF;

      IF ln_ccid IS NULL
      THEN
         ln_ccid := 1789354;
      END IF;

      RETURN ln_ccid;
   EXCEPTION
      WHEN OTHERS
      THEN
         x_ret_msg := 'Error Msg:' || SUBSTR (SQLERRM, 1, 250);

         IF ln_ccid IS NULL
         THEN
            ln_ccid := 1789354;
         END IF;

         RETURN ln_ccid;
   END;
END XXWC_AR_INVOICE_CONV_PKG;
/