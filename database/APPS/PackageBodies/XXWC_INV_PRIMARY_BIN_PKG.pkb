CREATE OR REPLACE PACKAGE BODY APPS.XXWC_INV_PRIMARY_BIN_PKG AS

   /*****************************************************************************************************************************************
   *   $Header XXWC_MWA_ROUTINES_PKG $                                                                                                      *
   *   Module Name: XXWC_MWA_ROUTINES_PKG                                                                                                   *
   *                                                                                                                                        *
   *   PURPOSE:   This package is used by the extensions in MSCA                                                                            *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        16-OCT-2014  Lee Spitzer               TMS Ticket 20141001-00198                                                          *
   *                                                     WC Canada: Enhancement: RFID / Mobile Supply Chain RFID / Mobile Supply Chain      *
   *****************************************************************************************************************************************/

  
  
  PROCEDURE DEBUG_LOG (P_MESSAGE   VARCHAR2)
  IS
   
    
       PRAGMA AUTONOMOUS_TRANSACTION;
       
       BEGIN
    
        IF g_debug = 'Y' THEN
      
          FND_LOG.STRING (G_LOG_LEVEL, upper(G_PACKAGE||'.'||G_CALL_FROM) ||' '|| G_CALL_POINT, P_MESSAGE);
         
          COMMIT;
        
        END IF;
  
  EXCEPTION
  
    WHEN others THEN
        
        g_sqlcode := SQLCODE;
        g_sqlerrm := SQLERRM;
        g_message := 'Error in generating debug log ';
    
            xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                 , p_calling             => g_call_point
                                                 , p_ora_error_msg       => SQLERRM
                                                 , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                 , p_distribution_list   => g_distro_list
                                                 , p_module              => g_module);

   
  END DEBUG_LOG;

                       
   /*****************************************************************************************************************************************
   *   PROCEDURE GET_DEFAULT_TYPE_LOCAOTRS_LOV                                                                                                           *
   *                                                                                                                                        *
   *   PURPOSE:   This procedure is used to get the default type for item transaction defaults                                              *
   *                                                                                                                                        *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        22-OCT-2014  Lee Spitzer               TMS Ticket 20141001-00198                                                          *
   *                                                     WC Canada: Enhancement: RFID / Mobile Supply Chain RFID / Mobile Supply Chain      *
   *****************************************************************************************************************************************/
  
    PROCEDURE GET_DEFAULT_TYPE_LOCATORS_LOV  ( x_default                   OUT    NOCOPY t_genref --0
                                             , p_description               IN     VARCHAR2
                                    ) IS
                                    
     BEGIN
     
            g_call_from := 'GET_DEFAULT_TYPE_LOCATORS_LOV';
            g_call_point := 'Start';
            
            
            OPEN x_default FOR
            SELECT  lookup_code,
                    meaning,
                    description
            from    (
                      SELECT lookup_code, 
                             meaning,
                             CASE 
                                WHEN lookup_code = 1 THEN 'Shipping' 
                                WHEN lookup_code = 2 THEN 'Receiving' 
                                WHEN lookup_code = 3 THEN 'Move Order Receipts' END description
                      FROM   mfg_lookups
                      WHERE  lookup_type = 'MTL_DEFAULT_LOCATORS'
                      AND    nvl(enabled_flag,'N') = 'Y'
                      AND    SYSDATE BETWEEN nvl(start_date_active, SYSDATE-1) AND nvl(end_date_active, SYSDATE+1)
                      UNION
                      SELECT -1 lookup_code, 
                             'All' meaning,
                             'All' description
                      FROM   dual)
            where   description like '%'||p_description||'%';
     
    EXCEPTION
  
    WHEN others THEN
        close x_default;
        g_sqlcode := SQLCODE;
        g_sqlerrm := SQLERRM;
        g_message := 'Error in getting default type lov ';
        
            xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                 , p_calling             => g_call_point
                                                 , p_ora_error_msg       => SQLERRM
                                                 , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                 , p_distribution_list   => g_distro_list
                                                 , p_module              => g_module);

     
     END GET_DEFAULT_TYPE_LOCATORS_LOV;
     
       /*****************************************************************************************************************************************
     *   PROCEDURE PROCESS_ITEM_DEFAULT_LOCATOR                                                                                               *
     *                                                                                                                                        *
     *   PURPOSE:   This procedure is used to insert or update MTL_ITEM_SUB_DEFAULTS AND MTL_ITEM_LOC_DEFAULTS                                *
     *                                                                                                                                        *
     *    Parameters: p_organization_id  IN NUMBER - organization_id                                                                          *
     *                p_inventory_item_id IN NUMBER -- inventory_item_id                                                                      *
     *                p_subinventory_code IN VARCHAR2 - subinventory_code                                                                     *
     *                p_locator_id IN NUMBER --LOCATOR_ID                                                                                     *
     *                p_default_type  IN  NUMBER - 1 = Shipping, 2 = Receiving, 3 = Move Order Requisition, lookup type from MFG_LOOKUPS of 'MTL_DEFAULT_LOCATORS'
     *                                                                                                                                        *
     *    Out :  X_RETURN = 0 = Success, 1 = Error                                                                                            *
     *           X_MESSAGE = Return message                                                                                                   *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        22-OCT-2014  Lee Spitzer               TMS Ticket 20141001-00198                                                          *
     *                                                     WC Canada: Enhancement: RFID / Mobile Supply Chain RFID / Mobile Supply Chain      *
     *********y********************************************************************************************************************************/

        PROCEDURE PROCESS_ITEM_DEFAULT_LOCATOR   ( p_organization_id    IN VARCHAR2
                                                 , p_inventory_item_id  IN VARCHAR2
                                                 , p_subinventory_code  IN VARCHAR2
                                                 , p_locator_id IN VARCHAR2
                                                 , p_default_type IN VARCHAR2
                                                 , x_return  OUT NUMBER
                                                 , x_message OUT VARCHAR2)
                  IS
       
        l_organization_id NUMBER;
        l_inventory_item_id NUMBER;
        l_locator_id NUMBER;
        l_default_type NUMBER;
       
       BEGIN
       
        
          g_call_from := 'PROCESS_ITEM_DEFAULT_LOCATOR';
          g_call_point := 'Start';
      
          debug_log('p_organization_id ' || p_organization_id);
          debug_log('p_inventory_item_id ' || p_inventory_item_id);
          debug_log('p_subinventory_code ' || p_subinventory_code);
          debug_log('p_locator_id ' || p_locator_id);
          debug_log('p_default_type ' || p_default_type);
        
        
          BEGIN 
            SELECT to_number(p_organization_id),
                   to_number(p_inventory_item_id),
                   to_number(p_locator_id),
                   to_number(p_default_type)
            INTO  l_organization_id,
                  l_inventory_item_id,
                  l_locator_id,
                  l_default_type
            FROM  dual;
          EXCEPTION
            WHEN others THEN
              g_sqlcode := SQLCODE;
              g_sqlerrm := SQLERRM;
              g_message := 'Could not convert parameters to number';
              raise g_exception;
          END;
                   
          XXWC_MWA_ROUTINES_PKG.PROCESS_ITEM_DEFAULT_LOCATOR( p_organization_id => l_organization_id
                                                            , p_inventory_item_id => l_inventory_item_id
                                                            , p_subinventory_code => p_subinventory_code
                                                            , p_locator_id => l_locator_id
                                                            , p_default_type => l_default_type
                                                            , x_return => x_return
                                                            , x_message => x_message);
                                                            
        EXCEPTION

          WHEN OTHERS THEN
        
            g_sqlcode := SQLCODE;
            g_sqlerrm := SQLERRM;
            g_message := 'Error in getting default type lov ';
            
                xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                     , p_calling             => g_call_point
                                                     , p_ora_error_msg       => SQLERRM
                                                     , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                     , p_distribution_list   => g_distro_list
                                                     , p_module              => g_module);
    
           
       END PROCESS_ITEM_DEFAULT_LOCATOR;
       
       
     /*****************************************************************************************************************************************
     *   PROCEDURE PROCESS_ITEM_SUB_MAX                                                                                                       *
     *                                                                                                                                        *
     *   PURPOSE:   This procedure is used to insert or update MTL_ITEM_SUB_INVENTORIES                                                       *
     *                                                                                                                                        *
     *    Parameters: p_organization_id  IN NUMBER - organization_id                                                                          *
     *                p_inventory_item_id IN NUMBER -- inventory_item_id                                                                      *
     *                p_subinventory_code IN VARCHAR2 - subinventory_code                                                                     *
     *                p_max_qty  IN  NUMBER                                                                                                   *
     *                                                                                                                                        *
     *    Out :  X_RETURN = 0 = Success, 1 = Error                                                                                            *
     *           X_MESSAGE = Return message                                                                                                   *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        22-OCT-2014  Lee Spitzer               TMS Ticket 20141001-00198                                                          *
     *                                                     WC Canada: Enhancement: RFID / Mobile Supply Chain RFID / Mobile Supply Chain      *
     *********y********************************************************************************************************************************/

        PROCEDURE PROCESS_ITEM_SUB_MAX           ( p_organization_id    IN VARCHAR2
                                                 , p_inventory_item_id  IN VARCHAR2
                                                 , p_subinventory_code  IN VARCHAR2
                                                 , p_max_qty IN VARCHAR2
                                                 , x_return  OUT NUMBER
                                                 , x_message OUT VARCHAR2)
                  IS
                  
           l_item_sub_count NUMBER DEFAULT 0;
           l_organization_id NUMBER;
           l_inventory_item_id NUMBER;
           l_max_qty NUMBER;
           l_inventory_planning_code NUMBER := 6;
           l_locator_id NUMBER;
        
        BEGIN
        
          g_call_from := 'PROCESS_ITEM_SUB_MAX';
          g_call_point := 'Start';
      
          debug_log('p_organization_id ' || p_organization_id);
          debug_log('p_inventory_item_id ' || p_inventory_item_id);
          debug_log('p_subinventory_code ' || p_subinventory_code);
          debug_log('p_max_qty ' || p_max_qty);
          
          BEGIN 
            SELECT to_number(p_organization_id),
                   to_number(p_inventory_item_id),
                   to_number(p_max_qty)
            INTO  l_organization_id,
                  l_inventory_item_id,
                  l_max_qty
            FROM  dual;
          EXCEPTION
            WHEN others THEN
              g_sqlcode := SQLCODE;
              g_sqlerrm := SQLERRM;
              g_message := 'Could not convert parameters to number';
              raise g_exception;
          END;
                

         XXWC_MWA_ROUTINES_PKG.PROCESS_ITEM_SUB_MAX ( p_organization_id => l_organization_id
                                                    , p_inventory_item_id => l_inventory_item_id
                                                    , p_subinventory_code => p_subinventory_code
                                                    , p_max_qty => l_max_qty
                                                    , x_return => x_return
                                                    , x_message => x_message);
                                                            
        EXCEPTION

          WHEN OTHERS THEN
        
            g_sqlcode := SQLCODE;
            g_sqlerrm := SQLERRM;
            g_message := 'Error in getting default type lov ';
            
                xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                     , p_calling             => g_call_point
                                                     , p_ora_error_msg       => SQLERRM
                                                     , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                     , p_distribution_list   => g_distro_list
                                                     , p_module              => g_module);
    
           
       END PROCESS_ITEM_SUB_MAX;
       
       
      /*****************************************************************************************************************************************
     *   PROCEDURE PROCESS_ITEM_LOCATOR_TIE                                                                                                    *
     *                                                                                                                                        *
     *   PURPOSE:   This procedure is used to update or insert records into the MTL_SECONDARY_LOCATORS                                        *
     *                                                                                                                                        *
     *    Parameters: p_organization_id  IN NUMBER - organization_id                                                                          *
     *                p_inventory_item_id IN NUMBER -- inventory_item_id                                                                      *
     *                p_subinventory_code IN VARCHAR2 - subinventory_code                                                                     *
     *                p_locator_id IN NUMBER --LOCATOR_ID                                                                                     *
     *                                                                                                                                        *
     *    Out :  X_RETURN = 0 = Success, 1 = Error                                                                                            *
     *           X_MESSAGE = Return message                                                                                                   *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        22-OCT-2014  Lee Spitzer               TMS Ticket 20141001-00198                                                          *
     *                                                     WC Canada: Enhancement: RFID / Mobile Supply Chain RFID / Mobile Supply Chain      *
     *********y********************************************************************************************************************************/

        PROCEDURE PROCESS_ITEM_LOCATOR_TIE       ( p_organization_id    IN VARCHAR2
                                                 , p_inventory_item_id  IN VARCHAR2
                                                 , p_subinventory_code  IN VARCHAR2
                                                 , p_locator_id IN VARCHAR2
                                                 , x_return  OUT NUMBER
                                                 , x_message OUT VARCHAR2)
                  IS
            
            l_organization_id NUMBER;
            l_inventory_item_id NUMBER;
            l_locator_id NUMBER;
              
        BEGIN
        
          g_call_from := 'PROCESS_ITEM_LOCATOR_TIE';
          g_call_point := 'Start';
      
          debug_log('p_organization_id ' || p_organization_id);
          debug_log('p_inventory_item_id ' || p_inventory_item_id);
          debug_log('p_subinventory_code ' || p_subinventory_code);
          debug_log('p_locator_id ' || p_locator_id);
        
          BEGIN 
            SELECT to_number(p_organization_id),
                   to_number(p_inventory_item_id),
                   to_number(p_locator_id)
            INTO  l_organization_id,
                  l_inventory_item_id,
                  l_locator_id
            FROM  dual;
          EXCEPTION
            WHEN others THEN
              g_sqlcode := SQLCODE;
              g_sqlerrm := SQLERRM;
              g_message := 'Could not convert parameters to number';
              raise g_exception;
          END;
                
      
      
         XXWC_MWA_ROUTINES_PKG.PROCESS_ITEM_LOCATOR_TIE ( p_organization_id => l_organization_id
                                                        , p_inventory_item_id => l_inventory_item_id
                                                        , p_subinventory_code => p_subinventory_code
                                                        , p_locator_id => l_locator_id
                                                        , x_return => x_return
                                                        , x_message => x_message);
                                                            
        EXCEPTION

          WHEN OTHERS THEN
        
            g_sqlcode := SQLCODE;
            g_sqlerrm := SQLERRM;
            g_message := 'Error in getting default type lov ';
            
                xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                     , p_calling             => g_call_point
                                                     , p_ora_error_msg       => SQLERRM
                                                     , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                     , p_distribution_list   => g_distro_list
                                                     , p_module              => g_module);
      
        
        
        END PROCESS_ITEM_LOCATOR_TIE;
        
    /*****************************************************************************************************************************************
   *   PROCEDURE GET_MILD_INQUIRY                                                
   *   PURPOSE:   This procedure is used to get the results from MTL_ITEM_LOC_DEFAULTS                                                      *
   *                                                                                                                                        *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        22-OCT-2014  Lee Spitzer               TMS Ticket 20141001-00198                                                          *
   *                                                     WC Canada: Enhancement: RFID / Mobile Supply Chain RFID / Mobile Supply Chain      *
   *****************************************************************************************************************************************/
  
    PROCEDURE GET_MILD_INQUIRY  ( x_default                   OUT    NOCOPY t_genref --0
                                , p_organization_id           IN     VARCHAR2
                                , p_inventory_item_id         IN     VARCHAR2
                                , p_subinventory_code         IN     VARCHAR2
                                , p_locator_id                IN     VARCHAR2
                                , p_default_type              IN     VARCHAR2
                                )
  
              IS
              
      l_organization_id NUMBER;
      l_inventory_item_id NUMBER;
      l_locator_id NUMBER;
      l_default_type NUMBER;
              
    BEGIN
    
          g_call_from := 'GET_MILD_INQUIRY';
          g_call_point := 'Start';
      
          debug_log('p_organization_id ' || p_organization_id);
          debug_log('p_inventory_item_id ' || p_inventory_item_id);
          debug_log('p_subinventory_code ' || p_subinventory_code);
          debug_log('p_locator_id ' || p_locator_id);
          debug_log('p_default_type ' || p_default_type);
          
          BEGIN 
            SELECT to_number(p_organization_id),
                   to_number(p_inventory_item_id),
                   to_number(p_locator_id),
                   to_number(p_default_type)
            INTO  l_organization_id,
                  l_inventory_item_id,
                  l_locator_id,
                  l_default_type
            FROM  dual;
          EXCEPTION
            WHEN others THEN
              g_sqlcode := SQLCODE;
              g_sqlerrm := SQLERRM;
              g_message := 'Could not convert parameters to number';
              raise g_exception;
          END;
                
          
          OPEN x_default FOR
            SELECT msik.concatenated_segments item_number,
                   mild.inventory_item_id,
                   msik.description,
                   mild.subinventory_code,
                   wilk.concatenated_segments bin_locator,
                   mild.locator_id,
                   mild.default_type,
                   nvl(xxwc_mwa_routines_pkg.GET_ITEM_SUB_MAX_QTY(mild.organization_id, mild.inventory_item_id, mild.subinventory_code),0) MAX,
                   CASE 
                    WHEN ml.lookup_code = 1 THEN 'Shipping' 
                    WHEN ml.lookup_code = 2 THEN 'Receiving' 
                    WHEN ml.lookup_code = 3 THEN 'Move Order Receipts' END default_description
            FROM   mtl_item_loc_defaults mild,
                   mtl_system_items_kfv msik,
                   wms_item_locations_kfv wilk,
                   mfg_lookups ml
            WHERE  msik.organization_id = mild.organization_id
            AND    msik.inventory_item_id = mild.inventory_item_id
            AND    wilk.subinventory_code = mild.subinventory_code
            AND    wilk.inventory_location_id = mild.locator_id
            AND    ml.lookup_code = mild.default_type
            AND    ml.lookup_type = 'MTL_DEFAULT_LOCATORS'
            AND    nvl(ml.enabled_flag,'N') = 'Y'
            AND    SYSDATE BETWEEN nvl(ml.start_date_active, SYSDATE-1) AND nvl(ml.end_date_active, SYSDATE+1)
            ---Parameters
            AND    mild.organization_id = l_organization_id
            AND    mild.inventory_item_id = nvl(l_inventory_item_id, mild.inventory_item_id)
            AND    mild.subinventory_code = nvl(p_subinventory_code, mild.subinventory_code)
            AND    mild.locator_id = nvl(l_locator_id, mild.locator_id)
            and    mild.default_type = nvl(l_default_type, mild.default_type);
    EXCEPTION
        WHEN others THEN
            CLOSE x_default;
            g_sqlcode := SQLCODE;
            g_sqlerrm := SQLERRM;
            g_message := 'Error getting lov for mtl_item_locator_defauls ' ;
            
                xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                     , p_calling             => g_call_point
                                                     , p_ora_error_msg       => SQLERRM
                                                     , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                     , p_distribution_list   => g_distro_list
                                                     , p_module              => g_module);
      
    
    END GET_MILD_INQUIRY;
     
   
END XXWC_INV_PRIMARY_BIN_PKG;