CREATE OR REPLACE PACKAGE BODY APPS."XXCUSGL_IMPORT_PKG" AS
  /**************************************************************************

  File Name: xxcusgl_import_pkg

  PROGRAM TYPE: PL/SQL Package

  PURPOSE: Automates journal import by allowing a program to be scheduled which 
  will pick out specific sources form the gl_interface table.

  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     03/15/2012    John Bozik    Initial creation of the package
  ********************************************************************************/ 
  
  
  /********* Procedure To Submit Journal Import  ***************/

  PROCEDURE submit_journal_import (errbuf  OUT VARCHAR2,
                                  retcode OUT NUMBER) IS

   
    --v_request_id        NUMBER := 0;
    --l_rphase            VARCHAR2(30);
    --l_rstatus           VARCHAR2(30);
    --l_dphase            VARCHAR2(30);
    --l_dstatus           VARCHAR2(30);
    --l_conc_id           NUMBER := 0;
    --l_program_completed BOOLEAN;
    --l_message           VARCHAR2(240);
    --v_group_id          NUMBER;
    --l_group_id          NUMBER;
    v_run_id            NUMBER := 0;
    l_user_name         VARCHAR2(30) := 'GLINTERFACE';
    l_resp_key          VARCHAR2(30) := 'GNRL_LDGR_FSS';
    l_user_id           NUMBER;
    l_resp_id           NUMBER;
    l_resp_appl_id      NUMBER;    
    l_sec               VARCHAR2(4000);
    l_req_id_1          NUMBER;
    l_err_msg             VARCHAR2(3000);
    l_err_code            NUMBER;
    l_procedure_name VARCHAR2(75) := 'submit_journal_import';
    l_distro_list   VARCHAR2(75) DEFAULT 'HDS.OracleGLSupport@hdsupply.com';

  BEGIN

        l_sec := 'Getting User Info.'; 
        fnd_file.put_line(fnd_file.log, l_sec);
        fnd_file.put_line(fnd_file.output, l_sec);              
       SELECT user_id
         INTO l_user_id
         FROM fnd_user
        WHERE user_name = l_user_name;           
      
        l_sec := 'Getting Responsibility Info.'; 
        fnd_file.put_line(fnd_file.log, l_sec);
        fnd_file.put_line(fnd_file.output, l_sec);
      SELECT responsibility_id
        INTO l_resp_id
        FROM fnd_responsibility
       WHERE responsibility_key = l_resp_key;       
       
        l_sec := 'Getting Application ID Info.'; 
        fnd_file.put_line(fnd_file.log, l_sec);
        fnd_file.put_line(fnd_file.output, l_sec);
      SELECT responsibility_application_id
        INTO l_resp_appl_id
        FROM fnd_user_resp_groups
       WHERE user_id = l_user_id
         AND responsibility_id = l_resp_id;         
         
      fnd_global.apps_initialize(l_user_id, l_resp_id, l_resp_appl_id);
            
    l_sec := 'Insert into gl_interface_control.';  
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);    
    FOR c_batch_loop in (SELECT DISTINCT j.je_source_name,
                      'S' S,
                      g.group_id,
                      g.ledger_id,
                      'GL_INTERFACE' gl
        FROM gl.gl_interface g, gl.gl_je_sources_tl j
       WHERE g.user_je_source_name in
             (SELECT description
                FROM apps.fnd_lookup_values
               WHERE lookup_type = 'XXCUS_GLAUTOJIMP'
                 AND lookup_code like 'SRC%') 
                 AND g.user_je_source_Name = j.user_je_source_name
                 AND g.status = 'NEW' ) LOOP               
                                           
   INSERT INTO gl_interface_control
     (je_source_name,
      status,
      interface_run_id,
      group_id,
      set_of_books_id,
      interface_table_name)
      VALUES
     (c_batch_loop.je_source_name,
      c_batch_loop.s,
      c_batch_loop.group_id,
      c_batch_loop.group_id,
      c_batch_loop.ledger_id,
      c_batch_loop.gl); 
      
     SELECT gl_journal_import_s.NEXTVAL INTO v_run_id FROM dual;    
      
      l_sec := 'Submitting Import Process.';
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);

      --Once initialized then Submit the Concurrent Request Journal Import
      l_req_id_1 := fnd_request.submit_request(application => 'SQLGL',
                                               program => 'GLLEZL',
                                               description => NULL,
                                               start_time => SYSDATE,
                                               sub_request => FALSE,
                                               argument1 => c_batch_loop.group_id,
                                               argument2 => c_batch_loop.ledger_id,
                                               argument3 => 'N',
                                               argument4 => NULL,
                                               argument5 => NULL,
                                               argument6 => 'N',
                                               argument7 => 'O');     
      
          
      End loop;
    
     COMMIT;
     
EXCEPTION
    WHEN program_error THEN
      ROLLBACK;    

      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := 'Error in program ' || l_procedure_name || ' Error: ' ||
                    l_sec;
      l_err_msg  := l_err_msg || ' ERROR ' || substr(SQLERRM, 1, 2000);    

      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode := l_err_code;
      errbuf  := l_err_msg;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_procedure_name,
                                           p_calling => l_sec,
                                           p_request_id => fnd_global.conc_request_id,
                                           p_ora_error_msg => SQLERRM,
                                           p_error_desc => 'Error running HDS Journal Import - FSS ONLY in XXCUSGL_IMPORT_PKG.submit_journal_import package with Program Error Exception',
                                           p_distribution_list => l_distro_list,
                                           p_module => 'GL');
    

    WHEN OTHERS THEN
     fnd_file.put_line(fnd_file.log, 'Going into error package');
     fnd_file.put_line(fnd_file.output, 'Going into error package');       
          xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_procedure_name,
                                           p_calling => l_sec,
                                           p_request_id => fnd_global.conc_request_id,
                                           p_ora_error_msg => SQLERRM,
                                           p_error_desc => 'Error running HDS Journal Import - FSS ONLY in XXCUSGL_IMPORT_PKG.submit_journal_import package with OTHERS Exception',
                                           p_distribution_list => l_distro_list,
                                           p_module => 'GL'); 
    fnd_file.put_line(fnd_file.log, l_procedure_name);
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.log, fnd_global.conc_request_id);
    fnd_file.put_line(fnd_file.log, SQLERRM);
    fnd_file.put_line(fnd_file.log, l_distro_list);                                                                             
    fnd_file.put_line(fnd_file.log, 'Done into error package');
    fnd_file.put_line(fnd_file.output, 'Done into error package'); 
    
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := l_sec || l_err_msg || ' ErRoR ' || SQLCODE ||
                    substr(SQLERRM, 1, 2000);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode := l_err_code;
      errbuf  := l_err_msg;
    

  END;
  END;
/
