CREATE OR REPLACE PACKAGE BODY APPS.xxwc_rental_engine_pkg
AS 
/*************************************************************************
*  Copyright (c) 2011 Lucidity Consulting Group
*  All rights reserved.
**************************************************************************
*   $Header XXXWC_RENTAL_ENGINE_PKG.pkb $
*   Module Name: XXWC_RENTAL_ENGINE_PKG .pkb
*
*   PURPOSE:   This package is used by the XXWC Rental Recurring Invoice Engine Conc Program
*              to create RMA Lines for Short and Long Term Rentals and also Charge Lines
*              for Long Term Rentals.
*
*   REVISIONS:
*   Ver        Date        Author                     Description
*   ---------  ----------  ---------------         -------------------------
*   1.0        09/29/2011  Srinivas Malkapuram             Initial Version
*   1.1        10/30/2013  Gopi Damuluri           TMS# 20130823-00477
*                                                  -- Copy Shipping Instructions, Serial Number from ItemLine to ChargeLine
*   1.2        12/18/2013  Ram Talluri            TMS# 20131213-00055 - Restricting Billing engineto look only rental orders. 
*   1.3        01/07/2014 Raghav Velichetti      TMS # 20140107-00054 Bug fix to stop Rental date cascading to Awaiting Return lines 
*   2.0        8/1/2014     Ram Talluri                  to_number function added for TMS #20140801-00035 
*   3.0        8/15/2014     Ram Talluri           TMS #20140815-00209 Remove Rental OVerride to $300 if Selling Price is Zero 
*   4.0		   20/10/2014	  Veera C	            TMS#20141002-00037 Canada Mulit Org Changes
*   5.0        13/06/2016   Niraj K Ranjan        TMS#20160606-00067   Rental recurring invoice engine logic change for better error handling
*   6.0        30/Aug/2018  Niraj K Ranjan         TMS#20180622-00050   Rental Price Not Created # 28917535
* ***************************************************************************/
   l_request_id NUMBER;
/*************************************************************************
*   Procedure : LOG_ORDER_ERROR
*
*   PURPOSE:   This is the 
*
*   REVISIONS:
*   Ver        Date          Author                     Description
*   ---------  ------------  ---------------         -------------------------
*   5.0        15-Jun-2016   Niraj K Ranjan           TMS#20160606-00067   Rental recurring invoice engine logic change for better error handling
* ************************************************************************/
   PROCEDURE log_order_error (p_order_header_id   IN       NUMBER,
                              p_order_line_id     IN       NUMBER,
                              p_error_code        IN       VARCHAR2,
                              p_error_message     IN       VARCHAR2,
                              p_retcode           OUT      VARCHAR2,
                              p_retmsg            OUT      VARCHAR2							  
                             )
   IS
      PRAGMA AUTONOMOUS_TRANSACTION; 
      --l_request_id NUMBER := fnd_global.conc_request_id;
      l_error_index NUMBER;
   BEGIN
      p_retcode := NULL;
	  p_retmsg  := NULL;

      SELECT NVL(MAX(ERROR_INDEX),0)+1 INTO l_error_index
      FROM XXWC_RENTAL_ENGINE_ERROR_LOG
      WHERE ORDER_HEADER_ID = p_order_header_id
	  AND REQUEST_ID =  l_request_id;
	  
      INSERT INTO XXWC_RENTAL_ENGINE_ERROR_LOG
	             ( ORDER_HEADER_ID  
				  ,ORDER_LINE_ID    
				  ,REQUEST_ID       
				  ,ERROR_CODE
                  ,ERROR_INDEX				  
				  ,ERROR_MESSAGE    
				  ,CREATION_DATE    
				  ,CREATED_BY       
				  ,LAST_UPDATE_DATE 
				  ,LAST_UPDATED_BY  
				  ,LAST_UPDATE_LOGIN
				 )
	       VALUES( p_order_header_id
                  ,p_order_line_id
                  ,l_request_id
                  ,p_error_code
				  ,l_error_index
                  ,p_error_message
                  ,SYSDATE
                  ,FND_GLOBAL.USER_ID
                  ,SYSDATE
                  ,FND_GLOBAL.USER_ID
                  ,FND_GLOBAL.LOGIN_ID
                 );
      COMMIT;
   EXCEPTION
      WHEN OTHERS THEN
         p_retcode := SQLCODE;
         p_retmsg  := SQLERRM;
   END log_order_error;
/*************************************************************************
*   Procedure : log_msg
*
*   PURPOSE:   This  Procedure is used to log message in log file
*
*   REVISIONS:
*   Ver        Date        Author                     Description
*   ---------  ----------  ---------------         -------------------------
*   1.0        09/29/2011  Srinivas Malkapuram             Initial Version
*   1.1        12/18/2013  Ram Talluri            TMS# 20131213-00055 - Restricting Billing engineto look only rental orders.
* 3.0       8/15/2014     Ram Talluri           TMS #20140815-00209 Remove Rental OVerride to $300 if Selling Price is Zero
* ************************************************************************/   
   PROCEDURE log_msg (
      p_debug_level   IN   NUMBER,
      p_mod_name      IN   VARCHAR2,
      p_debug_msg     IN   VARCHAR2
   )
   IS
      PRAGMA AUTONOMOUS_TRANSACTION;
   BEGIN
      --   IF (p_debug_level >= FND_LOG.G_CURRENT_RUNTIME_LEVEL)
       --  THEN
      fnd_log.STRING (p_debug_level, p_mod_name, p_debug_msg);
      --  END IF;
      fnd_file.put_line (fnd_file.LOG, p_debug_msg);
      COMMIT;
   END log_msg;

/*************************************************************************
*   Procedure : MAIN
*
*   PURPOSE:   This is the MAIN Procedure that is being called by
*
*   REVISIONS:
*   Ver        Date        Author                     Description
*   ---------  ----------  ---------------         -------------------------
*   1.0        09/29/2011  Srinivas Malkapuram             Initial Version
*   1.1        12/18/2013  Ram Talluri            TMS# 20131213-00055 - Restricting Billing engineto look only rental orders.
* 3.0       8/15/2014     Ram Talluri           TMS #20140815-00209 Remove Rental OVerride to $300 if Selling Price is Zero
* ************************************************************************/
   PROCEDURE main (
      errbuf           OUT      VARCHAR2,
      retcode          OUT      NUMBER,
      p_order_number   IN       VARCHAR2,
      p_log_enabled    IN       VARCHAR2 DEFAULT 'N'
   )
   IS
/*************************************************************************
   *  Copyright (c) 2011 Lucidity Consulting Group
   *  All rights reserved.
   **************************************************************************
   *   $Header XXXWC_RENTAL_ENGINE_PKG .pkb $
   *   Module Name: XXWC_RENTAL_ENGINE_PKG.pkb
   *
   *   PURPOSE:   This package is used by the XXWC Rental Recurring Invoice Engine Conc Program
   *              to create RMA Lines and Charge Lines for Long Term Rentals
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        09/29/2011  Srinivas Malkapuram             Initial Version
   *   2.0        07/12/2012  Consuelo Gonzalez        Changes to use new rental
   *                                                   dates  in DFF fields
   *   5.0        13/06/2016   Niraj K Ranjan        TMS#20160606-00067   Rental recurring invoice engine logic change for better error handling
   * ***************************************************************************/

      /* Cursor to get the List of Rental Orders */
      CURSOR cur_get_rental_orders
      IS
         SELECT   *
             FROM apps.oe_order_headers oeh
            WHERE order_number = NVL (p_order_number, order_number)
              AND oeh.flow_status_code NOT IN
                                           ('CANCELLED', 'CLOSED', 'ENTERED')
              AND UPPER (oeh.attribute1) IN
                              (g_oeh_attr1_long_term, g_oeh_attr1_short_term)
              AND EXISTS --Added  Ram Talluri 12/18/2013 TMS# 20131213-00055
                (SELECT 1
                   FROM oe_transaction_types_tl
                  WHERE     language = 'US'
                        AND name IN
                               ('WC LONG TERM RENTAL', 'WC SHORT TERM RENTAL') --end  Ram Talluri 12/18/2013 TMS# 20131213-00055
                        AND transaction_type_id = oeh.order_type_id)
         ORDER BY order_number DESC;

/* Cursor to Get only the Shipped Lines */
      CURSOR cur_get_rental_line_info (p_order_header_id NUMBER)
      IS
         SELECT   oel.*
                       -- 07/12/2012 CG: Added for new rental dates in DFF
                  ,
                  TO_DATE (oel.attribute12,
                           'YYYY/MM/DD HH24:MI:SS'
                          ) new_rental_date
             FROM apps.oe_order_lines oel
            WHERE oel.header_id = p_order_header_id
              AND oel.flow_status_code NOT IN ('CANCELLED','AWAITING_SHIPPING')
              AND link_to_line_id IS NULL
              -- 07/12/2012 CG
              -- actual_shipment_date IS NOT NULL
              AND NVL (oel.attribute12, oel.actual_shipment_date) IS NOT NULL
         ORDER BY line_id;

      CURSOR cur_get_rma_details (p_order_header_id NUMBER, p_line_id NUMBER)
      IS
         SELECT   line_number, shipment_number, line_id, actual_shipment_date,
                  flow_status_code, ordered_quantity,
                  
                  -- 07/12/2012 CG: Added for new rental dates in DFF
                  TO_DATE (attribute12,
                           'YYYY/MM/DD HH24:MI:SS'
                          ) new_rental_date
             FROM apps.oe_order_lines
            WHERE header_id = p_order_header_id
              AND return_attribute1 = p_order_header_id
              AND return_attribute2 = p_line_id
              AND line_category_code = 'RETURN'
              AND flow_status_code <> 'CANCELLED'
         ORDER BY shipment_number;

      -- Declare Local Variables
      lx_rma                   NUMBER;
      lx_rma_status            VARCHAR2 (20);
      l_return_status          VARCHAR2 (1);
      lx_return_status         VARCHAR2 (1);
      lx_charge_line_id        NUMBER;
      l_billing_cycle          NUMBER;
      l_billing_days           NUMBER;
      l_bill                   VARCHAR2 (1)        := 'N';
      l_return_subinv          VARCHAR2 (20);
      l_return_date            DATE;
      l_rental_days            NUMBER;
      l_chr_line_exists        VARCHAR2 (1)        := 'N';
      l_msg_data               VARCHAR2 (2000);
      l_initial_bill_cycle     NUMBER              := 1;
      l_initial_bill_days      NUMBER              := 0;
      l_rcv_qty                NUMBER              := 0;
      l_debug_msg              VARCHAR2 (2000);
      v_rma_cnt                NUMBER              := 0;
      l_rma_status             VARCHAR2 (20);
      v_adj_billing_cycle      NUMBER;
      l_any_chr_bill_cycle     NUMBER;
      l_serial_status          BOOLEAN;
      l_reservation_record     reservation_int_rec;
      l_ship                   VARCHAR2 (1)        := 'N';
      l_delivery_id            NUMBER;
      l_ship_date              DATE;
      l_rental_billing_terms   VARCHAR2 (60);
      l_actual_billing_cycle   NUMBER;
      x_return_status          VARCHAR2 (10);
      x_error_msg              VARCHAR2 (1000);
      x_error_count            NUMBER;
      l_api_name               VARCHAR2 (30)       := 'MAIN';
      l_module_name            VARCHAR2 (120);
      l_actual_shipment_date   DATE;
      -- 07/17/2012
      l_tmp_rental_date        DATE;
      l_curr_cycle             NUMBER;
      l_total_cycles           NUMBER;
      l_proc_prorated_charge   VARCHAR2 (1);
      --Define user defined exception
      skip_record              EXCEPTION;
      no_charge_line_needed    EXCEPTION;
      api_failure              EXCEPTION;
      
      -- local error handling variables
   l_req_id          NUMBER := fnd_global.conc_request_id;
   l_err_callfrom    VARCHAR2 (75):= 'XXWC_RENTAL_ENGINE_PKG.MAIN';
   l_err_callpoint   VARCHAR2 (75) := 'START';
   l_distro_list     VARCHAR2 (75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
   l_message         VARCHAR2 (2000);
   
      l_order_header_id         NUMBER;         --Ver 5.0
	  l_order_line_id           NUMBER;         --Ver 5.0
	  l_error_code              VARCHAR2(200);  --Ver 5.0
	  l_error_message           VARCHAR2(3000); --Ver 5.0
	  l_retcode                 VARCHAR2(200);  --Ver 5.0
	  l_retmsg                  VARCHAR2(3000); --Ver 5.0
   
   BEGIN
      retcode := '0';
      errbuf  := '';
      l_request_id := l_req_id;
      -- Begining of API
      l_module_name := g_module_name || '.' || l_api_name;
      --Initialize the API out parameter
      x_return_status := 'S';
      x_error_msg := NULL;
      x_error_count := 0;
      oe_debug_pub.setdebuglevel (fnd_profile.VALUE ('OE: Debug Level'));
      
      l_err_callpoint := 'STAGE1';

      FOR get_rental_orders_rec IN cur_get_rental_orders
      LOOP                               /* Main Loop for Selecting Orders */
         BEGIN  --Ver 5.0
            l_error_code    := NULL; --Ver 5.0
			l_error_message := NULL; --Ver 5.0
            l_order_header_id := get_rental_orders_rec.header_id; --Ver 5.0
            xxwc_rental_engine_pkg.log_msg
               (p_debug_level      => g_level_statement,
                p_mod_name         => l_module_name,
                p_debug_msg        => '***************************************************'
               );
            xxwc_rental_engine_pkg.log_msg
                               (p_debug_level      => g_level_statement,
                                p_mod_name         => l_module_name,
                                p_debug_msg        =>    'Processing Order# '
                                                      || get_rental_orders_rec.order_number
                               );
            xxwc_rental_engine_pkg.log_msg
               (p_debug_level      => g_level_statement,
                p_mod_name         => l_module_name,
                p_debug_msg        => '***************************************************'
               );  
            l_err_callpoint := 'STAGE2';
            FOR get_rental_line_info_rec IN
               cur_get_rental_line_info (get_rental_orders_rec.header_id)
            LOOP                                   /* Loop for Selecting Lines */
               l_order_line_id := get_rental_line_info_rec.line_id; --Ver 5.0
               IF p_log_enabled IN ('y', 'Y')
               THEN
                  xxwc_rental_engine_pkg.log_msg
                                            (p_debug_level      => g_level_statement,
                                             p_mod_name         => l_module_name,
                                             p_debug_msg        => '  '
                                            );
                  xxwc_rental_engine_pkg.log_msg
                             (p_debug_level      => g_level_statement,
                              p_mod_name         => l_module_name,
                              p_debug_msg        =>    ' Processing Line#  '
                                                    || get_rental_line_info_rec.line_number
                             );
                  xxwc_rental_engine_pkg.log_msg
                     (p_debug_level      => g_level_statement,
                      p_mod_name         => l_module_name,
                      p_debug_msg        => '--------------------------------------------------'
                     );
               END IF;
               
            
               SELECT COUNT (line_id)
                 INTO v_rma_cnt
                 FROM apps.oe_order_lines
                WHERE header_id = get_rental_orders_rec.header_id
                  AND return_attribute1 = get_rental_orders_rec.header_id
                  AND return_attribute2 = get_rental_line_info_rec.line_id
                  AND line_category_code = 'RETURN'
                  AND flow_status_code <> 'CANCELLED';
                  
                  l_err_callpoint := 'STAGE3';
            
               IF v_rma_cnt = 0
               THEN
                  IF p_log_enabled IN ('y', 'Y')
                  THEN
                     xxwc_rental_engine_pkg.log_msg
                              (p_debug_level      => g_level_statement,
                               p_mod_name         => l_module_name,
                               p_debug_msg        => 'NO RMA Exists. Creating RMA Line'
                              );
                  END IF;
                  
                  l_err_callpoint := 'STAGE4';
            
                  /* IF RMA doesn't exist, Create RMA Line */
                  create_rma_line
                                (get_rental_line_info_rec.header_id,
                                 get_rental_line_info_rec.line_id,
                                 0,
                                 get_rental_line_info_rec.inventory_item_id,
                                 -- 07/12/2012 CG: Changed to account for change in Rental DFF
                                 NVL (get_rental_line_info_rec.ordered_quantity,
                                      get_rental_line_info_rec.shipped_quantity
                                     ),
                                 get_rental_line_info_rec.ship_from_org_id,
                                 l_return_status
                                );
                                
                   
                  IF l_return_status = fnd_api.g_ret_sts_success
                  THEN
                     xxwc_rental_engine_pkg.log_msg
                                 (p_debug_level      => g_level_procedure,
                                  p_mod_name         => l_module_name,
                                  p_debug_msg        => 'RMA Line Created Successfully'
                                 );
                  l_err_callpoint := 'STAGE5';
                  ELSE
                     xxwc_rental_engine_pkg.log_msg
                                          (p_debug_level      => g_level_error,
                                           p_mod_name         => l_module_name,
                                           p_debug_msg        => 'RMA Line ERRORED Out'
                                          );
                     xxwc_rental_engine_pkg.log_msg
                        (p_debug_level      => g_level_statement,
                         p_mod_name         => l_module_name,
                         p_debug_msg        => '--------------------------------------------------'
                        );
                     l_err_callpoint := 'STAGE6';
                     l_return_status := 'E';
                     RAISE api_failure;
                  END IF;
            
                  /* If Order is Long Term, Create CHARGE Line as well */
                  IF     UPPER (get_rental_orders_rec.attribute1) =
                                                            g_oeh_attr1_long_term
                     AND l_return_status = 'S'
                  THEN
                     get_rma_details
                             (p_header_id        => get_rental_line_info_rec.header_id,
                              p_line_id          => get_rental_line_info_rec.line_id,
                              p_log_enabled      => p_log_enabled,
                              x_rma              => lx_rma,
                              x_rma_status       => lx_rma_status
                             );
            
                     IF p_log_enabled IN ('y', 'Y')
                     THEN
                        xxwc_rental_engine_pkg.log_msg
                           (p_debug_level      => g_level_statement,
                            p_mod_name         => l_module_name,
                            p_debug_msg        => 'Creating Charge Line for Long Term Rental'
                           );
                     END IF;
                     
                     l_err_callpoint := 'STAGE7';
            
                     create_charge_line
                         (get_rental_line_info_rec.header_id,
                          get_rental_line_info_rec.line_id,
                          get_rental_line_info_rec.inventory_item_id,
                          --07/12/2012 CGL Changed for Rental Date DFF Change
                          -- get_rental_line_info_rec.shipped_quantity,
                          NVL (get_rental_line_info_rec.ordered_quantity,
                               get_rental_line_info_rec.shipped_quantity
                              ),
                          l_initial_bill_cycle -- Initial Bill Cycle (Attribute9)
                                              ,
                          l_initial_bill_days                     -- Billing Days
                                             ,
                          get_rental_line_info_rec.ship_from_org_id,
                          get_rental_line_info_rec.attribute2,
                          get_rental_line_info_rec.attribute4,
                          get_rental_line_info_rec.unit_list_price,
                          NULL                             -- Return SubInventory
                              ,
                          NULL                                --Received Quantity
                              ,
                          lx_rma,
                          lx_rma_status,
                          NULL                               -- p_Apply_Hold_Flag
                              ,
                          lx_return_status,
                          lx_charge_line_id
                         );
            
                     -- 04/02/2012 CG
                     IF p_log_enabled IN ('y', 'Y')
                     THEN
                        xxwc_rental_engine_pkg.log_msg
                                            (p_debug_level      => g_level_statement,
                                             p_mod_name         => l_module_name,
                                             p_debug_msg        =>    'Bill Cycle '
                                                                   || l_initial_bill_cycle
                                            );
                     END IF;
            
                     IF lx_return_status = fnd_api.g_ret_sts_success
                     THEN
                        IF p_log_enabled IN ('y', 'Y')
                        THEN
                           xxwc_rental_engine_pkg.log_msg
                              (p_debug_level      => g_level_procedure,
                               p_mod_name         => l_module_name,
                               p_debug_msg        => 'Charge Line Created Successfully'
                              );
                        END IF;
                     l_err_callpoint := 'STAGE8';
                     ELSE
                        xxwc_rental_engine_pkg.log_msg
                                      (p_debug_level      => g_level_error,
                                       p_mod_name         => l_module_name,
                                       p_debug_msg        => 'Charge Line ERRORED out.'
                                      );
                        l_err_callpoint := 'STAGE9';
                        lx_return_status := 'E';
                        RAISE api_failure;
                     END IF;
                  END IF;
               /* IF RMA exists and Order is Long term, then create Charge Line  for the corresponding billing Cycle if doesn't exist*/
               ELSIF (    v_rma_cnt > 0
                      AND UPPER (get_rental_orders_rec.attribute1) =
                                                            g_oeh_attr1_long_term
                     )
               THEN
                  IF p_log_enabled IN ('y', 'Y')
                  THEN
                     xxwc_rental_engine_pkg.log_msg
                         (p_debug_level      => g_level_error,
                          p_mod_name         => l_module_name,
                          p_debug_msg        => 'RMA Already Exists for LongTerm Order'
                         );
                  END IF;
                  
                  l_err_callpoint := 'STAGE10';
            
                  /* Loop for Every RMA Record */
                  FOR get_rma_details_rec IN
                     cur_get_rma_details (get_rental_orders_rec.header_id,
                                          get_rental_line_info_rec.line_id
                                         )
                  LOOP
                     xxwc_rental_engine_pkg.log_msg
                                            (p_debug_level      => g_level_statement,
                                             p_mod_name         => l_module_name,
                                             p_debug_msg        => '  '
                                            );
                     xxwc_rental_engine_pkg.log_msg
                              (p_debug_level      => g_level_statement,
                               p_mod_name         => l_module_name,
                               p_debug_msg        =>    ' Processing RMA Line# '
                                                     || get_rma_details_rec.line_number
                                                     || '.'
                                                     || get_rma_details_rec.shipment_number
                              );
                     xxwc_rental_engine_pkg.log_msg
                        (p_debug_level      => g_level_statement,
                         p_mod_name         => l_module_name,
                         p_debug_msg        => '###################################################'
                        );
                     l_rma_status := get_rma_details_rec.flow_status_code;
            
                     IF p_log_enabled IN ('y', 'Y')
                     THEN
                        xxwc_rental_engine_pkg.log_msg
                                            (p_debug_level      => g_level_statement,
                                             p_mod_name         => l_module_name,
                                             p_debug_msg        =>    ' RMA Status is '
                                                                   || l_rma_status
                                            );
                     END IF;
                     
                     l_err_callpoint := 'STAGE11';
            
                     /* Get the Billing Cycle */
                     -- 07/12/2012 CG: Added to use new DFF for rental dates
                     l_tmp_rental_date := NULL;
                     l_tmp_rental_date :=
                        NVL (get_rental_line_info_rec.new_rental_date,
                             get_rental_line_info_rec.actual_shipment_date
                            );
                     xxwc_rental_engine_pkg.log_msg
                                         (p_debug_level      => g_level_statement,
                                          p_mod_name         => l_module_name,
                                          p_debug_msg        =>    'Shipment Date(377) '
                                                                || l_tmp_rental_date
                                         );
            
                     BEGIN
                        get_billing_cycle
                           (p_actual_shipment_date      => l_tmp_rental_date,
                                -- get_rental_line_info_rec.actual_shipment_date,
                            p_line_id                   => get_rental_line_info_rec.line_id,
                            p_rma_status                => l_rma_status,
                            --lx_rma_status,
                            p_rental_terms              => get_rental_line_info_rec.attribute2,
                            x_billing_cycle             => l_billing_cycle,
                            x_billing_days              => l_billing_days,
                            x_bill                      => l_bill
                           );
                           
                           l_err_callpoint := 'STAGE12';
            
                        -- 04/02/2012 CG
                        IF p_log_enabled IN ('y', 'Y')
                        THEN
                           xxwc_rental_engine_pkg.log_msg
                                            (p_debug_level      => g_level_statement,
                                             p_mod_name         => l_module_name,
                                             p_debug_msg        =>    'Bill Cycle '
                                                                   || l_billing_cycle
                                            );
                        END IF;
            
                        IF p_log_enabled IN ('y', 'Y')
                        THEN
                           xxwc_rental_engine_pkg.log_msg
                                   (p_debug_level      => g_level_error,
                                    p_mod_name         => l_module_name,
                                    p_debug_msg        =>    'Actual Billing Cycle is '
                                                          || l_billing_cycle
                                   );
                           xxwc_rental_engine_pkg.log_msg
                                           (p_debug_level      => g_level_error,
                                            p_mod_name         => l_module_name,
                                            p_debug_msg        =>    'Billing Days are '
                                                                  || l_billing_days
                                           );
                        END IF;
                     EXCEPTION
                        WHEN OTHERS
                        THEN
                        l_err_callpoint := 'STAGE13';
                           l_msg_data :=
                                  'ERROR: Calling Get Billing Cycle ' || SQLERRM;
                           xxwc_rental_engine_pkg.log_msg
                                                (p_debug_level      => g_level_error,
                                                 p_mod_name         => l_module_name,
                                                 p_debug_msg        => l_msg_data
                                                );
                     --RAISE SKIP_RECORD;
                     END;
            
                     /* If the RMA Line is Partially received , check if any of charge line has right billing attribute9*/
                     IF (get_rma_details_rec.ordered_quantity !=
                                      get_rental_line_info_rec.fulfilled_quantity
                        )
                     THEN
                        IF p_log_enabled IN ('y', 'Y')
                        THEN
                           xxwc_rental_engine_pkg.log_msg
                                             (p_debug_level      => g_level_error,
                                              p_mod_name         => l_module_name,
                                              p_debug_msg        => 'RMA Line is Split'
                                             );
                        END IF;
                        l_err_callpoint := 'STAGE14';
            
                        l_any_chr_bill_cycle :=
                           get_chr_line_attr9_split_rma
                                                (get_rental_line_info_rec.line_id);
            
                        IF l_bill = 'N'
                        THEN
                           IF l_any_chr_bill_cycle = l_billing_cycle
                           THEN
                              l_chr_line_exists := 'Y';
                           ELSE
                              l_chr_line_exists := 'N';
                           END IF;
                        END IF;
            
                        IF p_log_enabled IN ('y', 'Y')
                        THEN
                           xxwc_rental_engine_pkg.log_msg
                              (p_debug_level      => g_level_statement,
                               p_mod_name         => l_module_name,
                               p_debug_msg        =>    'Billing Cycle for Any Charge Line of the Main Line is '
                                                     || l_any_chr_bill_cycle
                              );
                        END IF;
                     ELSE
                     l_err_callpoint := 'STAGE15';
                        /* Check if Charge Line exists for the Corresponding Billing Cycle */
                        BEGIN
                           IF l_bill = 'N'
                           THEN
                              SELECT 'Y'
                                INTO l_chr_line_exists
                                FROM apps.oe_order_lines
                               WHERE header_id =
                                               get_rental_line_info_rec.header_id
                                 AND link_to_line_id =
                                                      get_rma_details_rec.line_id
                                 --lx_rma
                                 AND attribute9 = l_billing_cycle;
                           END IF;
            
                           IF p_log_enabled IN ('y', 'Y')
                           THEN
                              xxwc_rental_engine_pkg.log_msg
                                        (p_debug_level      => g_level_statement,
                                         p_mod_name         => l_module_name,
                                         p_debug_msg        =>    ' l_chr_line_exists '
                                                               || l_chr_line_exists
                                        );
                           END IF;
            
                           IF p_log_enabled IN ('y', 'Y')
                           THEN
                              xxwc_rental_engine_pkg.log_msg
                                 (p_debug_level      => g_level_statement,
                                  p_mod_name         => l_module_name,
                                  p_debug_msg        =>    ' Charge Line exists for Billing Cycle '
                                                        || l_billing_cycle
                                 );
                           END IF;
                        EXCEPTION
                           WHEN OTHERS
                           THEN
                           l_err_callpoint := 'STAGE16';
                              l_chr_line_exists := 'N';
                              xxwc_rental_engine_pkg.log_msg
                                 (p_debug_level      => g_level_statement,
                                  p_mod_name         => l_module_name,
                                  p_debug_msg        =>    ' Charge Line does Not exist for Billing Cycle '
                                                        || l_billing_cycle
                                 );
                        END;
            
                        IF p_log_enabled IN ('y', 'Y')
                        THEN
                           xxwc_rental_engine_pkg.log_msg
                                         (p_debug_level      => g_level_statement,
                                          p_mod_name         => l_module_name,
                                          p_debug_msg        =>    ' l_chr_line_exists'
                                                                || l_chr_line_exists
                                                                || ' , l_bill'
                                                                || l_bill
                                         );
                        END IF;
                     END IF;
            
                     /* If Charge line doesn't exist, create for the above billing Cycle */
                     IF (l_bill = 'N' AND l_chr_line_exists = 'N')
                     -- 10/17/2012 CG: added to not process for closed RMA lines
                       AND l_rma_status NOT IN ('RETURNED', 'CLOSED')                  
                     THEN
                        IF p_log_enabled IN ('y', 'Y')
                        THEN
                           xxwc_rental_engine_pkg.log_msg
                              (p_debug_level      => g_level_statement,
                               p_mod_name         => l_module_name,
                               p_debug_msg        =>    ' Creating Charge Line for Billing Cycle '
                                                     || l_billing_cycle
                              );
                        END IF;
                        l_err_callpoint := 'STAGE17';
            
                        create_charge_line
                                     (get_rental_line_info_rec.header_id,
                                      get_rental_line_info_rec.line_id,
                                      get_rental_line_info_rec.inventory_item_id,
                                      get_rma_details_rec.ordered_quantity, --Get_Rental_Line_Info_REC.shipped_quantity,
                                      l_billing_cycle,
                                      l_billing_cycle * 28,
                                      get_rental_line_info_rec.ship_from_org_id,
                                      get_rental_line_info_rec.attribute2,
                                      get_rental_line_info_rec.attribute4,
                                      get_rental_line_info_rec.unit_list_price,
                                      NULL,
                                      NULL,
                                      get_rma_details_rec.line_id,
                                      NULL                         --l_rma_status
                                          ,
                                      NULL                    --p_Apply_Hold_Flag
                                          ,
                                      lx_return_status,
                                      lx_charge_line_id
                                     );
            
                        IF lx_return_status = fnd_api.g_ret_sts_success
                        THEN
                        l_err_callpoint := 'STAGE18';
                           xxwc_rental_engine_pkg.log_msg
                              (p_debug_level      => g_level_procedure,
                               p_mod_name         => l_module_name,
                               p_debug_msg        => 'Charge Line Created Successfully'
                              );
                        ELSE
                        l_err_callpoint := 'STAGE19';
                           xxwc_rental_engine_pkg.log_msg
                              (p_debug_level      => g_level_error,
                               p_mod_name         => l_module_name,
                               p_debug_msg        => 'ERROR: Charge Line creation Errored OUT.'
                              );
                           xxwc_rental_engine_pkg.log_msg
                              (p_debug_level      => g_level_statement,
                               p_mod_name         => l_module_name,
                               p_debug_msg        => '###################################################'
                              );
                           lx_return_status := 'E';
                           RAISE api_failure;
                        END IF;
                     END IF;
            
                     /* IF RMA Line is Returned, Create Charge Line for Received Transaction*/
                     IF l_rma_status IN ('RETURNED', 'CLOSED')
                     -- 07/17/2012 CG
                     -- 07/24/2012 CG Removed new OR to return to prior status, not process unless receipt is complete
                     -- OR get_rma_details_rec.new_rental_date is not null
                     THEN
                        IF p_log_enabled IN ('y', 'Y')
                        THEN
                           xxwc_rental_engine_pkg.log_msg
                                (p_debug_level      => g_level_procedure,
                                 p_mod_name         => l_module_name,
                                 p_debug_msg        =>    'Processing RMA for Line ID '
                                                       || get_rma_details_rec.line_id
                                );
                        END IF;
                        l_err_callpoint := 'STAGE20';
            
                        -- 07/18/2012 CG: Added to ensure that all possible billing cycles based
                        -- on ship and return date have been created and it wont prorate all
                        -- remaining time. Keep billing charges to 28 days
                        l_curr_cycle := NULL;
                        l_total_cycles := NULL;
                        l_proc_prorated_charge := NULL;
            
                        IF     NVL (get_rma_details_rec.new_rental_date,
                                    get_rma_details_rec.actual_shipment_date
                                   ) IS NOT NULL
                           AND NVL (get_rental_line_info_rec.new_rental_date,
                                    get_rental_line_info_rec.actual_shipment_date
                                   ) IS NOT NULL
                        THEN
                           IF p_log_enabled IN ('y', 'Y')
                           THEN
                              xxwc_rental_engine_pkg.log_msg
                                 (p_debug_level      => g_level_procedure,
                                  p_mod_name         => l_module_name,
                                  p_debug_msg        =>    'Finding total cycles and max curr cycle related to RMA Line ID '
                                                        || get_rma_details_rec.line_id
                                 );
                           END IF;
                           
                           l_err_callpoint := 'STAGE21';
            
                           -- Total min cycles based on ship/return dates
                           BEGIN
                              SELECT FLOOR
                                        (ROUND
                                            ((  (  NVL
                                                      (get_rma_details_rec.new_rental_date,
                                                       get_rma_details_rec.actual_shipment_date
                                                      )
                                                 - NVL
                                                      (get_rental_line_info_rec.new_rental_date,
                                                       get_rental_line_info_rec.actual_shipment_date
                                                      )
                                                 + 1
                                                )
                                              / 28
                                             ),
                                             2
                                            )
                                        )
                                INTO l_total_cycles
                                FROM DUAL;
                                l_err_callpoint := 'STAGE22';
                           EXCEPTION
                              WHEN OTHERS
                              THEN
                              l_err_callpoint := 'STAGE23';
                                 IF p_log_enabled IN ('y', 'Y')
                                 THEN
                                    xxwc_rental_engine_pkg.log_msg
                                       (p_debug_level      => g_level_procedure,
                                        p_mod_name         => l_module_name,
                                        p_debug_msg        =>    'Could not determine total cycle associated to RMA Line ID'
                                                              || get_rma_details_rec.line_id
                                       );
                                 END IF;
            
                                 l_total_cycles := 0;
                           END;
            
                           -- Current Max/Cur cycle associated to RMA Line ID
                           BEGIN
                              -- 07/24/2012 CG: Corrected on 07/24/to be able to pull lines for split return lines
                              SELECT MAX (TO_NUMBER (chr_ln.attribute9))
                                INTO l_curr_cycle
                                FROM apps.oe_order_lines chr_ln,
                                     apps.oe_order_lines rma_ln
                               WHERE chr_ln.header_id =
                                               get_rental_line_info_rec.header_id
                                 --AND chr_ln.link_to_line_id = get_rma_details_rec.line_id
                                 AND chr_ln.link_to_line_id = rma_ln.line_id
                                 AND rma_ln.line_number =
                                                  get_rma_details_rec.line_number
                                 AND chr_ln.attribute9 IS NOT NULL;
                                 l_err_callpoint := 'STAGE24';
                           EXCEPTION
                              WHEN OTHERS
                              THEN
                              l_err_callpoint := 'STAGE25';
                                 IF p_log_enabled IN ('y', 'Y')
                                 THEN
                                    xxwc_rental_engine_pkg.log_msg
                                       (p_debug_level      => g_level_procedure,
                                        p_mod_name         => l_module_name,
                                        p_debug_msg        =>    'Could not determine total cycle associated to RMA Line ID'
                                                              || get_rma_details_rec.line_id
                                       );
                                 END IF;
            
                                 l_curr_cycle := -1;
                           END;
            
                           xxwc_rental_engine_pkg.log_msg
                              (p_debug_level      => g_level_procedure,
                               p_mod_name         => l_module_name,
                               p_debug_msg        =>    'Ship Date '
                                                     || NVL
                                                           (get_rental_line_info_rec.new_rental_date,
                                                            get_rental_line_info_rec.actual_shipment_date
                                                           )
                              );
                           xxwc_rental_engine_pkg.log_msg
                              (p_debug_level      => g_level_procedure,
                               p_mod_name         => l_module_name,
                               p_debug_msg        =>    'Return Date '
                                                     || NVL
                                                           (get_rma_details_rec.new_rental_date,
                                                            get_rma_details_rec.actual_shipment_date
                                                           )
                              );
                           xxwc_rental_engine_pkg.log_msg
                              (p_debug_level      => g_level_procedure,
                               p_mod_name         => l_module_name,
                               p_debug_msg        =>    'Total cycles based on Ship/Return Date '
                                                     || l_total_cycles
                              );
                           xxwc_rental_engine_pkg.log_msg
                              (p_debug_level      => g_level_procedure,
                               p_mod_name         => l_module_name,
                               p_debug_msg        =>    'Current Max cycles based on Ship/Return Date '
                                                     || l_curr_cycle
                              );
            
                           IF NVL (l_curr_cycle, -1) < NVL (l_total_cycles, 0)
                           THEN
                              l_proc_prorated_charge := 'N';
                           ELSE
                              l_proc_prorated_charge := 'Y';
                           END IF;
                        ELSE
                        l_err_callpoint := 'STAGE26';
                           IF p_log_enabled IN ('y', 'Y')
                           THEN
                              xxwc_rental_engine_pkg.log_msg
                                 (p_debug_level      => g_level_procedure,
                                  p_mod_name         => l_module_name,
                                  p_debug_msg        =>    'Shipment or Return date missing for RMA Line ID, should not be procssing prorated charge. Line ID '
                                                        || get_rma_details_rec.line_id
                                 );
                           END IF;
            
                           l_proc_prorated_charge := 'N';
                        END IF;
            
                        -- 07/18/2012 CG: Associated to above changes
                        IF NVL (l_proc_prorated_charge, 'N') = 'Y'
                        THEN
                           BEGIN
                           
                           l_err_callpoint := 'STAGE27';
                           --start TMS #20140815-00209
                           l_any_chr_bill_cycle :=
                           get_chr_line_attr9_split_rma
                                                (get_rental_line_info_rec.line_id);
                           --end TMS #20140815-00209
                           
                              fnd_file.put_line (fnd_file.LOG,
                                                    'l_any_chr_bill_cycle '
                                                 || l_any_chr_bill_cycle
                                                );
                              -- 07/12/2012 CG: Added to use new DFF for rental dates
                              l_tmp_rental_date := NULL;
                              l_tmp_rental_date :=
                                 NVL
                                    (get_rental_line_info_rec.new_rental_date,
                                     get_rental_line_info_rec.actual_shipment_date
                                    );
                              xxwc_rental_engine_pkg.log_msg
                                         (p_debug_level      => g_level_statement,
                                          p_mod_name         => l_module_name,
                                          p_debug_msg        =>    'Shipment Date(610) '
                                                                || l_tmp_rental_date
                                         );
                                l_err_callpoint := 'STAGE28';
                              get_return_subinventory
                                 (get_rental_line_info_rec.header_id,
                                  get_rma_details_rec.line_id,
                                  l_tmp_rental_date,
                                -- get_rental_line_info_rec.actual_shipment_date,
                                  l_any_chr_bill_cycle,
                                  l_return_subinv,
                                  l_return_date,
                                  l_rental_days,
                                  l_rcv_qty
                                 );
            
                              IF p_log_enabled IN ('y', 'Y')
                              THEN
                                 xxwc_rental_engine_pkg.log_msg
                                         (p_debug_level      => g_level_error,
                                          p_mod_name         => l_module_name,
                                          p_debug_msg        =>    'Return Date is '
                                                                || l_return_date
                                                                || ' with Rental Days '
                                                                || l_rental_days
                                         );
                              END IF;
                           EXCEPTION
                              WHEN OTHERS
                              THEN
                              l_err_callpoint := 'STAGE29';
                                 l_msg_data :=
                                       'ERROR: Calling Get Return SubInventory '
                                    || SQLERRM;
                                 xxwc_rental_engine_pkg.log_msg
                                                 (p_debug_level      => g_level_error,
                                                  p_mod_name         => l_module_name,
                                                  p_debug_msg        => l_msg_data
                                                 );
                           --RAISE SKIP_RECORD;
                           END;
            
                           l_billing_cycle := NULL;
            
                           BEGIN
                              SELECT 'Y'
                                INTO l_chr_line_exists
                                FROM apps.oe_order_lines
                               WHERE header_id =
                                               get_rental_line_info_rec.header_id
                                 AND link_to_line_id =
                                                      get_rma_details_rec.line_id
                                 --lx_rma
                                 AND attribute9 IS NULL;
                                 l_err_callpoint := 'STAGE30';
            
                              l_bill := 'Y';
            
                              IF p_log_enabled IN ('y', 'Y')
                              THEN
                                 xxwc_rental_engine_pkg.log_msg
                                    (p_debug_level      => g_level_statement,
                                     p_mod_name         => l_module_name,
                                     p_debug_msg        => ' Charge Line exists for returned item'
                                    );
                              END IF;
                           EXCEPTION
                              WHEN NO_DATA_FOUND
                              THEN
                              l_err_callpoint := 'STAGE31';
                                 l_chr_line_exists := 'N';
            
                                 IF     (   l_billing_days > 28
                                         OR (    l_billing_days < 28
                                             AND UPPER (l_return_subinv) IN
                                                    (g_lost_sub_inv,
                                                     g_dfr_sub_inv,
                                                     g_dbr_sub_inv,
                                                     -- 07/24/12 New subinve def
                                                     g_rent_sale_sub_inv
                                                    )
                                            )
                                         -- 07/17/2012 CG
                                         OR (get_rma_details_rec.new_rental_date IS NOT NULL
                                            )
                                        )
                                    -- 07/24/2012 CG: Changed to prevent negative
                                    --AND NVL (l_rental_days, 0) > 0-- 3.0 TMS #20140815-00209
                                    AND NVL (l_rental_days, 0) >= 0-- 3.0 TMS #20140815-00209
                                 THEN
                                    l_bill := 'N';
                                    l_err_callpoint := 'STAGE32';
                                 ELSE
                                 l_err_callpoint := 'STAGE33';
                                    l_bill := 'Y';
                                    -- RAISE NO_CHARGE_LINE_NEEDED;
                                    -- 07/10/2012 CG: Commented per Toby for now to test other option and issue resolution
                                    -- Zero Charge Lines are created :
                                    -- 29-JUN-2012 SatishU
                                    -- Create a Chargeline with zero price
                                    g_zero_unit_price_flag := 'Y';
                                    create_charge_line
                                       (get_rental_line_info_rec.header_id,
                                        get_rental_line_info_rec.line_id,
                                        get_rental_line_info_rec.inventory_item_id,
                                        get_rental_line_info_rec.shipped_quantity,
                                        NULL,                 -- l_billing_cycle,
                                        l_rental_days,
                                        get_rental_line_info_rec.ship_from_org_id,
                                        get_rental_line_info_rec.attribute2,
                                        '0',
                                          -- get_rental_line_info_rec.attribute4,
                                        get_rental_line_info_rec.unit_list_price,
                                        l_return_subinv,
                                        l_rcv_qty,
                                        get_rma_details_rec.line_id,
                                        l_rma_status,
                                        'Y',                  --p_Apply_Hold_Flag
                                        lx_return_status,
                                        lx_charge_line_id
                                       );
            
                                    IF lx_return_status =
                                                        fnd_api.g_ret_sts_success
                                    THEN
                                       IF p_log_enabled IN ('y', 'Y')
                                       THEN
                                          xxwc_rental_engine_pkg.log_msg
                                             (p_debug_level      => g_level_procedure,
                                              p_mod_name         => l_module_name,
                                              p_debug_msg        =>    'Zero Charge Line Created Successfully for RMA Status '
                                                                    || l_rma_status
                                             );
                                       END IF;
                                       l_err_callpoint := 'STAGE34';
                                    ELSE
                                       xxwc_rental_engine_pkg.log_msg
                                          (p_debug_level      => g_level_error,
                                           p_mod_name         => l_module_name,
                                           p_debug_msg        => 'ERROR: Charge Line creation Errored OUT.'
                                          );
                                          l_err_callpoint := 'STAGE35';
                                       lx_return_status := 'E';
                                       RAISE api_failure;
                                    END IF;
                                 END IF;
                              WHEN TOO_MANY_ROWS
                              THEN
                              l_err_callpoint := 'STAGE35';
                                 xxwc_rental_engine_pkg.log_msg
                                    (p_debug_level      => g_level_statement,
                                     p_mod_name         => l_module_name,
                                     p_debug_msg        =>    ' Duplicate Charge Lines exist for returned item '
                                                           || SQLERRM
                                    );
                              -- RAISE SKIP_RECORD;
                              WHEN OTHERS
                              THEN
                              l_err_callpoint := 'STAGE36';
                                 xxwc_rental_engine_pkg.log_msg
                                    (p_debug_level      => g_level_statement,
                                     p_mod_name         => l_module_name,
                                     p_debug_msg        =>    ' ERROR: finding Charge Lines '
                                                           || SQLERRM
                                    );
                                 l_error_code    := SQLCODE; --Ver 5.0
                                 l_error_message := ' ERROR: finding Charge Lines=> '|| SQLERRM; --Ver 5.0
                                 RAISE skip_record;
                           END;
            
                           IF p_log_enabled IN ('y', 'Y')
                           THEN
                              xxwc_rental_engine_pkg.log_msg
                                        (p_debug_level      => g_level_statement,
                                         p_mod_name         => l_module_name,
                                         p_debug_msg        =>    ' l_chr_line_exists '
                                                               || l_chr_line_exists
                                                               || '- l_bill '
                                                               || l_bill
                                        );
                           END IF;
            
                           IF (l_chr_line_exists = 'N' AND l_bill = 'N')
                           THEN
                           l_err_callpoint := 'STAGE37';
                              create_charge_line
                                 (get_rental_line_info_rec.header_id,
                                  get_rental_line_info_rec.line_id,
                                  get_rental_line_info_rec.inventory_item_id,
                                  -- 07/12/2012 CG: Changed to accomodate Rental Dates DFF Changes
                                  -- get_rental_line_info_rec.shipped_quantity,
                                  NVL (get_rental_line_info_rec.ordered_quantity,
                                       get_rental_line_info_rec.shipped_quantity
                                      ),
                                  l_billing_cycle,
                                  l_rental_days,
                                  get_rental_line_info_rec.ship_from_org_id,
                                  get_rental_line_info_rec.attribute2,
                                  get_rental_line_info_rec.attribute4,
                                  get_rental_line_info_rec.unit_list_price,
                                  l_return_subinv,
                                  l_rcv_qty,
                                  get_rma_details_rec.line_id,
                                  l_rma_status,
                                  
                                  -- 07/03/2012 CG changed to Y
                                  'Y'                         --p_Apply_Hold_Flag
                                     ,
                                  lx_return_status,
                                  lx_charge_line_id
                                 );
            
                              IF lx_return_status = fnd_api.g_ret_sts_success
                              THEN
                                 IF p_log_enabled IN ('y', 'Y')
                                 THEN
                                    xxwc_rental_engine_pkg.log_msg
                                       (p_debug_level      => g_level_procedure,
                                        p_mod_name         => l_module_name,
                                        p_debug_msg        =>    'Charge Line Created Successfully for RMA Status '
                                                              || l_rma_status
                                       );
                                 END IF;
                                 l_err_callpoint := 'STAGE38';
            
                                 l_serial_status :=
                                    is_item_serialized
                                       (get_rma_details_rec.line_id
                                                                   --Get_Rental_Line_Info_REC.line_id
                                    ,
                                        get_rental_line_info_rec.inventory_item_id,
                                        get_rental_line_info_rec.ship_from_org_id,
                                        g_rma_trxn_type,
                                        UPPER (l_return_subinv),
                                        g_rma_source_code
                                       );
                                 l_err_callpoint := 'STAGE39';
            
                                 IF     (UPPER (l_return_subinv) IN
                                            (g_lost_sub_inv,
                                             g_dbr_sub_inv,
                                             -- 07/24/2012 CG New Subinv definition
                                             g_rent_sale_sub_inv
                                            )
                                        )
                                    AND l_serial_status
                                 THEN
                                 l_err_callpoint := 'STAGE40';
                                    fnd_file.put_line
                                                   (fnd_file.LOG,
                                                    'Creating Reservations.... '
                                                   );
                                    l_reservation_record.v_reservation_interface_id :=
                                             mtl_reservations_interface_s.NEXTVAL;
                                    l_reservation_record.v_reservation_batch_id :=
                                                                               10;
                                    l_reservation_record.v_requirement_date :=
                                                                          SYSDATE;
                                    l_reservation_record.v_organization_id :=
                                        get_rental_line_info_rec.ship_from_org_id;
                                    l_reservation_record.v_inventory_item_id :=
                                       get_rental_line_info_rec.inventory_item_id;
                                    l_reservation_record.v_demand_source_type_id :=
                                                                                2;
                                    l_reservation_record.v_demand_source_header_id :=
                                       get_mtl_sales_order_id
                                              (get_rental_orders_rec.order_number);
                                    l_reservation_record.v_demand_source_line_id :=
                                                                lx_charge_line_id;
                                    -- Get_Rental_Line_Info_REC.line_id;
                                    l_reservation_record.v_reservation_uom_code :=
                                       get_rental_line_info_rec.order_quantity_uom;
                                    l_reservation_record.v_reservation_quantity :=
                                                                        l_rcv_qty;
                                    -- Get_Rental_Line_Info_REC.ordered_quantity;
                                    l_reservation_record.v_subinventory_code :=
                                                                  l_return_subinv;
                                    l_reservation_record.v_serial_number := NULL;
                                    l_reservation_record.v_supply_source_type_id :=
                                                                               13;
                                    l_reservation_record.v_row_status_code := 1;
                                    l_reservation_record.v_reservation_action_code :=
                                                                                1;
                                    l_reservation_record.v_transaction_mode := 3;
                                    l_reservation_record.v_last_update_date :=
                                                                          SYSDATE;
                                    l_reservation_record.v_last_updated_by :=
                                                                        g_user_id;
                                    l_reservation_record.v_creation_date :=
                                                                          SYSDATE;
                                    l_reservation_record.v_created_by :=
                                                                        g_user_id;
                                    create_reservation_line
                                       (l_reservation_record,
                                        get_rma_details_rec.line_id,
                                        get_rental_line_info_rec.inventory_item_id,
                                        UPPER (l_return_subinv),
                                        lx_return_status
                                       );
            
                                    IF lx_return_status =
                                                        fnd_api.g_ret_sts_success
                                    THEN
                                    l_err_callpoint := 'STAGE41';
                                       xxwc_rental_engine_pkg.log_msg
                                          (p_debug_level      => g_level_procedure,
                                           p_mod_name         => l_module_name,
                                           p_debug_msg        =>    'Reservation Line Created for Line ID '
                                                                 || get_rma_details_rec.line_id
                                          );
                                       xxwc_rental_engine_pkg.log_msg
                                          (p_debug_level      => g_level_statement,
                                           p_mod_name         => l_module_name,
                                           p_debug_msg        => '###################################################'
                                          );
                                    ELSE
                                    l_err_callpoint := 'STAGE42';
                                       xxwc_rental_engine_pkg.log_msg
                                          (p_debug_level      => g_level_error,
                                           p_mod_name         => l_module_name,
                                           p_debug_msg        => 'ERROR: Reservation Line Not created.'
                                          );
                                       xxwc_rental_engine_pkg.log_msg
                                          (p_debug_level      => g_level_statement,
                                           p_mod_name         => l_module_name,
                                           p_debug_msg        => '###################################################'
                                          );
                                    END IF;
                                 END IF;
            
                                 IF (    UPPER (l_return_subinv) IN
                                            (g_lost_sub_inv,
                                             g_dfr_sub_inv,
                                             g_dbr_sub_inv,
                                             -- 07/24/2012 CG New subinv definition
                                             g_rent_sale_sub_inv
                                            )
                                     AND l_billing_days > 28
                                    )
                                 THEN
                                 l_err_callpoint := 'STAGE43';
                                    create_charge_line
                                       (get_rental_line_info_rec.header_id,
                                        get_rental_line_info_rec.line_id,
                                        get_rental_line_info_rec.inventory_item_id,
                                        -- 07/12/2012 CG: Changed to accomodate Rental Dates DFF Changes
                                        -- get_rental_line_info_rec.shipped_quantity,
                                        NVL
                                           (get_rental_line_info_rec.ordered_quantity,
                                            get_rental_line_info_rec.shipped_quantity
                                           ),
                                        l_billing_cycle,
                                        l_rental_days,
                                        get_rental_line_info_rec.ship_from_org_id,
                                        get_rental_line_info_rec.attribute2,
                                        get_rental_line_info_rec.attribute4,
                                        get_rental_line_info_rec.unit_list_price,
                                        'Rental',
                                        l_rcv_qty,
                                        get_rma_details_rec.line_id,
                                        l_rma_status,
                                        'Y'                   --p_Apply_Hold_Flag
                                           ,
                                        lx_return_status,
                                        lx_charge_line_id
                                       );
            
                                    IF lx_return_status =
                                                        fnd_api.g_ret_sts_success
                                    THEN
                                    l_err_callpoint := 'STAGE44';
                                       xxwc_rental_engine_pkg.log_msg
                                          (p_debug_level      => g_level_procedure,
                                           p_mod_name         => l_module_name,
                                           p_debug_msg        =>    'Additional Charge Line created for  '
                                                                 || l_return_subinv
                                          );
                                       xxwc_rental_engine_pkg.log_msg
                                          (p_debug_level      => g_level_statement,
                                           p_mod_name         => l_module_name,
                                           p_debug_msg        => '###################################################'
                                          );
                                    ELSE
                                    l_err_callpoint := 'STAGE45';
                                       xxwc_rental_engine_pkg.log_msg
                                          (p_debug_level      => g_level_error,
                                           p_mod_name         => l_module_name,
                                           p_debug_msg        => 'ERROR: Additional Charge Line creation Errored OUT.'
                                          );
                                       xxwc_rental_engine_pkg.log_msg
                                          (p_debug_level      => g_level_statement,
                                           p_mod_name         => l_module_name,
                                           p_debug_msg        => '###################################################'
                                          );
                                       lx_return_status := 'E';
                                       RAISE api_failure;
                                    END IF;
                                 END IF;    /* End of 2nd Charge Line Creation */
                              ELSE
                              l_err_callpoint := 'STAGE46';
                                 xxwc_rental_engine_pkg.log_msg
                                    (p_debug_level      => g_level_error,
                                     p_mod_name         => l_module_name,
                                     p_debug_msg        => 'ERROR: Charge Line creation Errored OUT.'
                                    );
                                 lx_return_status := 'E';
                                 RAISE api_failure;
                              END IF;
                           END IF;  /* End of 1st Charge Line Creation */
                        END IF; /*    End if for check to create final/prorated charge */ -- 07/18/2012 CG:
                     END IF;  /* End of RMA Status Check */
                  END LOOP; /* End of RMA Line Loop */
               END IF;
            END LOOP; /* End of Order Line Loop */
         EXCEPTION  --Start Ver 5.0
            WHEN skip_record
            THEN
               log_order_error ( p_order_header_id   => l_order_header_id
                                ,p_order_line_id     => l_order_line_id
                                ,p_error_code        => l_error_code
                                ,p_error_message     => l_error_message
                                ,p_retcode           => l_retcode
                                ,p_retmsg            =>	l_retmsg 					  
                               );
               IF l_retcode IS NOT NULL THEN
                  l_retmsg := 'Error inside proc log_order_error=>'||l_retmsg;
                  xxwc_rental_engine_pkg.log_msg (p_debug_level      => g_level_statement,
                                                  p_mod_name         => l_module_name,
                                                  p_debug_msg        => l_retmsg
                                                 );
               END IF;
               retcode := '1';
            WHEN no_charge_line_needed --This error is nowhere raised hence log into error table is not handled here
            THEN
               l_debug_msg := ('*********No Charge Line Needs to be created *********');
               xxwc_rental_engine_pkg.log_msg (p_debug_level      => g_level_statement,
                                               p_mod_name         => l_module_name,
                                               p_debug_msg        => l_debug_msg
                                              );
               retcode := '1';
            WHEN api_failure -- Log into error table is handled at respective procedures hence not handled here
            THEN
               l_debug_msg := ('*********Please refer log table XXWC_RENTAL_ENGINE_ERROR_LOG to verify API error for order#'||get_rental_orders_rec.order_number||'*********');
               xxwc_rental_engine_pkg.log_msg (p_debug_level      => g_level_statement,
                                               p_mod_name         => l_module_name,
                                               p_debug_msg        => l_debug_msg
                                              );
               retcode := '1';
            WHEN NO_DATA_FOUND
            THEN
               l_error_code := SQLCODE;
               l_error_message := 'Error: No Data Found=>'||SQLERRM;
               l_debug_msg := ('*********Skipping Process of Order#'||get_rental_orders_rec.order_number||' due to no records*********');
               xxwc_rental_engine_pkg.log_msg (p_debug_level      => g_level_statement,
                                               p_mod_name         => l_module_name,
                                               p_debug_msg        => l_debug_msg
                                              );
               log_order_error ( p_order_header_id   => l_order_header_id
                                ,p_order_line_id     => l_order_line_id
                                ,p_error_code        => l_error_code
                                ,p_error_message     => l_error_message
                                ,p_retcode           => l_retcode
                                ,p_retmsg            =>	l_retmsg 					  
                               );
               IF l_retcode IS NOT NULL THEN
                  l_retmsg := 'Error inside proc log_order_error=>'||l_retmsg;
                  xxwc_rental_engine_pkg.log_msg (p_debug_level      => g_level_statement,
                                                  p_mod_name         => l_module_name,
                                                  p_debug_msg        => l_retmsg
                                                 );
               END IF;
               retcode := '1';
         END;  --End Ver 5.0
      END LOOP; /* End of Order Number Loop */
   EXCEPTION
      -- Comment Start for Ver 5.0
      /*WHEN skip_record
      THEN
         l_debug_msg :=
                 ('*********Skipping further Processing of Order *********');
         xxwc_rental_engine_pkg.log_msg (p_debug_level      => g_level_statement,
                                         p_mod_name         => l_module_name,
                                         p_debug_msg        => l_debug_msg
                                        );
         retcode := '1';
      WHEN no_charge_line_needed
      THEN
         l_debug_msg :=
                   ('*********No Charge Line Needs to be created *********');
         xxwc_rental_engine_pkg.log_msg (p_debug_level      => g_level_statement,
                                         p_mod_name         => l_module_name,
                                         p_debug_msg        => l_debug_msg
                                        );
         retcode := '1';
      WHEN api_failure
      THEN
         l_debug_msg :=
                   ('*********Please Verify API Failure Messages *********');
         xxwc_rental_engine_pkg.log_msg (p_debug_level      => g_level_statement,
                                         p_mod_name         => l_module_name,
                                         p_debug_msg        => l_debug_msg
                                        );
         retcode := '1';
      WHEN NO_DATA_FOUND
      THEN
         l_debug_msg :=
            ('*********Skipping further Processing of Order due to no records *********');
         xxwc_rental_engine_pkg.log_msg (p_debug_level      => g_level_statement,
                                         p_mod_name         => l_module_name,
                                         p_debug_msg        => l_debug_msg
                                        );
         retcode := '1';
      */ -- Comment End for Ver 5.0
      WHEN OTHERS
      THEN
         xxcus_error_pkg.xxcus_error_main_api (
         p_called_from         => l_err_callfrom,
         p_calling             => l_err_callpoint,
         p_request_id          => l_req_id,
         p_ora_error_msg       => SQLERRM,
         p_error_desc          => l_message,
         p_distribution_list   => l_distro_list,
         p_module              => 'XXWC');
         l_error_code := SQLCODE; --Ver 5.0
         l_debug_msg  := SQLERRM;
		 l_error_message := l_debug_msg; --Ver 5.0
         xxwc_rental_engine_pkg.log_msg (p_debug_level      => g_level_statement,
                                         p_mod_name         => l_module_name,
                                         p_debug_msg        => l_debug_msg
                                        );
         --Start Ver 5.0
         log_order_error ( p_order_header_id   => l_order_header_id
                          ,p_order_line_id     => l_order_line_id
                          ,p_error_code        => l_error_code
                          ,p_error_message     => l_error_message
                          ,p_retcode           => l_retcode
                          ,p_retmsg            => l_retmsg 					  
                         );
         IF l_retcode IS NOT NULL THEN
            l_retmsg := 'Error inside proc log_order_error=>'||l_retmsg;
            xxwc_rental_engine_pkg.log_msg (p_debug_level      => g_level_statement,
                                            p_mod_name         => l_module_name,
                                            p_debug_msg        => l_retmsg
                                           );
         END IF;
         retcode := '2';
         errbuf := SQLERRM;
         --End Ver 5.0
   END main;

   PROCEDURE create_rma_line (
      p_header_id            IN              NUMBER,
      p_rma_line_id          IN              NUMBER,
      p_unit_selling_price   IN              NUMBER,
      p_inventory_item_id    IN              NUMBER,
      p_rma_qty              IN              NUMBER,
      -- p_Log_Enabled        IN VARCHAR2,
      p_ship_from_org_id     IN              NUMBER,
      x_return_status        OUT NOCOPY      VARCHAR2
   )
   IS
      v_api_version_number           NUMBER                              := 1;
      v_return_status                VARCHAR2 (2000);
      v_msg_count                    NUMBER;
      v_msg_data                     VARCHAR2 (2000);
      l_api_name                     VARCHAR2 (30)       := 'CREATE_RMA_LINE';
      l_module_name                  VARCHAR2 (120);
      v_msg_index                    NUMBER;
      v_data                         VARCHAR2 (2000);
      v_loop_count                   NUMBER;
      v_debug_file                   VARCHAR2 (200);
      b_return_status                VARCHAR2 (200);
      b_msg_count                    NUMBER;
      b_msg_data                     VARCHAR2 (2000);
      l_result                       BOOLEAN;
      l_msg_data                     VARCHAR2 (2000);
      v_line_id                      NUMBER;
      k                              NUMBER                              := 1;
      -- IN VARIABLES
      v_header_rec                   oe_order_pub.header_rec_type;
      v_line_tbl                     oe_order_pub.line_tbl_type;
      v_action_request_tbl           oe_order_pub.request_tbl_type;
      v_line_adj_tbl                 oe_order_pub.line_adj_tbl_type;
      v_lot_serial_tbl               oe_order_pub.lot_serial_tbl_type;
-- OUT Variables --
      v_header_rec_out               oe_order_pub.header_rec_type;
      v_header_val_rec_out           oe_order_pub.header_val_rec_type;
      v_header_adj_tbl_out           oe_order_pub.header_adj_tbl_type;
      v_header_adj_val_tbl_out       oe_order_pub.header_adj_val_tbl_type;
      v_header_price_att_tbl_out     oe_order_pub.header_price_att_tbl_type;
      v_header_adj_att_tbl_out       oe_order_pub.header_adj_att_tbl_type;
      v_header_adj_assoc_tbl_out     oe_order_pub.header_adj_assoc_tbl_type;
      v_header_scredit_tbl_out       oe_order_pub.header_scredit_tbl_type;
      v_header_scredit_val_tbl_out   oe_order_pub.header_scredit_val_tbl_type;
      v_line_tbl_out                 oe_order_pub.line_tbl_type;
      v_line_val_tbl_out             oe_order_pub.line_val_tbl_type;
      v_line_adj_tbl_out             oe_order_pub.line_adj_tbl_type;
      v_line_adj_val_tbl_out         oe_order_pub.line_adj_val_tbl_type;
      v_line_price_att_tbl_out       oe_order_pub.line_price_att_tbl_type;
      v_line_adj_att_tbl_out         oe_order_pub.line_adj_att_tbl_type;
      v_line_adj_assoc_tbl_out       oe_order_pub.line_adj_assoc_tbl_type;
      v_line_scredit_tbl_out         oe_order_pub.line_scredit_tbl_type;
      v_line_scredit_val_tbl_out     oe_order_pub.line_scredit_val_tbl_type;
      v_lot_serial_tbl_out           oe_order_pub.lot_serial_tbl_type;
      v_lot_serial_val_tbl_out       oe_order_pub.lot_serial_val_tbl_type;
      v_action_request_tbl_out       oe_order_pub.request_tbl_type;

      CURSOR cur_get_serialized_info (
         p_lineid             NUMBER,
         p_inventoryitemid    NUMBER,
         p_ship_from_org_id   NUMBER
      )
      IS
         SELECT msn.serial_number,
                COUNT (*) OVER (PARTITION BY msn.last_transaction_id, msn.inventory_item_id)
                                                        total_serialized_cnt
           FROM mtl_serial_numbers msn,
                mtl_material_transactions mmt,
                mtl_transaction_types mtt
          WHERE mmt.transaction_id = msn.last_transaction_id
            AND mmt.inventory_item_id = p_inventoryitemid
            AND mmt.trx_source_line_id = p_lineid
            AND mmt.transaction_type_id = mtt.transaction_type_id
            AND UPPER (mtt.transaction_type_name) = g_mtl_transaction_type
            AND UPPER (mmt.subinventory_code) = g_mtl_subinventory_code
            AND UPPER (mmt.source_code) = g_mtl_source_code;

-- Version# 1.1 > Start
   l_est_ret_date           VARCHAR2(240);
   l_serial_num             VARCHAR2(240);
   l_rental_date            VARCHAR2(240);
   l_shipping_instructions  VARCHAR2(2000);
   l_packing_instructions   VARCHAR2(2000);
-- Version# 1.1 < End

      l_retcode                 VARCHAR2(200);  --Ver 5.0
	  l_retmsg                  VARCHAR2(3000); --Ver 5.0
   BEGIN
      l_module_name := g_module_name || '.' || l_api_name;
        --OE_MSG_PUB.initialize;
      -- Initialize Return Status Variable
      x_return_status := fnd_api.g_ret_sts_success;

       --mo_global.set_policy_context ('S', 161);
      -- mo_global.init('ONT');
      SELECT oe_order_lines_s.NEXTVAL
        INTO v_line_id
        FROM DUAL;

      v_action_request_tbl (1) := oe_order_pub.g_miss_request_rec;
      v_line_tbl (1) := oe_order_pub.g_miss_line_rec;
      v_line_tbl (1).operation := oe_globals.g_opr_create;
      v_line_tbl (1).header_id := p_header_id;
      v_line_tbl (1).inventory_item_id := p_inventory_item_id;
      v_line_tbl (1).ordered_quantity := p_rma_qty;
      v_line_tbl (1).unit_selling_price := 0;
      /* Unit Selling Price should always be Zero for RMA Line */
      v_line_tbl (1).calculate_price_flag := 'N';
      v_line_tbl (1).line_type_id := get_wc_trxn_id (g_ret_with_receipt);
      v_line_tbl (1).return_reason_code := g_return_reason_code;
      v_line_tbl (1).ship_from_org_id := p_ship_from_org_id;
      v_line_tbl (1).link_to_line_id := p_rma_line_id;
      v_line_tbl (1).return_attribute1 := p_header_id;
      v_line_tbl (1).return_attribute2 := p_rma_line_id;
      v_line_tbl (1).line_id := v_line_id;
      v_line_tbl (1).unit_list_price := 0;
      /* List Price should always be Zero for RMA Line */
      v_line_tbl (1).subinventory := 'Rental';

      IF is_item_serialized (p_rma_line_id,
                             p_inventory_item_id,
                             p_ship_from_org_id,
                             g_mtl_transaction_type,
                             g_mtl_subinventory_code,
                             g_mtl_source_code
                            )
      THEN
         FOR get_serialized_info_rec IN
            cur_get_serialized_info (p_rma_line_id,
                                     p_inventory_item_id,
                                     p_ship_from_org_id
                                    )
         LOOP
            v_lot_serial_tbl (k) := oe_order_pub.g_miss_lot_serial_rec;
            v_lot_serial_tbl (k).from_serial_number :=
                                        get_serialized_info_rec.serial_number;
            v_lot_serial_tbl (k).to_serial_number :=
                                        get_serialized_info_rec.serial_number;
            v_lot_serial_tbl (k).quantity := 1;
            v_lot_serial_tbl (k).line_id := v_line_id;
            v_lot_serial_tbl (k).operation := oe_globals.g_opr_create;
            k := k + 1;
         END LOOP;
      END IF;

      -- Version# 1.1 > Start
        BEGIN
        SELECT attribute1
             , attribute7
--             , attribute12 TMS #  20140107-00054 V1.3
             , shipping_instructions
             , packing_instructions
          INTO l_est_ret_date
             , l_serial_num
--             , l_rental_date TMS #  20140107-00054 V1.3
             , l_shipping_instructions
             , l_packing_instructions
          FROM apps.oe_order_lines
         WHERE line_id = p_rma_line_id;
         
--         fnd_file.put_line (fnd_file.LOG,'l_est_ret_date - ' || l_est_ret_date);
--         fnd_file.put_line (fnd_file.LOG,'l_serial_num - ' || l_serial_num);
--         fnd_file.put_line (fnd_file.LOG,'l_rental_date - ' || l_rental_date);
--         fnd_file.put_line (fnd_file.LOG,'l_shipping_instructions - ' || l_shipping_instructions);
--         fnd_file.put_line (fnd_file.LOG,'l_packing_instructions - ' || l_packing_instructions);
         
        EXCEPTION
        WHEN OTHERS THEN
         fnd_file.put_line (fnd_file.LOG,'Error deriving ShippingInstructions from Rental ItemLine ' || SQLERRM);
        END;


        v_line_tbl (1).attribute1             := l_est_ret_date;
        v_line_tbl (1).attribute7             := l_serial_num;
--        v_line_tbl (1).attribute12            := l_rental_date; TMS #  20140107-00054 V1.3
        v_line_tbl (1).shipping_instructions  := l_shipping_instructions;
        v_line_tbl (1).packing_instructions   := l_packing_instructions;
      -- Version# 1.1 < End

      oe_msg_pub.initialize;
      oe_order_pub.process_order
                    (p_api_version_number          => v_api_version_number,
                     p_init_msg_list               => fnd_api.g_true,
                     p_header_rec                  => v_header_rec,
                     p_line_tbl                    => v_line_tbl,
                     p_action_request_tbl          => v_action_request_tbl,
                     p_line_adj_tbl                => v_line_adj_tbl,
                     p_lot_serial_tbl              => v_lot_serial_tbl,
                     x_header_rec                  => v_header_rec_out,
                     x_header_val_rec              => v_header_val_rec_out,
                     x_header_adj_tbl              => v_header_adj_tbl_out,
                     x_header_adj_val_tbl          => v_header_adj_val_tbl_out,
                     x_header_price_att_tbl        => v_header_price_att_tbl_out,
                     x_header_adj_att_tbl          => v_header_adj_att_tbl_out,
                     x_header_adj_assoc_tbl        => v_header_adj_assoc_tbl_out,
                     x_header_scredit_tbl          => v_header_scredit_tbl_out,
                     x_header_scredit_val_tbl      => v_header_scredit_val_tbl_out,
                     x_line_tbl                    => v_line_tbl_out,
                     x_line_val_tbl                => v_line_val_tbl_out,
                     x_line_adj_tbl                => v_line_adj_tbl_out,
                     x_line_adj_val_tbl            => v_line_adj_val_tbl_out,
                     x_line_price_att_tbl          => v_line_price_att_tbl_out,
                     x_line_adj_att_tbl            => v_line_adj_att_tbl_out,
                     x_line_adj_assoc_tbl          => v_line_adj_assoc_tbl_out,
                     x_line_scredit_tbl            => v_line_scredit_tbl_out,
                     x_line_scredit_val_tbl        => v_line_scredit_val_tbl_out,
                     x_lot_serial_tbl              => v_lot_serial_tbl_out,
                     x_lot_serial_val_tbl          => v_lot_serial_val_tbl_out,
                     x_action_request_tbl          => v_action_request_tbl_out,
                     x_return_status               => v_return_status,
                     x_msg_count                   => v_msg_count,
                     x_msg_data                    => v_msg_data
                    );

      IF v_return_status = fnd_api.g_ret_sts_success
      THEN
         COMMIT;
         x_return_status := 'S';
         l_msg_data := 'RMA Line Addition to Existing Order Success :';
		 xxwc_rental_engine_pkg.log_msg (p_debug_level      => g_level_procedure,
                                         p_mod_name         => l_module_name,
                                         p_debug_msg        => l_msg_data
                                        );
      ELSE
         ROLLBACK;
         x_return_status := fnd_api.g_ret_sts_error;
         fnd_file.put_line (fnd_file.LOG, 'RMA Line Addition for Order Header Id '||p_header_id||' has failed');
         FOR i IN 1 .. v_msg_count
         LOOP
            v_msg_data := oe_msg_pub.get (p_msg_index      => i,
                                          p_encoded        => 'F');
            fnd_file.put_line (fnd_file.LOG, i || ') '||'Order_Header_Id:'||p_header_id||'=> ' || v_msg_data);
            /*Start Ver 5.0*/
			l_msg_data := 'RMA Line Addition to Existing Order failed:' || v_msg_data;
            log_order_error ( p_order_header_id   => p_header_id
                             ,p_order_line_id     => p_rma_line_id
                             ,p_error_code        => ''
                             ,p_error_message     => l_msg_data
                             ,p_retcode           => l_retcode
                             ,p_retmsg            => l_retmsg 					  
                            );
            IF l_retcode IS NOT NULL THEN
               l_retmsg := substr('Error inside proc log_order_error=>'||l_retmsg,1,3000);
               xxwc_rental_engine_pkg.log_msg (p_debug_level      => g_level_procedure,
                                               p_mod_name         => l_module_name,
                                               p_debug_msg        => l_retmsg
                                              );
            END IF;
            /*End Ver 5.0*/
         END LOOP;

         /*l_msg_data :=
                   'RMA Line Addition to Existing Order failed:' || v_msg_data;
         xxwc_rental_engine_pkg.log_msg (p_debug_level      => g_level_procedure,
                                         p_mod_name         => l_module_name,
                                         p_debug_msg        => l_msg_data
                                        );*/
         
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         x_return_status := fnd_api.g_ret_sts_error;
         l_msg_data := 'ERROR: Creating RMA LINE:' || SQLERRM;
         xxwc_rental_engine_pkg.log_msg (p_debug_level      => g_level_procedure,
                                         p_mod_name         => l_module_name,
                                         p_debug_msg        => l_msg_data
                                        );
         /*Start Ver 5.0*/
         log_order_error ( p_order_header_id   => p_header_id
                          ,p_order_line_id     => p_rma_line_id
                          ,p_error_code        => ''
                          ,p_error_message     => l_msg_data
                          ,p_retcode           => l_retcode
                          ,p_retmsg            => l_retmsg 					  
                         );
         IF l_retcode IS NOT NULL THEN
            l_retmsg := substr('Error inside proc log_order_error=>'||l_retmsg,1,3000);
            xxwc_rental_engine_pkg.log_msg (p_debug_level      => g_level_statement,
                                            p_mod_name         => l_module_name,
                                            p_debug_msg        => l_retmsg
                                           );
         END IF;
         /*End Ver 5.0*/
   END create_rma_line;

   PROCEDURE create_charge_line (
      p_header_id                IN              NUMBER,
      p_charge_line_id           IN              NUMBER,
      p_inventory_item_id        IN              NUMBER,
      p_qty                      IN              NUMBER,
      p_bill_cycle               IN              NUMBER,
      p_billing_days             IN              NUMBER,
      p_ship_from_org_id         IN              NUMBER,
      p_rental_billing_term      IN              VARCHAR2,
      p_override_rental_charge   IN              NUMBER,
      p_unit_list_price          IN              NUMBER,
      p_return_subinv            IN              VARCHAR2,
      p_rcv_quantity             IN              NUMBER,
      p_rma_line_id              IN              NUMBER,
      p_rma_status               IN              VARCHAR2,
      p_apply_hold_flag          IN              VARCHAR2,
      lx_return_status           OUT NOCOPY      VARCHAR2,
      lx_charge_line_id          OUT NOCOPY      NUMBER
   )
   IS
      v_api_version_number           NUMBER                              := 1;
      v_return_status                VARCHAR2 (2000);
      v_msg_count                    NUMBER;
      v_msg_data                     VARCHAR2 (2000);
      l_api_name                     VARCHAR2 (30)    := 'CREATE_CHARGE_LINE';
      l_module_name                  VARCHAR2 (120);
      v_msg_index                    NUMBER;
      v_data                         VARCHAR2 (2000);
      v_loop_count                   NUMBER;
      v_debug_file                   VARCHAR2 (200);
      b_return_status                VARCHAR2 (200);
      b_msg_count                    NUMBER;
      b_msg_data                     VARCHAR2 (2000);
      l_charge_item                  NUMBER;
      l_unit_price                   NUMBER;
      l_rma_qty                      NUMBER;
      l_rma_line_id                  NUMBER;
      l_trans_type                   NUMBER;
      l_flow_status_code             VARCHAR2 (50);
      l_last_bill                    NUMBER;
      l_sub_inv                      VARCHAR2 (20);
      l_rental_days                  NUMBER;
      l_return_date                  DATE;
      l_rental_billing_terms         VARCHAR2 (50);
      l_avg_cost                     NUMBER;
      l_hold_return_status           VARCHAR2 (20);
      l_item_description             VARCHAR2 (50);
      -- IN Variables --
      v_header_rec                   oe_order_pub.header_rec_type;
      v_line_tbl                     oe_order_pub.line_tbl_type;
      v_action_request_tbl           oe_order_pub.request_tbl_type;
      v_line_adj_tbl                 oe_order_pub.line_adj_tbl_type;
      -- OUT Variables --
      v_header_rec_out               oe_order_pub.header_rec_type;
      v_header_val_rec_out           oe_order_pub.header_val_rec_type;
      v_header_adj_tbl_out           oe_order_pub.header_adj_tbl_type;
      v_header_adj_val_tbl_out       oe_order_pub.header_adj_val_tbl_type;
      v_header_price_att_tbl_out     oe_order_pub.header_price_att_tbl_type;
      v_header_adj_att_tbl_out       oe_order_pub.header_adj_att_tbl_type;
      v_header_adj_assoc_tbl_out     oe_order_pub.header_adj_assoc_tbl_type;
      v_header_scredit_tbl_out       oe_order_pub.header_scredit_tbl_type;
      v_header_scredit_val_tbl_out   oe_order_pub.header_scredit_val_tbl_type;
      v_line_tbl_out                 oe_order_pub.line_tbl_type;
      v_line_val_tbl_out             oe_order_pub.line_val_tbl_type;
      v_line_adj_tbl_out             oe_order_pub.line_adj_tbl_type;
      v_line_adj_val_tbl_out         oe_order_pub.line_adj_val_tbl_type;
      v_line_price_att_tbl_out       oe_order_pub.line_price_att_tbl_type;
      v_line_adj_att_tbl_out         oe_order_pub.line_adj_att_tbl_type;
      v_line_adj_assoc_tbl_out       oe_order_pub.line_adj_assoc_tbl_type;
      v_line_scredit_tbl_out         oe_order_pub.line_scredit_tbl_type;
      v_line_scredit_val_tbl_out     oe_order_pub.line_scredit_val_tbl_type;
      v_lot_serial_tbl_out           oe_order_pub.lot_serial_tbl_type;
      v_lot_serial_val_tbl_out       oe_order_pub.lot_serial_val_tbl_type;
      v_action_request_tbl_out       oe_order_pub.request_tbl_type;

   -- Version# 1.1 > Start
      l_est_ret_date           VARCHAR2(240);
      l_serial_num             VARCHAR2(240);
      l_rental_date            VARCHAR2(240);
      l_shipping_instructions  VARCHAR2(2000);
      l_packing_instructions   VARCHAR2(2000);
   -- Version# 1.1 < End

      l_retcode                 VARCHAR2(200);  --Ver 5.0
	  l_retmsg                  VARCHAR2(3000); --Ver 5.0

   BEGIN
      l_module_name := g_module_name || '.' || l_api_name;
      lx_return_status := fnd_api.g_ret_sts_success;
      xxwc_rental_engine_pkg.log_msg
                            (p_debug_level      => g_level_procedure,
                             p_mod_name         => l_module_name,
                             p_debug_msg        => 'Beginning of Create Charge line'
                            );
      v_action_request_tbl (1) := oe_order_pub.g_miss_request_rec;
      /* Get Item Description */
      l_item_description := get_item_description (p_charge_line_id);
      /* Get ChargeItem for the Inventory Item*/
      l_charge_item :=
                     get_charge_item (p_inventory_item_id, p_ship_from_org_id);

      /* Get Unit Price for the Above Charge Item*/
      IF p_override_rental_charge IS NULL
      THEN
         l_unit_price :=
            get_charge_item_unit_price (p_inventory_item_id, --l_charge_item,
                                        p_rma_line_id);
      ELSE
         l_unit_price := p_override_rental_charge;
      END IF;

      fnd_file.put_line (fnd_file.LOG,
                         'Unit price before calling RMA Qty ' || l_unit_price
                        );

      /*  Get RMA Qty */
      IF p_rcv_quantity IS NULL
      THEN
         l_rma_qty := p_qty;  --GET_RMA_QTY (p_header_id, p_charge_line_id );
      ELSE
         l_rma_qty := p_rcv_quantity;
      END IF;

      --IF l_flow_status_code IN ('RETURNED','CLOSED') THEN
      IF     (   p_rma_status IN ('RETURNED', 'CLOSED')
              OR p_rcv_quantity IS NOT NULL
             )
         AND g_zero_unit_price_flag = 'N'
      THEN
         l_unit_price :=
            get_return_unit_price (p_header_id             => p_header_id,
                                   p_line_id               => p_rma_line_id,
                                   --l_rma_line_id,
                                   p_billing_terms         => p_rental_billing_term,
                                   p_charge_item           => l_charge_item,
                                   p_rental_item           => p_inventory_item_id,
                                   p_unit_price            => l_unit_price,
                                   p_ship_from_org_id      => p_ship_from_org_id,
                                   p_rentaldays            => p_billing_days,
                                   p_return_subinv         => p_return_subinv
                                  );
         fnd_file.put_line
                         (fnd_file.LOG,
                             'Unit price after calling Get Return Unit price '
                          || l_unit_price
                         );

         IF p_rental_billing_term = '28 Day ReRental Billing'
         THEN
            -- 02/06/2013 CG: Changed to the same as regular charge lines per Toby, it shouldn't create a credit line but a bill line
            -- using TMS 20130205-01491
            -- l_trans_type := get_wc_trxn_id (g_oel_credit_only);
            l_trans_type := get_wc_trxn_id (g_oel_bill_only);
         ELSE
            l_trans_type := get_wc_trxn_id (g_oel_bill_only);
         END IF;
      ELSE
         l_trans_type := get_wc_trxn_id (g_oel_bill_only);
      END IF;

      -- Line Record --
      v_line_tbl (1) := oe_order_pub.g_miss_line_rec;
      v_line_tbl (1).operation := oe_globals.g_opr_create;
      v_line_tbl (1).header_id := p_header_id;
      v_line_tbl (1).inventory_item_id := l_charge_item;
      v_line_tbl (1).ordered_quantity := l_rma_qty;

      IF g_zero_unit_price_flag = 'Y'
      THEN
         v_line_tbl (1).unit_selling_price := 0;
         -- Reset the Flag after assinging zero for current Line
         g_zero_unit_price_flag := 'N';
      ELSE
         v_line_tbl (1).unit_selling_price := l_unit_price;
      END IF;

      v_line_tbl (1).calculate_price_flag := 'N';
      v_line_tbl (1).ship_from_org_id := p_ship_from_org_id;
      v_line_tbl (1).link_to_line_id := p_rma_line_id;        --l_rma_line_id;
      v_line_tbl (1).line_type_id := l_trans_type;
      v_line_tbl (1).attribute9 := p_bill_cycle;
      v_line_tbl (1).unit_list_price := l_unit_price;

      /* List Price = Unit Selling Price for Charge Lines */
      IF UPPER (p_return_subinv) = g_lost_sub_inv
      THEN
         v_line_tbl (1).user_item_description :=
                                'Lost Charge for Item ' || l_item_description;
      ELSIF UPPER (p_return_subinv) = g_dfr_sub_inv
      THEN
         v_line_tbl (1).user_item_description :=
                   'Charge for Damaged But Repairable ' || l_item_description;
      ELSIF UPPER (p_return_subinv) = g_dbr_sub_inv
      THEN
         v_line_tbl (1).user_item_description :=
                'Charge for Damaged Beyond Repairable ' || l_item_description;
      -- 07/24/20121 CG: Added for new subinv definition
      ELSIF UPPER (p_return_subinv) = g_rent_sale_sub_inv
      THEN
         v_line_tbl (1).user_item_description :=
                                 'Sale of Rental Item ' || l_item_description;
      ELSE
         v_line_tbl (1).user_item_description :=
                              'Rental Charge for Item ' || l_item_description;
      END IF;

      IF UPPER (p_return_subinv) NOT IN
            (g_lost_sub_inv, g_dfr_sub_inv, g_dbr_sub_inv,
             
             -- 07/24/2012 CG: Added for new subinv definition
             g_rent_sale_sub_inv)
      THEN
         v_line_tbl (1).subinventory := 'Rental';        -- G_RENTAL_SUB_INV;
      ELSE
         v_line_tbl (1).subinventory := p_return_subinv;
      END IF;

      IF UPPER (p_return_subinv) IN
                         (g_lost_sub_inv, g_dbr_sub_inv,
                                                        -- 07/24/2012 CG: Added for new subinv
                                                        g_rent_sale_sub_inv)
      THEN
         v_line_tbl (1).inventory_item_id := p_inventory_item_id;
         v_line_tbl (1).line_type_id := get_wc_trxn_id (g_oel_wc_bill_only);
         v_line_tbl (1).schedule_ship_date := SYSDATE;
      END IF;

      -- 02/05/2013 CG TMS: 20130205-01491
      -- Resolve issue with changing price on a rental line
      /*IF p_rental_billing_term = '28 Day ReRental Billing'
      THEN
         v_line_tbl (1).return_reason_code := g_return_reason_code;
         v_line_tbl (1).return_attribute1 := p_header_id;
         v_line_tbl (1).return_attribute2 := p_charge_line_id;
      END IF;*/

      -- Version# 1.1 > Start
        fnd_file.put_line (fnd_file.LOG,'p_charge_line_id - ' || p_charge_line_id);
        BEGIN
        SELECT attribute1
             , attribute7
--             , attribute12 TMS #  20140107-00054 V1.3
             , shipping_instructions
             , packing_instructions
          INTO l_est_ret_date
             , l_serial_num
--             , l_rental_date TMS #  20140107-00054 V1.3
             , l_shipping_instructions
             , l_packing_instructions
          FROM apps.oe_order_lines
         WHERE line_id = p_charge_line_id;
         
--         fnd_file.put_line (fnd_file.LOG,'l_est_ret_date - ' || l_est_ret_date);
--         fnd_file.put_line (fnd_file.LOG,'l_serial_num - ' || l_serial_num);
--         fnd_file.put_line (fnd_file.LOG,'l_rental_date - ' || l_rental_date);
--         fnd_file.put_line (fnd_file.LOG,'l_shipping_instructions - ' || l_shipping_instructions);
--         fnd_file.put_line (fnd_file.LOG,'l_packing_instructions - ' || l_packing_instructions);
         
        EXCEPTION
        WHEN OTHERS THEN
         fnd_file.put_line (fnd_file.LOG,'Error deriving ShippingInstructions from ItemLine - ' || SQLERRM);
        END;


        v_line_tbl (1).attribute1             := l_est_ret_date;
        v_line_tbl (1).attribute7             := l_serial_num;
--        v_line_tbl (1).attribute12            := l_rental_date; TMS #  20140107-00054 V1.3
        v_line_tbl (1).shipping_instructions  := l_shipping_instructions;
        v_line_tbl (1).packing_instructions   := l_packing_instructions;
      -- Version# 1.1 < End

      -- Calling the API to add a new line to an existing Order --
      oe_order_pub.process_order
                    (p_api_version_number          => v_api_version_number,
                     p_header_rec                  => v_header_rec,
                     p_line_tbl                    => v_line_tbl,
                     p_action_request_tbl          => v_action_request_tbl,
                     p_line_adj_tbl                => v_line_adj_tbl,
                     -- OUT variables
                     x_header_rec                  => v_header_rec_out,
                     x_header_val_rec              => v_header_val_rec_out,
                     x_header_adj_tbl              => v_header_adj_tbl_out,
                     x_header_adj_val_tbl          => v_header_adj_val_tbl_out,
                     x_header_price_att_tbl        => v_header_price_att_tbl_out,
                     x_header_adj_att_tbl          => v_header_adj_att_tbl_out,
                     x_header_adj_assoc_tbl        => v_header_adj_assoc_tbl_out,
                     x_header_scredit_tbl          => v_header_scredit_tbl_out,
                     x_header_scredit_val_tbl      => v_header_scredit_val_tbl_out,
                     x_line_tbl                    => v_line_tbl_out,
                     x_line_val_tbl                => v_line_val_tbl_out,
                     x_line_adj_tbl                => v_line_adj_tbl_out,
                     x_line_adj_val_tbl            => v_line_adj_val_tbl_out,
                     x_line_price_att_tbl          => v_line_price_att_tbl_out,
                     x_line_adj_att_tbl            => v_line_adj_att_tbl_out,
                     x_line_adj_assoc_tbl          => v_line_adj_assoc_tbl_out,
                     x_line_scredit_tbl            => v_line_scredit_tbl_out,
                     x_line_scredit_val_tbl        => v_line_scredit_val_tbl_out,
                     x_lot_serial_tbl              => v_lot_serial_tbl_out,
                     x_lot_serial_val_tbl          => v_lot_serial_val_tbl_out,
                     x_action_request_tbl          => v_action_request_tbl_out,
                     x_return_status               => v_return_status,
                     x_msg_count                   => v_msg_count,
                     x_msg_data                    => v_msg_data
                    );

      IF v_return_status = fnd_api.g_ret_sts_success
      THEN
         COMMIT;
         lx_return_status := 'S';
         lx_charge_line_id := v_line_tbl_out (1).line_id;
         fnd_file.put_line (fnd_file.LOG,
                            'Charge Line Id is ' || lx_charge_line_id
                           );
         xxwc_rental_engine_pkg.log_msg
              (p_debug_level      => g_level_procedure,
               p_mod_name         => l_module_name,
               p_debug_msg        => 'Charge Line Addition to Existing Order Success'
              );

         -- 07/03/2012 CGonzalez: Modified to only look at the apply hold flag
         /*IF (   UPPER (p_return_subinv) NOT IN
                                      (g_rental_sub_inv, g_rental_nr_sub_inv)
             OR (    UPPER (p_return_subinv) IN
                                      (g_rental_sub_inv, g_rental_nr_sub_inv)
                 AND p_apply_hold_flag = 'Y'
                )
            )
         THEN*/
         IF NVL (p_apply_hold_flag, 'N') = 'Y'
         THEN
            apply_hold_charge_line (p_header_id,
                                    v_line_tbl_out (1).line_id,
                                    l_hold_return_status
                                   );
         END IF;
      ELSE
         ROLLBACK;
         lx_return_status := 'E';
         fnd_file.put_line (fnd_file.LOG, 'Charge Line Addition to Existing Order header id '||p_header_id||' failed');
         FOR i IN 1 .. v_msg_count
         LOOP
            v_msg_data := oe_msg_pub.get (p_msg_index      => i,
                                          p_encoded        => 'F');
			
            fnd_file.put_line (fnd_file.LOG, i || ') '||'Order_Header_Id:'||p_header_id||'=> ' || v_msg_data);
			/*Start Ver 5.0*/
            b_msg_data := 'Charge Line Addition to Existing Order failed:' || v_msg_data;
            log_order_error ( p_order_header_id   => p_header_id
                             ,p_order_line_id     => p_charge_line_id
                             ,p_error_code        => ''
                             ,p_error_message     => b_msg_data
                             ,p_retcode           => l_retcode
                             ,p_retmsg            => l_retmsg 					  
                            );
            IF l_retcode IS NOT NULL THEN
               l_retmsg := substr('Error inside proc log_order_error=>'||l_retmsg,1,3000);
               xxwc_rental_engine_pkg.log_msg (p_debug_level      => g_level_procedure,
                                               p_mod_name         => l_module_name,
                                               p_debug_msg        => l_retmsg
                                              );
            END IF;
            /*End Ver 5.0*/
         END LOOP;

         /*b_msg_data :=
                'Charge Line Addition to Existing Order failed:' || v_msg_data;
         xxwc_rental_engine_pkg.log_msg (p_debug_level      => g_level_procedure,
                                         p_mod_name         => l_module_name,
                                         p_debug_msg        => b_msg_data
                                        );*/
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         lx_return_status := fnd_api.g_ret_sts_error;
         b_msg_data := 'When Others Error :' || SQLERRM;
         xxwc_rental_engine_pkg.log_msg (p_debug_level      => g_level_procedure,
                                         p_mod_name         => l_module_name,
                                         p_debug_msg        => b_msg_data
                                        );
         /*Start Ver 5.0*/
         log_order_error ( p_order_header_id   => p_header_id
                          ,p_order_line_id     => p_charge_line_id
                          ,p_error_code        => ''
                          ,p_error_message     => b_msg_data
                          ,p_retcode           => l_retcode
                          ,p_retmsg            => l_retmsg 					  
                         );
         IF l_retcode IS NOT NULL THEN
            l_retmsg := substr('Error inside proc log_order_error=>'||l_retmsg,1,3000);
            xxwc_rental_engine_pkg.log_msg (p_debug_level      => g_level_procedure,
                                            p_mod_name         => l_module_name,
                                            p_debug_msg        => l_retmsg
                                           );
         END IF;
         /*End Ver 5.0*/
   END create_charge_line;

/*************************************************************************
   *   Procedure : CHECK_RMA_EXISTS
   *
   *   PURPOSE:   This Procedure Checks for RMA Lines
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        09/29/2011  Srinivas Malkapuram             Initial Version
   * ************************************************************************/
   PROCEDURE get_rma_details (
      p_header_id     IN              NUMBER,
      p_line_id       IN              NUMBER,
      p_log_enabled   IN              VARCHAR2,
      x_rma           OUT NOCOPY      NUMBER,
      x_rma_status    OUT NOCOPY      VARCHAR2
   )
   IS
      l_api_name      VARCHAR2 (30)   := 'CHECK_RMA_EXISTS';
      l_module_name   VARCHAR2 (120);
      l_msg_data      VARCHAR2 (2000);
   BEGIN
      l_module_name := g_module_name || '.' || l_api_name;
      -- Initialize Return Status Variable
      x_rma := NULL;
      x_rma_status := NULL;

      IF p_log_enabled IN ('y', 'Y')
      THEN
         xxwc_rental_engine_pkg.log_msg
                      (p_debug_level      => g_level_procedure,
                       p_mod_name         => l_module_name,
                       p_debug_msg        =>    'Checking RMA Exists for HeaderID :'
                                             || p_header_id
                                             || ' and LineID : '
                                             || p_line_id
                      );
      END IF;

      SELECT line_id, flow_status_code
        INTO x_rma, x_rma_status
        FROM apps.oe_order_lines
		WHERE header_id = p_header_id
         AND return_attribute1 = p_header_id
         AND return_attribute2 = p_line_id
         AND line_category_code = 'RETURN'
         AND flow_status_code <> 'CANCELLED';

      IF p_log_enabled IN ('y', 'Y')
      THEN
         xxwc_rental_engine_pkg.log_msg
                                (p_debug_level      => g_level_procedure,
                                 p_mod_name         => l_module_name,
                                 p_debug_msg        =>    'RMA Exists with Line id '
                                                       || x_rma
                                                       || ' and status '
                                                       || x_rma_status
                                );
      END IF;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         x_rma := NULL;
         x_rma_status := 'NO RMA';
         l_msg_data := 'RMA line Does not exists';
         xxwc_rental_engine_pkg.log_msg (p_debug_level      => g_level_error,
                                         p_mod_name         => l_module_name,
                                         p_debug_msg        => l_msg_data
                                        );
      WHEN TOO_MANY_ROWS
      THEN
         x_rma := NULL;
         x_rma_status := 'Duplicate RMA Lines';
         l_msg_data := 'Duplicate RMA Lines';
         xxwc_rental_engine_pkg.log_msg (p_debug_level      => g_level_error,
                                         p_mod_name         => l_module_name,
                                         p_debug_msg        => l_msg_data
                                        );
      WHEN OTHERS
      THEN
         x_rma := NULL;
         x_rma_status := NULL;
         l_msg_data := 'ERROR in Retrieving RMA Line Details';
         xxwc_rental_engine_pkg.log_msg (p_debug_level      => g_level_error,
                                         p_mod_name         => l_module_name,
                                         p_debug_msg        => l_msg_data
                                        );
   END get_rma_details;

/*************************************************************************
   *   Procedure : GET_BILLING_CYCLE
   *
   *   PURPOSE:   This Procedure Returns the Billing Cycle, Billing Days and Bill Count
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        09/29/2011  Srinivas Malkapuram             Initial Version
   *   2.0         09/18/2013   Ram Talluri               dded by Ram Talluri TMS 20130918-00610 9/18/2013
   * ************************************************************************/
   PROCEDURE get_billing_cycle (
      p_actual_shipment_date   IN              DATE,
      p_line_id                IN              NUMBER,
      p_rma_status             IN              VARCHAR2,
      p_rental_terms           IN              VARCHAR2,
      x_billing_cycle          OUT             NUMBER,
      x_billing_days           OUT NOCOPY      NUMBER,
      x_bill                   OUT NOCOPY      VARCHAR2
   )
   IS
      l_api_name         VARCHAR2 (30)   := 'GET_BILLING_CYCLE';
      l_module_name      VARCHAR2 (120);
      l_msg_data         VARCHAR2 (2000);
      l_max_bill_cycle   NUMBER;
   BEGIN
      l_module_name := g_module_name || '.' || l_api_name;

      -- 03/30/2012 CGonzalez: added + 1 to include current day
      SELECT TRUNC (SYSDATE) - TRUNC (p_actual_shipment_date) + 1
        INTO x_billing_days
        FROM DUAL;

      x_bill := 'N';

      -- 03/30/2012 CGonzalez: added + 1 to include current day
      -- 05/16/2012 CGonzalez: Commented/changed to calcualte based on line rental term
      /*SELECT FLOOR (ROUND ((TRUNC(SYSDATE) - TRUNC(p_actual_shipment_date) + 1) / 28, 2)) --+ 1
      INTO x_billing_cycle
      FROM DUAL;*/
      IF NVL (p_rental_terms, 'Unknown') = '28 Day ReRental Billing'
      THEN
         -- 28 Days ReRental bills first cycle in advance
         -- then bills every 28 days, at the beginning of every cycle
         SELECT CEIL (ROUND (  (  TRUNC (SYSDATE)
                                - TRUNC (p_actual_shipment_date)
                                + 1
                               )
                             / 28,
                             2
                            )
                     )
           INTO x_billing_cycle
           FROM DUAL;
      ELSE
         -- Non 28 Days ReRental bills the first cycle in advance
         -- then bills at the end of each cycle after that
         SELECT FLOOR (ROUND (  (  TRUNC (SYSDATE)
                                 - TRUNC (p_actual_shipment_date)
                                 + 1
                                )
                              / 28,
                              2
                             )
                      )                                           -- - 1 --+ 1
           INTO x_billing_cycle
           FROM DUAL;
      END IF;

      IF x_billing_cycle <= 0
      THEN
         x_billing_cycle := 1;
      END IF;

      -- 05/16/2012 CGonzalez

      -- 04/02/2012 CG
      xxwc_rental_engine_pkg.log_msg (p_debug_level      => g_level_statement,
                                      p_mod_name         => l_module_name,
                                      p_debug_msg        =>    'In '
                                                            || l_module_name
                                                            || ' - Bill Cycle '
                                                            || x_billing_cycle
                                     );

      BEGIN
         -- 05/23/2013 CG: TMS 20130523-01071: Changed to make it to_number...risk, there are lines out there with garbage/non-numeric values in the field
         -- it may cause the program to act up
         SELECT MAX (TO_NUMBER(oel3.attribute9))
           INTO l_max_bill_cycle
           FROM apps.oe_order_lines oel1,
                apps.oe_order_lines oel2,
                apps.oe_order_lines oel3
          WHERE oel1.line_id = oel2.link_to_line_id
            AND oel2.line_id = oel3.link_to_line_id
            AND oel1.line_id = p_line_id
            AND oel3.attribute9 IS NOT NULL;
            
            --Added by Ram Talluri TMS 20130918-00610 9/18/2013
            --Start
            IF l_max_bill_cycle IS NULL THEN
            l_max_bill_cycle:=0;
            END IF;      
            --End by Ram Talluri TMS 20130918-00610 9/18/2013
            
      EXCEPTION
         WHEN OTHERS
         THEN
            l_max_bill_cycle := NULL;
      END;

      xxwc_rental_engine_pkg.log_msg (p_debug_level      => g_level_statement,
                                      p_mod_name         => l_module_name,
                                      p_debug_msg        =>    'l_max_bill_cycle : '
                                                            || l_max_bill_cycle
                                     );

      IF NVL (l_max_bill_cycle, x_billing_cycle) < x_billing_cycle
      THEN
         x_billing_cycle := l_max_bill_cycle + 1;
      --elsif l_max_bill_cycle < x_billing_cycle
      ELSE
         x_bill := 'Y';
      END IF;

      xxwc_rental_engine_pkg.log_msg
                                 (p_debug_level      => g_level_statement,
                                  p_mod_name         => l_module_name,
                                  p_debug_msg        =>    'Final x_billing_cycle : '
                                                        || x_billing_cycle
                                 );
   -- 04/02/2012 CGonzalez: Usage is unknown? commenting out
       /*IF x_billing_cycle = 2 THEN

               IF (p_rental_terms IS NULL
                   OR
                   p_rental_terms != '28 Day ReRental Billing'  ) THEN
                   x_bill := 'Y';
               ELSE
                   x_bill := 'N';
               END IF;
       END IF;*/

   /*IF (p_rental_terms IS NULL
              OR
       p_rental_terms != '28 Day ReRental Billing' ) THEN

          IF x_billing_cycle > 2 THEN
                  x_billing_cycle := x_billing_cycle-1;
                  l_Msg_Data := 'Manually Adjusting Billing Cycle to '||x_billing_cycle;
                  XXWC_RENTAL_ENGINE_PKG.LOG_MSG (p_debug_level    => G_LEVEL_ERROR,
                                                  p_mod_name      => l_Module_Name,
                                                  P_DEBUG_MSG     => l_Msg_Data);
          END IF;

      END IF;*/

   -- END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         l_msg_data := 'ERROR: Retrieving Billing Cycle ' || SQLERRM;
         xxwc_rental_engine_pkg.log_msg (p_debug_level      => g_level_error,
                                         p_mod_name         => l_module_name,
                                         p_debug_msg        => l_msg_data
                                        );
   END get_billing_cycle;

/*************************************************************************
*   Procedure : GET_RETURN_UNIT_PRICE
*
*   PURPOSE:   This Procedure Returns the Unit Price for Charge Line
*
*   REVISIONS:
*   Ver        Date        Author                     Description
*   ---------  ----------  ---------------         -------------------------
*   1.0        09/29/2011  Srinivas Malkapuram             Initial Version
*  2.0        03/07/2014   TOby Rave                    Addede roundign precision to two decimals.TMS 20131013-00045
* 3.0       8/15/2014     Ram Talluri           TMS #20140815-00209 Remove Rental OVerride to $300 if Selling Price is Zero 
* ************************************************************************/
   FUNCTION get_return_unit_price (
      p_header_id          IN   NUMBER,
      p_line_id            IN   NUMBER,
      p_billing_terms      IN   VARCHAR2,
      p_charge_item        IN   NUMBER,
      p_rental_item        IN   NUMBER,
      p_unit_price         IN   NUMBER,
      p_ship_from_org_id   IN   NUMBER,
      p_rentaldays         IN   NUMBER,
      p_return_subinv      IN   VARCHAR2
   )
      RETURN NUMBER
   IS
      l_api_name             VARCHAR2 (30)   := 'GET_RETURN_UNIT_PRICE';
      l_module_name          VARCHAR2 (120);
      l_msg_data             VARCHAR2 (2000);
      l_return_date          DATE;
      l_sub_inv              VARCHAR2 (50);
      x_bill                 VARCHAR2 (1);
      l_rental_days          NUMBER;
      l_avg_cost             NUMBER;
      l_unit_price           NUMBER;
      -- 03/30/2012 CG Added to determine the standard item for the rentatl item
      l_rental_item_number   VARCHAR2 (40);
      l_standard_item        VARCHAR2 (40);
      l_standard_item_id     NUMBER;
   BEGIN
      l_module_name := g_module_name || '.' || l_api_name;
      -- 03/30/2012 CGonzalez: Added to determine the base item for the rental item
      l_rental_item_number := NULL;
      l_standard_item := NULL;
      l_standard_item_id := NULL;

      BEGIN
         SELECT segment1
           INTO l_rental_item_number
           FROM mtl_system_items_b
          WHERE inventory_item_id = p_rental_item
            AND organization_id = p_ship_from_org_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_rental_item_number := NULL;
      END;

      IF l_rental_item_number IS NOT NULL
      THEN
         BEGIN
            SELECT inventory_item_id, segment1
              INTO l_standard_item_id, l_standard_item
              FROM mtl_system_items_b
             WHERE organization_id = p_ship_from_org_id
               AND segment1 =
                      (CASE
                          WHEN l_rental_item_number LIKE 'RR%'
                             THEN SUBSTR (l_rental_item_number, 3)
                          WHEN l_rental_item_number LIKE 'R%'
                             THEN SUBSTR (l_rental_item_number, 2)
                          ELSE l_rental_item_number
                       END
                      );
         EXCEPTION
            WHEN OTHERS
            THEN
               l_standard_item_id := p_rental_item;
               l_standard_item := l_rental_item_number;
         END;
      ELSE
         l_standard_item_id := p_rental_item;
      END IF;

      xxwc_rental_engine_pkg.log_msg (p_debug_level      => g_level_procedure,
                                      p_mod_name         => l_module_name,
                                      p_debug_msg        =>    'Rental Item '
                                                            || l_rental_item_number
                                                            || ' - '
                                                            || p_rental_item
                                     );
      xxwc_rental_engine_pkg.log_msg (p_debug_level      => g_level_procedure,
                                      p_mod_name         => l_module_name,
                                      p_debug_msg        =>    'Standard Item '
                                                            || l_standard_item
                                                            || ' - '
                                                            || l_standard_item_id
                                     );
      -- 03/30/2012 CGonzalez
      l_rental_days := p_rentaldays;
      fnd_file.put_line (fnd_file.LOG, 'Rental Days are ' || l_rental_days);
      
      IF UPPER (p_return_subinv) IN
                         (g_lost_sub_inv, g_dbr_sub_inv,
                                                        -- 07/24/2012 CG Added for new subinv
                                                        g_rent_sale_sub_inv)
      THEN
         -- 04/24/2012 CGonzalez
         BEGIN
            SELECT   item_cost
                   * NVL (fnd_profile.VALUE ('XXWC_RENTAL_SUBINV_DMG_CHARGES'),
                          1
                         )
              INTO l_unit_price
              FROM cst_item_costs
             WHERE inventory_item_id = l_standard_item_id
               -- 03/30/2012 CG p_rental_item
               AND organization_id = p_ship_from_org_id
               AND cost_type_id = 2;
               
          --3.0 TMS #20140815-00209 
          IF l_unit_price = 0 THEN
          l_unit_price := 300;
          END IF;
          --3.0 TMS #20140815-00209 
          
         EXCEPTION
            WHEN OTHERS
            THEN
               l_unit_price := 300;
         END;
      ELSIF UPPER (p_return_subinv) = g_dfr_sub_inv
      THEN
         -- 04/24/2012 CGonzalez
         BEGIN
            SELECT item_cost
              INTO l_unit_price
              FROM cst_item_costs
             WHERE inventory_item_id = l_standard_item_id
               -- 03/30/2012 CG p_rental_item
               AND organization_id = p_ship_from_org_id
               AND cost_type_id = 2;
          
          --3.0 TMS #20140815-00209 
          IF l_unit_price = 0 THEN
          l_unit_price := 300;
          END IF;
          --3.0 TMS #20140815-00209 
               
         EXCEPTION
            WHEN OTHERS
            THEN
               l_unit_price := 300;
         END;
      ELSIF (   p_billing_terms IS NULL
             OR p_billing_terms != '28 Day ReRental Billing'
            )
      THEN
         SELECT ROUND ((p_unit_price / 28 * l_rental_days), 2)
           -- l_rental_Days is actual prorated days..
         INTO   l_unit_price
           FROM DUAL;
      ELSIF p_billing_terms = '28 Day ReRental Billing'
      THEN
         SELECT ROUND ((p_unit_price / 28 * (28 - l_rental_days)), 2)
           INTO l_unit_price
           FROM DUAL;
      END IF;

      IF l_unit_price = 0
      THEN
         --SELECT DECODE (l_unit_price, 0, 300, l_unit_price) 3.0 Commented for TMS #20140815-00209 by Ram Talluri 8/15/14
         SELECT DECODE (l_unit_price, 0, 0, l_unit_price) --added for 3.0 TMS #20140815-00209   by Ram Talluri 8/15/14
           INTO l_unit_price
           FROM DUAL;
      END IF;

      RETURN ROUND((l_unit_price),2);/*TMS 20131013-00045 , Toby Rave, March 07,2014 */
   EXCEPTION
      WHEN OTHERS
      THEN
         l_msg_data := 'Error: In getting Unit price of charge item';
         xxwc_rental_engine_pkg.log_msg (p_debug_level      => g_level_error,
                                         p_mod_name         => l_module_name,
                                         p_debug_msg        => l_msg_data
                                        );
   END get_return_unit_price;

/*************************************************************************
*   Procedure : GET_RETURN_SUBINVENTORY (For Long Term Rentals)
*
*   PURPOSE:   This Procedure Returns the SubInventory to which Rental Item is returned.
*
*   REVISIONS:
*   Ver        Date        Author                     Description
*   ---------  ----------  ---------------         -------------------------
*   1.0        09/29/2011  Srinivas Malkapuram             Initial Version
*  2.0        8/1/2014     Ram Talluri                  to_number function added for TMS #20140801-00035 
* ************************************************************************/
   PROCEDURE get_return_subinventory (
      p_header_id              IN              NUMBER,
      p_line_id                IN              NUMBER,
      p_actual_shipment_date   IN              DATE,
      p_any_chr_bill_cycle     IN              NUMBER,
      x_return_subinv          OUT NOCOPY      VARCHAR2,
      x_return_date            OUT NOCOPY      DATE,
      x_rental_days            OUT NOCOPY      NUMBER,
      x_rcv_quantity           OUT NOCOPY      NUMBER
   )
   IS
      l_api_name      VARCHAR2 (30)   := 'GET_RETURN_SUBINVENTORY';
      l_module_name   VARCHAR2 (120);
      l_msg_data      VARCHAR2 (2000);
   BEGIN
      l_module_name := g_module_name || '.' || l_api_name;
      -- Initialize Return Status Variable
      xxwc_rental_engine_pkg.log_msg
                      (p_debug_level      => g_level_procedure,
                       p_mod_name         => l_module_name,
                       p_debug_msg        => 'Getting SubInventory for Return Line '
                      );

      SELECT NVL (rt.subinventory, oel.subinventory),
             NVL (TO_DATE (oel.attribute12, 'YYYY/MM/DD HH24:MI:SS'),
                  rt.transaction_date
                 ),
             NVL (rt.quantity, oel.ordered_quantity)
        --  ,oe_order_line_id
      INTO   x_return_subinv,
             x_return_date,
             x_rcv_quantity
        --  ,x_rcv_line_id
      FROM   rcv_transactions rt, apps.oe_order_lines oel
       WHERE oel.header_id = p_header_id
         AND oel.line_id = p_line_id
         AND oel.header_id = rt.oe_order_header_id(+)
         AND oel.line_id = rt.oe_order_line_id(+)
         --AND rt.oe_order_header_id = p_header_id
         --AND rt.oe_order_line_id = p_line_id
         AND rt.destination_type_code(+) = 'INVENTORY'
         AND rt.source_document_code(+) = 'RMA'
         AND rt.transaction_type(+) = 'DELIVER';

      fnd_file.put_line (fnd_file.LOG, 'Return Date ' || x_return_date);
      fnd_file.put_line (fnd_file.LOG,
                         'Actual Shipment Date ' || p_actual_shipment_date
                        );
      fnd_file.put_line (fnd_file.LOG,
                         'Return SubInventory ' || x_return_subinv
                        );

      IF x_return_date IS NOT NULL
      THEN
         IF p_any_chr_bill_cycle IS NULL
         THEN
            SELECT   (TRUNC (x_return_date) - TRUNC (p_actual_shipment_date))
                   + 1
                   - (28 * attribute9)                 --trunc(creation_date )
              INTO x_rental_days
              FROM apps.oe_order_lines
             WHERE attribute9 =
                      --(SELECT NVL (MAX (attribute9), 1)--commented by Ram Talluri for TMS #20140801-00035 
                      (SELECT NVL (MAX (TO_NUMBER(attribute9)), 1)--to_number function added by Ram Talluri for TMS #20140801-00035
                         FROM apps.oe_order_lines
                        WHERE header_id = p_header_id
                          AND link_to_line_id = p_line_id)
               AND header_id = p_header_id
               AND link_to_line_id = p_line_id;
         ELSE
            SELECT   (TRUNC (x_return_date) - TRUNC (p_actual_shipment_date))
                   + 1
                   - (28 * p_any_chr_bill_cycle)
              INTO x_rental_days
              FROM DUAL;
         END IF;
      ELSE
         x_return_subinv := NULL;
         x_rcv_quantity := NULL;
         x_rental_days := NULL;
      END IF;
      -- 3.0 TMS #20140815-00209
      IF x_rental_days<0 THEN
      x_rental_days:=0;
      END IF;
      -- 3.0 TMS #20140815-00209
      
   EXCEPTION
      WHEN OTHERS
      THEN
         l_msg_data := 'Error: In Getting Subinventory Information';
         xxwc_rental_engine_pkg.log_msg (p_debug_level      => g_level_error,
                                         p_mod_name         => l_module_name,
                                         p_debug_msg        => l_msg_data
                                        );
   END get_return_subinventory;

/*************************************************************************
*   Procedure : GET_RETURN_SUBINVENTORY (For Short Term Rentals)
*
*   PURPOSE:   This Procedure Returns the SubInventory to which Rental Item is returned.
*
*   REVISIONS:
*   Ver        Date        Author                     Description
*   ---------  ----------  ---------------         -------------------------
*   1.0        09/29/2011  Srinivas Malkapuram             Initial Version
* ************************************************************************/
   PROCEDURE get_return_subinventory (
      p_header_id              IN              NUMBER,
      p_line_id                IN              NUMBER,
      p_actual_shipment_date   IN              DATE,
      x_return_subinv          OUT NOCOPY      VARCHAR2,
      x_return_date            OUT NOCOPY      DATE,
      x_rental_days            OUT NOCOPY      NUMBER,
      x_rcv_quantity           OUT NOCOPY      NUMBER
   )
   IS
      l_api_name      VARCHAR2 (30)   := 'GET_RETURN_SUBINVENTORY';
      l_module_name   VARCHAR2 (120);
      l_msg_data      VARCHAR2 (2000);
   BEGIN
      l_module_name := g_module_name || '.' || l_api_name;
      -- Initialize Return Status Variable
      xxwc_rental_engine_pkg.log_msg
                      (p_debug_level      => g_level_procedure,
                       p_mod_name         => l_module_name,
                       p_debug_msg        => 'Getting SubInventory for Return Line '
                      );

      /*SELECT subinventory, transaction_date, quantity
        INTO x_return_subinv, x_return_date, x_rcv_quantity
        FROM rcv_transactions
       WHERE oe_order_header_id = p_header_id
         AND oe_order_line_id = p_line_id
         AND destination_type_code = 'INVENTORY'
         AND source_document_code = 'RMA'
         AND transaction_type = 'DELIVER';*/
      SELECT NVL (rt.subinventory, oel.subinventory),
             NVL (TO_DATE (oel.attribute12, 'YYYY/MM/DD HH24:MI:SS'),
                  rt.transaction_date
                 ),
             NVL (rt.quantity, oel.ordered_quantity)
        INTO x_return_subinv,
             x_return_date,
             x_rcv_quantity
        FROM rcv_transactions rt, apps.oe_order_lines oel
       WHERE oel.header_id = p_header_id
         AND oel.line_id = p_line_id
         AND oel.header_id = rt.oe_order_header_id(+)
         AND oel.line_id = rt.oe_order_line_id(+)
         --AND rt.oe_order_header_id = p_header_id
         --AND rt.oe_order_line_id = p_line_id
         AND rt.destination_type_code(+) = 'INVENTORY'
         AND rt.source_document_code(+) = 'RMA'
         AND rt.transaction_type(+) = 'DELIVER';

      IF x_return_date IS NOT NULL
      THEN
         SELECT TRUNC (x_return_date) - TRUNC (p_actual_shipment_date)
           INTO x_rental_days
           FROM DUAL;
      ELSE
         x_return_subinv := NULL;
         x_rcv_quantity := NULL;
         x_rental_days := NULL;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         l_msg_data := 'Error: In Getting Subinventory Information';
         xxwc_rental_engine_pkg.log_msg (p_debug_level      => g_level_error,
                                         p_mod_name         => l_module_name,
                                         p_debug_msg        => l_msg_data
                                        );
   END get_return_subinventory;

/*************************************************************************
*   Procedure : APPLY_HOLD_CHARGE_LINE
*
*   PURPOSE:   This Procedure applies the Hold against charge line.
*
*   REVISIONS:
*   Ver        Date        Author                     Description
*   ---------  ----------  ---------------         -------------------------
*   1.0        09/29/2011  Srinivas Malkapuram             Initial Version
* ************************************************************************/
   PROCEDURE apply_hold_charge_line (
      p_header_id            IN              NUMBER,
      p_charge_line_id       IN              NUMBER,
      l_hold_return_status   OUT NOCOPY      VARCHAR2
   )
   IS
      l_api_name        VARCHAR2 (30)             := 'APPLY_HOLD_CHARGE_LINE';
      l_module_name     VARCHAR2 (120);
      l_debug_msg       VARCHAR2 (2000);
      l_rma_qty         NUMBER                      := 0;
      l_order_tbl       oe_holds_pvt.order_tbl_type;
      l_hold_id         NUMBER;
      l_line_id         NUMBER;
      l_header_id       NUMBER;
      l_return_status   VARCHAR2 (5);
      l_msg_count       NUMBER;
      l_msg_data        VARCHAR2 (2000);
      hold_msg_data     VARCHAR2 (2000);
   BEGIN
      l_hold_return_status := fnd_api.g_ret_sts_success;

      SELECT hold_id
        INTO l_hold_id
        FROM oe_hold_definitions
       WHERE UPPER (NAME) = g_rental_hold;

      l_order_tbl (1).header_id := p_header_id;
      l_order_tbl (1).line_id := p_charge_line_id;
      oe_holds_pub.apply_holds
                            (p_api_version           => 1.0,
                             p_init_msg_list         => fnd_api.g_false,
                             p_commit                => fnd_api.g_true,
                             --FND_API.G_FALSE ,
                             p_validation_level      => fnd_api.g_valid_level_full,
                             p_order_tbl             => l_order_tbl,
                             p_hold_id               => l_hold_id,
                             x_return_status         => l_return_status,
                             x_msg_count             => l_msg_count,
                             x_msg_data              => l_msg_data
                            );

      IF l_return_status = fnd_api.g_ret_sts_success
      THEN
         COMMIT;
         l_hold_return_status := 'S';
         xxwc_rental_engine_pkg.log_msg
                                    (p_debug_level      => g_level_procedure,
                                     p_mod_name         => l_module_name,
                                     p_debug_msg        => 'Charge Line Put on HOLD'
                                    );
      ELSE
         ROLLBACK;
         l_hold_return_status := 'E';

         FOR i IN 1 .. l_msg_count
         LOOP
            l_msg_data := oe_msg_pub.get (p_msg_index      => i,
                                          p_encoded        => 'F');
            fnd_file.put_line (fnd_file.LOG, i || ') ' || l_msg_data);
         END LOOP;

         hold_msg_data := 'Charge Line cannot be Put on Hold' || l_msg_data;
         xxwc_rental_engine_pkg.log_msg (p_debug_level      => g_level_procedure,
                                         p_mod_name         => l_module_name,
                                         p_debug_msg        => hold_msg_data
                                        );
      END IF;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         l_debug_msg := ('No Hold found for  ' || g_rental_hold);
         xxwc_rental_engine_pkg.log_msg (p_debug_level      => g_level_statement,
                                         p_mod_name         => l_module_name,
                                         p_debug_msg        => l_debug_msg
                                        );
      WHEN TOO_MANY_ROWS
      THEN
         l_debug_msg :=
                  ('Duplicate Hold Definitions found for ' || g_rental_hold
                  );
         xxwc_rental_engine_pkg.log_msg (p_debug_level      => g_level_statement,
                                         p_mod_name         => l_module_name,
                                         p_debug_msg        => l_debug_msg
                                        );
      WHEN OTHERS
      THEN
         l_debug_msg := ('ERROR while Obtaining Hold for ' || g_rental_hold);
         xxwc_rental_engine_pkg.log_msg (p_debug_level      => g_level_statement,
                                         p_mod_name         => l_module_name,
                                         p_debug_msg        => l_debug_msg
                                        );
   END apply_hold_charge_line;

/*************************************************************************
   *   Function : GET_WC_TRXN_ID
   *
   *   PURPOSE:   This Function Returns Transaction ID for WC BILL ONLY.
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        09/29/2011  Srinivas Malkapuram             Initial Version
   * ************************************************************************/
   FUNCTION get_wc_trxn_id (p_trxtype IN VARCHAR2)
      RETURN NUMBER
   IS
      l_trans_type_id   NUMBER;
      l_api_name        VARCHAR2 (30)   := 'GET_WC_TRXN_ID';
      l_module_name     VARCHAR2 (120);
      l_debug_msg       VARCHAR2 (2000);
   BEGIN
      l_module_name := g_module_name || '.' || l_api_name;
      xxwc_rental_engine_pkg.log_msg
                           (p_debug_level      => g_level_procedure,
                            p_mod_name         => l_module_name,
                            p_debug_msg        =>    'Getting Transaction Type for '
                                                  || p_trxtype
                           );

      SELECT transaction_type_id
        INTO l_trans_type_id
        FROM oe_transaction_types_tl
       WHERE LANGUAGE = 'US' AND UPPER (NAME) = UPPER (p_trxtype);

      RETURN l_trans_type_id;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         l_debug_msg := ('Transaction Type Not Defined for  ' || p_trxtype);
         xxwc_rental_engine_pkg.log_msg (p_debug_level      => g_level_statement,
                                         p_mod_name         => l_module_name,
                                         p_debug_msg        => l_debug_msg
                                        );
         l_trans_type_id := 0;
      WHEN TOO_MANY_ROWS
      THEN
         l_debug_msg :=
                 ('Duplicate Data found for Transaction Type ' || p_trxtype
                 );
         xxwc_rental_engine_pkg.log_msg (p_debug_level      => g_level_statement,
                                         p_mod_name         => l_module_name,
                                         p_debug_msg        => l_debug_msg
                                        );
         l_trans_type_id := 0;
      WHEN OTHERS
      THEN
         l_debug_msg :=
              ('ERROR while Obtaining Transaction TypeID for ' || p_trxtype
              );
         xxwc_rental_engine_pkg.log_msg (p_debug_level      => g_level_statement,
                                         p_mod_name         => l_module_name,
                                         p_debug_msg        => l_debug_msg
                                        );
         l_trans_type_id := 0;
   END get_wc_trxn_id;

   FUNCTION is_item_serialized (
      p_line_id                 IN   NUMBER,
      p_inventory_item_id       IN   NUMBER,
      p_ship_from_org_id        IN   NUMBER,
      p_mtl_transaction_type    IN   VARCHAR2,
      p_mtl_subinventory_code   IN   VARCHAR2,
      p_mtl_source_code         IN   VARCHAR2
   )
      RETURN BOOLEAN
   IS
      l_api_name         VARCHAR2 (30)   := 'IS_ITEM_SERIALIZED';
      l_module_name      VARCHAR2 (120);
      l_debug_msg        VARCHAR2 (2000);
      l_serialized_cnt   NUMBER;
   BEGIN
      l_module_name := g_module_name || '.' || l_api_name;
      xxwc_rental_engine_pkg.log_msg
         (p_debug_level      => g_level_procedure,
          p_mod_name         => l_module_name,
          p_debug_msg        =>    'Inside the Function for Getting Serialized Count for Line ID '
                                || p_line_id
         );

      SELECT COUNT (msn.serial_number)
        INTO l_serialized_cnt
        FROM mtl_serial_numbers msn,
             mtl_material_transactions mmt,
             mtl_transaction_types mtt
       WHERE mmt.transaction_id = msn.last_transaction_id
         AND mmt.inventory_item_id = p_inventory_item_id
         AND mmt.trx_source_line_id = p_line_id
         AND mmt.transaction_type_id = mtt.transaction_type_id
         AND UPPER (mtt.transaction_type_name) = p_mtl_transaction_type
         --G_MTL_TRANSACTION_TYPE
         AND UPPER (mmt.subinventory_code) = p_mtl_subinventory_code
         --G_MTL_SUBINVENTORY_CODE
         AND UPPER (mmt.source_code) = p_mtl_source_code;

      --G_MTL_SOURCE_CODE ;
      xxwc_rental_engine_pkg.log_msg (p_debug_level      => g_level_procedure,
                                      p_mod_name         => l_module_name,
                                      p_debug_msg        =>    'Serialized Count '
                                                            || l_serialized_cnt
                                     );

      IF l_serialized_cnt = 0
      THEN
         RETURN FALSE;
      ELSE
         RETURN TRUE;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN FALSE;
         l_debug_msg :=
               'ERROR: while trying to Check if ITEM is Serialized for Line id '
            || p_line_id;
         xxwc_rental_engine_pkg.log_msg (p_debug_level      => g_level_procedure,
                                         p_mod_name         => l_module_name,
                                         p_debug_msg        => l_debug_msg
                                        );
   END is_item_serialized;

   FUNCTION get_charge_item (p_inventory_item_id IN NUMBER)
      RETURN NUMBER
   IS
      l_api_name      VARCHAR2 (30)   := 'GET_CHARGE_ITEM';
      l_module_name   VARCHAR2 (120);
      l_debug_msg     VARCHAR2 (2000);
      l_charge_item   NUMBER          := 0;
   BEGIN
      SELECT related_item_id
        INTO l_charge_item
        FROM mtl_related_items
       WHERE inventory_item_id = p_inventory_item_id
         AND UPPER (attr_char1) = 'Rental';

      RETURN l_charge_item;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         l_debug_msg :=
                       ('No Charge Item found for  ' || p_inventory_item_id
                       );
         xxwc_rental_engine_pkg.log_msg (p_debug_level      => g_level_statement,
                                         p_mod_name         => l_module_name,
                                         p_debug_msg        => l_debug_msg
                                        );
         RETURN NULL;
      WHEN TOO_MANY_ROWS
      THEN
         l_debug_msg :=
                 ('Too Many Charge Items found for ' || p_inventory_item_id
                 );
         xxwc_rental_engine_pkg.log_msg (p_debug_level      => g_level_statement,
                                         p_mod_name         => l_module_name,
                                         p_debug_msg        => l_debug_msg
                                        );
         RETURN NULL;
      WHEN OTHERS
      THEN
         l_debug_msg :=
            ('ERROR while Obtaining Charge Item for ' || p_inventory_item_id
            );
         xxwc_rental_engine_pkg.log_msg (p_debug_level      => g_level_statement,
                                         p_mod_name         => l_module_name,
                                         p_debug_msg        => l_debug_msg
                                        );
         RETURN NULL;
   END get_charge_item;

   FUNCTION get_charge_item (
      p_inventory_item_id   IN   NUMBER,
      p_ship_from_org_id    IN   NUMBER
   )
      RETURN NUMBER
   IS
      l_api_name      VARCHAR2 (30)   := 'GET_CHARGE_ITEM';
      l_module_name   VARCHAR2 (120);
      l_debug_msg     VARCHAR2 (2000);
      l_charge_item   NUMBER          := 0;
   BEGIN
      /* SELECT related_item_id
         INTO l_charge_item
        FROM mtl_related_items
        WHERE inventory_item_id = p_inventory_item_id
          AND UPPER(attr_char1) = 'RENTAL';   */
      SELECT inventory_item_id
        INTO l_charge_item
        FROM mtl_system_items_b
       WHERE segment1 = 'Rental Charge'
         AND organization_id = p_ship_from_org_id;

      RETURN l_charge_item;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         l_debug_msg :=
                       ('No Charge Item found for  ' || p_inventory_item_id
                       );
         xxwc_rental_engine_pkg.log_msg (p_debug_level      => g_level_statement,
                                         p_mod_name         => l_module_name,
                                         p_debug_msg        => l_debug_msg
                                        );
         RETURN NULL;
      WHEN TOO_MANY_ROWS
      THEN
         l_debug_msg :=
                 ('Too Many Charge Items found for ' || p_inventory_item_id
                 );
         xxwc_rental_engine_pkg.log_msg (p_debug_level      => g_level_statement,
                                         p_mod_name         => l_module_name,
                                         p_debug_msg        => l_debug_msg
                                        );
         RETURN NULL;
      WHEN OTHERS
      THEN
         l_debug_msg :=
            ('ERROR while Obtaining Charge Item for ' || p_inventory_item_id
            );
         xxwc_rental_engine_pkg.log_msg (p_debug_level      => g_level_statement,
                                         p_mod_name         => l_module_name,
                                         p_debug_msg        => l_debug_msg
                                        );
         RETURN NULL;
   END get_charge_item;
   /*************************************************************************
    *   Procedure : get_charge_item_unit_price
    *
    *   PURPOSE:   get charge item unit price 
    *
    *   REVISIONS:
    *   Ver        Date          Author                     Description
    *   ---------  ------------  ---------------         -------------------------
    *   6.0        30-Aug-2018   Niraj K Ranjan           TMS#20180622-00050   Rental Price Not Created # 28917535
    * ************************************************************************/
   FUNCTION get_charge_item_unit_price (
      p_charge_item      IN   NUMBER,
      p_charge_line_id   IN   NUMBER
   )
      RETURN NUMBER
   IS
      l_api_name      VARCHAR2 (30)   := 'GET_CHARGE_ITEM_UNIT_PRICE';
      l_module_name   VARCHAR2 (120);
      l_debug_msg     VARCHAR2 (2000);
      l_unit_price    NUMBER          := 0;
   BEGIN
      SELECT ql.operand
        INTO l_unit_price
        FROM qp_list_lines ql,
             qp_pricing_attributes qa,
             apps.oe_order_lines oel
       WHERE ql.list_header_id = oel.price_list_id
         AND ql.list_line_id = qa.list_line_id
         --AND qa.product_uom_code = 'EA' --Ver 6.0
         AND qa.product_attr_value = TO_CHAR (p_charge_item)
         AND oel.line_id = p_charge_line_id
		 and trunc(nvl(ql.END_DATE_ACTIVE,sysdate+1)) > trunc(sysdate) --Ver 6.0
		 and rownum = 1; --Ver 6.0

      RETURN l_unit_price;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         l_debug_msg := ('No List Price found for  ' || p_charge_item);
         xxwc_rental_engine_pkg.log_msg (p_debug_level      => g_level_statement,
                                         p_mod_name         => l_module_name,
                                         p_debug_msg        => l_debug_msg
                                        );
         RETURN NULL;
      WHEN TOO_MANY_ROWS
      THEN
         l_debug_msg := ('Too Many List Prices found for ' || p_charge_item);
         xxwc_rental_engine_pkg.log_msg (p_debug_level      => g_level_statement,
                                         p_mod_name         => l_module_name,
                                         p_debug_msg        => l_debug_msg
                                        );
         RETURN NULL;
      WHEN OTHERS
      THEN
         l_debug_msg := ('ERROR while Obtaining Price List for ' || SQLERRM);
         xxwc_rental_engine_pkg.log_msg (p_debug_level      => g_level_statement,
                                         p_mod_name         => l_module_name,
                                         p_debug_msg        => l_debug_msg
                                        );
         RETURN NULL;
   END get_charge_item_unit_price;

   FUNCTION get_rma_qty (p_header_id IN NUMBER, p_charge_line_id IN NUMBER)
      RETURN NUMBER
   IS
      l_api_name      VARCHAR2 (30)   := 'GET_RMA_QTY';
      l_module_name   VARCHAR2 (120);
      l_debug_msg     VARCHAR2 (2000);
      l_rma_qty       NUMBER          := 0;
   BEGIN
      SELECT SUM (ordered_quantity)
        INTO l_rma_qty
        FROM apps.oe_order_lines
       WHERE header_id = p_header_id
         AND return_attribute1 = p_header_id
         AND return_attribute2 = p_charge_line_id
         AND line_category_code = 'RETURN'
         AND flow_status_code <> 'CANCELLED';

      RETURN l_rma_qty;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         l_debug_msg := ('No RMA Qty found for  ' || p_charge_line_id);
         xxwc_rental_engine_pkg.log_msg (p_debug_level      => g_level_statement,
                                         p_mod_name         => l_module_name,
                                         p_debug_msg        => l_debug_msg
                                        );
         RETURN NULL;
      WHEN TOO_MANY_ROWS
      THEN
         l_debug_msg :=
                  ('Too Many RMA Quantities found for ' || p_charge_line_id
                  );
         xxwc_rental_engine_pkg.log_msg (p_debug_level      => g_level_statement,
                                         p_mod_name         => l_module_name,
                                         p_debug_msg        => l_debug_msg
                                        );
         RETURN NULL;
      WHEN OTHERS
      THEN
         l_debug_msg :=
                  ('ERROR while Obtaining RMA Qty for ' || p_charge_line_id
                  );
         xxwc_rental_engine_pkg.log_msg (p_debug_level      => g_level_statement,
                                         p_mod_name         => l_module_name,
                                         p_debug_msg        => l_debug_msg
                                        );
         RETURN NULL;
   END get_rma_qty;
/*************************************************************************
   *   Function : get_chr_line_attr9_split_rma
   *
   *   PURPOSE:   This Function Returns billing cycle.
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        09/29/2011  Srinivas Malkapuram             Initial Version
   *  2.0        8/1/2014     Ram Talluri                  to_number function added for TMS #20140801-00035 
   * ************************************************************************/
   FUNCTION get_chr_line_attr9_split_rma (p_line_id IN NUMBER)
      RETURN NUMBER
   IS
      l_api_name             VARCHAR2 (30)  := 'GET_CHR_LINE_ATTR9_SPLIT_RMA';
      l_module_name          VARCHAR2 (120);
      l_debug_msg            VARCHAR2 (2000);
      l_any_chr_bill_cycle   NUMBER;
   BEGIN
      --SELECT MAX (oel3.attribute9)--commented by Ram Talluri for TMS #20140801-00035 
      SELECT MAX (TO_NUMBER(oel3.attribute9))--to_number function added by Ram Talluri for TMS #20140801-00035
        INTO l_any_chr_bill_cycle
        FROM apps.oe_order_lines oel1,
             apps.oe_order_lines oel2,
             apps.oe_order_lines oel3
       WHERE oel1.line_id = oel2.link_to_line_id
         AND oel2.line_id = oel3.link_to_line_id
         AND oel1.line_id = p_line_id;
         
      RETURN l_any_chr_bill_cycle;
      
      
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         l_debug_msg :=
            (   'No Data found while Retrieving GET_CHR_LINE_ATTR9_SPLIT_RMA  '
             || p_line_id
            );
         xxwc_rental_engine_pkg.log_msg (p_debug_level      => g_level_statement,
                                         p_mod_name         => l_module_name,
                                         p_debug_msg        => l_debug_msg
                                        );
         RETURN NULL;
      WHEN TOO_MANY_ROWS
      THEN
         l_debug_msg :=
            (   'Too Many Rows found while Retrieving GET_CHR_LINE_ATTR9_SPLIT_RMA '
             || p_line_id
            );
         xxwc_rental_engine_pkg.log_msg (p_debug_level      => g_level_statement,
                                         p_mod_name         => l_module_name,
                                         p_debug_msg        => l_debug_msg
                                        );
         RETURN NULL;
      WHEN OTHERS
      THEN
         l_debug_msg :=
            (   'ERROR while Retrieving GET_CHR_LINE_ATTR9_SPLIT_RMA for '
             || p_line_id
            );
         xxwc_rental_engine_pkg.log_msg (p_debug_level      => g_level_statement,
                                         p_mod_name         => l_module_name,
                                         p_debug_msg        => l_debug_msg
                                        );
         RETURN NULL;
   END get_chr_line_attr9_split_rma;

   FUNCTION get_item_description (p_line_id IN NUMBER)
      RETURN VARCHAR
   IS
      l_api_name           VARCHAR2 (30)   := 'GET_ITEM_DESCRIPTION';
      l_module_name        VARCHAR2 (120);
      l_debug_msg          VARCHAR2 (2000);
      l_item_description   VARCHAR2 (50);
   BEGIN
      SELECT ordered_item
        INTO l_item_description
        FROM apps.oe_order_lines oel
       WHERE oel.line_id = p_line_id;

      RETURN l_item_description;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         l_debug_msg :=
            (   'No Data found while Retrieving GET_ITEM_DESCRIPTION  '
             || p_line_id
            );
         xxwc_rental_engine_pkg.log_msg (p_debug_level      => g_level_statement,
                                         p_mod_name         => l_module_name,
                                         p_debug_msg        => l_debug_msg
                                        );
         RETURN NULL;
      WHEN TOO_MANY_ROWS
      THEN
         l_debug_msg :=
            (   'Too Many Rows found while Retrieving GET_ITEM_DESCRIPTION '
             || p_line_id
            );
         xxwc_rental_engine_pkg.log_msg (p_debug_level      => g_level_statement,
                                         p_mod_name         => l_module_name,
                                         p_debug_msg        => l_debug_msg
                                        );
         RETURN NULL;
      WHEN OTHERS
      THEN
         l_debug_msg :=
            ('ERROR while Retrieving GET_ITEM_DESCRIPTION for ' || p_line_id
            );
         xxwc_rental_engine_pkg.log_msg (p_debug_level      => g_level_statement,
                                         p_mod_name         => l_module_name,
                                         p_debug_msg        => l_debug_msg
                                        );
         RETURN NULL;
   END get_item_description;

   FUNCTION get_mtl_sales_order_id (p_order_num IN NUMBER)
      RETURN NUMBER
   IS
      l_api_name         VARCHAR2 (30)   := 'GET_MTL_SALES_ORDER_ID';
      l_module_name      VARCHAR2 (120);
      l_debug_msg        VARCHAR2 (2000);
      l_sales_order_id   NUMBER;
   BEGIN
      SELECT sales_order_id
        INTO l_sales_order_id
        FROM mtl_sales_orders
       WHERE segment1 = p_order_num AND enabled_flag = 'Y';

      RETURN l_sales_order_id;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         l_debug_msg :=
            ('No Sales Order ID found in MTL Sales Orders for  '
             || p_order_num
            );
         xxwc_rental_engine_pkg.log_msg (p_debug_level      => g_level_statement,
                                         p_mod_name         => l_module_name,
                                         p_debug_msg        => l_debug_msg
                                        );
         RETURN NULL;
      WHEN TOO_MANY_ROWS
      THEN
         l_debug_msg :=
            (   'Too Many Sales Orders found in MTL Sales Orders for '
             || p_order_num
            );
         xxwc_rental_engine_pkg.log_msg (p_debug_level      => g_level_statement,
                                         p_mod_name         => l_module_name,
                                         p_debug_msg        => l_debug_msg
                                        );
         RETURN NULL;
      WHEN OTHERS
      THEN
         l_debug_msg :=
            (   'ERROR while Obtaining Sales Order ID in MTL Sales Orders '
             || p_order_num
            );
         xxwc_rental_engine_pkg.log_msg (p_debug_level      => g_level_statement,
                                         p_mod_name         => l_module_name,
                                         p_debug_msg        => l_debug_msg
                                        );
         RETURN NULL;
   END get_mtl_sales_order_id;

   PROCEDURE create_reservation_line (
      v_reservation_record   IN              reservation_int_rec,
      p_rma_line_id          IN              NUMBER,
      p_inventory_item_id    IN              NUMBER,
      p_return_subinv        IN              VARCHAR2,
      lx_return_status       OUT NOCOPY      VARCHAR2
   )
   IS
      v_reservation_rec            inv_reservation_global.mtl_reservation_rec_type;
      v_reserv_serial_rec          inv_reservation_global.serial_number_tbl_type;
      v_reservation_quantity       NUMBER;
      v_reservation_interface_id   NUMBER;
      l_reservation_id             NUMBER;
      lx_msg_count                 NUMBER;
      lx_msg_data                  VARCHAR2 (2000);
      l_reserv_serial_rec          inv_reservation_global.serial_number_tbl_type;
      l_api_name                   VARCHAR2 (30) := 'CREATE_RESERVATION_LINE';
      l_module_name                VARCHAR2 (120)
                                                 := 'Calling Reservation API';
      l_debug_msg                  VARCHAR2 (2000);
      serial_ctr                   NUMBER                                := 1;

/* Cursor to get the Serial Numbers */
      CURSOR cur_get_serial_no (
         p_line_id                 NUMBER,
         p_inventory_item_id       NUMBER,
         p_mtl_transaction_type    VARCHAR2,
         p_mtl_subinventory_code   VARCHAR2,
         p_mtl_source_code         VARCHAR2
      )
      IS
         SELECT msn.serial_number
           FROM mtl_serial_numbers msn,
                mtl_material_transactions mmt,
                mtl_transaction_types mtt
          WHERE mmt.transaction_id = msn.last_transaction_id
            AND mmt.inventory_item_id = p_inventory_item_id
            AND mmt.trx_source_line_id = p_line_id
            AND mmt.transaction_type_id = mtt.transaction_type_id
            AND UPPER (mtt.transaction_type_name) = p_mtl_transaction_type
            AND UPPER (mmt.subinventory_code) = p_mtl_subinventory_code
            AND UPPER (mmt.source_code) = p_mtl_source_code;
   BEGIN
      v_reservation_quantity := v_reservation_record.v_reservation_quantity;
      v_reservation_interface_id :=
                              v_reservation_record.v_reservation_interface_id;

      FOR get_get_serial_no IN cur_get_serial_no (p_rma_line_id,
                                                  p_inventory_item_id,
                                                  g_rma_trxn_type,
                                                  p_return_subinv,
                                                  g_rma_source_code
                                                 )
      LOOP
         v_reserv_serial_rec (serial_ctr).serial_number :=
                                              get_get_serial_no.serial_number;
         fnd_file.put_line (fnd_file.LOG,
                               'Reservation serial Num for '
                            || ' Counter '
                            || serial_ctr
                            || ','
                            || v_reserv_serial_rec (serial_ctr).serial_number
                           );
         v_reserv_serial_rec (serial_ctr).inventory_item_id :=
                                                           p_inventory_item_id;
         fnd_file.put_line (fnd_file.LOG,
                               'Inventory Item ID '
                            || v_reserv_serial_rec (serial_ctr).inventory_item_id
                           );
         serial_ctr := serial_ctr + 1;
      END LOOP;

      v_reservation_rec.requirement_date :=
                                       v_reservation_record.v_requirement_date;
      v_reservation_rec.organization_id :=
                                        v_reservation_record.v_organization_id;
      v_reservation_rec.inventory_item_id :=
                                      v_reservation_record.v_inventory_item_id;
      v_reservation_rec.demand_source_type_id :=
                                  v_reservation_record.v_demand_source_type_id;
      v_reservation_rec.demand_source_name := NULL;
      v_reservation_rec.demand_source_header_id :=
                                v_reservation_record.v_demand_source_header_id;
      v_reservation_rec.demand_source_line_id :=
                                  v_reservation_record.v_demand_source_line_id;
      v_reservation_rec.primary_uom_code :=
                                   v_reservation_record.v_reservation_uom_code;
      v_reservation_rec.primary_uom_id := NULL;
      v_reservation_rec.reservation_uom_code :=
                                   v_reservation_record.v_reservation_uom_code;
      v_reservation_rec.reservation_uom_id := NULL;
      v_reservation_rec.reservation_quantity :=
                                   v_reservation_record.v_reservation_quantity;
      v_reservation_rec.primary_reservation_quantity :=
                                   v_reservation_record.v_reservation_quantity;
      v_reservation_rec.supply_source_type_id :=
                                  v_reservation_record.v_supply_source_type_id;
      v_reservation_rec.ship_ready_flag := 1;
      v_reservation_rec.attribute15 := NULL;
      v_reservation_rec.attribute14 := NULL;
      v_reservation_rec.attribute13 := NULL;
      v_reservation_rec.attribute12 := NULL;
      v_reservation_rec.attribute11 := NULL;
      v_reservation_rec.attribute10 := NULL;
      v_reservation_rec.attribute9 := NULL;
      v_reservation_rec.attribute8 := NULL;
      v_reservation_rec.attribute7 := NULL;
      v_reservation_rec.attribute6 := NULL;
      v_reservation_rec.attribute5 := NULL;
      v_reservation_rec.attribute4 := NULL;
      v_reservation_rec.attribute3 := NULL;
      v_reservation_rec.attribute2 := NULL;
      v_reservation_rec.attribute1 := NULL;
      v_reservation_rec.attribute_category := NULL;
      v_reservation_rec.lpn_id := NULL;
      v_reservation_rec.pick_slip_number := NULL;
      v_reservation_rec.lot_number_id := NULL;
      v_reservation_rec.lot_number := NULL;
      v_reservation_rec.locator_id := NULL;
      v_reservation_rec.subinventory_id := NULL;
      v_reservation_rec.subinventory_code :=
                                      v_reservation_record.v_subinventory_code;
      v_reservation_rec.revision := NULL;
      v_reservation_rec.supply_source_line_id := NULL;
      v_reservation_rec.supply_source_header_id := NULL;
      v_reservation_rec.supply_source_line_detail := NULL;
      v_reservation_rec.supply_source_name := NULL;
      v_reservation_rec.external_source_line_id := NULL;
      v_reservation_rec.external_source_code := NULL;
      v_reservation_rec.autodetail_group_id := NULL;
      v_reservation_rec.demand_source_delivery := NULL;
      inv_reservation_pub.create_reservation
                               (p_api_version_number      => 1.0,
                                x_return_status           => lx_return_status,
                                x_msg_count               => lx_msg_count,
                                x_msg_data                => lx_msg_data,
                                p_rsv_rec                 => v_reservation_rec,
                                p_serial_number           => v_reserv_serial_rec,
                                x_serial_number           => l_reserv_serial_rec,
                                x_quantity_reserved       => v_reservation_quantity,
                                x_reservation_id          => l_reservation_id
                               );

      IF lx_msg_count >= 1
      THEN
         FOR i IN 1 .. lx_msg_count
         LOOP
            l_debug_msg :=
                         fnd_msg_pub.get (p_msg_index      => i,
                                          p_encoded        => 'F');
            fnd_file.put_line (fnd_file.LOG, i || ') ' || l_debug_msg);
         END LOOP;
      ELSE
         fnd_file.put_line (fnd_file.LOG,
                            'Reservation ID Created ' || l_reservation_id
                           );
      END IF;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         l_debug_msg :=
            (   'No Data Found for Item ID  '
             || v_reservation_record.v_inventory_item_id
            );
         xxwc_rental_engine_pkg.log_msg (p_debug_level      => g_level_statement,
                                         p_mod_name         => l_module_name,
                                         p_debug_msg        => l_debug_msg
                                        );
      WHEN TOO_MANY_ROWS
      THEN
         l_debug_msg :=
            (   'Duplicate Data for Item ID '
             || v_reservation_record.v_inventory_item_id
            );
         xxwc_rental_engine_pkg.log_msg (p_debug_level      => g_level_statement,
                                         p_mod_name         => l_module_name,
                                         p_debug_msg        => l_debug_msg
                                        );
      WHEN OTHERS
      THEN
         l_debug_msg :=
            (   'ERROR while Creating Reservation '
             || v_reservation_record.v_inventory_item_id
            );
         xxwc_rental_engine_pkg.log_msg (p_debug_level      => g_level_statement,
                                         p_mod_name         => l_module_name,
                                         p_debug_msg        => l_debug_msg
                                        );
   END create_reservation_line;
END xxwc_rental_engine_pkg;
/