--
-- XXWC_RENTAL_ITEMS_MASS_ADD_PKG  (Package Body) 
--
CREATE OR REPLACE PACKAGE BODY APPS.XXWC_RENTAL_ITEMS_MASS_ADD_PKG
AS
   /*************************************************************************
   *  Copyright (c) 2011 Lucidity Consulting Group
   *  All rights reserved.
   **************************************************************************
   *   $Header XXWC_RENTAL_ITEMS_MASS_ADD_PKG.pks $
   *   Module Name: XXWC_RENTAL_ITEMS_MASS_ADD_PKG.sql
   *
   *   PURPOSE:   This package is for Categorizing Rental Items to FA MASS ADDITIONS
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        09/07/2011  Srinivas Malkapuram      Initial Version
   * ***************************************************************************/



   PROCEDURE MAIN (errbuf             OUT VARCHAR2
                  ,retcode            OUT NUMBER
                  ,p_Log_Enabled   IN     VARCHAR2 DEFAULT 'N')
   IS
      /* Cursor to get the Rental Items to be converted to Assets */

      -- Satish U: Pick Up Only Those Miscallenous transactions whose Attribute9 has 'N' value.

      CURSOR Cur_Get_Rental_Items
      IS
         SELECT mmt.inventory_item_id INVENTORY_ITEM_ID
               ,mmt.organization_id ORGANIZATION_ID
               ,msi.description DESCRIPTION
               ,msi.Segment1 Segment1
               ,mmt.transaction_id
               ,mmt.transfer_transaction_id
               ,mmt.subinventory_code
               ,mmt.transaction_quantity TRANS_QTY
               ,mmt.transfer_subinventory
               ,mmt.attribute_category
               ,mmt.attribute1 Home_Branch
               ,mmt.attribute2 Custodian_Branch
               ,mmt.attribute3 Major_Category
               ,mmt.attribute4 Minor_Category
               ,mmt.attribute5 STATE
               ,mmt.attribute6 CITY
               ,mmt.attribute7 TRANSACTION_VALUE
               ,mmt.attribute10 VENDOR
               ,mmt.attribute11 PONumber
               ,mmt.attribute12 ItemNumber
               ,mmt.attribute13 CERNumber
           FROM mfg_lookups lkup1
               ,mtl_system_items_b msi
               ,mtl_material_transactions mmt
               ,mtl_transaction_types mtt
          WHERE     1 = 1
                AND lkup1.lookup_type = G_TRXN_ACTION_LOOKUP
                AND UPPER (lkup1.meaning) = G_TRANSACTION_ACTION
                AND lkup1.lookup_code = mmt.transaction_action_id
                AND msi.organization_id = mmt.organization_id
                AND msi.inventory_item_id = mmt.inventory_item_id
                AND mmt.transaction_type_id = mtt.transaction_type_id
                AND UPPER (mmt.subinventory_code) =
                       UPPER (G_RENTAL_SUBINV_CODE)
                AND UPPER (mtt.transaction_type_name) = G_TRANSACTION_TYPE
                AND mmt.Attribute9 = 'N';

      ---AND ( UPPER(mmt.attribute9) != 'Y' or mmt.attribute9 IS NULL) ;  Commented Apr062012


      l_asset_category_id   FA_CATEGORIES.CATEGORY_ID%TYPE;
      l_asset_type          FA_CATEGORIES.CATEGORY_TYPE%TYPE;
      l_asset_number        FA_ADDITIONS.ASSET_NUMBER%TYPE;
      l_depreciate_flag     VARCHAR2 (10) := 'Y';
      l_expense_ccid        NUMBER;
      l_asset_ccid          NUMBER;
      l_assets_cost         NUMBER := 0;
      l_location_id         NUMBER;
      l_serial_number       VARCHAR2 (20);
      l_Vendor_Number       NUMBER;


      l_mod_name            VARCHAR2 (50)
                               := 'XXWC_RENTAL_ITEMS_MASS_ADD_PKG.MAIN:';
      l_Commit_Cntr         NUMBER := 0;
      X_Error_msg           VARCHAR2 (2000);
      X_Error_code          VARCHAR2 (80);
      x_Return_status       VARCHAR2 (80);

      l_API_NAME            VARCHAR2 (30) := 'MAIN';
      l_Module_Name         VARCHAR2 (120);
      l_Debug_Msg           VARCHAR2 (2000);
      l_cnt_rental_items    NUMBER;

      -- Satish U: 08-APR-2012
      l_Location_Segment    VARCHAR2 (30);


      --Define user defined exception
      SKIP_RECORD           EXCEPTION;
      VALIDATION_ERROR      EXCEPTION;
   BEGIN
      -- Initialize the status
      x_Return_status := Fnd_Api.g_ret_sts_success;
      l_Module_Name := G_MODULE_NAME || '.' || l_API_NAME;

      l_Debug_Msg := 'START PROGRAM ';
      errbuf := NULL;
      retcode := '0';



      XXWC_RENTAL_ENGINE_PKG.LOG_MSG (p_debug_level   => G_LEVEL_PROCEDURE
                                     ,p_mod_name      => l_Module_Name
                                     ,P_DEBUG_MSG     => l_Debug_Msg);

      SELECT COUNT (*)
        INTO l_cnt_Rental_Items
        FROM mfg_lookups lkup1
            ,mtl_system_items_b msi
            ,mtl_material_transactions mmt
            ,mtl_transaction_types mtt
       WHERE     1 = 1
             AND lkup1.lookup_type = G_TRXN_ACTION_LOOKUP
             AND UPPER (lkup1.meaning) = G_TRANSACTION_ACTION
             AND lkup1.lookup_code = mmt.transaction_action_id
             AND msi.organization_id = mmt.organization_id
             AND msi.inventory_item_id = mmt.inventory_item_id
             AND mmt.transaction_type_id = mtt.transaction_type_id
             AND UPPER (mmt.subinventory_code) = UPPER (G_RENTAL_SUBINV_CODE)
             AND UPPER (mtt.transaction_type_name) = G_TRANSACTION_TYPE
             AND (UPPER (mmt.attribute9) != 'Y' OR mmt.attribute9 IS NULL);

      IF (l_cnt_Rental_Items > 0)
      THEN
         FOR Rental_Items_Rec IN Cur_Get_Rental_Items
         LOOP
            BEGIN
               IF p_Log_Enabled IN ('y', 'Y')
               THEN
                  l_Debug_Msg :=
                     (   '*********Processing for Transaction ID *********'
                      || Rental_Items_Rec.Transaction_ID);
                  XXWC_RENTAL_ENGINE_PKG.LOG_MSG (
                     p_debug_level   => G_LEVEL_PROCEDURE
                    ,p_mod_name      => l_Module_Name
                    ,P_DEBUG_MSG     => l_Debug_Msg);
               END IF;



               l_asset_category_id :=
                  GET_CATEGORY_ID (Rental_Items_Rec.Major_Category
                                  ,Rental_Items_Rec.Minor_Category);

               IF l_asset_category_id IS NULL
               THEN
                  RAISE SKIP_RECORD;
               END IF;

               IF p_Log_Enabled IN ('y', 'Y')
               THEN
                  l_Debug_Msg :=
                     ('Asset Category ID: ' || l_asset_category_id);
                  XXWC_RENTAL_ENGINE_PKG.LOG_MSG (
                     p_debug_level   => G_LEVEL_STATEMENT
                    ,p_mod_name      => l_Module_Name
                    ,P_DEBUG_MSG     => l_Debug_Msg);
               END IF;


               SELECT Rental_Items_Rec.TRANSACTION_VALUE /*NVL(cst_cost_api.get_item_cost(1,
                                                         Rental_Items_Rec.INVENTORY_ITEM_ID,
                                                         Rental_Items_Rec.ORGANIZATION_ID,
                                                         NULL,
                                                         NULL), 0)  */
               INTO l_assets_cost FROM DUAL;

               IF p_Log_Enabled IN ('y', 'Y')
               THEN
                  l_Debug_Msg := ('Asset Cost is: ' || l_assets_cost);
                  XXWC_RENTAL_ENGINE_PKG.LOG_MSG (
                     p_debug_level   => G_LEVEL_STATEMENT
                    ,p_mod_name      => l_Module_Name
                    ,P_DEBUG_MSG     => l_Debug_Msg);
               END IF;



               l_location_id :=
                  Get_Location_Id (Rental_Items_Rec.Home_Branch
                                  ,Rental_Items_Rec.Custodian_Branch
                                  ,G_LOC_SEGMENT3
                                  ,          -- Rental_Items_Rec.LOC_SEGMENT3,
                                   Rental_Items_Rec.STATE
                                  ,Rental_Items_Rec.CITY);



               IF p_Log_Enabled IN ('y', 'Y')
               THEN
                  l_Debug_Msg := ('Location ID: ' || l_location_id);
                  XXWC_RENTAL_ENGINE_PKG.LOG_MSG (
                     p_debug_level   => G_LEVEL_STATEMENT
                    ,p_mod_name      => l_Module_Name
                    ,P_DEBUG_MSG     => l_Debug_Msg);
               END IF;

               IF (l_location_id IS NULL)
               THEN
                  RAISE SKIP_RECORD;
               END IF;

               -- Get Location SEGmetn from the Expense Account of MTL_PARAMETERS

               l_Location_Segment :=
                  GET_LOCATION_SEGMENT (
                     P_Organization_ID => Rental_Items_Rec.Organization_ID);

               IF l_Location_Segment IS NULL
               THEN
                  l_Location_Segment := G_EXP_SEGMENT2;
               END IF;

               l_expense_ccid :=
                  Get_Code_Combination_ID (G_EXP_SEGMENT1
                                          ,l_Location_Segment --- G_EXP_SEGMENT2
                                          ,G_EXP_SEGMENT3
                                          ,G_EXP_SEGMENT4
                                          ,G_EXP_SEGMENT5
                                          ,G_EXP_SEGMENT6
                                          ,G_EXP_SEGMENT7);


               IF p_Log_Enabled IN ('y', 'Y')
               THEN
                  l_Debug_Msg :=
                     ('Expense Code Combination ID: ' || l_expense_ccid);
                  XXWC_RENTAL_ENGINE_PKG.LOG_MSG (
                     p_debug_level   => G_LEVEL_STATEMENT
                    ,p_mod_name      => l_Module_Name
                    ,P_DEBUG_MSG     => l_Debug_Msg);
               END IF;


               IF l_expense_ccid IS NULL
               THEN
                  RAISE SKIP_RECORD;
               END IF;



               IF IS_ITEM_SERIALIZED (Rental_Items_Rec.Transaction_ID
                                     ,Rental_Items_Rec.Inventory_Item_ID)
               THEN
                  l_Serial_Number :=
                     GET_SERIAL_NUMBER (Rental_Items_Rec.Transaction_ID);

                  IF p_Log_Enabled IN ('y', 'Y')
                  THEN
                     l_Debug_Msg := ('Serial Number is: ' || l_serial_number);
                     XXWC_RENTAL_ENGINE_PKG.LOG_MSG (
                        p_debug_level   => G_LEVEL_STATEMENT
                       ,p_mod_name      => l_Module_Name
                       ,P_DEBUG_MSG     => l_Debug_Msg);
                  END IF;
               END IF;

               /* If Dynamic Insert is turned OFF for AssetKey Flexfield, then Asset Key needs to be Populated */
               -- 21-FEB-23012 : Added  Item Number to the Description Column as per Bob's Request
               BEGIN
                  INSERT INTO fa_mass_additions (ACCOUNTING_DATE
                                                ,ASSET_CATEGORY_ID
                                                ,ASSET_TYPE
                                                -- ,ASSET_KEY_CCID
                                                ,BOOK_TYPE_CODE
                                                ,CREATED_BY
                                                ,CREATION_DATE
                                                ,DATE_PLACED_IN_SERVICE
                                                ,DEPRECIATE_FLAG
                                                ,DESCRIPTION
                                                ,EXPENSE_CODE_COMBINATION_ID
                                                ,FIXED_ASSETS_COST
                                                ,FIXED_ASSETS_UNITS
                                                ,LAST_UPDATE_DATE
                                                ,LAST_UPDATE_LOGIN
                                                ,LAST_UPDATED_BY
                                                ,LOCATION_ID
                                                ,MASS_ADDITION_ID
                                                ,PAYABLES_CODE_COMBINATION_ID
                                                ,PAYABLES_COST
                                                ,PAYABLES_UNITS
                                                ,POSTING_STATUS
                                                ,QUEUE_NAME
                                                ,SERIAL_NUMBER
                                                --,VENDOR_NUMBER
                                                ,attribute8
                                                ,attribute4
                                                ,attribute5         --PONumber
                                                ,attribute6        --CERNumber
                                                ,attribute7           --Vendor
                                                           )
                       VALUES (
                                 SYSDATE
                                ,l_asset_category_id
                                ,G_ASSET_TYPE
                                --  ,l_asset_ccid
                                ,G_BOOKTYPE_CODE             --'HDS WHITE CAP'
                                ,G_USER_ID
                                ,SYSDATE
                                ,SYSDATE
                                ,l_depreciate_flag
                                ,   Rental_Items_Rec.DESCRIPTION
                                 || '~'
                                 || Rental_Items_Rec.Segment1
                                 || '~'
                                 || Rental_Items_Rec.Transaction_id
                                ,l_expense_ccid
                                ,l_assets_cost             --Fixed Assets Cost
                                ,Rental_Items_Rec.TRANS_QTY
                                ,SYSDATE
                                ,G_USER_ID
                                ,G_USER_ID
                                ,l_location_id                         --13724
                                ,fa_mass_additions_s.NEXTVAL
                                ,NULL
                                ,l_assets_cost                 --Payables Cost
                                ,Rental_Items_Rec.TRANS_QTY
                                ,G_POSTING_STATUS
                                ,G_QUEUE_NAME
                                ,l_serial_number
                                --,l_Vendor_Number
                                ,l_Location_Segment
                                ,Rental_Items_Rec.ItemNumber
                                ,Rental_Items_Rec.PONumber
                                ,Rental_Items_Rec.CERNumber
                                ,Rental_Items_Rec.VENDOR);

                  l_Debug_Msg :=
                     (   '*********Successfully Inserted Transaction *********'
                      || Rental_Items_Rec.Transaction_ID);
                  XXWC_RENTAL_ENGINE_PKG.LOG_MSG (
                     p_debug_level   => G_LEVEL_STATEMENT
                    ,p_mod_name      => l_Module_Name
                    ,P_DEBUG_MSG     => l_Debug_Msg);

                  UPDATE MTL_MATERIAL_TRANSACTIONS
                     SET ATTRIBUTE9 = 'Y'
                   WHERE TRANSACTION_ID = Rental_Items_Rec.Transaction_ID;

                  COMMIT;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     l_Debug_Msg :=
                        (   'ERROR Inserting Record '
                         || l_asset_number
                         || ' into FA MASS ADDITIONS '
                         || SQLERRM);
                     XXWC_RENTAL_ENGINE_PKG.LOG_MSG (
                        p_debug_level   => G_LEVEL_STATEMENT
                       ,p_mod_name      => l_Module_Name
                       ,P_DEBUG_MSG     => l_Debug_Msg);
                     ROLLBACK;
                     retcode := '1';
                     RAISE SKIP_RECORD;
               END;
            EXCEPTION
               WHEN SKIP_RECORD
               THEN
                  l_Debug_Msg :=
                     (   '*********Skipping further Processing of Transaction ID *********'
                      || Rental_Items_Rec.Transaction_ID);
                  XXWC_RENTAL_ENGINE_PKG.LOG_MSG (
                     p_debug_level   => G_LEVEL_STATEMENT
                    ,p_mod_name      => l_Module_Name
                    ,P_DEBUG_MSG     => l_Debug_Msg);
                  retcode := '1';
               WHEN NO_DATA_FOUND
               THEN
                  l_Debug_Msg :=
                     (   '*********Skipping further Processing of Transaction ID *********'
                      || Rental_Items_Rec.Transaction_ID);
                  XXWC_RENTAL_ENGINE_PKG.LOG_MSG (
                     p_debug_level   => G_LEVEL_STATEMENT
                    ,p_mod_name      => l_Module_Name
                    ,P_DEBUG_MSG     => l_Debug_Msg);
                  retcode := '1';
                  errbuf := l_Debug_Msg;
               WHEN OTHERS
               THEN
                  l_Debug_Msg := SQLERRM;
                  XXWC_RENTAL_ENGINE_PKG.LOG_MSG (
                     p_debug_level   => G_LEVEL_STATEMENT
                    ,p_mod_name      => l_Module_Name
                    ,P_DEBUG_MSG     => l_Debug_Msg);
                  retcode := '1';
                  errbuf := SQLERRM;
                  ROLLBACK;
            END;
         END LOOP;
      ELSE
         l_Debug_Msg := ('*********NO Records Found to Process *********');
         XXWC_RENTAL_ENGINE_PKG.LOG_MSG (p_debug_level   => G_LEVEL_STATEMENT
                                        ,p_mod_name      => l_Module_Name
                                        ,P_DEBUG_MSG     => l_Debug_Msg);
         retcode := '1';
         errbuf := l_Debug_Msg;
      END IF;
   --END;

   END MAIN;


   FUNCTION GET_CATEGORY_ID (p_segment1 IN VARCHAR2, p_segment2 IN VARCHAR2)
      RETURN NUMBER
   IS
      l_category_id   NUMBER;

      l_API_NAME      VARCHAR2 (30) := 'GET_CATEGORY_ID';
      l_Module_Name   VARCHAR2 (120);
      l_Debug_Msg     VARCHAR2 (2000);
   BEGIN
      l_Module_Name := G_MODULE_NAME || '.' || l_API_NAME;

      SELECT CATEGORY_ID
        INTO l_category_id
        FROM FA_CATEGORIES
       WHERE     SEGMENT1 = p_segment1
             AND SEGMENT2 = p_segment2
             AND ENABLED_FLAG = 'Y';


      RETURN l_category_id;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         l_Debug_Msg :=
               'No FA Category Found for '
            || p_segment1
            || ' and '
            || p_segment2;
         XXWC_RENTAL_ENGINE_PKG.LOG_MSG (p_debug_level   => G_LEVEL_PROCEDURE
                                        ,p_mod_name      => l_Module_Name
                                        ,P_DEBUG_MSG     => l_Debug_Msg);
         RETURN NULL;
      WHEN TOO_MANY_ROWS
      THEN
         l_Debug_Msg :=
               'Duplicate FA Categories Found for '
            || p_segment1
            || ' and '
            || p_segment2;
         XXWC_RENTAL_ENGINE_PKG.LOG_MSG (p_debug_level   => G_LEVEL_PROCEDURE
                                        ,p_mod_name      => l_Module_Name
                                        ,P_DEBUG_MSG     => l_Debug_Msg);
         RETURN NULL;
      WHEN OTHERS
      THEN
         l_Debug_Msg :=
               'ERROR: while trying to Obtain FA Categories for '
            || p_segment1
            || ' and '
            || p_segment2;
         XXWC_RENTAL_ENGINE_PKG.LOG_MSG (p_debug_level   => G_LEVEL_PROCEDURE
                                        ,p_mod_name      => l_Module_Name
                                        ,P_DEBUG_MSG     => l_Debug_Msg);
         RETURN NULL;
   END;

   FUNCTION GET_LOCATION_ID (p_segment1   IN VARCHAR2
                            ,p_segment2   IN VARCHAR2
                            ,p_segment3   IN VARCHAR2
                            ,p_segment4   IN VARCHAR2
                            ,p_segment5   IN VARCHAR2)
      RETURN NUMBER
   IS
      l_location_id   NUMBER;
      l_API_NAME      VARCHAR2 (30) := 'GET_CATEGORY_ID';
      l_Module_Name   VARCHAR2 (120);
      l_Debug_Msg     VARCHAR2 (2000);
   BEGIN
      l_Module_Name := G_MODULE_NAME || '.' || l_API_NAME;


      SELECT NVL (location_id, 0)
        INTO l_location_id
        FROM FA_LOCATIONS
       WHERE     segment1 = p_segment1
             AND segment2 = p_segment2
             AND UPPER (segment3) = UPPER (p_segment3)
             AND UPPER (segment4) = UPPER (p_segment4)
             AND UPPER (segment5) = UPPER (p_segment5);

      RETURN l_location_id;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         l_Debug_Msg :=
               'No Location Found in FA_LOCATIONS for Segments: '
            || p_segment1
            || ', '
            || p_segment2
            || ', '
            || p_segment3
            || ', '
            || p_segment4
            || ', '
            || p_segment5;
         XXWC_RENTAL_ENGINE_PKG.LOG_MSG (p_debug_level   => G_LEVEL_STATEMENT
                                        ,p_mod_name      => l_Module_Name
                                        ,P_DEBUG_MSG     => l_Debug_Msg);
         RETURN NULL;
      WHEN TOO_MANY_ROWS
      THEN
         l_Debug_Msg :=
               'Duplicate FA Categories Found for Segments: '
            || p_segment1
            || ', '
            || p_segment2
            || ', '
            || p_segment3
            || ', '
            || p_segment4
            || ', '
            || p_segment5;
         XXWC_RENTAL_ENGINE_PKG.LOG_MSG (p_debug_level   => G_LEVEL_STATEMENT
                                        ,p_mod_name      => l_Module_Name
                                        ,P_DEBUG_MSG     => l_Debug_Msg);
         RETURN NULL;
      WHEN OTHERS
      THEN
         l_Debug_Msg :=
               'ERROR: while trying to Obtain FA Categories for Segments: '
            || p_segment1
            || ', '
            || p_segment2
            || ', '
            || p_segment3
            || ', '
            || p_segment4
            || ', '
            || p_segment5;
         XXWC_RENTAL_ENGINE_PKG.LOG_MSG (p_debug_level   => G_LEVEL_STATEMENT
                                        ,p_mod_name      => l_Module_Name
                                        ,P_DEBUG_MSG     => l_Debug_Msg);
         RETURN NULL;
   END;


   FUNCTION GET_CODE_COMBINATION_ID (p_segment1   IN VARCHAR2
                                    ,p_segment2   IN VARCHAR2
                                    ,p_segment3   IN VARCHAR2
                                    ,p_segment4   IN VARCHAR2
                                    ,p_segment5   IN VARCHAR2
                                    ,p_segment6   IN VARCHAR2
                                    ,p_segment7   IN VARCHAR2)
      RETURN NUMBER
   IS
      l_ccid          NUMBER;
      l_API_NAME      VARCHAR2 (30) := 'GET_CODE_COMBINATION_ID';
      l_Module_Name   VARCHAR2 (120);
      l_Debug_Msg     VARCHAR2 (2000);
   BEGIN
      l_Module_Name := G_MODULE_NAME || '.' || l_API_NAME;


      SELECT code_combination_id
        INTO l_ccid
        FROM GL_CODE_COMBINATIONS
       WHERE     segment1 = p_segment1
             AND segment2 = p_segment2
             AND segment3 = p_segment3
             AND segment4 = p_segment4
             AND segment5 = p_segment5
             AND segment6 = p_segment6
             AND segment7 = p_segment7;

      RETURN l_ccid;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         l_Debug_Msg :=
               'No Code Combination ID found for '
            || p_segment1
            || ', '
            || p_segment2
            || ', '
            || p_segment3
            || ', '
            || p_segment4
            || ', '
            || p_segment5
            || ', '
            || p_segment6;
         XXWC_RENTAL_ENGINE_PKG.LOG_MSG (p_debug_level   => G_LEVEL_PROCEDURE
                                        ,p_mod_name      => l_Module_Name
                                        ,P_DEBUG_MSG     => l_Debug_Msg);
         RETURN NULL;
      WHEN TOO_MANY_ROWS
      THEN
         l_Debug_Msg :=
               'Duplicate Code Combinations found for '
            || p_segment1
            || ', '
            || p_segment2
            || ', '
            || p_segment3
            || ', '
            || p_segment4
            || ', '
            || p_segment5
            || ', '
            || p_segment6;
         XXWC_RENTAL_ENGINE_PKG.LOG_MSG (p_debug_level   => G_LEVEL_PROCEDURE
                                        ,p_mod_name      => l_Module_Name
                                        ,P_DEBUG_MSG     => l_Debug_Msg);
         RETURN NULL;
      WHEN OTHERS
      THEN
         l_Debug_Msg :=
               'ERROR: while trying to Obtain Code Combination for '
            || p_segment1
            || ', '
            || p_segment2
            || ', '
            || p_segment3
            || ', '
            || p_segment4
            || ', '
            || p_segment5
            || ', '
            || p_segment6;
         XXWC_RENTAL_ENGINE_PKG.LOG_MSG (p_debug_level   => G_LEVEL_PROCEDURE
                                        ,p_mod_name      => l_Module_Name
                                        ,P_DEBUG_MSG     => l_Debug_Msg);
         RETURN NULL;
   END;


   FUNCTION GET_SERIAL_NUMBER (p_Transfer_Trx_ID IN NUMBER)
      RETURN VARCHAR2
   IS
      l_serial_number   VARCHAR2 (20);
      l_API_NAME        VARCHAR2 (30) := 'GET_SERIAL_NUMBER';
      l_Module_Name     VARCHAR2 (120);
      l_Debug_Msg       VARCHAR2 (2000);
   BEGIN
      l_Module_Name := G_MODULE_NAME || '.' || l_API_NAME;

      SELECT serial_number
        INTO l_serial_number
        FROM MTL_SERIAL_NUMBERS msn
       WHERE msn.last_transaction_id = p_Transfer_Trx_ID;

      RETURN l_serial_number;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         --l_Debug_Msg := 'No Serial Number found for Transaction SET ID '||p_Transfer_Trx_ID;
         /* XXWC_RENTAL_ENGINE_PKG.LOG_MSG(p_debug_level   => G_LEVEL_PROCEDURE,
                                     p_mod_name      => l_Module_Name,
                                     P_DEBUG_MSG     => l_Debug_Msg );  */
         RETURN NULL;
      WHEN TOO_MANY_ROWS
      THEN
         l_Debug_Msg :=
               'Duplicate Serial Numbers found for Transaction SET ID '
            || p_Transfer_Trx_ID;
         XXWC_RENTAL_ENGINE_PKG.LOG_MSG (p_debug_level   => G_LEVEL_PROCEDURE
                                        ,p_mod_name      => l_Module_Name
                                        ,P_DEBUG_MSG     => l_Debug_Msg);
         RETURN NULL;
      WHEN OTHERS
      THEN
         l_Debug_Msg :=
               'ERROR: while trying to Obtain Serial Number Transaction SET ID '
            || p_Transfer_Trx_ID;
         XXWC_RENTAL_ENGINE_PKG.LOG_MSG (p_debug_level   => G_LEVEL_PROCEDURE
                                        ,p_mod_name      => l_Module_Name
                                        ,P_DEBUG_MSG     => l_Debug_Msg);
         RETURN NULL;
   END;


   --- FUNCTION GET_LOCATION_SEGMENT (Organization_ID Numnber )  Return  Varchar2
   --- Function Returns  Expnese Account Location Segment for a given Organization
   FUNCTION GET_LOCATION_SEGMENT (P_Organization_ID IN NUMBER)
      RETURN VARCHAR2
   IS
      l_Expense_Account    NUMBER;
      l_Location_Segment   VARCHAR2 (30);
   BEGIN
      SELECT Expense_Account
        INTO l_Expense_Account
        FROM MTL_PARAMETERS_VIEW
       WHERE Organization_ID = P_Organization_ID;

      IF l_Expense_Account IS NOT NULL
      THEN
         SELECT Segment2
           INTO l_Location_Segment
           FROM GL_CODE_COMBINATIONS
          WHERE code_Combination_ID = l_Expense_Account;

         RETURN l_Location_Segment;
      ELSE
         RETURN NULL;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END GET_LOCATION_SEGMENT;

   FUNCTION IS_ITEM_SERIALIZED (p_Transfer_Trx_ID     IN NUMBER
                               ,p_Inventory_Item_ID   IN NUMBER)
      RETURN BOOLEAN
   IS
      l_API_NAME         VARCHAR2 (30) := 'IS_ITEM_SERIALIZED';
      l_Module_Name      VARCHAR2 (120);
      l_Debug_Msg        VARCHAR2 (2000);
      l_serialized_cnt   NUMBER := 0;
   BEGIN
      l_Module_Name := G_MODULE_NAME || '.' || l_API_NAME;

      SELECT COUNT (serial_number)
        INTO l_serialized_cnt
        FROM MTL_SERIAL_NUMBERS msn
       WHERE     msn.last_transaction_id = p_Transfer_Trx_ID
             AND msn.inventory_item_id = p_Inventory_Item_ID;

      IF l_serialized_cnt = 0
      THEN
         RETURN FALSE;
      ELSE
         RETURN TRUE;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         l_Debug_Msg :=
               'ERROR: While checking if Item is Serialized '
            || p_Transfer_Trx_ID;
         XXWC_RENTAL_ENGINE_PKG.LOG_MSG (p_debug_level   => G_LEVEL_PROCEDURE
                                        ,p_mod_name      => l_Module_Name
                                        ,P_DEBUG_MSG     => l_Debug_Msg);
   END;


   FUNCTION GET_VENDOR_NUMBER (p_VendorName IN VARCHAR2)
      RETURN NUMBER
   IS
      l_vendor_number   VARCHAR2 (20);
      l_API_NAME        VARCHAR2 (30) := 'GET_VENDOR_NUMBER';
      l_Module_Name     VARCHAR2 (120);
      l_Debug_Msg       VARCHAR2 (2000);
   BEGIN
      l_Module_Name := G_MODULE_NAME || '.' || l_API_NAME;

      SELECT pv.segment1
        INTO l_vendor_number
        FROM po_vendors pv
       WHERE     UPPER (pv.vendor_name) = UPPER (p_VendorName)
             AND ENABLED_FLAG = 'Y';

      RETURN l_vendor_number;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         l_Debug_Msg := 'No Vendor Number found for Vendor ' || p_VendorName;
         XXWC_RENTAL_ENGINE_PKG.LOG_MSG (p_debug_level   => G_LEVEL_PROCEDURE
                                        ,p_mod_name      => l_Module_Name
                                        ,P_DEBUG_MSG     => l_Debug_Msg);
         RETURN NULL;
      WHEN TOO_MANY_ROWS
      THEN
         l_Debug_Msg :=
            'Duplicate Vendor Numbers found for Vendor ' || p_VendorName;
         XXWC_RENTAL_ENGINE_PKG.LOG_MSG (p_debug_level   => G_LEVEL_PROCEDURE
                                        ,p_mod_name      => l_Module_Name
                                        ,P_DEBUG_MSG     => l_Debug_Msg);
         RETURN NULL;
      WHEN OTHERS
      THEN
         l_Debug_Msg :=
               'ERROR: while trying to Obtain Vendor Number for '
            || p_VendorName;
         XXWC_RENTAL_ENGINE_PKG.LOG_MSG (p_debug_level   => G_LEVEL_PROCEDURE
                                        ,p_mod_name      => l_Module_Name
                                        ,P_DEBUG_MSG     => l_Debug_Msg);
         RETURN NULL;
   END;
END;
/

