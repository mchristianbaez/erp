CREATE OR REPLACE PACKAGE BODY APPS.XXWC_INV_AGILITY_EXTRACT_PKG
IS
   g_err_callfrom   VARCHAR2 (1000) := 'XXWC_INV_AGILITY_EXTRACT_PKG';
   g_module         VARCHAR2 (100) := 'XXWC';
   g_distro_list    VARCHAR2 (80) := 'wc-itdevalerts-u1@hdsupply.com';
   g_host           VARCHAR2 (256) := 'mailoutrelay.hdsupply.net';
   g_hostport       VARCHAR2 (20) := '25';
   g_filepath       VARCHAR2 (1000);

   gvc_file         UTL_FILE.file_type;

   /**************************************************************************************************************************************
        $Header XXWC_INV_AGILITY_EXTRACT_PKG $
        Module Name: XXWC_INV_AGILITY_EXTRACT_PKG.pks

        PURPOSE:   This package is called by the concurrent programs
                   Inventory data Extract for Agility system.
        REVISIONS:
        Ver        Date        Author             Description
        ---------  ----------  ---------------   ----------------------------------------------------------------------------------------
         1.0       01/09/2018  P.Vamshidhar      Initial Build - Task ID: 20171101-00037 - Agility PIM Project and Data Transfer from ERP
         2.0       06-Mar-2018 Niraj K Ranjan    TMS#20171116-00176   Agility PIM Project Catclass Structure
      ***********************************************************************************************************************************/

   PROCEDURE write_log (p_debug_msg IN VARCHAR2)
   IS
   /**************************************************************************************************************************************
     Procedure : Write_Log

     PURPOSE:   This procedure logs debug message in Concurrent Log file
     Parameter:
            IN p_debug_msg      -- Debug Message
        REVISIONS:
        Ver        Date        Author             Description
        ---------  ----------  ---------------   ----------------------------------------------------------------------------------------
         1.0       01/09/2018  P.Vamshidhar      Initial Build - Task ID: 20171101-00037 - Agility PIM Project and Data Transfer from ERP
   ****************************************************************************************************************************************/
   BEGIN
      fnd_file.put_line (fnd_file.LOG, p_debug_msg);
      DBMS_OUTPUT.put_line (p_debug_msg);
   END write_log;



   PROCEDURE main (x_errbuf            OUT VARCHAR2,
                   x_retcode           OUT VARCHAR2,
                   p_extract_type   IN     VARCHAR2)
   IS
   /***********************************************************************************************************************************
     Procedure : main

     PURPOSE   :Main procedure.
     REVISIONS:
     Ver        Date        Author             Description
     ---------  ----------  ---------------   ----------------------------------------------------------------------------------------
      1.0       01/09/2018  P.Vamshidhar      Initial Build - Task ID: 20171101-00037 - Agility PIM Project and Data Transfer from ERP
   ***********************************************************************************************************************************/
   BEGIN
      IF p_extract_type = 'Inactive'
      THEN
         Item_status_extract;
      ELSE
         IF p_extract_type = 'Delta'
         THEN
            DELTA_EXTRACT;
         ELSIF p_extract_type = 'Full'
         THEN
            full_extract;
         END IF;
      END IF;
   END;

   PROCEDURE DELTA_EXTRACT
   IS
      /***********************************************************************************************************************************
        Procedure : delta_extract

        PURPOSE   :delta_extract procedure .
        REVISIONS:
        Ver        Date        Author             Description
        ---------  ----------  ---------------   ----------------------------------------------------------------------------------------
         1.0       01/09/2018  P.Vamshidhar      Initial Build - Task ID: 20171101-00037 - Agility PIM Project and Data Transfer from ERP
      ***********************************************************************************************************************************/

      ld_last_extract     DATE := TRUNC (SYSDATE) - 1;
      ln_request_id       FND_CONCURRENT_REQUESTS.REQUEST_ID%TYPE
                             := FND_GLOBAL.CONC_REQUEST_ID;

      CURSOR cur_delta
      IS
           SELECT inventory_item_id, ROWID item_rowid
             FROM XXWC.XXWC_INV_AGILITY_EXTRACT_T
            WHERE PROCESS_FLAG = 'NEW'
         ORDER BY INVENTORY_ITEM_ID;

      lvc_outbound_dir    VARCHAR2 (1000) := 'XXWC_INV_AGILITY_FEED_DIR';
      lvc_outbound_file   VARCHAR2 (1000);
      ln_count            NUMBER;
      l_sec               VARCHAR2 (32767);
      lvc_procedure       VARCHAR2 (100) := 'DELTA_EXTRACT';
      lvc_env             VARCHAR2 (100);
   BEGIN
      l_sec := 'Deriving File Name';

      SELECT UPPER (name) INTO lvc_env FROM v$database;

      IF lvc_env LIKE 'EBSPRD'
      THEN
         lvc_outbound_file :=
               'XXWC_Agility_Delta_Extract_'
            || TO_CHAR (SYSDATE, 'MMDDYYYYHH24MISS')
            || '.txt';
      ELSE
         lvc_outbound_file :=
               'XXWC_'
            || SUBSTR (lvc_env, INSTR (lvc_env, 'EBS', 1) + 3)
            || '_Agility_Delta_Extract_'
            || TO_CHAR (SYSDATE, 'MMDDYYYYHH24MISS')
            || '.txt';
      END IF;

      BEGIN
         SELECT directory_path
           INTO g_filepath
           FROM dba_directories
          WHERE directory_name = lvc_outbound_dir;
      EXCEPTION
         WHEN OTHERS
         THEN
            g_filepath := NULL;
      END;



      l_sec := ' Deleting Data from custom table';

      DELETE FROM XXWC.XXWC_INV_AGILITY_EXTRACT_T
            WHERE CREATION_DATE <= ld_last_extract;

      FND_FILE.PUT_LINE (FND_FILE.LOG,
                         'Numer of Rows Deleted :' || SQL%ROWCOUNT);

      COMMIT;

      l_sec := ' Insertring Data from custom table';

      INSERT INTO                                                  /*APPEND+*/
                 XXWC.XXWC_INV_AGILITY_EXTRACT_T (INVENTORY_ITEM_ID,
                                                  ITEM_TYPE,
                                                  ITEM_STATUS,
                                                  CREATION_DATE,
                                                  PROCESS_FLAG)
         (SELECT MSIB.INVENTORY_ITEM_ID,
                 MSIB.ITEM_TYPE,
                 MSIB.INVENTORY_ITEM_STATUS_CODE,
                 MSIB.CREATION_DATE,
                 'NEW'
            FROM APPS.MTL_SYSTEM_ITEMS_B MSIB
           WHERE     MSIB.CREATION_DATE > ld_last_extract
                 AND MSIB.ORGANIZATION_ID = 222
                 AND MSIB.ITEM_TYPE NOT IN ('RENTAL', 'RE_RENT', 'SPECIAL')
                 AND MSIB.INVENTORY_ITEM_STATUS_CODE NOT IN ('Inactive',
                                                             'New Item')
                 AND NOT EXISTS
                        (SELECT 1
                           FROM XXWC.XXWC_INV_AGILITY_EXTRACT_T A
                          WHERE A.INVENTORY_ITEM_ID = MSIB.INVENTORY_ITEM_ID));

      FND_FILE.PUT_LINE (FND_FILE.LOG,
                         'Numer of Rows Inserted :' || SQL%ROWCOUNT);
      COMMIT;

      SELECT COUNT (1)
        INTO ln_count
        FROM XXWC.XXWC_INV_AGILITY_EXTRACT_T
       WHERE PROCESS_FLAG = 'NEW';

      FND_FILE.PUT_LINE (FND_FILE.LOG,
                         'Number of Rows to print :' || ln_count);

      l_sec := 'Before Print item data';

      IF ln_count > 0
      THEN
         l_sec := 'Openign Data File';
         gvc_file :=
            UTL_FILE.fopen (LOCATION       => lvc_outbound_dir,
                            filename       => lvc_outbound_file,
                            open_mode      => 'W',
                            max_linesize   => 32767);

         l_sec := 'Printing Data file column titles';
         UTL_FILE.put_line (
            gvc_file,
            '|ITEM_NUMBER|ITEM_DESCRIPTION| ITEM_LONG_DESCRIPTION|MANUFACTURER_NAME|VENDOR_PART_NUMBER|UPC_NUMBER|INV_CAT_CLASS|UNSPSC_CODE|UOM|COUNTRY_OF_ORIGIN|HAZARDOUS_MATERIAL|HAZARD_CLASS|SHELF_LIFE_DAYS|PART_TYPE|UN_NUMBER|HAZMAT_CONTAINER|MSDS_NO|ORMD_FLAG|HAZMAT_PACKAGING_GROUP|HAZMAT_DESCRIPTION|PESTICIDE_FLAG|PESTICIDE_FLAG_STATE|CA_PROP_65|VOC_GL|VOC_CATEGORY|VOC_SUB_CATEGORY|TAXWARE_CODE|UNIT_WEIGHT|ITEM_STATUS|WEBSITE_ITEM|WC_WEB_HIERARCHY|WEB_MFG_PART_NUMBER|CYBERSOURCE_TAX_CODE|WEB_SHORT_DESCRIPTION|WEB_LONG_DESCRIPTION|BRAND|PRODUCT_LINE|WEB_SEQUENCE|WEB_KEYWORDS|BULKY_ITEM|FIXED_LOT_MULTIPLIER|INVENTORY_ITEM_FLAG|INVOICE_ENABLED_FLAG|PURCHASABLE|STOCKABLE |TRANSACTABLE|');

         l_sec := 'Printing Data';

         FOR REC_DELTA IN CUR_DELTA
         LOOP
            l_sec := rec_delta.inventory_item_id || ' Proess Starts';
            print_item_data (rec_delta.inventory_item_id);

            l_sec :=
                  'Printing item Data: '
               || rec_delta.inventory_item_id
               || ' Done ';

            UPDATE xxwc.xxwc_inv_agility_extract_t
               SET process_flag = 'PROCESSED'
             WHERE ROWID = rec_delta.item_rowid;

            l_sec := 'Updating proessed data: ' || rec_delta.inventory_item_id;
         END LOOP;

         COMMIT;
         UTL_FILE.FCLOSE (gvc_file);

         -- File Permissions change.
         IF g_filepath IS NOT NULL
         THEN
            XXWC_COMMON_UTILITIES_PKG.FILE_PERMISSIONS_CHNG (
               g_filepath,
               lvc_outbound_file);
         END IF;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || lvc_procedure,
            p_calling             => l_sec,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          => NULL,
            p_distribution_list   => g_distro_list,
            p_module              => g_module);
   END;

   PROCEDURE FULL_EXTRACT
   IS
      /***********************************************************************************************************************************
        Procedure : full_extract

        PURPOSE   :full_extract procedure .
        REVISIONS:
        Ver        Date        Author             Description
        ---------  ----------  ---------------   ----------------------------------------------------------------------------------------
         1.0       01/09/2018  P.Vamshidhar      Initial Build - Task ID: 20171101-00037 - Agility PIM Project and Data Transfer from ERP
      ***********************************************************************************************************************************/
      lvc_outbound_dir    VARCHAR2 (1000) := 'XXWC_INV_AGILITY_FEED_DIR';
      lvc_outbound_file   VARCHAR2 (1000);
      lvc_env             VARCHAR2 (10);
      l_sec               VARCHAR2 (32767);

      CURSOR cur_items
      IS
         SELECT INVENTORY_ITEM_ID, INVENTORY_ITEM_STATUS_CODE
           FROM APPS.MTL_SYSTEM_ITEMS_B
          WHERE ORGANIZATION_ID = 222;

      ln_item_count       NUMBER := 0;
      lvc_procedure       VARCHAR2 (100) := 'FULL_EXTRACT';
   BEGIN
      l_sec := 'Generating File Name';

      SELECT UPPER (name) INTO lvc_env FROM v$database;

      IF lvc_env LIKE 'EBSPRD'
      THEN
         lvc_outbound_file :=
               'XXWC_Agility_Full_Extract_'
            || TO_CHAR (SYSDATE, 'MMDDYYYYHH24MISS')
            || '.txt';
      ELSE
         lvc_outbound_file :=
               'XXWC_'
            || SUBSTR (lvc_env, INSTR (lvc_env, 'EBS', 1) + 3)
            || '_Agility_Full_Extract_'
            || TO_CHAR (SYSDATE, 'MMDDYYYYHH24MISS')
            || '.txt';
      END IF;

      BEGIN
         SELECT directory_path
           INTO g_filepath
           FROM dba_directories
          WHERE directory_name = lvc_outbound_dir;
      EXCEPTION
         WHEN OTHERS
         THEN
            g_filepath := NULL;
      END;

      l_sec := 'Opening Data File';
      gvc_file :=
         UTL_FILE.fopen (LOCATION       => lvc_outbound_dir,
                         filename       => lvc_outbound_file,
                         open_mode      => 'W',
                         max_linesize   => 32767);

      l_sec := 'Printing Data file column titles';
      UTL_FILE.put_line (
         gvc_file,
         '|ITEM_NUMBER|ITEM_DESCRIPTION| ITEM_LONG_DESCRIPTION|MANUFACTURER_NAME|VENDOR_PART_NUMBER|UPC_NUMBER|INV_CAT_CLASS|UNSPSC_CODE|UOM|COUNTRY_OF_ORIGIN|HAZARDOUS_MATERIAL|HAZARD_CLASS|SHELF_LIFE_DAYS|PART_TYPE|UN_NUMBER|HAZMAT_CONTAINER|MSDS_NO|ORMD_FLAG|HAZMAT_PACKAGING_GROUP|HAZMAT_DESCRIPTION|PESTICIDE_FLAG|PESTICIDE_FLAG_STATE|CA_PROP_65|VOC_GL|VOC_CATEGORY|VOC_SUB_CATEGORY|TAXWARE_CODE|UNIT_WEIGHT|ITEM_STATUS|WEBSITE_ITEM|WC_WEB_HIERARCHY|WEB_MFG_PART_NUMBER|CYBERSOURCE_TAX_CODE|WEB_SHORT_DESCRIPTION|WEB_LONG_DESCRIPTION|BRAND|PRODUCT_LINE|WEB_SEQUENCE|WEB_KEYWORDS|BULKY_ITEM|FIXED_LOT_MULTIPLIER|INVENTORY_ITEM_FLAG|INVOICE_ENABLED_FLAG|PURCHASABLE|STOCKABLE |TRANSACTABLE|');

      l_sec := 'Printing Data';

      FOR rec_items IN cur_items
      LOOP
         l_sec := rec_items.inventory_item_id || ' Item process start';

         IF rec_items.inventory_item_status_code NOT IN ('New Item',
                                                         'Inactive')
         THEN
            l_sec := rec_items.inventory_item_id || ' inside loop';
            ln_item_count := ln_item_count + 1;
            print_item_data (rec_items.inventory_item_id);
            l_sec := rec_items.inventory_item_id || 'Print completed';
         END IF;
      END LOOP;

      UTL_FILE.FCLOSE (gvc_file);

      -- File Permissions change.
      IF g_filepath IS NOT NULL
      THEN
         XXWC_COMMON_UTILITIES_PKG.FILE_PERMISSIONS_CHNG (g_filepath,
                                                          lvc_outbound_file);
      END IF;

      DBMS_OUTPUT.PUT_LINE ('Number Items Data Extracted ' || ln_item_count);
   EXCEPTION
      WHEN OTHERS
      THEN
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || lvc_procedure,
            p_calling             => l_sec,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          => NULL,
            p_distribution_list   => g_distro_list,
            p_module              => g_module);
   END;


   PROCEDURE Item_status_extract
   IS
      /***********************************************************************************************************************************
        Procedure : Item_status_extract

        PURPOSE   :Item_status_extract procedure .
        REVISIONS:
        Ver        Date        Author             Description
        ---------  ----------  ---------------   ----------------------------------------------------------------------------------------
         1.0       01/09/2018  P.Vamshidhar      Initial Build - Task ID: 20171101-00037 - Agility PIM Project and Data Transfer from ERP
      ***********************************************************************************************************************************/
      lvc_outbound_dir    VARCHAR2 (1000) := 'XXWC_INV_AGILITY_FEED_DIR';
      lvc_outbound_file   VARCHAR2 (1000);
      lvc_env             VARCHAR2 (100);

      l_sec               VARCHAR2 (32767);
      lvc_procedure       VARCHAR2 (100) := 'ITEM_STATUS_EXTRACT';

      CURSOR cur_items
      IS
         SELECT MSIB.SEGMENT1 ITEM_NUMBER,
                REGEXP_REPLACE (REPLACE (msib.description, '|', '/'),
                                '[[:cntrl:]]')
                   ITEM_DESCRIPTION,
                REGEXP_REPLACE (REPLACE (msib.long_description, '|', '/'),
                                '[[:cntrl:]]')
                   ITEM_LONG_DESCRIPTION,
                NULL MANUFACTURER_NAME,
                NULL VENDOR_PART_NUMBER,
                NULL UPC,
                NULL INV_CAT_CLASS,
                NULL UNSPSC_CODE,
                MSIB.PRIMARY_UNIT_OF_MEASURE UOM,
                NULL COUNTRY_OF_ORIGIN,
                MSIB.HAZARDOUS_MATERIAL_FLAG HAZARDOUS_MATERIAL,
                NULL HAZARD_CLASS,
                MSIB.SHELF_LIFE_DAYS SHELF_LIFE_DAYS,
                MSIB.ITEM_TYPE PART_TYPE,
                MSIB.UN_NUMBER_ID UN_NUMBER,
                MSIB.ATTRIBUTE18 HAZMAT_CONTAINER,
                MSIB.ATTRIBUTE8 MSDS_NO,
                MSIB.ATTRIBUTE11 ORMD_FLAG,
                MSIB.ATTRIBUTE9 HAZMAT_PACKAGING_GROUP,
                MSIB.ATTRIBUTE17 HAZMAT_DESCRIPTION,
                MSIB.ATTRIBUTE3 PESTICIDE_FLAG,
                MSIB.ATTRIBUTE5 PESTICIDE_FLAG_STATE,
                MSIB.ATTRIBUTE1 CA_PROP_65,
                MSIB.ATTRIBUTE4 VOC_GL,
                MSIB.ATTRIBUTE6 VOC_CATEGORY,
                MSIB.ATTRIBUTE7 VOC_SUB_CATEGORY,
                MSIB.ATTRIBUTE22 TAXWARE_CODE,
                msib.UNIT_WEIGHT UNIT_WEIGHT,
                MSIB.INVENTORY_ITEM_STATUS_CODE ITEM_STATUS,
                NULL WEBSITE_ITEM,
                NULL WC_WEB_HIERARCHY,
                NULL WEB_MFG_PART_NUMBER,
                NULL CYBESOURCE_TAX_CODE,
                NULL WEB_SHORT_DESCRIPTION,
                NULL WEB_LONG_DESCRIPTION,
                NULL BRAND,
                NULL PRODUCT_LINE,
                NULL WEB_SEQUENCE,
                NULL WEB_KEYWORDS,
                NULL BULKY_ITEM,
                MSIB.FIXED_LOT_MULTIPLIER,
                MSIB.INVENTORY_ITEM_FLAG,
                MSIB.INVOICE_ENABLED_FLAG,
                MSIB.PURCHASING_ENABLED_FLAG,
                MSIB.STOCK_ENABLED_FLAG,
                MSIB.MTL_TRANSACTIONS_ENABLED_FLAG
           FROM APPS.MTL_SYSTEM_ITEMS_VL MSIB
          WHERE     1 = 1
                AND MSIB.ORGANIZATION_ID = 222
                AND ITEM_TYPE NOT IN ('SPECIAL')
                AND MSIB.INVENTORY_ITEM_STATUS_CODE = 'Inactive';
   BEGIN
      SELECT UPPER (name) INTO lvc_env FROM v$database;

      IF lvc_env LIKE 'EBSPRD'
      THEN
         lvc_outbound_file :=
               'XXWC_Agility_Inactive_Extract_'
            || TO_CHAR (SYSDATE, 'MMDDYYYYHH24MISS')
            || '.txt';
      ELSE
         lvc_outbound_file :=
               'XXWC_'
            || SUBSTR (lvc_env, INSTR (lvc_env, 'EBS', 1) + 3)
            || '_Agility_Inactive_Extract_'
            || TO_CHAR (SYSDATE, 'MMDDYYYYHH24MISS')
            || '.txt';
      END IF;

      BEGIN
         SELECT directory_path
           INTO g_filepath
           FROM dba_directories
          WHERE directory_name = lvc_outbound_dir;
      EXCEPTION
         WHEN OTHERS
         THEN
            g_filepath := NULL;
      END;


      l_sec := 'Opening Data File';
      gvc_file :=
         UTL_FILE.fopen (LOCATION       => lvc_outbound_dir,
                         filename       => lvc_outbound_file,
                         open_mode      => 'W',
                         max_linesize   => 32767);

      l_sec := 'Printing Data file column titles';
      UTL_FILE.put_line (
         gvc_file,
         '|ITEM_NUMBER|ITEM_DESCRIPTION| ITEM_LONG_DESCRIPTION|MANUFACTURER_NAME|VENDOR_PART_NUMBER|UPC_NUMBER|INV_CAT_CLASS|UNSPSC_CODE|UOM|COUNTRY_OF_ORIGIN|HAZARDOUS_MATERIAL|HAZARD_CLASS|SHELF_LIFE_DAYS|PART_TYPE|UN_NUMBER|HAZMAT_CONTAINER|MSDS_NO|ORMD_FLAG|HAZMAT_PACKAGING_GROUP|HAZMAT_DESCRIPTION|PESTICIDE_FLAG|PESTICIDE_FLAG_STATE|CA_PROP_65|VOC_GL|VOC_CATEGORY|VOC_SUB_CATEGORY|TAXWARE_CODE|UNIT_WEIGHT|ITEM_STATUS|WEBSITE_ITEM|WC_WEB_HIERARCHY|WEB_MFG_PART_NUMBER|CYBERSOURCE_TAX_CODE|WEB_SHORT_DESCRIPTION|WEB_LONG_DESCRIPTION|BRAND|PRODUCT_LINE|WEB_SEQUENCE|WEB_KEYWORDS|BULKY_ITEM|FIXED_LOT_MULTIPLIER|INVENTORY_ITEM_FLAG|INVOICE_ENABLED_FLAG|PURCHASABLE|STOCKABLE |TRANSACTABLE|');

      l_sec := 'Printing Data';

      FOR rec_item_data IN cur_items
      LOOP
         l_sec := rec_item_data.ITEM_NUMBER || ' Item process start';

         UTL_FILE.put_line (
            gvc_file,
               '|'
            || rec_item_data.ITEM_NUMBER
            || '|'
            || rec_item_data.ITEM_DESCRIPTION
            || '|'
            || rec_item_data.ITEM_LONG_DESCRIPTION
            || '|'
            || rec_item_data.MANUFACTURER_NAME
            || '|'
            || rec_item_data.VENDOR_PART_NUMBER
            || '|'
            || rec_item_data.UPC
            || '|'
            || rec_item_data.INV_CAT_CLASS
            || '|'
            || rec_item_data.UNSPSC_CODE
            || '|'
            || rec_item_data.UOM
            || '|'
            || rec_item_data.COUNTRY_OF_ORIGIN
            || '|'
            || rec_item_data.HAZARDOUS_MATERIAL
            || '|'
            || rec_item_data.HAZARD_CLASS
            || '|'
            || rec_item_data.SHELF_LIFE_DAYS
            || '|'
            || rec_item_data.PART_TYPE
            || '|'
            || rec_item_data.UN_NUMBER
            || '|'
            || rec_item_data.HAZMAT_CONTAINER
            || '|'
            || rec_item_data.MSDS_NO
            || '|'
            || rec_item_data.ORMD_FLAG
            || '|'
            || rec_item_data.HAZMAT_PACKAGING_GROUP
            || '|'
            || rec_item_data.HAZMAT_DESCRIPTION
            || '|'
            || rec_item_data.PESTICIDE_FLAG
            || '|'
            || rec_item_data.PESTICIDE_FLAG_STATE
            || '|'
            || rec_item_data.CA_PROP_65
            || '|'
            || rec_item_data.VOC_GL
            || '|'
            || rec_item_data.VOC_CATEGORY
            || '|'
            || rec_item_data.VOC_SUB_CATEGORY
            || '|'
            || rec_item_data.TAXWARE_CODE
            || '|'
            || rec_item_data.UNIT_WEIGHT
            || '|'
            || rec_item_data.ITEM_STATUS
            || '|'
            || rec_item_data.WEBSITE_ITEM
            || '|'
            || rec_item_data.WC_WEB_HIERARCHY
            || '|'
            || rec_item_data.WEB_MFG_PART_NUMBER
            || '|'
            || rec_item_data.CYBESOURCE_TAX_CODE
            || '|'
            || rec_item_data.WEB_SHORT_DESCRIPTION
            || '|'
            || rec_item_data.WEB_LONG_DESCRIPTION
            || '|'
            || rec_item_data.BRAND
            || '|'
            || rec_item_data.PRODUCT_LINE
            || '|'
            || rec_item_data.WEB_SEQUENCE
            || '|'
            || rec_item_data.WEB_KEYWORDS
            || '|'
            || rec_item_data.BULKY_ITEM
            || '|'
            || rec_item_data.FIXED_LOT_MULTIPLIER
            || '|'
            || rec_item_data.INVENTORY_ITEM_FLAG
            || '|'
            || rec_item_data.INVOICE_ENABLED_FLAG
            || '|'
            || rec_item_data.PURCHASING_ENABLED_FLAG
            || '|'
            || rec_item_data.STOCK_ENABLED_FLAG
            || '|'
            || rec_item_data.MTL_TRANSACTIONS_ENABLED_FLAG
            || '|');

         l_sec := rec_item_data.ITEM_NUMBER || ' Item process completed';
      END LOOP;

      UTL_FILE.FCLOSE (gvc_file);

      -- File Permissions change.
      IF g_filepath IS NOT NULL
      THEN
         XXWC_COMMON_UTILITIES_PKG.FILE_PERMISSIONS_CHNG (g_filepath,
                                                          lvc_outbound_file);
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || lvc_procedure,
            p_calling             => l_sec,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          => NULL,
            p_distribution_list   => g_distro_list,
            p_module              => g_module);
   END;


   PROCEDURE print_item_data (P_INVENTORY_ITEM_ID NUMBER)
   IS
      CURSOR cur_item_data
      IS
         SELECT MSIB.SEGMENT1 ITEM_NUMBER,
                REGEXP_REPLACE (REPLACE (msib.description, '|', '/'),
                                '[[:cntrl:]]')
                   ITEM_DESCRIPTION,
                REGEXP_REPLACE (REPLACE (msib.long_description, '|', '/'),
                                '[[:cntrl:]]')
                   ITEM_LONG_DESCRIPTION,
                VENDOR.VENDOR_NAME MANUFACTURER_NAME,
                VENDOR.VENDOR_XREF VENDOR_PART_NUMBER,
                upc_xref UPC,
                (SELECT MIC.SEGMENT1 || '.' || MIC.SEGMENT2
                   FROM APPS.MTL_ITEM_CATEGORIES_V MIC
                  WHERE     MIC.INVENTORY_ITEM_ID = MSIB.INVENTORY_ITEM_ID
                        AND MIC.ORGANIZATION_ID = MSIB.ORGANIZATION_ID
                        AND MIC.CATEGORY_SET_NAME = 'Inventory Category')
                   INV_CAT_CLASS,
                (SELECT c_ext_attr8 UNSPSC
                   FROM apps.EGO_MTL_SY_ITEMS_EXT_B
                  WHERE     attr_group_id = 861
                        AND organization_id = 222
                        AND INVENTORY_ITEM_ID = msib.inventory_item_id
                        AND ROWNUM = 1)
                   UNSPSC_CODE,
                MSIB.PRIMARY_UNIT_OF_MEASURE UOM,
                INT_ATT.COUNTRY_OF_ORIGIN,
                HAZARDOUS_MATERIAL_FLAG HAZARDOUS_MATERIAL,
                PHCT.HAZARD_CLASS HAZARD_CLASS,
                MSIB.SHELF_LIFE_DAYS SHELF_LIFE_DAYS,
                MSIB.ITEM_TYPE PART_TYPE,
                MSIB.UN_NUMBER_ID UN_NUMBER,
                MSIB.ATTRIBUTE18 HAZMAT_CONTAINER,
                MSIB.ATTRIBUTE8 MSDS_NO,
                MSIB.ATTRIBUTE11 ORMD_FLAG,
                MSIB.ATTRIBUTE9 HAZMAT_PACKAGING_GROUP,
                MSIB.ATTRIBUTE17 HAZMAT_DESCRIPTION,
                MSIB.ATTRIBUTE3 PESTICIDE_FLAG,
                MSIB.ATTRIBUTE5 PESTICIDE_FLAG_STATE,
                MSIB.ATTRIBUTE1 CA_PROP_65,
                MSIB.ATTRIBUTE4 VOC_GL,
                MSIB.ATTRIBUTE6 VOC_CATEGORY,
                MSIB.ATTRIBUTE7 VOC_SUB_CATEGORY,
                MSIB.ATTRIBUTE22 TAXWARE_CODE,
                msib.UNIT_WEIGHT UNIT_WEIGHT,
                MSIB.INVENTORY_ITEM_STATUS_CODE ITEM_STATUS,
                DECODE (
                   XXWC_EGO_ITEM_IMPORT_PKG.check_web_item (
                      MSIB.INVENTORY_ITEM_ID),
                   1, 'Y',
                   'N')
                   WEBSITE_ITEM,
                (SELECT CATEGORY_CONCAT_SEGS
                   FROM apps.MTL_ITEM_CATEGORIES_V
                  WHERE     inventory_item_id = msib.inventory_item_id
                        AND organization_id = 222
                        AND CATEGORY_SET_NAME = 'WC Web Hierarchy')
                   WC_WEB_HIERARCHY,
                WC_ATT.WEB_MFG_PART_NUMBER,
                WC_ATT.CYBESOURCE_TAX_CODE,
                WC_ATT.WEB_SHORT_DESCRIPTION,
                WC_ATT.WEB_LONG_DESCRIPTION,
                WC_ATT.BRAND,
                WC_ATT.PRODUCT_LINE,
                WC_ATT.WEB_SEQUENCE,
                WC_ATT.WEB_KEYWORDS,
                WC_ATT.BULKY_ITEM,
                MSIB.FIXED_LOT_MULTIPLIER,
                MSIB.INVENTORY_ITEM_FLAG,
                MSIB.INVOICE_ENABLED_FLAG,
                MSIB.PURCHASING_ENABLED_FLAG,
                MSIB.STOCK_ENABLED_FLAG,
                MSIB.MTL_TRANSACTIONS_ENABLED_FLAG,
                MSIB.INVENTORY_ITEM_ID
           FROM APPS.MTL_SYSTEM_ITEMS_VL MSIB,
                PO_HAZARD_CLASSES_TL PHCT,
                (SELECT INVENTORY_ITEM_ID,
                        C_EXT_ATTR1 BRAND,
                        C_EXT_ATTR3 BULKY_ITEM,
                        C_EXT_ATTR7 PRODUCT_LINE,
                        C_EXT_ATTR6 WEB_MFG_PART_NUMBER,
                        C_EXT_ATTR11 CYBESOURCE_TAX_CODE,
                        N_EXT_ATTR1 WEB_SEQUENCE,
                        TL_EXT_ATTR3 WEB_KEYWORDS,
                        C_EXT_ATTR14 WEB_SHORT_DESCRIPTION,
                        TL_EXT_ATTR2 WEB_LONG_DESCRIPTION
                   FROM EGO_MTL_SY_ITEMS_EXT_VL
                  WHERE attr_group_id = 479 AND organization_id = 222) WC_ATT,
                (SELECT INVENTORY_ITEM_ID,
                           FTV.TERRITORY_CODE
                        || ' - '
                        || FTV.TERRITORY_SHORT_NAME
                           COUNTRY_OF_ORIGIN
                   FROM EGO_MTL_SY_ITEMS_EXT_VL EGSI,
                        APPS.FND_TERRITORIES_VL FTV
                  WHERE     attr_group_id = 861
                        AND ORGANIZATION_ID = 222
                        AND EGSI.C_EXT_ATTR2 = FTV.TERRITORY_CODE(+)) INT_ATT,
                (SELECT inventory_item_id,
                        REPLACE (cross_reference, '|', '') upc_xref,
                        organization_id
                   FROM mtl_cross_references
                  WHERE cross_reference_type = 'UPC') UPC,
                (SELECT INVENTORY_ITEM_ID,
                        REPLACE (CROSS_REFERENCE, '|', '') VENDOR_XREF,
                        (SELECT VENDOR_NAME
                           FROM APPS.AP_SUPPLIERS
                          WHERE SEGMENT1 = MCR.ATTRIBUTE1)
                           VENDOR_NAME
                   FROM APPS.MTL_CROSS_REFERENCES MCR
                  WHERE MCR.CROSS_REFERENCE_TYPE = 'VENDOR') VENDOR
          WHERE     1 = 1
                AND MSIB.INVENTORY_ITEM_ID = P_INVENTORY_ITEM_ID
                AND MSIB.ORGANIZATION_ID = 222
                AND MSIB.ITEM_TYPE NOT IN ('RENTAL', 'RE_RENT', 'SPECIAL')
                AND MSIB.INVENTORY_ITEM_ID = WC_ATT.INVENTORY_ITEM_ID(+)
                AND MSIB.INVENTORY_ITEM_ID = INT_ATT.INVENTORY_ITEM_ID(+)
                AND MSIB.HAZARD_CLASS_ID = PHCT.HAZARD_CLASS_ID(+)
                AND PHCT.LANGUAGE(+) = 'US'
                AND MSIB.INVENTORY_ITEM_ID = UPC.INVENTORY_ITEM_ID(+)
                AND MSIB.INVENTORY_ITEM_ID = VENDOR.INVENTORY_ITEM_ID(+)
                AND NOT EXISTS
                       (SELECT 1
                          FROM APPS.MTL_ITEM_CATEGORIES MIC,
                               apps.mtl_categories MC
                         WHERE     mic.category_id = mc.category_id
                               AND mic.organization_id = 222
                               AND mc.segment1 IN ('SP', '99')
                               AND mic.inventory_item_id =
                                      msib.inventory_item_id);

      lvc_procedure   VARCHAR2 (100) := 'PRINT_ITEM_DATA';
      l_sec           VARCHAR2 (1000);
   BEGIN
      FOR rec_item_data IN cur_item_data
      LOOP
         l_sec := rec_item_data.inventory_item_id || ' Item process start';
         -- Writing File Lines

         UTL_FILE.put_line (
            gvc_file,
               '|'
            || rec_item_data.ITEM_NUMBER
            || '|'
            || rec_item_data.ITEM_DESCRIPTION
            || '|'
            || rec_item_data.ITEM_LONG_DESCRIPTION
            || '|'
            || rec_item_data.MANUFACTURER_NAME
            || '|'
            || rec_item_data.VENDOR_PART_NUMBER
            || '|'
            || rec_item_data.UPC
            || '|'
            || rec_item_data.INV_CAT_CLASS
            || '|'
            || rec_item_data.UNSPSC_CODE
            || '|'
            || rec_item_data.UOM
            || '|'
            || rec_item_data.COUNTRY_OF_ORIGIN
            || '|'
            || rec_item_data.HAZARDOUS_MATERIAL
            || '|'
            || rec_item_data.HAZARD_CLASS
            || '|'
            || rec_item_data.SHELF_LIFE_DAYS
            || '|'
            || rec_item_data.PART_TYPE
            || '|'
            || rec_item_data.UN_NUMBER
            || '|'
            || rec_item_data.HAZMAT_CONTAINER
            || '|'
            || rec_item_data.MSDS_NO
            || '|'
            || rec_item_data.ORMD_FLAG
            || '|'
            || rec_item_data.HAZMAT_PACKAGING_GROUP
            || '|'
            || rec_item_data.HAZMAT_DESCRIPTION
            || '|'
            || rec_item_data.PESTICIDE_FLAG
            || '|'
            || rec_item_data.PESTICIDE_FLAG_STATE
            || '|'
            || rec_item_data.CA_PROP_65
            || '|'
            || rec_item_data.VOC_GL
            || '|'
            || rec_item_data.VOC_CATEGORY
            || '|'
            || rec_item_data.VOC_SUB_CATEGORY
            || '|'
            || rec_item_data.TAXWARE_CODE
            || '|'
            || rec_item_data.UNIT_WEIGHT
            || '|'
            || rec_item_data.ITEM_STATUS
            || '|'
            || rec_item_data.WEBSITE_ITEM
            || '|'
            || rec_item_data.WC_WEB_HIERARCHY
            || '|'
            || rec_item_data.WEB_MFG_PART_NUMBER
            || '|'
            || rec_item_data.CYBESOURCE_TAX_CODE
            || '|'
            || rec_item_data.WEB_SHORT_DESCRIPTION
            || '|'
            || rec_item_data.WEB_LONG_DESCRIPTION
            || '|'
            || rec_item_data.BRAND
            || '|'
            || rec_item_data.PRODUCT_LINE
            || '|'
            || rec_item_data.WEB_SEQUENCE
            || '|'
            || rec_item_data.WEB_KEYWORDS
            || '|'
            || rec_item_data.BULKY_ITEM
            || '|'
            || rec_item_data.FIXED_LOT_MULTIPLIER
            || '|'
            || rec_item_data.INVENTORY_ITEM_FLAG
            || '|'
            || rec_item_data.INVOICE_ENABLED_FLAG
            || '|'
            || rec_item_data.PURCHASING_ENABLED_FLAG
            || '|'
            || rec_item_data.STOCK_ENABLED_FLAG
            || '|'
            || rec_item_data.MTL_TRANSACTIONS_ENABLED_FLAG
            || '|');
         l_sec := rec_item_data.inventory_item_id || ' Item process End';
      END LOOP;

   EXCEPTION
      WHEN OTHERS
      THEN
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || lvc_procedure,
            p_calling             => l_sec,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          => NULL,
            p_distribution_list   => g_distro_list,
            p_module              => g_module);
   END;

   /********************************************************************************
  -- PROCEDURE: GEN_CATCLASS_FILE
  --
  -- PURPOSE: procedure to generate catclass structure for Agility PIM
  --
  -- HISTORY
  -- ===========================================================================
  -- ===========================================================================
  -- VERSION DATE          AUTHOR(S)       DESCRIPTION
  -- ------- -----------   --------------- -------------------------------------
  -- 2.0     06-Mar-2018   Niraj K Ranjan  TMS#20171116-00176   Agility PIM Project Catclass Structure

  *******************************************************************************/
   PROCEDURE gen_catclass_file (p_errbuf    OUT VARCHAR2,
                                p_retcode   OUT VARCHAR2)
   IS
      l_phase                  VARCHAR2 (240);
      l_status                 VARCHAR2 (240);

      l_err_msg                CLOB;
      l_sec                    VARCHAR2 (110) DEFAULT 'START';
      l_user_id                fnd_user.user_id%TYPE;
      l_procedure              VARCHAR2 (50) := 'GEN_CATCLASS_FILE';
      l_application_name       VARCHAR2 (30) := 'XXWC';
      l_sequence               NUMBER;
      l_run_date               DATE;

      l_filename               VARCHAR2 (100);
      l_file_dir               VARCHAR2 (100) := 'XXWC_INV_AGILITY_FEED_DIR';
      l_file_handle            UTL_FILE.file_type;
      l_file_name_temp         VARCHAR2 (100);
      l_base_file_exists       BOOLEAN;
      l_base_file_length       NUMBER;
      l_base_file_block_size   BINARY_INTEGER;
      l_sid                    VARCHAR2 (8);

      --l_org_id            NUMBER := mo_global.get_current_org_id;

      CURSOR cr_catclass
      IS
         SELECT BUSINESS_UNIT,
                SOURCE_SYSTEM,
                CATEGORY,
                CATEGORY_VALUE,
                CATEGORY_DESC,
                SUB_CATEGORY,
                SUB_CATEGORY_VALUE,
                SUB_CATEGORY_DESC,
                PRODUCT_LINE,
                PRODUCT_LINE_VALUE,
                PRODUCT_LINE_DESC,
                CAT_CLASS,
                CAT_CLASS_VALUE,
                CAT_CLASS_DESC,
                OPERATING_UNIT_ID,
                INTERFACE_DATE
           FROM XXWC_AGILITY_PIM_CATCLASS_VW;
   BEGIN
      fnd_file.put_line (fnd_file.LOG,
                         'Beginning EBS To Agility File Generation ');
      fnd_file.put_line (
         fnd_file.LOG,
         '========================================================');
      fnd_file.put_line (fnd_file.LOG, '  ');
      --Initialize the Out Parameter
      p_errbuf := '';
      p_retcode := '0';

      --derive Instance name
      SELECT UPPER (name) INTO l_sid FROM v$database;

      IF l_sid LIKE 'EBSPRD'
      THEN
         SELECT    'XXWC_Agility_Category_Class_hierarchy_'
                || TO_CHAR (SYSTIMESTAMP, 'MMDDYYYYHH24MISS')
                || '.txt'
           INTO l_filename
           FROM DUAL;
      ELSE
         SELECT    'XXWC_'
                || SUBSTR (l_sid, INSTR (l_sid, 'EBS', 1) + 3)
                || '_Agility_Category_Class_hierarchy_'
                || TO_CHAR (SYSTIMESTAMP, 'MMDDYYYYHH24MISS')
                || '.txt'
           INTO l_filename
           FROM DUAL;
      END IF;


      BEGIN
         SELECT directory_path
           INTO g_filepath
           FROM dba_directories
          WHERE directory_name = l_file_dir;
      EXCEPTION
         WHEN OTHERS
         THEN
            g_filepath := NULL;
      END;


      --Derive the file name


      --Set the file name and open the file
      l_file_name_temp := 'TEMP_' || l_filename;
      fnd_file.put_line (fnd_file.LOG, 'File Name:  ' || l_filename);

      --Open the file handler
      l_file_handle :=
         UTL_FILE.fopen (location    => l_file_dir,
                         filename    => l_file_name_temp,
                         open_mode   => 'w');
      fnd_file.put_line (fnd_file.LOG, ' Writing to the file ... ');
      UTL_FILE.put_line (
         l_file_handle,
            'HDS Business Unit'
         || '|'
         || 'Source System'
         || '|'
         || 'Product Hierarchy Level 1 Label'
         || '|'
         || 'Product Hierarchy Level 1 Member Value'
         || '|'
         || 'Product Hierarchy Level 1 Member Description'
         || '|'
         || 'Product Hierarchy Level 2 Label'
         || '|'
         || 'Product Hierarchy Level 2 Member Value'
         || '|'
         || 'Product Hierarchy Level 2 Member Description'
         || '|'
         || 'Product Hierarchy Level 3 Label'
         || '|'
         || 'Product Hierarchy Level 3 Member Value'
         || '|'
         || 'Product Hierarchy Level 3 Member Description'
         || '|'
         || 'Product Hierarchy Level 4 Label'
         || '|'
         || 'Product Hierarchy Level 4 Member Value'
         || '|'
         || 'Product Hierarchy Level 4 Member Description');

      FOR rec_catclass IN cr_catclass
      LOOP
         l_sec := 'Writing file';
         -- Writting file -- Version# 1.6 > Start
         -- Removed special Chars in file  -- Version# 1.11 < Start
         UTL_FILE.put_line (
            l_file_handle,
               rec_catclass.BUSINESS_UNIT
            || '|'
            || rec_catclass.SOURCE_SYSTEM
            || '|'
            || rec_catclass.CATEGORY
            || '|'
            || LPAD (rec_catclass.CATEGORY_VALUE, 4, 0)
            || '|'
            || rec_catclass.CATEGORY_DESC
            || '|'
            || rec_catclass.SUB_CATEGORY
            || '|'
            || LPAD (rec_catclass.SUB_CATEGORY_VALUE, 4, 0)
            || '|'
            || rec_catclass.SUB_CATEGORY_DESC
            || '|'
            || rec_catclass.PRODUCT_LINE
            || '|'
            || rec_catclass.PRODUCT_LINE_VALUE
            || '|'
            || rec_catclass.PRODUCT_LINE_DESC
            || '|'
            || rec_catclass.CAT_CLASS
            || '|'
            || rec_catclass.CAT_CLASS_VALUE
            || '|'
            || rec_catclass.CAT_CLASS_DESC);
      --rec_catclass.OPERATING_UNIT_ID||'|'||
      --rec_catclass.INTERFACE_DATE
      END LOOP;

      --utl_file.put_line(l_file_handle, 'EOF');
      --Close the file
      l_sec := 'Closing the File; ';
      fnd_file.put_line (fnd_file.LOG, l_sec);
      fnd_file.put_line (fnd_file.output, l_sec);
      UTL_FILE.fclose (l_file_handle);

      l_sec := 'Verifying if data exists in file';
      fnd_file.put_line (fnd_file.LOG, 'Verifying if data exists in file');
      UTL_FILE.fgetattr (location      => l_file_dir,
                         filename      => l_file_name_temp,
                         fexists       => l_base_file_exists,
                         file_length   => l_base_file_length,
                         block_size    => l_base_file_block_size);

      fnd_file.put_line (
         fnd_file.LOG,
            'l_base_file_length: '
         || l_base_file_length
         || ' l_base_file_block_size: '
         || l_base_file_block_size);

      IF l_base_file_length <= 1 --Version 1.1 change from 0 to allow for end of line record
      THEN
         UTL_FILE.fremove (location   => l_file_dir,
                           filename   => l_file_name_temp);
      ELSE
         UTL_FILE.frename (l_file_dir,
                           l_file_name_temp,
                           l_file_dir,
                           l_filename);
      END IF;

      -- File Permissions change.
      IF g_filepath IS NOT NULL
      THEN
         XXWC_COMMON_UTILITIES_PKG.FILE_PERMISSIONS_CHNG (g_filepath,
                                                          l_filename);
      END IF;
      
      
   EXCEPTION
      WHEN UTL_FILE.invalid_path
      THEN
         UTL_FILE.fclose (l_file_handle);
         l_err_msg := 'File Path is invalid.';
         fnd_file.put_line (fnd_file.LOG, l_err_msg);
         -- Calling ERROR API
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || l_procedure,
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (l_err_msg, 1, 2000),
            p_error_desc          => SUBSTR (l_err_msg, 1, 240),
            p_distribution_list   => g_distro_list,
            p_module              => 'INV');

         p_retcode := 2;
         p_errbuf := l_err_msg;
      WHEN UTL_FILE.invalid_mode
      THEN
         UTL_FILE.fclose (l_file_handle);
         l_err_msg := 'The open_mode parameter in FOPEN is invalid.';
         fnd_file.put_line (fnd_file.LOG, l_err_msg);
         -- Calling ERROR API
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || l_procedure,
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (l_err_msg, 1, 2000),
            p_error_desc          => SUBSTR (l_err_msg, 1, 240),
            p_distribution_list   => g_distro_list,
            p_module              => 'INV');

         p_retcode := 2;
         p_errbuf := l_err_msg;
      WHEN UTL_FILE.invalid_filehandle
      THEN
         UTL_FILE.fclose (l_file_handle);
         l_err_msg := 'File handle is invalid..';
         fnd_file.put_line (fnd_file.LOG, l_err_msg);
         -- Calling ERROR API
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || l_procedure,
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (l_err_msg, 1, 2000),
            p_error_desc          => SUBSTR (l_err_msg, 1, 240),
            p_distribution_list   => g_distro_list,
            p_module              => 'INV');

         p_retcode := 2;
         p_errbuf := l_err_msg;
      WHEN UTL_FILE.invalid_operation
      THEN
         UTL_FILE.fclose (l_file_handle);
         l_err_msg := 'File could not be opened or operated on as requested';
         fnd_file.put_line (fnd_file.LOG, l_err_msg);
         -- Calling ERROR API
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || l_procedure,
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (l_err_msg, 1, 2000),
            p_error_desc          => SUBSTR (l_err_msg, 1, 240),
            p_distribution_list   => g_distro_list,
            p_module              => 'INV');

         p_retcode := 2;
         p_errbuf := l_err_msg;
      WHEN UTL_FILE.write_error
      THEN
         UTL_FILE.fclose (l_file_handle);
         l_err_msg :=
            'Operating system error occurred during the write operation';
         fnd_file.put_line (fnd_file.LOG, l_err_msg);
         -- Calling ERROR API
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || l_procedure,
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (l_err_msg, 1, 2000),
            p_error_desc          => SUBSTR (l_err_msg, 1, 240),
            p_distribution_list   => g_distro_list,
            p_module              => 'INV');

         p_retcode := 2;
         p_errbuf := l_err_msg;
      WHEN UTL_FILE.internal_error
      THEN
         UTL_FILE.fclose (l_file_handle);
         l_err_msg := 'Unspecified PL/SQL error.';
         fnd_file.put_line (fnd_file.LOG, l_err_msg);
      WHEN UTL_FILE.file_open
      THEN
         UTL_FILE.fclose (l_file_handle);
         l_err_msg :=
            'The requested operation failed because the file is open.';
         fnd_file.put_line (fnd_file.LOG, l_err_msg);
         -- Calling ERROR API
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || l_procedure,
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (l_err_msg, 1, 2000),
            p_error_desc          => SUBSTR (l_err_msg, 1, 240),
            p_distribution_list   => g_distro_list,
            p_module              => 'INV');

         p_retcode := 2;
         p_errbuf := l_err_msg;
      WHEN UTL_FILE.invalid_filename
      THEN
         UTL_FILE.fclose (l_file_handle);
         l_err_msg := 'The filename parameter is invalid.';
         fnd_file.put_line (fnd_file.LOG, l_err_msg);
         -- Calling ERROR API
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || l_procedure,
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (l_err_msg, 1, 2000),
            p_error_desc          => SUBSTR (l_err_msg, 1, 240),
            p_distribution_list   => g_distro_list,
            p_module              => 'INV');

         p_retcode := 2;
         p_errbuf := l_err_msg;
      WHEN UTL_FILE.access_denied
      THEN
         UTL_FILE.fclose (l_file_handle);
         l_err_msg := 'Permission to access to the file location is denied.';
         fnd_file.put_line (fnd_file.LOG, l_err_msg);
         -- Calling ERROR API
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || l_procedure,
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (l_err_msg, 1, 2000),
            p_error_desc          => SUBSTR (l_err_msg, 1, 240),
            p_distribution_list   => g_distro_list,
            p_module              => 'INV');

         p_retcode := 2;
         p_errbuf := l_err_msg;
      WHEN PROGRAM_ERROR
      THEN
         ROLLBACK;
         l_err_msg :=
               l_err_msg
            || l_sec
            || ' ...Error_Stack...'
            || DBMS_UTILITY.format_error_stack ()
            || ' Error_Backtrace...'
            || DBMS_UTILITY.format_error_backtrace ();
         fnd_file.put_line (fnd_file.LOG, l_err_msg);
         -- Calling ERROR API
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || l_procedure,
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (l_err_msg, 1, 2000),
            p_error_desc          => SUBSTR (l_err_msg, 1, 240),
            p_distribution_list   => g_distro_list,
            p_module              => 'INV');

         p_retcode := 2;
         p_errbuf := l_err_msg;
      WHEN OTHERS
      THEN
         UTL_FILE.fclose (l_file_handle);
         l_err_msg :=
               l_err_msg
            || l_sec
            || ' ...Error_Stack...'
            || DBMS_UTILITY.format_error_stack ()
            || ' Error_Backtrace...'
            || DBMS_UTILITY.format_error_backtrace ();
         fnd_file.put_line (fnd_file.LOG, l_err_msg);
         -- Calling ERROR API
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || l_procedure,
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (l_err_msg, 1, 2000),
            p_error_desc          => SUBSTR (l_err_msg, 1, 240),
            p_distribution_list   => g_distro_list,
            p_module              => 'INV');

         p_retcode := 2;
         p_errbuf := l_err_msg;
   END gen_catclass_file;
END XXWC_INV_AGILITY_EXTRACT_PKG;
/