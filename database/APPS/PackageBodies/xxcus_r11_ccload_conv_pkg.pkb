CREATE OR REPLACE PACKAGE BODY APPS.xxcus_r11_ccload_conv_pkg IS

  /*******************************************************************************
  * Procedure:   
  * Description: program for credit card conversion from R11 to R12
  *              Include: (1) all active CC.
  *                       (2) all CC with unused transactions not on ER.
  *                       (3) all CC with unreconcilled transactions on an ER not 
                              push to AP.
   *              Exclude all expired CC and CC w/ program ID 10000
  *
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     04/01/2011    Luong Vu        Initial creation  
  
  ********************************************************************************/
  PROCEDURE cc_data_load(errbuf  OUT VARCHAR2
                        ,retcode OUT NUMBER) IS
  
    -- Package Variables
    l_count_r11 NUMBER;
    l_count_r12 NUMBER;
    l_table     VARCHAR(50);
    l_sequence  VARCHAR(50);
    l_seq_r11   NUMBER;
    l_seq_r12   NUMBER;
    l_org CONSTANT hr_all_organization_units.organization_id%TYPE := 163; --HD Supply US GSC
  
    l_err_code NUMBER;
    l_err_msg  VARCHAR2(3000);
    l_sec      VARCHAR2(255);
  
    l_err_callfrom  VARCHAR2(75) DEFAULT 'xxcus_r11_ccload_conv_pkg';
    l_err_callpoint VARCHAR2(75) DEFAULT 'START';
    l_distro_list   VARCHAR2(75) DEFAULT 'luong.vu@hdsupply.com';
  
    cc_prg_id NUMBER;
  
  BEGIN
    dbms_output.enable(10000000);
    ------------------------------< AP.AP_CARDS_ALL >-----------------------------
    BEGIN
      l_table     := 'AP.AP_CARDS_ALL';
      l_sequence  := 'AP.AP_CARDS_S';
      l_sec       := 'Loading  table ' || l_table;
      l_count_r11 := 0;
      l_count_r12 := 0;
    
    
      EXECUTE IMMEDIATE 'SELECT COUNT(*) FROM ' || l_table || '@r12_to_fin'
        INTO l_count_r11;
    
      EXECUTE IMMEDIATE 'SELECT MAX(card_id) FROM ' || l_table ||
                        '@r12_to_fin'
        INTO l_seq_r11;
    
      FOR c_ccnum IN (SELECT DISTINCT a.card_number
                        FROM ap.ap_cards_all@r12_to_fin a
                       WHERE a.card_program_id <> 10000
                         AND (a.inactive_date >= trunc(SYSDATE) OR
                             a.inactive_date IS NULL)
                         AND a.card_number NOT LIKE '%00000000%'
                      
                      UNION
                      
                      SELECT DISTINCT acc.card_number
                        FROM ap.ap_credit_card_trxns_all@r12_to_fin acc
                       WHERE acc.report_header_id IS NULL
                         AND acc.transaction_type NOT IN ('30', '31')
                         AND acc.trx_id <> 652460
                      
                      UNION
                      
                      SELECT DISTINCT acc.card_number
                        FROM ap.ap_credit_card_trxns_all@r12_to_fin acc
                       WHERE acc.report_header_id IN
                             (SELECT h.report_header_id
                                FROM ap.ap_expense_report_headers_all@r12_to_fin h
                               WHERE h.vouchno = 0))
      
      LOOP
      
        /*     
         SELECT card_program_id
           INTO cc_prg_id
           FROM ap.ap_cards_all@r12_to_fin
          WHERE card_number = c_ccnum.card_number;
        
        */
      
        dbms_output.put_line('START LOOP -  CC NUM ' ||
                             c_ccnum.card_number);
      
        INSERT INTO ap.ap_cards_all
          SELECT (SELECT z.person_id
                    FROM hr.per_all_people_f@r12_to_fin f
                        ,hr.per_all_people_f            z
                   WHERE a.employee_id = f.person_id
                     AND f.employee_number = z.employee_number(+)
                     AND f.effective_end_date >= '31-DEC-4712'
                     AND z.effective_end_date >= '31-DEC-4712'
                  --   AND f.person_type_id <> 9
                  /* AND (a.inactive_date >= trunc(SYSDATE) OR
                  a.inactive_date IS NULL)*/
                  ) employee_id
                ,card_number
                ,card_id
                ,limit_override_amount
                ,trx_limit_override_amount
                ,profile_id
                ,cardmember_name
                ,department_name
                ,physical_card_flag
                ,paper_statement_req_flag
                ,location_id
                ,mothers_maiden_name
                ,description
                ,org_id
                ,inactive_date
                ,last_update_date
                ,last_updated_by
                ,last_update_login
                ,creation_date
                ,created_by
                ,attribute_category
                ,attribute1
                ,attribute2
                ,attribute3
                ,attribute4
                ,attribute5
                ,attribute6
                ,attribute7
                ,attribute8
                ,attribute9
                ,attribute10
                ,attribute11
                ,attribute12
                ,attribute13
                ,attribute14
                ,attribute15
                ,attribute16
                ,attribute17
                ,attribute18
                ,attribute19
                ,attribute20
                ,attribute21
                ,attribute22
                ,attribute23
                ,attribute24
                ,attribute25
                ,attribute26
                ,attribute27
                ,attribute28
                ,attribute29
                ,attribute30
                ,card_expiration_date
                ,CASE card_program_id
                   WHEN 10001 THEN
                    10000
                   WHEN 10020 THEN
                    10001
                   WHEN 10060 THEN
                    10002
                   WHEN 10040 THEN
                    10003
                   WHEN 10080 THEN
                    10004
                 END
                ,request_id
                ,NULL --card_reference_id
                ,NULL
            FROM ap.ap_cards_all@r12_to_fin a
           WHERE a.card_number = c_ccnum.card_number
             AND a.card_program_id <> '10000'
          /* AND (a.inactive_date >= trunc(SYSDATE) OR
          a.inactive_date IS NULL)*\*/
          ;
      
      
      END LOOP;
      COMMIT;
    
      -- dbms_output.put_line('INSERT COMPLETED');
      UPDATE ap.ap_cards_all
         SET org_id = l_org;
      COMMIT;
    
      EXECUTE IMMEDIATE 'alter sequence ' || l_sequence || ' increment by ' ||
                        l_seq_r11;
    
      EXECUTE IMMEDIATE 'select ' || l_sequence || '.nextval from dual '
        INTO l_seq_r12;
    
      EXECUTE IMMEDIATE 'alter sequence ' || l_sequence ||
                        ' increment by 1 ';
    
      EXECUTE IMMEDIATE 'SELECT COUNT(*) FROM ' || l_table
        INTO l_count_r12;
    
      /*     INSERT INTO xxcus.xxcus_cc_conv_tbl
        (table_name
        ,r11_count
        ,r12_count
        ,sequence
        ,seq_r11_num
        ,seq_r12_num
        ,inserted_date)
      VALUES
        (l_table
        ,l_count_r11
        ,l_count_r12
        ,l_sequence
        ,l_seq_r11
        ,l_seq_r12
        ,SYSDATE);*/
    
      COMMIT;
    
    END;
  
    ------------------------------< AP.AP_CARD_DETAILS >-----------------------------
    BEGIN
      l_table     := 'AP.AP_CARD_DETAILS';
      l_sec       := 'Loading table ' || l_table;
      l_count_r11 := 0;
      l_count_r12 := 0;
    
      EXECUTE IMMEDIATE 'SELECT COUNT(*) FROM ' || l_table || '@r12_to_fin'
        INTO l_count_r11;
    
      INSERT /*+ APPEND */
      INTO ap.ap_card_details
        SELECT acd.*
          FROM ap.ap_card_details@r12_to_fin acd
              ,ap.ap_cards_all               aca
         WHERE acd.card_id = aca.card_id;
      COMMIT;
    
    
      EXECUTE IMMEDIATE 'SELECT COUNT(*) FROM ' || l_table
        INTO l_count_r12;
      /* 
      INSERT INTO xxcus.xxcus_cc_conv_tbl
        (table_name
        ,r11_count
        ,r12_count
        ,sequence
        ,seq_r11_num
        ,seq_r12_num
        ,inserted_date)
      VALUES
        (l_table
        ,l_count_r11
        ,l_count_r12
        ,NULL
        ,NULL
        ,NULL
        ,SYSDATE);
      
      COMMIT;*/
    
    END;
  
  
    ------------------------------< AP.AP_CARD_EMP_CANDIDATES >-----------------------------
    BEGIN
      l_table     := 'AP.AP_CARD_EMP_CANDIDATES';
      l_sec       := 'Loading table ' || l_table;
      l_count_r11 := 0;
      l_count_r12 := 0;
    
      EXECUTE IMMEDIATE 'SELECT COUNT(*) FROM ' || l_table || '@r12_to_fin'
        INTO l_count_r11;
    
      INSERT /*+ APPEND */
      INTO ap.ap_card_emp_candidates
        SELECT cec.*
          FROM ap.ap_card_emp_candidates@r12_to_fin cec
              ,ap.ap_cards_all                      aca
         WHERE cec.card_id = aca.card_id;
      COMMIT;
    
    
      EXECUTE IMMEDIATE 'SELECT COUNT(*) FROM ' || l_table
        INTO l_count_r12;
    
      /*    INSERT INTO xxcus.xxcus_cc_conv_tbl
        (table_name
        ,r11_count
        ,r12_count
        ,sequence
        ,seq_r11_num
        ,seq_r12_num
        ,inserted_date)
      VALUES
        (l_table
        ,l_count_r11
        ,l_count_r12
        ,NULL
        ,NULL
        ,NULL
        ,SYSDATE);
      COMMIT;*/
    END;
  
  END cc_data_load;
  /*******************************************************************************
  * Procedure:   
  * Description: Create CC in payment module (11i -> R12 conversion)
  *              
  *
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     04/01/2011    Luong Vu        Initial creation  
  
  ********************************************************************************/
  PROCEDURE create_cc_iby(errbuf  OUT VARCHAR2
                         ,retcode OUT NUMBER) IS
  
    ------------------------------< Create CC in IBY.IBY_CREDITCARDS >-----------------------------   
  BEGIN
  
    DECLARE
      l_return_status   VARCHAR2(1000);
      l_msg_count       NUMBER;
      l_msg_data        VARCHAR2(4000);
      l_card_id         NUMBER;
      l_response        iby_fndcpt_common_pub.result_rec_type;
      l_card_instrument iby_fndcpt_setup_pub.creditcard_rec_type;
      l_count           NUMBER;
    
    
      CURSOR card_list IS
        SELECT pf.party_id
              ,aca.card_number
              ,aca.cardmember_name
          FROM ap_cards_all        aca
              ,hr.per_all_people_f pf
         WHERE aca.employee_id = pf.person_id(+);
    
    BEGIN
      dbms_output.enable(10000000);
    
      FOR c_rec IN card_list
      LOOP
        SELECT COUNT(cc.ccnumber)
          INTO l_count
          FROM iby.iby_creditcard cc
         WHERE cc.ccnumber = c_rec.card_number;
      
        IF l_count = 0 THEN
          /* dbms_output.put_line('Processing cc number: ' ||
          c_rec.card_number);*/
        
          l_card_instrument.owner_id         := c_rec.party_id; --party id
          l_card_instrument.card_holder_name := c_rec.cardmember_name;
          -- l_card_instrument.billing_address_id := 3643;
          l_card_instrument.card_number := c_rec.card_number;
          --  l_card_instrument.expiration_date    := '21-OCT-2011';
          l_card_instrument.instrument_type   := 'CREDITCARD';
          l_card_instrument.purchasecard_flag := 'N';
          l_card_instrument.card_issuer       := 'VISA';
          l_card_instrument.single_use_flag   := 'N';
          l_card_instrument.info_only_flag    := 'N';
          l_card_instrument.card_purpose      := 'N';
          --   l_card_instrument.card_description   := 'Card for BW Corporate Purchases';
          l_card_instrument.active_flag := 'Y';
          iby_fndcpt_setup_pub.create_card(p_api_version => 1.0,
                                           x_return_status => l_return_status,
                                           x_msg_count => l_msg_count,
                                           x_msg_data => l_msg_data,
                                           p_card_instrument => l_card_instrument,
                                           x_card_id => l_card_id,
                                           x_response => l_response,
                                           p_init_msg_list => fnd_api.g_true);
          /*       dbms_output.put_line('card_number = ' || c_rec.card_number);
          dbms_output.put_line('l_return_status = ' || l_return_status);
          dbms_output.put_line('l_msg_count = ' || l_msg_count);
          dbms_output.put_line('l_card_id = ' || l_card_id);
          dbms_output.put_line('l_response.Result_Code = ' ||
                               l_response.result_code);
          dbms_output.put_line('l_response.Result_Category = ' ||
                               l_response.result_category);
          dbms_output.put_line('l_response.Result_Message = ' ||
                               l_response.result_message);*/
        
          UPDATE ap.ap_cards_all -- link card with that created in IBY
             SET card_reference_id = l_card_id
           WHERE card_number = c_rec.card_number;
        
          /*          UPDATE ap.ap_cards_all -- clear CC number from this table
            SET card_number = NULL
          WHERE card_number = c_rec.card_number;*/
        
          COMMIT;
        
        END IF;
      END LOOP;
    END;
  
  END create_cc_iby;

  /*******************************************************************************
  * Procedure:   cc_trans_data_load
  *
  * Description: program for credit card transactions conversion from R11 to R12
  *              
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     04/20/2011    Luong Vu        Initial creation  
  
  ********************************************************************************/

  PROCEDURE cc_trans_data_load(errbuf  OUT VARCHAR2
                              ,retcode OUT NUMBER) IS
  
    -- Package Variables
    l_count_r11 NUMBER;
    l_count_r12 NUMBER;
    l_table     VARCHAR(50);
    l_sequence  VARCHAR(50);
    l_seq_r11   NUMBER;
    l_seq_r12   NUMBER;
    l_org CONSTANT hr_all_organization_units.organization_id%TYPE := 163; --HD Supply US GSC
  
    l_err_code NUMBER;
    l_err_msg  VARCHAR2(3000);
    l_sec      VARCHAR2(255);
  
    l_err_callfrom  VARCHAR2(75) DEFAULT 'xxcus_r11_ccload_conv_pkg';
    l_err_callpoint VARCHAR2(75) DEFAULT 'START';
    l_distro_list   VARCHAR2(75) DEFAULT 'luong.vu@hdsupply.com';
  
  BEGIN
    ------------------------------< AP.AP_CREDIT_CARD_TRXNS_ALL >-----------------------------
    BEGIN
      l_table     := 'AP.AP_CREDIT_CARD_TRXNS_ALL';
      l_sequence  := 'AP.AP_CREDIT_CARD_TRXNS_S1';
      l_sec       := 'Loading  table ' || l_table;
      l_count_r11 := 0;
      l_count_r12 := 0;
    
      EXECUTE IMMEDIATE 'SELECT COUNT(*) FROM ' || l_table || '@r12_to_fin'
        INTO l_count_r11;
    
      EXECUTE IMMEDIATE 'SELECT MAX(trx_id) FROM ' || l_table ||
                        '@r12_to_fin'
        INTO l_seq_r11;
    
      EXECUTE IMMEDIATE 'truncate table ap.ap_credit_card_trxns_all';
      EXECUTE IMMEDIATE 'truncate table xxcus.xxcusap_cc_trxns_all_temp1';
      EXECUTE IMMEDIATE 'truncate table xxcus.xxcusap_cc_trxns_all_temp2';
    
      INSERT /*+ APPEND */
      INTO xxcus.xxcusap_cc_trxns_all_temp1
      
        SELECT acc.trx_id
              ,acc.validate_code
              ,CASE acc.card_program_id
                 WHEN 10001 THEN
                  10000
                 WHEN 10020 THEN
                  10001
                 WHEN 10060 THEN
                  10002
                 WHEN 10040 THEN
                  10003
                 WHEN 10080 THEN
                  10004
               END card_program_id --acc.card_program_id
              ,0 --acc.expensed_amount
              ,acc.card_number
              ,acc.reference_number
              ,acc.transaction_type
              ,acc.transaction_date
              ,acc.transaction_amount
              ,acc.debit_flag
              ,acc.billed_date
              ,acc.billed_amount
              ,acc.billed_decimal
              ,acc.billed_currency_code
              ,acc.posted_date
              ,acc.posted_amount
              ,acc.posted_decimal
              ,acc.posted_currency_code
              ,acc.currency_conversion_exponent
              ,acc.currency_conversion_rate
              ,acc.mis_industry_code
              ,acc.sic_code
              ,acc.merchant_tax_id
              ,acc.merchant_reference
              ,acc.merchant_name1
              ,acc.merchant_name2
              ,acc.merchant_address1
              ,acc.merchant_address2
              ,acc.merchant_address3
              ,acc.merchant_address4
              ,acc.merchant_city
              ,acc.merchant_province_state
              ,acc.merchant_postal_code
              ,acc.merchant_country
              ,acc.total_tax
              ,acc.local_tax
              ,acc.national_tax
              ,acc.other_tax
              ,163
              ,acc.last_update_date
              ,acc.last_updated_by
              ,acc.last_update_login
              ,acc.creation_date
              ,acc.created_by
              ,acc.folio_type
              ,acc.atm_cash_advance
              ,acc.atm_transaction_date
              ,acc.atm_fee_amount
              ,acc.atm_type
              ,acc.atm_id
              ,acc.atm_network_id
              ,acc.restaurant_food_amount
              ,acc.restaurant_beverage_amount
              ,acc.restaurant_tip_amount
              ,acc.car_rental_date
              ,acc.car_return_date
              ,acc.car_rental_location
              ,acc.car_rental_state
              ,acc.car_return_location
              ,acc.car_return_state
              ,acc.car_renter_name
              ,acc.car_rental_days
              ,acc.car_rental_agreement_number
              ,acc.car_class
              ,acc.car_total_mileage
              ,acc.car_gas_amount
              ,acc.car_insurance_amount
              ,acc.car_mileage_amount
              ,acc.car_daily_rate
              ,acc.hotel_arrival_date
              ,acc.hotel_depart_date
              ,acc.hotel_charge_desc
              ,acc.hotel_guest_name
              ,acc.hotel_stay_duration
              ,acc.hotel_room_rate
              ,acc.hotel_no_show_flag
              ,acc.hotel_room_amount
              ,acc.hotel_telephone_amount
              ,acc.hotel_room_tax
              ,acc.hotel_bar_amount
              ,acc.hotel_movie_amount
              ,acc.hotel_gift_shop_amount
              ,acc.hotel_laundry_amount
              ,acc.hotel_health_amount
              ,acc.hotel_restaurant_amount
              ,acc.hotel_business_amount
              ,acc.hotel_parking_amount
              ,acc.hotel_room_service_amount
              ,acc.hotel_tip_amount
              ,acc.hotel_misc_amount
              ,acc.hotel_city
              ,acc.hotel_state
              ,acc.hotel_folio_number
              ,acc.hotel_room_type
              ,acc.air_departure_date
              ,acc.air_departure_city
              ,acc.air_routing
              ,acc.air_arrival_city
              ,acc.air_stopover_flag
              ,acc.air_base_fare_amount
              ,acc.air_fare_basis_code
              ,acc.air_service_class
              ,acc.air_carrier_abbreviation
              ,acc.air_carrier_code
              ,acc.air_ticket_issuer
              ,acc.air_issuer_city
              ,acc.air_passenger_name
              ,acc.air_refund_ticket_number
              ,acc.air_exchanged_ticket_number
              ,acc.air_agency_number
              ,acc.air_ticket_number
              ,acc.financial_category
              ,acc.payment_flag
              ,acc.record_type
              ,acc.merchant_activity
              ,NULL --acc.category
              ,NULL --acc.report_header_id
              ,NULL --acc.expense_status
              ,acc.company_prepaid_invoice_id
              ,acc.inactive_emp_wf_item_key
              ,acc.location_id
              ,NULL --acc.request_id
              ,acc.merchant_country_code
              ,acc.dispute_date
              ,acc.payment_due_from_code
              ,acc.trx_available_date
              ,acc.card_acceptor_id
              ,acc.trxn_detail_flag
               /*,(SELECT cc.card_id
                FROM iby.iby_creditcard c
                    ,ap.ap_cards_all    cc
               WHERE acc.card_number = c.ccnumber(+)
                 AND c.instrid = cc.card_reference_id) --card_id*/
              ,aca.card_id
              ,acc.description
              ,NULL --COMPANY_NUMBER
              ,NULL --MARKET_CODE
              ,NULL --VALIDATE_REQUEST_ID
          FROM ap.ap_credit_card_trxns_all@r12_to_fin acc
               --  ,iby.iby_creditcard                     icc
              ,ap.ap_cards_all@r12_to_fin aca
         WHERE acc.card_number = aca.card_number
           AND aca.card_program_id <> 10000
           AND acc.report_header_id IS NOT NULL
           AND acc.report_header_id IN
               (SELECT h.report_header_id
                  FROM ap.ap_expense_report_headers_all@r12_to_fin h
                 WHERE h.vouchno = 0)
        
        --AND aca.employee_id IS NOT NULL
        ;
    
      COMMIT;
    
      INSERT /*+ APPEND */
      INTO xxcus.xxcusap_cc_trxns_all_temp2
        SELECT acc.trx_id
              ,acc.validate_code
              ,CASE acc.card_program_id
                 WHEN 10001 THEN
                  10000
                 WHEN 10020 THEN
                  10001
                 WHEN 10060 THEN
                  10002
                 WHEN 10040 THEN
                  10003
                 WHEN 10080 THEN
                  10004
               END card_program_id --acc.card_program_id
              ,0 --acc.expensed_amount
              ,acc.card_number
              ,acc.reference_number
              ,acc.transaction_type
              ,acc.transaction_date
              ,acc.transaction_amount
              ,acc.debit_flag
              ,acc.billed_date
              ,acc.billed_amount
              ,acc.billed_decimal
              ,acc.billed_currency_code
              ,acc.posted_date
              ,acc.posted_amount
              ,acc.posted_decimal
              ,acc.posted_currency_code
              ,acc.currency_conversion_exponent
              ,acc.currency_conversion_rate
              ,acc.mis_industry_code
              ,acc.sic_code
              ,acc.merchant_tax_id
              ,acc.merchant_reference
              ,acc.merchant_name1
              ,acc.merchant_name2
              ,acc.merchant_address1
              ,acc.merchant_address2
              ,acc.merchant_address3
              ,acc.merchant_address4
              ,acc.merchant_city
              ,acc.merchant_province_state
              ,acc.merchant_postal_code
              ,acc.merchant_country
              ,acc.total_tax
              ,acc.local_tax
              ,acc.national_tax
              ,acc.other_tax
              ,163
              ,acc.last_update_date
              ,acc.last_updated_by
              ,acc.last_update_login
              ,acc.creation_date
              ,acc.created_by
              ,acc.folio_type
              ,acc.atm_cash_advance
              ,acc.atm_transaction_date
              ,acc.atm_fee_amount
              ,acc.atm_type
              ,acc.atm_id
              ,acc.atm_network_id
              ,acc.restaurant_food_amount
              ,acc.restaurant_beverage_amount
              ,acc.restaurant_tip_amount
              ,acc.car_rental_date
              ,acc.car_return_date
              ,acc.car_rental_location
              ,acc.car_rental_state
              ,acc.car_return_location
              ,acc.car_return_state
              ,acc.car_renter_name
              ,acc.car_rental_days
              ,acc.car_rental_agreement_number
              ,acc.car_class
              ,acc.car_total_mileage
              ,acc.car_gas_amount
              ,acc.car_insurance_amount
              ,acc.car_mileage_amount
              ,acc.car_daily_rate
              ,acc.hotel_arrival_date
              ,acc.hotel_depart_date
              ,acc.hotel_charge_desc
              ,acc.hotel_guest_name
              ,acc.hotel_stay_duration
              ,acc.hotel_room_rate
              ,acc.hotel_no_show_flag
              ,acc.hotel_room_amount
              ,acc.hotel_telephone_amount
              ,acc.hotel_room_tax
              ,acc.hotel_bar_amount
              ,acc.hotel_movie_amount
              ,acc.hotel_gift_shop_amount
              ,acc.hotel_laundry_amount
              ,acc.hotel_health_amount
              ,acc.hotel_restaurant_amount
              ,acc.hotel_business_amount
              ,acc.hotel_parking_amount
              ,acc.hotel_room_service_amount
              ,acc.hotel_tip_amount
              ,acc.hotel_misc_amount
              ,acc.hotel_city
              ,acc.hotel_state
              ,acc.hotel_folio_number
              ,acc.hotel_room_type
              ,acc.air_departure_date
              ,acc.air_departure_city
              ,acc.air_routing
              ,acc.air_arrival_city
              ,acc.air_stopover_flag
              ,acc.air_base_fare_amount
              ,acc.air_fare_basis_code
              ,acc.air_service_class
              ,acc.air_carrier_abbreviation
              ,acc.air_carrier_code
              ,acc.air_ticket_issuer
              ,acc.air_issuer_city
              ,acc.air_passenger_name
              ,acc.air_refund_ticket_number
              ,acc.air_exchanged_ticket_number
              ,acc.air_agency_number
              ,acc.air_ticket_number
              ,acc.financial_category
              ,acc.payment_flag
              ,acc.record_type
              ,acc.merchant_activity
              ,NULL --acc.category
              ,NULL --acc.report_header_id
              ,NULL --acc.expense_status
              ,acc.company_prepaid_invoice_id
              ,acc.inactive_emp_wf_item_key
              ,acc.location_id
              ,NULL --acc.request_id
              ,acc.merchant_country_code
              ,acc.dispute_date
              ,acc.payment_due_from_code
              ,acc.trx_available_date
              ,acc.card_acceptor_id
              ,acc.trxn_detail_flag
               /*              ,(SELECT cc.card_id
                FROM iby.iby_creditcard c
                    ,ap.ap_cards_all    cc
               WHERE acc.card_number = c.ccnumber
                 AND c.instrid = cc.card_reference_id) --card_id*/
              ,aca.card_id
              ,acc.description
              ,NULL --COMPANY_NUMBER
              ,NULL --MARKET_CODE
              ,NULL --VALIDATE_REQUEST_ID
          FROM ap.ap_credit_card_trxns_all@r12_to_fin acc
               --  ,iby.iby_creditcard                     icc
              ,ap.ap_cards_all aca
         WHERE acc.report_header_id IS NULL
           AND acc.card_number = aca.card_number
              -- AND aca.employee_id IS NOT NULL
           AND acc.transaction_type NOT IN ('30', '31')
           AND acc.trx_id <> 652460;
      -- AND acc.trx_id <> tmp.trx_id
    
      COMMIT;
    
      FOR c_rec IN (SELECT tmp.*
                      FROM xxcus.xxcusap_cc_trxns_all_temp1 tmp
                    UNION
                    SELECT tmpp.*
                      FROM xxcus.xxcusap_cc_trxns_all_temp2 tmpp)
      LOOP
        INSERT INTO ap.ap_credit_card_trxns_all
        VALUES
          (c_rec.trx_id
          ,c_rec.validate_code
          ,c_rec.card_program_id
          ,c_rec.expensed_amount
          ,c_rec.card_number
          ,c_rec.reference_number
          ,c_rec.transaction_type
          ,c_rec.transaction_date
          ,c_rec.transaction_amount
          ,c_rec.debit_flag
          ,c_rec.billed_date
          ,c_rec.billed_amount
          ,c_rec.billed_decimal
          ,c_rec.billed_currency_code
          ,c_rec.posted_date
          ,c_rec.posted_amount
          ,c_rec.posted_decimal
          ,c_rec.posted_currency_code
          ,c_rec.currency_conversion_exponent
          ,c_rec.currency_conversion_rate
          ,c_rec.mis_industry_code
          ,c_rec.sic_code
          ,c_rec.merchant_tax_id
          ,c_rec.merchant_reference
          ,c_rec.merchant_name1
          ,c_rec.merchant_name2
          ,c_rec.merchant_address1
          ,c_rec.merchant_address2
          ,c_rec.merchant_address3
          ,c_rec.merchant_address4
          ,c_rec.merchant_city
          ,c_rec.merchant_province_state
          ,c_rec.merchant_postal_code
          ,c_rec.merchant_country
          ,c_rec.total_tax
          ,c_rec.local_tax
          ,c_rec.national_tax
          ,c_rec.other_tax
          ,163
          ,c_rec.last_update_date
          ,c_rec.last_updated_by
          ,c_rec.last_update_login
          ,c_rec.creation_date
          ,c_rec.created_by
          ,c_rec.folio_type
          ,c_rec.atm_cash_advance
          ,c_rec.atm_transaction_date
          ,c_rec.atm_fee_amount
          ,c_rec.atm_type
          ,c_rec.atm_id
          ,c_rec.atm_network_id
          ,c_rec.restaurant_food_amount
          ,c_rec.restaurant_beverage_amount
          ,c_rec.restaurant_tip_amount
          ,c_rec.car_rental_date
          ,c_rec.car_return_date
          ,c_rec.car_rental_location
          ,c_rec.car_rental_state
          ,c_rec.car_return_location
          ,c_rec.car_return_state
          ,c_rec.car_renter_name
          ,c_rec.car_rental_days
          ,c_rec.car_rental_agreement_number
          ,c_rec.car_class
          ,c_rec.car_total_mileage
          ,c_rec.car_gas_amount
          ,c_rec.car_insurance_amount
          ,c_rec.car_mileage_amount
          ,c_rec.car_daily_rate
          ,c_rec.hotel_arrival_date
          ,c_rec.hotel_depart_date
          ,c_rec.hotel_charge_desc
          ,c_rec.hotel_guest_name
          ,c_rec.hotel_stay_duration
          ,c_rec.hotel_room_rate
          ,c_rec.hotel_no_show_flag
          ,c_rec.hotel_room_amount
          ,c_rec.hotel_telephone_amount
          ,c_rec.hotel_room_tax
          ,c_rec.hotel_bar_amount
          ,c_rec.hotel_movie_amount
          ,c_rec.hotel_gift_shop_amount
          ,c_rec.hotel_laundry_amount
          ,c_rec.hotel_health_amount
          ,c_rec.hotel_restaurant_amount
          ,c_rec.hotel_business_amount
          ,c_rec.hotel_parking_amount
          ,c_rec.hotel_room_service_amount
          ,c_rec.hotel_tip_amount
          ,c_rec.hotel_misc_amount
          ,c_rec.hotel_city
          ,c_rec.hotel_state
          ,c_rec.hotel_folio_number
          ,c_rec.hotel_room_type
          ,c_rec.air_departure_date
          ,c_rec.air_departure_city
          ,c_rec.air_routing
          ,c_rec.air_arrival_city
          ,c_rec.air_stopover_flag
          ,c_rec.air_base_fare_amount
          ,c_rec.air_fare_basis_code
          ,c_rec.air_service_class
          ,c_rec.air_carrier_abbreviation
          ,c_rec.air_carrier_code
          ,c_rec.air_ticket_issuer
          ,c_rec.air_issuer_city
          ,c_rec.air_passenger_name
          ,c_rec.air_refund_ticket_number
          ,c_rec.air_exchanged_ticket_number
          ,c_rec.air_agency_number
          ,c_rec.air_ticket_number
          ,c_rec.financial_category
          ,c_rec.payment_flag
          ,c_rec.record_type
          ,c_rec.merchant_activity
          ,c_rec.category
          ,c_rec.report_header_id
          ,c_rec.expense_status
          ,c_rec.company_prepaid_invoice_id
          ,c_rec.inactive_emp_wf_item_key
          ,c_rec.location_id
          ,c_rec.request_id
          ,c_rec.merchant_country_code
          ,c_rec.dispute_date
          ,c_rec.payment_due_from_code
          ,c_rec.trx_available_date
          ,c_rec.card_acceptor_id
          ,c_rec.trxn_detail_flag
          ,c_rec.card_id
          ,c_rec.description
          ,c_rec.company_number
          ,c_rec.market_code
          ,c_rec.validate_request_id);
      END LOOP;
      COMMIT;
    
      UPDATE ap.ap_credit_card_trxns_all
         SET org_id = l_org;
    
      COMMIT;
    
      EXECUTE IMMEDIATE 'alter sequence ' || l_sequence || ' increment by ' ||
                        l_seq_r11;
    
      EXECUTE IMMEDIATE 'select ' || l_sequence || '.nextval from dual '
        INTO l_seq_r12;
    
      EXECUTE IMMEDIATE 'alter sequence ' || l_sequence ||
                        ' increment by 1 ';
    
      EXECUTE IMMEDIATE 'SELECT COUNT(*) FROM ' || l_table
        INTO l_count_r12;
    
      /*      INSERT INTO xxcus.xxcus_cc_conv_tbl
        (table_name
        ,r11_count
        ,r12_count
        ,sequence
        ,seq_r11_num
        ,seq_r12_num
        ,inserted_date)
      VALUES
        (l_table
        ,l_count_r11
        ,l_count_r12
        ,l_sequence
        ,l_seq_r11
        ,l_seq_r12
        ,SYSDATE);*/
    
      COMMIT;
    
    END;
  
  END cc_trans_data_load;

END xxcus_r11_ccload_conv_pkg;
/
