CREATE OR REPLACE PACKAGE BODY APPS.xxcusap_frgt_pmt_pkg AS

  /********************************************************************************
  
  File Name: xxcusap_frgt_pmt_pkg
  
  PROGRAM TYPE: PL/SQL Package spec and body
  
  PURPOSE: Create file to send to the logistics team via FTP to provide Oracle Accounts Payable
           containing payment data for freight vendors for each pay run. Oracle contains
           freight payment data for only the IPVF and Uitlities LOBs.
  
  
           Service Ticket XXXXX
  
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)            DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     06/11/2011    Ivelice De Jesus    Initial creation of the procedure
  2.0     27-JAN-2012   Luong Vu            Upgraded to R12
  2.1     01-NOV-2013   Luong Vu            Add Org lookup to include invoices for
                                            CAD Org
  2.2     16-Jan-2014   Luong Vu            Remove ap.ap_invoice_distributions_all.
                                            It is not being used and causing duplicate
                                            rows.
                                            Add or to where clause to find match in
                                            CAN_USD or CAN_CAD of Freight Admin app.
  ********************************************************************************/

  PROCEDURE create_pmt_file(errbuf  OUT VARCHAR2
                           ,retcode OUT NUMBER) IS
    -- this is based on the output of the file and this will be the parameters.
  
    --Intialize Variables
    l_last_run_date  DATE;
    l_err_msg        VARCHAR2(2000);
    l_err_code       NUMBER;
    l_sid            VARCHAR2(15);
    l_sec            VARCHAR2(150);
    l_org_id         NUMBER;
    l_procedure_name VARCHAR2(75) := 'xxcusap_frgt_pmt_pkg.create_pmt_file';
    l_exists         INTEGER;
  
    --Email Defaults
    pl_dflt_email fnd_user.email_address%TYPE := 'hds-HDSFASTap-u1@hughessuply.com';
  
    --File Variables
    l_file_name_recon   VARCHAR2(150);
    l_file_name_temp    VARCHAR2(150);
    l_file_dir          VARCHAR2(100);
    l_file_handle_recon utl_file.file_type;
  
    --Audit File Variables
    l_start_date TIMESTAMP;
    l_end_date   TIMESTAMP;
  
  
  BEGIN
  
    l_start_date := systimestamp;
  
    SELECT lower(NAME)
      INTO l_sid
      FROM v$database;
  
    -- l_file_dir := '/xx_iface/' || l_sid || '/outbound/uc4/ap/freight';
    errbuf     := NULL;
    retcode    := NULL;
    l_file_dir := 'ORACLE_INT_UC4_FREIGHT';
  
  
    --Identify file names
    l_file_name_recon := 'HDSPayOra' || to_char(SYSDATE, 'YYYY-MM-DD') || 'T' ||
                         to_char(SYSDATE, 'HH24MISS') || '.txt';
    l_file_name_temp  := 'XX' || l_file_name_recon;
  
    -- Get ORG_ID
    SELECT org.organization_id
      INTO l_org_id
      FROM hr_all_organization_units org
     WHERE org.name = 'HD Supply Corp USD - Org';
  
    --Check for new records
    BEGIN
    
      l_sec := 'Check for new records.';
      --dbms_output.put_line('Section: ' || l_sec);
    
      SELECT nvl(MAX(last_run_date), '01-Jan-2001')
        INTO l_last_run_date
        FROM xxcus.xxcusap_audit_frt_pay_ext_tbl;
    
      SELECT COUNT(i.invoice_id)
        INTO l_exists
        FROM ap.ap_invoices_all i
             --,po.po_vendors                   v
            ,po_vendors                 v
            ,ap.ap_invoice_payments_all p
            ,ap.ap_checks_all           c
             -- ,ap.ap_invoice_distributions_all d   v2.2
             --,po.po_vendor_sites_all                                     vs
            ,po_vendor_sites_all                                         vs -- R12 - use view
            ,itops_apps.frt_carrier_xref@apxprd_lnk.hsi.hughessupply.com s --xpeprd_lnk.hsi.hughessupply.com s
       WHERE c.check_id = p.check_id
         AND i.invoice_id = p.invoice_id
         AND v.vendor_id = i.vendor_id
         AND i.vendor_site_id = vs.vendor_site_id
            --AND p.invoice_id = d.invoice_id
            --AND to_char(vs.vendor_site_id) = s.dcm       v2.2
         AND (to_char(vs.vendor_site_id) = s.dcm OR
             to_char(vs.vendor_site_id) = s.can_usd OR
             to_char(vs.vendor_site_id) = s.can_cad)
         AND i.pay_group_lookup_code = 'FREIGHT'
         AND i.cancelled_date IS NULL
         AND c.void_date IS NULL
            --AND i.org_id = l_org_id
         AND i.org_id IN -- v2.1
             (SELECT meaning
                FROM applsys.fnd_lookup_values
               WHERE lookup_type = 'HDS_AP_FREIGHT_ORG')
         AND c.check_date > l_last_run_date
         AND c.check_date > l_last_run_date
      --AND i.discount_amount_taken > 0
      ;
    
      IF l_exists >= 1 THEN
      
        --Truncate XXCUS_AP_FRGT_PYMT_EXTRACT_TBL
        BEGIN
          l_sec := 'Truncate the summary table before loading specified time frame.';
          --dbms_output.put_line('Section: ' || l_sec);
        
          EXECUTE IMMEDIATE 'TRUNCATE TABLE XXCUS.XXCUSAP_FRGT_PYMT_EXTRACT_TBL';
        
        EXCEPTION
          WHEN OTHERS THEN
            l_err_msg := 'Failed to Truncate XXCUS.XXCUSAP_FRGT_PYMT_EXTRACT_TBL: ' ||
                         SQLERRM;
            RAISE program_error;
        END;
      
        -- Get extract information begin
        BEGIN
        
          l_sec := 'Populate the xxcusap_frgt_pymt_extract_tbl table.';
          --dbms_output.put_line('Section: ' || l_sec);
        
          INSERT INTO xxcus.xxcusap_frgt_pymt_extract_tbl
            (SELECT i.invoice_date
                   ,i.invoice_num
                   ,'009L'
                   ,i.invoice_amount
                   ,i.discount_amount_taken
                   ,c.amount
                   ,c.check_number
                   ,c.check_date
                   ,s.scac_cd
               FROM ap.ap_invoices_all i
                    --,po.po_vendors                   v
                   ,po_vendors                 v
                   ,ap.ap_invoice_payments_all p
                   ,ap.ap_checks_all           c
                    -- ,ap.ap_invoice_distributions_all d        v2.2
                    --,po.po_vendor_sites_all                                     vs
                   ,po_vendor_sites_all                                         vs --R12 uses view
                   ,itops_apps.frt_carrier_xref@apxprd_lnk.hsi.hughessupply.com s --xpeprd_lnk.hsi.hughessupply.com s
              WHERE c.check_id = p.check_id
                AND i.invoice_id = p.invoice_id
                AND v.vendor_id = i.vendor_id
                AND i.vendor_site_id = vs.vendor_site_id
                   --AND p.invoice_id = d.invoice_id                               v2.2
                   --AND to_char(vs.vendor_site_id) = s.dcm       v2.2
                AND (to_char(vs.vendor_site_id) = s.dcm OR
                    to_char(vs.vendor_site_id) = s.can_usd OR
                    to_char(vs.vendor_site_id) = s.can_cad)
                AND i.pay_group_lookup_code = 'FREIGHT'
                AND i.cancelled_date IS NULL
                AND c.void_date IS NULL
                   --AND i.org_id = l_org_id                
                AND i.org_id IN --v2.1
                    (SELECT meaning
                       FROM applsys.fnd_lookup_values
                      WHERE lookup_type = 'HDS_AP_FREIGHT_ORG')
                AND c.check_date > l_last_run_date
             --AND i.discount_amount_taken > 0
             );
          --dbms_output.put_line('Section: here2');
          COMMIT;
        
        EXCEPTION
          WHEN OTHERS THEN
            l_err_msg := 'Failed to Populate the xxcusap_frgt_pymt_extract_tbl table: ' ||
                         SQLERRM;
            RAISE program_error;
        END;
      
        BEGIN
        
          --Set the file name and open the file
        
          l_sec := 'Write first entry to log and open file.';
          --dbms_output.put_line('Section: ' || l_sec);
        
          fnd_file.put_line(fnd_file.log,
                            'Filename generated : ' || l_file_name_recon);
          fnd_file.put_line(fnd_file.output,
                            'Filename generated : ' || l_file_name_recon);
        
          l_file_handle_recon := utl_file.fopen(l_file_dir, l_file_name_temp,
                                                'w');
        
          l_sec := 'Looping through table writing to file.';
          --dbms_output.put_line('IM HERE');
        
          FOR c_ap_frgt_file IN (SELECT *
                                   FROM xxcus.xxcusap_frgt_pymt_extract_tbl)
          LOOP
          
            --Write detail rows
            utl_file.put_line(l_file_handle_recon,
                              to_char(c_ap_frgt_file.invoice_date,
                                       'YYYY-MM-DD') || ' 0:00^' ||
                               c_ap_frgt_file.invoice_num || '^' ||
                               c_ap_frgt_file.fru || '^' ||
                               c_ap_frgt_file.invoice_amount || '^' ||
                               c_ap_frgt_file.discount_amount_taken || '^' ||
                               c_ap_frgt_file.check_amount || '^' ||
                               c_ap_frgt_file.check_number || '^' ||
                               to_char(c_ap_frgt_file.check_date,
                                       'YYYY-MM-DD') || ' 0:00^' ||
                               c_ap_frgt_file.scac_cd);
          
          
          END LOOP;
          --dbms_output.put_line('IM HERE2');
          l_sec := 'Enter second log record and close file.';
          --dbms_output.put_line('Section: ' || l_sec);
        
          --Close the file
          l_sec := 'Closing the File; ';
          --dbms_output.put_line('Section: ' || l_sec);
          fnd_file.put_line(fnd_file.log, l_sec);
          fnd_file.put_line(fnd_file.output, l_sec);
        
          utl_file.fclose(l_file_handle_recon);
        
          l_sec := 'Rename file for pickup';
          --dbms_output.put_line('Section: ' || l_sec);
          --dbms_output.put_line('IM HERE3');
          utl_file.frename(l_file_dir, l_file_name_temp, l_file_dir,
                           l_file_name_recon);
          --dbms_output.put_line('IM HERE4');
        
          COMMIT;
        EXCEPTION
          WHEN OTHERS THEN
            l_err_msg := 'Failed to write to file: ' || SQLERRM;
            RAISE program_error;
          
        END;
      
        BEGIN
        
          l_sec := 'Insert into audit table. xxcusap_audit_frgt_pay_ext_tbl';
          --dbms_output.put_line('Section: ' || l_sec);
          l_end_date := systimestamp;
        
          INSERT INTO xxcus.xxcusap_audit_frt_pay_ext_tbl
            (SELECT xxcus_audt_frt_pay_ext_seq.nextval
                   ,ct
                   ,nvl(amt, 0)
                   ,SYSDATE
                   ,l_end_date - l_start_date
               FROM (SELECT COUNT(invoice_num) AS ct
                           ,SUM(invoice_amount) AS amt
                       FROM xxcus.xxcusap_frgt_pymt_extract_tbl) rsl);
        
        END;
      ELSE
        --No records found. Abort.
        l_sec := 'No new records found in Freight Pay Extract';
        --dbms_output.put_line('Section: ' || l_sec);
        l_err_msg := 'There is no current data to produce XXCUS.XXCUSAP_FRGT_PYMT_EXTRACT_TBL. No file will be created. No entries into the Audit Table.';
      END IF;
    
    END;
  
    COMMIT;
  
  EXCEPTION
    -- this section traps my errors
    WHEN program_error THEN
      ROLLBACK;
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := 'Error in program ' || l_procedure_name || ' Error: ' ||
                    l_sec;
      l_err_msg  := l_err_msg || ' ERROR ' || substr(SQLERRM, 1, 2000);
      --dbms_output.put_line('Section: ' || l_err_msg);
    
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      utl_file.fclose(l_file_handle_recon);
      utl_file.fremove(l_file_dir, l_file_name_recon);
      retcode := l_err_code;
      errbuf  := l_err_msg;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_procedure_name,
                                           p_calling => l_sec,
                                           p_request_id => fnd_global.conc_request_id,
                                           p_ora_error_msg => SQLERRM,
                                           p_error_desc => 'Error running AP Freight Payment package with Program Error Exception',
                                           p_distribution_list => pl_dflt_email,
                                           p_module => 'AP');
    WHEN OTHERS THEN
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := l_sec || l_err_msg || ' ERROR ' || SQLCODE ||
                    substr(SQLERRM, 1, 2000);
      --dbms_output.put_line('Section: ' || l_err_msg);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      utl_file.fclose(l_file_handle_recon);
      utl_file.fremove(l_file_dir, l_file_name_temp);
      retcode := l_err_code;
      errbuf  := l_err_msg;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_procedure_name,
                                           p_calling => l_sec,
                                           p_request_id => fnd_global.conc_request_id,
                                           p_ora_error_msg => SQLERRM,
                                           p_error_desc => 'Error running AP Freight Payment package with Program Error Exception',
                                           p_distribution_list => pl_dflt_email,
                                           p_module => 'AP');
    
  END create_pmt_file;


  /*******************************************************************************
  
  * Procedure:   UC4_FRT_PAY
  
  * Description: This is for UC4 to start the concurrent request to extract Freight Payment Info
  
  *              for Transportation Management System
  
  *
  
  HISTORY
  
  ===============================================================================
  
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  
  ------- -----------   --------------- -----------------------------------------
  
  1.0     09/24/2010    Kathy Poling    Initial creation of the procedure
  1.1     07/25/2011    Jonathan Gaviria  Modified for Freight Pay File Extract execution via UC4
  
  
  
  ********************************************************************************/



  PROCEDURE uc4_frt_pay(errbuf  OUT VARCHAR2
                       ,retcode OUT NUMBER) IS
  
    --
  
    -- Package Variables
  
    --
  
    l_package            VARCHAR2(50) := 'xxcusap_frgt_pmt_pkg.create_pmt_file';
    l_dflt_email         VARCHAR2(200) := 'hdsoracleapsupport@hdsupply.com';
    l_email              fnd_user.email_address%TYPE;
    l_req_id             NUMBER NULL;
    v_phase              VARCHAR2(50);
    v_status             VARCHAR2(50);
    v_dev_status         VARCHAR2(50);
    v_dev_phase          VARCHAR2(50);
    v_message            VARCHAR2(250);
    v_error_message      VARCHAR2(3000);
    v_supplier_id        NUMBER;
    v_rec_cnt            NUMBER := 0;
    l_message            VARCHAR2(150);
    l_errormessage       VARCHAR2(3000);
    pl_errorstatus       NUMBER;
    l_can_submit_request BOOLEAN := TRUE;
    l_globalset          VARCHAR2(100);
    l_err_msg            VARCHAR2(3000);
    l_err_code           NUMBER;
    l_sec                VARCHAR2(255);
    l_statement          VARCHAR2(9000);
    l_user               fnd_user.user_id%TYPE; --REBTINTERFACE user
  
  
  
    -- Error DEBUG  
    l_err_callfrom  VARCHAR2(75) DEFAULT 'XXCUS_ERROR_PKG';
    l_err_callpoint VARCHAR2(75) DEFAULT 'START';
    l_distro_list   VARCHAR2(75) DEFAULT 'luong.vu@hdsupply.com' --'HDSOracleDevelopers@hdsupply.com'
    ;
  
  BEGIN
    l_sec := 'UC4 call to run concurrent request Freight Pay File Extract job.';
  
    l_req_id := fnd_request.submit_request('XXCUS', 'XXCUS_FRGT', NULL, NULL,
                                           FALSE);
    COMMIT;
  
    IF (l_req_id != 0) THEN
      IF fnd_concurrent.wait_for_request(l_req_id, 60, 15000, v_phase,
                                         v_status, v_dev_phase, v_dev_status,
                                         v_message) THEN
        v_error_message := chr(10) || 'ReqID=' || l_req_id || ' DPhase ' ||
                           v_dev_phase || ' DStatus ' || v_dev_status ||
                           chr(10) || ' MSG - ' || v_message;
        -- Error Returned      
        IF v_dev_phase != 'COMPLETE'
           OR v_dev_status != 'NORMAL' THEN
          l_statement := 'An error occured running the xxcusap_frgt_pmt_pkg Freight Create Pay File Process' ||
                         v_error_message || '.';
          fnd_file.put_line(fnd_file.log, l_statement);
          fnd_file.put_line(fnd_file.output, l_statement);
          RAISE program_error;
        END IF;
      
        -- Then Success!
      
      ELSE
        l_statement := 'An error occured running the xxcusap_frgt_pmt_pkg Freight Create Pay File Process' ||
                       v_error_message || '.';
        fnd_file.put_line(fnd_file.log, l_statement);
        fnd_file.put_line(fnd_file.output, l_statement);
        RAISE program_error;
      END IF;
    
    ELSE
      l_statement := 'An error occured running the xxcusap_frgt_pmt_pkg Freight Create Pay File Process';
      fnd_file.put_line(fnd_file.log, l_statement);
      fnd_file.put_line(fnd_file.output, l_statement);
      RAISE program_error;
    END IF;
  
    /*   dbms_output.put_line(l_sec);  
    dbms_output.put_line('Request ID:  ' || l_req_id);*/
  
  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;
      l_err_code := 2;
      l_err_msg  := l_sec;
      l_err_msg  := l_err_msg || ' ERROR ' || substr(SQLERRM, 1, 2000);
      --      dbms_output.put_line(l_err_msg);    
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      l_err_callpoint := l_message;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_err_callfrom,
                                           p_calling => l_err_callpoint,
                                           p_request_id => l_req_id,
                                           p_ora_error_msg => SQLERRM,
                                           p_error_desc => 'Error running xxcusap_frgt_pmt_pkg package with PROGRAM ERROR',
                                           p_distribution_list => l_distro_list,
                                           p_module => 'AP');
    
      fnd_file.put_line(fnd_file.output, 'Fix the error!');
    
    WHEN OTHERS THEN
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := l_sec;
      l_err_msg  := l_sec || l_err_msg || ' ERROR ' || SQLCODE ||
                    substr(SQLERRM, 1, 2000);
    
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      l_err_callpoint := l_message;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_err_callfrom,
                                           p_calling => l_err_callpoint,
                                           p_request_id => l_req_id,
                                           p_ora_error_msg => substr(l_err_msg,
                                                                      1, 2000),
                                           p_error_desc => 'Error running xxcusap_frgt_pmt_pkg package with OTHERS Exception',
                                           p_distribution_list => l_distro_list,
                                           p_module => 'AP');
    
  END uc4_frt_pay;

END xxcusap_frgt_pmt_pkg;
/
