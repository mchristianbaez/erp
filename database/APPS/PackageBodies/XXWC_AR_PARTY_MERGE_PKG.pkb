CREATE OR REPLACE PACKAGE BODY APPS.XXWC_AR_PARTY_MERGE_PKG
AS
   /**************************************************************************
    *
    * PACKAGE NAME: XXWC_AR_PARTY_MERGE_PKG
    *
    * DESCRIPTION : Program to Merge Customer Parties
    *
    * HISTORY
    * =======
    * VERSION DATE           AUTHOR(S)               DESCRIPTION
    * ------- ----------- ---------------------- ------------------------------------
    * 1.00    12/17/2013   HARSHAVARDHAN YEDLA            Creation
    *
    *************************************************************************/

   PROCEDURE XXWC_AR_PARTY_MERGE_MAIN (ERRBUF    OUT VARCHAR2,
                                       RETCODE   OUT NUMBER)
   AS
      ln_req_id              NUMBER;
      l_dev_phase            VARCHAR2 (10);
      l_dev_status           VARCHAR2 (10);
      l_message              VARCHAR2 (3000);
      l_phase                VARCHAR2 (50);
      l_status               VARCHAR2 (10);
      l_get_request_status   BOOLEAN;
      V_DAT_FILE             VARCHAR2 (50);
      V_DAT_FILE_PATH        VARCHAR2 (60);
      V_CTL_FILE             VARCHAR2 (50);
      V_CTL_FILE_PATH        VARCHAR2 (60);
      V_LOG_FILE_PATH        VARCHAR2 (60);


      CURSOR C1
      IS
         SELECT xapm.ROWID rid, xapm.*
           FROM xxwc.xxwc_ar_party_merge_stg xapm
          WHERE PROCESS_FLAG = 'N';
   BEGIN
      BEGIN
         SELECT attribute1 DAT_FILE,
                attribute2 DAT_FILE_PATH,
                attribute3 CTL_FILE,
                attribute4 CTL_FILE_PATH,
                attribute5 LOG_FILE_PATH
           INTO V_DAT_FILE,
                V_DAT_FILE_PATH,
                V_CTL_FILE,
                V_CTL_FILE_PATH,
                V_LOG_FILE_PATH
           FROM fnd_lookup_values
          WHERE     lookup_type = 'XXWC_LOADER_COMMON_LOOKUP'
                AND LANGUAGE = USERENV ('LANG')
                AND MEANING =
                       (SELECT CONCURRENT_PROGRAM_NAME
                          FROM FND_CONCURRENT_PROGRAMS
                         WHERE CONCURRENT_PROGRAM_ID =
                                  FND_GLOBAL.CONC_PROGRAM_ID)
                AND ENABLED_FLAG = 'Y'
                AND TRUNC (SYSDATE) BETWEEN TRUNC (START_DATE_ACTIVE)
                                        AND TRUNC (
                                               NVL (END_DATE_ACTIVE, SYSDATE));
      EXCEPTION
         WHEN OTHERS
         THEN
            fnd_file.put_line (fnd_file.LOG, 'In Others Of Common Lookup..' || SQLERRM);
      END;


      FND_FILE.PUT_LINE (
         FND_FILE.LOG,
         'V_DAT_FILE,V_DAT_FILE_PATH,V_CTL_FILE,V_CTL_FILE_PATH,V_LOG_FILE_PATH..');
      FND_FILE.PUT_LINE (
         FND_FILE.LOG,
            V_DAT_FILE
         || ','
         || V_DAT_FILE_PATH
         || ','
         || V_CTL_FILE
         || ','
         || V_CTL_FILE_PATH
         || ','
         || V_LOG_FILE_PATH);


      Fnd_file.put_line (fnd_file.LOG, 'Calling Genreic Loader Program...');

      BEGIN
         ln_req_id :=
            fnd_request.submit_request (application   => 'XXWC',
                                        program       => 'XXWCGLP',
                                        description   => NULL,
                                        start_time    => SYSDATE,
                                        sub_request   => FALSE,
                                        argument1     => V_DAT_FILE,
                                        argument2     => V_DAT_FILE_PATH,
                                        argument3     => V_CTL_FILE,
                                        argument4     => V_CTL_FILE_PATH,
                                        argument5     => V_LOG_FILE_PATH);
         COMMIT;

         fnd_file.put_line (fnd_file.LOG,
                            'Request for batch submit..' || ln_req_id);

         IF ln_req_id > 0
         THEN
            fnd_file.put_line (
               fnd_file.LOG,
               'In Submitting the XXWC Genric Loader Program');
         
         END IF;

         l_get_request_status :=
            FND_CONCURRENT.WAIT_FOR_REQUEST (phase        => l_phase,
                                             request_id   => ln_req_id,
                                             INTERVAL     => 10,
                                             max_wait     => 0,
                                             STATUS       => l_status,
                                             dev_phase    => l_dev_phase,
                                             dev_status   => l_dev_status,
                                             MESSAGE      => l_message);
         fnd_file.put_line (fnd_file.LOG, 'Program Phase :' || l_dev_phase);
         fnd_file.put_line (fnd_file.LOG, 'Program Status :' || l_dev_status);
      EXCEPTION
         WHEN OTHERS
         THEN
            FND_FILE.PUT_LINE (FND_FILE.LOG,
                               'In Others Of wait for request...' || SQLERRM);
      END;

      FND_FILE.PUT_LINE (
         FND_FILE.OUTPUT,
         'From Party Name | From Party Number | To Party Name | To Party Number | Batch No');

      FOR I IN C1
      LOOP
         BEGIN
            XXWC_AR_PARTY_MERGE_PROC (i.FROM_PARTY_NO, i.TO_PARTY_NO, I.RID);
         EXCEPTION
            WHEN OTHERS
            THEN
               FND_FILE.PUT_LINE (
                  FND_FILE.LOG,
                     'IN OTHERS OF XXWC_AR_PARTY_MERGE_MAIN BEFORE LOOP ..'
                  || SQLERRM);
         END;

         UPDATE XXWC.XXWC_AR_PARTY_MERGE_STG
            SET process_flag = 'S'
          WHERE PROCESS_FLAG = 'N' AND ROWID = i.rid;

         COMMIT;
      END LOOP;
   EXCEPTION
      WHEN OTHERS
      THEN
         v_errbuf :=
               'Error_Stack...'
            || CHR (10)
            || DBMS_UTILITY.format_error_stack ()
            || CHR (10)
            || ' Error_Backtrace...'
            || CHR (10)
            || DBMS_UTILITY.format_error_backtrace ();

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_AR_PARTY_MERGE_MAIN',
            p_calling             => 'XXWC_AR_PARTY_MERGE_PROC',
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                       REGEXP_REPLACE (v_errbuf,
                                                       '[[:cntrl:]]',
                                                       NULL),
                                       1,
                                       2000),
            p_error_desc          =>    'Error running '
                                     || 'XXWC_AR_PARTY_MERGE_PKG'
                                     || '.'
                                     || 'XXWC_AR_PARTY_MERGE_MAIN',
            p_distribution_list   => 'HDSOracleDevelopers@hdsupply.com',
            p_module              => 'ONT');
   END XXWC_AR_PARTY_MERGE_MAIN;


   PROCEDURE XXWC_AR_PARTY_MERGE_PROC (p_from_partyno   IN VARCHAR2,
                                       p_to_partyno     IN VARCHAR2,
                                       P_RID               VARCHAR2)
   AS
      CURSOR C1 (p_party_id NUMBER)
      IS
         SELECT *
           FROM hz_party_sites
          WHERE party_id = p_party_id;

      V_TO_PARTY_NAME     VARCHAR2 (200);
      v_to_party_number   VARCHAR2 (30);
      v_to_party_id       NUMBER;
      V_FM_PARTY_NAME     VARCHAR2 (200);
      v_fm_party_number   VARCHAR2 (30);
      V_RETURN_STATUS     VARCHAR2 (2);
      V_BATCH_ID          NUMBER;
      V_FM_PARTY_ID       NUMBER;
      V_BATCH_PARTY_ID    NUMBER;
      V_MSG_COUNT         NUMBER;
      V_MSG_DATA          VARCHAR2 (2000);
      L_REQ_ID            NUMBER;
      v_error_flag        VARCHAR2 (2) := 'N';
      v_error_msg         VARCHAR2 (3000);
   BEGIN
      BEGIN
         SELECT party_name, party_number, party_id
           INTO v_to_party_name, v_to_party_number, v_to_party_id
           FROM hz_parties
          WHERE     PARTY_NUMBER = REPLACE (p_to_partyno, CHR (13))
                AND party_type = 'ORGANIZATION';
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            FND_FILE.PUT_LINE (
               FND_FILE.LOG,
               'P_TO_PARTYNO DOES NOT EXISTS..' || p_to_partyno);
            v_error_flag := 'Y';
         WHEN OTHERS
         THEN
            FND_FILE.PUT_LINE (
               FND_FILE.LOG,
               'IN OTHERS OF VALIDATING P_TO_PARTYNO ..' || SQLERRM);
            v_error_flag := 'Y';
            v_error_msg := 'to party' || SQLERRM;
      END;

      FND_FILE.PUT_LINE (
         FND_FILE.LOG,
            '...v_to_party_name,v_to_party_number,v_to_party_id...'
         || v_to_party_name
         || ','
         || v_to_party_number
         || ','
         || v_to_party_id);

      BEGIN
         SELECT party_name, party_number, party_id
           INTO v_fm_party_name, v_fm_party_number, v_fm_party_id
           FROM hz_parties
          WHERE     PARTY_NUMBER = REPLACE (p_from_partyno, CHR (13))
                AND party_type = 'ORGANIZATION';
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            FND_FILE.PUT_LINE (
               FND_FILE.LOG,
               'P_FROM_PARTYNO DOES NOT EXISTS..' || p_from_partyno);
            v_error_flag := 'Y';
         WHEN OTHERS
         THEN
            FND_FILE.PUT_LINE (
               FND_FILE.LOG,
               'IN OTHERS OF VALIDATING p_from_partyno ..' || SQLERRM);
            v_error_flag := 'Y';
            v_error_msg := 'from party' || SQLERRM;
      END;

      FND_FILE.PUT_LINE (
         FND_FILE.LOG,
            '...v_fm_party_name,v_fm_party_number,v_to_party_id...'
         || v_fm_party_name
         || ','
         || v_fm_party_number
         || ','
         || v_fm_party_id);


      --------------------------------------------
      ----Creating batch for party merger..
      --------------------------------------------

      BEGIN
         HZ_MERGE_BATCH_PUB.create_merge_batch (
            p_api_version         => 1.0,
            p_batch_name          => v_to_party_name, --||'--Fm '||v_fm_party_number||'--to '||v_to_party_number,
            p_batch_commit        => 'B',
            p_batch_delete        => 'N',
            p_merge_reason_code   => 'MERGER',
            x_return_status       => v_return_status,
            x_msg_count           => v_msg_count,
            x_msg_data            => v_msg_data,
            x_batch_id            => v_batch_id);
      EXCEPTION
         WHEN OTHERS
         THEN
            FND_FILE.PUT_LINE (FND_FILE.LOG,
                               'IN OTHERS OF CREATE MERGE BATCH' || SQLERRM);
      END;


      IF v_return_status <> 'S'
      THEN
         fnd_file.put_line (fnd_file.LOG, 'v_msg_data..' || v_msg_data);
         v_error_flag := 'Y';
         v_error_msg := 'HZ_MERGE_BATCH_PUB.create_merge_batch' || v_msg_data;
      ELSE
         COMMIT;
         fnd_file.put_line (fnd_file.LOG, 'v_batch_id..' || v_batch_id);
         fnd_file.put_line (fnd_file.LOG,
                            'v_return_status..' || v_return_status);
      END IF;


      --------------------------------------------
      ----Creating batch for party merger..
      --------------------------------------------

      BEGIN
         HZ_MERGE_PARTIES_PUB.create_merge_party (
            p_api_version         => 1.0,
            p_batch_id            => v_batch_id,
            p_merge_type          => 'PARTY_MERGE',
            p_from_party_id       => v_fm_party_id,
            p_to_party_id         => v_to_party_id,
            p_merge_reason_code   => 'MERGER',
            x_return_status       => v_return_status,
            x_msg_count           => v_msg_count,
            x_msg_data            => v_msg_data,
            x_batch_party_id      => V_batch_party_id);
      EXCEPTION
         WHEN OTHERS
         THEN
            FND_FILE.PUT_LINE (FND_FILE.LOG,
                               'IN OTHERS OF CREATE MERGE BATCH' || SQLERRM);
      END;


      IF v_return_status <> 'S'
      THEN
         fnd_file.put_line (fnd_file.LOG, 'v_msg_data..' || v_msg_data);
         v_error_flag := 'Y';
         v_error_msg := 'HZ_MERGE_BATCH_PUB.create_merge_batch' || v_msg_data;
      ELSE
         COMMIT;
         fnd_file.put_line (fnd_file.LOG, 'v_batch_id..' || v_batch_id);
         fnd_file.put_line (fnd_file.LOG,
                            'v_return_status..' || v_return_status);
         fnd_file.put_line (fnd_file.LOG,
                            'V_batch_party_ids..' || V_batch_party_id);
      END IF;



      FND_FILE.PUT_LINE (
         FND_FILE.OUTPUT,
            v_fm_party_name
         || '|'
         || v_fm_party_number
         || '|'
         || v_to_party_name
         || '|'
         || v_fm_party_number
         || '|'
         || v_batch_id);

      --------------------

      l_req_id :=
         fnd_request.submit_request (application   => 'AR',
                                     program       => 'ARHPMERGE',
                                     description   => NULL,
                                     start_time    => SYSDATE,
                                     sub_request   => FALSE,
                                     argument1     => v_batch_id,
                                     argument2     => 'N');
      COMMIT;


      fnd_file.put_line (fnd_file.LOG,
                         'Request for batch submit..' || l_req_id);

      -----------------------------

      IF v_error_flag = 'Y'
      THEN
         UPDATE XXWC.XXWC_AR_PARTY_MERGE_STG
            SET process_flag = 'E'
               ,error_msg=v_error_msg
          WHERE PROCESS_FLAG = 'N' AND ROWID = P_RID;

         COMMIT;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         v_errbuf :=
               'Error_Stack...'
            || CHR (10)
            || DBMS_UTILITY.format_error_stack ()
            || CHR (10)
            || ' Error_Backtrace...'
            || CHR (10)
            || DBMS_UTILITY.format_error_backtrace ();

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_AR_PARTY_MERGE_PKG',
            p_calling             => 'XXWC_AR_PARTY_MERGE_PROC',
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                       REGEXP_REPLACE (v_errbuf,
                                                       '[[:cntrl:]]',
                                                       NULL),
                                       1,
                                       2000),
            p_error_desc          =>    'Error running '
                                     || 'XXWC_AR_PARTY_MERGE_PKG'
                                     || '.'
                                     || 'XXWC_AR_PARTY_MERGE_PROC',
            p_distribution_list   => 'HDSOracleDevelopers@hdsupply.com',
            p_module              => 'ONT');
   END XXWC_AR_PARTY_MERGE_PROC;
END XXWC_AR_PARTY_MERGE_PKG;
/
