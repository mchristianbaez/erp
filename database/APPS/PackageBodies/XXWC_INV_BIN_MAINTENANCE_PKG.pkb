create or replace PACKAGE BODY           XXWC_INV_BIN_MAINTENANCE_PKG AS

   /*****************************************************************************************************************************************
   *   $Header XXWC_INV_BIN_MAINTENANCE_PKG $                                                                                               *
   *   Module Name: XXWC_INV_BIN_MAINTENANCE_PKG                                                                                            *
   *                                                                                                                                        *
   *   PURPOSE:   This package is used by the extensions in MSCA Bin Maintenance Mobile Pages                                               *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        09-FEB-2015  Lee Spitzer               TMS Ticket 20150302-00164                                                          *
   *                                                     RF - Bin maintenance form                                                          *
   *   1.1        08-JUL-2015  Lee Spitzer               TMS Ticket 20150714-00142 RF - Implement fixes to include with release 2           *
   *   1.2        27-JAN-2016  Lee Spitzer               TMS Ticket 20151103-00188 Mobile Stocking                                          *
   *   1.3        09-Mar-2017  Ashwin Sridhar           Added Debug messages and logic for TMS#20170126-00027 in the procedure WC_LOCATOR   *
   *****************************************************************************************************************************************/


  
  
  PROCEDURE DEBUG_LOG (P_MESSAGE   VARCHAR2)
  IS
   
    
       PRAGMA AUTONOMOUS_TRANSACTION;
       
       BEGIN
    
        IF g_debug = 'Y' THEN
      
          FND_LOG.STRING (G_LOG_LEVEL, upper(G_PACKAGE||'.'||G_CALL_FROM) ||' '|| G_CALL_POINT, P_MESSAGE);
         
          COMMIT;
        
        END IF;
  
  EXCEPTION
  
    WHEN others THEN
        
        g_sqlcode := SQLCODE;
        g_sqlerrm := SQLERRM;
        g_message := 'Error in generating debug log ';
    
            xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                 , p_calling             => g_call_point
                                                 , p_ora_error_msg       => SQLERRM
                                                 , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                 , p_distribution_list   => g_distro_list
                                                 , p_module              => g_module);

   
  END DEBUG_LOG;
  
  
     /*****************************************************************************************************************************************
     *  FUNCTION WC_BRANCH_LOCATOR_CONTROL                                                                                                    *
     *                                                                                                                                        *
     *   PURPOSE:   This function is to return if the subinventory has locators assigned to it                                                *
     *                                                                                                                                        *
     *    Parameters: p_organization_id IN  VARCHAR2                                                                                            *
     *                p_subinventory_code IN VARCHAR2                                                                                         *
     *    Return : Count of locators tied to subinventory for the organization                                                                *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        09-FEB-2015  Lee Spitzer               TMS Ticket 20150302-00164                                                          *
     *                                                     RF - Bin maintenance form                                                          *
     *****************************************************************************************************************************************/


      FUNCTION WC_BRANCH_LOCATOR_CONTROL ( p_organization_id      IN VARCHAR2
                                         , p_subinventory_code   IN VARCHAR2)
              RETURN NUMBER
      IS
      
        l_organization_id NUMBER;
        l_count NUMBER DEFAULT 0; 
        
      BEGIN
        
        g_call_from := 'WC_BRANCH_LOCATOR_CONTROL';
        g_call_point := 'Start';
 
    
        IF g_debug = 'Y' THEN
              debug_log('p_organization_id ' || p_organization_id);
              debug_log('p_subinventory_code ' || p_subinventory_code);
        END IF;
    
      
        g_call_point := 'Convert organization_id to number';
        
        BEGIN
          SELECT to_number(p_organization_id) 
          INTO   l_organization_Id
          from   dual;
        EXCEPTION
          WHEN others THEN
              g_sqlcode := SQLCODE;
              g_sqlerrm := SQLERRM;
              g_message := 'Error in converting organization_id to number ';
              raise g_exception;
        END;
        
          debug_log('l_organizaion_id ' || l_organization_id);
          
        g_call_point := 'Calling XXWC_MWA_ROUTINES_PKG.WC_BRANCH_LOCATOR_CONTROL';
        
          BEGIN
            l_count := XXWC_MWA_ROUTINES_PKG.WC_BRANCH_LOCATOR_CONTROL( p_organization_id => l_organization_id
                                                                      , p_subinventory_code => p_subinventory_code);
          EXCEPTION
            WHEN others THEN
                g_sqlcode := SQLCODE;
                g_sqlerrm := SQLERRM;
                g_message := 'Error getting count in XXWC_MWA_ROUTINES_PKG.WC_BRANCH_LOCATOR_CONTROL';
                raise g_exception;
          END;
          
          
        return l_count;
        
      EXCEPTION
        
        WHEN g_exception THEN
        
          RETURN l_count;
          
            debug_log('l_count ' || l_count);
          
            g_message := g_message || ' ' || g_call_point || g_sqlcode || g_sqlerrm;
            xxcus_error_pkg.xxcus_error_main_api (  p_called_from        => g_package||'.'||g_call_from
                                                , p_calling             => g_call_point
                                                , p_ora_error_msg       => SQLERRM
                                                , p_error_desc          => 'Error in call ' || g_call_from || g_call_point
                                                , p_distribution_list   => g_distro_list
                                                , p_module              => g_module);

        
        WHEN others THEN
         
         RETURN l_count;
          
            debug_log('l_count ' || l_count);
          
            g_message := 'Error ' || g_message;
      
           xxcus_error_pkg.xxcus_error_main_api (  p_called_from        => g_package||'.'||g_call_from
                                                , p_calling             => g_call_point
                                                , p_ora_error_msg       => SQLERRM
                                                , p_error_desc          => 'Error in call ' || g_call_from || g_call_point
                                                , p_distribution_list   => g_distro_list
                                                , p_module              => g_module);

      
      END WC_BRANCH_LOCATOR_CONTROL;
      
     /*****************************************************************************************************************************************
     *   PROCEDURE PROCESS_ITEM_LOCATOR_TIE                                                                                                    *
     *                                                                                                                                        *
     *   PURPOSE:   This procedure is used to update or insert records into the MTL_SECONDARY_LOCATORS                                        *
     *                                                                                                                                        *
     *    Parameters: p_organization_id  IN NUMBER - organization_id                                                                          *
     *                p_inventory_item_id IN NUMBER -- inventory_item_id                                                                      *
     *                p_subinventory_code IN VARCHAR2 - subinventory_code                                                                     *
     *                p_locator_id IN NUMBER --LOCATOR_ID                                                                                     *
     *                                                                                                                                        *
     *    Out :  X_RETURN = 0 = Success, 1 = Error                                                                                            *
     *           X_MESSAGE = Return message                                                                                                   *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        22-OCT-2014  Lee Spitzer               TMS Ticket 20150302-00164                                                          *
     *                                                     RF - Bin maintenance form                                                          *
     *********y********************************************************************************************************************************/


        PROCEDURE PROCESS_ITEM_LOCATOR_TIE       ( p_organization_id    IN VARCHAR2
                                                 , p_inventory_item_id  IN VARCHAR2
                                                 , p_subinventory_code  IN VARCHAR2
                                                 , p_locator_id IN VARCHAR2
                                                 , x_return  OUT NUMBER
                                                 , x_message OUT VARCHAR2)
                  IS
            
            l_organization_id NUMBER;
            l_inventory_item_id NUMBER;
            l_locator_id NUMBER;
            l_max_exists NUMBER;
            l_max_count  NUMBER;
            l_max_count_0 NUMBER;
            l_max NUMBER := 9;
            l_prefix VARCHAR2(2);
            
              
        BEGIN
        
          g_call_from := 'PROCESS_ITEM_LOCATOR_TIE';
          g_call_point := 'Start';
      
          debug_log('p_organization_id ' || p_organization_id);
          debug_log('p_inventory_item_id ' || p_inventory_item_id);
          debug_log('p_subinventory_code ' || p_subinventory_code);
          debug_log('p_locator_id ' || p_locator_id);
        
          BEGIN 
            SELECT to_number(p_organization_id),
                   to_number(p_inventory_item_id),
                   to_number(p_locator_id)
            INTO  l_organization_id,
                  l_inventory_item_id,
                  l_locator_id
            FROM  dual;
          EXCEPTION
            WHEN others THEN
              g_sqlcode := SQLCODE;
              g_sqlerrm := SQLERRM;
              g_message := 'Could not convert parameters to number';
              raise g_exception;
          END;
                
      
                 --Check to see if max is already at 9
        BEGIN
          SELECT  count(*)
          INTO    l_max_exists
          FROM   wms_item_locations_kfv wil,
                   mtl_secondary_locators msl
          WHERE  wil.organization_id = p_organization_id
          AND    msl.organization_id = wil.organization_id
          AND    msl.inventory_item_id = p_inventory_item_id
          AND    msl.secondary_locator = wil.inventory_location_id
          AND    WC_LOCATOR_PREFIX(wil.locator_segments) = l_max
          ;
        EXCEPTION
          WHEN others THEN
            l_max_exists := 0;
        END;
        
        
        --Check to see if max is already at 9
        BEGIN
          SELECT  count(*)
          INTO    l_max_count
          FROM   wms_item_locations_kfv wil,
                   mtl_secondary_locators msl
          WHERE  wil.organization_id = p_organization_id
          AND    msl.organization_id = wil.organization_id
          AND    msl.inventory_item_id = p_inventory_item_id
          AND    msl.secondary_locator = wil.inventory_location_id
          AND   WC_LOCATOR_PREFIX(wil.locator_segments) != '0'
          ;
        EXCEPTION
          WHEN others THEN
            l_max_count := 0;
        END;
        
        
         --Check to see if max is already at 9
        BEGIN
          SELECT  count(*)
          INTO    l_max_count_0
          FROM   wms_item_locations_kfv wil,
                   mtl_secondary_locators msl
          WHERE  wil.organization_id = p_organization_id
          AND    msl.organization_id = wil.organization_id
          AND    msl.inventory_item_id = p_inventory_item_id
          AND    msl.secondary_locator = wil.inventory_location_id
          ;
        EXCEPTION
          WHEN others THEN
            l_max_count_0 := 0;
        END;
        
        BEGIN
          SELECT WC_LOCATOR_PREFIX(wil.locator_segments)
          INTO   l_prefix
          FROM   wms_item_locations_kfv wil
          WHERE  wil.organization_id = p_organization_id
          AND    wil.inventory_item_id = p_inventory_item_id
          AND    wil.inventory_location_id = l_locator_id
          ;
        EXCEPTION
          WHEN others THEN
              l_prefix := NULL;
        END;
        
        
         IF l_max_count > l_max THEN
          g_message := 'Assigning locator will create a location above ' || l_max || '.  Unable to create locator.  Max error.';
          raise g_exception;
         end if;
        
         IF l_prefix = '0' AND l_max_count_0 > l_max + 1 THEN
          g_message := 'Assigning locator will create a location above ' || l_max || '.  Unable to create locator. Max 0 count error.';
          raise g_exception;
         END IF;
         
         
         
         XXWC_MWA_ROUTINES_PKG.PROCESS_ITEM_LOCATOR_TIE ( p_organization_id => l_organization_id
                                                        , p_inventory_item_id => l_inventory_item_id
                                                        , p_subinventory_code => p_subinventory_code
                                                        , p_locator_id => l_locator_id
                                                        , x_return => x_return
                                                        , x_message => x_message);
                                                         
        EXCEPTION

           WHEN g_exception THEN
           
            x_return := 1;
            x_message := g_message;
            

          WHEN OTHERS THEN
        
            g_sqlcode := SQLCODE;
            g_sqlerrm := SQLERRM;
            g_message := 'Error in PROCESS_ITEM_LOCATOR_TIE ';
            
                xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                     , p_calling             => g_call_point
                                                     , p_ora_error_msg       => SQLERRM
                                                     , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                     , p_distribution_list   => g_distro_list
                                                     , p_module              => g_module);
      
        
        
        END PROCESS_ITEM_LOCATOR_TIE;


     /*****************************************************************************************************************************************
     *   PROCEDURE DELETE_ITEM_LOCATOR_TIE                                                                                                    *
     *                                                                                                                                        *
     *   PURPOSE:   This procedure is used to remove records into the MTL_SECONDARY_LOCATORS                                                  *
     *                                                                                                                                        *
     *    Parameters: p_organization_id  IN NUMBER - organization_id                                                                          *
     *                p_inventory_item_id IN NUMBER -- inventory_item_id                                                                      *
     *                p_subinventory_code IN VARCHAR2 - subinventory_code                                                                     *
     *                p_locator_id IN NUMBER --LOCATOR_ID                                                                                     *
     *                                                                                                                                        *
     *    Out :  X_RETURN = 0 = Success, 1 = Error                                                                                            *
     *           X_MESSAGE = Return message                                                                                                   *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        22-OCT-2014  Lee Spitzer               TMS Ticket 20150302-00164                                                          *
     *                                                     RF - Bin maintenance form                                                          *
     *********y********************************************************************************************************************************/

        PROCEDURE DELETE_ITEM_LOCATOR_TIE        ( p_organization_id    IN VARCHAR2
                                                 , p_inventory_item_id  IN VARCHAR2
                                                 , p_subinventory_code  IN VARCHAR2
                                                 , p_locator_id IN VARCHAR2
                                                 , x_return  OUT NUMBER
                                                 , x_message OUT VARCHAR2)
          IS
          
            l_organization_id NUMBER;
            l_inventory_item_id NUMBER;
            l_locator_id NUMBER;
              
        BEGIN
        
          g_call_from := 'DELETE_ITEM_LOCATOR_TIE';
          g_call_point := 'Start';
      
          debug_log('p_organization_id ' || p_organization_id);
          debug_log('p_inventory_item_id ' || p_inventory_item_id);
          debug_log('p_subinventory_code ' || p_subinventory_code);
          debug_log('p_locator_id ' || p_locator_id);
        
          BEGIN 
            SELECT to_number(p_organization_id),
                   to_number(p_inventory_item_id),
                   to_number(p_locator_id)
            INTO  l_organization_id,
                  l_inventory_item_id,
                  l_locator_id
            FROM  dual;
          EXCEPTION
            WHEN others THEN
              g_sqlcode := SQLCODE;
              g_sqlerrm := SQLERRM;
              g_message := 'Could not convert parameters to number';
              raise g_exception;
          END;
                
      
          g_call_point := 'Calling XXWC_MWA_ROUTINES_PKG.DELETE_ITEM_LOCATOR_TIE';
      
      
         XXWC_MWA_ROUTINES_PKG.DELETE_ITEM_LOCATOR_TIE  ( p_organization_id => l_organization_id
                                                        , p_inventory_item_id => l_inventory_item_id
                                                        , p_subinventory_code => p_subinventory_code
                                                        , p_locator_id => l_locator_id
                                                        , x_return => x_return
                                                        , x_message => x_message);
                                                            
        EXCEPTION

          WHEN g_exception THEN
            
            g_message := 'Error in DELETE_ITEM_LOCATOR_TIE ' || g_sqlcode || g_sqlerrm;
            x_return := 1;
            x_message := g_message;
                xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                     , p_calling             => g_call_point
                                                     , p_ora_error_msg       => SQLERRM
                                                     , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                     , p_distribution_list   => g_distro_list
                                                     , p_module              => g_module);
          WHEN OTHERS THEN
            
            g_sqlcode := SQLCODE;
            g_sqlerrm := SQLERRM;
            g_message := 'Error in DELETE_ITEM_LOCATOR_TIE ' || g_sqlcode || g_sqlerrm;
            x_return := 1;
            x_message := g_message;
                xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                     , p_calling             => g_call_point
                                                     , p_ora_error_msg       => SQLERRM
                                                     , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                     , p_distribution_list   => g_distro_list
                                                     , p_module              => g_module);
      

       END DELETE_ITEM_LOCATOR_TIE;
       
       
    /*****************************************************************************************************************************************
    *   PROCEDURE GET_WC_BIN_ITEM_LOC                                                                                                        *
    *                                                                                                                                        *
    *   PURPOSE:   This procedure is used to get the branch bin assignments for an item organization combination                             *
    *                                                                                                                                        *
    *                                                                                                                                        *
    *   REVISIONS:                                                                                                                           *
    *   Ver        Date        Author                     Description                                                                        *
    *   ---------  ----------  ---------------         -------------------------                                                             *
    *   1.0        22-OCT-2014  Lee Spitzer               TMS Ticket 20150302-00164                                                          *
    *                                                     RF - Bin maintenance form                                                          *
    *****************************************************************************************************************************************/
  
  
    PROCEDURE GET_WC_BIN_ITEM_LOC   ( x_locators               OUT    NOCOPY t_genref --0
                                    , p_organization_id        IN     NUMBER    --1
                                    , p_subinventory_code      IN     VARCHAR2  --2
                                    , p_restrict_locators_code IN     NUMBER    --3
                                    , p_inventory_item_id      IN     VARCHAR2    --4
                                    , p_concatenated_segments  IN     VARCHAR2  --5
                                    , p_project_id             IN     NUMBER    --6
                                    , p_task_id                IN     NUMBER    --7
                                    --, p_alias                  IN     VARCHAR2
                                    ) IS
         
        
    BEGIN

      g_call_from := 'GET_WC_BIN_ITEM_LOC';
      g_call_point := 'Start';
      
      
        debug_log('p_organization_id ' || p_organization_id);
        debug_log('p_subinventory_code ' || p_subinventory_code);
        debug_log('p_restrict_locators_code  ' || p_restrict_locators_code);
        debug_log('p_inventory_item_id ' || p_inventory_item_id);
        debug_log('p_concatenated_segments ' || p_concatenated_segments);
        debug_log('p_project_id ' || p_project_id);
        debug_log('p_task_id ' || p_task_id);
        --debug_log('p_alias ' || p_alias);
        
    
          OPEN x_locators FOR
          --Primary Bin Query
          SELECT   loc.wc_locator
                 , loc.wc_pre_fix
                 , loc.inventory_location_id
                 , loc.concatenated_segments
                 , loc.description
          FROM  (SELECT   WC_LOCATOR(wil.locator_segments) wc_locator
                        , WC_LOCATOR_PREFIX(wil.locator_segments) wc_pre_fix
                        , wil.inventory_location_id
                        , wil.locator_segments concatenated_segments
                        , wil.description
                 FROM wms_item_locations_kfv wil
                 WHERE wil.organization_id = p_organization_id
                 AND NVL(wil.disable_date, TRUNC(SYSDATE + 1)) > TRUNC(SYSDATE)
                 AND wil.subinventory_code = p_subinventory_code
                 AND wil.PROJECT_ID IS NULL
                 AND wil.task_id IS NULL
                 AND  WC_LOCATOR(wil.locator_segments) like nvl(p_concatenated_segments, WC_LOCATOR(wil.locator_segments))
                 --Secondary Locators
                 AND EXISTS (SELECT *
                             FROM   mtl_secondary_locators msl
                             WHERE wil.organization_id = msl.organization_id
                             AND wil.subinventory_code = msl.subinventory_code
                             AND wil.inventory_location_id = msl.secondary_locator
                             AND msl.inventory_item_id = p_inventory_item_id)) loc
                             
              order by loc.wc_pre_fix, loc.wc_locator, loc.concatenated_segments;
                 
    
    EXCEPTION
    
      WHEN others THEN
             g_sqlcode := SQLCODE;
             g_sqlerrm := SQLERRM;
             g_message := 'when others error ';
             debug_log(g_message || g_sqlcode || g_sqlerrm);
    
            xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                 , p_calling             => g_call_point
                                                 , p_ora_error_msg       => SQLERRM
                                                 , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                 , p_distribution_list   => g_distro_list
                                                 , p_module              => g_module);


    
    END  GET_WC_BIN_ITEM_LOC;


     /*****************************************************************************************************************************************
     *  FUNCTION WC_LOCATOR                                                                                                                   *
     *                                                                                                                                        *
     *   PURPOSE:   This function is to return if the locator exists                                                                          *
     *                                                                                                                                        *
     *    Parameters: p_organization_id IN  VARCHAR2                                                                                          *
     *                p_subinventory_code IN VARCHAR2                                                                                         *
     *    Return : Count of locators tied to subinventory for the organization                                                                *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        09-FEB-2015  Lee Spitzer            TMS Ticket 20150302-00164                                                          *
     *                                                  RF - Bin maintenance form   
     *   1.1        09-Mar-2017  Ashwin Sridhar         Added Debug messages and logic for TMS#20170126-00027
     *****************************************************************************************************************************************/



      FUNCTION WC_LOCATOR ( p_locator              IN VARCHAR2)
              RETURN VARCHAR2 IS
              
          l_locator VARCHAR2(40) DEFAULT p_locator;
          --l_organization_id NUMBER; --Commented by Ashwin.s on 09-Mar-2017 for TMS#20170126-00027
          ln_user_id NUMBER;          --Added by Ashwin.s on 09-Mar-2017 for TMS#20170126-00027
          lv_user_name VARCHAR2(100); --Added by Ashwin.s on 09-Mar-2017 for TMS#20170126-00027
          
      BEGIN
      
      g_call_from := 'WC_LOCATOR';
      g_call_point := 'Start';      
      
      debug_log('p_locator ' || p_locator);   

      --Added the IF condition by Ashwin.s on 09-Mar-2017 for TMS#20170126-00027
      IF ln_user_id IS NOT NULL THEN
  
        BEGIN
  
          SELECT user_name
          INTO   lv_user_name
          FROM   fnd_user
          WHERE user_id=ln_user_id;

        EXCEPTION
        WHEN others THEN

          lv_user_name:=null;

        END;
  
      END IF;  
        
        
       l_locator := XXWC_MWA_ROUTINES_PKG.WC_LOCATOR(p_locator => p_locator);

       --Added by Ashwin.s on 09-Mar-2017 for TMS#20170126-00027
       debug_log('l_locator ' || l_locator); 
       debug_log('lv_user_name ' || lv_user_name);
                                                                
        return l_locator;
        
    EXCEPTION
    
        WHEN g_exception THEN
            
            --Added this debug for locator by Ashwin.S on 09-Mar-2017 for TMS#20170126-00027
            g_message := 'Error in WC_LOCATOR '||'-'||' for parameter Locator '||p_locator||' for user '||lv_user_name||' '||g_sqlcode|| g_sqlerrm;
            
                xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                     , p_calling             => g_call_point
                                                     , p_ora_error_msg       => SQLERRM
                                                     , p_error_desc          => g_message
                                                     , p_distribution_list   => g_distro_list
                                                     , p_module              => g_module);
          return l_locator;

          WHEN OTHERS THEN
            
            g_sqlcode := SQLCODE;
            g_sqlerrm := SQLERRM;

            --Added this debug for locator by Ashwin.S on 09-Mar-2017 for TMS#20170126-00027
            g_message := 'Error in WC_LOCATOR '||'-'||' for parameter Locator '||p_locator||' for user '||lv_user_name||' '||g_sqlcode || g_sqlerrm;
            
                xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                     , p_calling             => g_call_point
                                                     , p_ora_error_msg       => SQLERRM
                                                     , p_error_desc          => g_message
                                                     , p_distribution_list   => g_distro_list
                                                     , p_module              => g_module);
        return l_locator;
      
      END WC_LOCATOR;
      
    
     /*****************************************************************************************************************************************
     *  FUNCTION WC_LOCATOR_PREFIX                                                                                                                   *
     *                                                                                                                                        *
     *   PURPOSE:   This function is to return if the locator exists                                                                          *
     *                                                                                                                                        *
     *    Parameters: p_organization_id IN  VARCHAR2                                                                                          *
     *                p_subinventory_code IN VARCHAR2                                                                                         *
     *    Return : Count of locators tied to subinventory for the organization                                                                *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        09-FEB-2015  Lee Spitzer               TMS Ticket 20150302-00164                                                          *
     *                                                     RF - Bin maintenance form                                                          *
     *****************************************************************************************************************************************/

      FUNCTION WC_LOCATOR_PREFIX ( p_locator              IN VARCHAR2)
              RETURN VARCHAR2 IS
              
          l_prefix VARCHAR2(40) DEFAULT '';
          l_organization_id NUMBER;
          
      BEGIN
      
      g_call_from := 'WC_LOCATOR_PREFIX';
      g_call_point := 'Start';
      
      
        debug_log('p_locator ' || p_locator);
        
        
        l_prefix := XXWC_MWA_ROUTINES_PKG.WC_LOCATOR_PREFIX( p_locator => p_locator);
                                                                
        return l_prefix;
        
    EXCEPTION
    
        WHEN g_exception THEN
            
            g_message := 'Error in WC_LOCATOR ' || g_sqlcode || g_sqlerrm;
            return l_prefix;
                xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                     , p_calling             => g_call_point
                                                     , p_ora_error_msg       => SQLERRM
                                                     , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                     , p_distribution_list   => g_distro_list
                                                     , p_module              => g_module);
          WHEN OTHERS THEN
            
            g_sqlcode := SQLCODE;
            g_sqlerrm := SQLERRM;
            g_message := 'Error in WC_LOCATOR ' || g_sqlcode || g_sqlerrm;
            return l_prefix;
                xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                     , p_calling             => g_call_point
                                                     , p_ora_error_msg       => SQLERRM
                                                     , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                     , p_distribution_list   => g_distro_list
                                                     , p_module              => g_module);
      
      END WC_LOCATOR_PREFIX;
      
      
      /*****************************************************************************************************************************************
     *  PROCEDURE CREATE_WC_LOCATOR                                                                                                           *
     *                                                                                                                                        *
     *   PURPOSE:   This function is to return if the locator exists                                                                          *
     *                                                                                                                                        *
     *    Parameters: p_organization_id IN  VARCHAR2                                                                                          *
     *                p_subinventory_code IN VARCHAR2                                                                                         *
     *    Return : Count of locators tied to subinventory for the organization                                                                *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        09-FEB-2015  Lee Spitzer               TMS Ticket 20150302-00164                                                          *
     *                                                     RF - Bin maintenance form                                                          *
     *****************************************************************************************************************************************/



      PROCEDURE CREATE_WC_LOCATOR ( p_organization_id IN VARCHAR2
                                  , p_subinventory_code IN VARCHAR2
                                  , p_locator IN VARCHAR2
                                  , x_return  OUT NUMBER
                                  , x_message OUT VARCHAR2) IS
  
        l_organization_id NUMBER;
      
      BEGIN
      
       g_call_from := 'CREATE_WC_LOCATOR';
       g_call_point := 'Start';
      
        debug_log('p_organization_id ' || p_organization_id);
        debug_log('p_subinventory_code ' || p_subinventory_code);
        debug_log('p_locator ' || p_locator);
        
        
        g_call_point := 'Convert organization_id to number';
        
        BEGIN
          select to_number(p_organization_id)
          into   l_organization_id
          from   dual;
        EXCEPTION
          WHEN others THEN
              g_sqlcode := SQLCODE;
              g_sqlerrm := SQLERRM;
              g_message := 'Error converting organization id to number';
              raise g_exception;
        END;
        
          XXWC_MWA_ROUTINES_PKG.CREATE_WC_LOCATOR( p_organization_id => l_organization_id
                                                 , p_subinventory_code => p_subinventory_code
                                                 , p_locator => p_locator
                                                 , x_return => x_return
                                                 , x_message => x_message);
        
      
      EXCEPTION
    
        WHEN g_exception THEN
            
            g_message := 'Error in CREATE_WC_LOCATOR ' || g_sqlcode || g_sqlerrm;
            x_return := 1;
            x_message := g_message;
                xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                     , p_calling             => g_call_point
                                                     , p_ora_error_msg       => SQLERRM
                                                     , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                     , p_distribution_list   => g_distro_list
                                                     , p_module              => g_module);
          WHEN OTHERS THEN
            
            g_sqlcode := SQLCODE;
            g_sqlerrm := SQLERRM;
            g_message := 'Error in CREATE_WC_LOCATOR ' || g_sqlcode || g_sqlerrm;
            x_return := 1;
            x_message := g_message;
                xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                     , p_calling             => g_call_point
                                                     , p_ora_error_msg       => SQLERRM
                                                     , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                     , p_distribution_list   => g_distro_list
                                                     , p_module              => g_module);
      
    
      
      END CREATE_WC_LOCATOR;
      
    /*****************************************************************************************************************************************
    *   PROCEDURE GET_WC_BIN_LOC                                                                                                             *
    *                                                                                                                                        *
    *   PURPOSE:   This procedure is used to get the branch bin assignments for an item organization combination                             *
    *                                                                                                                                        *
    *                                                                                                                                        *
    *   REVISIONS:                                                                                                                           *
    *   Ver        Date        Author                     Description                                                                        *
    *   ---------  ----------  ---------------         -------------------------                                                             *
    *   1.0        22-OCT-2014  Lee Spitzer               TMS Ticket 20150302-00164                                                          *
    *                                                     RF - Bin maintenance form                                                          *
    *   1.1        02-MAR-2016  Lee Spitzer               TMS Ticket 20160309-00091 Mobile Stocking Changes                                       *
    *****************************************************************************************************************************************/
  
  
    PROCEDURE GET_WC_BIN_LOC       ( x_locators               OUT    NOCOPY t_genref --0
                                    , p_organization_id        IN     NUMBER    --1
                                    , p_subinventory_code      IN     VARCHAR2  --2
                                    , p_restrict_locators_code IN     NUMBER    --3
                                    , p_inventory_item_id      IN     VARCHAR2    --4
                                    , p_concatenated_segments  IN     VARCHAR2  --5
                                    , p_project_id             IN     NUMBER    --6
                                    , p_task_id                IN     NUMBER    --7
                                    , p_hide_prefix            IN     VARCHAR2
                                    --, p_alias                  IN     VARCHAR2
                                    ) IS
         
     
     l_next_bin_prefix VARCHAR2(100); 
     l_concatenated_segments VARCHAR2(40);--Added 3/2/2016 TMS 20160309-00091
        
    BEGIN

      g_call_from := 'GET_WC_BIN_LOC';
      g_call_point := 'Start';
      
      
        debug_log('p_organization_id ' || p_organization_id);
        debug_log('p_subinventory_code ' || p_subinventory_code);
        debug_log('p_restrict_locators_code  ' || p_restrict_locators_code);
        debug_log('p_inventory_item_id ' || p_inventory_item_id);
        debug_log('p_concatenated_segments ' || p_concatenated_segments);
        debug_log('p_project_id ' || p_project_id);
        debug_log('p_task_id ' || p_task_id);
        debug_log('p_hide_prefix ' || p_hide_prefix);
        --debug_log('p_alias ' || p_alias);
        
        
        
        BEGIN --Added 3/2/2016 TMS 20160309-00091
          SELECT upper(p_concatenated_segments)  --Added 3/2/2016 TMS 20160309-00091
          INTO   l_concatenated_segments  --Added 3/2/2016 TMS 20160309-00091
          FROM   dual;  --Added 3/2/2016 TMS 20160309-00091
        EXCEPTION  --Added 3/2/2016 TMS 20160309-00091
          WHEN others THEN  --Added 3/2/2016 TMS 20160309-00091
            g_sqlcode := SQLCODE;  --Added 3/2/2016 TMS 20160309-00091
            g_sqlerrm := SQLERRM;  --Added 3/2/2016 TMS 20160309-00091
            g_message := 'Could not upper ' || p_concatenated_segments;  --Added 3/2/2016 TMS 20160309-00091
            raise g_exception; --Added 3/2/2016 TMS 20160309-00091
        END; --Added 3/2/2016 TMS 20160309-00091
        
        
        debug_log('l_concatenated_segments ' || l_concatenated_segments); --Added 3/2/2016 TMS 20160309-00091
        
        if p_hide_prefix = 'N' THEN
    
         IF p_restrict_locators_code is null or p_restrict_locators_code = 2 THEN
         
              --Show Prefix and Locator Restrict is off
              OPEN x_locators FOR
              --Primary Bin Query
              SELECT   loc.wc_locator
                     , loc.wc_pre_fix
                     , loc.inventory_location_id
                     , loc.concatenated_segments
                     , loc.description
                     , loc.subinventory_code
              FROM  (SELECT --  WC_LOCATOR(wil.locator_segments) wc_locator
                            --, WC_LOCATOR_PREFIX(wil.locator_segments) wc_pre_fix
                              substr(SEGMENT1,instr(SEGMENT1,'-',1,1)+1, LENGTH(SEGMENT1) -  instr(SEGMENT1,'-',1,1)) wc_locator
                            , substr(segment1, 1,instr(segment1,'-',1,1)-1) wc_pre_fix
                            , wil.inventory_location_id
                            , wil.locator_segments concatenated_segments
                            , wil.description
                            , wil.subinventory_code
                     FROM wms_item_locations_kfv wil
                     WHERE wil.organization_id = p_organization_id
                     AND NVL(wil.disable_date, TRUNC(SYSDATE + 1)) > TRUNC(SYSDATE)
                     AND wil.subinventory_code = nvl(p_subinventory_code, wil.subinventory_code)
                     AND wil.PROJECT_ID IS NULL
                     AND wil.task_id IS NULL
                     --AND (substr(SEGMENT1,instr(SEGMENT1,'-',1,1)+1, LENGTH(SEGMENT1) -  instr(SEGMENT1,'-',1,1))) like nvl(p_concatenated_segments,(substr(SEGMENT1,instr(SEGMENT1,'-',1,1)+1, LENGTH(SEGMENT1) -  instr(SEGMENT1,'-',1,1))))) loc --Removed 3/2/2016 TMS 20160309-00091
                     AND (substr(SEGMENT1,instr(SEGMENT1,'-',1,1)+1, LENGTH(SEGMENT1) -  instr(SEGMENT1,'-',1,1))) like nvl(l_concatenated_segments,(substr(SEGMENT1,instr(SEGMENT1,'-',1,1)+1, LENGTH(SEGMENT1) -  instr(SEGMENT1,'-',1,1))))) loc --Added 3/2/2016 TMS 20160309-00091
                  order by loc.wc_pre_fix, loc.wc_locator, loc.concatenated_segments;
                  
         ELSIF p_restrict_locators_code = 3 THEN
         
         
            BEGIN l_next_bin_prefix := nvl(XXWC_INV_BIN_MAINTENANCE_PKG.NEXT_BIN_PREFIX(p_organization_id, p_inventory_item_id),0); END;
            
            debug_log('l_next_bin_prefix ' || l_next_bin_prefix);
         
              --Show Prefix and Locator Restrict is off
              OPEN x_locators FOR
              --Primary Bin Query
              SELECT   loc.wc_locator
                     , loc.wc_pre_fix
                     , loc.inventory_location_id
                     , loc.concatenated_segments
                     , loc.description
                     , loc.subinventory_code
              FROM  (SELECT --  WC_LOCATOR(wil.locator_segments) wc_locator
                            --, WC_LOCATOR_PREFIX(wil.locator_segments) wc_pre_fix
                              substr(SEGMENT1,instr(SEGMENT1,'-',1,1)+1, LENGTH(SEGMENT1) -  instr(SEGMENT1,'-',1,1)) wc_locator
                            , substr(segment1, 1,instr(segment1,'-',1,1)-1) wc_pre_fix
                            , wil.inventory_location_id
                            , wil.locator_segments concatenated_segments
                            , wil.description
                            , wil.subinventory_code
                     FROM wms_item_locations_kfv wil
                     WHERE wil.organization_id = p_organization_id
                     AND NVL(wil.disable_date, TRUNC(SYSDATE + 1)) > TRUNC(SYSDATE)
                     AND wil.subinventory_code = nvl(p_subinventory_code, wil.subinventory_code)
                     AND wil.PROJECT_ID IS NULL
                     AND wil.task_id IS NULL
                     /*AND nvl(substr(wil.segment1, 1,instr(wil.segment1,'-',1,1)-1),0) > nvl((SELECT max(substr(wil2.segment1, 1,instr(wil2.segment1,'-',1,1)-1))
                                                                                       FROM   mtl_secondary_locators msl,
                                                                                              wms_item_locations_kfv wil2
                                                                                       WHERE  msl.inventory_item_id = nvl(p_inventory_item_id, msl.inventory_item_id)
                                                                                       AND    msl.organization_id = p_organization_id
                                                                                       AND    msl.subinventory_code = wil2.subinventory_code
                                                                                       AND    msl.secondary_locator = wil2.inventory_location_id),-1)
                     */
                     AND nvl(substr(wil.segment1, 1,instr(wil.segment1,'-',1,1)-1),0) = l_next_bin_prefix--nvl(XXWC_INV_BIN_MAINTENANCE_PKG.NEXT_BIN_PREFIX(p_organization_id, p_inventory_item_id),0)
                     --AND (substr(SEGMENT1,instr(SEGMENT1,'-',1,1)+1, LENGTH(SEGMENT1) -  instr(SEGMENT1,'-',1,1))) like nvl(p_concatenated_segments,(substr(SEGMENT1,instr(SEGMENT1,'-',1,1)+1, LENGTH(SEGMENT1) -  instr(SEGMENT1,'-',1,1))))) loc --Removed 3/2/2016 TMS 20160309-00091
                     AND (substr(SEGMENT1,instr(SEGMENT1,'-',1,1)+1, LENGTH(SEGMENT1) -  instr(SEGMENT1,'-',1,1))) like nvl(l_concatenated_segments,(substr(SEGMENT1,instr(SEGMENT1,'-',1,1)+1, LENGTH(SEGMENT1) -  instr(SEGMENT1,'-',1,1))))) loc --Added 3/2/2016 TMS 20160309-00091
                  ORDER BY loc.wc_pre_fix, loc.wc_locator, loc.concatenated_segments;

                 
          else
          
              --Show Prefix and Locator Restrict is on
                   OPEN x_locators FOR
              --Primary Bin Query
              SELECT   loc.wc_locator
                     , loc.wc_pre_fix
                     , loc.inventory_location_id
                     , loc.concatenated_segments
                     , loc.description
                     , loc.subinventory_code
              FROM  (SELECT --  WC_LOCATOR(wil.locator_segments) wc_locator
                            --, WC_LOCATOR_PREFIX(wil.locator_segments) wc_pre_fix
                              substr(SEGMENT1,instr(SEGMENT1,'-',1,1)+1, LENGTH(SEGMENT1) -  instr(SEGMENT1,'-',1,1)) wc_locator
                            , substr(segment1, 1,instr(segment1,'-',1,1)-1) wc_pre_fix
                            , wil.inventory_location_id
                            , wil.locator_segments concatenated_segments
                            , wil.description
                            , wil.subinventory_code
                     FROM wms_item_locations_kfv wil
                     WHERE wil.organization_id = p_organization_id
                     AND NVL(wil.disable_date, TRUNC(SYSDATE + 1)) > TRUNC(SYSDATE)
                     AND wil.subinventory_code = nvl(p_subinventory_code, wil.subinventory_code)
                     AND wil.PROJECT_ID IS NULL
                     AND wil.task_id IS NULL
                     AND EXISTS (SELECT *
                                 FROM   mtl_secondary_locators msl
                                 WHERE  msl.inventory_item_id = nvl(p_inventory_item_id, msl.inventory_item_id)
                                 AND    msl.organization_id = p_organization_id
                                 AND    msl.subinventory_code = wil.subinventory_code
                                 AND    msl.secondary_locator = wil.inventory_location_id)
                     --AND (substr(SEGMENT1,instr(SEGMENT1,'-',1,1)+1, LENGTH(SEGMENT1) -  instr(SEGMENT1,'-',1,1))) like nvl(p_concatenated_segments,(substr(SEGMENT1,instr(SEGMENT1,'-',1,1)+1, LENGTH(SEGMENT1) -  instr(SEGMENT1,'-',1,1))))) loc --Removed 3/2/2016 TMS 20160309-00091
                     AND (substr(SEGMENT1,instr(SEGMENT1,'-',1,1)+1, LENGTH(SEGMENT1) -  instr(SEGMENT1,'-',1,1))) like nvl(l_concatenated_segments,(substr(SEGMENT1,instr(SEGMENT1,'-',1,1)+1, LENGTH(SEGMENT1) -  instr(SEGMENT1,'-',1,1))))) loc --Added 3/2/2016 TMS 20160309-00091
                  order by loc.wc_pre_fix, loc.wc_locator, loc.concatenated_segments;
          
          END IF;
        
        
        
        ELSE
        --Hide Prefix and Locator Restrict is off
        
          IF p_restrict_locators_code IS NULL OR p_restrict_locators_code = 2 THEN
         
            OPEN x_locators FOR
              --Primary Bin Query
              SELECT DISTINCT
                         loc.wc_locator
                       , NULL wc_prefix--, loc.wc_pre_fix
                       , NULL inventory_location_id--, loc.inventory_location_id
                       , NULL concatenated_segments--, loc.concatenated_segments
                       , NULL description--, loc.description
                       , loc.subinventory_code
              FROM  (SELECT --  WC_LOCATOR(wil.locator_segments) wc_locator
                            --, WC_LOCATOR_PREFIX(wil.locator_segments) wc_pre_fix
                              substr(SEGMENT1,instr(SEGMENT1,'-',1,1)+1, LENGTH(SEGMENT1) -  instr(SEGMENT1,'-',1,1)) wc_locator
                            --, substr(segment1, 1,instr(segment1,'-',1,1)-1) wc_pre_fix
                            --, wil.inventory_location_id
                            --, wil.locator_segments concatenated_segments
                            --, wil.description
                            , wil.subinventory_code
                     FROM wms_item_locations_kfv wil
                     WHERE wil.organization_id = p_organization_id
                     AND NVL(wil.disable_date, TRUNC(SYSDATE + 1)) > TRUNC(SYSDATE)
                     AND wil.subinventory_code = nvl(p_subinventory_code, wil.subinventory_code)
                     AND wil.PROJECT_ID IS NULL
                     AND wil.task_id IS NULL
                     AND EXISTS (SELECT *
                                 FROM   mtl_secondary_locators msl
                                 WHERE  msl.inventory_item_id = nvl(p_inventory_item_id, msl.inventory_item_id)
                                 AND    msl.organization_id = p_organization_id
                                 AND    msl.subinventory_code = wil.subinventory_code
                                 AND    msl.secondary_locator = wil.inventory_location_id)
                     --AND (substr(SEGMENT1,instr(SEGMENT1,'-',1,1)+1, LENGTH(SEGMENT1) -  instr(SEGMENT1,'-',1,1))) like nvl(p_concatenated_segments,(substr(SEGMENT1,instr(SEGMENT1,'-',1,1)+1, LENGTH(SEGMENT1) -  instr(SEGMENT1,'-',1,1))))) loc  --Removed 3/2/2016 TMS 20160309-00091
                     AND (substr(SEGMENT1,instr(SEGMENT1,'-',1,1)+1, LENGTH(SEGMENT1) -  instr(SEGMENT1,'-',1,1))) like nvl(l_concatenated_segments,(substr(SEGMENT1,instr(SEGMENT1,'-',1,1)+1, LENGTH(SEGMENT1) -  instr(SEGMENT1,'-',1,1))))) loc  --Added 3/2/2016 TMS 20160309-00091
                  order by loc.wc_locator;
    
          
          
          ELSE
            --Hide Prefix and Locator Restrict is on
                         OPEN x_locators FOR
              --Primary Bin Query
              SELECT DISTINCT
                         loc.wc_locator
                       , NULL wc_prefix--, loc.wc_pre_fix
                       , NULL inventory_location_id--, loc.inventory_location_id
                       , NULL concatenated_segments--, loc.concatenated_segments
                       , NULL description--, loc.description
                       , loc.subinventory_code
              FROM  (SELECT --  WC_LOCATOR(wil.locator_segments) wc_locator
                            --, WC_LOCATOR_PREFIX(wil.locator_segments) wc_pre_fix
                              substr(SEGMENT1,instr(SEGMENT1,'-',1,1)+1, LENGTH(SEGMENT1) -  instr(SEGMENT1,'-',1,1)) wc_locator
                            --, substr(segment1, 1,instr(segment1,'-',1,1)-1) wc_pre_fix
                            --, wil.inventory_location_id
                            --, wil.locator_segments concatenated_segments
                            --, wil.description
                            , wil.subinventory_code
                     FROM wms_item_locations_kfv wil
                     WHERE wil.organization_id = p_organization_id
                     AND NVL(wil.disable_date, TRUNC(SYSDATE + 1)) > TRUNC(SYSDATE)
                     AND wil.subinventory_code = nvl(p_subinventory_code, wil.subinventory_code)
                     AND wil.PROJECT_ID IS NULL
                     AND wil.task_id IS NULL
                     --AND (substr(SEGMENT1,instr(SEGMENT1,'-',1,1)+1, LENGTH(SEGMENT1) -  instr(SEGMENT1,'-',1,1))) like nvl(p_concatenated_segments,(substr(SEGMENT1,instr(SEGMENT1,'-',1,1)+1, LENGTH(SEGMENT1) -  instr(SEGMENT1,'-',1,1))))) loc --Removed 3/2/2016 TMS 20160309-00091
                     AND (substr(SEGMENT1,instr(SEGMENT1,'-',1,1)+1, LENGTH(SEGMENT1) -  instr(SEGMENT1,'-',1,1))) like nvl(l_concatenated_segments,(substr(SEGMENT1,instr(SEGMENT1,'-',1,1)+1, LENGTH(SEGMENT1) -  instr(SEGMENT1,'-',1,1))))) loc --Added 3/2/2016 TMS 20160309-00091
                  order by loc.wc_locator;
    
            END IF;
            
    END IF;
    
    EXCEPTION
    
      WHEN others THEN
             g_sqlcode := SQLCODE;
             g_sqlerrm := SQLERRM;
             g_message := 'when others error ';
             debug_log(g_message || g_sqlcode || g_sqlerrm);
    
            xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                 , p_calling             => g_call_point
                                                 , p_ora_error_msg       => SQLERRM
                                                 , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                 , p_distribution_list   => g_distro_list
                                                 , p_module              => g_module);


    
    END  GET_WC_BIN_LOC;

    /*****************************************************************************************************************************************
    *   FUNCTION BIN_INQUIRY                                                                                                                 *
    *                                                                                                                                        *
    *   PURPOSE:   This function is used to return all assigned bins to an item in 1 function return separeted by |                          *
    *                                                                                                                                        *
    *                                                                                                                                        *
    *   REVISIONS:                                                                                                                           *
    *   Ver        Date        Author                     Description                                                                        *
    *   ---------  ----------  ---------------         -------------------------                                                             *
    *   1.0        22-OCT-2014  Lee Spitzer               TMS Ticket 20150302-00164                                                          *
    *                                                     RF - Bin maintenance form                                                          *
    *****************************************************************************************************************************************/
  
  


      FUNCTION BIN_INQUIRY           ( p_organization_id        IN     NUMBER    --1
                                     , p_inventory_item_id      IN     VARCHAR2  --3
                                     )
              RETURN VARCHAR2 IS
        
        CURSOR c_loc IS
          SELECT wil.concatenated_segments concatenated_segments
          FROM   wms_item_locations_kfv wil,
                 mtl_secondary_locators msl
          WHERE  wil.organization_id = p_organization_id
          AND    msl.organization_id = wil.organization_id
          AND    msl.inventory_item_id = p_inventory_item_id
          AND    msl.secondary_locator = wil.inventory_location_id
          ORDER BY substr(wil.segment1, 1,instr(wil.segment1,'-',1,1)-1);
      
        l_locator VARCHAR2(10000) DEFAULT 'No bins associated for item';
        l_count NUMBER DEFAULT 1;
              
      BEGIN
      
       
        g_call_from := 'BIN_INQUIRY';
        g_call_point := 'Start';
      
        debug_log('p_organization_id ' || p_organization_id);
        debug_log('p_inventory_item_id ' || p_inventory_item_id);

      
        FOR r_loc IN c_loc
          
            loop
            
              IF l_count = 1 THEN
              
                    l_locator := substr(r_loc.concatenated_segments,1,10000);

              else
                  
                    l_locator := substr(l_locator ||'|'||r_loc.concatenated_segments,1,10000);
                    
                    
              END IF;
              
              l_count := 1 + l_count;
              
            end loop;
            
            debug_log('l_locator ' || l_locator);
            return l_locator;
            
            
            
    EXCEPTION
    
      WHEN g_exception THEN
      
            debug_log('l_locator ' || l_locator);
            RETURN l_locator;
            
      WHEN others THEN
      
             debug_log('l_locator ' || l_locator);
             RETURN l_locator;
             
             g_sqlcode := SQLCODE;
             g_sqlerrm := SQLERRM;
             g_message := 'when others error ';
             debug_log(g_message || g_sqlcode || g_sqlerrm);
    
             xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                 , p_calling             => g_call_point
                                                 , p_ora_error_msg       => SQLERRM
                                                 , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                 , p_distribution_list   => g_distro_list
                                                 , p_module              => g_module);

        
      END BIN_INQUIRY;
  
    /*****************************************************************************************************************************************
    *   FUNCTION NEXT_BIN_PREFIX                                                                                                             *
    *                                                                                                                                        *
    *   PURPOSE:   This function is used to return all assigned bins to an item in 1 function return separeted by |                          *
    *                                                                                                                                        *
    *                                                                                                                                        *
    *   REVISIONS:                                                                                                                           *
    *   Ver        Date        Author                     Description                                                                        *
    *   ---------  ----------  ---------------         -------------------------                                                             *
    *   1.0        22-OCT-2014  Lee Spitzer               TMS Ticket 20150302-00164                                                          *
    *                                                     RF - Bin maintenance form                                                          *
    *****************************************************************************************************************************************/


    FUNCTION NEXT_BIN_PREFIX           ( p_organization_id        IN     NUMBER    --1
                                       , p_inventory_item_id      IN     VARCHAR2  --2
                                       )
              RETURN VARCHAR2 IS
              
      l_bin_prefix VARCHAR2(100) DEFAULT '0';
      l_min NUMBER;
      l_max NUMBER;
      l_count NUMBER DEFAULT 1;
      
    
      CURSOR c_loc IS
          SELECT substr(wil.segment1, 1,instr(wil.segment1,'-',1,1)-1) prefix
          FROM   wms_item_locations_kfv wil,
                 mtl_secondary_locators msl
          WHERE  wil.organization_id = p_organization_id
          AND    msl.organization_id = wil.organization_id
          AND    msl.inventory_item_id = p_inventory_item_id
          AND    msl.secondary_locator = wil.inventory_location_id 
          ----
          AND    substr(wil.segment1, 1,instr(wil.segment1,'-',1,1)-1)!= '0' --Skip 0 located bin prefixes
          --GROUP BY  wil.concatenated_segments,
                 --substr(wil.segment1, 1,instr(wil.segment1,'-',1,1)-1) 
          ORDER BY substr(wil.segment1, 1,instr(wil.segment1,'-',1,1)-1);
              
    BEGIN
    
        g_call_from := 'BIN_INQUIRY';
        g_call_point := 'Start';
      
        debug_log('p_organization_id ' || p_organization_id);
        debug_log('p_inventory_item_id ' || p_inventory_item_id);
         
          for r_loc in c_loc

            LOOP
                  IF (l_count = r_loc.prefix) THEN
                  
                      l_count := l_count + 1;
                
                  elsif (l_count != r_loc.prefix) then 
                  
                          l_count := l_count;
                  
                      exit;
                      
                  else
                   
                        l_count := l_count;
                        
                      exit;
                        
                  end if;
              
                
              end loop;
          
          
          BEGIN
            l_bin_prefix := substr(l_count, 1, 100);
          EXCEPTION
            WHEN others THEN
                g_sqlcode := SQLCODE;
                g_sqlerrm := SQLERRM;
                raise g_exception;
          END;
          
          
          
          RETURN l_bin_prefix;
          
              
      EXCEPTION
    
      WHEN g_exception THEN
      
            RETURN l_bin_prefix;
            
             g_message := 'when g_exception error ' || g_sqlcode || g_sqlerrm;
             debug_log(g_message || g_sqlcode || g_sqlerrm);
    
             xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                 , p_calling             => g_call_point
                                                 , p_ora_error_msg       => SQLERRM
                                                 , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                 , p_distribution_list   => g_distro_list
                                                 , p_module              => g_module);

      WHEN others THEN
      
             RETURN l_bin_prefix;
             
             g_sqlcode := SQLCODE;
             g_sqlerrm := SQLERRM;
             g_message := 'when others error ';
             debug_log(g_message || g_sqlcode || g_sqlerrm);
    
             xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                 , p_calling             => g_call_point
                                                 , p_ora_error_msg       => SQLERRM
                                                 , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                 , p_distribution_list   => g_distro_list
                                                 , p_module              => g_module);

    
    END NEXT_BIN_PREFIX;
    
   
    
    /*****************************************************************************************************************************************
    *   FUNCTION VAL_BIN                                                                                                                     *
    *                                                                                                                                        *
    *   PURPOSE:   This function is used to validate if the bin entered by the user has a -1, -2, or -3 record                               *
    *                                                                                                                                        *
    *                                                                                                                                        *
    *   REVISIONS:                                                                                                                           *
    *   Ver        Date        Author                     Description                                                                        *
    *   ---------  ----------  ---------------         -------------------------                                                             *
    *   1.0        22-OCT-2014  Lee Spitzer               TMS Ticket 20150302-00164                                                          *
    *                                                     RF - Bin maintenance form                                                          *
    *****************************************************************************************************************************************/
  


    
   FUNCTION VAL_BIN                   ( p_organization_id        IN     NUMBER    --1
                                      , p_locator                IN     VARCHAR2  --2
                                      )

               RETURN NUMBER IS
              
      l_count NUMBER DEFAULT 0;
      
                
    BEGIN
    
        g_call_from := 'VAL_BIN';
        g_call_point := 'Start';
      
        debug_log('p_organization_id ' || p_organization_id);
        debug_log('p_locator ' || p_locator);
         
         BEGIN
          SELECT count(*)
          INTO   l_count
          FROM   wms_item_locations_kfv wil
          WHERE  wil.organization_id = p_organization_id
          AND    substr(wil.segment1, 1,instr(wil.segment1,'-',1,1)-1) in (1,2,3) --Skip 0 located bin prefixes
          AND    substr(SEGMENT1,instr(SEGMENT1,'-',1,1)+1, LENGTH(SEGMENT1) -  instr(SEGMENT1,'-',1,1)) = p_locator
          ;
        EXCEPTION
          WHEN others THEN
                l_count := 0;
        END;
       
           debug_log('l_count ' || l_count);
       
          RETURN l_count;
              
      EXCEPTION
    
      WHEN g_exception THEN
      
            debug_log('l_count ' || l_count);
            
            RETURN l_count;
            
             g_message := 'when g_exception error ' || g_sqlcode || g_sqlerrm;
             debug_log(g_message || g_sqlcode || g_sqlerrm);
    
             xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                 , p_calling             => g_call_point
                                                 , p_ora_error_msg       => SQLERRM
                                                 , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                 , p_distribution_list   => g_distro_list
                                                 , p_module              => g_module);

      WHEN others THEN
      
             debug_log('l_count ' || l_count);
             
             RETURN l_count;
             
             g_sqlcode := SQLCODE;
             g_sqlerrm := SQLERRM;
             g_message := 'when others error ';
             debug_log(g_message || g_sqlcode || g_sqlerrm);
    
             xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                 , p_calling             => g_call_point
                                                 , p_ora_error_msg       => SQLERRM
                                                 , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                 , p_distribution_list   => g_distro_list
                                                 , p_module              => g_module);

    

     END VAL_BIN;
     
     
     /*****************************************************************************************************************************************
    *   FUNCTION GET_BIN_VAL_LOC                                                                                                             *
    *                                                                                                                                        *
    *   PURPOSE:   This function is used to validate if the bin entered by the user has a -1, -2, or -3 record                               *
    *                                                                                                                                        *
    *                                                                                                                                        *
    *   REVISIONS:                                                                                                                           *
    *   Ver        Date        Author                     Description                                                                        *
    *   ---------  ----------  ---------------         -------------------------                                                             *
    *   1.0        22-OCT-2014  Lee Spitzer               TMS Ticket 20150302-00164                                                          *
    *                                                     RF - Bin maintenance form                                                          *
    *****************************************************************************************************************************************/
  
  


      FUNCTION GET_BIN_VAL_LOC           ( p_organization_id        IN     NUMBER    --1
                                         , p_locator                IN     VARCHAR2  --2
                                         , p_locator_prefix         IN     VARCHAR2  --3
                                         )
              RETURN NUMBER IS
              
          
           l_locator_id NUMBER DEFAULT -1;
      
                
    BEGIN
    
        g_call_from := 'GET_BIN_VAL_LOC';
        g_call_point := 'Start';
      
        debug_log('p_organization_id ' || p_organization_id);
        debug_log('p_locator ' || p_locator);
        debug_log('p_locator_prefix ' || p_locator_prefix);
         
         BEGIN
          SELECT wil.inventory_location_id
          INTO   l_locator_id
          FROM   wms_item_locations_kfv wil
          WHERE  wil.organization_id = p_organization_id
          AND    substr(wil.segment1, 1,instr(wil.segment1,'-',1,1)-1) = p_locator_prefix
          AND    substr(SEGMENT1,instr(SEGMENT1,'-',1,1)+1, LENGTH(SEGMENT1) -  instr(SEGMENT1,'-',1,1)) = p_locator
          ;
        EXCEPTION
          WHEN others THEN
                l_locator_id := -1;
        END;
       
          debug_log('l_locator_id ' || l_locator_id);
          
          RETURN l_locator_id;
              
      EXCEPTION
    
      WHEN g_exception THEN
            
            debug_log('l_locator_id ' || l_locator_id);
            RETURN l_locator_id;
            
             g_message := 'when g_exception error ' || g_sqlcode || g_sqlerrm;
             debug_log(g_message || g_sqlcode || g_sqlerrm);
    
             xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                 , p_calling             => g_call_point
                                                 , p_ora_error_msg       => SQLERRM
                                                 , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                 , p_distribution_list   => g_distro_list
                                                 , p_module              => g_module);

      WHEN others THEN
      
             debug_log('l_locator_id ' || l_locator_id);
             RETURN l_locator_id;
             
             g_sqlcode := SQLCODE;
             g_sqlerrm := SQLERRM;
             g_message := 'when others error ';
             debug_log(g_message || g_sqlcode || g_sqlerrm);
    
             xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                 , p_calling             => g_call_point
                                                 , p_ora_error_msg       => SQLERRM
                                                 , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                 , p_distribution_list   => g_distro_list
                                                 , p_module              => g_module);


      
      
      
      END GET_BIN_VAL_LOC;
      
      
 
     /*****************************************************************************************************************************************
     *  PROCEDURE CREATE_NEW_LOCATOR                                                                                                          *
     *                                                                                                                                        *
     *   PURPOSE:   This function is to return if the locator exists                                                                          *
     *                                                                                                                                        *
     *    Parameters: p_organization_id IN  VARCHAR2                                                                                          *
     *                p_subinventory_code IN VARCHAR2                                                                                         *
     *    Return : Count of locators tied to subinventory for the organization                                                                *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        09-FEB-2015  Lee Spitzer               TMS Ticket 20150302-00164                                                          *
     *                                                     RF - Bin maintenance form                                                          *
     *   1.1        08-JUL-2015  Lee Spitzer               TMS Ticket 20150714-00142 RF - Implement fixes to include with release 2           *
     *****************************************************************************************************************************************/


      PROCEDURE CREATE_NEW_LOCATOR ( p_organization_id IN NUMBER
                                  , p_subinventory_code IN VARCHAR2
                                  , p_locator IN VARCHAR2
                                  , p_locator_prefix IN VARCHAR2
                                  , x_return  OUT NUMBER
                                  , x_message OUT VARCHAR2) IS
      
      
          l_exists NUMBER;
          l_return_status           VARCHAR2 (1) := fnd_api.g_ret_sts_success;
          l_msg_count               NUMBER;
          l_msg_data                VARCHAR2 (4000);
          l_inventory_location_id   NUMBER;
          l_locator_exists          VARCHAR2 (1);
          l_error_msg               VARCHAR2 (2000) := NULL;
          l_locator_id              NUMBER;
          l_count                   NUMBER DEFAULT 0;
          l_new_locator             VARCHAR2(40) DEFAULT '';
          l_reason_id               NUMBER; --Added 01-JUN-2015 TMS Ticket 20150714-00142 RF - Implement fixes to include with release 2
      
      BEGIN
      
       g_call_from := 'CREATE_NEW_LOCATOR';
       g_call_point := 'Start';
      
        debug_log('p_organization_id ' || p_organization_id);
        debug_log('p_subinventory_code ' || p_subinventory_code);
        debug_log('p_locator ' || p_locator);
        debug_log('p_locator_prefix ' || p_locator_prefix);
        
        l_new_locator := p_locator_prefix ||'-'||p_locator||'.';
        
        debug_log('l_new_locator ' || l_new_locator);
        
                g_call_point := 'Check if locators exists in org';
                debug_log(g_call_point);
        
                
                BEGIN
                  SELECT count(*)
                  INTO   l_exists
                  FROM   wms_item_locations_kfv wil
                  WHERE  wil.organization_id = p_organization_id
                  AND    wil.subinventory_code = nvl(p_subinventory_code, wil.subinventory_code)
                  and    wil.concatenated_segments = l_new_locator;
              
                EXCEPTION
                  WHEN others THEN
                      g_sqlcode := SQLCODE;
                      g_sqlerrm := SQLERRM;
                      g_message := 'Error getting count';
                      raise g_exception;
                END;
              
                debug_log('l_exists ' || l_exists || ' for locator ' || l_new_locator);
                
                --if the locator doesn't exists in the org
                if l_exists = 0 then
                
                    --Added 01-JUN-2015 TMS Ticket 20150714-00142 RF - Implement fixes to include with release 2
                    BEGIN
          
                      SELECT reason_id
                      into   l_reason_id
                      FROM   MTL_TRANSACTION_REASONS
                      where  reason_name = '15 - RF';
              
                    EXCEPTION
                        WHEN others THEN
                              l_reason_id := NULL;
                    END;
                    --End Added 01-JUN-2015 TMS Ticket 20150714-00142 RF - Implement fixes to include with release 2
                    
                    --Create the locator
                             --mo_global.init ('INV');  
      
                            l_error_msg := NULL;
                            l_inventory_location_id := NULL;
                            l_locator_exists := NULL;
                            l_msg_data := NULL;
                            l_msg_count := NULL;
                            l_return_status := NULL;

                    inv_loc_wms_pub.create_locator (
                          x_return_status              => l_return_status,
                          x_msg_count                  => l_msg_count,
                          x_msg_data                   => l_msg_data,
                          x_inventory_location_id      => l_inventory_location_id,
                          x_locator_exists             => l_locator_exists,
                          p_organization_id            => p_organization_id,
                          p_organization_code          => NULL,
                          p_concatenated_segments      => l_new_locator, 
                          p_description                => '', 
                          p_inventory_location_type    => NULL,                     --3,
                          p_picking_order              => NULL,
                          p_location_maximum_units     => NULL,
                          p_subinventory_code          => p_subinventory_code,
                          p_location_weight_uom_code   => NULL,
                          p_max_weight                 => NULL,
                          p_volume_uom_code            => NULL,
                          p_max_cubic_area             => NULL,
                          p_x_coordinate               => NULL,
                          p_y_coordinate               => NULL,
                          p_z_coordinate               => NULL,
                          p_physical_location_id       => NULL,
                          p_pick_uom_code              => NULL,
                          p_dimension_uom_code         => NULL,
                          p_length                     => NULL,
                          p_width                      => NULL,
                          p_height                     => NULL,
                          p_status_id                  => 1,
                          p_dropping_order             => NULL,
                          p_attribute_category         => '',
                          --p_attribute1                 => '', --Removed 01-JUN-2015 TMS Ticket 20150714-00142 RF - Implement fixes to include with release 2
                          p_attribute1                 => l_reason_id, --Added 01-JUN-2015 TMS Ticket 20150714-00142 RF - Implement fixes to include with release 2
                          p_attribute2                 => '',
                          p_attribute3                 => '',
                          p_attribute4                 => '',
                          p_attribute5                 => '',
                          p_attribute6                 => '',
                          p_attribute7                 => '',
                          p_attribute8                 => '',
                          p_attribute9                 => '',
                          p_attribute10                => '',
                          p_attribute11                => '',
                          p_attribute12                => '',
                          p_attribute13                => '',
                          p_attribute14                => '',
                          p_attribute15                => '',
                          p_alias                      => '');
                  
                      debug_log('l_return_status ' || l_return_status);
                  
                      IF (l_return_status <> 'S')
                      THEN
                          l_count := l_count + 1;
                          FOR l_index IN 1 .. l_msg_count
                          LOOP
                              l_msg_data := fnd_msg_pub.get (l_index, 'F');
                              --l_error_msg :=                         l_error_msg || '-' || SUBSTR (l_msg_data, 1, 50);
                          END LOOP;
                          l_error_msg := l_error_msg || '-' || SUBSTR (l_msg_data, 1, 100);
                          debug_log(l_error_msg);
                          
                      ELSE
                        l_error_msg := '';
                      
                      END IF;
                      
                
                END IF;
              
        
          COMMIT;
          
        
        IF (l_return_status <> 'S') THEN
            x_message := 'Error creating locator ' || l_new_locator ||' ' || l_error_msg;
            x_return := -1;
        ELSE
            x_message := 'Success ';
            x_return := l_inventory_location_id;
        END IF;
        
        debug_log('x_message ' || x_message);
        debug_log('x_return ' || x_return);
        
      EXCEPTION
    
        WHEN g_exception THEN
            
            g_message := 'Error in CREATE_WC_LOCATOR ' || g_sqlcode || g_sqlerrm;
            x_return := 1;
            x_message := g_message;
            
            debug_log('x_message ' || x_message);
            debug_log('x_return ' || x_return);
            
                xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                     , p_calling             => g_call_point
                                                     , p_ora_error_msg       => SQLERRM
                                                     , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                     , p_distribution_list   => g_distro_list
                                                     , p_module              => g_module);
          WHEN OTHERS THEN
            
            g_sqlcode := SQLCODE;
            g_sqlerrm := SQLERRM;
            g_message := 'Error in CREATE_WC_LOCATOR ' || g_sqlcode || g_sqlerrm;
            x_return := 1;
            x_message := g_message;
            
            debug_log('x_message ' || x_message);
            debug_log('x_return ' || x_return);
            
                xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                     , p_calling             => g_call_point
                                                     , p_ora_error_msg       => SQLERRM
                                                     , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                     , p_distribution_list   => g_distro_list
                                                     , p_module              => g_module);
      
    
      
      END CREATE_NEW_LOCATOR;
      
     /*****************************************************************************************************************************************
     *  PROCEDURE CHECK_FOR_DUP_ASSIGNMENTS                                                                                                   *
     *                                                                                                                                        *
     *   PURPOSE:   This function is to return if the locator exists                                                                          *
     *                                                                                                                                        *
     *    Parameters: p_organization_id IN  VARCHAR2                                                                                          *
     *                p_subinventory_code IN VARCHAR2                                                                                         *
     *    Return : Count of locators tied to subinventory for the organization                                                                *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        09-FEB-2015  Lee Spitzer               TMS Ticket 20150302-00164                                                          *
     *                                                     RF - Bin maintenance form                                                          *
     *****************************************************************************************************************************************/


      PROCEDURE CHECK_FOR_DUP_ASSIGNMENTS ( p_organization_id IN NUMBER       --1
                                          , p_inventory_item_id IN VARCHAR2   --2
                                          , p_subinventory_code IN VARCHAR2   --3
                                          , p_locator IN VARCHAR2             --4
                                          , x_return  OUT NUMBER              --5
                                          , x_message OUT VARCHAR2)           --6
            IS
        
        CURSOR c_loc IS
          SELECT wil.concatenated_segments loc
          FROM   wms_item_locations_kfv wil,
                 mtl_secondary_locators msl
          WHERE  wil.organization_id = p_organization_id
          AND    msl.organization_id = wil.organization_id
          AND    msl.inventory_item_id = p_inventory_item_id
          AND    msl.secondary_locator = wil.inventory_location_id 
          AND    substr(SEGMENT1,instr(SEGMENT1,'-',1,1)+1, LENGTH(SEGMENT1) -  instr(SEGMENT1,'-',1,1)) = p_locator
          --GROUP BY  wil.concatenated_segments,
                 --substr(wil.segment1, 1,instr(wil.segment1,'-',1,1)-1) 
          ORDER BY substr(wil.segment1, 1,instr(wil.segment1,'-',1,1)-1);

        l_locator VARCHAR2(10000) DEFAULT '';
        l_count NUMBER DEFAULT 0;
        l_loop_count NUMBER DEFAULT 1;
      
        
      BEGIN
      
       g_call_from := 'CHECK_FOR_DUP_ASSIGNMENTS';
       g_call_point := 'Start';
      
        debug_log('p_organization_id ' || p_organization_id);
        debug_log('p_subinventory_code ' || p_subinventory_code);
        debug_log('p_inventory_item_id ' || p_inventory_item_id);
        debug_log('p_locator ' || p_locator);


      g_call_point := 'Being Count';
      
        BEGIN
          SELECT count(wil.concatenated_segments) loc
          INTO   l_count
          FROM   wms_item_locations_kfv wil,
                 mtl_secondary_locators msl
          WHERE  wil.organization_id = p_organization_id
          AND    msl.organization_id = wil.organization_id
          AND    msl.inventory_item_id = p_inventory_item_id
          AND    msl.secondary_locator = wil.inventory_location_id 
          AND    substr(SEGMENT1,instr(SEGMENT1,'-',1,1)+1, LENGTH(SEGMENT1) -  instr(SEGMENT1,'-',1,1)) = p_locator;
        EXCEPTION
          WHEN others THEN
                l_count := 0;
        END;
        
        
        IF l_count > 0 THEN
        
          FOR r_loc IN c_loc 
          
            LOOP
            
              IF l_loop_count = 1 THEN
              
                  l_locator := substr(r_loc.loc,1,10000);
                  
              ELSE
              
                  l_locator := substr(l_locator ||'|' || r_loc.loc,1,10000);
                  
              END IF;
              
              l_loop_count := nvl(l_loop_count,0) + 1;
              
         END loop;

      END IF;         
          
        
    x_return := l_count;
    x_message := nvl(l_locator || '|Already assigned to item' ,'Assignment does not already exists');
  
    debug_log('x_message ' || x_message);
    debug_log('x_return ' || x_return);
            
      EXCEPTION
    
        WHEN g_exception THEN
            
            g_message := 'Error in '||g_call_from || g_sqlcode || g_sqlerrm;
              
              x_return := 1;
              x_message := g_message;

            debug_log('x_message ' || x_message);
            debug_log('x_return ' || x_return);
            
                xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                     , p_calling             => g_call_point
                                                     , p_ora_error_msg       => SQLERRM
                                                     , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                     , p_distribution_list   => g_distro_list
                                                     , p_module              => g_module);
          WHEN OTHERS THEN
            
            g_sqlcode := SQLCODE;
            g_sqlerrm := SQLERRM;

            g_message := 'Error in '||g_call_from || g_sqlcode || g_sqlerrm;
              
             x_return := 1;
             x_message := g_message;

            debug_log('x_message ' || x_message);
            debug_log('x_return ' || x_return);
            
                xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                     , p_calling             => g_call_point
                                                     , p_ora_error_msg       => SQLERRM
                                                     , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                     , p_distribution_list   => g_distro_list
                                                     , p_module              => g_module);
      
      END CHECK_FOR_DUP_ASSIGNMENTS;

     /*****************************************************************************************************************************************
     *   PROCEDURE validate_loc_prefix_val                                                                                                    *
     *                                                                                                                                        *
     *   PURPOSE:   This procedure is used to validate the locator pre fix value                                                              *
     *                                                                                                                                        *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        27-JAN-2016  Lee Spitzer               TMS Ticket 20151103-00188 Mobile Stocking                                          *
     *                                                                                                                                        *
     *****************************************************************************************************************************************/
  
      PROCEDURE validate_loc_prefix_val  ( p_qty         IN VARCHAR2
                                         , x_return  OUT NUMBER
                                         , x_message OUT VARCHAR2
                                         )  
            IS
            
       l_qty  NUMBER;
       l_return NUMBER;
       l_remainder NUMBER;
        
      BEGIN
      
       g_call_from := 'VALIDATE_LOC_PREFIX_VAL';
       g_call_point := 'Start';
      
        debug_log('p_qty ' || p_qty);
        
       g_call_point := 'Converting parameters from varchar2 to numbers';

      BEGIN
         SELECT NVL (TO_NUMBER (p_qty), 0) INTO l_qty FROM DUAL;
      EXCEPTION
         WHEN OTHERS
         THEN
            g_sqlcode := SQLCODE;
            g_sqlerrm := SQLERRM;
            g_message := 'Could not convert ' || p_qty || ' to number';
            RAISE g_exception;
      END;
      
      BEGIN
        SELECT REMAINDER(l_qty,1) INTO l_remainder FROM dual;
      EXCEPTION
        WHEN OTHERS 
        THEN
            g_sqlcode := SQLCODE;
            g_sqlerrm := SQLERRM;
            g_message := 'Could not get the remainder ' || l_qty || ' to number';
            RAISE g_exception;
      END;
      
      IF   NVL(l_qty, 0) < 0
      THEN
         l_return := 1;
         g_message := 'Value must be greater than or equal to 0';
      ELSIF NVL(l_qty,0) > 9
      THEN
         l_return := 1;
         g_message := 'Value must be less than 10';
      ELSIF NVL(l_remainder,0) != 0
      THEN
         l_return := 1;
         g_message := 'Value must be an integer.'; 
      ELSE
         l_return := 0;
         g_message := 'Success';
      END IF;

      debug_log ('x_return ' || l_return);
      debug_log ('x_message ' || g_message);


      x_return := l_return;
      x_message := g_message;
      
    EXCEPTION
      WHEN g_exception
      THEN
         x_return := 1;
         x_message := g_message;

         debug_log ('x_return ' || l_return);
         debug_log ('x_message ' || g_message);
      WHEN OTHERS
      THEN
         x_return := 1;

         g_sqlcode := SQLCODE;
         g_sqlerrm := SQLERRM;
         g_message := 'when others error ' || g_sqlcode || g_sqlerrm;
         debug_log (g_message || g_sqlcode || g_sqlerrm);

         x_return := 1;
         x_message := g_message;

         debug_log ('x_return ' || l_return);
         debug_log ('x_message ' || g_message);

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_package || '.' || g_call_from,
            p_calling             => g_call_point,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          => g_message || g_sqlcode || g_sqlerrm,
            p_distribution_list   => g_distro_list,
            p_module              => g_module);

  END validate_loc_prefix_val;
  
     /*****************************************************************************************************************************************
     *   PROCEDURE cascade_bin_assignments_add                                                                                                *
     *                                                                                                                                        *
     *   PURPOSE:   This procedure is used to cascade bin assignments                                                                         *
     *                                                                                                                                        *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        27-JAN-2016  Lee Spitzer               TMS Ticket 20151103-00188 Mobile Stocking                                          *
     *                                                                                                                                        *
     *****************************************************************************************************************************************/
  
      PROCEDURE cascade_bin_assignments_add   ( p_organization_id IN NUMBER   --1
                                              , p_inventory_item_id IN VARCHAR2   --2
                                              , p_subinventory_code IN VARCHAR2   --3
                                              , p_locator_id IN VARCHAR2          --4
                                              , x_return  OUT NUMBER              --5
                                              , x_message OUT VARCHAR2)          --6 
        IS
        
        --Current Locator Assignments
        CURSOR c_loc (p_p_prefix  IN VARCHAR2) IS
          SELECT  wil.concatenated_segments concatenated_segments
                 ,WC_LOCATOR(wil.locator_segments) wc_locator
                 ,WC_LOCATOR_PREFIX(wil.locator_segments) wc_pre_fix
                 ,wil.inventory_location_id
          FROM   wms_item_locations_kfv wil,
                 mtl_secondary_locators msl
          WHERE  wil.organization_id = p_organization_id
          AND    msl.organization_id = wil.organization_id
          AND    msl.inventory_item_id = p_inventory_item_id
          AND    msl.secondary_locator = wil.inventory_location_id
          AND    WC_LOCATOR_PREFIX(wil.locator_segments) >= p_p_prefix
          AND    wil.inventory_location_id != p_locator_id
          ORDER BY WC_LOCATOR_PREFIX(wil.locator_segments);
 
 
       l_p_prefix VARCHAR2(2); --parameter locator prefix
       l_count NUMBER;
       l_new_locator_prefix VARCHAR(2);
       l_new_locator VARCHAR2(40);
       l_new_locator_id NUMBER; --new locator id in cursor
       l_return NUMBER;
       l_message VARCHAR2(2000);
        
      BEGIN
      
       g_call_from := 'cascade_bin_assignments_add';
       g_call_point := 'Start';
      
        debug_log('p_organization_id ' || p_organization_id);
        debug_log('p_inventory_item_id ' || p_inventory_item_id);
        debug_log('p_subinventory_code ' || p_subinventory_code);
        debug_log('p_locator_id ' || p_locator_id);
        

      g_call_point := 'Get p_locator_id prefix';
      
        BEGIN
          SELECT WC_LOCATOR_PREFIX(wil.locator_segments)
          INTO   l_p_prefix
          FROM   wms_item_locations_kfv wil
          WHERE  wil.organization_id = p_organization_id
          AND    wil.inventory_location_id = p_locator_id
          and    wil.subinventory_code = p_subinventory_code;
        EXCEPTION
          WHEN others THEN
                      g_sqlcode := SQLCODE;
                      g_sqlerrm := SQLERRM;
                      g_message := 'Error p_locator prefix';
                      raise g_exception;
        END;
        
        
        debug_log('l_p_prefix ' || l_p_prefix);
        
        
        --Check to see if the prefix exists to see if we need to cascade
        BEGIN
          SELECT  count(*)
          INTO    l_count
          FROM   wms_item_locations_kfv wil,
                   mtl_secondary_locators msl
          WHERE  wil.organization_id = p_organization_id
          AND    msl.organization_id = wil.organization_id
          AND    msl.inventory_item_id = p_inventory_item_id
          AND    msl.secondary_locator = wil.inventory_location_id
          AND    WC_LOCATOR_PREFIX(wil.locator_segments) = l_p_prefix
          AND    wil.inventory_location_id != p_locator_id;
        EXCEPTION
          WHEN others THEN
              l_count := 0;
        END;
        
        
        if l_count > 0 THEN
        
        g_call_point := 'loop through assignments equal to or higher current assignment ';
        
        
          FOR r_loc IN c_loc (l_p_prefix)
          
            LOOP
            
              
                g_call_point := 'get locator prefix in cursor and increment by 1 ';
        
        
                --Get locator prefix in cursor and increment by 1
                    
                BEGIN  
                    SELECT nvl(r_loc.wc_pre_fix,0) + 1 
                    INTO   l_new_locator_prefix
                    FROM   dual;
                EXCEPTION
                      WHEN others THEN
                      g_sqlcode := SQLCODE;
                      g_sqlerrm := SQLERRM;
                      g_message := 'Error p_locator prefix';
                      raise g_exception;
                END;
                
              
              
                debug_log('l_new_locator_prefix ' || l_new_locator_prefix);
                
                --Concat new prefix with wc_locator in cursor
              
              g_call_point := 'Build new locator';  
                
                BEGIN
                
                  BEGIN
                    SELECT l_new_locator_prefix || '-' || r_loc.wc_locator
                    INTO   l_new_locator
                    FROM   dual;
                  EXCEPTION
                      WHEN others THEN
                      g_sqlcode := SQLCODE;
                      g_sqlerrm := SQLERRM;
                      g_message := 'Error concatenating prefix';
                      raise g_exception;
                  END;
                
                EXCEPTION
                  WHEN OTHERS THEN
                      g_sqlcode := SQLCODE;
                      g_sqlerrm := SQLERRM;
                      g_message := 'Error new locator';
                      raise g_exception;
                END;
                
                
                debug_log('l_new_locator ' || l_new_locator);
                
                g_call_point := 'get new locator id ';
                
                BEGIN
                    SELECT wil.inventory_location_id
                    INTO   l_new_locator_id
                    FROM   wms_item_locations_kfv wil
                    WHERE  wil.organization_id = p_organization_id
                    AND    wil.concatenated_segments = l_new_locator
                    AND    wil.subinventory_code = p_subinventory_code;
                
                  EXCEPTION
                      WHEN no_data_found THEN
                      
                        l_new_locator_id := -1;
                      
                      WHEN others THEN
                        g_sqlcode := SQLCODE;
                        g_sqlerrm := SQLERRM;
                        g_message := 'Error new locator id';
                        raise g_exception;
                  END;

              debug_log('l_new_locator_id ' || l_new_locator_id);
                
              g_call_point := 'get new locator id ';
              
              --We need to create a new locator if the locator id does not exist -1
              IF nvl(l_new_locator_id,-1) = -1 THEN
              
                  g_call_point := 'Create new locator';
                  g_message := '';
                  
                  CREATE_NEW_LOCATOR  ( p_organization_id => p_organization_id
                                      , p_subinventory_code => p_subinventory_code
                                      , p_locator => r_loc.wc_locator
                                      , p_locator_prefix => l_new_locator_prefix
                                      , x_return  => l_new_locator_id
                                      , x_message => l_message);
                                    
                  
              END IF;
              
              
              g_call_point := 'after create new locator';
              
              debug_log('l_new_locator_id ' || l_new_locator_id);

              IF nvl(l_new_locator_id,-1) = -1 THEN
             
                  g_message := 'Can not create locator ' || l_new_locator_prefix || '-' || l_new_locator|| ' in ' || p_subinventory_code || ' ' ||l_message;
                  raise g_exception;
             
              END IF;         
              
              g_call_point := 'assign new locators';
              
              
                  PROCESS_ITEM_LOCATOR_TIE       ( p_organization_id    => p_organization_id
                                                 , p_inventory_item_id  => p_inventory_item_id
                                                 , p_subinventory_code  => p_subinventory_code
                                                 , p_locator_id => l_new_locator_id
                                                 , x_return  => l_return
                                                 , x_message => l_message);
                                                 
                                                 
              if l_return != 0 THEN
              
                  g_message := 'Can assign new create locator ' || l_new_locator_prefix || '-' || l_new_locator|| ' in ' || p_subinventory_code || ' ' ||l_message;
                  raise g_exception;
             
              
              end if;


               g_call_point := 'remove old locators';
              

                  DELETE_ITEM_LOCATOR_TIE       ( p_organization_id    => p_organization_id
                                                 , p_inventory_item_id  => p_inventory_item_id
                                                 , p_subinventory_code  => p_subinventory_code
                                                 , p_locator_id => r_loc.inventory_location_id
                                                 , x_return  => l_return
                                                 , x_message => l_message);
                                                 
                                                 
              if l_return != 0 THEN
              
                  g_message := 'Can not delete locator ' || r_loc.concatenated_segments || ' in ' || p_subinventory_code || ' ' ||l_message;
                  raise g_exception;
             
              
              end if;
              

            END LOOP;
            
        END IF;
        
        x_return := 0;
        x_message := 'Success';
      
      EXCEPTION
    
        WHEN g_exception THEN
            
            g_message := 'Error in '||g_call_from || g_sqlcode || g_sqlerrm;
              
              x_return := 1;
              x_message := g_message;

            debug_log('x_message ' || x_message);
            debug_log('x_return ' || x_return);
            
                xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                     , p_calling             => g_call_point
                                                     , p_ora_error_msg       => SQLERRM
                                                     , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                     , p_distribution_list   => g_distro_list
                                                     , p_module              => g_module);
          WHEN OTHERS THEN
            
            g_sqlcode := SQLCODE;
            g_sqlerrm := SQLERRM;

            g_message := 'Error in '||g_call_from || g_sqlcode || g_sqlerrm;
              
             x_return := 1;
             x_message := g_message;

            debug_log('x_message ' || x_message);
            debug_log('x_return ' || x_return);
            
                xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                     , p_calling             => g_call_point
                                                     , p_ora_error_msg       => SQLERRM
                                                     , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                     , p_distribution_list   => g_distro_list
                                                     , p_module              => g_module);
  
        
      END cascade_bin_assignments_add;


     /*****************************************************************************************************************************************
     *   PROCEDURE cascade_bin_assignments_delete                                                                                             *
     *                                                                                                                                        *
     *   PURPOSE:   This procedure is used to cascade bin assignments                                                                         *
     *                                                                                                                                        *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        27-JAN-2016  Lee Spitzer               TMS Ticket 20151103-00188 Mobile Stocking                                          *
     *                                                                                                                                        *
     *****************************************************************************************************************************************/
  
      PROCEDURE cascade_bin_assignments_delete   ( p_organization_id IN NUMBER   --1
                                                 , p_inventory_item_id IN VARCHAR2   --2
                                                 , p_subinventory_code IN VARCHAR2   --3
                                                 , p_locator_id IN VARCHAR2          --4
                                                 , x_return  OUT NUMBER              --5
                                                 , x_message OUT VARCHAR2)          --6 
        IS
        
        --Current Locator Assignments
        CURSOR c_loc (p_p_prefix  IN VARCHAR2) IS
          SELECT  wil.concatenated_segments concatenated_segments
                 ,WC_LOCATOR(wil.locator_segments) wc_locator
                 ,WC_LOCATOR_PREFIX(wil.locator_segments) wc_pre_fix
                 ,wil.inventory_location_id
          FROM   wms_item_locations_kfv wil,
                 mtl_secondary_locators msl
          WHERE  wil.organization_id = p_organization_id
          AND    msl.organization_id = wil.organization_id
          AND    msl.inventory_item_id = p_inventory_item_id
          AND    msl.secondary_locator = wil.inventory_location_id
          AND    WC_LOCATOR_PREFIX(wil.locator_segments) >= p_p_prefix
          ORDER BY WC_LOCATOR_PREFIX(wil.locator_segments);
 
 
       l_p_prefix VARCHAR2(2); --parameter locator prefix
       l_new_locator_prefix VARCHAR(2);
       l_new_locator VARCHAR2(40);
       l_new_locator_id NUMBER; --new locator id in cursor
       l_return NUMBER;
       l_message VARCHAR2(2000);
        
      BEGIN
      
       g_call_from := 'cascade_bin_assignments_delete';
       g_call_point := 'Start';
      
        debug_log('p_organization_id ' || p_organization_id);
        debug_log('p_inventory_item_id ' || p_inventory_item_id);
        debug_log('p_subinventory_code ' || p_subinventory_code);
        debug_log('p_locator_id ' || p_locator_id);
        

      g_call_point := 'Get p_locator_id prefix';
      
        BEGIN
          SELECT WC_LOCATOR_PREFIX(wil.locator_segments)
          INTO   l_p_prefix
          FROM   wms_item_locations_kfv wil
          WHERE  wil.organization_id = p_organization_id
          AND    wil.inventory_location_id = p_locator_id
          and    wil.subinventory_code = p_subinventory_code;
        EXCEPTION
          WHEN others THEN
                      g_sqlcode := SQLCODE;
                      g_sqlerrm := SQLERRM;
                      g_message := 'Error p_locator prefix';
                      raise g_exception;
        END;
        
        
        debug_log('l_p_prefix ' || l_p_prefix);
        
        if (l_p_prefix != '0') THEN  --Added per Testing feedback that if 0- is deleted do not cascade.
        
        g_call_point := 'loop through assignments equal to or higher current assignment ';
        
        
          FOR r_loc IN c_loc (l_p_prefix)
          
            LOOP
            
                g_call_point := 'get locator prefix in cursor and decrement by 1 ';
        
        
                --Get locator prefix in cursor and increment by 1
                    
                BEGIN  
                    SELECT nvl(r_loc.wc_pre_fix,0) - 1 
                    INTO   l_new_locator_prefix
                    FROM   dual;
                EXCEPTION
                      WHEN others THEN
                      g_sqlcode := SQLCODE;
                      g_sqlerrm := SQLERRM;
                      g_message := 'Error p_locator prefix';
                      raise g_exception;
                END;
              
              
                debug_log('l_new_locator_prefix ' || l_new_locator_prefix);
                
                --Concat new prefix with wc_locator in cursor
              
              g_call_point := 'Build new locator';  
                
                BEGIN
                
                  BEGIN
                    SELECT l_new_locator_prefix || '-' || r_loc.wc_locator
                    INTO   l_new_locator
                    FROM   dual;
                  EXCEPTION
                      WHEN others THEN
                      g_sqlcode := SQLCODE;
                      g_sqlerrm := SQLERRM;
                      g_message := 'Error concatenating prefix';
                      raise g_exception;
                  END;
                
                EXCEPTION
                  WHEN OTHERS THEN
                      g_sqlcode := SQLCODE;
                      g_sqlerrm := SQLERRM;
                      g_message := 'Error new locator';
                      raise g_exception;
                END;
                
                
                debug_log('l_new_locator ' || l_new_locator);
                
                g_call_point := 'get new locator id ';
                
                BEGIN
                    SELECT wil.inventory_location_id
                    INTO   l_new_locator_id
                    FROM   wms_item_locations_kfv wil
                    WHERE  wil.organization_id = p_organization_id
                    AND    wil.concatenated_segments = l_new_locator
                    AND    wil.subinventory_code = p_subinventory_code;
                
                  EXCEPTION
                      WHEN no_data_found THEN
                      
                        l_new_locator_id := -1;
                      
                      WHEN others THEN
                        g_sqlcode := SQLCODE;
                        g_sqlerrm := SQLERRM;
                        g_message := 'Error new locator id';
                        raise g_exception;
                  END;

              debug_log('l_new_locator_id ' || l_new_locator_id);
                
              g_call_point := 'get new locator id ';
              
              --We need to create a new locator if the locator id does not exist -1
              IF nvl(l_new_locator_id,-1) = -1 THEN
              
                  g_call_point := 'Create new locator';
                  g_message := '';
                  
                  CREATE_NEW_LOCATOR  ( p_organization_id => p_organization_id
                                      , p_subinventory_code => p_subinventory_code
                                      , p_locator => r_loc.wc_locator
                                      , p_locator_prefix => l_new_locator_prefix
                                      , x_return  => l_new_locator_id
                                      , x_message => l_message);
                                    
                  
              END IF;
              
              
              g_call_point := 'after create new locator';
              
              debug_log('l_new_locator_id ' || l_new_locator_id);

              IF nvl(l_new_locator_id,-1) = -1 THEN
             
                  g_message := 'Can not create locator ' || l_new_locator_prefix || '-' || l_new_locator|| ' in ' || p_subinventory_code || ' ' ||l_message;
                  raise g_exception;
             
              END IF;         
              
              g_call_point := 'assign new locators';
              
              
                  PROCESS_ITEM_LOCATOR_TIE       ( p_organization_id    => p_organization_id
                                                 , p_inventory_item_id  => p_inventory_item_id
                                                 , p_subinventory_code  => p_subinventory_code
                                                 , p_locator_id => l_new_locator_id
                                                 , x_return  => l_return
                                                 , x_message => l_message);
                                                 
                                                 
              if l_return != 0 THEN
              
                  g_message := 'Can assign new create locator ' || l_new_locator_prefix || '-' || l_new_locator|| ' in ' || p_subinventory_code || ' ' ||l_message;
                  raise g_exception;
             
              
              end if;


               g_call_point := 'remove old locators';
              

                  DELETE_ITEM_LOCATOR_TIE       ( p_organization_id    => p_organization_id
                                                 , p_inventory_item_id  => p_inventory_item_id
                                                 , p_subinventory_code  => p_subinventory_code
                                                 , p_locator_id => r_loc.inventory_location_id
                                                 , x_return  => l_return
                                                 , x_message => l_message);
                                                 
                                                 
              if l_return != 0 THEN
              
                  g_message := 'Can not delete locator ' || r_loc.concatenated_segments || ' in ' || p_subinventory_code || ' ' ||l_message;
                  raise g_exception;
             
              
              end if;
              

            END LOOP;
      
        END IF;
        
        x_return := 0;
        x_message := 'Success';
      
      EXCEPTION
    
        WHEN g_exception THEN
            
            g_message := 'Error in '||g_call_from || g_sqlcode || g_sqlerrm;
              
              x_return := 1;
              x_message := g_message;

            debug_log('x_message ' || x_message);
            debug_log('x_return ' || x_return);
            
                xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                     , p_calling             => g_call_point
                                                     , p_ora_error_msg       => SQLERRM
                                                     , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                     , p_distribution_list   => g_distro_list
                                                     , p_module              => g_module);
          WHEN OTHERS THEN
            
            g_sqlcode := SQLCODE;
            g_sqlerrm := SQLERRM;

            g_message := 'Error in '||g_call_from || g_sqlcode || g_sqlerrm;
              
             x_return := 1;
             x_message := g_message;

            debug_log('x_message ' || x_message);
            debug_log('x_return ' || x_return);
            
                xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                     , p_calling             => g_call_point
                                                     , p_ora_error_msg       => SQLERRM
                                                     , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                     , p_distribution_list   => g_distro_list
                                                     , p_module              => g_module);
  
        
      END cascade_bin_assignments_delete;
             
END XXWC_INV_BIN_MAINTENANCE_PKG;
/