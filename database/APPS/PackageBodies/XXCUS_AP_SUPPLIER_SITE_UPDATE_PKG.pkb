/* Formatted on 1/6/2015 4:09:00 PM (QP5 v5.254.13202.43190) */
CREATE OR REPLACE PACKAGE BODY APPS.XXCUS_AP_SUPP_SITE_UPDATE_PKG
AS
   /**************************************************************************
      *
      * PACKAGE
      * XXCUS_AP_SUPP_SITE_UPDATE_PKG
      *
      * DESCRIPTION
      *  program to assign 'N' or 'Y' to AP_SUPPLIER_SITES_ALL.create_debit_memo_flag field.
      *
      * PARAMETERS
      * ==========
      * NAME              TYPE     DESCRIPTION
     .* ----------------- -------- ---------------------------------------------
      * Operating Unit
      * Debit Memo(Y/N)
      *
      *
      *
      *
      * CALLED BY
      *   HDS AP GSC: Update Supplier Site - iProcurement Debit Memo Flag
      *
      * HISTORY
      * =======
      *
      * VERSION DATE        AUTHOR(S)       DESCRIPTION
      * ------- ----------- --------------- ------------------------------------
      * 1.00    01/6/2015   Maharajan S     Initial creation ESMS# 273959
      *
      *************************************************************************/
   PROCEDURE UPDATE_DEBIT_MEMO_FLAG (errbuf             OUT VARCHAR2,
                                     retcode            OUT VARCHAR2,
                                     p_org_id        IN     NUMBER,
                                     p_update_flag   IN     VARCHAR2)
   IS
      l_vendor_site_rec   ap_vendor_pub_pkg.r_vendor_site_rec_type;
      l_return_status     VARCHAR2 (10);
      l_msg_count         NUMBER;
      l_msg_data          VARCHAR2 (3000);
      l_vendor_site_id    NUMBER;
      l_sec               VARCHAR2 (200);
      l_msg               VARCHAR2 (150);
      l_distro_list       VARCHAR2 (75)
                             DEFAULT 'HDSOracleDevelopers@hdsupply.com';
      l_req_id            NUMBER := fnd_global.conc_request_id;



      CURSOR C1
      IS
           SELECT aps.vendor_id,
                  apss.vendor_site_id,
                  apss.org_id,
                  po.segment1,
                  aps.vendor_name,
                  apss.vendor_site_code,
                  apss.purchasing_site_flag,
                  apss.create_debit_memo_flag,
                  apss.Auto_tax_calc_flag,
                  apss.offset_tax_flag
             FROM po_headers po, ap_suppliers aps, ap_supplier_sites apss
            WHERE     1 = 1
                  AND aps.vendor_id = apss.vendor_id
                  AND apss.vendor_id = po.vendor_id
                  AND apss.vendor_site_id = po.vendor_site_id
                  AND po.type_lookup_code = 'CONTRACT'
         ORDER BY 1, 2;
   BEGIN
      --Initialize the apps
      mo_global.set_policy_context ('S', p_org_id);
      fnd_global.apps_initialize (fnd_global.user_id,
                                  fnd_global.resp_id,
                                  fnd_global.resp_appl_id,
                                  0);
      l_sec := 'Opening Cursor ';

      FOR c1_rec IN C1
      LOOP
         BEGIN
            l_vendor_site_rec.vendor_id := c1_rec.vendor_id;
            l_vendor_site_rec.vendor_site_id := c1_rec.vendor_site_id;
            l_vendor_site_rec.create_debit_memo_flag := p_update_flag;
            --l_vendor_site_rec.org_id := c1_rec.org_id;

            l_sec := 'Before calling UPDATE_VENDOR_SITE API ';
            AP_VENDOR_PUB_PKG.UPDATE_VENDOR_SITE (
               p_api_version       => 1.0,
	       x_return_status     => l_return_status,
               x_msg_count         => l_msg_count,
               x_msg_data          => l_msg_data,
               p_vendor_site_rec   => l_vendor_site_rec,
               p_vendor_site_id    => l_vendor_site_rec.vendor_site_id 
               );

           fnd_file.put_line (fnd_file.LOG,
                                    'Return_status: ' || l_return_status);
           fnd_file.put_line (fnd_file.LOG, 'msg_data: ' || l_msg_data);


           IF (l_return_status <> 'S')
           THEN
               fnd_file.put_line ( fnd_file.LOG,' Error In Vendor Site update API ' || l_msg_data);
               ROLLBACK;
           ELSE
              COMMIT;
               fnd_file.put_line (fnd_file.LOG,' Create Debit memo flag is updated as '|| p_update_flag||' for Vendor '|| c1_rec.vendor_name||' and Site code '|| c1_rec.vendor_site_code);
           END IF;           

         EXCEPTION
            WHEN OTHERS
            THEN
               l_msg := 'Error: ' || SQLERRM;
               fnd_file.put_line (
                  FND_FILE.LOG,
                  'When others exception in APPS.XXCUS_AP_SUPPLIER_SITE_UPDATE_PKG');
               xxcus_error_pkg.xxcus_error_main_api (
                  p_called_from         => 'XXCUS_AP_SUPPLIER_SITE_UPDATE_PKG',
                  p_calling             => l_sec,
                  p_request_id          => l_req_id,
                  p_ora_error_msg       => l_msg,
                  p_error_desc          => 'Error running XXCUS_AP_SUPPLIER_SITE_UPDATE_PKG WHEN OTHERS EXCEPTION',
                  p_distribution_list   => l_distro_list,
                  p_module              => 'AP');
         END;
      END LOOP;
   END;
END;
/