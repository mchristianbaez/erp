CREATE OR REPLACE PACKAGE BODY APPS.xxcus_fndgscst_pkg AS
/*
 -- ESMS ticket 229677
 -- Date:14-NOV-2013
 -- Author: Balaguru Seshadri
 -- Scope: This package will enhance HD Supply to run the gather schema statistics program one db schema at a time
 -- =================================================================================================================================
 -- Important Note: How to exclude schemas from gathering schema stats using the wrapper HDS Gather Schema Statistics -Master program
 -- Under lookup type HDS FND: Exclude Schemas from Gather Stats, please add new lookup codes to exclude the schemas.
 -- Example: Currently the OZF schema is excluded from the gather schema stats. We will take care of OZF separately
 -- =================================================================================================================================
 -- Modification history
 -- ====================
 -- Date          Author                      ESMS/RFC            Comment
 -- ==================================================================================================================================
 -- 25-JUN-2014   BALAGURU SESHADRI           ESMS 250107         Change in parameter values to the Oracle Gather Schema job.
*/

  procedure print_log(p_message in varchar2) is
  begin 
   if fnd_global.conc_request_id >0 then
    fnd_file.put_line(fnd_file.log, p_message);
   else
    dbms_output.put_line(p_message);
   end if;
  exception
   when others then
    fnd_file.put_line(fnd_file.log, 'Issue in print_log routine ='||sqlerrm);
  end print_log;

  procedure main (
    retcode               out number
   ,errbuf                out varchar2
  ) is    
  --      
   cursor db_users is
   select oracle_username
         ,description 
   from   fnd_oracle_userid
   where  1 =1
     and  not exists
           (
             select '1'
             from   fnd_lookup_values_vl
             where  1 =1
               and  lookup_type ='XXCUS_FND_GATHER_SCHEMA_EXCL'
               and  lookup_code =oracle_username
           );   
   /*
   select oracle_username
         ,description 
   from   fnd_oracle_userid;
  */   
  --  
    type my_user_tbl is table of db_users%rowtype index by binary_integer;
    my_user_rec my_user_tbl;          
  --
   lc_request_data VARCHAR2(20) :='';
   ln_request_id   NUMBER :=0;
   n_req_id NUMBER :=0;
   b_keep_going BOOLEAN;
  --
  begin  
  
   lc_request_data :=fnd_conc_global.request_data;
   
   n_req_id :=fnd_global.conc_request_id;
 
   if lc_request_data is null then     
       
       lc_request_data :='1';    
       
      Begin 
       --
       Begin 
         --
         Open db_users;
         Fetch db_users Bulk Collect Into my_user_rec;
         Close db_users;
         --
         b_keep_going :=TRUE;
         --       
       Exception
        when others then
           b_keep_going :=FALSE;         
       End;   
       --
      Exception
       When Others Then
       --
        b_keep_going :=FALSE;
       --
      End;     
      -- 
      if (b_keep_going) then
       --
          If my_user_rec.count =0 Then
          
           print_log('No active db user to gather statistics');
           
          Else --my_user_rec.count >0 
           print_log('Total db users ='||my_user_rec.count);
           --      
              for indx in 1 .. my_user_rec.count loop                                   
                --                                   
                    ln_request_id :=fnd_request.submit_request
                          (
                           application      =>'FND',
                           program          =>'FNDGSCST',
                           description      =>'',
                           start_time       =>'',
                           sub_request      =>TRUE,
                           argument1        =>my_user_rec(indx).oracle_username,
                           argument2        =>'', --'10',
                           argument3        =>'6', --'4',
                           argument4        =>'BACK',                       
                           argument5        =>'',
                           argument6        =>'LASTRUN',
                           argument7        =>'GATHER AUTO', --'GATHER',                                                                                                                                                                                                                                                                                  
                           argument8        =>'5', --'10',
                           argument9        =>'Y' --'N'                                                      
                          ); 
                          
                      if ln_request_id >0 then 
                        print_log('Submitted Gather Schema Statistics for DB user '
                                                         ||my_user_rec(indx).oracle_username
                                                         ||', Request Id ='
                                                         ||ln_request_id
                                 );
                      else
                        print_log('Failed to submit Gather Schema Statistics for DB user '
                                                         ||my_user_rec(indx).oracle_username                                                    
                                 );
                      end if;     
                       
                     fnd_conc_global.set_req_globals(conc_status =>'PAUSED' ,request_data =>lc_request_data);
               
                     lc_request_data :=to_char(to_number(lc_request_data)+1);      
              end loop;            
           --
          End If; -- checking if my_offer_rec.count =0
       --      
      else
       print_log('Variable b_keep_going is set to FALSE, message ='||sqlerrm);
      end if;
      --                                   
   else     
    --           
      retcode :=0;
      errbuf :='Child request[s] completed. Exit HDS Gather Schema Statistics -Master';
      print_log(errbuf);
    --
   end if;
  exception
   when others then
    print_log('Error in caller Apps.XXCUS_FNDGSCST_PKG.Main ='||sqlerrm);
    rollback;
  end main;
  
end xxcus_fndgscst_pkg;
/
