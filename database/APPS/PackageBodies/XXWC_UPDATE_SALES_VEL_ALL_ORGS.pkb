CREATE OR REPLACE PACKAGE BODY APPS.xxwc_update_sales_vel_all_orgs
AS
/******************************************************************************
   NAME:       XXWC_UPDATE_SALES_VEL_ALL_ORGS
   PURPOSE:
/*
 -- Date: 24-JUN-2014
 -- Author: Dheeresh Chintala
 -- Scope: This is a wrapper program to run XXWC Calculate and Update Sales Velocity - Program
           for all branches
 -- TMS ticket - 20131120-00056
 -- Update History:
 --
 -- Parameters: None

   REVISIONS:
   Ver        Date        Author            Description
   ---------  ----------  ---------------   ------------------------------------
   1.0        6/24/2014   Dheeresh Chintala 1. Created this package.
   1.1        8/11/2014   Manjula Chellappan Modified the concurrent program submission to pass organization_code as parameter
                                             For TMS# 20140810-00015  
   1.2		  11/04/2014  Veera 		     TMS#20141002-00038 Canada OU Changes		
   1.3		  07/24/2018  Naveen Kalidindi	 TMS# 20180722-00005 Exclude AHH Orgs
******************************************************************************/

   /*************************************************************************
        Procedure : Main

        PURPOSE:   This procedure spawns XWC Calculate and Update Sales Velocity - Program for each org
        Parameter: p_region

   REVISIONS:
   Ver        Date        Author            Description
   ---------  ----------  ---------------   ------------------------------------
   1.0        6/24/2014   Dheeresh Chintala Created this package.TMS ticket - 20131120-00056
   1.1        8/11/2014   Manjula Chellappan Modified the concurrent program submission to pass organization_code as parameter
                                             For TMS# 20140810-00015  
   1.2		  11/04/2014  Veera 		     TMS#20141002-00038 Canada OU Changes 
      ************************************************************************/

   -- Global variables
   g_application     fnd_application.application_short_name%TYPE   := 'XXWC';
   g_cp_short_code   VARCHAR2 (60)         := 'XXWC_CALCULATE_SALES_VELOCITY';
                                                -- XXWC Cycle Count Scheduler
   g_dflt_email      fnd_user.email_address%TYPE
                                        := 'HDSOracleDevelopers@hdsupply.com';

   PROCEDURE main (
      retcode    OUT      NUMBER,
      errbuf     OUT      VARCHAR2,
      p_region   IN       VARCHAR2
   )
   IS
       /*CURSOR wc_inv_orgs (n_org_id IN NUMBER) IS
            SELECT organization_id,
                   organization_code,
                   organization_name,
                   set_of_books_id,
                   chart_of_accounts_id,
                   legal_entity
              FROM org_organization_definitions
             WHERE 1 = 1
               AND operating_unit =n_org_id
               AND disable_date IS NULL
               AND organization_code NOT IN ('WCC', 'MST') --WC Corporate Organization, WC Item Master
               --AND rownum <3    --testing for upto 7 organizations
          ORDER BY organization_code ASC;
      */
      CURSOR cur_inv_orgs (n_org_id IN NUMBER)
      IS
         SELECT   ood.organization_id, ood.organization_code,
                  ood.organization_name, ood.set_of_books_id,
                  ood.chart_of_accounts_id, ood.legal_entity,
                  mp.attribute9 region
             FROM org_organization_definitions ood, mtl_parameters mp
            WHERE 1 = 1
              AND ood.operating_unit = n_org_id
              AND ood.disable_date IS NULL
             /* AND ood.organization_code NOT IN
                     ('WCC', 'MST')*/ --TMS#20141002-00038 Commented by Veera
                                   --WC Corporate Organization, WC Item Master
			  AND ood.organization_code NOT IN
                     (fnd_profile.VALUE ('XXWC_CORPORATE_ORGANIZATION'), 'MST')			   
								    --TMS#20141002-00038 Added by Veera
              AND ood.organization_id = mp.organization_id
              -- Version 1.3 TMS# 20180722-00005 Exclude AHH Orgs
			  AND ood.organization_code NOT IN (SELECT lookup_code
                                       FROM applsys.fnd_lookup_values
                                      WHERE lookup_type = 'XXWC_INV_EXCL_ORG_FOR_SV_CLASS'
                                        AND enabled_flag = 'Y'
                                        AND SYSDATE BETWEEN nvl(start_date_active
                                                               ,trunc(SYSDATE)) AND
                                            nvl(end_date_active
                                               ,trunc(SYSDATE + 1)))
			  AND mp.attribute9 = NVL (p_region, mp.attribute9)
         ORDER BY ood.organization_code ASC;

      TYPE l_inv_orgs_tbl IS TABLE OF cur_inv_orgs%ROWTYPE
         INDEX BY BINARY_INTEGER;

      l_inv_orgs_rec   l_inv_orgs_tbl;
      l_request_data   VARCHAR2 (20)      := '';
      l_request_id     NUMBER             := 0;
      l_org_id         NUMBER             := 0;
      l_user_id        NUMBER             := 0;
      output_file_id   UTL_FILE.file_type;
      v_path           VARCHAR2 (80);
      p_period_id      NUMBER             := 0;
      l_sec            VARCHAR2 (100);
   BEGIN
      l_sec := 'Start of Procedure MAIN';
      l_request_data := fnd_conc_global.request_data;

      --n_req_id :=fnd_global.conc_request_id;
      IF l_request_data IS NULL
      THEN
         l_request_data := '1';
         l_org_id := mo_global.get_current_org_id;
         l_user_id := fnd_global.user_id;
         fnd_file.put_line (fnd_file.LOG, '');
         fnd_file.put_line (fnd_file.LOG, 'l_org_id =' || l_org_id);
         fnd_file.put_line (fnd_file.LOG, 'l_user_id =' || l_user_id);
         fnd_file.put_line (fnd_file.LOG, '');
         l_sec := 'Before opening the cursor - CUR_INV_ORGS';

         OPEN cur_inv_orgs (l_org_id);

         FETCH cur_inv_orgs
         BULK COLLECT INTO l_inv_orgs_rec;

         CLOSE cur_inv_orgs;

         IF l_inv_orgs_rec.COUNT = 0
         THEN
            fnd_file.put_line
               (fnd_file.LOG,
                'No active inventory organizations found for operating unit WhiteCap'
               );
         ELSE                                       --wc_inv_orgs_rec.count >0
            l_sec :=
                   'Trigger Calc and Update Sales Velocity for every Inv Org';

            -- We will kick off for every single inventory orgs and not wait to check how many are running at a time
            FOR indx IN 1 .. l_inv_orgs_rec.COUNT
            LOOP
               fnd_file.put_line (fnd_file.LOG,
                                     'Org IDs - '
                                  || l_inv_orgs_rec (indx).organization_id
                                 );
               --
               -- If the concurrent program requires operating unit as a parameter then uncomment the below call
               -- fnd_request.set_org_id(mo_global.get_current_org_id);
               --

               --Except for arguments 1, 2 and 7 all other arguments are defaulted based on the parameter values
               --established from a sample run by the end user
               l_sec :=
                     'Before submitting the concurrent program - '
                  || g_cp_short_code;
                  
-- Modififed the below statement for TMS# 20140810-00015 By Manjula to pass the organization_code as parameter value

               l_request_id :=
                  fnd_request.submit_request
                           (application      => g_application,
                            program          => g_cp_short_code,
                            description      => '',
                            start_time       => '',
                            sub_request      => TRUE,
                            argument1        => l_inv_orgs_rec (indx).organization_code,
                            argument2        => 'N'               --Debug Flag
                                                   ,
                            argument3        => 'N'     --Print Detail Report)
                           );
               COMMIT;
               l_sec :=
                     'After submitting the concurrent program - '
                  || g_cp_short_code;
               fnd_file.put_line (fnd_file.LOG,
                                  'Request_id: ' || l_request_id);

               IF l_request_id > 0
               THEN
                  fnd_file.put_line
                     (fnd_file.LOG,
                         'Submitted XXWC Calculate and Update Sales Velocity - Program for organization ='
                      || l_inv_orgs_rec (indx).organization_name
                      || ', Request Id ='
                      || l_request_id
                     );
               ELSE
                  fnd_file.put_line
                     (fnd_file.LOG,
                         'Failed to submit XXWC Calculate and Update Sales Velocity - Program for organization ='
                      || l_inv_orgs_rec (indx).organization_name
                     );
                  fnd_file.put_line (fnd_file.LOG,
                                     SQLCODE || ' Error :' || SQLERRM
                                    );
               END IF;

               fnd_conc_global.set_req_globals (conc_status       => 'PAUSED',
                                                request_data      => l_request_data
                                               );
               l_request_data := TO_CHAR (TO_NUMBER (l_request_data) + 1);
            END LOOP;
         END IF;                       -- checking if wc_inv_orgs_rec.count =0
      ELSE
            /* For future use comment Begin
                Begin
                    select a.user_concurrent_program_name
                          ,c.email_address
                    into   v_cp_long_name
                          ,v_email
                    from fnd_amp_requests_v a
                        ,fnd_concurrent_programs d
                        ,fnd_user c
                    where 1 =1
                      and a.request_id =n_req_id
                      and a.concurrent_program_id =d.concurrent_program_id
                      and a.requested_by =c.user_id;

                Exception
                 When Others Then
                   Null;
                End;
         For future use comment End */
         retcode := 0;
         errbuf :=
            'Child request[s] completed. Exit XXWC Calculate and Update Sales Velocity - Program -Master';
         fnd_file.put_line (fnd_file.LOG, errbuf);
      --return;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         fnd_file.put_line
             (fnd_file.LOG,
                 'Error in caller apps.XXWC_UPDATE_SALES_VEL_ALL_ORGS.MAIN ='
              || SQLERRM
             );
         xxcus_error_pkg.xxcus_error_main_api
            (p_called_from            => 'XXWC_UPDATE_SALES_VEL_ALL_ORGS.MAIN',
             p_calling                => l_sec,
             p_request_id             => fnd_global.conc_request_id,
             p_ora_error_msg          => SUBSTR (SQLERRM, 1, 2000),
             p_error_desc             => 'Error in XXWC Calculate and Update Sales Velocity - Program -Main procedure, When Others Exception',
             p_distribution_list      => g_dflt_email,
             p_module                 => 'XXWC'
            );
         fnd_file.put_line
            (fnd_file.LOG,
                'Error in caller apps.XXWC_INV_CYCLE_COUNT_PKG.SUBMIT_SCHEDULER ='
             || SQLERRM
            );
         ROLLBACK;
   END main;
END xxwc_update_sales_vel_all_orgs;
/
