CREATE OR REPLACE PACKAGE BODY APPS.XXWC_LOCATOR_MASS_UPLOAD_PKG
AS
   /*************************************************************************
        $Header XXWC_LOCATOR_MASS_UPLOAD_PKG $
        Module Name: XXWC_LOCATOR_MASS_UPLOAD_PKG.pkb

        PURPOSE:   This package is used by the WebADI For Mass Location/Assignment
                   upload

        REVISIONS:
        Ver        Date        Author                     Description
        ---------  ----------  ---------------         -------------------------
        1.0        08/23/2013  Lee Spitzer             Initial Version
        2.0        10/18/2013  Consuelo Gonzalez       Completed processing code
        3.0        11/26/2013  Consuelo Gonzalez       TMS 20131120-00024: Added ability to delete
                                                       bin assignment, added archive functionality,
                                                       added unique key per record, added WHO columns
        4.0        01/22/2014  Consuelo Gonzalez       TMS 20140122-00181: Updated to allow assignment of
                                                       multiple 0- locatioins.
        5.0        05/18/2015  Manjula Chellappan      TMS 20150430-00019 : Create Copy of XXWC Stock Locator Assignment
                                                       for Branch Relocation.New Procedure Upload_subinv_stock_loc added 
      **************************************************************************/



   /**************************************************************************
    *
    * PROCEDURE
    *  Bin_Upload
    *
    * DESCRIPTION
    *  This procedure is used to upload the user-supplied data from the Excel spreadsheet into the custom interface table
    *
    * PARAMETERS
    * ==========
    * NAME                   TYPE     IN/OUT  DESCRIPTION
   .* ---------------------- -------- ------  ---------------------------------------
    * P_ORG_CODE             VARCHAR2 IN      inventory organizatin code
    * P_ITEM_NUMBER          VARCHAR2 IN      inventory item number
    * P_OLD_LOCATION         VARCAHR2 IN      old stock locator
    * P_NEW_LOCATION         VARCAHR2 IN      new stock locator
    *
    * CALLED BY
    *  XXWC Item Location Upload Integrator during the WebADI upload process
    *
    *************************************************************************/
   PROCEDURE Bin_Upload (i_org_code       IN VARCHAR2,
                         i_item_number    IN VARCHAR2,
                         i_item_desc      IN VARCHAR2,
                         i_cur_location   IN VARCHAR2,
                         i_delete_flag    IN VARCHAR2)
   IS
      XXWC_ERROR            EXCEPTION;
      -- global variables for error processing
      l_return_status       VARCHAR2 (1) := FND_API.G_Ret_Sts_Success;
      l_msg_count           NUMBER;
      l_msg_data            VARCHAR2 (4000);
      l_msg_index_out       NUMBER;

      -- Error DEBUG
      l_err_msg             VARCHAR2 (280);
      l_err_callfrom        VARCHAR2 (175) := 'XXWC_LOCATOR_MASS_UPLOAD_PKG';
      l_err_callpoint       VARCHAR2 (175) := 'START';
      l_distro_list         VARCHAR2 (80) := 'cgonzalez@luciditycg.com';
      l_module              VARCHAR2 (80);

      -- retrieve item / location assignment data from custom interface table
      CURSOR C_Item_Locations (
         p_proc_key    NUMBER)
      IS
           SELECT SLI.organization_code,
                  SLI.item_number,
                  SLI.cur_location,
                  REGEXP_REPLACE (SLI.cur_location, '\.', '')
                     formatted_location,
                  SLI.status,
                  mp.organization_id,
                  SLI.ROWID r_id,
                  -- 11/26/2013 CG: TMS 20131120-00024: Added Delete Flag
                  TRIM (UPPER (SLI.delete_flag)) delete_flag
             FROM XXWC.XXWC_INV_MASS_LOC_LOAD_IFACE SLI, mtl_parameters mp
            WHERE     sli.cust_assign_seq = p_proc_key
                  AND SLI.status IS NULL
                  AND sli.organization_code = mp.organization_code
         ORDER BY SLI.organization_code, SLI.item_number, SLI.cur_location;

      CURSOR c_item_cur_loc (
         p_inventory_item_id    NUMBER,
         p_organization_id      NUMBER,
         p_loc_level            NUMBER)
      IS
         SELECT msl.inventory_item_id,
                msl.organization_id,
                mil.inventory_location_id
           FROM MTL_SECONDARY_LOCATORS MSL, MTL_ITEM_LOCATIONS_KFV MIL
          WHERE     msl.inventory_item_id = p_inventory_item_id
                AND msl.organization_id = p_organization_id
                AND msl.subinventory_code = 'General'
                AND msl.secondary_locator = mil.inventory_location_id
                AND msl.organization_id = mil.organization_id
                AND msl.subinventory_code = msl.subinventory_code
                AND SUBSTR (segment1,
                            1,
                              INSTR (segment1,
                                     '-',
                                     1,
                                     1)
                            - 1) = TO_CHAR (p_loc_level);

      l_locator             VARCHAR2 (80);
      l_loc_level           NUMBER;
      l_cur_locator_id      NUMBER;
      l_dummy_loc_id        NUMBER;
      i                     NUMBER;
      l_loc_exists          NUMBER;
      l_inventory_item_id   NUMBER;
      l_item_loc_exist      NUMBER;

      l_cnt                 NUMBER := 0;
      l_del_cnt             NUMBER := 0;

      -- 11/26/2013 CG: TMS 20131120-00024: Added WHO Columns Vars and unique sequence
      l_user_id             NUMBER
         := NVL (TO_NUMBER (fnd_profile.VALUE ('USER_ID')), -1);
      l_resp_id             NUMBER := fnd_profile.VALUE ('RESP_ID');
      l_resp_appl_id        NUMBER := fnd_profile.VALUE ('RESP_APPL_ID');
      l_login_id            NUMBER := fnd_profile.VALUE ('LOGIN_ID');
      l_unique_key          NUMBER;
      l_cur_location        NUMBER;
   BEGIN
      l_module := 'Load_Location';

      IF i_cur_location IS NOT NULL
      THEN
         l_unique_key := NULL;

         BEGIN
            SELECT xxwc.xxwc_inv_mass_loc_upl_s.NEXTVAL
              INTO l_unique_key
              FROM DUAL;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_unique_key := NULL;
         END;

         BEGIN
            INSERT INTO XXWC.XXWC_INV_MASS_LOC_LOAD_IFACE (organization_code,
                                                           item_number,
                                                           cur_location -- 11/26/2013 CG: TMS 20131120-00024: Added delete flag, WHO Columns and unique sequence
                                                                       ,
                                                           delete_flag,
                                                           creation_date,
                                                           created_by,
                                                           last_update_date,
                                                           last_updated_by,
                                                           last_update_login,
                                                           cust_assign_seq)
                 VALUES (i_org_code,
                         i_item_number,
                         i_cur_location -- 11/26/2013 CG: TMS 20131120-00024: Added delete flag, WHO Columns and unique sequence
                                       ,
                         i_delete_flag,
                         SYSDATE,
                         l_user_id,
                         SYSDATE,
                         l_user_id,
                         l_login_id,
                         l_unique_key);
         EXCEPTION
            WHEN OTHERS
            THEN
               NULL;
         END;
      END IF;

      COMMIT;

      -- loop thru all records in interface table
      l_err_callpoint := 'Before C_Item_Locations Loop';

      FOR R1 IN C_Item_Locations (l_unique_key)
      LOOP
         -- 11/26/2013 CG: TMS 20131120-00024: Added to delete marked records
         IF NVL (r1.delete_flag, 'N') = 'Y'
         THEN
            l_inventory_item_id := NULL;

            BEGIN
               SELECT inventory_item_id
                 INTO l_inventory_item_id
                 FROM mtl_system_items_b
                WHERE     organization_id = r1.organization_id
                      AND segment1 = r1.item_number;
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_inventory_item_id := NULL;
            END;

            l_cur_location := NULL;

            BEGIN
               SELECT inventory_location_id
                 INTO l_cur_location
                 FROM mtl_item_locations
                WHERE     segment1 = r1.formatted_location
                      AND organization_id = r1.organization_id
                      AND subinventory_code = 'General';
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_cur_location := NULL;
            END;

            IF l_inventory_item_id IS NOT NULL AND l_cur_location IS NOT NULL
            THEN
               BEGIN
                  DELETE FROM MTL_SECONDARY_LOCATORS
                        WHERE     organization_id = r1.organization_id
                              AND secondary_locator = l_cur_location
                              AND inventory_item_id = l_inventory_item_id;
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     NULL;
               END;
            END IF;
         ELSE
            --dbms_output.put_line('Starting code...');
            -- Verifying Locator
            l_locator := NULL;
            l_loc_level := NULL;
            l_cur_locator_id := NULL;

            BEGIN
               SELECT SUBSTR (R1.formatted_location,
                              1,
                                INSTR (R1.formatted_location,
                                       '-',
                                       1,
                                       1)
                              - 1)
                         loc_level,
                      SUBSTR (R1.formatted_location,
                              (  INSTR (R1.formatted_location,
                                        '-',
                                        1,
                                        1)
                               + 1),
                                LENGTH (R1.formatted_location)
                              - (INSTR (R1.formatted_location,
                                        '-',
                                        1,
                                        1)))
                         main_loc
                 INTO l_loc_level, l_locator
                 FROM DUAL;
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_locator := NULL;
                  l_loc_level := NULL;
            END;

            --dbms_output.put_line('l_locator: '||l_locator);
            --dbms_output.put_line('l_loc_level: '||l_loc_level);

            BEGIN
               SELECT mil.inventory_location_id
                 INTO l_cur_locator_id
                 FROM MTL_ITEM_LOCATIONS_KFV mil
                WHERE     mil.segment1 = R1.formatted_location
                      AND mil.organization_id = r1.organization_id
                      AND mil.subinventory_code = 'General';
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_cur_locator_id := NULL;
            END;

            --dbms_output.put_line('l_cur_locator_id: '||l_cur_locator_id);

            -- Creating Locators if they dont exist (1-, 2- and 3-)
            -- 01/22/14 CG: TMS 20140122-00181: Updated to allow creation dynamically of 0- if they dont exist
            -- i := 1;
            i := 0;

            -- for i IN 1..G_LOC_MAX_LEVELS
            FOR i IN 0 .. G_LOC_MAX_LEVELS
            LOOP
               -- if i = nvl(l_loc_level,0) and l_cur_locator_id is null then
               IF i = NVL (l_loc_level, -1) AND l_cur_locator_id IS NULL
               THEN
                  create_locator (r1.organization_id,
                                  'General',
                                  (i || '-' || l_locator || '.'),
                                  l_cur_locator_id);
               --dbms_output.put_line('Created item locator '||l_cur_locator_id||' for level '||i);
               -- elsif i != nvl(l_loc_level,0) then
               ELSIF i != NVL (l_loc_level, -1)
               THEN
                  l_loc_exists := 0;

                  BEGIN
                     SELECT COUNT (*)
                       INTO l_loc_exists
                       FROM MTL_ITEM_LOCATIONS_KFV mil
                      WHERE     mil.segment1 = (i || '-' || l_locator)
                            AND mil.organization_id = r1.organization_id
                            AND mil.subinventory_code = 'General';
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        l_loc_exists := 0;
                  END;

                  IF l_loc_exists = 0
                  THEN
                     l_dummy_loc_id := NULL;
                     create_locator (r1.organization_id,
                                     'General',
                                     (i || '-' || l_locator || '.'),
                                     l_dummy_loc_id);
                  --dbms_output.put_line('Created dummy locator '||l_dummy_loc_id||' for level '||i);
                  END IF;
               ELSE
                  -- noop
                  NULL;
               END IF;
            END LOOP;

            -- Verifying Item
            -- 01/22/14 CG: TMS 20140122-00181: Updated to allow use of 0- locators
            -- if r1.item_number is not null and nvl(l_loc_level,0) != 0 then
            IF r1.item_number IS NOT NULL AND NVL (l_loc_level, -1) != -1
            THEN
               l_inventory_item_id := NULL;

               BEGIN
                  SELECT inventory_item_id
                    INTO l_inventory_item_id
                    FROM mtl_system_items_b
                   WHERE     organization_id = r1.organization_id
                         AND segment1 = r1.item_number;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     l_inventory_item_id := NULL;
               END;

               --dbms_output.put_line('l_inventory_item_id: '||l_inventory_item_id);

               IF l_inventory_item_id IS NOT NULL
               THEN
                  -- Removing existing locations at same level of new assignment
                  -- 01/22/14 CG: TMS 20140122-00181: Added IF condition to prevent multiples locations other than 0-
                  IF l_loc_level != 0
                  THEN
                     FOR r2
                        IN c_item_cur_loc (l_inventory_item_id,
                                           r1.organization_id,
                                           l_loc_level)
                     LOOP
                        EXIT WHEN c_item_cur_loc%NOTFOUND;

                        BEGIN
                           DELETE FROM MTL_SECONDARY_LOCATORS
                                 WHERE     organization_id =
                                              r2.organization_id
                                       AND secondary_locator =
                                              r2.inventory_location_id
                                       AND inventory_item_id =
                                              r2.inventory_item_id;
                        EXCEPTION
                           WHEN NO_DATA_FOUND
                           THEN
                              NULL;
                        END;
                     END LOOP;
                  END IF;

                  IF l_cur_locator_id IS NOT NULL
                  THEN
                     -- create item / location assignment
                     l_err_callpoint :=
                        'Before call to INV_LOC_WMS_PUB.Create_Loc_Item_Tie';
                     INV_LOC_WMS_PUB.Create_Loc_Item_Tie (
                        x_return_status           => l_return_status,
                        x_msg_count               => l_msg_count,
                        x_msg_data                => l_msg_data,
                        p_inventory_item_id       => l_inventory_item_id,
                        p_item                    => NULL,
                        p_organization_id         => r1.organization_id,
                        p_organization_code       => NULL,
                        p_subinventory_code       => 'General',
                        p_inventory_location_id   => l_cur_locator_id,
                        p_locator                 => NULL,
                        p_status_id               => 1,
                        p_par_level               => NULL);

                     -- if problems, display messages and terminate processing
                     IF l_return_status != 'S'
                     THEN
                        FND_FILE.Put_Line (
                           FND_FILE.LOG,
                              'error processing item / location assignment for '
                           || R1.organization_code
                           || ' '
                           || R1.item_number
                           || ' '
                           || R1.cur_location);
                        DBMS_OUTPUT.put_line (
                              'error processing item / location assignment for '
                           || R1.organization_code
                           || ' '
                           || R1.item_number
                           || ' '
                           || R1.cur_location);

                        FOR ln_index IN 1 .. l_msg_count
                        LOOP
                           FND_MSG_PUB.Get (
                              p_msg_index       => ln_index,
                              p_encoded         => FND_API.G_False,
                              p_data            => l_msg_data,
                              p_msg_index_out   => l_msg_index_out);
                           FND_FILE.Put_Line (FND_FILE.LOG, l_msg_data);
                        END LOOP;

                        RAISE XXWC_ERROR;
                     END IF;

                     l_cnt := l_cnt + 1;
                  END IF;
               END IF;
            END IF;
         END IF;

         BEGIN
            UPDATE XXWC.XXWC_INV_MASS_LOC_LOAD_IFACE
               SET STATUS = 'PROCESSED'
             WHERE ROWID = r1.r_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               NULL;
         END;
      END LOOP;

      -- clean-up interface table
      l_err_callpoint := 'Before clean-up of XXWCINV_STOCK_LOCATOR_IFACE';


      -- all or nothing processing, as any errors will rollback changes
      COMMIT;
      FND_FILE.Put_Line (FND_FILE.LOG,
                         l_del_cnt || ' Item / Location Assignments Deleted');
      FND_FILE.Put_Line (FND_FILE.LOG,
                         l_cnt || ' Item / Location Assignments Created');

      l_err_callpoint := 'Before End';
   EXCEPTION
      WHEN XXWC_ERROR
      THEN
         --errbuf := 'Warning';
         --retcode := 1;
         ROLLBACK;
      WHEN OTHERS
      THEN
         --errbuf := SQLERRM;
         --retcode := 2;
         ROLLBACK;
         l_err_msg :=
               'Error in '
            || l_err_callpoint
            || ' WITH  '
            || SUBSTR (SQLERRM, 1, 1000);
         --dbms_output.put_line(l_fulltext);
         FND_FILE.Put_Line (FND_FILE.LOG, l_err_msg);
         FND_FILE.Put_Line (FND_FILE.Output, l_err_msg);

         XXCUS_ERROR_PKG.XXCUS_ERROR_MAIN_API (
            p_called_from         => l_err_callfrom,
            p_calling             => l_err_callpoint,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          => 'Error running XXWC_LOCATOR_MASS_UPLOAD_PKG package with PROGRAM ERROR',
            p_distribution_list   => l_distro_list,
            p_module              => l_module);
   END Bin_Upload;



   PROCEDURE Create_Locator (p_organization_id     IN     NUMBER,
                             p_subinventory_code   IN     VARCHAR2,
                             p_stock_locator       IN     VARCHAR2,
                             x_inventory_loc_id       OUT NUMBER)
   IS
      l_return_status           VARCHAR2 (1) := fnd_api.g_ret_sts_success;
      l_msg_count               NUMBER;
      l_msg_data                VARCHAR2 (4000);
      l_inventory_location_id   NUMBER;
      l_locator_exists          VARCHAR2 (1);
      l_error_msg               VARCHAR2 (2000) := NULL;
   BEGIN
      mo_global.init ('INV');

      l_error_msg := NULL;
      l_inventory_location_id := NULL;
      l_locator_exists := NULL;
      l_msg_data := NULL;
      l_msg_count := NULL;
      l_return_status := NULL;

      inv_loc_wms_pub.create_locator (
         x_return_status              => l_return_status,
         x_msg_count                  => l_msg_count,
         x_msg_data                   => l_msg_data,
         x_inventory_location_id      => l_inventory_location_id,
         x_locator_exists             => l_locator_exists,
         p_organization_id            => p_organization_id,
         p_organization_code          => NULL,
         p_concatenated_segments      => p_stock_locator,
         p_description                => '',
         p_inventory_location_type    => NULL,                            --3,
         p_picking_order              => NULL,
         p_location_maximum_units     => NULL,
         p_subinventory_code          => p_subinventory_code,
         p_location_weight_uom_code   => NULL,
         p_max_weight                 => NULL,
         p_volume_uom_code            => NULL,
         p_max_cubic_area             => NULL,
         p_x_coordinate               => NULL,
         p_y_coordinate               => NULL,
         p_z_coordinate               => NULL,
         p_physical_location_id       => NULL,
         p_pick_uom_code              => NULL,
         p_dimension_uom_code         => NULL,
         p_length                     => NULL,
         p_width                      => NULL,
         p_height                     => NULL,
         p_status_id                  => 1,
         p_dropping_order             => NULL,
         p_attribute_category         => '',
         p_attribute1                 => '',
         p_attribute2                 => '',
         p_attribute3                 => '',
         p_attribute4                 => '',
         p_attribute5                 => '',
         p_attribute6                 => '',
         p_attribute7                 => '',
         p_attribute8                 => '',
         p_attribute9                 => '',
         p_attribute10                => '',
         p_attribute11                => '',
         p_attribute12                => '',
         p_attribute13                => '',
         p_attribute14                => '',
         p_attribute15                => '',
         p_alias                      => '');

      IF (l_return_status <> 'S')
      THEN
         FOR l_index IN 1 .. l_msg_count
         LOOP
            l_msg_data := fnd_msg_pub.get (l_index, 'F');
         --l_error_msg :=                         l_error_msg || '-' || SUBSTR (l_msg_data, 1, 50);
         END LOOP;

         l_error_msg := l_error_msg || '-' || SUBSTR (l_msg_data, 1, 100);

         DBMS_OUTPUT.put_line ('Err creating loc ' || l_error_msg);

         -- 01/22/14 CG: TMS 20140122-00181: Added to view if there are errors in mass creation
         fnd_file.put_line (
            fnd_file.output,
            'Err creating loc ' || p_stock_locator || ': ' || l_error_msg);
      END IF;

      x_inventory_loc_id := l_inventory_location_id;
   END Create_Locator;

   -- 01/22/14 CG: TMS 20140122-00181: Added new procedure to regularly create the missing 0- locators
   PROCEDURE xxwc_created_0_loc (errbuf        OUT VARCHAR2,
                                 retcode       OUT NUMBER,
                                 p_org_id   IN     NUMBER)
   IS
      CURSOR cur_org_list
      IS
           SELECT organization_id, organization_code
             FROM mtl_parameters mp
            WHERE mp.organization_id = NVL (p_org_id, mp.organization_id)
         ORDER BY mp.organization_code;

      CURSOR cur_org_locators (
         p_organization_id    NUMBER)
      IS
         SELECT DISTINCT (mp.organization_code),
                         mp.organization_id,
                         mil.subinventory_code,
                         SUBSTR (mil.segment1,
                                 (  INSTR (mil.segment1,
                                           '-',
                                           1,
                                           1)
                                  + 1),
                                   LENGTH (mil.segment1)
                                 - (INSTR (mil.segment1,
                                           '-',
                                           1,
                                           1)))
                            main_loc
           --, ('0-'||substr(mil.segment1, (instr(mil.segment1, '-',1,1)+1), length(mil.segment1) - (instr(mil.segment1, '-',1,1)))||'.') new_locator
           --, mil.segment1 locators
           FROM mtl_item_locations mil, mtl_parameters mp
          WHERE     mil.subinventory_code = 'General'
                AND mil.organization_id = mp.organization_id
                AND mp.organization_id = p_organization_id
                AND NOT EXISTS
                           (SELECT 'Has 0-'
                              FROM mtl_item_locations mil2
                             WHERE     mil2.segment1 =
                                          (   '0-'
                                           || SUBSTR (mil.segment1,
                                                      (  INSTR (mil.segment1,
                                                                '-',
                                                                1,
                                                                1)
                                                       + 1),
                                                        LENGTH (mil.segment1)
                                                      - (INSTR (mil.segment1,
                                                                '-',
                                                                1,
                                                                1))))
                                   AND mil2.organization_id =
                                          mil.organization_id
                                   AND mil2.subinventory_code =
                                          mil.subinventory_code);

      l_count_org_loc    NUMBER;
      l_count            NUMBER;
      l_cur_locator_id   NUMBER;


      l_error_message2   CLOB;
      l_conc_req_id      NUMBER := fnd_global.conc_request_id;
      l_start_time       NUMBER;
      l_end_time         NUMBER;
      l_login_id         NUMBER := fnd_global.login_id;
   BEGIN
      fnd_file.put_line (fnd_file.output,
                         'Starting XXWC INV Mass Create 0- Locators');
      fnd_file.put_line (
         fnd_file.output,
         '=====================================================');
      fnd_file.put_line (fnd_file.output, ' ');

      l_count := 0;

      FOR c0 IN cur_org_list
      LOOP
         EXIT WHEN cur_org_list%NOTFOUND;

         fnd_file.put_line (
            fnd_file.output,
            'Starting 0- creation for ' || c0.organization_code);
         l_count_org_loc := 0;

         FOR c1 IN cur_org_locators (c0.organization_id)
         LOOP
            EXIT WHEN cur_org_list%NOTFOUND;

            create_locator (c0.organization_id,
                            'General',
                            (0 || '-' || c1.main_loc || '.'),
                            l_cur_locator_id);

            IF l_cur_locator_id IS NOT NULL
            THEN
               l_count := l_count + 1;
               l_count_org_loc := l_count_org_loc + 1;
            END IF;
         END LOOP;

         fnd_file.put_line (
            fnd_file.output,
               'Created '
            || l_count_org_loc
            || ' for org '
            || c0.organization_code);
         fnd_file.put_line (fnd_file.output,
                            '--------------------------------');
         fnd_file.put_line (fnd_file.output, ' ');
         COMMIT;
      END LOOP;

      fnd_file.put_line (fnd_file.output, ' ');
      fnd_file.put_line (
         fnd_file.output,
         '=====================================================');
      fnd_file.put_line (fnd_file.output, ' ');
      fnd_file.put_line (fnd_file.output,
                         'Created ' || l_count || ' loators');

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         l_error_message2 :=
               'XXWC_LOCATOR_MASS_UPLOAD_PKG.xxwc_created_0_loc '
            || 'Error_Stack...'
            || DBMS_UTILITY.format_error_stack ()
            || ' Error_Backtrace...'
            || DBMS_UTILITY.format_error_backtrace ();

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'xxwc_po_vendor_min_pkg.xxwc_vendor_min_extract_rpt',
            p_calling             => 'xxwc_po_vendor_min_pkg.xxwc_vendor_min_extract_rpt',
            p_request_id          => l_conc_req_id,
            p_ora_error_msg       => l_error_message2,
            p_error_desc          => 'Error running XXWC INV Mass Create 0- Locators',
            p_distribution_list   => 'cgonzalez@luciditycg.com' -- HDSOracleDevelopers@hdsupply.com
                                                               ,
            p_module              => 'INV');

         fnd_file.put_line (fnd_file.LOG, ' ');
         fnd_file.put_line (
            fnd_file.LOG,
               'Error in main XXWC_LOCATOR_MASS_UPLOAD_PKG.xxwc_created_0_loc. Error: '
            || l_error_message2);

         errbuf := l_error_message2;
         retcode := 2;
   END xxwc_created_0_loc;


   --<<Revision 5.0 Begin
   PROCEDURE Upload_subinv_stock_loc (p_org_code      IN VARCHAR2,
                                      p_item_number   IN VARCHAR2,
                                      p_cur_locator   IN VARCHAR2,
                                      p_cur_subinv    IN VARCHAR2,
                                      p_delete_flag   IN VARCHAR2)
   /*************************************************************************
   $Header XXWC_LOCATOR_MASS_UPLOAD_PKG.Upload_subinv_stock_loc $
    Module Name: Upload_subinv_stock_loc

    PURPOSE:   This package is used by the WebADI For Mass Location Assignment
                upload for BranchRelo subinventory

    REVISIONS:
    Ver        Date        Author                     Description
    ---------  ----------  ---------------         -------------------------
    1.0        05/18/2015  Manjula Chellappan      TMS 20150430-00019 : Create Copy of XXWC Stock Locator Assignment
                                                    for Branch Relocation.
    **************************************************************************/
   IS
      l_org_code            VARCHAR2 (5) := p_org_code;
      l_item_number         VARCHAR2 (40) := p_item_number;
      l_cur_locator         VARCHAR2 (40) := p_cur_locator;
      l_cur_subinv          VARCHAR2 (10) := p_cur_subinv;
      l_delete_flag         VARCHAR2 (10) := p_delete_flag;


      XXWC_ERROR            EXCEPTION;
      -- Error processing
      l_return_status       VARCHAR2 (1) := FND_API.G_Ret_Sts_Success;
      l_msg_count           NUMBER;
      l_msg_data            VARCHAR2 (4000);
      l_msg_index_out       NUMBER;

      -- Error DEBUG
      l_err_msg             VARCHAR2 (280);
      l_err_callfrom        VARCHAR2 (175)
         := 'XXWC_LOCATOR_MASS_UPLOAD_PKG.Upload_subinv_stock_loc';
      l_err_callpoint       VARCHAR2 (175) := 'START';
      l_distro_list         VARCHAR2 (80) := 'HDSOracleDevelopers@hdsupply.com';
      l_module              VARCHAR2 (80);
      l_sec                 VARCHAR2 (100);

      -- retrieve item / location assignment data from custom interface table
      CURSOR Cur_Item_Locations (
         l_proc_key    NUMBER)
      IS
           SELECT SLI.organization_code,
                  SLI.item_number,
                  SLI.cur_location,
                  SLI.cur_subinv,
                  REGEXP_REPLACE (SLI.cur_location, '\.', '')
                     formatted_location,
                  SLI.status,
                  mp.organization_id,
                  SLI.ROWID r_id,
                  TRIM (UPPER (SLI.delete_flag)) delete_flag
             FROM XXWC.XXWC_INV_MASS_LOC_LOAD_IFACE SLI, mtl_parameters mp
            WHERE     sli.cust_assign_seq = l_proc_key
                  AND SLI.status IS NULL
                  AND sli.organization_code = mp.organization_code
         ORDER BY SLI.organization_code, SLI.item_number, SLI.cur_location;

      CURSOR cur_item_cur_loc (
         l_inventory_item_id    NUMBER,
         l_organization_id      NUMBER,
         l_cur_subinv           VARCHAR2,
         l_loc_level            NUMBER)
      IS
         SELECT msl.inventory_item_id,
                msl.organization_id,
                mil.inventory_location_id
           FROM MTL_SECONDARY_LOCATORS MSL, MTL_ITEM_LOCATIONS_KFV MIL
          WHERE     msl.inventory_item_id = l_inventory_item_id
                AND msl.organization_id = l_organization_id
                AND msl.subinventory_code = l_cur_subinv
                AND msl.secondary_locator = mil.inventory_location_id
                AND msl.organization_id = mil.organization_id
                AND msl.subinventory_code = msl.subinventory_code
                AND SUBSTR (segment1,
                            1,
                              INSTR (segment1,
                                     '-',
                                     1,
                                     1)
                            - 1) = TO_CHAR (l_loc_level);

      l_locator             VARCHAR2 (80);
      l_loc_level           NUMBER;
      l_cur_locator_id      NUMBER;
      l_dummy_loc_id        NUMBER;
      i                     NUMBER;
      l_loc_exists          NUMBER;
      l_inventory_item_id   NUMBER;
      l_item_loc_exist      NUMBER;

      l_cnt                 NUMBER := 0;
      l_del_cnt             NUMBER := 0;

      l_user_id             NUMBER
         := NVL (TO_NUMBER (fnd_profile.VALUE ('USER_ID')), -1);
      l_resp_id             NUMBER := fnd_profile.VALUE ('RESP_ID');
      l_resp_appl_id        NUMBER := fnd_profile.VALUE ('RESP_APPL_ID');
      l_login_id            NUMBER := fnd_profile.VALUE ('LOGIN_ID');

      l_cust_assign_seq     NUMBER;
      l_cur_location        NUMBER;
   BEGIN
      l_err_callpoint := 'Begin';

      IF p_cur_locator IS NOT NULL
      THEN
         l_cust_assign_seq := NULL;

         l_err_callpoint := 'Get value for l_cust_assign_seq';

         BEGIN
            SELECT xxwc.xxwc_inv_mass_loc_upl_s.NEXTVAL
              INTO l_cust_assign_seq
              FROM DUAL;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_cust_assign_seq := NULL;
         END;

         l_err_callpoint := 'Insert into XXWC.XXWC_INV_MASS_LOC_LOAD_IFACE';

         BEGIN
            INSERT INTO XXWC.XXWC_INV_MASS_LOC_LOAD_IFACE (organization_code,
                                                           item_number,
                                                           cur_location,
                                                           cur_subinv,
                                                           delete_flag,
                                                           creation_date,
                                                           created_by,
                                                           last_update_date,
                                                           last_updated_by,
                                                           last_update_login,
                                                           cust_assign_seq)
                 VALUES (p_org_code,
                         p_item_number,
                         p_cur_locator,
                         p_cur_subinv,
                         p_delete_flag,
                         SYSDATE,
                         l_user_id,
                         SYSDATE,
                         l_user_id,
                         l_login_id,
                         l_cust_assign_seq);

            COMMIT;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_err_msg :=
                     l_err_msg
                  || 'Failed to insert record into XXWC_INV_MASS_LOC_LOAD_IFACE '
                  || SQLERRM
                  || CHR (32);
         END;
      END IF;


      l_err_callpoint := 'loop thru all records in interface table ';

      FOR Rec_Item_Locations IN Cur_Item_Locations (l_cust_assign_seq)
      LOOP
         IF NVL (Rec_Item_Locations.delete_flag, 'N') = 'Y'
         THEN
            l_inventory_item_id := NULL;

            l_err_callpoint := 'Get inventory_item_id For Deletion';

            BEGIN
               SELECT inventory_item_id
                 INTO l_inventory_item_id
                 FROM mtl_system_items_b
                WHERE     organization_id =
                             Rec_Item_Locations.organization_id
                      AND segment1 = Rec_Item_Locations.item_number;

               DBMS_OUTPUT.put_line (
                  'l_inventory_item_id : ' || l_inventory_item_id);
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_inventory_item_id := NULL;
            END;

            l_cur_location := NULL;


            l_err_callpoint := 'Get inventory_location_id For Deletion';

            BEGIN
               SELECT inventory_location_id
                 INTO l_cur_location
                 FROM mtl_item_locations
                WHERE     segment1 = Rec_Item_Locations.formatted_location
                      AND organization_id =
                             Rec_Item_Locations.organization_id
                      AND subinventory_code = l_cur_subinv;

               DBMS_OUTPUT.put_line ('l_cur_location : ' || l_cur_location);
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_cur_location := NULL;
            END;

            l_err_callpoint := 'Delete the Locator ';

            IF l_inventory_item_id IS NOT NULL AND l_cur_location IS NOT NULL
            THEN
               BEGIN
                  DELETE FROM MTL_SECONDARY_LOCATORS
                        WHERE     organization_id =
                                     Rec_Item_Locations.organization_id
                              AND secondary_locator = l_cur_location
                              AND inventory_item_id = l_inventory_item_id;
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     NULL;
               END;
            END IF;
         ELSE
            DBMS_OUTPUT.put_line ('Create New Locator ');

            l_err_callpoint := 'Create New Locator ';

            l_locator := NULL;
            l_loc_level := NULL;
            l_cur_locator_id := NULL;

            l_err_callpoint :=
               'Get locator Level and Locator for creating New locator ';

            BEGIN
               SELECT SUBSTR (Rec_Item_Locations.formatted_location,
                              1,
                                INSTR (Rec_Item_Locations.formatted_location,
                                       '-',
                                       1,
                                       1)
                              - 1)
                         loc_level,
                      SUBSTR (
                         Rec_Item_Locations.formatted_location,
                         (  INSTR (Rec_Item_Locations.formatted_location,
                                   '-',
                                   1,
                                   1)
                          + 1),
                           LENGTH (Rec_Item_Locations.formatted_location)
                         - (INSTR (Rec_Item_Locations.formatted_location,
                                   '-',
                                   1,
                                   1)))
                         main_loc
                 INTO l_loc_level, l_locator
                 FROM DUAL;


               DBMS_OUTPUT.put_line ('l_loc_level : ' || l_loc_level);

               DBMS_OUTPUT.put_line ('l_locator : ' || l_locator);
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_locator := NULL;
                  l_loc_level := NULL;
            END;

            l_err_callpoint := 'Get inventory_location_id for creating new ';

            BEGIN
               SELECT mil.inventory_location_id
                 INTO l_cur_locator_id
                 FROM MTL_ITEM_LOCATIONS_KFV mil
                WHERE     mil.segment1 =
                             Rec_Item_Locations.formatted_location
                      AND mil.organization_id =
                             Rec_Item_Locations.organization_id
                      AND mil.subinventory_code = l_cur_subinv;

               DBMS_OUTPUT.put_line (
                  'l_cur_locator_id : ' || l_cur_locator_id);
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_cur_locator_id := NULL;
            END;

            i := 0;

            FOR i IN 0 .. G_LOC_MAX_LEVELS
            LOOP
               IF i = NVL (l_loc_level, -1) AND l_cur_locator_id IS NULL
               THEN
                  l_err_callpoint := 'Create new locator ';

                  create_locator (Rec_Item_Locations.organization_id,
                                  l_cur_subinv,
                                  (i || '-' || l_locator || '.'),
                                  l_cur_locator_id);
               ELSIF i != NVL (l_loc_level, -1)
               THEN
                  l_loc_exists := 0;

                  BEGIN
                     SELECT COUNT (*)
                       INTO l_loc_exists
                       FROM MTL_ITEM_LOCATIONS_KFV mil
                      WHERE     mil.segment1 = (i || '-' || l_locator)
                            AND mil.organization_id =
                                   Rec_Item_Locations.organization_id
                            AND mil.subinventory_code = l_cur_subinv;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        l_loc_exists := 0;
                  END;

                  l_err_callpoint :=
                     'Get inventory_location_id for creating new ';

                  IF l_loc_exists = 0
                  THEN
                     l_dummy_loc_id := NULL;
                     create_locator (Rec_Item_Locations.organization_id,
                                     l_cur_subinv,
                                     (i || '-' || l_locator || '.'),
                                     l_dummy_loc_id);
                     DBMS_OUTPUT.put_line (
                           'Created dummy locator '
                        || l_dummy_loc_id
                        || ' for level '
                        || i);
                  END IF;
               ELSE
                  NULL;
               END IF;
            END LOOP;

            l_err_callpoint := 'Verify Item ';

            IF     Rec_Item_Locations.item_number IS NOT NULL
               AND NVL (l_loc_level, -1) != -1
            THEN
               l_inventory_item_id := NULL;

               BEGIN
                  SELECT inventory_item_id
                    INTO l_inventory_item_id
                    FROM mtl_system_items_b
                   WHERE     organization_id =
                                Rec_Item_Locations.organization_id
                         AND segment1 = Rec_Item_Locations.item_number;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     l_inventory_item_id := NULL;
               END;

               DBMS_OUTPUT.put_line (
                  'l_inventory_item_id: ' || l_inventory_item_id);

               IF l_inventory_item_id IS NOT NULL
               THEN
                  l_err_callpoint :=
                     'Removing existing locations at same level of new assignment ';

                  IF l_loc_level != 0
                  THEN
                     FOR rec_item_cur_loc
                        IN cur_item_cur_loc (
                              l_inventory_item_id,
                              Rec_Item_Locations.organization_id,
                              Rec_Item_Locations.cur_subinv,
                              l_loc_level)
                     LOOP
                        EXIT WHEN cur_item_cur_loc%NOTFOUND;

                        BEGIN
                           DELETE FROM MTL_SECONDARY_LOCATORS
                                 WHERE     organization_id =
                                              rec_item_cur_loc.organization_id
                                       AND secondary_locator =
                                              rec_item_cur_loc.inventory_location_id
                                       AND inventory_item_id =
                                              rec_item_cur_loc.inventory_item_id;
                        EXCEPTION
                           WHEN NO_DATA_FOUND
                           THEN
                              NULL;
                        END;
                     END LOOP;
                  END IF;

                  IF l_cur_locator_id IS NOT NULL
                  THEN
                     l_err_callpoint := 'Create item / location assignment ';

                     l_err_callpoint :=
                        'Before call to INV_LOC_WMS_PUB.Create_Loc_Item_Tie';
                     INV_LOC_WMS_PUB.Create_Loc_Item_Tie (
                        x_return_status           => l_return_status,
                        x_msg_count               => l_msg_count,
                        x_msg_data                => l_msg_data,
                        p_inventory_item_id       => l_inventory_item_id,
                        p_item                    => NULL,
                        p_organization_id         => Rec_Item_Locations.organization_id,
                        p_organization_code       => NULL,
                        p_subinventory_code       => l_cur_subinv,
                        p_inventory_location_id   => l_cur_locator_id,
                        p_locator                 => NULL,
                        p_status_id               => 1,
                        p_par_level               => NULL);



                     IF l_return_status != 'S'
                     THEN
                        l_err_callpoint :=
                           'Error processing item / location assignment  ';

                        DBMS_OUTPUT.Put_Line (
                              'Error processing item / location assignment for '
                           || Rec_Item_Locations.organization_code
                           || ' '
                           || Rec_Item_Locations.item_number
                           || ' '
                           || Rec_Item_Locations.cur_location);


                        FOR ln_index IN 1 .. l_msg_count
                        LOOP
                           FND_MSG_PUB.Get (
                              p_msg_index       => ln_index,
                              p_encoded         => FND_API.G_False,
                              p_data            => l_msg_data,
                              p_msg_index_out   => l_msg_index_out);
                           DBMS_OUTPUT.Put_Line (l_msg_data);
                        END LOOP;

                        RAISE XXWC_ERROR;
                     END IF;

                     l_cnt := l_cnt + 1;
                  END IF;
               END IF;
            END IF;
         END IF;

         BEGIN
            l_err_callpoint := 'Update of XXWCINV_STOCK_LOCATOR_IFACE';

            UPDATE XXWC.XXWC_INV_MASS_LOC_LOAD_IFACE
               SET STATUS = 'PROCESSED'
             WHERE ROWID = Rec_Item_Locations.r_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_err_msg := SQLERRM;
         END;
      END LOOP;

      COMMIT;


      DBMS_OUTPUT.Put_Line (
         l_del_cnt || ' Item / Location Assignments Deleted');
      DBMS_OUTPUT.Put_Line (l_cnt || ' Item / Location Assignments Created');
   EXCEPTION
      WHEN XXWC_ERROR
      THEN
         fnd_message.set_name ('XXWC', 'XXWC_ADI_ERROR_MSG');
         fnd_message.set_token ('ERROR_MESSAGE', l_err_msg);

         ROLLBACK;
      WHEN OTHERS
      THEN
         ROLLBACK;
         l_err_msg :=
               'Error in '
            || l_err_callpoint
            || ' WITH  '
            || SUBSTR (SQLERRM, 1, 1000);

         FND_FILE.Put_Line (FND_FILE.LOG, l_err_msg);

         fnd_message.set_name ('XXWC', 'XXWC_ADI_ERROR_MSG');
         fnd_message.set_token ('ERROR_MESSAGE', l_err_msg);

         XXCUS_ERROR_PKG.XXCUS_ERROR_MAIN_API (
            p_called_from         => l_err_callfrom,
            p_calling             => l_err_callpoint,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (l_err_msg, 1, 240),
            p_distribution_list   => l_distro_list,
            p_module              => l_module);
   END Upload_subinv_stock_loc;
END XXWC_LOCATOR_MASS_UPLOAD_PKG;
/