--
-- XXWC_AP_INV_ADI_PKG  (Package Body) 
--
CREATE OR REPLACE PACKAGE BODY apps.xxwc_ap_inv_adi_pkg
IS
   /********************************************************************************
   FILE NAME: XXWC_AP_INV_ADI_PKG.pkg

   PROGRAM TYPE: PL/SQL Package Body

   PURPOSE: Package to load AP Invoice Interface Tables through ADI

   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.0     07/25/2012    Gopi Damuluri    Initial creation of the procedure
   1.1     06/25/2015    Pattabhi Avula   TMS#20140717-00179 Fixed Return Number
										  DFF issue
   ********************************************************************************/

   g_user_id   NUMBER := fnd_global.user_id;
   g_org_id    NUMBER := fnd_profile.VALUE ('ORG_ID');

   PROCEDURE load_interface_tbl (p_invoice_num                  IN VARCHAR2
                                ,p_invoice_type_lookup_code     IN VARCHAR2
                                ,p_invoice_date                 IN DATE
                                ,p_po_number                    IN VARCHAR2
                                ,p_vendor_num                   IN VARCHAR2
                                ,p_vendor_name                  IN VARCHAR2
                                ,p_vendor_site_code             IN VARCHAR2
                                ,p_invoice_amount               IN NUMBER
                                ,p_invoice_currency_code        IN VARCHAR2
                                ,p_exchange_rate                IN NUMBER
                                ,p_exchange_rate_type           IN VARCHAR2
                                ,p_exchange_date                IN DATE
                                ,p_terms_name                   IN VARCHAR2
                                ,p_description                  IN VARCHAR2
                                ,p_awt_group_name               IN VARCHAR2
                                ,p_attribute1                   IN VARCHAR2
                                ,p_attribute2                   IN VARCHAR2
                                ,p_attribute3                   IN VARCHAR2
                                ,p_attribute4                   IN VARCHAR2
                                ,p_attribute5                   IN VARCHAR2
                                ,p_attribute6                   IN VARCHAR2
                                ,p_attribute7                   IN VARCHAR2
                                ,p_attribute8                   IN VARCHAR2
                                ,p_attribute9                   IN VARCHAR2
                                ,p_attribute10                  IN VARCHAR2
                                ,p_attribute11                  IN VARCHAR2
                                ,p_attribute12                  IN VARCHAR2
                                ,p_attribute13                  IN VARCHAR2
                                ,p_attribute14                  IN VARCHAR2
                                ,p_attribute15                  IN VARCHAR2
                                ,p_source                       IN VARCHAR2
                                ,p_group_id                     IN VARCHAR2
                                ,p_payment_method_lookup_code   IN VARCHAR2
                                ,p_pay_group_lookup_code        IN VARCHAR2
                                ,p_gl_date                      IN DATE
                                ,p_line_type_lookup_code        IN VARCHAR2
                                ,p_amount                       IN NUMBER
                                ,p_accounting_date              IN DATE
                                ,p_line_description             IN VARCHAR2
                                ,p_distribution_set_name        IN VARCHAR2
                                ,p_dist_code_concatenated       IN VARCHAR2
                                ,p_line_attribute1              IN VARCHAR2
                                ,p_line_attribute2              IN VARCHAR2
                                ,p_line_attribute3              IN VARCHAR2
                                ,p_line_attribute4              IN VARCHAR2
                                ,p_line_attribute5              IN VARCHAR2
                                ,p_line_attribute6              IN VARCHAR2
                                ,p_line_attribute7              IN VARCHAR2
                                ,p_line_attribute8              IN VARCHAR2
                                ,p_line_attribute9              IN VARCHAR2
                                ,p_line_attribute10             IN VARCHAR2
                                ,p_line_attribute11             IN VARCHAR2
                                ,p_line_attribute12             IN VARCHAR2
                                ,p_line_attribute13             IN VARCHAR2
                                ,p_line_attribute14             IN VARCHAR2
                                ,p_line_attribute15             IN VARCHAR2
                                ,p_asset_book_type_code         IN VARCHAR2
                                ,p_tax_classification_code      IN VARCHAR2)
   IS
      l_inv_cnt          NUMBER := 0;
      l_intf_cnt         NUMBER := 0;
      l_vendor_id        NUMBER := NULL;
      l_vendor_site_id   NUMBER := NULL;
	  l_rtv_num          NUMBER := NULL;
      l_error_message    VARCHAR2 (250);
      PROGRAM_ERROR      EXCEPTION;
   BEGIN
      l_vendor_site_id := TO_NUMBER (p_vendor_site_code);

      --          l_error_message    := l_vendor_site_id;
      --          RAISE program_error;

      --------------------------------------------------------
      -- Validate Vendor Site Code
      --------------------------------------------------------
      BEGIN
         SELECT ap_ss.vendor_id
           INTO l_vendor_id
           FROM ap_supplier_sites_all ap_ss
          WHERE     1 = 1
                AND ap_ss.vendor_site_id = l_vendor_site_id
                AND ap_ss.org_id = g_org_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_error_message := 'Error validating Supplier Site - ' || SQLERRM;
            RAISE PROGRAM_ERROR;
      END;

      --------------------------------------------------------
      -- Check if invoice exists in Interface Table
      --------------------------------------------------------
      BEGIN
         SELECT COUNT (1)
           INTO l_intf_cnt
           FROM ap_invoices_interface
          WHERE     1 = 1
                AND invoice_num = p_invoice_num
                AND vendor_id = l_vendor_id
                AND org_id = g_org_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_error_message :=
                  'Error validating duplicate invoice in Interface Table - '
               || SQLERRM;
            RAISE PROGRAM_ERROR;
      END;

      --------------------------------------------------------
      -- Check if invoice exists in Interface Table
      --------------------------------------------------------
      IF p_dist_code_concatenated IS NULL
      THEN
         l_error_message :=
            'DIST_CODE_CONCATENATED is not populated - ' || SQLERRM;
         RAISE PROGRAM_ERROR;
      END IF;

      --------------------------------------------------------
      -- Check if invoice exists in EBS
      --------------------------------------------------------
      BEGIN
         SELECT COUNT (1)
           INTO l_inv_cnt
           FROM ap_invoices_all aia
          WHERE     1 = 1
                AND aia.invoice_num = p_invoice_num
                AND aia.vendor_id = l_vendor_id
                AND aia.org_id = g_org_id;

         IF l_inv_cnt > 0
         THEN
            l_error_message := 'Duplicate invoice in Oracle';
            RAISE PROGRAM_ERROR;
         END IF;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_error_message :=
               'Error validating if Invoice exists in EBS - ' || SQLERRM;
            RAISE PROGRAM_ERROR;
      END;
      /*--------------------------------------------------------
      -- Deriving Return Number  Start Ver#1.1 TMS#20140717-00179
      --------------------------------------------------------
       BEGIN
         SELECT  mmt.transaction_id
         INTO    l_rtv_num
         FROM    mtl_material_transactions mmt
         WHERE   mmt.transaction_type_id = 102
         AND     mmt.attribute2=p_line_attribute1;         		 
       EXCEPTION
	     WHEN no_data_found THEN
		    l_error_message :=
               'Return number not exists in EBS - ' || SQLERRM;
		 WHEN too_many_rows THEN
		   SELECT  mmt.transaction_id
            INTO    l_rtv_num
            FROM    mtl_material_transactions mmt
            WHERE   mmt.transaction_type_id = 102
            AND     mmt.attribute2=p_line_attribute1
			AND     rownum<2;
         WHEN OTHERS
         THEN
            l_error_message :=
               'Error while fetching the Return Number in EBS - ' || SQLERRM;
            l_rtv_num:=NULL;
      END; 
      -- End Ver#1.1  */
      IF l_intf_cnt = 0
      THEN
         --------------------------------------------------------
         -- Load invoice into AP Invoice Interface Table
         --------------------------------------------------------
         INSERT INTO ap_invoices_interface (invoice_id
                                           ,invoice_num
                                           ,invoice_type_lookup_code
                                           ,invoice_date
                                           ,po_number
                                           ,vendor_id
                                           ,vendor_site_id
                                           ,invoice_amount
                                           ,invoice_currency_code
                                           ,exchange_rate
                                           ,exchange_rate_type
                                           ,exchange_date
                                           ,terms_name
                                           ,description
                                           ,awt_group_name
                                           ,attribute1
                                           ,attribute2
                                           ,attribute3
                                           ,attribute4
                                           ,attribute5
                                           ,attribute6
                                           ,attribute7
                                           ,attribute8
                                           ,attribute9
                                           ,attribute10
                                           ,attribute11
                                           ,attribute12
                                           ,attribute13
                                           ,attribute14
                                           ,attribute15
                                           ,source
                                           ,GROUP_ID
                                           ,payment_method_lookup_code
                                           ,pay_group_lookup_code
                                           ,gl_date
                                           ,last_updated_by
                                           ,last_update_date
                                           ,created_by
                                           ,creation_date
                                           ,org_id)
              VALUES (ap_invoices_interface_s.NEXTVAL
                     ,p_invoice_num
                     ,p_invoice_type_lookup_code
                     ,p_invoice_date
                     ,p_po_number
                     ,l_vendor_id
                     ,l_vendor_site_id
                     ,p_invoice_amount
                     ,p_invoice_currency_code
                     ,p_exchange_rate
                     ,p_exchange_rate_type
                     ,p_exchange_date
                     ,p_terms_name
                     ,p_description
                     ,p_awt_group_name
                     ,p_attribute1
                     ,p_attribute2
                     ,p_attribute3
                     ,p_attribute4
                     ,p_attribute5
                     ,p_attribute6
                     ,p_attribute7
                     ,p_attribute8
                     ,p_attribute9
                     ,p_attribute10
                     ,p_attribute11
                     ,p_attribute12
                     ,p_attribute13
                     ,p_attribute14
                     ,p_attribute15
                     ,p_source
                     ,p_group_id
                     ,p_payment_method_lookup_code
                     ,p_pay_group_lookup_code
                     ,p_gl_date
                     ,g_user_id
                     ,SYSDATE
                     ,g_user_id
                     ,SYSDATE
                     ,g_org_id);
      END IF;


      --------------------------------------------------------
      -- Load invoice into AP Invoice Lines Interface Table
      --------------------------------------------------------
      INSERT INTO ap_invoice_lines_interface (invoice_id
                                             ,invoice_line_id
                                             ,line_type_lookup_code
                                             ,amount
                                             ,accounting_date
                                             ,description
                                             ,distribution_set_name
                                             ,dist_code_concatenated
                                             ,awt_group_name
											 ,attribute_category  -- Ver#1.1
                                             ,attribute1
                                             ,attribute2
                                             ,attribute3
                                             ,attribute4
                                             ,attribute5
                                             ,attribute6
                                             ,attribute7
                                             ,attribute8
                                             ,attribute9
                                             ,attribute10
                                             ,attribute11
                                             ,attribute12
                                             ,attribute13
                                             ,attribute14
                                             ,attribute15
                                             ,asset_book_type_code
                                             ,tax_classification_code
                                             ,last_updated_by
                                             ,last_update_date
                                             ,created_by
                                             ,creation_date
                                             ,org_id)
           VALUES (ap_invoices_interface_s.CURRVAL
                  ,ap_invoice_lines_interface_s.NEXTVAL
                  ,p_line_type_lookup_code
                  ,p_amount
                  ,p_accounting_date
                  ,p_line_description
                  ,p_distribution_set_name
                  ,p_dist_code_concatenated
                  ,p_awt_group_name
				  ,'162'   -- Ver#1.1
                  ,p_line_attribute1
                  ,p_line_attribute2
                  ,p_line_attribute3
                  ,p_line_attribute4
                  ,p_line_attribute5
                  ,p_line_attribute6
                  ,p_line_attribute7
                  ,p_line_attribute8
                  ,p_line_attribute9
                  ,p_line_attribute10
                  ,p_line_attribute11
                  ,p_line_attribute12
                  ,p_line_attribute13
                  ,p_line_attribute14
                  ,p_line_attribute15
                  ,p_asset_book_type_code
                  ,p_tax_classification_code
                  ,g_user_id
                  ,SYSDATE
                  ,g_user_id
                  ,SYSDATE
                  ,g_org_id);
   EXCEPTION
      WHEN PROGRAM_ERROR
      THEN
         fnd_message.set_name ('XXWC', 'XXWC_ADI_ERROR_MSG');
         fnd_message.set_token ('ERROR_MESSAGE', l_error_message);
      WHEN OTHERS
      THEN
         l_error_message := 'Unidentified error - ' || SQLERRM;
         fnd_message.set_name ('XXWC', 'XXWC_ADI_ERROR_MSG');
         fnd_message.set_token ('ERROR_MESSAGE', l_error_message);
   END load_interface_tbl;
END xxwc_ap_inv_adi_pkg;
/

