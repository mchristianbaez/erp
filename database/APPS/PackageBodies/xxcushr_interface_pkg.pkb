CREATE OR REPLACE PACKAGE BODY apps.xxcushr_interface_pkg AS
  /******************************************************************************
   NAME:       xxcushr_interface_pkg
   DESCRIPTION:    Employee interface from Lawson into Oracle HR
  
   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        24-Apr-2011 Kathy Poling     Initial Version copied from R11 modified
                                           for R12 and Shared HR
   3.0        30-Oct-2014 Kathy Poling     TMS 20140925-00212 Initial creation
                                           update_emp_position   
   4.0        28-Apr-2015 Maharajan        TMS#20150416-00179 ##No change in code## 
			              Shunmugam        **If HR interface skip updating the employee
                                           location,purchasing will update location manually 
										   only for SCM employees and that should remains the same***
   5.0        17-Aug-2017 Pattabhi avula    TMS#20170802-00051 - FW: Location Inactive -- Added 
                                            Inactive_date condition
                                                                 
  ******************************************************************************/
  /* Declare global variables  */

  -- Report variables
  g_rep_ln       VARCHAR2(150);
  g_rep_ln1      VARCHAR2(150);
  g_fill_spaces  VARCHAR2(1) := ' ';
  g_line_cnt     NUMBER := 0;
  g_file_name    VARCHAR2(100);
  g_file_dir     VARCHAR2(100);
  g_file_handle  utl_file.file_type;
  g_sid          VARCHAR2(8);
  g_err_msg      VARCHAR2(9000);
  g_err_code     NUMBER;
  g_distro_list  fnd_user.email_address%TYPE := 'hds.oracleglsupport@hdsupply.com'; --Version 1.2 'HDSFAST@hdsupply.com';
  g_email        fnd_user.email_address%TYPE;
  g_instance     VARCHAR2(100);
  g_sender       VARCHAR2(100);
  g_errorstatus  NUMBER;
  g_host         VARCHAR2(256) := 'mailoutrelay.hdsupply.net';
  g_hostport     VARCHAR2(20) := '25';
  g_errormessage VARCHAR2(9000);
  -- Error DEBUG
  l_err_callfrom  VARCHAR2(75) DEFAULT 'XXCUS_HR_INTERFACE_PKG';
  l_err_callpoint VARCHAR2(75) DEFAULT 'START';
  l_distro_list   VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';

  -- Create a procedure to accept string as a parameter and
  -- insert that into log file or output of current concurrent program.

  PROCEDURE debug(p_type VARCHAR2, p_string VARCHAR2) IS
  BEGIN
    IF p_type = 'L'
    THEN
      fnd_file.put_line(fnd_file.log, p_string);
    ELSIF p_type = 'O'
    THEN
      fnd_file.put_line(fnd_file.output, p_string);
    ELSIF p_type = 'LO'
    THEN
      fnd_file.put_line(fnd_file.log, p_string);
      fnd_file.put_line(fnd_file.output, p_string);
    END IF;
  
    dbms_output.put_line(p_string);
  END debug;

  /***********************************get location_id **********************************************/
  --   VERSION DATE          AUTHOR(S)       DESCRIPTION
  -- ------- -----------   --------------- -------------------------------------------------------
  -- 1.0                                   Creation
  -- 1.1     26-Jul-2013   Kathy Poling    SR 201993 Task ID: 20130415-00545 for White Cap need
  --                                       to get the actual location of employee.  If the corresponding 
  --                                       Location is not yet created then the HR Interface should skip 
  --                                       the automatic update on the White Cap employee record.
  -- 4.0     28-Apr-2015   Maharajan       TMS#20150416-00179 ##No change in code## 
  -- 		           Shunmugam           **If HR interface skip updating the employee location, 
  --					                   purchasing will update location manually only for SCM employees and 
  --					                   that should remains the same***
  -- 5.0     17-Aug-2017 Pattabhi avula    TMS#20170802-00051 - FW: Location Inactive -- Added 
  --                                       Inactive_date condition
  -- ---------------------------------------------------------------------------------------------

  FUNCTION get_location_id(p_fru VARCHAR2) RETURN NUMBER IS
  
    l_branch      VARCHAR2(6);
    l_bu          VARCHAR2(5);
    l_location_id NUMBER;
  
  BEGIN
  
    BEGIN
      SELECT lob_branch, business_unit
        INTO l_branch, l_bu
        FROM apps.xxcus_location_code_vw t
       WHERE t.fru = p_fru
         AND rownum < 2;
    EXCEPTION
      WHEN no_data_found THEN
        --l_location_id := 164; --Default location 'HDS - Legal Entity address USA'
        l_location_id := -1;
    END;
  
    IF l_bu = 'WC1US' --White Cap
    THEN
      BEGIN
        SELECT location_id
          INTO l_location_id
          FROM hr_locations
         WHERE location_code LIKE l_branch || '%'
           AND office_site_flag = 'Y'
		   AND (INACTIVE_DATE IS NULL OR INACTIVE_DATE>SYSDATE) -- Ver#5.0
           AND ROWNUM < 2;
      
        RETURN l_location_id;
      
      EXCEPTION
        WHEN no_data_found THEN
          --l_location_id := 164; --Default location 'HDS - Legal Entity address USA'
          l_location_id := -1;
      END;
    
    ELSE
      --l_location_id := 164; --Default location 'HDS - Legal Entity address USA'
      l_location_id := -1;
    END IF;
  
    /* -- Version 1.1
    CURSOR get_location_cur IS
      SELECT location_id FROM hr_locations WHERE location_code = p_name;
     
       get_location_rec get_location_cur%ROWTYPE;
     
     BEGIN
       OPEN get_location_cur;
       FETCH get_location_cur
         INTO get_location_rec;
       CLOSE get_location_cur;
       RETURN get_location_rec.location_id;
    */
    RETURN l_location_id;
  
  END get_location_id;

  -- **********************get org_id **************************

  FUNCTION get_org_id(p_name VARCHAR2) RETURN NUMBER IS
  
    CURSOR get_org_id_cur IS
      SELECT organization_id
        FROM hr_organization_units
       WHERE NAME = p_name;
  
    get_org_id_rec get_org_id_cur%ROWTYPE;
  
  BEGIN
    OPEN get_org_id_cur;
    FETCH get_org_id_cur
      INTO get_org_id_rec;
    CLOSE get_org_id_cur;
    RETURN get_org_id_rec.organization_id;
  
  END get_org_id;

  -- ******** Function to Get Set Of Books ID    *****************

  FUNCTION get_sob_id(p_name VARCHAR2) RETURN NUMBER IS
  
    CURSOR get_sob_id_cur IS
      SELECT set_of_books_id FROM gl_sets_of_books WHERE NAME = p_name;
  
    get_sob_id_rec    get_sob_id_cur%ROWTYPE;
    l_get_sob_id_code NUMBER;
  
  BEGIN
    OPEN get_sob_id_cur;
    FETCH get_sob_id_cur
      INTO get_sob_id_rec;
    CLOSE get_sob_id_cur;
    RETURN get_sob_id_rec.set_of_books_id;
  
  END get_sob_id;

  -- ******** Function to Get Chart of Accounts   *****************

  /*FUNCTION get_coa(p_name VARCHAR2) RETURN NUMBER IS
  
    CURSOR coa_cur IS
      SELECT chart_of_accounts_id
        FROM gl_sets_of_books
       WHERE NAME = 'HD Supply USD';
  
    coa_rec coa_cur%ROWTYPE;
  
  BEGIN
  
    OPEN coa_cur;
    FETCH coa_cur
      INTO coa_rec;
    IF coa_cur%NOTFOUND
    THEN
      CLOSE coa_cur;
      RETURN NULL;
    ELSE
      CLOSE coa_cur;
      RETURN coa_rec.chart_of_accounts_id;
    END IF;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN NULL;
      CLOSE coa_cur;
  END get_coa;*/

  -- ******** Function to Get segment2  *****************
  --   VERSION DATE          AUTHOR(S)       DESCRIPTION
  -- ------- -----------   --------------- -------------------------------------------------------
  -- 1.1     25-Mar-2014   Kathy Poling    Changed parm to ledger id from name 
  --  
  -- --------------------------------------------------------------------------------------------- 
  FUNCTION get_seg2(p_ccid VARCHAR2, p_ledger_id NUMBER) RETURN VARCHAR2 IS
  
    CURSOR seg2_cur IS
      SELECT segment2
        FROM gl_code_combinations
       WHERE code_combination_id = p_ccid
         AND chart_of_accounts_id =
             (SELECT chart_of_accounts_id
                FROM gl_sets_of_books
               WHERE set_of_books_id = p_ledger_id);
  
    seg2_rec seg2_cur%ROWTYPE;
  
  BEGIN
  
    OPEN seg2_cur;
    FETCH seg2_cur
      INTO seg2_rec;
    IF seg2_cur%NOTFOUND
    THEN
      CLOSE seg2_cur;
      RETURN NULL;
    ELSE
      CLOSE seg2_cur;
      RETURN seg2_rec.segment2;
    END IF;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN NULL;
      CLOSE seg2_cur;
  END get_seg2;

  -- ******** Function to Get CCID   *****************

  /*FUNCTION get_ccid(p_corp_id VARCHAR2, p_dept VARCHAR2) RETURN NUMBER IS
    l_seg1    VARCHAR2(25);
    l_seg2    VARCHAR2(25);
    l_seg3    VARCHAR2(25);
    v_ccid    NUMBER;
    cti_flag  VARCHAR2(2) DEFAULT NULL;
    cti_error BOOLEAN DEFAULT FALSE;
  
  BEGIN
  
    BEGIN
      SELECT entrp_entity, entrp_loc, entrp_cc
        INTO l_seg1, l_seg2, l_seg3
        FROM apps.xxcus_location_code_vw
       WHERE fru = p_corp_id
         AND rownum < 2;
    EXCEPTION
      WHEN no_data_found THEN
        v_ccid := 0;
    END;
  
    -- Check for CTI org
    -- Added FM to Department pull for cost center
    BEGIN
      SELECT 'Y'
        INTO cti_flag
        FROM dual
       WHERE l_seg1 IN
            \*(SELECT segment1
             FROM xxcus.xxcus_expnstmplt_branch_tbl
            WHERE template_name IN
                  ('CTI EXPENSE TEMPLATE', 'FM EXPENSE TEMPLATE')); *\
             (SELECT lookup_code
                FROM apps.fnd_lookup_values
               WHERE lookup_type = 'HDS_AP_EXPENSE_TEMPLATE'
                 AND enabled_flag = 'Y'
                 AND nvl(end_date_active, SYSDATE + 1) > SYSDATE);
    EXCEPTION
      WHEN no_data_found THEN
        cti_flag := 'N';
    END;
  
    IF cti_flag = 'Y'
    THEN
    
      l_seg1 := NULL;
      l_seg2 := NULL;
      l_seg3 := NULL;
    
      BEGIN
        SELECT DISTINCT entrp_entity, entrp_loc, entrp_cc
          INTO l_seg1, l_seg2, l_seg3
          FROM apps.xxcus_location_code_vw
         WHERE fru = p_corp_id
           AND lob_dept = p_dept;
      EXCEPTION
        WHEN OTHERS THEN
          cti_error := TRUE;
      END;
    
    END IF;
  
    -- Force ZZ branches to succeed
    IF upper(substr(p_corp_id, 1, 1)) = 'Z'
    THEN
      v_ccid := 13931;
    ELSE
    
      v_ccid := gl_code_combinations_pkg.get_ccid(50328
                                                 ,to_char(SYSDATE
                                                         ,'DD-MON-RR')
                                                 ,l_seg1 || '.' || l_seg2 || '.' ||
                                                  l_seg3 || '.' || '658020' || '.' ||
                                                  '00000' || '.' || '00000');
    
    END IF;
  
    RETURN v_ccid;
  
  EXCEPTION
    WHEN OTHERS THEN
      RETURN NULL;
  END get_ccid;*/

  -- ******** Function to Get Person Type Id   *****************

  /*FUNCTION get_person_type_id(p_name VARCHAR2) RETURN NUMBER IS
  
    CURSOR person_type_id_cur IS
      SELECT person_type_id
        FROM per_person_types
       WHERE user_person_type = p_name;
  
    person_type_id_rec person_type_id_cur%ROWTYPE;
  
  BEGIN
  
    OPEN person_type_id_cur;
    FETCH person_type_id_cur
      INTO person_type_id_rec;
    IF person_type_id_cur%NOTFOUND
    THEN
      CLOSE person_type_id_cur;
      RETURN NULL;
    ELSE
      CLOSE person_type_id_cur;
      RETURN person_type_id_rec.person_type_id;
    END IF;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN NULL;
      CLOSE person_type_id_cur;
  END get_person_type_id;*/

  -- ******** Function to Get Position_id   *****************

  /*FUNCTION get_position_id(p_name VARCHAR2) RETURN NUMBER IS
  
    CURSOR position_id_cur IS
      SELECT position_id FROM hr_all_positions_f WHERE NAME = p_name;
  
    position_id_rec position_id_cur%ROWTYPE;
  
  BEGIN
  
    OPEN position_id_cur;
    FETCH position_id_cur
      INTO position_id_rec;
    IF position_id_cur%NOTFOUND
    THEN
      CLOSE position_id_cur;
      RETURN NULL;
    ELSE
      CLOSE position_id_cur;
      RETURN position_id_rec.position_id;
    END IF;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN NULL;
      CLOSE position_id_cur;
  END get_position_id;*/

  -- ******** Function to Get Job id   *****************

  /*FUNCTION get_job_id(p_name VARCHAR2) RETURN NUMBER IS
  
    CURSOR job_id_cur IS
      SELECT job_id FROM per_jobs WHERE upper(NAME) = upper(p_name);
  
    job_id_rec job_id_cur%ROWTYPE;
  
  BEGIN
  
    OPEN job_id_cur;
    FETCH job_id_cur
      INTO job_id_rec;
    IF job_id_cur%NOTFOUND
    THEN
      CLOSE job_id_cur;
    
      RETURN NULL;
    
    ELSE
      CLOSE job_id_cur;
      RETURN job_id_rec.job_id;
    END IF;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN NULL;
      CLOSE job_id_cur;
  END get_job_id;*/

  -- ******************AP Supervisor Signing Limit ***********************
  -- Fixed bug that processed invalid code combs to process everytime.
  -- need to direct this to the web limits API (package)
  --
  --   VERSION DATE          AUTHOR(S)       DESCRIPTION
  -- ------- -----------   --------------- -------------------------------------------------------
  -- 1.1     11/13/2013    Luong Vu        Add code to get org_id for Canada OIE project
  --                       Kathy Poling    change function get_seg2 to pass ledger id 
  -- ---------------------------------------------------------------------------------------------
  PROCEDURE supervisor_signing_limit(p_org_name VARCHAR2) IS
    -- Updating and inserting the supervisor data in AP
  
    v_dual      NUMBER;
    l_msg       VARCHAR2(500);
    l_org_id    NUMBER;
    l_ledger_id NUMBER;
  
    CURSOR chk_supervisor_csr IS
      SELECT DISTINCT (supervisor_id) supervisor_id, default_code_comb_id
        FROM per_all_assignments_f a
       WHERE supervisor_id IS NOT NULL
         AND effective_end_date = '31-DEC-4712'
         AND default_code_comb_id IS NOT NULL -- workaround
         AND NOT EXISTS
       (SELECT 1
                FROM ap_web_signing_limits_all web
               WHERE web.employee_id = a.supervisor_id);
  
    CURSOR delete_supervisor_csr IS
      SELECT DISTINCT (employee_id) employee_id
        FROM ap_web_signing_limits_all web
       WHERE NOT EXISTS (SELECT 1
                FROM per_all_assignments_f a
               WHERE web.employee_id = a.supervisor_id);
  
    -- deleting the supervisor from AP signing limit that are no more supervisors
  BEGIN
  
    l_msg := 'Deleting The Supervisors From Ap_Web_Signing_Limits_All.';
    FOR delete_employee IN delete_supervisor_csr
    LOOP
      BEGIN
        DELETE FROM ap_web_signing_limits_all ap
         WHERE employee_id IN delete_employee.employee_id;
      
        IF SQL%NOTFOUND
        THEN
          l_msg := 'Unable to delete employee in ap_web_signing_limits_all: ' ||
                   delete_employee.employee_id;
          debug('O', l_msg);
        ELSE
          l_msg := 'Employee Deleted in ap_web_signing_limits_all: ' ||
                   delete_employee.employee_id;
          debug('O', l_msg);
        END IF;
      
      EXCEPTION
        WHEN OTHERS THEN
          debug('O', l_msg);
          l_msg := substr('Error in deleting supervisor: ' ||
                          delete_employee.employee_id || '-' || SQLERRM
                         ,1
                         ,500);
          debug('O', l_msg);
      END;
    
    END LOOP;
  
    l_msg := 'Supervisors From Ap_Web_Signing_Limits_All.';
  
    -- creating supervisor with signing limit that do not have AP Signing Limit
    FOR supervisor_rec IN chk_supervisor_csr
    LOOP
      BEGIN
        v_dual := NULL;
      
        --Version 1.1
        -- Get org id
        SELECT to_number(attribute1)
          INTO l_ledger_id
          FROM apps.fnd_flex_values_vl
         WHERE value_category = 'XXCUS_GL_PRODUCT'
           AND flex_value =
               (SELECT segment1
                  FROM gl.gl_code_combinations gcc
                 WHERE gcc.code_combination_id =
                       supervisor_rec.default_code_comb_id);
      
        CASE l_ledger_id
          WHEN 2061 THEN
            l_org_id := 163;
          WHEN 2063 THEN
            l_org_id := 167;
        END CASE;
      
        --Verifying If The Supervisor Exists In The Ap_Web_Signing_Limits_All Table.
        l_msg := 'Supervisor does not Exists In The Ap_Web_Signing_Limits_All Table; Hence Inserting.';
      
        INSERT INTO ap_web_signing_limits_all
        VALUES
          ('APEXP'
          ,supervisor_rec.supervisor_id
          ,get_seg2(supervisor_rec.default_code_comb_id, l_ledger_id) --Version 1.1 
          ,1000000
          ,SYSDATE
          ,g_user_id
          ,g_login_id
          ,SYSDATE
          ,g_user_id
           --,get_org_id(p_org_name)  --Version 1.1
          ,l_org_id);
      
        IF SQL%NOTFOUND
        THEN
          l_msg := 'Unable to insert employee in ap_web_signing_limits_all: ' ||
                   supervisor_rec.supervisor_id;
          debug('O', l_msg);
        ELSE
          l_msg := 'Employee Inserted in ap_web_signing_limits_all: ' ||
                   supervisor_rec.supervisor_id;
          debug('O', l_msg);
        END IF;
        -- end if;
      EXCEPTION
        WHEN OTHERS THEN
          debug('O', l_msg);
          l_msg := substr('Error in inserting supervisor- Spervisors Employee_id-: ' ||
                          supervisor_rec.supervisor_id || '-' || SQLERRM
                         ,1
                         ,500);
          debug('O', l_msg);
      END;
    END LOOP;
  EXCEPTION
    WHEN OTHERS THEN
      debug('L', l_msg);
      l_msg := substr('Procedure supervisor_signing_limit Final Exception: ' ||
                      SQLERRM
                     ,1
                     ,500);
      debug('L', l_msg);
  END;

  -- ************************* PROCEDURE***********************************************************
  --  This is a standard procedure which is typically run by a concurrent request HDS HR PEOPLESOFT MAIN PROCESS      
  --   VERSION DATE          AUTHOR(S)       DESCRIPTION
  -- ------- -----------   --------------- -------------------------------------------------------
  -- 1.1     26-Jul-2013   Kathy Poling    SR 133025 Changing removing the 90 days from term date
  --                                       adding attribute5 for cost center
  -- --------------------------------------------------------------------------------------------- 
  PROCEDURE emp_interface
  
    --(p_id IN NUMBER DEFAULT NULL)
  (x_errbuf     OUT VARCHAR2
  ,x_retcode    OUT NUMBER
  ,p_msg_id     IN VARCHAR2
  ,p_session_id IN VARCHAR2) IS
  
    -- This is the cursor to  report on error records
    CURSOR emp_cur IS
      SELECT ROWID, s.*
        FROM xxcus.xxcushr_interface_tbl s
       WHERE msg_id = p_msg_id
         AND session_id = p_session_id
         AND stg_status NOT IN ('P', 'NEW');
  
    -- cursor to call the API on all the new records
    CURSOR emp_load_cur IS
      SELECT ROWID, s.*
        FROM xxcus.xxcushr_interface_tbl s
       WHERE msg_id = p_msg_id
         AND session_id = p_session_id
         AND stg_status = 'NEW'
       ORDER BY s.processing_sequence DESC;
  
    CURSOR cur_person(p_employee_number VARCHAR2) IS
      SELECT person_id
            ,object_version_number
            ,effective_start_date
            ,employee_number
        FROM per_all_people_f
       WHERE employee_number = p_employee_number
         AND person_type_id IN
             (SELECT person_type_id
                FROM per_person_types
               WHERE user_person_type = 'Employee')
         AND effective_end_date = '31-DEC-4712';
  
    cur_person_rec cur_person%ROWTYPE;
  
    --Cursor find information for all Terminate records
    CURSOR cur_terminate(p_employee_number VARCHAR2) IS
      SELECT pos.object_version_number
            ,period_of_service_id
            ,p.effective_end_date
            ,p.effective_start_date
            ,p.person_type_id
        FROM per_people_f p, per_periods_of_service pos
       WHERE p.person_id = pos.person_id
         AND employee_number = p_employee_number
         AND person_type_id IN
             (SELECT person_type_id
                FROM per_person_types
               WHERE user_person_type IN ('Employee', 'Ex-employee'))
         AND p.effective_end_date = '31-DEC-4712';
    -- AND pos.actual_termination_date IS NULL; -- fix to fix re-fires.   --Version 1.2
  
    cur_terminate_rec cur_terminate%ROWTYPE;
  
    -- Cursor to Find information about final processing of terminate records
    CURSOR cur_final_process(p_employee_number VARCHAR2) IS
      SELECT pos.object_version_number
            ,period_of_service_id
            ,p.effective_start_date
        FROM per_people_f p, per_periods_of_service pos
       WHERE p.person_id = pos.person_id
         AND employee_number = p_employee_number
         AND person_type_id IN
             (SELECT person_type_id
                FROM per_person_types
               WHERE user_person_type IN ('Employee', 'Ex-employee'))
         AND p.effective_end_date = '31-DEC-4712';
    --AND pos.object_version_number = cur_terminate_rec.object_version_number; --Version 1.2  
  
    cur_final_process_rec cur_final_process%ROWTYPE;
    -- cursor to find information about rehire records
    /*  --Version 1.1 
       CURSOR cur_rehire(p_employee_number VARCHAR2) IS
         SELECT p.person_id
               ,p.object_version_number pov
               ,p.object_version_number ppx
               ,p.effective_start_date
           FROM per_people_f p
          WHERE p.employee_number = p_employee_number
            AND p.effective_end_date = '31-DEC-4712';
       cur_rehire_rec cur_rehire%ROWTYPE;
     
       --Cursor to find if she is an ex-employee
       CURSOR validate_term_date(p_employee_number VARCHAR2) IS
         SELECT effective_end_date
           FROM per_people_f
          WHERE employee_number = p_employee_number
            AND person_type_id IN
                (SELECT person_type_id
                   FROM per_person_types
                  WHERE user_person_type = 'Ex-employee');
    */
  
    --Version 1.1
    CURSOR cur_rehire(p_employee_number VARCHAR2) IS
      SELECT pos.rowid row_id, pos.*
        FROM per_periods_of_service pos
       WHERE pos.person_id =
             (SELECT per.person_id
                FROM apps.per_people_x per
               WHERE per.employee_number = p_employee_number) --'test2'
       ORDER BY nvl(actual_termination_date
                   ,fnd_date.canonical_to_date('4712/12/31')) DESC;
  
    cur_rehire_rec cur_rehire%ROWTYPE;
    ------
    -- Cursor to find Employee Assignments and  Supervisor_id
    CURSOR cur_supervisor(p_employee_number VARCHAR2) IS
      SELECT nvl(ppa.supervisor_id, '-99') supervisor_id
            ,ppa.assignment_id
            ,ppa.object_version_number
            ,ppe.employee_number
            ,'-99' supervisor_employee_number
        FROM per_people_f ppe, per_all_assignments_f ppa
       WHERE ppe.employee_number = p_employee_number
         AND ppa.person_id = ppe.person_id
         AND ppe.person_type_id IN
             (SELECT person_type_id
                FROM per_person_types
               WHERE user_person_type = 'Employee')
            --  AND pps.person_id(+) = ppa.supervisor_id
         AND ppe.effective_end_date = '31-DEC-4712'
      -- AND ppa.effective_end_date = '31-DEC-4712'   Version 1.2
      -- AND pps.effective_end_date(+) = '31-DEC-4712'
      ;
    cur_supervisor_rec cur_supervisor%ROWTYPE;
  
    -- Cursor to find the earliest possible date a supervisor can be a new emp's supervisor.
    -- if the date is greater than the new hire's date, then this will result in the mode
    -- of the update being changed from CORRECTION TO UPDATE as found in metalink as the appropriate
    -- solution for this particular problem --MR 3/23/07.
    CURSOR cur_supervisor_date(p_employee_number VARCHAR2) IS
      SELECT MIN(ppe.effective_start_date) min_date
        FROM per_people_f ppe
       WHERE ppe.employee_number = p_employee_number
         AND ppe.person_type_id IN
             (SELECT person_type_id
                FROM per_person_types
               WHERE user_person_type = 'Employee');
  
    cur_supervisor_date_rec cur_supervisor_date%ROWTYPE;
  
    --DECLARE
    l_error_code   VARCHAR2(10);
    l_error_mesg   VARCHAR2(250);
    v_count        NUMBER := 0;
    v_error_count  NUMBER := 0;
    v_load_count   NUMBER := 0;
    v_record_count NUMBER := 0;
    v_run          NUMBER := 1;
    l_suploop1     NUMBER := 0;
    l_suploop2     NUMBER := 0;
    l_retry        BOOLEAN := FALSE;
  
    --l_attribute_category         VARCHAR2(30) := '163'; --HD Supply Corp USD - Org  --Version 1.2      
    l_person_id                  NUMBER;
    l_assignment_id              NUMBER;
    l_per_object_version_number  NUMBER;
    l_asg_object_version_number  NUMBER;
    l_per_effective_start_date   DATE;
    l_effective_start_date       DATE;
    l_effective_end_date         DATE;
    l_per_effective_end_date     DATE;
    l_full_name                  VARCHAR2(240);
    l_per_comment_id             NUMBER;
    l_assignment_sequence        NUMBER;
    l_assignment_number          VARCHAR2(30);
    l_email_addy                 VARCHAR2(200) DEFAULT NULL;
    l_fnd_user_name              VARCHAR2(200) DEFAULT NULL;
    l_name_combination_warning   BOOLEAN;
    l_assign_payroll_warning     BOOLEAN;
    l_orig_hire_warning          BOOLEAN;
    l_attribute3                 VARCHAR2(150);
    v_process_action             VARCHAR2(1);
    l_employee_number            VARCHAR2(20);
    l_end_process_date           DATE;
    l_p_effective_date           DATE;
    l_final_date                 DATE;
    l_ps_object_v_number         NUMBER;
    l_period_of_service_id       NUMBER;
    l_date_track_update_mode     VARCHAR2(100);
    l_supervisor_warning         BOOLEAN := NULL;
    l_event_warning              BOOLEAN := NULL;
    l_interview_warning          BOOLEAN := NULL;
    l_review_warning             BOOLEAN := NULL;
    l_recruiter_warning          BOOLEAN := NULL;
    l_asg_future_changes_warning BOOLEAN := NULL;
    l_entries_changed_warning    VARCHAR2(100) := NULL;
    l_pay_proposal_warning       BOOLEAN := NULL;
    l_dod_warning                BOOLEAN := NULL;
    l_hire_date                  DATE := NULL;
    l_termination_date           DATE;
    l_skip                       VARCHAR2(1) := 'F';
    l_ex_emp_status              hr.per_person_types.person_type_id%TYPE;
    l_emp_status                 hr.per_person_types.person_type_id%TYPE;
    v_exists                     VARCHAR2(1);
    l_msg                        VARCHAR2(240);
    l_term_date                  DATE;
    v_term_date                  DATE;
    error_flag                   BOOLEAN := FALSE;
    data_error                   BOOLEAN := FALSE;
    v_num                        NUMBER;
    v_supervisor_id              NUMBER;
    l_group_name                 VARCHAR2(100);
    l_org_now_no_manager_warning BOOLEAN;
    l_spp_delete_warning         BOOLEAN;
  
    l_tax_district_changed_warning BOOLEAN;
    l_people_group_id              NUMBER;
    l_special_ceiling_step_id      NUMBER;
    lp_object_version_number       NUMBER;
    v_date_update_track            VARCHAR2(20);
  
    l_soft_coding_keyflex_id      NUMBER;
    l_comment_id                  NUMBER;
    l_cagr_grade_def_id           NUMBER;
    l_cagr_concatenated_segments  VARCHAR2(100);
    l_concatenated_segments       VARCHAR2(255);
    l_no_managers_warning         BOOLEAN;
    l_other_manager_warning       BOOLEAN;
    l_hourly_salaried_warning     BOOLEAN;
    l_gsp_post_process_warning    VARCHAR2(255);
    l_freq                        VARCHAR2(255);
    l_preferred_name              VARCHAR2(240); --Version 1.1
    l_user_desc                   VARCHAR2(240); --Version 1.1
    l_rehire_reason               VARCHAR2(20); --Version 1.1
    l_process                     BOOLEAN; --Version 1.1
    x_status                      VARCHAR2(64); --Version 1.1
    x_assignment_status_id        NUMBER; --Version 1.1
    x_s_final_process_date        DATE; --Version 1.1
    x_s_actual_termination_date   DATE; --Version 1.1
    x_c_assignment_status_type_id NUMBER; --Version 1.1
    x_requery_required            VARCHAR2(15); --Version 1.1
    l_user_guid                   RAW(16);
    l_ledger_id                   NUMBER; --v.2.0
    l_term_days                   NUMBER; --Version 1.1  4/28/14
    l_location_id                 NUMBER; --Version 1.2  4/28/14
    l_new_location                NUMBER; --Version 1.2  4/28/14
  
    --exceptions
    null_value_found  EXCEPTION;
    validation_error  EXCEPTION;
    empl_load_error   EXCEPTION;
    update_user_error EXCEPTION; --Version 1.2
  
  BEGIN
    -- Initializing the write email file and skip process.
    SELECT lower(NAME) INTO g_sid FROM v$database;
  
    SELECT pty.person_type_id
      INTO l_emp_status
      FROM hr.per_person_types pty
     WHERE pty.user_person_type = 'Employee';
  
    SELECT pty.person_type_id
      INTO l_ex_emp_status
      FROM hr.per_person_types pty
     WHERE pty.user_person_type = 'Ex-employee';
  
    --Version 1.1  4/28/14    
    BEGIN
      --lookup for the number of days to add on to the employees termination date   
      SELECT to_number(description)
        INTO l_term_days
        FROM fnd_lookup_values_vl
       WHERE lookup_type = 'HDS_HR_EMP_MISC'
         AND lookup_code = 'TERM'
         AND enabled_flag = 'Y'
         AND nvl(end_date_active, SYSDATE) >= SYSDATE;
    EXCEPTION
      WHEN no_data_found THEN
        l_msg := 'Failed to find termination days to add value : ' ||
                 SQLERRM;
        fnd_file.put_line(fnd_file.log, l_msg);
        RAISE program_error;
    END;
    --4/28/14     
  
    --  Supervisor bubble loop
    --  This will re-run the interface until the code no longer reports a decrease in errors.
    BEGIN
      LOOP
        l_suploop1 := l_suploop2;
      
        debug('L', 'Start of the program');
      
        -- sets all status's to new where not already processed successfully for this batch.
        UPDATE xxcus.xxcushr_interface_tbl s
           SET stg_status = 'NEW', stg_error_comments = NULL
         WHERE msg_id = p_msg_id
           AND session_id = p_session_id
           AND stg_status <> 'P';
      
        --Report_Header;
        debug('L', 'Record Number' || '|' || 'Error Comment');
      
        -- Sets status to 'NOT EXISTS' where updates or Terminates are not in oracle HR tables.
        UPDATE xxcus.xxcushr_interface_tbl s
           SET stg_status         = 'NOT EXIST'
              ,stg_error_comments = 'Employee Does Not Exist in Oracle. This stg_process_type = T or U Employee Should Exist In Oracle.'
         WHERE msg_id = p_msg_id
           AND session_id = p_session_id
           AND stg_process_flag IN ('U', 'T')
           AND stg_status = 'NEW'
           AND NOT EXISTS
         (SELECT 1
                  FROM per_all_people_f ppf
                 WHERE ppf.employee_number = s.stg_employee_number);
      
        --  Sets status to 'Exists' where New records already exist in oracle HR tables.
        UPDATE xxcus.xxcushr_interface_tbl s
           SET stg_status         = 'EXISTS'
              ,stg_error_comments = substr('This stg_process_type = N But Employee Already Exists in Oracle.'
                                          ,1
                                          ,150)
         WHERE msg_id = p_msg_id
           AND session_id = p_session_id
           AND stg_process_flag IN ('N')
           AND stg_status = 'NEW'
           AND EXISTS
         (SELECT 1
                  FROM per_all_people_f ppf
                 WHERE ppf.employee_number = s.stg_employee_number);
      
        -- Never seen this used
        UPDATE xxcus.xxcushr_interface_tbl s
           SET stg_status         = 'NVAL'
              ,stg_error_comments = substr(stg_error_comments ||
                                           ';One Or All Required Column is Null In This Record.'
                                          ,1
                                          ,150)
         WHERE msg_id = p_msg_id
           AND session_id = p_session_id
           AND (stg_employee_number IS NULL OR stg_last_name IS NULL OR
               stg_first_name IS NULL OR stg_hire_date IS NULL OR
               stg_sex IS NULL OR stg_process_flag IS NULL);
      
        debug('L', 'Before start of emp_load_cur LOOP ...');
      
        /****************************************************************************************/
        /****************************************************************************************/
        /**              START OF CREATE, UPDATE, REHIRE, TERMINATE PROCESS                    **/
        /****************************************************************************************/
        /****************************************************************************************/
        FOR empl_load IN emp_load_cur
        LOOP
          l_msg := NULL;
          SAVEPOINT create_employee;
        
          BEGIN
            --Initializing the Variables
            l_ps_object_v_number   := NULL;
            l_person_id            := NULL;
            l_period_of_service_id := NULL;
            l_p_effective_date     := NULL;
            --l_person_id                 := NULL;
            l_per_object_version_number := NULL;
            l_asg_object_version_number := NULL;
            l_assignment_id             := NULL;
            l_effective_start_date      := NULL;
            l_effective_end_date        := NULL;
            l_employee_number           := NULL;
            l_full_name                 := NULL;
            l_per_comment_id            := NULL;
            l_assignment_sequence       := NULL;
            l_name_combination_warning  := NULL;
            l_assign_payroll_warning    := NULL;
            l_orig_hire_warning         := NULL;
            v_supervisor_id             := NULL;
            l_soft_coding_keyflex_id    := NULL;
            l_comment_id                := NULL;
            l_concatenated_segments     := NULL;
            l_no_managers_warning       := NULL;
            l_other_manager_warning     := NULL;
            v_date_update_track         := 'CORRECTION'; --Version 1.1   := NULL;
            l_preferred_name            := NULL; --Version 1.1
            l_user_desc                 := NULL; --Version 1.1
            l_rehire_reason             := NULL; --Version 1.1
            l_location_id               := NULL; --Version 1.2
            l_new_location              := NULL; --Version 1.2
          
            /****************************************************************************************/
            /****************************************************************************************/
            /**                            NEW HIRE PROCESS                                        **/
            /****************************************************************************************/
            /****************************************************************************************/
            IF empl_load.stg_process_flag = 'N'
            THEN
            
              -- Fix Hire date issue
              IF to_date(empl_load.stg_hire_date, 'MMDDYYYY') >
                 to_date(empl_load.stg_effective_start_date, 'MMDDYYYY')
              THEN
                l_hire_date := to_date(empl_load.stg_effective_start_date
                                      ,'MMDDYYYY');
              ELSE
                l_hire_date := to_date(empl_load.stg_hire_date, 'MMDDYYYY');
              END IF;
            
              l_msg := 'Starting Create New Employee API - ' ||
                       empl_load.stg_employee_number || '-Trx_id-' ||
                       empl_load.stg_trx_id || ' ';
              debug('L', l_msg);
            
              BEGIN
                hr_employee_api.create_us_employee(p_hire_date         => l_hire_date
                                                  ,p_business_group_id => 0
                                                  ,p_last_name         => empl_load.stg_last_name
                                                  ,p_first_name        => empl_load.stg_first_name
                                                  ,p_sex               => empl_load.stg_sex
                                                  ,p_work_telephone    => empl_load.work_telephone
                                                  ,p_email_address     => empl_load.email_address
                                                  ,p_employee_number   => empl_load.stg_employee_number
                                                  ,p_middle_names      => empl_load.stg_middle_names
                                                   --,p_attribute_category        => l_attribute_category
                                                  ,p_attribute1 => empl_load.stg_attribute1
                                                   /*   ,p_attribute2                => to_char(to_date(empl_load.stg_attribute2
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        ,'MMDDYYYY')
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                ,'YYYY/MM/DD HH24:MI:SS')
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         ,p_attribute3                => empl_load.stg_attribute3
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         ,p_attribute4                => empl_load.nt_login    */
                                                  ,p_attribute5                => empl_load.stg_attribute9 --Version 1.1
                                                  ,p_person_id                 => l_person_id
                                                  ,p_assignment_id             => l_assignment_id
                                                  ,p_per_object_version_number => l_per_object_version_number
                                                  ,p_asg_object_version_number => l_asg_object_version_number
                                                  ,p_per_effective_start_date  => l_effective_start_date
                                                  ,p_per_effective_end_date    => l_effective_end_date
                                                  ,p_full_name                 => l_full_name
                                                  ,p_per_comment_id            => l_per_comment_id
                                                  ,p_assignment_sequence       => l_assignment_sequence
                                                  ,p_assignment_number         => l_assignment_number
                                                  ,p_name_combination_warning  => l_name_combination_warning
                                                  ,p_assign_payroll_warning    => l_assign_payroll_warning
                                                  ,p_orig_hire_warning         => l_orig_hire_warning
                                                  ,p_validate                  => FALSE);
              
                l_msg := ' - Creating employee num:' ||
                         empl_load.stg_employee_number;
                debug('L', empl_load.stg_trx_id || l_msg);
              
              EXCEPTION
                WHEN OTHERS THEN
                  l_msg := empl_load.stg_trx_id ||
                           ' - Error in Hr_Employee_Api.create_us_employee.  Error MSG : ' || '-' ||
                           SQLERRM;
                  debug('L', l_msg);
                  RAISE empl_load_error;
              END;
            
              /****************************************************************************************/
              /****************************************************************************************/
              /**                              UPDATE PROCESS                                        **/
              /****************************************************************************************/
              /****************************************************************************************/
            
            ELSIF empl_load.stg_process_flag = 'U'
            THEN
              l_msg := ' - For this TRX_ID, MSG ERROR : ';
            
              cur_person_rec := NULL;
            
              OPEN cur_person(empl_load.stg_employee_number);
              FETCH cur_person
                INTO cur_person_rec;
              IF cur_person%NOTFOUND
              THEN
              
                l_msg := ' - For this TRX_ID, Unable To Find The Employee - ' ||
                         to_char(empl_load.stg_employee_number) ||
                         ' - At Update Employee API.';
              
                debug('L', empl_load.stg_trx_id || l_msg);
              
                CLOSE cur_person;
                RAISE empl_load_error;
              ELSE
                CLOSE cur_person;
              END IF;
            
              IF cur_person%ISOPEN
              THEN
                CLOSE cur_person;
              END IF;
            
              BEGIN
              
                hr_person_api.update_us_person(p_validate              => FALSE
                                              ,p_effective_date        => to_date(empl_load.creation_date
                                                                                 ,'MMDDYYYY')
                                              ,p_datetrack_update_mode => 'CORRECTION' --'UPDATE'  can not use UPDATE on a HR shared install
                                              ,p_person_id             => cur_person_rec.person_id
                                              ,p_employee_number       => cur_person_rec.employee_number
                                              ,p_object_version_number => cur_person_rec.object_version_number
                                              ,p_first_name            => empl_load.stg_first_name
                                              ,p_middle_names          => empl_load.stg_middle_names
                                              ,p_last_name             => empl_load.stg_last_name
                                              ,p_sex                   => empl_load.stg_sex
                                              ,p_work_telephone        => empl_load.work_telephone
                                              ,p_email_address         => empl_load.email_address
                                              ,p_attribute1            => empl_load.stg_attribute1
                                               --,p_attribute4               => empl_load.nt_login
                                              ,p_attribute5               => empl_load.stg_attribute9 --Version 1.1
                                              ,p_effective_start_date     => l_effective_start_date
                                              ,p_effective_end_date       => l_effective_end_date
                                              ,p_full_name                => l_full_name
                                              ,p_comment_id               => l_per_comment_id
                                              ,p_name_combination_warning => l_name_combination_warning
                                              ,p_assign_payroll_warning   => l_assign_payroll_warning
                                              ,p_orig_hire_warning        => l_orig_hire_warning);
              
                l_msg := ' - Updating employee num: ' ||
                         empl_load.stg_employee_number;
                debug('L', empl_load.stg_trx_id || l_msg);
              
              EXCEPTION
                WHEN OTHERS THEN
                  l_msg := empl_load.stg_trx_id ||
                           ' - Error in  Hr_Person_Api.update_us_person. Error MSG : ' ||
                           SQLERRM;
                  debug('L', l_msg);
                  RAISE empl_load_error;
              END;
            
              -- Start of code to update FND_USER email address and description to preferred name 
              --info if needed.
              BEGIN
                -- DEBUG NAME
                l_err_callpoint := 'Updating Emails in FND_USER table';
                -- DEBUG NAME
              
                IF xxcus_create_apps_user_pkg.xxcus_check_user_exists(empl_load.stg_employee_number) = 'Y'
                THEN
                  BEGIN
                  
                    SELECT a.email_address
                          ,a.user_name
                          ,a.description
                          ,a.user_guid --Version 1.1  update user name                           
                      INTO l_email_addy
                          ,l_fnd_user_name
                          ,l_user_desc
                          ,l_user_guid
                      FROM apps.fnd_user a, apps.per_employees_x b
                     WHERE b.employee_num = empl_load.stg_employee_number
                       AND a.employee_id = b.employee_id;
                  EXCEPTION
                    WHEN OTHERS THEN
                      l_email_addy := ' ';
                  END;
                
                  --Version 1.1 need to verify if user description needs to be updated
                  IF nvl(empl_load.stg_middle_names, 'XXX') <> 'XXX'
                  THEN
                  
                    l_preferred_name := empl_load.stg_last_name || ', ' ||
                                        empl_load.stg_first_name || ' ' ||
                                        empl_load.stg_middle_names;
                  ELSE
                  
                    l_preferred_name := empl_load.stg_last_name || ', ' ||
                                        empl_load.stg_first_name;
                  END IF;
                  ---
                
                  IF nvl(l_email_addy, 'XXX') <>
                     nvl(empl_load.email_address, l_email_addy) OR
                     (l_user_desc IS NOT NULL AND
                      l_user_desc <> l_preferred_name)
                  THEN
                  
                    --Version 1.1  
                    IF l_user_guid IS NOT NULL
                    THEN
                      update_user_guid(l_fnd_user_name);
                    END IF;
                  
                    BEGIN
                    
                      l_msg := 'Updating user email or description for emp num:' ||
                               empl_load.stg_employee_number;
                      debug('L', empl_load.stg_trx_id || ' - ' || l_msg);
                    
                      fnd_user_pkg.updateuser(x_user_name     => l_fnd_user_name
                                             ,x_owner         => 'CUST'
                                             ,x_email_address => empl_load.email_address
                                             ,x_description   => l_preferred_name);
                    EXCEPTION
                      WHEN OTHERS THEN
                        l_msg := empl_load.stg_trx_id ||
                                 ' - Error in fnd_user_pkg.updateuser.  Error MSG : ' || '-' ||
                                 SQLERRM;
                        debug('L', l_msg);
                        RAISE update_user_error;
                    END;
                  
                  END IF;
                
                END IF;
                l_msg := ' - Email employee num:' ||
                         empl_load.stg_employee_number;
                debug('L', empl_load.stg_trx_id || l_msg);
              EXCEPTION
                WHEN OTHERS THEN
                  xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                                      ,p_calling           => l_err_callpoint
                                                      ,p_request_id        => fnd_global.conc_request_id
                                                      ,p_ora_error_msg     => SQLERRM
                                                      ,p_error_desc        => 'Updating FND email address user' ||
                                                                              l_fnd_user_name
                                                      ,p_distribution_list => l_distro_list
                                                      ,p_module            => 'HR');
              END;
            
              l_assignment_id             := NULL;
              l_asg_object_version_number := NULL;
            
              /****************************************************************************************/
              /****************************************************************************************/
              /**                              REHIRE PROCESS                                        **/
              /****************************************************************************************/
              /****************************************************************************************/
              /*            
                         ELSIF empl_load.stg_process_flag = 'R'
                         THEN
                           l_msg := 'Error At Re-hire Employee API - ' ||
                                    empl_load.stg_employee_number;
                         
                          --  Validating that this employee is an ex-employee
                           OPEN validate_term_date(empl_load.stg_employee_number);
                           FETCH validate_term_date
                             INTO v_term_date;
                           IF validate_term_date%FOUND
                           THEN
                             -- Found as an ex-employee
                             CLOSE validate_term_date;
                           
                             OPEN cur_rehire(empl_load.stg_employee_number);
                             FETCH cur_rehire
                               INTO cur_rehire_rec;
                             IF cur_rehire%NOTFOUND
                             THEN
                               l_msg := 'Unable To Find The Re-Hire Employee-' ||
                                        to_char(empl_load.stg_employee_number) ||
                                        '- At Re-hire Employee API.';
                               debug('L', empl_load.stg_trx_id || '|' || l_msg);
                               CLOSE cur_rehire;
                               RAISE empl_load_error;
                             ELSE
                               CLOSE cur_rehire;
                             END IF;
                           
                             --l_msg := 'Before API call re_hire_ex_employee: ' || empl_load.stg_employee_number; 
                           
                             BEGIN
                               hr_employee_api.re_hire_ex_employee(p_validate                  => FALSE
                                                                  ,p_hire_date                 => to_date(empl_load.creation_date
                                                                                                         ,'MMDDYYYY')
                                                                  ,p_person_id                 => cur_rehire_rec.person_id
                                                                  ,p_per_object_version_number => cur_rehire_rec.ppx
                                                                  ,p_rehire_reason             => l_rehire_reason  --'REHIRE'  3-12/14
                                                                  ,p_assignment_id             => l_assignment_id
                                                                  ,p_asg_object_version_number => l_per_object_version_number
                                                                  ,p_per_effective_start_date  => l_effective_start_date
                                                                  ,p_per_effective_end_date    => l_effective_end_date
                                                                  ,p_assignment_sequence       => l_assignment_sequence
                                                                  ,p_assignment_number         => l_assignment_number
                                                                  ,p_assign_payroll_warning    => l_assign_payroll_warning);
                             
                               dbms_output.put_line('rehire employee num:' ||
                                                    empl_load.stg_employee_number);
                             EXCEPTION
                               WHEN OTHERS THEN
                                 l_msg := empl_load.stg_trx_id ||
                                          ' - Error in  re_hire_ex_employee. Error MSG : ' || '-' ||
                                          SQLERRM;
                                 debug('L', l_msg);
                                 RAISE empl_load_error;
                             END;
                           
                           ELSE
                             --Although the process flag = 'R', she is a new employee in Oracle.
                             --Else for this : IF validate_term_date%FOUND THEN 
                           
                             IF validate_term_date%ISOPEN
                             THEN
                               CLOSE validate_term_date;
                             END IF;
                           
                             -- l_msg := 'Starting Create New Employee API with a REhire Flag - ' ||  empl_load.stg_employee_number;
                           
                             BEGIN
                               hr_employee_api.create_us_employee(p_hire_date         => to_date(empl_load.stg_hire_date
                                                                                                ,'MMDDYYYY')
                                                                 ,p_business_group_id => 0
                                                                 ,p_last_name         => empl_load.stg_last_name
                                                                 ,p_first_name        => empl_load.stg_first_name
                                                                 ,p_sex               => empl_load.stg_sex
                                                                 ,p_employee_number   => empl_load.stg_employee_number
                                                                 ,p_middle_names      => empl_load.stg_middle_names
                                                                 ,p_attribute1        => empl_load.stg_attribute1
                                                                  --   ,p_attribute2                => to_char(to_date(empl_load.stg_attribute2
                                                                  --                                                ,'DD-MON-YYYY')
                                                                  --                                                  ,'YYYY/MM/DD HH24:MI:SS')
                                                                  --                                                  ,p_attribute3                => empl_load.stg_attribute3
                                                                  --                                                    ,p_attribute4                => empl_load.nt_login     
                                                                 ,p_attribute5                => empl_load.stg_attribute9 --Version 1.1
                                                                 ,p_person_id                 => l_person_id
                                                                 ,p_assignment_id             => l_assignment_id
                                                                 ,p_per_object_version_number => l_per_object_version_number
                                                                 ,p_asg_object_version_number => l_asg_object_version_number
                                                                 ,p_per_effective_start_date  => l_effective_start_date
                                                                 ,p_per_effective_end_date    => l_effective_end_date
                                                                 ,p_full_name                 => l_full_name
                                                                 ,p_per_comment_id            => l_per_comment_id
                                                                 ,p_assignment_sequence       => l_assignment_sequence
                                                                 ,p_assignment_number         => l_assignment_number
                                                                 ,p_name_combination_warning  => l_name_combination_warning
                                                                 ,p_assign_payroll_warning    => l_assign_payroll_warning
                                                                 ,p_orig_hire_warning         => l_orig_hire_warning
                                                                 ,p_validate                  => FALSE);
                             
                             EXCEPTION
                               WHEN OTHERS THEN
                                 l_msg := empl_load.stg_trx_id ||
                                          ' - Error in Hr_Employee_Api.create_us_employee. Error MSG : ' || '-' ||
                                          SQLERRM;
                                 debug('L', l_msg);
                                 RAISE empl_load_error;
                             END;
                           
                           END IF;
              */
            
              --Version 1.1 3/12/14
            ELSIF empl_load.stg_process_flag = 'R'
            THEN
              l_msg := 'Error At Re-hire Employee API - ' ||
                       empl_load.stg_employee_number;
            
              /* Looking at the available APIs, the re-hire one (hr_employee_api.re_hire_ex_employee) 
              seemed to be the appropriate one. The API works, yet it creates date tracked information 
              which is not desirable on this installation as this is not the main HR system.  The 
              following is the code to reverse a termination for an employee in a shared HR installation..
              What this call will do is clear the termination date from the periods of service table, 
              eliminate the per_people record with person type 2 (ex-employee) and set the effective end 
              date to 4712/12/31 for the per_people record with person type 1*/
              BEGIN
                l_process := TRUE;
              
                OPEN cur_rehire(empl_load.stg_employee_number);
                FETCH cur_rehire
                  INTO cur_rehire_rec;
                IF cur_rehire%NOTFOUND
                THEN
                  l_msg := 'Unable To Find The Re-Hire Employee - ' ||
                           to_char(empl_load.stg_employee_number) ||
                           ' - At Re-hire Employee API.';
                  debug('L', empl_load.stg_trx_id || '|' || l_msg);
                  CLOSE cur_rehire;
                  RAISE empl_load_error;
                ELSE
                  CLOSE cur_rehire;
                END IF;
              
                IF cur_rehire_rec.period_of_service_id IS NULL
                THEN
                  l_process := FALSE;
                  l_msg     := 'Unable To Find The Re-Hire Employee - ' ||
                               to_char(empl_load.stg_employee_number) ||
                               '- At Re-hire Employee API.';
                  debug('L', empl_load.stg_trx_id || '|' || l_msg);
                ELSIF cur_rehire_rec.actual_termination_date IS NULL
                THEN
                  l_process := FALSE;
                
                  l_msg := 'Person is currenly active employee Rehire Employee - ' ||
                           to_char(empl_load.stg_employee_number) ||
                           '- At Re-hire Employee API.';
                  debug('L', empl_load.stg_trx_id || '|' || l_msg);
                END IF;
              
                IF l_process
                THEN
                
                  x_s_final_process_date      := cur_rehire_rec.final_process_date;
                  x_s_actual_termination_date := cur_rehire_rec.actual_termination_date;
                
                  BEGIN
                  
                    per_periods_of_service_pkg.populate_status(p_person_id            => cur_rehire_rec.person_id
                                                              ,p_status               => x_status
                                                              ,p_assignment_status_id => x_assignment_status_id);
                  
                    per_periods_of_service_pkg.update_term_row(p_row_id                      => cur_rehire_rec.row_id
                                                              ,p_period_of_service_id        => cur_rehire_rec.period_of_service_id
                                                              ,p_business_group_id           => cur_rehire_rec.business_group_id
                                                              ,p_person_id                   => cur_rehire_rec.person_id
                                                              ,p_date_start                  => cur_rehire_rec.date_start
                                                              ,p_termination_accepted_per_id => cur_rehire_rec.termination_accepted_person_id
                                                              ,p_accepted_termination_date   => cur_rehire_rec.accepted_termination_date
                                                              ,p_actual_termination_date     => cur_rehire_rec.actual_termination_date
                                                              ,p_comments                    => cur_rehire_rec.comments
                                                              ,p_final_process_date          => cur_rehire_rec.final_process_date
                                                              ,p_last_standard_process_date  => cur_rehire_rec.last_standard_process_date
                                                              ,p_leaving_reason              => cur_rehire_rec.leaving_reason
                                                              ,p_notified_termination_date   => cur_rehire_rec.notified_termination_date
                                                              ,p_projected_termination_date  => cur_rehire_rec.projected_termination_date
                                                              ,p_request_id                  => cur_rehire_rec.request_id
                                                              ,p_program_application_id      => cur_rehire_rec.program_application_id
                                                              ,p_program_id                  => cur_rehire_rec.program_id
                                                              ,p_program_update_date         => cur_rehire_rec.program_update_date
                                                              ,p_attribute_category          => cur_rehire_rec.attribute_category
                                                              ,p_attribute1                  => cur_rehire_rec.attribute1
                                                              ,p_attribute2                  => cur_rehire_rec.attribute2
                                                              ,p_attribute3                  => cur_rehire_rec.attribute3
                                                              ,p_attribute4                  => cur_rehire_rec.attribute4
                                                              ,p_attribute5                  => cur_rehire_rec.attribute5
                                                              ,p_attribute6                  => cur_rehire_rec.attribute6
                                                              ,p_attribute7                  => cur_rehire_rec.attribute7
                                                              ,p_attribute8                  => cur_rehire_rec.attribute8
                                                              ,p_attribute9                  => cur_rehire_rec.attribute9
                                                              ,p_attribute10                 => cur_rehire_rec.attribute10
                                                              ,p_attribute11                 => cur_rehire_rec.attribute11
                                                              ,p_attribute12                 => cur_rehire_rec.attribute12
                                                              ,p_attribute13                 => cur_rehire_rec.attribute13
                                                              ,p_attribute14                 => cur_rehire_rec.attribute14
                                                              ,p_attribute15                 => cur_rehire_rec.attribute15
                                                              ,p_attribute16                 => cur_rehire_rec.attribute16
                                                              ,p_attribute17                 => cur_rehire_rec.attribute17
                                                              ,p_attribute18                 => cur_rehire_rec.attribute18
                                                              ,p_attribute19                 => cur_rehire_rec.attribute19
                                                              ,p_attribute20                 => cur_rehire_rec.attribute20
                                                              ,p_initiate_cancellation       => 'Y'
                                                              ,p_s_final_process_date        => x_s_final_process_date
                                                              ,p_s_actual_termination_date   => x_s_actual_termination_date
                                                              ,p_c_assignment_status_type_id => x_c_assignment_status_type_id
                                                              ,p_d_status                    => x_status
                                                              ,p_requery_required            => x_requery_required
                                                              ,p_clear_details               => 'Y'
                                                              ,p_legislation_code            => 'US'
                                                              ,p_pds_information_category    => cur_rehire_rec.pds_information_category
                                                              ,p_pds_information1            => cur_rehire_rec.pds_information1
                                                              ,p_pds_information2            => cur_rehire_rec.pds_information2
                                                              ,p_pds_information3            => cur_rehire_rec.pds_information3
                                                              ,p_pds_information4            => cur_rehire_rec.pds_information4
                                                              ,p_pds_information5            => cur_rehire_rec.pds_information5
                                                              ,p_pds_information6            => cur_rehire_rec.pds_information6
                                                              ,p_pds_information7            => cur_rehire_rec.pds_information7
                                                              ,p_pds_information8            => cur_rehire_rec.pds_information8
                                                              ,p_pds_information9            => cur_rehire_rec.pds_information9
                                                              ,p_pds_information10           => cur_rehire_rec.pds_information10
                                                              ,p_pds_information11           => cur_rehire_rec.pds_information11
                                                              ,p_pds_information12           => cur_rehire_rec.pds_information12
                                                              ,p_pds_information13           => cur_rehire_rec.pds_information13
                                                              ,p_pds_information14           => cur_rehire_rec.pds_information14
                                                              ,p_pds_information15           => cur_rehire_rec.pds_information15
                                                              ,p_pds_information16           => cur_rehire_rec.pds_information16
                                                              ,p_pds_information17           => cur_rehire_rec.pds_information17
                                                              ,p_pds_information18           => cur_rehire_rec.pds_information18
                                                              ,p_pds_information19           => cur_rehire_rec.pds_information19
                                                              ,p_pds_information20           => cur_rehire_rec.pds_information20
                                                              ,p_pds_information21           => cur_rehire_rec.pds_information21
                                                              ,p_pds_information22           => cur_rehire_rec.pds_information22
                                                              ,p_pds_information23           => cur_rehire_rec.pds_information23
                                                              ,p_pds_information24           => cur_rehire_rec.pds_information24
                                                              ,p_pds_information25           => cur_rehire_rec.pds_information25
                                                              ,p_pds_information26           => cur_rehire_rec.pds_information26
                                                              ,p_pds_information27           => cur_rehire_rec.pds_information27
                                                              ,p_pds_information28           => cur_rehire_rec.pds_information28
                                                              ,p_pds_information29           => cur_rehire_rec.pds_information29
                                                              ,p_pds_information30           => cur_rehire_rec.pds_information30
                                                              ,p_adjusted_svc_date           => cur_rehire_rec.adjusted_svc_date);
                  
                    l_msg := ' - Rehire employee num:' ||
                             empl_load.stg_employee_number;
                    debug('L', empl_load.stg_trx_id || l_msg);
                  
                  EXCEPTION
                    WHEN OTHERS THEN
                      l_msg := empl_load.stg_trx_id ||
                               ' - Error in  re_hire_ex_employee. Error MSG : ' || '-' ||
                               SQLERRM;
                      debug('L', l_msg);
                      RAISE empl_load_error;
                  END;
                
                END IF;
              
              END;
            
              /****************************************************************************************/
              /****************************************************************************************/
              /**                              TERMINATE PROCESS                                     **/
              /****************************************************************************************/
              /****************************************************************************************/
            
            ELSIF empl_load.stg_process_flag = 'T'
            THEN
              debug('L'
                   ,'Start termination for employee: ' ||
                    empl_load.stg_employee_number);
            
              l_msg             := ' Error At Terminate Employee API - ' ||
                                   empl_load.stg_employee_number;
              cur_terminate_rec := NULL;
            
              OPEN cur_terminate(empl_load.stg_employee_number);
              FETCH cur_terminate
                INTO cur_terminate_rec;
              IF cur_terminate%NOTFOUND
              THEN
              
                l_msg := 'Unable To Find The Employee - ' ||
                         to_char(empl_load.stg_employee_number) ||
                         ' - At Terminate Employee API.';
              
                debug('L', empl_load.stg_trx_id || '|' || l_msg);
              
                CLOSE cur_terminate;
                RAISE empl_load_error;
              ELSIF cur_terminate_rec.person_type_id = l_ex_emp_status
              THEN
                l_skip := 'T';
                UPDATE xxcus.xxcushr_interface_tbl
                   SET stg_status         = 'P'
                      ,stg_error_comments = 'Person terminated already'
                 WHERE ROWID = empl_load.rowid;
                CLOSE cur_terminate;
              
              ELSE
                -- Version 1.1 Fix termination date issue
                --l_term_date := empl_load.stg_last_day_paid; 
                IF nvl(empl_load.stg_last_day_paid
                      ,to_date(empl_load.stg_attribute2, 'MMDDYYYY')) +
                   l_term_days <
                   to_date(empl_load.stg_effective_start_date, 'MMDDYYYY')
                
                THEN
                  l_term_date := to_date(empl_load.stg_effective_start_date
                                        ,'MMDDYYYY') + l_term_days;
                ELSE
                  l_term_date := nvl(empl_load.stg_last_day_paid
                                    ,to_date(empl_load.stg_attribute2
                                            ,'MMDDYYYY')) + l_term_days;
                END IF;
                -- 4/2/14
              
              END IF;
            
              IF cur_terminate%ISOPEN
              THEN
                CLOSE cur_terminate;
              END IF;
              l_msg := 'Before EE API call Terminate us person: ' ||
                       empl_load.stg_employee_number;
            
              --l_end_process_date := l_term_date + 90;   
            
              IF l_skip = 'F'
              THEN
                -- Skip if found but already termed.                
              
                BEGIN
                
                  hr_ex_employee_api.actual_termination_emp(p_validate                   => FALSE
                                                           ,p_effective_date             => l_term_date
                                                           ,p_period_of_service_id       => cur_terminate_rec.period_of_service_id
                                                           ,p_object_version_number      => cur_terminate_rec.object_version_number
                                                           ,p_actual_termination_date    => l_term_date
                                                           ,p_last_standard_process_date => l_term_date --l_end_process_date    3/12/14
                                                            /*    ,p_attribute2                 => to_char(to_date(empl_load.stg_attribute2
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       ,'MMDDYYYY')
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               ,'DD-MON-YYYY')   */
                                                           ,p_last_std_process_date_out  => l_end_process_date
                                                           ,p_supervisor_warning         => l_supervisor_warning
                                                           ,p_event_warning              => l_event_warning
                                                           ,p_interview_warning          => l_interview_warning
                                                           ,p_review_warning             => l_review_warning
                                                           ,p_recruiter_warning          => l_recruiter_warning
                                                           ,p_asg_future_changes_warning => l_asg_future_changes_warning
                                                           ,p_entries_changed_warning    => l_entries_changed_warning
                                                           ,p_pay_proposal_warning       => l_pay_proposal_warning
                                                           ,p_dod_warning                => l_dod_warning);
                
                  l_msg := ' - Terminate employee num:' ||
                           empl_load.stg_employee_number;
                  debug('L', empl_load.stg_trx_id || l_msg);
                
                EXCEPTION
                  WHEN OTHERS THEN
                    l_msg := empl_load.stg_trx_id ||
                             ' - Error in actual_termination_emp. Error MSG : ' ||
                             ' - ' || SQLERRM;
                    debug('L', l_msg);
                    RAISE empl_load_error;
                END;
                COMMIT;
              END IF; --Skip process if existing term
            
              cur_final_process_rec := NULL;
            
              l_msg := 'Cur_final_process(+)';
            
              OPEN cur_final_process(empl_load.stg_employee_number);
              FETCH cur_final_process
                INTO cur_final_process_rec;
              IF cur_final_process%NOTFOUND
              THEN
              
                l_msg := 'Unable To Find The Employee - ' ||
                         to_char(empl_load.stg_employee_number) ||
                         '- At final_process Employee API.';
              
                debug('L', empl_load.stg_trx_id || ' | ' || l_msg);
              
                CLOSE cur_final_process;
                RAISE empl_load_error;
              ELSE
                CLOSE cur_final_process;
              END IF;
            
              IF cur_final_process%ISOPEN
              THEN
                CLOSE cur_final_process;
              END IF;
              l_msg := 'Before EE API call final_process_emp : ' ||
                       empl_load.stg_employee_number;
            
              --l_end_process_date := l_term_date + 90; --Version 1.1
            
              IF l_skip = 'F'
              THEN
              
                -- Skip if found but already termed.
                BEGIN
                
                  hr_ex_employee_api.final_process_emp(p_validate                   => FALSE
                                                      ,p_period_of_service_id       => cur_final_process_rec.period_of_service_id
                                                      ,p_object_version_number      => cur_final_process_rec.object_version_number
                                                      ,p_final_process_date         => l_term_date --l_end_process_date  3/12/14
                                                      ,p_org_now_no_manager_warning => l_supervisor_warning
                                                      ,p_asg_future_changes_warning => l_asg_future_changes_warning
                                                      ,p_entries_changed_warning    => l_entries_changed_warning);
                
                  l_msg := ' - Final process employee num:' ||
                           empl_load.stg_employee_number;
                  debug('L', empl_load.stg_trx_id || l_msg);
                
                EXCEPTION
                  WHEN OTHERS THEN
                    l_msg := empl_load.stg_trx_id ||
                             ' - Error in final_process_emp. Error MSG : ' || '-' ||
                             SQLERRM;
                    debug('L', l_msg || SQLERRM);
                    RAISE empl_load_error;
                END;
              
              END IF; -- skip process
            
              l_skip := 'F'; --reset skip
              COMMIT; --kp 4/22
            END IF; -- end of main IF statement to process initial API's
          
            /****************************************************************************************/
            /****************************************************************************************/
            /**                   END OF CREATE, UPDATE, REHIRE, TERMINATE PROCESS                 **/
            /****************************************************************************************/
            /****************************************************************************************/
          
            /****************************************************************************************/
            /****************************************************************************************/
            /**                        START UPDATE ASSIGNMENTS PROCESS                           **/
            /****************************************************************************************/
            /****************************************************************************************/
          
            -- Creating the supervisor for the employee
            IF empl_load.stg_process_flag <> 'T'
            THEN
              -- not required for terminated empoyee
            
              v_count            := 0;
              cur_supervisor_rec := NULL;
              OPEN cur_supervisor(empl_load.stg_employee_number);
              LOOP
                FETCH cur_supervisor
                  INTO cur_supervisor_rec;
                EXIT WHEN cur_supervisor%NOTFOUND;
              END LOOP;
              v_count := cur_supervisor%ROWCOUNT;
            
              IF cur_supervisor%ISOPEN
              THEN
                CLOSE cur_supervisor;
              END IF;
              --     DEBUG('L','here5');
              IF v_count > 1
              THEN
                l_msg := 'Too Many Rows Found For Cursor cur_supervisor - Employee _number: ' ||
                         empl_load.stg_employee_number;
                debug('O', empl_load.stg_trx_id || '|' || l_msg);
                RAISE empl_load_error;
              END IF;
            
              /*   AAA   dup code ?  */
            
              -- code to find if supervisor was current for new employee.
              cur_supervisor_date_rec := NULL;
              OPEN cur_supervisor_date(empl_load.supervisor_id);
              FETCH cur_supervisor_date
                INTO cur_supervisor_date_rec;
              CLOSE cur_supervisor_date;
            
              IF nvl(empl_load.supervisor_id, -99) <> -99
              THEN
                --3/7/14 added if
                -- Need to examine further as it appears to do the same as the cursor up above.
                BEGIN
                  v_supervisor_id := NULL;
                  SELECT nvl(person_id, '-99')
                    INTO v_supervisor_id
                    FROM per_all_people_f
                   WHERE employee_number = to_char(empl_load.supervisor_id)
                     AND person_type_id IN
                         (SELECT person_type_id
                            FROM per_person_types
                           WHERE user_person_type = 'Employee')
                     AND effective_end_date = '31-DEC-4712';
                EXCEPTION
                  WHEN too_many_rows THEN
                    v_supervisor_id := NULL;
                    debug('L'
                         ,empl_load.stg_trx_id ||
                          'Too Many Rows in finding supervisor_id for this trx_id:');
                    RAISE empl_load_error;
                  WHEN OTHERS THEN
                    v_supervisor_id := NULL;
                    l_msg           := empl_load.stg_trx_id ||
                                       ' - For this TRX_ID, Unable To find THE supervisor_id. MSG ERROR : ' ||
                                       SQLERRM;
                    debug('L', l_msg);
                    RAISE empl_load_error;
                END;
              
              END IF; --3/7/14 added if
            
              -- v.2.0 
              -- Get ledger id, use by API  
              BEGIN
              
                l_ledger_id := NULL;
              
                SELECT to_number(attribute1)
                  INTO l_ledger_id
                  FROM apps.fnd_flex_values_vl
                 WHERE value_category = 'XXCUS_GL_PRODUCT'
                   AND flex_value =
                       (SELECT entrp_entity
                          FROM apps.xxcus_location_code_vw lc
                         WHERE lc.fru = empl_load.stg_attribute1);
              EXCEPTION
                WHEN too_many_rows THEN
                  v_supervisor_id := NULL;
                  debug('L'
                       ,empl_load.stg_trx_id ||
                        ' Too Many Rows in finding ledger ID for this trx_id - FRU =' ||
                        empl_load.stg_attribute1);
                  RAISE empl_load_error;
                WHEN OTHERS THEN
                  v_supervisor_id := NULL;
                  l_msg           := empl_load.stg_trx_id ||
                                     ' - For this TRX_ID, Unable To find ledger_id.' ||
                                     ' For FRU - ' ||
                                     empl_load.stg_attribute1 ||
                                     '  MSG ERROR : ' || SQLERRM;
                  debug('L', l_msg);
                  RAISE empl_load_error;
              END;
            
              /*     --Version 1.2 shared install can only do 'CORRECTION'        
                 IF empl_load.stg_process_flag = 'N'
                 THEN
                   -- Logic to validate the supervisor was in our system when do the update.  Mode
                   -- is changed to UPDATE in order to accomodate this scenario.
                   IF cur_supervisor_date_rec.min_date >=
                      to_date(empl_load.stg_hire_date, 'MMDDYYYY')
                   THEN
                     v_date_update_track := 'CORRECTION'; --'UPDATE';
                  
              
                   ELSE   
                    v_date_update_track := 'CORRECTION';
                     
                     --  Logic to determine if supervisor was previously termed during the span of the hire date.
                   
                     BEGIN
                       FOR c_sup_term_dt IN (SELECT effective_start_date
                                                   ,effective_end_date
                                               FROM hr.per_all_people_f
                                              WHERE employee_number =
                                                    to_char(empl_load.supervisor_id)
                                                AND person_type_id = 9)
                       LOOP
                         IF to_date(empl_load.stg_hire_date, 'MMDDYYYY') >=
                            c_sup_term_dt.effective_start_date AND
                            to_date(empl_load.stg_hire_date, 'MMDDYYYY') <=
                            c_sup_term_dt.effective_end_date
                         THEN
                           v_date_update_track := 'CORRECTION'; --'UPDATE';
                           EXIT;
                         ELSE
                           v_date_update_track := 'CORRECTION';
                         END IF;
                       END LOOP;
                     
                     EXCEPTION
                       WHEN OTHERS THEN
                         v_date_update_track := 'CORRECTION';
                     END;
                    
                   
                   END IF;
                 ELSIF empl_load.stg_process_flag = 'U'
                 THEN
                   v_date_update_track := 'CORRECTION'; ---'UPDATE';
                 END IF;
               
                 IF empl_load.stg_process_flag = 'R'
                 THEN
                   v_date_update_track := 'CORRECTION';
                 END IF;
              */ --03/11/2014      
            
              l_per_object_version_number := nvl(l_asg_object_version_number
                                                ,cur_supervisor_rec.object_version_number);
            
              --        If statement to check if FRU/Cost Center will result in error.
              --        If this does process, leave and do not process any other API's for this employee. Rollback
              IF xxcus_misc_pkg.get_ccid_create(lpad(empl_load.stg_attribute1
                                                    ,4
                                                    ,'0')
                                               ,empl_load.stg_attribute9) = -1
              THEN
                l_msg := 'default code combo is invalid, please check value in stg_attribute 1 and 9 : ' ||
                         empl_load.stg_attribute1 || '  ' ||
                         empl_load.stg_attribute9;
                debug('L', l_msg);
              
                BEGIN
                  --       raise empl_load_error;
                  --       Call the API for assignment but don't pass in the ccid
                  hr_assignment_api.update_us_emp_asg(p_validate              => FALSE
                                                     ,p_effective_date        => to_date(empl_load.creation_date
                                                                                        ,'MMDDYYYY')
                                                     , --(empl_load.stg_effective_start_date,'MMDDYYYY'),
                                                      p_datetrack_update_mode => v_date_update_track
                                                     ,p_assignment_id         => nvl(l_assignment_id
                                                                                    ,cur_supervisor_rec.assignment_id)
                                                     ,p_object_version_number => l_per_object_version_number
                                                     ,p_supervisor_id         => v_supervisor_id
                                                     ,
                                                      -- CCID cut out for v2.4
                                                      p_job_post_source_name => 'Approver'
                                                      --,p_set_of_books_id          => get_sob_id('HD Supply USD') --v2.0
                                                     ,p_set_of_books_id            => l_ledger_id --v2.0
                                                     ,p_cagr_grade_def_id          => l_cagr_grade_def_id
                                                     ,p_cagr_concatenated_segments => l_cagr_concatenated_segments
                                                     ,p_concatenated_segments      => l_concatenated_segments
                                                     ,p_soft_coding_keyflex_id     => l_soft_coding_keyflex_id
                                                     ,p_comment_id                 => l_comment_id
                                                     ,p_effective_start_date       => l_effective_start_date
                                                     ,p_effective_end_date         => l_effective_end_date
                                                     ,p_no_managers_warning        => l_no_managers_warning
                                                     ,p_other_manager_warning      => l_other_manager_warning
                                                     ,p_hourly_salaried_warning    => l_hourly_salaried_warning --4/25 KP
                                                     ,p_gsp_post_process_warning   => l_gsp_post_process_warning); --4/25 KP);
                
                  -- 2.4  removed the error when an invalid code combination found.
                  -- DEBUG NAME
                  l_err_callpoint := 'Retrieving code combination. ';
                  -- DEBUG NAME
                  l_msg := ' - Update no valid ccid employee num:' ||
                           empl_load.stg_employee_number;
                  debug('L', empl_load.stg_trx_id || l_msg);
                
                  xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                                      ,p_calling           => l_err_callpoint
                                                      ,p_request_id        => fnd_global.conc_request_id
                                                      ,p_ora_error_msg     => 'Emp# ' ||
                                                                              empl_load.stg_employee_number
                                                      ,p_error_desc        => 'For Trx_id- ' ||
                                                                              empl_load.stg_trx_id || '. ' ||
                                                                              'default code combo is invalid, please check value in stg_attribute 1 and 9 : ' ||
                                                                              empl_load.stg_attribute1 || '  ' ||
                                                                              empl_load.stg_attribute9
                                                      ,p_distribution_list => g_distro_list
                                                      ,p_module            => 'HR'
                                                      ,p_argument1         => 'Last Name ' ||
                                                                              empl_load.stg_last_name
                                                      ,p_argument2         => 'First Name ' ||
                                                                              empl_load.stg_first_name
                                                      ,p_argument3         => 'Employee # ' ||
                                                                              empl_load.stg_employee_number
                                                      ,p_argument4         => 'Branch : ' ||
                                                                              empl_load.stg_attribute1);
                EXCEPTION
                  WHEN OTHERS THEN
                    --l_msg := '_____________________________________________________________________________________________';
                    --debug('L', l_msg);
                    l_msg := '1-During API Call to Update Supervisor flag for employee - ' ||
                             empl_load.stg_employee_number;
                    debug('L', l_msg);
                    l_msg := '1-Trx_id-' || empl_load.stg_trx_id;
                    debug('L', l_msg);
                    l_msg := '1-Effective date: ' ||
                             to_date(empl_load.stg_effective_start_date
                                    ,'MMDDYYYY');
                    debug('L', l_msg);
                    l_msg := '1-datetrack mode : ' || v_date_update_track;
                    debug('L', l_msg);
                    l_msg := '1-assignment ID: ' ||
                             nvl(l_assignment_id
                                ,cur_supervisor_rec.assignment_id) ||
                             '-object version: ' ||
                             l_per_object_version_number;
                    debug('L', l_msg);
                    l_msg := '1-supervisor ID: ' || v_supervisor_id;
                    debug('L', l_msg);
                    /* l_msg := 'default code combo: ' ||
                             xxcus_misc_pkg.get_ccid_create(lpad(empl_load.stg_attribute1
                                                                ,4
                                                                ,'0')
                                                           ,empl_load.stg_attribute9);
                    */
                    debug('L', l_msg);
                    l_msg := '1-default code combo for FRU: ' ||
                             empl_load.stg_attribute1 || ' cost center: ' ||
                             empl_load.stg_attribute9;
                    debug('L', l_msg);
                    l_msg := '1-SOB ID: ' || get_sob_id('HD Supply USD');
                    debug('L', l_msg);
                    l_msg := '1-hire date : ' || empl_load.stg_hire_date;
                    debug('L', l_msg); -- debug code, use only if neccessary
                    l_msg := substr('-20007- 1 Error in update_emp_asg - stg_trx_id:' ||
                                    empl_load.stg_trx_id || '-' || SQLERRM
                                   ,1
                                   ,240);
                    debug('L'
                         ,'1-Error in Hr_Assignment_Api.update_emp_asg:' ||
                          SQLERRM);
                    -- l_msg := '_____________________________________________________________________________________________';
                    -- debug('L', l_msg);
                    RAISE empl_load_error;
                END;
              
              ELSE
              
                -- ADD CODE TO CHECK if Update for employee is on a date before the emp was created.
                -- CHANGE THE update mode to correction and update it on the last effective start date
              
                BEGIN
                  -- This is the API to update the assignment with info from the staging table.
                
                  hr_assignment_api.update_us_emp_asg(p_validate              => FALSE
                                                     ,p_effective_date        => to_date(empl_load.creation_date
                                                                                        ,'MMDDYYYY')
                                                     , --TO_DATE(empl_load.stg_effective_start_date,'MMDDYYYY'),
                                                      p_datetrack_update_mode => v_date_update_track
                                                     ,p_assignment_id         => nvl(l_assignment_id
                                                                                    ,cur_supervisor_rec.assignment_id)
                                                     ,p_object_version_number => l_per_object_version_number
                                                     ,p_supervisor_id         => v_supervisor_id
                                                     ,p_default_code_comb_id  => xxcus_misc_pkg.get_ccid_create(lpad(empl_load.stg_attribute1
                                                                                                                    ,4
                                                                                                                    ,'0')
                                                                                                               ,empl_load.stg_attribute9)
                                                     ,p_job_post_source_name  => 'Approver'
                                                      --p_set_of_books_id => get_sob_id('HD Supply USD'), v2.0
                                                     ,p_set_of_books_id            => l_ledger_id --v2.0
                                                     ,p_cagr_grade_def_id          => l_cagr_grade_def_id
                                                     ,p_cagr_concatenated_segments => l_cagr_concatenated_segments
                                                     ,p_concatenated_segments      => l_concatenated_segments
                                                     ,p_soft_coding_keyflex_id     => l_soft_coding_keyflex_id
                                                     ,p_comment_id                 => l_comment_id
                                                     ,p_effective_start_date       => l_effective_start_date
                                                     ,p_effective_end_date         => l_effective_end_date
                                                     ,p_no_managers_warning        => l_no_managers_warning
                                                     ,p_other_manager_warning      => l_other_manager_warning
                                                     ,p_hourly_salaried_warning    => l_hourly_salaried_warning --4/25 KP
                                                     ,p_gsp_post_process_warning   => l_gsp_post_process_warning); --4/25 KP
                
                  l_msg := ' - Update assignments employee num:' ||
                           empl_load.stg_employee_number;
                  debug('L', empl_load.stg_trx_id || l_msg);
                
                EXCEPTION
                  WHEN OTHERS THEN
                    --l_msg := '_____________________________________________________________________________________________';
                    --debug('L', l_msg);
                    l_msg := 'During API Call to Update Supervisor flag for employee - ' ||
                             empl_load.stg_employee_number;
                    debug('L', l_msg);
                    l_msg := 'Trx_id-' || empl_load.stg_trx_id;
                    debug('L', l_msg);
                    l_msg := 'Effective date: ' ||
                             to_date(empl_load.stg_effective_start_date
                                    ,'MMDDYYYY');
                    debug('L', l_msg);
                    l_msg := 'datetrack mode : ' || v_date_update_track;
                    debug('L', l_msg);
                    l_msg := 'assignment ID: ' ||
                             nvl(l_assignment_id
                                ,cur_supervisor_rec.assignment_id) ||
                             '-object version: ' ||
                             l_per_object_version_number;
                    debug('L', l_msg);
                    l_msg := 'supervisor ID: ' || v_supervisor_id;
                    debug('L', l_msg);
                    /* l_msg := 'default code combo: ' ||
                             xxcus_misc_pkg.get_ccid_create(lpad(empl_load.stg_attribute1
                                                                ,4
                                                                ,'0')
                                                           ,empl_load.stg_attribute9);
                    */
                    debug('L', l_msg);
                    l_msg := 'default code combo for FRU: ' ||
                             empl_load.stg_attribute1 || ' cost center: ' ||
                             empl_load.stg_attribute9;
                    debug('L', l_msg);
                    l_msg := 'SOB ID: ' || get_sob_id('HD Supply USD');
                    debug('L', l_msg);
                    l_msg := 'hire date : ' || empl_load.stg_hire_date;
                    debug('L', l_msg); -- debug code, use only if neccessary
                    l_msg := substr('-20007-Error in update_emp_asg - stg_trx_id:' ||
                                    empl_load.stg_trx_id || '-' || SQLERRM
                                   ,1
                                   ,240);
                    debug('L'
                         ,'Error in Hr_Assignment_Api.update_emp_asg:' ||
                          SQLERRM);
                    -- l_msg := '_____________________________________________________________________________________________';
                    -- debug('L', l_msg);
                    RAISE empl_load_error;
                END;
              
              END IF; --   KP 4/30/14 
              /*             --   KP 4/25   
              BEGIN
                
                  -- Get assignment ID ? and object version number.
                  -- i can understand the OBJ (need the latest), but not the assignment ID
                  -- since it can come from thhe previous API.
                  BEGIN
                    SELECT ppa.assignment_id, ppa.object_version_number
                      INTO l_assignment_id, lp_object_version_number
                      FROM per_people_f ppe, per_all_assignments_f ppa
                     WHERE ppe.employee_number = empl_load.stg_employee_number
                       AND ppa.person_id = ppe.person_id
                       AND ppe.person_type_id IN
                           (SELECT person_type_id
                              FROM per_person_types
                             WHERE user_person_type = 'Employee')
                       AND ppe.effective_end_date = '31-DEC-4712'
                       AND ppa.effective_end_date = '31-DEC-4712';
                  EXCEPTION
                    WHEN OTHERS THEN
                      l_msg := empl_load.stg_trx_id ||
                               ' - For this TRX_ID, Unable to find the assignment for the position.';
                      debug('O', l_msg);
                      RAISE empl_load_error;
                  END;
                
                  IF empl_load.job_class IS NULL
                  THEN
                    l_msg := empl_load.stg_trx_id ||
                             ' - For this TRX_ID, The job Class For This Transaction Is Null.';
                    debug('O', l_msg);
                  END IF;
                
                  hr_assignment_api.update_emp_asg_criteria(p_effective_date               => l_effective_start_date + 1
                                                           ,p_datetrack_update_mode        => v_date_update_track --'UPDATE' --'CORRECTION'
                                                           ,p_assignment_id                => l_assignment_id
                                                           ,p_validate                     => FALSE
                                                           ,p_called_from_mass_update      => TRUE
                                                           ,p_grade_id                     => NULL
                                                           ,p_position_id                  => NULL
                                                           ,p_job_id                       => get_job_id(empl_load.job_class)
                                                           ,p_payroll_id                   => NULL
                                                           ,p_location_id                  => get_location_id('HDS - Legal Entity address USA')
                                                           ,p_organization_id              => 0
                                                           ,p_people_group_id              => l_people_group_id
                                                           ,p_object_version_number        => lp_object_version_number
                                                           ,p_special_ceiling_step_id      => l_special_ceiling_step_id
                                                           ,p_group_name                   => l_group_name
                                                           ,p_effective_start_date         => l_effective_start_date
                                                           ,p_effective_end_date           => l_effective_end_date
                                                           ,p_org_now_no_manager_warning   => l_org_now_no_manager_warning
                                                           ,p_other_manager_warning        => l_other_manager_warning
                                                           ,p_spp_delete_warning           => l_spp_delete_warning
                                                           ,p_entries_changed_warning      => l_entries_changed_warning
                                                           ,p_tax_district_changed_warning => l_tax_district_changed_warning);
                
                EXCEPTION
                  WHEN OTHERS THEN
                    l_msg := empl_load.stg_trx_id ||
                             ' - 20007-Error in update_emp_asg_criteria. Error MSG : ' || '-' ||
                             SQLERRM;
                    debug('O'
                         ,'Error in Hr_Assignment_Api.update_emp_asg_criteria:' ||
                          SQLERRM);
                    RAISE empl_load_error;
                END;
              */ --KP 4/25 
              COMMIT;
            
              IF (l_no_managers_warning)
              THEN
                debug('L'
                     ,'No Manager Warning - ' ||
                      to_char(cur_person_rec.employee_number));
              END IF;
            
              IF (l_other_manager_warning)
              THEN
                debug('L'
                     ,' l_other_manager_warning- ' ||
                      to_char(cur_person_rec.employee_number));
              END IF;
            
            END IF;
          
            /****************************************************************************************/
            /****************************************************************************************/
            /**                              END UPDATE NEW EMPLOYEE PROCESS                       **/
            /****************************************************************************************/
            /****************************************************************************************/
          
            --update the flag for successfully loaded records
            UPDATE xxcus.xxcushr_interface_tbl
               SET stg_status = 'P'
             WHERE ROWID = empl_load.rowid;
          
            /****************************************************************************************/
            /****************************************************************************************/
            /**                              EMPL_LOAD_ERROR EXCEPTION                             **/
            /****************************************************************************************/
            /****************************************************************************************/
          EXCEPTION
            WHEN empl_load_error THEN
              ROLLBACK TO SAVEPOINT create_employee;
              error_flag := TRUE;
            
              UPDATE xxcus.xxcushr_interface_tbl
                 SET stg_status         = 'E'
                    ,stg_error_comments = substr(stg_error_comments || '-' ||
                                                 l_msg
                                                ,1
                                                ,150)
               WHERE ROWID = empl_load.rowid;
            
            --Version 1.2  03/10/2014 added ccid and update user exception             
            WHEN update_user_error THEN
              ROLLBACK TO SAVEPOINT create_employee;
              error_flag := TRUE;
            
              UPDATE xxcus.xxcushr_interface_tbl
                 SET stg_status         = 'E'
                    ,stg_error_comments = substr(stg_error_comments || '-' ||
                                                 l_msg
                                                ,1
                                                ,150)
               WHERE ROWID = empl_load.rowid;
              --03/10/2014  
          
            -- main drop out for main block.
            WHEN OTHERS THEN
              ROLLBACK TO SAVEPOINT create_employee;
              error_flag := TRUE;
            
              debug('L', empl_load.stg_trx_id || l_msg || SQLERRM);
            
              UPDATE xxcus.xxcushr_interface_tbl
                 SET stg_status         = 'E'
                    ,stg_error_comments = substr(stg_error_comments || '-' ||
                                                 l_msg
                                                ,1
                                                ,150)
               WHERE ROWID = empl_load.rowid;
            
          END;
          COMMIT;
        END LOOP; -- end of emp_load cursor loop
      
        BEGIN
          SELECT COUNT(*)
            INTO l_suploop2
            FROM xxcus.xxcushr_interface_tbl
           WHERE msg_id = p_msg_id
             AND stg_process_flag = 'N'
             AND stg_status = 'E';
        EXCEPTION
          WHEN no_data_found THEN
            l_suploop2 := l_suploop1;
        END;
      
        EXIT WHEN l_suploop2 = l_suploop1;
      END LOOP; -- supervisor loop
    END;
  
    /****************************************************************************************/
    /****************************************************************************************/
    /**                              REPORT PROCESS                                        **/
    /****************************************************************************************/
    /****************************************************************************************/
  
    report_header;
    FOR emp_rec IN emp_cur
    LOOP
      --initializing the variables
      v_exists := NULL;
      l_msg    := NULL;
    
      v_record_count := v_record_count + 1;
    
      BEGIN
        NULL;
        IF emp_rec.stg_error_comments <>
           'Employee Does Not Exist in Oracle. This stg_process_type = T or U Employee Should Exist In Oracle.'
        THEN
          report_body(emp_rec.stg_trx_id, emp_rec.stg_error_comments);
        END IF;
      EXCEPTION
        WHEN OTHERS THEN
          l_msg := l_msg || ': ' || SQLERRM;
        
          xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                              ,p_calling           => l_err_callpoint
                                              ,p_request_id        => fnd_global.conc_request_id
                                              ,p_ora_error_msg     => SQLERRM
                                              ,p_error_desc        => l_msg
                                              ,p_distribution_list => l_distro_list
                                              ,p_module            => 'HR');
        
          UPDATE xxcus.xxcushr_interface_tbl
             SET stg_status         = 'E'
                ,stg_error_comments = substr(stg_error_comments || '-' ||
                                             l_msg
                                            ,1
                                            ,150)
           WHERE ROWID = emp_rec.rowid;
          debug('L', emp_rec.stg_trx_id || '|' || SQLERRM);
        
      END;
    
    END LOOP; -- end of emp_Rec loop
    IF error_flag
    THEN
    
      l_msg := 'There Are Errors In The Employee Records. Please Review And Correct The Errors Before Proceeding.';
      debug('O', l_msg);
      logoutput_footer(1);
    ELSE
      logoutput_footer(0);
    
    END IF;
  
    -- Calling the Procedure at add or remove supervisors from the AP Signing Limit.
  
    supervisor_signing_limit('HD Supply Corp USD - Org');
  
  EXCEPTION
    WHEN validation_error THEN
      debug('L'
           ,'There Are Error Records In The Data File. Please Review The Errors.');
      debug('L', 'Please Correct The Errors And Restart The Program.');
      l_msg := 'There Are Errors In The Records. Please Review And Correct The Errors Before Proceeding.';
      IF v_num <> 0
      THEN
        l_msg := 'Unable To Send Email.';
        debug('L', l_msg);
      END IF;
    
    WHEN OTHERS THEN
      l_error_code := to_char(SQLCODE);
      debug('L', l_msg);
      l_msg := 'The Employee Interface With The above Req ID has been submitted.  The Program Stopped For Unknown Reason.  
                      Please Review The Log File.';
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => ' Error_Stack...' ||
                                                                  dbms_utility.format_error_stack() ||
                                                                  ' Error_Backtrace...' ||
                                                                  dbms_utility.format_error_backtrace()
                                          ,p_error_desc        => l_msg
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'HR');
      debug('L', substr(l_msg, 1, 150));
    
      IF v_num <> 0
      THEN
        l_msg := 'Unable To Send Email.';
        debug('L', l_msg);
      END IF;
      debug('L', 'Error in final others: ' || substr(SQLERRM, 1, 150));
      debug('L', 'Error code ' || l_error_code);
    
  END emp_interface;

  /************************* PROCEDURE***********************************************************/
  /*  This is an overloaded procedure to allow our support teams to run a batch for a single trx_id */
  /*  Created 1/3/08 */

  /*PROCEDURE emp_interface
  
    --(p_id IN NUMBER DEFAULT NULL)
  (x_errbuf     OUT VARCHAR2
  ,x_retcode    OUT NUMBER
  ,p_msg_id     IN VARCHAR2
  ,p_session_id IN VARCHAR2
  ,p_trx_id     IN VARCHAR2) IS
  
    -- This is the cursor to  report on error records
    CURSOR emp_cur IS
      SELECT ROWID, s.*
        FROM xxcus.xxcushr_interface_tbl s
       WHERE msg_id = p_msg_id
         AND session_id = p_session_id
         AND stg_trx_id = p_trx_id
         AND stg_status NOT IN ('P', 'NEW');
  
    -- cursor to call the API on all the new records
    CURSOR emp_load_cur IS
      SELECT ROWID, s.*
        FROM xxcus.xxcushr_interface_tbl s
       WHERE msg_id = p_msg_id
         AND session_id = p_session_id
         AND stg_trx_id = p_trx_id
         AND stg_status = 'NEW'
       ORDER BY s.processing_sequence DESC;
  
    CURSOR cur_person(p_employee_number VARCHAR2) IS
      SELECT person_id
            ,object_version_number
            ,effective_start_date
            ,employee_number
        FROM per_all_people_f
       WHERE employee_number = p_employee_number
         AND person_type_id IN
             (SELECT person_type_id
                FROM per_person_types
               WHERE user_person_type = 'Employee')
         AND effective_end_date = '31-DEC-4712';
  
    cur_person_rec cur_person%ROWTYPE;
  
    --Cursor find information for all Terminate records
    CURSOR cur_terminate(p_employee_number VARCHAR2) IS
      SELECT pos.object_version_number
            ,period_of_service_id
            ,p.effective_end_date
            ,p.effective_start_date
            ,p.person_type_id
        FROM per_people_f p, per_periods_of_service pos
       WHERE p.person_id = pos.person_id
         AND employee_number = p_employee_number
         AND person_type_id IN
             (SELECT person_type_id
                FROM per_person_types
               WHERE user_person_type IN ('Employee', 'Ex-employee')) --added ex's. MR
         AND p.effective_end_date = '31-DEC-4712'
         AND pos.actual_termination_date IS NULL; -- 2.2 fix to fix re-fires.
  
    cur_terminate_rec cur_terminate%ROWTYPE;
  
    -- Cursor to Find information about final processing of terminate records
    CURSOR cur_final_process(p_employee_number VARCHAR2) IS
      SELECT pos.object_version_number
            ,period_of_service_id
            ,p.effective_start_date
        FROM per_people_f p, per_periods_of_service pos
       WHERE p.person_id = pos.person_id
         AND employee_number = p_employee_number
         AND person_type_id IN
             (SELECT person_type_id
                FROM per_person_types
               WHERE user_person_type IN ('Employee', 'Ex-employee')) --added ex's. MR
         AND p.effective_end_date = '31-DEC-4712'
         AND pos.object_version_number =
             cur_terminate_rec.object_version_number; --added to tie back to fix 2.2 MR
  
    cur_final_process_rec cur_final_process%ROWTYPE;
    -- cursor to find information about rehire records
    CURSOR cur_rehire(p_employee_number VARCHAR2) IS
      SELECT p.person_id
            ,p.object_version_number pov
            ,p.object_version_number ppx
            ,p.effective_start_date
        FROM per_people_f p
       WHERE p.employee_number = p_employee_number
         AND p.effective_end_date = '31-DEC-4712';
    cur_rehire_rec cur_rehire%ROWTYPE;
  
    --Cursor to find if she is an ex-employee
    CURSOR validate_term_date(p_employee_number VARCHAR2) IS
      SELECT effective_end_date
        FROM per_people_f
       WHERE employee_number = p_employee_number
         AND person_type_id IN
             (SELECT person_type_id
                FROM per_person_types
               WHERE user_person_type = 'Ex-employee');
  
    -- Cursor to find Employee Assignments and  Supervisor_id
    CURSOR cur_supervisor(p_employee_number VARCHAR2) IS
      SELECT nvl(ppa.supervisor_id, '-99') supervisor_id
            ,ppa.assignment_id
            ,ppa.object_version_number
            ,ppe.employee_number
            ,'-99' supervisor_employee_number
        FROM per_people_f ppe, per_all_assignments_f ppa
       WHERE ppe.employee_number = p_employee_number
         AND ppa.person_id = ppe.person_id
         AND ppe.person_type_id IN
             (SELECT person_type_id
                FROM per_person_types
               WHERE user_person_type = 'Employee')
            --  AND pps.person_id(+) = ppa.supervisor_id
         AND ppe.effective_end_date = '31-DEC-4712'
         AND ppa.effective_end_date = '31-DEC-4712'
      -- AND pps.effective_end_date(+) = '31-DEC-4712'
      ;
    cur_supervisor_rec cur_supervisor%ROWTYPE;
  
    -- Cursor to find the earliest possible date a supervisor can be a new emp's supervisor.
    -- if the date is greater than the new hire's date, then this will result in the mode
    -- of the update being changed from CORRECTION TO UPDATE as found in metalink as the appropriate
    -- solution for this particular problem --MR 3/23/07.
    CURSOR cur_supervisor_date(p_employee_number VARCHAR2) IS
      SELECT MIN(ppe.effective_start_date) min_date
        FROM per_people_f ppe
       WHERE ppe.employee_number = p_employee_number
         AND ppe.person_type_id IN
             (SELECT person_type_id
                FROM per_person_types
               WHERE user_person_type = 'Employee');
  
    cur_supervisor_date_rec cur_supervisor_date%ROWTYPE;
  
    --DECLARE
    l_error_code   VARCHAR2(10);
    l_error_mesg   VARCHAR2(250);
    v_count        NUMBER := 0;
    v_error_count  NUMBER := 0;
    v_load_count   NUMBER := 0;
    v_record_count NUMBER := 0;
    v_run          NUMBER := 1;
    l_suploop1     NUMBER := 0;
    l_suploop2     NUMBER := 0;
    l_retry        BOOLEAN := FALSE;
  
    l_attribute_category         VARCHAR2(30) := '163'; --HD Supply Corp USD - Org 
    l_person_id                  NUMBER;
    l_assignment_id              NUMBER;
    l_per_object_version_number  NUMBER;
    l_asg_object_version_number  NUMBER;
    l_per_effective_start_date   DATE;
    l_effective_start_date       DATE;
    l_effective_end_date         DATE;
    l_per_effective_end_date     DATE;
    l_full_name                  VARCHAR2(240);
    l_per_comment_id             NUMBER;
    l_assignment_sequence        NUMBER;
    l_assignment_number          VARCHAR2(30);
    l_email_addy                 VARCHAR2(200) DEFAULT NULL;
    l_fnd_user_name              VARCHAR2(200) DEFAULT NULL;
    l_name_combination_warning   BOOLEAN;
    l_assign_payroll_warning     BOOLEAN;
    l_orig_hire_warning          BOOLEAN;
    l_attribute3                 VARCHAR2(150);
    v_process_action             VARCHAR2(1);
    l_employee_number            VARCHAR2(20);
    l_end_process_date           DATE;
    l_p_effective_date           DATE;
    l_final_date                 DATE;
    l_ps_object_v_number         NUMBER;
    l_period_of_service_id       NUMBER;
    l_date_track_update_mode     VARCHAR2(100);
    l_supervisor_warning         BOOLEAN := NULL;
    l_event_warning              BOOLEAN := NULL;
    l_interview_warning          BOOLEAN := NULL;
    l_review_warning             BOOLEAN := NULL;
    l_recruiter_warning          BOOLEAN := NULL;
    l_asg_future_changes_warning BOOLEAN := NULL;
    l_entries_changed_warning    VARCHAR2(100) := NULL;
    l_pay_proposal_warning       BOOLEAN := NULL;
    l_dod_warning                BOOLEAN := NULL;
    l_hire_date                  DATE := NULL;
    l_termination_date           DATE;
    l_skip                       VARCHAR2(1) := 'F';
    l_ex_emp_status              hr.per_person_types.person_type_id%TYPE;
    l_emp_status                 hr.per_person_types.person_type_id%TYPE;
    v_exists                     VARCHAR2(1);
    l_msg                        VARCHAR2(240);
    l_term_date                  DATE;
    v_term_date                  DATE;
    error_flag                   BOOLEAN := FALSE;
    data_error                   BOOLEAN := FALSE;
    v_num                        NUMBER;
    v_supervisor_id              NUMBER;
    l_group_name                 VARCHAR2(100);
    l_org_now_no_manager_warning BOOLEAN;
    l_spp_delete_warning         BOOLEAN;
  
    l_tax_district_changed_warning BOOLEAN;
    l_people_group_id              NUMBER;
    l_special_ceiling_step_id      NUMBER;
    lp_object_version_number       NUMBER;
    v_date_update_track            VARCHAR2(20);
  
    l_soft_coding_keyflex_id     NUMBER;
    l_comment_id                 NUMBER;
    l_cagr_grade_def_id          NUMBER;
    l_cagr_concatenated_segments VARCHAR2(100);
    l_concatenated_segments      VARCHAR2(255);
    l_no_managers_warning        BOOLEAN;
    l_other_manager_warning      BOOLEAN;
    l_freq                       VARCHAR2(255);
  
    --exceptions
    null_value_found EXCEPTION;
    validation_error EXCEPTION;
    empl_load_error  EXCEPTION;
  
  BEGIN
    -- Initializing the write email file and skip process.
    SELECT lower(NAME) INTO g_sid FROM v$database;
  
    SELECT pty.person_type_id
      INTO l_emp_status
      FROM hr.per_person_types pty
     WHERE pty.user_person_type = 'Employee';
  
    SELECT pty.person_type_id
      INTO l_ex_emp_status
      FROM hr.per_person_types pty
     WHERE pty.user_person_type = 'Ex-employee';
  
    --  Supervisor bubble loop
    --  This will re-run the interface until the code no longer reports a decrease in errors.
    BEGIN
      LOOP
        l_suploop1 := l_suploop2;
      
        debug('L', 'Start of the program');
      
        -- sets all status's to new where not already processed successfully for this batch.
        UPDATE xxcus.xxcushr_interface_tbl s
           SET stg_status = 'NEW', stg_error_comments = NULL
         WHERE msg_id = p_msg_id
           AND session_id = p_session_id
           AND stg_trx_id = p_trx_id
           AND stg_status <> 'P';
      
        --Report_Header;
        debug('O', 'Record Number' || '|' || 'Error Comment');
      
        -- Sets status to 'NOT EXISTS' where updates or Terminates are not in oracle HR tables.
        UPDATE xxcus.xxcushr_interface_tbl s
           SET stg_status         = 'NOT EXIST'
              ,stg_error_comments = 'Employee Does Not Exist in Oracle. This stg_process_type = T or U Employee Should Exist In Oracle.'
         WHERE msg_id = p_msg_id
           AND session_id = p_session_id
           AND stg_trx_id = p_trx_id
           AND stg_process_flag IN ('U', 'T')
           AND stg_status = 'NEW'
           AND NOT EXISTS
         (SELECT 1
                  FROM per_all_people_f ppf
                 WHERE ppf.employee_number = s.stg_employee_number);
      
        --  Sets status to 'Exists' where New records already exist in oracle HR tables.
        UPDATE xxcus.xxcushr_interface_tbl s
           SET stg_status         = 'EXISTS'
              ,stg_error_comments = substr('This stg_process_type = N But Employee Already Exists in Oracle.'
                                          ,1
                                          ,150)
         WHERE msg_id = p_msg_id
           AND session_id = p_session_id
           AND stg_trx_id = p_trx_id
           AND stg_process_flag IN ('N')
           AND stg_status = 'NEW'
           AND EXISTS
         (SELECT 1
                  FROM per_all_people_f ppf
                 WHERE ppf.employee_number = s.stg_employee_number);
      
        -- Never seen this used
        UPDATE xxcus.xxcushr_interface_tbl s
           SET stg_status         = 'NVAL'
              ,stg_error_comments = substr(stg_error_comments ||
                                           ';One Or All Required Column is Null In This Record.'
                                          ,1
                                          ,150)
         WHERE msg_id = p_msg_id
           AND session_id = p_session_id
           AND stg_trx_id = p_trx_id
           AND (stg_employee_number IS NULL OR stg_last_name IS NULL OR
               stg_first_name IS NULL OR stg_hire_date IS NULL OR
               stg_sex IS NULL OR stg_process_flag IS NULL);
      
        debug('L', 'Before start of emp_load_cur LOOP ...');
      
        \****************************************************************************************\
        \****************************************************************************************\
        \**              START OF CREATE, UPDATE, REHIRE, TERMINATE PROCESS                    **\
        \****************************************************************************************\
        \****************************************************************************************\
        FOR empl_load IN emp_load_cur
        LOOP
          l_msg := NULL;
          SAVEPOINT create_employee;
        
          BEGIN
            --Initializing the Variables
            l_ps_object_v_number        := NULL;
            l_person_id                 := NULL;
            l_period_of_service_id      := NULL;
            l_p_effective_date          := NULL;
            l_person_id                 := NULL;
            l_per_object_version_number := NULL;
            l_asg_object_version_number := NULL;
            l_assignment_id             := NULL;
            l_effective_start_date      := NULL;
            l_effective_end_date        := NULL;
            l_employee_number           := NULL;
            l_full_name                 := NULL;
            l_per_comment_id            := NULL;
            l_assignment_sequence       := NULL;
            l_name_combination_warning  := NULL;
            l_assign_payroll_warning    := NULL;
            l_orig_hire_warning         := NULL;
            v_supervisor_id             := NULL;
            l_soft_coding_keyflex_id    := NULL;
            l_comment_id                := NULL;
            l_concatenated_segments     := NULL;
            l_no_managers_warning       := NULL;
            l_other_manager_warning     := NULL;
            v_date_update_track         := NULL;
          
            \****************************************************************************************\
            \****************************************************************************************\
            \**                            NEW HIRE PROCESS                                        **\
            \****************************************************************************************\
            \****************************************************************************************\
            IF empl_load.stg_process_flag = 'N'
            THEN
            
              -- Fix Hire date issue
              IF to_date(empl_load.stg_hire_date, 'MMDDYYYY') >
                 to_date(empl_load.stg_effective_start_date, 'MMDDYYYY')
              THEN
                l_hire_date := to_date(empl_load.stg_effective_start_date
                                      ,'MMDDYYYY');
              ELSE
                l_hire_date := to_date(empl_load.stg_hire_date, 'MMDDYYYY');
              END IF;
            
              l_msg := 'Starting Create New Employee API - ' ||
                       empl_load.stg_employee_number || '-Trx_id-' ||
                       empl_load.stg_trx_id || ' ';
              debug('L', l_msg);
            
              fnd_file.put_line(fnd_file.output, l_msg);
              BEGIN
                hr_employee_api.create_us_employee(p_hire_date         => l_hire_date
                                                  ,p_business_group_id => 0
                                                  ,p_last_name         => empl_load.stg_last_name
                                                  ,p_first_name        => empl_load.stg_first_name
                                                  ,p_sex               => empl_load.stg_sex
                                                  ,p_work_telephone    => empl_load.work_telephone
                                                  ,p_email_address     => empl_load.email_address
                                                  ,p_employee_number   => empl_load.stg_employee_number
                                                  ,p_middle_names      => empl_load.stg_middle_names
                                                   --,p_attribute_category        => l_attribute_category
                                                  ,p_attribute1 => empl_load.stg_attribute1
                                                   \*      ,p_attribute2                => to_char(to_date(empl_load.stg_attribute2
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                ,'MMDDYYYY')
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        ,'YYYY/MM/DD HH24:MI:SS')
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 ,p_attribute3                => empl_load.stg_attribute3
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 ,p_attribute4                => empl_load.nt_login
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 ,p_attribute5                => empl_load.stg_attribute9        *\
                                                  ,p_person_id                 => l_person_id
                                                  ,p_assignment_id             => l_assignment_id
                                                  ,p_per_object_version_number => l_per_object_version_number
                                                  ,p_asg_object_version_number => l_asg_object_version_number
                                                  ,p_per_effective_start_date  => l_effective_start_date
                                                  ,p_per_effective_end_date    => l_effective_end_date
                                                  ,p_full_name                 => l_full_name
                                                  ,p_per_comment_id            => l_per_comment_id
                                                  ,p_assignment_sequence       => l_assignment_sequence
                                                  ,p_assignment_number         => l_assignment_number
                                                  ,p_name_combination_warning  => l_name_combination_warning
                                                  ,p_assign_payroll_warning    => l_assign_payroll_warning
                                                  ,p_orig_hire_warning         => l_orig_hire_warning
                                                  ,p_validate                  => FALSE);
              
                fnd_file.put_line(fnd_file.output
                                 ,'New Person ID:  ' || l_person_id);
              EXCEPTION
                WHEN OTHERS THEN
                  l_msg := empl_load.stg_trx_id ||
                           ' - Error in Hr_Employee_Api.create_us_employee.  Error MSG : ' || '-' ||
                           SQLERRM;
                  debug('O', l_msg);
                  RAISE empl_load_error;
              END;
            
              \*        Potential debugging code
              debug('O','Processed new employee '||empl_load.stg_last_name||', '||empl_load.stg_first_name);
              debug('O','with out values of ');
              debug('O','assignment_id ' ||l_assignment_id  );
              debug('O','person_id ' ||l_person_id  );
              debug('O','per object version id ' ||l_per_object_version_number  );
              debug('O','asg object version id ' ||l_asg_object_version_number  );                   *\
            
              \****************************************************************************************\
              \****************************************************************************************\
              \**                              UPDATE PROCESS                                        **\
              \****************************************************************************************\
              \****************************************************************************************\
            
            ELSIF empl_load.stg_process_flag = 'U'
            THEN
              l_msg := ' - For this TRX_ID, MSG ERROR : ';
            
              cur_person_rec := NULL;
            
              OPEN cur_person(empl_load.stg_employee_number);
              FETCH cur_person
                INTO cur_person_rec;
              IF cur_person%NOTFOUND
              THEN
              
                l_msg := ' - For this TRX_ID, Unable To Find The Employee - ' ||
                         to_char(empl_load.stg_employee_number) ||
                         '- At Update Employee API.';
              
                debug('O', empl_load.stg_trx_id || l_msg);
              
                CLOSE cur_person;
                RAISE empl_load_error;
              ELSE
                CLOSE cur_person;
              END IF;
            
              IF cur_person%ISOPEN
              THEN
                CLOSE cur_person;
              END IF;
            
              BEGIN
                hr_person_api.update_us_person(p_validate              => FALSE
                                              ,p_effective_date        => to_date(empl_load.creation_date
                                                                                 ,'MMDDYYYY')
                                              ,p_datetrack_update_mode => 'CORRECTION' --'UPDATE'
                                              ,p_person_id             => cur_person_rec.person_id
                                              ,p_employee_number       => cur_person_rec.employee_number
                                              ,p_object_version_number => cur_person_rec.object_version_number
                                              ,p_first_name            => empl_load.stg_first_name
                                              ,p_middle_names          => empl_load.stg_middle_names
                                              ,p_last_name             => empl_load.stg_last_name
                                              ,p_work_telephone        => empl_load.work_telephone
                                              ,p_email_address         => empl_load.email_address
                                              ,p_attribute1            => empl_load.stg_attribute1
                                               --,p_attribute4               => empl_load.nt_login
                                               --,p_attribute5               => empl_load.stg_attribute9
                                              ,p_effective_start_date     => l_effective_start_date
                                              ,p_effective_end_date       => l_effective_end_date
                                              ,p_full_name                => l_full_name
                                              ,p_comment_id               => l_per_comment_id
                                              ,p_name_combination_warning => l_name_combination_warning
                                              ,p_assign_payroll_warning   => l_assign_payroll_warning
                                              ,p_orig_hire_warning        => l_orig_hire_warning);
              
                fnd_file.put_line(fnd_file.output
                                 ,'Update Person ID: ' ||
                                  cur_person_rec.person_id);
              EXCEPTION
                WHEN OTHERS THEN
                  l_msg := empl_load.stg_trx_id ||
                           ' - Error in  Hr_Person_Api.update_us_person. Error MSG : ' ||
                           SQLERRM;
                  debug('O', l_msg);
                  RAISE empl_load_error;
              END;
            
              -- Start of code to update FND_USER email address info if needed.
              BEGIN
                -- DEBUG NAME
                l_err_callpoint := 'Updating Emails in FND_USER table';
                -- DEBUG NAME
              
                IF xxcus_create_apps_user_pkg.xxcus_check_user_exists(empl_load.stg_employee_number) = 'Y'
                THEN
                  BEGIN
                    SELECT a.email_address, a.user_name
                      INTO l_email_addy, l_fnd_user_name
                      FROM apps.fnd_user a, apps.per_employees_x b
                     WHERE b.employee_num = empl_load.stg_employee_number
                       AND a.employee_id = b.employee_id;
                  EXCEPTION
                    WHEN OTHERS THEN
                      l_email_addy := ' ';
                  END;
                
                  IF nvl(l_email_addy, 'XXX') <>
                     nvl(empl_load.email_address, l_email_addy)
                  THEN
                    fnd_user_pkg.updateuser(x_user_name     => l_fnd_user_name
                                           ,x_owner         => 'CUST'
                                           ,x_email_address => empl_load.email_address);
                  
                    fnd_file.put_line(fnd_file.output
                                     ,'Update user email: ' ||
                                      l_fnd_user_name);
                  END IF;
                
                END IF;
              EXCEPTION
                WHEN OTHERS THEN
                  xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                                      ,p_calling           => l_err_callpoint
                                                      ,p_ora_error_msg     => SQLERRM
                                                      ,p_error_desc        => 'Updating FND email address'
                                                      ,p_distribution_list => l_distro_list
                                                      ,p_module            => 'HR');
              END;
            
              l_assignment_id             := NULL;
              l_asg_object_version_number := NULL;
            
              \****************************************************************************************\
              \****************************************************************************************\
              \**                              REHIRE PROCESS                                        **\
              \****************************************************************************************\
              \****************************************************************************************\
            
            ELSIF empl_load.stg_process_flag = 'R'
            THEN
              l_msg := 'Error At Re-hire Employee API - ' ||
                       empl_load.stg_employee_number;
            
              -- Validating that this employee is an ex-employee
              OPEN validate_term_date(empl_load.stg_employee_number);
              FETCH validate_term_date
                INTO v_term_date;
              IF validate_term_date%FOUND
              THEN
                -- Found as an ex-employee
                CLOSE validate_term_date;
              
                OPEN cur_rehire(empl_load.stg_employee_number);
                FETCH cur_rehire
                  INTO cur_rehire_rec;
                IF cur_rehire%NOTFOUND
                THEN
                  l_msg := 'Unable To Find The Re-Hire Employee-' ||
                           to_char(empl_load.stg_employee_number) ||
                           '- At Re-hire Employee API.';
                  debug('O', empl_load.stg_trx_id || '|' || l_msg);
                  CLOSE cur_rehire;
                  RAISE empl_load_error;
                ELSE
                  CLOSE cur_rehire;
                END IF;
              
                --l_msg := 'Before API call re_hire_ex_employee: ' || empl_load.stg_employee_number; 
              
                BEGIN
                  hr_employee_api.re_hire_ex_employee(p_validate                  => FALSE
                                                     ,p_hire_date                 => to_date(empl_load.stg_effective_start_date
                                                                                            ,'MMDDYYYY')
                                                     ,p_person_id                 => cur_rehire_rec.person_id
                                                     ,p_per_object_version_number => cur_rehire_rec.ppx
                                                     ,p_rehire_reason             => 'REHIRE'
                                                     ,p_assignment_id             => l_assignment_id
                                                     ,p_asg_object_version_number => l_per_object_version_number
                                                     ,p_per_effective_start_date  => l_effective_start_date
                                                     ,p_per_effective_end_date    => l_effective_end_date
                                                     ,p_assignment_sequence       => l_assignment_sequence
                                                     ,p_assignment_number         => l_assignment_number
                                                     ,p_assign_payroll_warning    => l_assign_payroll_warning);
                
                  fnd_file.put_line(fnd_file.output
                                   ,'Rehire ex employee person_id: ' ||
                                    cur_rehire_rec.person_id);
                EXCEPTION
                  WHEN OTHERS THEN
                    l_msg := empl_load.stg_trx_id ||
                             ' - Error in  re_hire_ex_employee. Error MSG : ' || '-' ||
                             SQLERRM;
                    debug('O', l_msg);
                    RAISE empl_load_error;
                END;
              
              ELSE
                -- Although the process flag = 'R', she is a new employee in Oracle.
                -- Else for this : IF validate_term_date%FOUND THEN 
              
                IF validate_term_date%ISOPEN
                THEN
                  CLOSE validate_term_date;
                END IF;
              
                --l_msg := 'Starting Create New Employee API with a REhire Flag - ' ||  empl_load.stg_employee_number;
              
                BEGIN
                  hr_employee_api.create_us_employee(p_hire_date         => to_date(empl_load.stg_hire_date
                                                                                   ,'MMDDYYYY')
                                                    ,p_business_group_id => 0
                                                    ,p_last_name         => empl_load.stg_last_name
                                                    ,p_first_name        => empl_load.stg_first_name
                                                    ,p_sex               => empl_load.stg_sex
                                                    ,p_employee_number   => empl_load.stg_employee_number
                                                    ,p_middle_names      => empl_load.stg_middle_names
                                                    ,p_attribute1        => empl_load.stg_attribute1
                                                     \*      ,p_attribute2                => to_char(to_date(empl_load.stg_attribute2
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ,'DD-MON-YYYY')
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    ,'YYYY/MM/DD HH24:MI:SS')
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             ,p_attribute3                => empl_load.stg_attribute3
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             ,p_attribute4                => empl_load.nt_login
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             ,p_attribute5                => empl_load.stg_attribute9      *\
                                                    ,p_person_id                 => l_person_id
                                                    ,p_assignment_id             => l_assignment_id
                                                    ,p_per_object_version_number => l_per_object_version_number
                                                    ,p_asg_object_version_number => l_asg_object_version_number
                                                    ,p_per_effective_start_date  => l_effective_start_date
                                                    ,p_per_effective_end_date    => l_effective_end_date
                                                    ,p_full_name                 => l_full_name
                                                    ,p_per_comment_id            => l_per_comment_id
                                                    ,p_assignment_sequence       => l_assignment_sequence
                                                    ,p_assignment_number         => l_assignment_number
                                                    ,p_name_combination_warning  => l_name_combination_warning
                                                    ,p_assign_payroll_warning    => l_assign_payroll_warning
                                                    ,p_orig_hire_warning         => l_orig_hire_warning
                                                    ,p_validate                  => FALSE);
                
                  fnd_file.put_line(fnd_file.output
                                   ,'Rehire flag new person_id: ' ||
                                    l_person_id);
                
                EXCEPTION
                  WHEN OTHERS THEN
                    l_msg := empl_load.stg_trx_id ||
                             ' - Error in Hr_Employee_Api.create_us_employee. Error MSG : ' || '-' ||
                             SQLERRM;
                    debug('O', l_msg);
                    RAISE empl_load_error;
                END;
              
              END IF;
            
              \****************************************************************************************\
              \****************************************************************************************\
              \**                              TERMINATE PROCESS                                     **\
              \****************************************************************************************\
              \****************************************************************************************\
            
            ELSIF empl_load.stg_process_flag = 'T'
            THEN
              l_msg             := 'Error At Terminate Employee API - ' ||
                                   empl_load.stg_employee_number;
              cur_terminate_rec := NULL;
            
              OPEN cur_terminate(empl_load.stg_employee_number);
              FETCH cur_terminate
                INTO cur_terminate_rec;
              IF cur_terminate%NOTFOUND
              THEN
              
                l_msg := 'Unable To Find The Employee - ' ||
                         to_char(empl_load.stg_employee_number) ||
                         '- At Terminate Employee API.';
              
                debug('L', empl_load.stg_trx_id || ' | ' || l_msg);
              
                CLOSE cur_terminate;
                RAISE empl_load_error;
              ELSIF cur_terminate_rec.person_type_id = l_ex_emp_status
              THEN
                l_skip := 'T';
                UPDATE xxcus.xxcushr_interface_tbl
                   SET stg_status         = 'P'
                      ,stg_error_comments = 'Person terminated already'
                 WHERE ROWID = empl_load.rowid;
                CLOSE cur_terminate;
                \*          ELSIF TO_DATE(empl_load.stg_attribute2,'MMDDYYYY') <= cur_terminate_rec.effective_start_date THEN
                      l_term_date := cur_terminate_rec.effective_start_date+1;
                ELSE
                      l_term_date := TO_DATE(empl_load.stg_attribute2,'MMDDYYYY') ;*\
                -- v1.2 Start mod SR24339
              ELSE
                l_term_date := empl_load.stg_last_day_paid;
              
              END IF;
            
              IF cur_terminate%ISOPEN
              THEN
                CLOSE cur_terminate;
              END IF;
              l_msg := 'Before EE API call Terminate us person: ' ||
                       empl_load.stg_employee_number;
            
              l_end_process_date := l_term_date + 90;
            
              IF l_skip = 'F'
              THEN
                -- Skip if found but already termed.
              
                BEGIN
                
                  hr_ex_employee_api.actual_termination_emp(p_validate                   => FALSE
                                                           ,p_effective_date             => l_term_date
                                                           ,p_period_of_service_id       => cur_terminate_rec.period_of_service_id
                                                           ,p_object_version_number      => cur_terminate_rec.object_version_number
                                                           ,p_actual_termination_date    => l_term_date
                                                           ,p_last_standard_process_date => l_end_process_date
                                                           ,p_attribute2                 => to_char(to_date(empl_load.stg_attribute2
                                                                                                           ,'MMDDYYYY')
                                                                                                   ,'DD-MON-YYYY')
                                                           ,p_last_std_process_date_out  => l_end_process_date
                                                           ,p_supervisor_warning         => l_supervisor_warning
                                                           ,p_event_warning              => l_event_warning
                                                           ,p_interview_warning          => l_interview_warning
                                                           ,p_review_warning             => l_review_warning
                                                           ,p_recruiter_warning          => l_recruiter_warning
                                                           ,p_asg_future_changes_warning => l_asg_future_changes_warning
                                                           ,p_entries_changed_warning    => l_entries_changed_warning
                                                           ,p_pay_proposal_warning       => l_pay_proposal_warning
                                                           ,p_dod_warning                => l_dod_warning);
                
                  fnd_file.put_line(fnd_file.output
                                   ,'Termination service id: ' ||
                                    cur_terminate_rec.period_of_service_id);
                
                EXCEPTION
                  WHEN OTHERS THEN
                    l_msg := empl_load.stg_trx_id ||
                             ' - Error in actual_termination_emp. Error MSG : ' || '-' ||
                             SQLERRM;
                    debug('L', l_msg);
                    RAISE empl_load_error;
                END;
                COMMIT;
              END IF; --Skip process if existing term
            
              cur_final_process_rec := NULL;
            
              l_msg := 'Cur_final_process(+)';
            
              OPEN cur_final_process(empl_load.stg_employee_number);
              FETCH cur_final_process
                INTO cur_final_process_rec;
              IF cur_final_process%NOTFOUND
              THEN
              
                l_msg := 'Unable To Find The Employee - ' ||
                         to_char(empl_load.stg_employee_number) ||
                         ' - At final_process Employee API.';
              
                debug('O', empl_load.stg_trx_id || '|' || l_msg);
              
                CLOSE cur_final_process;
                RAISE empl_load_error;
              ELSE
                CLOSE cur_final_process;
              END IF;
            
              IF cur_final_process%ISOPEN
              THEN
                CLOSE cur_final_process;
              END IF;
              l_msg := 'Before EE API call final_process_emp : ' ||
                       empl_load.stg_employee_number;
            
              l_end_process_date := l_term_date + 90;
            
              IF l_skip = 'F'
              THEN
                -- Skip if found but already termed.
                BEGIN
                  hr_ex_employee_api.final_process_emp(p_validate                   => FALSE
                                                      ,p_period_of_service_id       => cur_final_process_rec.period_of_service_id
                                                      ,p_object_version_number      => cur_final_process_rec.object_version_number
                                                      ,p_final_process_date         => l_end_process_date
                                                      ,p_org_now_no_manager_warning => l_supervisor_warning
                                                      ,p_asg_future_changes_warning => l_asg_future_changes_warning
                                                      ,p_entries_changed_warning    => l_entries_changed_warning);
                
                  fnd_file.put_line(fnd_file.output
                                   ,'final process service id: ' ||
                                    cur_final_process_rec.period_of_service_id);
                
                EXCEPTION
                  WHEN OTHERS THEN
                    l_msg := empl_load.stg_trx_id ||
                             ' - Error in final_process_emp. Error MSG : ' || '-' ||
                             SQLERRM;
                    debug('O', l_msg || SQLERRM);
                    RAISE empl_load_error;
                END;
              
              END IF; -- skip process
            
              l_skip := 'F'; --reset skip
            
            END IF; -- end of main IF statement to process initial API's
          
            \****************************************************************************************\
            \****************************************************************************************\
            \**                   END OF CREATE, UPDATE, REHIRE, TERMINATE PROCESS                 **\
            \****************************************************************************************\
            \****************************************************************************************\
          
            \****************************************************************************************\
            \****************************************************************************************\
            \**                        START UPDATE ASSIGNMENTS PROCESS                           **\
            \****************************************************************************************\
            \****************************************************************************************\
          
            -- Creating the supervisor for the employee
            IF empl_load.stg_process_flag <> 'T'
            THEN
              -- not required for terminated empoyee
            
              v_count            := 0;
              cur_supervisor_rec := NULL;
              OPEN cur_supervisor(empl_load.stg_employee_number);
              LOOP
                FETCH cur_supervisor
                  INTO cur_supervisor_rec;
                EXIT WHEN cur_supervisor%NOTFOUND;
              END LOOP;
              v_count := cur_supervisor%ROWCOUNT;
            
              IF cur_supervisor%ISOPEN
              THEN
                CLOSE cur_supervisor;
              END IF;
              --     DEBUG('L','here5');
              IF v_count > 1
              THEN
                l_msg := 'Too Many Rows Found For Cursor cur_supervisor - Employee _number: ' ||
                         empl_load.stg_employee_number;
                debug('L', empl_load.stg_trx_id || ' | ' || l_msg);
                RAISE empl_load_error;
              END IF;
            
              \*   AAA   dup code ?  *\
            
              -- code to find if supervisor was current for new employee.
              cur_supervisor_date_rec := NULL;
              OPEN cur_supervisor_date(empl_load.supervisor_id);
              FETCH cur_supervisor_date
                INTO cur_supervisor_date_rec;
              CLOSE cur_supervisor_date;
            
              -- Need to examine further as it appears to do the same as the cursor up above.
              BEGIN
                v_supervisor_id := NULL;
                SELECT nvl(person_id, '-99')
                  INTO v_supervisor_id
                  FROM per_all_people_f
                 WHERE employee_number = to_char(empl_load.supervisor_id)
                   AND person_type_id IN
                       (SELECT person_type_id
                          FROM per_person_types
                         WHERE user_person_type = 'Employee')
                   AND effective_end_date = '31-DEC-4712';
              EXCEPTION
                WHEN too_many_rows THEN
                  v_supervisor_id := NULL;
                  debug('L'
                       ,empl_load.stg_trx_id ||
                        ' Too Many Rows in finding supervisor_id for this trx_id:');
                  RAISE empl_load_error;
                WHEN OTHERS THEN
                  v_supervisor_id := NULL;
                  l_msg           := empl_load.stg_trx_id ||
                                     ' - For this TRX_ID, Unable To find THE supervisor_id. MSG ERROR : ' ||
                                     SQLERRM;
                  debug('L', l_msg);
                  RAISE empl_load_error;
              END;
            
              IF empl_load.stg_process_flag = 'N'
              THEN
                -- Logic to validate the supervisor was in our system when do the update.  Mode
                -- is changed to UPDATE in order to accomodate this scenario.
                IF cur_supervisor_date_rec.min_date >=
                   to_date(empl_load.stg_hire_date, 'MMDDYYYY')
                THEN
                  v_date_update_track := 'UPDATE';
                
                ELSE
                  v_date_update_track := 'CORRECTION';
                  -- Logic to determine if supervisor was previously termed during the span of the hire date.
                
                  BEGIN
                    FOR c_sup_term_dt IN (SELECT effective_start_date
                                                ,effective_end_date
                                            FROM hr.per_all_people_f
                                           WHERE employee_number =
                                                 to_char(empl_load.supervisor_id)
                                             AND person_type_id = 9)
                    LOOP
                      IF to_date(empl_load.stg_hire_date, 'MMDDYYYY') >=
                         c_sup_term_dt.effective_start_date AND
                         to_date(empl_load.stg_hire_date, 'MMDDYYYY') <=
                         c_sup_term_dt.effective_end_date
                      THEN
                        v_date_update_track := 'UPDATE';
                        EXIT;
                      ELSE
                        v_date_update_track := 'CORRECTION';
                      END IF;
                    END LOOP;
                  
                  EXCEPTION
                    WHEN OTHERS THEN
                      v_date_update_track := 'CORRECTION';
                  END;
                
                END IF;
              ELSIF empl_load.stg_process_flag = 'U'
              THEN
                v_date_update_track := 'UPDATE';
              END IF;
            
              IF empl_load.stg_process_flag = 'R'
              THEN
                v_date_update_track := 'CORRECTION';
              END IF;
            
              l_per_object_version_number := nvl(l_asg_object_version_number
                                                ,cur_supervisor_rec.object_version_number);
            
              --        If statement to check if branch id branch will result in error.
              --        A 0 returned will cause errors in the API as no ccid will be found.
              --        If this does process, leave and do not process any other API's for this employee. Rollback
              IF xxcus_misc_pkg.get_ccid_create(lpad(empl_load.stg_attribute1
                                                    ,4
                                                    ,'0')
                                               ,empl_load.stg_attribute9) = 0
              THEN
                l_msg := 'Default code combo is invalid, please check value in stg_attribute 1 and 9 : ' ||
                         empl_load.stg_attribute1 || ' | ' ||
                         empl_load.stg_attribute9 ||
                         ' | For Employee Number:  ' ||
                         empl_load.stg_employee_number;
                debug('L', l_msg);
              
                --       raise empl_load_error;
                --       Call the API for assignment but don't pass in the ccid
                hr_assignment_api.update_us_emp_asg(p_validate                   => FALSE
                                                   ,p_effective_date             => to_date(empl_load.creation_date
                                                                                           ,'MMDDYYYY')
                                                   ,p_datetrack_update_mode      => v_date_update_track
                                                   ,p_assignment_id              => nvl(l_assignment_id
                                                                                       ,cur_supervisor_rec.assignment_id)
                                                   ,p_object_version_number      => l_per_object_version_number
                                                   ,p_supervisor_id              => v_supervisor_id
                                                   ,p_job_post_source_name       => 'Approver'
                                                   ,p_set_of_books_id            => get_sob_id('HD Supply USD')
                                                   ,p_cagr_grade_def_id          => l_cagr_grade_def_id
                                                   ,p_cagr_concatenated_segments => l_cagr_concatenated_segments
                                                   ,p_concatenated_segments      => l_concatenated_segments
                                                   ,p_soft_coding_keyflex_id     => l_soft_coding_keyflex_id
                                                   ,p_comment_id                 => l_comment_id
                                                   ,p_effective_start_date       => l_effective_start_date
                                                   ,p_effective_end_date         => l_effective_end_date
                                                   ,p_no_managers_warning        => l_no_managers_warning
                                                   ,p_other_manager_warning      => l_other_manager_warning);
              
                l_msg := 'Update assignment id: ' ||
                         cur_supervisor_rec.assignment_id;
                debug('L', l_msg);
              
                -- removed the error when an invalid code combination found.
                -- DEBUG NAME
                l_err_callpoint := 'Retrieving code combination. ';
                -- DEBUG NAME
              
                xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                                    ,p_calling           => l_err_callpoint
                                                    ,p_ora_error_msg     => 'Emp# ' ||
                                                                            empl_load.stg_employee_number
                                                    ,p_error_desc        => 'For Trx_id- ' ||
                                                                            empl_load.stg_trx_id || '. ' ||
                                                                            'default code combo is invalid, please check value in stg_attribute 1 and 9 : ' ||
                                                                            empl_load.stg_attribute1 || '  ' ||
                                                                            empl_load.stg_attribute9
                                                    ,p_distribution_list => g_distro_list
                                                    ,p_module            => 'HR'
                                                    ,p_argument1         => 'Last Name ' ||
                                                                            empl_load.stg_last_name
                                                    ,p_argument2         => 'First Name ' ||
                                                                            empl_load.stg_first_name
                                                    ,p_argument3         => 'Employee # ' ||
                                                                            empl_load.stg_employee_number
                                                    ,p_argument4         => 'Branch : ' ||
                                                                            empl_load.stg_attribute1);
              ELSE
              
                -- ADD CODE TO CHECK if Update for employee is on a date before the emp was created.
                -- CHANGE THE update mode to correction and update it on the last effective start date
              
                BEGIN
                
                  -- This is the API to update the assignment with info from the staging table.
                
                  hr_assignment_api.update_us_emp_asg(p_validate                   => FALSE
                                                     ,p_effective_date             => to_date(empl_load.creation_date
                                                                                             ,'MMDDYYYY')
                                                     ,p_datetrack_update_mode      => v_date_update_track
                                                     ,p_assignment_id              => nvl(l_assignment_id
                                                                                         ,cur_supervisor_rec.assignment_id)
                                                     ,p_object_version_number      => l_per_object_version_number
                                                     ,p_supervisor_id              => v_supervisor_id
                                                     ,p_default_code_comb_id       => xxcus_misc_pkg.get_ccid_create(lpad(empl_load.stg_attribute1
                                                                                                                         ,4
                                                                                                                         ,'0')
                                                                                                                    ,empl_load.stg_attribute9)
                                                     ,p_job_post_source_name       => 'Approver'
                                                     ,p_set_of_books_id            => get_sob_id('HD Supply USD')
                                                     ,p_cagr_grade_def_id          => l_cagr_grade_def_id
                                                     ,p_cagr_concatenated_segments => l_cagr_concatenated_segments
                                                     ,p_concatenated_segments      => l_concatenated_segments
                                                     ,p_soft_coding_keyflex_id     => l_soft_coding_keyflex_id
                                                     ,p_comment_id                 => l_comment_id
                                                     ,p_effective_start_date       => l_effective_start_date
                                                     ,p_effective_end_date         => l_effective_end_date
                                                     ,p_no_managers_warning        => l_no_managers_warning
                                                     ,p_other_manager_warning      => l_other_manager_warning);
                
                EXCEPTION
                  WHEN OTHERS THEN
                    l_msg := '_____________________________________________________________________________________________';
                    debug('O', l_msg);
                    l_msg := 'During API Call to Update Supervisor flag for employee - ' ||
                             empl_load.stg_employee_number;
                    debug('O', l_msg);
                    l_msg := 'Trx_id-' || empl_load.stg_trx_id;
                    debug('O', l_msg);
                    l_msg := 'Effective date: ' ||
                             to_date(empl_load.stg_effective_start_date
                                    ,'MMDDYYYY');
                    debug('O', l_msg);
                    l_msg := 'datetrack mode : ' || v_date_update_track;
                    debug('O', l_msg);
                    l_msg := 'assignment ID: ' ||
                             nvl(l_assignment_id
                                ,cur_supervisor_rec.assignment_id) ||
                             '-object version: ' ||
                             l_per_object_version_number;
                    debug('O', l_msg);
                    l_msg := 'supervisor ID: ' || v_supervisor_id;
                    debug('O', l_msg);
                    l_msg := 'default code combo: ' ||
                             xxcus_misc_pkg.get_ccid_create(lpad(empl_load.stg_attribute1
                                                                ,4
                                                                ,'0')
                                                           ,empl_load.stg_attribute9);
                    debug('O', l_msg);
                    l_msg := 'SOB ID: ' || get_sob_id('HD Supply USD');
                    debug('O', l_msg);
                    l_msg := 'hire date : ' || empl_load.stg_hire_date;
                    debug('O', l_msg); -- debug code, use only if neccessary
                    l_msg := substr('-20007-Error in update_emp_asg - stg_trx_id:' ||
                                    empl_load.stg_trx_id || '-' || SQLERRM
                                   ,1
                                   ,240);
                    debug('O'
                         ,'Error in Hr_Assignment_Api.update_emp_asg:' ||
                          SQLERRM);
                    l_msg := '_____________________________________________________________________________________________';
                    debug('O', l_msg);
                    RAISE empl_load_error;
                END;
              END IF;
            
              BEGIN
              
                -- Get assignment ID ? and object version number.
                -- i can understand the OBJ (need the latest), but not the assignment ID
                -- since it can come from thhe previous API.
                BEGIN
                  SELECT ppa.assignment_id, ppa.object_version_number
                    INTO l_assignment_id, lp_object_version_number
                    FROM per_people_f ppe, per_all_assignments_f ppa
                   WHERE ppe.employee_number =
                         empl_load.stg_employee_number
                     AND ppa.person_id = ppe.person_id
                     AND ppe.person_type_id IN
                         (SELECT person_type_id
                            FROM per_person_types
                           WHERE user_person_type = 'Employee')
                     AND ppe.effective_end_date = '31-DEC-4712';
                EXCEPTION
                  WHEN OTHERS THEN
                    l_msg := empl_load.stg_trx_id ||
                             ' - For this TRX_ID, Unable to find the assignment for the position.';
                    debug('O', l_msg);
                    RAISE empl_load_error;
                END;
              
                IF empl_load.job_class IS NULL
                THEN
                  l_msg := empl_load.stg_trx_id ||
                           ' - For this TRX_ID, The job Class For This Transaction Is Null.';
                  debug('O', l_msg);
                END IF;
              
                hr_assignment_api.update_emp_asg_criteria(p_effective_date               => l_effective_start_date + 1
                                                         ,p_datetrack_update_mode        => v_date_update_track --'UPDATE' --'CORRECTION'
                                                         ,p_assignment_id                => l_assignment_id
                                                         ,p_validate                     => FALSE
                                                         ,p_called_from_mass_update      => TRUE
                                                         ,p_grade_id                     => NULL
                                                         ,p_position_id                  => NULL
                                                         ,p_job_id                       => get_job_id(empl_load.job_class)
                                                         ,p_payroll_id                   => NULL
                                                         ,p_location_id                  => get_location_id(empl_load.stg_attribute1) --('HDS - Legal Entity address USA')  --Version 1.2
                                                         ,p_organization_id              => 0
                                                         ,p_people_group_id              => l_people_group_id
                                                         ,p_object_version_number        => lp_object_version_number
                                                         ,p_special_ceiling_step_id      => l_special_ceiling_step_id
                                                         ,p_group_name                   => l_group_name
                                                         ,p_effective_start_date         => l_effective_start_date
                                                         ,p_effective_end_date           => l_effective_end_date
                                                         ,p_org_now_no_manager_warning   => l_org_now_no_manager_warning
                                                         ,p_other_manager_warning        => l_other_manager_warning
                                                         ,p_spp_delete_warning           => l_spp_delete_warning
                                                         ,p_entries_changed_warning      => l_entries_changed_warning
                                                         ,p_tax_district_changed_warning => l_tax_district_changed_warning);
              
              EXCEPTION
                WHEN OTHERS THEN
                  l_msg := empl_load.stg_trx_id ||
                           ' - 20007-Error in update_emp_asg_criteria. Error MSG : ' || '-' ||
                           SQLERRM;
                  debug('O'
                       ,'Error in Hr_Assignment_Api.update_emp_asg_criteria:' ||
                        SQLERRM);
                  RAISE empl_load_error;
              END;
            
              COMMIT;
            
              IF (l_no_managers_warning)
              THEN
                debug('O'
                     ,'No Manager Warning - ' ||
                      to_char(cur_person_rec.employee_number));
              END IF;
            
              IF (l_other_manager_warning)
              THEN
                debug('O'
                     ,' l_other_manager_warning- ' ||
                      to_char(cur_person_rec.employee_number));
              END IF;
            END IF;
          
            \****************************************************************************************\
            \****************************************************************************************\
            \**                              END UPDATE NEW EMPLOYEE PROCESS                       **\
            \****************************************************************************************\
            \****************************************************************************************\
          
            --update the flag for successfully loaded records
            UPDATE xxcus.xxcushr_interface_tbl
               SET stg_status = 'P'
             WHERE ROWID = empl_load.rowid;
          
            \****************************************************************************************\
            \****************************************************************************************\
            \**                              EMPL_LOAD_ERROR EXCEPTION                             **\
            \****************************************************************************************\
            \****************************************************************************************\
          EXCEPTION
            WHEN empl_load_error THEN
              ROLLBACK TO SAVEPOINT create_employee;
              error_flag := TRUE;
            
              UPDATE xxcus.xxcushr_interface_tbl
                 SET stg_status         = 'E'
                    ,stg_error_comments = substr(stg_error_comments || '-' ||
                                                 l_msg
                                                ,1
                                                ,150)
               WHERE ROWID = empl_load.rowid;
            
            -- main drop out for main block.
            WHEN OTHERS THEN
              ROLLBACK TO SAVEPOINT create_employee;
              error_flag := TRUE;
            
              debug('O', empl_load.stg_trx_id || l_msg || SQLERRM);
            
              UPDATE xxcus.xxcushr_interface_tbl
                 SET stg_status         = 'E'
                    ,stg_error_comments = substr(stg_error_comments || '-' ||
                                                 l_msg
                                                ,1
                                                ,150)
               WHERE ROWID = empl_load.rowid;
            
          END;
          COMMIT;
        END LOOP; -- end of emp_load cursor loop
      
        BEGIN
          SELECT COUNT(*)
            INTO l_suploop2
            FROM xxcus.xxcushr_interface_tbl
           WHERE msg_id = p_msg_id
             AND stg_process_flag = 'N'
             AND stg_status = 'E';
        EXCEPTION
          WHEN no_data_found THEN
            l_suploop2 := l_suploop1;
        END;
      
        EXIT WHEN l_suploop2 = l_suploop1;
      END LOOP; -- supervisor loop
    END;
  
    \****************************************************************************************\
    \****************************************************************************************\
    \**                              REPORT PROCESS                                        **\
    \****************************************************************************************\
    \****************************************************************************************\
  
    report_header;
    FOR emp_rec IN emp_cur
    LOOP
      --initializing the variables
      v_exists := NULL;
      l_msg    := NULL;
    
      v_record_count := v_record_count + 1;
    
      BEGIN
        NULL;
        IF emp_rec.stg_error_comments <>
           'Employee Does Not Exist in Oracle. This stg_process_type = T or U Employee Should Exist In Oracle.'
        THEN
          report_body(emp_rec.stg_trx_id, emp_rec.stg_error_comments);
        END IF;
      EXCEPTION
        WHEN OTHERS THEN
          l_msg := 'TX ID/EMP NUM:  ' || emp_rec.stg_trx_id || '/' ||
                   emp_rec.stg_employee_number || ' : ' || SQLERRM;
        
          xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                              ,p_calling           => l_err_callpoint
                                              ,p_ora_error_msg     => SQLERRM
                                              ,p_error_desc        => l_msg
                                              ,p_distribution_list => l_distro_list
                                              ,p_module            => 'HR');
        
          UPDATE xxcus.xxcushr_interface_tbl
             SET stg_status         = 'E'
                ,stg_error_comments = substr(stg_error_comments || '-' ||
                                             l_msg
                                            ,1
                                            ,150)
           WHERE ROWID = emp_rec.rowid;
          debug('O', emp_rec.stg_trx_id || '|' || SQLERRM);
        
      END;
    
    END LOOP; -- end of emp_Rec loop
    IF error_flag
    THEN
    
      l_msg := 'There Are Errors In The Employee Records. Please Review And Correct The Errors Before Proceeding.';
      debug('O', l_msg);
      logoutput_footer(1);
    ELSE
      logoutput_footer(0);
    
    END IF;
  
    -- Calling the Procedure at add or remove supervisors from the AP Signing Limit.
  
    supervisor_signing_limit('HD Supply Corp USD - Org');
  
  EXCEPTION
    WHEN validation_error THEN
      debug('L'
           ,'There Are Error Records In The Data File. Please Review The Errors.');
      debug('L', 'Please Correct The Errors And Restart The Program.');
      l_msg := 'There Are Errors In The Records. Please Review And Correct The Errors Before Proceeding.';
      IF v_num <> 0
      THEN
        l_msg := 'Unable To Send Email.';
        debug('L', l_msg);
      END IF;
    
    WHEN OTHERS THEN
      l_error_code := to_char(SQLCODE);
      debug('L', l_msg);
      l_msg := 'The Employee Interface With The above Req ID has been submitted.
                      The Program Stopped For Unknown Reason. Please Review The Log File.';
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => l_msg
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'HR');
      debug('L', substr(l_msg, 1, 150));
    
      IF v_num <> 0
      THEN
        l_msg := 'Unable To Send Email.';
        debug('L', l_msg);
      END IF;
      debug('L', 'Error in final others: ' || substr(SQLERRM, 1, 150));
      debug('L', 'Error code ' || l_error_code);
    
  END emp_interface;*/

  /**********************************Report Header Procedure*********************************************/

  PROCEDURE report_header IS
  
  BEGIN
  
    /*--  For Emailing purposes
    
    g_file_dir    := '/xx_iface/' || g_sid || '/outbound';
    g_file_name   := 'HDS_HR_ERRORS_' || to_char(SYSDATE, 'YYYYMMDDHHMISS') || '.txt';
    g_file_handle := utl_file.fopen(g_file_dir, g_file_name, 'w');*/
  
    -- Emailing purposes end.
  
    fnd_file.put(fnd_file.output
                ,'HDSUPPLY' || rpad(g_fill_spaces, 37, ' '));
    --  utl_file.put_line (g_file_handle,'HDSUPPLY' || RPAD(g_fill_spaces, 37, ' '));
    fnd_file.put(fnd_file.output
                ,'HDS : EMPLOYEE INTERFACE' ||
                 rpad(g_fill_spaces, 30, ' ') || 'RUN DATE :');
    -- utl_file.put_line (g_file_handle,'HDSI : EMPLOYEE INTERFACE' || RPAD(g_fill_spaces, 30, ' ') || 'RUN DATE :');
    fnd_file.put_line(fnd_file.output, to_char(SYSDATE, 'MM/DD/YYYY'));
    --  utl_file.put_line (g_file_handle, TO_CHAR(SYSDATE, 'MM/DD/YYYY'));
    fnd_file.put(fnd_file.output, rpad(g_fill_spaces, 116, ' '));
    --  utl_file.put_line (g_file_handle, RPAD(g_fill_spaces, 116, ' '));
    fnd_file.put_line(fnd_file.output
                     ,'RUN TIME :' || to_char(SYSDATE, 'HH:MM'));
    --   utl_file.put_line (g_file_handle,  'RUN TIME :' || TO_CHAR(SYSDATE, 'HH:MM'));
  
  END;
  /**********************************Report Body Procedure*********************************************/
  PROCEDURE report_body(record_num VARCHAR2, err_msg VARCHAR2) IS
  
  BEGIN
  
    g_rep_ln := rpad(record_num, 30, ' ') || rpad(' ', 16) ||
                rpad(err_msg, 80, ' ');
  
    xxcushr_interface_pkg.pk_ln_cnt := xxcushr_interface_pkg.pk_ln_cnt + 1;
    IF xxcushr_interface_pkg.pk_ln_cnt >
       xxcushr_interface_pkg.pk_ln_cnt_max
    THEN
      xxcushr_interface_pkg.pk_pg_cnt := xxcushr_interface_pkg.pk_pg_cnt + 1;
    
      fnd_file.put_line(fnd_file.log, xxcushr_interface_pkg.pk_pg_cnt);
      --     utl_file.put_line(g_file_handle,XXCUS_Hr_Interface_Pkg.pk_pg_cnt);
    
      xxcushr_interface_pkg.pk_ln_cnt := 0;
      /* Skip to Next Page */
      fnd_file.new_line(fnd_file.output, 1);
      fnd_file.put(fnd_file.output, chr(12));
      --    utl_file.put_line(g_file_handle,CHR(12));
      report_header;
      fnd_file.put(fnd_file.output, rpad(g_fill_spaces, 1, ' '));
      fnd_file.put_line(fnd_file.output, g_rep_ln);
      --    utl_file.put_line(g_file_handle,RPAD(g_fill_spaces, 1, ' '));
      --    utl_file.put_line(g_file_handle, g_rep_ln);
    ELSE
      fnd_file.put(fnd_file.output, rpad(g_fill_spaces, 1, ' '));
      fnd_file.put_line(fnd_file.output, g_rep_ln);
      --    utl_file.put_line(g_file_handle,RPAD(g_fill_spaces, 1, ' '));
      --   utl_file.put_line(g_file_handle,g_rep_ln);
    END IF;
  
  END report_body;

  /**********************************Report Footer Procedure*********************************************/

  PROCEDURE logoutput_footer(p_trans_count IN NUMBER) IS
    v_rep_ln     VARCHAR2(132);
    v_rep_footer VARCHAR2(132);
  
  BEGIN
    SELECT NAME INTO g_instance FROM v$database;
    g_sender := 'Oracle.Applications_' || g_instance || '@hdsupply.com';
  
    IF p_trans_count > 0
    THEN
      v_rep_footer := lpad('*** END OF REPORT ***', 74, ' ');
    ELSE
      v_rep_footer := lpad('*** There Are No Errors In The Data File !!!***'
                          ,74
                          ,' ');
    END IF;
    fnd_file.new_line(fnd_file.output, 2);
    fnd_file.put_line(fnd_file.output, v_rep_footer);
    --   utl_file.put_line(g_file_handle, 2);
    --   utl_file.put_line(g_file_handle, v_rep_footer);
  
    -- Close the file
    --   utl_file.fclose(g_file_handle);
  
    -- send email when errors occur
    /*    IF p_trans_count > 0 THEN
     g_ErrorStatus := SendMailJPkg.SendMail(  SMTPServerName => g_host   --'hsim1.hsi.hughessupply.com'
                                             ,Sender         => g_sender --'Oracle.Applications@hughessupply.com'
                                             ,Recipient      => g_dflt_email,
                                              CcRecipient    => '',
                                              BccRecipient   => '',
                                              Subject        => 'HR Errors attachment file' ||g_file_name,
                                              Body           => 'HR Errors attachment file' ||SendMailJPkg.EOL ||
                                                                'THIS IS A SEND ONLY EMAIL - DO NOT REPLY ',
                                              ErrorMessage   => g_ErrorMessage,
                                              Attachments    => SendMailJPkg.ATTACHMENTS_LIST(g_file_dir || '/' ||
                                                                                              g_file_name));
    END IF;
    
    utl_file.fremove('CUSTOMER',g_file_name);*/
  
  END logoutput_footer;

  /**********************************CCID fix*********************************************/
  -- Procedure to find employees that are in the system but have no CCID.
  -- This program will find them and see if they were imported from lawson and try to find
  -- the latest information for them from GL if avaialable.

  /*PROCEDURE ccid_fix(x_errbuf OUT VARCHAR2, x_retcode OUT NUMBER) IS
  
    l_msg                        VARCHAR2(240);
    l_soft_coding_keyflex_id     NUMBER;
    l_comment_id                 NUMBER;
    l_cagr_grade_def_id          NUMBER;
    l_cagr_concatenated_segments VARCHAR2(100);
    l_concatenated_segments      VARCHAR2(255);
    l_no_managers_warning        BOOLEAN;
    l_other_manager_warning      BOOLEAN;
    l_effective_start_date       DATE;
    l_effective_end_date         DATE;
    repair_error EXCEPTION;
  
  BEGIN
    dbms_output.enable(1000000);
  
    FOR cc_repair IN (SELECT paa.assignment_id
                            ,paa.object_version_number
                            ,pap.attribute1
                            ,pap.employee_number
                            ,pap.last_name
                            ,pap.first_name
                            ,pap.attribute9
                        FROM hr.per_all_assignments_f paa
                            ,hr.per_all_people_f      pap
                       WHERE pap.person_id = paa.person_id
                         AND paa.supervisor_id IN
                             (SELECT DISTINCT a.supervisor_id
                                FROM hr.per_all_assignments_f a
                               WHERE a.supervisor_id IS NOT NULL
                                 AND a.effective_end_date = '31-DEC-4712'
                                 AND NOT EXISTS
                               (SELECT 1
                                        FROM ap.ap_web_signing_limits_all web
                                       WHERE web.employee_id = a.supervisor_id))
                         AND paa.effective_end_date = '31-DEC-4712'
                         AND pap.effective_end_date = '31-DEC-4712')
    LOOP
    
      BEGIN
        BEGIN
          --  Check to see if it's a buggy bug with the legacy setup for branches.
          IF cc_repair.attribute1 = 0
          THEN
            RAISE repair_error;
          ELSE
          
            --          Since there is no procedure for this api, need to cut and paste this... yuk!!!
            BEGIN
              hr_assignment_api.update_us_emp_asg(p_validate                   => FALSE
                                                 ,p_effective_date             => SYSDATE
                                                 ,p_datetrack_update_mode      => 'CORRECTION' --'UPDATE'
                                                 ,p_assignment_id              => cc_repair.assignment_id
                                                 ,p_object_version_number      => cc_repair.object_version_number
                                                 ,p_default_code_comb_id       => xxcus_misc_pkg.get_ccid_create(cc_repair.attribute1
                                                                                                                ,cc_repair.attribute9)
                                                 ,p_cagr_grade_def_id          => l_cagr_grade_def_id
                                                 ,p_cagr_concatenated_segments => l_cagr_concatenated_segments
                                                 ,p_concatenated_segments      => l_concatenated_segments
                                                 ,p_soft_coding_keyflex_id     => l_soft_coding_keyflex_id
                                                 ,p_comment_id                 => l_comment_id
                                                 ,p_effective_start_date       => l_effective_start_date
                                                 ,p_effective_end_date         => l_effective_end_date
                                                 ,p_no_managers_warning        => l_no_managers_warning
                                                 ,p_other_manager_warning      => l_other_manager_warning);
            
              l_msg := cc_repair.first_name || ' ' || cc_repair.last_name ||
                       ' was fixed as employee # ' ||
                       cc_repair.employee_number;
              debug('O', l_msg);
            EXCEPTION
              WHEN OTHERS THEN
                l_msg := 'Error in cc_repair process API for Assignment id ' ||
                         cc_repair.assignment_id || ' With error ' ||
                         SQLERRM;
                debug('O', l_msg);
                RAISE repair_error;
            END; --API
          
          END IF;
        
        EXCEPTION
          WHEN OTHERS THEN
            RAISE repair_error;
        END;
      
      EXCEPTION
        WHEN repair_error THEN
          l_msg := 'Error in cc_repair process for Assignment ID ' ||
                   cc_repair.assignment_id ||
                   '. Check branch in Attribute1 ' || cc_repair.attribute1 ||
                   '.   With error ' || SQLERRM;
          debug('O', l_msg);
        
      END;
    
    END LOOP;
  
  END ccid_fix;*/

  -- ************************* PROCEDURE***********************************************************
  --  This is used to clear user_guid if the user description or email needs to be updated or 
  --  fnd_user_pkg.updateuser will fail Unabled to call fnd_ldap_wrapp when updating user     
  --   VERSION DATE          AUTHOR(S)       DESCRIPTION
  -- ------- -----------   --------------- -------------------------------------------------------
  -- 1.0     01-Apr-2014   Kathy Poling    SR 133025 Initial creation 
  -- --------------------------------------------------------------------------------------------- 

  PROCEDURE update_user_guid(p_user_name IN VARCHAR2) IS
  
  BEGIN
  
    UPDATE apps.fnd_user
       SET user_guid = NULL
     WHERE user_name = p_user_name;
  
    COMMIT;
  END;

  -- ************************* PROCEDURE***********************************************************
  --  This is used to change the location for White Cap employees    
  --   VERSION DATE          AUTHOR(S)       DESCRIPTION
  -- ------- -----------   --------------- -------------------------------------------------------
  -- 1.0     30-Apr-2014   Kathy Poling    Initial creation
  --                                       SR 201993 Task ID: 20130415-00545 for White Cap need
  --                                       to get the actual location of employee.  If the corresponding 
  --                                       Location is not yet created then the HR Interface should skip 
  --                                       the automatic update on the White Cap employee record.
  -- 2.1     28-Jul-2014   Kathy Poling    SR 258493 code fix in procedure update_emp_location
  --                                       need to correct api for position_id and job_id.  Also made
  --                                       a change to use the lookup to know if the location should
  --                                       be updated        
  -- 4.0     28-Apr-2015   Maharajan       TMS#20150416-00179 ##No change in code## 
  --			   Shunmugam       **If HR interface skip updating the employee location, 
  --					     purchasing will update location manually only for SCM employees and 
  --					     that should remains the same***                                 
  -- --------------------------------------------------------------------------------------------- 

  PROCEDURE update_emp_location(x_errbuf  OUT VARCHAR2
                               ,x_retcode OUT NUMBER) IS
  
    --DECLARE
    l_error_code  NUMBER;
    l_err_msg     VARCHAR2(3000);
    l_body        VARCHAR2(32767);
    l_body_footer VARCHAR2(32767);
    l_sql_hint    VARCHAR2(32767);
    l_sender      VARCHAR2(100);
  
    l_procedure                    VARCHAR2(30) := 'UPDATE_EMP_LOCATION';
    l_assignment_id                NUMBER;
    l_effective_start_date         DATE;
    l_effective_end_date           DATE;
    l_end_process_date             DATE;
    l_date_track_update_mode       VARCHAR2(100);
    l_entries_changed_warning      VARCHAR2(100) := NULL;
    l_msg                          VARCHAR2(500);
    l_group_name                   VARCHAR2(100);
    l_org_now_no_manager_warning   BOOLEAN;
    l_spp_delete_warning           BOOLEAN;
    l_tax_district_changed_warning BOOLEAN;
    l_people_group_id              NUMBER := hr_api.g_number;
    l_special_ceiling_step_id      NUMBER;
    l_date_update_track            VARCHAR2(20);
    l_soft_coding_keyflex_id       NUMBER;
    l_concatenated_segments        VARCHAR2(255);
    l_no_managers_warning          BOOLEAN;
    l_other_manager_warning        BOOLEAN;
    l_object_version_number        NUMBER;
    l_location_id                  NUMBER;
    l_new_location                 NUMBER;
    l_job_id                       NUMBER;
    l_position_id                  NUMBER;
  
    --exceptions
    empl_location_error EXCEPTION;
  
  BEGIN
  
    --Get Database
    SELECT lower(NAME) INTO g_sid FROM v$database;
  
    l_sender := 'Oracle.Applications_' || g_sid || '@hdsupply.com';
  
    BEGIN
      l_msg := 'Truncate table xxcushr_employee_wc_loc_tbl';
      EXECUTE IMMEDIATE 'truncate table xxcus.xxcushr_employee_wc_loc_tbl';
    EXCEPTION
      WHEN OTHERS THEN
        debug('L', l_msg);
        l_msg := 'Failed to Truncate xxcushr_employee_wc_loc_tbl : ' ||
                 SQLERRM;
        debug('L', l_msg);
        RAISE program_error;
    END;
  
    --Load table with latest data for White Cap employees
  
    BEGIN
      l_msg := 'Insert xxcushr_employee_wc_loc_tbl';
      INSERT /*+ APPEND */
      INTO xxcus.xxcushr_employee_wc_loc_tbl
        SELECT ps.employee_number
              ,ps.last_name
              ,ps.first_name
              ,ps.middle_names
              ,ps.hire_date
              ,ps.effective_start_date
              ,ps.attribute1
              ,ps.process_lev
              ,ps.last_day_paid
          FROM xxcus.xxcushr_ps_emp_all_tbl ps
         WHERE process_lev IN
               (SELECT meaning
                  FROM apps.fnd_lookup_values b
                 WHERE b.lookup_type = 'HDS_HR-INT PS-ORA'
                   AND b.enabled_flag = 'Y'
                   AND nvl(b.end_date_active, SYSDATE) >= SYSDATE
                   AND meaning = process_lev
                   AND b.attribute_category = 'XXCUS_HR_INTERFACE'
                   AND b.attribute1 = 'Y') --Version 2.1 
           AND ps.process_flag = 'A';
    
      COMMIT;
    END;
  
    FOR empl_load IN (SELECT ps.employee_number
                            ,ps.last_name
                            ,ps.first_name
                            ,ps.middle_names
                            ,ps.hire_date
                            ,ps.effective_start_date
                            ,ps.attribute1
                            ,ps.process_lev
                            ,ps.last_day_paid
                            ,xxcushr_interface_pkg.get_location_id(attribute1) location
                        FROM xxcus.xxcushr_employee_wc_loc_tbl ps
                       WHERE ps.employee_number IN
                             (SELECT orac.employee_number
                                FROM hr.per_all_people_f      orac
                                    ,hr.per_all_assignments_f oraac
                               WHERE orac.employee_number =
                                     ps.employee_number
                                 AND orac.person_id = oraac.person_id
                                    --AND orac.person_type_id = 6
                                 AND nvl(oraac.location_id, -1) <>
                                     xxcushr_interface_pkg.get_location_id(ps.attribute1)))
    LOOP
    
      l_msg := 'Loop empl_load number = ' || empl_load.employee_number;
    
      l_assignment_id          := NULL;
      l_effective_start_date   := NULL;
      l_effective_end_date     := NULL;
      l_soft_coding_keyflex_id := NULL;
      l_concatenated_segments  := NULL;
      l_no_managers_warning    := NULL;
      l_other_manager_warning  := NULL;
      l_date_update_track      := 'CORRECTION';
      l_location_id            := NULL;
      l_new_location           := NULL;
      l_job_id                 := NULL;
      l_position_id            := NULL;
    
      BEGIN
      
        SELECT ppa.assignment_id
              ,ppa.object_version_number
              ,nvl(ppa.location_id, -1)
              ,position_id --Version 2.1
              ,job_id --Version 2.1
          INTO l_assignment_id
              ,l_object_version_number
              ,l_location_id
              ,l_position_id
              ,l_job_id
          FROM per_people_f ppe, per_all_assignments_f ppa
         WHERE ppe.employee_number = empl_load.employee_number
           AND ppa.person_id = ppe.person_id
           AND ppe.person_type_id IN
               (SELECT person_type_id
                  FROM per_person_types
                 WHERE user_person_type = 'Employee')
           AND ppe.effective_end_date = '31-DEC-4712';
      
        l_msg := 'Emp #' || empl_load.employee_number || ' New Location - ' ||
                 empl_load.location || ' Old Location - ' || l_location_id;
      EXCEPTION
        WHEN OTHERS THEN
          l_msg := 'Unable to find the assignment for the Emp #' ||
                   empl_load.employee_number;
          debug('L', l_msg);
      END;
    
      IF empl_load.location <> -1 AND empl_load.location <> l_location_id
      THEN
        l_msg := 'Call api update location on assignment_id - ' ||
                 l_assignment_id || ' New Location - ' ||
                 empl_load.location || ' Old Location - ' || l_location_id;
      
        BEGIN
        
          hr_assignment_api.update_emp_asg_criteria(p_effective_date               => trunc(SYSDATE)
                                                   ,p_datetrack_update_mode        => l_date_update_track
                                                   ,p_assignment_id                => l_assignment_id
                                                   ,p_validate                     => FALSE
                                                   ,p_called_from_mass_update      => FALSE
                                                   ,p_grade_id                     => NULL
                                                   ,p_position_id                  => l_position_id --Version 2.1
                                                   ,p_job_id                       => l_job_id --Version 2.1
                                                   ,p_payroll_id                   => NULL
                                                   ,p_location_id                  => empl_load.location
                                                   ,p_organization_id              => 0
                                                   ,p_pay_basis_id                 => NULL
                                                   ,p_segment1                     => NULL
                                                   ,p_segment2                     => NULL
                                                   ,p_segment3                     => NULL
                                                   ,p_segment4                     => NULL
                                                   ,p_segment5                     => NULL
                                                   ,p_segment6                     => NULL
                                                   ,p_segment7                     => NULL
                                                   ,p_segment8                     => NULL
                                                   ,p_segment9                     => NULL
                                                   ,p_segment10                    => NULL
                                                   ,p_segment11                    => NULL
                                                   ,p_segment12                    => NULL
                                                   ,p_segment13                    => NULL
                                                   ,p_segment14                    => NULL
                                                   ,p_segment15                    => NULL
                                                   ,p_segment16                    => NULL
                                                   ,p_segment17                    => NULL
                                                   ,p_segment18                    => NULL
                                                   ,p_segment19                    => NULL
                                                   ,p_segment20                    => NULL
                                                   ,p_segment21                    => NULL
                                                   ,p_segment22                    => NULL
                                                   ,p_segment23                    => NULL
                                                   ,p_segment24                    => NULL
                                                   ,p_segment25                    => NULL
                                                   ,p_segment26                    => NULL
                                                   ,p_segment27                    => NULL
                                                   ,p_segment28                    => NULL
                                                   ,p_segment29                    => NULL
                                                   ,p_segment30                    => NULL
                                                   ,p_employment_category          => NULL
                                                   ,p_concat_segments              => NULL
                                                   ,p_contract_id                  => NULL
                                                   ,p_establishment_id             => NULL
                                                   ,p_scl_segment1                 => NULL
                                                   ,p_grade_ladder_pgm_id          => NULL
                                                   ,p_supervisor_assignment_id     => NULL
                                                   ,p_object_version_number        => l_object_version_number
                                                   ,p_special_ceiling_step_id      => l_special_ceiling_step_id
                                                   ,p_people_group_id              => l_people_group_id
                                                   ,p_soft_coding_keyflex_id       => l_soft_coding_keyflex_id
                                                   ,p_group_name                   => l_group_name
                                                   ,p_effective_start_date         => l_effective_start_date
                                                   ,p_effective_end_date           => l_effective_end_date
                                                   ,p_org_now_no_manager_warning   => l_org_now_no_manager_warning
                                                   ,p_other_manager_warning        => l_other_manager_warning
                                                   ,p_spp_delete_warning           => l_spp_delete_warning
                                                   ,p_entries_changed_warning      => l_entries_changed_warning
                                                   ,p_tax_district_changed_warning => l_tax_district_changed_warning
                                                   ,p_concatenated_segments        => l_concatenated_segments);
        
          l_msg := 'Call api update location on assignment_id - ' ||
                   l_assignment_id || ' New Location - ' ||
                   empl_load.location || ' Old Location - ' ||
                   l_location_id;
        
        EXCEPTION
          WHEN OTHERS THEN
          
            l_sql_hint    := '<BR>SELECT  ert.*<BR>' ||
                             'FROM    hr.hr_locations<BR>' ||
                             'WHERE   location_id = <BR>' ||
                             empl_load.location || '';
            l_body_footer := '</tr></table><br>Use the following SQL statment to get the location code.<BR>' ||
                             l_sql_hint; --Version 2.1
          
            l_msg  := empl_load.employee_number ||
                      ' - Emp#   Assignment_ID - ' || l_assignment_id ||
                      ' - Error in hr_assignment_api.update_emp_asg_criteria with location_id = ' ||
                      l_location_id;
            l_body := l_msg || l_body_footer;
            debug('L', l_msg);
            RAISE empl_location_error;
        END;
      
      END IF;
    
    END LOOP;
  
  EXCEPTION
    WHEN empl_location_error THEN
    
      xxcus_misc_pkg.html_email(p_to            => g_distro_list
                               ,p_from          => l_sender
                               ,p_text          => 'test'
                               ,p_subject       => '***Update White Cap Employee Location***'
                               ,p_html          => l_body
                               ,p_smtp_hostname => g_host
                               ,p_smtp_portnum  => g_hostport);
    
    WHEN program_error THEN
      ROLLBACK;
      l_error_code    := 2;
      l_err_callpoint := l_msg;
      l_err_msg       := ' Error_Stack...' ||
                         dbms_utility.format_error_stack() ||
                         ' Error_Backtrace...' ||
                         dbms_utility.format_error_backtrace();
      debug('L', l_msg);
      debug('L', l_err_msg);
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'HR');
    
    WHEN OTHERS THEN
      l_error_code    := 2;
      l_err_callpoint := l_msg;
      l_err_msg       := ' Error_Stack...' ||
                         dbms_utility.format_error_stack() ||
                         ' Error_Backtrace...' ||
                         dbms_utility.format_error_backtrace();
      debug('L', l_msg);
      debug('L', l_err_msg);
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom || '.' ||
                                                                  l_procedure
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'HR');
  END update_emp_location;

  -- ************************* PROCEDURE***********************************************************
  --  This is used to update the users fax number.  A SSIS package was created that will load data  
  --  from the AD+ view "vw_RF_800Numbers" to the Oracle staging table xxcus.xxcushr_emp_fax_tbl 
  --  White Cap uses the fax number from FND_USER on the purchase orders.  If the fax is null it 
  --  will get the fax number from the branch.  Concurrent program "XXCUS HR: Update User Fax Numbers"
  --  created and included with the request set for the HR Interface
  -- 
  --   VERSION DATE          AUTHOR(S)       DESCRIPTION
  -- ------- -----------   --------------- -------------------------------------------------------
  -- 2.0     22-May-2014   Kathy Poling    Initial creation
  --                                       SR 201993 Task ID: 20130415-00545 for White Cap need
  --                                       to get the fax number for employees and update the fnd user 
  -- --------------------------------------------------------------------------------------------- 

  PROCEDURE update_user_fax(p_errbuf OUT VARCHAR2, p_retcode OUT NUMBER) IS
  
    -- Variables
    l_err_msg     VARCHAR2(4000);
    l_sec         VARCHAR2(110);
    l_module      VARCHAR2(24) := 'HR';
    l_body        VARCHAR2(32767);
    l_body_footer VARCHAR2(32767);
    l_sender      VARCHAR2(100);
    l_fax         VARCHAR2(50);
  
    --exceptions
    user_fax_error EXCEPTION;
  
  BEGIN
  
    --Get Database
    SELECT lower(NAME) INTO g_sid FROM v$database;
  
    l_sender := 'Oracle.Applications_' || g_sid || '@hdsupply.com';
  
    l_sec := 'Start loop for updating Users Fax Numbers';
  
    FOR cur_fax IN (SELECT u.user_name
                          ,u.fax
                          ,ps.last_name || ', ' || ps.first_name || ' ' ||
                           middle_names employee_name
                          ,u.user_guid
                          ,fax.fax_number
                          ,length(fax.fax_number) length
                          ,regexp_replace(fax_number
                                         ,'(\d{3})(\d{3})(\d{4})'
                                         ,'(\1) \2-\3') formatted_fax
                      FROM xxcus.xxcushr_ps_emp_all_tbl ps
                          ,apps.fnd_user                u
                          ,xxcus.xxcushr_emp_fax_tbl    fax
                     WHERE ps.process_flag = 'A'
                       AND upper(ps.nt_id) = u.user_name
                       AND upper(fax.nt_login) = u.user_name
                       AND nvl(u.fax, 'X') <>
                           regexp_replace(fax_number
                                         ,'(\d{3})(\d{3})(\d{4})'
                                         ,'(\1) \2-\3'))
    LOOP
      --want to make sure fax has a length of 7 or more don't want to make any change in Oracle  
      IF cur_fax.fax_number >= 7
      THEN
        --this will null the user_guid because it will cause the update user api to fail
        IF cur_fax.user_guid IS NOT NULL
        THEN
          update_user_guid(cur_fax.user_name);
        END IF;
        --This will format the fax number (xxx) xxx-xxxx
        IF cur_fax.length = 10
        THEN
          l_fax := cur_fax.formatted_fax;
        ELSE
          l_fax := cur_fax.fax_number;
        END IF;
      
        BEGIN
          l_sec := 'Updating User Name - ' || cur_fax.user_name || 'Fax - ' ||
                   l_fax;
        
          fnd_user_pkg.updateuser(x_user_name => cur_fax.user_name
                                 ,x_owner     => 'CUST'
                                 ,x_fax       => l_fax);
        EXCEPTION
          WHEN OTHERS THEN
          
            l_sec  := '<BR>Please update the follower User fax number manually.' ||
                      '<BR>You may use the Users or the Users Edit Fax form in E-Business Suite (EBS) to update the fax number.' ||
                      '<BR>Employee:  ' || cur_fax.employee_name ||
                      '<BR>User name:  ' || cur_fax.user_name ||
                      '<BR>AD+ fax number:  ' || cur_fax.formatted_fax;
            l_body := l_sec;
          
            fnd_file.put_line(fnd_file.log, 'Error in ' || l_sec);
            RAISE user_fax_error;
        END;
      END IF;
    END LOOP;
  EXCEPTION
    WHEN user_fax_error THEN
      l_err_msg := 'Email sent to update user manually for: ' || l_sec;
      fnd_file.put_line(fnd_file.log, l_err_msg);
    
      xxcus_misc_pkg.html_email(p_to            => 'HDSWCOracleSupport@hdsupply.com'
                               ,p_from          => l_sender
                               ,p_text          => 'test'
                               ,p_subject       => '***Update User Fax Number Manually***'
                               ,p_html          => l_body
                               ,p_smtp_hostname => g_host
                               ,p_smtp_portnum  => g_hostport);
    
    WHEN program_error THEN
      ROLLBACK;
      l_err_msg := l_sec || ' ...Error_Stack... ' ||
                   dbms_utility.format_error_stack() ||
                   ' Error_Backtrace...' ||
                   dbms_utility.format_error_backtrace();
      fnd_file.put_line(fnd_file.log, l_err_msg);
      l_err_callpoint := l_sec;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => substr(' Error_Stack...' ||
                                                                         dbms_utility.format_error_stack() ||
                                                                         ' Error_Backtrace...' ||
                                                                         dbms_utility.format_error_backtrace()
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => l_module);
    
      p_retcode := 2;
      p_errbuf  := l_err_msg;
    
    WHEN OTHERS THEN
      l_err_msg := l_sec;
      l_err_msg := l_sec || ' ...Error_Stack...' ||
                   dbms_utility.format_error_stack() ||
                   ' Error_Backtrace...' ||
                   dbms_utility.format_error_backtrace();
      fnd_file.put_line(fnd_file.log, l_err_msg);
      l_err_callpoint := l_sec;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => substr(' Error_Stack...' ||
                                                                         dbms_utility.format_error_stack() ||
                                                                         ' Error_Backtrace...' ||
                                                                         dbms_utility.format_error_backtrace()
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => l_module);
    
      p_retcode := 2;
      p_errbuf  := l_err_msg;
    
  END update_user_fax;

  /************************* PROCEDURE***********************************************************
  --  This is used to remove position and job from terminated employees.  Employee must be a buyer
  --  and process will use the last day paid.  Added a step to end date the buyer record also.
  --  Concurrent request 'HDS HR Employee Remove Position'
  --  
  --   VERSION DATE          AUTHOR(S)       DESCRIPTION
  -- ------- -----------   --------------- -------------------------------------------------------
  -- 3.0     30-Oct-2014   Kathy Poling    Initial creation
  --                                       TMS 20140925-00212
  --                                          
  -- ---------------------------------------------------------------------------------------------*/

  PROCEDURE update_emp_position(p_errbuf  OUT VARCHAR2
                               ,p_retcode OUT NUMBER) IS
  
    -- Variables
    l_module      VARCHAR2(24) := 'HR';
    l_err_msg     CLOB;
    l_sec         VARCHAR2(110) DEFAULT 'START';
    l_body        VARCHAR2(32767);
    l_body_footer VARCHAR2(32767);
    l_sql_hint    VARCHAR2(32767);
    l_sender      VARCHAR2(100);
    l_msg         VARCHAR2(500);
  
    l_procedure            VARCHAR2(30) := 'UPDATE_EMP_POSITION';
    l_effective_start_date DATE;
    l_effective_end_date   DATE;
    l_date_track_update_mode       VARCHAR2(100);
    l_entries_changed_warning      VARCHAR2(100) := NULL;
    l_group_name                   VARCHAR2(100);
    l_org_now_no_manager_warning   BOOLEAN;
    l_spp_delete_warning           BOOLEAN;
    l_tax_district_changed_warning BOOLEAN;
    l_people_group_id              NUMBER := hr_api.g_number;
    l_special_ceiling_step_id      NUMBER;
    l_date_update_track            VARCHAR2(20);
    l_soft_coding_keyflex_id       NUMBER;
    l_concatenated_segments        VARCHAR2(255);
    l_other_manager_warning BOOLEAN;
  
    --exceptions
    empl_position_error EXCEPTION;
  
  BEGIN
    --Get Database
    SELECT lower(NAME) INTO g_sid FROM v$database;
  
    l_sender := 'Oracle.Applications_' || g_sid || '@hdsupply.com';
  
    FOR cur_position IN (SELECT ps.employee_number
                               ,ps.last_name
                               ,ps.first_name
                               ,ps.middle_names
                               ,ps.hire_date
                               ,ppa.effective_end_date
                               ,ps.last_day_paid
                               ,ppe.person_id
                               ,ppa.assignment_id
                               ,ppa.object_version_number
                               ,ppa.location_id
                               ,pa.end_date_active
                           FROM xxcus.xxcushr_ps_emp_all_tbl ps
                               ,per_people_f                 ppe
                               ,per_all_assignments_f        ppa
                               ,po_agents                    pa
                          WHERE process_lev IN
                                (SELECT meaning
                                   FROM apps.fnd_lookup_values b
                                  WHERE b.lookup_type = 'HDS_HR-INT PS-ORA'
                                    AND b.enabled_flag = 'Y'
                                    AND nvl(b.end_date_active, SYSDATE) >=
                                        SYSDATE
                                    AND meaning = process_lev
                                    AND b.attribute_category =
                                        'XXCUS_HR_INTERFACE'
                                    AND b.attribute1 = 'Y')
                            AND process_flag = 'I'
                            AND ps.employee_number = ppe.employee_number
                            AND ppa.person_id = ppe.person_id
                            AND ppe.person_type_id = 6 --'Employee'
                            AND ppe.effective_end_date <> '31-DEC-4712'
                            AND position_id IS NOT NULL
                            AND ppe.person_id = pa.agent_id
                            FOR UPDATE OF pa.end_date_active skip locked)
    LOOP
      IF cur_position.effective_end_date > SYSDATE
      THEN
      
        l_effective_start_date   := NULL;
        l_effective_end_date     := NULL;
        l_soft_coding_keyflex_id := NULL;
        l_concatenated_segments  := NULL;
        l_other_manager_warning := NULL;
        l_date_update_track     := 'CORRECTION';
      
        BEGIN
        
          hr_assignment_api.update_emp_asg_criteria(p_effective_date               => trunc(SYSDATE)
                                                   ,p_datetrack_update_mode        => l_date_update_track
                                                   ,p_assignment_id                => cur_position.assignment_id
                                                   ,p_validate                     => FALSE
                                                   ,p_called_from_mass_update      => FALSE
                                                   ,p_grade_id                     => NULL
                                                   ,p_position_id                  => NULL
                                                   ,p_job_id                       => NULL
                                                   ,p_payroll_id                   => NULL
                                                   ,p_location_id                  => NULL 
                                                   ,p_organization_id              => 0
                                                   ,p_pay_basis_id                 => NULL
                                                   ,p_segment1                     => NULL
                                                   ,p_segment2                     => NULL
                                                   ,p_segment3                     => NULL
                                                   ,p_segment4                     => NULL
                                                   ,p_segment5                     => NULL
                                                   ,p_segment6                     => NULL
                                                   ,p_segment7                     => NULL
                                                   ,p_segment8                     => NULL
                                                   ,p_segment9                     => NULL
                                                   ,p_segment10                    => NULL
                                                   ,p_segment11                    => NULL
                                                   ,p_segment12                    => NULL
                                                   ,p_segment13                    => NULL
                                                   ,p_segment14                    => NULL
                                                   ,p_segment15                    => NULL
                                                   ,p_segment16                    => NULL
                                                   ,p_segment17                    => NULL
                                                   ,p_segment18                    => NULL
                                                   ,p_segment19                    => NULL
                                                   ,p_segment20                    => NULL
                                                   ,p_segment21                    => NULL
                                                   ,p_segment22                    => NULL
                                                   ,p_segment23                    => NULL
                                                   ,p_segment24                    => NULL
                                                   ,p_segment25                    => NULL
                                                   ,p_segment26                    => NULL
                                                   ,p_segment27                    => NULL
                                                   ,p_segment28                    => NULL
                                                   ,p_segment29                    => NULL
                                                   ,p_segment30                    => NULL
                                                   ,p_employment_category          => NULL
                                                   ,p_concat_segments              => NULL
                                                   ,p_contract_id                  => NULL
                                                   ,p_establishment_id             => NULL
                                                   ,p_scl_segment1                 => NULL
                                                   ,p_grade_ladder_pgm_id          => NULL
                                                   ,p_supervisor_assignment_id     => NULL
                                                   ,p_object_version_number        => cur_position.object_version_number
                                                   ,p_special_ceiling_step_id      => l_special_ceiling_step_id
                                                   ,p_people_group_id              => l_people_group_id
                                                   ,p_soft_coding_keyflex_id       => l_soft_coding_keyflex_id
                                                   ,p_group_name                   => l_group_name
                                                   ,p_effective_start_date         => l_effective_start_date
                                                   ,p_effective_end_date           => l_effective_end_date
                                                   ,p_org_now_no_manager_warning   => l_org_now_no_manager_warning
                                                   ,p_other_manager_warning        => l_other_manager_warning
                                                   ,p_spp_delete_warning           => l_spp_delete_warning
                                                   ,p_entries_changed_warning      => l_entries_changed_warning
                                                   ,p_tax_district_changed_warning => l_tax_district_changed_warning
                                                   ,p_concatenated_segments        => l_concatenated_segments);
        
          l_err_msg := 'Call api update location on assignment_id - ' ||
                       cur_position.assignment_id;
        
        EXCEPTION
          WHEN OTHERS THEN
          
            l_msg  := 'Employee Number - ' || cur_position.employee_number ||
                      chr(10) ||
                      ' Error in the api trying to remove the Position and Job.  Need to manually remove from employee ' ||
                      cur_position.first_name || ' ' ||
                      cur_position.last_name;
            l_body := l_msg || l_body_footer;
            debug('L', l_msg);
            RAISE empl_position_error;
        END;
      
        IF cur_position.end_date_active IS NULL
        THEN
        
          l_sec := 'Updating end date in po_agents for person_id = ' ||
                   cur_position.person_id;
        
          UPDATE po_agents
             SET end_date_active = trunc(SYSDATE)
           WHERE agent_id = cur_position.person_id;
        END IF;
      
      END IF;
    END LOOP;
  
  EXCEPTION
    WHEN empl_position_error THEN
    
      xxcus_misc_pkg.html_email(p_to            => g_distro_list
                               ,p_from          => l_sender
                               ,p_text          => 'test'
                               ,p_subject       => '***Need to manually remove Employee Position***'
                               ,p_html          => l_body
                               ,p_smtp_hostname => g_host
                               ,p_smtp_portnum  => g_hostport);
    
    WHEN program_error THEN
      ROLLBACK;
      l_err_msg := l_sec || ' ...Error_Stack... ' ||
                   dbms_utility.format_error_stack() ||
                   ' Error_Backtrace...' ||
                   dbms_utility.format_error_backtrace();
      fnd_file.put_line(fnd_file.log, l_err_msg);
      l_err_callpoint := l_sec;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => substr(' Error_Stack...' ||
                                                                         dbms_utility.format_error_stack() ||
                                                                         ' Error_Backtrace...' ||
                                                                         dbms_utility.format_error_backtrace()
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => l_module);
    
      p_retcode := 2;
      p_errbuf  := l_err_msg;
    
    WHEN OTHERS THEN
      l_err_msg := l_sec;
      l_err_msg := l_sec || ' ...Error_Stack...' ||
                   dbms_utility.format_error_stack() ||
                   ' Error_Backtrace...' ||
                   dbms_utility.format_error_backtrace();
      fnd_file.put_line(fnd_file.log, l_err_msg);
      l_err_callpoint := l_sec;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => substr(' Error_Stack...' ||
                                                                         dbms_utility.format_error_stack() ||
                                                                         ' Error_Backtrace...' ||
                                                                         dbms_utility.format_error_backtrace()
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => l_module);
    
      p_retcode := 2;
      p_errbuf  := l_err_msg;
    
  END update_emp_position;

END xxcushr_interface_pkg;
/
