CREATE OR REPLACE PACKAGE BODY apps.XXWC_INV_DEMAND_HISTORY_PKG
IS
   /*************************************************************************
     $Header XXWC_INV_DEMAND_HISTORY_PKG $
     Module Name: XXWC_INV_DEMAND_HISTORY_PKG

     PURPOSE: To update mtl_demand_histories for specific transactions defined
              in custom table xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES.
              Any new Additional transactions types should be inserted into xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES
              To avoid to update multiple times table xxwc.XXWC_MTL_DEMAND_HIST_LAST_RUN and xxwc.XXWC_MTL_DEMAND_HISTORY_TXN  will keep history


     REVISIONS:
     Ver        Date        Author                Description
     ---------  ----------  ------------------    ----------------
     1.0        04/29/2014  Rasikha Galimova      Initial Version TMS # 20140203-00283
     1.1        07/07/2014  Manjula Chellappan    Added Headers and Exceptions.
                                                  Updated the Table Names and Varibles as per coding standards
     1.2        03/06/2015  Manjula Chellappan    TMS # 20150306-00023 Add only one record to XXWC_MTL_DEMAND_HIST_LAST_RUN
                                                  when new brach created
   **************************************************************************/

   v_message   CLOB;

   PROCEDURE drop_table (p_owner VARCHAR2, p_table_name VARCHAR2)
   IS
      /*************************************************************************
        $Header drop_table $
        Module Name: drop_table

        PURPOSE: Local Procedure to drop the temporary tables

        REVISIONS:
        Ver        Date        Author                Description
        ---------  ----------  ------------------    ----------------
        1.0        04/29/2014  Rasikha Galimova      Initial Version TMS # 20140203-00283
      **************************************************************************/
      l_ddl_string   VARCHAR2 (1024);
   BEGIN
      FOR cur_table
         IN (SELECT object_name, owner
               FROM all_objects
              WHERE     object_name = UPPER (TRIM (p_table_name))
                    AND object_type = 'TABLE'
                    AND owner = UPPER (TRIM (p_owner)))
      LOOP
         l_ddl_string :=
            'drop table ' || cur_table.owner || '.' || cur_table.object_name;

         EXECUTE IMMEDIATE l_ddl_string;
      END LOOP;
   EXCEPTION
      WHEN OTHERS
      THEN
         NULL;
   END;

   --*******************************************************************
   --*******************************************************************

   --*******************************************************************
   --*******************************************************************
   PROCEDURE get_demand_history (p_period_type             IN     VARCHAR2,
                                 p_organization_id         IN     VARCHAR2,
                                 p_specific_category_set   IN     VARCHAR2,
                                 p_error_msg                  OUT CLOB)
   /*************************************************************************
       $Header correct_demand_history $
       Module Name: correct_demand_history#

       PURPOSE: Procedure to correct the Demand History based on the additional transactions

       REVISIONS:
       Ver        Date        Author                Description
       ---------  ----------  ------------------    ----------------
       1.0        04/29/2014  Rasikha Galimova      Initial Version TMS # 20140203-00283
       1.1        07/07/2014  Manjula Chellappan    Added Headers and Exceptions.
       1.2        03/06/2015  Manjula Chellappan    TMS # 20150306-00023 Add only one record to XXWC_MTL_DEMAND_HIST_LAST_RUN
                                                  when new brach created	   
     **************************************************************************/

   IS
      l_j_period_start_date     NUMBER;
      l_req_id                  NUMBER := fnd_global.conc_request_id;
      l_org_id                  NUMBER := mo_global.get_current_org_id;
      l_last_j_date             NUMBER;
      l_specific_category_set   VARCHAR2 (64);
      l_sql                     CLOB;
      l_exit_exception          EXCEPTION;
      l_count                   NUMBER := 0;
      l_last_run_date           NUMBER := 0;
   BEGIN
      drop_table ('XXWC', 'XXWC_DEMD_HISTORY_DATES##');

      EXECUTE IMMEDIATE
            'CREATE TABLE XXWC.XXWC_DEMD_HISTORY_DATES##
            AS
               SELECT TO_NUMBER (TO_CHAR (period_start_date, ''J'')) last_j_date
--Below line modified by Manjula on 21-Aug-14 to exclude the last day of the period 
--from the current selected period               
                  ,TO_NUMBER (TO_CHAR (next_date-1, ''J'')) prev_j_date
                  ,organization_id
                  ,period_start_date
              FROM bom_period_start_dates d, mtl_parameters p
             WHERE     p.organization_id = '
         || p_organization_id
         || '
                   AND p.calendar_code = d.calendar_code
                   AND d.exception_set_id = p.calendar_exception_set_id
                   AND d.period_start_date <= SYSDATE';

      COMMIT;

      FOR cur_last_run_date IN (SELECT last_run_date_j
                                  FROM xxwc.XXWC_MTL_DEMAND_HIST_LAST_RUN
                                 WHERE organization_id = p_organization_id)
      LOOP
         l_last_run_date := cur_last_run_date.last_run_date_j;
      END LOOP;

      IF l_last_run_date = 0
      THEN
         /* Added for Revision 1.2 Begin*/

         INSERT INTO XXWC.XXWC_MTL_DEMAND_HIST_LAST_RUN
                 VALUES (
                           p_organization_id,
                           TRUNC (SYSDATE) - 600,
                           TO_NUMBER (TO_CHAR (SYSDATE - 600, 'J')));
						   
		/* Added for Revision 1.2 end*/
		
      /* Commented for Revision 1.2
         INSERT INTO xxwc.XXWC_MTL_DEMAND_HIST_LAST_RUN
            SELECT p_organization_id,
                   SYSDATE - 600,
                   TO_NUMBER (TO_CHAR (SYSDATE - 600, 'J'))
              FROM mtl_parameters;
      */
         COMMIT;
      END IF;

      --*******************************************************************
      --*******************************************************************

      drop_table ('XXWC', 'XXWC_DEMD_HISTORY_ITEMS_TR1##');

      EXECUTE IMMEDIATE
         'CREATE TABLE xxwc.XXWC_DEMD_HISTORY_ITEMS_TR1##
                        (
                           transaction_id               NUMBER,
                           bucket_to_update             VARCHAR2 (124),
                           include_exclude_flag         VARCHAR2 (32),
                           transaction_date             NUMBER,
                           inventory_item_id            NUMBER,
                           transaction_source_type_id   NUMBER,
                           transaction_action_id        NUMBER,
                           transaction_type_id          NUMBER,
                           primary_quantity             NUMBER,
                           ol_source_type_code          VARCHAR2 (124),
                           source_line_id               NUMBER,
                           organization_id              NUMBER,
                           insert_date                  DATE,
                           vsql                         VARCHAR2 (512)
                )';

      EXECUTE IMMEDIATE
         'alter table xxwc.XXWC_DEMD_HISTORY_ITEMS_TR1## nologging';

      drop_table ('XXWC', 'xxwc_demd_history_items_tr##');

      EXECUTE IMMEDIATE
         ' create table xxwc.xxwc_demd_history_items_tr## as select * from xxwc.XXWC_DEMD_HISTORY_ITEMS_TR1## where 1=2';

      EXECUTE IMMEDIATE
         'alter table xxwc.xxwc_demd_history_items_tr## nologging';

      --*******************************************************************
      --*******************************************************************
      -- all external sales orders to exclude
      --*******************************************************************
      EXECUTE IMMEDIATE
            'INSERT /*+append*/
                      INTO  xxwc.XXWC_DEMD_HISTORY_ITEMS_TR1##
                   SELECT m.transaction_id,
                          x.bucket_to_update,
                          x.include_exclude_flag,
                          TO_NUMBER (TO_CHAR (m.transaction_date, ''J''))
                      ,m.inventory_item_id
                      ,m.transaction_source_type_id
                      ,m.transaction_action_id
                      ,m.transaction_type_id
                      , ( (-1) * m.primary_quantity)
                      ,''S''
                      ,m.source_line_id
                      ,m.organization_id
                      ,sysdate
                      ,null
                  FROM mtl_material_transactions m, xxwc.XXWC_MTL_DEMAND_HIST_LAST_RUN z, xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES x
                 WHERE     m.transaction_source_type_id IN (2)                                                             --Sales order
                       AND m.organization_id = '
         || p_organization_id
         || '
                       AND m.organization_id = z.organization_id
                       AND m.primary_quantity < 0
                       AND x.transaction_sign = ''negative''
                       AND m.transaction_date BETWEEN z.last_run_date AND SYSDATE
                       AND m.transaction_date >= z.last_run_date
                       AND x.transaction_source_type_id = m.transaction_source_type_id
                       AND x.transaction_type_id = m.transaction_type_id
                       AND x.add_information IS NOT NULL
                       and not exists (select 1 from  xxwc.XXWC_MTL_DEMAND_HISTORY_TXN  n where n.transaction_id=m.transaction_id)';

      COMMIT;

      l_sql :=
         ' INSERT /*+append*/
             INTO  xxwc.xxwc_demd_history_items_tr##
           SELECT *
             FROM xxwc.XXWC_DEMD_HISTORY_ITEMS_TR1## m
            WHERE EXISTS
                     (SELECT line_id
                        FROM oe_order_lines_all ol
                       WHERE     ol.line_id = m.source_line_id
                             AND NVL (ol.source_type_code, ''V'') = ''EXTERNAL'')
                   AND m.ol_source_type_code = ''S''';

      -- DBMS_OUTPUT.put_line (l_sql);
      -- RAISE l_exit_exception;
      EXECUTE IMMEDIATE l_sql;

      COMMIT;

      drop_table ('XXWC', 'XXWC_DEMD_HISTORY_ITEMS_TR1##');

      --*******************************************************************
      --*******************************************************************
      --*******************************************************************

      --*******************************************************************
      --*******************************************************************
      --*******************************************************************
      EXECUTE IMMEDIATE
            'INSERT /*+append*/
            INTO  xxwc.xxwc_demd_history_items_tr##
          SELECT m.transaction_id,x.bucket_to_update
              ,x.include_exclude_flag
              ,TO_NUMBER (TO_CHAR (m.transaction_date, ''J''))
              ,m.inventory_item_id
              ,m.transaction_source_type_id
              ,m.transaction_action_id
              ,m.transaction_type_id
              , ( (-1) * m.primary_quantity)
              ,transaction_source_type_name || '' '' || transaction_type_name
              ,m.source_line_id
              ,m.organization_id
              ,sysdate
              ,null
          FROM mtl_material_transactions m, xxwc.XXWC_MTL_DEMAND_HIST_LAST_RUN z, xxwc.XXWC_MTL_DEMAND_HISTORY_TYPES x
         WHERE     m.organization_id = '
         || p_organization_id
         || '
               AND m.organization_id = z.organization_id
               AND (   (m.primary_quantity > 0 AND x.transaction_sign = ''positive'')
                    OR (m.primary_quantity < 0 AND x.transaction_sign = ''negative''))
               AND m.transaction_date BETWEEN z.last_run_date AND SYSDATE
               AND m.transaction_date >= z.last_run_date
               AND x.transaction_source_type_id = m.transaction_source_type_id
               AND x.transaction_type_id = m.transaction_type_id
               AND x.add_information IS NULL
               AND x.include_exclude_flag IS NOT NULL
               and not exists (select 1 from  xxwc.XXWC_MTL_DEMAND_HISTORY_TXN  n where n.transaction_id=m.transaction_id)';

      COMMIT;

      --*******************************************************************

      --*******************************************************************
      --*******************************************************************
      --*******************************************************************
      l_specific_category_set := p_specific_category_set;

      IF l_specific_category_set IS NULL
      THEN
         SELECT category_set_id
           INTO l_specific_category_set
           FROM mtl_default_sets_view
          WHERE functional_area_id = 1;
      END IF;

      EXECUTE IMMEDIATE
            'DELETE FROM xxwc.xxwc_demd_history_items_tr##
                  WHERE ROWID IN (SELECT x.ROWID
                                    FROM xxwc.xxwc_demd_history_items_tr## x
                                   WHERE NOT EXISTS
                                                (SELECT 1
                                                   FROM mtl_item_categories
                                                  WHERE     organization_id =
                                                               x.organization_id
                                                        AND inventory_item_id =
                                                               x.inventory_item_id
                                                        AND category_set_id =
                                                               '
         || l_specific_category_set
         || '))';

      COMMIT;

      EXECUTE IMMEDIATE
         'DECLARE
               l_sql   CLOB;
            BEGIN
                            FOR cur_xxwc_demd_hist IN (SELECT a.*
                                            ,last_j_date
                                            ,prev_j_date
                                            ,period_start_date
                                            ,DECODE (a.include_exclude_flag, ''exclude'', ''-1'', ''1'') signr
                                            ,a.ROWID browid
                                        FROM xxwc.xxwc_demd_history_items_tr## a, xxwc.XXWC_DEMD_HISTORY_DATES## b
                                       WHERE a.transaction_date BETWEEN last_j_date AND prev_j_date AND a.organization_id = b.organization_id)
                            LOOP
                                FOR cur_demd_hist
                                    IN (SELECT ROWID vrowid, a.period_start_date
                                          FROM  mtl_demand_histories a
                                         WHERE     a.inventory_item_id = cur_xxwc_demd_hist.inventory_item_id
                                               AND a.period_type = 3
                                               AND a.organization_id = cur_xxwc_demd_hist.organization_id
                                               AND TO_NUMBER (TO_CHAR (a.period_start_date, ''J'')) = cur_xxwc_demd_hist.last_j_date)
                                LOOP
                                    l_sql :=
                                           '' update mtl_demand_histories set ''
                                        || TRIM (cur_xxwc_demd_hist.bucket_to_update)
                                        || ''=''
                                        || TRIM (cur_xxwc_demd_hist.bucket_to_update)
                                        || ''+''
                                        || cur_xxwc_demd_hist.signr
                                                        || ''*''
                                                        || cur_xxwc_demd_hist.primary_quantity
                                                        || '' where rowid =''''''
                                                        || cur_demd_hist.vrowid
                                                        || '''''''';

                                                    --   dbms_output.put_line (l_sql);
                                                    UPDATE xxwc.xxwc_demd_history_items_tr##
                                                       SET vsql = l_sql
                                                     WHERE ROWID = cur_xxwc_demd_hist.browid;
                                                END LOOP;
                                            END LOOP;

                                            COMMIT;
            END;';

      BEGIN
         EXECUTE IMMEDIATE
            ' BEGIN
                FOR cur_txn IN (SELECT vsql, transaction_id
                            FROM xxwc.xxwc_demd_history_items_tr## a where vsql is not null )
                LOOP
                     EXECUTE IMMEDIATE cur_txn.vsql;
                     EXECUTE IMMEDIATE  ''insert into xxwc.XXWC_MTL_DEMAND_HISTORY_TXN  values(''||cur_txn.transaction_id||'')'' ;
                END LOOP;

                COMMIT;
            END;';
      END;

      --save history
      EXECUTE IMMEDIATE
         'alter table xxwc.XXWC_MTL_DEMAND_HISTORIES nologging';

      EXECUTE IMMEDIATE
         'INSERT /*+ append*/
              INTO  xxwc.XXWC_MTL_DEMAND_HISTORIES
            SELECT * FROM xxwc.xxwc_demd_history_items_tr## where vsql is not null';

      COMMIT;
      drop_table ('XXWC', 'xxwc_demd_history_items_tr##');

      UPDATE xxwc.XXWC_MTL_DEMAND_HIST_LAST_RUN
         SET last_run_date = SYSDATE - 60,
             last_run_date_j = TO_NUMBER (TO_CHAR (SYSDATE - 60, 'J'))
       WHERE organization_id = p_organization_id;

      COMMIT;
   EXCEPTION
      WHEN l_exit_exception
      THEN
         DBMS_OUTPUT.put_line (v_message);
      WHEN OTHERS
      THEN
         v_message :=
               'xxwc_arexti '
            || 'Error_Stack...'
            || DBMS_UTILITY.format_error_stack ()
            || ' Error_Backtrace...'
            || DBMS_UTILITY.format_error_backtrace ();
         DBMS_OUTPUT.put_line (v_message);
         p_error_msg := v_message;
         RAISE;
   END;


   --*******************************************************************
   --*******************************************************************


   PROCEDURE correct_demand_history (
      p_errbuf               OUT VARCHAR2,
      p_retcode              OUT NUMBER,
      p_organization_id   IN     VARCHAR2 DEFAULT NULL)
   IS
      /*************************************************************************
          $Header correct_demand_history $
          Module Name: correct_demand_history#

          PURPOSE: Procedure to correct the Demand History based on the additional transactions

          REVISIONS:
          Ver        Date        Author                Description
          ---------  ----------  ------------------    ----------------
          1.0        04/29/2014  Rasikha Galimova      Initial Version TMS # 20140203-00283
          1.1        07/07/2014  Manjula Chellappan    Added Header and Exceptions.
        **************************************************************************/

      l_sec         VARCHAR2 (500);
      l_error_msg   CLOB;
   BEGIN
      p_retcode := 0;

      FOR cur_org
         IN (SELECT organization_id
               FROM org_organization_definitions
              WHERE     organization_id <> 222
                    AND operating_unit = 162
                    AND NVL (disable_date, SYSDATE + 1) > SYSDATE
                    AND organization_id =
                           NVL (p_organization_id, organization_id))
      LOOP
         l_sec :=
               'Getting Demand History for Branch Id '
            || cur_org.organization_id;

         get_demand_history ('3',
                             p_organization_id         => cur_org.organization_id,
                             p_specific_category_set   => '1100000062',
                             p_error_msg               => l_error_msg);
      END LOOP;
   EXCEPTION
      WHEN OTHERS
      THEN
         v_message :=
               'xxwc_arexti '
            || 'Error_Stack...'
            || DBMS_UTILITY.format_error_stack ()
            || ' Error_Backtrace...'
            || DBMS_UTILITY.format_error_backtrace ();
         DBMS_OUTPUT.put_line (v_message);


         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_INV_DEMAND_HOSTORY_PKG.CORRECT_DEMAND_HISTORY',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (l_error_msg, 1, 2000),
            p_error_desc          => SUBSTR (v_message || ' ' || SQLERRM, 1, 240),
            p_distribution_list   => g_distribution_list,
            p_module              => 'INV');

         p_retcode := 2;
         p_errbuf := v_message;
   END;
--*******************************************************************
--*******************************************************************

END;
/