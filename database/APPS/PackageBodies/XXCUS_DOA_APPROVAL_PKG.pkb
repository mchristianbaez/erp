CREATE OR REPLACE PACKAGE BODY APPS.XXCUS_DOA_APPROVAL_PKG AS
/**************************************************************************
   $Header XXCUS_DOA_APPROVAL_PKG $
   Module Name: XXCUS_DOA_APPROVAL_PKG.pks

   PURPOSE:   This package is called by the concurrent programs
              XXCUS DOA Approval Automation program
              for updating the job name for per employees.
   REVISIONS:
   Ver        Date        Author             Description
   ---------  ----------  ---------------   -------------------------
    1.0       10/07/2018  Ashwin Sridhar    Initial Build - Task ID: 20180410-00199
/*************************************************************************/

g_loc VARCHAR2(20000);

/*************************************************************************
  Procedure : Write_Log

  PURPOSE:   This procedure logs debug message in Concurrent Log file
  Parameter: IN  p_debug_msg      -- Debug Message
  REVISIONS:
  Ver        Date        Author             Description
  ---------  ----------  ---------------   -------------------------
   1.0       10/07/2018  Ashwin Sridhar     Initial Build - Task ID: 20180410-00199
************************************************************************/
PROCEDURE write_log (p_debug_msg IN VARCHAR2) IS

BEGIN
      
  fnd_file.put_line (fnd_file.LOG, p_debug_msg);
  DBMS_OUTPUT.put_line (p_debug_msg);

END write_log;

/*************************************************************************
  Procedure : Write_Ouput

  PURPOSE:   This procedure logs the records in Concurrent Output file
  Parameter: IN  p_debug_msg      -- Debug Message
  REVISIONS:
  Ver        Date        Author             Description
  ---------  ----------  ---------------   -------------------------
   1.0       10/07/2018  Ashwin Sridhar     Initial Build - Task ID: 20180410-00199
************************************************************************/
PROCEDURE Write_Ouput (p_debug_msg IN VARCHAR2) IS

BEGIN
      
  fnd_file.put_line (fnd_file.OUTPUT, p_debug_msg);

END Write_Ouput;

/*************************************************************************
  Procedure : write_error

  PURPOSE:   This procedure logs debug message in Concurrent Out file
  Parameter: IN  p_debug_msg      -- Debug Message
  REVISIONS:
  Ver        Date        Author             Description
  ---------  ----------  ---------------   -------------------------
   1.0       10/07/2018  Ashwin Sridhar     Initial Build - Task ID: 20180410-00199
************************************************************************/
--Add message to concurrent output file
PROCEDURE write_error (p_debug_msg IN VARCHAR2) IS

l_req_id          NUMBER := fnd_global.conc_request_id;
l_err_callfrom    VARCHAR2 (75) DEFAULT 'XXCUS_CONCUR_EMP_PKG';
l_err_callpoint   VARCHAR2 (75) DEFAULT 'START';
l_distro_list     VARCHAR2 (75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';

BEGIN

  xxcus_error_pkg.xxcus_error_main_api (
      p_called_from         => l_err_callfrom,
      p_calling             => l_err_callpoint,
      p_request_id          => l_req_id,
      p_ora_error_msg       => SUBSTR (p_debug_msg, 1, 2000),
      p_error_desc          => 'Error running XXCUS_CONCUR_EMP_PKG with PROGRAM ERROR',
      p_distribution_list   => l_distro_list,
      p_module              => 'HRMS');

END write_error;

/*************************************************************************
  Procedure : UPDATE_JOB_NAME

  PURPOSE:   This procedure updates the job name for the given employee
  Parameter: IN  p_debug_msg      -- Debug Message
  REVISIONS:
  Ver        Date        Author             Description
  ---------  ----------  ---------------   -------------------------
  1.0        10/07/2018  Ashwin Sridhar    Initial Build - Task ID: 20180410-00199
************************************************************************/
PROCEDURE UPDATE_JOB_NAME(p_errbuf  OUT VARCHAR2
                         ,p_retcode OUT VARCHAR2) IS

l_err_msg  VARCHAR2(5000);
lv_success VARCHAR2(1);
ln_count   NUMBER:=0;

CURSOR cu_emp IS
SELECT XPEA.FIRST_NAME ||' '||XPEA.MIDDLE_NAMES||' '||XPEA.LAST_NAME EMP_NAME
,      XPEA.EMPLOYEE_NUMBER
,      XPEA.SUPERVISOR_ID
,      FLV.MEANING      JOB_DESCR
,      FLV.DESCRIPTION  DOLLAR_AMOUNT
,      PAPF.PERSON_ID
,      PAAF.ASSIGNMENT_ID
,      PAAF.EMPLOYEE_CATEGORY
,      PJ.JOB_ID    JOB_NEW
,      PAAF.JOB_ID  JOB_OLD
,      PAAF.OBJECT_VERSION_NUMBER
FROM   XXCUS.XXCUSHR_PS_EMP_ALL_TBL XPEA
,      FND_LOOKUP_VALUES_VL         FLV
,      PER_ALL_PEOPLE_F             PAPF
,      PER_ALL_ASSIGNMENTS_F        PAAF
,      PER_JOBS                     PJ
WHERE  1=1
AND    PAPF.EMPLOYEE_NUMBER = XPEA.EMPLOYEE_NUMBER
AND    SYSDATE BETWEEN PAPF.EFFECTIVE_START_DATE AND PAPF.EFFECTIVE_END_DATE
AND    PAPF.PERSON_ID       = PAAF.PERSON_ID
AND    SYSDATE BETWEEN PAAF.EFFECTIVE_START_DATE AND PAAF.EFFECTIVE_END_DATE
AND    PJ.NAME              = FLV.DESCRIPTION
AND    XPEA.JOB_DESCR       = FLV.MEANING
AND    FLV.ENABLED_FLAG     = 'Y'
AND    flv.lookup_type      = 'XXCUS_DOA_AUTO_APPROVAL_JOB';

BEGIN

  --Printing in Parameters
  g_loc := 'Program Start...  ';
  write_log (g_loc);
  write_log ('========================================================');

  --Initialize the Out Parameter
  p_errbuf := NULL;
  p_retcode := '0';
  
  Write_Ouput('Updated Records Summary:');
  Write_Ouput('========================');
  Write_Ouput(RPAD('EMP_NAME',50)||'|'||RPAD('EMPLOYEE NUMBER',30)||'|'||RPAD('JOB DESCR',50)
                                 ||'|'||RPAD('DOLLAR AMOUNT',30)||'|'||RPAD('JOB OLD',10)||'|'||RPAD('JOB NEW',10));
  
  write_log ('Before Starting the loop...');

  FOR rec_emp IN cu_emp LOOP
  
    write_log ('Inside loop for Employee No...'||rec_emp.EMPLOYEE_NUMBER);
  
    IF rec_emp.JOB_NEW!=NVL(rec_emp.JOB_OLD,0) THEN

      ln_count:=ln_count+1;

      Write_Ouput(RPAD(rec_emp.EMP_NAME,50)||'|'||RPAD(rec_emp.EMPLOYEE_NUMBER,30)||'|'||RPAD(rec_emp.JOB_DESCR,50)
                                           ||'|'||RPAD(rec_emp.DOLLAR_AMOUNT,30)||'|'||RPAD(NVL(rec_emp.JOB_OLD,''),10)||'|'||RPAD(rec_emp.JOB_NEW,10));
  
      CALL_HR_API(rec_emp.assignment_id
                 ,rec_emp.JOB_NEW
                 ,rec_emp.OBJECT_VERSION_NUMBER
                 ,lv_success);
      
    END IF;
  
  END LOOP;
  
  IF ln_count=0 THEN
  
    Write_Ouput('                                    ');
    Write_Ouput('***No Eligible Employee records available to update***');
  
  END IF;
  
  write_log ('XXCUS DOA Auto Approval Program - Successfully completed');

  --Initialize the Out Parameter
  p_errbuf := NULL;
  p_retcode := '0';

EXCEPTION
WHEN others THEN

   l_err_msg := 'Error Msg :' || SQLERRM;
   p_errbuf := l_err_msg;
   p_retcode := '2';
   write_error (l_err_msg);
   write_log (l_err_msg);

END UPDATE_JOB_NAME;

PROCEDURE CALL_HR_API(p_assignment_id IN NUMBER
                     ,p_job_id        IN NUMBER
                     ,p_object_ver    IN NUMBER
                     ,p_success       OUT VARCHAR2) IS

   -- Local Variables
   -- -----------------------
   lc_dt_ud_mode          VARCHAR2(100)    := NULL;
   ln_supervisor_id       NUMBER           := NULL;
   ln_object_number       NUMBER           := p_object_ver;
   ln_people_group_id     NUMBER           := hr_api.g_number;
 
   -- Out Variables for Find Date Track Mode API
   -- -----------------------------------------------------------------
   lb_correction             BOOLEAN;
   lb_update                 BOOLEAN;
   lb_update_override        BOOLEAN;
   lb_update_change_insert   BOOLEAN;
  
   -- Out Variables for Update Employee Assignment API
   -- ----------------------------------------------------------------------------
   ln_soft_coding_keyflex_id     HR_SOFT_CODING_KEYFLEX.SOFT_CODING_KEYFLEX_ID%TYPE;
   lc_concatenated_segments      VARCHAR2(2000);
   ln_comment_id                 PER_ALL_ASSIGNMENTS_F.COMMENT_ID%TYPE;
   lb_no_managers_warning        BOOLEAN;

   -- Out Variables for Update Employee Assignment Criteria
   -- -------------------------------------------------------------------------------
   ln_special_ceiling_step_id        PER_ALL_ASSIGNMENTS_F.SPECIAL_CEILING_STEP_ID%TYPE;
   lc_group_name                     VARCHAR2(30);
   ld_effective_start_date           PER_ALL_ASSIGNMENTS_F.EFFECTIVE_START_DATE%TYPE;
   ld_effective_end_date             PER_ALL_ASSIGNMENTS_F.EFFECTIVE_END_DATE%TYPE;
   lb_org_now_no_manager_warning     BOOLEAN;
   lb_other_manager_warning          BOOLEAN;
   lb_spp_delete_warning             BOOLEAN;
   lc_entries_changed_warning        VARCHAR2(30);
   lb_tax_district_changed_warn      BOOLEAN;

BEGIN

  write_log ('Inside CALL_HR_API...');
 
  -- Find Date Track Mode for the API 
  -- ------------------------------------------------------
  dt_api.find_dt_upd_modes
  (  p_effective_date          => TRUNC(SYSDATE),
     p_base_table_name         => 'PER_ALL_ASSIGNMENTS_F',
     p_base_key_column         => 'ASSIGNMENT_ID',
     p_base_key_value          => p_assignment_id,
     --Output data elements
     ---------------------------------
     p_correction              => lb_correction,
     p_update                  => lb_update,
     p_update_override         => lb_update_override,
     p_update_change_insert    => lb_update_change_insert
  );

  --Future dated changes - do insert then overrides the future record
  IF ( lb_update_override = TRUE OR lb_update_change_insert = TRUE )
  THEN
    
    --UPDATE_OVERRIDE
    ----------------------------------
    lc_dt_ud_mode := 'UPDATE_OVERRIDE';

  END IF;

  --Over writes the existing record, no history will be maintained
  IF ( lb_correction = TRUE )
  THEN
     -- CORRECTION
     -- ----------------------
     lc_dt_ud_mode := 'CORRECTION';

  END IF;

  --Inserts a new record effective as of the effective date parameter and keeps the history
  IF ( lb_update = TRUE )
  THEN
     -- UPDATE
     -- --------------
     lc_dt_ud_mode := 'UPDATE';

  END IF;

  write_log ('lc_dt_ud_mode..'||lc_dt_ud_mode);
  
 -- Update Employee Assignment Criteria
 -- -----------------------------------------------------
 hr_assignment_api.update_emp_asg_criteria
 ( -- Input data Elements
  -- ------------------------------
  p_effective_date                      => TRUNC(SYSDATE),
  p_datetrack_update_mode               => lc_dt_ud_mode,
  p_assignment_id                       => p_assignment_id,
  p_location_id                         => '',
  p_grade_id                            => '',
  p_job_id                              => p_job_id,
  p_payroll_id                          => '',
  p_organization_id                     => 0,
  p_employment_category                 => '',
  -- Output data Elements
  -- -------------------------------
  p_people_group_id                => ln_people_group_id,
  p_object_version_number          => ln_object_number,
  p_special_ceiling_step_id        => ln_special_ceiling_step_id,
  p_group_name                     => lc_group_name,
  p_effective_start_date           => ld_effective_start_date,
  p_effective_end_date             => ld_effective_end_date,
  p_org_now_no_manager_warning     => lb_org_now_no_manager_warning,
  p_other_manager_warning          => lb_other_manager_warning,
  p_spp_delete_warning             => lb_spp_delete_warning,
  p_entries_changed_warning        => lc_entries_changed_warning,
  p_tax_district_changed_warning   => lb_tax_district_changed_warn
 );
 
  p_success:='Y';
  
  write_log ('JOB Updated successfully for Assignment ID...'||p_assignment_id);
 
  COMMIT;
  
EXCEPTION
WHEN others THEN
            
  p_success:='N';
  write_log ('Error in updating JOB for Assignment ID...'||p_assignment_id||'-'||SQLERRM);
  ROLLBACK;
  
END CALL_HR_API;

END XXCUS_DOA_APPROVAL_PKG;
/