CREATE OR REPLACE PACKAGE BODY APPS.xxwc_om_poa_pkg
IS
   /*************************************************************************
   *   $Header xxwc_om_poa_pkg.pkb$
   *   Module Name: xxwc_om_poa_pkg.pkb
   *
   *   PURPOSE:   This package is used by the XXWC OM POA unapply prepayments conc program
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        09/06/2013  Shankar Hariharan        Initial Version
   * ***************************************************************************/
   PROCEDURE unapply_prepayment (errbuf OUT VARCHAR2, retcode OUT VARCHAR2)
   IS
      l_return_status   VARCHAR2 (1);
      l_msg_count       NUMBER;
      l_msg_data        VARCHAR2 (240);
      p_count           NUMBER := 0;

      CURSOR c1
      IS
         SELECT a.receipt_number,
                b.cash_receipt_id,
                b.receivable_application_id,
                a.receipt_date,
                b.amount_applied
           FROM ar_cash_receipts_all a, ar_receivable_applications_all b
          WHERE     a.cash_receipt_id = b.cash_receipt_id
                AND b.application_type = 'CASH'
                AND b.applied_payment_schedule_id = -7
                AND b.application_ref_type = 'OM'
                AND b.display = 'Y'
                AND a.creation_date > SYSDATE - 240
                --AND a.amount=b.amount_applied
                AND EXISTS
                       (SELECT 1
                          FROM oe_order_lines_all l
                         WHERE     l.header_id = b.application_ref_id
                               AND l.inventory_item_id = 2931195);
   BEGIN
    mo_global.init('AR');
    mo_global.set_policy_context('S',162);
      FOR c1_rec IN c1
      LOOP
         AR_RECEIPT_API_PUB.UNAPPLY_OTHER_ACCOUNT (
            p_api_version                 => 1.0,
            p_init_msg_list               => FND_API.G_TRUE,
            p_commit                      => FND_API.G_TRUE,
            p_validation_level            => FND_API.G_VALID_LEVEL_FULL,
            x_return_status               => l_return_status,
            x_msg_count                   => l_msg_count,
            x_msg_data                    => l_msg_data,
            p_cash_receipt_id             => c1_rec.cash_receipt_id,
            p_reversal_gl_date            => SYSDATE,
            p_receivable_application_id   => c1_rec.receivable_application_id,
            p_cancel_claim_flag           => 'N',
            p_called_from                 => 'PREPAYMENT');

         IF l_msg_count > 1
         THEN
            LOOP
               p_count := p_count + 1;
               l_msg_data :=
                  FND_MSG_PUB.Get (FND_MSG_PUB.G_NEXT, FND_API.G_FALSE);

               IF l_msg_data IS NULL
               THEN
                  EXIT;
               END IF;

               fnd_file.put_line (
                  fnd_file.output,
                     c1_rec.receipt_number||':'||c1_rec.receipt_date||':'||c1_rec.amount_applied
                  || ':'
                  || l_return_status
                  || ':'
                  || l_msg_data);
            END LOOP;
         ELSE
            fnd_file.put_line (
               fnd_file.output,
                  c1_rec.receipt_number||':'||c1_rec.receipt_date||':'||c1_rec.amount_applied
               || ':'
               || l_return_status
               || ':'
               || l_msg_data);
         END IF;
      END LOOP;
   END unapply_prepayment;
END xxwc_om_poa_pkg;
/