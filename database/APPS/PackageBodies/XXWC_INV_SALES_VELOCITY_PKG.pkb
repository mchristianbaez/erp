create or replace PACKAGE BODY      XXWC_INV_SALES_VELOCITY_PKG AS

   /*************************************************************************************************
   *   REVISIONS:                                                                                   *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   1.0        11/13/2014  Vijaysrinivasan           TMS#20141002-00048 Multi org changes        *
   * ************************************************************************************************/

    -- 12/26/12 CG: Added funtion to pull EiSR Sales Hits count
    
    procedure get_eisr_sales_hits (p_inventory_item_id IN NUMBER
                                , p_organization_id IN NUMBER
                                , x_sales_hits out NUMBER
                                , x_dc_sales_hits out NUMBER)                                
    is
        l_sales_hits    number;
        l_dc_sales_hits number;
    begin
    
        l_sales_hits    := 0;
        l_dc_sales_hits := 0;

        -- 06/10/2013 CG TMS 20130610-00668: NVL'd values to 0
        select  nvl(Hit4_store_sales,0) sales_hits
                , (nvl(Hit4_store_sales,0) + nvl(HIT4_OTHER_INV_SALES,0)) dc_sales_hits
        into    l_sales_hits      
                , l_dc_sales_hits         
        from    xxeis.EIS_XXWC_PO_ISR_TAB
        where   inventory_item_id = p_inventory_item_id
        and     organization_id = p_organization_id
        and     rownum = 1;
        
        x_sales_hits := l_sales_hits;
        x_dc_sales_hits := l_dc_sales_hits;
    exception
    when others then
        x_sales_hits := 0;
        x_dc_sales_hits := 0;
    end get_eisr_sales_hits;

    function get_annual_sales (p_inventory_item_id IN NUMBER
                                , p_organization_id IN NUMBER)
    return number
    IS
        l_eisr_annual_sale number;
    begin
        l_eisr_annual_sale := null;
        
        -- 02/19/2013 CG: TMS 20130218-00781. Point 2: Changed to include the monthly buckets and other inv monthly buckets
        select  (nvl(twelve_store_sale,0)
                + nvl(jan_store_sale,0)
                + nvl(feb_store_sale,0)
                + nvl(mar_store_sale,0)
                + nvl(apr_store_sale,0)
                + nvl(may_store_sale,0)
                + nvl(jun_store_sale,0)
                + nvl(jul_store_sale,0)
                + nvl(aug_store_sale,0)
                + nvl(sep_store_sale,0)
                + nvl(oct_store_sale,0)
                + nvl(nov_store_sale,0)
                + nvl(dec_store_sale,0)
                + nvl(jan_other_inv_sale,0)
                + nvl(feb_other_inv_sale,0)
                + nvl(mar_other_inv_sale,0)
                + nvl(apr_other_inv_sale,0)
                + nvl(may_other_inv_sale,0)
                + nvl(jun_other_inv_sale,0)
                + nvl(jul_other_inv_sale,0)
                + nvl(aug_other_inv_sale,0)
                + nvl(sep_other_inv_sale,0)
                + nvl(oct_other_inv_sale,0)
                + nvl(nov_other_inv_sale,0)
                + nvl(dec_other_inv_sale,0) )
        into    l_eisr_annual_sale     
        from    xxeis.EIS_XXWC_PO_ISR_TAB
        where   inventory_item_id = p_inventory_item_id
        and     organization_id = p_organization_id
        and     rownum = 1;
        
        return nvl(l_eisr_annual_sale, 0);
    exception
    when others then
        return 0;
    end get_annual_sales;
    
    -- Procedure will read all the Items assigned to a given
    Procedure  Populate_GT(P_Organization_ID Number) IS
       -- Define a Cursor that gets all the items for a given Oragnization ID
       -- Get Items list that are shippable.
       --
       CURSOR Get_Items_Cur ( p_Organization_Id Number ) Is
          SELECT MSI.INVENTORY_ITEM_ID,MSI.Segment1, msi.Organization_Id,
                msi.Inventory_Item_Status_Code, mic.Category_ID, MSI.Creation_Date, mp.Attribute7, mic.Category_Concat_Segments
                -- 02/19/2013 CG: TMS 20130218-00781. Point 3: To reclass NEW Items with 0/0 min/max
                , msi.min_minmax_quantity, msi.max_minmax_quantity
          FROM MTL_SYSTEM_ITEMS_B msi,
               MTL_PARAMETERS mp,
               XXWC_SV_ITEM_CATEGORIES_V mic
          WHERE msi.Organization_Id             = MP.organization_Id
          AND   mp.Organization_ID              = p_Organization_ID
          AND  ( (Msi.SHIPPABLE_ITEM_FLAG         = 'Y'
          AND   MSI.CUSTOMER_ORDER_ENABLED_FLAG = 'Y'
          AND   MSI.INVOICEABLE_ITEM_FLAG       = 'Y'
          AND   MSI.SO_TRANSACTIONS_FLAG        = 'Y')
            Or ( msi.Inventory_Item_Status_Code in (G_ITEM_STATUS_DISCONTINUED,G_ITEM_STATUS_NON_STOCK,
                                                    G_ITEM_STATUS_NOT_REPLENISHED)))
          AND   msi.Organization_Id             = mic.Organization_Id(+)
          AND   msi.Inventory_item_ID           = mic.Inventory_Item_ID(+)
          AND   mic.Category_Set_ID (+)         = G_CATEGORY_SET_ID
          --AND   MSI.SEGMENT1  = '137DIA37' --13423102'
          AND  Not Exists (
             Select Category_ID
            From  MTL_CATEGORY_SET_VALID_CATS_V mcsvc
            Where Category_Set_ID = G_CATEGORY_SET_ID
            AND   mcsvc.Category_ID = NVL(mic.Category_ID,0)
            And   mcsvc.CATEGORY_CONCAT_SEGMENTS in ( G_CATEGORY_DISCONTINUED,G_CATEGORY_NOT_REPLENISHED,G_CATEGORY_NON_STOCK,
                                                      --05/07/13 TMS 20130502-01289: CG Added to consider new SV Classes D and V to behave like Z and N
                                                      G_CATEGORY_WC_DISCONTINUED, G_CATEGORY_SUP_DISCONTINUED,
                                                      --05/07/13 TMS 20130502-01289
                                                      G_CATEGORY_NEW
                                                      --20140203-00255   Change to the Sales Velocity Classification program @SC Eliminate logic that classifies items to the value E (any item created within the last 7 days is assigned with a class of E ... 
                                                    ));

       -- Get Category ID  for a Given Category
       Cursor Get_Category_Cur(p_Category  Varchar2 ) Is
            Select Category_ID, CATEGORY_CONCAT_SEGMENTS
            From  MTL_CATEGORY_SET_VALID_CATS_V mcsvc
            Where Category_Set_ID = G_CATEGORY_SET_ID
            And CATEGORY_CONCAT_SEGMENTS = p_Category ;


       l_Record_Count    Number := 0 ;
       l_Category        Varchar2(30) ;
       l_Category_Id     Number;
       l_TOtal_Items     Number ;
       l_Organization_Code   Varchar2(3);
       l_API_Name        Varchar2(30) := 'Populate_GT';

    Begin
       Fnd_File.Put_Line ( Fnd_File.LOG, 'Begning of Populate_GT API :' );
       For Items_Rec In Get_Items_Cur(p_Organization_Id => p_Organization_ID) Loop
       Begin
          -- Insert INto Global Temp Table
          -- Check if Item Status Code
          l_Category_ID := NULL;
          l_Category    := NULL;
          Fnd_File.Put_Line ( Fnd_File.LOG, 'Inserting Records into Sales Velocity GT :' || Items_Rec.Segment1);
          Fnd_File.Put_Line ( Fnd_File.LOG, 'Item Status Code :' || Items_Rec.Inventory_Item_Status_Code );
          Fnd_File.Put_Line ( Fnd_File.LOG, 'Item Current Category :' || Items_Rec.Category_Concat_Segments );
          
          If Items_Rec.Inventory_Item_Status_Code =  G_ITEM_STATUS_DISCONTINUED THen  --G_CATEGORY_DISCONTINUED
          
           Fnd_File.Put_Line ( Fnd_File.LOG, 'Checking Category Discontinued :' );
            FOr Get_Category_Rec In Get_Category_Cur(p_Category => G_CATEGORY_DISCONTINUED ) Loop
               l_Category_ID := Get_Category_Rec.Category_ID ;
               l_Category    := Get_Category_Rec.CATEGORY_CONCAT_SEGMENTS ;
            End Loop;
          
          Elsif Items_Rec.Inventory_Item_Status_Code =  G_ITEM_STATUS_NON_STOCK Then  --G_CATEGORY_NON_STOCK
          
             Fnd_File.Put_Line ( Fnd_File.LOG, 'Checking Category Non Stock :' );
             FOr Get_Category_Rec In Get_Category_Cur(p_Category => G_CATEGORY_NON_STOCK ) Loop
                l_Category_ID := Get_Category_Rec.Category_ID ;
                l_Category    := Get_Category_Rec.CATEGORY_CONCAT_SEGMENTS ;
             End Loop;
          
          Elsif Items_Rec.Inventory_Item_Status_Code =  G_ITEM_STATUS_NOT_REPLENISHED THen  --G_CATEGORY_NOT_REPLENISHED
          
             Fnd_File.Put_Line ( Fnd_File.LOG, 'Checking Category Not Replenished :' );
             FOr Get_Category_Rec In Get_Category_Cur(p_Category => G_CATEGORY_NOT_REPLENISHED ) Loop
                l_Category_ID := Get_Category_Rec.Category_ID ;
                l_Category    := Get_Category_Rec.CATEGORY_CONCAT_SEGMENTS ;
             END Loop;
         
         --Removed 20140203-00255   Change to the Sales Velocity Classification program @SC Eliminate logic that classifies items to the value E (any item created within the last 7 days is assigned with a class of E ... 
         /* Elsif Trunc(Items_Rec.Creation_Date) > Trunc(Sysdate -7) Then
          --Elsif Trunc(Items_Rec.Creation_Date) > Trunc(Add_Months(Sysdate, -1)) Then
          
             Fnd_File.Put_Line ( Fnd_File.LOG, 'Checking Category New :' );
             FOr Get_Category_Rec In Get_Category_Cur(p_Category => G_CATEGORY_NEW) Loop
                l_Category_ID := Get_Category_Rec.Category_ID ;
                l_Category    := Get_Category_Rec.CATEGORY_CONCAT_SEGMENTS ;
             End Loop;
          */
          -- 02/19/2013 CG: TMS 20130218-00781. Point 1 - Null categories or no categories should be classed as N
          Elsif Items_Rec.Category_Concat_Segments is null then 
          
             Fnd_File.Put_Line ( Fnd_File.LOG, 'Checking N category for items without category:' );
             FOr Get_Category_Rec In Get_Category_Cur(p_Category => G_CATEGORY_NON_STOCK) Loop
                l_Category_ID := Get_Category_Rec.Category_ID ;
                l_Category    := Get_Category_Rec.CATEGORY_CONCAT_SEGMENTS ;
             End Loop;
          
         --Removed 20140203-00255   Change to the Sales Velocity Classification program @SC Eliminate logic that classifies items to the value E (any item created within the last 7 days is assigned with a class of E ... 
          /* -- 02/19/2013 CG: TMS 20130218-00781. Point 3: To reclass NEW Items with 0/0 min/max
          Elsif Items_Rec.Category_Concat_Segments = G_CATEGORY_NEW 
                and nvl(Items_Rec.min_minmax_quantity,0) = 0
                and  nvl(Items_Rec.max_minmax_quantity,0) = 0 then
          
             Fnd_File.Put_Line ( Fnd_File.LOG, 'Reclassing to N, new items with null or 0/0 min/max:' );
             FOr Get_Category_Rec In Get_Category_Cur(p_Category => G_CATEGORY_NON_STOCK) Loop
                l_Category_ID := Get_Category_Rec.Category_ID ;
                l_Category    := Get_Category_Rec.CATEGORY_CONCAT_SEGMENTS ;
             End Loop;
          */
          End If;
          --
          
          Fnd_File.Put_Line ( Fnd_File.LOG, ' Sales Velocty GT : Category :' || l_Category);
          Fnd_File.Put_Line ( Fnd_File.LOG, ' Sales Velocty GT : New Category ID :' || l_Category_ID);
          INSERT INTO XXWC_SALES_VELOCITY_GT (
               INVENTORY_ITEM_ID,
               SEGMENT1,
               ORGANIZATION_ID,
               OLD_CATEGORY_ID,
               ITEM_STATUS_CODE,
               SALES_VELOCITY_CATEGORY,
               NEW_CATEGORY_ID )
          VALUES ( Items_Rec.INVENTORY_ITEM_ID,
                   Items_Rec.SEGMENT1,
                   Items_Rec.Organization_ID,
                   Items_Rec.Category_ID,
                   Items_Rec.Inventory_Item_Status_Code,
                   l_Category,
                   l_Category_ID);

          l_Record_Count := l_Record_Count + 1 ;

          If l_Category_ID is Not null THen
            Fnd_File.Put_Line ( Fnd_File.LOG, 'Updating Item Category for Item :' || Items_Rec.Segment1);
            Update_Item_Category(p_Organization_Id     => Items_Rec.Organization_ID,
                                 p_Inventory_Item_ID   => Items_Rec.INVENTORY_ITEM_ID  );
          End If;

          If l_Record_Count >= G_COMMIT_RECORDS_COUNT Then
             Commit ;
             l_Record_Count := 0;
          End If;
       Exception
          When Others Then
             Fnd_File.Put_Line ( Fnd_File.LOG, ' When Others exception for API :'||  l_API_Name || ' : Error Message is  :'  || SQLERRM);
             Fnd_File.Put_Line ( Fnd_File.LOG,' Processing Item :' || Items_REc.Segment1 || ' from warehoues :' || G_ORGANIZATION_CODE );
             G_Return_Status := 'E' ;

       End;
       End Loop;
       -- Commit Remaining Records
       Commit;
       --- Get Statistical Information and populate Error Log and Out Files accordingly
       Select Count(*)
       Into l_TOtal_Items
       From XXWC_SALES_VELOCITY_GT;



       Fnd_File.Put_Line ( Fnd_File.LOG, 'Total Number of iTems inserted  :' ||l_TOtal_Items );
       
       -- 11/28/12 CG: Changed to add in XML report tags
       --Fnd_File.Put_Line ( fnd_file.output,'-----------------------------------------------------------------------');
       --Fnd_File.Put_Line ( fnd_file.output,'Processing items from warehouse                 :' ||G_Organization_Code );
       -- Fnd_File.Put_Line ( fnd_file.output,'Total Number of items identified for processing :' ||l_TOtal_Items );
       Fnd_File.Put_Line ( Fnd_File.output,'<INITIAL_ITEM_COUNT>'||l_TOtal_Items||'</INITIAL_ITEM_COUNT>');
       
       Fnd_File.Put_Line ( Fnd_File.LOG, 'End of Populate_GT API :' );
    End Populate_GT;

    -- Procedure will calculate Sales Velocity Counts for N weeks for all the items and Update GT table
    Procedure Sales_Velocity_Weeks(p_ORganization_ID Number) IS
       -- Define a Cursor that gets Sales Velocity Counts for all the Sales ORders for a Given Warehouse.
       -- Pick up only External Customers Sales Orders Only
       -- Consider only Sales Order Lines that are closed.

       -- 10/23/2012 CG: Altered to count counter orders and exclude quotes
       Cursor  Sales_Count_Cur (p_Organization_ID Number ) IS
           Select svgt.Inventory_Item_Id, 0 Sales_Count
           From XXWC_SALES_VELOCITY_GT svgt
           where    svgt.Organization_ID = p_Organization_ID
           And  NVL(svgt.Sales_Velocity_Category,'0') Not In ( G_CATEGORY_DISCONTINUED , G_CATEGORY_NOT_REPLENISHED,G_CATEGORY_NON_STOCK, G_CATEGORY_NEW, 
                                                               --05/07/13 TMS 20130502-01289: CG Added to consider new SV Classes D and V to behave like Z and N
                                                               G_CATEGORY_WC_DISCONTINUED, G_CATEGORY_SUP_DISCONTINUED
                                                               --05/07/13 TMS 20130502-01289
                                                             )
           order by svgt.Inventory_Item_Id;
           
           /*Select distinct ool.Inventory_Item_Id, COUNT ( * ) OVER (PARTITION BY ool.inventory_item_id) Sales_Count
           From Oe_Order_Lines_all  ool ,
                XXWC_SALES_VELOCITY_GT svgt,
                oe_order_headers_all ooh, 
                oe_transaction_types_tl ott
           Where ool.Ship_From_Org_ID = p_Organization_ID
           -- 10/23/2012 CG
           -- And   Trunc(ool.Actual_Shipment_Date) >  Trunc(( Sysdate - (G_NUMBER_OF_WEEKS * 7) ))
           -- And  ( 
           --         (ott.name != 'COUNTER LINE' and Trunc(ool.Actual_Shipment_Date) >  Trunc(( Sysdate - (G_NUMBER_OF_WEEKS * 7) )))            
           --         OR 
           --         (ott.name = 'COUNTER LINE' and Trunc(ool.fulfillment_Date) >  Trunc(( Sysdate - (G_NUMBER_OF_WEEKS * 7) )))
           --     )
           -- 10/23/2012 CG
           ANd  ool.Flow_Status_Code = 'CLOSED'
           AND  OOL.LINE_CATEGORY_CODE = 'ORDER'
           AND  ool.INVOICE_INTERFACE_STATUS_CODE = 'YES'
           AND  ool.Ship_From_Org_ID = svgt.Organization_ID
           And  ool.Inventory_Item_ID = svgt.Inventory_Item_ID
           -- 10/23/2012 CG
           AND  ool.header_id = ooh.header_id
           AND  ooh.order_number is not null
           AND  ool.line_type_id = ott.transaction_type_id
           -- 10/23/2012 CG
           And  NVL(svgt.Sales_Velocity_Category,'0') Not In ( G_CATEGORY_DISCONTINUED , G_CATEGORY_NOT_REPLENISHED,G_CATEGORY_NON_STOCK,G_CATEGORY_NEW )
           And Not Exists ( Select 1
                            From Mtl_Parameters Mp
                            Where  mp.Organization_ID = ool.Ship_to_Org_ID )
           Group By ool.Header_Id, ool.Inventory_Item_ID ;*/
           /**********************
           And Not Exists (Select 1
                           From XXWC_SALES_VELOCITY_GT svgt
                           Where svgt.Inventory_Item_ID = ool.Inventory_Item_ID
                           And   svgt.Organization_ID  = ool.Ship_From_Org_ID
                           And   svgt.SALES_VELOCITY_CATEGORY in (G_CATEGORY_DISCONTINUED, G_CATEGORY_NON_STOCK,
                                   G_CATEGORY_NOT_REPLENISHED,G_CATEGORY_NEW  ) )

           *********************/

        -- Cursor To Determine Sales Velocity Category
        Cursor  Get_Category_CUr (p_Lookup_Type Varchar2,
                                  p_Sales_Count Number ) IS
            Select Lookup_Code
            From fnd_lookup_Values
            Where Lookup_Type      = p_Lookup_Type
            And Enabled_Flag       = 'Y'
            And Start_Date_Active <= SYSDATE
            And Attribute_Category = 'XXWC_SALES_VELOCITY'
            And p_Sales_Count Between To_Number(Attribute1) and To_Number(Attribute2)
            ANd Attribute1 is Not Null
            And Attribute2 is Not Null ;

        -- Get Category ID  for a Given Category
        -- SatishU : Added P_Category Is Not Null to Cursor Definition
       Cursor Get_Category_ID_Cur(p_Category  Varchar2 ) Is
            Select Category_ID, CATEGORY_CONCAT_SEGMENTS
            From  MTL_CATEGORY_SET_VALID_CATS_V mcsvc
            Where Category_Set_ID = G_CATEGORY_SET_ID
            And CATEGORY_CONCAT_SEGMENTS = p_Category
            AND P_Category Is Not Null;


         --Satish U: Define a Cursor to go through all the items in A DC Warehouse
         Cursor DC_Item_Cur(p_Organization_ID Number ) Is
           Select *
           From XXWC_SALES_VELOCITY_GT svgt
           Where  NVL(svgt.Sales_Velocity_Category,'0')
                Not In ( G_CATEGORY_DISCONTINUED , G_CATEGORY_NOT_REPLENISHED, G_CATEGORY_NON_STOCK, G_CATEGORY_NEW, 
                         --05/07/13 TMS 20130502-01289: CG Added to consider new SV Classes D and V to behave like Z and N
                         G_CATEGORY_WC_DISCONTINUED, G_CATEGORY_SUP_DISCONTINUED
                         --05/07/13 TMS 20130502-01289
                       )
           AND svgt.New_Category_Id Is  Null ;



        l_Warehouse_Mode      Varchar2(30) ;
        l_Lookup_TYpe         Varchar2(30);
        l_Category            Varchar2(10);
        l_SO_Sales_Count      Number ;
        l_DC_SO_Sales_Count   Number ;
        l_DC_Annual_SO_Sales_Count Number ;
        l_RMA_Count           Number ;
        l_Category_ID         Number;
        l_Record_Count        Number := 0 ;
        l_API_Name            Varchar2(30) := 'Sales_Velocity_Weeks';
        l_Inventory_Item_ID   Number := NULL;
        
        l_twelve_month_sales  NUMBER;

    Begin
        Fnd_File.Put_Line ( Fnd_File.LOG,',Beging of API :' || l_API_Name ) ;
        
        -- Find OUt Warehouse Mode
        Select Attribute7
        Into l_Warehouse_Mode
        From MTL_PARAMETERS
        WHERE Organization_ID = p_Organization_ID ;
        
        If l_Warehouse_Mode  = G_WHOUSE_MODE_STORE  THen
            IF G_NUMBER_OF_WEEKS = 12 THen
               l_LookUP_Type := '12-WEEKS-STORE' ;
            ELSIF G_NUMBER_OF_WEEKS = 16 THen
               l_LookUP_Type := '16-WEEKS-STORE' ;
            END IF;
        Elsif l_Warehouse_Mode  = G_WHOUSE_MODE_DC Then
           IF G_NUMBER_OF_WEEKS = 12 Then
              l_LookUP_Type := '12-WEEKS-DC' ;
           ELSIF G_NUMBER_OF_WEEKS = 16 Then
              l_LookUP_Type := '16-WEEKS-DC' ;
           END IF;
        End IF;
        
        Fnd_File.Put_Line ( Fnd_File.LOG,'Before For Loop Sales Count Cursor' ) ;
        For Sales_Count_REc in Sales_Count_Cur(p_Organization_ID) Loop
        
            Begin
               
               l_SO_Sales_Count           := 0;
               l_RMA_Count                := 0;
               l_DC_SO_Sales_Count        := 0;
               l_DC_Annual_SO_Sales_Count := 0;
               l_Category_ID              := NULL;
               l_Category                 := NULL;
               
               Fnd_File.Put_Line ( Fnd_File.LOG,'Before If Statement 100' ) ;
               
                If l_Inventory_Item_ID Is NULL or l_Inventory_Item_ID <> Sales_Count_REc.Inventory_Item_ID Then
               
                    Fnd_File.Put_Line ( Fnd_File.LOG,'Processing Inventory Item Id:' || Sales_Count_Rec.Inventory_Item_ID ) ;
                    
                    l_INventory_Item_ID := Sales_Count_REc.Inventory_Item_ID ;
                    l_Record_Count := l_Record_Count + 1 ;
                    
                    -- 12/26/12 CG: Commenting out to use the EiSR Numbers to determine SV
                    /*l_SO_Sales_Count := Sales_Count_REc.Sales_Count;
                    
                    Fnd_File.Put_Line ( Fnd_File.LOG,'l_SO_Sales_Count 1:' ||l_SO_Sales_Count ) ;
                    -- RMA Count
                    RMA_WEEKS(P_Organization_Id      => p_Organization_ID,
                              p_Inventory_Item_ID    => Sales_Count_Rec.INventory_Item_ID,
                              x_RMA_Count            => l_RMA_Count);
                    
                    Fnd_File.Put_Line ( Fnd_File.LOG,'l_RMA_Count:' ||l_RMA_Count ) ;
                    
                    l_SO_Sales_Count := l_SO_Sales_Count - l_RMA_Count ;
                    
                    If l_SO_Sales_Count < 0 THen
                       l_SO_Sales_Count := 0;
                    End If;
                    
                    -- 10/18/2012 CG
                    Get_DC_Sales_Count(p_Inventory_Item_ID     => Sales_Count_Rec.INventory_Item_ID,
                                         p_DC_Organization_ID     => p_Organization_ID,
                                         x_DC_Sales_Count         => l_DC_SO_Sales_Count,
                                         X_DC_Annual_Sales_Count  => l_DC_Annual_SO_Sales_Count) ;

                   -- Add Current DC SO Count
                   Fnd_File.Put_Line ( Fnd_File.LOG,'Week Sales Count:' || l_SO_Sales_Count ) ;
                   l_DC_SO_Sales_Count := l_DC_SO_Sales_Count + l_SO_Sales_Count ;
                   Fnd_File.Put_Line ( Fnd_File.LOG,'DC Week DC Sales Count:' || l_DC_SO_Sales_Count ) ;
                   -- Get Sales Velocity Count for each of the items for N weeks*/
                   
                    get_eisr_sales_hits (p_inventory_item_id => l_INventory_Item_ID
                                        , p_organization_id => p_Organization_ID
                                        , x_sales_hits      => l_SO_Sales_Count
                                        , x_dc_sales_hits   => l_DC_SO_Sales_Count);
                                        
                    l_twelve_month_sales := null;                                    
                    l_twelve_month_sales := get_annual_sales (p_inventory_item_id => l_INventory_Item_ID
                                                              , p_organization_id => p_Organization_ID);                                   
                                        
                    Fnd_File.Put_Line ( Fnd_File.LOG,'EiSR Week Sales Count:' || l_SO_Sales_Count );
                    Fnd_File.Put_Line ( Fnd_File.LOG,'EiSR DC Week DC Sales Count:' || l_DC_SO_Sales_Count );
                    Fnd_File.Put_Line ( Fnd_File.LOG,'EiSR Twelve Month Sales:' || l_twelve_month_sales );
                    
                    If l_Warehouse_Mode  = G_WHOUSE_MODE_DC  THen
                        -- 10/18/2012 CG
                       /*Get_DC_Sales_Count(p_Inventory_Item_ID     => Sales_Count_Rec.INventory_Item_ID,
                                         p_DC_Organization_ID     => p_Organization_ID,
                                         x_DC_Sales_Count         => l_DC_SO_Sales_Count,
                                         X_DC_Annual_Sales_Count  => l_DC_Annual_SO_Sales_Count) ;

                       -- Add Current DC SO Count
                       Fnd_File.Put_Line ( Fnd_File.LOG,'DC Week Sales Count:' || l_SO_Sales_Count ) ;
                       l_DC_SO_Sales_Count := l_DC_SO_Sales_Count + l_SO_Sales_Count ;
                       Fnd_File.Put_Line ( Fnd_File.LOG,'DC Week DC Sales Count:' || l_DC_SO_Sales_Count ) ;
                       -- Get Sales Velocity Count for each of the items for N weeks*/
                       
                       if l_DC_SO_Sales_Count > 0 then
                        
                            For Get_Category_Rec In Get_Category_CUr (p_Lookup_Type   => l_Lookup_TYpe ,
                                                                     p_Sales_Count   => l_DC_SO_Sales_Count ) Loop
                                l_Category := Get_Category_Rec.Lookup_Code ;
                            End Loop;
                       
                       -- 02/19/2013 CG: TMS 20130218-00781. Point 2: Hits 4 = 0 and 12 months sale > 0 then 7
                       --                                             Hits 4 = 0 and 12 months sale = 0 then C
                       elsif l_DC_SO_Sales_Count = 0 and nvl(l_twelve_month_sales,0) > 0 then
                            l_Category := '7';
                       elsif l_DC_SO_Sales_Count = 0 and nvl(l_twelve_month_sales,0) = 0 then
                            l_Category := 'C';
                       end if;
                       
                    Else
                    
                       Fnd_File.Put_Line ( Fnd_File.LOG,'Lookup TYpe is :' || l_Lookup_TYpe ) ;
                       Fnd_File.Put_Line ( Fnd_File.LOG,'Sales Count :' || l_SO_Sales_Count) ;
                       -- Get Sales Velocity Count for each of the items for N weeks
                       
                       -- 12/10/2012 GG Changed to use DC count per MD050
                       -- if l_SO_Sales_Count > 0 then
                       if l_DC_SO_Sales_Count > 0 then                       
                           For Get_Category_Rec In Get_Category_CUr (p_Lookup_Type   => l_Lookup_TYpe ,
                                                                     -- p_Sales_Count   => l_SO_Sales_Count ) Loop
                                                                     p_Sales_Count   => l_DC_SO_Sales_Count ) Loop
                              l_Category := Get_Category_Rec.Lookup_Code ;
                           End Loop;
                       
                       -- 02/19/2013 CG: TMS 20130218-00781. Point 2: Hits 4 = 0 and 12 months sale > 0 then 7
                       --                                             Hits 4 = 0 and 12 months sale = 0 then C
                       elsif l_DC_SO_Sales_Count = 0 and nvl(l_twelve_month_sales,0) > 0 then
                            l_Category := '7';
                       elsif l_DC_SO_Sales_Count = 0 and nvl(l_twelve_month_sales,0) = 0 then
                            l_Category := 'C';
                       end if;
                       
                       --Fnd_File.Put_Line ( Fnd_File.LOG,'Category is  :' || l_Category ) ;                                      
                    ENd If;

                    -- Get Category ID for a given Sales Velocity Category
                    Fnd_File.Put_Line ( Fnd_File.LOG,'Category Set: '||To_Number(Fnd_Profile.Value('XXWC_SALES_VELOCITY_CATEOGRY_SET'))); 
                    Fnd_File.Put_Line ( Fnd_File.LOG,'Category: '||l_Category);
                    For Get_Category_ID_Rec In Get_Category_ID_Cur(p_Category => l_Category) Loop
                        l_Category_ID := Get_Category_ID_Rec.Category_ID ;                    
                    End Loop;
                    
                    Fnd_File.Put_Line ( Fnd_File.LOG,'l_Category_ID:' ||l_Category_ID ) ;

                    If l_Warehouse_Mode  = G_WHOUSE_MODE_DC  THen
                       -- Update Global Temp Table
                       Update XXWC_SALES_VELOCITY_GT
                          Set SALES_VELOCITY_CATEGORY = l_Category,
                            NEW_CATEGORY_ID         = l_Category_ID,
                            SALES_VELOCITY_COUNT    = l_SO_Sales_Count,
                            DC_SALES_VELOCITY_COUNT = NVL(l_DC_SO_Sales_Count,0)
                            --DC_ANNUAL_SALES_VELOCITY_COUNT = 0
                            --DC_ANNUAL_SALES_VELOCITY_COUNT = NVL(l_DC_Annual_SO_Sales_Count,0)
                       Where Inventory_Item_ID = Sales_Count_REc.INventory_Item_ID
                       And   Organization_ID   =  p_Organization_ID ;
                    Else
                       Fnd_File.Put_Line ( Fnd_File.LOG,'Sales Velocity Count is :' || l_SO_Sales_Count ) ;
                       -- Update Global Temp Table
                       Update XXWC_SALES_VELOCITY_GT
                          Set SALES_VELOCITY_CATEGORY = l_Category,
                            NEW_CATEGORY_ID         = l_Category_ID,
                            SALES_VELOCITY_COUNT    = l_SO_Sales_Count,
                            DC_SALES_VELOCITY_COUNT = NVL(l_DC_SO_Sales_Count,0)
                            -- ANNUAL_SALES_VELOCITY_COUNT = 0
                       Where Inventory_Item_ID = Sales_Count_REc.INventory_Item_ID
                       And   Organization_ID   =  p_Organization_ID ;
                    End If;
                    -- Commit Transactions after Reaching limit of Commit Records Count
                    If l_Record_Count >= G_COMMIT_RECORDS_COUNT Then
                       Commit ;
                       l_Record_Count := 0;
                    End If;
               Else
                  -- Do Nothing but go to the next record
                  NULL;
               End If;
               
            EXCEPTION
               WHEN OTHERS THEN
                  Fnd_File.Put_Line ( Fnd_File.LOG, ' When Others exception for API :'||  l_API_Name || ' : Error Message is  :'  || SQLERRM);
                  Fnd_File.Put_Line ( Fnd_File.LOG,' Processing Item ID :' || Sales_Count_REc.Inventory_Item_ID || ' from warehoues :' || G_ORGANIZATION_CODE );
                  G_Return_Status := 'E' ;
            End;
            
        ENd Loop;

        -- Calculate DC MODE Sales Velocity for Items that are not sold at DC MODE in 16 weeks.
        if l_Warehouse_Mode  = G_WHOUSE_MODE_DC Then
           Fnd_File.Put_Line ( Fnd_File.LOG,' Second If Statement for DC MODE  :'  ) ;
           
           For DC_Item_REc In DC_Item_Cur(p_Organization_ID ) Loop
                Fnd_File.Put_Line ( Fnd_File.LOG,' Item found for DC MODE  :' || DC_Item_REc.Segment1 ) ;
                l_SO_Sales_Count := 0;
                l_DC_SO_Sales_Count := 0 ;
                l_DC_Annual_SO_Sales_Count := 0;
                
                -- 02/20/13 CG Removed
                /*Get_DC_Sales_Count(p_Inventory_Item_ID     => DC_Item_Rec.INventory_Item_ID,
                                   p_DC_Organization_ID    => p_Organization_ID,
                                   x_DC_Sales_Count        => l_DC_SO_Sales_Count,
                                   X_DC_Annual_Sales_Count => l_DC_Annual_SO_Sales_Count );*/
                                   
                -- 12/26/12 CG: Adding to use the EiSR Numbers
                get_eisr_sales_hits (p_inventory_item_id => DC_Item_Rec.INventory_Item_ID
                                    , p_organization_id => p_Organization_ID
                                    , x_sales_hits      => l_SO_Sales_Count
                                    , x_dc_sales_hits   => l_DC_SO_Sales_Count);

                -- 02/20/13 CG: Added
                l_twelve_month_sales := null;                                    
                l_twelve_month_sales := get_annual_sales (p_inventory_item_id => DC_Item_Rec.INventory_Item_ID
                                                          , p_organization_id => p_Organization_ID);                                   
                                        
                Fnd_File.Put_Line ( Fnd_File.LOG,'EiSR Week Sales Count:' || l_SO_Sales_Count );
                Fnd_File.Put_Line ( Fnd_File.LOG,'EiSR DC Week DC Sales Count:' || l_DC_SO_Sales_Count );
                Fnd_File.Put_Line ( Fnd_File.LOG,'EiSR Twelve Month Sales:' || l_twelve_month_sales );

                IF NVL(l_DC_SO_Sales_Count,0) > 0 then
                   -- Get Category for DC Sales Count
                   -- Get Sales Velocity Count for each of the items for N weeks
                   For Get_Category_Rec In Get_Category_CUr (p_Lookup_Type   => l_Lookup_TYpe ,
                                                             p_Sales_Count   => l_DC_SO_Sales_Count ) Loop
                      l_Category := Get_Category_Rec.Lookup_Code ;
                   End Loop;
                
                -- 02/19/2013 CG: TMS 20130218-00781. Point 2: Hits 4 = 0 and 12 months sale > 0 then 7
                --                                             Hits 4 = 0 and 12 months sale = 0 then C
                elsif NVL(l_DC_SO_Sales_Count,0) = 0 and nvl(l_twelve_month_sales,0) > 0 then
                    l_Category := '7';
                elsif NVL(l_DC_SO_Sales_Count,0) = 0 and nvl(l_twelve_month_sales,0) = 0 then
                    l_Category := 'C';
                end if;

                Fnd_File.Put_Line ( Fnd_File.LOG,'Category Set: '||To_Number(Fnd_Profile.Value('XXWC_SALES_VELOCITY_CATEOGRY_SET'))); 
                Fnd_File.Put_Line ( Fnd_File.LOG,'Category: '||l_Category);
               -- Get Category ID for a given Sales Velocity Category
                For Get_Category_ID_Rec In Get_Category_ID_Cur(p_Category  => l_Category ) Loop
                    l_Category_ID := Get_Category_ID_Rec.Category_ID ;
                End Loop;
                Fnd_File.Put_Line ( Fnd_File.LOG,'l_Category_ID:' ||l_Category_ID ) ;

                -- Update Global Temp Table
                Update XXWC_SALES_VELOCITY_GT
                  Set SALES_VELOCITY_CATEGORY = l_Category,
                      NEW_CATEGORY_ID         = l_Category_ID,
                      SALES_VELOCITY_COUNT    = 0,
                      DC_SALES_VELOCITY_COUNT = NVL(l_DC_SO_Sales_Count,0),
                      DC_ANNUAL_SALES_VELOCITY_COUNT = 0
                      --DC_ANNUAL_SALES_VELOCITY_COUNT = NVL(l_DC_Annual_SO_Sales_Count,0)
                 Where Inventory_Item_ID    = DC_Item_REc.INventory_Item_ID
                 And   Organization_ID      =  p_Organization_ID ;

           End Loop;

        End If;
        
    ENd Sales_Velocity_Weeks;

    -- Procedure RMA_WEEKS
    -- Procedure Calcuatates count of RMA for all the items and Updte GT Table.
    Procedure RMA_WEEKS(P_Organization_Id   IN Number,
                        p_Inventory_Item_ID IN  Number,
                        x_RMA_Count         Out NOCOPY Number) IS
       -- Define a Cursor that gets all RMA Lines and its corresponding SO Lines
       -- 10/23/2012 CG: Altered to consider counter lines
       Cursor RMA_Cur (p_Organization_ID Number ) Is
       -- 12/12/12 CG: Commented out grouping by line if there are mult return against the same item
          Select Ret.Inventory_Item_ID, Ret.REFERENCE_HEADER_ID, -- Ret.REFERENCE_LINE_ID,
             Sum(Nvl(ret.Ordered_Quantity,0) - NVL(Ret.Cancelled_Quantity,0)) Return_Qty ,
             Sum(Nvl(shp.Ordered_Quantity,0) - NVL(shp.Cancelled_Quantity,0)) Ship_Qty
          --Modified the below line for TMS ## 20141002-00048  by VijaySrinivasan  on 11/13/2014 
          From oe_Order_Lines Ret,
          --Modified the below line for TMS ## 20141002-00048  by VijaySrinivasan  on 11/13/2014 
               oe_order_Lines shp,
               oe_transaction_types_tl ott
          Where Ret.LINE_CATEGORY_CODE = 'RETURN'
          AND   Ret.RETURN_CONTEXT     = 'ORDER'
          ANd   Ret.Flow_Status_Code = 'CLOSED'
          AND   Ret.REFERENCE_HEADER_ID = shp.Header_ID
          AND   Ret.REFERENCE_LINE_ID   = Shp.Line_ID
          AND   Ret.Inventory_Item_ID   = p_Inventory_Item_ID
          And   Ret.Inventory_Item_ID   = shp.Inventory_Item_ID
          AND   shp.Ship_from_Org_ID = p_ORganization_ID
          -- 10/23/2012 CG
          -- And   Trunc(shp.Actual_Shipment_Date) >  Trunc(( Sysdate - (G_NUMBER_OF_WEEKS * 7) ))
          AND   shp.line_type_id = ott.transaction_type_id
          AND   Trunc( Sysdate - (G_NUMBER_OF_WEEKS * 7) ) < (CASE WHEN ott.name != 'COUNTER LINE' THEN Trunc(shp.Actual_Shipment_Date)
                                                                   ELSE Trunc(shp.fulfillment_Date)
                                                                   END)
          -- 10/23/2012 CG
          Group by Ret.Inventory_Item_ID,Ret.REFERENCE_HEADER_ID; --, Ret.REFERENCE_LINE_ID  ;
          /***************************
          And Not Exists (Select 1
                           From XXWC_SALES_VELOCITY_GT svgt
                           Where svgt.Inventory_Item_ID = shp.Inventory_Item_ID
                           And   svgt.Organization_ID  = shp.Ship_From_Org_ID
                           And   svgt.SALES_VELOCITY_CATEGORY in (G_CATEGORY_DISCONTINUED, G_CATEGORY_NON_STOCK,
                                   G_CATEGORY_NOT_REPLENISHED,G_CATEGORY_NEW  ) )
          ****************************/
         -- Get Sales Velocity Count for given Period from GT
         Cursor  SO_Cur (p_Organization_ID   Number,
                         p_Inventory_Item_ID Number ) Is
            Select Sales_Velocity_Count
            From XXWC_SALES_VELOCITY_GT
            Where Inventory_Item_ID =  p_INventory_Item_ID
            And   Organization_ID   =  p_Organization_ID ;

         l_Sales_Velocity_Count Number ;
         l_API_Name             Varchar2(30) := 'RMA_WEEKS';
    Begin
       l_Sales_Velocity_Count := 0;
       For RMA_Rec In RMA_Cur(p_Organization_ID ) Loop
       Begin
          If (RMA_Rec.Return_Qty = RMA_Rec.Ship_Qty ) And RMA_Rec.Return_Qty <> 0  Then
             l_Sales_Velocity_Count := l_Sales_Velocity_Count + 1;
          End If;
       Exception
          When Others Then
             Fnd_File.Put_Line ( Fnd_File.LOG, ' When Others exception for API :'||  l_API_Name || ' : Error Message is  :'  || SQLERRM);
             Fnd_File.Put_Line ( Fnd_File.LOG,' Processing Item ID :' || p_Inventory_Item_ID || ' from warehouse :' || G_ORGANIZATION_CODE );
             G_Return_Status := 'E' ;
       End;
       ENd Loop;
      x_RMA_Count := l_Sales_Velocity_Count ;
    ENd RMA_WEEKS;


    -- Procedure will calculate Sales Velocity Counts for N weeks for all the items and Update GT table
    Procedure Sales_Velocity_Annual(p_ORganization_ID Number) IS
       -- Define a Cursor that gets Sales Velocity Counts for all the Sales ORders for a Given Warehouse.
       -- Pick up only External Customers Sales Orders Only
       -- Consider only Sales Order Lines that are closed.

       -- 10/23/2012 CG: Altered to count counter orders and exclude quotes
       Cursor  Sales_Count_Cur (p_Organization_ID Number ) IS
           Select svgt.Inventory_Item_Id, 0 Sales_Count
           From XXWC_SALES_VELOCITY_GT svgt
           where    svgt.Organization_ID = p_Organization_ID
           And  NVL(svgt.Sales_Velocity_Category,'0') Not In ( G_CATEGORY_DISCONTINUED , G_CATEGORY_NOT_REPLENISHED,G_CATEGORY_NON_STOCK,G_CATEGORY_NEW,
                                                               --05/07/13 TMS 20130502-01289: CG Added to consider new SV Classes D and V to behave like Z and N
                                                               G_CATEGORY_WC_DISCONTINUED, G_CATEGORY_SUP_DISCONTINUED
                                                               --05/07/13 TMS 20130502-01289
                                                             )
           order by svgt.Inventory_Item_Id;
           
           /*Select distinct ool.Inventory_Item_Id, COUNT ( * ) OVER (PARTITION BY ool.inventory_item_id) Sales_Count
           From Oe_Order_Lines_all  ool ,
                XXWC_SALES_VELOCITY_GT svgt,
                oe_order_headers_all ooh, 
                oe_transaction_types_tl ott
           Where ool.Ship_From_Org_ID = p_Organization_ID
           -- 10/23/2012 CG
           -- And   Trunc(ool.Actual_Shipment_Date) >  Trunc(G_DATE_ONE_YEAR_BACK )
           And  ( 
                    (ott.name != 'COUNTER LINE' and Trunc(ool.Actual_Shipment_Date) > Trunc(G_DATE_ONE_YEAR_BACK ) )            
                    OR 
                    (ott.name = 'COUNTER LINE' and Trunc(ool.fulfillment_Date) > Trunc(G_DATE_ONE_YEAR_BACK ) )
                )
           -- 10/23/2012 CG
           ANd  ool.Flow_Status_Code = 'CLOSED'
           AND  OOL.LINE_CATEGORY_CODE = 'ORDER'
           AND  ool.INVOICE_INTERFACE_STATUS_CODE = 'YES'
           AND  ool.Ship_From_Org_ID = svgt.Organization_ID
           And  ool.Inventory_Item_ID = svgt.Inventory_Item_ID
           -- 10/23/2012 CG
           AND  ool.header_id = ooh.header_id
           AND  ooh.order_number is not null
           AND  ool.line_type_id = ott.transaction_type_id
           -- 10/23/2012 CG
           And  NVL(svgt.Sales_Velocity_Category,'0') Not In ( G_CATEGORY_DISCONTINUED , G_CATEGORY_NOT_REPLENISHED, G_CATEGORY_NON_STOCK, G_CATEGORY_NEW )
           --AND svgt.New_Category_Id Is  Null
           And Not Exists ( Select 1
                            From Mtl_Parameters Mp
                            Where  mp.Organization_ID = ool.Ship_to_Org_ID )
           Group By  ool.Inventory_Item_ID,ool.Header_Id;*/

           /***************
           And Not Exists (Select 1
                           From XXWC_SALES_VELOCITY_GT svgt
                           Where svgt.Inventory_Item_ID = ool.Inventory_Item_ID
                           And   svgt.Organization_ID  = ool.Ship_From_Org_ID
                           And   (svgt.SALES_VELOCITY_CATEGORY in (G_CATEGORY_DISCONTINUED, G_CATEGORY_NON_STOCK,
                                   G_CATEGORY_NOT_REPLENISHED,G_CATEGORY_NEW  )
                           AND svgt.New_Category_Id Is Not Null) )

           ****************/
        -- Cursor To Determine Sales Velocity Category
        Cursor  Get_Category_CUr (p_Lookup_Type Varchar2,
                                  p_Sales_Count Number ) IS
            Select Lookup_Code
            From fnd_lookup_Values
            Where Lookup_Type      = p_Lookup_Type
            And Enabled_Flag       = 'Y'
            And Start_Date_Active <= SYSDATE
            And Attribute_Category = 'XXWC_SALES_VELOCITY'
            And ( Attribute1 is Not Null
               And (p_Sales_Count Between To_Number(NVL(Attribute1,'0')) and To_Number(NVL(Attribute2,'0')) )
               Or ( P_Sales_Count > 0 and Lookup_Code = '7' )
               Or ( p_Sales_Count = 0 and Lookup_Code = 'C'));

        -- Get Category ID  for a Given Category
       Cursor Get_Category_ID_Cur(p_Category  Varchar2 ) Is
            Select Category_ID, CATEGORY_CONCAT_SEGMENTS
            From  MTL_CATEGORY_SET_VALID_CATS_V mcsvc
            Where Category_Set_ID = G_CATEGORY_SET_ID
            And CATEGORY_CONCAT_SEGMENTS = p_Category ;

       --Cursor to get list of Items that are not sold for whole year
        Cursor No_Sales_Cur (p_Organization_Id Number ) Is
           Select *
           From XXWC_SALES_VELOCITY_GT svgt
           Where New_Category_ID is Null
           and   Organization_ID = p_Organization_ID ;

        l_Warehouse_Mode             Varchar2(30) ;
        l_Lookup_TYpe                Varchar2(30);
        l_Category                   Varchar2(10);
        l_SO_Sales_Count             Number ;
        l_SO_Annual_Sales_Count      Number ;
        l_DC_SO_Sales_Count          Number ;
        l_DC_Annual_SO_Sales_Count   Number ;
        l_RMA_Count                  Number ;
        l_Category_ID                Number;
        l_Record_Count               Number := 0 ;
        l_API_Name                   Varchar2(30) ;
        l_Inventory_Item_ID          Number;
        
        l_new_cat                    number;
        l_new_cat_desc               varchar2(1);

    Begin
        Fnd_File.Put_Line ( Fnd_File.LOG, 'Begning of Sales_Velocity_Annual API :' );
        -- Find OUt Warehouse Mode
        Select Attribute7
        Into l_Warehouse_Mode
        From MTL_PARAMETERS
        WHERE Organization_ID = p_Organization_ID ;
        
        If l_Warehouse_Mode  = G_WHOUSE_MODE_STORE  THen
            IF G_NUMBER_OF_WEEKS = 12 THen
               l_LookUP_Type := '12-WEEKS-STORE' ;
            ELSIF G_NUMBER_OF_WEEKS = 16 THen
               l_LookUP_Type := '16-WEEKS-STORE' ;
            END IF;
        Elsif l_Warehouse_Mode  = G_WHOUSE_MODE_DC Then
           IF G_NUMBER_OF_WEEKS = 12 Then
              l_LookUP_Type := '12-WEEKS-DC' ;
           ELSIF G_NUMBER_OF_WEEKS = 16 Then
              l_LookUP_Type := '16-WEEKS-DC' ;
           END IF;
        End IF;

        For Sales_Count_REc in Sales_Count_Cur(p_Organization_ID) Loop
        Begin
           --initialize Variables
           l_Category                  := NULL;
           l_Category_ID              := NULL;
           l_SO_Sales_Count           := 0;
           l_RMA_Count                := 0;
           l_DC_SO_Sales_Count        := 0;
           l_DC_Annual_SO_Sales_Count := 0;
           l_new_cat                  := null;
           l_new_cat_desc             := null;
           
           If l_Inventory_Item_ID Is Null or l_Inventory_Item_ID <> Sales_Count_Rec.Inventory_Item_ID  Then
              
              l_Inventory_Item_ID := Sales_Count_Rec.Inventory_Item_ID ;
              
                Fnd_File.Put_Line ( Fnd_File.LOG, 'Inside the Loop 100 :' );
                Fnd_File.Put_Line ( Fnd_File.LOG, 'Item Number  :' || Sales_Count_Rec.Inventory_Item_ID);
                --LOG_WRITER (G_LEVEL_STATEMENT, 'Inventory Item ID Is :' || Sales_Count_Rec.INventory_Item_ID );
                --LOG_WRITER (G_LEVEL_STATEMENT, 'ORganization ID Is :' || p_Organization_Id );
                l_Record_Count := l_Record_Count + 1 ;
                l_SO_Sales_Count := Sales_Count_REc.Sales_Count;
                
                -- RMA Count
                RMA_ANNUAL(P_Organization_Id      => p_Organization_ID,
                          p_Inventory_Item_ID    => Sales_Count_Rec.INventory_Item_ID,
                          x_RMA_Count            => l_RMA_Count);

                l_SO_Sales_Count := l_SO_Sales_Count - l_RMA_Count ;

                If l_SO_Sales_Count < 0 THen
                   l_SO_Sales_Count := 0;
                End If;
                
                -- Check if Warehouse Mode is DC
                --10/18/12 CG
                Get_DC_Sales_Count(p_Inventory_Item_ID     => Sales_Count_Rec.INventory_Item_ID,
                                     p_DC_Organization_ID     => p_Organization_ID,
                                     x_DC_Sales_Count         => l_DC_SO_Sales_Count,
                                     X_DC_Annual_Sales_Count  => l_DC_Annual_SO_Sales_Count) ;

                -- Add Current DC SO Count
                -- 10/23/2012 CG: Corrected to use the annual numbers
                -- l_DC_SO_Sales_Count := l_DC_SO_Sales_Count + l_SO_Sales_Count;
                l_DC_Annual_SO_Sales_Count := l_DC_Annual_SO_Sales_Count + l_SO_Sales_Count;
                
                begin                
                    Select  new_category_id
                            , sales_velocity_category
                    into    l_new_cat
                            , l_new_cat_desc
                    From    XXWC_SALES_VELOCITY_GT svgt
                    Where   inventory_item_id = Sales_Count_Rec.INventory_Item_ID
                    and     Organization_ID = p_Organization_ID ;
                exception
                when others then
                    l_new_cat := null;
                end;
                
                Fnd_File.Put_Line ( Fnd_File.LOG, 'l_new_cat_desc  :' || l_new_cat_desc);
                
                If l_Warehouse_Mode  = G_WHOUSE_MODE_DC  THen
                   /*Get_DC_Sales_Count(p_Inventory_Item_ID     => Sales_Count_Rec.INventory_Item_ID,
                                     p_DC_Organization_ID     => p_Organization_ID,
                                     x_DC_Sales_Count         => l_DC_SO_Sales_Count,
                                     X_DC_Annual_Sales_Count  => l_DC_Annual_SO_Sales_Count) ;

                   -- Add Current DC SO Count
                   l_DC_SO_Sales_Count := l_DC_SO_Sales_Count + l_SO_Sales_Count ;*/
                   
                   Fnd_File.Put_Line ( Fnd_File.LOG, 'Item Number  :' || Sales_Count_Rec.Inventory_Item_ID);
                   Fnd_File.Put_Line ( Fnd_File.LOG, 'DC Sales Count  :' || Nvl(l_DC_SO_Sales_Count,0));
                   -- Get Sales Velocity Count for each of the items for N weeks
                   IF l_new_cat IS NULL THEN
                       For Get_Category_Rec In Get_Category_CUr (p_Lookup_Type   => l_Lookup_TYpe ,
                                                                 p_Sales_Count   => Nvl(l_DC_SO_Sales_Count,0) ) Loop
                           l_Category := Get_Category_Rec.Lookup_Code ;
                       End Loop;
                   END IF;
                Else
                
                   Fnd_File.Put_Line ( Fnd_File.LOG, 'Item Number  :' || Sales_Count_Rec.Inventory_Item_ID);
                   Fnd_File.Put_Line ( Fnd_File.LOG, 'Sales Count  :' || l_SO_Sales_Count);
                   -- Get Sales Velocity Count for each of the items for N weeks
                   IF l_new_cat IS NULL THEN
                        -- 12/10/12 CG: Changed to use DC count per MD050
                       For Get_Category_Rec In Get_Category_CUr (p_Lookup_Type   => l_Lookup_TYpe ,
                                                                 -- p_Sales_Count   => NVL(l_SO_Sales_Count,0) ) Loop
                                                                 p_Sales_Count   => Nvl(l_DC_SO_Sales_Count,0) ) Loop
                          l_Category := Get_Category_Rec.Lookup_Code ;
                       End Loop;
                   END IF;
                
                ENd If;

                -- Get Category ID for a given Sales Velocity Category
                For Get_Category_ID_Rec In Get_Category_ID_Cur(p_Category  => l_Category ) Loop
                    l_Category_ID := Get_Category_ID_Rec.Category_ID ;
                End Loop;
                
                Fnd_File.Put_Line ( Fnd_File.LOG, 'l_Category_ID  :'||l_Category_ID);
                
                If l_Warehouse_Mode  = G_WHOUSE_MODE_DC  THen
                   -- Update Global Temp Table
                   Update XXWC_SALES_VELOCITY_GT
                      Set SALES_VELOCITY_CATEGORY      = NVL(l_new_cat_desc,l_Category),
                        NEW_CATEGORY_ID                = NVL(l_new_cat,l_Category_ID),
                        ANNUAL_SALES_VELOCITY_COUNT    = NVL(l_SO_Sales_Count,0),
                        DC_ANNUAL_SALES_VELOCITY_COUNT = NVL(l_DC_Annual_SO_Sales_Count,0)
                   Where Inventory_Item_ID             = Sales_Count_REc.INventory_Item_ID
                   And   Organization_ID               =  p_Organization_ID ;
                Else
                   -- Sales_Velocity Update to 0
                   -- Update Global Temp Table
                   Update XXWC_SALES_VELOCITY_GT
                      Set SALES_VELOCITY_CATEGORY      = NVL(l_new_cat_desc, l_Category),
                          NEW_CATEGORY_ID                = NVL(l_new_cat, l_Category_ID),
                          -- 10/18/12 CG: SALES_VELOCITY_COUNT           = 0,
                          ANNUAL_SALES_VELOCITY_COUNT    = NVL(l_SO_Sales_Count,0),
                          DC_ANNUAL_SALES_VELOCITY_COUNT = NVL(l_DC_Annual_SO_Sales_Count,0)
                   Where Inventory_Item_ID             = Sales_Count_REc.INventory_Item_ID
                   And   Organization_ID               =  p_Organization_ID ;
                End If;
                -- Commit Transactions after checking Records Count
                If l_Record_Count >= G_COMMIT_RECORDS_COUNT Then
                   Commit ;
                   l_Record_Count := 0;
                End If;
            Else
               -- Do Nothing Just Skip the record
               NULL;
            End If;
        EXCEPTION
           WHEN OTHERS THEN
              Fnd_File.Put_Line ( Fnd_File.LOG, ' When Others exception for API :'||  l_API_Name || ' : Error Message is  :'  || SQLERRM);
              Fnd_File.Put_Line ( Fnd_File.LOG,' Processing Item ID :' || Sales_Count_REc.Inventory_Item_ID || ' from warehoues :' || G_ORGANIZATION_CODE );
              G_Return_Status := 'E' ;
        End;
        ENd Loop;

        For No_Sales_Rec In No_Sales_Cur (p_Organization_Id => p_Organization_ID ) Loop
        Begin
           --initialize Variables
           l_Category    := NULL;
           l_Category_ID := NULL;
           l_SO_Sales_Count           := 0;
           l_RMA_Count                := 0;
           l_DC_SO_Sales_Count        := 0;
           l_DC_Annual_SO_Sales_Count := 0;
           Fnd_File.Put_Line ( Fnd_File.LOG, 'Inside the Loop 200 :' );
           If l_Warehouse_Mode  = G_WHOUSE_MODE_DC  THen
               Get_DC_Sales_Count(p_Inventory_Item_ID     => No_Sales_Rec.Inventory_Item_ID,
                                 p_DC_Organization_ID     => p_Organization_ID,
                                 x_DC_Sales_Count         => l_DC_SO_Sales_Count,
                                 X_DC_Annual_Sales_Count  => l_DC_Annual_SO_Sales_Count) ;


               -- Get Sales Velocity Count for each of the items for N weeks
               For Get_Category_Rec In Get_Category_CUr (p_Lookup_Type   => l_Lookup_TYpe ,
                                                         p_Sales_Count   => l_DC_SO_Sales_Count ) Loop
                   l_Category := Get_Category_Rec.Lookup_Code ;
               End Loop;
            Else
               -- Get Sales Velocity Count for each of the items for N weeks
               For Get_Category_Rec In Get_Category_CUr (p_Lookup_Type   => l_Lookup_TYpe ,
                                                         p_Sales_Count   => 0 ) Loop
                  l_Category := Get_Category_Rec.Lookup_Code ;
               End Loop;

            ENd If;

            -- Get Category ID for a given Sales Velocity Category
            For Get_Category_ID_Rec In Get_Category_ID_Cur(p_Category  => l_Category ) Loop
                l_Category_ID := Get_Category_ID_Rec.Category_ID ;
            End Loop;

            If l_Warehouse_Mode  = G_WHOUSE_MODE_DC  THen
               -- Update Global Temp Table
               Update XXWC_SALES_VELOCITY_GT
                  Set SALES_VELOCITY_CATEGORY      = l_Category,
                    NEW_CATEGORY_ID                = l_Category_ID,
                    DC_SALES_VELOCITY_COUNT        = l_DC_SO_Sales_Count,
                    DC_ANNUAL_SALES_VELOCITY_COUNT = NVL(l_DC_Annual_SO_Sales_Count,0)
               Where Inventory_Item_ID             = No_Sales_Rec.Inventory_Item_ID
               And   Organization_ID               =  p_Organization_ID ;
            Else
               -- Update Global Temp Table
               Update XXWC_SALES_VELOCITY_GT
                  Set SALES_VELOCITY_CATEGORY      = l_Category,
                    NEW_CATEGORY_ID                = l_Category_ID,
                    SALES_VELOCITY_COUNT           = 0,
                    ANNUAL_SALES_VELOCITY_COUNT    = 0
               Where Inventory_Item_ID             = No_Sales_Rec.Inventory_Item_ID
               And   Organization_ID               =  p_Organization_ID ;
            End If;
            -- Commit Transactions after checking Records Count
            If l_Record_Count >= G_COMMIT_RECORDS_COUNT Then
               Commit ;
               l_Record_Count := 0;
            End If;
        Exception
           When Others Then
              Fnd_File.Put_Line ( Fnd_File.LOG, ' When Others exception for API :'||  l_API_Name || ' : Error Message is  :'  || SQLERRM);
              Fnd_File.Put_Line ( Fnd_File.LOG,' Processing Item ID :' || No_Sales_Rec.Inventory_Item_ID || ' from warehoues :' || G_ORGANIZATION_CODE );
              G_Return_Status := 'E' ;

        End;
        End Loop;
    ENd Sales_Velocity_Annual;

    -- Procedure RMA_WEEKS
    -- Procedure Calcuatates count of RMA for all the items and Updte GT Table.
    Procedure RMA_ANNUAL(P_Organization_Id   IN Number,
                        p_Inventory_Item_ID IN  Number,
                        x_RMA_Count         Out NOCOPY Number) IS
       -- Define a Cursor that gets all RMA Lines and its corresponding SO Lines
       -- 10/23/2012 CG: Altered to consider counter lines
       Cursor RMA_Cur (p_Organization_ID Number ) Is
       -- 12/12/12 CG: Commented out grouping by line if there are mult return against the same item
          Select Ret.Inventory_Item_ID, Ret.REFERENCE_HEADER_ID, -- Ret.REFERENCE_LINE_ID,
             Sum(Nvl(ret.Ordered_Quantity,0) - NVL(Ret.Cancelled_Quantity,0)) Return_Qty ,
             Sum(Nvl(shp.Ordered_Quantity,0) - NVL(shp.Cancelled_Quantity,0)) Ship_Qty
          --Modified the below line for TMS ## 20141002-00048  by VijaySrinivasan  on 11/13/2014 
          From oe_Order_Lines Ret,
               --Modified the below line for TMS ## 20141002-00048  by VijaySrinivasan  on 11/13/2014 
               oe_order_Lines shp,
               oe_transaction_types_tl ott
          Where Ret.LINE_CATEGORY_CODE = 'RETURN'
          AND   Ret.RETURN_CONTEXT     = 'ORDER'
          ANd   Ret.Flow_Status_Code = 'CLOSED'
          AND   Ret.REFERENCE_HEADER_ID = shp.Header_ID
          AND   Ret.REFERENCE_LINE_ID   = Shp.Line_ID
          AND   Ret.Inventory_Item_ID   = p_Inventory_Item_ID
          And   Ret.Inventory_Item_ID   = shp.Inventory_Item_ID
          AND   shp.Ship_from_Org_ID = p_ORganization_ID
          -- 10/23/2012 CG
          -- And   Trunc(shp.Actual_Shipment_Date) >  Trunc(G_DATE_ONE_YEAR_BACK)
          AND   shp.line_type_id = ott.transaction_type_id
          AND   Trunc(G_DATE_ONE_YEAR_BACK ) < (CASE WHEN ott.name != 'COUNTER LINE' THEN Trunc(shp.Actual_Shipment_Date)
                                                     ELSE Trunc(shp.fulfillment_Date)
                                                     END)
          -- 10/23/2012 CG
          Group by Ret.Inventory_Item_ID,Ret.REFERENCE_HEADER_ID; -- , Ret.REFERENCE_LINE_ID  ;
          /***************************
          And Not Exists (Select 1
                           From XXWC_SALES_VELOCITY_GT svgt
                           Where svgt.Inventory_Item_ID = shp.Inventory_Item_ID
                           And   svgt.Organization_ID  = shp.Ship_From_Org_ID
                           And   svgt.SALES_VELOCITY_CATEGORY in (G_CATEGORY_DISCONTINUED, G_CATEGORY_NON_STOCK,
                                   G_CATEGORY_NOT_REPLENISHED,G_CATEGORY_NEW  ) )
          ****************************/
         -- Get Sales Velocity Count for given Period from GT
         Cursor  SO_Cur (p_Organization_ID   Number,
                         p_Inventory_Item_ID Number ) Is
            Select Sales_Velocity_Count
            From XXWC_SALES_VELOCITY_GT
            Where Inventory_Item_ID =  p_INventory_Item_ID
            And   Organization_ID   =  p_Organization_ID ;

         l_Sales_Velocity_Count Number ;
         l_API_Name             Varchar2(30) := 'RAM_ANNUAL';
    Begin
      l_Sales_Velocity_Count := 0;
      For RMA_Rec In RMA_Cur(p_Organization_ID ) Loop
      Begin
         If (RMA_Rec.Return_Qty = RMA_Rec.Ship_Qty ) And RMA_Rec.Return_Qty <> 0  Then
             l_Sales_Velocity_Count := l_Sales_Velocity_Count + 1;
         End If;
      Exception
         When Others Then
            Fnd_File.Put_Line ( Fnd_File.LOG, ' When Others exception for API :'||  l_API_Name || ' : Error Message is  :'  || SQLERRM);
            Fnd_File.Put_Line ( Fnd_File.LOG,' Processing Item ID :' || p_Inventory_Item_ID || ' from warehoues :' || G_ORGANIZATION_CODE );
            G_Return_Status := 'E' ;
      End;
      ENd Loop;
      x_RMA_Count := l_Sales_Velocity_Count ;
    ENd RMA_ANNUAL;
    -- Procedure Get_DC_Sales_Count
    Procedure Get_DC_Sales_Count(p_Inventory_Item_ID     IN Number,
                                 p_DC_Organization_ID    IN Number,
                                 x_DC_Sales_Count        OUT NOCOPY Number,
                                 X_DC_Annual_Sales_Count OUT NOCOPY Number ) IS

        C_ASSIGNMENT_TYPE_ITEM     CONSTANT   Number := 3;
        C_ASSIGNMENT_TYPE_ITEM_ORG CONSTANT   Number := 6;
        C_ASSIGNMENT_TYPE_ORG      CONSTANT   Number := 4;
        C_ASSIGNMENT_TYPE_GLOBAL   CONSTANT   Number := 1;
        C_SOURCE_TYPE_TRANSFER     CONSTANT   Number := 1;
        l_Default_Assignment_Set_ID        Number ;
       -- Cursor to get the list of warehouses that sources from DC

       Cursor   whse_list_Cur (p_Inventory_Item_ID   Number,
                               p_DC_Organization_ID  Number ) Is    
       select msib.organization_id,
               mp.organization_code
        from   mtl_system_items_b msib,
               mtl_parameters mp
        where  msib.source_organization_id = p_DC_Organization_ID
        and    msib.source_type = 1 --Internally_Sourced
        and    msib.inventory_item_id = p_Inventory_Item_ID
        and    msib.source_organization_id = mp.organization_id;

        -- 10/22/2012 CG: Changed to use internal sourcing setup on item master
        /*Select msro.Receipt_ORganization_ID  Organization_ID, mra.Assignment_set_Id Assignment_Set_ID
         From MRP_SR_RECEIPT_ORG msro,
              MRP_SOURCING_RULES msr ,
              MRP_SR_SOURCE_ORG msso,
              MRP_SR_ASSIGNMENTS mra
         Where 1= 1
         And   mra.Assignment_set_ID       = l_Default_Assignment_Set_ID
         And   mra.INventory_item_ID       = p_Inventory_Item_ID
         and   mra.assignment_type         = C_ASSIGNMENT_TYPE_ITEM
         and   mra.Sourcing_RUle_ID        = msr.Sourcing_Rule_ID
         and   msr.Organization_id         = msro.RECEIPT_ORGANIZATION_ID
         and   msr.Sourcing_Rule_ID        = msro.Sourcing_Rule_ID
         and   msro.SR_Receipt_ID          = msso.SR_Receipt_ID
         and   msso.Source_Type            = C_SOURCE_TYPE_TRANSFER
         and   msso.Source_organization_Id = p_DC_Organization_ID
         Union
         Select msro.Receipt_ORganization_ID Organization_ID,mra.Assignment_set_Id Assignment_Set_ID
         From MRP_SR_RECEIPT_ORG msro,
              MRP_SOURCING_RULES msr ,
              MRP_SR_SOURCE_ORG msso,
              MRP_SR_ASSIGNMENTS mra
         Where 1 = 1
         And   mra.Assignment_set_ID    = l_Default_Assignment_Set_ID
         and   mra.assignment_type      = C_ASSIGNMENT_TYPE_ITEM_ORG
         And   mra.INventory_item_ID    = p_Inventory_Item_ID
         and   mra.Organization_Id      = msro.Receipt_Organization_Id
         and   mra.sourcing_rule_Id     = msr.Sourcing_Rule_ID
         and   msr.Sourcing_Rule_ID     = msro.Sourcing_Rule_ID
         and   msro.SR_Receipt_ID       = msso.SR_Receipt_ID
         and   msso.Source_Type         = C_SOURCE_TYPE_TRANSFER
         and   msso.Source_organization_Id = p_DC_Organization_ID
         Union
         Select msro.Receipt_ORganization_ID Organization_ID, mra.Assignment_Set_ID Assignment_Set_ID
         From MRP_SR_RECEIPT_ORG_V msro,
              MRP_SOURCING_RULES msr ,
              MRP_SR_SOURCE_ORG_V msso,
              MRP_SR_ASSIGNMENTS mra
         Where 1 =1
         And   mra.Assignment_set_ID    = l_Default_Assignment_Set_ID
         and   mra.assignment_type      =  C_ASSIGNMENT_TYPE_ORG
         and   mra.Organization_Id      = msro.Receipt_Organization_Id
         and   msr.Organization_id      = msro.RECEIPT_ORGANIZATION_ID
         and   mra.sourcing_rule_Id     = msr.Sourcing_Rule_ID
         and   msr.Sourcing_Rule_ID     = msro.Sourcing_Rule_ID
         and   msso.Source_Type         = C_SOURCE_TYPE_TRANSFER
         and   msso.Source_organization_Id = p_DC_Organization_ID
         Union
         Select mra.ORganization_ID Organization_ID, mra.Assignment_set_Id Assignment_Set_ID
         From MRP_SR_RECEIPT_ORG_V msro,
              MRP_SOURCING_RULES msr ,
              MRP_SR_SOURCE_ORG_V msso,
              MRP_SR_ASSIGNMENTS mra
         Where 1= 1
         And   mra.Assignment_set_ID  = l_Default_Assignment_Set_ID
         and   mra.assignment_type      =  C_ASSIGNMENT_TYPE_ORG
         and   mra.sourcing_rule_Id     = msr.Sourcing_Rule_ID
         and   msr.Sourcing_Rule_ID     = msro.Sourcing_Rule_ID
         and   msro.Receipt_Organization_Id is NULL
         and   msr.Organization_Id Is NULL
         and   msso.Source_Type      = C_SOURCE_TYPE_TRANSFER
         and   msso.Source_organization_Id = p_DC_Organization_ID ;*/


        l_Sales_Velocity                   Number ;
        l_Annual_Sales_Velocity            Number ;
        l_API_Name                         Varchar2(30);
        l_item_id                          NUMBER;

    Begin
       x_DC_Sales_Count        := 0;
       X_DC_Annual_Sales_Count := 0;

       Fnd_File.Put_Line ( Fnd_File.LOG,',Beging of API :' || l_API_Name ) ;
       Begin
          select profile_option_value
          Into l_Default_Assignment_Set_ID
          from   fnd_profile_option_values fpov,
                 fnd_profile_options  fpo
          Where fpo.PROFILE_OPTION_NAME = 'MRP_DEFAULT_ASSIGNMENT_SET'
          and   fpo.Profile_OPtion_Id   = fpov.Profile_Option_Id;
       Exception
          When Others Then
             l_Default_Assignment_Set_ID := FND_PROFILE.Value('MRP_DEFAULT_ASSIGNMENT_SET');
       ENd ;

       For whse_list_Rec in whse_list_Cur(p_Inventory_Item_ID  => p_Inventory_Item_Id,
                                          p_DC_Organization_ID => p_DC_Organization_ID ) Loop
       Begin
          l_Sales_Velocity := 0;
          l_Annual_Sales_Velocity  := 0;
          l_item_id                := null;
          
          /*Select To_Number(Nvl(Attribute12,0)), To_Number(nvl(Attribute14,0))
          Into l_Sales_Velocity , l_Annual_Sales_Velocity
          From Mtl_System_Items_b
          Where Organization_ID   = Whse_List_Rec.Organization_ID
          And   Inventory_Item_ID = p_Inventory_Item_ID ;*/
          
          -- 10/22/2012 CG
          -- Weekly
          Select distinct ool.Inventory_Item_Id, COUNT ( * ) OVER (PARTITION BY ool.inventory_item_id) Sales_Count
           INTO l_item_id, l_Sales_Velocity
           --Modified the below line for TMS ## 20141002-00048  by VijaySrinivasan  on 11/13/2014 
           From Oe_Order_Lines  ool ,
                --Modified the below line for TMS ## 20141002-00048  by VijaySrinivasan  on 11/13/2014 
                oe_order_headers ooh, 
                oe_transaction_types_tl ott
           Where ool.Ship_From_Org_ID = Whse_List_Rec.Organization_ID
           -- 10/23/2012 CG
           And  ( 
                    (ott.name != 'COUNTER LINE' and Trunc(ool.Actual_Shipment_Date) >  Trunc(( Sysdate - (G_NUMBER_OF_WEEKS * 7) )))            
                    OR 
                    (ott.name = 'COUNTER LINE' and Trunc(ool.fulfillment_Date) >  Trunc(( Sysdate - (G_NUMBER_OF_WEEKS * 7) )))
                )
           -- 10/23/2012 CG
           ANd  ool.Flow_Status_Code = 'CLOSED'
           AND  OOL.LINE_CATEGORY_CODE = 'ORDER'
           AND  ool.INVOICE_INTERFACE_STATUS_CODE = 'YES'
           AND  ool.inventory_item_id = p_Inventory_Item_ID
           -- 10/23/2012 CG
           AND  ool.header_id = ooh.header_id
           AND  ooh.order_number is not null
           AND  ool.line_type_id = ott.transaction_type_id
           -- 10/23/2012 CG
           And Not Exists ( Select 1
                            From Mtl_Parameters Mp
                            Where  mp.Organization_ID = ool.Ship_to_Org_ID )
           Group By ool.Header_Id, ool.Inventory_Item_ID;
           
           
           -- Annual
           Select distinct ool.Inventory_Item_Id, COUNT ( * ) OVER (PARTITION BY ool.inventory_item_id) Sales_Count
           INTO l_item_id, l_Annual_Sales_Velocity
          --Modified the below line for TMS ## 20141002-00048  by VijaySrinivasan  on 11/13/2014 
           From Oe_Order_Lines  ool ,
                --Modified the below line for TMS ## 20141002-00048  by VijaySrinivasan  on 11/13/2014 
                oe_order_headers ooh, 
                oe_transaction_types_tl ott
           Where ool.Ship_From_Org_ID = Whse_List_Rec.Organization_ID
           -- 10/23/2012 CG
           -- And   Trunc(ool.Actual_Shipment_Date) >  Trunc(G_DATE_ONE_YEAR_BACK )
           And  ( 
                    (ott.name != 'COUNTER LINE' and Trunc(ool.Actual_Shipment_Date) >  Trunc(G_DATE_ONE_YEAR_BACK ) )            
                    OR 
                    (ott.name = 'COUNTER LINE' and Trunc(ool.fulfillment_Date) >  Trunc(G_DATE_ONE_YEAR_BACK ) )
                )
           -- 10/23/2012 CG
           ANd  ool.Flow_Status_Code = 'CLOSED'
           AND  OOL.LINE_CATEGORY_CODE = 'ORDER'
           AND  ool.INVOICE_INTERFACE_STATUS_CODE = 'YES'
           AND  ool.inventory_item_id = p_Inventory_Item_ID
           -- 10/23/2012 CG
           AND  ool.header_id = ooh.header_id
           AND  ooh.order_number is not null
           AND  ool.line_type_id = ott.transaction_type_id
           -- 10/23/2012 CG
           And Not Exists ( Select 1
                            From Mtl_Parameters Mp
                            Where  mp.Organization_ID = ool.Ship_to_Org_ID )
           Group By  ool.Inventory_Item_ID,ool.Header_Id;
          
          
          x_DC_Sales_Count        := x_DC_Sales_Count + l_Sales_Velocity ;
          x_DC_Annual_Sales_Count := x_DC_Annual_Sales_Count + l_Annual_Sales_Velocity ;
          Fnd_File.Put_Line ( Fnd_File.LOG,',x_DC_Sales_Count :' || x_DC_Sales_Count || 'For Organization Id :' || Whse_list_Rec.Organization_ID ) ;
          Fnd_File.Put_Line ( Fnd_File.LOG,',x_DC_Annual_Sales_Count :' || x_DC_Annual_Sales_Count || 'For Organization Id :' || Whse_list_Rec.Organization_ID ) ;
       Exception
          When Others Then
              NULL;

       End;
       End Loop;

    End Get_DC_Sales_Count;

    -- Following Procedure will Create Item Cateogyr if it does not exist.
   -- If Exists it updates Item Category

   Procedure  Update_Item_Category(p_Organization_Id   Number,
                                   p_Inventory_Item_ID Number ) Is
       Cursor Item_Cat_Cur (p_Organization_Id   Number,
                            p_Inventory_Item_ID Number ) Is
            Select *
            From  XXWC_SALES_VELOCITY_GT
            Where Inventory_Item_ID =  p_INventory_Item_ID
            And   Organization_ID   =  p_Organization_ID ;

      l_Msg_Data      Varchar2(2000) ;
      l_Return_Status Varchar2(1) ;
      l_Msg_Count     Number;
      l_errorcode     Varchar2(240);
      l_API_Name      Varchar2(30) := 'UPDATE_ITEM_CATEGORY';
   Begin
       For Item_Cat_Rec In Item_Cat_Cur ( p_Organization_Id => p_Organization_Id ,
                                          p_Inventory_Item_ID => p_Inventory_Item_ID ) Loop
       Begin
          If Item_Cat_Rec.New_Category_ID Is  NULL Then
             Fnd_File.Put_Line ( Fnd_File.LOG, 'NEW Category ID is Null :'  );
             Fnd_File.Put_Line ( Fnd_File.LOG, 'Inventory Item ID Is :' || p_Inventory_Item_ID );
             Fnd_File.Put_Line ( Fnd_File.LOG, 'ORganization ID Is :' || p_Organization_Id );
             G_Return_Status := 'E' ;
          End If;
          If Item_Cat_Rec.Old_Category_ID Is Null
             ANd Item_Cat_Rec.New_Category_ID Is NOT NULL Then
              -- Add Item to existing Category
              Fnd_File.Put_Line ( Fnd_File.LOG, 'Creating Category Assignment for item ' || Item_Cat_Rec.Segment1);
              INV_ITEM_CATEGORY_PUB.Create_Category_Assignment
                   (
                   p_api_version          =>1.0
                  ,p_init_msg_list        =>'T'
                  ,p_commit               =>'T'
                  ,p_category_id          =>Item_Cat_Rec.New_category_id
                  ,p_category_set_id      =>g_category_set_id
                  ,p_inventory_item_id    =>p_Inventory_Item_ID
                  ,p_organization_id      =>p_organization_id
                  ,x_return_status        =>l_return_status
                  ,x_errorcode            =>l_errorcode
                  ,x_msg_count            =>l_msg_count
                  ,x_msg_data             =>l_msg_data
                   );
                IF l_Return_Status <> 'S' Then
                    FOR K IN 1 .. L_MSG_COUNT LOOP
                       L_MSG_DATA := FND_MSG_PUB.GET( P_MSG_INDEX => K,    P_ENCODED => 'F');

                    END LOOP;
                    Fnd_File.Put_Line ( Fnd_File.LOG, 'Error while assigning item to Item Category:' || l_Msg_Data);

                End If;

          Elsif NVL(Item_Cat_Rec.Old_Category_ID,0) <> NVL(Item_Cat_Rec.New_Category_ID,0) Then
              Fnd_File.Put_Line ( Fnd_File.LOG, 'Updating Category Assignment for item ' || Item_Cat_Rec.Segment1);
              -- Update Item Category
              Fnd_File.Put_Line ( Fnd_File.LOG, 'Inventory_Item ID is ' || p_Inventory_Item_ID);
              INV_ITEM_CATEGORY_PUB.Update_Category_Assignment
                   (
                     p_api_version       => 1.0,
                     p_init_msg_list     =>'T' ,
                     p_commit            =>'T' ,
                     p_category_id       => Item_Cat_Rec.New_category_id,
                     p_old_category_id   => Item_Cat_Rec.Old_category_id,
                     p_category_set_id   => G_CATEGORY_SET_ID,
                     p_inventory_item_id => p_Inventory_Item_ID,
                     p_organization_id   => p_Organization_ID,
                     x_return_status     => l_Return_Status,
                     x_errorcode         => l_ErrorCode,
                     x_msg_count         => l_Msg_Count,
                     x_msg_data          => l_Msg_Data  );
              IF l_Return_Status <> 'S' Then
                    FOR K IN 1 .. L_MSG_COUNT LOOP
                       L_MSG_DATA := FND_MSG_PUB.GET( P_MSG_INDEX => K,    P_ENCODED => 'F');

                    END LOOP;
                    Fnd_File.Put_Line ( Fnd_File.LOG, 'Error while Updating tem Category:' || l_Msg_Data);
                    Fnd_File.Put_Line ( Fnd_File.LOG, 'Inventory Item  :' || Item_Cat_Rec.Segment1 );
                    Fnd_File.Put_Line ( Fnd_File.LOG, 'ORganization ID Is :' || p_Organization_Id );
              Else
                 Fnd_File.Put_Line ( Fnd_File.LOG, 'Update Category Assignment Successfull for Item :' || Item_Cat_Rec.Segment1);
              End If;
          End If;
       Exception
          When Others Then
             Fnd_File.Put_Line ( Fnd_File.LOG, ' When Others exception for API :'||  l_API_Name || ' : Error Message is  :'  || SQLERRM);
             Fnd_File.Put_Line ( Fnd_File.LOG,' Processing Item :' || Item_Cat_REc.Segment1 || ' from warehoues :' || G_ORGANIZATION_CODE );
             G_Return_Status := 'E' ;
       End;
       End Loop;

   End Update_Item_Category;

   --Procedure to Update Sales Velocity Values
   -- Procedure to Update Sales Velocity values
    Procedure  Update_Sales_Velocity(P_Organization_ID Number)  IS
       --Define a Cursor to read values from GT
       Cursor  Sales_Vel_Cur (P_Organization_ID Number ) Is
          Select *
          From XXWC_SALES_VELOCITY_GT svgt
          Where svgt.Organization_ID = p_Organization_ID ;

       -- Cursor to get a Lock on MTL_SYSTEM_ITEMS_B Reocrd
      Cursor Item_Cur(p_Inventory_Item_ID  Number
                    , p_Organization_ID    Number ) Is
         Select Inventory_Item_ID, Organization_ID, Attribute12, Attribute13, Attribute14, Attribute15
         From Mtl_System_Items_B
         Where Inventory_Item_ID = p_Inventory_Item_ID
         And   Organization_ID   = p_Organization_ID
         For Update Nowait ;

         l_TOtal_Items    Number ;
         l_API_Name       Varchar2(30) := 'UPADATE_SALES_VELOCITY';
    Begin
        FOr Sales_Vel_Rec In Sales_Vel_Cur (p_Organization_ID => p_Organization_ID ) Loop
          Begin
             FOr Item_Rec In Item_Cur(p_Inventory_Item_Id => Sales_Vel_Rec.Inventory_Item_ID ,
                                      p_Organization_ID  => Sales_Vel_Rec.Organization_ID ) Loop
                 If G_WHOUSE_MODE = G_WHOUSE_MODE_DC Then
                    Update Mtl_System_Items_B
                    Set Attribute12 = Sales_Vel_Rec.Sales_Velocity_Count,
                        Attribute13 = Sales_Vel_Rec.DC_Sales_Velocity_Count,
                        Attribute14 = Sales_Vel_Rec.Annual_Sales_Velocity_Count,
                        Attribute15 = Sales_Vel_Rec.DC_Annual_Sales_Velocity_Count
                    Where Inventory_Item_ID = Item_Rec.Inventory_Item_ID
                    And   Organization_ID   = Item_Rec.Organization_ID
                    And   (To_Number(NVL(Attribute12,'-1')) <> NVL(Sales_Vel_Rec.Sales_Velocity_Count,-1)
                     OR To_Number(NVL(Attribute13,'-1')) <> NVL(Sales_Vel_Rec.DC_Sales_Velocity_Count,-1)
                     Or To_Number(NVL(Attribute14,'-1')) <> NVL(Sales_Vel_Rec.Annual_Sales_Velocity_Count,-1)
                     OR To_Number(NVL(Attribute15,'-1')) <> NVL(Sales_Vel_Rec.DC_Annual_Sales_Velocity_Count,-1)) ;
                 Else
                    Update Mtl_System_Items_B
                    Set Attribute12 = Sales_Vel_Rec.Sales_Velocity_Count,
                        Attribute13 = Sales_Vel_Rec.DC_Sales_Velocity_Count,
                        Attribute14 = Sales_Vel_Rec.Annual_Sales_Velocity_Count,
                        Attribute15 = Sales_Vel_Rec.DC_Annual_Sales_Velocity_Count
                    Where Inventory_Item_ID = Item_Rec.Inventory_Item_ID
                    And   Organization_ID   = Item_Rec.Organization_ID
                    And   (To_Number(NVL(Attribute12,'-1')) <> NVL(Sales_Vel_Rec.Sales_Velocity_Count,-1)
                     Or To_Number(NVL(Attribute13,'-1')) <> NVL(Sales_Vel_Rec.Sales_Velocity_Count,-1)
                     Or To_Number(NVL(Attribute14,'-1')) <> NVL(Sales_Vel_Rec.Annual_Sales_Velocity_Count,-1)
                     OR To_Number(NVL(Attribute15,'-1')) <> NVL(Sales_Vel_Rec.DC_Annual_Sales_Velocity_Count,-1)) ;
                 End If;

             ENd Loop ;
             Update_Item_Category(p_Organization_Id   => p_Organization_ID,
                               p_Inventory_Item_ID => Sales_Vel_Rec.Inventory_Item_ID );

          Exception
             When Others Then
                 -- Fnd_File.Put_Line ( fnd_file.output, 'When Others Error Processing Item :' || Sales_Vel_Rec.Segment1 || ' from warehoues :' || G_ORGANIZATION_CODE );
                 Fnd_File.Put_Line ( fnd_file.LOG, 'When Others Error Processing Item :' || Sales_Vel_Rec.Segment1 || ' from warehoues :' || G_ORGANIZATION_CODE );
                 Fnd_File.Put_Line ( Fnd_File.LOG, 'When Others Exception :' || SQLERRM);
                 Fnd_File.Put_Line ( Fnd_File.LOG,' Processing Item ID :' || Sales_Vel_Rec.Inventory_Item_ID || ' from warehoues :' || G_ORGANIZATION_CODE );
                 G_Return_Status := 'E' ;
          End ;

        End Loop;
        --- Get Statistical Information and populate Error Log and Out Files accordingly
       Select Count(*)
       Into l_Total_Items
       From XXWC_SALES_VELOCITY_GT
       Where  New_Category_ID is Not Null;


       Fnd_File.Put_Line ( Fnd_File.LOG, 'Total Number of Items Processed  :' ||l_TOtal_Items );
       Fnd_File.Put_Line ( Fnd_File.LOG,'Warehouse:' ||G_Organization_Code );
       
       -- 11/28/12 CG: Changed to generate XML Tags for Reporting
       -- Fnd_File.Put_Line ( fnd_file.output, 'Total Number of Items Processed  :' ||l_TOtal_Items );
       -- Fnd_File.Put_Line ( fnd_file.output,'Warehouse:' ||G_Organization_Code );
       Fnd_File.Put_Line ( Fnd_File.output,'<PROCESSED_ITEM_COUNT>'||l_TOtal_Items||'</PROCESSED_ITEM_COUNT>');

       Fnd_File.Put_Line ( Fnd_File.LOG, 'End of Populate_GT API :' );
    End Update_Sales_Velocity;
    
    
    -- 11/28/12 CG: Procedure to generate report of before and after value for sales velocity
    Procedure  Sales_Velocity_Report (P_Organization_ID Number) IS
    
        cursor  cur_updated_items is
                select  msib.segment1 item_number
                        , Attribute12 Cur_SV_Count
                        , Attribute14 Cur_Annual_SV_Count
                        , Attribute13 Cur_DC_SV_Count
                        , Attribute15 Cur_DC_Annual_SV_Count
                        , mic.category_concat_segments cur_sv_category
                        , svgt.sales_velocity_count new_sv_count
                        , svgt.annual_sales_velocity_count new_annual_sv_count
                        , svgt.dc_sales_velocity_count new_dc_sv_count
                        , svgt.dc_annual_sales_velocity_count new_dc_annual_sv_count
                        , (select segment1 from  mtl_categories_b where category_id = svgt.new_category_id) new_sv_category
                from    XXWC_SALES_VELOCITY_GT svgt
                        , mtl_system_items_b msib
                        , apps.xxwc_sv_item_categories_v mic
                where   svgt.organization_id = p_organization_id
                and     svgt.inventory_item_id = msib.inventory_item_id
                and     svgt.organization_id = msib.organization_id
                -- 12/03/12 CG
                and     nvl(msib.item_type,'ZZZ') != 'SPECIAL'
                and     msib.segment1 not like 'SP/%'
                -- 12/03/12 CG
                and     msib.inventory_item_id = mic.inventory_item_id (+)
                and     msib.organization_id = mic.organization_id (+)
                and     mic.Category_Set_ID(+) = To_Number(Fnd_Profile.Value('XXWC_SALES_VELOCITY_CATEOGRY_SET'));
    
    Begin
        
        Fnd_File.Put_Line ( Fnd_File.output,'<LIST_G_ITEMS>');
        
        for c1 in cur_updated_items
        loop
            exit when cur_updated_items%notfound;
            
            Fnd_File.Put_Line ( Fnd_File.output,'<G_ITEMS>');
            
            Fnd_File.Put_Line ( Fnd_File.output,'<ITEM_NUMBER>'|| c1.item_number ||'</ITEM_NUMBER>');
            Fnd_File.Put_Line ( Fnd_File.output,'<CUR_SV_COUNT>'|| c1.Cur_SV_Count ||'</CUR_SV_COUNT>');
            Fnd_File.Put_Line ( Fnd_File.output,'<CUR_ANNUAL_SV_COUNT>'|| c1.Cur_Annual_SV_Count ||'</CUR_ANNUAL_SV_COUNT>');
            Fnd_File.Put_Line ( Fnd_File.output,'<CUR_DC_SV_COUNT>'|| c1.Cur_DC_SV_Count ||'</CUR_DC_SV_COUNT>');
            Fnd_File.Put_Line ( Fnd_File.output,'<CUR_DC_ANNUAL_SV_COUNT>'|| c1.Cur_DC_Annual_SV_Count ||'</CUR_DC_ANNUAL_SV_COUNT>');
            Fnd_File.Put_Line ( Fnd_File.output,'<CUR_SV_CATEGORY>'|| c1.cur_sv_category ||'</CUR_SV_CATEGORY>');
            Fnd_File.Put_Line ( Fnd_File.output,'<NEW_SV_COUNT>'|| c1.new_sv_count ||'</NEW_SV_COUNT>');
            Fnd_File.Put_Line ( Fnd_File.output,'<NEW_ANNUAL_SV_COUNT>'|| c1.new_annual_sv_count ||'</NEW_ANNUAL_SV_COUNT>');
            Fnd_File.Put_Line ( Fnd_File.output,'<NEW_DC_SV_COUNT>'|| c1.new_dc_sv_count ||'</NEW_DC_SV_COUNT>');
            Fnd_File.Put_Line ( Fnd_File.output,'<NEW_DC_ANNUAL_SV_COUNT>'|| c1.new_dc_annual_sv_count ||'</NEW_DC_ANNUAL_SV_COUNT>');
            Fnd_File.Put_Line ( Fnd_File.output,'<NEW_SV_CATEGORY>'|| c1.new_sv_category ||'</NEW_SV_CATEGORY>');
            
            Fnd_File.Put_Line ( Fnd_File.output,'</G_ITEMS>');
            
        end loop;
        
        Fnd_File.Put_Line ( Fnd_File.output,'</LIST_G_ITEMS>');
    exception
    when others then
        null;
    END Sales_Velocity_Report;
    
    
   -- Main Procedure to calculate Sales Velocity for all Items for a Given Warehouse.
   -- If Warehouse information is not passed then the program calculates

   Procedure  MAIN (Errbuf                      OUT NOCOPY VARCHAR2,
                    Retcode                     OUT NOCOPY NUMBER,
                    p_Organization_Code         IN VARCHAR2,
                    p_Debug_Flag                IN VARCHAR2 DEFAULT 'N', 
                    p_print_detail               IN VARCHAR2 DEFAULT 'N' ) IS
       -- Declare Local Variables
       l_Return_Status          Varchar2(1) ;
       l_Error_Message          Varchar2(2000);
       l_API_NAME               VARCHAR2(80) := '.MAIN';
       l_Module_Name            Varchar2(80) := 'XXWC_INV_SALES_VELOCITY_PKG';

       l_err_callpoint                      VARCHAR2(175) := 'START';
       l_distro_list                        VARCHAR2(80) := 'OracleDevelopmentGroup@hdsupply.com';
       -- Define Cursors
       -- Cursor to get all warehouses thare marked as Store or DC

       Cursor  Cur_warehouse  Is
          Select Organization_Id, Organization_Code, Attribute7
          From Mtl_Parameters
          Where Attribute7 in (G_WHOUSE_MODE_STORE, G_WHOUSE_MODE_DC)
          And   ( p_Organization_Code is Null Or
                  Organization_Code = p_Organization_Code )
          Order by Organization_Code, Attribute7 asc ;

      -- Cursor to get a Lock on MTL_SYSTEM_ITEMS_B Reocrd
      Cursor Cur_Item_Sales_Velocity(p_Inventory_Item_ID  Number
                                   , p_Organization_ID    Number ) Is
         Select Inventory_Item_ID, Organization_ID, Attribute12, Attribute13, Attribute14, Attribute15
         From Mtl_System_Items_B
         Where Inventory_Item_ID = p_Inventory_Item_ID
         And   Organization_ID   = p_Organization_ID
         For Update Nowait ;



   Begin
      fnd_file.put_line (fnd_file.output,'<?xml version="1.0"?>');
      Fnd_File.Put_Line ( Fnd_File.output,'<XXWC_INV_SALES_VELOCITY>');
      Fnd_File.Put_Line ( Fnd_File.output,'<P_ORG_PARAMETER>'||p_Organization_Code||'</P_ORG_PARAMETER>');
      Fnd_File.Put_Line ( Fnd_File.output,'<P_DEBUG_PARAMETER>'||p_Organization_Code||'</P_DEBUG_PARAMETER>');
   
      ---Initialize l_Return_Stauts Variable
      l_Return_Status := 'S';
      -- Assign Value To Debug Flag
      G_DEBUG_FLAG       := p_Debug_Flag ;
      G_RETURN_STATUS    := 'S';

      l_MODULE_NAME      := G_MODULE_NAME || l_API_NAME ;
      Fnd_File.Put_Line ( Fnd_File.LOG, 'Begning of Main API :' );
      Fnd_File.Put_Line ( Fnd_File.LOG, 'Organization Code passed is  :' || p_Organization_Code);
      Fnd_File.Put_Line ( Fnd_File.LOG, 'Debug Flag value is  :' || P_Debug_Flag);
      
      Fnd_File.Put_Line ( Fnd_File.output,'<LIST_G_WAREHOUSE>');
      For Rec_warehouse   in Cur_warehouse  Loop
      Begin
      
          Fnd_File.Put_Line ( Fnd_File.output,'<G_WAREHOUSE>');
          Fnd_File.Put_Line ( Fnd_File.output,'<ORG_CODE>'||Rec_Warehouse.Organization_Code||'</ORG_CODE>');
          Fnd_File.Put_Line ( Fnd_File.output,'<WHS_MODE>'||Rec_Warehouse.Attribute7||'</WHS_MODE>');
          
          l_err_callpoint := '110 :  Inside For Loop Rec Warehouse:' ;
          -- Assign Warehouse Mode to Global Variable.
          G_WHOUSE_MODE       := Rec_Warehouse.Attribute7 ;
          G_ORGANIZATION_CODE := Rec_Warehouse.Organization_Code ;
          
          -- Delete all the records from Global Temprary Tables.
          Fnd_File.Put_Line ( Fnd_File.LOG, 'Deleting Records from Global Temp Table :' );
          l_err_callpoint := '120 :  Deleting records from Global Temp Table:' ;
          Delete From XXWC_SALES_VELOCITY_GT
          Where Organization_ID = Rec_Warehouse.Organization_ID ;

          -- Populate Global Temp Table
          Fnd_File.Put_Line ( Fnd_File.LOG, 'Populate Records in Global Temp Table :' );
          l_err_callpoint := '130 :  Populte Global Temp Table:' ;
          Populate_GT(P_Organization_ID =>Rec_Warehouse.Organization_ID  ) ;
          Fnd_File.Put_Line ( Fnd_File.LOG, 'After Populating Records in Global Temp Table :' );
          
          -- Calculate Sales Velocity for N Weeks
          -- Procedure will calculate Sales Velocity Counts for N weeks for all the items and Update GT table
          l_err_callpoint := '140 :  Call API Sales_Velocity_Weeks:' ;
          Sales_Velocity_Weeks(p_ORganization_ID => Rec_Warehouse.Organization_ID );
          
          -- Calculate Sales Velocity for an year
          l_err_callpoint := '150 :  Call API Sales_Velocity_Annual:' ;          
          Sales_Velocity_Annual(p_Organization_ID => Rec_Warehouse.Organization_ID);
          
          IF nvl(p_print_detail, 'N') = 'Y' THEN
              l_err_callpoint := '160 :  Call API Sales_Velocity_Report:' ;
              Sales_Velocity_Report (P_Organization_ID => Rec_Warehouse.Organization_ID);
          END IF;
          
          -- Update Sales Velocity
          l_err_callpoint := '170 :  Call API Update_Sales_Velocity:' ;
          Update_Sales_Velocity(P_Organization_ID => Rec_Warehouse.Organization_ID) ;
               
          /*insert into XXWC.XXWC_SALES_VELOCITY_GT_TEMP
          select *
          from  XXWC_SALES_VELOCITY_GT;*/
          
          Fnd_File.Put_Line ( Fnd_File.output,'</G_WAREHOUSE>');    
      Exception
         When Others THen
            Fnd_File.Put_Line ( Fnd_File.LOG, 'When Others Exception :' || SQLERRM);
            Fnd_File.Put_Line ( Fnd_File.LOG, 'Warehoues :' || G_ORGANIZATION_CODE );
            G_Return_Status := 'E' ;
      End;
      End Loop;
      
      Fnd_File.Put_Line ( Fnd_File.output,'</LIST_G_WAREHOUSE>');
      Fnd_File.Put_Line ( Fnd_File.output,'</XXWC_INV_SALES_VELOCITY>');
      -- Complete Concurrent Program with Warning if Return_Status is Not S
      If G_RETURN_STATUS  <> 'S' THen
         RetCode := 1;
      ENd If;
   Exception
      When Others Then
          l_Return_Status := 'U';
          Fnd_File.Put_Line ( Fnd_File.LOG, 'WHen Others Exception :' || SQLERRM);
          -- PRogram Should end with Warning  status

          l_err_callpoint := '210 :  When Others Exception :' ;

          Fnd_File.Put_Line ( Fnd_File.LOG, 'WHen Others Exception :' || SQLERRM);

          errbuf := SQLERRM;
          retcode := 2;
          ROLLBACK;

          FND_FILE.Put_Line(FND_FILE.Log, SQLERRM);
          FND_FILE.Put_Line(FND_FILE.Output, SQLERRM);

          XXCUS_ERROR_PKG.XXCUS_ERROR_MAIN_API(p_called_from     => G_MODULE_NAME,
                                             p_calling           => l_err_callpoint,
                                             p_ora_error_msg     => SQLERRM,
                                             p_error_desc        => 'When Others Exception XXWC_INV_SALES_VELOCITY_PKG.MAIN',
                                             p_distribution_list => l_distro_list,
                                             p_module            => 'INV');

   End MAIN;


END XXWC_INV_SALES_VELOCITY_PKG;
/
