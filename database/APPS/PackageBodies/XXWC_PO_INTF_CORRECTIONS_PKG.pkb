CREATE OR REPLACE PACKAGE BODY APPS.XXWC_PO_INTF_CORRECTIONS_PKG
AS
   /*****************************************************************************************************************************************
   -- File Name: XXWC_PO_INTF_CORRECTIONS_PKG.pkb
   --
   -- PROGRAM TYPE: PL/SQL Script   <API>
   --
   -- PURPOSE:
   -- HISTORY
   -- ========================================================================================================================================
   -- ========================================================================================================================================
   -- VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- --------------------------------------------------------------------------------------------------
   -- 1.0     25-AUG-2015   P.vamshidhar    TMS#20140317-00206  - Purch -Requisition Import enhancement - set default buyer
   --                                       Initial Version.

   ******************************************************************************************************************************************/

   g_err_callfrom   VARCHAR2 (100) := 'XXWC_PO_INTF_CORRECTIONS_PKG';
   g_distro_list    VARCHAR2 (100) := 'HDSOracleDevelopers@hdsupply.com';

   PROCEDURE PO_SUGGESTED_BUYER_UPD_PRC (x_errbuf         OUT VARCHAR2,
                                         x_retcode        OUT VARCHAR2,
                                         p_from_date   IN     VARCHAR2)
   IS
      /******************************************************************************************************************************************
        PROCEDURE : PO_SUGGESTED_BUYER_UPD_PRC

        REVISIONS:
        Ver        Date         Author                     Description
        ---------  ----------   ---------------    --------------------------------------------------------------------------------------
        1.0        25-AUG-2015  P.Vamshidhar       TMS#20140317-00206  - Purch -Requisition Import enhancement - set default buyer
                                                   Initial Version

      ******************************************************************************************************************************************/

      CURSOR CUR_REQ_DATA (
         CP_FROM_DATE    DATE,
         CP_ORG_ID       NUMBER)
      IS
         SELECT pri.ROWID row_id,
                mp.organization_code,
                msib.segment1 item,
                NULL new_buyer_id,
                NULL new_buyer,
                pri.suggested_buyer_id old_buyer_id,
                pav2.agent_name old_buyer,
                pri.process_flag,
                pri.request_id,
                pri.transaction_id,
                pri.destination_organization_id,
                pri.requisition_type,
                pri.creation_date,
                msib.segment1,
                msib.inventory_item_id
           FROM po_requisitions_interface_all pri,
                po_interface_errors pie,
                apps.mtl_parameters mp,
                PO_AGENTS_V pav2,
                apps.mtl_system_items_b msib
          WHERE     pri.PROCESS_FLAG = 'ERROR'
                AND pie.COLUMN_NAME = 'SUGGESTED_BUYER_ID'
                AND pri.transaction_id = pie.interface_transaction_id(+)
                AND pri.creation_date >= CP_FROM_DATE
                AND pri.DESTINATION_ORGANIZATION_ID = mp.organization_id
                AND pri.SUGGESTED_BUYER_ID = pav2.agent_id
                AND pri.item_id = msib.inventory_item_id
                AND pri.DESTINATION_ORGANIZATION_ID = msib.organization_id
                AND pri.org_id = CP_ORG_ID;


      l_err_msg        CLOB;
      l_sec            VARCHAR2 (110) DEFAULT 'START';
      l_procedure      VARCHAR2 (50) := 'PO_SUGGESTED_BUYER_UPD_PRC';
      l_org_id         NUMBER := FND_PROFILE.VALUE ('ORG_ID');
      l_update_login   NUMBER := FND_GLOBAL.LOGIN_ID;
      ln_user_id       FND_USER.USER_ID%TYPE := FND_GLOBAL.USER_ID;
      l_request_id     FND_CONCURRENT_REQUESTS.REQUEST_ID%TYPE;
      PROGRAM_ERROR    EXCEPTION;
      ln_num_req       NUMBER;
      lp_from_date     DATE;

      x_item_update    VARCHAR2 (1);
      x_item_update1   VARCHAR2 (1);

      l_item_update    VARCHAR2 (1);
      l_rec_count      NUMBER;
   BEGIN
      l_rec_count := 0;
      FND_FILE.PUT_LINE (
         FND_FILE.OUTPUT,
         ' Following Requisitions buyer information updated to New Buyer');
      FND_FILE.PUT_LINE (FND_FILE.OUTPUT, ' ');
      FND_FILE.PUT_LINE (
         FND_FILE.OUTPUT,
         '------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------  ');
      FND_FILE.PUT_LINE (
         FND_FILE.OUTPUT,
         '    Transaction Id               Item Number           Old Buyer                              New Buyer                        Dist. Organization code                                        ');
      FND_FILE.PUT_LINE (
         FND_FILE.OUTPUT,
         '------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------  ');
      FND_FILE.PUT_LINE (FND_FILE.OUTPUT, ' ');

      lp_from_date :=
         TRUNC (
            NVL (TO_DATE (p_from_date, 'YYYY/MM/DD HH24:MI:SS'), SYSDATE));


      FOR REC_REQ_DATA IN CUR_REQ_DATA (lp_from_date, L_ORG_ID)
      LOOP
         l_sec := 'Updating PO_REQUISITIONS_INTERFACE_ALL';

         --- Updating Interface table.
         BEGIN
            UPDATE PO_REQUISITIONS_INTERFACE_ALL
               SET SUGGESTED_BUYER_ID = NULL,
                   process_flag = NULL,
                   request_id = NULL,
                   batch_id = REC_REQ_DATA.destination_organization_id,
                   last_updated_by = ln_user_id,
                   last_update_date = SYSDATE,
                   last_update_login = l_update_login
             WHERE ROWID = REC_REQ_DATA.ROW_ID;

            l_rec_count := l_rec_count + 1;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_err_msg := SUBSTR (SQLERRM, 1, 1000);
               RAISE PROGRAM_ERROR;
         END;

         IF SQL%ROWCOUNT >= 1
         THEN
            FND_FILE.PUT_LINE (
               FND_FILE.OUTPUT,
                  '     '
               || RPAD (REC_REQ_DATA.transaction_id, 26, ' ')
               || ' '
               || RPAD (REC_REQ_DATA.segment1, 20, ' ')
               || ' '
               || RPAD (REC_REQ_DATA.old_buyer, 80, ' ')
               || ' '
               || RPAD (REC_REQ_DATA.new_buyer, 40, ' ')
               || ' '
               || REC_REQ_DATA.organization_code);

            -- Updating Item buyer information.
            l_sec := 'Updating Item Buyer Information ';

            BEGIN
               SELECT 'Y'
                 INTO l_item_update
                 FROM APPS.MTL_SYSTEM_ITEMS_B
                WHERE     inventory_item_id = REC_REQ_DATA.inventory_item_id
                      AND organization_id =
                             REC_REQ_DATA.destination_organization_id
                      AND buyer_id IS NOT NULL;

               IF l_item_update = 'Y'
               THEN
                  ITEM_BUYER_UPDATE_PRC (
                     REC_REQ_DATA.inventory_item_id,
                     REC_REQ_DATA.destination_organization_id,
                     x_item_update);

                  IF x_item_update = 'E'
                  THEN
                     x_item_update1 := 'E';
                  END IF;
               END IF;
            EXCEPTION
               WHEN OTHERS
               THEN
                  NULL;
            END;
         END IF;
      END LOOP;

      COMMIT;
      FND_FILE.PUT_LINE (FND_FILE.OUTPUT, ' ');
      FND_FILE.PUT_LINE (
         FND_FILE.OUTPUT,
         '------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------  ');
      FND_FILE.PUT_LINE (FND_FILE.OUTPUT, ' ');

      -- Submiting Planning Report/requisition concurrent Program


      IF l_rec_count > 0
      THEN
         l_sec := 'Submiting Requisition Import Program';

         l_request_id :=
            fnd_request.submit_request (
               application   => 'XXWC',
               program       => 'XXWC_PLANNING_REQ_IMPORT',
               description   => 'Requisition Import for all orgs',
               start_time    => SYSDATE,
               sub_request   => FALSE,
               argument1     => 1);

         IF l_request_id > 0
         THEN
            COMMIT;
            FND_FILE.PUT_LINE (
               FND_FILE.LOG,
                  'Req import for inv source submitted - Request_id:'
               || l_request_id);
            x_retcode := 0;
            x_errbuf := NULL;
         ELSE
            FND_FILE.PUT_LINE (FND_FILE.LOG, 'Req import submission failed');
            x_retcode := 1;
         END IF;

         IF x_item_update1 = 'E'
         THEN
            FND_FILE.PUT_LINE (FND_FILE.LOG, 'Item Buyer Update Failed');
            RAISE PROGRAM_ERROR;
         END IF;
      END IF;
   EXCEPTION
      WHEN PROGRAM_ERROR
      THEN
         l_err_msg :=
               l_err_msg
            || l_sec
            || ' ...Error_Stack...'
            || DBMS_UTILITY.format_error_stack ()
            || ' Error_Backtrace...'
            || DBMS_UTILITY.format_error_backtrace ();
         fnd_file.put_line (fnd_file.LOG, l_err_msg);
         -- Calling ERROR API
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || l_procedure,
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (l_err_msg, 1, 2000),
            p_error_desc          => SUBSTR (l_err_msg, 1, 240),
            p_distribution_list   => g_distro_list,
            p_module              => 'INV');

         x_retcode := 2;
         x_errbuf := l_err_msg;
      WHEN OTHERS
      THEN
         l_err_msg :=
               l_err_msg
            || l_sec
            || ' ...Error_Stack...'
            || DBMS_UTILITY.format_error_stack ()
            || ' Error_Backtrace...'
            || DBMS_UTILITY.format_error_backtrace ();
         fnd_file.put_line (fnd_file.LOG, l_err_msg);
         -- Calling ERROR API
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || l_procedure,
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (l_err_msg, 1, 2000),
            p_error_desc          => SUBSTR (l_err_msg, 1, 240),
            p_distribution_list   => g_distro_list,
            p_module              => 'INV');

         x_retcode := 2;
         x_errbuf := l_err_msg;
   END;


   PROCEDURE ITEM_BUYER_UPDATE_PRC (p_item_id          IN     NUMBER,
                                    p_orgaization_id   IN     NUMBER,
                                    x_process_status      OUT VARCHAR2)
   /******************************************************************************************************************************************
     PROCEDURE : ITEM_BUYER_UPDATE_PRC

     REVISIONS:
     Ver        Date         Author                     Description
     ---------  ----------   ---------------    --------------------------------------------------------------------------------------
     1.0        25-AUG-2015  P.Vamshidhar       TMS#20140317-00206  - Purch -Requisition Import enhancement - set default buyer
                                                Initial Version

   ******************************************************************************************************************************************/

   IS
      PRAGMA AUTONOMOUS_TRANSACTION;

      l_item_tbl_typ    ego_item_pub.item_tbl_type;
      x_item_table      ego_item_pub.item_tbl_type;
      x_return_status   VARCHAR2 (1);
      x_msg_count       NUMBER (10);
      x_msg_data        VARCHAR2 (1000);
      x_message_list    error_handler.error_tbl_type;
   BEGIN
      x_process_status := NULL;

      l_item_tbl_typ (1).transaction_type := 'UPDATE';
      l_item_tbl_typ (1).inventory_item_id := p_item_id;
      l_item_tbl_typ (1).organization_id := p_orgaization_id;
      l_item_tbl_typ (1).buyer_id := NULL;

      ego_item_pub.process_items (p_api_version     => 1.0,
                                  p_init_msg_list   => fnd_api.g_true,
                                  p_commit          => fnd_api.g_true,
                                  p_item_tbl        => l_item_tbl_typ,
                                  x_item_tbl        => x_item_table,
                                  x_return_status   => x_return_status,
                                  x_msg_count       => x_msg_count);

      IF (x_return_status = fnd_api.g_ret_sts_success)
      THEN
         x_msg_data := 'Item Updated Successfully';
         FND_FILE.PUT_LINE (
            FND_FILE.LOG,
               p_item_id
            || ' Item Buyer information updated successfully for Org '
            || p_orgaization_id);
         COMMIT;
         x_process_status := 'S';
      ELSE
         x_process_status := 'E';
         FND_FILE.PUT_LINE (
            FND_FILE.LOG,
               p_item_id
            || ' Item Buyer information not updated for Org '
            || p_orgaization_id);

         error_handler.get_message_list (x_message_list => x_message_list);

         FOR i IN 1 .. x_message_list.COUNT
         LOOP
            x_msg_data :=
               x_msg_data || ' - ' || x_message_list (i).MESSAGE_TEXT;
         END LOOP;

         FND_FILE.PUT_LINE (FND_FILE.LOG,
                            p_item_id || ' Error information ' || x_msg_data);

         ROLLBACK;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         x_msg_data := SUBSTR ('Error Occurted: ' || SQLERRM, 1, 999);
         x_process_status := 'E';
         FND_FILE.PUT_LINE (FND_FILE.LOG, ' ' || x_msg_data);
   END;
END XXWC_PO_INTF_CORRECTIONS_PKG;
/