create or replace 
PACKAGE BODY      xxwc_lead_time_pkg
IS
   /*************************************************************************
   *  Copyright (c) 2011 Lucidity Consulting Group
   *  All rights reserved.
   **************************************************************************
   *   $Header xxwc_lead_time_pkg.pks $
   *   Module Name: xxwc_lead_time_pkg.pks
   *
   *   PURPOSE:   This package is used by the conc program WC Calculate and Update Lead time
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        09/01/2011  Vivek Lakaman             Initial Version
   *  2.0        11/03/2014  Vijaysrinivasan        TMS#20141002-00048  Multi org changes 
   * ************************************************************************/

   /*************************************************************************
    *   Procedure Name: Write_log
    *
    *************************************************************************
    *   Purpose       : To write the debug message to conc log file
    *   Parameter:
    *       IN
    *            p_msg    -Debug Message
    * ***********************************************************************/

   PROCEDURE Write_log (p_msg IN VARCHAR2)
   IS
   BEGIN
      FND_FILE.PUT_LINE (FND_FILE.LOG, p_msg);
   END Write_log;

   /*************************************************************************
    *   Procedure Name: Write_output
    *
    *************************************************************************
    *   Purpose       : To write the message to conc output file
    *   Parameter:
    *       IN
    *            p_msg    -Debug Message
    * ***********************************************************************/

   PROCEDURE Write_output (p_msg IN VARCHAR2)
   IS
   BEGIN
      FND_FILE.PUT_LINE (FND_FILE.output, p_msg);
   END Write_output;

   /*************************************************************************
    *   Procedure Name: calculate_update
    *
    *************************************************************************
    *   Purpose       : Main procedure to calculate and update the lead time for the items
    *   Parameter:
    *       OUT
    *            errbuff    --Error Message
    *            retcode    --Return Code
    * ***********************************************************************/

   PROCEDURE calculate_update (errbuff OUT VARCHAR2, retcode OUT VARCHAR2)
   IS
      l_conc_prog_last_run_date   DATE;
      l_org_id                    NUMBER := FND_PROFILE.VALUE ('ORG_ID');
      l_quantity_received         NUMBER;
      l_first_shipment_line_id    NUMBER;
      l_revision_num              NUMBER;
      l_beginning_date            DATE;
      l_last_lead_time            NUMBER;
      l_last_lead_time_dff        VARCHAR2 (100);
      l_calc_lead_time_dff        VARCHAR2 (100);
      l_static_lead_time          NUMBER; --added 1/2/2013 by Lee Spitzer

      --Define user defined exception
      SKIP_RECORD EXCEPTION;

      CURSOR cur_po_std (
         p_run_date   IN            DATE
      )
      IS
           SELECT   *
             FROM   (SELECT   rt.transaction_date,
                              rt.po_header_id,
                              rt.po_line_id,
                              NULL po_release_id,
                              NULL release_num,
                              rt.po_line_location_id,
                              rt.shipment_header_id,
                              rt.shipment_line_id,
                              rt.quantity transaction_quantity,
                              pl.item_id,
                              rt.organization_id,
                              rt.transaction_id,
                              ph.print_count,
                              ph.segment1 po_number,
                              ph.printed_date
                       FROM   rcv_transactions rt,
                              po_headers ph,
                              po_lines pl,
                              po_document_types pdt
                      WHERE       rt.transaction_type = 'RECEIVE'
                              AND rt.source_document_code = 'PO'
                              AND rt.po_header_id = ph.po_header_id
                              AND rt.po_line_id = pl.po_line_id
                              AND rt.source_document_code =
                                    pdt.document_type_code
                              AND ph.type_lookup_code = pdt.document_subtype
                              AND ph.type_lookup_code = 'STANDARD'
                              AND rt.transaction_date > p_run_date
                     UNION
                     SELECT   rt.transaction_date,
                              rt.po_header_id,
                              rt.po_line_id,
                              pr.po_release_id,
                              pr.release_num,
                              rt.po_line_location_id,
                              rt.shipment_header_id,
                              rt.shipment_line_id,
                              rt.quantity transaction_quantity,
                              pl.item_id,
                              rt.organization_id,
                              rt.transaction_id,
                              pr.print_count,
                              ph.segment1 po_number,
                              pr.printed_date
                       FROM   rcv_transactions rt,
                              po_headers ph,
                              po_releases pr,
                              po_lines pl
                      WHERE       rt.transaction_type = 'RECEIVE'
                              AND rt.source_document_code = 'PO'
                              AND rt.po_header_id = ph.po_header_id
                              AND rt.po_release_id = pr.po_release_id
                              AND rt.po_line_id = pl.po_line_id
                              AND rt.transaction_date > p_run_date)
         ORDER BY   1;
   BEGIN
      errbuff := NULL;
      retcode := '0';

      Write_log ('At Begin');
      Write_log ('l_org_id=' || l_org_id);

      --Initialize the apps
      Write_log ('Initializing the apps');
      mo_global.set_policy_context ('S', l_org_id);
      fnd_global.apps_initialize (fnd_global.user_id,
                                  fnd_global.resp_id,
                                  fnd_global.resp_appl_id);

      SELECT   MAX (cp_last_run_date)
        INTO   l_conc_prog_last_run_date
        --enabled the below synonym with rls policy for TMS ## 20141002-00048  by VijaySrinivasan  on 11/03/2014 
        FROM   xxwc_calc_update_lead_time;

      Write_log('l_conc_prog_last_run_date='
                || TO_CHAR (l_conc_prog_last_run_date,
                            'DD-MON-YYYY HH24:MI:SS'));

      IF l_conc_prog_last_run_date IS NULL
      THEN
         SELECT   MIN (transaction_date)
           INTO   l_conc_prog_last_run_date
           FROM   rcv_transactions rt
          WHERE   rt.transaction_type = 'RECEIVE'
                  AND rt.source_document_code = 'PO';

         Write_log('Minimum -l_conc_prog_last_run_date='
                   || TO_CHAR (l_conc_prog_last_run_date,
                               'DD-MON-YYYY HH24:MI:SS'));
      END IF;

      FOR i IN cur_po_std (l_conc_prog_last_run_date)
      LOOP
         BEGIN
            Write_log (
               '+--------------------------------------------------------+'
            );
            Write_log ('i.po_number        =' || i.po_number);
            Write_log ('i.po_header_id     =' || i.po_header_id);
            Write_log ('i.po_release_id    =' || i.po_release_id);
            Write_log ('i.transaction_id   =' || i.transaction_id);
            Write_log ('i.transaction_date =' || i.transaction_date);
            Write_log ('i.po_line_id       =' || i.po_line_id);
            Write_log ('i.shipment_line_id =' || i.shipment_line_id);

            IF i.po_release_id IS NULL
            THEN
               BEGIN
                  SELECT   MIN (shipment_line_id)
                    INTO   l_first_shipment_line_id
                    FROM   rcv_shipment_lines
                   WHERE   po_line_id = i.po_line_id
                           AND po_header_id = i.po_header_id;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     l_first_shipment_line_id := NULL;
               END;
            ELSE
               BEGIN
                  SELECT   MIN (shipment_line_id)
                    INTO   l_first_shipment_line_id
                    FROM   rcv_shipment_lines
                   WHERE   po_release_id = i.po_release_id;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     l_first_shipment_line_id := NULL;
               END;
            END IF;

            Write_log (
               'l_first_shipment_line_id   =' || l_first_shipment_line_id
            );
            Write_log ('i.shipment_line_id =' || i.shipment_line_id);


            IF i.shipment_line_id = l_first_shipment_line_id
            THEN
               Write_log ('i.print_count =' || i.print_count);

               -- if the print count =0
               IF NVL (i.print_count, 0) = 1
               THEN
                  l_beginning_date := i.printed_date;
               ELSIF NVL (i.print_count, 0) > 1
               THEN
                  IF i.po_release_id IS NULL
                  THEN
                     BEGIN
                        SELECT   MAX (revision_num)
                          INTO   l_revision_num
        --Modified the below line for TMS ## 20141002-00048  by VijaySrinivasan  on 11/03/2014 
                          FROM   po_headers_archive
                         WHERE   po_header_id = i.po_header_id;
                     EXCEPTION
                        WHEN OTHERS
                        THEN
                           l_revision_num := 0;
                     END;
                  ELSE
                     BEGIN
                        SELECT   MAX (revision_num)
                          INTO   l_revision_num
        --Modified the below line for TMS ## 20141002-00048  by VijaySrinivasan  on 11/03/2014 
                          FROM   po_releases_archive
                         WHERE   po_release_id = i.po_release_id;
                     EXCEPTION
                        WHEN OTHERS
                        THEN
                           l_revision_num := 0;
                     END;
                  END IF;

                  Write_log ('l_revision_num=' || l_revision_num);

                  IF i.po_release_id IS NULL
                  THEN
                     BEGIN
                        SELECT   NVL (printed_date, approved_date)
                          INTO   l_beginning_date
        --Modified the below line for TMS ## 20141002-00048  by VijaySrinivasan  on 11/03/2014 
                          FROM   po_headers_archive
                         WHERE   po_header_id = i.po_header_id
                                 AND revision_num = l_revision_num;
                     EXCEPTION
                        WHEN OTHERS
                        THEN
                           l_beginning_date := NULL;
                     END;
                  ELSE
                     BEGIN
                        SELECT   printed_date
                          INTO   l_beginning_date
        --Modified the below line for TMS ## 20141002-00048  by VijaySrinivasan  on 11/03/2014 
                          FROM   po_releases_archive
                         WHERE   po_release_id = i.po_release_id
                                 AND revision_num = l_revision_num;
                     EXCEPTION
                        WHEN OTHERS
                        THEN
                           l_beginning_date := NULL;
                     END;
                  END IF;
                  Write_log ('l_beginning_date =' || l_beginning_date);
               ELSE
                  IF i.po_release_id IS NULL
                  THEN
                     BEGIN
                        SELECT   MIN (revision_num)
                          INTO   l_revision_num
        --Modified the below line for TMS ## 20141002-00048  by VijaySrinivasan  on 11/03/2014 
                          FROM   po_headers_archive
                         WHERE   po_header_id = i.po_header_id;
                     EXCEPTION
                        WHEN OTHERS
                        THEN
                           l_revision_num := 0;
                     END;
                  ELSE
                     BEGIN
                        SELECT   MIN (revision_num)
                          INTO   l_revision_num
        --Modified the below line for TMS ## 20141002-00048  by VijaySrinivasan  on 11/03/2014 
                          FROM   po_releases_archive
                         WHERE   po_release_id = i.po_release_id;
                     EXCEPTION
                        WHEN OTHERS
                        THEN
                           l_revision_num := 0;
                     END;
                  END IF;

                  Write_log ('l_revision_num=' || l_revision_num);

                  IF i.po_release_id IS NULL
                  THEN
                     BEGIN
                        SELECT   approved_date
                          INTO   l_beginning_date
        --Modified the below line for TMS ## 20141002-00048  by VijaySrinivasan  on 11/03/2014 
                          FROM   po_headers_archive
                         WHERE   po_header_id = i.po_header_id
                                 AND revision_num = l_revision_num;
                     EXCEPTION
                        WHEN OTHERS
                        THEN
                           l_beginning_date := NULL;
                     END;
                  ELSE
                     BEGIN
                        SELECT   approved_date
                          INTO   l_beginning_date
        --Modified the below line for TMS ## 20141002-00048  by VijaySrinivasan  on 11/03/2014 
                          FROM   po_releases_archive
                         WHERE   po_release_id = i.po_release_id
                                 AND revision_num = l_revision_num;
                     EXCEPTION
                        WHEN OTHERS
                        THEN
                           l_beginning_date := NULL;
                     END;
                  END IF;
               END IF;

               Write_log('l_beginning_date  ='
                         || TO_CHAR (l_beginning_date,
                                     'DD-MON-YYYY HH24:MI:SS'));
               Write_log('i.transaction_date='
                         || TO_CHAR (i.transaction_date,
                                     'DD-MON-YYYY HH24:MI:SS'));

               IF l_beginning_date IS NOT NULL
               THEN
                  --calculate the last lead time
                  l_last_lead_time :=
                     ROUND ( (i.transaction_date - l_beginning_date), 0);
                  Write_log ('l_last_lead_time=' || l_last_lead_time);

                  BEGIN
                     SELECT   NVL (attribute30, '0') calc_lead_time,
                              NVL (attribute2, '0') last_lead_time,
                              NVL (full_lead_time, '0') full_lead_time --added 1/2/2013 by Lee Spitzer
                       INTO   l_calc_lead_time_dff, l_last_lead_time_dff, 
                              l_static_lead_time --added 1/2/2013 by Lee Spitzer
                       FROM   mtl_system_items
                      WHERE   inventory_item_id = i.item_id
                              AND organization_id = i.organization_id;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        l_calc_lead_time_dff := NULL;
                        l_last_lead_time_dff := NULL;
                        l_static_lead_time := NULL; --Added 1/2/2013 by Lee Spitzer
                  END;

                  Write_log ('******Before Calculation****************');
                  Write_log ('l_calc_lead_time_dff=' || l_calc_lead_time_dff);
                  Write_log ('l_last_lead_time_dff=' || l_last_lead_time_dff);
                  Write_log ('l_static_lead_time=' || l_static_lead_time); --added 1/2/2013 by Lee Spitzer
                  
                  --New Logic added 1/2/2013 by Lee Spitzer - TMS Ticket 20121217-00757
                  /*IF "0" or null value is found in the DFF Calc Lead time field, then look at the static lead time field in org items. 
                    IF static lead time = "0" of null, use the actual lead time from the PO approval date to received date.*/

                  IF l_calc_lead_time_dff = 0 OR l_calc_lead_time_dff IS NULL THEN
                  
                      IF l_static_lead_time = 0 OR l_static_lead_time IS NULL THEN
                      
                         l_calc_lead_time_dff := l_last_lead_time;
                            
                      ELSE
                      
                          l_calc_lead_time_dff := l_static_lead_time;
                    
                      
                      END IF;
                      
                      
                  END IF;
                  
                  --End add new logic 1/2/2013 by Lee Spitzer - TMS Ticket 20121217-00757
            
                  l_calc_lead_time_dff :=
                     TO_CHAR(ROUND (
                                ( ( (TO_NUMBER (l_calc_lead_time_dff) * 2)
                                   + l_last_lead_time)
                                 / 3),
                                0
                             ));
                  l_last_lead_time_dff := TO_CHAR (l_last_lead_time);

                  Write_log ('******After Calculation****************');
                  Write_log('l_calc_lead_time_dff(After Recalc)='
                            || l_calc_lead_time_dff);
                  Write_log('l_last_lead_time_dff(After Recalc)='
                            || l_last_lead_time_dff);

                  Write_log('i.item_id=' || i.item_id);
                  Write_log('i.organization_id=' || i.organization_id);

                  UPDATE   mtl_system_items_b
                     SET   attribute2 = l_last_lead_time_dff,
                           attribute30 = l_calc_lead_time_dff,
                           attribute_category = 'WC'
                   WHERE   inventory_item_id = i.item_id
                           AND organization_id = i.organization_id;
                  Write_log('No of Rows Updated =' || sql%rowcount);
               END IF;
            ELSE
               --Skip the receiving txn if the shipment is not the first line
               RAISE SKIP_RECORD;
            END IF;
         EXCEPTION
            WHEN SKIP_RECORD
            THEN
               Write_log('Skipping the transaction as it is not the first one for the shipment');
         END;
      END LOOP;

      Write_log ('Inserting into xxwc_calc_update_lead_time');

      INSERT INTO xxwc_calc_update_lead_time (cp_last_run_date,
                                              last_update_date,
                                              last_updated_by,
                                              creation_date,
                                              created_by,
                                              last_update_login,
                                              request_id)
        VALUES   (SYSDATE,
                  SYSDATE,
                  fnd_global.user_id,
                  SYSDATE,
                  fnd_global.user_id,
                  fnd_global.login_id,
                  fnd_global.conc_request_id);

      Write_log ('No of Rows Inserted(xxwc_calc_update_lead_time) =>'||sql%rowcount);
      Write_log ('End of the Program');
      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         errbuff := SQLERRM;
         retcode := '2';
   END calculate_update;
END xxwc_lead_time_pkg;
/
