CREATE OR REPLACE PACKAGE BODY APPS.XXCUS_REBATES_ARCHIVE
-- ESMS TICKET HISTORY
-- Scope: Oracle Trade Management / Rebates accruals and gl journals extract
-- Used by concurrent jobs HDS Rebates: Archive Partitioned Reporting Tables
-- Tables to be archived: XXCUS.XXCUS_OZF_XLA_ACCRUALS_B and XXCUS.XXCUS_OZF_GL_JOURNALS_B
-- Change History:
-- Ticket#               Version  Date          Comment
-- ====================  =======  ============  ============================================
-- TMS 20160316-00196    1.0      04/15/2016    Created.
-- TMS 20160519-00078    1.1      Parameter name change and other fixes [ ESMS 327004 ]
as
  --
  ln_request_id NUMBER :=0;
  --
  procedure print_log(p_message in varchar2) is
  begin
   if fnd_global.conc_request_id >0 then
    fnd_file.put_line(fnd_file.log, p_message);
   else
    dbms_output.put_line(p_message);
   end if;
  exception
   when others then
    fnd_file.put_line(fnd_file.log, 'Issue in print_log routine ='||sqlerrm);
  end print_log;
  --
  procedure tm_accruals
  (
    retcode                 out number
   ,errbuf                   out varchar2
   ,p_cal_year           in    number -- Ver 1.1
  )  
  is
   --
   l_delete_sql  varchar2(4000) :=Null;
   l_insert_sql  varchar2(10000) :=Null;
   l_proceed boolean :=FALSE;
   --
   l_deleted_count number :=0;
   l_archived_count number :=0;
   --
   l_record_type varchar2(30) :='REBATE_ACCRUAL';
   --
   l_request_id number :=fnd_global.conc_request_id;
   l_user_id number :=fnd_global.user_id;
   --
    v_sql VARCHAR2(240) :=Null; --Ver 1.1
   --
    cursor fiscal_periods is
    select period_name                                         fiscal_period
               ,substr(upper(period_name), 1, 3) fiscal_month
    from gl_periods
    where 1 =1
    and period_set_name ='4-4-QTR'
    --and period_year =p_cal_year --End Ver 1.1
    and end_date between to_date('01-JAN-'||p_cal_year) and to_date('31-DEC-'||p_cal_year)  --Ver 1.1
    and adjustment_period_flag ='N'
    order by period_year asc, period_num asc; 
   --
   -- Begin Ver 1.1
   cursor rebuild_partitioned_indexes1 (p_table in varchar2) is
    select 'ALTER INDEX '||owner||'.'||uip.index_name||' REBUILD PARTITION '||uip.partition_name||' PARALLEL 4' idx_sql
    from dba_ind_partitions  uip
        ,dba_tab_partitions  utp
        ,dba_indexes         ui
    where 1 =1
      and uip.partition_name  =utp.partition_name
      and ui.index_name       =uip.index_name
      and ui.table_name       =utp.table_name
      and ui.owner            ='XXCUS'
      and utp.table_name      =p_table
    order by uip.index_name;
   --  
   -- End Ver 1.1    
   --   
  begin
     --
     execute immediate 'ALTER SESSION FORCE PARALLEL DML';
     execute immediate 'ALTER TABLE xxcus.xxcus_ozf_xla_accruals_archive PARALLEL 8';
     execute immediate 'ALTER TABLE xxcus.xxcus_ozf_xla_accruals_b PARALLEL 8';
     --
     -- First insert partition...If everything is successful then delete the same set of data
     --
     for rec in fiscal_periods loop
     --
             begin 
              -- Insert sub set of data based on partition of the year to be archived
              l_insert_sql :='insert /*+ append parallel (INS 8) */ into xxcus.xxcus_ozf_xla_accruals_archive INS'||'
                (
                select /*+ parallel (tmsla 8) */ *
                from xxcus.xxcus_ozf_xla_accruals_b partition ('||rec.fiscal_month||') tmsla '||'
                '||'where 1 =1'||'
                '||'     and tmsla.xaeh_period_name ='||''''||rec.fiscal_period||''''||')';
              --
                   print_log('Insert SQL: ');
                   print_log('==========');   
                   print_log(l_insert_sql); 
                   print_log(' ');  
                  --
                   --
                   print_log ('Begin Delete 2: Time: '||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
                   execute immediate 'begin ' || l_insert_sql || '; :x := sql%rowcount; end;' using OUT l_archived_count;
                   print_log ('End Delete 2: Time: '||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));     
                  --                  
                 l_proceed :=TRUE;
                 --
                 commit;
                 --
             exception
              when others then
               print_log('Error during insert for fiscal period '||rec.fiscal_period||', message ='||sqlerrm);
               l_proceed :=FALSE;
             end;
             -- Delete sub set of data based on partition of the year to be archived
             if (l_proceed) then 
                --
                 begin 
                    l_delete_sql := 'DELETE /*+ PARALLEL (D 2) */ XXCUS.XXCUS_OZF_XLA_ACCRUALS_B partition ('  --TMS 20160316-00196
                         ||rec.fiscal_month||') D '
                         ||'
                         '||' WHERE 1 =1 '
                           ||'
                           '||' AND D.XAEH_PERIOD_NAME ='||''''||rec.fiscal_period||'''';
                    --
                    --
                       print_log('Delete SQL: ');
                       print_log('==========');   
                       print_log(l_delete_sql); 
                       print_log(' ');  
                       --
                       print_log ('Begin Delete 2: Time: '||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
                       execute immediate 'begin ' || l_delete_sql || '; :x := sql%rowcount; end;' using OUT l_deleted_count;
                       print_log ('End Delete 2: Time: '||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));     
                      --
                      commit;
                      --
                      begin
                            --
                           savepoint square2; 
                            --
                            insert into xxcus.xxcus_rebates_archive_log 
                            (
                              fiscal_period,
                              delete_sql,
                              insert_sql,
                              request_id,
                              created_by,
                              creation_date,
                              records_archived,                          
                              records_deleted,
                              record_type
                            )    
                           values
                           (
                              rec.fiscal_period,
                              l_delete_sql,
                              l_insert_sql,
                              l_request_id,
                              l_user_id,
                              sysdate,
                              l_archived_count,                          
                              l_deleted_count,
                              l_record_type
                           );           
                      exception
                       when others then
                        rollback to square2;
                        print_log('Error during insert into archive log table  xxcus.xxcus_rebates_archive_log  for fiscal period '||rec.fiscal_period||', message ='||sqlerrm);
                      end;                  
                      --        
                 exception
                   when others then
                      print_log('Error during delete for fiscal period '||rec.fiscal_period||', message ='||sqlerrm);
                  end;
                  --             
             else
              print_log('Error during insert for fiscal period '||rec.fiscal_period||', message ='||sqlerrm);
             end if;
              --         
     end loop;  -- end of cursor fiscal_periods
    --
   -- Begin Ver 1.1
    begin
     --
     print_log ('Begin rebuild of custom accrual detail table partitioned indexes...');
     --
     for rec in rebuild_partitioned_indexes1 (p_table =>'XXCUS_OZF_XLA_ACCRUALS_B') loop
      --
       v_sql :=rec.idx_sql;
      --
        execute immediate rec.idx_sql;
      --
     end loop;
     --
     print_log ('End rebuild of custom accrual detail table partitioned indexes...');
     --
    exception
     when others then
      print_log ('Issue in rebuild of custom accrual detail table partitioned indexes, current sql =>'||v_sql);
    end;
    --
    -- End Ver 1.1    
    --       
  exception
   when others then
    print_log( 'Issue in tm_accruals routine ='||sqlerrm);
  end tm_accruals;  
  --
  procedure tm_journals
  (
    retcode                 out number
   ,errbuf                   out varchar2
   ,p_cal_year           in    number --Ver 1.1
  )  
  is
   --
   l_delete_sql  varchar2(4000) :=Null;
   l_insert_sql  varchar2(10000) :=Null;
   l_proceed boolean :=FALSE;
   --
   l_deleted_count number :=0;
   l_archived_count number :=0;
   --
   l_record_type varchar2(30) :='REBATE_JOURNAL';   
   --
   l_request_id number :=fnd_global.conc_request_id;
   l_user_id number :=fnd_global.user_id;
   --
   v_sql varchar2(240) :=Null; --Ver 1.1
   --
    cursor fiscal_periods  is
    select period_name                                         fiscal_period
               ,substr(upper(period_name), 1, 3) fiscal_month          
    from gl_periods
    where 1 =1
    and period_set_name ='4-4-QTR'
    --and period_year =p_cal_year --End Ver 1.1
    and end_date between to_date('01-JAN-'||p_cal_year) and to_date('31-DEC-'||p_cal_year)  --Ver 1.1
    and adjustment_period_flag ='N'
    order by period_year asc, period_num asc; 
   --
   -- Begin Ver 1.1
   cursor rebuild_partitioned_indexes1 (p_table in varchar2) is
    select 'ALTER INDEX '||owner||'.'||uip.index_name||' REBUILD PARTITION '||uip.partition_name||' PARALLEL 4'  idx_sql
    from dba_ind_partitions  uip
        ,dba_tab_partitions  utp
        ,dba_indexes         ui
    where 1 =1
      and uip.partition_name  =utp.partition_name
      and ui.index_name       =uip.index_name
      and ui.table_name       =utp.table_name
      and ui.owner            ='XXCUS'
      and utp.table_name      =p_table 
      order by uip.index_name;
  --    
  -- End Ver 1.1
  --
  begin
     --
     execute immediate 'ALTER SESSION FORCE PARALLEL DML';
     execute immediate 'ALTER TABLE xxcus.xxcus_ozf_gl_journals_archive PARALLEL 8';
     execute immediate 'ALTER TABLE xxcus.xxcus_ozf_gl_journals_b PARALLEL 8';
     --
     -- First insert partition...If everything is successful then delete the same set of data
     --
     for rec in fiscal_periods loop
     --
             begin 
              -- Insert sub set of data based on partition of the year to be archived
              l_insert_sql :='insert /*+ append parallel (INS 8) */ into xxcus.xxcus_ozf_gl_journals_archive INS'||'
                (
                select /*+ parallel (tmgl 8) */ *
                from xxcus.xxcus_ozf_gl_journals_b partition ('||rec.fiscal_month||') tmgl '||'
                '||'where 1 =1'||'
                '||'     and tmgl.gl_period ='||''''||rec.fiscal_period||''''||')';
              --
                   print_log('Insert SQL: ');
                   print_log('==========');   
                   print_log(l_insert_sql); 
                   print_log(' ');  
                  --
                   --
                   print_log ('Begin Insert : Time: '||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
                   execute immediate 'begin ' || l_insert_sql || '; :x := sql%rowcount; end;' using OUT l_archived_count;
                   print_log ('End Insert : Time: '||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));     
                  --                  
                 l_proceed :=TRUE;
                 --
                 commit;
                 --
             exception
              when others then
               print_log('Error during insert for fiscal period '||rec.fiscal_period||', message ='||sqlerrm);
               l_proceed :=FALSE;
             end;
             -- Delete sub set of data based on partition of the year to be archived
             if (l_proceed) then 
                --
                 begin 
                    l_delete_sql := 'DELETE /*+ PARALLEL (D 2) */ xxcus.xxcus_ozf_gl_journals_b partition ('  --TMS 20160316-00196
                         ||rec.fiscal_month||') D '
                         ||'
                         '||' WHERE 1 =1 '
                           ||'
                           '||' AND D.GL_PERIOD ='||''''||rec.fiscal_period||'''';
                    --
                    --
                       print_log('Delete SQL: ');
                       print_log('==========');   
                       print_log(l_delete_sql); 
                       print_log(' ');  
                       --
                       print_log ('Begin Delete : Time: '||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
                       execute immediate 'begin ' || l_delete_sql || '; :x := sql%rowcount; end;' using OUT l_deleted_count;
                       print_log ('End Delete : Time: '||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));     
                      --
                      commit;
                      --
                      begin
                            --
                           savepoint square2; 
                            --
                            insert into xxcus.xxcus_rebates_archive_log 
                            (
                              fiscal_period,
                              delete_sql,
                              insert_sql,
                              request_id,
                              created_by,
                              creation_date,
                              records_archived,                          
                              records_deleted,
                              record_type
                            )    
                           values
                           (
                              rec.fiscal_period,
                              l_delete_sql,
                              l_insert_sql,
                              l_request_id,
                              l_user_id,
                              sysdate,
                              l_archived_count,                          
                              l_deleted_count,
                              l_record_type                                             
                           );           
                      exception
                       when others then
                        rollback to square2;
                        print_log('Error during insert into archive log table  xxcus.xxcus_rebates_archive_log  for fiscal period '||rec.fiscal_period||', message ='||sqlerrm);
                      end;                  
                      --        
                 exception
                   when others then
                      print_log('Error during delete for fiscal period '||rec.fiscal_period||', message ='||sqlerrm);
                  end;
                  --             
             else
              print_log('Error during insert for fiscal period '||rec.fiscal_period||', message ='||sqlerrm);
             end if;
              --         
     end loop;  -- end of cursor fiscal_periods
     --
	 -- Begin Ver 1.1
    begin
     --
     print_log ('Begin rebuild of TM GL journals custom table partitioned indexes...');
     --
     for rec in rebuild_partitioned_indexes1  (p_table =>'XXCUS_OZF_GL_JOURNALS_B') loop
      --
       v_sql :=rec.idx_sql;
      --
        execute immediate rec.idx_sql;
      --
      print_log ('...Rebuild Partition SQL: '||rec.idx_sql||' was executed successfully');
      --
     end loop;
     --
     print_log ('End rebuild of TM GL journals custom table partitioned indexes...');
     print_log ('');
     --
    exception
     when others then
      print_log ('Issue in rebuild of TM GL journals custom table partitioned index, current sql =>'||v_sql);
      print_log ('');
    end;
	-- End Ver 1.1
    --         
  exception
   when others then
    print_log( 'Issue in tm_journals routine ='||sqlerrm);
  end tm_journals;  
  --
  -- Begin Ver 1.1
  procedure setup_gl_archive_partitions
  (
    retcode                 out number
   ,errbuf                   out varchar2
   ,p_cal_year           in    number
  )  
  is
   --
   l_split_partition_sql  varchar2(4000) :=Null;
   l_proceed boolean;
   --
   l_request_id number :=fnd_global.conc_request_id;
   l_user_id number :=fnd_global.user_id;
   --
    cursor fiscal_periods  is
    select period_name                                         fiscal_period
               ,substr(upper(period_name), 1, 3) fiscal_month
               ,upper(replace(period_name, '-', '')) fiscal_partition
               ,(
                    select count(utp.partition_name)
                    from dba_tab_partitions  utp
                    where 1 =1
                      and utp.table_owner ='XXCUS'
                      and utp.table_name  ='XXCUS_OZF_GL_JOURNALS_ARCHIVE'
                      and utp.partition_name =upper(replace(period_name, '-', ''))
                ) partition_count      
    from gl_periods
    where 1 =1
    and period_set_name ='4-4-QTR'
    and end_date between to_date('01-JAN-'||p_cal_year) and to_date('31-DEC-'||p_cal_year)
    and adjustment_period_flag ='N'
    order by period_year asc, period_num asc; 
   --
  begin
     --
     for rec in fiscal_periods loop
      --
      if (rec.partition_count =0) then
       --
       l_split_partition_sql :=Null;
       --
            select 'ALTER TABLE XXCUS.XXCUS_OZF_GL_JOURNALS_ARCHIVE '||'
                   '||'SPLIT PARTITION NA VALUES ('||''''||rec.fiscal_period||''''||') '||'
                   '||' INTO '||'
                   '||' ( '||'
                   '||' PARTITION '||rec.fiscal_partition||','||'
                   '||' PARTITION NA '||'
                   '||')' 
            into l_split_partition_sql
            from dual;
       --
           print_log(' ');
           print_log(l_split_partition_sql);
           print_log(' ');
           print_log('...Before creating partition '||rec.fiscal_partition);
           execute immediate  l_split_partition_sql;
           print_log('......After creating partition '||rec.fiscal_partition);           
       --
      else
           --Partition exists...no need to create...move on      
           print_log(' ');
           print_log('TM GL archive table partition '||rec.fiscal_partition||' already exists.');
           print_log(' ');
       --
      end if;
      --
     end loop;     
     --
  exception
   when others then
    print_log( 'Issue in setup_gl_archive_partitions routine ='||sqlerrm);
  end setup_gl_archive_partitions;
  --
  procedure setup_accr_archive_partitions
  (
    retcode                 out number
   ,errbuf                   out varchar2
   ,p_cal_year           in    number
  )  
  is
   --
   l_split_partition_sql  varchar2(4000) :=Null;
   l_proceed boolean;
   --
   l_request_id number :=fnd_global.conc_request_id;
   l_user_id number :=fnd_global.user_id;
   --
    cursor fiscal_periods  is
    select period_name                                         fiscal_period
               ,substr(upper(period_name), 1, 3) fiscal_month
               ,upper(replace(period_name, '-', '')) fiscal_partition
               ,(
                    select count(utp.partition_name)
                    from dba_tab_partitions  utp
                    where 1 =1
                      and utp.table_owner ='XXCUS'
                      and utp.table_name  ='XXCUS_OZF_XLA_ACCRUALS_ARCHIVE'
                      and utp.partition_name =upper(replace(period_name, '-', ''))
                ) partition_count      
    from gl_periods
    where 1 =1
    and period_set_name ='4-4-QTR'
    and end_date between to_date('01-JAN-'||p_cal_year) and to_date('31-DEC-'||p_cal_year)
    and adjustment_period_flag ='N'
    order by period_year asc, period_num asc; 
   --
  begin
     --   
     for rec in fiscal_periods loop
      --
      if (rec.partition_count =0) then
       --
       l_split_partition_sql :=Null;
       --
            select 'ALTER TABLE XXCUS.XXCUS_OZF_XLA_ACCRUALS_ARCHIVE '||'
                   '||'SPLIT PARTITION NA VALUES ('||''''||rec.fiscal_period||''''||') '||'
                   '||' INTO '||'
                   '||' ( '||'
                   '||' PARTITION '||rec.fiscal_partition||','||'
                   '||' PARTITION NA '||'
                   '||')' 
            into l_split_partition_sql
            from dual;
       --
           print_log(' ');
           print_log(l_split_partition_sql);
           print_log(' ');
           print_log('...Before creating partition '||rec.fiscal_partition);
           execute immediate  l_split_partition_sql;
           print_log('......After creating partition '||rec.fiscal_partition);           
       --  
      else
           --Partition exists...no need to create...move on      
           print_log(' ');
           print_log('TM Accrual archive table partition '||rec.fiscal_partition||' already exists.');
           print_log(' ');
       --
      end if;
      --
     end loop;     
     --
  exception
   when others then
    print_log( 'Issue in setup_accr_archive_partitions routine ='||sqlerrm);
  end setup_accr_archive_partitions;  
  --
  -- End Ver 1.1       
  --      
end XXCUS_REBATES_ARCHIVE;
/
show errors;