CREATE OR REPLACE 
PACKAGE BODY     APPS.XXWC_INTANGBLE_ITEM_CORRECTION
AS
   /*************************************************************************
   *   $Header XXWC_INTANGBLE_ITEM_CORRECTION.PKG $
   *   Module Name: XXWC_INTANGBLE_ITEM_CORRECTION.PKG
   *
   *   PURPOSE:   This package is used update the intangible items status
   *              and corrected the SO and PO open line items
   *
   *   REVISIONS:
   *   Ver     Date        Author                Description
   *   ------  ----------  ---------------       -------------------------
   *   1.0     08/09/2018  Pattabhi Avula        Initial Version -- TMS#20180808-00080
   * ***************************************************************************/

   PROCEDURE MAIN (p_errbuf               OUT VARCHAR2,
                   p_retcode              OUT VARCHAR2,
                   p_item_id           IN     NUMBER,
                   p_organization_id   IN     NUMBER,
                   p_days_old          IN     NUMBER)
   AS
      l_so_errbuf      VARCHAR2 (2000);
      l_so_retcode     VARCHAR2 (100);

      l_po_errbuf      VARCHAR2 (2000);
      l_po_retcode     VARCHAR2 (100);

      -- item API
      l_item_errbuf    VARCHAR2 (2000);
      l_item_retcode   VARCHAR2 (100);

      -- SO  lines Adding API
      l_sl_errbuf      VARCHAR2 (2000);
      l_sl_retcode     VARCHAR2 (100);

      -- PO linesAdding API
      l_pl_errbuf      VARCHAR2 (2000);
      l_pl_retcode     VARCHAR2 (100);

      CURSOR c1
      IS
         SELECT inventory_item_id item_id, ship_from_org_id organization_id
           FROM xxwc.xxwc_oe_ord_intangble_lines
          WHERE GLOBAL_ATTRIBUTE1 = 'Y'
         UNION
         SELECT pl.ITEM_ID item_id,
                ploc.SHIP_TO_ORGANIZATION_ID organization_id
           FROM xxwc.xxwc_po_intangible_lines pl,
                xxwc.xxwc_po_line_loc_intangibles ploc
          WHERE     1 = 1
                AND pl.po_line_id = ploc.po_line_id
                AND pl.GLOBAL_ATTRIBUTE1 = 'Y';
   BEGIN
      BEGIN
         DELETE FROM po.po_headers_interface
               WHERE po_header_id IN (SELECT DISTINCT po_header_id
                                        FROM xxwc.xxwc_po_intangible_lines
                                       WHERE item_id = p_item_id);

         COMMIT;
      EXCEPTION
         WHEN OTHERS
         THEN
            FND_FILE.PUT_LINE (
               FND_FILE.LOG,
                  'Error/no data found while deleting records from po_headers_interface-'
               || SQLERRM);
            NULL;
      END;

      BEGIN
         DELETE FROM po.po_lines_interface
               WHERE po_header_id IN (SELECT DISTINCT po_header_id
                                        FROM xxwc.xxwc_po_intangible_lines
                                       WHERE item_id = p_item_id);

         COMMIT;
      EXCEPTION
         WHEN OTHERS
         THEN
            FND_FILE.PUT_LINE (
               FND_FILE.LOG,
                  'Error/no data found while deleting records from po_lines_interface- '
               || SQLERRM);
            NULL;
      END;

      BEGIN
         DELETE FROM po.po_distributions_interface
               WHERE po_header_id IN (SELECT DISTINCT po_header_id
                                        FROM xxwc.xxwc_po_intangible_lines
                                       WHERE item_id = p_item_id);

         COMMIT;
      EXCEPTION
         WHEN OTHERS
         THEN
            FND_FILE.PUT_LINE (
               FND_FILE.LOG,
                  'Error/no data found while deleting records from po_distributions_interface- '
               || SQLERRM);
            NULL;
      END;

      BEGIN
         DELETE FROM xxwc.xxwc_po_headers_intang_tbl
               WHERE po_header_id IN (SELECT DISTINCT po_header_id
                                        FROM xxwc.xxwc_po_intangible_lines
                                       WHERE item_id = p_item_id);

         COMMIT;
      EXCEPTION
         WHEN OTHERS
         THEN
            FND_FILE.PUT_LINE (
               FND_FILE.LOG,
                  'Error/no data found while deleting records from backup xxwc_po_headers_intang_tbl-'
               || SQLERRM);
            NULL;
      END;

      BEGIN
         DELETE FROM xxwc.xxwc_po_line_loc_intangibles
               WHERE po_line_id IN (SELECT DISTINCT po_line_id
                                      FROM xxwc.xxwc_po_intangible_lines
                                     WHERE item_id = p_item_id);

         COMMIT;
      EXCEPTION
         WHEN OTHERS
         THEN
            FND_FILE.PUT_LINE (
               FND_FILE.LOG,
                  'Error/no data found while deleting records from backup xxwc_po_line_loc_intangibles- '
               || SQLERRM);
            NULL;
      END;

      BEGIN
         DELETE FROM xxwc.xxwc_po_intangible_lines
               WHERE item_id = p_item_id;

         COMMIT;
      EXCEPTION
         WHEN OTHERS
         THEN
            FND_FILE.PUT_LINE (
               FND_FILE.LOG,
                  'Error/no data found while deleting records from backup xxwc_po_intangible_lines-'
               || SQLERRM);
            NULL;
      END;

      BEGIN
         DELETE FROM xxwc.xxwc_oe_ord_intangble_lines
               WHERE inventory_item_id = p_item_id;

         COMMIT;
      EXCEPTION
         WHEN OTHERS
         THEN
            FND_FILE.PUT_LINE (
               FND_FILE.LOG,
                  'Error/no data found while deleting records from backup xxwc_oe_ord_intangble_lines- '
               || SQLERRM);
            NULL;
      END;

      FND_FILE.PUT_LINE (FND_FILE.LOG,
                         'Sales order line cancellation process - Start');
                         
      -- PO process
      FND_FILE.PUT_LINE (FND_FILE.LOG,
                         'Purchase order line cancellation process - Start');

      PURCHASE_ORD_PROCESS (l_po_errbuf,
                            l_po_retcode,
                            p_item_id,
                            p_organization_id,
                            p_days_old);

      FND_FILE.PUT_LINE (FND_FILE.LOG,
                         'Purchase order line cancellation process - End');                         

      SO_CANCEL_PROCESS (l_so_errbuf,
                         l_so_retcode,
                         p_item_id,
                         p_organization_id,
                         p_days_old);

      FND_FILE.PUT_LINE (FND_FILE.LOG,
                         'Sales order line cancellation process - End');

     -- Item update API
      FND_FILE.PUT_LINE (FND_FILE.LOG, 'Item status update process - Start');

      FOR item_upd IN c1
      LOOP
         item_status_udpate (l_item_errbuf,
                             l_item_retcode,
                             item_upd.item_id,
                             item_upd.organization_id);
      END LOOP;

      FND_FILE.PUT_LINE (FND_FILE.LOG, 'Item status update process - End');

      -- SO line adding process
      FND_FILE.PUT_LINE (FND_FILE.LOG,
                         'Sales order line add process - Start');

      SO_LINE_ADD (l_sl_errbuf, l_sl_retcode);

      FND_FILE.PUT_LINE (FND_FILE.LOG, 'Sales order line add process - End');

      -- PO line adding process
      FND_FILE.PUT_LINE (FND_FILE.LOG,
                         'Purchase order line add process - Start');

      add_lines_to_po (l_pl_errbuf, l_pl_retcode);

      FND_FILE.PUT_LINE (FND_FILE.LOG,
                         'Purchase order line add process - End');
   EXCEPTION
      WHEN OTHERS
      THEN
         FND_FILE.PUT_LINE (FND_FILE.LOG, 'Error details are ' || SQLERRM);
   END;

   PROCEDURE SO_CANCEL_PROCESS (p_errbuf               OUT VARCHAR2,
                                p_retcode              OUT VARCHAR2,
                                p_item_id           IN     NUMBER,
                                p_organization_id   IN     NUMBER,
                                p_days_old          IN     NUMBER)
   IS
      l_errbuf                       VARCHAR2 (2000);
      l_retcode                      VARCHAR2 (100);
      l_user_id                      NUMBER;
      l_resp_id                      NUMBER;
      l_appl_id                      NUMBER;
      l_item_status_so               VARCHAR2 (20);
      l_item_name                    VARCHAR2 (40);


      l_header_rec_in                oe_order_pub.header_rec_type; -- pl/sql table and record definition to be used as IN parameters
      l_line_tbl_in                  oe_order_pub.line_tbl_type; -- pl/sql table and record definition to be used as IN parameters
      l_action_request_tbl_in        oe_order_pub.request_tbl_type; -- Used to assigining Book Order related input parameters
      l_header_rec_out               oe_order_pub.header_rec_type; -- pl/sql table and record definition to be used as OUT parameters
      l_line_tbl_out                 oe_order_pub.line_tbl_type;
      l_header_val_rec_out           oe_order_pub.header_val_rec_type;
      l_header_adj_tbl_out           oe_order_pub.header_adj_tbl_type;
      l_header_adj_val_tbl_out       oe_order_pub.header_adj_val_tbl_type;
      l_header_price_att_tbl_out     oe_order_pub.header_price_att_tbl_type;
      l_header_adj_att_tbl_out       oe_order_pub.header_adj_att_tbl_type;
      l_header_adj_assoc_tbl_out     oe_order_pub.header_adj_assoc_tbl_type;
      l_header_scredit_tbl_out       oe_order_pub.header_scredit_tbl_type;
      l_header_scredit_val_tbl_out   oe_order_pub.header_scredit_val_tbl_type;
      l_line_val_tbl_out             oe_order_pub.line_val_tbl_type;
      l_line_adj_tbl_out             oe_order_pub.line_adj_tbl_type;
      l_line_adj_val_tbl_out         oe_order_pub.line_adj_val_tbl_type;
      l_line_price_att_tbl_out       oe_order_pub.line_price_att_tbl_type;
      l_line_adj_att_tbl_out         oe_order_pub.line_adj_att_tbl_type;
      l_line_adj_assoc_tbl_out       oe_order_pub.line_adj_assoc_tbl_type;
      l_line_scredit_tbl_out         oe_order_pub.line_scredit_tbl_type;
      l_line_scredit_val_tbl_out     oe_order_pub.line_scredit_val_tbl_type;
      l_lot_serial_tbl_out           oe_order_pub.lot_serial_tbl_type;
      l_lot_serial_val_tbl_out       oe_order_pub.lot_serial_val_tbl_type;
      l_action_request_tbl_out       oe_order_pub.request_tbl_type;
      l_chr_program_unit_name        VARCHAR2 (100); -- To store the package and procedure name for logging
      l_chr_ret_status               VARCHAR2 (1000) := NULL; -- To store the error message code returned by API
      l_msg_count                    NUMBER := 0; -- To store the number of error messages API has encountered
      l_msg_data                     VARCHAR2 (2000); -- To store the error message text returned by API
      l_num_api_version              NUMBER := 1.0;             -- API version
      l_action_request_tbl           OE_ORDER_PUB.Request_Tbl_Type;
      l_msg_index_out                NUMBER (10);
      lv_msg_data                    VARCHAR2 (2000);
      
      result boolean;
      resulti number;

      CURSOR Cur_orders
      IS
           SELECT oel.*
             FROM oe_order_lines_all oel, mtl_system_items_b msi
            WHERE     1 = 1
                  AND TRUNC (oel.last_update_date) >
                         TRUNC (SYSDATE) - NVL (p_days_old, 90)
                  AND oel.inventory_item_id =
                         NVL (p_item_id, oel.inventory_item_id)
                  AND oel.ship_from_org_id =
                         NVL (p_organization_id, oel.ship_from_org_id)
                  AND NVL (oel.cancelled_flag, 'N') = 'N'
                  AND oel.org_id = 162
                  AND oel.booked_flag = 'Y'
                  AND NVL (shipped_quantity, 0) = 0
                  AND NVL (fulfilled_quantity, 0) = 0
                  AND oel.open_flag = 'Y'
                  AND oel.inventory_item_id = msi.inventory_item_id
                  AND oel.ship_from_org_id = msi.organization_id
                  AND msi.INVENTORY_ITEM_STATUS_CODE <> 'Intangible'
                  AND oel.flow_status_code IN ('AWAITING_SHIPPING','AWAITING_RECEIPT','BOOKED')
                  AND (   msi.mtl_transactions_enabled_flag = 'Y'
                       OR msi.INVENTORY_ASSET_FLAG = 'Y'
                       OR msi.COSTING_ENABLED_FLAG = 'Y')
                  /*AND EXISTS
                        (SELECT 1
                           FROM org_organization_definitions ood
                          WHERE     operating_unit = 162
                        AND NVL (disable_date, TRUNC (SYSDATE)) >=
                             TRUNC (SYSDATE)
                      AND organization_id = oel.ship_from_org_id)*/
                  AND 0 =
                         (SELECT NVL (SUM (item_cost), 0)
                            FROM cst_item_costs
                           WHERE     cost_type_id = 2
                                 AND inventory_item_id = oel.inventory_item_id
                                 AND organization_id = oel.ship_from_org_id)
                  AND NOT EXISTS 
                         (SELECT transaction_quantity
                            FROM mtl_onhand_quantities_detail
                           WHERE     inventory_item_id = oel.inventory_item_id
                                 AND organization_id = oel.ship_from_org_id
                                 AND ROWNUM=1)
         ORDER BY 1;
   BEGIN
      DBMS_OUTPUT.ENABLE (100000);

      SELECT user_id
        INTO l_user_id
        FROM fnd_user
       WHERE user_name = 'XXWC_INT_SUPPLYCHAIN';

      SELECT responsibility_id, application_id
        INTO l_resp_id, l_appl_id
        FROM fnd_responsibility_vl
       WHERE responsibility_name = 'HDS Order Mgmt Pricing Super - WC';

      fnd_global.apps_initialize (l_user_id, l_resp_id, l_appl_id);
      
   

      FOR I IN Cur_orders
      LOOP
         -- Inserting into bkp table

         INSERT INTO xxwc.xxwc_oe_ord_intangble_lines
            SELECT *
              FROM oe_order_lines_all
             WHERE line_id = I.line_id;

         COMMIT;
         
            begin
  -- Call the function
            result := fnd_profile.save(x_name => 'XXWC_OM_DEFAULT_SHIPPING_ORG',
                             x_value => i.ship_from_org_id,
                             x_level_name => 'USER',
                             x_level_value => l_user_id,
                             x_level_value_app_id => NULL,
                             x_level_value2 => NULL);
            resulti := sys.diutil.bool_to_int(result);
            COMMIT;
            EXCEPTION
            WHEN OTHERS THEN 
            NULL;
            end; 

         -- Cancelling the sales order lines
         l_line_tbl_in (1) := oe_order_pub.g_miss_line_rec;
         l_line_tbl_in (1).line_id := I.line_id;
         l_line_tbl_in (1).ordered_quantity := 0;
         l_line_tbl_in (1).change_reason := 'Admin Error';
         l_line_tbl_in (1).change_comments := 'CANCEL LINE TMS 20180808-00080';
         l_line_tbl_in (1).operation := oe_globals.g_opr_update;

         oe_msg_pub.delete_msg;
         mo_global.init ('ONT');
         mo_global.set_policy_context ('S', I.org_id);

         oe_order_pub.process_order (
            p_api_version_number       => l_num_api_version,
            p_org_id                   => mo_global.get_current_org_id,
            p_init_msg_list            => fnd_api.g_false,
            p_return_values            => fnd_api.g_false,
            p_action_commit            => fnd_api.g_false,
            p_line_tbl                 => l_line_tbl_in,
            x_header_rec               => l_header_rec_out,
            x_header_val_rec           => l_header_val_rec_out,
            x_header_adj_tbl           => l_header_adj_tbl_out,
            x_header_adj_val_tbl       => l_header_adj_val_tbl_out,
            x_header_price_att_tbl     => l_header_price_att_tbl_out,
            x_header_adj_att_tbl       => l_header_adj_att_tbl_out,
            x_header_adj_assoc_tbl     => l_header_adj_assoc_tbl_out,
            x_header_scredit_tbl       => l_header_scredit_tbl_out,
            x_header_scredit_val_tbl   => l_header_scredit_val_tbl_out,
            x_line_tbl                 => l_line_tbl_out,
            x_line_val_tbl             => l_line_val_tbl_out,
            x_line_adj_tbl             => l_line_adj_tbl_out,
            x_line_adj_val_tbl         => l_line_adj_val_tbl_out,
            x_line_price_att_tbl       => l_line_price_att_tbl_out,
            x_line_adj_att_tbl         => l_line_adj_att_tbl_out,
            x_line_adj_assoc_tbl       => l_line_adj_assoc_tbl_out,
            x_line_scredit_tbl         => l_line_scredit_tbl_out,
            x_line_scredit_val_tbl     => l_line_scredit_val_tbl_out,
            x_lot_serial_tbl           => l_lot_serial_tbl_out,
            x_lot_serial_val_tbl       => l_lot_serial_val_tbl_out,
            x_action_request_tbl       => l_action_request_tbl_out,
            x_return_status            => l_chr_ret_status,
            x_msg_count                => l_msg_count,
            x_msg_data                 => l_msg_data);

         l_line_tbl_in.DELETE;

         l_msg_data := NULL;
         FND_FILE.PUT_LINE (FND_FILE.LOG,
                            'Return Status: ' || l_chr_ret_status);

         IF l_chr_ret_status <> 'S'
         THEN
            FOR iindx IN 1 .. l_msg_count
            LOOP
               l_msg_data := l_msg_data || '.' || oe_msg_pub.get (iindx);
               FND_FILE.PUT_LINE (
                  FND_FILE.LOG,
                     'Sales order Line-'
                  || I.line_id
                  || ' '
                  || 'Cancel error'
                  || 'l_msg_data-'
                  || l_msg_data);
            END LOOP;

            UPDATE xxwc.xxwc_oe_ord_intangble_lines
               SET GLOBAL_ATTRIBUTE1 = 'E'
             WHERE     line_id = I.line_id
                   AND ship_from_org_id = I.ship_from_org_id;

            COMMIT;
         ELSE
            UPDATE xxwc.xxwc_oe_ord_intangble_lines
               SET GLOBAL_ATTRIBUTE1 = 'Y'
             WHERE     line_id = I.line_id
                   AND ship_from_org_id = I.ship_from_org_id;

            COMMIT;
            FND_FILE.PUT_LINE (
               FND_FILE.LOG,
                  'Sales order Line-'
               || I.line_id
               || ' '
               || 'Cancelled Successfully'
               || 'l_msg_data-'
               || l_msg_data);
         END IF;
      ------- End -------- order cancellation

      END LOOP;                                                  -- First loop
   EXCEPTION
      WHEN OTHERS
      THEN
         NULL;
   END;                                                   -- MAIN PROC CLOSING

   PROCEDURE SO_LINE_ADD (p_errbuf OUT VARCHAR2, p_retcode OUT VARCHAR2)
   IS
      -- Addling line API variables
      l_msg_count                NUMBER := 0; -- To store the number of error messages API has encountered
      l_msg_data                 VARCHAR2 (2000); -- To store the error message text returned by API
      l_num_api_version          NUMBER := 1.0;                 -- API version
      l_action_request_tbl       OE_ORDER_PUB.Request_Tbl_Type;
      l_msg_index_out            NUMBER (10);
      lv_msg_data                VARCHAR2 (2000);
      l_return_status            VARCHAR2 (1);
      l_line_cnt                 NUMBER := 0;
      l_line_tbl                 OE_ORDER_PUB.Line_Tbl_Type;  --line_tbl_type;
      l_flow_status_code         VARCHAR2 (100);
      l_header_rec               OE_ORDER_PUB.Header_Rec_Type
                                    := OE_ORDER_PUB.G_MISS_HEADER_REC;
      l_old_header_rec           OE_ORDER_PUB.Header_Rec_Type;

      --l_line_tbl   OE_ORDER_PUB.Line_Tbl_Type;
      l_old_line_tbl             OE_ORDER_PUB.Line_Tbl_Type;
      x_header_rec               OE_ORDER_PUB.Header_Rec_Type;
      x_header_val_rec           OE_ORDER_PUB.Header_Val_Rec_Type;
      x_Header_Adj_tbl           OE_ORDER_PUB.Header_Adj_Tbl_Type;
      x_Header_Adj_val_tbl       OE_ORDER_PUB.Header_Adj_Val_Tbl_Type;
      x_Header_price_Att_tbl     OE_ORDER_PUB.Header_Price_Att_Tbl_Type;
      x_Header_Adj_Att_tbl       OE_ORDER_PUB.Header_Adj_Att_Tbl_Type;
      x_Header_Adj_Assoc_tbl     OE_ORDER_PUB.Header_Adj_Assoc_Tbl_Type;
      x_Header_Scredit_tbl       OE_ORDER_PUB.Header_Scredit_Tbl_Type;
      x_Header_Scredit_val_tbl   OE_ORDER_PUB.Header_Scredit_Val_Tbl_Type;
      x_line_tbl                 OE_ORDER_PUB.Line_Tbl_Type;
      x_line_val_tbl             OE_ORDER_PUB.Line_Val_Tbl_Type;
      x_Line_Adj_tbl             OE_ORDER_PUB.Line_Adj_Tbl_Type;
      x_Line_Adj_val_tbl         OE_ORDER_PUB.Line_Adj_Val_Tbl_Type;
      x_Line_price_Att_tbl       OE_ORDER_PUB.Line_Price_Att_Tbl_Type;
      x_Line_Adj_Att_tbl         OE_ORDER_PUB.Line_Adj_Att_Tbl_Type;
      x_Line_Adj_Assoc_tbl       OE_ORDER_PUB.Line_Adj_Assoc_Tbl_Type;
      x_Line_Scredit_tbl         OE_ORDER_PUB.Line_Scredit_Tbl_Type;
      x_Line_Scredit_val_tbl     OE_ORDER_PUB.Line_Scredit_Val_Tbl_Type;
      x_Lot_Serial_tbl           OE_ORDER_PUB.Lot_Serial_Tbl_Type;
      x_Lot_Serial_val_tbl       OE_ORDER_PUB.Lot_Serial_Val_Tbl_Type;
      x_action_request_tbl       OE_ORDER_PUB.Request_Tbl_Type;

      CURSOR cur_lines_add
      IS
         SELECT *
           FROM xxwc.xxwc_oe_ord_intangble_lines
          WHERE GLOBAL_ATTRIBUTE1 = 'Y';
   BEGIN
      -- Calling API to add order line to existing order
      FOR SA IN cur_lines_add
      LOOP
         FND_FILE.PUT_LINE (FND_FILE.LOG, 'I.line_id' || SA.line_id);
         DBMS_OUTPUT.put_line ('I.line_id' || SA.line_id);
         l_line_cnt := l_line_cnt + 1;
         l_line_tbl (l_line_cnt) := OE_ORDER_PUB.G_MISS_LINE_REC;
         l_line_tbl (l_line_cnt).operation := OE_GLOBALS.G_OPR_CREATE;

         -- l_line_tbl (l_line_cnt).operation              := 'CREATE'; --OE_GLOBALS.G_OPR_CREATE;
         l_line_tbl (l_line_cnt).header_id := SA.header_id;
         l_line_tbl (l_line_cnt).inventory_item_id := SA.inventory_item_id;
         l_line_tbl (l_line_cnt).ship_from_org_id := SA.ship_from_org_id;
         l_line_tbl (l_line_cnt).ordered_quantity := SA.ORDERED_QUANTITY;
         l_line_tbl (l_line_cnt).UNIT_SELLING_PRICE := SA.UNIT_SELLING_PRICE;
         l_line_tbl (l_line_cnt).CALCULATE_PRICE_FLAG := 'N';
         l_line_tbl (l_line_cnt).PACKING_INSTRUCTIONS :=
            SA.PACKING_INSTRUCTIONS;
         l_line_tbl (l_line_cnt).SOURCE_TYPE_CODE := SA.SOURCE_TYPE_CODE;
         l_line_tbl (l_line_cnt).SHIPPING_INSTRUCTIONS :=
            SA.SHIPPING_INSTRUCTIONS;
         l_line_tbl (l_line_cnt).SUBINVENTORY := SA.SUBINVENTORY;
         --l_line_tbl (l_line_cnt).SUBINVENTORY := SA.ORDER_QUANTITY_UOM;
         --l_line_tbl (l_line_cnt).SUBINVENTORY := SA.PRICING_QUANTITY_UOM;
         l_line_tbl (l_line_cnt).CUST_PO_NUMBER := SA.CUST_PO_NUMBER;
         l_line_tbl (l_line_cnt).SHIPPING_METHOD_CODE :=
            SA.SHIPPING_METHOD_CODE;

         OE_ORDER_PUB.Process_Order (
            p_api_version_number       => 1,
            p_org_id                   => 162,
            p_init_msg_list            => FND_API.G_TRUE,
            p_return_values            => FND_API.G_TRUE,
            p_action_commit            => FND_API.G_TRUE,
            x_return_status            => l_return_status,
            x_msg_count                => l_msg_count,
            x_msg_data                 => l_msg_data,
            p_action_request_tbl       => l_action_request_tbl,
            p_header_rec               => l_header_rec,
            p_old_header_rec           => l_old_header_rec,
            p_line_tbl                 => l_line_tbl,
            p_old_line_tbl             => l_old_line_tbl,
            x_header_rec               => x_header_rec,
            x_header_val_rec           => x_header_val_rec,
            x_Header_Adj_tbl           => x_Header_Adj_tbl,
            x_Header_Adj_val_tbl       => x_Header_Adj_val_tbl,
            x_Header_price_Att_tbl     => x_Header_Price_Att_Tbl,
            x_Header_Adj_Att_tbl       => x_Header_Adj_Att_Tbl,
            x_Header_Adj_Assoc_tbl     => x_Header_Adj_Assoc_Tbl,
            x_Header_Scredit_tbl       => x_Header_Scredit_Tbl,
            x_Header_Scredit_val_tbl   => x_Header_Scredit_Val_Tbl,
            x_line_tbl                 => x_Line_Tbl,
            x_line_val_tbl             => x_Line_Val_Tbl,
            x_Line_Adj_tbl             => x_Line_Adj_Tbl,
            x_Line_Adj_val_tbl         => x_Line_Adj_Val_Tbl,
            x_Line_price_Att_tbl       => x_Line_Price_Att_Tbl,
            x_Line_Adj_Att_tbl         => x_Line_Adj_Att_Tbl,
            x_Line_Adj_Assoc_tbl       => x_Line_Adj_Assoc_Tbl,
            x_Line_Scredit_tbl         => x_Line_Scredit_Tbl,
            x_Line_Scredit_val_tbl     => x_Line_Scredit_Val_Tbl,
            x_Lot_Serial_tbl           => x_Lot_Serial_Tbl,
            x_Lot_Serial_val_tbl       => x_Lot_Serial_Val_Tbl,
            x_action_request_tbl       => x_action_request_tbl);

         l_action_request_tbl.DELETE;
         l_line_tbl.DELETE;
         l_old_line_tbl.DELETE;
         l_line_cnt := 0;

         IF l_return_status = FND_API.G_RET_STS_SUCCESS
         THEN
            FOR i IN 1 .. l_msg_count
            LOOP
               Oe_Msg_Pub.get (p_msg_index       => i,
                               p_encoded         => Fnd_Api.G_FALSE,
                               p_data            => l_msg_data,
                               p_msg_index_out   => l_msg_index_out);
               lv_msg_data :=
                  lv_msg_data || ':' || SUBSTR (l_msg_data, 1, 100);
            END LOOP;

            l_msg_data := NVL (lv_msg_data, 'Lines Added Successfully');
            l_return_status := l_return_status;
            --       o_msg_data:= 'Lines Added Successfully';
            --       o_return_status := 'S';
            DBMS_OUTPUT.put_line ('Return Status ==>' || l_return_status);
            FND_FILE.PUT_LINE (
               FND_FILE.LOG,
               'SO Line add API Return Status' || l_return_status);
            DBMS_OUTPUT.put_line ('l_msg_data ==>' || l_msg_data);
            FND_FILE.PUT_LINE (FND_FILE.LOG,
                               'SO Line add API Return Status' || l_msg_data);

            UPDATE xxwc.xxwc_oe_ord_intangble_lines
               SET GLOBAL_ATTRIBUTE1 = 'P'
             WHERE     line_id = SA.line_id
                   AND ship_from_org_id = SA.ship_from_org_id;

            COMMIT;
         ELSE
            -- Retrieve messages
            FOR i IN 1 .. l_msg_count
            LOOP
               Oe_Msg_Pub.get (p_msg_index       => i,
                               p_encoded         => Fnd_Api.G_FALSE,
                               p_data            => l_msg_data,
                               p_msg_index_out   => l_msg_index_out);
               lv_msg_data :=
                  lv_msg_data || ':' || SUBSTR (l_msg_data, 1, 100);
            END LOOP;

            UPDATE xxwc.xxwc_oe_ord_intangble_lines
               SET GLOBAL_ATTRIBUTE1 = 'E'
             WHERE     line_id = SA.line_id
                   AND ship_from_org_id = SA.ship_from_org_id;

            COMMIT;
            l_msg_data := lv_msg_data;
            l_return_status := l_return_status;
            DBMS_OUTPUT.put_line ('Return Status ==>' || l_return_status);
            FND_FILE.PUT_LINE (
               FND_FILE.LOG,
               'SO Line add API Return Status' || l_return_status);
            DBMS_OUTPUT.put_line ('l_msg_data ==>' || l_msg_data);
            FND_FILE.PUT_LINE (FND_FILE.LOG,
                               'SO Line add API Return Status' || l_msg_data);
         END IF;
      END LOOP;                                                   -- Main loop
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.put_line ('Error adding SO Line using API' || SQLERRM);
         FND_FILE.PUT_LINE (FND_FILE.LOG,
                            'Error adding SO Line using API' || SQLERRM);
         NULL;
   END;

   -- Item update proc

   PROCEDURE item_status_udpate (p_errbuf               OUT VARCHAR2,
                                 p_retcode              OUT VARCHAR2,
                                 p_item_id           IN     NUMBER,
                                 p_organization_id   IN     NUMBER)
   IS
      l_user_id             NUMBER;
      l_resp_id             NUMBER;
      l_appl_id             NUMBER;
      l_item_tbl_typ        ego_item_pub.item_tbl_type;
      x_item_table          ego_item_pub.item_tbl_type;
      x_inventory_item_id   mtl_system_items_b.inventory_item_id%TYPE;
      x_organization_id     mtl_system_items_b.organization_id%TYPE;
      x_return_status       VARCHAR2 (1);
      x_msg_count           NUMBER (10);
      x_msg_data            VARCHAR2 (1000);
      x_message_list        error_handler.error_tbl_type;
   BEGIN
      DBMS_OUTPUT.ENABLE (100000);

      SELECT user_id
        INTO l_user_id
        FROM fnd_user
       WHERE user_name = 'XXWC_INT_SUPPLYCHAIN';

      SELECT responsibility_id, application_id
        INTO l_resp_id, l_appl_id
        FROM fnd_responsibility_vl
       WHERE responsibility_name = 'HDS Inventory Super User - WC';

      fnd_global.apps_initialize (l_user_id, l_resp_id, l_appl_id);

      --FIRST Item definition
      l_item_tbl_typ (1).transaction_type := 'UPDATE'; -- Replace this with 'UPDATE' for update transaction.
      l_item_tbl_typ (1).inventory_item_id := p_item_id;
      l_item_tbl_typ (1).organization_id := p_organization_id;
      l_item_tbl_typ (1).inventory_item_status_code := 'Intangible';
      --DBMS_OUTPUT.put_line ('=====================================');
      --DBMS_OUTPUT.put_line ('Calling EGO_ITEM_PUB.Process_Items API');
      ego_item_pub.process_items (p_api_version     => 1.0,
                                  p_init_msg_list   => fnd_api.g_true,
                                  p_commit          => fnd_api.g_true,
                                  p_item_tbl        => l_item_tbl_typ,
                                  x_item_tbl        => x_item_table,
                                  x_return_status   => x_return_status,
                                  x_msg_count       => x_msg_count);
      --DBMS_OUTPUT.put_line ('==================================');
      --DBMS_OUTPUT.put_line ('Return Status ==>' || x_return_status);
      l_item_tbl_typ.DELETE;

      IF (x_return_status = fnd_api.g_ret_sts_success)
      THEN
         FOR i IN 1 .. x_item_table.COUNT
         LOOP
            FND_FILE.PUT_LINE (
               FND_FILE.LOG,
                  'Item staus update is successfull inventory_item_id- :'
               || TO_CHAR (x_item_table (i).inventory_item_id));
            FND_FILE.PUT_LINE (
               FND_FILE.LOG,
                  'Item staus update is successfull organization_id- :'
               || TO_CHAR (x_item_table (i).organization_id));
            --DBMS_OUTPUT.put_line ('Inventory Item Id :' || TO_CHAR (x_item_table (i).inventory_item_id));
            --DBMS_OUTPUT.put_line ('Organization Id   :' || TO_CHAR (x_item_table (i).organization_id));
            NULL;
         END LOOP;
      ELSE
         FND_FILE.PUT_LINE (FND_FILE.LOG,
                            'Item staus update API error messages :');
         error_handler.get_message_list (x_message_list => x_message_list);

         FOR i IN 1 .. x_message_list.COUNT
         LOOP
            FND_FILE.PUT_LINE (FND_FILE.LOG, x_message_list (i).MESSAGE_TEXT);
         END LOOP;
      END IF;
   --DBMS_OUTPUT.put_line ('==================================');

   -- END LOOP;

   EXCEPTION
      WHEN OTHERS
      THEN
         FND_FILE.PUT_LINE (
            FND_FILE.LOG,
            'Error while updating item status using API' || SQLERRM);
         DBMS_OUTPUT.put_line ('Exception Occured :');
         DBMS_OUTPUT.put_line (SQLCODE || ':' || SQLERRM);
         DBMS_OUTPUT.put_line ('=====================================');
         NULL;
   END;

   -- PO Process starts from here
   PROCEDURE PURCHASE_ORD_PROCESS (p_errbuf               OUT VARCHAR2,
                                   p_retcode              OUT VARCHAR2,
                                   p_item_id           IN     NUMBER,
                                   p_organization_id   IN     NUMBER,
                                   p_days_old          IN     NUMBER)
   IS
      l_errbuf           VARCHAR2 (2000);
      l_retcode          VARCHAR2 (100);
      l_user_id          NUMBER;
      l_resp_id          NUMBER;
      l_appl_id          NUMBER;
      l_item_status_po   VARCHAR2 (20);
      l_item_name_po     VARCHAR2 (40);

      v_return_status    VARCHAR2 (10);
      v_msg_data         VARCHAR2 (1000);
      v_po_header_id     NUMBER;
      v_doc_subtype      VARCHAR2 (10) := 'STANDARD';
      v_doc_type         VARCHAR2 (10) := 'PO';
      v_org_id           NUMBER := fnd_profile.VALUE ('Org_id');
      v_action           VARCHAR2 (10) := 'CANCEL';
      v_action_date      DATE := SYSDATE;
      v_line_num         NUMBER;
      v_ship_num         NUMBER;
      v_po_c_flag        VARCHAR2 (10);
      v_line_po_c_flag   VARCHAR2 (10);
      v_ship_po_c_flag   VARCHAR2 (10);
      v_line_loc_id      NUMBER;

      CURSOR Cur_po_dtls
      IS
         SELECT pla.*, plla.ship_to_organization_id, plla.line_location_id
           FROM po_lines_all pla,
                po_line_locations_all plla,
                po_headers_all pha
          WHERE     1 = 1
                AND pha.po_header_id = pla.po_header_id
                AND pha.po_header_id = plla.po_header_id
                AND pla.po_line_id = plla.po_line_id
                AND NVL (pla.cancel_flag, 'N') = 'N'
                AND NVL(pha.CLOSED_CODE,'OPEN')='OPEN'
                AND NVL(pla.CLOSED_CODE,'OPEN')='OPEN'
                AND NVL(plla.CLOSED_CODE,'OPEN')='OPEN'
                --AND NVL(plla.DROP_SHIP_FLAG,'N')='N'
                AND pha.authorization_status='APPROVED'
                AND NVL(plla.QUANTITY_RECEIVED,0)=0
                AND pla.item_id = NVL (p_item_id, pla.item_id)
                AND plla.ship_to_organization_id =
                       NVL (p_organization_id, plla.ship_to_organization_id)
                AND pha.org_id = 162
                AND TRUNC (pla.last_update_date) >
                       TRUNC (SYSDATE) - NVL (p_days_old, 90)
                AND 0 =
                       (SELECT NVL (SUM (item_cost), 0)
                          FROM cst_item_costs
                         WHERE     cost_type_id = 2
                               AND inventory_item_id = pla.item_id
                               AND organization_id =
                                      plla.ship_to_organization_id)
                /*AND EXISTS
                        (SELECT 1
                           FROM org_organization_definitions ood
                          WHERE     operating_unit = 162
                        AND NVL (disable_date, TRUNC (SYSDATE)) >=
                             TRUNC (SYSDATE)
                      AND organization_id = plla.ship_to_organization_id)*/
                AND NOT EXISTS
                       (SELECT transaction_quantity
                          FROM mtl_onhand_quantities_detail
                         WHERE     inventory_item_id = pla.item_id
                               AND organization_id =
                                      plla.ship_to_organization_id
                               AND ROWNUM=1)
                AND EXISTS
                       (SELECT 1
                          FROM mtl_supply msu,
                               org_organization_definitions ood,
                               mtl_system_items_b msi,
                               po_headers_all pha1,
                               po_lines_all pla1,
                               po_line_locations_all plla1,
                               po_distributions_all pda1
                         WHERE     msu.item_id = msi.inventory_item_id
                               AND msu.to_organization_id =
                                      msi.organization_id
                               AND msu.to_organization_id =
                                      ood.organization_id
                               AND msu.SUPPLY_TYPE_CODE = 'PO'
                               AND msu.po_header_id = pha1.po_header_id
                               AND msu.po_line_id = pla1.po_line_id
                               AND msu.po_line_id = pla.po_line_id
                               AND msi.INVENTORY_ITEM_STATUS_CODE <>
                                      'Intangible'
                               AND msu.po_line_location_id =
                                      plla1.line_location_id
                               AND msu.po_distribution_id =
                                      pda1.po_distribution_id
                               AND msu.item_id = pla1.item_id
                               AND msu.item_id = pla.item_id
                               AND TRUNC (pla1.last_update_date) >
                                      TRUNC (SYSDATE) - NVL (p_days_old, 90)
                               AND (   msi.mtl_transactions_enabled_flag =
                                          'Y'
                                    OR msi.INVENTORY_ASSET_FLAG = 'Y'
                                    OR msi.COSTING_ENABLED_FLAG = 'Y')
                               AND msi.inventory_item_id =
                                      NVL (p_item_id, msi.inventory_item_id)
                               AND msi.organization_id =
                                      NVL (p_organization_id,
                                           msi.organization_id));
   -- Adding line API variables
   BEGIN
      DBMS_OUTPUT.ENABLE (100000);

      SELECT user_id
        INTO l_user_id
        FROM fnd_user
       WHERE user_name = 'XXWC_INT_SUPPLYCHAIN';

      SELECT responsibility_id, application_id
        INTO l_resp_id, l_appl_id
        FROM fnd_responsibility_vl
       WHERE responsibility_name = 'HDS Purchasing Super User - WC';

      fnd_global.apps_initialize (l_user_id, l_resp_id, l_appl_id);

      FOR I IN Cur_po_dtls
      LOOP
         -- Inserting into bkp table



         INSERT INTO xxwc.xxwc_po_headers_intang_tbl
            SELECT *
              FROM po_headers_all
             WHERE po_header_id = I.po_header_id;

         INSERT INTO xxwc.xxwc_po_intangible_lines
            SELECT *
              FROM po_lines_all
             WHERE po_line_id = I.po_line_id;

         INSERT INTO xxwc.xxwc_po_line_loc_intangibles
            SELECT *
              FROM po_line_locations_all
             WHERE line_location_id = I.line_location_id;

         COMMIT;


         -- Cancelling the po lines
         mo_global.init ('PO');
         mo_global.set_policy_context ('S', I.org_id);


         FND_FILE.PUT_LINE (
            FND_FILE.LOG,
            'Calling API For Cancelling PO line-' || I.po_line_id);

         PO_DOCUMENT_CONTROL_PUB.CONTROL_DOCUMENT (
            p_api_version        => 1.0,
            p_init_msg_list      => fnd_api.g_true,
            p_commit             => fnd_api.g_false,
            x_return_status      => v_return_status,
            p_doc_type           => v_doc_type,
            p_doc_subtype        => v_doc_subtype,
            p_doc_id             => I.po_header_id,
            p_doc_num            => NULL,
            p_release_id         => NULL,
            p_release_num        => NULL,
            p_doc_line_id        => NULL,
            p_doc_line_num       => I.LINE_NUM,
            p_doc_line_loc_id    => NULL,
            p_doc_shipment_num   => NULL,
            p_action             => v_action,
            p_action_date        => v_action_date,
            p_cancel_reason      => 'CANCEL LINE TMS 20180808-00080',
            p_cancel_reqs_flag   => 'N',
            p_print_flag         => NULL,
            p_note_to_vendor     => NULL,
            p_use_gldate         => NULL,
            p_org_id             => I.org_id);
         COMMIT;

         fnd_file.put_line (
            fnd_file.LOG,
               'The Return Status of the cancel PO line API : '
            || v_return_status);

         IF v_return_status = fnd_api.g_ret_sts_success
         THEN
            fnd_file.put_line (
               fnd_file.LOG,
                  'PO line '
               || I.LINE_NUM
               || 'Successfully Cancelled for po line id : '
               || I.po_line_id);

            UPDATE xxwc.xxwc_po_headers_intang_tbl
               SET global_attribute1 = 'Y'
             WHERE po_header_id = I.po_header_id;

            UPDATE xxwc.xxwc_po_intangible_lines
               SET global_attribute1 = 'Y'
             WHERE     po_line_id = I.po_line_id
                   AND po_header_id = I.po_header_id;

            UPDATE xxwc.xxwc_po_line_loc_intangibles
               SET global_attribute1 = 'Y'
             WHERE     po_line_id = I.po_line_id
                   AND po_header_id = I.po_header_id
                   AND line_location_id = I.line_location_id;

            COMMIT;
         ELSE
            fnd_file.put_line (
               fnd_file.LOG,
                  'Cancellation of PO Line failed for po line id : '
               || I.po_line_id);

            -- ROLLBACK;
            FOR i IN 1 .. FND_MSG_PUB.COUNT_MSG
            LOOP
               v_msg_data :=
                  FND_MSG_PUB.GET (p_msg_index => i, p_encoded => 'F');
               fnd_file.put_line (fnd_file.output, i || ') ' || v_msg_data);
            END LOOP;

            UPDATE xxwc.xxwc_po_headers_intang_tbl
               SET global_attribute1 = 'E'
             WHERE po_header_id = I.po_header_id;

            UPDATE xxwc.xxwc_po_intangible_lines
               SET global_attribute1 = 'E'
             WHERE     po_line_id = I.po_line_id
                   AND po_header_id = I.po_header_id;

            UPDATE xxwc.xxwc_po_line_loc_intangibles
               SET global_attribute1 = 'E'
             WHERE     po_line_id = I.po_line_id
                   AND po_header_id = I.po_header_id
                   AND line_location_id = I.line_location_id;

            COMMIT;
         END IF;
      ------- End -------- Purchase order cancellation

      END LOOP;                                                  -- First loop
   EXCEPTION
      WHEN OTHERS
      THEN
         FND_FILE.PUT_LINE (
            FND_FILE.LOG,
            'Error while cancelling PO line using API' || SQLERRM);
         NULL;
   END;

   -- add lines proc
   PROCEDURE add_lines_to_po (p_errbuf OUT VARCHAR2, p_retcode OUT VARCHAR2)
   IS
      l_user_id               NUMBER;
      l_resp_id               NUMBER;
      l_appl_id               NUMBER;

      v_process_code          VARCHAR2 (25);
      v_header_id             NUMBER;
      v_po_status             VARCHAR2 (240);
      v_po_line_num           NUMBER := 0;
      v_need_by_date          DATE;
      v_uom                   VARCHAR2 (25);
      v_vendor_prod_num       VARCHAR2 (25);

      v_line_cnt              NUMBER;
      l_qty                   NUMBER;
      l_agent_id              NUMBER;
      l_item_id               NUMBER;

      -- Params for FND_CONCURRENT.wait_for_request
      l_request_id            NUMBER;
      l_conc_req_phase        VARCHAR2 (80);
      l_conc_req_status       VARCHAR2 (80);
      l_conc_req_dev_phase    VARCHAR2 (80);
      l_conc_req_dev_status   VARCHAR2 (80);
      l_conc_req_message      VARCHAR2 (80);

      l_return_status         VARCHAR2 (1);
      l_count_errors          NUMBER;

      l_on_open_lines         NUMBER;
      l_on_closed_lines       NUMBER;
      l_result                NUMBER;
      l_api_errors            PO_API_ERRORS_REC_TYPE;
      l_po_num                VARCHAR2 (20);
      l_line_num              NUMBER;
      l_revision_num          NUMBER;
      l_release_num           NUMBER;
      l_shipment_num          NUMBER;
      l_org_id                NUMBER;
      l_original_qty          NUMBER;


      l_consigned_flag        VARCHAR2 (1);
      l_count_consignment     NUMBER;
      l_MessageLevel          VARCHAR2 (2);
      l_item_key              VARCHAR2 (100);

      CURSOR cur_lines_add
      IS
         SELECT pl.po_line_id, pl.po_header_id, ploc.ship_to_organization_id
           FROM xxwc.xxwc_po_intangible_lines pl,
                xxwc.xxwc_po_line_loc_intangibles ploc
          WHERE     1 = 1
                AND pl.po_line_id = ploc.po_line_id
                AND NVL(ploc.DROP_SHIP_FLAG,'N')='N'
                AND pl.GLOBAL_ATTRIBUTE1 = 'Y';
   BEGIN
      DBMS_OUTPUT.ENABLE (100000);

      SELECT user_id
        INTO l_user_id
        FROM fnd_user
       WHERE user_name = 'XXWC_INT_SUPPLYCHAIN';

      SELECT responsibility_id, application_id
        INTO l_resp_id, l_appl_id
        FROM fnd_responsibility_vl
       WHERE responsibility_name = 'HDS Purchasing Super User - WC';

      fnd_global.apps_initialize (l_user_id, l_resp_id, l_appl_id);

      mo_global.init ('PO');
      mo_global.set_policy_context ('S', 162);

      FOR I IN cur_lines_add
      LOOP
         BEGIN
            v_po_line_num := 0;

            SELECT MAX (line_num)
              INTO v_po_line_num
              FROM po_lines_all
             WHERE po_header_id = I.po_header_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               v_po_status := NULL;
         END;


         /** SELECT po.po_headers_interface_s.nextval  -- get unique header_id
            INTO v_header_id
            FROM dual;
            dbms_output.put_line('p_po_header_id '||p_po_header_id);**/
         -- Insert Header Record
         BEGIN
            INSERT INTO po.po_headers_interface (interface_header_id,
                                                 batch_id,
                                                 process_code,
                                                 action,
                                                 org_id,
                                                 document_type_code,
                                                 document_num,
                                                 vendor_id,
                                                 vendor_site_id)
               SELECT po_header_id                               --v_header_id
                                  ,
                      po_header_id                               --v_header_id
                                  ,
                      'PENDING',
                      'UPDATE',
                      org_id,
                      type_lookup_code,
                      segment1,
                      vendor_id,
                      vendor_site_id
                 FROM po_headers_all
                WHERE po_header_id = I.po_header_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               DBMS_OUTPUT.put_line ('Error in header interface ' || SQLERRM);
         END;

         DBMS_OUTPUT.put_line ('after insert to po_headers_interface');

         BEGIN
            SELECT need_by_date
              INTO v_need_by_date
              FROM xxwc.xxwc_po_line_loc_intangibles
             WHERE     po_header_id = I.po_header_id
                   AND po_line_id = I.po_line_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               v_need_by_date := NULL;
         END;


         -- Adding Lines
         v_po_line_num := v_po_line_num + 1;

         INSERT INTO po.po_lines_interface (interface_line_id,
                                            interface_header_id,
                                            process_code,
                                            action,
                                            line_num,
                                            shipment_num,
                                            line_type,
                                            item_id,
                                            item_description,
                                            unit_of_measure,
                                            quantity,
                                            unit_price,
                                            promised_date,
                                            need_by_date,
                                            vendor_product_num -- 07/16/2013 CG: TMS 20130711-00842: Added to correct issue with Consigned Items not being added correctly to PO
                                                              ,
                                            consigned_flag)
            SELECT po.po_lines_interface_s.NEXTVAL,
                   I.po_header_id,
                   'PENDING',
                   'ADD',
                   v_po_line_num,
                   1,
                   'Goods',
                   ITEM_ID,
                   ITEM_DESCRIPTION,
                   UNIT_MEAS_LOOKUP_CODE,
                   QUANTITY -- 04/25/2013 TMS 20130424-01825; CG Changed to calculate with BPA Cost first the org list price (SCWB contains that logic)
                           -- , :VENDOR_MIN_ITEMS.LIST_PRICE
                   ,
                   UNIT_PRICE,
                   v_need_by_date,
                   v_need_by_date,
                   v_vendor_prod_num -- 07/16/2013 CG: TMS 20130711-00842: Added to correct issue with Consigned Items not being added correctly to PO
                                    ,
                   NULL
              FROM xxwc.xxwc_po_intangible_lines
             WHERE po_line_id = I.po_line_id;

         SELECT QUANTITY
           INTO l_qty
           FROM xxwc.xxwc_po_intangible_lines
          WHERE po_line_id = I.po_line_id;

         DBMS_OUTPUT.put_line ('after insert to po_lines_interface');

         INSERT
           INTO po.po_distributions_interface (interface_header_id,
                                               interface_line_id,
                                               interface_distribution_id,
                                               distribution_num,
                                               quantity_ordered)
         VALUES (I.po_header_id,
                 po.po_lines_interface_s.CURRVAL,
                 po.po_distributions_interface_s.NEXTVAL,
                 1,
                 l_qty);

         DBMS_OUTPUT.put_line ('after inser to po_distributions_interface');

         COMMIT;

         SELECT item_id
           INTO l_item_id
           FROM xxwc.xxwc_po_intangible_lines
          WHERE PO_line_id = I.po_line_id;

         DBMS_OUTPUT.put_line ('after insert to l_item_id: ' || l_item_id);

         -- Get PO Number, Release Number, Revision Number, Line Number, Shipment Number and Org ID
         l_po_num := NULL;
         l_line_num := NULL;
         l_revision_num := NULL;
         l_release_num := NULL;
         l_shipment_num := NULL;
         l_org_id := NULL;
         l_original_qty := NULL;
         DBMS_OUTPUT.put_line ('before PO_PDOI_GRP.start_process');



         PO_PDOI_GRP.start_process (
            p_api_version                  => 1.0,
            p_init_msg_list                => 'T',
            p_validation_level             => 100,
            p_commit                       => 'T',
            x_return_status                => l_return_status,
            p_gather_intf_tbl_stat         => 'F',
            p_calling_module               => 'UNKNOWN', --PO_PDOI_CONSTANTS.g_CALL_MOD_CONCURRENT_PRGM,
            p_selected_batch_id            => I.po_header_id,   --v_header_id,
            p_batch_size                   => 5000, --PO_PDOI_CONSTANTS.g_DEF_BATCH_SIZE,
            p_buyer_id                     => NULL,
            p_document_type                => 'STANDARD',
            p_document_subtype             => NULL,
            p_create_items                 => 'N',
            p_create_sourcing_rules_flag   => 'N',
            p_rel_gen_method               => NULL,
            p_sourcing_level               => NULL,
            p_sourcing_inv_org_id          => NULL,
            p_approved_status              => 'APPROVED',
            p_process_code                 => 'PENDING', --PO_PDOI_CONSTANTS.g_process_code_PENDING,
            p_interface_header_id          => NULL,
            p_org_id                       => 162,
            p_ga_flag                      => NULL);

         DBMS_OUTPUT.put_line ('After PO_PDOI_GRP.start_process ');

         IF (l_return_status <> 'S')
         THEN
            FND_FILE.PUT_LINE (
               FND_FILE.LOG,
               'PO Import completed with error please review interface table...');

            UPDATE xxwc.xxwc_po_headers_intang_tbl
               SET global_attribute1 = 'E'
             WHERE po_header_id = I.po_header_id;

            UPDATE xxwc.xxwc_po_intangible_lines
               SET global_attribute1 = 'E'
             WHERE     po_line_id = I.po_line_id
                   AND po_header_id = I.po_header_id;

            UPDATE xxwc.xxwc_po_line_loc_intangibles
               SET global_attribute1 = 'E'
             WHERE     po_line_id = I.po_line_id
                   AND po_header_id = I.po_header_id
                   AND ship_to_organization_id = I.ship_to_organization_id;

            COMMIT;
         ELSE
            -- 07/16/2013 CG: TMS 20130711-00842: Added to correct issue with Consigned Items not being added correctly to PO
            l_count_consignment := 0;


            SELECT agent_id
              INTO l_agent_id
              FROM xxwc.xxwc_po_headers_intang_tbl
             WHERE po_header_id = I.po_header_id;

            SELECT I.po_header_id || '-' || TO_CHAR (po_wf_itemkey_s.NEXTVAL)
              INTO l_item_key
              FROM DUAL;

            po_reqapproval_init1.start_wf_process (
               ItemType                 => 'POAPPRV',
               ItemKey                  => l_item_key,
               WorkflowProcess          => 'POAPPRV_TOP',
               ActionOriginatedFrom     => 'PO_FORM',
               DocumentID               => I.po_header_id      -- po_header_id
                                                         ,
               DocumentNumber           => l_po_num   -- Purchase Order Number
                                                   ,
               PreparerID               => l_agent_id     -- Buyer/Preparer_id
                                                     ,
               DocumentTypeCode         => 'PO',
               DocumentSubtype          => 'STANDARD',
               SubmitterAction          => 'APPROVE',
               forwardToID              => NULL,
               forwardFromID            => NULL,
               DefaultApprovalPathID    => NULL,
               Note                     => NULL,
               PrintFlag                => 'N',
               FaxFlag                  => 'N',
               FaxNumber                => NULL,
               EmailFlag                => 'N',
               EmailAddress             => NULL,
               CreateSourcingRule       => 'N',
               ReleaseGenMethod         => 'N',
               UpdateSourcingRule       => 'N',
               MassUpdateReleases       => 'N',
               RetroactivePriceChange   => 'N',
               OrgAssignChange          => 'N',
               CommunicatePriceChange   => 'N',
               p_Background_Flag        => 'N',
               p_Initiator              => NULL,
               p_xml_flag               => NULL,
               FpdsngFlag               => 'N',
               p_source_type_code       => NULL);

            DBMS_OUTPUT.put_line (
               'After  po_reqapproval_init1.start_wf_process ');
         END IF;

         UPDATE xxwc.xxwc_po_headers_intang_tbl
            SET global_attribute1 = 'P'
          WHERE po_header_id = I.po_header_id;

         UPDATE xxwc.xxwc_po_intangible_lines
            SET global_attribute1 = 'P'
          WHERE po_line_id = I.po_line_id AND po_header_id = I.po_header_id;

         UPDATE xxwc.xxwc_po_line_loc_intangibles
            SET global_attribute1 = 'P'
          WHERE     po_line_id = I.po_line_id
                AND po_header_id = I.po_header_id
                AND ship_to_organization_id = I.ship_to_organization_id;

         COMMIT;
      END LOOP;
   EXCEPTION
      WHEN OTHERS
      THEN
         NULL;
   END;
END XXWC_INTANGBLE_ITEM_CORRECTION;
/