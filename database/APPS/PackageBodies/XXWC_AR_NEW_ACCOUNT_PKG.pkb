CREATE OR REPLACE PACKAGE BODY apps.xxwc_ar_new_account_pkg
AS
   /*****************************************************************************************
     $Header xxwc_ar_new_account_pkg $
     Module Name: xxwc_ar_new_account_pkg.pkb

     PURPOSE:   This package is used in the new account creation extension

     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.0        03/21/2013  Shankar Hariharan       Initial Version
     2.0        1/7/2014    Ram Talluri             TMS #20131206-00108 
     3.0        09/23/2014  Pattabhi Avula          TMS #20141001-00031
     4.0        01/28/2015  Maharajan Shunmugam     TMS# 20140516-00042 
     5.0        03/03/2015  Maharajan Shunmugam     TMS# 20140516-00042 Ap role break fix
     6.0        09/22/2015  Maharajan Shunmugam     TMS# 20150123-00197 AR - Customer Flag as indicator to auto apply credits
   ******************************************************************************************/
   
   l_org_id            NUMBER:=mo_global.get_current_org_id;   
   
   FUNCTION return_phone_number (i_party_id IN NUMBER)
      RETURN VARCHAR
   IS
      l_return_num   VARCHAR2 (30);
   BEGIN
      SELECT area_code || phone_number
        INTO l_return_num
        FROM apps.ar_phones_v
       WHERE     owner_Table_name = 'HZ_PARTIES'
             AND owner_table_id = i_party_id
             AND phone_type = 'GEN'
             AND primary_flag = 'Y';

      RETURN (l_return_num);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            SELECT area_code || phone_number
              INTO l_return_num
              FROM apps.ar_phones_v
             WHERE     owner_Table_name = 'HZ_PARTIES'
                   AND owner_table_id = i_party_id
                   AND phone_type = 'GEN'
                   AND primary_flag = 'N'
                   AND ROWNUM = 1;

            RETURN (l_return_num);
         EXCEPTION
            WHEN OTHERS
            THEN
               BEGIN
                  SELECT area_code || phone_number
                    INTO l_return_num
                    FROM apps.ar_phones_v
                   WHERE     owner_Table_name = 'HZ_PARTIES'
                         AND owner_table_id = i_party_id
                         AND primary_flag = 'N'
                         AND ROWNUM = 1;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     l_return_num := NULL;
                     RETURN (l_return_num);
               END;
         END;

         RETURN (l_return_num);
   END return_phone_number;


   PROCEDURE create_account (i_record_id       IN     NUMBER
                           , o_return_status      OUT VARCHAR2
                           , o_return_msg         OUT VARCHAR2)
   IS
      CURSOR c1
      IS
         SELECT *
           FROM apps.xxwc_ar_new_acct_tbl
          WHERE record_id = i_record_id;

      ostatus                          VARCHAR2 (1);
      omsgcount                        NUMBER;
      omsgdata                         VARCHAR2 (2000);
      l_msg_index_out                  NUMBER;
      l_msg_Data                       VARCHAR2 (2000);
      l_count                          NUMBER;
      l_party_id                       NUMBER;
      l_phone_exist                    VARCHAR2 (1);
      -- party variables
      porganizationrec                 hz_party_v2pub.organization_rec_type;
      opartyid                         hz_parties.party_id%TYPE;
      opartynumber                     hz_parties.party_number%TYPE;
      oprofileid                       NUMBER;
      osprofile_id                     NUMBER;
      -- party account variables
      pcustaccountrec                  hz_cust_account_v2pub.cust_account_rec_type;
      pprofilerec                      hz_customer_profile_v2pub.customer_profile_rec_type;
      ocustaccountid                   hz_cust_accounts.cust_account_id%TYPE;
      ocustaccountno                   hz_cust_accounts.account_number%TYPE;
      --location variable
      plocationrec                     hz_location_v2pub.location_rec_type;
      olocationid                      hz_locations.location_id%TYPE;
      --site variables
      ppartysiterec                    hz_party_site_v2pub.party_site_rec_type;
      opartysiteid                     hz_party_sites.party_site_id%TYPE;
      opartysiteno                     hz_party_sites.party_site_number%TYPE;
      --site account avriables
      pcustacctsiterec                 hz_cust_account_site_v2pub.cust_acct_site_rec_type;
      ocustacctsiteid                  hz_cust_acct_sites.cust_acct_site_id%TYPE;
      --site use variables
      pcustacctsiteuserec              hz_cust_account_site_v2pub.cust_site_use_rec_type;
      pcustomerprofile                 hz_customer_profile_v2pub.customer_profile_rec_type;
      ocustacctsiteuseid               NUMBER;

      --contact variables
      pcontactpointrec                 hz_contact_point_v2pub.contact_point_rec_type;
      pphonerec                        hz_contact_point_v2pub.phone_rec_type;
 --Added for ver 4.0 <<Start
      pemailrec                        hz_contact_point_v2pub.email_rec_type;            
      ptelexrec                        hz_contact_point_v2pub.TELEX_REC_TYPE;
      pwebrec                          hz_contact_point_v2pub.WEB_REC_TYPE;
      pedirec                          hz_contact_point_v2pub.EDI_REC_TYPE;
      ocontactpointid                  NUMBER;
--<<End
      -- amount profile variables
      pcustproamtrec                   hz_customer_profile_v2pub.cust_profile_amt_rec_type;
      ocustacctprofileamtid            NUMBER;
      ---------

      p_object_version_number          NUMBER;
      l_api_name                       VARCHAR2 (30) := 'ONT_UI_ADD_CUSTOMER'; --'XXWC_NEW_ACCOUNT';
      lv_primary_ship_to_flag          VARCHAR2 (2);

      -- Contact variables
      p_create_person_rec              hz_party_v2pub.person_rec_type;
      x_contact_party_id               NUMBER;
      x_contact_party_number           VARCHAR2 (2000);
      x_contact_profile_id             NUMBER;
      -- contact org
      p_org_contact_rec                hz_party_contact_v2pub.org_contact_rec_type;
      x_org_contact_id                 NUMBER;
      x_party_rel_id                   NUMBER;
      x_rel_party_id                   NUMBER;
      x_rel_party_number               VARCHAR2 (2000);
      -- contact acct
      p_cr_cust_acc_role_rec           hz_cust_account_role_v2pub.cust_account_role_rec_type;
      x_cust_account_role_id           NUMBER;
      -- Role resp
      p_ROLE_RESPONSIBILITY_REC_TYPE   HZ_CUST_ACCOUNT_ROLE_V2PUB.role_responsibility_rec_type;
      orespid                          NUMBER;
      
      --ADDED BY RAM TALLURI 1/7/2014 FORTMS 20131206-00108
      l_res_category                   VARCHAR2(20):=NULL;   
      
	  --ADDED BY Pattabhi 25/9/2014 for TMS#20141001-00031 OU Testing
      lv_currency_code                  VARCHAR2 (10);	 
      lv_country_code                   VARCHAR2 (5);		 
 --Added below for version 4.0
     l_acc_obj_version_number         NUMBER;



      PROCEDURE return_msg (in_msg_count     IN     NUMBER
                          , in_msg_data      IN     VARCHAR2
                          , out_return_msg      OUT VARCHAR2)
      IS
      BEGIN
         IF in_msg_count > 1
         THEN
            FOR I IN 1 .. in_msg_count
            LOOP
               Oe_Msg_Pub.get (p_msg_index       => i
                             , p_encoded         => Fnd_Api.G_FALSE
                             , p_data            => l_msg_data
                             , p_msg_index_out   => l_msg_index_out);
               out_return_msg :=
                  out_return_msg || ':' || SUBSTR (l_msg_data, 1, 200);
            END LOOP;
         ELSE
            out_return_msg := in_msg_data;
         END IF;
      END;
   BEGIN
     ----Starts -- 25/09/2014 Added by pattabhi for TMS#20141001-00031 OU Testing 
     --====================================================================
         -- Fetching the Curency code
     --====================================================================
	 BEGIN
	   SELECT  gsob.currency_code 
	   INTO    lv_currency_code
       FROM    hr_operating_units hou
              ,gl_sets_of_books gsob
       WHERE   1 = 1
       AND     gsob.set_of_books_id = hou.set_of_books_id
       AND     hou.organization_id=l_org_id;
	 EXCEPTION
	   WHEN OTHERS
               THEN
                  lv_currency_code := NULL;
     END;	 
	 --====================================================================
         -- Fetching the Country code
     --====================================================================
	  BEGIN
	   SELECT  SUBSTR(gsob.currency_code,1,2) 
	   INTO    lv_country_code
       FROM    hr_operating_units hou
              ,gl_sets_of_books gsob
       WHERE   1 = 1
       AND     gsob.set_of_books_id = hou.set_of_books_id
       AND     hou.organization_id=l_org_id;
	 EXCEPTION
	   WHEN OTHERS
               THEN
                  lv_country_code := NULL;
     END;	 
	 
	 ----End's here -- 25/09/2014 Added by pattabhi for TMS#20141001-00031 OU Testing 
      FOR c1_rec IN c1
      LOOP
         IF c1_rec.process_flag = 'Y'
         THEN
            o_return_msg := 'Record already processed';
            o_return_status := 'E';
            RETURN;
         END IF;

         SAVEPOINT customer_record;
         l_party_id := c1_rec.party_id;

         --====================================================================
         -- Create party if no party id passed
         --====================================================================
         IF c1_rec.party_id IS NULL
         THEN
            opartyid := NULL;
            porganizationrec := NULL;
            --initialize the values
            ostatus := NULL;
            omsgcount := NULL;
            omsgdata := NULL;
            -- create the party organization record
            porganizationrec.organization_name := c1_rec.customer_name;
            porganizationrec.created_by_module := l_api_name;
            hz_party_v2pub.create_organization (
               p_init_msg_list      => 'T'
             , p_organization_rec   => porganizationrec
             , x_return_status      => ostatus
             , x_msg_count          => omsgcount
             , x_msg_data           => omsgdata
             , x_party_id           => opartyid
             , x_party_number       => opartynumber
             , x_profile_id         => oprofileid);

            IF ostatus <> 'S'
            THEN
               return_msg (oMsgCount, omsgdata, o_return_msg);
               o_return_msg := 'Party Creation: ' || o_return_msg;
               o_return_status := ostatus;
               ROLLBACK TO customer_record;
               RETURN;
            END IF;

            l_party_id := opartyid;
         END IF;                                                --Party record

         --====================================================================
         -- Create telephone record for the party
         --====================================================================
         IF c1_rec.telephone_number IS NOT NULL
         THEN
            BEGIN
               SELECT 'Y'
                 INTO l_phone_exist
                 FROM apps.ar_phones_v
                WHERE     owner_table_name = 'HZ_PARTIES'
                      AND owner_table_id = l_party_id
                      AND phone_type = 'GEN'
                      AND area_code || phone_number = c1_rec.telephone_number
                      AND ROWNUM = 1;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  l_phone_exist := 'N';
               WHEN OTHERS
               THEN
                  l_phone_exist := 'N';
            END;

            IF l_phone_exist = 'N'
            THEN
               pcontactpointrec := NULL;
               pcontactpointrec.contact_point_type := 'PHONE';
               pcontactpointrec.owner_table_name := 'HZ_PARTIES';
               pcontactpointrec.owner_table_id := l_party_id;
               pcontactpointrec.primary_flag := 'Y';
               pcontactpointrec.contact_point_purpose := 'BUSINESS';
               pphonerec.phone_country_code := NULL;
               pphonerec.phone_area_code :=
                  SUBSTR (c1_rec.telephone_number, 1, 3);
               pphonerec.phone_number := SUBSTR (c1_rec.telephone_number, 4);
               pphonerec.phone_extension := NULL;
               pphonerec.phone_line_type := 'GEN';
               pcontactpointrec.created_by_module := l_api_name;

               hz_contact_point_v2pub.create_contact_point (
                  p_init_msg_list       => 'T'
                , p_contact_point_rec   => pcontactpointrec
                , p_phone_rec           => pphonerec
                , x_contact_point_id    => ocontactpointid
                , x_return_status       => ostatus
                , x_msg_count           => omsgcount
                , x_msg_data            => omsgdata);

               IF ostatus <> 'S'
               THEN
                  return_msg (oMsgCount, omsgdata, o_return_msg);
                  o_return_msg := 'Phone Creation: ' || o_return_msg;
                  o_return_status := ostatus;
                  ROLLBACK TO customer_record;
                  RETURN;
               END IF;
            END IF;
         END IF;                                            --Telephone record

         --====================================================================
         -- Create fax record for the party
         --====================================================================
         IF c1_rec.fax_number IS NOT NULL
         THEN
            BEGIN
               SELECT 'Y'
                 INTO l_phone_exist
                 FROM apps.ar_phones_v
                WHERE     owner_table_name = 'HZ_PARTIES'
                      AND owner_table_id = l_party_id
                      AND phone_type = 'FAX'
                      AND area_code || phone_number = c1_rec.fax_number
                      AND ROWNUM = 1;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  l_phone_exist := 'N';
               WHEN OTHERS
               THEN
                  l_phone_exist := 'N';
            END;

            IF l_phone_exist = 'N'
            THEN
               pcontactpointrec := NULL;
               pcontactpointrec.contact_point_type := 'PHONE';
               pcontactpointrec.owner_table_name := 'HZ_PARTIES';
               pcontactpointrec.owner_table_id := l_party_id;
               pcontactpointrec.primary_flag := 'N';
               pcontactpointrec.contact_point_purpose := 'BUSINESS';
               pphonerec.phone_country_code := NULL;
               pphonerec.phone_area_code := SUBSTR (c1_rec.fax_number, 1, 3);
               pphonerec.phone_number := SUBSTR (c1_rec.fax_number, 4);
               pphonerec.phone_extension := NULL;
               pphonerec.phone_line_type := 'FAX';
               pcontactpointrec.created_by_module := l_api_name;

               hz_contact_point_v2pub.create_contact_point (
                  p_init_msg_list       => 'T'
                , p_contact_point_rec   => pcontactpointrec
                , p_phone_rec           => pphonerec
                , x_contact_point_id    => ocontactpointid
                , x_return_status       => ostatus
                , x_msg_count           => omsgcount
                , x_msg_data            => omsgdata);

               IF ostatus <> 'S'
               THEN
                  return_msg (oMsgCount, omsgdata, o_return_msg);
                  o_return_msg := 'Fax Creation: ' || o_return_msg;
                  o_return_status := ostatus;
                  ROLLBACK TO customer_record;
                  RETURN;
               END IF;
            END IF;
         END IF;                                                  --fax record

 	 --====================================================================
         -- Create email record for the party                                          --Added for ver 4.0
         --====================================================================
         IF c1_rec.email_address IS NOT NULL
         THEN
            BEGIN
               SELECT 'Y'
                 INTO l_phone_exist
                 FROM apps.ar_phones_v
                WHERE     owner_table_name = 'HZ_PARTIES'
                      AND owner_table_id = l_party_id
                      AND phone_type = 'EMAIL'
                      AND UPPER(EMAIL_ADDRESS) = UPPER(c1_rec.email_address)
                      AND ROWNUM = 1;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  l_phone_exist := 'N';
               WHEN OTHERS
               THEN
                  l_phone_exist := 'N';
            END;

            IF l_phone_exist = 'N'
            THEN
               pcontactpointrec := NULL;
               pemailrec.email_address := c1_rec.email_address;
               pemailrec.email_format       := 'MAILHTML';
               pcontactpointrec.contact_point_type := 'EMAIL';
               pcontactpointrec.owner_table_name := 'HZ_PARTIES';
               pcontactpointrec.owner_table_id := l_party_id;
               pcontactpointrec.primary_flag := 'Y';
               pcontactpointrec.contact_point_purpose := 'BUSINESS';
               pcontactpointrec.created_by_module := l_api_name;

               hz_contact_point_v2pub.create_contact_point (
                  p_init_msg_list       => 'T'
                , p_contact_point_rec   => pcontactpointrec
                , p_email_rec           => pemailrec
                , x_contact_point_id    => ocontactpointid
                , x_return_status       => ostatus
                , x_msg_count           => omsgcount
                , x_msg_data            => omsgdata);

               IF ostatus <> 'S'
               THEN
                  return_msg (oMsgCount, omsgdata, o_return_msg);
                  o_return_msg := 'Fax Creation: ' || o_return_msg;
                  o_return_status := ostatus;
                  ROLLBACK TO customer_record;
                  RETURN;
               END IF;
            END IF;
         END IF;                                                  

         --====================================================================
         -- Create customer account
         --2.0        1/7/2014    Ram Talluri             TMS #20131206-00108 
         --====================================================================
         pcustproamtrec := NULL;
         pcustaccountrec := NULL;
         pprofilerec := NULL;
         lv_primary_ship_to_flag := NULL;


         porganizationrec.party_rec.party_id := l_party_id;
         pcustaccountrec.account_name := c1_rec.customer_name;
         pcustaccountrec.status := 'A';                     --cust_acc.status;
         pcustaccountrec.tax_code := NULL;
         pcustaccountrec.attribute_category := 'No';                  --'Yes';

         SELECT apps.hz_cust_accounts_s.NEXTVAL
           INTO pcustaccountrec.cust_account_id
           FROM DUAL;

         pcustaccountrec.orig_system_reference :=
            pcustaccountrec.cust_account_id;
         --customer source
         pcustaccountrec.attribute4 := c1_rec.customer_source;
         pcustaccountrec.attribute6 := c1_rec.prism_account_number;
         --account date established
         --pcustaccountrec.account_established_date := c1_rec.customer_since; --Commented for ver 4.0
         pcustaccountrec.account_established_date := TRUNC(sysdate);
         pcustaccountrec.created_by_module := l_api_name;
         pcustaccountrec.customer_class_code := c1_rec.classification;
         pcustaccountrec.status := 'A';
         pcustaccountrec.customer_type := 'R';

         --Added for ver 4.0
         pcustaccountrec.attribute9 :=  c1_rec.predom_trade;
         
  --DFF mandatory notes added for ver 4.0 <<Start
         pcustaccountrec.attribute_category := c1_rec.attribute_category;
         pcustaccountrec.attribute17 := c1_rec.attribute1;
	 pcustaccountrec.attribute18 := c1_rec.attribute2;
	 pcustaccountrec.attribute19 := c1_rec.attribute3;
	 pcustaccountrec.attribute20 := c1_rec.attribute4;
	 pcustaccountrec.attribute16 := c1_rec.attribute5;
--<<End

         pcustaccountrec.attribute11 := 'Y';                     --Added for ver# 6.0

         SELECT COUNT (1)
           INTO l_count
           FROM apps.xxwc_ar_new_acct_authbuyer_tbl
          WHERE record_id = c1_rec.record_id
            AND role = 'Authorized Buyer';                        --Added for ver# 5.0

         IF NVL (l_count, 0) > 0
         THEN
            pcustaccountrec.attribute7 := 'Y';
         ELSE
            pcustaccountrec.attribute7 := 'N';
         END IF;

         pprofilerec.profile_class_id := c1_rec.profile_class_id;
         pprofilerec.credit_checking := 'Y';
         pprofilerec.attribute3 := SUBSTR (c1_rec.statement_by_job, 1, 1);
         pprofilerec.collector_id := c1_rec.collector_id;
         pprofilerec.credit_analyst_id := c1_rec.credit_manager_id;
         pprofilerec.credit_classification := c1_rec.credit_classification;
         pprofilerec.attribute2 := c1_rec.inv_remit_to;
         pprofilerec.account_status := c1_rec.account_status;
         pprofilerec.tolerance := 0;
         
         --Start- below block added by Ram Talluri 1/7/2014 for TMS TMS 20131206-00108
--23/09/2014 replaced the table "jtf_rs_salesreps" to"jtf_rs_salesreps_mo_v" by pattabhi for TMS#20141001-00031 OU Testing  
        BEGIN       
        SELECT jrre.category
          INTO l_res_category
        FROM apps.jtf_rs_salesreps_mo_v jrs, 
                  jtf_rs_resource_extns_tl jrre 
          WHERE 1=1
            AND jrs.resource_id=jrre.resource_id
            AND jrs.salesrep_id=c1_rec.salesrep_id
           -- AND jrs.org_id=162     -- 23/09/2014 Commented by pattabhi for TMS#20141001-00031 OU Testing 
            AND jrs.STATUS='A'
            AND ROWNUM=1;
            
         IF l_res_category='OTHER' AND c1_rec.salesrep_id NOT IN (100000617,100000650)
         THEN
            pcustaccountrec.sales_channel_code:='BQU_UNKNOWN';
         ELSIF l_res_category='OTHER' AND c1_rec.salesrep_id IN (100000617,100000650)
         THEN
            pcustaccountrec.sales_channel_code:='ACCT_MGR';
         ELSIF l_res_category='EMPLOYEE' 
         THEN 
            pcustaccountrec.sales_channel_code:='ACCT_MGR';
         ELSE
            pcustaccountrec.sales_channel_code:=NULL;
         END IF;
         
         EXCEPTION
            WHEN OTHERS 
            THEN pcustaccountrec.sales_channel_code:=NULL;
         END;
         
         --End-by Ram Talluri 1/7/2014 for TMS TMS 20131206-00108

         hz_cust_account_v2pub.create_cust_account (
            p_init_msg_list          => 'T'
          , p_cust_account_rec       => pcustaccountrec
          ,                                         -- customer account record
           p_organization_rec        => porganizationrec
          ,                                       -- party organization record
           p_customer_profile_rec    => pprofilerec
          , p_create_profile_amt     => fnd_api.g_true
          ,                                                   --fnd_api.g_true
           x_cust_account_id         => ocustaccountid
          , x_account_number         => ocustaccountno
          , x_party_id               => opartyid
          , x_party_number           => opartynumber
          , x_profile_id             => oprofileid
          , x_return_status          => ostatus
          , x_msg_count              => omsgcount
          , x_msg_data               => omsgdata);

         IF ostatus <> 'S'
         THEN
            return_msg (oMsgCount, omsgdata, o_return_msg);
            o_return_msg := 'Account Creation: ' || o_return_msg;
            o_return_status := ostatus;
            ROLLBACK TO customer_record;
            RETURN;
         END IF;


         IF oprofileid IS NULL
         THEN
            BEGIN
               SELECT cust_account_profile_id
                 INTO oprofileid
                 FROM apps.hz_customer_profiles
                WHERE cust_account_id = ocustaccountid;
            EXCEPTION
               WHEN OTHERS
               THEN
                  NULL;
            END;
         END IF;

         --====================================================================
         --Update amount profile at Account Level
         --====================================================================
         pcustproamtrec := NULL;

         IF oprofileid IS NOT NULL
         THEN
            BEGIN
               SELECT cust_acct_profile_amt_id, object_version_number
                 INTO ocustacctprofileamtid, p_object_version_number
                 FROM apps.hz_cust_profile_amts
                WHERE cust_account_profile_id = oprofileid
                      AND currency_code =lv_currency_code;   -- 'USD';  -- 25/09/2014 Added by pattabhi for TMS#20141001-00031 OU Testing 
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  ocustacctprofileamtid := NULL;
                  p_object_version_number := NULL;
            END;

            IF ocustacctprofileamtid IS NOT NULL
            THEN
               --Update amount profile
               pcustproamtrec.cust_acct_profile_amt_id :=
                  ocustacctprofileamtid;
               --pcustproamtrec.trx_credit_limit := c1_rec.yard_credit_limit;
               pcustproamtrec.overall_credit_limit := 0; --c1_rec.yard_credit_limit;
               pcustproamtrec.created_by_module := l_api_name;
               hz_customer_profile_v2pub.update_cust_profile_amt (
                  p_init_msg_list           => 'T'
                , p_cust_profile_amt_rec    => pcustproamtrec
                , p_object_version_number   => p_object_version_number
                , x_return_status           => ostatus
                , x_msg_count               => omsgcount
                , x_msg_data                => omsgdata);
            ELSE
               --Create Profile Amount
               pcustproamtrec.cust_account_profile_id := oprofileid; -- customer profile id from above api
               pcustproamtrec.cust_account_id := ocustaccountid;
               pcustproamtrec.currency_code := lv_currency_code;  -- 'USD';  -- 25/09/2014 Added by pattabhi for TMS#20141001-00031 OU Testing 
               --pcustproamtrec.trx_credit_limit := c1_rec.yard_credit_limit;
               pcustproamtrec.overall_credit_limit := 0; --c1_rec.yard_credit_limit;
               pcustproamtrec.created_by_module := l_api_name;
               hz_customer_profile_v2pub.create_cust_profile_amt (
                  p_init_msg_list              => 'T'
                , p_check_foreign_key          => 'T'
                , p_cust_profile_amt_rec       => pcustproamtrec
                , x_cust_acct_profile_amt_id   => ocustacctprofileamtid
                , x_return_status              => ostatus
                , x_msg_count                  => omsgcount
                , x_msg_data                   => omsgdata);
            END IF;

            IF ostatus <> 'S'
            THEN
               return_msg (oMsgCount, omsgdata, o_return_msg);
               o_return_msg := 'Profile Amount Creation: ' || o_return_msg;
               o_return_status := ostatus;
               ROLLBACK TO customer_record;
               RETURN;
            END IF;

            --====================================================================
            -- Create usage rules
            --====================================================================
            hz_credit_usages_cascade_pkg.cascade_credit_usage_rules (
               p_cust_acct_profile_amt_id   => ocustacctprofileamtid
             , p_cust_profile_id            => osprofile_id
             , p_profile_class_amt_id       => NULL
             , p_profile_class_id           => NULL
             , x_return_status              => ostatus
             , x_msg_count                  => omsgcount
             , x_msg_data                   => omsgdata);

            IF ostatus <> 'S'
            THEN
               return_msg (oMsgCount, omsgdata, o_return_msg);
               o_return_msg := 'Acc usage rule: ' || o_return_msg;
               o_return_status := ostatus;
               ROLLBACK TO customer_record;
               RETURN;
            END IF;
         END IF;

         --====================================================================
         -- create location for the primary bill to 
         --====================================================================
         plocationrec := NULL;
         ppartysiterec := NULL;
         pcustacctsiterec := NULL;
         pcustacctsiteuserec := NULL;
         pcustomerprofile := NULL;
         ostatus := NULL;
         omsgcount := NULL;
         omsgdata := NULL;


         l_count := 0;
         olocationid := NULL;
         ostatus := NULL;

       --  plocationrec.country := lv_country_code;  --  'US';  -- 25/09/2014 Added by pattabhi for TMS#20141001-00031 OU Testing 
         plocationrec.country     := c1_rec.bill_to_country;      --Added for ver 4.0
         plocationrec.postal_code := c1_rec.bill_to_postal_code;
         plocationrec.address1 := c1_rec.bill_to_address1;
         plocationrec.address2 := c1_rec.bill_to_address2;
         plocationrec.address3 := c1_rec.bill_to_address3;
         plocationrec.address4 := c1_rec.bill_to_address4;
         plocationrec.state := c1_rec.bill_to_state;
         plocationrec.city := c1_rec.bill_to_city;
         plocationrec.county := c1_rec.bill_to_county;
         plocationrec.created_by_module := l_api_name;
         plocationrec.sales_tax_geocode := c1_rec.bill_to_geocode;

         hz_location_v2pub.create_location (p_init_msg_list   => 'T'
                                          , p_location_rec    => plocationrec
                                          , x_location_id     => olocationid
                                          , x_return_status   => ostatus
                                          , x_msg_count       => omsgcount
                                          , x_msg_data        => omsgdata);

         IF ostatus <> 'S'
         THEN
            return_msg (oMsgCount, omsgdata, o_return_msg);
            o_return_msg := 'Bill To Loc: ' || o_return_msg;
            o_return_status := ostatus;
            ROLLBACK TO customer_record;
            RETURN;
         END IF;

         --====================================================================
         -- Create Party Site for the primary bill to
         --====================================================================
         ostatus := NULL;
         omsgcount := NULL;
         omsgdata := NULL;
         opartysiteid := NULL;
         opartysiteno := NULL;
         osprofile_id := NULL;
         ppartysiterec.created_by_module := l_api_name;

         -- create a party site now
         ppartysiterec.party_id := l_party_id;
         ppartysiterec.location_id := olocationid;
         ppartysiterec.identifying_address_flag := 'Y';

         hz_party_site_v2pub.create_party_site (
            p_init_msg_list       => 'T'
          , p_party_site_rec      => ppartysiterec
          , x_party_site_id       => opartysiteid
          , x_party_site_number   => opartysiteno
          , x_return_status       => ostatus
          , x_msg_count           => omsgcount
          , x_msg_data            => omsgdata);

         IF ostatus <> 'S'
         THEN
            return_msg (oMsgCount, omsgdata, o_return_msg);
            o_return_msg := 'Bill To PS: ' || o_return_msg;
            o_return_status := ostatus;
            ROLLBACK TO customer_record;
            RETURN;
         END IF;

         --====================================================================
         -- Create cust acct site 
         --====================================================================
         ostatus := NULL;
         omsgcount := NULL;
         omsgdata := NULL;
         ocustacctsiteid := NULL;
         pcustacctsiterec.cust_account_id := ocustaccountid;
         pcustacctsiterec.party_site_id := opartysiteid;
         pcustacctsiterec.created_by_module := l_api_name;
         pcustacctsiterec.org_id := l_org_id; -- 162;  -- 23/09/2014 Added by pattabhi for TMS#20141001-00031 OU Testing 
         pcustacctsiterec.attribute3 := SUBSTR (c1_rec.mand_po_number, 1, 1);
         pcustacctsiterec.attribute17 := c1_rec.prism_account_number;
--Added below for ver 4.0
         BEGIN
	   SELECT CASE WHEN customer_class_code = 'COMMERCIAL' THEN 'PUBLIC'
           	       WHEN customer_class_code = 'GOVERNMENT' THEN 'PRIVATE'
            	       WHEN customer_class_code = 'NON-CONTRACTOR/OTHER' THEN 'PUBLIC'
            	       WHEN customer_class_code = 'RESELLER' THEN 'PUBLIC'
            	       WHEN customer_class_code = 'RESIDENTIAL_CONTRACTOR' THEN 'RES'
                       WHEN customer_class_code = 'UNKNOWN' THEN 'PUBLIC'
            	  ELSE NULL
            	  END INTO pcustacctsiterec.customer_category_code
	   FROM hz_cust_accounts
	   WHERE cust_account_id = ocustaccountid;
         EXCEPTION
         WHEN OTHERS THEN
            pcustacctsiterec.customer_category_code := null;
         END;


         hz_cust_account_site_v2pub.create_cust_acct_site (
            p_init_msg_list        => 'T'
          , p_cust_acct_site_rec   => pcustacctsiterec
          , x_cust_acct_site_id    => ocustacctsiteid
          , x_return_status        => ostatus
          , x_msg_count            => omsgcount
          , x_msg_data             => omsgdata);

         IF ostatus <> 'S'
         THEN
            return_msg (oMsgCount, omsgdata, o_return_msg);
            o_return_msg := 'Bill To Acct Site: ' || o_return_msg;
            o_return_status := ostatus;
            ROLLBACK TO customer_record;
            RETURN;
         END IF;

         --====================================================================
         -- Create site use for primary bill to
         --====================================================================
         pcustacctsiteuserec := NULL;
         ostatus := NULL;
         omsgcount := NULL;
         omsgdata := NULL;
         ocustacctsiteuseid := NULL;

         pcustacctsiteuserec.site_use_code := 'BILL_TO';
         pcustacctsiteuserec.cust_acct_site_id := ocustacctsiteid;
         pcustacctsiteuserec.attribute1 := 'MSTR';
         pcustacctsiteuserec.primary_flag := 'Y';
         pcustacctsiteuserec.location := c1_rec.customer_name;
         pcustacctsiteuserec.created_by_module := l_api_name;
         pcustacctsiteuserec.primary_salesrep_id := c1_rec.salesrep_id;

         pcustomerprofile.profile_class_id := c1_rec.profile_class_id;
         pcustomerprofile.credit_checking := 'Y';
         pcustomerprofile.attribute3 := SUBSTR (c1_rec.statement_by_job, 1, 1);
         pcustomerprofile.collector_id := c1_rec.collector_id;
         pcustomerprofile.credit_analyst_id := c1_rec.credit_manager_id;
         pcustomerprofile.credit_classification :=
            c1_rec.credit_classification;
         pcustomerprofile.attribute2 := c1_rec.inv_remit_to;
         pcustomerprofile.account_status := c1_rec.account_status;
         pcustomerprofile.tolerance := 0;

         hz_cust_account_site_v2pub.create_cust_site_use (
            p_init_msg_list          => 'T'
          , p_cust_site_use_rec      => pcustacctsiteuserec
          , p_customer_profile_rec   => pcustomerprofile
          , p_create_profile         => 'T'
          , p_create_profile_amt     => 'T'
          , x_site_use_id            => ocustacctsiteuseid
          , x_return_status          => ostatus
          , x_msg_count              => omsgcount
          , x_msg_data               => omsgdata);


         IF ostatus <> 'S'
         THEN
            return_msg (oMsgCount, omsgdata, o_return_msg);
            o_return_msg := 'Bill To SU: ' || o_return_msg;
            o_return_status := ostatus;
            ROLLBACK TO customer_record;
            RETURN;
         END IF;

         --====================================================================
         -- create site profile amount
         --====================================================================
         pcustproamtrec := NULL;

         BEGIN
            SELECT cust_account_profile_id
              INTO osprofile_id
              FROM apps.hz_customer_profiles
             WHERE cust_account_id = ocustaccountid
                   AND site_use_id = ocustacctsiteuseid;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               osprofile_id := NULL;
         END;


         IF osprofile_id IS NOT NULL
         THEN
            BEGIN
               SELECT cust_acct_profile_amt_id, object_version_number
                 INTO ocustacctprofileamtid, p_object_version_number
                 FROM apps.hz_cust_profile_amts
                WHERE cust_account_profile_id = osprofile_id
                      AND currency_code = lv_currency_code; --'USD';  -- 23/09/2014 Added by pattabhi for TMS#20141001-00031 OU Testing 
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  ocustacctprofileamtid := NULL;
                  p_object_version_number := NULL;
            END;

            IF ocustacctprofileamtid IS NOT NULL
            THEN
               --Update amount profile
               pcustproamtrec.cust_acct_profile_amt_id :=
                  ocustacctprofileamtid;
               --pcustproamtrec.trx_credit_limit := c1_rec.yard_credit_limit;
               pcustproamtrec.overall_credit_limit := 0; --c1_rec.yard_credit_limit;
               pcustproamtrec.created_by_module := l_api_name;
               hz_customer_profile_v2pub.update_cust_profile_amt (
                  p_init_msg_list           => 'T'
                , p_cust_profile_amt_rec    => pcustproamtrec
                , p_object_version_number   => p_object_version_number
                , x_return_status           => ostatus
                , x_msg_count               => omsgcount
                , x_msg_data                => omsgdata);
            ELSE
               --Create Profile Amount
               pcustproamtrec.cust_account_profile_id := osprofile_id;
               pcustproamtrec.cust_account_id := ocustaccountid;
               pcustproamtrec.site_use_id := ocustacctsiteuseid;
               pcustproamtrec.currency_code := lv_currency_code; -- 'USD';  -- 25/09/2014 Added by pattabhi for TMS#20141001-00031 OU Testing 
               --pcustproamtrec.trx_credit_limit := c1_rec.yard_credit_limit;
               pcustproamtrec.overall_credit_limit := 0; --c1_rec.yard_credit_limit;
               pcustproamtrec.created_by_module := l_api_name;
               hz_customer_profile_v2pub.create_cust_profile_amt (
                  p_init_msg_list              => 'T'
                , p_check_foreign_key          => 'T'
                , p_cust_profile_amt_rec       => pcustproamtrec
                , x_cust_acct_profile_amt_id   => ocustacctprofileamtid
                , x_return_status              => ostatus
                , x_msg_count                  => omsgcount
                , x_msg_data                   => omsgdata);
            END IF;

            IF ostatus <> 'S'
            THEN
               return_msg (oMsgCount, omsgdata, o_return_msg);
               o_return_msg := 'BT Site Profile: ' || o_return_msg;
               o_return_status := ostatus;
               ROLLBACK TO customer_record;
               RETURN;
            END IF;

            --====================================================================
            -- Create usage rules
            --====================================================================
            hz_credit_usages_cascade_pkg.cascade_credit_usage_rules (
               p_cust_acct_profile_amt_id   => ocustacctprofileamtid
             , p_cust_profile_id            => osprofile_id
             , p_profile_class_amt_id       => NULL
             , p_profile_class_id           => NULL
             , x_return_status              => ostatus
             , x_msg_count                  => omsgcount
             , x_msg_data                   => omsgdata);

            IF ostatus <> 'S'
            THEN
               return_msg (oMsgCount, omsgdata, o_return_msg);
               o_return_msg := 'BT usage rule: ' || o_return_msg;
               o_return_status := ostatus;
               ROLLBACK TO customer_record;
               RETURN;
            END IF;
         END IF;

         --====================================================================
         -- Create contact for Auth Buyer
         --====================================================================
         FOR c2_rec
            IN (SELECT a.ROWID, a.* --a.first_name, a.last_name                       --commented and added all columns for ver 4.0
                  FROM apps.xxwc_ar_new_acct_authbuyer_tbl a
                 WHERE record_id = c1_rec.record_id
                       AND NVL (process_flag, 'N') = 'N')
         LOOP
            p_create_person_rec := NULL;
            x_contact_party_id := NULL;
            x_contact_party_number := NULL;
            x_contact_profile_id := NULL;
            p_create_person_rec.person_first_name := c2_rec.first_name;
            p_create_person_rec.person_last_name := c2_rec.last_name;
            p_create_person_rec.created_by_module := l_api_name;
            hz_party_v2pub.create_person ('T'
                                        , p_create_person_rec
                                        , x_contact_party_id
                                        , x_contact_party_number
                                        , x_contact_profile_id
                                        , ostatus
                                        , omsgcount
                                        , omsgdata);

            IF ostatus <> 'S'
            THEN
               return_msg (oMsgCount, omsgdata, o_return_msg);
               o_return_msg := 'AuthBuyer Party: ' || o_return_msg;
               o_return_status := ostatus;
               ROLLBACK TO customer_record;
               RETURN;
            END IF;

            --====================================================================
            -- org contact
            --====================================================================
            p_org_contact_rec := NULL;
            p_org_contact_rec.created_by_module := l_api_name;
            p_org_contact_rec.party_rel_rec.subject_id := x_contact_party_id;
            p_org_contact_rec.party_rel_rec.subject_type := 'PERSON';
            p_org_contact_rec.party_rel_rec.subject_table_name := 'HZ_PARTIES';
            p_org_contact_rec.party_rel_rec.object_id := l_party_id;
            p_org_contact_rec.party_rel_rec.object_type := 'ORGANIZATION';
            p_org_contact_rec.party_rel_rec.object_table_name := 'HZ_PARTIES';
            p_org_contact_rec.party_rel_rec.relationship_code := 'CONTACT_OF';
            p_org_contact_rec.party_rel_rec.relationship_type := 'CONTACT';
            p_org_contact_rec.party_rel_rec.start_date := SYSDATE;
            hz_party_contact_v2pub.create_org_contact ('T'
                                                     , p_org_contact_rec
                                                     , x_org_contact_id
                                                     , x_party_rel_id
                                                     , x_rel_party_id
                                                     , x_rel_party_number
                                                     , ostatus
                                                     , omsgcount
                                                     , omsgdata);

            IF ostatus <> 'S'
            THEN
               return_msg (oMsgCount, omsgdata, o_return_msg);
               o_return_msg := 'AuthBuyer rel: ' || o_return_msg;
               o_return_status := ostatus;
               ROLLBACK TO customer_record;
               RETURN;
            END IF;


       ------------------------------------------------------------------------------------------------------------
       -- Create a Phone Contact Point using party_id   --Added PHONE, EMAIL AND FAX FOR VER 4.0
       ------------------------------------------------------------------------------------------------------------
      IF c2_rec.phone_number IS NOT NULL THEN

       -- Initializing the Mandatory API parameters
       pcontactpointrec.contact_point_type    := 'PHONE';
       pcontactpointrec.owner_table_name      := 'HZ_PARTIES';
       pcontactpointrec.owner_table_id        := x_rel_party_id ;--l_party_id;
       pcontactpointrec.primary_flag          := 'Y';
       pcontactpointrec.contact_point_purpose := 'BUSINESS';
       pcontactpointrec.created_by_module     := l_api_name;--'BO_API';

       pphonerec.phone_area_code               :=  NULL;
       pphonerec.phone_country_code            := '1';
       pphonerec.phone_number                  := c2_rec.phone_number ;
       pphonerec.phone_line_type               := 'GEN';


       HZ_CONTACT_POINT_V2PUB.CREATE_CONTACT_POINT  (p_init_msg_list      => FND_API.G_TRUE
                                                 ,   p_contact_point_rec  => pcontactpointrec
                                                 ,   p_edi_rec            => pedirec
                                                 ,   p_email_rec          => pemailrec
                                                 ,   p_phone_rec          => pphonerec
                                                 ,   p_telex_rec          => ptelexrec
                                                 ,   p_web_rec            => pwebrec
                                                 ,   x_contact_point_id   => ocontactpointid
                                                 ,   x_return_status      => ostatus
                                                 ,   x_msg_count          => omsgcount
                                                 ,   x_msg_data           => omsgdata);

         IF ostatus != 'S' THEN
          return_msg (oMsgCount, omsgdata, o_return_msg);
          o_return_msg := 'Contact phone: ' || o_return_msg;
          o_return_status := ostatus;
          ROLLBACK TO customer_record;
          RETURN;
         END IF;
       END IF; 

 ------------------------------------------------------------------------------------------------------------
    -- Create a Fax Contact Point using party_id
    ------------------------------------------------------------------------------------------------------------
    IF c2_rec.fax_number IS NOT NULL THEN
       -- Initializing the Mandatory API parameters
       pcontactpointrec.contact_point_type    := 'PHONE';
       pcontactpointrec.owner_table_name      := 'HZ_PARTIES';
       pcontactpointrec.owner_table_id        := x_rel_party_id;--l_party_id;
       pcontactpointrec.primary_flag          := 'Y';
       pcontactpointrec.contact_point_purpose := 'BUSINESS';
       pcontactpointrec.created_by_module     := l_api_name;--'BO_API';

       pphonerec.phone_area_code               :=  NULL;
       pphonerec.phone_country_code            := '1';
       pphonerec.phone_number                  := c2_rec.fax_number ;
       pphonerec.phone_line_type               := 'FAX';


       HZ_CONTACT_POINT_V2PUB.CREATE_CONTACT_POINT  (p_init_msg_list      => FND_API.G_TRUE
                                                 ,   p_contact_point_rec  => pcontactpointrec
                                                 ,   p_edi_rec            => pedirec
                                                 ,   p_email_rec          => pemailrec
                                                 ,   p_phone_rec          => pphonerec
                                                 ,   p_telex_rec          => ptelexrec
                                                 ,   p_web_rec            => pwebrec
                                                 ,   x_contact_point_id   => ocontactpointid
                                                 ,   x_return_status      => ostatus
                                                 ,   x_msg_count          => omsgcount
                                                 ,   x_msg_data           => omsgdata);

     IF ostatus != 'S' THEN
          return_msg (oMsgCount, omsgdata, o_return_msg);
          o_return_msg := 'Contact fax: ' || o_return_msg;
          o_return_status := ostatus;
          ROLLBACK TO customer_record;
          RETURN;
         END IF;
       END IF; 


 


    ------------------------------------------------------------------------------------------------------------
    -- Create a Email Contact Point using party_id
    ------------------------------------------------------------------------------------------------------------
    IF c2_rec.email_address IS NOT NULL THEN
       -- Initializing the Mandatory API parameters
       pcontactpointrec.contact_point_type    := 'EMAIL';
       pcontactpointrec.owner_table_name      := 'HZ_PARTIES';
       pcontactpointrec.owner_table_id        := x_rel_party_id;--l_party_id;
       pcontactpointrec.primary_flag          := 'Y';
       pcontactpointrec.contact_point_purpose := 'BUSINESS';
       pcontactpointrec.created_by_module     := 'BO_API';

       pemailrec.email_format             :=  'MAILHTML';
       pemailrec.email_address            := c2_rec.email_address;

       HZ_CONTACT_POINT_V2PUB.CREATE_CONTACT_POINT  (p_init_msg_list      => FND_API.G_TRUE
                                                 ,   p_contact_point_rec  => pcontactpointrec
                                                 ,   p_edi_rec            => pedirec
                                                 ,   p_email_rec          => pemailrec
                                                 ,   p_phone_rec          => pphonerec
                                                 ,   p_telex_rec          => ptelexrec
                                                 ,   p_web_rec            => pwebrec
                                                 ,   x_contact_point_id   => ocontactpointid
                                                 ,   x_return_status      => ostatus
                                                 ,   x_msg_count          => omsgcount
                                                 ,   x_msg_data           => omsgdata);

     IF ostatus != 'S' THEN
          return_msg (oMsgCount, omsgdata, o_return_msg);
          o_return_msg := 'Contact email: ' || o_return_msg;
          o_return_status := ostatus;
          ROLLBACK TO customer_record;
          RETURN;
         END IF;
       END IF; 

            --====================================================================
            -- create location for contact
            --====================================================================
            plocationrec := NULL;
            ppartysiterec := NULL;
            pcustacctsiterec := NULL;
            pcustacctsiteuserec := NULL;
            pcustomerprofile := NULL;
            ostatus := NULL;
            omsgcount := NULL;
            omsgdata := NULL;


            l_count := 0;
            olocationid := NULL;
            ostatus := NULL;

           -- plocationrec.country := lv_country_code;  -- 'US';  -- 25/09/2014 Added by pattabhi for TMS#20141001-00031 OU Testing 
            plocationrec.country     := c1_rec.bill_to_country;    --commented above and added below for ver 4.0
            plocationrec.postal_code := c1_rec.bill_to_postal_code;
            plocationrec.address1 := c1_rec.bill_to_address1;
            plocationrec.address2 := c1_rec.bill_to_address2;
            plocationrec.address3 := c1_rec.bill_to_address3;
            plocationrec.address4 := c1_rec.bill_to_address4;
            plocationrec.state := c1_rec.bill_to_state;
            plocationrec.city := c1_rec.bill_to_city;
            plocationrec.county := c1_rec.bill_to_county;
            plocationrec.created_by_module := l_api_name;
            plocationrec.sales_tax_geocode := c1_rec.bill_to_geocode;

            hz_location_v2pub.create_location (
               p_init_msg_list   => 'T'
             , p_location_rec    => plocationrec
             , x_location_id     => olocationid
             , x_return_status   => ostatus
             , x_msg_count       => omsgcount
             , x_msg_data        => omsgdata);

            IF ostatus <> 'S'
            THEN
               return_msg (oMsgCount, omsgdata, o_return_msg);
               o_return_msg := 'Auth Buyer Loc: ' || o_return_msg;
               o_return_status := ostatus;
               ROLLBACK TO customer_record;
               RETURN;
            END IF;

            --====================================================================
            -- Create Party Site
            --====================================================================
            ostatus := NULL;
            omsgcount := NULL;
            omsgdata := NULL;
            opartysiteid := NULL;
            opartysiteno := NULL;
            osprofile_id := NULL;
            ppartysiterec.created_by_module := l_api_name;

            -- create a party site now
            ppartysiterec.party_id := x_rel_party_id;
            ppartysiterec.location_id := olocationid;
            ppartysiterec.identifying_address_flag := 'Y';

            hz_party_site_v2pub.create_party_site (
               p_init_msg_list       => 'T'
             , p_party_site_rec      => ppartysiterec
             , x_party_site_id       => opartysiteid
             , x_party_site_number   => opartysiteno
             , x_return_status       => ostatus
             , x_msg_count           => omsgcount
             , x_msg_data            => omsgdata);


            IF ostatus <> 'S'
            THEN
               return_msg (oMsgCount, omsgdata, o_return_msg);
               o_return_msg := 'Auth Buyer PS: ' || o_return_msg;
               o_return_status := ostatus;
               ROLLBACK TO customer_record;
               RETURN;
            END IF;

            --====================================================================
            -- create account role
            --====================================================================

            p_cr_cust_acc_role_rec := NULL;
            p_cr_cust_acc_role_rec.party_id := x_rel_party_id;
            --p_cr_cust_acc_role_rec.org_contact_id := x_org_contact_id;
            p_cr_cust_acc_role_rec.cust_account_id := ocustaccountid;
            p_cr_cust_acc_role_rec.primary_flag := 'N';
            p_cr_cust_acc_role_rec.role_type := 'CONTACT';
            p_cr_cust_acc_role_rec.created_by_module := l_api_name;
            hz_cust_account_role_v2pub.create_cust_account_role (
               'T'
             , p_cr_cust_acc_role_rec
             , x_cust_account_role_id
             , ostatus
             , omsgcount
             , omsgdata);

            IF ostatus <> 'S'
            THEN
               return_msg (oMsgCount, omsgdata, o_return_msg);
               o_return_msg := 'AuthBuyer role: ' || o_return_msg;
               o_return_status := ostatus;
               ROLLBACK TO customer_record;
               RETURN;
            END IF;

            --====================================================================
            -- Assign Authorized Buyer role
            --====================================================================
            p_ROLE_RESPONSIBILITY_REC_TYPE := NULL;
            p_ROLE_RESPONSIBILITY_REC_TYPE.cust_account_role_id :=  x_cust_account_role_id;

--Added below IF CLAUSE for ver 4.0 
            IF c2_rec.role = 'Accounts Payable' THEN
            	p_ROLE_RESPONSIBILITY_REC_TYPE.responsibility_type := 'ACC_PAY';
            ELSIF c2_rec.role = 'Authorized Buyer' THEN
            	p_ROLE_RESPONSIBILITY_REC_TYPE.responsibility_type := 'AUTH_BUYER';
            END IF;

            p_ROLE_RESPONSIBILITY_REC_TYPE.created_by_module := l_api_name;
            hz_cust_account_role_v2pub.create_role_responsibility (
               p_init_msg_list             => 'T'
             , p_role_responsibility_rec   => p_ROLE_RESPONSIBILITY_REC_TYPE
             , x_responsibility_id         => orespid
             , x_return_status             => ostatus
             , x_msg_count                 => omsgcount
             , x_msg_data                  => omsgdata);

            IF ostatus <> 'S'
            THEN
               return_msg (oMsgCount, omsgdata, o_return_msg);
               o_return_msg := 'AuthBuyer role resp: ' || o_return_msg;
               o_return_status := ostatus;
               ROLLBACK TO customer_record;
               RETURN;
            END IF;


            UPDATE apps.xxwc_ar_new_acct_authbuyer_tbl
               SET process_flag = 'Y'
             WHERE ROWID = c2_rec.ROWID;
         END LOOP;

         --====================================================================
         -- IF Ship to Address populated-- create bill to and ship to site use
         --====================================================================
         IF     c1_rec.ship_to_address1 IS NOT NULL
            AND c1_rec.ship_to_city IS NOT NULL
            AND c1_rec.ship_to_county IS NOT NULL
            AND c1_rec.ship_to_state IS NOT NULL
            AND c1_rec.ship_to_postal_code IS NOT NULL
             
         THEN
            -- create location
            plocationrec := NULL;
            ppartysiterec := NULL;
            pcustacctsiterec := NULL;
            pcustacctsiteuserec := NULL;
            pcustomerprofile := NULL;
            ostatus := NULL;
            omsgcount := NULL;
            omsgdata := NULL;


            l_count := 0;
            olocationid := NULL;
            ostatus := NULL;

           -- plocationrec.country := lv_country_code; -- 'US';  -- 25/09/2014 Added by pattabhi for TMS#20141001-00031 OU Testing 
            plocationrec.country     := c1_rec.ship_to_country; --Commented above and added below for ver 4.0
            plocationrec.postal_code := c1_rec.ship_to_postal_code;
            plocationrec.address1 := c1_rec.ship_to_address1;
            plocationrec.address2 := c1_rec.ship_to_address2;
            plocationrec.address3 := c1_rec.ship_to_address3;
            plocationrec.address4 := c1_rec.ship_to_address4;
            plocationrec.state := c1_rec.ship_to_state;
            plocationrec.city := c1_rec.ship_to_city;
            plocationrec.county := c1_rec.ship_to_county;
            plocationrec.created_by_module := l_api_name;
            plocationrec.sales_tax_geocode := c1_rec.ship_to_geocode;

            hz_location_v2pub.create_location (
               p_init_msg_list   => 'T'
             , p_location_rec    => plocationrec
             , x_location_id     => olocationid
             , x_return_status   => ostatus
             , x_msg_count       => omsgcount
             , x_msg_data        => omsgdata);

            IF ostatus <> 'S'
            THEN
               return_msg (oMsgCount, omsgdata, o_return_msg);
               o_return_msg := 'ship To Loc: ' || o_return_msg;
               o_return_status := ostatus;
               ROLLBACK TO customer_record;
               RETURN;
            END IF;

            --====================================================================
            -- Create Party Site
            --====================================================================
            ostatus := NULL;
            omsgcount := NULL;
            omsgdata := NULL;
            opartysiteid := NULL;
            opartysiteno := NULL;
            osprofile_id := NULL;
            ppartysiterec.created_by_module := l_api_name;

            -- create a party site now
            ppartysiterec.party_id := l_party_id;
            ppartysiterec.location_id := olocationid;

            hz_party_site_v2pub.create_party_site (
               p_init_msg_list       => 'T'
             , p_party_site_rec      => ppartysiterec
             , x_party_site_id       => opartysiteid
             , x_party_site_number   => opartysiteno
             , x_return_status       => ostatus
             , x_msg_count           => omsgcount
             , x_msg_data            => omsgdata);

            IF ostatus <> 'S'
            THEN
               return_msg (oMsgCount, omsgdata, o_return_msg);
               o_return_msg := 'Ship To PS: ' || o_return_msg;
               o_return_status := ostatus;
               ROLLBACK TO customer_record;
               RETURN;
            END IF;

            --====================================================================
            -- Create cust acct site
            --====================================================================
            ostatus := NULL;
            omsgcount := NULL;
            omsgdata := NULL;
            ocustacctsiteid := NULL;
            pcustacctsiterec.cust_account_id := ocustaccountid;
            pcustacctsiterec.party_site_id := opartysiteid;
            pcustacctsiterec.created_by_module := l_api_name;
            pcustacctsiterec.org_id := l_org_id;  --162;  -- 23/09/2014 Added by pattabhi for TMS#20141001-00031 OU Testing 
            pcustacctsiterec.attribute3 :=
               SUBSTR (c1_rec.mand_po_number, 1, 1);
            pcustacctsiterec.attribute17 := c1_rec.prism_yard_site_num;
            hz_cust_account_site_v2pub.create_cust_acct_site (
               p_init_msg_list        => 'T'
             , p_cust_acct_site_rec   => pcustacctsiterec
             , x_cust_acct_site_id    => ocustacctsiteid
             , x_return_status        => ostatus
             , x_msg_count            => omsgcount
             , x_msg_data             => omsgdata);

            IF ostatus <> 'S'
            THEN
               return_msg (oMsgCount, omsgdata, o_return_msg);
               o_return_msg := 'Ship To Acct Site: ' || o_return_msg;
               o_return_status := ostatus;
               ROLLBACK TO customer_record;
               RETURN;
            END IF;

            --====================================================================
            -- Create site use (bill to)
            --====================================================================
            pcustacctsiteuserec := NULL;
            ostatus := NULL;
            omsgcount := NULL;
            omsgdata := NULL;
            ocustacctsiteuseid := NULL;

            pcustacctsiteuserec.site_use_code := 'BILL_TO';
            pcustacctsiteuserec.cust_acct_site_id := ocustacctsiteid;
            pcustacctsiteuserec.attribute1 := 'YARD';
            pcustacctsiteuserec.primary_flag := 'N';
            pcustacctsiteuserec.location := c1_rec.customer_name || '/YARD';
            pcustacctsiteuserec.created_by_module := l_api_name;
            pcustacctsiteuserec.primary_salesrep_id := c1_rec.salesrep_id;

            pcustomerprofile.profile_class_id := c1_rec.profile_class_id;
            pcustomerprofile.credit_checking := 'Y';
            pcustomerprofile.attribute3 :=
               SUBSTR (c1_rec.statement_by_job, 1, 1);
            pcustomerprofile.collector_id := c1_rec.collector_id;
            pcustomerprofile.credit_analyst_id := c1_rec.credit_manager_id;
            pcustomerprofile.credit_classification :=
               c1_rec.credit_classification;
            pcustomerprofile.attribute2 := c1_rec.inv_remit_to;
            pcustomerprofile.account_status := c1_rec.account_status;
            pcustomerprofile.tolerance := 0;

            hz_cust_account_site_v2pub.create_cust_site_use (
               p_init_msg_list          => 'T'
             , p_cust_site_use_rec      => pcustacctsiteuserec
             , p_customer_profile_rec   => pcustomerprofile
             , p_create_profile         => 'T'
             , p_create_profile_amt     => 'T'
             , x_site_use_id            => ocustacctsiteuseid
             , x_return_status          => ostatus
             , x_msg_count              => omsgcount
             , x_msg_data               => omsgdata);


            IF ostatus <> 'S'
            THEN
               return_msg (oMsgCount, omsgdata, o_return_msg);
               o_return_msg := 'Ship To SU: ' || o_return_msg;
               o_return_status := ostatus;
               ROLLBACK TO customer_record;
               RETURN;
            END IF;

            --====================================================================
            -- create site profile amount
            --====================================================================
            pcustproamtrec := NULL;

            BEGIN
               SELECT cust_account_profile_id
                 INTO osprofile_id
                 FROM apps.hz_customer_profiles
                WHERE cust_account_id = ocustaccountid
                      AND site_use_id = ocustacctsiteuseid;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  osprofile_id := NULL;
            END;


            IF osprofile_id IS NOT NULL
            THEN
               BEGIN
                  SELECT cust_acct_profile_amt_id, object_version_number
                    INTO ocustacctprofileamtid, p_object_version_number
                    FROM apps.hz_cust_profile_amts
                   WHERE cust_account_profile_id = osprofile_id
                         AND currency_code = lv_currency_code;  -- 'USD';  - -- 25/09/2014 Added by pattabhi for TMS#20141001-00031 OU Testing 
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     ocustacctprofileamtid := NULL;
                     p_object_version_number := NULL;
               END;

               IF ocustacctprofileamtid IS NOT NULL
               THEN
                  --Update amount profile
                  pcustproamtrec.cust_acct_profile_amt_id :=
                     ocustacctprofileamtid;
                  -- pcustproamtrec.trx_credit_limit := c1_rec.yard_credit_limit;
                  pcustproamtrec.overall_credit_limit :=
                     c1_rec.yard_credit_limit;
                  pcustproamtrec.created_by_module := l_api_name;
                  hz_customer_profile_v2pub.update_cust_profile_amt (
                     p_init_msg_list           => 'T'
                   , p_cust_profile_amt_rec    => pcustproamtrec
                   , p_object_version_number   => p_object_version_number
                   , x_return_status           => ostatus
                   , x_msg_count               => omsgcount
                   , x_msg_data                => omsgdata);
               ELSE
                  --Create Profile Amount
                  pcustproamtrec.cust_account_profile_id := osprofile_id;
                  pcustproamtrec.cust_account_id := ocustaccountid;
                  pcustproamtrec.site_use_id := ocustacctsiteuseid;
                  pcustproamtrec.currency_code := lv_currency_code;  -- 'USD';  -- 25/09/2014 Added by pattabhi for TMS#20141001-00031 OU Testing 
                  --pcustproamtrec.trx_credit_limit := c1_rec.yard_credit_limit;
                  pcustproamtrec.overall_credit_limit :=
                     c1_rec.yard_credit_limit;
                  pcustproamtrec.created_by_module := l_api_name;
                  hz_customer_profile_v2pub.create_cust_profile_amt (
                     p_init_msg_list              => 'T'
                   , p_check_foreign_key          => 'T'
                   , p_cust_profile_amt_rec       => pcustproamtrec
                   , x_cust_acct_profile_amt_id   => ocustacctprofileamtid
                   , x_return_status              => ostatus
                   , x_msg_count                  => omsgcount
                   , x_msg_data                   => omsgdata);
               END IF;

               IF ostatus <> 'S'
               THEN
                  return_msg (oMsgCount, omsgdata, o_return_msg);
                  o_return_msg := 'BT Site Profile: ' || o_return_msg;
                  o_return_status := ostatus;
                  ROLLBACK TO customer_record;
                  RETURN;
               END IF;

               --====================================================================
               -- Create usage rules
               --====================================================================
               hz_credit_usages_cascade_pkg.cascade_credit_usage_rules (
                  p_cust_acct_profile_amt_id   => ocustacctprofileamtid
                , p_cust_profile_id            => osprofile_id
                , p_profile_class_amt_id       => NULL
                , p_profile_class_id           => NULL
                , x_return_status              => ostatus
                , x_msg_count                  => omsgcount
                , x_msg_data                   => omsgdata);

               IF ostatus <> 'S'
               THEN
                  return_msg (oMsgCount, omsgdata, o_return_msg);
                  o_return_msg := 'ST usage rule: ' || o_return_msg;
                  o_return_status := ostatus;
                  ROLLBACK TO customer_record;
                  RETURN;
               END IF;
            END IF;


            --====================================================================
            -- Create site use (ship to)
            --====================================================================
            pcustacctsiteuserec := NULL;
            ostatus := NULL;
            omsgcount := NULL;
            omsgdata := NULL;
            ocustacctsiteuseid := NULL;

            pcustacctsiteuserec.site_use_code := 'SHIP_TO';
            pcustacctsiteuserec.cust_acct_site_id := ocustacctsiteid;
            pcustacctsiteuserec.attribute1 := 'YARD';
            pcustacctsiteuserec.primary_flag := 'Y';
            pcustacctsiteuserec.location := c1_rec.customer_name || '/YARD';
            pcustacctsiteuserec.created_by_module := l_api_name;
            pcustacctsiteuserec.bill_to_site_use_id := ocustacctsiteuseid;
            pcustacctsiteuserec.primary_salesrep_id := c1_rec.salesrep_id;
            /*
                        pcustomerprofile.profile_class_id := c1_rec.profile_class_id;
                        pcustomerprofile.credit_checking := 'Y';
                        pcustomerprofile.attribute3 :=
                           SUBSTR (c1_rec.statement_by_job, 1, 1);
                        pcustomerprofile.collector_id := c1_rec.collector_id;
                        pcustomerprofile.credit_analyst_id := c1_rec.credit_manager_id;
                        pcustomerprofile.credit_classification :=
                           c1_rec.credit_classification;
                        pcustomerprofile.attribute2 := c1_rec.inv_remit_to;
                        pcustomerprofile.account_status := c1_rec.account_status;
                        pcustomerprofile.tolerance := 0;
            */
            hz_cust_account_site_v2pub.create_cust_site_use (
               p_init_msg_list          => 'T'
             , p_cust_site_use_rec      => pcustacctsiteuserec
             , p_customer_profile_rec   => pcustomerprofile
             , p_create_profile         => 'F'
             , p_create_profile_amt     => 'F'
             , x_site_use_id            => ocustacctsiteuseid
             , x_return_status          => ostatus
             , x_msg_count              => omsgcount
             , x_msg_data               => omsgdata);


            IF ostatus <> 'S'
            THEN
               return_msg (oMsgCount, omsgdata, o_return_msg);
               o_return_msg := 'Ship To SU: ' || o_return_msg;
               o_return_status := ostatus;
               ROLLBACK TO customer_record;
               RETURN;
            END IF;
         END IF;                                  -- ship to address populated

          --====================================================================
            -- Update account status to inactive -- added for Version 4.0
            --====================================================================
          BEGIN
            SELECT object_version_number
               INTO l_acc_obj_version_number
              FROM hz_cust_accounts 
            WHERE cust_account_id = ocustaccountid;
          EXCEPTION
          WHEN OTHERS THEN
              l_acc_obj_version_number := 1;
          END;

          pcustaccountrec.cust_account_id	:= ocustaccountid;
          pcustaccountrec.status   		:= 'I';
               
          HZ_CUST_ACCOUNT_V2PUB.UPDATE_CUST_ACCOUNT ( p_init_msg_list         => 'T',
				                      p_cust_account_rec      => pcustaccountrec,
					              p_object_version_number => l_acc_obj_version_number,
			                              x_return_status         => ostatus,
				                      x_msg_count             => omsgcount,
				                      x_msg_data              => omsgdata );


         o_return_status := ostatus;
         o_return_msg := 'Customer Created Successfully:' || ocustaccountno;

         UPDATE apps.xxwc_ar_new_acct_tbl
            SET process_flag = 'Y', account_number = ocustaccountno
          WHERE record_id = c1_rec.record_id;

         COMMIT;
      END LOOP;
   END create_account;
END xxwc_ar_new_account_pkg;
/