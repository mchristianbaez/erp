/* Formatted on 1/7/2014 6:29:18 PM (QP5 v5.206) */
-- Start of DDL Script for Package Body APPS.ECE_POCO_TRANSACTION
-- Generated 1/7/2014 6:29:14 PM from APPS@EBIZDEV

CREATE OR REPLACE PACKAGE BODY apps.ece_poco_transaction
AS
    -- $Header: ECPOCOB.pls 120.10.12010000.1 2008/07/25 07:25:44 appldev ship $
    ioutput_width   INTEGER := 4000;                                                                          -- 2823215
    i_path          VARCHAR2 (1000);
    i_filename      VARCHAR2 (1000);

    PROCEDURE extract_poco_outbound (errbuf                 OUT NOCOPY VARCHAR2
                                    ,retcode                OUT NOCOPY VARCHAR2
                                    ,coutput_path        IN            VARCHAR2
                                    ,coutput_filename    IN            VARCHAR2
                                    ,cpo_number_from     IN            VARCHAR2
                                    ,cpo_number_to       IN            VARCHAR2
                                    ,crdate_from         IN            VARCHAR2
                                    ,crdate_to           IN            VARCHAR2
                                    ,cpc_type            IN            VARCHAR2
                                    ,cvendor_name        IN            VARCHAR2
                                    ,cvendor_site_code   IN            VARCHAR2
                                    ,v_debug_mode        IN            NUMBER DEFAULT 0)
    IS
        xprogress                  VARCHAR2 (80);
        v_levelprocessed           VARCHAR2 (40);
        irun_id                    NUMBER := 0;
        ioutput_width              INTEGER := 4000;
        ctransaction_type          VARCHAR2 (120) := 'POCO';
        ccommunication_method      VARCHAR2 (120) := 'EDI';
        cheader_interface          VARCHAR2 (120) := 'ECE_PO_INTERFACE_HEADERS';
        cline_interface            VARCHAR2 (120) := 'ECE_PO_INTERFACE_LINES';
        cshipment_interface        VARCHAR2 (120) := 'ECE_PO_INTERFACE_SHIPMENTS';
        cproject_interface         VARCHAR2 (120) := 'ECE_PO_DISTRIBUTIONS';                              -- Bug 1891291
        l_line_text                VARCHAR2 (2000);
        crevised_date_from         DATE := TO_DATE (crdate_from, 'YYYY/MM/DD HH24:MI:SS');
        crevised_date_to           DATE := TO_DATE (crdate_to, 'YYYY/MM/DD HH24:MI:SS') + 1;
        cenabled                   VARCHAR2 (1) := 'Y';
        ece_transaction_disabled   EXCEPTION;
        xheadercount               NUMBER;
        cfilename                  VARCHAR2 (30) := NULL;                                                      --2430822

        CURSOR c_output
        IS
              SELECT text
                FROM ece_output
               WHERE run_id = irun_id
            ORDER BY line_id;
    BEGIN
        xprogress := 'POCO-10-1000';
        ec_debug.enable_debug (v_debug_mode);
        ec_debug.push ('ECE_POCO_TRANSACTION.EXTRACT_POCO_OUTBOUND');
        ec_debug.pl (3, 'cOutput_Path: ', coutput_path);
        ec_debug.pl (3, 'cOutput_Filename: ', coutput_filename);
        ec_debug.pl (3, 'cPO_Number_From: ', cpo_number_from);
        ec_debug.pl (3, 'cPO_Number_To: ', cpo_number_to);
        ec_debug.pl (3, 'cRDate_From: ', crdate_from);
        ec_debug.pl (3, 'cRDate_To: ', crdate_to);
        ec_debug.pl (3, 'cPC_Type: ', cpc_type);
        ec_debug.pl (3, 'cVendor_Name: ', cvendor_name);
        ec_debug.pl (3, 'cVendor_Site_Code: ', cvendor_site_code);
        ec_debug.pl (3, 'v_debug_mode: ', v_debug_mode);

        /* Check to see if the transaction is enabled. If not, abort */
        xprogress := 'POCO-10-1001';
        fnd_profile.get ('ECE_' || ctransaction_type || '_ENABLED', cenabled);

        xprogress := 'POCO-10-1002';

        IF cenabled = 'N'
        THEN
            xprogress := 'POCO-10-1003';
            RAISE ece_transaction_disabled;
        END IF;

        xprogress := 'POCO-10-1004';

        BEGIN
            SELECT ece_output_runs_s.NEXTVAL INTO irun_id FROM DUAL;
        EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
                ec_debug.pl (0
                            ,'EC'
                            ,'ECE_GET_NEXT_SEQ_FAILED'
                            ,'PROGRESS_LEVEL'
                            ,xprogress
                            ,'SEQ'
                            ,'ECE_OUTPUT_RUNS_S');
        END;

        ec_debug.pl (3, 'iRun_id: ', irun_id);

        xprogress := 'POCO-10-1005';
        ec_debug.pl (0
                    ,'EC'
                    ,'ECE_POCO_START'
                    ,NULL);

        xprogress := 'POCO-10-1010';
        ec_debug.pl (0
                    ,'EC'
                    ,'ECE_RUN_ID'
                    ,'RUN_ID'
                    ,irun_id);

        ece_poo_transaction.project_sel_c := 0;                                                            --Bug 2490109

        IF coutput_filename IS NULL
        THEN                                                                                               --Bug 2430822
            --cfilename := 'POCO' || irun_id || '.dat';
            cfilename := 'POCO' || irun_id || '.tmp';-- changed by Rasikha 01/06/2014 TMS 20130415-00522, to prevent the file ftp before write will finish
            --another custom CP "XXWC EDI File extention renaming" will change it back to .dat after checking that original CP finished processing.
        ELSE
            cfilename := coutput_filename;
        END IF;

        -- Open the file for write.
        xprogress := 'POO-10-1040';

        IF ec_debug.g_debug_level = 1
        THEN
            ec_debug.pl (1, 'Output File:', cfilename);
            ec_debug.pl (1, 'path --> ', coutput_path);
        --   ec_debug.pl(1,'Open Output file');            --Bug 2034376
        END IF;

        i_path := coutput_path;
        i_filename := cfilename;
        -- ece_poo_transaction.uFile_type := utl_file.fopen(cOutput_Path,cFilename,'W',32767); --Bug 2887790

        xprogress := 'POCO-10-1020';
        ec_debug.pl (1, 'Call Populate Poco Trx procedure');                                               --Bug 2034376
        ece_poco_transaction.populate_poco_trx (ccommunication_method
                                               ,ctransaction_type
                                               ,ioutput_width
                                               ,SYSDATE
                                               ,irun_id
                                               ,cheader_interface
                                               ,cline_interface
                                               ,cshipment_interface
                                               ,cproject_interface
                                               ,crevised_date_from
                                               ,crevised_date_to
                                               ,cvendor_name
                                               ,cvendor_site_code
                                               ,cpc_type
                                               ,cpo_number_from
                                               ,cpo_number_to);

        /*      xProgress := 'POCO-10-1030';
          ec_debug.pl(1,'Call Put To Output Table procedure');   --Bug 2034376

              select count(*)
              into xHeaderCount
              from ECE_PO_INTERFACE_HEADERS
              where run_id = iRun_id;  */

        -- 2823215

        /*         ec_debug.pl(1,'NUMBER OF RECORDS PROCESSED IS ',xHeaderCount);
                 ece_poco_transaction.put_data_to_output_table(
                    cCommunication_Method,
                    cTransaction_Type,
                    iOutput_width,
                    iRun_id,
                    cHeader_Interface,
                    cLine_Interface,
                    cShipment_Interface,
                    cProject_Interface); */

        xprogress := 'POCO-10-1090';
        ec_debug.pl (1, 'Close Output file');                                                              --Bug 2034376

        IF (UTL_FILE.is_open (ece_poo_transaction.ufile_type))
        THEN
            UTL_FILE.fclose (ece_poo_transaction.ufile_type);
        END IF;

        IF ec_mapping_utils.ec_get_trans_upgrade_status (ctransaction_type) = 'U'
        THEN
            ec_debug.pl (0
                        ,'EC'
                        ,'ECE_REC_TRANS_PENDING'
                        ,NULL);
            retcode := 1;
        END IF;

        ec_debug.pop ('ECE_POCO_TRANSACTION.EXTRACT_POCO_OUTBOUND');
        ec_debug.disable_debug;
        COMMIT;
    EXCEPTION
        WHEN ece_transaction_disabled
        THEN
            ec_debug.pl (0
                        ,'EC'
                        ,'ECE_TRANSACTION_DISABLED'
                        ,'TRANSACTION'
                        ,ctransaction_type);
            retcode := 1;
            ec_debug.disable_debug;
            ROLLBACK;
        WHEN UTL_FILE.write_error
        THEN
            ec_debug.pl (0
                        ,'EC'
                        ,'ECE_UTL_WRITE_ERROR'
                        ,NULL);
            ec_debug.pl (0
                        ,'EC'
                        ,'ECE_ERROR_MESSAGE'
                        ,'ERROR_MESSAGE'
                        ,SQLERRM);

            retcode := 2;
            ec_debug.disable_debug;

            IF (UTL_FILE.is_open (ece_poo_transaction.ufile_type))
            THEN
                UTL_FILE.fclose (ece_poo_transaction.ufile_type);
            END IF;

            ece_poo_transaction.ufile_type :=
                UTL_FILE.fopen (coutput_path
                               ,cfilename
                               ,'W'
                               ,32767);
            UTL_FILE.fclose (ece_poo_transaction.ufile_type);
            ROLLBACK;
        WHEN UTL_FILE.invalid_path
        THEN
            ec_debug.pl (0
                        ,'EC'
                        ,'ECE_UTIL_INVALID_PATH'
                        ,NULL);
            ec_debug.pl (0
                        ,'EC'
                        ,'ECE_ERROR_MESSAGE'
                        ,'ERROR_MESSAGE'
                        ,SQLERRM);

            retcode := 2;
            ec_debug.disable_debug;
            ROLLBACK;
        WHEN UTL_FILE.invalid_operation
        THEN
            ec_debug.pl (0
                        ,'EC'
                        ,'ECE_UTIL_INVALID_OPERATION'
                        ,NULL);
            ec_debug.pl (0
                        ,'EC'
                        ,'ECE_ERROR_MESSAGE'
                        ,'ERROR_MESSAGE'
                        ,SQLERRM);

            retcode := 2;
            ec_debug.disable_debug;

            IF (UTL_FILE.is_open (ece_poo_transaction.ufile_type))
            THEN
                UTL_FILE.fclose (ece_poo_transaction.ufile_type);
            END IF;

            ece_poo_transaction.ufile_type :=
                UTL_FILE.fopen (coutput_path
                               ,cfilename
                               ,'W'
                               ,32767);
            UTL_FILE.fclose (ece_poo_transaction.ufile_type);
            ROLLBACK;
        WHEN OTHERS
        THEN
            ec_debug.pl (0
                        ,'EC'
                        ,'ECE_PROGRAM_ERROR'
                        ,'PROGRESS_LEVEL'
                        ,xprogress);
            ec_debug.pl (0
                        ,'EC'
                        ,'ECE_ERROR_MESSAGE'
                        ,'ERROR_MESSAGE'
                        ,SQLERRM);

            retcode := 2;
            ec_debug.disable_debug;

            IF (UTL_FILE.is_open (ece_poo_transaction.ufile_type))
            THEN
                UTL_FILE.fclose (ece_poo_transaction.ufile_type);
            END IF;

            ece_poo_transaction.ufile_type :=
                UTL_FILE.fopen (coutput_path
                               ,cfilename
                               ,'W'
                               ,32767);
            UTL_FILE.fclose (ece_poo_transaction.ufile_type);
            ROLLBACK;
    END extract_poco_outbound;

    PROCEDURE populate_poco_trx (ccommunication_method   IN VARCHAR2
                                ,ctransaction_type       IN VARCHAR2
                                ,ioutput_width           IN INTEGER
                                ,dtransaction_date       IN DATE
                                ,irun_id                 IN INTEGER
                                ,cheader_interface       IN VARCHAR2
                                ,cline_interface         IN VARCHAR2
                                ,cshipment_interface     IN VARCHAR2
                                ,cproject_interface      IN VARCHAR2
                                ,crevised_date_from      IN DATE
                                ,crevised_date_to        IN DATE
                                ,csupplier_name          IN VARCHAR2
                                ,csupplier_site          IN VARCHAR2
                                ,cdocument_type          IN VARCHAR2
                                ,cpo_number_from         IN VARCHAR2
                                ,cpo_number_to           IN VARCHAR2)
    IS
        xprogress                    VARCHAR2 (80);
        v_levelprocessed             VARCHAR2 (40);

        catt_header_interface        VARCHAR2 (120) := 'ECE_ATTACHMENT_HEADERS';
        catt_detail_interface        VARCHAR2 (120) := 'ECE_ATTACHMENT_DETAILS';

        l_header_tbl                 ece_flatfile_pvt.interface_tbl_type;
        l_line_tbl                   ece_flatfile_pvt.interface_tbl_type;
        l_shipment_tbl               ece_flatfile_pvt.interface_tbl_type;
        l_key_tbl                    ece_flatfile_pvt.interface_tbl_type;

        l_hdr_att_hdr_tbl            ece_flatfile_pvt.interface_tbl_type;
        l_hdr_att_dtl_tbl            ece_flatfile_pvt.interface_tbl_type;
        l_ln_att_hdr_tbl             ece_flatfile_pvt.interface_tbl_type;
        l_ln_att_dtl_tbl             ece_flatfile_pvt.interface_tbl_type;
        l_mi_att_hdr_tbl             ece_flatfile_pvt.interface_tbl_type;
        l_mi_att_dtl_tbl             ece_flatfile_pvt.interface_tbl_type;
        l_msi_att_hdr_tbl            ece_flatfile_pvt.interface_tbl_type;
        l_msi_att_dtl_tbl            ece_flatfile_pvt.interface_tbl_type;
        l_shp_att_hdr_tbl            ece_flatfile_pvt.interface_tbl_type;
        l_shp_att_dtl_tbl            ece_flatfile_pvt.interface_tbl_type;

        iatt_hdr_pos                 NUMBER := 0;
        iatt_ln_pos                  NUMBER := 0;
        iatt_mi_pos                  NUMBER := 0;
        iatt_msi_pos                 NUMBER := 0;
        iatt_shp_pos                 NUMBER := 0;

        v_project_acct_installed     BOOLEAN;
        v_project_acct_short_name    VARCHAR2 (2) := 'PA';
        v_project_acct_status        VARCHAR2 (120);
        v_project_acct_industry      VARCHAR2 (120);
        v_project_acct_schema        VARCHAR2 (120);

        v_att_enabled                VARCHAR2 (10);
        v_header_att_enabled         VARCHAR2 (10);
        v_line_att_enabled           VARCHAR2 (10);
        v_mitem_att_enabled          VARCHAR2 (10);
        v_iitem_att_enabled          VARCHAR2 (10);
        v_ship_att_enabled           VARCHAR2 (10);
        n_att_seg_size               NUMBER;

        v_entity_name                VARCHAR2 (120);
        v_pk1_value                  VARCHAR2 (120);
        v_pk2_value                  VARCHAR2 (120);

        header_sel_c                 INTEGER;
        line_sel_c                   INTEGER;
        shipment_sel_c               INTEGER;

        cheader_select               VARCHAR2 (32000);
        cline_select                 VARCHAR2 (32000);
        cshipment_select             VARCHAR2 (32000);

        cheader_from                 VARCHAR2 (32000);
        cline_from                   VARCHAR2 (32000);
        cshipment_from               VARCHAR2 (32000);

        cheader_where                VARCHAR2 (32000);
        cline_where                  VARCHAR2 (32000);
        cshipment_where              VARCHAR2 (32000);

        iheader_count                NUMBER := 0;
        iline_count                  NUMBER := 0;
        ishipment_count              NUMBER := 0;
        --iKey_count                 NUMBER := 0;

        l_header_fkey                NUMBER;
        l_line_fkey                  NUMBER;
        l_shipment_fkey              NUMBER;

        nheader_key_pos              NUMBER;
        nline_key_pos                NUMBER;
        nshipment_key_pos            NUMBER;

        dummy                        INTEGER;
        n_trx_date_pos               NUMBER;
        ndocument_type_pos           NUMBER;
        npo_number_pos               NUMBER;
        npo_type_pos                 NUMBER;
        nrelease_num_pos             NUMBER;
        nrelease_id_pos              NUMBER;
        nline_num_pos                NUMBER;
        nline_location_id_pos        NUMBER;
        nship_line_location_id_pos   NUMBER;
        nquantity_pending_pos        NUMBER;
        ncancel_flag_pos             NUMBER;
        ncancel_date_pos             NUMBER;
        ncancel_date_posl            NUMBER;
        l_document_type              VARCHAR2 (30);
        norganization_id             NUMBER;
        nitem_id_pos                 NUMBER;

        v_drop_ship_flag             NUMBER;
        rec_order_line_info          oe_drop_ship_grp.order_line_info_rec_type;                                --2887790
        nheader_cancel_flag_pos      NUMBER;
        nline_cancel_flag_pos        NUMBER;
        nshipment_cancel_flag_pos    NUMBER;
        v_header_cancel_flag         VARCHAR2 (10);
        v_line_cancel_flag           VARCHAR2 (10);
        v_shipment_cancel_flag       VARCHAR2 (10);

        ntrans_code_pos              NUMBER;                                                                  -- 2823215

        c_file_common_key            VARCHAR2 (255);                                                          -- 2823215

        -- Bug 2823215

        nship_release_num_pos        NUMBER;
        nline_uom_code_pos           NUMBER;
        nline_location_uom_pos       NUMBER;
        nlp_att_cat_pos              NUMBER;
        nlp_att1_pos                 NUMBER;
        nlp_att2_pos                 NUMBER;
        nlp_att3_pos                 NUMBER;
        nlp_att4_pos                 NUMBER;
        nlp_att5_pos                 NUMBER;
        nlp_att6_pos                 NUMBER;
        nlp_att7_pos                 NUMBER;
        nlp_att8_pos                 NUMBER;
        nlp_att9_pos                 NUMBER;
        nlp_att10_pos                NUMBER;
        nlp_att11_pos                NUMBER;
        nlp_att12_pos                NUMBER;
        nlp_att13_pos                NUMBER;
        nlp_att14_pos                NUMBER;
        nlp_att15_pos                NUMBER;
        nst_cust_name_pos            NUMBER;
        nst_cont_name_pos            NUMBER;
        nst_cont_phone_pos           NUMBER;
        nst_cont_fax_pos             NUMBER;
        nst_cont_email_pos           NUMBER;
        nshipping_instruct_pos       NUMBER;
        npacking_instruct_pos        NUMBER;
        nshipping_method_pos         NUMBER;
        ncust_po_num_pos             NUMBER;
        ncust_po_line_num_pos        NUMBER;
        ncust_po_ship_num_pos        NUMBER;
        ncust_prod_desc_pos          NUMBER;
        ndeliv_cust_loc_pos          NUMBER;
        ndeliv_cust_name_pos         NUMBER;
        ndeliv_cont_name_pos         NUMBER;
        ndeliv_cont_phone_pos        NUMBER;
        ndeliv_cont_fax_pos          NUMBER;
        ndeliv_cust_addr_pos         NUMBER;
        ndeliv_cont_email_pos        NUMBER;
        nheader_cancel_date_pos      NUMBER;
        --Bug 2823215

        -- Timezone enhancement
        nrel_date_pos                PLS_INTEGER;
        nrel_dt_tz_pos               PLS_INTEGER;
        nrel_dt_off_pos              PLS_INTEGER;
        ncrtn_date_pos               PLS_INTEGER;
        ncrtn_dt_tz_pos              PLS_INTEGER;
        ncrtn_dt_off_pos             PLS_INTEGER;
        nrev_date_pos                PLS_INTEGER;
        nrev_dt_tz_pos               PLS_INTEGER;
        nrev_dt_off_pos              PLS_INTEGER;
        nacc_due_dt_pos              PLS_INTEGER;
        nacc_due_tz_pos              PLS_INTEGER;
        nacc_due_off_pos             PLS_INTEGER;
        nblkt_srt_dt_pos             PLS_INTEGER;
        nblkt_srt_tz_pos             PLS_INTEGER;
        nblkt_srt_off_pos            PLS_INTEGER;
        nblkt_end_dt_pos             PLS_INTEGER;
        nblkt_end_tz_pos             PLS_INTEGER;
        nblkt_end_off_pos            PLS_INTEGER;
        npcard_exp_dt_pos            PLS_INTEGER;
        npcard_exp_tz_pos            PLS_INTEGER;
        npcard_exp_off_pos           PLS_INTEGER;
        nline_can_dt_pos             PLS_INTEGER;
        nline_can_tz_pos             PLS_INTEGER;
        nline_can_off_pos            PLS_INTEGER;
        nexprn_dt_pos                PLS_INTEGER;
        nexprn_tz_pos                PLS_INTEGER;
        nexprn_off_pos               PLS_INTEGER;
        nship_need_dt_pos            PLS_INTEGER;
        nship_need_tz_pos            PLS_INTEGER;
        nship_need_off_pos           PLS_INTEGER;
        nship_prom_dt_pos            PLS_INTEGER;
        nship_prom_tz_pos            PLS_INTEGER;
        nship_prom_off_pos           PLS_INTEGER;
        nship_accept_dt_pos          PLS_INTEGER;
        nship_accept_tz_pos          PLS_INTEGER;
        nship_accept_off_pos         PLS_INTEGER;
        nshp_can_dt_pos              PLS_INTEGER;
        nshp_can_tz_pos              PLS_INTEGER;
        nshp_can_off_pos             PLS_INTEGER;
        nshp_strt_dt_pos             PLS_INTEGER;
        nshp_strt_tz_pos             PLS_INTEGER;
        nshp_strt_off_pos            PLS_INTEGER;
        nshp_end_dt_pos              PLS_INTEGER;
        nshp_end_tz_pos              PLS_INTEGER;
        nshp_end_off_pos             PLS_INTEGER;
        -- Timezone enhancement

        nshp_uom_pos                 NUMBER;
        nline_uom_pos                NUMBER;
        init_msg_list                VARCHAR2 (20);
        simulate                     VARCHAR2 (20);
        validation_level             VARCHAR2 (20);
        commt                        VARCHAR2 (20);
        return_status                VARCHAR2 (20);
        msg_count                    NUMBER;
        msg_data                     VARCHAR2 (2000);                                                         -- 3650215

        cline_part_number            VARCHAR2 (80);
        cline_part_attrib_category   VARCHAR2 (80);

        -- bug 6511409
        cline_part_attribute1        mtl_item_flexfields.attribute1%TYPE;
        cline_part_attribute2        mtl_item_flexfields.attribute2%TYPE;
        cline_part_attribute3        mtl_item_flexfields.attribute3%TYPE;
        cline_part_attribute4        mtl_item_flexfields.attribute4%TYPE;
        cline_part_attribute5        mtl_item_flexfields.attribute5%TYPE;
        cline_part_attribute6        mtl_item_flexfields.attribute6%TYPE;
        cline_part_attribute7        mtl_item_flexfields.attribute7%TYPE;
        cline_part_attribute8        mtl_item_flexfields.attribute8%TYPE;
        cline_part_attribute9        mtl_item_flexfields.attribute9%TYPE;
        cline_part_attribute10       mtl_item_flexfields.attribute10%TYPE;
        cline_part_attribute11       mtl_item_flexfields.attribute11%TYPE;
        cline_part_attribute12       mtl_item_flexfields.attribute12%TYPE;
        cline_part_attribute13       mtl_item_flexfields.attribute13%TYPE;
        cline_part_attribute14       mtl_item_flexfields.attribute14%TYPE;
        cline_part_attribute15       mtl_item_flexfields.attribute15%TYPE;

        d_dummy_date                 DATE;
        counter                      NUMBER;
        cancel_flag_value            VARCHAR2 (1);
        cancel_date_value            DATE;
        imap_id                      NUMBER;
        c_header_common_key_name     VARCHAR2 (40);
        c_line_common_key_name       VARCHAR2 (40);
        c_shipment_key_name          VARCHAR2 (40);
        n_header_common_key_pos      NUMBER;
        n_line_common_key_pos        NUMBER;
        n_ship_common_key_pos        NUMBER;

        fail_convert_to_ext          EXCEPTION;

        CURSOR c_org_id (p_line_id NUMBER)
        IS
            SELECT DISTINCT ship_to_organization_id
              FROM po_line_locations
             WHERE po_line_id = p_line_id;
    BEGIN
        ec_debug.push ('ECE_POCO_TRANSACTION.POPULATE_POCO_TRX');
        ec_debug.pl (3, 'cCommunication_Method: ', ccommunication_method);
        ec_debug.pl (3, 'cTransaction_Type: ', ctransaction_type);
        ec_debug.pl (3, 'iOutput_width: ', ioutput_width);
        ec_debug.pl (3, 'dTransaction_date: ', dtransaction_date);
        ec_debug.pl (3, 'iRun_id: ', irun_id);
        ec_debug.pl (3, 'cHeader_Interface: ', cheader_interface);
        ec_debug.pl (3, 'cLine_Interface: ', cline_interface);
        ec_debug.pl (3, 'cShipment_Interface: ', cshipment_interface);
        ec_debug.pl (3, 'cProject_Interface: ', cproject_interface);
        ec_debug.pl (3, 'cRevised_Date_From: ', crevised_date_from);
        ec_debug.pl (3, 'cRevised_Date_To: ', crevised_date_to);
        ec_debug.pl (3, 'cSupplier_Name: ', csupplier_name);
        ec_debug.pl (3, 'cSupplier_Site: ', csupplier_site);
        ec_debug.pl (3, 'cDocument_Type: ', cdocument_type);
        ec_debug.pl (3, 'cPO_Number_From: ', cpo_number_from);
        ec_debug.pl (3, 'cPO_Number_To: ', cpo_number_to);

        xprogress := 'POCOB-10-1000';

        BEGIN
            SELECT inventory_organization_id INTO norganization_id FROM financials_system_parameters;
        EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
                ec_debug.pl (0
                            ,'EC'
                            ,'ECE_NO_ROW_SELECTED'
                            ,'PROGRESS_LEVEL'
                            ,xprogress
                            ,'INFO'
                            ,'INVENTORY ORGANIZATION ID'
                            ,'TABLE_NAME'
                            ,'FINANCIALS_SYSTERM_PARAMETERS');
        END;

        ec_debug.pl (3, 'nOrganization_ID: ', norganization_id);

        -- Let's See if Project Accounting is Installed
        xprogress := 'POCOB-10-1001';
        v_project_acct_installed :=
            fnd_installation.get_app_info (v_project_acct_short_name
                                          ,                                                                 -- i.e. 'PA'
                                           v_project_acct_status
                                          ,                                                  -- 'I' means it's installed
                                           v_project_acct_industry
                                          ,v_project_acct_schema);

        v_project_acct_status := NVL (v_project_acct_status, 'X');
        ec_debug.pl (3, 'v_project_acct_status: ', v_project_acct_status);
        ec_debug.pl (3, 'v_project_acct_industry: ', v_project_acct_industry);
        ec_debug.pl (3, 'v_project_acct_schema: ', v_project_acct_schema);

        -- Get Profile Option Values for Attachments
        xprogress := 'POCOB-10-1002';
        fnd_profile.get ('ECE_' || ctransaction_type || '_HEAD_ATT', v_header_att_enabled);
        fnd_profile.get ('ECE_' || ctransaction_type || '_LINE_ATT', v_line_att_enabled);
        fnd_profile.get ('ECE_' || ctransaction_type || '_MITEM_ATT', v_mitem_att_enabled);
        fnd_profile.get ('ECE_' || ctransaction_type || '_IITEM_ATT', v_iitem_att_enabled);
        fnd_profile.get ('ECE_' || ctransaction_type || '_SHIP_ATT', v_ship_att_enabled);
        fnd_profile.get ('ECE_' || ctransaction_type || '_ATT_SEG_SIZE', n_att_seg_size);

        -- Check to see if any attachments are enabled
        xprogress := 'POCOB-10-1004';

        IF    NVL (v_header_att_enabled, 'N') = 'Y'
           OR NVL (v_mitem_att_enabled, 'N') = 'Y'
           OR NVL (v_iitem_att_enabled, 'N') = 'Y'
           OR NVL (v_ship_att_enabled, 'N') = 'Y'
        THEN
            v_att_enabled := 'Y';
        END IF;

        IF v_att_enabled = 'Y'
        THEN
            BEGIN
                IF    n_att_seg_size < 1
                   OR n_att_seg_size > ece_poo_transaction.g_max_att_seg_size
                   OR n_att_seg_size IS NULL
                THEN
                    RAISE INVALID_NUMBER;
                END IF;
            EXCEPTION
                WHEN VALUE_ERROR OR INVALID_NUMBER
                THEN
                    ec_debug.pl (0
                                ,'EC'
                                ,'ECE_INVALID_SEGMENT_NUM'
                                ,'SEGMENT_VALUE'
                                ,n_att_seg_size
                                ,'SEGMENT_DEFAULT'
                                ,ece_poo_transaction.g_default_att_seg_size);
                    n_att_seg_size := ece_poo_transaction.g_default_att_seg_size;
            END;
        END IF;

        ec_debug.pl (3, 'v_header_att_enabled: ', v_header_att_enabled);
        ec_debug.pl (3, 'v_line_att_enabled: ', v_line_att_enabled);
        ec_debug.pl (3, 'v_mitem_att_enabled: ', v_mitem_att_enabled);
        ec_debug.pl (3, 'v_iitem_att_enabled: ', v_iitem_att_enabled);
        ec_debug.pl (3, 'v_ship_att_enabled: ', v_ship_att_enabled);
        ec_debug.pl (3, 'v_att_enabled: ', v_att_enabled);
        ec_debug.pl (3, 'n_att_seg_size: ', n_att_seg_size);

        xprogress := 'POCOB-10-1010';
        ece_flatfile_pvt.init_table (ctransaction_type
                                    ,cheader_interface
                                    ,NULL
                                    ,FALSE
                                    ,l_header_tbl
                                    ,l_key_tbl);

        xprogress := 'POCOB-10-1020';
        l_key_tbl := l_header_tbl;

        xprogress := 'POCOB-10-1025';
        --iKey_count := l_header_tbl.COUNT;
        --ec_debug.pl(3,'iKey_count: ',iKey_count );

        xprogress := 'POCOB-10-1030';
        ece_flatfile_pvt.init_table (ctransaction_type
                                    ,cline_interface
                                    ,NULL
                                    ,TRUE
                                    ,l_line_tbl
                                    ,l_key_tbl);

        xprogress := 'POCOB-10-1040';
        ece_flatfile_pvt.init_table (ctransaction_type
                                    ,cshipment_interface
                                    ,NULL
                                    ,TRUE
                                    ,l_shipment_tbl
                                    ,l_key_tbl);

        -- ****************************************************************************
        -- Here, I am building the SELECT, FROM, and WHERE  clauses for the dynamic SQL
        -- call. The ece_extract_utils_pub.select_clause uses the EDI data dictionary
        -- for the build.
        -- ****************************************************************************
        BEGIN
            SELECT map_id
              INTO imap_id
              FROM ece_mappings
             WHERE map_code = 'EC_' || ctransaction_type || '_FF';
        EXCEPTION
            WHEN OTHERS
            THEN
                NULL;
        END;

        xprogress := 'POCOB-10-1050';
        ece_extract_utils_pub.select_clause (ctransaction_type
                                            ,ccommunication_method
                                            ,cheader_interface
                                            ,l_header_tbl
                                            ,cheader_select
                                            ,cheader_from
                                            ,cheader_where);

        BEGIN
            SELECT eit.key_column_name
              INTO c_header_common_key_name
              FROM ece_interface_tables eit
             WHERE     eit.transaction_type = ctransaction_type
                   AND eit.interface_table_name = cheader_interface
                   AND eit.map_id = imap_id;
        EXCEPTION
            WHEN OTHERS
            THEN
                NULL;
        END;

        xprogress := 'POCOB-10-1060';
        ece_extract_utils_pub.select_clause (ctransaction_type
                                            ,ccommunication_method
                                            ,cline_interface
                                            ,l_line_tbl
                                            ,cline_select
                                            ,cline_from
                                            ,cline_where);

        BEGIN
            SELECT eit.key_column_name
              INTO c_line_common_key_name
              FROM ece_interface_tables eit
             WHERE     eit.transaction_type = ctransaction_type
                   AND eit.interface_table_name = cline_interface
                   AND eit.map_id = imap_id;
        EXCEPTION
            WHEN OTHERS
            THEN
                NULL;
        END;

        xprogress := 'POCOB-10-1070';
        ece_extract_utils_pub.select_clause (ctransaction_type
                                            ,ccommunication_method
                                            ,cshipment_interface
                                            ,l_shipment_tbl
                                            ,cshipment_select
                                            ,cshipment_from
                                            ,cshipment_where);

        BEGIN
            SELECT eit.key_column_name
              INTO c_shipment_key_name
              FROM ece_interface_tables eit
             WHERE     eit.transaction_type = ctransaction_type
                   AND eit.interface_table_name = cshipment_interface
                   AND eit.map_id = imap_id;
        EXCEPTION
            WHEN OTHERS
            THEN
                NULL;
        END;

        -- **************************************************************************
        -- Here, I am customizing the WHERE clause to join the Interface tables together.
        -- i.e. Headers -- Lines -- Line Details
        --
        -- Select   Data1, Data2, Data3...........
        -- From  Header_View
        -- Where A.Transaction_Record_ID = D.Transaction_Record_ID (+)
        -- and   B.Transaction_Record_ID = E.Transaction_Record_ID (+)
        -- and   C.Transaction_Record_ID = F.Transaction_Record_ID (+)
        -- ******* (Customization should be added here) ********
        -- and   A.Communication_Method = 'EDI'
        -- and   A.xxx = B.xxx   ........
        -- and   B.yyy = C.yyy   .......
        -- **************************************************************************
        -- **************************************************************************
        -- :po_header_id is a place holder for foreign key value.
        -- A PL/SQL table (list of values) will be used to store data.
        -- Procedure ece_flatfile.Find_pos will be used to locate the specific
        -- data value in the PL/SQL table.
        -- dbms_sql (Native Oracle db functions that come with every Oracle Apps)
        -- dbms_sql.bind_variable will be used to assign data value to :transaction_id.
        --
        -- Let's use the above example:
        --
        -- 1. Execute dynamic SQL 1 for headers (A) data
        --    Get value of A.xxx (foreign key to B)
        --
        -- 2. bind value A.xxx to variable B.xxx
        --
        -- 3. Execute dynamic SQL 2 for lines (B) data
        --    Get value of B.yyy (foreigh key to C)
        --
        -- 4. bind value B.yyy to variable C.yyy
        --
        -- 5. Execute dynamic SQL 3 for line_details (C) data
        -- **************************************************************************
        xprogress := 'POCOB-10-1080';
        cheader_where := cheader_where || ' communication_method = ' || ':cComm_Method';

        xprogress := 'POCOB-10-1090';

        IF crevised_date_from IS NOT NULL
        THEN
            cheader_where := cheader_where || ' AND ' || ' revised_date >= ' || ':cRevised_Dt_From';
        END IF;

        xprogress := 'POCOB-10-1100';

        IF crevised_date_to IS NOT NULL
        THEN
            cheader_where := cheader_where || ' AND ' || ' revised_date <= ' || ':cRevised_Dt_To';
        END IF;

        xprogress := 'POCOB-10-1110';

        IF csupplier_name IS NOT NULL
        THEN
            cheader_where := cheader_where || ' AND ' || ' supplier_number = ' || ':cSuppl_Name';
        END IF;

        xprogress := 'POCOB-10-1120';

        IF csupplier_site IS NOT NULL
        THEN
            cheader_where := cheader_where || ' AND ' || ' vendor_site_id = ' || ':cSuppl_Site';
        END IF;

        xprogress := 'POCOB-10-1130';

        IF cdocument_type IS NOT NULL
        THEN
            cheader_where := cheader_where || ' AND ' || ' document_type = ' || ':cDoc_Type';
        END IF;

        xprogress := 'POCOB-10-1140';

        IF cpo_number_from IS NOT NULL
        THEN
            cheader_where := cheader_where || ' AND ' || ' po_number >= ' || ':cPO_Num_From';
        END IF;

        xprogress := 'POCOB-10-1150';

        IF cpo_number_to IS NOT NULL
        THEN
            cheader_where := cheader_where || ' AND ' || ' po_number <= ' || ':cPO_Num_To';
        END IF;

        xprogress := 'POCOB-10-1160';
        cheader_where := cheader_where || ' ORDER BY po_number, por_release_num';

        xprogress := 'POCOB-10-1170';
        cline_where :=
               cline_where
            || ' ece_poco_lines_v.po_header_id = :po_header_id AND'
            || ' ece_poco_lines_v.por_release_num = :por_release_num '
            || ' ORDER BY line_num';

        xprogress := 'POCOB-10-1180';
        cshipment_where :=
               cshipment_where
            || ' ece_poco_shipments_v.po_header_id = :po_header_id AND'
            || ' ece_poco_shipments_v.po_line_id = :po_line_id AND'
            || ' ece_poco_shipments_v.por_release_id = :por_release_id'
            || --4645680          ' ece_poo_shipments_v.por_release_id = :por_release_id'   ||
               ' ORDER BY shipment_number';                                                                    --2823215
        -- 3957851
        --                   ' ece_poco_shipments_v.por_release_id = :por_release_id AND'   ||
        --                   ' ((ece_poco_shipments_v.por_release_id = 0) OR'               ||
        --                   ' (ece_poco_shipments_v.por_release_id <> 0 AND'               ||
        --                   ' ece_poco_shipments_v.shipment_number = :shipment_number))'   ||
        --                   ' ORDER BY shipment_number';   --2823215

        xprogress := 'POCOB-10-1190';
        cheader_select := cheader_select || cheader_from || cheader_where;
        ec_debug.pl (3, 'cHeader_select: ', cheader_select);

        cline_select := cline_select || cline_from || cline_where;
        ec_debug.pl (3, 'cLine_select: ', cline_select);

        cshipment_select := cshipment_select || cshipment_from || cshipment_where;
        ec_debug.pl (3, 'cShipment_select: ', cshipment_select);

        -- ***************************************************
        -- ***   Get data setup for the dynamic SQL call.
        -- ***   Open a cursor for each of the SELECT call
        -- ***   This tells the database to reserve spaces
        -- ***   for the data returned by the SQL statement
        -- ***************************************************
        xprogress := 'POCOB-10-1200';
        header_sel_c := DBMS_SQL.open_cursor;

        xprogress := 'POCOB-10-1210';
        line_sel_c := DBMS_SQL.open_cursor;

        xprogress := 'POCOB-10-1220';
        shipment_sel_c := DBMS_SQL.open_cursor;

        -- ***************************************************
        -- Parse each of the SELECT statement
        -- so the database understands the command
        -- ***************************************************
        xprogress := 'POCOB-10-1230';

        BEGIN
            DBMS_SQL.parse (header_sel_c, cheader_select, DBMS_SQL.native);
        EXCEPTION
            WHEN OTHERS
            THEN
                ece_error_handling_pvt.print_parse_error (DBMS_SQL.last_error_position, cheader_select);
                app_exception.raise_exception;
        END;

        xprogress := 'POCOB-10-1240';

        BEGIN
            DBMS_SQL.parse (line_sel_c, cline_select, DBMS_SQL.native);
        EXCEPTION
            WHEN OTHERS
            THEN
                ece_error_handling_pvt.print_parse_error (DBMS_SQL.last_error_position, cline_select);
                app_exception.raise_exception;
        END;

        xprogress := 'POCOB-10-1250';

        BEGIN
            DBMS_SQL.parse (shipment_sel_c, cshipment_select, DBMS_SQL.native);
        EXCEPTION
            WHEN OTHERS
            THEN
                ece_error_handling_pvt.print_parse_error (DBMS_SQL.last_error_position, cshipment_select);
                app_exception.raise_exception;
        END;

        -- ************
        -- set counter
        -- ************
        xprogress := 'POCOB-10-1260';
        iheader_count := l_header_tbl.COUNT;
        ec_debug.pl (3, 'iHeader_count: ', iheader_count);

        xprogress := 'POCOB-10-1270';
        iline_count := l_line_tbl.COUNT;
        ec_debug.pl (3, 'iLine_count: ', iline_count);

        xprogress := 'POCOB-10-1280';
        ishipment_count := l_shipment_tbl.COUNT;
        ec_debug.pl (3, 'iShipment_count: ', ishipment_count);

        -- ******************************************************
        --  Define TYPE for every columns in the SELECT statement
        --  For each piece of the data returns, we need to tell
        --  the database what type of information it will be.
        --  e.g. ID is NUMBER, due_date is DATE
        --  However, for simplicity, we will convert
        --  everything to varchar2.
        -- ******************************************************
        xprogress := 'POCOB-10-1290';
        ece_flatfile_pvt.define_interface_column_type (header_sel_c
                                                      ,cheader_select
                                                      ,ece_extract_utils_pub.g_maxcolwidth
                                                      ,l_header_tbl);

        xprogress := 'POCOB-10-1300';
        ece_flatfile_pvt.define_interface_column_type (line_sel_c
                                                      ,cline_select
                                                      ,ece_extract_utils_pub.g_maxcolwidth
                                                      ,l_line_tbl);

        xprogress := 'POCOB-10-1310';
        ece_flatfile_pvt.define_interface_column_type (shipment_sel_c
                                                      ,cshipment_select
                                                      ,ece_extract_utils_pub.g_maxcolwidth
                                                      ,l_shipment_tbl);

        -- **************************************************************
        -- ***  The following is custom tailored for this transaction
        -- ***  It finds the values and use them in the WHERE clause to
        -- ***  join tables together.
        -- **************************************************************
        -- ***************************************************
        -- To complete the Line SELECT statement,
        --  we will need values for the join condition.
        -- ***************************************************
        -- Header Level Positions
        xprogress := 'POCOB-10-1320';
        ece_extract_utils_pub.find_pos (l_header_tbl, ece_extract_utils_pub.g_transaction_date, n_trx_date_pos);
        ec_debug.pl (3, 'n_trx_date_pos: ', n_trx_date_pos);

        xprogress := 'POCOB-10-1330';
        ece_extract_utils_pub.find_pos (l_header_tbl, 'PO_HEADER_ID', nheader_key_pos);
        ec_debug.pl (3, 'nHeader_key_pos: ', nheader_key_pos);

        xprogress := 'POCOB-10-1340';
        ece_extract_utils_pub.find_pos (l_header_tbl, 'DOCUMENT_TYPE', ndocument_type_pos);
        ec_debug.pl (3, 'nDocument_type_pos: ', ndocument_type_pos);

        xprogress := 'POCOB-10-1350';
        ece_extract_utils_pub.find_pos (l_header_tbl, 'PO_NUMBER', npo_number_pos);
        ec_debug.pl (3, 'nPO_Number_pos: ', npo_number_pos);

        xprogress := 'POCOB-10-1360';
        ece_extract_utils_pub.find_pos (l_header_tbl, 'PO_TYPE', npo_type_pos);
        ec_debug.pl (3, 'nPO_Type_pos: ', npo_type_pos);

        xprogress := 'POCOB-10-1370';
        ece_extract_utils_pub.find_pos (l_header_tbl, 'POR_RELEASE_NUM', nrelease_num_pos);
        ec_debug.pl (3, 'nRelease_num_pos: ', nrelease_num_pos);

        xprogress := 'POCOB-10-1380';
        ece_extract_utils_pub.find_pos (l_header_tbl, 'POR_RELEASE_ID', nrelease_id_pos);
        ec_debug.pl (3, 'nRelease_id_pos: ', nrelease_id_pos);

        xprogress := 'POCOB-10-1381';
        ece_extract_utils_pub.find_pos (l_header_tbl, 'CANCEL_FLAG', nheader_cancel_flag_pos);

        xprogress := 'POOB-10-1382';
        ece_flatfile_pvt.find_pos (l_header_tbl, ece_flatfile_pvt.g_translator_code, ntrans_code_pos);         --2823215

        xprogress := 'POCOB-10-1283';
        ece_flatfile_pvt.find_pos (l_header_tbl, 'PO_CANCELLED_DATE', nheader_cancel_date_pos);                --2823215

        -- Line Level Positions
        xprogress := 'POCOB-10-1390';
        ece_extract_utils_pub.find_pos (l_line_tbl, 'PO_LINE_LOCATION_ID', nline_location_id_pos);
        ec_debug.pl (3, 'nLine_Location_ID_pos: ', nline_location_id_pos);

        xprogress := 'POCOB-10-1400';
        ece_extract_utils_pub.find_pos (l_line_tbl, 'LINE_NUM', nline_num_pos);
        ec_debug.pl (3, 'nLine_num_pos: ', nline_num_pos);

        xprogress := 'POCOB-10-1402';
        ece_extract_utils_pub.find_pos (l_line_tbl, 'PO_LINE_ID', nline_key_pos);
        ec_debug.pl (3, 'nLine_key_pos: ', nline_key_pos);

        xprogress := 'POCOB-10-1404';
        ece_extract_utils_pub.find_pos (l_line_tbl, 'ITEM_ID', nitem_id_pos);
        ec_debug.pl (3, 'nItem_id_pos: ', nitem_id_pos);

        xprogress := 'POCOB-10-1405';
        ece_extract_utils_pub.find_pos (l_line_tbl, 'CANCEL_FLAG', nline_cancel_flag_pos);

        xprogress := 'POCOB-10-1406';
        ece_extract_utils_pub.find_pos (l_line_tbl, 'UOM_CODE', nline_uom_code_pos);

        xprogress := 'POCOB-10-1407';
        ece_extract_utils_pub.find_pos (l_line_tbl, 'LP_ATTRIBUTE_CATEGORY', nlp_att_cat_pos);

        xprogress := 'POCOB-10-1408';
        ece_extract_utils_pub.find_pos (l_line_tbl, 'LP_ATTRIBUTE1', nlp_att1_pos);

        xprogress := 'POCOB-10-1409';
        ece_extract_utils_pub.find_pos (l_line_tbl, 'LP_ATTRIBUTE2', nlp_att2_pos);

        xprogress := 'POCOB-10-1410';
        ece_extract_utils_pub.find_pos (l_line_tbl, 'LP_ATTRIBUTE3', nlp_att3_pos);

        xprogress := 'POCOB-10-1411';
        ece_extract_utils_pub.find_pos (l_line_tbl, 'LP_ATTRIBUTE4', nlp_att4_pos);

        xprogress := 'POCOB-10-1412';
        ece_extract_utils_pub.find_pos (l_line_tbl, 'LP_ATTRIBUTE5', nlp_att5_pos);

        xprogress := 'POCOB-10-1413';
        ece_extract_utils_pub.find_pos (l_line_tbl, 'LP_ATTRIBUTE6', nlp_att6_pos);

        xprogress := 'POCOB-10-1414';
        ece_extract_utils_pub.find_pos (l_line_tbl, 'LP_ATTRIBUTE7', nlp_att7_pos);

        xprogress := 'POCOB-10-1415';
        ece_extract_utils_pub.find_pos (l_line_tbl, 'LP_ATTRIBUTE8', nlp_att8_pos);

        xprogress := 'POCOB-10-1416';
        ece_extract_utils_pub.find_pos (l_line_tbl, 'LP_ATTRIBUTE9', nlp_att9_pos);

        xprogress := 'POCOB-10-1417';
        ece_extract_utils_pub.find_pos (l_line_tbl, 'LP_ATTRIBUTE10', nlp_att10_pos);

        xprogress := 'POCOB-10-1418';
        ece_extract_utils_pub.find_pos (l_line_tbl, 'LP_ATTRIBUTE11', nlp_att11_pos);

        xprogress := 'POCOB-10-1419';
        ece_extract_utils_pub.find_pos (l_line_tbl, 'LP_ATTRIBUTE12', nlp_att12_pos);

        xprogress := 'POCOB-10-1420';
        ece_extract_utils_pub.find_pos (l_line_tbl, 'LP_ATTRIBUTE13', nlp_att13_pos);

        xprogress := 'POCOB-10-1421';
        ece_extract_utils_pub.find_pos (l_line_tbl, 'LP_ATTRIBUTE14', nlp_att14_pos);

        xprogress := 'POCOB-10-1422';
        ece_extract_utils_pub.find_pos (l_line_tbl, 'LP_ATTRIBUTE15', nlp_att15_pos);

        -- Shipment Level Positions
        xprogress := 'POCOB-10-1406';
        ece_extract_utils_pub.find_pos (l_shipment_tbl, 'LINE_LOCATION_ID', nship_line_location_id_pos);
        ec_debug.pl (3, 'nShip_Line_Location_ID_pos: ', nship_line_location_id_pos);

        xprogress := 'POCOB-10-1407';
        ece_extract_utils_pub.find_pos (l_shipment_tbl, 'QUANTITY_PENDING', nquantity_pending_pos);

        xprogress := 'POCOB-10-1408';
        ece_extract_utils_pub.find_pos (l_shipment_tbl, 'CANCELLED_FLAG', nshipment_cancel_flag_pos);

        --2823215

        xprogress := 'POOB-10-1025';
        ece_extract_utils_pub.find_pos (l_shipment_tbl, 'POR_RELEASE_NUM', nship_release_num_pos);

        xprogress := 'POOB-10-1026';
        ece_extract_utils_pub.find_pos (l_shipment_tbl, 'UOM_CODE', nline_location_uom_pos);

        xprogress := 'POOB-10-1427';
        ece_extract_utils_pub.find_pos (l_shipment_tbl, 'SHIPMENT_NUMBER', nshipment_key_pos);

        xprogress := 'POOB-10-1428';
        ece_flatfile_pvt.find_pos (l_shipment_tbl, 'SHIP_TO_CUSTOMER_NAME', nst_cust_name_pos);

        xprogress := 'POOB-10-1429';
        ece_flatfile_pvt.find_pos (l_shipment_tbl, 'SHIP_TO_CONTACT_NAME', nst_cont_name_pos);

        xprogress := 'POOB-10-1430';
        ece_flatfile_pvt.find_pos (l_shipment_tbl, 'SHIP_TO_CONTACT_PHONE', nst_cont_phone_pos);

        xprogress := 'POOB-10-1431';
        ece_flatfile_pvt.find_pos (l_shipment_tbl, 'SHIP_TO_CONTACT_FAX', nst_cont_fax_pos);

        xprogress := 'POOB-10-1432';
        ece_flatfile_pvt.find_pos (l_shipment_tbl, 'SHIP_TO_CONTACT_EMAIL', nst_cont_email_pos);

        xprogress := 'POOB-10-1433';
        ece_flatfile_pvt.find_pos (l_shipment_tbl, 'SHIPPING_INSTRUCTIONS', nshipping_instruct_pos);

        xprogress := 'POOB-10-1434';
        ece_flatfile_pvt.find_pos (l_shipment_tbl, 'PACKING_INSTRUCTIONS', npacking_instruct_pos);

        xprogress := 'POOB-10-1435';
        ece_flatfile_pvt.find_pos (l_shipment_tbl, 'SHIPPING_METHOD', nshipping_method_pos);

        xprogress := 'POOB-10-1437';
        ece_flatfile_pvt.find_pos (l_shipment_tbl, 'CUSTOMER_PO_NUMBER', ncust_po_num_pos);

        xprogress := 'POOB-10-1438';
        ece_flatfile_pvt.find_pos (l_shipment_tbl, 'CUSTOMER_PO_LINE_NUM', ncust_po_line_num_pos);

        xprogress := 'POOB-10-1439';
        ece_flatfile_pvt.find_pos (l_shipment_tbl, 'CUSTOMER_PO_SHIPMENT_NUM', ncust_po_ship_num_pos);

        xprogress := 'POOB-10-1440';
        ece_flatfile_pvt.find_pos (l_shipment_tbl, 'CUSTOMER_ITEM_DESCRIPTION', ncust_prod_desc_pos);

        xprogress := 'POOB-10-1441';
        ece_flatfile_pvt.find_pos (l_shipment_tbl, 'DELIVER_TO_LOCATION', ndeliv_cust_loc_pos);

        xprogress := 'POOB-10-1442';
        ece_flatfile_pvt.find_pos (l_shipment_tbl, 'DELIVER_TO_CUSTOMER_NAME', ndeliv_cust_name_pos);

        xprogress := 'POOB-10-1443';
        ece_flatfile_pvt.find_pos (l_shipment_tbl, 'DELIVER_TO_CONTACT_NAME', ndeliv_cont_name_pos);

        xprogress := 'POOB-10-1444';
        ece_flatfile_pvt.find_pos (l_shipment_tbl, 'DELIVER_TO_CONTACT_PHONE', ndeliv_cont_phone_pos);

        xprogress := 'POOB-10-1445';
        ece_flatfile_pvt.find_pos (l_shipment_tbl, 'DELIVER_TO_CONTACT_FAX', ndeliv_cont_fax_pos);

        xprogress := 'POOB-10-1446';
        ece_flatfile_pvt.find_pos (l_shipment_tbl, 'DELIVER_TO_CUSTOMER_ADDRESS', ndeliv_cust_addr_pos);

        xprogress := 'POOB-10-1447';
        ece_flatfile_pvt.find_pos (l_shipment_tbl, 'DELIVER_TO_CONTACT_EMAIL', ndeliv_cont_email_pos);

        -- 2823215
        xprogress := 'POCOB-10-1448';
        ece_flatfile_pvt.find_pos (l_line_tbl, 'UOM_CODE', nline_uom_pos);

        xprogress := 'POCOB-10-1449';
        ece_flatfile_pvt.find_pos (l_shipment_tbl, 'CODE_UOM', nshp_uom_pos);
        -- 2412921 begin

        xprogress := 'POCOB-10-1410';
        ece_extract_utils_pub.find_pos (l_header_tbl, 'CANCEL_DATE', ncancel_date_pos);
        ec_debug.pl (3, 'nCancel_Date_pos -> ', ncancel_date_pos);

        xprogress := 'POCOB-10-1412';
        ece_extract_utils_pub.find_pos (l_line_tbl, 'CANCEL_DATE', ncancel_date_posl);
        ec_debug.pl (3, 'nCancel_Date_posl -> ', ncancel_date_posl);
        -- 2412921 end

        -- Timezone enhancement
        xprogress := 'POCOB-TZ-1000';
        ece_flatfile_pvt.find_pos (l_header_tbl, 'RELEASE_DATE', nrel_date_pos);

        xprogress := 'POCOB-TZ-1001';
        ece_flatfile_pvt.find_pos (l_header_tbl, 'RELEASE_DT_TZ_CODE', nrel_dt_tz_pos);

        xprogress := 'POCOB-TZ-1002';
        ece_flatfile_pvt.find_pos (l_header_tbl, 'RELEASE_DT_OFF', nrel_dt_off_pos);

        xprogress := 'POCOB-TZ-1003';
        ece_flatfile_pvt.find_pos (l_header_tbl, 'CREATION_DATE', ncrtn_date_pos);

        xprogress := 'POCOB-TZ-1004';
        ece_flatfile_pvt.find_pos (l_header_tbl, 'CREATION_DT_TZ_CODE', ncrtn_dt_tz_pos);

        xprogress := 'POCOB-TZ-1005';
        ece_flatfile_pvt.find_pos (l_header_tbl, 'CREATION_DT_OFF', ncrtn_dt_off_pos);

        xprogress := 'POCOB-TZ-1006';
        ece_flatfile_pvt.find_pos (l_header_tbl, 'PO_REVISION_DATE', nrev_date_pos);

        xprogress := 'POCOB-TZ-1007';
        ece_flatfile_pvt.find_pos (l_header_tbl, 'REVISION_DT_TZ_CODE', nrev_dt_tz_pos);

        xprogress := 'POCOB-TZ-1008';
        ece_flatfile_pvt.find_pos (l_header_tbl, 'REVISION_DT_OFF', nrev_dt_off_pos);

        xprogress := 'POCOB-TZ-1009';
        ece_flatfile_pvt.find_pos (l_header_tbl, 'PO_ACCEPTANCE_DUE_BY_DATE', nacc_due_dt_pos);

        xprogress := 'POCOB-TZ-1010';
        ece_flatfile_pvt.find_pos (l_header_tbl, 'PO_ACCEPT_DUE_TZ_CODE', nacc_due_tz_pos);

        xprogress := 'POCOB-TZ-1011';
        ece_flatfile_pvt.find_pos (l_header_tbl, 'PO_ACCEPT_DUE_OFF', nacc_due_off_pos);

        xprogress := 'POCOB-TZ-1012';
        ece_flatfile_pvt.find_pos (l_header_tbl, 'BLANKET_START_DATE', nblkt_srt_dt_pos);

        xprogress := 'POCOB-TZ-1013';
        ece_flatfile_pvt.find_pos (l_header_tbl, 'BLANKET_SRT_DT_TZ_CODE', nblkt_srt_tz_pos);

        xprogress := 'POCOB-TZ-1014';
        ece_flatfile_pvt.find_pos (l_header_tbl, 'BLANKET_SRT_DT_OFF', nblkt_srt_off_pos);

        xprogress := 'POCOB-TZ-1015';
        ece_flatfile_pvt.find_pos (l_header_tbl, 'BLANKET_END_DATE', nblkt_end_dt_pos);

        xprogress := 'POCOB-TZ-1016';
        ece_flatfile_pvt.find_pos (l_header_tbl, 'BLANKET_END_DT_TZ_CODE', nblkt_end_tz_pos);

        xprogress := 'POCOB-TZ-1017';
        ece_flatfile_pvt.find_pos (l_header_tbl, 'BLANKET_END_DT_OFF', nblkt_end_off_pos);

        xprogress := 'POCOB-TZ-1018';
        ece_flatfile_pvt.find_pos (l_header_tbl, 'PCARD_EXPIRATION_DATE', npcard_exp_dt_pos);

        xprogress := 'POCOB-TZ-1019';
        ece_flatfile_pvt.find_pos (l_header_tbl, 'PCARD_EXPRN_DT_TZ_CODE', npcard_exp_tz_pos);

        xprogress := 'POCOB-TZ-1020';
        ece_flatfile_pvt.find_pos (l_header_tbl, 'PCARD_EXPRN_DT_OFF', npcard_exp_off_pos);

        xprogress := 'POCOB-TZ-1021';
        ece_flatfile_pvt.find_pos (l_line_tbl, 'LINE_CANCELLED_DATE', nline_can_dt_pos);

        xprogress := 'POCOB-TZ-1022';
        ece_flatfile_pvt.find_pos (l_line_tbl, 'LINE_CANCEL_DT_TZ_CODE', nline_can_tz_pos);

        xprogress := 'POCOB-TZ-1023';
        ece_flatfile_pvt.find_pos (l_line_tbl, 'LINE_CANCEL_DT_OFF', nline_can_off_pos);

        xprogress := 'POCOB-TZ-1024';
        ece_flatfile_pvt.find_pos (l_line_tbl, 'EXPIRATION_DATE', nexprn_dt_pos);

        xprogress := 'POCOB-TZ-1025';
        ece_flatfile_pvt.find_pos (l_line_tbl, 'EXPIRATION_DT_TZ_CODE', nexprn_tz_pos);

        xprogress := 'POCOB-TZ-1026';
        ece_flatfile_pvt.find_pos (l_line_tbl, 'EXPIRATION_DT_OFF', nexprn_off_pos);

        xprogress := 'POCOB-TZ-1027';
        ece_flatfile_pvt.find_pos (l_shipment_tbl, 'SHIPMENT_NEED_BY_DATE', nship_need_dt_pos);

        xprogress := 'POCOB-TZ-1028';
        ece_flatfile_pvt.find_pos (l_shipment_tbl, 'SHIPMENT_NEED_DT_TZ_CODE', nship_need_tz_pos);

        xprogress := 'POCOB-TZ-1029';
        ece_flatfile_pvt.find_pos (l_shipment_tbl, 'SHIPMENT_NEED_DT_OFF', nship_need_off_pos);

        xprogress := 'POCOB-TZ-1030';
        ece_flatfile_pvt.find_pos (l_shipment_tbl, 'SHIPMENT_PROMISED_DATE', nship_prom_dt_pos);

        xprogress := 'POCOB-TZ-1031';
        ece_flatfile_pvt.find_pos (l_shipment_tbl, 'SHIPMENT_PROM_DT_TZ_CODE', nship_prom_tz_pos);

        xprogress := 'POCOB-TZ-1032';
        ece_flatfile_pvt.find_pos (l_shipment_tbl, 'SHIPMENT_PROM_DT_OFF', nship_prom_off_pos);

        xprogress := 'POCOB-TZ-1033';
        ece_flatfile_pvt.find_pos (l_shipment_tbl, 'SHIPMENT_LAST_ACCEPTABLE_DATE', nship_accept_dt_pos);

        xprogress := 'POCOB-TZ-1034';
        ece_flatfile_pvt.find_pos (l_shipment_tbl, 'SHIPMENT_LAST_ACC_DT_TZ_CODE', nship_accept_tz_pos);

        xprogress := 'POCOB-TZ-1035';
        ece_flatfile_pvt.find_pos (l_shipment_tbl, 'SHIPMENT_LAST_ACC_DT_OFF', nship_accept_off_pos);

        xprogress := 'POCOB-TZ-1036';
        ece_flatfile_pvt.find_pos (l_shipment_tbl, 'CANCELLED_DATE', nshp_can_dt_pos);

        xprogress := 'POCOB-TZ-1037';
        ece_flatfile_pvt.find_pos (l_shipment_tbl, 'CANCEL_DT_TZ_CODE', nshp_can_tz_pos);

        xprogress := 'POCOB-TZ-1038';
        ece_flatfile_pvt.find_pos (l_shipment_tbl, 'CANCEL_DT_OFF', nshp_can_off_pos);

        xprogress := 'POCOB-TZ-1039';
        ece_flatfile_pvt.find_pos (l_shipment_tbl, 'START_DATE', nshp_strt_dt_pos);

        xprogress := 'POCOB-TZ-1040';
        ece_flatfile_pvt.find_pos (l_shipment_tbl, 'START_DT_TZ_CODE', nshp_strt_tz_pos);

        xprogress := 'POCOB-TZ-1041';
        ece_flatfile_pvt.find_pos (l_shipment_tbl, 'START_DT_OFF', nshp_strt_off_pos);

        xprogress := 'POCOB-TZ-1042';
        ece_flatfile_pvt.find_pos (l_shipment_tbl, 'END_DATE', nshp_end_dt_pos);

        xprogress := 'POCOB-TZ-1043';
        ece_flatfile_pvt.find_pos (l_shipment_tbl, 'END_DT_TZ_CODE', nshp_end_tz_pos);

        xprogress := 'POCOB-TZ-1044';
        ece_flatfile_pvt.find_pos (l_shipment_tbl, 'END_DT_OFF', nshp_end_off_pos);
        -- Timezone enhancement
        xprogress := 'POCOB-09-1400';
        ece_flatfile_pvt.find_pos (l_header_tbl, c_header_common_key_name, n_header_common_key_pos);

        xprogress := 'POCOB-09-1401';
        ece_flatfile_pvt.find_pos (l_line_tbl, c_line_common_key_name, n_line_common_key_pos);

        xprogress := 'POCOB-09-1402';
        ece_flatfile_pvt.find_pos (l_shipment_tbl, c_shipment_key_name, n_ship_common_key_pos);

        xprogress := 'POCOB-10-1413';
        DBMS_SQL.bind_variable (header_sel_c, 'cComm_Method', ccommunication_method);

        IF crevised_date_from IS NOT NULL
        THEN
            DBMS_SQL.bind_variable (header_sel_c, 'cRevised_Dt_From', crevised_date_from);
        END IF;

        IF crevised_date_to IS NOT NULL
        THEN
            DBMS_SQL.bind_variable (header_sel_c, 'cRevised_Dt_To', crevised_date_to);
        END IF;

        IF csupplier_name IS NOT NULL
        THEN
            DBMS_SQL.bind_variable (header_sel_c, 'cSuppl_Name', csupplier_name);
        END IF;

        IF csupplier_site IS NOT NULL
        THEN
            DBMS_SQL.bind_variable (header_sel_c, 'cSuppl_Site', csupplier_site);
        END IF;

        IF cdocument_type IS NOT NULL
        THEN
            DBMS_SQL.bind_variable (header_sel_c, 'cDoc_Type', cdocument_type);
        END IF;

        IF cpo_number_from IS NOT NULL
        THEN
            DBMS_SQL.bind_variable (header_sel_c, 'cPO_Num_From', cpo_number_from);
        END IF;

        IF cpo_number_to IS NOT NULL
        THEN
            DBMS_SQL.bind_variable (header_sel_c, 'cPO_Num_To', cpo_number_to);
        END IF;

        -- EXECUTE the SELECT statement
        xprogress := 'POCOB-10-1414';
        dummy := DBMS_SQL.execute (header_sel_c);

        -- ***************************************************
        -- The model is:
        -- HEADER - LINE - SHIPMENT ...
        -- With data for each HEADER line, populate the header
        -- interfacetable then get all LINES that belongs
        -- to the HEADER. Then get all
        -- SHIPMENTS that belongs to the LINE.
        -- ***************************************************
        xprogress := 'POCOB-10-1410';

        WHILE DBMS_SQL.fetch_rows (header_sel_c) > 0
        LOOP                                                                                                   -- Header
            IF (NOT UTL_FILE.is_open (ece_poo_transaction.ufile_type))
            THEN
                ece_poo_transaction.ufile_type :=
                    UTL_FILE.fopen (i_path
                                   ,i_filename
                                   ,'W'
                                   ,32767);
            END IF;

            counter := 0;
            -- **************************************
            --  store internal values in pl/sql table
            -- **************************************
            xprogress := 'POCOB-10-1420';
            ece_flatfile_pvt.assign_column_value_to_tbl (header_sel_c
                                                        ,0
                                                        ,l_header_tbl
                                                        ,l_key_tbl);

            -- ***************************************************
            --  also need to populate transaction_date and run_id
            -- ***************************************************
            xprogress := 'POCOB-10-1430';
            l_header_tbl (n_trx_date_pos).VALUE := TO_CHAR (dtransaction_date, 'YYYYMMDD HH24MISS');

            --  The application specific feedback logic begins here.
            xprogress := 'POCOB-10-1440';

            BEGIN
                /* Bug 2396394 Added the document type CONTRACT in SQL below */

                SELECT DECODE (l_header_tbl (ndocument_type_pos).VALUE
                              ,'BLANKET', 'NB'
                              ,'STANDARD', 'NS'
                              ,'PLANNED', 'NP'
                              ,'RELEASE', 'NR'
                              ,'BLANKET RELEASE', 'NR'
                              ,'CONTRACT', 'NC'
                              ,'NR')
                  INTO l_document_type
                  FROM DUAL;
            EXCEPTION
                WHEN NO_DATA_FOUND
                THEN
                    ec_debug.pl (0
                                ,'EC'
                                ,'ECE_DECODE_FAILED'
                                ,'PROGRESS_LEVEL'
                                ,xprogress
                                ,'CODE'
                                ,l_header_tbl (ndocument_type_pos).VALUE);
            END;

            ec_debug.pl (3, 'l_document_type: ', l_document_type);

            xprogress := 'POCOB-10-1450';
            ece_poo_transaction.update_po (l_document_type
                                          ,l_header_tbl (npo_number_pos).VALUE
                                          ,l_header_tbl (npo_type_pos).VALUE
                                          ,l_header_tbl (nrelease_num_pos).VALUE);
            xprogress := 'POCOB-TZ-1500';
            ece_timezone_api.get_server_timezone_details (
                TO_DATE (l_header_tbl (nrel_date_pos).VALUE, 'YYYYMMDD HH24MISS')
               ,l_header_tbl (nrel_dt_off_pos).VALUE
               ,l_header_tbl (nrel_dt_tz_pos).VALUE);

            xprogress := 'POCOB-TZ-1510';

            ece_timezone_api.get_server_timezone_details (
                TO_DATE (l_header_tbl (ncrtn_date_pos).VALUE, 'YYYYMMDD HH24MISS')
               ,l_header_tbl (ncrtn_dt_off_pos).VALUE
               ,l_header_tbl (ncrtn_dt_tz_pos).VALUE);

            xprogress := 'POCOB-TZ-1520';

            ece_timezone_api.get_server_timezone_details (
                TO_DATE (l_header_tbl (nrev_date_pos).VALUE, 'YYYYMMDD HH24MISS')
               ,l_header_tbl (nrev_dt_off_pos).VALUE
               ,l_header_tbl (nrev_dt_tz_pos).VALUE);

            xprogress := 'POCOB-TZ-1530';
            ece_timezone_api.get_server_timezone_details (
                TO_DATE (l_header_tbl (nacc_due_dt_pos).VALUE, 'YYYYMMDD HH24MISS')
               ,l_header_tbl (nacc_due_off_pos).VALUE
               ,l_header_tbl (nacc_due_tz_pos).VALUE);

            xprogress := 'POCOB-TZ-1540';
            ece_timezone_api.get_server_timezone_details (
                TO_DATE (l_header_tbl (nblkt_srt_dt_pos).VALUE, 'YYYYMMDD HH24MISS')
               ,l_header_tbl (nblkt_srt_off_pos).VALUE
               ,l_header_tbl (nblkt_srt_tz_pos).VALUE);

            xprogress := 'POCOB-TZ-1550';
            ece_timezone_api.get_server_timezone_details (
                TO_DATE (l_header_tbl (nblkt_end_dt_pos).VALUE, 'YYYYMMDD HH24MISS')
               ,l_header_tbl (nblkt_end_off_pos).VALUE
               ,l_header_tbl (nblkt_end_tz_pos).VALUE);

            xprogress := 'POCOB-TZ-1560';
            ece_timezone_api.get_server_timezone_details (
                TO_DATE (l_header_tbl (npcard_exp_dt_pos).VALUE, 'YYYYMMDD HH24MISS')
               ,l_header_tbl (npcard_exp_off_pos).VALUE
               ,l_header_tbl (npcard_exp_tz_pos).VALUE);

            -- pass the pl/sql table in for xref
            xprogress := 'POCOB-10-1460';
            ec_code_conversion_pvt.populate_plsql_tbl_with_extval (p_api_version_number   => 1.0
                                                                  ,p_init_msg_list        => init_msg_list
                                                                  ,p_simulate             => simulate
                                                                  ,p_commit               => commt
                                                                  ,p_validation_level     => validation_level
                                                                  ,p_return_status        => return_status
                                                                  ,p_msg_count            => msg_count
                                                                  ,p_msg_data             => msg_data
                                                                  ,p_key_tbl              => l_key_tbl
                                                                  ,p_tbl                  => l_header_tbl);

            -- ***************************
            -- insert into interface table
            -- ***************************
            xprogress := 'POCOB-10-1480';

            BEGIN
                SELECT ece_poco_header_s.NEXTVAL INTO l_header_fkey FROM DUAL;
            EXCEPTION
                WHEN NO_DATA_FOUND
                THEN
                    ec_debug.pl (0
                                ,'EC'
                                ,'ECE_GET_NEXT_SEQ_FAILED'
                                ,'PROGRESS_LEVEL'
                                ,xprogress
                                ,'SEQ'
                                ,'ECE_POCO_HEADER_S');
            END;

            ec_debug.pl (3, 'l_header_fkey: ', l_header_fkey);

            xprogress := 'POOB-10-1490';
            --2823215
            c_file_common_key := RPAD (SUBSTRB (NVL (l_header_tbl (ntrans_code_pos).VALUE, ' '), 1, 25), 25);

            xprogress := 'POOB-10-1491';
            c_file_common_key :=
                   c_file_common_key
                || RPAD (SUBSTRB (NVL (l_header_tbl (n_header_common_key_pos).VALUE, ' '), 1, 22), 22)
                || RPAD (' ', 22)
                || RPAD (' ', 22);

            ec_debug.pl (3, 'c_file_common_key: ', c_file_common_key);
            /* ece_extract_utils_pub.insert_into_interface_tbl(
                iRun_id,
                cTransaction_Type,
                cCommunication_Method,
                cHeader_Interface,
                l_header_tbl,
                l_header_fkey); */

            -- Now update the columns values of which have been obtained
            -- thru the procedure calls.

            -- ********************************************************
            -- Call custom program stub to populate the extension table
            -- ********************************************************
            xprogress := 'POCOB-10-1500';
            ece_poco_x.populate_ext_header (l_header_fkey, l_header_tbl);

            -- 2823215
            ece_poo_transaction.write_to_file (ctransaction_type
                                              ,ccommunication_method
                                              ,cheader_interface
                                              ,l_header_tbl
                                              ,ioutput_width
                                              ,irun_id
                                              ,c_file_common_key
                                              ,l_header_fkey);
            -- 2823215
            -- Header Level Attachment Handler
            xprogress := 'POCOB-10-1501';

            IF v_header_att_enabled = 'Y'
            THEN
                xprogress := 'POCOB-10-1502';

                IF l_document_type = 'NR'
                THEN                                                                         -- If this is a Release PO.
                    xprogress := 'POCOB-10-1503';
                    v_entity_name := 'PO_RELEASES';
                    v_pk1_value := l_header_tbl (nrelease_id_pos).VALUE;
                    ec_debug.pl (3, 'release_id: ', l_header_tbl (nrelease_id_pos).VALUE);
                ELSE                                                                     -- If this is a non-Release PO.
                    xprogress := 'POCOB-10-1504';
                    v_entity_name := 'PO_HEADERS';
                    v_pk1_value := l_header_tbl (nheader_key_pos).VALUE;
                    ec_debug.pl (3, 'po_header_id: ', l_header_tbl (nheader_key_pos).VALUE);
                END IF;

                xprogress := 'POCOB-10-1505';
                ece_poo_transaction.populate_text_attachment (ccommunication_method
                                                             ,ctransaction_type
                                                             ,irun_id
                                                             ,2
                                                             ,3
                                                             ,catt_header_interface
                                                             ,catt_detail_interface
                                                             ,v_entity_name
                                                             ,'VENDOR'
                                                             ,v_pk1_value
                                                             ,ece_poo_transaction.c_any_value
                                                             ,                                            -- BUG:5367903
                                                              ece_poo_transaction.c_any_value
                                                             ,ece_poo_transaction.c_any_value
                                                             ,ece_poo_transaction.c_any_value
                                                             ,n_att_seg_size
                                                             ,l_key_tbl
                                                             ,c_file_common_key
                                                             ,l_hdr_att_hdr_tbl
                                                             ,l_hdr_att_dtl_tbl
                                                             ,iatt_hdr_pos);                                  -- 2823215
            END IF;

            -- ***************************************************
            -- From Header data, we can assign values to
            -- place holders (foreign keys) in Line_select and
            -- Line_detail_Select
            -- set values into binding variables
            -- ***************************************************

            -- use the following bind_variable feature as you see fit.
            xprogress := 'POCOB-10-1510';
            DBMS_SQL.bind_variable (line_sel_c, 'po_header_id', l_header_tbl (nheader_key_pos).VALUE);

            xprogress := 'POCOB-10-1515';
            DBMS_SQL.bind_variable (line_sel_c, 'por_release_num', l_header_tbl (nrelease_num_pos).VALUE);

            xprogress := 'POCOB-10-1520';
            DBMS_SQL.bind_variable (shipment_sel_c, 'po_header_id', l_header_tbl (nheader_key_pos).VALUE);

            xprogress := 'POOB-10-1525';
            DBMS_SQL.bind_variable (shipment_sel_c, 'por_release_id', l_header_tbl (nrelease_id_pos).VALUE);   --2823215

            xprogress := 'POCOB-10-1530';
            dummy := DBMS_SQL.execute (line_sel_c);

            -- *********************
            -- Line Level Loop Starts Here
            -- *********************
            xprogress := 'POCOB-10-1540';

            WHILE DBMS_SQL.fetch_rows (line_sel_c) > 0
            LOOP                                                                                                --- Line
                -- ****************************
                -- store values in pl/sql table
                -- ****************************
                xprogress := 'POCOB-10-1550';
                ece_flatfile_pvt.assign_column_value_to_tbl (line_sel_c
                                                            ,iheader_count
                                                            ,l_line_tbl
                                                            ,l_key_tbl);

                -- The following procedure gets the part number for the
                -- item ID returned
                xprogress := 'POCOB-10-1640';
                ece_inventory.get_item_number (l_line_tbl (nitem_id_pos).VALUE
                                              ,norganization_id
                                              ,cline_part_number
                                              ,cline_part_attrib_category
                                              ,cline_part_attribute1
                                              ,cline_part_attribute2
                                              ,cline_part_attribute3
                                              ,cline_part_attribute4
                                              ,cline_part_attribute5
                                              ,cline_part_attribute6
                                              ,cline_part_attribute7
                                              ,cline_part_attribute8
                                              ,cline_part_attribute9
                                              ,cline_part_attribute10
                                              ,cline_part_attribute11
                                              ,cline_part_attribute12
                                              ,cline_part_attribute13
                                              ,cline_part_attribute14
                                              ,cline_part_attribute15);

                BEGIN
                    SELECT uom_code
                      INTO l_line_tbl (nline_uom_pos).VALUE
                      FROM mtl_units_of_measure
                     WHERE unit_of_measure = l_line_tbl (nline_uom_code_pos).VALUE;
                EXCEPTION
                    WHEN OTHERS
                    THEN
                        NULL;
                END;

                xprogress := 'POCOB-TZ-2500';
                ece_timezone_api.get_server_timezone_details (
                    TO_DATE (l_line_tbl (nline_can_dt_pos).VALUE, 'YYYYMMDD HH24MISS')
                   ,l_line_tbl (nline_can_off_pos).VALUE
                   ,l_line_tbl (nline_can_tz_pos).VALUE);

                xprogress := 'POCOB-TZ-2510';

                ece_timezone_api.get_server_timezone_details (
                    TO_DATE (l_line_tbl (nexprn_dt_pos).VALUE, 'YYYYMMDD HH24MISS')
                   ,l_line_tbl (nexprn_off_pos).VALUE
                   ,l_line_tbl (nexprn_tz_pos).VALUE);
                -- pass the pl/sql table in for xref
                xprogress := 'POCOB-10-1570';
                ec_code_conversion_pvt.populate_plsql_tbl_with_extval (p_api_version_number   => 1.0
                                                                      ,p_init_msg_list        => init_msg_list
                                                                      ,p_simulate             => simulate
                                                                      ,p_commit               => commt
                                                                      ,p_validation_level     => validation_level
                                                                      ,p_return_status        => return_status
                                                                      ,p_msg_count            => msg_count
                                                                      ,p_msg_data             => msg_data
                                                                      ,p_key_tbl              => l_key_tbl
                                                                      ,p_tbl                  => l_line_tbl);

                xprogress := 'POCOB-10-1590';

                BEGIN
                    SELECT ece_poco_line_s.NEXTVAL INTO l_line_fkey FROM DUAL;
                EXCEPTION
                    WHEN NO_DATA_FOUND
                    THEN
                        ec_debug.pl (0
                                    ,'EC'
                                    ,'ECE_GET_NEXT_SEQ_FAILED'
                                    ,'PROGRESS_LEVEL'
                                    ,xprogress
                                    ,'SEQ'
                                    ,'ECE_POCO_LINE_S');
                END;

                ec_debug.pl (3, 'l_line_fkey: ', l_line_fkey);

                -- Insert into Interface Table
                /*         xProgress := 'POCOB-10-1600';
                         ece_extract_utils_pub.insert_into_interface_tbl(
                            iRun_id,
                            cTransaction_Type,
                            cCommunication_Method,
                            cLine_Interface,
                            l_line_tbl,
                            l_line_fkey); */
                -- 2823215

                IF ec_debug.g_debug_level = 3
                THEN
                    ec_debug.pl (3, 'cline_part_number: ', cline_part_number);
                    ec_debug.pl (3, 'cline_part_attrib_category: ', cline_part_attrib_category);
                    ec_debug.pl (3, 'cline_part_attribute1: ', cline_part_attribute1);
                    ec_debug.pl (3, 'cline_part_attribute2: ', cline_part_attribute2);
                    ec_debug.pl (3, 'cline_part_attribute3: ', cline_part_attribute3);
                    ec_debug.pl (3, 'cline_part_attribute4: ', cline_part_attribute4);
                    ec_debug.pl (3, 'cline_part_attribute5: ', cline_part_attribute5);
                    ec_debug.pl (3, 'cline_part_attribute6: ', cline_part_attribute6);
                    ec_debug.pl (3, 'cline_part_attribute7: ', cline_part_attribute7);
                    ec_debug.pl (3, 'cline_part_attribute8: ', cline_part_attribute8);
                    ec_debug.pl (3, 'cline_part_attribute9: ', cline_part_attribute9);
                    ec_debug.pl (3, 'cline_part_attribute10: ', cline_part_attribute10);
                    ec_debug.pl (3, 'cline_part_attribute11: ', cline_part_attribute11);
                    ec_debug.pl (3, 'cline_part_attribute12: ', cline_part_attribute12);
                    ec_debug.pl (3, 'cline_part_attribute13: ', cline_part_attribute13);
                    ec_debug.pl (3, 'cline_part_attribute14: ', cline_part_attribute14);
                    ec_debug.pl (3, 'cline_part_attribute15: ', cline_part_attribute15);
                END IF;

                xprogress := 'POOB-10-1591';
                -- 2823215
                l_line_tbl (nlp_att_cat_pos).VALUE := cline_part_attrib_category;
                l_line_tbl (nlp_att1_pos).VALUE := cline_part_attribute1;
                l_line_tbl (nlp_att2_pos).VALUE := cline_part_attribute2;
                l_line_tbl (nlp_att3_pos).VALUE := cline_part_attribute3;
                l_line_tbl (nlp_att4_pos).VALUE := cline_part_attribute4;
                l_line_tbl (nlp_att5_pos).VALUE := cline_part_attribute5;
                l_line_tbl (nlp_att6_pos).VALUE := cline_part_attribute6;
                l_line_tbl (nlp_att7_pos).VALUE := cline_part_attribute7;
                l_line_tbl (nlp_att8_pos).VALUE := cline_part_attribute8;
                l_line_tbl (nlp_att9_pos).VALUE := cline_part_attribute9;
                l_line_tbl (nlp_att10_pos).VALUE := cline_part_attribute10;
                l_line_tbl (nlp_att11_pos).VALUE := cline_part_attribute11;
                l_line_tbl (nlp_att12_pos).VALUE := cline_part_attribute12;
                l_line_tbl (nlp_att13_pos).VALUE := cline_part_attribute13;
                l_line_tbl (nlp_att14_pos).VALUE := cline_part_attribute14;
                l_line_tbl (nlp_att15_pos).VALUE := cline_part_attribute15;

                xprogress := 'POOB-10-1600';
                c_file_common_key :=
                       RPAD (SUBSTRB (NVL (l_header_tbl (ntrans_code_pos).VALUE, ' '), 1, 25), 25)
                    || RPAD (SUBSTRB (NVL (l_header_tbl (n_header_common_key_pos).VALUE, ' '), 1, 22), 22)
                    || RPAD (SUBSTRB (NVL (l_line_tbl (n_line_common_key_pos).VALUE, ' '), 1, 22), 22)
                    || RPAD (' ', 22);

                IF ec_debug.g_debug_level = 3
                THEN
                    ec_debug.pl (3, 'c_file_common_key: ', c_file_common_key);
                END IF;

                xprogress := 'POOB-10-1621';

                -- 2823215

                -- Now update the columns values of which have been obtained thru the procedure calls.
                /*       xProgress := 'POCOB-10-1610';
                       UPDATE   ece_po_interface_lines
                       SET      line_part_number          = cline_part_number,
                                line_part_attrib_category = cline_part_attrib_category,
                                line_part_attribute1      = cline_part_attribute1,
                                line_part_attribute2      = cline_part_attribute2,
                                line_part_attribute3      = cline_part_attribute3,
                                line_part_attribute4      = cline_part_attribute4,
                                line_part_attribute5      = cline_part_attribute5,
                                line_part_attribute6      = cline_part_attribute6,
                                line_part_attribute7      = cline_part_attribute7,
                                line_part_attribute8      = cline_part_attribute8,
                                line_part_attribute9      = cline_part_attribute9,
                                line_part_attribute10     = cline_part_attribute10,
                                line_part_attribute11     = cline_part_attribute11,
                                line_part_attribute12     = cline_part_attribute12,
                                line_part_attribute13     = cline_part_attribute13,
                                line_part_attribute14     = cline_part_attribute14,
                                line_part_attribute15     = cline_part_attribute15
                       WHERE    transaction_record_id     = l_line_fkey;  */

                --2412921 begin
                ec_debug.pl (3, 'document type ', l_header_tbl (ndocument_type_pos).VALUE);

                IF l_header_tbl (ndocument_type_pos).VALUE NOT IN ('RELEASE', 'BLANKET RELEASE')
                THEN
                    BEGIN
                        cancel_flag_value := l_header_tbl (nheader_cancel_flag_pos).VALUE;
                        ec_debug.pl (3, 'cancel_flag_value->', cancel_flag_value);

                        IF cancel_flag_value = 'Y'
                        THEN
                            cancel_date_value := TO_DATE (l_line_tbl (ncancel_date_posl).VALUE, 'YYYYMMDD HH24MISS');
                            ec_debug.pl (3, 'cancel_date_value->', l_line_tbl (ncancel_date_posl).VALUE);
                        END IF;

                        IF cancel_date_value IS NOT NULL
                        THEN
                            counter := counter + 1;
                        END IF;

                        ec_debug.pl (3, 'counter -->', counter);

                        /* If Header is already updated with cancel date from line, then no need
                        to update again */
                        IF counter = 1
                        THEN
                            /* update ece_po_interface_headers set
                            po_cancelled_date = cancel_date_value
                            where po_header_id = l_header_tbl(nHeader_key_pos).value; */
                            l_header_tbl (nheader_cancel_date_pos).VALUE := cancel_date_value;
                        END IF;
                    EXCEPTION
                        WHEN NO_DATA_FOUND
                        THEN
                            NULL;
                        WHEN OTHERS
                        THEN
                            NULL;
                    END;
                END IF;

                -- 2823215
                xprogress := 'POCOB-10-1620';
                ece_poco_x.populate_ext_line (l_line_fkey, l_line_tbl);

                ece_poo_transaction.write_to_file (ctransaction_type
                                                  ,ccommunication_method
                                                  ,cline_interface
                                                  ,l_line_tbl
                                                  ,ioutput_width
                                                  ,irun_id
                                                  ,c_file_common_key
                                                  ,l_line_fkey);

                -- 2823215
                -- 2412921 end

                IF SQL%NOTFOUND
                THEN
                    ec_debug.pl (0
                                ,'EC'
                                ,'ECE_NO_ROW_UPDATED'
                                ,'PROGRESS_LEVEL'
                                ,xprogress
                                ,'INFO'
                                ,'LINE PART'
                                ,'TABLE_NAME'
                                ,'ECE_PO_INTERFACE_LINES');
                END IF;

                -- ********************************************************
                -- Call custom program stub to populate the extension table
                -- ********************************************************

                /***************************
                *  Line Level Attachments  *
                ***************************/
                IF v_line_att_enabled = 'Y'
                THEN
                    xprogress := 'POCOB-10-1621';
                    /* Bug 2235872  IF l_document_type = 'NR' THEN -- If this is a Release PO.
                                    xProgress := 'POCOB-10-1622';
                                    v_entity_name := 'PO_SHIPMENTS';
                                    v_pk1_value := l_line_tbl(nLine_Location_ID_pos).value; -- LINE_LOCATION_ID
                                    ec_debug.pl(3,'PO_LINE_LOCATION_ID: ',l_line_tbl(nLine_Location_ID_pos).value);
                                 ELSE -- If this is a non-Release PO.  */
                    xprogress := 'POCOB-10-1623';
                    v_entity_name := 'PO_LINES';
                    v_pk1_value := l_line_tbl (nline_key_pos).VALUE;                                          -- LINE_ID
                    ec_debug.pl (3, 'PO_LINE_ID: ', l_line_tbl (nline_key_pos).VALUE);
                    --  END IF;

                    xprogress := 'POCOB-10-1624';
                    ece_poo_transaction.populate_text_attachment (ccommunication_method
                                                                 ,ctransaction_type
                                                                 ,irun_id
                                                                 ,5
                                                                 ,6
                                                                 ,catt_header_interface
                                                                 ,catt_detail_interface
                                                                 ,v_entity_name
                                                                 ,'VENDOR'
                                                                 ,v_pk1_value
                                                                 ,ece_poo_transaction.c_any_value
                                                                 ,                                        -- BUG:5367903
                                                                  ece_poo_transaction.c_any_value
                                                                 ,ece_poo_transaction.c_any_value
                                                                 ,ece_poo_transaction.c_any_value
                                                                 ,n_att_seg_size
                                                                 ,l_key_tbl
                                                                 ,c_file_common_key
                                                                 ,l_ln_att_hdr_tbl
                                                                 ,l_ln_att_dtl_tbl
                                                                 ,iatt_ln_pos);
                END IF;

                /***************************
                *  Master Org Attachments  *
                ***************************/
                IF v_mitem_att_enabled = 'Y'
                THEN
                    xprogress := 'POCOB-10-1625';
                    v_entity_name := 'MTL_SYSTEM_ITEMS';
                    v_pk1_value := norganization_id;                                          -- Master Inventory Org ID
                    ec_debug.pl (3, 'Master Org ID: ', v_pk1_value);

                    v_pk2_value := l_line_tbl (nitem_id_pos).VALUE;                                           -- Item ID
                    ec_debug.pl (3, 'Item ID: ', v_pk2_value);

                    xprogress := 'POCOB-10-1626';
                    ece_poo_transaction.populate_text_attachment (ccommunication_method
                                                                 ,ctransaction_type
                                                                 ,irun_id
                                                                 ,7
                                                                 ,8
                                                                 ,catt_header_interface
                                                                 ,catt_detail_interface
                                                                 ,v_entity_name
                                                                 ,'VENDOR'
                                                                 ,v_pk1_value
                                                                 ,v_pk2_value
                                                                 ,NULL
                                                                 ,NULL
                                                                 ,NULL
                                                                 ,n_att_seg_size
                                                                 ,l_key_tbl
                                                                 ,c_file_common_key
                                                                 ,l_mi_att_hdr_tbl
                                                                 ,l_mi_att_dtl_tbl
                                                                 ,iatt_mi_pos);
                END IF;

                /******************************
                *  Inventory Org Attachments  *
                ******************************/
                IF v_iitem_att_enabled = 'Y'
                THEN
                    xprogress := 'POCOB-10-1627';
                    v_entity_name := 'MTL_SYSTEM_ITEMS';
                    v_pk2_value := l_line_tbl (nitem_id_pos).VALUE;                                           -- Item ID
                    ec_debug.pl (3, 'Item ID: ', v_pk2_value);

                    xprogress := 'POCOB-10-1628';

                    FOR v_org_id IN c_org_id (l_line_tbl (nline_key_pos).VALUE)
                    LOOP                                                                  -- Value passed is the Line ID
                        IF v_org_id.ship_to_organization_id <> norganization_id
                        THEN                                  -- Only do this if it is not the same as the Master Org ID
                            v_pk1_value := v_org_id.ship_to_organization_id;
                            ec_debug.pl (3, 'Inventory Org ID: ', v_pk1_value);

                            xprogress := 'POCOB-10-1626';
                            ece_poo_transaction.populate_text_attachment (ccommunication_method
                                                                         ,ctransaction_type
                                                                         ,irun_id
                                                                         ,9
                                                                         ,10
                                                                         ,catt_header_interface
                                                                         ,catt_detail_interface
                                                                         ,v_entity_name
                                                                         ,'VENDOR'
                                                                         ,v_pk1_value
                                                                         ,v_pk2_value
                                                                         ,NULL
                                                                         ,NULL
                                                                         ,NULL
                                                                         ,n_att_seg_size
                                                                         ,l_key_tbl
                                                                         ,c_file_common_key
                                                                         ,l_msi_att_hdr_tbl
                                                                         ,l_msi_att_dtl_tbl
                                                                         ,iatt_msi_pos);
                        END IF;
                    END LOOP;
                END IF;

                -- **********************
                -- set LINE_NUMBER values
                -- **********************
                --  Removed based on bug:3957851
                --               xProgress := 'POOB-10-1627';
                --              dbms_sql.bind_variable(shipment_sel_c,'shipment_number',l_line_tbl(nLine_num_pos).value);

                xprogress := 'POCOB-10-1630';
                DBMS_SQL.bind_variable (shipment_sel_c, 'po_line_id', l_line_tbl (nline_key_pos).VALUE);

                xprogress := 'POCOB-10-1640';
                dummy := DBMS_SQL.execute (shipment_sel_c);

                -- *************************
                -- Shipment loop starts here
                -- *************************
                xprogress := 'POCOB-10-1650';

                WHILE DBMS_SQL.fetch_rows (shipment_sel_c) > 0
                LOOP                                                                                        --- Shipment
                    -- ****************************
                    -- store values in pl/sql table
                    -- ****************************
                    xprogress := 'POCOB-10-1660';
                    ece_flatfile_pvt.assign_column_value_to_tbl (shipment_sel_c
                                                                ,iheader_count + iline_count
                                                                ,l_shipment_tbl
                                                                ,l_key_tbl);

                    -- Calculate Pending Quantity
                    xprogress := 'POCOB-10-1665';
                    l_shipment_tbl (nquantity_pending_pos).VALUE :=
                        NVL (rcv_quantities_s.get_pending_qty (l_shipment_tbl (nship_line_location_id_pos).VALUE), 0);
                    ec_debug.pl (3
                                ,'l_Shipment_tbl(nQuantity_pending_pos).value: '
                                ,l_shipment_tbl (nquantity_pending_pos).VALUE);

                    xprogress := 'POCOB-10-1670';

                    BEGIN
                        SELECT ece_poco_shipment_s.NEXTVAL INTO l_shipment_fkey FROM DUAL;
                    EXCEPTION
                        WHEN NO_DATA_FOUND
                        THEN
                            ec_debug.pl (0
                                        ,'EC'
                                        ,'ECE_GET_NEXT_SEQ_FAILED'
                                        ,'PROGRESS_LEVEL'
                                        ,xprogress
                                        ,'SEQ'
                                        ,'ECE_POCO_SHIPMENT_S');
                    END;

                    ec_debug.pl (3, 'l_Shipment_fkey: ', l_shipment_fkey);

                    l_shipment_tbl (nline_location_uom_pos).VALUE := l_line_tbl (nline_uom_code_pos).VALUE; -- bug 2823215

                    l_shipment_tbl (nship_release_num_pos).VALUE := l_header_tbl (nrelease_num_pos).VALUE; -- bug 2823215

                    l_shipment_tbl (nshp_uom_pos).VALUE := l_line_tbl (nline_uom_pos).VALUE;

                    xprogress := 'POCOB-TZ-3500';
                    ece_timezone_api.get_server_timezone_details (
                        TO_DATE (l_shipment_tbl (nship_need_dt_pos).VALUE, 'YYYYMMDD HH24MISS')
                       ,l_shipment_tbl (nship_need_off_pos).VALUE
                       ,l_shipment_tbl (nship_need_tz_pos).VALUE);

                    xprogress := 'POCOB-TZ-3510';
                    ece_timezone_api.get_server_timezone_details (
                        TO_DATE (l_shipment_tbl (nship_prom_dt_pos).VALUE, 'YYYYMMDD HH24MISS')
                       ,l_shipment_tbl (nship_prom_off_pos).VALUE
                       ,l_shipment_tbl (nship_prom_tz_pos).VALUE);

                    xprogress := 'POCOB-TZ-3520';
                    ece_timezone_api.get_server_timezone_details (
                        TO_DATE (l_shipment_tbl (nship_accept_dt_pos).VALUE, 'YYYYMMDD HH24MISS')
                       ,l_shipment_tbl (nship_accept_off_pos).VALUE
                       ,l_shipment_tbl (nship_accept_tz_pos).VALUE);

                    xprogress := 'POCOB-TZ-3530';
                    ece_timezone_api.get_server_timezone_details (
                        TO_DATE (l_shipment_tbl (nshp_can_dt_pos).VALUE, 'YYYYMMDD HH24MISS')
                       ,l_shipment_tbl (nshp_can_off_pos).VALUE
                       ,l_shipment_tbl (nshp_can_tz_pos).VALUE);

                    xprogress := 'POCOB-TZ-3540';
                    ece_timezone_api.get_server_timezone_details (
                        TO_DATE (l_shipment_tbl (nshp_strt_dt_pos).VALUE, 'YYYYMMDD HH24MISS')
                       ,l_shipment_tbl (nshp_strt_off_pos).VALUE
                       ,l_shipment_tbl (nshp_strt_tz_pos).VALUE);

                    xprogress := 'POCOB-TZ-3550';
                    ece_timezone_api.get_server_timezone_details (
                        TO_DATE (l_shipment_tbl (nshp_end_dt_pos).VALUE, 'YYYYMMDD HH24MISS')
                       ,l_shipment_tbl (nshp_end_off_pos).VALUE
                       ,l_shipment_tbl (nshp_end_tz_pos).VALUE);

                    -- pass the pl/sql table in for xref
                    xprogress := 'POCOB-10-1680';
                    ec_code_conversion_pvt.populate_plsql_tbl_with_extval (p_api_version_number   => 1.0
                                                                          ,p_init_msg_list        => init_msg_list
                                                                          ,p_simulate             => simulate
                                                                          ,p_commit               => commt
                                                                          ,p_validation_level     => validation_level
                                                                          ,p_return_status        => return_status
                                                                          ,p_msg_count            => msg_count
                                                                          ,p_msg_data             => msg_data
                                                                          ,p_key_tbl              => l_key_tbl
                                                                          ,p_tbl                  => l_shipment_tbl);

                    xprogress := 'POCOB-10-1700';
                    /*  ece_extract_utils_pub.insert_into_interface_tbl(
                         iRun_id,
                         cTransaction_Type,
                         cCommunication_Method,
                         cShipment_Interface,
                         l_shipment_tbl,
                         l_shipment_fkey); */

                    xprogress := 'POOB-10-1690';
                    c_file_common_key :=
                           RPAD (SUBSTRB (NVL (l_header_tbl (ntrans_code_pos).VALUE, ' '), 1, 25), 25)
                        || RPAD (SUBSTRB (NVL (l_header_tbl (n_header_common_key_pos).VALUE, ' '), 1, 22), 22)
                        || RPAD (SUBSTRB (NVL (l_line_tbl (n_line_common_key_pos).VALUE, ' '), 1, 22), 22)
                        || RPAD (SUBSTRB (NVL (l_shipment_tbl (n_ship_common_key_pos).VALUE, ' '), 1, 22), 22);

                    IF ec_debug.g_debug_level = 3
                    THEN
                        ec_debug.pl (3, 'c_file_common_key: ', c_file_common_key);
                    END IF;

                    xprogress := 'POOB-10-1700';
                    ece_poco_x.populate_ext_shipment (l_shipment_fkey, l_shipment_tbl);

                    -- Drop shipment
                    xprogress := 'POCOB-10-1701';
                    v_drop_ship_flag :=
                        oe_drop_ship_grp.po_line_location_is_drop_ship (
                            l_shipment_tbl (nship_line_location_id_pos).VALUE);
                    xprogress := 'POCOB-10-1702';

                    IF ec_debug.g_debug_level = 3
                    THEN
                        ec_debug.pl (3, 'Drop Ship Flag:', v_drop_ship_flag);
                    END IF;

                    IF (v_drop_ship_flag IS NOT NULL)
                    THEN
                        v_header_cancel_flag := l_header_tbl (nheader_cancel_flag_pos).VALUE;
                        v_line_cancel_flag := l_line_tbl (nline_cancel_flag_pos).VALUE;
                        v_shipment_cancel_flag := l_shipment_tbl (nshipment_cancel_flag_pos).VALUE;

                        IF ec_debug.g_debug_level = 3
                        THEN
                            ec_debug.pl (3, 'v_header_cancel_flag:', v_header_cancel_flag);
                            ec_debug.pl (3, 'v_line_cancel_flag:', v_line_cancel_flag);
                            ec_debug.pl (3, 'v_shipment_cancel_flag:', v_shipment_cancel_flag);
                        END IF;

                        IF (    (NVL (v_header_cancel_flag, 'N') <> 'Y')
                            AND (NVL (v_line_cancel_flag, 'N') <> 'Y')
                            AND (NVL (v_shipment_cancel_flag, 'N') <> 'Y'))
                        THEN
                            xprogress := 'POCOB-10-1703';
                            oe_drop_ship_grp.get_order_line_info (1.0
                                                                 ,l_header_tbl (nheader_key_pos).VALUE
                                                                 ,l_line_tbl (nline_key_pos).VALUE
                                                                 ,l_shipment_tbl (nship_line_location_id_pos).VALUE
                                                                 ,l_header_tbl (nrelease_id_pos).VALUE
                                                                 ,2
                                                                 ,rec_order_line_info
                                                                 ,msg_data
                                                                 ,msg_count
                                                                 ,return_status);
                            xprogress := 'POCOB-10-1704';

                            IF ec_debug.g_debug_level = 3
                            THEN
                                ec_debug.pl (3, 'Ship to Customer Name:', rec_order_line_info.ship_to_customer_name);
                                ec_debug.pl (3, 'Ship to Contact Name:', rec_order_line_info.ship_to_contact_name);
                                ec_debug.pl (3, 'Ship to Contact Phone:', rec_order_line_info.ship_to_contact_phone);
                                ec_debug.pl (3, 'Ship to Contact Fax:', rec_order_line_info.ship_to_contact_fax);
                                ec_debug.pl (3, 'Ship to Contact Email:', rec_order_line_info.ship_to_contact_email);
                                ec_debug.pl (3, 'Shipping Instructions:', rec_order_line_info.shipping_instructions);
                                ec_debug.pl (3, 'Packing Instructions:', rec_order_line_info.packing_instructions);
                                ec_debug.pl (3, 'Shipping Method:', rec_order_line_info.shipping_method);
                                ec_debug.pl (3, 'Customer PO Number:', rec_order_line_info.customer_po_number);
                                ec_debug.pl (3
                                            ,'Customer PO Line Number:'
                                            ,rec_order_line_info.customer_po_line_number);
                                ec_debug.pl (3
                                            ,'Customer PO Shipment Num:'
                                            ,rec_order_line_info.customer_po_shipment_number);
                                ec_debug.pl (3
                                            ,'Customer Item Description:'
                                            ,rec_order_line_info.customer_product_description);
                                ec_debug.pl (3
                                            ,'Deliver to Location:'
                                            ,rec_order_line_info.deliver_to_customer_location);
                                ec_debug.pl (3
                                            ,'Deliver to Customer Name:'
                                            ,rec_order_line_info.deliver_to_customer_name);
                                ec_debug.pl (3
                                            ,'Deliver to Contact Name:'
                                            ,rec_order_line_info.deliver_to_customer_name);
                                ec_debug.pl (3
                                            ,'Deliver to Contact Phone:'
                                            ,rec_order_line_info.deliver_to_contact_phone);
                                ec_debug.pl (3, 'Deliver to Contact Fax:', rec_order_line_info.deliver_to_contact_fax);
                                ec_debug.pl (3
                                            ,'Deliver to Customer Address:'
                                            ,rec_order_line_info.deliver_to_customer_address);
                                ec_debug.pl (3
                                            ,'Deliver to Contact Email:'
                                            ,rec_order_line_info.deliver_to_contact_email);
                            END IF;

                            -- 2823215
                            l_shipment_tbl (nst_cust_name_pos).VALUE := rec_order_line_info.ship_to_customer_name;
                            l_shipment_tbl (nst_cont_name_pos).VALUE := rec_order_line_info.ship_to_contact_name;
                            l_shipment_tbl (nst_cont_phone_pos).VALUE := rec_order_line_info.ship_to_contact_phone;
                            l_shipment_tbl (nst_cont_fax_pos).VALUE := rec_order_line_info.ship_to_contact_fax;
                            l_shipment_tbl (nst_cont_email_pos).VALUE := rec_order_line_info.ship_to_contact_email;
                            l_shipment_tbl (nshipping_instruct_pos).VALUE := rec_order_line_info.shipping_instructions;
                            l_shipment_tbl (npacking_instruct_pos).VALUE := rec_order_line_info.packing_instructions;
                            l_shipment_tbl (nshipping_method_pos).VALUE := rec_order_line_info.shipping_method;
                            l_shipment_tbl (ncust_po_num_pos).VALUE := rec_order_line_info.customer_po_number;
                            l_shipment_tbl (ncust_po_line_num_pos).VALUE := rec_order_line_info.customer_po_line_number;
                            l_shipment_tbl (ncust_po_ship_num_pos).VALUE :=
                                rec_order_line_info.customer_po_shipment_number;
                            l_shipment_tbl (ncust_prod_desc_pos).VALUE :=
                                rec_order_line_info.customer_product_description;
                            l_shipment_tbl (ndeliv_cust_loc_pos).VALUE :=
                                rec_order_line_info.deliver_to_customer_location;
                            l_shipment_tbl (ndeliv_cust_name_pos).VALUE := rec_order_line_info.deliver_to_customer_name;
                            l_shipment_tbl (ndeliv_cont_name_pos).VALUE := rec_order_line_info.deliver_to_contact_name;
                            l_shipment_tbl (ndeliv_cont_phone_pos).VALUE := rec_order_line_info.deliver_to_contact_phone;
                            l_shipment_tbl (ndeliv_cont_fax_pos).VALUE := rec_order_line_info.deliver_to_contact_fax;
                            l_shipment_tbl (ndeliv_cust_addr_pos).VALUE :=
                                rec_order_line_info.deliver_to_customer_address;
                            l_shipment_tbl (ndeliv_cont_email_pos).VALUE := rec_order_line_info.deliver_to_contact_email;
                        -- 2823215
                        END IF;
                    END IF;

                    ece_poo_transaction.write_to_file (ctransaction_type
                                                      ,ccommunication_method
                                                      ,cshipment_interface
                                                      ,l_shipment_tbl
                                                      ,ioutput_width
                                                      ,irun_id
                                                      ,c_file_common_key
                                                      ,l_shipment_fkey);

                    -- ********************************************************
                    -- Call custom program stub to populate the extension table
                    -- ********************************************************
                    /*   xProgress := 'POCOB-10-1710';
                       ece_poco_x.populate_ext_shipment(l_shipment_fkey,l_shipment_tbl);
                     */
                    -- Shipment Level Attachment Handler
                    IF v_ship_att_enabled = 'Y'
                    THEN
                        v_entity_name := 'PO_SHIPMENTS';
                        v_pk1_value := l_shipment_tbl (nship_line_location_id_pos).VALUE;
                        ec_debug.pl (3
                                    ,'Ship Level Line Location ID: '
                                    ,l_shipment_tbl (nship_line_location_id_pos).VALUE);

                        xprogress := 'POCOB-10-1720';
                        ece_poo_transaction.populate_text_attachment (ccommunication_method
                                                                     ,ctransaction_type
                                                                     ,irun_id
                                                                     ,12
                                                                     ,13
                                                                     ,catt_header_interface
                                                                     ,catt_detail_interface
                                                                     ,v_entity_name
                                                                     ,'VENDOR'
                                                                     ,v_pk1_value
                                                                     ,ece_poo_transaction.c_any_value
                                                                     ,                                    -- BUG:5367903
                                                                      ece_poo_transaction.c_any_value
                                                                     ,ece_poo_transaction.c_any_value
                                                                     ,ece_poo_transaction.c_any_value
                                                                     ,n_att_seg_size
                                                                     ,l_key_tbl
                                                                     ,c_file_common_key
                                                                     ,l_shp_att_hdr_tbl
                                                                     ,l_shp_att_dtl_tbl
                                                                     ,iatt_shp_pos);
                    END IF;

                    -- Project Level Handler
                    xprogress := 'POCOB-10-1730';
                    --     if project_acct_status = 'I' THEN -- Project Accounting is Installed   bug 1891291
                    ece_poo_transaction.populate_distribution_info (ccommunication_method
                                                                   ,ctransaction_type
                                                                   ,irun_id
                                                                   ,cproject_interface
                                                                   ,l_key_tbl
                                                                   ,l_header_tbl (nheader_key_pos).VALUE
                                                                   ,                                     -- PO_HEADER_ID
                                                                    l_header_tbl (nrelease_id_pos).VALUE
                                                                   ,                                    -- PO_RELEASE_ID
                                                                    l_line_tbl (nline_key_pos).VALUE
                                                                   ,                                       -- PO_LINE_ID
                                                                    l_shipment_tbl (nship_line_location_id_pos).VALUE
                                                                   ,                                 -- LINE_LOCATION_ID
                                                                    c_file_common_key);                        --2823215
                --                 END IF;

                END LOOP;                                                                         -- SHIPMENT Level Loop

                xprogress := 'POCOB-10-1740';

                IF DBMS_SQL.last_row_count = 0
                THEN
                    v_levelprocessed := 'SHIPMENT';
                    ec_debug.pl (0
                                ,'EC'
                                ,'ECE_NO_DB_ROW_PROCESSED'
                                ,'PROGRESS_LEVEL'
                                ,xprogress
                                ,'LEVEL_PROCESSED'
                                ,v_levelprocessed
                                ,'TRANSACTION_TYPE'
                                ,ctransaction_type);
                END IF;
            END LOOP;                                                                                 -- LINE Level Loop

            xprogress := 'POCOB-10-1750';

            IF DBMS_SQL.last_row_count = 0
            THEN
                v_levelprocessed := 'LINE';
                ec_debug.pl (0
                            ,'EC'
                            ,'ECE_NO_DB_ROW_PROCESSED'
                            ,'PROGRESS_LEVEL'
                            ,xprogress
                            ,'LEVEL_PROCESSED'
                            ,v_levelprocessed
                            ,'TRANSACTION_TYPE'
                            ,ctransaction_type);
            END IF;
        END LOOP;                                                                                   -- HEADER Level Loop

        xprogress := 'POCOB-10-1760';

        IF DBMS_SQL.last_row_count = 0
        THEN
            v_levelprocessed := 'HEADER';
            ec_debug.pl (0
                        ,'EC'
                        ,'ECE_NO_DB_ROW_PROCESSED'
                        ,'LEVEL_PROCESSED'
                        ,v_levelprocessed
                        ,'PROGRESS_LEVEL'
                        ,xprogress
                        ,'TRANSACTION_TYPE'
                        ,ctransaction_type);
        END IF;

        xprogress := 'POCOB-10-1770';

        IF (ece_poo_transaction.project_sel_c > 0)
        THEN                                                                                               --Bug 2819176
            DBMS_SQL.close_cursor (ece_poo_transaction.project_sel_c);                                     --Bug 2490109
        END IF;

        xprogress := 'POCOB-10-1780';

        DBMS_SQL.close_cursor (shipment_sel_c);

        xprogress := 'POCOB-10-1790';
        DBMS_SQL.close_cursor (line_sel_c);

        xprogress := 'POCOB-10-1800';
        DBMS_SQL.close_cursor (header_sel_c);

        ec_debug.pop ('ECE_POCO_TRANSACTION.POPULATE_POCO_TRX');
    EXCEPTION
        WHEN OTHERS
        THEN
            ec_debug.pl (0
                        ,'EC'
                        ,'ECE_PROGRAM_ERROR'
                        ,'PROGRESS_LEVEL'
                        ,xprogress);
            ec_debug.pl (0
                        ,'EC'
                        ,'ECE_ERROR_MESSAGE'
                        ,'ERROR_MESSAGE'
                        ,SQLERRM);

            app_exception.raise_exception;
    END populate_poco_trx;

    PROCEDURE put_data_to_output_table (ccommunication_method   IN VARCHAR2
                                       ,ctransaction_type       IN VARCHAR2
                                       ,ioutput_width           IN INTEGER
                                       ,irun_id                 IN INTEGER
                                       ,cheader_interface       IN VARCHAR2
                                       ,cline_interface         IN VARCHAR2
                                       ,cshipment_interface     IN VARCHAR2
                                       ,cproject_interface      IN VARCHAR2)
    IS
        xprogress                   VARCHAR2 (80);
        v_levelprocessed            VARCHAR2 (40);

        catt_header_interface       VARCHAR2 (120) := 'ECE_ATTACHMENT_HEADERS';
        catt_detail_interface       VARCHAR2 (120) := 'ECE_ATTACHMENT_DETAILS';

        l_header_tbl                ece_flatfile_pvt.interface_tbl_type;
        l_line_tbl                  ece_flatfile_pvt.interface_tbl_type;
        l_shipment_tbl              ece_flatfile_pvt.interface_tbl_type;

        l_document_type             VARCHAR2 (30);

        c_header_common_key_name    VARCHAR2 (40);
        c_line_common_key_name      VARCHAR2 (40);
        c_shipment_key_name         VARCHAR2 (40);
        c_file_common_key           VARCHAR2 (255);

        nheader_key_pos             NUMBER;
        nline_key_pos               NUMBER;
        nshipment_key_pos           NUMBER;
        ntrans_code_pos             NUMBER;

        header_sel_c                INTEGER;
        line_sel_c                  INTEGER;
        shipment_sel_c              INTEGER;

        header_del_c1               INTEGER;
        line_del_c1                 INTEGER;
        shipment_del_c1             INTEGER;

        header_del_c2               INTEGER;
        line_del_c2                 INTEGER;
        shipment_del_c2             INTEGER;

        cheader_select              VARCHAR2 (32000);
        cline_select                VARCHAR2 (32000);
        cshipment_select            VARCHAR2 (32000);

        cheader_from                VARCHAR2 (32000);
        cline_from                  VARCHAR2 (32000);
        cshipment_from              VARCHAR2 (32000);

        cheader_where               VARCHAR2 (32000);
        cline_where                 VARCHAR2 (32000);
        cshipment_where             VARCHAR2 (32000);

        cheader_delete1             VARCHAR2 (32000);
        cline_delete1               VARCHAR2 (32000);
        cshipment_delete1           VARCHAR2 (32000);

        cheader_delete2             VARCHAR2 (32000);
        cline_delete2               VARCHAR2 (32000);
        cshipment_delete2           VARCHAR2 (32000);

        iheader_count               NUMBER;
        iline_count                 NUMBER;
        ishipment_count             NUMBER;

        rheader_rowid               ROWID;
        rline_rowid                 ROWID;
        rshipment_rowid             ROWID;

        cheader_x_interface         VARCHAR2 (50);
        cline_x_interface           VARCHAR2 (50);
        cshipment_x_interface       VARCHAR2 (50);

        rheader_x_rowid             ROWID;
        rline_x_rowid               ROWID;
        rshipment_x_rowid           ROWID;

        iheader_start_num           INTEGER;
        iline_start_num             INTEGER;
        ishipment_start_num         INTEGER;
        dummy                       INTEGER;

        ndocument_type_pos          NUMBER;
        npos1                       NUMBER;
        ntrans_id                   NUMBER;
        n_po_header_id              NUMBER;
        nrelease_id                 NUMBER;
        nrelease_id_pos             NUMBER;
        n_po_line_id                NUMBER;
        npo_line_location_id_pos    NUMBER;
        npo_line_location_id        NUMBER;
        nline_location_id_pos       NUMBER;
        nline_location_id           NUMBER;
        nline_num_pos               NUMBER;
        nline_num                   NUMBER;
        nrelease_num                NUMBER;
        nrelease_num_pos            NUMBER;
        norganization_id            NUMBER;
        nitem_id_pos                NUMBER;
        nitem_id                    NUMBER;

        v_project_acct_installed    BOOLEAN;
        v_project_acct_short_name   VARCHAR2 (2) := 'PA';
        v_project_acct_status       VARCHAR2 (120);
        v_project_acct_industry     VARCHAR2 (120);
        v_project_acct_schema       VARCHAR2 (120);

        v_entity_name               VARCHAR2 (120);
        v_pk1_value                 VARCHAR2 (120);
        v_pk2_value                 VARCHAR2 (120);

        CURSOR c_org_id (p_line_id NUMBER)
        IS
            SELECT DISTINCT ship_to_organization_id
              FROM po_line_locations
             WHERE po_line_id = p_line_id;
    BEGIN
        ec_debug.push ('ECE_POCO_TRANSACTION.PUT_DATA_TO_OUTPUT_TABLE');
        ec_debug.pl (3, 'cCommunication_Method: ', ccommunication_method);
        ec_debug.pl (3, 'cTransaction_Type: ', ctransaction_type);
        ec_debug.pl (3, 'iOutput_width: ', ioutput_width);
        ec_debug.pl (3, 'iRun_id: ', irun_id);
        ec_debug.pl (3, 'cHeader_Interface: ', cheader_interface);
        ec_debug.pl (3, 'cLine_Interface: ', cline_interface);
        ec_debug.pl (3, 'cShipment_Interface: ', cshipment_interface);

        BEGIN
            SELECT inventory_organization_id INTO norganization_id FROM financials_system_parameters;
        EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
                ec_debug.pl (0
                            ,'EC'
                            ,'ECE_NO_ROW_SELECTED'
                            ,'PROGRESS_LEVEL'
                            ,xprogress
                            ,'INFO'
                            ,'INVENTORY ORGANIZATION ID'
                            ,'TABLE_NAME'
                            ,'FINANCIALS_SYSTEM_PARAMETERS');
        END;

        ec_debug.pl (3, 'norganization_id: ', norganization_id);

        -- Let's See if Project Accounting is Installed
        xprogress := 'POCOB-20-1000';
        v_project_acct_installed :=
            fnd_installation.get_app_info (v_project_acct_short_name
                                          ,                                                                 -- i.e. 'PA'
                                           v_project_acct_status
                                          ,                                                  -- 'I' means it's installed
                                           v_project_acct_industry
                                          ,v_project_acct_schema);

        v_project_acct_status := NVL (v_project_acct_status, 'X');
        ec_debug.pl (3, 'v_project_acct_status: ', v_project_acct_status);
        ec_debug.pl (3, 'v_project_acct_industry: ', v_project_acct_industry);
        ec_debug.pl (3, 'v_project_acct_schema: ', v_project_acct_schema);

        xprogress := 'POCOB-20-1005';
        ece_flatfile_pvt.select_clause (ctransaction_type
                                       ,ccommunication_method
                                       ,cheader_interface
                                       ,cheader_x_interface
                                       ,l_header_tbl
                                       ,c_header_common_key_name
                                       ,cheader_select
                                       ,cheader_from
                                       ,cheader_where);

        xprogress := 'POCOB-20-1010';
        ece_flatfile_pvt.select_clause (ctransaction_type
                                       ,ccommunication_method
                                       ,cline_interface
                                       ,cline_x_interface
                                       ,l_line_tbl
                                       ,c_line_common_key_name
                                       ,cline_select
                                       ,cline_from
                                       ,cline_where);

        xprogress := 'POCOB-20-1020';
        ece_flatfile_pvt.select_clause (ctransaction_type
                                       ,ccommunication_method
                                       ,cshipment_interface
                                       ,cshipment_x_interface
                                       ,l_shipment_tbl
                                       ,c_shipment_key_name
                                       ,cshipment_select
                                       ,cshipment_from
                                       ,cshipment_where);

        -- Header Level Find Positions
        xprogress := 'POCOB-20-1021';
        ece_flatfile_pvt.find_pos (l_header_tbl, ece_flatfile_pvt.g_translator_code, ntrans_code_pos);
        ec_debug.pl (3, 'nTrans_code_pos: ', ntrans_code_pos);

        xprogress := 'POCOB-20-1022';
        ece_flatfile_pvt.find_pos (l_header_tbl, c_header_common_key_name, nheader_key_pos);
        ec_debug.pl (3, 'nHeader_key_pos: ', nheader_key_pos);

        xprogress := 'POCOB-20-1023';
        ece_flatfile_pvt.find_pos (l_header_tbl, 'RELEASE_NUMBER', nrelease_num_pos);
        ec_debug.pl (3, 'nRelease_num_pos: ', nrelease_num_pos);

        xprogress := 'POCOB-20-1024';
        ece_flatfile_pvt.find_pos (l_header_tbl, 'PO_RELEASE_ID', nrelease_id_pos);
        ec_debug.pl (3, 'nRelease_id_pos: ', nrelease_id_pos);

        xprogress := 'POCOB-20-1025';
        ece_flatfile_pvt.find_pos (l_header_tbl, 'DOCUMENT_TYPE', ndocument_type_pos);
        ec_debug.pl (3, 'nDocument_type_pos: ', ndocument_type_pos);

        -- Line Level Find Positions
        xprogress := 'POCOB-20-1026';
        ece_flatfile_pvt.find_pos (l_line_tbl, c_line_common_key_name, nline_key_pos);
        ec_debug.pl (3, 'nLine_key_pos: ', nline_key_pos);

        xprogress := 'POCOB-20-1027';
        ece_flatfile_pvt.find_pos (l_line_tbl, 'LINE_NUMBER', nline_num_pos);
        ec_debug.pl (3, 'nLine_num_pos: ', nline_num_pos);

        xprogress := 'POCOB-20-1028';
        ece_flatfile_pvt.find_pos (l_line_tbl, 'PO_LINE_LOCATION_ID', npo_line_location_id_pos);
        ec_debug.pl (3, 'nPO_Line_Location_ID_pos: ', npo_line_location_id_pos);

        xprogress := 'POCOB-20-1029';
        ece_flatfile_pvt.find_pos (l_line_tbl, 'ITEM_ID', nitem_id_pos);
        ec_debug.pl (3, 'nItem_id_pos: ', nitem_id_pos);

        -- Shipment Level Find Positions
        xprogress := 'POCOB-20-1030';
        ece_flatfile_pvt.find_pos (l_shipment_tbl, 'LINE_LOCATION_ID', nline_location_id_pos);
        ec_debug.pl (3, 'nLine_Location_ID_pos: ', nline_location_id_pos);

        xprogress := 'POCOB-20-1032';
        ece_flatfile_pvt.find_pos (l_shipment_tbl, c_shipment_key_name, nshipment_key_pos);
        ec_debug.pl (3, 'nShipment_key_pos: ', nshipment_key_pos);

        -- Build SELECT Statement
        xprogress := 'POCOB-20-1035';
        cheader_where := cheader_where || ' AND ' || cheader_interface || '.run_id = ' || ':Run_id';

        cline_where :=
               cline_where
            || ' AND '
            || cline_interface
            || '.run_id = '
            || ':Run_id'
            || ' AND '
            || cline_interface
            || '.po_header_id = :po_header_id AND '
            || cline_interface
            || '.release_number = :por_release_num';

        cshipment_where :=
               cshipment_where
            || ' AND '
            || cshipment_interface
            || '.RUN_ID ='
            || ':Run_id'
            || ' AND '
            || cshipment_interface
            || '.po_header_id = :po_header_id AND '
            || cshipment_interface
            || '.po_line_id = :po_line_id AND '
            || cshipment_interface
            || '.release_number = :por_release_num AND (('
            || cshipment_interface
            || '.release_number = 0) OR ('
            || cshipment_interface
            || '.release_number <> 0 AND '
            || cshipment_interface
            || '.shipment_number = :shipment_number))';

        xprogress := 'POCOB-20-1040';
        cheader_select :=
               cheader_select
            || ','
            || cheader_interface
            || '.rowid,'
            || cheader_x_interface
            || '.rowid,'
            || cheader_interface
            || '.po_header_id,'
            || cheader_interface
            || '.release_number ';

        cline_select :=
               cline_select
            || ','
            || cline_interface
            || '.rowid,'
            || cline_x_interface
            || '.rowid,'
            || cline_interface
            || '.po_line_id,'
            || cline_interface
            || '.line_number ';

        cshipment_select :=
               cshipment_select
            || ','
            || cshipment_interface
            || '.rowid,'
            || cshipment_x_interface
            || '.rowid,'
            || cshipment_interface
            || '.shipment_number ';

        xprogress := 'POCOB-20-1050';
        cheader_select :=
               cheader_select
            || cheader_from
            || cheader_where
            || ' ORDER BY '
            || cheader_interface
            || '.po_header_id,'
            || cheader_interface
            || '.release_number '
            || ' FOR UPDATE';
        ec_debug.pl (3, 'cHeader_select: ', cheader_select);

        cline_select :=
               cline_select
            || cline_from
            || cline_where
            || ' ORDER BY '
            || cline_interface
            || '.line_number '
            || ' FOR UPDATE';
        ec_debug.pl (3, 'cLine_select: ', cline_select);

        cshipment_select :=
               cshipment_select
            || cshipment_from
            || cshipment_where
            || ' ORDER BY '
            || cshipment_interface
            || '.shipment_number '
            || ' FOR UPDATE';
        ec_debug.pl (3, 'cShipment_select: ', cshipment_select);

        xprogress := 'POCOB-20-1060';
        cheader_delete1 := 'DELETE FROM ' || cheader_interface || ' WHERE rowid = :col_rowid';
        ec_debug.pl (3, 'cHeader_delete1: ', cheader_delete1);

        cline_delete1 := 'DELETE FROM ' || cline_interface || ' WHERE rowid = :col_rowid';
        ec_debug.pl (3, 'cLine_delete1: ', cline_delete1);

        cshipment_delete1 := 'DELETE FROM ' || cshipment_interface || ' WHERE rowid = :col_rowid';
        ec_debug.pl (3, 'cShipment_delete1: ', cshipment_delete1);

        xprogress := 'POCOB-20-1070';
        cheader_delete2 := 'DELETE FROM ' || cheader_x_interface || ' WHERE rowid = :col_rowid';
        ec_debug.pl (3, 'cHeader_delete2: ', cheader_delete2);

        cline_delete2 := 'DELETE FROM ' || cline_x_interface || ' WHERE rowid = :col_rowid';
        ec_debug.pl (3, 'cLine_delete2: ', cline_delete2);

        cshipment_delete2 := 'DELETE FROM ' || cshipment_x_interface || ' WHERE rowid = :col_rowid';
        ec_debug.pl (3, 'cShipment_delete2: ', cshipment_delete2);

        -- ***************************************************
        -- ***   Get data setup for the dynamic SQL call.
        -- ***   Open a cursor for each of the SELECT call
        -- ***   This tells the database to reserve spaces
        -- ***   for the data returned by the SQL statement
        -- ***************************************************
        xprogress := 'POCOB-20-1080';
        header_sel_c := DBMS_SQL.open_cursor;

        xprogress := 'POCOB-20-1090';
        line_sel_c := DBMS_SQL.open_cursor;

        xprogress := 'POCOB-20-1100';
        shipment_sel_c := DBMS_SQL.open_cursor;

        xprogress := 'POCOB-20-1110';
        header_del_c1 := DBMS_SQL.open_cursor;

        xprogress := 'POCOB-20-1120';
        line_del_c1 := DBMS_SQL.open_cursor;

        xprogress := 'POCOB-20-1130';
        shipment_del_c1 := DBMS_SQL.open_cursor;

        xprogress := 'POCOB-20-1140';
        header_del_c2 := DBMS_SQL.open_cursor;

        xprogress := 'POCOB-20-1150';
        line_del_c2 := DBMS_SQL.open_cursor;

        xprogress := 'POCOB-20-1160';
        shipment_del_c2 := DBMS_SQL.open_cursor;

        -- *****************************************
        -- Parse each of the SELECT statement
        -- so the database understands the command
        -- *****************************************
        xprogress := 'POCOB-20-1170';

        BEGIN
            DBMS_SQL.parse (header_sel_c, cheader_select, DBMS_SQL.native);
        EXCEPTION
            WHEN OTHERS
            THEN
                ece_error_handling_pvt.print_parse_error (DBMS_SQL.last_error_position, cheader_select);
                app_exception.raise_exception;
        END;

        xprogress := 'POCOB-20-1180';

        BEGIN
            DBMS_SQL.parse (line_sel_c, cline_select, DBMS_SQL.native);
        EXCEPTION
            WHEN OTHERS
            THEN
                ece_error_handling_pvt.print_parse_error (DBMS_SQL.last_error_position, cline_select);
                app_exception.raise_exception;
        END;

        xprogress := 'POCOB-20-1190';

        BEGIN
            DBMS_SQL.parse (shipment_sel_c, cshipment_select, DBMS_SQL.native);
        EXCEPTION
            WHEN OTHERS
            THEN
                ece_error_handling_pvt.print_parse_error (DBMS_SQL.last_error_position, cshipment_select);
                app_exception.raise_exception;
        END;

        xprogress := 'POCOB-20-1200';

        BEGIN
            DBMS_SQL.parse (header_del_c1, cheader_delete1, DBMS_SQL.native);
        EXCEPTION
            WHEN OTHERS
            THEN
                ece_error_handling_pvt.print_parse_error (DBMS_SQL.last_error_position, cheader_delete1);
                app_exception.raise_exception;
        END;

        xprogress := 'POCOB-20-1210';

        BEGIN
            DBMS_SQL.parse (line_del_c1, cline_delete1, DBMS_SQL.native);
        EXCEPTION
            WHEN OTHERS
            THEN
                ece_error_handling_pvt.print_parse_error (DBMS_SQL.last_error_position, cline_delete1);
                app_exception.raise_exception;
        END;

        xprogress := 'POCOB-20-1220';

        BEGIN
            DBMS_SQL.parse (shipment_del_c1, cshipment_delete1, DBMS_SQL.native);
        EXCEPTION
            WHEN OTHERS
            THEN
                ece_error_handling_pvt.print_parse_error (DBMS_SQL.last_error_position, cshipment_delete1);
                app_exception.raise_exception;
        END;

        xprogress := 'POCOB-20-1230';

        BEGIN
            DBMS_SQL.parse (header_del_c2, cheader_delete2, DBMS_SQL.native);
        EXCEPTION
            WHEN OTHERS
            THEN
                ece_error_handling_pvt.print_parse_error (DBMS_SQL.last_error_position, cheader_delete2);
                app_exception.raise_exception;
        END;

        xprogress := 'POCOB-20-1240';

        BEGIN
            DBMS_SQL.parse (line_del_c2, cline_delete2, DBMS_SQL.native);
        EXCEPTION
            WHEN OTHERS
            THEN
                ece_error_handling_pvt.print_parse_error (DBMS_SQL.last_error_position, cline_delete2);
                app_exception.raise_exception;
        END;

        xprogress := 'POCOB-20-1250';

        BEGIN
            DBMS_SQL.parse (shipment_del_c2, cshipment_delete2, DBMS_SQL.native);
        EXCEPTION
            WHEN OTHERS
            THEN
                ece_error_handling_pvt.print_parse_error (DBMS_SQL.last_error_position, cshipment_delete2);
                app_exception.raise_exception;
        END;

        -- *************
        -- set counter
        -- *************
        xprogress := 'POCOB-20-1260';
        iheader_count := l_header_tbl.COUNT;
        iline_count := l_line_tbl.COUNT;
        ishipment_count := l_shipment_tbl.COUNT;

        ec_debug.pl (3, 'iHeader_count: ', iheader_count);
        ec_debug.pl (3, 'iLine_count: ', iline_count);
        ec_debug.pl (3, 'iShipment_count: ', ishipment_count);

        -- ******************************************************
        --  Define TYPE for every columns in the SELECT statement
        --  For each piece of the data returns, we need to tell
        --  the database what type of information it will be.
        --  e.g. ID is NUMBER, due_date is DATE
        --  However, for simplicity, we will convert
        --  everything to varchar2.
        -- ******************************************************
        xprogress := 'POCOB-20-1270';
        ece_flatfile_pvt.define_interface_column_type (header_sel_c
                                                      ,cheader_select
                                                      ,ece_flatfile_pvt.g_maxcolwidth
                                                      ,l_header_tbl);

        -- ***************************************************
        -- Need rowid for delete (Header Level)
        -- ***************************************************
        xprogress := 'POCOB-20-1280';
        DBMS_SQL.define_column_rowid (header_sel_c, iheader_count + 1, rheader_rowid);

        xprogress := 'POCOB-20-1290';
        DBMS_SQL.define_column_rowid (header_sel_c, iheader_count + 2, rheader_x_rowid);

        xprogress := 'POCOB-20-1300';
        DBMS_SQL.define_column (header_sel_c, iheader_count + 3, n_po_header_id);

        xprogress := 'POCOB-20-1310';
        ece_flatfile_pvt.define_interface_column_type (line_sel_c
                                                      ,cline_select
                                                      ,ece_flatfile_pvt.g_maxcolwidth
                                                      ,l_line_tbl);

        -- ***************************************************
        -- Need rowid for delete (Line Level)
        -- ***************************************************
        xprogress := 'POCOB-20-1320';
        DBMS_SQL.define_column_rowid (line_sel_c, iline_count + 1, rline_rowid);

        xprogress := 'POCOB-20-1330';
        DBMS_SQL.define_column_rowid (line_sel_c, iline_count + 2, rline_x_rowid);

        xprogress := 'POCOB-20-1340';
        DBMS_SQL.define_column (line_sel_c, iline_count + 3, n_po_line_id);

        xprogress := 'POCOB-20-1350';
        ece_flatfile_pvt.define_interface_column_type (shipment_sel_c
                                                      ,cshipment_select
                                                      ,ece_flatfile_pvt.g_maxcolwidth
                                                      ,l_shipment_tbl);

        -- ***************************************************
        -- Need rowid for delete (Shipment Level)
        -- ***************************************************
        xprogress := 'POCOB-20-1360';
        DBMS_SQL.define_column_rowid (shipment_sel_c, ishipment_count + 1, rshipment_rowid);

        xprogress := 'POCOB-20-1370';
        DBMS_SQL.define_column_rowid (shipment_sel_c, ishipment_count + 2, rshipment_x_rowid);

        xprogress := 'POCOB-20-1375';
        DBMS_SQL.bind_variable (header_sel_c, 'Run_id', irun_id);
        DBMS_SQL.bind_variable (line_sel_c, 'Run_id', irun_id);
        DBMS_SQL.bind_variable (shipment_sel_c, 'Run_id', irun_id);

        --- EXECUTE the SELECT statement
        xprogress := 'POCOB-20-1380';
        dummy := DBMS_SQL.execute (header_sel_c);

        -- ********************************************************************
        -- ***   With data for each HEADER line, populate the ECE_OUTPUT table
        -- ***   then populate ECE_OUTPUT with data from all LINES that belongs
        -- ***   to the HEADER. Then populate ECE_OUTPUT with data from all
        -- ***   LINE TAX that belongs to the LINE.
        -- ********************************************************************

        -- HEADER - LINE - SHIPMENT ...
        xprogress := 'POCOB-20-1390';

        WHILE DBMS_SQL.fetch_rows (header_sel_c) > 0
        LOOP                                                                                                   -- Header
            -- ******************************
            --   store values in pl/sql table
            -- ******************************
            xprogress := 'POCOB-20-1400';
            ece_flatfile_pvt.assign_column_value_to_tbl (header_sel_c, l_header_tbl);

            xprogress := 'POCOB-20-1410';
            DBMS_SQL.COLUMN_VALUE (header_sel_c, iheader_count + 1, rheader_rowid);

            xprogress := 'POCOB-20-1420';
            DBMS_SQL.COLUMN_VALUE (header_sel_c, iheader_count + 2, rheader_x_rowid);

            xprogress := 'POCOB-20-1430';
            DBMS_SQL.COLUMN_VALUE (header_sel_c, iheader_count + 3, n_po_header_id);

            xprogress := 'POCOB-20-1440';
            nrelease_num := l_header_tbl (nrelease_num_pos).VALUE;
            ec_debug.pl (3, 'nRelease_num: ', nrelease_num);

            xprogress := 'POCOB-20-1450';
            nrelease_id := l_header_tbl (nrelease_id_pos).VALUE;
            ec_debug.pl (3, 'nRelease_ID: ', nrelease_id);

            BEGIN
                xprogress := 'POCOB-20-1455';

                /* Bug 2396394 Added the document type CONTRACT in SQL below */

                SELECT DECODE (l_header_tbl (ndocument_type_pos).VALUE
                              ,'BLANKET', 'NB'
                              ,'STANDARD', 'NS'
                              ,'PLANNED', 'NP'
                              ,'RELEASE', 'NR'
                              ,'BLANKET RELEASE', 'NR'
                              ,'CONTRACT', 'NC'
                              ,'NR')
                  INTO l_document_type
                  FROM DUAL;
            EXCEPTION
                WHEN NO_DATA_FOUND
                THEN
                    ec_debug.pl (0
                                ,'EC'
                                ,'ECE_DECODE_FAILED'
                                ,'PROGRESS_LEVEL'
                                ,xprogress
                                ,'CODE'
                                ,l_header_tbl (ndocument_type_pos).VALUE);
            END;

            ec_debug.pl (3, 'l_document_type: ', l_document_type);

            xprogress := 'POCOB-20-1460';
            c_file_common_key := RPAD (SUBSTRB (NVL (l_header_tbl (ntrans_code_pos).VALUE, ' '), 1, 25), 25);

            xprogress := 'POCOB-20-1470';
            c_file_common_key :=
                   c_file_common_key
                || RPAD (SUBSTRB (NVL (l_header_tbl (nheader_key_pos).VALUE, ' '), 1, 22), 22)
                || RPAD (' ', 22)
                || RPAD (' ', 22);

            ec_debug.pl (3, 'c_file_common_key: ', c_file_common_key);

            xprogress := 'POCOB-20-1480';
            ece_poo_transaction.write_to_file (ctransaction_type
                                              ,ccommunication_method
                                              ,cheader_interface
                                              ,l_header_tbl
                                              ,ioutput_width
                                              ,irun_id
                                              ,c_file_common_key
                                              ,NULL);

            IF l_document_type = 'NR'
            THEN                                                                             -- If this is a Release PO.
                xprogress := 'POCOB-20-1481';
                v_entity_name := 'PO_RELEASES';
                v_pk1_value := nrelease_id;
                ec_debug.pl (3, 'release_id: ', nrelease_id);
            ELSE                                                                         -- If this is a non-Release PO.
                xprogress := 'POCOB-20-1482';
                v_entity_name := 'PO_HEADERS';
                v_pk1_value := n_po_header_id;
                ec_debug.pl (3, 'po_header_id: ', n_po_header_id);
            END IF;

            xprogress := 'POCOB-20-1483';
            ece_poo_transaction.put_att_to_output_table (ccommunication_method
                                                        ,ctransaction_type
                                                        ,ioutput_width
                                                        ,irun_id
                                                        ,2
                                                        ,3
                                                        ,catt_header_interface
                                                        ,catt_detail_interface
                                                        ,v_entity_name
                                                        ,'VENDOR'
                                                        ,v_pk1_value
                                                        ,NULL
                                                        ,NULL
                                                        ,NULL
                                                        ,NULL
                                                        ,c_file_common_key);

            -- ***************************************************
            -- With Header data at hand, we can assign values to
            -- place holders (foreign keys) in Line_select and
            -- Line_detail_Select
            -- ***************************************************
            -- ******************************************
            -- set values into binding variables
            -- ******************************************
            xprogress := 'POCOB-20-1490';
            DBMS_SQL.bind_variable (line_sel_c, 'po_header_id', n_po_header_id);

            xprogress := 'POCOB-20-1500';
            DBMS_SQL.bind_variable (shipment_sel_c, 'po_header_id', n_po_header_id);

            xprogress := 'POCOB-20-1505';
            DBMS_SQL.bind_variable (line_sel_c, 'por_release_num', nrelease_num);

            xprogress := 'POCOB-20-1506';
            DBMS_SQL.bind_variable (shipment_sel_c, 'por_release_num', nrelease_num);

            xprogress := 'POCOB-10-1510';
            dummy := DBMS_SQL.execute (line_sel_c);

            -- ***************************************************
            -- line loop starts here
            -- ***************************************************
            xprogress := 'POCOB-20-1520';

            WHILE DBMS_SQL.fetch_rows (line_sel_c) > 0
            LOOP                                                                                                --- Line
                -- ***************************************************
                --   store values in pl/sql table
                -- ***************************************************
                xprogress := 'POCOB-20-1530';
                ece_flatfile_pvt.assign_column_value_to_tbl (line_sel_c, l_line_tbl);

                xprogress := 'POCOB-20-1533';
                DBMS_SQL.COLUMN_VALUE (line_sel_c, iline_count + 1, rline_rowid);

                xprogress := 'POCOB-20-1535';
                DBMS_SQL.COLUMN_VALUE (line_sel_c, iline_count + 2, rline_x_rowid);

                xprogress := 'POCOB-20-1537';
                DBMS_SQL.COLUMN_VALUE (line_sel_c, iline_count + 3, n_po_line_id);
                ec_debug.pl (3, 'n_po_line_id: ', n_po_line_id);

                xprogress := 'POCOB-20-1540';
                nline_num := l_line_tbl (nline_num_pos).VALUE;
                ec_debug.pl (3, 'nLine_num: ', nline_num);

                xprogress := 'POCOB-20-1544';
                npo_line_location_id := l_line_tbl (npo_line_location_id_pos).VALUE;
                ec_debug.pl (3, 'nPO_Line_Location_ID: ', npo_line_location_id);

                xprogress := 'POCOB-20-1545';
                nitem_id := l_line_tbl (nitem_id_pos).VALUE;
                ec_debug.pl (3, 'nItem_ID: ', nitem_id);

                xprogress := 'POCOB-20-1550';
                c_file_common_key :=
                       RPAD (SUBSTRB (NVL (l_header_tbl (ntrans_code_pos).VALUE, ' '), 1, 25), 25)
                    || RPAD (SUBSTRB (NVL (l_header_tbl (nheader_key_pos).VALUE, ' '), 1, 22), 22)
                    || RPAD (SUBSTRB (NVL (l_line_tbl (nline_key_pos).VALUE, ' '), 1, 22), 22)
                    || RPAD (' ', 22);
                ec_debug.pl (3, 'c_file_common_key: ', c_file_common_key);

                xprogress := 'POCOB-20-1551';
                ece_poo_transaction.write_to_file (ctransaction_type
                                                  ,ccommunication_method
                                                  ,cline_interface
                                                  ,l_line_tbl
                                                  ,ioutput_width
                                                  ,irun_id
                                                  ,c_file_common_key
                                                  ,NULL);

                -- Line Level Attachment Handler
                /* Bug 2235872  IF l_document_type = 'NR' THEN -- If this is a Release PO.
                                 xProgress := 'POCOB-20-1552';
                                 v_entity_name := 'PO_SHIPMENTS';
                                 v_pk1_value := nPO_Line_Location_ID; -- LINE_LOCATION_ID
                              ELSE -- If this is a non-Release PO. */
                xprogress := 'POCOB-20-1553';
                v_entity_name := 'PO_LINES';
                v_pk1_value := n_po_line_id;                                                                  -- LINE_ID
                --   END IF;

                xprogress := 'POCOB-20-1554';
                ece_poo_transaction.put_att_to_output_table (ccommunication_method
                                                            ,ctransaction_type
                                                            ,ioutput_width
                                                            ,irun_id
                                                            ,5
                                                            ,6
                                                            ,catt_header_interface
                                                            ,catt_detail_interface
                                                            ,v_entity_name
                                                            ,'VENDOR'
                                                            ,v_pk1_value
                                                            ,NULL
                                                            ,NULL
                                                            ,NULL
                                                            ,NULL
                                                            ,c_file_common_key);

                -- Master Item Attachment Handler
                xprogress := 'POCOB-20-1555';
                v_entity_name := 'MTL_SYSTEM_ITEMS';
                v_pk1_value := norganization_id;                                              -- Master Inventory Org ID
                ec_debug.pl (3, 'Master Org ID: ', v_pk1_value);

                v_pk2_value := nitem_id;                                                                      -- Item ID
                ec_debug.pl (3, 'Item ID: ', v_pk2_value);

                xprogress := 'POCOB-20-1556';
                ece_poo_transaction.put_att_to_output_table (ccommunication_method
                                                            ,ctransaction_type
                                                            ,ioutput_width
                                                            ,irun_id
                                                            ,7
                                                            ,8
                                                            ,catt_header_interface
                                                            ,catt_detail_interface
                                                            ,v_entity_name
                                                            ,'VENDOR'
                                                            ,v_pk1_value
                                                            ,v_pk2_value
                                                            ,NULL
                                                            ,NULL
                                                            ,NULL
                                                            ,c_file_common_key);

                -- Inventory Item Attachment Handler
                xprogress := 'POCOB-20-1557';

                FOR v_org_id IN c_org_id (n_po_line_id)
                LOOP                                                                      -- Value passed is the Line ID
                    IF v_org_id.ship_to_organization_id <> norganization_id
                    THEN                                      -- Only do this if it is not the same as the Master Org ID
                        v_pk1_value := v_org_id.ship_to_organization_id;
                        ec_debug.pl (3, 'Inventory Org ID: ', v_pk1_value);

                        xprogress := 'POCOB-20-1558';
                        ece_poo_transaction.put_att_to_output_table (ccommunication_method
                                                                    ,ctransaction_type
                                                                    ,ioutput_width
                                                                    ,irun_id
                                                                    ,9
                                                                    ,10
                                                                    ,catt_header_interface
                                                                    ,catt_detail_interface
                                                                    ,v_entity_name
                                                                    ,'VENDOR'
                                                                    ,v_pk1_value
                                                                    ,v_pk2_value
                                                                    ,NULL
                                                                    ,NULL
                                                                    ,NULL
                                                                    ,c_file_common_key);
                    END IF;
                END LOOP;

                -- **************************
                --   set LINE_NUMBER values
                -- **************************
                xprogress := 'POCOB-20-1560';
                DBMS_SQL.bind_variable (shipment_sel_c, 'po_line_id', n_po_line_id);

                xprogress := 'POCOB-20-1575';
                DBMS_SQL.bind_variable (shipment_sel_c, 'shipment_number', nline_num);

                xprogress := 'POCOB-20-1580';
                dummy := DBMS_SQL.execute (shipment_sel_c);

                -- ****************************
                --  Shipment loop starts here
                -- ****************************
                xprogress := 'POCOB-20-1590';

                WHILE DBMS_SQL.fetch_rows (shipment_sel_c) > 0
                LOOP                                                                                       --- Shipments
                    -- *********************************
                    --  store values in pl/sql table
                    -- *********************************
                    xprogress := 'POCOB-20-1600';
                    ece_flatfile_pvt.assign_column_value_to_tbl (shipment_sel_c, l_shipment_tbl);

                    xprogress := 'POCOB-20-1603';
                    DBMS_SQL.COLUMN_VALUE (shipment_sel_c, ishipment_count + 1, rshipment_rowid);

                    xprogress := 'POCOB-20-1606';
                    DBMS_SQL.COLUMN_VALUE (shipment_sel_c, ishipment_count + 2, rshipment_x_rowid);

                    xprogress := 'POCOB-20-1610';
                    nline_location_id := l_shipment_tbl (nline_location_id_pos).VALUE;
                    ec_debug.pl (3, 'Ship Level Line Location ID: ', nline_location_id);

                    xprogress := 'POCOB-20-1620';
                    c_file_common_key :=
                           RPAD (SUBSTRB (NVL (l_header_tbl (ntrans_code_pos).VALUE, ' '), 1, 25), 25)
                        || RPAD (SUBSTRB (NVL (l_header_tbl (nheader_key_pos).VALUE, ' '), 1, 22), 22)
                        || RPAD (SUBSTRB (NVL (l_line_tbl (nline_key_pos).VALUE, ' '), 1, 22), 22)
                        || RPAD (SUBSTRB (NVL (l_shipment_tbl (nshipment_key_pos).VALUE, ' '), 1, 22), 22);
                    ec_debug.pl (3, 'c_file_common_key: ', c_file_common_key);

                    xprogress := 'POCOB-20-1630';
                    ece_poo_transaction.write_to_file (ctransaction_type
                                                      ,ccommunication_method
                                                      ,cshipment_interface
                                                      ,l_shipment_tbl
                                                      ,ioutput_width
                                                      ,irun_id
                                                      ,c_file_common_key
                                                      ,NULL);

                    -- Shipment Level Attachment Handler
                    v_entity_name := 'PO_SHIPMENTS';
                    v_pk1_value := nline_location_id;

                    xprogress := 'POCOB-20-1632';
                    ece_poo_transaction.put_att_to_output_table (ccommunication_method
                                                                ,ctransaction_type
                                                                ,ioutput_width
                                                                ,irun_id
                                                                ,12
                                                                ,13
                                                                ,catt_header_interface
                                                                ,catt_detail_interface
                                                                ,v_entity_name
                                                                ,'VENDOR'
                                                                ,v_pk1_value
                                                                ,NULL
                                                                ,NULL
                                                                ,NULL
                                                                ,NULL
                                                                ,c_file_common_key);

                    -- Project Level Handler
                    xprogress := 'POCOB-20-1634';
                    -- IF v_project_acct_status = 'I' THEN -- Project Accounting is Installed bug1891291
                    ece_poo_transaction.put_distdata_to_out_tbl (ccommunication_method
                                                                ,ctransaction_type
                                                                ,ioutput_width
                                                                ,irun_id
                                                                ,cproject_interface
                                                                ,n_po_header_id
                                                                ,                                        -- PO_HEADER_ID
                                                                 nrelease_id
                                                                ,                                       -- PO_RELEASE_ID
                                                                 n_po_line_id
                                                                ,                                          -- PO_LINE_ID
                                                                 nline_location_id
                                                                ,                                    -- LINE_LOCATION_ID
                                                                 c_file_common_key);
                    --   END IF;

                    xprogress := 'POCOB-20-1640';
                    DBMS_SQL.bind_variable (shipment_del_c1, 'col_rowid', rshipment_rowid);

                    xprogress := 'POCOB-20-1650';
                    DBMS_SQL.bind_variable (shipment_del_c2, 'col_rowid', rshipment_x_rowid);

                    xprogress := 'POCOB-20-1660';
                    dummy := DBMS_SQL.execute (shipment_del_c1);

                    xprogress := 'POCOB-20-1670';
                    dummy := DBMS_SQL.execute (shipment_del_c2);
                END LOOP;                                                                              -- Shipment Level

                xprogress := 'POCOB-20-1674';

                IF DBMS_SQL.last_row_count = 0
                THEN
                    v_levelprocessed := 'SHIPMENT';
                    ec_debug.pl (0
                                ,'EC'
                                ,'ECE_NO_DB_ROW_PROCESSED'
                                ,'PROGRESS_LEVEL'
                                ,xprogress
                                ,'LEVEL_PROCESSED'
                                ,v_levelprocessed
                                ,'TRANSACTION_TYPE'
                                ,ctransaction_type);
                END IF;

                -- *********************
                -- Use rowid for delete
                -- *********************
                xprogress := 'POCOB-20-1680';
                DBMS_SQL.bind_variable (line_del_c1, 'col_rowid', rline_rowid);

                xprogress := 'POCOB-20-1690';
                DBMS_SQL.bind_variable (line_del_c2, 'col_rowid', rline_x_rowid);

                xprogress := 'POCOB-20-1700';
                dummy := DBMS_SQL.execute (line_del_c1);

                xprogress := 'POCOB-20-1710';
                dummy := DBMS_SQL.execute (line_del_c2);
            END LOOP;                                                                                      -- Line Level

            xprogress := 'POCOB-20-1714';

            IF DBMS_SQL.last_row_count = 0
            THEN
                v_levelprocessed := 'LINE';
                ec_debug.pl (0
                            ,'EC'
                            ,'ECE_NO_DB_ROW_PROCESSED'
                            ,'PROGRESS_LEVEL'
                            ,xprogress
                            ,'LEVEL_PROCESSED'
                            ,v_levelprocessed
                            ,'TRANSACTION_TYPE'
                            ,ctransaction_type);
            END IF;

            xprogress := 'POCOB-20-1720';
            DBMS_SQL.bind_variable (header_del_c1, 'col_rowid', rheader_rowid);

            xprogress := 'POCOB-20-1730';
            DBMS_SQL.bind_variable (header_del_c2, 'col_rowid', rheader_x_rowid);

            xprogress := 'POCOB-20-1740';
            dummy := DBMS_SQL.execute (header_del_c1);

            xprogress := 'POCOB-20-1750';
            dummy := DBMS_SQL.execute (header_del_c2);
        END LOOP;                                                                                        -- Header Level

        xprogress := 'POCOB-20-1754';

        IF DBMS_SQL.last_row_count = 0
        THEN
            v_levelprocessed := 'HEADER';
            ec_debug.pl (0
                        ,'EC'
                        ,'ECE_NO_DB_ROW_PROCESSED'
                        ,'PROGRESS_LEVEL'
                        ,xprogress
                        ,'LEVEL_PROCESSED'
                        ,v_levelprocessed
                        ,'TRANSACTION_TYPE'
                        ,ctransaction_type);
        END IF;

        xprogress := 'POCOB-20-1760';
        DBMS_SQL.close_cursor (header_sel_c);

        xprogress := 'POCOB-20-1770';
        DBMS_SQL.close_cursor (line_sel_c);

        xprogress := 'POCOB-20-1780';
        DBMS_SQL.close_cursor (shipment_sel_c);

        xprogress := 'POCOB-20-1790';
        DBMS_SQL.close_cursor (header_del_c1);

        xprogress := 'POCOB-20-1800';
        DBMS_SQL.close_cursor (line_del_c1);

        xprogress := 'POCOB-20-1812';
        DBMS_SQL.close_cursor (shipment_del_c1);

        xprogress := 'POCOB-20-1814';
        DBMS_SQL.close_cursor (header_del_c2);

        xprogress := 'POCOB-20-1816';
        DBMS_SQL.close_cursor (line_del_c2);

        xprogress := 'POCOB-20-1818';
        DBMS_SQL.close_cursor (shipment_del_c2);

        -- Bug 2490109 Closing the distribution cursors.
        xprogress := 'POCOB-50-1819';

        IF (ece_poo_transaction.project_sel_c > 0)
        THEN                                                                                               --Bug 2819176
            DBMS_SQL.close_cursor (ece_poo_transaction.project_sel_c);

            xprogress := 'POCOB-50-1820';
            DBMS_SQL.close_cursor (ece_poo_transaction.project_del_c1);

            xprogress := 'POCOB-50-1821';
            DBMS_SQL.close_cursor (ece_poo_transaction.project_del_c2);
        END IF;

        xprogress := 'POCOB-20-1820';
        ec_debug.pop ('ECE_POCO_TRANSACTION.PUT_DATA_TO_OUTPUT_TABLE');
    EXCEPTION
        WHEN OTHERS
        THEN
            ec_debug.pl (0
                        ,'EC'
                        ,'ECE_PROGRAM_ERROR'
                        ,'PROGRESS_LEVEL'
                        ,xprogress);
            ec_debug.pl (0
                        ,'EC'
                        ,'ECE_ERROR_MESSAGE'
                        ,'ERROR_MESSAGE'
                        ,SQLERRM);

            app_exception.raise_exception;
    END put_data_to_output_table;
END ece_poco_transaction;
/

-- End of DDL Script for Package Body APPS.ECE_POCO_TRANSACTION
