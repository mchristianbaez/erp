CREATE OR REPLACE PACKAGE BODY APPS.xxwc_ego_item_cross_ref_pkg
/**************************************************************************
 *
 * HEADER
 *   XXWC_EGO_ITEM_CROSS_REF_PKG
 *
 * PROGRAM NAME
 *  XXWC_EGO_ITEM_CROSS_REF_PKG.pkb
 *
 * DESCRIPTION
 *  This package body will upload the item cross reference in to base table using the API
 *
 *
 * PARAMETERS
 * ==========
 * NAME              DESCRIPTION
.* ----------------- ------------------------------------------------------
    None
 *
 * DEPENDENCIES
 *   None
 *
 * CALLED BY
 *   Item Cross reference Upload WebADI
 *
 * LAST UPDATE DATE   19-AUG-2014
 *
 * HISTORY
 * =======
 *
 * VERSION DATE        AUTHOR(S)       DESCRIPTION
 * ------- ----------- --------------- ------------------------------------
 * 1.00    19-AUG-2014 KPIT            Created for Item Cross reference Upload WebADI TMS # 20140210-00116
 *************************************************************************/
AS
/**************************************************************************
 *
 * PROCEDURE
 * upload_xref
 *
 * DESCRIPTION
 *  This procedure will upload the cross reference in to base table thorough Web ADI
 *
 * PARAMETERS
 * ==========
 * NAME                   TYPE                  DESCRIPTION
.* ----------------- -------- ---------------------------------------------
 * P_ITEM_NUMBER          VARCHAR2 IN      inventory item number
 * P_TRANSACTION_TYPE     VARCHAR2 IN      transaction type for cross-reference
 * P_CROSS_TYPE            VARCAHR2 IN      cross-reference type
 * P_XREF_CODE            VARCHAR2 IN      old cross-reference
 * P_NEW_XREF_CODE        VARCHAR2 IN      new cross-reference
 * P_MST_VEN_NO           VARCHAR2 IN      Master Vendor Number
 *
 * RETURN VALUE
 *  None
 *
 * CALLED BY
 *   Upload Item Cross reference Web ADI
 *
 *************************************************************************/
   PROCEDURE upload_xref (
      p_item_number        IN   VARCHAR2,
      p_transaction_type   IN   VARCHAR2,
      p_cross_type         IN   VARCHAR2,
      p_xref_code          IN   VARCHAR2 DEFAULT NULL,
      p_new_xref_code      IN   VARCHAR2,
      p_mst_ven_no         IN   VARCHAR2 DEFAULT NULL
   )
   AS
-- --------------------------------------------------------------
-- Declaring Global Exception
-- --------------------------------------------------------------
      xxwc_error        EXCEPTION;
-- --------------------------------------------------------------
-- Declaring local variables
-- --------------------------------------------------------------
      l_org_id          NUMBER                                 := NULL;
      l_item_id         NUMBER;
      l_upc_code        VARCHAR2 (255);
      l_xref_type       VARCHAR2 (25);
      l_xref_id         NUMBER;
      l_xref_tbl        mtl_cross_references_pub.xref_tbl_type;
      l_cnt             NUMBER                                 := 0;
      l_return_status   VARCHAR2 (1);
      l_msg_count       NUMBER;
      l_message_list    error_handler.error_tbl_type;
      l_module          VARCHAR2 (100)                       := 'upload_xref';
      l_err_msg         VARCHAR2 (4000);
      l_api_error_msg   VARCHAR2 (4000);
      l_err_callfrom    VARCHAR2 (175)       := 'XXWC_EGO_ITEM_CROSS_REF_PKG';
      l_err_callpoint   VARCHAR2 (175)                         := 'START';
      l_distro_list     VARCHAR2 (80)
                                     := 'OracleDevelopmentGroup@hdsupply.com';
      l_invoke_api      NUMBER                                 := 0;
      l_display_msg     VARCHAR2 (4000)                        := NULL;
   BEGIN
      fnd_message.CLEAR;

-- --------------------------------------------------------------
-- Validating Organization
-- --------------------------------------------------------------
  /*     IF p_org_code IS NOT NULL
      THEN
         BEGIN
            SELECT organization_id
              INTO l_org_id
              FROM mtl_parameters
             WHERE organization_code = 'MST';
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               -- fnd_message.set_name ('INV', 'INV-NO ORG INFORMATION');
                --RAISE xxwc_error;
               l_invoke_api := 1;
               l_display_msg :=
                            'Organization MST is not valid.';
         END;
      ELSE */
      BEGIN
         SELECT organization_id
           INTO l_org_id
           FROM mtl_parameters
          WHERE organization_code = 'MST';
           EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               -- fnd_message.set_name ('INV', 'INV-NO ORG INFORMATION');
                --RAISE xxwc_error;
               l_invoke_api := 1;
               l_display_msg :=
                            'Organization MST is not valid.';
         END;
    --  END IF;

      -- validate inventory item number
      BEGIN
         SELECT inventory_item_id
           INTO l_item_id
           FROM mtl_system_items_b
          WHERE organization_id = l_org_id AND segment1 = p_item_number;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            --  fnd_message.set_name ('INV', 'INV_INVALID_ITEM_ORG');
            --  RAISE xxwc_error;
            l_invoke_api := 1;
            l_display_msg :=
               l_display_msg || ' Item ' || p_item_number || ' is not valid.';
      END;

      -- validate cross-reference type
      BEGIN
         SELECT cross_reference_type
           INTO l_xref_type
           FROM mtl_cross_reference_types
          WHERE cross_reference_type = p_cross_type
            AND TRUNC (SYSDATE) <= NVL (disable_date, SYSDATE + 1);
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
             --fnd_message.set_name ('INV', 'INV_XREF_TYPE_INACTIVE');
            -- fnd_message.set_token ('CROSS_REFERENCE_TYPE', P_CROSS_TYPE);
            l_invoke_api := 1;
            l_display_msg :=
                  l_display_msg
               || ' Cross Refrence Type '
               || p_cross_type
               || ' is not valid.';
      END;

      -- validate cross-reference code
/*       IF (    (p_new_xref_code IS NULL)
          AND (   (p_transaction_type = 'UPDATE')
               OR (p_transaction_type = 'CREATE')
              )
         )
      THEN
         fnd_message.set_name ('INV', 'INV_XREF_NULL_COLS');
         RAISE xxwc_error;
      END IF; */
      IF ((p_transaction_type = 'DELETE') AND (p_xref_code IS NULL))
      THEN
         l_invoke_api := 1;
         l_display_msg :=
                    l_display_msg || ' DELETE XREF_CODE cannot be blank.';
      END IF;

      IF ((p_transaction_type = 'DELETE') AND (p_new_xref_code IS NOT NULL))
      THEN
         l_invoke_api := 1;
         l_display_msg :=
                l_display_msg || ' DELETE NEW_XREF_CODE should be blank.';
      END IF;

      IF ((p_transaction_type = 'UPDATE') AND (p_new_xref_code IS NULL))
      THEN
         l_invoke_api := 1;
         l_display_msg :=
                l_display_msg || ' UPDATE NEW_XREF_CODE cannot be blank.';
      END IF;

      IF ((p_transaction_type = 'UPDATE') AND (p_xref_code IS NULL))
      THEN
         l_invoke_api := 1;
         l_display_msg :=
                    l_display_msg || ' UPDATE XREF_CODE cannot be blank.';
      END IF;

      IF ((p_transaction_type = 'CREATE') AND (p_new_xref_code IS NULL))
      THEN
         l_invoke_api := 1;
         l_display_msg :=
                l_display_msg || ' CREATE NEW_XREF_CODE cannot be blank.';
      END IF;

      IF ((p_transaction_type = 'CREATE') AND (p_xref_code IS NOT NULL))
      THEN
         l_invoke_api := 1;
         l_display_msg :=
                    l_display_msg || ' CREATE XREF_CODE should be blank.';
      END IF;

      IF (    (p_new_xref_code IS NOT NULL)
          AND (   (p_transaction_type = 'UPDATE')
               OR (p_transaction_type = 'CREATE')
              )
         )
      THEN
         BEGIN
            SELECT cross_reference
              INTO l_upc_code
              FROM mtl_cross_references_b
             WHERE cross_reference_type = p_cross_type
            /*    AND DECODE (organization_id,
                           NULL, org_independent_flag,
                           organization_id
                          ) = DECODE (p_org_code, NULL, 'Y', l_org_id) */
               AND inventory_item_id = l_item_id
               AND cross_reference = p_new_xref_code;

            -- fnd_message.set_name ('INV', 'INV_CROSS_REF_EXISTS');
            -- RAISE xxwc_error;
            l_invoke_api := 1;
            l_display_msg :=
                  l_display_msg
               || ' Cross Refrence '
               || p_new_xref_code
               || ' already exists.';
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               NULL;
         END;
      END IF;

      IF (    (p_xref_code IS NOT NULL)
          AND (   (p_transaction_type = 'UPDATE')
               OR (p_transaction_type = 'DELETE')
              )
         )
      THEN
         BEGIN
            SELECT cross_reference_id
              INTO l_xref_id
              FROM mtl_cross_references_b
             WHERE cross_reference_type = p_cross_type
              /*  AND DECODE (organization_id,
                           NULL, org_independent_flag,
                           organization_id
                          ) = DECODE (p_org_code, NULL, 'Y', l_org_id) */
               AND inventory_item_id = l_item_id
               AND cross_reference = p_xref_code;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
                -- fnd_message.set_name ('INV', 'INV_CROSS_REF_NOT_EXISTS');
               --  RAISE xxwc_error;
               l_invoke_api := 1;
               l_display_msg :=
                     l_display_msg
                  || ' Cross Refrence '
                  || p_xref_code
                  || ' does not exists.';
         END;
      END IF;

      IF (l_invoke_api = 1)
      THEN
         fnd_message.set_name ('XXWC', 'XXWCEGO_XREF_ERROR');
         fnd_message.set_token ('VALUE', l_display_msg);
         RAISE xxwc_error;
      END IF;

      l_err_callpoint := 'Before Org independent flag logic';

      IF (l_invoke_api = 0)
      THEN
       /*   IF p_org_code IS NULL
         THEN */
            l_xref_tbl (1).organization_id := NULL;
            l_xref_tbl (1).org_independent_flag := 'Y';
    /*      ELSE
            l_xref_tbl (1).organization_id := l_org_id;
            l_xref_tbl (1).org_independent_flag := 'N';
         END IF; */

         l_err_callpoint := 'Before populating API variables';
         l_xref_tbl (1).inventory_item_id := l_item_id;
         l_xref_tbl (1).cross_reference_type := p_cross_type;
         l_xref_tbl (1).cross_reference := p_new_xref_code;
      --   l_xref_tbl (1).description := p_description;
         l_xref_tbl (1).transaction_type := p_transaction_type;
         -- CREATE, UPDATE, DELETE
         l_xref_tbl (1).cross_reference_id := l_xref_id;
         l_xref_tbl (1).last_update_date := SYSDATE;
         l_xref_tbl (1).last_updated_by := fnd_global.user_id;
         l_xref_tbl (1).creation_date := SYSDATE;
         l_xref_tbl (1).created_by := fnd_global.user_id;
         l_xref_tbl (1).last_update_login := fnd_global.login_id;
         l_xref_tbl (1).request_id := fnd_global.conc_request_id;
         l_xref_tbl (1).program_application_id := fnd_global.prog_appl_id;
         l_xref_tbl (1).program_id := fnd_global.conc_program_id;
         l_xref_tbl (1).program_update_date := SYSDATE;
         l_err_callpoint :=
                        'Before call to MTL_CROSS_REFERENCES_PUB.Process_XRef';
         mtl_cross_references_pub.process_xref
                                          (p_api_version        => 1.0,
                                           p_init_msg_list      => fnd_api.g_true,
                                           p_commit             => fnd_api.g_false,
                                           p_xref_tbl           => l_xref_tbl,
                                           x_return_status      => l_return_status,
                                           x_msg_count          => l_msg_count,
                                           x_message_list       => l_message_list
                                          );

         IF l_return_status <> 'S'
         THEN
            FOR ndx IN 1 .. l_message_list.COUNT
            LOOP
               l_api_error_msg :=
                  l_api_error_msg || ', '
                  || l_message_list (ndx).MESSAGE_TEXT;
            END LOOP;

            l_display_msg :=
                            l_display_msg || ' API Error: ' || l_api_error_msg;
            fnd_message.set_name ('XXWC', 'XXWCEGO_XREF_ERROR');
            fnd_message.set_token ('VALUE', l_display_msg);
            RAISE xxwc_error;
         END IF;

         COMMIT;
      END IF;

      l_err_callpoint := 'Before End';
   EXCEPTION
      WHEN xxwc_error
      THEN
         ROLLBACK;
      WHEN OTHERS
      THEN
         ROLLBACK;
         l_err_msg :=
               'Error in '
            || l_err_callpoint
            || ' WITH  '
            || SUBSTR (SQLERRM, 1, 3900);
         xxcus_error_pkg.xxcus_error_main_api
                                        (p_called_from            => l_err_callfrom,
                                         p_calling                => l_err_callpoint,
                                         p_ora_error_msg          => SQLERRM,
                                         p_error_desc             => l_err_msg,
                                         p_distribution_list      => l_distro_list,
                                         p_module                 => l_module
                                        );
   END upload_xref;
END xxwc_ego_item_cross_ref_pkg;
/
