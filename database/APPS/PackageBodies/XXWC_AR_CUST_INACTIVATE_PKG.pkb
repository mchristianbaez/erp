CREATE OR REPLACE PACKAGE BODY APPS.XXWC_AR_CUST_INACTIVATE_PKG
AS
   /****************************************************************************************************
    *
    * FUNCTION | PROCEDURE | CURSOR
    *  XXWC_AR_CUST_INACTIVATE_PKG
    *
    * DESCRIPTION
    *  FIN / Mass update of accounts that haven't had activity within the last XX month
    *
    * PARAMETERS
    * ==========
    * NAME              TYPE     DESCRIPTION
   .* ----------------- -------- ------------------------------------------------------------------------
    * P_MODE           <IN>       Report Only shows only report and commit will inactivate sites
    * P_CLASS          <IN>       Class name
    * P_MONTH          <IN>       No of months
    * P_AS_OF_DATE     <IN>       Report run date
    * P_EMAIL          <IN>       Email report output needs to be send
    *
    *
    * CALLED BY
    *   Which program, if any, calls this one
    *
    * HISTORY
    * =======
    *
    * VERSION DATE        AUTHOR(S)       DESCRIPTION
    * ------- ----------- --------------- ----------------------------------------------------------------
    * 1.0    05/21/2015   Maharajan S     Initial Creation - TMS#20130909-00768 Inactivate accounts that 
    *                                     haven't had activity within the last XX months
    * 2.0    05/21/2015   Maharajan S     TMS#20130909-00768 Changed email address to parameter
	* 3.0    11/28/2016   Pattabhi Avula  TMS#20160606-00273 FTP helper developed in UC4 for output 
	*                                     file transfer to destination shared drive as below
	*                                     P:\087 Accounts Receivable\Inactivated Accounts Sites Reports
	*
    *                                     
    ******************************************************************************************************/

   PROCEDURE XXWC_CUST_INACTIVATE(errbuf        OUT VARCHAR2,
                                  retcode       OUT NUMBER,
                                  p_resp        IN  VARCHAR2,
                                  P_resp_status IN  VARCHAR2,
                                  p_mode        IN  VARCHAR2,
                                  p_month       IN  NUMBER,
                                  p_date        IN  DATE,
                                  p_email       IN  VARCHAR2)
   IS
      
      p_cust_account_rec              HZ_CUST_ACCOUNT_V2PUB.CUST_ACCOUNT_REC_TYPE;
      p_party_rec                     HZ_PARTY_V2PUB.PARTY_REC_TYPE;
      p_organization_rec              HZ_PARTY_V2PUB.ORGANIZATION_REC_TYPE;
      l_data_file_name             VARCHAR2 (1000);
      l_path                          VARCHAR2 (80)  := 'XXWC_INACTIVE_CUST_SITE';                       
      l_header                        VARCHAR2 (2000) := 'Account Number|Account Name|Collector|Profile Class|Last Sale Date|Last Payment Date|Order Date|Account 
Receivable balance';
      l_sec                              VARCHAR2(200);      
      l_bill_to_customer              NUMBER;
      l_bill_sale_date                DATE;
      l_ship_to_customer              NUMBER;
      l_ship_sale_date                DATE;
      l_customer_id                   NUMBER;
      l_payment_date                  DATE ;
      l_account_bal                   NUMBER;
      l_sold_to_org_id                NUMBER;
      l_order_date                    DATE;
      l_profile_id                    NUMBER;
      l_account_name                  VARCHAR2 (2000);
      l_return_status                 VARCHAR2 (2000);
      l_msg_count                     NUMBER;
      l_msg_data                      VARCHAR2 (2000);
      l_detail                   VARCHAR2 (5000);
      l_child_requests              VARCHAR2 (2);
      l_email                      fnd_user.email_address%TYPE := NULL;
      l_req_id                     NUMBER := 0;
      l_request_id                NUMBER := 0;
      l_cp_long_name                  fnd_concurrent_programs_tl.user_concurrent_program_name%TYPE        := NULL;
      l_distro_list                   VARCHAR2 (75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
      l_msg                           VARCHAR2 (150);
      l_output_file_id                UTL_FILE.FILE_TYPE;
      
     
   BEGIN

      l_req_id := fnd_global.conc_request_id;

      fnd_file.put_line (fnd_file.LOG, 'Print Request id : '||l_req_id);
      fnd_file.put_line (fnd_file.LOG, 'Report only: '||p_mode);

      ----------------------------------------------------------------
      -- Opening file
      ----------------------------------------------------------------

         BEGIN

           l_sec := ' Opening file to write data';

            l_data_file_name :=
               'HDS_Inactive_customers' || '_' || l_req_id || '.txt';

 --Opening file
            BEGIN
            l_output_file_id := UTL_FILE.fopen (l_path, l_data_file_name, 'w');
            EXCEPTION
            WHEN OTHERS THEN
             fnd_file.put_line (
               fnd_file.LOG,
                  'Error opening file '
               || l_data_file_name
               || 'in directory '
               || l_path||SQLERRM);
            END;

 --Writing into file
           BEGIN     
           UTL_FILE.put_line (l_output_file_id, l_header);
           EXCEPTION
            WHEN OTHERS THEN
             fnd_file.put_line (
               fnd_file.LOG,
                  'Error opening file '
               || l_data_file_name
               || 'in directory '
               || l_path||SQLERRM);
            END;

      ----------------------------------------------------------------
      -- Open cursor for writing into output file
      ----------------------------------------------------------------

 l_sec := ' Opening cursor';

 FOR I IN (SELECT /*+leading(hca)*/
      cust.account_number,
       cust.account_name,
       cust.cust_account_id,
       party.party_number,
       party.party_name,
       party.party_id,
       coll.name collector,
       pc.name profile_class,
       NVL (cust.object_version_number, 1) cust_object_version_number,
       NVL (party.object_version_number, 1) party_object_version_number
  FROM AR.hz_cust_accounts cust,
       AR.hz_parties party,
       AR.hz_customer_profiles prof,
       AR.hz_cust_profile_classes pc,
       AR.ar_collectors coll
 WHERE     cust.party_id = party.party_id
       AND cust.cust_account_id = prof.cust_account_id
       AND prof.profile_class_id = pc.profile_class_id
       AND prof.collector_id = coll.collector_id(+)
       AND NVL (cust.status, 'A') = 'A'
       AND NVL (party.status, 'A') = 'A'
       AND prof.site_use_id IS NULL
       AND pc.name NOT IN ('DEFAULT',
                           'Intercompany Customers',
                           'WC Branches',
                           'Branch Cash Account')
       AND NOT EXISTS
              (SELECT 1
                 FROM hz_party_sites hps,
                      hz_cust_acct_sites hcsa,
                      hz_cust_site_uses hcsu
                WHERE hps.party_id = party.party_id
                 AND  hps.party_site_id = hcsa.party_site_id
                 AND  hcsa.cust_acct_site_id = hcsu.cust_acct_site_id 
                 AND  hps.status = 'A'
                 AND  hcsu.site_use_code != 'BILL_TO')
       AND cust.creation_date < ADD_MONTHS (
                   NVL (
                      P_DATE,
                      TO_DATE (TO_CHAR (SYSDATE, 'DD-MON-YYYY'),
                               'DD-MON-YYYY')),
                   (-1) * p_month))
        LOOP

    l_sec := 'Get last bill to sale date'; 
           
    BEGIN
         SELECT bill_to_customer_id, 
            MAX (trx_date)
           INTO l_bill_to_customer, 
            l_bill_sale_date
        FROM apps.ra_customer_trx       
          WHERE bill_to_customer_id = i.cust_account_id
       GROUP BY bill_to_customer_id;
    EXCEPTION
       WHEN OTHERS
       THEN
          l_bill_to_customer := NULL;
          l_bill_sale_date := NULL;
    END;

    l_sec := 'Get last ship to sale date';         

    BEGIN
            SELECT   ship_to_customer_id,
                     MAX (trx_date)
                INTO l_ship_to_customer,
                     l_ship_sale_date
            FROM apps.ra_customer_trx   
            where ship_to_customer_id = i.cust_account_id
            GROUP BY ship_to_customer_id;
        EXCEPTION
        WHEN OTHERS THEN
          l_ship_to_customer := NULL;
          l_ship_sale_date := NULL;
        END;
       
    l_sec := 'Get last payment date'; 
        
    BEGIN
        SELECT   customer_id,
                 MAX (TRUNC (last_update_date)),
                 SUM (amount_due_remaining) 
            INTO l_customer_id,
                 l_payment_date,
                 l_account_bal
         FROM apps.ar_payment_schedules     
         WHERE customer_id= i.cust_account_id
         GROUP BY customer_id;
         EXCEPTION
         WHEN OTHERS THEN
          l_customer_id:= NULL;
          l_payment_date     := NULL;
          l_account_bal := 0;
        END;
        
        l_sec := 'Check if any open SO '; 
       
        BEGIN
        SELECT oeh.sold_to_org_id,
              MAX(oeh.ordered_date)
         INTO l_sold_to_org_id,
              l_order_date 
        FROM apps.oe_order_headers oeh     
        WHERE oeh.sold_to_org_id = i.cust_account_id
         AND EXISTS ( SELECT 1 FROM oe_order_headers ooh 
                           WHERE ooh.sold_to_org_id = oeh.sold_to_org_id
                            AND ooh.flow_status_code IN ('ENTERED','BOOKED','DRAFT','PENDING_CUSTOMER_ACCEPTANCE'))
        GROUP BY oeh.sold_to_org_id;
        EXCEPTION
        WHEN OTHERS THEN
           l_sold_to_org_id := NULL;
           l_order_date := NULL;
        END;
        

       l_sec := 'Checking if any activity within N period of time'; 

  IF  (l_payment_date  IS NULL OR l_payment_date  < ADD_MONTHS (
                   NVL (
                      P_DATE,
                      TO_DATE (TO_CHAR (SYSDATE, 'DD-MON-YYYY'),
                               'DD-MON-YYYY')),
                   (-1) * p_month))
        AND l_account_bal = 0
        AND (l_bill_sale_date IS NULL OR l_ship_sale_date IS NULL OR NVL(l_bill_sale_date,l_ship_sale_date)  < ADD_MONTHS (
                   NVL (
                      P_DATE,
                      TO_DATE (TO_CHAR (SYSDATE, 'DD-MON-YYYY'),
                               'DD-MON-YYYY')),
                   (-1) * p_month))
         AND l_order_date IS NULL THEN

       l_sec := 'Check for commit mode'; 


  IF p_mode = 'N' THEN 

    BEGIN                   
          p_cust_account_rec.cust_account_id := I.cust_account_id;
          p_cust_account_rec.status := 'I';

         BEGIN
         HZ_CUST_ACCOUNT_V2PUB.update_cust_account (
            p_init_msg_list           => 'T',
            p_cust_account_rec        => p_cust_account_rec,
            p_object_version_number   => I.cust_object_version_number,
            x_return_status           => l_return_status,
            x_msg_count               => l_msg_count,
            x_msg_data                => l_msg_data);
         EXCEPTION
         WHEN OTHERS THEN
           fnd_file.put_line (FND_FILE.LOG,'Exception in update account API for customer '
               || I.account_number
               || ' '
               || l_msg_data);
         END;
         
         IF l_return_status <> 'S'
         THEN
            fnd_file.put_line (FND_FILE.LOG,'Issue Inactivating Account for customer '
               || I.account_number
               || ' '
               || l_msg_data);
         ELSE
             COMMIT;
            fnd_file.put_line (FND_FILE.LOG,'Successfully updated Account for customer '
               || I.account_number
               || ' '
               || l_msg_data);
         END IF;
     END;              

 END IF;                      
      
             l_detail :=
                        i.account_number
                     || '|'
                     || i.account_name
                     || '|'
                     || i.party_number
                     || '|'
                     || i.party_name
                     || '|'
                     || i.collector
                     || '|'
                     || i.profile_class
                     || '|'
                     ||NVL(l_ship_sale_date,l_bill_sale_date)
                     || '|'
                     || l_payment_date
                     || '|'
                     || l_order_date
                     || '|'
                     || l_account_bal;

--writing detail into file
               BEGIN
                  UTL_FILE.put_line (l_output_file_id, l_detail);
               EXCEPTION
 	       WHEN OTHERS THEN
               fnd_file.put_line (
                   fnd_file.LOG,
                  	'Error writing file with details '||SQLERRM);
               		
              END;                  
     END IF;
   END LOOP;


            fnd_file.put_line (
               fnd_file.LOG,
                  'All detail records are copied into the file '
               || l_path
               || '/'
               || l_data_file_name);

--Closing file
           BEGIN
            UTL_FILE.fclose (l_output_file_id);
            EXCEPTION
 	       WHEN OTHERS THEN
               fnd_file.put_line (
                   fnd_file.LOG,
                  	'Error closing file '||SQLERRM);
               		
              END;   

      ------------------------------------------------------------------------------------------
      --create a file to point us where the data file is located along with the absolute path.          
      ------------------------------------------------------------------------------------------

          l_sec := ' Creating file to point data file location in report mode';
            
            l_data_file_name := 'zip_file_extract_' || l_req_id || '.txt';

            l_output_file_id := UTL_FILE.fopen (l_path, l_data_file_name, 'w');

            fnd_file.put_line (
               fnd_file.LOG,
                  'After opening fileid for file '
               || l_path
               || '/'
               || l_data_file_name);
         
      ------------------------------------------------------------------------------------------
      --  Getting the direcoty path from direcoty name         
      ------------------------------------------------------------------------------------------
          l_sec := ' Getting directory path';
  
              SELECT directory_path
               INTO l_path
              FROM all_directories 
              WHERE directory_name = 'XXWC_INACTIVE_CUST_SITE';
       --End>

            UTL_FILE.put_line (
               l_output_file_id,
                  l_path
               || '/'
               || 'HDS_Inactive_customers'
               || '_'
               || l_req_id
               || '.txt');

            UTL_FILE.fclose (l_output_file_id);
            fnd_file.put_line (
               fnd_file.LOG,
               'After close of file ' || l_path || '/' || l_data_file_name);

            l_child_requests := 'OK';
         EXCEPTION
            WHEN OTHERS
            THEN
               l_child_requests := 'NA';
         END;

 -- <START> Ver#3.0  
  /*      fnd_file.put_line (fnd_file.LOG, 'Before IF clause ' || p_email);  
         
      ------------------------------------------------------------------------------------------
      --  Submitting the emailing program         
      ------------------------------------------------------------------------------------------
   l_sec := ' Submitting email program in report mode';

         IF l_child_requests <> 'NA'
         THEN
            l_cp_long_name := 'XXWC Inactive Customers Update';
            l_email :=   p_email;                  --'maharajan.shunmugam@hdsupply.com'; --p_email;  --commented email address and added p_email for ver2.0
            fnd_file.put_line (fnd_file.LOG, 'In IF clause ' || l_email);          

            l_request_id :=
               fnd_request.submit_request (
                  application   => 'XXCUS',
                  program       => 'XXCUS_PEREND_POSTPROCESSOR',
                  description   => 'Email with attachment',
                  start_time    => '',
                  sub_request   => FALSE,
                  argument1     => l_req_id,
                  argument2     =>    l_path
                                   || '/zip_file_extract_'
                                   || l_req_id
                                   || '.txt',
                  argument3     => l_cp_long_name,
                  argument4     => l_email,
                  argument5     =>    'HDS_Inactive_customers'
                                   || '_'
                                   || l_req_id
                                   || '.zip',
                  argument6     => 'HDS Inactive Customers Report');

            
            IF l_request_id > 0
            THEN
               fnd_file.put_line (
                  fnd_file.LOG,
                  'Successfully submitted program XXHDS Period End Post Processor Program to send email');
            ELSE
               fnd_file.put_line (
                  fnd_file.LOG,
                  'Failed to submit send email program XXHDS Period End Post Processor Program');
            END IF;
         ELSE                                         --v_child_requests ='NA'
            NULL;
         END IF;  */
		  -- <ENDS> Ver#3.0
   EXCEPTION
      WHEN OTHERS
      THEN
          l_msg := 'Error: ' || SQLERRM;
         fnd_file.put_line (FND_FILE.LOG,'Error in XXWC_AR_CUST_INACTIVATE_PKG');
         xxcus_error_pkg.xxcus_error_main_api
         (p_called_from            => 'XXWC_AR_CUST_INACTIVATE_PKG.XXWC_CUST_INACTIVATE',
          p_calling                => l_sec,
          p_request_id             => l_req_id,
          p_ora_error_msg          =>  SUBSTR(DBMS_UTILITY.format_error_stack ()|| DBMS_UTILITY.format_error_backtrace (), 1,2000),
          p_error_desc             => 'WHEN OTHERS EXCEPTION in XXWC_AR_CUST_INACTIVATE_PKG',
          p_distribution_list      => l_distro_list,
          p_module                 => 'AR'
         );
   END;
END;
/