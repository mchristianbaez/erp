CREATE OR REPLACE PACKAGE BODY APPS.xxcus_ar_tax_return_pkg
/****************************************************************************************************************************************
 * PROGRAM NAME
 *  xxcus_ar_tax_return_pkg.pkb
 * 
 * IMPORTANT NOTE: SVN revisions 3776, 3771 and 3767 must not be used as base for any new tickets 
 * 
 * DESCRIPTION
 *  Used to generate the HDS tax detail report for the tax team.
 *
 * DEPENDENCIES
 *   None.
 *
 * CALLED BY
 *   Request Set: HDS AR Tax Audit Return Report Set
 *
 * LAST UPDATE DATE: 07/10/2014
 *
 * HISTORY
 * =======
 *
 * ESMS               DATE        AUTHOR(S)          DESCRIPTION
 * --------           ----------- ---------------    ------------------------------------
 * 194652              04/02/2013    Balaguru Seshadri  Created
 * 194654
 * 194655
 * 200494              05/10/2013   Balaguru Seshadri  Changes to transform_taxaudit_tax logic 
 * 216141              08/04/2013   Balaguru Seshadri  First four items of the ticket only
 * 224619              09/30/2013  Balaguru Seshadri   Remove $.05 variance filter from WC Sales tax reports.
 * 435030[Incident]    10/08/2013  Balaguru Seshadri   A custom logic forces the taxware amounts not to  print if the taxware_match field is NO.
 * 220662              01/14/2014 Balaguru Seshadri    Note: Taxware Summary Report to show only three columns period, invoice# and total tax. Added summary file to create_files. 
 * 234373              01/14/2014 Balaguru Seshadri    Note: Taxware Summary Report to show only three columns period, invoice# and total tax. Added summary file to create_files.
 * 20140127-00180[TMS] 07/14/2014 Balaguru Seshadri   Changes to transform_taxaudit_tax routine to use invlinenum field from taxware.taxaudit_detail table
 * 269062              11/05/2014 Balaguru Seshadri   Add regexp_replace function in the main routine for ship and bill to address.
 * 565858[Incident]    03/05/2015  Balaguru Seshadri  Function unload_data declaration modified. Commented parallel and partition clause
 * 575152[Incident]    04/14/2015  Balaguru Seshadri  Added email function to the main routine if there are any errors during insert of reporting 
 *                                                    summary and detail table. Af the end we check if there are any errors we force the concurrent request
 *                                                    to error.
 *                                                    Note: SVN revisions 3776, 3771 and 3767 need to be excluded and not used as base for any new tickets
 *                                                   Base version for changes related to Incident:575152 is 3655. 
 * 287127              05/04/2015  Balaguru Seshadri Fix routine transform_taxaudit_tax cursor trx_lines     
 * 287127              05/05/2015  Balaguru Seshadri Fix routine transform_taxaudit_tax cursor all_trx, add new field total_prod_lines 
 ***************************************************************************************************************************************/
AS
/**************************************************************************
 *
 * FUNCTION: print_log
 * DESCRIPTION: To print messages either in a SQL buffer or through a concurrent log file.
 * PARAMETERS
 * ==========
 * NAME              TYPE     DESCRIPTION
.* ----------------- -------- ---------------------------------------------
 * p_message        <IN>       Any string enclosed within quotes
 * RETURN VALUE: None
 * CALLED BY: Multiple
 *
 *************************************************************************/ 
  procedure print_log(p_message in varchar2) is
  begin 
   if fnd_global.conc_request_id >0 then
    fnd_file.put_line(fnd_file.log, p_message);
   else
    dbms_output.put_line(p_message);
   end if;
  exception
   when others then
    fnd_file.put_line(fnd_file.log, 'Issue in print_log routine ='||sqlerrm);
  end print_log;
  --
 /**************************************************************************************************************
 *
 * FUNCTION: capture_extract_errors
 * DESCRIPTION: To copy all records that were not inserted into the reporting summary and detail tables
 * PARAMETERS
 * ==========
 * NAME                 TYPE     DESCRIPTION
.* -----------------    -------- ---------------------------------------------
 * p_record_type          IN     SUMMARY_DATA  or DETAIL_DATA
 * p_customer_trx_id      IN     Oracle customer_trx_id for an invoice.
 * p_trx_number           IN     Invoice Number
 * p_period_name          IN     Fiscal Period
 * p_request_id           IN     Concurrent request id
 * RETURN VALUE: None     
 * CALLED BY: Multiple
 *
 *************************************************************************************************************/   
  procedure capture_extract_errors 
       (
         p_record_type     varchar2
        ,p_customer_trx_id number
        ,p_trx_number      varchar2
        ,p_period_name     varchar2
        ,p_request_id      number
       ) IS
     PRAGMA AUTONOMOUS_TRANSACTION;
   BEGIN
     insert into xxcus.xxcus_artax_extract_errors_all
      (
          record_type     --VARCHAR2(30)
         ,customer_trx_id --NUMBER
         ,trx_number      --VARCHAR2(30)
         ,period_name     --VARCHAR2(20)  
         ,request_id      --NUMBER    
      )
     Values
      (
          p_record_type
         ,p_customer_trx_id
         ,p_trx_number
         ,p_period_name
         ,p_request_id     
      );
     COMMIT;
   END capture_extract_errors;  
  --   
/**************************************************************************
 *
 * FUNCTION: get_exempt_amount
 * DESCRIPTION: To return the exemption amount for an invoice line.
 * PARAMETERS
 * ==========
 * NAME              TYPE     DESCRIPTION
.* ----------------- -------- ---------------------------------------------
 * p_invoice_id      IN       AR invoice id
 * p_line_id         In       AR invoice line id   
 * RETURN VALUE: Number
 * CALLED BY: main
 *************************************************************************/  
  function get_exempt_amount (p_invoice_id in number, p_line_id in number) return number is
   n_exempt_amount number :=0;
  begin  
    select nvl(sum(prod_lines.extended_amount), 0)
    into   n_exempt_amount
    from   ra_customer_trx_lines prod_lines   
    where  1 =1
      and  prod_lines.customer_trx_id =p_invoice_id
      and  prod_lines.customer_trx_line_id =nvl(p_line_id, prod_lines.customer_trx_line_id)
      and  line_type ='LINE'
      and not exists
         (
           select customer_trx_id
           from   ra_customer_trx_lines tax_lines
           where  1 =1
             and  tax_lines.customer_trx_id          =p_invoice_id
             and  tax_lines.link_to_cust_trx_line_id =prod_lines.customer_trx_line_id
             and  tax_lines.line_type                ='TAX'
             and  tax_lines.extended_amount          <>0
         );
     return n_exempt_amount;
  exception
   when no_data_found then
    return n_exempt_amount;
   when others then
    print_log('Issue while fetching get_exempt_amount for customer_trx_id ='||p_invoice_id);
    print_log('Issue while fetching get_exempt_amount, message ='||SQLERRM);
    return n_exempt_amount; 
  end get_exempt_amount;
/**************************************************************************
 *
 * FUNCTION: get_taxable_amount
 * DESCRIPTION: To return the tax amount for an invoice line.
 * PARAMETERS
 * ==========
 * NAME              TYPE     DESCRIPTION
.* ----------------- -------- ---------------------------------------------
 * p_invoice_id      IN       AR invoice id
 * p_line_id         In       AR invoice line id   
 * RETURN VALUE: Number
 * CALLED BY: main
 *************************************************************************/
  function get_taxable_amount (p_invoice_id in number, p_line_id in number) return number is
   n_taxable_amount number :=0;
  begin  
    select nvl(sum(prod_lines.extended_amount), 0)
    into   n_taxable_amount
    from   ra_customer_trx_lines prod_lines   
    where  1 =1
      and  prod_lines.customer_trx_id      =p_invoice_id
      and  prod_lines.customer_trx_line_id =nvl(p_line_id, prod_lines.customer_trx_line_id)
      and  line_type ='LINE'
      and  exists
         (
           select customer_trx_id
           from   ra_customer_trx_lines tax_lines
           where  1 =1
             and  tax_lines.customer_trx_id          =p_invoice_id
             and  tax_lines.link_to_cust_trx_line_id =prod_lines.customer_trx_line_id
             and  tax_lines.line_type                ='TAX'
             and  tax_lines.extended_amount          <>0
         );
     return n_taxable_amount;
  exception
   when no_data_found then
    return n_taxable_amount;
   when others then
    print_log('Issue while fetching get_taxable_amount for customer_trx_id ='||p_invoice_id);
    print_log('Issue while fetching get_taxable_amount , message ='||sqlerrm);    
    return n_taxable_amount; 
  end get_taxable_amount; 
/**************************************************************************
 *
 * FUNCTION: get_location_based_tax
 * DESCRIPTION: To return the different tax amounts for an invoice line.
 * PARAMETERS
 * ==========
 * NAME              TYPE     DESCRIPTION
.* ----------------- -------- ---------------------------------------------
 * p_invoice_id      IN       AR invoice id
 * p_line_id         In       AR invoice line id 
 * p_county_amt      Out      number
 * p_state_amt       Out      number
 * p_city_amt        Out      number
 * RETURN VALUE: State, City and county taxes
 * CALLED BY: main
 *************************************************************************/  
  procedure get_location_based_tax 
   (
     p_invoice_id in  number
    ,p_line_id    in  number
    ,p_state_amt  out number
    ,p_county_amt out number
    ,p_city_amt   out number
   ) is
   cursor location_based_taxes is 
     select zxl.tax_rate_code tax_rate_code, tax, nvl(sum(nvl(zxl.tax_amt ,0)), 0) tax_amt
     from zx_lines zxl
     where 1 =1
       and application_id =222 --when using zx_lines fetch records sourced from AR and not other applications 
       and trx_id =p_invoice_id
       and trx_line_id =nvl(p_line_id, trx_line_id)
     group by zxl.tax_rate_code, zxl.tax;   
  begin  
     for rec in location_based_taxes loop
       if (rec.tax_rate_code  ='STATE' AND rec.tax ='STATE') then
         p_state_amt  :=rec.tax_amt;
       elsif (rec.tax_rate_code ='COUNTY' AND rec.tax ='COUNTY') then
         p_county_amt :=rec.tax_amt;
       elsif (rec.tax_rate_code ='CITY' AND rec.tax ='CITY') then
         p_city_amt   :=rec.tax_amt; 
       elsif rec.tax_rate_code ='HDS-PRISM' then
         p_state_amt  :=rec.tax_amt;
         p_county_amt :=0;
         p_city_amt   :=0;
       elsif (rec.tax_rate_code ='STD' AND rec.tax ='STATE') then
         p_state_amt  :=rec.tax_amt;
       elsif (rec.tax_rate_code ='STD' AND rec.tax ='COUNTY') then
         p_county_amt :=rec.tax_amt;         
       elsif (rec.tax_rate_code ='STD' AND rec.tax ='CITY') then
         p_city_amt   :=rec.tax_amt;
       else
         print_log('Warning: We found a tax rate code in zx_lines that is not programmed to deal with for trx_id ='||p_invoice_id);
         p_state_amt  :=0;
         p_county_amt :=0;
         p_city_amt   :=0;
       end if;
     end loop;
  exception
   when no_data_found then
     p_state_amt  :=0;
     p_county_amt :=0;
     p_city_amt   :=0;
   when others then
    print_log('@get_location_based_tax, Issue while fetching get_taxable_amount for customer_trx_id ='||p_invoice_id);
    print_log('@get_location_based_tax, Issue while fetching get_taxable_amount for customer_trx_id ='||SQLERRM);    
     p_state_amt  :=0;
     p_county_amt :=0;
     p_city_amt   :=0; 
  end get_location_based_tax;  
/**************************************************************************
 *
 * FUNCTION: transform_taxaudit_tax
 * DESCRIPTION: To process invoices that are errored out during first try.
 * PARAMETERS
 * ==========
 * NAME              TYPE     DESCRIPTION
.* ----------------- -------- ---------------------------------------------
 * p_period          IN       Fiscal period id
 * p_gl_from_date    IN       Fiscal period start date
 * p_gl_to_date      IN       Fiscal period end date 
 * RETURN VALUE: None
 * CALLED BY: transform_taxaudit_tax [overloaded]
 *************************************************************************/  
 procedure transform_taxaudit_tax(p_period in varchar2, p_gl_from_date in date, p_gl_to_date in date) is
    cursor my_customers is
    select
       itr.creation_date                ar_creation_date
      ,itr.ship_to_customer_id          ship_to_customer_id
      ,itr.ship_to_site_use_id          ship_to_site_use_id
      ,itr.bill_to_customer_id          bill_to_customer_id
      ,itr.bill_to_site_use_id          bill_to_site_use_id
      ,itr.batch_source_id              batch_source_id
      ,itr.cust_trx_type_id             cust_trx_type_id             
      ,count(itr.customer_trx_id)       total_invoices
    from  xxcus.xxcus_taxaudit_exceptions itr 
    where 1 =1
    group by
       itr.creation_date 
      ,itr.ship_to_customer_id
      ,itr.ship_to_site_use_id
      ,itr.bill_to_customer_id
      ,itr.bill_to_site_use_id 
      ,itr.batch_source_id
      ,itr.cust_trx_type_id         
    order by
       itr.creation_date
      ,itr.ship_to_customer_id asc nulls first
      ,itr.ship_to_site_use_id asc nulls first
      ,itr.bill_to_customer_id
      ,itr.bill_to_site_use_id
      ,itr.batch_source_id;
      
   type my_customer_tbl is table of my_customers%rowtype index by binary_integer;
   my_customer_rec my_customer_tbl; 
   
   cursor my_trx1 is
    select  
           to_date(null)                   ar_creation_date
          ,to_date(null)                   ar_invoice_date
          ,to_char(null)                   process_flag
          ,to_char(null)                   period
          ,to_number(null)                 ship_to_customer_id
          ,to_number(null)                 ship_to_site_use_id
          ,to_number(null)                 bill_to_customer_id
          ,to_number(null)                 bill_to_site_use_id
          ,to_number(null)                 customer_trx_id 
          ,to_number(null)                 ar_taxware_detailno
          ,to_char(null)                   invoice_source 
          ,to_char(null)                   invoice_number 
          ,to_number(null)                 my_headerno     
    from   dual;   
    
    type my_trx_tbl is table of my_trx1%rowtype index by binary_integer;
    my_trx_rec my_trx_tbl; 
    
   cursor trx_lines(n_customer_trx_id in number) is 
    select                     
           lines.interface_line_attribute1  sales_order
          ,oeoh.header_id                   so_header_id
          ,oeol.line_id                     so_line_id
          ,lines.customer_trx_line_id       customer_trx_line_id
          ,lines.line_number                invoice_line
          ,oeol.line_number                 order_line  
          ,lines.inventory_item_id          item_id
          ,msi.segment1                     item_number
          ,lines.description                description  
          ,nvl( lines.quantity_invoiced
               ,lines.quantity_credited
              )                             invoice_qty
          ,lines.unit_selling_price         invoice_unit_price
          ,lines.extended_amount            extended_amount
          ,lines.warehouse_id               warehouse_id
          ,org.organization_code            branch
          ,case
            when (msi.segment1 ='DELIVERY CHARGE') then 
             (
              SELECT SHIPPING_METHOD_CODE
              FROM   OE_ORDER_HEADERS_ALL
              WHERE 1 =1
              AND ORDER_NUMBER =TO_NUMBER(lines.interface_line_attribute1)
             )
            else 
             (
                 nvl
                (
                  oeol.shipping_method_code
                 ,oeoh.shipping_method_code
                )             
             )
           end                              shipping_method_code
          --,oeol.shipping_method_code        shipping_method_code
          ,to_char(null)                    point_of_taxation
          ,to_char(null)                    ship_via
          ,to_char(null)                    branch_address1                                        
          ,to_char(null)                    branch_city
          ,to_char(null)                    branch_state
          ,to_char(null)                    branch_postal_code
          ,to_char(null)                    branch_county
          ,to_char(null)                    branch_geocode 
       from  ra_customer_trx_lines         lines
            ,oe_order_lines_all            oeol
            ,oe_order_headers_all          oeoh
            ,(
               select inventory_item_id item_id, segment1
               from   mtl_system_items a
               where 1 =1
                 and organization_id =222 --Just for looking up the item number, let's use the WC Item Master org
             ) msi
            ,org_organization_definitions  org             
       where  1 =1
         and  lines.customer_trx_id  =n_customer_trx_id -- 10000045733  1683901 
         and  lines.line_type        <>'TAX'         
         and  oeol.line_id(+)        =to_number(lines.interface_line_attribute6)
         and  oeoh.header_id(+)      =oeol.header_id
         and  org.organization_id(+) =lines.warehouse_id
         and  msi.item_id(+)         =lines.inventory_item_id
       order by lines.customer_trx_line_id asc; 
         
    type trx_lines_tbl is table of trx_lines%rowtype index by binary_integer;
    trx_lines_row trx_lines_tbl;    
    
    cursor my_txw_tax 
      (
        p_headerno in number
       ,p_detailno in number
      ) is
    select
           txwtax.headerno                  headerno
          ,txwtax.detailno                  detailno
          ,txwtax.taxlevel                  taxlevel
          ,txwtax.taxtype                   taxtype
          ,round(txwtax.taxamt, 2)          taxamt
          ,txwtax.taxrate                   taxrate
          ,txwtax.cmplcode                  cmplcode
          ,txwtax.exmptamt                  exmptamt
          ,txwtax.admncode                  admncode
          ,txwtax.taxcert                   taxcert
          ,txwtax.reascode                  reascode
          ,txwtax.baseamt                   baseamt
          ,txwtax.taxcode                   taxcode
          ,txwtax.excpcode                  excpcode
          ,txwtax.stepstatus                stepstatus
          ,txwtax.stepcomment               stepcomment  
          ,to_date(null)                    ar_creation_date
          ,to_date(null)                    ar_invoice_date   
          ,to_number(null)                  ship_to_customer_id
          ,to_number(null)                  ship_to_site_use_id
          ,to_number(null)                  bill_to_customer_id
          ,to_number(null)                  bill_to_site_use_id                        
          ,to_char(null)                    sales_order
          ,to_number(null)                  so_header_id
          ,to_number(null)                  so_line_id
          ,to_number(null)                  customer_trx_id
          ,to_number(null)                  ar_taxware_detailno        
          ,to_number(null)                  customer_trx_line_id
          ,to_number(null)                  invoice_line
          ,to_number(null)                  order_line  
          ,to_number(null)                  item_id
          ,to_char(null)                    item_number
          ,to_char(null)                    description  
          ,to_number(null)                  invoice_qty
          ,to_number(null)                  invoice_unit_price
          ,to_number(null)                  extended_amount
          ,to_number(null)                  warehouse_id
          ,to_char(null)                    period
          ,to_char(null)                    invoice_source 
          ,to_char(null)                    invoice_number
          ,to_char(null)                    branch
          ,to_number(null)                  batch_source_id
          ,txw_lines.custprodcode           custprodcode
          ,txw_lines.juristype              juristype
          ,txw_lines.pot                    pot
          ,txwjur.jurisno                   jurisno
          ,txwjur.stcode                    juris_stcode                  
          ,txwjur.cntycode                  juris_cntycode
          ,txwjur.geocode                   juris_geocode
          ,txwjur.zipcode                   juris_zipcode
          ,txwjur.zipextn                   juris_zipextn
          ,(
            select statename 
            from   taxware.stepstcd_tbl
            where  1 =1
              and  stcode =txwjur.stcode
           )                                juris_stname  
          ,txwjur.cntyname                  juris_cntyname
          ,txwjur.loclname                  juris_cityname
          ,to_char(null)                    point_of_taxation                   
          ,to_char(null)                    ship_method_code
          ,to_char(null)                    ship_via
          ,to_char(null)                    branch_address1
          ,to_char(null)                    branch_city                              
          ,to_char(null)                    branch_state
          ,to_char(null)                    branch_postal_code
          ,to_char(null)                    branch_county
          ,to_char(null)                    branch_geocode                    
       from   taxware.taxaudit_tax    txwtax
             ,taxware.taxaudit_header hdr
             ,taxware.taxaudit_detail txw_lines
             ,taxware.taxaudit_juris  txwjur
       where  1 =1
         and  hdr.headerno        =p_headerno
         and  txw_lines.headerno  =hdr.headerno
         and  txw_lines.detailno  =p_detailno
         and  txwtax.headerno     =hdr.headerno
         and  txwtax.detailno     =txw_lines.detailno
         and  txwjur.headerno     =txw_lines.headerno
         and  txwjur.jurisno      =txw_lines.jurisno
         and  txwjur.jurlevel     =txw_lines.juristype ;
         --and  txwtax.taxamt  <>0; 
    type my_txw_tax_tbl is table of my_txw_tax%rowtype index by binary_integer;
    my_txw_tax_row      my_txw_tax_tbl;
    my_txw_new_rows     my_txw_tax_tbl;
    
    type lc_refcursor is ref cursor;
    my_trx lc_refcursor;
    
    type err_lines_tbl is table of xxcus.xxcus_taxaudit_error_lines%rowtype index by binary_integer;
    err_lines err_lines_tbl;
    
    cursor fix_inv_with_2_lines is
    select customer_trx_id                       customer_trx_id
          ,headerno                              headerno
          ,sum(ebtax_line_total)                 ebtax_line_total
          ,sum(taxware_line_total)               taxware_tax_total
          ,'Y'                                   process_flag
          ,abs
           (
             sum(ebtax_line_total)
             -
             sum(taxware_line_total)          
           )                                     diff          
    from   xxcus.xxcus_taxaudit_error_lines a
    where  1 =1
      and  nvl(process_flag, 'N') ='N'
--      and  (headerno is not null and ar_taxware_detailno is not null)
    and not exists
    (
     select 1
     from   xxcus.xxcus_taxaudit_tax_archive b 
     where  1 =1
       and  b.customer_trx_id =a.customer_trx_id
    )
    group by customer_trx_id, headerno;
  
    type  fix_inv_lines_tbl is table of fix_inv_with_2_lines%rowtype index by binary_integer;                      
    two_lines_rec fix_inv_lines_tbl;
    
    cursor err_lines_fix (p_trx_id in number) is
    select *  
    from   xxcus.xxcus_taxaudit_error_lines
    where  1 =1
      and  customer_trx_id =p_trx_id;
      --and (ebtax_line_total-taxware_line_total) <>0 ;    
    
    type  err_lines_fix_tbl is table of err_lines_fix%rowtype index by binary_integer;                      
    two_lines_txw_rec err_lines_fix_tbl;    
    
    cursor unmatched_trx (p_trx_id in number) is
    select 
           headerno
          ,-99                              detailno
          ,to_char(null)                    taxlevel
          ,to_char(null)                    taxtype
          ,to_number(null)                  taxamt
          ,to_number(null)                  taxrate
          ,to_char(null)                    cmplcode
          ,to_number(null)                  exmptamt
          ,to_char(null)                    admncode
          ,to_char(null)                    taxcert
          ,to_char(null)                    reascode
          ,to_number(null)                  baseamt
          ,to_char(null)                    taxcode
          ,to_char(null)                    excpcode
          ,to_char(null)                    stepstatus
          ,to_char(null)                    stepcomment  
          ,ar_creation_date
          ,ar_invoice_date   
          ,ship_to_customer_id
          ,ship_to_site_use_id
          ,bill_to_customer_id
          ,bill_to_site_use_id                        
          ,sales_order
          ,so_header_id
          ,so_line_id
          ,customer_trx_id
          ,ar_taxware_detailno        
          ,customer_trx_line_id
          ,invoice_line
          ,order_line  
          ,item_id
          ,item_number
          ,description  
          ,invoice_qty
          ,invoice_unit_price
          ,extended_amount
          ,warehouse_id
          ,period_name
          ,invoice_source 
          ,invoice_number
          ,branch
          ,batch_source_id
          ,to_char(null) custprodcode
          ,to_char(null) juristype
          ,to_char(null) pot
          ,to_number(null)                  jurisno
          ,to_char(null)                    juris_stcode                  
          ,to_char(null)                    juris_cntycode
          ,to_char(null)                    juris_geocode
          ,to_char(null)                    juris_zipcode
          ,to_char(null)                    juris_zipextn
          ,to_char(null)                    juris_stname  
          ,to_char(null)                    juris_cntyname
          ,to_char(null)                    juris_cityname 
          ,to_char(null)                    point_of_taxation                   
          ,to_char(null)                    ship_method_code
          ,to_char(null)                    ship_via
          ,to_char(null)                    branch_address1
          ,to_char(null)                    branch_city                              
          ,to_char(null)                    branch_state
          ,to_char(null)                    branch_postal_code
          ,to_char(null)                    branch_county
          ,to_char(null)                    branch_geocode         
        from xxcus.xxcus_taxaudit_error_lines
        where 1 =1
          and customer_trx_id =p_trx_id;  
          
    type unmatched_trx_tbl is table of unmatched_trx%rowtype index by binary_integer;
    unmatched_trx_rec unmatched_trx_tbl; 
    
   cursor get_branch_address ( p_warehouse_id in number ) is
    select g.address_line_1     branch_address1
          ,g.town_or_city       branch_city
          ,g.region_2           branch_state
          ,g.postal_code        branch_postal_code
          ,g.region_1           branch_county
          ,g.loc_information13  branch_geocode --this is the geocode for the branch 
    from  hr_all_organization_units f
         ,hr_locations_all g                
    where 1 =1
      and f.organization_id =p_warehouse_id
      and g.location_id     =f.location_id;
      
   l_branch_address   get_branch_address%rowtype :=Null;                   
    
    -- ================================================================    
    
    v_process_flag       varchar2(1) :=Null;
    g_idx                number :=0;
    g_detailno           number :=0;
    err_idx              number :=0;
    b_proceed            boolean;
    n_eb_inv_total_tax   number :=0;
    n_eb_line_total_tax  number :=0;
    n_txw_total_tax      number :=0;
    n_txw_detail_tax     number :=0;
    g_prev_txw_inv_num   varchar2(30) :=Null;
    g_prev_txw_inv_count number :=0;  
    p_where_clause       varchar2(4000) :=Null;
    g_prev_inv_num       varchar2(30) :=Null;
    g_invoice_count      number :=0;
    b_txw_insert         boolean;
    v_fetch_flag         varchar2(1) :=Null;
    sql_my_trx           varchar2(20000);
    sql_text_rank        varchar2(2000) :=Null;
    sql_text_headerno    varchar2(2000) :=Null;
    sql_text_order_by    varchar2(2000) :=Null;
    g_headerno           number :=0;    
    v_cr                 varchar2(1) :='
';
    l_ship_via          varchar2(80) :=null;
    l_pot               varchar2(20) :=null;
    l_ship_method_code  varchar2(30) :=null;
    l_whse_id           number :=Null;
    l_branch_geocode    varchar2(20) :=Null;
   --
   l_module        VARCHAR2(24) :='AR';
   l_err_msg       CLOB;
   l_sec           VARCHAR2(255) :='Fetch 1012 ,';
   l_err_callfrom  VARCHAR2(75) DEFAULT 'apps.xxcus_ar_tax_return_pkg.transform_taxaudit_tax';
   l_err_callpoint VARCHAR2(75) DEFAULT 'START Exceptions';
   l_distro_list   VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';    
   --     
    -- ================================================================
     
 n_customer_count number :=0;        
  begin 
   open my_customers;
   fetch my_customers bulk collect into my_customer_rec;
   close my_customers;

   if my_customer_rec.count =0 then
    --
    print_log ('Exceptions: We are not able to find customers/invoices with exception rules for the period '||p_period);
    --
   else
    --execute immediate 'truncate table xxcus.xxcus_taxaudit_exceptions';
    --print_log('Second delete of xxcus.xxcus_taxaudit_exceptions for period ='||p_period);   
    --execute immediate 'truncate table xxcus.xxcus_taxaudit_error_lines';
    --print_log('Second delete of xxcus.xxcus_taxaudit_error_lines for period ='||p_period);   
    --print_log('Inside where count <>0');
    for idx in 1 .. my_customer_rec.count loop
    -- 
     n_customer_count :=n_customer_count + my_customer_rec(idx).total_invoices;
     --   
     --print_log('Begin processing invoices');
     --
       begin  
         v_fetch_flag :=Null;
             
         if ((my_customer_rec(idx).ship_to_customer_id IS NOT NULL) and (my_customer_rec(idx).ship_to_site_use_id IS NOT NULL)) then
          p_where_clause :='
                  and  trx.batch_source_id        =:4'||v_cr||'
                  and  trx.cust_trx_type_id       =:5'||v_cr||'
                  and  :6                         =trx.creation_date'||v_cr||'
                  and  trx.ship_to_customer_id    =:7'||v_cr||'
                  and  trx.ship_to_site_use_id    =:8';
         else
          --                       and (trx.ship_to_customer_id IS NULL and trx.ship_to_site_use_id IS NULL)'||v_cr||'
          p_where_clause :='
                  and  trx.batch_source_id        =:4'||v_cr||'
                  and  trx.cust_trx_type_id       =:5'||v_cr||'
                  and  :6                         =trx.creation_date
                  and  nvl(:7, 0)                 =0'||v_cr||'
                  and  nvl(:8, 0)                 =0'||v_cr||'
                  and  (trx.ship_to_customer_id IS NULL AND trx.ship_to_site_use_id IS NULL)';
         end if;
         
         -- we are using id's to compare what we need to include. this is bcoz the performance of the query
         -- got better when not merged with ra_batch_sources_all.Down the line we will circle back to work and make the code manageable.
             
         if my_customer_rec(idx).batch_source_id IN (1008, 1009, 1010) then
             
          --Assign the ship to condition only when field is not null else print is null
              
             
          --only if batch source is REPAIR OM SOURCE (OR) STANDARD OM SOURCE (OR) WC MANUAL                
          -- prepare SQL for ar_taxware_detailno, headerno and orderby fields
          -- ===============
             sql_text_rank :='          ,rank() 
                                            OVER 
                                             (
                                               ORDER BY trx.invoice_number asc
                                             )                              ar_taxware_detailno';
          -- ===============                                                 
             sql_text_headerno :='          ,case 
                                                when
                                                 (
                                                    rank() 
                                                                OVER 
                                                                 (
                                                                   ORDER BY trx.invoice_number asc
                                                                 )             
                                                 ) =1 then (select headerno from taxware.taxaudit_header where oracleid =trx.customer_trx_id)
                                                else null
                                             end                              my_headerno';
          -- ===============
             sql_text_order_by :='order by trx.invoice_number asc';
          -- ===============                                                               
             v_fetch_flag :='Y';
             --print_log('Batch_source_id in 1008/1009/1010, Total lines ='||my_trx_rec.count);           
         elsif my_customer_rec(idx).batch_source_id =1003 then 
          --only if batch source ORDER MANAGEMENT
          -- prepare SQL for ar_taxware_detailno, headerno and orderby fields
          -- ===============
             sql_text_rank :='          ,rank() 
                                            OVER 
                                             (
                                               ORDER BY trx.customer_trx_id asc
                                             )                              ar_taxware_detailno';
          -- ===============                                                 
             sql_text_headerno :='          ,case 
                                                when
                                                 (
                                                    rank() 
                                                                OVER 
                                                                 (
                                                                   ORDER BY trx.customer_trx_id asc
                                                                 )             
                                                 ) =1 then (select headerno from taxware.taxaudit_header where oracleid =trx.customer_trx_id)
                                                else null
                                             end                              my_headerno';
          -- ===============
             sql_text_order_by :='order by trx.customer_trx_id asc';
          -- ===============               
             v_fetch_flag :='Y';
             --print_log('Batch_source_id is 1003, Total lines ='||my_trx_rec.count);                 
         else
           print_log('Batch_source_id ='||my_customer_rec(idx).batch_source_id||', v_fetch_flag is set to N and not going to process invoices, go next customer');
           v_fetch_flag :='N';
         end if;  
             
         if v_fetch_flag ='Y' then
             sql_my_trx :='    select   
               trx.creation_date                ar_creation_date
              ,trx.ar_invoice_date              ar_invoice_date 
              ,to_char(null)                    process_flag 
              ,:1                               period 
              ,trx.ship_to_customer_id          ship_to_customer_id
              ,trx.ship_to_site_use_id          ship_to_site_use_id
              ,trx.bill_to_customer_id          bill_to_customer_id
              ,trx.bill_to_site_use_id          bill_to_site_use_id
              ,trx.customer_trx_id              customer_trx_id';
             sql_my_trx :=sql_my_trx
                        ||v_cr
                        ||sql_text_rank
                        ||v_cr
                        ||',trx.invoice_source      invoice_source 
              ,trx.invoice_number                   invoice_number'; 
             sql_my_trx :=sql_my_trx
                        ||v_cr
                        ||sql_text_headerno
                        ||v_cr 
                        ||'    from   xxcus.xxcus_taxaudit_exceptions  trx
                where  1 =1
                  /* and not exists
                        (
                           select 1
                           from   xxcus.xxcus_taxaudit_tax_archive b
                           where  1 =1
                             and  b.customer_trx_id =trx.customer_trx_id
                        ) */
                  and  trx.bill_to_customer_id    =:2'||v_cr||'
                  and  trx.bill_to_site_use_id    =:3'||v_cr||
                  p_where_clause;
             sql_my_trx :=sql_my_trx
                        ||v_cr
                        ||sql_text_order_by;
             --                            
             --print_log(sql_my_trx);
             -- ================================================================================================================   
             -- reset the g_detailno and g_headerno for each customer ship/bill/source/invoice type/creation date combination
               g_detailno :=Null;
               g_headerno :=Null;
               err_lines.delete;
             -- ================================================================================================================
             --                
             begin -- start working thru the cursor my_trx
               --
                 open my_trx 
                   for sql_my_trx 
                    using
                      p_period                                 -- ;1
                     ,my_customer_rec(idx).bill_to_customer_id -- :2
                     ,my_customer_rec(idx).bill_to_site_use_id -- :3
                     ,my_customer_rec(idx).batch_source_id     -- :4
                     ,my_customer_rec(idx).cust_trx_type_id    -- :5
                     ,my_customer_rec(idx).ar_creation_date   -- :6                         
                     ,my_customer_rec(idx).ship_to_customer_id -- :7
                     ,my_customer_rec(idx).ship_to_site_use_id; -- :8
                --                         
                 fetch my_trx bulk collect into my_trx_rec;
                 --
                   if (my_trx_rec.count >0 and v_fetch_flag ='Y') then
                    --
                     for trxidx in my_trx_rec.first .. my_trx_rec.last loop                          
                       --keep a count of the total invoices fetched.
                       g_invoice_count :=g_invoice_count + 1; 
                       b_proceed :=Null;                        
                       -- 
                       if trxidx =1 then 
                        -- the below assignment is very important.
                        -- Because of the way taxware inserts the tax audit details, taxware.taxaudit_tax table carries the 
                        -- same headerno for multiple invoices created for the same ship to customer/site and 
                        -- bill to customer/site.The cursor my_trx will 
                        -- fetch the headerno for the first row. we need to assign this to a variable and use it for the set of 
                        -- invoice lines we are dealing with
                        g_headerno :=my_trx_rec(trxidx).my_headerno;
                       else
                        -- the reset happens at the customer ship/bill/source/invoice type/creation date combination
                        Null;
                       end if; 
                       --
                       -- Fetch invoice lines for an invoice id
                       --                           
                       begin 
                        -- 
                         open trx_lines (my_trx_rec(trxidx).customer_trx_id);
                         fetch trx_lines bulk collect into trx_lines_row;
                         close trx_lines;
                        -- 
                         if trx_lines_row.count >0 then
                          --
                          l_ship_method_code :=null;
                          l_pot              :=null;
                          l_ship_via         :=null;
                          l_whse_id          :=null;
                          l_branch_address   :=null;
                          --
                          for lines_idx in trx_lines_row.first .. trx_lines_row.last loop 
                           --
                           -- Do not reset the detailno until we are done with all the invoice headers for the
                           -- above customer ship/bill site/source/invoicre type and creation date combination
                           --
                           if (g_detailno  Is Null) then 
                            g_detailno :=1;
                           else
                            g_detailno :=g_detailno + 1;                               
                           end if;   
                           -- 
                            if l_ship_method_code is null then  
                             --get ship method description and point of taxation.             
                              begin
                                --                
                                select a.ship_method_meaning 
                                      ,nvl(
                                             (
                                               select 'ORIGIN'
                                               from   fnd_lookup_values_vl
                                               where  1 =1
                                                 and  lookup_type ='HDS_WILLCALL_METHODS'
                                                 and  meaning     =a.ship_method_meaning
                                             )
                                           ,'DESTINATION'
                                          )
                                into  trx_lines_row(lines_idx).ship_via
                                     ,trx_lines_row(lines_idx).point_of_taxation
                                from  wsh_carrier_services a               
                                where 1 =1      
                                  and a.ship_method_code =trx_lines_row(lines_idx).shipping_method_code;  
                               --
                               l_ship_via         :=trx_lines_row(lines_idx).ship_via;
                               l_pot              :=trx_lines_row(lines_idx).point_of_taxation;
                               l_ship_method_code :=trx_lines_row(lines_idx).shipping_method_code;
                               --
                              exception
                               when no_data_found then
                                trx_lines_row(lines_idx).ship_via           :=Null;
                                trx_lines_row(lines_idx).point_of_taxation  :=Null;     
                               when too_many_rows then
                                trx_lines_row(lines_idx).ship_via           :=Null;
                                trx_lines_row(lines_idx).point_of_taxation  :=Null;
                               when others then
                                trx_lines_row(lines_idx).ship_via           :=Null;
                                trx_lines_row(lines_idx).point_of_taxation  :=Null;
                                 --print_log ('get branch, when others , customer_trx_id ='||gl_sla_ar_row(idx).dtl_id);     
                                 --print_log ('get branch, when others '||sqlerrm);                 
                              end;
                             --
                            else 
                             --
                                 if l_ship_method_code =trx_lines_row(lines_idx).shipping_method_code then
                                  l_ship_method_code                          :=trx_lines_row(lines_idx).shipping_method_code;
                                  trx_lines_row(lines_idx).ship_via           :=l_ship_via;
                                  trx_lines_row(lines_idx).point_of_taxation  :=l_pot;
                                 else
                                     --get ship method description and point of taxation.             
                                      begin
                                        --                
                                        select a.ship_method_meaning 
                                              ,nvl(
                                                     (
                                                       select 'ORIGIN'
                                                       from   fnd_lookup_values_vl
                                                       where  1 =1
                                                         and  lookup_type ='HDS_WILLCALL_METHODS'
                                                         and  meaning     =a.ship_method_meaning
                                                     )
                                                   ,'DESTINATION'
                                                  )
                                        into  trx_lines_row(lines_idx).ship_via
                                             ,trx_lines_row(lines_idx).point_of_taxation
                                        from  wsh_carrier_services a               
                                        where 1 =1      
                                          and a.ship_method_code =trx_lines_row(lines_idx).shipping_method_code;  
                                       --
                                       l_ship_via         :=trx_lines_row(lines_idx).ship_via;
                                       l_pot              :=trx_lines_row(lines_idx).point_of_taxation;
                                       l_ship_method_code :=trx_lines_row(lines_idx).shipping_method_code;
                                       --
                                      exception
                                       when no_data_found then
                                        trx_lines_row(lines_idx).ship_via           :=Null;
                                        trx_lines_row(lines_idx).point_of_taxation  :=Null;     
                                       when too_many_rows then
                                        trx_lines_row(lines_idx).ship_via           :=Null;
                                        trx_lines_row(lines_idx).point_of_taxation  :=Null;
                                       when others then
                                        trx_lines_row(lines_idx).ship_via           :=Null;
                                        trx_lines_row(lines_idx).point_of_taxation  :=Null;
                                         --print_log ('get branch, when others , customer_trx_id ='||gl_sla_ar_row(idx).dtl_id);     
                                         --print_log ('get branch, when others '||sqlerrm);                 
                                      end;
                                     --
                                 end if;
                             --
                            end if;  
                           --
                           --            
                            if l_whse_id is null then
                              --get branch address
                              l_whse_id :=trx_lines_row(lines_idx).warehouse_id;
                              --
                                  begin               
                                    -- 
                                    open   get_branch_address (trx_lines_row(lines_idx).warehouse_id);
                                    fetch  get_branch_address into l_branch_address;
                                    close  get_branch_address;
                                    -- 
                                    trx_lines_row(lines_idx).branch_address1    :=l_branch_address.branch_address1;
                                    trx_lines_row(lines_idx).branch_city        :=l_branch_address.branch_city;
                                    trx_lines_row(lines_idx).branch_state       :=l_branch_address.branch_state;
                                    trx_lines_row(lines_idx).branch_postal_code :=l_branch_address.branch_postal_code;
                                    trx_lines_row(lines_idx).branch_county      :=l_branch_address.branch_county;
                                    trx_lines_row(lines_idx).branch_geocode     :=l_branch_address.branch_geocode;
                                    --
                                  exception
                                   when no_data_found then
                                    trx_lines_row(lines_idx).branch_address1    :=Null;
                                    trx_lines_row(lines_idx).branch_city        :=Null; 
                                    trx_lines_row(lines_idx).branch_state       :=Null; 
                                    trx_lines_row(lines_idx).branch_postal_code :=Null; 
                                    trx_lines_row(lines_idx).branch_county      :=Null;
                                    trx_lines_row(lines_idx).branch_geocode     :=Null;     
                                   when too_many_rows then
                                    trx_lines_row(lines_idx).branch_address1    :=Null;
                                    trx_lines_row(lines_idx).branch_city        :=Null; 
                                    trx_lines_row(lines_idx).branch_state       :=Null; 
                                    trx_lines_row(lines_idx).branch_postal_code :=Null; 
                                    trx_lines_row(lines_idx).branch_county      :=Null;
                                    trx_lines_row(lines_idx).branch_geocode     :=Null;                
                                   when others then
                                    trx_lines_row(lines_idx).branch_address1    :=Null;
                                    trx_lines_row(lines_idx).branch_city        :=Null; 
                                    trx_lines_row(lines_idx).branch_state       :=Null; 
                                    trx_lines_row(lines_idx).branch_postal_code :=Null; 
                                    trx_lines_row(lines_idx).branch_county      :=Null;
                                    trx_lines_row(lines_idx).branch_geocode     :=Null;                
                                    --print_log ('get branch, when others , customer_trx_id ='||gl_sla_ar_row(idx).dtl_id);     
                                    --print_log ('get branch, when others '||sqlerrm);                 
                                  end;               
                               --                
                            else
                              if l_whse_id =trx_lines_row(lines_idx).warehouse_id then
                                -- 
                                trx_lines_row(lines_idx).branch_address1    :=l_branch_address.branch_address1;
                                trx_lines_row(lines_idx).branch_city        :=l_branch_address.branch_city;
                                trx_lines_row(lines_idx).branch_state       :=l_branch_address.branch_state;
                                trx_lines_row(lines_idx).branch_postal_code :=l_branch_address.branch_postal_code;
                                trx_lines_row(lines_idx).branch_county      :=l_branch_address.branch_county;
                                trx_lines_row(lines_idx).branch_geocode     :=l_branch_address.branch_geocode;
                                --
                              else
                                    --get branch address
                                l_whse_id :=trx_lines_row(lines_idx).warehouse_id;
                              --
                                  begin               
                                    -- 
                                    open   get_branch_address (trx_lines_row(lines_idx).warehouse_id);
                                    fetch  get_branch_address into l_branch_address;
                                    close  get_branch_address;
                                    -- 
                                    trx_lines_row(lines_idx).branch_address1    :=l_branch_address.branch_address1;
                                    trx_lines_row(lines_idx).branch_city        :=l_branch_address.branch_city;
                                    trx_lines_row(lines_idx).branch_state       :=l_branch_address.branch_state;
                                    trx_lines_row(lines_idx).branch_postal_code :=l_branch_address.branch_postal_code;
                                    trx_lines_row(lines_idx).branch_county      :=l_branch_address.branch_county;
                                    trx_lines_row(lines_idx).branch_geocode     :=l_branch_address.branch_geocode;
                                    --
                                  exception
                                   when no_data_found then
                                    trx_lines_row(lines_idx).branch_address1    :=Null;
                                    trx_lines_row(lines_idx).branch_city        :=Null; 
                                    trx_lines_row(lines_idx).branch_state       :=Null; 
                                    trx_lines_row(lines_idx).branch_postal_code :=Null; 
                                    trx_lines_row(lines_idx).branch_county      :=Null;
                                    trx_lines_row(lines_idx).branch_geocode     :=Null;     
                                   when too_many_rows then
                                    trx_lines_row(lines_idx).branch_address1    :=Null;
                                    trx_lines_row(lines_idx).branch_city        :=Null; 
                                    trx_lines_row(lines_idx).branch_state       :=Null; 
                                    trx_lines_row(lines_idx).branch_postal_code :=Null; 
                                    trx_lines_row(lines_idx).branch_county      :=Null;
                                    trx_lines_row(lines_idx).branch_geocode     :=Null;                
                                   when others then
                                    trx_lines_row(lines_idx).branch_address1    :=Null;
                                    trx_lines_row(lines_idx).branch_city        :=Null; 
                                    trx_lines_row(lines_idx).branch_state       :=Null; 
                                    trx_lines_row(lines_idx).branch_postal_code :=Null; 
                                    trx_lines_row(lines_idx).branch_county      :=Null;
                                    trx_lines_row(lines_idx).branch_geocode     :=Null;                
                                    --print_log ('get branch, when others , customer_trx_id ='||gl_sla_ar_row(idx).dtl_id);     
                                    --print_log ('get branch, when others '||sqlerrm);                 
                                  end;               
                               --  
                              end if;
                            end if;                           
                           --                                                       
                           -- get invoice line level eb tax total 
                           begin 
                                  select nvl(sum(abs(round(ebtax.tax_amt, 2))), 0)
                                  into   n_eb_line_total_tax
                                  from   zx_lines ebtax    
                                  where  1 =1
                                    and  ebtax.application_id =222
                                    and  ebtax.trx_id         =my_trx_rec(trxidx).customer_trx_id
                                    and  ebtax.trx_line_id    =trx_lines_row(lines_idx).customer_trx_line_id;
                           exception
                             when no_data_found then
                               n_eb_line_total_tax :=0;
                             when others then
                               n_eb_line_total_tax :=0;
                           end; 
                           -- for each invoice line, fetch the sum of taxamt from taxware.taxaudit_tax                               
                           begin  
                              select nvl(sum(abs(round(b.taxamt, 2))), 0)
                              into   n_txw_total_tax
                              from   taxware.taxaudit_tax    b   
                              where  1 =1
                                and  b.headerno  =g_headerno
                                and  b.detailno  =g_detailno;
                           exception
                              when no_data_found then
                                n_txw_total_tax :=-1;         
                              when others then 
                                n_txw_total_tax :=-1;       
                                print_log('Exceptions: when others: Fetch 1001, Issue fetching record in taxware.taxaudit_header for customer_trx_id ='||my_trx_rec(trxidx).customer_trx_id||
                                          ', headerno ='||g_headerno||
                                          ', detailno ='||g_detailno
                                         );
                                print_log('Exceptions: Fetch 1001 ,'||sqlerrm);
                           end; 
                           --
                           -- if n_eb_line_total_tax is same as n_txw_total_tax then insert the corresponding taxware lines 
                           --
                           if (n_txw_total_tax -n_eb_line_total_tax) =0 then 
                            --
                             v_process_flag :='Y';
                            --
                             if ((b_proceed Is Null) OR (b_proceed)) then
                              b_proceed :=TRUE;                              
                             else
                              Null; 
                              --This null will make sure we found atleast one invoice line that has an issue with the tax matching
                              -- may be because of the detail no we assumed went upside down                                
                             end if;
                            --
                           else
                            v_process_flag :='N';
                            --
                            --at any point of time if b_proceed is false it will got get reset
                            --and this signals we will ignore this invoicedo not insert the entire invoice into the custom table with the taxware data
                            b_proceed :=FALSE;
                            --
                            -- Log the invoice line into the exception table
                            Null;                               
                            --
                           end if; -- (n_txw_total_tax -n_eb_line_total_tax) <> 0 
                           --                               
                             err_idx :=err_idx + 1;
                             err_lines(err_idx).invoice_number        :=my_trx_rec(trxidx).invoice_number;
                             err_lines(err_idx).invoice_source        :=my_trx_rec(trxidx).invoice_source;
                             err_lines(err_idx).customer_trx_id       :=my_trx_rec(trxidx).customer_trx_id;
                             err_lines(err_idx).customer_trx_line_id  :=trx_lines_row(lines_idx).customer_trx_line_id;
                             err_lines(err_idx).headerno              :=g_headerno;
                             err_lines(err_idx).ar_taxware_detailno   :=g_detailno;
                             err_lines(err_idx).extended_amount       :=trx_lines_row(lines_idx).extended_amount;  
                             err_lines(err_idx).invoice_line          :=trx_lines_row(lines_idx).invoice_line;
                             err_lines(err_idx).order_line            :=trx_lines_row(lines_idx).order_line;
                             err_lines(err_idx).ar_creation_date      :=my_trx_rec(trxidx).ar_creation_date;
                             err_lines(err_idx).ar_invoice_date       :=my_trx_rec(trxidx).ar_invoice_date;  
                             err_lines(err_idx).sales_order           :=trx_lines_row(lines_idx).sales_order;
                             err_lines(err_idx).so_header_id          :=trx_lines_row(lines_idx).so_header_id;
                             err_lines(err_idx).so_line_id            :=trx_lines_row(lines_idx).so_line_id;                                                                                                                                
                             err_lines(err_idx).item_id               :=trx_lines_row(lines_idx).item_id;
                             err_lines(err_idx).item_number           :=trx_lines_row(lines_idx).item_number;
                             err_lines(err_idx).description           :=trx_lines_row(lines_idx).description;
                             err_lines(err_idx).invoice_qty           :=trx_lines_row(lines_idx).invoice_qty;                             
                             err_lines(err_idx).invoice_unit_price    :=trx_lines_row(lines_idx).invoice_unit_price;
                             err_lines(err_idx).warehouse_id          :=trx_lines_row(lines_idx).warehouse_id;
                             err_lines(err_idx).branch                :=trx_lines_row(lines_idx).branch;
                             err_lines(err_idx).ship_to_customer_id   :=my_trx_rec(trxidx).ship_to_customer_id;
                             err_lines(err_idx).ship_to_site_use_id   :=my_trx_rec(trxidx).ship_to_site_use_id;
                             err_lines(err_idx).bill_to_customer_id   :=my_trx_rec(trxidx).bill_to_customer_id;
                             err_lines(err_idx).bill_to_site_use_id   :=my_trx_rec(trxidx).bill_to_site_use_id;
                             err_lines(err_idx).batch_source_id       :=my_customer_rec(idx).batch_source_id;
                             err_lines(err_idx).period_name           :=p_period;
                             err_lines(err_idx).ebtax_line_total      :=n_eb_line_total_tax;
                             err_lines(err_idx).taxware_line_total    :=n_txw_total_tax;
                             err_lines(err_idx).process_flag          :=v_process_flag;
                             err_lines(err_idx).point_of_taxation     :=trx_lines_row(lines_idx).point_of_taxation;
                             err_lines(err_idx).ship_via              :=trx_lines_row(lines_idx).ship_via;
                             err_lines(err_idx).ship_method_code      :=trx_lines_row(lines_idx).shipping_method_code;
                             err_lines(err_idx).branch_address1       :=trx_lines_row(lines_idx).branch_address1;
                             err_lines(err_idx).branch_city           :=trx_lines_row(lines_idx).branch_city;
                             err_lines(err_idx).branch_state          :=trx_lines_row(lines_idx).branch_state;
                             err_lines(err_idx).branch_postal_code    :=trx_lines_row(lines_idx).branch_postal_code;
                             err_lines(err_idx).branch_county         :=trx_lines_row(lines_idx).branch_county;                                                                                                          
                             err_lines(err_idx).branch_geocode        :=trx_lines_row(lines_idx).branch_geocode;                             
                           --
                          end loop; --lines_idx in trx_lines_row.first .. trx_lines_row.last loop
                          --
                         end if; -- trx_lines_row.count >0
                        -- 
                       exception
                         when no_data_found then
                          --come back and insert the invoice header into the exception table later.
                          print_log('Exceptions: Failed to fetch invoice line information for Invoice Number ='||my_trx_rec(trxidx).invoice_number||
                                    ', Customer_Trx_Id ='||my_trx_rec(trxidx).customer_trx_id
                                   );
                         when others then
                          print_log('Exceptions: Fetch 1002, Issue in fetching invoice lines for customer_trx_id ='||my_trx_rec(trxidx).customer_trx_id); 
                          print_log('Exceptions: Fetch 1002 ,'||sqlerrm);                          
                       end;                              
                       --
                       if (b_proceed) then 
                        --The below assignment tells us all invoice lines for the invoice are a perfect match.
                        --In a situation where there is only one invoice, this will be the only time the assignment happens.
                        --and when we have more invoices, it can be a Y or a N. We will loop thru the plsql table
                        --mY-trx_rec again after done with all the invoices to see if we have any invoice with a N flag.
                        -- If a N exist for any reason, we will not dump the entire set of taxware lines for all the invoices combined
                        --and instead wipe of the txw_tax_row plsql table and copy the invoice table to the exception table
                        my_trx_rec(trxidx).process_flag :='Y';
                       else
                        -- what this means is the ar line tax is not matching with taxware line tax total
                        my_trx_rec(trxidx).process_flag :='N';
                        -- we dont need to log the exception again as all the invoice lines have already been logged above
                        --
                       end if; --end if for (b_proceed) condition
                                            
                     end loop; --trxidx in my_trx_rec.first .. my_trx_rec.last loop
                         
                   else -- else condition for  (my_trx_rec.count >0 and v_fetch_flag ='Y')
                    Null;
                   end if; --end if condition for (my_trx_rec.count >0 and v_fetch_flag ='Y')                     
                 close my_trx; --close invoice cursor my_trx and move to next set of customer ship/bill sites/source/invoie type and creation date combo
             exception
              when no_data_found then
               Null;
              when others then
               print_log('Exceptions: Issue in dynamic cursor, use the below internal id''s to check the validity of the SQL shown below.');
               print_log('Exceptions: Location: 102, Other errors in fetching transactions for bill_to_customer_id ='||my_customer_rec(idx).bill_to_customer_id||v_cr||
                    '   ,bill_to_site_use_id ='||my_customer_rec(idx).bill_to_site_use_id||v_cr||
                    '   ,ship_to_customer_id ='||my_customer_rec(idx).ship_to_customer_id||v_cr||          
                    '   ,ship_to_site_use_id ='||my_customer_rec(idx).ship_to_site_use_id||v_cr||
                    '   ,batch_source_id ='||my_customer_rec(idx).batch_source_id||v_cr||
                    '   ,cust_trx_type_id ='||my_customer_rec(idx).cust_trx_type_id||v_cr||                                                
                    '   ,invoice creation_date ='||my_customer_rec(idx).ar_creation_date
                   );
               print_log(' ********** Dynamic SQL ***********');                  
               print_log(sql_my_trx);               
               print_log('Exceptions: Location 102, '||sqlerrm);
             end; --end of the cursor my_trx                              
         else
           Null; 
           --We have already printed messages in the request log saying 
           --we have found an invoice source other than ORDER MANAGEMENT, STANDARD OM SOURCE, REPAIR OM SOURCE and WC MANUAL           
         end if; --if condifion for v_fetch_flag ='Y' 
                       
       exception
        when no_data_found then
         Null;
        when others then
           print_log('Exceptions: Location: 101, Other errors somewhere in between begin and end processing invoices for bill_to_customer_id ='||my_customer_rec(idx).bill_to_customer_id||v_cr||
                    '   ,bill_to_site_use_id ='||my_customer_rec(idx).bill_to_site_use_id||v_cr||
                    '   ,ship_to_customer_id ='||my_customer_rec(idx).ship_to_customer_id||v_cr||          
                    '   ,ship_to_site_use_id ='||my_customer_rec(idx).ship_to_site_use_id||v_cr||
                    '   ,batch_source_id ='||my_customer_rec(idx).batch_source_id||v_cr||
                    '   ,cust_trx_type_id ='||my_customer_rec(idx).cust_trx_type_id||v_cr||                                                
                    '   ,invoice creation_date ='||my_customer_rec(idx).ar_creation_date
                   );
          print_log('Exceptions: Location 101, '||sqlerrm);
       end;
     --
     -- Now loop thru to see if all invoices for the customer are a perfect match. if not dump the entire invoice listing to the exception table.
     --
     begin      
      -- Even once if we set b_txw_insert to FALSE,ignore all the invoices and dump them into the exception table      
      --b_txw_insert :=TRUE;
      --
      for idx101 in my_trx_rec.first .. my_trx_rec.last loop 
       --
        if my_trx_rec(idx101).process_flag ='Y' then
         -- ======================
          --print_log(my_trx_rec(idx101).invoice_number);
         --
         for idx1 in err_lines.first .. err_lines.last loop 
          if my_trx_rec(idx101).invoice_number =err_lines(idx1).invoice_number then
            begin 
             -- 
               --print_log('Headerno ='||err_lines(idx1).headerno||', Detailno ='||err_lines(idx1).ar_taxware_detailno);
               open my_txw_tax ( err_lines(idx1).headerno, err_lines(idx1).ar_taxware_detailno);
               fetch my_txw_tax bulk collect into my_txw_tax_row;  
               close my_txw_tax;
               --print_log('Invoice# '||my_trx_rec(idx101).invoice_number||', total taxware lines ='||my_txw_tax_row.count);
             -- *********************
                   if my_txw_tax_row.count >0 then
                     for txwidx in my_txw_tax_row.first .. my_txw_tax_row.last loop 
                      my_txw_tax_row(txwidx).ar_creation_date       :=err_lines(idx1).ar_creation_date;
                      my_txw_tax_row(txwidx).ar_invoice_date        :=err_lines(idx1).ar_invoice_date;
                      my_txw_tax_row(txwidx).ship_to_customer_id    :=err_lines(idx1).ship_to_customer_id; 
                      my_txw_tax_row(txwidx).ship_to_site_use_id    :=err_lines(idx1).ship_to_site_use_id;
                      my_txw_tax_row(txwidx).bill_to_customer_id    :=err_lines(idx1).bill_to_customer_id; 
                      my_txw_tax_row(txwidx).bill_to_site_use_id    :=err_lines(idx1).bill_to_site_use_id;                     
                      my_txw_tax_row(txwidx).sales_order            :=err_lines(idx1).sales_order;
                      my_txw_tax_row(txwidx).so_header_id           :=err_lines(idx1).so_header_id;
                      my_txw_tax_row(txwidx).so_line_id             :=err_lines(idx1).so_line_id; 
                      my_txw_tax_row(txwidx).customer_trx_id        :=err_lines(idx1).customer_trx_id;
                      my_txw_tax_row(txwidx).ar_taxware_detailno    :=err_lines(idx1).ar_taxware_detailno;     
                      my_txw_tax_row(txwidx).customer_trx_line_id   :=err_lines(idx1).customer_trx_line_id;
                      my_txw_tax_row(txwidx).invoice_line           :=err_lines(idx1).invoice_line;
                      my_txw_tax_row(txwidx).order_line             :=err_lines(idx1).order_line;
                      my_txw_tax_row(txwidx).item_id                :=err_lines(idx1).item_id;
                      my_txw_tax_row(txwidx).item_number            :=err_lines(idx1).item_number; 
                      my_txw_tax_row(txwidx).description            :=err_lines(idx1).description;
                      my_txw_tax_row(txwidx).invoice_qty            :=err_lines(idx1).invoice_qty;
                      my_txw_tax_row(txwidx).invoice_unit_price     :=err_lines(idx1).invoice_unit_price; 
                      my_txw_tax_row(txwidx).extended_amount        :=err_lines(idx1).extended_amount;
                      my_txw_tax_row(txwidx).warehouse_id           :=err_lines(idx1).warehouse_id;                       
                      my_txw_tax_row(txwidx).period                 :=err_lines(idx1).period_name;
                      my_txw_tax_row(txwidx).invoice_source         :=err_lines(idx1).invoice_source; 
                      my_txw_tax_row(txwidx).invoice_number         :=err_lines(idx1).invoice_number;
                      my_txw_tax_row(txwidx).branch                 :=err_lines(idx1).branch;
                      my_txw_tax_row(txwidx).batch_source_id        :=err_lines(idx1).batch_source_id;
                      my_txw_tax_row(txwidx).ship_method_code       :=err_lines(idx1).ship_method_code;                                                       
                      my_txw_tax_row(txwidx).ship_via               :=err_lines(idx1).ship_via;
                      my_txw_tax_row(txwidx).point_of_taxation      :=err_lines(idx1).point_of_taxation;
                      my_txw_tax_row(txwidx).branch_address1        :=err_lines(idx1).branch_address1;
                      my_txw_tax_row(txwidx).branch_city            :=err_lines(idx1).branch_city;
                      my_txw_tax_row(txwidx).branch_state           :=err_lines(idx1).branch_state;
                      my_txw_tax_row(txwidx).branch_postal_code     :=err_lines(idx1).branch_postal_code;
                      my_txw_tax_row(txwidx).branch_county          :=err_lines(idx1).branch_county;
                      my_txw_tax_row(txwidx).branch_geocode         :=err_lines(idx1).branch_geocode;                                                                                                                                                          
                      -- 
                      if sign(my_txw_tax_row(txwidx).extended_amount) =-1 then 
                        my_txw_tax_row(txwidx).taxamt :=(-1)*my_txw_tax_row(txwidx).taxamt;
                      else
                        my_txw_tax_row(txwidx).taxamt :=(1)*my_txw_tax_row(txwidx).taxamt;                          
                      end if;
                      --
                      --g_idx :=g_idx + 1;
                      --
                      -- keep copying all taxware tax records for the entire set of invoices and in one shot do a bulk insert down the line
                      --my_txw_new_rows(g_idx) :=my_txw_tax_row(txwidx);
                      b_txw_insert :=TRUE;
                      --                     
                     end loop; 
                    --
                      -- start bulk insert of records
                       --if (b_txw_insert) then 
                          begin  
                             forall idx in my_txw_tax_row.first .. my_txw_tax_row.last
                             insert into xxcus.xxcus_taxaudit_tax_archive values my_txw_tax_row(idx);
                                 --print_log('Successful bulk insert...');
                             --commit;
                          exception
                            when others then
                              print_log('Exceptions: my_txw_tax_row, bulk insert failed , message ='||sqlerrm);             
                          end;                                         
                       --end if;
                      --
                    --                       
                   end if;
               -- *********************               
            exception
                --
                when no_data_found then
                    print_log('Exceptions: Location NO DATA FOUND, err_lines, no_data_found for trx_id ='||err_lines(idx).customer_trx_id||
                              ', '||err_lines(err_idx).customer_trx_line_id||
                              ', '||err_lines(err_idx).headerno||
                              ', '||err_lines(err_idx).ar_taxware_detailno
                             );
            when others then
                    print_log('Exceptions: Location WHEN OTHERS, err_lines, no_data_found for trx_id ='||err_lines(idx).customer_trx_id||
                              ', '||err_lines(err_idx).customer_trx_line_id||
                              ', '||err_lines(err_idx).headerno||
                              ', '||err_lines(err_idx).ar_taxware_detailno
                             );
                    print_log('Exceptions: Location :err_lines, '||sqlerrm);               
            end;          
          else
           Null;
          end if;
         end loop;         
         -- ======================
        else
         Null;
        end if;        
       --
      end loop; --end of idx101 loop variable
      --   
       begin
         -- bulk insert all error lines into the error exception lines table  
         forall idx in err_lines.first .. err_lines.last
         insert into xxcus.xxcus_taxaudit_error_lines values err_lines(idx);
         --delete the entire error plsql exception lines table for the customer to get ready for next                             
         --
       exception
        when others then
          print_log('Exceptions: Bulk collect failed when inserting into xxcus.xxcus_taxaudit_error_lines, message ='||sqlerrm);
          print_log('Exceptions: Fetch 1003 ,'||sqlerrm);             
       end;
      --
     exception
      when others then
        print_log('Exceptions: Other errors in post processing of invoices, message ='||sqlerrm);
        print_log('Exceptions: Fetch 1004 ,'||sqlerrm); 
            
     end;
     --
     --print_log('End processing invoices');
    end loop; -- idx in 1 .. my_customer_rec.count loop
    --
   end if;--my_customer_rec.count =0 then 
   --
   begin 
    open fix_inv_with_2_lines;
    fetch fix_inv_with_2_lines bulk collect into two_lines_rec;
    close fix_inv_with_2_lines;
    
    if two_lines_rec.count >0 then
     --
     print_log('two_lines_rec.count ='||two_lines_rec.count);
     for idx in two_lines_rec.first .. two_lines_rec.last loop 
      --
       b_proceed :=Null;
      --
      if two_lines_rec(idx).diff <>0 then 
       --
       --Something is still wrong with this invoice. Go ahead and dump the entire set of lines to the xxcus.xxcus_taxaudit_tax_archive table. 
       --
       begin 
         -- 
         open unmatched_trx (two_lines_rec(idx).customer_trx_id);
         fetch unmatched_trx bulk collect into unmatched_trx_rec;
         close unmatched_trx;
         
         --print_log('unmatched customer_trx_id ='||two_lines_rec(idx).customer_trx_id||', count ='||unmatched_trx_rec.count);
         --
          begin  
           --savepoint start1; 
             forall idx in unmatched_trx_rec.first .. unmatched_trx_rec.last
             insert into xxcus.xxcus_taxaudit_tax_archive values unmatched_trx_rec(idx);
          exception
            when others then
             --rollback to start1;
              print_log('Exceptions: unmatched_trx_rec, bulk insert failed , customer_trx_id ='||two_lines_rec(idx).customer_trx_id);            
              print_log('Exceptions: unmatched_trx_rec, bulk insert failed , message ='||sqlerrm);             
          end;
         --  
         --
          begin  
           --savepoint start1; 
             delete xxcus.xxcus_taxaudit_error_lines 
             where  1 =1
               and  customer_trx_id =two_lines_rec(idx).customer_trx_id;
          exception
            when others then
             --rollback to start1;
              print_log('Exceptions: unmatched_trx_rec, delete failed , customer_trx_id ='||two_lines_rec(idx).customer_trx_id);            
              print_log('Exceptions: unmatched_trx_rec, delete failed , message ='||sqlerrm);             
          end;
         --                   
       exception
        when no_data_found then
         Null;
        when others then
         print_log('Exceptions: Issue in copying records for unmatched customer_trx_id ='||two_lines_rec(idx).customer_trx_id);
         print_log('Exceptions: Fetch 1005 ,'||sqlerrm);
       end;
       --
      else
       --
       --print_log('matched customer_trx_id ='||two_lines_rec(idx).customer_trx_id);
          begin
           -- 
           open err_lines_fix (two_lines_rec(idx).customer_trx_id);
           fetch err_lines_fix bulk collect into two_lines_txw_rec;
           close err_lines_fix;
           --
           if two_lines_txw_rec.count >0 then
            --        
             for idx2 in two_lines_txw_rec.first .. two_lines_txw_rec.last loop 
              --
              if (two_lines_txw_rec(idx2).ebtax_line_total =two_lines_txw_rec(idx2).taxware_line_total) then 
               Null; --good to go with this record, move to next record
              else 
               -- +++++++++++++++++++++++++++++++
                  --
                  begin 
                    select detailno
                          ,sum(abs(round(b.taxamt, 2)))
                    into   g_detailno
                          ,n_txw_detail_tax     
                    from   taxware.taxaudit_tax    b   
                    where  1 =1
                      and  b.headerno             =two_lines_rec(idx).headerno
                      and  b.baseamt              =abs(two_lines_txw_rec(idx2).extended_amount)
                    group by detailno;
                   -- 
                     if n_txw_detail_tax =two_lines_txw_rec(idx2).ebtax_line_total then 
                       two_lines_txw_rec(idx2).ar_taxware_detailno :=g_detailno;
                     else
                       g_detailno :=Null;
                       n_txw_detail_tax :=Null;
                     end if; 
                    
                   -- 
                  exception
                   when no_data_found then
                    g_detailno :=Null;
                    n_txw_detail_tax  :=Null;
                   when too_many_rows then
                    g_detailno :=Null;
                    n_txw_detail_tax  :=Null;
                   when others then
                    print_log('Exceptions: Requery detailno: current trx_id , headerno and baseamt for trx_id ='||two_lines_rec(idx).customer_trx_id||
                               ', headerno ='||two_lines_rec(idx).headerno||
                               ', extended_amount ='||abs(two_lines_txw_rec(idx2).extended_amount)
                             );
                    print_log('Exceptions: Requery detailno: when-others, message ='||sqlerrm);                
                    g_detailno :=Null;
                    n_txw_detail_tax  :=Null;                
                  end;
                  --
                  if g_detailno is null then
                   two_lines_rec(idx).process_flag :='N'; 
                   exit;
                  end if;
                  --
               -- +++++++++++++++++++++++++++++++              
              end if;
             end loop;
            --
           end if;
           --
          exception
           when no_data_found then
            null;
           when others then
             print_log('Exceptions: Issue in cursor err_lines_fix, trx_id ='||two_lines_rec(idx).customer_trx_id);
             print_log('Exceptions: Issue in cursor err_lines_fix, message ='||sqlerrm);
          end;      
       --
      --
          if two_lines_rec(idx).process_flag ='Y' then 
           --
           for idx2 in two_lines_txw_rec.first .. two_lines_txw_rec.last loop  
            --
              begin 
                  open my_txw_tax (two_lines_txw_rec(idx2).headerno, two_lines_txw_rec(idx2).ar_taxware_detailno);
                  fetch my_txw_tax bulk collect into my_txw_tax_row;  
                  close my_txw_tax;
                --
                -- ++++++++++++++++++++
                   if my_txw_tax_row.count >0 then
                     for txwidx in my_txw_tax_row.first .. my_txw_tax_row.last loop 
                      my_txw_tax_row(txwidx).ar_creation_date       :=two_lines_txw_rec(idx2).ar_creation_date;
                      my_txw_tax_row(txwidx).ar_invoice_date        :=two_lines_txw_rec(idx2).ar_invoice_date;
                      my_txw_tax_row(txwidx).ship_to_customer_id    :=two_lines_txw_rec(idx2).ship_to_customer_id; 
                      my_txw_tax_row(txwidx).ship_to_site_use_id    :=two_lines_txw_rec(idx2).ship_to_site_use_id;
                      my_txw_tax_row(txwidx).bill_to_customer_id    :=two_lines_txw_rec(idx2).bill_to_customer_id; 
                      my_txw_tax_row(txwidx).bill_to_site_use_id    :=two_lines_txw_rec(idx2).bill_to_site_use_id;                     
                      my_txw_tax_row(txwidx).sales_order            :=two_lines_txw_rec(idx2).sales_order;
                      my_txw_tax_row(txwidx).so_header_id           :=two_lines_txw_rec(idx2).so_header_id;
                      my_txw_tax_row(txwidx).so_line_id             :=two_lines_txw_rec(idx2).so_line_id; 
                      my_txw_tax_row(txwidx).customer_trx_id        :=two_lines_txw_rec(idx2).customer_trx_id;
                      my_txw_tax_row(txwidx).ar_taxware_detailno    :=two_lines_txw_rec(idx2).ar_taxware_detailno;     
                      my_txw_tax_row(txwidx).customer_trx_line_id   :=two_lines_txw_rec(idx2).customer_trx_line_id;
                      my_txw_tax_row(txwidx).invoice_line           :=two_lines_txw_rec(idx2).invoice_line;
                      my_txw_tax_row(txwidx).order_line             :=two_lines_txw_rec(idx2).order_line;
                      my_txw_tax_row(txwidx).item_id                :=two_lines_txw_rec(idx2).item_id;
                      my_txw_tax_row(txwidx).item_number            :=two_lines_txw_rec(idx2).item_number; 
                      my_txw_tax_row(txwidx).description            :=two_lines_txw_rec(idx2).description;
                      my_txw_tax_row(txwidx).invoice_qty            :=two_lines_txw_rec(idx2).invoice_qty;
                      my_txw_tax_row(txwidx).invoice_unit_price     :=two_lines_txw_rec(idx2).invoice_unit_price; 
                      my_txw_tax_row(txwidx).extended_amount        :=two_lines_txw_rec(idx2).extended_amount;
                      my_txw_tax_row(txwidx).warehouse_id           :=two_lines_txw_rec(idx2).warehouse_id;                       
                      my_txw_tax_row(txwidx).period                 :=two_lines_txw_rec(idx2).period_name;
                      my_txw_tax_row(txwidx).invoice_source         :=two_lines_txw_rec(idx2).invoice_source; 
                      my_txw_tax_row(txwidx).invoice_number         :=two_lines_txw_rec(idx2).invoice_number;
                      my_txw_tax_row(txwidx).branch                 :=two_lines_txw_rec(idx2).branch;
                      my_txw_tax_row(txwidx).batch_source_id        :=two_lines_txw_rec(idx2).batch_source_id;
                      my_txw_tax_row(txwidx).point_of_taxation      :=two_lines_txw_rec(idx2).point_of_taxation;                        
                      my_txw_tax_row(txwidx).ship_method_code       :=two_lines_txw_rec(idx2).ship_method_code;
                      my_txw_tax_row(txwidx).ship_via               :=two_lines_txw_rec(idx2).ship_via;                      
                      my_txw_tax_row(txwidx).branch_address1        :=two_lines_txw_rec(idx2).branch_address1;
                      my_txw_tax_row(txwidx).branch_city            :=two_lines_txw_rec(idx2).branch_city;
                      my_txw_tax_row(txwidx).branch_state           :=two_lines_txw_rec(idx2).branch_state;
                      my_txw_tax_row(txwidx).branch_postal_code     :=two_lines_txw_rec(idx2).branch_postal_code;
                      my_txw_tax_row(txwidx).branch_county          :=two_lines_txw_rec(idx2).branch_county;
                      my_txw_tax_row(txwidx).branch_geocode         :=two_lines_txw_rec(idx2).branch_geocode;                                                     
                      -- 
                      if sign(my_txw_tax_row(txwidx).extended_amount) =-1 then 
                        my_txw_tax_row(txwidx).taxamt :=(-1)*my_txw_tax_row(txwidx).taxamt;
                      else
                        my_txw_tax_row(txwidx).taxamt :=(1)*my_txw_tax_row(txwidx).taxamt;                          
                      end if;
                      --                     
                     end loop; 
                    --
                      -- start bulk insert of records
                       --if (b_txw_insert) then 
                          begin  
                             forall idx in my_txw_tax_row.first .. my_txw_tax_row.last
                             insert into xxcus.xxcus_taxaudit_tax_archive values my_txw_tax_row(idx);

                          exception
                            when others then
                              print_log('Exceptions: two_lines_txw_rec and my_txw_tax_row, bulk insert failed , message ='||sqlerrm);             
                          end;                                         
                       --end if;
                    --                       
                   end if;
                -- ++++++++++++++++++++              
                --
              exception
               when no_data_found then
                 Null;
               when others then
                 print_log('Exceptions: Issue in two_lines_txw_rec/my_txw_tax , customer_trx_id ='||two_lines_txw_rec(idx2).customer_trx_id||
                           ', customer_trx_line_id ='||two_lines_txw_rec(idx2).customer_trx_line_id
                          );
                 print_log('Exceptions: Issue in two_lines_txw_rec/my_txw_tax , message ='||sqlerrm);                                  
              end;       
           end loop;
             --
              begin   
               savepoint start1; 
                 delete xxcus.xxcus_taxaudit_error_lines
                 where  1 =1
                   and  customer_trx_id =two_lines_rec(idx).customer_trx_id;
              exception
                when others then
                 rollback to start1;
                  print_log('Exceptions: matched_trx_rec, delete failed , customer_trx_id ='||two_lines_rec(idx).customer_trx_id);            
                  print_log('Exceptions: matched_trx_rec, delete failed , message ='||sqlerrm);             
              end;
              --print_log('@matched_trx_rec, deleted record , customer_trx_id ='||two_lines_rec(idx).customer_trx_id);
           --
          else  --two_lines_rec(idx).process_flag <>'Y' then 
           --
           --
           --Something is still wrong with this invoice. Go ahead and dump the entire set of lines to the xxcus.xxcus_taxaudit_tax_archive table. 
           --
           begin 
             -- 
             open unmatched_trx (two_lines_rec(idx).customer_trx_id);
             fetch unmatched_trx bulk collect into unmatched_trx_rec;
             close unmatched_trx;
             --
              begin  
               --savepoint start1; 
                 forall idx in unmatched_trx_rec.first .. unmatched_trx_rec.last
                 insert into xxcus.xxcus_taxaudit_tax_archive values unmatched_trx_rec(idx);
              exception
                when others then
                 --rollback to start1;
                  print_log('Exceptions: unmatched_trx_rec2, bulk insert failed , customer_trx_id ='||two_lines_rec(idx).customer_trx_id);            
                  print_log('Exceptions: unmatched_trx_rec2, bulk insert failed , message ='||sqlerrm);             
              end;
             --  
             --
              begin  
               --savepoint start1; 
                 delete xxcus.xxcus_taxaudit_error_lines 
                 where  1 =1
                   and  customer_trx_id =two_lines_rec(idx).customer_trx_id;
              exception
                when others then
                 --rollback to start1;
                  print_log('@unmatched_trx_rec2, delete failed , customer_trx_id ='||two_lines_rec(idx).customer_trx_id);            
                  print_log('@unmatched_trx_rec2, delete failed , message ='||sqlerrm);             
              end;
                  --print_log('@unmatched_trx_rec2, deleted record, customer_trx_id ='||two_lines_rec(idx).customer_trx_id);              
             --                   
           exception
            when no_data_found then
             Null;
            when others then
             print_log('Exceptions: Issue in copying records for unmatched[2] customer_trx_id ='||two_lines_rec(idx).customer_trx_id);
             print_log('Exceptions: Fetch 1006 ,'||sqlerrm);
           end;
           --             
           --       
          end if;
          --               
      end if;      
     end loop; --idx in two_lines_rec.first .. two_lines_rec.last loop
     --
    else
     print_log('Exceptions: plsql table two_lines_rec returned no rows, count ='||two_lines_rec.count);
    end if;
    
   exception
    when no_data_found then
     Null;
    when others then
     print_log('Exceptions: Issue in cursor fix_inv_with_2_lines, message ='||sqlerrm);
   end;
   
   --
   print_log('Exceptions: Total customer invoices fetched ='||n_customer_count);
   print_log('Exceptions: Total invoices processed ='||g_invoice_count);
    --   
    --   --
    --   begin 
    --    execute immediate 'ALTER INDEX XXCUS.XXCUS_TAXAUDIT_TAX_ARCHIVE_N1 REBUILD';
    --    print_log('Successfully altered index XXCUS.XXCUS_TAXAUDIT_TAX_ARCHIVE_N1');      
    --   exception
    --    when others then
    --     raise_application_error(-20010, 'Issue in altering index XXCUS.XXCUS_TAXAUDIT_TAX_ARCHIVE_N1, '||SQLERRM);
    --   end;    
  exception
   when others then
    print_log('Issue in outer block of error transform_taxaudit_tax'||sqlerrm);
    --
    --fnd_file.put_line(fnd_file.log, 'Error in caller apps.xxcus_ar_tax_return_pkg.main ='||sqlerrm);
    l_err_msg := l_sec;
        l_err_msg := l_err_msg || ' ...Error_Stack...' ||
                     dbms_utility.format_error_stack() || ' Error_Backtrace...' ||
                     dbms_utility.format_error_backtrace();
        fnd_file.put_line(fnd_file.log, l_err_msg);
        l_err_callpoint := l_sec;      
        xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                            ,p_calling           => l_err_callpoint
                                            ,p_request_id        => fnd_global.conc_request_id
                                            ,p_ora_error_msg     => substr(' Error_Stack...' ||
                                                                           dbms_utility.format_error_stack() ||
                                                                           ' Error_Backtrace...' ||
                                                                           dbms_utility.format_error_backtrace()
                                                                          ,1
                                                                          ,2000)
                                            ,p_error_desc        => substr(l_err_msg
                                                                          ,1
                                                                          ,240)
                                            ,p_distribution_list => l_distro_list
                                            ,p_module            => l_module);
   --       
  end transform_taxaudit_tax;
/**************************************************************************
 *
 * FUNCTION: transform_taxaudit_tax
 * DESCRIPTION: To gather detail tax level and other info for an invoice and copy to a temp table.
 * PARAMETERS
 * ==========
 * NAME              TYPE     DESCRIPTION
.* ----------------- -------- ---------------------------------------------
 * p_period_name    IN       Fiscal period id
 * p_ledger_id     IN       Primary ledger id for Whitecap
 * Version          Ticket# 
 * ===========      ===========================
 * 1.1              ESMS 287127
*  1.2              ESMS 287127   05/05/2015 Add new field to cursor all_trx [total_prod_lines] 
 * RETURN VALUE: Oracle standard concurrent program default return values
 * CALLED BY:  HDS AR Tax Audit Return Report Set (Request Set)
 *************************************************************************/
  procedure transform_taxaudit_tax
  (
    retcode               out number
   ,errbuf                out varchar2
   ,p_ledger_id           in  number
   ,p_period_name         in  varchar2
  ) is  
    cursor all_trx (p_from_gl_date in date, p_to_gl_date in date) is 
    select   
           trunc(trx.creation_date)         ar_creation_date
          ,trunc(trx.trx_date)              ar_invoice_date   
          ,trx.ship_to_customer_id          ship_to_customer_id
          ,trx.ship_to_site_use_id          ship_to_site_use_id
          ,trx.bill_to_customer_id          bill_to_customer_id
          ,trx.bill_to_site_use_id          bill_to_site_use_id
          ,trx.customer_trx_id              customer_trx_id
          ,trx.cust_trx_type_id             cust_trx_type_id 
          ,trx.trx_number                   invoice_number
          ,rbs.name                         invoice_source
          ,rbs.batch_source_id              batch_source_id
          -- Begin ESMS 287127   1.2         
          ,(
               select count(1)
               from   ra_customer_trx_lines
               where  1 =1
                 and  customer_trx_id =trx.customer_trx_id
                 and  line_type <>'TAX'
           )                               total_prod_lines 
          -- End ESMS 287127  1.2                     
          -- Begin ESMS 287127
          ,case
            when 
              (
                select count(1)
                from   ra_customer_trx
                where  1 =1
                  and  previous_customer_trx_id =trx.customer_trx_id
              ) >0 then 'YES'
            else 'NO'
           end                              rebill_cm_exists 
          -- End ESMS 287127                    
    from  ra_customer_trx trx
         ,ra_batch_sources rbs  
    where 1 =1
      --and trx.customer_trx_id =8209960
      and trx.batch_source_id not in (1004, 1005, 1006, 1007) --Exclude PRISM, PRISM REFUND, REBILL and REBILL-CM invoices 
      and rbs.batch_source_id =trx.batch_source_id      
      and exists
             (
                select 1
                from   apps.ar_payment_schedules_all
                where  1 =1
                  and  customer_trx_id  =trx.customer_trx_id
                  and  gl_date between p_from_gl_date and p_to_gl_date  --we need invoices that matches the period requested         
             )
    order by trunc(trx.creation_date) asc
      ,trx.ship_to_customer_id asc nulls first
      ,trx.ship_to_site_use_id asc nulls first
      ,trx.bill_to_customer_id
      ,trx.bill_to_site_use_id
      ,trx.cust_trx_type_id
      ,trx.batch_source_id;               
   
   type all_trx_tbl is table of all_trx%rowtype index by binary_integer;
   all_trx_rec all_trx_tbl;
   
   type error_tbl is table of xxcus.xxcus_taxaudit_exceptions%rowtype index by binary_integer;
   my_err_trx error_tbl;
   /*
   Cursor: trx_lines
   -- ESMS 287127 1.1 Add new parameter p_rebill_cm_exist
   -- ESMS 287127 1.2 Add new parameter p_max_lines   
   */   
   cursor trx_lines(n_customer_trx_id in number, p_batch_source_id in number, p_rebill_cm_exists IN VARCHAR2, p_max_lines IN NUMBER) is 
   select ar_inv_lines.*
   from
   (
    select
           hdr.headerno                     headerno 
          ,'TXW_INV_LINE_NUM_BLANK'         fetch_type            
          ,to_date(null)                    ar_creation_date
          ,to_date(null)                    ar_invoice_date   
          ,to_number(null)                  ship_to_customer_id
          ,to_number(null)                  ship_to_site_use_id
          ,to_number(null)                  bill_to_customer_id
          ,to_number(null)                  bill_to_site_use_id                        
          ,lines.interface_line_attribute1  sales_order
          ,oeoh.header_id                   so_header_id
          ,oeol.line_id                     so_line_id
          ,case
            when 
             (p_batch_source_id IN (1008, 1009, 1010)) then
             (
               rank() 
                OVER 
                 (
                  ORDER BY lines.line_number desc --ORDER BY trx.invoice_number asc
                 )
             )
            else
             (
               rank() 
                OVER 
                 (
                  ORDER BY lines.line_number asc
                 )
             ) 
           end                              ar_taxware_detailno
          ,lines.customer_trx_line_id       customer_trx_line_id
          ,lines.line_number                invoice_line
          ,oeol.line_number                 order_line  
          ,lines.inventory_item_id          item_id
          ,msi.segment1                     item_number
          ,lines.description                description  
          ,nvl( lines.quantity_invoiced
               ,lines.quantity_credited
              )                             invoice_qty
          ,lines.unit_selling_price         invoice_unit_price
          ,lines.extended_amount            extended_amount
          ,lines.warehouse_id               warehouse_id
          ,p_period_name                    period
          ,null                             invoice_source 
          ,hdr.invnum                       invoice_number
          ,org.organization_code            branch
          ,case
            when (msi.segment1 ='DELIVERY CHARGE') then 
             (
              SELECT SHIPPING_METHOD_CODE
              FROM   OE_ORDER_HEADERS_ALL
              WHERE 1 =1
              AND ORDER_NUMBER =TO_NUMBER(lines.interface_line_attribute1)
             )
            else 
             (
                 nvl
                (
                  oeol.shipping_method_code
                 ,oeoh.shipping_method_code
                )             
             )
           end                              shipping_method_code
          --,oeol.shipping_method_code        shipping_method_code
          ,to_char(null)                    point_of_taxation
          ,to_char(null)                    ship_via
          ,to_char(null)                    branch_address1                                        
          ,to_char(null)                    branch_city
          ,to_char(null)                    branch_state
          ,to_char(null)                    branch_postal_code
          ,to_char(null)                    branch_county 
          ,to_char(null)                    branch_geocode 
          ,msi.avp_code                     item_avp_code                                                                                                            
       from   (
                /*  ESMS 287127
                   select a.*
                   from   taxware.taxaudit_header a
                   where  1 =1
                     and  exists
                      (
                         select 1
                         from   taxware.taxaudit_detail b
                         where  1 =1
                           and  b.headerno =a.headerno
                           and  b.invlinenum is null 
                      )
                 */  -- ESMS 287127
                 --Begin ESMS 287127
                   select a.*
                   from   taxware.taxaudit_header a
                   where  1 =1  
                     and  a.oracleid =n_customer_trx_id                 
                     and  exists
                      (
                         select 1
                         from   taxware.taxaudit_detail b
                         where  1 =1
                           and  b.headerno =a.headerno
                           and  b.invlinenum is null
                      ) 
                  --End ESMS 287127                                      
              ) hdr
             ,(
               select interface_line_attribute1
                     ,interface_line_attribute6
                     ,line_number
                     ,customer_trx_line_id
                     ,inventory_item_id
                     ,description
                     ,quantity_invoiced
                     ,quantity_credited
                     ,unit_selling_price
                     ,extended_amount
                     ,warehouse_id
                     ,customer_trx_id
               from   ra_customer_trx_lines
               where  1 =1
                 and  customer_trx_id =n_customer_trx_id
                 and  line_type        <>'TAX'
               --order  by customer_trx_line_id, line_number
              ) lines
             ,oe_order_lines_all            oeol
             ,oe_order_headers_all          oeoh
             ,(
               select inventory_item_id item_id, segment1, attribute22 avp_code
               from   mtl_system_items a
               where 1 =1
                 and organization_id =222 --Just for looking up the item number, let's use the WC Item Master org
              ) msi
             ,org_organization_definitions  org             
       where  1 =1
         and  hdr.oracleid           =n_customer_trx_id -- 10000045733  1683901 
         and  lines.customer_trx_id  =hdr.oracleid         
         and  oeol.line_id(+)        =to_number(lines.interface_line_attribute6)
         and  oeoh.header_id(+)      =oeol.header_id
         and  org.organization_id(+) =lines.warehouse_id
         and  msi.item_id(+)         =lines.inventory_item_id
   UNION
    select
           hdr.headerno                     headerno 
          ,'TXW_INV_LINE_NUM_PRESENT'       fetch_type 
          ,to_date(null)                    ar_creation_date
          ,to_date(null)                    ar_invoice_date   
          ,to_number(null)                  ship_to_customer_id
          ,to_number(null)                  ship_to_site_use_id
          ,to_number(null)                  bill_to_customer_id
          ,to_number(null)                  bill_to_site_use_id                        
          ,lines.interface_line_attribute1  sales_order
          ,oeoh.header_id                   so_header_id
          ,oeol.line_id                     so_line_id
          ,Null                             ar_taxware_detailno
          ,lines.customer_trx_line_id       customer_trx_line_id
          ,lines.line_number                invoice_line
          ,oeol.line_number                 order_line  
          ,lines.inventory_item_id          item_id
          ,msi.segment1                     item_number
          ,lines.description                description  
          ,nvl( lines.quantity_invoiced
               ,lines.quantity_credited
              )                             invoice_qty
          ,lines.unit_selling_price         invoice_unit_price
          ,lines.extended_amount            extended_amount
          ,lines.warehouse_id               warehouse_id
          ,p_period_name                    period
          ,null                             invoice_source 
          ,hdr.invnum                       invoice_number
          ,org.organization_code            branch
          ,case
            when (msi.segment1 ='DELIVERY CHARGE') then 
             (
              SELECT SHIPPING_METHOD_CODE
              FROM   OE_ORDER_HEADERS_ALL
              WHERE 1 =1
              AND ORDER_NUMBER =TO_NUMBER(lines.interface_line_attribute1)
             )
            else 
             (
                 nvl
                (
                  oeol.shipping_method_code
                 ,oeoh.shipping_method_code
                )             
             )
           end                              shipping_method_code
          --,oeol.shipping_method_code        shipping_method_code
          ,to_char(null)                    point_of_taxation
          ,to_char(null)                    ship_via
          ,to_char(null)                    branch_address1                                        
          ,to_char(null)                    branch_city
          ,to_char(null)                    branch_state
          ,to_char(null)                    branch_postal_code
          ,to_char(null)                    branch_county 
          ,to_char(null)                    branch_geocode 
          ,msi.avp_code                     item_avp_code                                                                                                            
       from   (
                  /*  ESMS 287127
                   select a.*
                   from   taxware.taxaudit_header a
                   where  1 =1
                     and  exists
                      (
                         select 1
                         from   taxware.taxaudit_detail b
                         where  1 =1
                           and  b.headerno =a.headerno
                           and  (b.invlinenum is not null) --TMS# 20140127-00180
                      )
                   */ -- ESMS 287127
                  --Begin ESMS 287127
                   select a.*
                   from   taxware.taxaudit_header a
                   where  1 =1
                     and  p_rebill_cm_exists ='NO'
                     and  a.oracleid =n_customer_trx_id                 
                     and  exists
                      (
                         select 1
                         from   taxware.taxaudit_detail b
                         where  1 =1
                           and  b.headerno =a.headerno
                           and  (b.invlinenum is not null)
                      )
                   union all
                   select a.*
                   from   taxware.taxaudit_header a
                   where  1 =1
                     and  p_rebill_cm_exists ='YES'
                     and  a.oracleid =n_customer_trx_id                   
                     and  exists
                      (
                         select 1
                         from   taxware.taxaudit_detail b
                         where  1 =1
                           and  b.headerno =a.headerno
                           and  (b.invlinenum is not null)
                           and  b.detailno <=p_max_lines -- ESMS 287127 1.2
                           /* -- ESMS 287127 1.2
                            (
                             select max(line_number)
                             from   apps.ra_customer_trx_lines l1
                             where  1 =1
                               and  l1.customer_trx_id =n_customer_trx_id
                               and  l1.line_type ='LINE'                 
                            )
                            */ -- ESMS 287127 1.2
                      )
                --End ESMS 287127
              ) hdr
             ,(
               select interface_line_attribute1
                     ,interface_line_attribute6
                     ,line_number
                     ,customer_trx_line_id
                     ,inventory_item_id
                     ,description
                     ,quantity_invoiced
                     ,quantity_credited
                     ,unit_selling_price
                     ,extended_amount
                     ,warehouse_id
                     ,customer_trx_id
               from   ra_customer_trx_lines
               where  1 =1
                 and  customer_trx_id =n_customer_trx_id
                 and  line_type        <>'TAX'
               --order  by customer_trx_line_id, line_number
              ) lines
             ,oe_order_lines_all            oeol
             ,oe_order_headers_all          oeoh
             ,(
               select inventory_item_id item_id, segment1, attribute22 avp_code
               from   mtl_system_items a
               where 1 =1
                 and organization_id =222 --Just for looking up the item number, let's use the WC Item Master org
              ) msi
             ,org_organization_definitions  org             
       where  1 =1
         and  hdr.oracleid           =n_customer_trx_id -- 10000045733  1683901 
         and  lines.customer_trx_id  =hdr.oracleid         
         and  oeol.line_id(+)        =to_number(lines.interface_line_attribute6)
         and  oeoh.header_id(+)      =oeol.header_id
         and  org.organization_id(+) =lines.warehouse_id
         and  msi.item_id(+)         =lines.inventory_item_id
   ) ar_inv_lines            
   order by        
         case when (p_batch_source_id IN (1008, 1009, 1010)) then ar_inv_lines.invoice_line end desc --lines.line_number end desc
        ,case when (p_batch_source_id =1003) then ar_inv_lines.invoice_line end asc; --then lines.line_number end asc;         
         
    type trx_lines_tbl is table of trx_lines%rowtype index by binary_integer;
    trx_lines_row trx_lines_tbl;           
   
    cursor my_txw_tax 
      (
        p_headerno      in number
       ,p_detailno      in number        
       ,p_item_avp_code in varchar2
       ,p_inv_line_no   in number
       ,p_type          in varchar2
       ,p_rebill_cm_exists in varchar2 --ESMS 287127 1.2
       ,p_max_lines        in number   --ESMS 287127 1.2       
      ) is
        select
               txwtax.headerno                  headerno
              ,txwtax.detailno                  detailno
              ,txwtax.taxlevel                  taxlevel
              ,txwtax.taxtype                   taxtype
              ,case
                when txwdtl.credind ='C' then (round(txwtax.taxamt, 2) * -1)
                else (round(txwtax.taxamt, 2))
               end                              taxamt
              --,round(txwtax.taxamt, 2)          taxamt
              ,txwtax.taxrate                   taxrate
              ,txwtax.cmplcode                  cmplcode
              ,txwtax.exmptamt                  exmptamt
              ,txwtax.admncode                  admncode
              ,txwtax.taxcert                   taxcert
              ,txwtax.reascode                  reascode
              ,txwtax.baseamt                   baseamt
              ,txwtax.taxcode                   taxcode
              ,txwtax.excpcode                  excpcode
              ,txwtax.stepstatus                stepstatus
              ,txwtax.stepcomment               stepcomment  
              ,to_date(null)                    ar_creation_date
              ,to_date(null)                    ar_invoice_date   
              ,to_number(null)                  ship_to_customer_id
              ,to_number(null)                  ship_to_site_use_id
              ,to_number(null)                  bill_to_customer_id
              ,to_number(null)                  bill_to_site_use_id                        
              ,to_char(null)                    sales_order
              ,to_number(null)                  so_header_id
              ,to_number(null)                  so_line_id
              ,to_number(null)                  customer_trx_id
              ,to_number(null)                  ar_taxware_detailno        
              ,to_number(null)                  customer_trx_line_id
              ,to_number(null)                  invoice_line
              ,to_number(null)                  order_line  
              ,to_number(null)                  item_id
              ,to_char(null)                    item_number
              ,to_char(null)                    description  
              ,to_number(null)                  invoice_qty
              ,to_number(null)                  invoice_unit_price
              ,to_number(null)                  extended_amount
              ,to_number(null)                  warehouse_id
              ,to_char(null)                    period
              ,to_char(null)                    invoice_source 
              ,to_char(null)                    invoice_number
              ,to_char(null)                    branch 
              ,to_number(null)                  batch_source_id
              --,txwdtl.custprodcode              custprodcode
              ,p_item_avp_code                  custprodcode --10/03/2013
              ,txwdtl.juristype                 juristype
              ,txwdtl.pot                       pot
              ,txwjur.jurisno                   jurisno
              ,txwjur.stcode                    juris_stcode                  
              ,txwjur.cntycode                  juris_cntycode
              ,txwjur.geocode                   juris_geocode
              ,txwjur.zipcode                   juris_zipcode
              ,txwjur.zipextn                   juris_zipextn
              ,(
                select statename 
                from   taxware.stepstcd_tbl
                where  1 =1
                  and  stcode =txwjur.stcode
               )                                juris_stname  
              ,txwjur.cntyname                  juris_cntyname
              ,txwjur.loclname                  juris_cityname
              ,to_char(null)                    point_of_taxation                   
              ,to_char(null)                    ship_method_code
              ,to_char(null)                    ship_via
              ,to_char(null)                    branch_address1
              ,to_char(null)                    branch_city                              
              ,to_char(null)                    branch_state
              ,to_char(null)                    branch_postal_code
              ,to_char(null)                    branch_county
              ,to_char(null)                    branch_geocode
           from   taxware.taxaudit_tax    txwtax
                 ,taxware.taxaudit_detail txwdtl
                 ,taxware.taxaudit_juris  txwjur
           where  1 =1
             and  txwdtl.headerno     =p_headerno
             and  txwdtl.detailno     =p_detailno
             and  p_type              ='TXW_INV_LINE_NUM_BLANK'
             and  txwdtl.invlinenum is null --ESMS 287127 1.2
             and  txwtax.headerno     =txwdtl.headerno
             and  txwtax.detailno     =txwdtl.detailno
             and  txwjur.headerno     =txwdtl.headerno
             and  txwjur.jurisno      =txwdtl.jurisno
             and  txwjur.jurlevel     =txwdtl.juristype                  
       UNION
        select
               txwtax.headerno                  headerno
              ,txwtax.detailno                  detailno
              ,txwtax.taxlevel                  taxlevel
              ,txwtax.taxtype                   taxtype
              ,case
                when txwdtl.credind ='C' then (round(txwtax.taxamt, 2) * -1)
                else (round(txwtax.taxamt, 2))
               end                              taxamt
              --,round(txwtax.taxamt, 2)          taxamt
              ,txwtax.taxrate                   taxrate
              ,txwtax.cmplcode                  cmplcode
              ,txwtax.exmptamt                  exmptamt
              ,txwtax.admncode                  admncode
              ,txwtax.taxcert                   taxcert
              ,txwtax.reascode                  reascode
              ,txwtax.baseamt                   baseamt
              ,txwtax.taxcode                   taxcode
              ,txwtax.excpcode                  excpcode
              ,txwtax.stepstatus                stepstatus
              ,txwtax.stepcomment               stepcomment  
              ,to_date(null)                    ar_creation_date
              ,to_date(null)                    ar_invoice_date   
              ,to_number(null)                  ship_to_customer_id
              ,to_number(null)                  ship_to_site_use_id
              ,to_number(null)                  bill_to_customer_id
              ,to_number(null)                  bill_to_site_use_id                        
              ,to_char(null)                    sales_order
              ,to_number(null)                  so_header_id
              ,to_number(null)                  so_line_id
              ,to_number(null)                  customer_trx_id
              --,to_number(null)                  ar_taxware_detailno --commented out 06/23/2014
              ,txwtax.detailno                  ar_taxware_detailno  --added 06/23/2014      
              ,to_number(null)                  customer_trx_line_id
              ,to_number(null)                  invoice_line
              ,to_number(null)                  order_line  
              ,to_number(null)                  item_id
              ,to_char(null)                    item_number
              ,to_char(null)                    description  
              ,to_number(null)                  invoice_qty
              ,to_number(null)                  invoice_unit_price
              ,to_number(null)                  extended_amount
              ,to_number(null)                  warehouse_id
              ,to_char(null)                    period
              ,to_char(null)                    invoice_source 
              ,to_char(null)                    invoice_number
              ,to_char(null)                    branch 
              ,to_number(null)                  batch_source_id
              --,txwdtl.custprodcode              custprodcode
              ,p_item_avp_code                  custprodcode --10/03/2013
              ,txwdtl.juristype                 juristype
              ,txwdtl.pot                       pot
              ,txwjur.jurisno                   jurisno
              ,txwjur.stcode                    juris_stcode                  
              ,txwjur.cntycode                  juris_cntycode
              ,txwjur.geocode                   juris_geocode
              ,txwjur.zipcode                   juris_zipcode
              ,txwjur.zipextn                   juris_zipextn
              ,(
                select statename 
                from   taxware.stepstcd_tbl
                where  1 =1
                  and  stcode =txwjur.stcode
               )                                juris_stname  
              ,txwjur.cntyname                  juris_cntyname
              ,txwjur.loclname                  juris_cityname
              ,to_char(null)                    point_of_taxation                   
              ,to_char(null)                    ship_method_code
              ,to_char(null)                    ship_via
              ,to_char(null)                    branch_address1
              ,to_char(null)                    branch_city                              
              ,to_char(null)                    branch_state
              ,to_char(null)                    branch_postal_code
              ,to_char(null)                    branch_county
              ,to_char(null)                    branch_geocode
           from   taxware.taxaudit_tax    txwtax
                 ,taxware.taxaudit_detail txwdtl
                 ,taxware.taxaudit_juris  txwjur
           where  1 =1
             and  txwdtl.headerno     =p_headerno
             and  txwdtl.invlinenum   =p_inv_line_no
             and  p_type              ='TXW_INV_LINE_NUM_PRESENT'
             and  txwtax.headerno     =txwdtl.headerno
             and  txwtax.detailno     =txwdtl.detailno
             and  txwjur.headerno     =txwdtl.headerno
             and  txwjur.jurisno      =txwdtl.jurisno
             and  txwjur.jurlevel     =txwdtl.juristype
             -- Begin ESMS 287127 1.2
             and  (
                     (
                         p_rebill_cm_exists ='NO' and  (txwdtl.invlinenum is not null)
                     )
                    OR
                     (
                           p_rebill_cm_exists ='YES' 
                       and
                           (
                                txwdtl.invlinenum is not null
                            and txwdtl.detailno <=p_max_lines
                           )
                     )
                  )
            -- End ESMS 287127 1.2                        
       order by taxlevel desc;
       
   type my_txw_tax_tbl is table of my_txw_tax%rowtype index by binary_integer;
   my_txw_tax_row my_txw_tax_tbl; 
   
   cursor get_branch_address ( p_warehouse_id in number ) is
    select g.address_line_1     branch_address1
          ,g.town_or_city       branch_city
          ,g.region_2           branch_state
          ,g.postal_code        branch_postal_code
          ,g.region_1           branch_county
          ,g.loc_information13  branch_geocode --this is the geocode for the branch 
    from  hr_all_organization_units f
         ,hr_locations_all g                
    where 1 =1
      and f.organization_id =p_warehouse_id
      and g.location_id     =f.location_id;
      
   l_branch_address   get_branch_address%rowtype :=Null; 
   
   -- =====================================    
   d_gl_from_date      date;
   d_gl_to_date        date;
   n_headerno          number :=0;
   n_txw_total_tax     number :=0;
   n_eb_total_tax      number :=0;
   n_check_diff        number :=0.04;
   error_idx           number :=0;
   insert_idx          number :=0;
   n_invoices_fetched  number :=0;
   n_min_rounding      number :=-0.5;
   n_max_rounding      number :=0.5;
   l_count             number :=0;
   l_ship_via          varchar2(80) :=null;
   l_pot               varchar2(20) :=null;
   l_ship_method_code  varchar2(30) :=null;
   l_whse_id           number :=Null;
   l_branch_geocode    varchar2(20) :=Null;
   --
   l_module        VARCHAR2(24) :='AR';
   l_err_msg       CLOB;
   l_sec           VARCHAR2(255) :='Fetch 1012 ,';
   l_err_callfrom  VARCHAR2(75) DEFAULT 'apps.xxcus_ar_tax_return_pkg.transform_taxaudit_tax';
   l_err_callpoint VARCHAR2(75) DEFAULT 'START';
   l_distro_list   VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';    
   --   
   -- =====================================
   
  begin 
    --
    begin  
     select count(1)
     into   l_count
     from all_indexes
     where 1 =1
       and owner ='XXCUS'
       and index_name ='XXCUS_TAXAUDIT_TAX_ARCHIVE_N3';
     --
       if l_count >0 then
        -- 
        execute immediate 'DROP INDEX XXCUS.XXCUS_TAXAUDIT_TAX_ARCHIVE_N3'; 
        print_log('Successfully dropped index XXCUS.XXCUS_TAXAUDIT_TAX_ARCHIVE_N3');
        --         
       else
        Null;
       end if;
    exception
     when others then
      null;
    end;
    --    
   begin 
    --execute immediate 'DROP INDEX XXCUS.XXCUS_TAXAUDIT_TAX_ARCHIVE_N3'; 
    --print_log('Successfully dropped index XXCUS.XXCUS_TAXAUDIT_TAX_ARCHIVE_N3');    
    execute immediate 'truncate table xxcus.xxcus_taxaudit_tax_archive';
    print_log('Successfully deleted old records from table xxcus.xxcus_taxaudit_tax_archive for period ='||p_period_name); 
    execute immediate 'truncate table xxcus.xxcus_taxaudit_exceptions';
    print_log('Successfully deleted old records from table xxcus.xxcus_taxaudit_exceptions for period ='||p_period_name);   
    execute immediate 'truncate table xxcus.xxcus_taxaudit_error_lines';
    print_log('Successfully deleted old records from table xxcus.xxcus_taxaudit_error_lines for period ='||p_period_name);         
   exception
    when others then
     raise_application_error(-20010, 'Issue in deleting existing record for period ='||p_period_name);
     print_log('Fetch 1007 ,'||sqlerrm);
   end;  
  
   begin 
    select gps.start_date, gps.end_date
    into   d_gl_from_date, d_gl_to_date
    from   gl_period_statuses gps
    where 1 =1
      and gps.application_id =101 --GL owned periods only
      and gps.period_name    =p_period_name
      and gps.ledger_id      =p_ledger_id;  
   exception
    when no_data_found then
     d_gl_from_date :=Null;
     d_gl_to_date   :=Null;
    when too_many_rows then
     d_gl_from_date :=Null;
     d_gl_to_date   :=Null;    
    when others then
     print_log('Issue in Period ='||p_period_name);
     raise_application_error(-20010, 'Error in fetching start and end dates for the period ='||sqlerrm);
     print_log('Fetch 1008 ,'||sqlerrm);
   end;  
   
   if (d_gl_from_date is null OR d_gl_to_date is null) then 
    raise_application_error(-20010, 'Failed to derive gl start and end dates for the period ='||p_period_name);
   else
    -- ===========================================================
    -- Now we are going to process the invoices from the 
    -- for the period requested. We will verify if the 
    -- eb tax total is the same as the taxware total.
    -- If they are same then we will proceed to fetch the details
    -- of the taxware audit tax data. If it's not a match then we will
    -- flag them into the exception table and deal with it at the end
    -- using a overloaded procedure.
    -- ===========================================================    
    open all_trx (d_gl_from_date, d_gl_to_date);
    fetch all_trx bulk collect into all_trx_rec;
    close all_trx;
    
    if all_trx_rec.count >0 then 
       --
     for idx in all_trx_rec.first .. all_trx_rec.last loop 
     
       --keep count of how many we have fetched
       n_invoices_fetched :=n_invoices_fetched + 1;
      
       --get headerno and sum of taxware tax amount
       begin  
        /* --Commented on 09/30/2013
          select a.headerno
                ,nvl(sum(abs(round(b.taxamt, 2))), 0)
          into   n_headerno
                ,n_txw_total_tax
          from   taxware.taxaudit_header a
                ,taxware.taxaudit_tax    b   
          where  1 =1
            and  a.oracleid  =all_trx_rec(idx).customer_trx_id
            and  b.headerno  =a.headerno
          group by a.headerno;
            
           --
            begin 
                  select nvl(sum(abs(round(ebtax.tax_amt, 2))), 0)
                  into   n_eb_total_tax
                  from   zx_lines ebtax    
                  where  1 =1
                    and  ebtax.application_id =222
                    and  ebtax.trx_id         =all_trx_rec(idx).customer_trx_id;
            exception
             when no_data_found then
               n_eb_total_tax :=0;
             when others then
               print_log('Fetch 1009 ,'||sqlerrm);
               n_eb_total_tax :=0;
            end;
       */         
           --    
            -- Commented the below if statement on 09/30/2013 
            -- if ((n_txw_total_tax -n_eb_total_tax) BETWEEN n_min_rounding and n_max_rounding) then   --((n_txw_total_tax -n_eb_total_tax) =0)            
            --
              open trx_lines ( n_customer_trx_id =>all_trx_rec(idx).customer_trx_id, p_batch_source_id =>all_trx_rec(idx).batch_source_id
                              ,p_rebill_cm_exists =>all_trx_rec(idx).rebill_cm_exists -- ESMS 287127
                              ,p_max_lines        =>all_trx_rec(idx).total_prod_lines   --ESMS 287127 1.2
                             );
              fetch trx_lines bulk collect into trx_lines_row;
              close trx_lines;
              -- 
              l_ship_method_code :=Null;
              l_ship_via         :=Null;
              l_pot              :=Null;
              l_whse_id          :=Null;
              --              
             --process invoice lines
             if trx_lines_row.count >0 then
              --
              for idx2 in trx_lines_row.first .. trx_lines_row.last loop 
               -- 
                if l_ship_method_code is null then 
                 --get ship method description and point of taxation.             
                  begin
                    --                
                    select a.ship_method_meaning 
                          ,nvl(
                                 (
                                   select 'ORIGIN'
                                   from   fnd_lookup_values_vl
                                   where  1 =1
                                     and  lookup_type ='HDS_WILLCALL_METHODS'
                                     and  meaning     =a.ship_method_meaning
                                 )
                               ,'DESTINATION'
                              )
                    into  trx_lines_row(idx2).ship_via
                         ,trx_lines_row(idx2).point_of_taxation
                    from  wsh_carrier_services a               
                    where 1 =1      
                      and a.ship_method_code         =trx_lines_row(idx2).shipping_method_code;  
                   --
                   l_ship_via         :=trx_lines_row(idx2).ship_via;
                   l_pot              :=trx_lines_row(idx2).point_of_taxation;
                   l_ship_method_code :=trx_lines_row(idx2).shipping_method_code;
                   --                                          
                  exception
                   when no_data_found then
                    trx_lines_row(idx2).ship_via           :=Null;
                    trx_lines_row(idx2).point_of_taxation  :=Null;     
                   when too_many_rows then
                    trx_lines_row(idx2).ship_via           :=Null;
                    trx_lines_row(idx2).point_of_taxation  :=Null;
                   when others then
                    trx_lines_row(idx2).ship_via           :=Null;
                    trx_lines_row(idx2).point_of_taxation  :=Null;
                     --print_log ('get branch, when others , customer_trx_id ='||gl_sla_ar_row(idx).dtl_id);     
                     --print_log ('get branch, when others '||sqlerrm);                 
                  end;
                 --
                else 
                 --
                     if l_ship_method_code =trx_lines_row(idx2).shipping_method_code then
                      l_ship_method_code                     :=trx_lines_row(idx2).shipping_method_code;
                      trx_lines_row(idx2).ship_via           :=l_ship_via;
                      trx_lines_row(idx2).point_of_taxation  :=l_pot;
                     else
                         --get ship method description and point of taxation.             
                          begin
                            --                
                            select a.ship_method_meaning 
                                  ,nvl(
                                         (
                                           select 'ORIGIN'
                                           from   fnd_lookup_values_vl
                                           where  1 =1
                                             and  lookup_type ='HDS_WILLCALL_METHODS'
                                             and  meaning     =a.ship_method_meaning
                                         )
                                       ,'DESTINATION'
                                      )
                            into  trx_lines_row(idx2).ship_via
                                 ,trx_lines_row(idx2).point_of_taxation
                            from  wsh_carrier_services a               
                            where 1 =1      
                              and a.ship_method_code         =trx_lines_row(idx2).shipping_method_code;  
                           --
                           l_ship_via         :=trx_lines_row(idx2).ship_via;
                           l_pot              :=trx_lines_row(idx2).point_of_taxation;
                           l_ship_method_code :=trx_lines_row(idx2).shipping_method_code;
                           --                                          
                          exception
                           when no_data_found then
                            trx_lines_row(idx2).ship_via           :=Null;
                            trx_lines_row(idx2).point_of_taxation  :=Null;     
                           when too_many_rows then
                            trx_lines_row(idx2).ship_via           :=Null;
                            trx_lines_row(idx2).point_of_taxation  :=Null;
                           when others then
                            trx_lines_row(idx2).ship_via           :=Null;
                            trx_lines_row(idx2).point_of_taxation  :=Null;
                             --print_log ('get branch, when others , customer_trx_id ='||gl_sla_ar_row(idx).dtl_id);     
                             --print_log ('get branch, when others '||sqlerrm);                 
                          end;                 
                         --
                     end if;
                 --
                end if;  
                --            
                if l_whse_id is null then
                  --get branch address
                  l_whse_id :=trx_lines_row(idx2).warehouse_id;
                  --
                      begin               
                        -- 
                        open   get_branch_address (trx_lines_row(idx2).warehouse_id);
                        fetch  get_branch_address into l_branch_address;
                        close  get_branch_address;
                        -- 
                        trx_lines_row(idx2).branch_address1    :=l_branch_address.branch_address1;
                        trx_lines_row(idx2).branch_city        :=l_branch_address.branch_city;
                        trx_lines_row(idx2).branch_state       :=l_branch_address.branch_state;
                        trx_lines_row(idx2).branch_postal_code :=l_branch_address.branch_postal_code;
                        trx_lines_row(idx2).branch_county      :=l_branch_address.branch_county;
                        trx_lines_row(idx2).branch_geocode     :=l_branch_address.branch_geocode;
                        --
                      exception
                       when no_data_found then
                        trx_lines_row(idx2).branch_address1    :=Null;
                        trx_lines_row(idx2).branch_city        :=Null; 
                        trx_lines_row(idx2).branch_state       :=Null; 
                        trx_lines_row(idx2).branch_postal_code :=Null; 
                        trx_lines_row(idx2).branch_county      :=Null;
                        trx_lines_row(idx2).branch_geocode     :=Null;     
                       when too_many_rows then
                        trx_lines_row(idx2).branch_address1    :=Null;
                        trx_lines_row(idx2).branch_city        :=Null; 
                        trx_lines_row(idx2).branch_state       :=Null; 
                        trx_lines_row(idx2).branch_postal_code :=Null; 
                        trx_lines_row(idx2).branch_county      :=Null;
                        trx_lines_row(idx2).branch_geocode     :=Null;                
                       when others then
                        trx_lines_row(idx2).branch_address1    :=Null;
                        trx_lines_row(idx2).branch_city        :=Null; 
                        trx_lines_row(idx2).branch_state       :=Null; 
                        trx_lines_row(idx2).branch_postal_code :=Null; 
                        trx_lines_row(idx2).branch_county      :=Null;
                        trx_lines_row(idx2).branch_geocode     :=Null;                
                        --print_log ('get branch, when others , customer_trx_id ='||gl_sla_ar_row(idx).dtl_id);     
                        --print_log ('get branch, when others '||sqlerrm);                 
                      end;               
                   --                
                else
                  if l_whse_id =trx_lines_row(idx2).warehouse_id then
                    -- 
                    trx_lines_row(idx2).branch_address1    :=l_branch_address.branch_address1;
                    trx_lines_row(idx2).branch_city        :=l_branch_address.branch_city;
                    trx_lines_row(idx2).branch_state       :=l_branch_address.branch_state;
                    trx_lines_row(idx2).branch_postal_code :=l_branch_address.branch_postal_code;
                    trx_lines_row(idx2).branch_county      :=l_branch_address.branch_county;
                    trx_lines_row(idx2).branch_geocode     :=l_branch_address.branch_geocode;
                    --
                  else
                        --get branch address
                    l_whse_id :=trx_lines_row(idx2).warehouse_id;
                    --
                      begin               
                        -- 
                        open   get_branch_address (trx_lines_row(idx2).warehouse_id);
                        fetch  get_branch_address into l_branch_address;
                        close  get_branch_address;
                        -- 
                        trx_lines_row(idx2).branch_address1    :=l_branch_address.branch_address1;
                        trx_lines_row(idx2).branch_city        :=l_branch_address.branch_city;
                        trx_lines_row(idx2).branch_state       :=l_branch_address.branch_state;
                        trx_lines_row(idx2).branch_postal_code :=l_branch_address.branch_postal_code;
                        trx_lines_row(idx2).branch_county      :=l_branch_address.branch_county;
                        trx_lines_row(idx2).branch_geocode     :=l_branch_address.branch_geocode;
                        --
                      exception
                       when no_data_found then
                        trx_lines_row(idx2).branch_address1    :=Null;
                        trx_lines_row(idx2).branch_city        :=Null; 
                        trx_lines_row(idx2).branch_state       :=Null; 
                        trx_lines_row(idx2).branch_postal_code :=Null; 
                        trx_lines_row(idx2).branch_county      :=Null;
                        trx_lines_row(idx2).branch_geocode     :=Null;     
                       when too_many_rows then
                        trx_lines_row(idx2).branch_address1    :=Null;
                        trx_lines_row(idx2).branch_city        :=Null; 
                        trx_lines_row(idx2).branch_state       :=Null; 
                        trx_lines_row(idx2).branch_postal_code :=Null; 
                        trx_lines_row(idx2).branch_county      :=Null;
                        trx_lines_row(idx2).branch_geocode     :=Null;                
                       when others then
                        trx_lines_row(idx2).branch_address1    :=Null;
                        trx_lines_row(idx2).branch_city        :=Null; 
                        trx_lines_row(idx2).branch_state       :=Null; 
                        trx_lines_row(idx2).branch_postal_code :=Null; 
                        trx_lines_row(idx2).branch_county      :=Null;
                        trx_lines_row(idx2).branch_geocode     :=Null;                
                        --print_log ('get branch, when others , customer_trx_id ='||gl_sla_ar_row(idx).dtl_id);     
                        --print_log ('get branch, when others '||sqlerrm);                 
                      end;               
                   --
                  end if;
                end if;
               --
                begin 
                --
                open  my_txw_tax                
                  (
                    p_headerno      =>trx_lines_row(idx2).headerno            --in number
                   ,p_detailno      =>trx_lines_row(idx2).ar_taxware_detailno --in number        
                   ,p_item_avp_code =>trx_lines_row(idx2).item_avp_code       --in varchar2
                   ,p_inv_line_no   =>trx_lines_row(idx2).invoice_line        --in number
                   ,p_type          =>trx_lines_row(idx2).fetch_type          --in varchar2 
                   ,p_rebill_cm_exists =>all_trx_rec(idx).rebill_cm_exists   --ESMS 287127 1.2
                   ,p_max_lines        =>all_trx_rec(idx).total_prod_lines   --ESMS 287127 1.2      
                  );
                fetch my_txw_tax bulk collect into my_txw_tax_row;
                close my_txw_tax;
                --
                if my_txw_tax_row.count >0 then
                 -- 
                  for idx3 in my_txw_tax_row.first .. my_txw_tax_row.last loop 
                   --
                    my_txw_tax_row (idx3).ar_creation_date      :=all_trx_rec(idx).ar_creation_date;
                    my_txw_tax_row (idx3).ar_invoice_date       :=all_trx_rec(idx).ar_invoice_date;
                    my_txw_tax_row (idx3).ship_to_customer_id   :=all_trx_rec(idx).ship_to_customer_id;
                    my_txw_tax_row (idx3).ship_to_site_use_id   :=all_trx_rec(idx).ship_to_site_use_id;
                    my_txw_tax_row (idx3).bill_to_customer_id   :=all_trx_rec(idx).bill_to_customer_id;
                    my_txw_tax_row (idx3).bill_to_site_use_id   :=all_trx_rec(idx).bill_to_site_use_id;
                    my_txw_tax_row (idx3).sales_order           :=trx_lines_row(idx2).sales_order;
                    my_txw_tax_row (idx3).so_header_id          :=trx_lines_row(idx2).so_header_id;
                    my_txw_tax_row (idx3).so_line_id            :=trx_lines_row(idx2).so_line_id;
                    my_txw_tax_row (idx3).customer_trx_id       :=all_trx_rec(idx).customer_trx_id;
                    --
                    if trx_lines_row(idx2).fetch_type ='TXW_INV_LINE_NUM_BLANK' then   
                     my_txw_tax_row (idx3).ar_taxware_detailno   :=trx_lines_row(idx2).ar_taxware_detailno;
                    else
                     Null; --What this means is we will leave the value from the cursor as it is
                    end if;
                    --
                    my_txw_tax_row (idx3).customer_trx_line_id  :=trx_lines_row(idx2).customer_trx_line_id;
                    my_txw_tax_row (idx3).invoice_line          :=trx_lines_row(idx2).invoice_line;
                    my_txw_tax_row (idx3).order_line            :=trx_lines_row(idx2).order_line;
                    my_txw_tax_row (idx3).item_id               :=trx_lines_row(idx2).item_id;
                    my_txw_tax_row (idx3).item_number           :=trx_lines_row(idx2).item_number;
                    my_txw_tax_row (idx3).description           :=trx_lines_row(idx2).description;
                    my_txw_tax_row (idx3).invoice_qty           :=trx_lines_row(idx2).invoice_qty;
                    my_txw_tax_row (idx3).invoice_unit_price    :=trx_lines_row(idx2).invoice_unit_price;
                    my_txw_tax_row (idx3).extended_amount       :=trx_lines_row(idx2).extended_amount;
                    my_txw_tax_row (idx3).warehouse_id          :=trx_lines_row(idx2).warehouse_id;
                    my_txw_tax_row (idx3).period                :=p_period_name;
                    my_txw_tax_row (idx3).invoice_source        :=all_trx_rec(idx).invoice_source;
                    my_txw_tax_row (idx3).invoice_number        :=all_trx_rec(idx).invoice_number;
                    my_txw_tax_row (idx3).branch                :=trx_lines_row(idx2).branch;
                    my_txw_tax_row (idx3).batch_source_id       :=all_trx_rec(idx).batch_source_id;
                    my_txw_tax_row (idx3).point_of_taxation     :=trx_lines_row(idx2).point_of_taxation;
                    my_txw_tax_row (idx3).ship_method_code      :=trx_lines_row(idx2).shipping_method_code;                                
                    my_txw_tax_row (idx3).ship_via              :=trx_lines_row(idx2).ship_via;
                    my_txw_tax_row (idx3).branch_address1       :=trx_lines_row(idx2).branch_address1;
                    my_txw_tax_row (idx3).branch_city           :=trx_lines_row(idx2).branch_city;
                    my_txw_tax_row (idx3).branch_state          :=trx_lines_row(idx2).branch_state;
                    my_txw_tax_row (idx3).branch_postal_code    :=trx_lines_row(idx2).branch_postal_code;
                    my_txw_tax_row (idx3).branch_county         :=trx_lines_row(idx2).branch_county;
                    my_txw_tax_row (idx3).branch_geocode        :=trx_lines_row(idx2).branch_geocode;
                   --                    
                   --
                  end loop;               
                end if;               
               exception
                when no_data_found then
                 print_log('No data found from taxware.taxaudit_tax table, Trx_id ='||all_trx_rec(idx).customer_trx_id||
                           ', Headerno ='||trx_lines_row(idx2).headerno||
                           ', Detailno'||trx_lines_row(idx2).ar_taxware_detailno
                          );
                 Null;
                when others then
                 print_log('Issue in cursor my_txw_tax, Headerno ='||trx_lines_row(idx2).headerno||', Detailno'||trx_lines_row(idx2).ar_taxware_detailno);
               end;
               --
                begin  
                     forall idx4 in my_txw_tax_row.first .. my_txw_tax_row.last
                     insert into xxcus.xxcus_taxaudit_tax_archive values my_txw_tax_row(idx4);
               exception
                when others then
                 print_log('Bulk insert to xxcus.xxcus_taxaudit_tax_archive failed, message ='||sqlerrm);
                 print_log('No data found from taxware.taxaudit_tax table, Trx_id ='||all_trx_rec(idx).customer_trx_id||
                           ', Headerno ='||trx_lines_row(idx2).headerno||
                           ', Detailno'||trx_lines_row(idx2).ar_taxware_detailno
                          );
               end;               
               --
              end loop;
              --             
             end if;               
             --
            /* --Commented on 09/30/2013             
            else --eb tax total and taxware total does not match, log into the exception table and we will deal with this later
             error_idx :=error_idx + 1;
             my_err_trx(error_idx).customer_trx_id     :=all_trx_rec(idx).customer_trx_id;
             my_err_trx(error_idx).headerno            :=n_headerno;
             my_err_trx(error_idx).invoice_total_tax   :=n_eb_total_tax;
             my_err_trx(error_idx).taxware_total_tax   :=n_txw_total_tax;
             my_err_trx(error_idx).ship_to_customer_id :=all_trx_rec(idx).ship_to_customer_id;
             my_err_trx(error_idx).ship_to_site_use_id :=all_trx_rec(idx).ship_to_site_use_id;
             my_err_trx(error_idx).bill_to_customer_id :=all_trx_rec(idx).bill_to_customer_id;
             my_err_trx(error_idx).bill_to_site_use_id :=all_trx_rec(idx).bill_to_site_use_id;
             my_err_trx(error_idx).cust_trx_type_id    :=all_trx_rec(idx).cust_trx_type_id;
             my_err_trx(error_idx).creation_date       :=all_trx_rec(idx).ar_creation_date;
             my_err_trx(error_idx).batch_source_id     :=all_trx_rec(idx).batch_source_id;             
             my_err_trx(error_idx).invoice_number      :=all_trx_rec(idx).invoice_number;
             my_err_trx(error_idx).ar_invoice_date     :=all_trx_rec(idx).ar_invoice_date;
             my_err_trx(error_idx).invoice_source      :=all_trx_rec(idx).invoice_source;
            end if;
            */ --Commented on 09/30/2013
           --           
       exception
        when no_data_found then
          error_idx :=error_idx + 1;
          my_err_trx(error_idx).customer_trx_id     :=all_trx_rec(idx).customer_trx_id;
          my_err_trx(error_idx).headerno            :=Null;
          my_err_trx(error_idx).invoice_total_tax   :=Null;
          my_err_trx(error_idx).taxware_total_tax   :=Null; 
          my_err_trx(error_idx).ship_to_customer_id :=all_trx_rec(idx).ship_to_customer_id;
          my_err_trx(error_idx).ship_to_site_use_id :=all_trx_rec(idx).ship_to_site_use_id;
          my_err_trx(error_idx).bill_to_customer_id :=all_trx_rec(idx).bill_to_customer_id;
          my_err_trx(error_idx).bill_to_site_use_id :=all_trx_rec(idx).bill_to_site_use_id;
          my_err_trx(error_idx).cust_trx_type_id    :=all_trx_rec(idx).cust_trx_type_id;
          my_err_trx(error_idx).creation_date       :=all_trx_rec(idx).ar_creation_date;
          my_err_trx(error_idx).batch_source_id     :=all_trx_rec(idx).batch_source_id;
          my_err_trx(error_idx).invoice_number      :=all_trx_rec(idx).invoice_number;
          my_err_trx(error_idx).ar_invoice_date     :=all_trx_rec(idx).ar_invoice_date;
          my_err_trx(error_idx).invoice_source      :=all_trx_rec(idx).invoice_source;                       
         --print_log('No record found in taxware.taxaudit_header for oracleid ='||all_trx_rec(idx).customer_trx_id);         
        when others then        
         print_log('when others: Issue fetching record in taxware.taxaudit_header for oracleid ='||all_trx_rec(idx).customer_trx_id);
         print_log('Fetch 1010 ,'||sqlerrm);
       end;
       --        
     end loop;
     --
     begin  
      forall idx5 in my_err_trx.first .. my_err_trx.last
       insert into xxcus.xxcus_taxaudit_exceptions values my_err_trx(idx5);
     exception
      when others then
       print_log('Bulk insert to xxcus.xxcus_taxaudit_exceptions failed, message ='||sqlerrm);
       print_log('Fetch 1011 ,'||sqlerrm);
     end;     
    else
     print_log('No invoices retrieved in transform taxaudit tax');
    end if;
    
   end if;
   
   -- ===========================================================
   -- Now we are going to reprocess the invoices from the 
   -- exception table xxcus.xxcus_taxaudit_exceptions
   -- using the overloaded procedure transform_taxaudit_tax
   -- =========================================================== 
   transform_taxaudit_tax
    (
      p_period       =>p_period_name
     ,p_gl_from_date =>d_gl_from_date
     ,p_gl_to_date   =>d_gl_to_date
    );    
  exception
   when others then
    --
    --fnd_file.put_line(fnd_file.log, 'Error in caller apps.xxcus_ar_tax_return_pkg.main ='||sqlerrm);
    l_err_msg := l_sec;
        l_err_msg := l_err_msg || ' ...Error_Stack...' ||
                     dbms_utility.format_error_stack() || ' Error_Backtrace...' ||
                     dbms_utility.format_error_backtrace();
        fnd_file.put_line(fnd_file.log, l_err_msg);
        l_err_callpoint := l_sec;      
        xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                            ,p_calling           => l_err_callpoint
                                            ,p_request_id        => fnd_global.conc_request_id
                                            ,p_ora_error_msg     => substr(' Error_Stack...' ||
                                                                           dbms_utility.format_error_stack() ||
                                                                           ' Error_Backtrace...' ||
                                                                           dbms_utility.format_error_backtrace()
                                                                          ,1
                                                                          ,2000)
                                            ,p_error_desc        => substr(l_err_msg
                                                                          ,1
                                                                          ,240)
                                            ,p_distribution_list => l_distro_list
                                            ,p_module            => l_module);
   --   
    raise_application_error(-20010, 'Outer block, Issue in transform_taxaudit_tax routine, message ='||substr(sqlerrm, 1, 200));
    print_log('Fetch 1012 ,'||sqlerrm);
  end transform_taxaudit_tax; 
/**************************************************************************
 *
 * FUNCTION: main
 * DESCRIPTION: To massage the temp table data and populate the summary and the detail table with all required columns.
 * PARAMETERS
 * ==========
 * NAME              TYPE     DESCRIPTION
.* ----------------- -------- ---------------------------------------------
 * p_period_name     IN       Fiscal period id
 * p_ledger_id       IN       Primary ledger id for Whitecap
 * p_detail_required IN      Indicate if a detail report is required
 * retcode           OUT   standard concurrent program return variable
 * errbuf            OUT    standard concurrent program return variable 
 * RETURN VALUE: Mentioned above
 * CALLED BY:  HDS AR Tax Audit Return Report Set (Request Set)
 *************************************************************************/  
  PROCEDURE main 
  (
    retcode               out number
   ,errbuf                out varchar2
   ,p_ledger_id           in  number
   ,p_period_name         in  varchar2
   ,p_detail_required     in  varchar2 default 'N'
  ) IS

    cursor taxware_data 
              (
                p_customer_trx_id number
              ) is
    select tax_b.taxlevel          taxlevel
          ,''                      taxcode
          ,''                      detailno
          ,max(tax_b.taxrate*100)  taxrate
          ,sum(tax_b.exmptamt)     exmptamt
          ,sum(tax_b.baseamt)      baseamt
          ,sum(tax_b.taxamt)       taxamt
     --tax_b.taxlevel, tax_b.taxcode, tax_b.detailno, tax_b.taxamt, tax_b.taxrate*100 taxrate, tax_b.exmptamt, tax_b.baseamt
    from   xxcus.xxcus_taxaudit_tax_archive tax_b    
    where  1 =1
      and  tax_b.customer_trx_id  =p_customer_trx_id
    group by tax_b.taxlevel;    

    type taxware_data_tbl is table of taxware_data%rowtype index by binary_integer;
    txw_audit_tax taxware_data_tbl;
    txw_ST_rec taxware_data_tbl;
    txw_CN_rec taxware_data_tbl;
    txw_LO_rec taxware_data_tbl;
    txw_SS_rec taxware_data_tbl;
    txw_SC_rec taxware_data_tbl;
    txw_SL_rec taxware_data_tbl;

    CURSOR gl_sla_ar(p_gl_from_date in date, p_gl_to_date in date) IS 
    select  
       to_char(null)                            branch
      ,p_period_name                            period 
      ,to_char(null)                            invoice_source
      ,trunc(itr.creation_date)                 invoice_creation_date   
      ,itr.trx_number                           invoice_number
      ,itr.trx_date                             invoice_date
      ,to_char(null)                            point_of_taxation
      ,to_char(null)                            ship_via
      ,to_char(null)                            taxable_address
      ,to_char(null)                            branch_address1
      ,to_char(null)                            branch_city
      ,to_char(null)                            branch_state
      ,to_char(null)                            branch_postal_code  
      ,to_char(null)                            branch_county
      ,to_char(null)                            ship_to_address1
      ,to_char(null)                            ship_to_city
      ,to_char(null)                            ship_to_state
      ,to_char(null)                            ship_to_postal_code  
      ,to_char(null)                            ship_to_county  
      ,0                                        invoice_total
      ,0                                        gross_amount
      ,0                                        taxable_amount
      ,0                                        total_freight
      ,0                                        total_tax
      ,0                                        ar_state_tax
      ,0                                        ar_county_tax
      ,0                                        ar_city_tax  
      ,0                                        exempt_amount
      ,0                                        state_exempt_amount
      ,0                                        county_exempt_amount
      ,0                                        city_exempt_amount  
      ,0                                        sec_state_exempt_amount
      ,0                                        sec_county_exempt_amount 
      ,0                                        sec_city_exempt_amount
      ,0                                        state_tax_rate
      ,0                                        state_tax_amount
      ,0                                        county_tax_rate
      ,0                                        county_tax_amount
      ,0                                        city_tax_rate
      ,0                                        city_tax_amount
      ,0                                        sec_state_tax_rate
      ,0                                        sec_state_tax_amount
      ,0                                        sec_county_tax_rate
      ,0                                        sec_county_tax_amount
      ,0                                        sec_city_tax_rate
      ,0                                        sec_city_tax_amount
      ,to_char(null)                            other_tax_type
      ,0                                        other_state_tax_rate
      ,0                                        other_state_tax_amount
      ,0                                        other_county_tax_rate
      ,0                                        other_county_tax_amount
      ,0                                        other_city_tax_rate
      ,0                                        other_city_tax_amount
      ,itr.customer_trx_id                      dtl_id
      ,itr.ship_to_customer_id                  ship_to_customer_id
      ,itr.ship_to_site_use_id                  ship_to_site_use_id
      ,itr.bill_to_customer_id                  bill_to_customer_id
      ,itr.bill_to_site_use_id                  bill_to_site_use_id
      ,to_char(null)                            bill_to_address1
      ,to_char(null)                            bill_to_city
      ,to_char(null)                            bill_to_state
      ,to_char(null)                            bill_to_postal_code  
      ,to_char(null)                            bill_to_county  
      ,to_char(null)                            bill_to_customer_name
      ,to_char(null)                            ship_to_customer_name
      ,to_char(null)                            job_code
      ,to_char(null)                            taxware_reference
      ,to_char(null)                            taxable_geocode   
      ,xxcus_ar_tax_return_pkg.g_req_id         request_id  
      ,xxcus_ar_tax_return_pkg.g_creation_date  creation_date
      ,xxcus_ar_tax_return_pkg.g_user_id        created_by
      ,itr.interface_header_attribute10         intf_header_attribute10
      ,itr.interface_header_attribute7          intf_header_attribute7
      ,itr.interface_header_attribute1          intf_header_attribute1
      ,itr.interface_header_context             intf_header_context
      ,itr.batch_source_id                      batch_source_id
      ,to_char(null)                            site_number
    from  ra_customer_trx itr  
    where 1 =1
      --and itr.customer_trx_id =8209960
      and itr.batch_source_id not in (1004, 1005, 1006, 1007) --Exclude PRISM, PRISM REFUND, REBILL and REBILL-CM invoices
      and exists
             (
                select 1
                from   apps.ar_payment_schedules_all
                where  1 =1
                  and  customer_trx_id  =itr.customer_trx_id
                  and  gl_date between p_gl_from_date and p_gl_to_date  --we need invoices that matches the period requested 
                  --and  nvl(tax_original, 0) <>0        
             )       
    order by itr.creation_date asc, itr.bill_to_customer_id asc, itr.ship_to_site_use_id asc nulls first; 

    TYPE gl_sla_ar_tbl IS TABLE OF gl_sla_ar%ROWTYPE INDEX BY BINARY_INTEGER;
    gl_sla_ar_row gl_sla_ar_tbl;  
    
    cursor my_hdr_inv is
    select dtl_id dtl_id
    from   xxcus.xxcus_tax_return_report_b
    where  1 =1
    group by dtl_id
    order by dtl_id asc;
    
    type my_hdr_tbl is table of my_hdr_inv%rowtype index by binary_integer;
    my_hdr_inv_rec my_hdr_tbl;   
 
    cursor my_inv_header_info (p_trx_id in number) is
    select 
       point_of_taxation
      ,ship_via
      ,taxable_address
      ,branch_address1
      ,branch_city
      ,branch_state
      ,branch_postal_code
      ,branch_county
      ,ship_to_address1
      ,ship_to_city
      ,ship_to_state
      ,ship_to_postal_code
      ,ship_to_county  
      ,invoice_total
      ,total_freight
      ,total_tax
      ,bill_to_address1
      ,bill_to_city
      ,bill_to_state
      ,bill_to_postal_code
      ,bill_to_county
      ,bill_to_customer_name
      ,ship_to_customer_name
      ,taxable_geocode
      ,site_number
    /* 
    -- 10/07/2013
     ,case   
        when (
                (nvl(TOTAL_TAX,0) = (nvl(AR_STATE_TAX,0) + nvl(AR_COUNTY_TAX,0) + nvl(AR_CITY_TAX,0) )) AND
                (nvl(TOTAL_TAX,0) = (nvl(STATE_TAX_AMOUNT,0) + nvl(COUNTY_TAX_AMOUNT,0) + nvl(CITY_TAX_AMOUNT,0) + nvl(SEC_STATE_TAX_AMOUNT,0) + nvl(SEC_COUNTY_TAX_AMOUNT,0) + nvl(SEC_CITY_TAX_AMOUNT,0) )) 
             ) then 'YES'
       else 'NO'
       end TAXWARE_MATCH
    -- 10/07/2013 
    */
      ,to_char(null) taxware_match      
    from   xxcus.xxcus_tax_return_report_b
    where  1 =1
      and  dtl_id =p_trx_id; 

    my_invoice_info my_inv_header_info%rowtype;
    
    cursor my_unique_lines (p_trx_id in number) is
    select customer_trx_line_id
    from   xxcus.xxcus_taxaudit_tax_archive
    where 1 =1
      and customer_trx_id =p_trx_id
    group by customer_trx_line_id
    order by 1 asc;
    
    type my_unique_lines_tbl is table of my_unique_lines%rowtype index by binary_integer;
    my_unique_inv_lines my_unique_lines_tbl;
    
    cursor my_invoice_lines (p_trx_id in number, p_trx_line_id in number, p_req_id in number, p_match in varchar2) is
    select 
        xx_txw_dtl.period                period
       ,xx_txw_dtl.invoice_source        invoice_source
       ,xx_txw_dtl.invoice_number        invoice_number      
       ,xx_txw_dtl.ar_creation_date      inv_creation_date
       ,xx_txw_dtl.ar_invoice_date       invoice_date
       ,xx_txw_dtl.point_of_taxation     point_of_taxation
       ,xx_txw_dtl.ship_via              ship_via
       ,to_char(null)                    taxable_address       
       ,xx_txw_dtl.branch                branch
       ,xx_txw_dtl.branch_address1       branch_address1
       ,xx_txw_dtl.branch_city           branch_city
       ,xx_txw_dtl.branch_state          branch_state
       ,xx_txw_dtl.branch_postal_code    branch_postal_code
       ,xx_txw_dtl.branch_county         branch_county
       ,xx_txw_dtl.branch_geocode        branch_geocode 
       ,to_char(null)                    bill_to_customer_name                   
       ,to_char(null)                    bill_to_address1
       ,to_char(null)                    bill_to_city
       ,to_char(null)                    bill_to_state
       ,to_char(null)                    bill_to_postal_code
       ,to_char(null)                    bill_to_county
       ,to_char(null)                    ship_to_customer_name       
       ,to_char(null)                    ship_to_address1
       ,to_char(null)                    ship_to_city
       ,to_char(null)                    ship_to_state
       ,to_char(null)                    ship_to_postal_code
       ,to_char(null)                    ship_to_county 
       ,to_char(null)                    ship_to_geocode        
       ,to_char(null)                    job_code 
       ,to_char(null)                    po_number
       ,xx_txw_dtl.sales_order           sales_order
       ,xx_txw_dtl.order_line            sales_order_line_num
       ,xx_txw_dtl.invoice_line          invoice_line_num      
       ,xx_txw_dtl.item_number           item_number
       ,xx_txw_dtl.description           description
       ,xx_txw_dtl.invoice_qty           invoice_qty
       ,xx_txw_dtl.invoice_unit_price    unit_price
       ,to_number(null)                  invoice_total
       ,xx_txw_dtl.extended_amount       gross_amount 
       ,to_number(null)                  taxable_amount
       ,to_number(null)                  total_freight
       ,to_number(null)                  total_tax
       ,to_number(null)                  ar_state_tax
       ,to_number(null)                  ar_county_tax
       ,to_number(null)                  ar_city_tax
       ,to_number(null)                  ar_exempt_amount
       ,xx_txw_dtl.taxlevel              st_txw_taxlevel
       ,xx_txw_dtl.taxtype               st_txw_taxtype
--       ,case
--         when p_match ='YES' then xx_txw_dtl.taxamt
--         else to_number(null)
--        end                              st_txw_taxamt
       ,xx_txw_dtl.taxamt                st_txw_taxamt
       ,(xx_txw_dtl.taxrate*100)         st_txw_taxrate
       ,xx_txw_dtl.cmplcode              st_txw_cmplcode
       ,xx_txw_dtl.exmptamt              st_txw_exmptamt
       ,xx_txw_dtl.admncode              st_txw_admncode
       ,xx_txw_dtl.taxcert               st_txw_taxcert
       ,xx_txw_dtl.reascode              st_txw_reascode
       ,xx_txw_dtl.baseamt               st_txw_baseamt
       ,xx_txw_dtl.taxcode               st_txw_taxcode
       ,xx_txw_dtl.excpcode              st_txw_excpcode 
       ,xx_txw_dtl_cn.taxlevel              cn_txw_taxlevel
       ,xx_txw_dtl_cn.taxtype               cn_txw_taxtype
--       ,case
--         when p_match ='YES' then xx_txw_dtl_cn.taxamt
--         else to_number(null)
--        end                                 cn_txw_taxamt
       ,xx_txw_dtl_cn.taxamt                cn_txw_taxamt 
       ,(xx_txw_dtl_cn.taxrate*100)         cn_txw_taxrate
       ,xx_txw_dtl_cn.cmplcode              cn_txw_cmplcode
       ,xx_txw_dtl_cn.exmptamt              cn_txw_exmptamt
       ,xx_txw_dtl_cn.admncode              cn_txw_admncode
       ,xx_txw_dtl_cn.taxcert               cn_txw_taxcert
       ,xx_txw_dtl_cn.reascode              cn_txw_reascode
       ,xx_txw_dtl_cn.baseamt               cn_txw_baseamt
       ,xx_txw_dtl_cn.taxcode               cn_txw_taxcode
       ,xx_txw_dtl_cn.excpcode              cn_txw_excpcode
       ,xx_txw_dtl_lo.taxlevel              lo_txw_taxlevel
       ,xx_txw_dtl_lo.taxtype               lo_txw_taxtype
--       ,case
--         when p_match ='YES' then xx_txw_dtl_lo.taxamt
--         else to_number(null)
--        end                                 lo_txw_taxamt
       ,xx_txw_dtl_lo.taxamt                lo_txw_taxamt                
       ,(xx_txw_dtl_lo.taxrate*100)         lo_txw_taxrate
       ,xx_txw_dtl_lo.cmplcode              lo_txw_cmplcode
       ,xx_txw_dtl_lo.exmptamt              lo_txw_exmptamt
       ,xx_txw_dtl_lo.admncode              lo_txw_admncode
       ,xx_txw_dtl_lo.taxcert               lo_txw_taxcert
       ,xx_txw_dtl_lo.reascode              lo_txw_reascode
       ,xx_txw_dtl_lo.baseamt               lo_txw_baseamt
       ,xx_txw_dtl_lo.taxcode               lo_txw_taxcode
       ,xx_txw_dtl_lo.excpcode              lo_txw_excpcode
       ,xx_txw_dtl_ss.taxlevel              ss_txw_taxlevel
       ,xx_txw_dtl_ss.taxtype               ss_txw_taxtype
--       ,case
--         when p_match ='YES' then xx_txw_dtl_ss.taxamt
--         else to_number(null)
--        end                                 ss_txw_taxamt
       ,xx_txw_dtl_ss.taxamt                ss_txw_taxamt                
       ,(xx_txw_dtl_ss.taxrate*100)         ss_txw_taxrate
       ,xx_txw_dtl_ss.cmplcode              ss_txw_cmplcode
       ,xx_txw_dtl_ss.exmptamt              ss_txw_exmptamt
       ,xx_txw_dtl_ss.admncode              ss_txw_admncode
       ,xx_txw_dtl_ss.taxcert               ss_txw_taxcert
       ,xx_txw_dtl_ss.reascode              ss_txw_reascode
       ,xx_txw_dtl_ss.baseamt               ss_txw_baseamt
       ,xx_txw_dtl_ss.taxcode               ss_txw_taxcode
       ,xx_txw_dtl_ss.excpcode              ss_txw_excpcode
       ,xx_txw_dtl_sc.taxlevel              sc_txw_taxlevel
       ,xx_txw_dtl_sc.taxtype               sc_txw_taxtype
--       ,case
--         when p_match ='YES' then xx_txw_dtl_sc.taxamt
--         else to_number(null)
--        end                                 sc_txw_taxamt
       ,xx_txw_dtl_sc.taxamt                sc_txw_taxamt                
       ,(xx_txw_dtl_sc.taxrate*100)         sc_txw_taxrate
       ,xx_txw_dtl_sc.cmplcode              sc_txw_cmplcode
       ,xx_txw_dtl_sc.exmptamt              sc_txw_exmptamt
       ,xx_txw_dtl_sc.admncode              sc_txw_admncode
       ,xx_txw_dtl_sc.taxcert               sc_txw_taxcert
       ,xx_txw_dtl_sc.reascode              sc_txw_reascode
       ,xx_txw_dtl_sc.baseamt               sc_txw_baseamt
       ,xx_txw_dtl_sc.taxcode               sc_txw_taxcode
       ,xx_txw_dtl_sc.excpcode              sc_txw_excpcode
       ,xx_txw_dtl_sl.taxlevel              sl_txw_taxlevel
       ,xx_txw_dtl_sl.taxtype               sl_txw_taxtype
--       ,case
--         when p_match ='YES' then xx_txw_dtl_sl.taxamt
--         else to_number(null)
--        end                                 sl_txw_taxamt
       ,xx_txw_dtl_sl.taxamt                sl_txw_taxamt                
       ,(xx_txw_dtl_sl.taxrate*100)         sl_txw_taxrate
       ,xx_txw_dtl_sl.cmplcode              sl_txw_cmplcode
       ,xx_txw_dtl_sl.exmptamt              sl_txw_exmptamt
       ,xx_txw_dtl_sl.admncode              sl_txw_admncode
       ,xx_txw_dtl_sl.taxcert               sl_txw_taxcert
       ,xx_txw_dtl_sl.reascode              sl_txw_reascode
       ,xx_txw_dtl_sl.baseamt               sl_txw_baseamt
       ,xx_txw_dtl_sl.taxcode               sl_txw_taxcode
       ,xx_txw_dtl_sl.excpcode              sl_txw_excpcode
       ,to_char(null)                       txw_reference
       ,xx_txw_dtl.custprodcode             custprodcode
       ,xx_txw_dtl.juristype                juristype
       ,xx_txw_dtl.pot                      pot
       ,xx_txw_dtl.jurisno                  jurisno
       ,xx_txw_dtl.juris_stcode             juris_stcode
       ,xx_txw_dtl.juris_cntycode           juris_cntycode
       ,xx_txw_dtl.juris_geocode            juris_geocode
       ,xx_txw_dtl.juris_zipcode            juris_zipcode
       ,xx_txw_dtl.juris_zipextn            juris_zipextn
       ,xx_txw_dtl.juris_stname             juris_stname
       ,xx_txw_dtl.juris_cntyname           juris_cntyname
       ,xx_txw_dtl.juris_cityname           juris_cityname 
       ,p_req_id                            request_id 
       ,p_match                             taxware_match    
    from  xxcus.xxcus_taxaudit_tax_archive xx_txw_dtl --2588869 19029710
         ,(
           select *
           from   xxcus.xxcus_taxaudit_tax_archive
           where  1 =1
             and customer_trx_id       =p_trx_id
             and customer_trx_line_id  =p_trx_line_id
             and taxlevel              ='CN'
          ) xx_txw_dtl_cn
         ,(
           select *
           from   xxcus.xxcus_taxaudit_tax_archive
           where  1 =1
             and customer_trx_id       =p_trx_id
             and customer_trx_line_id  =p_trx_line_id
             and taxlevel              ='LO'
          ) xx_txw_dtl_lo          
         ,(
           select *
           from   xxcus.xxcus_taxaudit_tax_archive
           where  1 =1
             and customer_trx_id       =p_trx_id
             and customer_trx_line_id  =p_trx_line_id
             and taxlevel              ='SS'
          ) xx_txw_dtl_ss
         ,(
           select *
           from   xxcus.xxcus_taxaudit_tax_archive
           where  1 =1
             and customer_trx_id       =p_trx_id
             and customer_trx_line_id  =p_trx_line_id
             and taxlevel              ='SC'
          ) xx_txw_dtl_sc 
         ,(
           select *
           from   xxcus.xxcus_taxaudit_tax_archive
           where  1 =1
             and customer_trx_id       =p_trx_id
             and customer_trx_line_id  =p_trx_line_id
             and taxlevel              ='SL'
          ) xx_txw_dtl_sl                    
    where 1 =1
      and xx_txw_dtl.customer_trx_id       =p_trx_id
      and xx_txw_dtl.customer_trx_line_id  =p_trx_line_id
      and xx_txw_dtl.taxlevel              ='ST'
    UNION
    select 
        xx_txw_dtl.period                period
       ,xx_txw_dtl.invoice_source        invoice_source
       ,xx_txw_dtl.invoice_number        invoice_number      
       ,xx_txw_dtl.ar_creation_date      inv_creation_date
       ,xx_txw_dtl.ar_invoice_date       invoice_date
       ,xx_txw_dtl.point_of_taxation     point_of_taxation
       ,xx_txw_dtl.ship_via              ship_via
       ,to_char(null)                    taxable_address       
       ,xx_txw_dtl.branch                branch
       ,xx_txw_dtl.branch_address1       branch_address1
       ,xx_txw_dtl.branch_city           branch_city
       ,xx_txw_dtl.branch_state          branch_state
       ,xx_txw_dtl.branch_postal_code    branch_postal_code
       ,xx_txw_dtl.branch_county         branch_county
       ,xx_txw_dtl.branch_geocode        branch_geocode 
       ,to_char(null)                    bill_to_customer_name                   
       ,to_char(null)                    bill_to_address1
       ,to_char(null)                    bill_to_city
       ,to_char(null)                    bill_to_state
       ,to_char(null)                    bill_to_postal_code
       ,to_char(null)                    bill_to_county
       ,to_char(null)                    ship_to_customer_name       
       ,to_char(null)                    ship_to_address1
       ,to_char(null)                    ship_to_city
       ,to_char(null)                    ship_to_state
       ,to_char(null)                    ship_to_postal_code
       ,to_char(null)                    ship_to_county 
       ,to_char(null)                    ship_to_geocode        
       ,to_char(null)                    job_code 
       ,to_char(null)                    po_number
       ,xx_txw_dtl.sales_order           sales_order
       ,xx_txw_dtl.order_line            sales_order_line_num
       ,xx_txw_dtl.invoice_line          invoice_line_num      
       ,xx_txw_dtl.item_number           item_number
       ,xx_txw_dtl.description           description
       ,xx_txw_dtl.invoice_qty           invoice_qty
       ,xx_txw_dtl.invoice_unit_price    unit_price
       ,to_number(null)                  invoice_total
       ,xx_txw_dtl.extended_amount       gross_amount 
       ,to_number(null)                  taxable_amount
       ,to_number(null)                  total_freight
       ,to_number(null)                  total_tax
       ,to_number(null)                  ar_state_tax
       ,to_number(null)                  ar_county_tax
       ,to_number(null)                  ar_city_tax
       ,to_number(null)                  ar_exempt_amount
       ,xx_txw_dtl.taxlevel              st_txw_taxlevel
       ,xx_txw_dtl.taxtype               st_txw_taxtype
       ,xx_txw_dtl.taxamt                st_txw_taxamt
       ,(xx_txw_dtl.taxrate*100)         st_txw_taxrate
       ,xx_txw_dtl.cmplcode              st_txw_cmplcode
       ,xx_txw_dtl.exmptamt              st_txw_exmptamt
       ,xx_txw_dtl.admncode              st_txw_admncode
       ,xx_txw_dtl.taxcert               st_txw_taxcert
       ,xx_txw_dtl.reascode              st_txw_reascode
       ,xx_txw_dtl.baseamt               st_txw_baseamt
       ,xx_txw_dtl.taxcode               st_txw_taxcode
       ,xx_txw_dtl.excpcode              st_txw_excpcode 
       ,to_char(null)                    cn_txw_taxlevel
       ,to_char(null)                    cn_txw_taxtype
       ,to_number(null)                  cn_txw_taxamt
       ,to_number(null)                  cn_txw_taxrate
       ,to_char(null)                    cn_txw_cmplcode
       ,to_number(null)                  cn_txw_exmptamt
       ,to_char(null)                    cn_txw_admncode
       ,to_char(null)                    cn_txw_taxcert
       ,to_char(null)                    cn_txw_reascode
       ,to_number(null)                  cn_txw_baseamt
       ,to_char(null)                    cn_txw_taxcode
       ,to_char(null)                    cn_txw_excpcode
       ,to_char(null)                    lo_txw_taxlevel
       ,to_char(null)                    lo_txw_taxtype
       ,to_number(null)                  lo_txw_taxamt
       ,to_number(null)                  lo_txw_taxrate
       ,to_char(null)                    lo_txw_cmplcode
       ,to_number(null)                  lo_txw_exmptamt
       ,to_char(null)                    lo_txw_admncode
       ,to_char(null)                    lo_txw_taxcert
       ,to_char(null)                    lo_txw_reascode
       ,to_number(null)                  lo_txw_baseamt
       ,to_char(null)                    lo_txw_taxcode
       ,to_char(null)                    lo_txw_excpcode
       ,to_char(null)                    ss_txw_taxlevel
       ,to_char(null)                    ss_txw_taxtype
       ,to_number(null)                  ss_txw_taxamt
       ,to_number(null)                  ss_txw_taxrate
       ,to_char(null)                    ss_txw_cmplcode
       ,to_number(null)                  ss_txw_exmptamt
       ,to_char(null)                    ss_txw_admncode
       ,to_char(null)                    ss_txw_taxcert
       ,to_char(null)                    ss_txw_reascode
       ,to_number(null)                  ss_txw_baseamt
       ,to_char(null)                    ss_txw_taxcode
       ,to_char(null)                    ss_txw_excpcode       
       ,to_char(null)                    sc_txw_taxlevel
       ,to_char(null)                    sc_txw_taxtype
       ,to_number(null)                  sc_txw_taxamt
       ,to_number(null)                  sc_txw_taxrate
       ,to_char(null)                    sc_txw_cmplcode
       ,to_number(null)                  sc_txw_exmptamt
       ,to_char(null)                    sc_txw_admncode
       ,to_char(null)                    sc_txw_taxcert
       ,to_char(null)                    sc_txw_reascode
       ,to_number(null)                  sc_txw_baseamt
       ,to_char(null)                    sc_txw_taxcode
       ,to_char(null)                    sc_txw_excpcode                     
       ,to_char(null)                    sl_txw_taxlevel
       ,to_char(null)                    sl_txw_taxtype
       ,to_number(null)                  sl_txw_taxamt
       ,to_number(null)                  sl_txw_taxrate
       ,to_char(null)                    sl_txw_cmplcode
       ,to_number(null)                  sl_txw_exmptamt
       ,to_char(null)                    sl_txw_admncode
       ,to_char(null)                    sl_txw_taxcert
       ,to_char(null)                    sl_txw_reascode
       ,to_number(null)                  sl_txw_baseamt
       ,to_char(null)                    sl_txw_taxcode
       ,to_char(null)                    sl_txw_excpcode
       ,to_char(null)                       txw_reference
       ,xx_txw_dtl.custprodcode             custprodcode
       ,xx_txw_dtl.juristype                juristype
       ,xx_txw_dtl.pot                      pot
       ,xx_txw_dtl.jurisno                  jurisno
       ,xx_txw_dtl.juris_stcode             juris_stcode
       ,xx_txw_dtl.juris_cntycode           juris_cntycode
       ,xx_txw_dtl.juris_geocode            juris_geocode
       ,xx_txw_dtl.juris_zipcode            juris_zipcode
       ,xx_txw_dtl.juris_zipextn            juris_zipextn
       ,xx_txw_dtl.juris_stname             juris_stname
       ,xx_txw_dtl.juris_cntyname           juris_cntyname
       ,xx_txw_dtl.juris_cityname           juris_cityname 
       ,p_req_id                            request_id 
       ,p_match                             taxware_match                  
    from  xxcus.xxcus_taxaudit_tax_archive xx_txw_dtl --2588869 19029710                   
    where 1 =1
      and xx_txw_dtl.customer_trx_id       =p_trx_id
      and xx_txw_dtl.customer_trx_line_id  =p_trx_line_id
      and xx_txw_dtl.taxlevel Is Null;
    
    type my_lines_tbl is table of my_invoice_lines%rowtype index by binary_integer;
    my_lines_rec my_lines_tbl;  
 --    
 -- =========================================================================================
  l_module        VARCHAR2(24) :='AR';
  l_err_msg       CLOB;
  l_sec           VARCHAR2(255);
  l_err_callfrom  VARCHAR2(75) DEFAULT 'apps.xxcus_ar_tax_return_pkg.main';
  l_err_callpoint VARCHAR2(75) DEFAULT 'START';
  l_distro_list   VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';    
 --      
  lc_request_data VARCHAR2(20) :='';
  n_req_id NUMBER :=0;
  p_limit NUMBER :=5000;
  sql_text VARCHAR2(20000);
  l_taxable_amount number :=0;
  l_exempt_amount  number :=0;
  l_ar_state_tax   number :=0;
  l_ar_county_tax  number :=0;
  l_ar_city_tax    number :=0;
  
  v_CN_flag VARCHAR2(1) :='N';
  v_LO_flag VARCHAR2(1) :='N';
  v_SC_flag VARCHAR2(1) :='N';
  v_SL_flag VARCHAR2(1) :='N';
  v_SS_flag VARCHAR2(1) :='N';
  v_ST_flag VARCHAR2(1) :='N'; 
   
  b_fetch boolean :=FALSE;
  n_taxamt  number :=0;
  n_taxrate number :=0;
  n_exmptamt number :=0;
  n_baseamt number :=0;  
  n_dtl_idx number;
  --
  n_prev_source_id number :=0;
  v_invoice_source ra_batch_sources.name%type :=Null;
  d_gl_from_date  date;
  d_gl_to_date    date;
  n_headerno number :=0;
  n_exempt_amount number :=0;
  v_wrtoff_file_name varchar2(40);
  --
  n_user_id NUMBER :=0;
  v_email       fnd_user.email_address%type :=Null;
  v_cp_long_name   fnd_concurrent_programs_tl.user_concurrent_program_name%type :=Null;   
  v_path VARCHAR2(80);
  --
  CONC_STATUS BOOLEAN;
  ln_request_id NUMBER :=0;
  v_child_requests VARCHAR2(2);
  n_tbl_count Number :=0;
  output_file_id   UTL_FILE.FILE_TYPE;
  v_audit_missing varchar2(1) :=Null;
  v_oracle_branch varchar2(3);
  --
  n_ship_siteuse_id  number :=0;
  n_ship_cust_id     number :=0;
  g_checkpoint_flag varchar2(1) :='N';
  v_trx_source ra_batch_sources_all.name%type;
  v_prism_branch varchar2(40);
  v_prism_ship_method varchar2(40);
  --
  b_move_fwd boolean;
  b_extract_errors boolean :=FALSE;  
  n_total_fetch number :=0;
  n_customer_trx_id number :=0;
  v_shipto_missing VARCHAR2(3) :=Null;
  --
  v_branch_geocode varchar2(10) :=Null;
  v_bill_to_site_number hz_party_sites.party_site_number%type;
  v_enter varchar2(1) :='
';
  v_header varchar2(2000) :='PERIOD|ACCOUNT|BRANCH|INVOICE_SOURCE|ORDER_NUMBER|INVOICE_NUMBER|INVOICE_DATE|SHIPTO_CUSTOMER_NUMBER|SHIPTO_CUSTOMER_NAME|INVOICE_TOTAL|PRODUCT_TOTAL|FREIGHT_TOTAL|TAX_TOTAL|WRTOFF_TOTAL|SHIP_VIA|BRANCH_CITY|BRANCH_STATE|BRANCH_POSTALCODE|SHIPTO_CITY|SHIPTO_STATE|SHIPTO_COUNTY|SHIPTO_POSTALCODE|RECEIPT_NUMBER|RECEIPT_DATE|INTERNAL_ID';
 --
 -- =========================================================================================
 --  
  begin 
   
   print_log('');  
   print_log('Inside main, p_ledger_id ='||p_ledger_id);
   print_log('Inside main, p_period_name ='||p_period_name);   
   print_log('');  
  
   begin 
    execute immediate 'truncate table xxcus.xxcus_tax_return_report_b';
    print_log('Successfully deleted old records from table xxcus.xxcus_tax_return_report_b for period ='||p_period_name);  
    execute immediate 'truncate table xxcus.xxcus_tax_return_rpt_detail_b';
    print_log('Successfully deleted old records from table xxcus.xxcus_tax_return_rpt_detail_b for period ='||p_period_name);
    execute immediate 'truncate table xxcus.xxcus_artax_extract_errors_all';
    print_log('Successfully deleted old records from table xxcus.xxcus_artax_extract_errors_all for period ='||p_period_name);    
    execute immediate 'CREATE INDEX XXCUS.XXCUS_TAXAUDIT_TAX_ARCHIVE_N3 ON XXCUS.XXCUS_TAXAUDIT_TAX_ARCHIVE (CUSTOMER_TRX_ID, CUSTOMER_TRX_LINE_ID, TAXLEVEL)'; 
    print_log('Successfully rebuilt index xxcus.XXCUS_TAXAUDIT_TAX_ARCHIVE_n3');               
   exception
    when others then
     raise_application_error(-20010, 'Issue in truncating table and creating index for period ='||p_period_name);
   end;
       
   -- Store the concurrent request id
   n_req_id :=fnd_global.conc_request_id;     
   g_req_id :=n_req_id;
   
   -- get gl period start and end dates using the period name 
   begin 
     
    select gps.start_date, gps.end_date
    into   d_gl_from_date, d_gl_to_date
    from   gl_period_statuses gps
    where 1 =1
      and gps.application_id =101 --GL owned periods only
      and gps.period_name    =p_period_name
      and gps.ledger_id      =p_ledger_id;  
   exception
    when others then
     print_log('Issue in Period ='||p_period_name);
     raise_application_error(-20010, 'Error in fetching start and end dates for the period ='||sqlerrm);
   end;  
   
   if (d_gl_from_date is null OR d_gl_to_date is null) then 
    raise_application_error(-20010, 'Failed to derive gl start and end dates for the period ='||p_period_name);
   else
    Null; --We are good to go for the rest of the code 
   end if;
     
   open gl_sla_ar (d_gl_from_date, d_gl_to_date);   
   loop 
    n_total_fetch :=n_total_fetch +1;
    fetch gl_sla_ar bulk collect into gl_sla_ar_row limit p_limit;      
    exit when gl_sla_ar_row.COUNT =0;
    
    if gl_sla_ar_row.COUNT >0 then
     print_log('Fetch seq '||n_total_fetch||', gl_sla_ar_row.COUNT ='||gl_sla_ar_row.COUNT);
    
      for idx in 1 .. gl_sla_ar_row.COUNT loop 
           --lets massage the data a bit more to fill in some more fields   
           
          -- get taxable amount
          gl_sla_ar_row(idx).taxable_amount :=get_taxable_amount(gl_sla_ar_row(idx).dtl_id, '');
           
          --get exempt amount
          gl_sla_ar_row(idx).exempt_amount  :=get_exempt_amount(gl_sla_ar_row(idx).dtl_id, ''); 
          
          --get location based ar taxes
          get_location_based_tax
                (
                  gl_sla_ar_row(idx).dtl_id         --Input
                 ,''                                --Input,its null because we need the sum at invoice level
                 ,gl_sla_ar_row(idx).ar_state_tax   --Output
                 ,gl_sla_ar_row(idx).ar_county_tax  --Output
                 ,gl_sla_ar_row(idx).ar_city_tax    --Output
                );                               
          
          If n_prev_source_id is null then
               begin 
                select name
                into   v_invoice_source
                from   ra_batch_sources_all
                where  1 =1
                  and  batch_source_id  =gl_sla_ar_row(idx).batch_source_id
                  and  org_id           =mo_global.get_current_org_id;
                  
                  n_prev_source_id                  :=gl_sla_ar_row(idx).batch_source_id;                  
                  gl_sla_ar_row(idx).invoice_source :=v_invoice_source;
                  
               exception
                when no_data_found then 
                 gl_sla_ar_row(idx).invoice_source :=Null;
                 n_prev_source_id                  :=Null;
                 v_invoice_source                  :=Null;
                when others then
                 gl_sla_ar_row(idx).invoice_source :=Null;
                 n_prev_source_id                  :=Null;
                 v_invoice_source                  :=Null;
                 print_log('@04-NOV-106, MSG ='||sqlerrm);
               end; 
                        
          Else
               If (n_prev_source_id =gl_sla_ar_row(idx).batch_source_id) then
                gl_sla_ar_row(idx).invoice_source :=v_invoice_source;
               Else
                   begin 
                    select name
                    into   v_invoice_source
                    from   ra_batch_sources_all
                    where  1 =1
                      and  batch_source_id  =gl_sla_ar_row(idx).batch_source_id
                      and  org_id           =mo_global.get_current_org_id;
                      
                      n_prev_source_id                  :=gl_sla_ar_row(idx).batch_source_id;                  
                      gl_sla_ar_row(idx).invoice_source :=v_invoice_source;
                      
                   exception
                    when no_data_found then 
                     gl_sla_ar_row(idx).invoice_source :=Null;
                     n_prev_source_id                  :=Null;
                     v_invoice_source                  :=Null;
                    when others then
                     gl_sla_ar_row(idx).invoice_source :=Null;
                     n_prev_source_id                  :=Null;
                     v_invoice_source                  :=Null;
                     print_log('@04-NOV-105, MSG ='||sqlerrm);
                   end;           
               End If;
          End If;
           
           begin 
            if gl_sla_ar_row(idx).invoice_source in ('STANDARD OM SOURCE', 'ORDER MANAGEMENT', 'REPAIR OM SOURCE') then           
             Null;
            elsif gl_sla_ar_row(idx).invoice_source in ('REBILL', 'REBILL-CM') then           
                gl_sla_ar_row(idx).branch :=substr(lpad(substr(gl_sla_ar_row(idx).invoice_number, 1, (instr(gl_sla_ar_row(idx).invoice_number,'-')-1)), 9, '0'), 1, 3);
                gl_sla_ar_row(idx).branch_address1    :=Null;
                gl_sla_ar_row(idx).branch_city        :=Null; 
                gl_sla_ar_row(idx).branch_state       :=Null; 
                gl_sla_ar_row(idx).branch_postal_code :=Null; 
                gl_sla_ar_row(idx).branch_county      :=Null;             
            else
             --for all other sources we will default branch to NA which stands for not available
                gl_sla_ar_row(idx).branch             :='NA';
                gl_sla_ar_row(idx).branch_address1    :=Null;
                gl_sla_ar_row(idx).branch_city        :=Null; 
                gl_sla_ar_row(idx).branch_state       :=Null; 
                gl_sla_ar_row(idx).branch_postal_code :=Null; 
                gl_sla_ar_row(idx).branch_county      :=Null;             
            end if;
           exception
            when others then
             print_log('Location: check invoice source , customer_trx_id ='||gl_sla_ar_row(idx).dtl_id);
             print_log('Location: check invoice source '||sqlerrm);
           end; 
           
           v_shipto_missing       :=Null;
           v_bill_to_site_number  :=Null;
           
           begin 
            select regexp_replace
                    (
                       a.cust_ship_to_address1
                      ,'([^[:print:]])',''
                    )                
                  ,a.cust_ship_to_city
                  ,a.cust_ship_to_state_prov
                  ,a.cust_ship_to_zip_code
                  ,a.cust_ship_to_county
                  ,a.cust_ship_to_name
                  ,case
                    when (a.cust_ship_to_city is Null OR a.cust_ship_to_state_prov Is Null OR a.cust_ship_to_zip_code Is Null) then 'YES'
                    else 'NO'
                   end
                  ,a.total_freight
                  ,a.total_tax
                  ,a.total_invoice
                  ,a.total_line_items
                  ,regexp_replace
                    (
                       a.cust_bill_to_address1
                      ,'([^[:print:]])',''
                    )
                  ,a.cust_bill_to_city
                  ,a.cust_bill_to_state_prov
                  ,a.cust_bill_to_zip_code
                  ,a.cust_bill_to_county 
                  ,a.cust_bill_to_name
                  ,a.cust_ship_to_site_number
                  ,a.cust_bill_to_site_number                 
            -- ========================================                  
            into   gl_sla_ar_row(idx).ship_to_address1
                  ,gl_sla_ar_row(idx).ship_to_city
                  ,gl_sla_ar_row(idx).ship_to_state
                  ,gl_sla_ar_row(idx).ship_to_postal_code
                  ,gl_sla_ar_row(idx).ship_to_county
                  ,gl_sla_ar_row(idx).ship_to_customer_name
                  ,v_shipto_missing
                  ,gl_sla_ar_row(idx).total_freight
                  ,gl_sla_ar_row(idx).total_tax
                  ,gl_sla_ar_row(idx).invoice_total
                  ,gl_sla_ar_row(idx).gross_amount
                  ,gl_sla_ar_row(idx).bill_to_address1
                  ,gl_sla_ar_row(idx).bill_to_city
                  ,gl_sla_ar_row(idx).bill_to_state
                  ,gl_sla_ar_row(idx).bill_to_postal_code
                  ,gl_sla_ar_row(idx).bill_to_county 
                  ,gl_sla_ar_row(idx).bill_to_customer_name 
                  ,gl_sla_ar_row(idx).site_number
                  ,v_bill_to_site_number                  
            -- ========================================                  
            from   xxwc.xxwcar_taxware_audit_tbl a
            where  1 =1
              and  a.customer_trx_id =gl_sla_ar_row(idx).dtl_id;
              
           exception           
            when no_data_found then             
                 begin 
                   select apsa.freight_original 
                         ,apsa.tax_original                
                         ,apsa.amount_due_original 
                         ,apsa.amount_line_items_original
                         ,regexp_replace
                            (
                               hzl.address1
                              ,'([^[:print:]])',''
                            )
                         ,hzl.city
                         ,hzl.state
                         ,hzl.postal_code
                         ,hzl.county
                         ,regexp_replace
                            (
                              bill_hzl.address1
                              ,'([^[:print:]])',''
                            )
                         ,bill_hzl.city
                         ,bill_hzl.state
                         ,bill_hzl.postal_code
                         ,bill_hzl.county 
                         ,hzp.party_name 
                         ,bill_hzp.party_name
                         ,hzps.party_site_number --this table will fetch the site number that has a usage of ship to
                         ,bill_hzps.party_site_number                         
                   into   gl_sla_ar_row(idx).total_freight
                         ,gl_sla_ar_row(idx).total_tax
                         ,gl_sla_ar_row(idx).invoice_total
                         ,gl_sla_ar_row(idx).gross_amount
                         ,gl_sla_ar_row(idx).ship_to_address1
                         ,gl_sla_ar_row(idx).ship_to_city
                         ,gl_sla_ar_row(idx).ship_to_state
                         ,gl_sla_ar_row(idx).ship_to_postal_code
                         ,gl_sla_ar_row(idx).ship_to_county
                         ,gl_sla_ar_row(idx).bill_to_address1
                         ,gl_sla_ar_row(idx).bill_to_city
                         ,gl_sla_ar_row(idx).bill_to_state
                         ,gl_sla_ar_row(idx).bill_to_postal_code
                         ,gl_sla_ar_row(idx).bill_to_county 
                         ,gl_sla_ar_row(idx).ship_to_customer_name
                         ,gl_sla_ar_row(idx).bill_to_customer_name
                         ,gl_sla_ar_row(idx).site_number
                         ,v_bill_to_site_number
                   from   ar_payment_schedules_all apsa
                         ,hz_cust_accounts hzca
                         ,hz_parties hzp
                         ,hz_cust_site_uses_all hzcsu
                         ,hz_party_sites hzps
                         ,hz_cust_acct_sites_all hzcas
                         ,hz_locations hzl
                         ,hz_cust_accounts bill_hzca
                         ,hz_parties bill_hzp                         
                         ,hz_cust_site_uses_all bill_hzcsu
                         ,hz_party_sites bill_hzps
                         ,hz_cust_acct_sites_all bill_hzcas
                         ,hz_locations bill_hzl                         
                   where  1 =1
                     and  apsa.customer_trx_id            =gl_sla_ar_row(idx).dtl_id
                     and  hzca.cust_account_id(+)         =gl_sla_ar_row(idx).ship_to_customer_id
                     and  hzcas.cust_account_id(+)        =hzca.cust_account_id
                     and  hzp.party_id(+)                 =hzca.party_id
                     and  hzcsu.site_use_id(+)            =gl_sla_ar_row(idx).ship_to_site_use_id         
                     and  hzcas.cust_acct_site_id         =hzcsu.cust_acct_site_id          
                     and  hzps.party_site_id(+)           =hzcas.party_site_id
                     and  hzl.location_id(+)              =hzps.location_id
                     and  bill_hzca.cust_account_id(+)    =gl_sla_ar_row(idx).bill_to_customer_id                     
                     and  bill_hzcas.cust_account_id(+)   =bill_hzca.cust_account_id
                     and  bill_hzp.party_id(+)            =bill_hzca.party_id                     
                     and  bill_hzcsu.site_use_id(+)       =gl_sla_ar_row(idx).bill_to_site_use_id         
                     and  bill_hzcas.cust_acct_site_id    =hzcsu.cust_acct_site_id          
                     and  bill_hzps.party_site_id(+)      =hzcas.party_site_id
                     and  bill_hzl.location_id(+)         =hzps.location_id;
                 exception
                  when no_data_found then
                     gl_sla_ar_row(idx).ship_to_address1      :=Null;
                     gl_sla_ar_row(idx).ship_to_city          :=Null;
                     gl_sla_ar_row(idx).ship_to_state         :=Null;
                     gl_sla_ar_row(idx).ship_to_postal_code   :=Null;
                     gl_sla_ar_row(idx).ship_to_county        :=Null;
                     gl_sla_ar_row(idx).bill_to_address1      :=Null;
                     gl_sla_ar_row(idx).bill_to_city          :=Null;
                     gl_sla_ar_row(idx).bill_to_state         :=Null;
                     gl_sla_ar_row(idx).bill_to_postal_code   :=Null;
                     gl_sla_ar_row(idx).bill_to_county        :=Null;
                     v_shipto_missing                         :=Null;
                     gl_sla_ar_row(idx).total_freight         :=Null;
                     gl_sla_ar_row(idx).total_tax             :=Null;
                     gl_sla_ar_row(idx).invoice_total         :=Null;
                     gl_sla_ar_row(idx).gross_amount          :=Null; 
                     gl_sla_ar_row(idx).ship_to_customer_name :=Null;
                     gl_sla_ar_row(idx).bill_to_customer_name :=Null;
                     gl_sla_ar_row(idx).site_number           :=Null;
                     v_bill_to_site_number                    :=Null;                  
                  when others then
                     gl_sla_ar_row(idx).ship_to_address1      :=Null;
                     gl_sla_ar_row(idx).ship_to_city          :=Null;
                     gl_sla_ar_row(idx).ship_to_state         :=Null;
                     gl_sla_ar_row(idx).ship_to_postal_code   :=Null;
                     gl_sla_ar_row(idx).ship_to_county        :=Null;
                     gl_sla_ar_row(idx).bill_to_address1      :=Null;
                     gl_sla_ar_row(idx).bill_to_city          :=Null;
                     gl_sla_ar_row(idx).bill_to_state         :=Null;
                     gl_sla_ar_row(idx).bill_to_postal_code   :=Null;
                     gl_sla_ar_row(idx).bill_to_county        :=Null;
                     v_shipto_missing                         :=Null;
                     gl_sla_ar_row(idx).total_freight         :=Null;
                     gl_sla_ar_row(idx).total_tax             :=Null;
                     gl_sla_ar_row(idx).invoice_total         :=Null;
                     gl_sla_ar_row(idx).gross_amount          :=Null; 
                     gl_sla_ar_row(idx).ship_to_customer_name :=Null;
                     gl_sla_ar_row(idx).bill_to_customer_name :=Null;
                     gl_sla_ar_row(idx).site_number           :=Null;
                     v_bill_to_site_number                    :=Null;
                     print_log('@Step2: Issue in checking address info from audit table record, customer_trx_id ='||gl_sla_ar_row(idx).dtl_id);
                     CONC_STATUS := FND_CONCURRENT.SET_COMPLETION_STATUS('ERROR','@Check2, CUSTOMER_TRX_ID ='||gl_sla_ar_row(idx).dtl_id||', MSG ='||SQLERRM);                                 
                 end;                     
            when others then               
               gl_sla_ar_row(idx).ship_to_address1      :=Null;
               gl_sla_ar_row(idx).ship_to_city          :=Null;
               gl_sla_ar_row(idx).ship_to_state         :=Null;
               gl_sla_ar_row(idx).ship_to_postal_code   :=Null;
               gl_sla_ar_row(idx).ship_to_county        :=Null;
               gl_sla_ar_row(idx).ship_to_customer_name :=Null;
               gl_sla_ar_row(idx).bill_to_address1      :=Null;
               gl_sla_ar_row(idx).bill_to_city          :=Null;
               gl_sla_ar_row(idx).bill_to_state         :=Null;
               gl_sla_ar_row(idx).bill_to_postal_code   :=Null;
               gl_sla_ar_row(idx).bill_to_county        :=Null;
               gl_sla_ar_row(idx).bill_to_customer_name :=Null;               
               v_shipto_missing                         :=Null;
               gl_sla_ar_row(idx).total_freight         :=Null;
               gl_sla_ar_row(idx).total_tax             :=Null;
               gl_sla_ar_row(idx).invoice_total         :=Null;
               gl_sla_ar_row(idx).gross_amount          :=Null;
               gl_sla_ar_row(idx).site_number           :=Null;
               v_bill_to_site_number                    :=Null;
               print_log('@Step1: Issue in checking address info from audit table record, customer_trx_id ='||gl_sla_ar_row(idx).dtl_id);
               CONC_STATUS := FND_CONCURRENT.SET_COMPLETION_STATUS('ERROR','@Check1, CUSTOMER_TRX_ID ='||gl_sla_ar_row(idx).dtl_id||', MSG ='||SQLERRM);
               --raise_application_error(-20010, '@Step2: Failed to derive address info for customer_trx_id '||gl_sla_ar_row(idx).dtl_id);                            
           end;
       
           if v_shipto_missing ='YES' then
                 begin 
                   select regexp_replace
                            (
                               hzl.address1
                              ,'([^[:print:]])',''
                            )
                         ,hzl.city
                         ,hzl.state
                         ,hzl.postal_code
                         ,hzl.county
                         ,hzps.party_site_number
                   into   gl_sla_ar_row(idx).ship_to_address1
                         ,gl_sla_ar_row(idx).ship_to_city
                         ,gl_sla_ar_row(idx).ship_to_state
                         ,gl_sla_ar_row(idx).ship_to_postal_code
                         ,gl_sla_ar_row(idx).ship_to_county 
                         ,gl_sla_ar_row(idx).site_number                                                                                                   
                   from   hz_cust_site_uses_all hzcsu
                         ,hz_party_sites hzps
                         ,hz_cust_acct_sites_all hzcas
                         ,hz_locations hzl
                   where  1 =1
                     and  hzcas.cust_account_id      =gl_sla_ar_row(idx).ship_to_customer_id
                     and  hzcsu.site_use_id          =gl_sla_ar_row(idx).ship_to_site_use_id         
                     and  hzcas.cust_acct_site_id    =hzcsu.cust_acct_site_id          
                     and  hzps.party_site_id         =hzcas.party_site_id
                     and  hzl.location_id            =hzps.location_id;
                 exception
                  when no_data_found then
                    gl_sla_ar_row(idx).ship_to_address1      :=Null;
                    gl_sla_ar_row(idx).ship_to_city          :=Null;
                    gl_sla_ar_row(idx).ship_to_state         :=Null;
                    gl_sla_ar_row(idx).ship_to_postal_code   :=Null;
                    gl_sla_ar_row(idx).ship_to_county        :=Null;                  
                  when others then
                    gl_sla_ar_row(idx).ship_to_address1      :=Null;
                    gl_sla_ar_row(idx).ship_to_city          :=Null;
                    gl_sla_ar_row(idx).ship_to_state         :=Null;
                    gl_sla_ar_row(idx).ship_to_postal_code   :=Null;
                    gl_sla_ar_row(idx).ship_to_county        :=Null; 
                    CONC_STATUS := FND_CONCURRENT.SET_COMPLETION_STATUS
                                      (
                                         'ERROR'
                                        ,'@Check3, CUST_ACCOUNT_ID ='||gl_sla_ar_row(idx).ship_to_customer_id||
                                         ', SITE_USE_ID ='||gl_sla_ar_row(idx).ship_to_site_use_id||
                                         ', MSG ='||SQLERRM
                                      );             
                 end;
           else --I think we have covered all the scenarios at this time.
            Null;
           end if;  
           --           
           --copy back the branch and ship to geocodes 
           -- get the taxaudit_header.headerno which is one to one with customer_trx_id
           -- customer_trx_id is stored in oracleid field. 
           
           If (gl_sla_ar_row(idx).site_number Is Not Null) Then
              Null; --Looks like the ship to site number is populated
           Else
             if v_bill_to_site_number is not null then
               gl_sla_ar_row(idx).site_number :=v_bill_to_site_number;             
             else
               gl_sla_ar_row(idx).site_number :='NA';             
             end if;           
           End If;                     
           
           begin 
              
              n_exempt_amount :=0;
              -- Clean out the plsql table for each invoice before we copy the taxaudit_tax data 
              
              begin 
                txw_CN_rec.delete;
                txw_LO_rec.delete;
                txw_SC_rec.delete;
                txw_SL_rec.delete;
                txw_SS_rec.delete;
                txw_ST_rec.delete;              
              exception
               when others then
                 print_log('Fetch seq and Dtl_Id are '||n_total_fetch||' and '||gl_sla_ar_row(idx).dtl_id||', Issue in deleting plsql table '||sqlerrm);
              end;
              
              begin
               
               n_dtl_idx :=0;
               
               v_CN_flag :='N';
               v_LO_flag :='N';
               v_SC_flag :='N';
               v_SL_flag :='N';
               v_SS_flag :='N';
               v_ST_flag :='N';
               
               open taxware_data (gl_sla_ar_row(idx).dtl_id);
               fetch taxware_data bulk collect into txw_audit_tax;
               close taxware_data;
               
                if txw_audit_tax.count >0 then
                 for idx in 1 .. txw_audit_tax.count loop                    
                     n_dtl_idx :=n_dtl_idx + 1;                    
                     if txw_audit_tax(idx).taxlevel ='CN' then                        
                       txw_CN_rec(n_dtl_idx) :=txw_audit_tax(idx);
                       v_CN_flag :='Y';
                     elsif txw_audit_tax(idx).taxlevel ='LO' then
                       txw_LO_rec(n_dtl_idx) :=txw_audit_tax(idx); 
                       v_LO_flag :='Y';                   
                     elsif txw_audit_tax(idx).taxlevel ='SC' then
                       txw_SC_rec(n_dtl_idx) :=txw_audit_tax(idx);
                       v_SC_flag :='Y';                    
                     elsif txw_audit_tax(idx).taxlevel ='SL' then
                       txw_SL_rec(n_dtl_idx) :=txw_audit_tax(idx); 
                       v_SL_flag :='Y';                   
                     elsif txw_audit_tax(idx).taxlevel ='SS' then
                       txw_SS_rec(n_dtl_idx) :=txw_audit_tax(idx);
                       v_SS_flag :='Y';                    
                     elsif txw_audit_tax(idx).taxlevel ='ST' then
                       txw_ST_rec(n_dtl_idx) :=txw_audit_tax(idx); 
                       v_ST_flag :='Y';                   
                     else
                       Null;                                                                                            
                     end if;                      
                 end loop;   
                 b_fetch :=TRUE;                                 
                else
                 b_fetch :=FALSE;                
                  --print_log('No data found  for taxware.taxaudit_tax data, oracleid ='||gl_sla_ar_row(idx).dtl_id);
                end if;
                
              exception
               when no_data_found then
                b_fetch :=FALSE;
               when others then
                b_fetch :=FALSE;
                print_log('Issue in fetching xxcus.xxcus_taxaudit_tax_archive data for oracleid ='||gl_sla_ar_row(idx).dtl_id);
              end;
              
              -- if we have the records from the taxaudit_tax table then check the rest of the logic else stop it 
              if (not b_fetch) then
               Null; --we do not have any tax audit records to check with
              else
                  -- get primary state tax rate and amount 
                  begin  
                    -- get primary state level numbers
                    --if txw_ST_rec.count >1 then 
                    
                      n_taxamt   :=0;
                      n_taxrate  :=0;
                      n_exmptamt :=0;
                      n_baseamt  :=0;
                      
                    If v_ST_flag ='Y' then
                      for idx in txw_ST_rec.first .. txw_ST_rec.last loop
                        n_taxamt   :=n_taxamt   + txw_ST_rec(idx).taxamt;
                        n_taxrate  :=txw_ST_rec(idx).taxrate;
                        n_exmptamt :=n_exmptamt + txw_ST_rec(idx).exmptamt;
                        --n_baseamt  :=n_baseamt  + txw_ST_rec(idx).baseamt;
                      end loop;
                      
                        gl_sla_ar_row(idx).state_exempt_amount    :=n_exmptamt;
                        gl_sla_ar_row(idx).state_tax_amount       :=n_taxamt;
                        gl_sla_ar_row(idx).state_tax_rate         :=n_taxrate; 
                    Else
                        gl_sla_ar_row(idx).state_exempt_amount    :=0;
                        gl_sla_ar_row(idx).state_tax_amount       :=0;
                        gl_sla_ar_row(idx).state_tax_rate         :=0;                    
                    End If;                  

                    --print_log('After ST ');
                    
                      n_taxamt   :=0;
                      n_taxrate  :=0;
                      n_exmptamt :=0;
                      n_baseamt  :=0;
                    
                    If v_CN_flag ='Y' then 
                      for idx in txw_CN_rec.first .. txw_CN_rec.last loop
                        n_taxamt   :=n_taxamt   + txw_CN_rec(idx).taxamt;
                        n_taxrate  :=txw_CN_rec(idx).taxrate;
                        n_exmptamt :=n_exmptamt + txw_CN_rec(idx).exmptamt;
                        --n_baseamt  :=n_baseamt  + txw_CN_rec(idx).baseamt;                                                       
                      end loop;
                      
                        gl_sla_ar_row(idx).county_exempt_amount    :=n_exmptamt;
                        gl_sla_ar_row(idx).county_tax_amount       :=n_taxamt;
                        gl_sla_ar_row(idx).county_tax_rate         :=n_taxrate;                    
                    Else
                        gl_sla_ar_row(idx).county_exempt_amount    :=0;
                        gl_sla_ar_row(idx).county_tax_amount       :=0;
                        gl_sla_ar_row(idx).county_tax_rate         :=0;                    
                    End If; 
                        
                    --print_log('After CN ');
                    
                      n_taxamt   :=0;
                      n_taxrate  :=0;
                      n_exmptamt :=0;
                      n_baseamt  :=0;
                      
                    If v_LO_flag ='Y' then  
                      for idx in txw_LO_rec.first .. txw_LO_rec.last loop
                        n_taxamt   :=n_taxamt   + txw_LO_rec(idx).taxamt;
                        n_taxrate  :=txw_LO_rec(idx).taxrate;
                        n_exmptamt :=n_exmptamt + txw_LO_rec(idx).exmptamt;
                        --n_baseamt  :=n_baseamt  + txw_LO_rec(idx).baseamt;                         
                      end loop;
                      
                        gl_sla_ar_row(idx).city_exempt_amount    :=n_exmptamt;
                        gl_sla_ar_row(idx).city_tax_amount       :=n_taxamt;
                        gl_sla_ar_row(idx).city_tax_rate         :=n_taxrate;                    
                    Else 
                        gl_sla_ar_row(idx).city_exempt_amount    :=0;
                        gl_sla_ar_row(idx).city_tax_amount       :=0;
                        gl_sla_ar_row(idx).city_tax_rate         :=0;                     
                    End If;

                    --print_log('After LO '); 
                                       
                      n_taxamt   :=0;
                      n_taxrate  :=0;
                      n_exmptamt :=0;
                      n_baseamt  :=0;
                      
                    If v_SS_flag ='Y' then 
                      for idx in txw_SS_rec.first .. txw_SS_rec.last loop
                       --print_log('SS taxamt ='||txw_SS_rec(idx).taxamt);
                        n_taxamt   :=n_taxamt   + txw_SS_rec(idx).taxamt;
                        n_taxrate  :=txw_SS_rec(idx).taxrate;
                        n_exmptamt :=n_exmptamt + txw_SS_rec(idx).exmptamt;
                        --n_baseamt  :=n_baseamt  + txw_SS_rec(idx).baseamt;                        
                      end loop;
                      
                        gl_sla_ar_row(idx).sec_state_exempt_amount    :=n_exmptamt;
                        gl_sla_ar_row(idx).sec_state_tax_amount       :=n_taxamt;
                        gl_sla_ar_row(idx).sec_state_tax_rate         :=n_taxrate;                    
                    Else
                        gl_sla_ar_row(idx).sec_state_exempt_amount    :=0;
                        gl_sla_ar_row(idx).sec_state_tax_amount       :=0;
                        gl_sla_ar_row(idx).sec_state_tax_rate         :=0;
                    End If;
                        
                    --print_log('After SS ');
                                        
                      n_taxamt   :=0;
                      n_taxrate  :=0;
                      n_exmptamt :=0;
                      n_baseamt  :=0;
                    
                    If v_SC_flag ='Y' then 
                      for idx in txw_SC_rec.first .. txw_SC_rec.last loop
                        n_taxamt   :=n_taxamt   + txw_SC_rec(idx).taxamt;
                        n_taxrate  :=txw_SC_rec(idx).taxrate;
                        n_exmptamt :=n_exmptamt + txw_SC_rec(idx).exmptamt;
                        --n_baseamt  :=n_baseamt  + txw_SC_rec(idx).baseamt;                 
                      end loop;
                      
                        gl_sla_ar_row(idx).sec_county_exempt_amount    :=n_exmptamt;
                        gl_sla_ar_row(idx).sec_county_tax_amount       :=n_taxamt;
                        gl_sla_ar_row(idx).sec_county_tax_rate         :=n_taxrate;                    
                    Else
                        gl_sla_ar_row(idx).sec_county_exempt_amount    :=0;
                        gl_sla_ar_row(idx).sec_county_tax_amount       :=0;
                        gl_sla_ar_row(idx).sec_county_tax_rate         :=0;                    
                    End If; 

                    --print_log('After SC '); 
                                           
                      n_taxamt   :=0;
                      n_taxrate  :=0;
                      n_exmptamt :=0;
                      n_baseamt  :=0;
                      
                    If v_SL_flag ='Y' then 
                      for idx in txw_SL_rec.first .. txw_SL_rec.last loop
                         n_taxamt   :=n_taxamt   + txw_SL_rec(idx).taxamt;
                         n_taxrate  :=txw_SL_rec(idx).taxrate;
                         n_exmptamt :=n_exmptamt + txw_SL_rec(idx).exmptamt;
                         --n_baseamt  :=n_baseamt  + txw_SL_rec(idx).baseamt;                 
                      end loop;
                      
                        gl_sla_ar_row(idx).sec_city_exempt_amount    :=n_exmptamt;
                        gl_sla_ar_row(idx).sec_city_tax_amount       :=n_taxamt;
                        gl_sla_ar_row(idx).sec_city_tax_rate         :=n_taxrate;                    
                    Else
                        gl_sla_ar_row(idx).sec_city_exempt_amount    :=0;
                        gl_sla_ar_row(idx).sec_city_tax_amount       :=0;
                        gl_sla_ar_row(idx).sec_city_tax_rate         :=0;                    
                    End If;
                                          
                    --print_log('After SL ');
                    
                    --print_log('exit analyzing secondary city tax data');                

                  exception
                   when no_data_found then
                     print_log('@04-NOV-102, MSG ='||sqlerrm);
                   when others then                    
                     print_log('Issue in analyzing taxaudit_tax table data for headerno ='||n_headerno||', customer_trx_id ='||gl_sla_ar_row(idx).dtl_id);
                     print_log('Issue in analyzing taxaudit_tax table data, message ='||sqlerrm);                     
                  end;              
              end if;               
           exception
            when no_data_found then
             print_log
                     (
                       'Failed to fetch taxware.taxaudit_header record for oracleid ='||gl_sla_ar_row(idx).dtl_id||
                       ', transdate ='||gl_sla_ar_row(idx).invoice_creation_date||
                       ', custnum [AR Sitenumber] ='||gl_sla_ar_row(idx).site_number
                     );
             Null;
            when others then
             print_log('@04-NOV-101, MSG ='||sqlerrm);
           end;
                    
      end loop;                       

       begin 
         print_log ('Before raw SQL bulk insert');       
        forall recs in gl_sla_ar_row.first .. gl_sla_ar_row.last
         insert into xxcus.xxcus_tax_return_report_b values gl_sla_ar_row(recs);
           if sql%rowcount >0 then
            print_log ('Bulk insert count ='||sql%rowcount);
           else
            print_log ('Bulk insert count ='||0);
           end if;
         print_log ('After raw SQL bulk insert');
       exception
        when others then
           --
           begin
            for idx in 1 .. gl_sla_ar_row.count loop 
             capture_extract_errors 
               (
                 p_record_type     =>'SUMMARY DATA' --varchar2
                ,p_customer_trx_id =>gl_sla_ar_row(idx).dtl_id --number
                ,p_trx_number      =>gl_sla_ar_row(idx).invoice_number --varchar2
                ,p_period_name     =>p_period_name --varchar2
                ,p_request_id      =>n_req_id
               );
            end loop;                     
           end;
           --
           xxcus_error_pkg.xxcus_error_main_api
                               (
                                 p_called_from       => l_err_callfrom
                                ,p_calling           => '@ Insert of table xxcus.xxcus_tax_return_report_b [Invoice Summary].'
                                ,p_request_id        => fnd_global.conc_request_id
                                ,p_ora_error_msg     => substr(' Error_Stack...' ||
                                                               dbms_utility.format_error_stack() ||
                                                               ' Error_Backtrace...' ||
                                                               dbms_utility.format_error_backtrace()
                                                              ,1
                                                              ,2000)
                                ,p_error_desc        => substr(sqlerrm
                                                              ,1
                                                              ,240)
                                ,p_distribution_list => l_distro_list
                                ,p_module            => l_module
                               );        
         print_log('@Fetch seq '||n_total_fetch||', issue in forall insert ='||sqlerrm);
         --  
         b_extract_errors :=TRUE;
         --          
       end;
                  
    else
     print_log('gl_sla_ar_row.COUNT =0');  
    end if; --gl_sla_ar_row.COUNT =0          
   end loop;   
   close gl_sla_ar;       
   print_log('After Close cursor');
   print_log('Summary data generated, exit.');
   --
   if p_detail_required ='Y' then
    --
    begin 
      execute immediate 'ALTER INDEX XXCUS.XXCUS_TAXAUDIT_TAX_ARCHIVE_N1 REBUILD';
    exception
     when others then
      print_log('Issue in rebuild of index XXCUS.XXCUS_TAXAUDIT_TAX_ARCHIVE_N1, message ='||sqlerrm);
    end;
    --
    begin
      execute immediate 'ALTER INDEX XXCUS.XXCUS_TAXAUDIT_TAX_ARCHIVE_N2 REBUILD';
    exception
     when others then
      print_log('Issue in rebuild of index XXCUS.XXCUS_TAXAUDIT_TAX_ARCHIVE_N2, message ='||sqlerrm);
    end;    
    -- 
    print_log('Detail audit data is requested. We will proceed to generate the audit detail data.');
    --
    open my_hdr_inv;
    fetch my_hdr_inv bulk collect into my_hdr_inv_rec;
    close my_hdr_inv;
    --
    if my_hdr_inv_rec.count >0 then
    --
     for idx101 in my_hdr_inv_rec.first .. my_hdr_inv_rec.last loop 
      -- ++++++++++++++++++  
        open my_inv_header_info (my_hdr_inv_rec(idx101).dtl_id);
        fetch my_inv_header_info into my_invoice_info;
        close my_inv_header_info;       
      --
        open my_unique_lines (my_hdr_inv_rec(idx101).dtl_id);
        fetch my_unique_lines bulk collect into my_unique_inv_lines;
        close my_unique_lines;
      --
       if my_unique_inv_lines.count >0 then
       --
        for idx3 in my_unique_inv_lines.first .. my_unique_inv_lines.last loop
         --
          -- get taxable amount
          l_taxable_amount :=get_taxable_amount(my_hdr_inv_rec(idx101).dtl_id, my_unique_inv_lines(idx3).customer_trx_line_id);
                           
          --get exempt amount
          l_exempt_amount  :=get_exempt_amount(my_hdr_inv_rec(idx101).dtl_id, my_unique_inv_lines(idx3).customer_trx_line_id); 
                          
          --get location based ar taxes
          get_location_based_tax
                (
                  my_hdr_inv_rec(idx101).dtl_id                       --Input
                 ,my_unique_inv_lines(idx3).customer_trx_line_id   --Input
                 ,l_ar_state_tax   --Output
                 ,l_ar_county_tax  --Output
                 ,l_ar_city_tax    --Output
                );         
         --
          open my_invoice_lines (my_hdr_inv_rec(idx101).dtl_id, my_unique_inv_lines(idx3).customer_trx_line_id, g_req_id, my_invoice_info.taxware_match);
          fetch my_invoice_lines bulk collect into my_lines_rec;
          close my_invoice_lines;
         --
             if my_lines_rec.count >0 then
             --
                  for idx2 in my_lines_rec.first .. my_lines_rec.last loop 
                   --   
                       --At this point there must be some sort of ship to address,but not guaranteed.
                       
                       If my_lines_rec(idx2).point_of_taxation ='ORIGIN' Then
                       
                         my_lines_rec(idx2).taxable_address 
                                    :=my_lines_rec(idx2).branch_address1||', '||
                                      my_lines_rec(idx2).branch_city||', '||
                                      my_lines_rec(idx2).branch_state||', '||
                                      my_lines_rec(idx2).branch_postal_code||', '||
                                      my_lines_rec(idx2).branch_county;
                         
                         --my_lines_rec(idx2).taxable_geocode :=my_lines_rec(idx2).branch_geocode;
                         
                       Elsif my_lines_rec(idx2).point_of_taxation ='DESTINATION' Then
                       
                         my_lines_rec(idx2).taxable_address 
                                    :=my_invoice_info.ship_to_address1||', '||
                                      my_invoice_info.ship_to_city||', '||
                                      my_invoice_info.ship_to_state||', '||
                                      my_invoice_info.ship_to_postal_code||', '||
                                      my_invoice_info.ship_to_county;
                         
                         /*
                         If (
                                  my_invoice_info.ship_to_state is not null 
                              and my_invoice_info.ship_to_city is not null 
                              and my_invoice_info.ship_to_postal_code is not null
                         
                            ) Then
                            
                           --print_log('GEOCODE state code ='||gl_sla_ar_row(idx).ship_to_state);
                           
                           my_lines_rec(idx2).taxable_geocode :=apps.xxcus_ar_geocode_pkg.get_ship_to_geocode
                                                                (
                                                                  upper(my_invoice_info.ship_to_state)
                                                                 ,upper(my_invoice_info.ship_to_city)
                                                                 ,substr(my_invoice_info.ship_to_postal_code, 1, 5)
                                                                 ,upper(my_invoice_info.ship_to_county)
                                                                );
                         Else
                         
                            my_lines_rec(idx2).taxable_geocode :=Null;
                            
                         End If; 
                        */
                         
                       Else
                         my_lines_rec(idx2).taxable_address :=Null;
                         --my_lines_rec(idx2).taxable_geocode :=Null;
                       End If;                   
                   --       
                   --my_lines_rec(idx2).point_of_taxation    :=my_invoice_info.point_of_taxation;
                   --my_lines_rec(idx2).ship_via             :=my_invoice_info.ship_via;
                   --
                   /*
                   my_lines_rec(idx2).taxable_address      :=my_invoice_info.taxable_address;                   
                   my_lines_rec(idx2).branch_address1      :=my_invoice_info.branch_address1;
                   my_lines_rec(idx2).branch_city          :=my_invoice_info.branch_city;
                   my_lines_rec(idx2).branch_state         :=my_invoice_info.branch_state;
                   my_lines_rec(idx2).branch_postal_code   :=my_invoice_info.branch_postal_code;
                   my_lines_rec(idx2).branch_county        :=my_invoice_info.branch_county;
                   my_lines_rec(idx2).branch_geocode       :=my_invoice_info.branch_geocode;
                   */ 
                   --      
                   my_lines_rec(idx2).bill_to_customer_name :=my_invoice_info.bill_to_customer_name;                      
                   my_lines_rec(idx2).bill_to_address1     :=my_invoice_info.bill_to_address1; 
                   my_lines_rec(idx2).bill_to_city         :=my_invoice_info.bill_to_city;
                   my_lines_rec(idx2).bill_to_state        :=my_invoice_info.bill_to_state;
                   my_lines_rec(idx2).bill_to_postal_code  :=my_invoice_info.bill_to_postal_code;
                   my_lines_rec(idx2).bill_to_county       :=my_invoice_info.bill_to_county;
                   --
                   my_lines_rec(idx2).ship_to_customer_name :=my_invoice_info.ship_to_customer_name;         
                   my_lines_rec(idx2).ship_to_address1     :=my_invoice_info.ship_to_address1; 
                   my_lines_rec(idx2).ship_to_city         :=my_invoice_info.ship_to_city;
                   my_lines_rec(idx2).ship_to_state        :=my_invoice_info.ship_to_state;
                   my_lines_rec(idx2).ship_to_postal_code  :=my_invoice_info.ship_to_postal_code;
                   my_lines_rec(idx2).ship_to_county       :=my_invoice_info.ship_to_county; 
                   my_lines_rec(idx2).ship_to_geocode      :=Null; 
                   -- 
                   my_lines_rec(idx2).job_code             :=my_invoice_info.site_number;
                   my_lines_rec(idx2).po_number            :=Null;
                   --
                   my_lines_rec(idx2).invoice_total        :=my_invoice_info.invoice_total;
                   my_lines_rec(idx2).total_freight        :=my_invoice_info.total_freight;   
                   my_lines_rec(idx2).total_tax            :=my_invoice_info.total_tax;                       
                   --                                
                   my_lines_rec(idx2).taxable_amount       :=l_taxable_amount;
                   my_lines_rec(idx2).ar_state_tax         :=l_ar_state_tax; 
                   my_lines_rec(idx2).ar_county_tax        :=l_ar_county_tax; 
                   my_lines_rec(idx2).ar_city_tax          :=l_ar_city_tax;  
                   my_lines_rec(idx2).ar_exempt_amount     :=l_exempt_amount; 
                   my_lines_rec(idx2).taxware_match        :=my_invoice_info.taxware_match;                   
                   --                                                              
                  end loop; -- idx2 in my_lines_rec.first .. my_lines_rec.last loop 
             --
                  begin        
                    forall recs in my_lines_rec.first .. my_lines_rec.last
                     insert into xxcus.xxcus_tax_return_rpt_detail_b values my_lines_rec(recs);
                  exception
                    --
                    when others then
                       --
                       begin
                        for idx in 1 .. my_lines_rec.count loop 
                         capture_extract_errors 
                           (
                             p_record_type     =>'DETAIL DATA' --varchar2
                            ,p_customer_trx_id =>my_hdr_inv_rec(idx101).dtl_id --number
                            ,p_trx_number      =>my_lines_rec(idx).invoice_number --varchar2
                            ,p_period_name     =>p_period_name --varchar2
                            ,p_request_id      =>g_req_id
                           );
                        end loop;                     
                       end;
                       --
                       xxcus_error_pkg.xxcus_error_main_api
                                           (
                                             p_called_from       => l_err_callfrom
                                            ,p_calling           => '@ Insert of table xxcus.xxcus_tax_return_rpt_detail_b [Invoice Detail].'
                                            ,p_request_id        => fnd_global.conc_request_id
                                            ,p_ora_error_msg     => substr(' Error_Stack...' ||
                                                                           dbms_utility.format_error_stack() ||
                                                                           ' Error_Backtrace...' ||
                                                                           dbms_utility.format_error_backtrace()
                                                                          ,1
                                                                          ,2000)
                                            ,p_error_desc        => substr(sqlerrm
                                                                          ,1
                                                                          ,240)
                                            ,p_distribution_list => l_distro_list
                                            ,p_module            => l_module
                                           );                    
                     print_log('Customer_Trx_Id ='|| my_hdr_inv_rec(idx101).dtl_id|| 
                               ',Customer_Trx_Line_Id = '||my_unique_inv_lines(idx3).customer_trx_line_id
                              );                                         
                     print_log('@xxcus.xxcus_tax_return_rpt_detail_b bulk insert, message ='||sqlerrm);
                     --
                     b_extract_errors :=TRUE;
                     --                     
                  end;             
             --
             end if;  --my_lines_rec.count >0           
         --        
        end loop; --idx3         
       --     
       end if; --my_unique_inv_lines.count >0    
      -- ++++++++++++++++++
     end loop; -- idx101 in my_hdr_inv_rec.first .. my_hdr_inv_rec.last
    end if; --my_hdr_inv_rec.count >0
    --
    --    
   else
    --
     print_log('Detail audit data is not requested. End of program.');
    --
   end if;
   --
   IF (b_extract_errors) THEN
    CONC_STATUS := FND_CONCURRENT.SET_COMPLETION_STATUS('ERROR','Reporting table extract errors, please check request log and table xxcus.xxcus_artax_extract_errors_all.');
   END IF;
   --  
  exception
   --
   when others then
    --
    --fnd_file.put_line(fnd_file.log, 'Error in caller apps.xxcus_ar_tax_return_pkg.main ='||sqlerrm);
    l_err_msg := l_sec;
        l_err_msg := l_err_msg || ' ...Error_Stack...' ||
                     dbms_utility.format_error_stack() || ' Error_Backtrace...' ||
                     dbms_utility.format_error_backtrace();
        fnd_file.put_line(fnd_file.log, l_err_msg);
        l_err_callpoint := l_sec;      
        xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                            ,p_calling           => l_err_callpoint
                                            ,p_request_id        => fnd_global.conc_request_id
                                            ,p_ora_error_msg     => substr(' Error_Stack...' ||
                                                                           dbms_utility.format_error_stack() ||
                                                                           ' Error_Backtrace...' ||
                                                                           dbms_utility.format_error_backtrace()
                                                                          ,1
                                                                          ,2000)
                                            ,p_error_desc        => substr(l_err_msg
                                                                          ,1
                                                                          ,240)
                                            ,p_distribution_list => l_distro_list
                                            ,p_module            => l_module);
   --                                                
  end main;  
/**************************************************************************
 *
 * FUNCTION: unload_data
 * DESCRIPTION: To massage the temp table data and populate the summary and the detail table with all required columns.
 * PARAMETERS
 * ==========
 * NAME              TYPE     DESCRIPTION
.* ----------------- -------- ---------------------------------------------
 * p_my_cursor       IN       REFCURSOR [pass an entire SQL]
 * p_period          IN       Fiscal period name like Jan-2014
 * p_file_name       IN      text file name
 * p_file_path IN    IN      path where text file is created
 * p_file_extn      IN      .txt 
 * RETURN VALUE: Details of text files created along with session info and queried like a table
 * CALLED BY:  create_files
 *************************************************************************/    
  Function unload_data
   (
     p_my_cursor         IN SYS_REFCURSOR
    ,p_period            IN VARCHAR2
    ,p_file_name         IN VARCHAR2
    ,p_file_path         IN VARCHAR2
    ,p_file_extn         IN VARCHAR2 DEFAULT '.txt'
   ) Return xxcus_spool_file_tbl_ntt PIPELINED  As --PARALLEL_ENABLE (PARTITION p_my_cursor BY ANY)
   --
  
   --
    TYPE rows_ntt IS TABLE OF VARCHAR2(32767);
    my_rows       rows_ntt;
    file_id       utl_file.file_type;

   --
    v_buffer      VARCHAR2(32767);
    n_sid         NUMBER;
    v_sid         VARCHAR2(10);
    v_name        VARCHAR2(150);
    n_lines       PLS_INTEGER :=0;
    C_EOL         CONSTANT VARCHAR2(1) :=CHR(10);
    C_EOL_LEN     CONSTANT PLS_INTEGER :=LENGTH(C_EOL);
    C_MAXLINE     CONSTANT PLS_INTEGER :=32767;   
   --
    d_start_date  date :=sysdate;
    d_end_date    date :=Null; 
    p_limit       Number :=5000;   
   --
  begin 
  --
   execute immediate 'ALTER SESSION ENABLE PARALLEL DML';
   
   -- Get the SID padded to append the file names for ease of identification
   n_sid   :=sys_context('USERENV', 'sid');
   v_sid   :=lpad(sys_context('USERENV', 'sid'), 10,0);
   
   --Assign the file name 
   v_name :=p_file_name||'_'||p_period||'_'||v_sid||p_file_extn;
   
   --Get the file pointer
   file_id :=utl_file.fopen(p_file_path, v_name, 'W', C_MAXLINE);     
  --
   Loop
    --
    Fetch p_my_cursor Bulk Collect Into my_rows Limit p_limit;
    --
    Exit When my_rows.count =0;
    --
    If my_rows.count >0 then
     --
     --
      For idx in 1 .. my_rows.count Loop 
      --
        utl_file.put_line(file_id, my_rows(idx));
      --
      End Loop;
     -- 
      n_lines :=n_lines + my_rows.count;
     --
    End If;
    --
   End Loop;
  --
   Close p_my_cursor;
  --
   utl_file.put_line(file_id, v_buffer);
   utl_file.fclose(file_id);
  --
   PIPE ROW (xxcus_spool_file_tbl(v_name, n_lines, n_sid, d_start_date, sysdate));
  --
  exception
  --
   when others then
   --
    print_log('@unload_data, outer block, when others -message ='||sqlerrm);
   --
  end unload_data;
 /* *************************************************************************
 *
 * FUNCTION: create_files
 * DESCRIPTION: To massage the temp table data and populate the summary and the detail table with all required columns.
 * PARAMETERS
 * ==========
 * NAME              TYPE     DESCRIPTION
.* ----------------- -------- ---------------------------------------------
 * p_period          IN       Fiscal period name like Jan-2014
 * p_file_name       IN      text file name
 * p_file_path IN    IN      path where text file is created
 * p_file_extn       IN      .txt 
 * retcode           OUT   standard concurrent program return variable
 * errbuf            OUT    standard concurrent program return variable
 * CALLED BY:  HDS AR Tax Audit Return Report Set (Request Set)
 ************************************************************************ */   
  Procedure create_files
   (
     retcode      OUT NUMBER
    ,errbuf       OUT VARCHAR2
    ,p_period     IN  VARCHAR2    
    ,p_file_name  IN  VARCHAR2
    ,p_file_path  IN  VARCHAR2
    ,p_file_extn  IN  VARCHAR2 DEFAULT '.txt'
   ) 
   IS
   --
    CURSOR files_written
     (
       p_file IN VARCHAR2
      ,p_path IN VARCHAR2
      ,p_extn IN VARCHAR2
     ) 
     IS
    SELECT my_tbl.file_name
          ,my_tbl.total_lines
          ,my_tbl.session_id
          ,to_char(my_tbl.start_date, 'DD-MON-YYYY HH24:MI:SS') start_date_time
          ,to_char(my_tbl.end_date, 'DD-MON-YYYY HH24:MI:SS') end_date_time
    FROM   TABLE
            (
              xxcus_ar_tax_return_pkg.unload_data
               (
                 CURSOR
                  (
                    SELECT /*+ PARALLEL(txw_dtl, 2, 1) */
                           tax_audit_detail_data
                    FROM   XXCUS.XXCUS_TAX_RETURN_RPT_DETAIL_V txw_dtl
                  )
                 ,p_period
                 ,p_file
                 ,p_path
                 ,p_extn             
               ) --end for xxcus_ar_tax_return_pkg.unload_data       
            ) my_tbl; --end for virtual TABLE      
   --
    CURSOR summary_file
     (
       p_file IN VARCHAR2
      ,p_path IN VARCHAR2
      ,p_extn IN VARCHAR2
     ) 
     IS
    SELECT my_tbl.file_name
          ,my_tbl.total_lines
          ,my_tbl.session_id
          ,to_char(my_tbl.start_date, 'DD-MON-YYYY HH24:MI:SS') start_date_time
          ,to_char(my_tbl.end_date, 'DD-MON-YYYY HH24:MI:SS') end_date_time
    FROM   TABLE
            (
              xxcus_ar_tax_return_pkg.unload_data
               (
                 CURSOR
                  (
                    SELECT /*+ PARALLEL (txw_summ,8) */
                           tax_audit_summary_data
                    FROM   XXCUS.XXCUS_TAX_RETURN_RPT_SUMMARY_V txw_summ
                  )
                 ,p_period
                 ,p_file
                 ,p_path
                 ,p_extn             
               ) --end for xxcus_ar_tax_return_pkg.unload_data       
            ) my_tbl; --end for virtual TABLE    
   --
   v_cr VARCHAR2(1) :='
';  
   v_file_name varchar2(60) :='HDS_TAXAUDIT_SUMMARY'; 
   --
  begin 
  --
   print_log('File Path ='||p_file_path);
   -- Taxaudit detail file extract
   for records in files_written (p_file_name, p_file_path, p_file_extn)
    loop 
     
     print_log(
                 'File name ='||records.file_name||v_cr||
                 ', Total lines ='||records.total_lines||v_cr||
                 ', Session id ='||records.session_id||v_cr||
                 ', Start date and time ='||records.start_date_time||v_cr||                                                   
                 ', End date and time ='||records.end_date_time
              );
    end loop;
  -- Taxaudit summary file extract
   for records in summary_file (v_file_name, p_file_path, p_file_extn)
    loop 
     
     print_log(
                 'File name ='||records.file_name||v_cr||
                 ', Total lines ='||records.total_lines||v_cr||
                 ', Session id ='||records.session_id||v_cr||
                 ', Start date and time ='||records.start_date_time||v_cr||                                                   
                 ', End date and time ='||records.end_date_time
              );
    end loop;
  --  
  exception
  --
   when others then
   --
    print_log('@create_files, outer block, when others -message ='||sqlerrm);
    rollback;
   --
  end create_files;

end xxcus_ar_tax_return_pkg;
/