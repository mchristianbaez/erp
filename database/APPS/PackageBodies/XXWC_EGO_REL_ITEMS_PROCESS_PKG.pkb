CREATE OR REPLACE PACKAGE BODY apps.xxwc_ego_rel_items_process_pkg
/********************************************************************************
File Name: xxwc_ego_rel_items_process_pkg
PROGRAM TYPE: PL/SQL Package body
HISTORY
PURPOSE: Procedures and functions for creating the WCS extracts
==================================================================================
VERSION DATE          AUTHOR(S)               DESCRIPTION
------- -----------   --------------- --------------------------------------------
1.0     13-May-2014   Praveen Pawar           Initial creation of the package body
*********************************************************************************/
AS
   FUNCTION load_and_process_rel_items (
      pv_transaction_type        IN   VARCHAR2,
      pv_org_code                IN   VARCHAR2,
      pv_item_number             IN   VARCHAR2,
      pv_related_item_number     IN   VARCHAR2,
      pv_relationship_type       IN   VARCHAR2,
      pv_reciprocal_flag         IN   VARCHAR2,
      pv_planning_enabled_flag   IN   VARCHAR2,
      pd_start_date              IN   DATE,
      pd_end_date                IN   DATE
   )
      RETURN VARCHAR2
   IS
      l_org_id                  NUMBER          := NULL;
      l_rec_cnt                 NUMBER          := NULL;
      l_inv_item_id             NUMBER          := NULL;
      l_related_item_id         NUMBER          := NULL;
      l_relationship_type_id    NUMBER          := NULL;
      l_end_date                DATE            := NULL;
      l_start_date              DATE            := NULL;
      x_rowid                   VARCHAR2 (4000) := NULL;
      l_err_msg                 VARCHAR2 (4000) := NULL;
      l_reciprocal_flag         VARCHAR2 (1)    := NULL;
      l_planning_enabled_flag   VARCHAR2 (1)    := NULL;
   BEGIN
      l_org_id := fnd_profile.VALUE ('XXWC_ITEM_MASTER_ORG');
      l_relationship_type_id := TO_NUMBER (pv_relationship_type);

      IF pv_transaction_type = 'CREATE'
      THEN
         BEGIN
            SELECT COUNT (mtri.ROWID)
              INTO l_rec_cnt
              FROM mtl_system_items_b msia,
                   mtl_system_items_b msib,
                   mtl_related_items mtri
             WHERE msia.segment1 = TRIM (pv_item_number)
               AND msib.segment1 = TRIM (pv_related_item_number)
               AND mtri.inventory_item_id = msia.inventory_item_id
               AND mtri.related_item_id = msib.inventory_item_id
               AND msia.organization_id = msib.organization_id
               AND msia.organization_id = l_org_id
               AND mtri.relationship_type_id = l_relationship_type_id;

            IF     TRIM (pd_start_date) IS NOT NULL
               AND TRIM (pd_end_date) IS NOT NULL
            THEN
               IF TRIM (pd_start_date) > TRIM (pd_end_date)
               THEN
                  l_err_msg :=
                               'Start date can not be greater than end date.';
                  RETURN l_err_msg;
               END IF;
            END IF;
												
												IF TRIM(pv_item_number) = TRIM(pv_related_item_number)
												THEN
												
                  l_err_msg :=
                               'You cannot relate an item to itself.';
                  RETURN l_err_msg;
												
												END IF;

            IF l_rec_cnt = 0
            THEN
               BEGIN
                  SELECT inventory_item_id
                    INTO l_inv_item_id
                    FROM mtl_system_items_b
                   WHERE segment1 = TRIM (pv_item_number)
                     AND organization_id = l_org_id;

                  SELECT inventory_item_id
                    INTO l_related_item_id
                    FROM mtl_system_items_b
                   WHERE segment1 = TRIM (pv_related_item_number)
                     AND organization_id = l_org_id;

                  mtl_related_items_pkg.insert_row
                         (x_rowid                      => x_rowid,
                          x_inventory_item_id          => l_inv_item_id,
                          x_organization_id            => l_org_id,
                          x_related_item_id            => l_related_item_id,
                          x_relationship_type_id       => l_relationship_type_id,
                          x_reciprocal_flag            => pv_reciprocal_flag,
                          x_planning_enabled_flag      => pv_planning_enabled_flag,
                          x_start_date                 => pd_start_date,
                          x_end_date                   => pd_end_date,
                          x_attr_context               => NULL,
                          x_attr_char1                 => NULL,
                          x_attr_char2                 => NULL,
                          x_attr_char3                 => NULL,
                          x_attr_char4                 => NULL,
                          x_attr_char5                 => NULL,
                          x_attr_char6                 => NULL,
                          x_attr_char7                 => NULL,
                          x_attr_char8                 => NULL,
                          x_attr_char9                 => NULL,
                          x_attr_char10                => NULL,
                          x_attr_num1                  => NULL,
                          x_attr_num2                  => NULL,
                          x_attr_num3                  => NULL,
                          x_attr_num4                  => NULL,
                          x_attr_num5                  => NULL,
                          x_attr_num7                  => NULL,
                          x_attr_num6                  => NULL,
                          x_attr_num8                  => NULL,
                          x_attr_num9                  => NULL,
                          x_attr_num10                 => NULL,
                          x_attr_date1                 => NULL,
                          x_attr_date2                 => NULL,
                          x_attr_date3                 => NULL,
                          x_attr_date4                 => NULL,
                          x_attr_date5                 => NULL,
                          x_attr_date6                 => NULL,
                          x_attr_date7                 => NULL,
                          x_attr_date8                 => NULL,
                          x_attr_date9                 => NULL,
                          x_attr_date10                => NULL,
                          x_last_update_date           => SYSDATE,
                          x_last_updated_by            => fnd_global.user_id,
                          x_creation_date              => SYSDATE,
                          x_created_by                 => fnd_global.user_id,
                          x_last_update_login          => NULL,
                          x_object_version_number      => NULL
                         );
                  COMMIT;
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     l_err_msg := 'Item doesnot exists. Error: ' || SQLERRM;
                     RETURN l_err_msg;
                  WHEN OTHERS
                  THEN
                     l_err_msg :=
                           'Error while creating relationship. Error: '
                        || SQLERRM;
                     RETURN l_err_msg;
               END;
            ELSIF l_rec_cnt = 1
            THEN
               l_err_msg :=
                  'CREATE transaction is invalid for this record as the relationship already exists.';
               RETURN l_err_msg;
            ELSIF l_rec_cnt > 1
            THEN
               l_err_msg :=
                  'Multiple records exists with similar relationship between these SKUs. It is a data issue in';
               RETURN l_err_msg;
            END IF;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_err_msg :=
                     'Error during relationship CREATE operation. Error: '
                  || SQLERRM;
               RETURN l_err_msg;
         END;
      ELSIF pv_transaction_type = 'UPDATE'
      THEN
         x_rowid := NULL;

         BEGIN
            SELECT mtri.ROWID, mtri.start_date, mtri.end_date,
                   NVL (pv_reciprocal_flag, mtri.reciprocal_flag),
                   NVL (pv_planning_enabled_flag, mtri.planning_enabled_flag)
              INTO x_rowid, l_start_date, l_end_date,
                   l_reciprocal_flag,
                   l_planning_enabled_flag
              FROM mtl_system_items_b msia,
                   mtl_system_items_b msib,
                   mtl_related_items mtri
             WHERE msia.segment1 = TRIM (pv_item_number)
               AND msib.segment1 = TRIM (pv_related_item_number)
               AND mtri.inventory_item_id = msia.inventory_item_id
               AND mtri.related_item_id = msib.inventory_item_id
               AND msia.organization_id = msib.organization_id
               AND msia.organization_id = l_org_id
               AND mtri.relationship_type_id = l_relationship_type_id;

            IF x_rowid IS NOT NULL
            THEN
               IF     TRIM (pd_start_date) IS NOT NULL
                  AND TRIM (pd_end_date) IS NOT NULL
               THEN
                  IF TRIM (pd_start_date) > TRIM (pd_end_date)
                  THEN
                     l_err_msg :=
                               'Start date can not be greater than end date.';
                     RETURN l_err_msg;
                  END IF;
               END IF;

               BEGIN
                  SELECT inventory_item_id
                    INTO l_inv_item_id
                    FROM mtl_system_items_b
                   WHERE segment1 = TRIM (pv_item_number)
                     AND organization_id = l_org_id;

                  SELECT inventory_item_id
                    INTO l_related_item_id
                    FROM mtl_system_items_b
                   WHERE segment1 = TRIM (pv_related_item_number)
                     AND organization_id = l_org_id;

                  IF pd_start_date = TO_DATE ('31-DEC-9999', 'DD-MON-YYYY')
                  THEN
                     l_start_date := NULL;
                  ELSIF TRIM (pd_start_date) IS NOT NULL
                  THEN
                     l_start_date := pd_start_date;
                  END IF;

                  IF pd_end_date = TO_DATE ('31-DEC-9999', 'DD-MON-YYYY')
                  THEN
                     l_end_date := NULL;
                  ELSIF TRIM (pd_end_date) IS NOT NULL
                  THEN
                     l_end_date := pd_end_date;
                  END IF;

                  mtl_related_items_pkg.update_row
                         (x_rowid                      => x_rowid,
                          x_inventory_item_id          => l_inv_item_id,
                          x_organization_id            => l_org_id,
                          x_related_item_id            => l_related_item_id,
                          x_relationship_type_id       => l_relationship_type_id,
                          x_reciprocal_flag            => l_reciprocal_flag,
                          x_planning_enabled_flag      => l_planning_enabled_flag,
                          x_start_date                 => l_start_date,
                          x_end_date                   => l_end_date,
                          x_attr_context               => NULL,
                          x_attr_char1                 => NULL,
                          x_attr_char2                 => NULL,
                          x_attr_char3                 => NULL,
                          x_attr_char4                 => NULL,
                          x_attr_char5                 => NULL,
                          x_attr_char6                 => NULL,
                          x_attr_char7                 => NULL,
                          x_attr_char8                 => NULL,
                          x_attr_char9                 => NULL,
                          x_attr_char10                => NULL,
                          x_attr_num1                  => NULL,
                          x_attr_num2                  => NULL,
                          x_attr_num3                  => NULL,
                          x_attr_num4                  => NULL,
                          x_attr_num5                  => NULL,
                          x_attr_num7                  => NULL,
                          x_attr_num6                  => NULL,
                          x_attr_num8                  => NULL,
                          x_attr_num9                  => NULL,
                          x_attr_num10                 => NULL,
                          x_attr_date1                 => NULL,
                          x_attr_date2                 => NULL,
                          x_attr_date3                 => NULL,
                          x_attr_date4                 => NULL,
                          x_attr_date5                 => NULL,
                          x_attr_date6                 => NULL,
                          x_attr_date7                 => NULL,
                          x_attr_date8                 => NULL,
                          x_attr_date9                 => NULL,
                          x_attr_date10                => NULL,
                          x_last_update_date           => SYSDATE,
                          x_last_updated_by            => fnd_global.user_id,
                          x_last_update_login          => NULL
                         );
                  COMMIT;
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     l_err_msg := 'Item doesnot exists. Error: ' || SQLERRM;
                     RETURN l_err_msg;
                  WHEN OTHERS
                  THEN
                     l_err_msg :=
                           'Error while updating relationship. Error: '
                        || SQLERRM;
                     RETURN l_err_msg;
               END;
            END IF;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_err_msg :=
                     'Error during relationship UPDATE operation. Error: '
                  || SQLERRM;
               RETURN l_err_msg;
         END;
      ELSIF pv_transaction_type = 'DELETE'
      THEN
         x_rowid := NULL;

         BEGIN
            SELECT mtri.ROWID
              INTO x_rowid
              FROM mtl_system_items_b msia,
                   mtl_system_items_b msib,
                   mtl_related_items mtri
             WHERE msia.segment1 = TRIM (pv_item_number)
               AND msib.segment1 = TRIM (pv_related_item_number)
               AND mtri.inventory_item_id = msia.inventory_item_id
               AND mtri.related_item_id = msib.inventory_item_id
               AND msia.organization_id = msib.organization_id
               AND msia.organization_id = l_org_id
               AND mtri.relationship_type_id = l_relationship_type_id;

            IF x_rowid IS NOT NULL
            THEN
               BEGIN
                  mtl_related_items_pkg.delete_row (x_rowid => x_rowid);
                  COMMIT;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     l_err_msg :=
                           'Error while deleting relationship. Error: '
                        || SQLERRM;
                     RETURN l_err_msg;
               END;
            END IF;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               l_err_msg :=
                            'Record you are trying to delete does not exist.';
               RETURN l_err_msg;
            WHEN OTHERS
            THEN
               l_err_msg :=
                     'Error during relationship DELETE operation. Error: '
                  || SQLERRM;
               RETURN l_err_msg;
         END;
      END IF;

      RETURN NULL;
   EXCEPTION
      WHEN OTHERS
      THEN
         l_err_msg := 'Encountered unexpected error while processing related items. Please contact System Administrator. Error Message: '||SQLERRM;
         RETURN l_err_msg;
   END;
END xxwc_ego_rel_items_process_pkg;
/