create or replace package body XXWC_AR_FREIGHT_TERMS is
  /*************************************************************************
  *   $Header XXWC_AR_FREIGHT_TERMS $
  *   Module Name: XXWC_AR_FREIGHT_TERMS
  *
  *   PURPOSE:   This package will put the Account
  *   and Primary BT site flags in sync so that the JOTF's will be flagged correctly.
  *
  *   REVISIONS:
  *   Ver        Date        Author                     Description
  *   ---------  ----------  ---------------         -------------------------
  *   1.0        07/31/2015  Nancy Pahwa            Initial Version Task ID: 20150710-00080
    * ***************************************************************************/
  procedure XXWC_AR_EXEMPT_FLAG(errbuf OUT VARCHAR2, retcode OUT NUMBER) IS
    v_object_version_number1 NUMBER := 1;
    v_object_version_number2 NUMBER := 1;
    v_cust_account_rec       hz_cust_account_v2pub.cust_account_rec_type;
    v_cust_site_use_rec      hz_cust_account_site_v2pub.cust_site_use_rec_type;
    v_running_message1       VARCHAR2(1024);
    l_msg_dummy              VARCHAR2(1000);
    v_return_status          VARCHAR2(1024);
    v_msg_count              NUMBER;
    v_msg_data               VARCHAR2(1024);
    l_sec                    VARCHAR2(200);
    l_dflt_email             fnd_user.email_address%TYPE := 'HDSOracleDevelopers@hdsupply.com';
    l_errbuf                 CLOB;
    l_running_messaget       CLOB;
    /*************************************************************************
    *   PURPOSE:   This package will put the Account
    *   and Primary BT site flags in sync so that the JOTF's will be flagged correctly.
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
    *   1.0        07/31/2015  Nancy Pahwa            Initial Version Task ID: 20150710-00080
      * ***************************************************************************/
  BEGIN
    l_running_messaget := NULL;
    l_sec              := 'Process begin';
    FOR r IN (SELECT * FROM hz_cust_accounts where freight_term = 'EXEMPT') LOOP
      l_sec := 'Get all account with exempt';
    
      FOR t in (SELECT hcsua.freight_term,
                       hcsua.primary_flag,
                       hcsua.site_use_id,
                       hcsua.cust_acct_site_id,
                       hcsua.object_version_number
                  from hz_cust_site_uses_all  hcsua,
                       hz_cust_acct_sites_all hcasa
                 where hcsua.cust_acct_site_id = hcasa.cust_acct_site_id
                   and hcasa.cust_account_id = r.cust_account_id
                   and (hcsua.freight_term is null or
                       hcsua.freight_term = 'NONEXEMPT')) LOOP
        v_cust_site_use_rec.site_use_id       := t.site_use_id;
        v_cust_site_use_rec.cust_acct_site_id := t.cust_acct_site_id;
        v_cust_site_use_rec.freight_term      := 'EXEMPT';
        v_object_version_number2              := t.object_version_number;
        begin
          MO_GLOBAL.SET_POLICY_CONTEXT('S', 162);
          l_sec := 'Calling hz_cust_account_site_v2pub API ';
          hz_cust_account_site_v2pub.update_cust_site_use(p_init_msg_list         => FND_API.G_FALSE,
                                                          p_cust_site_use_rec     => v_cust_site_use_rec,
                                                          p_object_version_number => v_object_version_number2,
                                                          x_return_status         => v_return_status,
                                                          x_msg_count             => v_msg_count,
                                                          x_msg_data              => v_msg_data);
        
        exception
          when others then
            fnd_file.put_line(fnd_file.LOG,
                              'Error from API for Site Exempt-' ||
                              t.cust_acct_site_id || v_msg_data);
        end;
        if v_return_status = 'S' then
          fnd_file.put_line(fnd_file.LOG,
                            'Success Site update' || t.site_use_id);
          COMMIT;
        else
          fnd_file.put_line(fnd_file.LOG,
                            'Failure from API for Site Exempt' ||
                            t.cust_acct_site_id || v_return_status || '-' ||
                            v_msg_data);
        end if;
      end loop;
    end loop;
  
    begin
      FOR i IN (SELECT hca.object_version_number object_version_number1,
                       hca.cust_account_id
                  FROM hz_cust_accounts hca
                 WHERE (hca.freight_term IS NULL OR
                       hca.freight_term = 'NONEXEMPT')
                   AND EXISTS
                 (SELECT '1'
                          FROM hz_cust_site_uses_all  hcsua2,
                               hz_cust_acct_sites_all hcasa2
                         WHERE hcasa2.cust_account_id = hca.cust_account_id
                           AND hcsua2.cust_acct_site_id =
                               hcasa2.cust_acct_site_id
                           AND hcsua2.site_use_code = 'BILL_TO'
                           AND hcsua2.primary_flag = 'Y'
                           AND hcsua2.freight_term = 'EXEMPT')) LOOP
        begin
        
          v_cust_account_rec.freight_term    := 'EXEMPT';
          v_cust_account_rec.cust_account_id := i.cust_account_id;
          v_object_version_number1           := i.object_version_number1;
          begin
            MO_GLOBAL.SET_POLICY_CONTEXT('S', 162);
            l_sec := 'Calling hz_cust_account_v2pub API ';
            hz_cust_account_v2pub.update_cust_account(p_init_msg_list         => FND_API.G_FALSE,
                                                      p_cust_account_rec      => v_cust_account_rec,
                                                      p_object_version_number => v_object_version_number1,
                                                      x_return_status         => v_return_status,
                                                      x_msg_count             => v_msg_count,
                                                      x_msg_data              => v_msg_data);
          exception
            when others then
              fnd_file.put_line(fnd_file.LOG,
                                'Error from API for Account Exempt-' ||
                                i.cust_account_id || v_msg_data);
            
          end;
          if v_return_status = 'S' then
            fnd_file.put_line(fnd_file.LOG, 'Success Account Exempt');
            COMMIT;
          else
            fnd_file.put_line(fnd_file.LOG,
                              'Failure from API for Account Exempt' ||
                              i.cust_account_id || v_return_status);
          
          end if;
        end;
        for l in (SELECT hcsua.freight_term,
                         hcsua.primary_flag,
                         hcsua.site_use_id,
                         hcsua.cust_acct_site_id,
                         hcsua.object_version_number object_version_number2
                    FROM hz_cust_site_uses_all  hcsua,
                         hz_cust_acct_sites_all hcasa
                   WHERE hcasa.cust_account_id = i.cust_account_id
                     and hcsua.cust_acct_site_id = hcasa.cust_acct_site_id
                     AND (hcsua.freight_term = 'NONEXEMPT' OR
                         hcsua.freight_term IS NULL)
                        
                     AND EXISTS
                   (SELECT '1'
                            FROM hz_cust_site_uses_all  hcsua2,
                                 hz_cust_acct_sites_all hcasa2
                           WHERE hcasa2.cust_account_id =
                                 hcasa.cust_account_id
                             AND hcsua2.cust_acct_site_id =
                                 hcasa2.cust_acct_site_id
                             AND hcsua2.site_use_code = 'BILL_TO'
                             AND hcsua2.primary_flag = 'Y'
                             AND hcsua2.freight_term = 'EXEMPT')) loop
        
          v_cust_site_use_rec.site_use_id       := l.site_use_id;
          v_cust_site_use_rec.cust_acct_site_id := l.cust_acct_site_id;
          v_cust_site_use_rec.freight_term      := 'EXEMPT';
          v_object_version_number2              := l.object_version_number2;
        
          begin
            MO_GLOBAL.SET_POLICY_CONTEXT('S', 162);
            l_sec := 'Calling site API for non exempt accounts with primary as exempt ';
            hz_cust_account_site_v2pub.update_cust_site_use(p_init_msg_list         => FND_API.G_FALSE,
                                                            p_cust_site_use_rec     => v_cust_site_use_rec,
                                                            p_object_version_number => v_object_version_number2,
                                                            x_return_status         => v_return_status,
                                                            x_msg_count             => v_msg_count,
                                                            x_msg_data              => v_msg_data);
          
          exception
            when others then
              fnd_file.put_line(fnd_file.LOG,
                                'Error from API for all Site Exempt-' ||
                                l.cust_acct_site_id || v_msg_data);
            
          end;
          if v_return_status = 'S' then
            fnd_file.put_line(fnd_file.LOG,
                              'Success all Site update' || l.site_use_id);
          
            COMMIT;
          else
            fnd_file.put_line(fnd_file.LOG,
                              'Failure from API for all Site Exempt ' ||
                              l.site_use_id || ' ' || v_return_status || '-' ||
                              v_msg_data);
          
          end if;
        
        end loop;
      end loop;
    end;
  
  EXCEPTION
    WHEN OTHERS THEN
      /* l_running_messaget := NULL;
      l_running_messaget := 'Error...XXWC_AR_EXEMPT_FLAG';*/
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => 'XXWC_AR_FREIGHT_TERMS.XXWC_AR_EXEMPT_FLAG',
                                           p_calling           => l_sec,
                                           p_request_id        => fnd_global.conc_request_id,
                                           p_ora_error_msg     => SUBSTR(DBMS_UTILITY.format_error_stack() ||
                                                                         DBMS_UTILITY.format_error_backtrace(),
                                                                         1,
                                                                         2000),
                                           p_error_desc        => 'OTHERS Exception',
                                           p_distribution_list => l_dflt_email,
                                           p_module            => 'AR');
  end;
end XXWC_AR_FREIGHT_TERMS;

/