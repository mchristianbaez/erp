create or replace PACKAGE BODY          XXWC_LABEL_PRINTERS_PKG AS

   /*****************************************************************************************************************************************
   *   $Header XXWC_LABEL_PRINTERS_PKG $                                                                                                    *
   *   Module Name: XXWC_LABEL_PRINTERS_PKG                                                                                                 *
   *                                                                                                                                        *
   *   PURPOSE:   This package is used by the extensions in MSCA for Label Printing                                                         *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        16-FEB-2015  Lee Spitzer               TMS Ticket 20150302-00152                                                          *
   *                                                     RF - Receiving
   *   1.1        27-JUL-2015  M Hari Prasad             TMS #  20150723-00015 To display the proper error message                          *
   *   1.2        11-FEB-2016  Lee Spitzer               TMS 20151103-00188 - RF Enhancements                                               *
   *****************************************************************************************************************************************/

    PROCEDURE DEBUG_LOG (P_MESSAGE   VARCHAR2)
  IS


       PRAGMA AUTONOMOUS_TRANSACTION;

       BEGIN

        IF g_debug = 'Y' THEN

          FND_LOG.STRING (G_LOG_LEVEL, upper(G_PACKAGE||'.'||G_CALL_FROM) ||' '|| G_CALL_POINT, P_MESSAGE);

          COMMIT;

        END IF;

  EXCEPTION

    WHEN others THEN

        g_sqlcode := SQLCODE;
        g_sqlerrm := SQLERRM;
        g_message := 'Error in generating debug log ';

            xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                 , p_calling             => g_call_point
                                                 , p_ora_error_msg       => SQLERRM
                                                 , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                 , p_distribution_list   => g_distro_list
                                                 , p_module              => g_module);


  END DEBUG_LOG;


   /*****************************************************************************************************************************************
   *   PROCEDURE GET_PRINTERS_LOV                                                                                                           *
   *                                                                                                                                        *
   *   PURPOSE:   This procedure is used to get the printers lov for receiving mobile forms                                                 *
   *                                                                                                                                        *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        16-FEB-2015  Lee Spitzer               TMS Ticket 20150302-00152                                                          *
   *                                                     RF - Receiving                                                                     *
   *****************************************************************************************************************************************/

    PROCEDURE GET_PRINTERS_LOV ( x_printer      OUT NOCOPY t_genref
                               , p_printer_name IN VARCHAR2) IS


    BEGIN

      g_call_from := 'GET_PRINTERS_LOV';
      g_call_point := 'Start';


        debug_log('p_printer_name ' || p_printer_name);


      g_call_point := 'Getting list of printers';



        --Added 5/12/2015 to prevent open cursors
        if x_printer%isOPEN then
           debug_log('closing x_printer cursor');
            close x_printer;

        end if;


      BEGIN
        OPEN x_printer FOR
          SELECT printer_name, description
          FROM   fnd_printer_tl
          WHERE  printer_name LIKE nvl(p_printer_name, printer_name)
          AND    language = g_language
          ORDER BY printer_name;
      EXCEPTION
        WHEN others THEN
          g_sqlcode := SQLCODE;
          g_sqlerrm := SQLERRM;
          g_message := 'When others getting list of printers ' || g_call_point || g_sqlcode || g_sqlerrm;
          debug_log(g_message);
          RAISE g_exception;
      END;

    EXCEPTION

      WHEN g_exception THEN

        g_sqlcode := SQLCODE;
             g_sqlerrm := SQLERRM;
             g_message := 'when others error ';
             debug_log(g_message || g_sqlcode || g_sqlerrm);

               --Added 5/12/2015 to prevent open cursors
            if x_printer%isOPEN then
               debug_log('closing x_printer cursor');
                close x_printer;

            end if;


            xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                 , p_calling             => g_call_point
                                                 , p_ora_error_msg       => SQLERRM
                                                 , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                 , p_distribution_list   => g_distro_list
                                                 , p_module              => g_module);


      WHEN others THEN

             g_sqlcode := SQLCODE;
             g_sqlerrm := SQLERRM;
             g_message := 'when others error ';
             debug_log(g_message || g_sqlcode || g_sqlerrm);

                 --Added 5/12/2015 to prevent open cursors
              if x_printer%isOPEN then
                 debug_log('closing x_printer cursor');
                  close x_printer;

              end if;

            xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                 , p_calling             => g_call_point
                                                 , p_ora_error_msg       => SQLERRM
                                                 , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                 , p_distribution_list   => g_distro_list
                                                 , p_module              => g_module);

    END GET_PRINTERS_LOV;



   /*****************************************************************************************************************************************
   *   PROCEDURE set_printers                                                                                                               *
   *                                                                                                                                        *
   *   PURPOSE:   This procedure is used to set the printers lov for receiving mobile forms                                                 *
   *                                                                                                                                        *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        16-FEB-2015  Lee Spitzer               TMS Ticket 20150302-00152                                                          *
   *                                                     RF - Receiving
   *    1.1        27-JUL-2015  M Hari Prasad             TMS #  20150723-00015 To display the proper error message                         *
   *   1.2        11-FEB-2016  Lee Spitzer               TMS#20151103-00188 - RF Enhancements                                               *
    *****************************************************************************************************************************************/

    PROCEDURE set_printers ( p_printer_name IN VARCHAR2
                           , p_profile_option_name IN VARCHAR2
                           , x_return OUT NUMBER
                           , x_message OUT VARCHAR2)
      IS

    l_printer_name VARCHAR2(30);
    l_return NUMBER DEFAULT 0;
    l_boolean BOOLEAN;
    l_profile_option_name VARCHAR2(30);

  BEGIN

  G_CALL_FROM  := 'set_printers';
  G_CALL_POINT := 'start';
    debug_log('p_printer_name ' || p_printer_name);
    debug_log('p_profile_option_name ' || p_profile_option_name);

/* TMS#20151103-00188 removed next section as we are using this API for other mobile profile options, not just printers

  G_CALL_POINT := 'validating printer name'; --Removed TMS#20151103-00188

      BEGIN --Removed TMS#20151103-00188
          select printer_name --Removed TMS#20151103-00188
          INTO   l_printer_name --Removed TMS#20151103-00188
          FROM   fnd_printer_tl --Removed TMS#20151103-00188
          WHERE  printer_name LIKE nvl(p_printer_name, printer_name) --Removed TMS#20151103-00188
          AND    language = g_language; --Removed TMS#20151103-00188
        EXCEPTION --Removed TMS#20151103-00188
        when others then --Removed TMS#20151103-00188
            g_sqlcode := SQLCODE; --Removed TMS#20151103-00188
            g_sqlerrm := substr(SQLERRM,1,1000); --Removed TMS#20151103-00188
            debug_log('WHEN OTHERS finding printer name '|| p_printer_name ||' '|| G_SQLCODE||G_SQLERRM); --Removed TMS#20151103-00188
            l_return := 1; --Removed TMS#20151103-00188
            raise g_exception; --Removed TMS#20151103-00188
      END; --Removed TMS#20151103-00188

      debug_log('l_printer_name ' || l_printer_name); --Removed TMS#20151103-00188

    G_CALL_POINT := 'validating profile option name'; --Removed TMS#20151103-00188

    BEGIN --Removed TMS#20151103-00188
      SELECT profile_option_name --Removed TMS#20151103-00188
      INTO   l_profile_option_name --Removed TMS#20151103-00188
      FROM   fnd_profile_options --Removed TMS#20151103-00188
      WHERE  profile_option_name = p_profile_option_name; --Removed TMS#20151103-00188
    exception --Removed TMS#20151103-00188
      WHEN others THEN --Removed TMS#20151103-00188
            g_sqlcode := SQLCODE; --Removed TMS#20151103-00188
            g_sqlerrm := SQLERRM; --Removed TMS#20151103-00188
            debug_log('WHEN OTHERS finding profile option name '|| p_profile_option_name ||' '|| G_SQLCODE||G_SQLERRM); --Removed TMS#20151103-00188
            raise g_exception; --Removed TMS#20151103-00188
    END; --Removed TMS#20151103-00188

*/ --Removed TMS#20151103-00188
    G_CALL_POINT := 'setting profile option name';

    l_boolean := FND_PROFILE.save(x_name  => p_profile_option_name
                                , x_value => p_printer_name
                                , x_level_name => 'USER'
                                , x_level_value => fnd_global.user_id
                                , x_level_value_app_id   => NULL);


      debug_log('l_boolean ' || to_char(sys.diutil.bool_to_int(l_boolean)));

      commit;


     debug_log('l_return ' || l_return);

      x_return := 0;
      x_message := 'Success';

    EXCEPTION

      WHEN g_exception THEN

            g_sqlcode := SQLCODE;
            g_sqlerrm := SQLERRM;
            g_message := 'Error in ' ||g_package||'.'||g_call_from || g_sqlcode || g_sqlerrm;
            debug_log(g_message || g_sqlcode || g_sqlerrm);

            xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                 , p_calling             => g_call_point
                                                 --, p_ora_error_msg     => SQLERRM --- Commented By Hari for TMS # 20150723-00015
												 , p_ora_error_msg       => g_message --- Added By Hari for TMS # 20150723-00015
                                                 , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                 , p_distribution_list   => g_distro_list
                                                 , p_module              => g_module);


             x_return := 1;
             x_message := 'Error ' || g_message;

      WHEN others THEN

            g_sqlcode := SQLCODE;
            g_sqlerrm := SQLERRM;
            g_message := 'When others in ' ||g_package||'.'||g_call_from || g_sqlcode || g_sqlerrm;
            debug_log(g_message || g_sqlcode || g_sqlerrm);

            xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                 , p_calling             => g_call_point
                                                 , p_ora_error_msg       => SQLERRM
                                                 , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                 , p_distribution_list   => g_distro_list
                                                 , p_module              => g_module);



  END set_printers;

     /*****************************************************************************************************************************************
   *   PROCEDURE get_default_printers                                                                                                       *
   *                                                                                                                                        *
   *   PURPOSE:   This function is used to get the default printer profile option                                                           *
   *                                                                                                                                        *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        16-FEB-2015  Lee Spitzer               TMS Ticket 20150302-00152                                                          *
   *                                                     RF - Receiving                                                                     *
   *****************************************************************************************************************************************/

    FUNCTION get_default_printer ( p_profile_option_name IN VARCHAR2)
      RETURN VARCHAR2 IS

    l_printer VARCHAR2(30);

    BEGIN

      G_CALL_FROM  := 'get_default_printer';
      DEBUG_LOG('p_profile_option_name ' || p_profile_option_name);

      l_printer := FND_PROFILE.VALUE(p_profile_option_name);

        /*if l_printer = '' or l_printer is null then

            l_printer := 'noprint';
        end if;
      */
      debug_log('l_printer ' || l_printer);


    RETURN l_printer;

    EXCEPTION

     WHEN others THEN

            g_sqlcode := SQLCODE;
            g_sqlerrm := SQLERRM;
            g_message := 'When others in ' ||g_package||'.'||g_call_from || g_sqlcode || g_sqlerrm;
            debug_log(g_message || g_sqlcode || g_sqlerrm);

            xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                 , p_calling             => g_call_point
                                                 , p_ora_error_msg       => SQLERRM
                                                 , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                 , p_distribution_list   => g_distro_list
                                                 , p_module              => g_module);


    END get_default_printer;

   /*****************************************************************************************************************************************
   *   PROCEDURE validate_lbl_qty_val                                                                                                       *
   *                                                                                                                                        *
   *   PURPOSE:   This procedure is used to validate the receiving qty value                                                                *
   *                                                                                                                                        *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        16-FEB-2015  Lee Spitzer               TMS Ticket 20150302-00152                                                          *
   *                                                     RF - Receiving                                                                     *
   *****************************************************************************************************************************************/

    PROCEDURE validate_lbl_qty_val  ( p_qty     IN VARCHAR2
                                    , x_return  OUT NUMBER
                                    , x_message OUT VARCHAR2
                                    )
     IS

      l_qty NUMBER;
      l_return NUMBER DEFAULT 0;
      l_remainder NUMBER;

    BEGIN


      g_call_from := 'validate_lbl_qty_val';
      g_call_point := 'Start';

       debug_log('p_qty ' || p_qty);


      g_call_point := 'Converting parameters from varchar2 to numbers';

      BEGIN
        SELECT to_number(p_qty) INTO l_qty FROM dual;
      EXCEPTION
        WHEN OTHERS THEN
          g_sqlcode := SQLCODE;
          g_sqlerrm := SQLERRM;
          g_message := 'Could not convert ' || p_qty || ' to number';
          raise g_exception;
      END;


      IF l_qty <= 0 THEN

        l_return := 1;
        g_message := 'Value must be greater than 0';

        raise g_exception;

      END IF;


      g_call_point := 'Get remainder';

      BEGIN
        SELECT remainder(l_qty, 1)
        INTO   l_remainder
        from dual;
      EXCEPTION
        WHEN others THEN
          g_sqlcode := SQLCODE;
          g_sqlerrm := SQLERRM;
          g_message := 'Could not convert remainder ' || l_qty;
          raise g_exception;
      END;


      IF l_remainder = 0 THEN


        l_return := 0;
        g_message := 'Success';


      ELSE

        l_return := 1;
        g_message := l_qty || ' is not a whole number.';

      END IF;

      debug_log('x_return ' || l_return);
      debug_log('x_message ' || g_message);


      x_return := l_return;
      x_message := g_message;

    EXCEPTION
      WHEN g_exception THEN

        x_return := 1;
        x_message := g_message;

      debug_log('x_return ' || l_return);
      debug_log('x_message ' || g_message);

      WHEN others THEN

        x_return := 1;

             g_sqlcode := SQLCODE;
             g_sqlerrm := SQLERRM;
             g_message := 'when others error ' || g_sqlcode || g_sqlerrm;
             debug_log(g_message || g_sqlcode || g_sqlerrm);

            x_return := 1;
            x_message := g_message;

            debug_log('x_return ' || l_return);
            debug_log('x_message ' || g_message);

            xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                 , p_calling             => g_call_point
                                                 , p_ora_error_msg       => SQLERRM
                                                 , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                 , p_distribution_list   => g_distro_list
                                                 , p_module              => g_module);



    END validate_lbl_qty_val;


     /*****************************************************************************************************************************************
     *  PROCEDURE GET_UBD_STATUS_LOV                                                                                                            *
     *                                                                                                                                        *
     *   PURPOSE:   This procedure used for list of values for time sensitive label list of valus on the mobile device, C or H                *
     *                                                                                                                                        *
     *    Parameters: None                                                                                                                    *
     *    Return : x_flexvalus                                                                                                                *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        16-FEB-2015  Lee Spitzer               TMS Ticket 20150302-00152                                                          *
     *                                                     RF - Receiving                                                                     *
     *****************************************************************************************************************************************/

  PROCEDURE GET_UBD_STATUS_LOV ( x_flexvalues        OUT    NOCOPY t_genref --0
                                , p_flex_value             IN     VARCHAR2
                                )
            IS

  BEGIN

      g_call_from := 'GET_UBD_STATUS_LOV';
      g_call_point := 'Start';


        IF g_debug = 'Y' THEN
              debug_log('p_flex_value ' || p_flex_value);

        END IF;

               --Added 5/12/2015 to prevent open cursors
        if x_flexvalues%isOPEN then
           debug_log('closing x_flexvalues cursor');
            close x_flexvalues;

        end if;


        OPEN x_flexvalues FOR
        SELECT   ffvv.flex_value,
                 ffvv.flex_value_meaning,
                 ffvv.description
          FROM   FND_FLEX_VALUE_SETS ffvs,
                 FND_FLEX_VALUES_VL ffvv
          WHERE  ffvs.flex_value_set_name = 'XXWC_INV_LOT_FLAG'
          AND    ffvs.flex_value_set_id = ffvv.flex_value_set_id
          AND    ffvv.enabled_flag = 'Y'
          AND    SYSDATE BETWEEN nvl(ffvv.start_date_active, SYSDATE-1) AND nvl(ffvv.end_date_active,SYSDATE + 1)
          AND    ffvv.description like upper(nvl(p_flex_value, ffvv.description))
          ;




    EXCEPTION
        WHEN others THEN
          g_sqlcode := SQLCODE;
          g_sqlerrm := SQLERRM;
          g_message := 'When others list of values for XXWC_INV_LOT_FLAG valueset ' || g_call_point || g_sqlcode || g_sqlerrm;
          debug_log(g_message);

                     --Added 5/12/2015 to prevent open cursors
        if x_flexvalues%isOPEN then
           debug_log('closing x_flexvalues cursor');
            close x_flexvalues;

        end if;



  END GET_UBD_STATUS_LOV;


     /*****************************************************************************************************************************************
     *  PROCEDURE PROCESS_UBD_CCR                                                                                                             *
     *                                                                                                                                        *
     *   PURPOSE:   This procedure used to submit the XXWC UBD Label Report from a RF Device                                                  *
     *                                                                                                                                        *
     *    Parameters: p_date    IN VARCHAR2 --Use by date                                                                                     *                                        *
     *                p_label_copies    IN VARCHAR2 --Number of Copies                                                                        *
     *                p_status_flag     IN VARCHAR2 --F or S -- parameters to determine label size                                            *
     *    Out : x_return -- 0 = Success, 1 = Warning, 2 = Error                                                                               *
     *          x_message --Message out                                                                                                       *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        16-FEB-2015  Lee Spitzer               TMS Ticket 20150302-00152                                                          *
     *                                                     RF - Receiving                                                                     *
     *****************************************************************************************************************************************/

     PROCEDURE PROCESS_UBD_CCR
                                                 ( p_date               IN  VARCHAR2
                                                 , p_label_copies       IN  VARCHAR2
                                                 , p_status_flag        IN VARCHAR2
                                                 , x_return             OUT NUMBER
                                                 , x_message            OUT VARCHAR2
                                                 )
      IS

      l_date                VARCHAR2(50); --convert to report date format;
      l_label_copies        NUMBER;
      l_status_flag         VARCHAR2(1);
      l_printer             VARCHAR2 (30);
      l_set_print_options   BOOLEAN;
      l_set_mode            BOOLEAN;
      xml_layout            BOOLEAN;
      x_request_id          NUMBER;

    BEGIN

       g_call_from := 'PROCESS_UBD_CCR';
       g_call_point := 'Start';

         debug_log('p_date ' || p_date);
         debug_log('p_label_copies ' || p_label_copies);
         debug_log('p_status_flag ' || p_status_flag);

      g_call_point := 'get date format';

      BEGIN
        SELECT to_char(to_date(p_date), 'YYYY/MM/DD HH:MI:SS')
        into   l_date
        from dual;
      EXCEPTION
        WHEN others THEN
              l_date := NULL;
              g_sqlcode := SQLCODE;
              g_sqlerrm := SQLERRM;
              g_message := 'Error convertng date format ' || g_sqlcode || g_sqlerrm;
              debug_log(g_message);
              raise g_exception;
      END;

        debug_log('l_date ' || l_date);

      g_call_point := 'get label copies';

      BEGIN
        select p_label_copies
        into   l_label_copies
        FROM   dual;
      exception
        when others then
              l_label_copies := 0;
              g_sqlcode := SQLCODE;
              g_sqlerrm := SQLERRM;
              g_message := 'Error getting label copies ' || g_sqlcode || g_sqlerrm;
              debug_log(g_message);
              raise g_exception;
      end;

        debug_log('l_label_copies ' || l_label_copies);

      IF l_label_copies < 0 THEN
        g_message := 'Number of copies has to be greater or equal than 0';
        raise g_exception;
      end if;


      g_call_point := 'get flex value';

      BEGIN
        SELECT ffvv.flex_value
        into   l_status_flag
        FROM   FND_FLEX_VALUE_SETS ffvs,
                 FND_FLEX_VALUES_VL ffvv
        WHERE  ffvs.flex_value_set_name = 'XXWC_INV_LOT_FLAG'
        AND    ffvs.flex_value_set_id = ffvv.flex_value_set_id
        AND    ffvv.enabled_flag = 'Y'
        AND    SYSDATE BETWEEN nvl(ffvv.start_date_active, SYSDATE-1) AND nvl(ffvv.end_date_active,SYSDATE + 1)
        AND    ffvv.flex_value = p_status_flag;
       exception
        WHEN others THEN
              l_status_flag := 'F';
              g_sqlcode := SQLCODE;
              g_sqlerrm := SQLERRM;
              g_message := 'Error l_status_flag ' || g_sqlcode || g_sqlerrm;
              debug_log(g_message);
              raise g_exception;
      END;

        debug_log('l_status_flag ' || l_status_flag);

      g_call_point := 'get printer value';

      BEGIN
        l_printer := fnd_profile.VALUE('XXWC_LABEL_PRINTER');
      EXCEPTION
        WHEN others THEN
             l_printer := 'noprint';
              g_sqlcode := SQLCODE;
              g_sqlerrm := SQLERRM;
              g_message := 'Error l_printer ' || g_sqlcode || g_sqlerrm;
              debug_log(g_message);
              raise g_exception;
      END;

       debug_log('l_printer ' || l_printer);

         l_set_print_options :=
            fnd_request.set_print_options (printer               => l_printer,
                                           style                 => NULL,
                                           copies                => 1, --Updated 4/8/2015 to resolve issues with label printing
                                           save_output           => TRUE,
                                           print_together        => 'N',
                                           validate_printer      => 'RESOLVE'
                                          );
         xml_layout :=
            fnd_request.add_layout ('XXWC',
                                    'XXWC_UBD_LABELS',
                                    'en',
                                    'US',
                                    'PDF'
                                   );
         x_request_id :=
            fnd_request.submit_request
                                   (application      => 'XXWC',
                                    program          => 'XXWC_UBD_LABELS',
                                    description      => 'XXWC UBD Label Report',
                                    sub_request      => FALSE,
                                    argument1        => l_date,
                                    argument2        => l_label_copies,
                                    argument3        => l_status_flag
                                   );


        COMMIT;


        x_return := 0;
        x_message := 'Success Request ' || x_request_id || ' to printer ' || l_printer;

    EXCEPTION

    when g_exception then

      x_return := 2;
      x_message := 'Error ' || g_message;

           xxcus_error_pkg.xxcus_error_main_api (  p_called_from        => g_package||'.'||g_call_from
                                                , p_calling             => g_call_point
                                                , p_ora_error_msg       => SQLERRM
                                                , p_error_desc          => 'Error in call ' || g_call_from || g_call_point
                                                , p_distribution_list   => g_distro_list
                                                , p_module              => g_module);


    WHEN others THEN
      x_return := 2;
      x_message := 'Error ' || g_message;

           xxcus_error_pkg.xxcus_error_main_api (  p_called_from        => g_package||'.'||g_call_from
                                                , p_calling             => g_call_point
                                                , p_ora_error_msg       => SQLERRM
                                                , p_error_desc          => 'Error in call ' || g_call_from || g_call_point
                                                , p_distribution_list   => g_distro_list
                                                , p_module              => g_module);


    END PROCESS_UBD_CCR;


     /*****************************************************************************************************************************************
     *  PROCEDURE GET_ITEM_LBL_STATUS_LOV                                                                                                     *
     *                                                                                                                                        *
     *   PURPOSE:   This procedure used for list of values for time sensitive label list of valus on the mobile device, C or H                *
     *                                                                                                                                        *
     *    Parameters: None                                                                                                                    *
     *    Return : x_flexvalus                                                                                                                *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        16-FEB-2015  Lee Spitzer               TMS Ticket 20150302-00152                                                          *
     *                                                     RF - Receiving                                                                     *
     *****************************************************************************************************************************************/


    PROCEDURE GET_ITEM_LBL_STATUS_LOV ( x_flexvalues             OUT    NOCOPY t_genref --0
                                      , p_flex_value             IN     VARCHAR2
                                      )
        IS

    BEGIN

      g_call_from := 'GET_ITEM_LBL_STATUS_LOV';
      g_call_point := 'Start';


        IF g_debug = 'Y' THEN
              debug_log('p_flex_value ' || p_flex_value);

        END IF;

                   --Added 5/12/2015 to prevent open cursors
        if x_flexvalues%isOPEN then
           debug_log('closing x_flexvalues cursor');
            close x_flexvalues;

        end if;


        OPEN x_flexvalues FOR
        SELECT   ffvv.flex_value,
                 ffvv.flex_value_meaning,
                 ffvv.description
          FROM   FND_FLEX_VALUE_SETS ffvs,
                 FND_FLEX_VALUES_VL ffvv
          WHERE  ffvs.flex_value_set_name = 'XXWC_INV_ITEMFLAG'
          AND    ffvs.flex_value_set_id = ffvv.flex_value_set_id
          AND    ffvv.enabled_flag = 'Y'
          AND    SYSDATE BETWEEN nvl(ffvv.start_date_active, SYSDATE-1) AND nvl(ffvv.end_date_active,SYSDATE + 1)
          AND    ffvv.description like upper(nvl(p_flex_value, ffvv.description))
          ;

    EXCEPTION
        WHEN others THEN
          g_sqlcode := SQLCODE;
          g_sqlerrm := SQLERRM;
          g_message := 'When others list of values for XXWC_INV_LOT_FLAG valueset ' || g_call_point || g_sqlcode || g_sqlerrm;
          debug_log(g_message);

                   --Added 5/12/2015 to prevent open cursors
        if x_flexvalues%isOPEN then
           debug_log('closing x_flexvalues cursor');
            close x_flexvalues;

        end if;


    END GET_ITEM_LBL_STATUS_LOV;


   /*****************************************************************************************************************************************
   *   PROCEDURE GET_YES_NO_LOV                                                                                                             *
   *                                                                                                                                        *
   *   PURPOSE:  This procedure is used to get the Yes or No List of Values                                                                 *
   *                                                                                                                                        *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        16-FEB-2015  Lee Spitzer               TMS Ticket 20150302-00152                                                          *
   *                                                     RF - Receiving                                                                     *
   *****************************************************************************************************************************************/

    PROCEDURE GET_YES_NO_LOV  ( x_default                   OUT    NOCOPY t_genref --0
                              , p_description               IN     VARCHAR2
                              ) IS

     BEGIN

            g_call_from := 'GET_YES_NO_LOV';
            g_call_point := 'Start';


                           --Added 5/12/2015 to prevent open cursors
        if x_default%isOPEN then
           debug_log('closing x_default cursor');
            close x_default;

        end if;



            OPEN x_default FOR
              SELECT  lookup_code,
                      trim(meaning),
                      trim(meaning)
              FROM   mfg_lookups
              WHERE  lookup_type = 'SYS_YES_NO'
              AND    nvl(enabled_flag,'N') = 'Y'
              AND    SYSDATE BETWEEN nvl(start_date_active, SYSDATE-1) AND nvl(end_date_active, SYSDATE+1)
              AND    meaning LIKE nvl(p_description, meaning)
              ;

    EXCEPTION

    WHEN others THEN
        close x_default;
        g_sqlcode := SQLCODE;
        g_sqlerrm := SQLERRM;
        g_message := 'Error in getting yes no type lov ';

                           --Added 5/12/2015 to prevent open cursors
        if x_default%isOPEN then
           debug_log('closing x_default cursor');
            close x_default;

        end if;



            xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                 , p_calling             => g_call_point
                                                 , p_ora_error_msg       => SQLERRM
                                                 , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                 , p_distribution_list   => g_distro_list
                                                 , p_module              => g_module);


     END GET_YES_NO_LOV;

     /*****************************************************************************************************************************************
     *  PROCEDURE PROCESS_ITEM_LABEL_CCR                                                                                                             *
     *                                                                                                                                        *
     *   PURPOSE:   This procedure used to submit the XXWC UBD Label Report from a RF Device                                                  *
     *                                                                                                                                        *
     *    Parameters: p_date    IN VARCHAR2 --Use by date                                                                                     *                                        *
     *                p_label_copies    IN VARCHAR2 --Number of Copies                                                                        *
     *                p_status_flag     IN VARCHAR2 --F or S -- parameters to determine label size                                            *
     *    Out : x_return -- 0 = Success, 1 = Warning, 2 = Error                                                                               *
     *          x_message --Message out                                                                                                       *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        16-FEB-2015  Lee Spitzer               TMS Ticket 20150302-00152                                                          *
     *                                                     RF - Receiving                                                                     *
     *****************************************************************************************************************************************/

     PROCEDURE PROCESS_ITEM_LABEL_CCR
                                                 ( p_organization_id               IN  VARCHAR2 --1
                                                 , p_inventory_item_id             IN  VARCHAR2 --2
                                                 , P_INV_ENABLE_BIN_LOC            IN  VARCHAR2 --3
                                                 , P_BIN_LOC_SELECTED              IN  VARCHAR2 --4
                                                 , P_ENABLE_BIN_LOC_RANGE          IN  VARCHAR2 --5
                                                 , P_BIN_LOC_LOW                   IN  VARCHAR2 --6
                                                 , P_BIN_LOC_HIGH                  IN  VARCHAR2 --7
                                                 , P_PRICE                         IN  VARCHAR2 --8
                                                 , P_FLAG                          IN  VARCHAR2 --9
                                                 , P_PRINT_LOCATORS                IN  VARCHAR2 --10
                                                 , P_SHELF_PRICING                 IN  VARCHAR2 --11
                                                 , P_COPIES                        IN  VARCHAR2 --12
                                                 , x_return                        OUT NUMBER
                                                 , x_message                       OUT VARCHAR2
                                                 )
          IS

      l_organization_id     NUMBER;
      l_inventory_item_id   NUMBER;
      l_copies              NUMBER;

      l_printer             VARCHAR2 (30);
      l_set_print_options   BOOLEAN;
      l_set_mode            BOOLEAN;
      xml_layout            BOOLEAN;
      x_request_id          NUMBER;

    BEGIN

       g_call_from := 'PROCESS_ITEM_LABEL_CCR';
       g_call_point := 'Start';


          debug_log('p_organization_id ' || p_organization_id);
          debug_log('p_inventory_item_id ' || p_inventory_item_id);
          debug_log('P_INV_ENABLE_BIN_LOC ' || P_INV_ENABLE_BIN_LOC);
          debug_log('P_BIN_LOC_SELECTED ' || P_BIN_LOC_SELECTED);
          debug_log('P_ENABLE_BIN_LOC_RANGE ' || P_ENABLE_BIN_LOC_RANGE);
          debug_log('P_BIN_LOC_LOW ' || P_BIN_LOC_LOW);
          debug_log('P_BIN_LOC_HIGH ' ||P_BIN_LOC_HIGH);
          debug_log('P_PRICE ' || P_PRICE);
          debug_log('P_FLAG ' || P_FLAG);
          debug_log('P_PRINT_LOCATORS ' || P_PRINT_LOCATORS);
          debug_log('P_SHELF_PRICING ' || P_SHELF_PRICING);
          debug_log('P_COPIES ' || P_COPIES);


      g_call_point := 'Converting p_organization_id to number ';

      BEGIN
        SELECT to_number(p_organization_id)
        INTO   l_organization_id
        from   dual;
      exception
        when others then
              g_sqlcode := SQLCODE;
              g_sqlerrm := SQLERRM;
              g_message := 'Error converting organization id to number ' || g_sqlcode || g_sqlerrm;
              debug_log(g_message);
              raise g_exception;
      end;


      g_call_point := 'Converting p_inventory_item_id to number ';

      BEGIN
        SELECT to_number(p_inventory_item_id)
        INTO   l_inventory_item_id
        from   dual;
      exception
        when others then
              g_sqlcode := SQLCODE;
              g_sqlerrm := SQLERRM;
              g_message := 'Error converting inventory_item_id to number ' || g_sqlcode || g_sqlerrm;
              debug_log(g_message);
              raise g_exception;
      END;

      g_call_point := 'get printer value';

      BEGIN
        l_printer := fnd_profile.VALUE('XXWC_LABEL_PRINTER');
      EXCEPTION
        WHEN others THEN
             l_printer := 'noprint';
              g_sqlcode := SQLCODE;
              g_sqlerrm := SQLERRM;
              g_message := 'Error l_printer ' || g_sqlcode || g_sqlerrm;
              debug_log(g_message);
              raise g_exception;
      END;

      debug_log('l_printer ' || l_printer);

      g_call_point := 'get label copies';

      BEGIN
        select to_number(p_copies)
        into   l_copies
        FROM   dual;
      exception
        when others then
              l_copies := 0;
              g_sqlcode := SQLCODE;
              g_sqlerrm := SQLERRM;
              g_message := 'Error getting copies ' || g_sqlcode || g_sqlerrm;
              debug_log(g_message);
              raise g_exception;
      end;

        debug_log('l_copies ' || l_copies);

      IF l_copies < 0 THEN
        g_message := 'Number of copies has to be greater or equal than 0';
        raise g_exception;
      END IF;


         l_set_print_options :=
            fnd_request.set_print_options (printer               => l_printer,
                                           style                 => NULL,
                                           copies                => l_copies,
                                           save_output           => TRUE,
                                           print_together        => 'N',
                                           validate_printer      => 'RESOLVE'
                                          );
         xml_layout :=
            fnd_request.add_layout ('XXWC',
                                    'XXWC_INV_ITEMLABELS',
                                    'en',
                                    'US',
                                    'PDF'
                                   );
         x_request_id :=
            fnd_request.submit_request
                                   (application      => 'XXWC',
                                    PROGRAM          => 'XXWC_INV_ITEMLABELS',
                                    description      => 'XXWC Inventory Item Labels Report',
                                    sub_request      => FALSE,
                                    argument1        => l_organization_id,
                                    argument2        => l_inventory_item_id,
                                    argument3        => P_INV_ENABLE_BIN_LOC,
                                    argument4        => P_BIN_LOC_SELECTED,
                                    argument5        => P_ENABLE_BIN_LOC_RANGE,
                                    argument6        => P_BIN_LOC_LOW,
                                    argument7        => P_BIN_LOC_HIGH,
                                    argument8        => P_PRICE,
                                    argument9        => P_FLAG,
                                    argument10       => P_PRINT_LOCATORS,
                                    argument11       => P_SHELF_PRICING
                                   );
        COMMIT;


        x_return := 0;
        x_message := 'Success Request ' || x_request_id || ' to printer ' || l_printer;

    EXCEPTION

    when g_exception then

      x_return := 2;
      x_message := 'Error ' || g_message;

           xxcus_error_pkg.xxcus_error_main_api (  p_called_from        => g_package||'.'||g_call_from
                                                , p_calling             => g_call_point
                                                , p_ora_error_msg       => SQLERRM
                                                , p_error_desc          => 'Error in call ' || g_call_from || g_call_point
                                                , p_distribution_list   => g_distro_list
                                                , p_module              => g_module);


    WHEN others THEN
      x_return := 2;
      x_message := 'Error ' || g_message;

           xxcus_error_pkg.xxcus_error_main_api (  p_called_from        => g_package||'.'||g_call_from
                                                , p_calling             => g_call_point
                                                , p_ora_error_msg       => SQLERRM
                                                , p_error_desc          => 'Error in call ' || g_call_from || g_call_point
                                                , p_distribution_list   => g_distro_list
                                                , p_module              => g_module);




      END PROCESS_ITEM_LABEL_CCR;


     /*****************************************************************************************************************************************
     *  PROCEDURE VAL_UBD_DATE                                                                                                                *
     *                                                                                                                                        *
     *   PURPOSE:   This procedure used to validate the UBD date format and return a message                                                  *
     *                                                                                                                                        *
     *    Parameters: p_date    IN VARCHAR2 --Use by date                                                                                     *
     *                p_label_copies    IN VARCHAR2 --Number of Copies                                                                        *
     *                p_status_flag     IN VARCHAR2 --F or S -- parameters to determine label size                                            *
     *    Out : x_return -- 0 = Success, 1 = Warning, 2 = Error                                                                               *
     *          x_message --Message out                                                                                                       *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        16-FEB-2015  Lee Spitzer               TMS Ticket 20150302-00152                                                          *
     *                                                     RF - Receiving                                                                     *
     *****************************************************************************************************************************************/

     PROCEDURE VAL_UBD_DATE
                                                 ( p_date               IN  VARCHAR2
                                                 , x_return             OUT NUMBER
                                                 , x_message            OUT VARCHAR2
                                                 , x_date               OUT VARCHAR2
                                                 ) IS

        l_date DATE;
        l_date_char VARCHAR2(30);

    BEGIN

       g_call_from := 'VAL_UBD_DATE';
       g_call_point := 'Start';


          debug_log('p_date ' || p_date);


      g_call_from := 'Converting p_date to date';

      BEGIN
        SELECT to_date(upper(p_date),'DD-MON-YYYY')
        INTO l_date
        FROM dual;
      EXCEPTION
        WHEN others THEN
            BEGIN
              SELECT to_date(upper(p_date),'DD-MON-YY')
              INTO l_date
              FROM dual;
             EXCEPTION
               WHEN others THEN
                    g_sqlcode := SQLCODE;
                    g_sqlerrm := SQLERRM;
                    raise g_exception;
              END;
      END;

      BEGIN
        SELECT to_char(l_date,'DD-MON-YYYY')
        INTO l_date_char
        FROM dual;
      EXCEPTION
        WHEN others THEN
            g_sqlcode := SQLCODE;
            g_sqlerrm := SQLERRM;
            raise g_exception;

      END;

         debug_log('l_date ' || l_date);
         debug_log('l_date_char ' || l_date_char);

      x_return := 0;
      x_message := p_date || ' is a valid date';
      x_date := l_date_char;

      debug_log('x_return ' || x_return);
      debug_log('x_message ' || x_message);
      debug_log('x_date ' || x_date);



    EXCEPTION

      WHEN g_exception THEN

            debug_log('Hitting date g_exception ' || g_sqlcode || g_sqlerrm);
            x_return := 1;
            x_message := p_date || ' is not a valid date or date format.  Must be a valid date in the format of DD-MON-YYYY, ie current date is ' || to_char(SYSDATE,'DD-MON-YYYY') ||'.';
            x_date := '';

            debug_log('x_return ' || x_return);
            debug_log('x_message ' || x_message);
            debug_log('x_date ' || x_date);

      WHEN others THEN

            g_sqlcode := SQLCODE;
            g_sqlerrm := SQLERRM;
            g_message := 'Error validating date. '  ||  g_sqlcode || g_sqlerrm;

            x_return :=1;
            x_message := g_message;
            x_date := '';

            debug_log('x_return ' || x_return);
            debug_log('x_message ' || x_message);
            debug_log('x_date ' || x_date);

            xxcus_error_pkg.xxcus_error_main_api (  p_called_from        => g_package||'.'||g_call_from
                                                  , p_calling             => g_call_point
                                                  , p_ora_error_msg       => SQLERRM
                                                  , p_error_desc          => 'Error in call ' || g_call_from || g_call_point
                                                  , p_distribution_list   => g_distro_list
                                                  , p_module              => g_module);

    END VAL_UBD_DATE;



   /*****************************************************************************************************************************************
   *   PROCEDURE GET_WC_BIN_LOC                                                                                                             *
   *                                                                                                                                        *
   *   PURPOSE:   This procedure is used to get the branch bin assignments for an item organization combination                             *
   *                                                                                                                                        *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        16-FEB-2015  Lee Spitzer               TMS Ticket 20150302-00152                                                          *
   *                                                     RF - Receiving                                                                     *
   *   1.1        02-MAR-2016  Lee Spitzer               TMS Ticket  20160309-00091 Mobile Stocking                                          *
   *****************************************************************************************************************************************/

    PROCEDURE GET_BIN_LOC           ( x_locators               OUT    NOCOPY t_genref --0
                                    , p_organization_id        IN     NUMBER    --1
                                    , p_subinventory_code      IN     VARCHAR2  --2
                                    , p_restrict_locators_code IN     NUMBER    --3
                                    , p_inventory_item_id      IN     VARCHAR2    --4
                                    , p_concatenated_segments  IN     VARCHAR2  --5
                                    , p_project_id             IN     NUMBER    --6
                                    , p_task_id                IN     NUMBER    --7
                                    , p_hide_prefix            IN     VARCHAR2
                                    --, p_alias                  IN     VARCHAR2
                                    ) IS

     l_concatenated_segments VARCHAR2(40);--Added 3/2/2016 TMS 20160309-00091


    BEGIN

      g_call_from := 'GET_WC_BIN_LOC';
      g_call_point := 'Start';


        debug_log('p_organization_id ' || p_organization_id);
        debug_log('p_subinventory_code ' || p_subinventory_code);
        debug_log('p_restrict_locators_code  ' || p_restrict_locators_code);
        debug_log('p_inventory_item_id ' || p_inventory_item_id);
        debug_log('p_concatenated_segments ' || p_concatenated_segments);
        debug_log('p_project_id ' || p_project_id);
        debug_log('p_task_id ' || p_task_id);
        debug_log('p_hide_prefix ' || p_hide_prefix);
        --debug_log('p_alias ' || p_alias);


                                   --Added 5/12/2015 to prevent open cursors
        if x_locators%isOPEN then
           debug_log('closing x_locators cursor');
            close x_locators;

        end if;


       BEGIN --Added 3/2/2016 TMS 20160309-00091
          SELECT upper(p_concatenated_segments)  --Added 3/2/2016 TMS 20160309-00091
          INTO   l_concatenated_segments  --Added 3/2/2016 TMS 20160309-00091
          FROM   dual;  --Added 3/2/2016 TMS 20160309-00091
        EXCEPTION  --Added 3/2/2016 TMS 20160309-00091
          WHEN others THEN  --Added 3/2/2016 TMS 20160309-00091
            g_sqlcode := SQLCODE;  --Added 3/2/2016 TMS 20160309-00091
            g_sqlerrm := SQLERRM;  --Added 3/2/2016 TMS 20160309-00091
            g_message := 'Could not upper ' || p_concatenated_segments;  --Added 3/2/2016 TMS 20160309-00091
            raise g_exception; --Added 3/2/2016 TMS 20160309-00091
        END; --Added 3/2/2016 TMS 20160309-00091
        
        
        debug_log('l_concatenated_segments ' || l_concatenated_segments); --Added 3/2/2016 TMS 20160309-00091



        if p_hide_prefix = 'N' THEN

         IF p_restrict_locators_code is null or p_restrict_locators_code = 2 THEN

              --Show Prefix and Locator Restrict is off
              OPEN x_locators FOR
              --Primary Bin Query
              SELECT   loc.wc_locator
                     , loc.wc_pre_fix
                     , loc.inventory_location_id
                     , loc.concatenated_segments
                     , loc.description
                     , loc.subinventory_code
              FROM  (SELECT --  WC_LOCATOR(wil.locator_segments) wc_locator
                            --, WC_LOCATOR_PREFIX(wil.locator_segments) wc_pre_fix
                              substr(SEGMENT1,instr(SEGMENT1,'-',1,1)+1, LENGTH(SEGMENT1) -  instr(SEGMENT1,'-',1,1)) wc_locator
                            , substr(segment1, 1,instr(segment1,'-',1,1)-1) wc_pre_fix
                            , wil.inventory_location_id
                            , wil.locator_segments concatenated_segments
                            , wil.description
                            , wil.subinventory_code
                     FROM wms_item_locations_kfv wil
                     WHERE wil.organization_id = p_organization_id
                     AND NVL(wil.disable_date, TRUNC(SYSDATE + 1)) > TRUNC(SYSDATE)
                     AND wil.subinventory_code = nvl(p_subinventory_code, wil.subinventory_code)
                     AND wil.PROJECT_ID IS NULL
                     AND wil.task_id IS NULL
                     --AND wil.segment1 like nvl(p_concatenated_segments,wil.segment1)) loc -- Removed 3/2/2016 TMS 20160309-00091
                     AND wil.segment1 like nvl(l_concatenated_segments,wil.segment1)) loc --Added 3/2/2016 TMS 20160309-00091
                  order by loc.wc_pre_fix, loc.wc_locator, loc.concatenated_segments;

         ELSIF p_restrict_locators_code = 3 THEN

              --Show Prefix and Locator Restrict is off
              OPEN x_locators FOR
              --Primary Bin Query
              SELECT   loc.wc_locator
                     , loc.wc_pre_fix
                     , loc.inventory_location_id
                     , loc.concatenated_segments
                     , loc.description
                     , loc.subinventory_code
              FROM  (SELECT --  WC_LOCATOR(wil.locator_segments) wc_locator
                            --, WC_LOCATOR_PREFIX(wil.locator_segments) wc_pre_fix
                              substr(SEGMENT1,instr(SEGMENT1,'-',1,1)+1, LENGTH(SEGMENT1) -  instr(SEGMENT1,'-',1,1)) wc_locator
                            , substr(segment1, 1,instr(segment1,'-',1,1)-1) wc_pre_fix
                            , wil.inventory_location_id
                            , wil.locator_segments concatenated_segments
                            , wil.description
                            , wil.subinventory_code
                     FROM wms_item_locations_kfv wil
                     WHERE wil.organization_id = p_organization_id
                     AND NVL(wil.disable_date, TRUNC(SYSDATE + 1)) > TRUNC(SYSDATE)
                     AND wil.subinventory_code = nvl(p_subinventory_code, wil.subinventory_code)
                     AND wil.PROJECT_ID IS NULL
                     AND wil.task_id IS NULL
                     AND nvl(substr(wil.segment1, 1,instr(wil.segment1,'-',1,1)-1),0) > nvl((SELECT max(substr(wil2.segment1, 1,instr(wil2.segment1,'-',1,1)-1))
                                                                                       FROM   mtl_secondary_locators msl,
                                                                                              wms_item_locations_kfv wil2
                                                                                       WHERE  msl.inventory_item_id = nvl(p_inventory_item_id, msl.inventory_item_id)
                                                                                       AND    msl.organization_id = p_organization_id
                                                                                       AND    msl.subinventory_code = wil2.subinventory_code
                                                                                       AND    msl.secondary_locator = wil2.inventory_location_id),-1)
                     --AND wil.segment1 like nvl(p_concatenated_segments,wil.segment1)) loc --Removed 3/2/2016 TMS 20160309-00091
                     AND wil.segment1 like nvl(l_concatenated_segments,wil.segment1)) loc --Added 3/2/2016 TMS 20160309-00091
                  ORDER BY loc.wc_pre_fix, loc.wc_locator, loc.concatenated_segments;


          else

              --Show Prefix and Locator Restrict is on
                   OPEN x_locators FOR
              --Primary Bin Query
              SELECT   loc.wc_locator
                     , loc.wc_pre_fix
                     , loc.inventory_location_id
                     , loc.concatenated_segments
                     , loc.description
                     , loc.subinventory_code
              FROM  (SELECT --  WC_LOCATOR(wil.locator_segments) wc_locator
                            --, WC_LOCATOR_PREFIX(wil.locator_segments) wc_pre_fix
                              substr(SEGMENT1,instr(SEGMENT1,'-',1,1)+1, LENGTH(SEGMENT1) -  instr(SEGMENT1,'-',1,1)) wc_locator
                            , substr(segment1, 1,instr(segment1,'-',1,1)-1) wc_pre_fix
                            , wil.inventory_location_id
                            , wil.locator_segments concatenated_segments
                            , wil.description
                            , wil.subinventory_code
                     FROM wms_item_locations_kfv wil
                     WHERE wil.organization_id = p_organization_id
                     AND NVL(wil.disable_date, TRUNC(SYSDATE + 1)) > TRUNC(SYSDATE)
                     AND wil.subinventory_code = nvl(p_subinventory_code, wil.subinventory_code)
                     AND wil.PROJECT_ID IS NULL
                     AND wil.task_id IS NULL
                     AND EXISTS (SELECT *
                                 FROM   mtl_secondary_locators msl
                                 WHERE  msl.inventory_item_id = nvl(p_inventory_item_id, msl.inventory_item_id)
                                 AND    msl.organization_id = p_organization_id
                                 AND    msl.subinventory_code = wil.subinventory_code
                                 AND    msl.secondary_locator = wil.inventory_location_id)
                     -- AND wil.segment1 like nvl(p_concatenated_segments,wil.segment1)) loc --Removed 3/2/2016 TMS 20160309-00091 
                     AND wil.segment1 like nvl(l_concatenated_segments,wil.segment1)) loc --Added 3/2/2016 TMS 20160309-00091
                  order by loc.wc_pre_fix, loc.wc_locator, loc.concatenated_segments;

          END IF;



        ELSE
        --Hide Prefix and Locator Restrict is off

          IF p_restrict_locators_code IS NULL OR p_restrict_locators_code = 2 THEN

            OPEN x_locators FOR
              --Primary Bin Query
              SELECT DISTINCT
                         loc.wc_locator
                       , NULL wc_prefix--, loc.wc_pre_fix
                       , NULL inventory_location_id--, loc.inventory_location_id
                       , NULL concatenated_segments--, loc.concatenated_segments
                       , NULL description--, loc.description
                       , loc.subinventory_code
              FROM  (SELECT --  WC_LOCATOR(wil.locator_segments) wc_locator
                            --, WC_LOCATOR_PREFIX(wil.locator_segments) wc_pre_fix
                              substr(SEGMENT1,instr(SEGMENT1,'-',1,1)+1, LENGTH(SEGMENT1) -  instr(SEGMENT1,'-',1,1)) wc_locator
                            --, substr(segment1, 1,instr(segment1,'-',1,1)-1) wc_pre_fix
                            --, wil.inventory_location_id
                            --, wil.locator_segments concatenated_segments
                            --, wil.description
                            , wil.subinventory_code
                     FROM wms_item_locations_kfv wil
                     WHERE wil.organization_id = p_organization_id
                     AND NVL(wil.disable_date, TRUNC(SYSDATE + 1)) > TRUNC(SYSDATE)
                     AND wil.subinventory_code = nvl(p_subinventory_code, wil.subinventory_code)
                     AND wil.PROJECT_ID IS NULL
                     AND wil.task_id IS NULL
                     AND EXISTS (SELECT *
                                 FROM   mtl_secondary_locators msl
                                 WHERE  msl.inventory_item_id = nvl(p_inventory_item_id, msl.inventory_item_id)
                                 AND    msl.organization_id = p_organization_id
                                 AND    msl.subinventory_code = wil.subinventory_code
                                 AND    msl.secondary_locator = wil.inventory_location_id)
                     --AND wil.segment1 like nvl(p_concatenated_segments,wil.segment1)) loc --Removed 3/2/2016 TMS 20160309-00091
                     AND wil.segment1 like nvl(l_concatenated_segments,wil.segment1)) loc --Added 3/2/2016 TMS 20160309-00091
                  order by loc.wc_locator;



          ELSE
            --Hide Prefix and Locator Restrict is on
                         OPEN x_locators FOR
              --Primary Bin Query
              SELECT DISTINCT
                         loc.wc_locator
                       , NULL wc_prefix--, loc.wc_pre_fix
                       , NULL inventory_location_id--, loc.inventory_location_id
                       , NULL concatenated_segments--, loc.concatenated_segments
                       , NULL description--, loc.description
                       , loc.subinventory_code
              FROM  (SELECT --  WC_LOCATOR(wil.locator_segments) wc_locator
                            --, WC_LOCATOR_PREFIX(wil.locator_segments) wc_pre_fix
                              substr(SEGMENT1,instr(SEGMENT1,'-',1,1)+1, LENGTH(SEGMENT1) -  instr(SEGMENT1,'-',1,1)) wc_locator
                            --, substr(segment1, 1,instr(segment1,'-',1,1)-1) wc_pre_fix
                            --, wil.inventory_location_id
                            --, wil.locator_segments concatenated_segments
                            --, wil.description
                            , wil.subinventory_code
                     FROM wms_item_locations_kfv wil
                     WHERE wil.organization_id = p_organization_id
                     AND NVL(wil.disable_date, TRUNC(SYSDATE + 1)) > TRUNC(SYSDATE)
                     AND wil.subinventory_code = nvl(p_subinventory_code, wil.subinventory_code)
                     AND wil.PROJECT_ID IS NULL
                     AND wil.task_id IS NULL
                     --AND wil.segment1 like nvl(p_concatenated_segments,wil.segment1)) loc --Removed 3/2/2016 TMS 20160309-00091
                     AND wil.segment1 like nvl(l_concatenated_segments,wil.segment1)) loc --Added 3/2/2016 TMS 20160309-00091
                  order by loc.wc_locator;

            END IF;

    END IF;

    EXCEPTION

      WHEN others THEN
             g_sqlcode := SQLCODE;
             g_sqlerrm := SQLERRM;
             g_message := 'when others error ';
             debug_log(g_message || g_sqlcode || g_sqlerrm);

         if x_locators%isOPEN then
           debug_log('closing x_locators cursor');
            close x_locators;

        end if;



            xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                 , p_calling             => g_call_point
                                                 , p_ora_error_msg       => SQLERRM
                                                 , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                 , p_distribution_list   => g_distro_list
                                                 , p_module              => g_module);



    END  GET_BIN_LOC;



    /*****************************************************************************************************************************************
    *   PROCEDURE VAL_SHELF_AND_UBD                                                                                                          *
    *                                                                                                                                        *
    *   PURPOSE:   This procedure is used to validate shelf and UBD date                                                                     *
    *                                                                                                                                        *
    *                                                                                                                                        *
    *   REVISIONS:                                                                                                                           *
    *   Ver        Date        Author                     Description                                                                        *
    *   ---------  ----------  ---------------         -------------------------                                                             *
    *   1.0        16-FEB-2015  Lee Spitzer               TMS Ticket 20150302-00152                                                          *
    *                                                     RF - Receiving                                                                     *
    *****************************************************************************************************************************************/



        PROCEDURE VAL_SHELF_AND_UBD  ( p_shelf_life   IN VARCHAR2
                                     , p_ubd          IN VARCHAR2
                                     , x_return       OUT NUMBER
                                     , x_message      OUT VARCHAR2
                                     ) IS


          l_shelf_life NUMBER;
          l_ubd DATE;
          l_min_shelf_life NUMBER;
          l_shelf_75_percent NUMBER;
          l_message VARCHAR2(2000);
          l_return NUMBER;


        BEGIN


          g_call_from := 'VAL_SHELF_AND_UBD';
          g_call_point := 'Start';


          debug_log('p_shelf_life ' || p_shelf_life);
          debug_log('p_ubd ' || p_ubd);


          g_call_point := 'Converting shelf life to date';

          BEGIN
            SELECT to_number(p_shelf_life)
            INTO  l_shelf_life
            FROM  dual;
          EXCEPTION
            WHEN OTHERS THEN
              g_sqlcode := SQLCODE;
              g_sqlerrm := SQLERRM;
              g_message := 'Error l_shelf_life ' || g_sqlcode || g_sqlerrm;
              debug_log(g_message);
          END;

          debug_log('l_shelf_life ' || l_shelf_life);

          g_call_point := 'Converting ubd to date';

          BEGIN
            SELECT to_date(p_ubd,'DD-MON-YYYY')
            INTO  l_ubd
            FROM  dual;
          EXCEPTION
            WHEN others THEN
              g_sqlcode := SQLCODE;
              g_sqlerrm := SQLERRM;
              g_message := 'Error l_ubd ' || g_sqlcode || g_sqlerrm;
              debug_log(g_message);
          END;

          debug_log('l_ubd ' || l_ubd);

          g_call_point := 'UBD - sysdate';


          BEGIN
            SELECT l_ubd - SYSDATE
            INTO  l_min_shelf_life
            FROM  dual;
          EXCEPTION
            WHEN others THEN
              g_sqlcode := SQLCODE;
              g_sqlerrm := SQLERRM;
              g_message := 'Error l_min_shelf_life ' || g_sqlcode || g_sqlerrm;
              debug_log(g_message);
          END;

          g_call_point := 'Shelf life at 75%';


          BEGIN
            SELECT l_shelf_life * 0.75
            INTO  l_shelf_75_percent
            FROM  dual;
          EXCEPTION
            WHEN others THEN
              g_sqlcode := SQLCODE;
              g_sqlerrm := SQLERRM;
              g_message := 'Error l_shelf_75_percent ' || g_sqlcode || g_sqlerrm;
              debug_log(g_message);
          END;


          IF (l_min_shelf_life < l_shelf_75_percent) THEN

                l_return := 1;
                l_message := 'Shelf life is less than 75% of the item.';

          ELSE
                l_return := 0;
                l_message := '';

          END IF;

            x_return := l_return;
            x_message := l_message;

            debug_log('x_return ' || x_return);
            debug_log('x_message ' || x_message);

        END VAL_SHELF_AND_UBD;


    /*****************************************************************************************************************************************
    *   PROCEDURE PROCESS_EXPECTED_RECEIPTS_CCR                                                                                              *
    *                                                                                                                                        *
    *   PURPOSE:   This procedure is used to launch  the expected receipts report concurrent program                                         *
    *                                                                                                                                        *
    *                                                                                                                                        *
    *   REVISIONS:                                                                                                                           *
    *   Ver        Date        Author                     Description                                                                        *
    *   ---------  ----------  ---------------         -------------------------                                                             *
    *   1.0        16-FEB-2015  Lee Spitzer               TMS Ticket 20150302-00152                                                          *
    *                                                     RF - Receiving                                                                     *
    *   1.1        01-JUN-2015  Lee Spitzer               TMS Ticket 20150601-0247                                                           *
    *                                                     RF / Break Fix Expected Receipts Runs Wide Open                                    *
    *****************************************************************************************************************************************/


         PROCEDURE PROCESS_EXPECTED_RECEIPTS_CCR ( p_organization_id IN NUMBER
                                                 , p_po_number       IN VARCHAR2
                                                 , p_req_number      IN VARCHAR2
                                                 , p_rma_number      IN VARCHAR2
                                                 , p_vendor          IN VARCHAR2
                                                 , p_structure_id    IN NUMBER
                                                 , x_return          OUT NUMBER
                                                 , x_message         OUT VARCHAR2
                                                 )
            IS

          l_copies              NUMBER DEFAULT 1;
          l_status_flag         VARCHAR2(1);
          l_printer             VARCHAR2 (30);
          l_set_print_options   BOOLEAN;
          l_set_mode            BOOLEAN;
          xml_layout            BOOLEAN;
          x_request_id          NUMBER;
          l_user_id             NUMBER;
          l_resp_id             NUMBER;
          l_resp_appl_id        NUMBER;
        BEGIN


          g_call_from := 'PROCESS_EXPECTED_RECEIPTS_CCR';
          g_call_point := 'Start';


            debug_log('p_organization_id ' || p_organization_id);
            debug_log('p_po_number ' || p_po_number);
            debug_log('p_req_number ' || p_req_number);
            debug_log('p_rma_number ' || p_rma_number);
            debug_log('p_vendor ' || p_vendor);
            debug_log('p_structure_id ' || p_structure_id);


            /*TMS Ticket 20150601-0247 Added 01-JUN-2015 to prevent wide open expected receipts report*/
            /*If the PO, REQ, and RMA is null raise g_exception to prevent concurrent program execution*/

            if p_po_number is null AND p_req_number is null AND p_rma_number is null then

              g_message := 'PO Number, Req Number, or RMA is null.  Unable to print Expected Receipts Report.';
              raise g_exception;

            end if;

            /*If the PO, REQ, and RMA = '' raise g_exception to prevent concurrent program execution*/
            if p_po_number = '' AND p_req_number = '' AND p_rma_number = '' then

              g_message := 'PO Number, Req Number, or RMA is blank.  Unable to print Expected Receipts Report.';
              raise g_exception;

            end if;

            /*End TMS Ticket 20150601-0247 Added 01-JUN-2015*/

            MO_GLOBAL.INIT('PO'); --Required for Multi-Org or expected receipts report will error

            l_user_id := fnd_profile.VALUE('USER_ID');
            l_resp_id := fnd_profile.VALUE('RESP_ID');
            l_resp_appl_id := fnd_profile.VALUE('RESP_APPL_ID');

            debug_log('l_user_id ' || l_user_id);
            debug_log('l_resp_id ' || l_resp_id);
            debug_log('l_resp_appl_id ' || l_resp_appl_id);

            fnd_global.apps_initialize(l_user_id,l_resp_id,l_resp_appl_id); --Required for Multi-Org or expected receipts report will error

            l_printer := fnd_profile.value('PRINTER');

            if l_printer = 'noprint' then

                l_copies := 0;

            else

                l_copies := 1;

            end if;


            debug_log('l_printer ' || l_printer);
            debug_log('l_copies ' || l_copies);


            l_set_print_options :=
              fnd_request.set_print_options (printer               => l_printer,
                                             style                 => NULL,
                                             copies                => l_copies,
                                             save_output           => TRUE,
                                             print_together        => 'N',
                                             validate_printer      => 'RESOLVE'
                                            );
            xml_layout :=
                fnd_request.add_layout ('XXWC',
                                        'XXWC_POXRVXRV_PRINT',
                                        'en',
                                        'US',
                                        'PDF'
                                       );

           x_request_id :=
                fnd_request.submit_request
                                       (application      => 'XXWC',
                                        PROGRAM          => 'XXWC_POXRVXRV_PRINT',
                                        description      => 'XXWC Expected Receipts Report - Print',
                                        sub_request      => FALSE,
                                        argument1        => p_organization_id,
                                        argument2        => NULL,
                                        argument3        => NULL,
                                        argument4        => NULL,
                                        argument5        => p_po_number,
                                        argument6        => NULL,
                                        argument7        => NULL,
                                        argument8        => p_req_number,
                                        argument9        => p_vendor,
                                        argument10       => NULL,
                                        argument11       => NULL,
                                        argument12       => p_structure_id,
                                        argument13       => NULL,
                                        argument14       => NULL,
                                        argument15       => NULL,
                                        argument16       => NULL,
                                        argument17       => NULL,
                                        argument18       => 2,
                                        argument19       => p_rma_number, --RMA
                                        argument20       => p_rma_number, --RMA
                                        argument21       => NULL,
                                        argument22       => NULL
                                        );
        COMMIT;


        x_return := 0;
        x_message := 'Success Request ' || x_request_id || ' to printer ' || l_printer;

    EXCEPTION

    when g_exception then

      x_return := 2;
      x_message := 'Error ' || g_message;

           xxcus_error_pkg.xxcus_error_main_api (  p_called_from        => g_package||'.'||g_call_from
                                                , p_calling             => g_call_point
                                                , p_ora_error_msg       => SQLERRM
                                                , p_error_desc          => 'Error in call ' || g_call_from || g_call_point
                                                , p_distribution_list   => g_distro_list
                                                , p_module              => g_module);


    WHEN others THEN
      x_return := 2;
      x_message := 'Error ' || g_message;

           xxcus_error_pkg.xxcus_error_main_api (  p_called_from        => g_package||'.'||g_call_from
                                                , p_calling             => g_call_point
                                                , p_ora_error_msg       => SQLERRM
                                                , p_error_desc          => 'Error in call ' || g_call_from || g_call_point
                                                , p_distribution_list   => g_distro_list
                                                , p_module              => g_module);



        END PROCESS_EXPECTED_RECEIPTS_CCR;


    /*****************************************************************************************************************************************
    *   FUNCTION GET_WC_BIN_COUNT                                                                                                            *
    *                                                                                                                                        *
    *   PURPOSE:   This function is to get the count of locators assigned to an item                                                         *
    *                                                                                                                                        *
    *                                                                                                                                        *
    *   REVISIONS:                                                                                                                           *
    *   Ver        Date        Author                     Description                                                                        *
    *   ---------  ----------  ---------------         -------------------------                                                             *
    *   1.0        16-FEB-2015  Lee Spitzer               TMS Ticket 20150302-00152                                                          *
    *                                                     RF - Receiving                                                                     *
    *****************************************************************************************************************************************/



           FUNCTION GET_WC_BIN_COUNT         (  p_organization_id        IN     NUMBER    --1
                                              , p_subinventory_code      IN     VARCHAR2  --2
                                              , p_inventory_item_id      IN     VARCHAR2  --3
                                              )
              RETURN NUMBER
            IS

              l_count NUMBER DEFAULT 0;

            BEGIN

              g_call_from := 'GET_WC_BIN_COUNT';
              g_call_point := 'Start';


              debug_log('p_organization_id ' || p_organization_id);
              debug_log('p_subinventory_code ' || p_subinventory_code);
              debug_log('p_inventory_item_id ' || p_inventory_item_id);



                BEGIN
                  SELECT count(*)
                  into   l_count
                  FROM   mtl_secondary_locators msl
                  WHERE  msl.inventory_item_id = nvl(p_inventory_item_id, msl.inventory_item_id)
                  AND    msl.organization_id = p_organization_id
                  AND    msl.subinventory_code = nvl(p_subinventory_code, msl.subinventory_code);
                EXCEPTION
                  when others then
                        l_count := 0;
                END;


              return l_count;

            EXCEPTION

            WHEN others THEN

              return l_count;

              g_sqlcode := sqlcode;
              g_sqlerrm := sqlerrm;
              g_message := 'Error getting count ';

              debug_log(g_message);

              xxcus_error_pkg.xxcus_error_main_api (  p_called_from        => g_package||'.'||g_call_from
                                                  , p_calling             => g_call_point
                                                  , p_ora_error_msg       => SQLERRM
                                                  , p_error_desc          => 'Error in call ' || g_call_from || g_call_point
                                                  , p_distribution_list   => g_distro_list
                                                  , p_module              => g_module);


            END GET_WC_BIN_COUNT;


    /*****************************************************************************************************************************************
    *   FUNCTION GET_ITEM_LABEL_DESC                                                                                                         *
    *                                                                                                                                        *
    *   PURPOSE:   This function is to get the count of locators assigned to an item                                                         *
    *                                                                                                                                        *
    *                                                                                                                                        *
    *   REVISIONS:                                                                                                                           *
    *   Ver        Date        Author                     Description                                                                        *
    *   ---------  ----------  ---------------         -------------------------                                                             *
    *   1.0        11-FEB-2016  Lee Spitzer               TMS#20151103-00188 - RF Enhancements                                               *
    *****************************************************************************************************************************************/



    FUNCTION GET_ITEM_LABEL_DESC (p_flex_value             IN     VARCHAR2) RETURN VARCHAR2
        IS

      l_description VARCHAR2(240);

    BEGIN

      g_call_from := 'GET_ITEM_LABEL_DESC';
      g_call_point := 'Start';


        IF g_debug = 'Y' THEN
              debug_log('p_flex_value ' || p_flex_value);

        END IF;

        
      BEGIN
        SELECT   ffvv.description
        into     l_description
          FROM   FND_FLEX_VALUE_SETS ffvs,
                 FND_FLEX_VALUES_VL ffvv
          WHERE  ffvs.flex_value_set_name = 'XXWC_INV_ITEMFLAG'
          AND    ffvs.flex_value_set_id = ffvv.flex_value_set_id
          AND    ffvv.enabled_flag = 'Y'
          AND    SYSDATE BETWEEN nvl(ffvv.start_date_active, SYSDATE-1) AND nvl(ffvv.end_date_active,SYSDATE + 1)
          AND    ffvv.flex_value like upper(nvl(p_flex_value, ffvv.flex_value))
          ;

    EXCEPTION
        WHEN others THEN
          g_sqlcode := SQLCODE;
          g_sqlerrm := SQLERRM;
          g_message := 'When others list of values for XXWC_INV_ITEMFLAG valueset ' || g_call_point || g_sqlcode || g_sqlerrm;
          debug_log(g_message);
          l_description := '';
    END;
    
    return l_description;


    END GET_ITEM_LABEL_DESC;
    
    
    /*****************************************************************************************************************************************
    *   FUNCTION GET_YES_NO_MEAN                                                                                                             *
    *                                                                                                                                        *
    *   PURPOSE:   This function is to get the count of locators assigned to an item                                                         *
    *                                                                                                                                        *
    *                                                                                                                                        *
    *   REVISIONS:                                                                                                                           *
    *   Ver        Date        Author                     Description                                                                        *
    *   ---------  ----------  ---------------         -------------------------                                                             *
    *   1.0        11-FEB-2016  Lee Spitzer               TMS#20151103-00188 - RF Enhancements                                               *
    *****************************************************************************************************************************************/

    FUNCTION GET_YES_NO_MEAN (p_lookup_code               IN     VARCHAR2
                              ) RETURN VARCHAR2 IS

      l_meaning VARCHAR2(240);
      
     BEGIN

            g_call_from := 'GET_YES_NO_MEAN';
            g_call_point := 'Start';




            BEGIN
              SELECT  trim(meaning)
              into   l_meaning
              FROM   mfg_lookups
              WHERE  lookup_type = 'SYS_YES_NO'
              AND    nvl(enabled_flag,'N') = 'Y'
              AND    SYSDATE BETWEEN nvl(start_date_active, SYSDATE-1) AND nvl(end_date_active, SYSDATE+1)
              AND    lookup_code LIKE nvl(p_lookup_code, lookup_code)
              ;

          EXCEPTION
      
          WHEN others THEN
              g_sqlcode := SQLCODE;
              g_sqlerrm := SQLERRM;
              g_message := 'Error in getting yes no type lov ';
              l_meaning := '';
      
        END;
    
    
    return l_meaning;
    
    
    
    EXCEPTION 
    
     WHEN OTHERS THEN

            xxcus_error_pkg.xxcus_error_main_api ( p_called_from         => g_package||'.'||g_call_from
                                                 , p_calling             => g_call_point
                                                 , p_ora_error_msg       => SQLERRM
                                                 , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                                 , p_distribution_list   => g_distro_list
                                                 , p_module              => g_module);


     END GET_YES_NO_MEAN;




END XXWC_LABEL_PRINTERS_PKG;
/