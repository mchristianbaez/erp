CREATE OR REPLACE PACKAGE BODY APPS.XXWC_HZ_LOC_DQM_SYNC AS
   /* ************************************************************************
    HD Supply
    All rights reserved.
   **************************************************************************

     PURPOSE:Invoke party site update and DQM Sync Serial Index automagically so that end users can search the 
             customers using the location name from the customers UI, credit management and the iReceivables UI.   

     REVISIONS:
     Ticket                 Ver         Date         Author                     Description
     ---------              ----------  ----------   ------------------------   -------------------------
     TMS 20121217-00636     1.0         06/05/2015    Balaguru Seshadri          Invoke party site update and DQM Sync Serial Index automagically
     TMS 20121217-00636     1.1         06/16/2015    Balaguru Seshadri          Add exceptions and more meaningful messages.     
   ************************************************************************* */  
   --    
   Function update_party_site
                     (
                        P_Subscription_Guid In Raw
                       ,P_Event             In Out Nocopy Wf_Event_T
                     )
      Return Varchar2 Is
      --
      L_Api_Name    Constant Varchar2(30) := 'update_party_site';
      L_Api_Version Constant Number := 1.0;
      L_Rule           Varchar2(20);
      --
      L_Event_Key      Varchar2(240);
      L_Event_Name     Varchar2(50);
      --      
      L_Parameter_List Wf_Parameter_List_T := Wf_Parameter_List_T();
      L_Parameter_T    Wf_Parameter_T      := Wf_Parameter_T(Null, Null);
      L_Parameter_Name Varchar2(2000);
      --
      Param_Index      Pls_Integer;
      --
      l_party_site_id  Number :=0;
      l_site_use_id    Number :=0;
      --
      l_errbuf  Varchar2(150) :=Null;
      l_retcode Varchar2(150) :=Null;
      l_request_id Number :=0;
      --
      -- Begin TMS 20121217-00636 Ver 1.1
        l_err_msg         VARCHAR2 (2000);
        l_err_callfrom    VARCHAR2 (175) := 'XXWC_HZ_LOC_DQM_SYNC';
        l_err_callpoint   VARCHAR2 (175) := 'START';
        l_distro_list     VARCHAR2 (80) := 'hdsoracledevelopers@hdsupply.com'; --Updated to use new Distribution List
        l_module          VARCHAR2 (80) := 'WC AR';
        l_message         VARCHAR2 (1000);
        l_sqlcode         VARCHAR2 (1000);
        l_sqlerrm         VARCHAR2 (1000);      
      -- End TMS 20121217-00636 Ver 1.1
   --
   Begin   
    --           
              L_Parameter_List := P_Event.Getparameterlist();
              L_Event_Key      := P_Event.Geteventkey();
           
              If L_Parameter_List Is Not Null Then
              
                 Param_Index := L_Parameter_List.First; 
                              
                 While (Param_Index <= L_Parameter_List.Last) 
                 
                 Loop 
                    --
                    -- Logic to kickoff DQM sync and party site update.
                    -- 
                    --                                                           
                    if (L_Parameter_List(Param_Index).Getname() ='SITE_USE_ID') then                                       
                     --
                     l_site_use_id :=L_Parameter_List(Param_Index).Getvalue();
                     --
                     l_err_callpoint :='@Business event, fetch party_site_id using site_use_id '||l_site_use_id; -- Ver 1.1
                     --
                      BEGIN
                        SELECT hcasa.party_site_id
                        INTO   l_party_site_id
                        FROM   HZ_CUST_ACCT_SITES_ALL hcasa
                              ,HZ_CUST_SITE_USES_ALL  hcsua
                        WHERE  1 =1
                          AND  hcsua.site_use_id       =l_site_use_id
                          AND  hcasa.cust_acct_site_id =hcsua.cust_acct_site_id
                          AND  hcsua.org_id IN (162);
                      EXCEPTION
                        WHEN NO_DATA_FOUND THEN
                         l_party_site_id :=0;                        
                        WHEN TOO_MANY_ROWS THEN
                         l_party_site_id :=0;                        
                        WHEN OTHERS THEN
                         l_party_site_id :=0;
                         -- Begin Ver 1.1
                                l_err_msg := l_err_msg || ' ...Error_Stack...' ||
                                             dbms_utility.format_error_stack() || ' Error_Backtrace...' ||
                                             dbms_utility.format_error_backtrace();      
                                xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                                                    ,p_calling           => l_err_callpoint
                                                                    ,p_request_id        => fnd_global.conc_request_id
                                                                    ,p_ora_error_msg     => substr(' Error_Stack...' ||
                                                                                                   dbms_utility.format_error_stack() ||
                                                                                                   ' Error_Backtrace...' ||
                                                                                                   dbms_utility.format_error_backtrace()
                                                                                                  ,1
                                                                                                  ,2000)
                                                                    ,p_error_desc        => substr(l_err_msg
                                                                                                  ,1
                                                                                                  ,240)
                                                                    ,p_distribution_list => l_distro_list
                                                                    ,p_module            => l_module);
                         -- End Ver 1.1                                                            
                      END;
                     --
                     -- DO NOT issue commit anywhere in the business event as the workflow may try to rollback all changes if it finds 
                     -- something wrong with our customization
                     --
                     if l_party_site_id >0 then
                      --
                      begin -- Ver 1.1
                       l_err_callpoint :='Calling XXWC_HZ_OAF_INFO_PKG.party_site_update, l_party_site_id ='||l_party_site_id; -- Ver 1.1
                       XXWC_HZ_OAF_INFO_PKG.party_site_update
                        (
                         errbuf =>l_errbuf
                        ,retcode =>l_retcode
                        ,p_party_site_id =>l_party_site_id
                        );
                      exception -- Ver 1.1
                       when others then -- Ver 1.1
                         -- Begin Ver 1.1
                                l_err_msg := l_err_msg || ' ...Error_Stack...' ||
                                             dbms_utility.format_error_stack() || ' Error_Backtrace...' ||
                                             dbms_utility.format_error_backtrace();      
                                xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                                                    ,p_calling           => l_err_callpoint
                                                                    ,p_request_id        => fnd_global.conc_request_id
                                                                    ,p_ora_error_msg     => substr(' Error_Stack...' ||
                                                                                                   dbms_utility.format_error_stack() ||
                                                                                                   ' Error_Backtrace...' ||
                                                                                                   dbms_utility.format_error_backtrace()
                                                                                                  ,1
                                                                                                  ,2000)
                                                                    ,p_error_desc        => substr(l_err_msg
                                                                                                  ,1
                                                                                                  ,240)
                                                                    ,p_distribution_list => l_distro_list
                                                                    ,p_module            => l_module);
                         -- End Ver 1.1                        
                      end; -- Ver 1.1                      
                      --
                     end if;
                     --
                     begin -- Ver 1.1 
                       l_err_callpoint :='@Business event, Before WC DQM Sync Auto Kickoff'; -- Ver 1.1                     
                     l_request_id :=fnd_request.submit_request
                      (
                        application      =>'AR'
                       ,program          =>'ARHDQMSS'
                       ,description      =>'WC DQM Sync Auto '
                       ,start_time       =>''
                       ,sub_request      =>FALSE              
                      );
                     exception -- Ver 1.1
                       when others then -- Ver 1.1
                         -- Begin Ver 1.1
                                l_err_msg := l_err_msg || ' ...Error_Stack...' ||
                                             dbms_utility.format_error_stack() || ' Error_Backtrace...' ||
                                             dbms_utility.format_error_backtrace();      
                                xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                                                    ,p_calling           => l_err_callpoint
                                                                    ,p_request_id        => fnd_global.conc_request_id
                                                                    ,p_ora_error_msg     => substr(' Error_Stack...' ||
                                                                                                   dbms_utility.format_error_stack() ||
                                                                                                   ' Error_Backtrace...' ||
                                                                                                   dbms_utility.format_error_backtrace()
                                                                                                  ,1
                                                                                                  ,2000)
                                                                    ,p_error_desc        => substr(l_err_msg
                                                                                                  ,1
                                                                                                  ,240)
                                                                    ,p_distribution_list => l_distro_list
                                                                    ,p_module            => l_module);
                         -- End Ver 1.1                       
                     end; -- Ver 1.1
                     --
                     --
                    else 
                     --
                     Null;
                     --
                    end if;
                    --
                    Param_Index :=L_Parameter_List.Next(Param_Index);
                    --                    
                 End Loop;
                --
              End If;                           
    --
      Return 'SUCCESS';
    -- 
   Exception
      When Others Then
       WF_CORE.CONTEXT('oracle.apps.ar.hz.CustAcctSiteUse.update',
                             p_event.getEventName( ), p_subscription_guid);
       WF_EVENT.setErrorInfo(p_event, 'ERROR');      
         Return 'ERROR';
   End update_party_site;    
   --
END XXWC_HZ_LOC_DQM_SYNC;
/