create or replace PACKAGE BODY iby_fd_extract_ext_pub AS
  /* $Header: ibyfdxeb.pls 120.2 2006/09/20 18:52:12 frzhang noship $ */

  --
  -- This API is called once only for the payment instruction.
  -- Implementor should construct the extract extension elements
  -- at the payment instruction level as a SQLX XML Aggregate
  -- and return the aggregate.
  --
  -- Below is an example implementation:
  /*
    FUNCTION Get_Ins_Ext_Agg(p_payment_instruction_id IN NUMBER)
    RETURN XMLTYPE
    IS
      l_ins_ext_agg XMLTYPE;
  
      CURSOR l_ins_ext_csr (p_payment_instruction_id IN NUMBER) IS
      SELECT XMLConcat(
               XMLElement("Extend",
                 XMLElement("Name", ext_table.attr_name1),
                 XMLElement("Value", ext_table.attr_value1)),
               XMLElement("Extend",
                 XMLElement("Name", ext_table.attr_name2),
                 XMLElement("Value", ext_table.attr_value2))
             )
        FROM your_pay_instruction_lvl_table ext_table
       WHERE ext_table.payment_instruction_id = p_payment_instruction_id;
  
    BEGIN
  
      OPEN l_ins_ext_csr (p_payment_instruction_id);
      FETCH l_ins_ext_csr INTO l_ins_ext_agg;
      CLOSE l_ins_ext_csr;
  
      RETURN l_ins_ext_agg;
  
    END Get_Ins_Ext_Agg;
  */

  /********************************************************************************
  -- File Name: IBY_FD_EXTRACT_EXT_PUB.GET_INS_EXT_AGG
  --
  -- PROGRAM TYPE: PL/SQL Script   <API>
  --
  -- PURPOSE: The RTF template was not able to apply the correct format of using the  
  --          10 digits from the right side
  -- HISTORY
  -- ===========================================================================
  -- ===========================================================================
  -- VERSION DATE          AUTHOR(S)       DESCRIPTION
  -- ------- -----------   --------------- -------------------------------------
  -- 1.0     20-May-2014   Kathy Poling    Incident 498892 Canada ACH for BOA fix 
  --                                       issue with eText template in formating
  --                                       the file trailer account number hash
  --                                       total
  *******************************************************************************/

  FUNCTION get_ins_ext_agg(p_payment_instruction_id IN NUMBER) RETURN xmltype IS
  
    l_ins_ext_agg     xmltype;
    l_err_callfrom    VARCHAR2(75) DEFAULT 'iby_fd_extract_ext_pub.get_ins_ext_agg';
    l_distro_list     VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
    l_err_msg         CLOB;
    l_sec             VARCHAR2(110);
    l_checksum        NUMBER;
    l_payment_profile NUMBER;
    l_lookup          VARCHAR2(30);
  
  BEGIN
  
    BEGIN
      l_sec := 'getting payment profile for Payment_instruction_id: ' ||
               p_payment_instruction_id;
      SELECT payment_profile_id
        INTO l_payment_profile
        FROM iby_pay_instructions_all
       WHERE payment_instruction_id = p_payment_instruction_id;
    EXCEPTION
      WHEN no_data_found THEN
        RETURN NULL;
        fnd_file.put_line(fnd_file.log, l_sec);
    END;
  
    l_sec := 'Payment_instruction_id: ' || p_payment_instruction_id ||
             ' payment_profile_id: ' || l_payment_profile;

    BEGIN
    
      SELECT description
        INTO l_lookup
        FROM fnd_lookup_values
       WHERE 1 = 1
         AND lookup_type = 'XXCUS_IBY_FD_EXTRACT_EXT'
         AND enabled_flag = 'Y'
         AND SYSDATE BETWEEN start_date_active AND
             nvl(end_date_active, SYSDATE + 1)
         AND lookup_code LIKE 'PMT_DATE_%'
         AND description = l_payment_profile;
    EXCEPTION
      WHEN no_data_found THEN
        l_lookup := -1;
        fnd_file.put_line(fnd_file.log, l_sec);
    END;
  
    IF l_payment_profile != -1 --HDS_CAN_FRT_ACH_PPP payment profile name
    THEN
      /* Notes: Sum of P41.5 elements if P41 Records are present, zero fill if P41.5 records are 
      not present.
      Add 15 leftmost digits (substitute a '1' for any non-numeric character).  
      Truncate sum to 10 rightmost digits only.
      */
      SELECT nvl(substr(SUM(to_number(substr(translate(upper(ieba.bank_account_num)
                                                      ,'ABCDEFGHIJKLMNOPQRSTUVWXYZ-'
                                                      ,'111111111111111111111111111')
                                            ,1
                                            ,15)))
                       ,-10)
                ,SUM(to_number(substr(translate(upper(ieba.bank_account_num)
                                               ,'ABCDEFGHIJKLMNOPQRSTUVWXYZ-'
                                               ,'111111111111111111111111111')
                                     ,1
                                     ,15))))
        INTO l_checksum
        FROM iby_payments_all ipa, iby_ext_bank_accounts ieba
       WHERE payment_instruction_id = p_payment_instruction_id
         AND ipa.external_bank_account_id = ieba.ext_bank_account_id;
    
      SELECT xmlconcat(xmlelement("XXCUSchecksum", l_checksum))
        INTO l_ins_ext_agg
        FROM dual;
    
      RETURN l_ins_ext_agg;
    ELSE
      RETURN NULL;
    END IF;
  
  EXCEPTION
    WHEN no_data_found THEN
      RETURN NULL;
    WHEN too_many_rows THEN
      RETURN NULL;
    WHEN OTHERS THEN
      RETURN NULL;
    
      l_err_msg := l_sec || ' ...Error_Stack...' ||
                   dbms_utility.format_error_stack() ||
                   ' Error_Backtrace...' ||
                   dbms_utility.format_error_backtrace();
      fnd_file.put_line(fnd_file.log, l_err_msg);
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'IBY');
  END get_ins_ext_agg;

  --
  -- This API is called once per payment.
  -- Implementor should construct the extract extension elements
  -- at the payment level as a SQLX XML Aggregate
  -- and return the aggregate.
  --
  /********************************************************************************
  -- File Name: IBY_FD_EXTRACT_EXT_PUB.GET_PMT_EXT_AGG
  --
  -- PROGRAM TYPE: PL/SQL Script   <API>
  --
  -- PURPOSE: The RTF template was not able to apply the correct format of using the  
  --          payment date at the end of the month
  -- HISTORY
  -- ===========================================================================
  -- ===========================================================================
  -- VERSION DATE          AUTHOR(S)       DESCRIPTION
  -- ------- -----------   --------------- -------------------------------------
  -- 1.0     20-May-2014   Kathy Poling    ESMS 259494 Canada payment for BOA fix 
  --                                       issue with eText template in formating
  --                                       the payment date
  --                                       
  *******************************************************************************/
  FUNCTION get_pmt_ext_agg(p_payment_id IN NUMBER) RETURN xmltype IS
  
    l_pmt_ext_agg     xmltype;
    l_err_callfrom    VARCHAR2(75) DEFAULT 'iby_fd_extract_ext_pub.get_pmt_ext_agg';
    l_distro_list     VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
    l_err_msg         CLOB;
    l_sec             VARCHAR2(110);
    l_check_date      DATE;
    l_payment_profile NUMBER;
    l_lookup          VARCHAR2(30);
  BEGIN
  
    BEGIN
      l_sec := 'getting payment profile for Payment_id: ' || p_payment_id;
      SELECT payment_profile_id
        INTO l_payment_profile
        FROM iby.iby_payments_all
       WHERE payment_id = p_payment_id;
    EXCEPTION
      WHEN no_data_found THEN
        RETURN NULL;
        fnd_file.put_line(fnd_file.log, l_sec);
    END;
  
    l_sec := 'Check lookup for payment_profile_id Payment_id: ' ||
             p_payment_id || ' payment_profile_id: ' || l_payment_profile;
  
    BEGIN
    
      SELECT description
        INTO l_lookup
        FROM fnd_lookup_values
       WHERE 1 = 1
         AND lookup_type = 'XXCUS_IBY_FD_EXTRACT_EXT'
         AND enabled_flag = 'Y'
         AND SYSDATE BETWEEN start_date_active AND
             nvl(end_date_active, SYSDATE + 1)
         AND lookup_code LIKE 'PMT_DATE_%'
         AND description = l_payment_profile;
    EXCEPTION
      WHEN no_data_found THEN
        l_lookup := -1;
        fnd_file.put_line(fnd_file.log, l_sec);
    END;
  
    IF l_payment_profile != -1 --HDS_CAN_FRT_ACH_PPP payment profile name
    THEN
    
      SELECT ipa.payment_date + 1
        INTO l_check_date
        FROM iby.iby_payments_all ipa
       WHERE payment_id = p_payment_id;
       
           
          SELECT xmlconcat(xmlelement("XXCUScheckdate", l_check_date))
        INTO l_pmt_ext_agg
        FROM dual;
    
      RETURN l_pmt_ext_agg;
    ELSE
      RETURN NULL;
    END IF;
    
  EXCEPTION
    WHEN no_data_found THEN
      RETURN NULL;
    WHEN too_many_rows THEN
      RETURN NULL;
    WHEN OTHERS THEN
      RETURN NULL;
    
      l_err_msg := l_sec || ' ...Error_Stack...' ||
                   dbms_utility.format_error_stack() ||
                   ' Error_Backtrace...' ||
                   dbms_utility.format_error_backtrace();
      fnd_file.put_line(fnd_file.log, l_err_msg);
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'IBY');
  END get_pmt_ext_agg;

  --
  -- This API is called once per document payable.
  -- Implementor should construct the extract extension elements
  -- at the document level as a SQLX XML Aggregate
  -- and return the aggregate.
/********************************************************************************
  -- File Name: IBY_FD_EXTRACT_EXT_PUB.GET_DOC_EXT_AGG
  --
  -- PROGRAM TYPE: PL/SQL Script   <API>
  --
  -- PURPOSE: This to print the remittance messages in the check

  -- HISTORY
  -- ===========================================================================
  -- ===========================================================================
  -- VERSION DATE          AUTHOR(S)              DESCRIPTION
  -- ------- -----------   --------------- -------------------------------------
  -- 1.0     17-Jun-2015   Maharajan Shunmugam    TMS# 20150318-00123 AP - Invoice/Credit Memo number 
  --						  from AR should should appear on the refund check to the customer
  -- 2.0     05-Aug-2015   Maharajan Shunmugam    TMS#20150805-00075 AP Check format correction in SVN
  *******************************************************************************/
   FUNCTION get_doc_ext_agg(p_document_payable_id IN NUMBER) RETURN xmltype IS 
    l_doc_ext_agg 	xmltype;
    l_remit_message   VARCHAR2(2000);
    l_err_callfrom    VARCHAR2(75) DEFAULT 'iby_fd_extract_ext_pub.get_doc_ext_agg';
    l_distro_list     VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
    l_err_msg         CLOB;
    l_sec             VARCHAR2(110);
  BEGIN


   SELECT aps.Remittance_message1||aps.remittance_message2||aps.remittance_message3
     INTO l_remit_message
   FROM ap_payment_schedules_all aps,
        iby_docs_payable_all dp
   WHERE aps.invoice_id = dp.calling_app_doc_unique_ref2 
     AND dp.document_payable_id = P_document_payable_id;
      
 --SELECT xmlconcat(xmlelement("XXCUSREMIT", l_remit_message))  --commented and added for ver#2.0
 SELECT xmlconcat(xmlelement("REMIT", l_remit_message))
        INTO l_doc_ext_agg
        FROM dual;


    RETURN l_doc_ext_agg ;

 EXCEPTION
    WHEN no_data_found THEN
      RETURN NULL;
    WHEN too_many_rows THEN
      RETURN NULL;
    WHEN OTHERS THEN
      RETURN NULL;
      
      l_err_msg := l_sec || ' ...Error_Stack...' ||
                   dbms_utility.format_error_stack() ||
                   ' Error_Backtrace...' ||
                   dbms_utility.format_error_backtrace();
      fnd_file.put_line(fnd_file.log, l_err_msg);
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom 
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'IBY');

  END get_doc_ext_agg;

  --
  -- This API is called once per document payable line.
  -- Implementor should construct the extract extension elements
  -- at the doc line level as a SQLX XML Aggregate
  -- and return the aggregate.
  --
  -- Parameters:
  --   p_document_payable_id: primary key of IBY iby_docs_payable_all table
  --   p_line_number: calling app doc line number. For AP this is
  --   ap_invoice_lines_all.line_number.
  --
  -- The combination of p_document_payable_id and p_line_number
  -- can uniquely locate a document line.
  -- For example if the calling product of a doc is AP
  -- p_document_payable_id can locate
  -- iby_docs_payable_all/ap_documents_payable.calling_app_doc_unique_ref2,
  -- which is ap_invoice_all.invoice_id. The combination of invoice_id and
  -- p_line_number will uniquely identify the doc line.
  --
  FUNCTION get_docline_ext_agg(p_document_payable_id IN NUMBER
                              ,p_line_number         IN NUMBER)
    RETURN xmltype IS
  BEGIN
    RETURN NULL;
  END get_docline_ext_agg;

  --
  -- This API is called once only for the payment process request.
  -- Implementor should construct the extract extension elements
  -- at the payment request level as a SQLX XML Aggregate
  -- and return the aggregate.
  --
  FUNCTION get_ppr_ext_agg(p_payment_service_request_id IN NUMBER)
    RETURN xmltype IS
  BEGIN
    RETURN NULL;
  END get_ppr_ext_agg;

END iby_fd_extract_ext_pub;
/