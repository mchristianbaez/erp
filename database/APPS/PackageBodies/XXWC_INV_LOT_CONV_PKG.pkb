CREATE OR REPLACE PACKAGE BODY APPS.XXWC_INV_LOT_CONV_PKG AS
/*************************************************************************
  $Header XXWC_INV_LOT_CONV_PKG $
  Module Name: XXWC_INV_LOT_CONV_PKG.pkb

  PURPOSE: Remove Lot Control from Oralce

  REVISIONS:
  Ver        Date        Author                     Description
  ---------  ----------  ---------------    -------------------------
  1.0        08/16/2012  Gopi Damuluri      Initial Version
**************************************************************************/

g_user_id NUMBER := fnd_profile.VALUE ('USER_ID');

-- ####################################
-- Check for Reservations
-- ####################################
   FUNCTION check_reservations (p_item_id IN NUMBER) RETURN VARCHAR2
      IS
      l_rec_count NUMBER := 0;
   BEGIN
     SELECT COUNT(1)
       INTO l_rec_count
       FROM mtl_reservations
      WHERE 1 = 1
        AND inventory_item_id = p_item_id;

      IF l_rec_count = 0 THEN
        RETURN 'V';
      ELSE
        RETURN 'E';
      END IF;

   EXCEPTION
   WHEN OTHERS THEN
     RETURN 'E';
     dbms_output.put_line('Error - '||SQLERRM);
   END;

-- ####################################
-- Miscellaneous Issue
-- ####################################
   PROCEDURE create_misc_issue (p_item_id IN NUMBER, p_consigned_flag IN NUMBER)
          IS
       CURSOR cur_item
           IS
       SELECT moh.organization_id
            , moh.subinventory_code
            , moh.inventory_item_id
            , mp.material_account
            , moh.cost_group_id
            , moh.is_consigned
            , SUM(moh.transaction_quantity) transaction_quantity
            , moh.orig_date_received
         FROM xxwc.XXWC_LOT_CONV_STG_TBL  moh
            , mtl_parameters mp
        WHERE moh.lot_number           IS NOT NULL
          AND moh.organization_id       = mp.organization_id
          and moh.transaction_quantity != 0
          and moh.inventory_item_id     = p_item_id
          and moh.is_consigned          = p_consigned_flag
          and moh.status                IN ('N','E')
       group by moh.organization_id
              , moh.is_consigned
              , moh.orig_date_received
              , moh.subinventory_code
              , moh.inventory_item_id
              , mp.material_account
              , moh.cost_group_id;

       CURSOR cur_lot (p_item_id IN NUMBER, p_organization_id IN NUMBER, p_sub_inventory IN VARCHAR2, p_date_received IN DATE)
           IS
       select lot_number
            , moh.is_consigned
            , sum(transaction_quantity) transaction_quantity
            , moh.orig_date_received
         from xxwc.XXWC_LOT_CONV_STG_TBL moh
        where organization_id        = p_organization_id
           and inventory_item_id     = p_item_id
           and subinventory_code     = p_sub_inventory
           and moh.status            IN ('N','E')
           and moh.is_consigned       = p_consigned_flag
           and moh.orig_date_received     = p_date_received
           and transaction_quantity != 0
       group by lot_number
              , moh.orig_date_received
              , moh.is_consigned;

              l_flow_schedule            VARCHAR2 (1) := 'Y';
              l_scheduled_flag           NUMBER := 2;
              l_transaction_mode         NUMBER := 3;
              l_process_flag             NUMBER := 1;
              l_lock_mode                NUMBER := 2;
              l_distribution_account_id  NUMBER;
              l_interfaceprocess_count   NUMBER := 0;
              l_interfaceerror_count     NUMBER := 0;
              l_interfacerec_count       NUMBER := 0;
              l_errormsg                 VARCHAR2 (1000);
              l_organization_code        VARCHAR2 (10);
              l_transaction_type_id      NUMBER  := 32; -- Misc Issue
              l_trans_source_type_id     NUMBER;
              l_trans_action_id          NUMBER;
              l_transaction_source_id    NUMBER;
              l_item_uom                 VARCHAR2(5);

              l_program_err              EXCEPTION;
              l_error_msg                VARCHAR2(200);

       BEGIN

          ---------------------------------------------------------------
          -- Derive transaction source and transaction id
          ---------------------------------------------------------------
          BEGIN
          SELECT transaction_source_type_id
               , transaction_action_id
            INTO l_trans_source_type_id
               , l_trans_action_id
            FROM mtl_transaction_types
           WHERE transaction_type_id = l_transaction_type_id;
          EXCEPTION
          WHEN OTHERS THEN
            l_error_msg := 'Error deriving transaction source and transaction id';
            RAISE l_program_err;
          END;

         FOR rec_item IN cur_item LOOP

          BEGIN
            l_item_uom := NULL;
            ---------------------------------------------------------------
            -- Derive Distribution Account
            ---------------------------------------------------------------
            BEGIN
              SELECT disposition_id
                   , distribution_account
                INTO l_transaction_source_id
                   , l_distribution_account_id
                FROM mtl_generic_dispositions
               WHERE organization_id = rec_item.organization_id
                 AND segment1        = 'CONVERSION';
            EXCEPTION
            WHEN OTHERS THEN
              l_error_msg := 'Error deriving Distribution Account';
              RAISE l_program_err;
            END;

            ---------------------------------------------------------------
            -- Derive Primary UOM Code for the item
            ---------------------------------------------------------------
            BEGIN
              SELECT primary_uom_code
                INTO l_item_uom
                FROM mtl_system_items_b
               WHERE organization_id   = rec_item.organization_id
                 AND inventory_item_id = rec_item.inventory_item_id;
            EXCEPTION
            WHEN OTHERS THEN
              l_error_msg := 'Error deriving Primary UOM Code for the item';
              RAISE l_program_err;
            END;

            ---------------------------------------------------------------
            -- Insert into MTL_TRANSACTIONS_INTERFACE
            ---------------------------------------------------------------
            BEGIN
               INSERT INTO mtl_transactions_interface (source_code,
                                                  process_flag,
                                                  transaction_mode,
                                                  transaction_uom,
                                                  transaction_type_id,
                                                  flow_schedule,
                                                  scheduled_flag,
                                                  created_by,
                                                  last_updated_by,
                                                  organization_id,
                                                  subinventory_code,
                                                  transaction_date,
                                                  new_average_cost,
                                                  transaction_reference,
                                                  inventory_item_id,
                                                  source_header_id,
                                                  source_line_id,
                                                  transaction_quantity,
                                                  transaction_source_id,
                                                  distribution_account_id,
                                                  locator_name,
                                                  lock_flag,
                                                  transaction_action_id,
                                                  transaction_source_type_id,
                                                  creation_date,
                                                  last_update_date,
                                                  transaction_interface_id,
                                                  transaction_header_id,
                                                  material_account,
                                                  cost_group_id
                                                           )
                VALUES   ('CONVERSION',                                    -- r_validrec_stg.transaction_source,
                         1,                                               -- l_process_flag,
                         3,                                               -- l_transaction_mode,
                         l_item_uom,                                      -- transaction_uom
                         l_transaction_type_id,                           -- 'Miscellaneous receipt'
                         l_flow_schedule,                                 -- flow_schedule
                         l_scheduled_flag,                                -- scheduled_flag
                         g_user_id,                                       -- created_by
                         g_user_id,                                       -- last_updated_by
                         rec_item.organization_id,                        -- organization_id
                         rec_item.subinventory_code,                      -- subinventory_code
                         sysdate,                                         -- transaction_date
       --                     decode(r_validrec_stg.how_priced,
       --                            'E',r_validrec_stg.transaction_cost,
       --                            'C',r_validrec_stg.transaction_cost/100,
       --                            'M',r_validrec_stg.transaction_cost/1000,
       --                             r_validrec_stg.transaction_cost),   -- new_average_cost
                         NULL,                                            -- new_average_cost 
                         NULL,                                            -- transaction_reference
                         rec_item.inventory_item_id,                      -- inventory_item_id
                         99,                                              -- source_header_id
                         98,                                              -- source_line_id
                         -1*rec_item.transaction_quantity,                -- transaction_quantity
                         l_transaction_source_id,                         -- transaction_source_id
                         l_distribution_account_id,                       -- distribution_account_id
                         NULL,                                            -- locator_name
                         l_lock_mode,                                     -- lock_flag
                         l_trans_action_id,                               -- transaction_action_id
                         l_trans_source_type_id,                          -- transaction_source_type_id
                         SYSDATE,                                         -- creation_date
                         SYSDATE,                                         -- last_update_date
                         mtl_material_transactions_s.NEXTVAL,             -- transaction_interface_id
                         TO_CHAR (rec_item.organization_id)|| TO_CHAR (SYSDATE, 'ddmmyyyy'),
                         rec_item.material_account,
                         rec_item.cost_group_id
                         );
            EXCEPTION
            WHEN OTHERS THEN
              l_error_msg := 'Error inserting into MTL_TRANSACTIONS_INTERFACE';
              RAISE l_program_err;
            END;


             FOR rec_lot IN cur_lot (rec_item.inventory_item_id, rec_item.organization_id, rec_item.subinventory_code, rec_item.orig_date_received) LOOP

             ---------------------------------------------------------------
             -- Insert into MTL_TRANSACTION_LOTS_INTERFACE
             ---------------------------------------------------------------

             BEGIN
                INSERT INTO mtl_transaction_lots_interface (transaction_interface_id,
                                                            lot_number,
                                                            transaction_quantity,
                                                            last_update_date,
                                                            last_updated_by,
                                                            creation_date,
                                                            created_by,
                                                            last_update_login,
                                                            product_transaction_id,
                                                            lot_expiration_date)
                  VALUES   (mtl_material_transactions_s.CURRVAL,    -- transaction_interface_id
                            rec_lot.lot_number,                     -- lot_number
                            -1*rec_lot.transaction_quantity,        -- transaction_quantity,
                            SYSDATE,                                -- last_update_date
                            g_user_id,                              -- last_updated_by
                            SYSDATE,                                -- creation_date
                            g_user_id,                              -- created_by
                            -1,                                     -- last_update_login
                            mtl_material_transactions_s.CURRVAL,    -- product_transaction_id
                            SYSDATE + 1                             -- lot_expiration_date
                            );
             EXCEPTION
             WHEN OTHERS THEN
               l_error_msg := 'Error inserting into MTL_TRANSACTION_LOTS_INTERFACE';
               RAISE l_program_err;
             END;

            END LOOP; -- cur_lot

            EXCEPTION
            WHEN l_program_err THEN
              UPDATE xxwc.XXWC_LOT_CONV_STG_TBL stg
                 SET status = 'E'
                   , error_message = l_error_msg
               WHERE stg.organization_id    = rec_item.organization_id
                   AND stg.subinventory_code  = rec_item.subinventory_code
                   AND stg.inventory_item_id  = rec_item.inventory_item_id
                   AND stg.is_consigned       = rec_item.is_consigned
                   AND stg.orig_date_received      = rec_item.orig_date_received;
            WHEN OTHERS THEN
              l_error_msg :=  'UnIdentified error - '||SQLERRM;
              UPDATE xxwc.XXWC_LOT_CONV_STG_TBL stg
                 SET status = 'E'
                   , error_message = l_error_msg
               WHERE stg.organization_id    = rec_item.organization_id
                   AND stg.subinventory_code  = rec_item.subinventory_code
                   AND stg.inventory_item_id  = rec_item.inventory_item_id
                   AND stg.is_consigned       = rec_item.is_consigned
                   AND stg.orig_date_received      = rec_item.orig_date_received;
            END;

              UPDATE xxwc.XXWC_LOT_CONV_STG_TBL stg
                 SET status = '1'
                   , error_message = NULL
               WHERE stg.organization_id    = rec_item.organization_id
                   AND stg.subinventory_code  = rec_item.subinventory_code
                   AND stg.inventory_item_id  = rec_item.inventory_item_id
                   AND stg.is_consigned       = rec_item.is_consigned
                   AND stg.orig_date_received      = rec_item.orig_date_received;
          END LOOP;   -- cur_item

       EXCEPTION
       WHEN OTHERS THEN
         dbms_output.put_line('Error - '||SQLERRM);
       END create_misc_issue;

-- ####################################
-- Miscellaneous Receipt
-- ####################################
   PROCEDURE create_misc_receipt (p_item_id IN NUMBER, p_consigned_flag IN NUMBER)
          IS
       CURSOR cur_item
           IS
       SELECT moh.organization_id
            , moh.subinventory_code
            , moh.inventory_item_id
            , mp.material_account
            , moh.cost_group_id
            , moh.is_consigned
            , SUM(moh.transaction_quantity) transaction_quantity
            , moh.orig_date_received
         FROM xxwc.XXWC_LOT_CONV_STG_TBL  moh
            , mtl_parameters mp
        WHERE moh.lot_number           IS NOT NULL
          AND moh.organization_id       = mp.organization_id
          and moh.transaction_quantity != 0
          and moh.inventory_item_id     = p_item_id
          and moh.is_consigned          = 2 -- p_consigned_flag
          and moh.status                IN ('3','E')
       group by moh.organization_id
              , moh.orig_date_received
              , moh.subinventory_code
              , moh.inventory_item_id
              , mp.material_account
              , moh.cost_group_id
              , moh.is_consigned;

              l_flow_schedule            VARCHAR2 (1) := 'Y';
              l_scheduled_flag           NUMBER := 2;
              l_transaction_mode         NUMBER := 3;
              l_process_flag             NUMBER := 1;
              l_lock_mode                NUMBER := 2;
              l_distribution_account_id  NUMBER;
              l_interfaceprocess_count   NUMBER := 0;
              l_interfaceerror_count     NUMBER := 0;
              l_interfacerec_count       NUMBER := 0;
              l_errormsg                 VARCHAR2 (1000);
              l_organization_code        VARCHAR2 (10);
              l_transaction_type_id      NUMBER  := 42; -- Misc Receipt
              l_trans_source_type_id     NUMBER;
              l_trans_action_id          NUMBER;
              l_transaction_source_id    NUMBER;
              l_item_uom                 VARCHAR2(5);

              l_program_err              EXCEPTION;
              l_error_msg                VARCHAR2(200);

   BEGIN

      ---------------------------------------------------------------
      -- Derive transaction source and transaction id
      ---------------------------------------------------------------
      BEGIN
      SELECT transaction_source_type_id
           , transaction_action_id
        INTO l_trans_source_type_id
           , l_trans_action_id
        FROM mtl_transaction_types
       WHERE transaction_type_id = l_transaction_type_id;
      EXCEPTION
      WHEN OTHERS THEN
        l_error_msg := 'Error deriving transaction source and transaction id';
        RAISE l_program_err;
      END;

     FOR rec_item IN cur_item LOOP

      BEGIN
        l_item_uom := NULL;
        ---------------------------------------------------------------
        -- Derive Distribution Account
        ---------------------------------------------------------------
        BEGIN
          SELECT disposition_id
               , distribution_account
            INTO l_transaction_source_id
               , l_distribution_account_id
            FROM mtl_generic_dispositions
           WHERE organization_id = rec_item.organization_id
             AND segment1        = 'CONVERSION';
        EXCEPTION
        WHEN OTHERS THEN
          l_error_msg := 'Error deriving Distribution Account';
          RAISE l_program_err;
        END;

        ---------------------------------------------------------------
        -- Derive Primary UOM Code for the item
        ---------------------------------------------------------------
        BEGIN
          SELECT primary_uom_code
            INTO l_item_uom
            FROM mtl_system_items_b
           WHERE organization_id   = rec_item.organization_id
             AND inventory_item_id = rec_item.inventory_item_id;
        EXCEPTION
        WHEN OTHERS THEN
          l_error_msg := 'Error deriving Primary UOM Code for the item';
          RAISE l_program_err;
        END;

        ---------------------------------------------------------------
        -- Insert into MTL_TRANSACTIONS_INTERFACE
        ---------------------------------------------------------------
        BEGIN
           INSERT INTO mtl_transactions_interface (source_code,
                                              process_flag,
                                              transaction_mode,
                                              transaction_uom,
                                              transaction_type_id,
                                              flow_schedule,
                                              scheduled_flag,
                                              created_by,
                                              last_updated_by,
                                              organization_id,
                                              subinventory_code,
                                              transaction_date,
                                              new_average_cost,
                                              transaction_reference,
                                              inventory_item_id,
                                              source_header_id,
                                              source_line_id,
                                              transaction_quantity,
                                              transaction_source_id,
                                              distribution_account_id,
                                              locator_name,
                                              lock_flag,
                                              transaction_action_id,
                                              transaction_source_type_id,
                                              creation_date,
                                              last_update_date,
                                              transaction_interface_id,
                                              transaction_header_id,
                                              material_account,
                                              cost_group_id
                                                       )
            VALUES   ('CONVERSION',                                    -- r_validrec_stg.transaction_source,
                     1,                                               -- l_process_flag,
                     3,                                               -- l_transaction_mode,
                     l_item_uom,                                      -- transaction_uom
                     l_transaction_type_id,                           -- 'Miscellaneous receipt'
                     l_flow_schedule,                                 -- flow_schedule
                     l_scheduled_flag,                                -- scheduled_flag
                     g_user_id,                                       -- created_by
                     g_user_id,                                       -- last_updated_by
                     rec_item.organization_id,                        -- organization_id
                     rec_item.subinventory_code,                      -- subinventory_code
                     sysdate,                                         -- transaction_date
   --                     decode(r_validrec_stg.how_priced,
   --                            'E',r_validrec_stg.transaction_cost,
   --                            'C',r_validrec_stg.transaction_cost/100,
   --                            'M',r_validrec_stg.transaction_cost/1000,
   --                             r_validrec_stg.transaction_cost),   -- new_average_cost
                     NULL,                                            -- new_average_cost 
                     NULL,                                            -- transaction_reference
                     rec_item.inventory_item_id,                      -- inventory_item_id
                     99,                                              -- source_header_id
                     98,                                              -- source_line_id
                     rec_item.transaction_quantity,                   -- transaction_quantity
                     l_transaction_source_id,                         -- transaction_source_id
                     l_distribution_account_id,                       -- distribution_account_id
                     NULL,                                            -- locator_name
                     l_lock_mode,                                     -- lock_flag
                     l_trans_action_id,                               -- transaction_action_id
                     l_trans_source_type_id,                          -- transaction_source_type_id
                     SYSDATE,                                         -- creation_date
                     SYSDATE,                                         -- last_update_date
                     mtl_material_transactions_s.NEXTVAL,             -- transaction_interface_id
                     TO_CHAR (rec_item.organization_id)|| TO_CHAR (SYSDATE, 'ddmmyyyy'),
                     rec_item.material_account,
                     rec_item.cost_group_id
                     );
        EXCEPTION
        WHEN OTHERS THEN
          l_error_msg := 'Error inserting into MTL_TRANSACTIONS_INTERFACE';
          RAISE l_program_err;
        END;

        EXCEPTION
        WHEN l_program_err THEN
          UPDATE xxwc.XXWC_LOT_CONV_STG_TBL stg
             SET status = 'E'
               , error_message = l_error_msg
           WHERE stg.organization_id    = rec_item.organization_id
               AND stg.subinventory_code  = rec_item.subinventory_code
               AND stg.inventory_item_id  = rec_item.inventory_item_id
               AND stg.is_consigned       = rec_item.is_consigned
               AND stg.orig_date_received      = rec_item.orig_date_received;
        WHEN OTHERS THEN
          l_error_msg :=  'UnIdentified error - '||SQLERRM;
          UPDATE xxwc.XXWC_LOT_CONV_STG_TBL stg
             SET status = 'E'
               , error_message = l_error_msg
           WHERE stg.organization_id    = rec_item.organization_id
               AND stg.subinventory_code  = rec_item.subinventory_code
               AND stg.inventory_item_id  = rec_item.inventory_item_id
               AND stg.is_consigned       = rec_item.is_consigned
               AND stg.orig_date_received      = rec_item.orig_date_received;
        END;

          UPDATE xxwc.XXWC_LOT_CONV_STG_TBL stg
             SET status = '4'
               , error_message = NULL
           WHERE stg.organization_id    = rec_item.organization_id
               AND stg.subinventory_code  = rec_item.subinventory_code
               AND stg.inventory_item_id  = rec_item.inventory_item_id
               AND stg.is_consigned       = rec_item.is_consigned
               AND stg.orig_date_received      = rec_item.orig_date_received;
      END LOOP;   -- cur_item

   EXCEPTION
   WHEN OTHERS THEN
     dbms_output.put_line('Error - '||SQLERRM);
   END create_misc_receipt;

-- ########################################################################
-- Update Lot Divisible Flag - Org Item Level
-- ########################################################################
   PROCEDURE update_lot_div_flag (p_item_id IN NUMBER)
        IS
     l_item_table                             ego_item_pub.item_tbl_type;
     x_item_table                             ego_item_pub.item_tbl_type;
     x_return_status                          VARCHAR2(1);
     x_msg_count                              NUMBER(10);
     x_msg_data                               VARCHAR2(1000);
     i  NUMBER := 0;

   -------------------------------------------------------------------------------
   -- Cursor to Update Lot Divisible Flag - Org Item Level
   -------------------------------------------------------------------------------
   CURSOR cur_lot
       IS
     SELECT msib.inventory_item_id
          , mp.organization_id
          , msib.segment1
      FROM mtl_system_items_b msib
         , mtl_parameters mp
     WHERE msib.organization_id       = mp.organization_id
       AND msib.inventory_item_id     = p_item_id
       AND msib.lot_divisible_flag   != 'N'
       ;

   l_user_id NUMBER := 1296;
   l_resp_id NUMBER := 20634;
   l_appl_id NUMBER := 401;
   l_error_message  VARCHAR2(200);

   BEGIN

      fnd_global.apps_initialize(l_user_id, l_resp_id, l_appl_id);
      mo_global.init ('INV');
      mo_global.set_policy_context ('S', 162);

     FOR rec_lot IN cur_lot LOOP
       i                                    := i + 1;
       l_item_table(i).transaction_type     := 'UPDATE';
       l_item_table(i).inventory_item_id    := rec_lot.inventory_item_id;
       l_item_table(i).organization_id      := rec_lot.organization_id;
       l_item_table(i).lot_divisible_flag   := 'N'; -- RESERVABLE = NO
     END LOOP;

   -------------------------------------------------------------------------------
   -- API call to to update
   -------------------------------------------------------------------------------
      ego_item_pub.process_items(p_api_version    => 1.0
               , p_init_msg_list   => fnd_api.g_true
               , p_commit          => fnd_api.g_true
               , p_item_tbl        => l_item_table
               , x_item_tbl        => x_item_table
               , x_return_status   => x_return_status
               , x_msg_count       => x_msg_count
               );
      COMMIT;

          UPDATE xxwc.XXWC_LOT_CONV_STG_TBL stg
             SET status = '2'
               , error_message = NULL
           WHERE 1 = 1
             AND stg.inventory_item_id  = p_item_id;

      dbms_output.put_line('Status  = '||x_return_status);

   EXCEPTION
   WHEN OTHERS THEN
     dbms_output.put_line('Error Message - '||SQLERRM);
     
     l_error_message := substr('SQLERRM',1,200);
     
     UPDATE xxwc.xxwc_lot_conv_stg_tbl stg
        SET error_message = l_error_message
      WHERE 1 = 1
        AND stg.inventory_item_id  = p_item_id;
   END update_lot_div_flag;

-- ########################################################################
-- Update RESERVABLE_TYPE and LOT_CONTROL_CODE - Master Item Level
-- ########################################################################
   PROCEDURE update_rsv_lot_ctrl_flag(p_item_id IN NUMBER)
          IS
     l_item_table                             ego_item_pub.item_tbl_type;
     x_item_table                             ego_item_pub.item_tbl_type;
     x_return_status                          VARCHAR2(1);
     x_msg_count                              NUMBER(10);
     x_msg_data                               VARCHAR2(1000);
     i  NUMBER := 0;

    -------------------------------------------------------------------------------
    -- Cursor to update RESERVABLE_TYPE and LOT_CONTROL_CODE - Master Item Level
    -------------------------------------------------------------------------------
    CURSOR cur_lot
        IS
      SELECT msib.inventory_item_id
           , mp.organization_id
           , msib.segment1
       FROM mtl_system_items_b msib, mtl_parameters mp
      WHERE msib.organization_id   = mp.organization_id
        AND mp.organization_code   = 'MST'
        AND msib.inventory_item_id = p_item_id
        AND msib.lot_control_code  = 2
        ;

    l_user_id        NUMBER := 1296;
    l_resp_id        NUMBER := 20634;
    l_appl_id        NUMBER := 401;
    l_error_message  VARCHAR2(200);

    BEGIN

       fnd_global.apps_initialize(l_user_id, l_resp_id, l_appl_id);
       mo_global.init ('INV');
       mo_global.set_policy_context ('S', 162);

      FOR rec_lot IN cur_lot LOOP
        i                                    := i + 1;
        l_item_table(i).transaction_type     := 'UPDATE';
        l_item_table(i).inventory_item_id    := rec_lot.inventory_item_id;
        l_item_table(i).organization_id      := rec_lot.organization_id;
        l_item_table(i).reservable_type       := 2; -- RESERVABLE = NO
        l_item_table(i).lot_control_code      := 1; -- LOT_CONTROL_CODE = NO CONTROL
      END LOOP;

      -------------------------------------------------------------------------------
      -- API call to to update
      -------------------------------------------------------------------------------
       ego_item_pub.process_items(p_api_version    => 1.0
                   , p_init_msg_list   => fnd_api.g_true
                   , p_commit          => fnd_api.g_true
                   , p_item_tbl        => l_item_table
                   , x_item_tbl        => x_item_table
                   , x_return_status   => x_return_status
                   , x_msg_count       => x_msg_count
                   );
       COMMIT;

          UPDATE xxwc.XXWC_LOT_CONV_STG_TBL stg
             SET status = '3'
               , error_message = NULL
           WHERE 1 = 1
             AND stg.inventory_item_id  = p_item_id;

       dbms_output.put_line('Status  = '||x_return_status);

   EXCEPTION
   WHEN OTHERS THEN
     dbms_output.put_line('Error Message - '||SQLERRM);
     l_error_message := substr('SQLERRM',1,200);
     
     UPDATE xxwc.xxwc_lot_conv_stg_tbl stg
        SET error_message = l_error_message
      WHERE 1 = 1
        AND stg.inventory_item_id  = p_item_id;
   END update_rsv_lot_ctrl_flag;

-- ########################################################################
-- Main Procedure
-- ########################################################################
   PROCEDURE main(p_errbuf           OUT VARCHAR2,
                  p_retcode          OUT VARCHAR2,
                  p_process_flag      IN VARCHAR2,
                  p_item_number       IN VARCHAR2,
                  p_consigned_flag    IN NUMBER)
   IS

     CURSOR cur_item
         IS
     SELECT DISTINCT stg.inventory_item_id
       FROM xxwc.XXWC_LOT_CONV_STG_TBL stg
      WHERE 1 = 1
        AND NVL(status,'0') != 'S'
        AND item_num = NVL(p_item_number, item_num)
        AND is_consigned = p_consigned_flag;

     l_process_flag   VARCHAR2(1);
   BEGIN

      FOR rec_item IN cur_item LOOP
         l_process_flag := 0;

         IF check_reservations(rec_item.inventory_item_id) = 'V' THEN
            IF p_process_flag = '1' THEN
               ------------------------------------------------------
               -- Create Miscellaneous issue
               ------------------------------------------------------
               create_misc_issue(rec_item.inventory_item_id, p_consigned_flag);
            ELSIF p_process_flag = '2' THEN
               ------------------------------------------------------
               -- Update Lot Division Flag - Org Level
               ------------------------------------------------------
               update_lot_div_flag(rec_item.inventory_item_id);

            ELSIF p_process_flag = '3' THEN
               ------------------------------------------------------
               -- Update Reservable flag , Lot Control - Master Level
               ------------------------------------------------------
               update_rsv_lot_ctrl_flag(rec_item.inventory_item_id);
            
            ELSIF p_process_flag = '4' THEN
               ------------------------------------------------------
               -- Create Miscellaneous Receipt
               ------------------------------------------------------
               create_misc_receipt(rec_item.inventory_item_id, p_consigned_flag);

               UPDATE xxwc.XXWC_LOT_CONV_STG_TBL
                  SET status = 'S'
                    , error_message = NULL
                WHERE inventory_item_id = rec_item.inventory_item_id
                  AND is_consigned      = p_consigned_flag;

            END IF;
         ELSE
           UPDATE xxwc.XXWC_LOT_CONV_STG_TBL
              SET status = 'E'
                , error_message = 'Item has open reservations'
            WHERE inventory_item_id = rec_item.inventory_item_id
              AND is_consigned      = p_consigned_flag;
         END IF;
      END LOOP;

   END main;

-- ########################################################################
-- Update Item OnHand BornOnDate in Oralce
-- ########################################################################
PROCEDURE update_born_on_date
IS
   l_trx_date date;
   CURSOR c1
   IS
      SELECT organization_id
           , inventory_item_id
           , orig_date_received trx_date
           , is_consigned
           , subinventory_code
            , SUM(transaction_quantity) transaction_quantity
        FROM xxwc.xxwc_lot_conv_stg_tbl
       WHERE 1 = 1
         AND status = 'S'
         AND organization_id != 222
   GROUP BY organization_id
           , inventory_item_id
           , orig_date_received 
           , is_consigned
           , subinventory_code;
BEGIN

   FOR c1_rec IN c1
   LOOP
      UPDATE mtl_onhand_quantities_detail
         SET orig_date_received = c1_rec.trx_date
       WHERE inventory_item_id  = c1_rec.inventory_item_id
         AND organization_id    = c1_rec.organization_id
         AND is_consigned       = c1_rec.is_consigned
         AND subinventory_code  = c1_rec.subinventory_code
         AND transaction_quantity = c1_rec.transaction_quantity
         AND trunc(orig_date_received) = trunc(SYSDATE)
         AND rownum = 1;

      UPDATE xxwc.xxwc_lot_conv_stg_tbl
         SET status = '9'
       WHERE inventory_item_id  = c1_rec.inventory_item_id
         AND organization_id    = c1_rec.organization_id
         AND is_consigned       = c1_rec.is_consigned
         AND subinventory_code  = c1_rec.subinventory_code
         AND transaction_quantity = c1_rec.transaction_quantity
         AND trunc(orig_date_received) = trunc(SYSDATE)
         AND rownum = 1;

   END LOOP;
END update_born_on_date;

END XXWC_INV_LOT_CONV_PKG;
/