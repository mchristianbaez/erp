CREATE OR REPLACE PACKAGE BODY APPS.xxwc_mv_routines_pkg
AS
   /*************************************************************************
    Copyright (c) 2012 Lucidity Consulting Group
    All rights reserved.
   **************************************************************************
     $Header xxwc_mv_routines_pkg $
     Module Name: xxwc_mv_routines_pkg.pks

     PURPOSE:   This package is used by the BT Interfaces, EBS to Prism

     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.0        01/11/2012  Consuelo Gonzalez      Initial Version
     2.0        06/27/2014  Ram Talluri            get_vendor_quote_cost   --TMS 20140627-00126 
     3.0        05/29/2015  Maharajan Shunmugam    TMS 20150527-00031 OM Rental Receipt Bug Fix
	 4.0        09/04/2015  Manjula Chellappan     TMS 20150901-00161 - XXWC_CUST_CONTACTS Performance Tuning
	 5.0        11/06/2015  Pattabhi Avula         TMS 20150722-00210 - OM - Ability to edit contacts from the sales order (job on the fly)

   **************************************************************************/
   FUNCTION get_phone_fax_number (
      p_contact_point_type   IN   VARCHAR2,
      p_party_id             IN   NUMBER,
      p_party_site_id        IN   NUMBER,
      p_phone_line_type      IN   VARCHAR2
   )
      RETURN VARCHAR2
   IS
      l_contact_number   VARCHAR2 (50);
   BEGIN

      l_contact_number := NULL;

      -- try site level first
      IF p_party_site_id is not null then
          BEGIN
             -- Primary and active
             SELECT raw_phone_number
               INTO l_contact_number
               FROM hz_contact_points
              WHERE owner_table_name = 'HZ_PARTY_SITES'
                AND owner_table_id = p_party_site_id
                AND contact_point_type = p_contact_point_type
                AND phone_line_type = p_phone_line_type
                AND status = 'A'
                AND primary_flag = 'Y'
                AND ROWNUM = 1;
          EXCEPTION
             WHEN OTHERS
             THEN
                BEGIN
                   -- Any Active
                   SELECT raw_phone_number
                     INTO l_contact_number
                     FROM hz_contact_points
                    WHERE owner_table_name = 'HZ_PARTY_SITES'
                      AND owner_table_id = p_party_site_id
                      AND contact_point_type = p_contact_point_type
                      AND phone_line_type = p_phone_line_type
                      AND status = 'A'
                      AND ROWNUM = 1;
                EXCEPTION
                   WHEN OTHERS
                   THEN
                      l_contact_number := NULL;
                END;
          END;
      END IF;

      IF p_party_id IS NOT NULL
      THEN
         -- try party level next
         BEGIN
            -- Primary and active
            SELECT raw_phone_number
              INTO l_contact_number
              FROM hz_contact_points
             WHERE owner_table_id = p_party_id
               AND owner_table_name = 'HZ_PARTIES'
               AND contact_point_type = p_contact_point_type
               AND primary_flag = 'Y'
               AND phone_line_type = p_phone_line_type
               AND status = 'A'
               AND ROWNUM = 1;
         EXCEPTION
            WHEN OTHERS
            THEN
               BEGIN
                  -- Any Active
                  SELECT raw_phone_number
                    INTO l_contact_number
                    FROM hz_contact_points
                   WHERE owner_table_id = p_party_id
                     AND owner_table_name = 'HZ_PARTIES'
                     AND contact_point_type = p_contact_point_type
                     AND phone_line_type = p_phone_line_type
                     AND status = 'A'
                     AND ROWNUM = 1;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     l_contact_number := NULL;
               END;
         END;
      END IF;

      RETURN l_contact_number;
   END get_phone_fax_number;

   FUNCTION get_open_po_qty (
      p_inventory_item_id   IN   NUMBER,
      p_organization_id     IN   NUMBER
   )
      RETURN NUMBER
   IS
      l_open_qty   NUMBER;
   BEGIN
      l_open_qty := 0;

      SELECT SUM (  plla.quantity
                  - NVL (plla.quantity_received, 0)
                  - NVL (plla.quantity_cancelled, 0)
                 )
        INTO l_open_qty
        FROM po_lines_all pll, po_line_locations_all plla
       WHERE NVL (pll.cancel_flag, 'N') = 'N'
         AND NVL (pll.closed_flag, 'N') = 'N'
         AND pll.item_id = p_inventory_item_id
         AND pll.po_line_id = plla.po_line_id
         AND pll.po_header_id = plla.po_header_id
         AND plla.ship_to_organization_id = p_organization_id
         AND NVL (plla.cancel_flag, 'N') = 'N'
         AND NVL (plla.closed_flag, 'N') = 'N';

      RETURN (NVL (l_open_qty, 0));
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN (0);
   END get_open_po_qty;

   FUNCTION get_safety_stock_qty (
      p_inventory_item_id   IN   NUMBER,
      p_organization_id     IN   NUMBER
   )
      RETURN NUMBER
   IS
      l_ss_qty   NUMBER;
   BEGIN
      l_ss_qty := NULL;

      SELECT   mss.safety_stock_quantity
          INTO l_ss_qty
          FROM mtl_safety_stocks mss
         WHERE mss.inventory_item_id = p_inventory_item_id
           AND mss.organization_id = p_organization_id
           AND TRUNC (effectivity_date) <= TRUNC (SYSDATE)
           AND ROWNUM = 1
      ORDER BY effectivity_date DESC;

      RETURN (NVL (l_ss_qty, 0));
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN (0);
   END get_safety_stock_qty;

   FUNCTION get_open_so_ln_qty (
      p_inventory_item_id   IN   NUMBER,
      p_organization_id     IN   NUMBER
   )
      RETURN NUMBER
   IS
      l_open_qty   NUMBER;
   BEGIN
      l_open_qty := NULL;

      SELECT SUM (  oola.ordered_quantity
                  - NVL (oola.shipped_quantity, 0)
                  - NVL (oola.cancelled_quantity, 0)
                 )
        INTO l_open_qty
        FROM oe_order_lines_all oola
       WHERE oola.inventory_item_id = p_inventory_item_id
         AND oola.ship_from_org_id = p_organization_id
         AND oola.line_category_code = 'ORDER'
         AND NVL (oola.cancelled_flag, 'N') = 'N'
         AND NVL (oola.booked_flag, 'N') = 'Y';

      RETURN (NVL (l_open_qty, 0));
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN (0);
   END get_open_so_ln_qty;

   FUNCTION get_transit_qty (
      p_inventory_item_id   IN   NUMBER,
      p_organization_id     IN   NUMBER
   )
      RETURN NUMBER
   IS
      l_transit_qty   NUMBER;
   BEGIN
      l_transit_qty := 0;

      SELECT SUM (x1.ordered_qty - x1.shipped_qty - x1.cancelled_qty)
        INTO l_transit_qty
        FROM (SELECT mp_from.organization_code ship_from_branch_num,
                     mp_from.organization_id ship_from_org_id,
                     mp_to.organization_code ship_to_branch_num,
                     mp_to.organization_id ship_to_org_id,
                     oola.flow_status_code line_status,
                     ooha.creation_date order_date,
                     oola.actual_shipment_date shipped_date,
                     msib.segment1 sku_number, msib.inventory_item_id,
                     NVL (oola.ordered_quantity, 0) ordered_qty,
                     NVL (oola.shipped_quantity, 0) shipped_qty,
                     NVL (oola.cancelled_quantity, 0) cancelled_qty,
                     req_ln.quantity requested_qty,
                     NVL (req_ln.quantity_received, 0) received_qty,
                     req_hdr.segment1 po_ref_num
                FROM po_requisition_headers_all req_hdr,
                     po_requisition_lines_all req_ln,
                     oe_order_headers_all ooha,
                     oe_order_sources oos,
                     oe_order_lines_all oola,
                     mtl_parameters mp_from,
                     org_organization_definitions ood_from,
                     mtl_parameters mp_to,
                     org_organization_definitions ood_to,
                     mtl_system_items_b msib,
                     hr_operating_units hou
               WHERE req_hdr.type_lookup_code = 'INTERNAL'
                 AND req_hdr.requisition_header_id =
                                                  req_ln.requisition_header_id
                 AND req_hdr.requisition_header_id = ooha.source_document_id(+)
                 AND ooha.order_source_id = oos.order_source_id(+)
                 AND oos.NAME(+) = 'Internal'
                 AND req_ln.requisition_header_id = oola.source_document_id(+)
                 AND req_ln.requisition_line_id = oola.source_document_line_id(+)
                 AND NVL (oola.cancelled_flag(+), 'N') = 'N'
                 AND NVL (oola.booked_flag(+), 'N') = 'Y'
                 AND req_ln.source_organization_id = mp_from.organization_id
                 AND mp_from.organization_id = ood_from.organization_id
                 AND req_ln.destination_organization_id =
                                                         mp_to.organization_id
                 AND mp_to.organization_id = ood_to.organization_id
                 AND req_ln.item_id = msib.inventory_item_id
                 AND req_ln.destination_organization_id = msib.organization_id
                 AND ood_from.operating_unit = hou.organization_id
                 -- 06/25/2012 CG Changed to pull what is expected in the org
                 -- AND mp_from.organization_id = p_organization_id
                 AND mp_to.organization_id = p_organization_id
                 AND msib.inventory_item_id = p_inventory_item_id
              UNION
              -- Intransit Shipments
              SELECT mp_from.organization_code ship_from_branch_num,
                     mp_from.organization_id ship_from_org_id,
                     mp_to.organization_code ship_to_branch_num,
                     mp_to.organization_id ship_to_org_id,
                     rsl.shipment_line_status_code line_status,
                     rsh.creation_date ordered_date, rsh.shipped_date,
                     msib.segment1 sku_number, msib.inventory_item_id,
                     rsl.quantity_shipped ordered_qty,
                     rsl.quantity_shipped shipped_qty, 0 cancelled_qty,
                     rsl.quantity_shipped requested_qty,
                     NVL (rsl.quantity_received, 0) received_qty,
                     rsh.shipment_num po_ref_num
                FROM rcv_shipment_headers rsh,
                     rcv_shipment_lines rsl,
                     mtl_parameters mp_from,
                     org_organization_definitions ood_from,
                     mtl_parameters mp_to,
                     org_organization_definitions ood_to,
                     mtl_system_items_b msib,
                     hr_operating_units hou
               WHERE rsh.receipt_source_code = 'INVENTORY'
                 AND rsh.shipment_header_id = rsl.shipment_header_id
                 AND rsl.source_document_code = 'INVENTORY'
                 AND rsl.from_organization_id = mp_from.organization_id
                 AND mp_from.organization_id = ood_from.organization_id
                 AND rsl.to_organization_id = mp_to.organization_id
                 AND mp_to.organization_id = ood_to.organization_id
                 AND rsl.item_id = msib.inventory_item_id
                 AND rsl.to_organization_id = msib.organization_id
                 AND ood_from.operating_unit = hou.organization_id
                 -- 06/25/2012 CG Changed to pull what is expected in the org
                 -- AND mp_from.organization_id = p_organization_id
                 AND mp_to.organization_id = p_organization_id
                 AND msib.inventory_item_id = p_inventory_item_id) x1;

      RETURN (NVL (l_transit_qty, 0));
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN (0);
   END get_transit_qty;

   FUNCTION get_oldest_receipt_date (
      p_inventory_item_id   IN   NUMBER,
      p_organization_id     IN   NUMBER
   )
      RETURN DATE
   IS
      l_oldest_receipt   DATE;
   BEGIN
      l_oldest_receipt := NULL;

      SELECT MIN (rt.transaction_date)
        INTO l_oldest_receipt
        FROM rcv_transactions rt, rcv_shipment_lines rsl
       WHERE rt.transaction_type = 'RECEIVE'
         AND rt.organization_id = p_organization_id
         AND rt.shipment_line_id = rsl.shipment_line_id
         AND rsl.item_id = p_inventory_item_id;

      RETURN (l_oldest_receipt);
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN (NULL);
   END get_oldest_receipt_date;

   FUNCTION get_last_sales_date (
      p_inventory_item_id   IN   NUMBER,
      p_organization_id     IN   NUMBER
   )
      RETURN DATE
   IS
      l_last_sales_date   DATE;
   BEGIN
      l_last_sales_date := NULL;

      SELECT MAX (oola.actual_shipment_date)
        INTO l_last_sales_date
        FROM oe_order_lines_all oola
       WHERE oola.inventory_item_id = p_inventory_item_id
         AND oola.ship_from_org_id = p_organization_id
         AND NVL (oola.cancelled_flag, 'N') = 'N'
         AND oola.actual_shipment_date IS NOT NULL;

      RETURN (l_last_sales_date);
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN (NULL);
   END get_last_sales_date;

   FUNCTION get_last_count_date (
      p_inventory_item_id   IN   NUMBER,
      p_organization_id     IN   NUMBER
   )
      RETURN DATE
   IS
      l_last_count_date   DATE;
   BEGIN
      l_last_count_date := NULL;

      SELECT MAX (count_date_current)
        INTO l_last_count_date
        FROM mtl_cycle_count_entries mcce
       WHERE mcce.inventory_item_id = p_inventory_item_id
         AND mcce.organization_id = p_organization_id
         AND (   mcce.count_date_current IS NULL
              OR mcce.count_date_current =
                    (SELECT MAX (mcce2.count_date_current)
                       FROM mtl_cycle_count_entries mcce2
                      WHERE mcce2.inventory_item_id = mcce.inventory_item_id
                        AND mcce2.organization_id = mcce.organization_id)
             );

      RETURN l_last_count_date;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN (NULL);
   END get_last_count_date;

   FUNCTION get_last_count_qty (
      p_inventory_item_id   IN   NUMBER,
      p_organization_id     IN   NUMBER
   )
      RETURN NUMBER
   IS
      l_last_count_qty   NUMBER;
   BEGIN
      l_last_count_qty := NULL;

      SELECT   mcce.count_quantity_current
          INTO l_last_count_qty
          FROM mtl_cycle_count_entries mcce
         WHERE mcce.inventory_item_id = p_inventory_item_id
           AND mcce.organization_id = p_organization_id
           --and       rownum <= 1
           AND (   mcce.count_date_current IS NULL
                OR mcce.count_date_current =
                      (SELECT MAX (mcce2.count_date_current)
                         FROM mtl_cycle_count_entries mcce2
                        WHERE mcce2.inventory_item_id = mcce.inventory_item_id
                          AND mcce2.organization_id = mcce.organization_id)
               )
      ORDER BY count_date_current DESC;

      RETURN (NVL (l_last_count_qty, 0));
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN (0);
   END get_last_count_qty;

   FUNCTION get_last_counted_by (
      p_inventory_item_id   IN   NUMBER,
      p_organization_id     IN   NUMBER
   )
      RETURN VARCHAR2
   IS
      l_last_counted_by   VARCHAR2 (240);
   BEGIN
      l_last_counted_by := NULL;

      SELECT   papf.full_name
          INTO l_last_counted_by
          FROM mtl_cycle_count_entries mcce, per_all_people_f papf
         WHERE mcce.inventory_item_id = p_inventory_item_id
           AND mcce.organization_id = p_organization_id
           AND mcce.counted_by_employee_id_current = papf.person_id
           --and       rownum <= 1
           AND (   mcce.count_date_current IS NULL
                OR mcce.count_date_current =
                      (SELECT MAX (mcce2.count_date_current)
                         FROM mtl_cycle_count_entries mcce2
                        WHERE mcce2.inventory_item_id = mcce.inventory_item_id
                          AND mcce2.organization_id = mcce.organization_id)
               )
      ORDER BY count_date_current DESC;

      RETURN (l_last_counted_by);
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN (NULL);
   END get_last_counted_by;

   FUNCTION get_cat_segment_label (
      p_structure_id   IN   NUMBER,
      p_cat_segment    IN   VARCHAR2,
      p_cat_value      IN   VARCHAR2
   )
      RETURN VARCHAR2
   IS
      l_cat_segment_label   VARCHAR2 (240);
   BEGIN
      l_cat_segment_label := NULL;

      SELECT fifs.segment_name
        INTO l_cat_segment_label
        FROM fnd_id_flex_segments fifs,
             fnd_flex_values ffvb,
             fnd_flex_values_tl ffvt
       WHERE fifs.id_flex_num = p_structure_id
         AND fifs.application_column_name = p_cat_segment
         AND fifs.application_id = 401
         AND ffvb.flex_value = p_cat_value
         AND fifs.flex_value_set_id = ffvb.flex_value_set_id
         AND ffvb.flex_value_id = ffvt.flex_value_id;

      RETURN (l_cat_segment_label);
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN (NULL);
   END get_cat_segment_label;

   FUNCTION get_cat_segment_desc (
      p_structure_id   IN   NUMBER,
      p_cat_segment    IN   VARCHAR2,
      p_cat_value      IN   VARCHAR2
   )
      RETURN VARCHAR2
   IS
      l_cat_segment_desc   VARCHAR2 (240);
   BEGIN
      l_cat_segment_desc := NULL;

      SELECT ffvt.description
        INTO l_cat_segment_desc
        FROM fnd_id_flex_segments fifs,
             fnd_flex_values ffvb,
             fnd_flex_values_tl ffvt
       WHERE fifs.id_flex_num = p_structure_id
         AND fifs.application_column_name = p_cat_segment
         AND fifs.application_id = 401
         AND ffvb.flex_value = p_cat_value
         AND fifs.flex_value_set_id = ffvb.flex_value_set_id
         AND ffvb.flex_value_id = ffvt.flex_value_id;

      RETURN (l_cat_segment_desc);
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN (NULL);
   END get_cat_segment_desc;

   FUNCTION get_cust_credit_limit (
      p_cust_account_id   IN   NUMBER,
      p_site_use_id       IN   NUMBER
   )
      RETURN NUMBER
   IS
      l_cust_credit_limit   NUMBER;
   BEGIN
      l_cust_credit_limit := NULL;

      BEGIN
         SELECT   overall_credit_limit
             INTO l_cust_credit_limit
             FROM hz_cust_profile_amts
            WHERE cust_account_id = p_cust_account_id
              AND site_use_id = p_site_use_id
              AND ROWNUM = 1
         ORDER BY site_use_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_cust_credit_limit := NULL;
      END;

      IF l_cust_credit_limit IS NULL
      THEN
         SELECT   overall_credit_limit
             INTO l_cust_credit_limit
             FROM hz_cust_profile_amts
            WHERE cust_account_id = p_cust_account_id
              AND site_use_id IS NULL
              AND ROWNUM = 1
         ORDER BY site_use_id;
      END IF;

      RETURN (l_cust_credit_limit);
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN (NULL);
   END get_cust_credit_limit;

   FUNCTION get_credit_manager (
      p_cust_account_id   IN   NUMBER,
      p_site_use_id       IN   NUMBER
   )
      RETURN NUMBER
   IS
      l_credit_manager   NUMBER;
   BEGIN
      l_credit_manager := NULL;

      BEGIN
         SELECT   credit_analyst_id
             INTO l_credit_manager
             FROM hz_customer_profiles
            WHERE cust_account_id = p_cust_account_id
              AND site_use_id = p_site_use_id
              AND ROWNUM = 1
         ORDER BY site_use_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_credit_manager := NULL;
      END;

      IF l_credit_manager IS NULL
      THEN
         SELECT   credit_analyst_id
             INTO l_credit_manager
             FROM hz_customer_profiles
            WHERE cust_account_id = p_cust_account_id
              AND site_use_id IS NULL
              AND ROWNUM = 1
         ORDER BY site_use_id;
      END IF;

      RETURN (l_credit_manager);
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN (NULL);
   END get_credit_manager;

   FUNCTION get_contact_name (p_contact_id IN NUMBER)
      RETURN VARCHAR2
   IS
      l_contact_name   VARCHAR2 (240);
   BEGIN
      l_contact_name := NULL;

      SELECT party.party_name
        INTO l_contact_name
        FROM hz_cust_account_roles acct_role,
             hz_parties party,
             hz_relationships rel,
             hz_org_contacts org_cont
       WHERE acct_role.cust_account_role_id = p_contact_id
         AND acct_role.party_id = rel.party_id
         AND acct_role.role_type = 'CONTACT'
         AND org_cont.party_relationship_id = rel.relationship_id
         AND rel.subject_table_name = 'HZ_PARTIES'
         AND rel.object_table_name = 'HZ_PARTIES'
         AND rel.directional_flag = 'F'
         AND rel.subject_id = party.party_id;

      RETURN (l_contact_name);
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN (NULL);
   END get_contact_name;

   FUNCTION get_last_po_price (
      p_inventory_item_id   IN   NUMBER,
      p_organization_id     IN   NUMBER
   )
      RETURN NUMBER
   IS
      l_last_po_price   NUMBER;
   BEGIN
      l_last_po_price := NULL;

      /*select    max(pla.unit_price) keep (dense_rank last order by creation_date) last_unit_price
      into    l_last_po_price
      from    po_lines_all pla
      where   pla.item_id = p_inventory_item_id
      and     nvl(pla.cancel_flag, 'N') = 'N';*/
      SELECT pol.unit_price
        INTO l_last_po_price
        FROM rcv_transactions rt,
             rcv_shipment_headers rsh,
             rcv_shipment_lines rsl,
             po_headers_all poh,
             po_lines_all pol,
             po_line_locations_all poll
       WHERE rsh.shipment_header_id = rt.shipment_header_id
         AND rsl.shipment_line_id = rt.shipment_line_id
         AND rt.transaction_type = 'DELIVER'
         AND poh.po_header_id = rt.po_header_id
         AND pol.po_line_id = rt.po_line_id
         AND poll.line_location_id = rt.po_line_location_id
         AND rsl.item_id = p_inventory_item_id
         AND rsl.to_organization_id = p_organization_id
         AND rt.transaction_date =
                (SELECT MAX (rt.transaction_date)
                   FROM rcv_transactions rt1,
                        rcv_shipment_headers rsh1,
                        rcv_shipment_lines rsl1
                  WHERE rsh1.shipment_header_id = rt1.shipment_header_id
                    AND rsl1.shipment_line_id = rt1.shipment_line_id
                    AND rt1.transaction_type = 'DELIVER'
                    AND rsl1.item_id = p_inventory_item_id
                    AND rsl1.to_organization_id = p_organization_id);

      RETURN (l_last_po_price);
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN (0);
   END get_last_po_price;

   FUNCTION get_first_sale_date (p_cust_account_id IN NUMBER)
      RETURN DATE
   IS
      l_first_sale_date   DATE;
   BEGIN
      l_first_sale_date := NULL;

      SELECT MAX (ooha.ordered_date)KEEP (DENSE_RANK FIRST ORDER BY ordered_date)
                                                             first_order_date
        INTO l_first_sale_date
        FROM oe_order_headers_all ooha
       WHERE ooha.sold_to_org_id = p_cust_account_id
         AND NVL (ooha.cancelled_flag, 'N') = 'N'
         AND NVL (ooha.booked_flag, 'N') = 'Y';

      RETURN l_first_sale_date;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN (NULL);
   END get_first_sale_date;

   FUNCTION get_last_sale_date (p_cust_account_id IN NUMBER)
      RETURN DATE
   IS
      l_last_sale_date   DATE;
   BEGIN
      l_last_sale_date := NULL;

      SELECT MAX (ooha.ordered_date)KEEP (DENSE_RANK LAST ORDER BY ordered_date)
                                                              last_order_date
        INTO l_last_sale_date
        FROM oe_order_headers_all ooha
       WHERE ooha.sold_to_org_id = p_cust_account_id
         AND NVL (ooha.cancelled_flag, 'N') = 'N'
         AND NVL (ooha.booked_flag, 'N') = 'Y';

      RETURN l_last_sale_date;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN (NULL);
   END get_last_sale_date;

   FUNCTION get_ar_term_desc (p_term_id IN NUMBER)
      RETURN VARCHAR2
   IS
      l_ar_payment_term   VARCHAR2 (240);
   BEGIN
      l_ar_payment_term := NULL;

      SELECT NAME
        INTO l_ar_payment_term
        FROM ra_terms_tl
       WHERE term_id = p_term_id AND ROWNUM = 1;

      RETURN (l_ar_payment_term);
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN (NULL);
   END get_ar_term_desc;

   /*FUNCTION GET_HIGH_CREDIT_YTD(p_party_id IN NUMBER,
                                  p_cust_account_id IN NUMBER,
                                  p_customer_site_use_id IN NUMBER)
   RETURN VARCHAR2 IS*/
   FUNCTION get_high_credit_ytd (p_cust_account_id IN NUMBER)
      RETURN NUMBER
   IS
      l_high_credit_ytd   NUMBER;
      l_num_val           NUMBER;
      l_char_val          VARCHAR2 (1000);
      l_conversion_type   VARCHAR (30);
      l_currency_code     VARCHAR2 (30);
   BEGIN
      l_conversion_type :=
              NVL (fnd_profile.VALUE ('IEX_EXCHANGE_RATE_TYPE'), 'Corporate');
      l_currency_code :=
             NVL (fnd_profile.VALUE ('AR_CMGT_DEFAULT_CURRENCY_CODE'), 'USD');

      /*IF p_party_id IS NOT NULL THEN

          SELECT MAX(gl_currency_api.convert_amount_sql(trx_summ.currency, l_currency_code,
                  sysdate, l_conversion_type , trx_summ.op_bal_high_watermark))
          INTO l_high_credit_ytd
          FROM ar_trx_summary trx_summ
              , ar_system_parameters_all asp
              , hz_cust_accounts ca
          WHERE trx_summ.org_id=asp.org_id
          AND trx_summ.cust_account_id = ca.cust_account_id
          AND ca.party_id = p_party_id;

      ELSIF p_cust_account_id IS NOT NULL THEN*/
      SELECT MAX
                (gl_currency_api.convert_amount_sql
                                               (trx_summ.currency,
                                                l_currency_code,
                                                SYSDATE,
                                                l_conversion_type,
                                                trx_summ.op_bal_high_watermark
                                               )
                )
        INTO l_high_credit_ytd
        FROM ar_trx_summary trx_summ, ar_system_parameters_all asp
       WHERE trx_summ.org_id = asp.org_id
         AND trx_summ.cust_account_id = p_cust_account_id;

      /*ELSIF p_customer_site_use_id IS NOT NULL THEN

          SELECT MAX(gl_currency_api.convert_amount_sql(trx_summ.currency, l_currency_code,
             sysdate, l_conversion_type , trx_summ.op_bal_high_watermark))
          INTO l_high_credit_ytd
          FROM ar_trx_summary trx_summ,ar_system_parameters_all asp
          WHERE trx_summ.org_id=asp.org_id
          AND trx_summ.site_use_id = p_customer_site_use_id;

      END IF;*/
      l_num_val := NVL (l_high_credit_ytd, 0);
      --l_char_val := RTRIM(TO_CHAR(l_num_val, fnd_currency.get_format_mask(l_currency_code, 50)));
      RETURN l_num_val;                                          --l_char_val;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN (0);
   END get_high_credit_ytd;

   /*FUNCTION GET_HIGH_CREDIT_DATE_YTD(p_party_id IN NUMBER,
                                  p_cust_account_id IN NUMBER,
                                  p_customer_site_use_id IN NUMBER)
   RETURN DATE IS*/
   FUNCTION get_high_credit_date_ytd (p_cust_account_id IN NUMBER)
      RETURN DATE
   IS
      l_high_credit_ytd   NUMBER;
      l_num_val           NUMBER;
      l_char_val          VARCHAR2 (1000);
      l_conversion_type   VARCHAR (30);
      l_currency_code     VARCHAR2 (30);
      l_high_date         DATE;
   BEGIN
      l_conversion_type :=
              NVL (fnd_profile.VALUE ('IEX_EXCHANGE_RATE_TYPE'), 'Corporate');
      l_currency_code :=
             NVL (fnd_profile.VALUE ('AR_CMGT_DEFAULT_CURRENCY_CODE'), 'USD');

      /*IF p_party_id IS NOT NULL THEN

          SELECT MAX(gl_currency_api.convert_amount_sql(trx_summ.currency, l_currency_code,
                  sysdate, l_conversion_type , trx_summ.op_bal_high_watermark))
                 , trx_summ.op_bal_high_watermark_date
          INTO l_high_credit_ytd
                  , l_high_date
          FROM ar_trx_summary trx_summ
              , ar_system_parameters_all asp
              , hz_cust_accounts ca
          WHERE trx_summ.org_id=asp.org_id
          AND trx_summ.cust_account_id = ca.cust_account_id
          AND ca.party_id = p_party_id;

      ELSIF p_cust_account_id IS NOT NULL THEN

          SELECT MAX(gl_currency_api.convert_amount_sql(trx_summ.currency, l_currency_code,
                  sysdate, l_conversion_type , trx_summ.op_bal_high_watermark))
                  , trx_summ.op_bal_high_watermark_date
          INTO l_high_credit_ytd
                  , l_high_date
          FROM ar_trx_summary trx_summ,ar_system_parameters_all asp
          WHERE trx_summ.org_id=asp.org_id
          AND trx_summ.cust_account_id = p_cust_account_id;

      ELSIF p_customer_site_use_id IS NOT NULL THEN

          SELECT MAX(gl_currency_api.convert_amount_sql(trx_summ.currency, l_currency_code,
             sysdate, l_conversion_type , trx_summ.op_bal_high_watermark))
             , trx_summ.op_bal_high_watermark_date
          INTO l_high_credit_ytd
                  , l_high_date
          FROM ar_trx_summary trx_summ,ar_system_parameters_all asp
          WHERE trx_summ.org_id=asp.org_id
          AND trx_summ.site_use_id = p_customer_site_use_id;

      END IF;*/
      SELECT op_bal_high_watermark_date
        INTO l_high_date
        FROM ar_trx_summary trx_summ, ar_system_parameters_all asp
       WHERE trx_summ.org_id = asp.org_id
         AND trx_summ.cust_account_id = p_cust_account_id
         AND (trx_summ.cust_account_id, trx_summ.op_bal_high_watermark) =
                (SELECT   trx_summ2.cust_account_id,
                          MAX (trx_summ2.op_bal_high_watermark)
                     FROM ar_trx_summary trx_summ2,
                          ar_system_parameters_all asp2
                    WHERE trx_summ2.org_id = asp.org_id
                      AND trx_summ2.cust_account_id = p_cust_account_id
                 GROUP BY trx_summ2.cust_account_id);

      RETURN l_high_date;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN (NULL);
   END get_high_credit_date_ytd;

   FUNCTION get_last_ar_trx_date (p_cust_account_id IN NUMBER)
      RETURN DATE
   IS
      l_last_ar_trx_date   DATE;
   BEGIN
      l_last_ar_trx_date := NULL;

      SELECT MAX (rcta.trx_date)
        INTO l_last_ar_trx_date
        FROM ra_customer_trx_all rcta
       WHERE rcta.bill_to_customer_id = p_cust_account_id;

      RETURN (l_last_ar_trx_date);
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN (NULL);
   END get_last_ar_trx_date;

   FUNCTION get_cust_sales (p_cust_account_id IN NUMBER)
      RETURN NUMBER
   IS
      l_cust_sales      NUMBER;
      l_currency_code   VARCHAR2 (30);
   BEGIN
      l_currency_code :=
             NVL (fnd_profile.VALUE ('AR_CMGT_DEFAULT_CURRENCY_CODE'), 'USD');

      /*IF p_party_id IS NOT NULL THEN

          SELECT  SUM(arpcurr.functional_amount(ps.amount_due_original,
                                                  g_curr_rec.base_currency,
                                                  nvl(ps.exchange_rate,1),
                                                  g_curr_rec.base_precision,
                                                  g_curr_rec.base_min_acc_unit)
                  + GET_ADJ_FOR_TOT_REC(ps.payment_schedule_id,p_end_date))
          INTO    l_tot_rec
          FROM    ar_payment_schedules   ps,
                  hz_cust_accounts       ca
          WHERE   ps.class in ('INV', 'DM', 'CB', 'DEP' )
          AND     ps.payment_schedule_id <> -1
          AND     ps.gl_date BETWEEN l_temp_start AND p_end_date
          AND     ps.customer_id = ca.cust_account_id
          AND     ca.party_id = p_party_id;

      ELSIF p_cust_account_id IS NOT NULL THEN*/
      SELECT SUM
                (  arpcurr.functional_amount
                                           (ps.amount_due_original,
                                            l_currency_code,
                                            NVL (ps.exchange_rate, 1),
                                            (SELECT PRECISION
                                               FROM fnd_currencies
                                              WHERE currency_code =
                                                               l_currency_code),
                                            (SELECT minimum_accountable_unit
                                               FROM fnd_currencies
                                              WHERE currency_code =
                                                               l_currency_code)
                                           )
                 + NVL ((SELECT SUM (NVL (a.acctd_amount, 0))
                           FROM ar_adjustments_all a
                          WHERE a.payment_schedule_id = ps.payment_schedule_id
                            AND a.status = 'A'
                            AND a.gl_date <= TRUNC (SYSDATE)),
                        0
                       )
                )
        INTO l_cust_sales
        FROM ar_payment_schedules_all ps
       WHERE ps.CLASS IN ('INV', 'DM', 'CB', 'DEP')
         AND ps.payment_schedule_id <> -1
         AND ps.gl_date BETWEEN TRUNC (ADD_MONTHS (SYSDATE, -12))
                            AND TRUNC (SYSDATE)
         AND ps.customer_id = p_cust_account_id;

      /*ELSIF p_customer_site_use_id IS NOT NULL THEN

          SELECT  SUM(arpcurr.functional_amount(ps.amount_due_original,
                                                g_curr_rec.base_currency,
                                                nvl(ps.exchange_rate,1),
                                                g_curr_rec.base_precision,
                                                g_curr_rec.base_min_acc_unit)
                  + GET_ADJ_FOR_TOT_REC(ps.payment_schedule_id,p_end_date))
          INTO    l_tot_rec
          FROM    ar_payment_schedules   ps
          WHERE   ps.class in ('INV', 'DM', 'CB', 'DEP' )
          AND     ps.payment_schedule_id <> -1
          AND     ps.gl_date BETWEEN l_temp_start AND p_end_date
          AND     ps.customer_site_use_id = p_customer_site_use_id;

      END IF;*/
      RETURN (NVL (l_cust_sales, 0));
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN (0);
   END get_cust_sales;

   FUNCTION get_avg_days_late (p_cust_account_id IN NUMBER)
      RETURN NUMBER
   IS
      l_avg_days_late   NUMBER;
   BEGIN
      l_avg_days_late := NULL;

      /*IF p_party_id IS NOT NULL THEN

          SELECT  sum(TRUNC(sysdate) - ps.due_date) / COUNT(1)
          INTO    l_avg_days_late
          FROM    ar_payment_schedules ps
                  , hz_cust_accounts ca
          WHERE   ps.gl_date between TRUNC(add_months(sysdate, -12)) and  TRUNC(sysdate)
          AND     ps.class in ('INV','DEP','DM','CB')
          AND     ps.gl_date_closed > TRUNC(sysdate)
          AND     ps.status = 'OP'
          AND     ps.due_date       < TRUNC(sysdate)
          AND     ps.payment_schedule_id <> -1
          AND     ca.cust_account_id = ps.customer_id
          AND     ca.party_id = p_party_id;

      ELSIF p_cust_account_id IS NOT NULL THEN*/
      SELECT SUM (TRUNC (SYSDATE) - ps.due_date) / COUNT (1)
        INTO l_avg_days_late
        FROM ar_payment_schedules ps
       WHERE ps.gl_date BETWEEN TRUNC (ADD_MONTHS (SYSDATE, -12))
                            AND TRUNC (SYSDATE)
         AND ps.CLASS IN ('INV', 'DEP', 'DM', 'CB')
         AND ps.gl_date_closed > TRUNC (SYSDATE)
         AND ps.status = 'OP'
         AND ps.due_date < TRUNC (SYSDATE)
         AND ps.payment_schedule_id <> -1
         AND ps.customer_id = p_cust_account_id;

      /*ELSIF p_customer_site_use_id IS NOT NULL THEN

          SELECT  sum(TRUNC(sysdate) - ps.due_date) / COUNT(1)
          INTO    l_avg_days_late
          FROM    ar_payment_schedules ps
          WHERE   ps.gl_date between TRUNC(add_months(sysdate, -12)) and  TRUNC(sysdate)
          AND     ps.class in ('INV','DEP','DM','CB')
          AND     ps.gl_date_closed > TRUNC(sysdate)
          AND     ps.status = 'OP'
          AND     ps.due_date       < TRUNC(sysdate)
          AND     ps.payment_schedule_id <> -1
          AND     ps.customer_site_use_id = p_customer_site_use_id;

      END IF;*/
      RETURN (NVL (l_avg_days_late, 0));
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN (NULL);
   END get_avg_days_late;

   FUNCTION get_order_shp_from_org_id (p_order_number IN VARCHAR2)
      RETURN NUMBER
   IS
      l_ship_from_org_id   NUMBER;
   BEGIN
      l_ship_from_org_id := NULL;

      --if nvl(p_invoice_batch_source, 'UNKNOWN') in ('ORDER MANAGEMENT','STANDARD OM SOURCE') then
      SELECT ooha.ship_from_org_id
        INTO l_ship_from_org_id
        FROM oe_order_headers_all ooha
       WHERE ooha.order_number = TO_NUMBER (p_order_number) AND ROWNUM = 1;

      --else
          --l_ship_from_org_id := null;
      --end if;
      RETURN (l_ship_from_org_id);
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN (NULL);
   END get_order_shp_from_org_id;

   FUNCTION get_tax_rate (
      p_customer_trx_id            IN   NUMBER,
      p_link_to_cust_trx_line_id   IN   NUMBER
   )
      RETURN NUMBER
   IS
      l_tax_rate            NUMBER;
      l_count_taxed_lines   NUMBER;
   BEGIN
      l_tax_rate := NULL;
      l_count_taxed_lines := NULL;

      /*select    nvl(x1.final_tax_rate, 0)
      into    l_tax_rate
      from    (
                  select  customer_trx_id
                          , link_to_cust_trx_line_id
                          , sum(tax_rate) final_tax_rate
                  from    ra_customer_trx_lines_all
                  where   line_type = 'TAX'
                  and     customer_trx_id = p_customer_trx_id
                  and     link_to_cust_trx_line_id = nvl(p_link_to_cust_trx_line_id, link_to_cust_trx_line_id)
                  group by customer_trx_id
                          , link_to_cust_trx_line_id
                  order by 1,2) x1
      where   rownum = 1;*/
      SELECT   SUM (tax_rate), COUNT (DISTINCT (link_to_cust_trx_line_id))
          INTO l_tax_rate, l_count_taxed_lines
          FROM ra_customer_trx_lines_all
         WHERE line_type = 'TAX'
           AND customer_trx_id = p_customer_trx_id
           AND link_to_cust_trx_line_id =
                    NVL (p_link_to_cust_trx_line_id, link_to_cust_trx_line_id)
           AND tax_rate > 0
      GROUP BY customer_trx_id;

      IF p_customer_trx_id IS NOT NULL AND p_link_to_cust_trx_line_id IS NULL
      THEN
         IF NVL (l_count_taxed_lines, 0) > 0
         THEN
            l_tax_rate := l_tax_rate / l_count_taxed_lines;
         END IF;
      END IF;

      RETURN (NVL (l_tax_rate, 0));
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN (0);
   END get_tax_rate;

   FUNCTION get_tax_amount (
      p_customer_trx_id            IN   NUMBER,
      p_link_to_cust_trx_line_id   IN   NUMBER
   )
      RETURN NUMBER
   IS
      l_tax_amount   NUMBER;
   BEGIN
      l_tax_amount := NULL;

      SELECT SUM (extended_amount)
        INTO l_tax_amount
        FROM ra_customer_trx_lines_all
       WHERE line_type = 'TAX'
         AND customer_trx_id = p_customer_trx_id
         AND link_to_cust_trx_line_id =
                    NVL (p_link_to_cust_trx_line_id, link_to_cust_trx_line_id);

      RETURN (l_tax_amount);
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN (0);
   END get_tax_amount;

   FUNCTION get_user_employee (p_user_id IN NUMBER)
      RETURN VARCHAR
   IS
      l_employee_name   VARCHAR2 (240);
   BEGIN
      l_employee_name := NULL;

      SELECT papf.full_name
        INTO l_employee_name
        FROM fnd_user fu, per_all_people_f papf
       WHERE fu.user_id = p_user_id
         AND fu.employee_id = papf.person_id
         AND TRUNC (papf.effective_start_date(+)) <= TRUNC (SYSDATE)
         AND NVL (TRUNC (papf.effective_end_date(+)), TRUNC (SYSDATE)) >=
                                                               TRUNC (SYSDATE)
         AND ROWNUM = 1;

      RETURN (l_employee_name);
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN (NULL);
   END get_user_employee;

   FUNCTION get_rental_ship_date (p_line_id IN NUMBER,p_link_to_line_id IN NUMBER)    --<<Added new parameter p_link_to_line_id for Ver3.0
      RETURN DATE
  /*************************************************************************
    Copyright (c) 2012 Lucidity Consulting Group
    All rights reserved.
   **************************************************************************
     $Header xxwc_mv_routines_pkg $
     Function Name: get_rental_ship_date

     PURPOSE:   To get rental start date

     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.0        01/11/2012  Consuelo Gonzalez      Initial Version
     3.0        05/29/2015  Maharajan Shunmugam    TMS20150527-00031 OM Rental Receipt Bug Fix
   **************************************************************************/

   IS
      l_shipment_date      DATE;
      l_rental_type        VARCHAR2 (240);
      l_rental_period      NUMBER;
      l_line_rental_term   VARCHAR2 (240);
--<<Start Added below for Ver3.0
      l_max_lines       NUMBER;
      l_max_cycle       NUMBER; 
      l_split_from_line_id NUMBER ;
--<<End
   BEGIN
      -- 07/16/2012 CG Modified to pull dates for rental based on change to use DFFs
      l_shipment_date := NULL;
      l_rental_type := NULL;
      l_rental_period := NULL;
      l_line_rental_term := NULL;

      BEGIN
         SELECT NVL( to_date(shp_ln.attribute12,'YYYY/MM/DD HH24:MI:SS'), shp_ln.actual_shipment_date ),
                ooha.attribute1,
                DECODE (oola.attribute9, 0, 1, oola.attribute9),
                shp_ln.attribute2
           INTO l_shipment_date, l_rental_type,
                l_rental_period,
                l_line_rental_term
           FROM oe_order_lines_all oola,
                oe_order_lines_all rma_ln,
                oe_order_lines_all shp_ln,
                oe_order_headers_all ooha
          WHERE oola.line_id = p_line_id
            AND oola.link_to_line_id = rma_ln.line_id
            AND rma_ln.line_category_code = 'RETURN'
            AND rma_ln.link_to_line_id = shp_ln.line_id
            AND shp_ln.line_category_code = 'ORDER'
            AND shp_ln.header_id = ooha.header_id
            AND ROWNUM = 1;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_shipment_date := NULL;
      END;

      IF l_shipment_date IS NULL
      THEN
         BEGIN
            SELECT NVL(to_date(oola.attribute12,'YYYY/MM/DD HH24:MI:SS'), oola.actual_shipment_date)
              INTO l_shipment_date
              FROM oe_order_lines_all oola
             WHERE oola.line_id = p_line_id AND ROWNUM = 1;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_shipment_date := NULL;
         END;
      END IF;

      IF     NVL (l_rental_type, 'Unknown') = 'Long Term'
         AND l_shipment_date IS NOT NULL
         AND l_rental_period IS NOT NULL            --Added for Ver3.0
      THEN      
        -- 10/10/12 CG: Changed from 29 to 28
         -- l_shipment_date := l_shipment_date + ((l_rental_period - 1) * 29);
         l_shipment_date := l_shipment_date + ((l_rental_period - 1) * 28);
 -- <<START Added for Ver3.0 
       ELSIF NVL (l_rental_type, 'Unknown') = 'Long Term'
         AND l_shipment_date IS NOT NULL
         AND l_rental_period is NULL
      THEN
          IF (get_rental_return_date(p_line_id) - l_shipment_date)  > 28 THEN
          l_max_cycle := 0;          
          begin
              select    max(to_number(ol.attribute9))
              into      l_max_cycle
              from        oe_order_lines_all ol 
              where        ol.link_to_line_id = p_link_to_line_id 
              and            ol.attribute9 is not null;
              
  BEGIN
   IF l_max_cycle IS NULL THEN
     BEGIN
        SELECT NVL(split_from_line_id,0)
       INTO l_split_from_line_id
    FROM oe_order_lines_all
    WHERE line_id = p_link_to_line_id ;
        
         BEGIN    
          SELECT NVL(MAX (TO_NUMBER (ol.attribute9)),0)
            INTO l_max_cycle
          FROM oe_order_lines ol
           WHERE ol.link_to_line_id = l_split_from_line_id 
             AND ol.attribute9 IS NOT NULL;
      EXCEPTION 
          WHEN OTHERS THEN
           l_max_cycle :=0;
         END;                                                                          
                      
      EXCEPTION 
       WHEN OTHERS THEN
         l_split_from_line_id := 0;
     l_max_cycle :=0;
      END;
    END IF;                                    
  END;
  
 l_max_cycle := l_max_cycle + 1;

 EXCEPTION
  WHEN OTHERS THEN
   l_max_cycle := l_max_cycle + 1;
 END;
          
   l_shipment_date := l_shipment_date + ((l_max_cycle - 1) * 28);
          
   ELSE
    NULL;
 END IF;
--<<END Version 3.0

 END IF;

      RETURN (l_shipment_date);
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN (NULL);
   END get_rental_ship_date;

   FUNCTION get_rental_return_date (p_line_id IN NUMBER)
      RETURN DATE
   IS
      l_return_date          DATE;
      l_rental_type          VARCHAR2 (240);
      l_rental_period        NUMBER;
      l_line_rental_term     VARCHAR2 (240);
      l_original_ship_date   DATE;
   BEGIN
      -- 07/16/2012 CG Modified to pull dates for rental based on change to use DFFs
      l_return_date := NULL;
      l_rental_type := NULL;
      l_rental_period := NULL;
      l_line_rental_term := NULL;
      l_original_ship_date := NULL;

      BEGIN
         SELECT NVL( to_date(rma_ln.attribute12,'YYYY/MM/DD HH24:MI:SS'), rma_ln.actual_shipment_date),
                ooha.attribute1,
                DECODE (oola.attribute9, 0, 1, oola.attribute9),
                shp_ln.attribute2,
                NVL( to_date(shp_ln.attribute12,'YYYY/MM/DD HH24:MI:SS'), shp_ln.actual_shipment_date)
           INTO l_return_date,
           l_rental_type,
                l_rental_period,
                l_line_rental_term,
                l_original_ship_date
           FROM oe_order_lines_all oola,
                oe_order_lines_all rma_ln,
                oe_order_lines_all shp_ln,
                oe_order_headers_all ooha
          WHERE oola.line_id = p_line_id
            AND oola.link_to_line_id = rma_ln.line_id
            AND rma_ln.line_category_code = 'RETURN'
            AND rma_ln.link_to_line_id = shp_ln.line_id
            AND shp_ln.line_category_code = 'ORDER'
            AND shp_ln.header_id = ooha.header_id
            AND ROWNUM = 1;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_return_date := NULL;
      END;

      IF l_return_date IS NULL
      THEN
         BEGIN
            SELECT NVL( to_date(rma_ln.attribute12,'YYYY/MM/DD HH24:MI:SS'), rma_ln.actual_shipment_date)
              INTO l_return_date
              FROM oe_order_lines_all oola, oe_order_lines_all rma_ln
             WHERE oola.line_id = p_line_id
               AND oola.line_id = rma_ln.link_to_line_id
               AND rma_ln.line_category_code = 'RETURN'
               AND NVL( to_date(rma_ln.attribute12,'YYYY/MM/DD HH24:MI:SS'), rma_ln.actual_shipment_date) IS NOT NULL
               AND ROWNUM = 1;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_return_date := NULL;
         END;
      END IF;

      IF NVL (l_rental_type, 'Unknown') = 'Long Term' AND l_rental_period is not null
      THEN
         l_return_date := ( ( l_original_ship_date + ( (l_rental_period) * 28) ) - 1 );
      END IF;

      RETURN (l_return_date);
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN (NULL);
   END get_rental_return_date;

   FUNCTION get_bt_trx_salesrep (
      p_trx_salesrep_id       IN   NUMBER,
      p_bill_to_salesrep_id   IN   NUMBER,
      p_org_id                IN   NUMBER
   )
      RETURN VARCHAR2
   IS
      l_salesrep_name   VARCHAR2 (240);
   BEGIN
      l_salesrep_name := NULL;

      -- Pull sales rep for transaction
      BEGIN
         SELECT NVL (jrs.NAME, papf.full_name)
           INTO l_salesrep_name
           FROM jtf.jtf_rs_salesreps jrs, hr.per_all_people_f papf
          WHERE jrs.salesrep_id = p_trx_salesrep_id
            AND jrs.org_id = p_org_id
            AND jrs.person_id = papf.person_id(+)
            AND TRUNC (papf.effective_start_date(+)) <= TRUNC (SYSDATE)
            AND NVL (TRUNC (papf.effective_end_date(+)), TRUNC (SYSDATE)) >=
                                                               TRUNC (SYSDATE);
      EXCEPTION
         WHEN OTHERS
         THEN
            l_salesrep_name := NULL;
      END;

      IF NVL (l_salesrep_name, 'No Sales Credit') = 'No Sales Credit'
      THEN
         -- Pull sales rep for bill to
         BEGIN
            SELECT NVL (jrs.NAME, papf.full_name)
              INTO l_salesrep_name
              FROM jtf.jtf_rs_salesreps jrs, hr.per_all_people_f papf
             WHERE jrs.salesrep_id = p_bill_to_salesrep_id
               AND jrs.org_id = p_org_id
               AND jrs.person_id = papf.person_id(+)
               AND TRUNC (papf.effective_start_date(+)) <= TRUNC (SYSDATE)
               AND NVL (TRUNC (papf.effective_end_date(+)), TRUNC (SYSDATE)) >=
                                                               TRUNC (SYSDATE);
         EXCEPTION
            WHEN OTHERS
            THEN
               l_salesrep_name := NULL;
         END;
      END IF;

      IF NVL (l_salesrep_name, 'No Sales Credit') = 'No Sales Credit'
      THEN
         l_salesrep_name := NULL;
      END IF;

      RETURN (l_salesrep_name);
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN (NULL);
   END get_bt_trx_salesrep;

   FUNCTION get_inv_adj_amount (
      p_customer_trx_id        IN   NUMBER,
      p_customer_trx_line_id   IN   NUMBER,
      p_org_id                 IN   NUMBER
   )
      RETURN NUMBER
   IS
      l_adjustment_amount   NUMBER;
   BEGIN
      SELECT SUM (amount)
        INTO l_adjustment_amount
        FROM ar_adjustments_all
       WHERE customer_trx_id = p_customer_trx_id
         AND customer_trx_line_id =
                            NVL (p_customer_trx_line_id, customer_trx_line_id)
         AND org_id = p_org_id;

      RETURN l_adjustment_amount;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 0;
   END get_inv_adj_amount;

   FUNCTION get_mst_organization_id (p_operating_unit_id IN NUMBER)
      RETURN NUMBER
   IS
      l_org_id   NUMBER;
   BEGIN
      l_org_id := NULL;

      SELECT DISTINCT (ood.organization_id)
                 INTO l_org_id
                 FROM org_organization_definitions ood, mtl_parameters mp
                WHERE ood.operating_unit = p_operating_unit_id
                  AND ood.organization_id = mp.master_organization_id
                  AND ROWNUM = 1;

      RETURN l_org_id;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 222;
   END;

--03/07/2014:Harsha--TMS 20140306-00133  --EDW - Incorrect RVP Reporting for DMs

   FUNCTION get_salesrep_areamgr_info (p_salesrep_num IN VARCHAR2)
      RETURN VARCHAR2
   IS
      l_salesrep_name   VARCHAR2 (240);
      l_reg_mgr         VARCHAR2 (240);
   BEGIN
      l_salesrep_name := NULL;
      l_reg_mgr:=null;


       begin
        select REGIONALMANAGER_HREMPLOYEEID into l_reg_mgr from xxwc.dw_salesreps where HREMPLOYEEID=p_salesrep_num;
        exception
        when others then
          return null;
            end;

      SELECT NVL (NVL (jrs.NAME, papf.full_name), jrre.resource_name)
        INTO l_salesrep_name
        FROM jtf_rs_salesreps jrs,
             jtf_rs_defresources_v jrre,
             per_all_people_f papf
       WHERE jrs.salesrep_number = l_reg_mgr
         AND jrs.resource_id = jrre.resource_id(+)
         AND jrs.person_id = papf.person_id(+)
         AND TRUNC (papf.effective_start_date(+)) <= TRUNC (SYSDATE)
         AND NVL (TRUNC (papf.effective_end_date(+)), TRUNC (SYSDATE)) >=
                                                               TRUNC (SYSDATE);

      RETURN l_salesrep_name;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END get_salesrep_areamgr_info;
--03/07/2014:Harsha--TMS 20140306-00133  --EDW - Incorrect RVP Reporting for DMs


  FUNCTION get_salesrep_info (p_salesrep_num IN VARCHAR2)
      RETURN VARCHAR2
   IS
      l_salesrep_name   VARCHAR2 (240);
   BEGIN
      l_salesrep_name := NULL;


      

      SELECT NVL (NVL (jrs.NAME, papf.full_name), jrre.resource_name)
        INTO l_salesrep_name
        FROM jtf_rs_salesreps jrs,
             jtf_rs_defresources_v jrre,
             per_all_people_f papf
       WHERE jrs.salesrep_number = p_salesrep_num
         AND jrs.resource_id = jrre.resource_id(+)
         AND jrs.person_id = papf.person_id(+)
         AND TRUNC (papf.effective_start_date(+)) <= TRUNC (SYSDATE)
         AND NVL (TRUNC (papf.effective_end_date(+)), TRUNC (SYSDATE)) >=
                                                               TRUNC (SYSDATE);

      RETURN l_salesrep_name;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END get_salesrep_info;




   FUNCTION get_ap_invoice_num (
      p_po_header_id          IN   NUMBER,
      p_po_line_id            IN   NUMBER,
      p_po_line_location_id   IN   NUMBER,
      p_shipment_line_id      IN   NUMBER,
      p_rcv_transaction_id    IN   NUMBER
   )
      RETURN VARCHAR2
   IS
      l_invoice_num   VARCHAR2 (240);
   BEGIN
      l_invoice_num := NULL;

      SELECT x1.invoice_num
        INTO l_invoice_num
        FROM (SELECT   aia.invoice_num, aia.creation_date
                  FROM ap_invoices_all aia, ap_invoice_lines_all aila
                 WHERE aia.invoice_id = aila.invoice_id
                   AND aila.po_header_id = p_po_header_id
                   AND NVL (aila.po_line_id, -1) =
                                  NVL (p_po_line_id, NVL (aila.po_line_id, -1))
                   AND NVL (aila.po_line_location_id, -1) =
                          NVL (p_po_line_location_id,
                               NVL (aila.po_line_location_id, -1)
                              )
              -- 05/31/2012 CGonzalez: commented match based on receipt info, cant find data matching at that level
              --and       nvl(aila.rcv_shipment_line_id,-1) = nvl(p_shipment_line_id,nvl(aila.rcv_shipment_line_id,-1))
              --and       nvl(aila.rcv_transaction_id,-1) = nvl(p_rcv_transaction_id,nvl(aila.rcv_transaction_id,-1))
              ORDER BY aia.invoice_date DESC) x1
       WHERE ROWNUM = 1;

      RETURN l_invoice_num;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END get_ap_invoice_num;

   FUNCTION get_ap_invoice_tax (p_invoice_id IN NUMBER, p_org_id IN NUMBER)
      RETURN NUMBER
   IS
      l_tax_amount   NUMBER;
   BEGIN
      l_tax_amount := NULL;

      SELECT SUM (NVL (included_tax_amount, 0))
        INTO l_tax_amount
        FROM ap_invoice_lines_all
       WHERE invoice_id = p_invoice_id AND org_id = p_org_id;

      RETURN l_tax_amount;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 0;
   END get_ap_invoice_tax;

   FUNCTION format_phone_number (p_phone_number IN VARCHAR2)
      RETURN VARCHAR2
   IS
      l_formated_number   VARCHAR2 (80);
   BEGIN
      l_formated_number := NULL;

      SELECT DECODE (p_phone_number,
                     NULL, NULL,
                     (   '('
                      || SUBSTR (REGEXP_REPLACE (p_phone_number,
                                                 '[(]|[)]|[-]|[ ]|[.]',
                                                 ''
                                                ),
                                 1,
                                 3
                                )
                      || ') '
                      || SUBSTR (REGEXP_REPLACE (p_phone_number,
                                                 '[(]|[)]|[-]|[ ]|[.]',
                                                 ''
                                                ),
                                 4,
                                 3
                                )
                      || '-'
                      || SUBSTR (REGEXP_REPLACE (p_phone_number,
                                                 '[(]|[)]|[-]|[ ]|[.]',
                                                 ''
                                                ),
                                 7
                                )
                     )
                    )
        INTO l_formated_number
        FROM DUAL;

      RETURN l_formated_number;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN p_phone_number;
   END format_phone_number;

   FUNCTION get_bt_trx_salesrep_phone (
      p_trx_salesrep_id       IN   NUMBER,
      p_bill_to_salesrep_id   IN   NUMBER,
      p_org_id                IN   NUMBER
   )
      RETURN VARCHAR2
   IS
      l_salesrep_name   VARCHAR2 (240);
      l_person_id       NUMBER;
      l_phone_number    VARCHAR2 (80);
   BEGIN
      l_salesrep_name := NULL;
      l_person_id := NULL;
      l_phone_number := NULL;

      -- Pull sales rep for transaction
      BEGIN
         SELECT NVL (jrs.NAME, papf.full_name), papf.person_id
           INTO l_salesrep_name, l_person_id
           FROM jtf.jtf_rs_salesreps jrs, hr.per_all_people_f papf
          WHERE jrs.salesrep_id = p_trx_salesrep_id
            AND jrs.org_id = p_org_id
            AND jrs.person_id = papf.person_id(+)
            AND TRUNC (papf.effective_start_date(+)) <= TRUNC (SYSDATE)
            AND NVL (TRUNC (papf.effective_end_date(+)), TRUNC (SYSDATE)) >=
                                                               TRUNC (SYSDATE);
      EXCEPTION
         WHEN OTHERS
         THEN
            l_salesrep_name := NULL;
      END;

      IF NVL (l_salesrep_name, 'No Sales Credit') = 'No Sales Credit'
      THEN
         -- Pull sales rep for bill to
         BEGIN
            SELECT NVL (jrs.NAME, papf.full_name), papf.person_id
              INTO l_salesrep_name, l_person_id
              FROM jtf.jtf_rs_salesreps jrs, hr.per_all_people_f papf
             WHERE jrs.salesrep_id = p_bill_to_salesrep_id
               AND jrs.org_id = p_org_id
               AND jrs.person_id = papf.person_id(+)
               AND TRUNC (papf.effective_start_date(+)) <= TRUNC (SYSDATE)
               AND NVL (TRUNC (papf.effective_end_date(+)), TRUNC (SYSDATE)) >=
                                                               TRUNC (SYSDATE);
         EXCEPTION
            WHEN OTHERS
            THEN
               l_salesrep_name := NULL;
         END;
      END IF;

      IF NVL (l_salesrep_name, 'No Sales Credit') = 'No Sales Credit'
      THEN
         l_salesrep_name := NULL;
         l_person_id := NULL;
      END IF;

      IF l_person_id IS NOT NULL
      THEN
         SELECT xxwc_mv_routines_pkg.format_phone_number (pp.phone_number)
           INTO l_phone_number
           FROM per_phones pp
          WHERE pp.parent_id = l_person_id -- papf.person_id = pp.parent_id(+)
            AND pp.parent_table = 'PER_ALL_PEOPLE_F'
            AND pp.phone_type = 'W1';
      END IF;

      RETURN l_phone_number;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END get_bt_trx_salesrep_phone;

   FUNCTION get_prism_rental_date (
      p_return_date_type   IN   VARCHAR2,
      p_trx_number         IN   VARCHAR2
   )
      RETURN DATE
   IS
      l_rental_date   DATE;
      l_from_date     DATE;
      l_to_date       DATE;
   BEGIN
      l_rental_date := NULL;
      l_from_date := NULL;
      l_to_date := NULL;

      SELECT x1.from_date, x1.TO_DATE
        INTO l_from_date, l_to_date
        FROM xxwc.xxwc_ar_prism_rental_ord_stg x1
       WHERE trx_number = p_trx_number AND ROWNUM = 1;

      IF p_return_date_type = 'FROM'
      THEN
         l_rental_date := l_from_date;
      ELSE
         l_rental_date := l_to_date;
      END IF;

      RETURN l_rental_date;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END get_prism_rental_date;

   FUNCTION get_prism_rental_type (p_trx_number IN VARCHAR2)
      RETURN VARCHAR2
   IS
      l_rental_type   VARCHAR2 (64);
   BEGIN
      l_rental_type := NULL;

      SELECT TRIM (RTRIM (LTRIM (x1.rental_text2, '*'), '*'))
        INTO l_rental_type
        FROM xxwc.xxwc_ar_prism_rental_ord_stg x1
       WHERE trx_number = p_trx_number AND ROWNUM = 1;

      RETURN l_rental_type;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END get_prism_rental_type;

   FUNCTION get_prism_inv_line_uom (
      p_trx_number    IN   VARCHAR2,
      p_line_number   IN   VARCHAR2
   )
      RETURN VARCHAR2
   IS
      l_prism_uom   VARCHAR2 (3);
   BEGIN
      l_prism_uom := NULL;

      SELECT uom_code
        INTO l_prism_uom
        FROM xxwc.xxwc_ar_inv_stg_tbl
       WHERE trx_number = p_trx_number
         AND TO_CHAR (line_number) = p_line_number
         AND ROWNUM = 1;

      RETURN l_prism_uom;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END get_prism_inv_line_uom;

   FUNCTION get_uom_conv_rate_to_base (p_rcv_uom IN VARCHAR2
                                       , p_trx_uom IN VARCHAR2)
      RETURN NUMBER
   IS
      l_conversion_rate   NUMBER;
      l_is_base           VARCHAR2(1);
   BEGIN
      l_conversion_rate := NULL;

      if p_rcv_uom = p_trx_uom
      then
        l_conversion_rate := 1;
      else

        select  muo.base_uom_flag
        into    l_is_base
        from    mtl_units_of_measure_tl muo
        where   muo.unit_of_measure = p_rcv_uom
        and     nvl(trunc(disable_date),trunc(sysdate)) >= trunc(sysdate)
        and     rownum = 1;

        if  nvl(l_is_base,'N') = 'Y' then

          SELECT (1/conversion_rate)
            INTO l_conversion_rate
            FROM mtl_uom_conversions muc
           WHERE muc.unit_of_measure = p_trx_uom
             --and       nvl(trunc(muc.disable_date),trunc(sysdate)) > trunc(sysdate+1)
             AND muc.disable_date IS NULL
             AND ROWNUM = 1;

        else

          SELECT conversion_rate
            INTO l_conversion_rate
            FROM mtl_uom_conversions muc
           WHERE muc.unit_of_measure = p_rcv_uom
             --and       nvl(trunc(muc.disable_date),trunc(sysdate)) > trunc(sysdate+1)
             AND muc.disable_date IS NULL
             AND ROWNUM = 1;

        end if;

      end if;

      RETURN NVL (l_conversion_rate, 1);
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 1;
   END get_uom_conv_rate_to_base;

   FUNCTION get_item_cross_reference (
      p_inventory_item_id      IN   NUMBER,
      p_cross_reference_type   IN   VARCHAR2,
      p_rownum                 IN   NUMBER
   )
      RETURN VARCHAR2
   IS
      l_cross_reference   VARCHAR2 (255);
   BEGIN
      l_cross_reference := NULL;

      SELECT x1.cross_reference
        INTO l_cross_reference
        FROM (SELECT mcrb.inventory_item_id, mcrb.cross_reference_type,
                     mcrb.cross_reference,
                     ROW_NUMBER () OVER (PARTITION BY mcrb.inventory_item_id, mcrb.cross_reference_type ORDER BY mcrb.cross_reference_id)
                                                                           rn
                FROM mtl_cross_references_b mcrb
               WHERE mcrb.inventory_item_id = p_inventory_item_id
                 AND mcrb.cross_reference_type = p_cross_reference_type) x1
       WHERE x1.rn = p_rownum AND ROWNUM = 1;

      RETURN l_cross_reference;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END get_item_cross_reference;

   FUNCTION get_salesrep_branch (p_fru_number IN VARCHAR2)
      RETURN VARCHAR2
   IS
      l_branch_location   VARCHAR2 (3);
   BEGIN
      l_branch_location := NULL;

      -- 02/11/13 CG: Removed NVL to fru since it's not a recognizable value to match to the other extract
      -- SELECT NVL (organization_code, p_fru_number)
      SELECT organization_code
        INTO l_branch_location
        FROM mtl_parameters
       WHERE attribute10 = p_fru_number;

      RETURN l_branch_location;
   EXCEPTION
      WHEN OTHERS
      THEN
         -- 02/11/13 CG: Removed NVL to fru since it's not a recognizable value to match to the other extract
         RETURN null;
   END get_salesrep_branch;

   /*
   function used to retrieve individual segments from an item category
   or full concatenated segments.
   Currently will only retrieve segments 1 through 4 and all.

   p_segment_num indicates which segment:
   1 - Segment1
   2 - Segment2
   3 - Segment3
   4 - Segment4
   0 - All Concatenated Segments
   */
   FUNCTION get_item_category (
      p_inventory_item_id   IN   NUMBER,
      p_organization_id     IN   NUMBER,
      p_category_set        IN   VARCHAR2,
      p_segment_num         IN   NUMBER
   )
      RETURN VARCHAR2
   IS
      l_cat_segment1      VARCHAR2 (40);
      l_cat_segment2      VARCHAR2 (40);
      l_cat_segment3      VARCHAR2 (40);
      l_cat_segment4      VARCHAR2 (40);
      l_cat_seg_concat    VARCHAR2 (163);
      l_return_category   VARCHAR2 (163);
   BEGIN
      l_cat_segment1 := NULL;
      l_cat_segment2 := NULL;
      l_cat_segment3 := NULL;
      l_cat_segment4 := NULL;
      l_cat_seg_concat := NULL;
      l_return_category := NULL;

      SELECT mic.segment1, mic.segment2, mic.segment3, mic.segment4,
             mic.category_concat_segs
        INTO l_cat_segment1, l_cat_segment2, l_cat_segment3, l_cat_segment4,
             l_cat_seg_concat
        FROM mtl_item_categories_v mic
       WHERE mic.category_set_name = p_category_set
         AND mic.inventory_item_id = p_inventory_item_id
         AND mic.organization_id = p_organization_id
         AND ROWNUM = 1;

      IF p_segment_num = 1
      THEN
         l_return_category := l_cat_segment1;
      ELSIF p_segment_num = 2
      THEN
         l_return_category := l_cat_segment2;
      ELSIF p_segment_num = 3
      THEN
         l_return_category := l_cat_segment3;
      ELSIF p_segment_num = 4
      THEN
         l_return_category := l_cat_segment4;
      ELSE
         l_return_category := l_cat_seg_concat;
      END IF;

      RETURN l_return_category;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END get_item_category;

   FUNCTION get_item_sr_vendor (
      p_inventory_item_id   IN   NUMBER,
      p_organization_id     IN   NUMBER
   )
      RETURN VARCHAR2
   IS
      l_supplier_number   VARCHAR2 (30);
   BEGIN
      l_supplier_number := NULL;

      SELECT /*msr.sourcing_rule_id
             , msr.sourcing_rule_name
             , msr.sourcing_rule_type
             , msro.sr_receipt_id
             , msso.sr_source_id
             , msso.vendor_id
             , msso.vendor_site_id
             ,*/ asa.segment1 supplier_number
        --, asa.vendor_name
      INTO    l_supplier_number
        FROM mrp_sourcing_rules msr,
             mrp_sr_receipt_org msro,
             mrp_sr_source_org msso,
             ap_suppliers asa,
             mrp_sr_assignments msa,
             mrp_assignment_sets mas
       WHERE msr.sourcing_rule_id = msro.sourcing_rule_id
         AND msro.sr_receipt_id = msso.sr_receipt_id
         AND msso.vendor_id = asa.vendor_id
         AND msr.sourcing_rule_id = msa.sourcing_rule_id
         AND msa.assignment_type = 6                   -- Item/Org Assignments
         AND msa.sourcing_rule_type = 1                       -- Sourcing Rule
         AND msa.assignment_set_id = mas.assignment_set_id
         AND mas.assignment_set_name = 'WC Default'
         AND msa.organization_id = p_organization_id
         AND msa.inventory_item_id = p_inventory_item_id
         AND ROWNUM = 1;

      RETURN l_supplier_number;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END get_item_sr_vendor;

   FUNCTION get_last_receipt_date (
      p_inventory_item_id   IN   NUMBER,
      p_organization_id     IN   NUMBER,
      p_type                IN   VARCHAR2
   )
      RETURN DATE
   IS
      l_last_receipt_date   DATE;
   BEGIN
      l_last_receipt_date := NULL;

      -- 02/19/13 CG: TMS 20130218-01098. Altered where clause and moved the link for organization_id to the
      -- shipment lines table to improve performance
      SELECT MAX (rt.transaction_date)
        INTO l_last_receipt_date
        FROM rcv_transactions rt, rcv_shipment_lines rsl
       WHERE rt.transaction_type = 'RECEIVE'
         AND (   (    p_type = 'Intransit'
                  AND rt.source_document_code IN ('REQ', 'INVENTORY')
                 )
              OR (p_type = 'PO' AND rt.source_document_code = 'PO')
              OR (p_type = 'RMA' AND rt.source_document_code IN ('RMA'))
             )
         -- 02/19/13 CG: TMS 20130218-01098
         -- AND rt.organization_id = p_organization_id
         AND rt.shipment_line_id = rsl.shipment_line_id
         AND rsl.item_id = p_inventory_item_id
         -- 02/19/13 CG: TMS 20130218-01098
         AND rsl.to_organization_id = p_organization_id;

      RETURN l_last_receipt_date;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END get_last_receipt_date;

   FUNCTION get_last_transf_out_date (
      p_inventory_item_id   IN   NUMBER,
      p_organization_id     IN   NUMBER
   )
      RETURN DATE
   IS
      l_last_sales_date   DATE;
   BEGIN
      l_last_sales_date := NULL;

      SELECT MAX (oola.actual_shipment_date)
        INTO l_last_sales_date
        FROM oe_order_lines_all oola, oe_order_sources oos
       WHERE oola.inventory_item_id = p_inventory_item_id
         AND oola.ship_from_org_id = p_organization_id
         AND NVL (oola.cancelled_flag, 'N') = 'N'
         AND oola.actual_shipment_date IS NOT NULL
         AND oola.order_source_id = oos.order_source_id
         AND oos.NAME = 'Internal';

      RETURN (l_last_sales_date);
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN (NULL);
   END get_last_transf_out_date;

   FUNCTION get_onhand_qty (
      p_inventory_item_id   IN   NUMBER,
      p_organization_id     IN   NUMBER,
      p_subinventory_name   IN   VARCHAR2 DEFAULT NULL,
      p_return_value_type   IN   VARCHAR2
   )
      RETURN NUMBER
   IS
      -- Cursor to pull subinventories that are reservable by organization
      -- and susbinventory name if given, if not it will pull all subinventories
      -- Only currently active subinventories
      CURSOR cur_subinvs (p_organization_id NUMBER)
      IS
         SELECT secondary_inventory_name
           FROM mtl_secondary_inventories
          WHERE organization_id = p_organization_id
            AND UPPER (secondary_inventory_name) =
                   UPPER (NVL (p_subinventory_name, secondary_inventory_name))
            AND TRUNC (NVL (disable_date, SYSDATE)) >= TRUNC (SYSDATE);

      l_return_status     VARCHAR2 (10)   := NULL;
      l_msg_count         NUMBER          := NULL;
      l_msg_data          VARCHAR2 (1000) := NULL;
      l_qoh               NUMBER          := NULL;
      l_rqoh              NUMBER          := NULL;
      l_qr                NUMBER          := NULL;
      l_qs                NUMBER          := NULL;
      l_att               NUMBER          := NULL;
      l_atr               NUMBER          := NULL;
      l_total_qoh         NUMBER          := NULL;
      l_total_rqoh        NUMBER          := NULL;
      l_total_qr          NUMBER          := NULL;
      l_total_qs          NUMBER          := NULL;
      l_total_att         NUMBER          := NULL;
      l_total_atr         NUMBER          := NULL;
      l_return_quantity   NUMBER          := NULL;
   BEGIN
      -- Block to pull total available to reserve quantity and onhand quantity for a specific item in an organization
      l_return_quantity := 0;
      l_total_qoh := 0;
      l_total_rqoh := 0;
      l_total_qr := 0;
      l_total_qs := 0;
      l_total_att := 0;
      l_total_atr := 0;

      FOR rec_subinvs IN cur_subinvs (p_organization_id)
      LOOP
         EXIT WHEN cur_subinvs%NOTFOUND;

         BEGIN
            inv_quantity_tree_pub.clear_quantity_cache;
            --commit;
            inv_quantity_tree_pub.query_quantities
                (p_api_version_number              => 1.0,
                 p_init_msg_lst                    => fnd_api.g_false,
                 x_return_status                   => l_return_status,
                 x_msg_count                       => l_msg_count,
                 x_msg_data                        => l_msg_data,
                 p_organization_id                 => p_organization_id,
                 p_inventory_item_id               => p_inventory_item_id,
                 p_tree_mode                       => inv_quantity_tree_pub.g_transaction_mode,
                 p_is_revision_control             => FALSE,
                 p_is_lot_control                  => FALSE,
                 p_is_serial_control               => FALSE,
                 p_demand_source_type_id           => -9999,
                 p_demand_source_header_id         => -9999,
                 p_demand_source_line_id           => -9999,
                 p_demand_source_name              => NULL,
                 p_lot_expiration_date             => NULL,
                 p_revision                        => NULL,
                 p_lot_number                      => NULL,
                 p_subinventory_code               => rec_subinvs.secondary_inventory_name,
                 p_locator_id                      => NULL,
                 p_onhand_source                   => 3,
                 x_qoh                             => l_qoh,
                 x_rqoh                            => l_rqoh,
                 x_qr                              => l_qr,
                 x_qs                              => l_qs,
                 x_att                             => l_att,
                 x_atr                             => l_atr,
                 p_transfer_subinventory_code      => NULL,
                 p_cost_group_id                   => NULL,
                 p_lpn_id                          => NULL,
                 p_transfer_locator_id             => NULL
                );

            IF l_return_status = 'S'
            THEN
               l_total_qoh := l_total_qoh + l_qoh;
               l_total_rqoh := l_total_rqoh + l_rqoh;
               l_total_qr := l_total_qr + l_qr;
               l_total_qs := l_total_qs + l_qs;
               l_total_att := l_total_att + l_att;
               l_total_atr := l_total_atr + l_atr;
            ELSE
               l_total_qoh := l_total_qoh + 0;
               l_total_rqoh := l_total_rqoh + 0;
               l_total_qr := l_total_qr + 0;
               l_total_qs := l_total_qs + 0;
               l_total_att := l_total_att + 0;
               l_total_atr := l_total_atr + 0;
            END IF;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_total_qoh := l_total_qoh + 0;
               l_total_rqoh := l_total_rqoh + 0;
               l_total_qr := l_total_qr + 0;
               l_total_qs := l_total_qs + 0;
               l_total_att := l_total_att + 0;
               l_total_atr := l_total_atr + 0;
         END;
      END LOOP;

      IF p_return_value_type = 'QOH'
      THEN
         l_return_quantity := l_total_qoh;
      ELSIF p_return_value_type = 'RQOH'
      THEN
         l_return_quantity := l_total_rqoh;
      ELSIF p_return_value_type = 'QRES'
      THEN
         l_return_quantity := l_total_qr;
      ELSIF p_return_value_type = 'QSUG'
      THEN
         l_return_quantity := l_total_qs;
      ELSIF p_return_value_type = 'ATT'
      THEN
         l_return_quantity := l_total_att;
      ELSIF p_return_value_type = 'ATR'
      THEN
         l_return_quantity := l_total_atr;
      ELSE
         l_return_quantity := 0;
      END IF;

      RETURN (NVL (l_return_quantity, 0));
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 0;
   END get_onhand_qty;

   FUNCTION get_vendor_consigned_qty (
      p_inventory_item_id   IN   NUMBER,
      p_organization_id     IN   NUMBER
   )
      RETURN NUMBER
   IS
      l_vendor_consigned_qty   NUMBER;
   BEGIN
      l_vendor_consigned_qty := 0;

      -- 06/05/2012 CG: Per metalink note 406390.1 - Point 27.2
      -- Differentiates a view from the master table, pointing out the view only
      -- displays company owned inventory via the is_consigned set to 2
      SELECT SUM (primary_transaction_quantity)
        INTO l_vendor_consigned_qty
        FROM mtl_onhand_quantities_detail
       WHERE inventory_item_id = p_inventory_item_id
         AND organization_id = p_organization_id
         AND NVL (is_consigned, 2) != 2;

      RETURN (NVL (l_vendor_consigned_qty, 0));
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 0;
   END get_vendor_consigned_qty;

   FUNCTION get_source_mgr_rep_num (p_source_id IN NUMBER)
      RETURN VARCHAR2
   IS
      l_source_mgr_rep_num   VARCHAR2 (30);
   BEGIN
      l_source_mgr_rep_num := NULL;

      SELECT jrs.salesrep_number
        INTO l_source_mgr_rep_num
        FROM jtf_rs_resource_extns jrre, jtf_rs_salesreps jrs
       WHERE jrre.source_id = p_source_id
         AND jrre.resource_id = jrs.resource_id;

      RETURN l_source_mgr_rep_num;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END get_source_mgr_rep_num;

   FUNCTION get_item_qp_list_price (p_inventory_item_id IN NUMBER)
      RETURN NUMBER
   IS
      l_item_list_price   NUMBER;
   BEGIN
      l_item_list_price := 0;

      SELECT /*qlhb.list_header_id
             , qlht.name prl_name
             , */ qll.operand
        INTO  l_item_list_price
        FROM qp_list_headers_b qlhb,
             qp_list_headers_tl qlht,
             qp_list_lines qll,
             qp_pricing_attributes qpa
       WHERE qlhb.pte_code = 'ORDFUL'
         AND qlhb.list_type_code = 'PRL'
         AND NVL (qlhb.active_flag, 'N') = 'Y'
         AND NVL (TRUNC (qlhb.start_date_active), TRUNC (SYSDATE)) <=
                                                               TRUNC (SYSDATE)
         AND NVL (TRUNC (qlhb.end_date_active), TRUNC (SYSDATE)) >=
                                                               TRUNC (SYSDATE)
         AND qlhb.list_header_id = qlht.list_header_id
         AND qlhb.list_header_id = qll.list_header_id
         AND qll.list_line_type_code = 'PLL'
         AND qll.modifier_level_code = 'LINE'
         AND NVL (TRUNC (qll.start_date_active), TRUNC (SYSDATE)) <=
                                                               TRUNC (SYSDATE)
         AND NVL (TRUNC (qll.end_date_active), TRUNC (SYSDATE)) >=
                                                               TRUNC (SYSDATE)
         -- Pricing Attributes
         AND qll.list_line_id = qpa.list_line_id
         AND qll.list_header_id = qpa.list_header_id
         AND qpa.product_attribute_context = 'ITEM'
         AND qpa.product_attribute = 'PRICING_ATTRIBUTE1'
         AND qpa.product_attr_value = TO_CHAR (p_inventory_item_id);

      RETURN l_item_list_price;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 20000;
   END get_item_qp_list_price;

   FUNCTION get_order_has_dropships (p_order_header_id IN NUMBER)
      RETURN VARCHAR2
   IS
      l_count_dropship   NUMBER;
      l_has_dropship     VARCHAR2 (1);
   BEGIN
      l_count_dropship := 0;
      l_has_dropship := NULL;

      SELECT COUNT (*)
        INTO l_count_dropship
        FROM oe_order_lines_all oola
       WHERE oola.header_id = p_order_header_id
         AND NVL (oola.booked_flag, 'N') = 'Y'
         AND NVL (oola.cancelled_flag, 'N') = 'Y'
         AND oola.source_type_code = 'EXTERNAL';

      IF l_count_dropship > 0
      THEN
         l_has_dropship := 'Y';
      ELSE
         l_has_dropship := 'N';
      END IF;

      RETURN l_has_dropship;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 'N';
   END get_order_has_dropships;

   FUNCTION get_oe_ds_line_vendor (p_order_line_id IN NUMBER)
      RETURN VARCHAR2
   IS
      l_ship_to_site_identifier   VARCHAR2 (46);
   BEGIN
      l_ship_to_site_identifier := NULL;

      SELECT (asa.segment1 || '-' || assa.vendor_site_code)
        INTO l_ship_to_site_identifier
        FROM oe_drop_ship_sources odss,
             po_headers_all pha,
             ap_suppliers asa,
             ap_supplier_sites_all assa
       WHERE odss.line_id = p_order_line_id
         AND odss.po_header_id = pha.po_header_id
         AND pha.vendor_id = asa.vendor_id
         AND pha.vendor_site_id = assa.vendor_site_id
         AND ROWNUM = 1;

      RETURN l_ship_to_site_identifier;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END get_oe_ds_line_vendor;

   FUNCTION get_backordered_so_item_qty (
      p_inventory_item_id   IN   NUMBER,
      p_organization_id     IN   NUMBER
   )
      RETURN NUMBER
   IS
      l_backordered_qty   NUMBER;
   BEGIN
      l_backordered_qty := NULL;

      SELECT SUM (  oola.ordered_quantity
                  - NVL (oola.shipped_quantity, 0)
                  - NVL (oola.cancelled_quantity, 0)
                 )
        INTO l_backordered_qty
        FROM oe_order_lines_all oola, wsh_delivery_details wdd
       WHERE oola.inventory_item_id = p_inventory_item_id
         AND oola.ship_from_org_id = p_organization_id
         AND oola.line_category_code = 'ORDER'
         AND NVL (oola.cancelled_flag, 'N') = 'N'
         AND NVL (oola.booked_flag, 'N') = 'Y'
         AND oola.line_id = wdd.source_line_id
         AND wdd.source_code = 'OE'
         AND wdd.released_status = 'B';

      RETURN l_backordered_qty;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 0;
   END get_backordered_so_item_qty;

   FUNCTION get_customer_open_balance (p_customer_id IN NUMBER)
      RETURN NUMBER
   IS
      l_open_balance   NUMBER;
   BEGIN
      l_open_balance := NULL;

      SELECT SUM (apsa2.amount_due_remaining)
        INTO l_open_balance
        FROM ar_payment_schedules_all apsa2
       WHERE apsa2.customer_id = p_customer_id;

      RETURN l_open_balance;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 0;
   END get_customer_open_balance;

   FUNCTION get_customer_last_payment_date (p_customer_id IN NUMBER)
      RETURN DATE
   IS
      l_last_payment_date   DATE;
   BEGIN
      l_last_payment_date := NULL;

      SELECT MAX (apsa2.trx_date)
        INTO l_last_payment_date
        FROM ar_payment_schedules_all apsa2
       WHERE apsa2.customer_id = p_customer_id AND apsa2.CLASS = 'PMT';

      RETURN l_last_payment_date;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END get_customer_last_payment_date;

   FUNCTION get_gl_code_segment (
      p_code_combination_id   IN   NUMBER,
      p_return_segment        IN   NUMBER
   )
      RETURN VARCHAR2
   IS
      l_segment1   VARCHAR2 (25);
      l_segment2   VARCHAR2 (25);
      l_segment3   VARCHAR2 (25);
      l_segment4   VARCHAR2 (25);
      l_segment5   VARCHAR2 (25);
      l_segment6   VARCHAR2 (25);
      l_segment7   VARCHAR2 (25);
   BEGIN
      l_segment1 := NULL;
      l_segment2 := NULL;
      l_segment3 := NULL;
      l_segment4 := NULL;
      l_segment5 := NULL;
      l_segment6 := NULL;
      l_segment7 := NULL;

      SELECT segment1, segment2, segment3, segment4, segment5,
             segment6, segment7
        INTO l_segment1, l_segment2, l_segment3, l_segment4, l_segment5,
             l_segment6, l_segment7
        FROM gl_code_combinations
       WHERE code_combination_id = p_code_combination_id;

      IF p_return_segment = 1
      THEN
         RETURN l_segment1;
      ELSIF p_return_segment = 2
      THEN
         RETURN l_segment2;
      ELSIF p_return_segment = 3
      THEN
         RETURN l_segment3;
      ELSIF p_return_segment = 4
      THEN
         RETURN l_segment4;
      ELSIF p_return_segment = 5
      THEN
         RETURN l_segment5;
      ELSIF p_return_segment = 6
      THEN
         RETURN l_segment6;
      ELSIF p_return_segment = 7
      THEN
         RETURN l_segment7;
      ELSE
         RETURN NULL;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END get_gl_code_segment;

FUNCTION get_om_rental_item (p_line_id IN NUMBER, p_return_type IN VARCHAR2)
   RETURN VARCHAR2
IS
   l_item_number         VARCHAR2 (40);
   l_item_desc           VARCHAR2 (240);
   --Added by Ram Talluri for TMS 20140613-00192 6/18/2014
   l_inventory_item_id   NUMBER;
   l_order_type_id       NUMBER;
   l_reference_line_id   NUMBER;
   l_err_callfrom        VARCHAR2 (75)
                            := 'XXWC_MV_ROUTINES_PKG.GET_OM_RENTAL_ITEM';
   l_err_callpoint       VARCHAR2 (75) := 'START';
   l_distro_list         VARCHAR2 (75)
                            DEFAULT 'HDSOracleDevelopers@hdsupply.com';
   l_message             VARCHAR2 (2000);
BEGIN
   l_item_number := NULL;
   l_item_desc := NULL;
   l_inventory_item_id := NULL; --Added by Ram Talluri for TMS 20140613-00192 6/18/2014
   l_reference_line_id := NULL; --Added by Ram Talluri for TMS 20140613-00192 6/18/2014

   BEGIN
      SELECT order_type_id
        INTO l_order_type_id
        FROM oe_order_headers_all oha, oe_order_lines_all ola
       WHERE     ola.header_id = oha.header_id
             AND ola.org_id = oha.org_id
             AND ola.line_id = p_line_id;
   EXCEPTION
      WHEN OTHERS
      THEN
         l_order_type_id := NULL;
         l_err_callpoint  := 'l_order_type_id'||l_order_type_id;
   END;

   IF l_order_type_id IN (1013, 1014) --Long term and short term rental orders-- Added by Ram Talluri for TMS 20140613-00192 6/18/2014
   THEN
      SELECT msib.segment1, msib.description, msib.inventory_item_id --inventory_item_id Added by Ram Talluri for TMS 20140613-00192 6/18/2014
        INTO l_item_number, l_item_desc, l_inventory_item_id --inventory_item_id Added by Ram Talluri for TMS 20140613-00192 6/18/2014
        FROM oe_order_lines_all oola,
             oe_order_lines_all rma_ln,
             oe_order_lines_all shp_ln,
             mtl_system_items_b msib
       WHERE     oola.line_id = p_line_id
             AND oola.link_to_line_id = rma_ln.line_id
             AND rma_ln.line_category_code = 'RETURN'
             AND rma_ln.link_to_line_id = shp_ln.line_id
             AND shp_ln.line_category_code = 'ORDER'
             AND shp_ln.inventory_item_id = msib.inventory_item_id
             AND shp_ln.ship_from_org_id = msib.organization_id
             AND ROWNUM = 1;

      IF p_return_type = 'DESC'
      THEN
         RETURN l_item_desc;
         l_err_callpoint  := 'Deriving item desc-rental SO ln';
      ELSIF p_return_type = 'INVENTORY_ITEM_ID' -- Added by Ram Talluri for TMS 20140613-00192 6/18/2014
      THEN
         RETURN TO_CHAR(l_inventory_item_id);
         l_err_callpoint  := 'Deriving item_id-rental SO ln';
      ELSE
         RETURN l_item_number;
         l_err_callpoint  := 'Deriving item number-rental SO ln';
      END IF;
      --Return Order-- Added by Ram Talluri for TMS 20140613-00192 6/18/2014
   ELSIF l_order_type_id = 1006                                 
   THEN
      BEGIN
         SELECT REFERENCE_LINE_ID
           INTO l_reference_line_id
           FROM oe_order_lines_all
          WHERE line_id = p_line_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_reference_line_id := NULL;
            l_err_callpoint := 'REFERNCE_LINE_ID';
      END;

      IF l_reference_line_id IS NOT NULL
      THEN
         SELECT msib.segment1, msib.description, msib.inventory_item_id
           INTO l_item_number, l_item_desc, l_inventory_item_id
           FROM oe_order_lines_all oola,
                oe_order_lines_all rma_ln,
                oe_order_lines_all shp_ln,
                mtl_system_items_b msib
          WHERE     oola.line_id = l_reference_line_id
                AND oola.link_to_line_id = rma_ln.line_id
                AND rma_ln.line_category_code = 'RETURN'
                AND rma_ln.link_to_line_id = shp_ln.line_id
                AND shp_ln.line_category_code = 'ORDER'
                AND shp_ln.inventory_item_id = msib.inventory_item_id
                AND shp_ln.ship_from_org_id = msib.organization_id
                AND ROWNUM = 1;

         
         IF p_return_type = 'DESC'
         THEN
            RETURN l_item_desc;
            l_err_callpoint  := 'Deriving item desc-RMA rental_ln';
         ELSIF p_return_type = 'INVENTORY_ITEM_ID'
         THEN
            RETURN l_inventory_item_id;
            l_err_callpoint  := 'Deriving item_id-RMA rental_ln';
         ELSE
            RETURN l_item_number;
            l_err_callpoint  := 'Deriving item_number-RMA rental_ln';
         END IF;
         
      ELSE      
           RETURN NULL;-- Added by Ram Talluri for TMS 20140621-00009  6/21/2014 
      END IF;
      
   ELSE
      --NULL;-- commented by Ram Talluri for TMS 20140621-00009  6/21/2014
      RETURN NULL;-- Added by Ram Talluri for TMS 20140621-00009  6/21/2014 
   END IF;
   
EXCEPTION
   WHEN OTHERS
   THEN
      RETURN NULL;
      xxcus_error_pkg.xxcus_error_main_api (
         p_called_from         => l_err_callfrom,
         p_calling             => l_err_callpoint,
         p_ora_error_msg       => SQLERRM,
         p_error_desc          => l_message,
         p_distribution_list   => l_distro_list,
         p_module              => 'XXWC');
END get_om_rental_item;

   FUNCTION get_item_dflt_loc (
      p_inventory_item_id   IN   NUMBER,
      p_organization_id     IN   NUMBER,
      p_subinventory        IN   VARCHAR2,
      p_default_loc_lvl     IN   NUMBER
   )
      RETURN VARCHAR2
   IS
      l_item_def_locator   VARCHAR2 (40);
   BEGIN
      l_item_def_locator := NULL;

      SELECT l.segment1
        INTO l_item_def_locator
        FROM mtl_secondary_locators s, mtl_item_locations l
       WHERE s.inventory_item_id = p_inventory_item_id
         AND s.organization_id = p_organization_id
         AND s.subinventory_code = NVL (p_subinventory, s.subinventory_code)
         AND s.secondary_locator = l.inventory_location_id
         AND s.organization_id = l.organization_id
         AND l.segment1 LIKE (TO_CHAR (p_default_loc_lvl) || '-%')
         AND ROWNUM = 1;

      RETURN l_item_def_locator;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END get_item_dflt_loc;

   FUNCTION get_order_line_cost (p_line_id  IN NUMBER)
   RETURN NUMBER
   IS
    l_item_cost     number;
   BEGIN
    l_item_cost := null;

    select  actual_cost
    into    l_item_cost
    from    (
                select  nvl(mmt.transaction_cost, mmt.actual_cost) actual_cost
                        , mmt.trx_source_line_id
                        , ol.line_id
                from    mtl_material_transactions mmt
                        , mtl_transaction_types mtt
                        , oe_order_lines_all ol
                        , oe_order_headers_all oh
                        , oe_transaction_types_tl ott
                        -- , wsh_delivery_details wdd
                where   mmt.transaction_type_id = mtt.transaction_type_id
                and     mtt.transaction_type_name in ('RMA Receipt','RMA Return','Sales Order Pick','Sales order issue')
                and     ol.line_category_code = 'ORDER'
                and     ol.header_id = oh.header_id
                and     oh.order_type_id = ott.transaction_type_id
                and     ott.name not in ('COUNTER LINE'
                                        ,'COUNTER ORDER'
                                        ,'REPAIR ORDER')
                and     ott.language = 'US'
                --and   ol.line_id = wdd.source_line_id
                --and   wdd.transaction_id = mmt.transaction_id
                and     ol.line_id = mmt.trx_source_line_id
                and     ol.line_id = mmt.source_line_id
                and     mmt.source_code = 'ORDER ENTRY'
                UNION
                select  nvl(mmt.transaction_cost, mmt.actual_cost) actual_cost
                        , mmt.trx_source_line_id
                        , ol.line_id
                from    mtl_material_transactions mmt
                        , mtl_transaction_types mtt
                        , oe_order_lines_all ol
                        , oe_order_headers_all oh
                        , oe_transaction_types_tl ott
                where   mmt.transaction_type_id = mtt.transaction_type_id
                and     mtt.transaction_type_name in ('RMA Receipt','RMA Return','Sales Order Pick','Sales order issue')
                and     ol.line_category_code = 'ORDER'
                and     ol.header_id = oh.header_id
                and     oh.order_type_id = ott.transaction_type_id
                and     ott.name in ('COUNTER LINE'
                                    ,'COUNTER ORDER'
                                    ,'REPAIR ORDER')
                and     ott.language = 'US'
                and     ol.line_id = mmt.trx_source_line_id
                UNION
                select  nvl(mmt.transaction_cost, mmt.actual_cost) actual_cost
                        , mmt.trx_source_line_id
                        , ol.line_id
                from    mtl_material_transactions mmt
                        , mtl_transaction_types mtt
                        , oe_order_lines_all ol
                where   mmt.transaction_type_id = mtt.transaction_type_id
                and     mtt.transaction_type_name in ('RMA Receipt','RMA Return','Sales Order Pick','Sales order issue')
                and     mmt.trx_source_line_id = ol.line_id
                and     ol.line_category_code = 'RETURN'
            ) x1
    where   x1.line_id = p_line_id
    and     rownum = 1;

    return l_item_cost;
   EXCEPTION
   WHEN OTHERS THEN
    return 0;
   END get_order_line_cost;

   FUNCTION is_item_branch_source (p_inventory_item_id IN NUMBER
                                    , p_organization_id IN NUMBER)
   RETURN VARCHAR2
   IS
    l_cnt_child_branch number;
    l_is_source_branch varchar2(1);
   begin
    l_is_source_branch := null;
    l_cnt_child_branch := 0;

    begin
        select  count(*)
        into    l_cnt_child_branch
        from    mtl_system_items_b
        where   inventory_item_id = p_inventory_item_id
        and     source_organization_id = p_organization_id;
    exception
    when others then
        l_cnt_child_branch := 0;
    end;

    if nvl(l_cnt_child_branch,0) > 0 then
        l_is_source_branch := 'Y';
    else
        l_is_source_branch := 'N';
    end if;

    return l_is_source_branch;
   exception
   when others then
    return ('N');
   end is_item_branch_source;

   FUNCTION get_prism_line_unit_price (p_invoice_number IN VARCHAR2
                                        , p_invoice_line_no IN VARCHAR2)
   RETURN NUMBER
   IS
        l_unit_price    NUMBER;
   BEGIN
        l_unit_price := null;

        SELECT  price
        INTO    l_unit_price
        FROM    xxwc.xxwc_ar_prism_inv_ln_notes_stg
        WHERE   trx_number = p_invoice_number
        AND     line_no = p_invoice_line_no
        AND     line_order = 1
        AND     rownum = 1;

        return l_unit_price;
   EXCEPTION
   WHEN OTHERS THEN
        RETURN NULL;
   END get_prism_line_unit_price;

   FUNCTION get_prism_line_ext_price (p_invoice_number IN VARCHAR2
                                        , p_invoice_line_no IN VARCHAR2)
   RETURN NUMBER
   IS
        l_ext_price NUMBER;
   BEGIN
        l_ext_price := null;

        SELECT  extended_amount
        INTO    l_ext_price
        FROM    xxwc.xxwc_ar_prism_inv_ln_notes_stg
        WHERE   trx_number = p_invoice_number
        AND     line_no = p_invoice_line_no
        AND     line_order = 1
        AND     rownum = 1;

        return l_ext_price;
   EXCEPTION
   WHEN OTHERS THEN
        RETURN NULL;
   END get_prism_line_ext_price;

   FUNCTION get_last_counted_by_ntid (
      p_inventory_item_id   IN   NUMBER,
      p_organization_id     IN   NUMBER
   )
      RETURN VARCHAR2
   IS
      l_last_counted_by   VARCHAR2 (240);
   BEGIN
      l_last_counted_by := NULL;

      SELECT   fu.user_name
          INTO l_last_counted_by
          FROM mtl_cycle_count_entries mcce,
               per_all_people_f papf,
               fnd_user fu
         WHERE mcce.inventory_item_id = p_inventory_item_id
           AND mcce.organization_id = p_organization_id
           AND mcce.counted_by_employee_id_current = papf.person_id
           AND papf.person_id = fu.employee_id
           AND (   mcce.count_date_current IS NULL
                OR mcce.count_date_current =
                      (SELECT MAX (mcce2.count_date_current)
                         FROM mtl_cycle_count_entries mcce2
                        WHERE mcce2.inventory_item_id = mcce.inventory_item_id
                          AND mcce2.organization_id = mcce.organization_id)
               )
           AND rownum = 1
      ORDER BY count_date_current DESC;

      RETURN (l_last_counted_by);
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN (NULL);
   END get_last_counted_by_ntid;

   FUNCTION get_username (p_user_id IN NUMBER)
   RETURN VARCHAR2
   IS
    l_user_name varchar2(100);
   BEGIN
    l_user_name := null;

    select  user_name
    into    l_user_name
    from    fnd_user
    where   user_id = p_user_id;

    return l_user_name;
   EXCEPTION
   WHEN OTHERS THEN
    RETURN NULL;
   END get_username;
   /*************************************************************************
   **************************************************************************
     $Header get_vendor_quote_cost 
     Module Name: get_vendor_quote_cost

     PURPOSE:   This package is added by Ram Talluri for TMS 20140602-00208 on 6/3/2014

     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.0        06/11/2014  Ram Talluri              Initial Version
     2.0       06/27/2014  Ram Talluri            TMS 20140627-00126 by Ram Talluri on 6/27/2014-  Added logic to derive special cost based on dates versus refering order line

   **************************************************************************/
   FUNCTION get_vendor_quote_cost (p_line_id NUMBER)
   RETURN NUMBER
   IS
    l_vendor_quote_cost number;
    -- local error handling variables--Added by Ram Talluri for TMS 20140602-00208 on 6/3/2014
   l_err_callfrom             VARCHAR2 (75) := 'XXWC_MV_ROUTINES_PKG.GET_VENDOR_QUOTE_COST';
   l_err_callpoint            VARCHAR2 (75) := 'START';
   l_distro_list              VARCHAR2 (75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
   l_message                  VARCHAR2 (2000);
   BEGIN
    l_vendor_quote_cost := null;

    /*select  to_number(olq.attribute5)
    into    l_vendor_quote_cost
    from    oe_order_headers_all oh
            , oe_order_lines_all ol
            , oe_price_adjustments opa
            , qp_list_lines qll
            , qp_list_headers_b qlh
            , oe_order_headers_all ohq
            , oe_order_lines_all olq
    where   oh.header_id = ol.header_id
    and     ol.line_id = p_line_id
    and     ol.header_id = opa.header_id
    and     ol.line_id= opa.line_id
    and     opa.list_line_type_code != 'TAX'
    and     opa.list_line_id = qll.list_line_id
    and     qll.list_header_id = qlh.list_header_id
    and     qlh.attribute10 = 'Vendor Quote'
    and     qlh.attribute11 = ohq.order_number
    and     ohq.header_id = olq.header_id
    and     ol.inventory_item_id = olq.inventory_item_id
    and     ol.ship_from_org_id = olq.ship_from_org_id
    and     rownum = 1;*/--Commented by Ram Talluri for TMS 20140602-00208 on 6/3/2014
    
    --below logic added by Ram Talluri for TMS 20140602-00208 on 6/3/2014
    /* SELECT xxcpl.special_cost
      INTO l_vendor_quote_cost
      FROM apps.oe_price_adjustments adj,
           apps.qp_list_lines qll,
           apps.qp_list_headers qlh,
           apps.xxwc_om_contract_pricing_hdr xxcph,
           apps.xxwc_om_contract_pricing_lines xxcpl
     WHERE xxcph.agreement_id = TO_CHAR (qlh.attribute14)
       AND xxcpl.agreement_id = xxcph.agreement_id
       AND xxcpl.agreement_type = 'VQN'
       AND qlh.list_header_id = qll.list_header_id
       AND qlh.list_header_id = adj.list_header_id
       AND qll.list_line_id = adj.list_line_id
       AND adj.list_line_type_code = 'DIS'
       AND adj.applied_flag = 'Y'
       AND adj.line_id = p_line_id
       AND xxcpl.agreement_line_id = TO_CHAR (qll.attribute2)
       AND ROWNUM = 1;*/--commented for TMS 20140627-00126 by Ram Talluri on 6/27/2014
       
  l_err_callpoint:='Deriving special cost';     
  SELECT TO_NUMBER (qll.ATTRIBUTE4)--Special Cost
  INTO l_vendor_quote_cost
  FROM qp_qualifiers qq,
       qp_list_headers qlh,
       qp_list_lines qll,
       apps.xxwc_om_contract_pricing_lines xxcpl,
       apps.xxwc_om_contract_pricing_hdr xxcph,
       mtl_system_items_b_kfv msi,
       ra_customer_trx_lines_all ril,
       po_vendors pov,
       mtl_parameters mp
 WHERE     1 = 1
       AND qq.qualifier_context = 'CUSTOMER'
       AND qq.comparison_operator_code = '='
       AND qq.active_flag = 'Y'
       AND (   (    qlh.attribute10 = ('Contract Pricing')
                AND qq.qualifier_attribute = 'QUALIFIER_ATTRIBUTE11'
                AND (qq.qualifier_attr_value =
                        TO_CHAR (ril.ship_to_site_use_id)))
            OR (    qlh.attribute10 = ('Contract Pricing')
                AND qq.qualifier_attribute = 'QUALIFIER_ATTRIBUTE32'
                AND (qq.qualifier_attr_value =
                        TO_CHAR (ril.ship_to_customer_id)))
            OR (    qlh.attribute10 = ('CSP-NATIONAL')
                AND qq.qualifier_attribute = 'QUALIFIER_ATTRIBUTE32'
                AND (qq.qualifier_attr_value =
                        TO_CHAR (ril.ship_to_customer_id))))
       AND qlh.list_header_id = qq.list_header_id
       AND qlh.active_flag = 'Y'
       AND xxcph.agreement_id = TO_CHAR (qlh.attribute14)
       AND xxcpl.agreement_id = xxcph.agreement_id
       AND xxcpl.agreement_type = 'VQN'
       AND qlh.list_header_id = qll.list_header_id
       AND xxcpl.agreement_line_id = TO_CHAR (qll.attribute2)
       AND msi.inventory_item_id = ril.inventory_item_id
       AND xxcpl.product_value = msi.segment1
       AND msi.organization_id = ril.warehouse_id
       AND pov.vendor_id = xxcpl.vendor_id
       AND mp.organization_id = msi.organization_id
       AND ril.INTERFACE_LINE_ATTRIBUTE6 = TO_CHAR (p_line_id)
       AND TRUNC (ril.creation_date) BETWEEN NVL (qll.start_date_active,
                                                  TRUNC (ril.creation_date-1))
                                         AND NVL (qll.end_date_active,
                                                  TRUNC (ril.creation_date+1))
       AND ROWNUM = 1;--Added for TMS 20140627-00126 by Ram Talluri on 6/27/2014


    return l_vendor_quote_cost;
   EXCEPTION
   WHEN OTHERS THEN
    RETURN NULL;
    --Added by Ram Talluri for TMS 20140602-00208 on 6/3/2014
    xxcus_error_pkg.xxcus_error_main_api (
         p_called_from         => l_err_callfrom,
         p_calling             => l_err_callpoint,
         p_ora_error_msg       => SQLERRM,
         p_error_desc          => l_message,
         p_distribution_list   => l_distro_list,
         p_module              => 'XXWC');
   END get_vendor_quote_cost;

   FUNCTION get_aged_inventory_rcvd (p_inventory_item_id IN NUMBER
                                    , p_organization_id IN NUMBER
                                    , p_from_age IN NUMBER
                                    , p_to_age IN NUMBER
                                    , p_above_age IN NUMBER)
    RETURN NUMBER
    IS
        l_aged_inventory    number;
    BEGIN
        l_aged_inventory := null;

        if p_from_age is not null and p_to_age is not null
        then
            select  sum(primary_transaction_quantity)
            into    l_aged_inventory
            from    xxwcinv_inv_onhand_mv
            where   inventory_item_id = p_inventory_item_id
            and     organization_id = p_organization_id
            and     ( trunc(sysdate) - trunc(orig_date_received) ) between p_from_age and p_to_age;
        elsif p_above_age is not null then
            select  sum(primary_transaction_quantity)
            into    l_aged_inventory
            from    xxwcinv_inv_onhand_mv
            where   inventory_item_id = p_inventory_item_id
            and     organization_id = p_organization_id
            and     ( trunc(sysdate) - trunc(orig_date_received) ) > p_above_age;
        else
            select  sum(primary_transaction_quantity)
            into    l_aged_inventory
            from    xxwcinv_inv_onhand_mv
            where   inventory_item_id = p_inventory_item_id
            and     organization_id = p_organization_id;
        end if;

        return nvl(l_aged_inventory,0);

    EXCEPTION
    WHEN OTHERS THEN
        return 0;
    END get_aged_inventory_rcvd;

   FUNCTION text_to_number (
      iv_character_value   IN   VARCHAR2
   )
      RETURN NUMBER
   IS
   BEGIN
      RETURN TO_NUMBER (iv_character_value);
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN TO_NUMBER (NULL);
   END text_to_number;

   FUNCTION get_customer_name (
      in_customer_id   IN   NUMBER
   )
      RETURN VARCHAR2
   IS
    gv_return_value               VARCHAR2 (240);
   BEGIN
      SELECT hp.party_name
        INTO gv_return_value
        FROM hz_cust_accounts hca
           , hz_parties hp
       WHERE hca.party_id = hp.party_id
         AND hca.cust_account_id = in_customer_id;

      RETURN gv_return_value;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END get_customer_name;

   FUNCTION get_master_inv (in_org_id IN NUMBER DEFAULT NULL)
      RETURN NUMBER
   IS
   i_master_org_id mtl_parameters.master_organization_id%TYPE;
   BEGIN
      select max(master_organization_id)
       into i_master_org_id
       from mtl_parameters
       where organization_id = nvl(in_org_id,organization_id);
      RETURN i_master_org_id;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN TO_NUMBER (222);
   END get_master_inv;

   FUNCTION get_lookup_value (p_lookup_type in VARCHAR2
                             , p_lookup_code in VARCHAR2
                             , p_view_application_id in NUMBER)
   RETURN VARCHAR2
   IS
    l_lookup_meaning    varchar2(80);
   BEGIN

    l_lookup_meaning := null;

    select  meaning
    into    l_lookup_meaning
    from    fnd_lookup_values
    where   lookup_type = p_lookup_type
    and     lookup_code = p_lookup_code
    and     view_application_id = nvl(p_view_application_id, view_application_id)
    and     rownum = 1;

    return (l_lookup_meaning);

   EXCEPTION
   WHEN OTHERS THEN
    return (p_lookup_code);
   END get_lookup_value;

   FUNCTION get_prev_periods_sales (p_customer_id   in NUMBER
                                    , p_prior_months_count in NUMBER
                                    , p_org_id  in NUMBER)
   RETURN NUMBER
   IS
    l_total_period_sales NUMBER;
   BEGIN

    l_total_period_sales := null;

    select  sum(amount_due_original)
    into    l_total_period_sales
    from    ar_payment_schedules_all apsa
    where   apsa.class not in ('PMT')
    and     apsa.org_id = p_org_id
    and     apsa.customer_id = p_customer_id
    and     trunc(apsa.trx_date) between (add_months(trunc(sysdate), p_prior_months_count)) and trunc(sysdate);

    return (nvl(l_total_period_sales,0));

   EXCEPTION
   WHEN OTHERS THEN
    return (0);
   END get_prev_periods_sales;

   FUNCTION get_site_type_credit_limit  (p_cust_account_id in NUMBER
                                        , p_site_use_id in NUMBER
                                        , p_site_type in VARCHAR2)
   RETURN NUMBER
   IS
    l_credit_limit number;
   BEGIN
    l_credit_limit := null;

    select  sum(overall_credit_limit)
    into    l_credit_limit
    from    (
                select  distinct(hcasa.cust_account_id)
                        , hcasa.party_site_id
                        , hcsua.site_use_id
                        , hcpa.overall_credit_limit
                from    hz_cust_acct_sites_all hcasa
                        , hz_cust_site_uses_all hcsua
                        , hz_cust_profile_amts hcpa
                where   hcasa.cust_account_id = p_cust_account_id
                and     hcasa.cust_acct_site_id = hcsua.cust_acct_site_id
                and     hcsua.site_use_id = nvl(p_site_use_id, hcsua.site_use_id)
                and     nvl(hcsua.attribute1,'ZZZ') = nvl(nvl(p_site_type, hcsua.attribute1),'ZZZ')
                and     hcasa.cust_account_id = hcpa.cust_account_id
                and     hcsua.site_use_id = hcpa.site_use_id
            );

    return nvl(l_credit_limit,0);

   EXCEPTION
   WHEN OTHERS THEN
    RETURN 0;
   END get_site_type_credit_limit;

   -- 02/18/13 CG: TMS 20130218-01098. Altered to pass supplier number, or site or name (for performance)
   FUNCTION get_default_supplier (p_inventory_item_id in NUMBER
                                , p_organization_id in NUMBER
                                , p_return_type in VARCHAR2)
   RETURN VARCHAR2
   IS
    l_default_supplier VARCHAR2(45);
    l_default_site     VARCHAR2(15);
    l_supplier_name    VARCHAR2(240);
   BEGIN

    l_default_supplier := null;
    l_default_site      := null;
    l_supplier_name     := null;

    begin
        SELECT    -- (asa.segment1||'-'||assa.vendor_site_id)
                  asa.segment1
                  , assa.vendor_site_code
                  , asa.vendor_name
             INTO   l_default_supplier
                    , l_default_site
                    , l_supplier_name
             FROM   mrp_sr_assignments msra,
                    mrp_sourcing_rules msr,
                    mrp_sr_receipt_org msro,
                    mrp_sr_source_org msso,
                    ap_suppliers asa,
                    ap_supplier_sites_all assa
            WHERE   msra.sourcing_rule_id = msr.sourcing_rule_id
            AND     msr.sourcing_rule_id = msro.sourcing_rule_id
            AND     msro.sr_receipt_id = msso.sr_receipt_id
            AND     msso.vendor_id = asa.vendor_id
            AND     msso.vendor_site_id = assa.vendor_site_id
            AND     asa.vendor_id = assa.vendor_id
            AND     msra.inventory_item_id = p_inventory_item_id
            AND     msra.organization_id = p_organization_id
            AND     rownum = 1;
    exception
    when no_data_found then
        begin
            SELECT    -- (asa.segment1||'-'||assa.vendor_site_id)
                      asa.segment1
                      , assa.vendor_site_code
                      , asa.vendor_name
                 INTO   l_default_supplier
                        , l_default_site
                        , l_supplier_name
                 FROM   mrp_sr_assignments msra,
                        mrp_sourcing_rules msr,
                        mrp_sr_receipt_org msro,
                        mrp_sr_source_org msso,
                        ap_suppliers asa,
                        ap_supplier_sites_all assa
                WHERE   msra.sourcing_rule_id = msr.sourcing_rule_id
                AND     msr.sourcing_rule_id = msro.sourcing_rule_id
                AND     msro.sr_receipt_id = msso.sr_receipt_id
                AND     msso.vendor_id = asa.vendor_id
                AND     msso.vendor_site_id = assa.vendor_site_id
                AND     asa.vendor_id = assa.vendor_id
                AND     msra.inventory_item_id = p_inventory_item_id
                AND     rownum = 1;
        exception
        when others then
            l_default_supplier := null;
            l_default_site     := null;
            l_supplier_name := null;
        end;
    when others then
        l_default_supplier := null;
        l_default_site     := null;
        l_supplier_name := null;
    end;

    if nvl(p_return_type, 'IDENTIFIER')  = 'IDENTIFIER'
    then
        return l_default_supplier;
    elsif nvl(p_return_type, 'IDENTIFIER')  = 'SITE'
    then
        return  l_default_site;
    else
        return l_supplier_name;
    end if;
   EXCEPTION
   WHEN OTHERS THEN
    return null;
   END get_default_supplier;

   FUNCTION get_branch_num_from_lob (p_branch_lob IN VARCHAR2)
   RETURN VARCHAR2
   IS
    l_branch_num varchar2(6);
   BEGIN

    l_branch_num := null;

    select  x1.lob_branch
    into    l_branch_num
    from    xxcus.xxcus_location_code_tbl x1
    where   x1.entrp_loc = p_branch_lob
    and     rownum = 1;

    return l_branch_num;

   EXCEPTION
   WHEN OTHERS THEN
    return p_branch_lob;
   END get_branch_num_from_lob;

   FUNCTION get_prism_inv_house_rep (p_invoice_number IN VARCHAR2)
   RETURN VARCHAR2
   IS
    l_house_acct_number varchar2(30);
   BEGIN
    l_house_acct_number := null;

    select  to_char(primary_salesrep_id)
    into    l_house_acct_number
    from    XXWC.XXWC_AR_INV_STG_TBL
    where   trx_number = p_invoice_number
    and     nvl(stg_attribute4, '0') = '1'
    and     rownum = 1;

    return l_house_acct_number;
   EXCEPTION
   WHEN OTHERS THEN
    return null;
   END get_prism_inv_house_rep;

   procedure exec_dynamic_sql (sql_query    IN  VARCHAR2,
                                char_qty    OUT VARCHAR2)
   is
     l_query VARCHAR2(4000);
   BEGIN
      l_query := sql_query;

      EXECUTE IMMEDIATE l_query INTO char_qty;

   EXCEPTION
   WHEN OTHERS THEN
    char_qty := NULL;
   END exec_dynamic_sql;

--USED FOR CP TO REFRESH THE VIEW FROM APPS OFF SCHEDULE
--xxwc_mv_routines_pkg.XXWC_AR_CUST_BAL_MV_REFRESH
PROCEDURE XXWC_AR_CUST_BAL_MV_REFRESH (p_errbuf OUT VARCHAR2, p_retcode OUT VARCHAR2)
IS
    L_ERRBUF   CLOB;
BEGIN
   xxeis.xxwc_eis_customer_balance_v2.main (p_errbuf, p_retcode ,70);
EXCEPTION
    WHEN OTHERS
    THEN


        L_ERRBUF :=
               L_ERRBUF
            || 'Error_Stack...'
            || CHR (10)
            || DBMS_UTILITY.FORMAT_ERROR_STACK ()
            || CHR (10)
            || ' Error_Backtrace...'
            || CHR (10)
            || DBMS_UTILITY.FORMAT_ERROR_BACKTRACE ();
        FND_FILE.PUT_LINE (FND_FILE.LOG, '*Message from XXWC_AR_CUSTOMER_BALANCE_MV_REFRESH ' || L_ERRBUF);
         p_retcode :=  '2';
         p_errbuf:=L_ERRBUF;
END XXWC_AR_CUST_BAL_MV_REFRESH;


   FUNCTION get_cust_high_balance (p_cust_account_id IN NUMBER)
   RETURN NUMBER
   is
    l_max_balance number;
   begin

    l_max_balance := null;

    SELECT     max(cum_balance)
      INTO     l_max_balance
      FROM     (SELECT  customer_id
                        , invoice_currency_code
                        , as_of_date
                        , SUM(net_amount)
                          OVER (
                             PARTITION BY customer_id
                                        , invoice_currency_code
                             ORDER BY
                                customer_id
                              , invoice_currency_code
                             ROWS UNBOUNDED PRECEDING
                          ) cum_balance
                FROM   (  SELECT    customer_id
                                    , invoice_currency_code
                                    , as_of_date
                                    , SUM (net_amount) net_amount
                            FROM   (  SELECT   ps.customer_id
                                             , ps.invoice_currency_code
                                             , ps.trx_date as_of_date
                                             , SUM (ps.amount_due_original) net_amount
                                        FROM   ar_payment_schedules_all ps
                                       WHERE   ps.class IN ('INV', 'CM', 'DM', 'DEP', 'BR', 'CB')
                                               AND ps.customer_id > 0
                                    GROUP BY   ps.customer_id
                                             , ps.invoice_currency_code
                                             , ps.trx_date
                                    UNION ALL
                                      SELECT   ps.customer_id
                                             , ps.invoice_currency_code
                                             , ra.apply_date as_of_date
                                             , SUM(-ra.amount_applied
                                                   - NVL (ra.earned_discount_taken
                                                        , 0)
                                                   - NVL (ra.unearned_discount_taken
                                                        , 0))
                                                  net_amount
                                        FROM   ar_payment_schedules_all ps
                                             , ar_receivable_applications_all ra
                                       WHERE   ps.payment_schedule_id =
                                                  ra.applied_payment_schedule_id
                                               AND ps.customer_id > 0
                                               AND ra.status = 'APP'
                                               AND ra.application_type = 'CASH'
                                               AND NVL (ra.confirmed_flag, 'Y') = 'Y'
                                               AND ps.class IN ('INV', 'CM', 'DM', 'DEP', 'BR', 'CB')
                                    GROUP BY   ps.customer_id
                                             , ps.invoice_currency_code
                                             , ra.apply_date
                                    UNION ALL
                                      SELECT   ps.customer_id
                                             , ps.invoice_currency_code
                                             , adj.apply_date as_of_date
                                             , SUM (adj.amount)
                                        FROM   ar_payment_schedules_all ps
                                             , ar_adjustments_all adj
                                       WHERE   ps.payment_schedule_id =
                                                  adj.payment_schedule_id
                                               AND ps.class IN ('INV', 'CM', 'DM', 'DEP', 'BR', 'CB')
                                               AND adj.status = 'A'
                                               AND ps.customer_id > 0
                                    GROUP BY   ps.customer_id
                                             , ps.invoice_currency_code
                                             , adj.apply_date)
                        GROUP BY   customer_id
                                 , invoice_currency_code
                                 , as_of_date
                        ORDER BY   customer_id
                                 , invoice_currency_code
                                 , as_of_date))
     where  customer_id = p_cust_account_id;

    return nvl(l_max_balance,0);

   exception
   when others then
    return 0;
   end get_cust_high_balance;

   FUNCTION get_cust_in_proc_bal (p_cust_account_id IN NUMBER)
   RETURN NUMBER
   is
    l_in_proc_balance number;
   begin
    l_in_proc_balance := null;

    select sum(d.ordered_quantity*nvl(d.unit_selling_price,0))
      into l_in_proc_balance
      from ont.oe_order_headers_all b,
           apps.org_organization_definitions a,
           ont.oe_transaction_types_tl c,
           ont.oe_order_lines_all d
    where a.organization_id=d.ship_from_org_id
      and c.transaction_type_id = b.order_type_id
      and b.flow_status_code not in ('ENTERED','CANCELLED','DRAFT')
      and b.header_id=d.header_id
      and d.flow_status_code not in ('ENTERED','CANCELLED','DRAFT')
      and b.ordered_date is not null
      and not exists (select 'x' from ra_customer_trx_all ih, ra_customer_trx_lines_all il
                       where ih.customer_trx_id=il.customer_trx_id
                         and ih.interface_header_context='ORDER ENTRY'
                         and il.interface_line_context='ORDER ENTRY'
                         and il.interface_line_attribute1=b.order_number
                         and il.interface_line_attribute10=to_char(d.ship_from_org_id)
                         and il.interface_line_attribute6=to_char(d.line_id)
                         and il.line_type='LINE'
                         )
      and not exists (select 'x' from ra_interface_lines_all il
                       where il.interface_line_context='ORDER ENTRY'
                         and il.interface_line_attribute1=b.order_number
                         and il.interface_line_attribute10=to_char(d.ship_from_org_id)
                         and il.interface_line_attribute6=to_char(d.line_id)
                         and il.line_type='LINE'
                         )
      and b.sold_to_org_id = p_cust_account_id;

    return nvl(l_in_proc_balance,0);
   exception
   when others then
    return 0;
   end get_cust_in_proc_bal;

   FUNCTION get_edw_cust_pb_site (p_cust_account_id IN NUMBER
                                   , p_org_id   IN NUMBER)
   RETURN VARCHAR2
   IS
    v_pb_site varchar2(50);
   BEGIN
    v_pb_site := null;

    select  to_char(hps.party_site_number||hcsua.site_use_id)
    into    v_pb_site
    from    hz_cust_accounts hca
            , hz_cust_acct_sites_all hcasa
            , hz_party_sites hps
            , hz_cust_site_uses_all hcsua
    where   hca.cust_account_id = hcasa.cust_account_id
    and     hcasa.party_site_id = hps.party_site_id
    and     hcasa.cust_acct_site_id = hcsua.cust_acct_site_id
    and     hcsua.status = 'A'
    and     hcsua.site_use_code = 'BILL_TO'
    and     hcsua.primary_flag = 'Y'
    and     hca.cust_account_id = p_cust_account_id
    and     hcasa.org_id = p_org_id;

    return v_pb_site;
   EXCEPTION
   WHEN OTHERS THEN
    return null;
   END get_edw_cust_pb_site;

   FUNCTION get_ap_invoice_payment_amts (p_invoice_id   IN NUMBER
                                        , p_org_id     IN NUMBER
                                        , p_return_type IN VARCHAR2)
   RETURN NUMBER
   IS
    l_payment_amount    number;
    l_discount_amount    number;
   BEGIN

    l_payment_amount    := 0;
    l_discount_amount   := 0;

    select  sum(nvl(aip.amount,0))
            , sum(nvl(discount_taken,0))
    into    l_payment_amount
            , l_discount_amount
    from    ap_invoice_payments_all aip
            , ap_checks_all aca
    where   aip.invoice_id = p_invoice_id
    and     aip.org_id = p_org_id
    and     NVL (aip.reversal_flag(+), 'N') = 'N'
    and     aip.check_id = aca.check_id(+)
    and     aca.status_lookup_code(+) != 'VOIDED'
    and     aca.void_date(+) IS NULL;

    IF p_return_type = 'PAYMENT' THEN
        return nvl(l_payment_amount,0);
    ELSIF p_return_type = 'DISCOUNT' THEN
        return nvl(l_discount_amount,0);
    ELSE
        return 0;
    END IF;

   EXCEPTION
   WHEN OTHERS THEN
    return 0;
   END get_ap_invoice_payment_amts;

   FUNCTION get_ap_invoice_payment_info (p_invoice_id   IN NUMBER
                                        , p_org_id      IN NUMBER
                                        , p_return_type IN VARCHAR2)
   RETURN VARCHAR2
   IS
    l_accounting_date   date;
    l_payment_number    number;
    l_check_number      number;
   BEGIN
    l_accounting_date   := null;
    l_payment_number    := null;
    l_check_number      := null;

    select  payment_date
            , payment_num
            , check_number
    into    l_accounting_date
            , l_payment_number
            , l_check_number
    from    (select aip.accounting_date payment_date
                    , aip.payment_num
                    , aca.check_number
            from    ap_invoice_payments_all aip
                    , ap_checks_all aca
            where   aip.invoice_id = p_invoice_id
            and     aip.org_id = p_org_id
            and     NVL (aip.reversal_flag(+), 'N') = 'N'
            and     aip.check_id = aca.check_id(+)
            and     aca.status_lookup_code(+) != 'VOIDED'
            and     aca.void_date(+) IS NULL
            order by aip.invoice_payment_id desc)
    where rownum = 1;

    IF p_return_type = 'ACCOUNTING_DATE' THEN
        return to_char(l_accounting_date,'MM/DD/YYYY');
    ELSIF p_return_type = 'PAYMENT_NUM' THEN
        return to_char(l_payment_number);
    ELSIF p_return_type = 'CHECK_NUM' THEN
        return to_char(l_check_number);
    ELSE
        return null;
    END IF;

   EXCEPTION
   WHEN OTHERS THEN
    return null;
   END get_ap_invoice_payment_info;

   FUNCTION get_edw_supp_prim_pay_site (p_vendor_num IN VARCHAR2
                                        , p_org_id IN NUMBER)
   RETURN VARCHAR2
   IS
    l_site_identifier varchar2(80);
   BEGIN
    l_site_identifier := null;

    select  x1.site_identifier
    into    l_site_identifier
    from    (
                select  (asa.segment1||'-'||assa.vendor_site_id) site_identifier
                from    ap_suppliers asa
                        , ap_supplier_sites_all assa
                where   asa.segment1 = p_vendor_num
                and     asa.vendor_id = assa.vendor_id
                and     assa.org_id = p_org_id
                and     nvl(assa.pay_site_flag, 'N') = 'Y'
                order by nvl(assa.primary_pay_site_flag, 'N')
            ) x1
    where   rownum = 1;

    return l_site_identifier;

   EXCEPTION
   WHEN OTHERS THEN
    RETURN NULL;
   END get_edw_supp_prim_pay_site;

   -- 01/30/13 CG: TMS 20130122-01578 Retrieve employees branch location
   FUNCTION get_emp_inv_branch_loc (p_employee_id IN NUMBER)
   RETURN VARCHAR2
   IS
    l_branch_id varchar2(3);
   BEGIN
    l_branch_id := null;

    select  mp.organization_code
    into    l_branch_id
    from    per_assignments_f paf
            , hr_locations_all hla
            , mtl_parameters mp
    where   paf.person_id = p_employee_id
    and     TRUNC (paf.effective_start_date) <= TRUNC (SYSDATE)
    and     NVL (TRUNC (paf.effective_end_date), TRUNC (SYSDATE)) >= TRUNC (SYSDATE)
    and     paf.location_id = hla.location_id
    and     hla.inventory_organization_id = mp.organization_id
    and     rownum = 1;

    return l_branch_id;

   EXCEPTION
   WHEN OTHERS THEN
    RETURN NULL;
   END get_emp_inv_branch_loc;


   -- 02/18/13 CG: TMS 20130218-01098: Added function to pull last supplier for SPECIAL items
   FUNCTION get_special_last_pay_site (p_inventory_item_id IN NUMBER
                                       , p_organization_id IN NUMBER
                                       , p_operating_unit_id IN NUMBER)
   RETURN VARCHAR2
   IS
    l_last_po_header_id NUMBER;
    l_pay_site          VARCHAR2(50);
   BEGIN

    l_last_po_header_id := null;
    l_pay_site          := null;

    begin
        select  max(pla.po_header_id)
        into    l_last_po_header_id
        from    po_lines_all pla
                , po_line_locations_all plla
        where   nvl(pla.cancel_flag, 'N') = 'N'
        and     pla.org_id = p_operating_unit_id
        and     pla.item_id = p_inventory_item_id
        and     pla.po_line_id = plla.po_line_id
        and     plla.ship_to_organization_id = p_organization_id
        and     nvl(plla.cancel_flag, 'N') = 'N';
    exception
    when others then
        l_last_po_header_id := null;
    end;

    if l_last_po_header_id is not null then
        begin
            select  x1.pay_site_code
            into    l_pay_site
            from    (
                        select  (asa.segment1||'-'||assa.vendor_site_id) pay_site_code
                        from    po_headers_all pha
                                , ap_suppliers asa
                                , ap_supplier_sites_all assa
                        where   pha.po_header_id = l_last_po_header_id
                        and     pha.vendor_id = asa.vendor_id
                        and     asa.vendor_id = assa.vendor_id
                        and     assa.org_id = p_operating_unit_id
                        and     nvl(assa.pay_site_flag, 'N') = 'Y'
                        order by assa.primary_pay_site_flag desc
                    ) x1
            where   rownum = 1;
        exception
        when others then
            l_pay_site := null;
        end;
    end if;

    return l_pay_site;

   EXCEPTION
   WHEN OTHERS THEN
     RETURN NULL;
   END get_special_last_pay_site;

-- 03/26/13 CG: TMS 20130121-00481: Added function to retrieve a contact number (person/site/party) based on it's position or number in a full pull
-- Used by the new custom contact from ONLY
FUNCTION XXWC_CONTACT_POINT_FNC (
    P_OWNER_TABLE_NAME VARCHAR2
    , P_OWNER_TABLE_ID NUMBER
    , P_CONTACT_TYPE VARCHAR2
    , P_ROWNUM    NUMBER
) RETURN VARCHAR2 IS
   /*************************************************************************
    Copyright (c) 2012 Lucidity Consulting Group
    All rights reserved.
   **************************************************************************
     $Header xxwc_mv_routines_pkg $
     Module Name: xxwc_mv_routines_pkg.xxwc_contact_point_fnc

     PURPOSE:   Used by the new customer contact form ONLY

     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.0        01/11/2012  Consuelo Gonzalez      Initial Version
	 4.0        09/04/2015  Manjula Chellappan     TMS 20150901-00161 - XXWC_CUST_CONTACTS Performance Tuning
	 5.0        11/06/2015  Pattabhi Avula         TMS 20150722-00210 - OM - Ability to edit contacts from the sales order (job on the fly)

   **************************************************************************/
 l_email    VARCHAR2 (2000);
 l_phone_number VARCHAR2 (240);
 l_phone_extension VARCHAR2 (50);  -- Added for Rev 5.0
 l_phone_area_code VARCHAR2 (10); -- Added for Rev 5.0
 l_phone_frmt  VARCHAR2 (100);
BEGIN

    l_email := NULL;
    l_phone_number := NULL;
	l_phone_extension  := NULL;  -- Added for Rev 5.0
    l_phone_area_code := NULL; -- Added for Rev 5.0
	l_phone_frmt := NULL;

    SELECT   qrslt.email_address
	        ,qrslt.phone_extension
			,qrslt.phone_area_code
			,qrslt.phone_number
                    -- , qrslt.formatted_phone_number -- Commented for Rev 4.0 
					-- Added for Rev 4.0 Begin
					-- Commented as per 5.0 Begin
				 /*,(CASE WHEN phone_extension IS NOT NULL
				 -- Commented as per 5.0 Begin
                       -- then (xxwc_mv_routines_pkg.format_phone_number (raw_phone_number)||' X '||phone_extension)
                       -- else xxwc_mv_routines_pkg.format_phone_number (raw_phone_number)
					   THEN (xxwc_mv_routines_pkg.format_phone_number (phone_number)||' X '||phone_extension)
                        ELSE xxwc_mv_routines_pkg.format_phone_number (phone_number)
						 -- Commented as per 5.0 End
                      END)  -- Commented as per 5.0 Begin */
				   -- Added for Rev 4.0 End
        INTO      l_email
		         ,l_phone_extension  -- Added for Rev 5.0
				 ,l_phone_area_code  -- Added for Rev 5.0
                 ,l_phone_number
    FROM   (SELECT   hcp.contact_point_id
                   , hcp.contact_point_type
                   , hcp.status
                   , hcp.owner_table_name
                   , hcp.owner_table_id
                   , hcp.primary_flag
                   , nvl(hcp.phone_line_type, hcp.contact_point_type) contact_type
				   -- Commented for Rev 4.0 Begin
                  /* , (case when hcp.phone_extension is not null
                        then (xxwc_mv_routines_pkg.format_phone_number (hcp.raw_phone_number)||' X '||hcp.phone_extension)
                        else xxwc_mv_routines_pkg.format_phone_number (hcp.raw_phone_number)
                      end) formatted_phone_number
					  */
				   -- Commented for Rev 4.0 End
                   , hcp.raw_phone_number
				   , hcp.phone_number  -- Added for Rev 5.0
				   , hcp.phone_area_code  -- Added for Rev 5.0
				   , hcp.phone_extension -- Added for Rev 4.0
                   , hcp.email_address
                   , hcp.object_version_number
                   , row_number() over ( partition by hcp.owner_table_name, hcp.owner_table_id, nvl(hcp.phone_line_type, hcp.contact_point_type) order by hcp.contact_point_id ) rn
              FROM     hz_contact_points hcp
             WHERE       hcp.contact_point_type in ('PHONE','EMAIL')
			         AND hcp.primary_flag='Y' -- Ver 5.0
                     AND hcp.status = 'A') qrslt
   WHERE owner_table_name = P_OWNER_TABLE_NAME
   AND   owner_table_id = P_OWNER_TABLE_ID
   AND   contact_type = P_CONTACT_TYPE
   AND   rn = P_ROWNUM;
   
   -- Added for Rev 5.0 Begin
      IF l_phone_area_code IS NULL THEN
	    l_phone_frmt:= SUBSTR (REGEXP_REPLACE (l_phone_number,
                                                 '[(]|[)]|[-]|[ ]|[.]',
                                                 ''
                                                ),
                                 1,
                                 3
                                )
                      || '-'
                      || SUBSTR (REGEXP_REPLACE (l_phone_number,
                                                 '[(]|[)]|[-]|[ ]|[.]',
                                                 ''
                                                ),
                                 4
                                );
		    IF l_phone_extension IS NOT NULL THEN						
		       l_phone_number:=(l_phone_frmt||' X '||l_phone_extension);
			ELSE 
               l_phone_number:=l_phone_frmt;
            END IF;			   
	  ELSE
	    
         l_phone_frmt:=('('||SUBSTR (REGEXP_REPLACE (l_phone_area_code||l_phone_number,
                                                 '[(]|[)]|[-]|[ ]|[.]',
                                                 ''
                                                ),
                                 1,
                                 3
                                )
                      || ') '
                      || SUBSTR (REGEXP_REPLACE (l_phone_area_code||l_phone_number,
                                                 '[(]|[)]|[-]|[ ]|[.]',
                                                 ''
                                                ),
                                 4,
                                 3
                                )
                      || '-'
                      || SUBSTR (REGEXP_REPLACE (l_phone_area_code||l_phone_number,
                                                 '[(]|[)]|[-]|[ ]|[.]',
                                                 ''
                                                ),
                                 7
                                )
                     );
					 
				IF l_phone_extension IS NOT NULL THEN						
		           l_phone_number:=(l_phone_frmt||' X '||l_phone_extension);
			    ELSE 
                   l_phone_number:=l_phone_frmt;
                END IF;	 
        
      END IF;		
      
   -- Added for Rev 5.0 End
   

     if P_CONTACT_TYPE = 'EMAIL' then
             return l_email;
     else
             return l_phone_number;
     end if;

EXCEPTION
WHEN OTHERS THEN
    RETURN NULL;
END XXWC_CONTACT_POINT_FNC;


--02/04/2013:Harsha--TMS 20140130-00206 : Added function to retrieve order method details

FUNCTION XXWC_order_method_FNC (
    P_Source_id number,
  P_Attribute2 varchar2
) RETURN VARCHAR2
is
v_order_source   varchar2(50);
v_shipping_method  varchar2(50);
v_order_type       varchar2(50);
v_line_id          varchar2(100);

begin

begin
select name into v_order_source  from oe_order_sources where order_source_id=P_Source_id;
exception
when others then
v_order_source:=null;
end;

if v_order_source ='SpeedBuild' then
return 'SpeedBuild';
ELSE
return P_Attribute2;
end if;

exception
when others then
return null;

end XXWC_order_method_FNC;


/*
function get_avg_days_late(p_cust_account_id IN NUMBER)
RETURN number
is

begin

    return ();

exception
when others then
    return(NULL);
end get_avg_days_late;
*/

END xxwc_mv_routines_pkg;
/