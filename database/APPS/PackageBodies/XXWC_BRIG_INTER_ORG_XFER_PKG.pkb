CREATE OR REPLACE PACKAGE BODY APPS.XXWC_BRIG_INTER_ORG_XFER_PKG AS

   /*************************************************************************
   *   $Header XXWC_BRIG_INTER_ORG_XFER_PKG.PKG $
   *   Module Name: XXWC_BRIG_INTER_ORG_XFER_PKG.PKG
   *
   *   PURPOSE:   This package is used for Inter Org Transfer of Brigade Items
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        07/11/2014  Gopi Damuluri             Initial Version  
   *   1.1        02/10/2014  Gopi Damuluri             TMS# 20150210-00099 Changed the
   *                                                    req_number_segment1 and deliver_to_requestor_name
   *                                                    values and other values as per TMS request   
   * ***************************************************************************/

   pl_dflt_email              fnd_user.email_address%TYPE := 'HDSOracleDevelopers@hdsupply.com';

   PROCEDURE load_interface (p_item_num                IN  VARCHAR2,
                             p_src_org                 IN  VARCHAR2,
                             p_dest_org                IN  VARCHAR2,
                             p_location                IN  VARCHAR2,
                             p_sub_inv                 IN  VARCHAR2,
                             p_quantity                IN  NUMBER)
    /**************************************************************************
     *
     * PROCEDURE   : load_interface
     * DESCRIPTION : Called by Web ADI and loads Requisitions into 
     *               PO_REQUISITIONS_INTERFACE_ALL
     *
     * REVISIONS   :
     * Ver        Date        Author                     Description
     * ---------  ----------  ---------------         -------------------------
     * 1.0        07/11/2014  Gopi Damuluri             Initial Version
     *************************************************************************/
               IS
      l_error_message         VARCHAR2(3000);
      l_employee_id           NUMBER  := 52989;
      l_user_id               NUMBER  := 9489;
      l_expense_acct          NUMBER  := 54562;
      l_location_id           NUMBER;   -- Version# 1.1  TMS# 20150210-00099
      l_program_err           EXCEPTION;   -- Version# 1.1  TMS# 20150210-00099
   BEGIN

/*
     BEGIN
        SELECT employee_id
             , user_id
          INTO l_employee_id
             , l_user_id
          FROM fnd_user
         WHERE 1 = 1 --user_id = fnd_global.user_id;
           AND user_name = '10010621';
     EXCEPTION
     WHEN OTHERS THEN
       NULL;
     END;

     BEGIN
       SELECT expense_account
         INTO l_expense_acct
         FROM mtl_system_items_b 
        WHERE 1 = 1
          AND segment1 = p_item_num
          AND organization_id = 790;
     EXCEPTION
     WHEN OTHERS THEN
       NULL;
     END;
*/
 -- Start :  Below statement added by Gopi -- Version# 1.1  TMS# 20150210-00099  
     BEGIN
    SELECT location_id
      INTO l_location_id
          FROM apps.hr_locations_all           hrl,
               apps.mtl_parameters             orgs
         WHERE hrl.inventory_organization_id = orgs.organization_id
           AND orgs.organization_code        = p_dest_org
           AND hrl.office_site_flag          = 'Y';
     EXCEPTION
     WHEN OTHERS THEN
       l_error_message :=  'Error deriving DeliverToLocation - '||SQLERRM;
       RAISE l_program_err;
     END;
   -- End :  Statement added by Gopi -- Version# 1.1  TMS# 20150210-00099  
   
        INSERT INTO PO_REQUISITIONS_INTERFACE_ALL
        ( interface_source_code
        , source_type_code
        , requisition_type
        , destination_type_code
        , item_segment1
        , quantity
        , authorization_status
        , preparer_id
        , autosource_flag
        , source_organization_code
        , source_subinventory
        , destination_organization_code
        , destination_subinventory
        , deliver_to_location_id
        , deliver_to_requestor_name
        , need_by_date
        , gl_date
        , charge_account_id
        , org_id
        , creation_date
        , created_by
        , last_update_date
        , last_updated_by
        , req_number_segment1
        )
        VALUES ('INV'                                                              -- interface_source_code
        , 'INVENTORY'                                                              -- source_type_code
        , 'INTERNAL'                                                               -- requisition_type
        , 'INVENTORY'                                                              -- destination_type_code
        ,  p_item_num                                                              -- item_segment1
        ,  p_quantity                                                              -- quantity
        ,  'APPROVED'                                                              -- authorization_status
        ,  l_employee_id                                                           -- preparer_id
        ,  'P'                                                                     -- autosource_flag
        ,  p_src_org                                                               -- source_organization_code
        ,  p_sub_inv                                                               -- source_subinventory
        ,  p_dest_org                                                              -- destination_organization_code
        ,  p_sub_inv                                                               -- destination_subinventory
        ,  l_location_id                                                           -- deliver_to_location_id
        ,  'XXWC_EMP_SUPPLYCHAIN, Dummy Employee'                                  -- deliver_to_requestor_name   -- Version# 1.1  TMS# 20150210-00099
        ,  SYSDATE                                                                 -- need_by_date
        ,  SYSDATE                                                                 -- gl_date
        ,  l_expense_acct                                                          -- charge_account_id
        ,  162                                                                     -- org_id
        ,  SYSDATE                                                                 -- creation_date
        ,  fnd_global.user_id                                                      -- created_by
        ,  SYSDATE                                                                 -- last_update_date
        ,  fnd_global.user_id                                                      -- last_updated_by
        ,  p_src_org||'MX_'||to_char(sysdate, 'MMDDYYYYHHMISS')                    -- req_number_segment1    -- Version# 1.1  TMS# 20150210-00099
        ) ;

   EXCEPTION    -- Version# 1.1  TMS# 20150210-00099
   WHEN l_program_err THEN
     FND_MESSAGE.SET_NAME('XXWC','XXWC_ADI_ERROR_MSG'); 
     FND_MESSAGE.SET_TOKEN('ERROR_MESSAGE',l_error_message); 
   WHEN OTHERS THEN
        l_error_message :=  'Error insering - '||SQLERRM;
     FND_MESSAGE.SET_NAME('XXWC','XXWC_ADI_ERROR_MSG'); 
     FND_MESSAGE.SET_TOKEN('ERROR_MESSAGE',l_error_message); 
   END LOAD_INTERFACE;

END XXWC_BRIG_INTER_ORG_XFER_PKG;
/
