create or replace package body apps.xxcus_ont_htmomse_diagnostics as
/*+
 -- Date: 14-MAR-2013
 -- Author: Balaguru Seshadri
-- Scope: This package will extract sales order info for a list of sales order and email the user for troubleshooting with Oracle support.
 -- ESMS ticket 195498
 -- Parameters: List of Sales Orders separated by commas
*/

 -- Global variables
 G_Application    Fnd_Application.Application_Short_Name%Type :='ONT';
 G_Cp_Short_Code  Fnd_Concurrent_Programs.Concurrent_Program_Name%Type :='ONTOMSE'; --Period Close Reconcialiation Report
 G_Def_UTL_Folder Varchar2(80) :='/ebizlog/tmp';

 procedure main (
    retcode               out number
   ,errbuf                out varchar2
   ,p_list_of_orders      in  varchar2 
  ) is
    
    CURSOR wc_orders IS
    select oh.header_id     order_header_id
          ,oh.order_number  order_number
    from      
    (
     select to_number(xmltbl.column_value) hds_sales_orders
     from   xmltable(p_list_of_orders) xmltbl
    ) my_orders
     ,oe_order_headers_all oh
    where 1 =1
      and my_orders.hds_sales_orders =oh.order_number;
    
    type wc_orders_tbl is table of wc_orders%rowtype index by binary_integer;
    wc_orders_rec wc_orders_tbl;        
  
   lc_request_data VARCHAR2(20) :='';
   ln_request_id   NUMBER :=0;
   n_req_id NUMBER :=0;
   l_org_id NUMBER :=0; 
   n_count_child_requests NUMBER :=0; 
   N_conc_pgm_id NUMBER :=0;
   N_running_count NUMBER :=0;
   b_keep_looping BOOLEAN;
   n_program_app_id NUMBER :=0;
   N_user_id NUMBER :=0;
   v_email       fnd_user.email_address%type :=Null;
   v_cp_long_name   fnd_concurrent_programs_tl.user_concurrent_program_name%type :=Null; 
   v_cp_phase       Varchar2(40) :=Null;
   v_cp_status      Varchar2(40) :=Null; 
   v_child_requests Varchar2(40) :='Z';
   output_file_id   UTL_FILE.FILE_TYPE;
   v_path varchar2(80);
   p_period_id NUMBER :=0;
   
  begin  
  
   lc_request_data :=fnd_conc_global.request_data;
   
   n_req_id :=fnd_global.conc_request_id;
 
   if lc_request_data is null then     
       
       lc_request_data :='1';
  
       l_org_id :=mo_global.get_current_org_id;         
       
       N_user_id :=fnd_global.user_id;             
       
       fnd_file.put_line(fnd_file.log, '');       
       fnd_file.put_line(fnd_file.log, 'p_list_of_orders ='||p_list_of_orders);              
       fnd_file.put_line(fnd_file.log, 'l_org_id ='||l_org_id);
       fnd_file.put_line(fnd_file.log, 'n_user_id ='||n_user_id);
       fnd_file.put_line(fnd_file.log, '');      
      
      Open wc_orders;
      Fetch wc_orders Bulk Collect Into wc_orders_rec;
      Close wc_orders;    
      
      If wc_orders_rec.count =0 Then
      
       fnd_file.put_line(fnd_file.log,'Cannot validate sales orders against user requested list of sales orders. Please check the technical script');
       
      Else 
      
          for indx in 1 .. wc_orders_rec.count loop
                                   
                ln_request_id :=fnd_request.submit_request
                      (
                       application      =>G_Application,
                       program          =>G_Cp_Short_Code,
                       description      =>'',
                       start_time       =>'',
                       sub_request      =>TRUE,
                       argument1        =>wc_orders_rec(Indx).order_header_id, --Sales OrderHeader Id 
                       argument2        =>''                                   --Order Line Number, lets leave it blank for all lines                                                                                                                                                                                                                                               
                      ); 
                      
                  if ln_request_id >0 then 
                    fnd_file.put_line(fnd_file.log, 'Submitted Diagnostics: HTMomse report for sales order ='
                                                     ||wc_orders_rec(Indx).order_number
                                                     ||', Request Id ='
                                                     ||ln_request_id
                                                    );
                  else
                    fnd_file.put_line(fnd_file.log, 'Failed to submit Diagnostics: HTMomse report for sales order ='
                                                     ||wc_orders_rec(Indx).order_number                                                
                                                    );
                  end if;     
                   
                 fnd_conc_global.set_req_globals(conc_status =>'PAUSED' ,request_data =>lc_request_data);
           
                 lc_request_data :=to_char(to_number(lc_request_data)+1);      
          end loop;     
                              
      End If; -- checking if wc_inv_orgs_rec.count =0      
                             
   else     
   
        Begin  
            select a.user_concurrent_program_name
                  ,c.email_address
            into   v_cp_long_name
                  ,v_email
            from fnd_amp_requests_v a
                ,fnd_concurrent_programs d
                ,fnd_user c
            where 1 =1
              and a.request_id =n_req_id
              and a.concurrent_program_id =d.concurrent_program_id
              and a.requested_by =c.user_id;                                                  
                                                                                                          
        Exception                         
         When Others Then
           Null;
        End;    
         
        Begin
        
          select '/xx_iface/'||lower(name)||'/outbound' into v_path from v$database;
          output_file_id  :=UTL_FILE.FOPEN(v_path, 'zip_file_extract_'||n_req_id||'.txt', 'w');
        
          for my_orders in wc_orders loop 
              for rec in ( select 'HTMomse12_'||my_orders.order_number||'_'||a.request_id||'.html' html_file
                           from fnd_concurrent_requests a
                           where 1 =1
                             and a.parent_request_id =n_req_id
                             and a.argument1         =my_orders.order_header_id
                         )
              loop
               begin 
                  UTL_FILE.FCOPY (
                                   src_location  =>G_Def_UTL_Folder --IN VARCHAR2,
                                  ,src_filename  =>rec.html_file    --IN VARCHAR2,
                                  ,dest_location =>v_path           --IN VARCHAR2,
                                  ,dest_filename =>rec.html_file    --IN VARCHAR2,
                                  ,start_line    =>1                --IN PLS_INTEGER DEFAULT 1,
                                  ,end_line      =>Null             --IN PLS_INTEGER DEFAULT NULL
                                 );
                                 
                  fnd_file.put_line(fnd_file.log, 'Successfully copied file '||rec.html_file||' to '||v_path);
                  
                      v_child_requests :='OK';
                      
                      UTL_FILE.PUT_LINE(output_file_id, v_path||'/'||rec.html_file);
                  
                      fnd_file.put_line(fnd_file.log, 'Noted file '||rec.html_file||' in zip_file_extract_'||n_req_id||'.txt');                  
                   /*                                 
                   begin  
                      UTL_FILE.frename
                                  (
                                     src_location   =>v_path --IN VARCHAR2,
                                    ,src_filename   =>rec.html_file    --IN VARCHAR2,
                                    ,dest_location  =>v_path           --IN VARCHAR2,
                                    ,dest_filename  =>rec.html_file||'.out'    --IN VARCHAR2,
                                    ,overwrite      =>FALSE            --IN BOOLEAN DEFAULT FALSE
                                  );
                                  
                      v_child_requests :='OK';
                      
                      UTL_FILE.PUT_LINE(output_file_id, v_path||'/'||rec.html_file||'.out');
                  
                      fnd_file.put_line(fnd_file.log, 'Noted file '||rec.html_file||' in zip_file_extract_'||n_req_id||'.txt');                      
                                                        
                   exception
                    when others then
                     fnd_file.put_line(fnd_file.log, 'Issue in renaming file '||rec.html_file||' to '||rec.html_file||'.out');
                      if v_child_requests <>'NA' then
                          v_child_requests :='NA';
                      else
                        null; --we may encounter this situation first time, so careful not to overwrite it 
                      end if;                                      
                   end;                  
                  */                                                                    
               exception
                when others then
                 fnd_file.put_line(fnd_file.log, 'Issue in copying file '||rec.html_file||' to '||v_path);
                 if v_child_requests <>'NA' then
                  v_child_requests :='NA';
                 else
                  null; --we may encounter this situation first time, so careful not to overwrite it 
                 end if;                 
               end;
              end loop;          
          end loop;  

          UTL_FILE.FCLOSE(output_file_id);
          --v_child_requests :='OK';
        Exception
         When Others Then
          v_child_requests :='NA';
        End;   
        
        If v_child_requests <>'NA' Then
        ln_request_id :=fnd_request.submit_request
              (
               application      =>'XXCUS',
               program          =>'XXCUS_PEREND_POSTPROCESSOR',
               description      =>'HDS HTMomse Email',
               start_time       =>'',
               sub_request      =>TRUE,
               argument1        =>n_req_id,
               argument2        =>v_path||'/zip_file_extract_'||n_req_id||'.txt', 
               argument3        =>v_cp_long_name,               
               argument4        =>v_email,
               argument5        =>'HDS_HTMOMSE12_ORDERS_'||n_req_id||'.zip',
               argument6        =>'HDS Diagnostics: HTMomse Sales Orders -Status Update'                                                                         
              ); 
         --commit work;                         
          if ln_request_id >0 then 
             fnd_file.put_line(fnd_file.log, 'Successfully submitted XXHDS Period End Post Processor Program');

          else
             fnd_file.put_line(fnd_file.log, 'Failed to submit XXHDS Period End Post Processor Program');
             -- Lets leave the output_emailed field to N to show we were not able to email the user regarding the status
             -- of the request and the output of the zip file.
             --This condition can also be resolved by manually submitting a program to be devloped and to process
             -- all records where output_emailed ='N' 
          end if; 
        Else --v_child_requests ='NA'
          Null;        
        End If;
           
      retcode :=0;
      errbuf :='Child request[s] completed. Exit HDS Diagnostics: HTMomse Sales Order Extract -Parent';
      fnd_file.put_line(fnd_file.log,errbuf);
      --return;
   end if;
  exception
   when others then
    fnd_file.put_line(fnd_file.log, 'Error in caller apps.xxcus_ont_htmomse_diagnostics.main ='||sqlerrm);
    rollback;
  end main;
  
end xxcus_ont_htmomse_diagnostics;
/
