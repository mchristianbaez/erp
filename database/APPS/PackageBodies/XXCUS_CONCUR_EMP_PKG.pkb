CREATE OR REPLACE PACKAGE BODY APPS.XXCUS_CONCUR_EMP_PKG AS
/**************************************************************************
   $Header XXCUS_CONCUR_EMP_PKG $
   Module Name: XXCUS_CONCUR_EMP_PKG.pks

   PURPOSE:   This package is called by the concurrent programs
              HDS CONCUR: Generate Concur Employee Export File
              for generating Employee pipe separated file.
   REVISIONS:
   Ver        Date        Author             Description
   ---------  ----------  ---------------   -------------------------
    1.0       08/05/2018  Ashwin Sridhar    Initial Build - Task ID: 20180227-00179
/*************************************************************************/

g_loc VARCHAR2(20000);

/*************************************************************************
  Procedure : Write_Log

  PURPOSE:   This procedure logs debug message in Concurrent Log file
  Parameter: IN  p_debug_msg      -- Debug Message
  REVISIONS:
  Ver        Date        Author             Description
  ---------  ----------  ---------------   -------------------------
  1.0        08/05/2018  Ashwin Sridhar     Initial Build - Task ID: 20180227-00179
************************************************************************/
PROCEDURE write_log (p_debug_msg IN VARCHAR2) IS

BEGIN
      
  fnd_file.put_line (fnd_file.LOG, p_debug_msg);
  DBMS_OUTPUT.put_line (p_debug_msg);

END write_log;

/*************************************************************************
  Procedure : write_error

  PURPOSE:   This procedure logs debug message in Concurrent Out file
  Parameter: IN  p_debug_msg      -- Debug Message
  REVISIONS:
  Ver        Date        Author             Description
  ---------  ----------  ---------------   -------------------------
  1.0        08/05/2018  Ashwin Sridhar     Initial Build - Task ID: 20180227-00179
************************************************************************/
--Add message to concurrent output file
PROCEDURE write_error (p_debug_msg IN VARCHAR2) IS

l_req_id          NUMBER := fnd_global.conc_request_id;
l_err_callfrom    VARCHAR2 (75) DEFAULT 'XXCUS_CONCUR_EMP_PKG';
l_err_callpoint   VARCHAR2 (75) DEFAULT 'START';
l_distro_list     VARCHAR2 (75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';

BEGIN

  xxcus_error_pkg.xxcus_error_main_api (
      p_called_from         => l_err_callfrom,
      p_calling             => l_err_callpoint,
      p_request_id          => l_req_id,
      p_ora_error_msg       => SUBSTR (p_debug_msg, 1, 2000),
      p_error_desc          => 'Error running XXCUS_CONCUR_EMP_PKG with PROGRAM ERROR',
      p_distribution_list   => l_distro_list,
      p_module              => 'HRMS');

END write_error;

/*************************************************************************
  Procedure : USER_EXPORT

  PURPOSE:   This procedure exports the employee data in UTL_FILE
  Parameter: IN  p_debug_msg      -- Debug Message
  REVISIONS:
  Ver        Date        Author             Description
  ---------  ----------  ---------------   -------------------------
  1.0        08/05/2018  Ashwin Sridhar     Initial Build - Task ID: 20180227-00179
************************************************************************/
PROCEDURE USER_EXPORT(p_errbuf  OUT VARCHAR2
                     ,p_retcode OUT VARCHAR2) IS

--Cursor to fetch the header
CURSOR cur_header IS
SELECT *
FROM APPS.XXCUS_CONCUR_EMP_EXPORT_100_V;

--Cursor to fetch the contractors
CURSOR cur_contractors IS
SELECT  DECODE(TRIM(TRX_TYPE),NULL,NULL,'"'||TRIM(TRX_TYPE)||'"')           TRX_TYPE
,DECODE(TRIM(EMPLOYEE_ID),NULL,NULL,'"'||TRIM(EMPLOYEE_ID)||'"')            EMPLOYEE_ID
,DECODE(TRIM(LOGIN_ID),NULL,NULL,'"'||TRIM(LOGIN_ID)||'"')                  LOGIN_ID
,DECODE(TRIM(FIRST_NAME),NULL,NULL,'"'||TRIM(FIRST_NAME)||'"')              FIRST_NAME
,DECODE(TRIM(MIDDLE_NAME),NULL,NULL,'"'||TRIM(MIDDLE_NAME)||'"')            MIDDLE_NAME
,DECODE(TRIM(LAST_NAME),NULL,NULL,'"'||TRIM(LAST_NAME)||'"')                LAST_NAME
,DECODE(TRIM(EMAIL_ADDRESS),NULL,NULL,'"'||TRIM(EMAIL_ADDRESS)||'"')        EMAIL_ADDRESS
,DECODE(TRIM(PASSWD),NULL,NULL,'"'||TRIM(PASSWD)||'"')                      PASSWD
,DECODE(TRIM(LOCALE_CODE),NULL,NULL,'"'||TRIM(LOCALE_CODE)||'"')            LOCALE_CODE
,DECODE(TRIM(EXPENSE_USER),NULL,NULL,'"'||TRIM(EXPENSE_USER)||'"')          EXPENSE_USER
,DECODE(TRIM(EXPENSE_APPROVER),NULL,NULL,'"'||TRIM(EXPENSE_APPROVER)||'"')  EXPENSE_APPROVER     
,DECODE(TRIM(INVOICE_USER),NULL,NULL,'"'||TRIM(INVOICE_USER)||'"')          INVOICE_USER
,DECODE(TRIM(INVOICE_APPROVER),NULL,NULL,'"'||TRIM(INVOICE_APPROVER)||'"')  INVOICE_APPROVER          
,DECODE(TRIM(TRAVEL_USER),NULL,NULL,'"'||TRIM(TRAVEL_USER)||'"')            TRAVEL_USER        
,DECODE(TRIM(ACTIVE),NULL,NULL,'"'||TRIM(ACTIVE)||'"')                      ACTIVE
,DECODE(TRIM(NO_MIDDLE_NAME),NULL,NULL,'"'||TRIM(NO_MIDDLE_NAME)||'"')      NO_MIDDLE_NAME
,DECODE(TRIM(LOCATE_AND_ALERT),NULL,NULL,'"'||TRIM(LOCATE_AND_ALERT)||'"')  LOCATE_AND_ALERT           
,DECODE(TRIM(EXPENSEIT_USER_ROLE),NULL,NULL,'"'||TRIM(EXPENSEIT_USER_ROLE)||'"')  EXPENSEIT_USER_ROLE    
,DECODE(TRIM(FUTURE_USE_5),NULL,NULL,'"'||TRIM(FUTURE_USE_5)||'"')                FUTURE_USE_5    
,DECODE(TRIM(FUTURE_USE_6),NULL,NULL,'"'||TRIM(FUTURE_USE_6)||'"')                FUTURE_USE_6
,DECODE(TRIM(FUTURE_USE_7),NULL,NULL,'"'||TRIM(FUTURE_USE_7)||'"')                FUTURE_USE_7    
,DECODE(TRIM(FUTURE_USE_8),NULL,NULL,'"'||TRIM(FUTURE_USE_8)||'"')                FUTURE_USE_8
,DECODE(TRIM(FUTURE_USE_9),NULL,NULL,'"'||TRIM(FUTURE_USE_9)||'"')                FUTURE_USE_9    
,DECODE(TRIM(FUTURE_USE_10),NULL,NULL,'"'||TRIM(FUTURE_USE_10)||'"')              FUTURE_USE_10  
FROM APPS.XXCUS_CONCUR_EMP_EXPORT_310_V;

--Cursor to fetch the Employees
CURSOR cur_employees IS
SELECT DECODE(TRIM(TRX_TYPE),NULL,NULL,'"'||TRIM(TRX_TYPE)||'"')              TRX_TYPE
,DECODE(TRIM(FIRST_NAME),NULL,NULL,'"'||TRIM(FIRST_NAME)||'"')                FIRST_NAME
,DECODE(TRIM(MIDDLE_NAME),NULL,NULL,'"'||TRIM(MIDDLE_NAME)||'"')              MIDDLE_NAME
,DECODE(TRIM(LAST_NAME),NULL,NULL,'"'||TRIM(LAST_NAME)||'"')                  LAST_NAME
,DECODE(TRIM(EMPLOYEE_ID),NULL,NULL,'"'||TRIM(EMPLOYEE_ID)||'"')              EMPLOYEE_ID
,DECODE(TRIM(LOGIN_ID),NULL,NULL,'"'||TRIM(LOGIN_ID)||'"')                    LOGIN_ID
,DECODE(TRIM(PASSWORD),NULL,NULL,'"'||TRIM(PASSWORD)||'"')                    PASSWORD
,DECODE(TRIM(EMAIL_ADDRESS),NULL,NULL,'"'||TRIM(EMAIL_ADDRESS)||'"')          EMAIL_ADDRESS
,DECODE(TRIM(LOCLE_CODE),NULL,NULL,'"'||TRIM(LOCLE_CODE)||'"')                LOCLE_CODE                          
,DECODE(TRIM(CTRY_CODE),NULL,NULL,'"'||TRIM(CTRY_CODE)||'"')                  CTRY_CODE 
,DECODE(TRIM(CTRY_SUB_CODE),NULL,NULL,'"'||TRIM(CTRY_SUB_CODE)||'"')          CTRY_SUB_CODE 
,DECODE(TRIM(LEDGER_CODE),NULL,NULL,'"'||TRIM(LEDGER_CODE)||'"')              LEDGER_CODE 
,DECODE(TRIM(REIMBURSEMENT_CRN_CODE),NULL,NULL,'"'||TRIM(REIMBURSEMENT_CRN_CODE)||'"')              REIMBURSEMENT_CRN_CODE 
,DECODE(TRIM(CASH_ADVANCE_ACCOUNT_CODE),NULL,NULL,'"'||TRIM(CASH_ADVANCE_ACCOUNT_CODE)||'"')        CASH_ADVANCE_ACCOUNT_CODE 
,DECODE(TRIM(ACTIVE_Y_N),NULL,NULL,'"'||TRIM(ACTIVE_Y_N)||'"')              ACTIVE_Y_N 
,DECODE(TRIM(ORG_UNIT_1_ERP_SYSTEM),NULL,NULL,'"'||TRIM(ORG_UNIT_1_ERP_SYSTEM)||'"')              ORG_UNIT_1_ERP_SYSTEM 
,DECODE(TRIM(ORG_UNIT_2_COMPANY),NULL,NULL,'"'||TRIM(ORG_UNIT_2_COMPANY)||'"')              ORG_UNIT_2_COMPANY 
,DECODE(TRIM(ORG_UNIT_3_BRANCH),NULL,NULL,'"'||TRIM(ORG_UNIT_3_BRANCH)||'"')              ORG_UNIT_3_BRANCH 
,DECODE(TRIM(ORG_UNIT_4_COST_CENTER),NULL,NULL,'"'||TRIM(ORG_UNIT_4_COST_CENTER)||'"')              ORG_UNIT_4_COST_CENTER 
,DECODE(TRIM(ORG_UNIT_5_NOT_USED),NULL,NULL,'"'||TRIM(ORG_UNIT_5_NOT_USED)||'"')              ORG_UNIT_5_NOT_USED 
,DECODE(TRIM(ORG_UNIT_6_PROJECT),NULL,NULL,'"'||TRIM(ORG_UNIT_6_PROJECT)||'"')              ORG_UNIT_6_PROJECT 
,DECODE(TRIM(CUSTOM_1_ERP_SYSTEM),NULL,NULL,'"'||TRIM(CUSTOM_1_ERP_SYSTEM)||'"')              CUSTOM_1_ERP_SYSTEM 
,DECODE(TRIM(CUSTOM_2_COMPANY),NULL,NULL,'"'||TRIM(CUSTOM_2_COMPANY)||'"')              CUSTOM_2_COMPANY                          
,DECODE(TRIM(CUSTOM_3_BRANCH),NULL,NULL,'"'||TRIM(CUSTOM_3_BRANCH)||'"')              CUSTOM_3_BRANCH 
,DECODE(TRIM(CUSTOM_4_COST_CENTER),NULL,NULL,'"'||TRIM(CUSTOM_4_COST_CENTER)||'"')              CUSTOM_4_COST_CENTER 
,DECODE(TRIM(CUSTOM_5_NO_LONGER_BEING_USED),NULL,NULL,'"'||TRIM(CUSTOM_5_NO_LONGER_BEING_USED)||'"')              CUSTOM_5_NO_LONGER_BEING_USED 
,DECODE(TRIM(CUSTOM_6_PROJECT),NULL,NULL,'"'||TRIM(CUSTOM_6_PROJECT)||'"')              CUSTOM_6_PROJECT 
,DECODE(TRIM(CUSTOM_7),NULL,NULL,'"'||TRIM(CUSTOM_7)||'"')                CUSTOM_7 
,DECODE(TRIM(CUSTOM_8),NULL,NULL,'"'||TRIM(CUSTOM_8)||'"')                CUSTOM_8 
,DECODE(TRIM(CUSTOM_9),NULL,NULL,'"'||TRIM(CUSTOM_9)||'"')                CUSTOM_9 
,DECODE(TRIM(CUSTOM_10),NULL,NULL,'"'||TRIM(CUSTOM_10)||'"')              CUSTOM_10 
,DECODE(TRIM(CUSTOM_11),NULL,NULL,'"'||TRIM(CUSTOM_11)||'"')              CUSTOM_11 
,DECODE(TRIM(CUSTOM_12),NULL,NULL,'"'||TRIM(CUSTOM_12)||'"')              CUSTOM_12 
,DECODE(TRIM(CUSTOM_13),NULL,NULL,'"'||TRIM(CUSTOM_13)||'"')              CUSTOM_13 
,DECODE(TRIM(CUSTOM_14),NULL,NULL,'"'||TRIM(CUSTOM_14)||'"')              CUSTOM_14 
,DECODE(TRIM(CUSTOM_15),NULL,NULL,'"'||TRIM(CUSTOM_15)||'"')              CUSTOM_15 
,DECODE(TRIM(CUSTOM_16),NULL,NULL,'"'||TRIM(CUSTOM_16)||'"')              CUSTOM_16                          
,DECODE(TRIM(CUSTOM_17)                                      ,NULL,NULL,'"'||TRIM(CUSTOM_17)||'"') CUSTOM_17
,DECODE(TRIM(CUSTOM_18)                                      ,NULL,NULL,'"'||TRIM(CUSTOM_18)||'"') CUSTOM_18
,DECODE(TRIM(CUSTOM_19)                                      ,NULL,NULL,'"'||TRIM(CUSTOM_19)||'"') CUSTOM_19
,DECODE(TRIM(CUSTOM_20)                                      ,NULL,NULL,'"'||TRIM(CUSTOM_20)||'"') CUSTOM_20
,DECODE(TRIM(CUSTOM_21_CONCUR_EXP_GRP_HIER)                  ,NULL,NULL,'"'||TRIM(CUSTOM_21_CONCUR_EXP_GRP_HIER)||'"') CUSTOM_21_CONCUR_EXP_GRP_HIER
,DECODE(TRIM(EMAIL_CASH_ADV_STAT_CHANGE)                     ,NULL,NULL,'"'||TRIM(EMAIL_CASH_ADV_STAT_CHANGE)||'"')    EMAIL_CASH_ADV_STAT_CHANGE
,DECODE(TRIM(EMAIL_CASH_ADV_AWT_APRV)                        ,NULL,NULL,'"'||TRIM(EMAIL_CASH_ADV_AWT_APRV)||'"') EMAIL_CASH_ADV_AWT_APRV
,DECODE(TRIM(EMAIL_RPT_STATUS_CHANGE)                        ,NULL,NULL,'"'||TRIM(EMAIL_RPT_STATUS_CHANGE)||'"') EMAIL_RPT_STATUS_CHANGE
,DECODE(TRIM(EMAIL_RPT_AWT_APRV)                             ,NULL,NULL,'"'||TRIM(EMAIL_RPT_AWT_APRV)||'"')  EMAIL_RPT_AWT_APRV
,DECODE(TRIM(PROMPT_FOR_APRV_ON_SUBMIT_RPT)                  ,NULL,NULL,'"'||TRIM(PROMPT_FOR_APRV_ON_SUBMIT_RPT)||'"') PROMPT_FOR_APRV_ON_SUBMIT_RPT
,DECODE(TRIM(EMAIL_TRAVEL_REQ_STAT_CHANGE)                   ,NULL,NULL,'"'||TRIM(EMAIL_TRAVEL_REQ_STAT_CHANGE)||'"')  EMAIL_TRAVEL_REQ_STAT_CHANGE
,DECODE(TRIM(EMAIL_TRAVEL_REQ_AWT_APRV)                      ,NULL,NULL,'"'||TRIM(EMAIL_TRAVEL_REQ_AWT_APRV)||'"')     EMAIL_TRAVEL_REQ_AWT_APRV
,DECODE(TRIM(PMRPT_FOR_APRV_SUB_TRAVEL_REQ)                  ,NULL,NULL,'"'||TRIM(PMRPT_FOR_APRV_SUB_TRAVEL_REQ)||'"') PMRPT_FOR_APRV_SUB_TRAVEL_REQ
,DECODE(TRIM(EMAIL_WHEN_PMT_STATUS_CHANGES)                  ,NULL,NULL,'"'||TRIM(EMAIL_WHEN_PMT_STATUS_CHANGES)||'"') EMAIL_WHEN_PMT_STATUS_CHANGES
,DECODE(TRIM(EMAIL_PMT_AWAITING_APPROVAL)                    ,NULL,NULL,'"'||TRIM(EMAIL_PMT_AWAITING_APPROVAL)||'"')   EMAIL_PMT_AWAITING_APPROVAL
,DECODE(TRIM(PRMPT_FOR_APRV_SUB_PMT)                         ,NULL,NULL,'"'||TRIM(PRMPT_FOR_APRV_SUB_PMT)||'"')        PRMPT_FOR_APRV_SUB_PMT
,DECODE(TRIM(PRMPT_ADD_CC_TRANS_TO_RPT)                      ,NULL,NULL,'"'||TRIM(PRMPT_ADD_CC_TRANS_TO_RPT)||'"')     PRMPT_ADD_CC_TRANS_TO_RPT
,DECODE(TRIM(EMAIL_WHEN_NEW_CC_TRANS_ARRIVE)                 ,NULL,NULL,'"'||TRIM(EMAIL_WHEN_NEW_CC_TRANS_ARRIVE)||'"') EMAIL_WHEN_NEW_CC_TRANS_ARRIVE
,DECODE(TRIM(EMAIL_WHEN_FAX_RCPTS_ARRIVE)                    ,NULL,NULL,'"'||TRIM(EMAIL_WHEN_FAX_RCPTS_ARRIVE)||'"')    EMAIL_WHEN_FAX_RCPTS_ARRIVE
,DECODE(TRIM(SHOW_HELP_ON_APP_PAGES)                         ,NULL,NULL,'"'||TRIM(SHOW_HELP_ON_APP_PAGES)||'"')         SHOW_HELP_ON_APP_PAGES
,DECODE(TRIM(SHOW_IMAGING_INTRO_PAGE)                        ,NULL,NULL,'"'||TRIM(SHOW_IMAGING_INTRO_PAGE)||'"')        SHOW_IMAGING_INTRO_PAGE
,DECODE(TRIM(EMP_ID_OF_EXPENSE_RPT_APPROVER)                 ,NULL,NULL,'"'||TRIM(EMP_ID_OF_EXPENSE_RPT_APPROVER)||'"') EMP_ID_OF_EXPENSE_RPT_APPROVER
,DECODE(TRIM(EMP_ID_OF_CASH_ADV_APPROVER)                    ,NULL,NULL,'"'||TRIM(EMP_ID_OF_CASH_ADV_APPROVER)||'"')    EMP_ID_OF_CASH_ADV_APPROVER
,DECODE(TRIM(EMP_ID_OF_REQUEST_APPROVER)                     ,NULL,NULL,'"'||TRIM(EMP_ID_OF_REQUEST_APPROVER)||'"')     EMP_ID_OF_REQUEST_APPROVER
,DECODE(TRIM(EMP_ID_OF_INVOICE_APPROVER)                     ,NULL,NULL,'"'||TRIM(EMP_ID_OF_INVOICE_APPROVER)||'"')     EMP_ID_OF_INVOICE_APPROVER
,DECODE(TRIM(EXPENSE_USER_Y_N)                               ,NULL,NULL,'"'||TRIM(EXPENSE_USER_Y_N)||'"')               EXPENSE_USER_Y_N
,DECODE(TRIM(EXP_ANDOR_CASH_ADV_APRVR_Y_N)                   ,NULL,NULL,'"'||TRIM(EXP_ANDOR_CASH_ADV_APRVR_Y_N)||'"')   EXP_ANDOR_CASH_ADV_APRVR_Y_N
,DECODE(TRIM(COMPANY_CARD_ADMIN)                             ,NULL,NULL,'"'||TRIM(COMPANY_CARD_ADMIN)||'"')     COMPANY_CARD_ADMIN
,DECODE(TRIM(FUTURE_USE)                                     ,NULL,NULL,'"'||TRIM(FUTURE_USE)||'"')             FUTURE_USE
,DECODE(TRIM(RECEIPT_PROCESSOR)                              ,NULL,NULL,'"'||TRIM(RECEIPT_PROCESSOR)||'"')      RECEIPT_PROCESSOR
,DECODE(TRIM(FUTURE_USE_1_1)                                 ,NULL,NULL,'"'||TRIM(FUTURE_USE_1_1)||'"')         FUTURE_USE_1_1
,DECODE(TRIM(IMPORT_EXTRACT_MONITOR)                         ,NULL,NULL,'"'||TRIM(IMPORT_EXTRACT_MONITOR)||'"') IMPORT_EXTRACT_MONITOR
,DECODE(TRIM(COMPANY_INFO_ADMIN)                             ,NULL,NULL,'"'||TRIM(COMPANY_INFO_ADMIN)||'"')     COMPANY_INFO_ADMIN
,DECODE(TRIM(OFFLINE_USER)                                   ,NULL,NULL,'"'||TRIM(OFFLINE_USER)||'"')           OFFLINE_USER
,DECODE(TRIM(REPORTING_CONFIG_ADMIN)                         ,NULL,NULL,'"'||TRIM(REPORTING_CONFIG_ADMIN)||'"') REPORTING_CONFIG_ADMIN
,DECODE(TRIM(INVOICE_USER)                                   ,NULL,NULL,'"'||TRIM(INVOICE_USER)||'"')           INVOICE_USER
,DECODE(TRIM(INVOICE_APPROVER)                               ,NULL,NULL,'"'||TRIM(INVOICE_APPROVER)||'"')       INVOICE_APPROVER
,DECODE(TRIM(INVOICE_VENDOR_MANAGER)                         ,NULL,NULL,'"'||TRIM(INVOICE_VENDOR_MANAGER)||'"') INVOICE_VENDOR_MANAGER
,DECODE(TRIM(EXPENSE_AUDIT_REQUIRED)                         ,NULL,NULL,'"'||TRIM(EXPENSE_AUDIT_REQUIRED)||'"') EXPENSE_AUDIT_REQUIRED
,DECODE(TRIM(BI_MANAGER_EMPLOYEE_ID)                         ,NULL,NULL,'"'||TRIM(BI_MANAGER_EMPLOYEE_ID)||'"') BI_MANAGER_EMPLOYEE_ID
,DECODE(TRIM(REQUEST_USER_Y_N)                               ,NULL,NULL,'"'||TRIM(REQUEST_USER_Y_N)||'"')       REQUEST_USER_Y_N
,DECODE(TRIM(REQUEST_APPROVER_Y_N)                           ,NULL,NULL,'"'||TRIM(REQUEST_APPROVER_Y_N)||'"')   REQUEST_APPROVER_Y_N
,DECODE(TRIM(EXPENSE_RPT_APPROVER_EMP_ID_2)                  ,NULL,NULL,'"'||TRIM(EXPENSE_RPT_APPROVER_EMP_ID_2)||'"') EXPENSE_RPT_APPROVER_EMP_ID_2
,DECODE(TRIM(PAYMENT_REQUEST_ASSIGNED)                       ,NULL,NULL,'"'||TRIM(PAYMENT_REQUEST_ASSIGNED)||'"')      PAYMENT_REQUEST_ASSIGNED
,DECODE(TRIM(FUTURE_USE_1)                                   ,NULL,NULL,'"'||TRIM(FUTURE_USE_1)||'"')  FUTURE_USE_1
,DECODE(TRIM(FUTURE_USE_2)                                   ,NULL,NULL,'"'||TRIM(FUTURE_USE_2)||'"')  FUTURE_USE_2
,DECODE(TRIM(TAX_ADMINISTRATOR)                              ,NULL,NULL,'"'||TRIM(TAX_ADMINISTRATOR)||'"')  TAX_ADMINISTRATOR
,DECODE(TRIM(FBT_ADMINISTRATOR)                              ,NULL,NULL,'"'||TRIM(FBT_ADMINISTRATOR)||'"')  FBT_ADMINISTRATOR
,DECODE(TRIM(TRAVEL_WIZARD_USER)                             ,NULL,NULL,'"'||TRIM(TRAVEL_WIZARD_USER)||'"') TRAVEL_WIZARD_USER
,DECODE(TRIM(CUST_22_CONCUR_INVC_GROUP_HIER)                 ,NULL,NULL,'"'||TRIM(CUST_22_CONCUR_INVC_GROUP_HIER)||'"') CUST_22_CONCUR_INVC_GROUP_HIER
,DECODE(TRIM(REQUEST_APPROVER_EMPLOYEE_ID_2)                 ,NULL,NULL,'"'||TRIM(REQUEST_APPROVER_EMPLOYEE_ID_2)||'"') REQUEST_APPROVER_EMPLOYEE_ID_2
,DECODE(TRIM(IS_NON_EMPLOYEE_Y_N)                            ,NULL,NULL,'"'||TRIM(IS_NON_EMPLOYEE_Y_N)||'"')            IS_NON_EMPLOYEE_Y_N
,DECODE(TRIM(REIMBURSEMENT_TYPE)                             ,NULL,NULL,'"'||TRIM(REIMBURSEMENT_TYPE)||'"') REIMBURSEMENT_TYPE
,DECODE(TRIM(ADP_EMPLOYEE_ID)                                ,NULL,NULL,'"'||TRIM(ADP_EMPLOYEE_ID)||'"')    ADP_EMPLOYEE_ID
,DECODE(TRIM(ADP_COMPANY_CODE)                               ,NULL,NULL,'"'||TRIM(ADP_COMPANY_CODE)||'"')   ADP_COMPANY_CODE
,DECODE(TRIM(ADP_DEDUCTION_CODE)                             ,NULL,NULL,'"'||TRIM(ADP_DEDUCTION_CODE)||'"') ADP_DEDUCTION_CODE
,DECODE(TRIM(FUTURE_USE_7),NULL,NULL,'"'||TRIM(FUTURE_USE_7)||'"') FUTURE_USE_7
,DECODE(TRIM(FUTURE_USE_8),NULL,NULL,'"'||TRIM(FUTURE_USE_8)||'"') FUTURE_USE_8
,DECODE(TRIM(FUTURE_USE_9),NULL,NULL,'"'||TRIM(FUTURE_USE_9)||'"') FUTURE_USE_9
,DECODE(TRIM(FUTURE_USE_10),NULL,NULL,'"'||TRIM(FUTURE_USE_10)||'"') FUTURE_USE_10
,DECODE(TRIM(FUTURE_USE_11),NULL,NULL,'"'||TRIM(FUTURE_USE_11)||'"') FUTURE_USE_11
,DECODE(TRIM(FUTURE_USE_12),NULL,NULL,'"'||TRIM(FUTURE_USE_12)||'"') FUTURE_USE_12
,DECODE(TRIM(FUTURE_USE_13),NULL,NULL,'"'||TRIM(FUTURE_USE_13)||'"') FUTURE_USE_13
,DECODE(TRIM(FUTURE_USE_14),NULL,NULL,'"'||TRIM(FUTURE_USE_14)||'"') FUTURE_USE_14
,DECODE(TRIM(FUTURE_USE_15),NULL,NULL,'"'||TRIM(FUTURE_USE_15)||'"') FUTURE_USE_15
,DECODE(TRIM(FUTURE_USE_16),NULL,NULL,'"'||TRIM(FUTURE_USE_16)||'"') FUTURE_USE_16
,DECODE(TRIM(FUTURE_USE_17),NULL,NULL,'"'||TRIM(FUTURE_USE_17)||'"') FUTURE_USE_17
,DECODE(TRIM(FUTURE_USE_18),NULL,NULL,'"'||TRIM(FUTURE_USE_18)||'"') FUTURE_USE_18
,DECODE(TRIM(FUTURE_USE_19),NULL,NULL,'"'||TRIM(FUTURE_USE_19)||'"') FUTURE_USE_19
,DECODE(TRIM(FUTURE_USE_20),NULL,NULL,'"'||TRIM(FUTURE_USE_20)||'"') FUTURE_USE_20
,DECODE(TRIM(FUTURE_USE_21),NULL,NULL,'"'||TRIM(FUTURE_USE_21)||'"') FUTURE_USE_21
,DECODE(TRIM(FUTURE_USE_22),NULL,NULL,'"'||TRIM(FUTURE_USE_22)||'"') FUTURE_USE_22
,DECODE(TRIM(FUTURE_USE_23),NULL,NULL,'"'||TRIM(FUTURE_USE_23)||'"') FUTURE_USE_23
,DECODE(TRIM(FUTURE_USE_24),NULL,NULL,'"'||TRIM(FUTURE_USE_24)||'"') FUTURE_USE_24
,DECODE(TRIM(FUTURE_USE_25),NULL,NULL,'"'||TRIM(FUTURE_USE_25)||'"') FUTURE_USE_25
,DECODE(TRIM(FUTURE_USE_26),NULL,NULL,'"'||TRIM(FUTURE_USE_26)||'"') FUTURE_USE_26
,DECODE(TRIM(FUTURE_USE_27),NULL,NULL,'"'||TRIM(FUTURE_USE_27)||'"') FUTURE_USE_27
,DECODE(TRIM(FUTURE_USE_28),NULL,NULL,'"'||TRIM(FUTURE_USE_28)||'"') FUTURE_USE_28
,DECODE(TRIM(FUTURE_USE_29),NULL,NULL,'"'||TRIM(FUTURE_USE_29)||'"') FUTURE_USE_29
,DECODE(TRIM(FUTURE_USE_30),NULL,NULL,'"'||TRIM(FUTURE_USE_30)||'"') FUTURE_USE_30
,DECODE(TRIM(FUTURE_USE_31),NULL,NULL,'"'||TRIM(FUTURE_USE_31)||'"') FUTURE_USE_31
,DECODE(TRIM(FUTURE_USE_32),NULL,NULL,'"'||TRIM(FUTURE_USE_32)||'"') FUTURE_USE_32
,DECODE(TRIM(FUTURE_USE_33),NULL,NULL,'"'||TRIM(FUTURE_USE_33)||'"') FUTURE_USE_33
,DECODE(TRIM(FUTURE_USE_34),NULL,NULL,'"'||TRIM(FUTURE_USE_34)||'"') FUTURE_USE_34
,DECODE(TRIM(FUTURE_USE_35),NULL,NULL,'"'||TRIM(FUTURE_USE_35)||'"') FUTURE_USE_35
,DECODE(TRIM(FUTURE_USE_36),NULL,NULL,'"'||TRIM(FUTURE_USE_36)||'"') FUTURE_USE_36
,DECODE(TRIM(FUTURE_USE_37),NULL,NULL,'"'||TRIM(FUTURE_USE_37)||'"') FUTURE_USE_37
,DECODE(TRIM(FUTURE_USE_38),NULL,NULL,'"'||TRIM(FUTURE_USE_38)||'"') FUTURE_USE_38
,DECODE(TRIM(FUTURE_USE_39),NULL,NULL,'"'||TRIM(FUTURE_USE_39)||'"') FUTURE_USE_39
,DECODE(TRIM(FUTURE_USE_40),NULL,NULL,'"'||TRIM(FUTURE_USE_40)||'"') FUTURE_USE_40
,DECODE(TRIM(FUTURE_USE_41),NULL,NULL,'"'||TRIM(FUTURE_USE_41)||'"') FUTURE_USE_41
,DECODE(TRIM(FUTURE_USE_42),NULL,NULL,'"'||TRIM(FUTURE_USE_42)||'"') FUTURE_USE_42
,DECODE(TRIM(FUTURE_USE_43),NULL,NULL,'"'||TRIM(FUTURE_USE_43)||'"') FUTURE_USE_43
,DECODE(TRIM(FUTURE_USE_44),NULL,NULL,'"'||TRIM(FUTURE_USE_44)||'"') FUTURE_USE_44
,DECODE(TRIM(FUTURE_USE_45),NULL,NULL,'"'||TRIM(FUTURE_USE_45)||'"') FUTURE_USE_45
,DECODE(TRIM(FUTURE_USE_46),NULL,NULL,'"'||TRIM(FUTURE_USE_46)||'"') FUTURE_USE_46
,DECODE(TRIM(FUTURE_USE_47),NULL,NULL,'"'||TRIM(FUTURE_USE_47)||'"') FUTURE_USE_47
,DECODE(TRIM(FUTURE_USE_48),NULL,NULL,'"'||TRIM(FUTURE_USE_48)||'"') FUTURE_USE_48
,DECODE(TRIM(FUTURE_USE_49),NULL,NULL,'"'||TRIM(FUTURE_USE_49)||'"') FUTURE_USE_49
,DECODE(TRIM(FUTURE_USE_50),NULL,NULL,'"'||TRIM(FUTURE_USE_50)||'"') FUTURE_USE_50
FROM APPS.XXCUS_CONCUR_EMP_EXPORT_305_V;

--Cursor to fetch the supervisors
CURSOR cur_supervisor IS
SELECT DECODE(TRIM(TRX_TYPE),NULL,NULL,'"'||TRIM(TRX_TYPE)||'"')                          TRX_TYPE
,DECODE(TRIM(EMPLOYEE_ID),NULL,NULL,'"'||TRIM(EMPLOYEE_ID)||'"')                          EMPLOYEE_ID
,DECODE(TRIM(NAME_PREFIX),NULL,NULL,'"'||TRIM(NAME_PREFIX)||'"')                          NAME_PREFIX
,DECODE(TRIM(NAME_SUFFIX),NULL,NULL,'"'||TRIM(NAME_SUFFIX)||'"')                          NAME_SUFFIX
,DECODE(TRIM(NICK_NAME),NULL,NULL,'"'||TRIM(NICK_NAME)||'"')                              NICK_NAME
,DECODE(TRIM(REDRESS_NUMBER),NULL,NULL,'"'||TRIM(REDRESS_NUMBER)||'"')                    REDRESS_NUMBER
,DECODE(TRIM(GENDER),NULL,NULL,'"'||TRIM(GENDER)||'"')                                    GENDER
,DECODE(TRIM(DATE_OF_BIRTH),NULL,NULL,'"'||TRIM(DATE_OF_BIRTH)||'"')                      DATE_OF_BIRTH
,DECODE(TRIM(MANAGER_COMPANY_EMPL_ID),NULL,NULL,'"'||TRIM(MANAGER_COMPANY_EMPL_ID)||'"')  MANAGER_COMPANY_EMPL_ID
,DECODE(TRIM(JOB_TITLE),NULL,NULL,'"'||TRIM(JOB_TITLE)||'"')                              JOB_TITLE
,DECODE(TRIM(WORK_PHONE),NULL,NULL,'"'||TRIM(WORK_PHONE)||'"')                        WORK_PHONE
,DECODE(TRIM(WORK_PHONE_EXTENSION),NULL,NULL,'"'||TRIM(WORK_PHONE_EXTENSION)||'"')    WORK_PHONE_EXTENSION
,DECODE(TRIM(WORK_FAX),NULL,NULL,'"'||TRIM(WORK_FAX)||'"')                            WORK_FAX
,DECODE(TRIM(CELL_PHONE),NULL,NULL,'"'||TRIM(CELL_PHONE)||'"')                        CELL_PHONE
,DECODE(TRIM(PAGER_PHONE),NULL,NULL,'"'||TRIM(PAGER_PHONE)||'"')                      PAGER_PHONE
,DECODE(TRIM(HOME_PHONE),NULL,NULL,'"'||TRIM(HOME_PHONE)||'"')                        HOME_PHONE                        
,DECODE(TRIM(TRAVEL_NAME_REMARK),NULL,NULL,'"'||TRIM(TRAVEL_NAME_REMARK)||'"')        TRAVEL_NAME_REMARK           
,DECODE(TRIM(TRAVEL_CLASS_NAME),NULL,NULL,'"'||TRIM(TRAVEL_CLASS_NAME)||'"')          TRAVEL_CLASS_NAME                         
,DECODE(TRIM(GDS_PROFILE_NAME),NULL,NULL,'"'||TRIM(GDS_PROFILE_NAME)||'"')            GDS_PROFILE_NAME                        
,DECODE(TRIM(ORGUNIT_OR_DIVISION),NULL,NULL,'"'||TRIM(ORGUNIT_OR_DIVISION)||'"')      ORGUNIT_OR_DIVISION              
,DECODE(TRIM(HOME_STREET_ADDRESS),NULL,NULL,'"'||TRIM(HOME_STREET_ADDRESS)||'"')      HOME_STREET_ADDRESS                          
,DECODE(TRIM(HOME_CITY),NULL,NULL,'"'||TRIM(HOME_CITY)||'"')                          HOME_CITY
,DECODE(TRIM(HOME_STATE),NULL,NULL,'"'||TRIM(HOME_STATE)||'"')                        HOME_STATE
,DECODE(TRIM(HOME_POSTAL_CODE),NULL,NULL,'"'||TRIM(HOME_POSTAL_CODE)||'"')            HOME_POSTAL_CODE          
,DECODE(TRIM(HOME_COUNTRY),NULL,NULL,'"'||TRIM(HOME_COUNTRY)||'"')                    HOME_COUNTRY
,DECODE(TRIM(WORK_STREET_ADDRESS),NULL,NULL,'"'||TRIM(WORK_STREET_ADDRESS)||'"')      WORK_STREET_ADDRESS          
,DECODE(TRIM(WORK_CITY),NULL,NULL,'"'||TRIM(WORK_CITY)||'"')                          WORK_CITY
,DECODE(TRIM(WORK_STATE),NULL,NULL,'"'||TRIM(WORK_STATE)||'"')                        WORK_STATE
,DECODE(TRIM(WORK_POSTAL_CODE),NULL,NULL,'"'||TRIM(WORK_POSTAL_CODE)||'"')            WORK_POSTAL_CODE               
,DECODE(TRIM(WORK_COUNTRY),NULL,NULL,'"'||TRIM(WORK_COUNTRY)||'"')                    WORK_COUNTRY                         
,DECODE(TRIM(EMAIL_2),NULL,NULL,'"'||TRIM(EMAIL_2)||'"')                              EMAIL_2                        
,DECODE(TRIM(EMAIL_3),NULL,NULL,'"'||TRIM(EMAIL_3)||'"')                              EMAIL_3                  
,DECODE(TRIM(CUSTOM_1),NULL,NULL,'"'||TRIM(CUSTOM_1)||'"')                            CUSTOM_1                      
,DECODE(TRIM(CUSTOM_2),NULL,NULL,'"'||TRIM(CUSTOM_2)||'"')                            CUSTOM_2               
,DECODE(TRIM(CUSTOM_3),NULL,NULL,'"'||TRIM(CUSTOM_3)||'"')                            CUSTOM_3                         
,DECODE(TRIM(CUSTOM_4),NULL,NULL,'"'||TRIM(CUSTOM_4)||'"')                            CUSTOM_4                        
,DECODE(TRIM(CUSTOM_5),NULL,NULL,'"'||TRIM(CUSTOM_5)||'"')                            CUSTOM_5                  
,DECODE(TRIM(CUSTOM_6),NULL,NULL,'"'||TRIM(CUSTOM_6)||'"')                            CUSTOM_6                       
,DECODE(TRIM(CUSTOM_7),NULL,NULL,'"'||TRIM(CUSTOM_7)||'"')                            CUSTOM_7                            
,DECODE(TRIM(CUSTOM_8),NULL,NULL,'"'||TRIM(CUSTOM_8)||'"')                            CUSTOM_8                            
,DECODE(TRIM(CUSTOM_9),NULL,NULL,'"'||TRIM(CUSTOM_9)||'"')                            CUSTOM_9                           
,DECODE(TRIM(CUSTOM_10),NULL,NULL,'"'||TRIM(CUSTOM_10)||'"')                          CUSTOM_10                         
,DECODE(TRIM(CUSTOM_11),NULL,NULL,'"'||TRIM(CUSTOM_11)||'"')                          CUSTOM_11                           
,DECODE(TRIM(CUSTOM_12),NULL,NULL,'"'||TRIM(CUSTOM_12)||'"')                          CUSTOM_12                           
,DECODE(TRIM(CUSTOM_13),NULL,NULL,'"'||TRIM(CUSTOM_13)||'"')                          CUSTOM_13                           
,DECODE(TRIM(CUSTOM_14),NULL,NULL,'"'||TRIM(CUSTOM_14)||'"')                          CUSTOM_14                           
,DECODE(TRIM(CUSTOM_15),NULL,NULL,'"'||TRIM(CUSTOM_15)||'"')                          CUSTOM_15                           
,DECODE(TRIM(CUSTOM_16),NULL,NULL,'"'||TRIM(CUSTOM_16)||'"')                          CUSTOM_16                           
,DECODE(TRIM(CUSTOM_17),NULL,NULL,'"'||TRIM(CUSTOM_17)||'"')                          CUSTOM_17                          
,DECODE(TRIM(CUSTOM_18),NULL,NULL,'"'||TRIM(CUSTOM_18)||'"')                          CUSTOM_18                          
,DECODE(TRIM(CUSTOM_19),NULL,NULL,'"'||TRIM(CUSTOM_19)||'"')                          CUSTOM_19                          
,DECODE(TRIM(CUSTOM_20),NULL,NULL,'"'||TRIM(CUSTOM_20)||'"')                          CUSTOM_20                          
,DECODE(TRIM(CUSTOM_21),NULL,NULL,'"'||TRIM(CUSTOM_21)||'"')                          CUSTOM_21                          
,DECODE(TRIM(CUSTOM_22),NULL,NULL,'"'||TRIM(CUSTOM_22)||'"')                          CUSTOM_22                          
,DECODE(TRIM(CUSTOM_23),NULL,NULL,'"'||TRIM(CUSTOM_23)||'"')                          CUSTOM_23                          
,DECODE(TRIM(CUSTOM_24),NULL,NULL,'"'||TRIM(CUSTOM_24)||'"')                          CUSTOM_24                        
,DECODE(TRIM(CUSTOM_25),NULL,NULL,'"'||TRIM(CUSTOM_25)||'"')                          CUSTOM_25                        
,DECODE(TRIM(XML_PROFILE_SYNC_ID),NULL,NULL,'"'||TRIM(XML_PROFILE_SYNC_ID)||'"')      XML_PROFILE_SYNC_ID                         
,DECODE(TRIM(PROFILE_USER_PERMISSION),NULL,NULL,'"'||TRIM(PROFILE_USER_PERMISSION)||'"')            PROFILE_USER_PERMISSION                         
,DECODE(TRIM(AMADEUS_USER_PERMISSION),NULL,NULL,'"'||TRIM(AMADEUS_USER_PERMISSION)||'"')            AMADEUS_USER_PERMISSION                         
,DECODE(TRIM(OPEN_BOOKING_USER_PERMISSION),NULL,NULL,'"'||TRIM(OPEN_BOOKING_USER_PERMISSION)||'"')  OPEN_BOOKING_USER_PERMISSION                         
,DECODE(TRIM(FUTURE_USE_5),NULL,NULL,'"'||TRIM(FUTURE_USE_5)||'"')                          FUTURE_USE_5                        
,DECODE(TRIM(FUTURE_USE_6),NULL,NULL,'"'||TRIM(FUTURE_USE_6)||'"')                          FUTURE_USE_6                         
,DECODE(TRIM(FUTURE_USE_7),NULL,NULL,'"'||TRIM(FUTURE_USE_7)||'"')                          FUTURE_USE_7                         
,DECODE(TRIM(FUTURE_USE_8),NULL,NULL,'"'||TRIM(FUTURE_USE_8)||'"')                          FUTURE_USE_8                         
,DECODE(TRIM(FUTURE_USE_9),NULL,NULL,'"'||TRIM(FUTURE_USE_9)||'"')                          FUTURE_USE_9               
,DECODE(TRIM(FUTURE_USE_10),NULL,NULL,'"'||TRIM(FUTURE_USE_10)||'"')                        FUTURE_USE_10           
FROM APPS.XXCUS_CONCUR_EMP_EXPORT_350_V;

l_filename         VARCHAR2 (200):=Null;
l_filename_final   VARCHAR2 (200):=Null;
l_count            NUMBER;
g_prog_exception   EXCEPTION;
v_file             UTL_FILE.file_type;
l_err_msg          VARCHAR2 (5000);
l_directory_name   VARCHAR2 (5000) := 'XXCUS_EMP_FILE_EXPORT_DIR';
l_yyyymmdd         VARCHAR2(20) :=NULL;
l_yyyymmddhhmiss   VARCHAR2(20) :=NULL;
BEGIN

  --Printing in Parameters
  g_loc := 'Begining sample file Extract  ';
  write_log (g_loc);
  write_log ('========================================================');

  --Initialize the Out Parameter
  p_errbuf := NULL;
  p_retcode := '0';

  -- Get the file name
  SELECT    TO_CHAR (SYSDATE, 'YYYYMMDD')
           ,TO_CHAR (SYSDATE, 'YYYYMMDDHHMISS') 
  INTO l_yyyymmdd
  ,    l_yyyymmddhhmiss
  FROM DUAL;
l_filename :='employee_tmp_'||l_yyyymmdd||'_'||l_yyyymmddhhmiss||'.txt';
  l_filename_final :='employee_'||l_yyyymmdd||'_'||l_yyyymmddhhmiss||'.txt';

  g_loc := 'Getting file name ';
  write_log (g_loc || 'Temporary File Name:  ' || l_filename);
  write_log (g_loc || 'Final File Name : ' || l_filename_final);

  --Open the file handler
  g_loc := 'Open File handler ';
  write_log (g_loc);
  v_file :=UTL_FILE.fopen (LOCATION       => l_directory_name,
                           filename       => l_filename,
                           open_mode      => 'w',
                           max_linesize   => 32767);

  g_loc := 'Writing to the file ... Opening Cursor ';
  write_log (g_loc);

  l_count := 0;
  g_loc := 'Resetting the counter and now writing in file first line  ';
  write_log (g_loc);
               
  g_loc := 'Now writing in file whole data ';
  
  --Looping through Header Cursor...
  FOR rec_header IN cur_header LOOP
  
    l_count:=l_count+1;
   
    EXIT WHEN cur_header%NOTFOUND;

    BEGIN

      UTL_FILE.put_line(v_file,rec_header.header);

    EXCEPTION
    WHEN others THEN

      l_err_msg :='Error at header cursor'
                  || '---- '
                  || SUBSTR (SQLERRM, 1, 2000);

      write_error (l_err_msg);
      write_log (l_err_msg);

    END;

  END LOOP;
  
  --Looping through contractor Cursor...
  FOR rec_contractors IN cur_contractors LOOP
  
    l_count:=l_count+1;
   
    EXIT WHEN cur_contractors%NOTFOUND;

    BEGIN

      UTL_FILE.put_line(v_file,rec_contractors.TRX_TYPE
                  || '|'
                  || rec_contractors.EMPLOYEE_ID
                  || '|'
                  || rec_contractors.LOGIN_ID
                  || '|'
                  || rec_contractors.FIRST_NAME
                  || '|'
                  || rec_contractors.MIDDLE_NAME
                  || '|'
                  || rec_contractors.LAST_NAME
                  || '|'
                  || rec_contractors.EMAIL_ADDRESS
                  || '|'
                  || rec_contractors.PASSWD
                  || '|'
                  || rec_contractors.LOCALE_CODE
                  || '|'
                  || rec_contractors.EXPENSE_USER
                  || '|'
                  || rec_contractors.EXPENSE_APPROVER
                  || '|'
                  || rec_contractors.INVOICE_USER  
                  || '|'
                  || rec_contractors.INVOICE_APPROVER
                  || '|'
                  || rec_contractors.TRAVEL_USER 
                  || '|'
                  || rec_contractors.ACTIVE 
                  || '|'
                  || rec_contractors.NO_MIDDLE_NAME  
                  || '|'
                  || rec_contractors.LOCATE_AND_ALERT
                  || '|'
                  || rec_contractors.EXPENSEIT_USER_ROLE 
                  || '|'
                  || rec_contractors.FUTURE_USE_5   
                  || '|'  
                  || rec_contractors.FUTURE_USE_6
                  || '|'
                  || rec_contractors.FUTURE_USE_7         
                  || '|'
                  || rec_contractors.FUTURE_USE_8
                  || '|'
                  || rec_contractors.FUTURE_USE_9
                  || '|'
                  || rec_contractors.FUTURE_USE_10
                  );

    EXCEPTION
    WHEN others THEN

      l_err_msg :='Error at contractor cursor'
                  || '---- '
                  || SUBSTR (SQLERRM, 1, 2000);

      write_error (l_err_msg);
      write_log (l_err_msg);

    END;

  END LOOP;

  --Looping through Employee Cursor...
  FOR rec_employees IN cur_employees LOOP
  
    l_count:=l_count+1;
   
    EXIT WHEN cur_employees%NOTFOUND;

    BEGIN
  
      UTL_FILE.put_line(v_file,rec_employees.TRX_TYPE
                  || '|'
                  || rec_employees.FIRST_NAME 
                                    || '|'                         
                  || rec_employees.MIDDLE_NAME 
                                    || '|'                        
                  || rec_employees.LAST_NAME 
                                    || '|'                          
                  || rec_employees.EMPLOYEE_ID 
                                    || '|'                        
                  || rec_employees.LOGIN_ID  
                                    || '|'                          
                  || rec_employees.PASSWORD  
                                    || '|'                          
                  || rec_employees.EMAIL_ADDRESS  
                                    || '|'                     
                  || rec_employees.LOCLE_CODE   
                                    || '|'                       
                  || rec_employees.CTRY_CODE   
                                    || '|'                        
                  || rec_employees.CTRY_SUB_CODE  
                                    || '|'                     
                  || rec_employees.LEDGER_CODE   
                                    || '|'                      
                  || rec_employees.REIMBURSEMENT_CRN_CODE 
                                    || '|'             
                  || rec_employees.CASH_ADVANCE_ACCOUNT_CODE    
                                    || '|'       
                  || rec_employees.ACTIVE_Y_N      
                                    || '|'                    
                  || rec_employees.ORG_UNIT_1_ERP_SYSTEM  
                                    || '|'             
                  || rec_employees.ORG_UNIT_2_COMPANY 
                                    || '|'                 
                  || rec_employees.ORG_UNIT_3_BRANCH     
                                    || '|'              
                  || rec_employees.ORG_UNIT_4_COST_CENTER 
                                    || '|'             
                  || rec_employees.ORG_UNIT_5_NOT_USED
                                    || '|'                 
                  || rec_employees.ORG_UNIT_6_PROJECT    
                                    || '|'              
                  || rec_employees.CUSTOM_1_ERP_SYSTEM   
                                    || '|'              
                  || rec_employees.CUSTOM_2_COMPANY  
                                    || '|'                  
                  || rec_employees.CUSTOM_3_BRANCH   
                                    || '|'                  
                  || rec_employees.CUSTOM_4_COST_CENTER  
                                    || '|'              
                  || rec_employees.CUSTOM_5_NO_LONGER_BEING_USED  
                                    || '|'     
                  || rec_employees.CUSTOM_6_PROJECT   
                                    || '|'                 
                  || rec_employees.CUSTOM_7   
                                    || '|'                         
                  || rec_employees.CUSTOM_8 
                                    || '|'                           
                  || rec_employees.CUSTOM_9   
                                    || '|'                         
                  || rec_employees.CUSTOM_10  
                                    || '|'                         
                  || rec_employees.CUSTOM_11     
                                    || '|'                      
                  || rec_employees.CUSTOM_12  
                                    || '|'                         
                  || rec_employees.CUSTOM_13   
                                    || '|'                        
                  || rec_employees.CUSTOM_14  
                                    || '|'                         
                  || rec_employees.CUSTOM_15  
                                    || '|'                         
                  || rec_employees.CUSTOM_16    
                                    || '|'                       
                  || rec_employees.CUSTOM_17  
                                    || '|'                         
                  || rec_employees.CUSTOM_18  
                                    || '|'                         
                  || rec_employees.CUSTOM_19 
                                    || '|'                          
                  || rec_employees.CUSTOM_20  
                                    || '|'                         
                  || rec_employees.CUSTOM_21_CONCUR_EXP_GRP_HIER 
                                    || '|'      
                  || rec_employees.EMAIL_CASH_ADV_STAT_CHANGE  
                                    || '|'        
                  || rec_employees.EMAIL_CASH_ADV_AWT_APRV  
                                    || '|'           
                  || rec_employees.EMAIL_RPT_STATUS_CHANGE 
                                    || '|'           
                  || rec_employees.EMAIL_RPT_AWT_APRV    
                                    || '|'              
                  || rec_employees.PROMPT_FOR_APRV_ON_SUBMIT_RPT  
                                    || '|'     
                  || rec_employees.EMAIL_TRAVEL_REQ_STAT_CHANGE  
                                    || '|'      
                  || rec_employees.EMAIL_TRAVEL_REQ_AWT_APRV          
                                    || '|' 
                  || rec_employees.PMRPT_FOR_APRV_SUB_TRAVEL_REQ      
                                    || '|' 
                  || rec_employees.EMAIL_WHEN_PMT_STATUS_CHANGES     
                                    || '|'  
                  || rec_employees.EMAIL_PMT_AWAITING_APPROVAL   
                                    || '|'      
                  || rec_employees.PRMPT_FOR_APRV_SUB_PMT    
                                    || '|'          
                  || rec_employees.PRMPT_ADD_CC_TRANS_TO_RPT     
                                    || '|'      
                  || rec_employees.EMAIL_WHEN_NEW_CC_TRANS_ARRIVE 
                                    || '|'     
                  || rec_employees.EMAIL_WHEN_FAX_RCPTS_ARRIVE   
                                    || '|'      
                  || rec_employees.SHOW_HELP_ON_APP_PAGES     
                                    || '|'         
                  || rec_employees.SHOW_IMAGING_INTRO_PAGE         
                                    || '|'    
                  || rec_employees.EMP_ID_OF_EXPENSE_RPT_APPROVER   
                                    || '|'   
                  || rec_employees.EMP_ID_OF_CASH_ADV_APPROVER      
                                    || '|'   
                  || rec_employees.EMP_ID_OF_REQUEST_APPROVER    
                                    || '|'      
                  || rec_employees.EMP_ID_OF_INVOICE_APPROVER     
                                    || '|'     
                  || rec_employees.EXPENSE_USER_Y_N          
                                    || '|'          
                  || rec_employees.EXP_ANDOR_CASH_ADV_APRVR_Y_N 
                                    || '|'       
                  || rec_employees.COMPANY_CARD_ADMIN        
                                    || '|'          
                  || rec_employees.FUTURE_USE            
                                    || '|'              
                  || rec_employees.RECEIPT_PROCESSOR   
                                    || '|'                
                  || rec_employees.FUTURE_USE_1_1    
                                    || '|'                  
                  || rec_employees.IMPORT_EXTRACT_MONITOR  
                                    || '|'            
                  || rec_employees.COMPANY_INFO_ADMIN   
                                    || '|'               
                  || rec_employees.OFFLINE_USER    
                                    || '|'                    
                  || rec_employees.REPORTING_CONFIG_ADMIN    
                                    || '|'          
                  || rec_employees.INVOICE_USER       
                                    || '|'                 
                  || rec_employees.INVOICE_APPROVER     
                                    || '|'               
                  || rec_employees.INVOICE_VENDOR_MANAGER  
                                    || '|'            
                  || rec_employees.EXPENSE_AUDIT_REQUIRED 
                                    || '|'             
                  || rec_employees.BI_MANAGER_EMPLOYEE_ID  
                                    || '|'            
                  || rec_employees.REQUEST_USER_Y_N   
                                    || '|'                 
                  || rec_employees.REQUEST_APPROVER_Y_N    
                                    || '|'            
                  || rec_employees.EXPENSE_RPT_APPROVER_EMP_ID_2   
                                    || '|'    
                  || rec_employees.PAYMENT_REQUEST_ASSIGNED  
                                    || '|'          
                  || rec_employees.FUTURE_USE_1              
                                    || '|'          
                  || rec_employees.FUTURE_USE_2        
                                    || '|'                
                  || rec_employees.TAX_ADMINISTRATOR     
                                    || '|'              
                  || rec_employees.FBT_ADMINISTRATOR    
                                    || '|'               
                  || rec_employees.TRAVEL_WIZARD_USER       
                                    || '|'           
                  || rec_employees.CUST_22_CONCUR_INVC_GROUP_HIER  
                                    || '|'    
                  || rec_employees.REQUEST_APPROVER_EMPLOYEE_ID_2      
                                    || '|'
                  || rec_employees.IS_NON_EMPLOYEE_Y_N     
                                    || '|'            
                  || rec_employees.REIMBURSEMENT_TYPE   
                                    || '|'               
                  || rec_employees.ADP_EMPLOYEE_ID        
                                    || '|'             
                  || rec_employees.ADP_COMPANY_CODE     
                                    || '|'               
                  || rec_employees.ADP_DEDUCTION_CODE    
                                    || '|'              
                  || rec_employees.FUTURE_USE_7    
                                    || '|'                    
                  || rec_employees.FUTURE_USE_8      
                                    || '|'                  
                  || rec_employees.FUTURE_USE_9        
                                    || '|'                
                  || rec_employees.FUTURE_USE_10 
                                    || '|'                      
                  || rec_employees.FUTURE_USE_11  
                                    || '|'                     
                  || rec_employees.FUTURE_USE_12       
                                    || '|'                
                  || rec_employees.FUTURE_USE_13      
                                    || '|'                 
                  || rec_employees.FUTURE_USE_14         
                                    || '|'              
                  || rec_employees.FUTURE_USE_15      
                                    || '|'                 
                  || rec_employees.FUTURE_USE_16  
                                    || '|'                     
                  || rec_employees.FUTURE_USE_17         
                                    || '|'              
                  || rec_employees.FUTURE_USE_18   
                                    || '|'                    
                  || rec_employees.FUTURE_USE_19     
                                    || '|'                  
                  || rec_employees.FUTURE_USE_20  
                                    || '|'                     
                  || rec_employees.FUTURE_USE_21        
                                    || '|'               
                  || rec_employees.FUTURE_USE_22     
                                    || '|'                  
                  || rec_employees.FUTURE_USE_23            
                                    || '|'           
                  || rec_employees.FUTURE_USE_24    
                                    || '|'                   
                  || rec_employees.FUTURE_USE_25    
                                    || '|'                   
                  || rec_employees.FUTURE_USE_26    
                                    || '|'                   
                  || rec_employees.FUTURE_USE_27   
                                    || '|'                    
                  || rec_employees.FUTURE_USE_28      
                                    || '|'                 
                  || rec_employees.FUTURE_USE_29      
                                    || '|'                 
                  || rec_employees.FUTURE_USE_30  
                                    || '|'                     
                  || rec_employees.FUTURE_USE_31    
                                    || '|'                   
                  || rec_employees.FUTURE_USE_32     
                                    || '|'                  
                  || rec_employees.FUTURE_USE_33    
                                    || '|'                   
                  || rec_employees.FUTURE_USE_34    
                                    || '|'                   
                  || rec_employees.FUTURE_USE_35  
                                    || '|'                     
                  || rec_employees.FUTURE_USE_36     
                                    || '|'                  
                  || rec_employees.FUTURE_USE_37   
                                    || '|'                    
                  || rec_employees.FUTURE_USE_38        
                                    || '|'               
                  || rec_employees.FUTURE_USE_39     
                                    || '|'                  
                  || rec_employees.FUTURE_USE_40     
                                    || '|'                  
                  || rec_employees.FUTURE_USE_41   
                                    || '|'                    
                  || rec_employees.FUTURE_USE_42  
                                    || '|'                     
                  || rec_employees.FUTURE_USE_43  
                                    || '|'                     
                  || rec_employees.FUTURE_USE_44  
                                    || '|'                     
                  || rec_employees.FUTURE_USE_45  
                                    || '|'                     
                  || rec_employees.FUTURE_USE_46   
                                    || '|'                    
                  || rec_employees.FUTURE_USE_47   
                                    || '|'                    
                  || rec_employees.FUTURE_USE_48 
                                    || '|'                      
                  || rec_employees.FUTURE_USE_49  
                                    || '|'                     
                  || rec_employees.FUTURE_USE_50                       
                  );
  
    EXCEPTION
    WHEN others THEN

      l_err_msg :='Error at employee cursor'
                  || '---- '
                  || SUBSTR (SQLERRM, 1, 2000);

      write_error (l_err_msg);
      write_log (l_err_msg);

    END;

  END LOOP;

  --Looping through Supervisor Cursor...
  FOR rec_supervisor IN cur_supervisor LOOP
  
    l_count:=l_count+1;
   
    EXIT WHEN cur_supervisor%NOTFOUND;

    BEGIN

      UTL_FILE.put_line(v_file,rec_supervisor.TRX_TYPE
                  || '|'
                  || rec_supervisor.EMPLOYEE_ID
                  || '|'
                  || rec_supervisor.NAME_PREFIX   
                  || '|'
                  || rec_supervisor.NAME_SUFFIX   
                  || '|'
                  || rec_supervisor.NICK_NAME   
                  || '|'
                  || rec_supervisor.REDRESS_NUMBER   
                  || '|'
                  || rec_supervisor.GENDER   
                  || '|'
                  || rec_supervisor.DATE_OF_BIRTH   
                  || '|'
                  || rec_supervisor.MANAGER_COMPANY_EMPL_ID
                  || '|'
                  || rec_supervisor.JOB_TITLE 
                  || '|'
                  || rec_supervisor.WORK_PHONE 
                  || '|'
                  || rec_supervisor.WORK_PHONE_EXTENSION 
                  || '|'
                  || rec_supervisor.WORK_FAX 
                  || '|'
                  || rec_supervisor.HOME_PHONE  
                  || '|'
                  || rec_supervisor.CELL_PHONE 
                  || '|'
                  || rec_supervisor.PAGER_PHONE 
                  || '|'
                  || rec_supervisor.TRAVEL_NAME_REMARK 
                  || '|'
                  || rec_supervisor.TRAVEL_CLASS_NAME 
                  || '|'
                  || rec_supervisor.GDS_PROFILE_NAME 
                  || '|'
                  || rec_supervisor.ORGUNIT_OR_DIVISION                   
                  || '|'
                  || rec_supervisor.HOME_STREET_ADDRESS 
                  || '|'
                  || rec_supervisor.HOME_CITY 
                  || '|'
                  || rec_supervisor.HOME_STATE 
                  || '|'
                  || rec_supervisor.HOME_POSTAL_CODE 
                  || '|'
                  || rec_supervisor.HOME_COUNTRY                   
                  || '|'
                  || rec_supervisor.WORK_STREET_ADDRESS 
                  || '|'
                  || rec_supervisor.WORK_CITY 
                  || '|'
                  || rec_supervisor.WORK_STATE 
                  || '|'
                  || rec_supervisor.WORK_POSTAL_CODE 
                  || '|'
                  || rec_supervisor.WORK_COUNTRY                   
                  || '|'
                  || rec_supervisor.EMAIL_2  
                  || '|'
                  || rec_supervisor.EMAIL_3  
                  || '|'
                  || rec_supervisor.CUSTOM_1  
                  || '|'
                  || rec_supervisor.CUSTOM_2  
                  || '|'
                  || rec_supervisor.CUSTOM_3  
                  || '|'
                  || rec_supervisor.CUSTOM_4
                  || '|'
                  || rec_supervisor.CUSTOM_5
                  || '|'
                  || rec_supervisor.CUSTOM_6
                  || '|'
                  || rec_supervisor.CUSTOM_7
                  || '|'
                  || rec_supervisor.CUSTOM_8
                  || '|'
                  || rec_supervisor.CUSTOM_9
                  || '|'
                  || rec_supervisor.CUSTOM_10
                  || '|'
                  || rec_supervisor.CUSTOM_11
                  || '|'
                  || rec_supervisor.CUSTOM_12
                  || '|'
                  || rec_supervisor.CUSTOM_13
                  || '|'
                  || rec_supervisor.CUSTOM_14
                  || '|'
                  || rec_supervisor.CUSTOM_15
                  || '|'
                  || rec_supervisor.CUSTOM_16
                  || '|'
                  || rec_supervisor.CUSTOM_17
                  || '|'
                  || rec_supervisor.CUSTOM_18
                  || '|'
                  || rec_supervisor.CUSTOM_19
                  || '|'
                  || rec_supervisor.CUSTOM_20
                  || '|'
                  || rec_supervisor.CUSTOM_21
                  || '|'
                  || rec_supervisor.CUSTOM_22
                  || '|'
                  || rec_supervisor.CUSTOM_23
                  || '|'
                  || rec_supervisor.CUSTOM_24
                  || '|'
                  || rec_supervisor.CUSTOM_25
                  || '|'
                  || rec_supervisor.XML_PROFILE_SYNC_ID
                  || '|'
                  || rec_supervisor.PROFILE_USER_PERMISSION
                  || '|'
                  || rec_supervisor.AMADEUS_USER_PERMISSION
                  || '|'
                  || rec_supervisor.OPEN_BOOKING_USER_PERMISSION
                  || '|'
                  || rec_supervisor.FUTURE_USE_5
                  || '|'
                  || rec_supervisor.FUTURE_USE_6
                  || '|'
                  || rec_supervisor.FUTURE_USE_7
                  || '|'
                  || rec_supervisor.FUTURE_USE_8
                  || '|'
                  || rec_supervisor.FUTURE_USE_9
                  || '|'
                  || rec_supervisor.FUTURE_USE_10
                  );

    EXCEPTION
    WHEN others THEN

      l_err_msg :='Error at supervisor cursor'
                  || '---- '
                  || SUBSTR (SQLERRM, 1, 2000);

      write_error (l_err_msg);
      write_log (l_err_msg);

    END;

  END LOOP;

  write_log ('Wrote ' || l_count || ' records to file');
  write_log ('Closing File Handler');
  g_loc := 'Now closing in file ';
      
  --Closing the file handler
  UTL_FILE.fclose (v_file);
  
  --Renaming the Temp file to a new file 
  UTL_FILE.FRENAME(l_directory_name, l_filename, l_directory_name, l_filename_final, TRUE);

  p_retcode := '0';
  
  write_log ('HDS CONCUR: Generate Concur Employee Export File - Program Successfully completed');

EXCEPTION
WHEN UTL_FILE.invalid_path THEN

   UTL_FILE.fclose (v_file);
   p_errbuf := 'File Path is invalid.';
   p_retcode := '2';
   write_error (p_errbuf);
   write_log (p_errbuf);
   
WHEN UTL_FILE.invalid_mode THEN

   UTL_FILE.fclose (v_file);
   p_errbuf := 'The open_mode parameter in FOPEN is invalid.';
   p_retcode := '2';
   write_error (p_errbuf);
   write_log (p_errbuf);
   
WHEN UTL_FILE.invalid_filehandle THEN
   UTL_FILE.fclose (v_file);
   p_errbuf := 'File handle is invalid..';
   p_retcode := '2';
   write_error (p_errbuf);
   write_log (p_errbuf);
   
WHEN UTL_FILE.invalid_operation THEN

   UTL_FILE.fclose (v_file);
   p_errbuf := 'File could not be opened or operated on as requested';
   p_retcode := '2';
   write_error (p_errbuf);
   write_log (p_errbuf);
   
WHEN UTL_FILE.write_error THEN

   UTL_FILE.fclose (v_file);
   p_errbuf :='Operating system error occurred during the write operation';
   p_retcode := '2';
   write_error (p_errbuf);
   write_log (p_errbuf);
   
WHEN UTL_FILE.internal_error THEN

   UTL_FILE.fclose (v_file);
   write_log ('Unspecified PL/SQL error.');
   
WHEN UTL_FILE.file_open THEN

   UTL_FILE.fclose (v_file);
   p_errbuf := 'The requested operation failed because the file is open.';
   p_retcode := '2';
   write_error (p_errbuf);
   write_log (p_errbuf);
   
WHEN UTL_FILE.invalid_filename THEN

   UTL_FILE.fclose (v_file);
   p_errbuf := 'The filename parameter is invalid.';
   p_retcode := '2';
   write_error (p_errbuf);
   write_log (p_errbuf);
   
WHEN UTL_FILE.access_denied THEN

   UTL_FILE.fclose (v_file);
   p_errbuf := 'Permission to access to the file location is denied.';
   p_retcode := '2';
   write_error (p_errbuf);
   write_log (p_errbuf);
   
WHEN g_prog_exception THEN

   p_errbuf := l_err_msg;
   p_retcode := '2';
   write_error (l_err_msg);
   write_log (l_err_msg);
   
WHEN others THEN

   l_err_msg := 'Error Msg :' || SQLERRM;
   p_errbuf := l_err_msg;
   p_retcode := '2';
   write_error (l_err_msg);
   write_log (l_err_msg);

END USER_EXPORT;

END XXCUS_CONCUR_EMP_PKG;
/