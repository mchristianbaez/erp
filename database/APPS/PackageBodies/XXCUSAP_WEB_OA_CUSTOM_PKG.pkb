CREATE OR REPLACE PACKAGE BODY APPS."XXCUSAP_WEB_OA_CUSTOM_PKG" AS
/* $Header: XXCUSAP_WEB_OA_CUSTOM_PKG.pls 115.3 2007/08/08 22:30:11 mani kumar noship $
   			Fax Number is made to profile option value. New Profile  (XXCUS_OIE_EXP_REP_FAX_NUM)
			XXCUSIE: Expense Report Fax Number 	 		*/

    ------------------------------------------------------------------------------------
    -- Function   : GetCustomizedExpRepSummary
    -- Author     : Mani Kumar
    -- Description: This function is part of the HDS extension solution which provides
    --              a way to display a customized Expense Report Summary in the
    --              Review Page and the Confirmation Page.
    -- Assumptions: Custom Profile Option XXCUSIE: Expense Report Fax Number exists
    --              and is up-to-date with the fax number information.
    --
    -- Parameters :
    --
    --    P_ReportHeaderId    Contains the Expense Report Header Id
    --    P_CurrentPage       Contains the page where the call was made (Current Page)
    ------------------------------------------------------------------------------------

FUNCTION GetCustomExpRep(P_ReportHeaderId IN VARCHAR2,
		 					P_CurrentPage IN VARCHAR2)	RETURN VARCHAR2 IS
l_msgtxt VARCHAR2(2000);
l_invoice_num	 AP_EXPENSE_REPORT_HEADERS.invoice_num%TYPE;
l_employee_id	 AP_EXPENSE_REPORT_HEADERS.employee_id%TYPE;
l_total 		 VARCHAR2(200);
l_description	 AP_EXPENSE_REPORT_HEADERS.description%TYPE;
l_full_name		 PER_ALL_PEOPLE_F.FULL_NAME%TYPE;
l_employee_number  PER_ALL_PEOPLE_F.EMPLOYEE_NUMBER%TYPE;
l_fax_to		   VARCHAR2(25) := FND_PROFILE.VALUE('XXCUS_OIE_EXP_REP_FAX_NUM');
BEGIN
--
-- These are the queries to pull up data for an expense report.
--

BEGIN
	 SELECT aer.INVOICE_NUM, aer.EMPLOYEE_ID, LTRIM(TO_CHAR(aer.TOTAL,'$999999999.99')), aer.DESCRIPTION
	 INTO l_invoice_num, l_employee_id, l_total, l_description
	 FROM ap_expense_report_headers aer
	 WHERE aer.report_header_id = P_ReportHeaderId;
	 EXCEPTION
	 	WHEN OTHERS THEN
			 Fnd_Message.SET_NAME('SQLAP','AP_DEBUG');
			 Fnd_Message.SET_TOKEN('ERROR',SQLERRM);
			 Fnd_Message.SET_TOKEN('CALLING_SEQUENCE','GetExpenseReportSummary');
			 Fnd_Message.SET_TOKEN('DEBUG_INFO','Page: '||P_CurrentPage||'Report: '||P_ReportHeaderId);
			 l_msgtxt := Fnd_Message.get;
	 	RETURN l_msgtxt;
END;

BEGIN
	 SELECT papf.FULL_NAME , papf.EMPLOYEE_NUMBER
	 INTO l_full_name, l_employee_number
	 FROM per_all_people_f papf
	 WHERE papf.PERSON_ID = l_employee_id
	 AND   (TO_CHAR(papf.effective_end_date,'DD-MON-YYYY') = '31-DEC-4712' OR papf.effective_end_date IS NULL);
	 EXCEPTION
	 	WHEN OTHERS THEN
			 Fnd_Message.SET_NAME('SQLAP','AP_DEBUG');
			 Fnd_Message.SET_TOKEN('ERROR',SQLERRM);
			 Fnd_Message.SET_TOKEN('CALLING_SEQUENCE','GetExpenseReportSummary1');
			 Fnd_Message.SET_TOKEN('DEBUG_INFO','Page: '||P_CurrentPage||'Report: '||P_ReportHeaderId);
			 l_msgtxt := Fnd_Message.get;
	 	RETURN l_msgtxt;
END;

--
-- Creating the messages.
--

l_msgtxt :=
		 '<HTML>'||
		 '<BODY>'||
		 '<br><br>'||
		 '<pre>'||
         '<FONT size = "6" color = "black" align="left" face="Arial, Helvetica,sans-serif" >'||
		 '      '||TO_CHAR(l_invoice_num)||
         '</pre>'||
		 '<br><br>'||
		 '<FONT size = "5" color = "black">'||
     '<center>'||
		 '<b> iExpense Fax Cover Page <br>'||		 		      
		 'Fax this page and your receipts to: <br>'||     
		 l_fax_to||' </b> </center> </FONT>'||
		 '<br><br><br>'||
		 '<pre>'||
         '<FONT size = "3" color = "black" align="left" face="Arial, Helvetica,sans-serif" >'||
		 '                                                  Report Summary  '||
 		 '<br>'||
		 '                                                  Employee Name:        '||TO_CHAR(l_full_name)||
		 '<br>'||
		 '                                                  Employee ID:              '||TO_CHAR(l_employee_number)||
		 '<br>'||
		 '                                                  Report Total:               '||TO_CHAR(l_total)||
		 '<br>'||
		 '                                                  Report Name:             '||TO_CHAR(l_description)||
         '</pre>'||
		 '<br><br><br><br><br><br><br><br><br><br><br>'||
		 '<pre>'||
         '<FONT size = "5" color = "black" align="left" face="Arial, Helvetica,sans-serif" >'||
		 '                                                                               '||TO_CHAR(l_invoice_num)||
         '</pre>'||
		 '<br>'||
		 '</BODY>'||
		 '</HTML>';

-----l_msgtxt :=  l_invoice_num||' and '||l_employee_id|| 'and '||l_total ||'and '||l_description	;
--
-- In the review page, the user may not have clicked the save button
-- after making changes. Display a warning message to remind the
-- user to save in after order to view the refreshed expense report
-- summary.
--
IF (P_CurrentPage = Ap_Web_Oa_Custom_Pkg.C_ReviewPage) THEN
   l_msgtxt := l_msgtxt||
		 '<tr><td><b><font size="2" face="Arial, Helvetica,sans-serif">'||
		 'Your expense account must be "saved" to ensure the calculated '||
		 'totals below include all appropriate amounts. If you have	 not '||
		 'done so please do so now.</td></tr>'||
		 '<tr><td height="2"></td></tr>';
END IF;
	RETURN l_msgtxt;
END GetCustomExpRep;


END XXCUSAP_WEB_OA_CUSTOM_PKG;
/
