--
-- XXWC_PRICE_LIST_WEB_ADI_PKG  (Package Body) 
--
CREATE OR REPLACE PACKAGE BODY APPS.xxwc_price_list_web_adi_pkg
IS
   /*************************************************************************
   *  Copyright (c) 2011 Lucidity Consulting Group
   *  All rights reserved.
   **************************************************************************
   *   $Header xxwc_price_list_web_adi_pkg.pkb $
   *   Module Name: xxwc_price_list_web_adi_pkg.pkb
   *
   *   PURPOSE:   This package is used by the Price List Conversion
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        09/01/2011  Vivek Lakaman             Initial Version
   * ***************************************************************************/

   /*************************************************************************
   *   Procedure : LOG_MSG
   *
   *   PURPOSE:   This procedure is the table handler called by the On-Insert Trigger
   *   Parameter:
   *          IN
   *              p_debug_level    -- Debug Level
   *              p_mod_name       -- Module Name
   *              p_debug_msg      -- Debug Message
   * ************************************************************************/

   -- add debug message to log table and concurrent log file
   PROCEDURE LOG_MSG (p_debug_level   IN NUMBER
                     ,p_mod_name      IN VARCHAR2
                     ,p_debug_msg     IN VARCHAR2)
   IS
      PRAGMA AUTONOMOUS_TRANSACTION;
   BEGIN
      --IF (p_debug_level >= FND_LOG.G_CURRENT_RUNTIME_LEVEL)
      --THEN
      -- Call Oracle FND_LOG API to insert the debug message
      FND_LOG.STRING (p_debug_level, p_mod_name, P_DEBUG_MSG);
      -- END IF;

      fnd_file.put_line (fnd_file.LOG, P_DEBUG_MSG);
      COMMIT;
   END LOG_MSG;


   /*************************************************************************
   *   Procedure : Update_function
   *
   *   PURPOSE:   This procedure is used to log the debug message
   *   Parameter:
   *          IN
   *              p_list_header_id    -- List Header Id
   * ************************************************************************/

   PROCEDURE Update_function (p_list_header_id IN NUMBER)
   IS
      CURSOR c1
      IS
         SELECT *
           FROM FND_FORM_FUNCTIONS_VL
          WHERE FUNCTION_NAME = 'BNE_XXWC_QP_CONV';

      X_WEB_HOST_NAME            FND_FORM_FUNCTIONS_VL.WEB_HOST_NAME%TYPE;
      X_WEB_AGENT_NAME           FND_FORM_FUNCTIONS_VL.WEB_AGENT_NAME%TYPE;
      X_WEB_HTML_CALL            FND_FORM_FUNCTIONS_VL.WEB_HTML_CALL%TYPE;
      X_WEB_ENCRYPT_PARAMETERS   FND_FORM_FUNCTIONS_VL.WEB_ENCRYPT_PARAMETERS%TYPE;
      X_WEB_SECURED              FND_FORM_FUNCTIONS_VL.WEB_SECURED%TYPE;
      X_WEB_ICON                 FND_FORM_FUNCTIONS_VL.WEB_ICON%TYPE;
      X_OBJECT_ID                FND_FORM_FUNCTIONS_VL.OBJECT_ID%TYPE;
      X_REGION_APPLICATION_ID    FND_FORM_FUNCTIONS_VL.REGION_APPLICATION_ID%TYPE;
      X_REGION_CODE              FND_FORM_FUNCTIONS_VL.REGION_CODE%TYPE;
      X_FUNCTION_NAME            FND_FORM_FUNCTIONS_VL.FUNCTION_NAME%TYPE;
      X_APPLICATION_ID           FND_FORM_FUNCTIONS_VL.APPLICATION_ID%TYPE;
      X_FORM_ID                  FND_FORM_FUNCTIONS_VL.FORM_ID%TYPE;
      X_PARAMETERS               FND_FORM_FUNCTIONS_VL.PARAMETERS%TYPE;
      X_TYPE                     FND_FORM_FUNCTIONS_VL.TYPE%TYPE;
      X_USER_FUNCTION_NAME       FND_FORM_FUNCTIONS_VL.USER_FUNCTION_NAME%TYPE;
      X_DESCRIPTION              FND_FORM_FUNCTIONS_VL.DESCRIPTION%TYPE;
      X_LAST_UPDATE_DATE         FND_FORM_FUNCTIONS_VL.LAST_UPDATE_DATE%TYPE;
      X_LAST_UPDATED_BY          FND_FORM_FUNCTIONS_VL.LAST_UPDATED_BY%TYPE;
      X_LAST_UPDATE_LOGIN        FND_FORM_FUNCTIONS_VL.LAST_UPDATE_LOGIN%TYPE;

      l_param1                   VARCHAR2 (1000);
      l_param2                   VARCHAR2 (200);
      l_param                    VARCHAR2 (2000);
      l_mod_name                 VARCHAR2 (200)
         := 'xxwc_price_list_web_adi_pkg.Update_function :';
   BEGIN
      LOG_MSG (g_level_statement, l_mod_name, 'At Begin');
      LOG_MSG (g_level_statement
              ,l_mod_name
              ,'p_list_header_id =>' || p_list_header_id);

      l_param1 :=
         'bne:page=BneCreateDoc:viewer=BNE:EXCEL2007:reporting=N:integrator=XXWC:XXWC_QP_CONV_XINTG:layout=XXWC:XXWC_QP_CONV:content=XXWC:XXWC_QP_CONV_CNT2=';
      l_param2 := ':map=XXWC:XXWC_QP_CONV:noreview=Y';
      l_param := l_param1 || p_list_header_id || l_param2;

      fnd_global.apps_initialize (user_id        => fnd_global.user_id
                                 ,resp_id        => fnd_global.resp_id
                                 ,resp_appl_id   => fnd_global.resp_appl_id);

      FOR i IN C1
      LOOP
         BEGIN
            LOG_MSG (g_level_statement
                    ,l_mod_name
                    ,'i.function_id=>' || i.function_id);
            LOG_MSG (g_level_statement
                    ,l_mod_name
                    ,'i.web_host_name=>' || i.web_host_name);
            LOG_MSG (g_level_statement
                    ,l_mod_name
                    ,'i.web_html_call=>' || i.web_html_call);
            LOG_MSG (g_level_statement
                    ,l_mod_name
                    ,'Calling fnd_form_functions_pkg');
            fnd_form_functions_pkg.update_row (
               X_FUNCTION_ID              => i.function_id
              ,x_web_host_name            => i.web_host_name
              ,x_web_agent_name           => i.web_agent_name
              ,x_web_html_call            => i.web_html_call
              ,x_web_encrypt_parameters   => i.web_encrypt_parameters
              ,x_web_secured              => i.web_secured
              ,x_web_icon                 => i.web_icon
              ,x_object_id                => i.object_id
              ,x_region_application_id    => i.region_application_id
              ,x_region_code              => i.region_code
              ,x_function_name            => i.function_name
              ,x_application_id           => i.application_id
              ,x_form_id                  => i.form_id
              ,x_parameters               => l_param
              ,x_type                     => i.TYPE
              ,x_user_function_name       => i.user_function_name
              ,x_description              => i.description
              ,x_last_update_date         => SYSDATE
              ,x_last_updated_by          => fnd_global.user_id
              ,x_last_update_login        => fnd_global.login_id);
            LOG_MSG (g_level_statement
                    ,l_mod_name
                    ,'Rows Updated =>' || SQL%ROWCOUNT);
         EXCEPTION
            WHEN OTHERS
            THEN
               LOG_MSG (g_level_statement, l_mod_name, 'Error =>' || SQLERRM);
         END;
      END LOOP;

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
   END Update_function;

   /*************************************************************************
    *   Procedure : Get_product_attr_value
    *
    *   PURPOSE:   This procedure is used to get the item number or the category
    *   Parameter:
    *          IN
    *              p_product_attribute  -- product_attribute
    *              p_product_attr_value -- product_attr_value
    * ************************************************************************/

   FUNCTION Get_product_attr_value (p_product_attribute    IN VARCHAR2
                                   ,p_product_attr_value   IN VARCHAR2)
      RETURN VARCHAR2
   IS
      l_return_value   VARCHAR2 (240) := NULL;
   BEGIN
      IF p_product_attribute = 'PRICING_ATTRIBUTE1'
      THEN
         SELECT segment1
           INTO l_return_value
           FROM mtl_system_items
          WHERE     inventory_item_id = TO_NUMBER (p_product_attr_value)
                AND Organization_ID =
                       Fnd_profile.VALUE ('XXWC_ITEM_MASTER_ORG')
                AND ROWNUM = 1;
      ELSIF p_product_attribute = 'PRICING_ATTRIBUTE2'
      THEN
         SELECT category_concat_segs
           INTO l_return_value
           FROM mtl_categories_v
          WHERE category_id = TO_NUMBER (p_product_attr_value);
      ELSIF p_product_attribute = 'PRICING_ATTRIBUTE3'
      THEN
         l_return_value := 'ALL';
      END IF;

      RETURN l_return_value;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN l_return_value;
   END Get_product_attr_value;

   /*************************************************************************
    *   Procedure : Get_Formula_name
    *
    *   PURPOSE:   This procedure is used to get formula name
    *   Parameter:
    *          IN
    *              p_formula_id  -- product_attribute
    * ************************************************************************/

   FUNCTION Get_Formula_name (p_formula_id IN NUMBER)
      RETURN VARCHAR2
   IS
      l_return_value   VARCHAR2 (300) := NULL;
   BEGIN
      SELECT name
        INTO l_return_value
        FROM qp_price_formulas_vl
       WHERE price_formula_id = p_formula_id;

      RETURN l_return_value;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN l_return_value;
   END Get_Formula_name;



   /*************************************************************************
    *   Procedure : import
    *
    *   PURPOSE:   This procedure is called from the WEDADI excel spreadsheet
    *   Parameter:
    *          IN
    *              p_list_header_id      -- Price List Id
    *              p_price_list_name     -- Price list name
    *              p_list_line_id        -- Price Line Id
    *              p_product_attr_value  -- Item Num
    *              p_product_uom_code    -- UOM Code
    *              p_operand             -- Unit Selling Price
    *              p_start_date_active   -- Start Date
    *              p_end_date_active     -- End date
    *              p_product_attribute   -- Product attribute either ITEM or ITEM_CATEGORY
    *              p_dynamic_formula_id  -- Dynamic formula Id
    *              p_static_formula_id   -- Static Formula Id
    *              p_escalator_dff       -- Escalator DFF Column
    * ************************************************************************/


   PROCEDURE import (p_list_header_id        NUMBER
                    ,p_price_list_name       VARCHAR2
                    ,p_list_line_id          NUMBER
                    ,p_product_attribute     VARCHAR2
                    ,p_product_attr_value    VARCHAR2
                    ,p_product_uom_code      VARCHAR2
                    ,p_operand               NUMBER
                    ,p_start_date_active     DATE
                    ,p_end_date_active       DATE
                    ,p_dynamic_formula_id    NUMBER
                    ,p_static_formula_id     NUMBER
                    ,p_escalator_dff         VARCHAR2
                    ,P_Precedence            NUMBER)
   IS
      v_return_status               VARCHAR2 (1) := NULL;
      v_msg_count                   NUMBER := 0;
      v_msg_data                    VARCHAR2 (2000);
      --l_list_header_id              NUMBER;
      l_inv_item_id                 NUMBER;
      l_pricing_attribute_id        NUMBER;
      l_category_id                 NUMBER;

      v_price_list_rec              qp_price_list_pub.price_list_rec_type;
      v_price_list_val_rec          qp_price_list_pub.price_list_val_rec_type;
      v_price_list_line_tbl         qp_price_list_pub.price_list_line_tbl_type;
      v_price_list_line_val_tbl     qp_price_list_pub.price_list_line_val_tbl_type;
      v_qualifiers_tbl              qp_qualifier_rules_pub.qualifiers_tbl_type;
      v_qualifiers_val_tbl          qp_qualifier_rules_pub.qualifiers_val_tbl_type;
      v_pricing_attr_tbl            qp_price_list_pub.pricing_attr_tbl_type;
      v_pricing_attr_val_tbl        qp_price_list_pub.pricing_attr_val_tbl_type;
      ppr_price_list_rec            qp_price_list_pub.price_list_rec_type;
      ppr_price_list_val_rec        qp_price_list_pub.price_list_val_rec_type;
      ppr_price_list_line_tbl       qp_price_list_pub.price_list_line_tbl_type;
      ppr_price_list_line_val_tbl   qp_price_list_pub.price_list_line_val_tbl_type;
      ppr_qualifiers_tbl            qp_qualifier_rules_pub.qualifiers_tbl_type;
      ppr_qualifiers_val_tbl        qp_qualifier_rules_pub.qualifiers_val_tbl_type;
      ppr_pricing_attr_tbl          qp_price_list_pub.pricing_attr_tbl_type;
      ppr_pricing_attr_val_tbl      qp_price_list_pub.pricing_attr_val_tbl_type;
      l_mod_name                    VARCHAR2 (100)
         := 'XXWC_PRICE_LIST_WEB_ADI_PKG.IMPORT :';

      x_error_msg                   VARCHAR2 (1000);

      e_validation_error            EXCEPTION;
      e_api_failure                 EXCEPTION;
   BEGIN
      LOG_MSG (g_LEVEL_STATEMENT, l_mod_name, 'At Begin');
      oe_msg_pub.initialize;
      LOG_MSG (g_LEVEL_STATEMENT
              ,l_mod_name
              ,'p_list_header_id =' || p_list_header_id);
      LOG_MSG (g_LEVEL_STATEMENT
              ,l_mod_name
              ,'p_price_list_name =' || p_price_list_name);
      LOG_MSG (g_LEVEL_STATEMENT
              ,l_mod_name
              ,'p_list_line_id =' || p_list_line_id);
      LOG_MSG (g_LEVEL_STATEMENT
              ,l_mod_name
              ,'p_product_attribute =' || p_product_attribute);
      LOG_MSG (g_LEVEL_STATEMENT
              ,l_mod_name
              ,'p_product_attr_value =' || p_product_attr_value);
      LOG_MSG (g_LEVEL_STATEMENT
              ,l_mod_name
              ,'p_product_uom_code =' || p_product_uom_code);
      LOG_MSG (g_LEVEL_STATEMENT, l_mod_name, 'p_operand =' || p_operand);
      LOG_MSG (g_LEVEL_STATEMENT
              ,l_mod_name
              ,'p_start_date_active =' || p_start_date_active);
      LOG_MSG (g_LEVEL_STATEMENT
              ,l_mod_name
              ,'p_end_date_active =' || p_end_date_active);
      LOG_MSG (g_LEVEL_STATEMENT
              ,l_mod_name
              ,'p_dynamic_formula_id =' || p_dynamic_formula_id);
      LOG_MSG (g_LEVEL_STATEMENT
              ,l_mod_name
              ,'p_escalator_dff =' || p_escalator_dff);
      LOG_MSG (g_LEVEL_STATEMENT
              ,l_mod_name
              ,'p_precedence =' || p_Precedence);

      SELECT mo_global.get_current_org_id () INTO g_org_id FROM DUAL;

      LOG_MSG (g_LEVEL_STATEMENT, l_mod_name, 'g_org_id =' || g_org_id);

      g_org_id := fnd_profile.VALUE ('ORG_ID');
      LOG_MSG (g_LEVEL_STATEMENT, l_mod_name, 'g_org_id =' || g_org_id);

      --mo_global.set_policy_context ('S', g_org_id);
      --mo_global.set_policy_context ('S', 161);
      --mo_global.init ('ONT');
      -- INITIALIZATION REQUIRED FOR R12
      fnd_global.apps_initialize (user_id        => fnd_global.user_id
                                 ,resp_id        => fnd_global.resp_id
                                 ,resp_appl_id   => fnd_global.resp_appl_id);


      IF p_product_attribute = 'ITEM'
      THEN
         BEGIN
            LOG_MSG (g_LEVEL_STATEMENT
                    ,l_mod_name
                    ,'Validating Item =' || p_product_attr_value);

            SELECT inventory_item_id
              INTO l_inv_item_id
              FROM mtl_system_items
             WHERE segment1 = p_product_attr_value AND ROWNUM = 1;
         EXCEPTION
            WHEN OTHERS
            THEN
               fnd_message.set_name ('XXWC', 'XXWC_QP_ITEM_MISSING');
               fnd_message.set_token ('XXWC_ITEM', p_product_attr_value);
               RAISE e_validation_error;
         END;

         LOG_MSG (g_LEVEL_STATEMENT
                 ,l_mod_name
                 ,'l_inv_item_id =' || l_inv_item_id);
      ELSIF p_product_attribute = 'ITEM_CATEGORY'
      THEN
         BEGIN
            LOG_MSG (g_LEVEL_STATEMENT
                    ,l_mod_name
                    ,'Validating Item Category=' || p_product_attr_value);

            SELECT category_id
              INTO l_category_id
              FROM mtl_categories_v
             WHERE UPPER (category_concat_segs) =
                      UPPER (p_product_attr_value);
         EXCEPTION
            WHEN OTHERS
            THEN
               fnd_message.set_name ('XXWC', 'XXWC_QP_CATEGORY_MISSING');
               fnd_message.set_token ('XXWC_CATEGORY', p_product_attr_value);
               RAISE e_validation_error;
         END;

         LOG_MSG (g_LEVEL_STATEMENT
                 ,l_mod_name
                 ,'l_category_id =' || l_category_id);
      END IF;


      IF p_dynamic_formula_id IS NOT NULL AND p_static_formula_id IS NOT NULL
      THEN
         fnd_message.set_name ('XXWC', 'XXWC_QP_DYNAMIC_STATIC');
         RAISE e_validation_error;
      END IF;

      v_price_list_line_tbl (1).list_header_id := p_list_header_id;   --13328;
      v_price_list_line_tbl (1).list_line_type_code := 'PLL';

      IF p_list_line_id IS NULL
      THEN
         v_price_list_line_tbl (1).operation := qp_globals.g_opr_create;
         v_price_list_line_tbl (1).list_line_id := fnd_api.g_miss_num;

         IF    p_dynamic_formula_id IS NOT NULL
            OR p_static_formula_id IS NOT NULL
         THEN
            v_price_list_line_tbl (1).price_by_formula_id :=
               p_dynamic_formula_id;
            v_price_list_line_tbl (1).generate_using_formula_id :=
               p_static_formula_id;
         ELSE
            v_price_list_line_tbl (1).operand := p_operand;
         END IF;

         v_price_list_line_tbl (1).arithmetic_operator := 'UNIT_PRICE';
         v_price_list_line_tbl (1).start_date_active := p_start_date_active;
         v_price_list_line_tbl (1).end_date_active := p_end_date_active;
         v_price_list_line_tbl (1).organization_id := NULL;
         v_price_list_line_tbl (1).attribute1 := p_escalator_dff;


         v_pricing_attr_tbl (1).operation := qp_globals.g_opr_create;
         v_pricing_attr_tbl (1).pricing_attribute_id := fnd_api.g_miss_num;
         v_pricing_attr_tbl (1).list_line_id := fnd_api.g_miss_num;
         v_pricing_attr_tbl (1).product_attribute_context := 'ITEM';

         IF p_product_attribute = 'ITEM'
         THEN
            v_pricing_attr_tbl (1).product_attribute := 'PRICING_ATTRIBUTE1';
            v_pricing_attr_tbl (1).product_attr_value :=
               TO_CHAR (l_inv_item_id);                           --'2013686';
         ELSIF p_product_attribute = 'ITEM_CATEGORY'
         THEN
            v_pricing_attr_tbl (1).product_attribute := 'PRICING_ATTRIBUTE2';
            v_pricing_attr_tbl (1).product_attr_value :=
               TO_CHAR (l_category_id);
         ELSIF p_product_attribute = 'ALL'
         THEN
            v_pricing_attr_tbl (1).product_attribute := 'PRICING_ATTRIBUTE3';
            v_pricing_attr_tbl (1).product_attr_value := 'ALL';
         END IF;

         v_pricing_attr_tbl (1).product_uom_code := p_product_uom_code; --'EA';
         v_pricing_attr_tbl (1).excluder_flag := 'N';
         v_pricing_attr_tbl (1).attribute_grouping_no := 1;
         v_pricing_attr_tbl (1).price_list_line_index := 1;
      ELSE
         v_price_list_line_tbl (1).operation := qp_globals.g_opr_update;
         v_price_list_line_tbl (1).list_line_id := p_list_line_id;

         IF p_product_attribute = 'ITEM'
         THEN
            v_pricing_attr_tbl (1).product_attribute := 'PRICING_ATTRIBUTE1';
            v_pricing_attr_tbl (1).product_attr_value :=
               TO_CHAR (l_inv_item_id);
         ELSIF p_product_attribute = 'ITEM_CATEGORY'
         THEN
            v_pricing_attr_tbl (1).product_attribute := 'PRICING_ATTRIBUTE2';
            v_pricing_attr_tbl (1).product_attr_value :=
               TO_CHAR (l_category_id);
         ELSIF p_product_attribute = 'ALL'
         THEN
            v_pricing_attr_tbl (1).product_attribute := 'PRICING_ATTRIBUTE3';
            v_pricing_attr_tbl (1).product_attr_value := 'ALL';
         END IF;

         IF    p_dynamic_formula_id IS NOT NULL
            OR p_static_formula_id IS NOT NULL
         THEN
            v_price_list_line_tbl (1).price_by_formula_id :=
               p_dynamic_formula_id;
            v_price_list_line_tbl (1).generate_using_formula_id :=
               p_static_formula_id;
         ELSE
            v_price_list_line_tbl (1).operand := p_operand;
         END IF;

         -- Satish U: 10-APR-2012 :
         LOG_MSG (g_LEVEL_STATEMENT
                 ,l_mod_name
                 ,'Product Precedence=' || p_Precedence);
         v_price_list_line_tbl (1).Product_Precedence := p_Precedence;
         v_price_list_line_tbl (1).start_date_active := p_start_date_active;
         v_price_list_line_tbl (1).end_date_active := p_end_date_active;
         v_price_list_line_tbl (1).attribute1 := p_escalator_dff;
      END IF;

      LOG_MSG (g_LEVEL_STATEMENT
              ,l_mod_name
              ,'Calling API to Enter Item Into Price List');

      qp_price_list_pub.process_price_list (
         p_api_version_number        => 1
        ,p_init_msg_list             => fnd_api.g_true
        ,p_return_values             => fnd_api.g_false
        ,p_commit                    => fnd_api.g_false
        ,x_return_status             => v_return_status
        ,x_msg_count                 => v_msg_count
        ,x_msg_data                  => v_msg_data
        ,p_price_list_rec            => v_price_list_rec
        ,p_price_list_line_tbl       => v_price_list_line_tbl
        ,p_pricing_attr_tbl          => v_pricing_attr_tbl
        ,x_price_list_rec            => ppr_price_list_rec
        ,x_price_list_val_rec        => ppr_price_list_val_rec
        ,x_price_list_line_tbl       => ppr_price_list_line_tbl
        ,x_qualifiers_tbl            => ppr_qualifiers_tbl
        ,x_qualifiers_val_tbl        => ppr_qualifiers_val_tbl
        ,x_pricing_attr_tbl          => ppr_pricing_attr_tbl
        ,x_pricing_attr_val_tbl      => ppr_pricing_attr_val_tbl
        ,x_price_list_line_val_tbl   => ppr_price_list_line_val_tbl);


      IF v_return_status = fnd_api.g_ret_sts_success
      THEN
         --COMMIT;
         LOG_MSG (g_LEVEL_STATEMENT
                 ,l_mod_name
                 ,'The Item loading into the price list is Sucessfull');
      ELSE
         LOG_MSG (g_LEVEL_STATEMENT
                 ,l_mod_name
                 ,'The Item loading into the price list Failed');

         FOR i IN 1 .. v_msg_count
         LOOP
            v_msg_data := oe_msg_pub.get (p_msg_index => i, p_encoded => 'F');
            LOG_MSG (g_LEVEL_STATEMENT, l_mod_name, i || ') ' || v_msg_data);
         END LOOP;

         x_error_msg := v_msg_data;
         RAISE e_api_failure;
      END IF;
   EXCEPTION
      WHEN e_api_failure
      THEN
         --x_error_msg :=  fnd_message.get();
         --raise_application_error (-20001, SUBSTR (x_error_msg, 1, 100));
         NULL;
      WHEN e_validation_error
      THEN
         --x_error_msg := fnd_message.get ();
         --raise_application_error (-20001, SUBSTR (x_error_msg, 1, 100));
         NULL;
      WHEN OTHERS
      THEN
         fnd_message.set_name ('XXWC', 'XXWC_WHEN_OTHERS_ERROR');
         fnd_message.set_token ('SQLERROR_MSG', SUBSTR (SQLERRM, 1, 60));
         --fnd_message.set_name
         x_error_msg := SQLERRM;
         raise_application_error (-20001, x_error_msg);
   END import;
END xxwc_price_list_web_adi_pkg;
/

