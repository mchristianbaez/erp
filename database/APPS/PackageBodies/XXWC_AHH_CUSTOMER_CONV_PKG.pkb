CREATE OR REPLACE PACKAGE BODY APPS.XXWC_AHH_CUSTOMER_CONV_PKG
IS
   /******************************************************************************************************************************************************
        $Header XXWC_AHH_CUSTOMER_CONV_PKG $
        Module Name: XXWC_AHH_CUSTOMER_CONV_PKG.pkb

        PURPOSE:   AHH Customer Conversion

        REVISIONS:
        Ver        Date        Author           Description
        ---------  ----------  ---------------  ------------------------------------------------------------------------------------------------
        1.0        06/06/2018  P.Vamshidhar     TMS#20180308-00290 -AH HARRIS Customer Conversion
   ******************************************************************************************************************************************************/
   g_err_callfrom          VARCHAR2 (1000) := 'XXWC_AHH_CUSTOMER_CONV_PKG';
   g_module                VARCHAR2 (100) := 'XXWC';
   g_distro_list           VARCHAR2 (80) := 'wc-itdevalerts-u1@hdsupply.com';
   gvc_debug_enabled       VARCHAR2 (1) := 'Y';
   gn_org_id               NUMBER := '162';
   g_currency_code         VARCHAR2 (30);
   g_country_code          VARCHAR2 (30);
   g_api_name              VARCHAR2 (30) := 'ONT_UI_ADD_CUSTOMER';
   g_msg_data              VARCHAR2 (32767);
   g_msg_index_out         VARCHAR2 (4000);
   g_acct_status           VARCHAR2 (100);
   g_cust_acct_no          HZ_CUST_ACCOUNTS.ACCOUNT_NUMBER%TYPE;
   g_bill_to_site_use_id   NUMBER;
   g_primbillto_exists     VARCHAR2 (1);

   PROCEDURE return_msg (in_msg_count     IN     NUMBER,
                         in_msg_data      IN     VARCHAR2,
                         out_return_msg      OUT VARCHAR2)
   IS
   -- ==============================================================================================================================
   -- Procedure: return_msg
   -- Purpose: Return Message procedure
   -- ==============================================================================================================================
   --  REVISIONS:
   --   Ver        Date        Author               Description
   --  ---------  ---------- ---------------  --------------------------------------------------------------------------------------
   --  1.0        06/06/2018  P.Vamshidhar     TMS#20180308-00290 -AH HARRIS Customer Conversion
   -- ==============================================================================================================================
   BEGIN
      IF in_msg_count > 1
      THEN
         FOR I IN 1 .. in_msg_count
         LOOP
            Oe_Msg_Pub.get (p_msg_index       => i,
                            p_encoded         => Fnd_Api.G_FALSE,
                            p_data            => g_msg_data,
                            p_msg_index_out   => g_msg_index_out);
            out_return_msg :=
               out_return_msg || ':' || SUBSTR (g_msg_data, 1, 200);
         END LOOP;
      ELSE
         out_return_msg := in_msg_data;
      END IF;
   END;

   PROCEDURE debug (P_RECORD_ID IN NUMBER, p_msg IN VARCHAR2)
   IS
      -- ==============================================================================================================================
      -- Procedure: debug
      -- Purpose: Debugging procedure
      -- ==============================================================================================================================
      --  REVISIONS:
      --   Ver        Date        Author               Description
      --  ---------  ---------- ---------------  --------------------------------------------------------------------------------------
      --  1.0        06/06/2018  P.Vamshidhar     TMS#20180308-00290 -AH HARRIS Customer Conversion
      -- ==============================================================================================================================

      PRAGMA AUTONOMOUS_TRANSACTION;
      l_sec           VARCHAR2 (1000);
      lvc_procedure   VARCHAR2 (100) := 'DEBUG';
   BEGIN
      IF gvc_debug_enabled = 'Y'
      THEN
         l_sec := ' Inserting Data into Log Table';

         INSERT
           INTO XXWC.XXWC_AR_EQUIFAX_LOG_TBL (LOG_SEQ_NO, RECORD_ID, LOG_MSG)
         VALUES (XXWC.XXWC_AR_EQUIFAX_LOG_SEQ.NEXTVAL, P_RECORD_ID, P_MSG);

         COMMIT;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || lvc_procedure,
            p_calling             => l_sec,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          =>    'Error Occured for '
                                     || p_record_id
                                     || 'record id in XXWC_AR_EQUIFAX_LOG_TBL.',
            p_distribution_list   => g_distro_list,
            p_module              => g_module);
   END;


   PROCEDURE main (x_errbuf          OUT VARCHAR2,
                   x_retcode         OUT VARCHAR2,
                   p_validation   IN     VARCHAR2)
   IS
      -- ==============================================================================================================================
      -- Procedure: Main
      -- Purpose: Main procedure
      -- ==============================================================================================================================
      --  REVISIONS:
      --   Ver        Date        Author               Description
      --  ---------  ---------- ---------------  --------------------------------------------------------------------------------------
      --  1.0        06/06/2018  P.Vamshidhar     TMS#20180308-00290 -AH HARRIS Customer Conversion
      -- ==============================================================================================================================
      CURSOR CUR_MAIN
      IS
         SELECT DISTINCT XACA.CUSTOMER_NUMBER
           FROM XXWC.XXWC_AR_CONV_CUST_ACCT_TBL XACA
          WHERE     XACA.PROCESS_FLAG = 'V'
                AND NOT EXISTS
                       (SELECT 1
                          FROM XXWC.XXWC_AR_CONV_CUST_ACCT_TBL XACA1
                         WHERE     XACA.RECORD_ID = XACA1.RECORD_ID
                               AND XACA1.PROCESS_FLAG = 'E'
                               AND XACA1.CUSTOMER_SITE_NUMBER IS NULL);

      l_sec                 VARCHAR2 (32767);
      ln_cust_account_id    HZ_CUST_ACCOUNTS.CUST_ACCOUNT_ID%TYPE;
      ln_party_id           HZ_PARTIES.PARTY_ID%TYPE;
      lvc_cust_exist_flag   VARCHAR2 (1);
      ln_count              NUMBER;
   BEGIN
      mo_global.init ('AR');
      mo_global.set_org_context (162, NULL, 'AR');
      fnd_global.set_nls_context ('AMERICAN');

      IF p_validation = 'Y'
      THEN
         validate_inbound_data;
      ELSE
         --====================================================================
         -- Fetching the Currency code
         --====================================================================

         l_sec := 'Deriving Currency code, country code';

         BEGIN
            SELECT gsob.currency_code, SUBSTR (gsob.currency_code, 1, 2)
              INTO g_currency_code, g_country_code
              FROM hr_operating_units hou, gl_sets_of_books gsob
             WHERE     1 = 1
                   AND gsob.set_of_books_id = hou.set_of_books_id
                   AND hou.organization_id = gn_org_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               g_currency_code := 'USD';
               g_country_code := 'US';
         END;

         debug (NULL,
                l_sec || ' ' || g_currency_code || ' ' || g_country_code);

         l_sec := 'Main Looping Starting';

         FOR rec_main IN cur_main
         LOOP
            l_sec := 'Customer Number: ' || rec_main.customer_number;
            lvc_cust_exist_flag := 'N';
            g_primbillto_exists := NULL;

            BEGIN
               ln_party_id := NULL;
               ln_cust_account_id := NULL;

               SELECT hca.CUST_ACCOUNT_ID, hca.PARTY_ID
                 INTO ln_cust_account_id, ln_party_id
                 FROM HZ_CUST_ACCOUNTS HCA,
                      XXWC.XXWC_AHH_AR_CUST_CROSS_OVER_T XACC
                WHERE     REPLACE (REPLACE (XACC.CUST_NUM, CHR (13), ''),
                                   CHR (10),
                                   '') = rec_main.CUSTOMER_NUMBER
                      AND REPLACE (
                             REPLACE (XACC.ORACLE_CUST_NUM, CHR (13), ''),
                             CHR (10),
                             '') = HCA.ACCOUNT_NUMBER
                      AND ROWNUM = 1;

               l_sec := 'Customers Data fetched ' || ln_cust_account_id;

               IF ln_cust_account_id IS NOT NULL
               THEN
                  lvc_cust_exist_flag := 'Y';
               END IF;
            EXCEPTION
               WHEN OTHERS
               THEN
                  lvc_cust_exist_flag := 'N';
                  ln_cust_account_id := NULL;
                  ln_party_id := NULL;
            END;

            l_sec := 'Customer Number: ' || rec_main.customer_number;

            g_acct_status := NULL;

            BEGIN
               SELECT UPPER (
                         REPLACE (REPLACE (a.ACCOUNT_STATUS, CHR (13), ''),
                                  CHR (10),
                                  ''))
                 INTO g_acct_status
                 FROM XXWC.XXWC_AHH_AR_CUST_ACCT_STAT_T A
                WHERE A.CUSTOMER_NUM = rec_main.customer_number;
            EXCEPTION
               WHEN OTHERS
               THEN
                  g_acct_status := 'ACTIVE';
            END;

            l_sec :=
                  'Customer Number (customer status): '
               || rec_main.customer_number
               || ' '
               || g_acct_status;

            IF lvc_cust_exist_flag = 'N'
            THEN
               l_sec := 'Create Customer: ' || rec_main.customer_number;
               create_customer (rec_main.customer_number,
                                ln_cust_account_id,
                                ln_party_id);

               l_sec :=
                     'Create Customer bill to: '
                  || rec_main.customer_number
                  || ' ln_cust_account_id:'
                  || ln_cust_account_id
                  || ' ln_party_id:'
                  || ln_party_id;

               IF NVL (ln_cust_account_id, 0) > 0
               THEN
                  l_sec :=
                        'Create Customer bill to before: '
                     || rec_main.customer_number;
                  primary_bill_to (rec_main.customer_number,
                                   ln_cust_account_id,
                                   ln_party_id);

                  l_sec :=
                        'Create Customer bill to after: '
                     || rec_main.customer_number;

                  IF g_primbillto_exists = 'Y'
                  THEN
                     l_sec :=
                           'Create Customer Primary ship to: '
                        || rec_main.customer_number;
                     primary_ship_to (rec_main.customer_number,
                                      ln_cust_account_id,
                                      ln_party_id);
                     l_sec :=
                           'Create Customer Primary ship to after:'
                        || rec_main.customer_number;
                     multiple_shipto_sites (rec_main.customer_number,
                                            ln_cust_account_id,
                                            ln_party_id);
                     l_sec :=
                           'Create customer multiple ship to after:'
                        || rec_main.customer_number;
                  ELSE
                     UPDATE XXWC.XXWC_AR_CONV_CUST_ACCT_TBL
                        SET PROCESS_MESSAGE =
                               'Primary Bill To Creation failed',
                            PROCESS_FLAG = 'E'
                      WHERE     customer_number = rec_main.customer_number
                            AND PROCESS_MESSAGE IS NULL;

                     l_sec := 'Update flag:' || rec_main.customer_number;
                  END IF;
               ELSE
                  UPDATE XXWC.XXWC_AR_CONV_CUST_ACCT_TBL
                     SET PROCESS_MESSAGE =
                            'Customer Account creation Failure.',
                         PROCESS_FLAG = 'E'
                   WHERE     customer_number = rec_main.customer_number
                         AND PROCESS_MESSAGE IS NULL;

                  l_sec := 'Update flag1:' || rec_main.customer_number;
               END IF;
            ELSE
               multiple_shipto_sites (rec_main.customer_number,
                                      ln_cust_account_id,
                                      ln_party_id);
               l_sec :=
                     'multiple_shipto_sites (cross over):'
                  || rec_main.customer_number;

               BEGIN
                  ln_count := 0;

                  SELECT COUNT (1)
                    INTO ln_count
                    FROM XXWC.XXWC_AR_CONV_CUST_ACCT_TBL
                   WHERE     customer_number = rec_main.customer_number
                         AND process_flag = 'E';
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     ln_count := 0;
               END;

               IF ln_count = 0
               THEN
                  UPDATE XXWC.XXWC_AR_CONV_CUST_ACCT_TBL xaca1
                     SET xaca1.PROCESS_FLAG = 'Y'
                   WHERE     customer_number = rec_main.customer_number
                         AND process_flag = 'V';
               END IF;
            END IF;
         END LOOP;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         fnd_file.put_line (fnd_file.LOG, 'error occured (Main):' || l_sec);
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.MAIN',
            p_calling             => l_sec,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          => NULL,
            p_distribution_list   => g_distro_list,
            p_module              => g_module);
   END;

   PROCEDURE validate_inbound_data
   IS
      -- ==============================================================================================================================
      -- Procedure: validate_inbound_data
      -- Purpose: Validating Inbound data
      -- ==============================================================================================================================
      --  REVISIONS:
      --   Ver        Date        Author               Description
      --  ---------  ---------- ---------------  --------------------------------------------------------------------------------------
      --  1.0        06/06/2018  P.Vamshidhar     TMS#20180308-00290 -AH HARRIS Customer Conversion
      -- ==============================================================================================================================
      CURSOR cur_validate
      IS
         SELECT RECORD_ID,
                CUSTOMER_NUMBER,
                UPPER (CUSTOMER_SITE_NUMBER) CUSTOMER_SITE_NUMBER,
                BUSINESS_PHONE,
                BUSINESS_FAX,
                UPPER (CREDIT_MANAGER) credit_manager,
                PREDOMINANT_TRADE,
                UPPER (STATUS) STATUS,
                BILLING_ADDRESS1,
                BILLING_CITY,
                BILLING_COUNTY,
                BILLING_STATE,
                NVL (BILLING_COUNTRY, 'US') BILLING_COUNTRY,
                BILLING_ZIP_CODE,
                NVL (SHIPPING_ADDRESS1, BILLING_ADDRESS1) SHIPPING_ADDRESS1,
                NVL (SHIPPING_CITY, BILLING_CITY) SHIPPING_CITY,
                NVL (SHIPPING_COUNTY, BILLING_COUNTY) SHIPPING_COUNTY,
                NVL (SHIPPING_STATE, BILLING_STATE) SHIPPING_STATE,
                NVL (SHIPPING_COUNTRY, NVL (BILLING_COUNTRY, 'US'))
                   SHIPPING_COUNTRY,
                NVL (SHIPPING_ZIP_CODE, BILLING_ZIP_CODE) SHIPPING_ZIP_CODE,
                CUST_SITE_CLASSIFICATION,
                PURCHASE_ORDER_REQUIRED,
                DUNSNUMBER,
                JOB_SITE_NAME,
                ROWID ROW_ID
           FROM XXWC.XXWC_AR_CONV_CUST_ACCT_TBL xrwt
          WHERE NVL (PROCESS_FLAG, 'I') = 'I';

      CURSOR cur_err_recs
      IS
         SELECT DISTINCT RECORD_ID
           FROM XXWC.XXWC_AR_CONV_CUST_ACCT_TBL
          WHERE PROCESS_FLAG = 'E' AND CUSTOMER_SITE_NUMBER IS NULL;


      lvc_procedure   VARCHAR2 (100) := 'validate_inbound_data';
      o_ret_mess      VARCHAR2 (32767);
      ln_err_count    NUMBER := 0;
      ln_valid_num    NUMBER;
      l_sec           VARCHAR2 (32767);
      ln_rec_count    NUMBER := 0;
      ln_count        NUMBER;

      PROCEDURE err_mess (p_field_name IN VARCHAR2)
      IS
      BEGIN
         IF o_ret_mess IS NULL
         THEN
            o_ret_mess := p_field_name;
            ln_err_count := 1;
         ELSE
            o_ret_mess := o_ret_mess || '*' || p_field_name;
            ln_err_count := ln_err_count + 1;
         END IF;
      END;
   BEGIN
      -- Intializing process message
      UPDATE XXWC.XXWC_AR_CONV_CUST_ACCT_TBL
         SET PROCESS_FLAG = NULL, PROCESS_MESSAGE = NULL
       WHERE PROCESS_FLAG = 'E';

      UPDATE XXWC.XXWC_AR_CONV_CUST_ACCT_TBL
         SET BILLING_ZIP_CODE =
                DECODE (LENGTH (BILLING_ZIP_CODE),
                        4, LPAD (BILLING_ZIP_CODE, 5, '0'),
                        BILLING_ZIP_CODE)
       WHERE LENGTH (BILLING_ZIP_CODE) = 4;


      UPDATE XXWC.XXWC_AR_CONV_CUST_ACCT_TBL
         SET SHIPPING_ZIP_CODE =
                DECODE (LENGTH (SHIPPING_ZIP_CODE),
                        4, LPAD (SHIPPING_ZIP_CODE, 5, '0'),
                        SHIPPING_ZIP_CODE)
       WHERE LENGTH (SHIPPING_ZIP_CODE) = 4;


      COMMIT;

      FOR rec_validate IN cur_validate
      LOOP
         o_ret_mess := NULL;

         BEGIN
            ln_rec_count := ln_rec_count + 1;
            l_sec := 'Business Phone Number Validation';
            ln_valid_num := NULL;


            IF rec_validate.business_phone IS NOT NULL
            THEN
               BEGIN
                  ln_valid_num := NULL;

                  SELECT 1
                    INTO ln_valid_num
                    FROM DUAL
                   WHERE REGEXP_LIKE (rec_validate.business_phone,
                                      '^[[:digit:]]+$');
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     ln_valid_num := NULL;
               END;

               IF ln_valid_num IS NULL
               THEN
                  err_mess (
                     rec_validate.business_phone || ' Invalid Business Phone');
               ELSE
                  IF LENGTH (rec_validate.business_phone) < 10
                  THEN
                     err_mess (
                           rec_validate.business_phone
                        || ' Business Phone Less than 10 digits');
                  END IF;
               END IF;
            END IF;

            l_sec := 'Business Fax Number Validation';
            ln_valid_num := NULL;

            IF rec_validate.business_fax IS NOT NULL
            THEN
               BEGIN
                  ln_valid_num := NULL;

                  SELECT 1
                    INTO ln_valid_num
                    FROM DUAL
                   WHERE REGEXP_LIKE (rec_validate.business_fax,
                                      '^[[:digit:]]+$');
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     ln_valid_num := NULL;
               END;

               IF ln_valid_num IS NULL
               THEN
                  err_mess (
                     rec_validate.business_fax || ' Invalid Business Fax');
               ELSE
                  IF LENGTH (rec_validate.business_fax) < 10
                  THEN
                     err_mess (
                           rec_validate.business_fax
                        || ' Business Fax Less than 10 digits');
                  END IF;
               END IF;
            END IF;


            l_sec := 'Credit Manager (Collector_id) Validation';
            ln_valid_num := NULL;

            IF rec_validate.credit_manager IS NOT NULL
            THEN
               BEGIN
                  SELECT COUNT (1)
                    INTO ln_valid_num
                    FROM AR_COLLECTORS a, XXWC.XXWC_AHH_CRD_COLL_ANALYST_T b
                   WHERE     UPPER (TRIM (b.ahh_coll_code)) =
                                UPPER (TRIM (rec_validate.credit_manager))
                         AND B.ORA_COLL_ID = a.collector_id;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     ln_valid_num := 0;
               END;

               IF NVL (ln_valid_num, 0) = 0
               THEN
                  err_mess (
                     'Credit Manager (Collector_id) is blank or Credit Manager not defined in Reference table.');
               END IF;
            END IF;

            IF rec_validate.credit_manager IS NOT NULL
            THEN
               ln_valid_num := NULL;

               BEGIN
                  SELECT COUNT (1)
                    INTO ln_valid_num
                    FROM AR_COLLECTORS a, XXWC.XXWC_AHH_CRD_COLL_ANALYST_T b
                   WHERE     UPPER (TRIM (b.ahh_coll_code)) =
                                UPPER (TRIM (rec_validate.credit_manager))
                         AND B.ORA_CRD_ANLST_ID = a.RESOURCE_ID;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     ln_valid_num := 0;
               END;

               IF NVL (ln_valid_num, 0) = 0
               THEN
                  err_mess (
                     'Credit Manager (Credit Analyst) is blank or Credit Manager not defined in Reference table.');
               END IF;
            END IF;

            IF     rec_validate.JOB_SITE_NAME IS NULL
               AND rec_validate.CUSTOMER_SITE_NUMBER IS NOT NULL
            THEN
               err_mess ('Job Site Name should not be null/blank.');
            END IF;

            IF rec_validate.BILLING_ADDRESS1 IS NULL
            THEN
               err_mess ('Billing Address1 is blank.');
            END IF;

            l_sec := 'City, County,State and Zipcode Validation';

            IF     rec_validate.BILLING_CITY IS NULL
               AND rec_validate.BILLING_COUNTY IS NULL
               AND rec_validate.BILLING_STATE IS NULL
               AND rec_validate.BILLING_ZIP_CODE IS NULL
            THEN
               err_mess ('Billing City, County,State,Zip_code are blank.');
            END IF;

            --             l_sec := 'City, County,State and Zipcode Validation';
            --
            --            IF     rec_validate.SHIPPING_CITY IS NULL
            --               AND rec_validate.SHIPPING_COUNTY IS NULL
            --               AND rec_validate.SHIPPING_STATE IS NULL
            --               AND rec_validate.SHIPPING_ZIP_CODE IS NULL
            --            THEN
            --               err_mess ('Shipping City, County,State,Zip_code are blank.');
            --            END IF;

            ln_valid_num := NULL;
            l_sec := 'DUNSNUMBER Validation';
            ln_valid_num := NULL;

            IF rec_validate.dunsnumber IS NOT NULL
            THEN
               BEGIN
                  ln_valid_num := NULL;

                  SELECT 1
                    INTO ln_valid_num
                    FROM DUAL
                   WHERE REGEXP_LIKE (rec_validate.dunsnumber,
                                      '^[[:digit:]]+$');
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     ln_valid_num := NULL;
               END;

               IF ln_valid_num IS NULL
               THEN
                  err_mess (rec_validate.dunsnumber || ' Invalid Dunsnumber');
               END IF;
            END IF;

            IF o_ret_mess IS NOT NULL
            THEN
               UPDATE XXWC.XXWC_AR_CONV_CUST_ACCT_TBL
                  SET process_message = o_ret_mess, process_flag = 'E'
                WHERE ROWID = rec_validate.row_id;
            ELSE
               UPDATE XXWC.XXWC_AR_CONV_CUST_ACCT_TBL
                  SET process_flag = 'V'
                WHERE ROWID = rec_validate.row_id;
            END IF;
         EXCEPTION
            WHEN OTHERS
            THEN
               NULL;
               xxcus_error_pkg.xxcus_error_main_api (
                  p_called_from         => g_err_callfrom || '.' || lvc_procedure,
                  p_calling             => l_sec,
                  p_ora_error_msg       => SQLERRM,
                  p_error_desc          =>    'Error Occured for '
                                           || rec_validate.record_id,
                  p_distribution_list   => g_distro_list,
                  p_module              => g_module);
         END;

         IF ln_rec_count >= 1000
         THEN
            COMMIT;
            ln_rec_count := 0;
         END IF;
      END LOOP;

      -- Updating records.
      l_sec := 'Updating child/site records for accounts in error status';

      FOR rec_err_recs IN cur_err_recs
      LOOP
         UPDATE XXWC.XXWC_AR_CONV_CUST_ACCT_TBL
            SET PROCESS_FLAG = 'E',
                Process_message =
                   'Account/Primary Bill TO site record in error status (Validation Level)'
          WHERE     RECORD_ID = rec_err_recs.RECORD_ID
                AND CUSTOMER_SITE_NUMBER IS NOT NULL;

         ln_count := ln_count + SQL%ROWCOUNT;

         IF ln_count > 1000
         THEN
            ln_count := 0;
            COMMIT;
         END IF;
      END LOOP;

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         fnd_file.put_line (fnd_file.LOG,
                            'l_sec:' || l_sec || ' ' || SQLERRM);
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || lvc_procedure,
            p_calling             => l_sec,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          => NULL,
            p_distribution_list   => g_distro_list,
            p_module              => g_module);
   END;

   PROCEDURE CREATE_CUSTOMER (P_CUSTOMER_NUMBER   IN     VARCHAR2,
                              p_cust_account_id      OUT NUMBER,
                              p_party_id             OUT NUMBER)
   IS
      -- ==============================================================================================================================
      -- Procedure: CREATE_CUSTOMER
      -- Purpose: To Create customer
      -- ==============================================================================================================================
      --  REVISIONS:
      --   Ver        Date        Author               Description
      --  ---------  ---------- ---------------  --------------------------------------------------------------------------------------
      --  1.0        06/06/2018  P.Vamshidhar     TMS#20180308-00290 -AH HARRIS Customer Conversion
      -- ==============================================================================================================================

      CURSOR CUR_MAIN
      IS
         SELECT xaca.RECORD_ID,
                xaca.CUSTOMER_NUMBER,
                xaca.BUSINESS_NAME,
                xaca.BUSINESS_PHONE,
                xaca.BUSINESS_FAX,
                xaca.EMAIL_ADDRESS,
                xaca.OUTSIDE_SALES_REP_ID,
                xaca.credit_manager,
                DECODE (xaca.CREDIT_HOLD, 'YES', 'Y', 'N') CREDIT_HOLD,
                NVL (xaca.PREDOMINANT_TRADE, 'UNKNOWN') PREDOMINANT_TRADE,
                SUBSTR (xaca.STATUS, 1, 1) STATUS,
                xaca.CREDIT_LIMIT,
                xaca.DEFAULT_JOB_CREDIT_LIMIT,
                xaca.AP_PHONE,
                xaca.AP_EMAIL,
                xaca.terms,
                xaca.INVOICE_EMAIL_ADDRESS,
                xaca.DUNSNUMBER,
                xaca.CREATION_DATE,
                SUBSTR (xaca.SEND_STATEMENT, 1, 1) SEND_STATEMENT,
                INITCAP (xaca.MANDATORY_NOTES) MANDATORY_NOTES,
                xaca.MANDATORY_NOTES1,
                xaca.MANDATORY_NOTES2,
                xaca.MANDATORY_NOTES3,
                xaca.MANDATORY_NOTES4,
                xaca.MANDATORY_NOTES5,
                NVL ( (SELECT DECODE (a.AUTO_APPLY, 'OFF', 'N', 'Y')
                         FROM XXWC.XXWC_AHH_AR_CUST_AUTO_APPLY_T a
                        WHERE a.CUST_NUM = xaca.CUSTOMER_NUMBER),
                     'Y')
                   AUTO_APPLY,
                (SELECT REPLACE (REPLACE (A.site_class, CHR (13), ''),
                                 CHR (10),
                                 '')
                   FROM XXWC.XXWC_CUST_SITE_CLASSIF_TBL a
                  WHERE     a.CUSTOMER_NUMBER = xaca.customer_number
                        AND UPPER (a.CUST_SITE_NUM) =
                               UPPER (xaca.CUSTOMER_SITE_NUMBER)
                        AND ROWNUM = 1)
                   yard_job_accnt_project,
                ROWID ROW_ID
           FROM XXWC.XXWC_AR_CONV_CUST_ACCT_TBL xaca
          WHERE     xaca.CUSTOMER_NUMBER = P_CUSTOMER_NUMBER
                AND xaca.PROCESS_FLAG = 'V'
                AND xaca.CUSTOMER_SITE_NUMBER IS NULL;

      porganizationrec          hz_party_v2pub.organization_rec_type;
      pcontactpointrec          hz_contact_point_v2pub.contact_point_rec_type;
      pphonerec                 hz_contact_point_v2pub.phone_rec_type;
      pemailrec                 hz_contact_point_v2pub.email_rec_type;
      pcustproamtrec            hz_customer_profile_v2pub.cust_profile_amt_rec_type;
      pcustaccountrec           hz_cust_account_v2pub.cust_account_rec_type;
      pprofilerec               hz_customer_profile_v2pub.customer_profile_rec_type;
      o_party_id                HZ_PARTIES.PARTY_ID%TYPE;
      o_party_number            HZ_PARTIES.PARTY_NUMBER%TYPE;
      o_profile_id              HZ_CUSTOMER_PROFILES.CUST_ACCOUNT_PROFILE_ID%TYPE;
      ol_profile_id             HZ_CUSTOMER_PROFILES.CUST_ACCOUNT_PROFILE_ID%TYPE;
      o_cust_account_no         HZ_CUST_ACCOUNTS.ACCOUNT_NUMBER%TYPE;
      o_custacctprofileamtid    NUMBER;
      o_contact_pointid         NUMBER;
      p_object_version_number   NUMBER;
      o_cust_account_id         HZ_CUST_ACCOUNTS.CUST_ACCOUNT_ID%TYPE;
      o_msg_data                VARCHAR2 (32767);
      o_return_msg              VARCHAR2 (32767);
      o_ret_status              VARCHAR2 (1);
      x_msg_data                VARCHAR2 (32767);
      x_ret_status              VARCHAR2 (1);
      o_msg_count               NUMBER;
      lvc_sec                   VARCHAR2 (4000);
      ln_salesrep_id            RA_SALESREPS_ALL.SALESREP_ID%TYPE;
      l_res_category            jtf_rs_resource_extns_tl.category%TYPE;
      lvc_procedure             VARCHAR2 (20) := 'CREATE_CUSTOMER';
      ln_profile_class_id       NUMBER;
      ln_collector_id           NUMBER;
      ln_credit_analyst         NUMBER;
      ln_contact_party_id       NUMBER;
   BEGIN
      lvc_sec := 'Procedure Started';

      SAVEPOINT customer_record;
      g_cust_acct_no := NULL;

      FOR rec_main IN cur_main
      LOOP
         -- Party Creation
         lvc_sec := 'Party Creation';
         porganizationrec := NULL;
         porganizationrec.organization_name := rec_main.BUSINESS_NAME;
         porganizationrec.created_by_module := g_api_name;

         IF rec_main.dunsnumber IS NOT NULL
         THEN
            porganizationrec.duns_number_c := rec_main.dunsnumber;
         END IF;

         hz_party_v2pub.create_organization (
            p_init_msg_list      => 'T',
            p_organization_rec   => porganizationrec,
            x_return_status      => o_ret_status,
            x_msg_count          => o_msg_count,
            x_msg_data           => o_msg_data,
            x_party_id           => o_party_id,
            x_party_number       => o_party_number,
            x_profile_id         => o_profile_id);

         IF o_ret_status <> 'S'
         THEN
            return_msg (o_msg_count, o_msg_data, o_return_msg);
            x_msg_data := 'Error Party Creation: ' || o_return_msg;
            debug (p_customer_number, x_msg_data);
            x_ret_status := o_ret_status;
            ROLLBACK TO customer_record;
            GOTO STATUS_UPDATE;
         END IF;

         lvc_sec :=
            'Party Creation Process Completed - API Status ' || o_ret_status;

         debug (p_customer_number, lvc_sec);

         lvc_sec := 'Business Phone Creation';

         IF rec_main.business_phone IS NOT NULL
         THEN
            o_ret_status := NULL;
            o_msg_count := NULL;
            o_msg_data := NULL;
            pcontactpointrec := NULL;
            pcontactpointrec.contact_point_type := 'PHONE';
            pcontactpointrec.owner_table_name := 'HZ_PARTIES';
            pcontactpointrec.owner_table_id := o_party_id;
            pcontactpointrec.primary_flag := 'Y';
            pcontactpointrec.contact_point_purpose := 'BUSINESS';
            pphonerec.phone_country_code := NULL;
            pphonerec.phone_area_code :=
               SUBSTR (rec_main.business_phone, 1, 3);
            pphonerec.phone_number := SUBSTR (rec_main.business_phone, 4);
            pphonerec.phone_extension := NULL;
            pphonerec.phone_line_type := 'GEN';
            pcontactpointrec.created_by_module := g_api_name;

            hz_contact_point_v2pub.create_contact_point (
               p_init_msg_list       => 'T',
               p_contact_point_rec   => pcontactpointrec,
               p_phone_rec           => pphonerec,
               x_contact_point_id    => o_contact_pointid,
               x_return_status       => o_ret_status,
               x_msg_count           => o_msg_count,
               x_msg_data            => o_msg_data);

            IF o_ret_status <> 'S'
            THEN
               return_msg (o_msg_count, o_msg_data, o_return_msg);
               x_msg_data := 'Error Phone Creation: ' || o_return_msg;
               debug (p_customer_number, x_msg_data);
               x_ret_status := o_ret_status;
               ROLLBACK TO customer_record;
               GOTO STATUS_UPDATE;
            END IF;

            lvc_sec :=
                  'Business Phone Creation Completed - API Status '
               || o_ret_status;
            debug (p_customer_number, lvc_sec);
         END IF;

         lvc_sec := 'Business Fax Creation';

         IF rec_main.business_fax IS NOT NULL
         THEN
            o_ret_status := NULL;
            o_msg_count := NULL;
            o_msg_data := NULL;
            pcontactpointrec := NULL;
            pcontactpointrec.contact_point_type := 'PHONE';
            pcontactpointrec.owner_table_name := 'HZ_PARTIES';
            pcontactpointrec.owner_table_id := o_party_id;
            pcontactpointrec.primary_flag := 'N';
            pcontactpointrec.contact_point_purpose := 'BUSINESS';
            pphonerec.phone_area_code := SUBSTR (rec_main.business_fax, 1, 3);
            pphonerec.phone_number := SUBSTR (rec_main.business_fax, 4);
            pphonerec.phone_extension := NULL;
            pphonerec.phone_line_type := 'FAX';
            pcontactpointrec.created_by_module := g_api_name;

            hz_contact_point_v2pub.create_contact_point (
               p_init_msg_list       => 'T',
               p_contact_point_rec   => pcontactpointrec,
               p_phone_rec           => pphonerec,
               x_contact_point_id    => o_contact_pointid,
               x_return_status       => o_ret_status,
               x_msg_count           => o_msg_count,
               x_msg_data            => o_msg_data);

            IF o_ret_status <> 'S'
            THEN
               return_msg (o_msg_count, o_msg_data, o_return_msg);
               x_msg_data := 'Error: Fax Creation: ' || o_return_msg;
               debug (p_customer_number, x_msg_data);
               x_ret_status := o_ret_status;
               ROLLBACK TO customer_record;
               GOTO STATUS_UPDATE;
            END IF;

            lvc_sec :=
                  'Business Fax Creation Completed - API Status '
               || o_ret_status;
            debug (p_customer_number, lvc_sec);
         END IF;

         lvc_sec := 'Party Email Creation';

         IF rec_main.email_address IS NOT NULL
         THEN
            o_ret_status := NULL;
            o_msg_count := NULL;
            o_msg_data := NULL;
            pcontactpointrec := NULL;
            pemailrec.email_address := rec_main.email_address;
            pemailrec.email_format := 'MAILHTML';
            pcontactpointrec.contact_point_type := 'EMAIL';
            pcontactpointrec.owner_table_name := 'HZ_PARTIES';
            pcontactpointrec.owner_table_id := o_party_id;
            pcontactpointrec.primary_flag := 'Y';
            pcontactpointrec.contact_point_purpose := 'BUSINESS';
            pcontactpointrec.created_by_module := g_api_name;

            hz_contact_point_v2pub.create_contact_point (
               p_init_msg_list       => 'T',
               p_contact_point_rec   => pcontactpointrec,
               p_email_rec           => pemailrec,
               x_contact_point_id    => o_contact_pointid,
               x_return_status       => o_ret_status,
               x_msg_count           => o_msg_count,
               x_msg_data            => o_msg_data);

            IF o_ret_status <> 'S'
            THEN
               return_msg (o_msg_count, o_msg_data, o_return_msg);
               x_msg_data := 'Error: Email Creation: ' || o_return_msg;
               debug (p_customer_number, x_msg_data);
               x_ret_status := o_ret_status;
               ROLLBACK TO customer_record;
               GOTO STATUS_UPDATE;
            END IF;

            lvc_sec :=
               'Email Creation Completed - API Status ' || o_ret_status;
            debug (p_customer_number, lvc_sec);
         END IF;

         lvc_sec := 'Invoice Email Creation';

         IF rec_main.invoice_email_address IS NOT NULL
         THEN
            o_ret_status := NULL;
            o_msg_count := NULL;
            o_msg_data := NULL;
            pcontactpointrec := NULL;
            pemailrec.email_address := rec_main.invoice_email_address;
            pemailrec.email_format := 'MAILHTML';
            pcontactpointrec.contact_point_type := 'EMAIL';
            pcontactpointrec.owner_table_name := 'HZ_PARTIES';
            pcontactpointrec.owner_table_id := o_party_id;
            pcontactpointrec.contact_point_purpose := 'INVOICE';
            pcontactpointrec.created_by_module := g_api_name;

            hz_contact_point_v2pub.create_contact_point (
               p_init_msg_list       => 'T',
               p_contact_point_rec   => pcontactpointrec,
               p_email_rec           => pemailrec,
               x_contact_point_id    => o_contact_pointid,
               x_return_status       => o_ret_status,
               x_msg_count           => o_msg_count,
               x_msg_data            => o_msg_data);

            IF o_ret_status <> 'S'
            THEN
               return_msg (o_msg_count, o_msg_data, o_return_msg);
               x_msg_data := 'Error: Invoice Email Creation: ' || o_return_msg;
               debug (p_customer_number, x_msg_data);
               x_ret_status := o_ret_status;
               ROLLBACK TO customer_record;
               GOTO STATUS_UPDATE;
            END IF;

            lvc_sec :=
                  'Invoice Email Creation Completed - API Status '
               || o_ret_status;
            debug (p_customer_number, lvc_sec);
         END IF;

         lvc_sec := 'Customer Account Creation';

         BEGIN
            ln_profile_class_id := NULL;

            SELECT B.profile_class_id
              INTO ln_profile_class_id
              FROM XXWC.XXWC_AHH_PROFILE_CLASSES_T A,
                   HZ_CUST_PROFILE_CLASSES B
             WHERE     A.CUSTOMER_NUM = rec_main.customer_number
                   AND UPPER (TRIM (a.profile_class_name)) = UPPER (b.name)
                   AND ROWNUM = 1;
         EXCEPTION
            WHEN OTHERS
            THEN
               BEGIN
                  SELECT PROFILE_CLASS_ID
                    INTO ln_profile_class_id
                    FROM HZ_CUST_PROFILE_CLASSES
                   WHERE NAME = 'Contractor - Non Key';
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     ln_profile_class_id := NULL;
               END;
         END;

         DEBUG (p_customer_number,
                'ln_profile_class_id:' || ln_profile_class_id);

         BEGIN
            -- Deriving Sales Rep
            BEGIN
               SELECT A.SALESREP_ID
                 INTO ln_salesrep_id
                 FROM RA_SALESREPS_ALL A, XXWC.XXWC_AHH_AM_T B
                WHERE     A.ORG_ID = 162
                      AND UPPER (TRIM (B.SLSREPOUT)) =
                             UPPER (TRIM (rec_main.OUTSIDE_SALES_REP_ID))
                      AND A.SALESREP_NUMBER = B.SALESREP_ID
                      AND ROWNUM = 1;
            EXCEPTION
               WHEN OTHERS
               THEN
                  ln_salesrep_id := -3;
            END;

            DEBUG (p_customer_number, 'ln_salesrep_id:' || ln_salesrep_id);

            -- Deriving Collector
            BEGIN
               SELECT ORA_COLL_ID
                 INTO ln_collector_id
                 FROM XXWC.XXWC_AHH_CRD_COLL_ANALYST_T b
                WHERE     UPPER (TRIM (b.ahh_coll_code)) =
                             UPPER (rec_main.credit_manager)
                      AND ROWNUM = 1;
            EXCEPTION
               WHEN OTHERS
               THEN
                  BEGIN
                     SELECT ORA_COLL_ID
                       INTO ln_collector_id
                       FROM XXWC.XXWC_AHH_CRD_COLL_ANALYST_T b
                      WHERE     UPPER (TRIM (b.ahh_coll_code)) = 'DEFAULT'
                            AND ROWNUM = 1;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        ln_collector_id := 1;             -- Default Collector
                  END;
            END;

            DEBUG (p_customer_number, 'ln_collector_id:' || ln_collector_id);

            BEGIN
               SELECT ORA_CRD_ANLST_ID
                 INTO ln_credit_analyst
                 FROM XXWC.XXWC_AHH_CRD_COLL_ANALYST_T b
                WHERE     UPPER (TRIM (b.ahh_coll_code)) =
                             UPPER (rec_main.credit_manager)
                      AND ROWNUM = 1;
            EXCEPTION
               WHEN OTHERS
               THEN
                  BEGIN
                     SELECT ORA_CRD_ANLST_ID
                       INTO ln_credit_analyst
                       FROM XXWC.XXWC_AHH_CRD_COLL_ANALYST_T b
                      WHERE     UPPER (TRIM (b.ahh_coll_code)) = 'DEFAULT'
                            AND ROWNUM = 1;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        ln_credit_analyst := NULL;
                  END;
            END;

            DEBUG (p_customer_number,
                   'ln_credit_analyst:' || ln_credit_analyst);

            o_ret_status := NULL;
            o_msg_count := NULL;
            o_msg_data := NULL;
            porganizationrec := NULL;
            pcustaccountrec := NULL;
            pprofilerec := NULL;

            SELECT apps.hz_cust_accounts_s.NEXTVAL
              INTO pcustaccountrec.cust_account_id
              FROM DUAL;

            porganizationrec.party_rec.party_id := o_party_id;
            pcustaccountrec.account_name := rec_main.business_name;
            pcustaccountrec.status := rec_main.status;
            pcustaccountrec.tax_code := NULL;
            pcustaccountrec.attribute_category := 'No';
            pcustaccountrec.account_established_date :=
               TO_DATE (rec_main.CREATION_DATE, 'DD-MON-RR');
            pcustaccountrec.created_by_module := g_api_name;
            pcustaccountrec.customer_type := 'R';
            pcustaccountrec.attribute4 := 'AHH';
            pcustaccountrec.attribute7 := 'N';
            pcustaccountrec.attribute11 := rec_main.auto_apply;
            pcustaccountrec.customer_class_code := 'UNKNOWN';
            pcustaccountrec.orig_system_reference :=
               pcustaccountrec.cust_account_id;

            debug (p_customer_number,
                   'Cust_account_id:' || pcustaccountrec.cust_account_id);

            BEGIN
               SELECT A.TERM_ID
                 INTO pprofilerec.standard_terms
                 FROM RA_TERMS A, XXWC.XXWC_AHH_PAYMENT_TERMS_T B
                WHERE     UPPER (TRIM (B.AHH_TERM_NAME)) =
                             UPPER (TRIM (rec_main.terms))
                      AND UPPER (TRIM (B.TERM_NAME)) = UPPER (A.NAME)
                      AND ROWNUM = 1;
            EXCEPTION
               WHEN OTHERS
               THEN
                  BEGIN
                     SELECT TERM_ID
                       INTO pprofilerec.standard_terms
                       FROM RA_TERMS
                      WHERE name = 'N30D';
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        pprofilerec.standard_terms := NULL;
                  END;
            END;

            pcustaccountrec.attribute_category := rec_main.mandatory_notes;
            pcustaccountrec.attribute17 :=
               SUBSTR (rec_main.mandatory_notes1, 1, 150);
            pcustaccountrec.attribute18 :=
               SUBSTR (rec_main.mandatory_notes2, 1, 150);
            pcustaccountrec.attribute19 :=
               SUBSTR (rec_main.mandatory_notes3, 1, 150);
            pcustaccountrec.attribute20 :=
               SUBSTR (rec_main.mandatory_notes4, 1, 150);
            pcustaccountrec.attribute16 :=
               SUBSTR (rec_main.mandatory_notes5, 1, 150);
            pcustaccountrec.status := rec_main.status;
            pcustaccountrec.attribute9 := rec_main.predominant_trade;
            pprofilerec.profile_class_id := ln_profile_class_id;
            pprofilerec.credit_rating := 'Y';
            pprofilerec.credit_hold := rec_main.credit_hold;

            IF rec_main.send_statement = 'Y'
            THEN
               pprofilerec.send_statements := rec_main.send_statement;
            ELSE
               pprofilerec.send_statements := rec_main.send_statement;
               pprofilerec.statement_cycle_id := NULL;
               pprofilerec.credit_balance_statements := 'N';
            END IF;



            pprofilerec.credit_checking := 'Y';

            IF ln_collector_id IS NOT NULL
            THEN
               pprofilerec.collector_id := ln_collector_id;
            END IF;

            IF ln_credit_analyst IS NOT NULL
            THEN
               pprofilerec.credit_analyst_id := ln_credit_analyst;
            END IF;

            pprofilerec.account_status := g_acct_status;
            pprofilerec.attribute2 := 2;
            pprofilerec.attribute3 := 'Y';
            pprofilerec.tolerance := 0;
            pprofilerec.credit_classification := 'MODERATE';

            BEGIN
               SELECT jrre.category
                 INTO l_res_category
                 FROM apps.jtf_rs_salesreps_mo_v jrs,
                      jtf_rs_resource_extns_tl jrre
                WHERE     1 = 1
                      AND jrs.resource_id = jrre.resource_id
                      AND jrs.resource_id = ln_salesrep_id
                      AND jrs.STATUS = 'A'
                      AND ROWNUM = 1;

               IF     l_res_category = 'OTHER'
                  AND ln_salesrep_id NOT IN (100000617, 100000650)
               THEN
                  pcustaccountrec.sales_channel_code := 'BQU_UNKNOWN';
               ELSIF     l_res_category = 'OTHER'
                     AND ln_salesrep_id IN (100000617, 100000650)
               THEN
                  pcustaccountrec.sales_channel_code := 'ACCT_MGR';
               ELSIF l_res_category = 'EMPLOYEE'
               THEN
                  pcustaccountrec.sales_channel_code := 'ACCT_MGR';
               ELSE
                  pcustaccountrec.sales_channel_code := NULL;
               END IF;
            EXCEPTION
               WHEN OTHERS
               THEN
                  pcustaccountrec.sales_channel_code := NULL;
            END;

            hz_cust_account_v2pub.create_cust_account (
               p_init_msg_list          => 'T',
               p_cust_account_rec       => pcustaccountrec,
               p_organization_rec       => porganizationrec,
               p_customer_profile_rec   => pprofilerec,
               p_create_profile_amt     => fnd_api.g_true,
               x_cust_account_id        => o_cust_account_id,
               x_account_number         => o_cust_account_no,
               x_party_id               => o_party_id,
               x_party_number           => o_party_number,
               x_profile_id             => o_profile_id,
               x_return_status          => o_ret_status,
               x_msg_count              => o_msg_count,
               x_msg_data               => o_msg_data);


            IF o_ret_status <> 'S'
            THEN
               return_msg (o_msg_count, o_msg_data, o_return_msg);
               x_msg_data := 'Error: Account Creation: ' || o_return_msg;
               debug (p_customer_number, x_msg_data);
               x_ret_status := o_ret_status;
               ROLLBACK TO customer_record;
               GOTO STATUS_UPDATE;
            END IF;

            g_cust_acct_no := o_cust_account_no;
            lvc_sec :=
                  'Customer Account Creation Process Completed - API Status '
               || o_ret_status;
            debug (p_customer_number, lvc_sec);
         END;


         SAVEPOINT customer_record;

         BEGIN
            SELECT CUST_ACCOUNT_PROFILE_ID
              INTO ol_profile_id
              FROM APPS.HZ_CUSTOMER_PROFILES
             WHERE     CUST_ACCOUNT_ID = o_cust_account_id
                   AND SITE_USE_ID IS NULL;
         EXCEPTION
            WHEN OTHERS
            THEN
               ol_profile_id := NULL;
         END;

         IF o_profile_id IS NULL
         THEN
            o_profile_id := ol_profile_id;
         END IF;

         lvc_sec := 'Profile Amounts Update';

         pcustproamtrec := NULL;

         IF ol_profile_id IS NOT NULL
         THEN
            BEGIN
               SELECT cust_acct_profile_amt_id, object_version_number
                 INTO o_custacctprofileamtid, p_object_version_number
                 FROM apps.hz_cust_profile_amts
                WHERE     cust_account_profile_id = ol_profile_id
                      AND currency_code = g_currency_code;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  o_custacctprofileamtid := NULL;
                  p_object_version_number := NULL;
            END;

            IF rec_main.credit_limit = 0
            THEN
               pcustproamtrec.overall_credit_limit := 9999999.99;
            ELSIF rec_main.credit_limit = NULL
            THEN
               pcustproamtrec.overall_credit_limit := NULL;
            ELSE
               pcustproamtrec.overall_credit_limit := rec_main.credit_limit;
            END IF;

            IF o_custacctprofileamtid IS NOT NULL
            THEN
               pcustproamtrec.cust_acct_profile_amt_id :=
                  o_custacctprofileamtid;
               hz_customer_profile_v2pub.update_cust_profile_amt (
                  p_init_msg_list           => 'T',
                  p_cust_profile_amt_rec    => pcustproamtrec,
                  p_object_version_number   => p_object_version_number,
                  x_return_status           => o_ret_status,
                  x_msg_count               => o_msg_count,
                  x_msg_data                => o_msg_data);
            ELSE
               pcustproamtrec.cust_account_profile_id := ol_profile_id;
               pcustproamtrec.cust_account_id := o_cust_account_id;
               pcustproamtrec.currency_code := g_currency_code;
               pcustproamtrec.created_by_module := g_api_name;
               hz_customer_profile_v2pub.create_cust_profile_amt (
                  p_init_msg_list              => 'T',
                  p_check_foreign_key          => 'T',
                  p_cust_profile_amt_rec       => pcustproamtrec,
                  x_cust_acct_profile_amt_id   => o_custacctprofileamtid,
                  x_return_status              => o_ret_status,
                  x_msg_count                  => o_msg_count,
                  x_msg_data                   => o_msg_data);
            END IF;

            IF o_ret_status <> 'S'
            THEN
               return_msg (o_msg_count, o_msg_data, o_return_msg);
               x_msg_data :=
                  'Error: Account Level Profile amounts: ' || o_return_msg;
               debug (p_customer_number, x_msg_data);
               x_ret_status := o_ret_status;
               ROLLBACK TO customer_record;
               GOTO STATUS_UPDATE;
            END IF;
         END IF;


        <<STATUS_UPDATE>>
         IF NVL (x_ret_status, 'S') = 'S'
         THEN
            p_cust_account_id := o_cust_account_id;
            p_party_id := o_party_id;

            UPDATE XXWC.XXWC_AR_CONV_CUST_ACCT_TBL
               SET PROCESS_FLAG = 'Y',
                   ORACLE_CUSTOMER_NUMBER = o_cust_account_no
             WHERE ROWID = rec_main.row_id;
         ELSE
            p_cust_account_id := NULL;
            p_party_id := NULL;

            UPDATE XXWC.XXWC_AR_CONV_CUST_ACCT_TBL
               SET PROCESS_FLAG = 'E', PROCESS_MESSAGE = x_msg_data
             WHERE ROWID = rec_main.row_id;
         END IF;
      END LOOP;

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         p_cust_account_id := NULL;
         x_msg_data := SQLERRM;
         debug (p_customer_number, x_msg_data);

         UPDATE xxwc.XXWC_AR_CONV_CUST_ACCT_TBL
            SET process_flag = 'E', PROCESS_message = x_msg_data
          WHERE     record_id = p_customer_number
                AND customer_site_number IS NULL;

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || lvc_procedure,
            p_calling             => lvc_sec,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          =>    'Error Occured for '
                                     || p_customer_number
                                     || 'record id in XXWC_AR_CONV_CUST_ACCT_TBL.',
            p_distribution_list   => g_distro_list,
            p_module              => g_module);
   END;

   PROCEDURE primary_bill_to (p_customer_number   IN VARCHAR2,
                              p_cust_account_id   IN NUMBER,
                              p_party_id          IN NUMBER)
   IS
      -- ==============================================================================================================================
      -- Procedure: Primary_bill_to
      -- Purpose: To customer primary bill to
      -- ==============================================================================================================================
      --  REVISIONS:
      --   Ver        Date        Author               Description
      --  ---------  ---------- ---------------  --------------------------------------------------------------------------------------
      --  1.0        06/06/2018  P.Vamshidhar     TMS#20180308-00290 -AH HARRIS Customer Conversion
      -- ==============================================================================================================================
      CURSOR CUR_MAIN
      IS
         SELECT xaca.RECORD_ID,
                xaca.CUSTOMER_NUMBER,
                UPPER (xaca.CUSTOMER_SITE_NUMBER) CUSTOMER_SITE_NUMBER,
                xaca.BUSINESS_NAME,
                xaca.BUSINESS_PHONE,
                xaca.BUSINESS_FAX,
                xaca.EMAIL_ADDRESS,
                xaca.OUTSIDE_SALES_REP_ID,
                xaca.INSIDE_SALES_REP_ID,
                xaca.FSD,
                xaca.credit_manager,
                DECODE (xaca.CREDIT_HOLD, 'YES', 'Y', 'N') CREDIT_HOLD,
                NVL (xaca.PREDOMINANT_TRADE, 'UNKNOWN') PREDOMINANT_TRADE,
                --SUBSTR (xaca.STATUS, 1, 1) STATUS,
                'A' STATUS,
                xaca.CREDIT_LIMIT,
                xaca.DEFAULT_JOB_CREDIT_LIMIT,
                xaca.BILLING_ADDRESS1,
                xaca.BILLING_ADDRESS2,
                xaca.BILLING_CITY,
                xaca.BILLING_COUNTY,
                xaca.BILLING_STATE,
                NVL (xaca.BILLING_COUNTRY, 'US') BILLING_COUNTRY,
                xaca.BILLING_ZIP_CODE,
                xaca.AP_CONTACT_FIRST_NAME,
                xaca.AP_CONTACT_LAST_NAME,
                xaca.AP_PHONE,
                xaca.AP_EMAIL,
                xaca.terms,
                xaca.INVOICE_EMAIL_ADDRESS,
                xaca.DUNSNUMBER,
                SUBSTR (xaca.JOB_SITE_NAME, 1, 35) JOB_SITE_NAME,
                xaca.purchase_order_required,
                DECODE (SUBSTR (xaca.print_prices_on_order, 1, 1),
                        'Y', '1',
                        '2')
                   print_prices_on_order,
                (SELECT a.ORA_TAX_EXEMP_TYPE
                   FROM XXWC.XXWC_AHH_AR_TAX_EXEMPT_TYPES_T a
                  WHERE     a.cust_num = xaca.customer_number
                        AND UPPER (a.ship_to_site) =
                               UPPER (xaca.customer_site_number))
                   TAX_EXEMPTION,
                NVL ( (SELECT DECODE (a.AUTO_APPLY, 'OFF', 'N', 'Y')
                         FROM XXWC.XXWC_AHH_AR_CUST_AUTO_APPLY_T a
                        WHERE a.CUST_NUM = xaca.CUSTOMER_NUMBER),
                     'Y')
                   AUTO_APPLY,
                (SELECT REPLACE (REPLACE (A.SITE_CLASS, CHR (13), ''),
                                 CHR (10),
                                 '')
                   FROM XXWC.XXWC_CUST_SITE_CLASSIF_TBL a
                  WHERE     a.CUSTOMER_NUMBER = xaca.customer_number
                        AND UPPER (a.CUST_SITE_NUM) =
                               UPPER (xaca.CUSTOMER_SITE_NUMBER)
                        AND ROWNUM = 1)
                   yard_job_accnt_project,
                ROWID ROW_ID
           FROM XXWC.XXWC_AR_CONV_CUST_ACCT_TBL xaca
          WHERE     xaca.CUSTOMER_NUMBER = P_CUSTOMER_NUMBER
                AND xaca.PROCESS_FLAG = 'Y'
                AND xaca.CUSTOMER_SITE_NUMBER IS NULL;

      lvc_sec                          VARCHAR2 (4000);
      plocationrec                     hz_location_v2pub.location_rec_type;
      ppartysiterec                    hz_party_site_v2pub.party_site_rec_type;
      pcustacctsiterec                 hz_cust_account_site_v2pub.cust_acct_site_rec_type;
      pcustacctsiteuserec              hz_cust_account_site_v2pub.cust_site_use_rec_type;
      pcustomerprofile                 hz_customer_profile_v2pub.customer_profile_rec_type;
      pcustproamtrec                   hz_customer_profile_v2pub.cust_profile_amt_rec_type;
      pcontactpointrec                 hz_contact_point_v2pub.contact_point_rec_type;
      p_create_person_rec              hz_party_v2pub.person_rec_type;
      p_cr_cust_acc_role_rec           hz_cust_account_role_v2pub.cust_account_role_rec_type;
      p_org_contact_rec                hz_party_contact_v2pub.org_contact_rec_type;
      pemailrec                        hz_contact_point_v2pub.email_rec_type;
      pedirec                          hz_contact_point_v2pub.EDI_REC_TYPE;
      pphonerec                        hz_contact_point_v2pub.phone_rec_type;
      ptelexrec                        hz_contact_point_v2pub.TELEX_REC_TYPE;
      pwebrec                          hz_contact_point_v2pub.WEB_REC_TYPE;
      p_ROLE_RESPONSIBILITY_REC_TYPE   HZ_CUST_ACCOUNT_ROLE_V2PUB.role_responsibility_rec_type;
      o_location_id                    HZ_LOCATIONS.location_id%TYPE;
      o_contact_point_id               NUMBER;
      x_msg_count                      NUMBER;
      x_msg_data                       VARCHAR2 (32767);
      x_contact_party_id               NUMBER;
      x_contact_party_number           NUMBER;
      x_contact_profile_id             NUMBER;
      x_rel_party_number               NUMBER;
      x_party_rel_id                   NUMBER;
      x_rel_party_id                   NUMBER;
      x_org_contact_id                 NUMBER;
      x_ret_status                     VARCHAR2 (1);
      o_ret_status                     VARCHAR2 (1);
      o_msg_count                      NUMBER;
      x_cust_account_role_id           NUMBER;
      o_msg_data                       VARCHAR2 (32767);
      o_return_msg                     VARCHAR2 (32767);
      o_party_site_id                  hz_party_sites.party_site_id%TYPE;
      o_party_site_no                  hz_party_sites.party_site_number%TYPE;
      o_cust_acct_site_id              hz_cust_acct_sites_all.cust_acct_site_id%TYPE;
      o_cust_acct_site_use_id          hz_cust_site_uses_all.site_use_id%TYPE;
      o_sprofile_id                    NUMBER;
      p_object_version_number          NUMBER;
      o_cust_acct_profile_amt_id       NUMBER;
      lvc_procedure                    VARCHAR2 (20) := 'PRIMARY_BILL_TO';
      ln_profile_class_id              NUMBER;
      ln_collector_id                  NUMBER;
      ln_credit_analyst                NUMBER;
      ln_contact_party_id              NUMBER;
      o_resp_id                        NUMBER;
   BEGIN
      lvc_sec := 'Primary Bill Procedure Started';
      debug (p_customer_number, lvc_sec);

      lvc_sec := 'Location Creation';

      SAVEPOINT customer_record;

      FOR rec_main IN cur_main
      LOOP
         lvc_sec := 'Deriving country code';

         BEGIN
            BEGIN
               SELECT TERRITORY_CODE
                 INTO plocationrec.country
                 FROM HR_TERRITORIES_V
                WHERE (   UPPER (TERRITORY_SHORT_NAME) =
                             UPPER (rec_main.BILLING_COUNTRY)
                       OR UPPER (TERRITORY_CODE) =
                             UPPER (rec_main.BILLING_COUNTRY));
            EXCEPTION
               WHEN OTHERS
               THEN
                  x_msg_data := 'Invalid Billing Country';
                  debug (p_customer_number, x_msg_data);
                  x_ret_status := 'E';
                  GOTO STATUS_UPDATE;
            END;

            plocationrec.address1 := rec_main.BILLING_ADDRESS1;
            plocationrec.address2 := rec_main.BILLING_ADDRESS2;
            plocationrec.state := rec_main.BILLING_STATE;
            plocationrec.city := rec_main.BILLING_CITY;
            plocationrec.postal_code := rec_main.BILLING_ZIP_CODE;
            plocationrec.county := rec_main.BILLING_COUNTY;
            plocationrec.created_by_module := g_api_name;
            plocationrec.sales_tax_geocode := NULL;

            hz_location_v2pub.create_location (
               p_init_msg_list   => 'T',
               p_location_rec    => plocationrec,
               x_location_id     => o_location_id,
               x_return_status   => o_ret_status,
               x_msg_count       => o_msg_count,
               x_msg_data        => o_msg_data);

            IF o_ret_status <> 'S'
            THEN
               return_msg (o_msg_count, o_msg_data, o_return_msg);
               x_msg_data :=
                  'Error: Primary Bill to Location creation' || o_return_msg;
               debug (p_customer_number, x_msg_data);
               x_ret_status := o_ret_status;
               ROLLBACK TO customer_record;
               GOTO STATUS_UPDATE;
            END IF;

            lvc_sec :=
                  'Location Account for Bill To Site Creation Process Completed - API Status'
               || x_ret_status;

            debug (p_customer_number, lvc_sec);

            lvc_sec := 'Party Site Creation';

            x_ret_status := NULL;
            o_msg_count := NULL;
            o_msg_data := NULL;
            o_party_site_id := NULL;
            o_party_site_no := NULL;
            o_sprofile_id := NULL;
            ppartysiterec.created_by_module := g_api_name;
            ppartysiterec.party_id := p_party_id;
            ppartysiterec.status := rec_main.status;
            ppartysiterec.location_id := o_location_id;
            ppartysiterec.identifying_address_flag := 'Y';

            hz_party_site_v2pub.create_party_site (
               p_init_msg_list       => 'T',
               p_party_site_rec      => ppartysiterec,
               x_party_site_id       => o_party_site_id,
               x_party_site_number   => o_party_site_no,
               x_return_status       => x_ret_status,
               x_msg_count           => o_msg_count,
               x_msg_data            => o_msg_data);

            IF x_ret_status <> 'S'
            THEN
               return_msg (o_msg_count, o_msg_data, o_return_msg);
               x_msg_data :=
                  'Error:Primary Bill Party Site Creation: ' || o_return_msg;
               debug (p_customer_number, x_msg_data);
               ROLLBACK TO customer_record;
               GOTO STATUS_UPDATE;
            END IF;

            lvc_sec :=
                  'Party Site Creation Process Completed - API Status '
               || x_ret_status;

            debug (p_customer_number, lvc_sec);

            BEGIN
               ln_profile_class_id := NULL;

               SELECT B.profile_class_id
                 INTO ln_profile_class_id
                 FROM XXWC.XXWC_AHH_PROFILE_CLASSES_T A,
                      HZ_CUST_PROFILE_CLASSES B
                WHERE     A.CUSTOMER_NUM = rec_main.customer_number
                      AND UPPER (TRIM (a.profile_class_name)) =
                             UPPER (b.name)
                      AND ROWNUM = 1;
            EXCEPTION
               WHEN OTHERS
               THEN
                  BEGIN
                     SELECT PROFILE_CLASS_ID
                       INTO ln_profile_class_id
                       FROM HZ_CUST_PROFILE_CLASSES
                      WHERE NAME = 'Contractor - Non Key';
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        ln_profile_class_id := NULL;
                  END;
            END;

            lvc_sec := 'Primary Bill to Account Site Creation';

            pcustacctsiterec := NULL;
            x_ret_status := NULL;
            o_msg_count := NULL;
            o_msg_data := NULL;
            pcustacctsiterec.cust_account_id := p_cust_account_id;
            pcustacctsiterec.party_site_id := o_party_site_id;
            pcustacctsiterec.created_by_module := g_api_name;
            pcustacctsiterec.status := rec_main.status;
            pcustacctsiterec.org_id := gn_org_id;
            pcustacctsiterec.attribute3 :=
               SUBSTR (NVL (rec_main.purchase_order_required, 'N'), 1, 1);
            pcustacctsiterec.attribute_category := 'No';
            pcustacctsiterec.attribute1 := rec_main.print_prices_on_order;
            pcustacctsiterec.attribute15 := rec_main.tax_exemption;
            --pcustacctsiterec.attribute15 := 'None';
            pcustacctsiterec.attribute14 := 'N';
            pcustacctsiterec.attribute17 := p_customer_number;



            BEGIN
               SELECT CASE
                         WHEN customer_class_code = 'COMMERCIAL'
                         THEN
                            'PUBLIC'
                         WHEN customer_class_code = 'GOVERNMENT'
                         THEN
                            'PRIVATE'
                         WHEN customer_class_code = 'NON-CONTRACTOR/OTHER'
                         THEN
                            'PUBLIC'
                         WHEN customer_class_code = 'RESELLER'
                         THEN
                            'PUBLIC'
                         WHEN customer_class_code = 'RESIDENTIAL_CONTRACTOR'
                         THEN
                            'RES'
                         WHEN customer_class_code = 'UNKNOWN'
                         THEN
                            NULL
                         ELSE
                            NULL
                      END
                 INTO pcustacctsiterec.customer_category_code
                 FROM hz_cust_accounts
                WHERE cust_account_id = p_cust_account_id;
            EXCEPTION
               WHEN OTHERS
               THEN
                  pcustacctsiterec.customer_category_code := NULL;
            END;

            hz_cust_account_site_v2pub.create_cust_acct_site (
               p_init_msg_list        => 'T',
               p_cust_acct_site_rec   => pcustacctsiterec,
               x_cust_acct_site_id    => o_cust_acct_site_id,
               x_return_status        => x_ret_status,
               x_msg_count            => o_msg_count,
               x_msg_data             => o_msg_data);

            IF x_ret_status <> 'S'
            THEN
               return_msg (o_msg_count, o_msg_data, o_return_msg);
               x_msg_data :=
                  'Error:Primary Bill to Customer Account: ' || o_return_msg;
               debug (p_customer_number, x_msg_data);
               ROLLBACK TO customer_record;
               GOTO STATUS_UPDATE;
            END IF;

            lvc_sec :=
                  'Primary Cust Acct Site Creation Process Completed - API Status '
               || x_ret_status;
            debug (p_customer_number, lvc_sec);

            lvc_sec := 'Primary Bill to Account Site Use Creation';

            pcustacctsiteuserec := NULL;
            x_ret_status := NULL;
            o_msg_count := NULL;
            o_msg_data := NULL;

            BEGIN
               SELECT A.TERM_ID
                 INTO pcustomerprofile.standard_terms
                 FROM RA_TERMS A, XXWC.XXWC_AHH_PAYMENT_TERMS_T B
                WHERE     UPPER (TRIM (B.AHH_TERM_NAME)) =
                             UPPER (TRIM (rec_main.terms))
                      AND UPPER (TRIM (B.TERM_NAME)) = UPPER (A.NAME)
                      AND ROWNUM = 1;
            EXCEPTION
               WHEN OTHERS
               THEN
                  BEGIN
                     SELECT TERM_ID
                       INTO pcustomerprofile.standard_terms
                       FROM RA_TERMS
                      WHERE name = 'N30D';
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        pcustomerprofile.standard_terms := NULL;
                  END;
            END;


            -- Deriving Collector
            BEGIN
               SELECT ORA_COLL_ID
                 INTO ln_collector_id
                 FROM XXWC.XXWC_AHH_CRD_COLL_ANALYST_T b
                WHERE     UPPER (TRIM (b.ahh_coll_code)) =
                             UPPER (rec_main.credit_manager)
                      AND ROWNUM = 1;
            EXCEPTION
               WHEN OTHERS
               THEN
                  BEGIN
                     SELECT ORA_COLL_ID
                       INTO ln_collector_id
                       FROM XXWC.XXWC_AHH_CRD_COLL_ANALYST_T b
                      WHERE     UPPER (TRIM (b.ahh_coll_code)) = 'DEFAULT'
                            AND ROWNUM = 1;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        ln_collector_id := 1;             -- Default Collector
                  END;
            END;

            DEBUG (p_customer_number, 'ln_collector_id:' || ln_collector_id);

            BEGIN
               SELECT ORA_CRD_ANLST_ID
                 INTO ln_credit_analyst
                 FROM XXWC.XXWC_AHH_CRD_COLL_ANALYST_T b
                WHERE     UPPER (TRIM (b.ahh_coll_code)) =
                             UPPER (rec_main.credit_manager)
                      AND ROWNUM = 1;
            EXCEPTION
               WHEN OTHERS
               THEN
                  BEGIN
                     SELECT ORA_CRD_ANLST_ID
                       INTO ln_credit_analyst
                       FROM XXWC.XXWC_AHH_CRD_COLL_ANALYST_T b
                      WHERE     UPPER (TRIM (b.ahh_coll_code)) = 'DEFAULT'
                            AND ROWNUM = 1;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        ln_credit_analyst := NULL;
                  END;
            END;

            DEBUG (p_customer_number,
                   'ln_credit_analyst:' || ln_credit_analyst);


            pcustacctsiteuserec.site_use_code := 'BILL_TO';
            pcustacctsiteuserec.cust_acct_site_id := o_cust_acct_site_id;
            pcustacctsiteuserec.attribute1 := 'MSTR';
            pcustacctsiteuserec.primary_flag := 'Y';
            pcustacctsiteuserec.location :=
               SUBSTR (rec_main.job_site_name, 1, 35);
            pcustacctsiteuserec.created_by_module := g_api_name;
            pcustomerprofile.credit_checking := 'Y';
            pcustomerprofile.attribute2 := 2;
            pcustomerprofile.attribute3 := 'Y';
            pcustomerprofile.collector_id := ln_collector_id;
            pcustomerprofile.account_status := g_acct_status;
            pcustomerprofile.credit_rating := 'Y';
            pcustomerprofile.tolerance := 0;
            pcustomerprofile.credit_analyst_id := ln_credit_analyst;
            pcustomerprofile.credit_classification := 'MODERATE';
            pcustomerprofile.profile_class_id := ln_profile_class_id;

            BEGIN
               SELECT A.SALESREP_ID
                 INTO pcustacctsiteuserec.primary_salesrep_id
                 FROM RA_SALESREPS_ALL A, XXWC.XXWC_AHH_AM_T B
                WHERE     A.ORG_ID = 162
                      AND UPPER (B.SLSREPOUT) =
                             UPPER (rec_main.OUTSIDE_SALES_REP_ID)
                      AND A.SALESREP_NUMBER = B.SALESREP_ID;
            EXCEPTION
               WHEN OTHERS
               THEN
                  pcustacctsiteuserec.primary_salesrep_id := -3;
            END;

--            IF rec_main.INSIDE_SALES_REP_ID IS NOT NULL
--            THEN
               BEGIN
                  SELECT A.SALESREP_ID
                    INTO pcustacctsiteuserec.attribute12
                    FROM RA_SALESREPS_ALL A, XXWC.XXWC_AHH_AM_T B
                   WHERE     A.ORG_ID = 162
                         AND UPPER (B.SLSREPOUT) =
                                UPPER (rec_main.INSIDE_SALES_REP_ID)
                         AND A.SALESREP_NUMBER = B.SALESREP_ID;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     pcustacctsiteuserec.attribute12 := -3;
               END;
--            END IF;

--            IF rec_main.FSD IS NOT NULL
--            THEN
               BEGIN
                  SELECT A.SALESREP_ID
                    INTO pcustacctsiteuserec.attribute13
                    FROM RA_SALESREPS_ALL A, XXWC.XXWC_AHH_AM_T B
                   WHERE     A.ORG_ID = 162
                         AND UPPER (B.SLSREPOUT) = UPPER (rec_main.FSD)
                         AND A.SALESREP_NUMBER = B.SALESREP_ID;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     pcustacctsiteuserec.attribute13 := -3;
               END;
--            END IF;

            hz_cust_account_site_v2pub.create_cust_site_use (
               p_init_msg_list          => 'T',
               p_cust_site_use_rec      => pcustacctsiteuserec,
               p_customer_profile_rec   => pcustomerprofile,
               p_create_profile         => 'T',
               p_create_profile_amt     => 'T',
               x_site_use_id            => o_cust_acct_site_use_id,
               x_return_status          => x_ret_status,
               x_msg_count              => o_msg_count,
               x_msg_data               => o_msg_data);

            IF x_ret_status <> 'S'
            THEN
               return_msg (o_msg_count, o_msg_data, o_return_msg);
               x_msg_data :=
                     'Error: Account Site Use Creation(Bill To): '
                  || o_return_msg;
               debug (p_customer_number, x_msg_data);
               ROLLBACK TO customer_record;
               GOTO STATUS_UPDATE;
            END IF;

            lvc_sec :=
                  'Cust Acct Site Use Creation Process Completed - API Status '
               || x_ret_status;

            debug (p_customer_number, lvc_sec);


            lvc_sec := 'Primary Bill to Site Profile Amounts update. ';


            pcustproamtrec := NULL;
            o_sprofile_id := NULL;
            x_ret_status := NULL;
            o_msg_count := NULL;
            o_msg_data := NULL;

            BEGIN
               SELECT cust_account_profile_id
                 INTO o_sprofile_id
                 FROM apps.hz_customer_profiles
                WHERE     cust_account_id = p_cust_account_id
                      AND site_use_id = o_cust_acct_site_use_id;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  o_sprofile_id := NULL;
            END;

            IF o_sprofile_id IS NOT NULL
            THEN
               BEGIN
                  SELECT cust_acct_profile_amt_id, object_version_number
                    INTO o_cust_acct_profile_amt_id, p_object_version_number
                    FROM apps.hz_cust_profile_amts
                   WHERE     cust_account_profile_id = o_sprofile_id
                         AND currency_code = g_currency_code;
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     o_cust_acct_profile_amt_id := NULL;
                     p_object_version_number := NULL;
               END;


               IF rec_main.credit_limit = 0
               THEN
                  pcustproamtrec.overall_credit_limit := 9999999.99;
               ELSIF rec_main.credit_limit = NULL
               THEN
                  pcustproamtrec.overall_credit_limit := NULL;
               ELSE
                  pcustproamtrec.overall_credit_limit := rec_main.credit_limit;
               END IF;


               IF o_cust_acct_profile_amt_id IS NOT NULL
               THEN
                  --Update amount profile
                  pcustproamtrec.cust_acct_profile_amt_id :=
                     o_cust_acct_profile_amt_id;
                  -- pcustproamtrec.created_by_module := l_api_name;  -- commented by Vamshi

                  hz_customer_profile_v2pub.update_cust_profile_amt (
                     p_init_msg_list           => 'T',
                     p_cust_profile_amt_rec    => pcustproamtrec,
                     p_object_version_number   => p_object_version_number,
                     x_return_status           => x_ret_status,
                     x_msg_count               => o_msg_count,
                     x_msg_data                => o_msg_data);
               ELSE
                  --Create Profile Amount
                  pcustproamtrec.cust_account_profile_id := o_sprofile_id;
                  pcustproamtrec.cust_account_id := p_cust_account_id;
                  pcustproamtrec.site_use_id := o_cust_acct_site_use_id;
                  pcustproamtrec.currency_code := g_currency_code;
                  pcustproamtrec.created_by_module := g_api_name;
                  hz_customer_profile_v2pub.create_cust_profile_amt (
                     p_init_msg_list              => 'T',
                     p_check_foreign_key          => 'T',
                     p_cust_profile_amt_rec       => pcustproamtrec,
                     x_cust_acct_profile_amt_id   => o_cust_acct_profile_amt_id,
                     x_return_status              => x_ret_status,
                     x_msg_count                  => o_msg_count,
                     x_msg_data                   => o_msg_data);
               END IF;

               IF x_ret_status <> 'S'
               THEN
                  return_msg (o_msg_count, o_msg_data, o_return_msg);
                  x_msg_data := 'Profile Amount Creation: ' || o_return_msg;
                  debug (p_customer_number, x_msg_data);
                  ROLLBACK TO customer_record;
                  GOTO STATUS_UPDATE;
               END IF;

               lvc_sec :=
                     'Profile Amount Creation Process Completed - API Status '
                  || x_ret_status;
               debug (p_customer_number, lvc_sec);

               lvc_sec := 'Primary Bill to Usage Rules - API Status ';

               hz_credit_usages_cascade_pkg.cascade_credit_usage_rules (
                  p_cust_acct_profile_amt_id   => o_cust_acct_profile_amt_id,
                  p_cust_profile_id            => o_sprofile_id,
                  p_profile_class_amt_id       => NULL,
                  p_profile_class_id           => NULL,
                  x_return_status              => x_ret_status,
                  x_msg_count                  => o_msg_count,
                  x_msg_data                   => o_msg_data);

               IF x_ret_status <> 'S'
               THEN
                  return_msg (o_msg_count, o_msg_data, o_return_msg);
                  x_msg_data :=
                        'Primary Bill to Profile Amounts Usage Rules: '
                     || o_return_msg;
                  debug (p_customer_number, x_msg_data);
                  ROLLBACK TO customer_record;
                  GOTO STATUS_UPDATE;
               END IF;

               lvc_sec :=
                     'Primary Bill to Profile Amt uage rules Completed - API Status '
                  || x_ret_status;

               debug (p_customer_number, lvc_sec);
            END IF;

            lvc_sec := 'Party Contact person name Creation';

            IF     rec_main.ap_contact_last_name IS NOT NULL
               AND rec_main.ap_contact_first_name IS NOT NULL
            THEN
               ln_contact_party_id := NULL;
               p_create_person_rec := NULL;
               x_contact_party_id := NULL;
               x_contact_party_number := NULL;
               x_contact_profile_id := NULL;
               p_create_person_rec.person_first_name :=
                  rec_main.ap_contact_first_name;
               p_create_person_rec.person_last_name :=
                  rec_main.ap_contact_last_name;
               p_create_person_rec.created_by_module := g_api_name;
               hz_party_v2pub.create_person ('T',
                                             p_create_person_rec,
                                             ln_contact_party_id,
                                             x_contact_party_number,
                                             x_contact_profile_id,
                                             o_ret_status,
                                             o_msg_count,
                                             o_msg_data);

               IF o_ret_status <> 'S'
               THEN
                  return_msg (o_msg_count, o_msg_data, o_return_msg);
                  x_msg_data :=
                        'Contact Person creation (first and last names): '
                     || o_return_msg;
                  debug (p_customer_number, x_msg_data);
                  x_ret_status := o_ret_status;
                  ROLLBACK TO customer_record;
                  GOTO STATUS_UPDATE;
               END IF;


               lvc_sec :=
                     'Conact Person (Name) Completed - API Status '
                  || o_ret_status;
               debug (p_customer_number, lvc_sec);


               p_org_contact_rec := NULL;
               p_org_contact_rec.created_by_module := g_api_name;
               p_org_contact_rec.party_rel_rec.subject_id :=
                  ln_contact_party_id;
               p_org_contact_rec.party_rel_rec.subject_type := 'PERSON';
               p_org_contact_rec.party_rel_rec.subject_table_name :=
                  'HZ_PARTIES';
               p_org_contact_rec.party_rel_rec.object_id := p_party_id;
               p_org_contact_rec.party_rel_rec.object_type := 'ORGANIZATION';
               p_org_contact_rec.party_rel_rec.object_table_name :=
                  'HZ_PARTIES';
               p_org_contact_rec.party_rel_rec.relationship_code :=
                  'CONTACT_OF';
               p_org_contact_rec.party_rel_rec.relationship_type := 'CONTACT';
               p_org_contact_rec.party_rel_rec.start_date := SYSDATE;
               hz_party_contact_v2pub.create_org_contact ('T',
                                                          p_org_contact_rec,
                                                          x_org_contact_id,
                                                          x_party_rel_id,
                                                          x_rel_party_id,
                                                          x_rel_party_number,
                                                          o_ret_status,
                                                          o_msg_count,
                                                          o_msg_data);

               IF o_ret_status <> 'S'
               THEN
                  return_msg (o_msg_count, o_msg_data, o_return_msg);
                  x_msg_data :=
                     'Creating Org Contact (Name): ' || o_return_msg;
                  debug (p_customer_number, x_msg_data);
                  x_ret_status := o_ret_status;
                  ROLLBACK TO customer_record;
                  GOTO STATUS_UPDATE;
               END IF;

               lvc_sec :=
                     'Conact Person linking to Party is Completed - API Status '
                  || o_ret_status;
               debug (p_customer_number, lvc_sec);

               IF rec_main.ap_email IS NOT NULL
               THEN
                  lvc_sec := 'Creating AP Email for Contact Person '; -- Initializing the Mandatory API parameters
                  pcontactpointrec.contact_point_type := 'EMAIL';
                  pcontactpointrec.owner_table_name := 'HZ_PARTIES';
                  pcontactpointrec.owner_table_id := x_rel_party_id;
                  pcontactpointrec.primary_flag := 'Y';
                  pcontactpointrec.contact_point_purpose := 'BUSINESS';
                  pcontactpointrec.created_by_module := g_api_name;
                  pemailrec.email_format := 'MAILHTML';
                  pemailrec.email_address := rec_main.ap_email;

                  HZ_CONTACT_POINT_V2PUB.CREATE_CONTACT_POINT (
                     p_init_msg_list       => FND_API.G_TRUE,
                     p_contact_point_rec   => pcontactpointrec,
                     p_edi_rec             => pedirec,
                     p_email_rec           => pemailrec,
                     p_phone_rec           => pphonerec,
                     p_telex_rec           => ptelexrec,
                     p_web_rec             => pwebrec,
                     x_contact_point_id    => o_contact_point_id,
                     x_return_status       => o_ret_status,
                     x_msg_count           => o_msg_count,
                     x_msg_data            => o_msg_data);

                  IF o_ret_status <> 'S'
                  THEN
                     return_msg (o_msg_count, o_msg_data, o_return_msg);
                     x_msg_data :=
                           'Creating Email Contact (for Party): '
                        || o_return_msg;
                     debug (p_customer_number, x_msg_data);
                     x_ret_status := o_ret_status;
                     ROLLBACK TO customer_record;
                     GOTO STATUS_UPDATE;
                  END IF;
               END IF;

               lvc_sec :=
                     'Creating AP Email Contact Email Process Completed - API Status '
                  || o_ret_status;


               debug (p_customer_number, lvc_sec);

               lvc_sec := 'Create Party Site for Contact person';

               o_ret_status := NULL;
               o_msg_count := NULL;
               o_msg_data := NULL;
               o_party_site_id := NULL;
               o_party_site_no := NULL;
               o_sprofile_id := NULL;
               ppartysiterec.created_by_module := g_api_name;

               -- create a party site now
               ppartysiterec.party_id := x_rel_party_id;
               ppartysiterec.location_id := o_location_id;
               ppartysiterec.identifying_address_flag := 'Y';

               hz_party_site_v2pub.create_party_site (
                  p_init_msg_list       => 'T',
                  p_party_site_rec      => ppartysiterec,
                  x_party_site_id       => o_party_site_id,
                  x_party_site_number   => o_party_site_no,
                  x_return_status       => o_ret_status,
                  x_msg_count           => o_msg_count,
                  x_msg_data            => o_msg_data);


               IF o_ret_status <> 'S'
               THEN
                  return_msg (o_msg_count, o_msg_data, o_return_msg);
                  x_msg_data := 'Creating contact location: ' || o_return_msg;
                  debug (p_customer_number, x_msg_data);
                  x_ret_status := o_ret_status;
                  ROLLBACK TO customer_record;
                  GOTO STATUS_UPDATE;
               END IF;

               lvc_sec :=
                     'Creating contact location Process Completed - API Status '
                  || o_ret_status;


               debug (p_customer_number, lvc_sec);

               o_ret_status := NULL;
               o_msg_count := NULL;
               o_msg_data := NULL;
               p_cr_cust_acc_role_rec := NULL;
               p_cr_cust_acc_role_rec.party_id := x_rel_party_id;
               p_cr_cust_acc_role_rec.cust_account_id := p_cust_account_id;
               p_cr_cust_acc_role_rec.primary_flag := 'N';
               p_cr_cust_acc_role_rec.role_type := 'CONTACT';
               p_cr_cust_acc_role_rec.created_by_module := g_api_name;
               hz_cust_account_role_v2pub.create_cust_account_role (
                  'T',
                  p_cr_cust_acc_role_rec,
                  x_cust_account_role_id,
                  o_ret_status,
                  o_msg_count,
                  o_msg_data);

               IF o_ret_status <> 'S'
               THEN
                  return_msg (o_msg_count, o_msg_data, o_return_msg);
                  x_msg_data := 'Creating Account role: ' || o_return_msg;
                  debug (p_customer_number, x_msg_data);
                  x_ret_status := o_ret_status;
                  ROLLBACK TO customer_record;
                  RETURN;
               END IF;

               lvc_sec :=
                     'Creating Account role Process Completed - API Status '
                  || o_ret_status;


               debug (p_customer_number, lvc_sec);


               o_ret_status := NULL;
               o_msg_count := NULL;
               o_msg_data := NULL;
               p_ROLE_RESPONSIBILITY_REC_TYPE := NULL;
               p_ROLE_RESPONSIBILITY_REC_TYPE.cust_account_role_id :=
                  x_cust_account_role_id;
               p_ROLE_RESPONSIBILITY_REC_TYPE.responsibility_type := 'ACC_PAY';

               p_ROLE_RESPONSIBILITY_REC_TYPE.created_by_module := g_api_name;

               hz_cust_account_role_v2pub.create_role_responsibility (
                  p_init_msg_list             => 'T',
                  p_role_responsibility_rec   => p_ROLE_RESPONSIBILITY_REC_TYPE,
                  x_responsibility_id         => o_resp_id,
                  x_return_status             => o_ret_status,
                  x_msg_count                 => o_msg_count,
                  x_msg_data                  => o_msg_data);

               IF o_ret_status <> 'S'
               THEN
                  return_msg (o_msg_count, o_msg_data, o_return_msg);
                  x_msg_data := 'Assing AP role: ' || o_return_msg;
                  debug (p_customer_number, x_msg_data);
                  x_ret_status := o_ret_status;
                  ROLLBACK TO customer_record;
                  GOTO STATUS_UPDATE;
               END IF;

               lvc_sec :=
                     'Assing AP role Process Completed - API Status '
                  || o_ret_status;

               debug (p_customer_number, lvc_sec);
            END IF;


           <<STATUS_UPDATE>>
            IF NVL (x_ret_status, 'S') = 'S'
            THEN
               UPDATE XXWC.XXWC_AR_CONV_CUST_ACCT_TBL
                  SET PROCESS_FLAG = 'Y',
                      ORACLE_CUSTOMER_NUMBER = g_cust_acct_no
                WHERE ROWID = rec_main.row_id;

               g_primbillto_exists := 'Y';
            ELSE
               g_primbillto_exists := 'N';

               UPDATE XXWC.XXWC_AR_CONV_CUST_ACCT_TBL
                  SET PROCESS_FLAG = 'E', PROCESS_MESSAGE = x_msg_data
                WHERE ROWID = rec_main.row_id;
            END IF;

            COMMIT;
         EXCEPTION
            WHEN OTHERS
            THEN
               x_msg_data := SQLERRM;
               debug (p_customer_number, x_msg_data);

               UPDATE xxwc.XXWC_AR_CONV_CUST_ACCT_TBL
                  SET process_flag = 'E',
                      PROCESS_message = PROCESS_message || ' ' || x_msg_data
                WHERE     record_id = p_customer_number
                      AND ROWID = rec_main.row_id;

               xxcus_error_pkg.xxcus_error_main_api (
                  p_called_from         => g_err_callfrom || '.' || lvc_procedure,
                  p_calling             => lvc_sec,
                  p_ora_error_msg       => SQLERRM,
                  p_error_desc          =>    'Error Occured for '
                                           || p_customer_number
                                           || 'record id in XXWC_AR_CONV_CUST_ACCT_TBL.',
                  p_distribution_list   => g_distro_list,
                  p_module              => g_module);
         END;
      END LOOP;
   END;

   PROCEDURE primary_ship_to (p_customer_number   IN VARCHAR2,
                              p_cust_account_id   IN NUMBER,
                              p_party_id          IN NUMBER)
   IS
      -- ==============================================================================================================================
      -- Procedure: Primary_ship_to
      -- Purpose: To customer primary ship to
      -- ==============================================================================================================================
      --  REVISIONS:
      --   Ver        Date        Author               Description
      --  ---------  ---------- ---------------  --------------------------------------------------------------------------------------
      --  1.0        06/06/2018  P.Vamshidhar     TMS#20180308-00290 -AH HARRIS Customer Conversion
      -- ==============================================================================================================================
      CURSOR cur_yard (
         i_record_id    VARCHAR2)
      IS
         SELECT ROWID ROW_ID
           FROM XXWC.XXWC_AR_CONV_CUST_ACCT_TBL xxac
          WHERE     customer_number = i_record_id
                AND process_flag = 'V'
                AND UPPER (xxac.CUSTOMER_SITE_NUMBER) = 'YARD';

      --         SELECT ROWID ROW_ID
      --           FROM XXWC.XXWC_AR_CONV_CUST_ACCT_TBL xxac
      --          WHERE     customer_number = i_record_id
      --                AND process_flag = 'V'
      --                AND EXISTS
      --                       (SELECT 1
      --                          FROM XXWC.XXWC_CUST_SITE_CLASSIF_TBL A
      --                         WHERE     A.CUSTOMER_NUMBER = xxac.customer_number
      --                               AND UPPER (A.CUST_SITE_NUM) =
      --                                      UPPER (xxac.CUSTOMER_SITE_NUMBER)
      --                               AND UPPER (
      --                                      REPLACE (
      --                                         REPLACE (site_class, CHR (13), ''),
      --                                         CHR (10),
      --                                         '')) = 'YARD')
      --                AND ROWNUM = 1;



      CURSOR cur_non_yard (
         i_record_id    VARCHAR2)
      IS
         SELECT ROWID ROW_ID
           FROM XXWC.XXWC_AR_CONV_CUST_ACCT_TBL xxac
          WHERE     customer_number = i_record_id
                AND process_flag = 'V'
                AND CUSTOMER_SITE_NUMBER IS NOT NULL
                AND ROWNUM = 1;

      CURSOR cur_cust (
         p_rowid    VARCHAR2)
      IS
         SELECT xaca.RECORD_ID,
                xaca.CUSTOMER_NUMBER,
                UPPER (xaca.CUSTOMER_SITE_NUMBER) CUSTOMER_SITE_NUMBER,
                xaca.BUSINESS_NAME,
                xaca.BUSINESS_PHONE,
                xaca.BUSINESS_FAX,
                xaca.EMAIL_ADDRESS,
                xaca.OUTSIDE_SALES_REP_ID,
                xaca.INSIDE_SALES_REP_ID,
                xaca.FSD,
                xaca.credit_manager,
                DECODE (xaca.CREDIT_HOLD, 'YES', 'Y', 'N') CREDIT_HOLD,
                NVL (xaca.PREDOMINANT_TRADE, 'UNKNOWN') PREDOMINANT_TRADE,
                SUBSTR (xaca.STATUS, 1, 1) STATUS,
                xaca.DEFAULT_JOB_CREDIT_LIMIT,
                NVL (xaca.SHIPPING_ADDRESS1, xaca.BILLING_ADDRESS1)
                   SHIPPING_ADDRESS1,
                DECODE (xaca.SHIPPING_ADDRESS1,
                        NULL, xaca.BILLING_ADDRESS2,
                        xaca.SHIPPING_ADDRESS2)
                   SHIPPING_ADDRESS2,
                NVL (xaca.SHIPPING_CITY, xaca.BILLING_CITY) SHIPPING_CITY,
                NVL (xaca.SHIPPING_COUNTY, xaca.BILLING_COUNTY)
                   SHIPPING_COUNTY,
                NVL (xaca.SHIPPING_STATE, xaca.BILLING_STATE) SHIPPING_STATE,
                NVL (xaca.SHIPPING_COUNTRY, NVL (xaca.BILLING_COUNTRY, 'US'))
                   SHIPPING_COUNTRY,
                NVL (xaca.SHIPPING_ZIP_CODE, xaca.BILLING_ZIP_CODE)
                   SHIPPING_ZIP_CODE,
                xaca.AP_CONTACT_FIRST_NAME,
                xaca.AP_CONTACT_LAST_NAME,
                xaca.AP_PHONE,
                xaca.AP_EMAIL,
                xaca.terms,
                xaca.INVOICE_EMAIL_ADDRESS,
                xaca.DUNSNUMBER,
                SUBSTR (xaca.JOB_SITE_NAME, 1, 35) JOB_SITE_NAME,
                xaca.purchase_order_required,
                DECODE (SUBSTR (xaca.print_prices_on_order, 1, 1),
                        'Y', '1',
                        '2')
                   print_prices_on_order,
                (SELECT a.ORA_TAX_EXEMP_TYPE
                   FROM XXWC.XXWC_AHH_AR_TAX_EXEMPT_TYPES_T a
                  WHERE     a.cust_num = xaca.customer_number
                        AND UPPER (a.ship_to_site) =
                               UPPER (xaca.customer_site_number))
                   TAX_EXEMPTION,
                (SELECT REPLACE (REPLACE (A.SITE_CLASS, CHR (13), ''),
                                 CHR (10),
                                 '')
                   FROM XXWC.XXWC_CUST_SITE_CLASSIF_TBL a
                  WHERE     a.CUSTOMER_NUMBER = xaca.customer_number
                        AND UPPER (a.CUST_SITE_NUM) =
                               UPPER (xaca.CUSTOMER_SITE_NUMBER)
                        AND ROWNUM = 1)
                   yard_job_accnt_project,
                ROWID ROW_ID
           FROM XXWC.XXWC_AR_CONV_CUST_ACCT_TBL xaca
          WHERE ROWID = P_ROWID;

      lvc_row_id                   VARCHAR2 (100);
      lvc_sec                      VARCHAR2 (32767);
      plocationrec                 hz_location_v2pub.location_rec_type;
      ppartysiterec                hz_party_site_v2pub.party_site_rec_type;
      pcustacctsiterec             hz_cust_account_site_v2pub.cust_acct_site_rec_type;
      pcustacctsiteuserec          hz_cust_account_site_v2pub.cust_site_use_rec_type;
      pcustomerprofile             hz_customer_profile_v2pub.customer_profile_rec_type;
      pcustproamtrec               hz_customer_profile_v2pub.cust_profile_amt_rec_type;
      o_msg_count                  NUMBER;
      o_return_msg                 VARCHAR2 (4000);
      o_msg_data                   VARCHAR2 (4000) := NULL;
      o_location_id                HZ_LOCATIONS.LOCATION_ID%TYPE;
      o_party_site_id              NUMBER;
      o_bill_to_site_use_id        NUMBER;
      o_party_site_no              VARCHAR2 (100);
      x_msg_data                   VARCHAR2 (32767);
      x_ret_status                 VARCHAR2 (1);
      o_ret_status                 VARCHAR2 (1);
      lvc_procedure                VARCHAR2 (20) := 'PRIMARY_SHIP_TO';
      ln_profile_class_id          NUMBER;
      o_cust_acct_site_id          NUMBER;
      o_cust_acct_site_use_id      NUMBER;
      o_sprofile_id                NUMBER;
      o_cust_acct_profile_amt_id   NUMBER;
      p_object_version_number      NUMBER;
      ln_collector_id              NUMBER;
      ln_credit_analyst            NUMBER;
   BEGIN
      lvc_sec := 'Primary Ship TO Procedure Start';
      debug (p_customer_number, lvc_sec);
      SAVEPOINT customer_record;

      OPEN cur_yard (p_customer_number);

      FETCH cur_yard INTO lvc_row_id;

      CLOSE cur_yard;

      lvc_sec := 'Primary Ship TO Yard Rowid' || lvc_row_id;
      debug (p_customer_number, lvc_sec);



      IF lvc_row_id IS NULL
      THEN
         OPEN cur_non_yard (p_customer_number);

         FETCH cur_non_yard INTO lvc_row_id;

         CLOSE cur_non_yard;
      END IF;

      lvc_sec := 'Primary Ship TO Non-Yard Rowid' || lvc_row_id;
      debug (p_customer_number, lvc_sec);


      FOR REC_CUST IN cur_cust (lvc_row_id)
      LOOP
         BEGIN
            lvc_sec := 'Ship TO Loop Inside ';

            BEGIN
               SELECT territory_code
                 INTO plocationrec.country
                 FROM hr_territories_v
                WHERE (   UPPER (TERRITORY_SHORT_NAME) =
                             UPPER (rec_cust.SHIPPING_COUNTRY)
                       OR UPPER (TERRITORY_CODE) =
                             UPPER (rec_cust.SHIPPING_COUNTRY));
            EXCEPTION
               WHEN OTHERS
               THEN
                  plocationrec.country := NULL;
                  return_msg (o_msg_count, o_msg_data, o_return_msg);
                  x_msg_data := 'Invalid Shipping Country ' || o_return_msg;
                  debug (p_customer_number, x_msg_data);
                  x_ret_status := o_ret_status;
                  ROLLBACK TO customer_record;
                  GOTO STATUS_UPDATE;
            END;

            lvc_sec := 'Primary Ship TO Location Creation';
            debug (p_customer_number, lvc_sec);

            plocationrec.postal_code := rec_cust.shipping_zip_code;
            plocationrec.address1 := rec_cust.shipping_address1;
            plocationrec.address2 := rec_cust.shipping_address2;
            plocationrec.state := rec_cust.shipping_STATE;
            plocationrec.city := rec_cust.shipping_CITY;
            plocationrec.county := rec_cust.shipping_COUNTY;
            plocationrec.created_by_module := g_api_name;
            plocationrec.sales_tax_geocode := NULL;

            hz_location_v2pub.create_location (
               p_init_msg_list   => 'T',
               p_location_rec    => plocationrec,
               x_location_id     => o_location_id,
               x_return_status   => o_ret_status,
               x_msg_count       => o_msg_count,
               x_msg_data        => o_msg_data);

            IF o_ret_status <> 'S'
            THEN
               return_msg (o_msg_count, o_msg_data, o_return_msg);
               x_msg_data := 'Ship to Location Creation: ' || o_return_msg;
               debug (p_customer_number, x_msg_data);
               x_ret_status := o_ret_status;
               ROLLBACK TO customer_record;
               GOTO STATUS_UPDATE;
            END IF;

            lvc_sec :=
                  'Location for Ship to Creation Process Completed - API Status '
               || o_ret_status;

            debug (p_customer_number, lvc_sec);


            lvc_sec := 'Prmary Ship to Party Site Creation';

            o_ret_status := NULL;
            o_msg_count := NULL;
            o_msg_data := NULL;
            ppartysiterec.created_by_module := g_api_name;
            ppartysiterec.status := rec_cust.status;
            ppartysiterec.party_id := p_party_id;
            ppartysiterec.location_id := o_location_id;
            --ppartysiterec.identifying_address_flag := 'Y';

            hz_party_site_v2pub.create_party_site (
               p_init_msg_list       => 'T',
               p_party_site_rec      => ppartysiterec,
               x_party_site_id       => o_party_site_id,
               x_party_site_number   => o_party_site_no,
               x_return_status       => o_ret_status,
               x_msg_count           => o_msg_count,
               x_msg_data            => o_msg_data);

            IF o_ret_status <> 'S'
            THEN
               return_msg (o_msg_count, o_msg_data, o_return_msg);
               x_msg_data :=
                  'Primary Ship to Party Site Creation: ' || o_return_msg;
               debug (p_customer_number, x_msg_data);
               x_ret_status := o_ret_status;
               ROLLBACK TO customer_record;
               GOTO STATUS_UPDATE;
            END IF;

            lvc_sec :=
                  'Party Site for Ship to Creation Process Completed - API Status '
               || o_ret_status;
            debug (p_customer_number, lvc_sec);

            lvc_sec := 'Prmary Ship to Account Site Creation';

            o_ret_status := NULL;
            o_msg_count := NULL;
            o_msg_data := NULL;
            pcustacctsiterec.cust_account_id := p_cust_account_id;
            pcustacctsiterec.party_site_id := o_party_site_id;
            pcustacctsiterec.created_by_module := g_api_name;
            pcustacctsiterec.status := rec_cust.status;
            pcustacctsiterec.org_id := gn_org_id;
            pcustacctsiterec.attribute3 :=
               SUBSTR (NVL (rec_cust.purchase_order_required, 'N'), 1, 1);
            pcustacctsiterec.attribute_category := 'No';
            pcustacctsiterec.attribute1 := rec_cust.print_prices_on_order;
            pcustacctsiterec.attribute14 := 'N';
            pcustacctsiterec.attribute15 := rec_cust.tax_exemption;
            --pcustacctsiterec.attribute15 := 'None';
            pcustacctsiterec.attribute17 :=
                  rec_cust.customer_number
               || '-'
               || rec_cust.customer_site_number;


            BEGIN
               SELECT CASE
                         WHEN customer_class_code = 'COMMERCIAL'
                         THEN
                            'PUBLIC'
                         WHEN customer_class_code = 'GOVERNMENT'
                         THEN
                            'PRIVATE'
                         WHEN customer_class_code = 'NON-CONTRACTOR/OTHER'
                         THEN
                            'PUBLIC'
                         WHEN customer_class_code = 'RESELLER'
                         THEN
                            'PUBLIC'
                         WHEN customer_class_code = 'RESIDENTIAL_CONTRACTOR'
                         THEN
                            'RES'
                         WHEN customer_class_code = 'UNKNOWN'
                         THEN
                            NULL
                         ELSE
                            NULL
                      END
                 INTO pcustacctsiterec.customer_category_code
                 FROM hz_cust_accounts
                WHERE cust_account_id = p_cust_account_id;
            EXCEPTION
               WHEN OTHERS
               THEN
                  pcustacctsiterec.customer_category_code := NULL;
            END;

            hz_cust_account_site_v2pub.create_cust_acct_site (
               p_init_msg_list        => 'T',
               p_cust_acct_site_rec   => pcustacctsiterec,
               x_cust_acct_site_id    => o_cust_acct_site_id,
               x_return_status        => o_ret_status,
               x_msg_count            => o_msg_count,
               x_msg_data             => o_msg_data);

            IF o_ret_status <> 'S'
            THEN
               return_msg (o_msg_count, o_msg_data, o_return_msg);
               x_msg_data := 'Error: Primary Ship to Site : ' || o_return_msg;
               debug (p_customer_number, x_msg_data);
               x_ret_status := o_ret_status;
               ROLLBACK TO customer_record;
               GOTO STATUS_UPDATE;
            END IF;

            lvc_sec :=
                  'Cust Acct Site for Ship to Creation Process Completed - API Status '
               || o_ret_status;

            debug (p_customer_number, lvc_sec);

            lvc_sec :=
               'Cust Acct Site Use for Ship to Creation Process Creation';
            o_ret_status := NULL;
            o_msg_count := NULL;
            o_msg_data := NULL;
            pcustacctsiteuserec.site_use_code := 'BILL_TO';
            pcustacctsiteuserec.cust_acct_site_id := o_cust_acct_site_id;
            pcustacctsiteuserec.created_by_module := g_api_name;
            pcustacctsiteuserec.attribute1 :=
               NVL (rec_cust.yard_job_accnt_project, 'YARD');

            pcustacctsiteuserec.location :=
               SUBSTR (rec_cust.job_site_name, 1, 35);

            IF rec_cust.OUTSIDE_SALES_REP_ID IS NOT NULL
            THEN
               BEGIN
                  SELECT RSA.SALESREP_ID
                    INTO pcustacctsiteuserec.primary_salesrep_id
                    FROM XXWC.XXWC_AHH_AM_T XAAT, APPS.RA_SALESREPS_ALL RSA
                   WHERE     XAAT.SALESREP_ID = RSA.SALESREP_NUMBER
                         AND UPPER (TRIM (XAAT.SLSREPOUT)) =
                                UPPER (TRIM (rec_cust.OUTSIDE_SALES_REP_ID))
                         AND RSA.ORG_ID = 162
                         AND ROWNUM = 1;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     pcustacctsiteuserec.primary_salesrep_id := -3;
               END;
            END IF;


--            IF rec_cust.INSIDE_SALES_REP_ID IS NOT NULL
--            THEN
               BEGIN
                  SELECT A.SALESREP_ID
                    INTO pcustacctsiteuserec.attribute12
                    FROM RA_SALESREPS_ALL A, XXWC.XXWC_AHH_AM_T B
                   WHERE     A.ORG_ID = 162
                         AND UPPER (B.SLSREPOUT) =
                                UPPER (rec_cust.INSIDE_SALES_REP_ID)
                         AND A.SALESREP_NUMBER = B.SALESREP_ID;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     pcustacctsiteuserec.attribute12 := -3;
               END;
--            END IF;

--            IF rec_cust.FSD IS NOT NULL
--            THEN
               BEGIN
                  SELECT A.SALESREP_ID
                    INTO pcustacctsiteuserec.attribute13
                    FROM RA_SALESREPS_ALL A, XXWC.XXWC_AHH_AM_T B
                   WHERE     A.ORG_ID = 162
                         AND UPPER (B.SLSREPOUT) = UPPER (rec_cust.FSD)
                         AND A.SALESREP_NUMBER = B.SALESREP_ID;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     pcustacctsiteuserec.attribute13 := -3;
               END;
--            END IF;

            -- Deriving Collector
            BEGIN
               SELECT ORA_COLL_ID
                 INTO ln_collector_id
                 FROM XXWC.XXWC_AHH_CRD_COLL_ANALYST_T b
                WHERE     UPPER (TRIM (b.ahh_coll_code)) =
                             UPPER (rec_cust.credit_manager)
                      AND ROWNUM = 1;
            EXCEPTION
               WHEN OTHERS
               THEN
                  ln_collector_id := 1;                   -- Default Collector
            END;

            BEGIN
               SELECT ORA_CRD_ANLST_ID
                 INTO ln_credit_analyst
                 FROM XXWC.XXWC_AHH_CRD_COLL_ANALYST_T b
                WHERE     UPPER (TRIM (b.ahh_coll_code)) =
                             UPPER (rec_cust.credit_manager)
                      AND ROWNUM = 1;
            EXCEPTION
               WHEN OTHERS
               THEN
                  ln_credit_analyst := NULL;
            END;


            BEGIN
               SELECT A.TERM_ID
                 INTO pcustomerprofile.standard_terms
                 FROM RA_TERMS A, XXWC.XXWC_AHH_PAYMENT_TERMS_T B
                WHERE     UPPER (TRIM (B.AHH_TERM_NAME)) =
                             UPPER (TRIM (rec_cust.terms))
                      AND UPPER (TRIM (B.TERM_NAME)) = UPPER (A.NAME)
                      AND ROWNUM = 1;
            EXCEPTION
               WHEN OTHERS
               THEN
                  BEGIN
                     SELECT TERM_ID
                       INTO pcustomerprofile.standard_terms
                       FROM RA_TERMS
                      WHERE name = 'N30D';
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        pcustomerprofile.standard_terms := NULL;
                  END;
            END;



            pcustomerprofile.credit_checking := 'Y';
            pcustomerprofile.attribute2 := 2;
            pcustomerprofile.attribute3 := 'Y';
            pcustomerprofile.collector_id := ln_collector_id;
            pcustomerprofile.credit_analyst_id := ln_credit_analyst;
            pcustomerprofile.credit_rating := 'Y';
            pcustomerprofile.account_status := g_acct_status;
            pcustomerprofile.tolerance := 0;
            pcustomerprofile.credit_classification := 'MODERATE';

            BEGIN
               SELECT B.profile_class_id
                 INTO pcustomerprofile.profile_class_id
                 FROM XXWC.XXWC_AHH_PROFILE_CLASSES_T A,
                      HZ_CUST_PROFILE_CLASSES B
                WHERE     A.CUSTOMER_NUM = rec_cust.customer_number
                      AND UPPER (TRIM (a.profile_class_name)) =
                             UPPER (b.name)
                      AND ROWNUM = 1;
            EXCEPTION
               WHEN OTHERS
               THEN
                  BEGIN
                     SELECT PROFILE_CLASS_ID
                       INTO pcustomerprofile.profile_class_id
                       FROM HZ_CUST_PROFILE_CLASSES
                      WHERE NAME = 'Contractor - Non Key';
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        pcustomerprofile.profile_class_id := NULL;
                  END;
            END;


            hz_cust_account_site_v2pub.create_cust_site_use (
               p_init_msg_list          => 'T',
               p_cust_site_use_rec      => pcustacctsiteuserec,
               p_customer_profile_rec   => pcustomerprofile,
               p_create_profile         => 'T',
               p_create_profile_amt     => 'T',
               x_site_use_id            => o_bill_to_site_use_id,
               x_return_status          => o_ret_status,
               x_msg_count              => o_msg_count,
               x_msg_data               => o_msg_data);

            g_bill_to_site_use_id := o_bill_to_site_use_id;

            IF o_ret_status <> 'S'
            THEN
               return_msg (o_msg_count, o_msg_data, o_return_msg);
               x_msg_data :=
                  'Primary Ship TO Site Creation(BillTo1): ' || o_return_msg;
               debug (p_customer_number, x_msg_data);
               x_ret_status := o_ret_status;
               ROLLBACK TO customer_record;
               GOTO STATUS_UPDATE;
            END IF;

            lvc_sec :=
                  'Cust Acct Site Use (Billto) for Ship to Creation Process Completed - API Status '
               || o_ret_status;

            debug (p_customer_number, lvc_sec);


            --====================================================================
            --Create ShipTo site profile amount
            --====================================================================
            o_sprofile_id := NULL;
            o_cust_acct_profile_amt_id := NULL;

            BEGIN
               SELECT cust_account_profile_id
                 INTO o_sprofile_id
                 FROM apps.hz_customer_profiles
                WHERE     cust_account_id = p_cust_account_id
                      AND site_use_id = o_bill_to_site_use_id;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  o_sprofile_id := NULL;
            END;

            IF o_sprofile_id IS NOT NULL
            THEN
               BEGIN
                  SELECT cust_acct_profile_amt_id, object_version_number
                    INTO o_cust_acct_profile_amt_id, p_object_version_number
                    FROM apps.hz_cust_profile_amts
                   WHERE     cust_account_profile_id = o_sprofile_id
                         AND currency_code = g_currency_code;
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     o_cust_acct_profile_amt_id := NULL;
                     p_object_version_number := NULL;
               END;

               IF rec_cust.default_job_credit_limit <> 0
               THEN
                  pcustproamtrec.overall_credit_limit :=
                     rec_cust.default_job_credit_limit;
               END IF;

               IF o_cust_acct_profile_amt_id IS NOT NULL
               THEN
                  --Update amount profile
                  pcustproamtrec.cust_acct_profile_amt_id :=
                     o_cust_acct_profile_amt_id;
                  hz_customer_profile_v2pub.update_cust_profile_amt (
                     p_init_msg_list           => 'T',
                     p_cust_profile_amt_rec    => pcustproamtrec,
                     p_object_version_number   => p_object_version_number,
                     x_return_status           => o_ret_status,
                     x_msg_count               => o_msg_count,
                     x_msg_data                => o_msg_data);
               ELSE
                  --Create Profile Amount
                  pcustproamtrec.cust_account_profile_id := o_sprofile_id;
                  pcustproamtrec.cust_account_id := p_cust_account_id;
                  pcustproamtrec.site_use_id := o_cust_acct_site_use_id;
                  pcustproamtrec.currency_code := g_currency_code;
                  pcustproamtrec.created_by_module := g_api_name;
                  hz_customer_profile_v2pub.create_cust_profile_amt (
                     p_init_msg_list              => 'T',
                     p_check_foreign_key          => 'T',
                     p_cust_profile_amt_rec       => pcustproamtrec,
                     x_cust_acct_profile_amt_id   => o_cust_acct_profile_amt_id,
                     x_return_status              => o_ret_status,
                     x_msg_count                  => o_msg_count,
                     x_msg_data                   => o_msg_data);
               END IF;

               IF o_ret_status <> 'S'
               THEN
                  return_msg (o_msg_count, o_msg_data, o_return_msg);
                  x_msg_data :=
                     'Shipto Profile Amount Creation: ' || o_return_msg;
                  debug (p_customer_number, x_msg_data);
                  x_ret_status := o_ret_status;
                  ROLLBACK TO customer_record;
                  GOTO STATUS_UPDATE;
               END IF;


               IF     NVL (o_cust_acct_profile_amt_id, 0) > 0
                  AND rec_cust.DEFAULT_JOB_CREDIT_LIMIT = 0
               THEN
                  BEGIN
                     UPDATE APPS.HZ_CUST_PROFILE_AMTS
                        SET OVERALL_CREDIT_LIMIT = NULL
                      WHERE CUST_ACCT_PROFILE_AMT_ID =
                               o_cust_acct_profile_amt_id;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        NULL;
                  END;
               END IF;
            END IF;

            --====================================================================
            -- Create site use for Ship to
            --====================================================================
            pcustacctsiteuserec := NULL;
            o_ret_status := NULL;
            o_msg_count := NULL;
            o_msg_data := NULL;

            pcustacctsiteuserec.site_use_code := 'SHIP_TO';
            pcustacctsiteuserec.cust_acct_site_id := o_cust_acct_site_id;

            BEGIN
               SELECT inv_remit_to_code
                 INTO pcustomerprofile.attribute2
                 FROM xxwc_ar_cm_remitcode_tbl
                WHERE area_code = SUBSTR (rec_cust.business_phone, 1, 3);
            EXCEPTION
               WHEN OTHERS
               THEN
                  pcustomerprofile.attribute2 := 2;
            END;

            pcustomerprofile.attribute3 := 'Y';
            pcustacctsiteuserec.bill_to_site_use_id := g_bill_to_site_use_id;

            pcustacctsiteuserec.primary_flag := 'Y';
            pcustacctsiteuserec.created_by_module := g_api_name;



            IF rec_cust.OUTSIDE_SALES_REP_ID IS NOT NULL
            THEN
               BEGIN
                  SELECT A.SALESREP_ID
                    INTO pcustacctsiteuserec.primary_salesrep_id
                    FROM RA_SALESREPS_ALL A, XXWC.XXWC_AHH_AM_T B
                   WHERE     A.ORG_ID = 162
                         AND UPPER (TRIM (B.SLSREPOUT)) =
                                UPPER (TRIM (rec_cust.OUTSIDE_SALES_REP_ID))
                         AND A.SALESREP_NUMBER = B.SALESREP_ID
                         AND ROWNUM = 1;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     pcustacctsiteuserec.primary_salesrep_id := -3;
               END;
            END IF;

            BEGIN
               SELECT A.TERM_ID
                 INTO pcustomerprofile.standard_terms
                 FROM RA_TERMS A, XXWC.XXWC_AHH_PAYMENT_TERMS_T B
                WHERE     UPPER (TRIM (B.AHH_TERM_NAME)) =
                             UPPER (TRIM (rec_cust.terms))
                      AND UPPER (TRIM (B.TERM_NAME)) = UPPER (A.NAME)
                      AND ROWNUM = 1;
            EXCEPTION
               WHEN OTHERS
               THEN
                  BEGIN
                     SELECT TERM_ID
                       INTO pcustomerprofile.standard_terms
                       FROM RA_TERMS
                      WHERE name = 'N30D';
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        pcustomerprofile.standard_terms := NULL;
                  END;
            END;


            pcustomerprofile.credit_checking := 'Y';
            pcustomerprofile.collector_id := ln_collector_id;
            pcustomerprofile.credit_analyst_id := ln_credit_analyst;
            pcustomerprofile.account_status := g_acct_status;
            pcustomerprofile.tolerance := 0;
            pcustacctsiteuserec.location :=
               SUBSTR (rec_cust.job_site_name, 1, 35);
            pcustacctsiteuserec.attribute1 := rec_cust.yard_job_accnt_project;
            pcustomerprofile.credit_classification := 'MODERATE';

            IF UPPER (rec_cust.yard_job_accnt_project) IN ('JOB')
            THEN
               pcustomerprofile.credit_hold := 'Y';
            END IF;

            BEGIN
               SELECT B.profile_class_id
                 INTO pcustomerprofile.profile_class_id
                 FROM XXWC.XXWC_AHH_PROFILE_CLASSES_T A,
                      HZ_CUST_PROFILE_CLASSES B
                WHERE     A.CUSTOMER_NUM = rec_cust.customer_number
                      AND UPPER (TRIM (a.profile_class_name)) =
                             UPPER (b.name)
                      AND ROWNUM = 1;
            EXCEPTION
               WHEN OTHERS
               THEN
                  BEGIN
                     SELECT PROFILE_CLASS_ID
                       INTO pcustomerprofile.profile_class_id
                       FROM HZ_CUST_PROFILE_CLASSES
                      WHERE NAME = 'Contractor - Non Key';
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        pcustomerprofile.profile_class_id := NULL;
                  END;
            END;

            hz_cust_account_site_v2pub.create_cust_site_use (
               p_init_msg_list          => 'T',
               p_cust_site_use_rec      => pcustacctsiteuserec,
               p_customer_profile_rec   => pcustomerprofile,
               p_create_profile         => 'T',
               p_create_profile_amt     => 'T',
               x_site_use_id            => o_cust_acct_site_use_id,
               x_return_status          => o_ret_status,
               x_msg_count              => o_msg_count,
               x_msg_data               => o_msg_data);

            IF o_ret_status <> 'S'
            THEN
               return_msg (o_msg_count, o_msg_data, o_return_msg);
               x_msg_data := 'Account Site Creation(SHIPTO): ' || o_return_msg;
               debug (p_customer_number, x_msg_data);
               x_ret_status := o_ret_status;
               ROLLBACK TO customer_record;
               GOTO STATUS_UPDATE;
            END IF;

            lvc_sec :=
                  'Cust Acct Site Use for Ship to Creation Process Completed - API Status '
               || o_ret_status;
            debug (p_customer_number, lvc_sec);

            --====================================================================
            -- Create ShipTo site profile amount
            --====================================================================
            pcustproamtrec := NULL;
            o_sprofile_id := NULL;
            o_cust_acct_profile_amt_id := NULL;

            BEGIN
               SELECT cust_account_profile_id
                 INTO o_sprofile_id
                 FROM apps.hz_customer_profiles
                WHERE     cust_account_id = p_cust_account_id
                      AND site_use_id = o_cust_acct_site_use_id;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  o_sprofile_id := NULL;
            END;

            IF o_sprofile_id IS NOT NULL
            THEN
               BEGIN
                  SELECT cust_acct_profile_amt_id, object_version_number
                    INTO o_cust_acct_profile_amt_id, p_object_version_number
                    FROM apps.hz_cust_profile_amts
                   WHERE     cust_account_profile_id = o_sprofile_id
                         AND currency_code = g_currency_code;
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     o_cust_acct_profile_amt_id := NULL;
                     p_object_version_number := NULL;
               END;


               IF NVL (rec_cust.default_job_credit_limit, 0) = 0
               THEN
                  pcustproamtrec.overall_credit_limit := NULL;
               ELSE
                  pcustproamtrec.overall_credit_limit :=
                     rec_cust.default_job_credit_limit;
               END IF;

               IF o_cust_acct_profile_amt_id IS NOT NULL
               THEN
                  --Update amount profile
                  pcustproamtrec.cust_acct_profile_amt_id :=
                     o_cust_acct_profile_amt_id;
                  hz_customer_profile_v2pub.update_cust_profile_amt (
                     p_init_msg_list           => 'T',
                     p_cust_profile_amt_rec    => pcustproamtrec,
                     p_object_version_number   => p_object_version_number,
                     x_return_status           => o_ret_status,
                     x_msg_count               => o_msg_count,
                     x_msg_data                => o_msg_data);
               ELSE
                  --Create Profile Amount
                  pcustproamtrec.cust_account_profile_id := o_sprofile_id;
                  pcustproamtrec.cust_account_id := p_cust_account_id;
                  pcustproamtrec.site_use_id := o_cust_acct_site_use_id;
                  pcustproamtrec.currency_code := g_currency_code;
                  pcustproamtrec.created_by_module := g_api_name;
                  hz_customer_profile_v2pub.create_cust_profile_amt (
                     p_init_msg_list              => 'T',
                     p_check_foreign_key          => 'T',
                     p_cust_profile_amt_rec       => pcustproamtrec,
                     x_cust_acct_profile_amt_id   => o_cust_acct_profile_amt_id,
                     x_return_status              => o_ret_status,
                     x_msg_count                  => o_msg_count,
                     x_msg_data                   => o_msg_data);
               END IF;

               IF o_ret_status <> 'S'
               THEN
                  return_msg (o_msg_count, o_msg_data, o_return_msg);
                  x_msg_data :=
                     'Shipto Profile Amount Creation: ' || o_return_msg;
                  debug (p_customer_number, x_msg_data);
                  x_ret_status := o_ret_status;
                  ROLLBACK TO customer_record;
                  GOTO STATUS_UPDATE;
               END IF;


               IF     NVL (o_cust_acct_profile_amt_id, 0) > 0
                  AND rec_cust.DEFAULT_JOB_CREDIT_LIMIT = 0
               THEN
                  BEGIN
                     UPDATE APPS.HZ_CUST_PROFILE_AMTS
                        SET OVERALL_CREDIT_LIMIT = NULL
                      WHERE CUST_ACCT_PROFILE_AMT_ID =
                               o_cust_acct_profile_amt_id;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        NULL;
                  END;
               END IF;
            END IF;

            lvc_sec :=
                  'Profile Amts for Ship to Creation Process Completed - API Status '
               || o_ret_status;
            debug (p_customer_number, lvc_sec);

            lvc_sec := ' Primary Ship TO usage rules';

            o_msg_count := NULL;
            o_msg_data := NULL;
            o_ret_status := NULL;

            hz_credit_usages_cascade_pkg.cascade_credit_usage_rules (
               p_cust_acct_profile_amt_id   => o_cust_acct_profile_amt_id,
               p_cust_profile_id            => o_sprofile_id,
               p_profile_class_amt_id       => NULL,
               p_profile_class_id           => NULL,
               x_return_status              => o_ret_status,
               x_msg_count                  => o_msg_count,
               x_msg_data                   => o_msg_data);

            IF o_ret_status <> 'S'
            THEN
               return_msg (o_msg_count, o_msg_data, o_return_msg);
               x_msg_data :=
                  'Shipto cascade_credit_usage_rules: ' || o_return_msg;
               debug (p_customer_number, x_msg_data);
               x_ret_status := o_ret_status;
               ROLLBACK TO customer_record;
               GOTO STATUS_UPDATE;
            END IF;

            lvc_sec :=
                  'Profile Amts usage Rules for Ship to Creation Process Completed - API Status '
               || o_ret_status;
            debug (p_customer_number, lvc_sec);


           <<STATUS_UPDATE>>
            IF NVL (x_ret_status, 'S') = 'S'
            THEN
               UPDATE XXWC.XXWC_AR_CONV_CUST_ACCT_TBL
                  SET PROCESS_FLAG = 'Y',
                      ORACLE_CUSTOMER_NUMBER = g_cust_acct_no
                WHERE ROWID = rec_cust.row_id;
            ELSE
               UPDATE XXWC.XXWC_AR_CONV_CUST_ACCT_TBL
                  SET PROCESS_FLAG = 'E', PROCESS_MESSAGE = x_msg_data
                WHERE ROWID = rec_cust.row_id;
            END IF;
         EXCEPTION
            WHEN OTHERS
            THEN
               x_msg_data := SQLERRM;
               debug (p_customer_number, x_msg_data);

               UPDATE xxwc.XXWC_AR_CONV_CUST_ACCT_TBL
                  SET process_flag = 'E', PROCESS_message = x_msg_data
                WHERE ROWID = rec_cust.row_id;

               xxcus_error_pkg.xxcus_error_main_api (
                  p_called_from         => g_err_callfrom || '.' || lvc_procedure,
                  p_calling             => lvc_sec,
                  p_ora_error_msg       => SQLERRM,
                  p_error_desc          =>    'Error Occured for '
                                           || p_customer_number
                                           || 'record id in XXWC_AR_CONV_CUST_ACCT_TBL.',
                  p_distribution_list   => g_distro_list,
                  p_module              => g_module);
         END;
      END LOOP;

      COMMIT;
   END;


   PROCEDURE multiple_shipto_sites (p_customer_number   IN VARCHAR2,
                                    p_cust_account_id   IN NUMBER,
                                    p_party_id          IN NUMBER)
   IS
      -- ==============================================================================================================================
      -- Procedure: multiple_shipto_sites
      -- Purpose: To create customer all ship to sites except primary shipto
      -- ==============================================================================================================================
      --  REVISIONS:
      --   Ver        Date        Author               Description
      --  ---------  ---------- ---------------  --------------------------------------------------------------------------------------
      --  1.0        06/06/2018  P.Vamshidhar     TMS#20180308-00290 -AH HARRIS Customer Conversion
      -- ==============================================================================================================================

      CURSOR cust_sites
      IS
         SELECT RECORD_ID,
                SALES_FORCE_ID,
                CUSTOMER_NUMBER,
                UPPER (CUSTOMER_SITE_NUMBER) CUSTOMER_SITE_NUMBER,
                BUSINESS_NAME,
                BUSINESS_PHONE,
                BUSINESS_FAX,
                EMAIL_ADDRESS,
                OUTSIDE_SALES_REP_ID,
                INSIDE_SALES_REP_ID,
                FSD,
                'UNKNOWN' type_of_business,
                CREDIT_HOLD,
                PREDOMINANT_TRADE,
                SUBSTR (STATUS, 1, 1) STATUS,
                BILLING_ADDRESS1,
                BILLING_ADDRESS2,
                BILLING_CITY,
                BILLING_COUNTY,
                BILLING_STATE,
                NVL (BILLING_COUNTRY, 'US') BILLING_COUNTRY,
                BILLING_ZIP_CODE,
                credit_manager,
                NVL (SHIPPING_ADDRESS1, BILLING_ADDRESS1) SHIPPING_ADDRESS1,
                DECODE (SHIPPING_ADDRESS1,
                        NULL, BILLING_ADDRESS2,
                        SHIPPING_ADDRESS2)
                   SHIPPING_ADDRESS2,
                NVL (SHIPPING_CITY, BILLING_CITY) SHIPPING_CITY,
                NVL (SHIPPING_COUNTY, BILLING_COUNTY) SHIPPING_COUNTY,
                NVL (SHIPPING_STATE, BILLING_STATE) SHIPPING_STATE,
                TAXING_STATE,
                NVL (SHIPPING_COUNTRY, NVL (BILLING_COUNTRY, 'US'))
                   SHIPPING_COUNTRY,
                NVL (SHIPPING_ZIP_CODE, BILLING_ZIP_CODE) SHIPPING_ZIP_CODE,
                CUST_SITE_CLASSIFICATION,
                CREDIT_LIMIT,
                DEFAULT_JOB_CREDIT_LIMIT,
                AP_CONTACT_FIRST_NAME,
                AP_CONTACT_LAST_NAME,
                AP_PHONE,
                AP_EMAIL,
                INVOICE_EMAIL_ADDRESS,
                POD_EMAIL_ADDRESS,
                SOA_EMAIL_ADDRESS,
                PURCHASE_ORDER_REQUIRED,
                DUNSNUMBER,
                CREATION_DATE,
                DECODE (SUBSTR (print_prices_on_order, 1, 1), 'Y', '1', '2')
                   print_prices_on_order,
                SUBSTR (JOB_SITE_NAME, 1, 35) job_site_name,
                JOB_DESC,
                REMIT_TO_ADDRESS,
                (SELECT ORA_TAX_EXEMP_TYPE
                   FROM XXWC.XXWC_AHH_AR_TAX_EXEMPT_TYPES_T
                  WHERE     cust_num = xaca.customer_number
                        AND UPPER (ship_to_site) =
                               UPPER (xaca.customer_site_number))
                   TAX_EXEMPTION,
                COLLECTION_NOTES,
                SHIP_METHOD,
                TAX_CERTIFICATE,
                NON_TAX_REASON,
                TERMS,
                (SELECT REPLACE (REPLACE (A.SITE_CLASS, CHR (13), ''),
                                 CHR (10),
                                 '')
                   FROM XXWC.XXWC_CUST_SITE_CLASSIF_TBL A
                  WHERE     A.CUSTOMER_NUMBER = xaca.customer_number
                        AND UPPER (A.CUST_SITE_NUM) =
                               UPPER (xaca.CUSTOMER_SITE_NUMBER)
                        AND ROWNUM = 1)
                   yard_job_accnt_project,
                ROWID row_id
           FROM XXWC.XXWC_AR_CONV_CUST_ACCT_TBL xaca
          WHERE     xaca.RECORD_ID = P_customer_number
                AND process_flag = 'V'
                AND customer_site_number IS NOT NULL;

      lvc_sec                      VARCHAR2 (32767);
      o_msg_data                   VARCHAR2 (32767);
      o_msg_count                  NUMBER;
      o_location_id                HZ_LOCATIONS.LOCATION_ID%TYPE;
      o_sprofile_id                NUMBER;
      o_party_site_id              NUMBER;
      o_party_site_no              VARCHAR2 (100);
      o_return_msg                 VARCHAR2 (32767);
      o_ret_status                 VARCHAR2 (1);
      plocationrec                 hz_location_v2pub.location_rec_type;
      ppartysiterec                hz_party_site_v2pub.party_site_rec_type;
      pcustacctsiterec             hz_cust_account_site_v2pub.cust_acct_site_rec_type;
      pcustacctsiteuserec          hz_cust_account_site_v2pub.cust_site_use_rec_type;
      pcustomerprofile             hz_customer_profile_v2pub.customer_profile_rec_type;
      pcustproamtrec               hz_customer_profile_v2pub.cust_profile_amt_rec_type;
      o_bill_to_site_use_id        NUMBER;
      o_cust_acct_site_id          NUMBER;
      o_cust_acct_site_use_id      NUMBER;
      o_cust_acct_profile_amt_id   NUMBER;
      x_msg_data                   VARCHAR2 (32767);
      x_ret_status                 VARCHAR2 (1);
      p_object_version_number      NUMBER;
      lvc_procedure                VARCHAR2 (30) := 'MULTIPLE_SHIPTO_SITES';
      ln_profile_class_id          NUMBER;
      ln_collector_id              NUMBER;
      ln_credit_analyst            NUMBER;
   BEGIN
      debug (p_customer_number, 'Inside Create Multiple Sites');



      FOR rec_sites IN cust_sites
      LOOP
         BEGIN
            SAVEPOINT customer_record;
            lvc_sec := 'Ship TO location Creation';
            plocationrec := NULL;
            ppartysiterec := NULL;
            pcustacctsiterec := NULL;
            pcustacctsiteuserec := NULL;
            pcustomerprofile := NULL;
            o_ret_status := NULL;
            o_msg_count := NULL;
            o_msg_data := NULL;
            o_location_id := NULL;
            o_cust_acct_site_use_id := NULL;

            IF rec_sites.SHIPPING_COUNTRY IS NOT NULL
            THEN
               BEGIN
                  SELECT TERRITORY_CODE
                    INTO plocationrec.country
                    FROM HR_TERRITORIES_V
                   WHERE (   UPPER (TERRITORY_SHORT_NAME) =
                                UPPER (rec_sites.BILLING_COUNTRY)
                          OR UPPER (TERRITORY_CODE) =
                                UPPER (rec_sites.BILLING_COUNTRY));
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     plocationrec.country := NULL;
                     return_msg (o_msg_count, o_msg_data, o_return_msg);
                     o_return_msg :=
                        'Invalid Shipping Country ' || o_return_msg;
                     ROLLBACK TO customer_record;
                     GOTO STATUS_UPDATE;
               END;
            ELSE
               plocationrec.country := NULL;
            END IF;

            plocationrec.postal_code := rec_sites.SHIPPING_ZIP_CODE;
            plocationrec.address1 := rec_sites.SHIPPING_ADDRESS1;
            plocationrec.address2 := rec_sites.SHIPPING_ADDRESS2;
            plocationrec.state := rec_sites.SHIPPING_STATE;
            plocationrec.city := rec_sites.SHIPPING_CITY;
            plocationrec.county := rec_sites.SHIPPING_COUNTY;
            plocationrec.created_by_module := g_api_name;
            plocationrec.sales_tax_geocode := NULL;

            hz_location_v2pub.create_location (
               p_init_msg_list   => 'T',
               p_location_rec    => plocationrec,
               x_location_id     => o_location_id,
               x_return_status   => o_ret_status,
               x_msg_count       => o_msg_count,
               x_msg_data        => o_msg_data);


            IF o_ret_status <> 'S'
            THEN
               return_msg (o_msg_count, o_msg_data, o_return_msg);
               x_msg_data := 'Location Creation: ' || o_return_msg;
               debug (p_customer_number, x_msg_data);
               x_ret_status := o_ret_status;
               ROLLBACK TO customer_record;
               GOTO STATUS_UPDATE;
            END IF;

            lvc_sec :=
                  'Location Account for Ship To Site Creation Process Completed - API Status'
               || o_ret_status;

            debug (p_customer_number, lvc_sec);

            lvc_sec := 'Ship TO Party Site Creation';

            o_ret_status := NULL;
            o_msg_count := NULL;
            o_msg_data := NULL;
            o_party_site_id := NULL;
            o_party_site_no := NULL;
            o_sprofile_id := NULL;
            ppartysiterec.created_by_module := g_api_name;

            --Create a party site now
            ppartysiterec.party_id := p_party_id;
            ppartysiterec.status := rec_sites.status;
            ppartysiterec.location_id := o_location_id;
            --ppartysiterec.identifying_address_flag := 'Y';

            hz_party_site_v2pub.create_party_site (
               p_init_msg_list       => 'T',
               p_party_site_rec      => ppartysiterec,
               x_party_site_id       => o_party_site_id,
               x_party_site_number   => o_party_site_no,
               x_return_status       => o_ret_status,
               x_msg_count           => o_msg_count,
               x_msg_data            => o_msg_data);

            IF o_ret_status <> 'S'
            THEN
               return_msg (o_msg_count, o_msg_data, o_return_msg);
               x_msg_data := 'Error: Ship to Party Creation ' || o_return_msg;
               debug (p_customer_number, x_msg_data);
               x_ret_status := o_ret_status;
               ROLLBACK TO customer_record;
               GOTO STATUS_UPDATE;
            END IF;

            lvc_sec :=
                  'Ship TO Party Site Creation Process Completed - API Status '
               || o_ret_status;

            debug (p_customer_number, lvc_sec);

            lvc_sec := 'Ship to Site Account Creation';



            pcustacctsiterec := NULL;
            o_ret_status := NULL;
            o_msg_count := NULL;
            o_msg_data := NULL;
            o_cust_acct_site_id := NULL;
            pcustacctsiterec.cust_account_id := p_cust_account_id;
            pcustacctsiterec.party_site_id := o_party_site_id;
            pcustacctsiterec.created_by_module := g_api_name;
            pcustacctsiterec.status := rec_sites.status;
            pcustacctsiterec.org_id := gn_org_id;
            pcustacctsiterec.attribute3 :=
               SUBSTR (NVL (rec_sites.purchase_order_required, 'N'), 1, 1);
            pcustacctsiterec.attribute_category := 'No';
            pcustacctsiterec.attribute1 := rec_sites.print_prices_on_order;
            pcustacctsiterec.attribute14 := 'N';
            pcustacctsiterec.attribute15 := rec_sites.tax_exemption;
            --pcustacctsiterec.attribute15 := 'None';
            pcustacctsiterec.attribute17 :=
                  rec_sites.customer_number
               || '-'
               || rec_sites.customer_site_number;

            BEGIN
               SELECT CASE
                         WHEN customer_class_code = 'COMMERCIAL'
                         THEN
                            'PUBLIC'
                         WHEN customer_class_code = 'GOVERNMENT'
                         THEN
                            'PRIVATE'
                         WHEN customer_class_code = 'NON-CONTRACTOR/OTHER'
                         THEN
                            'PUBLIC'
                         WHEN customer_class_code = 'RESELLER'
                         THEN
                            'PUBLIC'
                         WHEN customer_class_code = 'RESIDENTIAL_CONTRACTOR'
                         THEN
                            'RES'
                         WHEN customer_class_code = 'UNKNOWN'
                         THEN
                            NULL
                         ELSE
                            NULL
                      END
                 INTO pcustacctsiterec.customer_category_code
                 FROM hz_cust_accounts
                WHERE cust_account_id = p_cust_account_id;
            EXCEPTION
               WHEN OTHERS
               THEN
                  pcustacctsiterec.customer_category_code := NULL;
            END;

            hz_cust_account_site_v2pub.create_cust_acct_site (
               p_init_msg_list        => 'T',
               p_cust_acct_site_rec   => pcustacctsiterec,
               x_cust_acct_site_id    => o_cust_acct_site_id,
               x_return_status        => o_ret_status,
               x_msg_count            => o_msg_count,
               x_msg_data             => o_msg_data);


            IF o_ret_status <> 'S'
            THEN
               return_msg (o_msg_count, o_msg_data, o_return_msg);
               x_msg_data := 'Ship TO Site Accont: ' || o_return_msg;
               debug (p_customer_number, x_msg_data);
               x_ret_status := o_ret_status;
               ROLLBACK TO customer_record;
               GOTO STATUS_UPDATE;
            END IF;

            lvc_sec :=
                  'Ship To Cust Acct Site Creation Process Completed - API Status '
               || o_ret_status;
            debug (p_customer_number, lvc_sec);

            lvc_sec := 'Creaete Ship TO Use for Bill TO';


            -- Deriving Collector
            BEGIN
               ln_collector_id := NULL;

               SELECT ORA_COLL_ID
                 INTO ln_collector_id
                 FROM XXWC.XXWC_AHH_CRD_COLL_ANALYST_T b
                WHERE     UPPER (TRIM (b.ahh_coll_code)) =
                             UPPER (rec_sites.credit_manager)
                      AND ROWNUM = 1;
            EXCEPTION
               WHEN OTHERS
               THEN
                  ln_collector_id := 1;                   -- Default Collector
            END;

            BEGIN
               ln_credit_analyst := NULL;

               SELECT ORA_CRD_ANLST_ID
                 INTO ln_credit_analyst
                 FROM XXWC.XXWC_AHH_CRD_COLL_ANALYST_T b
                WHERE     UPPER (TRIM (b.ahh_coll_code)) =
                             UPPER (rec_sites.credit_manager)
                      AND ROWNUM = 1;
            EXCEPTION
               WHEN OTHERS
               THEN
                  ln_credit_analyst := NULL;
            END;

            pcustacctsiteuserec := NULL;
            o_ret_status := NULL;
            o_msg_count := NULL;
            o_msg_data := NULL;
            o_cust_acct_site_use_id := NULL;

            pcustacctsiteuserec.site_use_code := 'BILL_TO';
            pcustacctsiteuserec.cust_acct_site_id := o_cust_acct_site_id;
            pcustacctsiteuserec.created_by_module := g_api_name;
            pcustacctsiteuserec.location :=
               SUBSTR (rec_sites.job_site_name, 1, 35);
            pcustacctsiteuserec.attribute1 := rec_sites.yard_job_accnt_project;

            BEGIN
               SELECT A.SALESREP_ID
                 INTO pcustacctsiteuserec.primary_salesrep_id
                 FROM RA_SALESREPS_ALL A, XXWC.XXWC_AHH_AM_T B
                WHERE     A.ORG_ID = 162
                      AND UPPER (TRIM (B.SLSREPOUT)) =
                             UPPER (TRIM (rec_sites.OUTSIDE_SALES_REP_ID))
                      AND A.SALESREP_NUMBER = B.SALESREP_ID
                      AND ROWNUM = 1;
            EXCEPTION
               WHEN OTHERS
               THEN
                  pcustacctsiteuserec.primary_salesrep_id := -3;
            END;

--            IF rec_sites.INSIDE_SALES_REP_ID IS NOT NULL
--            THEN
               BEGIN
                  SELECT A.SALESREP_ID
                    INTO pcustacctsiteuserec.attribute12
                    FROM RA_SALESREPS_ALL A, XXWC.XXWC_AHH_AM_T B
                   WHERE     A.ORG_ID = 162
                         AND UPPER (B.SLSREPOUT) =
                                UPPER (rec_sites.INSIDE_SALES_REP_ID)
                         AND A.SALESREP_NUMBER = B.SALESREP_ID;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     pcustacctsiteuserec.attribute12 := -3;
               END;
--            END IF;

--            IF rec_sites.FSD IS NOT NULL
--            THEN
               BEGIN
                  SELECT A.SALESREP_ID
                    INTO pcustacctsiteuserec.attribute13
                    FROM RA_SALESREPS_ALL A, XXWC.XXWC_AHH_AM_T B
                   WHERE     A.ORG_ID = 162
                         AND UPPER (B.SLSREPOUT) = UPPER (rec_sites.FSD)
                         AND A.SALESREP_NUMBER = B.SALESREP_ID;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     pcustacctsiteuserec.attribute13 := -3;
               END;
--            END IF;



            BEGIN
               SELECT A.TERM_ID
                 INTO pcustomerprofile.standard_terms
                 FROM RA_TERMS A, XXWC.XXWC_AHH_PAYMENT_TERMS_T B
                WHERE     UPPER (TRIM (B.AHH_TERM_NAME)) =
                             UPPER (TRIM (rec_sites.terms))
                      AND UPPER (TRIM (B.TERM_NAME)) = UPPER (A.NAME)
                      AND ROWNUM = 1;
            EXCEPTION
               WHEN OTHERS
               THEN
                  BEGIN
                     SELECT TERM_ID
                       INTO pcustomerprofile.standard_terms
                       FROM RA_TERMS
                      WHERE name = 'N30D';
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        pcustomerprofile.standard_terms := NULL;
                  END;
            END;


            pcustomerprofile.credit_checking := 'Y';
            pcustomerprofile.attribute2 := 2;
            pcustomerprofile.attribute3 := 'Y';
            pcustomerprofile.collector_id := ln_collector_id;
            pcustomerprofile.credit_analyst_id := ln_credit_analyst;
            pcustomerprofile.credit_rating := 'Y';
            pcustomerprofile.account_status := g_acct_status;
            pcustomerprofile.tolerance := 0;
            pcustomerprofile.credit_classification := 'MODERATE';

            BEGIN
               ln_profile_class_id := NULL;

               SELECT B.profile_class_id
                 INTO ln_profile_class_id
                 FROM XXWC.XXWC_AHH_PROFILE_CLASSES_T A,
                      HZ_CUST_PROFILE_CLASSES B
                WHERE     A.CUSTOMER_NUM = rec_sites.customer_number
                      AND UPPER (TRIM (a.profile_class_name)) =
                             UPPER (b.name)
                      AND ROWNUM = 1;
            EXCEPTION
               WHEN OTHERS
               THEN
                  BEGIN
                     SELECT PROFILE_CLASS_ID
                       INTO ln_profile_class_id
                       FROM HZ_CUST_PROFILE_CLASSES
                      WHERE NAME = 'Contractor - Non Key';
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        ln_profile_class_id := NULL;
                  END;
            END;

            pcustomerprofile.profile_class_id := ln_profile_class_id;

            hz_cust_account_site_v2pub.create_cust_site_use (
               p_init_msg_list          => 'T',
               p_cust_site_use_rec      => pcustacctsiteuserec,
               p_customer_profile_rec   => pcustomerprofile,
               p_create_profile         => 'T',
               p_create_profile_amt     => 'T',
               x_site_use_id            => o_bill_to_site_use_id,
               x_return_status          => o_ret_status,
               x_msg_count              => o_msg_count,
               x_msg_data               => o_msg_data);

            IF o_ret_status <> 'S'
            THEN
               return_msg (o_msg_count, o_msg_data, o_return_msg);
               x_msg_data :=
                  'Account Site Creation(BillTo1): ' || o_return_msg;
               debug (p_customer_number, x_msg_data);
               x_ret_status := o_ret_status;
               ROLLBACK TO customer_record;
               GOTO STATUS_UPDATE;
            END IF;

            lvc_sec :=
                  'Cust Acct Site Use (Billto) for Ship to Creation Process Completed - API Status '
               || o_ret_status;

            debug (p_customer_number, lvc_sec);

            lvc_sec := 'Shipto Site Profile Amounts Create';

            pcustproamtrec := NULL;
            o_sprofile_id := NULL;
            o_cust_acct_profile_amt_id := NULL;

            BEGIN
               SELECT cust_account_profile_id
                 INTO o_sprofile_id
                 FROM apps.hz_customer_profiles
                WHERE     cust_account_id = p_cust_account_id
                      AND site_use_id = o_bill_to_site_use_id;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  o_sprofile_id := NULL;
            END;

            IF o_sprofile_id IS NOT NULL
            THEN
               BEGIN
                  SELECT cust_acct_profile_amt_id, object_version_number
                    INTO o_cust_acct_profile_amt_id, p_object_version_number
                    FROM apps.hz_cust_profile_amts
                   WHERE     cust_account_profile_id = o_sprofile_id
                         AND currency_code = g_currency_code;
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     o_cust_acct_profile_amt_id := NULL;
                     p_object_version_number := NULL;
               END;


               IF Rec_sites.default_job_credit_limit <> 0
               THEN
                  pcustproamtrec.overall_credit_limit :=
                     rec_sites.default_job_credit_limit;
               END IF;

               IF o_cust_acct_profile_amt_id IS NOT NULL
               THEN
                  --Update amount profile
                  pcustproamtrec.cust_acct_profile_amt_id :=
                     o_cust_acct_profile_amt_id;
                  hz_customer_profile_v2pub.update_cust_profile_amt (
                     p_init_msg_list           => 'T',
                     p_cust_profile_amt_rec    => pcustproamtrec,
                     p_object_version_number   => p_object_version_number,
                     x_return_status           => o_ret_status,
                     x_msg_count               => o_msg_count,
                     x_msg_data                => o_msg_data);
               ELSE
                  --Create Profile Amount
                  pcustproamtrec.cust_account_profile_id := o_sprofile_id;
                  pcustproamtrec.cust_account_id := p_cust_account_id;
                  pcustproamtrec.site_use_id := o_cust_acct_site_use_id;
                  pcustproamtrec.currency_code := g_currency_code;
                  pcustproamtrec.created_by_module := g_api_name;
                  hz_customer_profile_v2pub.create_cust_profile_amt (
                     p_init_msg_list              => 'T',
                     p_check_foreign_key          => 'T',
                     p_cust_profile_amt_rec       => pcustproamtrec,
                     x_cust_acct_profile_amt_id   => o_cust_acct_profile_amt_id,
                     x_return_status              => o_ret_status,
                     x_msg_count                  => o_msg_count,
                     x_msg_data                   => o_msg_data);
               END IF;

               IF o_ret_status <> 'S'
               THEN
                  return_msg (o_msg_count, o_msg_data, o_return_msg);
                  x_msg_data :=
                        'Error:Shipto Profile Amount Creation: '
                     || o_return_msg
                     || ' '
                     || o_ret_status;
                  debug (p_customer_number, x_msg_data);
                  x_ret_status := o_ret_status;
                  ROLLBACK TO customer_record;
                  GOTO STATUS_UPDATE;
               END IF;

               IF     NVL (o_cust_acct_profile_amt_id, 0) > 0
                  AND rec_sites.DEFAULT_JOB_CREDIT_LIMIT = 0
               THEN
                  BEGIN
                     UPDATE APPS.HZ_CUST_PROFILE_AMTS
                        SET OVERALL_CREDIT_LIMIT = NULL
                      WHERE CUST_ACCT_PROFILE_AMT_ID =
                               o_cust_acct_profile_amt_id;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        NULL;
                  END;
               END IF;
            END IF;

            lvc_sec :=
                  'Cust Acct Site Use (Billto) for Ship to Profile amts update - API Status '
               || o_ret_status;

            debug (p_customer_number, lvc_sec);


            lvc_sec := 'ShipTO Site Create Usage Rules';
            hz_credit_usages_cascade_pkg.cascade_credit_usage_rules (
               p_cust_acct_profile_amt_id   => o_cust_acct_profile_amt_id,
               p_cust_profile_id            => o_sprofile_id,
               p_profile_class_amt_id       => NULL,
               p_profile_class_id           => NULL,
               x_return_status              => o_ret_status,
               x_msg_count                  => o_msg_count,
               x_msg_data                   => o_msg_data);

            IF o_ret_status <> 'S'
            THEN
               return_msg (o_msg_count, o_msg_data, o_return_msg);
               x_msg_data :=
                     'Ship TO Site Profile Amounts Usage Rules: '
                  || o_return_msg;
               debug (p_customer_number, x_msg_data);
               x_ret_status := o_ret_status;
               ROLLBACK TO customer_record;
               GOTO STATUS_UPDATE;
            END IF;

            lvc_sec :=
                  'Profile Amt usage rules Creation Process Completed - API Status '
               || o_ret_status;

            debug (p_customer_number, lvc_sec);


            lvc_sec := 'Create site use for Ship to (Multiple) ';

            pcustacctsiteuserec := NULL;
            o_ret_status := NULL;
            o_msg_count := NULL;
            o_msg_data := NULL;

            pcustacctsiteuserec.site_use_code := 'SHIP_TO';
            pcustacctsiteuserec.cust_acct_site_id := o_cust_acct_site_id;

            BEGIN
               SELECT inv_remit_to_code
                 INTO pcustomerprofile.attribute2
                 FROM xxwc_ar_cm_remitcode_tbl
                WHERE area_code = SUBSTR (rec_sites.business_phone, 1, 3);
            EXCEPTION
               WHEN OTHERS
               THEN
                  pcustomerprofile.attribute2 := 2;
            END;

            pcustomerprofile.attribute3 := 'Y';
            -- pcustacctsiteuserec.bill_to_site_use_id := g_bill_to_site_use_id;
            pcustacctsiteuserec.bill_to_site_use_id := o_bill_to_site_use_id; --g_bill_to_site_use_id;

            pcustacctsiteuserec.primary_flag := 'Y';
            pcustacctsiteuserec.created_by_module := g_api_name;

            IF rec_sites.OUTSIDE_SALES_REP_ID IS NOT NULL
            THEN
               BEGIN
                  SELECT A.SALESREP_ID
                    INTO pcustacctsiteuserec.primary_salesrep_id
                    FROM RA_SALESREPS_ALL A, XXWC.XXWC_AHH_AM_T B
                   WHERE     A.ORG_ID = 162
                         AND UPPER (TRIM (B.SLSREPOUT)) =
                                UPPER (TRIM (rec_sites.OUTSIDE_SALES_REP_ID))
                         AND A.SALESREP_NUMBER = B.SALESREP_ID
                         AND ROWNUM = 1;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     pcustacctsiteuserec.primary_salesrep_id := -3;
               END;
            END IF;

            BEGIN
               SELECT A.TERM_ID
                 INTO pcustomerprofile.standard_terms
                 FROM RA_TERMS A, XXWC.XXWC_AHH_PAYMENT_TERMS_T B
                WHERE     UPPER (TRIM (B.AHH_TERM_NAME)) =
                             UPPER (TRIM (rec_sites.terms))
                      AND UPPER (TRIM (B.TERM_NAME)) = UPPER (A.NAME)
                      AND ROWNUM = 1;
            EXCEPTION
               WHEN OTHERS
               THEN
                  BEGIN
                     SELECT TERM_ID
                       INTO pcustomerprofile.standard_terms
                       FROM RA_TERMS
                      WHERE name = 'N30D';
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        pcustomerprofile.standard_terms := NULL;
                  END;
            END;

            pcustomerprofile.credit_checking := 'Y';
            pcustomerprofile.collector_id := ln_collector_id;
            pcustomerprofile.credit_analyst_id := ln_credit_analyst;
            pcustomerprofile.account_status := g_acct_status;
            pcustomerprofile.tolerance := 0;
            pcustacctsiteuserec.location :=
               SUBSTR (rec_sites.job_site_name, 1, 35);
            pcustacctsiteuserec.attribute1 := rec_sites.yard_job_accnt_project;
            pcustomerprofile.credit_classification := 'MODERATE';

            BEGIN
               SELECT B.profile_class_id
                 INTO pcustomerprofile.profile_class_id
                 FROM XXWC.XXWC_AHH_PROFILE_CLASSES_T A,
                      HZ_CUST_PROFILE_CLASSES B
                WHERE     A.CUSTOMER_NUM = rec_sites.customer_number
                      AND UPPER (TRIM (a.profile_class_name)) =
                             UPPER (b.name)
                      AND ROWNUM = 1;
            EXCEPTION
               WHEN OTHERS
               THEN
                  BEGIN
                     SELECT PROFILE_CLASS_ID
                       INTO pcustomerprofile.profile_class_id
                       FROM HZ_CUST_PROFILE_CLASSES
                      WHERE NAME = 'Contractor - Non Key';
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        pcustomerprofile.profile_class_id := NULL;
                  END;
            END;

            hz_cust_account_site_v2pub.create_cust_site_use (
               p_init_msg_list          => 'T',
               p_cust_site_use_rec      => pcustacctsiteuserec,
               p_customer_profile_rec   => pcustomerprofile,
               p_create_profile         => 'T',
               p_create_profile_amt     => 'T',
               x_site_use_id            => o_cust_acct_site_use_id,
               x_return_status          => o_ret_status,
               x_msg_count              => o_msg_count,
               x_msg_data               => o_msg_data);

            --ln_ship_site_use_id := o_cust_acct_site_use_id;

            IF o_ret_status <> 'S'
            THEN
               return_msg (o_msg_count, o_msg_data, o_return_msg);
               x_msg_data := 'Account Site Creation(SHIPTO): ' || o_return_msg;
               debug (p_customer_number, x_msg_data);
               x_ret_status := o_ret_status;
               ROLLBACK TO customer_record;
               GOTO STATUS_UPDATE;
            END IF;

            lvc_sec :=
                  'Cust Acct Site Use for Ship to Creation Process Completed - API Status '
               || o_ret_status;
            debug (p_customer_number, lvc_sec);

            lvc_sec :=
               'Cust Acct Site Use for Ship to Profile Amounts update ';

            pcustproamtrec := NULL;
            o_sprofile_id := NULL;
            o_cust_acct_profile_amt_id := NULL;

            BEGIN
               SELECT cust_account_profile_id
                 INTO o_sprofile_id
                 FROM apps.hz_customer_profiles
                WHERE     cust_account_id = p_cust_account_id
                      AND site_use_id = o_cust_acct_site_use_id;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  o_sprofile_id := NULL;
            END;

            IF o_sprofile_id IS NOT NULL
            THEN
               BEGIN
                  SELECT cust_acct_profile_amt_id, object_version_number
                    INTO o_cust_acct_profile_amt_id, p_object_version_number
                    FROM apps.hz_cust_profile_amts
                   WHERE     cust_account_profile_id = o_sprofile_id
                         AND currency_code = g_currency_code;
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     o_cust_acct_profile_amt_id := NULL;
                     p_object_version_number := NULL;
               END;


               IF NVL (rec_sites.DEFAULT_JOB_CREDIT_LIMIT, 0) = 0
               THEN
                  pcustproamtrec.overall_credit_limit := NULL;
               ELSE
                  pcustproamtrec.overall_credit_limit :=
                     rec_sites.DEFAULT_JOB_CREDIT_LIMIT;
               END IF;

               IF o_cust_acct_profile_amt_id IS NOT NULL
               THEN
                  --Update amount profile
                  pcustproamtrec.cust_acct_profile_amt_id :=
                     o_cust_acct_profile_amt_id;
                  hz_customer_profile_v2pub.update_cust_profile_amt (
                     p_init_msg_list           => 'T',
                     p_cust_profile_amt_rec    => pcustproamtrec,
                     p_object_version_number   => p_object_version_number,
                     x_return_status           => o_ret_status,
                     x_msg_count               => o_msg_count,
                     x_msg_data                => o_msg_data);
               ELSE
                  --Create Profile Amount
                  pcustproamtrec.cust_account_profile_id := o_sprofile_id;
                  pcustproamtrec.cust_account_id := p_cust_account_id;
                  pcustproamtrec.site_use_id := o_cust_acct_site_use_id;
                  pcustproamtrec.currency_code := g_currency_code;
                  pcustproamtrec.created_by_module := g_api_name;
                  hz_customer_profile_v2pub.create_cust_profile_amt (
                     p_init_msg_list              => 'T',
                     p_check_foreign_key          => 'T',
                     p_cust_profile_amt_rec       => pcustproamtrec,
                     x_cust_acct_profile_amt_id   => o_cust_acct_profile_amt_id,
                     x_return_status              => o_ret_status,
                     x_msg_count                  => o_msg_count,
                     x_msg_data                   => o_msg_data);
               END IF;

               IF o_ret_status <> 'S'
               THEN
                  return_msg (o_msg_count, o_msg_data, o_return_msg);
                  x_msg_data :=
                     'Shipto Profile Amount Creation: ' || o_return_msg;
                  debug (p_customer_number, x_msg_data);
                  x_ret_status := o_ret_status;
                  ROLLBACK TO customer_record;
                  GOTO STATUS_UPDATE;
               END IF;

               IF     NVL (o_cust_acct_profile_amt_id, 0) > 0
                  AND rec_sites.DEFAULT_JOB_CREDIT_LIMIT = 0
               THEN
                  BEGIN
                     UPDATE APPS.HZ_CUST_PROFILE_AMTS
                        SET OVERALL_CREDIT_LIMIT = NULL
                      WHERE CUST_ACCT_PROFILE_AMT_ID =
                               o_cust_acct_profile_amt_id;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        NULL;
                  END;
               END IF;
            END IF;

            lvc_sec :=
                  'Profile Amts for Ship to Creation Process Completed - API Status '
               || o_ret_status;
            debug (p_customer_number, lvc_sec);

            lvc_sec := ' Primary Ship TO usage rules';

            o_msg_count := NULL;
            o_msg_data := NULL;
            o_ret_status := NULL;

            hz_credit_usages_cascade_pkg.cascade_credit_usage_rules (
               p_cust_acct_profile_amt_id   => o_cust_acct_profile_amt_id,
               p_cust_profile_id            => o_sprofile_id,
               p_profile_class_amt_id       => NULL,
               p_profile_class_id           => NULL,
               x_return_status              => o_ret_status,
               x_msg_count                  => o_msg_count,
               x_msg_data                   => o_msg_data);

            IF o_ret_status <> 'S'
            THEN
               return_msg (o_msg_count, o_msg_data, o_return_msg);
               x_msg_data :=
                  'Shipto cascade_credit_usage_rules: ' || o_return_msg;
               debug (p_customer_number, x_msg_data);
               x_ret_status := o_ret_status;
               ROLLBACK TO customer_record;
               GOTO STATUS_UPDATE;
            END IF;

            lvc_sec :=
                  'Profile Amts usage Rules for Ship to Creation Process Completed - API Status '
               || o_ret_status;
            debug (p_customer_number, lvc_sec);


           <<STATUS_UPDATE>>
            IF NVL (x_ret_status, 'S') = 'S'
            THEN
               UPDATE XXWC.XXWC_AR_CONV_CUST_ACCT_TBL
                  SET PROCESS_FLAG = 'Y',
                      ORACLE_CUSTOMER_NUMBER = g_cust_acct_no
                WHERE ROWID = rec_sites.row_id;
            ELSE
               UPDATE XXWC.XXWC_AR_CONV_CUST_ACCT_TBL
                  SET PROCESS_FLAG = 'E', PROCESS_MESSAGE = x_msg_data
                WHERE ROWID = rec_sites.row_id;
            END IF;
         EXCEPTION
            WHEN OTHERS
            THEN
               x_msg_data := SQLERRM;
               debug (p_customer_number, x_msg_data);

               UPDATE xxwc.XXWC_AR_CONV_CUST_ACCT_TBL
                  SET process_flag = 'E', PROCESS_message = x_msg_data
                WHERE ROWID = rec_sites.row_id;

               xxcus_error_pkg.xxcus_error_main_api (
                  p_called_from         => g_err_callfrom || '.' || lvc_procedure,
                  p_calling             => lvc_sec,
                  p_ora_error_msg       => SQLERRM,
                  p_error_desc          =>    'Error Occured for '
                                           || p_customer_number
                                           || 'record id in XXWC_AR_CONV_CUST_ACCT_TBL.',
                  p_distribution_list   => g_distro_list,
                  p_module              => g_module);
         END;
      END LOOP;

      COMMIT;
   END;


   PROCEDURE Bill_to_only (p_customer_number   IN VARCHAR2,
                           p_cust_account_id   IN NUMBER,
                           p_party_id          IN NUMBER)
   IS
      -- ==============================================================================================================================
      -- Procedure: bill_to_only
      -- Purpose: To create customer bill to only site.
      -- ==============================================================================================================================
      --  REVISIONS:
      --   Ver        Date        Author               Description
      --  ---------  ---------- ---------------  --------------------------------------------------------------------------------------
      --  1.0        06/06/2018  P.Vamshidhar     TMS#20180308-00290 -AH HARRIS Customer Conversion
      -- ==============================================================================================================================

      CURSOR CUR_MAIN
      IS
         SELECT xaca.RECORD_ID,
                xaca.CUSTOMER_NUMBER,
                UPPER (xaca.CUSTOMER_SITE_NUMBER) CUSTOMER_SITE_NUMBER,
                xaca.BUSINESS_NAME,
                xaca.BUSINESS_PHONE,
                xaca.BUSINESS_FAX,
                xaca.EMAIL_ADDRESS,
                xaca.OUTSIDE_SALES_REP_ID,
                (SELECT ORA_COLL_ID
                   FROM XXWC.XXWC_AHH_CRD_COLL_ANALYST_T b
                  WHERE     UPPER (b.ahh_coll_code) =
                               UPPER (xaca.credit_manager)
                        AND ROWNUM = 1)
                   CREDIT_MANAGER,
                (SELECT ORA_CRD_ANLST_ID
                   FROM XXWC.XXWC_AHH_CRD_COLL_ANALYST_T b
                  WHERE     UPPER (b.ahh_coll_code) =
                               UPPER (xaca.credit_manager)
                        AND ROWNUM = 1)
                   credit_analyst,
                DECODE (xaca.CREDIT_HOLD, 'YES', 'Y', 'N') CREDIT_HOLD,
                NVL (xaca.PREDOMINANT_TRADE, 'UNKNOWN') PREDOMINANT_TRADE,
                SUBSTR (xaca.STATUS, 1, 1) STATUS,
                xaca.CREDIT_LIMIT,
                xaca.DEFAULT_JOB_CREDIT_LIMIT,
                xaca.BILLING_ADDRESS1,
                xaca.BILLING_ADDRESS2,
                xaca.BILLING_CITY,
                xaca.BILLING_COUNTY,
                xaca.BILLING_STATE,
                NVL (xaca.BILLING_COUNTRY, 'US') BILLING_COUNTRY,
                xaca.BILLING_ZIP_CODE,
                xaca.AP_CONTACT_FIRST_NAME,
                xaca.AP_CONTACT_LAST_NAME,
                xaca.AP_PHONE,
                xaca.AP_EMAIL,
                SUBSTR (xaca.JOB_SITE_NAME, 1, 35) JOB_SITE_NAME,
                xaca.purchase_order_required,
                DECODE (SUBSTR (xaca.print_prices_on_order, 1, 1),
                        'Y', '1',
                        '2')
                   print_prices_on_order,
                (SELECT a.ORA_TAX_EXEMP_TYPE
                   FROM XXWC.XXWC_AHH_AR_TAX_EXEMPT_TYPES_T a
                  WHERE     a.cust_num = xaca.customer_number
                        AND UPPER (a.ship_to_site) =
                               UPPER (xaca.customer_site_number))
                   TAX_EXEMPTION,
                (SELECT REPLACE (REPLACE (A.SITE_CLASS, CHR (13), ''),
                                 CHR (10),
                                 '')
                   FROM XXWC.XXWC_CUST_SITE_CLASSIF_TBL a
                  WHERE     a.CUSTOMER_NUMBER = xaca.customer_number
                        AND UPPER (a.CUST_SITE_NUM) =
                               UPPER (xaca.CUSTOMER_SITE_NUMBER)
                        AND ROWNUM = 1)
                   yard_job_accnt_project,
                ROWID ROW_ID,
                TERMS
           FROM XXWC.XXWC_AR_CONV_CUST_ACCT_TBL xaca
          WHERE     xaca.CUSTOMER_NUMBER = P_CUSTOMER_NUMBER
                AND xaca.PROCESS_FLAG = 'Y'
                AND xaca.CUSTOMER_SITE_NUMBER IS NULL;

      lvc_sec                      VARCHAR2 (4000);
      plocationrec                 hz_location_v2pub.location_rec_type;
      ppartysiterec                hz_party_site_v2pub.party_site_rec_type;
      pcustacctsiterec             hz_cust_account_site_v2pub.cust_acct_site_rec_type;
      pcustacctsiteuserec          hz_cust_account_site_v2pub.cust_site_use_rec_type;
      pcustomerprofile             hz_customer_profile_v2pub.customer_profile_rec_type;
      pcustproamtrec               hz_customer_profile_v2pub.cust_profile_amt_rec_type;
      o_location_id                HZ_LOCATIONS.location_id%TYPE;
      x_msg_count                  NUMBER;
      x_msg_data                   VARCHAR2 (32767);
      x_ret_status                 VARCHAR2 (1);
      o_ret_status                 VARCHAR2 (1);
      o_msg_count                  NUMBER;
      o_msg_data                   VARCHAR2 (32767);
      o_return_msg                 VARCHAR2 (32767);
      o_party_site_id              hz_party_sites.party_site_id%TYPE;
      o_party_site_no              hz_party_sites.party_site_number%TYPE;
      o_cust_acct_site_id          hz_cust_acct_sites_all.cust_acct_site_id%TYPE;
      o_cust_acct_site_use_id      hz_cust_site_uses_all.site_use_id%TYPE;
      o_sprofile_id                NUMBER;
      p_object_version_number      NUMBER;
      o_cust_acct_profile_amt_id   NUMBER;
      lvc_procedure                VARCHAR2 (20) := 'BILL_TO_ONLY';
      ln_profile_class_id          NUMBER;
      ln_collector_id              NUMBER;
      ln_credit_analyst            NUMBER;
   BEGIN
      lvc_sec := 'Bill Only Procedure Started';
      debug (p_customer_number, lvc_sec);

      lvc_sec := 'Location Creation';

      SAVEPOINT customer_record;

      FOR rec_main IN cur_main
      LOOP
         BEGIN
            lvc_sec := 'Deriving country code';

            BEGIN
               SELECT TERRITORY_CODE
                 INTO plocationrec.country
                 FROM HR_TERRITORIES_V
                WHERE (   UPPER (TERRITORY_SHORT_NAME) =
                             UPPER (rec_main.BILLING_COUNTRY)
                       OR UPPER (TERRITORY_CODE) =
                             UPPER (rec_main.BILLING_COUNTRY));
            EXCEPTION
               WHEN OTHERS
               THEN
                  x_msg_data := 'Invalid Billing Country';
                  debug (p_customer_number, x_msg_data);
                  x_ret_status := 'E';
                  GOTO STATUS_UPDATE;
            END;

            plocationrec.address1 := rec_main.BILLING_ADDRESS1;
            plocationrec.address2 := rec_main.BILLING_ADDRESS2;
            plocationrec.state := rec_main.BILLING_STATE;
            plocationrec.city := rec_main.BILLING_CITY;
            plocationrec.postal_code := rec_main.BILLING_ZIP_CODE;
            plocationrec.county := rec_main.BILLING_COUNTY;
            plocationrec.created_by_module := g_api_name;
            plocationrec.sales_tax_geocode := NULL;

            hz_location_v2pub.create_location (
               p_init_msg_list   => 'T',
               p_location_rec    => plocationrec,
               x_location_id     => o_location_id,
               x_return_status   => o_ret_status,
               x_msg_count       => o_msg_count,
               x_msg_data        => o_msg_data);

            IF o_ret_status <> 'S'
            THEN
               return_msg (o_msg_count, o_msg_data, o_return_msg);
               x_msg_data :=
                  'Error: Bill to Only Location creation' || o_return_msg;
               debug (p_customer_number, x_msg_data);
               x_ret_status := o_ret_status;
               ROLLBACK TO customer_record;
               GOTO STATUS_UPDATE;
            END IF;

            lvc_sec :=
                  'Location Account for Bill To Only Site Creation Process Completed - API Status'
               || x_ret_status;

            debug (p_customer_number, lvc_sec);

            lvc_sec := 'Bill To Only Party Site Creation';

            x_ret_status := NULL;
            o_msg_count := NULL;
            o_msg_data := NULL;
            o_party_site_id := NULL;
            o_party_site_no := NULL;
            o_sprofile_id := NULL;
            ppartysiterec.created_by_module := g_api_name;
            ppartysiterec.party_id := p_party_id;
            ppartysiterec.location_id := o_location_id;
            ppartysiterec.identifying_address_flag := 'Y';

            hz_party_site_v2pub.create_party_site (
               p_init_msg_list       => 'T',
               p_party_site_rec      => ppartysiterec,
               x_party_site_id       => o_party_site_id,
               x_party_site_number   => o_party_site_no,
               x_return_status       => x_ret_status,
               x_msg_count           => o_msg_count,
               x_msg_data            => o_msg_data);

            IF x_ret_status <> 'S'
            THEN
               return_msg (o_msg_count, o_msg_data, o_return_msg);
               x_msg_data :=
                  'Error:Bill to Only Party Site Creation: ' || o_return_msg;
               debug (p_customer_number, x_msg_data);
               ROLLBACK TO customer_record;
               GOTO STATUS_UPDATE;
            END IF;

            lvc_sec :=
                  'Bill to Only Party Site Creation Process Completed - API Status '
               || x_ret_status;

            debug (p_customer_number, lvc_sec);


            lvc_sec := 'Bill to Only Account Site Creation';

            pcustacctsiterec := NULL;
            x_ret_status := NULL;
            o_msg_count := NULL;
            o_msg_data := NULL;
            pcustacctsiterec.cust_account_id := p_cust_account_id;
            pcustacctsiterec.party_site_id := o_party_site_id;
            pcustacctsiterec.created_by_module := g_api_name;
            pcustacctsiterec.org_id := gn_org_id;
            pcustacctsiterec.attribute3 :=
               SUBSTR (NVL (rec_main.purchase_order_required, 'N'), 1, 1);
            pcustacctsiterec.attribute_category := 'No';
            pcustacctsiterec.attribute1 := rec_main.print_prices_on_order;
            pcustacctsiterec.attribute14 := 'N';
            pcustacctsiterec.attribute15 := rec_main.tax_exemption;
            --pcustacctsiterec.attribute15 := 'None';
            pcustacctsiterec.attribute17 := p_customer_number;

            BEGIN
               SELECT CASE
                         WHEN customer_class_code = 'COMMERCIAL'
                         THEN
                            'PUBLIC'
                         WHEN customer_class_code = 'GOVERNMENT'
                         THEN
                            'PRIVATE'
                         WHEN customer_class_code = 'NON-CONTRACTOR/OTHER'
                         THEN
                            'PUBLIC'
                         WHEN customer_class_code = 'RESELLER'
                         THEN
                            'PUBLIC'
                         WHEN customer_class_code = 'RESIDENTIAL_CONTRACTOR'
                         THEN
                            'RES'
                         WHEN customer_class_code = 'UNKNOWN'
                         THEN
                            NULL
                         ELSE
                            NULL
                      END
                 INTO pcustacctsiterec.customer_category_code
                 FROM hz_cust_accounts
                WHERE cust_account_id = p_cust_account_id;
            EXCEPTION
               WHEN OTHERS
               THEN
                  pcustacctsiterec.customer_category_code := NULL;
            END;

            hz_cust_account_site_v2pub.create_cust_acct_site (
               p_init_msg_list        => 'T',
               p_cust_acct_site_rec   => pcustacctsiterec,
               x_cust_acct_site_id    => o_cust_acct_site_id,
               x_return_status        => x_ret_status,
               x_msg_count            => o_msg_count,
               x_msg_data             => o_msg_data);

            IF x_ret_status <> 'S'
            THEN
               return_msg (o_msg_count, o_msg_data, o_return_msg);
               x_msg_data :=
                  'Error:Bill to Only Customer Account: ' || o_return_msg;
               debug (p_customer_number, x_msg_data);
               ROLLBACK TO customer_record;
               GOTO STATUS_UPDATE;
            END IF;

            lvc_sec :=
                  'Bill To Only Cust Acct Site Creation Process Completed - API Status '
               || x_ret_status;
            debug (p_customer_number, lvc_sec);

            lvc_sec := 'Bill to Only Account Site Use Creation';

            BEGIN
               ln_profile_class_id := NULL;

               SELECT B.profile_class_id
                 INTO ln_profile_class_id
                 FROM XXWC.XXWC_AHH_PROFILE_CLASSES_T A,
                      HZ_CUST_PROFILE_CLASSES B
                WHERE     A.CUSTOMER_NUM = rec_main.customer_number
                      AND UPPER (TRIM (a.profile_class_name)) =
                             UPPER (b.name)
                      AND ROWNUM = 1;
            EXCEPTION
               WHEN OTHERS
               THEN
                  BEGIN
                     SELECT PROFILE_CLASS_ID
                       INTO ln_profile_class_id
                       FROM HZ_CUST_PROFILE_CLASSES
                      WHERE NAME = 'Contractor - Non Key';
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        ln_profile_class_id := NULL;
                  END;
            END;

            pcustacctsiteuserec := NULL;
            x_ret_status := NULL;
            o_msg_count := NULL;
            o_msg_data := NULL;


            BEGIN
               SELECT A.TERM_ID
                 INTO pcustomerprofile.standard_terms
                 FROM RA_TERMS A, XXWC.XXWC_AHH_PAYMENT_TERMS_T B
                WHERE     UPPER (TRIM (B.AHH_TERM_NAME)) =
                             UPPER (TRIM (rec_main.terms))
                      AND UPPER (TRIM (B.TERM_NAME)) = UPPER (A.NAME)
                      AND ROWNUM = 1;
            EXCEPTION
               WHEN OTHERS
               THEN
                  BEGIN
                     SELECT TERM_ID
                       INTO pcustomerprofile.standard_terms
                       FROM RA_TERMS
                      WHERE name = 'N30D';
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        pcustomerprofile.standard_terms := NULL;
                  END;
            END;



            pcustacctsiteuserec.site_use_code := 'BILL_TO';
            pcustacctsiteuserec.cust_acct_site_id := o_cust_acct_site_id;
            pcustacctsiteuserec.attribute1 := 'MSTR';
            pcustacctsiteuserec.primary_flag := 'N';
            pcustacctsiteuserec.location :=
               SUBSTR (rec_main.job_site_name, 1, 35);
            pcustacctsiteuserec.created_by_module := g_api_name;
            pcustomerprofile.credit_checking := 'Y';
            pcustomerprofile.attribute2 := 2;
            pcustomerprofile.attribute3 := 'Y';
            pcustomerprofile.collector_id := ln_collector_id;
            pcustomerprofile.account_status := g_acct_status;
            pcustomerprofile.credit_rating := 'Y';
            pcustomerprofile.tolerance := 0;
            pcustomerprofile.credit_analyst_id := ln_credit_analyst;
            pcustomerprofile.credit_classification := 'MODERATE';
            pcustomerprofile.profile_class_id := ln_profile_class_id;

            BEGIN
               SELECT A.SALESREP_ID
                 INTO pcustacctsiteuserec.primary_salesrep_id
                 FROM RA_SALESREPS_ALL A, XXWC.XXWC_AHH_AM_T B
                WHERE     A.ORG_ID = 162
                      AND UPPER (B.SLSREPOUT) =
                             UPPER (rec_main.OUTSIDE_SALES_REP_ID)
                      AND A.SALESREP_NUMBER = B.SALESREP_ID;
            EXCEPTION
               WHEN OTHERS
               THEN
                  pcustacctsiteuserec.primary_salesrep_id := -3;
            END;


            hz_cust_account_site_v2pub.create_cust_site_use (
               p_init_msg_list          => 'T',
               p_cust_site_use_rec      => pcustacctsiteuserec,
               p_customer_profile_rec   => pcustomerprofile,
               p_create_profile         => 'T',
               p_create_profile_amt     => 'T',
               x_site_use_id            => o_cust_acct_site_use_id,
               x_return_status          => x_ret_status,
               x_msg_count              => o_msg_count,
               x_msg_data               => o_msg_data);

            IF x_ret_status <> 'S'
            THEN
               return_msg (o_msg_count, o_msg_data, o_return_msg);
               x_msg_data :=
                     'Error: Account Site Use Creation(Bill To Only): '
                  || o_return_msg;
               debug (p_customer_number, x_msg_data);
               ROLLBACK TO customer_record;
               GOTO STATUS_UPDATE;
            END IF;

            lvc_sec :=
                  'Bill TO Cust Acct Site Use Creation Process Completed - API Status '
               || x_ret_status;

            debug (p_customer_number, lvc_sec);


            lvc_sec := 'Bill to Only Site Profile Amounts update. ';


            pcustproamtrec := NULL;
            o_sprofile_id := NULL;
            x_ret_status := NULL;
            o_msg_count := NULL;
            o_msg_data := NULL;

            BEGIN
               SELECT cust_account_profile_id
                 INTO o_sprofile_id
                 FROM apps.hz_customer_profiles
                WHERE     cust_account_id = p_cust_account_id
                      AND site_use_id = o_cust_acct_site_use_id;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  o_sprofile_id := NULL;
            END;

            IF o_sprofile_id IS NOT NULL
            THEN
               BEGIN
                  SELECT cust_acct_profile_amt_id, object_version_number
                    INTO o_cust_acct_profile_amt_id, p_object_version_number
                    FROM apps.hz_cust_profile_amts
                   WHERE     cust_account_profile_id = o_sprofile_id
                         AND currency_code = g_currency_code;
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     o_cust_acct_profile_amt_id := NULL;
                     p_object_version_number := NULL;
               END;

               IF NVL (rec_main.default_job_credit_limit, 0) = 0
               THEN
                  pcustproamtrec.overall_credit_limit := NULL;
               ELSE
                  pcustproamtrec.overall_credit_limit :=
                     rec_main.default_job_credit_limit;
               END IF;

               IF o_cust_acct_profile_amt_id IS NOT NULL
               THEN
                  --Update amount profile
                  pcustproamtrec.cust_acct_profile_amt_id :=
                     o_cust_acct_profile_amt_id;
                  -- pcustproamtrec.created_by_module := l_api_name;  -- commented by Vamshi

                  hz_customer_profile_v2pub.update_cust_profile_amt (
                     p_init_msg_list           => 'T',
                     p_cust_profile_amt_rec    => pcustproamtrec,
                     p_object_version_number   => p_object_version_number,
                     x_return_status           => x_ret_status,
                     x_msg_count               => o_msg_count,
                     x_msg_data                => o_msg_data);
               ELSE
                  --Create Profile Amount
                  pcustproamtrec.cust_account_profile_id := o_sprofile_id;
                  pcustproamtrec.cust_account_id := p_cust_account_id;
                  pcustproamtrec.site_use_id := o_cust_acct_site_use_id;
                  pcustproamtrec.currency_code := g_currency_code;
                  pcustproamtrec.created_by_module := g_api_name;
                  hz_customer_profile_v2pub.create_cust_profile_amt (
                     p_init_msg_list              => 'T',
                     p_check_foreign_key          => 'T',
                     p_cust_profile_amt_rec       => pcustproamtrec,
                     x_cust_acct_profile_amt_id   => o_cust_acct_profile_amt_id,
                     x_return_status              => x_ret_status,
                     x_msg_count                  => o_msg_count,
                     x_msg_data                   => o_msg_data);
               END IF;

               IF x_ret_status <> 'S'
               THEN
                  return_msg (o_msg_count, o_msg_data, o_return_msg);
                  x_msg_data :=
                     'Bill To Only Profile Amount Creation: ' || o_return_msg;
                  debug (p_customer_number, x_msg_data);
                  ROLLBACK TO customer_record;
                  GOTO STATUS_UPDATE;
               END IF;

               IF     NVL (o_cust_acct_profile_amt_id, 0) > 0
                  AND rec_main.DEFAULT_JOB_CREDIT_LIMIT = 0
               THEN
                  BEGIN
                     UPDATE APPS.HZ_CUST_PROFILE_AMTS
                        SET OVERALL_CREDIT_LIMIT = NULL
                      WHERE CUST_ACCT_PROFILE_AMT_ID =
                               o_cust_acct_profile_amt_id;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        NULL;
                  END;
               END IF;

               lvc_sec :=
                     'Bill TO Profile Amount Creation Process Completed - API Status '
                  || x_ret_status;
               debug (p_customer_number, lvc_sec);

               lvc_sec := 'Bill to Only Usage Rules - API Status ';

               hz_credit_usages_cascade_pkg.cascade_credit_usage_rules (
                  p_cust_acct_profile_amt_id   => o_cust_acct_profile_amt_id,
                  p_cust_profile_id            => o_sprofile_id,
                  p_profile_class_amt_id       => NULL,
                  p_profile_class_id           => NULL,
                  x_return_status              => x_ret_status,
                  x_msg_count                  => o_msg_count,
                  x_msg_data                   => o_msg_data);

               IF x_ret_status <> 'S'
               THEN
                  return_msg (o_msg_count, o_msg_data, o_return_msg);
                  x_msg_data :=
                        'Bill to Only Profile Amounts Usage Rules: '
                     || o_return_msg;
                  debug (p_customer_number, x_msg_data);
                  ROLLBACK TO customer_record;
                  GOTO STATUS_UPDATE;
               END IF;

               lvc_sec :=
                     'Bill to Only Profile Amt uage rules Completed - API Status '
                  || x_ret_status;

               debug (p_customer_number, lvc_sec);
            END IF;

           <<STATUS_UPDATE>>
            IF NVL (x_ret_status, 'S') = 'S'
            THEN
               UPDATE XXWC.XXWC_AR_CONV_CUST_ACCT_TBL
                  SET PROCESS_FLAG = 'Y',
                      ORACLE_CUSTOMER_NUMBER = g_cust_acct_no
                WHERE ROWID = rec_main.row_id;

               COMMIT;
            ELSE
               UPDATE XXWC.XXWC_AR_CONV_CUST_ACCT_TBL
                  SET PROCESS_FLAG = 'E', PROCESS_MESSAGE = x_msg_data
                WHERE ROWID = rec_main.row_id;
            END IF;
         EXCEPTION
            WHEN OTHERS
            THEN
               x_msg_data := SQLERRM;
               debug (p_customer_number, x_msg_data);

               UPDATE xxwc.XXWC_AR_CONV_CUST_ACCT_TBL
                  SET process_flag = 'E', PROCESS_message = x_msg_data
                WHERE ROWID = rec_main.row_id;

               xxcus_error_pkg.xxcus_error_main_api (
                  p_called_from         => g_err_callfrom || '.' || lvc_procedure,
                  p_calling             => lvc_sec,
                  p_ora_error_msg       => SQLERRM,
                  p_error_desc          =>    'Error Occured for '
                                           || p_customer_number
                                           || 'record id in XXWC_AR_CONV_CUST_ACCT_TBL.',
                  p_distribution_list   => g_distro_list,
                  p_module              => g_module);
         END;
      END LOOP;
   END;
END XXWC_AHH_CUSTOMER_CONV_PKG;
/