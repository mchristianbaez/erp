CREATE OR REPLACE PACKAGE BODY APPS."XXCUSGL_EXTRACT_PKG" IS
  /********************************************************************************
  
  File Name: XXCUSGL_EXTRACT_PKG
  
  PROGRAM TYPE: PL/SQL Package spec and body
  
  PURPOSE: Create Extract detailing JE detail for Audit purposes
  
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)                     DESCRIPTION
  ------- -----------   --------------- ----------------------------------------------------------------------------------
  1.0     10/26/2006    Jason Sloan                   Initial creation of the package
  1.1     07/18/2011    John Bozik                     Updating package for R12 specifics
  1.2     04/08/2016    Balaguru Seshadri      TMS 20160407-00172 / ESMS 322629, EBSPRD migration related fixes FOR GSC objects
  1.3     05/09/2016   Balaguru Seshadri       TMS 20160509-00109 / ESMS 322629, Rollback edits to ver 1.2 FOR GSC objects  
  ********************************************************************************/


  PROCEDURE CREATE_GL_LINE_AUDITNE_EXTRACT(errbuf   OUT VARCHAR2,
                                           retcode  OUT NUMBER,
                                           p_period IN VARCHAR2) IS
  
    --Intialize Variables
    l_sql_statement   VARCHAR2(2000);
    /*l_line_cnt        NUMBER := 0;*/
    l_file_name       VARCHAR2(100);
    l_file_dir        VARCHAR2(100);
    l_file_name_fin   VARCHAR2(100);
    l_file_handle     utl_file.file_type;
    l_sid             VARCHAR2(8);
    l_err_msg         VARCHAR2(2000);
    l_err_code        NUMBER;
    l_processed_count NUMBER := 0;
    l_sec             VARCHAR2(150);
    l_period          gl_balances.period_name%TYPE := p_period; --Will hold the period the extract is to be run for
    l_procedure_name  VARCHAR2(75) := 'XXCUSGL_extract_pkg.CREATE_GL_LINE_AUDIT_NE_EXTRACT';
  
    
    --Start Main Program
  BEGIN
    l_sec := 'In create file name; ';
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
  
    l_period := p_period;
    
    --Run create directory
    --CREATE OR REPLACE DIRECTORY ORACLE_INT_UC4 as '/xx_iface/ebizdev/outbound/uc4';
  
    --Get Database
    SELECT lower(NAME) INTO l_sid FROM v$database;
  
    --l_file_dir      := 'ORACLE_INT_UC4';
    l_file_dir      := '/xx_iface/ebizprd/outbound'; --1.2 --1.3
    --l_file_dir      := '/xx_iface/ebsprd/outbound'; --1.2   --1.3
    l_file_name     := 'TMP_GLLN_AUDIT_' || l_period || '_' ||
                       to_char(SYSDATE, 'YYYYMMDDHHMISS') || '.txt';
    l_file_name_fin := 'GLLN_AUDIT_' || l_period || '_' ||
                       to_char(SYSDATE, 'YYYYMMDDHHMISS') || '.txt';
  
    --Create output file
    l_sec := 'In opening file handle; ';
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
  
    l_file_handle     := utl_file.fopen(l_file_dir, l_file_name, 'w');
   /* l_line_cnt        := 0;*/
    l_processed_count := 0;
  
    l_sec           := 'Truncating Table XXCUSGL_LINE_AUDIT_MVW..';
    l_sql_statement := 'TRUNCATE TABLE XXCUS.XXCUSGL_LINE_AUDIT_MVW';
    EXECUTE IMMEDIATE l_sql_statement;
  
    --Insert Global Header GTT
    l_sec := 'Loading the GL Line table; ';
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
  
    Insert /*+ APPEND */
    into XXCUS.XXCUSGL_LINE_AUDIT_MVW
      (SELECT GL_CODE_COMBINATIONS.SEGMENT1,
              GL_CODE_COMBINATIONS.SEGMENT2,
              GL_CODE_COMBINATIONS.SEGMENT3,
              GL_CODE_COMBINATIONS.SEGMENT4,
              GL_CODE_COMBINATIONS.SEGMENT5,
              GL_CODE_COMBINATIONS.SEGMENT6,
              GL_CODE_COMBINATIONS.SEGMENT7,
              GL_JE_LINES.JE_LINE_NUM,
              GL_JE_LINES.LEDGER_ID,
              GL_JE_LINES.PERIOD_NAME,
              GL_JE_LINES.EFFECTIVE_DATE,
              GL_JE_HEADERS.POSTED_DATE,
              GL_JE_LINES.DESCRIPTION,
              GL_JE_SOURCES_TL.USER_JE_SOURCE_NAME,
              GL_JE_BATCHES.JE_BATCH_ID,
              GL_JE_BATCHES.NAME BATCHNAME, 
              GL_JE_HEADERS.JE_HEADER_ID, 
              GL_JE_HEADERS.NAME HEADERNAME,  
              NVL(PER_ALL_PEOPLE_F.FULL_NAME, UID2.USER_NAME),
              GL_JE_LINES.ENTERED_DR,
              GL_JE_LINES.ENTERED_CR
         FROM GL.GL_CODE_COMBINATIONS GL_CODE_COMBINATIONS,
              GL.GL_JE_HEADERS        GL_JE_HEADERS,
              GL.GL_JE_LINES          GL_JE_LINES,
              GL.GL_JE_SOURCES_TL     GL_JE_SOURCES_TL,
              GL.GL_JE_BATCHES        GL_JE_BATCHES,
              APPLSYS.FND_USER        UID2,
              HR.PER_ALL_PEOPLE_F     PER_ALL_PEOPLE_F
        WHERE GL_CODE_COMBINATIONS.CODE_COMBINATION_ID =
              GL_JE_LINES.CODE_COMBINATION_ID
          AND GL_JE_LINES.JE_HEADER_ID = GL_JE_HEADERS.JE_HEADER_ID
          AND GL_JE_HEADERS.JE_SOURCE = GL_JE_SOURCES_TL.JE_SOURCE_NAME
          AND GL_JE_HEADERS.JE_BATCH_ID = GL_JE_BATCHES.JE_BATCH_ID
          AND GL_JE_LINES.CREATED_BY = UID2.USER_ID
          AND UID2.EMPLOYEE_ID = PER_ALL_PEOPLE_F.PERSON_ID(+)
          AND GL_JE_LINES.LEDGER_ID IN
              (select to_number(description)
                 from apps.fnd_lookup_values
                where lookup_type = 'HDS_GLAUDITEXTRACT'
                  and lookup_code like 'LEDGER%')
          AND GL_JE_HEADERS.CURRENCY_CODE <> 'STAT'
          AND GL_JE_LINES.STATUS = 'P'
          AND PER_ALL_PEOPLE_F.EFFECTIVE_END_DATE(+) =
              TO_DATE('47121231000000', 'YYYYMMDDHH24MISS')
          AND GL_JE_LINES.STAT_AMOUNT IS NULL
          AND GL_JE_BATCHES.ACTUAL_FLAG = 'A'
          AND GL_JE_LINES.PERIOD_NAME = p_period);
  
    COMMIT;
  
    --Cursor to get the HDR lines
    l_sec := 'Writing the GL HIST file; ';
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
  
    FOR c_gl_audit_rec IN (Select * from XXCUS.XXCUSGL_LINE_AUDIT_MVW) LOOP
    
      --Write detail HDR rows
      utl_file.put_line(l_file_handle,
                        c_gl_audit_rec.SEGMENT1 || '|' ||
                         c_gl_audit_rec.SEGMENT2 || '|' ||
                        --c_gl_audit_rec.SEGMENT3 || '|' ||
                         c_gl_audit_rec.SEGMENT4 || '|' ||
                         c_gl_audit_rec.SEGMENT5 || '|' ||
                        --c_gl_audit_rec.SEGMENT6 || '|' ||
                        --c_gl_audit_rec.SEGMENT7 || '|' ||
                         c_gl_audit_rec.JE_LINE_NUM || '|' ||
                         c_gl_audit_rec.LEDGER_ID || '|' ||
                         c_gl_audit_rec.PERIOD_NAME || '|' ||
                         c_gl_audit_rec.EFFECTIVE_DATE || '|' ||
                         c_gl_audit_rec.POSTED_DATE || '|' ||
                         c_gl_audit_rec.DESCRIPTION || '|' ||
                         c_gl_audit_rec.USER_JE_SOURCE_NAME || '|' ||
                         c_gl_audit_rec.JE_BATCH_ID || '|' ||
                         c_gl_audit_rec.BATCHNAME || '|' ||  
                         c_gl_audit_rec.JE_HEADER_ID || '|' || 
                         c_gl_audit_rec.HEADERNAME || '|' ||  
                         c_gl_audit_rec.FULL_NAME || '|' ||
                         c_gl_audit_rec.ENTERED_DR || '|' ||
                         c_gl_audit_rec.ENTERED_CR);
    
      l_processed_count := l_processed_count + 1;
    END LOOP;
  
    --Close the file
    l_sec := 'Closing the GL Line Audit file; ';
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
  
    utl_file.fclose(l_file_handle);
  
    COMMIT;
  
    utl_file.frename(l_file_dir, l_file_name, l_file_dir, l_file_name_fin);
  
    COMMIT;
  
  EXCEPTION
    WHEN utl_file.invalid_operation THEN
      raise_application_error(-20110,
                              'CREATE_GL_EXTRACT_FILE: Invalid Operation ' ||
                              l_sec);
    WHEN utl_file.invalid_filehandle THEN
      raise_application_error(-20120,
                              'CREATE_GL_EXTRACT_FILE: Invalid File Handle ' ||
                              l_sec);
    WHEN utl_file.write_error THEN
      raise_application_error(-20130,
                              'CREATE_GL_EXTRACT_FILE: Write Error ' ||
                              l_sec);
    WHEN program_error THEN
      ROLLBACK;
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := 'Error in program ' || l_procedure_name || ' Error: ' ||
                    l_sec;
      l_err_msg  := l_err_msg || ' ERROR ' || substr(SQLERRM, 1, 2000);
    
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      utl_file.fclose(l_file_handle);
      utl_file.fremove(l_file_dir, l_file_name);
      retcode := l_err_code;
      errbuf  := l_err_msg;
    WHEN OTHERS THEN
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := l_sec || l_err_msg || ' ERROR ' || SQLCODE ||
                    substr(SQLERRM, 1, 2000);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      utl_file.fclose(l_file_handle);
      utl_file.fremove(l_file_dir, l_file_name);
      retcode := l_err_code;
      errbuf  := l_err_msg;
    
  END CREATE_GL_LINE_AUDITNE_EXTRACT;
END XXCUSGL_extract_pkg;
/
show errors
/