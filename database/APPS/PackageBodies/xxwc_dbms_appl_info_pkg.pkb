CREATE OR REPLACE PACKAGE BODY APPS.xxwc_dbms_appl_info_pkg
AS
/***************************************************************************
  $Header xxwc_dbms_appl_info_pkg $
  Module Name: xxwc_dbms_appl_info_pkg.pkb

  PURPOSE:   This package is called by the concurrent programs,
             SQL scripts, reports and forms to set the module and action
             for a given session.

  REVISIONS:
  Ver        Date        Author             Description
  ---------  ----------  ---------------   ---------------------------------
   1.0       08/02/2017  Ashwin Sridhar    Initial Build - TMS#20170116-00178
****************************************************************************/
/*************************************************************************
  Procedure : set_module_action_p

  PURPOSE:   This procedure is used to set the module and action for a 
             given session.
  Parameter:
         IN
             p_module    -- Module Name
             p_action    -- Action Name
************************************************************************/
PROCEDURE set_module_action_p (p_module IN VARCHAR2
                              ,p_action IN VARCHAR2)
  IS
   
--Local Variable Declaration...
lv_module VARCHAR2(100);
lv_action VARCHAR2(100);
   
BEGIN
  
  fnd_file.put_line(fnd_file.LOG, 'Reading the current module');
  DBMS_OUTPUT.put_line('Reading the current module');
  
  dbms_application_info.read_module(lv_module
                                   ,lv_action);
   
  fnd_file.put_line(fnd_file.LOG, 'Module is '||lv_module||' and Action is '||lv_action);
  DBMS_OUTPUT.put_line('Module is '||lv_module||' and Action is '||lv_action);
   
  fnd_file.put_line(fnd_file.LOG, 'Setting the custom module and action');
  DBMS_OUTPUT.put_line('Setting the custom module and action');
  
  dbms_application_info.set_module(p_module
                                  ,p_action);

  fnd_file.put_line(fnd_file.LOG, 'Custom Module is '||p_module||' and custom Action is '||p_action);
  DBMS_OUTPUT.put_line('Custom Module is '||p_module||' and custom Action is '||p_action);
 
EXCEPTION
WHEN others THEN

  fnd_file.put_line(fnd_file.LOG,'Unable to set the module '||SQLERRM);
  DBMS_OUTPUT.put_line('Unable to set the module '||SQLERRM);

END set_module_action_p;

END xxwc_dbms_appl_info_pkg;
/