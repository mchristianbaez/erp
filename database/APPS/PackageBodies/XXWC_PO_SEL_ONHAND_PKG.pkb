CREATE OR REPLACE PACKAGE BODY apps.xxwc_po_sel_onhand_pkg AS 
   /*************************************************************************
      $Header XXWC_PO_SEL_ONHAND_PKG $
      Module Name: XXWC_PO_SEL_ONHAND_PKG.PKB
   
      PURPOSE:   Package used for loading on-hand information
   
      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------
      1.0        02/12/2015  Gopi Damuluri           TMS# 20150209-00077 Initial Version
   ****************************************************************************/
   --Email Defaults
   g_dflt_email              fnd_user.email_address%TYPE := 'HDSOracleDevelopers@hdsupply.com';

   /*************************************************************************
      PROCEDURE Name: load_onhand_info
   
      PURPOSE:   Procedure to load onhand information into temporary table.

      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------
      1.0        02/12/2015  Gopi Damuluri             Initial Version   
   ****************************************************************************/
   PROCEDURE load_onhand_info (p_errbuf                 OUT  VARCHAR2,
                                                p_retcode                OUT  NUMBER)
          IS
       l_sec               VARCHAR2(100);
       l_program_err       EXCEPTION;
      BEGIN
           fnd_file.put_line (fnd_file.LOG,'Start Time - '||TO_CHAR(SYSDATE,'DD-MON-YYYY HH24:MI:SS'));

        --------------------------------------------------------------------------------
        -- 0. Truncate Staging table - XXWC_PO_SEL_ONHAND_TBL 
        --------------------------------------------------------------------------------
        l_sec := '0. Load Staging table - XXWC_PO_SEL_ONHAND_TBL with Item details';
        EXECUTE IMMEDIATE 'TRUNCATE TABLE xxwc.xxwc_po_sel_onhand_tbl';
        fnd_file.put_line (fnd_file.LOG,'After Step# 0 - '||TO_CHAR(SYSDATE,'DD-MON-YYYY HH24:MI:SS'));

        --------------------------------------------------------------------------------
        -- 1. Load Staging table - XXWC_PO_SEL_ONHAND_TBL with Item details
        --------------------------------------------------------------------------------
        l_sec := '1. Load Staging table - XXWC_PO_SEL_ONHAND_TBL with Item details';
        BEGIN
           INSERT INTO xxwc.xxwc_po_sel_onhand_tbl(inventory_item_id, organization_id, segment1, description, amu, min, max, primary_unit_of_measure)
           SELECT /*+ MTL_SYSTEM_ITEMS_B_U1*/inventory_item_id
                , organization_id
                , segment1
                , description
                , attribute20 amu
                , NVL(min_minmax_quantity, 0)
                , NVL(max_minmax_quantity, 0)
                , primary_unit_of_measure
             FROM mtl_system_items_b msi
            WHERE 1 = 1
              AND EXISTS(SELECT 'x'
                           FROM mtl_onhand_quantities   moq
                          WHERE msi.organization_id   = moq.organization_id
                            AND msi.inventory_item_id = moq.inventory_item_id);
        EXCEPTION
        WHEN OTHERS THEN
           RAISE l_program_err;
        END;

        DBMS_STATS.gather_index_stats (ownname => 'XXWC', indname => 'XXWC_PO_SEL_ONHAND_N1');
        DBMS_STATS.gather_index_stats (ownname => 'XXWC', indname => 'XXWC_PO_SEL_ONHAND_N2');

        fnd_file.put_line (fnd_file.LOG,'After Step# 1 - '||TO_CHAR(SYSDATE,'DD-MON-YYYY HH24:MI:SS'));
        --------------------------------------------------------------------------------
        -- 2. Update Organization_code and Pricing_region
        --------------------------------------------------------------------------------
        l_sec := '2. Update Organization_code and Pricing_region';
        BEGIN
           UPDATE xxwc.xxwc_po_sel_onhand_tbl stg
              SET (organization_code, pricing_region) = (SELECT organization_code, attribute9 FROM mtl_parameters mp WHERE stg.organization_id = mp.organization_id)
            WHERE 1 = 1;
        EXCEPTION
        WHEN OTHERS THEN
           RAISE l_program_err;
        END;
        fnd_file.put_line (fnd_file.LOG,'After Step# 2 - '||TO_CHAR(SYSDATE,'DD-MON-YYYY HH24:MI:SS'));

        --------------------------------------------------------------------------------
        -- 3. Update On-Hand Quantities Greater Than 90
        --------------------------------------------------------------------------------
        l_sec := '3. Update On-Hand Quantities Greater Than 90';
        BEGIN
           UPDATE xxwc.xxwc_po_sel_onhand_tbl stg
              SET on_hand_gt_90 = (SELECT NVL(SUM(primary_transaction_quantity),0)
                                     FROM xxwcinv_inv_onhand_mv mv
                                    WHERE mv.inventory_item_id         = stg.inventory_item_id
                                      AND mv.organization_id           = stg.organization_id
                                      AND trunc(mv.orig_date_received) < trunc(sysdate-90)
                                  )
            WHERE 1 = 1;
        EXCEPTION
        WHEN OTHERS THEN
           RAISE l_program_err;
        END;
        fnd_file.put_line (fnd_file.LOG,'After Step# 3 - '||TO_CHAR(SYSDATE,'DD-MON-YYYY HH24:MI:SS'));

        --------------------------------------------------------------------------------
        -- 4. Update On-Hand Quantities Greater Than 180
        --------------------------------------------------------------------------------
        l_sec := '4. Update On-Hand Quantities Greater Than 180';
        BEGIN
           UPDATE xxwc.xxwc_po_sel_onhand_tbl stg
              SET on_hand_gt_180 = (SELECT NVL(SUM(primary_transaction_quantity),0)
                                      FROM xxwcinv_inv_onhand_mv mv
                                     WHERE mv.inventory_item_id         = stg.inventory_item_id
                                       AND mv.organization_id           = stg.organization_id
                                       AND trunc(mv.orig_date_received) < trunc(sysdate-180)
                                      )
            WHERE 1 = 1;
        EXCEPTION
        WHEN OTHERS THEN
           RAISE l_program_err;
        END;
        fnd_file.put_line (fnd_file.LOG,'After Step# 4 - '||TO_CHAR(SYSDATE,'DD-MON-YYYY HH24:MI:SS'));

        --------------------------------------------------------------------------------
        -- 5. Update On-Hand Quantities Greater Than 270
        --------------------------------------------------------------------------------
        l_sec := '5. Update On-Hand Quantities Greater Than 270';
        BEGIN
           UPDATE xxwc.xxwc_po_sel_onhand_tbl stg
              SET on_hand_gt_270 = (SELECT NVL(SUM(primary_transaction_quantity),0)
                                      FROM xxwcinv_inv_onhand_mv mv
                                     WHERE mv.inventory_item_id         = stg.inventory_item_id
                                       AND mv.organization_id           = stg.organization_id
                                       AND trunc(mv.orig_date_received) < trunc(sysdate-270)
                                   )
            WHERE 1 = 1;
        EXCEPTION
        WHEN OTHERS THEN
           RAISE l_program_err;
        END;
        
        fnd_file.put_line (fnd_file.LOG,'After Step# 5 - '||TO_CHAR(SYSDATE,'DD-MON-YYYY HH24:MI:SS'));
COMMIT;

    EXCEPTION
    WHEN l_program_err THEN
      p_errbuf := 'Error';
      p_retcode := '2';
      fnd_file.put_line (fnd_file.LOG,SQLERRM);
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => 'XXWC_PO_SEL_ONHAND_PKG.LOAD_ONHAND_INFO'
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => substr(SQLERRM, 1, 2000)
                                          ,p_error_desc        => 'Error in LOAD_ONHAND_INFO procedure, Program Error'
                                          ,p_distribution_list => g_dflt_email
                                          ,p_module            => 'XXWC');
    WHEN OTHERS THEN
      p_errbuf := 'Error';
      p_retcode := '2';
      fnd_file.put_line (fnd_file.LOG,SQLERRM);
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => 'XXWC_PO_SEL_ONHAND_PKG.LOAD_ONHAND_INFO'
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => substr(SQLERRM, 1, 2000)
                                          ,p_error_desc        => 'Error in LOAD_ONHAND_INFO procedure, When Others Exception'
                                          ,p_distribution_list => g_dflt_email
                                          ,p_module            => 'XXWC');
    END load_onhand_info;

END xxwc_po_sel_onhand_pkg;
/