CREATE OR REPLACE PACKAGE BODY APPS.XXCUS_OZF_PROMO_OFFERS_PVT IS

  /*******************************************************************************
  * Procedure:
  * Description: Package will check all HDS custom validations well before oracle
  *             oracle starts checking the standard offer setup validations.
  *             This custom package is called within the oracle standard package
  *             OZF_Promotional_Offers_PVT.create_offers procedure.
  *
  * Dependencies: Oracle standard package OZF_Promotional_Offers_PVT.create_offers
  *
  * Note: How the function works?
  *
  *  Validations are performed in a sequence. When a failure occurs
  *  at each level, we raise the error message until there are no more failed
  *  HDS custom validations. We will call the function CHECK_HDS_VALIDATIONS
  *  only once.The function inturn can invoke as many validations as required. 
  *  For each validation we give user the ability to turn the validation on and off 
  *  using a custom profile created for each validation.
  * 
  *
  * Custom error messages: Error codes are setup using Application Developer responsibility
  *                       / Application / Messages for each validation that starts with XXCUS_OZF.
  *
  *                      Message description can be changed at any time without modifying the code.
  *
  *                      Example: XXCUS_OZF_CHECK_UNTIL_YEAR_DFF.
  *
  * WARNING: If for any reason the oracle package OZF_Promotional_Offers_PVT.create_offers   
  *         is recompiled using a patch or something else, please scan the package body
  *         script to make sure the custom XXCUS_OZF_PROMOTIONAL_OFFERS_PVT.CHECK_HDS_VALIDATIONS
  *         is invoked otherwise the offer entry will be missed altogether.
  *  
  HISTORY
  ===============================================================================
  ESMS / RFC      VERSION DATE          AUTHOR(S)        DESCRIPTION
  -------------   ------- -----------   ---------------  -----------------------------------------
  ESMS 273702      1.0   04/28/2015   Balaguru Seshadri Initial creation of the procedure
  ********************************************************************************/
  --
  PROCEDURE error_message
    (   p_message_name VARCHAR2,
        p_token_name   VARCHAR2 := NULL,
        P_token_value  VARCHAR2 := NULL
    )
    IS
    BEGIN
       IF FND_MSG_PUB.check_msg_level(FND_MSG_PUB.g_msg_lvl_error) THEN
          FND_MESSAGE.set_name('OZF', p_message_name);
          IF p_token_name IS NOT NULL THEN
             FND_MESSAGE.set_token(p_token_name, p_token_value);
          END IF;
          FND_MSG_PUB.add;
       END IF;
    END error_message;
    --
  PROCEDURE display_messages IS
       l_count  NUMBER;
       l_msg    VARCHAR2(2000);
  BEGIN
       l_count := FND_MSG_PUB.count_msg;
       FOR i IN 1 .. l_count LOOP
          l_msg := FND_MSG_PUB.get(i, FND_API.g_false);
       END LOOP;
  END display_messages;  
    --
  PROCEDURE CHECK_HDS_VALIDATIONS_LEVEL1 
             (
               p_list_header_id  IN  NUMBER              
              ,p_vol_offer_type  IN  VARCHAR2
              ,p_status          OUT VARCHAR2
             ) IS
  --
  l_validate_until_year      VARCHAR2(1) :='N';
  l_validate_global_flag     VARCHAR2(1) :='N';
  l_validate_incentive_type  VARCHAR2(1) :='N';  
  --
  l_global_flag              VARCHAR2(1)   :='N'; 
  l_until_year               VARCHAR2(150) :=Null; 
  l_yes_flag                 VARCHAR2(1)   :='Y'; 
  -- 
  l_incentive_type           VARCHAR2(30)   :='ACCRUAL';
  l_msg_count                NUMBER;
  l_msg_data                 VARCHAR2(2000);
  --    
  l_status VARCHAR2(1) :='F'; --Fail
  --        
  BEGIN
   --
   -- Check if we need to validate if offer is global.
   --
   BEGIN
    --
    SELECT FND_PROFILE.VALUE('XXCUS_CHECK_OFFER_GLOBAL_FLAG') 
    INTO   l_validate_global_flag 
    FROM   DUAL;
    --
   EXCEPTION
       WHEN OTHERS THEN
        --
        OZF_Utility_PVT.Error_Message(p_message_name => 'XXCUS_OZF_OFFER_ENTRY_VAL_PROF');
        --
        FND_MSG_PUB.Count_And_Get (
                p_encoded => FND_API.G_TRUE,
                p_count => l_msg_count,
                p_data  => l_msg_data
         );
   END;
   --   
   -- Check if we need to validate incentive type to always be Accrual.
   --
   BEGIN
    --
    SELECT FND_PROFILE.VALUE('XXCUS_OZF_CHECK_INCENTIVE_TYPE') 
    INTO   l_validate_incentive_type 
    FROM   DUAL;
    --
   EXCEPTION
     WHEN OTHERS THEN
        --
        OZF_Utility_PVT.Error_Message(p_message_name => 'XXCUS_OZF_OFFER_ENTRY_VAL_PROF');
        --
        FND_MSG_PUB.Count_And_Get (
                p_encoded => FND_API.G_TRUE,
                p_count => l_msg_count,
                p_data  => l_msg_data
         );  
   END;
   --   
   -- based on the values fetched from the profiles perform the validation
   --
   BEGIN
    -- 
    select global_flag
    into   l_global_flag
    from   qp_list_headers_b
    where  1 =1
      and  list_header_id =p_list_header_id;
    --
   EXCEPTION
    WHEN NO_DATA_FOUND THEN
     OZF_Utility_PVT.Error_Message(p_message_name => 'XXCUS_OZF_ERROR_IN_QP');
     RAISE FND_API.G_EXC_ERROR;
    WHEN OTHERS THEN
      OZF_Utility_PVT.Error_Message(p_message_name => 'XXCUS_OZF_ERROR_IN_QP');
      RAISE FND_API.G_EXC_ERROR;        
   END;
   --
   IF (l_global_flag =l_yes_flag AND l_validate_global_flag =l_yes_flag) THEN
     OZF_Utility_PVT.Error_Message(p_message_name => 'XXCUS_OZF_STOP_GLOBAL_OFFERS');
     RAISE FND_API.G_EXC_ERROR;   
   END IF;
   --
   IF ((p_vol_offer_type <> l_incentive_type) AND l_validate_incentive_type =l_yes_flag) THEN
     OZF_Utility_PVT.Error_Message(p_message_name => 'XXCUS_OZF_VALID_VOL_OFFER_TYPE');
     RAISE FND_API.G_EXC_ERROR;   
   END IF;   
   --
   l_status :='T';
   --
  EXCEPTION
   WHEN FND_API.G_EXC_ERROR THEN
     -- Standard call to get message count and if count=1, get the message
     FND_MSG_PUB.Count_And_Get (
            p_encoded => FND_API.G_TRUE,
            p_count => l_msg_count,
            p_data  => l_msg_data
     );
    l_status :='F';  
    RAISE;
   WHEN OTHERS THEN    
     FND_MSG_PUB.Count_And_Get (
            p_encoded => FND_API.G_TRUE,
            p_count => l_msg_count,
            p_data  => l_msg_data
     );
    l_status :='F';
    RAISE;   
  END CHECK_HDS_VALIDATIONS_LEVEL1;
  --
  --
  PROCEDURE CHECK_HDS_VALIDATIONS_LEVEL2 
             (
               p_list_header_id  IN  NUMBER
              ,p_status          OUT VARCHAR2
             ) IS
  --
  l_validate_until_year      VARCHAR2(1) :='N';
  l_validate_global_flag     VARCHAR2(1) :='N';
  l_validate_incentive_type  VARCHAR2(1) :='N';  
  --
  l_auto_renewal             VARCHAR2(10) :=Null; 
  l_until_year               VARCHAR2(150) :=Null; 
  l_yes_flag                 VARCHAR2(1)   :='Y'; 
  l_NO_flag                  VARCHAR2(1)   :='N';
  -- 
  l_incentive_type           VARCHAR2(30)   :='ACCRUAL';
  l_msg_count                NUMBER;
  l_msg_data                 VARCHAR2(2000);
  --    
  l_status VARCHAR2(1) :='F'; --Fail
  --        
  BEGIN 
   --
   -- Check if we need to validate until year descriptive flex field.
   --
   BEGIN
    --
    SELECT FND_PROFILE.VALUE('XXCUS_CHECK_OFFER_UNTIL_YEAR') 
    INTO   l_VALIDATE_UNTIL_YEAR 
    FROM   DUAL;
    --
   EXCEPTION
       WHEN OTHERS THEN
        --
        OZF_Utility_PVT.Error_Message(p_message_name => 'XXCUS_OZF_OFFER_ENTRY_VAL_PROF');
        --
        FND_MSG_PUB.Count_And_Get (
                p_encoded => FND_API.G_TRUE,
                p_count => l_msg_count,
                p_data  => l_msg_data
         );
   END;
   --
   -- based on the values fetched from the profiles perform the validation
   --
   BEGIN
    -- 
    select attribute2, nvl(attribute1, 'N')
    into   l_until_year, l_auto_renewal 
    from   qp_list_headers_b
    where  1 =1
      and  list_header_id =p_list_header_id;
    --
   EXCEPTION
    WHEN NO_DATA_FOUND THEN
     OZF_Utility_PVT.Error_Message(p_message_name => 'XXCUS_OZF_ERROR_IN_QP');
     RAISE FND_API.G_EXC_ERROR;
    WHEN OTHERS THEN
      OZF_Utility_PVT.Error_Message(p_message_name => 'XXCUS_OZF_ERROR_IN_QP');
      RAISE FND_API.G_EXC_ERROR;        
   END;
   --
   IF (l_until_year IS NOT NULL AND l_auto_renewal =l_NO_flag AND l_validate_until_year =l_yes_flag) THEN
     OZF_Utility_PVT.Error_Message(p_message_name => 'XXCUS_OZF_CHECK_UNTIL_YEAR_DFF');
     RAISE FND_API.G_EXC_ERROR;   
   END IF;
   --
   l_status :='T';
   --
  EXCEPTION
   WHEN FND_API.G_EXC_ERROR THEN
     -- Standard call to get message count and if count=1, get the message
     FND_MSG_PUB.Count_And_Get (
            p_encoded => FND_API.G_TRUE,
            p_count => l_msg_count,
            p_data  => l_msg_data
     );
    l_status :='F';  
    RAISE;
   WHEN OTHERS THEN    
     FND_MSG_PUB.Count_And_Get (
            p_encoded => FND_API.G_TRUE,
            p_count => l_msg_count,
            p_data  => l_msg_data
     );
    l_status :='F';
    RAISE;   
  END CHECK_HDS_VALIDATIONS_LEVEL2;
  --  
END XXCUS_OZF_PROMO_OFFERS_PVT;
/