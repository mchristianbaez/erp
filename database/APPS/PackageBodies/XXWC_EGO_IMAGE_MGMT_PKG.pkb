CREATE OR REPLACE PACKAGE BODY APPS.xxwc_ego_image_mgmt_pkg
/********************************************************************************
File Name: XXWC_EGO_IMAGE_MGMT_PKG
PROGRAM TYPE: PL/SQL Package spec and body
HISTORY
PURPOSE: Procedures and functions for creating the WCS extracts
==================================================================================
VERSION DATE          AUTHOR(S)               DESCRIPTION
------- -----------   --------------- --------------------------------------------
1.0     25-Feb-2014   Praveen Pawar           Initial creation of the package spec
*********************************************************************************/
AS
   PROCEDURE sync_image_exists_attr (
      errbuf    OUT NOCOPY   VARCHAR2,
      retcode   OUT NOCOPY   VARCHAR2
   )
   IS
      -- Variable declaration --
      ln_organization_id            mtl_parameters.organization_id%TYPE
                                                                      := NULL;
      lv_fullsize_image_url         ego_mtl_sy_items_ext_tl.tl_ext_attr1%TYPE
                                                                      := NULL;
      lv_thumbnail_image_url        ego_mtl_sy_items_ext_tl.tl_ext_attr1%TYPE
                                                                      := NULL;
      lv_fullsize_image_path        ego_mtl_sy_items_ext_tl.tl_ext_attr1%TYPE
                        := fnd_profile.VALUE ('XXWC_EGO_FULLSIZE_IMAGE_PATH');
      lv_thumbnail_image_path       ego_mtl_sy_items_ext_tl.tl_ext_attr1%TYPE
                       := fnd_profile.VALUE ('XXWC_EGO_THUMBNAIL_IMAGE_PATH');
      l_num_resp_id                 NUMBER;
      l_num_app_id                  NUMBER;
      l_tbl_rec_cnt                 NUMBER                               := 0;
      ln_image_mgmt_attr_grp_id     NUMBER                            := NULL;
      ln_rec_count                  NUMBER                               := 0;
      ln_data_set_id                NUMBER                               := 0;
      ln_min_data_set_id            NUMBER                               := 0;
      ln_max_data_set_id            NUMBER                               := 0;
      l_row_identifier              NUMBER                               := 0;
      cv_batch_size                 NUMBER                            := 5000;
      ln_inventory_item_id          NUMBER                            := NULL;
      ln_retcode                    NUMBER                            := NULL;
      lv_errbuff                    VARCHAR2 (4000)                   := NULL;
      lv_error_message              VARCHAR2 (4000)                   := NULL;
      cv_ego_itemmgmt_group         VARCHAR2 (60)     := 'EGO_ITEMMGMT_GROUP';
      cv_attr_fullsize_image        VARCHAR2 (40)    := 'XXWC_A_F_1_URL_ATTR';
      cv_attr_fullsize_image_chk    VARCHAR2 (40) := 'XXWC_A_F_1_IMAGE_CHECK';
      cv_attr_thumbnail_image       VARCHAR2 (40)    := 'XXWC_A_T_1_URL_ATTR';
      cv_attr_thumbnail_image_chk   VARCHAR2 (40) := 'XXWC_A_T_1_IMAGE_CHECK';
      lv_calling                    VARCHAR2 (1000)                   := NULL;
      ln_request_id                 NUMBER      := fnd_global.conc_request_id;
      cv_dist_list                  VARCHAR2 (100)
                                        := 'HDSOracleDevelopers@hdsupply.com';
      l_main_exception              EXCEPTION;

      -- Cursor declaration --
      CURSOR csr_item_image_info_sync_y
      IS
         SELECT   inventory_item_id, image_filename, image_filetype,
                  image_exists
             FROM (SELECT DISTINCT wmv.inventory_item_id inventory_item_id,
                                   TRIM
                                      (ifn.xxwc_image_filename_attr
                                      ) image_filename,
                                   TRIM
                                      (ifn.xxwc_image_filetype_attr
                                      ) image_filetype,
                                   'Y' image_exists
                              FROM apps.xxwc_white_c_m_agv wmv,
                                   xxwc.xxwc_ego_image_filenames ifn
                             WHERE UPPER (TRIM (wmv.xxwc_a_f_1_attr)) =
                                      UPPER
                                           (TRIM (ifn.xxwc_image_filename_attr)
                                           )
                               AND ifn.xxwc_image_filetype_attr = 'F'
                               AND (   EXISTS (
                                          SELECT 1
                                            FROM apps.ego_xxwc_image_manag_agv imv
                                           WHERE wmv.inventory_item_id =
                                                         imv.inventory_item_id
                                             AND wmv.organization_id =
                                                           imv.organization_id
                                             AND NVL
                                                    (imv.xxwc_a_f_1_image_check,
                                                     'N'
                                                    ) = 'N')
                                    OR (NOT EXISTS (
                                           SELECT 1
                                             FROM apps.ego_xxwc_image_manag_agv imv
                                            WHERE wmv.inventory_item_id =
                                                         imv.inventory_item_id
                                              AND wmv.organization_id =
                                                           imv.organization_id)
                                       )
                                   )
                   UNION
                   SELECT DISTINCT wmv.inventory_item_id inventory_item_id,
                                   TRIM
                                      (ifn.xxwc_image_filename_attr
                                      ) image_filename,
                                   TRIM
                                      (ifn.xxwc_image_filetype_attr
                                      ) image_filetype,
                                   'Y' image_exists
                              FROM apps.xxwc_white_c_m_agv wmv,
                                   xxwc.xxwc_ego_image_filenames ifn
                             WHERE UPPER (TRIM (wmv.xxwc_a_t_1_attr)) =
                                      UPPER
                                           (TRIM (ifn.xxwc_image_filename_attr)
                                           )
                               AND ifn.xxwc_image_filetype_attr = 'T'
                               AND (   EXISTS (
                                          SELECT 1
                                            FROM apps.ego_xxwc_image_manag_agv imv
                                           WHERE wmv.inventory_item_id =
                                                         imv.inventory_item_id
                                             AND wmv.organization_id =
                                                           imv.organization_id
                                             AND NVL
                                                    (imv.xxwc_a_t_1_image_check,
                                                     'N'
                                                    ) = 'N')
                                    OR (NOT EXISTS (
                                           SELECT 1
                                             FROM apps.ego_xxwc_image_manag_agv imv
                                            WHERE wmv.inventory_item_id =
                                                         imv.inventory_item_id
                                              AND wmv.organization_id =
                                                           imv.organization_id)
                                       )
                                   ))
         ORDER BY inventory_item_id;

      CURSOR csr_item_image_info_sync_n
      IS
         SELECT   inventory_item_id, image_filename, image_filetype,
                  image_exists
             FROM (SELECT wmv.inventory_item_id inventory_item_id,
                          wmv.xxwc_a_f_1_attr image_filename,
                          'F' image_filetype, 'N' image_exists
                     FROM apps.ego_xxwc_image_manag_agv imv,
                          apps.xxwc_white_c_m_agv wmv
                    WHERE imv.xxwc_a_f_1_image_check = 'Y'
                      AND wmv.inventory_item_id = imv.inventory_item_id
                      AND wmv.organization_id = imv.organization_id
                      AND UPPER (TRIM (wmv.xxwc_a_f_1_attr)) NOT IN (
                             SELECT UPPER (TRIM (efn.xxwc_image_filename_attr))
                               FROM xxwc.xxwc_ego_image_filenames efn
                              WHERE efn.xxwc_image_filetype_attr = 'F')
                   UNION
                   SELECT wmv.inventory_item_id inventory_item_id,
                          wmv.xxwc_a_t_1_attr image_filename,
                          'T' image_filetype, 'N' image_exists
                     FROM apps.ego_xxwc_image_manag_agv imv,
                          apps.xxwc_white_c_m_agv wmv
                    WHERE imv.xxwc_a_t_1_image_check = 'Y'
                      AND wmv.inventory_item_id = imv.inventory_item_id
                      AND wmv.organization_id = imv.organization_id
                      AND UPPER (TRIM (wmv.xxwc_a_t_1_attr)) NOT IN (
                             SELECT UPPER (TRIM (efn.xxwc_image_filename_attr))
                               FROM xxwc.xxwc_ego_image_filenames efn
                              WHERE efn.xxwc_image_filetype_attr = 'T'))
         ORDER BY inventory_item_id;
   -- Main block --
   BEGIN
      errbuf := NULL;
      retcode := 0;
      ln_organization_id := fnd_profile.VALUE ('XXWC_ITEM_MASTER_ORG');

      BEGIN
         SELECT responsibility_id, application_id
           INTO l_num_resp_id, l_num_app_id
           FROM apps.fnd_responsibility
          WHERE responsibility_key = 'EGO_PIM_DATA_LIBRARIAN';
      EXCEPTION
         WHEN OTHERS
         THEN
            lv_error_message :=
                  'Error while fetching responsibility and application ID. Error message: '
               || SQLERRM;
            lv_calling :=
               'Call while fetching application ID for responsibility EGO_PIM_DATA_LIBRARIAN...';
            RAISE l_main_exception;
      END;

      BEGIN
         SELECT attr_group_id
           INTO ln_image_mgmt_attr_grp_id
           FROM apps.ego_attr_groups_v
          WHERE attr_group_name = 'XXWC_IMAGE_MANAGEMENT'
            AND attr_group_type = cv_ego_itemmgmt_group;
      EXCEPTION
         WHEN OTHERS
         THEN
            lv_error_message :=
                  'Error while fetching Image Management attribute group ID. Error message: '
               || SQLERRM;
            lv_calling :=
               'Call while fetching attribute group ID for attribute group XXWC_IMAGE_MANAGEMENT...';
            RAISE l_main_exception;
      END;

      fnd_global.apps_initialize (fnd_global.user_id,
                                  l_num_resp_id,
                                  l_num_app_id
                                 );
      fnd_file.put_line (fnd_file.LOG,
                         'Opening CSR_ITEM_IMAGE_INFO_SYNC_Y cursor...'
                        );
      lv_calling := 'Opening item image info sync YES cursor...';
      ln_rec_count := 0;
      ln_data_set_id := 0;
      l_row_identifier := 0;
      ln_inventory_item_id := -999;

      SELECT ego_iua_data_set_id_s.NEXTVAL
        INTO ln_data_set_id
        FROM DUAL;

      ln_min_data_set_id := ln_data_set_id;

      FOR rec_item_image_info_sync_y IN csr_item_image_info_sync_y
      LOOP
         --fnd_file.put_line(fnd_file.LOG,CHR(10));
         --fnd_file.put_line(fnd_file.LOG,'inventory_item_id: '||rec_item_image_info_sync_y.inventory_item_id);
         --fnd_file.put_line(fnd_file.LOG,'image_filename: '||rec_item_image_info_sync_y.image_filename);
         --fnd_file.put_line(fnd_file.LOG,'image_filetype: '||rec_item_image_info_sync_y.image_filetype);
         --fnd_file.put_line(fnd_file.LOG,'image_exists: '||rec_item_image_info_sync_y.image_exists);
         IF ln_inventory_item_id <>
                                 rec_item_image_info_sync_y.inventory_item_id
         THEN
            l_row_identifier := l_row_identifier + 1;
            ln_inventory_item_id :=
                                 rec_item_image_info_sync_y.inventory_item_id;
         END IF;

         lv_fullsize_image_url := NULL;
         lv_thumbnail_image_url := NULL;

         IF rec_item_image_info_sync_y.image_filetype = 'F'
         THEN
            lv_fullsize_image_url :=
                  lv_fullsize_image_path
               || rec_item_image_info_sync_y.image_filename;
            lv_calling :=
                  'Inserting Fullsize URL for Inventory Item ID: '
               || rec_item_image_info_sync_y.inventory_item_id;

            BEGIN
               INSERT INTO ego.ego_itm_usr_attr_intrfc
                           (process_status, data_set_id, row_identifier,
                            attr_group_int_name, attr_int_name,
                            attr_value_str, transaction_type,
                            organization_id,
                            inventory_item_id,
                            created_by, creation_date, last_updated_by,
                            last_update_date, last_update_login,
                            attr_group_type,
                            interface_table_unique_id
                           )
                    VALUES (1, ln_data_set_id, l_row_identifier,
                            'XXWC_IMAGE_MANAGEMENT', cv_attr_fullsize_image,
                            lv_fullsize_image_url, 'SYNC',
                            ln_organization_id,
                            rec_item_image_info_sync_y.inventory_item_id,
                            fnd_global.user_id, SYSDATE, fnd_global.user_id,
                            SYSDATE, fnd_global.login_id,
                            cv_ego_itemmgmt_group,
                            mtl_system_items_interface_s.NEXTVAL
                           );

               ln_rec_count := ln_rec_count + 1;
            EXCEPTION
               WHEN OTHERS
               THEN
                  lv_error_message := 'Error Message: ' || SQLERRM;
                  xxcus_error_pkg.xxcus_error_main_api
                     (p_called_from            => 'XXWC_EGO_IMAGE_MGMT_PKG.SYNC_IMAGE_EXISTS_ATTR',
                      p_calling                => lv_calling,
                      p_request_id             => ln_data_set_id,
                      p_ora_error_msg          => lv_error_message,
                      p_error_desc             => 'User defined exception',
                      p_distribution_list      => cv_dist_list,
                      p_module                 => 'XXWC'
                     );
                  COMMIT;
                  retcode := 1;
            END;

            lv_calling :=
                  'Inserting Fullsize Image Exists? attribute value as Y for Inventory Item ID: '
               || rec_item_image_info_sync_y.inventory_item_id;

            BEGIN
               INSERT INTO ego.ego_itm_usr_attr_intrfc
                           (process_status, data_set_id, row_identifier,
                            attr_group_int_name,
                            attr_int_name, attr_value_str, transaction_type,
                            organization_id,
                            inventory_item_id,
                            created_by, creation_date, last_updated_by,
                            last_update_date, last_update_login,
                            attr_group_type,
                            interface_table_unique_id
                           )
                    VALUES (1, ln_data_set_id, l_row_identifier,
                            'XXWC_IMAGE_MANAGEMENT',
                            cv_attr_fullsize_image_chk, 'Y', 'SYNC',
                            ln_organization_id,
                            rec_item_image_info_sync_y.inventory_item_id,
                            fnd_global.user_id, SYSDATE, fnd_global.user_id,
                            SYSDATE, fnd_global.login_id,
                            cv_ego_itemmgmt_group,
                            mtl_system_items_interface_s.NEXTVAL
                           );

               ln_rec_count := ln_rec_count + 1;
            EXCEPTION
               WHEN OTHERS
               THEN
                  lv_error_message := 'Error Message: ' || SQLERRM;
                  xxcus_error_pkg.xxcus_error_main_api
                     (p_called_from            => 'XXWC_EGO_IMAGE_MGMT_PKG.SYNC_IMAGE_EXISTS_ATTR',
                      p_calling                => lv_calling,
                      p_request_id             => ln_data_set_id,
                      p_ora_error_msg          => lv_error_message,
                      p_error_desc             => 'User defined exception',
                      p_distribution_list      => cv_dist_list,
                      p_module                 => 'XXWC'
                     );
                  COMMIT;
                  retcode := 1;
            END;
         ELSIF rec_item_image_info_sync_y.image_filetype = 'T'
         THEN
            lv_thumbnail_image_url :=
                  lv_thumbnail_image_path
               || rec_item_image_info_sync_y.image_filename;
            lv_calling :=
                  'Inserting Thumbnail URL for Inventory Item ID: '
               || rec_item_image_info_sync_y.inventory_item_id;

            BEGIN
               INSERT INTO ego.ego_itm_usr_attr_intrfc
                           (process_status, data_set_id, row_identifier,
                            attr_group_int_name,
                            attr_int_name, attr_value_str,
                            transaction_type, organization_id,
                            inventory_item_id,
                            created_by, creation_date, last_updated_by,
                            last_update_date, last_update_login,
                            attr_group_type,
                            interface_table_unique_id
                           )
                    VALUES (1, ln_data_set_id, l_row_identifier,
                            'XXWC_IMAGE_MANAGEMENT',
                            cv_attr_thumbnail_image, lv_thumbnail_image_url,
                            'SYNC', ln_organization_id,
                            rec_item_image_info_sync_y.inventory_item_id,
                            fnd_global.user_id, SYSDATE, fnd_global.user_id,
                            SYSDATE, fnd_global.login_id,
                            cv_ego_itemmgmt_group,
                            mtl_system_items_interface_s.NEXTVAL
                           );

               ln_rec_count := ln_rec_count + 1;
            EXCEPTION
               WHEN OTHERS
               THEN
                  lv_error_message := 'Error Message: ' || SQLERRM;
                  xxcus_error_pkg.xxcus_error_main_api
                     (p_called_from            => 'XXWC_EGO_IMAGE_MGMT_PKG.SYNC_IMAGE_EXISTS_ATTR',
                      p_calling                => lv_calling,
                      p_request_id             => ln_data_set_id,
                      p_ora_error_msg          => lv_error_message,
                      p_error_desc             => 'User defined exception',
                      p_distribution_list      => cv_dist_list,
                      p_module                 => 'XXWC'
                     );
                  COMMIT;
                  retcode := 1;
            END;

            lv_calling :=
                  'Inserting Thumbnail Image Exists? attribute value as Y for Inventory Item ID: '
               || rec_item_image_info_sync_y.inventory_item_id;

            BEGIN
               INSERT INTO ego.ego_itm_usr_attr_intrfc
                           (process_status, data_set_id, row_identifier,
                            attr_group_int_name,
                            attr_int_name, attr_value_str, transaction_type,
                            organization_id,
                            inventory_item_id,
                            created_by, creation_date, last_updated_by,
                            last_update_date, last_update_login,
                            attr_group_type,
                            interface_table_unique_id
                           )
                    VALUES (1, ln_data_set_id, l_row_identifier,
                            'XXWC_IMAGE_MANAGEMENT',
                            cv_attr_thumbnail_image_chk, 'Y', 'SYNC',
                            ln_organization_id,
                            rec_item_image_info_sync_y.inventory_item_id,
                            fnd_global.user_id, SYSDATE, fnd_global.user_id,
                            SYSDATE, fnd_global.login_id,
                            cv_ego_itemmgmt_group,
                            mtl_system_items_interface_s.NEXTVAL
                           );

               ln_rec_count := ln_rec_count + 1;
            EXCEPTION
               WHEN OTHERS
               THEN
                  lv_error_message := 'Error Message: ' || SQLERRM;
                  xxcus_error_pkg.xxcus_error_main_api
                     (p_called_from            => 'XXWC_EGO_IMAGE_MGMT_PKG.SYNC_IMAGE_EXISTS_ATTR',
                      p_calling                => lv_calling,
                      p_request_id             => ln_data_set_id,
                      p_ora_error_msg          => lv_error_message,
                      p_error_desc             => 'User defined exception',
                      p_distribution_list      => cv_dist_list,
                      p_module                 => 'XXWC'
                     );
                  COMMIT;
                  retcode := 1;
            END;
         END IF;

         IF ln_rec_count >= cv_batch_size
         THEN
            SELECT ego_iua_data_set_id_s.NEXTVAL
              INTO ln_data_set_id
              FROM DUAL;

            ln_rec_count := 0;
         END IF;
      END LOOP;

      COMMIT;

      SELECT ego_iua_data_set_id_s.CURRVAL
        INTO ln_max_data_set_id
        FROM DUAL;

      FOR i IN ln_min_data_set_id .. ln_max_data_set_id
      LOOP
         lv_calling :=
               'Calling EGO_ITEM_USER_ATTRS_CP_PUB.PROCESS_ITEM_USER_ATTRS_DATA API for data set ID: '
            || i;
         lv_errbuff := NULL;
         ln_retcode := NULL;
         apps.ego_item_user_attrs_cp_pub.process_item_user_attrs_data
                       (errbuf                              => lv_errbuff,
                        retcode                             => ln_retcode,
                        p_data_set_id                       => i,
                        p_debug_level                       => 0,
                        p_purge_successful_lines            => apps.fnd_api.g_false,
                        p_initialize_error_handler          => apps.fnd_api.g_true,
                        p_validate_only                     => apps.fnd_api.g_false,
                        p_ignore_security_for_validate      => apps.fnd_api.g_false
                       );
         COMMIT;

         IF lv_errbuff IS NOT NULL
         THEN
            fnd_file.put_line
               (fnd_file.LOG,
                   'Error after calling EGO_ITEM_USER_ATTRS_CP_PUB.PROCESS_ITEM_USER_ATTRS_DATA API to sync image exists attribute value to Y. '
                || ' Data Set ID: '
                || i
                || 'Error Message: '
                || lv_errbuff
               );
            xxcus_error_pkg.xxcus_error_main_api
               (p_called_from            => 'XXWC_EGO_IMAGE_MGMT_PKG.SYNC_IMAGE_EXISTS_ATTR',
                p_calling                => lv_calling,
                p_request_id             => i,
                p_ora_error_msg          => lv_errbuff,
                p_error_desc             => 'User defined exception',
                p_distribution_list      => cv_dist_list,
                p_module                 => 'XXWC'
               );
            COMMIT;
            retcode := 1;
         END IF;
      END LOOP;

      fnd_file.put_line (fnd_file.LOG,
                         'Opening CSR_ITEM_IMAGE_INFO_SYNC_N cursor...'
                        );
      lv_calling := 'Opening item image info sync NO cursor...';
      ln_rec_count := 0;
      ln_data_set_id := 0;
      ln_min_data_set_id := 0;
      ln_max_data_set_id := 0;
      ln_inventory_item_id := -999;

      SELECT ego_iua_data_set_id_s.NEXTVAL
        INTO ln_data_set_id
        FROM DUAL;

      ln_min_data_set_id := ln_data_set_id;

      FOR rec_item_image_info_sync_n IN csr_item_image_info_sync_n
      LOOP
         --fnd_file.put_line(fnd_file.LOG,CHR(10));
         --fnd_file.put_line(fnd_file.LOG,'inventory_item_id: '||rec_item_image_info_sync_n.inventory_item_id);
         --fnd_file.put_line(fnd_file.LOG,'image_filename: '||rec_item_image_info_sync_n.image_filename);
         --fnd_file.put_line(fnd_file.LOG,'image_filetype: '||rec_item_image_info_sync_n.image_filetype);
         --fnd_file.put_line(fnd_file.LOG,'image_exists: '||rec_item_image_info_sync_n.image_exists);
         IF ln_inventory_item_id <>
                                 rec_item_image_info_sync_n.inventory_item_id
         THEN
            l_row_identifier := l_row_identifier + 1;
            ln_inventory_item_id :=
                                 rec_item_image_info_sync_n.inventory_item_id;
         END IF;

         IF rec_item_image_info_sync_n.image_filetype = 'F'
         THEN
            lv_calling :=
                  'Inserting Fullsize Image Exists? attribute value as N for Inventory Item ID: '
               || rec_item_image_info_sync_n.inventory_item_id;

            BEGIN
               INSERT INTO ego.ego_itm_usr_attr_intrfc
                           (process_status, data_set_id, row_identifier,
                            attr_group_int_name,
                            attr_int_name, attr_value_str, transaction_type,
                            organization_id,
                            inventory_item_id,
                            created_by, creation_date, last_updated_by,
                            last_update_date, last_update_login,
                            attr_group_type,
                            interface_table_unique_id
                           )
                    VALUES (1, ln_data_set_id, l_row_identifier,
                            'XXWC_IMAGE_MANAGEMENT',
                            cv_attr_fullsize_image_chk, 'N', 'SYNC',
                            ln_organization_id,
                            rec_item_image_info_sync_n.inventory_item_id,
                            fnd_global.user_id, SYSDATE, fnd_global.user_id,
                            SYSDATE, fnd_global.login_id,
                            cv_ego_itemmgmt_group,
                            mtl_system_items_interface_s.NEXTVAL
                           );

               ln_rec_count := ln_rec_count + 1;
            EXCEPTION
               WHEN OTHERS
               THEN
                  lv_error_message := 'Error Message: ' || SQLERRM;
                  xxcus_error_pkg.xxcus_error_main_api
                     (p_called_from            => 'XXWC_EGO_IMAGE_MGMT_PKG.SYNC_IMAGE_EXISTS_ATTR',
                      p_calling                => lv_calling,
                      p_request_id             => ln_data_set_id,
                      p_ora_error_msg          => lv_error_message,
                      p_error_desc             => 'User defined exception',
                      p_distribution_list      => cv_dist_list,
                      p_module                 => 'XXWC'
                     );
                  COMMIT;
                  retcode := 1;
            END;
         ELSIF rec_item_image_info_sync_n.image_filetype = 'T'
         THEN
            lv_calling :=
                  'Inserting Thumbnail Image Exists? attribute value as N for Inventory Item ID: '
               || rec_item_image_info_sync_n.inventory_item_id;

            BEGIN
               INSERT INTO ego.ego_itm_usr_attr_intrfc
                           (process_status, data_set_id, row_identifier,
                            attr_group_int_name,
                            attr_int_name, attr_value_str, transaction_type,
                            organization_id,
                            inventory_item_id,
                            created_by, creation_date, last_updated_by,
                            last_update_date, last_update_login,
                            attr_group_type,
                            interface_table_unique_id
                           )
                    VALUES (1, ln_data_set_id, l_row_identifier,
                            'XXWC_IMAGE_MANAGEMENT',
                            cv_attr_thumbnail_image_chk, 'N', 'SYNC',
                            ln_organization_id,
                            rec_item_image_info_sync_n.inventory_item_id,
                            fnd_global.user_id, SYSDATE, fnd_global.user_id,
                            SYSDATE, fnd_global.login_id,
                            cv_ego_itemmgmt_group,
                            mtl_system_items_interface_s.NEXTVAL
                           );

               ln_rec_count := ln_rec_count + 1;
            EXCEPTION
               WHEN OTHERS
               THEN
                  lv_error_message := 'Error Message: ' || SQLERRM;
                  xxcus_error_pkg.xxcus_error_main_api
                     (p_called_from            => 'XXWC_EGO_IMAGE_MGMT_PKG.SYNC_IMAGE_EXISTS_ATTR',
                      p_calling                => lv_calling,
                      p_request_id             => ln_data_set_id,
                      p_ora_error_msg          => lv_error_message,
                      p_error_desc             => 'User defined exception',
                      p_distribution_list      => cv_dist_list,
                      p_module                 => 'XXWC'
                     );
                  COMMIT;
                  retcode := 1;
            END;
         END IF;

         IF ln_rec_count >= cv_batch_size
         THEN
            SELECT ego_iua_data_set_id_s.NEXTVAL
              INTO ln_data_set_id
              FROM DUAL;

            ln_rec_count := 0;
         END IF;
      END LOOP;

      COMMIT;

      SELECT ego_iua_data_set_id_s.CURRVAL
        INTO ln_max_data_set_id
        FROM DUAL;

      FOR i IN ln_min_data_set_id .. ln_max_data_set_id
      LOOP
         lv_calling :=
               'Calling EGO_ITEM_USER_ATTRS_CP_PUB.PROCESS_ITEM_USER_ATTRS_DATA API for data set ID: '
            || i;
         lv_errbuff := NULL;
         ln_retcode := NULL;
         apps.ego_item_user_attrs_cp_pub.process_item_user_attrs_data
                       (errbuf                              => lv_errbuff,
                        retcode                             => ln_retcode,
                        p_data_set_id                       => i,
                        p_debug_level                       => 0,
                        p_purge_successful_lines            => apps.fnd_api.g_false,
                        p_initialize_error_handler          => apps.fnd_api.g_true,
                        p_validate_only                     => apps.fnd_api.g_false,
                        p_ignore_security_for_validate      => apps.fnd_api.g_false
                       );
         COMMIT;

         IF lv_errbuff IS NOT NULL
         THEN
            fnd_file.put_line
               (fnd_file.LOG,
                   'Error after calling EGO_ITEM_USER_ATTRS_CP_PUB.PROCESS_ITEM_USER_ATTRS_DATA API to sync image exists attribute value to N.'
                || ' Data Set ID: '
                || i
                || ' Error Message: '
                || lv_errbuff
               );
            xxcus_error_pkg.xxcus_error_main_api
               (p_called_from            => 'XXWC_EGO_IMAGE_MGMT_PKG.SYNC_IMAGE_EXISTS_ATTR',
                p_calling                => lv_calling,
                p_request_id             => i,
                p_ora_error_msg          => lv_errbuff,
                p_error_desc             => 'User defined exception',
                p_distribution_list      => cv_dist_list,
                p_module                 => 'XXWC'
               );
            COMMIT;
            retcode := 1;
         END IF;
      END LOOP;
   EXCEPTION
      WHEN l_main_exception
      THEN
         fnd_file.put_line (fnd_file.LOG,
                            'Error Message: ' || lv_error_message
                           );
         ROLLBACK;
         xxcus_error_pkg.xxcus_error_main_api
            (p_called_from            => 'XXWC_EGO_IMAGE_MGMT_PKG.SYNC_IMAGE_EXISTS_ATTR',
             p_calling                => lv_calling,
             p_request_id             => ln_request_id,
             p_ora_error_msg          => lv_error_message,
             p_error_desc             => 'User defined exception',
             p_distribution_list      => cv_dist_list,
             p_module                 => 'XXWC'
            );
         COMMIT;
         retcode := 2;
      WHEN OTHERS
      THEN
         ROLLBACK;
         lv_error_message :=
               'Error in main block during image exists attribute sync up activity. Error message: '
            || SQLERRM;
         xxcus_error_pkg.xxcus_error_main_api
            (p_called_from            => 'XXWC_EGO_IMAGE_MGMT_PKG.SYNC_IMAGE_EXISTS_ATTR',
             p_calling                => lv_calling,
             p_request_id             => ln_request_id,
             p_ora_error_msg          => lv_error_message
                                                         --SUBSTR (SQLERRM, 1, 2000)
         ,
             p_error_desc             => 'User defined exception',
             p_distribution_list      => cv_dist_list,
             p_module                 => 'XXWC'
            );
         COMMIT;
         errbuf := lv_error_message;
         retcode := 2;
   END sync_image_exists_attr;
END xxwc_ego_image_mgmt_pkg;
/
