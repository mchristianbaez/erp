CREATE OR REPLACE PACKAGE BODY APPS.XXWC_INV_MONTHEND_AUTO_PKG
AS
   /*************************************************************************
   *  Copyright (c) 2016 HD Supply Inc.
   *  All rights reserved.
   **************************************************************************
   *   $Header XXWC_INV_MONTHEND_AUTO_PKG.pkb $
   *   Module Name: XXWC_INV_MONTHEND_AUTO_PKG.pkb
   *
   *   PURPOSE:    This package is used to Automate INV month end process.
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        11/15/2016  Neha Saini               Initial Version TMS# 20160930-00014
   * ***************************************************************************/
   /*****************************
 -- GLOBAL VARIABLES
 ******************************/

   g_error_message     VARCHAR2 (32000);
   g_location          VARCHAR2 (15000);
   g_ledger_id         NUMBER;
   g_ap_app_id         NUMBER;
   g_po_app_id         NUMBER;
   g_inv_app_id        NUMBER;
   G_EXCEPTION         EXCEPTION;
   g_user_id           NUMBER := FND_GLOBAL.USER_ID;
   --Global variable to store the org id and conc request id
   g_conc_request_id   NUMBER := FND_GLOBAL.CONC_REQUEST_ID;
   g_org_id            NUMBER := FND_PROFILE.VALUE ('ORG_ID');


   /*************************************************************************
   *  Procedure : Write_Error
   *
   * PURPOSE:   This procedure logs error message in Custom Error table
   * Parameter:
   *        IN
   *            p_debug_msg      -- Debug Message
   * REVISIONS:
   * Ver        Date        Author                     Description
   *  ---------  ----------  ---------------         -------------------------
   *  1.0        11/15/2016  Neha Saini               Initial Version TMS# 20160930-00014
  ************************************************************************/
   --add message to Error table
   PROCEDURE write_error (p_debug_msg IN VARCHAR2)
   IS
      l_err_callfrom    VARCHAR2 (100) DEFAULT 'XXWC_INV_MONTHEND_AUTO_PKG';
      l_err_callpoint   VARCHAR2 (100) DEFAULT 'START';
      l_distro_list     VARCHAR2 (100)
                           DEFAULT 'HDSOracleDevelopers@hdsupply.com';
   BEGIN
      xxcus_error_pkg.xxcus_error_main_api (
         p_called_from         => l_err_callfrom,
         p_calling             => l_err_callpoint,
         p_request_id          => g_conc_request_id,
         p_ora_error_msg       => SUBSTR (p_debug_msg, 1, 2000),
         p_error_desc          => 'Error running XXWC_INV_MONTHEND_AUTO_PKG with PROGRAM ERROR ',
         p_distribution_list   => l_distro_list,
         p_module              => 'INV');
   END write_error;

   /*************************************************************************
   *   Procedure : Write_output
   *
   *   PURPOSE:   This procedure is used for adding message to concurrent output file
   *   Parameter:
   *          IN
   *              p_debug_msg      -- Debug Message
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *  ---------  ----------  ---------------         -------------------------
   *   1.0        11/15/2016  Neha Saini               Initial Version TMS# 20160930-00014
   * ************************************************************************/

   PROCEDURE Write_output (p_debug_msg IN VARCHAR2)
   IS
   BEGIN
      IF NVL (g_conc_request_id, 0) <= 0
      THEN
         DBMS_OUTPUT.PUT_LINE (p_debug_msg);
      ELSE
         fnd_file.put_line (fnd_file.output, p_debug_msg);
         fnd_file.put_line (fnd_file.LOG, p_debug_msg);
      END IF;
   END Write_output;

   /**************************************************************************
   *   procedure Name: open_next_period
   *  *   Parameter:
   *          IN   p_period_name
   *          OUT  None
   *   PURPOSE:  This procedure is used to open next period for PO
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        11/15/2016  Neha Saini             Initial Version TMS# 20160930-00014
   * ***************************************************************************/
   PROCEDURE open_next_period (p_period_name VARCHAR2)
   IS
      v_app_id              NUMBER;
      v_resp_id             NUMBER;
      v_resp_name           VARCHAR2 (100);
      v_request_id          NUMBER;
      v_app_name            VARCHAR2 (20);
      v_req_phase           VARCHAR2 (100);
      v_req_status          VARCHAR2 (100);
      v_req_dev_phase       VARCHAR2 (100);
      v_req_dev_status      VARCHAR2 (100);
      v_req_message         VARCHAR2 (100);
      v_req_return_status   BOOLEAN;
      l_hir_origin_id       NUMBER;
      l_hir_id              NUMBER;
   BEGIN
      g_error_message := NULL;
      g_location := 'opening next period now';
      write_output (g_location);
      --submit �Open Period Control� program

      g_location := 'get user id ' || g_user_id;
      write_output (g_location);

      BEGIN
         mo_global.init ('INV');
         mo_global.set_policy_context ('S', 162);

         SELECT fa.application_id,
                fr.responsibility_id,
                fr.responsibility_name,
                fa.application_short_name
           INTO v_app_id,
                v_resp_id,
                v_resp_name,
                v_app_name
           FROM fnd_responsibility_vl fr, fnd_application fa
          WHERE     responsibility_name LIKE 'HDS EOM Inventory - WC'
                AND fr.application_id = fa.application_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            v_app_id := NULL;
            v_resp_id := NULL;
            v_resp_name := NULL;
            v_app_name := NULL;
      END;

      -- Initializing apps
      fnd_global.apps_initialize (user_id        => g_user_id,
                                  resp_id        => v_resp_id,
                                  resp_appl_id   => v_app_id);

      g_location := 'Now submitting request for open Period Control';
      write_output (g_location);
      --Envirnment Set up
      FND_REQUEST.SET_ORG_ID (162);

      -- get Heirarchy origin id
      BEGIN
         SELECT organization_id
           INTO l_hir_origin_id
           FROM org_organization_definitions
          WHERE organization_name = '710 - WC Fort Worth Branch (PZ8)';



         SELECT ORGANIZATION_STRUCTURE_ID
           INTO l_hir_id
           FROM PER_ORGANIZATION_STRUCTURES
          WHERE name LIKE 'White Cap Gulf Coast - 710';
      EXCEPTION
         WHEN OTHERS
         THEN
            l_hir_id := 0;
            l_hir_origin_id := 0;
      END;

      g_location :=
         'Heirearchy id for  White Cap Gulf Coast - 710 ' || l_hir_id;
      write_output (g_location);
      g_location :=
            'Heirarchy origin id for  710 - WC Fort Worth Branch (PZ8)'
         || l_hir_origin_id;
      write_output (g_location);
      g_location := 'Now submitting Open Period Control program';
      write_output (g_location);

      --submitted Open Period Control program
      v_request_id :=
         fnd_request.submit_request (application   => v_app_name, /* Short name of the application under which the program is registered in this it will be ONT since the Reserve Order program is registered under order management*/
                                     program       => 'INVCPOPEN', -- Short name of the concurrent program needs to submitted
                                     description   => NULL,
                                     start_time    => NULL,
                                     sub_request   => NULL,
                                     argument1     => l_hir_origin_id,
                                     argument2     => l_hir_id,
                                     argument3     => NULL,
                                     argument4     => 'N',
                                     argument5     => 1,
                                     argument6     => 'O',
                                     argument7     => 1);



      COMMIT;

      g_location := 'Concurrent Program Req ID' || v_request_id;
      write_output (g_location);


      -- Concurrent Program Status
      IF v_request_id <> 0 AND v_request_id IS NOT NULL
      THEN
         LOOP
            v_req_return_status :=
               fnd_concurrent.wait_for_request (v_request_id, -- request ID for which results need to be known
                                                60, --interval parameter  time between checks default is 60 Seconds
                                                0, -- Max wait default is 0 , this is the maximum amount of time a program should wait for it to get completed
                                                v_req_phase,
                                                v_req_status,
                                                v_req_dev_phase,
                                                v_req_dev_status,
                                                v_req_message);
            /*Others are output parameters to get the phase and status of the submitted concurrent program*/
            EXIT WHEN    UPPER (v_req_phase) = 'COMPLETED'
                      OR UPPER (v_req_status) IN ('CANCELLED',
                                                  'ERROR',
                                                  'TERMINATED');
         END LOOP;
      END IF;

      g_location := 'Concurrent Program Status' || v_req_status;
      write_output (g_location);

      --check if Open Period Control  completed successfully if not end program with warning
      IF v_req_status IN ('CANCELLED', 'ERROR', 'TERMINATED')
      THEN
         g_location :=
               'Next period '
            || p_period_name
            || ' Not Open Successfully. Open Period Control not completed normal check log file';
         write_output (g_location);
         g_error_message :=
            'Program " �Open Period Control� "  did not completed Successfully';
      ELSE
         g_location := 'Next period ' || p_period_name || ' Open Successfully';
         write_output (g_location);
      END IF;

      g_location := 'open_next_period procedure completed Successfully';
      write_output (g_location);
   EXCEPTION
      WHEN OTHERS
      THEN
         g_error_message :=
               'Error at when others: open next period for '
            || p_period_name
            || ' - '
            || SQLERRM;
   END open_next_period;

   /**************************************************************************
 *   procedure Name: close_curr_period
 *  *   Parameter:
 *          IN    p_period_name,,p_next_period
 *          OUT   None
 *   PURPOSE:  This procedure is used to close current period for PO
 *   REVISIONS:
 *   Ver        Date        Author                     Description
 *   ---------  ----------  ---------------         -------------------------
 *   1.0        11/15/2016  Neha Saini             Initial Version TMS# 20160930-00014
 * ***************************************************************************/
   PROCEDURE close_curr_period (p_period_name    VARCHAR2,
                                p_next_period    VARCHAR2)
   IS
      v_app_id              NUMBER;
      v_resp_id             NUMBER;
      v_resp_name           VARCHAR2 (100);
      v_request_id          NUMBER;
      v_app_name            VARCHAR2 (20);
      v_req_phase           VARCHAR2 (100);
      v_req_status          VARCHAR2 (100);
      v_req_dev_phase       VARCHAR2 (100);
      v_req_dev_status      VARCHAR2 (100);
      v_req_message         VARCHAR2 (100);
      v_req_return_status   BOOLEAN;

      l_hir_origin_id       NUMBER;
      l_hir_id              NUMBER;
   BEGIN
      g_error_message := NULL;
      g_location := 'start " close current period "  now';
      write_output (g_location);
      --submit  Close Period Control

      g_user_id := FND_GLOBAL.USER_ID;


      g_location := 'get user id ' || g_user_id;
      write_output (g_location);

      BEGIN
         mo_global.init ('INV');
         mo_global.set_policy_context ('S', 162);

         SELECT fa.application_id,
                fr.responsibility_id,
                fr.responsibility_name,
                fa.application_short_name
           INTO v_app_id,
                v_resp_id,
                v_resp_name,
                v_app_name
           FROM fnd_responsibility_vl fr, fnd_application fa
          WHERE     responsibility_name LIKE 'HDS EOM Inventory - WC'
                AND fr.application_id = fa.application_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            v_app_id := NULL;
            v_resp_id := NULL;
            v_resp_name := NULL;
            v_app_name := NULL;
      END;

      -- Initializing apps

      fnd_global.apps_initialize (user_id        => g_user_id,
                                  resp_id        => v_resp_id,
                                  resp_appl_id   => v_app_id);

      g_location := 'Now submitting request for  Close Period Control';
      write_output (g_location);
      --Envirnment Set up
      FND_REQUEST.SET_ORG_ID (162);

      -- get Heirarchy origin id
      BEGIN
         SELECT organization_id
           INTO l_hir_origin_id
           FROM org_organization_definitions
          WHERE organization_name = '710 - WC Fort Worth Branch (PZ8)';



         SELECT ORGANIZATION_STRUCTURE_ID
           INTO l_hir_id
           FROM PER_ORGANIZATION_STRUCTURES
          WHERE name LIKE 'White Cap Gulf Coast - 710';
      EXCEPTION
         WHEN OTHERS
         THEN
            l_hir_id := 0;
            l_hir_origin_id := 0;
      END;

      g_location :=
         'Heirarchy id for  White Cap Gulf Coast - 710 ' || l_hir_id;
      write_output (g_location);
      g_location :=
            'Heirearchy origin id for  710 - WC Fort Worth Branch (PZ8)'
         || l_hir_origin_id;
      write_output (g_location);
      g_location := 'Now submitting Close Period Control program ';
      write_output (g_location);

      --submitted �Close  Period Contro"
      v_request_id :=
         fnd_request.submit_request (application   => v_app_name, /* Short name of the application under which the program is registered in this it will be ONT since the Reserve Order program is registered under order management*/
                                     PROGRAM       => 'INVCPCLOS', -- Short name of the concurrent program needs to submitted
                                     description   => NULL,
                                     start_time    => NULL,
                                     sub_request   => NULL,
                                     argument1     => l_hir_origin_id,
                                     argument2     => l_hir_id,
                                     argument3     => p_period_name,
                                     argument4     => 'Y',
                                     argument5     => NULL,
                                     argument6     => 'C',
                                     argument7     => 25);



      COMMIT;

      g_location := 'Concurrent Program Req ID' || v_request_id;
      write_output (g_location);


      -- Concurrent Program Status
      IF v_request_id <> 0 AND v_request_id IS NOT NULL
      THEN
         LOOP
            v_req_return_status :=
               fnd_concurrent.wait_for_request (v_request_id, -- request ID for which results need to be known
                                                60, --interval parameter  time between checks default is 60 Seconds
                                                0, -- Max wait default is 0 , this is the maximum amount of time a program should wait for it to get completed
                                                v_req_phase,
                                                v_req_status,
                                                v_req_dev_phase,
                                                v_req_dev_status,
                                                v_req_message);

            /*Others are output parameters to get the phase and status of the submitted concurrent program*/
            EXIT WHEN    UPPER (v_req_phase) = 'COMPLETED'
                      OR UPPER (v_req_status) IN ('CANCELLED',
                                                  'ERROR',
                                                  'TERMINATED');
         END LOOP;
      END IF;

      g_location := 'Concurrent Program Status' || v_req_status;
      write_output (g_location);

      --check if  �Close Period Control� completed successfully if not end program with warning
      IF v_req_status IN ('CANCELLED', 'ERROR', 'TERMINATED')
      THEN
         g_error_message :=
            'Program  Close Period Control did not completed Successfully.check log file.';
         RAISE g_exception;
      ELSE
         g_location := 'Current period ' || p_period_name || ' Closed Successfully';
         write_output (g_location);
      END IF;

      --End of procedure
      g_location := 'Completed Successfully close_curr_period';
      write_output (g_location);
   EXCEPTION
      WHEN g_exception
      THEN
         g_error_message :=
               'Error at g_exception: close current period '
            || g_error_message
            || '-'
            || SUBSTR (SQLERRM, 1, 1000);
      WHEN OTHERS
      THEN
         g_error_message :=
               'Error at when others: close current period '
            || g_error_message
            || '-'
            || SQLERRM;
   END close_curr_period;

   /**************************************************************************
  *   procedure Name: chk_pend_trx
  *  *   Parameter:
  *          IN    p_period_name,,p_next_period
  *          OUT   None
  *   PURPOSE:  This procedure is used to close current period for PO
  *   REVISIONS:
  *   Ver        Date        Author                     Description
  *   ---------  ----------  ---------------         -------------------------
  *   1.0        11/15/2016  Neha Saini             Initial Version TMS# 20160930-00014
  * ***************************************************************************/
   PROCEDURE chk_pend_trx (p_period_name VARCHAR2, p_next_period VARCHAR2)
   IS
      v_app_id                 NUMBER;
      l_pend_receiving         INTEGER;
      l_unproc_matl            INTEGER;
      l_pend_matl              INTEGER;
      l_uncost_matl            INTEGER;
      l_pend_move              INTEGER;
      l_pend_wip_cost          INTEGER;
      l_uncost_wsm             INTEGER;
      l_pend_wsm               INTEGER;
      l_pend_ship              INTEGER;
      l_pend_lcm               INTEGER;
      l_released_work_orders   INTEGER;
      l_return_status          VARCHAR2 (1000);
      l_period_id              NUMBER;
      l_status_count           NUMBER := 0;
      l_check_count            NUMBER := 0;
      l_count                  NUMBER;
   BEGIN
      g_error_message := NULL;
      g_location := 'start " chk_pend_trx "  now';
      write_output (g_location);

      FOR rec IN (SELECT acct_period_id, schedule_close_date, organization_id
                    FROM apps.org_acct_periods
                   WHERE period_name = p_period_name)
      LOOP
         l_count := 0;
         g_location := 0;
         write_output (g_location);

         --Check if any pending resolution required transactions exist and that prevent Inventory close

         CST_ACCOUNTINGPERIOD_PUB.get_pendingtcount (
            p_api_version            => 1.0,
            p_org_id                 => rec.organization_id,
            p_closing_period         => rec.acct_period_id,
            p_sched_close_date       => rec.schedule_close_date,
            x_pend_receiving         => l_pend_receiving,
            x_unproc_matl            => l_unproc_matl,
            x_pend_matl              => l_pend_matl,
            x_uncost_matl            => l_uncost_matl,
            x_pend_move              => l_pend_move,
            x_pend_wip_cost          => l_pend_wip_cost,
            x_uncost_wsm             => l_uncost_wsm,
            x_pending_wsm            => l_pend_wsm,
            x_pending_ship           => l_pend_ship,
            x_pending_lcm            => l_pend_lcm,
            x_released_work_orders   => l_released_work_orders,
            x_return_status          => l_return_status);


         g_location :=
               'For period_id '
            || rec.acct_period_id
            || ' and organization_id '
            || rec.organization_id
            || ' API CST_ACCOUNTINGPERIOD_PUB.get_pendingtcount cmpleted:return_status---> '
            || l_return_status;
         write_output (g_location);


         IF     l_return_status <> FND_API.g_ret_sts_success
            AND l_return_status IS NOT NULL
         THEN
            l_status_count := l_status_count + 1;
         END IF;



         --Stop closing if the counts are not equal to zero.
         IF l_pend_matl <> 0
         THEN
            -- chekcing if order entry transaction exists

            BEGIN
               SELECT COUNT (1)
                 INTO l_count
                 FROM mtl_transactions_interface
                WHERE     source_code = 'ORDER ENTRY'
                      AND organization_id = rec.organization_id;
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_count := 0;
            END;

            IF l_count > 0
            THEN
               l_check_count := l_check_count + 1;
               g_location :=
                     '"There is a pending material transaction with source ORDER ENTRY exist" '
                  || ' l_pend_matl '
                  || l_pend_matl;
               write_output (g_location);
            END IF;
         END IF;

         IF    l_unproc_matl <> 0
            OR l_uncost_matl <> 0
            OR l_pend_wip_cost <> 0
            OR l_uncost_wsm <> 0
            OR l_pend_ship <> 0
            OR l_pend_lcm <> 0
            OR l_pend_wsm <> 0
         THEN
            l_check_count := l_check_count + 1;
            g_location := ' l_unproc_matl ' || l_unproc_matl;
            write_output (g_location);
            g_location := ' l_pend_matl ' || l_pend_matl;
            write_output (g_location);
            g_location := ' l_uncost_matl ' || l_uncost_matl;
            write_output (g_location);
            g_location := ' l_pend_wip_cost ' || l_pend_wip_cost;
            write_output (g_location);
            g_location := ' l_uncost_wsm ' || l_uncost_wsm;
            write_output (g_location);
            g_location := ' l_pend_wsm ' || l_pend_wsm;
            write_output (g_location);
            g_location := ' l_pend_lcm ' || l_pend_lcm;
            write_output (g_location);
            g_location := ' l_pend_ship ' || l_pend_ship;
            write_output (g_location);
         END IF;
      END LOOP;

      g_location :=
            'Number of times status was error '
         || l_status_count
         || ' no of times counts are not 0 '
         || l_check_count;
      write_output (g_location);



      IF l_status_count > 0 OR l_check_count > 0
      THEN
         g_error_message :=
               ' failure while calling API CST_ACCOUNTINGPERIOD_PUB.get_pendingtcount  '
            || SQLERRM
            || 'for some periods in orgs the counts are not equal to zero ';
         RAISE g_exception;
      END IF;

      --End of procedure
      g_location := 'Completed Successfully chk_pend_trx';
      write_output (g_location);
   EXCEPTION
      WHEN g_exception
      THEN
         g_error_message :=
               'Error at g_exception:  chk_pend_trx procedure  '
            || g_error_message
            || '-'
            || SUBSTR (SQLERRM, 1, 1000);
      WHEN OTHERS
      THEN
         g_error_message :=
               'Error at when others: chk_pend_trx'
            || g_error_message
            || '-'
            || SQLERRM;
   END chk_pend_trx;

   /*************************************************************************
   *   Procedure : main
   *
   *   PURPOSE:   This procedure is for concurrent program -> XXWC INV Period Open/Close.
   *               Short Name: XXWC_INV_PERIOD_OPN_CLS
   *               Executable: XXWC_INV_PERIOD_OPN_CLS
   *
   *   Parameter:
   *          IN  p_operating_unit,p_action,p_next_period,p_curr_period
   *          OUT   ERRBUF,RETCODE
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        11/15/2016  Neha Saini               Initial Version TMS# 20160930-00014
   * ************************************************************************/

   PROCEDURE main (ERRBUF                OUT VARCHAR2,
                   RETCODE               OUT NUMBER,
                   p_operating_unit   IN     NUMBER,
                   p_action           IN     VARCHAR2,
                   p_next_period      IN     VARCHAR2,
                   p_curr_period      IN     VARCHAR2)
   IS
      l_next_ap_status    APPS.gl_period_statuses.closing_status%TYPE;
      l_curr_ap_status    APPS.gl_period_statuses.closing_status%TYPE;
      l_next_po_status    APPS.gl_period_statuses.closing_status%TYPE;
      l_curr_po_status    APPS.gl_period_statuses.closing_status%TYPE;
      l_next_inv_status   APPS.gl_period_statuses.closing_status%TYPE;
   BEGIN
      --initializing variables
      g_error_message := NULL;
      ERRBUF := NULL;
      RETCODE := 0;
      --printing IN parameters
      g_location := 'Start main for XXWC INV Period Open/Close Program';
      write_output (g_location);
      g_location := 'Now printing IN parameters ';
      write_output (g_location);
      g_location := 'p_operating_unit ' || p_operating_unit;
      write_output (g_location);
      g_location := 'p_action ' || p_action;
      write_output (g_location);
      g_location := 'p_next_period ' || p_next_period;
      write_output (g_location);
      g_location := 'p_curr_period ' || p_curr_period;
      write_output (g_location);

      -- get ledger id
      SELECT set_of_books_id
        INTO g_ledger_id
        FROM apps.hr_operating_units
       WHERE organization_id = p_operating_unit;

      g_location := 'g_ledger_id Ledger Id ' || g_ledger_id;
      write_output (g_location);

      -- get AP application id
      SELECT application_id
        INTO g_ap_app_id
        FROM apps.fnd_application_vl
       WHERE application_name LIKE 'Payables';

      g_location := 'g_ap_app_id  AP Application ID ' || g_ap_app_id;
      write_output (g_location);

      -- get  INV application id
      SELECT application_id
        INTO g_inv_app_id
        FROM apps.fnd_application_vl
       WHERE application_name LIKE 'Inventory';

      g_location := 'g_inv_app_id  AP Application ID ' || g_inv_app_id;
      write_output (g_location);

      -- get INV application id
      SELECT application_id
        INTO g_po_app_id
        FROM apps.fnd_application_vl
       WHERE application_name LIKE 'Purchasing';

      g_location := 'g_po_app_id INV Application ID ' || g_po_app_id;
      write_output (g_location);

      -- Actual logic started here

      --get next AP period Status
      BEGIN
         SELECT closing_status
           INTO l_next_ap_status
           FROM gl_period_statuses gps
          WHERE     gps.period_name = p_next_period
                AND gps.application_id = g_ap_app_id
                AND gps.set_of_books_id = g_ledger_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            g_error_message :=
                  'Error fetching status of AP next period '
               || SUBSTR (SQLERRM, 1, 1000);
            RAISE G_EXCEPTION;
      END;

      g_location :=
         'l_next_ap_status  for AP next period status ' || l_next_ap_status;
      write_output (g_location);

      --get current AP period status
      BEGIN
         SELECT closing_status
           INTO l_curr_ap_status
           FROM gl_period_statuses gps
          WHERE     gps.period_name = p_curr_period
                AND gps.application_id = g_ap_app_id
                AND gps.set_of_books_id = g_ledger_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            g_error_message :=
                  'Error fetching status of AP currrent period '
               || SUBSTR (SQLERRM, 1, 1000);
            RAISE G_EXCEPTION;
      END;

      g_location :=
            'l_curr_ap_status  for AP current period status '
         || l_curr_ap_status;
      write_output (g_location);

      --get status for PO Next Period
      BEGIN
         SELECT closing_status
           INTO l_next_po_status
           FROM gl_period_statuses gps
          WHERE     gps.period_name = p_next_period
                AND gps.application_id = g_po_app_id
                AND gps.set_of_books_id = g_ledger_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            g_error_message :=
                  'Error fetching status of PO next period '
               || SUBSTR (SQLERRM, 1, 1000);
            RAISE G_EXCEPTION;
      END;

      g_location :=
         'l_next_po_status  for next PO period status ' || l_next_po_status;
      write_output (g_location);


      --get status for PO Current Period
      BEGIN
         SELECT closing_status
           INTO l_curr_po_status
           FROM gl_period_statuses gps
          WHERE     gps.period_name = p_curr_period
                AND gps.application_id = g_po_app_id
                AND gps.set_of_books_id = g_ledger_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            g_error_message :=
                  'Error fetching status of PO Current Period '
               || SUBSTR (SQLERRM, 1, 1000);
            RAISE G_EXCEPTION;
      END;

      g_location :=
         'l_curr_po_status  for next PO Current Period' || l_curr_po_status;
      write_output (g_location);



      IF p_action = 'Open'
      THEN
         --Current INV period should only be allowed to close if:
         --Next AP period = Open
         --Current AP period = Closed
         --Next PO period = Open
         --Current PO period = Closed
         --check if Next AP and PO  period is OPEN
         IF l_next_ap_status = 'O' AND l_next_po_status = 'O'
         THEN
            --check if current AP and PO period is CLOSE
            IF l_curr_ap_status = 'C' AND l_curr_po_status = 'C'
            THEN
               --check if Next INV period is already OPEN
               BEGIN
                  SELECT DISTINCT OPEN_FLAG
                    INTO l_next_inv_status
                    FROM apps.ORG_ACCT_PERIODS
                   WHERE     period_name = p_next_period
                         AND organization_id IN (SELECT organization_id
                                                   FROM apps.org_organization_definitions
                                                  WHERE     operating_unit =
                                                               g_org_id
                                                        AND NVL (
                                                               disable_date,
                                                               SYSDATE + 1) >
                                                               SYSDATE);
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     l_next_inv_status := 0;
                  WHEN OTHERS
                  THEN
                     g_error_message :=
                           'Error at checking if next INV period is open or not '
                        || SQLERRM;
                     RAISE G_EXCEPTION;
               END;

               g_location :=
                     ' Next INV period is l_next_inv_status '
                  || l_next_inv_status;
               write_output (g_location);

               IF l_next_inv_status = 'Y'
               THEN
                  g_location := ' Next INV period is already OPEN';
                  write_output (g_location);
                  RAISE G_EXCEPTION;
               ELSE
                  --Now open INV next period
                  g_location :=
                     'Now open INV next period calling open_next_period';
                  write_output (g_location);
                  open_next_period (p_next_period);

                  IF g_error_message IS NOT NULL
                  THEN
                     RAISE G_EXCEPTION;
                  END IF;
               END IF;
            ELSE
               IF l_next_ap_status <> 'C'
               THEN
                  g_error_message := ' AP Current period is not CLOSE ';
               ELSIF l_next_po_status <> 'C'
               THEN
                  g_error_message := ' PO Current period is not CLOSE ';
               END IF;

               RAISE G_EXCEPTION;
            END IF;
         ELSE
            IF l_next_ap_status <> 'O'
            THEN
               g_error_message := ' Next AP period is not OPEN ';
            ELSIF l_next_po_status <> 'O'
            THEN
               g_error_message := ' Next PO period is not OPEN ';
            END IF;

            RAISE G_EXCEPTION;
         END IF;
      ELSIF p_action = 'Close'
      THEN
         --   Current INV period should only be allowed to close if:
         --   Next AP period = Open
         --   Current AP period = Closed
         --   Next PO period = Open
         --   Current PO period = closed
         --   Next INV period = Open

         IF l_next_ap_status = 'O' AND l_next_po_status = 'O'
         THEN
            --check if current AP and PO period is CLOSE
            IF l_curr_ap_status = 'C' AND l_curr_po_status = 'C'
            THEN
               --check if Next INV period is  OPEN
               BEGIN
                  SELECT DISTINCT OPEN_FLAG
                    INTO l_next_inv_status
                    FROM apps.ORG_ACCT_PERIODS
                   WHERE     period_name = p_next_period
                         AND organization_id IN (SELECT organization_id
                                                   FROM apps.org_organization_definitions
                                                  WHERE     operating_unit =
                                                               g_org_id
                                                        AND NVL (
                                                               disable_date,
                                                               SYSDATE + 1) >
                                                               SYSDATE);
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     l_next_inv_status := 0;
                  WHEN OTHERS
                  THEN
                     g_error_message :=
                           'Error at checking if next INV period is open or not '
                        || SQLERRM;
                     RAISE G_EXCEPTION;
               END;

               g_location :=
                     ' Next INV period is l_next_inv_status '
                  || l_next_inv_status;
               write_output (g_location);

               IF l_next_inv_status = 'Y'
               THEN
                  --Now close the INV Current period
                  g_location :=
                     'Now close INV current period calling close_curr_period';
                  write_output (g_location);
                  close_curr_period (p_curr_period, p_next_period);

                  IF g_error_message IS NOT NULL
                  THEN
                     RAISE G_EXCEPTION;
                  END IF;
               ELSE
                  g_error_message := 'Next INV period is not OPEN';
                  RAISE G_EXCEPTION;
               END IF;
            ELSE
               IF l_next_ap_status <> 'C'
               THEN
                  g_error_message := ' AP Current period is not CLOSE ';
               ELSIF l_next_po_status <> 'C'
               THEN
                  g_error_message := ' PO Current period is not CLOSE ';
               END IF;

               RAISE G_EXCEPTION;
            END IF;
         ELSE
            IF l_next_ap_status <> 'O'
            THEN
               g_error_message := ' Next AP period is not OPEN ';
            ELSIF l_next_po_status <> 'O'
            THEN
               g_error_message := ' Next PO period is not OPEN ';
            END IF;

            RAISE G_EXCEPTION;
         END IF;
      ELSIF p_action = 'Check Pending Trx'
      THEN
         g_location := 'calling chk_pend_trx ';
         write_output (g_location);
         chk_pend_trx (p_curr_period, p_next_period);

         IF g_error_message IS NOT NULL
         THEN
            RAISE G_EXCEPTION;
         END IF;
      END IF;


      -- Logic ends here

      g_location :=
         'Successful End of main for XXWC INV Period Open/Close Program';
      write_output (g_location);
   EXCEPTION
      WHEN G_EXCEPTION
      THEN
         ERRBUF := NULL;
         RETCODE := 1;
         g_location :=
               'Error at g_exception in main  '
            || g_error_message
            || ' - '
            || SUBSTR (SQLERRM, 1, 1000);
         write_output (g_location);
      WHEN OTHERS
      THEN
         ERRBUF := NULL;
         RETCODE := 2;
         g_location :=
               'Error at when others in main  '
            || g_error_message
            || ' -  '
            || SUBSTR (SQLERRM, 1, 1000);
         write_output (g_location);
   END;
END XXWC_INV_MONTHEND_AUTO_PKG;
/