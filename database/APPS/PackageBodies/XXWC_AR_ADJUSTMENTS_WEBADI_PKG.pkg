CREATE OR REPLACE PACKAGE XXWC_AR_ADJUSTMENTS_WEBADI_PKG 
/********************************************************************************
FILE NAME: APPS.XXWC_AR_ADJUSTMENTS_WEBADI_PKG.pkg

PROGRAM TYPE: PL/SQL Package

PURPOSE: API to load AR Invoice Adjustments using Web ADI.

HISTORY
===============================================================================
VERSION DATE          AUTHOR(S)       DESCRIPTION
------- -----------   --------------- -----------------------------------------
1.0     01/08/2013    Gopi Damuluri    Initial version.
********************************************************************************/
AS

 PROCEDURE IMPORT( p_customer_number       IN VARCHAR2
                 , p_customer_name         IN VARCHAR2
                 , p_customer_site_number  IN VARCHAR2
                 , p_invoice_source        IN VARCHAR2
                 , p_trx_number            IN VARCHAR2
                 , p_amount                IN NUMBER
                 , p_apply_date            IN DATE
                 , p_type                  IN VARCHAR2
                 , p_adjustment_type       IN VARCHAR2
                 , p_reason                IN VARCHAR2
                 , p_comments              IN VARCHAR2
--                 , p_code_combination_id   IN NUMBER
                 , p_created_from          IN VARCHAR2
                 , p_receivables_trx       IN VARCHAR2
                 );

END XXWC_AR_ADJUSTMENTS_WEBADI_PKG;
/
CREATE OR REPLACE PACKAGE BODY XXWC_AR_ADJUSTMENTS_WEBADI_PKG 
/********************************************************************************
FILE NAME: APPS.XXWC_AR_ADJUSTMENTS_WEBADI_PKG.pkg

PROGRAM TYPE: PL/SQL Package Body

PURPOSE: API to load AR Invoice Adjustments using Web ADI.

HISTORY
===============================================================================
VERSION DATE          AUTHOR(S)       DESCRIPTION
------- -----------   --------------- -----------------------------------------
1.0     01/08/2013    Gopi Damuluri    Initial version.
1.1     06/30/2015    Maharajan Shunmugam TMS#20150630-00125 AR - XXWC AR Invoice Adjustments ADI
1.2     08/03/2015    Maharajan Shunmugam TMS#IT - Remove code for XXWC AR Invoice Adjustments ADI
1.3     11/12/2015    Neha   Saini        TMS#20151111-00001 AR - XXWC AR Invoice Adjustments ADI
1.4     12/03/2015    Neha   Saini        TMS#20151201-00146 AR - Revert Changes XXWC AR Invoice Adjustments ADI
********************************************************************************/
AS

   g_user_id                  NUMBER       := fnd_global.user_id;
   g_org_id                   NUMBER       := fnd_profile.VALUE('ORG_ID');

 PROCEDURE IMPORT( p_customer_number       IN VARCHAR2
                 , p_customer_name         IN VARCHAR2
                 , p_customer_site_number  IN VARCHAR2
                 , p_invoice_source        IN VARCHAR2
                 , p_trx_number            IN VARCHAR2
                 , p_amount                IN NUMBER
                 , p_apply_date            IN DATE
                 , p_type                  IN VARCHAR2
                 , p_adjustment_type       IN VARCHAR2
                 , p_reason                IN VARCHAR2
                 , p_comments              IN VARCHAR2
--                 , p_code_combination_id   IN NUMBER
                 , p_created_from          IN VARCHAR2
                 , p_receivables_trx       IN VARCHAR2
                 )

IS
   up_adj_rec              ar_adjustments%rowtype;
   up_api_name             VARCHAR2(20);
   up_api_version          NUMBER;
   up_called_from          VARCHAR2(10);
   up_check_amount         VARCHAR2(1);
   up_chk_approval_limits  VARCHAR2(1);
   up_commit_flag          VARCHAR2(1);
   up_init_msg_list        VARCHAR2(1);
   up_move_deferred_tax    VARCHAR2(10);
   up_msg_count            NUMBER;
   up_msg_data             VARCHAR2(2000);
   up_new_adjust_id        ar_adjustments.adjustment_id%type;
   up_new_adjust_number    ar_adjustments.adjustment_number%type;
   up_old_adjust_id        ar_adjustments.adjustment_id%type;
   up_return_status        VARCHAR2(5);
   up_validation_level     NUMBER;
   
   l_error_message              VARCHAR2(250);
   l_program_error              EXCEPTION;
   l_customer_trx_id            NUMBER;
   l_payment_schedule_id        NUMBER;
   l_set_of_books_id            NUMBER;
   l_receivables_trx_id         NUMBER;
   l_check_flag                 NUMBER;
   l_dup_check                  NUMBER := 0;
   l_reason_code                VARCHAR2(100) := NULL;
   l_closing_status             VARCHAR2(1);
   l_amt_due_remaining          NUMBER;

BEGIN

mo_global.set_policy_context('S',162);
MO_GLOBAL.INIT ('AR');

up_adj_rec              := NULL;
up_api_name             := NULL;
up_api_version          := 1.0;
up_called_from          := NULL;
up_check_amount         := NULL;
up_chk_approval_limits  := 'Y';
up_commit_flag          := NULL;
up_init_msg_list        := FND_API.G_TRUE;
up_move_deferred_tax    := 'Y';
up_msg_count            := 0;
up_msg_data             := NULL;
up_new_adjust_id        := NULL;
up_new_adjust_number    := NULL;
up_old_adjust_id        := NULL;
up_return_status        := NULL;
up_validation_level     := FND_API.G_VALID_LEVEL_FULL;

   --------------------------------------------------------
   -- Derive SetOfBooksId
   --------------------------------------------------------
   BEGIN
   SELECT hou.set_of_books_id
     INTO l_set_of_books_id
     FROM hr_operating_units   hou
    WHERE 1                    = 1
      AND hou.organization_id  = g_org_id;
   EXCEPTION
   WHEN OTHERS THEN
     l_error_message    := 'Error derivig SetOfBooksId - '||SQLERRM;
     RAISE l_program_error;
   END;
   
   --------------------------------------------------------
   -- Check if AccountingDate is in Open Period
   --------------------------------------------------------
   BEGIN
   SELECT closing_status
     INTO l_closing_status
     FROM gl_period_statuses
    WHERE 1                    = 1
      AND set_of_books_id      = l_set_of_books_id
      AND application_id       = 222
      AND SYSDATE BETWEEN start_date AND end_date;      
   EXCEPTION
   WHEN OTHERS THEN
     l_error_message    := 'Error validating AccountingDate - '||SQLERRM;
     RAISE l_program_error;
   END;
   
   IF l_closing_status != 'O' THEN
     l_error_message    := 'AccountingDate is not in open period ';
     RAISE l_program_error;
   END IF;
   
   --------------------------------------------------------
   -- Derive Receivables Transaction Id
   --------------------------------------------------------
   BEGIN
   SELECT arta.receivables_trx_id
     INTO l_receivables_trx_id
     FROM ar_receivables_trx_all   arta
    WHERE 1                    = 1
      AND arta.name            = P_RECEIVABLES_TRX
      AND arta.status          = 'A'
      AND arta.org_id          = g_org_id;
   EXCEPTION
   WHEN NO_DATA_FOUND THEN
     l_error_message    := 'Invalid Receivables Transaction';
     RAISE l_program_error;
   WHEN OTHERS THEN
     l_error_message    := 'Error derivig Receivables Transaction Id - '||SQLERRM;
     RAISE l_program_error;
   END;
   
   --------------------------------------------------------
   -- Validate Invoice Number
   --------------------------------------------------------
   BEGIN
   SELECT rcta.customer_trx_id
        , apsa.payment_schedule_id
        , apsa.amount_due_remaining
     INTO l_customer_trx_id
        , l_payment_schedule_id
        , l_amt_due_remaining
     FROM ra_customer_trx_all      rcta
        , ra_batch_sources_all     rbsa
        , ar_payment_schedules_all apsa
    WHERE 1                    = 1
      AND rcta.trx_number      = p_trx_number
      AND rbsa.name            = P_INVOICE_SOURCE
      AND rcta.batch_source_id = rbsa.batch_source_id
      AND rcta.customer_trx_id = apsa.customer_trx_id
      AND apsa.org_id          = rcta.org_id
      AND rcta.org_id          = g_org_id;
   EXCEPTION
   WHEN NO_DATA_FOUND THEN
     l_error_message    := 'InvoiceSource and TrxNumber combination is Invalid';
     RAISE l_program_error;
   WHEN OTHERS THEN
     l_error_message    := 'Error validating InvoiceSource and TrxNumber combination - '||SQLERRM;
     RAISE l_program_error;
   END;

   --------------------------------------------------------
   -- Check if the Transaction is Open
   --------------------------------------------------------
--commented below for ver 1.1 --Reverted the comment for ver 1.2 --commented below for ver 1.4 --uncommented below for ver 1.4
   IF l_amt_due_remaining = 0 THEN
     l_error_message    := 'Trasaction is not open';
     RAISE l_program_error;
   END IF;
   
   --------------------------------------------------------
   -- Validate Reason Code
   --------------------------------------------------------
   BEGIN
     SELECT lookup_code
       INTO l_reason_code
       FROM fnd_lookup_values 
      WHERE lookup_type     = 'ADJUST_REASON' 
        AND enabled_flag    = 'Y'
        AND lookup_code     = P_REASON;
   EXCEPTION
   WHEN NO_DATA_FOUND THEN
     l_error_message    := 'Invalid ReasonCode';
     RAISE l_program_error;
   WHEN OTHERS THEN
     l_error_message    := 'Error validating Reason Code - '||SQLERRM;
     RAISE l_program_error;
   END;

   --------------------------------------------------------
   -- Check for duplicates
   --------------------------------------------------------
   BEGIN
      SELECT COUNT(1)
        INTO l_dup_check
        FROM ar_adjustments_all   aaa
       WHERE 1                    = 1
         AND aaa.customer_trx_id  = l_customer_trx_id
         AND aaa.amount           = p_amount
         AND aaa.type             = P_TYPE;
   EXCEPTION
   WHEN OTHERS THEN
     l_error_message    := 'Error validating for duplicates - '||SQLERRM;
     RAISE l_program_error;
   END;

   IF l_dup_check >0 THEN
     l_error_message    := 'Duplicate Transaction';
     RAISE l_program_error;
   END IF;

   DBMS_OUTPUT.PUT_LINE(' = 0.7 = ');   

   --------------------------------------------------------
   -- Check for Adjustment Type and Amount validation
   --------------------------------------------------------
--commented below for ver 1.1 --Reverted the comment for ver 1.2 -- commented below code for version 1.3 -- uncommented below code for version 1.4
   IF p_type = 'INVOICE' and p_amount != -1*l_amt_due_remaining THEN
     l_error_message    := 'Incorrect Adjustment Type';
     RAISE l_program_error;
   END IF;
  

   -- api- data adjustments mapping record - start 
   up_adj_rec.CUSTOMER_TRX_ID      := l_customer_trx_id;  -- Transaction for which adjustment is made
   up_adj_rec.ACCTD_AMOUNT         := p_amount;        
   up_adj_rec.AMOUNT               := p_amount;
   up_adj_rec.PAYMENT_SCHEDULE_ID  := l_payment_schedule_id;
   up_adj_rec.APPLY_DATE           := p_apply_date;
   up_adj_rec.GL_DATE              := p_apply_date;
   up_adj_rec.RECEIVABLES_TRX_ID   := l_receivables_trx_id;
   up_adj_rec.ADJUSTMENT_TYPE      := p_adjustment_type;  -- 'M';
   up_adj_rec.TYPE                 := p_type; 
   up_adj_rec.POSTING_CONTROL_ID   := -3;
   up_adj_rec.SET_OF_BOOKS_ID      := l_set_of_books_id;
   up_adj_rec.REASON_CODE          := p_reason;
   up_adj_rec.COMMENTS             := p_comments;
   up_adj_rec.STATUS               := 'A';
   up_adj_rec.CREATED_FROM         := p_created_from;
   up_adj_rec.CREATED_BY           := g_user_id;
   up_adj_rec.CREATION_DATE        := SYSDATE;
   up_adj_rec.LAST_UPDATE_DATE     := SYSDATE;
   up_adj_rec.LAST_UPDATED_BY      := g_user_id;

   AR_ADJUST_PUB.Create_Adjustment(p_api_name           => up_api_name,
                                p_api_version           => up_api_version,
                                p_init_msg_list         => up_init_msg_list,
                                p_commit_flag           => up_commit_flag,
                                p_validation_level      => up_validation_level,
                                p_msg_count             => up_msg_count,
                                p_msg_data              => up_msg_data,
                                p_return_status         => up_return_status,
                                p_adj_rec               => up_adj_rec,
                                p_chk_approval_limits   => up_chk_approval_limits,
                                p_check_amount          => up_check_amount,
                                p_move_deferred_tax     => up_move_deferred_tax,
                                p_new_adjust_number     => up_new_adjust_number,
                                p_new_adjust_id         => up_new_adjust_id,
                                p_called_from           => up_called_from,
                                p_old_adjust_id         => up_old_adjust_id
                                );

COMMIT;

IF up_msg_count >=1 THEN
FOR I IN 1..up_msg_count LOOP
dbms_output.put_line(I||'. '||SUBSTR(FND_MSG_PUB.Get(p_encoded => FND_API.G_FALSE ), 1, 255));
l_error_message := l_error_message||' . '||SUBSTR(FND_MSG_PUB.Get(p_encoded => FND_API.G_FALSE ), 1, 255);
END LOOP;
RAISE l_program_error;
END IF;

EXCEPTION
WHEN l_program_error THEN
     
     IF TRIM(l_error_message) = '.' THEN
       l_error_message := 'API Error - Waiting on Approval';
     END IF;
     
     dbms_output.put_line('l_error_message1  - '||l_error_message);
     FND_MESSAGE.SET_NAME('XXWC','XXWC_ADI_ERROR_MSG'); 
     FND_MESSAGE.SET_TOKEN('ERROR_MESSAGE',l_error_message); 
WHEN OTHERS THEN
     l_error_message := 'Unidentified error - '||SQLERRM;
     dbms_output.put_line('l_error_message2  - '||l_error_message);
     FND_MESSAGE.SET_NAME('XXWC','XXWC_ADI_ERROR_MSG'); 
     FND_MESSAGE.SET_TOKEN('ERROR_MESSAGE',l_error_message); 
END IMPORT;

END XXWC_AR_ADJUSTMENTS_WEBADI_PKG;
/