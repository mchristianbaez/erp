CREATE OR REPLACE PACKAGE BODY APPS.XXWCAR_UPDATE_ACCOUNT_PKG AS

/********************************************************************************
Procedure Name: UPDATE_CUST_PROFILE

Description : API to update customer profile.

HISTORY
===============================================================================
VERSION DATE          AUTHOR(S)          DESCRIPTION
------- -----------   --------------- -----------------------------------------
1.0     02/24/2014  Maharajan Shunmugam  TMS#20131126-00046
1.1     10/08/2014  Pattabhi Avula       TMS# 20141001-00058
********************************************************************************/

PROCEDURE update_profile_class (p_cust_account_profile_id     IN NUMBER
                             , p_object_version_num       IN NUMBER
                             , p_profile_class_id            IN NUMBER)
          IS
     p_customer_profile_rec_type     HZ_CUSTOMER_PROFILE_V2PUB.CUSTOMER_PROFILE_REC_TYPE;

     x_return_status                 VARCHAR2(2000);
     x_msg_count                     NUMBER;
     x_msg_data                      VARCHAR2(2000);
     l_object_version_num            NUMBER;
	  l_user_id                      NUMBER;  
      l_resp_id                      NUMBER;
      l_appl_id                      NUMBER ; 

      l_msg                    VARCHAR2 (150);
      l_distro_list            VARCHAR2 (75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
      l_sec                    VARCHAR2(200);

     CURSOR cur_cust_prof
         IS
     SELECT *
       FROM hz_customer_profiles
      WHERE cust_account_profile_id = p_cust_account_profile_id;

    BEGIN
      --------------------------------------------------------------------
      -- APPS Initializing
      -- Note: The values are hard-coded as this process will be executed
      -- from "HDS IT Operations Analyst - WC" responsibility
      --------------------------------------------------------------------
	  
	 l_user_id      :=fnd_global.user_id;             -- 10/08/2014 Added by Pattabhi for TMS# 20141001-00058 Canada OU testing
     l_resp_id    :=fnd_global.resp_id;               -- 10/08/2014 Added by Pattabhi for TMS# 20141001-00058 Canada OU testing
     l_appl_id    :=fnd_global.resp_appl_id;          -- 10/08/2014 Added by Pattabhi for TMS# 20141001-00058 Canada OU testing
    --  mo_global.init ('AR');
      fnd_global.apps_initialize (user_id           => l_user_id
                                , resp_id           => l_resp_id
                                , resp_appl_id      => l_appl_id);

    --  mo_global.set_policy_context ('S', 162);
     -- fnd_global.set_nls_context ('AMERICAN'); --*/ -- commented by pattabhi for TMS# 20141001-00058 : WC Canada: MultiOrg: Week of Oct 6 (Pattabhi)
      p_customer_profile_rec_type.cust_account_profile_id := p_cust_account_profile_id;
      p_customer_profile_rec_type.profile_class_id        := p_profile_class_id;
      l_object_version_num                                := p_object_version_num;


      FOR rec_cust_prof IN cur_cust_prof LOOP
--        p_customer_profile_rec_type.collector_id          := rec_cust_prof.collector_id;
--        p_customer_profile_rec_type.credit_analyst_id     := rec_cust_prof.credit_analyst_id;

   p_customer_profile_rec_type.cust_account_id                      :=   rec_cust_prof.cust_account_id              ;
   p_customer_profile_rec_type.status                               :=   rec_cust_prof.status                       ;
   p_customer_profile_rec_type.collector_id                         :=   rec_cust_prof.collector_id                 ;
   p_customer_profile_rec_type.credit_analyst_id                    :=   rec_cust_prof.credit_analyst_id            ;
   p_customer_profile_rec_type.credit_checking                      :=   rec_cust_prof.credit_checking              ;
   p_customer_profile_rec_type.next_credit_review_date              :=   rec_cust_prof.next_credit_review_date      ;
   p_customer_profile_rec_type.tolerance                            :=   rec_cust_prof.tolerance                    ;
   p_customer_profile_rec_type.discount_terms                       :=   rec_cust_prof.discount_terms               ;
   p_customer_profile_rec_type.dunning_letters                      :=   rec_cust_prof.dunning_letters              ;
   p_customer_profile_rec_type.interest_charges                     :=   rec_cust_prof.interest_charges             ;
   p_customer_profile_rec_type.send_statements                      :=   rec_cust_prof.send_statements              ;
   p_customer_profile_rec_type.credit_balance_statements            :=   rec_cust_prof.credit_balance_statements    ;
   p_customer_profile_rec_type.credit_hold                          :=   rec_cust_prof.credit_hold                  ;
   p_customer_profile_rec_type.site_use_id                          :=   rec_cust_prof.site_use_id                  ;
   p_customer_profile_rec_type.credit_rating                        :=   rec_cust_prof.credit_rating                ;
   p_customer_profile_rec_type.risk_code                            :=   rec_cust_prof.risk_code                    ;
   p_customer_profile_rec_type.standard_terms                       :=   rec_cust_prof.standard_terms               ;
   p_customer_profile_rec_type.override_terms                       :=   rec_cust_prof.override_terms               ;
   p_customer_profile_rec_type.dunning_letter_set_id                :=   rec_cust_prof.dunning_letter_set_id        ;
   p_customer_profile_rec_type.interest_period_days                 :=   rec_cust_prof.interest_period_days         ;
   p_customer_profile_rec_type.payment_grace_days                   :=   rec_cust_prof.payment_grace_days           ;
   p_customer_profile_rec_type.discount_grace_days                  :=   rec_cust_prof.discount_grace_days          ;
   p_customer_profile_rec_type.statement_cycle_id                   :=   rec_cust_prof.statement_cycle_id           ;
   p_customer_profile_rec_type.account_status                       :=   rec_cust_prof.account_status               ;
   p_customer_profile_rec_type.percent_collectable                  :=   rec_cust_prof.percent_collectable          ;
   p_customer_profile_rec_type.autocash_hierarchy_id                :=   rec_cust_prof.autocash_hierarchy_id        ;
   p_customer_profile_rec_type.attribute_category                   :=   rec_cust_prof.attribute_category           ;
   p_customer_profile_rec_type.attribute1                           :=   rec_cust_prof.attribute1                   ;
   p_customer_profile_rec_type.attribute2                           :=   rec_cust_prof.attribute2                   ;
   p_customer_profile_rec_type.attribute3                           :=   rec_cust_prof.attribute3                   ;
   p_customer_profile_rec_type.attribute4                           :=   rec_cust_prof.attribute4                   ;
   p_customer_profile_rec_type.attribute5                           :=   rec_cust_prof.attribute5                   ;
   p_customer_profile_rec_type.attribute6                           :=   rec_cust_prof.attribute6                   ;
   p_customer_profile_rec_type.attribute7                           :=   rec_cust_prof.attribute7                   ;
   p_customer_profile_rec_type.attribute8                           :=   rec_cust_prof.attribute8                   ;
   p_customer_profile_rec_type.attribute9                           :=   rec_cust_prof.attribute9                   ;
   p_customer_profile_rec_type.attribute10                          :=   rec_cust_prof.attribute10                  ;
   p_customer_profile_rec_type.attribute11                          :=   rec_cust_prof.attribute11                  ;
   p_customer_profile_rec_type.attribute12                          :=   rec_cust_prof.attribute12                  ;
   p_customer_profile_rec_type.attribute13                          :=   rec_cust_prof.attribute13                  ;
   p_customer_profile_rec_type.attribute14                          :=   rec_cust_prof.attribute14                  ;
   p_customer_profile_rec_type.attribute15                          :=   rec_cust_prof.attribute15                  ;
   p_customer_profile_rec_type.auto_rec_incl_disputed_flag          :=   rec_cust_prof.auto_rec_incl_disputed_flag  ;
   p_customer_profile_rec_type.tax_printing_option                  :=   rec_cust_prof.tax_printing_option          ;
   p_customer_profile_rec_type.charge_on_finance_charge_flag        :=   rec_cust_prof.charge_on_finance_charge_flag;
   p_customer_profile_rec_type.grouping_rule_id                     :=   rec_cust_prof.grouping_rule_id             ;
   p_customer_profile_rec_type.clearing_days                        :=   rec_cust_prof.clearing_days                ;
   p_customer_profile_rec_type.jgzz_attribute_category              :=   rec_cust_prof.jgzz_attribute_category      ;
   p_customer_profile_rec_type.jgzz_attribute1                      :=   rec_cust_prof.jgzz_attribute1              ;
   p_customer_profile_rec_type.jgzz_attribute2                      :=   rec_cust_prof.jgzz_attribute2              ;
   p_customer_profile_rec_type.jgzz_attribute3                      :=   rec_cust_prof.jgzz_attribute3              ;
   p_customer_profile_rec_type.jgzz_attribute4                      :=   rec_cust_prof.jgzz_attribute4              ;
   p_customer_profile_rec_type.jgzz_attribute5                      :=   rec_cust_prof.jgzz_attribute5              ;
   p_customer_profile_rec_type.jgzz_attribute6                      :=   rec_cust_prof.jgzz_attribute6              ;
   p_customer_profile_rec_type.jgzz_attribute7                      :=   rec_cust_prof.jgzz_attribute7              ;
   p_customer_profile_rec_type.jgzz_attribute8                      :=   rec_cust_prof.jgzz_attribute8              ;
   p_customer_profile_rec_type.jgzz_attribute9                      :=   rec_cust_prof.jgzz_attribute9              ;
   p_customer_profile_rec_type.jgzz_attribute10                     :=   rec_cust_prof.jgzz_attribute10             ;
   p_customer_profile_rec_type.jgzz_attribute11                     :=   rec_cust_prof.jgzz_attribute11             ;
   p_customer_profile_rec_type.jgzz_attribute12                     :=   rec_cust_prof.jgzz_attribute12             ;
   p_customer_profile_rec_type.jgzz_attribute13                     :=   rec_cust_prof.jgzz_attribute13             ;
   p_customer_profile_rec_type.jgzz_attribute14                     :=   rec_cust_prof.jgzz_attribute14             ;
   p_customer_profile_rec_type.jgzz_attribute15                     :=   rec_cust_prof.jgzz_attribute15             ;
   p_customer_profile_rec_type.global_attribute1                    :=   rec_cust_prof.global_attribute1            ;
   p_customer_profile_rec_type.global_attribute2                    :=   rec_cust_prof.global_attribute2            ;
   p_customer_profile_rec_type.global_attribute3                    :=   rec_cust_prof.global_attribute3            ;
   p_customer_profile_rec_type.global_attribute4                    :=   rec_cust_prof.global_attribute4            ;
   p_customer_profile_rec_type.global_attribute5                    :=   rec_cust_prof.global_attribute5            ;
   p_customer_profile_rec_type.global_attribute6                    :=   rec_cust_prof.global_attribute6            ;
   p_customer_profile_rec_type.global_attribute7                    :=   rec_cust_prof.global_attribute7            ;
   p_customer_profile_rec_type.global_attribute8                    :=   rec_cust_prof.global_attribute8            ;
   p_customer_profile_rec_type.global_attribute9                    :=   rec_cust_prof.global_attribute9            ;
   p_customer_profile_rec_type.global_attribute10                   :=   rec_cust_prof.global_attribute10           ;
   p_customer_profile_rec_type.global_attribute11                   :=   rec_cust_prof.global_attribute11           ;
   p_customer_profile_rec_type.global_attribute12                   :=   rec_cust_prof.global_attribute12           ;
   p_customer_profile_rec_type.global_attribute13                   :=   rec_cust_prof.global_attribute13           ;
   p_customer_profile_rec_type.global_attribute14                   :=   rec_cust_prof.global_attribute14           ;
   p_customer_profile_rec_type.global_attribute15                   :=   rec_cust_prof.global_attribute15           ;
   p_customer_profile_rec_type.global_attribute16                   :=   rec_cust_prof.global_attribute16           ;
   p_customer_profile_rec_type.global_attribute17                   :=   rec_cust_prof.global_attribute17           ;
   p_customer_profile_rec_type.global_attribute18                   :=   rec_cust_prof.global_attribute18           ;
   p_customer_profile_rec_type.global_attribute19                   :=   rec_cust_prof.global_attribute19           ;
   p_customer_profile_rec_type.global_attribute20                   :=   rec_cust_prof.global_attribute20           ;
   p_customer_profile_rec_type.global_attribute_category            :=   rec_cust_prof.global_attribute_category    ;
   p_customer_profile_rec_type.cons_inv_flag                        :=   rec_cust_prof.cons_inv_flag                ;
   p_customer_profile_rec_type.cons_inv_type                        :=   rec_cust_prof.cons_inv_type                ;
   p_customer_profile_rec_type.autocash_hierarchy_id_for_adr        :=   rec_cust_prof.autocash_hierarchy_id_for_adr;
   p_customer_profile_rec_type.lockbox_matching_option              :=   rec_cust_prof.lockbox_matching_option      ;
   p_customer_profile_rec_type.created_by_module                    :=   rec_cust_prof.created_by_module            ;
   p_customer_profile_rec_type.application_id                       :=   rec_cust_prof.application_id               ;
   p_customer_profile_rec_type.review_cycle                         :=   rec_cust_prof.review_cycle                 ;
   p_customer_profile_rec_type.last_credit_review_date              :=   rec_cust_prof.last_credit_review_date      ;
   p_customer_profile_rec_type.party_id                             :=   rec_cust_prof.party_id                     ;
   p_customer_profile_rec_type.credit_classification                :=   rec_cust_prof.credit_classification        ;
   p_customer_profile_rec_type.cons_bill_level                      :=   rec_cust_prof.cons_bill_level              ;
   p_customer_profile_rec_type.late_charge_calculation_trx          :=   rec_cust_prof.late_charge_calculation_trx  ;
   p_customer_profile_rec_type.credit_items_flag                    :=   rec_cust_prof.credit_items_flag            ;
   p_customer_profile_rec_type.disputed_transactions_flag           :=   rec_cust_prof.disputed_transactions_flag   ;
   p_customer_profile_rec_type.late_charge_type                     :=   rec_cust_prof.late_charge_type             ;
   p_customer_profile_rec_type.late_charge_term_id                  :=   rec_cust_prof.late_charge_term_id          ;
   p_customer_profile_rec_type.interest_calculation_period          :=   rec_cust_prof.interest_calculation_period  ;
   p_customer_profile_rec_type.hold_charged_invoices_flag           :=   rec_cust_prof.hold_charged_invoices_flag   ;
   p_customer_profile_rec_type.message_text_id                      :=   rec_cust_prof.message_text_id              ;
   p_customer_profile_rec_type.multiple_interest_rates_flag         :=   rec_cust_prof.multiple_interest_rates_flag ;
   p_customer_profile_rec_type.charge_begin_date                    :=   rec_cust_prof.charge_begin_date            ;
   p_customer_profile_rec_type.automatch_set_id                     :=   rec_cust_prof.automatch_set_id             ;

      END LOOP;

      --------------------------------------------------------------------
      -- API Call to update CustomerProfile
      --------------------------------------------------------------------
      hz_customer_profile_v2pub.update_customer_profile(p_init_msg_list              => fnd_api.g_true
                                      ,                 p_customer_profile_rec       => p_customer_profile_rec_type
                                      ,                 p_object_version_number      => l_object_version_num
                                      ,                 x_return_status              => x_return_status
                                      ,                 x_msg_count                  => x_msg_count
                                      ,                 x_msg_data                   => x_msg_data);

      --------------------------------------------------------------------
      -- Validate the status of API output
      --------------------------------------------------------------------
      IF x_msg_count >1 THEN
         FOR I IN 1..x_msg_count LOOP
           fnd_file.put_line(fnd_file.log,SUBSTR(FND_MSG_PUB.Get(p_encoded=>FND_API.G_FALSE ), 1, 255));
         END LOOP;

      --   p_error_message := SUBSTR(FND_MSG_PUB.Get(p_encoded=>FND_API.G_FALSE ), 1, 255);
      END IF;

    COMMIT;
    EXCEPTION
    WHEN OTHERS THEN
       fnd_file.put_line(fnd_file.log,'Error Message = '|| SQLERRM);
       -- p_error_message  := SQLERRM;
       xxcus_error_pkg.xxcus_error_main_api(p_called_from       => 'XXWCAR_UPDATE_CUSTOMER_PKG.UPDATE_PROFILE_CLASS'
                                            ,p_calling           => 'OTHERS exception'
                                            ,p_request_id        => NULL
                                            ,p_ora_error_msg     => substr(SQLERRM
                                                                          ,1
                                                                          ,2000)
                                            ,p_error_desc        => 'Error running XXWCAR_UPDATE_ACCOUNT_PKG.UPDATE_PROFILE_CLASS procedure with OTHERS Exception'
                                            ,p_distribution_list => 'HDSOracleDevelopers@hdsupply.com'
                                            ,p_module            => 'AR');
END update_profile_class;

PROCEDURE UPDATE_CASH_ACCOUNT    (p_errbuf    OUT      VARCHAR2
                                , p_retcode   OUT      NUMBER)
/********************************************************************************
Procedure Name: UPDATE_CASH_ACCOUNT

Description : API to update Customer Profile 

HISTORY
===============================================================================
VERSION DATE          AUTHOR(S)            DESCRIPTION
------- -----------   --------------- -----------------------------------------
1.0     02/24/2014    Maharajan Shunmugam  Initial Version
1.1     29/12/2017    Ashwin Sridhar       Added debug comments for TMS#20171229-00023
1.2     05/01/2018    Ashwin Sridhar       Added the SUBSTR condition for TMS#20171229-00023
********************************************************************************/

IS
     l_prof_amt_rec                 HZ_CUSTOMER_PROFILE_V2PUB.cust_profile_amt_rec_type;
     l_obj_version                  NUMBER;
     l_return_status                VARCHAR2(2000);
     l_msg_count                    NUMBER;
     l_msg_data                     VARCHAR2(2000);
     l_msg_index                    NUMBER;
     l_cust_account_id              NUMBER;
     l_class_id                     NUMBER;
     l_profile_id                   NUMBER;
     ln_request_id                  NUMBER:=fnd_global.conc_request_id; --Added by Ashwin.S for the TMS#20171229-00023 on 29-Dec-2017
	 l_sec                          VARCHAR2(250);                      --Added by Ashwin.S for the TMS#20171229-00023 on 29-Dec-2017
     p_cust_account_rec              HZ_CUST_ACCOUNT_V2PUB.CUST_ACCOUNT_REC_TYPE;
     p_cust_object_version_number    NUMBER;
     p_party_object_version_number   NUMBER;
     p_site_object_version_number    NUMBER;

     p_organization_rec          HZ_PARTY_V2PUB.ORGANIZATION_REC_TYPE;
     p_party_rec                 HZ_PARTY_V2PUB.PARTY_REC_TYPE;
     p_cust_site_use_rec         HZ_CUST_ACCOUNT_SITE_V2PUB.cust_site_use_rec_type;

   CURSOR c_acc_name
   IS
 SELECT UPPER(cust.account_name) acc_name,
       cust.cust_account_id,
       REPLACE(REPLACE('CASH/'||TRIM(replace(REGEXP_REPLACE(UPPER(cust.account_name),'[/,%,;,*,?,\,]',' '),'CASH','')),'/.','/'),'/-','/') update_acc_name,
       cust.account_number,
       cust.object_version_number cust_object_version_number,
       hp.party_id,
       hp.object_version_number party_object_version_number,
       pc.name Profile_Class_Name,
       RT.NAME payment_term
FROM   hz_parties hp,
       hz_cust_accounts cust,
       hz_customer_profiles prof,
       hz_cust_profile_classes pc,
       ra_terms rt
WHERE 1= 1
 AND hp.party_id = cust.party_id
 AND cust.cust_account_id = prof.cust_account_id (+)
 AND prof.profile_class_id = pc.profile_class_id
 AND prof.standard_terms = rt.term_id
 AND prof.site_use_id is  null
 AND LENGTH(account_number) != 3
 AND NVL(cust.status,'A') = 'A'
 AND SUBSTR(UPPER(ACCOUNT_NAME),1,5)  NOT LIKE 'CASH/%'
 AND SUBSTR(UPPER(ACCOUNT_NAME),1,6)  NOT LIKE 'CASH /%'
 AND pc.name = 'COD Customers'
 AND UPPER(cust.account_name) NOT LIKE '%WHITE%CAP%'
 AND RT.NAME IN ('COD','PRFRDCASH')
  AND hp.party_type = 'ORGANIZATION';

    CURSOR loc_cur(p_customer_id  IN NUMBER)
        IS 
     SELECT hcsu.object_version_number          
          , hcsu.site_use_id
          , hcsu.cust_acct_site_id  
          , hcsu.location      
           /*Added SUBSTR condition for the below location field by Ashwin.S on 05-Jan-2018 for TMS#20171229-00023*/		  
          ,SUBSTR(REPLACE(REPLACE('CASH/'||TRIM(replace(REGEXP_REPLACE(UPPER(hcsu.location),'[/,%,;,*,?,\,]',' '),'CASH','')),'/.','/'),'/-','/'),1,38) update_loc_name
       FROM hz_parties                 hp
           ,hz_party_sites             hps
           ,hz_cust_accounts            hca
           ,apps.hz_cust_acct_sites              hcas
           ,apps.hz_cust_site_uses               hcsu
      WHERE 1 = 1       
        AND hca.cust_account_id               = p_customer_id
        AND hp.party_id                  = hca.party_id
        AND hp.party_id                       = hps.party_id
        AND hcas.party_site_id                = hps.party_site_id
        AND hcas.cust_acct_site_id            = hcsu.cust_acct_site_id
        AND SUBSTR(UPPER(hcsu.location),1,5)  NOT LIKE 'CASH/%'
        AND SUBSTR(UPPER(hcsu.location),1,6)  NOT LIKE 'CASH /%'
        AND NVL(hp.status,'A')              = 'A'
        AND NVl(hca.status,'A')              = 'A';

BEGIN

   l_sec := 'Begin...'; --Added by Ashwin.S for the TMS#20171229-00023 on 29-Dec-2017
      --------------------------------------------------------------------
      -- API for cust account and party
      --------------------------------------------------------------------
   
BEGIN 

fnd_file.put_line (
         fnd_file.log,
         'Start processing cust account API');

  l_sec := 'Starting the Loop...'; --Added by Ashwin.S for the TMS#20171229-00023 on 29-Dec-2017
  
FOR r_acc_name IN c_acc_name
LOOP
    p_cust_account_rec.cust_account_id := r_acc_name.cust_account_id;
    p_cust_account_rec.account_name    := r_acc_name.update_acc_name;
    p_cust_object_version_number       := r_acc_name.cust_object_version_number;
	
	fnd_file.put_line (fnd_file.log,'Customer ID '||r_acc_name.cust_account_id);  --Added by Ashwin.S for the TMS#20171229-00023 on 29-Dec-2017
	fnd_file.put_line (fnd_file.log,'Account Name '||r_acc_name.update_acc_name); --Added by Ashwin.S for the TMS#20171229-00023 on 29-Dec-2017
	fnd_file.put_line (fnd_file.log,'Account Number '||r_acc_name.account_number); --Added by Ashwin.S for the TMS#20171229-00023 on 29-Dec-2017
	          	 
	l_sec := 'Starting the update cust account for '||r_acc_name.cust_account_id||'-'||'Account No.'||r_acc_name.account_number; --Added by Ashwin.S for the TMS#20171229-00023 on 29-Dec-2017
				
      --------------------------------------------------------------------
      -- Call API to update cust account name
      --------------------------------------------------------------------

hz_cust_account_v2pub.update_cust_account ( p_init_msg_list           => fnd_api.g_true,
                                p_cust_account_rec        => p_cust_account_rec,
                                p_object_version_number   => p_cust_object_version_number,
                                x_return_status           => l_return_status,
                                x_msg_count               => l_msg_count,
                                x_msg_data                => l_msg_data);


      --------------------------------------------------------------------
      -- Validate the status of API output
      --------------------------------------------------------------------
      IF l_msg_count >1 THEN
         FOR I IN 1..l_msg_count LOOP
           fnd_file.put_line(fnd_file.log,SUBSTR(FND_MSG_PUB.Get(p_encoded=>FND_API.G_FALSE ), 1, 255));
         END LOOP;
      END IF;

fnd_file.put_line (
         fnd_file.log,
         'cust account API status'||l_return_status);     

     IF l_return_status = 'S' THEN   

      l_sec := 'Starting the update for organization for party ID '||r_acc_name.party_id; --Added by Ashwin.S for the TMS#20171229-00023 on 29-Dec-2017	 

      --------------------------------------------------------------------
      -- Process update Organization name
      --------------------------------------------------------------------

       fnd_file.put_line (
         fnd_file.log,
         'Start processing Party-update organization API');
       BEGIN
           p_party_rec.party_id             := r_acc_name.party_id;
           p_organization_rec.organization_name  := r_acc_name.update_acc_name;
           p_organization_rec.party_rec      := p_party_rec;
           p_party_object_version_number        := r_acc_name.party_object_version_number;
		   
		   fnd_file.put_line (fnd_file.log,'Party ID '||r_acc_name.party_id);  --Added by Ashwin.S for the TMS#20171229-00023 on 29-Dec-2017
		   fnd_file.put_line (fnd_file.log,'Party Account Name '||r_acc_name.update_acc_name);  --Added by Ashwin.S for the TMS#20171229-00023 on 29-Dec-2017

             
 HZ_PARTY_V2PUB.update_organization ( p_init_msg_list             => FND_API.G_FALSE,
                       p_organization_rec         => p_organization_rec,
                       p_party_object_version_number     => p_party_object_version_number,
                       x_profile_id             => l_profile_id,
                       x_return_status             => l_return_status,
                       x_msg_count             => l_msg_count,
                       x_msg_data             => l_msg_data);
           
  IF l_msg_count >1 THEN
         FOR I IN 1..l_msg_count LOOP
           fnd_file.put_line(fnd_file.log,SUBSTR(FND_MSG_PUB.Get(p_encoded=>FND_API.G_FALSE ), 1, 255));
         END LOOP;
      END IF;

fnd_file.put_line (
         fnd_file.log,
         'Party -Organization update API status'||l_return_status);     

     IF l_return_status = 'S' THEN        
      COMMIT;
     ELSE 
     ROLLBACK;
     fnd_file.put_line (
         fnd_file.log,
         'Party -Organization update API Failed for Organization : '|| r_acc_name.acc_name);   
     END IF;
  END;


     --------------------------------------------------------------------
      -- Process update location
      --------------------------------------------------------------------
     BEGIN
	 
       FOR get_loc IN loc_cur(r_acc_name.cust_account_id)
       LOOP
       BEGIN
	   
	   l_sec := 'Starting the update for Location for site use ID'||get_loc.site_use_id; --Added by Ashwin.S for the TMS#20171229-00023 on 29-Dec-2017	 
	   
       p_cust_site_use_rec.site_use_id := get_loc.site_use_id;    
       p_cust_site_use_rec.cust_acct_site_id := get_loc.cust_acct_site_id; 
       p_cust_site_use_rec.location    := get_loc.update_loc_name; 
       p_site_object_version_number    := get_loc.object_version_number;
	   
	   fnd_file.put_line (fnd_file.log,'Site Use ID '||get_loc.site_use_id);  --Added by Ashwin.S for the TMS#20171229-00023 on 29-Dec-2017
	   fnd_file.put_line (fnd_file.log,'Cust Acct Site ID '||get_loc.cust_acct_site_id);  --Added by Ashwin.S for the TMS#20171229-00023 on 29-Dec-2017
	   
       HZ_CUST_ACCOUNT_SITE_V2PUB.update_cust_site_use (p_init_msg_list             => FND_API.G_FALSE,
                                                        p_cust_site_use_rec        => p_cust_site_use_rec,
                                                        p_object_version_number        => p_site_object_version_number,
                                                        x_return_status             => l_return_status,
                                           x_msg_count                 => l_msg_count,
                                           x_msg_data                  => l_msg_data);

IF l_msg_count >1 THEN
         FOR I IN 1..l_msg_count LOOP
           fnd_file.put_line(fnd_file.log,SUBSTR(FND_MSG_PUB.Get(p_encoded=>FND_API.G_FALSE ), 1, 255));
         END LOOP;
      END IF;


fnd_file.put_line (
         fnd_file.log,
         'Location update API status'||l_return_status);   

 IF l_return_status = 'S' THEN        
      COMMIT;
     ELSE 
     ROLLBACK;
     fnd_file.put_line (
         fnd_file.log,
         'Location update API Failed for Organization : '|| r_acc_name.acc_name);   
     END IF;
  END;
END LOOP;
END;


  ELSE
      fnd_file.put_line (
         fnd_file.log,
         'Cust account update API Failed for account : '|| r_acc_name.acc_name);  
  END IF; 
END LOOP;

  l_sec := 'End of Procedure...'; --Added by Ashwin.S for the TMS#20171229-00023 on 29-Dec-2017

END;

EXCEPTION
 WHEN OTHERS THEN
      fnd_file.put_line(fnd_file.log,'Error Message = '|| SQLERRM);
	 
       xxcus_error_pkg.xxcus_error_main_api(p_called_from       => 'XXWCAR_UPDATE_ACCOUNT_PKG.UPDATE_CASH_ACCOUNT'
                                            ,p_calling           => l_sec
                                            ,p_request_id        => ln_request_id --Added by Ashwin.S for the TMS#20171229-00023 on 29-Dec-2017
                                            ,p_ora_error_msg     => substr(SQLERRM
                                                                          ,1
                                                                          ,2000)
                                            ,p_error_desc        => 'Error running XXWCAR_UPDATE_ACCOUNT_PKG.UPDATE_CASH_ACCOUNT procedure with OTHERS Exception'
                                            ,p_distribution_list => 'HDSOracleDevelopers@hdsupply.com'
                                            ,p_module            => 'AR');
END  UPDATE_CASH_ACCOUNT;

END XXWCAR_UPDATE_ACCOUNT_PKG;
/