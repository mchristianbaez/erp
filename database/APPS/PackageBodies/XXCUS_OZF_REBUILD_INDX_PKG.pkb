create or replace package body apps.XXCUS_OZF_REBUILD_INDX_PKG as
/*
 -- ESMS: 233840
 -- Date: 03-JAN-2014
 -- Author: Balaguru Seshadri
 -- Scope: This package will enhance HD Supply to rebuild the trade management indexes, run OZF schema and extended stats.
*/
  --
  procedure print_log(p_message in varchar2) is
  begin 
   if fnd_global.conc_request_id >0 then
    fnd_file.put_line(fnd_file.log, p_message);
   else
    dbms_output.put_line(p_message);
   end if;
  exception
   when others then
    fnd_file.put_line(fnd_file.log, 'Issue in print_log routine ='||sqlerrm);
  end print_log; 
 --
  procedure main (
    retcode               out number
   ,errbuf                out varchar2
  ) is
    --
    type lc_refcursor is ref cursor;
    my_offers             lc_refcursor; 
    REBATES_SQL           varchar2(20000); 
   --
   CURSOR sla_partitions IS
    select 'ALTER INDEX '||owner||'.'||uip.index_name||' REBUILD PARTITION '||uip.partition_name ||' PARALLEL 8' idx_sql
    from dba_ind_partitions  uip
        ,dba_tab_partitions  utp
        ,dba_indexes         ui
    where 1 =1
      and uip.partition_name  =utp.partition_name
      and ui.index_name       =uip.index_name
      and ui.table_name       =utp.table_name
      and ui.owner            ='XLA'
      and uip.partition_name  ='OZF'
      order by uip.index_name;         
   --  
   CURSOR ozf_stack IS
    select 'ALTER INDEX '||OWNER||'.'||INDEX_NAME||' REBUILD PARALLEL 8' IDX_SQL
    from   all_indexes A
    where  1 =1
      and  A.owner ='OZF'
      and  EXISTS
       (
         select '1'
         from   all_tables
         where  1 =1
           and  owner =A.owner
           and  temporary ='N'
       )
    order by index_name;
   --
   CURSOR my_idx_group IS
    select thread_type, thread_identifier
    from   xxcus.xxcus_ozf_rebuild_indx
    where  1 =1
    group by thread_type, thread_identifier
    order by thread_type asc, to_number(thread_identifier) asc ;  
   --
   lc_request_data VARCHAR2(20) :='';
   ln_request_id   NUMBER :=0;
   n_req_id NUMBER :=0;
   l_org_id NUMBER :=0; 
   n_count_child_requests NUMBER :=0; 
   N_conc_pgm_id NUMBER :=0;
   N_running_count NUMBER :=0;
   b_keep_going BOOLEAN;
   n_program_app_id NUMBER :=0;
   N_user_id NUMBER :=0;
   v_email       fnd_user.email_address%type :=Null;
   v_cp_long_name   fnd_concurrent_programs_tl.user_concurrent_program_name%type :=Null; 
   v_cp_phase       Varchar2(40) :=Null;
   v_cp_status      Varchar2(40) :=Null; 
   v_child_requests Varchar2(240) :=Null;
   output_file_id   UTL_FILE.FILE_TYPE;
   v_path varchar2(80);
   p_period_id NUMBER :=0;
   n_high_priority NUMBER :=40;
   n_low_priority NUMBER :=50;
   N_OZF_SLA_MAX_THREADS NUMBER :=4;
   N_OZF_MAX_THREADS NUMBER :=50;  
  --
  begin  
   --
   lc_request_data :=fnd_conc_global.request_data;
   --
   n_req_id :=fnd_global.conc_request_id;
   --
   if lc_request_data is null then     
       --
       execute immediate 'TRUNCATE TABLE xxcus.xxcus_ozf_rebuild_indx';
       --       
       lc_request_data :='1';       
       --l_org_id :=mo_global.get_current_org_id;    
       --
       N_user_id :=fnd_global.user_id;         
       --
       print_log('');           
      --
      begin
       --
       n_count_child_requests :=N_OZF_SLA_MAX_THREADS;
       N_running_count :=1;
       --
        for rec in sla_partitions loop
         --
         if n_running_count <=N_OZF_SLA_MAX_THREADS then
          n_running_count :=n_running_count+1;
         else
          n_running_count :=0;
          n_count_child_requests :=n_count_child_requests + N_OZF_SLA_MAX_THREADS;                    
         end if;
         --
         insert into xxcus.xxcus_ozf_rebuild_indx
          (
            thread_type            
           ,thread_identifier
           ,index_sql_statement
          )
         values
          (
            'OZF_SLA'                          --thread_type
            ,to_char(n_count_child_requests)   --thread_identifier
            ,rec.idx_sql                       --index sql statement
          );
          --print_log(rec.idx_sql);
        end loop;
       --
       n_count_child_requests :=N_OZF_MAX_THREADS;
       N_running_count :=1;
       --
        for rec in ozf_stack loop
         --
         if n_running_count <=N_OZF_MAX_THREADS then
          n_running_count :=n_running_count+1;
         else
          n_running_count :=0;
          n_count_child_requests :=n_count_child_requests +N_OZF_MAX_THREADS;          
         end if;
         --
         insert into xxcus.xxcus_ozf_rebuild_indx
          (
            thread_type            
           ,thread_identifier
           ,index_sql_statement
          )
         values
          (
            'OZF_APP'                          --thread_type
            ,to_char(n_count_child_requests)   --thread_identifier
            ,rec.idx_sql                       --index sql statement
          );
         --print_log(rec.idx_sql);
        end loop;
       --       
         b_keep_going :=TRUE;
       --             
      exception
       when no_data_found then
        --
         print_log('@101, Failed to insert table XXCUS.XXCUS_OZF_REBUILD_INDX, message ='||sqlerrm);
         b_keep_going :=FALSE;
        --            
       when others then
        --
         print_log('@102, Failed to insert table XXCUS.XXCUS_OZF_REBUILD_INDX, message ='||sqlerrm);
         b_keep_going :=FALSE;
        --        
      end; 
      --
      if (b_keep_going) then
           --    
           for indx in my_idx_group loop                                   
            --                                   
            ln_request_id :=fnd_request.submit_request
                  (
                   application      =>'XXCUS',
                   program          =>'XXCUS_REBUILD_OZF_STACK_DETAIL',
                   description      =>'',
                   start_time       =>'',
                   sub_request      =>TRUE,
                   argument1        =>indx.thread_type,                                                                                                                                                                                                                                                                                  
                   argument2        =>indx.thread_identifier                 
                  ); 
                          
              if ln_request_id >0 then 
                print_log(
                            'Submitted HDS Rebates: Rebuild OZF Stack -Detail'
                                                 ||', thread_type ='
                                                 ||indx.thread_type
                                                 ||', thread_identifier ='
                                                 ||indx.thread_identifier
                                                 ||', Request Id ='
                                                 ||ln_request_id
                         );
              else
                print_log(
                             'Failed to submit HDS Rebates: Rebuild OZF Stack -Detail, thread type ='
                           ||indx.thread_type
                           ||', thread identifier ='
                           ||indx.thread_identifier
                         );
              end if;     
                       
             fnd_conc_global.set_req_globals(conc_status =>'PAUSED' ,request_data =>lc_request_data);
             --
             lc_request_data :=to_char(to_number(lc_request_data)+1);  
             --    
           end loop;            
           --       
      else
       print_log('Variable b_keep_going is set to FALSE, message ='||sqlerrm);
      end if;
      --                                  
   else     
    --           
      retcode :=0;
      errbuf :='Child request[s] completed. Exit HDS Rebates: Rebuild OZF Stack -Master';
      print_log(errbuf);
    --
   end if;
  exception
   when others then
    print_log('Error in caller Apps.XXCUS_OZF_REBUILD_INDX_PKG.Main ='||sqlerrm);
    rollback;
  end main;
  --
  procedure child (
    retcode               out number
   ,errbuf                out varchar2
   ,p_thread_type         in  varchar2
   ,p_thread_identifier   in  varchar2   
  ) is 
  --
   cursor rebuild_sql is
   select index_sql_statement idx_sql 
   from   xxcus.xxcus_ozf_rebuild_indx
   where  1 =1
     and  thread_type        =p_thread_type
     and  thread_identifier  =p_thread_identifier;
  --
  v_sql varchar2(4000);
  --
  begin 
   --
   print_log('');
   print_log('Thread_Type ='||p_thread_type);     
   print_log('Thread_Identifier ='||p_thread_identifier);   
   print_log('');
   --
   for rec in rebuild_sql loop 
    --
    v_sql :=rec.idx_sql;
    --
    begin
     --  
       execute immediate v_sql;
     --
       print_log('Index SQL, '||v_sql||' completed successfully');
     --
    exception
     when others then
      print_log('ERROR in rebuild of index, current SQL ='||v_sql||', message ='||sqlerrm);
    end;
    --
   end loop;
   --
   print_log('');
   --   
  exception
   when others then
    print_log('Error in caller Apps.XXCUS_OZF_REBUILD_INDX_PKG.child, SQL ='||v_sql);   
    print_log('Error in caller Apps.XXCUS_OZF_REBUILD_INDX_PKG.child ='||sqlerrm);
    rollback;
  end child; 
  --
  procedure ext_stats (
    retcode               out number
   ,errbuf                out varchar2   
  ) is 
  --
   cursor extended_stats_table is
   select a.owner, a.table_name
   from   all_tables a, xxcus.xxcus_ozf_ext_stats_table b
   where  1 =1
     and  a.owner      =b.ext_stats_owner
     and  a.table_name =b.ext_stats_table; 
  --
  v_table_name varchar2(30);
  --
  begin 
   --
   print_log('');     
   --
    begin
     --
     for rec in extended_stats_table loop
      --
      v_table_name :=rec.table_name;
      --
        DBMS_STATS.UNLOCK_TABLE_STATS(rec.owner, rec.table_name);
        print_log('Table ='||rec.table_name||' unlocked for gather stats.');
      --
        DBMS_STATS.GATHER_TABLE_STATS
        (
            ownname     =>rec.owner
           ,tabname     =>rec.table_name
           ,method_opt  =>'for all columns size repeat'
           ,degree      =>DBMS_STATS.AUTO_DEGREE
        );
        print_log('Successfully gathered table stats for table '||rec.table_name);        
       --
        DBMS_STATS.LOCK_TABLE_STATS(rec.owner, rec.table_name);
        print_log('Table ='||rec.table_name||' locked for gather stats.');        
       --   
       print_log('');         
     end loop;
     --
    end;   
   --
   print_log('');
   --   
  exception
   when others then
    print_log('Error in caller Apps.XXCUS_OZF_REBUILD_INDX_PKG.ext_stats, table_name ='||v_table_name);   
    print_log('Error in caller Apps.XXCUS_OZF_REBUILD_INDX_PKG.ext_stats ='||sqlerrm);
    rollback;
  end ext_stats; 
  --  
end XXCUS_OZF_REBUILD_INDX_PKG;
/
