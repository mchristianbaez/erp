CREATE OR REPLACE PACKAGE BODY APPS."XXCUSCE_INTERFACE_PKG" AS

  v_req_id             NUMBER := fnd_global.conc_request_id;
  v_set_req_status     BOOLEAN;
  v_req_phase          VARCHAR2(80);
  v_req_status         VARCHAR2(80);
  v_dev_phase          VARCHAR2(30);
  v_dev_status         VARCHAR2(30);
  v_message            VARCHAR2(255);
  v_interval           NUMBER := 10; -- In seconds
  v_max_time           NUMBER := 15000; -- In seconds
  v_completion_status  VARCHAR2(30) := 'NORMAL';
  v_error_message      VARCHAR2(3000);
  v_file_dir           VARCHAR2(100);
  v_dir                VARCHAR2(100);
  v_instance           VARCHAR2(8);
  v_shortname          VARCHAR2(30);
  l_can_submit_request BOOLEAN := TRUE;
  l_globalset          VARCHAR2(100);
  l_err_msg            VARCHAR2(3000);
  l_err_code           NUMBER;
  l_sec                VARCHAR2(255);
  l_message            VARCHAR2(150);
  l_statement          VARCHAR2(9000);

  /**************************************************************************
  File Name: XXCUSCE_INTERFACE_PKG
  
  PROGRAM TYPE: SQL Script
  
  PURPOSE:      package Specification is used to load lockbox and acm files
                delivered to the /xx_iface/prd/xtr/inbound/.  The file name
        will be inserted into a trigger table to know what is available
                to load by BizTalk.  These files will be used by Cash Management
                for the bank reconciliation process
  HISTORY
  =============================================================================
         Last Update Date : 09/17/2008
  =============================================================================
  =============================================================================
  VERSION DATE          AUTHOR(S)          DESCRIPTION
  ------- -----------   -----------------  ------------------------------------
  1.0     27-Jun-2011   Kathy Poling       Creation this package taken R11 and
                                           modified for R12
  
  =============================================================================
  *****************************************************************************/
  PROCEDURE load_ce_files(errbuf OUT VARCHAR2, retcode OUT NUMBER) IS
  
  BEGIN
  
    SELECT lower(NAME) INTO v_instance FROM v$database;
  
    v_file_dir := '/xx_iface/' || v_instance || '/xtr/inbound/eclipse/';
  
    --  Setup parameters for running FND JOBS!
    /*    l_can_submit_request := XXCUS_misc_pkg.set_responsibility('XTRINTERFACE',
                                                                  'HSI Treasury Control');
    
        IF l_can_submit_request THEN
          l_globalset := 'Global Variables are set.';
    
        ELSE
    
          l_globalset := 'Global Variables are not set.';
          l_sec       := 'Global Variables are not set for the Responsibility of XXCUS_CON and the User of Conversion.';
          fnd_file.put_line(fnd_file.log, l_sec);
          fnd_file.put_line(fnd_file.output, l_sec);
          RAISE PROGRAM_ERROR;
        END IF;
    */
    fnd_file.put_line(fnd_file.log, l_globalset);
    fnd_file.put_line(fnd_file.output, l_globalset);
    --  END OF Setup parameters for running FND JOBS!
    --
    -- BEGIN
    FOR c_files IN (SELECT stg_id, file_name
                      FROM xxcus.xxcusce_file_trg_tbl
                     WHERE status = 'PENDING'
                       AND file_name LIKE 'CELB%')
    LOOP
    
      -- DEBUG NAME
      v_message := 'Update staging table';
      l_message := v_message;
    
      --
      fnd_file.put_line(fnd_file.log, 'Input parameters are....:');
      fnd_file.put_line(fnd_file.log
                       ,'File Name...... = ' || c_files.file_name);
      fnd_file.put_line(fnd_file.log, 'Stg_id...... = ' || c_files.stg_id);
      fnd_file.put_line(fnd_file.output
                       ,'File Name:  ' || c_files.file_name || '   ' ||
                        'Stg_id:  ' || c_files.stg_id);
    
      -- DEBUG NAME
      v_message := 'Submitting HDS SQL*Loader-CE 999 files short name:  XXCUSCELB';
      l_message := v_message;
    
      IF c_files.file_name LIKE 'CELB%'
      THEN
        v_shortname := 'XXCUSCELB';
      END IF;
      --
      -- Submit the SQL loader program to load the lockbox and acm files into a table
      --
      fnd_file.put_line(fnd_file.log, v_message);
      v_req_id := apps.fnd_request.submit_request('XXCUS'
                                                 ,v_shortname
                                                 ,NULL
                                                 ,SYSDATE
                                                 ,FALSE
                                                 ,v_file_dir ||
                                                  c_files.file_name);
      COMMIT;
    
      UPDATE xxcus.xxcusce_file_trg_tbl
         SET status        = 'PROCESSING'
            ,creation_date = SYSDATE
            ,request_id    = v_req_id
       WHERE stg_id = c_files.stg_id;
      COMMIT;
    
      -- get the status of the child process.
      IF v_req_id != 0
      THEN
        --
        -- Wait for the previous concurrent request (XXCUSCELB) to complete
        --
        IF fnd_concurrent.wait_for_request(v_req_id
                                          ,v_interval
                                          ,v_max_time
                                          ,v_req_phase
                                          ,v_req_status
                                          ,v_dev_phase
                                          ,v_dev_status
                                          ,v_message)
        THEN
          v_error_message := chr(10) || 'ReqID=' || v_req_id || ' DPhase ' ||
                             v_dev_phase || ' DStatus ' || v_dev_status ||
                             chr(10) || ' MSG - ' || v_message;
          -- Error Returned
          IF v_dev_phase != 'COMPLETE' OR v_dev_status != 'NORMAL'
          THEN
            l_statement := 'An error occured in the running the Lockbox SQL Loader' ||
                           v_error_message || '.';
            fnd_file.put_line(fnd_file.log, l_statement);
            fnd_file.put_line(fnd_file.output, l_statement);
            RAISE program_error;
          
            UPDATE xxcus.xxcusce_file_trg_tbl
               SET status           = 'ERROR'
                  ,request_id       = v_req_id
                  ,last_update_date = SYSDATE
             WHERE stg_id = c_files.stg_id;
            COMMIT;
          
          END IF;
        
          -- Then Success!
          UPDATE xxcus.xxcusce_file_trg_tbl
             SET status           = 'COMPLETED'
                ,request_id       = v_req_id
                ,last_update_date = SYSDATE
           WHERE stg_id = c_files.stg_id;
          COMMIT;
        
          insert_ce_summary(errbuf, retcode, v_req_id, c_files.file_name);
        
        ELSE
          l_statement := '2.  An error occured running the Lockbox SQL Loader' ||
                         v_error_message || '.';
          fnd_file.put_line(fnd_file.log, l_statement);
          fnd_file.put_line(fnd_file.output, l_statement);
          RAISE program_error;
        
          UPDATE xxcus.xxcusce_file_trg_tbl
             SET status           = 'ERROR'
                ,request_id       = v_req_id
                ,last_update_date = SYSDATE
           WHERE stg_id = c_files.stg_id;
          COMMIT;
        END IF;
      
      ELSE
        l_statement := '3.  An error occured when trying to submitting the XXCUS CE INTERFACE Lockbox SQL Loader';
        fnd_file.put_line(fnd_file.log, l_statement);
        fnd_file.put_line(fnd_file.output, l_statement);
        RAISE program_error;
      
        UPDATE xxcus.xxcusce_file_trg_tbl
           SET status           = 'ERROR'
              ,request_id       = v_req_id
              ,last_update_date = SYSDATE
         WHERE stg_id = c_files.stg_id;
        COMMIT;
      
      END IF;
    
    END LOOP;
  
    v_set_req_status := fnd_concurrent.set_completion_status(v_completion_status
                                                            ,NULL);
    fnd_file.new_line(fnd_file.log, 1);
    fnd_file.put_line(fnd_file.log, 'End of File Processing...');
  
  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;
      l_err_code := 2;
      l_err_msg  := l_sec;
      l_err_msg  := l_err_msg || ' ERROR ' || substr(SQLERRM, 1, 2000);
      UPDATE xxcus.xxcusce_file_trg_tbl
         SET status           = 'ERROR'
            ,request_id       = v_req_id
            ,last_update_date = SYSDATE
       WHERE request_id = v_req_id;
    
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode         := l_err_code;
      errbuf          := l_err_msg;
      g_err_callpoint := l_message;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => g_err_callfrom
                                          ,p_calling           => g_err_callpoint
                                          ,p_request_id        => v_req_id
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'Error running XXCUSCE INTERFACE package with PROGRAM ERROR'
                                          ,p_distribution_list => g_distro_list
                                          ,p_module            => 'CE');
    
      fnd_file.put_line(fnd_file.output, 'Need fix the errors!');
    
    WHEN OTHERS THEN
      ROLLBACK;
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := l_sec || l_err_msg || ' ERROR ' || SQLCODE ||
                    substr(SQLERRM, 1, 2000);
      UPDATE xxcus.xxcusce_file_trg_tbl
         SET status           = 'ERROR'
            ,request_id       = v_req_id
            ,last_update_date = SYSDATE
       WHERE request_id = v_req_id;
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode         := l_err_code;
      errbuf          := l_err_msg;
      g_err_callpoint := l_message;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => g_err_callfrom
                                          ,p_calling           => g_err_callpoint
                                          ,p_request_id        => v_req_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => 'Error running XXCUSCE INTERFACE package with OTHERS Exception'
                                          ,p_distribution_list => g_distro_list
                                          ,p_module            => 'CE');
    
  END load_ce_files;
  ------------------------------------------------------------------------------------------
  PROCEDURE insert_ce_summary(errbuf    OUT VARCHAR2
                             ,retcode   OUT NUMBER
                             ,p_request IN NUMBER
                             ,p_file    IN VARCHAR2) IS
  
    CURSOR c_insert IS
    
      SELECT bank_account_number
            ,trx_type
            ,trx_date
            ,currency_code
            ,status
            ,status_dsp
            ,SUM(amount) amount
            ,cleared_amount
            ,charges_amount
            ,error_amount
            ,acctd_amount
            ,acctd_cleared_amount
            ,acctd_charges_amount
            ,acctd_error_amount
            ,gl_date
            ,cleared_date
            ,
             --creation_date,
             created_by
        FROM xxcus.xxcusce_999_interface_tbl
       WHERE summerized = 'N'
         AND request_id = -1
         AND file_name = 'NADA'
       GROUP BY bank_account_number
               ,trx_type
               ,trx_date
               ,currency_code
               ,status
               ,status_dsp
               ,cleared_amount
               ,charges_amount
               ,error_amount
               ,acctd_amount
               ,acctd_cleared_amount
               ,acctd_charges_amount
               ,acctd_error_amount
               ,gl_date
               ,cleared_date
               ,
                --creation_date,
                created_by;
  
  BEGIN
    FOR c IN c_insert
    LOOP
    
      INSERT INTO xxcus.xxcusce_999_inter_sum_tbl
        (trx_id
        ,bank_account_number
        ,trx_type
        ,trx_date
        ,currency_code
        ,status
        ,status_dsp
        ,amount
        ,cleared_amount
        ,charges_amount
        ,error_amount
        ,acctd_amount
        ,acctd_cleared_amount
        ,acctd_charges_amount
        ,acctd_error_amount
        ,gl_date
        ,cleared_date
        ,creation_date
        ,created_by
        ,last_update_date
        ,last_updated_by
        ,request_id
        ,file_name)
      VALUES
        (apps.xxcusce_interface_s.nextval
        ,c.bank_account_number
        ,c.trx_type
        ,c.trx_date
        ,c.currency_code
        ,c.status
        ,c.status_dsp
        ,c.amount
        ,c.cleared_amount
        ,c.charges_amount
        ,c.error_amount
        ,c.acctd_amount
        ,c.acctd_cleared_amount
        ,c.acctd_charges_amount
        ,c.acctd_error_amount
        ,c.gl_date
        ,c.cleared_date
        ,SYSDATE
        , -- c.creation_date,
         c.created_by
        ,NULL
        ,NULL
        ,p_request
        ,p_file);
    
      UPDATE xxcus.xxcusce_999_interface_tbl
         SET summerized = 'Y', request_id = p_request, file_name = p_file
       WHERE summerized = 'N';
      COMMIT;
    END LOOP;
  
  EXCEPTION
  
    WHEN OTHERS THEN
      ROLLBACK;
      l_err_code := 2;
      l_err_msg  := ' ERROR ' || SQLCODE || substr(SQLERRM, 1, 2000);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode := l_err_code;
      errbuf  := l_err_msg;
    
      COMMIT;
    
  END insert_ce_summary;

  /**************************************************************************
  File Name: XXCUS_CE_INTERFACE
  
  PROGRAM TYPE: SQL Script
  
  PURPOSE:      package Specification is used to load Eclipse file names
                delivered to the /xx_iface/prd/xtr/inbound/eclipse.  The file name
                will be inserted into a trigger table to know what is available
                to load that was FTP's by Eclipse.  These files will be used by
                Cash Management for the bank reconciliation process
                Concurrent request created "HDS CE Load Files From Eclipse"
                to load the file names into a staging table
  HISTORY
  =============================================================================
         Last Update Date : 09/17/2008
  =============================================================================
  =============================================================================
  VERSION DATE          AUTHOR(S)          DESCRIPTION
  ------- -----------   -----------------  ------------------------------------
  1.0     27-Jun-2011   Kathy Poling       Creation this package taken R11 and
                                           modified for R12
  
  =============================================================================
  *****************************************************************************/
  PROCEDURE load_eclipse_file_names(errbuf OUT VARCHAR2, retcode OUT NUMBER) IS
  
  BEGIN
  
    SELECT lower(NAME) INTO v_instance FROM v$database;
  
    v_file_dir :=  'ORACLE_INT_ECLIPSE';
    v_dir := '/xx_iface/' || v_instance || '/xtr/inbound/eclipse/';
    
  
    --  Setup parameters for running FND JOBS!
    /*    l_can_submit_request := XXCUS_misc_pkg.set_responsibility('XTRINTERFACE',
                                                                  'HSI Treasury Control');
        IF l_can_submit_request THEN
          l_globalset := 'Global Variables are set.';
    
        ELSE
    
          l_globalset := 'Global Variables are not set.';
          l_sec       := 'Global Variables are not set for the Responsibility of HSI Treasury Control and the User of XTRINTERFACE.';
          fnd_file.put_line(fnd_file.log, l_sec);
          fnd_file.put_line(fnd_file.output, l_sec);
          RAISE PROGRAM_ERROR;
        END IF;
    */
    fnd_file.put_line(fnd_file.log, l_globalset);
    fnd_file.put_line(fnd_file.output, l_globalset);
    --  END OF Setup parameters for running FND JOBS!
    --
    -- BEGIN
    FOR c_files IN (SELECT description
                      FROM apps.fnd_lookup_values
                     WHERE lookup_type = 'HDS_CE_INTERFACE_PKG'
                       AND lookup_code LIKE 'ECLIPSE_FILE_%'
                       and enabled_flag = 'Y')
    LOOP
    
      -- DEBUG NAME
      v_message := 'Submitting SQLLDR';
      l_message := v_message;
    
      --
      fnd_file.put_line(fnd_file.log, 'Input parameters are....:');
      fnd_file.put_line(fnd_file.log
                       ,'File Name...... = ' || c_files.description);
      fnd_file.put_line(fnd_file.output, 'File Name:  ' || c_files.description);
    
      -- DEBUG NAME
      v_message := 'Submitting HDS SQL*Loader-CE Load Eclipse file names: short name:  XXCUSCEECLISPEFILES';
      l_message := v_message;
    
      --
      -- Submit the SQL loader program to load the lockbox and acm files into a table
      --
      v_req_id := apps.fnd_request.submit_request('XXCUS'
                                                 ,'XXCUSCEECLISPEFILES'
                                                 ,NULL
                                                 ,SYSDATE
                                                 ,FALSE
                                                 ,v_dir ||
                                                  c_files.description);
      COMMIT;
    
      -- get the status of the child process.
      IF v_req_id != 0
      THEN
        --
        -- Wait for the previous concurrent request (XXCUSCELB) to complete
        --
        IF fnd_concurrent.wait_for_request(v_req_id
                                          ,v_interval
                                          ,v_max_time
                                          ,v_req_phase
                                          ,v_req_status
                                          ,v_dev_phase
                                          ,v_dev_status
                                          ,v_message)
        THEN
          v_error_message := chr(10) || 'ReqID=' || v_req_id || ' DPhase ' ||
                             v_dev_phase || ' DStatus ' || v_dev_status ||
                             chr(10) || ' MSG - ' || v_message;
          -- Error Returned
          IF v_dev_phase != 'COMPLETE' OR
             v_dev_status NOT IN ('NORMAL', 'WARNING')
          THEN
            l_statement := 'An error occured in the running the Eclipse file names SQL Loader' ||
                           v_error_message || '.';
            fnd_file.put_line(fnd_file.log, l_statement);
            fnd_file.put_line(fnd_file.output, l_statement);
            RAISE program_error;
          
          END IF;
        
          -- Then Success! Load the files the Eclipse delivered
          load_eclipse_files(errbuf, retcode);
        
        ELSE
          l_statement := 'An error occured running the Eclipse file names SQL Loader' ||
                         v_error_message || '.';
          fnd_file.put_line(fnd_file.log, l_statement);
          fnd_file.put_line(fnd_file.output, l_statement);
          RAISE program_error;
        END IF;
      
      ELSE
        l_statement := 'An error occured when trying to submitting the XXCUS CE INTERFACE Eclipse file names SQL Loader';
        fnd_file.put_line(fnd_file.log, l_statement);
        fnd_file.put_line(fnd_file.output, l_statement);
        RAISE program_error;
      END IF;
    
    END LOOP;
  
    v_set_req_status := fnd_concurrent.set_completion_status(v_completion_status
                                                            ,NULL);
    fnd_file.new_line(fnd_file.log, 1);
    fnd_file.put_line(fnd_file.log, 'End of File Processing...');
  
  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;
      l_err_code := 2;
      l_err_msg  := l_sec;
      l_err_msg  := l_err_msg || ' ERROR ' || substr(SQLERRM, 1, 2000);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode         := l_err_code;
      errbuf          := l_err_msg;
      g_err_callpoint := l_message;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => g_err_callfrom
                                          ,p_calling           => g_err_callpoint
                                          ,p_request_id        => v_req_id
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'Error running CE INTERFACE package with PROGRAM ERROR'
                                          ,p_distribution_list => g_distro_list
                                          ,p_module            => 'CE');
    
      fnd_file.put_line(fnd_file.output, 'Need fix the errors!');
    
    WHEN OTHERS THEN
      ROLLBACK;
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := l_sec || l_err_msg || ' ERROR ' || SQLCODE ||
                    substr(SQLERRM, 1, 2000);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode         := l_err_code;
      errbuf          := l_err_msg;
      g_err_callpoint := l_message;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => g_err_callfrom
                                          ,p_calling           => g_err_callpoint
                                          ,p_request_id        => v_req_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => 'Error running CE INTERFACE package with OTHERS Exception'
                                          ,p_distribution_list => g_distro_list
                                          ,p_module            => 'CE');
    
  END load_eclipse_file_names;

  /**************************************************************************
  File Name: XXCUS_CE_INTERFACE
  
  PROGRAM TYPE: SQL Script
  
  PURPOSE:      package Specification is used to load lockbox and Eclipse files
                delivered to the /xx_iface/prd/xtr/inbound/eclipse.  The file name
                will be inserted into a trigger table to know what is available
                to load that was FTP's by Eclipse.  These files will be used by
                Cash Management for the bank reconciliation process
  HISTORY
  =============================================================================
         Last Update Date : 09/17/2008
  =============================================================================
  =============================================================================
  VERSION DATE          AUTHOR(S)          DESCRIPTION
  ------- -----------   -----------------  ------------------------------------
  1.0     27-Jun-2011   Kathy Poling       Creation this package taken R11 and
                                           modified for R12
                                           
  =============================================================================
  *****************************************************************************/
  PROCEDURE load_eclipse_files(errbuf OUT VARCHAR2, retcode OUT NUMBER) IS
  
  BEGIN
  
    SELECT lower(NAME) INTO v_instance FROM v$database;
  
    v_file_dir := '/xx_iface/' || v_instance || '/xtr/inbound/eclipse/';
    
    --  Setup parameters for running FND JOBS!
    /*    l_can_submit_request := XXCUS_misc_pkg.set_responsibility('XTRINTERFACE',
                                                              'HSI Treasury Control');
    IF l_can_submit_request THEN
      l_globalset := 'Global Variables are set.';
    
    ELSE
    
      l_globalset := 'Global Variables are not set.';
      l_sec       := 'Global Variables are not set for the Responsibility of HSI Treasury Control and the User of XTRINTERFACE.';
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      RAISE PROGRAM_ERROR;
    END IF;
    
    fnd_file.put_line(fnd_file.log, l_globalset);
    fnd_file.put_line(fnd_file.output, l_globalset);    */
    --  END OF Setup parameters for running FND JOBS!
    --
    --BEGIN
    FOR c_files IN (SELECT file_name
                      FROM xxcus.xxcusce_eclipse_file_trg_tbl
                     WHERE status = 'PENDING')
    LOOP
    
      fnd_file.put_line(fnd_file.log, 'Input parameters are....:');
      fnd_file.put_line(fnd_file.log
                       ,'File Name...... = ' || c_files.file_name);
      fnd_file.put_line(fnd_file.output, 'File Name:  ' || c_files.file_name);
    
      -- DEBUG NAME
      v_message := 'Submitting SQLLDR';
      l_message := v_message;
      --
    
      IF c_files.file_name LIKE 'MiscCashReceipts%'
      THEN
        v_shortname := 'XXCUSCEMCR';
      ELSE
        v_shortname := 'XXCUSCECBJ';
      END IF;
      --
      -- Submit the SQL loader program to load the lockbox and acm files into a table
      --
      fnd_file.put_line(fnd_file.log, v_message);
      v_req_id := apps.fnd_request.submit_request('XXCUS'
                                                 ,v_shortname
                                                 ,NULL
                                                 ,SYSDATE
                                                 ,FALSE
                                                 ,v_file_dir ||
                                                  c_files.file_name);
      COMMIT;
    
      UPDATE xxcus.xxcusce_eclipse_file_trg_tbl
         SET status        = 'PROCESSING'
            ,creation_date = SYSDATE
            ,request_id    = v_req_id
       WHERE file_name = c_files.file_name;
      COMMIT;
    
      -- get the status of the child process.
      IF v_req_id != 0
      THEN
        --
        -- Wait for the previous concurrent request to complete
        --
        IF fnd_concurrent.wait_for_request(v_req_id
                                          ,v_interval
                                          ,v_max_time
                                          ,v_req_phase
                                          ,v_req_status
                                          ,v_dev_phase
                                          ,v_dev_status
                                          ,v_message)
        THEN
          v_error_message := chr(10) || 'ReqID=' || v_req_id || ' DPhase ' ||
                             v_dev_phase || ' DStatus ' || v_dev_status ||
                             chr(10) || ' MSG - ' || v_message;
          -- Error Returned
          IF v_dev_phase != 'COMPLETE' OR
             v_dev_status NOT IN ('NORMAL', 'WARNING')
          THEN
            l_statement := 'An error occured in the running the SQL Loader' ||
                           v_error_message || '.';
            fnd_file.put_line(fnd_file.log, l_statement);
            fnd_file.put_line(fnd_file.output, l_statement);
            RAISE program_error;
          
          END IF;
        
          -- Then Success!
          UPDATE xxcus.xxcusce_eclipse_file_trg_tbl
             SET status           = 'COMPLETED'
                ,request_id       = v_req_id
                ,last_update_date = SYSDATE
           WHERE file_name = c_files.file_name;
          COMMIT;
        
          IF c_files.file_name LIKE 'CashBoxJournal%'
          THEN
            UPDATE xxcus.xxcusce_eclipse_cash_box_tbl
               SET file_name = c_files.file_name, request_id = v_req_id
             WHERE file_name IS NULL;
            COMMIT;
          
          END IF;
        
        ELSE
          l_statement := 'An error occured running the Lockbox SQL Loader' ||
                         v_error_message || '.';
          fnd_file.put_line(fnd_file.log, l_statement);
          fnd_file.put_line(fnd_file.output, l_statement);
          RAISE program_error;
        END IF;
      
      ELSE
        l_statement := 'An error occured when trying to submitting the XXCUS CE INTERFACE Lockbox SQL Loader';
        fnd_file.put_line(fnd_file.log, l_statement);
        fnd_file.put_line(fnd_file.output, l_statement);
        RAISE program_error;
      END IF;
      --No email alerts sent when updates can't be done
      IF c_files.file_name LIKE 'MiscCashReceipts%'
      THEN
        UPDATE xxcus.xxcusce_eclipse_deposit_tbl
           SET file_name = c_files.file_name, request_id = v_req_id
         WHERE file_name IS NULL;
        COMMIT;
      
        UPDATE xxcus.xxcusce_eclipse_deposit_tbl
           SET comments = REPLACE(comments, '/')
         WHERE regexp_like(substr(comments, 1, 8), '^[[:digit:]]')
           AND bank_acct NOT IN ('CASH - MC/VISA', 'CASH - AMEX')
           AND instr(comments, '-') = 9
           AND trunc(creation_date) = trunc(SYSDATE);
        COMMIT;
      
        UPDATE xxcus.xxcusce_eclipse_deposit_tbl
           SET deposit     = to_date(substr(comments, 1, 8), 'MMDDYYYY')
              ,deposit_num = substr(comments, 10, 20)
         WHERE regexp_like(substr(comments, 1, 8), '^[[:digit:]]')
           AND bank_acct NOT IN ('CASH - MC/VISA', 'CASH - AMEX')
           AND instr(comments, '-') = 9
           AND trunc(creation_date) = trunc(SYSDATE);
        COMMIT;
      
        UPDATE xxcus.xxcusce_eclipse_deposit_tbl
           SET deposit     = to_date(substr(comments, 1, 6), 'MMDDRR')
              ,deposit_num = substr(comments, 8, 22)
         WHERE regexp_like(substr(comments, 1, 6), '^[[:digit:]]')
           AND bank_acct NOT IN ('CASH - MC/VISA', 'CASH - AMEX')
           AND instr(comments, '-') = 7
           AND trunc(creation_date) = trunc(SYSDATE);
        COMMIT;
      
        UPDATE xxcus.xxcusce_eclipse_deposit_tbl
           SET deposit_num = ltrim(deposit_num, '0')
         WHERE deposit_num IS NOT NULL
           AND trunc(creation_date) = trunc(SYSDATE);
        COMMIT;
      
      END IF;
    
    END LOOP;
  
    v_set_req_status := fnd_concurrent.set_completion_status(v_completion_status
                                                            ,NULL);
    fnd_file.new_line(fnd_file.log, 1);
    fnd_file.put_line(fnd_file.log, 'End of File Processing...');
  
  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;
      l_err_code := 2;
      l_err_msg  := l_sec;
      l_err_msg  := l_err_msg || ' ERROR ' || substr(SQLERRM, 1, 2000);
      UPDATE xxcus.xxcusce_eclipse_file_trg_tbl
         SET status           = 'ERROR'
            ,request_id       = v_req_id
            ,last_update_date = SYSDATE
       WHERE request_id = v_req_id;
    
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode         := l_err_code;
      errbuf          := l_err_msg;
      g_err_callpoint := l_message;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => g_err_callfrom
                                          ,p_calling           => g_err_callpoint
                                          ,p_request_id        => v_req_id
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'Error running XXCUSCE INTERFACE package with PROGRAM ERROR'
                                          ,p_distribution_list => g_distro_list
                                          ,p_module            => 'CE');
    
      fnd_file.put_line(fnd_file.output, 'Need fix the errors!');
    
    WHEN OTHERS THEN
      ROLLBACK;
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := l_sec || l_err_msg || ' ERROR ' || SQLCODE ||
                    substr(SQLERRM, 1, 2000);
      UPDATE xxcus.xxcusce_eclipse_file_trg_tbl
         SET status           = 'ERROR'
            ,request_id       = v_req_id
            ,last_update_date = SYSDATE
       WHERE request_id = v_req_id;
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode         := l_err_code;
      errbuf          := l_err_msg;
      g_err_callpoint := l_message;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => g_err_callfrom
                                          ,p_calling           => g_err_callpoint
                                          ,p_request_id        => v_req_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => 'Error running XXCUSCE INTERFACE package with OTHERS Exception'
                                          ,p_distribution_list => g_distro_list
                                          ,p_module            => 'CE');
  END load_eclipse_files;

  /**************************************************************************
  File Name: XXCUS_CE_INTERFACE
  
  PROGRAM TYPE: SQL Script
  
  PURPOSE:      package Specification is used to load Eclipse file names
                delivered to the /xx_iface/ebizprd/xtr/inbound/eclipse.  The file name
                will be inserted into a trigger table to know what is available
                to load that was FTP's by Eclipse.  These files will be used by
                Cash Management for the bank reconciliation process
                Concurrent request created "HDS CE Load Files From Eclipse"
                to load the file names into a staging table
  
                This allows user to load one file at a time.
  HISTORY
  =============================================================================
         Last Update Date : 09/17/2008
  =============================================================================
  =============================================================================
  VERSION DATE          AUTHOR(S)          DESCRIPTION
  ------- -----------   -----------------  ------------------------------------
  1.0     27-Jun-2011   Kathy Poling       Creation this package taken R11 and
                                           modified for R12
  
  =============================================================================
  *****************************************************************************/
  PROCEDURE load_eclipse_one_file(errbuf  OUT VARCHAR2
                                 ,retcode OUT NUMBER
                                 ,p_file  IN VARCHAR2) IS
  
  BEGIN
  
    SELECT lower(NAME) INTO v_instance FROM v$database;
  
    v_file_dir := '/xx_iface/' || v_instance || '/xtr/inbound/eclipse/';
  
    --  Setup parameters for running FND JOBS!
    /*   l_can_submit_request := XXCUS_misc_pkg.set_responsibility('XTRINTERFACE',
                                                                  'HSI Treasury Control');
        IF l_can_submit_request THEN
          l_globalset := 'Global Variables are set.';
    
        ELSE
    
          l_globalset := 'Global Variables are not set.';
          l_sec       := 'Global Variables are not set for the Responsibility of HSI Treasury Control and the User of XTRINTERFACE.';
          fnd_file.put_line(fnd_file.log, l_sec);
          fnd_file.put_line(fnd_file.output, l_sec);
          RAISE PROGRAM_ERROR;
        END IF;
    */
    fnd_file.put_line(fnd_file.log, l_globalset);
    fnd_file.put_line(fnd_file.output, l_globalset);
    --  END OF Setup parameters for running FND JOBS!
    --
    -- BEGIN
  
    -- DEBUG NAME
    v_message := 'Submitting SQLLDR';
    l_message := v_message;
  
    --
    fnd_file.put_line(fnd_file.log, 'Input parameters are....:');
    fnd_file.put_line(fnd_file.log, 'File Name...... = ' || p_file);
    fnd_file.put_line(fnd_file.output, 'File Name:  ' || p_file);
  
    -- DEBUG NAME
    v_message := 'Submitting HDS SQL*Loader-CE Load Eclipse file names: short name:  XXCUSCEECLISPEFILES';
    l_message := v_message;
  
    --
    -- Submit the SQL loader program to load the lockbox and acm files into a table
    --
    v_req_id := apps.fnd_request.submit_request('XXCUS'
                                               ,'XXCUSCEECLISPEFILES'
                                               ,NULL
                                               ,SYSDATE
                                               ,FALSE
                                               ,v_file_dir || p_file);
    COMMIT;
  
    -- get the status of the child process.
    IF v_req_id != 0
    THEN
      --
      -- Wait for the previous concurrent request (XXCUSCELB) to complete
      --
      IF fnd_concurrent.wait_for_request(v_req_id
                                        ,v_interval
                                        ,v_max_time
                                        ,v_req_phase
                                        ,v_req_status
                                        ,v_dev_phase
                                        ,v_dev_status
                                        ,v_message)
      THEN
        v_error_message := chr(10) || 'ReqID=' || v_req_id || ' DPhase ' ||
                           v_dev_phase || ' DStatus ' || v_dev_status ||
                           chr(10) || ' MSG - ' || v_message;
        -- Error Returned
        IF v_dev_phase != 'COMPLETE' OR
           v_dev_status NOT IN ('NORMAL', 'WARNING')
        THEN
          l_statement := 'An error occured in the running the Eclipse file names SQL Loader' ||
                         v_error_message || '.';
          fnd_file.put_line(fnd_file.log, l_statement);
          fnd_file.put_line(fnd_file.output, l_statement);
          RAISE program_error;
        
        END IF;
      
        -- Then Success! Load the files the Eclipse delivered
        load_eclipse_files(errbuf, retcode);
      
      ELSE
        l_statement := 'An error occured running the Eclipse file names SQL Loader' ||
                       v_error_message || '.';
        fnd_file.put_line(fnd_file.log, l_statement);
        fnd_file.put_line(fnd_file.output, l_statement);
        RAISE program_error;
      END IF;
    
    ELSE
      l_statement := 'An error occured when trying to submitting the XXCUSCE INTERFACE Eclipse file SQL Loader';
      fnd_file.put_line(fnd_file.log, l_statement);
      fnd_file.put_line(fnd_file.output, l_statement);
      RAISE program_error;
    END IF;
  
    v_set_req_status := fnd_concurrent.set_completion_status(v_completion_status
                                                            ,NULL);
    fnd_file.new_line(fnd_file.log, 1);
    fnd_file.put_line(fnd_file.log, 'End of File Processing...');
  
  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;
      l_err_code := 2;
      l_err_msg  := l_sec;
      l_err_msg  := l_err_msg || ' ERROR ' || substr(SQLERRM, 1, 2000);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode         := l_err_code;
      errbuf          := l_err_msg;
      g_err_callpoint := l_message;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => g_err_callfrom
                                          ,p_calling           => g_err_callpoint
                                          ,p_request_id        => v_req_id
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'Error running XXCUSCE INTERFACE package with PROGRAM ERROR'
                                          ,p_distribution_list => g_distro_list
                                          ,p_module            => 'CE');
    
      fnd_file.put_line(fnd_file.output, 'Need to fix the errors!');
    
    WHEN OTHERS THEN
      ROLLBACK;
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := l_sec || l_err_msg || ' ERROR ' || SQLCODE ||
                    substr(SQLERRM, 1, 2000);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode         := l_err_code;
      errbuf          := l_err_msg;
      g_err_callpoint := l_message;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => g_err_callfrom
                                          ,p_calling           => g_err_callpoint
                                          ,p_request_id        => v_req_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => 'Error running XXCUSCE INTERFACE package with OTHERS Exception'
                                          ,p_distribution_list => g_distro_list
                                          ,p_module            => 'CE');
    
  END load_eclipse_one_file;

END xxcusce_interface_pkg;
/
