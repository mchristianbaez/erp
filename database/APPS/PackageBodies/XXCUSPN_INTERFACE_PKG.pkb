CREATE OR REPLACE PACKAGE BODY APPS."XXCUSPN_INTERFACE_PKG" AS
  --
  -- DEBUG Variables
  --
  l_dflt_email  VARCHAR2(200); --:=  --'Manny.Rodriguez@hdsupply.com'; --'HDSOracleDevelopers@hdsupply.com' ; -- email used for errors within
  l_dflt_email2 VARCHAR2(200); -- := --'Manny.Rodriguez@hdsupply.com'; --'HDSOracleDevelopers@hdsupply.com' ; -- email used for errors within
  l_dflt_email3 VARCHAR2(200);
  l_gl_support  VARCHAR2(200) DEFAULT 'hsi.oracleglsupport@hdsupply.com';

  -- Error DEBUG
  l_err_callfrom  VARCHAR2(175) DEFAULT 'XXCUSPN_INTERFACE_PKG';
  l_err_callpoint VARCHAR2(175) DEFAULT 'START';
  l_distro_list   VARCHAR2(175) DEFAULT 'HDSOracleDevelopers@hdsupply.com';

  program_error EXCEPTION;

  l_rowid  VARCHAR2(100); --used for out from insert
  l_propid NUMBER; --used for out from insert

  l_errorstatus        NUMBER;
  l_req_id             NUMBER NULL;
  v_phase              VARCHAR2(50);
  v_status             VARCHAR2(50);
  v_dev_status         VARCHAR2(50);
  v_dev_phase          VARCHAR2(50);
  v_message            VARCHAR2(250);
  v_error_message      VARCHAR2(3000);
  l_statement          VARCHAR2(9000);
  l_host               VARCHAR2(256) := 'mailoutrelay.hdsupply.net';
  l_errormessage       VARCHAR2(3000);
  l_sender             VARCHAR2(100);
  l_sid                VARCHAR2(8);
  l_can_submit_request BOOLEAN := TRUE;
  l_globalset          VARCHAR2(100);
  l_err_msg            VARCHAR2(3000);
  l_err_code           NUMBER;
  l_sec                VARCHAR2(255);
  g_org_id             NUMBER DEFAULT 163; --HD Supply Corp USD - Org
  g_sysadmin_user      NUMBER DEFAULT 0;

  -- -----------------------------------------------------------------------------
  -- |----------------------------< pn_admin_process >----------------------------|
  -- -----------------------------------------------------------------------------
  --
  -- {Start Of Comments}
  --
  -- Description:
  --   This procedure creates locations for admin areas coming from PeopleSoft.
  --
  --   VERSION DATE          AUTHOR(S)       DESCRIPTION
  -- ------- -----------   --------------- ---------------------------------
  -- 1.0     01-OCT-2008   Manny         Created this procedure.
  -- Need to receive the data from dblink.  2 fields (locat_code, department)
  -- Some data in the feed needs to be created based on this data.
  -- Known fields :
  -- Batch_name : 'ADMINXX'
  -- entry_type : 'A'
  -- Office : 'Admin'
  -- UOM_CODE : 'SFT'
  -- last_update_date : sysdate
  -- creation_date : sysdate
  -- attribute_category : OFFICE
  -- Source : 'SHEET'
  -- Space_type_lookup_code : 'ADM'
  -- Assignable_area : 0
  -- Location_alias : 'A'
  --
  -- unknown fields :
  -- location_id: sequence
  -- location_type_lookup_code : derived from a floor in the building.
  -- Parent_location_id : Floor_id making up the location_type_lookup_code
  -- Active_start_date : same date as active start_date for Floor
  --
  -- LOGIC : skip location creation when fru-loc already exists.
  --       : Assume that all operation locactions exist in OPN so new PeopleSoft FRULOCS will be admin
  --       : Check if FRU-LOC combo exists in emp_space table, if so, check if it's still active by looking
  --         at the end_date field.
  --       : If the end_date field is populated, then UPDATE assignment with new start date as sysdate
  --       : If the FRULOC doesn't exists, check to see if the section .PEOPLE exists.  If it doesn't,
  --         then we need to create an INSERT into the Location_ITF table to for this section.
  --         After the creation of the section .PEOPLE, we create FRULOC as an INSERT into the assignment ITF table
  --       : If the FRULOC does exists, then do nothing.
  --       : TRIGGER like.   Check to see if a similar FRULOC with spacetype "OFFICE" exists.  This is a manaually
  --         created FRULOC and if we see this, we should end_date the .PEOPLE section based FRU's.
  --
  --1.1     10-DEC-2008  Kathy Poling
  --1.2     05-MAR-2009  Manny        Change made to allow automated script to create new PEOPLE FRU-LOC's when
  --                                  a People-based FRU-lOC was previously created but closed.   This was done
  --                                  by modifying the lawnotopn2 cursor to look for FRU-LOCS in PeopleSoft but not
  --                                  in OPN (Modded to not include previously closed FRU-lOCS)
  --1.3     06-Mar-2009  Manny        Added HR team to both emails sent out.
  --1.4     09-Mar-2009  Manny        Added comments column to cursor and body of email.
  --1.5     19-MAR-2009  Manny        Restrict OPNNOTLAW2 from ending anything but PPL
  --1.6     31-MAR-2009  Manny        Added a check to make sure PeopleSoft has data.
  --1.7     09-JUN-2009  Manny        Changed a where condition from looking at the standard_lookup_type for PPL
  --                                  to looking for the location_code like '%.PEOPLE'
  --1.8     01-MAR-2010  LUONG VU     SR 33410 - removed HRIS from alert.
  --1.9     12-MAY-2010  Murali       RFC: 16890 -- active start date with timestamp is locking the ADMIN SECTION
  --                                  Now made trunc of sysdate
  --                                  And ADMIN Fru PeopleSoft feed making section to BOTH is fixed
  --                                  So, RFC:16890 Admin SECTION must remain PPL always
  --1.10    07-JUN-2010  Murali       RFC for issue in PeopleSoft Feed BOTH to REAL flip is fixed
  --                                  curson for c_opnnotlaw is changed to process BOTH sections to REAL
  --1.12    10-OCT-2010  Manny        Removed email for developers when completed
  --1.13    27-DEC-2010  Kathy P      Changed employee view being used in apxcmm to use ps changes
  --1.14    17-MAY-2011  Luong Vu     SR-80387: ***FRULOC CHANGE ALERT*** rename to ***INACTIVE FRU-LOC ALERT***
  --                                  Add "hristech@hdsupply.com" back into he distribution list
  --                                  "PeopleSoft" with "PeopleSoft"
  --                                  ***INTERFACE ERRORS ALERT*** rename to ***INVALID FRU-LOC ALERT***
  --                                  Add "hristech@hdsupply.com" back into he distribution list
  --                                  Modify message text at bottom to read:
  --                                  "The LOCs and/or FRUs assigned above, are not a valid IDs in Oracle.
  --                                  Please correct your assignment in PeopleSoft."
  -- 2.0    19-MAY-2011 Kathy Poling  Copied from R11 and made some changes for R12 standards
  -- 2.1    19-OCT-2012  Luong Vu     SR 142865 - missing email notification for invalid FRULOC assignment
  --                                  when location does not exist.
  -- 3.0    31-Dec-2012  Luong Vu     Add step 8 to create CCID for Cost Center value not exist in 
  --                                  GL_CODE_COMBINATION table
  -------------------------------------------------------------------------------------------------------------------

  PROCEDURE pn_admin_process(errbuf  OUT VARCHAR2
                            ,retcode OUT NUMBER) IS
  
    --Initialize Local Variables
    l_prop_validate_flag        VARCHAR2(1) DEFAULT 'Y';
    l_process_action            BOOLEAN DEFAULT TRUE;
    l_process_action_no_check   VARCHAR2(200);
    l_sequence                  NUMBER;
    l_batch                     VARCHAR2(100);
    l_location_id               NUMBER;
    l_location_code             VARCHAR2(100);
    l_office                    VARCHAR2(100);
    l_uom_code                  VARCHAR2(100);
    l_parent_location_id        NUMBER;
    l_attribute_category        VARCHAR2(100);
    l_space_type_lookup_code    VARCHAR2(100);
    l_assignable_area           NUMBER;
    l_location_alias            VARCHAR2(100);
    l_active_start_date         DATE;
    l_floor                     NUMBER;
    l_flr_act_st_dt             DATE;
    l_flr_loc_code              VARCHAR2(175);
    l_standard_type_lookup_code VARCHAR2(50);
    l_people_loc                VARCHAR2(50);
    l_update_process            BOOLEAN DEFAULT TRUE;
    l_error_flag                BOOLEAN DEFAULT FALSE;
    l_cost_center_code          VARCHAR2(50);
    l_flag_nolink               NUMBER DEFAULT NULL;
    l_product                   VARCHAR2(60) := 'XXCUS_GL_PRODUCT';
    l_location                  VARCHAR2(60) := 'XXCUS_GL_LOCATION';
    l_lob                       VARCHAR2(10);
  
    -- Code Combination
    l_entrp_entity     VARCHAR2(10);
    l_code_combination VARCHAR2(50);
    l_segment5         VARCHAR(10) DEFAULT '00000';
    l_segment6         VARCHAR(10) DEFAULT '00000';
    l_segment7         VARCHAR(10) DEFAULT '00000';
    l_ccid             NUMBER;
    l_entrp_cc_fm      VARCHAR2(10) DEFAULT 'F099'; -- FM
    l_entrp_cc_cti     VARCHAR2(10) DEFAULT 'C950'; -- CTI
    l_entrp_cc_ww      VARCHAR2(10) DEFAULT 'W010'; -- WW
    l_entrp_cc         VARCHAR2(10) DEFAULT '0000'; -- Other LOB
    l_account          VARCHAR(10) DEFAULT '251302';
  
  
    -- Java Mailer
    l_sender      VARCHAR2(100);
    l_sid         VARCHAR2(8);
    l_body        VARCHAR2(32767);
    l_body_header VARCHAR2(32767);
    l_body_detail VARCHAR2(32767);
    l_body_footer VARCHAR2(32767);
    l_error_hint  VARCHAR2(1000);
    l_errorstatus NUMBER;
    l_host        VARCHAR2(256) := 'mailoutrelay.hdsupply.net';
    l_hostport    VARCHAR2(20) := '25';
  
    -- Java Mailer 2
    l_body2        VARCHAR2(32767);
    l_body_header2 VARCHAR2(32767);
    l_body_detail2 VARCHAR2(32767);
    l_body_footer2 VARCHAR2(32767);
  
    l_body3        VARCHAR2(32767);
    l_body_header3 VARCHAR2(32767);
    l_body_detail3 VARCHAR2(32767);
    l_body_footer3 VARCHAR2(32767);
  
    l_fulltext       VARCHAR2(7500);
    l_zone_type      VARCHAR2(200);
    l_portfolio_type VARCHAR2(200);
    l_status_type    VARCHAR2(200);
    l_count_process  NUMBER DEFAULT 0;
    l_cc_count       NUMBER DEFAULT 0;
  
    l_value VARCHAR2(200);
  
    --  Location Interface table CURSORS
    -- Cursor for Locations in PeopleSoft and in OPN
    CURSOR c_lawinopn IS
      SELECT DISTINCT d.*
        FROM pn.pn_locations_all        d
            ,pn.pn_space_assign_emp_all a
             
            ,(SELECT combo2.*
                FROM (SELECT DISTINCT fru
                                     ,location_code
                        FROM xxcus.xxcuspn_fruloc_tbl
                      INTERSECT
                      SELECT DISTINCT spc.attribute1 fru
                                     ,substr(location_code, 1,
                                             (instr(location_code, '.', 1) - 1)) location_code
                        FROM pn.pn_space_assign_emp_all spc
                            ,pn.pn_locations_all        loc
                       WHERE spc.location_id = loc.location_id
                         AND spc.attribute1 IS NOT NULL
                         AND spc.emp_assign_end_date IS NULL
                         AND loc.location_type_lookup_code = 'OFFICE'
                         AND loc.standard_type_lookup_code = 'REAL'
                         AND nvl(spc.emp_assign_end_date, SYSDATE) =
                             (SELECT MAX(nvl(spac.emp_assign_end_date, SYSDATE))
                                FROM pn.pn_space_assign_emp_all spac
                               WHERE spac.location_id = loc.location_id
                                 AND spac.attribute1 = spc.attribute1)) combo2
               WHERE combo2.location_code NOT IN
                     (SELECT substr(bbc.location_code, 1,
                                    (instr(bbc.location_code, '.', 1) - 1)) loc_code
                        FROM pn.pn_locations_all bbc
                            ,(SELECT DISTINCT fru
                                             ,location_code
                                FROM xxcus.xxcuspn_fruloc_tbl
                              INTERSECT
                              SELECT DISTINCT spc.attribute1 fru
                                             ,substr(location_code, 1,
                                                     (instr(location_code,
                                                             '.', 1) - 1)) location_code
                                FROM pn.pn_space_assign_emp_all spc
                                    ,pn.pn_locations_all        loc
                               WHERE spc.location_id = loc.location_id
                                 AND spc.attribute1 IS NOT NULL
                                 AND spc.emp_assign_end_date IS NULL
                                 AND loc.location_type_lookup_code =
                                     'OFFICE'
                                 AND loc.standard_type_lookup_code = 'REAL'
                                 AND nvl(spc.emp_assign_end_date, SYSDATE) =
                                     (SELECT MAX(nvl(spac.emp_assign_end_date,
                                                     SYSDATE))
                                        FROM pn.pn_space_assign_emp_all spac
                                       WHERE spac.location_id =
                                             loc.location_id
                                         AND spac.attribute1 = spc.attribute1)) combo
                       WHERE substr(bbc.location_code, 1,
                                    (instr(bbc.location_code, '.', 1) - 1)) =
                             combo.location_code
                         AND bbc.location_type_lookup_code = 'OFFICE'
                            --AND bbc.standard_type_lookup_code IN ('BOTH', 'PPL'))) combo3   --Version 1.1
                         AND bbc.standard_type_lookup_code = 'BOTH')) combo3 --Version 1.1
       WHERE d.location_id = a.location_id
         AND substr(d.location_code, 1, (instr(d.location_code, '.', 1) - 1)) =
             combo3.location_code
         AND a.attribute1 = combo3.fru
         AND d.standard_type_lookup_code != 'PPL'; -- Admin SECTION must remain PPL always
  
    -- Cursor for Locations in PeopleSoft but not in OPN
    CURSOR c_lawnotopn IS
    -- Cursor for Locations in PeopleSoft but not in OPN
    
      SELECT DISTINCT opnlaw.location_code
        FROM (SELECT DISTINCT substr(location_code, 1,
                                     (instr(location_code, '.', 1) - 1)) location_code
                FROM pn.pn_locations_all
               WHERE substr(location_code, 1,
                            (instr(location_code, '.', 1) - 1)) IS NOT NULL) locb
            ,(SELECT DISTINCT fru
                             ,location_code
                FROM xxcus.xxcuspn_fruloc_tbl
              MINUS
              -- list of all FRULOCS in OPN, regardless of end-date
              SELECT DISTINCT spc.attribute1 fru
                             ,substr(location_code, 1,
                                     (instr(location_code, '.', 1) - 1)) location_code
                FROM pn.pn_space_assign_emp_all spc
                    ,pn.pn_locations_all        loc
               WHERE spc.location_id = loc.location_id
                 AND spc.attribute1 IS NOT NULL) opnlaw
       WHERE opnlaw.location_code = locb.location_code(+)
      -- version 1.1
      /*   AND NOT EXISTS
      (SELECT DISTINCT substr(locb.location_code
                                  ,1
                                  ,(instr(locb.location_code, '.', 1) - 1)) location_code
            FROM pn.pn_locations_all locb
                ,(SELECT DISTINCT fru, location_code
                  FROM xxcus.xxcus_pn_fruloc_tbl
                  INTERSECT
                  SELECT DISTINCT spc.attribute1 fru
                                 ,substr(location_code
                                        ,1
                                        ,(instr(location_code, '.', 1) - 1)) location_code
                  FROM pn.pn_space_assign_emp_all spc
                      ,pn.pn_locations_all        loc
                  WHERE spc.location_id = loc.location_id
                  AND spc.attribute1 IS NOT NULL
                  AND spc.emp_assign_end_date IS NULL) opnlaw2
            WHERE substr(locb.location_code
                        ,1
                        ,(instr(locb.location_code, '.', 1) - 1)) =
                  opnlaw2.location_code
            AND substr(locb.location_code
                     ,1
                     ,(instr(locb.location_code, '.', 1) - 1)) =
                  opnlaw.location_code)
                  */
      ;
  
    -- Cursor for Locations in OPN but not in PeopleSoft
    -- cursor will subtract fru and loc from PeopleSoft from OPN FRU LOC
    CURSOR c_opnnotlaw IS
    -- Cursor for Locations in OPN but not in PeopleSoft
    -- cursor will subtract fru and loc from PeopleSoft from OPN FRU LOC
    -- V1.10 - This rewritten to process correctly in this version.
      SELECT DISTINCT b.* -- DISTINCT needed as there are mulitple rows with 12/31/4712
        FROM pn.pn_space_assign_emp_all a
            ,pn.pn_locations_all        b
       WHERE a.location_id = b.location_id -- join
         AND b.standard_type_lookup_code = 'BOTH' -- Onlt to flip BOTH to REAL
         AND a.attribute1 IS NOT NULL -- FRU attached ones
         AND nvl(b.active_end_date, SYSDATE) = -- Filter hisrtory rows
             (SELECT MAX(nvl(bss.active_end_date, SYSDATE))
                FROM pn.pn_locations_all bss
               WHERE bss.location_id = b.location_id)
         AND nvl(a.emp_assign_end_date, SYSDATE) = -- to filter history rows
             (SELECT MAX(nvl(ass.emp_assign_end_date, SYSDATE))
                FROM pn.pn_space_assign_emp_all ass
                    ,pn.pn_locations_all        bss
               WHERE ass.location_id = bss.location_id
                 AND ass.location_id = a.location_id -- to join with main query. This is needed for AZ032 for some reason
                 AND ass.attribute1 = a.attribute1)
         AND (a.attribute1,
              substr(b.location_code, 1, (instr(location_code, '.', 1) - 1))) NOT IN
             (SELECT DISTINCT fru
                             ,location_code
                FROM xxcus.xxcuspn_fruloc_tbl) -- but not in PeopleSoft for fru-loc combination
      ;
  
    --  ASSIGNMENT SPACE Interface table CURSORS
    -- Cursor for Locations in OPN and in PeopleSoft
    -- Will only return locations that are in-active inside the space assignment table.  This is after
    -- a comparison using an INTERSECT of both sides.
    CURSOR c_opninlaw2 IS
      SELECT DISTINCT substr(locb.location_code, 1,
                             (instr(locb.location_code, '.', 1) - 1)) location_code
                     ,spaced.emp_assign_end_date
                     ,spaced.emp_assign_start_date
                     ,spaced.attribute1 fru
                     ,spaced.emp_space_comments --v1.4
        FROM pn.pn_locations_all locb
            ,pn.pn_space_assign_emp_all spaced
            ,(SELECT fru
                    ,location_code
                FROM xxcus.xxcuspn_fruloc_tbl
              INTERSECT
              SELECT DISTINCT spc.attribute1 fru
                             ,substr(location_code, 1,
                                     (instr(location_code, '.', 1) - 1)) location_code
                FROM pn.pn_space_assign_emp_all spc
                    ,pn.pn_locations_all        loc
               WHERE spc.location_id = loc.location_id
                 AND spc.attribute1 IS NOT NULL) opnlaw
       WHERE substr(locb.location_code, 1,
                    (instr(locb.location_code, '.', 1) - 1)) =
             opnlaw.location_code
         AND locb.location_id = spaced.location_id
         AND opnlaw.fru = spaced.attribute1
         AND spaced.emp_assign_end_date IS NOT NULL
         AND nvl(spaced.emp_assign_end_date, SYSDATE) =
             (SELECT MAX(nvl(spc.emp_assign_end_date, SYSDATE))
                FROM pn.pn_space_assign_emp_all spc
                    ,pn.pn_locations_all        locss
               WHERE locss.location_id = spc.location_id
                 AND spaced.attribute1 = spc.attribute1
                 AND substr(locss.location_code, 1,
                            (instr(locss.location_code, '.', 1) - 1)) =
                     substr(locb.location_code, 1,
                            (instr(locb.location_code, '.', 1) - 1)));
  
    -- In PeopleSoft but not in As active OPN
    CURSOR lawnotopn2 IS
      SELECT DISTINCT fru
                     ,location_code
        FROM xxcus.xxcuspn_fruloc_tbl
      MINUS
      SELECT DISTINCT spc.attribute1 fru
                     ,substr(location_code, 1,
                             (instr(location_code, '.', 1) - 1)) location_code
        FROM pn.pn_space_assign_emp_all spc
            ,pn.pn_locations_all        loc
       WHERE spc.location_id = loc.location_id
         AND spc.attribute1 IS NOT NULL
         AND nvl(spc.emp_assign_end_date, SYSDATE) =
             (SELECT MAX(nvl(spac.emp_assign_end_date, SYSDATE))
                FROM pn.pn_space_assign_emp_all spac
                    ,pn.pn_locations_all        locss
               WHERE locss.location_id = spac.location_id
                 AND spc.attribute1 = spac.attribute1
                 AND substr(locss.location_code, 1,
                            (instr(locss.location_code, '.', 1) - 1)) =
                     substr(loc.location_code, 1,
                            (instr(loc.location_code, '.', 1) - 1)))
         AND spc.emp_assign_end_date IS NULL; --v1.2
  
    -- Cursor for Locations in OPN but not in PeopleSoft
    -- Will only return locations that are active inside the space assignment table.
    CURSOR c_opnnotlaw2 IS
      SELECT spaced.*
        FROM pn.pn_locations_all locs
            ,pn.pn_space_assign_emp_all spaced
            ,(SELECT DISTINCT spc.attribute1 fru
                             ,substr(location_code, 1,
                                     (instr(location_code, '.', 1) - 1)) location_code
                FROM pn.pn_space_assign_emp_all spc
                    ,pn.pn_locations_all        loc
               WHERE spc.location_id = loc.location_id
                 AND loc.standard_type_lookup_code = 'PPL'
                 AND spc.attribute1 IS NOT NULL
              MINUS
              SELECT DISTINCT fru
                             ,location_code
                FROM xxcus.xxcuspn_fruloc_tbl) opnotlaw
       WHERE opnotlaw.location_code =
             substr(locs.location_code, 1,
                    (instr(locs.location_code, '.', 1) - 1))
         AND opnotlaw.fru = spaced.attribute1
         AND spaced.location_id = locs.location_id
         AND nvl(spaced.emp_assign_end_date, SYSDATE) =
             (SELECT MAX(nvl(spc.emp_assign_end_date, SYSDATE))
                FROM pn.pn_space_assign_emp_all spc
                    ,pn.pn_locations_all        locss
               WHERE locss.location_id = spc.location_id
                 AND spaced.attribute1 = spc.attribute1
                 AND substr(locss.location_code, 1,
                            (instr(locss.location_code, '.', 1) - 1)) =
                     substr(locs.location_code, 1,
                            (instr(locs.location_code, '.', 1) - 1)))
         AND locs.standard_type_lookup_code = 'PPL' --v1.5
         AND emp_assign_end_date IS NULL;
  
    -- assignments which need to be end-dated since the prop team has created an alternative.
    CURSOR c_userswitch IS
      SELECT substr(bb.location_code, 1,
                    (instr(bb.location_code, '.', 1) - 1)) location_code
            ,aa.*
        FROM pn.pn_space_assign_emp_all aa
            ,pn.pn_locations_all bb
            ,(SELECT substr(b.location_code, 1,
                            (instr(b.location_code, '.', 1) - 1)) location_code
                    ,a.attribute1 fru
                FROM pn.pn_space_assign_emp_all a
                    ,pn.pn_locations_all b
                    ,(SELECT substr(b.location_code, 1,
                                    (instr(b.location_code, '.', 1) - 1)) location_code
                            ,a.attribute1 fru
                        FROM pn.pn_space_assign_emp_all a
                            ,pn.pn_locations_all        b
                       WHERE a.location_id = b.location_id(+)
                         AND nvl(a.emp_assign_end_date, SYSDATE) =
                             (SELECT MAX(nvl(spc.emp_assign_end_date, SYSDATE))
                                FROM pn.pn_space_assign_emp_all spc
                                    ,pn.pn_locations_all        locss
                               WHERE locss.location_id = spc.location_id
                                 AND substr(locss.location_code, 1,
                                            (instr(locss.location_code, '.', 1) - 1)) =
                                     substr(b.location_code, 1,
                                            (instr(b.location_code, '.', 1) - 1)))
                       GROUP BY substr(b.location_code, 1,
                                       (instr(b.location_code, '.', 1) - 1))
                               ,a.attribute1
                      HAVING COUNT(*) > 1) club
              
               WHERE a.location_id = b.location_id(+)
                 AND substr(b.location_code, 1,
                            (instr(b.location_code, '.', 1) - 1)) =
                     club.location_code
                 AND a.attribute1 = club.fru
                 AND b.standard_type_lookup_code = 'PPL'
              INTERSECT
              SELECT substr(b.location_code, 1,
                            (instr(b.location_code, '.', 1) - 1))
                    ,a.attribute1 fru
                FROM pn.pn_space_assign_emp_all a
                    ,pn.pn_locations_all b
                    ,(SELECT substr(b.location_code, 1,
                                    (instr(b.location_code, '.', 1) - 1)) location_code
                            ,a.attribute1 fru
                        FROM pn.pn_space_assign_emp_all a
                            ,pn.pn_locations_all        b
                       WHERE a.location_id = b.location_id(+)
                         AND nvl(a.emp_assign_end_date, SYSDATE) =
                             (SELECT MAX(nvl(spc.emp_assign_end_date, SYSDATE))
                                FROM pn.pn_space_assign_emp_all spc
                                    ,pn.pn_locations_all        locss
                               WHERE locss.location_id = spc.location_id
                                 AND substr(locss.location_code, 1,
                                            (instr(locss.location_code, '.', 1) - 1)) =
                                     substr(b.location_code, 1,
                                            (instr(b.location_code, '.', 1) - 1)))
                       GROUP BY substr(b.location_code, 1,
                                       (instr(b.location_code, '.', 1) - 1))
                               ,a.attribute1
                      HAVING COUNT(*) > 1) club
               WHERE a.location_id = b.location_id(+)
                 AND substr(b.location_code, 1,
                            (instr(b.location_code, '.', 1) - 1)) =
                     club.location_code
                 AND a.attribute1 = club.fru
                 AND b.standard_type_lookup_code <> 'PPL') sected
      
       WHERE aa.location_id = bb.location_id(+)
         AND aa.attribute1 = sected.fru
         AND substr(bb.location_code, 1,
                    (instr(bb.location_code, '.', 1) - 1)) =
             sected.location_code
         AND bb.standard_type_lookup_code = 'PPL';
  
    --****************************************************************************************************--
    --  START  MAIN PROGRAM
    --****************************************************************************************************--
  
  BEGIN
    -- DEBUG NAME
    l_err_callpoint := 'Aquiring batch from existing Sequence for';
    -- DEBUG NAME
    BEGIN
      SELECT 'ADMIN_' || to_char(xxcuspn_admin_batch_s.nextval)
        INTO l_batch
        FROM dual;
    
      --dbms_output.put_line('Batch '|| l_batch);
    EXCEPTION
      WHEN OTHERS THEN
        l_sec := 'Error in ' || l_err_callpoint;
        RAISE program_error;
    END;
  
    BEGIN
      SELECT description
        INTO l_dflt_email
        FROM apps.fnd_lookup_values
       WHERE lookup_type = 'HDS_PN_INTERFACE_PKG'
         AND lookup_code = 'EMAIL_1'
         AND enabled_flag = 'Y'
         AND nvl(end_date_active, SYSDATE) >= SYSDATE;
    
      SELECT description
        INTO l_dflt_email2
        FROM apps.fnd_lookup_values
       WHERE lookup_type = 'HDS_PN_INTERFACE_PKG'
         AND lookup_code = 'EMAIL_2'
         AND enabled_flag = 'Y'
         AND nvl(end_date_active, SYSDATE) >= SYSDATE;
    
      SELECT description
        INTO l_dflt_email3
        FROM apps.fnd_lookup_values
       WHERE lookup_type = 'HDS_PN_INTERFACE_PKG'
         AND lookup_code = 'EMAIL_3'
         AND enabled_flag = 'Y'
         AND nvl(end_date_active, SYSDATE) >= SYSDATE;
    
    EXCEPTION
      WHEN no_data_found THEN
        fnd_file.put_line(fnd_file.output,
                          'Cannot find valid email in the lookup HDS_PN_INTERFACE_PKG');
        --dbms_output.put_line('Cannot find valid email in the xxcus_parmeters table');
    END;
  
    SELECT lower(NAME)
      INTO l_sid
      FROM v$database;
  
    --dbms_output.put_line('sid '|| l_sid);
    -- populate custom table with PeopleSoft table
    EXECUTE IMMEDIATE 'TRUNCATE TABLE  xxcus.xxcuspn_products_tbl';
    --dbms_output.put_line('truncate xxcuspn_products_tbl ');
  
    INSERT INTO xxcus.xxcuspn_products_tbl
      SELECT DISTINCT fvt1.description    product
                     ,locmap.entrp_entity
                     ,locmap.fru
                     ,fvt2.description    location
        FROM apps.fnd_flex_values        fv2
            ,apps.fnd_flex_values_tl     fvt2
            ,apps.fnd_flex_value_sets    fvs2
            ,apps.fnd_flex_values        fv1
            ,apps.fnd_flex_values_tl     fvt1
            ,apps.fnd_flex_value_sets    fvs1
            ,gl.gl_code_combinations     gcc
            ,apps.xxcus_location_code_vw locmap
       WHERE gcc.segment2 = fv2.flex_value
         AND fv2.flex_value_id = fvt2.flex_value_id
         AND fv2.flex_value_set_id = fvs2.flex_value_set_id
         AND gcc.segment1 = fv1.flex_value
         AND fv1.flex_value_id = fvt1.flex_value_id
         AND fv1.flex_value_set_id = fvs1.flex_value_set_id
         AND fvs1.flex_value_set_name = l_product
         AND fvs2.flex_value_set_name = l_location
         AND gcc.enabled_flag = 'Y'
         AND gcc.segment2 = locmap.entrp_loc
         AND gcc.segment1 = locmap.entrp_entity;
    COMMIT;
    --dbms_output.put_line('inserted xxcuspn_products_tbl ');
    EXECUTE IMMEDIATE 'TRUNCATE TABLE  xxcus.xxcuspn_fruloc_test_tbl';
    --dbms_output.put_line('truncate xxcuspn_fruloc_test_tbl ');
  
    INSERT INTO xxcus.xxcuspn_fruloc_test_tbl
      SELECT DISTINCT REPLACE(fru, ' ') fru
                     ,REPLACE(location, ' ') location_code
        FROM apxcmmn.hr_employee_all_vw@apxprd_lnk.hsi.hughessupply.com
       WHERE (REPLACE(location, ' ') NOT LIKE ('REMOTE%') AND
             length(TRIM(translate(substr(location, 1, 1), ' +-.0123456789',
                                    ' '))) IS NOT NULL)
         AND hr_status = 'A';
    --dbms_output.put_line('inserted xxcuspn_fruloc_test_tbl ');
    --end of change 12/23/2010   Version 1.13
  
    --  Filter the PeopleSoft data
    EXECUTE IMMEDIATE 'TRUNCATE TABLE  xxcus.xxcuspn_fruloc_tbl';
    --dbms_output.put_line('truncate xxcuspn_fruloc_tbl ');
  
    INSERT INTO xxcus.xxcuspn_fruloc_tbl
      SELECT fru
            ,location_code
            ,product
        FROM xxcus.xxcuspn_fruloc_test_tbl
            ,xxcus.xxcuspn_products_tbl
       WHERE l_corp_id(+) = fru
      --AND  product = 'ELECTRICAL'    -- cold filtered to Electrical
      ;
    --dbms_output.put_line('inserted xxcuspn_fruloc_tbl ');
    -- V1.6
    -- DEBUG NAME
    l_err_callpoint := 'Pre-step:  Retrieving data from PeopleSoft.';
    -- DEBUG NAME
    --  Check to see if we got data from PeopleSoft
    BEGIN
      SELECT COUNT(*)
        INTO l_flag_nolink
        FROM xxcus.xxcuspn_fruloc_tbl;
    
      --dbms_output.put_line('flag_nolink '||l_flag_nolink);
    EXCEPTION
      WHEN OTHERS THEN
        RAISE program_error;
    END;
  
    IF l_flag_nolink < 100 THEN
      RAISE program_error;
    END IF;
  
    -- DEBUG NAME
    l_err_callpoint := 'step 1:  Start Main Loop for LawINopn';
    -- DEBUG NAME
    -- Getting list of location sections from HR PeopleSoft that are in the OPN Location table.
    FOR c_admin IN c_lawinopn
    LOOP
    
      --process the update.
      -- DEBUG NAME
      l_err_callpoint := 'step 1: inserting update row for standard type lookup_code';
      -- DEBUG NAME
    
      -- Update the 'REAL' value to 'BOTH' as we found HR folks belonging to this LOC.
      INSERT INTO pn.pn_locations_itf
        (batch_name
        ,entry_type
        ,location_id
        ,location_type_lookup_code
        ,location_code
        ,office
        ,uom_code
        ,parent_location_id
        ,creation_date
        ,attribute_category
        ,SOURCE
        ,space_type_lookup_code
        ,assignable_area
        ,location_alias
        ,active_start_date
        ,last_update_date
        ,change_date
        ,standard_type_lookup_code)
      VALUES
        (l_batch
        ,'R'
        ,c_admin.location_id
        ,'OFFICE'
        ,c_admin.location_code
        ,c_admin.office
        ,c_admin.uom_code
        ,c_admin.parent_location_id
        ,trunc(SYSDATE)
        ,c_admin.attribute_category
        ,'PEOPLESOFT1'
        ,c_admin.space_type_lookup_code
        ,c_admin.assignable_area
        ,c_admin.location_alias
        ,c_admin.active_start_date
        ,trunc(SYSDATE)
        ,trunc(SYSDATE)
        ,'BOTH');
    
    END LOOP;
    COMMIT;
  
    --****************************************************************************************************--
    -- DEBUG NAME
    l_err_callpoint := 'step 2:  Start Main Loop for LawNOTopn';
    -- DEBUG NAME
    -- Getting list of location sections from HR PeopleSoft that are not in the OPN Location table.
    FOR c_adminlno IN c_lawnotopn
    LOOP
      --intialize
      l_update_process := TRUE;
    
      -- check to see if an OFFICE location exists.  If so, we can skip much of this processing.
      BEGIN
        SELECT lcs.location_alias
          INTO l_people_loc
          FROM pn.pn_locations_all lcs
         WHERE lcs.location_type_lookup_code = 'OFFICE'
              --        AND lcs.standard_type_lookup_code ='PPL'          --version 1.1
           AND lcs.location_code LIKE '%.PEOPLE' --    version 1.7
           AND substr(lcs.location_code, 1,
                      (instr(lcs.location_code, '.', 1) - 1)) =
               c_adminlno.location_code
           AND rownum = 1;
      
        l_update_process := FALSE; --no need to process if we found an office.
      EXCEPTION
        WHEN no_data_found THEN
          l_update_process := TRUE;
        
          -- STEP 2: Process a new location
        
          -- DEBUG NAME
          l_err_callpoint := 'step 2:  Finding the 1st floor for a location';
          -- DEBUG NAME
          -- Getting the floor info needed.  Skip if no floors found.
          BEGIN
            SELECT MIN(loc.location_id)
              INTO l_floor
              FROM pn.pn_locations_all loc
             WHERE loc.location_type_lookup_code = 'FLOOR'
               AND loc.active_end_date = '31-DEC-4712'
               AND substr(loc.location_code, 1,
                          (instr(loc.location_code, '.', 1) - 1)) =
                   c_adminlno.location_code;
            --dbms_output.put_line('Floor is ' || l_floor);
          EXCEPTION
            WHEN no_data_found THEN
              l_update_process := FALSE;
              fnd_file.put_line(fnd_file.output,
                                'Cannot setup Admin.  Location Floor not found for Location_code ' ||
                                 c_adminlno.location_code);
              /*  dbms_output.put_line('Cannot setup Admin.  Location Floor not found for Location_code ' ||
              c_adminlno.location_code);*/
          END;
        
          -- DEBUG NAME
          l_err_callpoint := 'Step 2: Finding all pertinent variable fields for the table based on 1st floor found for ' ||
                             c_adminlno.location_code || ' ';
          -- DEBUG NAME
          -- Getting the floor info needed.  Skip if no floors found.
          IF l_update_process THEN
            BEGIN
              SELECT loc1.active_start_date
                    ,loc1.location_code || '.PEOPLE'
                    ,loc1.location_id
                INTO l_flr_act_st_dt
                    ,l_flr_loc_code
                    ,l_parent_location_id
                FROM pn.pn_locations_all loc1
               WHERE loc1.location_id = l_floor
                 AND loc1.active_end_date = '31-DEC-4712';
            
              /*    dbms_output.put_line('Parent location id is ' ||
              l_parent_location_id);*/
            EXCEPTION
              WHEN no_data_found THEN
                l_update_process := FALSE;
                fnd_file.put_line(fnd_file.output,
                                  'Cannot setup Admin.  Location Floor not found for Location_code ' ||
                                   c_adminlno.location_code);
            END;
          END IF;
        
          -- DEBUG NAME
          l_err_callpoint := 'step 2: creating the PPL location';
          -- DEBUG NAME
          IF l_update_process THEN
          
            -- DEBUG NAME
            l_err_callpoint := 'step 2: Aquiring Sequence ';
            -- DEBUG NAME
            BEGIN
              SELECT pn_locations_s.nextval
                INTO l_sequence
                FROM dual;
            EXCEPTION
              WHEN OTHERS THEN
                l_sec := 'Error in ' || l_err_callpoint;
                RAISE program_error;
            END;
          
            INSERT INTO pn.pn_locations_itf
              (batch_name
              ,entry_type
              ,location_id
              ,location_type_lookup_code
              ,location_code
              ,office
              ,uom_code
              ,parent_location_id
              ,creation_date
              ,SOURCE
              ,space_type_lookup_code
              ,assignable_area
              ,location_alias
              ,active_start_date
              ,last_update_date
              ,standard_type_lookup_code)
            VALUES
              (l_batch
              ,'A'
              ,l_sequence
              ,'OFFICE'
              ,l_flr_loc_code
              ,'PEOPLE'
              ,'SFT'
              ,l_parent_location_id
              ,SYSDATE
              ,'PEOPLESOFT2'
              ,'LAWSONADMIN'
              ,0
              ,'PEOPLE'
              ,trunc(SYSDATE) -- RFC 16890
              ,SYSDATE
              ,'PPL');
          
          END IF;
        
          l_update_process := TRUE; --reset process action.  default to true.
      
      END;
    END LOOP;
    COMMIT;
  
    --****************************************************************************************************--
    -- DEBUG NAME
    l_err_callpoint := 'step 3:  Start Main Loop for c_OpnNotLaw';
    -- DEBUG NAME
    -- Getting list of location sections from OPN that are not in PeopleSoft.
    -- The cursor for this evaluates assignment records in OPN and then compares it to records in PeopleSoft.
    -- It then gets a count of the Location_codes and determines if there are still valid location_codes in OPN
    -- for that location.  If so, it will leave the location_code alone, otherwise, update it to REAL.
  
    FOR c_adminonl IN c_opnnotlaw
    LOOP
    
      INSERT INTO pn.pn_locations_itf
        (batch_name
        ,entry_type
        ,location_id
        ,location_type_lookup_code
        ,location_code
        ,office
        ,uom_code
        ,parent_location_id
        ,creation_date
        ,SOURCE
        ,space_type_lookup_code
        ,assignable_area
        ,location_alias
        ,active_start_date
        ,last_update_date
        ,standard_type_lookup_code)
      VALUES
        (l_batch
        ,'R'
        ,c_adminonl.location_id
        ,'OFFICE'
        ,c_adminonl.location_code
        ,c_adminonl.office
        ,c_adminonl.uom_code
        ,c_adminonl.parent_location_id
        ,SYSDATE
        ,'PEOPLESOFT3'
        ,c_adminonl.space_type_lookup_code
        ,c_adminonl.assignable_area
        ,c_adminonl.location_alias
        ,c_adminonl.active_start_date
        ,SYSDATE
        ,'REAL');
    
    END LOOP;
  
    COMMIT;
  
    --START  LOCATION IMPORT--
  
    -- get count of processes
    SELECT COUNT(*)
      INTO l_count_process
      FROM pn.pn_locations_itf
     WHERE batch_name = l_batch;
  
    IF l_count_process > 0 THEN
      --Run the concurrent job for imports for locations
      BEGIN
        -- DEBUG NAME
        l_err_callpoint := 'step 3: Running concurrent job for locations import';
        -- DEBUG NAME
      
        l_can_submit_request := xxcus_misc_pkg.set_responsibility('OPNINTERFACE',
                                                                  'Property Manager');
        IF l_can_submit_request THEN
          l_globalset := 'Global Variables are set.';
        
        ELSE
        
          l_globalset := 'Global Variables are not set.';
          l_sec       := 'Global Variables are not set for the Responsibility of xxcus_CON and the User of Conversion.';
          fnd_file.put_line(fnd_file.log, l_sec);
          fnd_file.put_line(fnd_file.output, l_sec);
          RAISE program_error;
        END IF;
      
        fnd_file.put_line(fnd_file.log, l_globalset);
        fnd_file.put_line(fnd_file.output, l_globalset);
        --  END OF Setup parameters for running FND JOBS!
      
        BEGIN
        
          l_req_id := fnd_request.submit_request('PN', 'PNIMPCAD',
                                                 'PNIMPCAD - Import From CAD',
                                                 NULL, FALSE, l_batch, 'L',
                                                 g_org_id);
        
          COMMIT;
        
          IF (l_req_id != 0) THEN
            IF fnd_concurrent.wait_for_request(l_req_id, 6, 15000, v_phase,
                                               v_status, v_dev_phase,
                                               v_dev_status, v_message) THEN
              v_error_message := chr(10) || 'ReqID=' || l_req_id ||
                                 ' DPhase ' || v_dev_phase || ' DStatus ' ||
                                 v_dev_status || chr(10) || ' MSG - ' ||
                                 v_message;
              -- Error Returned
              IF v_dev_phase != 'COMPLETE' THEN
                --v1.12
                -- OR v_dev_status != 'NORMAL' THEN
              
                l_statement := 'An error occured in the running of the Location import' ||
                               v_error_message || '.';
                fnd_file.put_line(fnd_file.log, l_statement);
                fnd_file.put_line(fnd_file.output, l_statement);
                --
                xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_err_callfrom,
                                                     p_calling => l_err_callpoint,
                                                     p_ora_error_msg => SQLERRM,
                                                     p_error_desc => 'Location Interface completed with Error.  Check records for batch ' ||
                                                                      l_batch,
                                                     p_distribution_list => l_distro_list,
                                                     p_module => 'PN',
                                                     p_request_id => l_req_id);
              END IF;
              -- Then Success!
            ELSE
              l_statement := 'An error occured running the Location import' ||
                             v_error_message || '.';
              fnd_file.put_line(fnd_file.log, l_statement);
              fnd_file.put_line(fnd_file.output, l_statement);
              RAISE program_error;
            END IF;
          
          ELSE
            l_statement := 'An error occured when trying to submitting the Location import';
            fnd_file.put_line(fnd_file.log, l_statement);
            fnd_file.put_line(fnd_file.output, l_statement);
            RAISE program_error;
          END IF;
          fnd_file.put_line(fnd_file.output,
                            'Processed ' || to_char(l_count_process) ||
                             ' records.');
        
        END;
      END;
    END IF;
  
    --****************************************************************************************************--
    --'Start FRU-LOC processing'
  
    l_update_process := FALSE;
  
    -- DEBUG NAME
    l_err_callpoint := 'Aquiring new batch from existing Sequence for';
    -- DEBUG NAME
    BEGIN
      SELECT 'ADMIN_' || to_char(xxcuspn_admin_batch_s.nextval)
        INTO l_batch
        FROM dual;
    EXCEPTION
      WHEN OTHERS THEN
        l_sec := 'Error in ' || l_err_callpoint;
        RAISE program_error;
    END;
  
    -- DEBUG NAME
    l_err_callpoint := 'step 4:  Start Main Loop for LawINopn';
    -- DEBUG NAME
    -- Getting list of location sections from HR PeopleSoft that are in the OPN Location table.
  
    l_sender := 'Oracle.Applications_' || l_sid || '@hdsupply.com';
  
    l_body_header := '<style type="text/css">
.style1 {
	border-style: solid;
	border-width: 1px;
}
.style2 {
	border: 1px solid #000000;
}
.style3 {
	border: 1px solid #000000;
	background-color: #FFFF00;
}
.style4 {
	color: #FFFF00;
	border: 1px solid #000000;
	background-color: #000000;
}
.style5 {
	font-size: large;
}
.style6 {
	font-size: xx-large;
}
.style7 {
	color: #FFCC00;
}
</style>
<p align="center"><span class="style6"><strong>INACTIVE FRU-LOC REPORT<br></strong></span></p>

<BR><table border="2" cellpadding="2" cellspacing="2" width="100%">
	<tr>
<td class="style2"><B>Date - Opened</B></td><td class="style2"><B>Date - Closed</B></td><td class="style2"><B>FRU</B></td><td class="style2"><B>Location</B></td><td class="style2`"><B>Comments</B></td><tr>';
  
    FOR c_email IN c_opninlaw2
    LOOP
    
      --Get list of FRULOC info for the Email.
      l_body_detail    := l_body_detail || '<td class="style3">' ||
                          substr(c_email.emp_assign_start_date, 1, 19) ||
                          '</td><td class="style3">' ||
                          substr(c_email.emp_assign_end_date, 1, 19) ||
                          '</td><td class="style3">' ||
                          substr(c_email.fru, 1, 16) ||
                          '</td><td class="style3">' ||
                          substr(c_email.location_code, 1, 17) ||
                          '</td><td class="style3">' ||
                          c_email.emp_space_comments || '</td><TR>';
      l_update_process := TRUE; -- should only have this value if the loop contains data.
    END LOOP;
  
    l_body_detail := l_body_detail;
  
    l_body_footer := '</tr></table><br>
                     These following FRU-LOCs are Inactive in Oracle Property Manager but are listed as Active as of today in PeopleSoft.<BR>';
  
    l_body := l_body_header || l_body_detail || l_body_footer;
  
    -- Send email .
    -- DEBUG NAME
    l_err_callpoint := 'step 4:  Sending email';
    -- DEBUG NAME
    IF l_update_process THEN
      xxcus_misc_pkg.html_email(p_to => l_dflt_email, p_from => l_sender,
                                p_text => 'test',
                                p_subject => '********INACTIVE FRU-LOC ALERT****INACTIVE FRU-LOC ALERT*********',
                                --p_subject       => l_subject,
                                p_html => l_body, p_smtp_hostname => l_host,
                                p_smtp_portnum => l_hostport);
    
      xxcus_misc_pkg.html_email(p_to => l_dflt_email3, p_from => l_sender,
                                p_text => 'test',
                                p_subject => '********INACTIVE FRU-LOC ALERT****INACTIVE FRU-LOC ALERT*********',
                                --p_subject       => l_subject,
                                p_html => l_body, p_smtp_hostname => l_host,
                                p_smtp_portnum => l_hostport);
    
    END IF;
  
    --****************************************************************************************************--
    -- DEBUG NAME
    l_err_callpoint := 'step 5:  Start Main Loop for LawNOTopn for Assignments';
    -- DEBUG NAME
    -- Getting list of location sections from HR PeopleSoft that are not in the OPN Location table.
  
    l_update_process := FALSE;
    l_error_flag     := FALSE;
  
  
    FOR c_adminlno2 IN lawnotopn2
    LOOP
      --intialize
      --  l_update_process := FALSE;
      --  l_error_flag     := FALSE;
    
      --  Email setups incase we need to email
    
      l_sender := 'Oracle.Applications_' || l_sid || '@hdsupply.com';
    
      l_body_header2 := '<style type="text/css">
.style1 {
	border-style: solid;
	border-width: 1px;
}
.style2 {
	border: 1px solid #000000;
}
.style3 {
	border: 1px solid #000000;
	background-color: #FFFF00;
}
.style4 {
	color: #FFFF00;
	border: 1px solid #000000;
	background-color: #000000;
}
.style5 {
	font-size: large;
}
.style6 {
	font-size: xx-large;
}
.style7 {
	color: #FFCC00;
}
</style>
<p align="center"><span class="style6"><strong>INVALID FRU-LOC REPORT<br></strong></span></p>

<BR><table border="2" cellpadding="2" cellspacing="2" width="100%">
	<tr>
<td class="style2"><B>FRULOC</B></td><td class="style2`"><B>Comments</B></td><tr>';
      --<td class="style2"><B>Date - Opened</B></td><td class="style2"><B>Date - Closed</B></td><td class="style2"><B>FRU</B></td><td class="style2"><B>Location</B></td><td class="style2`"><B>Comments</B></td><tr>';
    
      -- Choose the created PPL standard_type first, else use any location section.
      BEGIN
        SELECT lcs.location_id
              ,lcs.assignable_area
          INTO l_location_id
              ,l_assignable_area
          FROM pn.pn_locations_all lcs
         WHERE lcs.location_type_lookup_code = 'OFFICE'
           AND lcs.location_code LIKE '%.PEOPLE' --version 1.7
           AND substr(lcs.location_code, 1,
                      (instr(lcs.location_code, '.', 1) - 1)) =
               c_adminlno2.location_code;
      
        l_update_process := TRUE;
        /*  dbms_output.put_line('location_id ' || l_location_id ||
        '.  location code ' ||
        c_adminlno2.location_code);*/
      EXCEPTION
        WHEN no_data_found THEN
          /*  dbms_output.put_line('Entering nodatafound land for ' ||
          c_adminlno2.location_code);*/
          /*  version 1.1
          BEGIN
            SELECT lcs.location_id, lcs.assignable_area
            INTO l_location_id, l_assignable_area
            FROM pn.pn_locations_all lcs
            WHERE lcs.location_type_lookup_code = 'OFFICE'
            AND lcs.standard_type_lookup_code <> 'PPL'
            AND substr(lcs.location_code
                     ,1
                     ,(instr(lcs.location_code, '.', 1) - 1)) =
                  c_adminlno2.location_code
            AND rownum < 2;
            l_update_process := TRUE;
          EXCEPTION
            WHEN no_data_found THEN
              BEGIN
                SELECT lcs.location_id, lcs.assignable_area
                INTO l_location_id, l_assignable_area
                FROM pn.pn_locations_all lcs
                WHERE lcs.location_type_lookup_code = 'OFFICE'
                AND nvl(lcs.standard_type_lookup_code, 'PPL') = 'PPL' -- as last resort take the NULL values
                AND substr(lcs.location_code
                         ,1
                         ,(instr(lcs.location_code, '.', 1) - 1)) =
                      c_adminlno2.location_code
                AND rownum < 2;
                l_update_process := TRUE;
              EXCEPTION
                WHEN no_data_found THEN
               */ -- version 1.1
          fnd_file.put_line(fnd_file.output,
                            'Cannot setup Admin.  Could not find a FLOOR section for  ' ||
                             c_adminlno2.location_code);
        
          --Get list of FRULOC info for the Email.
        
          l_body_detail2 := l_body_detail2 || '<td class="style3">' ||
                            substr(c_adminlno2.fru || '-' ||
                                   c_adminlno2.location_code, 1, 29) ||
                            '</td><td class="style3">' ||
                            substr('Cannot setup Admin.  Could not find a LOCATION section.',
                                   1, 55) || '</td><TR>';
          l_error_flag   := TRUE;
        
        /* version 1.1
            END;
        END;
        */
      END;
      -- DEBUG NAME
      l_err_callpoint := 'Step 5: Finding the cost_center_code for ' ||
                         c_adminlno2.location_code || ' ';
      -- DEBUG NAME
      IF l_update_process THEN
        BEGIN
          SELECT entrp_loc
            INTO l_cost_center_code
            FROM apps.xxcus_location_code_vw
           WHERE fru = c_adminlno2.fru
             AND rownum < 2; -- this fix needs to be addressed
        EXCEPTION
          WHEN no_data_found THEN
            fnd_file.put_line(fnd_file.output,
                              'Cannot setup Admin.  Fru not found in GL data tables for ' ||
                               c_adminlno2.fru);
            --Get list of FRULOC info for the Email.
          
            l_body_detail2 := l_body_detail2 || '<td class="style3">' ||
                              substr(c_adminlno2.fru || '-' ||
                                     c_adminlno2.location_code, 1, 29) ||
                              '</td><td class="style3">' ||
                              substr('Cannot setup Admin.  Could not find a LOCATION section.',
                                     1, 55) || '</td><TR>';
            l_error_flag   := TRUE;
          
            l_update_process := FALSE;
        END;
      END IF;
    
      -- DEBUG NAME
      l_err_callpoint := 'step 5: creating the assignment ';
      -- DEBUG NAME
    
      IF l_update_process THEN
        /*   dbms_output.put_line('Entering create the rec land for ' ||
        c_adminlno2.location_code);*/
        INSERT INTO pn.pn_emp_space_assign_itf
          (batch_name
          ,entry_type
          ,location_id
          ,cost_center_code
          ,allocated_area
          ,last_update_date
          ,created_by
          ,creation_date
          ,last_updated_by
          ,attribute1
          ,attribute2
          ,SOURCE
          ,emp_assign_start_date)
        VALUES
          (l_batch
          ,'A'
          ,l_location_id
          ,l_cost_center_code
          ,0
          ,SYSDATE
          ,37367
          ,SYSDATE
          ,37367
          ,c_adminlno2.fru
          ,'Y'
          ,'PEOPLESOFT5'
          ,SYSDATE);
      
      END IF;
    
    END LOOP;
  
    IF l_error_flag THEN
      --dbms_output.put_line('l_error_flag');
      -- DEBUG NAME
      l_err_callpoint := 'step 5:  Sending email';
      -- DEBUG NAME
      l_body_detail2 := l_body_detail2;
    
      l_body_footer2 := '</tr></table><br>
                        The LOCs and/or FRUs assigned above, are not a valid IDs in Oracle.  Please correct your assignment in PeopleSoft.<BR>';
    
      l_body2 := l_body_header2 || l_body_detail2 || l_body_footer2;
    
      xxcus_misc_pkg.html_email(p_to => l_dflt_email2, p_from => l_sender,
                                p_text => 'test',
                                p_subject => '********INVALID FRU-LOC ALERT****INVALID FRU-LOC ALERT*********',
                                --p_subject       => l_subject,
                                p_html => l_body2, p_smtp_hostname => l_host,
                                p_smtp_portnum => l_hostport);
    
      xxcus_misc_pkg.html_email(p_to => l_dflt_email3, p_from => l_sender,
                                p_text => 'test',
                                p_subject => '********INVALID FRU-LOC ALERT****INVALID FRU-LOC ALERT*********',
                                --p_subject       => l_subject,
                                p_html => l_body2, p_smtp_hostname => l_host,
                                p_smtp_portnum => l_hostport);
    
    END IF;
  
    COMMIT;
  
    ----------------------------------------------------------------------------------------------------------------
  
    -- DEBUG NAME
    l_err_callpoint := 'step 6:  Start Main Loop for OPNnotLaw for Assignments';
    -- DEBUG NAME
    -- Getting list of location sections from HR PeopleSoft that are not in the OPN Location table.
    --dbms_output.put_line(l_err_callpoint);
    FOR c_adminonl2 IN c_opnnotlaw2
    LOOP
    
      INSERT INTO pn.pn_emp_space_assign_itf
        (batch_name
        ,entry_type
        ,location_id
        ,cost_center_code
        ,emp_space_assign_id
        ,allocated_area
        ,last_update_date
        ,created_by
        ,creation_date
        ,last_updated_by
        ,attribute1
        ,SOURCE
        ,emp_assign_start_date
        ,emp_assign_end_date)
      VALUES
        (l_batch
        ,'R'
        ,c_adminonl2.location_id
        ,c_adminonl2.cost_center_code
        ,c_adminonl2.emp_space_assign_id
        ,0
        ,SYSDATE
        ,37367
        ,SYSDATE
        ,37367
        ,c_adminonl2.attribute1
        ,'PEOPLESOFT6'
        ,c_adminonl2.emp_assign_start_date
        ,SYSDATE);
    
    END LOOP;
  
    -- DEBUG NAME
    l_err_callpoint := 'step 7:  ending the FRULOC.';
    -- DEBUG NAME
    -- Because in the SQL we check for 'PPL' based Locations, we are only ending the PeopleSoft based FRULOC.
  
    FOR c_adminuser IN c_userswitch
    LOOP
    
      INSERT INTO pn.pn_emp_space_assign_itf
        (batch_name
        ,entry_type
        ,location_id
        ,cost_center_code
        ,emp_space_assign_id
        ,allocated_area
        ,last_update_date
        ,created_by
        ,creation_date
        ,last_updated_by
        ,attribute1
        ,SOURCE
        ,emp_assign_start_date
        ,emp_assign_end_date)
      VALUES
        (l_batch
        ,'U'
        ,c_adminuser.location_id
        ,c_adminuser.cost_center_code
        ,c_adminuser.emp_space_assign_id
        ,c_adminuser.allocated_area
        ,SYSDATE
        ,37367
        ,SYSDATE
        ,37367
        ,c_adminuser.attribute1
        ,'PEOPLESOFT7'
        ,c_adminuser.emp_assign_start_date
        ,SYSDATE);
    
    END LOOP;
  
    -- V 3.0 
    -- DEBUG NAME
    l_err_callpoint := 'Step 8:  Create CCID for Cost Center code not exist in gl_code_combinations table.';
    -- DEBUG NAME
  
    -- Find missing Cost Center Code to create new CCID
    FOR c_costcenter1 IN (SELECT esai.cost_center_code
                                ,esai.attribute1 fru
                                ,esai.location_id
                            FROM pn.pn_emp_space_assign_itf esai
                           WHERE esai.batch_name = l_batch
                             AND esai.cost_center_code NOT IN
                                 (SELECT segment2
                                    FROM gl.gl_code_combinations))
    LOOP
    
      -- Get product code and LOB
      SELECT entrp_entity
            ,business_unit
        INTO l_entrp_entity
            ,l_lob
        FROM apps.xxcus_location_code_vw
       WHERE fru = c_costcenter1.fru;
    
      -- Generate CCID
      IF l_lob LIKE 'FM%' THEN
        l_code_combination := l_entrp_entity || '.' ||
                              c_costcenter1.cost_center_code || '.' ||
                              l_entrp_cc_fm || '.' || l_account || '.' ||
                              l_segment5 || '.' || l_segment6 || '.' ||
                              l_segment7;
      
        l_ccid := xxcus_misc_pkg.create_ccid_generic(p_entrp_entity => l_entrp_entity,
                                                     p_entrp_loc => c_costcenter1.cost_center_code,
                                                     p_entpr_cc => l_entrp_cc_fm,
                                                     p_account => l_account,
                                                     p_segment5 => l_segment5,
                                                     p_segment6 => l_segment6,
                                                     p_segment7 => l_segment7);
      ELSIF l_lob LIKE 'WW%' THEN
        l_code_combination := l_entrp_entity || '.' ||
                              c_costcenter1.cost_center_code || '.' ||
                              l_entrp_cc_ww || '.' || l_account || '.' ||
                              l_segment5 || '.' || l_segment6 || '.' ||
                              l_segment7;
      
        l_ccid := xxcus_misc_pkg.create_ccid_generic(p_entrp_entity => l_entrp_entity,
                                                     p_entrp_loc => c_costcenter1.cost_center_code,
                                                     p_entpr_cc => l_entrp_cc_ww,
                                                     p_account => l_account,
                                                     p_segment5 => l_segment5,
                                                     p_segment6 => l_segment6,
                                                     p_segment7 => l_segment7);
      ELSIF l_lob LIKE 'CT%' THEN
        l_code_combination := l_entrp_entity || '.' ||
                              c_costcenter1.cost_center_code || '.' ||
                              l_entrp_cc_cti || '.' || l_account || '.' ||
                              l_segment5 || '.' || l_segment6 || '.' ||
                              l_segment7;
      
        l_ccid := xxcus_misc_pkg.create_ccid_generic(p_entrp_entity => l_entrp_entity,
                                                     p_entrp_loc => c_costcenter1.cost_center_code,
                                                     p_entpr_cc => l_entrp_cc_cti,
                                                     p_account => l_account,
                                                     p_segment5 => l_segment5,
                                                     p_segment6 => l_segment6,
                                                     p_segment7 => l_segment7);
      ELSE
        l_code_combination := l_entrp_entity || '.' ||
                              c_costcenter1.cost_center_code || '.' ||
                              l_entrp_cc || '.' || l_account || '.' ||
                              l_segment5 || '.' || l_segment6 || '.' ||
                              l_segment7;
      
        l_ccid := xxcus_misc_pkg.create_ccid_generic(p_entrp_entity => l_entrp_entity,
                                                     p_entrp_loc => c_costcenter1.cost_center_code,
                                                     p_entpr_cc => l_entrp_cc,
                                                     p_account => l_account,
                                                     p_segment5 => l_segment5,
                                                     p_segment6 => l_segment6,
                                                     p_segment7 => l_segment7);
      END IF;
    
      -- Send notification if the CCID was not successfully generated.
      IF l_ccid = 0 THEN
      
        l_sender := 'Oracle.Applications_' || l_sid || '@hdsupply.com';
      
        l_body_header3 := '<style type="text/css">
.style1 {
	border-style: solid;
	border-width: 1px;
}
.style2 {
	border: 1px solid #000000;
}
.style3 {
	border: 1px solid #000000;
	background-color: #FFFF00;
}
.style4 {
	color: #FFFF00;
	border: 1px solid #000000;
	background-color: #000000;
}
.style5 {
	font-size: large;
}
.style6 {
	font-size: xx-large;
}
.style7 {
	color: #FFCC00;
}
</style>
<p align="center"><span class="style6"><strong>INVALID OPN ACCOUNT COMBINATION REPORT<br></strong></span></p>

<BR><table border="2" cellpadding="2" cellspacing="2" width="100%">
	<tr>
<td class="style2"><B>LOC - FRU - COST CENTER</B></td><td class="style2`"><B>Comments</B></td><tr>';
      
        SELECT substr(location_code, 1, 5)
          INTO l_location_code
          FROM pn.pn_locations_all
         WHERE location_id = c_costcenter1.location_id;
      
        l_body_detail3 := l_body_detail3 || '<td class="style3">' ||
                          substr(l_location_code || ' - ' ||
                                 c_costcenter1.fru || ' - ' ||
                                 c_costcenter1.cost_center_code, 1, 29) ||
                          '</td><td class="style3">' ||
                          substr('Cannot setup Admin.  ENTRP_LOC code ' ||
                                 c_costcenter1.cost_center_code ||
                                 ' does not exist in GL_CODE_COMBINATIONS table.' ||
                                 '</br><br>' ||
                                 'DEBUG: The CCID value is: ' || l_ccid ||
                                 '</br><br>' ||
                                 'If the CCID value is 0, then the segment combination provided is invalid.  Please validate the segments with GL team.  Code Combination: ' ||
                                 l_code_combination, 1, 1000) ||
                          '</td><TR>';
      
        -- DEBUG NAME
        l_err_callpoint := 'step 8:  Sending email';
        -- DEBUG NAME
        l_body_detail3 := l_body_detail3;
      
        l_body_footer3 := '</tr></table><br>
                        The Cost Center Code assigned above, are not a valid in Oracle.<BR>';
      
        l_body3 := l_body_header3 || l_body_detail3 || l_body_footer3;
      
        xxcus_misc_pkg.html_email(p_to => l_distro_list, p_from => l_sender,
                                  p_text => 'test',
                                  p_subject => '********INVALID OPN Account Combination ALERT****INVALID OPN Account Combination ALERT*********',
                                  --p_subject       => l_subject,
                                  p_html => l_body3,
                                  p_smtp_hostname => l_host,
                                  p_smtp_portnum => l_hostport);
      
        xxcus_misc_pkg.html_email(p_to => l_gl_support, p_from => l_sender,
                                  p_text => 'test',
                                  p_subject => '********INVALID OPN Account Combination ALERT****INVALID OPN Account Combination ALERT*********',
                                  --p_subject       => l_subject,
                                  p_html => l_body3,
                                  p_smtp_hostname => l_host,
                                  p_smtp_portnum => l_hostport);
      
      END IF;
    END LOOP;
  
    --START SPACE ASSIGNMENT IMPORT--
  
    -- get count of processes
    SELECT COUNT(*)
      INTO l_count_process
      FROM pn.pn_emp_space_assign_itf
     WHERE batch_name = l_batch;
  
    IF l_count_process > 0 THEN
      --Run the concurrent job for imports for locations
      BEGIN
        -- DEBUG NAME
        l_err_callpoint := 'step 3: Running concurrent job for assignments import';
        -- DEBUG NAME
      
        l_can_submit_request := xxcus_misc_pkg.set_responsibility('OPNINTERFACE',
                                                                  'Property Manager');
        IF l_can_submit_request THEN
          l_globalset := 'Global Variables are set.';
        
        ELSE
        
          l_globalset := 'Global Variables are not set.';
          l_sec       := 'Global Variables are not set for the Responsibility of xxcus_CON and the User of Conversion.';
          fnd_file.put_line(fnd_file.log, l_sec);
          fnd_file.put_line(fnd_file.output, l_sec);
          RAISE program_error;
        END IF;
      
        fnd_file.put_line(fnd_file.log, l_globalset);
        fnd_file.put_line(fnd_file.output, l_globalset);
        --  END OF Setup parameters for running FND JOBS!
      
        BEGIN
        
          l_req_id := fnd_request.submit_request('PN', 'PNIMPCAD',
                                                 'PNIMPCAD - Import From CAD',
                                                 NULL, FALSE, l_batch, 'S',
                                                 g_org_id);
        
          COMMIT;
        
          IF (l_req_id != 0) THEN
            IF fnd_concurrent.wait_for_request(l_req_id, 6, 15000, v_phase,
                                               v_status, v_dev_phase,
                                               v_dev_status, v_message) THEN
              v_error_message := chr(10) || 'ReqID=' || l_req_id ||
                                 ' DPhase ' || v_dev_phase || ' DStatus ' ||
                                 v_dev_status || chr(10) || ' MSG - ' ||
                                 v_message;
              -- Error Returned
              IF v_dev_phase != 'COMPLETE' THEN
                --v1.12               -- OR v_dev_status != 'NORMAL' THEN
                l_statement := 'An error occured in the running of the Assignment import' ||
                               v_error_message || '.';
                fnd_file.put_line(fnd_file.log, l_statement);
                fnd_file.put_line(fnd_file.output, l_statement);
                --
                xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_err_callfrom,
                                                     p_calling => l_err_callpoint,
                                                     p_ora_error_msg => SQLERRM,
                                                     p_error_desc => 'Assignment Interface completed with Error.  Check records for batch ' ||
                                                                      l_batch,
                                                     p_distribution_list => l_distro_list,
                                                     p_module => 'PN',
                                                     p_request_id => l_req_id);
              END IF;
              -- Then Success!
            ELSE
              l_statement := 'An error occured running the Assignment import' ||
                             v_error_message || '.';
              fnd_file.put_line(fnd_file.log, l_statement);
              fnd_file.put_line(fnd_file.output, l_statement);
              RAISE program_error;
            END IF;
          
          ELSE
            l_statement := 'An error occured when trying to submitting the Assignment import';
            fnd_file.put_line(fnd_file.log, l_statement);
            fnd_file.put_line(fnd_file.output, l_statement);
            RAISE program_error;
          END IF;
          fnd_file.put_line(fnd_file.output,
                            'Processed ' || to_char(l_count_process) ||
                             ' records.');
        
        END;
      END;
    END IF;
  
    ----------------------------------------------------------------------------------------------------------------
  
    -- EXCEPTIONS
  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;
      l_err_code := 2;
      l_sec      := 'Error in ' || l_err_callpoint;
      l_err_msg  := l_sec;
      l_err_msg  := l_err_msg || ' WITH  ' || substr(SQLERRM, 1, 1000);
      l_fulltext := l_err_msg;
      --dbms_output.put_line(l_fulltext);
      fnd_file.put_line(fnd_file.log, l_fulltext);
      fnd_file.put_line(fnd_file.output, l_fulltext);
      retcode := l_err_code;
      errbuf  := l_err_msg;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_err_callfrom,
                                           p_calling => l_err_callpoint,
                                           p_ora_error_msg => SQLERRM,
                                           p_error_desc => 'Error running Interface package with PROGRAM ERROR',
                                           p_distribution_list => l_distro_list,
                                           p_module => 'PN');
    
      fnd_file.put_line(fnd_file.output, 'Fix the error!');
    
    WHEN OTHERS THEN
      ROLLBACK;
      l_err_code := 2;
      l_sec      := 'Error in ' || l_err_callpoint;
      l_err_msg  := l_sec;
      l_err_msg  := l_err_msg || ' WITH  ' || substr(SQLERRM, 1, 1000);
      l_fulltext := l_err_msg;
      --dbms_output.put_line(l_fulltext);
      fnd_file.put_line(fnd_file.log, l_fulltext);
      fnd_file.put_line(fnd_file.output, l_fulltext);
      retcode := l_err_code;
      errbuf  := l_err_msg;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_err_callfrom,
                                           p_calling => l_err_callpoint,
                                           p_ora_error_msg => SQLERRM,
                                           p_error_desc => 'Error running Interface package with PROGRAM ERROR',
                                           p_distribution_list => l_distro_list,
                                           p_module => 'PN');
    
  END pn_admin_process;

END xxcuspn_interface_pkg;
/
