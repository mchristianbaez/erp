CREATE OR REPLACE PACKAGE BODY APPS.XXWC_DB_GATHER_STATS_PKG
AS
   /*************************************************************************
     $Header XXWC_DB_GATHER_STATS $
     Module Name: XXWC_DB_GATHER_STATS.pkb

     PURPOSE:   Gather schema stats in DB for non-application schemas

     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.0        09/12/2016  Pattabhi Avula           Initial Version TMS#20160829-00243

   **************************************************************************/

   g_dflt_email   fnd_user.email_address%TYPE
                     := 'HDSOracleDevelopers@hdsupply.com';

   PROCEDURE DB_GATHER_STATS (errbuf              OUT VARCHAR2,
                              retcode             OUT VARCHAR2,
                              p_schema_value   IN     VARCHAR2)
   IS
      CURSOR SCHEMA_C1 (p_schema_name1 VARCHAR2)
      IS
         SELECT ffvv.flex_value schema_name
           FROM apps.fnd_flex_values_vl ffvv, apps.fnd_flex_value_sets ffvs
          WHERE     ffvv.flex_value_set_id = ffvs.flex_value_set_id
                AND ffvv.ENABLED_FLAG = 'Y'
                AND DECODE (p_schema_name1,
                            'ALL', ffvv.flex_value,
                            p_schema_name1) = ffvv.flex_value
                AND ffvs.flex_value_set_name = 'XXWC_DB_GATHER_STATS_VS'
                AND EXISTS
                       (SELECT 1
                          FROM DBA_TABLES
                         WHERE OWNER = FFVV.FLEX_VALUE);

      l_sec   VARCHAR2 (100);
   BEGIN
      FOR I IN SCHEMA_C1 (UPPER (p_schema_value))
      LOOP
         l_sec := i.schema_name || ' Schema Gather Stats Begin ';

         FND_FILE.PUT_LINE (
            FND_FILE.LOG,
            l_sec || ' ' || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));

         DBMS_STATS.GATHER_SCHEMA_STATS (ownname => i.schema_name);

         l_sec := i.schema_name || ' Schema Gather Stats End ';

         FND_FILE.PUT_LINE (
            FND_FILE.LOG,
            l_sec || ' ' || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
      END LOOP;
   EXCEPTION
      WHEN OTHERS
      THEN
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_DB_GATHER_STATS_PKG.DB_GATHER_STATS',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (SQLERRM, 1, 2000),
            p_error_desc          => 'Error in Non_apps_schema_stats Procedure, When Others Exception',
            p_distribution_list   => g_dflt_email,
            p_module              => 'XXWC');
   END;
END XXWC_DB_GATHER_STATS_PKG;
/