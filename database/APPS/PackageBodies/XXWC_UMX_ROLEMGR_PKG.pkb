CREATE OR REPLACE PACKAGE BODY XXWC_UMX_ROLEMGR_PKG IS
   /*************************************************************************
    Copyright (c) 2012 Lucidity Consulting Group
    All rights reserved.
   **************************************************************************
     $Header XXWC_UMX_ROLEMGR_PKG $
     Module Name: XXWC_UMX_ROLEMGR_PKG.pkb

     PURPOSE:   Concurrent program to change user password of firefighter to be called through Role Manager

     REVISIONS:
     Ver        Date         Author                     Description
     ---------  ----------   ---------------         -------------------------
     1.0        11-JUL-2016  Niraj K Ranjan          Initial Version TMS#20160629-00068   
	                                                 Role Manager - Create EBS Concurrent 
													 Program for User End Dating and Firefighter Password Resets
  **************************************************************************/
  -- Error DEBUG
  l_err_callfrom  VARCHAR2(75) DEFAULT 'XXWC_UMX_ROLEMGR_PKG';
  g_dflt_email   fnd_user.email_address%TYPE    := 'WC-ITDEVALERTS-U1@HDSupply.com';
  g_retmsg   VARCHAR2(2000);
    
   /********************************************************************************
   ProcedureName : XXWC_CHECK_USER_ACTIVE
   Purpose       : Check User if it is active

   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.0     12-JUL-2016   Niraj K Ranjan  Initial Version TMS#20160629-00068
   ********************************************************************************/ 
   FUNCTION xxwc_check_user_exists(p_user_name IN VARCHAR2) 
   RETURN VARCHAR2 IS
      l_user_count NUMBER;
      l_exists_flag VARCHAR2(1) := 'Y';
   BEGIN
     SELECT COUNT(1) INTO l_user_count
        FROM apps.fnd_user usr
       WHERE usr.user_name = UPPER(p_user_name)
	   AND NVL(usr.end_date,SYSDATE+1) > SYSDATE;

     IF l_user_count > 0 THEN
        l_exists_flag := 'Y';
     ELSE
        l_exists_flag := 'N';
     END IF;
     RETURN l_exists_flag;
   EXCEPTION
     WHEN OTHERS THEN
        l_exists_flag := 'E';
        RETURN l_exists_flag;
   END xxwc_check_user_exists;

  /********************************************************************************
   ProcedureName : XXWC_RESET_PASSWORD
   Purpose       : CP to change password - Main Procedure

   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.0     11-JUL-2016   Niraj K Ranjan  Initial Version  TMS#20160629-00068
   ********************************************************************************/ 
  PROCEDURE xxwc_reset_password(p_errbuf       OUT VARCHAR2
                               ,p_retcode      OUT NUMBER
							   ,p_user_name    IN  VARCHAR2
							   ,p_new_password IN  VARCHAR2) 
  IS

     l_user_exception         EXCEPTION;
     l_err_msg                VARCHAR2 (2000);
     l_err_code               VARCHAR2(200);
     l_sec                    VARCHAR2 (150);
	 l_user_exist_ind         VARCHAR2(1);
	 l_status                 BOOLEAN;
	 l_user_sub_string        NUMBER;
	 l_start_string_const     CONSTANT   VARCHAR2(7) := 'XXWC_FF';
  BEGIN
     p_errbuf   := 'Success';
     p_retcode  := '0';
     l_err_code := NULL;
	 l_err_msg  := NULL;
     g_retmsg   := NULL;
     fnd_file.put_line (fnd_file.LOG ,'----------------------------------------------------------------------------------');
     fnd_file.put_line (fnd_file.LOG , 'Start of Procedure : xxwc_reset_password'||' @ '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
     fnd_file.put_line (fnd_file.LOG ,'----------------------------------------------------------------------------------');
	 fnd_file.put_line (fnd_file.LOG ,'**********************************************************************************');
	 fnd_file.put_line (fnd_file.LOG ,'Parameter1=>User Name:'||p_user_name);
	 --fnd_file.put_line (fnd_file.LOG ,'g_retmsg:'||g_retmsg);
     l_sec := 'Start logic';
     BEGIN
        --Delete temp table
        EXECUTE IMMEDIATE 'TRUNCATE TABLE XXWC.XXWC_UMX_ROLEMGR_MSG_TBL';

		--Check if user / password is not null
		l_sec := 'Mandatory check for User';
		IF p_user_name IS NULL THEN
           l_err_code := 'Error 1';
           l_err_msg := 'User can not be null';
		   fnd_file.put_line (fnd_file.LOG ,l_err_code||': '||l_err_msg);
		   g_retmsg := l_err_msg;
		   RAISE l_user_exception;
		END IF;
		l_sec := 'Mandatory check for password';
		IF p_new_password IS NULL THEN
           l_err_code := 'Error 2';
           l_err_msg := 'Password can not be null';
		   fnd_file.put_line (fnd_file.LOG ,l_err_code||': '||l_err_msg);
		   g_retmsg := l_err_msg;
		   RAISE l_user_exception;
		END IF;
        
		--Check if user name satisfy the start string validation - XXWC_FF and XXWC_INT
        SELECT DECODE(INSTR(p_user_name,'XXWC_FF',1,1),1,1,
                      DECODE(INSTR(p_user_name,'XXWC_INT',1,1),1,1,0)
                     ) 
               INTO l_user_sub_string
        FROM DUAL;
		--l_user_start_string := TRIM(SUBSTR(p_user_name,1,length(l_start_string_const)));
        fnd_file.put_line (fnd_file.LOG ,'l_user_sub_string: '||l_user_sub_string);
		IF l_user_sub_string = 0 THEN
           l_err_code := 'Error 3';
           l_err_msg := 'User '||p_user_name||' is not a firefighter';
		   fnd_file.put_line (fnd_file.LOG ,l_err_code||': '||l_err_msg);
		   g_retmsg := l_err_msg;
		   RAISE l_user_exception;
		END IF;

        --Check if user exist
		l_sec := 'Validate User';
		l_user_exist_ind := xxwc_check_user_exists(p_user_name);
		IF l_user_exist_ind = 'N' THEN
           l_err_code := 'Error 4';
           l_err_msg := 'User '||p_user_name||' is either inactive or does not exist';
		   fnd_file.put_line (fnd_file.LOG ,l_err_code||': '||l_err_msg);
		   g_retmsg := l_err_msg;
		   RAISE l_user_exception;
		END IF;
        
		--Check password length between 8 to 45 char
        l_sec := 'Validate password length';
		IF LENGTH(p_new_password) < 8 THEN
           l_err_code := 'Error 5';
           l_err_msg := 'Password is too short';
		   fnd_file.put_line (fnd_file.LOG ,l_err_code||': '||l_err_msg);
		   g_retmsg := l_err_msg;
		   RAISE l_user_exception;
		END IF;
		IF LENGTH(p_new_password) > 45 THEN
           l_err_code := 'Error 6';
           l_err_msg := 'Password is too long';
		   fnd_file.put_line (fnd_file.LOG ,l_err_code||': '||l_err_msg);
		   g_retmsg := l_err_msg;
		   RAISE l_user_exception;
		END IF;

		--Check if user is authentic to chnage the password
        l_sec := 'Validate if password is changeable';
        IF apps.fnd_user_pkg.ispasswordchangeable(p_user_name) THEN
		   fnd_file.put_line (fnd_file.LOG ,'Password is changeable');
        ELSE
           l_err_code := 'Error 7';
		   l_err_msg := 'Password is not changeable';
           fnd_file.put_line (fnd_file.LOG ,l_err_code||': '||l_err_msg);
		   g_retmsg := l_err_msg;
		   RAISE l_user_exception;
        END IF;

		--Change Password
        l_sec := 'call fnd_user_pkg.changepassword to change password';
		l_status := apps.fnd_user_pkg.changepassword (username => p_user_name, newpassword => p_new_password);
        IF l_status THEN
           fnd_file.put_line (fnd_file.LOG ,'Password is Changed Successfully');
		   g_retmsg := 'Password is Changed Successfully';
        ELSE
          l_err_code := 'Error 8';
          l_err_msg := 'Password is not Changed for user '||p_user_name||' due to violation of oracle rule';
          fnd_file.put_line (fnd_file.LOG ,l_err_code||': '||l_err_msg);
		  g_retmsg := l_err_msg;
		  RAISE l_user_exception;
		  --ROLLBACK;
        END IF;
     EXCEPTION
        WHEN l_user_exception THEN
           p_errbuf := 'Warning';
           p_retcode := '1';
           ROLLBACK;
     END;
	 --p_errbuf := g_retmsg;
     INSERT INTO XXWC_UMX_ROLEMGR_MSG_TBL(REQUEST_ID,USER_NAME,RETURN_MESSAGE) 
	                   VALUES(fnd_global.conc_request_id,p_user_name,g_retmsg);
     COMMIT;
     fnd_file.put_line (fnd_file.LOG ,'**********************************************************************************');
     fnd_file.put_line (fnd_file.LOG ,'----------------------------------------------------------------------------------');
     fnd_file.put_line (fnd_file.LOG , 'End of Procedure : xxwc_reset_password'||' @ '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
     fnd_file.put_line (fnd_file.LOG ,'----------------------------------------------------------------------------------');
	 --fnd_file.put_line (fnd_file.LOG ,'g_retmsg:'||g_retmsg);
  EXCEPTION
     WHEN OTHERS THEN
       p_errbuf := 'Error';
       p_retcode := '2';
	   l_err_code := SQLCODE;
       l_err_msg  := SUBSTR(SQLERRM,1,2000);
       fnd_file.put_line (fnd_file.LOG, l_sec||'=>'||l_err_code||'=>'||l_err_msg);
       fnd_file.put_line (fnd_file.output, l_sec||'=>'||l_err_code||'=>'||l_err_msg);
	   ROLLBACK;
       xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_UMX_ROLEMGR_PKG.XXWC_RESET_PASSWORD'
           ,p_calling             => l_sec
           ,p_request_id          => fnd_global.conc_request_id
           ,p_ora_error_msg       => SQLERRM
           ,p_error_desc          => 'Error while Changing Password'
           ,p_distribution_list   => g_dflt_email
           ,p_module              => 'XXWC');
  END xxwc_reset_password;
  
  /********************************************************************************
   ProcedureName : SUBMIT_PW_RESET_CP
   Purpose       : Submit concurrent program "XXWC Role Manager User Password Reset".
                   This is a wraper procedure which will be called from Role Manager.

   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.0     13-JUL-2016   Niraj K Ranjan  Initial Version  TMS#20160629-00068
   ********************************************************************************/
   PROCEDURE submit_pw_reset_cp(p_user_name    IN  VARCHAR2
							   ,p_new_password IN  VARCHAR2
							   ,p_retcode      OUT  NUMBER
							   ,p_retmsg       OUT  VARCHAR2) 
   IS
       PRAGMA AUTONOMOUS_TRANSACTION;
       l_req_id                 NUMBER; 
	   l_complete               BOOLEAN;
       l_phase                  VARCHAR2 (100);
       l_status                 VARCHAR2 (100);
       l_dev_phase              VARCHAR2 (100);
       l_dev_status             VARCHAR2 (100);
       l_message                VARCHAR2 (100);
	   l_retcode                NUMBER;
	   l_retmsg                 VARCHAR2(2000);
	   l_user_excp              EXCEPTION;  
       l_user_name              VARCHAR2(100) := 'XXWC_INT_FINANCE';	   
       l_responsibility_key     VARCHAR2(30) := 'XXCUS_CON';	   
       l_user_id                NUMBER;      --FND_GLOBAL.USER_ID;
	   l_responsibility_id      NUMBER;      --FND_GLOBAL.RESP_ID;
	   l_resp_application_id    NUMBER;      --FND_GLOBAL.RESP_APPL_ID;
	   --l_can_submit_request BOOLEAN := TRUE;
	   --l_login_id               NUMBER := FND_GLOBAL.LOGIN_ID;
	   l_can_submit_request    VARCHAR2(2000);
   BEGIN
      BEGIN
	     SELECT user_id 
		 INTO l_user_id
		 FROM fnd_user 
		 WHERE user_name = l_user_name;
	  EXCEPTION
	     WHEN OTHERS THEN
            l_retcode := 2;
            l_retmsg  := 'User '|| l_user_name ||' is not found to submit EBS concurrent program';
			RAISE l_user_excp;
	  END;
	  
      BEGIN
         SELECT responsibility_id,application_id
		 INTO  l_responsibility_id,l_resp_application_id
         FROM  FND_RESPONSIBILITY_VL 
         WHERE RESPONSIBILITY_KEY = l_responsibility_key;
      EXCEPTION
         WHEN OTHERS THEN
            l_retcode := 2;
            l_retmsg  := 'Responsibility '||l_responsibility_key||' is not found to submit EBS concurrent program';
			RAISE l_user_excp;
	  END;
      FND_GLOBAL.APPS_INITIALIZE (l_user_id,
                                 l_responsibility_id,
                                 l_resp_application_id
								);
	 
      l_req_id := fnd_request.submit_request ( application   => 'XXWC',
                                               program       => 'XXWC_UMX_RM_PW_RESET',
                                               description   => NULL,
                                               start_time    => SYSDATE,
                                               sub_request   => FALSE,
                                               argument1     => p_user_name,
                                               argument2     => p_new_password
											  );
	  COMMIT;
	  IF l_req_id > 0 THEN

         l_complete := fnd_concurrent.wait_for_request (request_id      => l_req_id
                                                       ,interval        => 2
                                                       ,max_wait        => 3600
                                                       ,phase           => l_phase
                                                       ,status          => l_status
                                                       ,dev_phase       => l_dev_phase
                                                       ,dev_status      => l_dev_status
                                                       ,message         => l_message
                                                       );													   
         IF l_complete THEN
		    IF UPPER(l_dev_phase) = 'COMPLETE' THEN
               --Fetch return message from concurrent program submitted above pass on to Role Manager tool
               BEGIN
                  SELECT return_message INTO g_retmsg
	              FROM xxwc_umx_rolemgr_msg_tbl;
	           EXCEPTION
	              WHEN OTHERS THEN
		             g_retmsg := SUBSTR(SQLERRM,1,2000);
	           END;

               IF UPPER(l_dev_status) = 'WARNING' THEN
                  l_retcode := 1;
			      l_retmsg  := g_retmsg;
				  --UPPER(l_dev_status)||'! '||'Password is not changed. Please refer log file for request id:'||l_req_id;
               ELSIF UPPER(l_dev_status) = 'ERROR' THEN
                  l_retcode := 2;
			      l_retmsg  := UPPER(l_dev_status)||'! '||'Password is not changed. Please refer log file for request id:'||l_req_id;
               ELSIF UPPER(l_dev_status) = 'NORMAL' THEN
                  l_retcode := 0;
			      l_retmsg  := g_retmsg;
				  --'Password is changed successfully';
               ELSE
                  l_retcode := 2;
			      l_retmsg  := UPPER(l_dev_status)||'! '||'Password is not changed. Please refer log file for request id:'||l_req_id;
               END IF;
            ELSE
               l_retcode := 2;
			   l_retmsg  := UPPER(l_dev_phase)||'! '||'Password is not changed. Please refer log file for request id:'||l_req_id;
            END IF;
		 ELSE
		    l_retcode := 2;
			l_retmsg  := 'EBS Conc Program Wait timed out '||'for request id:'||l_req_id;
            --fnd_file.put_line (fnd_file.LOG, l_statement);
            --RAISE PROGRAM_ERROR;
		 END IF;
      ELSE
         l_retcode := 2;
         l_retmsg  := 'EBS Conc Program not initated';
         --fnd_file.put_line (fnd_file.LOG, l_statement);
         --RAISE PROGRAM_ERROR;
	  END IF;
      p_retcode := l_retcode;
      p_retmsg  := l_retmsg;
   EXCEPTION
      WHEN l_user_excp THEN
         p_retcode := l_retcode;
         p_retmsg  := l_retmsg;
      WHEN OTHERS THEN
         p_retcode := 2;
         p_retmsg  := SUBSTR(SQLERRM,1,2000);
   END submit_pw_reset_cp;
   
END XXWC_UMX_ROLEMGR_PKG;
/
