CREATE OR REPLACE PACKAGE BODY APPS.xxcusdw_pricing_extract_pkg IS
  /*******************************************************************************
  
  DESCRIPTION
                 This interface is the model for working with the Datawarehouse ETL tool.
                 The data-warehouse user INTERFACE_DSTAGE will have right to execute a stand alone
                 procedure that will execute a procedure from this package.  The DSTAGE
                 permissions will be restricted to execute only the specific execute and
                 they will not be granted access to the core tables.  They only will be able
                 to select from the end result table in the procedure below.
                 This reduces the footprint of the system in each other and directly points
                 to which system is encountering the issue:  If the calling system is executing
                 the result table is truncated and will have 0 rows.  If there are row in
                 the result table then any further issue is on the calling system (unless
                 it is the first run and you have a permission problem with the DSTAGE user).
                 It is very very important that the INTERFACE_DSTAGE user's access is restricted as much
                 as possible.
  
  ------------------------------------------------------------------------------------
  --  Procedure: LOAD_GL_INT_TABLE                                                  --
  ------------------------------------------------------------------------------------
  --  Version    Date         Created By        Description                         --
  ------------------------------------------------------------------------------------
  --    1.0      03/30/2012   Luong Vu          Migrated program from 11i to R12    --
  --
  ------------------------------------------------------------------------------------*/

  PROCEDURE load_gl_int_table(errbuf    OUT VARCHAR2
                             ,retcode   OUT NUMBER
                             ,p_fperiod IN VARCHAR2) IS
  
    --Intialize Variables
    l_ledger_id NUMBER;
    l_coa       NUMBER DEFAULT 50328;
  
    -- Error DEBUG
    l_err_msg        VARCHAR2(2000);
    l_err_code       NUMBER;
    l_sec            VARCHAR2(150);
    l_procedure_name VARCHAR2(75) := 'XXCUSDW_PRICING_EXTRACT_PKG.LOAD_GL_INT_TABLE';
    l_distro_list    VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
  
    --Start Main Program
  BEGIN
    l_sec := 'Start Main Program; ';
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
  
    --Truncate the Summary Table
    BEGIN
      l_sec := 'Truncate the summary table before loading specified time frame.';
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      EXECUTE IMMEDIATE 'TRUNCATE TABLE XXCUS.XXCUSDW_PRICING_GL_TBL';
    
    EXCEPTION
      WHEN OTHERS THEN
        l_err_msg := 'Failed to Truncate XXCUSDW_PRICING_GL_TBL: ' ||
                     SQLERRM;
        fnd_file.put_line(fnd_file.log, l_err_msg);
        RAISE program_error;
    END;
  
    -- Get ledger_id
    l_sec := 'Get ledger_id';
    SELECT ledger_id
      INTO l_ledger_id
      FROM gl.gl_ledgers
     WHERE NAME = 'HD Supply USD';
  
    --Insert Global Header GTT
    l_sec := 'Loading the cust data table; ';
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
  
    INSERT /*+ APPEND */
    INTO xxcus.xxcusdw_pricing_gl_tbl
      SELECT line_of_bus
            ,branch
            ,period_name
            ,period_year
            ,period_num
            ,pnl_hierarchy
            ,period_debit
            ,period_credit
            ,period_net
        FROM (SELECT fv.description line_of_bus
                    ,lc.fru branch
                    ,d.period_name
                    ,d.period_year period_year
                    ,d.period_num
                    ,f.pnl_order
                    ,f.pnl_hierarchy
                    ,SUM(nvl(d.period_net_dr, 0)) period_debit
                    ,SUM(nvl(d.period_net_cr, 0)) period_credit
                    ,SUM(nvl(d.period_net_dr, 0)) -
                     SUM(nvl(d.period_net_cr, 0)) period_net
                FROM gl.gl_balances d
                    ,gl.gl_code_combinations e
                    ,xxcus.xxcusdw_pricing_glpl_range_tbl f
                    ,applsys.fnd_flex_values ff
                    ,applsys.fnd_flex_values_tl fv
                    ,(SELECT loc.fru
                            ,loc.entrp_entity
                            ,loc.entrp_loc
                            ,loc.entrp_cc
                        FROM apps.xxcus_location_code_vw loc) lc
               WHERE d.code_combination_id = e.code_combination_id
                 AND lc.entrp_entity = ff.flex_value
                 AND ff.flex_value_id = fv.flex_value_id
                 AND ff.flex_value_set_id =
                     (SELECT fvs.flex_value_set_id
                        FROM applsys.fnd_flex_value_sets fvs
                       WHERE fvs.flex_value_set_name = 'XXCUS_GL_PRODUCT')
                 AND e.segment1 = lc.entrp_entity
                 AND e.segment2 = lc.entrp_loc
                 AND e.segment3 = lc.entrp_cc
                 AND d.period_name = p_fperiod
                 AND d.actual_flag = 'A'
                 AND d.currency_code <> 'STAT'
                 AND ledger_id = l_ledger_id
                 AND e.chart_of_accounts_id = l_coa
                 AND to_number(e.segment4) >= f.from_account
                 AND to_number(e.segment4) <= f.thru_account
               GROUP BY fv.description
                       ,lc.fru
                       ,f.pnl_order
                       ,f.pnl_hierarchy
                       ,d.period_name
                       ,d.period_year
                       ,d.period_num) a
       ORDER BY line_of_bus
               ,branch
               ,pnl_order;
  
    COMMIT;
  
    l_sec := 'Delete the cust data table ';
    DELETE FROM xxcus.xxcusdw_pricing_gl_total_tbl
     WHERE period_name = p_fperiod;
  
    COMMIT;
    l_sec := 'Insert data into xxcusdw_pricing_gl_total_tbl ';
    INSERT /*+ APPEND */
    INTO xxcus.xxcusdw_pricing_gl_total_tbl
      (SELECT *
         FROM xxcus.xxcusdw_pricing_gl_tbl);
  
    COMMIT;
  
  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := 'Error in program ' || l_procedure_name || ' Error: ' ||
                    l_sec;
      l_err_msg  := l_err_msg || ' ERROR ' || substr(SQLERRM, 1, 2000);
    
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode := l_err_code;
      errbuf  := l_err_msg;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_procedure_name,
                                           p_calling => l_sec,
                                           p_ora_error_msg => SQLERRM,
                                           p_error_desc => l_err_msg,
                                           p_distribution_list => l_distro_list,
                                           p_request_id => NULL);
    
    WHEN OTHERS THEN
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := l_sec || l_err_msg || ' ERROR ' || SQLCODE ||
                    substr(SQLERRM, 1, 2000);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode := l_err_code;
      errbuf  := l_err_msg;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_procedure_name,
                                           p_calling => l_sec,
                                           p_ora_error_msg => SQLERRM,
                                           p_error_desc => l_err_msg,
                                           p_distribution_list => l_distro_list,
                                           p_request_id => NULL);
    
  END load_gl_int_table;

END xxcusdw_pricing_extract_pkg;
/
