CREATE OR REPLACE PACKAGE BODY APPS.xxcus_fixed_asset_conv_pkg AS

  ---------------------------------------------------------------------------------
  -- Load Fixed Assets into staging table - XXCUS_FA_STG_TBL
  ---------------------------------------------------------------------------------
  PROCEDURE load_stg(p_errbuf   OUT VARCHAR2
                    ,p_retcode  OUT NUMBER
                    ,p_lob_name IN VARCHAR2) AS
  BEGIN
  
    fnd_file.put_line(fnd_file.log
                     ,'----------------------------------------------------------------------------------------------');
    fnd_file.put_line(fnd_file.log, ' Start of procedure - load_stg');
  
    EXECUTE IMMEDIATE 'TRUNCATE TABLE XXCUS.XXCUS_FA_STG_TBL';
  
    INSERT INTO xxcus.xxcus_fa_stg_tbl
      (book
      ,fiscal_year
      ,period
      ,period_counter
      ,units_assigned
      ,asset_number
      ,home_branch
      ,custodian_branch
      ,legal_entity
      ,state
      ,city
      ,prod
      ,location
      ,cost_center
      ,project_code
      ,future_use
      ,future_use2
      ,expense_account
      ,reserve_account
      ,fa_description
      ,major_category
      ,minor_category
      ,physical_inv
      ,units
      ,serial_number
      ,tag_number
      ,dps_
      ,depr_method
      ,life
      ,cost
      ,deprn_amount
      ,ytd_deprn
      ,deprn_reserve
      ,cost_account
      ,attribute1
      ,attribute2
      ,attribute3
      ,attribute4
      ,attribute5
      ,attribute6
      ,attribute7
      ,attribute8
      ,category_id
      ,location_id
      ,invoice_number
      ,po_number
      ,invoice_desc
      ,processed)
      (SELECT dp.book_type_code "BOOK"
              ,dp.fiscal_year "FISCAL_YEAR"
              ,dp.period_name "PERIOD"
              ,dp.period_counter "PERIOD_COUNTER"
              ,rsv_ldg_gt.units_assigned "UNITS_ASSIGNED"
              ,fa.asset_number "ASSET_NUMBER"
              ,loc.segment1 "HOME_BRANCH"
              ,loc.segment2 "CUSTODIAN_BRANCH"
              ,loc.segment3 "LEGAL_ENTITY"
              ,loc.segment4 "STATE"
              ,loc.segment5 "CITY"
              ,cc.segment1 "PROD"
              ,cc.segment2 "LOCATION"
              ,cc.segment3 "COST_CENTER"
              ,cc.segment5 "Project_Code"
              ,cc.segment6 "Future_Use"
              ,'00000' "Future_Use2"
              ,fcb.deprn_expense_acct "EXPENSE_ACCOUNT"
              ,rsv_ldg_gt.deprn_reserve_acct "RESERVE_ACCOUNT"
              ,fa.description "FA_DESCRIPTION"
              ,cb.segment1 "MAJOR_CATEGORY"
              ,cb.segment2 "MINOR_CATEGORY"
              ,fa.inventorial "PHYSICAL_INV"
              ,fa.current_units "UNITS"
              ,fa.serial_number "SERIAL_NUMBER"
              ,fa.tag_number "TAG_NUMBER"
              ,rsv_ldg_gt.date_placed_in_service "DPS"
              ,rsv_ldg_gt.method_code "DEPR_METHOD"
              ,rsv_ldg_gt.life "LIFE"
              ,rsv_ldg_gt.cost "COST"
              ,rsv_ldg_gt.deprn_amount "DEPRN_AMOUNT"
              ,rsv_ldg_gt.ytd_deprn "YTD_DEPRN"
              ,rsv_ldg_gt.deprn_reserve "DEPRN_RESERVE"
              ,fcb.asset_cost_acct "COST_ACCOUNT"
              ,fa.attribute1 "ATTRIBUTE1"
              ,fa.attribute2 "ATTRIBUTE2"
              ,fa.attribute3 "ATTRIBUTE3"
              ,fa.attribute4 "ATTRIBUTE4"
              ,fa.attribute5 "ATTRIBUTE5"
              ,fa.attribute6 "ATTRIBUTE6"
              ,fa.attribute7 "ATTRIBUTE7"
              ,fa.attribute8 "ATTRIBUTE8"
              ,cb.category_id "CATEGORY_ID"
              ,loc.location_id "LOCATION_ID"
              ,fai.invoice_number "INVOICE_NUMBER"
              ,fai.po_number "PO_NUMBER"
              ,fai.description "INV_DESCRIPTION"
              ,null
         FROM --R12
              --xxcus.XXCUSFA_RESERVE_LEDGER_TBL RSV_LDG_GT
              --R11
               xxhsi.xxhsi_fa_reserve_ledger@r12_to_fin.hsi.hughessupply.com rsv_ldg_gt
             ,apps.fa_additions@r12_to_fin.hsi.hughessupply.com             fa
             ,gl.gl_code_combinations@r12_to_fin.hsi.hughessupply.com       cc
             ,fa.fa_locations@r12_to_fin.hsi.hughessupply.com               loc
             ,fa.fa_deprn_periods@r12_to_fin.hsi.hughessupply.com           dp
             ,fa.fa_category_books@r12_to_fin.hsi.hughessupply.com          fcb
             ,fa.fa_categories_b@r12_to_fin.hsi.hughessupply.com            cb
             ,fa.fa_asset_invoices@r12_to_fin.hsi.hughessupply.com          fai
        WHERE rsv_ldg_gt.asset_id = fa.asset_id
          AND rsv_ldg_gt.dh_ccid = cc.code_combination_id
          AND rsv_ldg_gt.location_id = loc.location_id
          AND rsv_ldg_gt.period_counter = dp.period_counter
          AND rsv_ldg_gt.deprn_reserve_acct = fcb.deprn_reserve_acct
          AND cb.category_id = fa.asset_category_id
          AND dp.book_type_code = p_lob_name --'HDS WHITE CAP'
          AND fcb.book_type_code = dp.book_type_code
          AND cb.category_id = fcb.category_id
          AND rsv_ldg_gt.asset_id = fai.asset_id(+)
          AND (rsv_ldg_gt.transaction_type IS NULL OR
              --rsv_ldg_gt.transaction_type IN ('P', 'F', 'T', 'N', 'R', 'B')
              rsv_ldg_gt.transaction_type IN ('N')));
  
    fnd_file.put_line(fnd_file.log, ' End of procedure - load_stg');
    fnd_file.put_line(fnd_file.log
                     ,'----------------------------------------------------------------------------------------------');
  
  EXCEPTION
    WHEN OTHERS THEN
      fnd_file.put_line(fnd_file.log
                       ,'Error in the Procedure - load_stg - ' || SQLERRM);
  END load_stg;

  ---------------------------------------------------------------------------------
  -- Load Fixed Assets into Oracle
  ---------------------------------------------------------------------------------
  PROCEDURE load_fa(p_errbuf OUT VARCHAR2, p_retcode OUT NUMBER) AS
  
    l_trans_rec           fa_api_types.trans_rec_type;
    l_dist_trans_rec      fa_api_types.trans_rec_type;
    l_asset_hdr_rec       fa_api_types.asset_hdr_rec_type;
    l_asset_desc_rec      fa_api_types.asset_desc_rec_type;
    l_asset_cat_rec       fa_api_types.asset_cat_rec_type;
    l_asset_type_rec      fa_api_types.asset_type_rec_type;
    l_asset_hierarchy_rec fa_api_types.asset_hierarchy_rec_type;
    l_asset_fin_rec       fa_api_types.asset_fin_rec_type;
    l_asset_deprn_rec     fa_api_types.asset_deprn_rec_type;
    l_asset_dist_rec      fa_api_types.asset_dist_rec_type;
    l_asset_desc_dff_rec  fa_api_types.desc_flex_rec_type;
  
    l_asset_dist_tbl       fa_api_types.asset_dist_tbl_type;
    l_asset_dist_empty_tbl fa_api_types.asset_dist_tbl_type;
  
    l_inv_rec      fa_api_types.inv_rec_type;
    l_inv_tbl      fa_api_types.inv_tbl_type;
    l_inv_rate_tbl fa_api_types.inv_rate_tbl_type;
  
    l_ccid          NUMBER;
    l_msg_count     NUMBER;
    l_return_status VARCHAR2(200);
    l_msg_data      VARCHAR2(4000);
    l_dist_cntr     NUMBER := 0;
    l_asset_number  VARCHAR2(200);
    l_category_id   NUMBER;
    l_location_id   NUMBER;
  
    CURSOR c_assets IS
      SELECT c.r12_book book
             ,asset_number "ASSET_NUMBER"
             ,prod
             ,reserve_account
             ,fa_description
             ,c.r12_seg1 major_category
             ,c.r12_seg2 minor_category
             ,physical_inv
             ,units
             ,serial_number
             ,tag_number
             ,dps_
             ,depr_method
             ,life
             ,SUM(to_number(cost)) cost
             ,SUM(to_number(deprn_amount)) deprn_amount
             ,SUM(to_number(ytd_deprn)) ytd_deprn
             ,SUM(to_number(deprn_reserve)) deprn_reserve
             ,attribute1
             ,attribute2
             ,attribute3
             ,attribute4
             ,attribute5
             ,attribute6
             ,attribute7
             ,attribute8
             ,cost_account
             ,invoice_number
             ,po_number
             ,invoice_desc
        FROM xxcus.xxcus_fa_stg_tbl a, xxcus.xxcusfa_category_conv_tbl c
       WHERE a.book = c.r11_book
         AND a.major_category = c.r11_seg1
         AND a.minor_category = c.r11_seg2
         AND a.asset_number NOT IN ('WC65140', '151334', '144448', 'WC72485', 'WC77260')
         AND a.asset_number NOT IN
             (SELECT asset_number
                FROM fa_additions fa
               WHERE fa.asset_number = a.asset_number)
       GROUP BY c.r12_book
               ,asset_number
               ,prod
               ,reserve_account
               ,fa_description
               ,c.r12_seg1 -- major_category
               ,c.r12_seg2 -- minor_category
               ,physical_inv
               ,units
               ,serial_number
               ,tag_number
               ,dps_
               ,depr_method
               ,life
               ,attribute1
               ,attribute2
               ,attribute3
               ,attribute4
               ,attribute5
               ,attribute6
               ,attribute7
               ,attribute8
               ,cost_account
               ,invoice_number
               ,po_number
               ,invoice_desc;
  
    CURSOR cur_dist(p_asset_num VARCHAR2) IS
      SELECT units
            ,location
            ,units_assigned
            ,home_branch
            ,custodian_branch
            ,legal_entity
            ,state
            ,city
            ,cost_center
            ,expense_account
            ,project_code
        FROM xxcus.xxcus_fa_stg_tbl
       WHERE asset_number = p_asset_num;
  
  BEGIN
  
    fnd_file.put_line(fnd_file.log
                     ,'----------------------------------------------------------------------------------------------');
    fnd_file.put_line(fnd_file.log
                     ,'----------------------------------------------------------------------------------------------');
    fnd_file.put_line(fnd_file.log, ' Start of procedure - load_fa');
  
    fa_srvr_msg.init_server_message;
    fa_debug_pkg.initialize;
  
    FOR rec_c_assets IN c_assets
    LOOP
      fnd_file.put_line(fnd_file.log
                       ,'----------------------------------------------------------------------------------------------');
      fnd_file.put_line(fnd_file.log
                       ,'AssetNumber - ' || rec_c_assets.asset_number);
    
      l_ccid        := NULL;
      l_category_id := NULL;
    
      l_dist_trans_rec      := NULL;
      l_asset_hdr_rec       := NULL;
      l_asset_desc_rec      := NULL;
      l_asset_type_rec      := NULL;
      l_asset_cat_rec       := NULL;
      l_asset_hierarchy_rec := NULL;
      l_asset_fin_rec       := NULL;
      l_asset_deprn_rec     := NULL;
      l_asset_desc_dff_rec  := NULL;
    
      ----------------------------------------------------------------------------------------------
      -- Derive CategoryId
      ----------------------------------------------------------------------------------------------
      BEGIN
        SELECT category_id
          INTO l_category_id
          FROM apps.fa_categories fc
         WHERE 1 = 1
           AND upper(fc.segment1) = upper(rec_c_assets.major_category)
           AND upper(fc.segment2) = upper(rec_c_assets.minor_category);
        /*     upper(decode(rec_c_assets.major_category
                        ,'COMP EQUIP'
                        ,'Comp Equipment'
                        ,'BUILDG'
                        ,'Building'
                        ,rec_c_assets.major_category))
        AND REPLACE(upper(fc.segment2), '&', '1') =
            upper(decode(REPLACE(rec_c_assets.minor_category, '&', '1')
                        ,'LAND IMPR-DEDUCTIBLE'
                        ,'Land Impr - Deductible'
                        ,'LAND IMPR-DEPR'
                        ,'Land Impr - Deprec'
                        ,'LAND IMPR-NON DEDUCT'
                        ,'Land Impr - Non Deduct'
                        ,'ROUT 1 SWITCH'
                        ,'Rout 1 Swith'
                        ,'DEL VANS1FORKLIFTS-NEW'
                        ,'Del Vans1 Forklifts - New'
                        ,'DEL VANS1FORKLIFTS-USED'
                        ,'Del Vans1 Forklifts - Used'
                        ,'BUILDG'
                        ,'Building'
                        ,'BUILDG IMP'
                        ,'Building Imp'
                        ,REPLACE(rec_c_assets.minor_category, '&', '1')));   */
      
        fnd_file.put_line(fnd_file.log, 'l_category_id - ' || l_category_id);
      EXCEPTION
        WHEN OTHERS THEN
          NULL;
          fnd_file.put_line(fnd_file.log
                           ,'Error deriving category_id - ' || SQLERRM);
      END;
    
      l_asset_desc_rec.serial_number := rec_c_assets.serial_number;
      l_asset_desc_rec.description   := substr(rec_c_assets.fa_description
                                              ,1
                                              ,80);
    
      IF rec_c_assets.tag_number IS NOT NULL
      THEN
        l_asset_desc_rec.tag_number := rec_c_assets.tag_number;
        --l_asset_desc_rec.tag_number := 'AN3_' || rec_c_assets.tag_number;
      END IF;
      l_asset_cat_rec.category_id   := l_category_id;
      l_asset_desc_rec.asset_number := rec_c_assets.asset_number;
    
      l_asset_desc_dff_rec.attribute1 := substr(rec_c_assets.attribute1, 1, 150);
      l_asset_desc_dff_rec.attribute2 := substr(rec_c_assets.attribute2, 1, 150);
      l_asset_desc_dff_rec.attribute3 := substr(rec_c_assets.attribute3, 1, 150);
      l_asset_desc_dff_rec.attribute4 := substr(rec_c_assets.attribute4, 1, 150);
      l_asset_desc_dff_rec.attribute5 := substr(rec_c_assets.attribute5, 1, 150);
      l_asset_desc_dff_rec.attribute6 := substr(rec_c_assets.attribute6, 1, 150);
      l_asset_desc_dff_rec.attribute7 := substr(rec_c_assets.attribute7, 1, 150);
      l_asset_desc_dff_rec.attribute8 := substr(rec_c_assets.attribute8, 1, 150);
    
      l_asset_cat_rec.desc_flex := l_asset_desc_dff_rec;
    
      --type info
      l_asset_type_rec.asset_type := 'CAPITALIZED';
    
      --IF rec_c_assets.invoice_number IS NOT NULL THEN
    
      -- invoice info
      l_inv_rec.fixed_assets_cost  := rec_c_assets.cost;
      l_inv_rec.description        := rec_c_assets.invoice_desc; --'Asset Conversion';
      l_inv_rec.invoice_number     := rec_c_assets.invoice_number;
      l_inv_rec.feeder_system_name := '12.1.3'; --'11.5.8';
      --IF rec_c_assets.po_number IS NOT NULL THEN
    
      l_inv_rec.po_number := rec_c_assets.po_number;
      --END IF;
      l_inv_tbl(1) := l_inv_rec;
      --END IF;
    
      fnd_file.put_line(fnd_file.log
                       ,'   AssetNumber - ' || rec_c_assets.asset_number ||
                        '   Cost - ' || rec_c_assets.cost || '   DeprnAmt - ' ||
                        rec_c_assets.deprn_amount || '   YtdDeprn - ' ||
                        rec_c_assets.ytd_deprn || '   DeprnResrv - ' ||
                        rec_c_assets.deprn_reserve);
    
      l_asset_fin_rec.cost                   := rec_c_assets.cost;
      l_asset_fin_rec.date_placed_in_service := fnd_conc_date.string_to_date(rec_c_assets.dps_);
      l_asset_fin_rec.depreciate_flag        := 'YES';
      l_asset_fin_rec.deprn_method_code      := rec_c_assets.depr_method;
      l_asset_fin_rec.life_in_months         := rec_c_assets.life;
    
      -- deprn info
      l_asset_deprn_rec.ytd_deprn           := 0;
      l_asset_deprn_rec.deprn_reserve       := 0;
      l_asset_deprn_rec.bonus_ytd_deprn     := 0;
      l_asset_deprn_rec.bonus_deprn_reserve := 0;
    
      -- book / trans info
      l_asset_hdr_rec.book_type_code         := rec_c_assets.book;
      l_trans_rec.transaction_date_entered   := fnd_conc_date.string_to_date(rec_c_assets.dps_);
      l_trans_rec.who_info.last_updated_by   := -1; -- Fnd_Global.USER_ID;
      l_trans_rec.who_info.last_update_date  := SYSDATE;
      l_trans_rec.who_info.created_by        := -1; -- Fnd_Global.USER_ID;
      l_trans_rec.who_info.creation_date     := SYSDATE;
      l_trans_rec.who_info.last_update_login := -1; -- Fnd_Global.USER_ID;
    
      l_asset_number   := NULL;
      l_asset_number   := rec_c_assets.asset_number;
      l_dist_cntr      := 0;
      l_asset_dist_tbl := l_asset_dist_empty_tbl;
    
      FOR rec_dist IN cur_dist(l_asset_number)
      LOOP
        l_location_id := NULL;
        l_ccid        := NULL;
        l_dist_cntr   := l_dist_cntr + 1;
      
        BEGIN
          SELECT code_combination_id
            INTO l_ccid
            FROM gl_code_combinations
           WHERE segment1 = rec_c_assets.prod
             AND segment2 = rec_dist.location
             AND segment3 = rec_dist.cost_center
             AND segment4 = rec_dist.expense_account
             AND segment5 = rec_dist.project_code
             AND segment6 = '00000'
             AND segment7 = '00000';
        
          fnd_file.put_line(fnd_file.log, 'l_ccid - ' || l_ccid);
        EXCEPTION
          WHEN OTHERS THEN
            NULL;
            fnd_file.put_line(fnd_file.log
                             ,'Error deriving Expense CCID - ' || SQLERRM);
        END;
      
        BEGIN
          SELECT fl.location_id
            INTO l_location_id
            FROM fa_locations fl
           WHERE upper(fl.segment1) = upper(rec_dist.home_branch)
             AND upper(fl.segment2) = upper(rec_dist.custodian_branch)
             AND upper(fl.segment3) =
                 upper(decode(rec_dist.legal_entity
                             ,'HUGHES CANADA, INC.'
                             ,'HDS Canada, Inc'
                             ,'HUGHES ELECTRIC SUPPLY LTD'
                             ,'HDS Electric Supply LTD'
                             ,'HUGHES MRO LTD'
                             ,'HDS MRO LTD'
                             ,'HUGHES SUPPLY (VA) INC'
                             ,'HD Supply (VA) INC'
                             ,'HUGHES SUPPLY INC'
                             ,'HD Supply Inc'
                             ,'HUGHES SUPPLY SHRD SRVCS INC'
                             ,'HD Supply SHRD SRVCS INC'
                             ,'HUGHES UTILITIES LTD'
                             ,'HD Supply Utilities, LTD'
                             ,'UTILITIES PRODUCTS SUPP CO LLC'
                             ,'Utilities Products Supply LLC'
                             ,'HUGHES PLUMBING SUPPLY LTD'
                             ,'HDS Electric Supply LTD'
                             ,'HUGHES BUILDING MATERIALS LTD'
                             ,'HDS Building Materials LTD'
                             ,upper(rec_dist.legal_entity)))
             AND upper(fl.segment5) = upper(rec_dist.city)
             AND upper(fl.segment4) = upper(rec_dist.state);
        
          fnd_file.put_line(fnd_file.log, 'l_location_id - ' || l_location_id);
        EXCEPTION
          WHEN OTHERS THEN
            NULL;
            fnd_file.put_line(fnd_file.log
                             ,'Error deriving location_id - ' || SQLERRM);
        END;
      
        l_asset_dist_rec                   := NULL;
        l_asset_dist_rec.units_assigned    := NULL;
        l_asset_dist_rec.expense_ccid      := NULL;
        l_asset_dist_rec.location_ccid     := NULL;
        l_asset_dist_rec.assigned_to       := NULL;
        l_asset_dist_rec.transaction_units := NULL;
      
        -- distribution info
        l_asset_dist_rec.units_assigned := rec_dist.units_assigned;
        l_asset_dist_rec.expense_ccid := l_ccid;
        l_asset_dist_rec.location_ccid := l_location_id;
        l_asset_dist_rec.assigned_to := NULL;
        l_asset_dist_rec.transaction_units := rec_dist.units;
        l_asset_dist_tbl(l_dist_cntr) := l_asset_dist_rec;
      END LOOP;
    
      l_msg_data      := NULL;
      l_msg_count     := 0;
      l_return_status := NULL;
    
      BEGIN
        --  4) Finally Call API
        fa_addition_pub.do_addition(p_api_version          => 1.0
                                   ,p_init_msg_list        => fnd_api.g_true
                                   ,p_commit               => fnd_api.g_false
                                   ,p_validation_level     => fnd_api.g_valid_level_full
                                   ,x_return_status        => l_return_status
                                   ,x_msg_count            => l_msg_count
                                   ,x_msg_data             => l_msg_data
                                   ,p_calling_fn           => NULL
                                   ,px_trans_rec           => l_trans_rec
                                   ,px_dist_trans_rec      => l_dist_trans_rec
                                   ,px_asset_hdr_rec       => l_asset_hdr_rec
                                   ,px_asset_desc_rec      => l_asset_desc_rec
                                   ,px_asset_type_rec      => l_asset_type_rec
                                   ,px_asset_cat_rec       => l_asset_cat_rec
                                   ,px_asset_hierarchy_rec => l_asset_hierarchy_rec
                                   ,px_asset_fin_rec       => l_asset_fin_rec
                                   ,px_asset_deprn_rec     => l_asset_deprn_rec
                                   ,px_asset_dist_tbl      => l_asset_dist_tbl
                                   ,px_inv_tbl             => l_inv_tbl);
      
        COMMIT;
      
        --dump messages
        l_msg_count := fnd_msg_pub.count_msg;
      
        IF l_msg_count > 0
        THEN
          l_msg_data := chr(10) || substr(fnd_msg_pub.get(fnd_msg_pub.g_first
                                                         ,fnd_api.g_false)
                                         ,1
                                         ,250);
          fnd_file.put_line(fnd_file.log, l_msg_data);
        
          FOR i IN 1 .. (l_msg_count - 1)
          LOOP
            l_msg_data := substr(fnd_msg_pub.get(fnd_msg_pub.g_next
                                                ,fnd_api.g_false)
                                ,1
                                ,250);
            fnd_file.put_line(fnd_file.log, l_msg_data);
          END LOOP;
          fnd_msg_pub.delete_msg();
        END IF;
      
        IF (l_return_status <> fnd_api.g_ret_sts_success)
        THEN
          NULL;
          fnd_file.put_line(fnd_file.log, 'FAILURE');
        ELSE
          NULL;
          fnd_file.put_line(fnd_file.log, 'SUCCESS');
          fnd_file.put_line(fnd_file.log
                           ,'THID - ' ||
                            to_char(l_trans_rec.transaction_header_id));
          fnd_file.put_line(fnd_file.log
                           ,'ASSET_ID - ' || to_char(l_asset_hdr_rec.asset_id));
          fnd_file.put_line(fnd_file.log
                           ,'ASSET_NUMBER - ' || l_asset_desc_rec.asset_number);
        END IF;
      EXCEPTION
        WHEN OTHERS THEN
          NULL;
          -- fnd_file.put_line(fnd_file.log,'Error - '||SQLERRM);
      END;
    END LOOP;
  
    fnd_file.put_line(fnd_file.output
                     ,'----------------------------------------------------------------------------------------------');
    fnd_file.put_line(fnd_file.output
                     ,'----------------------------------------------------------------------------------------------');
    fnd_file.put_line(fnd_file.output
                     ,'ASSET_NUMBER        11i_COST            12g_COST            ');
    fnd_file.put_line(fnd_file.output
                     ,'----------------------------------------------------------------------------------------------');
    FOR rec_output IN (SELECT (rpad(fa.asset_number, 20, ' ') ||
                              rpad(to_number(stg.cost), 20, ' ') ||
                              rpad(to_number(fb.cost), 20, ' ')) out_line
                         FROM fa_additions fa
                             ,fa_books fb
                             ,(SELECT stg.asset_number, SUM(stg.cost) cost
                                 FROM xxcus.xxcus_fa_stg_tbl stg
                                GROUP BY stg.asset_number) stg
                        WHERE 1 = 1
                          AND fa.asset_id = fb.asset_id
                          AND stg.asset_number = fa.asset_number
                          --AND 'AN3_' || stg.asset_number = fa.asset_number
                          --AND fa.asset_number LIKE 'AN3_%'
                        ORDER BY fa.asset_number)
    LOOP
    
      fnd_file.put_line(fnd_file.output, rec_output.out_line);
    
    END LOOP;
  
    fnd_file.put_line(fnd_file.output
                     ,'----------------------------------------------------------------------------------------------');
    fnd_file.put_line(fnd_file.output
                     ,'----------------------------------------------------------------------------------------------');
  
    fnd_file.put_line(fnd_file.log, ' End of procedure load_fa');
    fnd_file.put_line(fnd_file.log
                     ,'----------------------------------------------------------------------------------------------');
    fnd_file.put_line(fnd_file.log
                     ,'----------------------------------------------------------------------------------------------');
  
  EXCEPTION
    WHEN OTHERS THEN
      NULL;
      fnd_file.put_line(fnd_file.log
                       ,'Error in the Procedure - load_fa - ' || SQLERRM);
  END load_fa;
  
  ---------------------------------------------------------------------------------
  -- Load Fixed Assets into Oracle Mass Additions   3-1-2012  Kathy Poling
  ---------------------------------------------------------------------------------
  PROCEDURE load_mass_add(p_errbuf OUT VARCHAR2, p_retcode OUT NUMBER) AS
  
    l_ccid          NUMBER;
    l_msg_count     NUMBER;
    l_return_status VARCHAR2(200);
    l_msg_data      VARCHAR2(4000);
    l_dist_cntr     NUMBER := 0;
    l_asset_number  VARCHAR2(200);
    l_category_id   NUMBER;
    l_location_id   NUMBER;
    l_location      VARCHAR2(200);
    l_units         VARCHAR2(200);
    l_branch        VARCHAR2(200);
    l_custodian_br  VARCHAR2(200);
    l_entity        VARCHAR2(200);
    l_state         VARCHAR2(200);
    l_city          VARCHAR2(200);
    l_cc            VARCHAR2(200);
    l_exp_acct      VARCHAR2(200);
    l_project       VARCHAR2(200);
    l_rownum        VARCHAR2(200);
    l_tag           VARCHAR2(200);
  
    CURSOR c_assets IS
      SELECT c.r12_book book
             ,asset_number "ASSET_NUMBER"
             ,prod
             ,reserve_account
             ,fa_description
             ,c.r12_seg1 major_category
             ,c.r12_seg2 minor_category
             ,physical_inv
             ,units
             ,serial_number
             ,tag_number
             ,dps_
             ,depr_method
             ,life
             ,SUM(to_number(cost)) cost
             ,SUM(to_number(deprn_amount)) deprn_amount
             ,SUM(to_number(ytd_deprn)) ytd_deprn
             ,SUM(to_number(deprn_reserve)) - SUM(to_number(deprn_amount)) deprn_reserve
             ,attribute1
             ,attribute2
             ,attribute3
             ,attribute4
             ,attribute5
             ,attribute6
             ,attribute7
             ,attribute8
             ,cost_account
             ,invoice_number
             ,po_number
             ,invoice_desc
        FROM xxcus.xxcus_fa_stg_tbl a, xxcus.xxcusfa_category_conv_tbl c
       WHERE a.book = c.r11_book
         AND a.major_category = c.r11_seg1
         AND a.minor_category = c.r11_seg2
         AND a.asset_number NOT IN
             ('WC65140', '151334', '144448', 'WC72485', 'WC77260')
         AND a.asset_number NOT IN
             (SELECT asset_number
                FROM fa_additions fa
               WHERE fa.asset_number = a.asset_number)
       GROUP BY c.r12_book
               ,asset_number
               ,prod
               ,reserve_account
               ,fa_description
               ,c.r12_seg1 -- major_category
               ,c.r12_seg2 -- minor_category
               ,physical_inv
               ,units
               ,serial_number
               ,tag_number
               ,dps_
               ,depr_method
               ,life
               ,attribute1
               ,attribute2
               ,attribute3
               ,attribute4
               ,attribute5
               ,attribute6
               ,attribute7
               ,attribute8
               ,cost_account
               ,invoice_number
               ,po_number
               ,invoice_desc;
  
  BEGIN
  
    fnd_file.put_line(fnd_file.log
                     ,'----------------------------------------------------------------------------------------------');
    fnd_file.put_line(fnd_file.log
                     ,'----------------------------------------------------------------------------------------------');
    fnd_file.put_line(fnd_file.log, ' Start of procedure - load_fa');
  
    FOR rec_c_assets IN c_assets
    LOOP
    
      fnd_file.put_line(fnd_file.log
                       ,'----------------------------------------------------------------------------------------------');
      fnd_file.put_line(fnd_file.log
                       ,'AssetNumber - ' || rec_c_assets.asset_number);
    
      dbms_output.put_line('AssetNumber - ' || rec_c_assets.asset_number);
    
      l_ccid        := NULL;
      l_category_id := NULL;
    
      ----------------------------------------------------------------------------------------------
      -- Derive CategoryId
      ----------------------------------------------------------------------------------------------
      BEGIN
        SELECT category_id
          INTO l_category_id
          FROM apps.fa_categories fc
         WHERE 1 = 1
           AND upper(fc.segment1) = upper(rec_c_assets.major_category)
           AND upper(fc.segment2) = upper(rec_c_assets.minor_category);
      
        fnd_file.put_line(fnd_file.log, 'l_category_id - ' || l_category_id);
        dbms_output.put_line('l_category_id - ' || l_category_id);
      EXCEPTION
        WHEN OTHERS THEN
          NULL;
          fnd_file.put_line(fnd_file.log
                           ,'Error deriving category_id - ' || SQLERRM);
        
          dbms_output.put_line('Error deriving category_id - ' || SQLERRM);
      END;
    
      fnd_file.put_line(fnd_file.log
                       ,'   AssetNumber - ' || rec_c_assets.asset_number ||
                        '   Cost - ' || rec_c_assets.cost || '   DeprnAmt - ' ||
                        rec_c_assets.deprn_amount || '   YtdDeprn - ' ||
                        rec_c_assets.ytd_deprn || '   DeprnResrv - ' ||
                        rec_c_assets.deprn_reserve);
    
      dbms_output.put_line('   AssetNumber - ' || rec_c_assets.asset_number ||
                           '   Cost - ' || rec_c_assets.cost ||
                           '   DeprnAmt - ' || rec_c_assets.deprn_amount ||
                           '   YtdDeprn - ' || rec_c_assets.ytd_deprn ||
                           '   DeprnResrv - ' || rec_c_assets.deprn_reserve);
    
      BEGIN
        SELECT ROWID
              ,location
              ,units_assigned
              ,home_branch
              ,custodian_branch
              ,legal_entity
              ,state
              ,city
              ,cost_center
              ,expense_account
              ,project_code
          INTO l_rownum
              ,l_location
              ,l_units
              ,l_branch
              ,l_custodian_br
              ,l_entity
              ,l_state
              ,l_city
              ,l_cc
              ,l_exp_acct
              ,l_project
          FROM xxcus.xxcus_fa_stg_tbl
         WHERE asset_number = rec_c_assets.asset_number
           AND rownum = 1;
      
      EXCEPTION
        WHEN OTHERS THEN
          NULL;
          fnd_file.put_line(fnd_file.log
                           ,'Error getting assignments - ' || SQLERRM);
        
          dbms_output.put_line('Error getting assignments - ' || SQLERRM);
      END;
    
      l_location_id := NULL;
      l_ccid        := NULL;
    
      BEGIN
        SELECT code_combination_id
          INTO l_ccid
          FROM gl_code_combinations
         WHERE segment1 = rec_c_assets.prod
           AND segment2 = l_location
           AND segment3 = l_cc
           AND segment4 = l_exp_acct
           AND segment5 = l_project
           AND segment6 = '00000'
           AND segment7 = '00000';
      
        fnd_file.put_line(fnd_file.log, 'l_ccid - ' || l_ccid);
        dbms_output.put_line('l_ccid - ' || l_ccid);
      EXCEPTION
        WHEN OTHERS THEN
          NULL;
          fnd_file.put_line(fnd_file.log
                           ,'Error deriving Expense CCID - ' || SQLERRM);
          dbms_output.put_line('Error deriving Expense CCID - ' || SQLERRM);
        
      END;
    
      BEGIN
        SELECT fl.location_id
          INTO l_location_id
          FROM fa_locations fl
         WHERE upper(fl.segment1) = upper(l_branch)
           AND upper(fl.segment2) = upper(l_custodian_br)
           AND upper(fl.segment3) =
               upper(decode(l_entity
                           ,'HUGHES CANADA, INC.'
                           ,'HDS Canada, Inc'
                           ,'HUGHES ELECTRIC SUPPLY LTD'
                           ,'HDS Electric Supply LTD'
                           ,'HUGHES MRO LTD'
                           ,'HDS MRO LTD'
                           ,'HUGHES SUPPLY (VA) INC'
                           ,'HD Supply (VA) INC'
                           ,'HUGHES SUPPLY INC'
                           ,'HD Supply Inc'
                           ,'HUGHES SUPPLY SHRD SRVCS INC'
                           ,'HD Supply SHRD SRVCS INC'
                           ,'HUGHES UTILITIES LTD'
                           ,'HD Supply Utilities, LTD'
                           ,'UTILITIES PRODUCTS SUPP CO LLC'
                           ,'Utilities Products Supply LLC'
                           ,'HUGHES PLUMBING SUPPLY LTD'
                           ,'HDS Electric Supply LTD'
                           ,'HUGHES BUILDING MATERIALS LTD'
                           ,'HDS Building Materials LTD'
                           ,upper(l_entity)))
           AND upper(fl.segment5) = upper(l_city)
           AND upper(fl.segment4) = upper(l_state);
      
        fnd_file.put_line(fnd_file.log, 'l_location_id - ' || l_location_id);
        dbms_output.put_line('l_location_id - ' || l_location_id);
      
      EXCEPTION
        WHEN OTHERS THEN
          NULL;
          fnd_file.put_line(fnd_file.log
                           ,'Error deriving location_id - ' || SQLERRM);
          dbms_output.put_line('Error deriving location_id - ' || SQLERRM);
      END;
    /*
      IF rec_c_assets.tag_number IS NOT NULL
      THEN
        l_tag := 'K11_' || rec_c_assets.tag_number;
      ELSE
        l_tag := NULL;
      END IF;
    */
      INSERT INTO fa.fa_mass_additions
        (mass_addition_id
        ,asset_number
        ,tag_number
        ,description
        ,asset_category_id
        ,serial_number
        ,book_type_code
        ,date_placed_in_service
        ,fixed_assets_cost
        ,payables_units
        ,fixed_assets_units
        ,feeder_system_name
        ,create_batch_date
        ,posting_status
        ,queue_name
        ,payables_cost
        ,asset_type
        ,deprn_reserve
        ,ytd_deprn
        ,accounting_date
        ,attribute1
        ,attribute2
        ,attribute3
        ,attribute4
        ,attribute5
        ,attribute6
        ,attribute7
        ,attribute8
        ,inventorial
        ,deprn_method_code
        ,life_in_months
        ,depreciate_flag
        ,last_updated_by
        ,last_update_date
        ,created_by
        ,creation_date
        ,expense_code_combination_id
        ,invoice_number
        ,po_number
        ,location_id)
      VALUES
        (fa_mass_additions_s.nextval
        ,rec_c_assets.asset_number
        ,rec_c_assets.tag_number
        ,substr(rec_c_assets.fa_description, 1, 80)
        ,l_category_id
        ,rec_c_assets.serial_number
        ,rec_c_assets.book
        ,rec_c_assets.dps_
        ,rec_c_assets.cost
        ,l_units
        ,l_units
        ,'11.5.9'
        ,'25-Feb-2012'
        ,'POST'
        ,'POST'
        ,rec_c_assets.cost
        ,'CAPITALIZED'
        ,(case when rec_c_assets.cost < rec_c_assets.deprn_reserve
        then rec_c_assets.cost
          else rec_c_assets.deprn_reserve
            end)
        ,0
        ,'15-FEB-2012'
        ,substr(rec_c_assets.attribute1, 1, 150)
        ,substr(rec_c_assets.attribute2, 1, 150)
        ,substr(rec_c_assets.attribute3, 1, 150)
        ,substr(rec_c_assets.attribute4, 1, 150)
        ,substr(rec_c_assets.attribute5, 1, 150)
        ,substr(rec_c_assets.attribute6, 1, 150)
        ,substr(rec_c_assets.attribute7, 1, 150)
        ,substr(rec_c_assets.attribute8, 1, 150)
        ,'YES'
        ,rec_c_assets.depr_method
        ,rec_c_assets.life
        ,'YES'
        ,-1
        ,SYSDATE
        ,-1
        ,SYSDATE
        ,l_ccid
        ,rec_c_assets.invoice_number
        ,rec_c_assets.po_number
        ,l_location_id);
    
      UPDATE xxcus.xxcus_fa_stg_tbl SET processed = 'Y' WHERE ROWID = l_rownum;
    
    END LOOP;
    fnd_file.put_line(fnd_file.output
                     ,'----------------------------------------------------------------------------------------------');
    fnd_file.put_line(fnd_file.output
                     ,'----------------------------------------------------------------------------------------------');
    fnd_file.put_line(fnd_file.output
                     ,'ASSET_NUMBER        11i_COST            12g_COST            ');
    fnd_file.put_line(fnd_file.output
                     ,'----------------------------------------------------------------------------------------------');
    FOR rec_output IN (SELECT (rpad(fa.asset_number, 20, ' ') ||
                              rpad(to_number(stg.cost), 20, ' ') ||
                              rpad(to_number(fa.fixed_assets_cost), 20, ' ')) out_line
                         FROM fa.fa_mass_additions fa
                             ,(SELECT stg.asset_number, SUM(stg.cost) cost
                                 FROM xxcus.xxcus_fa_stg_tbl stg
                                GROUP BY stg.asset_number) stg
                        WHERE 1 = 1
                          AND stg.asset_number = fa.asset_number
                          AND fa.book_type_code = 'HDS WHITE CAP'
                        ORDER BY fa.asset_number)
    LOOP
    
      fnd_file.put_line(fnd_file.output, rec_output.out_line);
    
    END LOOP;
  
    fnd_file.put_line(fnd_file.output
                     ,'----------------------------------------------------------------------------------------------');
    fnd_file.put_line(fnd_file.output
                     ,'----------------------------------------------------------------------------------------------');
  
    fnd_file.put_line(fnd_file.log, ' End of procedure load_fa');
    fnd_file.put_line(fnd_file.log
                     ,'----------------------------------------------------------------------------------------------');
    fnd_file.put_line(fnd_file.log
                     ,'----------------------------------------------------------------------------------------------');
  
  EXCEPTION
    WHEN OTHERS THEN
      NULL;
      fnd_file.put_line(fnd_file.log
                       ,'Error in the Procedure - load_fa - ' || SQLERRM);
  END load_mass_add; 

  ---------------------------------------------------------------------------------
  -- Add Fixed Assets Assignments into Oracle   3-1-2012  Kathy Poling
  ---------------------------------------------------------------------------------
  PROCEDURE add_assignments(p_errbuf OUT VARCHAR2, p_retcode OUT NUMBER) AS
  
    l_trans_rec                      fa_api_types.trans_rec_type;
    l_asset_hdr_rec                  fa_api_types.asset_hdr_rec_type;
    l_asset_dist_tbl                 fa_api_types.asset_dist_tbl_type;
    l_asset_dist_empty_tbl           fa_api_types.asset_dist_tbl_type;
    l_asset_dist_rec                 fa_api_types.asset_dist_rec_type;
    l_return_status  VARCHAR2(1);
    l_mesg_count     NUMBER;
    l_mesg           VARCHAR2(512);
  
    l_ccid        NUMBER;
    l_location_id NUMBER;
    l_rec         NUMBER;
    l_count       NUMBER;
  
    CURSOR c_assign IS
      SELECT stg.rowid
             ,stg.asset_number "ASSET_NUMBER"
             ,asset_id
             ,prod
             ,location
             ,to_number(units_assigned) units_assigned
             ,home_branch
             ,custodian_branch
             ,legal_entity
             ,state
             ,city
             ,cost_center
             ,expense_account
             ,project_code
        FROM xxcus.xxcus_fa_stg_tbl stg, fa_additions fa
       WHERE stg.asset_number = fa.asset_number
         AND stg.processed IS NULL
       ORDER BY stg.asset_number;
  
  BEGIN
  
    fnd_file.put_line(fnd_file.log
                     ,'----------------------------------------------------------------------------------------------');
    fnd_file.put_line(fnd_file.log
                     ,'----------------------------------------------------------------------------------------------');
    fnd_file.put_line(fnd_file.log, ' Start of procedure - add_assignments');
  
    --dbms_output.enable(1000000);
    fa_srvr_msg.init_server_message;
    fa_debug_pkg.initialize;    
    fnd_profile.put('PRINT_DEBUG', 'Y');
  
    FOR rec_c_assign IN c_assign
    LOOP
    
      fnd_file.put_line(fnd_file.log
                       ,'----------------------------------------------------------------------------------------------');
      fnd_file.put_line(fnd_file.log
                       ,'AssetNumber - ' || rec_c_assign.asset_number);
    
      --dbms_output.put_line('AssetNumber - ' || rec_c_assign.asset_number);
    
      l_ccid                             := NULL;
      --l_trans_rec     := NULL;
      --l_asset_hdr_rec := NULL;
      l_location_id                      := NULL;
      l_trans_rec.transaction_header_id  := NULL;
      --l_asset_hdr_rec.asset_id           := NULL;
      l_asset_hdr_rec.book_type_code     := 'HDS WHITE CAP';
      l_asset_dist_tbl                   := l_asset_dist_empty_tbl;
    
      -- Derive assignment ccid
      BEGIN
        SELECT code_combination_id
          INTO l_ccid
          FROM gl_code_combinations
         WHERE segment1 = rec_c_assign.prod
           AND segment2 = rec_c_assign.location
           AND segment3 = rec_c_assign.cost_center
           AND segment4 = rec_c_assign.expense_account
           AND segment5 = rec_c_assign.project_code
           AND segment6 = '00000'
           AND segment7 = '00000';
      
        fnd_file.put_line(fnd_file.log, 'l_ccid - ' || l_ccid);
        --dbms_output.put_line('l_ccid - ' || l_ccid);
      
      EXCEPTION
        WHEN OTHERS THEN
          NULL;
          fnd_file.put_line(fnd_file.log
                           ,'Error deriving Expense CCID - ' || SQLERRM);
      END;
    
      BEGIN
        SELECT fl.location_id
          INTO l_location_id
          FROM fa_locations fl
         WHERE upper(fl.segment1) = upper(rec_c_assign.home_branch)
           AND upper(fl.segment2) = upper(rec_c_assign.custodian_branch)
           AND upper(fl.segment3) =
               upper(decode(rec_c_assign.legal_entity
                           ,'HUGHES CANADA, INC.'
                           ,'HDS Canada, Inc'
                           ,'HUGHES ELECTRIC SUPPLY LTD'
                           ,'HDS Electric Supply LTD'
                           ,'HUGHES MRO LTD'
                           ,'HDS MRO LTD'
                           ,'HUGHES SUPPLY (VA) INC'
                           ,'HD Supply (VA) INC'
                           ,'HUGHES SUPPLY INC'
                           ,'HD Supply Inc'
                           ,'HUGHES SUPPLY SHRD SRVCS INC'
                           ,'HD Supply SHRD SRVCS INC'
                           ,'HUGHES UTILITIES LTD'
                           ,'HD Supply Utilities, LTD'
                           ,'UTILITIES PRODUCTS SUPP CO LLC'
                           ,'Utilities Products Supply LLC'
                           ,'HUGHES PLUMBING SUPPLY LTD'
                           ,'HDS Electric Supply LTD'
                           ,'HUGHES BUILDING MATERIALS LTD'
                           ,'HDS Building Materials LTD'
                           ,upper(rec_c_assign.legal_entity)))
           AND upper(fl.segment5) = upper(rec_c_assign.city)
           AND upper(fl.segment4) = upper(rec_c_assign.state);
      
        fnd_file.put_line(fnd_file.log, 'l_location_id - ' || l_location_id);
      
        --dbms_output.put_line('l_location_id - ' || l_location_id);
      
      EXCEPTION
        WHEN OTHERS THEN
          NULL;
          fnd_file.put_line(fnd_file.log
                           ,'Error deriving location_id - ' || SQLERRM);
      END;
    
      BEGIN
        SELECT COUNT(*)
          INTO l_count
          FROM xxcus.xxcus_fa_stg_tbl
         WHERE processed = 'Y'
           AND asset_number = rec_c_assign.asset_number;
      
        --dbms_output.put_line('Row number - ' || l_count);
      EXCEPTION
        WHEN OTHERS THEN
          NULL;
          fnd_file.put_line(fnd_file.log, 'Error record number - ' || SQLERRM);
      END;
      BEGIN
        -- fill in asset information
        l_asset_hdr_rec.asset_id := rec_c_assign.asset_id;
        --dbms_output.put_line('Asset ID - ' || rec_c_assign.asset_id);
      
        IF l_count = 1
        THEN
          l_asset_dist_tbl(2).transaction_units := rec_c_assign.units_assigned;
          l_asset_dist_tbl(2).assigned_to := NULL;
          l_asset_dist_tbl(2).expense_ccid := l_ccid;
          l_asset_dist_tbl(2).location_ccid := l_location_id;
        
          --dbms_output.put_line('Units - '||rec_c_assign.units_assigned || '  Exp CCID - '||l_ccid
          --                            ||'  Location ID ' || l_location_id);
        
        END IF;
      
        IF l_count = 2
        THEN
          l_asset_dist_tbl(3).transaction_units := rec_c_assign.units_assigned;
          l_asset_dist_tbl(3).assigned_to := NULL;
          l_asset_dist_tbl(3).expense_ccid := l_ccid;
          l_asset_dist_tbl(3).location_ccid := l_location_id;
        
          --dbms_output.put_line('Units - '||rec_c_assign.units_assigned || '  Exp CCID - '||l_ccid
          --                            ||'  Location ID ' || l_location_id);      
        
        END IF;
      
        IF l_count = 3
        THEN
          l_asset_dist_tbl(4).transaction_units := rec_c_assign.units_assigned;
          l_asset_dist_tbl(4).assigned_to := NULL;
          l_asset_dist_tbl(4).expense_ccid := l_ccid;
          l_asset_dist_tbl(4).location_ccid := l_location_id;
        
          --dbms_output.put_line('Units - '||rec_c_assign.units_assigned || '  Exp CCID - '||l_ccid
          --                           ||'  Location ID ' || l_location_id);
        
        END IF;
      
        IF l_count = 4
        THEN
          l_asset_dist_tbl(5).transaction_units := rec_c_assign.units_assigned;
          l_asset_dist_tbl(5).assigned_to := NULL;
          l_asset_dist_tbl(5).expense_ccid := l_ccid;
          l_asset_dist_tbl(5).location_ccid := l_location_id;
        
        END IF;
      
        IF l_count = 5
        THEN
          l_asset_dist_tbl(6).transaction_units := rec_c_assign.units_assigned;
          l_asset_dist_tbl(6).assigned_to := NULL;
          l_asset_dist_tbl(6).expense_ccid := l_ccid;
          l_asset_dist_tbl(6).location_ccid := l_location_id;
        
        END IF;
      
        IF l_count = 6
        THEN
          l_asset_dist_tbl(7).transaction_units := rec_c_assign.units_assigned;
          l_asset_dist_tbl(7).assigned_to := NULL;
          l_asset_dist_tbl(7).expense_ccid := l_ccid;
          l_asset_dist_tbl(7).location_ccid := l_location_id;
        
        END IF;
      
        IF l_count = 7
        THEN
          l_asset_dist_tbl(8).transaction_units := rec_c_assign.units_assigned;
          l_asset_dist_tbl(8).assigned_to := NULL;
          l_asset_dist_tbl(8).expense_ccid := l_ccid;
          l_asset_dist_tbl(8).location_ccid := l_location_id;
        
        END IF;
      
        IF l_count = 8
        THEN
          l_asset_dist_tbl(9).transaction_units := rec_c_assign.units_assigned;
          l_asset_dist_tbl(9).assigned_to := NULL;
          l_asset_dist_tbl(9).expense_ccid := l_ccid;
          l_asset_dist_tbl(9).location_ccid := l_location_id;
        
        END IF;
      
        IF l_count = 9
        THEN
          l_asset_dist_tbl(10).transaction_units := rec_c_assign.units_assigned;
          l_asset_dist_tbl(10).assigned_to := NULL;
          l_asset_dist_tbl(10).expense_ccid := l_ccid;
          l_asset_dist_tbl(10).location_ccid := l_location_id;
        
        END IF;
      
        IF l_count = 10
        THEN
          l_asset_dist_tbl(11).transaction_units := rec_c_assign.units_assigned;
          l_asset_dist_tbl(11).assigned_to := NULL;
          l_asset_dist_tbl(11).expense_ccid := l_ccid;
          l_asset_dist_tbl(11).location_ccid := l_location_id;
        
        END IF;
      
        IF l_count = 11
        THEN
          l_asset_dist_tbl(12).transaction_units := rec_c_assign.units_assigned;
          l_asset_dist_tbl(12).assigned_to := NULL;
          l_asset_dist_tbl(12).expense_ccid := l_ccid;
          l_asset_dist_tbl(12).location_ccid := l_location_id;
        
        END IF;
      
        -- create assignment    
        fa_unit_adj_pub.do_unit_adjustment(
                                           -- std parameters
                                           p_api_version      => 1.0
                                          ,p_init_msg_list    => fnd_api.g_false
                                          ,p_commit           => fnd_api.g_false
                                          ,p_validation_level => fnd_api.g_valid_level_full
                                          ,p_calling_fn       => NULL
                                          ,x_return_status    => l_return_status
                                          ,x_msg_count        => l_mesg_count
                                          ,x_msg_data         => l_mesg
                                          ,
                                           -- api parameters
                                           px_trans_rec      => l_trans_rec
                                          ,px_asset_hdr_rec  => l_asset_hdr_rec
                                          ,px_asset_dist_tbl => l_asset_dist_tbl);
        --dump messages
        l_mesg_count := fnd_msg_pub.count_msg;
        IF l_mesg_count > 0
        THEN
          l_mesg := chr(10) ||
                    substr(fnd_msg_pub.get(fnd_msg_pub.g_first, fnd_api.g_false)
                          ,1
                          ,250);
          --dbms_output.put_line(l_mesg);
          fnd_file.put_line(fnd_file.log, l_mesg);
        
          FOR i IN 1 .. (l_mesg_count - 1)
          LOOP
            l_mesg := substr(fnd_msg_pub.get(fnd_msg_pub.g_next
                                            ,fnd_api.g_false)
                            ,1
                            ,250);
            --dbms_output.put_line(l_mesg);
            fnd_file.put_line(fnd_file.log, l_mesg);
          END LOOP;
          fnd_msg_pub.delete_msg();
        END IF;
        IF (l_return_status <> fnd_api.g_ret_sts_success)
        THEN
          --dbms_output.put_line('FAILURE');
          fnd_file.put_line(fnd_file.log, 'FAILURE');
        ELSE
          --dbms_output.put_line('SUCCESS');
          --dbms_output.put_line('THID' ||
          --                     to_char(l_trans_rec.transaction_header_id));
          fnd_file.put_line(fnd_file.log, 'SUCCESS');
          fnd_file.put_line(fnd_file.log
                           ,'THID' ||
                            to_char(l_trans_rec.transaction_header_id));
        
          UPDATE xxcus.xxcus_fa_stg_tbl
             SET processed = 'Y'
           WHERE ROWID = rec_c_assign.rowid;
        
        END IF;
      
      END;
    END LOOP;
  END add_assignments;   

END xxcus_fixed_asset_conv_pkg;
/
