/* Formatted on 5/14/2014 11:38:36 AM (QP5 v5.254.13202.43190) */
CREATE OR REPLACE PACKAGE BODY APPS.xxwc_ego_uda_mass_upload_pkg
/********************************************************************************
File Name:    XXWC_EGO_IMAGE_MGMT_PKG
PROGRAM TYPE: PL/SQL Package body
PURPOSE:      Procedure for validating and processing UDAs
==================================================================================
VERSION DATE          AUTHOR(S)               DESCRIPTION
------- -----------   --------------- --------------------------------------------
1.0     18-Apr-2014   Praveen Pawar           Initial creation of the package body
1.1     06-May-2014   Praveen Pawar           Added a new parameter file name
*********************************************************************************/
AS
   PROCEDURE PROCESS_UDAS (errbuf            OUT NOCOPY VARCHAR2,
                           retcode           OUT NOCOPY VARCHAR2,
                           pv_file_name   IN            VARCHAR2)
   IS
      ln_count                    NUMBER := NULL;
      ln_txn_id                   NUMBER := NULL;
      ln_batch_id                 NUMBER := NULL;
      l_tbl_rec_cnt               NUMBER := 0;
      ln_first_batch_id           NUMBER := NULL;
      ln_last_batch_id            NUMBER := NULL;
      ln_attr_rec_cnt             NUMBER := NULL;
      ln_request_id               NUMBER := 0;
      ln_error_count              NUMBER := 0;
      ln_total_count              NUMBER := 0;
      ln_row_identifier           NUMBER := 0;
      ln_previous_attr_grp_id     NUMBER := NULL;
      ln_previous_inv_item_id     NUMBER := NULL;
      ln_cnt                      NUMBER := NULL;
      ln_error_cnt                NUMBER := NULL;

      lv_err_msg                  VARCHAR2 (4000) := NULL;
      lv_error_msg                VARCHAR2 (4000) := NULL;

      ln_resp_id                  fnd_responsibility.responsibility_id%TYPE;
      ln_app_id                   fnd_responsibility.application_id%TYPE;

      cv_dist_list                VARCHAR2 (100)
                                     := 'HDSOracleDevelopers@hdsupply.com';
      cv_ego_itemmgmt_group       VARCHAR2 (20) := 'EGO_ITEMMGMT_GROUP';
      cv_ego_pim_data_librarian   VARCHAR2 (25) := 'EGO_PIM_DATA_LIBRARIAN';

      ----------------------------------------------------------------------------
      --                                                             wait for request variable devlaration                                                                         --
      ----------------------------------------------------------------------------

      cn_intvl                    NUMBER := 5;
      cn_max_wait                 NUMBER := 120000;
      lv_phase                    VARCHAR2 (250) := NULL;
      lv_status                   VARCHAR2 (250) := NULL;
      lv_dev_phase                VARCHAR2 (250) := NULL;
      lv_dev_status               VARCHAR2 (250) := NULL;
      lv_msg                      VARCHAR2 (250) := NULL;
      lb_request_status           BOOLEAN;

      ln_req_id                   NUMBER := NULL;
      l_phase                     VARCHAR2 (150) := NULL;
      l_status                    VARCHAR2 (150) := NULL;
      l_dev_phase                 VARCHAR2 (150) := NULL;
      l_dev_status                VARCHAR2 (150) := NULL;
      l_message                   VARCHAR2 (4000) := NULL;
      V_DAT_FILE                  VARCHAR2 (150) := NULL;
      V_DAT_FILE_PATH             VARCHAR2 (150) := NULL;
      V_CTL_FILE                  VARCHAR2 (150) := NULL;
      V_CTL_FILE_PATH             VARCHAR2 (150) := NULL;
      V_LOG_FILE_PATH             VARCHAR2 (150) := NULL;
      l_get_request_status        BOOLEAN;


      TYPE request_id_tbl IS TABLE OF NUMBER
         INDEX BY BINARY_INTEGER;

      request_id_list             request_id_tbl;

      -- Cursor to fetch all the NEW records for data validaton --
      CURSOR cur_validate_item_details
      IS
           SELECT item_number,
                  organization_code,
                  item_catalog_category,
                  attribute_group_internal_name,
                  attribute_internal_name,
                  ROWID
             FROM xxwc.xxwc_ego_uda_stage_tab
            WHERE status = 'NEW'
         ORDER BY inventory_item_id, organization_id, attribute_group_id;

      -- Cursor to fetch all the VALID records for processing --
      CURSOR cur_item_detail
      IS
           SELECT inventory_item_id,
                  organization_id,
                  item_catalog_group_id,
                  attribute_group_id,
                  attribute_group_internal_name,
                  attribute_internal_name,
                  DECODE (data_type_code,
                          'C', attribute_value,
                          'A', attribute_value)
                     attr_value_char,
                  DECODE (data_type_code, 'N', attribute_value) attr_value_num,
                  DECODE (data_type_code,
                          'Y', attribute_value,
                          'X', attribute_value)
                     attr_value_date,
                  attribute_value,
                  data_type_code,
                  ROWID
             FROM xxwc.xxwc_ego_uda_stage_tab
            WHERE status = 'VALID'
         ORDER BY inventory_item_id, organization_id, attribute_group_id;

      -- Cursor to fetch data set Id from UDA interface table --
      CURSOR cur_distinct_data_set_id
      IS
         SELECT DISTINCT data_set_id
           FROM ego_itm_usr_attr_intrfc
          WHERE     process_status = 1                   --Unprocessed records
                AND data_set_id BETWEEN ln_first_batch_id
                                    AND ln_last_batch_id;
   BEGIN
      errbuf := NULL;
      retcode := 0;
      ln_count := NULL;

      BEGIN
         SELECT pv_file_name DAT_FILE,
                attribute2 DAT_FILE_PATH,
                attribute3 CTL_FILE,
                attribute4 CTL_FILE_PATH,
                attribute5 LOG_FILE_PATH
           INTO V_DAT_FILE,
                V_DAT_FILE_PATH,
                V_CTL_FILE,
                V_CTL_FILE_PATH,
                V_LOG_FILE_PATH
           FROM fnd_lookup_values
          WHERE     lookup_type = 'XXWC_LOADER_COMMON_LOOKUP'
                AND LANGUAGE = USERENV ('LANG')
                AND MEANING =
                       (SELECT CONCURRENT_PROGRAM_NAME
                          FROM FND_CONCURRENT_PROGRAMS
                         WHERE CONCURRENT_PROGRAM_ID =
                                  FND_GLOBAL.CONC_PROGRAM_ID)
                AND ENABLED_FLAG = 'Y'
                AND TRUNC (SYSDATE) BETWEEN TRUNC (START_DATE_ACTIVE)
                                        AND TRUNC (
                                               NVL (END_DATE_ACTIVE, SYSDATE));
      EXCEPTION
         WHEN OTHERS
         THEN
            fnd_file.put_line (fnd_file.LOG,
                               'In Others Of Common Lookup..' || SQLERRM);
      --v_errordetails := 'In Others Of Common Lookup..' || SQLERRM;
      END;

      BEGIN
         ln_req_id :=
            fnd_request.submit_request (application   => 'XXWC',
                                        program       => 'XXWCGLP',
                                        description   => NULL,
                                        start_time    => SYSDATE,
                                        sub_request   => FALSE,
                                        argument1     => v_dat_file,
                                        argument2     => v_dat_file_path,
                                        argument3     => v_ctl_file,
                                        argument4     => v_ctl_file_path,
                                        argument5     => v_log_file_path);
         COMMIT;



         fnd_file.put_line (fnd_file.LOG,
                            'Request for batch submit..' || ln_req_id);

         IF ln_req_id > 0
         THEN
            fnd_file.put_line (
               fnd_file.LOG,
               'In Submitting the XXWC Genric Loader Program');
         END IF;

         l_get_request_status :=
            FND_CONCURRENT.WAIT_FOR_REQUEST (phase        => l_phase,
                                             request_id   => ln_req_id,
                                             INTERVAL     => 10,
                                             max_wait     => 0,
                                             STATUS       => l_status,
                                             dev_phase    => l_dev_phase,
                                             dev_status   => l_dev_status,
                                             MESSAGE      => l_message);
         fnd_file.put_line (fnd_file.LOG, 'Program Phase :' || l_dev_phase);
         fnd_file.put_line (fnd_file.LOG, 'Program Status :' || l_dev_status);
      EXCEPTION
         WHEN OTHERS
         THEN
            FND_FILE.PUT_LINE (FND_FILE.LOG,
                               'In Others Of wait for request...' || SQLERRM);
      END;


      BEGIN
         SELECT ROUND ( (COUNT (1) / 50000) + 1)
           INTO ln_count
           FROM xxwc.xxwc_ego_uda_stage_tab
          WHERE LAST_UPDATE_DATE < (SYSDATE - 14);

         FOR I IN 1 .. ln_count
         LOOP
            -- Delete data which is 2 weeks old --
            DELETE FROM xxwc.xxwc_ego_uda_stage_tab
                  WHERE LAST_UPDATE_DATE < (SYSDATE - 14) AND ROWNUM < 50001;

            COMMIT;
         END LOOP;
      EXCEPTION
         WHEN OTHERS
         THEN
            fnd_file.put_line (FND_FILE.LOG,
                               'Error while deleting old processed data.');
            fnd_file.put_line (FND_FILE.LOG, 'Error Message: ' || SQLERRM);
            retcode := 1;
      END;

      FOR rec_validate_item_details IN cur_validate_item_details
      LOOP
         ln_cnt := 0;
         ln_error_cnt := 0;
         lv_error_msg := NULL;

         BEGIN
            -- Check if item exists? --
            SELECT COUNT (1)
              INTO ln_cnt
              FROM MTL_SYSTEM_ITEMS_B
             WHERE     segment1 = rec_validate_item_details.item_number
                   AND organization_id =
                          TO_NUMBER (
                             APPS.fnd_profile.VALUE ('XXWC_ITEM_MASTER_ORG'));
         EXCEPTION
            WHEN OTHERS
            THEN
               ln_error_cnt := ln_error_cnt + 1;
               lv_error_msg :=
                     'Error while checking if item number exists for SKU: '
                  || rec_validate_item_details.item_number
                  || '. Error Message: '
                  || SQLERRM;
               ln_cnt := NULL;
         END;

         IF ln_cnt = 0
         THEN
            ln_error_cnt := ln_error_cnt + 1;
            lv_error_msg :=
                  ln_error_cnt
               || ')Item Number: '
               || rec_validate_item_details.item_number
               || ' does not exist in PDH. ';
         END IF;

         ln_cnt := 0;

         BEGIN
            -- Check if org exists? --
            SELECT COUNT (1)
              INTO ln_cnt
              FROM MTL_PARAMETERS
             WHERE organization_code =
                      rec_validate_item_details.organization_code;
         EXCEPTION
            WHEN OTHERS
            THEN
               ln_error_cnt := ln_error_cnt + 1;
               lv_error_msg :=
                     lv_error_msg
                  || 'Error while checking if organization code exists. Org Code: '
                  || rec_validate_item_details.organization_code
                  || '. Error Message: '
                  || SQLERRM
                  || ' ';
               ln_cnt := NULL;
         END;

         IF ln_cnt = 0
         THEN
            ln_error_cnt := ln_error_cnt + 1;
            lv_error_msg :=
                  lv_error_msg
               || ln_error_cnt
               || ')Organization Code: '
               || rec_validate_item_details.organization_code
               || ' does not exist in PDH. ';
         END IF;

         ln_cnt := 0;

         BEGIN
            -- Check if attribute group exists? --
            SELECT COUNT (1)
              INTO ln_cnt
              FROM EGO_ATTR_GROUPS_V
             WHERE attr_group_name =
                      rec_validate_item_details.attribute_group_internal_name;
         EXCEPTION
            WHEN OTHERS
            THEN
               ln_error_cnt := ln_error_cnt + 1;
               lv_error_msg :=
                     lv_error_msg
                  || 'Error while checking if attribute group exists. Attribute Group: '
                  || rec_validate_item_details.attribute_group_internal_name
                  || '. Error Message: '
                  || SQLERRM
                  || ' ';
               ln_cnt := NULL;
         END;

         IF ln_cnt = 0
         THEN
            ln_error_cnt := ln_error_cnt + 1;
            lv_error_msg :=
                  lv_error_msg
               || ln_error_cnt
               || ')Attribute Group: '
               || rec_validate_item_details.attribute_group_internal_name
               || ' does not exist in PDH. ';
         ELSIF ln_cnt = 1
         THEN
            ln_cnt := 0;

            BEGIN
               -- Check if attribute exists? --
               SELECT COUNT (1)
                 INTO ln_cnt
                 FROM EGO_ATTRS_V
                WHERE     attr_name =
                             rec_validate_item_details.attribute_internal_name
                      AND attr_group_name =
                             rec_validate_item_details.attribute_group_internal_name;
            EXCEPTION
               WHEN OTHERS
               THEN
                  ln_error_cnt := ln_error_cnt + 1;
                  lv_error_msg :=
                        lv_error_msg
                     || 'Error while checking if attribute exists. Attribute: '
                     || rec_validate_item_details.attribute_internal_name
                     || '. Error Message: '
                     || SQLERRM;
                  ln_cnt := NULL;
            END;

            IF ln_cnt = 0
            THEN
               ln_error_cnt := ln_error_cnt + 1;
               lv_error_msg :=
                     lv_error_msg
                  || ln_error_cnt
                  || ')Attribute: '
                  || rec_validate_item_details.attribute_internal_name
                  || ' does not exist in PDH.';
            END IF;
         END IF;

         IF ln_error_cnt > 0
         THEN
            -- In case of error update the record with status as Invalid --
            UPDATE xxwc.xxwc_ego_uda_stage_tab
               SET status = 'INVALID', error_msg = lv_error_msg
             WHERE status = 'NEW' AND ROWID = rec_validate_item_details.ROWID;

            fnd_file.put_line (
               FND_FILE.LOG,
               'Data validation failed. Error Message: ' || lv_error_msg);
            retcode := 1;
         ELSIF ln_error_cnt = 0
         THEN
            -- In case of no error update the record with status as Validated --
            UPDATE xxwc.xxwc_ego_uda_stage_tab
               SET status = 'VALIDATED', error_msg = lv_error_msg
             WHERE status = 'NEW' AND ROWID = rec_validate_item_details.ROWID;
         END IF;

         COMMIT;
      END LOOP;

      ln_count := NULL;

      SELECT ROUND ( (COUNT (1) / 50000) + 1)
        INTO ln_count
        FROM xxwc.xxwc_ego_uda_stage_tab
       WHERE STATUS = 'VALIDATED';

      FOR I IN 1 .. ln_count
      LOOP
         BEGIN
            -- Update addtional info and mark record from VALIDATED to VALID status --
            UPDATE XXWC.XXWC_EGO_UDA_STAGE_TAB XSU
               SET (XSU.INVENTORY_ITEM_ID,
                    XSU.ORGANIZATION_ID,
                    XSU.ITEM_CATALOG_GROUP_ID,
                    XSU.ATTRIBUTE_GROUP_ID,
                    XSU.ATTRIBUTE_VALUE,
                    XSU.DATA_TYPE_CODE,
                    XSU.STATUS,
                    XSU.LAST_UPDATED_BY,
                    XSU.LAST_UPDATE_DATE) =
                      (SELECT MSI.INVENTORY_ITEM_ID,
                              MPI.ORGANIZATION_ID,
                              MSI.ITEM_CATALOG_GROUP_ID,
                              EAG.ATTR_GROUP_ID,
                              REPLACE (
                                 REPLACE (SUT.ATTRIBUTE_VALUE, CHR (10), ''),
                                 CHR (13),
                                 ''),
                              EAV.DATA_TYPE_CODE,
                              'VALID',
                              FND_GLOBAL.USER_ID,
                              SYSDATE
                         FROM APPS.MTL_SYSTEM_ITEMS_B MSI,
                              APPS.MTL_PARAMETERS MPI,
                              APPS.EGO_ATTR_GROUPS_V EAG,
                              APPS.EGO_ATTRS_V EAV,
                              XXWC.XXWC_EGO_UDA_STAGE_TAB SUT
                        WHERE     SUT.ITEM_NUMBER = MSI.SEGMENT1
                              AND SUT.ORGANIZATION_CODE =
                                     MPI.ORGANIZATION_CODE
                              AND MSI.ORGANIZATION_ID = MPI.ORGANIZATION_ID
                              AND SUT.ATTRIBUTE_GROUP_INTERNAL_NAME =
                                     EAG.ATTR_GROUP_NAME
                              AND SUT.ATTRIBUTE_INTERNAL_NAME = EAV.ATTR_NAME
                              AND EAG.ATTR_GROUP_NAME = EAV.ATTR_GROUP_NAME
                              AND SUT.STATUS = 'VALIDATED'
                              AND XSU.ROWID = SUT.ROWID)
             WHERE XSU.STATUS = 'VALIDATED' AND ROWNUM < 50001;

            COMMIT;
         EXCEPTION
            WHEN OTHERS
            THEN
               fnd_file.put_line (
                  FND_FILE.LOG,
                  'Error while updating additional valid info...');
               fnd_file.put_line (FND_FILE.LOG, 'Error Message: ' || SQLERRM);
               retcode := 2;
         END;
      END LOOP;

      BEGIN
         -- Fetch application Id and responsibility Id --
         SELECT application_id, responsibility_id
           INTO ln_app_id, ln_resp_id
           FROM fnd_responsibility
          WHERE responsibility_key = cv_ego_pim_data_librarian;
      EXCEPTION
         WHEN OTHERS
         THEN
            fnd_file.put_line (
               FND_FILE.LOG,
               'Error while fetching Application Id and Responsibility Id.');
            fnd_file.put_line (FND_FILE.LOG, 'Error Message: ' || SQLERRM);
            retcode := 2;
      END;

      -- Initialize the APPS environment --
      apps.fnd_global.apps_initialize (fnd_global.user_id,
                                       ln_resp_id,
                                       ln_app_id);

      ln_attr_rec_cnt := 0;
      ln_row_identifier := 0;
      ln_previous_attr_grp_id := -999;
      ln_previous_inv_item_id := -999;

      SELECT ego_iua_data_set_id_s.NEXTVAL INTO ln_batch_id FROM DUAL;

      ln_first_batch_id := ln_batch_id;

      SELECT NVL (MAX (eiuai.row_identifier), 0)
        INTO ln_row_identifier
        FROM ego_itm_usr_attr_intrfc eiuai;

      FOR rec_item_detail IN cur_item_detail
      LOOP
         IF    rec_item_detail.attribute_group_id <> ln_previous_attr_grp_id
            OR rec_item_detail.inventory_item_id <> ln_previous_inv_item_id
         THEN
            ln_row_identifier := ln_row_identifier + 1;
         END IF;

         IF     rec_item_detail.inventory_item_id <> ln_previous_inv_item_id
            AND ln_attr_rec_cnt >= 5000
         THEN
            SELECT ego_iua_data_set_id_s.NEXTVAL INTO ln_batch_id FROM DUAL;

            ln_attr_rec_cnt := 0;

            COMMIT;
         END IF;

         ln_txn_id := NULL;

         SELECT ego_iua_transaction_id_s.NEXTVAL INTO ln_txn_id FROM DUAL;

         BEGIN
            INSERT INTO ego_itm_usr_attr_intrfc (transaction_id,
                                                 process_status,
                                                 data_set_id,
                                                 organization_code,
                                                 item_number,
                                                 revision,
                                                 row_identifier,
                                                 attr_group_int_name,
                                                 attr_int_name,
                                                 attr_value_str,
                                                 attr_value_num,
                                                 attr_value_date,
                                                 attr_value_uom,
                                                 attr_uom_disp_value,
                                                 transaction_type,
                                                 organization_id,
                                                 inventory_item_id,
                                                 item_catalog_group_id,
                                                 revision_id,
                                                 attr_group_id,
                                                 request_id,
                                                 program_application_id,
                                                 program_id,
                                                 program_update_date,
                                                 created_by,
                                                 creation_date,
                                                 last_updated_by,
                                                 last_update_date,
                                                 last_update_login,
                                                 attr_group_type,
                                                 source_system_id,
                                                 source_system_reference)
                 VALUES (ln_txn_id,
                         1                           -- to be processed record
                          ,
                         ln_batch_id,
                         NULL,
                         NULL,
                         NULL,
                         ln_row_identifier,
                         rec_item_detail.attribute_group_internal_name,
                         rec_item_detail.attribute_internal_name,
                         rec_item_detail.attr_value_char,
                         rec_item_detail.attr_value_num,
                         rec_item_detail.attr_value_date,
                         NULL,
                         NULL,
                         'SYNC',
                         rec_item_detail.organization_id,
                         rec_item_detail.inventory_item_id,
                         rec_item_detail.item_catalog_group_id,
                         NULL,
                         rec_item_detail.attribute_group_id,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         fnd_global.user_id,
                         SYSDATE,
                         NULL,
                         NULL,
                         fnd_global.login_id,
                         cv_ego_itemmgmt_group,
                         NULL,
                         NULL);

            ln_attr_rec_cnt := ln_attr_rec_cnt + 1;

            UPDATE xxwc.xxwc_ego_uda_stage_tab
               SET status = 'LOAD_SUCCESS'
             WHERE ROWID = rec_item_detail.ROWID;
         EXCEPTION
            WHEN OTHERS
            THEN
               lv_err_msg := SQLERRM;

               UPDATE xxwc.xxwc_ego_uda_stage_tab
                  SET status = 'LOAD_ERROR', error_msg = lv_err_msg
                WHERE ROWID = rec_item_detail.ROWID;

               fnd_file.put_line (
                  FND_FILE.LOG,
                     'Error while inserting data into interface table. RowId: '
                  || rec_item_detail.ROWID);
               fnd_file.put_line (FND_FILE.LOG,
                                  'Error Message: ' || lv_err_msg);
               retcode := 1;
         END;

         ln_previous_inv_item_id := rec_item_detail.inventory_item_id;
         ln_previous_attr_grp_id := rec_item_detail.attribute_group_id;
      END LOOP;

      COMMIT;

      SELECT ego_iua_data_set_id_s.CURRVAL INTO ln_last_batch_id FROM DUAL;


      -----------------------------
      --Loop for each data_set_id--
      -----------------------------
      l_tbl_rec_cnt := 0;
      request_id_list.DELETE;

      FOR rec_distinct_data_set_id IN cur_distinct_data_set_id
      LOOP
         -------------------------------------------------------------
         --Submit Item/Item Revision User-Defined Attributes Program--
         -------------------------------------------------------------
         BEGIN
            --fnd_file.put_line
            --            (fnd_file.LOG,
            --                            'Run Item/Item Revision User-Defined Attributes Program for data_set_id: '
            --                || rec_distinct_data_set_id.data_set_id
            --            );
            ln_request_id :=
               fnd_request.submit_request (
                  application   => 'EGO',
                  program       => 'EGOIUAIP',
                  sub_request   => FALSE,
                  argument1     => rec_distinct_data_set_id.data_set_id,
                  argument2     => 3,                            --debug_level
                  argument3     => 'F'                   --deleting the record
                                      );
            COMMIT;                        --Commit the submit_request program

            -- get the request id for each request submitted
            l_tbl_rec_cnt := l_tbl_rec_cnt + 1;
            request_id_list (l_tbl_rec_cnt) := ln_request_id;
         --fnd_file.put_line
         --                                            (fnd_file.LOG,
         --                                                            'Submitted the request,Request Id = '
         --                                                || ln_request_id
         --                                            );

         EXCEPTION
            WHEN OTHERS
            THEN
               fnd_file.put_line (
                  fnd_file.LOG,
                  'Exception while submitting Item/Item Revision User-Defined Attributes Program');
               fnd_file.put_line (
                  fnd_file.LOG,
                  ' SQLCODE: ' || SQLCODE || ', SQLERRM: ' || SQLERRM);
               retcode := 1;
         END;

         IF ln_request_id = 0 OR ln_request_id IS NULL
         THEN
            fnd_file.put_line (
               fnd_file.LOG,
                  'Error in submitting the request,ln_request_id = '
               || ln_request_id);
            ln_error_count := ln_error_count + 1;                --Error count
         END IF;

         ln_total_count := ln_total_count + 1;                   --total count
      END LOOP;

      ----------- call wait for process for EGOIUAIP - child requests ---------------

      IF NVL (request_id_list.COUNT, 0) > 0
      THEN
         BEGIN
            FOR ln_rec_num IN request_id_list.FIRST .. request_id_list.LAST
            LOOP
               lv_phase := NULL;
               lv_status := NULL;
               lv_dev_phase := NULL;
               lv_dev_status := NULL;
               lv_msg := NULL;

               --Wait for request to complete--
               lb_request_status :=
                  fnd_concurrent.wait_for_request (
                     request_id_list (ln_rec_num),
                     cn_intvl,
                     cn_max_wait,
                     lv_phase,                                -- out parameter
                     lv_status,                               -- out parameter
                     lv_dev_phase,                            -- out parameter
                     lv_dev_status,                           -- out parameter
                     lv_msg                                   -- out parameter
                           );

               IF    UPPER (lv_dev_status) = 'WARNING'
                  OR UPPER (lv_dev_status) = 'ERROR'
               THEN
                  fnd_file.put_line (
                     fnd_file.LOG,
                        'Child request Id '
                     || request_id_list (ln_rec_num)
                     || ' completed in '
                     || lv_dev_status);
                  fnd_file.put_line (fnd_file.LOG, 'Phase: ' || lv_phase);
                  fnd_file.put_line (fnd_file.LOG,
                                     'Status: ' || lv_dev_status);
               ELSE
                  fnd_file.put_line (
                     fnd_file.LOG,
                        'Child request completed for processing UDAs. Request Id: '
                     || request_id_list (ln_rec_num));
                  fnd_file.put_line (fnd_file.LOG, 'Message: ' || lv_msg);
               END IF;
            END LOOP;
         EXCEPTION
            WHEN OTHERS
            THEN
               fnd_file.put_line (
                  fnd_file.LOG,
                     'Error while waiting for EGOIUAIP child requests to complete: '
                  || SQLERRM);
               retcode := 1;
         END;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         lv_err_msg := SQLERRM;
         fnd_file.put_line (
            fnd_file.LOG,
            'Error while processing UDAs. Error Message: ' || lv_err_msg);

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'xxwc_ego_uda_mass_upload_pkg.PROCESS_UDAS',
            p_calling             => ' validating and processing UDAs in bulk.',
            p_request_id          => FND_GLOBAL.CONC_REQUEST_ID,
            p_ora_error_msg       => lv_err_msg,
            p_error_desc          => 'WHEN OTHERS',
            p_distribution_list   => cv_dist_list,
            p_module              => 'XXWC');

         retcode := 2;
   END PROCESS_UDAS;
END xxwc_ego_uda_mass_upload_pkg;
/