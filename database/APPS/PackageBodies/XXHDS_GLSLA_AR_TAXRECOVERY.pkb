CREATE OR REPLACE PACKAGE BODY APPS.XXHDS_GLSLA_AR_TAXRECOVERY
-- ESMS TICKET 190433
-- ESMS TICKET 199523
-- Used by HDS AR Tax Reports concurrent program.
AS
  PROCEDURE print_log(p_message in varchar2) IS
  BEGIN
   IF fnd_global.conc_request_id >0 THEN
    fnd_file.put_line(fnd_file.log, p_message);
   ELSE
    dbms_output.put_line(p_message);
   END IF;
  EXCEPTION
   WHEN OTHERS THEN
    FND_FILE.PUT_LINE(FND_FILE.LOG, 'Issue in print_log routine ='||sqlerrm);
  END print_log;
  
  function get_acct_where_clause (p_account_list in varchar2) return varchar2 is
     v_accounts varchar2(240);
     i number :=1;
    cursor accts is
    select to_number(xmltbl.column_value) hds_acct
    from xmltable(p_account_list) xmltbl;
  begin
      for rec in accts loop
       if i =1 then
        v_accounts :=''''||rec.hds_acct||'''';
        i :=i+1;
       else    
        v_accounts :=v_accounts||','||''''||rec.hds_acct||'''';   
       end if;
      end loop;
   print_log ('v_accounts ='||v_accounts);
   return v_accounts;
  exception
   when others then
    print_log ('v_accounts ='||'OERR');
    return 'OERR';
  end get_acct_where_clause; 
  
  procedure get_geo_maxtaxrate 
   (
     p_report_type    in varchar2
    ,p_invoice_source in varchar2   
    ,p_invoice_id     in number   --customer_trx_id
    ,p_prism_branch   in varchar2 --prism branch number
    ,p_state          in varchar2 --customer ship to state
    ,p_cityname       in varchar2 --customer ship to city
    ,p_zipcode        in varchar2 --customer ship to postal code 
    ,p_county         in varchar2 --customer ship to county 
    ,p_geo                out varchar2
    ,p_tax                out number
    ,p_point_of_tax       out varchar2
    ,p_ship_via           out varchar2
    ,p_checkpoint_flag    out varchar2
    ,p_branch             out varchar2
    ,p_branch_city        out varchar2
    ,p_branch_state       out varchar2
    ,p_branch_zip         out varchar2 
    ,p_branch_geocode     out varchar2 
    ,p_header_id          out number
    ,p_order_number       out number      
   ) is
  cursor get_txw_info (p_state in varchar2, p_cityname in varchar2, p_zipcode in varchar2, p_county in varchar2) is
  select min(geo_code) txw_geo, combined_rate*100 max_tax_rate, STNAME
    from (
    SELECT h.stalpha as stcode, h.stname AS STNAME, s.stname AS STATE, c.cntyname AS COUNTY, l.cityname AS CITY, 
      l.zipcode AS "ZIP CODE", l.geocode AS "GEO_CODE", x.stalphacode||l.zipcode||l.geocode as "OVERRIDE GEO",
      (s.cursalerate + s.curspecrate) AS "STATE RATE", 
      (c.cursalerate + c.curspecrate) AS "COUNTY RATE", 
      (l.cursalerate + l.curspecrate) AS "LOCAL RATE",
      (s.cursalerate + s.curspecrate + c.cursalerate + 
      c.curspecrate + l.cursalerate + l.curspecrate ) AS "COMBINED_RATE"
    FROM taxsttax s
      JOIN taxware.taxcntytax c USING (stcode)
      JOIN taxlocltax l USING (stcode, cntycode)
      join taxware.stepstcd_tbl x using(stcode)
      join TAXSTINFO h using (stcode)
       WHERE  1 = 1 
        and h.stalpha    =p_state
        and cityname     =p_cityname
        and l.zipcode    =p_zipcode        
        and c.cntyname   =nvl(p_county, c.cntyname) 
        order by s.stname, c.cntyname,l.cityname,l.zipcode,l.geocode
    )
    group by combined_rate*100, stname
    order by to_number(min(geo_code));
    
    i                  number       :=1; 
    v_point_of_tax     varchar2(40) :=Null;
    v_checkpoint_flag  varchar2(1)  :='N';
    v_ship_method      varchar2(80) :=Null;
    v_branch           varchar2(4)  :=Null;
    v_state            varchar2(60) :=Null;
    v_cityname         varchar2(60) :=Null;
    v_zipcode          varchar2(20) :=Null;
    v_county           varchar2(60) :=Null;
    v_geocode          varchar2(40) :=Null;
    n_header_id        number       :=Null;
    n_order_number     number       :=Null;
    G_state            varchar2(60) :=Null;
    G_cityname         varchar2(60) :=Null;
    G_zipcode          varchar2(20) :=Null;
    G_county           varchar2(60) :=Null; 
    G_run              varchar2(1)  :='N';
    v_ct_reference     ra_customer_trx_all.ct_reference%type :=Null;           
  begin 
       -- get ship method, point of taxation, branch address info and branch geo code info. 
    If p_invoice_source IN ('PRISM', 'CONVERSION') Then
         begin  
            select 
                   case
                    when p_invoice_source ='PRISM' then d.interface_header_attribute12
                    when p_invoice_source ='CONVERSION' then null
                    else null
                   end --ship method  
                  ,lpad(d.interface_header_attribute7, 3,'0')
                  ,g.town_or_city
                  ,g.region_2
                  ,g.postal_code
                  ,to_char(null) --have not seen a record in hr_locations view with county info for branch addresses
                  ,g.loc_information13 --this is the geocode for the branch 
                  ,case
                    when upper(d.interface_header_attribute12) LIKE '%WILL%CALL%' then 'ORIGIN'
                    else 'DESTINATION' 
                   end point_of_tax
                   ,'Y' --We have retrieved either a destination based or origin based, lets just assign a flag for success
                   ,null --order_header_id
                   ,ct_reference --order_number
            into  v_ship_method
                 ,v_branch
                 ,v_cityname             
                 ,v_state
                 ,v_zipcode
                 ,v_county
                 ,v_geocode
                 ,v_point_of_tax
                 ,v_checkpoint_flag
                 ,n_header_id
                 ,v_ct_reference                   
            from  ra_customer_trx_all d          
                 --,org_organization_definitions e 
                 --,hr_all_organization_units f
                 ,hr_locations_all g                
            where 1 =1
              and d.customer_trx_id                =p_invoice_id --279777 --in (375504, 1425351) --Invoice 50000017443
              and d.interface_header_context       =CASE 
                                                     when p_invoice_source ='PRISM' then 'PRISM INVOICES'
                                                     when p_invoice_source ='CONVERSION' then 'CONVERSION'
                                                     else to_char(null)
                                                    END
              and substr(g.location_code(+), 1, 3) =CASE 
                                                     when p_invoice_source ='PRISM' then lpad(d.interface_header_attribute7, 3,'0')
                                                     when p_invoice_source ='CONVERSION' then p_prism_branch
                                                     else to_char(null)
                                                    END 
              and g.office_site_flag               ='Y';              
              --and e.organization_code(+)     =lpad(d.interface_header_attribute7, 3,'0') --bcoz sometimes branch 001 is stored as 1 
              --and f.organization_id(+)       =e.organization_id
             
            -- if we don't do the below check, we will hit an exception where the reference field has alpha characters
            -- and we are trying to assign it to a numeric variable n_order_number and then  to out variable p_order_number
             Begin 
              n_order_number :=to_number(v_ct_reference);
             Exception
              When Others Then
               print_log('reference field has a non numeric value for invoice id ='||p_invoice_id);
               n_order_number :=Null;
             End;   
              
             -- assign output variables
             p_point_of_tax       :=v_point_of_tax;
             p_checkpoint_flag    :=v_checkpoint_flag;
             p_ship_via           :=v_ship_method;  
             p_branch             :=v_branch;
             p_branch_city        :=v_cityname;
             p_branch_state       :=v_state;
             p_branch_zip         :=v_zipcode;
             p_branch_geocode     :=v_geocode;
             p_header_id          :=n_header_id;
             p_order_number       :=n_order_number; 
             p_geo                :=Null;
             p_tax                :=Null;                          
                   
         exception
            when no_data_found then
             p_point_of_tax       :='POT UNDEF';
             p_checkpoint_flag    :='N';
             p_ship_via           :=Null;  
             p_branch             :=Null;
             p_branch_city        :=Null;
             p_branch_state       :=Null;
             p_branch_zip         :=Null;
             p_branch_geocode     :=Null;
             p_geo                :='oerr1';
             p_tax                :=Null;
             p_header_id          :=Null;
             p_order_number       :=Null;                  
            when too_many_rows then
             p_point_of_tax       :='POT ERR11';
             p_checkpoint_flag    :='N';
             p_ship_via           :=Null;  
             p_branch             :=Null;
             p_branch_city        :=Null;
             p_branch_state       :=Null;
             p_branch_zip         :='oerr2';
             p_branch_geocode     :=Null;
             p_geo                :='oerr2';
             p_tax                :=Null; 
             p_header_id          :=Null;
             p_order_number       :=Null;                 
             print_log ('@get_geo_maxtaxrate, too many records in trying to find the point of tax');
            when others then
             p_point_of_tax       :='POT ERR22';
             p_checkpoint_flag    :='N';
             p_ship_via           :=Null;  
             p_branch             :=Null;
             p_branch_city        :=Null;
             p_branch_state       :=Null;
             p_branch_zip         :=Null;
             p_branch_geocode     :=Null;
             p_geo                :='oerr33';
             p_tax                :=Null;
             p_header_id          :=Null;
             p_order_number       :=Null;         
             print_log ('@get_geo_maxtaxrate trying to find the point of tax, when others '||sqlerrm);      
         end; 
         
       If p_report_type =xxhds_glsla_ar_taxrecovery.g_type_stax_recovery then --only if the report is AR Sales Tax Recovery then get tax rate
       
            If p_point_of_tax ='DESTINATION' then  
             
             If p_county ='0' Then
              G_county :=Null;
             Else
              G_county :=p_county;
             End If;
            
             G_state              :=p_state;    --customer ship to state
             G_cityname           :=p_cityname; --customer ship to city
             G_zipcode            :=p_zipcode;  --customer ship to zipcode
             --G_county             :=p_county;   --customer ship to county
             G_run                :='Y';
            Elsif p_point_of_tax ='ORIGIN' then  --tax is origin based, so pass the branch address details
             G_state              :=UPPER(v_state);     --state where branch is located 
             G_cityname           :=UPPER(v_cityname);  --city where branch is located
             G_zipcode            :=v_zipcode;   --zipcode where branch is located
             G_county             :=UPPER(v_county);    --county where branch is located, verfied this field is not available in hr_locations_all table 
             G_run                :='Y';                              
            Else
             G_run                :='N';
            End If;
            
             --get tax rate
             begin 
                  if G_run ='Y' then
                       for rec in get_txw_info(G_state, G_cityname, G_zipcode, G_county) loop 
                            if i =1 then 
                             p_geo :=rec.txw_geo;
                             p_tax :=rec.max_tax_rate;
                             i :=i+1;
                            else
                             null;
                            end if;
                                if i>1 then
                                 exit;
                                end if;
                       end loop;                             
                  else --this means p_point_of_tax is something other than ORIGIN or DESTINATION when report is sales tax recovery
                    p_geo :=Null;
                    p_tax :=Null;                               
                  end if; --end if for G_run ='Y' condition        
             exception
               when others then
                print_log ('@when looping to get maxtaxrate, other errors '||sqlerrm);   
                     p_point_of_tax       :='NA';
                     p_checkpoint_flag    :='N';
                     p_ship_via           :=Null;  
                     p_branch             :=Null;
                     p_branch_city        :=Null;
                     p_branch_state       :=Null;
                     p_branch_zip         :=Null;
                     p_branch_geocode     :=Null;
                     p_geo                :='oerr81';
                     p_tax                :=Null; 
                     p_header_id          :=Null;
                     p_order_number       :=Null;                             
             end;         
       Else --Report is other than Sales Tax Recovery, so we do not need the customer geocode and max tax rate info.       
         p_geo :=Null;
         p_tax :=Null;   
       End if;         

    Elsif p_invoice_source IN ('ORDER MANAGEMENT', 'REPAIR OM SOURCE', 'STANDARD OM SOURCE') Then
       begin 

        select a.ship_method_meaning
              ,e.organization_code
              ,g.town_or_city
              ,g.region_2
              ,g.postal_code
              ,to_char(null) --have not seen a record in hr_locations view with county info for branch addresses
              ,g.loc_information13 --this is the geocode for the branch 
              ,nvl(
                     (
                       select 'ORIGIN'
                       from   fnd_lookup_values_vl
                       where  1 =1
                         and  lookup_type ='HDS_WILLCALL_METHODS'
                         and  meaning     =a.ship_method_meaning
                     )
                   ,'DESTINATION'
                  ) point_of_tax
               ,'Y' --We have retrieved either a destination based or origin based, lets just assign a flag for success
               ,b.header_id
               ,b.order_number 
        into  v_ship_method
             ,v_branch
             ,v_cityname             
             ,v_state
             ,v_zipcode
             ,v_county
             ,v_geocode
             ,v_point_of_tax
             ,v_checkpoint_flag
             ,n_header_id
             ,n_order_number
        from  ra_customer_trx_all d 
             ,oe_order_headers b
             ,wsh_carrier_services a         
             ,org_organization_definitions e 
             ,hr_all_organization_units f
             ,hr_locations_all g                
        where 1 =1
          and d.customer_trx_id          =p_invoice_id --in (375504, 1425351) --Invoice 50000017443
          and d.interface_header_context ='ORDER ENTRY'
          and to_char(b.order_number)    =d.interface_header_attribute1 --use invoice header flexfield attribute1 which is order_number      
          and a.ship_method_code         =b.shipping_method_code
          and e.organization_id          =b.ship_from_org_id
          and f.organization_id          =e.organization_id
          and g.location_id              =f.location_id;
          
         -- assign output variables
         p_point_of_tax       :=v_point_of_tax;
         p_checkpoint_flag    :=v_checkpoint_flag;
         p_ship_via           :=v_ship_method;  
         p_branch             :=v_branch;
         p_branch_city        :=v_cityname;
         p_branch_state       :=v_state;
         p_branch_zip         :=v_zipcode;
         p_branch_geocode     :=v_geocode;
         p_header_id          :=n_header_id;
         p_order_number       :=n_order_number; 
               
       exception
        when no_data_found then
         p_point_of_tax       :='POT UNKNOWN';
         p_checkpoint_flag    :='N';
         p_ship_via           :=Null;  
         p_branch             :=Null;
         p_branch_city        :=Null;
         p_branch_state       :=Null;
         p_branch_zip         :=Null;
         p_branch_geocode     :=Null;
         p_geo                :='oerr1';
         p_tax                :=Null;
         p_header_id          :=Null;
         p_order_number       :=Null;                  
        when too_many_rows then
         p_point_of_tax       :='POT ERR1';
         p_checkpoint_flag    :='N';
         p_ship_via           :=Null;  
         p_branch             :=Null;
         p_branch_city        :=Null;
         p_branch_state       :=Null;
         p_branch_zip         :=Null;
         p_branch_geocode     :=Null;
         p_geo                :='oerr2';
         p_tax                :=Null; 
         p_header_id          :=Null;
         p_order_number       :=Null;                 
         print_log ('@get_geo_maxtaxrate, too many records in trying to find the point of tax');
        when others then
         p_point_of_tax       :='POT ERR2';
         p_checkpoint_flag    :='N';
         p_ship_via           :=Null;  
         p_branch             :=Null;
         p_branch_city        :=Null;
         p_branch_state       :=Null;
         p_branch_zip         :=Null;
         p_branch_geocode     :=Null;
         p_geo                :='oerr3';
         p_tax                :=Null;
         p_header_id          :=Null;
         p_order_number       :=Null;         
         print_log ('@get_geo_maxtaxrate trying to find the point of tax, when others '||sqlerrm);     
       end;  
  
       If p_report_type =xxhds_glsla_ar_taxrecovery.g_type_stax_recovery then --only if the report is AR Sales Tax Recovery then get tax rate
       
            If p_point_of_tax ='DESTINATION' then
             
             If p_county ='0' Then
              G_county :=Null;
             Else
              G_county :=p_county;
             End If;
                         
             G_state              :=p_state;    --customer ship to state
             G_cityname           :=p_cityname; --customer ship to city
             G_zipcode            :=p_zipcode;  --customer ship to zipcode
             --G_county             :=p_county;   --customer ship to county
             G_run                :='Y';
            Elsif p_point_of_tax ='ORIGIN' then  --tax is origin based, so pass the branch address details
             G_state              :=UPPER(v_state);     --state where branch is located 
             G_cityname           :=UPPER(v_cityname);  --city where branch is located
             G_zipcode            :=v_zipcode;   --zipcode where branch is located
             G_county             :=UPPER(v_county);    --county where branch is located, verfied this field is not available in hr_locations_all table 
             G_run                :='Y';                              
            Else
             G_run                :='N';
            End If;
            
             --get tax rate
             begin 
                  if G_run ='Y' then
                       for rec in get_txw_info(G_state, G_cityname, G_zipcode, G_county) loop 
                            if i =1 then 
                             p_geo :=rec.txw_geo;
                             p_tax :=rec.max_tax_rate;
                             i :=i+1;
                            else
                             null;
                            end if;
                                if i>1 then
                                 exit;
                                end if;
                       end loop;                             
                  else --this means p_point_of_tax is something other than ORIGIN or DESTINATION when report is sales tax recovery
                    p_geo :=Null;
                    p_tax :=Null;                               
                  end if; --end if for G_run ='Y' condition        
             exception
               when others then
                print_log ('@when looping to get maxtaxrate, other errors '||sqlerrm);   
                     p_point_of_tax       :='NA';
                     p_checkpoint_flag    :='N';
                     p_ship_via           :=Null;  
                     p_branch             :=Null;
                     p_branch_city        :=Null;
                     p_branch_state       :=Null;
                     p_branch_zip         :=Null;
                     p_branch_geocode     :=Null;
                     p_geo                :='oerr8';
                     p_tax                :=Null; 
                     p_header_id          :=Null;
                     p_order_number       :=Null;                             
             end;         
       Else --Report is other than Sales Tax Recovery, so we do not need the customer geocode and max tax rate info.       
         p_geo :=Null;
         p_tax :=Null;   
       End if;    
    Else
             p_point_of_tax       :=Null;
             p_checkpoint_flag    :=Null;
             p_ship_via           :=Null;  
             p_branch             :=Null;
             p_branch_city        :=Null;
             p_branch_state       :=Null;
             p_branch_zip         :=Null;
             p_branch_geocode     :=Null;
             p_geo                :=Null;
             p_tax                :=Null;
             p_header_id          :=Null;
             p_order_number       :=Null;
    End If;   
  exception
   when others then
    print_log ('@get_geo_maxtaxrate when others '||sqlerrm);   
         p_point_of_tax       :='NA';
         p_checkpoint_flag    :='N';
         p_ship_via           :=Null;  
         p_branch             :=Null;
         p_branch_city        :=Null;
         p_branch_state       :=Null;
         p_branch_zip         :=Null;
         p_branch_geocode     :=Null;
         p_geo                :='oerr9';
         p_tax                :=Null;
         p_header_id          :=Null;
         p_order_number       :=Null;               
  end get_geo_maxtaxrate ;  

  PROCEDURE main (
    retcode               out number
   ,errbuf                out varchar2
   ,p_ledger_id           in  number
   ,p_period_name         in  varchar2
   ,p_product             in  varchar2   
   ,p_list_of_accounts    in  varchar2
   ,p_report_type         in  varchar2   
  ) IS


  CURSOR gl_sla_ar IS 
  SELECT  branch
         ,gl_account_string
         ,customer_number
         ,customer_name
         ,adj_number
         ,wrtoff_date
         ,invoice_source
         ,customer_trx_id
         ,invoice_number
         ,invoice_date
         ,wrtoff_amount
         ,total_freight
         ,taxable_amount
         ,total_tax
         ,invoice_total
         ,geocode_taxware_rate
         ,ship_address1
         ,ship_address2
         ,ship_address3
         ,ship_city
         ,ship_state
         ,ship_zipcode
         ,ship_county
         ,geocode
         ,ship_via
         ,order_header_id
         ,order_number         
         ,point_of_taxation
         ,branch_city
         ,branch_state
         ,branch_postal_code
         ,branch_geocode         
         ,request_id
         ,je_source
         ,je_category
         ,sla_dist_accounted_net
         ,dtl_id
         ,dtl_entity_code
         ,dist_source_type
         ,period_name
         ,xdl_src_dist_type
         ,xdl_src_dist_id_num1
         ,created_by
         ,creation_date
         ,report_type
         ,receipt_number
         ,receipt_date
  FROM xxcus.xxcus_glsla_salestax_recovery
  WHERE 1 =1;   

  type lc_refcursor is ref cursor;
  WC_TaxRecovery    lc_refcursor;

  TYPE gl_sla_ar_tbl IS TABLE OF gl_sla_ar%ROWTYPE INDEX BY BINARY_INTEGER;
  gl_sla_ar_row gl_sla_ar_tbl;
  
    CURSOR inv_ids (p_req_id IN NUMBER) IS
    SELECT customer_trx_id, period_name, gl_account_string, SUM(sla_dist_accounted_net) total_wrtoff_amount
    FROM xxcus.xxcus_glsla_salestax_recovery
    WHERE 1 =1
    AND request_id =p_req_id
    AND customer_trx_id IS NOT NULL 
    GROUP BY customer_trx_id, period_name, gl_account_string
    ORDER BY 1 ASC;    
    
    TYPE inv_ids_tbl IS TABLE OF inv_ids%ROWTYPE INDEX BY BINARY_INTEGER;
    inv_ids_row inv_ids_tbl;
  
   CURSOR receipts (p_type in VARCHAR2, p_req_id in number) IS 
   SELECT 'NA' branch
         ,b.gl_account_string
         ,a.customer_number customer_number
         ,a.customer_name customer_name
         ,b.adj_number
         ,b.wrtoff_date
         ,b.invoice_source
         ,b.customer_trx_id
         ,b.invoice_number
         ,b.invoice_date
         ,b.sla_dist_accounted_net wrtoff_amount
         ,b.total_freight
         ,b.taxable_amount
         ,b.total_tax
         ,b.invoice_total
         ,b.geocode_taxware_rate
         ,b.ship_address1
         ,b.ship_address2
         ,b.ship_address3
         ,b.ship_city
         ,b.ship_state
         ,b.ship_zipcode
         ,b.ship_county
         ,b.geocode
         ,b.ship_via
         ,b.order_header_id
         ,b.order_number         
         ,b.point_of_taxation
         ,b.branch_city
         ,b.branch_state
         ,b.branch_postal_code
         ,b.branch_geocode         
         ,b.request_id 
         ,b.je_source
         ,b.je_category
         ,b.sla_dist_accounted_net
         ,b.dtl_id
         ,b.dtl_entity_code
         ,b.dist_source_type
         ,b.period_name      
         ,b.xdl_src_dist_type
         ,b.xdl_src_dist_id_num1
         ,b.created_by
         ,b.creation_date
         ,b.report_type
         ,a.receipt_number receipt_number
         ,a.receipt_date receipt_date         
    FROM xxcus.xxcus_glsla_salestax_recovery b
        ,ar_cash_receipts_v a
    WHERE 1 =1
      AND b.request_id =p_req_id
      AND b.report_type =p_type
      AND b.je_category ='Receipts'
      AND a.cash_receipt_id(+) =b.dtl_id;

  
  cursor my_writeoffs (p_type in VARCHAR2, p_req_id in number) is
    select period_name||'|'||
           gl_account_string||'|'||
           branch||'|'||
           invoice_source||'|'||
           order_number||'|'||
           invoice_number||'|'||
           invoice_date||'|'||
           customer_number||'|'||
           customer_name||'|'||
           invoice_total||'|'||
           taxable_amount||'|'||
           total_freight||'|'||
           total_tax||'|'||
           wrtoff_amount||'|'||
           geocode_taxware_rate||'|'||
           ship_via||'|'||
           branch_city||'|'||
           branch_state||'|'||
           branch_postal_code||'|'||
           ship_city||'|'||
           ship_state||'|'||
           ship_county||'|'||
           ship_zipcode||'|'||
           receipt_number||'|'||
           receipt_date||'|'||   
           case
            when receipt_number is null then customer_trx_id
            else dtl_id 
           end writeoff_line
    from   xxcus.xxcus_glsla_salestax_recovery           
    where  1 =1
      and  report_type =p_type
      and  request_id =p_req_id;  
  
  lc_request_data VARCHAR2(20) :='';
  n_req_id NUMBER :=0;
  p_limit NUMBER :=5000;
  sql_text VARCHAR2(20000);
  
  v_wrtoff_file_name varchar2(40);
  n_user_id NUMBER :=0;
  v_email       fnd_user.email_address%type :=Null;
  v_cp_long_name   fnd_concurrent_programs_tl.user_concurrent_program_name%type :=Null;   
  v_path VARCHAR2(80);
  ln_request_id NUMBER :=0;
  v_child_requests VARCHAR2(2);
  n_tbl_count Number :=0;
  output_file_id   UTL_FILE.FILE_TYPE;
  v_audit_missing varchar2(1) :=Null;
  v_oracle_branch varchar2(3);
  n_ship_siteuse_id  number :=0;
  n_ship_cust_id     number :=0;
  g_checkpoint_flag varchar2(1) :='N';
  v_trx_source ra_batch_sources_all.name%type;
  v_prism_branch varchar2(40);
  v_prism_ship_method varchar2(40);
  b_move_fwd boolean;
  n_total_fetch number :=0;
  n_customer_trx_id number :=0;
  v_wc_branch   XXCUS.XXHDS_GLSLA_AR_DRILLDOWN.wc_branch%TYPE :=Null;
  g_user_id number :=fnd_global.user_id;
  g_creation_date date :=sysdate;
  l_dollar_amount number;
  l_payment_amount number;
  n_prev_cash_receipt_id number :=0;
  n_rec_app_id number :=0;
  n_rec_trx_id number :=0;
  v_rcpt_wrt_off_name ar_receivable_applications_v.trx_number%type :=Null;
  v_rcpt_wrt_off_activity ar_receivable_applications_v.rec_activity_name%type :=Null;
  v_enter varchar2(1) :='
';
  v_header varchar2(2000) :='PERIOD|ACCOUNT|BRANCH|INVOICE_SOURCE|ORDER_NUMBER|INVOICE_NUMBER|INVOICE_DATE|SHIPTO_CUSTOMER_NUMBER|SHIPTO_CUSTOMER_NAME|INVOICE_TOTAL|PRODUCT_TOTAL|FREIGHT_TOTAL|TAX_TOTAL|WRTOFF_TOTAL|MAX_TAX_RATE|SHIP_VIA|BRANCH_CITY|BRANCH_STATE|BRANCH_POSTALCODE|SHIPTO_CITY|SHIPTO_STATE|SHIPTO_COUNTY|SHIPTO_POSTALCODE|RECEIPT_NUMBER|RECEIPT_DATE|INTERNAL_ID';
  begin
       begin 
       delete xxcus.xxcus_glsla_salestax_recovery
       where 1 =1
         and report_type =p_report_type;
       exception
        when others then
         rollback;
         raise_application_error(-20010, 'Issue in deleting existing record for report_type ='||p_report_type);
       end;
   -- Store the concurrent request id
   n_req_id :=fnd_global.conc_request_id;

   print_log('Inside main, p_ledger_id ='||p_ledger_id);
   print_log('Inside main, p_period_name ='||p_period_name);
   print_log('Inside main, p_product ='||p_product);
   print_log('Inside main, p_list_of_accounts ='||p_list_of_accounts); 
   print_log('Inside main, p_report_type ='||p_report_type);  
   print_log('');
   print_log('Before scripting dynamic SQL');
   print_log('');
   print_log('');      
   sql_text :='SELECT    to_char(null) branch
         ,gcc.segment4  gl_account_string
         ,to_char(null) customer_number
         ,to_char(null) customer_name
         ,to_char(null) adj_number
         ,to_date(null) wrtoff_date
         ,to_char(null) invoice_source
         ,case
           when dtl_entity_code =''ADJUSTMENTS'' then ar_dtls.adj_inv_id
           when dtl_entity_code =''RECEIPTS'' then null
           else ar_dtls.dtl_id
          end  customer_trx_id
         ,to_char(null) invoice_number
         ,to_date(null) invoice_date
         ,to_number(null) wrtoff_amount
         ,to_number(null) total_freight
         ,to_number(null) taxable_amount
         ,to_number(null) total_tax
         ,to_number(null) invoice_total
         ,to_char(null) geocode_taxware_rate
         ,to_char(null) ship_address1
         ,to_char(null) ship_address2
         ,to_char(null) ship_address3
         ,to_char(null) ship_city
         ,to_char(null) ship_state
         ,to_char(null) ship_zipcode
         ,to_char(null) ship_county
         ,to_char(null) geocode
         ,to_char(null) ship_via
         ,to_number(null) order_header_id
         ,to_number(null) order_number
         ,to_char(null) point_of_taxation
         ,to_char(null) branch_city
         ,to_char(null) branch_state
         ,to_char(null) branch_postal_code
         ,to_char(null) branch_geocode '||v_enter|| 
      '         ,'||n_req_id||' request_id'||v_enter||                
         --,to_number(null) request_id 
         ',jh.je_source je_source
         ,jh.je_category
         ,NVL (xdl.unrounded_accounted_cr, 0) - NVL (xdl.unrounded_accounted_dr, 0)  sla_dist_accounted_net
         ,ar_dtls.dtl_id
         ,ar_dtls.dtl_entity_code
         ,NVL (dist.source_type, NULL) dist_source_type
         ,jh.period_name period_name      
         ,xdl.source_distribution_type xdl_src_dist_type
         ,xdl.source_distribution_id_num_1 xdl_src_dist_id_num1'||v_enter||
      '         ,'||g_user_id||' created_by'||v_enter||
      '         ,'||'sysdate creation_date'||v_enter|| 
      '         ,'||''''||p_report_type||''''||' report_type'||v_enter||
      '         ,to_char(null) receipt_number'||v_enter||                         
      '         ,to_date(null) receipt_date'||v_enter||      
     '      FROM gl_ledgers gle,
          gl_je_headers jh,
          gl_je_lines jl,
          gl_code_combinations_kfv gcc,
          gl_import_references jir,
          xla_distribution_links xdl,
          xla_ae_lines ael,
          xla_ae_headers aeh,
          ar_distributions_all dist,
          ra_cust_trx_line_gl_dist_all ra_dist,
          gl_period_statuses gps,
          gl_je_batches gb,
          xla_events xlae,
          (SELECT  xlate.source_id_int_1 dtl_id,                 
                  xlate.entity_id dtl_entity_id,
                  xlate.entity_code dtl_entity_code,
                  to_number(null) adj_inv_id                                                 
             FROM xla_transaction_entities xlate,
                  fnd_application fndapp
            WHERE     1 = 1
                  AND fndapp.application_short_name = ''AR''
                  AND xlate.application_id = fndapp.application_id --do not miss this condition at any cost
                  AND xlate.entity_code =''TRANSACTIONS''
           UNION
           SELECT xlate.source_id_int_1 dtl_id,                 
                  xlate.entity_id dtl_entity_id,
                  xlate.entity_code dtl_entity_code, 
                  to_number(null) adj_inv_id                                   
             FROM xla_transaction_entities xlate,
                  fnd_application fndapp
            WHERE     1 = 1
                  AND fndapp.application_short_name = ''AR''
                  AND xlate.application_id = fndapp.application_id --do not miss this condition at any cost
                  AND xlate.entity_code =''RECEIPTS''
           UNION
           SELECT xlate.source_id_int_1 dtl_id,                
                  xlate.entity_id dtl_entity_id,
                  xlate.entity_code dtl_entity_code,
                  adj.customer_trx_id adj_inv_id                                             
             FROM xla_transaction_entities xlate,
                  fnd_application fndapp,
                  ar_adjustments adj                  
            WHERE     1 = 1
                  AND fndapp.application_short_name = ''AR''
                  AND xlate.application_id = fndapp.application_id --do not miss this condition at any cost
                  AND xlate.entity_code =''ADJUSTMENTS''
                  AND adj.adjustment_id =xlate.source_id_int_1
           ) ar_dtls
    WHERE     1 =1'||v_enter||
      '      AND gps.period_name ='||''''||p_period_name||''''||v_enter||
      '      AND gcc.segment1 ='||''''||p_product||'''';
    sql_text :=sql_text||v_enter||  
      '      AND gcc.segment4 in ('||get_acct_where_clause(p_list_of_accounts)||')'||v_enter||                      
      '      AND jh.je_header_id = jl.je_header_id
      AND jl.code_combination_id = gcc.code_combination_id
      AND jh.je_header_id = jir.je_header_id
      AND jl.je_line_num = jir.je_line_num
      AND jh.ledger_id = gle.ledger_id
      AND jh.je_source = ''Receivables'' 
      AND jir.gl_sl_link_id = ael.gl_sl_link_id(+)
      AND jir.gl_sl_link_table = ael.gl_sl_link_table(+)
      AND ael.ae_line_num = xdl.ae_line_num(+)
      AND ael.ae_header_id = xdl.ae_header_id(+)
      AND ael.ae_header_id = aeh.ae_header_id(+)
      AND xlae.application_id(+) = aeh.application_id              --sbala
      AND xlae.event_id(+) = aeh.event_id                         -- sbala          
      AND ar_dtls.dtl_entity_id(+) = xlae.entity_id                --sbala
      --AND ar_dtls.dtl_id =1864478
      --AND ar_dtls.dtl_entity_code =''TRANSACTIONS''
      AND dist.line_id(+) =
             DECODE (
                xdl.source_distribution_type,
                ''AR_DISTRIBUTIONS_ALL'', xdl.source_distribution_id_num_1,
                NULL)
      AND ra_dist.cust_trx_line_gl_dist_id(+) =
             DECODE (xdl.source_distribution_type,
                     ''AR_DISTRIBUTIONS_ALL'', NULL,
                     xdl.source_distribution_id_num_1)
      AND gps.application_id = 101
      AND gps.period_name = jh.period_name
      AND gps.ledger_id = gle.ledger_id
      AND gle.ledger_id ='||p_ledger_id||v_enter||  --sbala
      'AND jh.je_batch_id = gb.je_batch_id';
   print_log('');      
   print_log('After scripting dynamic SQL');      
   print_log ('');   
   print_log ('Current sql statement');
   print_log ('=====================');
   print_log ('');      
   print_log (sql_text);   
   print_log('');
   
   print_log('Before Open cursor');                   
   print_log ('');     
   
   open WC_TaxRecovery for sql_text;   
   loop 
    n_total_fetch :=n_total_fetch +1;
    fetch WC_TaxRecovery bulk collect into gl_sla_ar_row limit p_limit;      
    exit when gl_sla_ar_row.COUNT =0;
    
    if gl_sla_ar_row.COUNT >0 then
     print_log('Fetch seq '||n_total_fetch||', gl_sla_ar_row.COUNT ='||gl_sla_ar_row.COUNT);
    
           --Run the cursor and populate the table with chunks of data

       begin 
         print_log ('Before raw SQL bulk insert');       
        forall recs in gl_sla_ar_row.first .. gl_sla_ar_row.last
         insert into xxcus.xxcus_glsla_salestax_recovery values gl_sla_ar_row(recs);
           if sql%rowcount >0 then
            print_log ('Bulk insert count ='||sql%rowcount);
           else
            print_log ('Bulk insert count ='||0);
           end if;
         print_log ('After raw SQL bulk insert');
       exception
        when others then
         print_log('@Fetch seq '||n_total_fetch||', issue in forall insert ='||sqlerrm);
       end;
                  
    else
     print_log('gl_sla_ar_row.COUNT =0');  
    end if; --gl_sla_ar_row.COUNT =0          
   end loop;   
   close WC_TaxRecovery;       
   print_log('After Close cursor');
   
   --process invoice related tax write off which may come from adjustments, sales invoices, debit and credit memos
       -- get all unique internal invoice id's along with the supporting information
       
   begin 
    gl_sla_ar_row.delete;
    b_move_fwd :=TRUE;
   exception
    when others then
     print_log ('Issue during clean up of the plsql table gl_sla_ar_row'||sqlerrm);
     b_move_fwd :=FALSE;
   end;
       
   if (b_move_fwd) then
       open inv_ids(n_req_id); 
        fetch inv_ids bulk collect into inv_ids_row; 
       close inv_ids;
               
       print_log('Unique Invoice Record Count ='||inv_ids_row.count);
                 
         if inv_ids_row.count >0 then 
             
               -- ============================================================
               -- cleanup the current table for everything excluding receipts
               -- ============================================================       
               begin 
                 delete xxcus.xxcus_glsla_salestax_recovery
                 where 1 =1
                   and request_id      =n_req_id
                   and report_type     =p_report_type
                   and dtl_entity_code IN ('ADJUSTMENTS', 'TRANSACTIONS');
                 b_move_fwd :=TRUE;
               exception
                 when others then
                  print_log ('@Fetch seq '||n_total_fetch||', issue during clean up of the table xxcus.xxcus_glsla_salestax_recovery '||sqlerrm);
                  b_move_fwd :=FALSE;
               end;             
             
               for idx in inv_ids_row.first .. inv_ids_row.last loop 
               
                    Begin   
                        select a.name
                              ,b.ship_to_customer_id
                              ,b.ship_to_site_use_id
                              ,substr(lpad(substr(b.trx_number, 1, (instr(b.trx_number,'-')-1)), 9, '0'), 1, 3) 
                              ,SUBSTR(b.interface_header_attribute12, 1, 40)     
                        into   v_trx_source
                              ,n_ship_cust_id
                              ,n_ship_siteuse_id
                              ,v_prism_branch
                              ,v_prism_ship_method
                        from   ra_batch_sources   a
                              ,ra_customer_trx_all b
                        where  1 =1
                          and  b.customer_trx_id =inv_ids_row(idx).customer_trx_id
                          and  a.batch_source_id =b.batch_source_id;  
                          
                       gl_sla_ar_row(idx).invoice_source :=v_trx_source;
                       
                       --print_log('Legacy Branch ='||v_prism_branch);
                       
                          -- assign v_legacy_branch to branch number of batch source is CONVERSION, PRISM and PRISM REFUND 
                       If nvl(v_trx_source, 'Z') NOT IN ('CONVERSION', 'PRISM', 'PRISM REFUND') Then 
                             v_prism_branch       :=Null;
                             --gl_sla_ar_row(idx).branch :=Null;                             
                       Else
                         Null; --leave the existing prism branch and prism shipmethod as it is
                         If v_trx_source ='PRISM' Then
                            gl_sla_ar_row(idx).branch   :=v_prism_branch;
                            gl_sla_ar_row(idx).ship_via :=v_prism_ship_method;
                         Else                            
                            gl_sla_ar_row(idx).ship_via :=Null;  --only prism invoices store ship method                           
                         End If;                         
                       End If;                                                                                                                                              
                                         
                    Exception
                        When No_Data_Found Then
                         v_trx_source         :='*NO SOURCE*';
                         gl_sla_ar_row(idx).invoice_source :=v_trx_source;
                         n_ship_cust_id       :=Null;
                         n_ship_siteuse_id    :=Null; 
                         v_prism_branch       :=Null;
                         v_prism_ship_method  :=Null;                                                
                        When Others Then
                         v_trx_source         :='*NO INV SRC*';
                         gl_sla_ar_row(idx).invoice_source :=v_trx_source;
                         n_ship_cust_id       :=Null;
                         n_ship_siteuse_id    :=Null;
                         v_prism_branch       :=Null;
                         v_prism_ship_method  :=Null;                         
                    End;                                                   
               
                    begin 
                       select a.cust_ship_to_number
                                  ,a.cust_ship_to_name
                                  ,a.whs_branch_name
                                  ,a.trx_number
                                  ,a.trx_date
                                  ,a.total_freight
                                  ,a.total_tax                  
                                  ,a.total_invoice 
                                  ,a.total_line_items
                                  ,a.cust_ship_to_address1
                                  ,a.cust_ship_to_address2
                                  ,a.cust_ship_to_address3
                                  ,a.cust_ship_to_city
                                  ,a.cust_ship_to_state_prov
                                  ,a.cust_ship_to_zip_code
                                  ,a.cust_ship_to_county 
                                  ,a.customer_trx_id               
                       into   gl_sla_ar_row(idx).customer_number
                                  ,gl_sla_ar_row(idx).customer_name
                                  ,gl_sla_ar_row(idx).branch
                                  ,gl_sla_ar_row(idx).invoice_number                                                                        
                                  ,gl_sla_ar_row(idx).invoice_date
                                  ,gl_sla_ar_row(idx).total_freight
                                  ,gl_sla_ar_row(idx).total_tax
                                  ,gl_sla_ar_row(idx).invoice_total
                                  ,gl_sla_ar_row(idx).taxable_amount
                                  ,gl_sla_ar_row(idx).ship_address1
                                  ,gl_sla_ar_row(idx).ship_address2
                                  ,gl_sla_ar_row(idx).ship_address3                                    
                                  ,gl_sla_ar_row(idx).ship_city
                                  ,gl_sla_ar_row(idx).ship_state
                                  ,gl_sla_ar_row(idx).ship_zipcode
                                  ,gl_sla_ar_row(idx).ship_county
                                  ,n_customer_trx_id                                                                                                            
                       from   xxwc.xxwcar_taxware_audit_tbl a            
                       where  1 =1
                         and  a.customer_trx_id =inv_ids_row(idx).customer_trx_id; 
                         
                       --v_audit_missing :='N';                                                                                         
                                              
                    exception 
                     When No_Data_Found Then
                        -- what if the custom table has no record for an invoice id, lets use the one from sla
                        n_customer_trx_id :=inv_ids_row(idx).customer_trx_id;  
                        --v_audit_missing :='Y';
                        -- when this happens we need to query from the ar tables so we have all the fields filled in
                          begin 
                             select hzca.account_number
                                  ,hzp.party_name 
                                  ,case
                                    when a.interface_header_context ='ORDER ENTRY' 
                                      then
                                        (
                                          select organization_code
                                          from   org_organization_definitions
                                          where  1 =1
                                            and  to_char(organization_id) =a.interface_header_attribute10
                                        )
                                    when a.interface_header_context ='PRISM INVOICES' then (lpad(a.interface_header_attribute7, 3,'0'))
                                    when a.interface_header_context ='CONVERSION' 
                                           then (substr(lpad(substr(a.trx_number, 1, (instr(a.trx_number,'-')-1)), 9, '0'), 1, 3))
                                    else 'NA' 
                                   end whs_branch_name
                                  ,a.trx_number
                                  ,a.trx_date
                                  ,apsa.freight_original            total_freight
                                  ,apsa.tax_original                total_tax                  
                                  ,apsa.amount_due_original         total_invoice 
                                  ,apsa.amount_line_items_original  total_line_items
                                  ,hzl.address1 
                                  ,hzl.address2
                                  ,hzl.address3
                                  ,hzl.city
                                  ,hzl.state
                                  ,hzl.postal_code
                                  ,hzl.county 
                                  ,a.customer_trx_id 
                             into   gl_sla_ar_row(idx).customer_number
                                          ,gl_sla_ar_row(idx).customer_name
                                          ,gl_sla_ar_row(idx).branch
                                          ,gl_sla_ar_row(idx).invoice_number                                                                        
                                          ,gl_sla_ar_row(idx).invoice_date
                                          ,gl_sla_ar_row(idx).total_freight
                                          ,gl_sla_ar_row(idx).total_tax
                                          ,gl_sla_ar_row(idx).invoice_total
                                          ,gl_sla_ar_row(idx).taxable_amount
                                          ,gl_sla_ar_row(idx).ship_address1
                                          ,gl_sla_ar_row(idx).ship_address2
                                          ,gl_sla_ar_row(idx).ship_address3                                    
                                          ,gl_sla_ar_row(idx).ship_city
                                          ,gl_sla_ar_row(idx).ship_state
                                          ,gl_sla_ar_row(idx).ship_zipcode
                                          ,gl_sla_ar_row(idx).ship_county
                                          ,n_customer_trx_id                                  
                             from   ra_customer_trx_all a
                                     ,ar_payment_schedules_all apsa 
                                     ,hz_cust_accounts hzca
                                     ,hz_parties hzp
                                     ,hz_cust_site_uses_all hzcsu
                                     ,hz_party_sites hzps
                                     ,hz_cust_acct_sites_all hzcas
                                     ,hz_locations hzl                                       
                             where  1 =1 
                               and  a.customer_trx_id         =n_customer_trx_id    
                               and  apsa.customer_trx_id(+)    =a.customer_trx_id
                               and  apsa.class(+)              ='INV'
                               and  hzca.cust_account_id(+)    =a.ship_to_customer_id  
                               and  hzp.party_id(+)            =hzca.party_id 
                               and  hzcsu.site_use_id(+)       =a.ship_to_site_use_id                                                       
                               and  hzcas.cust_account_id      =hzca.cust_account_id         
                               and  hzcas.cust_acct_site_id(+) =hzcsu.cust_acct_site_id          
                               and  hzps.party_site_id(+)     =hzcas.party_site_id
                               and  hzl.location_id(+)         =hzps.location_id;
                                
                              gl_sla_ar_row(idx).wrtoff_amount     :=inv_ids_row(idx).total_wrtoff_amount;
                              gl_sla_ar_row(idx).gl_account_string :=inv_ids_row(idx).gl_account_string;
                              gl_sla_ar_row(idx).period_name       :=inv_ids_row(idx).period_name;
                              gl_sla_ar_row(idx).customer_trx_id   :=inv_ids_row(idx).customer_trx_id; 
                              gl_sla_ar_row(idx).report_type       :=p_report_type;
                              gl_sla_ar_row(idx).request_id        :=n_req_id; 
                                                                                                                                             
                          exception
                            when others then                            
                              gl_sla_ar_row(idx).customer_number  :=Null;
                              gl_sla_ar_row(idx).customer_name    :=Null;
                              gl_sla_ar_row(idx).invoice_number   :=Null;                                                                     
                              gl_sla_ar_row(idx).invoice_date     :=Null;
                              gl_sla_ar_row(idx).total_freight    :=Null;
                              gl_sla_ar_row(idx).total_tax        :=Null;
                              gl_sla_ar_row(idx).invoice_total    :=Null;
                              gl_sla_ar_row(idx).taxable_amount   :=Null;
                              gl_sla_ar_row(idx).ship_address1    :=Null;
                              gl_sla_ar_row(idx).ship_address2    :=Null;
                              gl_sla_ar_row(idx).ship_address3    :=Null;                                 
                              gl_sla_ar_row(idx).ship_city        :=Null;
                              gl_sla_ar_row(idx).ship_state       :=Null;
                              gl_sla_ar_row(idx).ship_zipcode     :=Null;
                              gl_sla_ar_row(idx).ship_county      :=Null;
                              gl_sla_ar_row(idx).wrtoff_amount     :=inv_ids_row(idx).total_wrtoff_amount;
                              gl_sla_ar_row(idx).gl_account_string :=inv_ids_row(idx).gl_account_string;
                              gl_sla_ar_row(idx).period_name       :=inv_ids_row(idx).period_name;
                              gl_sla_ar_row(idx).customer_trx_id   :=inv_ids_row(idx).customer_trx_id; 
                              gl_sla_ar_row(idx).report_type       :=p_report_type;
                              gl_sla_ar_row(idx).request_id        :=n_req_id;                         
                          end;                                           
                     When Too_many_rows then 
                        -- what if the custom table has no record for an invoice id, lets use the one from sla
                        n_customer_trx_id :=inv_ids_row(idx).customer_trx_id;  
                        --v_audit_missing :='Y';
                        -- when this happens we need to query from the ar tables so we have all the fields filled in                      
                          gl_sla_ar_row(idx).customer_number  :=Null;
                          gl_sla_ar_row(idx).customer_name    :=Null;
                          --gl_sla_ar_row(idx).branch           :='ER2';
                          gl_sla_ar_row(idx).invoice_number   :=Null;                                                                     
                          gl_sla_ar_row(idx).invoice_date     :=Null;
                          gl_sla_ar_row(idx).total_freight    :=Null;
                          gl_sla_ar_row(idx).total_tax        :=Null;
                          gl_sla_ar_row(idx).invoice_total    :=Null;
                          gl_sla_ar_row(idx).taxable_amount   :=Null;
                          gl_sla_ar_row(idx).ship_address1    :=Null;
                          gl_sla_ar_row(idx).ship_address2    :=Null;
                          gl_sla_ar_row(idx).ship_address3    :=Null;                                 
                          gl_sla_ar_row(idx).ship_city        :=Null;
                          gl_sla_ar_row(idx).ship_state       :=Null;
                          gl_sla_ar_row(idx).ship_zipcode     :=Null;
                          gl_sla_ar_row(idx).ship_county      :=Null;
                         gl_sla_ar_row(idx).wrtoff_amount     :=inv_ids_row(idx).total_wrtoff_amount;
                         gl_sla_ar_row(idx).gl_account_string :=inv_ids_row(idx).gl_account_string;
                         gl_sla_ar_row(idx).period_name       :=inv_ids_row(idx).period_name;
                         gl_sla_ar_row(idx).customer_trx_id   :=inv_ids_row(idx).customer_trx_id;
                         gl_sla_ar_row(idx).report_type       :=p_report_type;
                         gl_sla_ar_row(idx).request_id        :=n_req_id;                                                            
                     When others then
                        -- what if the custom table has no record for an invoice id, lets use the one from sla
                        n_customer_trx_id :=inv_ids_row(idx).customer_trx_id;  
                        --v_audit_missing :='Y';
                        -- when this happens we need to query from the ar tables so we have all the fields filled in                      
                          gl_sla_ar_row(idx).customer_number  :=Null;
                          gl_sla_ar_row(idx).customer_name    :=Null;
                          --gl_sla_ar_row(idx).branch           :='ER3';
                          gl_sla_ar_row(idx).invoice_number   :=Null;                                                                     
                          gl_sla_ar_row(idx).invoice_date     :=Null;
                          gl_sla_ar_row(idx).total_freight    :=Null;
                          gl_sla_ar_row(idx).total_tax        :=Null;
                          gl_sla_ar_row(idx).invoice_total    :=Null;
                          gl_sla_ar_row(idx).taxable_amount   :=Null;
                          gl_sla_ar_row(idx).ship_address1    :=Null;
                          gl_sla_ar_row(idx).ship_address2    :=Null;
                          gl_sla_ar_row(idx).ship_address3    :=Null;                                 
                          gl_sla_ar_row(idx).ship_city        :=Null;
                          gl_sla_ar_row(idx).ship_state       :=Null;
                          gl_sla_ar_row(idx).ship_zipcode     :=Null;
                          gl_sla_ar_row(idx).ship_county      :=Null;
                         gl_sla_ar_row(idx).wrtoff_amount     :=inv_ids_row(idx).total_wrtoff_amount;
                         gl_sla_ar_row(idx).gl_account_string :=inv_ids_row(idx).gl_account_string;
                         gl_sla_ar_row(idx).period_name       :=inv_ids_row(idx).period_name;
                         gl_sla_ar_row(idx).customer_trx_id   :=inv_ids_row(idx).customer_trx_id; 
                         gl_sla_ar_row(idx).report_type       :=p_report_type;
                         gl_sla_ar_row(idx).request_id        :=n_req_id;                                          
                    end;
                    
                       If (gl_sla_ar_row(idx).ship_city Is Not Null) Then 
                         Null; --keep moving, the audit table has ship to info 
                       Else
                        --Looks like the custom table has no ship address info
                        -- use the fields from ar tables to get the ship to info
                         If (n_ship_siteuse_id Is Not Null) Then 
                           --print_log('n_ship_siteuse_id is not null, ship_city is blank, requery AR ['||n_ship_siteuse_id||'.'||n_ship_cust_id||']');
                          --If () Then --(v_audit_missing ='N') Then
                             Begin 
                                select hzl.address1
                                      ,hzl.address2
                                      ,hzl.address3
                                      ,hzl.city
                                      ,hzl.state
                                      ,hzl.postal_code
                                      ,hzl.county
                                into   gl_sla_ar_row(idx).ship_address1
                                      ,gl_sla_ar_row(idx).ship_address2
                                      ,gl_sla_ar_row(idx).ship_address3                                    
                                      ,gl_sla_ar_row(idx).ship_city
                                      ,gl_sla_ar_row(idx).ship_state
                                      ,gl_sla_ar_row(idx).ship_zipcode
                                      ,gl_sla_ar_row(idx).ship_county        
                                from   hz_cust_site_uses_all hzcsu
                                      ,hz_party_sites hzps
                                      ,hz_cust_acct_sites_all hzcas
                                      ,hz_locations hzl
                                where  1 =1 
                                  and  hzcsu.site_use_id       =n_ship_siteuse_id                                                       
                                  and  hzcas.cust_account_id   =n_ship_cust_id --in ( 29738,92507)         
                                  and  hzcas.cust_acct_site_id =hzcsu.cust_acct_site_id          
                                  and  hzps.party_site_id      =hzcas.party_site_id
                                  and  hzl.location_id         =hzps.location_id;                                                                                     
                             Exception
                              When No_Data_Found Then
                                  gl_sla_ar_row(idx).ship_address1  :=Null;
                                  gl_sla_ar_row(idx).ship_address2  :=Null;
                                  gl_sla_ar_row(idx).ship_address3  :=Null;                                    
                                  gl_sla_ar_row(idx).ship_city      :=Null;
                                  gl_sla_ar_row(idx).ship_state     :=Null;
                                  gl_sla_ar_row(idx).ship_zipcode   :=Null;
                                  gl_sla_ar_row(idx).ship_county    :=Null;
                              When Others Then
                                  gl_sla_ar_row(idx).ship_address1  :=Null;
                                  gl_sla_ar_row(idx).ship_address2  :=Null;
                                  gl_sla_ar_row(idx).ship_address3  :=Null;                                    
                                  gl_sla_ar_row(idx).ship_city      :=Null;
                                  gl_sla_ar_row(idx).ship_state     :=Null;
                                  gl_sla_ar_row(idx).ship_zipcode   :=Null;
                                  gl_sla_ar_row(idx).ship_county    :=Null;
                             End;                          
                          --End If;
                         Else
                           --print_log('n_ship_siteuse_id is null, ship_city is blank, requery AR...');                         
                             --even ar invoice table has no ship to information                         
                              gl_sla_ar_row(idx).ship_address1  :=Null;
                              gl_sla_ar_row(idx).ship_address2  :=Null;
                              gl_sla_ar_row(idx).ship_address3  :=Null;                                    
                              gl_sla_ar_row(idx).ship_city      :=Null;
                              gl_sla_ar_row(idx).ship_state     :=Null;
                              gl_sla_ar_row(idx).ship_zipcode   :=Null;
                              gl_sla_ar_row(idx).ship_county    :=Null; 
                         End If;
                       End If;
                         
                         gl_sla_ar_row(idx).wrtoff_amount     :=inv_ids_row(idx).total_wrtoff_amount;
                         gl_sla_ar_row(idx).gl_account_string :=inv_ids_row(idx).gl_account_string;
                         gl_sla_ar_row(idx).period_name       :=inv_ids_row(idx).period_name;
                         gl_sla_ar_row(idx).customer_trx_id   :=inv_ids_row(idx).customer_trx_id;
                         gl_sla_ar_row(idx).report_type       :=p_report_type;
                         gl_sla_ar_row(idx).request_id        :=n_req_id;  
                         
                       get_geo_maxtaxrate 
                        (
                             p_report_type     =>p_report_type                           --in report type
                            ,p_invoice_source  =>gl_sla_ar_row(idx).invoice_source       --in invoice source
                            ,p_invoice_id      =>n_customer_trx_id                       --in customer_trx_id
                            ,p_prism_branch    =>v_prism_branch                          --prism branch    
                            ,p_state           =>UPPER(gl_sla_ar_row(idx).ship_state)    --in customer ship to state
                            ,p_cityname        =>UPPER(gl_sla_ar_row(idx).ship_city)     --in customer ship to city
                            ,p_zipcode         =>gl_sla_ar_row(idx).ship_zipcode         --in customer ship to postal code 
                            ,p_county          =>UPPER(gl_sla_ar_row(idx).ship_county)   --in customer ship to county 
                            ,p_geo             =>gl_sla_ar_row(idx).geocode              --out varchar2
                            ,p_tax             =>gl_sla_ar_row(idx).geocode_taxware_rate --out number
                            ,p_point_of_tax    =>gl_sla_ar_row(idx).point_of_taxation    --out varchar2
                            ,p_ship_via        =>gl_sla_ar_row(idx).ship_via             --out varchar2
                            ,p_checkpoint_flag =>g_checkpoint_flag                       --out varchar2
                            ,p_branch          =>v_oracle_branch                         --out varchar2
                            ,p_branch_city     =>gl_sla_ar_row(idx).branch_city          --out varchar2
                            ,p_branch_state    =>gl_sla_ar_row(idx).branch_state         --out varchar2
                            ,p_branch_zip      =>gl_sla_ar_row(idx).branch_postal_code   --out varchar2 
                            ,p_branch_geocode  =>gl_sla_ar_row(idx).branch_geocode       --out varchar2 
                            ,p_header_id       =>gl_sla_ar_row(idx).order_header_id      --out number
                            ,p_order_number    =>gl_sla_ar_row(idx).order_number         --out number                                  
                        );  
                        
                        If gl_sla_ar_row(idx).invoice_source IN ('ORDER MANAGEMENT', 'REPAIR OM SOURCE', 'STANDARD OM SOURCE') Then 
                             gl_sla_ar_row(idx).branch :=v_oracle_branch;                        
                        Elsif gl_sla_ar_row(idx).invoice_source IN ('CONVERSION', 'PRISM', 'PRISM REFUND') Then
                             gl_sla_ar_row(idx).branch :=v_prism_branch;                        
                        Else
                             gl_sla_ar_row(idx).branch :='NA';                        
                        End If;                                                                   
               end loop;
              
               begin 
                    print_log('Before Final Bulk Insert for Entity codes ADJUSTMENTS and TRANSACTIONS');
                    print_log('gl_sla_ar_row.count ='||gl_sla_ar_row.count);
                     begin
                       forall indx in gl_sla_ar_row.first .. gl_sla_ar_row.last
                          insert into xxcus.xxcus_glsla_salestax_recovery values gl_sla_ar_row(indx);                      
                     exception
                      when others then
                       print_log('Failed while inserting records for summarized invoices');
                     end;

                    print_log('After Final Bulk Insert for Entity codes ADJUSTMENTS and TRANSACTIONS');                      
               exception
                when others then
                 print_log('ADJUSTMENTS, TRANSACTIONS: Issue in forall final insert ='||sqlerrm);
               end;               
         else
          print_log('No invoices to fetch, summarize and add supporting information');
         end if;  
            
            
   else
    print_log('Error in deleting PLSQL table gl_sla_ar_row');       
   end if;      


    begin 
        gl_sla_ar_row.delete;
    exception
      when others then
         print_log ('Issue during clean up of the plsql table gl_sla_ar_row before copying receipt records'||sqlerrm);
    end;

    --just copy the receipts records back to the custom table.
    open receipts (p_type =>p_report_type, p_req_id =>n_req_id);
    fetch receipts bulk collect into gl_sla_ar_row;
    close receipts;
    
    -- ============================================================
    -- cleanup the current table for everything excluding receipts
    -- ============================================================       
    begin 
     delete xxcus.xxcus_glsla_salestax_recovery
     where 1 =1
       and request_id  =n_req_id
       and report_type =p_report_type
       and dtl_entity_code='RECEIPTS';
     b_move_fwd :=TRUE;
    exception
         when others then
          print_log ('Issue during clean up of the table xxcus.xxcus_glsla_salestax_recovery for receipts'||sqlerrm);
          b_move_fwd :=FALSE;
    end;     
    
    if gl_sla_ar_row.count >0 then
     begin
         forall indx in gl_sla_ar_row.first .. gl_sla_ar_row.last
          Insert into xxcus.xxcus_glsla_salestax_recovery values gl_sla_ar_row(indx);                     
     exception
      when others then
       print_log('Failed while inserting records for summarized receipts');
     end;
    else
     print_log('No receipts to copy back to the custom table for display');
    end if;

    begin 
     select count(1)
     into   n_tbl_count
     from   xxcus.xxcus_glsla_salestax_recovery
     where  1 =1
       and  request_id =n_req_id;
       
        Begin  
            select a.user_concurrent_program_name
                  ,c.email_address
            into   v_cp_long_name
                  ,v_email
            from fnd_amp_requests_v a
                ,fnd_concurrent_programs d
                ,fnd_user c
            where 1 =1
              and a.request_id =n_req_id
              and a.concurrent_program_id =d.concurrent_program_id
              and a.requested_by =c.user_id;                                                  
                                                                                                          
        Exception                         
         When Others Then
           v_cp_long_name :=Null;
           v_email :=Null;
        End;       
       
     if (n_tbl_count >0) then  
     
        if (v_email is not null) then
            begin 
             --create the data file with the tax write off records 
              v_wrtoff_file_name :='HDS_AR_'||p_report_type||'_'||n_req_id||'.txt';
              
               select '/xx_iface/'||lower(name)||'/outbound' into v_path from v$database;
               
               output_file_id  :=utl_file.fopen(v_path, v_wrtoff_file_name, 'w');
               
               print_log('After opening fileid for file '||v_path||'/'||v_wrtoff_file_name);
               
               utl_file.put_line(output_file_id, v_header);
               print_log('Header record copied into the file '||v_path||'/'||v_wrtoff_file_name);

               
               for rec in my_writeoffs (p_type =>p_report_type, p_req_id =>n_req_id) loop 
                utl_file.put_line(output_file_id, rec.writeoff_line); 
               end loop; 

              print_log('All detail records are copied into the file '||v_path||'/'||v_wrtoff_file_name);              
              utl_file.fclose(output_file_id); 
               
              print_log('After close of file '||v_path||'/'||v_wrtoff_file_name);
              
             --create a file to point us where the data file is located along with the absolute path.              
              v_wrtoff_file_name :='zip_file_extract_'||n_req_id||'.txt';
              
              output_file_id  :=utl_file.fopen(v_path, v_wrtoff_file_name, 'w');
              print_log('After opening fileid for file '||v_path||'/'||v_wrtoff_file_name);
                      
              utl_file.put_line(output_file_id, v_path||'/'||'HDS_AR_'||p_report_type||'_'||n_req_id||'.txt');
               
              utl_file.fclose(output_file_id);
              print_log('After close of file '||v_path||'/'||v_wrtoff_file_name);
              
              v_child_requests :='OK';
                             
            exception
              when others then
                v_child_requests :='NA';
            end;
          
            if v_child_requests <>'NA' Then
                
              ln_request_id :=fnd_request.submit_request
                  (
                   application      =>'XXCUS',
                   program          =>'XXCUS_PEREND_POSTPROCESSOR',
                   description      =>'Taxwriteoff Send Email',
                   start_time       =>'',
                   sub_request      =>FALSE,
                   argument1        =>n_req_id,
                   argument2        =>v_path||'/zip_file_extract_'||n_req_id||'.txt', 
                   argument3        =>v_cp_long_name,               
                   argument4        =>v_email,
                   argument5        =>'HDS_AR_'||p_report_type||'_'||n_req_id||'.zip',
                   argument6        =>'HDS AR Tax Writeoff Report -Status Update'                                                                         
                  ); 
             --commit work;                         
              if ln_request_id >0 then 
                 fnd_file.put_line(fnd_file.log, 'Successfully submitted program XXHDS Period End Post Processor Program to send email');
              else
                 fnd_file.put_line(fnd_file.log, 'Failed to submit send email program XXHDS Period End Post Processor Program');
              end if; 
                  
            else --v_child_requests ='NA'
              Null;                      
            end if;      
        else --email id is null
           print_log('Email id for the user is blank');
        end if;              
      
     else --no data in custom table
      print_log('Cannot send zip file as there is no data for request id '||n_req_id);
     end if;  
       
    exception
     when others then
      print_log('Other errors while checking zip file data for request id '||n_req_id);
    end;

  exception
   when others then
    fnd_file.put_line(fnd_file.log, 'Error in caller xxhds_glsla_ar_taxrecovery.main ='||sqlerrm);
    --rollback;
  end main; 

  function beforereport return boolean is
   n_ret_code number:=Null;
   v_err_buf  varchar2(2000) :=Null;
  begin
    -- ==============================================   
    -- Print incoming variables in the log file...
    -- ==============================================  
    print_log('');  
    print_log('Inside Before report');
    print_log('Parameter g_ledger_id ='||g_ledger_id);
    print_log('Parameter g_period_name ='||g_period_name);
    print_log('Parameter g_product ='||g_product);
    print_log('Parameter g_list_of_accounts ='||g_list_of_accounts); 
    print_log('Parameter g_report_type ='||g_report_type);    
    print_log('');
    
   xxhds_glsla_ar_taxrecovery.main 
    (
      retcode               =>n_ret_code
     ,errbuf                =>v_err_buf
     ,p_ledger_id           =>xxhds_glsla_ar_taxrecovery.g_ledger_id
     ,p_period_name         =>xxhds_glsla_ar_taxrecovery.g_period_name
     ,p_product             =>xxhds_glsla_ar_taxrecovery.g_product   
     ,p_list_of_accounts    =>xxhds_glsla_ar_taxrecovery.g_list_of_accounts
     ,p_report_type         =>xxhds_glsla_ar_taxrecovery.g_report_type   
    );    
   RETURN TRUE;
   print_log('xxhds_glsla_ar_taxrecovery.beforereport returned TRUE');
  exception
   when others then
     print_log('exception in xxhds_glsla_ar_taxrecovery.beforereport'); 
     print_log(sqlcode);
     print_log(sqlerrm);
     RETURN FALSE;
  end beforereport;

end XXHDS_GLSLA_AR_TAXRECOVERY;
/
