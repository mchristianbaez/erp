/* Formatted on 5/25/2013 8:48:52 AM (QP5 v5.240.12305.39446) */
CREATE OR REPLACE PACKAGE BODY APPS.XXWC_EGO_WC_METADATA_PKG
AS
   /**************************************************************************
    File Name:XXWC_EGO_WC_METADATA_PKG
    PROGRAM TYPE: PL/SQL Package spec and body
    PURPOSE: Upload Metadata
    HISTORY
    -- Description   : Called for creating teh metadata in PIM
    --
    --    Arguments
    --      errbuf  - error message
    --      retcode - error code
    --
    -- Dependencies Tables        : custom table XXWC.XXWC_EGO_MANAGED_ATTR_DEF
    --                              custom table XXWC.XXWC_EGO_ICCSTRUCTURE
    -- Dependencies Views         : None
    -- Dependencies Sequences     : None
    -- Dependencies Procedures    : None
    -- Dependencies Functions     : None
    -- Dependencies Packages      : FND_PROFILE
    -- Dependencies Types         : None
    -- Dependencies Database Links: None
    ================================================================
           Last Update Date : 06/06/2010
    ================================================================
    ================================================================
    VERSION DATE          AUTHOR(S)       DESCRIPTION
    ------- -----------   --------------- ---------------------------------
    1.0     09/23/2012    Rajiv Rathod    Initial creation of the package
   **************************************************************************/
   /********************************************************************************
                              PROGRAM TYPE: FUNCTION
   NAME: AG_NAME_FUNC
   PURPOSE: Function to provide abbreviated attribute group internal name
   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.0     09/23/2012    Rajiv Rathod    Initial creation of the function
    ********************************************************************************/
   FUNCTION ag_name_func (p_ag_name VARCHAR2)
      RETURN VARCHAR2
   IS
      l_ag_name      VARCHAR2 (100);
      l_location     NUMBER := 0;
      l_new_string   VARCHAR2 (100);
      l_return       VARCHAR2 (100) := 'XXWC_';
   BEGIN
      l_ag_name := UPPER (p_ag_name);
      l_new_string := l_ag_name;

      -- DBMS_OUTPUT.put_line ('Hello');

      IF (p_ag_name = 'Chemical Sprayers & Accessories')
      THEN
         RETURN 'XXWC_CHEM_S___A_AG';
      END IF;

      IF (p_ag_name = 'Conduit Bender & Accessories')
      THEN
         RETURN 'XXWC_COND_B___A_AG';
      END IF;

      IF (p_ag_name = 'Expanded Polystyrene Insulation')
      THEN
         RETURN 'XXWC_EXP_P_I_AG';
      END IF;

      IF (p_ag_name = 'Fastening Tool Accessories')
      THEN
         RETURN 'XXWC_FAST_T_A_AG';
      END IF;

      IF (LENGTH ('XXWC_' || p_ag_name || '_AG') > 29)
      THEN
         WHILE (l_location != -1)
         LOOP
            --   DBMS_OUTPUT.put_line (
            --     l_new_string || '-' || l_location || '-' || l_return
            --  );
            l_return := l_return || SUBSTR (l_new_string, 1, 1) || '_';

            SELECT INSTR (l_new_string, ' ') + 1 INTO l_location FROM DUAL;

            SELECT SUBSTR (l_new_string, l_location)
              INTO l_new_string
              FROM DUAL;

            IF (l_location = 1)
            THEN
               l_location := -1;
            END IF;
         END LOOP;

         l_return :=
               REPLACE (
                  REPLACE (
                     REPLACE (
                        REPLACE (
                           REPLACE (
                              REPLACE (
                                 REPLACE (
                                    REPLACE (
                                       REPLACE (REPLACE (l_return, ' ', '_'),
                                                '/',
                                                '_'),
                                       '&',
                                       '_'),
                                    '-',
                                    '_'),
                                 ',',
                                 '_'),
                              '''',
                              ''),
                           '(',
                           '_'),
                        ')',
                        '_'),
                     '@',
                     '_'),
                  '.',
                  '_')
            || 'AG';
         RETURN l_return;
      ELSE
         RETURN UPPER (
                      'XXWC_'
                   || REPLACE (
                         REPLACE (
                            REPLACE (
                               REPLACE (
                                  REPLACE (
                                     REPLACE (
                                        REPLACE (
                                           REPLACE (
                                              REPLACE (
                                                 REPLACE (p_ag_name,
                                                          ' ',
                                                          '_'),
                                                 '/',
                                                 '_'),
                                              '&',
                                              '_'),
                                           '-',
                                           '_'),
                                        ',',
                                        '_'),
                                     '''',
                                     ''),
                                  '(',
                                  '_'),
                               ')',
                               '_'),
                            '@',
                            '_'),
                         '.',
                         '_')
                   || '_AG');
      END IF;

      RETURN    'XXWC_'
             || REPLACE (
                   REPLACE (
                      REPLACE (
                         REPLACE (
                            REPLACE (
                               REPLACE (
                                  REPLACE (
                                     REPLACE (
                                        REPLACE (
                                           REPLACE (l_return, ' ', '_'),
                                           '/',
                                           '_'),
                                        '&',
                                        '_'),
                                     '-',
                                     '_'),
                                  ',',
                                  '_'),
                               '''',
                               ''),
                            '(',
                            '_'),
                         ')',
                         '_'),
                      '@',
                      '_'),
                   '.',
                   '_')
             || '_AG';
   END ag_name_func;

   /********************************************************************************
                                                                                                               PROGRAM TYPE: FUNCTION
   NAME: VS_NAME_FUNC
   PURPOSE: Function to provide abbreviated value set internal name
   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.0     09/23/2012    Rajiv Rathod    Initial creation of the function
    ********************************************************************************/

   FUNCTION vs_name_func (p_vs_name VARCHAR2)
      RETURN VARCHAR2
   IS
      l_vs_name      VARCHAR2 (100);
      l_location     NUMBER := 0;
      l_new_string   VARCHAR2 (100);
      l_return       VARCHAR2 (100) := 'XXWC_';
   BEGIN
      l_vs_name := p_vs_name;
      l_new_string := l_vs_name;
      DBMS_OUTPUT.put_line ('Hello');

      IF (LENGTH ('XXWC_' || p_vs_name || '_VS') > 40)
      THEN
         WHILE (l_location != -1)
         LOOP
            DBMS_OUTPUT.put_line (
               l_new_string || '-' || l_location || '-' || l_return);
            l_return := l_return || SUBSTR (l_new_string, 1, 1) || '_';

            SELECT INSTR (l_new_string, ' ') + 1 INTO l_location FROM DUAL;

            SELECT SUBSTR (l_new_string, l_location)
              INTO l_new_string
              FROM DUAL;

            IF (l_location = 1)
            THEN
               l_location := -1;
            END IF;
         END LOOP;

         l_return := REPLACE (l_return, ' ', '_') || 'VS';
         RETURN l_return;
      ELSE
         RETURN UPPER ('XXWC_' || REPLACE (p_vs_name, ' ', '_') || '_VS');
      END IF;

      RETURN 'XXWC_' || REPLACE (l_return, ' ', '_') || '_VS';
   END vs_name_func;

   /********************************************************************************
                              PROGRAM TYPE: FUNCTION
   NAME: ATTR_NAME_FUNC
   PURPOSE: Function to provide abbreviated attribute internal name
   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.0     09/23/2012    Rajiv Rathod    Initial creation of the function
    ********************************************************************************/

   FUNCTION attr_name_func (p_attr_name VARCHAR2)
      RETURN VARCHAR2
   IS
      l_attr_name    VARCHAR2 (100);
      l_location     NUMBER := 0;
      l_new_string   VARCHAR2 (100);
      l_return       VARCHAR2 (100) := 'XXWC_';
   BEGIN
      l_attr_name := UPPER (p_attr_name);
      l_new_string := l_attr_name;

      --  DBMS_OUTPUT.put_line ('Hello');

      IF (TRIM (p_attr_name) = 'Full Cure Time @ Temp.')
      THEN
         RETURN 'XXWC_FULL_C_T__T_ATTR';
      END IF;

      IF (TRIM (p_attr_name) = 'Full Cure Time @ Temperature')
      THEN
         RETURN 'XXWC_FULL_C_T__T_ATTR';
      END IF;

      IF (TRIM (p_attr_name) = 'Carried Member Fastener Type')
      THEN
         RETURN 'XXWC_CRD_M_F_T_ATTR';
      END IF;

      IF (TRIM (p_attr_name) = 'Number of Center Dividers')
      THEN
         RETURN 'XXWC_N_O_CR_D_ATTR';
      END IF;

      IF (LENGTH ('XXWC_' || p_attr_name || '_ATTR') > 30)
      THEN
         WHILE (l_location != -1)
         LOOP
            --      DBMS_OUTPUT.put_line (
            --       l_new_string || '-' || l_location || '-' || l_return
            --     );
            l_return := l_return || SUBSTR (l_new_string, 1, 1) || '_';

            SELECT INSTR (l_new_string, ' ') + 1 INTO l_location FROM DUAL;

            SELECT SUBSTR (l_new_string, l_location)
              INTO l_new_string
              FROM DUAL;

            IF (l_location = 1)
            THEN
               l_location := -1;
            END IF;
         END LOOP;

         l_return :=
               REPLACE (
                  REPLACE (
                     REPLACE (
                        REPLACE (
                           REPLACE (
                              REPLACE (
                                 REPLACE (
                                    REPLACE (
                                       REPLACE (REPLACE (l_return, ' ', '_'),
                                                '/',
                                                '_'),
                                       '&',
                                       '_'),
                                    '-',
                                    '_'),
                                 ',',
                                 '_'),
                              '''',
                              ''),
                           '(',
                           '_'),
                        ')',
                        '_'),
                     '@',
                     '_'),
                  '.',
                  '_')
            || 'ATTR';
         RETURN l_return;
      ELSE
         RETURN UPPER (
                      'XXWC_'
                   || REPLACE (
                         REPLACE (
                            REPLACE (
                               REPLACE (
                                  REPLACE (
                                     REPLACE (
                                        REPLACE (
                                           REPLACE (
                                              REPLACE (
                                                 REPLACE (p_attr_name,
                                                          ' ',
                                                          '_'),
                                                 '/',
                                                 '_'),
                                              '&',
                                              '_'),
                                           '-',
                                           '_'),
                                        ',',
                                        '_'),
                                     '''',
                                     ''),
                                  '(',
                                  '_'),
                               ')',
                               '_'),
                            '@',
                            '_'),
                         '.',
                         '_')
                   || '_ATTR');
      END IF;

      RETURN    'XXWC_'
             || REPLACE (
                   REPLACE (
                      REPLACE (
                         REPLACE (
                            REPLACE (
                               REPLACE (
                                  REPLACE (
                                     REPLACE (
                                        REPLACE (
                                           REPLACE (l_return, ' ', '_'),
                                           '/',
                                           '_'),
                                        '&',
                                        '_'),
                                     '-',
                                     '_'),
                                  ',',
                                  '_'),
                               '''',
                               ''),
                            '(',
                            '_'),
                         ')',
                         '_'),
                      '@',
                      '_'),
                   '.',
                   '_')
             || '_ATTR';
   END attr_name_func;

   /********************************************************************************
                                                                                                               PROGRAM TYPE: PROCEDURE
   NAME: CREATE_METADATA
   PURPOSE: Procedure to create metadata
   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.0     09/23/2012    Rajiv Rathod    Initial creation of the procedure
    ********************************************************************************/

   PROCEDURE create_metadata (errbuf OUT VARCHAR2, retcode OUT NUMBER)
   AS
      --declaring variables
      l_user_id         NUMBER;
      l_dc_number_seq   NUMBER := 0;
      l_dc_char_seq     NUMBER := 0;
      l_dc_long_seq     NUMBER := 0;
      l_n_morethan_1    NUMBER := NULL;
      l_l_morethan_1    NUMBER := NULL;
      l_c_morethan_1    NUMBER := NULL;
      i                 NUMBER;
      j                 NUMBER;
      k                 NUMBER;
      l_sequence_no     NUMBER := 0;
   BEGIN
      fnd_file.put_line (fnd_file.LOG, 'Beginning of Program');

      --apps initialize
      --block to get the user
      BEGIN
         SELECT user_id
           INTO l_user_id
           FROM fnd_user
          WHERE user_name = 'XXWC_INT_SUPPLYCHAIN';
      EXCEPTION
         WHEN OTHERS
         THEN
            fnd_file.put_line (
               fnd_file.LOG,
               'Unable to find user XXWC_INT_SUPPLYCHAIN ' || SQLERRM);
      END;

      fnd_global.apps_initialize (user_id        => l_user_id,
                                  resp_id        => 50296,
                                  resp_appl_id   => 431);

      -- Function to get the correct name for AG internal name

      UPDATE xxwc.xxwc_ego_iccstructure
         SET parent_name = 'WHITE CAP MST'
       WHERE parent_name IS NULL;

      UPDATE xxwc.xxwc_ego_iccstructure b
         SET b.leaf = 'X'
       WHERE NOT EXISTS
                    (SELECT 1
                       FROM xxwc.xxwc_ego_iccstructure a
                      WHERE UPPER (TRIM (b.child_name)) =
                               UPPER (TRIM (a.parent_name)));

      COMMIT;

      BEGIN
         FOR i IN (SELECT DISTINCT icc_name FROM xxwc_ego_managed_attr_def)
         LOOP
            BEGIN
               SELECT NVL (MAX (sequence), 0)
                 INTO l_sequence_no
                 FROM xxwc.xxwc_ego_attr_lookup_tbl
                WHERE segment1 = UPPER (i.icc_name);
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_sequence_no := 0;
            END;

            FOR j IN (  SELECT DISTINCT icc_name, attribute_label
                          FROM xxwc_ego_managed_attr_def
                         WHERE icc_name = i.icc_name
                      ORDER BY attribute_label ASC)
            LOOP
               l_sequence_no := l_sequence_no + 10;

               UPDATE xxwc_ego_managed_attr_def
                  SET sequence = l_sequence_no,
                      attr_group_int_name =
                         xxwc_ego_wc_metadata_pkg.ag_name_func (icc_name),
                      attr_int_name =
                         xxwc_ego_wc_metadata_pkg.attr_name_func (
                            attribute_label)
                WHERE     attribute_label = j.attribute_label
                      AND icc_name = j.icc_name;
            END LOOP;
         END LOOP;

         COMMIT;
      END;

      BEGIN
         FOR i IN (SELECT DISTINCT icc_name FROM xxwc_ego_managed_attr_def)
         LOOP
            BEGIN
               SELECT NVL (MAX (db_col_num), 0)
                 INTO l_dc_number_seq
                 FROM xxwc.xxwc_ego_attr_lookup_tbl
                WHERE segment1 = UPPER (i.icc_name) AND data_type_code = 'N';
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_dc_number_seq := 0;
            END;

            BEGIN
               SELECT NVL (MAX (db_col_num), 0)
                 INTO l_dc_char_seq
                 FROM xxwc.xxwc_ego_attr_lookup_tbl
                WHERE segment1 = UPPER (i.icc_name) AND data_type_code = 'C';
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_dc_char_seq := 0;
            END;

            BEGIN
               SELECT NVL (MAX (db_col_num), 0)
                 INTO l_dc_long_seq
                 FROM xxwc.xxwc_ego_attr_lookup_tbl
                WHERE segment1 = UPPER (i.icc_name) AND data_type_code = 'A';
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_dc_long_seq := 0;
            END;

            FOR j IN (  SELECT *
                          FROM xxwc_ego_managed_attr_def
                         WHERE icc_name = i.icc_name
                      ORDER BY sequence ASC)
            LOOP
               IF (j.attribute_data_type = 'Number')
               THEN
                  l_dc_number_seq := l_dc_number_seq + 1;
               END IF;

               IF (j.attribute_data_type = 'Long')
               THEN
                  l_dc_long_seq := l_dc_long_seq + 1;
               END IF;

               IF (   j.attribute_data_type = 'String'
                   OR j.attribute_data_type = 'Value Set')
               THEN
                  l_dc_char_seq := l_dc_char_seq + 1;
               END IF;

               IF (l_dc_number_seq > 20)
               THEN
                  l_dc_number_seq := 1;
                  l_n_morethan_1 := 2;
               END IF;

               IF (l_dc_long_seq > 20)
               THEN
                  l_dc_long_seq := 1;
                  l_l_morethan_1 := 2;
               END IF;

               IF (l_dc_char_seq > 40)
               THEN
                  l_dc_char_seq := 1;
                  l_c_morethan_1 := 2;
               END IF;


               UPDATE xxwc_ego_managed_attr_def
                  SET database_column =
                         DECODE (j.attribute_data_type,
                                 'Number', 'N_EXT_ATTR' || l_dc_number_seq,
                                 'Long', 'TL_EXT_ATTR' || l_dc_long_seq,
                                 'C_EXT_ATTR' || l_dc_char_seq),
                      more_than_1_ag =
                         DECODE (j.attribute_data_type,
                                 'Number', l_n_morethan_1,
                                 'Long', l_l_morethan_1,
                                 l_c_morethan_1)
                WHERE     attribute_label = j.attribute_label
                      AND icc_name = j.icc_name;
            END LOOP;

            l_dc_number_seq := 0;
            l_dc_char_seq := 0;
            l_dc_long_seq := 0;
            l_n_morethan_1 := NULL;
            l_l_morethan_1 := NULL;
            l_c_morethan_1 := NULL;
         END LOOP;

         COMMIT;

         FOR k IN (SELECT *
                     FROM xxwc_ego_managed_attr_def
                    WHERE more_than_1_ag = 2)
         LOOP
            UPDATE xxwc_ego_managed_attr_def
               SET attr_group_int_name =
                      RTRIM (attr_group_int_name, '_AG') || '2_AG'
             WHERE     attribute_label = k.attribute_label
                   AND icc_name = k.icc_name;
         END LOOP;

         COMMIT;

         UPDATE xxwc_ego_managed_attr_def
            SET attr_group_int_name = 'XXWC_FEATURES_BENEFITS_AG',
                more_than_1_ag = 2
          WHERE     icc_name = 'WHITE CAP MST'
                AND attribute_label = 'Features&Benefits';

         COMMIT;
      END;

      DELETE FROM xxwc.xxwc_ego_managed_attr_def a
            WHERE EXISTS
                     (SELECT 1
                        FROM ego_attrs_v b
                       WHERE     a.attr_group_int_name = b.attr_group_name
                             AND a.database_column = b.database_column);

      DELETE FROM xxwc.xxwc_ego_managed_attr_def a
            WHERE EXISTS
                     (SELECT 1
                        FROM ego_attrs_v b
                       WHERE     a.attr_group_int_name = b.attr_group_name
                             AND a.sequence = b.sequence);

      DELETE FROM xxwc.xxwc_ego_managed_attr_def a
            WHERE EXISTS
                     (SELECT 1
                        FROM ego_attrs_v b
                       WHERE     a.attr_group_int_name = b.attr_group_name
                             AND a.attr_int_name = b.attr_name);

      COMMIT;

      --truncating the interface table before metadata import
      EXECUTE IMMEDIATE 'truncate table ego.MTL_ITEM_CAT_GRPS_INTERFACE';

      -- to read data from custom table for ICC and load interface table

      --Inserting teh child node
      FOR c_icc IN (SELECT leaf,
                           child_key,
                           child_name,
                           parent_key,
                           parent_name
                      FROM xxwc.xxwc_ego_iccstructure)
      LOOP
         BEGIN
            INSERT
              INTO mtl_item_cat_grps_interface (item_catalog_group_id,
                                                item_catalog_name,
                                                inactive_date,
                                                summary_flag,
                                                enabled_flag,
                                                start_date_active,
                                                end_date_active,
                                                description,
                                                segment1,
                                                parent_catalog_group_name,
                                                item_creation_allowed_flag,
                                                set_process_id,
                                                transaction_id,
                                                process_status,
                                                transaction_type,
                                                request_id,
                                                program_application_id,
                                                program_id,
                                                program_update_date,
                                                created_by,
                                                creation_date,
                                                last_updated_by,
                                                last_update_date,
                                                last_update_login)
            VALUES (
                      NULL,
                      TRIM (UPPER (SUBSTR (c_icc.child_name, 1, 40))),
                      NULL,
                      'N',
                      'Y',
                      SYSDATE,
                      NULL,
                      TRIM (c_icc.child_name),
                      TRIM (UPPER (SUBSTR (c_icc.child_name, 1, 40))),
                      NVL (TRIM (UPPER (SUBSTR (c_icc.parent_name, 1, 40))),
                           'WHITE CAP MST'),
                      DECODE (c_icc.leaf, 'X', 'Y', 'N'),
                      1,
                      mtl_system_items_interface_s.NEXTVAL,
                      1,
                      'CREATE',
                      NULL,
                      431,
                      NULL,
                      SYSDATE,
                      fnd_profile.VALUE ('USER_ID'),
                      SYSDATE,
                      fnd_profile.VALUE ('USER_ID'),
                      SYSDATE,
                      fnd_profile.VALUE ('LOGIN_ID'));
         EXCEPTION
            WHEN OTHERS
            THEN
               fnd_file.put_line (
                  fnd_file.LOG,
                     'Error while inserting the ICC interface table for ICC '
                  || c_icc.child_name
                  || ' error: '
                  || SQLERRM);
         END;
      END LOOP;

      COMMIT;

      -- To load value sets
      --truncating the interface table before metadata import
      EXECUTE IMMEDIATE 'truncate table ego.EGO_FLEX_VALUE_SET_INTF';

      EXECUTE IMMEDIATE 'truncate table ego.EGO_FLEX_VALUE_INTF';

      EXECUTE IMMEDIATE 'truncate table ego.EGO_FLEX_VALUE_TL_INTF';

      FOR c_vs
         IN (SELECT DISTINCT
                    (TRIM (
                        UPPER (
                           SUBSTR (icc_name, 1, 40) || '-' || attribute_label)))
                       value_set_name
               FROM xxwc.xxwc_ego_managed_attr_def
              WHERE attribute_data_type = 'Value Set')
      LOOP
         BEGIN
            INSERT INTO ego_flex_value_set_intf (value_set_name,
                                                 value_set_id,
                                                 description,
                                                 version_description,
                                                 format_type,
                                                 longlist_flag,
                                                 validation_type,
                                                 parent_value_set_name,
                                                 version_seq_id,
                                                 start_active_date,
                                                 end_active_date,
                                                 maximum_size,
                                                 minimum_value,
                                                 maximum_value,
                                                 value_column_name,
                                                 value_column_type,
                                                 value_column_size,
                                                 id_column_name,
                                                 id_column_size,
                                                 id_column_type,
                                                 meaning_column_name,
                                                 meaning_column_size,
                                                 meaning_column_type,
                                                 table_application_id,
                                                 application_table_name,
                                                 additional_where_clause,
                                                 transaction_type,
                                                 transaction_id,
                                                 process_status,
                                                 set_process_id,
                                                 request_id,
                                                 program_application_id,
                                                 program_id,
                                                 program_update_date,
                                                 last_update_date,
                                                 last_updated_by,
                                                 creation_date,
                                                 created_by,
                                                 last_update_login)
                 VALUES (vs_name_func (c_vs.value_set_name),
                         NULL,
                         c_vs.value_set_name,
                         NULL,
                         'C',
                         'X',
                         'I',
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         '150',
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         'CREATE',
                         mtl_system_items_interface_s.NEXTVAL,
                         1,
                         1,
                         NULL,
                         431,
                         NULL,
                         NULL,
                         SYSDATE,
                         fnd_profile.VALUE ('USER_ID'),
                         SYSDATE,
                         fnd_profile.VALUE ('USER_ID'),
                         fnd_profile.VALUE ('LOGIN_ID'));
         EXCEPTION
            WHEN OTHERS
            THEN
               fnd_file.put_line (
                  fnd_file.LOG,
                     'Error while inserting the value set name for  '
                  || c_vs.value_set_name
                  || ' error: '
                  || SQLERRM);
         END;
      END LOOP;

      --to load value set name
      FOR c_vsv
         IN (SELECT DISTINCT
                    vs_name_func (
                       TRIM (
                          UPPER (
                                SUBSTR (icc_name, 1, 40)
                             || '-'
                             || attribute_label)))
                       value_set_name,
                    attribute_value
               FROM xxwc.xxwc_ego_managed_attr_def
              WHERE attribute_data_type = 'Value Set')
      LOOP
         BEGIN
            INSERT INTO ego_flex_value_intf (value_set_name,
                                             value_set_id,
                                             flex_value,
                                             flex_value_id,
                                             version_seq_id,
                                             disp_sequence,
                                             start_active_date,
                                             end_active_date,
                                             enabled_flag,
                                             transaction_type,
                                             transaction_id,
                                             process_status,
                                             set_process_id,
                                             request_id,
                                             program_application_id,
                                             program_id,
                                             program_update_date,
                                             last_update_date,
                                             last_updated_by,
                                             creation_date,
                                             created_by,
                                             last_update_login)
                 VALUES (c_vsv.value_set_name,
                         NULL,
                         c_vsv.attribute_value,
                         NULL,
                         NULL,
                         mtl_system_items_interface_s.NEXTVAL,
                         NULL,
                         NULL,
                         'Y',
                         'CREATE',
                         mtl_system_items_interface_s.NEXTVAL,
                         1,
                         1,
                         NULL,
                         431,
                         NULL,
                         NULL,
                         SYSDATE,
                         fnd_profile.VALUE ('USER_ID'),
                         SYSDATE,
                         fnd_profile.VALUE ('USER_ID'),
                         fnd_profile.VALUE ('LOGIN_ID'));

            INSERT INTO ego_flex_value_tl_intf (value_set_name,
                                                value_set_id,
                                                flex_value,
                                                flex_value_id,
                                                version_seq_id,
                                                language,
                                                description,
                                                source_lang,
                                                flex_value_meaning,
                                                transaction_type,
                                                transaction_id,
                                                process_status,
                                                set_process_id,
                                                request_id,
                                                program_application_id,
                                                program_id,
                                                program_update_date,
                                                last_update_date,
                                                last_updated_by,
                                                creation_date,
                                                created_by,
                                                last_update_login)
                 VALUES (c_vsv.value_set_name,
                         NULL,
                         c_vsv.attribute_value,
                         NULL,
                         NULL,
                         'US',
                         c_vsv.attribute_value,
                         'US',
                         c_vsv.attribute_value,
                         'CREATE',
                         mtl_system_items_interface_s.NEXTVAL,
                         1,
                         1,
                         NULL,
                         431,
                         NULL,
                         NULL,
                         SYSDATE,
                         fnd_profile.VALUE ('USER_ID'),
                         SYSDATE,
                         fnd_profile.VALUE ('USER_ID'),
                         fnd_profile.VALUE ('LOGIN_ID'));
         EXCEPTION
            WHEN OTHERS
            THEN
               fnd_file.put_line (
                  fnd_file.LOG,
                     'Error while inserting the values in value set  '
                  || c_vsv.attribute_value
                  || ' error: '
                  || SQLERRM);
         END;
      END LOOP;

      -- To load Attribute groups
      --truncating the interface table before metadata import
      EXECUTE IMMEDIATE 'truncate table ego.EGO_ATTR_GROUPS_INTERFACE';

      EXECUTE IMMEDIATE 'truncate table ego.EGO_ATTR_GROUPS_DL_INTERFACE';

      FOR c_ag
         IN (SELECT DISTINCT icc_name, attr_group_int_name, multirow
               FROM xxwc_ego_managed_attr_def
              WHERE TRIM (UPPER (icc_name)) IN
                       (SELECT segment1 FROM mtl_item_catalog_groups_b))
      LOOP
         BEGIN
            INSERT INTO ego_attr_groups_interface (set_process_id,
                                                   attr_group_id,
                                                   application_id,
                                                   attr_group_type,
                                                   attr_group_name,
                                                   attr_group_disp_name,
                                                   description,
                                                   multi_row,
                                                   variant,
                                                   num_of_rows,
                                                   num_of_cols,
                                                   owning_party_id,
                                                   owning_party,
                                                   transaction_id,
                                                   process_status,
                                                   transaction_type,
                                                   request_id,
                                                   program_application_id,
                                                   program_id,
                                                   program_update_date,
                                                   created_by,
                                                   creation_date,
                                                   last_updated_by,
                                                   last_update_date,
                                                   last_update_login)
                 VALUES (1,
                         NULL,
                         431,
                         'EGO_ITEMMGMT_GROUP',
                         c_ag.attr_group_int_name,
                         c_ag.icc_name,
                         c_ag.icc_name,
                         DECODE (c_ag.multirow, 'Yes', 'Y', 'N'),
                         'N',
                         NULL,
                         2,
                         -100,
                         NULL,
                         mtl_system_items_interface_s.NEXTVAL,
                         1,
                         'CREATE',
                         NULL,
                         431,
                         NULL,
                         NULL,
                         fnd_profile.VALUE ('USER_ID'),
                         SYSDATE,
                         fnd_profile.VALUE ('USER_ID'),
                         SYSDATE,
                         fnd_profile.VALUE ('LOGIN_ID'));

            INSERT
              INTO ego_attr_groups_dl_interface (set_process_id,
                                                 attr_group_id,
                                                 application_id,
                                                 attr_group_type,
                                                 attr_group_name,
                                                 data_level_id,
                                                 data_level_name,
                                                 defaulting,
                                                 defaulting_name,
                                                 view_privilege_id,
                                                 view_privilege_name,
                                                 user_view_priv_name,
                                                 edit_privilege_id,
                                                 edit_privilege_name,
                                                 user_edit_priv_name,
                                                 pre_business_event_flag,
                                                 business_event_flag,
                                                 transaction_id,
                                                 process_status,
                                                 transaction_type,
                                                 request_id,
                                                 program_application_id,
                                                 program_id,
                                                 program_update_date,
                                                 created_by,
                                                 creation_date,
                                                 last_updated_by,
                                                 last_update_date,
                                                 last_update_login)
            VALUES (1,
                    NULL,
                    431,
                    'EGO_ITEMMGMT_GROUP',
                    c_ag.attr_group_int_name,
                    NULL,
                    'ITEM_LEVEL',
                    'D',
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    mtl_system_items_interface_s.NEXTVAL,
                    1,
                    'CREATE',
                    NULL,
                    431,
                    NULL,
                    NULL,
                    fnd_profile.VALUE ('USER_ID'),
                    SYSDATE,
                    fnd_profile.VALUE ('USER_ID'),
                    SYSDATE,
                    fnd_profile.VALUE ('LOGIN_ID'));

            COMMIT;
         EXCEPTION
            WHEN OTHERS
            THEN
               fnd_file.put_line (
                  fnd_file.LOG,
                     'Error while inserting the Attribute Group name for  '
                  || c_ag.attr_group_int_name
                  || ' error: '
                  || SQLERRM);
         END;
      END LOOP;

      --to load attributes in attribute group
      --truncating the interface table before metadata import
      EXECUTE IMMEDIATE 'truncate table ego.EGO_ATTR_GROUP_COLS_INTF';

      FOR c_aga1
         IN (SELECT DISTINCT icc_name,
                             attr_group_int_name,
                             attr_int_name,
                             wcs_publish,
                             attribute_uom,
                             attribute_label,
                             sequence,
                             database_column,
                             multirow,
                             attribute_data_type
               FROM xxwc_ego_managed_attr_def
              WHERE TRIM (UPPER (icc_name)) IN
                       (SELECT segment1 FROM mtl_item_catalog_groups_b))
      LOOP
         BEGIN
            INSERT
              INTO ego_attr_group_cols_intf (set_process_id,
                                             attr_id,
                                             application_id,
                                             attr_group_type,
                                             attr_group_id,
                                             attr_group_name,
                                             internal_name,
                                             display_name,
                                             description,
                                             sequence,
                                             application_column_name,
                                             data_type,
                                             search_flag,
                                             unique_key_flag,
                                             info_1,
                                             uom_class,
                                             control_level,
                                             view_in_hierarchy_code,
                                             edit_in_hierarchy_code,
                                             customization_level,
                                             read_only_flag,
                                             enabled_flag,
                                             required_flag,
                                             security_enabled_flag,
                                             display_code,
                                             DEFAULT_VALUE,
                                             attribute_code,
                                             maximum_description_len,
                                             concatenation_description_len,
                                             flex_value_set_id,
                                             flex_value_set_name,
                                             transaction_id,
                                             process_status,
                                             transaction_type,
                                             request_id,
                                             program_application_id,
                                             program_id,
                                             program_update_date,
                                             created_by,
                                             creation_date,
                                             last_updated_by,
                                             last_update_date,
                                             last_update_login)
            VALUES (
                      1,
                      NULL,
                      431,
                      'EGO_ITEMMGMT_GROUP',
                      NULL,
                      c_aga1.attr_group_int_name,
                      c_aga1.attr_int_name,
                      c_aga1.attribute_label,
                         c_aga1.attribute_uom
                      || DECODE (c_aga1.wcs_publish, 'Yes', ' [WCS]', NULL),
                      c_aga1.sequence,
                      c_aga1.database_column,
                      DECODE (c_aga1.attribute_data_type,
                              'Number', 'N',
                              'Long', 'A',
                              'C'),
                      'N',
                      DECODE (c_aga1.multirow, 'Yes', 'Y', 'N'),
                      NULL,
                      NULL,
                      1,
                      NULL,
                      NULL,
                      'A',
                      'N',
                      'Y',
                      'N',
                      NULL,
                      'T',
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      DECODE (
                         c_aga1.attribute_data_type,
                         'Value Set', vs_name_func (
                                         TRIM (
                                            UPPER (
                                                  SUBSTR (c_aga1.icc_name,
                                                          1,
                                                          40)
                                               || '-'
                                               || c_aga1.attribute_label)))),
                      mtl_system_items_interface_s.NEXTVAL,
                      1,
                      'CREATE',
                      NULL,
                      431,
                      NULL,
                      NULL,
                      fnd_profile.VALUE ('USER_ID'),
                      SYSDATE,
                      fnd_profile.VALUE ('USER_ID'),
                      SYSDATE,
                      fnd_profile.VALUE ('LOGIN_ID'));

            COMMIT;
         EXCEPTION
            WHEN OTHERS
            THEN
               fnd_file.put_line (
                  fnd_file.LOG,
                     'Error while inserting the Attribute  name for  '
                  || c_aga1.attribute_label
                  || ' error: '
                  || SQLERRM);
         END;
      END LOOP;



      -- to populate AG ICC association
      --truncating the interface table before metadata import
      EXECUTE IMMEDIATE 'truncate table ego.EGO_ATTR_GRPS_ASSOC_INTERFACE';

      FOR c_ag_icc
         IN (SELECT DISTINCT
                    TRIM (UPPER (icc_name)) icc_name, attr_group_int_name
               FROM xxwc_ego_managed_attr_def
              WHERE TRIM (UPPER (icc_name)) IN
                       (SELECT segment1 FROM mtl_item_catalog_groups_b))
      LOOP
         BEGIN
            INSERT
              INTO ego_attr_grps_assoc_interface (item_catalog_group_id,
                                                  item_catalog_name,
                                                  data_level,
                                                  data_level_id,
                                                  attr_group_name,
                                                  attr_group_id,
                                                  set_process_id,
                                                  transaction_id,
                                                  process_status,
                                                  transaction_type,
                                                  request_id,
                                                  program_application_id,
                                                  program_id,
                                                  program_update_date,
                                                  created_by,
                                                  creation_date,
                                                  last_updated_by,
                                                  last_update_date,
                                                  last_update_login,
                                                  association_id)
            VALUES (NULL,
                    c_ag_icc.icc_name,
                    'ITEM_LEVEL',
                    NULL,
                    c_ag_icc.attr_group_int_name,
                    NULL,
                    1,
                    mtl_system_items_interface_s.NEXTVAL,
                    1,
                    'CREATE',
                    NULL,
                    431,
                    NULL,
                    NULL,
                    fnd_profile.VALUE ('USER_ID'),
                    SYSDATE,
                    fnd_profile.VALUE ('USER_ID'),
                    SYSDATE,
                    fnd_profile.VALUE ('LOGIN_ID'),
                    NULL);

            COMMIT;
         EXCEPTION
            WHEN OTHERS
            THEN
               fnd_file.put_line (
                  fnd_file.LOG,
                     'Error while inserting the Attribute group association for  name for  '
                  || c_ag_icc.attr_group_int_name
                  || ' error: '
                  || SQLERRM);
         END;
      END LOOP;

      -- to create the ICC Pages
      --truncating the interface table before metadata import
      EXECUTE IMMEDIATE 'truncate table ego.EGO_PAGES_INTERFACE';

      EXECUTE IMMEDIATE 'truncate table ego.EGO_PAGE_ENTRIES_INTERFACE';

      FOR c_icc_page
         IN (SELECT DISTINCT item_catalog_name icc
               FROM ego_attr_grps_assoc_interface)
      LOOP
         BEGIN
            INSERT INTO ego_pages_interface (set_process_id,
                                             page_id,
                                             display_name,
                                             internal_name,
                                             description,
                                             classification_code,
                                             classification_name,
                                             data_level,
                                             sequence,
                                             transaction_id,
                                             process_status,
                                             transaction_type,
                                             request_id,
                                             program_application_id,
                                             program_id,
                                             program_update_date,
                                             created_by,
                                             creation_date,
                                             last_updated_by,
                                             last_update_date,
                                             last_update_login)
                 VALUES (
                           1,
                           NULL,
                           c_icc_page.icc,
                              'XXWC_'
                           || REPLACE (
                                 REPLACE (
                                    REPLACE (
                                       REPLACE (
                                          REPLACE (
                                             REPLACE (
                                                REPLACE (
                                                   REPLACE (
                                                      REPLACE (
                                                         c_icc_page.icc,
                                                         ' ',
                                                         '_'),
                                                      '/',
                                                      '_'),
                                                   '&',
                                                   '_'),
                                                '-',
                                                '_'),
                                             ',',
                                             '_'),
                                          '''',
                                          ''),
                                       '(',
                                       '_'),
                                    ')',
                                    '_'),
                                 '@',
                                 '_')
                           || '_PG',
                           c_icc_page.icc,
                           NULL,
                           c_icc_page.icc,
                           'ITEM_LEVEL',
                           100,
                           mtl_system_items_interface_s.NEXTVAL,
                           1,
                           'CREATE',
                           NULL,
                           431,
                           NULL,
                           NULL,
                           fnd_profile.VALUE ('USER_ID'),
                           SYSDATE,
                           fnd_profile.VALUE ('USER_ID'),
                           SYSDATE,
                           fnd_profile.VALUE ('LOGIN_ID'));
         EXCEPTION
            WHEN OTHERS
            THEN
               fnd_file.put_line (
                  fnd_file.LOG,
                     'Error while inserting the Item Pages for ICC '
                  || c_icc_page.icc
                  || ' error: '
                  || SQLERRM);
         END;
      END LOOP;

      FOR c_page_ent
         IN (SELECT DISTINCT
                    TRIM (UPPER (icc_name)) icc_name,
                    attr_group_int_name,
                    more_than_1_ag
               FROM xxwc_ego_managed_attr_def
              WHERE TRIM (UPPER (icc_name)) IN
                       (SELECT segment1 FROM mtl_item_catalog_groups_b))
      LOOP
         INSERT INTO ego_page_entries_interface (set_process_id,
                                                 page_id,
                                                 internal_name,
                                                 old_association_id,
                                                 new_association_id,
                                                 old_attr_group_id,
                                                 new_attr_group_id,
                                                 old_attr_group_name,
                                                 new_attr_group_name,
                                                 sequence,
                                                 classification_code,
                                                 classification_name,
                                                 transaction_id,
                                                 process_status,
                                                 transaction_type,
                                                 request_id,
                                                 program_application_id,
                                                 program_id,
                                                 program_update_date,
                                                 creation_date,
                                                 created_by,
                                                 last_updated_by,
                                                 last_update_date,
                                                 last_update_login)
              VALUES (
                        1,
                        NULL,
                           'XXWC_'
                        || REPLACE (
                              REPLACE (
                                 REPLACE (
                                    REPLACE (
                                       REPLACE (
                                          REPLACE (
                                             REPLACE (
                                                REPLACE (
                                                   REPLACE (
                                                      c_page_ent.icc_name,
                                                      ' ',
                                                      '_'),
                                                   '/',
                                                   '_'),
                                                '&',
                                                '_'),
                                             '-',
                                             '_'),
                                          ',',
                                          '_'),
                                       '''',
                                       ''),
                                    '(',
                                    '_'),
                                 ')',
                                 '_'),
                              '@',
                              '_')
                        || '_PG',
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        c_page_ent.attr_group_int_name,
                        NULL,
                        DECODE (c_page_ent.more_than_1_ag, 2, 200, 100),
                        NULL,
                        c_page_ent.icc_name,
                        mtl_system_items_interface_s.NEXTVAL,
                        1,
                        'CREATE',
                        NULL,
                        431,
                        NULL,
                        NULL,
                        SYSDATE,
                        fnd_profile.VALUE ('USER_ID'),
                        fnd_profile.VALUE ('USER_ID'),
                        SYSDATE,
                        fnd_profile.VALUE ('LOGIN_ID'));
      END LOOP;



      COMMIT;
      fnd_file.put_line (fnd_file.LOG, 'End of Program');
   END create_metadata;

   /********************************************************************************
                                                         PROGRAM TYPE: PROCEDURE
   NAME: CONVERSION
   PURPOSE: Procedure to convert the item attributes
   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.0     09/23/2012    Rajiv Rathod    Initial creation of the procedure
    ********************************************************************************/

   PROCEDURE conversion (errbuf OUT VARCHAR2, retcode OUT NUMBER)
   AS
      --declaring variables
      l_user_id              NUMBER;
      l_attr_seq             NUMBER := 0;
      l_dc_number_seq        NUMBER := 0;
      l_dc_char_seq          NUMBER := 0;
      l_dc_long_seq          NUMBER := 0;
      l_category_rec         inv_item_category_pub.category_rec_type;
      l_return_status        VARCHAR2 (80);
      l_error_code           NUMBER;
      l_msg_count            NUMBER;
      l_msg_data             VARCHAR2 (80);
      l_out_category_id      NUMBER;
      l_category_set_id      NUMBER;
      l_category_id          NUMBER;
      l_structure_id         NUMBER;
      l_parent_category_id   NUMBER;
      l_request_wait         BOOLEAN;
      l_chr_phase            VARCHAR2 (100);
      l_chr_status           VARCHAR2 (100);
      l_chr_dev_phase        VARCHAR2 (100);
      l_chr_dev_status       VARCHAR2 (100);
      l_chr_msg              VARCHAR2 (1000);
      l_num_request_id       NUMBER;
      l_chr_errbuff          VARCHAR2 (10000);
      l_num_retcode          NUMBER;
      i                      NUMBER;
      j                      NUMBER;
      l_row_identifier       NUMBER := 0;
   BEGIN
      fnd_file.put_line (fnd_file.LOG, 'Beginning of Program');

      --apps initialize
      --block to get the user
      BEGIN
         SELECT user_id
           INTO l_user_id
           FROM fnd_user
          WHERE user_name = 'XXWC_INT_SUPPLYCHAIN';
      EXCEPTION
         WHEN OTHERS
         THEN
            fnd_file.put_line (
               fnd_file.LOG,
               'Unable to find user XXWC_INT_SUPPLYCHAIN ' || SQLERRM);
      END;

      fnd_global.apps_initialize (user_id        => l_user_id,
                                  resp_id        => 50296,
                                  resp_appl_id   => 431);

      BEGIN
         SELECT f.id_flex_num
           INTO l_category_rec.structure_id
           FROM fnd_id_flex_structures f
          WHERE f.id_flex_structure_code = 'XXWC_WEB_HIERARCHY';
      EXCEPTION
         WHEN OTHERS
         THEN
            fnd_file.put_line (
               fnd_file.LOG,
               'structure XXWC.XXWC_EGO_WEB_HIERARCHY not found ');
      END;

      fnd_file.put_line (fnd_file.LOG,
                         'Begin of category conversion program');

      FOR i
         IN (SELECT SUBSTR (TRIM (child), 1, 40) child, child description
               FROM xxwc.xxwc_ego_web_hierarchy)
      LOOP
         l_category_rec.segment1 := UPPER (i.child);
         l_category_rec.description := i.description;

         l_category_rec.web_status := 'N';

         l_category_rec.supplier_enabled_flag := 'Y';



         inv_item_category_pub.create_category (
            p_api_version     => 1.0,
            p_init_msg_list   => fnd_api.g_false,
            p_commit          => fnd_api.g_true,
            x_return_status   => l_return_status,
            x_errorcode       => l_error_code,
            x_msg_count       => l_msg_count,
            x_msg_data        => l_msg_data,
            p_category_rec    => l_category_rec,
            x_category_id     => l_out_category_id);
         COMMIT;

         IF l_return_status = fnd_api.g_ret_sts_success
         THEN
            NULL;
         ELSE
            fnd_file.put_line (
               fnd_file.LOG,
                  'Creation of Item Category Failed for: '
               || l_category_rec.segment1
               || ' with the error :'
               || l_error_code);
         END IF;
      END LOOP;

      BEGIN
         SELECT mcs_tl.category_set_id
           INTO l_category_set_id
           FROM mtl_category_sets_tl mcs_tl
          WHERE mcs_tl.category_set_name = 'WC Web Hierarchy';
      EXCEPTION
         WHEN OTHERS
         THEN
            fnd_file.put_line (fnd_file.LOG,
                               'category set WC Web Hierarchy not found ');
      END;

      BEGIN
         SELECT f.id_flex_num
           INTO l_structure_id
           FROM fnd_id_flex_structures f
          WHERE f.id_flex_structure_code = 'XXWC_WEB_HIERARCHY';
      EXCEPTION
         WHEN OTHERS
         THEN
            fnd_file.put_line (
               fnd_file.LOG,
               'structure XXWC.XXWC_EGO_WEB_HIERARCHY not found ');
      END;

      FOR i
         IN (SELECT SUBSTR (TRIM (child), 1, 40) child, TRIM (parent) parent
               FROM xxwc.xxwc_ego_web_hierarchy)
      LOOP
         BEGIN
            l_category_id := NULL;
            l_parent_category_id := NULL;

            SELECT mcb.category_id
              INTO l_category_id
              FROM mtl_categories_b mcb
             WHERE     mcb.segment1 = UPPER (i.child)
                   AND mcb.structure_id = l_structure_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               NULL;
         END;

         BEGIN
            SELECT mcb.category_id
              INTO l_parent_category_id
              FROM mtl_categories_b mcb
             WHERE     mcb.segment1 = UPPER (i.parent)
                   AND mcb.structure_id = l_structure_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               NULL;
         END;



         inv_item_category_pub.create_valid_category (
            p_api_version          => 1.0,
            p_init_msg_list        => fnd_api.g_false,
            p_commit               => fnd_api.g_true,
            x_return_status        => l_return_status,
            x_errorcode            => l_error_code,
            x_msg_count            => l_msg_count,
            x_msg_data             => l_msg_data,
            p_category_set_id      => l_category_set_id,
            p_category_id          => l_category_id,
            p_parent_category_id   => l_parent_category_id);

         IF l_return_status = fnd_api.g_ret_sts_success
         THEN
            COMMIT;
            NULL;
         ELSE
            fnd_file.put_line (
               fnd_file.LOG,
                  'Create Valid Category Failed with the error :'
               || l_error_code);
         END IF;
      END LOOP;

      COMMIT;
      fnd_file.put_line (fnd_file.LOG, 'End of category assignment program');

      fnd_file.put_line (fnd_file.LOG,
                         'Begin of icc adn web hierarchy association');

      FOR i IN (SELECT item_number,
                       icc,
                       TRIM (SUBSTR (web_hierarchy, 1, 40)) web_hierarchy,
                       manuf_part_no
                  FROM xxwc.xxwc_ego_icc_sku_asso)
      LOOP
         INSERT INTO mtl_system_items_interface (item_number,
                                                 set_process_id,
                                                 created_by,
                                                 creation_date,
                                                 last_update_date,
                                                 last_updated_by,
                                                 transaction_type,
                                                 process_flag,
                                                 organization_id,
                                                 item_catalog_group_name)
              VALUES ( (i.item_number),
                      1,
                      l_user_id,
                      SYSDATE,
                      SYSDATE,
                      l_user_id,
                      'UPDATE',
                      1,
                      222,
                      TRIM (UPPER (SUBSTR (i.icc, 1, 40))));

         INSERT INTO mtl_item_categories_interface (item_number,
                                                    set_process_id,
                                                    created_by,
                                                    creation_date,
                                                    last_update_date,
                                                    last_updated_by,
                                                    transaction_type,
                                                    process_flag,
                                                    organization_id,
                                                    category_set_name,
                                                    category_name,
                                                    transaction_id)
              VALUES (TRIM (i.item_number),
                      1,
                      l_user_id,
                      SYSDATE,
                      SYSDATE,
                      l_user_id,
                      'CREATE',
                      1,
                      222,
                      'WC Web Hierarchy',
                      UPPER (TRIM (i.web_hierarchy)),
                      mtl_system_items_interface_s.NEXTVAL);
      END LOOP;

      COMMIT;


      l_num_request_id :=
         apps.fnd_request.submit_request ('INV',
                                          'INCOIN',
                                          NULL,
                                          SYSDATE,
                                          FALSE,
                                          222,
                                          '1',
                                          '1',
                                          '1',
                                          '2',
                                          1 --process set                                                             ,
                                           ,
                                          '2');
      COMMIT;

      l_request_wait :=
         fnd_concurrent.wait_for_request (l_num_request_id,
                                          30,          -- wait 30 sec interval
                                          99999,              -- max wait time
                                          l_chr_phase,
                                          l_chr_status,
                                          l_chr_dev_phase,
                                          l_chr_dev_status,
                                          l_chr_msg);

      fnd_file.put_line (fnd_file.LOG,
                         'End of icc and web hierarchy association');

      fnd_file.put_line (fnd_file.LOG,
                         'Begin of TOP level attribute data value upload');

      /*  UPDATE   XXWC.XXWC_EGO_ATTRIBUTE_VALUES a
           SET   process_flag = 'N',
                 Error_message = 'Item does not exist in PIM'
         WHERE   NOT EXISTS (SELECT   1
                               FROM   mtl_system_items_b
                              WHERE   segment1 = a.item_number);

        COMMIT;



        UPDATE   XXWC.XXWC_EGO_ATTRIBUTE_VALUES a
           SET   process_flag = 'N',
                 Error_message = Error_message || ' Item does not exist in ICC'
         WHERE   NOT EXISTS
                    (SELECT   1
                       FROM   mtl_system_items_b b, mtl_item_catalog_groups_b c
                      WHERE   b.segment1 = a.item_number
                              AND b.item_catalog_group_id =
                                    c.item_catalog_group_id
                              AND ROWNUM = 1);

        COMMIT;

        UPDATE   XXWC.XXWC_EGO_ATTRIBUTE_VALUES a
           SET   attribute_group =
                    (SELECT   attr_group_int_name
                       FROM   XXWC_EGO_MANAGED_ATTR_DEF b
                      WHERE       b.icc_name = a.icc
                              AND b.ATTRIBUTE_LABEL = a.attribute_lable
                              AND ROWNUM = 1),
                 ATTRIBUTE =
                    XXWC_EGO_WC_METADATA_PKG.attr_name_func (attribute_lable);

        COMMIT;

        UPDATE   XXWC.XXWC_EGO_ATTRIBUTE_VALUES
           SET   data_type =
                    (SELECT   data_type_code
                       FROM   ego_attrs_v
                      WHERE   attr_group_name || attr_name =
                                 attribute_group || ATTRIBUTE)
         WHERE   attribute_group IS NOT NULL;

        COMMIT;

        FOR j
        IN (SELECT   DISTINCT item_number item_number
              FROM   XXWC.XXWC_EGO_ATTRIBUTE_VALUES
             WHERE   process_flag IS NULL AND attribute_group IS NOT NULL)
        LOOP
           l_row_identifier := l_row_identifier + 1;

           FOR i
           IN (  SELECT   DISTINCT item_number,
                                   icc,
                                   attribute_lable,
                                   attribute_value,
                                   data_type,
                                   attribute_group,
                                   ATTRIBUTE
                   FROM   XXWC.XXWC_EGO_ATTRIBUTE_VALUES a
                  WHERE   a.item_number = j.item_number AND process_flag IS NULL
               ORDER BY   1, 2)
           LOOP
              BEGIN
                 INSERT INTO ego.ego_itm_usr_attr_intrfc (
                                                             process_status,
                                                             data_set_id,
                                                             item_number,
                                                             row_identifier,
                                                             attr_group_int_name,
                                                             attr_int_name,
                                                             attr_value_str,
                                                             attr_value_num,
                                                             attr_disp_value,
                                                             transaction_type,
                                                             organization_id,
                                                             created_by,
                                                             creation_date,
                                                             last_updated_by,
                                                             last_update_date,
                                                             last_update_login,
                                                             attr_group_type,
                                                             interface_table_unique_id
                            )
                   VALUES   (
                                1,
                                1,
                                TRIM (i.item_number),
                                l_row_identifier,
                                i.attribute_group,
                                i.ATTRIBUTE,
                                TRIM(DECODE (i.data_type,
                                             'C', i.attribute_value,
                                             'A', i.attribute_value,
                                             NULL)),
                                TRIM(DECODE (i.data_type,
                                             'N', i.attribute_value,
                                             NULL)),
                                NULL,
                                'SYNC',
                                222,
                                fnd_profile.VALUE ('USER_ID'),
                                SYSDATE,
                                fnd_profile.VALUE ('USER_ID'),
                                SYSDATE,
                                fnd_profile.VALUE ('LOGIN_ID'),
                                'EGO_ITEMMGMT_GROUP',
                                mtl_system_items_interface_s.NEXTVAL
                            );
              EXCEPTION
                 WHEN OTHERS
                 THEN
                    DBMS_OUTPUT.put_line(   'Item Number '
                                         || j.item_number
                                         || i.attribute_value
                                         || SQLERRM);
              END;

              COMMIT;
           END LOOP;

           l_chr_errbuff := NULL;
           l_num_retcode := NULL;
           apps.ego_item_user_attrs_cp_pub.process_item_user_attrs_data (
              errbuf                           => l_chr_errbuff,
              retcode                          => l_num_retcode,
              p_data_set_id                    => 1,
              p_debug_level                    => 0,
              p_purge_successful_lines         => apps.fnd_api.g_false,
              p_initialize_error_handler       => apps.fnd_api.g_true,
              p_validate_only                  => apps.fnd_api.g_false,
              p_ignore_security_for_validate   => apps.fnd_api.g_false
           );
           COMMIT;

           IF (l_chr_errbuff IS NOT NULL)
           THEN
              fnd_file.put_line (
                 fnd_file.LOG,
                    'Item Number '
                 || j.item_number
                 || ' error  message '
                 || l_chr_errbuff
              );
           END IF;
        END LOOP;

        fnd_file.put_line (fnd_file.LOG,
                           'end of TOP level attribute data value upload');
        fnd_file.put_line (fnd_file.LOG,
                           'Begin of item level attribute data value upload');

        UPDATE   XXWC.XXWC_EGO_ATTRIBUTE_ITEM_VALUES a
           SET   process_flag = 'N',
                 Error_message = 'Item does not exist in PIM'
         WHERE   NOT EXISTS (SELECT   1
                               FROM   mtl_system_items_b
                              WHERE   segment1 = a.item_number);



        UPDATE   XXWC.XXWC_EGO_ATTRIBUTE_ITEM_VALUES a
           SET   process_flag = 'N',
                 Error_message =
                    Error_message
                    || ' Item does not exist in ICC provided in Data file'
         WHERE   NOT EXISTS
                    (SELECT   1
                       FROM   mtl_system_items_b b, mtl_item_catalog_groups_b c
                      WHERE   b.segment1 = a.item_number
                              AND b.item_catalog_group_id =
                                    c.item_catalog_group_id
                              AND UPPER (icc) = c.segment1
                              AND ROWNUM = 1);

        UPDATE   XXWC.XXWC_EGO_ATTRIBUTE_ITEM_VALUES a
           SET   attribute_group =
                    (SELECT   attr_group_int_name
                       FROM   XXWC_EGO_MANAGED_ATTR_DEF b
                      WHERE       b.icc_name = a.icc
                              AND b.ATTRIBUTE_LABEL = a.attribute_lable
                              AND ROWNUM = 1),
                 ATTRIBUTE =
                    XXWC_EGO_WC_METADATA_PKG.attr_name_func (attribute_lable);

        -- UPDATE   XXWC.XXWC_EGO_ATTRIBUTE_ITEM_VALUES
           --                                                 SET   data_type =
           --          (SELECT   data_type_code
            --            FROM   ego_attrs_v
            --           WHERE   attr_group_name || attr_name =
             --                     attribute_group || ATTRIBUTE);

        UPDATE   XXWC.XXWC_EGO_ATTRIBUTE_ITEM_VALUES
           SET   data_type =
                    (SELECT   DECODE (attribute_data_type,
                                      'Long', 'A',
                                      'String', 'C',
                                      'Numeric', 'N',
                                      'C')
                       FROM   XXWC_EGO_MANAGED_ATTR_DEF
                      WHERE   attr_group_int_name || attr_int_name =
                                 attribute_group || ATTRIBUTE)
         WHERE   attribute_group IS NOT NULL;

        COMMIT;

        FOR j
        IN (SELECT   DISTINCT item_number item_number
              FROM   XXWC.XXWC_EGO_ATTRIBUTE_ITEM_VALUES
             WHERE   process_flag IS NULL AND attribute_group IS NOT NULL)
        LOOP
           l_row_identifier := l_row_identifier + 1;

           FOR i
           IN (  SELECT   DISTINCT item_number,
                                   icc,
                                   attribute_lable,
                                   attribute_value,
                                   data_type,
                                   attribute_group,
                                   ATTRIBUTE
                   FROM   XXWC.XXWC_EGO_ATTRIBUTE_ITEM_VALUES a
                  WHERE   a.item_number = j.item_number AND process_flag IS NULL
               ORDER BY   1, 2)
           LOOP
              BEGIN
                 INSERT INTO ego.ego_itm_usr_attr_intrfc (
                                                             process_status,
                                                             data_set_id,
                                                             item_number,
                                                             row_identifier,
                                                             attr_group_int_name,
                                                             attr_int_name,
                                                             attr_value_str,
                                                             attr_value_num,
                                                             attr_disp_value,
                                                             transaction_type,
                                                             organization_id,
                                                             created_by,
                                                             creation_date,
                                                             last_updated_by,
                                                             last_update_date,
                                                             last_update_login,
                                                             attr_group_type,
                                                             interface_table_unique_id
                            )
                   VALUES   (
                                1,
                                2,
                                TRIM (i.item_number),
                                l_row_identifier,
                                i.attribute_group,
                                i.ATTRIBUTE,
                                TRIM(DECODE (i.data_type,
                                             'C', i.attribute_value,
                                             'A', i.attribute_value,
                                             NULL)),
                                TRIM(DECODE (i.data_type,
                                             'N', i.attribute_value,
                                             NULL)),
                                NULL,
                                'SYNC',
                                222,
                                FND_PROFILE.VALUE ('USER_ID'),
                                SYSDATE,
                                FND_PROFILE.VALUE ('USER_ID'),
                                SYSDATE,
                                fnd_profile.VALUE ('LOGIN_ID'),
                                'EGO_ITEMMGMT_GROUP',
                                mtl_system_items_interface_s.NEXTVAL
                            );
              EXCEPTION
                 WHEN OTHERS
                 THEN
                    NULL;
              END;

              COMMIT;
           END LOOP;

           l_chr_errbuff := NULL;
           l_num_retcode := NULL;
           apps.ego_item_user_attrs_cp_pub.process_item_user_attrs_data (
              errbuf                           => l_chr_errbuff,
              retcode                          => l_num_retcode,
              p_data_set_id                    => 2,
              p_debug_level                    => 0,
              p_purge_successful_lines         => apps.fnd_api.g_false,
              p_initialize_error_handler       => apps.fnd_api.g_true,
              p_validate_only                  => apps.fnd_api.g_false,
              p_ignore_security_for_validate   => apps.fnd_api.g_false
           );
           COMMIT;
        END LOOP;

        fnd_file.put_line (fnd_file.LOG,
                           'end of Item level attribute data value upload');
        fnd_file.put_line (fnd_file.LOG, 'Begin of features and benifets');

        UPDATE   XXWC.XXWC_EGO_FEATURES_BENEFIT a
           SET   process_flag = 'N',
                 Error_message = 'Item does not exist in PIM'
         WHERE   NOT EXISTS (SELECT   1
                               FROM   mtl_system_items_b
                              WHERE   segment1 = a.item_number);



        UPDATE   XXWC.XXWC_EGO_FEATURES_BENEFIT a
           SET   process_flag = 'N',
                 Error_message = Error_message || ' Item does not exist in ICC'
         WHERE   NOT EXISTS
                    (SELECT   1
                       FROM   mtl_system_items_b b, mtl_item_catalog_groups_b c
                      WHERE   b.segment1 = a.item_number
                              AND b.item_catalog_group_id =
                                    c.item_catalog_group_id
                              AND b.organization_id = 222
                              AND ROWNUM = 1);

        COMMIT;

        FOR j IN (SELECT   DISTINCT item_number item_number
                    FROM   XXWC.XXWC_EGO_FEATURES_BENEFIT
                   WHERE   process_flag IS NULL)
        LOOP
           FOR i
           IN (  SELECT   DISTINCT
                          item_number, sequence_no, TRIM (BENEFITS) BENEFITS
                   FROM   XXWC.XXWC_EGO_FEATURES_BENEFIT a
                  WHERE   a.item_number = j.item_number AND process_flag IS NULL
               ORDER BY   1, 2)
           LOOP
              INSERT INTO ego.ego_itm_usr_attr_intrfc (
                                                          process_status,
                                                          data_set_id,
                                                          item_number,
                                                          row_identifier,
                                                          attr_group_int_name,
                                                          attr_int_name,
                                                          attr_value_str,
                                                          attr_disp_value,
                                                          transaction_type,
                                                          organization_id,
                                                          created_by,
                                                          creation_date,
                                                          last_updated_by,
                                                          last_update_date,
                                                          last_update_login,
                                                          attr_group_type,
                                                          interface_table_unique_id
                         )
                VALUES   (1,
                          3,
                          TRIM (i.item_number),
                          mtl_system_items_interface_s.NEXTVAL,
                          'XXWC_FEATURES_BENEFITS_AG',
                          'XXWC_FEATURES_BENEFITS_ATTR',
                          TRIM (SUBSTR (i.BENEFITS, 1, 1000)),
                          NULL,
                          'SYNC',
                          222,
                          fnd_profile.VALUE ('USER_ID'),
                          SYSDATE,
                          fnd_profile.VALUE ('USER_ID'),
                          SYSDATE,
                          fnd_profile.VALUE ('LOGIN_ID'),
                          'EGO_ITEMMGMT_GROUP',
                          mtl_system_items_interface_s.NEXTVAL);

              COMMIT;
           END LOOP;

           l_chr_errbuff := NULL;
           l_num_retcode := NULL;
           apps.ego_item_user_attrs_cp_pub.process_item_user_attrs_data (
              errbuf                           => l_chr_errbuff,
              retcode                          => l_num_retcode,
              p_data_set_id                    => 3,
              p_debug_level                    => 0,
              p_purge_successful_lines         => apps.fnd_api.g_false,
              p_initialize_error_handler       => apps.fnd_api.g_true,
              p_validate_only                  => apps.fnd_api.g_false,
              p_ignore_security_for_validate   => apps.fnd_api.g_false
           );
           COMMIT;

           IF (l_chr_errbuff IS NOT NULL)
           THEN
              fnd_file.put_line (
                 fnd_file.LOG,
                    'Item Number '
                 || j.item_number
                 || ' error  message '
                 || l_chr_errbuff
              );
           END IF;
        END LOOP;

        FOR i IN (SELECT   *
                    FROM   ego_itm_usr_attr_intrfc
                   WHERE   data_set_id = 3 AND process_status = 3)
        LOOP
           UPDATE   ego.ego_itm_usr_attr_intrfc
              SET   process_status = 1
            WHERE       data_set_id = 3
                    AND process_status = 3
                    AND item_number = i.item_number
                    AND attr_int_name = i.attr_int_name
                    AND row_identifier = i.row_identifier;

           COMMIT;
           l_chr_errbuff := NULL;
           l_num_retcode := NULL;
           apps.ego_item_user_attrs_cp_pub.process_item_user_attrs_data (
              errbuf                           => l_chr_errbuff,
              retcode                          => l_num_retcode,
              p_data_set_id                    => 3,
              p_debug_level                    => 0,
              p_purge_successful_lines         => apps.fnd_api.g_false,
              p_initialize_error_handler       => apps.fnd_api.g_true,
              p_validate_only                  => apps.fnd_api.g_false,
              p_ignore_security_for_validate   => apps.fnd_api.g_false
           );
           COMMIT;
        END LOOP;  */

      fnd_file.put_line (fnd_file.LOG, 'End of features and BENEFITS');
   END conversion;

   /********************************************************************************
      PROGRAM TYPE: PROCEDURE
NAME: LOAD_FEA_BEN
PURPOSE: Procedure to convert the load features and Benefits
HISTORY
===============================================================================
VERSION DATE          AUTHOR(S)       DESCRIPTION
------- -----------   --------------- -----------------------------------------
1.0     09/23/2012    Rajiv Rathod    Initial creation of the procedure
 ********************************************************************************/
   PROCEDURE load_fea_ben (errbuf OUT VARCHAR2, retcode OUT NUMBER)
   IS
      l_chr_errbuff      VARCHAR2 (10000);
      l_num_retcode      NUMBER;
      l_user_id          NUMBER;
      l_row_identifier   NUMBER;
   BEGIN
      BEGIN
         SELECT user_id
           INTO l_user_id
           FROM fnd_user
          WHERE user_name = 'XXWC_INT_SUPPLYCHAIN';
      EXCEPTION
         WHEN OTHERS
         THEN
            NULL;
      END;

      fnd_global.apps_initialize (user_id        => l_user_id,
                                  resp_id        => 50296,
                                  resp_appl_id   => 431);

      UPDATE xxwc.xxwc_ego_features_benefit a
         SET process_flag = 'N', error_message = 'Item does not exist in PIM'
       WHERE NOT EXISTS
                (SELECT 1
                   FROM mtl_system_items_b
                  WHERE segment1 = a.item_number);



      UPDATE xxwc.xxwc_ego_features_benefit a
         SET process_flag = 'N',
             error_message = error_message || ' Item does not exist in ICC'
       WHERE NOT EXISTS
                    (SELECT 1
                       FROM mtl_system_items_b b, mtl_item_catalog_groups_b c
                      WHERE     b.segment1 = a.item_number
                            AND b.item_catalog_group_id =
                                   c.item_catalog_group_id
                            AND b.organization_id = 222
                            AND ROWNUM = 1);

      COMMIT;

      FOR j IN (SELECT DISTINCT item_number item_number
                  FROM xxwc.xxwc_ego_features_benefit
                 WHERE process_flag IS NULL)
      LOOP
         FOR i
            IN (  SELECT DISTINCT
                         item_number, sequence_no, TRIM (benefits) benefits
                    FROM xxwc.xxwc_ego_features_benefit a
                   WHERE a.item_number = j.item_number AND process_flag IS NULL
                ORDER BY 1, 2)
         LOOP
            INSERT
              INTO ego.ego_itm_usr_attr_intrfc (process_status,
                                                data_set_id,
                                                item_number,
                                                row_identifier,
                                                attr_group_int_name,
                                                attr_int_name,
                                                attr_value_str,
                                                attr_disp_value,
                                                transaction_type,
                                                organization_id,
                                                created_by,
                                                creation_date,
                                                last_updated_by,
                                                last_update_date,
                                                last_update_login,
                                                attr_group_type,
                                                interface_table_unique_id)
            VALUES (1,
                    3,
                    TRIM (i.item_number),
                    mtl_system_items_interface_s.NEXTVAL,
                    'XXWC_FEATURES_BENEFITS_AG',
                    'XXWC_FEATURES_BENEFITS_ATTR',
                    TRIM (SUBSTR (i.benefits, 1, 1000)),
                    NULL,
                    'SYNC',
                    222,
                    l_user_id,
                    SYSDATE,
                    l_user_id,
                    SYSDATE,
                    l_user_id,
                    'EGO_ITEMMGMT_GROUP',
                    mtl_system_items_interface_s.NEXTVAL);

            COMMIT;
         END LOOP;

         l_chr_errbuff := NULL;
         l_num_retcode := NULL;
         apps.ego_item_user_attrs_cp_pub.process_item_user_attrs_data (
            errbuf                           => l_chr_errbuff,
            retcode                          => l_num_retcode,
            p_data_set_id                    => 3,
            p_debug_level                    => 0,
            p_purge_successful_lines         => apps.fnd_api.g_false,
            p_initialize_error_handler       => apps.fnd_api.g_true,
            p_validate_only                  => apps.fnd_api.g_false,
            p_ignore_security_for_validate   => apps.fnd_api.g_false);
         COMMIT;
      END LOOP;

      FOR i IN (SELECT *
                  FROM ego_itm_usr_attr_intrfc
                 WHERE data_set_id = 3 AND process_status = 3)
      LOOP
         UPDATE ego.ego_itm_usr_attr_intrfc
            SET process_status = 1
          WHERE     data_set_id = 3
                AND process_status = 3
                AND item_number = i.item_number
                AND attr_int_name = i.attr_int_name
                AND row_identifier = i.row_identifier;

         COMMIT;
         l_chr_errbuff := NULL;
         l_num_retcode := NULL;
         apps.ego_item_user_attrs_cp_pub.process_item_user_attrs_data (
            errbuf                           => l_chr_errbuff,
            retcode                          => l_num_retcode,
            p_data_set_id                    => 3,
            p_debug_level                    => 0,
            p_purge_successful_lines         => apps.fnd_api.g_false,
            p_initialize_error_handler       => apps.fnd_api.g_true,
            p_validate_only                  => apps.fnd_api.g_false,
            p_ignore_security_for_validate   => apps.fnd_api.g_false);
         COMMIT;
      END LOOP;
   END load_fea_ben;

   /********************************************************************************
     PROGRAM TYPE: PROCEDURE
  NAME: LOAD_ITEM_VAL_ATTR
  PURPOSE: Procedure to load Item level attributes
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     09/23/2012    Rajiv Rathod    Initial creation of the procedure
   ********************************************************************************/
   PROCEDURE load_item_val_attr (errbuf OUT VARCHAR2, retcode OUT NUMBER)
   IS
      l_chr_errbuff      VARCHAR2 (10000);
      l_num_retcode      NUMBER;
      l_user_id          NUMBER;
      l_row_identifier   NUMBER := 0;
   BEGIN
      BEGIN
         SELECT user_id
           INTO l_user_id
           FROM fnd_user
          WHERE user_name = 'XXWC_INT_SUPPLYCHAIN';
      EXCEPTION
         WHEN OTHERS
         THEN
            NULL;
      END;

      fnd_global.apps_initialize (user_id        => l_user_id,
                                  resp_id        => 50296,
                                  resp_appl_id   => 431);

      UPDATE xxwc.xxwc_ego_attribute_item_values a
         SET process_flag = 'N', error_message = 'Item does not exist in PIM'
       WHERE NOT EXISTS
                (SELECT 1
                   FROM mtl_system_items_b
                  WHERE segment1 = a.item_number);

      COMMIT;



      UPDATE xxwc.xxwc_ego_attribute_item_values a
         SET process_flag = 'N',
             error_message =
                   error_message
                || ' Item does not exist in ICC provided in Data file'
       WHERE NOT EXISTS
                    (SELECT 1
                       FROM mtl_system_items_b b, mtl_item_catalog_groups_b c
                      WHERE     b.segment1 = a.item_number
                            AND b.item_catalog_group_id =
                                   c.item_catalog_group_id
                            AND UPPER (icc) = c.segment1
                            AND ROWNUM = 1);

      COMMIT;

      UPDATE xxwc.xxwc_ego_attribute_item_values a
         SET attribute_group =
                (SELECT attr_group_int_name
                   FROM (SELECT segment1 icc_name,
                                attr_group_name attr_group_int_name,
                                attr_display_name attribute_label
                           FROM xxwc.xxwc_ego_attr_lookup_tbl
                         UNION
                         SELECT UPPER (icc_name) icc_name,
                                attr_group_int_name,
                                attribute_label
                           FROM xxwc.xxwc_ego_managed_attr_def) b
                  WHERE     b.icc_name = UPPER (a.icc)
                        AND b.attribute_label = a.attribute_lable
                        AND ROWNUM = 1),
             attribute =
                xxwc_ego_wc_metadata_pkg.attr_name_func (attribute_lable);

      COMMIT;

      UPDATE xxwc.xxwc_ego_attribute_item_values a
         SET data_type =
                (SELECT data_type_code
                   FROM ego_attrs_v
                  WHERE     attr_group_type = 'EGO_ITEMMGMT_GROUP'
                        AND attr_group_name = a.attribute_group
                        AND attr_name = a.attribute
                        AND ROWNUM = 1)
       WHERE attribute_group IS NOT NULL;

      COMMIT;

      FOR j IN (SELECT DISTINCT item_number item_number
                  FROM xxwc.xxwc_ego_attribute_item_values
                 WHERE process_flag IS NULL AND attribute_group IS NOT NULL)
      LOOP
         l_row_identifier := l_row_identifier + 1;

         FOR i
            IN (  SELECT DISTINCT item_number,
                                  icc,
                                  attribute_lable,
                                  attribute_value,
                                  data_type,
                                  attribute_group,
                                  attribute
                    FROM xxwc.xxwc_ego_attribute_item_values a
                   WHERE a.item_number = j.item_number AND process_flag IS NULL
                ORDER BY 1, 2)
         LOOP
            BEGIN
               INSERT
                 INTO ego.ego_itm_usr_attr_intrfc (process_status,
                                                   data_set_id,
                                                   item_number,
                                                   row_identifier,
                                                   attr_group_int_name,
                                                   attr_int_name,
                                                   attr_value_str,
                                                   attr_value_num,
                                                   attr_disp_value,
                                                   transaction_type,
                                                   organization_id,
                                                   created_by,
                                                   creation_date,
                                                   last_updated_by,
                                                   last_update_date,
                                                   last_update_login,
                                                   attr_group_type,
                                                   interface_table_unique_id)
               VALUES (
                         1,
                         2,
                         TRIM (i.item_number),
                         l_row_identifier,
                         i.attribute_group,
                         i.attribute,
                         TRIM (
                            DECODE (i.data_type,
                                    'C', i.attribute_value,
                                    'A', i.attribute_value,
                                    NULL)),
                         TRIM (
                            DECODE (i.data_type,
                                    'N', i.attribute_value,
                                    NULL)),
                         NULL,
                         'SYNC',
                         222,
                         fnd_profile.VALUE ('USER_ID'),
                         SYSDATE,
                         fnd_profile.VALUE ('USER_ID'),
                         SYSDATE,
                         fnd_profile.VALUE ('LOGIN_ID'),
                         'EGO_ITEMMGMT_GROUP',
                         mtl_system_items_interface_s.NEXTVAL);
            EXCEPTION
               WHEN OTHERS
               THEN
                  NULL;
            END;

            COMMIT;
         END LOOP;

         l_chr_errbuff := NULL;
         l_num_retcode := NULL;
         apps.ego_item_user_attrs_cp_pub.process_item_user_attrs_data (
            errbuf                           => l_chr_errbuff,
            retcode                          => l_num_retcode,
            p_data_set_id                    => 2,
            p_debug_level                    => 0,
            p_purge_successful_lines         => apps.fnd_api.g_false,
            p_initialize_error_handler       => apps.fnd_api.g_true,
            p_validate_only                  => apps.fnd_api.g_false,
            p_ignore_security_for_validate   => apps.fnd_api.g_false);
         COMMIT;
      END LOOP;
   END load_item_val_attr;

   /********************************************************************************
      PROGRAM TYPE: PROCEDURE
   NAME: LOAD_TOP_ATTR
   PURPOSE: Procedure to load top level attributes
   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.0     09/23/2012    Rajiv Rathod    Initial creation of the procedure
    ********************************************************************************/
   PROCEDURE load_top_attr (errbuf OUT VARCHAR2, retcode OUT NUMBER)
   IS
      l_chr_errbuff      VARCHAR2 (10000);
      l_num_retcode      NUMBER;
      L_USER_ID          NUMBER;
      l_row_identifier   NUMBER := 0;
   BEGIN
      BEGIN
         SELECT user_id
           INTO l_user_id
           FROM fnd_user
          WHERE user_name = 'XXWC_INT_SUPPLYCHAIN';
      EXCEPTION
         WHEN OTHERS
         THEN
            NULL;
      END;

      fnd_global.apps_initialize (user_id        => l_user_id,
                                  resp_id        => 50296,
                                  resp_appl_id   => 431);

      UPDATE xxwc.xxwc_ego_attribute_values a
         SET process_flag = 'N', error_message = 'Item does not exist in PIM'
       WHERE NOT EXISTS
                (SELECT 1
                   FROM mtl_system_items_b
                  WHERE segment1 = a.item_number);

      COMMIT;



      UPDATE xxwc.xxwc_ego_attribute_values a
         SET process_flag = 'N',
             error_message = error_message || ' Item does not exist in ICC'
       WHERE NOT EXISTS
                    (SELECT 1
                       FROM mtl_system_items_b b, mtl_item_catalog_groups_b c
                      WHERE     b.segment1 = a.item_number
                            AND b.item_catalog_group_id =
                                   c.item_catalog_group_id
                            AND ROWNUM = 1);

      COMMIT;

      UPDATE xxwc.xxwc_ego_attribute_values a
         SET attribute_group =
                (SELECT attr_group_int_name
                   FROM (SELECT SEGMENT1 ICC_NAME,
                                ATTR_GROUP_NAME ATTR_GROUP_INT_NAME,
                                ATTR_DISPLAY_NAME ATTRIBUTE_LABEL
                           FROM XXWC.XXWC_EGO_ATTR_LOOKUP_TBL
                         UNION
                         SELECT UPPER (ICC_NAME) ICC_NAME,
                                ATTR_GROUP_INT_NAME,
                                ATTRIBUTE_LABEL
                           FROM XXWC.XXWC_EGO_MANAGED_ATTR_DEF
                         UNION
                         SELECT a.SEGMENT1 ICC_NAME,
                                c.ATTR_GROUP_NAME ATTR_GROUP_INT_NAME,
                                d.ATTR_DISPLAY_NAME ATTRIBUTE_LABEL
                           FROM MTL_ITEM_CATALOG_GROUPS_B A,
                                EGO_OBJ_AG_ASSOCS_B B,
                                EGO_ATTR_GROUPS_V C,
                                EGO_ATTRS_V D
                          WHERE     A.ITEM_CATALOG_GROUP_ID =
                                       B.CLASSIFICATION_CODE
                                AND B.ATTR_GROUP_ID = C.ATTR_GROUP_ID
                                AND C.ATTR_GROUP_NAME = D.ATTR_GROUP_NAME
                                AND A.SEGMENT1 = 'WHITE CAP MST'
                                AND C.ATTR_GROUP_TYPE = 'EGO_ITEMMGMT_GROUP') B
                  WHERE     b.icc_name = UPPER (a.icc)
                        AND b.ATTRIBUTE_LABEL = a.attribute_lable
                        AND ROWNUM = 1),
             ATTRIBUTE =
                XXWC_EGO_WC_METADATA_PKG.attr_name_func (attribute_lable);

      COMMIT;

      UPDATE xxwc.xxwc_ego_attribute_values a
         SET DATA_TYPE =
                (SELECT DATA_TYPE_CODE
                   FROM EGO_ATTRS_V
                  WHERE     ATTR_GROUP_TYPE = 'EGO_ITEMMGMT_GROUP'
                        AND ATTR_GROUP_NAME = A.ATTRIBUTE_GROUP
                        AND ATTR_NAME = A.ATTRIBUTE
                        AND ROWNUM = 1)
       WHERE attribute_group IS NOT NULL;

      COMMIT;

      FOR J IN (SELECT DISTINCT item_number item_number
                  FROM XXWC.XXWC_EGO_ATTRIBUTE_VALUES
                 WHERE process_flag IS NULL AND attribute_group IS NOT NULL)
      LOOP
         l_row_identifier := l_row_identifier + 1;

         FOR i
            IN (  SELECT DISTINCT item_number,
                                  icc,
                                  attribute_lable,
                                  attribute_value,
                                  data_type,
                                  attribute_group,
                                  attribute
                    FROM xxwc.xxwc_ego_attribute_values a
                   WHERE a.item_number = j.item_number AND process_flag IS NULL
                ORDER BY 1, 2)
         LOOP
            BEGIN
               INSERT
                 INTO ego.ego_itm_usr_attr_intrfc (process_status,
                                                   data_set_id,
                                                   item_number,
                                                   row_identifier,
                                                   attr_group_int_name,
                                                   attr_int_name,
                                                   attr_value_str,
                                                   attr_value_num,
                                                   attr_disp_value,
                                                   transaction_type,
                                                   organization_id,
                                                   created_by,
                                                   creation_date,
                                                   last_updated_by,
                                                   last_update_date,
                                                   last_update_login,
                                                   attr_group_type,
                                                   interface_table_unique_id)
               VALUES (
                         1,
                         1,
                         TRIM (i.item_number),
                         l_row_identifier,
                         i.attribute_group,
                         i.attribute,
                         TRIM (
                            DECODE (i.data_type,
                                    'C', i.attribute_value,
                                    'A', i.attribute_value,
                                    NULL)),
                         TRIM (
                            DECODE (i.data_type,
                                    'N', i.attribute_value,
                                    NULL)),
                         NULL,
                         'SYNC',
                         222,
                         fnd_profile.VALUE ('USER_ID'),
                         SYSDATE,
                         fnd_profile.VALUE ('USER_ID'),
                         SYSDATE,
                         fnd_profile.VALUE ('LOGIN_ID'),
                         'EGO_ITEMMGMT_GROUP',
                         mtl_system_items_interface_s.NEXTVAL);
            EXCEPTION
               WHEN OTHERS
               THEN
                  DBMS_OUTPUT.put_line (
                        'Item Number '
                     || j.item_number
                     || i.attribute_value
                     || SQLERRM);
            END;

            COMMIT;
         END LOOP;

         l_chr_errbuff := NULL;
         l_num_retcode := NULL;
         apps.ego_item_user_attrs_cp_pub.process_item_user_attrs_data (
            errbuf                           => l_chr_errbuff,
            retcode                          => l_num_retcode,
            p_data_set_id                    => 1,
            p_debug_level                    => 0,
            p_purge_successful_lines         => apps.fnd_api.g_false,
            p_initialize_error_handler       => apps.fnd_api.g_true,
            p_validate_only                  => apps.fnd_api.g_false,
            p_ignore_security_for_validate   => apps.fnd_api.g_false);
         COMMIT;

         IF (l_chr_errbuff IS NOT NULL)
         THEN
            fnd_file.put_line (
               fnd_file.LOG,
                  'Item Number '
               || j.item_number
               || ' error  message '
               || l_chr_errbuff);
         END IF;
      END LOOP;

      COMMIT;
   END load_top_attr;
END xxwc_ego_wc_metadata_pkg;
/