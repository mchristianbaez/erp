--
-- XXWC_WSH_BACKORD_TXNS_PKG  (Package Body) 
--
CREATE OR REPLACE PACKAGE BODY APPS.xxwc_wsh_backord_txns_pkg
AS
   /*************************************************************************
    Copyright (c) 2012 Lucidity Consulting Group
    All rights reserved.
   **************************************************************************
     $Header xxwc_wsh_backord_txns_pkg $
     Module Name: xxwc_wsh_backord_txns_pkg.pks

     PURPOSE:   This package is called by the concurrent programs
                XXWC WSH Backordered Transactions Processing

     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.0        01/18/2012  Consuelo Gonzalez       Initial Version
     1.1        10/01/2014  Gopi Damuluri           TMS# 20141001-00248 Multi Org Changes
   **************************************************************************/

   /*************************************************************************
  Procedure : Write_Log

  PURPOSE:   This procedure logs debug message in Concurrent Log file
  Parameter:
         IN
             p_debug_msg      -- Debug Message
************************************************************************/
   PROCEDURE write_log (p_debug_msg IN VARCHAR2)
   IS
   BEGIN
      fnd_file.put_line (fnd_file.LOG, p_debug_msg);
      DBMS_OUTPUT.put_line (p_debug_msg);
   END write_log;

   /*************************************************************************
      Procedure : Write_Error

     PURPOSE:   This procedure logs error message
     Parameter:
            IN
                p_debug_msg      -- Debug Message
   ************************************************************************/
   PROCEDURE write_error (p_debug_msg IN VARCHAR2, p_call_point IN VARCHAR2)
   IS
      l_req_id          NUMBER := fnd_global.conc_request_id;
      l_err_callfrom    VARCHAR2 (75) DEFAULT 'XXWC_WSH_BACKORD_TXNS_PKG';
      l_err_callpoint   VARCHAR2 (75) DEFAULT 'START';
      l_distro_list     VARCHAR2 (75)
                           DEFAULT 'HDSOracleDevelopers@hdsupply.com';
   BEGIN
      xxcus_error_pkg.xxcus_error_main_api (
         p_called_from         => l_err_callfrom
        ,p_calling             => l_err_callpoint
        ,p_request_id          => l_req_id
        ,p_ora_error_msg       => SUBSTR (p_debug_msg, 1, 2000)
        ,p_error_desc          => 'Error running xxwc_wsh_backord_txns_pkg with PROGRAM ERROR'
        ,p_distribution_list   => l_distro_list
        ,p_module              => 'WSH');
   END write_error;

   /*************************************************************************
     Procedure : Process_Subinv_Transf

     PURPOSE:
     Parameter:
            IN

   ************************************************************************/
   PROCEDURE process_subinv_transf (p_delivery_detail_id   IN     NUMBER
                                   ,p_return_status           OUT VARCHAR2)
   IS
      CURSOR cur_backord_ln_stg
      IS
         SELECT x1.ROWID row_id, x1.*
           FROM xxwc_backordered_txns_lns_stg x1
          WHERE     x1.delivery_detail_id = p_delivery_detail_id
                AND x1.status_code IN ('NEW', 'BACKORDERED')
                AND x1.request_id = g_request_id;

      CURSOR cur_backord_sn_stg (
         p_delivery_detail_id    NUMBER
        ,p_mo_line_id            NUMBER
        ,p_transaction_qty       NUMBER)
      IS
         SELECT x1.ROWID row_id, x1.*
           FROM xxwc_backordered_txns_sns_stg x1
          WHERE     x1.delivery_detail_id = p_delivery_detail_id
                AND x1.move_order_line_id = p_mo_line_id
                AND x1.status IN ('NEW', 'BACKORDERED')
                AND x1.request_id = g_request_id
                AND ROWNUM <= p_transaction_qty;

      CURSOR cur_backord_lot_stg (
         p_delivery_detail_id    NUMBER
        ,p_mo_line_id            NUMBER
        ,p_count_lots            NUMBER)
      IS
         SELECT x1.ROWID row_id, x1.*
           FROM xxwc_backordered_txns_sns_stg x1
          WHERE     x1.delivery_detail_id = p_delivery_detail_id
                AND x1.move_order_line_id = p_mo_line_id
                AND x1.status IN ('NEW', 'BACKORDERED')
                AND x1.request_id = g_request_id
                AND ROWNUM <= p_count_lots;

      CURSOR cur_backord_lot_stg_hld (
         p_delivery_detail_id    NUMBER
        ,p_mo_line_id            NUMBER
        ,p_count_lots            NUMBER)
      IS
         SELECT x1.ROWID row_id, x1.*
           FROM xxwc_backordered_txns_sns_stg x1
          WHERE     x1.delivery_detail_id = p_delivery_detail_id
                AND x1.move_order_line_id = p_mo_line_id
                AND x1.status IN ('TRANSFERRED', 'INTERFACED')
                AND x1.request_id = g_request_id
                AND ROWNUM <= p_count_lots;

      l_transaction_header_id      NUMBER;
      l_interface_transaction_id   NUMBER;
      l_count_serials              NUMBER;
      l_transaction_type_id        NUMBER;
      l_transaction_uom            VARCHAR2 (3);
      l_transaction_qty            NUMBER;
      l_gral_transaction_qty       NUMBER;
      l_processing_return          NUMBER;
      l_return_status              VARCHAR2 (240);
      l_msg_count                  NUMBER;
      l_msg_data                   VARCHAR2 (4000);
      l_trans_count                NUMBER;
   BEGIN
      write_log (
            '  3.1. Entering process_subinv_transf for WDD '
         || p_delivery_detail_id);

      FOR c1 IN cur_backord_ln_stg
      LOOP
         EXIT WHEN cur_backord_ln_stg%NOTFOUND;
         l_transaction_header_id := NULL;
         l_interface_transaction_id := NULL;
         l_count_serials := NULL;
         l_transaction_type_id := NULL;
         l_transaction_uom := NULL;
         l_transaction_qty := NULL;
         l_gral_transaction_qty := NULL;

         -- Finding Transaction Type ID
         BEGIN
            SELECT transaction_type_id
              INTO l_transaction_type_id
              FROM mtl_transaction_types
             WHERE transaction_type_name = 'Subinventory Transfer';
         EXCEPTION
            WHEN OTHERS
            THEN
               write_log (
                  '  3.1. Err Could not find transaction type information for Subinventory Transfer');
         END;

         -- Finding Original Move Order Line UOM
         BEGIN
            SELECT mtrl.uom_code
              INTO l_transaction_uom
              FROM mtl_txn_request_lines mtrl
             WHERE line_id = c1.move_order_line_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               write_log (
                     '  3.1. Err Could not find UOM Code from original MO Line for '
                  || c1.move_order_line_id);
         END;

         -- Count Number of ESNs in secondary table
         IF c1.serial_num_status != 'No serial number control'
         THEN                     -- or c1.lot_status != 'No lot control' then
            BEGIN
               SELECT COUNT (*)
                 INTO l_count_serials
                 FROM xxwc_backordered_txns_sns_stg x1
                WHERE     x1.delivery_detail_id = c1.delivery_detail_id
                      AND x1.move_order_line_id = c1.move_order_line_id
                      AND x1.status NOT IN ('ERROR', 'PROCESSED')
                      AND x1.request_id = g_request_id;
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_count_serials := 0;
                  write_log (
                        '  3.1. Err Could not find UOM Code from original MO Line for '
                     || c1.move_order_line_id);
            END;

            IF c1.wdd_requested_qty > 0 AND l_count_serials <= 0
            THEN
               l_transaction_qty := 0;
               l_gral_transaction_qty := 0;
               write_log (
                  '  3.1. Err item is serialized but no serials from original MO available to transact');
            ELSE
               -- 02/28/2012 CG Changed to accomodate movement to Holding and General
               --l_transaction_qty := l_count_serials;
               l_transaction_qty := c1.new_ordered_quantity;
               l_gral_transaction_qty :=
                  c1.wdd_requested_qty - c1.new_ordered_quantity;

               IF l_transaction_qty = 0 AND c1.hold_reserve_flag = 'Y'
               THEN
                  l_transaction_qty := c1.wdd_requested_qty;
                  l_gral_transaction_qty := 0;
               END IF;
            END IF;
         ELSIF c1.lot_status != 'No lot control'
         THEN
            BEGIN
               SELECT COUNT (*)
                 INTO l_count_serials
                 FROM xxwc_backordered_txns_sns_stg x1
                WHERE     x1.delivery_detail_id = c1.delivery_detail_id
                      AND x1.move_order_line_id = c1.move_order_line_id
                      AND x1.status NOT IN ('ERROR', 'PROCESSED')
                      AND x1.request_id = g_request_id;
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_count_serials := 0;
                  write_log (
                        '  3.1. Err Could not find lots for  '
                     || c1.move_order_line_id);
            END;

            IF c1.wdd_requested_qty > 0 AND l_count_serials <= 0
            THEN
               l_transaction_qty := 0;
               l_gral_transaction_qty := 0;
               write_log (
                  '  3.1. Err item is Lot Controlled but no lots found from original MO available to transact');
            ELSE
               l_transaction_qty :=
                  c1.wdd_requested_qty - NVL (c1.wdd_cancellation_qty, 0);
               l_gral_transaction_qty :=
                  c1.wdd_requested_qty - l_transaction_qty;

               IF l_transaction_qty = 0 AND c1.hold_reserve_flag = 'Y'
               THEN
                  l_transaction_qty := c1.wdd_requested_qty;
                  l_gral_transaction_qty := 0;
               END IF;
            END IF;
         ELSE
            -- 02/28/2012 CG Changed to accomodate movement to Holding and General
            --l_transaction_qty := c1.wdd_requested_qty;
            l_transaction_qty := c1.new_ordered_quantity;
            l_gral_transaction_qty :=
               c1.wdd_requested_qty - c1.new_ordered_quantity;

            IF l_transaction_qty = 0 AND c1.hold_reserve_flag = 'Y'
            THEN
               l_transaction_qty := c1.wdd_requested_qty;
               l_gral_transaction_qty := 0;
            END IF;
         END IF;

         write_log (
               '  3.1. Originally request '
            || c1.wdd_requested_qty
            || ' transacting '
            || l_count_serials
            || ' serials');
         write_log (
               '  3.1. To Holding '
            || l_transaction_qty
            || ' - To General '
            || l_gral_transaction_qty);

         IF l_transaction_qty > 0
         THEN
            write_log ('  3.1. Creating transactions for holding...');

            SELECT mtl_material_transactions_s.NEXTVAL
              INTO l_transaction_header_id
              FROM DUAL;

            SELECT mtl_material_transactions_s.NEXTVAL
              INTO l_interface_transaction_id
              FROM DUAL;

            BEGIN
               INSERT
                 INTO mtl_transactions_interface (transaction_interface_id
                                                 ,transaction_header_id
                                                 ,source_code
                                                 ,source_line_id
                                                 ,source_header_id
                                                 ,process_flag
                                                 ,transaction_mode
                                                 ,lock_flag
                                                 ,last_update_date
                                                 ,last_updated_by
                                                 ,creation_date
                                                 ,created_by
                                                 ,transaction_type_id
                                                 ,transaction_date
                                                 ,transaction_uom
                                                 ,inventory_item_id
                                                 ,subinventory_code
                                                 ,organization_id
                                                 ,transfer_subinventory
                                                 ,transfer_organization
                                                 ,transaction_quantity
                                                 ,primary_quantity)
               VALUES (l_interface_transaction_id
                      ,l_transaction_header_id
                      ,'Subinventory Transfer'
                      ,                                          --source code
                       l_interface_transaction_id
                      ,                                       --source line id
                       l_interface_transaction_id
                      ,                                     --source header id
                       1
                      ,                                         --process flag
                       3
                      ,                                     --transaction mode
                       2
                      ,                                            --lock flag
                       SYSDATE
                      ,                                     --last update date
                       0
                      ,                                      --last updated by
                       SYSDATE
                      ,                                         --created date
                       0
                      ,                                           --created by
                       l_transaction_type_id
                      ,                                    -- transaction type
                       SYSDATE
                      ,                                    -- transaction date
                       l_transaction_uom
                      ,                                                 -- uom
                       c1.inventory_item_id
                      ,--inventory item id
                       c1.to_subinventory_code
                      ,-- from subinventory (data pulled from move order lines
                       c1.ship_from_org_id
                      ,c1.holding_subinventory
                      ,--02/27/12 CG c1.from_subinventory_code, -- to subinventory
                       c1.ship_from_org_id
                      ,l_transaction_qty
                      ,l_transaction_qty);

               IF     c1.serial_num_status != 'No serial number control'
                  AND l_count_serials > 0
               THEN
                  FOR c2
                     IN cur_backord_sn_stg (c1.delivery_detail_id
                                           ,c1.move_order_line_id
                                           ,l_transaction_qty)
                  LOOP
                     EXIT WHEN cur_backord_sn_stg%NOTFOUND;

                     BEGIN
                        INSERT
                          INTO mtl_serial_numbers_interface (
                                  transaction_interface_id
                                 ,last_update_date
                                 ,last_updated_by
                                 ,creation_date
                                 ,created_by
                                 ,fm_serial_number
                                 ,to_serial_number)
                        VALUES (l_interface_transaction_id
                               ,SYSDATE
                               ,                            --last update date
                                0
                               ,                             --last updated by
                                SYSDATE
                               ,                                --created date
                                0
                               ,                                     --created
                                c2.serial_number
                               ,c2.serial_number);

                        UPDATE xxwc_backordered_txns_sns_stg
                           SET status = 'INTERFACED'
                         WHERE ROWID = c2.row_id;
                     EXCEPTION
                        WHEN OTHERS
                        THEN
                           write_log (
                                 '  3.1. Error inserting into MSNI for WDD '
                              || c1.delivery_detail_id);
                     END;
                  END LOOP;
               END IF;

               IF c1.lot_status != 'No lot control' AND l_count_serials > 0
               THEN
                  FOR c2
                     IN cur_backord_lot_stg (c1.delivery_detail_id
                                            ,c1.move_order_line_id
                                            ,l_count_serials)
                  LOOP
                     EXIT WHEN cur_backord_lot_stg%NOTFOUND;

                     BEGIN
                        INSERT
                          INTO mtl_transaction_lots_interface (
                                  transaction_interface_id
                                 ,last_update_date
                                 ,last_updated_by
                                 ,creation_date
                                 ,created_by
                                 ,lot_number
                                 ,transaction_quantity)
                        VALUES (l_interface_transaction_id
                               ,SYSDATE
                               ,                            --last update date
                                0
                               ,                             --last updated by
                                SYSDATE
                               ,                                --created date
                                0
                               ,                                     --created
                                c2.lot_number
                               ,l_transaction_qty);

                        UPDATE xxwc_backordered_txns_sns_stg
                           SET status = 'INTERFACED'
                         WHERE ROWID = c2.row_id;
                     EXCEPTION
                        WHEN OTHERS
                        THEN
                           write_log (
                                 '  3.1. Error inserting into MTLI for WDD '
                              || c1.delivery_detail_id);
                     END;
                  END LOOP;
               END IF;
            EXCEPTION
               WHEN OTHERS
               THEN
                  write_log (
                        '  3.1. Error inserting into MTI (Holding Transfer) for WDD '
                     || c1.delivery_detail_id);
            END;

            -- Calling API to transact record
            l_processing_return := NULL;
            l_return_status := NULL;
            l_msg_count := NULL;
            l_msg_data := NULL;
            l_trans_count := NULL;
            l_processing_return :=
               inv_txn_manager_pub.process_transactions (
                  p_api_version        => 1.0
                 ,p_init_msg_list      => fnd_api.g_true
                 ,p_commit             => fnd_api.g_true
                 ,p_validation_level   => fnd_api.g_valid_level_full
                 ,x_return_status      => l_return_status
                 ,x_msg_count          => l_msg_count
                 ,x_msg_data           => l_msg_data
                 ,x_trans_count        => l_trans_count
                 ,p_table              => 1
                 ,p_header_id          => l_transaction_header_id);
            write_log ('  3.1. l_processing_return: ' || l_processing_return);
            write_log ('  3.1. l_return_status: ' || l_return_status);
            write_log ('  3.1. l_msg_count: ' || l_msg_count);
            write_log ('  3.1. l_msg_data: ' || l_msg_data);
            write_log ('  3.1. l_trans_count: ' || l_trans_count);

            IF NVL (l_return_status, 'E') <> 'S'
            THEN
               BEGIN
                  UPDATE xxwc_backordered_txns_lns_stg
                     SET status_code = 'TRANSFER_ERROR'
                   WHERE     ROWID = c1.row_id
                         AND status_code IN ('NEW', 'BACKORDERED')
                         AND request_id = g_request_id;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     write_log (
                           '  3.1. Could not update TRX post transfer for WDD '
                        || p_delivery_detail_id);
               END;

               IF     (   c1.serial_num_status != 'No serial number control'
                       OR c1.lot_status != 'No lot control')
                  AND l_count_serials > 0
               THEN
                  BEGIN
                     UPDATE xxwc_backordered_txns_sns_stg
                        SET status = 'TRANSFER_ERROR'
                      WHERE     delivery_detail_id = c1.delivery_detail_id
                            AND move_order_line_id = c1.move_order_line_id
                            AND status = 'INTERFACED'                  --'NEW'
                            AND request_id = g_request_id
                            AND ROWNUM <= l_count_serials;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        write_log (
                              '  3.1. Could not update TRX SNs post transfer for WDD '
                           || p_delivery_detail_id);
                  END;
               END IF;
            ELSE
               BEGIN
                  UPDATE xxwc_backordered_txns_lns_stg
                     SET status_code = 'TRANSFERRED'
                   WHERE     ROWID = c1.row_id
                         AND status_code IN ('NEW', 'BACKORDERED')
                         AND request_id = g_request_id;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     write_log (
                           '  3.1. Could not update TRX post transfer for WDD '
                        || p_delivery_detail_id);
               END;

               IF     (   c1.serial_num_status != 'No serial number control'
                       OR c1.lot_status != 'No lot control')
                  AND l_count_serials > 0
               THEN
                  BEGIN
                     UPDATE xxwc_backordered_txns_sns_stg
                        SET status = 'TRANSFERRED'
                      WHERE     delivery_detail_id = c1.delivery_detail_id
                            AND move_order_line_id = c1.move_order_line_id
                            AND status = 'INTERFACED'                  --'NEW'
                            AND request_id = g_request_id
                            AND ROWNUM <= l_count_serials;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        write_log (
                              '  3.1. Could not update TRX SNs post transfer for WDD '
                           || p_delivery_detail_id);
                  END;
               END IF;
            END IF;
         ELSE
            write_log (
                  '  3.1. No quantity to transact for WDD '
               || c1.delivery_detail_id);
         -- Need to add update to table??
         END IF;

         IF l_gral_transaction_qty > 0
         THEN
            write_log ('  3.1. Creating transactions for general...');

            SELECT mtl_material_transactions_s.NEXTVAL
              INTO l_transaction_header_id
              FROM DUAL;

            SELECT mtl_material_transactions_s.NEXTVAL
              INTO l_interface_transaction_id
              FROM DUAL;

            BEGIN
               INSERT
                 INTO mtl_transactions_interface (transaction_interface_id
                                                 ,transaction_header_id
                                                 ,source_code
                                                 ,source_line_id
                                                 ,source_header_id
                                                 ,process_flag
                                                 ,transaction_mode
                                                 ,lock_flag
                                                 ,last_update_date
                                                 ,last_updated_by
                                                 ,creation_date
                                                 ,created_by
                                                 ,transaction_type_id
                                                 ,transaction_date
                                                 ,transaction_uom
                                                 ,inventory_item_id
                                                 ,subinventory_code
                                                 ,organization_id
                                                 ,transfer_subinventory
                                                 ,transfer_organization
                                                 ,transaction_quantity
                                                 ,primary_quantity)
               VALUES (l_interface_transaction_id
                      ,l_transaction_header_id
                      ,'Subinventory Transfer'
                      ,                                          --source code
                       l_interface_transaction_id
                      ,                                       --source line id
                       l_interface_transaction_id
                      ,                                     --source header id
                       1
                      ,                                         --process flag
                       3
                      ,                                     --transaction mode
                       2
                      ,                                            --lock flag
                       SYSDATE
                      ,                                     --last update date
                       0
                      ,                                      --last updated by
                       SYSDATE
                      ,                                         --created date
                       0
                      ,                                           --created by
                       l_transaction_type_id
                      ,                                    -- transaction type
                       SYSDATE
                      ,                                    -- transaction date
                       l_transaction_uom
                      ,                                                 -- uom
                       c1.inventory_item_id
                      ,--inventory item id
                       c1.to_subinventory_code
                      ,-- from subinventory (data pulled from move order lines
                       c1.ship_from_org_id
                      ,c1.from_subinventory_code
                      ,                                     -- to subinventory
                       c1.ship_from_org_id
                      ,l_gral_transaction_qty
                      ,l_gral_transaction_qty);

               IF     c1.serial_num_status != 'No serial number control'
                  AND l_count_serials > 0
               THEN
                  FOR c2
                     IN cur_backord_sn_stg (c1.delivery_detail_id
                                           ,c1.move_order_line_id
                                           ,l_gral_transaction_qty)
                  LOOP
                     EXIT WHEN cur_backord_sn_stg%NOTFOUND;

                     BEGIN
                        INSERT
                          INTO mtl_serial_numbers_interface (
                                  transaction_interface_id
                                 ,last_update_date
                                 ,last_updated_by
                                 ,creation_date
                                 ,created_by
                                 ,fm_serial_number
                                 ,to_serial_number)
                        VALUES (l_interface_transaction_id
                               ,SYSDATE
                               ,                            --last update date
                                0
                               ,                             --last updated by
                                SYSDATE
                               ,                                --created date
                                0
                               ,                                     --created
                                c2.serial_number
                               ,c2.serial_number);

                        UPDATE xxwc_backordered_txns_sns_stg
                           SET status = 'INTERFACED'
                         WHERE ROWID = c2.row_id;
                     EXCEPTION
                        WHEN OTHERS
                        THEN
                           write_log (
                                 '  3.1. Error inserting into MSNI for WDD '
                              || c1.delivery_detail_id);
                     END;
                  END LOOP;
               END IF;

               IF c1.lot_status != 'No lot control' AND l_count_serials > 0
               THEN
                  FOR c2
                     IN cur_backord_lot_stg (c1.delivery_detail_id
                                            ,c1.move_order_line_id
                                            ,l_count_serials)
                  LOOP
                     EXIT WHEN cur_backord_lot_stg%NOTFOUND;

                     BEGIN
                        INSERT
                          INTO mtl_transaction_lots_interface (
                                  transaction_interface_id
                                 ,last_update_date
                                 ,last_updated_by
                                 ,creation_date
                                 ,created_by
                                 ,lot_number
                                 ,transaction_quantity)
                        VALUES (l_interface_transaction_id
                               ,SYSDATE
                               ,                            --last update date
                                0
                               ,                             --last updated by
                                SYSDATE
                               ,                                --created date
                                0
                               ,                                     --created
                                c2.lot_number
                               ,l_gral_transaction_qty);

                        UPDATE xxwc_backordered_txns_sns_stg
                           SET status = 'INTERFACED'
                         WHERE ROWID = c2.row_id;
                     EXCEPTION
                        WHEN OTHERS
                        THEN
                           write_log (
                                 '  3.1. Error inserting into MTLI for WDD '
                              || c1.delivery_detail_id);
                     END;
                  END LOOP;

                  l_trans_count := 0;

                  BEGIN
                     SELECT COUNT (*)
                       INTO l_trans_count
                       FROM mtl_transaction_lots_interface
                      WHERE transaction_interface_id =
                               l_interface_transaction_id;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        l_trans_count := 0;
                  END;

                  IF NVL (l_trans_count, 0) = 0
                  THEN
                     write_log (
                           '  3.1. Unprocessed lots were not found for WDD '
                        || c1.delivery_detail_id);
                     write_log (
                           '  3.1. Attempting to reuse a lot with quantity sent to Hld for WDD '
                        || c1.delivery_detail_id);

                     -- Re-use Lot if processed holding quantity as well
                     FOR c2
                        IN cur_backord_lot_stg_hld (c1.delivery_detail_id
                                                   ,c1.move_order_line_id
                                                   ,l_count_serials)
                     LOOP
                        EXIT WHEN cur_backord_lot_stg_hld%NOTFOUND;

                        BEGIN
                           INSERT
                             INTO mtl_transaction_lots_interface (
                                     transaction_interface_id
                                    ,last_update_date
                                    ,last_updated_by
                                    ,creation_date
                                    ,created_by
                                    ,lot_number
                                    ,transaction_quantity)
                           VALUES (l_interface_transaction_id
                                  ,SYSDATE
                                  ,                         --last update date
                                   0
                                  ,                          --last updated by
                                   SYSDATE
                                  ,                             --created date
                                   0
                                  ,                                  --created
                                   c2.lot_number
                                  ,l_gral_transaction_qty);

                           UPDATE xxwc_backordered_txns_sns_stg
                              SET status = 'INTERFACED'
                            WHERE ROWID = c2.row_id;
                        EXCEPTION
                           WHEN OTHERS
                           THEN
                              write_log (
                                    '  3.1. Error inserting into MTLI for WDD '
                                 || c1.delivery_detail_id);
                        END;
                     END LOOP;
                  END IF;
               END IF;
            EXCEPTION
               WHEN OTHERS
               THEN
                  write_log (
                        '  3.1. Error inserting into MTI for WDD '
                     || c1.delivery_detail_id);
                  write_log ('  3.1. SQLERRM ' || SQLERRM);
            END;

            -- Calling API to transact record
            l_processing_return := NULL;
            l_return_status := NULL;
            l_msg_count := NULL;
            l_msg_data := NULL;
            l_trans_count := NULL;
            l_processing_return :=
               inv_txn_manager_pub.process_transactions (
                  p_api_version        => 1.0
                 ,p_init_msg_list      => fnd_api.g_true
                 ,p_commit             => fnd_api.g_true
                 ,p_validation_level   => fnd_api.g_valid_level_full
                 ,x_return_status      => l_return_status
                 ,x_msg_count          => l_msg_count
                 ,x_msg_data           => l_msg_data
                 ,x_trans_count        => l_trans_count
                 ,p_table              => 1
                 ,p_header_id          => l_transaction_header_id);
            write_log ('  3.1. l_processing_return: ' || l_processing_return);
            write_log ('  3.1. l_return_status: ' || l_return_status);
            write_log ('  3.1. l_msg_count: ' || l_msg_count);
            write_log ('  3.1. l_msg_data: ' || l_msg_data);
            write_log ('  3.1. l_trans_count: ' || l_trans_count);

            IF NVL (l_return_status, 'E') <> 'S'
            THEN
               BEGIN
                  UPDATE xxwc_backordered_txns_lns_stg
                     SET status_code = 'TRANSFER_ERROR'
                   WHERE     ROWID = c1.row_id
                         AND status_code IN ('NEW', 'BACKORDERED')
                         AND request_id = g_request_id;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     write_log (
                           '  3.1. Could not update TRX post transfer for WDD '
                        || p_delivery_detail_id);
               END;

               IF     (   c1.serial_num_status != 'No serial number control'
                       OR c1.lot_status != 'No lot control')
                  AND l_count_serials > 0
               THEN
                  BEGIN
                     UPDATE xxwc_backordered_txns_sns_stg
                        SET status = 'TRANSFER_ERROR'
                      WHERE     delivery_detail_id = c1.delivery_detail_id
                            AND move_order_line_id = c1.move_order_line_id
                            AND status = 'INTERFACED'                  --'NEW'
                            AND request_id = g_request_id
                            AND ROWNUM <= l_count_serials;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        write_log (
                              '  3.1. Could not update TRX SNs post transfer for WDD '
                           || p_delivery_detail_id);
                  END;
               END IF;
            ELSE
               BEGIN
                  UPDATE xxwc_backordered_txns_lns_stg
                     SET status_code = 'TRANSFERRED'
                   WHERE     ROWID = c1.row_id
                         AND status_code IN ('NEW', 'BACKORDERED')
                         AND request_id = g_request_id;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     write_log (
                           '  3.1. Could not update TRX post transfer for WDD '
                        || p_delivery_detail_id);
               END;

               IF     (   c1.serial_num_status != 'No serial number control'
                       OR c1.lot_status != 'No lot control')
                  AND l_count_serials > 0
               THEN
                  BEGIN
                     UPDATE xxwc_backordered_txns_sns_stg
                        SET status = 'TRANSFERRED'
                      WHERE     delivery_detail_id = c1.delivery_detail_id
                            AND move_order_line_id = c1.move_order_line_id
                            AND status = 'INTERFACED'                  --'NEW'
                            AND request_id = g_request_id
                            AND ROWNUM <= l_count_serials;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        write_log (
                              '  3.1. Could not update TRX SNs post transfer for WDD '
                           || p_delivery_detail_id);
                  END;
               END IF;
            END IF;
         ELSE
            write_log (
                  '  3.1. No quantity to transact for WDD '
               || c1.delivery_detail_id);
         -- Need to add update to table??
         END IF;
      END LOOP;

      p_return_status := l_return_status;                       --'COMPLETED';
      write_log (
            '  3.1. Exiting process_subinv_transf for WDD '
         || p_delivery_detail_id);
   END process_subinv_transf;

   /*************************************************************************
     Procedure : Update_SO_Line

     PURPOSE:
     Parameter:
            IN

   ************************************************************************/
   PROCEDURE update_so_line (p_delivery_detail_id   IN     NUMBER
                            ,p_return_status           OUT VARCHAR2)
   IS
      CURSOR cur_backord_ln_stg
      IS
         SELECT x1.ROWID row_id, x1.*
           FROM xxwc_backordered_txns_lns_stg x1
          WHERE     x1.delivery_detail_id = p_delivery_detail_id
                AND x1.status_code = 'TRANSFERRED'
                AND x1.request_id = g_request_id;

      l_header_rec               oe_order_pub.header_rec_type;
      l_line_tbl                 oe_order_pub.line_tbl_type;
      l_action_request_tbl       oe_order_pub.request_tbl_type;
      l_header_adj_tbl           oe_order_pub.header_adj_tbl_type;
      l_line_adj_tbl             oe_order_pub.line_adj_tbl_type;
      l_header_scr_tbl           oe_order_pub.header_scredit_tbl_type;
      l_line_scredit_tbl         oe_order_pub.line_scredit_tbl_type;
      l_request_rec              oe_order_pub.request_rec_type;
      l_return_status            VARCHAR2 (1000);
      l_msg_count                NUMBER;
      l_msg_data                 VARCHAR2 (1000);
      p_api_version_number       NUMBER := 1.0;
      p_init_msg_list            VARCHAR2 (10) := fnd_api.g_false;
      p_return_values            VARCHAR2 (10) := fnd_api.g_false;
      p_action_commit            VARCHAR2 (10) := fnd_api.g_false;
      x_return_status            VARCHAR2 (1);
      x_msg_count                NUMBER;
      x_msg_data                 VARCHAR2 (100);
      p_header_rec               oe_order_pub.header_rec_type
                                    := oe_order_pub.g_miss_header_rec;
      p_header_val_rec           oe_order_pub.header_val_rec_type
                                    := oe_order_pub.g_miss_header_val_rec;
      p_header_adj_tbl           oe_order_pub.header_adj_tbl_type
                                    := oe_order_pub.g_miss_header_adj_tbl;
      p_header_adj_val_tbl       oe_order_pub.header_adj_val_tbl_type
                                    := oe_order_pub.g_miss_header_adj_val_tbl;
      p_header_price_att_tbl     oe_order_pub.header_price_att_tbl_type
                                    := oe_order_pub.g_miss_header_price_att_tbl;
      p_header_adj_att_tbl       oe_order_pub.header_adj_att_tbl_type
                                    := oe_order_pub.g_miss_header_adj_att_tbl;
      p_header_adj_assoc_tbl     oe_order_pub.header_adj_assoc_tbl_type
                                    := oe_order_pub.g_miss_header_adj_assoc_tbl;
      p_header_scredit_tbl       oe_order_pub.header_scredit_tbl_type
                                    := oe_order_pub.g_miss_header_scredit_tbl;
      p_header_scredit_val_tbl   oe_order_pub.header_scredit_val_tbl_type
         := oe_order_pub.g_miss_header_scredit_val_tbl;
      p_line_tbl                 oe_order_pub.line_tbl_type
                                    := oe_order_pub.g_miss_line_tbl;
      p_line_val_tbl             oe_order_pub.line_val_tbl_type
                                    := oe_order_pub.g_miss_line_val_tbl;
      p_line_adj_tbl             oe_order_pub.line_adj_tbl_type
                                    := oe_order_pub.g_miss_line_adj_tbl;
      p_line_adj_val_tbl         oe_order_pub.line_adj_val_tbl_type
                                    := oe_order_pub.g_miss_line_adj_val_tbl;
      p_line_price_att_tbl       oe_order_pub.line_price_att_tbl_type
                                    := oe_order_pub.g_miss_line_price_att_tbl;
      p_line_adj_att_tbl         oe_order_pub.line_adj_att_tbl_type
                                    := oe_order_pub.g_miss_line_adj_att_tbl;
      p_line_adj_assoc_tbl       oe_order_pub.line_adj_assoc_tbl_type
                                    := oe_order_pub.g_miss_line_adj_assoc_tbl;
      p_line_scredit_tbl         oe_order_pub.line_scredit_tbl_type
                                    := oe_order_pub.g_miss_line_scredit_tbl;
      p_line_scredit_val_tbl     oe_order_pub.line_scredit_val_tbl_type
         := oe_order_pub.g_miss_line_scredit_val_tbl;
      p_lot_serial_tbl           oe_order_pub.lot_serial_tbl_type
                                    := oe_order_pub.g_miss_lot_serial_tbl;
      p_lot_serial_val_tbl       oe_order_pub.lot_serial_val_tbl_type
                                    := oe_order_pub.g_miss_lot_serial_val_tbl;
      p_action_request_tbl       oe_order_pub.request_tbl_type
                                    := oe_order_pub.g_miss_request_tbl;
      x_header_rec               oe_order_pub.header_rec_type;
      x_header_val_rec           oe_order_pub.header_val_rec_type;
      x_header_adj_tbl           oe_order_pub.header_adj_tbl_type;
      x_header_adj_val_tbl       oe_order_pub.header_adj_val_tbl_type;
      x_header_price_att_tbl     oe_order_pub.header_price_att_tbl_type;
      x_header_adj_att_tbl       oe_order_pub.header_adj_att_tbl_type;
      x_header_adj_assoc_tbl     oe_order_pub.header_adj_assoc_tbl_type;
      x_header_scredit_tbl       oe_order_pub.header_scredit_tbl_type;
      x_header_scredit_val_tbl   oe_order_pub.header_scredit_val_tbl_type;
      x_line_tbl                 oe_order_pub.line_tbl_type;
      x_line_val_tbl             oe_order_pub.line_val_tbl_type;
      x_line_adj_tbl             oe_order_pub.line_adj_tbl_type;
      x_line_adj_val_tbl         oe_order_pub.line_adj_val_tbl_type;
      x_line_price_att_tbl       oe_order_pub.line_price_att_tbl_type;
      x_line_adj_att_tbl         oe_order_pub.line_adj_att_tbl_type;
      x_line_adj_assoc_tbl       oe_order_pub.line_adj_assoc_tbl_type;
      x_line_scredit_tbl         oe_order_pub.line_scredit_tbl_type;
      x_line_scredit_val_tbl     oe_order_pub.line_scredit_val_tbl_type;
      x_lot_serial_tbl           oe_order_pub.lot_serial_tbl_type;
      x_lot_serial_val_tbl       oe_order_pub.lot_serial_val_tbl_type;
      x_action_request_tbl       oe_order_pub.request_tbl_type;
      x_debug_file               VARCHAR2 (100);
      l_line_tbl_index           NUMBER;
      l_msg_index_out            NUMBER (10);
      l_order_line_qty           NUMBER;
      l_org_id                   NUMBER;
   BEGIN
      write_log (
         '  3.2. Entering update_so_line for WDD ' || p_delivery_detail_id);
      oe_msg_pub.initialize;
      l_org_id := NULL;

      BEGIN
         SELECT organization_id
           INTO l_org_id
           FROM hr_operating_units
          WHERE NAME = 'HDS White Cap - Org';
      EXCEPTION
         WHEN OTHERS
         THEN
            l_org_id := fnd_global.org_id;
      END;

      l_line_tbl_index := 0;

      FOR c1 IN cur_backord_ln_stg
      LOOP
         EXIT WHEN cur_backord_ln_stg%NOTFOUND;
         l_line_tbl_index := l_line_tbl_index + 1;
         l_line_tbl (l_line_tbl_index) := oe_order_pub.g_miss_line_rec;
         l_order_line_qty := 0;

         BEGIN
            SELECT ordered_quantity
              INTO l_order_line_qty
              FROM oe_order_lines
             WHERE line_id = c1.line_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_order_line_qty := c1.ol_ordered_qty;
         END;

         -- 02/27/2012 CG
         IF c1.new_sched_ship_date IS NOT NULL
         THEN
            l_line_tbl (l_line_tbl_index).schedule_ship_date :=
               c1.new_sched_ship_date;
         END IF;

         -- Updating subinvntory to the holding location if new qty > 0 and hold/reserve = N
         --04/19/2012: if  (c1.ol_ordered_qty - c1.wdd_cancellation_qty) = 0 and c1.hold_reserve_flag = 'N'
         IF     (l_order_line_qty - c1.wdd_cancellation_qty) = 0
            AND c1.hold_reserve_flag = 'N'
         THEN
            -- Only scenario where the Subinventory is not resourced
            NULL;
         ELSE
            l_line_tbl (l_line_tbl_index).subinventory :=
               c1.holding_subinventory;
         END IF;

         l_line_tbl (l_line_tbl_index).ordered_quantity :=
            (l_order_line_qty - c1.wdd_cancellation_qty);
         -- 04/19/2012 CG (c1.ol_ordered_qty - c1.wdd_cancellation_qty);
         l_line_tbl (l_line_tbl_index).header_id := c1.header_id;
         l_line_tbl (l_line_tbl_index).line_id := c1.line_id;
         l_line_tbl (l_line_tbl_index).change_reason := '2';
         -- Incorrect Quantity (Confirm code in other instances)
         l_line_tbl (l_line_tbl_index).operation := oe_globals.g_opr_update;
         -- CALL TO PROCESS ORDER
         oe_order_pub.process_order (
            p_org_id                   => l_org_id
           ,p_api_version_number       => 1.0
           ,x_return_status            => l_return_status
           ,x_msg_count                => l_msg_count
           ,x_msg_data                 => l_msg_data
           ,p_header_rec               => l_header_rec
           ,p_line_tbl                 => l_line_tbl
           -- OUT PARAMETERS
           ,x_header_rec               => x_header_rec
           ,x_header_val_rec           => x_header_val_rec
           ,x_header_adj_tbl           => x_header_adj_tbl
           ,x_header_adj_val_tbl       => x_header_adj_val_tbl
           ,x_header_price_att_tbl     => x_header_price_att_tbl
           ,x_header_adj_att_tbl       => x_header_adj_att_tbl
           ,x_header_adj_assoc_tbl     => x_header_adj_assoc_tbl
           ,x_header_scredit_tbl       => x_header_scredit_tbl
           ,x_header_scredit_val_tbl   => x_header_scredit_val_tbl
           ,x_line_tbl                 => x_line_tbl
           ,x_line_val_tbl             => x_line_val_tbl
           ,x_line_adj_tbl             => x_line_adj_tbl
           ,x_line_adj_val_tbl         => x_line_adj_val_tbl
           ,x_line_price_att_tbl       => x_line_price_att_tbl
           ,x_line_adj_att_tbl         => x_line_adj_att_tbl
           ,x_line_adj_assoc_tbl       => x_line_adj_assoc_tbl
           ,x_line_scredit_tbl         => x_line_scredit_tbl
           ,x_line_scredit_val_tbl     => x_line_scredit_val_tbl
           ,x_lot_serial_tbl           => x_lot_serial_tbl
           ,x_lot_serial_val_tbl       => x_lot_serial_val_tbl
           ,x_action_request_tbl       => x_action_request_tbl);

         -- Retrieve messages
         FOR i IN 1 .. l_msg_count
         LOOP
            oe_msg_pub.get (p_msg_index       => i
                           ,p_encoded         => fnd_api.g_false
                           ,p_data            => l_msg_data
                           ,p_msg_index_out   => l_msg_index_out);

            IF l_msg_data IS NOT NULL
            THEN
               write_log (
                     '  3.2. Message index and data: '
                  || l_msg_index_out
                  || ' - '
                  || l_msg_data);
            END IF;
         END LOOP;

         -- Check the return status
         IF l_return_status = fnd_api.g_ret_sts_success
         THEN
            write_log ('  3.2. Line Quantity Update Sucessful');

            BEGIN
               UPDATE xxwc_backordered_txns_lns_stg
                  SET status_code = 'SO_LINE_UPD'
                WHERE     ROWID = c1.row_id
                      AND status_code = 'TRANSFERRED'
                      AND request_id = g_request_id;
            EXCEPTION
               WHEN OTHERS
               THEN
                  write_log (
                        '  3.2. Could not update TRX post line update for WDD '
                     || p_delivery_detail_id);
            END;

            IF    c1.serial_num_status != 'No serial number control'
               OR c1.lot_status != 'No lot control'
            THEN
               BEGIN
                  UPDATE xxwc_backordered_txns_sns_stg
                     SET status = 'SO_LINE_UPD'
                   WHERE     delivery_detail_id = c1.delivery_detail_id
                         AND move_order_line_id = c1.move_order_line_id
                         AND status = 'TRANSFERRED'
                         AND request_id = g_request_id;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     write_log (
                           '  3.2. Could not update TRX SNs post line update for WDD '
                        || p_delivery_detail_id);
               END;
            END IF;
         ELSE
            write_log ('  3.2. Line Quantity update Failed');

            BEGIN
               UPDATE xxwc_backordered_txns_lns_stg
                  SET status_code = 'SO_LINE_UPD_ERR'
                WHERE     ROWID = c1.row_id
                      AND status_code = 'TRANSFERRED'
                      AND request_id = g_request_id;
            EXCEPTION
               WHEN OTHERS
               THEN
                  write_log (
                        '  3.2. Could not update TRX post line update for WDD '
                     || p_delivery_detail_id);
            END;

            IF    c1.serial_num_status != 'No serial number control'
               OR c1.lot_status != 'No lot control'
            THEN
               BEGIN
                  UPDATE xxwc_backordered_txns_sns_stg
                     SET status = 'SO_LINE_UPD_ERR'
                   WHERE     delivery_detail_id = c1.delivery_detail_id
                         AND move_order_line_id = c1.move_order_line_id
                         AND status = 'TRANSFERRED'
                         AND request_id = g_request_id;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     write_log (
                           '  3.2. Could not update TRX SNs post line update for WDD '
                        || p_delivery_detail_id);
               END;
            END IF;
         END IF;
      END LOOP;

      COMMIT;
      p_return_status := l_return_status;                       --'COMPLETED';
      write_log (
         '  3.2. Exiting update_so_line for WDD ' || p_delivery_detail_id);
   END update_so_line;

   /*************************************************************************
     Procedure : create_reservation

     PURPOSE:
     Parameter:
            IN

   ************************************************************************/
   PROCEDURE create_reservation (p_delivery_detail_id   IN     NUMBER
                                ,p_return_status           OUT VARCHAR2)
   IS
      CURSOR cur_backord_ln_stg
      IS
         SELECT x1.ROWID row_id, x1.*
           FROM xxwc_backordered_txns_lns_stg x1
          WHERE     x1.delivery_detail_id = p_delivery_detail_id
                AND x1.status_code IN ('SO_LINE_UPD', 'TRANSFERRED')
                AND x1.request_id = g_request_id;

      CURSOR cur_backord_sn_stg (
         p_delivery_detail_id    NUMBER
        ,p_mo_line_id            NUMBER
        ,p_reservation_qty       NUMBER)
      IS
         SELECT x1.ROWID row_id, x1.*
           FROM xxwc_backordered_txns_sns_stg x1
          WHERE     x1.delivery_detail_id = p_delivery_detail_id
                AND x1.move_order_line_id = p_mo_line_id
                AND x1.status IN ('SO_LINE_UPD', 'TRANSFERRED')
                AND x1.request_id = g_request_id
                AND ROWNUM <= p_reservation_qty;

      -- Common Declarations
      l_api_version                   NUMBER := 1.0;
      l_init_msg_list                 VARCHAR2 (2) := fnd_api.g_true;
      x_return_status                 VARCHAR2 (2);
      x_msg_count                     NUMBER := 0;
      x_msg_data                      VARCHAR2 (255);
      -- WHO columns
      l_user_id                       NUMBER := -1;
      l_resp_id                       NUMBER := -1;
      l_application_id                NUMBER := -1;
      l_row_cnt                       NUMBER := 1;
      l_user_name                     VARCHAR2 (30) := 'SETUPUSER';
      l_resp_name                     VARCHAR2 (30) := 'INVENTORY';
      -- API specific declarations
      l_rsv_rec                       inv_reservation_global.mtl_reservation_rec_type;
      l_serial_number                 inv_reservation_global.serial_number_tbl_type;
      l_partial_reservation_flag      VARCHAR2 (2) := fnd_api.g_false;
      l_force_reservation_flag        VARCHAR2 (2) := fnd_api.g_false;
      l_validation_flag               VARCHAR2 (2) := fnd_api.g_true;
      l_partial_reservation_exists    BOOLEAN := FALSE;
      x_serial_number                 inv_reservation_global.serial_number_tbl_type;
      x_quantity_reserved             NUMBER := 0;
      x_reservation_id                NUMBER := 0;
      l_primary_reservation_qty       NUMBER := 0;
      -- total qty
      l_subinventory_code             VARCHAR2 (40) := 'WHSE';
      l_count_sns                     NUMBER := 0;
      v_count                         NUMBER := 0;
      l_serial_reservation_quantity   NUMBER := 0;
      l_trx_uom                       VARCHAR2 (15) := NULL;
      l_demand_source_header_id       NUMBER;
      l_lot_number                    VARCHAR2 (80) := NULL;
      l_lot_quantity                  NUMBER;
      l_order_line_qty                NUMBER;
      -- Record set clearing
      g_miss_rsv_rec                  inv_reservation_global.mtl_reservation_rec_type;
      g_miss_serial_number            inv_reservation_global.serial_number_tbl_type;
      g_miss_mtl_reservations_tbl     inv_reservation_global.mtl_reservation_tbl_type;
   BEGIN
      write_log (
            '  3.3. Entering create_reservation for WDD '
         || p_delivery_detail_id);

      FOR c1 IN cur_backord_ln_stg
      LOOP
         EXIT WHEN cur_backord_ln_stg%NOTFOUND;
         l_order_line_qty := 0;

         BEGIN
            SELECT ordered_quantity
              INTO l_order_line_qty
              FROM oe_order_lines
             WHERE line_id = c1.line_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_order_line_qty := c1.ol_ordered_qty;
         END;

         l_primary_reservation_qty := 0;
         --l_primary_reservation_qty := c1.ol_ordered_qty - nvl(c1.wdd_cancellation_qty,0);
         l_primary_reservation_qty :=
            l_order_line_qty - NVL (c1.wdd_cancellation_qty, 0);
         l_serial_reservation_quantity := 0;

         IF c1.serial_num_status != 'No serial number control'
         THEN
            l_row_cnt := 0;
            l_serial_number := g_miss_serial_number;

            FOR c2
               IN cur_backord_sn_stg (c1.delivery_detail_id
                                     ,c1.move_order_line_id
                                     ,l_primary_reservation_qty)
            LOOP
               EXIT WHEN cur_backord_sn_stg%NOTFOUND;
               l_row_cnt := l_row_cnt + 1;
               l_serial_number (l_row_cnt).inventory_item_id :=
                  c2.inventory_item_id;
               l_serial_number (l_row_cnt).serial_number := c2.serial_number;
            END LOOP;

            l_serial_reservation_quantity := l_row_cnt;
         END IF;

         l_lot_number := NULL;

         IF c1.lot_status != 'No lot control'
         THEN
            l_lot_number := NULL;
            l_lot_quantity := NULL;

            BEGIN
               SELECT x1.lot_number, ABS (x1.lot_quantity)
                 INTO l_lot_number, l_lot_quantity
                 FROM xxwc_backordered_txns_sns_stg x1
                WHERE     x1.delivery_detail_id = c1.delivery_detail_id
                      AND x1.move_order_line_id = c1.move_order_line_id
                      AND x1.status IN ('SO_LINE_UPD', 'TRANSFERRED')
                      AND x1.request_id = g_request_id
                      AND ROWNUM = 1;

               IF NVL (l_lot_quantity, 0) < l_primary_reservation_qty
               THEN
                  l_primary_reservation_qty := NVL (l_lot_quantity, 0);
               END IF;
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_lot_number := NULL;
                  write_log (
                        '  3.3. Lot not found to reserve for WDD '
                     || p_delivery_detail_id);
            END;
         END IF;

         l_trx_uom := NULL;

         BEGIN
            SELECT order_quantity_uom
              INTO l_trx_uom
              FROM oe_order_lines
             WHERE line_id = c1.line_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_trx_uom := 'EA';
         END;

         IF     (l_serial_reservation_quantity != 0)
            AND (l_serial_reservation_quantity < l_primary_reservation_qty)
         THEN
            l_primary_reservation_qty := l_serial_reservation_quantity;
         END IF;

         l_demand_source_header_id := NULL;

         BEGIN
            SELECT mso.sales_order_id
              INTO l_demand_source_header_id
              FROM oe_order_headers ooha, mtl_sales_orders mso
             WHERE     ooha.header_id = c1.header_id
                   AND ooha.order_number = mso.segment1
                   AND mso.segment3 = 'ORDER ENTRY';
         EXCEPTION
            WHEN OTHERS
            THEN
               l_demand_source_header_id := NULL;
         END;

         write_log (
            '  3.3. Primary Reservation Qty ' || l_primary_reservation_qty);
         write_log (
            '  3.3. Serial Quantity ' || l_serial_reservation_quantity);
         write_log ('  3.3. Lot Number ' || l_lot_number);

         IF l_demand_source_header_id IS NOT NULL
         THEN
            l_rsv_rec := g_miss_rsv_rec;
            l_rsv_rec.organization_id := c1.ship_from_org_id;
            l_rsv_rec.inventory_item_id := c1.inventory_item_id;
            l_rsv_rec.requirement_date := SYSDATE;      --c3.requirement_date;
            l_rsv_rec.demand_source_type_id :=
               inv_reservation_global.g_source_type_oe;
            l_rsv_rec.supply_source_type_id :=
               inv_reservation_global.g_source_type_inv;
            l_rsv_rec.demand_source_name := NULL;
            -- 'RSV_ITM_SRL_'||itm.segment1;
            l_rsv_rec.primary_reservation_quantity :=
               l_primary_reservation_qty;
            l_rsv_rec.primary_uom_code := l_trx_uom;
            -- 02/27/2012 CG
            --l_rsv_rec.subinventory_code               :=   c1.from_subinventory_code;
            l_rsv_rec.subinventory_code := c1.holding_subinventory;
            l_rsv_rec.demand_source_header_id := l_demand_source_header_id;
            l_rsv_rec.demand_source_line_id := c1.line_id;
            l_rsv_rec.reservation_uom_code := NULL;
            l_rsv_rec.reservation_quantity := NULL;
            l_rsv_rec.supply_source_header_id := NULL;
            l_rsv_rec.supply_source_line_id := NULL;
            l_rsv_rec.supply_source_name := NULL;
            l_rsv_rec.supply_source_line_detail := NULL;
            l_rsv_rec.lot_number := l_lot_number;
            l_rsv_rec.serial_number := NULL;
            l_rsv_rec.ship_ready_flag := NULL;
            l_rsv_rec.attribute15 := NULL;
            l_rsv_rec.attribute14 := NULL;
            l_rsv_rec.attribute13 := NULL;
            l_rsv_rec.attribute12 := NULL;
            l_rsv_rec.attribute11 := NULL;
            l_rsv_rec.attribute10 := NULL;
            l_rsv_rec.attribute9 := NULL;
            l_rsv_rec.attribute8 := NULL;
            l_rsv_rec.attribute7 := NULL;
            l_rsv_rec.attribute6 := NULL;
            l_rsv_rec.attribute5 := NULL;
            l_rsv_rec.attribute4 := NULL;
            l_rsv_rec.attribute3 := NULL;
            l_rsv_rec.attribute2 := NULL;
            l_rsv_rec.attribute1 := NULL;
            l_rsv_rec.attribute_category := NULL;
            l_rsv_rec.lpn_id := NULL;
            l_rsv_rec.pick_slip_number := NULL;
            l_rsv_rec.lot_number_id := NULL;
            l_rsv_rec.locator_id := NULL;
            l_rsv_rec.subinventory_id := NULL;
            l_rsv_rec.revision := NULL;
            l_rsv_rec.external_source_line_id := NULL;
            l_rsv_rec.external_source_code := NULL;
            l_rsv_rec.autodetail_group_id := NULL;
            l_rsv_rec.reservation_uom_id := NULL;
            l_rsv_rec.primary_uom_id := NULL;
            l_rsv_rec.demand_source_delivery := NULL;
            l_rsv_rec.serial_reservation_quantity :=
               l_serial_reservation_quantity;
            l_rsv_rec.project_id := NULL;
            l_rsv_rec.task_id := NULL;
            inv_reservation_pub.create_reservation (
               p_api_version_number         => l_api_version
              ,p_init_msg_lst               => l_init_msg_list
              ,p_rsv_rec                    => l_rsv_rec
              ,p_serial_number              => l_serial_number
              ,p_partial_reservation_flag   => l_partial_reservation_flag
              ,p_force_reservation_flag     => l_force_reservation_flag
              ,p_partial_rsv_exists         => l_partial_reservation_exists
              ,p_validation_flag            => l_validation_flag
              ,x_serial_number              => x_serial_number
              ,x_return_status              => x_return_status
              ,x_msg_count                  => x_msg_count
              ,x_msg_data                   => x_msg_data
              ,x_quantity_reserved          => x_quantity_reserved
              ,x_reservation_id             => x_reservation_id);

            IF (x_return_status <> fnd_api.g_ret_sts_success)
            THEN
               write_log (
                     '  3.3. Errored Reservation for WDD:'
                  || c1.delivery_detail_id);
               write_log ('  3.3. Message Count :' || x_msg_count);
               write_log ('  3.3. Error Message :' || x_msg_data);

               IF (NVL (x_msg_count, 0) = 0)
               THEN
                  write_log ('  3.3. no message return');
               ELSE
                  FOR i IN 1 .. x_msg_count
                  LOOP
                     x_msg_data := fnd_msg_pub.get (i, 'F');
                     write_log ('  3.3. Message: ' || x_msg_data);
                  END LOOP;
               END IF;

               BEGIN
                  UPDATE xxwc_backordered_txns_lns_stg
                     SET status_code = 'RESERVE_ERR'
                   WHERE     ROWID = c1.row_id
                         AND status_code IN ('SO_LINE_UPD', 'TRANSFERRED')
                         AND request_id = g_request_id;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     write_log (
                           '  3.3. Could not update TRX post reservation for WDD '
                        || p_delivery_detail_id);
               END;

               IF    c1.serial_num_status != 'No serial number control'
                  OR c1.lot_status != 'No lot control'
               THEN
                  BEGIN
                     UPDATE xxwc_backordered_txns_sns_stg
                        SET status = 'RESERVED_ERR'
                      WHERE     delivery_detail_id = c1.delivery_detail_id
                            AND move_order_line_id = c1.move_order_line_id
                            AND status IN ('SO_LINE_UPD', 'TRANSFERRED')
                            AND request_id = g_request_id;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        write_log (
                              '  3.3. Could not update TRX SNs/Lots post reservation for WDD '
                           || p_delivery_detail_id);
                  END;
               END IF;
            ELSE
               write_log (
                     '  3.3. Reservation ID :'
                  || x_reservation_id
                  || ' Quantity Reserved:'
                  || x_quantity_reserved);

               FOR srl IN 1 .. x_serial_number.COUNT
               LOOP
                  write_log (
                        '  3.3. Serial Reserved:'
                     || x_serial_number (srl).serial_number);
               END LOOP;

               BEGIN
                  UPDATE xxwc_backordered_txns_lns_stg
                     SET status_code = 'RESERVED'
                   WHERE     ROWID = c1.row_id
                         AND status_code IN ('SO_LINE_UPD', 'TRANSFERRED')
                         AND request_id = g_request_id;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     write_log (
                           '  3.3. Could not update TRX post reservation for WDD '
                        || p_delivery_detail_id);
               END;

               IF    c1.serial_num_status != 'No serial number control'
                  OR c1.lot_status != 'No lot control'
               THEN
                  BEGIN
                     UPDATE xxwc_backordered_txns_sns_stg
                        SET status = 'RESERVED'
                      WHERE     delivery_detail_id = c1.delivery_detail_id
                            AND move_order_line_id = c1.move_order_line_id
                            AND status IN ('SO_LINE_UPD', 'TRANSFERRED')
                            AND request_id = g_request_id;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        write_log (
                              '  3.3. Could not update TRX SNs/Lots post reservation for WDD '
                           || p_delivery_detail_id);
                  END;
               END IF;
            END IF;
         ELSE
            write_log (
                  '  3.3. MTL Sales Order not found for WDD '
               || p_delivery_detail_id
               || ' and order header id '
               || c1.header_id);
         END IF;
      END LOOP;

      p_return_status := x_return_status;                       --'COMPLETED';
      write_log (
         '  3.3. Exiting create_reservation for WDD ' || p_delivery_detail_id);
   END create_reservation;

   PROCEDURE reassign_open_delivery (p_new_batch_id   IN NUMBER
                                    ,p_header_id      IN NUMBER)
   IS
      CURSOR cur_reassign (
         p_new_batch_id      NUMBER
        ,p_header_id         NUMBER
        ,p_date_scheduled    DATE)
      IS
         SELECT wnd.delivery_id, wnd.NAME, wdd.delivery_detail_id
           FROM wsh_new_deliveries wnd
               ,wsh_delivery_assignments wda
               ,wsh_delivery_details wdd
          WHERE     wnd.delivery_id = wda.delivery_id
                AND wda.delivery_detail_id = wdd.delivery_detail_id
                AND wdd.batch_id = p_new_batch_id
                AND wdd.source_header_id = p_header_id
                AND wdd.date_scheduled = p_date_scheduled;

      l_delivery_id      NUMBER;
      l_delivery_name    VARCHAR2 (30);
      l_date_scheduled   DATE;
      p_api_version      NUMBER;
      p_init_msg_list    VARCHAR2 (30);
      p_commit           VARCHAR2 (30);
      p_delivery_id      NUMBER;
      p_delivery_name    VARCHAR2 (30);
      p_tabofdeldet      wsh_delivery_details_pub.id_tab_type;
      p_action           VARCHAR2 (30);
      x_return_status    VARCHAR2 (10);
      x_msg_count        NUMBER;
      x_msg_data         VARCHAR2 (2000);
      x_msg_details      VARCHAR2 (3000);
      x_msg_summary      VARCHAR2 (3000);
      i                  NUMBER;
   BEGIN
      write_log (
         '  3.5. Reassigning delivery details to existing open delivery');
      l_delivery_id := NULL;
      l_delivery_name := NULL;
      l_date_scheduled := NULL;

      BEGIN
           SELECT wnd.delivery_id, wnd.NAME, wdd.date_scheduled
             INTO l_delivery_id, l_delivery_name, l_date_scheduled
             FROM wsh_new_deliveries wnd
                 ,wsh_delivery_assignments wda
                 ,wsh_delivery_details wdd
            WHERE     wnd.status_code != 'CL'
                  AND wnd.delivery_id = wda.delivery_id
                  AND wda.delivery_detail_id = wdd.delivery_detail_id
                  AND EXISTS
                         (SELECT 'Same Delivery'
                            FROM wsh_new_deliveries wnd2
                                ,wsh_delivery_assignments wda2
                                ,wsh_delivery_details wdd2
                           WHERE     wnd2.delivery_id = wda2.delivery_id
                                 AND wda2.delivery_detail_id =
                                        wdd2.delivery_detail_id
                                 AND wdd2.batch_id = p_new_batch_id
                                 AND wdd2.source_header_id = p_header_id
                                 AND wdd2.batch_id != wdd.batch_id
                                 AND wdd2.source_header_id =
                                        wdd.source_header_id
                                 AND wdd2.date_scheduled = wdd.date_scheduled
                                 AND wdd2.ship_method_code =
                                        wdd.ship_method_code
                                 AND wnd2.ultimate_dropoff_location_id =
                                        wnd.ultimate_dropoff_location_id--and       wdd2.earliest_pickup_date = wnd.earliest_pickup_date
                      )
                  AND ROWNUM = 1
         ORDER BY wnd.NAME, wdd.delivery_detail_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_delivery_id := NULL;
            l_delivery_name := NULL;
            l_date_scheduled := NULL;
      END;

      IF l_delivery_id IS NOT NULL
      THEN
         x_return_status := wsh_util_core.g_ret_sts_success;
         i := 1;

         FOR c1
            IN cur_reassign (p_new_batch_id, p_header_id, l_date_scheduled)
         LOOP
            EXIT WHEN cur_reassign%NOTFOUND;
            p_delivery_id := c1.delivery_id;
            p_delivery_name := c1.NAME;
            p_tabofdeldet (i) := c1.delivery_detail_id;
            i := i + 1;
         END LOOP;

         p_action := 'UNASSIGN';                                   -- UNASSIGN
         write_log ('  3.5. Unassigning from delivery ' || p_delivery_name);
         -- Call to WSH_DELIVERY_DETAILS_PUB.Detail_to_Delivery.
         wsh_delivery_details_pub.detail_to_delivery (
            p_api_version     => 1.0
           ,p_init_msg_list   => p_init_msg_list
           ,p_commit          => p_commit
           ,x_return_status   => x_return_status
           ,x_msg_count       => x_msg_count
           ,x_msg_data        => x_msg_data
           ,p_tabofdeldets    => p_tabofdeldet
           ,p_action          => p_action
           ,p_delivery_id     => p_delivery_id
           ,p_delivery_name   => p_delivery_name);

         IF (x_return_status <> wsh_util_core.g_ret_sts_success)
         THEN
            wsh_util_core.get_messages ('Y'
                                       ,x_msg_summary
                                       ,x_msg_details
                                       ,x_msg_count);
            write_log (
                  '  3.5. Exception unassigning from delivery '
               || p_delivery_name);

            IF x_msg_count > 1
            THEN
               x_msg_data := x_msg_summary || x_msg_details;
               write_log ('  3.5. Message Data : ' || x_msg_data);
            ELSE
               x_msg_data := x_msg_summary;
               write_log ('  3.5. Message Data : ' || x_msg_data);
            END IF;
         ELSE
            -- Assign to new Delivery
            p_delivery_id := l_delivery_id;
            p_delivery_name := l_delivery_name;
            p_action := 'ASSIGN';                                  -- UNASSIGN
            write_log ('  3.5. Assigning to delivery ' || p_delivery_name);
            -- Call to WSH_DELIVERY_DETAILS_PUB.Detail_to_Delivery.
            wsh_delivery_details_pub.detail_to_delivery (
               p_api_version     => 1.0
              ,p_init_msg_list   => p_init_msg_list
              ,p_commit          => p_commit
              ,x_return_status   => x_return_status
              ,x_msg_count       => x_msg_count
              ,x_msg_data        => x_msg_data
              ,p_tabofdeldets    => p_tabofdeldet
              ,p_action          => p_action
              ,p_delivery_id     => p_delivery_id
              ,p_delivery_name   => p_delivery_name);

            IF (x_return_status <> wsh_util_core.g_ret_sts_success)
            THEN
               wsh_util_core.get_messages ('Y'
                                          ,x_msg_summary
                                          ,x_msg_details
                                          ,x_msg_count);
               write_log (
                     '  3.5. Exception unassigning to delivery '
                  || p_delivery_name);

               IF x_msg_count > 1
               THEN
                  x_msg_data := x_msg_summary || x_msg_details;
                  write_log ('  3.5. Message Data : ' || x_msg_data);
               ELSE
                  x_msg_data := x_msg_summary;
                  write_log ('  3.5. Message Data : ' || x_msg_data);
               END IF;
            ELSE
               write_log ('  3.5. Assigned to delivery ' || p_delivery_name);
            END IF;
         END IF;
      ELSE
         write_log (
               '  3.5. Could not find open delivery on second pull for header id '
            || p_header_id);
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         write_log (
               '  3.5. Error reassinging to open delivery for header id '
            || p_header_id);
   END reassign_open_delivery;

   /*************************************************************************
     Procedure : pick_release_order

     PURPOSE:
     Parameter:
            IN

   ************************************************************************/
   PROCEDURE pick_release_order (p_header_id       IN     NUMBER
                                ,p_from_org_id     IN     NUMBER
                                ,p_pick_rule       IN     VARCHAR2
                                ,p_return_status      OUT VARCHAR2)
   IS
      l_return_status     VARCHAR2 (1);
      l_msg_count         NUMBER (15);
      l_msg_data          VARCHAR2 (2000);
      l_count             NUMBER (15);
      l_msg_data_out      VARCHAR2 (2000);
      l_mesg              VARCHAR2 (2000);
      p_count             NUMBER (15);
      p_new_batch_id      NUMBER;
      l_rule_id           NUMBER;
      l_rule_name         VARCHAR2 (2000);
      l_batch_prefix      VARCHAR2 (2000);
      l_batch_info_rec    wsh_picking_batches_pub.batch_info_rec;
      l_request_id        NUMBER;
      v_phase             VARCHAR2 (50);
      v_status            VARCHAR2 (50);
      v_dev_status        VARCHAR2 (50);
      v_dev_phase         VARCHAR2 (50);
      v_message           VARCHAR2 (250);
      v_error_message     VARCHAR2 (3000);
      v_wait              BOOLEAN;
      l_open_deliveries   NUMBER;
   BEGIN
      write_log (
         '  3.4. Entering pick_release_order for header_id ' || p_header_id);

      BEGIN
         SELECT wpr.picking_rule_id, wpr.NAME
           INTO l_rule_id, l_rule_name
           FROM wsh_picking_rules wpr
          WHERE     wpr.organization_id = p_from_org_id
                AND wpr.NAME LIKE '%' || p_pick_rule || '%'  --'%Backordered%'
                AND TRUNC (wpr.start_date_active) <= TRUNC (SYSDATE)
                AND NVL (TRUNC (wpr.end_date_active), TRUNC (SYSDATE + 1)) >
                       TRUNC (SYSDATE)
                AND ROWNUM = 1;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_rule_id := NULL;
            l_rule_name := NULL;
      END;

      IF l_rule_id IS NOT NULL
      THEN
         l_batch_info_rec.backorders_only_flag := 'I';
         l_batch_info_rec.existing_rsvs_only_flag := 'N';
         l_batch_info_rec.order_header_id := p_header_id;
         l_batch_info_rec.from_scheduled_ship_date := NULL;
         l_batch_info_rec.organization_id := p_from_org_id;
         l_batch_info_rec.include_planned_lines := 'N';
         l_batch_info_rec.autocreate_delivery_flag := 'Y';
         l_batch_info_rec.autodetail_pr_flag := 'Y';
         l_batch_info_rec.allocation_method := 'I';
         l_batch_info_rec.pick_from_locator_id := NULL;
         l_batch_info_rec.auto_pick_confirm_flag := 'Y';
         l_batch_info_rec.autopack_flag := 'Y';
         --l_batch_info_rec.append_flag := 'Y';
         l_batch_prefix := NULL;
         wsh_picking_batches_pub.create_batch (
            p_api_version     => 1.0
           ,p_init_msg_list   => fnd_api.g_true
           ,p_commit          => fnd_api.g_true
           ,x_return_status   => l_return_status
           ,x_msg_count       => l_msg_count
           ,x_msg_data        => l_msg_data
           ,p_rule_id         => l_rule_id
           ,p_rule_name       => l_rule_name
           ,p_batch_rec       => l_batch_info_rec
           ,p_batch_prefix    => l_batch_prefix
           ,x_batch_id        => p_new_batch_id);

         IF l_return_status = 'S'
         THEN
            write_log (
                  '  3.4. Pick Release Batch Got Created Sucessfully '
               || p_new_batch_id);
         ELSE
            write_log ('  3.4. Message count ' || l_msg_count);

            IF l_msg_count = 1
            THEN
               write_log ('  3.4. Error Message ' || l_msg_data);
            ELSIF l_msg_count > 1
            THEN
               LOOP
                  p_count := p_count + 1;
                  l_msg_data :=
                     fnd_msg_pub.get (fnd_msg_pub.g_next, fnd_api.g_false);
                  write_log (
                        '  3.4. Error Message ('
                     || p_count
                     || '): '
                     || l_msg_data);
               END LOOP;
            END IF;
         END IF;

         write_log (
            '  3.4. Submitting Pick Release for Batch ' || p_new_batch_id);
         -- Release the batch Created Above
         wsh_picking_batches_pub.release_batch (
            p_api_version     => 1.0
           ,p_init_msg_list   => fnd_api.g_true
           ,p_commit          => fnd_api.g_true
           ,x_return_status   => l_return_status
           ,x_msg_count       => l_msg_count
           ,x_msg_data        => l_msg_data
           ,p_batch_id        => p_new_batch_id
           ,p_batch_name      => NULL
           ,p_log_level       => 1
           ,p_release_mode    => 'CONCURRENT'
           ,-- (ONLINE or CONCURRENT)
            x_request_id      => l_request_id);

         IF l_return_status = 'S'
         THEN
            write_log (
               '  3.4. Pick Selection List Generation ' || l_request_id);
            v_wait :=
               fnd_concurrent.wait_for_request (l_request_id
                                               ,6
                                               ,3600
                                               ,v_phase
                                               ,v_status
                                               ,v_dev_phase
                                               ,v_dev_status
                                               ,v_message);
            write_log ('  3.4. Pick Program Completion Status: ' || v_status);

            BEGIN
               write_log (
                  '  3.4. Updating DFF for Cancellation Qty to Null since WDD was repicked');

               -- 05/04/2012 CGonzalez: Changed attribute1 to attribute4
               UPDATE wsh_delivery_details
                  SET attribute4 = NULL, attribute2 = NULL, attribute3 = NULL
                WHERE    batch_id = p_new_batch_id
                      OR (    source_header_id = p_header_id
                          AND attribute4 IS NOT NULL);
            EXCEPTION
               WHEN OTHERS
               THEN
                  write_log (
                     '  3.4. Could not update DFF for Cancellation Qty to Null since WDD was repicked');
            END;

            -- Determine if the Order has other open deliveries
            l_open_deliveries := 0;

            BEGIN
                 SELECT COUNT (DISTINCT (wnd.NAME))
                   INTO l_open_deliveries
                   FROM wsh_new_deliveries wnd
                       ,wsh_delivery_assignments wda
                       ,wsh_delivery_details wdd
                  WHERE     wnd.status_code != 'CL'
                        AND wnd.delivery_id = wda.delivery_id
                        AND wda.delivery_detail_id = wdd.delivery_detail_id
                        AND EXISTS
                               (SELECT 'Same Delivery'
                                  FROM wsh_new_deliveries wnd2
                                      ,wsh_delivery_assignments wda2
                                      ,wsh_delivery_details wdd2
                                 WHERE     wnd2.delivery_id = wda2.delivery_id
                                       AND wda2.delivery_detail_id =
                                              wdd2.delivery_detail_id
                                       AND wdd2.batch_id = p_new_batch_id
                                       AND wdd2.source_header_id = p_header_id
                                       AND wdd2.batch_id != wdd.batch_id
                                       AND wdd2.source_header_id =
                                              wdd.source_header_id
                                       AND wdd2.date_scheduled =
                                              wdd.date_scheduled
                                       AND wdd2.ship_method_code =
                                              wdd.ship_method_code
                                       AND wnd2.ultimate_dropoff_location_id =
                                              wnd.ultimate_dropoff_location_id--and       wdd2.earliest_pickup_date = wnd.earliest_pickup_date
                            )
               ORDER BY wnd.NAME, wdd.delivery_detail_id;
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_open_deliveries := 0;
            END;

            IF NVL (l_open_deliveries, 0) > 0
            THEN
               -- Call to reassign to new delivery
               reassign_open_delivery (p_new_batch_id, p_header_id);
            END IF;

            BEGIN
               UPDATE xxwc_backordered_txns_lns_stg
                  SET status_code = 'PROCESSED'
                WHERE     header_id = p_header_id
                      AND status_code IN ('RESERVED')
                      AND request_id = g_request_id;
            EXCEPTION
               WHEN OTHERS
               THEN
                  write_log (
                        '  3.4. Could not update TRX post pick release for Header_id '
                     || p_header_id);
            END;

            BEGIN
               UPDATE xxwc_backordered_txns_sns_stg
                  SET status = 'PROCESSED'
                WHERE     header_id = p_header_id
                      AND status IN ('RESERVED')
                      AND request_id = g_request_id;
            EXCEPTION
               WHEN OTHERS
               THEN
                  write_log (
                        '  3.4. Could not update TRX SNs post pick release for Header_id '
                     || p_header_id);
            END;
         ELSE
            IF l_msg_count = 1
            THEN
               write_log ('  3.4. Error Message: ' || l_msg_data);
            ELSIF l_msg_count > 1
            THEN
               LOOP
                  p_count := p_count + 1;
                  l_msg_data :=
                     fnd_msg_pub.get (fnd_msg_pub.g_next, fnd_api.g_false);
                  write_log (
                        '  3.4. Error Message ('
                     || p_count
                     || '): '
                     || l_msg_data);
               END LOOP;
            END IF;

            BEGIN
               UPDATE xxwc_backordered_txns_lns_stg
                  SET status_code = 'PICK_ERR'
                WHERE     header_id = p_header_id
                      AND status_code IN ('RESERVED')
                      AND request_id = g_request_id;
            EXCEPTION
               WHEN OTHERS
               THEN
                  write_log (
                        '  3.4. Could not update TRX post pick release for Header_id '
                     || p_header_id);
            END;

            BEGIN
               UPDATE xxwc_backordered_txns_sns_stg
                  SET status = 'PICK_ERR'
                WHERE     header_id = p_header_id
                      AND status IN ('RESERVED')
                      AND request_id = g_request_id;
            EXCEPTION
               WHEN OTHERS
               THEN
                  write_log (
                        '  3.4. Could not update TRX SNs post pick release for Header_id '
                     || p_header_id);
            END;
         END IF;
      ELSE
         write_log (
            '  3.4. Could not find pick rule for header_id ' || p_header_id);
      END IF;

      write_log (
         '  3.4. Exiting pick_release_order for header_id ' || p_header_id);
      p_return_status := l_return_status;
   END pick_release_order;

   PROCEDURE backorder_deliveries (p_return_status OUT VARCHAR2)
   IS
      CURSOR cur_backord_ln_stg
      IS
         SELECT DISTINCT (x1.delivery_id)
           FROM xxwc_backordered_txns_lns_stg x1
          WHERE x1.status_code = 'NEW' AND x1.request_id = g_request_id;

      x_return_status          VARCHAR2 (3000);
      x_msg_count              NUMBER;
      x_msg_data               VARCHAR2 (3000);
      p_api_version_number     NUMBER;
      init_msg_list            VARCHAR2 (30);
      x_msg_details            VARCHAR2 (3000);
      x_msg_summary            VARCHAR2 (3000);
      p_validation_level       NUMBER;
      p_commit                 VARCHAR2 (30);
      p_action_code            VARCHAR2 (15);
      p_delivery_id            NUMBER;
      p_delivery_name          VARCHAR2 (30);
      p_asg_trip_id            NUMBER;
      p_asg_trip_name          VARCHAR2 (30);
      p_asg_pickup_stop_id     NUMBER;
      p_asg_pickup_loc_id      NUMBER;
      p_asg_pickup_loc_code    VARCHAR2 (30);
      p_asg_pickup_arr_date    DATE;
      p_asg_pickup_dep_date    DATE;
      p_asg_dropoff_stop_id    NUMBER;
      p_asg_dropoff_loc_id     NUMBER;
      p_asg_dropoff_loc_code   VARCHAR2 (30);
      p_asg_dropoff_arr_date   DATE;
      p_asg_dropoff_dep_date   DATE;
      p_sc_action_flag         VARCHAR2 (10);
      p_sc_close_trip_flag     VARCHAR2 (10);
      p_sc_create_bol_flag     VARCHAR2 (10);
      p_sc_stage_del_flag      VARCHAR2 (10);
      p_sc_trip_ship_method    VARCHAR2 (30);
      p_sc_actual_dep_date     VARCHAR2 (30);
      p_sc_report_set_id       NUMBER;
      p_sc_report_set_name     VARCHAR2 (60);
      p_wv_override_flag       VARCHAR2 (10);
      x_trip_id                VARCHAR2 (30);
      x_trip_name              VARCHAR2 (30);
      /*Handle exceptions*/
      fail_api                 EXCEPTION;
   BEGIN
      x_return_status := wsh_util_core.g_ret_sts_success;

      FOR c1 IN cur_backord_ln_stg
      LOOP
         EXIT WHEN cur_backord_ln_stg%NOTFOUND;
         write_log ('  2. Backordering delivery_id ' || c1.delivery_id);
         p_action_code := 'CONFIRM';
         p_sc_action_flag := 'C';
         p_sc_stage_del_flag := 'N';

         SELECT delivery_id, NAME
           INTO p_delivery_id, p_delivery_name
           FROM wsh_new_deliveries
          WHERE delivery_id = c1.delivery_id;

         --p_sc_trip_ship_method := 'GROUND';
         wsh_deliveries_pub.delivery_action (
            p_api_version_number     => 1.0
           ,p_init_msg_list          => init_msg_list
           ,x_return_status          => x_return_status
           ,x_msg_count              => x_msg_count
           ,x_msg_data               => x_msg_data
           ,p_action_code            => p_action_code
           ,p_delivery_id            => p_delivery_id
           ,p_delivery_name          => p_delivery_name
           ,p_asg_trip_id            => p_asg_trip_id
           ,p_asg_trip_name          => p_asg_trip_name
           ,p_asg_pickup_stop_id     => p_asg_pickup_stop_id
           ,p_asg_pickup_loc_id      => p_asg_pickup_loc_id
           ,p_asg_pickup_loc_code    => p_asg_pickup_loc_code
           ,p_asg_pickup_arr_date    => p_asg_pickup_arr_date
           ,p_asg_pickup_dep_date    => p_asg_pickup_dep_date
           ,p_asg_dropoff_stop_id    => p_asg_dropoff_stop_id
           ,p_asg_dropoff_loc_id     => p_asg_dropoff_loc_id
           ,p_asg_dropoff_loc_code   => p_asg_dropoff_loc_code
           ,p_asg_dropoff_arr_date   => p_asg_dropoff_arr_date
           ,p_asg_dropoff_dep_date   => p_asg_dropoff_dep_date
           ,p_sc_action_flag         => p_sc_action_flag
           ,p_sc_close_trip_flag     => p_sc_close_trip_flag
           ,p_sc_create_bol_flag     => p_sc_create_bol_flag
           ,p_sc_stage_del_flag      => p_sc_stage_del_flag
           ,p_sc_trip_ship_method    => p_sc_trip_ship_method
           ,p_sc_actual_dep_date     => p_sc_actual_dep_date
           ,p_sc_report_set_id       => p_sc_report_set_id
           ,p_sc_report_set_name     => p_sc_report_set_name
           ,p_wv_override_flag       => p_wv_override_flag
           ,x_trip_id                => x_trip_id
           ,x_trip_name              => x_trip_name);

         IF (x_return_status <> wsh_util_core.g_ret_sts_success)
         THEN
            wsh_util_core.get_messages ('Y'
                                       ,x_msg_summary
                                       ,x_msg_details
                                       ,x_msg_count);

            IF x_msg_count > 1
            THEN
               x_msg_data := x_msg_summary || x_msg_details;
            ELSE
               x_msg_data := x_msg_summary;
            END IF;

            write_log (
                  '  2. Exception backordering delivery_id '
               || c1.delivery_id
               || '. Error: '
               || x_msg_data);

            BEGIN
               UPDATE xxwc_backordered_txns_lns_stg
                  SET status_code = 'ERR_BACKORD'
                WHERE     request_id = g_request_id
                      AND delivery_id = c1.delivery_id;
            EXCEPTION
               WHEN OTHERS
               THEN
                  write_log ('  2. Could not update line staging table');
            END;

            BEGIN
               UPDATE xxwc_backordered_txns_sns_stg x1
                  SET x1.status = 'ERR_BACKORD'
                WHERE     x1.request_id = g_request_id
                      AND EXISTS
                             (SELECT 'Has Serials'
                                FROM xxwc_backordered_txns_lns_stg x2
                               WHERE     x2.request_id = g_request_id
                                     AND x2.delivery_id = c1.delivery_id
                                     AND x2.delivery_detail_id =
                                            x1.delivery_detail_id);
            EXCEPTION
               WHEN OTHERS
               THEN
                  write_log (
                     '  2. Could not update serial number staging table');
            END;

            p_return_status := 'E';
         ELSE
            write_log (
               '  2. Successfully backordered delivery_id ' || c1.delivery_id);
            p_return_status := 'S';

            BEGIN
               UPDATE xxwc_backordered_txns_lns_stg
                  SET status_code = 'BACKORDERED'
                WHERE     request_id = g_request_id
                      AND delivery_id = c1.delivery_id;
            EXCEPTION
               WHEN OTHERS
               THEN
                  write_log ('  2. Could not update line staging table');
            END;

            BEGIN
               UPDATE xxwc_backordered_txns_sns_stg x1
                  SET x1.status = 'BACKORDERED'
                WHERE     x1.request_id = g_request_id
                      AND EXISTS
                             (SELECT 'Has Serials'
                                FROM xxwc_backordered_txns_lns_stg x2
                               WHERE     x2.request_id = g_request_id
                                     AND x2.delivery_id = c1.delivery_id
                                     AND x2.delivery_detail_id =
                                            x1.delivery_detail_id);
            EXCEPTION
               WHEN OTHERS
               THEN
                  write_log (
                     '  2. Could not update serial number staging table');
            END;
         END IF;
      END LOOP;

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         write_log ('  2. Error in backorder_deliveries. Error ' || SQLERRM);
         p_return_status := 'E';
   END backorder_deliveries;

   PROCEDURE cancel_order (p_return_status OUT VARCHAR2)
   IS
      CURSOR cur_backord_ln_stg
      IS
         SELECT DISTINCT (x1.header_id)
           FROM xxwc_backordered_txns_lns_stg x1
          WHERE     x1.status_code = 'TRANSFERRED'
                AND x1.request_id = g_request_id;

      /*select    header_id
              , line_id
      from    oe_order_lines oola
      where   oola.booked_flag = 'Y'
      and     oola.cancelled_flag = 'N'
      and     header_id in (
                              select  distinct(x1.header_id)
                              from    xxwc_backordered_txns_lns_stg x1
                              where   x1.status_code = 'TRANSFERRED'
                              and     x1.request_id = g_request_id);*/
      l_header_rec               oe_order_pub.header_rec_type;
      l_line_tbl                 oe_order_pub.line_tbl_type;
      l_action_request_tbl       oe_order_pub.request_tbl_type;
      l_header_adj_tbl           oe_order_pub.header_adj_tbl_type;
      l_line_adj_tbl             oe_order_pub.line_adj_tbl_type;
      l_header_scr_tbl           oe_order_pub.header_scredit_tbl_type;
      l_line_scredit_tbl         oe_order_pub.line_scredit_tbl_type;
      l_return_status            VARCHAR2 (1000);
      l_msg_count                NUMBER;
      l_msg_data                 VARCHAR2 (1000);
      p_api_version_number       NUMBER := 1.0;
      p_init_msg_list            VARCHAR2 (10) := fnd_api.g_false;
      p_return_values            VARCHAR2 (10) := fnd_api.g_false;
      p_action_commit            VARCHAR2 (10) := fnd_api.g_false;
      x_return_status            VARCHAR2 (1);
      x_msg_count                NUMBER;
      x_msg_data                 VARCHAR2 (100);
      p_action_request_tbl       oe_order_pub.request_tbl_type
                                    := oe_order_pub.g_miss_request_tbl;
      x_header_rec               oe_order_pub.header_rec_type;
      x_header_val_rec           oe_order_pub.header_val_rec_type;
      x_header_adj_tbl           oe_order_pub.header_adj_tbl_type;
      x_header_adj_val_tbl       oe_order_pub.header_adj_val_tbl_type;
      x_header_price_att_tbl     oe_order_pub.header_price_att_tbl_type;
      x_header_adj_att_tbl       oe_order_pub.header_adj_att_tbl_type;
      x_header_adj_assoc_tbl     oe_order_pub.header_adj_assoc_tbl_type;
      x_header_scredit_tbl       oe_order_pub.header_scredit_tbl_type;
      x_header_scredit_val_tbl   oe_order_pub.header_scredit_val_tbl_type;
      x_line_tbl                 oe_order_pub.line_tbl_type;
      x_line_val_tbl             oe_order_pub.line_val_tbl_type;
      x_line_adj_tbl             oe_order_pub.line_adj_tbl_type;
      x_line_adj_val_tbl         oe_order_pub.line_adj_val_tbl_type;
      x_line_price_att_tbl       oe_order_pub.line_price_att_tbl_type;
      x_line_adj_att_tbl         oe_order_pub.line_adj_att_tbl_type;
      x_line_adj_assoc_tbl       oe_order_pub.line_adj_assoc_tbl_type;
      x_line_scredit_tbl         oe_order_pub.line_scredit_tbl_type;
      x_line_scredit_val_tbl     oe_order_pub.line_scredit_val_tbl_type;
      x_lot_serial_tbl           oe_order_pub.lot_serial_tbl_type;
      x_lot_serial_val_tbl       oe_order_pub.lot_serial_val_tbl_type;
      x_action_request_tbl       oe_order_pub.request_tbl_type;
      x_debug_file               VARCHAR2 (100);
      l_msg_index_out            NUMBER (10);
      l_line_tbl_index           NUMBER;
      l_org_id                   NUMBER;
   BEGIN
      oe_msg_pub.initialize;
      oe_debug_pub.initialize;
      x_debug_file := oe_debug_pub.set_debug_mode ('FILE');
      l_line_tbl_index := 0;

      FOR c1 IN cur_backord_ln_stg
      LOOP
         EXIT WHEN cur_backord_ln_stg%NOTFOUND;
         l_org_id := NULL;
         l_header_rec := oe_order_pub.g_miss_header_rec;
         l_header_rec.header_id := c1.header_id;
         l_header_rec.cancelled_flag := 'Y';
         l_header_rec.change_reason := '2';                 -- 'Not provided';
         l_header_rec.operation := oe_globals.g_opr_update;

         BEGIN
            SELECT org_id
              INTO l_org_id
              FROM oe_order_headers
             WHERE header_id = c1.header_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_org_id := mo_global.get_current_org_id ();
         END;

         /*l_line_tbl_index := l_line_tbl_index + 1;

         l_line_tbl(l_line_tbl_index) := OE_ORDER_PUB.G_MISS_LINE_REC;

         l_line_tbl(l_line_tbl_index).ordered_quantity := 0;
         l_line_tbl(l_line_tbl_index).header_id := c1.header_id;
         l_line_tbl(l_line_tbl_index).line_id := c1.line_id;
         l_line_tbl(l_line_tbl_index).change_reason := '2'; -- Incorrect Quantity (Confirm code in other instances)
         l_line_tbl(l_line_tbl_index).operation := OE_GLOBALS.G_OPR_UPDATE; */

         -- Call To Process Order API
         oe_order_pub.process_order (
            p_org_id                   => l_org_id
           ,p_api_version_number       => 1.0
           ,p_init_msg_list            => fnd_api.g_false
           ,p_return_values            => fnd_api.g_false
           ,p_action_commit            => fnd_api.g_false
           ,x_return_status            => l_return_status
           ,x_msg_count                => l_msg_count
           ,x_msg_data                 => l_msg_data
           ,p_header_rec               => l_header_rec
           ,p_line_tbl                 => l_line_tbl
           ,p_action_request_tbl       => l_action_request_tbl
           ,x_header_rec               => x_header_rec
           ,x_header_val_rec           => x_header_val_rec
           ,x_header_adj_tbl           => x_header_adj_tbl
           ,x_header_adj_val_tbl       => x_header_adj_val_tbl
           ,x_header_price_att_tbl     => x_header_price_att_tbl
           ,x_header_adj_att_tbl       => x_header_adj_att_tbl
           ,x_header_adj_assoc_tbl     => x_header_adj_assoc_tbl
           ,x_header_scredit_tbl       => x_header_scredit_tbl
           ,x_header_scredit_val_tbl   => x_header_scredit_val_tbl
           ,x_line_tbl                 => x_line_tbl
           ,x_line_val_tbl             => x_line_val_tbl
           ,x_line_adj_tbl             => x_line_adj_tbl
           ,x_line_adj_val_tbl         => x_line_adj_val_tbl
           ,x_line_price_att_tbl       => x_line_price_att_tbl
           ,x_line_adj_att_tbl         => x_line_adj_att_tbl
           ,x_line_adj_assoc_tbl       => x_line_adj_assoc_tbl
           ,x_line_scredit_tbl         => x_line_scredit_tbl
           ,x_line_scredit_val_tbl     => x_line_scredit_val_tbl
           ,x_lot_serial_tbl           => x_lot_serial_tbl
           ,x_lot_serial_val_tbl       => x_lot_serial_val_tbl
           ,x_action_request_tbl       => x_action_request_tbl);

         -- Check the return status
         IF l_return_status = fnd_api.g_ret_sts_success
         THEN
            write_log ('  4. Cancelled order Sucess');

            BEGIN
               UPDATE xxwc_backordered_txns_lns_stg
                  SET status_code = 'PROCESSED'
                WHERE     header_id = c1.header_id
                      --and       line_id = c1.line_id
                      AND status_code IN ('TRANSFERRED')
                      AND request_id = g_request_id;
            EXCEPTION
               WHEN OTHERS
               THEN
                  write_log (
                        '  4. Could not update TRX post Order Cancellation for Header_id '
                     || c1.header_id);
            END;

            BEGIN
               UPDATE xxwc_backordered_txns_sns_stg
                  SET status = 'PROCESSED'
                WHERE     header_id = c1.header_id
                      --and       line_id = c1.line_id
                      AND status IN ('TRANSFERRED')
                      AND request_id = g_request_id;
            EXCEPTION
               WHEN OTHERS
               THEN
                  write_log (
                        '  4. Could not update TRX SNs post Order Cancellation for Header_id '
                     || c1.header_id);
            END;
         ELSE
            write_log ('  4. Order Cancellation Failed');

            BEGIN
               UPDATE xxwc_backordered_txns_lns_stg
                  SET status_code = 'CANCEL_ERR'
                WHERE     header_id = c1.header_id
                      --and       line_id = c1.line_id
                      AND status_code IN ('TRANSFERRED')
                      AND request_id = g_request_id;
            EXCEPTION
               WHEN OTHERS
               THEN
                  write_log (
                        '  4. Could not update TRX post Order Cancellation for Header_id '
                     || c1.header_id);
            END;

            BEGIN
               UPDATE xxwc_backordered_txns_sns_stg
                  SET status = 'CANCEL_ERR'
                WHERE     header_id = c1.header_id
                      --and       line_id = c1.line_id
                      AND status IN ('TRANSFERRED')
                      AND request_id = g_request_id;
            EXCEPTION
               WHEN OTHERS
               THEN
                  write_log (
                        '  4. Could not update TRX SNs post Order Cancellation for Header_id '
                     || c1.header_id);
            END;
         END IF;

         write_log (
               '  4. OM Debug file: '
            || oe_debug_pub.g_dir
            || '/'
            || oe_debug_pub.g_file);

         FOR i IN 1 .. l_msg_count
         LOOP
            oe_msg_pub.get (p_msg_index       => i
                           ,p_encoded         => fnd_api.g_false
                           ,p_data            => l_msg_data
                           ,p_msg_index_out   => l_msg_index_out);
            write_log ('  4. message is: ' || l_msg_data);
            write_log ('  4. message index is: ' || l_msg_index_out);
         END LOOP;

         -- debug output
         DBMS_OUTPUT.put_line ('Debug Output');

         FOR i IN 1 .. oe_debug_pub.g_debug_count
         LOOP
            write_log (
               '  4. OE Debug Message: ' || oe_debug_pub.g_debug_tbl (i));
         END LOOP;
      END LOOP;

      p_return_status := x_return_status;
   EXCEPTION
      WHEN OTHERS
      THEN
         write_log ('  4. Error in cancel_order. Error ' || SQLERRM);
         p_return_status := 'E';
   END cancel_order;

   -- May need to add a procedure to return the OH, ATR, ATT
   /*************************************************************************
     Procedure : get_onhand_quantity

     PURPOSE:
     Parameter:
            IN

   ************************************************************************/

   /*************************************************************************
  Procedure : main_process

  PURPOSE:   This procedure creates file for the open invoices to
             Bill Trust
  Parameter:

************************************************************************/
   PROCEDURE main_process (errbuf              OUT VARCHAR2
                          ,retcode             OUT VARCHAR2
                          ,--p_delivery_name       IN VARCHAR2,
                           p_order_number   IN     VARCHAR2)
   IS
      CURSOR cur_get_backordered_lines
      IS
           SELECT wdd.delivery_detail_id
                 ,wdd.source_header_id ord_header_id
                 ,wdd.source_line_id ord_line_id
                 ,wdd.inventory_item_id
                 ,wdd.organization_id
                 ,mtrl.line_id mo_line_id
                 ,mtrl.from_subinventory_code
                 ,mtrl.to_subinventory_code
                 ,mtrl.quantity_delivered
                 ,flv.meaning serial_status
                 ,flv2.meaning lot_status
                 ,wdd.released_status
                 ,wdd.split_from_delivery_detail_id
                 ,oola.ordered_quantity ol_ord_qty
                 ,wdd.requested_quantity wdd_req_qty
                 ,NVL (wdd.cancelled_quantity, 0) wdd_cancel_qty
                 ,NVL (wdd.shipped_quantity, 0) wdd_ship_qty
                 ,NVL (wdd.attribute4, 0) wdd_dff_cancelation_qty
                 -- 05/04/2012 CGonzalez: Changed attribute1 to attribute4
                 -- 05/04/2012 , (oola.ordered_quantity - nvl(to_number(wdd.attribute4),0)) new_ordered_qty -- 05/04/2012 CGonzalez: Changed attribute1 to attribute4
                 , (  wdd.requested_quantity
                    - NVL (TO_NUMBER (wdd.attribute4), 0))
                     new_ordered_qty
                 ,wdd_prt.move_order_line_id prt_mo_line_id
                 ,wdd.source_header_number ord_order_num
                 ,wdd.source_line_number
                 -- 02/27/2012 CG
                 ,NVL (wdd.attribute2, 'N') hold_reserve_flag
                 ,TO_DATE (wdd.attribute3, 'YYYY/MM/DD HH24:MI:SS')
                     new_sched_ship_date
                 , (CASE
                       WHEN UPPER (ottl.NAME) LIKE '%RENTAL%' THEN 'RENTAL'
                       ELSE 'GENERAL'
                    END)
                     order_gen_type
                 , (CASE
                       WHEN UPPER (ottl.NAME) LIKE '%RENTAL%' THEN 'RentalHLD'
                       ELSE 'GeneralHLD'
                    END)
                     holding_subinv
                 , (CASE
                       WHEN UPPER (ottl.NAME) LIKE '%RENTAL%'
                       THEN
                          'Holding Rental'
                       ELSE
                          'Holing Std'
                    END)
                     holding_pick_rule
             FROM wsh_delivery_details wdd
                 ,wsh_delivery_details wdd_prt
                 ,oe_order_lines oola
                 ,mtl_txn_request_lines mtrl
                 ,mtl_system_items_b msib
                 ,fnd_lookup_values flv
                 ,fnd_lookup_values flv2
                 ,oe_order_headers ooha
                 ,oe_transaction_types ottl
            WHERE     wdd.source_code = 'OE'
                  AND wdd.source_header_number = p_order_number
                  AND wdd.released_status = 'B'
                  -- 05/04/2012 CGonzalez: Changed attribute1 to attribute4
                  AND wdd.attribute4 IS NOT NULL
                  -- Cancellation Quantity/Assume that it will be populated(Clear after processing?)
                  AND wdd.split_from_delivery_detail_id =
                         wdd_prt.delivery_detail_id(+)
                  AND wdd.source_line_id = oola.line_id
                  AND NVL (wdd.split_from_delivery_detail_id
                          ,wdd.delivery_detail_id) =
                         mtrl.txn_source_line_detail_id(+)
                  --
                  AND mtrl.line_id IN
                         (SELECT MAX (mtrl2.line_id)
                            FROM mtl_txn_request_lines mtrl2
                           WHERE NVL (
                                    wdd.split_from_delivery_detail_id
                                   ,wdd.delivery_detail_id) =
                                    mtrl2.txn_source_line_detail_id)
                  --
                  AND wdd.inventory_item_id = msib.inventory_item_id
                  AND wdd.organization_id = msib.organization_id
                  AND msib.serial_number_control_code = flv.lookup_code
                  AND flv.lookup_type = 'MTL_SERIAL_NUMBER'
                  AND msib.lot_control_code = flv2.lookup_code
                  AND flv2.lookup_type = 'MTL_LOT_CONTROL'
                  -- 02/27/2012
                  AND oola.header_id = ooha.header_id
                  AND ooha.order_type_id = ottl.transaction_type_id
         ORDER BY wdd.source_header_id, wdd.source_line_id;

      CURSOR cur_backord_ln_stg
      IS
           SELECT x1.ROWID row_id, x1.*
             FROM xxwc_backordered_txns_lns_stg x1
            WHERE     x1.status_code NOT IN ('ERROR', 'PROCESSED')
                  AND x1.request_id = g_request_id
         ORDER BY line_id ASC, wdd_cancellation_qty DESC;

      CURSOR cur_backord_sn_stg (
         p_delivery_detail_id    NUMBER
        ,p_header_id             NUMBER
        ,p_line_id               NUMBER)
      IS
         SELECT x1.ROWID row_id, x1.*
           FROM xxwc_backordered_txns_sns_stg x1
          WHERE     x1.delivery_detail_id = p_delivery_detail_id
                AND x1.header_id = p_header_id
                AND x1.line_id = p_line_id
                AND x1.status NOT IN ('ERROR', 'PROCESSED')
                AND x1.request_id = g_request_id;

      CURSOR cur_reserved_ln_stg
      IS
         SELECT DISTINCT (header_id), ship_from_org_id, holding_picking_rule
           FROM xxwc_backordered_txns_lns_stg x1
          WHERE x1.status_code = 'RESERVED' -- 02/24/2012 CG
                AND x1.hold_reserve_flag = 'N' -- Not set to hold so it can be picked
                AND x1.request_id = g_request_id;

      l_count_serials   NUMBER;
      l_return_status   VARCHAR2 (240);
   BEGIN
      write_log ('Begining WSH Backordered Transactions Processing ');
      write_log ('========================================================');
      write_log (
         'Processing backorderd lines for order number  ' || p_order_number);
      write_log (' ');
      g_request_id := fnd_global.conc_request_id;
      fnd_global.apps_initialize (user_id        => fnd_global.user_id
                                 ,--   18279,
                                  resp_id        => 20634
                                 ,resp_appl_id   => 401);
      --Initialize the Out Parameter
      errbuf := '';
      retcode := '0';
      -- 1
      -- Query to retrieve the lines associated to the order or delivery
      -- and insert into the global temp table xxwc_backordered_txns_lns_stg
      write_log (
         '1. Opening cursor to pull the backordered lines for a specific sales order');

      FOR c1 IN cur_get_backordered_lines
      LOOP
         EXIT WHEN cur_get_backordered_lines%NOTFOUND;

         BEGIN
            INSERT
              INTO xxwc_backordered_txns_lns_stg (delivery_detail_id
                                                      ,header_id
                                                      ,line_id
                                                      ,inventory_item_id
                                                      ,ship_from_org_id
                                                      ,move_order_line_id
                                                      ,from_subinventory_code
                                                      ,to_subinventory_code
                                                      ,mo_delivered_qty
                                                      ,serial_num_status
                                                      ,lot_status
                                                      ,wdd_release_status
                                                      ,split_from_dd_id
                                                      ,ol_ordered_qty
                                                      ,wdd_requested_qty
                                                      ,wdd_cancelled_qty
                                                      ,wdd_shipped_qty
                                                      ,wdd_cancellation_qty
                                                      ,new_ordered_quantity
                                                      -- 02/27/2012
                                                      ,new_sched_ship_date
                                                      ,hold_reserve_flag
                                                      ,order_type
                                                      ,holding_subinventory
                                                      ,holding_picking_rule
                                                      -- 02/27/2012
                                                      ,status_code
                                                      ,request_id)
            VALUES (c1.delivery_detail_id
                   ,c1.ord_header_id
                   ,c1.ord_line_id
                   ,c1.inventory_item_id
                   ,c1.organization_id
                   ,c1.mo_line_id
                   ,c1.from_subinventory_code
                   ,c1.to_subinventory_code
                   ,c1.quantity_delivered
                   ,c1.serial_status
                   ,c1.lot_status
                   ,c1.released_status
                   ,c1.split_from_delivery_detail_id
                   ,c1.ol_ord_qty
                   ,c1.wdd_req_qty
                   ,c1.wdd_cancel_qty
                   ,c1.wdd_ship_qty
                   ,c1.wdd_dff_cancelation_qty
                   ,c1.new_ordered_qty
                   -- 02/27/2012
                   ,c1.new_sched_ship_date
                   ,c1.hold_reserve_flag
                   ,c1.order_gen_type
                   ,c1.holding_subinv
                   ,c1.holding_pick_rule
                   -- 02/27/2012
                   ,'NEW'
                   ,g_request_id);

            IF c1.serial_status != 'No serial number control'
            THEN
               -- 1.2
               -- Based on the data in temp table xxwc_backordered_txns_lns_stg
               -- Retrieve the serial numbers associated to the move order that was backordered
               write_log (
                  '  1.1. Pulling ESNs that are still in staging for the specified delivery detail and mode order');

               BEGIN
                  INSERT INTO xxwc_backordered_txns_sns_stg (
                                 delivery_detail_id
                                ,header_id
                                ,line_id
                                ,move_order_line_id
                                ,from_subinventory
                                ,transfer_subinventory
                                ,inventory_item_id
                                ,ship_from_org_id
                                ,serial_number
                                ,status
                                ,request_id)
                       SELECT c1.delivery_detail_id
                             ,c1.ord_header_id
                             ,c1.ord_line_id
                             ,mmt.source_line_id
                             ,mmt.subinventory_code
                             ,mmt.transfer_subinventory
                             ,mmt.inventory_item_id
                             ,mmt.organization_id
                             ,mut.serial_number
                             ,'NEW'
                             ,g_request_id
                         FROM mtl_material_transactions mmt
                             ,mtl_unit_transactions mut
                             ,mtl_serial_numbers msn
                        WHERE     mmt.source_line_id = c1.mo_line_id
                              -- Move order line id
                              AND mmt.organization_id = c1.organization_id
                              --02/02/12 and      msn.current_subinventory_code != c1.from_subinventory_code
                              AND mmt.transaction_type_id = 52
                              AND mmt.transaction_quantity < 0
                              AND mmt.transaction_id = mut.transaction_id
                              AND mmt.organization_id =
                                     msn.current_organization_id
                              AND mmt.inventory_item_id = msn.inventory_item_id
                              AND mut.serial_number = msn.serial_number
                              AND msn.current_status = 3 -- Currently In Store
                     ORDER BY mmt.transaction_id, mut.serial_number;

                  l_count_serials := NULL;

                  BEGIN
                     SELECT COUNT (*)
                       INTO l_count_serials
                       FROM xxwc_backordered_txns_sns_stg
                      WHERE     delivery_detail_id = c1.delivery_detail_id
                            AND header_id = c1.ord_header_id
                            AND line_id = c1.ord_line_id
                            AND move_order_line_id = c1.mo_line_id
                            AND request_id = g_request_id;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        l_count_serials := 0;
                  END;

                  write_log (
                        '  1.1. WDD '
                     || c1.delivery_detail_id
                     || ' MO Line ID '
                     || c1.mo_line_id
                     || ' - ESN Count '
                     || l_count_serials);
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     write_log (
                           '  1.1. Error pulling serials for WDD '
                        || c1.delivery_detail_id
                        || ' MO Line ID '
                        || c1.mo_line_id);
                     write_log ('  1.1. Error:' || SQLERRM);
                     write_error (
                        (   '1.1. Error inserting ESNs for MO '
                         || c1.mo_line_id
                         || '. Err: '
                         || SQLERRM)
                       ,'main_process step 1.2');
               END;
            END IF;

            IF c1.lot_status != 'No lot control'
            THEN
               -- 1.2
               -- Based on the data in temp table xxwc_backordered_txns_lns_stg
               -- Retrieve the serial numbers associated to the move order that was backordered
               write_log (
                  '  1.1. Pulling LOTs that are still in staging for the specified delivery detail and mode order');

               BEGIN
                  INSERT INTO xxwc_backordered_txns_sns_stg (
                                 delivery_detail_id
                                ,header_id
                                ,line_id
                                ,move_order_line_id
                                ,from_subinventory
                                ,transfer_subinventory
                                ,inventory_item_id
                                ,ship_from_org_id
                                ,lot_number
                                ,lot_quantity
                                ,status
                                ,request_id)
                       SELECT c1.delivery_detail_id
                             ,c1.ord_header_id
                             ,c1.ord_line_id
                             ,mmt.source_line_id
                             ,mmt.subinventory_code
                             ,mmt.transfer_subinventory
                             ,mmt.inventory_item_id
                             ,mmt.organization_id
                             ,mtln.lot_number
                             ,mtln.transaction_quantity
                             ,'NEW'
                             ,g_request_id
                         FROM mtl_material_transactions mmt
                             ,mtl_transaction_lot_numbers mtln
                             ,mtl_lot_numbers mln
                        WHERE     mmt.source_line_id = c1.mo_line_id
                              -- Move order line id
                              AND mmt.organization_id = c1.organization_id
                              AND mmt.transaction_type_id = 52
                              AND mmt.transaction_quantity < 0
                              AND mmt.transaction_id = mtln.transaction_id
                              AND mtln.inventory_item_id =
                                     mln.inventory_item_id
                              AND mtln.organization_id = mln.organization_id
                              AND mtln.lot_number = mln.lot_number
                              AND ABS (mtln.transaction_quantity) =
                                     c1.wdd_req_qty
                              -- Not already used
                              AND NOT EXISTS
                                         (SELECT 'Not Used by other WDD'
                                            FROM xxwc_backordered_txns_sns_stg x1
                                           WHERE     x1.header_id =
                                                        c1.ord_header_id
                                                 AND x1.line_id =
                                                        c1.ord_line_id
                                                 AND x1.move_order_line_id =
                                                        mmt.source_line_id
                                                 AND x1.from_subinventory =
                                                        mmt.subinventory_code
                                                 AND x1.transfer_subinventory =
                                                        mmt.transfer_subinventory
                                                 AND x1.inventory_item_id =
                                                        mmt.inventory_item_id
                                                 AND x1.ship_from_org_id =
                                                        mmt.organization_id
                                                 AND x1.lot_number =
                                                        mtln.lot_number
                                                 AND x1.request_id =
                                                        g_request_id)
                              -- Not assigned to another delivery detail for the same line
                              AND NOT EXISTS
                                         (SELECT 'Not Used on another delivery detail'
                                            FROM wsh_delivery_details wdd
                                           WHERE     wdd.source_header_id =
                                                        c1.ord_header_id
                                                 AND wdd.source_line_id =
                                                        c1.ord_line_id
                                                 AND wdd.inventory_item_id =
                                                        mmt.inventory_item_id
                                                 AND wdd.organization_id =
                                                        mmt.organization_id
                                                 AND wdd.lot_number =
                                                        mtln.lot_number
                                                 AND wdd.delivery_detail_id !=
                                                        c1.delivery_detail_id)
                              AND ROWNUM = 1
                     ORDER BY mmt.transaction_id, mtln.lot_number;

                  l_count_serials := NULL;

                  BEGIN
                     SELECT lot_quantity
                       INTO l_count_serials
                       FROM xxwc_backordered_txns_sns_stg
                      WHERE     delivery_detail_id = c1.delivery_detail_id
                            AND header_id = c1.ord_header_id
                            AND line_id = c1.ord_line_id
                            AND move_order_line_id = c1.mo_line_id
                            AND request_id = g_request_id;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        l_count_serials := 0;
                  END;

                  write_log (
                        '  1.1. WDD '
                     || c1.delivery_detail_id
                     || ' MO Line ID '
                     || c1.mo_line_id
                     || ' New Qty Qty '
                     || c1.new_ordered_qty
                     || ' - Lots found with same Qty '
                     || l_count_serials);

                  IF NVL (l_count_serials, 0) = 0
                  THEN
                     INSERT INTO xxwc_backordered_txns_sns_stg (
                                    delivery_detail_id
                                   ,header_id
                                   ,line_id
                                   ,move_order_line_id
                                   ,from_subinventory
                                   ,transfer_subinventory
                                   ,inventory_item_id
                                   ,ship_from_org_id
                                   ,lot_number
                                   ,lot_quantity
                                   ,status
                                   ,request_id)
                          SELECT c1.delivery_detail_id
                                ,c1.ord_header_id
                                ,c1.ord_line_id
                                ,mmt.source_line_id
                                ,mmt.subinventory_code
                                ,mmt.transfer_subinventory
                                ,mmt.inventory_item_id
                                ,mmt.organization_id
                                ,mtln.lot_number
                                ,mtln.transaction_quantity
                                ,'NEW'
                                ,g_request_id
                            FROM mtl_material_transactions mmt
                                ,mtl_transaction_lot_numbers mtln
                                ,mtl_lot_numbers mln
                           WHERE     mmt.source_line_id = c1.mo_line_id
                                 -- Move order line id
                                 AND mmt.organization_id = c1.organization_id
                                 AND mmt.transaction_type_id = 52
                                 AND mmt.transaction_quantity < 0
                                 AND mmt.transaction_id = mtln.transaction_id
                                 AND mtln.inventory_item_id =
                                        mln.inventory_item_id
                                 AND mtln.organization_id = mln.organization_id
                                 AND mtln.lot_number = mln.lot_number
                                 AND ABS (mtln.transaction_quantity) >=
                                        c1.wdd_req_qty
                                 AND NOT EXISTS
                                            (SELECT 'Not Used by other WDD'
                                               FROM xxwc_backordered_txns_sns_stg x1
                                              WHERE     x1.header_id =
                                                           c1.ord_header_id
                                                    AND x1.line_id =
                                                           c1.ord_line_id
                                                    AND x1.move_order_line_id =
                                                           mmt.source_line_id
                                                    AND x1.from_subinventory =
                                                           mmt.subinventory_code
                                                    AND x1.transfer_subinventory =
                                                           mmt.transfer_subinventory
                                                    AND x1.inventory_item_id =
                                                           mmt.inventory_item_id
                                                    AND x1.ship_from_org_id =
                                                           mmt.organization_id
                                                    AND x1.lot_number =
                                                           mtln.lot_number
                                                    AND x1.request_id =
                                                           g_request_id)
                                 AND ROWNUM = 1
                        ORDER BY mmt.transaction_id, mtln.lot_number;

                     l_count_serials := NULL;

                     BEGIN
                        SELECT lot_quantity
                          INTO l_count_serials
                          FROM xxwc_backordered_txns_sns_stg
                         WHERE     delivery_detail_id = c1.delivery_detail_id
                               AND header_id = c1.ord_header_id
                               AND line_id = c1.ord_line_id
                               AND move_order_line_id = c1.mo_line_id
                               AND request_id = g_request_id;
                     EXCEPTION
                        WHEN OTHERS
                        THEN
                           l_count_serials := 0;
                     END;

                     write_log (
                           '  1.1. WDD '
                        || c1.delivery_detail_id
                        || ' MO Line ID '
                        || c1.mo_line_id
                        || ' New Qty Qty '
                        || c1.new_ordered_qty
                        || ' - Lots found with >= qty '
                        || l_count_serials);
                  END IF;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     write_log (
                           '  1.1. Error pulling Lots for WDD '
                        || c1.delivery_detail_id
                        || ' MO Line ID '
                        || c1.mo_line_id);
                     write_log ('  1.1. Error:' || SQLERRM);
                     write_error (
                        (   '1.1. Error inserting Lots for MO '
                         || c1.mo_line_id
                         || '. Err: '
                         || SQLERRM)
                       ,'main_process step 1.2');
               END;
            END IF;

            IF     c1.serial_status != 'No serial number control'
               AND c1.lot_status != 'No lot control'
            THEN
               write_log (
                     '  1.1. WDD '
                  || c1.delivery_detail_id
                  || ' item is serial and lot controlled, need to mix trx');
            END IF;
         EXCEPTION
            WHEN OTHERS
            THEN
               -- Could not insert raise issue and send email
               write_log (
                     '1. Error inserting detail for DD '
                  || c1.delivery_detail_id);
               write_log ('1. Error:' || SQLERRM);
               write_error (
                  (   '1. Error inserting detail for DD '
                   || c1.delivery_detail_id
                   || '. Err: '
                   || SQLERRM)
                 ,'main_process step 1');
         END;
      END LOOP;

      write_log (
         '1. Closed cursor to pull the backordered lines for a specific sales order');
      write_log (' ');
      -- 2
      -- Determine if the number of available serial number transacted by MO is
      -- Correct, if not use other availabel SNs that are not currently assigned
      -- to another return process? HOW TO HANDLE if the ESN gets consumed by
      -- another order
      write_log (
         '2. Looping through lines and ESNs to determine if available ESNs match trx qty');

      FOR c2 IN cur_backord_ln_stg
      LOOP
         EXIT WHEN cur_backord_ln_stg%NOTFOUND;

         IF c2.serial_num_status != 'No serial number control'
         THEN
            l_count_serials := NULL;

            SELECT COUNT (*)
              INTO l_count_serials
              FROM xxwc_backordered_txns_sns_stg x1
             WHERE     x1.delivery_detail_id = c2.delivery_detail_id
                   AND x1.header_id = c2.header_id
                   AND x1.line_id = c2.line_id;

            IF l_count_serials < c2.wdd_requested_qty
            THEN
               write_log (
                     '2. Not enough ESNs to transfer back from original MO for WDD '
                  || c2.delivery_detail_id
                  || ' MO Line ID '
                  || c2.move_order_line_id);

               UPDATE xxwc_backordered_txns_lns_stg
                  SET status_msg =
                         status_msg || '.ESN Cnt does not match WDD Req Qty'
                WHERE ROWID = c2.row_id;
            END IF;
         END IF;
      END LOOP;

      write_log (' ');
      write_log (' ');
      -- 3
      -- Cursor to loop through the temp table xxwc_backordered_txns_lns_stg
      -- to pull the unique transactions that need to be processed
      write_log ('3. Opening cursor to process the backordered lines');

      FOR c3 IN cur_backord_ln_stg
      LOOP
         EXIT WHEN cur_backord_ln_stg%NOTFOUND;
         write_log ('============================================');
         write_log ('  3.1. Processing WDD ' || c3.delivery_detail_id);
         -- 3.1
         -- Call to process_subinv_transf for a specific delivery detail
         write_log (
            '  3.1. Calling procedure process_subinv_transfer to move the data for the WDD specified');
         process_subinv_transf (c3.delivery_detail_id, l_return_status);
         l_return_status := 'S';

         IF NVL (l_return_status, 'U') = 'S'
         THEN
            fnd_global.apps_initialize (fnd_global.user_id,           --18279,
                                                           21623, 660);
            mo_global.init ('ONT');
            -- 3.2
            -- Compare the ordered quantity and the cancel quantity to determine if
            -- call to update_so_line is needed based on delivery detail id
            -- may change to pass header id, line id and new ordered quantity
            write_log (' ');
            write_log (
               '  3.2. Comparing the quantities/new sched ship date to determine which scenario of processing covers it for order line update');

            /*IF (c3.ol_ordered_qty = c3.wdd_cancellation_qty)
                OR (c3.ol_ordered_qty > c3.wdd_cancellation_qty
                    and c3.wdd_cancellation_qty != 0)
                OR (c3.new_sched_ship_date is not null)*/
            IF    (c3.ol_ordered_qty >= c3.wdd_cancellation_qty)
               OR (c3.new_sched_ship_date IS NOT NULL)
            THEN
               update_so_line (c3.delivery_detail_id, l_return_status);
            ELSE
               write_log ('  3.2. No Order Line Update is needed');
            END IF;

            -- 3.3
            -- Comparing the orderd quantity and cancel quantity determine if a
            -- reservation needs to be created, may change to pass the header id,
            -- line id
            write_log (' ');
            write_log (
               '  3.3. Comparing the quantities to determine which scenario of processing covers it for reservation creation');

            IF (c3.ol_ordered_qty > c3.wdd_cancellation_qty) -- 02/27/2012
               AND c3.hold_reserve_flag = 'N'
            THEN
               create_reservation (c3.delivery_detail_id, l_return_status);
            ELSE
               write_log (
                  '  3.3. SO Line is cancelled, no reservations to create');
            END IF;
         END IF;
      END LOOP;

      -- Single Pick Release by Order
      -- 3.4
      -- If a call is made to the reservation procedure and the reservation was
      -- successful then proceed to call the pick release process
      write_log (' ');
      write_log (
         '  3.4. Comparing the quantities to determine which scenario of processing covers it for pick release');

      FOR c4 IN cur_reserved_ln_stg
      LOOP
         EXIT WHEN cur_reserved_ln_stg%NOTFOUND;
         l_return_status := NULL;
         write_log (
            '  3.4. Calling pick release for header id ' || c4.header_id);
         pick_release_order (c4.header_id
                            ,c4.ship_from_org_id
                            ,c4.holding_picking_rule
                            ,l_return_status);
      -- 3.5
      -- May need this step if the pick release does not append the picked line
      -- to an open delivery
      -- Write_Log (' ');
      -- Write_Log ('  3.5. Step may be needed if need to reallocate delivery detail to open delivery for order');
      END LOOP;

      BEGIN
         -- 05/04/2012 CGonzalez: Changed attribute1 to attribute4
         UPDATE wsh_delivery_details
            SET attribute4 = NULL, attribute2 = NULL, attribute3 = NULL
          WHERE delivery_detail_id IN
                   (SELECT delivery_detail_id
                      FROM xxwc_backordered_txns_lns_stg
                     WHERE     status_code IN ('SO_LINE_UPD', 'PROCESSED')
                           AND request_id = g_request_id);
      EXCEPTION
         WHEN OTHERS
         THEN
            write_log (' ');
            write_log ('Error resetting delivery details DFF to Null');
      END;

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         errbuf := 'Error Msg :' || SQLERRM;
         retcode := '2';
         write_error (errbuf, 'main_process others exception');
   END main_process;

   /*************************************************************************
     Procedure : batch_process

     PURPOSE:   receive and initiate the batch processing of sales orders
                with the ability to cancel or reschedule full orders
     Parameter:

   ************************************************************************/
   PROCEDURE batch_process (errbuf                     OUT VARCHAR2
                           ,retcode                    OUT VARCHAR2
                           ,p_process_type          IN     VARCHAR2
                           ,p_order_number          IN     VARCHAR2
                           ,p_new_sched_ship_date   IN     VARCHAR2)
   IS
      CURSOR cur_get_lines
      IS
           SELECT wdd.delivery_detail_id
                 ,wdd.source_header_id ord_header_id
                 ,wdd.source_line_id ord_line_id
                 ,wdd.inventory_item_id
                 ,wdd.organization_id
                 ,mtrl.line_id mo_line_id
                 ,mtrl.from_subinventory_code
                 ,mtrl.to_subinventory_code
                 ,mtrl.quantity_delivered
                 ,flv.meaning serial_status
                 ,flv2.meaning lot_status
                 ,wdd.released_status
                 ,wdd.split_from_delivery_detail_id
                 ,oola.ordered_quantity ol_ord_qty
                 ,wdd.requested_quantity wdd_req_qty
                 ,NVL (wdd.cancelled_quantity, 0) wdd_cancel_qty
                 ,NVL (wdd.shipped_quantity, 0) wdd_ship_qty
                 -- 05/04/2012 CGonzalez: Changed attribute1 to attribute4
                 ,NVL (wdd.attribute4, 0) wdd_dff_cancelation_qty
                 --05/04/2012 , (oola.ordered_quantity - nvl(to_number(wdd.attribute4),0)) new_ordered_qty
                 , (  wdd.requested_quantity
                    - NVL (TO_NUMBER (wdd.attribute4), 0))
                     new_ordered_qty
                 ,wdd_prt.move_order_line_id prt_mo_line_id
                 ,wdd.source_header_number ord_order_num
                 ,wdd.source_line_number
                 , /*(case
                     when p_process_type = 'CANCEL' then 'N'
                     else 'Y'
                     end)*/
                  'N' hold_reserve_flag
                 ,TO_DATE (p_new_sched_ship_date, 'YYYY/MM/DD HH24:MI:SS')
                     new_sched_ship_date
                 , (CASE
                       WHEN UPPER (ottl.NAME) LIKE '%RENTAL%' THEN 'RENTAL'
                       ELSE 'GENERAL'
                    END)
                     order_gen_type
                 , (CASE
                       WHEN p_process_type = 'CANCEL'
                       THEN
                          mtrl.from_subinventory_code
                       ELSE
                          (CASE
                              WHEN UPPER (ottl.NAME) LIKE '%RENTAL%'
                              THEN
                                 'RentalHLD'
                              ELSE
                                 'GeneralHLD'
                           END)
                    END)
                     holding_subinv
                 , (CASE
                       WHEN UPPER (ottl.NAME) LIKE '%RENTAL%'
                       THEN
                          'Holding Rental'
                       ELSE
                          'Holing Std'
                    END)
                     holding_pick_rule
                 ,wnd.delivery_id
             FROM wsh_delivery_details wdd
                 ,wsh_delivery_details wdd_prt
                 ,oe_order_lines oola
                 ,mtl_txn_request_lines mtrl
                 ,mtl_system_items_b msib
                 ,fnd_lookup_values flv
                 ,fnd_lookup_values flv2
                 ,oe_order_headers ooha
                 ,oe_transaction_types ottl
                 ,wsh_delivery_assignments wda
                 ,wsh_new_deliveries wnd
            WHERE     wdd.source_code = 'OE'
                  AND wdd.source_header_number = p_order_number
                  AND wdd.released_status = 'Y'          -- Only Picked Orders
                  AND wdd.split_from_delivery_detail_id =
                         wdd_prt.delivery_detail_id(+)
                  AND wdd.source_line_id = oola.line_id
                  AND NVL (wdd.split_from_delivery_detail_id
                          ,wdd.delivery_detail_id) =
                         mtrl.txn_source_line_detail_id(+)
                  --
                  AND mtrl.line_id IN
                         (SELECT MAX (mtrl2.line_id)
                            FROM mtl_txn_request_lines mtrl2
                           WHERE NVL (
                                    wdd.split_from_delivery_detail_id
                                   ,wdd.delivery_detail_id) =
                                    mtrl2.txn_source_line_detail_id)
                  --
                  AND wdd.inventory_item_id = msib.inventory_item_id
                  AND wdd.organization_id = msib.organization_id
                  AND msib.serial_number_control_code = flv.lookup_code
                  AND flv.lookup_type = 'MTL_SERIAL_NUMBER'
                  AND msib.lot_control_code = flv2.lookup_code
                  AND flv2.lookup_type = 'MTL_LOT_CONTROL'
                  -- 02/27/2012
                  AND oola.header_id = ooha.header_id
                  AND ooha.order_type_id = ottl.transaction_type_id
                  -- 03/23/2012
                  AND NVL (oola.booked_flag, 'N') = 'Y'
                  AND NVL (oola.cancelled_flag, 'N') = 'N'
                  AND wdd.delivery_detail_id = wda.delivery_detail_id(+)
                  AND wda.delivery_id = wnd.delivery_id(+)
         ORDER BY wdd.source_header_id, wdd.source_line_id;

      CURSOR cur_backord_ln_stg
      IS
         SELECT x1.ROWID row_id, x1.*
           FROM xxwc_backordered_txns_lns_stg x1
          WHERE     x1.status_code = 'BACKORDERED'
                AND x1.request_id = g_request_id;

      CURSOR cur_backord_sn_stg (
         p_delivery_detail_id    NUMBER
        ,p_header_id             NUMBER
        ,p_line_id               NUMBER)
      IS
         SELECT x1.ROWID row_id, x1.*
           FROM xxwc_backordered_txns_sns_stg x1
          WHERE     x1.delivery_detail_id = p_delivery_detail_id
                AND x1.header_id = p_header_id
                AND x1.line_id = p_line_id
                AND x1.status = 'BACKORDERED'
                AND x1.request_id = g_request_id;

      CURSOR cur_reserved_ln_stg
      IS
         SELECT DISTINCT (header_id), ship_from_org_id, holding_picking_rule
           FROM xxwc_backordered_txns_lns_stg x1
          WHERE x1.status_code = 'RESERVED' -- 02/24/2012 CG
                AND x1.hold_reserve_flag = 'N' -- Not set to hold so it can be picked
                AND x1.request_id = g_request_id;

      l_new_sched_ship_date   DATE;
      l_valid_data_element    VARCHAR2 (1);
      l_count_serials         NUMBER;
      l_return_status         VARCHAR2 (240);
   BEGIN
      errbuf := '';
      retcode := '0';
      l_new_sched_ship_date :=
         TO_DATE (p_new_sched_ship_date, 'YYYY/MM/DD HH24:MI:SS');
      write_log ('Begining WSH Batch Backordering Process ');
      write_log ('========================================================');
      write_log ('Parameters: ');
      write_log ('Process Type: ' || p_process_type);
      write_log ('Order Number:  ' || p_order_number);
      write_log ('New Schedule Ship Date: ' || l_new_sched_ship_date);
      write_log ('========================================================');
      write_log (' ');
      g_request_id := fnd_global.conc_request_id;
      fnd_global.apps_initialize (user_id        => fnd_global.user_id
                                 ,--   18279,
                                  resp_id        => 20634
                                 ,resp_appl_id   => 401);

      -- Verify that the order number is present and valid
      IF p_order_number IS NOT NULL
      THEN
         l_valid_data_element := NULL;

         BEGIN
            SELECT 'Y'
              INTO l_valid_data_element
              FROM oe_order_headers ooha
             WHERE     ooha.order_number = p_order_number
                   AND ooha.flow_status_code NOT IN ('CLOSED', 'CANCELLED');
         EXCEPTION
            WHEN OTHERS
            THEN
               l_valid_data_element := 'N';
         END;

         -- Invalid Order number (not there or in a closed/cancelled status)
         IF NVL (l_valid_data_element, 'N') = 'N'
         THEN
            write_log (
               'Invalid Order Number. Order does not exist or is in an invalid status');
            errbuf :=
               'Invalid Order Number. Order does not exist or is in an invalid status';
            retcode := '2';
         END IF;
      ELSE
         -- Null Order Number
         l_valid_data_element := 'N';
         write_log ('Invalid Order Number. Value cannot be null');
         errbuf := 'Invalid Order Number. Value cannot be null';
         retcode := '2';
      END IF;

      IF     l_new_sched_ship_date IS NOT NULL
         AND l_new_sched_ship_date < SYSDATE
      THEN
         write_log (
            'Invalid New Schedule Ship Date. New Schedule Ship Date needs to be a future date');
         errbuf :=
            'Invalid New Schedule Ship Date. New Schedule Ship Date needs to be a future date';
         retcode := '2';
         l_valid_data_element := 'N';
      END IF;

      -- Will continue only when there is a valid Order Number
      IF l_valid_data_element = 'Y'
      THEN
         -- Pull Line Level Data For Processing
         write_log (
            '1. Opening cursor to pull the backordered lines for a specific sales order');

         FOR c1 IN cur_get_lines
         LOOP
            EXIT WHEN cur_get_lines%NOTFOUND;

            BEGIN
               INSERT
                 INTO xxwc_backordered_txns_lns_stg (
                         delivery_detail_id
                        ,header_id
                        ,line_id
                        ,inventory_item_id
                        ,ship_from_org_id
                        ,move_order_line_id
                        ,from_subinventory_code
                        ,to_subinventory_code
                        ,mo_delivered_qty
                        ,serial_num_status
                        ,lot_status
                        ,wdd_release_status
                        ,split_from_dd_id
                        ,ol_ordered_qty
                        ,wdd_requested_qty
                        ,wdd_cancelled_qty
                        ,wdd_shipped_qty
                        ,wdd_cancellation_qty
                        ,new_ordered_quantity
                        ,new_sched_ship_date
                        ,hold_reserve_flag
                        ,order_type
                        ,holding_subinventory
                        ,holding_picking_rule
                        ,status_code
                        ,request_id
                        ,delivery_id)
               VALUES (c1.delivery_detail_id
                      ,c1.ord_header_id
                      ,c1.ord_line_id
                      ,c1.inventory_item_id
                      ,c1.organization_id
                      ,c1.mo_line_id
                      ,c1.from_subinventory_code
                      ,c1.to_subinventory_code
                      ,c1.quantity_delivered
                      ,c1.serial_status
                      ,c1.lot_status
                      ,c1.released_status
                      ,c1.split_from_delivery_detail_id
                      ,c1.ol_ord_qty
                      ,c1.wdd_req_qty
                      ,c1.wdd_cancel_qty
                      ,c1.wdd_ship_qty
                      ,c1.wdd_dff_cancelation_qty
                      ,c1.new_ordered_qty
                      ,c1.new_sched_ship_date
                      ,c1.hold_reserve_flag
                      ,c1.order_gen_type
                      ,c1.holding_subinv
                      ,c1.holding_pick_rule
                      ,'NEW'
                      ,g_request_id
                      ,c1.delivery_id);

               IF c1.serial_status != 'No serial number control'
               THEN
                  -- 1.2
                  -- Based on the data in temp table xxwc_backordered_txns_lns_stg
                  -- Retrieve the serial numbers associated to the move order that was backordered
                  write_log (
                     '  1.1. Pulling ESNs that are still in staging for the specified delivery detail and mode order');

                  BEGIN
                     INSERT INTO xxwc_backordered_txns_sns_stg (
                                    delivery_detail_id
                                   ,header_id
                                   ,line_id
                                   ,move_order_line_id
                                   ,from_subinventory
                                   ,transfer_subinventory
                                   ,inventory_item_id
                                   ,ship_from_org_id
                                   ,serial_number
                                   ,status
                                   ,request_id)
                          SELECT c1.delivery_detail_id
                                ,c1.ord_header_id
                                ,c1.ord_line_id
                                ,mmt.source_line_id
                                ,mmt.subinventory_code
                                ,mmt.transfer_subinventory
                                ,mmt.inventory_item_id
                                ,mmt.organization_id
                                ,mut.serial_number
                                ,'NEW'
                                ,g_request_id
                            FROM mtl_material_transactions mmt
                                ,mtl_unit_transactions mut
                                ,mtl_serial_numbers msn
                           WHERE     mmt.source_line_id = c1.mo_line_id
                                 -- Move order line id
                                 AND mmt.organization_id = c1.organization_id
                                 --02/02/12 and      msn.current_subinventory_code != c1.from_subinventory_code
                                 AND mmt.transaction_type_id = 52
                                 AND mmt.transaction_quantity < 0
                                 AND mmt.transaction_id = mut.transaction_id
                                 AND mmt.organization_id =
                                        msn.current_organization_id
                                 AND mmt.inventory_item_id =
                                        msn.inventory_item_id
                                 AND mut.serial_number = msn.serial_number
                                 AND msn.current_status = 3 -- Currently In Store
                        ORDER BY mmt.transaction_id, mut.serial_number;

                     l_count_serials := NULL;

                     BEGIN
                        SELECT COUNT (*)
                          INTO l_count_serials
                          FROM xxwc_backordered_txns_sns_stg
                         WHERE     delivery_detail_id = c1.delivery_detail_id
                               AND header_id = c1.ord_header_id
                               AND line_id = c1.ord_line_id
                               AND move_order_line_id = c1.mo_line_id
                               AND request_id = g_request_id;
                     EXCEPTION
                        WHEN OTHERS
                        THEN
                           l_count_serials := 0;
                     END;

                     write_log (
                           '  1.1. WDD '
                        || c1.delivery_detail_id
                        || ' MO Line ID '
                        || c1.mo_line_id
                        || ' - ESN Count '
                        || l_count_serials);
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        write_log (
                              '  1.1. Error pulling serials for WDD '
                           || c1.delivery_detail_id
                           || ' MO Line ID '
                           || c1.mo_line_id);
                        write_log ('  1.1. Error:' || SQLERRM);
                        write_error (
                           (   '1.1. Error inserting ESNs for MO '
                            || c1.mo_line_id
                            || '. Err: '
                            || SQLERRM)
                          ,'main_process step 1.2');
                  END;
               END IF;

               IF c1.lot_status != 'No lot control'
               THEN
                  -- 1.2
                  -- Based on the data in temp table xxwc_backordered_txns_lns_stg
                  -- Retrieve the serial numbers associated to the move order that was backordered
                  write_log (
                     '  1.1. Pulling LOTs that are still in staging for the specified delivery detail and mode order');

                  BEGIN
                     INSERT INTO xxwc_backordered_txns_sns_stg (
                                    delivery_detail_id
                                   ,header_id
                                   ,line_id
                                   ,move_order_line_id
                                   ,from_subinventory
                                   ,transfer_subinventory
                                   ,inventory_item_id
                                   ,ship_from_org_id
                                   ,lot_number
                                   ,lot_quantity
                                   ,status
                                   ,request_id)
                          SELECT c1.delivery_detail_id
                                ,c1.ord_header_id
                                ,c1.ord_line_id
                                ,mmt.source_line_id
                                ,mmt.subinventory_code
                                ,mmt.transfer_subinventory
                                ,mmt.inventory_item_id
                                ,mmt.organization_id
                                ,mtln.lot_number
                                ,mtln.transaction_quantity
                                ,'NEW'
                                ,g_request_id
                            FROM mtl_material_transactions mmt
                                ,mtl_transaction_lot_numbers mtln
                                ,mtl_lot_numbers mln
                           WHERE     mmt.source_line_id = c1.mo_line_id
                                 -- Move order line id
                                 AND mmt.organization_id = c1.organization_id
                                 AND mmt.transaction_type_id = 52
                                 AND mmt.transaction_quantity < 0
                                 AND mmt.transaction_id = mtln.transaction_id
                                 AND mtln.inventory_item_id =
                                        mln.inventory_item_id
                                 AND mtln.organization_id = mln.organization_id
                                 AND mtln.lot_number = mln.lot_number
                                 AND ABS (mtln.transaction_quantity) =
                                        c1.wdd_req_qty
                                 AND NOT EXISTS
                                            (SELECT 'Not Used by other WDD'
                                               FROM xxwc_backordered_txns_sns_stg x1
                                              WHERE     x1.header_id =
                                                           c1.ord_header_id
                                                    AND x1.line_id =
                                                           c1.ord_line_id
                                                    AND x1.move_order_line_id =
                                                           mmt.source_line_id
                                                    AND x1.from_subinventory =
                                                           mmt.subinventory_code
                                                    AND x1.transfer_subinventory =
                                                           mmt.transfer_subinventory
                                                    AND x1.inventory_item_id =
                                                           mmt.inventory_item_id
                                                    AND x1.ship_from_org_id =
                                                           mmt.organization_id
                                                    AND x1.lot_number =
                                                           mtln.lot_number
                                                    AND x1.request_id =
                                                           g_request_id)
                                 -- Not assigned to another delivery detail for the same line
                                 AND NOT EXISTS
                                            (SELECT 'Not Used on another delivery detail'
                                               FROM wsh_delivery_details wdd
                                              WHERE     wdd.source_header_id =
                                                           c1.ord_header_id
                                                    AND wdd.source_line_id =
                                                           c1.ord_line_id
                                                    AND wdd.inventory_item_id =
                                                           mmt.inventory_item_id
                                                    AND wdd.organization_id =
                                                           mmt.organization_id
                                                    AND wdd.lot_number =
                                                           mtln.lot_number
                                                    AND wdd.delivery_detail_id !=
                                                           c1.delivery_detail_id)
                                 AND ROWNUM = 1
                        ORDER BY mmt.transaction_id, mtln.lot_number;

                     l_count_serials := NULL;

                     BEGIN
                        SELECT lot_quantity
                          INTO l_count_serials
                          FROM xxwc_backordered_txns_sns_stg
                         WHERE     delivery_detail_id = c1.delivery_detail_id
                               AND header_id = c1.ord_header_id
                               AND line_id = c1.ord_line_id
                               AND move_order_line_id = c1.mo_line_id
                               AND request_id = g_request_id;
                     EXCEPTION
                        WHEN OTHERS
                        THEN
                           l_count_serials := 0;
                     END;

                     write_log (
                           '  1.1. WDD '
                        || c1.delivery_detail_id
                        || ' MO Line ID '
                        || c1.mo_line_id
                        || ' New Qty Qty '
                        || c1.new_ordered_qty
                        || ' - Lot Trx Qty '
                        || l_count_serials);
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        write_log (
                              '  1.1. Error pulling Lots for WDD '
                           || c1.delivery_detail_id
                           || ' MO Line ID '
                           || c1.mo_line_id);
                        write_log ('  1.1. Error:' || SQLERRM);
                        write_error (
                           (   '1.1. Error inserting Lots for MO '
                            || c1.mo_line_id
                            || '. Err: '
                            || SQLERRM)
                          ,'main_process step 1.2');
                  END;
               END IF;
            EXCEPTION
               WHEN OTHERS
               THEN
                  -- Could not insert raise issue and send email
                  write_log (
                        '1. Error inserting detail for DD '
                     || c1.delivery_detail_id);
                  write_log ('1. Error:' || SQLERRM);
                  write_error (
                     (   '1. Error inserting detail for DD '
                      || c1.delivery_detail_id
                      || '. Err: '
                      || SQLERRM)
                    ,'main_process step 1');
            END;
         END LOOP;

         write_log (
            '1. Closed cursor to pull the backordered lines for a specific sales order');
         write_log (' ');
         -- 2. Backorder all Deliveries Associated to the Sales Order
         write_log (
            '2. Calling procedure to backorder open deliveries for order');
         backorder_deliveries (l_return_status);

         IF l_return_status = 'S'
         THEN
            -- 3. Transfer Picked Inventory to Subinventory based on process type
            write_log (
               '3. Performing Subinventory Transfer and SO Line Updates (If applicable)');

            FOR c2 IN cur_backord_ln_stg
            LOOP
               EXIT WHEN cur_backord_ln_stg%NOTFOUND;
               write_log (' ');
               write_log (
                     '3.1. Processing delivery detail id: '
                  || c2.delivery_detail_id);
               process_subinv_transf (c2.delivery_detail_id, l_return_status);
               l_return_status := 'S';

               IF     l_return_status = 'S'
                  AND NVL (p_process_type, 'UNKNOWN') = 'RESCHEDULE'
               THEN
                  fnd_global.apps_initialize (fnd_global.user_id,     --18279,
                                                                 21623, 660);
                  mo_global.init ('ONT');
                  -- 3.2 Updating SO Line
                  write_log (' ');
                  write_log (
                     '  3.2. Updating SO Line For Rescheduling Process');

                  IF c2.new_sched_ship_date IS NOT NULL
                  THEN
                     update_so_line (c2.delivery_detail_id, l_return_status);
                  ELSE
                     write_log ('  3.2. No Order Line Update is needed');
                  END IF;

                  -- 3.3 Creating Reservation
                  write_log (' ');
                  write_log (
                     '  3.3. Creating SO Line Reservaction For Rescheduling Process');

                  --IF (c2.ol_ordered_qty > c2.wdd_cancellation_qty)
                  IF c2.new_sched_ship_date IS NOT NULL
                  THEN
                     create_reservation (c2.delivery_detail_id
                                        ,l_return_status);
                  ELSE
                     write_log (
                        '  3.3. SO Line is cancelled, no reservations to create');
                  END IF;
               END IF;
            END LOOP;

            IF NVL (p_process_type, 'UNKNOWN') = 'CANCEL'
            THEN
               -- 4. Cancel Order and remaining lines
               write_log ('4. Submitting procedure to Cancel Order');
               fnd_global.apps_initialize (fnd_global.user_id,        --18279,
                                                              21623, 660);
               mo_global.init ('ONT');
               cancel_order (l_return_status);
            ELSIF NVL (p_process_type, 'UNKNOWN') = 'RESCHEDULE'
            THEN
               -- 4. Pick Order
               write_log ('4. Submitting procedure to Pick Order');

               FOR c3 IN cur_reserved_ln_stg
               LOOP
                  EXIT WHEN cur_reserved_ln_stg%NOTFOUND;
                  l_return_status := NULL;
                  write_log (
                        '  4.1. Calling pick release for header id '
                     || c3.header_id);
                  pick_release_order (c3.header_id
                                     ,c3.ship_from_org_id
                                     ,c3.holding_picking_rule
                                     ,l_return_status);
               END LOOP;
            ELSE
               -- In case the required check is removed or unspecified
               -- options are added to the lookup
               write_log ('Invalid Processing Type');
               errbuf := 'Error Invalid Processing Type';
               retcode := '2';
            END IF;
         END IF;         -- End if for Return Status from Backordering Process
      END IF;

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         errbuf := 'Error Msg :' || SQLERRM;
         write_error (errbuf, 'batch_process others exception');
         write_log ('Error in batch_process. ' || errbuf);
         retcode := '2';
   END batch_process;
END xxwc_wsh_backord_txns_pkg;
/

