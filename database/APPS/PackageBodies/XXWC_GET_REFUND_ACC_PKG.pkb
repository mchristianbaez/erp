CREATE OR REPLACE PACKAGE BODY APPS.XXWC_GET_REFUND_ACC_PKG
AS
   FUNCTION GET_REFUND_BRANCH_CODE (P_REC_APP_ID IN NUMBER)
      RETURN VARCHAR2
   /******************************************************************************
       NAME:       GET_REFUND_BRANCH_CODE

       PURPOSE:    To retrieve the refund branch code

       REVISIONS:
       Ver        Date        Author           Description
       ---------  ----------  ---------------  ------------------------------------
       1.0        12-JUL-13   Shankar Hariharan   1. Created the function
       1.1        25-SEP-14   Maharajan Shunmugam TMS#20141001-00153  Canada multi org changes
    ******************************************************************************/

   IS
      l_segment2      VARCHAR2 (25) DEFAULT 'BW080';
      l_header_id     NUMBER;
      l_ccid          NUMBER;
      l_amt_applied   NUMBER;
      l_exception     EXCEPTION;
   BEGIN
      BEGIN
         SELECT attribute1, amount_applied
           INTO l_header_id, l_amt_applied
           FROM apps.ar_receivable_applications
          WHERE     receivables_trx_id IS NOT NULL
                AND application_type = 'CASH'
                AND receivable_application_id = P_REC_APP_ID
                AND application_rule = 'ACTIVITY APPLICATION';
      EXCEPTION
         WHEN OTHERS
         THEN
            RAISE l_exception;
      END;

      IF l_header_id IS NULL
      THEN
         RETURN NULL;
      ELSE
         BEGIN
            SELECT b.segment2
              INTO l_segment2
              FROM apps.mtl_parameters a
                 , apps.gl_code_combinations_kfv b
                 , apps.oe_order_headers c
                 , apps.XXWC_OM_CASH_REFUND_TBL d
             WHERE d.header_id = l_header_id
                   AND (d.refund_amount = l_amt_applied
                        OR d.check_refund_amount = l_amt_applied)
                   AND d.return_header_id = c.header_id
                   AND c.ship_from_org_id = a.organization_id
                   AND a.material_account = b.code_combination_id
                   AND ROWNUM = 1;
         EXCEPTION
            WHEN OTHERS
            THEN
               RAISE l_exception;
         END;

         RETURN l_segment2;
      END IF;
   EXCEPTION
      WHEN l_exception
      THEN
         RETURN NULL;
      WHEN OTHERS
      THEN
         RETURN NULL;
   END GET_REFUND_BRANCH_CODE;

   FUNCTION GET_CC_REFUND_BRANCH_CODE (P_RECEIPT_ID IN NUMBER)
      RETURN VARCHAR2
   IS
   /******************************************************************************
       NAME:       GET_REFUND_BRANCH_CODE

       PURPOSE:    To retrieve the refund branch code

       REVISIONS:
       Ver        Date        Author           Description
       ---------  ----------  ---------------  ------------------------------------
       1.0        12-JUL-13   Shankar Hariharan   1. Created the function
       1.1        25-SEP-14   Maharajan Shunmugam TMS#20141001-00153  Canada multi org changes
    ******************************************************************************/
      l_segment2   VARCHAR2 (25) DEFAULT 'BW080';
   BEGIN
      SELECT f.segment2
        INTO l_segment2
        FROM apps.ar_cash_receipts a
           , apps.ar_receivable_applications b
           , apps.XXWC_OM_CASH_REFUND_TBL c
           , apps.oe_order_headers d
           , apps.mtl_parameters e
           , apps.gl_code_combinations_kfv f
       WHERE     a.reference_id = b.cash_receipt_id
             AND a.reference_type = 'RECEIPT'
             AND a.receivables_trx_id = b.receivables_trx_id
             AND a.amount * -1 = b.amount_applied
             AND b.display = 'Y'
             AND b.status = 'ACTIVITY'
             AND b.applied_payment_schedule_id = -6
             AND TO_NUMBER (b.attribute1) = c.header_id
             AND b.cash_receipt_id = c.cash_receipt_id
             AND c.return_header_id = d.header_id
             AND d.ship_from_org_id = e.organization_id
             AND e.material_account = f.code_combination_id
             AND c.refund_amount=b.amount_applied
             AND a.cash_receipt_id = p_receipt_id;

      RETURN l_segment2;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END GET_CC_REFUND_BRANCH_CODE;
END XXWC_GET_REFUND_ACC_PKG;
/