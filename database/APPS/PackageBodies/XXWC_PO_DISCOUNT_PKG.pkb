create or replace PACKAGE BODY APPS.XXWC_PO_DISCOUNT_PKG
/*************************************************************************
  $Header XXWC_PO_DISCOUNT_PKG.pkb $
  Module Name: XXWC_PO_DISCOUNT_PKG

  PURPOSE: To Apply / Remove discount in PO lines

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    ----------------
  1.0        21-Jul-2015  Manjula Chellappan    Initial Version TMS # 20131120-00027

**************************************************************************/
IS
   g_dflt_email   fnd_user.email_address%TYPE
                     := 'HDSOracleDevelopers@hdsupply.com';


   PROCEDURE apply_line_discount (p_po_header_id   IN     NUMBER,
                                  x_status_msg        OUT VARCHAR2)
   /*************************************************************************
     $Header XXWC_PO_DISCOUNT_PKG.apply_line_discount $
     Module Name: XXWC_PO_DISCOUNT_PKG

     PURPOSE: To Apply discount in PO lines

     REVISIONS:
     Ver        Date         Author                Description
     ---------  -----------  ------------------    ----------------
     1.0        21-Jul-2015  Manjula Chellappan    Initial Version TMS # 20131120-00027

   **************************************************************************/
   IS
      l_sec                     VARCHAR2 (100);
      l_status_msg              VARCHAR2 (4000);
      l_apply_discount_flag     NUMBER := 0;
      l_line_discount_percent   NUMBER;
      l_revision_num		NUMBER; 
      l_result                  NUMBER;
      l_api_errors              po_api_errors_rec_type;

      l_validation_error        EXCEPTION;

      CURSOR cur_po_lines
      IS
         SELECT pha.segment1 po_number,
                pha.revision_num,
                pha.po_header_id,
                pha.authorization_status,
                pha.attribute9 discount_percent,
                pla.po_line_id,
                pla.line_num,
                pha.org_id,
                pla.quantity,
                pla.unit_price,
                pola.line_location_id,
                pola.shipment_num,
                pola.promised_date,
                pola.need_by_date
           FROM po_headers pha,
                po_lines pla,
                po_line_locations_all pola,
                mtl_system_items_b msib
          WHERE     pha.po_header_id = pla.po_header_id
                AND pla.item_id = msib.inventory_item_id
                AND NVL (pola.ship_to_organization_id, 222) =
                       msib.organization_id
                AND msib.item_type <> 'INTANGIBLE'
                AND pha.type_lookup_code = 'STANDARD'
                AND NVL (pha.closed_code, 'X') NOT IN ('CLOSED',
                                                       'FINALLY CLOSED')
                AND NVL (pha.authorization_status, 'X') NOT IN ('REJECTED')
                AND pla.po_line_id = pola.po_line_id(+)
                AND NVL(pla.attribute1,'A') <> 'DA'
                AND NVL (pla.attribute2, 'N') = 'N'
                AND NVL (pola.cancel_flag, 'N') <> 'Y'
                AND NVL (pola.closed_code, 'OPEN') = 'OPEN'
                AND NVL (pola.quantity_received, 0) = 0
                AND NVL (pola.quantity_billed, 0) = 0
                AND pha.po_header_id = p_po_header_id;
   BEGIN
   
      l_sec := 'Apps Initialize';

      mo_global.init ('PO');

      mo_global.set_policy_context ('S', fnd_global.org_id);

      fnd_global.APPS_INITIALIZE (fnd_global.user_id,
                                  fnd_global.resp_id,
                                  fnd_global.resp_appl_id);

      l_sec := 'Validate if PO is eligible for Applying the Discount';

      BEGIN
         SELECT 0
           INTO l_apply_discount_flag
           FROM po_headers
          WHERE     type_lookup_code = 'STANDARD'
                AND NVL (closed_code, 'X') NOT IN ('CLOSED', 'FINALLY CLOSED')
                AND NVL (authorization_status, 'X') NOT IN ('REJECTED')
                AND NVL (attribute9, 0) > 0
                AND po_header_id = p_po_header_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_apply_discount_flag := 1;
            l_status_msg :=
               'PO is not eligible to Apply discount. Please check the PO Type/Status/Line Discount Percent';
            RAISE l_validation_error;
      END;

      l_sec := 'Validate if any lines are eligible for applying discount';

      BEGIN
         SELECT 0
           INTO l_apply_discount_flag
           FROM DUAL
          WHERE EXISTS
                   (SELECT 'x'
                      FROM po_lines pl,
                           po_line_locations pll,
                           mtl_system_items_b msib
                     WHERE     NVL(pl.attribute1,'A') <> 'DA'           -- Apply Discount
                           AND pl.item_id = msib.inventory_item_id
                           AND NVL (pll.ship_to_organization_id, 222) =
                                  msib.organization_id
                           AND msib.item_type <> 'INTANGIBLE'
                           AND NVL (pl.attribute2, 'N') = 'N'
                           AND pl.po_line_id = pll.po_line_id(+)
                           AND NVL (pll.cancel_flag, 'N') <> 'Y'
                           AND NVL (pll.closed_code, 'OPEN') = 'OPEN'
                           AND NVL (pll.quantity_received, 0) = 0
                           AND NVL (pll.quantity_billed, 0) = 0
                           AND pl.po_header_id = p_po_header_id);
      EXCEPTION
         WHEN OTHERS
         THEN
            l_apply_discount_flag := 1;
            l_status_msg :=
               'No PO Lines are eligible for applying discount. Please check the PO Line Quantity/Status';
            RAISE l_validation_error;
      END;

      l_sec := 'Apply Discount ';

      FOR rec_po_lines IN cur_po_lines
      LOOP
      
      
      IF cur_po_lines%ROWCOUNT = 1 THEN
      
       l_revision_num := rec_po_lines.revision_num; 
       
      ELSIF cur_po_lines%ROWCOUNT = 2 AND NVL(rec_po_lines.authorization_status,'X') ='APPROVED' THEN
      
       l_revision_num := rec_po_lines.revision_num+1 ; 
      
      END IF;
           
      
         l_result :=
            po_change_api1_s.update_po (
               x_po_number             => rec_po_lines.po_number, --Enter the PO Number
               x_release_number        => NULL,        --Enter the Release Num
               x_revision_number       => l_revision_num, --Enter the Revision Number
               x_line_number           => rec_po_lines.line_num, --Enter the Line Number
               x_shipment_number       => rec_po_lines.shipment_num, --Enter the Shipment Number
               new_quantity            => rec_po_lines.quantity, --Enter the new quantity
               new_price               => ROUND (
                                              rec_po_lines.unit_price
                                            * (  1
                                               -   rec_po_lines.discount_percent
                                                 / 100),
                                            5),         --Enter the new price,
               new_promised_date       => rec_po_lines.promised_date, --Enter the new promised date,
               new_need_by_date        => rec_po_lines.need_by_date, --Enter the new need by date,
               launch_approvals_flag   => 'N',
               update_source           => NULL,
               version                 => '1.0',
               x_override_date         => NULL,
               x_api_errors            => l_api_errors,
               p_buyer_name            => NULL,
               p_secondary_quantity    => NULL,
               p_preferred_grade       => NULL,
               p_org_id                => rec_po_lines.org_id);


         IF (l_result <> 1)
         THEN
            -- Log the errors
            FOR j IN 1 .. l_api_errors.MESSAGE_TEXT.COUNT
            LOOP
               l_status_msg :=
                  l_status_msg || ' - ' || (l_api_errors.MESSAGE_TEXT (j));
            END LOOP;
         ELSE
            l_sec := 'Update PO lines Attributes ';

            BEGIN
               UPDATE po_lines
                  SET attribute1 = 'R',
                      attribute2 = 'Y',
                      attribute3 = rec_po_lines.unit_price
                WHERE po_line_id = rec_po_lines.po_line_id;
            EXCEPTION
               WHEN OTHERS
               THEN
                   l_status_msg :=
                     'Failed to Update PO line Attributes for PO Line : '||rec_po_lines.line_num||'-'|| SQLERRM;

                  RAISE l_validation_error;
            END;
         END IF;
         
         COMMIT;
         
      END LOOP;



      l_status_msg := 'Discount Applied ';
      x_status_msg := SUBSTR (l_status_msg, 1, 200);
      
   EXCEPTION
      WHEN l_validation_error
      THEN
         x_status_msg := l_status_msg;
         
      WHEN OTHERS
      THEN
         l_status_msg :=
            'Failed to Apply discount for PO : ' || p_po_header_id;

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_PO_DISCOUNT_PKG.apply_line_discount',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => l_status_msg,
            p_distribution_list   => g_dflt_email,
            p_module              => 'PO');

         x_status_msg := l_status_msg;
         
   END apply_line_discount;


   PROCEDURE remove_line_discount (p_po_header_id   IN     NUMBER,
                                   x_status_msg        OUT VARCHAR2)
   /*************************************************************************
     $Header XXWC_PO_DISCOUNT_PKG.remove_line_discount $
     Module Name: XXWC_PO_DISCOUNT_PKG

     PURPOSE: To Remove discount in PO lines

     REVISIONS:
     Ver        Date         Author                Description
     ---------  -----------  ------------------    ----------------
     1.0        21-Jul-2015  Manjula Chellappan    Initial Version TMS # 20131120-00027

   **************************************************************************/
   IS
      l_sec                    VARCHAR2 (100);
      l_status_msg             VARCHAR2 (4000);
      l_remove_discount_flag   NUMBER := 0;
      l_revision_num 	       NUMBER;
      l_result                 NUMBER;
      l_api_errors             po_api_errors_rec_type;

      l_validation_error       EXCEPTION;

      CURSOR cur_po_lines
      IS
         SELECT pha.segment1 po_number,
                pha.revision_num,
                pha.po_header_id,                
                pha.authorization_status,
                pha.attribute9 discount_percent,
                pla.po_line_id,
                pla.line_num,
                pha.org_id,
                pla.quantity,
                pla.unit_price,
                pla.attribute3 original_line_cost,
                pola.line_location_id,
                pola.shipment_num,
                pola.promised_date,
                pola.need_by_date
           FROM po_headers pha, po_lines pla, po_line_locations_all pola
          WHERE     pha.po_header_id = pla.po_header_id
                AND pha.type_lookup_code = 'STANDARD'
                AND NVL (pha.closed_code, 'X') NOT IN ('CLOSED',
                                                       'FINALLY CLOSED')
                AND NVL (pha.authorization_status, 'X') NOT IN ('REJECTED')
                AND pla.po_line_id = pola.po_line_id(+)
                AND NVL(pla.attribute1,'A') <> 'DR'
                AND NVL (pla.attribute2, 'N') = 'Y'
                AND NVL (pola.cancel_flag, 'N') <> 'Y'
                AND NVL (pola.closed_code, 'OPEN') = 'OPEN'
                AND NVL (pola.quantity_received, 0) = 0
                AND NVL (pola.quantity_billed, 0) = 0
                AND pha.po_header_id = p_po_header_id;
   BEGIN
      l_sec := 'Initialize Apps';

      mo_global.init ('PO');

      mo_global.set_policy_context ('S', fnd_global.org_id);

      fnd_global.apps_initialize (fnd_global.user_id,
                                  fnd_global.resp_id,
                                  fnd_global.resp_appl_id);

      l_sec := 'Validate if PO is eligible for Removing the Discount';

      BEGIN
         SELECT 0
           INTO l_remove_discount_flag
           FROM po_headers
          WHERE     type_lookup_code = 'STANDARD'
                AND NVL (closed_code, 'X') NOT IN ('CLOSED', 'FINALLY CLOSED')
                AND NVL (authorization_status, 'X') NOT IN ('REJECTED')
                AND po_header_id = p_po_header_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_remove_discount_flag := 1;
            l_status_msg :=
               'PO is not eligible to Remove discount. Please check the PO Type/Status';
            RAISE l_validation_error;
      END;

      l_sec := 'Validate if any lines are eligible for Removing discount';

      BEGIN
         SELECT 0
           INTO l_remove_discount_flag
           FROM DUAL
          WHERE EXISTS
                   (SELECT 'x'
                      FROM po_lines pl, po_line_locations pll
                     WHERE     NVL(pl.attribute1,'A') <> 'DR'          -- Remove Discount
                           AND NVL (pl.attribute2, 'N') = 'Y'
                           AND pl.po_line_id = pll.po_line_id(+)
                           AND NVL (pll.cancel_flag, 'N') <> 'Y'
                           AND NVL (pll.closed_code, 'OPEN') = 'OPEN'
                           AND NVL (pll.quantity_received, 0) = 0
                           AND NVL (pll.quantity_billed, 0) = 0
                           AND pl.po_header_id = p_po_header_id);
      EXCEPTION
         WHEN OTHERS
         THEN
            l_remove_discount_flag := 1;
            l_status_msg :=
               'No PO Lines are eligible for Removing discount. Please check the PO Line Quantity/Status';
            RAISE l_validation_error;
      END;

      l_sec := 'Apply Discount ';

      FOR rec_po_lines IN cur_po_lines
      LOOP
      
            IF cur_po_lines%ROWCOUNT = 1 THEN
            
             l_revision_num := rec_po_lines.revision_num; 
             
            ELSIF cur_po_lines%ROWCOUNT = 2 AND NVL(rec_po_lines.authorization_status,'X') ='APPROVED' THEN
            
             l_revision_num := rec_po_lines.revision_num+1 ; 
            
      	    END IF;
      
      
         l_result :=
            po_change_api1_s.update_po (
               x_po_number             => rec_po_lines.po_number, --Enter the PO Number
               x_release_number        => NULL,        --Enter the Release Num
               x_revision_number       => l_revision_num, --Enter the Revision Number
               x_line_number           => rec_po_lines.line_num, --Enter the Line Number
               x_shipment_number       => rec_po_lines.shipment_num, --Enter the Shipment Number
               new_quantity            => rec_po_lines.quantity, --Enter the new quantity
               new_price               => rec_po_lines.original_line_cost, --Enter the new price,
               new_promised_date       => rec_po_lines.promised_date, --Enter the new promised date,
               new_need_by_date        => rec_po_lines.need_by_date, --Enter the new need by date,
               launch_approvals_flag   => 'N',
               update_source           => NULL,
               version                 => '1.0',
               x_override_date         => NULL,
               x_api_errors            => l_api_errors,
               p_buyer_name            => NULL,
               p_secondary_quantity    => NULL,
               p_preferred_grade       => NULL,
               p_org_id                => rec_po_lines.org_id);


         IF (l_result <> 1)
         THEN
            -- Log the errors
            FOR j IN 1 .. l_api_errors.MESSAGE_TEXT.COUNT
            LOOP
               l_status_msg :=
                  l_status_msg || ' - ' || (l_api_errors.MESSAGE_TEXT (j));
            END LOOP;
         ELSE
            l_sec := 'Update PO lines Attributes ';

            BEGIN
               UPDATE po_lines
                  SET attribute1 = 'A', attribute2 = 'N', attribute3 = NULL
                WHERE po_line_id = rec_po_lines.po_line_id;
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_status_msg :=
                     'Failed to Update PO line Attributes for PO Line : '||rec_po_lines.line_num||'-'|| SQLERRM;

                  RAISE l_validation_error;
            END;
         END IF;
         
         COMMIT;
      END LOOP;
      
      l_status_msg := 'Discount Removed ';
      x_status_msg := SUBSTR (l_status_msg, 1, 200);
      
   EXCEPTION
      WHEN l_validation_error
      THEN
         x_status_msg := l_status_msg;
      WHEN OTHERS
      THEN
         l_status_msg :=
            'Failed to Remove discount for PO : ' || p_po_header_id;

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_PO_DISCOUNT_PKG.remove_line_discount',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => l_status_msg,
            p_distribution_list   => g_dflt_email,
            p_module              => 'PO');
            
   END remove_line_discount;



   FUNCTION check_po_discounted (p_po_header_id IN NUMBER)
      RETURN VARCHAR2
   /*************************************************************************
     $Header XXWC_PO_DISCOUNT_PKG.check_po_discounted $
     Module Name: XXWC_PO_DISCOUNT_PKG

     PURPOSE: To Check if the PO Header or Lines discounted

     REVISIONS:
     Ver        Date         Author                Description
     ---------  -----------  ------------------    ----------------
     1.0        21-Jul-2015  Manjula Chellappan    Initial Version TMS # 20131120-00027

   **************************************************************************/
   IS
      l_discounted_flag   VARCHAR2 (1);
   BEGIN
      SELECT 'Y'
        INTO l_discounted_flag
        FROM DUAL
       WHERE EXISTS
                (SELECT 'X'
                   FROM po_headers_all ph, po_lines_all pl
                  WHERE     ph.po_header_id = p_po_header_id
                        AND ph.po_header_id = pl.po_header_id
                        AND (   NVL (ph.attribute8, 0) > 0
                             OR NVL (pl.attribute2, 'N') = 'Y'));

      RETURN (l_discounted_flag);
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN ('N');
   END check_po_discounted;


   FUNCTION check_po_header_discounted (p_po_header_id IN NUMBER)
      RETURN VARCHAR2
   /*************************************************************************
     $Header XXWC_PO_DISCOUNT_PKG.check_po_header_discounted $
     Module Name: XXWC_PO_DISCOUNT_PKG

     PURPOSE: To Check if the PO Header is discounted

     REVISIONS:
     Ver        Date         Author                Description
     ---------  -----------  ------------------    ----------------
     1.0        21-Jul-2015  Manjula Chellappan    Initial Version TMS # 20131120-00027

   **************************************************************************/
   IS
      l_discounted_flag   VARCHAR2 (1);
   BEGIN
      SELECT 'Y'
        INTO l_discounted_flag
        FROM DUAL
       WHERE EXISTS
                (SELECT 'X'
                   FROM po_headers_all ph
                  WHERE     ph.po_header_id = p_po_header_id
                        AND NVL (ph.attribute8, 0) > 0);

      RETURN (l_discounted_flag);
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN ('N');
   END check_po_header_discounted;



   FUNCTION check_po_lines_discounted (p_po_header_id IN NUMBER)
      RETURN VARCHAR2
   /*************************************************************************
     $Header XXWC_PO_DISCOUNT_PKG.check_po_lines_discounted $
     Module Name: XXWC_PO_DISCOUNT_PKG

     PURPOSE: To Check if the PO lines are discounted

     REVISIONS:
     Ver        Date         Author                Description
     ---------  -----------  ------------------    ----------------
     1.0        21-Jul-2015  Manjula Chellappan    Initial Version TMS # 20131120-00027

   **************************************************************************/
   IS
      l_discounted_flag   VARCHAR2 (1);
   BEGIN
      SELECT 'Y'
        INTO l_discounted_flag
        FROM DUAL
       WHERE EXISTS
                (SELECT 'X'
                   FROM po_lines_all pl
                  WHERE     pl.po_header_id = p_po_header_id
                        AND NVL (pl.attribute2, 'N') = 'Y');

      RETURN (l_discounted_flag);
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN ('N');
   END check_po_lines_discounted;


   FUNCTION get_po_header_discount (p_po_header_id IN NUMBER)
      RETURN VARCHAR2
   /*************************************************************************
     $Header XXWC_PO_DISCOUNT_PKG.get_po_header_discount $
     Module Name: XXWC_PO_DISCOUNT_PKG

     PURPOSE: To Check if the PO is discounted

     REVISIONS:
     Ver        Date         Author                Description
     ---------  -----------  ------------------    ----------------
     1.0        21-Jul-2015  Manjula Chellappan    Initial Version TMS # 20131120-00027

   **************************************************************************/
   IS
      l_discount   VARCHAR2 (20);
   BEGIN
      SELECT    DECODE (attribute7, 'NET', '$')
             || attribute8
             || DECODE (attribute7, 'PERCENT', '%')
        INTO l_discount
        FROM po_headers_all
       WHERE po_header_id = p_po_header_id AND NVL (attribute8, 0) > 0;

      RETURN (l_discount);
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN (NULL);
   END get_po_header_discount;
   
   FUNCTION get_discount_note_to_ap (p_po_header_id IN NUMBER)
      RETURN VARCHAR2
   /*************************************************************************
     $Header XXWC_PO_DISCOUNT_PKG.get_discount_note_to_ap $
     Module Name: XXWC_PO_DISCOUNT_PKG

     PURPOSE: To Check if the PO is discounted

     REVISIONS:
     Ver        Date         Author                Description
     ---------  -----------  ------------------    ----------------
     1.0        21-Jul-2015  Manjula Chellappan    Initial Version TMS # 20131120-00027

   **************************************************************************/
   IS
      l_discount_note   VARCHAR2 (150);
   BEGIN
      SELECT attribute10
        INTO l_discount_note
        FROM po_headers_all
       WHERE po_header_id = p_po_header_id ;

      RETURN (l_discount_note);
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN (NULL);
   END get_discount_note_to_ap;

   
END XXWC_PO_DISCOUNT_PKG;
/
