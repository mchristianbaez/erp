CREATE OR REPLACE PACKAGE BODY APPS.XXWC_OZF_VENDOR_QUOTE_PKG AS

 /*************************************************************************
     $Header APPS.XXWC_OZF_VENDOR_QUOTE_PKG
     Module Name: APPS.XXWC_OZF_VENDOR_QUOTE_PKG

     PURPOSE: This Package is created for EDW Invoice Line Extract
	 
     REVISIONS:

     Ver        Date        Author        				Description
     ---------  ----------  ----------    				----------------
	 1.0        No Revision History available.
     2.0        11/11/2014  Raghavendra Syamanaboina	TMS# 20141001-00254 - Canada Multi org Changes
  --*********************************************************************************************************/
    -- Procedure  MAIN
    -- THis API is called to convert Quotes into SSD Product Lines. 
    --*********************************************************************************************************/
    
        
   Procedure  MAIN (Errbuf                       OUT NOCOPY VARCHAR2,
                   Retcode                      OUT NOCOPY NUMBER,
                   p_OM_Header_ID               IN Number,
                   p_SDR_Req_Header_ID          IN Number )   IS
                   
       -- Declare Local Variables 
       l_Return_Status          Varchar2(1) ; 
       l_Error_Message          Varchar2(2000);
       l_API_NAME               VARCHAR2(80) := '.MAIN';
       l_Module_Name            Varchar2(80) ; 
       l_Msg_Count              Number;
       l_Msg_Data               Varchar2(2000);
       i                        Number ;
       l_Lines_Count            Number := 0; 
       l_Order_Number           Varchar2(30); 
       l_Request_Number          Varchar2(30) ; 
       l_Product_Cost           Number; 
       
       l_SDR_Hdr_rec             OZF_SD_REQUEST_PUB.SDR_hdr_rec_Type   ;
       l_SDR_lines_rec           OZF_SD_REQUEST_PUB.SDR_Lines_rec_Type   ;
       l_SDR_Cust_rec            OZF_SD_REQUEST_PUB.SDR_Cust_rec_Type   ;
       l_sdr_lines_tbl           OZF_SD_REQUEST_PUB.SDR_lines_tbl_type ;
       l_SDR_cust_tbl            OZF_SD_REQUEST_PUB.SDR_cust_tbl_type ; 
       
       
       
       
       C_CUST_USAGE_CODE         CONSTANT VARCHAR2(30) := 'CUSTOMER'; 
       C_END_CUSTOMER_FLAG_NO    CONSTANT VARCHAR2(1)  := 'N'; 
       C_PRODUCT_CONTEXT         CONSTANT VARCHAR2(30) := 'PRODUCT'; 
       C_DISCOUNT_TYPE_NEW_PRICE CONSTANT VARCHAR2(30) := 'NEWPRICE' ;
       C_DISCOUNT_TYPE_AMT       CONSTANT VARCHAR2(30) := 'AMT' ;
       
       l_err_callpoint                      VARCHAR2(175) := 'START';
       l_distro_list                        VARCHAR2(80) := 'OracleDevelopmentGroup@hdsupply.com';
       
      --- Define a Cursor to get SDR Header Request Information 
      Cursor   SDR_Hdr_Cur ( p_SDR_REQ_HEADER_ID Number ) IS
        Select   
           Request_Header_ID     ,
           object_version_number ,
           last_update_date       ,
           last_updated_by        ,
           last_update_login      ,
           request_id             ,
           program_application_id ,
           program_update_date    ,
           program_id             ,
           created_from           ,
           request_number         ,
           request_class          ,
           offer_type             ,
           offer_id               ,
           root_request_header_id ,
           linked_request_header_id  ,
           request_start_date   ,
           request_end_date     ,
           user_status_id         ,
           request_outcome        ,
           decline_reason_code     ,
           return_reason_code      ,
           request_currency_code   ,
           authorization_number    ,
           requested_budget_amount  ,
           approved_budget_amount   ,
           attribute_category       ,
           attribute1               ,
           attribute2               ,
           attribute3               ,
           attribute4               ,
           attribute5               ,
           attribute6               ,
           attribute7               ,
           attribute8               ,
           attribute9               ,
           attribute10              ,
           attribute11              ,
           attribute12              ,
           attribute13              ,
           attribute14              ,
           attribute15              ,
           supplier_id              ,
           supplier_site_id         ,
           supplier_contact_id        ,
           internal_submission_date     ,
           asignee_response_by_date     ,
           asignee_response_date        ,
           submtd_by_for_supp_approval ,
           supplier_response_by_date   ,
           supplier_response_date      ,
           supplier_submission_date    ,
           requestor_id                ,
           supplier_quote_number       ,
           internal_order_number       ,
           sales_order_currency        ,
           request_source              ,
           asignee_resource_id         ,
           org_id                      ,
           security_group_id            ,
           accrual_type                   ,
           cust_account_id                ,
           supplier_contact_email_address ,
           supplier_contact_phone_number  ,
           request_type_setup_id          ,
           request_basis                ,
          supplier_contact_name          
    From  apps.ozf_sd_Request_headers
    WHERE request_header_id=  p_SDR_REQ_HEADER_ID
    FOR UPDATE NOWAIT;   
    
    -- Get Customer Details from  Quote 
    Cursor Get_Customer_Cur ( p_OM_Header_ID Number ) IS
       Select Sold_To_Org_ID Cust_Account_ID, 
              --Sold_To_Site_Use_ID Site_Use_ID,
              Invoice_to_org_Id Bill_Site_Use_ID,
              Ship_To_Org_ID    Ship_Site_Use_ID,
              Party_ID 
       From apps.Oe_Order_Headers ooh, 
            apps.Hz_Cust_Accounts hca
       Where ooh.Header_ID = p_OM_Header_ID 
       And   hca.Cust_Account_ID   = ooh.Sold_To_Org_ID ; 
      
    
    ---Get  Customer Request Information 
    Cursor Get_cust_Req_Cur (P_Request_Header_ID Number, 
                             p_Party_ID         Number, 
                             p_Cust_Account_ID  Number, 
                             p_Bill_Site_USe_ID Number, 
                             P_Ship_Site_Use_ID Number ) IS 
       SELECT * 
       FROM   ozf_sd_customer_details
       WHERE  1 = 1
       --AND    cust_usage_code    = C_CUST_USAGE_CODE
       AND    party_id           = p_party_id
       AND    cust_account_id    = p_cust_account_id
       AND    NVL(site_use_id,0) = NVL(p_Bill_site_use_id,0) --//Bugfix :8724614
       AND    end_customer_flag  = C_END_CUSTOMER_FLAG_NO
       AND    Request_Header_ID  = P_Request_Header_ID 
       AND    ROWNUM =1  
       UNION 
       SELECT * 
       FROM   ozf_sd_customer_details
       WHERE  1 = 1
       --AND    cust_usage_code    = C_CUST_USAGE_CODE
       AND    party_id           = p_party_id
       AND    cust_account_id    = p_cust_account_id
       AND    NVL(site_use_id,0) = NVL(p_Ship_site_use_id,0) --//Bugfix :8724614
       AND    end_customer_flag  = C_END_CUSTOMER_FLAG_NO
       AND    Request_Header_ID  = P_Request_Header_ID 
       AND    ROWNUM =1  ;
       
   Cursor  Get_Lines_Cur ( p_Header_ID         Number, 
                           p_SDR_REQ_HEADER_ID Number ) Is 
      Select ool.Inventory_Item_Id, ool.Unit_Selling_Price, ool.ORDER_QUANTITY_UOM, 
             ool.ORDERED_QUANTITY, ool.ORG_ID, ooh.TRANSACTIONAL_CURR_CODE  Currency_Code, 
             ool.Request_Date, ooh.Order_Number, ool.Line_ID, ool.Attribute5, ool.Ship_From_Org_ID
      From apps.Oe_order_Lines ool, 
           apps.oe_Order_Headers ooh
      Where ool.Header_ID = p_Header_ID 
      ANd   ooh.Header_ID = ool.Header_ID 
      And Not Exists ( 
         Select 1 
         From apps.OZF_SD_REQUEST_LINES_ALL
         Where Attribute1 = ool.Line_Id 
         And   Request_Header_ID = p_SDR_REQ_HEADER_ID ) ; 
                           
   Cursor Get_Item_Cost_Cur(P_Inventory_Item_Id  Number, 
                     P_Organization_Id    Number ) Is 
      Select Item_Cost
      From cst_item_costs cic,
           cst_cost_types  cct 
      Where Upper(cct.Cost_type) = ('AVERAGE')
      And cic.Cost_Type_Id = cct.Cost_Type_ID 
      and cic.Inventory_Item_ID = P_INventory_Item_ID
      and cic.Organization_Id   = p_Organization_ID  ;
    -- Satish U: 10-APR-2012  
    C_FORMULA_VENDOR_ITEM_COST    CONSTANT VARCHAR2(30) :=  'WC_VENDOR_ITEM_COST'; 
    l_Price_Formula_ID          Number ; 
    
    Cursor Get_Formula_Cur  Is 
        Select Price_Formula_Id 
        From   QP_PRICE_FORMULAS_VL
        Where  NAME             = C_FORMULA_VENDOR_ITEM_COST;   

   Begin
      ---Initialize l_Return_Stauts Variable 
      l_Return_Status := 'S'; 
    
      l_MODULE_NAME      := G_MODULE_NAME || l_API_NAME ; 
      
      Fnd_File.Put_Line ( fnd_file.Log, 'Begning of Main API :' );
      Fnd_File.Put_Line ( Fnd_File.LOG, 'OM Header ID is  :' || p_OM_Header_ID );
      Fnd_File.Put_Line ( Fnd_File.LOG, 'SDR Request Header ID is :' || p_SDR_Req_Header_ID );
      
      
      -- Get SDR Header Request Record Information 
      l_err_callpoint := 'Before Opening Cursor SD_HDR_CUR' ; 
      For SDR_HDR_Rec In SDR_Hdr_Cur(p_SDR_REQ_HEADER_ID)  Loop 
          Fnd_File.Put_Line ( Fnd_File.LOG ,'Populating SDR Request Header :' );
          l_err_callpoint := '110 : Populating SDR Request Header' ; 
          l_SDR_HDR_Rec.Request_Header_ID         :=    SDR_HDR_Rec.Request_Header_ID ;
          l_SDR_HDR_Rec.object_version_number     :=    SDR_HDR_Rec.object_version_number ;
          
          
          l_SDR_HDR_Rec.request_number             :=    FND_API.g_miss_char ; 
          l_Request_Number                         :=    SDR_HDR_Rec.Request_Number;
          
          l_SDR_HDR_Rec.request_start_date       :=    SDR_HDR_Rec.request_start_date  ;
          L_SDR_HDR_Rec.request_end_date         :=    SDR_HDR_Rec.request_end_date    ;
          l_SDR_HDR_Rec.user_status_id           :=    SDR_HDR_Rec.user_status_id        ;
          l_SDR_HDR_Rec.request_outcome          :=    SDR_HDR_Rec.request_outcome       ;
          
          l_SDR_HDR_Rec.request_currency_code   :=     SDR_HDR_Rec.request_currency_code  ;
          l_SDR_HDR_Rec.authorization_number      :=   SDR_HDR_Rec.authorization_number   ;
          
          l_SDR_HDR_Rec.attribute_category          :=   SDR_HDR_Rec.attribute_category      ;
          l_SDR_HDR_Rec.attribute1                  :=   SDR_HDR_Rec.attribute1              ;
          l_SDR_HDR_Rec.attribute2                  :=   SDR_HDR_Rec.attribute2              ;
          l_SDR_HDR_Rec.attribute3                  :=   SDR_HDR_Rec.attribute3              ;
          l_SDR_HDR_Rec.attribute4                  :=   SDR_HDR_Rec.attribute4              ;
          l_SDR_HDR_Rec.attribute5                  :=   SDR_HDR_Rec.attribute5              ;
          l_SDR_HDR_Rec.attribute6                  :=   SDR_HDR_Rec.attribute6              ;
          l_SDR_HDR_Rec.attribute7                  :=   SDR_HDR_Rec.attribute7              ;
          l_SDR_HDR_Rec.attribute8                  :=   SDR_HDR_Rec.attribute8              ;
          l_SDR_HDR_Rec.attribute9                  :=   SDR_HDR_Rec.attribute9              ;
          l_SDR_HDR_Rec.attribute10                 :=   SDR_HDR_Rec.attribute10             ;
          l_SDR_HDR_Rec.attribute11                 :=   SDR_HDR_Rec.attribute11             ;
          l_SDR_HDR_Rec.attribute12                 :=   SDR_HDR_Rec.attribute12             ;
          l_SDR_HDR_Rec.attribute13                 :=   SDR_HDR_Rec.attribute13             ;
          l_SDR_HDR_Rec.attribute14                 :=   SDR_HDR_Rec.attribute14             ;
          l_SDR_HDR_Rec.attribute15                 :=   SDR_HDR_Rec.attribute15             ;
          l_SDR_HDR_Rec.supplier_id                 :=   SDR_HDR_Rec.supplier_id             ;
          l_SDR_HDR_Rec.supplier_site_id            :=   SDR_HDR_Rec.supplier_site_id         ;
          l_SDR_HDR_Rec.supplier_contact_id         :=   SDR_HDR_Rec.supplier_contact_id       ;
          l_SDR_HDR_Rec.internal_submission_date    :=   SDR_HDR_Rec.internal_submission_date    ;
          --l_SDR_HDR_Rec.asignee_response_by_date    :=   SDR_HDR_Rec.asignee_response_by_date    ;
          --l_SDR_HDR_Rec.asignee_response_date       :=   SDR_HDR_Rec.asignee_response_date       ;
          l_SDR_HDR_Rec.submtd_by_for_supp_approval :=   SDR_HDR_Rec.submtd_by_for_supp_approval;
          l_SDR_HDR_Rec.supplier_response_by_date   :=   SDR_HDR_Rec.supplier_response_by_date  ;
          l_SDR_HDR_Rec.supplier_response_date      :=   SDR_HDR_Rec.supplier_response_date      ;
          l_SDR_HDR_Rec.supplier_submission_date    :=   SDR_HDR_Rec.supplier_submission_date    ;
          l_SDR_HDR_Rec.requestor_id                :=   SDR_HDR_Rec.requestor_id               ;
          l_SDR_HDR_Rec.supplier_quote_number       :=   SDR_HDR_Rec.supplier_quote_number      ;
          l_SDR_HDR_Rec.internal_order_number       :=   SDR_HDR_Rec.internal_order_number      ;
          l_SDR_HDR_Rec.sales_order_currency        :=   SDR_HDR_Rec.sales_order_currency       ;
          --l_SDR_HDR_Rec.request_source              :=   SDR_HDR_Rec.request_source             ;
          --l_SDR_HDR_Rec.asignee_resource_id         :=   SDR_HDR_Rec.asignee_resource_id        ;
          l_SDR_HDR_Rec.org_id                      :=   SDR_HDR_Rec.org_id                     ;
          --l_SDR_HDR_Rec.security_group_id           :=   SDR_HDR_Rec.security_group_id           ;
          l_SDR_HDR_Rec.accrual_type                :=   SDR_HDR_Rec.accrual_type                  ;
          l_SDR_HDR_Rec.cust_account_id              :=  SDR_HDR_Rec.cust_account_id               ;
          l_SDR_HDR_Rec.supplier_contact_email_address := SDR_HDR_Rec.supplier_contact_email_address ;
          l_SDR_HDR_Rec.supplier_contact_phone_number   := SDR_HDR_Rec.supplier_contact_phone_number  ;
          l_SDR_HDR_Rec.request_type_setup_id           := SDR_HDR_Rec.request_type_setup_id          ;
          l_SDR_HDR_Rec.request_basis                 :=   SDR_HDR_Rec.request_basis                ;
          l_SDR_HDR_Rec.supplier_contact_name    :=      SDR_HDR_Rec.supplier_contact_name;
          l_SDR_HDR_Rec.User_ID                  := FND_GLOBAL.User_Id;
          l_err_callpoint := '120 : Populating SDR Request Header' ;
      End Loop ; 
      
      
      For Cust_Rec In Get_Customer_Cur ( p_OM_Header_ID) Loop 
         l_err_callpoint := '130 : Populating SDR Customer Record' ;
         Fnd_File.Put_Line ( Fnd_File.LOG, 'Populating SDR Customer Record :' );
         -- Check if a Record exists in Customer Request Tables 
         Fnd_File.Put_Line ( Fnd_File.LOG, 'Party ID  :' || Cust_Rec.Party_ID);
         Fnd_File.Put_Line ( Fnd_File.LOG, 'Cust Account ID  :' || Cust_Rec.Cust_Account_ID);
         Fnd_File.Put_Line ( Fnd_File.LOG, 'Bill Site Use ID  :' ||  Cust_Rec.Bill_Site_Use_ID);
         Fnd_File.Put_Line ( Fnd_File.LOG, 'Ship Site Use ID  :' ||  Cust_Rec.Ship_Site_Use_ID);
         
         l_err_callpoint := '140 : Getting Customer Account ID and other info' ;
         FOr Cust_Req_Rec In Get_cust_Req_Cur (P_Request_Header_ID => p_SDR_Req_Header_ID, 
                               p_Party_ID         => Cust_Rec.Party_ID, 
                               p_Cust_Account_ID  => Cust_Rec.Cust_Account_ID, 
                               p_Bill_Site_USe_ID => Cust_Rec.Bill_Site_Use_ID,
                               p_Ship_Site_USe_ID => Cust_Rec.Ship_Site_Use_ID ) Loop
            
             Fnd_File.Put_Line ( Fnd_File.LOG, 'Getting SDR Customer Record From Database:' );
             l_SDR_Cust_rec.REQUEST_CUSTOMER_ID := Cust_Req_Rec.REQUEST_CUSTOMER_ID;
             l_SDR_Cust_Rec.REQUEST_HEADER_ID   := Cust_Req_Rec.REQUEST_HEADER_ID ; 
             --l_SDR_Cust_Rec.REQUEST_NUMBER      := Cust_Req_Rec.REQUEST_NUMBER ; 
             l_SDR_Cust_Rec.CUST_ACCOUNT_ID     := Cust_Req_Rec.CUST_ACCOUNT_ID ; 
             l_SDR_Cust_Rec.Party_ID            := Cust_Req_Rec.Party_ID; 
             l_SDR_Cust_Rec.Site_Use_ID         := Cust_Req_Rec.Site_Use_ID ; 
             l_SDR_Cust_Rec.Cust_Usage_Code     := Cust_Req_Rec.Cust_Usage_Code; 
             
             l_SDR_Cust_Rec.attribute_category   := Cust_Req_Rec.attribute_category ; 
             l_SDR_Cust_Rec.attribute1           := Cust_Req_Rec.attribute1 ; 
             l_SDR_Cust_Rec.attribute2           := Cust_Req_Rec.attribute2 ; 
             l_SDR_Cust_Rec.attribute3           := Cust_Req_Rec.attribute3 ; 
             l_SDR_Cust_Rec.attribute4           := Cust_Req_Rec.attribute4 ; 
             l_SDR_Cust_Rec.attribute5           := Cust_Req_Rec.attribute5 ; 
             l_SDR_Cust_Rec.attribute6           := Cust_Req_Rec.attribute6 ; 
             l_SDR_Cust_Rec.attribute7           := Cust_Req_Rec.attribute7 ; 
             l_SDR_Cust_Rec.attribute8           := Cust_Req_Rec.attribute8 ; 
             l_SDR_Cust_Rec.attribute9           := Cust_Req_Rec.attribute9 ; 
             l_SDR_Cust_Rec.attribute10           := Cust_Req_Rec.attribute10 ; 
             l_SDR_Cust_Rec.attribute11           := Cust_Req_Rec.attribute11 ; 
             l_SDR_Cust_Rec.attribute12           := Cust_Req_Rec.attribute12 ; 
             l_SDR_Cust_Rec.attribute13           := Cust_Req_Rec.attribute13 ; 
             l_SDR_Cust_Rec.attribute14           := Cust_Req_Rec.attribute14 ; 
             l_SDR_Cust_Rec.attribute15           := Cust_Req_Rec.attribute15 ; 
             l_SDR_Cust_Rec.End_Customer_Flag     := Cust_Req_Rec.End_Customer_Flag ; 
             l_SDR_Cust_Rec.cust_usage_value      := Cust_Req_Rec.cust_usage_value ; 
             
         End Loop ; 
         
        
         
      End Loop; 
      
      l_SDR_Cust_Tbl(1)  := l_SDR_Cust_Rec; 
      
      -- Update Product Information p_SDR_Req_Header_ID
      i := 0; 
      For LInes_rec In Get_Lines_Cur ( p_Header_ID         => p_OM_Header_ID, 
                                       p_SDR_REQ_HEADER_ID => p_SDR_Req_Header_ID ) Loop 
         l_err_callpoint := '160 : Building SDR Line Record From OM Quote ' ;
         Fnd_File.Put_Line ( Fnd_File.LOG, 'Building SDR Line Record From OM Quote:' );
         i := i + 1; 
         l_Lines_Count := l_Lines_Count + 1; 
         l_SDR_lines_rec.Request_Header_ID        := p_SDR_Req_Header_ID ;
         l_SDR_lines_rec.product_context          := C_PRODUCT_CONTEXT; 
         l_SDR_lines_rec.inventory_item_id        := Lines_Rec.Inventory_Item_ID; 
         l_SDR_Lines_Rec.item_uom                 := Lines_Rec.Order_Quantity_UOM ; 
         -- Satish U: 09-APR-2012 
         l_SDR_Lines_Rec.requested_discount_type  := C_DISCOUNT_TYPE_NEW_PRICE ; --- C_DISCOUNT_TYPE_AMT; 
        -- l_SDR_Lines_Rec.requested_discount_value := Lines_rec.Unit_Selling_Price; 
        
        -- Satish U: 09-APR-2012 : 
         l_SDR_Lines_Rec.approved_discount_type   := C_DISCOUNT_TYPE_NEW_PRICE ; -- C_DISCOUNT_TYPE_AMT; 
         l_SDR_Lines_rec.start_date               := Lines_Rec.Request_Date ; 
         l_SDR_Lines_Rec.Org_ID                   := Lines_Rec.Org_ID ; 
         l_SDR_Lines_Rec.requested_discount_currency := Lines_Rec.Currency_Code ; 
         l_SDR_Lines_Rec.product_cost_currency    := Lines_Rec.Currency_Code ;
         l_SDR_Lines_REc.end_customer_currency    := Lines_Rec.Currency_Code ;
         l_SDR_Lines_Rec.approved_discount_currency := Lines_Rec.Currency_Code ;
         l_SDR_Lines_Rec.Attribute1                 := Lines_Rec.Line_ID ;
         l_Order_Number                             := Lines_Rec.Order_Number;
         
         l_err_callpoint := '170 : Before getting Attribute5 value ' ;
         
         If Lines_Rec.Attribute5 Is Not Null Then 
           l_Product_Cost :=  To_Number(Lines_Rec.Attribute5) ;
           l_SDR_Lines_Rec.requested_discount_value  := l_product_Cost ;  
         Else
            FOr Item_Cost_Rec In Get_Item_Cost_Cur(Lines_Rec.Inventory_Item_Id  , 
                                                   Lines_Rec.Ship_From_Org_Id    ) Loop 
                l_Product_Cost := Item_Cost_Rec.Item_Cost ; 
                l_SDR_Lines_Rec.requested_discount_value  := l_product_Cost ;
            End Loop; 
            
         End IF;
         
         -- Satish U: 11-APR-2012 
        l_err_callpoint := '180 : Getting Pricing Formula information ' ;
        For Formula_Rec In Get_Formula_Cur   Loop 
            l_Price_Formula_ID := Formula_Rec.Price_Formula_ID ; 
            l_SDR_Lines_Rec.Cost_Basis := l_Price_Formula_ID ; 
        ENd Loop; 
         
         
         l_SDR_Lines_Tbl(i) := l_SDR_Lines_Rec ; 
      End Loop; 
      l_SDR_Lines_Rec.requested_discount_value  := NULL; 
      l_SDR_Lines_Rec.Cost_Basis := 15038 ;
      Fnd_File.Put_Line ( Fnd_File.LOG, 'Call Public API Add Lines to existing SDR Request Header:' );
      -- Call API to Update 
      l_err_callpoint := '190 : Callin public API  OZF_SD_REQUEST_PUB.update_sd_request' ;
      OZF_SD_REQUEST_PUB.update_sd_request(
        p_api_version_number         => 1.0,
        p_init_msg_list              => FND_API.G_FALSE,
        p_commit                     => FND_API.G_FALSE,
        p_validation_level           => FND_API.g_valid_level_full,
        x_return_status              => l_Return_Status,
        x_msg_count                  => l_Msg_Count,
        x_msg_data                   => l_Msg_Data,
        p_SDR_hdr_rec                => l_SDR_Hdr_rec,
        p_SDR_lines_tbl              => l_SDR_lines_tbl,
        p_SDR_cust_tbl               => l_SDR_cust_tbl
      );
    
      l_err_callpoint := '200 :  update_SD_Request API Success :' ; 
      Fnd_File.Put_Line ( Fnd_File.LOG, 'Return Status of  OZF_SD_REQUEST_PUB.update_sd_request is :' || l_Return_Status );
    
      If l_Return_Status = 'S' Then 
         l_err_callpoint := '200 :  update_SD_Request API Success :' ; 
         --SatishU :  Since G_MIS_CHAR is passed while updating SD  Request Number, Neeed to be updated with 
         -- Original Request Number after Success 
         Update ozf_sd_Request_headers_all_b
             Set Request_Number = l_Request_Number 
         Where Request_Header_ID = p_SDR_Req_Header_ID ;
           
         
         Fnd_File.Put_Line ( fnd_file.output, 'SDR Request Header : ' || l_request_number);
         Fnd_File.Put_Line ( fnd_file.output,'OM Quote Number    : ' || l_Order_Number ); 
         Fnd_File.Put_Line ( fnd_file.output,'Number of Quote Lines imported    : ' || l_Lines_Count ); 
      
         Commit; 
      Else
             l_err_callpoint := '200 :  update_SD_Request API Failed :' ; 
          FOR i IN 1 .. l_msg_count loop
             l_Msg_Data := l_msg_data || oe_msg_pub.get( p_msg_index => i, p_encoded => 'F'  ); 
          End Loop ;
          Fnd_File.Put_Line ( fnd_file.output,'OZF_SD_REQUEST_PUB.update_sd_request  errored out  : ' ); 
          Fnd_File.Put_Line ( fnd_file.output,'Error Message is  : ' || l_Msg_Data );
          RetCode := 1; 
          Rollback ; 
      End If; 
      
   Exception 
      When Others Then 
          l_err_callpoint := '210 :  When Others Exception :' ; 
          l_Return_Status := 'U'; 
          Fnd_File.Put_Line ( Fnd_File.LOG, 'WHen Others Exception :' || SQLERRM);
          -- PRogram Should end with Warning  status 
          RetCode := 2; 
      
          errbuf := SQLERRM;
          retcode := 2;
          ROLLBACK;
    
          FND_FILE.Put_Line(FND_FILE.Log, SQLERRM);
          FND_FILE.Put_Line(FND_FILE.Output, SQLERRM);

          XXCUS_ERROR_PKG.XXCUS_ERROR_MAIN_API(p_called_from     => G_MODULE_NAME,
                                             p_calling           => l_err_callpoint,
                                             p_ora_error_msg     => SQLERRM,
                                             p_error_desc        => 'When Others Exception XXWC_OZF_VENDOR_QUOTE_PKG.MAIN',
                                             p_distribution_list => l_distro_list,
                                             p_module            => 'OZF');
      
   End MAIN;
        
   --*********************************************************************************************************
    -- Function  Check_SSD_Modifier_Exists
    -- Satish U: 15-MAR-2012 : THis Procedure checks if Modifier Exists with Supplier Ship And Debit Request Number 
    --  For a Given Inventory Item ID and Sales ORder Line Id and Price List ID 
    --*********************************************************************************************************
                     
    Function  Check_SSD_Modifier_Exists(p_Inventory_Item_ID  In Number, 
                                        P_Price_List_ID      In Number, 
                                        P_SO_Line_ID         In Number ) Return  Varchar2  IS 
       Cursor Check_Mod_Cur IS (
          Select qlh.name, ool.Line_Number, ool.Shipment_Number
          From apps.qp_list_headers qlh, 
               apps.ozf_sd_Request_headers srh, 
               oe_price_adjustments  opa ,
               apps.oe_Order_Lines ool 
          Where srh.Request_number = qlh.name
          and   qlh.list_Header_Id = opa.list_header_ID 
          And   ool.Line_Id =  opa.line_Id 
          And   ool.line_Id = p_SO_Line_ID ); 
          
       l_Modifer_Exists     Varchar2(1) := 'N'; 
    Begin 
       For Check_Mod_Rec in Check_Mod_Cur  Loop 
          l_Modifer_Exists := 'Y'; 
       End Loop ; 
       Return l_Modifer_Exists ; 
    Exception 
       When Others Then 
          l_Modifer_Exists := 'N';
          Return l_Modifer_Exists ;
    End; 
    
    
    --*********************************************************************************************************
    -- Function  Get_Vendor_Item_Cost
    -- Satish U: 15-MAR-2012 : This PRocedure will check get the Vendor Cost of the Item , for  a given Item, SO Line and Organization
    --  For a Given Inventory Item ID and Sales ORder Line Id and Price List ID 
    --*********************************************************************************************************
                                        
    Function Get_Vendor_Item_Cost(p_Inventory_Item_ID IN Number , 
                                  P_SO_Line_ID        IN Number , 
                                  P_Organization_ID   In Number  ) Return Number  Is 
                                  
       
          
        Cursor Get_Vendor_Cur (p_Line_ID      Number ,
                               p_Supplier_ID  Number) IS  
            Select poh.Vendor_ID SUPPLIER_ID, poh.Vendor_SITE_ID SUPPLIER_SITE_ID
            From apps.po_headers poh 
            Where  poh.type_lookup_code in ('BLANKET', 'STANDARD') 
            AND poh.enabled_flag          = 'Y'
            AND poh.authorization_status  = 'APPROVED'
            AND poh.approved_flag          = 'Y'
            ANd POH.Vendor_ID = P_Supplier_ID 
            And exists ( Select 1 
               From  apps.Po_lines pol ,
                     apps.oe_Order_Lines ool 
               Where poh.Po_Header_ID = pol.PO_Header_ID 
               ANd   pol.Item_ID      = ool.Inventory_Item_ID 
               AND   OOl.Line_ID      = p_Line_ID 
               AND   NVL (POL.cancel_flag, 'N') = 'N' )
            And poh.creation_date =
                  (SELECT MAX (poh2.creation_date)
                   FROM apps.po_headers poh2
                      , apps.po_lines pol2
                      , apps.oe_order_lines ool2
                   WHERE  poh2.type_lookup_code in ('BLANKET', 'STANDARD') 
                   AND poh2.enabled_flag = 'Y'
                   AND poh2.authorization_status = 'APPROVED'
                   AND poh2.approved_flag = 'Y'
                   AND poh2.po_header_id = pol2.po_header_id
                   AND NVL (pol2.cancel_flag, 'N') = 'N'
                   AND pol2.item_id = ool2.INVENTORY_ITEM_ID
                   And ool2.Line_ID = p_Line_ID 
                   AND poh2.vendor_id = poh.VENDOR_ID
                   AND poh2.vendor_site_id =   poh.vendor_site_id
                   AND NVL(pol2.cancel_flag, 'N') = 'N');  
        
        Cursor Get_BPA_Price (P_VENDOR_ID          NUMBER, 
                              P_VENDOR_SITE_ID     NUMBER , 
                              P_INVENTORY_ITEM_ID  NUMBER ) IS 
            Select b.LIST_PRICE_PER_UNIT , b.UNIT_PRICE , b.UNIT_MEAS_LOOKUP_CODE
            FROM apps.po_headers a, apps.po_lines b, po_ship_to_loc_org_v c
            WHERE     a.type_lookup_code = 'BLANKET'
            AND a.enabled_flag = 'Y'
            AND a.authorization_status = 'APPROVED'
            AND a.approved_flag = 'Y'
            AND a.po_header_id = b.po_header_id
            AND NVL (b.cancel_flag, 'N') = 'N'
            --AND NVL (b.closed_code, 'x') <> 'CLOSED'
            --AND NVL (b.expiration_date, SYSDATE + 1) > SYSDATE
            AND b.item_id   = P_INVENTORY_ITEM_ID
            AND a.vendor_id = P_VENDOR_ID
            AND a.vendor_site_id = NVL (P_VENDOR_SITE_ID, a.vendor_site_id)
            AND a.ship_to_location_id = c.ship_to_location_id
            --AND c.inventory_organization_id =  NVL (i_organization_id, c.inventory_organization_id)
            AND NVL (b.cancel_flag, 'N') = 'N'
            AND b.creation_date =
                  (SELECT MAX (b.creation_date)
                   FROM apps.po_headers a
                      , apps.po_lines b
                      , po_ship_to_loc_org_v c
                   WHERE     a.type_lookup_code = 'BLANKET'
                    AND a.enabled_flag = 'Y'
                   AND a.authorization_status = 'APPROVED'
                   AND a.approved_flag = 'Y'
                   AND a.po_header_id = b.po_header_id
                   AND NVL (b.cancel_flag, 'N') = 'N'
                   --AND NVL (b.closed_code, 'x') <> 'CLOSED'
                   --AND NVL (b.expiration_date, SYSDATE + 1) > SYSDATE
                   AND b.item_id = P_INVENTORY_ITEM_ID
                   AND a.vendor_id = P_VENDOR_ID
                   AND a.vendor_site_id =   NVL (P_VENDOR_SITE_ID, a.vendor_site_id)
                   AND a.ship_to_location_id = c.ship_to_location_id
                   --AND c.inventory_organization_id =   NVL (i_organization_id , c.inventory_organization_id)
                   AND NVL (b.cancel_flag, 'N') = 'N');       
            
            
        Cursor Get_PO_RCV_Price (P_VENDOR_ID          NUMBER, 
                             P_VENDOR_SITE_ID     NUMBER , 
                             P_INVENTORY_ITEM_ID  NUMBER ) Is 
            SELECT pol.LIST_PRICE_PER_UNIT , pol.UNIT_PRICE , pol.UNIT_MEAS_LOOKUP_CODE
            FROM PO_HEADERS_V poh, 
                 apps.po_lines   pol, 
                 ( SELECT POH2.PO_Header_ID,  Max(POH2.APPROVED_DATE)  APPROVED_DATE 
                   From PO_HEADERS_V poh2, 
                        apps.po_lines   pol2
                   WHERE 1 = 1 
                   -- poh2.type_Lookup_code     = 'BLANKET'
                   AND   poh2.AUTHORIZATION_STATUS = 'APPROVED' 
                   And   poh2.APPROVED_FLAG        = 'Y' 
                   And   poh2.Vendor_ID            = P_VENDOR_ID 
                   And   poh2.Vendor_Site_ID       = P_VENDOR_SITE_ID 
                   ANd   poh2.PO_Header_ID         = pol2.PO_Header_ID 
                   ANd   pol2.Item_ID              = P_INVENTORY_ITEM_ID 
                   AND   NVL(POL2.CANCEL_FLAG , 'N')   = 'N' 
                   AND   EXISTS ( SELECT 1
                        FROM RCV_SHIPMENT_LINES  rsl, 
                             RCV_SHIPMENT_HEADERS rsh
                        WHERE Rsh.Shipment_Header_ID = rsl.Shipment_Line_ID 
                        And rsh.RECEIPT_SOURCE_CODE  = 'VENDOR' 
                        AND RSL.SHIPMENT_LINE_STATUS_CODE In ( 'PARTIALLY RECEIVED', 'FULLY RECEIVED') 
                        AND RSL.SOURCE_DOCUMENT_CODE = 'PO'
                        AND RSL.PO_HEADER_ID         = pol2.PO_HEADER_ID 
                        AND RSL.PO_LINE_ID           = pol2.PO_LINE_ID    ) 
                   GROUP BY POH2.PO_HEADER_ID) poh3
            Where 1 =1 
            --poh.type_Lookup_code     = 'BLANKET'
            AND   poh.AUTHORIZATION_STATUS = 'APPROVED' 
            And   poh.APPROVED_FLAG        = 'Y' 
            And   poh.Vendor_ID            = P_VENDOR_ID 
            And   poh.Vendor_Site_ID       = P_VENDOR_SITE_ID 
            ANd   poh.PO_Header_ID         = pol.PO_Header_ID 
            ANd   pol.Item_ID              = P_INVENTORY_ITEM_ID 
            AND   NVL(POL.CANCEL_FLAG , 'N')   = 'N' 
            AND   POH.PO_HEADER_ID   = POH3.PO_HEADER_ID 
            AND   TRUNC(POH.APPROVED_DATE ) = TRUNC(POH3.APPROVED_DATE) ;
            
            CURSOR Get_PO_Price (P_VENDOR_ID           NUMBER, 
                                 P_VENDOR_SITE_ID      NUMBER , 
                                  P_INVENTORY_ITEM_ID  NUMBER ) Is 
            SELECT b.LIST_PRICE_PER_UNIT , b.UNIT_PRICE , b.UNIT_MEAS_LOOKUP_CODE
            FROM apps.po_headers a, apps.po_lines b, po_ship_to_loc_org_v c
            WHERE     a.type_lookup_code = 'STANDARD'
            AND a.enabled_flag = 'Y'
            AND a.authorization_status = 'APPROVED'
            AND a.approved_flag = 'Y'
            AND a.po_header_id = b.po_header_id
            AND NVL (b.cancel_flag, 'N') = 'N'
            --AND NVL (b.closed_code, 'x') <> 'CLOSED'
            --AND NVL (b.expiration_date, SYSDATE + 1) > SYSDATE
            AND b.item_id   = P_INVENTORY_ITEM_ID
            AND a.vendor_id = P_VENDOR_ID
            AND a.vendor_site_id = NVL (P_VENDOR_SITE_ID, a.vendor_site_id)
            AND a.ship_to_location_id = c.ship_to_location_id
            --AND c.inventory_organization_id =  NVL (i_organization_id, c.inventory_organization_id)
            AND NVL (b.cancel_flag, 'N') = 'N'
            AND b.creation_date =
                  (SELECT MAX (b.creation_date)
                   FROM apps.po_headers a
                      , apps.po_lines b
                      , po_ship_to_loc_org_v c
                   WHERE     a.type_lookup_code = 'STANDARD'
                    AND a.enabled_flag = 'Y'
                   AND a.authorization_status = 'APPROVED'
                   AND a.approved_flag = 'Y'
                   AND a.po_header_id = b.po_header_id
                   AND NVL (b.cancel_flag, 'N') = 'N'
                   --AND NVL (b.closed_code, 'x') <> 'CLOSED'
                   --AND NVL (b.expiration_date, SYSDATE + 1) > SYSDATE
                   AND b.item_id = P_INVENTORY_ITEM_ID
                   AND a.vendor_id = P_VENDOR_ID
                   AND a.vendor_site_id =   NVL (P_VENDOR_SITE_ID, a.vendor_site_id)
                   AND a.ship_to_location_id = c.ship_to_location_id
                   --AND c.inventory_organization_id =   NVL (i_organization_id , c.inventory_organization_id)
                   AND NVL (b.cancel_flag, 'N') = 'N');  
            
        l_org_id            Number ; 
        l_Vendor_Cost       Number(15,6) := 0;     
        l_Module_Name       VARCHAR2(80) := 'XXWC_OZF_VENDOR_QUOTE_PKG.GET_VENDOR_ITEM_COST';
        l_SSD_Request_Number Varchar2(240) ; 
        l_Supplier_ID       Number ; 
        l_Supplier_Site_ID  Number ; 
        l_Modifier_Name     Varchar2(240) ; 
        l_List_Header_ID    Number;
        
        Item_Cost_Found_Exp   Exception ; 
    Begin
       XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => G_LEVEL_PROCEDURE,
                                     p_mod_name      => l_Module_Name,
                                     P_DEBUG_MSG     => '1000 :  Begining of API :' || l_Module_Name );
       -- Begining of API 
      -- 30-Mar-2012 : Check if Line ID linked to SSD Request , 
      -- If so get Vendor_ID and Vendor Site ID 
      Begin 
         Select srh.REQUEST_NUMBER, srh.SUPPLIER_ID, srh.SUPPLIER_SITE_ID, 
             qlh.name, qlh.List_Header_ID 
         Into l_SSD_Request_Number, l_Supplier_ID, l_Supplier_Site_ID, 
            l_Modifier_Name, l_List_Header_ID 
         From apps.qp_list_headers  qlh, 
               apps.ozf_sd_Request_headers srh, 
               oe_price_adjustments  opa ,
               apps.oe_Order_Lines ool 
         Where srh.Request_number = qlh.name
         and   qlh.list_Header_Id = opa.list_header_ID 
         And   ool.Line_Id =  opa.line_Id 
         And   ool.line_Id = p_SO_Line_ID 
         And   rownum = 1 ; 
      Exception 
         When Others Then 
            l_Supplier_ID        := Null ; 
            l_Supplier_Site_ID   := Null;
            l_SSD_Request_Number := Null; 
            l_Modifier_Name      := Null; 
            l_List_Header_ID     := Null; 
      ENd ; 
      
      XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => G_LEVEL_PROCEDURE,
                                    p_mod_name      => l_Module_Name,
                                    P_DEBUG_MSG     => '1001 :  SSD Request Number :' || l_SSD_Request_Number );
      XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => G_LEVEL_PROCEDURE,
                                    p_mod_name      => l_Module_Name,
                                    P_DEBUG_MSG     => '1002 :  Supplier ID :' || l_Supplier_ID );
      XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => G_LEVEL_PROCEDURE,
                                    p_mod_name      => l_Module_Name,
                                    P_DEBUG_MSG     => '1003 :  Supplier Site ID :' || l_Supplier_Site_ID );
      
      
       For Vendor_Rec  In Get_Vendor_Cur(P_SO_Line_ID, l_Supplier_ID)   Loop 
       
           XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => G_LEVEL_PROCEDURE,
                                    p_mod_name      => l_Module_Name,
                                    P_DEBUG_MSG     => '1010 :  Inside Vendor Cursor :'  );
           
           -- Check if there is any BPA Document exists for the Vendor 
           FOR BPA_Rec In  Get_BPA_Price (P_VENDOR_ID         => Vendor_Rec.SUPPLIER_ID, 
                                          P_VENDOR_SITE_ID    => Vendor_Rec.SUPPLIER_SITE_ID , 
                                          P_INVENTORY_ITEM_ID => p_Inventory_Item_ID )  LOOP 
               XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => G_LEVEL_PROCEDURE,
                                    p_mod_name      => l_Module_Name,
                                    P_DEBUG_MSG     => '1020 :  Inside BPA Cursor :'  );
               
               If l_Vendor_Cost  = 0 Then 
                  l_Vendor_Cost := BPA_REC.UNIT_PRICE ; 
               End If; 
               
               -- IF there are more then one Record get the least Unit _Price 
               If l_Vendor_Cost < BPA_REC.UNIT_PRICE  Then 
                  l_Vendor_Cost := BPA_REC.UNIT_PRICE ;  
               End If; 
           End Loop ; 
           
           -- Check if BPA Cursor has found any Records, If So then Return V
           If L_Vendor_Cost > 0 Then 
              Raise Item_Cost_Found_Exp ;
              --Return L_Vendor_Cost ; 
           ENd IF; 
           
           XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => G_LEVEL_PROCEDURE,
                                    p_mod_name      => l_Module_Name,
                                    P_DEBUG_MSG     => '1031 :  Vendor ID :' || Vendor_Rec.SUPPLIER_ID );
                                    
           XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => G_LEVEL_PROCEDURE,
                                    p_mod_name      => l_Module_Name,
                                    P_DEBUG_MSG     => '1032 :  Vendor ID :' || Vendor_Rec.SUPPLIER_SITE_ID );
                                    
           ---- Since BPA Price is not Found, Check  Unit_Price on PO_Lines that is most recently received. 
           FOR PO_RCV_Rec In  Get_PO_RCV_Price (P_VENDOR_ID         => Vendor_Rec.SUPPLIER_ID, 
                                          P_VENDOR_SITE_ID    => Vendor_Rec.SUPPLIER_SITE_ID , 
                                          P_INVENTORY_ITEM_ID => p_Inventory_Item_ID )  LOOP 
                                          
               XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => G_LEVEL_PROCEDURE,
                                    p_mod_name      => l_Module_Name,
                                    P_DEBUG_MSG     => '1040 :  Inside PO RCV Cursor :'  );
               
               If l_Vendor_Cost  = 0 Then 
                  l_Vendor_Cost := PO_RCV_Rec.UNIT_PRICE ; 
               End If; 
               
               -- IF there are more then one Record get the least Unit _Price 
               If l_Vendor_Cost < PO_RCV_Rec.UNIT_PRICE  Then 
                  l_Vendor_Cost := PO_RCV_Rec.UNIT_PRICE ;  
               End If; 
           End Loop ; 
           
           -- Check if BPA Cursor has found any Records, If So then Return V
           If L_Vendor_Cost > 0 Then 
              Raise Item_Cost_Found_Exp ;
              --Return L_Vendor_Cost ; 
           ENd IF; 
           -- Check if There is Any Approved PO for the Vendor 
           ---- Since BPA Price is not Found, Check  Unit_Price on PO_Lines that is most recently received. 
           FOR PO_Rec In  Get_PO_Price (P_VENDOR_ID         => Vendor_Rec.SUPPLIER_ID, 
                                          P_VENDOR_SITE_ID    => Vendor_Rec.SUPPLIER_SITE_ID , 
                                          P_INVENTORY_ITEM_ID => p_Inventory_Item_ID )  LOOP 
                                          
               XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => G_LEVEL_PROCEDURE,
                                    p_mod_name      => l_Module_Name,
                                    P_DEBUG_MSG     => '1050 :  Inside PO REC Cursor :' );
               
               If l_Vendor_Cost  = 0 Then 
                  l_Vendor_Cost := PO_Rec.UNIT_PRICE ; 
               End If; 
               
               -- IF there are more then one Record get the least Unit _Price 
               If l_Vendor_Cost < PO_Rec.UNIT_PRICE  Then 
                  l_Vendor_Cost := PO_Rec.UNIT_PRICE ;  
               End If; 
           End Loop ; 
           
           -- Check if BPA Cursor has found any Records, If So then Return V
           If L_Vendor_Cost > 0 Then 
              Raise Item_Cost_Found_Exp ;
              --Return L_Vendor_Cost ; 
           ENd IF; 
           
           XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => G_LEVEL_PROCEDURE,
                                    p_mod_name      => l_Module_Name,
                                    P_DEBUG_MSG     => '1060 :  Inside PO REC Cursor :' );
                                    
           --- Satish U: 10-APR-2012 :  Pull List Price from Transactional Organization 
           Select LIST_PRICE_PER_UNIT
           INto  L_Vendor_Cost
           From Mtl_System_Items_b 
           Where Inventory_Item_ID = p_Inventory_Item_ID 
           and Organization_ID     = P_Organization_ID  ; -- Fnd_Profile.Value('XXWC_ITEM_MASTER_ORG') ;
           
           Raise Item_Cost_Found_Exp ; 
           --Return L_Vendor_Cost ;
           
       ENd Loop; 
       
       XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => G_LEVEL_PROCEDURE,
                                    p_mod_name      => l_Module_Name,
                                    P_DEBUG_MSG     => '1065 :  Vendor Item Cost is  :' || L_Vendor_Cost );
                                    
       XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => G_LEVEL_PROCEDURE,
                                    p_mod_name      => l_Module_Name,
                                    P_DEBUG_MSG     => '1072 :  P Inventory Item ID is  :' || p_Inventory_Item_ID );
                                    
       XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => G_LEVEL_PROCEDURE,
                                    p_mod_name      => l_Module_Name,
                                    P_DEBUG_MSG     => '1074 :  Item Master Org ID is   :' || Fnd_Profile.Value('XXWC_ITEM_MASTER_ORG') );
                              
        XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => G_LEVEL_PROCEDURE,
                                    p_mod_name      => l_Module_Name,
                                    P_DEBUG_MSG     => '1074.2 :  Organization ID is   :' || P_Organization_ID );
                                    
       If l_Vendor_Cost = 0 And p_Inventory_Item_ID IS Not NULL Then 
          Begin
              XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => G_LEVEL_PROCEDURE,
                                    p_mod_name      => l_Module_Name,
                                    P_DEBUG_MSG     => '1075 :  Vendor Item Cost is  :' || L_Vendor_Cost );
              Select LIST_PRICE_PER_UNIT
              INto  L_Vendor_Cost
              From Mtl_System_Items_b 
              Where Inventory_Item_ID = p_Inventory_Item_ID 
              and Organization_ID     = P_Organization_ID ; -- Fnd_Profile.Value('XXWC_ITEM_MASTER_ORG') ;
              
               XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => G_LEVEL_PROCEDURE,
                                    p_mod_name      => l_Module_Name,
                                    P_DEBUG_MSG     => '1076 :  Vendor Item Cost is  :' || L_Vendor_Cost );
              Raise Item_Cost_Found_Exp ; 
          
          End;
       Else 
          Return l_Vendor_Cost ; 
       End If; 
       
    Exception 
       When Item_Cost_Found_Exp Then 
           Return L_Vendor_Cost ;
           
       When Others Then 
          XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => G_LEVEL_PROCEDURE,
                                    p_mod_name      => l_Module_Name,
                                    P_DEBUG_MSG     => '1060 :  When Others Error  :' || SQLERRM );
          
          Return NULL ;
           
    End ; 
    
    
  --*********************************************************************************************************
    -- Function  Get_Vendor_Item_Cost
    -- Satish U: 15-MAR-2012 : This PRocedure will check get the Vendor Cost of the Item , for  a given Item, SO Line and Organization
    --  For a Given Inventory Item ID and Sales ORder Line Id and Price List ID 
    --*********************************************************************************************************
                                        
    Function Get_Vendor_Item_Cost02(p_Inventory_Item_ID IN Number , 
                                  P_SO_Line_ID        IN Number , 
                                  P_Organization_ID   In Number  ) Return Varchar2  Is 
        Cursor Get_Vendor_Cur IS (
          Select srh.REQUEST_NUMBER, srh.SUPPLIER_ID, srh.SUPPLIER_SITE_ID, 
             qlh.name, ool.Line_Id, qlh.List_Header_ID 
          From apps.qp_list_headers  qlh, 
               apps.ozf_sd_Request_headers srh, 
               oe_price_adjustments  opa ,
               apps.oe_Order_Lines ool 
          Where srh.Request_number = qlh.name
          and   qlh.list_Header_Id = opa.list_header_ID 
          And   ool.Line_Id =  opa.line_Id 
          And   ool.line_Id = p_SO_Line_ID 
          And   rownum = 1 ); 
        
        Cursor Get_BPA_Price (P_VENDOR_ID          NUMBER, 
                              P_VENDOR_SITE_ID     NUMBER , 
                              P_INVENTORY_ITEM_ID  NUMBER ) Is 
            Select pol.LIST_PRICE_PER_UNIT , pol.UNIT_PRICE , pol.UNIT_MEAS_LOOKUP_CODE
            From PO_HEADERS_V poh, 
                 apps.po_lines   pol, 
                 ( Select POH2.PO_Header_ID,  Max(POH2.APPROVED_DATE)  APPROVED_DATE 
                   From PO_HEADERS_V poh2, 
                        apps.po_lines   pol2
                   Where poh2.type_Lookup_code     = 'BLANKET'
                   AND   poh2.AUTHORIZATION_STATUS = 'APPROVED' 
                   And   poh2.APPROVED_FLAG        = 'Y' 
                   And   poh2.Vendor_ID            = P_VENDOR_ID 
                   And   poh2.Vendor_Site_ID       = P_VENDOR_SITE_ID 
                   ANd   poh2.PO_Header_ID         = pol2.PO_Header_ID 
                   ANd   pol2.Item_ID              = P_INVENTORY_ITEM_ID 
                   AND   NVL(POL2.CANCEL_FLAG , 'N')   = 'N' 
                   GROUP BY POH2.PO_HEADER_ID) poh3
            Where poh.type_Lookup_code     = 'BLANKET'
            AND   poh.AUTHORIZATION_STATUS = 'APPROVED' 
            And   poh.APPROVED_FLAG        = 'Y' 
            And   poh.Vendor_ID            = P_VENDOR_ID 
            And   poh.Vendor_Site_ID       = P_VENDOR_SITE_ID 
            ANd   poh.PO_Header_ID         = pol.PO_Header_ID 
            ANd   pol.Item_ID              = P_INVENTORY_ITEM_ID 
            AND   NVL(POL.CANCEL_FLAG , 'N')   = 'N' 
            AND   POH.PO_HEADER_ID   = POH3.PO_HEADER_ID 
            AND   TRUNC(POH.APPROVED_DATE ) = TRUNC(POH3.APPROVED_DATE) ;
            
            
        Cursor Get_PO_RCV_Price (P_VENDOR_ID          NUMBER, 
                             P_VENDOR_SITE_ID     NUMBER , 
                             P_INVENTORY_ITEM_ID  NUMBER ) Is 
            Select pol.LIST_PRICE_PER_UNIT , pol.UNIT_PRICE , pol.UNIT_MEAS_LOOKUP_CODE
            From PO_HEADERS_V poh, 
                 apps.po_lines   pol, 
                 ( Select POH2.PO_Header_ID,  Max(POH2.APPROVED_DATE)  APPROVED_DATE 
                   From PO_HEADERS_V poh2, 
                        apps.po_lines   pol2
                   Where 1 = 1 
                   -- poh2.type_Lookup_code     = 'BLANKET'
                   AND   poh2.AUTHORIZATION_STATUS = 'APPROVED' 
                   And   poh2.APPROVED_FLAG        = 'Y' 
                   And   poh2.Vendor_ID            = P_VENDOR_ID 
                   And   poh2.Vendor_Site_ID       = P_VENDOR_SITE_ID 
                   ANd   poh2.PO_Header_ID         = pol2.PO_Header_ID 
                   ANd   pol2.Item_ID              = P_INVENTORY_ITEM_ID 
                   AND   NVL(POL2.CANCEL_FLAG , 'N')   = 'N' 
                   AND   EXISTS ( SELECT 1
                        FROM RCV_SHIPMENT_LINES  rsl, 
                             RCV_SHIPMENT_HEADERS rsh
                        WHERE Rsh.Shipment_Header_ID = rsl.Shipment_Line_ID 
                        And rsh.RECEIPT_SOURCE_CODE  = 'VENDOR' 
                        AND RSL.SHIPMENT_LINE_STATUS_CODE In ( 'PARTIALLY RECEIVED', 'FULLY RECEIVED') 
                        AND RSL.SOURCE_DOCUMENT_CODE = 'PO'
                        AND RSL.PO_HEADER_ID         = pol2.PO_HEADER_ID 
                        AND RSL.PO_LINE_ID           = pol2.PO_LINE_ID    ) 
                   GROUP BY POH2.PO_HEADER_ID) poh3
            Where 1 =1 
            --poh.type_Lookup_code     = 'BLANKET'
            AND   poh.AUTHORIZATION_STATUS = 'APPROVED' 
            And   poh.APPROVED_FLAG        = 'Y' 
            And   poh.Vendor_ID            = P_VENDOR_ID 
            And   poh.Vendor_Site_ID       = P_VENDOR_SITE_ID 
            ANd   poh.PO_Header_ID         = pol.PO_Header_ID 
            ANd   pol.Item_ID              = P_INVENTORY_ITEM_ID 
            AND   NVL(POL.CANCEL_FLAG , 'N')   = 'N' 
            AND   POH.PO_HEADER_ID   = POH3.PO_HEADER_ID 
            AND   TRUNC(POH.APPROVED_DATE ) = TRUNC(POH3.APPROVED_DATE) ;
            
            Cursor Get_PO_Price (P_VENDOR_ID          NUMBER, 
                              P_VENDOR_SITE_ID     NUMBER , 
                              P_INVENTORY_ITEM_ID  NUMBER ) Is 
            Select pol.LIST_PRICE_PER_UNIT , pol.UNIT_PRICE , pol.UNIT_MEAS_LOOKUP_CODE
            From PO_HEADERS_V poh, 
                 apps.po_lines   pol, 
                 ( Select POH2.PO_Header_ID,  Max(POH2.APPROVED_DATE)  APPROVED_DATE 
                   From PO_HEADERS_V poh2, 
                        apps.po_lines   pol2
                   Where 1 =1 
                   --poh2.type_Lookup_code     = 'BLANKET'
                   AND   poh2.AUTHORIZATION_STATUS = 'APPROVED' 
                   And   poh2.APPROVED_FLAG        = 'Y' 
                   And   poh2.Vendor_ID            = P_VENDOR_ID 
                   And   poh2.Vendor_Site_ID       = P_VENDOR_SITE_ID 
                   ANd   poh2.PO_Header_ID         = pol2.PO_Header_ID 
                   ANd   pol2.Item_ID              = P_INVENTORY_ITEM_ID 
                   AND   NVL(POL2.CANCEL_FLAG , 'N')   = 'N' 
                   GROUP BY POH2.PO_HEADER_ID) poh3
            Where 1 =1 
            --poh.type_Lookup_code     = 'BLANKET'
            AND   poh.AUTHORIZATION_STATUS = 'APPROVED' 
            And   poh.APPROVED_FLAG        = 'Y' 
            And   poh.Vendor_ID            = P_VENDOR_ID 
            And   poh.Vendor_Site_ID       = P_VENDOR_SITE_ID 
            ANd   poh.PO_Header_ID         = pol.PO_Header_ID 
            ANd   pol.Item_ID              = P_INVENTORY_ITEM_ID 
            AND   NVL(POL.CANCEL_FLAG , 'N')   = 'N' 
            AND   POH.PO_HEADER_ID   = POH3.PO_HEADER_ID 
            AND   TRUNC(POH.APPROVED_DATE ) = TRUNC(POH3.APPROVED_DATE) ;
            
        l_org_id            Number ; 
        l_Vendor_Cost       Number(15,6) := 0;     
        l_Module_Name       VARCHAR2(80) := 'XXWC_OZF_VENDOR_QUOTE_PKG.GET_VENDOR_ITEM_COST';
        
        Item_Cost_Found_Exp   Exception ; 
    Begin
       XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => G_LEVEL_PROCEDURE,
                                    p_mod_name      => l_Module_Name,
                                    P_DEBUG_MSG     => '1000 Begning of Main API :' );
       
       -- Begining of API 
       For Vendor_Rec  In Get_Vendor_Cur  Loop 
           XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => G_LEVEL_PROCEDURE,
                                    p_mod_name      => l_Module_Name,
                                    P_DEBUG_MSG     => '1010 :  Inside Vendor Cursor :'  );
           
           -- Check if there is any BPA Document exists for the Vendor 
           FOR BPA_Rec In  Get_BPA_Price (P_VENDOR_ID         => Vendor_Rec.SUPPLIER_ID, 
                                          P_VENDOR_SITE_ID    => Vendor_Rec.SUPPLIER_SITE_ID , 
                                          P_INVENTORY_ITEM_ID => p_Inventory_Item_ID )  LOOP 
               XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => G_LEVEL_PROCEDURE,
                                    p_mod_name      => l_Module_Name,
                                    P_DEBUG_MSG     => '1020 :  Inside BPA Cursor :'  );
               
               If l_Vendor_Cost  = 0 Then 
                  l_Vendor_Cost := BPA_REC.UNIT_PRICE ; 
               End If; 
               
               -- IF there are more then one Record get the least Unit _Price 
               If l_Vendor_Cost < BPA_REC.UNIT_PRICE  Then 
                  l_Vendor_Cost := BPA_REC.UNIT_PRICE ;  
               End If; 
           End Loop ; 
           -- Check if BPA Cursor has found any Records, If So then Return V
           If L_Vendor_Cost > 0 Then 
              Raise Item_Cost_Found_Exp ;
              --Return L_Vendor_Cost ; 
           ENd IF; 
           XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => G_LEVEL_PROCEDURE,
                                    p_mod_name      => l_Module_Name,
                                    P_DEBUG_MSG     => '1031 :  Vendor ID :' || Vendor_Rec.SUPPLIER_ID );
                                    
           XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => G_LEVEL_PROCEDURE,
                                    p_mod_name      => l_Module_Name,
                                    P_DEBUG_MSG     => '1032 :  Vendor ID :' || Vendor_Rec.SUPPLIER_SITE_ID );
                                    
           ---- Since BPA Price is not Found, Check  Unit_Price on PO_Lines that is most recently received. 
           FOR PO_RCV_Rec In  Get_PO_RCV_Price (P_VENDOR_ID         => Vendor_Rec.SUPPLIER_ID, 
                                          P_VENDOR_SITE_ID    => Vendor_Rec.SUPPLIER_SITE_ID , 
                                          P_INVENTORY_ITEM_ID => p_Inventory_Item_ID )  LOOP 
                                          
               XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => G_LEVEL_PROCEDURE,
                                    p_mod_name      => l_Module_Name,
                                    P_DEBUG_MSG     => '1040 :  Inside PO RCV Cursor :'  );
               
               If l_Vendor_Cost  = 0 Then 
                  l_Vendor_Cost := PO_RCV_Rec.UNIT_PRICE ; 
               End If; 
               
               -- IF there are more then one Record get the least Unit _Price 
               If l_Vendor_Cost < PO_RCV_Rec.UNIT_PRICE  Then 
                  l_Vendor_Cost := PO_RCV_Rec.UNIT_PRICE ;  
               End If; 
           End Loop ; 
           
           -- Check if BPA Cursor has found any Records, If So then Return V
           If L_Vendor_Cost > 0 Then 
              Raise Item_Cost_Found_Exp ;
              --Return L_Vendor_Cost ; 
           ENd IF; 
           -- Check if There is Any Approved PO for the Vendor 
           ---- Since BPA Price is not Found, Check  Unit_Price on PO_Lines that is most recently received. 
           FOR PO_Rec In  Get_PO_Price (P_VENDOR_ID         => Vendor_Rec.SUPPLIER_ID, 
                                          P_VENDOR_SITE_ID    => Vendor_Rec.SUPPLIER_SITE_ID , 
                                          P_INVENTORY_ITEM_ID => p_Inventory_Item_ID )  LOOP 
                                          
               XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => G_LEVEL_PROCEDURE,
                                    p_mod_name      => l_Module_Name,
                                    P_DEBUG_MSG     => '1050 :  Inside PO REC Cursor :' );
               
               If l_Vendor_Cost  = 0 Then 
                  l_Vendor_Cost := PO_Rec.UNIT_PRICE ; 
               End If; 
               
               -- IF there are more then one Record get the least Unit _Price 
               If l_Vendor_Cost < PO_Rec.UNIT_PRICE  Then 
                  l_Vendor_Cost := PO_Rec.UNIT_PRICE ;  
               End If; 
           End Loop ; 
           
           -- Check if BPA Cursor has found any Records, If So then Return V
           If L_Vendor_Cost > 0 Then 
              Raise Item_Cost_Found_Exp ;
              --Return L_Vendor_Cost ; 
           ENd IF; 
           XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => G_LEVEL_PROCEDURE,
                                    p_mod_name      => l_Module_Name,
                                    P_DEBUG_MSG     => '1060 :  Inside PO REC Cursor :' );
                                    
           
           Select LIST_PRICE_PER_UNIT
           INto  L_Vendor_Cost
           From Mtl_System_Items_b 
           Where Inventory_Item_ID = p_Inventory_Item_ID 
           and Organization_ID     = Fnd_Profile.Value('XXWC_ITEM_MASTER_ORG') ;
           
           Raise Item_Cost_Found_Exp ; 
           --Return L_Vendor_Cost ;
           
       ENd Loop; 
       
       If l_Vendor_Cost = 0 Then 
          Return NULL; 
       End If; 
    Exception 
       When Item_Cost_Found_Exp Then 
           /***************************
           Update apps.Oe_Order_Lines
              Set Attribute6 = l_Vendor_Cost 
           Where Line_ID = P_SO_Line_ID ; 
            ****************************/
           Return 'Y' ;
           
       When Others Then 
          XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => G_LEVEL_PROCEDURE,
                                    p_mod_name      => l_Module_Name,
                                    P_DEBUG_MSG     => '1060 :  When Others Error  :' || SQLERRM );
          
          Return 'Y' ;
           
    End ; 
                                        
 


   ---*********************************************************************************************************
   -- Function  get_item_cost
   -- Satish U: 15-MAR-2012 : T-- Following API is initially copied from OZF_QP_QUAL_PVT.Get_Item_Cost 
   -- THis API is modifier to get Item Cost in WC way. 
   --*********************************************************************************************************

FUNCTION get_item_cost
(
 p_line_id NUMBER, p_resale_line_tbl ozf_order_price_pvt.resale_line_tbl_type -- OZF_ORDER_PRICE_PVT.G_RESALE_LINE_TBL
)
RETURN VARCHAR2
IS
  l_sdr_flag VARCHAR2(1) := 'Y';
  l_cost_price NUMBER;
  l_primary_uom_code VARCHAR2(3);
  l_order_uom_code  VARCHAR2(3);
  l_Uom_rate NUMBER;
  l_cost_price_currency  VARCHAR2(15);
  l_order_currency  VARCHAR2(15);
  l_conversion_type_code  VARCHAR2(30);
  l_conversion_rate  NUMBER;
  l_denominator  NUMBER;
  l_numerator  NUMBER;
  l_rate  NUMBER;
  l_inventory_item_id NUMBER;
  l_organization_id NUMBER;
  l_supplier_item_cost NUMBER;
  l_call_costing_api BOOLEAN := false;
  l_table_name varchar2(30);
  l_col_name varchar2(30);
  l_party_id NUMBER;
  l_idsm_ord_src_party_id  VARCHAR2(30);
  l_column_name  VARCHAR2(30);
  l_exp_qry_str VARCHAR2(500);
  l_qry_str VARCHAR2(500);
  l_uom_code VARCHAR2(30);
  l_inv_org_id NUMBER;
  x_msg_count NUMBER;
  x_msg_data VARCHAR2(2000);
  l_api_name VARCHAR2(30) := 'get_item_cost';
  l_resale_table_type VARCHAR2(10);
  l_direct_order_flag VARCHAR2(1);
  l_Module_Name       Varchar2(80) := 'XXWC_OZF_VENDOR_QUOTE_PKG.GET_ITEM_COST';
  -- Satish U: 29-MAR-2012 
  l_Get_WC_Vendor_Item_Cost   Boolean := FALSE; 

  CURSOR c_order_info(p_order_line_id IN NUMBER) IS
   SELECT line.inventory_item_id, line.org_id,
               line.order_quantity_uom, head.transactional_curr_code,
               head.conversion_type_code, line.ship_from_org_id
    FROM apps.oe_order_lines line, apps.oe_order_headers head
    WHERE line.line_id = p_order_line_id
    AND line.header_id = head.header_id;

  CURSOR c_resale_line_info(p_resale_line_id IN NUMBER) IS
   SELECT inventory_item_id,org_id, supplier_item_cost,
               nvl(supplier_item_uom,uom_code), currency_code
   FROM apps.ozf_resale_lines
   WHERE resale_line_id = p_resale_line_id;

  CURSOR c_iface_line_info(p_resale_line_int_id IN NUMBER) IS
   SELECT inventory_item_id,org_id, supplier_item_cost,
                nvl(supplier_item_uom,uom_code), currency_code
   FROM apps.ozf_resale_lines_int
   WHERE resale_line_int_id = p_resale_line_int_id;

  CURSOR c_iface_batch_info(p_resale_line_int_id IN NUMBER) IS
   SELECT orb.direct_order_flag
   FROM apps.ozf_resale_lines_int orli, apps.ozf_resale_batches orb
   WHERE orli.resale_line_int_id = p_resale_line_int_id
   AND orb.resale_batch_id = orli.resale_batch_id;

  CURSOR c_resale_batch_info(p_resale_line_id IN NUMBER) IS
   SELECT orb.direct_order_flag
   FROM  apps.ozf_resale_batches orb,
             apps.ozf_resale_batch_line_maps orblm
   WHERE orblm.resale_line_id = p_resale_line_id
   AND orblm.resale_batch_id = orb.resale_batch_id;

CURSOR c_exchange_rate_type_csr(p_org_id NUMBER)IS
  SELECT exchange_rate_type
  FROM apps.ozf_sys_parameters
  WHERE org_id=p_org_id;

CURSOR c_primary_uom_csr(p_inventory_item_id NUMBER, p_inv_org_id NUMBER)IS
  SELECT primary_uom_code
  FROM mtl_system_items
  WHERE inventory_item_id = p_inventory_item_id
  AND organization_id = p_inv_org_id;


CURSOR c_cost_price_curr_csr(p_organization_id NUMBER)IS
  SELECT ogs.currency_code
  FROM   org_organization_definitions ood, oe_gl_sets_of_books_v ogs
  WHERE  ood.organization_id =  p_organization_id
  AND  ood.set_of_books_id = ogs.set_of_books_id;


BEGIN

  XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => G_LEVEL_PROCEDURE,
                                p_mod_name      => l_Module_Name,
                                P_DEBUG_MSG     => '2000 :  Begining of API Get Item Cost is  :' );
/*
1)  For EBS OM orders
     1.1) get cost price
     1.2) Store cost price in oe_order_lines_all table. The column will be retreived from profile 'OZF: Store Item Cost'
     1.3) Shouldn't we have a default value for this profile?
     1.4) If any excpetion happens store -1 in this column

2) If resale_table_type is IFACE or RESALE then compare profile 'OZF: Use Indirect Sales as Order Source for Customer'
    with disti of the batch.

    2.1) If both are same then check get the value of column 'Supplier Item Cost'.
    If not provided during batch upload then get using costing API and store in this column.

    2.2) If both are diff then get using costing API and store in column Supplier Item Cost.

3) If its is iface store in ozf_resale_lines_int_all, if its resale store in ozf_resale_lines_all.

4) If exception happens or costing API doesn't return anything then store -1 in respective columns.
*/

IF p_resale_line_tbl.COUNT > 0 THEN
    l_resale_table_type := p_resale_line_tbl(1).resale_table_type;
END IF;


IF Fnd_Msg_Pub.Check_Msg_Level ( Fnd_Msg_Pub.G_MSG_LVL_DEBUG_LOW ) THEN
              Fnd_Msg_Pub.Add_Exc_Msg( 'l_resale_table_type', l_resale_table_type);
              Fnd_Msg_Pub.Add_Exc_Msg( 'p_line_id', p_line_id);
END IF;


XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => G_LEVEL_PROCEDURE,
                                p_mod_name      => l_Module_Name,
                                P_DEBUG_MSG     => '2010 :  Line_ID   :' || p_line_id);

CASE
    WHEN l_resale_table_type = 'RESALE' THEN
        XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => G_LEVEL_PROCEDURE,
                                p_mod_name      => l_Module_Name,
                                P_DEBUG_MSG     => '2020 :  Resale Table Type  :' );
                                
        l_qry_str := 'UPDATE ozf_resale_lines_all SET supplier_item_cost = :1 WHERE resale_line_id = :2';

        --derive resale order information like organization_id, item_id, uom etc.

        OPEN c_resale_line_info(p_line_id);
        FETCH c_resale_line_info INTO l_inventory_item_id, l_organization_id, l_supplier_item_cost,
                                                        l_uom_code, l_order_currency;
        CLOSE c_resale_line_info;

         --Based on the resale batch's OU derive exchange_rate_type from System Parameter's page.
        
        IF Fnd_Msg_Pub.Check_Msg_Level ( Fnd_Msg_Pub.G_MSG_LVL_DEBUG_LOW ) THEN
              Fnd_Msg_Pub.Add_Exc_Msg( 'l_organization_id', l_organization_id);
              Fnd_Msg_Pub.Add_Exc_Msg( 'l_supplier_item_cost', l_supplier_item_cost);
        END IF;
        
        XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => G_LEVEL_PROCEDURE,
                                p_mod_name      => l_Module_Name,
                                P_DEBUG_MSG     => '2030 :  Supplier Item Cost   :' || l_supplier_item_cost);

        OPEN c_exchange_rate_type_csr(l_organization_id);
        FETCH c_exchange_rate_type_csr INTO l_conversion_type_code;
        CLOSE c_exchange_rate_type_csr;

        /*Derive the value of direct_order_flag from ozf_resale_batches_all table
        IF yes then it's a direct order hence the purchase price should be provided
        during web-adi upload else set flag l_call_costing_api to true, so that
        purchase price can be derived later using costing API.*/

        OPEN c_resale_batch_info(p_line_id);
        FETCH c_resale_batch_info INTO l_direct_order_flag;
        CLOSE c_resale_batch_info;

        IF l_direct_order_flag = 'Y' THEN
                l_cost_price := l_supplier_item_cost;
        ELSE
                l_call_costing_api := true;
        END IF;

        l_inv_org_id := FND_PROFILE.value('AMS_ITEM_ORGANIZATION_ID');

    WHEN l_resale_table_type = 'IFACE' THEN

        /*If the pricing call is made for a IFACE order through conc. prog
        Third Party Accrual from Interface Table, then this code will be executed. */

        l_qry_str := 'UPDATE ozf_resale_lines_int_all SET supplier_item_cost = :1 WHERE resale_line_int_id = :2';

        --derive interface order information like organization_id, item_id, uom etc.
        XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => G_LEVEL_PROCEDURE,
                                p_mod_name      => l_Module_Name,
                                P_DEBUG_MSG     => '2040 :  Table Type IFACE   :' );

        OPEN c_iface_line_info(p_line_id);
        FETCH c_iface_line_info INTO l_inventory_item_id, l_organization_id, l_supplier_item_cost,
                                                      l_uom_code, l_order_currency;
        CLOSE c_iface_line_info;

        IF Fnd_Msg_Pub.Check_Msg_Level ( Fnd_Msg_Pub.G_MSG_LVL_DEBUG_LOW ) THEN
              Fnd_Msg_Pub.Add_Exc_Msg( 'l_organization_id', l_organization_id);
        END IF;

        IF Fnd_Msg_Pub.Check_Msg_Level ( Fnd_Msg_Pub.G_MSG_LVL_DEBUG_LOW ) THEN
              Fnd_Msg_Pub.Add_Exc_Msg( 'l_organization_id', l_organization_id);
              Fnd_Msg_Pub.Add_Exc_Msg( 'l_supplier_item_cost', l_supplier_item_cost);
        END IF;
        
         XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => G_LEVEL_PROCEDURE,
                                p_mod_name      => l_Module_Name,
                                P_DEBUG_MSG     => '2050 :  Supplier Item Cost   :' || l_supplier_item_cost);

        OPEN c_exchange_rate_type_csr(l_organization_id);
        FETCH c_exchange_rate_type_csr INTO l_conversion_type_code;
        CLOSE c_exchange_rate_type_csr;

        OPEN c_iface_batch_info(p_line_id);
        FETCH c_iface_batch_info INTO l_direct_order_flag;
        CLOSE c_iface_batch_info;

        IF l_direct_order_flag = 'Y' THEN
                l_cost_price := l_supplier_item_cost;
        ELSE
                l_call_costing_api := true;
        END IF;

         l_inv_org_id := FND_PROFILE.value('AMS_ITEM_ORGANIZATION_ID');

    ELSE
         /*If the pricing call is made for direct OM order then this code will be executed. */
         XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => G_LEVEL_PROCEDURE,
                                p_mod_name      => l_Module_Name,
                                P_DEBUG_MSG     => '2060 :  Pricing Call is made from OM   :' );
                                
         OPEN c_order_info(p_line_id);
         FETCH c_order_info INTO l_inventory_item_id, l_organization_id,
                                              l_uom_code, l_order_currency,
                                              l_conversion_type_code, l_inv_org_id;
         CLOSE c_order_info;

         IF Fnd_Msg_Pub.Check_Msg_Level ( Fnd_Msg_Pub.G_MSG_LVL_DEBUG_LOW ) THEN
              Fnd_Msg_Pub.Add_Exc_Msg( 'l_organization_id', l_organization_id);
         END IF;

         MO_GLOBAL.set_policy_context('S', l_organization_id);

         l_column_name := FND_PROFILE.value('OZF_ITEM_COST_COLUMN');
         
         XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => G_LEVEL_PROCEDURE,
                                p_mod_name      => l_Module_Name,
                                P_DEBUG_MSG     => '2070 : Column Name is    :' || l_column_name);

         IF Fnd_Msg_Pub.Check_Msg_Level ( Fnd_Msg_Pub.G_MSG_LVL_DEBUG_LOW ) THEN
              Fnd_Msg_Pub.Add_Exc_Msg( 'l_column_name', l_column_name);
         END IF;

                /*FOR OM orders we have a new seeded profile at site and org level
                OZF: Store Item Cost. This profile will store the name of DFF column from
                oe_order_lines_all where the calculated purchase price will be stored.
                If profile is not set then raise FND warning*/

         IF  l_column_name IS NOT NULL THEN
                 l_qry_str := 'UPDATE oe_order_lines_all SET '|| l_column_name ||'  = :1 WHERE line_id = :2';
                 l_call_costing_api := true;
                 -- Satish U: 29-MAR-2012 
                 l_Get_WC_Vendor_Item_Cost := TRUE; 
         ELSE
              IF FND_MSG_PUB.check_msg_level(FND_MSG_PUB.g_msg_lvl_debug_low) THEN
                       FND_MESSAGE.set_name('OZF', 'OZF_PROFILE_MISS_WRG');
                       FND_MESSAGE.Set_Token('TEXT','Profile OZF: Store Item Cost is NULL.');
                       FND_MSG_PUB.add;
                    END IF;
         END IF;

END CASE;

        /*If the processing is happening for resale/iface batch which is either indirect
        OR it is a direct batch but supplier_item_cost was not provided during POS data upload
        OR it is an EBS OM order then call costing API.*/

IF Fnd_Msg_Pub.Check_Msg_Level ( Fnd_Msg_Pub.G_MSG_LVL_DEBUG_LOW ) THEN
              Fnd_Msg_Pub.Add_Exc_Msg( 'l_inv_org_id', l_inv_org_id);
END IF;

IF ((l_call_costing_api OR l_cost_price IS NULL OR l_cost_price = FND_API.G_MISS_NUM)
                  AND (l_inv_org_id IS NOT NULL AND l_inv_org_id <> FND_API.G_MISS_NUM )) THEN
                  
    If  l_Get_WC_Vendor_Item_Cost Then 
       XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => G_LEVEL_PROCEDURE,
                                p_mod_name      => l_Module_Name,
                                P_DEBUG_MSG     => '2080 : Call WC Vendor Item Cost    :' );
                                
       l_Cost_Price := XXWC_OZF_VENDOR_QUOTE_PKG.Get_Vendor_Item_Cost(
          p_Inventory_Item_ID =>  l_inventory_item_id, 
          P_SO_Line_ID        => p_line_id , 
          P_Organization_ID   => l_inv_org_id  );
         
       XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => G_LEVEL_PROCEDURE,
                                p_mod_name      => l_Module_Name,
                                P_DEBUG_MSG     => '2081 : Vendor Item Cost    :' || l_Cost_Price);
          
    Else
       l_cost_price := cst_cost_api.get_item_cost
                           (p_api_version       => 1
                           ,p_inventory_item_id => l_inventory_item_id
                           ,p_organization_id   => l_inv_org_id
                           );
       XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => G_LEVEL_PROCEDURE,
                                p_mod_name      => l_Module_Name,
                                P_DEBUG_MSG     => '2090 : Standard Get Item COst API is Called    :' );
    End If; 

    IF Fnd_Msg_Pub.Check_Msg_Level ( Fnd_Msg_Pub.G_MSG_LVL_DEBUG_LOW ) THEN
              Fnd_Msg_Pub.Add_Exc_Msg( 'l_cost_price', l_cost_price);
    END IF;

    OPEN c_primary_uom_csr(l_inventory_item_id,l_inv_org_id);
    FETCH c_primary_uom_csr INTO l_primary_uom_code;
    CLOSE c_primary_uom_csr;


    IF Fnd_Msg_Pub.Check_Msg_Level ( Fnd_Msg_Pub.G_MSG_LVL_DEBUG_LOW ) THEN
              Fnd_Msg_Pub.Add_Exc_Msg( 'l_order_uom_code', l_order_uom_code);
              Fnd_Msg_Pub.Add_Exc_Msg( 'l_primary_uom_code', l_primary_uom_code);
    END IF;

   l_order_uom_code := l_uom_code;

   IF ((l_primary_uom_code IS NOT NULL AND l_primary_uom_code <> FND_API.G_MISS_CHAR)
             AND (l_primary_uom_code <> l_order_uom_code AND l_cost_price is not null
             AND l_cost_price <> fnd_api.g_miss_num )) THEN
       INV_CONVERT.INV_UM_CONVERSION(From_Unit => l_order_uom_code
                                                       ,To_Unit   => l_primary_uom_code
                                                       ,Item_ID   => l_inventory_item_id
                                                       ,Uom_Rate  => l_uom_rate);

        --Cost Price in Order Line UOM Ledger Currency
        l_cost_price := l_cost_price * l_Uom_rate;
        XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => G_LEVEL_PROCEDURE,
                                p_mod_name      => l_Module_Name,
                                P_DEBUG_MSG     => '2100 : Item Cost    :' || l_cost_price );
   END IF;

   IF Fnd_Msg_Pub.Check_Msg_Level ( Fnd_Msg_Pub.G_MSG_LVL_DEBUG_LOW ) THEN
              Fnd_Msg_Pub.Add_Exc_Msg( 'l_cost_price', l_cost_price);
   END IF;

   OPEN c_cost_price_curr_csr(l_organization_id);
   FETCH c_cost_price_curr_csr INTO l_cost_price_currency;
   CLOSE c_cost_price_curr_csr;


   IF Fnd_Msg_Pub.Check_Msg_Level ( Fnd_Msg_Pub.G_MSG_LVL_DEBUG_LOW ) THEN
              Fnd_Msg_Pub.Add_Exc_Msg( 'l_cost_price_currency', l_cost_price_currency);
              Fnd_Msg_Pub.Add_Exc_Msg( 'l_order_currency', l_order_currency);
   END IF;

   IF l_cost_price_currency IS NOT NULL AND l_cost_price_currency <> FND_API.G_MISS_CHAR
    AND l_cost_price_currency <> l_order_currency THEN

    /* If cost price currency is not equal to order currency (which is batch currency for resale orders.)
       then convert cost_price to order currency. As already explained the exchange_rate_type is
       derived from TM System Paramters for order's OU.
       If conversion rate type is not found then we will raise a warning in debugs*/


       IF l_conversion_type_code IS NULL OR l_conversion_type_code = FND_API.G_MISS_CHAR  THEN
           --Conversion type not entered in sales order header, unable to perform cost conversion
           IF FND_MSG_PUB.check_msg_level(FND_MSG_PUB.g_msg_lvl_debug_low) THEN
                       FND_MESSAGE.set_name('OZF', 'OZF_INS_RESALE_LOG_WRG');
                       FND_MESSAGE.Set_Token('TEXT','Conversion rate type is not defined for OU.'|| l_organization_id);
                       FND_MSG_PUB.add;
            END IF;
           RETURN NULL;
       END IF;

       /* USER Conversion Type is not supported.
       IF l_conversion_type_code = 'User' AND l_conversion_rate IS NULL THEN
           RETURN NULL;
       END IF;   */

       gl_currency_api.convert_amount
          (  x_from_currency    =>  l_cost_price_currency      ,
              x_to_currency      =>  l_order_currency      ,
              x_conversion_date  =>  SYSDATE      ,
              x_conversion_type  =>  l_conversion_type_code      ,
              x_amount           =>  l_cost_price      ,
              x_converted_amount =>  l_cost_price      ,
              x_denominator      =>  l_denominator      ,
              x_numerator        =>  l_numerator      ,
              x_rate             =>  l_rate      );

    END IF; --IF l_cost_price_currency <> l_order_currency THEN
END IF;


-- Satish U: 29-MAR-2012  : p_Line_ID is Not Null 

IF l_qry_str  IS NOT NULL AND l_cost_price IS NOT NULL AND l_cost_price <> fnd_api.g_miss_num  Then 
  --AND p_Line_ID is Not NULL THEN
     XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => G_LEVEL_PROCEDURE,
                                p_mod_name      => l_Module_Name,
                                P_DEBUG_MSG     => '2120 : Item Cost Update on SO line   :' || l_cost_price );
                                
    XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => G_LEVEL_PROCEDURE,
                                p_mod_name      => l_Module_Name,
                                P_DEBUG_MSG     => '2130 : Update String   :' || l_qry_str );
     XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => G_LEVEL_PROCEDURE,
                                p_mod_name      => l_Module_Name,
                                P_DEBUG_MSG     => '2140 : Update String  Line ID  :' || p_line_id );
                                
     EXECUTE IMMEDIATE l_qry_str USING l_cost_price, p_line_id;
END IF;

RETURN l_sdr_flag;

EXCEPTION
  WHEN Fnd_Api.G_EXC_ERROR THEN
    Fnd_Msg_Pub.Count_AND_Get
         ( p_count      =>      x_msg_count,
           p_data       =>      x_msg_data,
           p_encoded    =>      Fnd_Api.G_FALSE
          );
     XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => G_LEVEL_PROCEDURE,
                                p_mod_name      => l_Module_Name,
                                P_DEBUG_MSG     => '2130 : G_EXC_ERROR   :' );
     EXECUTE IMMEDIATE l_qry_str USING -1, p_line_id;
     RETURN l_sdr_flag;
  WHEN Fnd_Api.G_EXC_UNEXPECTED_ERROR THEN
    Fnd_Msg_Pub.Count_AND_Get
         ( p_count      =>      x_msg_count,
           p_data       =>      x_msg_data,
           p_encoded    =>      Fnd_Api.G_FALSE
          );
     XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => G_LEVEL_PROCEDURE,
                                p_mod_name      => l_Module_Name,
                                P_DEBUG_MSG     => '2140 : G_EXC_UNEXPECTED_ERROR   :' );
     EXECUTE IMMEDIATE l_qry_str USING -1, p_line_id;
     RETURN l_sdr_flag;
  WHEN OTHERS THEN
    IF Fnd_Msg_Pub.Check_Msg_Level ( Fnd_Msg_Pub.G_MSG_LVL_UNEXP_ERROR ) THEN
      Fnd_Msg_Pub.Add_Exc_Msg( G_MODULE_NAME,l_api_name);
    END IF;
    XXWC_GEN_ROUTINES_PKG.LOG_MSG(p_debug_level   => G_LEVEL_PROCEDURE,
                                p_mod_name      => l_Module_Name,
                                P_DEBUG_MSG     => '2150 : WHEN OTHERS ERROR   :' );
    Fnd_Msg_Pub.Count_AND_Get
       ( p_count      =>      x_msg_count,
         p_data       =>      x_msg_data,
         p_encoded    =>      Fnd_Api.G_FALSE
        );
     EXECUTE IMMEDIATE l_qry_str USING -1, p_line_id;
     RETURN l_sdr_flag;

 RETURN l_sdr_flag;
END get_item_cost;

     -- Procedure  To add  Warehouse to Global Temporary Table 
    -- XXWC_PA_WAREHOUSE_GT                              
    Procedure Add_Warehouse(P_Whse_Code In Varchar2 ) Is 
       l_whse_Count  Number ;
       l_Organization_ID  Number ; 
    
       -- Check if warehouse exits on the 
       Begin
          Select Organization_Id 
          Into l_Organization_ID 
          From  MTL_PARAMETERS 
          Where Organization_Code = p_Whse_COde; 
          
                  
          Select Count(*) 
          Into l_Whse_Count 
          from XXWC_PA_WAREHOUSE_GT ; 
          
          -- Validate if Whse IS Valid 
          
          
          If l_whse_Count = 0  Then 
             --Insert Into Global Temp Table 
             Insert Into XXWC_PA_WAREHOUSE_GT  
                  ( ORGANIZATION_ID) 
                  Values (L_Organization_ID ); 
                  
          Else
             -- UPdate Whse ID in Global Temp Table 
             Update XXWC_PA_WAREHOUSE_GT  
                Set ORGANIZATION_ID = L_Organization_ID ; 
              
          End If; 
       Exception 
          When Others THen 
              Null; 
       End Add_Warehouse; 
    
END XXWC_OZF_VENDOR_QUOTE_PKG;
/
show errors; 
commit;
exit;