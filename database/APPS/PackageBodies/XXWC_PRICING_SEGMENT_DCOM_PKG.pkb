CREATE OR REPLACE PACKAGE BODY APPS.XXWC_PRICING_SEGMENT_DCOM_PKG AS
   /*************************************************************************
      $Header XXWC_PRICING_SEGMENT_DCOM_PKG.PKB $
      Module Name: XXWC_PRICING_SEGMENT_DCOM_PKG.PKB

      PURPOSE:   This package is used to decomission Pricing Segmentation process

      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------
      1.0        10/09/2016  Gopi Damuluri           Initial Version TMS# 20160922-00037
   ****************************************************************************/

g_dflt_email   fnd_user.email_address%TYPE    := 'WC-ITDEVALERTS-U1@HDSupply.com';

/****************************************************************************************************
PROCEDURE Name: matrix_dcom

PURPOSE: Main procedure to decomission Pricing Segmentation

REVISIONS:
Ver        Date        Author                     Description
---------  ----------  ---------------         -------------------------
1.0        10/09/2015  Gopi Damuluri           Initial Version
****************************************************************************************************/
PROCEDURE matrix_dcom(p_errbuf              OUT VARCHAR2
                    , p_retcode             OUT NUMBER
                    , p_name                 IN VARCHAR2
                    , p_list_header_id_from  IN  NUMBER
                    , p_list_header_id_to    IN  NUMBER)
       IS

      ----------------------------------------------------------
      -- Matrix PriceList Modifiers
      ----------------------------------------------------------
      CURSOR cur_mtrx
          IS
      SELECT name
           , list_header_id
        FROM qp_list_headers_all qlh 
       WHERE qlh.attribute10 = 'Segmented Price'
         AND (p_name IS NULL OR name like p_name)
         AND list_header_id BETWEEN p_list_header_id_from AND p_list_header_id_to;

      l_sec                   VARCHAR2 (100);

BEGIN

   FOR rec_mtrx IN cur_mtrx LOOP
       --===============================================================================
       -- Delete all qualifiers
       --===============================================================================
       l_sec := 'Delete all qualifiers';
       delete_qualifiers(rec_mtrx.list_header_id);

       --===============================================================================
       -- To delete qualifier Group/Rule definitions.
       --===============================================================================
       l_sec := 'To delete qualifier Group/Rule definitions.';
       delete_qualifier_rules(rec_mtrx.list_header_id);

       --===============================================================================
       -- Delete Excluders from Matrix Modifier
       --===============================================================================
       l_sec := 'Delete Excluders from Matrix Modifier';
       delete_mtrx_excluders(rec_mtrx.list_header_id);

       --===============================================================================
       -- Update Incompatibility_Group of Matrix Modifiers to : Level 1 Incompatibility
       --===============================================================================
       l_sec := 'Update Incompatibility_Group of Matrix Modifiers to : Level 1 Incompatibility';
       upd_mtrx_incomp(rec_mtrx.list_header_id);

       --===============================================================================
       -- Mark Items as exclusions to ItemCategories
       --===============================================================================
       l_sec := 'Mark Items as exclusions to ItemCategories';
       mtrx_item_cat_excl(p_errbuf
                        , p_retcode
                        , rec_mtrx.name);
   END LOOP; -- cur_mtrx

   IF p_name IS NULL THEN
   --===============================================================================
   -- To delete qualifier Group/Rule definitions not associated to Modifiers.
   --===============================================================================
   l_sec := 'To delete qualifier Group/Rule definitions not associated to Modifiers.';
   delete_qualifier_rules(-1);
   END IF;

EXCEPTION
WHEN OTHERS THEN
   fnd_file.put_line (fnd_file.LOG, 'MATRIX_DCOM WHEN OTEHRS ERROR = '||SQLERRM);
   fnd_file.put_line (fnd_file.LOG, l_sec);
   
      xxcus_error_pkg.xxcus_error_main_api (
         p_called_from         => 'xxwc_pricing_segment_dcom_pkg.matrix_dcom',
         p_calling             => l_sec,
         p_request_id          => fnd_global.conc_request_id,
         p_ora_error_msg       => SQLERRM,
         p_error_desc          => 'Error in Pricing Segment Process - matrix_dcom procedure, When Others Exception',
         p_distribution_list   => g_dflt_email,
         p_module              => 'XXWC');

END matrix_dcom;

/*************************************************************************
PROCEDURE Name: delete_qualifiers

PURPOSE: To delete qualifier groups/rules associated with Matrix Modifiers.

REVISIONS:
Ver        Date        Author                     Description
---------  ----------  ---------------         -------------------------
1.0        10/09/2015  Gopi Damuluri           Initial Version
***************************************************************************/
PROCEDURE delete_qualifiers(p_list_header_id IN NUMBER)
       IS

      ----------------------------------------------------------
      -- PriceList Modifier Line - Cursor
      ----------------------------------------------------------
      CURSOR cur_csp_mod_line (p_list_header_id IN NUMBER, p_excluder_Flag IN VARCHAR2)
      IS
      SELECT * 
        FROM qp_modifier_summary_v  
       WHERE list_header_id         = p_list_header_id
         AND NVL(excluder_Flag,'N') = p_excluder_Flag
--         AND TRUNC(SYSDATE) BETWEEN TRUNC(NVL(START_DATE_ACTIVE, SYSDATE -1))  AND TRUNC(NVL(END_DATE_ACTIVE, SYSDATE+ 1))
      ;

      ----------------------------------------------------------
      -- Qualifiers associated with the Matrix Modifier
      ----------------------------------------------------------
      CURSOR cur_qualifiers (p_list_line_id   IN NUMBER)
          IS
      SELECT *
        FROM qp_qualifiers         qq
       WHERE 1 = 1
         AND qq.list_line_id    = p_list_line_id
         AND qq.created_from_rule_id IS NOT NULL
         ;

      i NUMBER;

      --------------------------------------------------------------
      --- Define Local Variables here
      --------------------------------------------------------------
      l_qualifier_rec            QP_QUALIFIER_RULES_PUB.QUALIFIERS_REC_TYPE     := QP_QUALIFIER_RULES_PUB.G_MISS_QUALIFIERS_REC;
      l_modifier_line_Rec        QP_MODIFIERS_PUB.MODIFIERS_REC_TYPE            := QP_MODIFIERS_PUB.G_MISS_MODIFIERS_REC;
      l_pricing_att_rec          QP_MODIFIERS_PUB.PRICING_ATTR_REC_TYPE         := QP_MODIFIERS_PUB.G_MISS_PRICING_ATTR_REC;
      l_error_message_list       ERROR_HANDLER.ERROR_TBL_TYPE;
      l_MODIFIER_LIST_rec        QP_Modifiers_PUB.Modifier_List_Rec_Type;
      l_MODIFIER_LIST_val_rec    QP_Modifiers_PUB.Modifier_List_Val_Rec_Type;
      l_MODIFIERS_tbl            QP_Modifiers_PUB.Modifiers_Tbl_Type;
      l_MODIFIERS_val_tbl        QP_Modifiers_PUB.Modifiers_Val_Tbl_Type;
      l_QUALIFIERS_tbl           QP_Qualifier_Rules_PUB.Qualifiers_Tbl_Type;
      l_QUALIFIERS_val_tbl       QP_Qualifier_Rules_PUB.Qualifiers_Val_Tbl_Type;
      l_PRICING_ATTR_tbl         QP_Modifiers_PUB.Pricing_Attr_Tbl_Type;
      l_PRICING_ATTR_val_tbl     QP_Modifiers_PUB.Pricing_Attr_Val_Tbl_Type;
      lx_MODIFIER_LIST_rec       QP_Modifiers_PUB.Modifier_List_Rec_Type;
      lx_MODIFIER_LIST_val_rec   QP_Modifiers_PUB.Modifier_List_Val_Rec_Type;
      lx_MODIFIERS_tbl           QP_Modifiers_PUB.Modifiers_Tbl_Type;
      lx_MODIFIERS_val_tbl       QP_Modifiers_PUB.Modifiers_Val_Tbl_Type;
      lx_QUALIFIERS_tbl          QP_Qualifier_Rules_PUB.Qualifiers_Tbl_Type;
      lx_QUALIFIERS_val_tbl      QP_Qualifier_Rules_PUB.Qualifiers_Val_Tbl_Type;
      lx_PRICING_ATTR_tbl        QP_Modifiers_PUB.Pricing_Attr_Tbl_Type;
      lx_PRICING_ATTR_val_tbl    QP_Modifiers_PUB.Pricing_Attr_Val_Tbl_Type;
      l_qll_rec                  qp_list_lines%ROWTYPE;
      l_pra_rec                  qp_pricing_attributes%ROWTYPE;
      l_control_rec              QP_GLOBALS.Control_Rec_Type;
      l_mod_name                 VARCHAR2 (100)  := 'XXWC_QP_MODIFIER_WEB_ADI_PKG.IMPORT :' ;
      l_return_status            VARCHAR2(1);

      l_qualifier_rules_rec       qp_qualifier_rules_pub.Qualifier_Rules_Rec_Type  := QP_QUALIFIER_RULES_PUB.G_MISS_QUALIFIER_RULES_REC;
      lx_qualifier_rules_rec      qp_qualifier_rules_pub.Qualifier_Rules_Rec_Type;
  
      l_qualifier_rules_val_rec   qp_qualifier_rules_pub.Qualifier_Rules_Val_Rec_Type;
      lx_qualifier_rules_val_rec  qp_qualifier_rules_pub.Qualifier_Rules_Val_Rec_Type;

      l_Msg_Data                 Varchar2(2000);
      l_Msg_Count                Number ;
      L_List_Header_ID           Number;
      l_List_Line_Id             Number ;
      l_Pricing_Attribute_ID     Number ;
      l_Qualifier_ID             Number ;
--      i                          Number ;
      l_Line_COunt               Number ;
      l_ql_count                 NUMBER;

      C_STATUS_VALIDATE          CONSTANT   VARCHAR2(1) := 'V';
      C_STATUS_ERROR             CONSTANT   VARCHAR2(1) := 'E';
      C_STATUS_PROCESSED         CONSTANT   VARCHAR2(1) := 'P' ;

      l_error_message            VARCHAR2 (2000);
      l_exception                EXCEPTION;

      l_errbuf                   VARCHAR2 (2000);
      l_retcode                  NUMBER;

      l_sec                      VARCHAR2 (100);
      l_ln_count                 NUMBER;
      l_pa_Count                 NUMBER;
      l_prev_excludr_line_id     NUMBER;
      l_prev_customer_id         NUMBER;
      l_qual_grp_num             NUMBER;
      l_qualifier_precedence     NUMBER;
      l_qual_exists              VARCHAR2 (1);
      l_qualifier_rule_id        NUMBER;
      l_addnl_qual               NUMBER;

--      l_process_date             DATE := FND_DATE.CANONICAL_TO_DATE(p_process_date); --TO_DATE(p_process_date,'DD-MON-YYYY'); -- Version# 1.4
      l_list_header_id       NUMBER;
      l_qq_exists                NUMBER; -- Version# 1.8
      --l_qualifier_rule_id        NUMBER;

BEGIN

            --================================================================================================================--
            -- Delete all qualifiers -- Start >>
            --================================================================================================================--
            l_sec := 'Delete all qualifiers -- Start >> ';
            fnd_file.put_line (fnd_file.LOG, l_sec);

               l_error_message := ' ';
               l_MODIFIER_LIST_rec                    := QP_MODIFIERS_PUB.G_MISS_MODIFIER_LIST_REC;
               l_MODIFIERS_Tbl                        := QP_MODIFIERS_PUB.G_MISS_MODIFIERS_TBL;
               l_QUALIFIERS_tbl                       := QP_MODIFIERS_PUB.G_MISS_QUALIFIERS_TBL;
               l_PRICING_ATTR_Tbl                     := QP_MODIFIERS_PUB.G_MISS_PRICING_ATTR_TBL;
               l_ln_count                             := 0;

               l_MODIFIER_LIST_rec.List_Header_ID          := p_list_header_id;
               l_MODIFIER_LIST_rec.operation               := QP_GLOBALS.G_OPR_UPDATE;                 -- G_OPR_CREATE ;

               fnd_file.put_line (fnd_file.LOG, '##### List Header Id ###### '||p_list_header_id);
               
               ----------------------------------------------------------------------------------
               -- Delete all qualifiers
               ----------------------------------------------------------------------------------
               l_ln_count := 0;
               l_ql_Count := 0;

               FOR Mod_Line_Rec IN cur_csp_mod_line (p_list_header_id,'N') LOOP
                   l_ln_count :=  l_ln_count + 1;
                   l_sec := 'Delete all qualifiers';
                   fnd_file.put_line (fnd_file.LOG, l_sec);

                   l_MODIFIERS_tbl(l_Ln_Count).LIST_LINE_ID                := Mod_Line_Rec.LIST_LINE_ID ;
                   --l_MODIFIERS_tbl(l_Ln_Count).LIST_LINE_NO                := NVL(Mod_Line_Rec.LIST_LINE_NO, Mod_Line_Rec.LIST_LINE_ID)  ;
                   l_MODIFIERS_tbl(l_Ln_Count).operation                   := QP_GLOBALS.G_OPR_UPDATE;

                   
                   fnd_file.put_line (fnd_file.LOG, 'Delete Qualifiers Mod_Line_Rec.LIST_LINE_ID - '||Mod_Line_Rec.LIST_LINE_ID);
                   
                   ----------------------------------------------------------------------------------
                   -- Delete Qualifiers > Start
                   ----------------------------------------------------------------------------------
                   FOR rec_qualifiers IN cur_qualifiers(Mod_Line_Rec.LIST_LINE_ID) LOOP

                       l_qualifier_rec                                         := QP_QUALIFIER_RULES_PUB.G_MISS_QUALIFIERS_REC;
                       l_ql_Count                                              := l_ql_Count + 1;

                       l_qualifier_rec.LIST_HEADER_ID                          :=  rec_qualifiers.list_header_id;
                       l_qualifier_rec.LIST_LINE_ID                            :=  rec_qualifiers.list_line_id;
                       l_qualifier_rec.QUALIFIER_ID                            :=  rec_qualifiers.qualifier_id;
                       l_qualifier_rec.QUALIFIER_RULE_ID                       :=  rec_qualifiers.qualifier_rule_id; --?????
                       l_qualifier_rec.Operation                               := QP_GLOBALS.G_OPR_DELETE;

                       l_QUALIFIERS_tbl(l_ql_Count)               := l_qualifier_rec;
                   END LOOP; -- rec_qualifiers
                   ----------------------------------------------------------------------------------
                   -- Delete Qualifiers < End
                   ----------------------------------------------------------------------------------
               END LOOP;        -- Mod_Line_Rec

               fnd_file.put_line (fnd_file.LOG, 'Delete Qualifiers l_Ln_Count - '||l_Ln_Count);
               fnd_file.put_line (fnd_file.LOG, 'Delete Qualifiers l_Ln_Count - '||l_Ln_Count);
               fnd_file.put_line (fnd_file.LOG, 'Delete Qualifiers l_ql_Count - '||l_ql_Count);

               IF l_ql_Count > 0 THEN
               ----------------------------------------------------------------------------------
               -- Add Modifier Lines
               ----------------------------------------------------------------------------------
               QP_Modifiers_PUB.Process_Modifiers
                  ( p_api_version_number    => 1.0
                  , p_init_msg_list         => FND_API.G_FALSE
                  , p_return_values         => FND_API.G_FALSE
                  , p_commit                => FND_API.G_TRUE
                  , x_return_status         => l_return_status
                  , x_msg_count             => l_msg_count
                  , x_msg_data              => l_msg_data
                  , p_MODIFIER_LIST_rec     => l_MODIFIER_LIST_rec
                  , p_MODIFIERS_tbl         => l_MODIFIERS_tbl
                  , p_QUALIFIERS_tbl        => l_QUALIFIERS_tbl
                  , p_PRICING_ATTR_tbl      => l_PRICING_ATTR_tbl
                  , x_MODIFIER_LIST_rec     => lx_MODIFIER_LIST_rec
                  , x_MODIFIER_LIST_val_rec => lx_MODIFIER_LIST_val_rec
                  , x_MODIFIERS_tbl         => lx_MODIFIERS_tbl
                  , x_MODIFIERS_val_tbl     => lx_MODIFIERS_val_tbl
                  , x_QUALIFIERS_tbl        => lx_QUALIFIERS_tbl
                  , x_QUALIFIERS_val_tbl    => lx_QUALIFIERS_val_tbl
                  , x_PRICING_ATTR_tbl      => lx_PRICING_ATTR_tbl
                  , x_PRICING_ATTR_val_tbl  => lx_PRICING_ATTR_val_tbl );

               l_sec := 'Delete all qualifiers ';
               fnd_file.put_line (fnd_file.LOG, l_sec);
               fnd_file.put_line (fnd_file.LOG, 'x_return_status -'||l_return_status);
               fnd_file.put_line (fnd_file.LOG, 'x_msg_count -'||l_msg_count);
               fnd_file.put_line (fnd_file.LOG, 'x_msg_data -'||l_msg_data);
               fnd_file.put_line (fnd_file.LOG, 'x_return_status -'||l_return_status);
               fnd_file.put_line (fnd_file.LOG, 'x_msg_data -'||l_msg_data);
               fnd_file.put_line (fnd_file.LOG, 'x_msg_count -'||l_msg_count);
               
               COMMIT;
               END IF; -- IF l_ql_Count > 0 THEN
            --================================================================================================================--
            -- Delete all qualifiers -- End <<
            --================================================================================================================--

EXCEPTION
WHEN OTHERS THEN
   fnd_file.put_line (fnd_file.LOG, 'MATRIX_DCOM WHEN OTEHRS ERROR = '||SQLERRM);
   fnd_file.put_line (fnd_file.LOG, l_sec);
      xxcus_error_pkg.xxcus_error_main_api (
         p_called_from         => 'xxwc_pricing_segment_dcom_pkg.delete_qualifiers',
         p_calling             => l_sec,
         p_request_id          => fnd_global.conc_request_id,
         p_ora_error_msg       => SQLERRM,
         p_error_desc          => 'Error in Pricing Segment Process - delete_qualifiers procedure, When Others Exception',
         p_distribution_list   => g_dflt_email,
         p_module              => 'XXWC');
END delete_qualifiers;

/*************************************************************************
PROCEDURE Name: delete_qualifier_rules

PURPOSE: To delete qualifier group/rule definitions.

REVISIONS:
Ver        Date        Author                     Description
---------  ----------  ---------------         -------------------------
1.0        10/09/2015  Gopi Damuluri           Initial Version
***************************************************************************/
PROCEDURE delete_qualifier_rules(p_list_header_id IN NUMBER)
       IS


-------------------------------------------------------------------------
-- Delete all Qualifier Rules and their association with Modifiers
-------------------------------------------------------------------------
CURSOR cur_qual_rules
    IS
SELECT *
  FROM qp_qualifier_rules qqr
 WHERE name like 'MATRIX%'
   AND p_list_header_id != -1
   AND EXISTS (SELECT '1'
                 FROM qp_qualifiers qq
                WHERE qq.created_from_rule_id = qqr.qualifier_rule_id
                  AND qq.list_header_id = p_list_header_id)
UNION
SELECT *
  FROM qp_qualifier_rules qqr
 WHERE name like 'MATRIX%'
   AND p_list_header_id = -1;


      i NUMBER;
      --------------------------------------------------------------
      --- Define Local Variables here
      --------------------------------------------------------------
      l_qualifier_rec            QP_QUALIFIER_RULES_PUB.QUALIFIERS_REC_TYPE     := QP_QUALIFIER_RULES_PUB.G_MISS_QUALIFIERS_REC;
      l_modifier_line_Rec        QP_MODIFIERS_PUB.MODIFIERS_REC_TYPE            := QP_MODIFIERS_PUB.G_MISS_MODIFIERS_REC;
      l_pricing_att_rec          QP_MODIFIERS_PUB.PRICING_ATTR_REC_TYPE         := QP_MODIFIERS_PUB.G_MISS_PRICING_ATTR_REC;
      l_error_message_list       ERROR_HANDLER.ERROR_TBL_TYPE;
      l_MODIFIER_LIST_rec        QP_Modifiers_PUB.Modifier_List_Rec_Type;
      l_MODIFIER_LIST_val_rec    QP_Modifiers_PUB.Modifier_List_Val_Rec_Type;
      l_MODIFIERS_tbl            QP_Modifiers_PUB.Modifiers_Tbl_Type;
      l_MODIFIERS_val_tbl        QP_Modifiers_PUB.Modifiers_Val_Tbl_Type;
      l_QUALIFIERS_tbl           QP_Qualifier_Rules_PUB.Qualifiers_Tbl_Type;
      l_QUALIFIERS_val_tbl       QP_Qualifier_Rules_PUB.Qualifiers_Val_Tbl_Type;
      l_PRICING_ATTR_tbl         QP_Modifiers_PUB.Pricing_Attr_Tbl_Type;
      l_PRICING_ATTR_val_tbl     QP_Modifiers_PUB.Pricing_Attr_Val_Tbl_Type;
      lx_MODIFIER_LIST_rec       QP_Modifiers_PUB.Modifier_List_Rec_Type;
      lx_MODIFIER_LIST_val_rec   QP_Modifiers_PUB.Modifier_List_Val_Rec_Type;
      lx_MODIFIERS_tbl           QP_Modifiers_PUB.Modifiers_Tbl_Type;
      lx_MODIFIERS_val_tbl       QP_Modifiers_PUB.Modifiers_Val_Tbl_Type;
      lx_QUALIFIERS_tbl          QP_Qualifier_Rules_PUB.Qualifiers_Tbl_Type;
      lx_QUALIFIERS_val_tbl      QP_Qualifier_Rules_PUB.Qualifiers_Val_Tbl_Type;
      lx_PRICING_ATTR_tbl        QP_Modifiers_PUB.Pricing_Attr_Tbl_Type;
      lx_PRICING_ATTR_val_tbl    QP_Modifiers_PUB.Pricing_Attr_Val_Tbl_Type;
      l_qll_rec                  qp_list_lines%ROWTYPE;
      l_pra_rec                  qp_pricing_attributes%ROWTYPE;
      l_control_rec              QP_GLOBALS.Control_Rec_Type;
      l_mod_name                 VARCHAR2 (100)  := 'XXWC_QP_MODIFIER_WEB_ADI_PKG.IMPORT :' ;
      l_return_status            VARCHAR2(1);

      l_qualifier_rules_rec       qp_qualifier_rules_pub.Qualifier_Rules_Rec_Type  := QP_QUALIFIER_RULES_PUB.G_MISS_QUALIFIER_RULES_REC;
      lx_qualifier_rules_rec      qp_qualifier_rules_pub.Qualifier_Rules_Rec_Type;
  
      l_qualifier_rules_val_rec   qp_qualifier_rules_pub.Qualifier_Rules_Val_Rec_Type;
      lx_qualifier_rules_val_rec  qp_qualifier_rules_pub.Qualifier_Rules_Val_Rec_Type;

      l_Msg_Data                 Varchar2(2000);
      l_Msg_Count                Number ;
      L_List_Header_ID           Number;
      l_List_Line_Id             Number ;
      l_Pricing_Attribute_ID     Number ;
      l_Qualifier_ID             Number ;
--      i                          Number ;
      l_Line_COunt               Number ;
      l_ql_count                 NUMBER;

      C_STATUS_VALIDATE          CONSTANT   VARCHAR2(1) := 'V';
      C_STATUS_ERROR             CONSTANT   VARCHAR2(1) := 'E';
      C_STATUS_PROCESSED         CONSTANT   VARCHAR2(1) := 'P' ;

      l_error_message            VARCHAR2 (2000);
      l_exception                EXCEPTION;

      l_errbuf                   VARCHAR2 (2000);
      l_retcode                  NUMBER;

      l_sec                      VARCHAR2 (100);
      l_ln_count                 NUMBER;
      l_pa_Count                 NUMBER;
      l_prev_excludr_line_id     NUMBER;
      l_prev_customer_id         NUMBER;
      l_qual_grp_num             NUMBER;
      l_qualifier_precedence     NUMBER;
      l_qual_exists              VARCHAR2 (1);
      l_qualifier_rule_id        NUMBER;
      l_addnl_qual               NUMBER;

--      l_process_date             DATE := FND_DATE.CANONICAL_TO_DATE(p_process_date); --TO_DATE(p_process_date,'DD-MON-YYYY'); -- Version# 1.4
      l_list_header_id       NUMBER;
      l_qq_exists                NUMBER; -- Version# 1.8
      --l_qualifier_rule_id        NUMBER;
BEGIN

fnd_file.put_line (fnd_file.LOG, 'p_list_header_id - '||p_list_header_id);

--================================================================================================================--
-- Delete Qualifier Group / Rule > Start
--================================================================================================================--
    i := 0;
    FOR rec_qual_rules IN cur_qual_rules LOOP
        l_qualifier_rules_rec.qualifier_rule_id      := rec_qual_rules.qualifier_rule_id;
        l_qualifier_rules_rec.name                   := rec_qual_rules.name;
        l_qualifier_rules_rec.operation              := QP_GLOBALS.G_OPR_DELETE;
        l_qualifier_rule_id                          := rec_qual_rules.qualifier_rule_id;

        qp_qualifier_rules_pub.Process_Qualifier_Rules(
              p_api_version_number            =>1.0
              ,p_init_msg_list                => fnd_api.G_FALSE
              ,p_return_values                => fnd_api.G_FALSE
              ,p_commit                       => fnd_api.G_FALSE
              ,x_return_status                => l_return_status
              ,x_msg_count                    => l_msg_count
              ,x_msg_data                     => l_msg_data
              ,p_QUALIFIER_RULES_rec          => l_qualifier_rules_rec
              ,p_QUALIFIER_RULES_val_rec      => l_qualifier_rules_val_rec
              ,p_QUALIFIERS_tbl               => l_QUALIFIERS_tbl
              ,p_QUALIFIERS_val_tbl           => l_QUALIFIERS_val_tbl
              ,x_QUALIFIER_RULES_rec          => lx_qualifier_rules_rec
              ,x_QUALIFIER_RULES_val_rec      => lx_qualifier_rules_val_rec
              ,x_QUALIFIERS_tbl               => lx_QUALIFIERS_tbl
              ,x_QUALIFIERS_val_tbl           => lx_QUALIFIERS_val_tbl
          );

        IF l_return_status = fnd_api.G_RET_STS_SUCCESS THEN
           COMMIT;
           fnd_file.put_line (fnd_file.LOG, 'Delete Qualifier Rules Success - '||l_qualifier_rule_id);
        ELSE
           COMMIT;

           fnd_file.put_line (fnd_file.LOG, 'Delete Qualifier Rules  - Failure - '||l_qualifier_rule_id);

           FOR I IN 1 .. l_msg_count LOOP
              l_msg_data := oe_msg_pub.get( p_msg_index => i,p_encoded => 'F');
              fnd_file.put_line (fnd_file.LOG, 'err msg ' || i ||'is:  ' || l_msg_data);
           END LOOP;
        END IF; -- l_return_status = fnd_api.G_RET_STS_SUCCESS THEN
        i := i+1;
    END LOOP;
    COMMIT;

    fnd_file.put_line (fnd_file.LOG, 'l_qualifier_rule_id = '||l_qualifier_rule_id);
    
    fnd_file.put_line (fnd_file.LOG, '# of Qualifier Grouping Rules deleted = '||i);
    fnd_file.put_line (fnd_file.LOG, '##### Delete Qualifier Group / Rule < End ###### ');
--================================================================================================================--
-- Delete Qualifier Group / Rule < End
--================================================================================================================--
EXCEPTION
WHEN OTHERS THEN
   fnd_file.put_line (fnd_file.LOG, 'DELETE_QUALIFIER_RULES WHEN OTEHRS ERROR = '||SQLERRM);
   fnd_file.put_line (fnd_file.LOG, l_sec);
      xxcus_error_pkg.xxcus_error_main_api (
         p_called_from         => 'xxwc_pricing_segment_dcom_pkg.delete_qualifier_rules',
         p_calling             => l_sec,
         p_request_id          => fnd_global.conc_request_id,
         p_ora_error_msg       => SQLERRM,
         p_error_desc          => 'Error in Pricing Segment Process - delete_qualifier_rules procedure, When Others Exception',
         p_distribution_list   => g_dflt_email,
         p_module              => 'XXWC');
END delete_qualifier_rules;

/*************************************************************************
PROCEDURE Name: delete_mtrx_excluders

PURPOSE: Delete Excluders from Matrix Modifier

REVISIONS:
Ver        Date        Author                     Description
---------  ----------  ---------------         -------------------------
1.0        10/09/2015  Gopi Damuluri           Initial Version
***************************************************************************/
PROCEDURE delete_mtrx_excluders(p_list_header_id IN NUMBER)
       IS

      ----------------------------------------------------------
      -- PriceList Modifier Line - Cursor
      ----------------------------------------------------------
      CURSOR cur_csp_mod_line (p_list_header_id IN NUMBER, p_excluder_Flag IN VARCHAR2)
      IS
      SELECT * 
        FROM qp_modifier_summary_v  
       WHERE list_header_id         = p_list_header_id
         AND NVL(excluder_Flag,'N') = p_excluder_Flag
--         AND TRUNC(SYSDATE) BETWEEN TRUNC(NVL(START_DATE_ACTIVE, SYSDATE -1))  AND TRUNC(NVL(END_DATE_ACTIVE, SYSDATE+ 1))
         -- AND ROWNUM < 10
      ;

      ----------------------------------------------------------
      -- PriceList Attribute - Cursor
      ----------------------------------------------------------
      CURSOR Get_Pricing_Attr_Cur (p_list_header_id IN NUMBER, p_list_line_id IN NUMBER, p_excluder_Flag IN VARCHAR2 )
      IS
      SELECT * 
        FROM qp_pricing_attributes 
       WHERE list_header_id = p_list_header_id
         AND list_line_id   = p_list_line_id
         AND NVL(excluder_Flag,'N') = p_excluder_Flag;

      i NUMBER;
      --------------------------------------------------------------
      --- Define Local Variables here
      --------------------------------------------------------------
      l_qualifier_rec            QP_QUALIFIER_RULES_PUB.QUALIFIERS_REC_TYPE     := QP_QUALIFIER_RULES_PUB.G_MISS_QUALIFIERS_REC;
      l_modifier_line_Rec        QP_MODIFIERS_PUB.MODIFIERS_REC_TYPE            := QP_MODIFIERS_PUB.G_MISS_MODIFIERS_REC;
      l_pricing_att_rec          QP_MODIFIERS_PUB.PRICING_ATTR_REC_TYPE         := QP_MODIFIERS_PUB.G_MISS_PRICING_ATTR_REC;
      l_error_message_list       ERROR_HANDLER.ERROR_TBL_TYPE;
      l_MODIFIER_LIST_rec        QP_Modifiers_PUB.Modifier_List_Rec_Type;
      l_MODIFIER_LIST_val_rec    QP_Modifiers_PUB.Modifier_List_Val_Rec_Type;
      l_MODIFIERS_tbl            QP_Modifiers_PUB.Modifiers_Tbl_Type;
      l_MODIFIERS_val_tbl        QP_Modifiers_PUB.Modifiers_Val_Tbl_Type;
      l_QUALIFIERS_tbl           QP_Qualifier_Rules_PUB.Qualifiers_Tbl_Type;
      l_QUALIFIERS_val_tbl       QP_Qualifier_Rules_PUB.Qualifiers_Val_Tbl_Type;
      l_PRICING_ATTR_tbl         QP_Modifiers_PUB.Pricing_Attr_Tbl_Type;
      l_PRICING_ATTR_val_tbl     QP_Modifiers_PUB.Pricing_Attr_Val_Tbl_Type;
      lx_MODIFIER_LIST_rec       QP_Modifiers_PUB.Modifier_List_Rec_Type;
      lx_MODIFIER_LIST_val_rec   QP_Modifiers_PUB.Modifier_List_Val_Rec_Type;
      lx_MODIFIERS_tbl           QP_Modifiers_PUB.Modifiers_Tbl_Type;
      lx_MODIFIERS_val_tbl       QP_Modifiers_PUB.Modifiers_Val_Tbl_Type;
      lx_QUALIFIERS_tbl          QP_Qualifier_Rules_PUB.Qualifiers_Tbl_Type;
      lx_QUALIFIERS_val_tbl      QP_Qualifier_Rules_PUB.Qualifiers_Val_Tbl_Type;
      lx_PRICING_ATTR_tbl        QP_Modifiers_PUB.Pricing_Attr_Tbl_Type;
      lx_PRICING_ATTR_val_tbl    QP_Modifiers_PUB.Pricing_Attr_Val_Tbl_Type;
      l_qll_rec                  qp_list_lines%ROWTYPE;
      l_pra_rec                  qp_pricing_attributes%ROWTYPE;
      l_control_rec              QP_GLOBALS.Control_Rec_Type;
      l_mod_name                 VARCHAR2 (100)  := 'XXWC_QP_MODIFIER_WEB_ADI_PKG.IMPORT :' ;
      l_return_status            VARCHAR2(1);

      l_qualifier_rules_rec       qp_qualifier_rules_pub.Qualifier_Rules_Rec_Type  := QP_QUALIFIER_RULES_PUB.G_MISS_QUALIFIER_RULES_REC;
      lx_qualifier_rules_rec      qp_qualifier_rules_pub.Qualifier_Rules_Rec_Type;
  
      l_qualifier_rules_val_rec   qp_qualifier_rules_pub.Qualifier_Rules_Val_Rec_Type;
      lx_qualifier_rules_val_rec  qp_qualifier_rules_pub.Qualifier_Rules_Val_Rec_Type;

      l_Msg_Data                 Varchar2(2000);
      l_Msg_Count                Number ;
      L_List_Header_ID           Number;
      l_List_Line_Id             Number ;
      l_Pricing_Attribute_ID     Number ;
      l_Qualifier_ID             Number ;
--      i                          Number ;
      l_Line_COunt               Number ;
      l_ql_count                 NUMBER;

      C_STATUS_VALIDATE          CONSTANT   VARCHAR2(1) := 'V';
      C_STATUS_ERROR             CONSTANT   VARCHAR2(1) := 'E';
      C_STATUS_PROCESSED         CONSTANT   VARCHAR2(1) := 'P' ;

      l_error_message            VARCHAR2 (2000);
      l_exception                EXCEPTION;

      l_errbuf                   VARCHAR2 (2000);
      l_retcode                  NUMBER;

      l_sec                      VARCHAR2 (100);
      l_ln_count                 NUMBER;
      l_pa_Count                 NUMBER;
      l_prev_excludr_line_id     NUMBER;
      l_prev_customer_id         NUMBER;
      l_qual_grp_num             NUMBER;
      l_qualifier_precedence     NUMBER;
      l_qual_exists              VARCHAR2 (1);
      l_qualifier_rule_id        NUMBER;
      l_addnl_qual               NUMBER;

--      l_process_date             DATE := FND_DATE.CANONICAL_TO_DATE(p_process_date); --TO_DATE(p_process_date,'DD-MON-YYYY'); -- Version# 1.4
      l_list_header_id       NUMBER;
      l_qq_exists                NUMBER; -- Version# 1.8
      --l_qualifier_rule_id        NUMBER;
       
BEGIN
fnd_file.put_line (fnd_file.LOG, '##### Delete Excluders from Matrix Modifier -- Start >> ###### ');

--================================================================================================================--
-- Delete Excluders from Matrix Modifier -- Start >>
--================================================================================================================--
      fnd_file.put_line (fnd_file.LOG, '##### Delete Excluders from Matrix Modifier -- Start ###### ');
      l_sec := 'Delete Excluders from Matrix Modifier';
      fnd_file.put_line (fnd_file.LOG, l_sec);
      fnd_file.put_line (fnd_file.LOG, l_sec);

      l_error_message := ' ';
      l_MODIFIER_LIST_rec                    := QP_MODIFIERS_PUB.G_MISS_MODIFIER_LIST_REC;
      l_QUALIFIERS_tbl                       := QP_MODIFIERS_PUB.G_MISS_QUALIFIERS_TBL;

      l_ln_count                             := 0;

      l_MODIFIER_LIST_rec.list_header_id     := p_list_header_id;
      l_MODIFIER_LIST_rec.operation          := QP_GLOBALS.G_OPR_UPDATE;                 -- G_OPR_CREATE ;

      FOR Mod_Line_Rec IN cur_csp_mod_line(p_list_header_id, 'Y') LOOP
          l_sec := 'Loop Matrix Modifier Lines for Delete';
          fnd_file.put_line (fnd_file.LOG, l_sec);
          fnd_file.put_line (fnd_file.LOG, l_sec);

      ----------------------------------------------------------------------------------
      -- Update CSP lines as Exclusions
      ----------------------------------------------------------------------------------
      l_ln_count := 0;
      l_MODIFIERS_Tbl                        := QP_MODIFIERS_PUB.G_MISS_MODIFIERS_TBL;
      l_pa_Count := 0;
      l_PRICING_ATTR_Tbl                     := QP_MODIFIERS_PUB.G_MISS_PRICING_ATTR_TBL;

          fnd_file.put_line (fnd_file.LOG, 'Matrix list_header_id - '||p_list_header_id);
          l_ln_count                                               := l_ln_count + 1;

          fnd_file.put_line (fnd_file.LOG, 'Mod_Line_Rec.list_line_id - '||Mod_Line_Rec.list_line_id);
          l_MODIFIERS_tbl(l_Ln_Count).LIST_LINE_ID                 := Mod_Line_Rec.list_line_id;       -- QP_LIST_LINES_S.NEXTVAL;

          If Mod_Line_Rec.MODIFIER_LEVEL_CODE IS NOT NULL Then
             l_MODIFIERS_tbl(l_Ln_Count).MODIFIER_LEVEL_CODE       := Mod_Line_Rec.MODIFIER_LEVEL_CODE ;
          End If;

          If Mod_Line_Rec.LIST_LINE_TYPE_CODE IS NOT NULL Then
             l_MODIFIERS_tbl(l_Ln_Count).LIST_LINE_TYPE_CODE       := Mod_Line_Rec.LIST_LINE_TYPE_CODE;

          End If;

          If Mod_Line_Rec.AUTOMATIC_FLAG IS NOT NULL Then
             l_MODIFIERS_tbl(l_Ln_Count).AUTOMATIC_FLAG            := Mod_Line_Rec.AUTOMATIC_FLAG;
          End If;

          If Mod_Line_Rec.OVERRIDE_FLAG  Is Not NULL Then
             l_MODIFIERS_tbl(l_Ln_Count).OVERRIDE_FLAG             := Mod_Line_Rec.OVERRIDE_FLAG;
          End If;

          If Mod_Line_Rec.PRICING_PHASE_ID Is Not Null Then
             l_MODIFIERS_tbl(l_Ln_Count).PRICING_PHASE_ID          := Mod_Line_Rec.PRICING_PHASE_ID;
          End If;

          If  Mod_Line_Rec.PRODUCT_PRECEDENCE Is Not Null Then
             l_MODIFIERS_tbl(l_Ln_Count).PRODUCT_PRECEDENCE        := Mod_Line_Rec.PRODUCT_PRECEDENCE;
          End If;

          l_MODIFIERS_tbl(l_Ln_Count).PRICE_BREAK_TYPE_CODE        := Mod_Line_Rec.PRICE_BREAK_TYPE_CODE;
          l_MODIFIERS_tbl(l_Ln_Count).ARITHMETIC_OPERATOR          := Mod_Line_Rec.ARITHMETIC_OPERATOR ;
          l_MODIFIERS_tbl(l_Ln_Count).OPERAND                      := Mod_Line_Rec.OPERAND ;
          l_MODIFIERS_tbl(l_Ln_Count).ACCRUAL_FLAG                 := FND_API.G_MISS_CHAR ;--Mod_Line_Rec.ACCRUAL_FLAG ;
          --l_MODIFIERS_tbl(l_Ln_Count).ACCRUAL_FLAG                 := Mod_Line_Rec.ACCRUAL_FLAG ; -- ?????
          l_MODIFIERS_tbl(l_Ln_Count).INCOMPATIBILITY_GRP_CODE     := Mod_Line_Rec.INCOMPATIBILITY_GRP_CODE ;

          If Mod_Line_Rec.PRICING_GROUP_SEQUENCE  Is Not Null Then
             l_MODIFIERS_tbl(l_Ln_Count).PRICING_GROUP_SEQUENCE    := Mod_Line_Rec.PRICING_GROUP_SEQUENCE ;
          End If;

          If Mod_Line_Rec.INCLUDE_ON_RETURNS_FLAG Is Not Null Then
             l_MODIFIERS_tbl(l_Ln_Count).INCLUDE_ON_RETURNS_FLAG   := Mod_Line_Rec.INCLUDE_ON_RETURNS_FLAG ;
          End If;

          l_MODIFIERS_tbl(l_Ln_Count).PRICE_BY_FORMULA_ID          := Mod_Line_Rec.PRICE_BY_FORMULA_ID ;

          l_MODIFIERS_tbl(l_Ln_Count).operation                    := QP_GLOBALS.G_OPR_UPDATE;   -- G_OPR_CREATE;
          l_MODIFIERS_tbl(l_Ln_Count).START_DATE_ACTIVE            := Mod_Line_Rec.START_DATE_ACTIVE;
          l_MODIFIERS_tbl(l_Ln_Count).END_DATE_ACTIVE              := Mod_Line_Rec.END_DATE_ACTIVE;

            ----------------------------------------------------------------------------------
            -- Delete Exclusion Pricing Attributes
            ----------------------------------------------------------------------------------

          FOR Pricing_Attr_Rec In Get_Pricing_Attr_Cur (p_list_header_id, Mod_Line_Rec.list_line_id, 'Y') LOOP

              l_sec := 'Delete Exclusion Pricing Attributes ';
              fnd_file.put_line (fnd_file.LOG, l_sec);
              fnd_file.put_line (fnd_file.LOG, l_sec);

              l_pa_Count := l_pa_Count + 1;

              l_PRICING_ATTR_tbl(l_pa_Count).LIST_HEADER_ID            :=  p_list_header_id;
              l_PRICING_ATTR_tbl(l_pa_Count).LIST_LINE_ID              :=  Mod_Line_Rec.list_line_id;

              l_PRICING_ATTR_tbl(l_pa_Count).PRICING_ATTRIBUTE_ID      :=  Pricing_Attr_Rec.PRICING_ATTRIBUTE_ID ;
              l_PRICING_ATTR_tbl(l_pa_Count).PRODUCT_ATTRIBUTE_CONTEXT :=  Pricing_Attr_Rec.PRODUCT_ATTRIBUTE_CONTEXT ;
              l_PRICING_ATTR_tbl(l_pa_Count).PRODUCT_ATTRIBUTE         :=  Pricing_Attr_Rec.PRODUCT_ATTRIBUTE ;
              l_PRICING_ATTR_tbl(l_pa_Count).PRODUCT_ATTR_VALUE        :=  Pricing_Attr_Rec.PRODUCT_ATTR_VALUE ;

              l_PRICING_ATTR_tbl(l_pa_Count).PRICING_ATTRIBUTE         :=  Pricing_Attr_Rec.PRICING_ATTRIBUTE ;
              l_PRICING_ATTR_tbl(l_pa_Count).PRODUCT_UOM_CODE          :=  Pricing_Attr_Rec.PRODUCT_UOM_CODE ;
              l_PRICING_ATTR_tbl(l_pa_Count).COMPARISON_OPERATOR_CODE  :=  Pricing_Attr_Rec.COMPARISON_OPERATOR_CODE ;
              l_PRICING_ATTR_tbl(l_pa_Count).PRICING_ATTRIBUTE_CONTEXT :=  Pricing_Attr_Rec.PRICING_ATTRIBUTE_CONTEXT ;
              l_PRICING_ATTR_tbl(l_pa_Count).PRICING_ATTR_VALUE_FROM   :=  Pricing_Attr_Rec.PRICING_ATTR_VALUE_FROM ;
              l_PRICING_ATTR_tbl(l_pa_Count).PRICING_ATTR_VALUE_TO     :=  Pricing_Attr_Rec.PRICING_ATTR_VALUE_TO ;

              l_PRICING_ATTR_tbl(l_pa_Count).ATTRIBUTE_GROUPING_NO     :=  Pricing_Attr_Rec.ATTRIBUTE_GROUPING_NO;

              l_PRICING_ATTR_tbl(l_pa_Count).MODIFIERS_INDEX           := l_pa_Count; -- ?????
              l_PRICING_ATTR_tbl(l_pa_Count).EXCLUDER_FLAG             :=  'Y';
              l_PRICING_ATTR_tbl(l_pa_Count).Operation                 :=  QP_GLOBALS.G_OPR_DELETE;
          END LOOP ;     -- Pricing Attributes

          ----------------------------------------------------------------------------------
          -- Add Modifier Lines
          ----------------------------------------------------------------------------------

          l_return_status :=  NULL;
          l_msg_count     := 0;
          l_msg_data      := NULL;

          QP_Modifiers_PUB.Process_Modifiers
             ( p_api_version_number    => 1.0
             , p_init_msg_list         => FND_API.G_FALSE
             , p_return_values         => FND_API.G_FALSE
             , p_commit                => FND_API.G_TRUE
             , x_return_status         => l_return_status
             , x_msg_count             => l_msg_count
             , x_msg_data              => l_msg_data
             , p_MODIFIER_LIST_rec     => l_MODIFIER_LIST_rec
             , p_MODIFIERS_tbl         => l_MODIFIERS_tbl
             , p_QUALIFIERS_tbl        => l_QUALIFIERS_tbl
             , p_PRICING_ATTR_tbl      => l_PRICING_ATTR_tbl
             , x_MODIFIER_LIST_rec     => lx_MODIFIER_LIST_rec
             , x_MODIFIER_LIST_val_rec => lx_MODIFIER_LIST_val_rec
             , x_MODIFIERS_tbl         => lx_MODIFIERS_tbl
             , x_MODIFIERS_val_tbl     => lx_MODIFIERS_val_tbl
             , x_QUALIFIERS_tbl        => lx_QUALIFIERS_tbl
             , x_QUALIFIERS_val_tbl    => lx_QUALIFIERS_val_tbl
             , x_PRICING_ATTR_tbl      => lx_PRICING_ATTR_tbl
             , x_PRICING_ATTR_val_tbl  => lx_PRICING_ATTR_val_tbl );
          COMMIT;

          l_sec := 'After QP_Modifiers_PUB.Process_Modifiers';
          fnd_file.put_line (fnd_file.LOG, l_sec);
          fnd_file.put_line (fnd_file.LOG, l_sec);

          fnd_file.put_line (fnd_file.LOG, 'x_return_status -'||l_return_status);
          fnd_file.put_line (fnd_file.LOG, 'x_msg_count -'||l_msg_count);
          
          fnd_file.put_line (fnd_file.LOG, 'x_return_status -'||l_return_status);
          fnd_file.put_line (fnd_file.LOG, 'x_msg_count -'||l_msg_count);


fnd_file.put_line (fnd_file.LOG, '# Of Delete Excluders from Matrix Modifier :'||l_Ln_Count);
fnd_file.put_line (fnd_file.LOG, '# Of Delete Excluders Pricing Attr from Matrix Modifier :'||l_pa_Count);

      END LOOP;    -- cur_csp_mod_line

      --================================================================================================================--
      -- Delete Excluders from Matrix Modifier -- End <<
      --================================================================================================================--

fnd_file.put_line (fnd_file.LOG, '##### Delete Excluders from Matrix Modifier -- End < ###### ');

EXCEPTION
WHEN OTHERS THEN
   fnd_file.put_line (fnd_file.LOG, 'delete_mtrx_excluders WHEN OTEHRS ERROR = '||SQLERRM);
   fnd_file.put_line (fnd_file.LOG, l_sec);
      xxcus_error_pkg.xxcus_error_main_api (
         p_called_from         => 'xxwc_pricing_segment_dcom_pkg.delete_mtrx_excluders',
         p_calling             => l_sec,
         p_request_id          => fnd_global.conc_request_id,
         p_ora_error_msg       => SQLERRM,
         p_error_desc          => 'Error in Pricing Segment Process - delete_mtrx_excluders procedure, When Others Exception',
         p_distribution_list   => g_dflt_email,
         p_module              => 'XXWC');
END delete_mtrx_excluders;


/*************************************************************************
PROCEDURE Name: upd_mtrx_incomp

PURPOSE: Update Incompatibility_Group of Matrix Modifiers to : Level 1 Incompatibility

REVISIONS:
Ver        Date        Author                     Description
---------  ----------  ---------------         -------------------------
1.0        10/09/2015  Gopi Damuluri           Initial Version
***************************************************************************/
PROCEDURE upd_mtrx_incomp(p_list_header_id IN NUMBER)
       IS

      ----------------------------------------------------------
      -- PriceList Modifier Line - Cursor Matrix PriceList
      ----------------------------------------------------------
      CURSOR cur_matrix_inc_grp (p_list_header_id IN NUMBER, p_incompatibility_grp IN VARCHAR2)
      IS
      SELECT *
        FROM qp_modifier_summary_v 
       WHERE list_header_id = NVL(p_list_header_id, list_header_id)
         AND NVL(excluder_Flag,'N') = 'N'
         -- AND TRUNC(SYSDATE) BETWEEN TRUNC(NVL(START_DATE_ACTIVE, SYSDATE -1))  AND TRUNC(NVL(END_DATE_ACTIVE, SYSDATE+ 1))
         AND NVL(incompatibility_grp,'-1') = p_incompatibility_grp;

      i NUMBER;
      --------------------------------------------------------------
      --- Define Local Variables here
      --------------------------------------------------------------
      l_qualifier_rec            QP_QUALIFIER_RULES_PUB.QUALIFIERS_REC_TYPE     := QP_QUALIFIER_RULES_PUB.G_MISS_QUALIFIERS_REC;
      l_modifier_line_Rec        QP_MODIFIERS_PUB.MODIFIERS_REC_TYPE            := QP_MODIFIERS_PUB.G_MISS_MODIFIERS_REC;
      l_pricing_att_rec          QP_MODIFIERS_PUB.PRICING_ATTR_REC_TYPE         := QP_MODIFIERS_PUB.G_MISS_PRICING_ATTR_REC;
      l_error_message_list       ERROR_HANDLER.ERROR_TBL_TYPE;
      l_MODIFIER_LIST_rec        QP_Modifiers_PUB.Modifier_List_Rec_Type;
      l_MODIFIER_LIST_val_rec    QP_Modifiers_PUB.Modifier_List_Val_Rec_Type;
      l_MODIFIERS_tbl            QP_Modifiers_PUB.Modifiers_Tbl_Type;
      l_MODIFIERS_val_tbl        QP_Modifiers_PUB.Modifiers_Val_Tbl_Type;
      l_QUALIFIERS_tbl           QP_Qualifier_Rules_PUB.Qualifiers_Tbl_Type;
      l_QUALIFIERS_val_tbl       QP_Qualifier_Rules_PUB.Qualifiers_Val_Tbl_Type;
      l_PRICING_ATTR_tbl         QP_Modifiers_PUB.Pricing_Attr_Tbl_Type;
      l_PRICING_ATTR_val_tbl     QP_Modifiers_PUB.Pricing_Attr_Val_Tbl_Type;
      lx_MODIFIER_LIST_rec       QP_Modifiers_PUB.Modifier_List_Rec_Type;
      lx_MODIFIER_LIST_val_rec   QP_Modifiers_PUB.Modifier_List_Val_Rec_Type;
      lx_MODIFIERS_tbl           QP_Modifiers_PUB.Modifiers_Tbl_Type;
      lx_MODIFIERS_val_tbl       QP_Modifiers_PUB.Modifiers_Val_Tbl_Type;
      lx_QUALIFIERS_tbl          QP_Qualifier_Rules_PUB.Qualifiers_Tbl_Type;
      lx_QUALIFIERS_val_tbl      QP_Qualifier_Rules_PUB.Qualifiers_Val_Tbl_Type;
      lx_PRICING_ATTR_tbl        QP_Modifiers_PUB.Pricing_Attr_Tbl_Type;
      lx_PRICING_ATTR_val_tbl    QP_Modifiers_PUB.Pricing_Attr_Val_Tbl_Type;
      l_qll_rec                  qp_list_lines%ROWTYPE;
      l_pra_rec                  qp_pricing_attributes%ROWTYPE;
      l_control_rec              QP_GLOBALS.Control_Rec_Type;
      l_mod_name                 VARCHAR2 (100)  := 'XXWC_QP_MODIFIER_WEB_ADI_PKG.IMPORT :' ;
      l_return_status            VARCHAR2(1);

      l_qualifier_rules_rec       qp_qualifier_rules_pub.Qualifier_Rules_Rec_Type  := QP_QUALIFIER_RULES_PUB.G_MISS_QUALIFIER_RULES_REC;
      lx_qualifier_rules_rec      qp_qualifier_rules_pub.Qualifier_Rules_Rec_Type;
  
      l_qualifier_rules_val_rec   qp_qualifier_rules_pub.Qualifier_Rules_Val_Rec_Type;
      lx_qualifier_rules_val_rec  qp_qualifier_rules_pub.Qualifier_Rules_Val_Rec_Type;

      l_Msg_Data                 Varchar2(2000);
      l_Msg_Count                Number ;
      L_List_Header_ID           Number;
      l_List_Line_Id             Number ;
      l_Pricing_Attribute_ID     Number ;
      l_Qualifier_ID             Number ;
--      i                          Number ;
      l_Line_COunt               Number ;
      l_ql_count                 NUMBER;

      C_STATUS_VALIDATE          CONSTANT   VARCHAR2(1) := 'V';
      C_STATUS_ERROR             CONSTANT   VARCHAR2(1) := 'E';
      C_STATUS_PROCESSED         CONSTANT   VARCHAR2(1) := 'P' ;

      l_error_message            VARCHAR2 (2000);
      l_exception                EXCEPTION;

      l_errbuf                   VARCHAR2 (2000);
      l_retcode                  NUMBER;

      l_sec                      VARCHAR2 (100);
      l_ln_count                 NUMBER;
      l_pa_Count                 NUMBER;
      l_prev_excludr_line_id     NUMBER;
      l_prev_customer_id         NUMBER;
      l_qual_grp_num             NUMBER;
      l_qualifier_precedence     NUMBER;
      l_qual_exists              VARCHAR2 (1);
      l_qualifier_rule_id        NUMBER;
      l_addnl_qual               NUMBER;

--      l_process_date             DATE := FND_DATE.CANONICAL_TO_DATE(p_process_date); --TO_DATE(p_process_date,'DD-MON-YYYY'); -- Version# 1.4
      l_list_header_id       NUMBER;
      l_qq_exists                NUMBER; -- Version# 1.8
      --l_qualifier_rule_id        NUMBER;
       
BEGIN

fnd_file.put_line (fnd_file.LOG, '##### Update Incompatibility_Group of Matrix Modifiers to : Level 1 Incompatibility > Start ###### ');

      -- ############################################################################## --
      ----------------------------------------------------------------------------------
      -- Update Incompatibility_Group of Matrix Modifiers to : Level 1 Incompatibility > Start
      ----------------------------------------------------------------------------------
      -- ############################################################################## --

         l_sec := 'Matrix PriceList Header - Cursor';
         fnd_file.put_line (fnd_file.LOG, l_sec);

         l_error_message := ' ';
         l_MODIFIER_LIST_rec                    := QP_MODIFIERS_PUB.G_MISS_MODIFIER_LIST_REC;
         l_MODIFIERS_Tbl                        := QP_MODIFIERS_PUB.G_MISS_MODIFIERS_TBL;
         l_PRICING_ATTR_Tbl                     := QP_MODIFIERS_PUB.G_MISS_PRICING_ATTR_TBL;
         l_ln_count                             := 0;

         l_ql_Count := 0 ;
         l_QUALIFIERS_tbl                       := QP_MODIFIERS_PUB.G_MISS_QUALIFIERS_TBL;

         l_MODIFIER_LIST_rec.List_Header_ID     := p_list_header_id;
         l_MODIFIER_LIST_rec.operation          := QP_GLOBALS.G_OPR_UPDATE;                 -- G_OPR_CREATE ;

         ----------------------------------------------------------------------------------
         -- Update Incompatibility_Group of Matrix PriceList Modifiers
         ----------------------------------------------------------------------------------
         l_ln_count := 0;
         l_sec := 'Update Incompatibility_Group of Matrix PriceList Modifiers';
         fnd_file.put_line (fnd_file.LOG, l_sec||TO_CHAR(SYSDATE,'DD-MON-YY HH24:MI:SS'));

         FOR rec_matrix_inc_grp IN cur_matrix_inc_grp (p_list_header_id, 'Exclusive Group') LOOP

             l_ln_count :=  l_ln_count + 1;

             l_MODIFIERS_tbl(l_Ln_Count).LIST_LINE_ID                := rec_matrix_inc_grp.LIST_LINE_ID ;
             l_MODIFIERS_tbl(l_Ln_Count).INCOMPATIBILITY_GRP_CODE    := 'LVL 1';
             l_MODIFIERS_tbl(l_Ln_Count).operation                   := QP_GLOBALS.G_OPR_UPDATE;
         END LOOP;                                               -- Mod_Line_Rec

         fnd_file.put_line (fnd_file.LOG, 'l_Ln_Count - '||l_Ln_Count||TO_CHAR(SYSDATE,'DD-MON-YY HH24:MI:SS'));

         IF l_Ln_Count > 0 THEN
         ----------------------------------------------------------------------------------
         -- Add Modifier Lines
         ----------------------------------------------------------------------------------
         QP_Modifiers_PUB.Process_Modifiers
            ( p_api_version_number    => 1.0
            , p_init_msg_list         => FND_API.G_FALSE
            , p_return_values         => FND_API.G_FALSE
            , p_commit                => FND_API.G_TRUE
            , x_return_status         => l_return_status
            , x_msg_count             => l_msg_count
            , x_msg_data              => l_msg_data
            , p_MODIFIER_LIST_rec     => l_MODIFIER_LIST_rec
            , p_MODIFIERS_tbl         => l_MODIFIERS_tbl
            , p_QUALIFIERS_tbl        => l_QUALIFIERS_tbl
            , p_PRICING_ATTR_tbl      => l_PRICING_ATTR_tbl
            , x_MODIFIER_LIST_rec     => lx_MODIFIER_LIST_rec
            , x_MODIFIER_LIST_val_rec => lx_MODIFIER_LIST_val_rec
            , x_MODIFIERS_tbl         => lx_MODIFIERS_tbl
            , x_MODIFIERS_val_tbl     => lx_MODIFIERS_val_tbl
            , x_QUALIFIERS_tbl        => lx_QUALIFIERS_tbl
            , x_QUALIFIERS_val_tbl    => lx_QUALIFIERS_val_tbl
            , x_PRICING_ATTR_tbl      => lx_PRICING_ATTR_tbl
            , x_PRICING_ATTR_val_tbl  => lx_PRICING_ATTR_val_tbl );

         l_sec := 'After calling Pricing API - BPW';
         fnd_file.put_line (fnd_file.LOG, l_sec);
         fnd_file.put_line (fnd_file.LOG, 'x_return_status -'||l_return_status);
         fnd_file.put_line (fnd_file.LOG, 'x_msg_count -'||l_msg_count);
         -- fnd_file.put_line (fnd_file.LOG, 'x_msg_data -'||l_msg_data);

         COMMIT;
         END IF; -- IF l_Ln_Count > 0 THEN

fnd_file.put_line (fnd_file.LOG, '# Of Update Incompatibility_Group of Matrix Modifiers to : Level 1 Incompatibility :'||l_Ln_Count);
      -- ############################################################################## --
      ----------------------------------------------------------------------------------
      -- Update Incompatibility_Group of Matrix Modifiers to : Level 1 Incompatibility < End
      ----------------------------------------------------------------------------------
      -- ############################################################################## --
fnd_file.put_line (fnd_file.LOG, '##### Update Incompatibility_Group of Matrix Modifiers to : Level 1 Incompatibility < End ###### ');

EXCEPTION
WHEN OTHERS THEN
   fnd_file.put_line (fnd_file.LOG, 'upd_mtrx_incomp WHEN OTEHRS ERROR = '||SQLERRM);
   fnd_file.put_line (fnd_file.LOG, l_sec);
      xxcus_error_pkg.xxcus_error_main_api (
         p_called_from         => 'xxwc_pricing_segment_dcom_pkg.upd_mtrx_incomp',
         p_calling             => l_sec,
         p_request_id          => fnd_global.conc_request_id,
         p_ora_error_msg       => SQLERRM,
         p_error_desc          => 'Error in Pricing Segment Process - upd_mtrx_incomp procedure, When Others Exception',
         p_distribution_list   => g_dflt_email,
         p_module              => 'XXWC');
END upd_mtrx_incomp;

/*************************************************************************
PROCEDURE Name: mtrx_item_cat_excl

PURPOSE: To mark Items as exclusions to ItemCategories

REVISIONS:
Ver        Date        Author                     Description
---------  ----------  ---------------         -------------------------
1.0        10/09/2015  Gopi Damuluri           Initial Version
***************************************************************************/
PROCEDURE mtrx_item_cat_excl(p_errbuf         OUT VARCHAR2
                           , p_retcode        OUT NUMBER
                           , p_name            IN VARCHAR2)
       IS

      ----------------------------------------------------------
      -- Matrix PriceList Modifiers
      ----------------------------------------------------------
      CURSOR cur_mtrx
          IS
      select list_header_id
        from qp_list_headers_all qlh 
       where qlh.attribute10 = 'Segmented Price'
         and qlh.name        = p_name
        ;

      ----------------------------------------------------------
      -- Excluder Line - Cursor
      ----------------------------------------------------------
      CURSOR cur_excluder_line (p_list_header_id IN NUMBER)
          IS
      SELECT distinct seg.list_line_id    item_cat_mod_line_id
           , seg_item.list_line_id        item_line_id
           , seg.product_attr_val         item_cat_id
           , seg_item.product_attr_val    item_id
           , seg.*
           , 'INSERT' excluder_type
        FROM qp_modifier_summary_v        seg
           , qp_modifier_summary_v        seg_item
           , mtl_item_categories          mic
       WHERE 1=1 
         AND seg.list_header_id         = p_list_header_id
         AND seg.product_attr           = 'PRICING_ATTRIBUTE2'      -- Item Category
         AND seg_item.list_header_id    = seg.list_header_id
         AND seg_item.product_attr      = 'PRICING_ATTRIBUTE1'      -- Inventory Item
         AND TO_CHAR(mic.category_id)   = seg.product_attr_val
         AND TRUNC(SYSDATE) BETWEEN TRUNC(NVL(seg.START_DATE_ACTIVE, SYSDATE -1))  AND TRUNC(NVL(seg.END_DATE_ACTIVE, SYSDATE+ 1))
         AND TRUNC(SYSDATE) BETWEEN TRUNC(NVL(seg_item.START_DATE_ACTIVE, SYSDATE -1))  AND TRUNC(NVL(seg_item.END_DATE_ACTIVE, SYSDATE+ 1))
         AND TO_CHAR(mic.inventory_item_id)      = seg_item.product_attr_val
         AND NVL(seg_item.excluder_Flag,'N') = 'N'
         AND NOT EXISTS (SELECT '1'
                       FROM qp_modifier_summary_v seg_item2
                      WHERE seg_item2.list_header_id = seg_item.list_header_id
                        AND seg_item2.product_attr   = 'PRICING_ATTRIBUTE1'      -- Inventory Item
                        AND seg_item2.product_attr_val  = seg_item.product_attr_val
                        AND NVL(seg_item2.excluder_Flag,'N') = 'Y'
                    )
         UNION
      SELECT distinct seg.list_line_id    item_cat_mod_line_id
           , seg_item.list_line_id        item_line_id
           , seg.product_attr_val         item_cat_id
           , seg_item.product_attr_val    item_id
           , seg.*
           , 'DELETE' excluder_type
        FROM qp_modifier_summary_v        seg
           , qp_modifier_summary_v        seg_item
           , mtl_item_categories          mic
       WHERE 1=1 
         AND seg.list_header_id              = p_list_header_id
         AND seg.product_attr                = 'PRICING_ATTRIBUTE2'      -- Item Category
         AND seg_item.list_header_id         = seg.list_header_id
         AND seg_item.product_attr           = 'PRICING_ATTRIBUTE1'      -- Inventory Item
         AND TO_CHAR(mic.category_id)        = seg.product_attr_val
         AND TO_CHAR(mic.inventory_item_id)  = seg_item.product_attr_val
         AND NVL(seg_item.excluder_Flag,'N') = 'Y'
         AND TRUNC(SYSDATE) BETWEEN TRUNC(NVL(seg.START_DATE_ACTIVE, SYSDATE -1))  AND TRUNC(NVL(seg.END_DATE_ACTIVE, SYSDATE+ 1))
         AND TRUNC(SYSDATE) BETWEEN TRUNC(NVL(seg_item.START_DATE_ACTIVE, SYSDATE -1))  AND TRUNC(NVL(seg_item.END_DATE_ACTIVE, SYSDATE+ 1))
         AND EXISTS (SELECT '1'
                       FROM qp_modifier_summary_v seg_item2
                      WHERE seg_item2.list_header_id = seg_item.list_header_id
                        AND seg_item2.product_attr   = 'PRICING_ATTRIBUTE1'      -- Inventory Item
                        AND seg_item2.product_attr_val  = seg_item.product_attr_val
                        AND NVL(seg_item2.excluder_Flag,'N') = 'N'
                        AND TRUNC(NVL(seg_item2.END_DATE_ACTIVE, SYSDATE+ 1)) < TRUNC(SYSDATE)  -- Inactive Item
                    )
         ;

      ----------------------------------------------------------
      -- PriceList Attribute - Cursor
      ----------------------------------------------------------
      CURSOR Get_Pricing_Attr_Cur (p_list_header_id IN NUMBER, p_list_line_id IN NUMBER, p_excluder_Flag IN VARCHAR2,p_product_attr_value IN VARCHAR2)
      IS
      SELECT * 
        FROM qp_pricing_attributes 
       WHERE list_header_id = p_list_header_id
         AND list_line_id   = p_list_line_id
         AND NVL(excluder_Flag,'N') = p_excluder_Flag
         AND product_attribute = 'PRICING_ATTRIBUTE1'
         AND product_attr_value = p_product_attr_value
         ;

      i NUMBER;

      --------------------------------------------------------------
      --- Define Local Variables here
      --------------------------------------------------------------
      l_qualifier_rec            QP_QUALIFIER_RULES_PUB.QUALIFIERS_REC_TYPE     := QP_QUALIFIER_RULES_PUB.G_MISS_QUALIFIERS_REC;
      l_modifier_line_Rec        QP_MODIFIERS_PUB.MODIFIERS_REC_TYPE            := QP_MODIFIERS_PUB.G_MISS_MODIFIERS_REC;
      l_pricing_att_rec          QP_MODIFIERS_PUB.PRICING_ATTR_REC_TYPE         := QP_MODIFIERS_PUB.G_MISS_PRICING_ATTR_REC;
      l_error_message_list       ERROR_HANDLER.ERROR_TBL_TYPE;
      l_MODIFIER_LIST_rec        QP_Modifiers_PUB.Modifier_List_Rec_Type;
      l_MODIFIER_LIST_val_rec    QP_Modifiers_PUB.Modifier_List_Val_Rec_Type;
      l_MODIFIERS_tbl            QP_Modifiers_PUB.Modifiers_Tbl_Type;
      l_MODIFIERS_val_tbl        QP_Modifiers_PUB.Modifiers_Val_Tbl_Type;
      l_QUALIFIERS_tbl           QP_Qualifier_Rules_PUB.Qualifiers_Tbl_Type;
      l_QUALIFIERS_val_tbl       QP_Qualifier_Rules_PUB.Qualifiers_Val_Tbl_Type;
      l_PRICING_ATTR_tbl         QP_Modifiers_PUB.Pricing_Attr_Tbl_Type;
      l_PRICING_ATTR_val_tbl     QP_Modifiers_PUB.Pricing_Attr_Val_Tbl_Type;
      lx_MODIFIER_LIST_rec       QP_Modifiers_PUB.Modifier_List_Rec_Type;
      lx_MODIFIER_LIST_val_rec   QP_Modifiers_PUB.Modifier_List_Val_Rec_Type;
      lx_MODIFIERS_tbl           QP_Modifiers_PUB.Modifiers_Tbl_Type;
      lx_MODIFIERS_val_tbl       QP_Modifiers_PUB.Modifiers_Val_Tbl_Type;
      lx_QUALIFIERS_tbl          QP_Qualifier_Rules_PUB.Qualifiers_Tbl_Type;
      lx_QUALIFIERS_val_tbl      QP_Qualifier_Rules_PUB.Qualifiers_Val_Tbl_Type;
      lx_PRICING_ATTR_tbl        QP_Modifiers_PUB.Pricing_Attr_Tbl_Type;
      lx_PRICING_ATTR_val_tbl    QP_Modifiers_PUB.Pricing_Attr_Val_Tbl_Type;
      l_qll_rec                  qp_list_lines%ROWTYPE;
      l_pra_rec                  qp_pricing_attributes%ROWTYPE;
      l_control_rec              QP_GLOBALS.Control_Rec_Type;
      l_mod_name                 VARCHAR2 (100)  := 'XXWC_QP_MODIFIER_WEB_ADI_PKG.IMPORT :' ;
      l_return_status            VARCHAR2(1);

      l_qualifier_rules_rec       qp_qualifier_rules_pub.Qualifier_Rules_Rec_Type  := QP_QUALIFIER_RULES_PUB.G_MISS_QUALIFIER_RULES_REC;
      lx_qualifier_rules_rec      qp_qualifier_rules_pub.Qualifier_Rules_Rec_Type;
  
      l_qualifier_rules_val_rec   qp_qualifier_rules_pub.Qualifier_Rules_Val_Rec_Type;
      lx_qualifier_rules_val_rec  qp_qualifier_rules_pub.Qualifier_Rules_Val_Rec_Type;

      l_Msg_Data                 Varchar2(2000);
      l_Msg_Count                Number ;
      L_List_Header_ID           Number;
      l_List_Line_Id             Number ;
      l_Pricing_Attribute_ID     Number ;
      l_Qualifier_ID             Number ;
--      i                          Number ;
      l_Line_COunt               Number ;
      l_ql_count                 NUMBER;

      C_STATUS_VALIDATE          CONSTANT   VARCHAR2(1) := 'V';
      C_STATUS_ERROR             CONSTANT   VARCHAR2(1) := 'E';
      C_STATUS_PROCESSED         CONSTANT   VARCHAR2(1) := 'P' ;

      l_error_message            VARCHAR2 (2000);
      l_exception                EXCEPTION;

      l_errbuf                   VARCHAR2 (2000);
      l_retcode                  NUMBER;

      l_sec                      VARCHAR2 (100);
      l_ln_count                 NUMBER;
      l_pa_Count                 NUMBER;
      l_prev_excludr_line_id     NUMBER;
      l_prev_customer_id         NUMBER;
      l_qual_grp_num             NUMBER;
      l_qualifier_precedence     NUMBER;
      l_qual_exists              VARCHAR2 (1);
      l_qualifier_rule_id        NUMBER;
      l_addnl_qual               NUMBER;
      l_list_header_id           NUMBER;
      l_qq_exists                NUMBER;
      l_excluder_flag            VARCHAR2(1);

BEGIN

fnd_file.put_line (fnd_file.LOG, '##### Mark items as exclusions to ItemCategories > Start ###### ');

      -- ############################################################################## --
      ----------------------------------------------------------------------------------
      -- Mark Items as exclusions to ItemCategories > Start
      ----------------------------------------------------------------------------------
      -- ############################################################################## --

      FOR rec_mtrx IN cur_mtrx LOOP

         l_sec := 'Mark items as exclusions to ItemCategories';
         fnd_file.put_line (fnd_file.LOG, l_sec);

         l_error_message := ' ';
         l_MODIFIER_LIST_rec                    := QP_MODIFIERS_PUB.G_MISS_MODIFIER_LIST_REC;

         l_MODIFIER_LIST_rec.list_header_id     := rec_mtrx.list_header_id;
         l_MODIFIER_LIST_rec.operation          := QP_GLOBALS.G_OPR_UPDATE;
         l_ln_count := 0;
         l_pa_Count := 0;

         FOR rec_excluder_line IN cur_excluder_line(rec_mtrx.list_header_id) LOOP

             l_sec := 'Loop Modifier Lines';
             fnd_file.put_line (fnd_file.LOG, l_sec);

             fnd_file.put_line (fnd_file.LOG, 'seg_list_header_id - '||rec_mtrx.list_header_id);
             l_ln_count                             := l_ln_count + 1;

             IF rec_excluder_line.excluder_type = 'DELETE' THEN
                l_MODIFIERS_tbl(l_Ln_Count).LIST_LINE_ID                 := rec_excluder_line.item_line_id;
                l_excluder_flag := 'Y';
             ELSE
                l_MODIFIERS_tbl(l_Ln_Count).LIST_LINE_ID                 := rec_excluder_line.item_cat_mod_line_id;
                l_excluder_flag := 'N';
             END IF;

             If rec_excluder_line.MODIFIER_LEVEL_CODE IS NOT NULL Then
                l_MODIFIERS_tbl(l_Ln_Count).MODIFIER_LEVEL_CODE       := rec_excluder_line.MODIFIER_LEVEL_CODE ;
             End If;

             If rec_excluder_line.LIST_LINE_TYPE_CODE IS NOT NULL Then
                l_MODIFIERS_tbl(l_Ln_Count).LIST_LINE_TYPE_CODE       := rec_excluder_line.LIST_LINE_TYPE_CODE;
             End If;

             l_MODIFIERS_tbl(l_Ln_Count).START_DATE_ACTIVE            := rec_excluder_line.START_DATE_ACTIVE;
             l_MODIFIERS_tbl(l_Ln_Count).END_DATE_ACTIVE              := rec_excluder_line.END_DATE_ACTIVE;

             If rec_excluder_line.AUTOMATIC_FLAG IS NOT NULL Then
                l_MODIFIERS_tbl(l_Ln_Count).AUTOMATIC_FLAG            := rec_excluder_line.AUTOMATIC_FLAG;
             End If;

             If rec_excluder_line.OVERRIDE_FLAG  Is Not NULL Then
                l_MODIFIERS_tbl(l_Ln_Count).OVERRIDE_FLAG             := rec_excluder_line.OVERRIDE_FLAG;
             End If;

             If rec_excluder_line.PRICING_PHASE_ID Is Not Null Then
                l_MODIFIERS_tbl(l_Ln_Count).PRICING_PHASE_ID          := rec_excluder_line.PRICING_PHASE_ID;
             End If;

             If  rec_excluder_line.PRODUCT_PRECEDENCE Is Not Null Then
                l_MODIFIERS_tbl(l_Ln_Count).PRODUCT_PRECEDENCE        := rec_excluder_line.PRODUCT_PRECEDENCE;
             End If;

             l_MODIFIERS_tbl(l_Ln_Count).PRICE_BREAK_TYPE_CODE        := rec_excluder_line.PRICE_BREAK_TYPE_CODE;
             l_MODIFIERS_tbl(l_Ln_Count).ARITHMETIC_OPERATOR          := rec_excluder_line.ARITHMETIC_OPERATOR ;
             l_MODIFIERS_tbl(l_Ln_Count).OPERAND                      := rec_excluder_line.OPERAND ;
             l_MODIFIERS_tbl(l_Ln_Count).ACCRUAL_FLAG                 := FND_API.G_MISS_CHAR; --rec_excluder_line.ACCRUAL_FLAG ;
             l_MODIFIERS_tbl(l_Ln_Count).INCOMPATIBILITY_GRP_CODE     := rec_excluder_line.INCOMPATIBILITY_GRP_CODE ;

             If rec_excluder_line.PRICING_GROUP_SEQUENCE  Is Not Null Then
                l_MODIFIERS_tbl(l_Ln_Count).PRICING_GROUP_SEQUENCE    := rec_excluder_line.PRICING_GROUP_SEQUENCE ;
             End If;

             If rec_excluder_line.INCLUDE_ON_RETURNS_FLAG Is Not Null Then
                l_MODIFIERS_tbl(l_Ln_Count).INCLUDE_ON_RETURNS_FLAG   := rec_excluder_line.INCLUDE_ON_RETURNS_FLAG ;
             End If;

             l_MODIFIERS_tbl(l_Ln_Count).PRICE_BY_FORMULA_ID          := rec_excluder_line.PRICE_BY_FORMULA_ID ;
             l_MODIFIERS_tbl(l_Ln_Count).operation                    := QP_GLOBALS.G_OPR_UPDATE;   -- G_OPR_CREATE;

             ----------------------------------------------------------------------------------
             -- Create Pricing Attributes - As Exclusions
             ----------------------------------------------------------------------------------
             FOR Pricing_Attr_Rec In Get_Pricing_Attr_Cur (rec_mtrx.list_header_id, rec_excluder_line.item_line_id, l_excluder_flag,rec_excluder_line.item_id) LOOP

                 l_sec := 'Create Pricing Attributes - As Exclusions';
                 fnd_file.put_line (fnd_file.LOG, l_sec);

                 l_pa_Count := l_pa_Count + 1;

                 l_PRICING_ATTR_tbl(l_pa_Count).LIST_HEADER_ID            :=  rec_mtrx.list_header_id;
                 
                 l_PRICING_ATTR_tbl(l_pa_Count).PRODUCT_ATTRIBUTE_CONTEXT :=  Pricing_Attr_Rec.PRODUCT_ATTRIBUTE_CONTEXT ;
                 l_PRICING_ATTR_tbl(l_pa_Count).PRODUCT_ATTRIBUTE         :=  Pricing_Attr_Rec.PRODUCT_ATTRIBUTE ;
                 l_PRICING_ATTR_tbl(l_pa_Count).PRODUCT_ATTR_VALUE        :=  Pricing_Attr_Rec.PRODUCT_ATTR_VALUE ;

                 l_PRICING_ATTR_tbl(l_pa_Count).PRICING_ATTRIBUTE         :=  Pricing_Attr_Rec.PRICING_ATTRIBUTE ;
                 l_PRICING_ATTR_tbl(l_pa_Count).PRODUCT_UOM_CODE          :=  Pricing_Attr_Rec.PRODUCT_UOM_CODE ;
                 l_PRICING_ATTR_tbl(l_pa_Count).COMPARISON_OPERATOR_CODE  :=  Pricing_Attr_Rec.COMPARISON_OPERATOR_CODE ;
                 l_PRICING_ATTR_tbl(l_pa_Count).PRICING_ATTRIBUTE_CONTEXT :=  Pricing_Attr_Rec.PRICING_ATTRIBUTE_CONTEXT ;
                 l_PRICING_ATTR_tbl(l_pa_Count).PRICING_ATTR_VALUE_FROM   :=  Pricing_Attr_Rec.PRICING_ATTR_VALUE_FROM ;
                 l_PRICING_ATTR_tbl(l_pa_Count).PRICING_ATTR_VALUE_TO     :=  Pricing_Attr_Rec.PRICING_ATTR_VALUE_TO ;

                 l_PRICING_ATTR_tbl(l_pa_Count).ATTRIBUTE_GROUPING_NO     :=  Pricing_Attr_Rec.ATTRIBUTE_GROUPING_NO;
                 
                 IF rec_excluder_line.excluder_type = 'DELETE' THEN
                    l_PRICING_ATTR_tbl(l_pa_Count).PRICING_ATTRIBUTE_ID   :=  Pricing_Attr_Rec.PRICING_ATTRIBUTE_ID;
                    l_PRICING_ATTR_tbl(l_pa_Count).Operation              :=  QP_GLOBALS.G_OPR_DELETE;
                    l_PRICING_ATTR_tbl(l_pa_Count).LIST_LINE_ID           :=  rec_excluder_line.item_line_id;
                 ELSE
                    l_PRICING_ATTR_tbl(l_pa_Count).PRICING_ATTRIBUTE_ID   :=  QP_PRICING_ATTRIBUTES_S.NEXTVAL;
                    l_PRICING_ATTR_tbl(l_pa_Count).Operation              :=  QP_GLOBALS.G_OPR_CREATE;
                    l_PRICING_ATTR_tbl(l_pa_Count).LIST_LINE_ID           :=  rec_excluder_line.item_cat_mod_line_id;
                 END IF;
                 
                 l_PRICING_ATTR_tbl(l_pa_Count).EXCLUDER_FLAG             :=  'Y';
                 l_PRICING_ATTR_tbl(l_pa_Count).MODIFIERS_INDEX           :=  l_pa_Count;
             END LOOP ;     -- Pricing Attributes
         END LOOP;          -- rec_excluder_line

         IF l_Ln_Count > 0 THEN
         ----------------------------------------------------------------------------------
         -- Add Modifier Lines
         ----------------------------------------------------------------------------------
         QP_Modifiers_PUB.Process_Modifiers
            ( p_api_version_number    => 1.0
            , p_init_msg_list         => FND_API.G_FALSE
            , p_return_values         => FND_API.G_FALSE
            , p_commit                => FND_API.G_TRUE
            , x_return_status         => l_return_status
            , x_msg_count             => l_msg_count
            , x_msg_data              => l_msg_data
            , p_MODIFIER_LIST_rec     => l_MODIFIER_LIST_rec
            , p_MODIFIERS_tbl         => l_MODIFIERS_tbl
            , p_QUALIFIERS_tbl        => l_QUALIFIERS_tbl
            , p_PRICING_ATTR_tbl      => l_PRICING_ATTR_tbl
            , x_MODIFIER_LIST_rec     => lx_MODIFIER_LIST_rec
            , x_MODIFIER_LIST_val_rec => lx_MODIFIER_LIST_val_rec
            , x_MODIFIERS_tbl         => lx_MODIFIERS_tbl
            , x_MODIFIERS_val_tbl     => lx_MODIFIERS_val_tbl
            , x_QUALIFIERS_tbl        => lx_QUALIFIERS_tbl
            , x_QUALIFIERS_val_tbl    => lx_QUALIFIERS_val_tbl
            , x_PRICING_ATTR_tbl      => lx_PRICING_ATTR_tbl
            , x_PRICING_ATTR_val_tbl  => lx_PRICING_ATTR_val_tbl );
         COMMIT;

          l_sec := 'After QP_Modifiers_PUB.Process_Modifiers';
          fnd_file.put_line (fnd_file.LOG, l_sec);
          fnd_file.put_line (fnd_file.LOG, l_sec);

          fnd_file.put_line (fnd_file.LOG, 'x_return_status -'||l_return_status);
          fnd_file.put_line (fnd_file.LOG, 'x_msg_count -'||l_msg_count);
          
          fnd_file.put_line (fnd_file.LOG, 'x_return_status -'||l_return_status);
          fnd_file.put_line (fnd_file.LOG, 'x_msg_count -'||l_msg_count);

          FOR I IN 1 .. l_msg_count LOOP
             l_msg_data := oe_msg_pub.get( p_msg_index => i,p_encoded => 'F');
             fnd_file.put_line (fnd_file.LOG, 'err msg ' || i ||'is:  ' || l_msg_data);
             fnd_file.put_line (fnd_file.LOG, 'err msg ' || i ||'is:  ' || l_msg_data);
          END LOOP;
         END IF;  -- IF l_Ln_Count > 0 THEN
      END LOOP; -- cur_mtrx

fnd_file.put_line (fnd_file.LOG, ' Mark items as exclusions to ItemCategories :'||l_Ln_Count);
      -- ############################################################################## --
      ----------------------------------------------------------------------------------
      -- mark Items as exclusions to ItemCategories < End
      ----------------------------------------------------------------------------------
      -- ############################################################################## --
fnd_file.put_line (fnd_file.LOG, '##### Mark items as exclusions to ItemCategories < End ###### ');

EXCEPTION
WHEN OTHERS THEN
   fnd_file.put_line (fnd_file.LOG, 'MTRX_ITEM_CAT_EXCL WHEN OTEHRS ERROR = '||SQLERRM);
   fnd_file.put_line (fnd_file.LOG, l_sec);
      xxcus_error_pkg.xxcus_error_main_api (
         p_called_from         => 'xxwc_pricing_segment_dcom_pkg.mtrx_item_cat_excl',
         p_calling             => l_sec,
         p_request_id          => fnd_global.conc_request_id,
         p_ora_error_msg       => SQLERRM,
         p_error_desc          => 'Error in Pricing Segment Process - mtrx_item_cat_excl procedure, When Others Exception',
         p_distribution_list   => g_dflt_email,
         p_module              => 'XXWC');
END mtrx_item_cat_excl;

/****************************************************************************************************
PROCEDURE Name: csp_bpw_dcom

PURPOSE: Change incompatibility of all active lines from Level 1 Incompatibility To Exclusive

REVISIONS:
Ver        Date        Author                     Description
---------  ----------  ---------------         -------------------------
1.0        10/09/2015  Gopi Damuluri           Initial Version
****************************************************************************************************/
PROCEDURE csp_bpw_dcom(p_errbuf    OUT VARCHAR2
                     , p_retcode   OUT NUMBER
                     , p_bpw_name  IN  VARCHAR2
                     , p_row_count IN  NUMBER)
       IS

      ----------------------------------------------------------
      -- BPW PriceList Modifiers
      ----------------------------------------------------------
      CURSOR cur_bpw
          IS
      SELECT qlh_b.*
        FROM qp_list_headers_all               qlh_b
           , xxwc.xxwc_om_contract_pricing_hdr xch_b
       WHERE 1 = 1
         AND (p_bpw_name IS NULL OR qlh_b.name = p_bpw_name)
         AND qlh_b.attribute10                 = 'Contract Pricing'
         AND xch_b.agreement_status            IN ('APPROVED', 'DRAFT', 'AWAITING_APPROVAL')
         AND TO_NUMBER (qlh_b.attribute14)     = xch_b.agreement_id
         AND qlh_b.active_flag                 = 'Y'
         AND xch_b.incompatability_group       = 'Best Price Wins'
         --AND TRUNC(SYSDATE) BETWEEN TRUNC(NVL(qlh_b.start_date_active, SYSDATE -1))  AND TRUNC(NVL(qlh_b.end_date_active, SYSDATE+ 1))
         AND ROWNUM                            < p_row_count;

      ----------------------------------------------------------
      -- PriceList Modifier Line - Cursor BPW PriceList
      ----------------------------------------------------------
      CURSOR cur_bpw_excl_grp (p_list_header_id IN NUMBER, p_incompatibility_grp IN VARCHAR2)
      IS
      SELECT *
        FROM qp_modifier_summary_v 
       WHERE list_header_id = NVL(p_list_header_id, list_header_id)
         AND NVL(excluder_Flag,'N') = 'N'
         AND NVL(incompatibility_grp,'-1') = p_incompatibility_grp;

      i NUMBER;

      --------------------------------------------------------------
      --- Define Local Variables here
      --------------------------------------------------------------
      l_qualifier_rec            QP_QUALIFIER_RULES_PUB.QUALIFIERS_REC_TYPE     := QP_QUALIFIER_RULES_PUB.G_MISS_QUALIFIERS_REC;
      l_modifier_line_Rec        QP_MODIFIERS_PUB.MODIFIERS_REC_TYPE            := QP_MODIFIERS_PUB.G_MISS_MODIFIERS_REC;
      l_pricing_att_rec          QP_MODIFIERS_PUB.PRICING_ATTR_REC_TYPE         := QP_MODIFIERS_PUB.G_MISS_PRICING_ATTR_REC;
      l_error_message_list       ERROR_HANDLER.ERROR_TBL_TYPE;
      l_MODIFIER_LIST_rec        QP_Modifiers_PUB.Modifier_List_Rec_Type;
      l_MODIFIER_LIST_val_rec    QP_Modifiers_PUB.Modifier_List_Val_Rec_Type;
      l_MODIFIERS_tbl            QP_Modifiers_PUB.Modifiers_Tbl_Type;
      l_MODIFIERS_val_tbl        QP_Modifiers_PUB.Modifiers_Val_Tbl_Type;
      l_QUALIFIERS_tbl           QP_Qualifier_Rules_PUB.Qualifiers_Tbl_Type;
      l_QUALIFIERS_val_tbl       QP_Qualifier_Rules_PUB.Qualifiers_Val_Tbl_Type;
      l_PRICING_ATTR_tbl         QP_Modifiers_PUB.Pricing_Attr_Tbl_Type;
      l_PRICING_ATTR_val_tbl     QP_Modifiers_PUB.Pricing_Attr_Val_Tbl_Type;
      lx_MODIFIER_LIST_rec       QP_Modifiers_PUB.Modifier_List_Rec_Type;
      lx_MODIFIER_LIST_val_rec   QP_Modifiers_PUB.Modifier_List_Val_Rec_Type;
      lx_MODIFIERS_tbl           QP_Modifiers_PUB.Modifiers_Tbl_Type;
      lx_MODIFIERS_val_tbl       QP_Modifiers_PUB.Modifiers_Val_Tbl_Type;
      lx_QUALIFIERS_tbl          QP_Qualifier_Rules_PUB.Qualifiers_Tbl_Type;
      lx_QUALIFIERS_val_tbl      QP_Qualifier_Rules_PUB.Qualifiers_Val_Tbl_Type;
      lx_PRICING_ATTR_tbl        QP_Modifiers_PUB.Pricing_Attr_Tbl_Type;
      lx_PRICING_ATTR_val_tbl    QP_Modifiers_PUB.Pricing_Attr_Val_Tbl_Type;
      l_qll_rec                  qp_list_lines%ROWTYPE;
      l_pra_rec                  qp_pricing_attributes%ROWTYPE;
      l_control_rec              QP_GLOBALS.Control_Rec_Type;
      l_mod_name                 VARCHAR2 (100)  := 'XXWC_QP_MODIFIER_WEB_ADI_PKG.IMPORT :' ;
      l_return_status            VARCHAR2(1);

      l_qualifier_rules_rec       qp_qualifier_rules_pub.Qualifier_Rules_Rec_Type  := QP_QUALIFIER_RULES_PUB.G_MISS_QUALIFIER_RULES_REC;
      lx_qualifier_rules_rec      qp_qualifier_rules_pub.Qualifier_Rules_Rec_Type;
  
      l_qualifier_rules_val_rec   qp_qualifier_rules_pub.Qualifier_Rules_Val_Rec_Type;
      lx_qualifier_rules_val_rec  qp_qualifier_rules_pub.Qualifier_Rules_Val_Rec_Type;

      l_Msg_Data                 Varchar2(2000);
      l_Msg_Count                Number ;
      L_List_Header_ID           Number;
      l_List_Line_Id             Number ;
      l_Pricing_Attribute_ID     Number ;
      l_Qualifier_ID             Number ;
      l_Line_COunt               Number ;
      l_ql_count                 NUMBER;

      C_STATUS_VALIDATE          CONSTANT   VARCHAR2(1) := 'V';
      C_STATUS_ERROR             CONSTANT   VARCHAR2(1) := 'E';
      C_STATUS_PROCESSED         CONSTANT   VARCHAR2(1) := 'P' ;

      l_error_message            VARCHAR2 (2000);
      l_exception                EXCEPTION;

      l_errbuf                   VARCHAR2 (2000);
      l_retcode                  NUMBER;

      l_sec                      VARCHAR2 (100);
      l_ln_count                 NUMBER;
      l_pa_Count                 NUMBER;
      l_prev_excludr_line_id     NUMBER;
      l_prev_customer_id         NUMBER;
      l_qual_grp_num             NUMBER;
      l_qualifier_precedence     NUMBER;
      l_qual_exists              VARCHAR2 (1);
      l_qualifier_rule_id        NUMBER;
      l_addnl_qual               NUMBER;
      l_list_header_id           NUMBER;
      l_qq_exists                NUMBER; -- Version# 1.8

BEGIN
      -- ############################################################################## --
      ----------------------------------------------------------------------------------
      -- Update Incompatibility_Group of BPW Modifiers to : Exclusive > Start
      ----------------------------------------------------------------------------------
      -- ############################################################################## --

      FOR rec_bpw IN cur_bpw LOOP

         l_sec := 'BPW PriceList Header - Cursor list_header_id - '||rec_bpw.list_header_id;
         fnd_file.put_line (fnd_file.LOG, l_sec);

         l_error_message := ' ';
         l_MODIFIER_LIST_rec                    := QP_MODIFIERS_PUB.G_MISS_MODIFIER_LIST_REC;
         l_MODIFIERS_Tbl                        := QP_MODIFIERS_PUB.G_MISS_MODIFIERS_TBL;
         l_PRICING_ATTR_Tbl                     := QP_MODIFIERS_PUB.G_MISS_PRICING_ATTR_TBL;
         l_ln_count                             := 0;

         l_ql_Count := 0 ;
         l_QUALIFIERS_tbl                       := QP_MODIFIERS_PUB.G_MISS_QUALIFIERS_TBL;

         l_MODIFIER_LIST_rec.List_Header_ID     := rec_bpw.list_header_id;
         l_MODIFIER_LIST_rec.operation          := QP_GLOBALS.G_OPR_UPDATE;                 -- G_OPR_CREATE ;

         ----------------------------------------------------------------------------------
         -- Update Incompatibility_Group of BPW PriceList Modifiers to Exclusive
         ----------------------------------------------------------------------------------
         l_ln_count := 0;
         l_sec := 'Update Incompatibility_Group of BPW PriceList Modifiers';
         fnd_file.put_line (fnd_file.LOG, l_sec||TO_CHAR(SYSDATE,'DD-MON-YY HH24:MI:SS'));

         FOR rec_bpw_excl_grp IN cur_bpw_excl_grp (rec_bpw.list_header_id, 'Level 1 Incompatibility') LOOP
             l_ln_count :=  l_ln_count + 1;
             l_MODIFIERS_tbl(l_Ln_Count).LIST_LINE_ID                := rec_bpw_excl_grp.LIST_LINE_ID ;
             l_MODIFIERS_tbl(l_Ln_Count).INCOMPATIBILITY_GRP_CODE    := 'EXCL';
             l_MODIFIERS_tbl(l_Ln_Count).operation                   := QP_GLOBALS.G_OPR_UPDATE;
         END LOOP;                                               -- Mod_Line_Rec

         fnd_file.put_line (fnd_file.LOG, 'l_Ln_Count - '||l_Ln_Count||TO_CHAR(SYSDATE,'DD-MON-YY HH24:MI:SS'));

         IF l_Ln_Count > 0 THEN
         ----------------------------------------------------------------------------------
         -- Add Modifier Lines
         ----------------------------------------------------------------------------------
         QP_Modifiers_PUB.Process_Modifiers
            ( p_api_version_number    => 1.0
            , p_init_msg_list         => FND_API.G_FALSE
            , p_return_values         => FND_API.G_FALSE
            , p_commit                => FND_API.G_TRUE
            , x_return_status         => l_return_status
            , x_msg_count             => l_msg_count
            , x_msg_data              => l_msg_data
            , p_MODIFIER_LIST_rec     => l_MODIFIER_LIST_rec
            , p_MODIFIERS_tbl         => l_MODIFIERS_tbl
            , p_QUALIFIERS_tbl        => l_QUALIFIERS_tbl
            , p_PRICING_ATTR_tbl      => l_PRICING_ATTR_tbl
            , x_MODIFIER_LIST_rec     => lx_MODIFIER_LIST_rec
            , x_MODIFIER_LIST_val_rec => lx_MODIFIER_LIST_val_rec
            , x_MODIFIERS_tbl         => lx_MODIFIERS_tbl
            , x_MODIFIERS_val_tbl     => lx_MODIFIERS_val_tbl
            , x_QUALIFIERS_tbl        => lx_QUALIFIERS_tbl
            , x_QUALIFIERS_val_tbl    => lx_QUALIFIERS_val_tbl
            , x_PRICING_ATTR_tbl      => lx_PRICING_ATTR_tbl
            , x_PRICING_ATTR_val_tbl  => lx_PRICING_ATTR_val_tbl );

         l_sec := 'After calling Pricing API - BPW';
         fnd_file.put_line (fnd_file.LOG, l_sec);
         fnd_file.put_line (fnd_file.LOG, 'x_return_status -'||l_return_status);
         fnd_file.put_line (fnd_file.LOG, 'x_msg_count -'||l_msg_count);

         COMMIT;
         END IF; -- IF l_Ln_Count > 0 THEN

      ----------------------------------------------------------------------------------
      -- Update Customer_Id and Modifier_Type 
      ----------------------------------------------------------------------------------
      l_sec := 'Update Customer_Id and Modifier_Type ';
      fnd_file.put_line (fnd_file.LOG, l_sec);

      UPDATE xxwc.xxwc_om_contract_pricing_hdr bpw
         SET incompatability_group = 'Exclusive'
       WHERE incompatability_group = 'Best Price Wins'
         AND agreement_id  = rec_bpw.attribute14;

      END LOOP; -- cur_bpw

fnd_file.put_line (fnd_file.LOG, '# Of Update Incompatibility_Group of BPW Modifiers to : Exclusive :'||l_Ln_Count);
      -- ############################################################################## --
      ----------------------------------------------------------------------------------
      -- Update Incompatibility_Group of BPW Modifiers to : Exclusive < End
      ----------------------------------------------------------------------------------
      -- ############################################################################## --
fnd_file.put_line (fnd_file.LOG, '##### Update Incompatibility_Group of BPW Modifiers to : Exclusive < End ###### ');

EXCEPTION
WHEN OTHERS THEN
   fnd_file.put_line (fnd_file.LOG, 'CSP_BPW_DCOM WHEN OTEHRS ERROR = '||SQLERRM);
   fnd_file.put_line (fnd_file.LOG, l_sec);
      xxcus_error_pkg.xxcus_error_main_api (
         p_called_from         => 'xxwc_pricing_segment_dcom_pkg.csp_bpw_dcom',
         p_calling             => l_sec,
         p_request_id          => fnd_global.conc_request_id,
         p_ora_error_msg       => SQLERRM,
         p_error_desc          => 'Error in Pricing Segment Process - csp_bpw_dcom procedure, When Others Exception',
         p_distribution_list   => g_dflt_email,
         p_module              => 'XXWC');
END csp_bpw_dcom;

END XXWC_PRICING_SEGMENT_DCOM_PKG;
/