CREATE OR REPLACE PACKAGE BODY APPS.XXWC_INV_PARTS_ON_FLY_PKG
AS
   /***************************************************************************************************************
   Object Name: XXWC_INV_PARTS_ON_FLY_PKG.pkb
     
   Change Log:
   1.0   07-Aug-2012    S Hariharan     Set default receiving subinv to General
   1.1   01-Jan-2014    Ram Talluri     TMS #20140130-00091 - Defualt price list precedence for specials to 250
   1.2   01-Sep-2015    P.Vamshidhar    TMS#20150629-00046 - 2015 PLM enhancement - Specials Reduction
   1.3   01-Dec-2016    P.Vamshidhar    TMS#20160122-00091  - GL Code Enhancements to leverage GL API
   ****************************************************************************************************************/

   -- *********************************************************************************
   -- PROCEDURE CREATE_CROSS_REF : Creates Cross Reference for the new product
   --  Calls API to created Cross Reference : mtl_cross_references_pkg.insert_row
   --  Commits Transaction once cross reference is successfully created
   -- *********************************************************************************

   PROCEDURE CREATE_CROSS_REF (p_segment1               IN VARCHAR2,
                               p_cross_reference_type   IN VARCHAR2,
                               p_cross_reference        IN VARCHAR2,
                               p_description            IN VARCHAR2)
   IS
      l_cross_reference_id   NUMBER;
      l_inventory_item_id    NUMBER;
      l_API_NAME             VARCHAR2 (30) := 'CREATE_CROSS_REF';
      l_Module_Name          VARCHAR2 (120);
   BEGIN
      --SP-SAT-1001
      -- Begining of API
      l_Module_Name := G_MODULE_NAME || '.' || l_API_NAME;
      XXWC_GEN_ROUTINES_PKG.LOG_MSG (p_debug_level   => G_LEVEL_PROCEDURE,
                                     p_mod_name      => l_Module_Name,
                                     P_DEBUG_MSG     => 'Beging of API');

      SELECT inventory_item_id
        INTO l_inventory_item_id
        FROM mtl_system_items_b msib
       WHERE msib.segment1 = p_segment1 AND ROWNUM = 1;

      mtl_cross_references_pkg.insert_row (
         p_source_system_id         => NULL,
         p_start_date_active        => NULL,
         p_end_date_active          => NULL,
         p_object_version_number    => NULL,
         p_uom_code                 => NULL,
         p_revision_id              => NULL,
         p_epc_gtin_serial          => NULL,
         p_inventory_item_id        => l_inventory_item_id,
         p_organization_id          => NULL,
         p_cross_reference_type     => p_cross_reference_type,
         p_cross_reference          => p_cross_reference,
         p_org_independent_flag     => 'Y',
         p_request_id               => NULL,
         p_attribute1               => NULL,
         p_attribute2               => NULL,
         p_attribute3               => NULL,
         p_attribute4               => NULL,
         p_attribute5               => NULL,
         p_attribute6               => NULL,
         p_attribute7               => NULL,
         p_attribute8               => NULL,
         p_attribute9               => NULL,
         p_attribute10              => NULL,
         p_attribute11              => NULL,
         p_attribute12              => NULL,
         p_attribute13              => NULL,
         p_attribute14              => NULL,
         p_attribute15              => NULL,
         p_attribute_category       => NULL,
         p_description              => p_description,
         p_creation_date            => NULL,
         p_created_by               => NULL,
         p_last_update_date         => NULL,
         p_last_updated_by          => NULL,
         p_last_update_login        => NULL,
         p_program_application_id   => NULL,
         p_program_id               => NULL,
         p_program_update_date      => NULL,
         x_cross_reference_id       => l_cross_reference_id);

      COMMIT;
      XXWC_GEN_ROUTINES_PKG.LOG_MSG (p_debug_level   => G_LEVEL_PROCEDURE,
                                     p_mod_name      => l_Module_Name,
                                     P_DEBUG_MSG     => 'End Of API');
   END CREATE_CROSS_REF;

   -- *********************************************************************************
   -- PROCEDURE ASSIGN_SOURCING_RULE: API to create Sorucing Rules
   -- Public API MRP_SOURCING_RULE_PUB.Process_Sourcing_Rule is called to process sourcing rules.
   -- Upon successfully processing sourcing rules a record is created in Sourcing Rule Assignments table.
   -- *********************************************************************************

   PROCEDURE ASSIGN_SOURCING_RULE (
      p_INVENTORY_ITEM_ID   IN            NUMBER,
      p_vendor_id           IN            NUMBER,
      p_vendor_site_id      IN            NUMBER,
      p_Organization_ID     IN            NUMBER,
      X_Return_Status          OUT NOCOPY VARCHAR2,
      x_Msg_Data               OUT NOCOPY VARCHAR2,
      x_Msg_Count              OUT NOCOPY NUMBER)
   IS
      l_item_id                  NUMBER;
      l_sourcing_rule_id         NUMBER;
      l_API_NAME                 VARCHAR2 (30) := 'ASSIGN_SOURCING_RULE';
      l_Module_Name              VARCHAR2 (120);
      L_Msg_Data                 VARCHAR2 (2000);
      l_Vendor_Name              VARCHAR2 (240);
      l_Vendor_Number            VARCHAR2 (40);
      l_Assignment_Set_ID        NUMBER;
      l_sourcing_rule_rec        MRP_SOURCING_RULE_PUB.SOURCING_RULE_REC_TYPE;
      l_sourcing_rule_val_rec    MRP_SOURCING_RULE_PUB.SOURCING_RULE_VAL_REC_TYPE;
      l_receiving_org_tbl        MRP_SOURCING_RULE_PUB.RECEIVING_ORG_TBL_TYPE;
      l_receiving_org_val_tbl    MRP_SOURCING_RULE_PUB.RECEIVING_ORG_VAL_TBL_TYPE;
      l_shipping_org_tbl         MRP_SOURCING_RULE_PUB.SHIPPING_ORG_TBL_TYPE;
      l_shipping_org_val_tbl     MRP_SOURCING_RULE_PUB.SHIPPING_ORG_VAL_TBL_TYPE;
      lx_sourcing_rule_rec       MRP_SOURCING_RULE_PUB.SOURCING_RULE_REC_TYPE;
      lx_sourcing_rule_val_rec   MRP_SOURCING_RULE_PUB.SOURCING_RULE_VAL_REC_TYPE;
      lx_receiving_org_tbl       MRP_SOURCING_RULE_PUB.RECEIVING_ORG_TBL_TYPE;
      lx_receiving_org_val_tbl   MRP_SOURCING_RULE_PUB.RECEIVING_ORG_VAL_TBL_TYPE;
      lx_shipping_org_tbl        MRP_SOURCING_RULE_PUB.SHIPPING_ORG_TBL_TYPE;
      lx_shipping_org_val_tbl    MRP_SOURCING_RULE_PUB.SHIPPING_ORG_VAL_TBL_TYPE;
   BEGIN
      -- Begining of API
      l_Module_Name := G_MODULE_NAME || '.' || l_API_NAME;
      -- Initialize Return Status Variable
      X_RETURN_STATUS := Fnd_API.G_RET_STS_SUCCESS;
      XXWC_GEN_ROUTINES_PKG.LOG_MSG (
         p_debug_level   => G_LEVEL_PROCEDURE,
         p_mod_name      => l_Module_Name,
         P_DEBUG_MSG     => '700 : Begining of API :');

      /****
      SELECT inventory_item_id
      INTO l_item_id
      FROM mtl_system_items_b
      WHERE segment1 = p_segment1
      AND ROWNUM =1;
      *********/
      BEGIN
         /********** Satish U:  Sourcing Rules are created at Global Level ************/
         -- So Need of checking Organization ID in SQL Query : 26-MAR-2012
         SELECT msr.sourcing_rule_id
           INTO l_sourcing_rule_id
           FROM mrp.mrp_sourcing_rules msr,
                mrp.mrp_sr_receipt_org msro,
                mrp.mrp_sr_source_org msso
          WHERE     msr.sourcing_rule_id = msro.sourcing_rule_id
                AND msro.sr_receipt_id = msso.sr_receipt_id
                AND msso.vendor_id = p_vendor_id
                AND msso.vendor_site_id = p_vendor_site_id
                --AND     msr.Organization_ID = p_Organization_ID
                AND ROWNUM = 1;

         XXWC_GEN_ROUTINES_PKG.LOG_MSG (
            p_debug_level   => G_LEVEL_STATEMENT,
            p_mod_name      => l_Module_Name,
            P_DEBUG_MSG     => '710 : Sourcing Rule Id found :');
      EXCEPTION
         WHEN OTHERS
         THEN
            l_Sourcing_Rule_ID := NULL;
      END;

      IF l_Sourcing_Rule_ID IS NULL
      THEN
         -- Get Vendor Number
         SELECT Segment1, Vendor_Name
           INTO l_Vendor_Number, l_Vendor_Name
           FROM PO_Vendors
          WHERE Vendor_ID = P_Vendor_ID;

         XXWC_GEN_ROUTINES_PKG.LOG_MSG (
            p_debug_level   => G_LEVEL_STATEMENT,
            p_mod_name      => l_Module_Name,
            P_DEBUG_MSG     => '720 : Sourcing Rule Id Not found :');

         -- calli Public API to create Sourcing RUle for Vendor/Vendor Site
         --l_Sourcing_RUle_ID := Fnd_Profile.Value('MRP_DEFAULT_ASSIGNMENT_SET');
         l_sourcing_rule_rec := MRP_SOURCING_RULE_PUB.G_MISS_SOURCING_RULE_REC;
         --l_sourcing_rule_rec.sourcing_rule_id := l_Sourcing_RUle_ID ;


         l_sourcing_rule_rec.sourcing_rule_name := 'SR-' || l_Vendor_Number; --SR Name
         -- Satish U: Creating Global Sourcing Rule :  So commenting Folling Line
         -- l_sourcing_rule_rec.Organization_Id    := p_Organization_ID;
         l_sourcing_rule_rec.planning_active := 1;                  -- Active?
         l_sourcing_rule_rec.status := 1;                 -- Update New record
         l_sourcing_rule_rec.sourcing_rule_type := 1;       -- 1:Sourcing Rule
         l_sourcing_rule_rec.operation := 'CREATE';


         l_receiving_org_tbl := MRP_SOURCING_RULE_PUB.G_MISS_RECEIVING_ORG_TBL;
         l_receiving_org_tbl (1).disable_date := SYSDATE + 90;
         l_receiving_org_tbl (1).effective_date := TRUNC (SYSDATE);
         l_receiving_org_tbl (1).operation := 'CREATE';
         --l_receiving_org_tbl(1).sr_receipt_id           := 3; -- sr_receipt_id;
         -- Satish U : 14-MAR-2012 : Create Global Sourcing Rule

         -- l_receiving_org_tbl(1).receipt_organization_id := p_Organization_ID;

         l_shipping_org_tbl := MRP_SOURCING_RULE_PUB.G_MISS_SHIPPING_ORG_TBL;
         l_shipping_org_tbl (1).RANK := 1;
         l_shipping_org_tbl (1).allocation_percent := 100;   -- Allocation 100
         l_shipping_org_tbl (1).source_type := 3;                  -- BUY FROM
         l_shipping_org_tbl (1).vendor_id := P_VENDOR_ID;
         l_Shipping_Org_Tbl (1).Vendor_Site_ID := p_Vendor_Site_ID;
         l_shipping_org_tbl (1).receiving_org_index := 1;
         l_shipping_org_tbl (1).operation := 'CREATE';


         MRP_SOURCING_RULE_PUB.Process_Sourcing_Rule (
            p_api_version_number      => 1.0,
            p_init_msg_list           => FND_API.G_FALSE,
            p_return_values           => FND_API.G_TRUE,
            p_commit                  => FND_API.G_TRUE,
            x_return_status           => x_Return_Status,
            x_msg_count               => x_Msg_Count,
            x_msg_data                => x_Msg_Data,
            p_Sourcing_Rule_rec       => l_sourcing_rule_rec,
            p_Sourcing_Rule_val_rec   => l_Sourcing_Rule_Val_Rec,
            p_Receiving_Org_tbl       => l_Receiving_Org_Tbl,
            p_Receiving_Org_val_tbl   => l_Receiving_Org_Val_Tbl,
            p_Shipping_Org_tbl        => l_Shipping_Org_Tbl,
            p_Shipping_Org_val_tbl    => l_Shipping_Org_Val_Tbl,
            x_Sourcing_Rule_rec       => lx_Sourcing_Rule_Rec,
            x_Sourcing_Rule_val_rec   => lx_Sourcing_Rule_Val_Rec,
            x_Receiving_Org_tbl       => lx_Receiving_Org_Tbl,
            x_Receiving_Org_val_tbl   => lx_Receiving_Org_Val_Tbl,
            x_Shipping_Org_tbl        => lx_Shipping_Org_Tbl,
            x_Shipping_Org_val_tbl    => lx_Shipping_Org_Val_Tbl);

         l_Sourcing_Rule_ID := lx_Sourcing_Rule_Rec.sourcing_rule_id;
         XXWC_GEN_ROUTINES_PKG.LOG_MSG (
            p_debug_level   => G_LEVEL_STATEMENT,
            p_mod_name      => l_Module_Name,
            P_DEBUG_MSG     =>    '730 : Sourding Rule ID :'
                               || l_Sourcing_Rule_ID);
      END IF;

      IF x_Return_Status = Fnd_API.G_RET_STS_SUCCESS
      THEN
         l_Assignment_Set_ID :=
            Fnd_Profile.VALUE ('MRP_DEFAULT_ASSIGNMENT_SET');
         XXWC_GEN_ROUTINES_PKG.LOG_MSG (
            p_debug_level   => G_LEVEL_STATEMENT,
            p_mod_name      => l_Module_Name,
            P_DEBUG_MSG     =>    '731 : Sourding Rule ID :'
                               || l_Sourcing_Rule_ID);

         INSERT INTO mrp.mrp_sr_assignments (ASSIGNMENT_ID,
                                             ASSIGNMENT_TYPE,
                                             SOURCING_RULE_ID,
                                             SOURCING_RULE_TYPE,
                                             ASSIGNMENT_SET_ID,
                                             LAST_UPDATE_DATE,
                                             LAST_UPDATED_BY,
                                             CREATION_DATE,
                                             CREATED_BY,
                                             ORGANIZATION_ID,
                                             INVENTORY_ITEM_ID)
              VALUES (MRP.MRP_SR_ASSIGNMENTS_S.NEXTVAL,
                      3,
                      l_sourcing_rule_id,
                      1,
                      l_Assignment_Set_ID,
                      SYSDATE,
                      fnd_global.user_id,
                      SYSDATE,
                      fnd_global.user_id,
                      p_Organization_ID,
                      p_Inventory_Item_ID);

         COMMIT;
         x_Return_status := 'S';
         L_Msg_Data :=
            'Record Successfully Inserted into Sourcing Rule Assignment table :';
         XXWC_GEN_ROUTINES_PKG.LOG_MSG (p_debug_level   => G_LEVEL_EVENT,
                                        p_mod_name      => l_Module_Name,
                                        P_DEBUG_MSG     => l_Msg_Data);
      ELSE
         IF x_msg_Count > 1
         THEN
            X_Msg_Data :=
                  '732 : Assign_Sourcing_Rule API Returned Error...'
               || CHR (13)
               || CHR (10);
            l_Msg_Data := ' ';

            FOR i IN 1 .. x_msg_count
            LOOP
               l_msg_data :=
                  fnd_msg_pub.get (p_msg_index => i, p_encoded => 'F');
               x_Msg_Data := x_Msg_Data || l_Msg_Data;
            END LOOP;
         ELSE
            X_Msg_Data :=
               '733: Assign_Sourcing_Rule API Errored Out :' || x_MSG_DATA;
         END IF;


         -- Exit the Process
         x_Return_Status := 'E';

         XXWC_GEN_ROUTINES_PKG.LOG_MSG (p_debug_level   => G_LEVEL_ERROR,
                                        p_mod_name      => l_Module_Name,
                                        P_DEBUG_MSG     => x_Msg_Data);
      END IF;

      XXWC_GEN_ROUTINES_PKG.LOG_MSG (p_debug_level   => G_LEVEL_PROCEDURE,
                                     p_mod_name      => l_Module_Name,
                                     P_DEBUG_MSG     => '750 : End of API :');
   EXCEPTION
      WHEN OTHERS
      THEN
         X_Return_Status := Fnd_API.G_RET_STS_ERROR;
         L_Msg_Data := 'When Others Error :' || SQLERRM;
         XXWC_GEN_ROUTINES_PKG.LOG_MSG (p_debug_level   => G_LEVEL_ERROR,
                                        p_mod_name      => l_Module_Name,
                                        P_DEBUG_MSG     => l_Msg_Data);
   END ASSIGN_SOURCING_RULE;


   -- *********************************************************************************
   -- PROCEDURE CREATE_ASL: API to create Approved Supplier List
   --  API creates a record in Approved Supplier List
   -- *********************************************************************************

   PROCEDURE CREATE_ASL (p_segment1         IN VARCHAR2,
                         p_vendor_id        IN NUMBER,
                         p_vendor_site_id   IN NUMBER)
   IS
      ln_item_id      NUMBER;
      l_API_NAME      VARCHAR2 (30) := 'CREATE_ASL';
      l_Module_Name   VARCHAR2 (120);
      l_Msg_Data      VARCHAR2 (2000);
   BEGIN
      l_Module_Name := G_MODULE_NAME || '.' || l_API_NAME;
      -- Initialize Return Status Variable

      XXWC_GEN_ROUTINES_PKG.LOG_MSG (p_debug_level   => G_LEVEL_PROCEDURE,
                                     p_mod_name      => l_Module_Name,
                                     P_DEBUG_MSG     => 'At begining of API');

      SELECT inventory_item_id
        INTO ln_item_id
        FROM mtl_system_items_b
       WHERE segment1 = p_segment1 AND ROWNUM = 1;

      INSERT INTO po.po_approved_supplier_list (asl_id,
                                                using_organization_id,
                                                owning_organization_id,
                                                vendor_business_type,
                                                asl_status_id,
                                                last_update_date,
                                                last_updated_by,
                                                creation_date,
                                                created_by,
                                                vendor_id,
                                                item_id,
                                                VENDOR_SITE_ID,
                                                last_update_login)
           VALUES (PO_APPROVED_SUPPLIER_LIST_S.NEXTVAL               -- asl_id
                                                      ,
                   -1                                 -- using_organization_id
                     ,
                   84                   -- master org - owning_organization_id
                     ,
                   'DIRECT'                            -- vendor_business_type
                           ,
                   2                                          -- asl_status_id
                    ,
                   SYSDATE                                 -- last_update_date
                          ,
                   fnd_global.user_id                       -- last_updated_by
                                     ,
                   SYSDATE                                    -- creation_date
                          ,
                   fnd_global.user_id                            -- created_by
                                     ,
                   p_vendor_id                                    -- vendor_id
                              ,
                   ln_item_id                                       -- item_id
                             ,
                   p_vendor_site_id,
                   fnd_global.login_id                    -- last_update_login
                                      );

      INSERT INTO po.po_asl_attributes (ASL_ID,
                                        USING_ORGANIZATION_ID,
                                        LAST_UPDATE_DATE,
                                        LAST_UPDATED_BY,
                                        CREATION_DATE,
                                        CREATED_BY,
                                        DOCUMENT_SOURCING_METHOD,
                                        ENABLE_PLAN_SCHEDULE_FLAG,
                                        ENABLE_SHIP_SCHEDULE_FLAG,
                                        ENABLE_AUTOSCHEDULE_FLAG,
                                        ENABLE_AUTHORIZATIONS_FLAG,
                                        VENDOR_ID,
                                        VENDOR_SITE_ID,
                                        ITEM_ID,
                                        LAST_UPDATE_LOGIN,
                                        ENABLE_VMI_FLAG)
           VALUES (PO_APPROVED_SUPPLIER_LIST_S.CURRVAL               -- ASL_ID
                                                      ,
                   -1                                 -- USING_ORGANIZATION_ID
                     ,
                   SYSDATE                                 -- LAST_UPDATE_DATE
                          ,
                   fnd_global.user_id                       -- LAST_UPDATED_BY
                                     ,
                   SYSDATE                                    -- CREATION_DATE
                          ,
                   fnd_global.user_id                            -- CREATED_BY
                                     ,
                   'ASL'                           -- DOCUMENT_SOURCING_METHOD
                        ,
                   'N'                            -- ENABLE_PLAN_SCHEDULE_FLAG
                      ,
                   'N'                            -- ENABLE_SHIP_SCHEDULE_FLAG
                      ,
                   'N'                             -- ENABLE_AUTOSCHEDULE_FLAG
                      ,
                   'N'                           -- ENABLE_AUTHORIZATIONS_FLAG
                      ,
                   p_vendor_id                                    -- VENDOR_ID
                              ,
                   p_vendor_site_id,
                   ln_item_id                                       -- ITEM_ID
                             ,
                   fnd_global.login_id                    -- LAST_UPDATE_LOGIN
                                      ,
                   'N'                                      -- ENABLE_VMI_FLAG
                      );

      XXWC_GEN_ROUTINES_PKG.LOG_MSG (p_debug_level   => G_LEVEL_PROCEDURE,
                                     p_mod_name      => l_Module_Name,
                                     P_DEBUG_MSG     => 'End of API');
      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         --X_Return_Status := Fnd_API.G_RET_STS_ERROR;
         l_Msg_Data := 'When Others Error :' || SQLERRM;
         XXWC_GEN_ROUTINES_PKG.LOG_MSG (p_debug_level   => G_LEVEL_ERROR,
                                        p_mod_name      => l_Module_Name,
                                        P_DEBUG_MSG     => l_Msg_Data);
   END CREATE_ASL;

   -- *********************************************************************************
   -- PROCEDURE CREATE_VENDOR_CATEGORY: API to Vendor Category :
   --  API mtl_categories_pkg.insert_row is called to create Vendor Category
   -- *********************************************************************************


   PROCEDURE CREATE_VENDOR_CATEGORY (
      p_category_set_id   IN            NUMBER,
      p_vendor_id         IN            NUMBER,
      p_vendor_site_id    IN            NUMBER,
      X_category_id          OUT NOCOPY NUMBER)
   IS
      v_address_concat   VARCHAR2 (240);
      v_structure_id     NUMBER;
      v_rowid            ROWID;
      l_API_NAME         VARCHAR2 (30) := 'CREATE_VENDOR_CATEGORY';
      l_Module_Name      VARCHAR2 (120);
   BEGIN
      l_Module_Name := G_MODULE_NAME || '.' || l_API_NAME;
      -- Initialize Return Status Variable

      XXWC_GEN_ROUTINES_PKG.LOG_MSG (p_debug_level   => G_LEVEL_PROCEDURE,
                                     p_mod_name      => l_Module_Name,
                                     P_DEBUG_MSG     => 'At begining of API');

      SELECT SUBSTR (
                   address_line1
                || ', '
                || address_line2
                || ', '
                || address_line3
                || ', '
                || city
                || ', '
                || state
                || ', '
                || zip
                || ', '
                || country,
                1,
                240)
        INTO v_address_concat
        FROM ap_supplier_sites_all assa
       WHERE     assa.vendor_id = p_vendor_id
             AND assa.vendor_site_id = p_vendor_site_id;

      SELECT structure_id
        INTO v_structure_id
        FROM mtl_category_sets_b
       WHERE category_set_id = p_category_set_id;

      SELECT mtl_categories_b_s.NEXTVAL INTO X_category_id FROM DUAL;

      mtl_categories_pkg.insert_row (
         x_rowid                   => v_rowid,
         x_category_id             => x_category_id,
         x_description             => v_address_concat,
         x_structure_id            => v_structure_id,
         x_disable_date            => NULL,
         x_web_status              => 'Y',
         x_supplier_enabled_flag   => 'Y',
         x_segment1                => p_vendor_id,
         x_segment2                => p_vendor_site_id,
         x_segment3                => NULL,
         x_segment4                => NULL,
         x_segment5                => NULL,
         x_segment6                => NULL,
         x_segment7                => NULL,
         x_segment8                => NULL,
         x_segment9                => NULL,
         x_segment10               => NULL,
         x_segment11               => NULL,
         x_segment12               => NULL,
         x_segment13               => NULL,
         x_segment14               => NULL,
         x_segment15               => NULL,
         x_segment16               => NULL,
         x_segment17               => NULL,
         x_segment18               => NULL,
         x_segment19               => NULL,
         x_segment20               => NULL,
         x_summary_flag            => 'N',
         x_enabled_flag            => 'Y',
         x_start_date_active       => NULL,
         x_end_date_active         => NULL,
         x_attribute_category      => NULL,
         x_attribute1              => NULL,
         x_attribute2              => NULL,
         x_attribute3              => NULL,
         x_attribute4              => NULL,
         x_attribute5              => NULL,
         x_attribute6              => NULL,
         x_attribute7              => NULL,
         x_attribute8              => NULL,
         x_attribute9              => NULL,
         x_attribute10             => NULL,
         x_attribute11             => NULL,
         x_attribute12             => NULL,
         x_attribute13             => NULL,
         x_attribute14             => NULL,
         x_attribute15             => NULL,
         x_last_update_date        => SYSDATE,
         x_last_updated_by         => -1,
         x_creation_date           => SYSDATE,
         x_created_by              => -1,
         x_last_update_login       => NULL);

      COMMIT;
   END CREATE_VENDOR_CATEGORY;

   -- *********************************************************************************
   -- PROCEDURE ASSIGN_CATEGORY: API to  Assign a Item Category if it is different from the default Category :
   --
   -- *********************************************************************************

   PROCEDURE ASSIGN_CATEGORY (p_category_id         IN            NUMBER,
                              p_organization_id     IN            NUMBER,
                              p_Inventory_Item_ID   IN            NUMBER,
                              x_Return_Status          OUT NOCOPY VARCHAR2,
                              x_Msg_Data               OUT NOCOPY VARCHAR2,
                              x_Msg_Count              OUT NOCOPY NUMBER)
   IS
      x_message_list                Error_Handler.Error_Tbl_Type;

      x_errorcode                   VARCHAR2 (2000);
      l_category_id                 NUMBER;
      l_Category_Set_ID             NUMBER;
      l_MSg_Count                   NUMBER;
      l_API_NAME                    VARCHAR2 (30) := 'ASSIGN_CATEGORY';
      l_Module_Name                 VARCHAR2 (120);
      l_MULT_ITEM_CAT_ASSIGN_FLAG   VARCHAR2 (1);
   BEGIN
      -- Begining of API
      l_Module_Name := G_MODULE_NAME || '.' || l_API_NAME;
      -- Initialize Return Status Variable
      X_RETURN_STATUS := Fnd_API.G_RET_STS_SUCCESS;
      XXWC_GEN_ROUTINES_PKG.LOG_MSG (p_debug_level   => G_LEVEL_PROCEDURE,
                                     p_mod_name      => l_Module_Name,
                                     P_DEBUG_MSG     => 'At begining of API');

      BEGIN
         SELECT mic.category_id, mic.Category_Set_ID
           INTO l_category_id, l_Category_Set_ID
           FROM mtl_item_categories mic
          WHERE     mic.inventory_item_id = p_Inventory_Item_ID
                AND organization_id = p_organization_id
                AND Mic.Category_Set_ID =
                       TO_NUMBER (
                          FND_PROFILE.VALUE (
                             'XXWC_PARTS_ON_FLY_CATEGORY_SET'))
                --AND mic.category_id = p_category_id
                AND ROWNUM = 1;

         -- Assign Item TO category only if multi
         --l_MULT_ITEM_CAT_ASSIGN_FLAG = 'Y'

         IF l_category_id <> p_Category_ID
         THEN
            -- Satish U: 10/13/2011 : Commenting this statement
            -- If User Choosen Category is different from Default Category then Update Item to User Choosen Category
            INV_ITEM_CATEGORY_PUB.Update_Category_Assignment (
               p_api_version         => 1.0,
               p_init_msg_list       => 'T',
               p_commit              => 'T',
               p_category_id         => p_category_id,
               p_old_category_id     => l_category_id,
               p_category_set_id     => l_category_set_id,
               p_inventory_item_id   => p_Inventory_Item_ID,
               p_organization_id     => p_organization_id,
               x_return_status       => x_return_status,
               x_errorcode           => x_errorcode,
               x_msg_count           => x_msg_count,
               x_msg_data            => x_msg_data);


            IF X_Return_Status <> 'S'
            THEN
               FOR K IN 1 .. X_MSG_COUNT
               LOOP
                  X_MSG_DATA :=
                     FND_MSG_PUB.GET (P_MSG_INDEX => K, P_ENCODED => 'F');
               END LOOP;

               XXWC_GEN_ROUTINES_PKG.LOG_MSG (
                  p_debug_level   => G_LEVEL_ERROR,
                  p_mod_name      => l_Module_Name,
                  P_DEBUG_MSG     =>    'Error while Updating Item Category : '
                                     || x_Msg_Data);
            ELSE
               X_Msg_Data := 'Updating Item to Item Category was successful ';
               XXWC_GEN_ROUTINES_PKG.LOG_MSG (
                  p_debug_level   => G_LEVEL_ERROR,
                  p_mod_name      => l_Module_Name,
                  P_DEBUG_MSG     => x_Msg_Data);
            END IF;
         END IF;

         XXWC_GEN_ROUTINES_PKG.LOG_MSG (p_debug_level   => G_LEVEL_PROCEDURE,
                                        p_mod_name      => l_Module_Name,
                                        P_DEBUG_MSG     => 'End of API');
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            BEGIN
               l_Category_Set_ID :=
                  FND_PROFILE.VALUE ('XXWC_PARTS_ON_FLY_CATEGORY_SET');

               INV_ITEM_CATEGORY_PUB.Create_Category_Assignment (
                  p_api_version         => 1.0,
                  p_init_msg_list       => 'T',
                  p_commit              => 'T',
                  p_category_id         => p_category_id,
                  p_category_set_id     => l_category_set_id,
                  p_inventory_item_id   => p_Inventory_Item_ID,
                  p_organization_id     => p_organization_id,
                  x_return_status       => x_return_status,
                  x_errorcode           => x_errorcode,
                  x_msg_count           => x_msg_count,
                  x_msg_data            => x_msg_data);

               IF X_Return_Status <> 'S'
               THEN
                  IF x_Msg_Count > 1
                  THEN
                     FOR K IN 1 .. X_MSG_COUNT
                     LOOP
                        X_MSG_DATA :=
                           FND_MSG_PUB.GET (P_MSG_INDEX => K, P_ENCODED => 'F');
                     END LOOP;
                  END IF;

                  XXWC_GEN_ROUTINES_PKG.LOG_MSG (
                     p_debug_level   => G_LEVEL_ERROR,
                     p_mod_name      => l_Module_Name,
                     P_DEBUG_MSG     =>    'Error while assigning item to Item Category : '
                                        || x_Msg_Data);
               ELSE
                  X_Msg_Data := 'Assinging Item to Category was successful ';
                  XXWC_GEN_ROUTINES_PKG.LOG_MSG (
                     p_debug_level   => G_LEVEL_ERROR,
                     p_mod_name      => l_Module_Name,
                     P_DEBUG_MSG     => x_Msg_Data);
               END IF;
            EXCEPTION
               WHEN OTHERS
               THEN
                  x_Return_Status := Fnd_API.G_RET_STS_UNEXP_ERROR;
                  XXWC_GEN_ROUTINES_PKG.LOG_MSG (
                     p_debug_level   => G_LEVEL_ERROR,
                     p_mod_name      => l_Module_Name,
                     P_DEBUG_MSG     =>    'When Others Error while creating Category Assignement : '
                                        || SQLERRM);
            END;
      END;

      COMMIT;
   END ASSIGN_CATEGORY;

   -- *********************************************************************************
   -- PROCEDURE Assign_to_price_list: API to  assign to a price List
   --
   -- *********************************************************************************

   PROCEDURE Assign_to_price_list (p_inventory_item_id      NUMBER,
                                   P_List_Header_ID         NUMBER,
                                   p_list_price_per_unit    NUMBER,
                                   p_primary_uom_code       VARCHAR2)
   IS
      L_PRICE_LIST_LINE_TBL       QP_PRICE_LIST_PUB.PRICE_LIST_LINE_TBL_TYPE;
      L_PRICING_ATTR_TBL          QP_PRICE_LIST_PUB.PRICING_ATTR_TBL_TYPE;
      X_PRICE_LIST_LINE_TBL       QP_PRICE_LIST_PUB.PRICE_LIST_LINE_TBL_TYPE;
      X_PRICING_ATTR_TBL          QP_PRICE_LIST_PUB.PRICING_ATTR_TBL_TYPE;
      --
      L_PRICE_LIST_REC            QP_PRICE_LIST_PUB.PRICE_LIST_REC_TYPE;
      L_PRICE_LIST_VAL_REC        QP_PRICE_LIST_PUB.PRICE_LIST_VAL_REC_TYPE;
      L_PRICE_LIST_LINE_VAL_TBL   QP_PRICE_LIST_PUB.PRICE_LIST_LINE_VAL_TBL_TYPE;
      L_QUALIFIERS_TBL            QP_QUALIFIER_RULES_PUB.QUALIFIERS_TBL_TYPE;
      L_QUALIFIERS_VAL_TBL        QP_QUALIFIER_RULES_PUB.QUALIFIERS_VAL_TBL_TYPE;
      L_PRICING_ATTR_VAL_TBL      QP_PRICE_LIST_PUB.PRICING_ATTR_VAL_TBL_TYPE;
      -------
      v_resp_trn_ent              qp_lookups.lookup_code%TYPE;
      v_resp_source               qp_lookups.lookup_code%TYPE;
      ------
      L_RETURN_STATUS             VARCHAR2 (1) := NULL;
      L_MSG_COUNT                 NUMBER := 0;
      L_MSG_DATA                  VARCHAR2 (2000);
      l_API_NAME                  VARCHAR2 (30) := 'ASSIGN_TO_PRICE_LIST';
      l_Hdr_Start_Date_Active     DATE;
      l_Module_Name               VARCHAR2 (120);
   BEGIN
      -- Begining of API
      l_Module_Name := G_MODULE_NAME || '.' || l_API_NAME;



      XXWC_GEN_ROUTINES_PKG.LOG_MSG (p_debug_level   => G_LEVEL_PROCEDURE,
                                     p_mod_name      => l_Module_Name,
                                     P_DEBUG_MSG     => 'Begining of API  : ');

      --Get default one
      --v_resp_trn_ent:=fnd_profile.value('QP_PRICING_TRANSACTION_ENTITY');
      --v_resp_source:=fnd_profile.value('QP_SOURCE_SYSTEM_CODE');
      --Set required one
      --fnd_profile.PUT('QP_PRICING_TRANSACTION_ENTITY',NULL );
      --fnd_profile.PUT('QP_SOURCE_SYSTEM_CODE',NULL);
      XXWC_GEN_ROUTINES_PKG.LOG_MSG (
         p_debug_level   => G_LEVEL_STATEMENT,
         p_mod_name      => l_Module_Name,
         P_DEBUG_MSG     => 'List Header ID   : ' || P_List_Header_ID);

      XXWC_GEN_ROUTINES_PKG.LOG_MSG (
         p_debug_level   => G_LEVEL_STATEMENT,
         p_mod_name      => l_Module_Name,
         P_DEBUG_MSG     => 'Inventory Item ID   : ' || p_inventory_item_id);


      XXWC_GEN_ROUTINES_PKG.LOG_MSG (
         p_debug_level   => G_LEVEL_STATEMENT,
         p_mod_name      => l_Module_Name,
         P_DEBUG_MSG     =>    'QP PRicing Tranaction Entity   : '
                            || v_resp_trn_ent);

      XXWC_GEN_ROUTINES_PKG.LOG_MSG (
         p_debug_level   => G_LEVEL_STATEMENT,
         p_mod_name      => l_Module_Name,
         P_DEBUG_MSG     => 'QP Source Syste Code   : ' || v_resp_source);

      -- Initialize PL/SQL Tables here
      -- POPULATE THE PRICE LIST LINE RECORD VALUES HERE
      -- Get Price List Hdr Start Date Active
      XXWC_GEN_ROUTINES_PKG.LOG_MSG (p_debug_level   => G_LEVEL_PROCEDURE,
                                     p_mod_name      => l_Module_Name,
                                     P_DEBUG_MSG     => 'Begining of API  : ');

      SELECT Start_Date_Active
        INTO l_Hdr_Start_Date_Active
        FROM qp_list_Headers_all
       WHERE List_Header_ID = P_List_Header_ID;

      L_PRICE_LIST_LINE_TBL (1).OPERATION := QP_GLOBALS.G_OPR_CREATE;
      L_PRICE_LIST_LINE_TBL (1).LIST_HEADER_ID := p_List_Header_Id; --MSRP 2008
      L_PRICE_LIST_LINE_TBL (1).LIST_LINE_ID := FND_API.G_MISS_NUM;
      L_PRICE_LIST_LINE_TBL (1).LIST_LINE_TYPE_CODE := 'PLL';
      L_PRICE_LIST_LINE_TBL (1).OPERAND := NVL (p_list_price_per_unit, 0);
      L_PRICE_LIST_LINE_TBL (1).ARITHMETIC_OPERATOR := 'UNIT_PRICE';

      IF     l_Hdr_Start_Date_Active IS NOT NULL
         AND l_Hdr_Start_Date_Active > SYSDATE
      THEN
         L_PRICE_LIST_LINE_TBL (1).START_DATE_ACTIVE :=
            l_Hdr_Start_Date_Active;
      ELSE
         L_PRICE_LIST_LINE_TBL (1).START_DATE_ACTIVE := SYSDATE;
      END IF;

      --L_PRICE_LIST_LINE_TBL(1).QUALIFICATION_IND          := 4 ;
      --L_PRICE_LIST_LINE_TBL(1).PRODUCT_PRECEDENCE         := 220 ;--Commented for TMS #20140130-00091 by Ram Talluri 1/30/2014
      --Added below IF Clause for TMS #20140130-00091 by Ram Talluri 1/30/2014
      IF p_List_Header_Id = fnd_profile.VALUE ('XXWC_QP_MARKET_NATIONAL_PRL')
      THEN
         L_PRICE_LIST_LINE_TBL (1).PRODUCT_PRECEDENCE := 250;
      ELSE
         L_PRICE_LIST_LINE_TBL (1).PRODUCT_PRECEDENCE := 220;
      END IF;


      -- Attribute
      L_PRICING_ATTR_TBL (1).OPERATION := QP_GLOBALS.G_OPR_CREATE;
      L_PRICING_ATTR_TBL (1).LIST_LINE_ID := FND_API.G_MISS_NUM;
      L_PRICING_ATTR_TBL (1).PRICING_ATTRIBUTE_ID := FND_API.G_MISS_NUM;
      L_PRICING_ATTR_TBL (1).LIST_HEADER_ID := p_List_Header_Id;   --MSRP 2008
      L_PRICING_ATTR_TBL (1).PRODUCT_ATTRIBUTE_CONTEXT := 'ITEM';
      L_PRICING_ATTR_TBL (1).PRODUCT_ATTRIBUTE := 'PRICING_ATTRIBUTE1';
      L_PRICING_ATTR_TBL (1).PRODUCT_ATTR_VALUE := p_inventory_item_id;
      L_PRICING_ATTR_TBL (1).PRODUCT_UOM_CODE := p_primary_uom_code;
      L_PRICING_ATTR_TBL (1).EXCLUDER_FLAG := 'N';
      L_PRICING_ATTR_TBL (1).ACCUMULATE_FLAG := 'N';
      --L_PRICING_ATTR_TBL(1).QUALIFICATION_IND          := 4 ;
      L_PRICING_ATTR_TBL (1).PRICE_LIST_LINE_INDEX := 1;
      L_PRICING_ATTR_TBL (1).ATTRIBUTE_GROUPING_NO := 1;
      XXWC_GEN_ROUTINES_PKG.LOG_MSG (
         p_debug_level   => G_LEVEL_STATEMENT,
         p_mod_name      => l_Module_Name,
         P_DEBUG_MSG     => 'Before calling QP_PRICE_LIST_PUB.PROCESS_PRICE_LIST : ');

      QP_PRICE_LIST_PUB.PROCESS_PRICE_LIST (
         P_API_VERSION_NUMBER        => 1.0,
         P_INIT_MSG_LIST             => FND_API.G_TRUE,
         P_RETURN_VALUES             => FND_API.G_FALSE,
         P_COMMIT                    => FND_API.G_TRUE,
         X_RETURN_STATUS             => L_RETURN_STATUS,
         X_MSG_COUNT                 => L_MSG_COUNT,
         X_MSG_DATA                  => L_MSG_DATA,
         P_PRICE_LIST_LINE_TBL       => L_PRICE_LIST_LINE_TBL,
         X_PRICE_LIST_LINE_TBL       => X_PRICE_LIST_LINE_TBL,
         P_PRICING_ATTR_TBL          => L_PRICING_ATTR_TBL,
         X_PRICING_ATTR_TBL          => X_PRICING_ATTR_TBL,
         P_PRICE_LIST_REC            => L_PRICE_LIST_REC,
         X_PRICE_LIST_REC            => L_PRICE_LIST_REC,
         X_PRICE_LIST_VAL_REC        => L_PRICE_LIST_VAL_REC,
         X_PRICE_LIST_LINE_VAL_TBL   => L_PRICE_LIST_LINE_VAL_TBL,
         X_QUALIFIERS_TBL            => L_QUALIFIERS_TBL,
         X_QUALIFIERS_VAL_TBL        => L_QUALIFIERS_VAL_TBL,
         X_PRICING_ATTR_VAL_TBL      => L_PRICING_ATTR_VAL_TBL);

      XXWC_GEN_ROUTINES_PKG.LOG_MSG (
         p_debug_level   => G_LEVEL_STATEMENT,
         p_mod_name      => l_Module_Name,
         P_DEBUG_MSG     => 'Return Status of API is  : ' || L_RETURN_STATUS);
      XXWC_GEN_ROUTINES_PKG.LOG_MSG (
         p_debug_level   => G_LEVEL_ERROR,
         p_mod_name      => l_Module_Name,
         P_DEBUG_MSG     =>    'Error Message Count : '
                            || L_Msg_Count
                            || L_Msg_Data);

      IF L_RETURN_STATUS <> 'S'
      THEN
         IF l_Msg_Count > 1
         THEN
            L_MSg_Data := '';

            FOR K IN 1 .. L_MSG_COUNT
            LOOP
               L_MSG_DATA :=
                     L_MSG_DATA
                  || oe_msg_pub.get (p_msg_index => K, p_encoded => 'F');
               --L_MSG_DATA := L_MSG_DATA || FND_MSG_PUB.GET( P_MSG_INDEX => K,    P_ENCODED => 'F');
               XXWC_GEN_ROUTINES_PKG.LOG_MSG (
                  p_debug_level   => G_LEVEL_ERROR,
                  p_mod_name      => l_Module_Name,
                  P_DEBUG_MSG     => 'Error Message : ' || L_Msg_Data);
            END LOOP;
         END IF;

         XXWC_GEN_ROUTINES_PKG.LOG_MSG (
            p_debug_level   => G_LEVEL_ERROR,
            p_mod_name      => l_Module_Name,
            P_DEBUG_MSG     =>    'Error while assing item to Price List : '
                               || L_Msg_Data);
      END IF;

      XXWC_GEN_ROUTINES_PKG.LOG_MSG (p_debug_level   => G_LEVEL_PROCEDURE,
                                     p_mod_name      => l_Module_Name,
                                     P_DEBUG_MSG     => 'End of API  : ');

      COMMIT;
   END;

   -- *********************************************************************************
   -- PROCEDURE CREATE_ITEM_COST: API to  create Item Cost : But API is more used in the function
   --
   -- *********************************************************************************

   PROCEDURE CREATE_ITEM_COST (p_Inventory_Item_ID   IN            NUMBER,
                               p_Organization_ID     IN            NUMBER,
                               p_Item_Cost           IN            NUMBER,
                               x_Return_Status          OUT NOCOPY VARCHAR2,
                               x_Msg_Data               OUT NOCOPY VARCHAR2,
                               x_Msg_Count              OUT NOCOPY NUMBER)
   IS
      l_API_NAME                         VARCHAR2 (30) := 'CREATE_ITEM_COST';
      l_Module_Name                      VARCHAR2 (120);
      C_COST_TYPE_AVG           CONSTANT NUMBER := 2;
      C_COST_ELEMENT_MATERIAL   CONSTANT NUMBER := 1;
      C_PROCESS_FLAG            CONSTANT NUMBER := 1;
   BEGIN
      -- Begining of API
      l_Module_Name := G_MODULE_NAME || '.' || l_API_NAME;

      XXWC_GEN_ROUTINES_PKG.LOG_MSG (p_debug_level   => G_LEVEL_PROCEDURE,
                                     p_mod_name      => l_Module_Name,
                                     P_DEBUG_MSG     => 'At begining of API');

      IF G_COST_GROUP_ID IS NULL
      THEN
         SELECT CST_LISTS_S.NEXTVAL INTO G_Cost_Group_ID FROM DUAL;
      END IF;

      -- Initialize Return Status Variable
      X_RETURN_STATUS := Fnd_API.G_RET_STS_SUCCESS;

      INSERT INTO cst_item_cst_dtls_interface (inventory_item_id,
                                               Organization_ID,
                                               cost_element_id,
                                               cost_type_id,
                                               usage_rate_or_amount,
                                               item_cost,
                                               process_flag,
                                               GROUP_ID,
                                               created_by,
                                               creation_date)
           VALUES (p_Inventory_Item_ID,
                   p_organization_id,
                   C_COST_TYPE_AVG,
                   C_COST_ELEMENT_MATERIAL,
                   p_Item_Cost,
                   p_Item_Cost,
                   C_PROCESS_FLAG,
                   G_COST_GROUP_ID,
                   fnd_global.user_id,
                   SYSDATE);

      XXWC_GEN_ROUTINES_PKG.LOG_MSG (p_debug_level   => G_LEVEL_PROCEDURE,
                                     p_mod_name      => l_Module_Name,
                                     P_DEBUG_MSG     => 'At begining of API');
      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         x_Return_Status := 'U';
         x_Msg_Data := 'When Others Error : ' || SQLERRM;
         x_Msg_Count := 1;
         XXWC_GEN_ROUTINES_PKG.LOG_MSG (p_debug_level   => G_LEVEL_ERROR,
                                        p_mod_name      => l_Module_Name,
                                        P_DEBUG_MSG     => x_Msg_Data);
   END CREATE_ITEM_COST;

   -- *********************************************************************************
   -- PROCEDURE SET_ITEM_NUMBER_NULL_SO: API to  assign NULL value  Global Temp Table when parts
   --  created from Sales ORder
   --
   -- *********************************************************************************

   PROCEDURE SET_ITEM_NUMBER_NULL_SO (P_TRANSACTION_ID IN NUMBER)
   IS
      PRAGMA AUTONOMOUS_TRANSACTION;
      l_Profile_Status   BOOLEAN;
   BEGIN
      DELETE FROM XXWC_SHARE_VARIABLES
            WHERE     Transaction_Type = G_TRX_TYPE_PARTS_ON_FLY_SO
                  AND Transaction_ID = P_TRANSACTION_ID
                  AND Created_By = Fnd_Global.USer_ID;

      COMMIT;
   END SET_ITEM_NUMBER_NULL_SO;

   -- *********************************************************************************
   -- PROCEDURE SET_ITEM_NUMBER_NULL_RO: API to  assign NULL value  Global Temp Table when parts
   --  created from Repair ORder
   --
   -- *********************************************************************************

   PROCEDURE SET_ITEM_NUMBER_NULL_RO (P_TRANSACTION_ID IN NUMBER)
   IS
      PRAGMA AUTONOMOUS_TRANSACTION;
      l_Profile_Status   BOOLEAN;
   BEGIN
      DELETE FROM XXWC_SHARE_VARIABLES
            WHERE     Transaction_Type = G_TRX_TYPE_PARTS_ON_FLY_RO
                  AND Transaction_ID = P_TRANSACTION_ID
                  AND Created_By = Fnd_Global.USer_ID;

      COMMIT;
   END SET_ITEM_NUMBER_NULL_RO;

   -- *********************************************************************************
   -- PROCEDURE SET_ITEM_NUMBER: API to  Updates values to Shared Variabls
   -- *********************************************************************************

   PROCEDURE SET_ITEM_NUMBER (P_INVENTORY_ITEM_ID   IN NUMBER,
                              P_ITEM_NUMBER         IN VARCHAR2,
                              P_TRANSACTION_TYPE    IN VARCHAR2,
                              P_ORGANIZATION_ID     IN NUMBER,
                              P_ORGANIZATION_CODE   IN VARCHAR2,
                              P_TRANSACTION_ID      IN NUMBER)
   IS
      PRAGMA AUTONOMOUS_TRANSACTION;

      -- Define a Cursor to update Existing Record
      CURSOR Item_Cur
      IS
         SELECT *
           FROM XXWC_SHARE_VARIABLES
          WHERE     Transaction_TYpe = p_Transaction_TYpe
                AND Inventory_Item_ID = P_INVENTORY_ITEM_ID
                AND TRANSACTION_ID = P_TRANSACTION_ID
                AND Created_By = Fnd_Global.User_ID;

      --And   Organization_ID   = P_Organization_ID
      --And   Item_Number       = p_Item_Number
      l_Record_Found   VARCHAR2 (1) := 'N';
   BEGIN
      FOR Item_Rec IN Item_Cur
      LOOP
         l_Record_Found := 'Y';

         UPDATE XXWC_SHARE_VARIABLES
            SET TRANSACTION_TYPE = p_Transaction_TYpe,
                TRANSACTION_ID = P_TRANSACTION_ID,
                INVENTORY_ITEM_ID = P_INVENTORY_ITEM_ID,
                ITEM_NUMBER = p_Item_Number,
                CREATED_BY = Fnd_Global.User_ID,
                ORGANIZATION_ID = P_Organization_ID
          WHERE     TRANSACTION_ID = p_TRANSACTION_ID
                AND TRANSACTION_TYPE = p_Transaction_TYpe
                AND CREATED_BY = Fnd_Global.User_ID
                AND Inventory_Item_ID = P_INVENTORY_ITEM_ID;
      END LOOP;

      IF L_Record_Found = 'N'
      THEN
         INSERT INTO XXWC_SHARE_VARIABLES (TRANSACTION_TYPE,
                                           TRANSACTION_ID,
                                           INVENTORY_ITEM_ID,
                                           ITEM_NUMBER,
                                           ORGANIZATION_ID,
                                           ORGANIZATION_CODE,
                                           CREATED_BY,
                                           CREATION_DATE)
              VALUES (P_TRANSACTION_TYPE,
                      P_TRANSACTION_ID,
                      P_INVENTORY_ITEM_ID,
                      P_ITEM_NUMBER,
                      P_ORGANIZATION_ID,
                      P_ORGANIZATION_CODE,
                      Fnd_Global.User_ID,
                      SYSDATE);
      END IF;

      COMMIT;
   END;

   -- *********************************************************************************
   -- FUNCTION GET_ITEM_NUMBER: Gets Item Number from Shared Variables table
   -- *********************************************************************************

   FUNCTION GET_ITEM_NUMBER (P_TRANSACTION_ID     IN NUMBER,
                             P_TRANSACTION_TYPE   IN VARCHAR2)
      RETURN VARCHAR2
   IS
      l_Item_Number   VARCHAR2 (40);
   BEGIN
        SELECT Item_Number
          INTO l_ITem_Number
          FROM XXWC_SHARE_VARIABLES
         WHERE     Transaction_ID = P_TRANSACTION_ID
               AND Transaction_Type = p_Transaction_Type
               AND Created_By = FNd_Global.User_ID
               AND ROWNUM = 1
      ORDER BY creation_date ASC;

      RETURN l_ITEM_NUMBER;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END GET_ITEM_NUMBER;

   -- *********************************************************************************
   -- FUNCTION GET_INVENTORY_ITEM_ID: Gets Item Item Id from Shared Variables table
   -- *********************************************************************************

   FUNCTION GET_INVENTORY_ITEM_ID (P_TRANSACTION_ID     IN NUMBER,
                                   P_TRANSACTION_TYPE   IN VARCHAR2)
      RETURN NUMBER
   IS
      l_Inventory_Item_ID   NUMBER;
   BEGIN
        SELECT Inventory_Item_ID
          INTO l_Inventory_Item_ID
          FROM XXWC_SHARE_VARIABLES
         WHERE     Transaction_ID = P_TRANSACTION_ID
               AND Transaction_Type = p_Transaction_Type
               AND Created_By = FNd_Global.User_ID
               AND ROWNUM = 1
      ORDER BY creation_date ASC;

      RETURN l_Inventory_Item_ID;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END GET_INVENTORY_ITEM_ID;

   -- *********************************************************************************
   -- FUNCTION GET_ITEM_UOM: Gets Item UOM Code for Item Master Record
   -- *********************************************************************************

   FUNCTION GET_ITEM_UOM (P_INVENTORY_ITEM_ID IN NUMBER)
      RETURN VARCHAR2
   IS
      l_UOM_Code   VARCHAR2 (10);
   BEGIN
      SELECT PRimary_UOM_CODE
        INTO l_UOM_Code
        FROM Mtl_System_Items_B msi
       WHERE     msi.Inventory_Item_ID = p_Inventory_Item_ID
             AND msi.Organization_Id =
                    Fnd_Profile.VALUE ('XXWC_ITEM_MASTER_ORG')
             AND ROWNUM = 1;

      RETURN l_UOM_CODE;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;

   -- *********************************************************************************
   -- FUNCTION GET_ITEM_UOM_DESC: Gets Item UOM Description for Item Master Record
   -- *********************************************************************************


   FUNCTION GET_ITEM_UOM_DESC (P_INVENTORY_ITEM_ID IN NUMBER)
      RETURN VARCHAR2
   IS
      l_UOM_DESC   VARCHAR2 (30);
   BEGIN
      SELECT UOM.Unit_Of_Measure
        INTO l_UOM_Desc
        FROM Mtl_System_Items_B msi, Mtl_Units_Of_Measure_vl uom
       WHERE     msi.Inventory_Item_ID = p_Inventory_Item_ID
             AND msi.Organization_Id =
                    Fnd_Profile.VALUE ('XXWC_ITEM_MASTER_ORG')
             AND msi.Primary_UOM_COde = uom.Uom_Code
             AND ROWNUM = 1;

      RETURN l_UOM_DESC;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END GET_ITEM_UOM_DESC;

   --*****************************************************************************************
   -- Program to Inactivate  Items whose ITEM_TYPE IS SPECIALS and that are created 90 ( P_DAYS_AFTER_CREATION )
   -- Satish U: 11-JUN-2012    Assign White Cap Category to Item Master
   --*****************************************************************************************
/***************************************************************************************************************
   Object Name: CREATE_ITEM
     
   Change Log:
   1.3   01-Dec-2016    P.Vamshidhar    TMS#20160122-00091  - GL Code Enhancements to leverage GL API
   ****************************************************************************************************************/   
   PROCEDURE CREATE_ITEM (
      p_Parts_On_Fly_Rec   IN OUT NOCOPY Parts_On_Fly_Rec,
      x_Return_Status         OUT NOCOPY VARCHAR2,
      x_Msg_Data              OUT NOCOPY VARCHAR2,
      x_Msg_Count             OUT NOCOPY NUMBER)
   IS
      l_Item_Rec                        Inv_Item_Grp.Item_Rec_Type;
      lg_Miss_Item_rec                  Inv_Item_Grp.Item_Rec_Type;
      lx_item_Rec                       Inv_Item_Grp.Item_rec_Type;
      lx_error_TBL                      Inv_Item_Grp.Error_Tbl_Type;
      lx_Message_List                   Error_Handler.Error_Tbl_Type;
      lx_Return_Status                  VARCHAR2 (1);
      lx_Msg_Data                       VARCHAR2 (2000) := NULL;
      l_Msg_Data                        VARCHAR2 (2000);
      i                                 NUMBER;
      lx_msg_Count                      NUMBER;
      l_result                          BOOLEAN;
      l_phase                           VARCHAR2 (500) := NULL;
      l_status                          VARCHAR2 (500) := NULL;
      l_devphase                        VARCHAR2 (500) := 'PENDING';
      l_devstatus                       VARCHAR2 (500) := NULL;
      l_message                         VARCHAR2 (500) := NULL;
      l_API_NAME                        VARCHAR2 (30) := 'CREATE_ITEM';
      l_Request_ID                      NUMBER;
      l_Module_Name                     VARCHAR2 (120);
      l_Import_Item_Cost_Only           NUMBER := 1;  --Import item costs only
      l_Cost_RUN_Option_Create          NUMBER := 1; -- Insert new cost information only
      l_Group_RUn_Option_Specific       NUMBER := 1; -- Run for Specific group ID
      l_Cost_TYpe_Avg_Rate              VARCHAR2 (30) := 'Avg Rates';
      l_Delete_Rows_Yes                 NUMBER := 1; -- Delete Rows after successful Import
      l_Profile_Status                  BOOLEAN;
      l_Item_Catalog_Group_ID           NUMBER := NULL;
      l_Inventory_Item_ID               NUMBER;

      -- 1. SQL For Geting Cost Import Options
      --Select Meaning, Lookup_Code
      --From MFG_LOOKUPS
      --WHERE LOOKUP_TYPE = 'CST_ITEM_IMP_TYPE'

      --2. SQL For Geting COst RUN Option
      --Select Meaning, Lookup_Code
      --From MFG_LOOKUPS
      --WHERE LOOKUP_TYPE = 'CST_RUN_OPTION'

      --3 SQL For Geting Group RUN Option ( Specific Group or All Groups )
      --Select Meaning, Lookup_Code
      --From MFG_LOOKUPS
      --WHERE LOOKUP_TYPE = 'CST_GROUP_OPTION'


      --4 SQL For Geting COst RUN Option
      --Select Meaning, Lookup_Code
      --From MFG_LOOKUPS
      --WHERE LOOKUP_TYPE = 'CST_GROUP_OPTION'
      -- 26-JUN-2012 : Vendor Product Number should point to VENDOR Cross Reference and Not Manufacturer Cross Reference.
      --C_CROSS_REFERENCE_TYPE    Constant     Varchar2(30) := 'MANUFACTURER';
      C_CROSS_REFERENCE_TYPE   CONSTANT VARCHAR2 (30) := 'VENDOR';

      -- Satish U: 13-MAR-2012 : Define a Cursor that gets list of all the Inventory Organizations for a given Master Organization ID
      CURSOR Inv_Orgs_Cur
      IS
         SELECT Organization_ID, Organization_Code
           FROM Mtl_Parameters mp
          WHERE     Mp.Master_Organization_ID =
                       Fnd_Profile.VALUE ('XXWC_ITEM_MASTER_ORG')
                AND MP.Organization_ID <> MP.Master_Organization_ID
                AND (   (    P_PARTS_ON_FLY_REC.RADIO_GROUP_VALUE <> 'R'
                         AND MP.Organization_ID =
                                P_Parts_ON_Fly_Rec.ASSGN_ORGANIZATION_ID)
                     OR (    P_PARTS_ON_FLY_REC.RADIO_GROUP_VALUE = 'R'
                         --commented by Shankar for CP 951-- no need to assign it to all orgs
                         --AND MP.Organization_ID = MP.Organization_ID )
                         AND MP.Organization_ID =
                                P_Parts_ON_Fly_Rec.ASSGN_ORGANIZATION_ID));

      CURSOR Get_Item_Category_Cur
      IS
         SELECT item_catalog_group_id
           FROM mtl_item_catalog_Groups_b
          WHERE segment1 = 'WHITE CAP';
   BEGIN
      -- Begining of API
      l_Module_Name := G_MODULE_NAME || '.' || l_API_NAME;
      -- Initialize Return Status Variable
      X_RETURN_STATUS := Fnd_API.G_RET_STS_SUCCESS;
      X_MSG_DATA := NULL;
      FND_MESSAGE.CLEAR;
      XXWC_GEN_ROUTINES_PKG.LOG_MSG (
         p_debug_level   => G_LEVEL_PROCEDURE,
         p_mod_name      => l_Module_Name,
         P_DEBUG_MSG     => '10001 : At begining of API');

      -- Assign Parts_ON_Fly Record Structure values to Item_REC .


      l_item_rec.segment1 := p_Parts_On_Fly_Rec.ITEM_NUMBER;
      l_item_rec.organization_id := p_Parts_On_Fly_Rec.MST_ORGANIZATION_ID;
      l_item_rec.description := p_Parts_On_Fly_Rec.ITEM_DESCRIPTION;
      l_item_rec.primary_uom_code := p_Parts_On_Fly_Rec.PRIMARY_UOM_CODE;
      -- Copy Cost value to List_Price in Inventory : Commenting Passing List Price : 02/10/2012 : SatishU
      --l_item_rec.list_price_per_unit := p_Parts_On_Fly_Rec.LIST_PRICE;
      l_item_rec.list_price_per_unit := p_Parts_On_Fly_Rec.ITEM_COST;
      l_item_rec.Weight_UOM_CODE := p_Parts_On_Fly_Rec.Weight_UOM_CODE;
      l_Item_Rec.Unit_Weight := P_Parts_ON_Fly_Rec.UNIT_WEIGHT;
      l_Item_Rec.Buyer_ID := P_Parts_ON_Fly_Rec.Buyer_ID;
      l_Item_Rec.Hazard_Class_ID := P_Parts_ON_Fly_Rec.Hazard_Class_ID;
      l_Item_Rec.HAZARDOUS_MATERIAL_FLAG :=
         P_Parts_On_Fly_Rec.HAZARDOUS_MATERIAL_FLAG;
      l_Item_Rec.Market_Price := p_Parts_On_Fly_Rec.Market_Price;


      --11-Jun-2012 : Assign Item Catalog Group ID for master ORg Level

      FOR Item_Category_Rec IN Get_Item_Category_Cur
      LOOP
         l_Item_Catalog_Group_ID := Item_Category_Rec.item_catalog_group_id;
      END LOOP;

      l_Item_Rec.ITEM_CATALOG_GROUP_ID := l_Item_Catalog_Group_ID;


      --l_item_rec.full_lead_time      := p_full_lead_time;
      --l_item_rec.attribute8          := TO_CHAR (p_spa_expiration, 'YYYY/MM/DD');
      -- l_item_rec.inventory_item_status_code := 'New';
      l_item_rec.Allow_Item_Desc_Update_Flag := 'Y';
      --Added by Shankar Hariharan 05-Oct-2012 -- to default to supplier
      l_item_rec.source_type := 2;

      --Added by Shankar Hariharan 28-Jun-2013 -- to default taxware code
      IF p_Parts_On_Fly_Rec.Category_ID IS NOT NULL
      THEN
         SELECT attribute4
           INTO l_item_rec.attribute22
           FROM mtl_categories_v
          WHERE category_id = p_Parts_On_Fly_Rec.Category_ID;

         l_item_rec.attribute_category := 'WC';
      END IF;

      -- Call Create_Item API from INV_ITEM_GRP API
      XXWC_GEN_ROUTINES_PKG.LOG_MSG (
         p_debug_level   => G_LEVEL_STATEMENT,
         p_mod_name      => l_Module_Name,
         P_DEBUG_MSG     => '1010 : Before Calling Create_Item API');
      INV_ITEM_GRP.Create_Item (
         P_Commit             => fnd_api.g_true,
         P_Validation_Level   => fnd_api.g_valid_level_full,
         P_Item_Rec           => l_Item_Rec,
         p_template_id        => p_Parts_On_Fly_Rec.ITEM_TEMPLATE_ID,
         X_Item_Rec           => lx_item_Rec,
         X_Return_Status      => lx_Return_Status,
         X_Error_TBL          => lx_error_tbl);


      IF (lx_return_status = 'S')
      THEN
         -- Commit Item Creation API
         -- 11-JUN-2012 : Assign Inventory Item Id to variable
         l_Inventory_Item_ID := lx_Item_Rec.Inventory_Item_ID;
         COMMIT;
         XXWC_GEN_ROUTINES_PKG.LOG_MSG (
            p_debug_level   => G_LEVEL_EVENT,
            p_mod_name      => l_Module_Name,
            P_DEBUG_MSG     => 'Procedure INV_ITEM_GRP.Create_Item processed successfully');
      ELSE
         --x_Return_Status := 'E' ;

         x_msg_Data := 'Error creating item...' || CHR (13) || CHR (10);

         FOR i IN 1 .. lx_error_tbl.COUNT
         LOOP
            lx_Msg_Data :=
                  x_msg_Data
               || lx_error_tbl (i).message_name
               || ' - '
               || lx_error_tbl (i).MESSAGE_TEXT
               || CHR (13)
               || CHR (10);
         END LOOP;

         XXWC_GEN_ROUTINES_PKG.LOG_MSG (p_debug_level   => G_LEVEL_ERROR,
                                        p_mod_name      => l_Module_Name,
                                        P_DEBUG_MSG     => lx_Msg_Data);
         -- Exit the Process
         x_Return_Status := 'E';
         x_Msg_Data := lx_Msg_Data;
         RETURN;
      END IF;

      -- Assign Item To Item Category
      IF p_Parts_On_Fly_Rec.Category_ID IS NOT NULL
      THEN
         XXWC_GEN_ROUTINES_PKG.LOG_MSG (
            p_debug_level   => G_LEVEL_PROCEDURE,
            p_mod_name      => l_Module_Name,
            P_DEBUG_MSG     => '1010.100 : Before Calling Assign Category API');

         -- Initialize Out variables  15-Feb-2012
         lx_Return_Status := NULL;
         lx_Msg_Data := NULL;
         lx_Msg_Count := NULL;

         XXWC_INV_PARTS_ON_FLY_PKG.ASSIGN_CATEGORY (
            p_category_id         => p_Parts_On_Fly_Rec.Category_ID,
            p_organization_id     => p_Parts_On_Fly_Rec.Mst_Organization_ID,
            p_Inventory_Item_ID   => lx_item_Rec.INVENTORY_ITEM_ID,
            x_Return_Status       => lx_Return_Status,
            x_Msg_Data            => lx_Msg_Data,
            x_Msg_Count           => lx_Msg_Count);

         IF lx_Return_status = Fnd_API.G_RET_STS_SUCCESS
         THEN
            XXWC_GEN_ROUTINES_PKG.LOG_MSG (
               p_debug_level   => G_LEVEL_PROCEDURE,
               p_mod_name      => l_Module_Name,
               P_DEBUG_MSG     => 'Assign Category call is successful');
         ELSE
            XXWC_GEN_ROUTINES_PKG.LOG_MSG (
               p_debug_level   => G_LEVEL_ERROR,
               p_mod_name      => l_Module_Name,
               P_DEBUG_MSG     =>    'Assign Category API errored out :'
                                  || lx_MSg_Data);
            lX_Msg_Data :=
                  lx_Msg_Data
               || 'Error creating item Cost for master Organization...'
               || CHR (13)
               || CHR (10);

            IF lx_msg_Count > 1
            THEN
               FOR i IN 1 .. lx_msg_count
               LOOP
                  l_msg_data :=
                     fnd_msg_pub.get (p_msg_index => i, p_encoded => 'F');
                  lx_Msg_Data := lx_Msg_Data || l_Msg_Data;
               END LOOP;
            END IF;

            x_Return_Status := 'E';
            x_Msg_Data := x_Msg_Data || lx_Msg_Data;
         END IF;
      ELSE
         x_Msg_Data :=
               x_Msg_Data
            || 'Item Category Information is not Provided'
            || CHR (13)
            || CHR (10);
         XXWC_GEN_ROUTINES_PKG.LOG_MSG (
            p_debug_level   => G_LEVEL_STATEMENT,
            p_mod_name      => l_Module_Name,
            P_DEBUG_MSG     => 'Item Category Information is not provided');
      END IF;


      -- Create Item Cost for Master Organization
      -- Create Item Cost only When User Enters Item COst on Custom UI
      -- Satish U: 10/07/2011
      XXWC_GEN_ROUTINES_PKG.LOG_MSG (
         p_debug_level   => G_LEVEL_STATEMENT,
         p_mod_name      => l_Module_Name,
         P_DEBUG_MSG     => '1030 : Before Calling Create_Item_Cost API');

      -- Assign Item to Assignment ORganization
      -- Satish U : 10/13/2011 : Call This API only When ASSGN_ORGANIZATION_ID has a Value
      XXWC_GEN_ROUTINES_PKG.LOG_MSG (
         p_debug_level   => G_LEVEL_STATEMENT,
         p_mod_name      => l_Module_Name,
         P_DEBUG_MSG     => '1040 : Before Calling Asgign_Item_To_Org API');

      -- Satish U: 13-MAR-2012 :
      FOR Inv_Orgs_Rec IN Inv_Orgs_Cur
      LOOP
         --If P_Parts_ON_Fly_Rec.ASSGN_ORGANIZATION_ID Is Not Null Then
         -- Initialize Out variables  15-Feb-2012
         lx_Return_Status := NULL;
         lx_Msg_Count := NULL;
         EGO_ITEM_PUB.Assign_Item_To_Org (
            p_api_version         => 1.0,
            p_init_msg_list       => FND_API.G_FALSE,
            p_commit              => FND_API.G_TRUE,
            p_Inventory_Item_Id   => lx_item_Rec.INVENTORY_ITEM_ID,
            p_Item_Number         => NULL,
            p_Organization_Id     => Inv_Orgs_Rec.Organization_ID ---P_Parts_ON_Fly_Rec.ASSGN_ORGANIZATION_ID
                                                                 ,
            p_Organization_Code   => NULL,
            p_Primary_Uom_Code    => P_Parts_ON_Fly_Rec.PRIMARY_UOM_CODE,
            x_return_status       => lx_return_status,
            x_msg_count           => lx_msg_count);

         IF (lx_return_status = 'S')
         THEN
            -- Added by Shankar 07-Aug-2012
            INSERT INTO mtl_item_sub_defaults (inventory_item_id,
                                               organization_id,
                                               subinventory_code,
                                               default_type,
                                               last_update_date,
                                               last_updated_by,
                                               creation_date,
                                               created_by,
                                               last_update_login)
                 VALUES (lx_item_Rec.INVENTORY_ITEM_ID,
                         Inv_Orgs_Rec.Organization_ID,
                         'General',
                         2,                                        --receiving
                         SYSDATE,
                         fnd_global.user_id,
                         SYSDATE,
                         fnd_global.user_id,
                         -1);

            -- Commit Item Creation API
            COMMIT;



            XXWC_GEN_ROUTINES_PKG.LOG_MSG (
               p_debug_level   => G_LEVEL_EVENT,
               p_mod_name      => l_Module_Name,
               P_DEBUG_MSG     => 'Procedure EGO_ITEM_PUB.Assign_Item_To_Org processed successfully');
         ELSE
            X_Msg_Data :=
                  'Error Assigning item to assignment organization'
               || CHR (13)
               || CHR (10);
            XXWC_GEN_ROUTINES_PKG.LOG_MSG (p_debug_level   => G_LEVEL_ERROR,
                                           p_mod_name      => l_Module_Name,
                                           P_DEBUG_MSG     => lx_Msg_Data);

            IF lx_msg_Count >= 1
            THEN
               lx_Msg_Data := ' ';

               FOR i IN 1 .. lx_msg_count
               LOOP
                  l_msg_data :=
                     fnd_msg_pub.get (p_msg_index => i, p_encoded => 'F');
                  lx_Msg_Data := lx_Msg_Data || l_Msg_Data;
               END LOOP;
            END IF;

            x_Return_Status := 'E';
            x_Msg_Data := x_Msg_Data || lx_Msg_Data;
            RETURN;
         END IF;

         -------- Item Category Code is moved up righr after Assignemtn of item


         --Iniitialize


         --------------------
         l_Item_Rec := lg_Miss_Item_rec;
         --lx_item_Rec  :=  Inv_Item_Grp.Item_rec_Type;
         --lx_error_TBL :=  Inv_Item_Grp.Error_Tbl_Type;

         l_Item_Rec.Inventory_Item_ID := l_Inventory_Item_ID;
         l_Item_Rec.Organization_ID := Inv_Orgs_Rec.Organization_ID;

         l_Item_Rec.COST_OF_SALES_ACCOUNT :=
            XXWC_INV_NEW_PROD_REQ_PKG.get_gl_account (
               i_inventory_item_id   => l_Inventory_Item_ID,
               i_organization_id     => Inv_Orgs_Rec.Organization_ID,
               i_acc_type            => 'C');

         l_Item_Rec.SALES_ACCOUNT :=
            XXWC_INV_NEW_PROD_REQ_PKG.get_gl_account (
               i_inventory_item_id   => l_Inventory_Item_ID,
               i_organization_id     => Inv_Orgs_Rec.Organization_ID,
               i_acc_type            => 'S');

        -- Added below code in Rev 1.3		
		l_Item_Rec.EXPENSE_ACCOUNT :=
            XXWC_INV_NEW_PROD_REQ_PKG.get_gl_account (
               i_inventory_item_id   => l_Inventory_Item_ID,
               i_organization_id     => Inv_Orgs_Rec.Organization_ID,
               i_acc_type            => 'E');

			   
			   
			   
         -- Satish U: 11-JUN-2012 :  Update Sales Account and Cost of Sales Account for new item created.
         INV_ITEM_GRP.Update_Item (
            P_Commit             => fnd_api.g_true,
            p_lock_rows          => fnd_api.g_TRUE,
            p_validation_level   => fnd_api.g_VALID_LEVEL_FULL,
            p_Item_rec           => l_Item_Rec,
            x_Item_rec           => lx_item_Rec,
            x_return_status      => lx_Return_Status,
            x_Error_tbl          => lx_error_tbl,
            p_Template_Id        => NULL,
            p_Template_Name      => NULL);

         IF (lx_return_status = 'S')
         THEN
            -- Commit Item Creation API
            COMMIT;
            XXWC_GEN_ROUTINES_PKG.LOG_MSG (
               p_debug_level   => G_LEVEL_EVENT,
               p_mod_name      => l_Module_Name,
               P_DEBUG_MSG     => 'Procedure INV_ITEM_GRP.Update_Item processed successfully');
         ELSE
            X_Msg_Data :=
                  'Error assigning Sales and Cost of Sales accounts using INV_ITEM_GRP.Update_Item'
               || CHR (13)
               || CHR (10);
            XXWC_GEN_ROUTINES_PKG.LOG_MSG (p_debug_level   => G_LEVEL_ERROR,
                                           p_mod_name      => l_Module_Name,
                                           P_DEBUG_MSG     => lx_Msg_Data);

            IF lx_msg_Count >= 1
            THEN
               lx_Msg_Data := ' ';

               FOR i IN 1 .. lx_msg_count
               LOOP
                  l_msg_data :=
                     fnd_msg_pub.get (p_msg_index => i, p_encoded => 'F');
                  lx_Msg_Data := lx_Msg_Data || l_Msg_Data;
               END LOOP;
            END IF;

            x_Return_Status := 'E';
            x_Msg_Data := x_Msg_Data || lx_Msg_Data;

            XXWC_GEN_ROUTINES_PKG.LOG_MSG (p_debug_level   => G_LEVEL_ERROR,
                                           p_mod_name      => l_Module_Name,
                                           P_DEBUG_MSG     => lx_Msg_Data);
            RETURN;
         END IF;

         -- Create Item Cost for Master Organization
         -- Create Item Cost only when Item Cost is provided by User on UI
         XXWC_GEN_ROUTINES_PKG.LOG_MSG (
            p_debug_level   => G_LEVEL_STATEMENT,
            p_mod_name      => l_Module_Name,
            P_DEBUG_MSG     => '1050 : Before Calling Create Item Cost API');
         -- Satish U: 21-MAR-2012  :  No need to Create Item Cost

         COMMIT;
      END LOOP;

      --End If;
      -- Create Manufacturer Part Number
      -- Added THis Code 0n 10/07/2011
      -- Manufracturer Party Number is nOt provided then Do not Call Create Cross Reference API
      IF p_Parts_On_Fly_Rec.Manufacturer_Part_Number IS NOT NULL
      THEN
         XXWC_GEN_ROUTINES_PKG.LOG_MSG (
            p_debug_level   => G_LEVEL_STATEMENT,
            p_mod_name      => l_Module_Name,
            P_DEBUG_MSG     => '1070 : Before Calling Create Cross Reference API');

         XXWC_INV_PARTS_ON_FLY_PKG.CREATE_CROSS_REF (
            p_segment1               => p_Parts_On_Fly_Rec.Item_Number,
            p_cross_reference_type   => C_CROSS_REFERENCE_TYPE,
            p_cross_reference        => p_Parts_On_Fly_Rec.Manufacturer_Part_Number,
            p_description            => p_Parts_On_Fly_Rec.Item_Description);

         XXWC_GEN_ROUTINES_PKG.LOG_MSG (
            p_debug_level   => G_LEVEL_STATEMENT,
            p_mod_name      => l_Module_Name,
            P_DEBUG_MSG     => 'After Call to API CREATE_CROSS_REF');
      END IF;

      -- Assing Item To Price List :
      -- Create Price List when List Price Value is Not Null -- Satish U : 10/07/2011
      IF p_Parts_On_Fly_Rec.list_price IS NOT NULL
      THEN
         XXWC_GEN_ROUTINES_PKG.LOG_MSG (
            p_debug_level   => G_LEVEL_STATEMENT,
            p_mod_name      => l_Module_Name,
            P_DEBUG_MSG     => '1080 : Before Calling Assign To Price List API');
         Assign_to_price_list (
            p_inventory_item_id     => lx_item_Rec.INVENTORY_ITEM_ID, --p_inventory_item_id
            P_List_Header_ID        => p_Parts_On_Fly_Rec.Price_List_Header_ID,
            p_list_price_per_unit   => p_Parts_On_Fly_Rec.list_price --p_list_price_per_unit
                                                                    ,
            p_primary_uom_code      => p_Parts_On_Fly_Rec.primary_uom_code); --p_primary_uom_code

         XXWC_GEN_ROUTINES_PKG.LOG_MSG (
            p_debug_level   => G_LEVEL_STATEMENT,
            p_mod_name      => l_Module_Name,
            P_DEBUG_MSG     => 'After Call to API ASSIGN_TO_PRICE_LIST');
      END IF;

      -- 07/08/2013 CG TMS 20130107-00832: Added to be able to add new specials to market setup
      IF fnd_profile.VALUE ('XXWC_QP_MARKET_NTL_SETUP_PRL') IS NOT NULL
      THEN
         XXWC_GEN_ROUTINES_PKG.LOG_MSG (
            p_debug_level   => G_LEVEL_STATEMENT,
            p_mod_name      => l_Module_Name,
            P_DEBUG_MSG     => '1090 : Before Calling Assign To Price List API for Ntl Setup Prl');

         Assign_to_price_list (
            p_inventory_item_id     => lx_item_Rec.INVENTORY_ITEM_ID, --p_inventory_item_id
            P_List_Header_ID        => fnd_profile.VALUE (
                                         'XXWC_QP_MARKET_NTL_SETUP_PRL'),
            p_list_price_per_unit   => p_Parts_On_Fly_Rec.list_price, --p_list_price_per_unit
            p_primary_uom_code      => p_Parts_On_Fly_Rec.primary_uom_code); --p_primary_uom_code

         XXWC_GEN_ROUTINES_PKG.LOG_MSG (
            p_debug_level   => G_LEVEL_STATEMENT,
            p_mod_name      => l_Module_Name,
            P_DEBUG_MSG     => 'After Call to API ASSIGN_TO_PRICE_LIST for Ntl Setup Prl');
      END IF;

      -- 07/08/2013 CG TMS 20130107-00832


      -- Create Sourcing Rules
      -- Creating Sourcing Rules only Vendor ID Is Not Null
      -- Commented by Shankar -- No need to create sourcing rules -- CP 983 - 04-OCT-2012
      /*
      If p_Parts_On_Fly_Rec.Vendor_SIte_ID Is Not Null THen
         XXWC_GEN_ROUTINES_PKG.LOG_MSG (p_debug_level   => G_LEVEL_PROCEDURE,
                                     p_mod_name      => l_Module_Name,
                                     P_DEBUG_MSG     => '1100 : Before Calling Assign Sourcing Rule API');
         -- Initialize Out variables  15-Feb-2012
         lx_Return_Status := Null;
         lx_Msg_Data      := Null;
         lx_Msg_Count     := Null;

         XXWC_INV_PARTS_ON_FLY_PKG.ASSIGN_SOURCING_RULE (
            p_INVENTORY_ITEM_ID  => lx_item_Rec.INVENTORY_ITEM_ID
           ,p_vendor_id          => p_Parts_On_Fly_Rec.Vendor_ID
           ,p_vendor_site_id     => p_Parts_On_Fly_Rec.Vendor_SIte_ID
           ,p_Organization_ID    => p_Parts_On_Fly_Rec.ASSGN_Organization_ID
           ,X_Return_Status      => lx_Return_Status
           ,x_Msg_Data           => lx_Msg_Data
           ,x_Msg_Count          => lx_Msg_Count );

         If  lx_Return_status = Fnd_API.G_RET_STS_SUCCESS THen
             XXWC_GEN_ROUTINES_PKG.LOG_MSG (p_debug_level   => G_LEVEL_PROCEDURE,
                                        p_mod_name      => l_Module_Name,
                                        P_DEBUG_MSG     => 'Assign Sourcing Rule API call is successful');
         Else
            XXWC_GEN_ROUTINES_PKG.LOG_MSG (p_debug_level   => G_LEVEL_ERROR,
                                           p_mod_name      => l_Module_Name,
                                           P_DEBUG_MSG     => 'Assign Sourcing Rule errored out :' || lx_MSg_Data );
            lX_Msg_Data :=  lx_Msg_Data || 'Error Assigning Sourcing Rules...'||CHR(13) || CHR(10) ;
             IF lx_msg_Count > 1 THen
                 FOR i IN 1 .. lx_msg_count
                 LOOP
                    l_msg_data := fnd_msg_pub.get (p_msg_index => i, p_encoded => 'F');
                    lx_Msg_Data := lx_Msg_Data || l_Msg_Data ;
                 END LOOP;
             End If;
            x_Return_Status := 'E' ;
            x_Msg_Data      := x_Msg_Data || lx_Msg_Data ;
         End IF;
      End If;

      */
      --G_ITEM_NUMBER       := p_Parts_On_Fly_Rec.ITEM_NUMBER;
      p_Parts_On_Fly_Rec.INVENTORY_ITEM_ID := lx_item_Rec.INVENTORY_ITEM_ID;

      IF p_Parts_On_Fly_Rec.Radio_Group_Value = 'P'
      THEN
         XXWC_GEN_ROUTINES_PKG.LOG_MSG (
            p_debug_level   => G_LEVEL_STATEMENT,
            p_mod_name      => l_Module_Name,
            P_DEBUG_MSG     => 'Assigning Item Number to Profile Options');

         IF FND_GLOBAL.APPLICATION_SHORT_NAME = 'ONT'
         THEN
            SET_ITEM_NUMBER (
               P_TRANSACTION_ID      => p_Parts_On_Fly_Rec.Transaction_ID,
               P_TRANSACTION_TYPE    => G_TRX_TYPE_PARTS_ON_FLY_SO,
               P_INVENTORY_ITEM_ID   => p_Parts_On_Fly_Rec.INVENTORY_ITEM_ID,
               P_ITEM_NUMBER         => p_Parts_On_Fly_Rec.ITEM_NUMBER,
               P_ORGANIZATION_ID     => p_Parts_On_Fly_Rec.ASSGN_ORGANIZATION_ID,
               P_ORGANIZATION_CODE   => p_Parts_On_Fly_Rec.ORGANIZATION_CODE);
         ELSIF FND_GLOBAL.APPLICATION_SHORT_NAME = 'CSD'
         THEN
            NULL;
         END IF;
      END IF;

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         X_Return_Status := Fnd_API.G_RET_STS_ERROR;
         X_Msg_Data := X_Msg_Data || 'When Others Error :' || SQLERRM;
         XXWC_GEN_ROUTINES_PKG.LOG_MSG (p_debug_level   => G_LEVEL_PROCEDURE,
                                        p_mod_name      => l_Module_Name,
                                        P_DEBUG_MSG     => lx_Msg_Data);
   END CREATE_ITEM;

   --*****************************************************************************************
   -- Program to Inactivate  Items whose ITEM_TYPE IS SPECIALS and that are created 90 ( P_DAYS_AFTER_CREATION )
   --*****************************************************************************************

   PROCEDURE Inactivate_Items (errbuf                     OUT VARCHAR2,
                               retcode                    OUT NUMBER,
                               P_DAYS_AFTER_CREATION   IN     NUMBER)
   IS
      l_Master_Organization_ID               NUMBER
         := Fnd_Profile.VALUE ('XXWC_ITEM_MASTER_ORG');

      C_ITEM_TYPE_SPECIAL           CONSTANT VARCHAR2 (30) := 'SPECIAL';
      C_ITEM_STATUS_CODE_ACTIVE     CONSTANT VARCHAR2 (30) := 'Active';
      C_ITEM_STATUS_CODE_INACTIVE   CONSTANT VARCHAR2 (30) := 'Inactive';

      l_Item_Rec                             INV_ITEM_GRP.Item_rec_type;
      lx_Item_Rec                            INV_ITEM_GRP.Item_rec_type;
      l_Return_Status                        VARCHAR2 (1);
      l_Error_Tbl                            INV_ITEM_GRP.Error_tbl_type;
      l_Template_ID                          NUMBER;
      l_Template_Name                        VARCHAR2 (30);
      l_Module_Name                          VARCHAR2 (30)
                                                := '.Inactivate_Items';
      x_MSg_Data                             VARCHAR2 (2000);
      lx_Msg_Data                            VARCHAR2 (2000);
      l_Success_Count                        NUMBER := 0;
      l_Failure_Count                        NUMBER := 0;

      -- Define a Cursor to get list of items
      -- Satish U: 30-MAY-2012 : Commented RownUm < 3
      CURSOR Get_Items_Cur
      IS
           SELECT msi.Inventory_Item_Id,
                  msi.Segment1,
                  mp.Organization_id,
                  mp.Organization_code
             FROM MTL_SYSTEM_ITEMS_B msi, MTL_PARAMETERS MP
            WHERE     msi.Item_Type = C_ITEM_TYPE_SPECIAL
                  AND MP.Master_Organization_ID = l_Master_Organization_ID
                  AND MP.Organization_ID = MSI.Organization_ID
                  AND TRUNC (MSI.Creation_Date) >=
                         TRUNC (SYSDATE - NVL (P_DAYS_AFTER_CREATION, 0))
                  AND MSI.INVENTORY_ITEM_STATUS_CODE =
                         C_ITEM_STATUS_CODE_ACTIVE
                  AND NVL (MSI.Attribute29, 'N') <> 'Y'
         ORDER BY msi.Inventory_Item_Id, mp.Organization_id;

      --AND   Rownum < 3
      --AND   msi.Segment1 = 'CFADALV3495959'


      -- Define Record Structures for Success and Failure
      TYPE Item_Success_Type IS RECORD
      (
         ORGANIZATION_CODE   VARCHAR2 (3),
         Item_Number         VARCHAR2 (40)
      );

      -- Define Record Structures for Success and Failure
      TYPE Item_Failure_Type IS RECORD
      (
         ORGANIZATION_CODE   VARCHAR2 (3),
         Item_Number         VARCHAR2 (40),
         Error_Message       VARCHAR2 (40)
      );

      TYPE Item_Success_tbl_type IS TABLE OF Item_Success_Type
         INDEX BY BINARY_INTEGER;

      TYPE Item_Failure_Tbl_Type IS TABLE OF Item_Failure_Type
         INDEX BY BINARY_INTEGER;

      l_err_callpoint                        VARCHAR2 (175) := 'START';
      l_distro_list                          VARCHAR2 (80)
         := 'OracleDevelopmentGroup@hdsupply.com';

      l_Item_Success_tbl                     Item_Success_tbl_type;
      l_Item_Failure_Tbl                     Item_Failure_Tbl_Type;
   BEGIN
      XXWC_GEN_ROUTINES_PKG.LOG_MSG (
         p_debug_level   => G_LEVEL_PROCEDURE,
         p_mod_name      => l_Module_Name,
         P_DEBUG_MSG     => '10001 : At begining of API');

      FOR Item_Rec IN Get_Items_Cur
      LOOP
         FND_MESSAGE.CLEAR;

         BEGIN
            -- Initialize Variables
            l_Template_ID := NULL;
            l_Template_Name := NULL;
            --l_Error_Tbl     := NULL;
            l_Item_Rec := INV_ITEM_GRP.g_Miss_Item_rec;
            lx_Item_Rec := NULL;
            l_Return_Status := 'S';

            l_Item_Rec.ORGANIZATION_ID := Item_Rec.ORGANIZATION_ID;
            l_Item_Rec.ORGANIZATION_CODE := Item_Rec.ORGANIZATION_CODE;
            l_Item_Rec.INVENTORY_ITEM_ID := Item_Rec.INVENTORY_ITEM_ID;
            l_Item_Rec.INVENTORY_ITEM_STATUS_CODE :=
               C_ITEM_STATUS_CODE_INACTIVE;

            -- Initialize  Item Record

            INV_ITEM_GRP.Update_Item (
               p_commit             => fnd_api.g_TRUE,
               p_lock_rows          => fnd_api.g_TRUE,
               p_validation_level   => fnd_api.g_VALID_LEVEL_FULL,
               p_Item_rec           => l_Item_Rec,
               x_Item_rec           => lx_Item_Rec,
               x_return_status      => l_Return_Status,
               x_Error_tbl          => l_Error_Tbl,
               p_Template_Id        => l_Template_ID,
               p_Template_Name      => l_Template_Name);

            IF (l_return_status = 'S')
            THEN
               l_Success_Count := l_Success_Count + 1;
               l_Item_Success_tbl (l_Success_Count).ORGANIZATION_CODE :=
                  Item_Rec.Organization_Code;
               l_Item_Success_tbl (l_Success_Count).Item_Number :=
                  Item_Rec.Segment1;

               -- Commit Item Creation API
               --COMMIT;
               XXWC_GEN_ROUTINES_PKG.LOG_MSG (
                  p_debug_level   => G_LEVEL_EVENT,
                  p_mod_name      => l_Module_Name,
                  P_DEBUG_MSG     =>    'Procedure INV_ITEM_GRP.Update_Item processed successfully for Item '
                                     || Item_Rec.Segment1
                                     || ' for warehouse '
                                     || Item_Rec.Organization_Code);
            ELSE
               --x_Return_Status := 'E' ;
               retcode := 1;
               x_msg_Data :=
                     'Error Updating item...'
                  || Item_Rec.Segment1
                  || ' for warehouse '
                  || Item_Rec.Organization_Code
                  || CHR (13)
                  || CHR (10);

               FOR i IN 1 .. l_error_tbl.COUNT
               LOOP
                  lx_Msg_Data :=
                        x_msg_Data
                     || l_error_tbl (i).message_name
                     || ' - '
                     || l_error_tbl (i).MESSAGE_TEXT
                     || CHR (13)
                     || CHR (10);
               END LOOP;

               XXWC_GEN_ROUTINES_PKG.LOG_MSG (
                  p_debug_level   => G_LEVEL_ERROR,
                  p_mod_name      => l_Module_Name,
                  P_DEBUG_MSG     => lx_Msg_Data);
               x_Msg_Data := lx_Msg_Data;
               l_Failure_Count := l_Failure_Count + 1;
               l_Item_Success_tbl (l_Failure_Count).ORGANIZATION_CODE :=
                  Item_Rec.Organization_Code;
               l_Item_Success_tbl (l_Failure_Count).Item_Number :=
                  Item_Rec.Segment1;
            END IF;
         EXCEPTION
            WHEN OTHERS
            THEN
               retcode := 1;
               XXWC_GEN_ROUTINES_PKG.LOG_MSG (
                  p_debug_level   => G_LEVEL_ERROR,
                  p_mod_name      => l_Module_Name,
                  P_DEBUG_MSG     => '10200 :' || SQLERRM);
         END;
      END LOOP;

      --- Print Success Records to Output File
      IF l_Success_Count > 0
      THEN
         /*Error Report printing Statememnts*/
         fnd_file.put_line (fnd_file.output, '<HTML><BODY>');
         fnd_file.put_line (fnd_file.output, '<PRE>');
         fnd_file.new_line (fnd_file.output, 1);
         fnd_file.put_line (
            fnd_file.output,
            '********************************************************************************************<BR>');
         fnd_file.put_line (
            fnd_file.output,
            '<H3>                List of Special Items that are successfully inactivated                     </H3>');
         fnd_file.put_line (
            fnd_file.output,
            '********************************************************************************************<BR>');
         fnd_file.put_line (
            fnd_file.output,
               '<H4> 1. Program Name           : '
            || ' XXWC INV Incativate Special Items ');
         fnd_file.put_line (
            fnd_file.output,
               ' 2. Start Date             :  '
            || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));
         fnd_file.put_line (fnd_file.output, '</H4>');
         fnd_file.put_line (
            fnd_file.output,
            '********************************************************************************************<BR>');
         fnd_file.put_line (fnd_file.output, '<BR>');
         fnd_file.put_line (fnd_file.output, '</PRE>');
         fnd_file.put_line (fnd_file.output, '<TABLE BORDER=1>');
         fnd_file.put_line (
            fnd_file.output,
            '<TR><TH> S.No </TH><TH>Item Number # </TH><TH>Organization Code # </TH> </TR>');

         FOR i IN 1 .. l_Success_Count
         LOOP
            fnd_file.put_line (
               fnd_file.output,
                  '<TR><TD>'
               || i
               || '</TD><TD>'
               || l_Item_Success_tbl (i).Item_Number
               || '</TD><TD>'
               || l_Item_Success_tbl (i).ORGANIZATION_CODE
               || '</TD></TR>');
         END LOOP;

         fnd_file.put_line (fnd_file.output, '</TABLE>');

         IF l_Failure_Count > 0
         THEN
            fnd_file.put_line (fnd_file.output, '</BODY>');
         ELSE
            fnd_file.put_line (fnd_file.output, '</BODY></HTML>');
         END IF;
      END IF;

      -- Print Failure Record to Output FIle
      IF l_Failure_Count > 0
      THEN
         /*Error Report printing Statememnts*/
         IF l_Success_Count = 0
         THEN
            fnd_file.put_line (fnd_file.output, '<HTML><BODY>');
         ELSE
            fnd_file.put_line (fnd_file.output, '<BODY>');
         END IF;

         fnd_file.put_line (fnd_file.output, '<PRE>');
         fnd_file.new_line (fnd_file.output, 1);
         fnd_file.put_line (
            fnd_file.output,
            '********************************************************************************************<BR>');
         fnd_file.put_line (
            fnd_file.output,
            '<H3>                List of Special Items that failed to be inactivated                     </H3>');
         fnd_file.put_line (
            fnd_file.output,
            '********************************************************************************************<BR>');
         fnd_file.put_line (
            fnd_file.output,
               '<H4> 1. Program Name           : '
            || ' XXWC INV Incativate Special Items ');
         fnd_file.put_line (
            fnd_file.output,
               ' 2. Start Date             :  '
            || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));
         fnd_file.put_line (fnd_file.output, '</H4>');
         fnd_file.put_line (
            fnd_file.output,
            '********************************************************************************************<BR>');
         fnd_file.put_line (fnd_file.output, '<BR>');
         fnd_file.put_line (fnd_file.output, '</PRE>');
         fnd_file.put_line (fnd_file.output, '<TABLE BORDER=1>');
         fnd_file.put_line (
            fnd_file.output,
            '<TR><TH> S.No </TH><TH>Item Number # </TH><TH>Organization Code # </TH> </TR>');

         FOR i IN 1 .. l_Failure_Count
         LOOP
            fnd_file.put_line (
               fnd_file.output,
                  '<TR><TD>'
               || i
               || '</TD><TD>'
               || l_Item_Success_tbl (i).Item_Number
               || '</TD><TD>'
               || l_Item_Success_tbl (i).ORGANIZATION_CODE
               || '</TD></TR>');
         END LOOP;

         fnd_file.put_line (fnd_file.output, '</TABLE>');
         fnd_file.put_line (fnd_file.output, '</BODY></HTML>');
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         retcode := 1;
         XXWC_GEN_ROUTINES_PKG.LOG_MSG (
            p_debug_level   => G_LEVEL_ERROR,
            p_mod_name      => l_Module_Name,
            P_DEBUG_MSG     => '1030 :' || SQLERRM);
         l_err_callpoint := '210 :  When Others Exception :';

         Fnd_File.Put_Line (Fnd_File.LOG,
                            'WHen Others Exception :' || SQLERRM);

         errbuf := SQLERRM;
         retcode := 2;
         ROLLBACK;

         FND_FILE.Put_Line (FND_FILE.LOG, SQLERRM);
         FND_FILE.Put_Line (FND_FILE.Output, SQLERRM);

         XXCUS_ERROR_PKG.XXCUS_ERROR_MAIN_API (
            p_called_from         => G_MODULE_NAME,
            p_calling             => l_err_callpoint,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          => 'When Others Exception XXWC_INV_SALES_VELOCITY_PKG.MAIN',
            p_distribution_list   => l_distro_list,
            p_module              => 'INV');
   END Inactivate_Items;

   --*****************************************************************************************
   -- Program to Move items from Manufacturer Cross Reference to Vendor Cross Reference
   --*****************************************************************************************
   PROCEDURE Update_Cross_Reference
   IS
      -- Cursor to go through all the Items that are currently assigned to Manufacturer Cross Reference Type
      CURSOR cur_mcr
      IS
         SELECT mcr.Cross_Reference_TYpe,
                msi.Segment1,
                msi.item_type,
                mcr.Inventory_Item_ID
           FROM mtl_Cross_references mcr, mtl_System_Items_b msi
          WHERE     mcr.Cross_Reference_Type = 'MANUFACTURER'
                AND mcr.organization_Id IS NULL
                AND mcr.inventory_item_Id = msi.inventory_Item_Id
                AND msi.Organization_ID =
                       Fnd_Profile.VALUE ('XXWC_ITEM_MASTER_ORG')
                AND msi.Item_Type IN ('SPECIAL', 'REPAIR');
   BEGIN
      FOR Rec_Mcr IN cur_mcr
      LOOP
         UPDATE mtl_Cross_references_B
            SET Cross_Reference_Type = 'VENDOR'
          WHERE     Cross_Reference_Type = Rec_Mcr.Cross_Reference_Type
                AND inventory_item_ID = Rec_Mcr.Inventory_Item_ID;
      END LOOP;

      COMMIT;
      DBMS_OUTPUT.Put_Line ('Item Cross References successfully updated:');
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.Put_Line ('When Others Errors :' || SQLERRM);
   END Update_Cross_Reference;

END XXWC_INV_PARTS_ON_FLY_PKG;
/
