--
-- XXWC_INV_CC_BLOCKOUT_DATES_PKG  (Package Body) 
--
CREATE OR REPLACE PACKAGE BODY APPS.XXWC_INV_CC_BLOCKOUT_DATES_PKG
AS
   /*************************************************************************
   *  Copyright (c) 2011 Lucidity Consulting Group
   *  All rights reserved.
   **************************************************************************
   *   $Header XXWC_INV_CC_BLOCKOUT_DATES_PKG.pks $
   *   Module Name: XXWC_INV_CC_BLOCKOUT_DATES_PKG.pks
   *
   *   PURPOSE:   This package is called by the concurrent program xxwc cyclecount
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        09/30/2011  Vivek Lakaman             Initial Version
   * ************************************************************************/

   /*************************************************************************
 *   Procedure : Write_Log
 *
 *   PURPOSE:   This procedure logs error message
 *   Parameter:
 *          IN
 *              p_debug_msg      -- Debug Message
 * ************************************************************************/
   -- add debug message to log table and concurrent log file
   PROCEDURE Write_Log (p_debug_level   IN NUMBER
                       ,p_mod_name      IN VARCHAR2
                       ,p_debug_msg     IN VARCHAR2)
   IS
      PRAGMA AUTONOMOUS_TRANSACTION;
   BEGIN
      --IF (p_debug_level >= FND_LOG.G_CURRENT_RUNTIME_LEVEL)
      --THEN
      -- Call Oracle FND_LOG API to insert the debug message
      FND_LOG.STRING (p_debug_level, p_mod_name, P_DEBUG_MSG);
      -- END IF;

      fnd_file.put_line (fnd_file.LOG, P_DEBUG_MSG);
      COMMIT;
   END Write_Log;

   /*************************************************************************
 *   Procedure : Write_Error
 *
 *   PURPOSE:   This procedure logs error message
 *   Parameter:
 *          IN
 *              p_debug_msg      -- Debug Message
 * ************************************************************************/

   --add message to concurrent output file
   PROCEDURE Write_Error (p_debug_msg IN VARCHAR2)
   IS
      l_req_id          NUMBER := fnd_global.conc_request_id;
      l_err_callfrom    VARCHAR2 (75)
                           DEFAULT 'XXWC_INV_CC_BLOCKOUT_DATES_PKG.submit';
      l_err_callpoint   VARCHAR2 (75) DEFAULT 'START';
      l_distro_list     VARCHAR2 (75)
                           DEFAULT 'HDSOracleDevelopers@hdsupply.com';
   BEGIN
      xxcus_error_pkg.xxcus_error_main_api (
         p_called_from         => l_err_callfrom
        ,p_calling             => l_err_callpoint
        ,p_request_id          => l_req_id
        ,p_ora_error_msg       => SUBSTR (p_debug_msg, 1, 2000)
        ,p_error_desc          => 'Error running xxwc_mstr_parts_excl_list_pkg with PROGRAM ERROR'
        ,p_distribution_list   => l_distro_list
        ,p_module              => 'AR');
   END Write_Error;

   /*************************************************************************
    *   Procedure : Write_output
    *
    *   PURPOSE:   This procedure logs message into concurrent output
    *   Parameter:
    *          IN
    *              p_debug_msg      -- Debug Message
    * ************************************************************************/
   PROCEDURE Write_output (p_debug_msg IN VARCHAR2)
   IS
   BEGIN
      fnd_file.put_line (fnd_file.output, p_debug_msg);
   END Write_output;


   PROCEDURE INSERT_ROW (PX_BLOCKOUT_DATE_ID       IN OUT NOCOPY NUMBER
                        ,P_INV_ORG_ID              IN            NUMBER
                        ,P_START_DATE              IN            DATE
                        ,P_END_DATE                IN            DATE
                        ,P_TYPE                    IN            VARCHAR2
                        ,P_OBJECT_VERSION_NUMBER   IN            NUMBER)
   IS
      l_mod_name   VARCHAR2 (100)
                      := 'xxwc_inv_cc_blockout_dates_pkg.insert_row';

      CURSOR C1
      IS
         SELECT XXWC_INV_CC_BLOCKOUT_DATE_S.NEXTVAL FROM DUAL;
   BEGIN
      IF    (PX_BLOCKOUT_DATE_ID IS NULL)
         OR (PX_BLOCKOUT_DATE_ID = FND_API.G_MISS_NUM)
      THEN
         OPEN C1;

         FETCH C1 INTO PX_BLOCKOUT_DATE_ID;

         CLOSE C1;
      END IF;

      Write_Log (g_level_statement
                ,l_mod_name
                ,'PX_BLOCKOUT_DATE_ID=' || PX_BLOCKOUT_DATE_ID);

      INSERT INTO XXWC_INV_CC_BLOCKOUT_DATES (BLOCKOUT_DATE_ID
                                             ,INV_ORG_ID
                                             ,START_DATE
                                             ,END_DATE
                                             ,TYPE
                                             ,LAST_UPDATE_DATE
                                             ,LAST_UPDATED_BY
                                             ,CREATION_DATE
                                             ,CREATED_BY
                                             ,LAST_UPDATE_LOGIN
                                             ,OBJECT_VERSION_NUMBER)
           VALUES (PX_BLOCKOUT_DATE_ID
                  ,P_INV_ORG_ID
                  ,P_START_DATE
                  ,P_END_DATE
                  ,P_TYPE
                  ,SYSDATE
                  ,                                        --LAST_UPDATE_DATE,
                   FND_GLOBAL.USER_ID
                  ,                                       --P_LAST_UPDATED_BY,
                   SYSDATE
                  ,                                         --P_CREATION_DATE,
                   FND_GLOBAL.USER_ID
                  ,                                            --P_CREATED_BY,
                   FND_GLOBAL.LOGIN_ID
                  ,                                     --P_LAST_UPDATE_LOGIN,
                   P_OBJECT_VERSION_NUMBER);
   END INSERT_ROW;

   PROCEDURE LOCK_ROW (P_BLOCKOUT_DATE_ID        IN NUMBER
                      ,P_OBJECT_VERSION_NUMBER   IN NUMBER)
   IS
      CURSOR c
      IS
             SELECT object_version_number
               FROM xxwc_inv_cc_blockout_dates
              WHERE blockout_date_id = p_blockout_date_id
         FOR UPDATE OF blockout_date_id NOWAIT;

      recinfo   c%ROWTYPE;
   BEGIN
      OPEN c;

      FETCH c INTO recinfo;

      IF (c%NOTFOUND)
      THEN
         CLOSE c;

         fnd_message.set_name ('FND', 'FORM_RECORD_DELETED');
         app_exception.raise_exception;
      END IF;

      CLOSE c;

      IF (recinfo.OBJECT_VERSION_NUMBER = P_OBJECT_VERSION_NUMBER)
      THEN
         NULL;
      ELSE
         fnd_message.set_name ('FND', 'FORM_RECORD_CHANGED');
         app_exception.raise_exception;
      END IF;
   END LOCK_ROW;


   PROCEDURE UPDATE_ROW (P_BLOCKOUT_DATE_ID        IN NUMBER
                        ,P_INV_ORG_ID              IN NUMBER
                        ,P_START_DATE              IN DATE
                        ,P_END_DATE                IN DATE
                        ,P_TYPE                    IN VARCHAR2
                        ,P_OBJECT_VERSION_NUMBER   IN NUMBER)
   IS
      l_mod_name   VARCHAR2 (100)
                      := 'xxwc_inv_cc_blockout_dates_pkg.update_row';
   BEGIN
      write_log (g_level_statement
                ,l_mod_name
                ,' p_blockout_date_id=' || p_blockout_date_id);

      UPDATE XXWC_INV_CC_BLOCKOUT_DATES
         SET INV_ORG_ID =
                DECODE (P_INV_ORG_ID
                       ,FND_API.G_MISS_NUM, INV_ORG_ID
                       ,P_INV_ORG_ID)
            ,START_DATE =
                DECODE (P_START_DATE
                       ,FND_API.G_MISS_DATE, START_DATE
                       ,P_START_DATE)
            ,END_DATE =
                DECODE (P_END_DATE
                       ,FND_API.G_MISS_DATE, END_DATE
                       ,P_END_DATE)
            ,TYPE = DECODE (P_TYPE, FND_API.G_MISS_CHAR, TYPE, P_TYPE)
            ,LAST_UPDATE_DATE = SYSDATE
            ,LAST_UPDATED_BY = fnd_global.user_id
            ,LAST_UPDATE_LOGIN = fnd_global.login_id
            ,OBJECT_VERSION_NUMBER =
                DECODE (P_OBJECT_VERSION_NUMBER
                       ,FND_API.G_MISS_NUM, OBJECT_VERSION_NUMBER
                       ,P_OBJECT_VERSION_NUMBER)
       WHERE blockout_date_id = p_blockout_date_id;
   END UPDATE_ROW;


   PROCEDURE DELETE_ROW (P_BLOCKOUT_DATE_ID IN NUMBER)
   IS
   BEGIN
      DELETE xxwc_inv_cc_blockout_dates
       WHERE blockout_date_id = p_blockout_date_id;
   END DELETE_ROW;
END XXWC_INV_CC_BLOCKOUT_DATES_PKG;
/

