CREATE OR REPLACE PACKAGE BODY APPS.xxcus_opn_utilities_pkg
 --
/**************************************************************************
 * PROGRAM NAME: XXCUS_OPN_UTILITIES_PKG.pkb
 * PROGRAM TYPE: PL/SQL Package Body
 * DESCRIPTION
 *  validate location code entered for HDS OPN Property Manager properties, building and land UI's
 *  APIs for GSC Property manager location entries
 * DEPENDENCIES
 *   None.
 *
 * CALLED BY Forms Personalization
 *   Form Location: $PN_TOP/forms/US/PNSULOCN.fmx
 *
 * LAST UPDATE DATE: 09/13/2014
 *
 * HISTORY
 * =======
 * 
 * Version ESMS          DATE         AUTHOR(S)           DESCRIPTION
 * ------- --------      -----------  ---------------     ------------------------------------
 * 1.0    263565         09/23/2014    Balaguru Seshadri   Created
 * 1.1    269017         01/21/2015    Balaguru Seshadri   Add logic change in the validate_loc_code procedure. 
 *************************************************************************/
 --
IS
/**************************************************************************
 *
 * PROCEDURE: print_log
 * DESCRIPTION: To print log messages either to the SQL PLUS session window or concurrent program log file 
 * PARAMETERS
 * ==========
 * NAME              TYPE     DESCRIPTION
.* ----------------- -------- ---------------------------------------------
 * p_message         IN       debug messages 
 * RETURN VALUE: Mentioned above
 * CALLED BY: Multiple
 *************************************************************************/
  PROCEDURE print_log(p_message in varchar2) IS
  BEGIN
   IF fnd_global.conc_request_id >0 THEN
    fnd_file.put_line(fnd_file.log, p_message);
   ELSE
    dbms_output.put_line(p_message);
   END IF;
  EXCEPTION
   WHEN OTHERS THEN
    FND_FILE.PUT_LINE(FND_FILE.LOG, 'Issue in print_log routine ='||sqlerrm);
  END print_log;
  --
 --
/**************************************************************************
 *
 * PROCEDURE: validate_loc_code
 * DESCRIPTION: To validate the location code entered and make sure it conforms to standards 
 * PARAMETERS
 * ==========
 *  NAME              TYPE     DESCRIPTION
.*  ----------------- -------- ---------------------------------------------
 *  p_property_id      IN     property_id
 *  p_loc_code         IN     user entered building or land id
 *  p_loc_type         IN     BUILDING OR LAND
 * RETURN VALUE: Building or Land ID
 * CALLED BY:  $PN_TOP/forms/US/PNSULOCN.fmx FORMS PERSONALIZATION
 *************************************************************************/ 
 -- 
  FUNCTION validate_loc_code
     (
       p_property_id  IN NUMBER
      ,p_loc_code     IN VARCHAR2
      ,p_loc_type     IN VARCHAR2
     ) RETURN VARCHAR2 IS
    --
      CURSOR get_prop IS
         SELECT property_code
           FROM pn_properties_v
          WHERE 1 = 1
            AND property_id =p_property_id;
    --
     v_prop_code         pn_properties_v.property_code%type;
     v_max_loc_code      pn_locations_all.location_code%type;
     --
     v_max_char          VARCHAR2(3) :=Null;
     v_new_loc_code      pn_locations_all.location_code%type;
    --
     v_letter_L  VARCHAR2(1) :=CHR(76); --Capital L --used for Land 
     v_letter_A  VARCHAR2(1) :=CHR(65); --Capital A --used for Buildings
    --
     v_sql_str   VARCHAR2(2000) :=Null; 
     v_new_char  VARCHAR2(3)    :=Null;
     --
     n_current_seq     NUMBER   :=Null;
     n_start_land_seq  NUMBER   :=1;  
     n_current_ascii   NUMBER   :=Null;
     v_entered_alphabet VARCHAR2(3) :=Null;
    --
   BEGIN
    begin 
     open get_prop;
     fetch get_prop into v_prop_code;
     close get_prop;
    exception
     when no_data_found then
      return Null;
     when others then
      return Null;
    end;
    --
    if (p_loc_type ='BUILDING' and v_prop_code is not null) then
     -- 
     -- get max building record entered for the property
     --
     begin  
      --
        select max(location_code)
        into   v_max_loc_code 
        from   pn_locations_all
        where  1 =1
          and  property_id               =p_property_id
          and  location_type_lookup_code =p_loc_type;
      --
       if (v_max_loc_code is not null) then
        --
        v_sql_str :='SELECT REGEXP_SUBSTR ('
                  ||''''
                  ||v_max_loc_code
                  ||''''
                  ||', '
                  ||''''
                  ||'[[:alpha:]]+$'
                  ||''''
                  ||', 1 )'
                  ||' FROM DUAL';
        --
           --
            begin 
             --
             execute immediate v_sql_str into v_max_char;
             --
             if (v_max_char is not null) then 
              --
                begin  
                 --
                 -- make sure there are no errors in getting the ascii value of a letter.
                 -- Example: we should get value 65 for capital A. If not there is something
                 -- wrong with the last location data stored.
                 --
                 n_current_ascii := ASCII ( UPPER ( v_max_char ) );
                 --
                 v_new_char := CHR ( n_current_ascii + 1 );                 
                 -- Since we are able to get the current ascii, lets move on to get the next letter
                 --
                 begin                                            
                  -- Take the current ascii and add one to it to get the next Capital Letter
                  -- Now that we have the new capital Letter, just concatenate with the property code and return back
                  --
                  if ( p_loc_code <>v_prop_code||v_new_char) then
                   -- 
                    v_new_loc_code  :=v_prop_code || v_new_char;
                   --
                  else
                   -- 
                    v_new_loc_code  :=p_loc_code;
                   --
                  end if;
                 exception
                  when others then
                   v_new_loc_code :=Null;
                 end;
                 --
                exception
                 when others then
                  v_new_loc_code :=Null;
                end;
              --
             else
              --
              if (v_max_loc_code =p_loc_code) then
               --
               v_new_loc_code :=Null;
               --
              else
               --
               v_new_loc_code :=p_loc_code||' is allowed only when an existing building is found with '||v_max_loc_code||v_Letter_A;               
               --
              end if;
              --
             end if;
             --
            exception
             when others then
              v_new_loc_code :=Null;
            end;
           --                  
        --
       else
         -- looks like this is the first building, so the only thing we can check at this point is if 
         -- the entered location code is same as property code or a property code with the letter capital A.
         --
         if (p_loc_code =v_prop_code OR p_loc_code =v_prop_code||v_letter_A) then
          --
          v_new_loc_code :=p_loc_code;
          --
         else
          --
          -- looks like the building code is not entered as per standard.
          --
          v_new_loc_code :=v_prop_code||' OR '||v_prop_code||v_letter_A;
         end if;
       end if;
      --
     exception
      when others then
       v_new_loc_code := Null;
     end;
     --
     return v_new_loc_code;
     --
    elsif (p_loc_type ='LAND' and v_prop_code is not null) then
     -- 
     -- get max land record entered for the property
     --
     begin  
      --
        select max(location_code)
        into   v_max_loc_code 
        from   pn_locations_all
        where  1 =1
          and  property_id               =p_property_id
          and  location_type_lookup_code =p_loc_type;
      --
       if (v_max_loc_code is not null) then
        --
        -- The carat ^ symbol means NOT condition
        -- In other words if we pass FL300L1 and property code FL300, we extract everything from FL300L1
        -- excluding the string that matches the property code and letter L combined and get the return value of "1"
        --
        v_sql_str :='SELECT REGEXP_SUBSTR ('
                  ||''''
                  ||v_max_loc_code
                  ||''''
                  ||', '
                  ||''''
                  ||'[[:digit:]]+$'
                  ||''''
                  ||', 1 )'
                  ||' FROM DUAL';
        --
           --
            begin  
             --
             -- make sure we can execute the above SQL and get the max number within the last land setup for property
             -- Example: For property FL300, if the last land setup was FL300L1, the dynamic SQL should return number 1
             -- into variable v_max_char
             --
             execute immediate v_sql_str into v_max_char;
             --
             if (v_max_char is not null) then 
              --
                begin  
                 --
                 -- make sure there are no errors in figuring out if the returned char is a number.
                 --If not there is something wrong with the last land id entered.
                 --
                 n_current_seq :=TO_NUMBER(v_max_char);
                 --
                 -- At this point we are sure we have a number extracted from the max land location code.
                 --
                 begin
                  --
                  -- Take the current number and add one to it to get the next land id and 
                  --return along the property code concatenated with letter L
                  --
                   v_new_loc_code  :=v_prop_code || v_letter_L || TO_CHAR (n_current_seq + 1);
                  --
                 exception
                  when others then
                   v_new_loc_code :=Null;
                 end;
                 --
                exception
                 when others then
                  -- if the code came thru this far then we have identified a non number in the last position after the letter L in a land id.
                  -- Example: The land id may be something like FL300La
                  v_new_loc_code :=Null;
                end;
              --
             else
              --
              v_new_loc_code :=Null;
              --
             end if;
             --
            exception
             when others then
              v_new_loc_code :=Null;
            end;
           --                  
        --
       else
         -- looks like this is the first land, so concatenate property code with letter L
         -- along with the land starting seq of "1" and return back
         -- Example for FL300, return back FL300L1
         --
         v_new_loc_code := v_prop_code || v_letter_L || TO_CHAR ( n_start_land_seq );        
        --
       end if;
      --
     exception
      when no_data_found then
       v_new_loc_code := v_prop_code || v_letter_L || TO_CHAR ( n_start_land_seq );
      when others then
       v_new_loc_code := Null;
     end;
     --
        --
        -- When p_loc_code is not null, a value already exists. Just make sure it conforms to the building/land id standards. 
        -- This logic is called from the forms personalization trigger When Validate Record.
        --
        if (p_loc_code is null) then 
         --
          return v_new_loc_code;
         --
        else
         --
         if (p_loc_code =v_new_loc_code) then
          --
          return p_loc_code;
          --
         else
          --
          return v_new_loc_code;
          --
         end if;
         --
        end if;
        --     
     --
    else
     return Null;
    end if;
    --
    -- When p_loc_code is not null, a value already exists. Just make sure it conforms to the building/land id standards. 
    -- This logic is called from the forms personalization trigger When Validate Record.
    --
    --    if (p_loc_code is null) then 
    --     --
    --      return v_new_loc_code;
    --     --
    --    else
    --     --
    --     if (p_loc_code =v_new_loc_code) then
    --      --
    --      return p_loc_code;
    --      --
    --     else
    --      --
    --      return v_new_loc_code;
    --      --
    --     end if;
    --     --
    --    end if;
    --    --
   EXCEPTION
      WHEN OTHERS THEN
       return Null;
   END validate_loc_code;
   --
   PROCEDURE fix_loc_code 
      (
        retcode               out number
       ,errbuf                out varchar2
       ,p_location_id         in  number
       ,p_new_loc_code        in  varchar2
      ) IS
       -- =========================================================================================
        l_module        VARCHAR2(24) :='HDS Property Mgr';
        l_err_msg       CLOB;
        l_sec           VARCHAR2(255);
        l_err_callfrom  VARCHAR2(75) DEFAULT 'apps.xxcus_opn_utilities_pkg.fix_loc_code';
        l_err_callpoint VARCHAR2(75) DEFAULT 'START';
        l_distro_list   VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';    
       --
        v_current_loc_code VARCHAR2(30);
       --
   BEGIN
     --
     begin 
      select location_code
      into   v_current_loc_code
      from   pn_locations_all
      where  1 =1
        and  location_id =p_location_id
        and  rownum <2;
     exception
      when no_data_found then
       raise program_error;
      when others then
       raise program_error;
     end;
     --
     print_log ('@Before update: Current location code =>'||v_current_loc_code);     
     --
     savepoint init_here;
     --
     update pn_locations_all
     set location_code =p_new_loc_code, location_alias =p_new_loc_code
     where 1 =1
       and location_id =p_location_id;
     --
     print_log ('@After update: New location code =>'||p_new_loc_code||', rows updated :'||sql%rowcount);
     --
   EXCEPTION
      --
       when program_error then
        print_log('Location ID =>'||p_location_id||', program error, message ='||sqlerrm);
        rollback to init_here;
      --  
       when others then   
        print_log('Location ID =>'||p_location_id||', outer block other errors');   
        print_log('Outer block, other errors in calling detail_wrapper, message ='||sqlerrm);
        l_err_msg := l_sec;
            l_err_msg := l_err_msg || ' ...Error_Stack...' ||
                         dbms_utility.format_error_stack() || ' Error_Backtrace...' ||
                         dbms_utility.format_error_backtrace();
            fnd_file.put_line(fnd_file.log, l_err_msg);
            l_err_callpoint := l_sec;      
            xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                                ,p_calling           => l_err_callpoint
                                                ,p_request_id        => fnd_global.conc_request_id
                                                ,p_ora_error_msg     => substr(' Error_Stack...' ||
                                                                               dbms_utility.format_error_stack() ||
                                                                               ' Error_Backtrace...' ||
                                                                               dbms_utility.format_error_backtrace()
                                                                              ,1
                                                                              ,2000)
                                                ,p_error_desc        => substr(l_err_msg
                                                                              ,1
                                                                              ,240)
                                                ,p_distribution_list => l_distro_list
                                                ,p_module            => l_module);    
        rollback to init_here;
      --   
   END fix_loc_code;      
   --
   FUNCTION get_file 
      (
        p_file_type IN VARCHAR2
      ) RETURN VARCHAR2 IS
   --
    cursor c_file_name (l_profile_option in varchar2) is
    select v.profile_option_value name
    from fnd_profile_options p, 
    fnd_profile_option_values v, 
    fnd_profile_options_tl n
    where p.profile_option_id =v.profile_option_id (+) 
    and p.profile_option_name =n.profile_option_name
    and p.profile_option_name =l_profile_option;   
   --
   file_info c_file_name%rowtype :=Null;
   --
   v_profile_option fnd_profile_options.profile_option_name%type :=Null;
   --
   BEGIN
    --
    if p_file_type ='ZIP' then
     --
     v_profile_option :=xxcus_opn_utilities_pkg.g_profile_for_zip_file;
     --
    elsif p_file_type ='DATA' then
     --
     v_profile_option :=xxcus_opn_utilities_pkg.g_profile_for_data_file;
     --    
    else
      v_profile_option :=Null;
    end if;
    --
    if (v_profile_option is not null) then 
         --
        OPEN  c_file_name ( l_profile_option =>v_profile_option);
        FETCH c_file_name INTO file_info;
        CLOSE c_file_name;
        --
        RETURN regexp_replace(file_info.name,'([^[:print:]])','');
        --     
     --
    else
     --We are not sure which file type we are trying to find.
     --At this time we have a profile setup to retrieve the zip as well as the data file compressed within the zip file.    
     RETURN Null; 
    end if;
    --
   EXCEPTION
    WHEN NO_DATA_FOUND THEN
     RETURN Null;
    WHEN TOO_MANY_ROWS THEN
     RETURN Null;     
    WHEN OTHERS THEN
     RETURN Null;
   END get_file;
   --
END xxcus_opn_utilities_pkg;
/