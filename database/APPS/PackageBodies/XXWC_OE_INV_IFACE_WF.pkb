--
-- XXWC_OE_INV_IFACE_WF  (Package Body) 
--
CREATE OR REPLACE PACKAGE BODY APPS.XXWC_OE_Inv_Iface_WF
AS
   /* $Header: OEXWIIFB.pls 120.0 2005/06/01 23:12:20 appldev noship $ */
   -- This Procedure gets called from the Workflow.
   -- This procedure is modified from Seeded procedure to support serial controlled transactions.
/*************************************************************************
  $Header xxwc_oe_inv_iface_wf$
  Module Name: XXWC_OE_Inv_Iface_WF.pkb

  PURPOSE:   This package will support serial controlled transactions

  REVISIONS:
  Ver        Date        Author                     Description
  ---------  ----------  ---------------         -------------------------
  1.0                                            No Previous version History
  1.1        02/03/2015  Pattabhi Avula          TMS# 20150202-00032 : @PERF: wf:OEOL  Performance issue code changes
**************************************************************************/

   PROCEDURE Inventory_Interface (itemtype    IN            VARCHAR2
                                 ,itemkey     IN            VARCHAR2
                                 ,actid       IN            NUMBER
                                 ,funcmode    IN            VARCHAR2
                                 ,resultout   IN OUT NOCOPY /* file.sql.39 change */
                                                            VARCHAR2)
   IS
      l_result_out      VARCHAR2 (30);
      l_return_status   VARCHAR2 (30);
      l_line_id         NUMBER;
-- Below Local variables are declared by Pattabhi on 02-02-2015 for TMS# 20150202-00032
	  l_pkg_name        VARCHAR2(50):='OE_Inv_Iface_WF';
	  l_proc_name       VARCHAR2(50):='Inventory_Interface';
	  l_arg3            VARCHAR2(50);  
   BEGIN
      --
      -- RUN mode - normal process execution
      --
      IF (funcmode = 'RUN')
      THEN
         OE_STANDARD_WF.Set_Msg_Context (actid);

         IF itemtype = OE_GLOBALS.G_WFI_LIN
         THEN
            l_line_id := TO_NUMBER (itemkey);
         ELSE
            RAISE FND_API.G_EXC_UNEXPECTED_ERROR;
         END IF;

		l_arg3:=TO_CHAR (actid);

         XXWC_OE_Inv_Iface_PVT.Inventory_Interface (
            p_line_id         => l_line_id
           ,x_return_status   => l_return_status
           ,x_result_out      => l_result_out);


         IF l_return_status = FND_API.G_RET_STS_SUCCESS
         THEN
            IF l_result_out = OE_GLOBALS.G_WFR_COMPLETE
            THEN
               resultout :=
                     OE_GLOBALS.G_WFR_COMPLETE
                  || ':'
                  || OE_GLOBALS.G_WFR_COMPLETE;
               OE_STANDARD_WF.Clear_Msg_Context;
               RETURN;
            ELSIF l_result_out = OE_GLOBALS.G_WFR_NOT_ELIGIBLE
            THEN
               resultout :=
                     OE_GLOBALS.G_WFR_COMPLETE
                  || ':'
                  || OE_GLOBALS.G_WFR_NOT_ELIGIBLE;
               OE_STANDARD_WF.Clear_Msg_Context;
               RETURN;
            END IF;
         ELSIF l_return_status = FND_API.G_RET_STS_ERROR
         THEN
            IF l_result_out = OE_GLOBALS.G_WFR_INCOMPLETE
            THEN
               resultout :=
                     OE_GLOBALS.G_WFR_COMPLETE
                  || ':'
                  || OE_GLOBALS.G_WFR_INCOMPLETE;
               OE_STANDARD_WF.Save_Messages (p_instance_id => actid);
               OE_STANDARD_WF.Clear_Msg_Context;
               RETURN;
            ELSIF l_result_out = OE_GLOBALS.G_WFR_ON_HOLD
            THEN
               resultout :=
                     OE_GLOBALS.G_WFR_COMPLETE
                  || ':'
                  || OE_GLOBALS.G_WFR_ON_HOLD;
               OE_STANDARD_WF.Save_Messages (p_instance_id => actid);
               OE_STANDARD_WF.Clear_Msg_Context;
               RETURN;
            ELSE                    -- STS_ERROR but not INCOMPLETE or ON_HOLD
               RAISE FND_API.G_EXC_UNEXPECTED_ERROR;
            END IF;
         ELSIF l_return_status = FND_API.G_RET_STS_UNEXP_ERROR
         THEN
            RAISE FND_API.G_EXC_UNEXPECTED_ERROR;
         END IF;
      END IF;

      --
      -- CANCEL mode
      --
      -- This is an event point is called with the effect of the activity must
      -- be undone, for example when a process is reset to an earlier point
      -- due to a loop back. OM does not use CANCEL MODE
      --
      IF (funcmode = 'CANCEL')
      THEN
         -- your cancel code goes here
         NULL;

         -- no result needed
         resultout := 'COMPLETE';
         RETURN;
      END IF;
   --
   -- Other execution modes may be created in the future.  Your
   -- activity will indicate that it does not implement a mode
   -- by returning null
   --

   EXCEPTION
      WHEN OTHERS
      THEN
         -- The line below records this function call in the error system
         -- in the case of an exception.
         wf_core.context (l_pkg_name  -- 'OE_Inv_Iface_WF'  -- Version# 1.1,TMS# 20150202-00032
                         ,l_proc_name  -- 'Inventory_Interface'  -- Version# 1.1,TMS# 20150202-00032
                         ,itemtype
                         ,itemkey
                         ,l_arg3 -- TO_CHAR (actid)  -- Version# 1.1,TMS# 20150202-00032
                         ,funcmode);
         OE_STANDARD_WF.Add_Error_Activity_Msg (p_actid      => actid
                                               ,p_itemtype   => itemtype
                                               ,p_itemkey    => itemkey);
         OE_STANDARD_WF.Save_Messages (p_instance_id => actid);
         OE_STANDARD_WF.Clear_Msg_Context;
         RAISE;
   END Inventory_Interface;
END XXWC_OE_Inv_Iface_WF;
/

