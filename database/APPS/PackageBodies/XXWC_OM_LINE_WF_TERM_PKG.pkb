CREATE OR REPLACE package body APPS.xxwc_om_line_wf_term_pkg as
/*
 -- Implementation Date:09/09/2013
 -- Author:Bala Seashadri
 -- SCOPE:Created this data fix script in refrence to the SR 3-6445032651 
 --Description of this package: When this package gets executed, the workflow gets terminated for the cancelled lines which is stuck in "Ship" Activity
 -- TMS TICKET # 20130909-00647 
 -- Modification History
 -- TMS# 20141002-00064  Canada Changes done by Pattabhi Avula on 10/31/2014

*/
  
 procedure print_log(p_message in varchar2) is
 begin
     if  fnd_global.conc_request_id >0 then
      fnd_file.put_line(fnd_file.log, p_message);
     else
      print_log(p_message);
     end if;
 end print_log;  
 --
 procedure om_line_wf_term (retcode out varchar2, errbuf out varchar2) as
  -- 
    cursor corrupt is 
    select ol.line_id, wi.end_date, ol.flow_status_code
    from apps.oe_order_lines ol,
         wf_items wi
    where wi.item_type='OEOL'
    and wi.item_key=to_char(ol.line_id)
    and ol.open_flag='Y'
    and ol.cancelled_flag='Y'
    and ol.ordered_quantity=0
    and ol.cancelled_quantity>0
    and ol.flow_status_code = 'CANCELLED'
    --and ol.line_id in (240247,257963,322983,261142,302057,379146,382776,163753)
    AND wi.end_date IS null;

    l_file_val   VARCHAR2(1000);
    --l_count      NUMBER := 0; This variable is not requried
    --err_msg  VARCHAR2(240);
    l_result VARCHAR2(30);
    b_proceed BOOLEAN;

 begin  
    oe_debug_pub.debug_on;
    oe_debug_pub.initialize;
    l_file_val    := OE_DEBUG_PUB.Set_Debug_Mode('FILE');
    oe_Debug_pub.setdebuglevel(5);
    print_log('Inside the script');
    print_log('OM Debug file path is:'||l_file_val);
    --
       for i in corrupt loop 
        --
           print_log('');          
           print_log('+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++');
           print_log('');                     
           print_log('Current OM Line Id =>'||i.line_id);  
        --       
         begin
          savepoint SQUARE1;  
          update oe_order_lines
            set open_flag ='N'
          where line_id   =i.line_id;
          -- 
           b_proceed :=TRUE;
          --          
           print_log('Operation: UPDATE, Message =>'||'Successfully updated OM Line Id open flag to N'); 
           print_log('');
          --
         exception
          when others then
           rollback to SQUARE1;
           print_log('Operation: UPDATE, Message =>'||'Failed to update open flag to N'); 
           print_log('');                      
           b_proceed :=FALSE;
         end;

         if (b_proceed) then 
             if i.end_date is null then 

                  begin
                   -- 
                       OE_Standard_WF.OEOL_SELECTOR
                        (
                            p_itemtype => 'OEOL'
                           ,p_itemkey  => to_char(i.line_id)
                           ,p_actid    => 12345 --this is a constant provided by oracle support team, do not change
                           ,p_funcmode => 'SET_CTX'
                           ,p_result   => l_result
                        );
                   --
                  exception
                    when others then
                       oe_Debug_pub.add('failed in selector call for line:'||i.line_id);
                  end;

                  begin
                   --
                      oe_Debug_pub.add('retrying the CLOSE_LINE activity for line:'||i.line_id);
                    wf_engine.handleerror('OEOL', to_char(i.line_id), 'CLOSE_LINE','RETRY','CANCEL'); 
                      oe_Debug_pub.add('CLOSE_LINE activity has been retried for line:'||i.line_id);
                   --
                  exception
                    when others then
                      oe_Debug_pub.add('retry activity failed for line:'||i.line_id);
                  end;

             end if;         
         else 
           print_log('Update to workflow lines stopped because OM Line open flag cannot be set to N for OM LINE ID ='||i.line_id);
         end if;
        --
         print_log('+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++');
         print_log('');
        --         
       end loop ;
    --
 exception
  when others then
   rollback;
   print_log('Error in xxwc_om_line_wf_term_pkg.om_line_wf_term, message ='||sqlerrm);
 end om_line_wf_term;
end xxwc_om_line_wf_term_pkg;
/