CREATE OR REPLACE PACKAGE BODY APPS.xxwc_ar_silverpop_intf_pkg
AS
   /*************************************************************************
     $Header xxwc_ar_silverpop_intf_pkg $
     Module Name: xxwc_ar_silverpop_intf_pkg.pkb

     PURPOSE:   This package is called by the concurrent programs
                XXWC Silver Pop Invoice Outbound Interface

     REVISIONS:
     Ver        Date        Author             Description
     ---------  ----------  ---------------   -------------------------
      1.0       29/01/2016  Neha Saini         Initial Build - TMS# 20160211-00160
      1.1       29/03/2016  Neha Saini         changes to add line if as fields and remove "" for TMS 20160328-00037
      1.2       31/01/2017  Ashwin Sridhar     Added a logic in the PROCEDURE generate_invoice_file to 
                                               copy the existing invoice text file generated 
                                               into a duplicate file for TMS#20161118-00098
   **************************************************************************/

   /*************************************************************************
     Procedure : Write_Log

     PURPOSE:   This procedure logs debug message in Concurrent Log file
     Parameter:
            IN
                p_debug_msg      -- Debug Message
   ************************************************************************/
   PROCEDURE write_log (p_debug_msg IN VARCHAR2)
   IS
   BEGIN
      fnd_file.put_line (fnd_file.LOG, p_debug_msg);
      DBMS_OUTPUT.put_line (p_debug_msg);
   END write_log;

   /*************************************************************************
      Procedure : Write_Error

     PURPOSE:   This procedure logs error message
     Parameter:
            IN
                p_debug_msg      -- Debug Message
   ************************************************************************/

   --add message to concurrent output file
   PROCEDURE write_error (p_debug_msg IN VARCHAR2)
   IS
      l_req_id          NUMBER := fnd_global.conc_request_id;
      l_err_callfrom    VARCHAR2 (75) DEFAULT 'XXWC_AR_SILVERPOP_INTF_PKG';
      l_err_callpoint   VARCHAR2 (75) DEFAULT 'START';
      l_distro_list     VARCHAR2 (75)
                           DEFAULT 'HDSOracleDevelopers@hdsupply.com';
   BEGIN
      xxcus_error_pkg.xxcus_error_main_api (
         p_called_from         => l_err_callfrom,
         p_calling             => l_err_callpoint,
         p_request_id          => l_req_id,
         p_ora_error_msg       => SUBSTR (p_debug_msg, 1, 2000),
         p_error_desc          => 'Error running xxwc_ar_silverpop_intf_pkg with PROGRAM ERROR',
         p_distribution_list   => l_distro_list,
         p_module              => 'AR');
   END write_error;

   /*************************************************************************
     Procedure : generate_invoice_file

     PURPOSE:   This procedure creates file for the open invoices to
                Silver Pop
     REVISIONS:
     Ver        Date        Author             Description
     ---------  ----------  ---------------   -------------------------
      1.0       29/01/2016  Neha Saini         Initial Build - TMS# 20160211-00160
      1.1       29/03/2016  Neha Saini         changes to add line if as fields and remove "" for TMS 20160328-00037
      1.2       31/01/2017  Ashwin Sridhar     Added a logic to create a duplicate copy of the existing 
                                               invoice text file generated into a new file for TMS#20161118-00098
   ************************************************************************/
   PROCEDURE generate_invoice_file (errbuf                OUT VARCHAR2,
                                    retcode               OUT VARCHAR2,
                                    p_directory_name   IN     VARCHAR2,
                                    p_from_date        IN     VARCHAR2,
                                    p_to_date          IN     VARCHAR2)
   IS
      CURSOR cur_inv_detail (
         v_date_from    DATE,
         v_date_to      DATE)
      IS
         WITH invoices
              AS (SELECT rbs.name SOURCE_NAME,
                         ract.creation_date,
                         ract.interface_header_attribute1 ORDER_NUMBER,
                         ract.attribute15 billtrust_date,
                         ract.trx_date,
                         ractl.extended_amount,
                         ractl.interface_line_attribute6 line_id,
                         ractl.quantity_invoiced,
                         ractl.quantity_credited,
                         ractl.quantity_ordered
                    FROM apps.RA_CUSTOMER_TRX ract,
                         apps.RA_BATCH_SOURCES rbs,
                         apps.ra_customer_trx_lines_all ractl
                   WHERE     1 = 1
                         AND ract.customer_trx_id = ractl.customer_trx_id
                         AND ract.batch_source_id = rbs.batch_source_id
                         AND ract.org_id = 162
                         AND NVL (ract.attribute_category, 162) = 162
                         AND ract.cust_trx_type_id = 1)
         SELECT DISTINCT
                msib.organization_id,
                ool.line_id,
                ooh.org_id operating_unit_id,
                TO_CHAR (
                   NVL (HCA.ACCOUNT_ESTABLISHED_DATE, HCA.CREATION_DATE),
                   'MM/DD/YYYY')
                   CREATION_DATE,
                TO_CHAR (
                   (CASE
                       WHEN inv.source_name IN ('WC MANUAL', 'MANUAL-OTHER')
                       THEN
                          TRUNC (inv.creation_date)
                       ELSE
                          (TRUNC (inv.creation_date) - 1)
                    END),
                   'MM/DD/YYYY')
                   INVOICE_POST_DATE,
                ooh.attribute2 ORDER_METHOD,
                ooh.order_number ORDER_NUMBER,
                REPLACE (
                   REGEXP_REPLACE (
                      msib.segment1,
                      '[^' || CHR (32) || '-' || CHR (127) || ']',
                      ' '),
                   '"',
                   ' ')
                   BUSINESS_SKU_NUMBER,                       --ver1.1 by Neha
                REGEXP_REPLACE (msib.description,
                                '[^' || CHR (32) || '-' || CHR (127) || ']',
                                ' ')
                   SKU_DESCRIPTION,
                hca.ACCOUNT_NUMBER CUSTOMER_ACCOUNT_CODE,
                NVL (HCA.ACCOUNT_NAME, HP.PARTY_NAME) CUSTOMER_ACCOUNT_NAME,
                REPLACE (hl.address1, '"', ' ') BILL_ADDRESS_1, --ver1.1 by Neha
                REPLACE (hl.address2, '"', ' ') BILL_ADDRESS_2, --ver 1.1 by Neha
                hl.CITY BILL_ADDRESS_CITY,
                CASE
                   WHEN LENGTH (NVL (hl.STATE, hl.PROVINCE)) > 2 THEN NULL
                   ELSE NVL (hl.STATE, hl.PROVINCE)
                END
                   BILL_ADDRESS_STATE_PROV,
                hl.postal_code BILL_ADDRESS_POSTAL_CODE,
                (apps.XXWC_MV_ROUTINES_PKG.GET_PHONE_FAX_NUMBER (
                    'PHONE',
                    hca.PARTY_ID,
                    NULL,
                    'GEN'))
                   MAIN_PHONE_NUMBER,
                mtc.INVENTORY_ITEM_ID,
                (SELECT FFV.DESCRIPTION
                   FROM APPS.FND_FLEX_VALUE_SETS FFVS,
                        APPS.FND_FLEX_VALUES_VL FFV
                  WHERE     FFVS.FLEX_VALUE_SET_NAME =
                               'XXWC_CATMGT_CATEGORIES'
                        AND FFVS.FLEX_VALUE_SET_ID = FFV.FLEX_VALUE_SET_ID
                        AND FFV.FLEX_VALUE = MC.ATTRIBUTE5)
                   CATEGORY_DESCRIPTION,
                (SELECT FFV.DESCRIPTION
                   FROM APPS.FND_FLEX_VALUE_SETS FFVS,
                        APPS.FND_FLEX_VALUES_VL FFV
                  WHERE     FFVS.FLEX_VALUE_SET_NAME =
                               'XXWC_CATMGT_SUBCATEGORIES'
                        AND FFVS.FLEX_VALUE_SET_ID = FFV.FLEX_VALUE_SET_ID
                        AND FFV.FLEX_VALUE = MC.ATTRIBUTE6)
                   SUB_CATEGORY_DISCRIPTION,
                (SELECT email_address
                   FROM apps.hz_contact_points
                  WHERE     contact_point_type = 'EMAIL'
                        AND owner_table_name = 'HZ_PARTIES'
                        AND owner_table_id = hp.party_id
                        AND primary_flag = 'Y'
                        AND status = 'A')
                   MAIN_EMAIL,
                (COALESCE (inv.quantity_invoiced,
                           inv.quantity_credited,
                           inv.quantity_ordered,
                           0))
                   trx_qty,
                inv.extended_amount sales
           FROM apps.hz_parties hp,
                apps.hz_party_sites hps,
                apps.hz_locations hl,
                apps.hz_cust_accounts hca,
                apps.hz_cust_acct_sites hcsa,
                apps.hz_cust_site_uses hcsu,
                apps.OE_ORDER_HEADERS ooh,
                apps.OE_ORDER_LINES ool,
                apps.MTL_SYSTEM_ITEMS_B msib,
                invoices inv,
                apps.mtl_item_categories mtc,
                apps.mtl_category_sets mcs,
                apps.MTL_CATEGORIES mc
          WHERE     1 = 1
                AND hp.party_id = hps.party_id
                AND hps.location_id = hl.location_id
                AND hp.party_id = hca.party_id
                AND hcsa.party_site_id = hps.party_site_id
                AND hcsu.cust_acct_site_id = hcsa.cust_acct_site_id
                AND hca.cust_account_id = hcsa.cust_account_id
                AND ooh.sold_to_org_id = hca.cust_account_id
                AND ooh.ship_to_org_id = hcsu.site_use_id
                AND ool.header_id = ooh.header_id
                AND ool.inventory_item_id = msib.inventory_item_id
                AND ool.ship_from_org_id = msib.organization_id
                AND inv.order_number = TO_CHAR (ooh.order_number)
                AND inv.line_id = ool.line_id
                AND ooh.org_id = 162
                AND mtc.INVENTORY_ITEM_ID = msib.inventory_item_id
                AND mtc.ORGANIZATION_ID = msib.organization_id
                AND mtc.CATEGORY_SET_ID = mcs.CATEGORY_SET_ID
                AND mcs.category_set_name = 'Inventory Category'
                AND mtc.category_id = mc.category_id
                AND inv.billtrust_date IS NOT NULL
                AND TRUNC (inv.trx_date) BETWEEN NVL (v_date_from,
                                                      TRUNC (SYSDATE))
                                             AND NVL (v_date_to,
                                                      TRUNC (SYSDATE));

      l_filename          VARCHAR2 (200);
      l_new_filename      VARCHAR2 (200); --Added by Ashwin Sridhar on 31-Jan-2017 for TMS#20161118-00098
      l_count             NUMBER;
      g_prog_exception    EXCEPTION;
      l_from_date         DATE;
      v_file              UTL_FILE.file_type;
      l_to_date           DATE;
      l_sku_description   VARCHAR2 (4000);
      l_err_msg           VARCHAR2 (4000);
      l_trx_cost          NUMBER;
      l_gross_margin      NUMBER;
      l_sku_number        APPS.MTL_SYSTEM_ITEMS_B.segment1%TYPE;--ver1.1 by Neha
      l_BILL_ADDRESS_2    apps.hz_locations.address2%TYPE;--ver1.1 by Neha
      l_BILL_ADDRESS_1    apps.hz_locations.address1%TYPE;--ver1.1 by Neha
   BEGIN
      -- printing in Parameters
      write_log ('Begining EBS To Silver Pop Invoice File Generation ');
      write_log ('========================================================');
      write_log ('Printing in parameter');
      write_log ('p_from_date:  ' || p_from_date);
      write_log ('p_to_date:  ' || p_to_date);
      write_log ('p_directory_name:  ' || p_directory_name);

      --Initialize the Out Parameter
      errbuf := NULL;
      retcode := '0';

      --Validating Directory p_directory_name

      IF (p_directory_name IS NULL)
      THEN
         l_err_msg := ' Please provide the Directory name to create a file.  ';
         RAISE g_prog_exception;
      END IF;



      SELECT TRUNC (FND_DATE.CANONICAL_TO_DATE (p_from_date)),
             TRUNC (FND_DATE.CANONICAL_TO_DATE (p_to_date))
        INTO l_from_date, l_to_date
        FROM DUAL;

      --verifying dates
      write_log ('l_from_date:  ' || l_from_date);
      write_log ('l_to_date:  ' || l_to_date);



      -- Get the file name
      SELECT    'AR_SILVPOP_INV_'
             || TO_CHAR (SYSDATE, 'MMDDRRRR_HH_MI_SS')
             || '.txt'
        INTO l_filename
        FROM DUAL;

      write_log ('l_filename:  ' || l_filename);

      --Added the below to get the new file name by Ashwin Sridhar on 31-Jan-2016 for TMS#20161118-00098
      SELECT    'CUST_TRANS_DB_'
             || TO_CHAR (SYSDATE, 'MMDDRRRR_HH_MI_SS')
             || '.txt'
      INTO l_new_filename
      FROM DUAL;

      write_log ('l_new_filename:  ' || l_new_filename);

      --Open the file handler
      v_file :=
         UTL_FILE.fopen (LOCATION       => p_directory_name,
                         filename       => l_filename,
                         open_mode      => 'w',
                         max_linesize   => 32767);

      write_log ('Writing to the file ... Opening Cursor');
      l_count := 0;

      FOR cur_rec IN cur_inv_detail (l_from_date, l_to_date)
      LOOP
         EXIT WHEN cur_inv_detail%NOTFOUND;

         --replace delimiter and special character from SKu Description

         l_sku_description :=
            REGEXP_REPLACE (cur_rec.SKU_DESCRIPTION,
                            '[^[:alnum:]|[:blank:]|[:punct:]]+',
                            NULL);
         l_sku_description :=
            REGEXP_REPLACE (l_sku_description,
                            '[^' || CHR (32) || '-' || CHR (127) || ']',
                            ' ');
         l_sku_description := REPLACE (l_sku_description, '"', ' '); --ver1.1 changes

         SELECT REPLACE (l_sku_description, '|', '')
           INTO l_sku_description
           FROM DUAL;

         -- changes for ver1.1 changes starts
         l_sku_number :=
            REGEXP_REPLACE (cur_rec.BUSINESS_SKU_NUMBER,
                            '[^' || CHR (32) || '-' || CHR (127) || ']',
                            ' ');
         l_BILL_ADDRESS_1 :=
            REGEXP_REPLACE (cur_rec.BILL_ADDRESS_1,
                            '[^' || CHR (32) || '-' || CHR (127) || ']',
                            ' ');
         l_BILL_ADDRESS_2 :=
            REGEXP_REPLACE (cur_rec.BILL_ADDRESS_2,
                            '[^' || CHR (32) || '-' || CHR (127) || ']',
                            ' ');

         ---- changes for ver1.1 changes ends

         -- getting gross margin
         BEGIN
            l_trx_cost := 0;

            SELECT ROUND (order_line_cost, 2)
              INTO l_trx_cost
              FROM (SELECT mmt.trx_source_line_id,
                           mmt.organization_id,
                           NVL (mmt.transaction_cost,
                                NVL (mmt.actual_cost, 0))
                              AS order_line_cost,
                           ROW_NUMBER ()
                           OVER (
                              PARTITION BY mmt.trx_source_line_id
                              ORDER BY
                                 NVL (mmt.transaction_cost,
                                      NVL (mmt.actual_cost, 0)) DESC)
                              AS rn
                      FROM inv.mtl_material_transactions mmt,
                           inv.mtl_transaction_types mtt
                     WHERE     mmt.transaction_type_id =
                                  mtt.transaction_type_id
                           AND mmt.organization_id = cur_rec.organization_id
                           AND mmt.trx_source_line_id =
                                  TO_CHAR (cur_rec.line_id))
             WHERE rn = 1;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_trx_cost := 0;
               l_err_msg :=
                     ' it got an error while fetching unit cost  for organization id'
                  || cur_rec.organization_id
                  || ' source line id '
                  || cur_rec.line_id
                  || ' and order number '
                  || cur_rec.order_number;
         --RAISE g_prog_exception;
         END;

         write_log (
               ' Sales value '
            || cur_rec.sales
            || ' for order line id '
            || cur_rec.line_id
            || ' and order number '
            || cur_rec.order_number);
         write_log (
               ' trx_cost '
            || l_trx_cost
            || ' for order line id '
            || cur_rec.line_id
            || ' and order number '
            || cur_rec.order_number);
         write_log (
               ' trx_qty '
            || cur_rec.trx_qty
            || ' for order line id '
            || cur_rec.line_id
            || ' and order number '
            || cur_rec.order_number);

         l_gross_margin := cur_rec.sales - (cur_rec.trx_qty * l_trx_cost);

         write_log (
               ' gross_margin '
            || l_gross_margin
            || ' for order line id '
            || cur_rec.line_id
            || ' and order number '
            || cur_rec.order_number);
         -- Writing File Lines

         UTL_FILE.put_line (
            v_file,
            UPPER (                                    --ver1.1 changes starts
                  TRIM (cur_rec.line_id)
               || '|'                                    --ver1.1 changes ends
               || TRIM (cur_rec.CREATION_DATE)
               || '|'
               || TRIM (cur_rec.INVOICE_POST_DATE)
               || '|'
               || TRIM (cur_rec.ORDER_METHOD)
               || '|'
               || TRIM (cur_rec.ORDER_NUMBER)
               || '|'
               || TRIM (l_sku_number) --ver1.1 changes 
               || '|'
               || TRIM (l_sku_description) --ver1.1 changes 
               || '|'
               || TRIM (cur_rec.CATEGORY_DESCRIPTION)
               || '|'
               || TRIM (cur_rec.SUB_CATEGORY_DISCRIPTION)
               || '|'
               || TRIM (cur_rec.CUSTOMER_ACCOUNT_CODE)
               || '|'
               || TRIM (cur_rec.CUSTOMER_ACCOUNT_NAME)
               || '|'
               || TRIM (l_BILL_ADDRESS_1)--ver1.1 changes 
               || '|'
               || TRIM (l_BILL_ADDRESS_2)--ver1.1 changes 
               || '|'
               || TRIM (cur_rec.BILL_ADDRESS_CITY)
               || '|'
               || TRIM (REPLACE(cur_rec.BILL_ADDRESS_STATE_PROV,'"',' '))--ver1.1 by Neha
               || '|'
               || TRIM (cur_rec.BILL_ADDRESS_POSTAL_CODE)
               || '|'
               || TRIM (cur_rec.MAIN_PHONE_NUMBER)
               || '|'
               || TRIM (cur_rec.MAIN_EMAIL)
               || '|'
               || cur_rec.sales
               || '|'
               || l_gross_margin
               || '|'));

         l_count := l_count + 1;
      END LOOP;

      write_log ('Wrote ' || l_count || ' records to file');
      write_log ('Closing File Handler');

      --Closing the file handler
      UTL_FILE.fclose (v_file);
  
      --Added the UTL_FILE copy logic by Ashwin Sridhar on 31-Jan-2017 for TMS#20161118-00098
      utl_file.fcopy(p_directory_name
                   , l_filename
                   , p_directory_name
                   , l_new_filename);

      retcode := '0';
      write_log ('Program Successfully completed');
   EXCEPTION
      WHEN UTL_FILE.invalid_path
      THEN
         UTL_FILE.fclose (v_file);
         errbuf := 'File Path is invalid.';
         retcode := '2';
         write_error (errbuf);
         write_log (errbuf);
      WHEN UTL_FILE.invalid_mode
      THEN
         UTL_FILE.fclose (v_file);
         errbuf := 'The open_mode parameter in FOPEN is invalid.';
         retcode := '2';
         write_error (errbuf);
         write_log (errbuf);
      WHEN UTL_FILE.invalid_filehandle
      THEN
         UTL_FILE.fclose (v_file);
         errbuf := 'File handle is invalid..';
         retcode := '2';
         write_error (errbuf);
         write_log (errbuf);
      WHEN UTL_FILE.invalid_operation
      THEN
         UTL_FILE.fclose (v_file);
         errbuf := 'File could not be opened or operated on as requested';
         retcode := '2';
         write_error (errbuf);
         write_log (errbuf);
      WHEN UTL_FILE.write_error
      THEN
         UTL_FILE.fclose (v_file);
         errbuf :=
            'Operating system error occurred during the write operation';
         retcode := '2';
         write_error (errbuf);
         write_log (errbuf);
      WHEN UTL_FILE.internal_error
      THEN
         UTL_FILE.fclose (v_file);
         write_log ('Unspecified PL/SQL error.');
      WHEN UTL_FILE.file_open
      THEN
         UTL_FILE.fclose (v_file);
         errbuf := 'The requested operation failed because the file is open.';
         retcode := '2';
         write_error (errbuf);
         write_log (errbuf);
      WHEN UTL_FILE.invalid_filename
      THEN
         UTL_FILE.fclose (v_file);
         errbuf := 'The filename parameter is invalid.';
         retcode := '2';
         write_error (errbuf);
         write_log (errbuf);
      WHEN UTL_FILE.access_denied
      THEN
         UTL_FILE.fclose (v_file);
         errbuf := 'Permission to access to the file location is denied.';
         retcode := '2';
         write_error (errbuf);
         write_log (errbuf);
      WHEN g_prog_exception
      THEN
         errbuf := l_err_msg;
         retcode := '2';
         write_error (l_err_msg);
         write_log (l_err_msg);
      WHEN OTHERS
      THEN
         -- UTL_FILE.fclose (v_file);
         l_err_msg := 'Error Msg :' || SQLERRM;
         errbuf := l_err_msg;
         retcode := '2';
         write_error (l_err_msg);
         write_log (l_err_msg);
   END generate_invoice_file;
END xxwc_ar_silverpop_intf_pkg;
/