CREATE OR REPLACE PACKAGE BODY APPS.xxcusgl_accrual_pkg IS

  /*******************************************************************************
  * Procedure:   LOAD_TE_ACCRUAL
  * Description: This will create an a accrual entry during month end closing
  *              for unprocessed credit card transactions.  Concurrent request
  *              to be run from HSI AP Administrator responsibility
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     08/12/2009    Kathy Poling    Initial creation of the procedure
  1.1     10/06/2009    Kathy Poling    Changed out of pocket to accrue expenses within
                                        90 days of accrual date using eht end_expense_date.
                                        For out of pocket changed to only accrue for the
                                        following status (EMP APPROVED, MGRAPPR, PENDMGR,
                                        PENDING EMP RESOLUTN, RESOLUTN, SAVED, No Status)
  1.2     10/13/2009    Kathy Poling    Changed the 90 days from sysdate to the last day
                                        of the period
  1.3     10/15/2009    Kathy Poling    Changed the liability account from the card program
                                        to product if CC expense is for Crown Bolt or FM to
                                        stop creating an intercompany transaction when an
                                        employee has changed locations or using the card
                                        from a differt LOB.  All others will default to Corp
  1.4     10/27/2009    Kathy Poling    Changed the source to HDS Accrual from iExpense
                                        Service Ticket 22315, RFC 9490
  1.5     07/16/2010    Kathy Poling    Service Ticket 48577  Changes to where clause
                                        adding two NVL
  1.6     08/05/2010    Kathy Poling    Service Ticket 50163  Change to look at CC Transaction
                                        and eliminate if category is PERSONAL
  1.7     08/30/2010    Kathi Robinson  Fix issue with PERSONAL exclusion - was not picking up
                                        the NULL category items in the CC Transaction table, only
                                        the BUSINESS ones.
  1.8     04/08/2012    Kathy Poling    Changed ledger  Service Ticket 134699
                                        Changed to email if import or post ends in error or warning
  1.9     06/07/2012    Kathy Poling    Service Ticket 141622 added category for reporting 
                                        RFC 34757
  1.11    07/12/2014    Nancy Pahwa     Changed the Posting select statement where clause for 
                                        gl.gl_je_batches based on Service Ticket 248924 to resolve 
                                        posting error.Added the success notification on posting compelition.
                                        Updated the package with new development standards like changing 
                                        the output variables errbuf and retcode to p_errbuf and 
                                        variables like with v_ to l_. Also made the change to the EXCEPTION
                                        WHEN program_error THEN. RFC #40979
  1.12    10/14/2014    Kathy Poling    Enhancment for Canada OIE, org_id 167 in procedure load_te_accrual
                                        RFC 42470 S272756                                                  
  ********************************************************************************/
  PROCEDURE load_te_accrual(p_errbuf  OUT VARCHAR2
                           ,p_retcode OUT NUMBER
                           ,p_fperiod IN VARCHAR2) IS
  
    --Intialize Variables
    --l_sec      VARCHAR2(750);
    l_sec VARCHAR2(110) DEFAULT 'START';
    -- l_err_msg  VARCHAR2(2000);
    l_err_msg  CLOB;
    l_err_code NUMBER;
    l_group_id NUMBER;
  
    --Import variable
    lc_table_name           VARCHAR2(15) := 'GL_INTERFACE';
    l_posting_id            NUMBER := 0;
    l_processed_data_action VARCHAR2(25) := NULL;
    l_journalname           VARCHAR2(100);
    l_date                  VARCHAR2(15);
  
    --Submission variables
    l_globalset          VARCHAR2(100);
    l_can_submit_request BOOLEAN := TRUE;
    l_req_id_1           NUMBER;
    l_req_id_2           NUMBER;
    l_phase              VARCHAR2(50);
    l_status             VARCHAR2(50);
    l_dev_status         VARCHAR2(50);
    l_dev_phase          VARCHAR2(50);
    l_message            VARCHAR2(50);
    l_error_message      VARCHAR2(150);
  
    --
    l_start_date gl_periods.start_date%TYPE;
    l_end_date   gl_periods.end_date%TYPE;
    l_period     gl_periods.period_name%TYPE;
    l_seg1       xxcus.xxcus_parameters.parm_value%TYPE;
    l_seg2       xxcus.xxcus_parameters.parm_value%TYPE;
    l_seg3       xxcus.xxcus_parameters.parm_value%TYPE;
    l_seg4       xxcus.xxcus_parameters.parm_value%TYPE;
    l_calendar    CONSTANT gl_periods.period_set_name%TYPE := '4-4-QTR';
    l_category    CONSTANT gl_je_categories.user_je_category_name%TYPE := 'iExp Accrual';
    l_source      CONSTANT gl_je_sources_tl.user_je_source_name%TYPE := 'HDS Accrual';
    l_jesourcesys CONSTANT gl_je_sources_tl.je_source_name%TYPE := '2';
    l_coa         CONSTANT gl_code_combinations.chart_of_accounts_id%TYPE := 50328; --Main Chart of Accounts
    --l_ledger      CONSTANT gl_ledgers.ledger_id%TYPE := 2061; --HDS Supply USD
    l_ledger             gl_ledgers.ledger_id%TYPE;
    l_access_set         NUMBER := 1042;
    l_procedure_name     VARCHAR2(75) := 'XXCUSGL_ACCRUAL.LOAD_TE_ACCRUAL';
    l_journalline        VARCHAR2(100);
    l_outofpocketdefault VARCHAR2(100);
    l_cc_default         VARCHAR2(100);
    l_cc_liability       VARCHAR2(100);
    l_org_id             NUMBER; --Version 1.12
    l_ledger_id          NUMBER; --Version 1.12
    l_conv_rate          NUMBER; --Version 1.12
    l_conv_type          VARCHAR2(100) DEFAULT 'Corporate'; --Version 1.12
    l_conv_date          DATE; --Version 1.12
    l_liability          VARCHAR2(10); --Version 1.12
  
    --Email Defaults
    l_dflt_email  fnd_user.email_address%TYPE := 'HDSIExpenseAccrualNotification@hdsupply.com';
    l_sender      VARCHAR2(100);
    l_host        VARCHAR2(256) := 'mailoutrelay.hdsupply.net';
    l_hostport    VARCHAR2(20) := '25';
    l_sid         VARCHAR2(8);
    l_subject     VARCHAR2(32767) DEFAULT NULL;
    l_body        VARCHAR2(32767) DEFAULT NULL;
    l_body_header VARCHAR2(32767) DEFAULT NULL;
    l_body_detail VARCHAR2(32767) DEFAULT NULL;
    l_body_footer VARCHAR2(32767) DEFAULT NULL;
  
    --Start Main Program
  BEGIN
  
    --Truncate xxcus_CC_ACCRUAL_TBL
    BEGIN
      l_sec := 'Truncate the summary table before loading specified time frame.';
      EXECUTE IMMEDIATE 'TRUNCATE TABLE XXCUS.XXCUSGL_TE_ACCRUAL_TBL';
    
    EXCEPTION
      WHEN OTHERS THEN
        l_err_msg := 'Failed to Truncate XXCUSGL_TE_ACCRUAL_TBL: ' ||
                     SQLERRM;
        RAISE program_error;
    END;
  
    l_sec := 'Getting default lookups for accrual';
  
    BEGIN
      SELECT period_name, start_date, end_date
        INTO l_period, l_start_date, l_end_date
        FROM gl.gl_periods
       WHERE period_set_name = l_calendar
         AND period_name = p_fperiod
         AND period_num <> 13;
    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := 'Failed to period dates for parameter ' || p_fperiod;
        fnd_file.put_line(fnd_file.log, l_err_msg);
        RAISE program_error;
    END;
/*  conversion rate not needed per Ivy  10/16/2014
    -- Get conversion rate (USD to CAD) version 1.12
    BEGIN
      SELECT show_conversion_rate, conversion_date
        INTO l_conv_rate, l_conv_date
        FROM gl_daily_rates_v t
       WHERE conversion_date =
             (SELECT MAX(conversion_date)
                FROM apps.gl_daily_rates_v
               WHERE conversion_date <= trunc(SYSDATE)
                 AND show_conversion_rate IS NOT NULL)
         AND from_currency = 'USD'
         AND to_currency = 'CAD'
         AND conversion_type = l_conv_type;
    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := 'Failed to conversion rate';
        fnd_file.put_line(fnd_file.log, l_err_msg);
        RAISE program_error;
    END;
  */
    l_sec := 'Insert data to staging tbl.';
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
  
    --insert data to be used for the accrual
    INSERT /*+ APPEND */
    INTO xxcus.xxcusgl_te_accrual_tbl
      (SELECT oracle_accounts
             ,oracle_product
             ,oracle_location
             ,oracle_cost_center
             ,oracle_account
             ,project_number segment5
             ,'00000' segment6
             ,'00000' segment7
             ,item_description
             ,line_amount
             ,currency_code
             ,full_name
             ,employee_number
             ,emp_default_prod
             ,emp_default_loc
             ,emp_default_costctr
             ,fru
             ,merchant_name
             ,credit_card_trx_id
             ,transaction_date
             ,card_program_id
             ,nvl(card_program_name, 'Out of Pocket') card_program_name
             ,mcc_code_no
             ,mcc_code
             ,nvl(imported_to_ap, 'N') imported_to_ap
             ,nvl(imported_to_gl, 'N') imported_to_gl
             ,query_num
             ,query_descr
             ,(SELECT description
                 FROM apps.fnd_lookup_values
                WHERE lookup_type = 'HDS_GL_ACCRUAL_PKG'
                  AND (lookup_code LIKE 'CARDPROGRAM_%' AND
                      substr(lookup_code, 13, 87) = card_program_name OR
                      lookup_code LIKE 'OUTOFPOCKET_' || oracle_product AND
                      card_program_name IS NULL)
                  AND enabled_flag = 'Y'
                  AND nvl(end_date_active, SYSDATE) >= SYSDATE) parm_value
             ,nvl(expense_report_status, 'No Status') expense_report_status
             ,start_expense_date
             ,end_expense_date
             ,cc_trns_category --Version 1.9
             ,org_id --Version 1.12
         FROM xxcus.xxcus_bullet_iexp_tbl
        WHERE nvl(merchant_name, 'X') <> 'CR BAL REFUND'
          AND nvl(imported_to_gl, 'N') = 'N'
          AND (transaction_date <= l_end_date OR
               end_expense_date <= l_end_date)
          AND (query_num = '4' AND end_expense_date >= l_end_date - 90)
           OR query_num IN ('2', '3', '5'));
  
    COMMIT;
  
    --loop thru to create JE by each org_id/ledger
    --Version 1.12
  
    FOR cur_org IN (SELECT DISTINCT org_id FROM xxcus.xxcusgl_te_accrual_tbl)
    LOOP
      
    l_ledger               := NULL;
    l_liability            := NULL;
    l_seg1                 := NULL;
    l_seg2                 := NULL;
    l_seg3                 := NULL;
    l_seg4                 := NULL;
    l_cc_default           := NULL;
    l_outofpocketdefault   := NULL;
    l_group_id             := NULL;
    l_date                 := NULL;
    l_journalname          := NULL;
    
      l_sec := 'Get ledger for ORG_ID ' || cur_org.org_id;
      fnd_file.put_line(fnd_file.log, l_sec);    
      ----Version 1.12 get ledger based on org for JE
      BEGIN
        SELECT ledger_id
          INTO l_ledger
          FROM hr_organization_information o1
              ,hr_organization_information o2
              ,gl_ledgers_public_v         gl
         WHERE o1.organization_id = o2.organization_id
           AND o1.organization_id = cur_org.org_id
           AND o1.org_information_context = 'CLASS'
           AND o2.org_information_context = 'Operating Unit Information'
           AND o1.org_information1 = 'OPERATING_UNIT'
           AND o1.org_information2 = 'Y'
           AND o2.org_information3 = gl.ledger_id;
      EXCEPTION
        WHEN no_data_found THEN
          l_err_msg := 'Ledger ID not found for org id ' || cur_org.org_id || ' - ' ||
                       SQLERRM;
          RAISE program_error;
      END;
    
      --get default segments for account code combination that will be used if a segment is missing 
      --Version 1.12 get default liability for org if product doesn't exists in 'ACCRUE_LIAB'
      BEGIN
        SELECT description
          INTO l_liability
          FROM apps.fnd_lookup_values
         WHERE lookup_type = 'HDS_GL_ACCRUAL_PKG'
           AND lookup_code like 'ACCRUE_DEFAULT_%'
           AND enabled_flag = 'Y'
           AND nvl(end_date_active, SYSDATE) >= SYSDATE
           AND tag = cur_org.org_id;
      EXCEPTION
        WHEN no_data_found THEN
          l_err_msg := 'Failed to find liability seg1 for org id ' ||
                       cur_org.org_id;
          fnd_file.put_line(fnd_file.log, l_err_msg);
          RAISE program_error;
      END;
    
      BEGIN
        SELECT description
          INTO l_seg1
          FROM apps.fnd_lookup_values
         WHERE lookup_type = 'HDS_GL_ACCRUAL_PKG'
              --AND lookup_code = 'SEG1_1'
           AND lookup_code like 'SEG1_%' --Version 1.12 
           AND enabled_flag = 'Y'
           AND nvl(end_date_active, SYSDATE) >= SYSDATE
           AND tag = cur_org.org_id; --Version 1.12 
      EXCEPTION
        WHEN no_data_found THEN
          l_err_msg := 'Failed to find default seg1 for org id ' ||
                       cur_org.org_id;
          fnd_file.put_line(fnd_file.log, l_err_msg);
          RAISE program_error;
      END;
    
      BEGIN
        SELECT description
          INTO l_seg2
          FROM apps.fnd_lookup_values
         WHERE lookup_type = 'HDS_GL_ACCRUAL_PKG'
              --AND lookup_code = 'SEG2_1'
           AND lookup_code like 'SEG2_%' --Version 1.12 
           AND enabled_flag = 'Y'
           AND nvl(end_date_active, SYSDATE) >= SYSDATE
           AND tag = cur_org.org_id; --Version 1.12 
      EXCEPTION
        WHEN no_data_found THEN
          l_err_msg := 'Failed to find default seg2 for org id ' ||
                       cur_org.org_id;
          fnd_file.put_line(fnd_file.log, l_err_msg);
          RAISE program_error;
      END;
    
      BEGIN
        SELECT description
          INTO l_seg3
          FROM apps.fnd_lookup_values
         WHERE lookup_type = 'HDS_GL_ACCRUAL_PKG'
              --AND lookup_code = 'SEG3_1'
           AND lookup_code like 'SEG3_%' --Version 1.12 
           AND enabled_flag = 'Y'
           AND nvl(end_date_active, SYSDATE) >= SYSDATE
           AND tag = cur_org.org_id; --Version 1.12 
      EXCEPTION
        WHEN no_data_found THEN
          l_err_msg := 'Failed to find default seg3 for org id ' ||
                       cur_org.org_id;
          fnd_file.put_line(fnd_file.log, l_err_msg);
          RAISE program_error;
      END;
    
      BEGIN
        SELECT description
          INTO l_seg4
          FROM apps.fnd_lookup_values
         WHERE lookup_type = 'HDS_GL_ACCRUAL_PKG'
              --AND lookup_code = 'SEG4_1'
           AND lookup_code like 'SEG4_%' --Version 1.12 
           AND enabled_flag = 'Y'
           AND nvl(end_date_active, SYSDATE) >= SYSDATE
           AND tag = cur_org.org_id; --Version 1.12 
      EXCEPTION
        WHEN no_data_found THEN
          l_err_msg := 'Failed to find default seg4 for org id ' ||
                       cur_org.org_id;
          fnd_file.put_line(fnd_file.log, l_err_msg);
          RAISE program_error;
      END;
    
      BEGIN
        SELECT description
          INTO l_cc_default
          FROM apps.fnd_lookup_values
         WHERE lookup_type = 'HDS_GL_ACCRUAL_PKG'
           AND lookup_code like 'CARDPROGRAM_DEFAULT%'
           AND enabled_flag = 'Y'
           AND nvl(end_date_active, SYSDATE) >= SYSDATE
           AND tag = cur_org.org_id; --Version 1.12
      EXCEPTION
        WHEN no_data_found THEN
          l_err_msg := 'Failed to find default acctg for cardprogram';
          fnd_file.put_line(fnd_file.log, l_err_msg);
          RAISE program_error;
      END;
    
      BEGIN
        SELECT description
          INTO l_outofpocketdefault
          FROM apps.fnd_lookup_values
         WHERE lookup_type = 'HDS_GL_ACCRUAL_PKG'
           AND lookup_code like 'OUTOFPOCKET_DEFAULT%'
           AND enabled_flag = 'Y'
           AND nvl(end_date_active, SYSDATE) >= SYSDATE
           AND tag = cur_org.org_id; --Version 1.12
      EXCEPTION
        WHEN no_data_found THEN
          l_err_msg := 'Failed to find default acctg for out of pocket';
          fnd_file.put_line(fnd_file.log, l_err_msg);
          RAISE program_error;
      END;
    
      SELECT gl_journal_import_s.nextval INTO l_group_id FROM dual;
    
      SELECT trunc(SYSDATE) INTO l_date FROM dual;
    
      l_journalname := 'iExpense ACCRUAL _' || l_group_id || ' P: ' ||
                       p_fperiod || ' on ' || l_date;
    
      INSERT INTO xxcus.xxcusgl_te_audit_tbl
      VALUES
        (l_group_id
        ,l_journalname
        ,fnd_global.conc_request_id
        ,SYSDATE
        ,nvl(fnd_global.user_name, 'NA')
        ,nvl(fnd_global.resp_name, 'NA')
        ,p_fperiod
        ,0
        ,'Beginning Program'
        ,SYSDATE);
    
      COMMIT;
    
      l_sec := 'Enter Loop For Expense.';
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
    
      UPDATE xxcus.xxcusgl_te_audit_tbl
         SET stage = l_sec, stg_date = SYSDATE
       WHERE group_id = l_group_id;
    
      --COMMIT;  
    
      FOR c_glint_exp IN (SELECT oracle_product
                                ,oracle_location
                                ,oracle_cost_center
                                ,oracle_account
                                ,segment5
                                ,segment6
                                ,segment7
                                ,SUM(line_amount) sum_amount
                                ,nvl(full_name, 'No Employee') full_name
                                ,merchant_name
                                ,nvl(currency_code, 'USD') currency_code
                            FROM xxcus.xxcusgl_te_accrual_tbl
                           WHERE nvl(credit_card_trx_id, -1) <> 652460
                             AND imported_to_gl = 'N'
                             AND nvl(item_description, 'X') <> 'Personal'
                             AND (card_program_name <> 'Out of Pocket' AND
                                  nvl(cc_trns_category, 'X') <> 'PERSONAL' --Version 1.9
                                  /* credit_card_trx_id IN
                                  (SELECT trx_id
                                  FROM ap.ap_credit_card_trxns_all t
                                  WHERE t.trx_id = credit_card_trx_id
                                  AND nvl(t.category, 'X') <> 'PERSONAL')
                                  */ --Version 1.9
                                  AND nvl(mcc_code_no, 'X') <> '0' OR
                                  card_program_name = 'Out of Pocket' AND
                                  expense_report_status IN
                                  (SELECT description
                                     FROM apps.fnd_lookup_values
                                    WHERE lookup_type = 'HDS_GL_ACCRUAL_PKG'
                                      AND lookup_code LIKE
                                          'OUTOFPOCKETSTATUS_%'
                                      AND enabled_flag = 'Y'
                                      AND nvl(end_date_active, SYSDATE) >=
                                          SYSDATE))
                             AND org_id = cur_org.org_id
                           GROUP BY oracle_product
                                   ,oracle_location
                                   ,oracle_cost_center
                                   ,oracle_account
                                   ,segment5
                                   ,segment6
                                   ,segment7
                                   ,full_name
                                   ,merchant_name
                                   ,nvl(currency_code, 'USD')
                                   ,org_id)
      LOOP
      
        IF c_glint_exp.oracle_product IS NULL
        THEN
          c_glint_exp.oracle_product := l_seg1;
        END IF;
      
        IF c_glint_exp.oracle_location IS NULL
        THEN
          c_glint_exp.oracle_location := l_seg2;
        END IF;
      
        IF c_glint_exp.oracle_cost_center IS NULL
        THEN
          c_glint_exp.oracle_cost_center := l_seg3;
        END IF;
      
        IF c_glint_exp.oracle_account IS NULL
        THEN
          c_glint_exp.oracle_account := l_seg4;
        END IF;
      
        --Insert the gl interface
        INSERT INTO gl.gl_interface
          (status
          ,ledger_id
          ,accounting_date
          ,currency_code
          ,date_created
          ,created_by
          ,actual_flag
          ,user_je_category_name
          ,user_je_source_name
          --,user_currency_conversion_type --Version 1.12
          --,currency_conversion_rate  --Version 1.12
          --,currency_conversion_date  --Version 1.12
          ,segment1
          ,segment2
          ,segment3
          ,segment4
          ,segment5
          ,segment6
          ,segment7
          ,reference4
          ,reference10
          ,entered_dr
          ,period_name
          ,group_id)
        VALUES
          ('NEW'
          ,l_ledger
          ,l_end_date
          ,c_glint_exp.currency_code
          ,SYSDATE
          ,0
          ,'A'
          ,l_category
          ,l_source
          --,CASE c_glint_exp.currency_code WHEN 'USD' THEN l_conv_type ELSE NULL END  --Version 1.12
          --,CASE c_glint_exp.currency_code WHEN 'USD' THEN l_conv_rate ELSE NULL END  --Version 1.12
          --,CASE c_glint_exp.currency_code WHEN 'USD' THEN l_conv_date ELSE NULL END  --Version 1.12
          ,c_glint_exp.oracle_product
          ,c_glint_exp.oracle_location
          ,c_glint_exp.oracle_cost_center
          ,c_glint_exp.oracle_account
          ,c_glint_exp.segment5
          ,c_glint_exp.segment6
          ,c_glint_exp.segment7
          ,l_journalname
          ,c_glint_exp.full_name || ' - ' || c_glint_exp.merchant_name
          ,c_glint_exp.sum_amount
          ,p_fperiod
          ,l_group_id);
      
        UPDATE xxcus.xxcusgl_te_audit_tbl
           SET stage = l_sec, stg_date = SYSDATE
         WHERE group_id = l_group_id;
      
      --COMMIT;
      
      END LOOP;
    
      l_sec := 'Enter Loop For Liability.';
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
    
      UPDATE xxcus.xxcusgl_te_audit_tbl
         SET stage = l_sec, stg_date = SYSDATE
       WHERE group_id = l_group_id;
    
      --COMMIT;
    
      FOR c_glint_lib IN (SELECT oracle_product
                                ,card_program_id
                                ,card_program_name
                                ,nvl(currency_code, 'USD') currency_code
                                ,lookup_value
                                ,SUM(line_amount) sum_amount
                            FROM xxcus.xxcusgl_te_accrual_tbl
                           WHERE nvl(credit_card_trx_id, -1) <> 652460
                             AND imported_to_gl = 'N'
                             AND nvl(item_description, 'X') <> 'Personal'
                             AND card_program_name = 'Out of Pocket'
                             AND expense_report_status IN
                                 (SELECT description
                                    FROM apps.fnd_lookup_values
                                   WHERE lookup_type = 'HDS_GL_ACCRUAL_PKG'
                                     AND lookup_code LIKE
                                         'OUTOFPOCKETSTATUS_%'
                                     AND enabled_flag = 'Y'
                                     AND nvl(end_date_active, SYSDATE) >=
                                         SYSDATE)
                             AND org_id = cur_org.org_id
                           GROUP BY card_program_id
                                   ,card_program_name
                                   ,nvl(currency_code, 'USD')
                                   ,oracle_product
                                   ,lookup_value
                          UNION ALL
                          SELECT b.description oracle_product
                                ,NULL
                                ,NULL
                                ,nvl(currency_code, 'USD') currency_code
                                ,NULL
                                ,SUM(a.line_amount) sum_amount
                            FROM xxcus.xxcusgl_te_accrual_tbl a
                                ,apps.fnd_lookup_values       b
                           WHERE nvl(credit_card_trx_id, -1) <> 652460 --1.5 Version added NVL
                             AND a.imported_to_gl = 'N'
                             AND nvl(item_description, 'X') <> 'Personal' --1.5 Version added NVL
                             AND a.card_program_name <> 'Out of Pocket'
                                /* AND credit_card_trx_id IN --1.6 Version
                                   (SELECT trx_id
                                      FROM ap.ap_credit_card_trxns_all t
                                     WHERE t.trx_id = credit_card_trx_id
                                       AND nvl(t.category, 'X') <> 'PERSONAL') --1.7 Version added NVL
                                */ --Version 1.9
                             AND nvl(cc_trns_category, 'X') <> 'PERSONAL' --Version 1.9 
                             AND nvl(a.mcc_code_no, 'X') <> '0' --1.5 Version added NVL
                             AND b.lookup_type = 'HDS_GL_ACCRUAL_PKG'
                             AND b.lookup_code LIKE 'ACCRUE_LIAB_%'
                             AND b.description = a.oracle_product
                             AND org_id = cur_org.org_id
                           GROUP BY b.description, currency_code
                          UNION ALL
                          SELECT l_liability oracle_product
                                ,NULL
                                ,NULL
                                ,nvl(currency_code, 'USD') currency_code
                                ,NULL
                                ,SUM(line_amount) sum_amount
                            FROM xxcus.xxcusgl_te_accrual_tbl
                           WHERE nvl(credit_card_trx_id, -1) <> 652460 --1.5 Version added NVL
                             AND imported_to_gl = 'N'
                             AND nvl(item_description, 'X') <> 'Personal' --1.5 Version added NVL
                             AND card_program_name <> 'Out of Pocket'
                                /*  AND credit_card_trx_id IN --1.6 Version
                                   (SELECT trx_id
                                      FROM ap.ap_credit_card_trxns_all t
                                     WHERE t.trx_id = credit_card_trx_id
                                       AND nvl(t.category, 'X') <> 'PERSONAL') --1.7 Version added NVL
                                */ -- Version 1.9
                             AND nvl(cc_trns_category, 'X') <> 'PERSONAL' --Version 1.9
                             AND nvl(mcc_code_no, 'X') <> '0' --1.5 Version added NVL
                             AND nvl(oracle_product, l_liability) NOT IN
                                 (SELECT description
                                    FROM apps.fnd_lookup_values
                                   WHERE lookup_type = 'HDS_GL_ACCRUAL_PKG' --ver 1.1
                                     AND lookup_code LIKE 'ACCRUE_LIAB_%')
                             AND org_id = cur_org.org_id
                           GROUP BY currency_code)
      LOOP
        --Version 1.3 Ended
      
        l_seg1 := NULL;
        l_seg2 := NULL;
        l_seg3 := NULL;
        l_seg4 := NULL;
      
        IF c_glint_lib.card_program_name = 'Out of Pocket' AND
           c_glint_lib.lookup_value IS NULL
        THEN
          l_seg1 := substr(l_outofpocketdefault, 1, 2);
          l_seg2 := substr(l_outofpocketdefault, 4, 5);
          l_seg3 := substr(l_outofpocketdefault, 10, 4);
          l_seg4 := substr(l_outofpocketdefault, 15, 6);
        
        ELSIF c_glint_lib.card_program_name = 'Out of Pocket' AND
              c_glint_lib.lookup_value IS NOT NULL
        THEN
          l_seg1 := substr(c_glint_lib.lookup_value, 1, 2);
          l_seg2 := substr(c_glint_lib.lookup_value, 4, 5);
          l_seg3 := substr(c_glint_lib.lookup_value, 10, 4);
          l_seg4 := substr(c_glint_lib.lookup_value, 15, 6);
        ELSIF c_glint_lib.card_program_name IS NULL
        THEN
        
          SELECT description
            INTO l_cc_liability
            FROM apps.fnd_lookup_values
           WHERE lookup_type = 'HDS_GL_ACCRUAL_PKG'
             AND lookup_code = 'CARDPROGRAM_' || c_glint_lib.oracle_product;
        
          l_seg1 := substr(l_cc_liability, 1, 2);
          l_seg2 := substr(l_cc_liability, 4, 5);
          l_seg3 := substr(l_cc_liability, 10, 4);
          l_seg4 := substr(l_cc_liability, 15, 6);
        ELSE
          l_seg1 := substr(l_cc_default, 1, 2);
          l_seg2 := substr(l_cc_default, 4, 5);
          l_seg3 := substr(l_cc_default, 10, 4);
          l_seg4 := substr(l_cc_default, 15, 6);
          --Version 1.3 changes to liability ended
        END IF;
      
        l_journalline := 'T.  ' || c_glint_lib.card_program_name ||
                         ' Accrual ' || p_fperiod || ' on ' || l_date;
      
        --Insert the gl interface
        INSERT INTO gl.gl_interface
          (status
          ,ledger_id
          ,accounting_date
          ,currency_code
          ,date_created
          ,created_by
          ,actual_flag
          ,user_je_category_name
          ,user_je_source_name          
          --,user_currency_conversion_type  --Version 1.12
          --,currency_conversion_rate  --Version 1.12
          --,currency_conversion_date  --Version 1.12 
          ,segment1
          ,segment2
          ,segment3
          ,segment4
          ,segment5
          ,segment6
          ,segment7
          ,reference4
          ,reference10
          ,entered_cr
          ,period_name
          ,group_id)
        VALUES
          ('NEW'
          ,l_ledger
          ,l_end_date
          ,c_glint_lib.currency_code
          ,SYSDATE
          ,0
          ,'A'
          ,l_category
          ,l_source
         --,CASE c_glint_lib.currency_code WHEN 'USD' THEN l_conv_type ELSE NULL END  --Version 1.12
         --,CASE c_glint_lib.currency_code WHEN 'USD' THEN l_conv_rate ELSE NULL END  --Version 1.12
         --,CASE c_glint_lib.currency_code WHEN 'USD' THEN l_conv_date ELSE NULL END  --Version 1.12
          ,l_seg1
          ,l_seg2
          ,l_seg3
          ,l_seg4
          ,'00000'
          ,'00000'
          ,'00000'
          ,l_journalname
          ,l_journalline
          ,c_glint_lib.sum_amount
          ,p_fperiod
          ,l_group_id);
      
        UPDATE xxcus.xxcusgl_te_audit_tbl
           SET stage = l_sec, stg_date = SYSDATE
         WHERE group_id = l_group_id;
      
      --COMMIT;
      
      END LOOP;
    
      l_sec := 'Through Loops and beginning Import';
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
    
      UPDATE xxcus.xxcusgl_te_audit_tbl
         SET stage = l_sec, stg_date = SYSDATE
       WHERE group_id = l_group_id;
    
      --COMMIT;
    
      --Intialize Global Submission Variables
      l_can_submit_request := xxcus_misc_pkg.set_responsibility('GLINTERFACE'
                                                               ,'HDS General Ledger Super User');
    
      IF l_can_submit_request
      THEN
        l_globalset := 'Global Variable are set.';
      ELSE
        l_globalset := 'Global Variable are not set.';
        l_sec       := 'Global Variable are not set for the Responsibility of GENERAL_LEDGER_SUPER_USER and the User of GLINTERFACE.';
        fnd_file.put_line(fnd_file.log, l_sec);
        fnd_file.put_line(fnd_file.output, l_sec);
        RAISE program_error;
      END IF;
    
      fnd_file.put_line(fnd_file.log, l_globalset);
      fnd_file.put_line(fnd_file.output, l_globalset);
    
      IF (l_can_submit_request)
      THEN
      
        INSERT INTO gl_interface_control
          (status
          ,je_source_name
          ,group_id
          ,set_of_books_id
          ,interface_run_id
          ,interface_table_name
          ,processed_table_code)
        VALUES
          ('S'
          ,l_jesourcesys
          ,l_group_id
          ,l_ledger
          ,l_group_id
          ,lc_table_name
          ,l_processed_data_action);
      
        --*******************************************
        --Submit the GLLEZL import process
        --*******************************************
        --Submit Import Process under GLINTERFACE under GENERAL_LEDGER_SUPER_USER
        l_sec := 'Submitting Import Process.';
        fnd_file.put_line(fnd_file.log, l_sec);
        fnd_file.put_line(fnd_file.output, l_sec);
      
        --Once initialized then Submit the Concurrent Request Journal Import
        l_req_id_1 := fnd_request.submit_request(application => 'SQLGL'
                                                ,program     => 'GLLEZL'
                                                ,description => NULL
                                                ,start_time  => SYSDATE
                                                ,sub_request => FALSE
                                                ,argument1   => l_group_id
                                                ,argument2   => l_ledger
                                                ,argument3   => 'N'
                                                ,argument4   => NULL
                                                ,argument5   => NULL
                                                ,argument6   => 'N'
                                                ,argument7   => 'O');
      
        COMMIT;
      
        fnd_file.put_line(fnd_file.log, l_req_id_1);
        fnd_file.put_line(fnd_file.output, l_req_id_1);
      
        --Write detail Line Log rows
        l_sec := 'Waiting for Import to finish ' || l_req_id_1;
        fnd_file.put_line(fnd_file.log, l_sec);
        fnd_file.put_line(fnd_file.output, l_sec);
      
        UPDATE xxcus.xxcusgl_te_audit_tbl
           SET stage = l_sec, stg_date = SYSDATE
         WHERE group_id = l_group_id;
      
        COMMIT;
      
        --Wait for Journal Import
        IF (l_req_id_1 != 0)
        THEN
          IF fnd_concurrent.wait_for_request(l_req_id_1
                                            ,6
                                            ,15000
                                            ,l_phase
                                            ,l_status
                                            ,l_dev_phase
                                            ,l_dev_status
                                            ,l_message)
          THEN
            l_error_message := 'ReqID=' || l_req_id_1 || ' DPhase ' ||
                               l_dev_phase || ' DStatus ' || l_dev_status ||
                               ' MSG ' || l_message;
          
            IF l_dev_phase != 'COMPLETE' OR l_dev_status != 'NORMAL'
            THEN
              l_sec := 'An error/warning occured in the Journal Import1, please review the Log for concurrent request ' ||
                       l_req_id_1 || ' - ' || l_error_message || '.';
              fnd_file.put_line(fnd_file.log, l_sec);
              fnd_file.put_line(fnd_file.output, l_sec);
              --RAISE program_error;
              --Version 1.8
              l_subject     := '*** Journal Import Error/Warning for T. Accrual ***';
              l_body_header := '<style type="text/css">.style1 {font-family: Arial, Helvetica, sans-serif;}</style><p class="style1">';
            
              l_body_detail := l_body_detail ||
                               '<BR>Journal Import - concurrent request:  ' ||
                               l_req_id_1;
              l_body_footer := l_body_footer;
            
              l_body := l_body_header || l_body_detail || l_body_footer;
              xxcus_misc_pkg.html_email(p_to            => l_dflt_email
                                       ,p_from          => l_sender
                                       ,p_text          => 'test'
                                       ,p_subject       => l_subject
                                       ,p_html          => l_body
                                       ,p_smtp_hostname => l_host
                                       ,p_smtp_portnum  => l_hostport);
            ELSE
              p_retcode := 0;
            END IF;
          ELSE
            l_sec := 'An error occured in the Journal Import2, please review the Log for concurrent request ' ||
                     l_req_id_1 || ' - ' || l_error_message || '.';
            fnd_file.put_line(fnd_file.log, l_sec);
            fnd_file.put_line(fnd_file.output, l_sec);
            --RAISE program_error;
            --Version 1.8
            l_subject     := '*** Journal Import Error/Warning for Tk Accrual ***';
            l_body_header := '<style type="text/css">.style1 {font-family: Arial, Helvetica, sans-serif;}</style><p class="style1">';
          
            l_body_detail := l_body_detail ||
                             '<BR>Journal Import - concurrent request was not submitted.  ';
            l_body_footer := l_body_footer;
          
            l_body := l_body_header || l_body_detail || l_body_footer;
            xxcus_misc_pkg.html_email(p_to            => l_dflt_email
                                     ,p_from          => l_sender
                                     ,p_text          => 'test'
                                     ,p_subject       => l_subject
                                     ,p_html          => l_body
                                     ,p_smtp_hostname => l_host
                                     ,p_smtp_portnum  => l_hostport);
          END IF;
        END IF;
      END IF;
    
      SELECT gl_je_posting_s.nextval INTO l_posting_id FROM dual;
    
      /* Start the journal post logic */
      --*******************************************
      --Submit the GLPPOS import process
      --*******************************************
      --Submit Import Process under GLINTERFACE under GENERAL_LEDGER_SUPER_USER
    
      l_sec := 'Submitting Posting Process.';
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
    
      UPDATE xxcus.xxcusgl_te_audit_tbl
         SET stage = l_sec, stg_date = SYSDATE
       WHERE group_id = l_group_id;
    
      COMMIT;
    
      --Once initialized then Submit the Concurrent Request Journal Posting
      l_req_id_2 := fnd_request.submit_request(application => 'SQLGL'
                                              ,program     => 'GLPPOSS'
                                              ,description => NULL
                                              ,start_time  => SYSDATE
                                              ,sub_request => FALSE
                                              ,argument1   => l_ledger
                                              ,argument2   => l_access_set
                                              ,argument3   => l_coa
                                              ,argument4   => l_posting_id);
      -- version 1.11 change
      UPDATE gl.gl_je_batches t
         SET status          = 'S'
            ,t.posted_date   = SYSDATE
            ,status_verified = 'Y'
            ,posting_run_id  = l_posting_id
            ,request_id      = l_req_id_2
            ,posted_by       = fnd_global.user_id
       WHERE t.default_period_name = p_fperiod
         AND NAME LIKE l_source || '%' || l_group_id || '%';
    
      COMMIT;
    
      fnd_file.put_line(fnd_file.log, l_req_id_2);
      fnd_file.put_line(fnd_file.output, l_req_id_2);
    
      --Write detail Line Log rows
      l_sec := 'Waiting for Posting to finish ' || l_req_id_2;
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
    
      UPDATE xxcus.xxcusgl_te_audit_tbl
         SET stage = l_sec, stg_date = SYSDATE
       WHERE group_id = l_group_id;
    
      COMMIT;
    
      --Wait for Journal Post
      IF (l_req_id_2 != 0)
      THEN
        IF fnd_concurrent.wait_for_request(l_req_id_2
                                          ,6
                                          ,15000
                                          ,l_phase
                                          ,l_status
                                          ,l_dev_phase
                                          ,l_dev_status
                                          ,l_message)
        THEN
          l_error_message := 'ReqID=' || l_req_id_2 || ' DPhase ' ||
                             l_dev_phase || ' DStatus ' || l_dev_status ||
                             ' MSG ' || l_message;
        
          IF l_dev_phase != 'COMPLETE' OR l_dev_status != 'NORMAL'
          THEN
            l_sec := 'An error occured in the Journal Post1, please review the Log for concurrent request ' ||
                     l_req_id_2 || ' - ' || l_error_message || '.';
            fnd_file.put_line(fnd_file.log, l_sec);
            fnd_file.put_line(fnd_file.output, l_sec);
            -- RAISE program_error;
            --Version 1.8
            l_subject     := '*** Journal Post Error/Warning for Tl Accrual ***';
            l_body_header := '<style type="text/css">.style1 {font-family: Arial, Helvetica, sans-serif;}</style><p class="style1">';
          
            l_body_detail := l_body_detail || '<BR>Journal Posting  - ' ||
                             ' Concurrent Request submitted.:  ' ||
                             l_req_id_2;
            l_body_footer := l_body_footer;
          
            l_body := l_body_header || l_body_detail || l_body_footer;
          
            xxcus_misc_pkg.html_email(p_to            => l_dflt_email
                                     ,p_from          => l_sender
                                     ,p_text          => 'test'
                                     ,p_subject       => l_subject
                                     ,p_html          => l_body
                                     ,p_smtp_hostname => l_host
                                     ,p_smtp_portnum  => l_hostport);
          ELSE
            p_retcode := 0;
            -- version 1.11 change
            l_sec := 'A journal entry request posted successfully for concurrent request ' ||
                     l_req_id_2 || '.';
            fnd_file.put_line(fnd_file.log, l_sec);
            fnd_file.put_line(fnd_file.output, l_sec);
            l_subject     := '*** TE Accrual Journal Post Success ***';
            l_body_header := '<style type="text/css">.style1 {font-family: Arial, Helvetica, sans-serif;}</style><p class="style1">';
          
            l_body_detail := l_body_detail || '<BR>Journal Posting  - ' ||
                             ' Concurrent Request submitted.:  ' ||
                             l_req_id_2 || ' has been successfully done';
            l_body_footer := l_body_footer;
          
            l_body := l_body_header || l_body_detail || l_body_footer;
          
            xxcus_misc_pkg.html_email(p_to            => l_dflt_email
                                     ,p_from          => l_sender
                                     ,p_text          => 'test'
                                     ,p_subject       => l_subject
                                     ,p_html          => l_body
                                     ,p_smtp_hostname => l_host
                                     ,p_smtp_portnum  => l_hostport);
          END IF;
        ELSE
          l_sec := 'An error occured in the Journal Post2, please review the Log for concurrent request ' ||
                   l_req_id_2 || ' - ' || l_error_message || '.';
          fnd_file.put_line(fnd_file.log, l_sec);
          fnd_file.put_line(fnd_file.output, l_sec);
          --RAISE program_error;
          --Version 1.8
          l_subject     := '*** Journal Post Error/Warning for Tl Accrual ***';
          l_body_header := '<style type="text/css">.style1 {font-family: Arial, Helvetica, sans-serif;}</style><p class="style1">';
        
          l_body_detail := l_body_detail || '<BR>Journal Posting  - ' ||
                           ' Concurrent Request submitted.:  ' ||
                           l_req_id_2;
          l_body_footer := l_body_footer;
        
          l_body := l_body_header || l_body_detail || l_body_footer;
        
          xxcus_misc_pkg.html_email(p_to            => l_dflt_email
                                   ,p_from          => l_sender
                                   ,p_text          => 'test'
                                   ,p_subject       => l_subject
                                   ,p_html          => l_body
                                   ,p_smtp_hostname => l_host
                                   ,p_smtp_portnum  => l_hostport);
        END IF;
      ELSE
        l_sec := 'An error occured when trying to submit Journal Post3, the concurrent request was not submitted.';
        fnd_file.put_line(fnd_file.log, l_sec);
        fnd_file.put_line(fnd_file.output, l_sec);
        --RAISE program_error;
        --Version 1.8
        l_subject     := '*** Journal Post Error/Warning for Tl Accrual ***';
        l_body_header := '<style type="text/css">.style1 {font-family: Arial, Helvetica, sans-serif;}</style><p class="style1">';
      
        l_body_detail := l_body_detail || '<BR>Journal Posting  - ' ||
                         ' Concurrent Request submitted.:  ' || l_req_id_2;
        l_body_footer := l_body_footer;
      
        l_body := l_body_header || l_body_detail || l_body_footer;
      
        xxcus_misc_pkg.html_email(p_to            => l_dflt_email
                                 ,p_from          => l_sender
                                 ,p_text          => 'test'
                                 ,p_subject       => l_subject
                                 ,p_html          => l_body
                                 ,p_smtp_hostname => l_host
                                 ,p_smtp_portnum  => l_hostport);
      
      END IF;
    
      /* End the journal post logic */
    
      --Write detail Line Log rows
    
      l_sec := 'Posting finished';
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
    
      UPDATE xxcus.xxcusgl_te_audit_tbl
         SET stage = l_sec, stg_date = SYSDATE
       WHERE group_id = l_group_id;
    
    END LOOP;
  
  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;
      l_err_msg := l_err_msg || l_sec || ' ...Error_Stack...' ||
                   dbms_utility.format_error_stack() ||
                   ' Error_Backtrace...' ||
                   dbms_utility.format_error_backtrace();
      fnd_file.put_line(fnd_file.log, l_err_msg);
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_procedure_name
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_dflt_email
                                          ,p_module            => 'GL');
    
      p_retcode := 2;
      p_errbuf  := l_err_msg;
    
    WHEN OTHERS THEN
    
      l_err_msg := l_err_msg || l_sec || ' ...Error_Stack...' ||
                   dbms_utility.format_error_stack() ||
                   ' Error_Backtrace...' ||
                   dbms_utility.format_error_backtrace();
      fnd_file.put_line(fnd_file.log, l_err_msg);
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_procedure_name
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_dflt_email
                                          ,p_module            => 'GL');
    
      p_retcode := 2;
      p_errbuf  := l_err_msg;
    
  END;

  /*******************************************************************************
  * Procedure:   LOAD_PAYROLL_ACCRUAL
  * Description: This will create an a accrual entry during month end closing
  *              for unprocessed credit card transactions.  Concurrent request
  *              to be run from
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     09/18/2009    Kathy Poling    Initial creation of the procedure  RFC #8251
  1.1     10/29/2009    Kathy Poling    Changed set of books in the control file
                                        to allow Canada to import and post.
                                        Changed the email to the GL Support Team.
                                        Service Ticket 21744, RFC 9490
  1.2     01/20/2011    Luong Vu        Add PeopleSoft Source SR 65406
  1.3     04/12/2012    Kathy Poling    Changed to email if import errors and ends
                                        with warning
  1.11    07/12/2014    Nancy Pahwa     Updated the package with new development standards like changing 
                                        the output variables errbuf and retcode to p_errbuf and 
                                        variables like with v_ to l_. Service Ticket 248924 RFC #40979
  ********************************************************************************/

  PROCEDURE load_payroll_accrual(p_errbuf        OUT VARCHAR2
                                ,p_retcode       OUT NUMBER
                                ,p_fperiod       IN VARCHAR2
                                ,p_lob           IN VARCHAR2
                                ,p_parent        IN VARCHAR2
                                ,p_batch_name    IN VARCHAR2
                                ,p_type_acct     IN VARCHAR2
                                ,p_time          IN VARCHAR2
                                ,p_accrue_branch IN VARCHAR2) IS
  
    --Intialize Variables
    l_sec      VARCHAR2(750);
    l_err_msg  VARCHAR2(2000);
    l_err_code NUMBER;
    l_group_id NUMBER;
  
    --Import variable
    lc_table_name           VARCHAR2(15) := 'GL_INTERFACE';
    l_msg                   VARCHAR2(500);
    l_run_id                NUMBER := 0;
    l_posting_id            NUMBER := 0;
    l_interface_run_id      NUMBER := 0;
    l_processed_data_action VARCHAR2(25) := NULL;
    l_journalname           VARCHAR2(100);
    l_journalprtax          VARCHAR2(100);
    l_batchname             VARCHAR2(100);
    l_product               VARCHAR2(2);
    l_date                  VARCHAR2(15);
    l_req                   NUMBER;
    l_count                 NUMBER;
  
    --Submission variables
    l_globalset          VARCHAR2(100);
    l_can_submit_request BOOLEAN := TRUE;
    l_req_id_1           NUMBER;
    l_req_id_2           NUMBER;
    l_req_id_3           NUMBER;
    l_phase              VARCHAR2(50);
    l_status             VARCHAR2(50);
    l_dev_status         VARCHAR2(50);
    l_dev_phase          VARCHAR2(50);
    l_message            VARCHAR2(50);
    l_error_message      VARCHAR2(150);
  
    --
    l_enter_dr NUMBER;
    l_time     VARCHAR2(10);
    l_tax      VARCHAR2(10);
    l_end_date gl_periods.end_date%TYPE;
    l_period   gl_periods.period_name%TYPE;
    l_fnd_product CONSTANT fnd_flex_values_vl.flex_value_set_id%TYPE := 1014547;
    l_calendar    CONSTANT gl_periods.period_set_name%TYPE := '4-4-QTR';
    l_category    CONSTANT gl_je_categories.user_je_category_name%TYPE := 'Payroll Accrual';
    l_source      CONSTANT gl_je_sources_tl.user_je_source_name%TYPE := 'HDS Accrual';
    l_jesourcesys CONSTANT gl_je_sources_tl.je_source_name%TYPE := '2';
    l_ledger         gl_ledgers.ledger_id%TYPE; --HDS Supply USD  --Version 1.2
    l_procedure_name VARCHAR2(75) := 'XXCUSGL_ACCRUAL.LOAD_PAYROLL_ACCRUAL';
  
    --Email Defaults
    l_dflt_email  fnd_user.email_address%TYPE := 'hds.gloraclesupport@hdsupply.com';
    l_sender      VARCHAR2(100);
    l_host        VARCHAR2(256) := 'mailoutrelay.hdsupply.net';
    l_hostport    VARCHAR2(20) := '25';
    l_sid         VARCHAR2(8);
    l_subject     VARCHAR2(32767) DEFAULT NULL;
    l_body        VARCHAR2(32767) DEFAULT NULL;
    l_body_header VARCHAR2(32767) DEFAULT NULL;
    l_body_detail VARCHAR2(32767) DEFAULT NULL;
    l_body_footer VARCHAR2(32767) DEFAULT NULL;
  
    --Start Main Program
  BEGIN
    --Create output file
    l_sec := 'Starting Payroll Accrual pull.';
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
  
    --Truncate xxcus_CC_ACCRUAL_TBL
    BEGIN
      l_sec := 'Truncate the summary table before loading specified time frame.';
      EXECUTE IMMEDIATE 'TRUNCATE TABLE XXCUS.XXCUSGL_PR_ACCRUAL_TBL';
    
    EXCEPTION
      WHEN OTHERS THEN
        l_err_msg := 'Failed to Truncate XXCUSGL_PR_ACCRUAL_TBL: ' ||
                     SQLERRM;
        RAISE program_error;
    END;
  
    SELECT period_name, end_date
      INTO l_period, l_end_date
      FROM gl.gl_periods
     WHERE period_set_name = l_calendar
       AND period_name = p_fperiod
       AND period_num <> 13;
  
    SELECT description
      INTO l_tax
      FROM apps.fnd_lookup_values h
     WHERE h.lookup_type LIKE '%GL_ACCRUAL_PKG'
       AND lookup_code = 'PAYROLL_TAX'
       AND enabled_flag = 'Y'
       AND nvl(end_date_active, SYSDATE) >= SYSDATE;
  
    INSERT /*+ APPEND */
    INTO xxcus.xxcusgl_pr_accrual_tbl
      (SELECT b.name
             ,h.default_effective_date
             ,l.je_header_id
             ,l.je_line_num
             ,l.accounted_dr
             ,l.accounted_cr
             ,round(l.accounted_dr * p_time, 2) accrual_dr
             ,round(l.accounted_cr * p_time, 2) accrual_cr
             ,round((l.accounted_dr * p_time) * to_number(l_tax) / 100, 2) tax_dr
             ,round((l.accounted_cr * p_time) * to_number(l_tax) / 100, 2) tax_cr
             ,c.segment1
             ,c.segment2
             ,c.segment3
             ,c.segment4
             ,c.segment5
             ,c.segment6
             ,c.segment7
             ,l.description
             ,h.currency_conversion_type
             ,h.currency_code
             ,h.ledger_id
         FROM gl.gl_je_batches        b
             ,gl.gl_je_headers        h
             ,gl.gl_je_lines          l
             ,gl.gl_code_combinations c
        WHERE h.je_header_id = l.je_header_id
          AND l.code_combination_id = c.code_combination_id
          AND je_category = 'Payroll'
          AND je_source IN ('5', '4') -- 4:PeopleSoft and 9:Lawson
          AND h.period_name = p_fperiod
          AND c.segment4 IN
              (SELECT description
                 FROM apps.fnd_lookup_values h
                WHERE lookup_type = 'HDS_GL_ACCRUAL_PKG'
                  AND lookup_code LIKE p_type_acct || '%')
          AND (c.segment1 IN (SELECT child_flex_value_low
                                FROM fnd_flex_value_norm_hierarchy
                               WHERE flex_value_set_id = 1014547
                                 AND parent_flex_value = p_parent
                                 AND range_attribute = 'C') OR
              c.segment1 = p_lob)
          AND b.name = p_batch_name
          AND h.je_batch_id = b.je_batch_id
          AND h.status = 'P'
          AND h.actual_flag = 'A');
  
    COMMIT;
  
    SELECT COUNT(*) INTO l_count FROM xxcus.xxcusgl_pr_accrual_tbl;
  
    SELECT gl_journal_import_s.nextval INTO l_group_id FROM dual;
  
    SELECT trunc(SYSDATE) INTO l_date FROM dual;
  
    IF p_lob IS NOT NULL
    THEN
      l_product := p_lob;
    ELSE
      l_product := p_parent;
    END IF;
  
    l_journalname := 'PAYROLL ACCRUAL _' || l_group_id || ' P: ' ||
                     p_fperiod || ' on ' || l_date;
  
    l_journalprtax := 'PAYROLL TAX ACCRUAL _' || l_group_id || ' P: ' ||
                      p_fperiod || ' on ' || l_date;
  
    l_batchname := l_product || ' - HDS Accrual ' || l_group_id;
  
    INSERT INTO xxcus.xxcusgl_pr_accrual_aud_tbl
    VALUES
      (l_group_id
      ,l_journalname
      ,nvl(fnd_global.conc_request_id, 0)
      ,SYSDATE
      ,nvl(fnd_global.user_name, 'NA')
      ,nvl(fnd_global.resp_name, 'NA')
      ,p_fperiod
      ,0
      ,'Beginning Program'
      ,SYSDATE);
  
    COMMIT;
  
    l_sec := 'Enter Loop For Inserting GL Interface.';
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
  
    IF p_time = .5
    THEN
      l_time := 'One Week';
    ELSE
      l_time := 'Two Weeks';
    END IF;
  
    UPDATE xxcus.xxcusgl_pr_accrual_aud_tbl
       SET stage = l_sec, stg_date = SYSDATE
     WHERE group_id = l_group_id;
  
    COMMIT;
  
    FOR c_glint_exp IN (SELECT SUM(accounted_dr - accounted_cr) accounted_amt
                              ,SUM(accrual_dr - accrual_cr) accrual_amt
                              ,SUM(tax_dr - tax_cr) tax_amt
                              ,segment1
                              ,segment2
                              ,segment3
                              ,segment4
                              ,segment5
                              ,segment6
                              ,segment7
                              ,ledger_id
                              ,currency_code
                              ,currency_conversion_type
                          FROM xxcus.xxcusgl_pr_accrual_tbl
                         GROUP BY segment1
                                 ,segment2
                                 ,segment3
                                 ,segment4
                                 ,segment5
                                 ,segment6
                                 ,segment7
                                 ,ledger_id
                                 ,currency_code
                                 ,currency_conversion_type)
    LOOP
    
      l_enter_dr := NULL;
    
      IF c_glint_exp.accrual_amt <> 0
      THEN
        l_enter_dr := c_glint_exp.accrual_amt;
      END IF;
    
      --Insert the gl interface for payroll accrual expense
      INSERT INTO gl.gl_interface
        (status
        ,ledger_id
        ,accounting_date
        ,currency_code
        ,date_created
        ,created_by
        ,actual_flag
        ,user_je_category_name
        ,user_je_source_name
        ,segment1
        ,segment2
        ,segment3
        ,segment4
        ,segment5
        ,segment6
        ,segment7
        ,reference1
        ,reference4
        ,reference10
        ,entered_dr
        ,period_name
        ,group_id)
      VALUES
        ('NEW'
        ,c_glint_exp.ledger_id
        ,l_end_date
        ,c_glint_exp.currency_code
        ,SYSDATE
        ,0
        ,'A'
        ,l_category
        ,l_source
        ,c_glint_exp.segment1
        ,c_glint_exp.segment2
        ,c_glint_exp.segment3
        ,c_glint_exp.segment4
        ,c_glint_exp.segment5
        ,c_glint_exp.segment6
        ,c_glint_exp.segment7
        ,l_batchname
        ,l_journalname
        ,c_glint_exp.segment1 || ' - Payroll Accrual - ' || l_time
        ,l_enter_dr
        ,p_fperiod
        ,l_group_id);
    
      l_enter_dr := NULL;
    
      IF c_glint_exp.tax_amt <> 0
      THEN
        l_enter_dr := c_glint_exp.tax_amt;
      END IF;
    
      --Insert the gl interface for payroll tax expense
      INSERT INTO gl.gl_interface
        (status
        ,ledger_id
        ,accounting_date
        ,currency_code
        ,date_created
        ,created_by
        ,actual_flag
        ,user_je_category_name
        ,user_je_source_name
        ,segment1
        ,segment2
        ,segment3
        ,segment4
        ,segment5
        ,segment6
        ,segment7
        ,reference1
        ,reference4
        ,reference10
        ,entered_dr
        ,period_name
        ,group_id)
      VALUES
        ('NEW'
        ,c_glint_exp.ledger_id
        ,l_end_date
        ,c_glint_exp.currency_code
        ,SYSDATE
        ,0
        ,'A'
        ,l_category
        ,l_source
        ,c_glint_exp.segment1
        ,c_glint_exp.segment2
        ,c_glint_exp.segment3
        ,'617010'
        ,c_glint_exp.segment5
        ,c_glint_exp.segment6
        ,c_glint_exp.segment7
        ,l_batchname
        ,l_journalprtax
        ,c_glint_exp.segment1 || ' - Payroll Accrual - ' || l_time
        ,l_enter_dr
        ,p_fperiod
        ,l_group_id);
    
      --Insert the gl interface for payroll tax credit
    
      IF p_accrue_branch IS NULL
      THEN
      
        l_enter_dr := NULL;
      
        IF c_glint_exp.tax_amt <> 0
        THEN
          l_enter_dr := c_glint_exp.tax_amt;
        END IF;
      
        INSERT INTO gl.gl_interface
          (status
          ,ledger_id
          ,accounting_date
          ,currency_code
          ,date_created
          ,created_by
          ,actual_flag
          ,user_je_category_name
          ,user_je_source_name
          ,segment1
          ,segment2
          ,segment3
          ,segment4
          ,segment5
          ,segment6
          ,segment7
          ,reference1
          ,reference4
          ,reference10
          ,entered_cr
          ,period_name
          ,group_id)
        VALUES
          ('NEW'
          ,c_glint_exp.ledger_id
          ,l_end_date
          ,c_glint_exp.currency_code
          ,SYSDATE
          ,0
          ,'A'
          ,l_category
          ,l_source
          ,c_glint_exp.segment1
          ,c_glint_exp.segment2
          ,c_glint_exp.segment3
          ,'224100'
          ,c_glint_exp.segment5
          ,c_glint_exp.segment6
          ,c_glint_exp.segment7
          ,l_batchname
          ,l_journalprtax
          ,c_glint_exp.segment1 || ' - Payroll Accrual - ' || l_time
          ,l_enter_dr
          ,p_fperiod
          ,l_group_id);
      
        --Insert the gl interface for payroll accrual credit
        l_enter_dr := NULL;
      
        IF c_glint_exp.accrual_amt <> 0
        THEN
          l_enter_dr := c_glint_exp.accrual_amt;
        END IF;
      
        INSERT INTO gl.gl_interface
          (status
          ,ledger_id
          ,accounting_date
          ,currency_code
          ,date_created
          ,created_by
          ,actual_flag
          ,user_je_category_name
          ,user_je_source_name
          ,segment1
          ,segment2
          ,segment3
          ,segment4
          ,segment5
          ,segment6
          ,segment7
          ,reference1
          ,reference4
          ,reference10
          ,entered_cr
          ,period_name
          ,group_id)
        VALUES
          ('NEW'
          ,c_glint_exp.ledger_id
          ,l_end_date
          ,c_glint_exp.currency_code
          ,SYSDATE
          ,0
          ,'A'
          ,l_category
          ,l_source
          ,c_glint_exp.segment1
          ,c_glint_exp.segment2
          ,c_glint_exp.segment3
          ,'221100'
          ,c_glint_exp.segment5
          ,c_glint_exp.segment6
          ,c_glint_exp.segment7
          ,l_batchname
          ,l_journalname
          ,c_glint_exp.segment1 || ' - Payroll Accrual - ' || l_time
          ,l_enter_dr
          ,p_fperiod
          ,l_group_id);
      
      END IF;
    END LOOP;
  
    --if p_accrue_branch is not null then summarize offset based on parameter
    IF p_accrue_branch IS NOT NULL
    THEN
    
      FOR c_glint_branch IN (SELECT SUM(accounted_dr - accounted_cr) accounted_amt
                                   ,SUM(accrual_dr - accrual_cr) accrual_amt
                                   ,SUM(tax_dr - tax_cr) tax_amt
                                   ,segment1
                                   ,segment3
                                   ,segment5
                                   ,segment6
                                   ,segment7
                                   ,ledger_id
                                   ,currency_code
                                   ,currency_conversion_type
                               FROM xxcus.xxcusgl_pr_accrual_tbl
                              GROUP BY segment1
                                      ,segment3
                                      ,segment5
                                      ,segment6
                                      ,segment7
                                      ,ledger_id
                                      ,currency_code
                                      ,currency_conversion_type)
      LOOP
      
        l_enter_dr := NULL;
        --Insert the gl interface for payroll tax credit with default of specified branch from parameter
        IF c_glint_branch.tax_amt <> 0
        THEN
          l_enter_dr := c_glint_branch.tax_amt;
        END IF;
      
        INSERT INTO gl.gl_interface
          (status
          ,ledger_id
          ,accounting_date
          ,currency_code
          ,date_created
          ,created_by
          ,actual_flag
          ,user_je_category_name
          ,user_je_source_name
          ,segment1
          ,segment2
          ,segment3
          ,segment4
          ,segment5
          ,segment6
          ,segment7
          ,reference1
          ,reference4
          ,reference10
          ,entered_cr
          ,period_name
          ,group_id)
        VALUES
          ('NEW'
          ,c_glint_branch.ledger_id
          ,l_end_date
          ,c_glint_branch.currency_code
          ,SYSDATE
          ,0
          ,'A'
          ,l_category
          ,l_source
          ,c_glint_branch.segment1
          ,p_accrue_branch
          ,c_glint_branch.segment3
          ,'224100'
          ,c_glint_branch.segment5
          ,c_glint_branch.segment6
          ,c_glint_branch.segment7
          ,l_batchname
          ,l_journalprtax
          ,c_glint_branch.segment1 || ' - Payroll Accrual - ' || l_time
          ,l_enter_dr
          ,p_fperiod
          ,l_group_id);
      
        --Insert the gl interface for payroll accrual credit with default of specified branch from parameter
        l_enter_dr := NULL;
      
        IF c_glint_branch.accrual_amt <> 0
        THEN
          l_enter_dr := c_glint_branch.accrual_amt;
        END IF;
      
        INSERT INTO gl.gl_interface
          (status
          ,ledger_id
          ,accounting_date
          ,currency_code
          ,date_created
          ,created_by
          ,actual_flag
          ,user_je_category_name
          ,user_je_source_name
          ,segment1
          ,segment2
          ,segment3
          ,segment4
          ,segment5
          ,segment6
          ,segment7
          ,reference1
          ,reference4
          ,reference10
          ,entered_cr
          ,period_name
          ,group_id)
        VALUES
          ('NEW'
          ,c_glint_branch.ledger_id
          ,l_end_date
          ,c_glint_branch.currency_code
          ,SYSDATE
          ,0
          ,'A'
          ,l_category
          ,l_source
          ,c_glint_branch.segment1
          ,p_accrue_branch
          ,c_glint_branch.segment3
          ,'221100'
          ,c_glint_branch.segment5
          ,c_glint_branch.segment6
          ,c_glint_branch.segment7
          ,l_batchname
          ,l_journalname
          ,c_glint_branch.segment1 || ' - Payroll Accrual - ' || l_time
          ,l_enter_dr
          ,p_fperiod
          ,l_group_id);
      
      END LOOP;
    END IF;
  
    l_sec := 'Through Loops and beginning Import';
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
  
    UPDATE xxcus.xxcusgl_pr_accrual_aud_tbl
       SET stage = l_sec, stg_date = SYSDATE
     WHERE group_id = l_group_id;
  
    COMMIT;
  
    --Intialize Global Submission Variables
    l_can_submit_request := xxcus_misc_pkg.set_responsibility('GLINTERFACE'
                                                             ,'HDS General Ledger Super User');
  
    IF l_can_submit_request
    THEN
      l_globalset := 'Global Variable are set.';
    ELSE
      l_globalset := 'Global Variable are not set.';
      l_sec       := 'Global Variable are not set for the Responsibility of GENERAL_LEDGER_SUPER_USER and the User of GLINTERFACE.';
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      RAISE program_error;
    END IF;
  
    fnd_file.put_line(fnd_file.log, l_globalset);
    fnd_file.put_line(fnd_file.output, l_globalset);
  
    IF (l_can_submit_request)
    THEN
    
      BEGIN
        SELECT DISTINCT ledger_id
          INTO l_ledger
          FROM xxcus.xxcusgl_pr_accrual_tbl;
      EXCEPTION
        WHEN OTHERS THEN
          l_ledger := 2061;
      END;
    
      INSERT INTO gl_interface_control
        (status
        ,je_source_name
        ,group_id
        ,set_of_books_id
        ,interface_run_id
        ,interface_table_name
        ,processed_table_code)
      VALUES
        ('S'
        ,l_jesourcesys
        ,l_group_id
        ,l_ledger
        ,l_group_id
        ,lc_table_name
        ,l_processed_data_action);
    
      --*******************************************
      --Submit the GLLEZL import process
      --*******************************************
      --Submit Import Process under GLINTERFACE under GENERAL_LEDGER_SUPER_USER
      l_sec := 'Submitting Import Process.';
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
    
      --Once initialized then Submit the Concurrent Request Journal Import
      l_req_id_1 := fnd_request.submit_request(application => 'SQLGL'
                                              ,program     => 'GLLEZL'
                                              ,description => NULL
                                              ,start_time  => SYSDATE
                                              ,sub_request => FALSE
                                              ,argument1   => l_group_id
                                              ,argument2   => l_ledger
                                              ,argument3   => 'N'
                                              ,argument4   => NULL
                                              ,argument5   => NULL
                                              ,argument6   => 'N'
                                              ,argument7   => 'O');
    
      COMMIT;
    
      fnd_file.put_line(fnd_file.log, l_req_id_1);
      fnd_file.put_line(fnd_file.output, l_req_id_1);
    
      --Write detail Line Log rows
      l_sec := 'Waiting for Import to finish ' || l_req_id_1;
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
    
      UPDATE xxcus.xxcusgl_pr_accrual_aud_tbl
         SET stage = l_sec, stg_date = SYSDATE
       WHERE group_id = l_group_id;
    
      COMMIT;
    
      --Wait for Journal Import
      IF (l_req_id_1 != 0)
      THEN
        IF fnd_concurrent.wait_for_request(l_req_id_1
                                          ,6
                                          ,15000
                                          ,l_phase
                                          ,l_status
                                          ,l_dev_phase
                                          ,l_dev_status
                                          ,l_message)
        THEN
          l_error_message := 'ReqID=' || l_req_id_1 || ' DPhase ' ||
                             l_dev_phase || ' DStatus ' || l_dev_status ||
                             ' MSG ' || l_message;
        
          IF l_dev_phase != 'COMPLETE' OR l_dev_status != 'NORMAL'
          THEN
            l_sec := 'An error occured in the Journal Import1, please review the Log for concurrent request ' ||
                     l_req_id_1 || ' - ' || l_error_message || '.';
            fnd_file.put_line(fnd_file.log, l_sec);
            fnd_file.put_line(fnd_file.output, l_sec);
            --RAISE program_error;
            --Version 1.3
            l_subject     := '*** Journal Import3 Error/Warning for Payroll Accrual ***';
            l_body_header := '<style type="text/css">.style1 {font-family: Arial, Helvetica, sans-serif;}</style><p class="style1">';
          
            l_body_detail := l_body_detail ||
                             '<BR>Journal Import - concurrent request:  ' ||
                             l_req_id_1;
            l_body_footer := l_body_footer;
          
            l_body := l_body_header || l_body_detail || l_body_footer;
            xxcus_misc_pkg.html_email(p_to            => l_dflt_email
                                     ,p_from          => l_sender
                                     ,p_text          => 'test'
                                     ,p_subject       => l_subject
                                     ,p_html          => l_body
                                     ,p_smtp_hostname => l_host
                                     ,p_smtp_portnum  => l_hostport);
          ELSE
            p_retcode := 0;
          END IF;
        ELSE
          l_sec := 'An error occured in the Journal Import2, please review the Log for concurrent request ' ||
                   l_req_id_1 || ' - ' || l_error_message || '.';
          fnd_file.put_line(fnd_file.log, l_sec);
          fnd_file.put_line(fnd_file.output, l_sec);
          --RAISE program_error;
          --Version 1.3
          l_subject     := '*** Journal Import3 Error/Warning for Payroll Accrual ***';
          l_body_header := '<style type="text/css">.style1 {font-family: Arial, Helvetica, sans-serif;}</style><p class="style1">';
        
          l_body_detail := l_body_detail ||
                           '<BR>Journal Import - concurrent request:  ' ||
                           l_req_id_1;
          l_body_footer := l_body_footer;
        
          l_body := l_body_header || l_body_detail || l_body_footer;
          xxcus_misc_pkg.html_email(p_to            => l_dflt_email
                                   ,p_from          => l_sender
                                   ,p_text          => 'test'
                                   ,p_subject       => l_subject
                                   ,p_html          => l_body
                                   ,p_smtp_hostname => l_host
                                   ,p_smtp_portnum  => l_hostport);
        END IF;
      ELSE
        l_sec := 'An error occured when trying to submit Journal Import3, the concurrent request was not submitted.';
        fnd_file.put_line(fnd_file.log, l_sec);
        fnd_file.put_line(fnd_file.output, l_sec);
        --RAISE program_error;
        --Version 1.3
        l_subject     := '*** Journal Import3 Error/Warning for Payroll Accrual ***';
        l_body_header := '<style type="text/css">.style1 {font-family: Arial, Helvetica, sans-serif;}</style><p class="style1">';
      
        l_body_detail := l_body_detail ||
                         '<BR>Journal Import - concurrent request was not submitted.  ';
        l_body_footer := l_body_footer;
      
        l_body := l_body_header || l_body_detail || l_body_footer;
        xxcus_misc_pkg.html_email(p_to            => l_dflt_email
                                 ,p_from          => l_sender
                                 ,p_text          => 'test'
                                 ,p_subject       => l_subject
                                 ,p_html          => l_body
                                 ,p_smtp_hostname => l_host
                                 ,p_smtp_portnum  => l_hostport);
      END IF;
    END IF;
  
    --Write detail Line Log rows
    l_sec := 'Import finished';
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
  
    UPDATE xxcus.xxcusgl_pr_accrual_aud_tbl
       SET stage = l_sec, stg_date = SYSDATE
     WHERE group_id = l_group_id;
  
    COMMIT;
  
  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;
      l_err_msg := l_err_msg || l_sec || ' ...Error_Stack...' ||
                   dbms_utility.format_error_stack() ||
                   ' Error_Backtrace...' ||
                   dbms_utility.format_error_backtrace();
      fnd_file.put_line(fnd_file.log, l_err_msg);
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_procedure_name
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_dflt_email
                                          ,p_module            => 'GL');
    
      p_retcode := 2;
      p_errbuf  := l_err_msg;
    
    WHEN OTHERS THEN
      l_err_msg := l_err_msg || l_sec || ' ...Error_Stack...' ||
                   dbms_utility.format_error_stack() ||
                   ' Error_Backtrace...' ||
                   dbms_utility.format_error_backtrace();
      fnd_file.put_line(fnd_file.log, l_err_msg);
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_procedure_name
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_dflt_email
                                          ,p_module            => 'GL');
    
      p_retcode := 2;
      p_errbuf  := l_err_msg;
    
  END load_payroll_accrual;

  /*******************************************************************************
  * Procedure:   LOAD_HEADCOUNT
  * Description: This will create an a accrual entry during month end closing
  *              for unprocessed credit card transactions.  Concurrent request
  *              to be run from
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     06/20/2011    Kathy Poling    Initial creation of the procedure copied from
                                        R11
  1.1     04/08/2012    Kathy Poling    Changed to email if import of post fails
                                        but process will continue since support and
                                        import and post if needed
                                        Service Ticket # 135785
  1.10    05/14/2014    John Bozik      SR 250031 change to exclude future date employees                                        
  1.11    07/12/2014    Nancy Pahwa     Added the success notification on posting compelition.
                                        Updated the package with new development standards like changing 
                                        the output variables errbuf and retcode to p_errbuf and 
                                        variables like with v_ to l_. Also made the change to the EXCEPTION
                                        WHEN program_error THEN Service Ticket 248924 RFC #40979
  
  ********************************************************************************/
  PROCEDURE load_headcount(p_errbuf  OUT VARCHAR2
                          ,p_retcode OUT NUMBER
                          ,p_fperiod IN VARCHAR2) IS
  
    --Intialize Variables
    l_sec      VARCHAR2(750);
    l_err_msg  VARCHAR2(2000);
    l_err_code NUMBER;
    l_group_id NUMBER;
    l_count    NUMBER;
  
    --Import variable
    lc_table_name           VARCHAR2(15) := 'GL_INTERFACE';
    l_msg                   VARCHAR2(500);
    l_run_id                NUMBER := 0;
    l_posting_id            NUMBER := 0;
    l_interface_run_id      NUMBER := 0;
    l_processed_data_action VARCHAR2(25) := NULL;
    l_journalname           VARCHAR2(100);
    l_batchname             VARCHAR2(100);
    l_date                  VARCHAR2(15);
    l_req                   NUMBER;
  
    --Submission variables
    l_globalset          VARCHAR2(100);
    l_can_submit_request BOOLEAN := TRUE;
    l_req_id_1           NUMBER;
    l_req_id_2           NUMBER;
    l_req_id_3           NUMBER;
    l_phase              VARCHAR2(50);
    l_status             VARCHAR2(50);
    l_dev_status         VARCHAR2(50);
    l_dev_phase          VARCHAR2(50);
    l_message            VARCHAR2(50);
    l_error_message      VARCHAR2(150);
  
    --
    l_time     VARCHAR2(10);
    l_end_date gl_periods.end_date%TYPE;
    l_period   gl_periods.period_name%TYPE;
    l_calendar    CONSTANT gl_periods.period_set_name%TYPE := '4-4-QTR';
    l_category    CONSTANT gl_je_categories.user_je_category_name%TYPE := 'Headcount';
    l_source      CONSTANT gl_je_sources_tl.user_je_source_name%TYPE := 'HDS Accrual';
    l_jesourcesys CONSTANT gl_je_sources_tl.je_source_name%TYPE := '2';
    l_coa         CONSTANT gl_code_combinations.chart_of_accounts_id%TYPE := 50328; --Hughes/Main Chart of Accounts
    l_ledger      CONSTANT gl_ledgers.ledger_id%TYPE := 2061; --HDS Supply USD    --Version 1.1
    l_ledger_cn   CONSTANT gl_ledgers.ledger_id%TYPE := 2063; --HDS Supply CAD    --Version 1.1
    l_procedure_name VARCHAR2(75) := 'XXCUS_GL_ACCRUAL.LOAD_HEADCOUNT';
    l_fnd_location CONSTANT fnd_flex_values_vl.flex_value_set_id%TYPE := 1014548; --Location/Branch segment2
    l_fnd_product  CONSTANT fnd_flex_values_vl.flex_value_set_id%TYPE := 1014547; --Product segment1
    l_product      CONSTANT fnd_flex_value_sets.flex_value_set_name%TYPE := 'XXCUS_GLHC';
    l_product_cn   CONSTANT fnd_flex_value_sets.flex_value_set_name%TYPE := 'XXCUS_GLHC_CN';
    l_access_set NUMBER := 1042;
  
    --Email Defaults
    l_dflt_email  fnd_user.email_address%TYPE := 'hds.gloraclesupport@hdsupply.com';
    l_sender      VARCHAR2(100);
    l_host        VARCHAR2(256) := 'mailoutrelay.hdsupply.net';
    l_hostport    VARCHAR2(20) := '25';
    l_sid         VARCHAR2(8);
    l_subject     VARCHAR2(32767) DEFAULT NULL;
    l_body        VARCHAR2(32767) DEFAULT NULL;
    l_body_header VARCHAR2(32767) DEFAULT NULL;
    l_body_detail VARCHAR2(32767) DEFAULT NULL;
    l_body_footer VARCHAR2(32767) DEFAULT NULL;
  
    --Start Main Program
  BEGIN
    --Create output file
    l_sec := 'Starting Headcount pull.';
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
  
    SELECT lower(NAME) INTO l_sid FROM v$database;
    l_sender := 'Oracle.Applications_' || l_sid || '@hdsupply.com';
  
    SELECT period_name, end_date
      INTO l_period, l_end_date
      FROM gl.gl_periods
     WHERE period_set_name = l_calendar
       AND period_name = p_fperiod
       AND period_num <> 13;
  
    --  Check to see if Employee in APXCMMN has data
    SELECT COUNT(*)
      INTO l_count
      FROM apxcmmn.hr_employee_all_vw@apxprd_lnk.hsi.hughessupply.com;
  
    IF l_count < 1
    THEN
      l_sec := 'No data in apxcmmn.hr_employee_all_vw ' || l_err_msg;
      -- APXCMMN Employee doesn't have any data.
      RAISE program_error;
    END IF;
  
    BEGIN
    
      --l_ledger := 2061;     --Version 1.1
    
      SELECT gl_journal_import_s.nextval INTO l_group_id FROM dual;
    
      SELECT trunc(SYSDATE) INTO l_date FROM dual;
    
      l_journalname := 'Headcount _' || l_group_id || ' P: ' || p_fperiod ||
                       ' on ' || l_date;
    
      l_batchname := l_source || ' - Headcount ' || l_group_id;
    
      INSERT INTO xxcus.xxcusgl_headcount_aud_tbl
      VALUES
        (l_group_id
        ,l_journalname
        ,nvl(fnd_global.conc_request_id, 0)
        ,SYSDATE
        ,nvl(fnd_global.user_name, 'NA')
        ,nvl(fnd_global.resp_name, 'NA')
        ,p_fperiod
        ,0
        ,'Beginning Program'
        ,SYSDATE
        ,l_ledger);
    
      COMMIT;
    
      l_sec := 'Enter Loop For Inserting GL Interface.';
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
    
      UPDATE xxcus.xxcusgl_headcount_aud_tbl
         SET stage = l_sec, stg_date = SYSDATE
       WHERE group_id = l_group_id;
    
      COMMIT;
    END;
    --US JE
    BEGIN
      FOR c_headcount IN (SELECT l.entrp_entity
                                ,l.entrp_loc
                                ,l.entrp_cc
                                ,(CASE
                                   WHEN TRIM(empl_type) IN ('S', 'E') AND
                                        TRIM(substr(deptid, 5, 3)) NOT IN
                                        ('060', '100') THEN
                                    '991010'
                                   WHEN TRIM(empl_type) = 'H' AND
                                        TRIM(substr(rpad(deptid, 10, '0')
                                                   ,5
                                                   ,3)) NOT IN ('060', '100') THEN
                                    '991011'
                                   WHEN TRIM(empl_type) IN ('S', 'E') AND
                                        TRIM(substr(deptid, 5, 3)) IN
                                        ('060', '100') THEN
                                    '991110'
                                   WHEN TRIM(empl_type) = 'H' AND
                                        TRIM(substr(deptid, 5, 3)) IN
                                        ('060', '100') THEN
                                    '991111'
                                   ELSE
                                    '991010'
                                 END) segment4
                                ,1 debit
                                ,fvt2.description || '   ' ||
                                 TRIM(e.first_name) || '   ' ||
                                 TRIM(e.last_name) || ' - ' || (CASE
                                   WHEN TRIM(empl_status) IN ('A', 'N') THEN
                                    'Active'
                                   ELSE
                                    'Inactive'
                                 END) description
                                ,TRIM(empl_status)
                                ,TRIM(empl_type)
                                ,substr(deptid, 5, 3)
                            FROM apxcmmn.hr_employee_all_vw@apxprd_lnk.hsi.hughessupply.com e
                                ,apps.xxcus_location_code_vw                                l
                                ,apps.fnd_flex_values_vl                                    fvt2
                           WHERE TRIM(e.fru) = l.fru
                             AND fvt2.flex_value = l.entrp_loc
                             AND fvt2.flex_value_set_id = l_fnd_location
                             AND TRIM(e.hr_status) = 'A'
                             AND e.hire_dt <= SYSDATE --Version 1.10
                             AND l.entrp_entity IN
                                 (SELECT fv2.flex_value
                                    FROM applsys.fnd_flex_values_tl  fvt2
                                        ,applsys.fnd_flex_values     fv2
                                        ,applsys.fnd_flex_value_sets fvs2
                                   WHERE fvt2.flex_value_id =
                                         fv2.flex_value_id
                                     AND fv2.flex_value_set_id =
                                         fvs2.flex_value_set_id
                                     AND fvs2.flex_value_set_name = l_product
                                     AND fvt2.language = 'US'
                                     AND enabled_flag = 'Y'))
      LOOP
      
        --Insert the gl interface for US Headcount
        INSERT INTO gl.gl_interface
          (status
          ,ledger_id
          ,accounting_date
          ,currency_code
          ,date_created
          ,created_by
          ,actual_flag
          ,user_je_category_name
          ,user_je_source_name
          ,segment1
          ,segment2
          ,segment3
          ,segment4
          ,segment5
          ,segment6
          ,segment7
          ,reference1
          ,reference4
          ,reference10
          ,entered_dr
          ,period_name
          ,group_id)
        VALUES
          ('NEW'
          ,l_ledger
          ,l_end_date
          ,'STAT'
          , --c_headcount.currency_code,
           SYSDATE
          ,0
          ,'A'
          ,l_category
          ,l_source
          ,c_headcount.entrp_entity
          ,c_headcount.entrp_loc
          ,c_headcount.entrp_cc
          ,c_headcount.segment4
          ,'00000' --segment5
          ,'00000' --segment6
          ,'00000' --segment7
          ,l_batchname
          ,l_journalname
          ,c_headcount.description
          ,c_headcount.debit
          ,p_fperiod
          ,l_group_id);
      END LOOP;
    
      l_sec := 'Through Loops and ready to Import';
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
    
      UPDATE xxcus.xxcusgl_headcount_aud_tbl
         SET stage = l_sec, stg_date = SYSDATE
       WHERE group_id = l_group_id;
    
      COMMIT;
    END;
  
    --l_ledger      := NULL;    --Version 1.1
    l_journalname := NULL;
    l_batchname   := NULL;
  
    BEGIN
    
      --l_ledger := 2063;    --Version 1.1
    
      SELECT gl_journal_import_s.nextval INTO l_group_id FROM dual;
    
      SELECT trunc(SYSDATE) INTO l_date FROM dual;
    
      l_journalname := 'Headcount CAD_' || l_group_id || ' P: ' ||
                       p_fperiod || ' on ' || l_date;
    
      l_batchname := l_source || ' - Headcount CAD ' || l_group_id;
    
      INSERT INTO xxcus.xxcusgl_headcount_aud_tbl
      VALUES
        (l_group_id
        ,l_journalname
        ,nvl(fnd_global.conc_request_id, 0)
        ,SYSDATE
        ,nvl(fnd_global.user_name, 'NA')
        ,nvl(fnd_global.resp_name, 'NA')
        ,p_fperiod
        ,0
        ,'Beginning Program'
        ,SYSDATE
        ,l_ledger_cn);
    
      COMMIT;
    
      l_sec := 'Enter Loop For Inserting GL Interface.';
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
    
      UPDATE xxcus.xxcusgl_headcount_aud_tbl
         SET stage = l_sec, stg_date = SYSDATE
       WHERE group_id = l_group_id;
    
      COMMIT;
    END;
    --CN JE
    BEGIN
    
      FOR c_headcount IN (SELECT l.entrp_entity
                                ,l.entrp_loc
                                ,l.entrp_cc
                                ,(CASE
                                   WHEN TRIM(empl_type) IN ('S', 'E') AND
                                        TRIM(substr(deptid, 5, 3)) NOT IN
                                        ('060', '100') THEN
                                    '991010'
                                   WHEN TRIM(empl_type) = 'H' AND
                                        TRIM(substr(rpad(deptid, 10, '0')
                                                   ,5
                                                   ,3)) NOT IN ('060', '100') THEN
                                    '991011'
                                   WHEN TRIM(empl_type) IN ('S', 'E') AND
                                        TRIM(substr(deptid, 5, 3)) IN
                                        ('060', '100') THEN
                                    '991110'
                                   WHEN TRIM(empl_type) = 'H' AND
                                        TRIM(substr(deptid, 5, 3)) IN
                                        ('060', '100') THEN
                                    '991111'
                                   ELSE
                                    '991010'
                                 END) segment4
                                ,1 debit
                                ,fvt2.description || '   ' ||
                                 TRIM(e.first_name) || '   ' ||
                                 TRIM(e.last_name) || ' - ' || (CASE
                                   WHEN TRIM(empl_status) IN ('A', 'N') THEN
                                    'Active'
                                   ELSE
                                    'Inactive'
                                 END) description
                                ,TRIM(empl_status)
                                ,TRIM(empl_type)
                                ,substr(deptid, 5, 3)
                            FROM apxcmmn.hr_employee_all_vw@apxprd_lnk.hsi.hughessupply.com e
                                ,apps.xxcus_location_code_vw                                l
                                ,apps.fnd_flex_values_vl                                    fvt2
                           WHERE TRIM(e.fru) = l.fru
                             AND fvt2.flex_value = l.entrp_loc
                             AND fvt2.flex_value_set_id = l_fnd_location
                             AND TRIM(e.hr_status) = 'A'
                             AND e.hire_dt <= SYSDATE --Version 1.10
                             AND l.entrp_entity IN
                                 (SELECT fv2.flex_value
                                    FROM applsys.fnd_flex_values_tl  fvt2
                                        ,applsys.fnd_flex_values     fv2
                                        ,applsys.fnd_flex_value_sets fvs2
                                   WHERE fvt2.flex_value_id =
                                         fv2.flex_value_id
                                     AND fv2.flex_value_set_id =
                                         fvs2.flex_value_set_id
                                     AND fvs2.flex_value_set_name =
                                         l_product_cn
                                     AND fvt2.language = 'US'
                                     AND enabled_flag = 'Y'))
      LOOP
      
        --Insert the gl interface for CN Headcount
        INSERT INTO gl.gl_interface
          (status
          ,ledger_id
          ,accounting_date
          ,currency_code
          ,date_created
          ,created_by
          ,actual_flag
          ,user_je_category_name
          ,user_je_source_name
          ,segment1
          ,segment2
          ,segment3
          ,segment4
          ,segment5
          ,segment6
          ,segment7
          ,reference1
          ,reference4
          ,reference10
          ,entered_dr
          ,period_name
          ,group_id)
        VALUES
          ('NEW'
          ,l_ledger_cn
          ,l_end_date
          ,'STAT'
          ,SYSDATE
          ,0
          ,'A'
          ,l_category
          ,l_source
          ,c_headcount.entrp_entity
          ,c_headcount.entrp_loc
          ,c_headcount.entrp_cc
          ,c_headcount.segment4
          ,'00000'
          ,'00000'
          ,'00000'
          ,l_batchname
          ,l_journalname
          ,c_headcount.description
          ,c_headcount.debit
          ,p_fperiod
          ,l_group_id);
      END LOOP;
    
      l_sec := 'Through Loops and ready to Import';
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
    
      UPDATE xxcus.xxcusgl_headcount_aud_tbl
         SET stage = l_sec, stg_date = SYSDATE
       WHERE group_id = l_group_id;
    
      COMMIT;
    END;
  
    BEGIN
    
      FOR c_import IN (SELECT group_id, ledger_id
                         FROM xxcus.xxcusgl_headcount_aud_tbl
                        WHERE trunc(stg_date) = trunc(SYSDATE)
                          AND stage = 'Through Loops and ready to Import')
      LOOP
      
        l_can_submit_request := xxcus_misc_pkg.set_responsibility('GLINTERFACE'
                                                                 ,'HDS General Ledger Super User');
      
        IF l_can_submit_request
        THEN
          l_globalset := 'Global Variable are set.';
        ELSE
          l_globalset := 'Global Variable are not set.';
          l_sec       := 'Global Variable are not set for the Responsibility of HDS General Ledger Accountant and the User of GLINTERFACE.';
          fnd_file.put_line(fnd_file.log, l_sec);
          fnd_file.put_line(fnd_file.output, l_sec);
          RAISE program_error;
        END IF;
      
        fnd_file.put_line(fnd_file.log, l_globalset);
        fnd_file.put_line(fnd_file.output, l_globalset);
      
        IF (l_can_submit_request)
        THEN
        
          INSERT INTO gl_interface_control
            (status
            ,je_source_name
            ,group_id
            ,set_of_books_id
            ,interface_run_id
            ,interface_table_name
            ,processed_table_code)
          VALUES
            ('S'
            ,l_jesourcesys
            ,c_import.group_id
            ,c_import.ledger_id
            ,c_import.group_id
            ,lc_table_name
            ,l_processed_data_action);
        
          --*******************************************
          --Submit the GLLEZL import process
          --*******************************************
          --Submit Import Process under GLINTERFACE under HDS General Ledger Accountant
          l_sec := 'Submitting Import Process.';
          fnd_file.put_line(fnd_file.log, l_sec);
          fnd_file.put_line(fnd_file.output, l_sec);
        
          --Once initialized then Submit the Concurrent Request Journal Import
          l_req_id_1 := fnd_request.submit_request(application => 'SQLGL'
                                                  ,program     => 'GLLEZL'
                                                  ,description => NULL
                                                  ,start_time  => SYSDATE
                                                  ,sub_request => FALSE
                                                  ,argument1   => c_import.group_id
                                                  ,argument2   => c_import.ledger_id
                                                  ,argument3   => 'N'
                                                  ,argument4   => NULL
                                                  ,argument5   => NULL
                                                  ,argument6   => 'N'
                                                  ,argument7   => 'O');
        
          COMMIT;
        
          fnd_file.put_line(fnd_file.log, l_req_id_1);
          fnd_file.put_line(fnd_file.output, l_req_id_1);
        
          --Write detail Line Log rows
          l_sec := 'Waiting for Import to finish ' || l_req_id_1;
          fnd_file.put_line(fnd_file.log, l_sec);
          fnd_file.put_line(fnd_file.output, l_sec);
        
          UPDATE xxcus.xxcusgl_headcount_aud_tbl
             SET stage = l_sec, stg_date = SYSDATE
           WHERE group_id = c_import.group_id;
        
          COMMIT;
        
          --Wait for Journal Import
          IF (l_req_id_1 != 0)
          THEN
            IF fnd_concurrent.wait_for_request(l_req_id_1
                                              ,6
                                              ,15000
                                              ,l_phase
                                              ,l_status
                                              ,l_dev_phase
                                              ,l_dev_status
                                              ,l_message)
            THEN
              l_error_message := 'ReqID=' || l_req_id_1 || ' DPhase ' ||
                                 l_dev_phase || ' DStatus ' || l_dev_status ||
                                 ' MSG ' || l_message;
            
              IF upper(l_dev_phase) != 'COMPLETE' OR
                --upper(l_dev_status) NOT IN ('NORMAL', 'WARNING')     --Version 1.1
                 upper(l_dev_status) != 'NORMAL'
              THEN
                --Version 1.1
                l_sec := 'An error occured in the Journal Import1, please review the Log for concurrent request ' ||
                         l_req_id_1 || ' - ' || l_error_message || '.';
                fnd_file.put_line(fnd_file.log, l_sec);
                fnd_file.put_line(fnd_file.output, l_sec);
                --RAISE program_error;
                --Version 1.1
                l_subject     := '*** Journal Import1 Error/Warning for Head Count Accrual ***';
                l_body_header := '<style type="text/css">.style1 {font-family: Arial, Helvetica, sans-serif;}</style><p class="style1">';
              
                l_body_detail := l_body_detail ||
                                 '<BR>Journal Import for US Ledger - Concurrent Request:  ' ||
                                 l_req_id_1;
                l_body_footer := l_body_footer;
              
                l_body := l_body_header || l_body_detail || l_body_footer;
              
                xxcus_misc_pkg.html_email(p_to            => l_dflt_email
                                         ,p_from          => l_sender
                                         ,p_text          => 'test'
                                         ,p_subject       => l_subject
                                         ,p_html          => l_body
                                         ,p_smtp_hostname => l_host
                                         ,p_smtp_portnum  => l_hostport);
              ELSE
                p_retcode := 0;
              
              END IF;
            
            END IF;
          
          ELSE
            l_sec := 'An error occured when trying to submit Journal Import3, the concurrent request was not submitted.';
            fnd_file.put_line(fnd_file.log, l_sec);
            fnd_file.put_line(fnd_file.output, l_sec);
            --RAISE program_error;
            --Version 1.1
            l_subject     := '*** Journal Import3 Error/Warning for Head Count Accrual ***';
            l_body_header := '<style type="text/css">.style1 {font-family: Arial, Helvetica, sans-serif;}</style><p class="style1">';
          
            l_body_detail := l_body_detail ||
                             '<BR>Journal Import - concurrent request was not submitted.  ';
            l_body_footer := l_body_footer;
          
            l_body := l_body_header || l_body_detail || l_body_footer;
            xxcus_misc_pkg.html_email(p_to            => l_dflt_email
                                     ,p_from          => l_sender
                                     ,p_text          => 'test'
                                     ,p_subject       => l_subject
                                     ,p_html          => l_body
                                     ,p_smtp_hostname => l_host
                                     ,p_smtp_portnum  => l_hostport);
          END IF;
        END IF;
      
        --Write detail Line Log rows
        l_sec := 'Import finished';
        fnd_file.put_line(fnd_file.log, l_sec);
        fnd_file.put_line(fnd_file.output, l_sec);
      
        UPDATE xxcus.xxcusgl_headcount_aud_tbl
           SET stage = l_sec, stg_date = SYSDATE
         WHERE group_id = c_import.group_id;
      
        COMMIT;
      
        SELECT gl_je_posting_s.nextval INTO l_posting_id FROM dual;
      
        /* Start the journal post logic */
        --*******************************************
        --Submit the GLPPOS import process
        --*******************************************
        --Submit Import Process under GLINTERFACE under HDS General Ledger Accountant
      
        l_sec := 'Submitting Posting Process.';
        fnd_file.put_line(fnd_file.log, l_sec);
        fnd_file.put_line(fnd_file.output, l_sec);
      
        UPDATE xxcus.xxcusgl_headcount_aud_tbl
           SET stage = l_sec, stg_date = SYSDATE
         WHERE group_id = c_import.group_id;
      
        COMMIT;
      
        --Once initialized then Submit the Concurrent Request Journal Posting
        l_req_id_2 := fnd_request.submit_request(application => 'SQLGL'
                                                ,program     => 'GLPPOSS'
                                                ,description => NULL
                                                ,start_time  => SYSDATE
                                                ,sub_request => FALSE
                                                ,argument1   => c_import.ledger_id
                                                ,argument2   => l_access_set
                                                ,argument3   => l_coa
                                                ,argument4   => l_posting_id);
      
        UPDATE gl.gl_je_batches t
           SET status          = 'S'
              ,t.posted_date   = SYSDATE
              ,status_verified = 'Y'
              ,posting_run_id  = l_posting_id
              ,request_id      = l_req_id_2
              ,posted_by       = fnd_global.user_id
         WHERE t.default_period_name = p_fperiod
              /*AND NAME LIKE l_source || '%' || c_import.group_id;*/ --Version 1.1
           AND NAME LIKE l_source || '%' || c_import.group_id || '%';
      
        COMMIT;
      
        fnd_file.put_line(fnd_file.log, l_req_id_2);
        fnd_file.put_line(fnd_file.output, l_req_id_2);
      
        --Write detail Line Log rows
        l_sec := 'Waiting for Posting to finish ' || l_req_id_2;
        fnd_file.put_line(fnd_file.log, l_sec);
        fnd_file.put_line(fnd_file.output, l_sec);
      
        UPDATE xxcus.xxcusgl_headcount_aud_tbl
           SET stage = l_sec, stg_date = SYSDATE
         WHERE group_id = c_import.group_id;
      
        COMMIT;
      
        --Wait for Journal Post
        IF (l_req_id_2 != 0)
        THEN
          IF fnd_concurrent.wait_for_request(l_req_id_2
                                            ,6
                                            ,15000
                                            ,l_phase
                                            ,l_status
                                            ,l_dev_phase
                                            ,l_dev_status
                                            ,l_message)
          THEN
            l_error_message := 'ReqID=' || l_req_id_2 || ' DPhase ' ||
                               l_dev_phase || ' DStatus ' || l_dev_status ||
                               ' MSG ' || l_message;
          
            IF l_dev_phase != 'COMPLETE' OR
              --l_dev_status NOT IN ('NORMAL', 'WARNING')     --Version 1.1
               l_dev_status != 'NORMAL'
            THEN
              --Version 1.1
              l_sec := 'An error occured in the Journal Post1, please review the Log for concurrent request ' ||
                       l_req_id_2 || ' - ' || l_error_message || '.';
              fnd_file.put_line(fnd_file.log, l_sec);
              fnd_file.put_line(fnd_file.output, l_sec);
              --RAISE program_error;
              --Version 1.1
              l_subject     := '*** Journal Post1 Error/Warning for Head Count Accrual ***';
              l_body_header := '<style type="text/css">.style1 {font-family: Arial, Helvetica, sans-serif;}</style><p class="style1">';
            
              l_body_detail := l_body_detail ||
                               '<BR>Journal Post for Ledger: ' ||
                               c_import.ledger_id ||
                               ' Please review the log for Concurrent Request:  ' ||
                               l_req_id_2;
              l_body_footer := l_body_footer;
            
              l_body := l_body_header || l_body_detail || l_body_footer;
            
              xxcus_misc_pkg.html_email(p_to            => l_dflt_email
                                       ,p_from          => l_sender
                                       ,p_text          => 'test'
                                       ,p_subject       => l_subject
                                       ,p_html          => l_body
                                       ,p_smtp_hostname => l_host
                                       ,p_smtp_portnum  => l_hostport);
            ELSE
              p_retcode := 0;
              -- version 1.11 change
              l_sec := 'A journal entry request posted successfully for concurrent request ' ||
                       l_req_id_2 || '.';
              fnd_file.put_line(fnd_file.log, l_sec);
              fnd_file.put_line(fnd_file.output, l_sec);
            
              l_subject     := '*** Head Count Journal Post Success ***';
              l_body_header := '<style type="text/css">.style1 {font-family: Arial, Helvetica, sans-serif;}</style><p class="style1">';
            
              l_body_detail := l_body_detail || '<BR>Journal Posting  - ' ||
                               ' Concurrent Request submitted.:  ' ||
                               l_req_id_2 || ' has been successfully done';
              l_body_footer := l_body_footer;
            
              l_body := l_body_header || l_body_detail || l_body_footer;
            
              xxcus_misc_pkg.html_email(p_to            => l_dflt_email
                                       ,p_from          => l_sender
                                       ,p_text          => 'test'
                                       ,p_subject       => l_subject
                                       ,p_html          => l_body
                                       ,p_smtp_hostname => l_host
                                       ,p_smtp_portnum  => l_hostport);
            END IF;
          ELSE
            l_sec := 'An error occured in the Journal Post2, please review the Log for concurrent request ' ||
                     l_req_id_2 || ' - ' || l_error_message || '.';
            fnd_file.put_line(fnd_file.log, l_sec);
            fnd_file.put_line(fnd_file.output, l_sec);
            --RAISE program_error;
            --Version 1.1
            l_subject     := '*** Journal Post2 Error/Warning for Head Count Accrual ***';
            l_body_header := '<style type="text/css">.style1 {font-family: Arial, Helvetica, sans-serif;}</style><p class="style1">';
          
            l_body_detail := l_body_detail ||
                             '<BR>Journal Post for Ledger: ' ||
                             c_import.ledger_id ||
                             '  - Concurrent Request:  ' || l_req_id_2;
            l_body_footer := l_body_footer;
          
            l_body := l_body_header || l_body_detail || l_body_footer;
          
            xxcus_misc_pkg.html_email(p_to            => l_dflt_email
                                     ,p_from          => l_sender
                                     ,p_text          => 'test'
                                     ,p_subject       => l_subject
                                     ,p_html          => l_body
                                     ,p_smtp_hostname => l_host
                                     ,p_smtp_portnum  => l_hostport);
          
          END IF;
        ELSE
          l_sec := 'An error occured when trying to submit Journal Post3, the concurrent request was not submitted.';
          fnd_file.put_line(fnd_file.log, l_sec);
          fnd_file.put_line(fnd_file.output, l_sec);
          --RAISE program_error;
          --Version 1.1
          l_subject     := '*** Journal Post3 Error/Warning for Head Count Accrual ***';
          l_body_header := '<style type="text/css">.style1 {font-family: Arial, Helvetica, sans-serif;}</style><p class="style1">';
        
          l_body_detail := l_body_detail || '<BR>Journal Post for Ledger: ' ||
                           c_import.ledger_id ||
                           '  Concurrent Request was mpt submitted.:  ' ||
                           l_req_id_2;
          l_body_footer := l_body_footer;
        
          l_body := l_body_header || l_body_detail || l_body_footer;
        
          xxcus_misc_pkg.html_email(p_to            => l_dflt_email
                                   ,p_from          => l_sender
                                   ,p_text          => 'test'
                                   ,p_subject       => l_subject
                                   ,p_html          => l_body
                                   ,p_smtp_hostname => l_host
                                   ,p_smtp_portnum  => l_hostport);
        END IF;
      
        /* End the journal post logic */
      
        --Write detail Line Log rows
      
        l_sec := 'Posting finished';
        fnd_file.put_line(fnd_file.log, l_sec);
        fnd_file.put_line(fnd_file.output, l_sec);
      
        UPDATE xxcus.xxcusgl_headcount_aud_tbl
           SET stage = l_sec, stg_date = SYSDATE
         WHERE group_id = c_import.group_id;
      
        COMMIT;
      
      END LOOP;
    END;
  
  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;
      l_err_msg := l_err_msg || l_sec || ' ...Error_Stack...' ||
                   dbms_utility.format_error_stack() ||
                   ' Error_Backtrace...' ||
                   dbms_utility.format_error_backtrace();
      fnd_file.put_line(fnd_file.log, l_err_msg);
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_procedure_name
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_dflt_email
                                          ,p_module            => 'GL');
    
      p_retcode := 2;
      p_errbuf  := l_err_msg;
    
    WHEN OTHERS THEN
      l_err_msg := l_err_msg || l_sec || ' ...Error_Stack...' ||
                   dbms_utility.format_error_stack() ||
                   ' Error_Backtrace...' ||
                   dbms_utility.format_error_backtrace();
      fnd_file.put_line(fnd_file.log, l_err_msg);
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_procedure_name
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_dflt_email
                                          ,p_module            => 'GL');
    
      p_retcode := 2;
      p_errbuf  := l_err_msg;
    
  END load_headcount;

END xxcusgl_accrual_pkg;
/
