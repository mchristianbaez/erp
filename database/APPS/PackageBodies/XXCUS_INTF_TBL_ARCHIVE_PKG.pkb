create or replace 
PACKAGE BODY      apps.XXCUS_INTF_TBL_ARCHIVE_PKG
  /********************************************************************************
  FILE NAME: APPS.XXCUS_INTF_TBL_ARCHIVE_PKG.pkb

  PROGRAM TYPE: PL/SQL Package Body

  PURPOSE: To Archive and purging the data based on given days in loopkup
  XXCUS_INTF_TBL_ARCHIVE_DAYS

  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     07/24/2015    Pattabhi Avula  TMS#20150401-00052 Initial version.
  1.1     03/13/2018    Pattabhi Avula  TMS#20180312-00016 -- Program completed 
                                                            error
  *******************************************************************************/

IS
  l_package_name VARCHAR2(100) := 'XXCUS_INTF_TBL_ARCHIVE_PKG.data_purge';
  l_distro_list  VARCHAR2(100) DEFAULT 'WC-ITDevelopment-U1@HDSupply.com';

  /********************************************************************************
  FILE NAME: APPS.XXCUS_INTF_TBL_ARCHIVE_PKG.pkb

  PROGRAM TYPE: data_purge Procedure

  PURPOSE: To Archive and purging the data based on given days in loopkup
  XXCUS_INTF_TBL_ARCHIVE_DAYS

  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     07/24/2015    Pattabhi Avula  TMS#20150401-00052 Initial version.
  1.1     03/13/2018    Pattabhi Avula  TMS#20180312-00016 -- Program completed 
                                                            error
  ********************************************************************************/

  -- --------------------------------------------------------------------------------
  -- Procedure to purge the audit data
  -- --------------------------------------------------------------------------------
PROCEDURE data_purge(
    p_errbuf OUT VARCHAR2,
    p_retcode OUT VARCHAR2)
IS
  l_error_msg   VARCHAR2(2000); -- Ver#1.1
  l_program_err EXCEPTION;
  l_calling     VARCHAR2(240);
  l_statement   VARCHAR2(300);
  l_insert_stmt VARCHAR2(500);
  l_arch_owner  VARCHAR2(10);
  l_arc_count   NUMBER(5);
  l_purg_owner  VARCHAR2(10);
  l_purg_count  NUMBER(5);
  l_dpurg_owner VARCHAR2(10);
  l_dpurg_count NUMBER(5);
 
  CURSOR C_purg
  IS
    SELECT lookup_code,
           tag,
           attribute1,
           attribute2,
		   attribute3
    FROM   fnd_lookup_values
    WHERE  lookup_type='XXCUS_INTF_TBL_ARCHIVE_DAYS'
    AND    language     ='US'
    AND    enabled_flag ='Y'
	AND TRUNC(SYSDATE) BETWEEN start_date_active AND NVL(end_date_active, TRUNC(SYSDATE) + 1);

BEGIN
  l_calling:='Calling Purg Cursor from data_purge procedure';

    FOR p_rec IN C_purg
    LOOP
	  BEGIN
	    fnd_file.put_line(fnd_file.log,'---------------------------------------------------------------------------------------------');--Ver#1.1		
	    l_insert_stmt:=NULL; -- Ver#1.1
		l_statement:=NULL; -- Ver#1.1
		
        IF p_rec.attribute1 IS NOT NULL THEN

          -- --------------------------------------------------------------------------------
          -- Checking  Archive table exists or not in Database
          -- --------------------------------------------------------------------------------
          BEGIN
            SELECT owner,
                   COUNT(*)
            INTO   l_arch_owner,
                   l_arc_count
            FROM   dba_tables
            WHERE  table_name=p_rec.attribute1
            GROUP BY owner;
          EXCEPTION
          WHEN OTHERS THEN
            l_calling:='Error while fetching the Archive table count for '||' '||p_rec.attribute1;
			l_error_msg:=l_calling||' '||SUBSTR(SQLERRM,1,240);  -- Ver#1.1
			fnd_file.put_line(fnd_file.log, l_error_msg);  -- Ver#1.1
            RAISE l_program_err;
          END;
		  
          fnd_file.put_line(fnd_file.log,'Archive table : ' || p_rec.attribute1 ||' count is : '||l_arc_count);   -- Ver#1.1

         -- --------------------------------------------------------------------------------
         -- Checking  purge table exists or not in Database
         -- --------------------------------------------------------------------------------
		 l_calling:='Checking  purge table exists or not in Database'; -- Ver#1.1
            IF l_arc_count=1 THEN
      
              BEGIN
                SELECT owner,
                       COUNT(*)
                INTO   l_purg_owner,
                       l_purg_count
                FROM   dba_tables
                WHERE  table_name=p_rec.lookup_code
                GROUP BY owner;
              EXCEPTION
              WHEN OTHERS THEN
                l_calling:='Error while fetching the purge table count for '||' '||p_rec.lookup_code;
				l_error_msg:=l_calling||' '||SUBSTR(SQLERRM,1,240);  -- Ver#1.1
				fnd_file.put_line(fnd_file.log, l_error_msg);   -- Ver#1.1
                RAISE l_program_err;
              END;
              fnd_file.put_line(fnd_file.log,'Purge table : ' ||p_rec.lookup_code||' count is : '||l_purg_count);  -- Ver#1.1
      
            ELSE
              l_calling:='Duplicate or Not found the Archive table for '||p_rec.attribute1;  -- Ver#1.1
			  l_error_msg:=l_calling;  -- Ver#1.1
			  fnd_file.put_line(fnd_file.log, l_error_msg);  -- Ver#1.1
              RAISE l_program_err;
            END IF;

        -- --------------------------------------------------------------------------------
        -- Checking purge table is exists or not, if exists then inserting the data into
        -- Archive table in case attribute3 value is not null case
        -- --------------------------------------------------------------------------------
		l_calling:='Checking purge table is exists or not'; -- Ver#1.1
        IF l_purg_count =1 THEN

		  IF p_rec.attribute3 IS NOT NULL
		    THEN
			  l_calling:='Checking attribute3 is not null'; -- Ver#1.1
			  
              l_insert_stmt:='INSERT INTO '||l_arch_owner||'.'||p_rec.attribute1||' SELECT * FROM '||l_purg_owner||'.'||p_rec.lookup_code||' WHERE TRUNC(creation_date)< TRUNC(SYSDATE)-'||p_rec.attribute2|| 'AND '||p_rec.attribute3;
			  
              fnd_file.put_line(fnd_file.log,'l_insert_stmt : ' || l_insert_stmt);
			  l_error_msg:= l_insert_stmt;--Ver#1.1

              EXECUTE IMMEDIATE l_insert_stmt;

			  fnd_file.put_line(fnd_file.log,'Total Records Inserted: ' ||SQL%ROWCOUNT);--Ver#1.1
              COMMIT;

             fnd_file.put_line(fnd_file.log,'Archive the data successfully for attrubute3 is not null condition  : '||' '||p_rec.attribute1);

  -- --------------------------------------------------------------------------------
  -- purging data from the base table After Archive process finished for Attribute3
  -- value is not null
  -- --------------------------------------------------------------------------------
  
              l_calling:='Purging the data from Base table after Archived'; -- Ver#1.1
              
              l_statement:= 'DELETE FROM '||l_purg_owner||'.'||p_rec.lookup_code||' WHERE TRUNC(creation_date)< TRUNC(SYSDATE) - '||p_rec.tag|| 'AND '||p_rec.attribute3;
			  
			  fnd_file.put_line(fnd_file.log,'l_statement : ' || l_statement);
			  l_error_msg:= l_statement;--Ver#1.1
			  
              EXECUTE IMMEDIATE l_statement;
		    
			  fnd_file.put_line(fnd_file.log,'Total Records Deleted: ' ||SQL%ROWCOUNT);--Ver#1.1

              COMMIT;

              fnd_file.put_line(fnd_file.log,'Data Purged successfully for attrubute3 is not null condition :'||' '||p_rec.lookup_code);

		  ELSE
  -- --------------------------------------------------------------------------------
  -- Inserting the data into Archive table in case attribute3 value IS NULL
  -- --------------------------------------------------------------------------------

            l_calling:='Inserting the data into Archive table in case attribute3 value IS NULL';  -- Ver#1.1

		    l_insert_stmt:='INSERT INTO '||l_arch_owner||'.'||p_rec.attribute1||' SELECT * FROM '||l_purg_owner||'.'||p_rec.lookup_code||' WHERE TRUNC(creation_date)< TRUNC(SYSDATE)-'||p_rec.attribute2;
			
			fnd_file.put_line(fnd_file.log,'l_insert_stmt : ' || l_insert_stmt);
			l_error_msg:= l_insert_stmt;--Ver#1.1
  		   			
            EXECUTE IMMEDIATE l_insert_stmt;			
	  
			fnd_file.put_line(fnd_file.log,'Total Records Inserted: ' ||SQL%ROWCOUNT);--Ver#1.1

            COMMIT;

            fnd_file.put_line(fnd_file.log,'Archive the data successfully for attrubute3 is null condition  : '||' '||p_rec.attribute1);

  -- --------------------------------------------------------------------------------
  -- Purging the data from base table in case attribute3 value IS NULL
  -- --------------------------------------------------------------------------------
            l_calling:='Purging the data from base table in case attribute3 value IS NULL'; 

		    l_statement:= 'DELETE FROM '||l_purg_owner||'.'||p_rec.lookup_code||' WHERE TRUNC(creation_date)< TRUNC(SYSDATE)-'||p_rec.tag;  
			  
			fnd_file.put_line(fnd_file.log,'l_statement : ' || l_statement);
			l_error_msg:= l_statement;--Ver#1.1
           		
            EXECUTE IMMEDIATE l_statement;
			
            fnd_file.put_line(fnd_file.log,'Total Records Inserted: ' ||SQL%ROWCOUNT);--Ver#1.1
            COMMIT;

			fnd_file.put_line(fnd_file.log,'Data Purged successfully, After Archive the table :'||' '||p_rec.lookup_code);

		  END IF;

        ELSE
          l_calling:='Error or Duplicate Purge tables found for '||' '||p_rec.lookup_code;
		  l_error_msg:=l_calling; -- Ver#1.1
		  fnd_file.put_line(fnd_file.log,l_error_msg);  -- Ver#1.1
          RAISE l_program_err;
        END IF;

      ELSE

  -- --------------------------------------------------------------------------------
  -- Checking the Purge table exists or not in Database
  -- --------------------------------------------------------------------------------
       l_calling:='Checking the Purge table exists or not in case of attribute1 null'; -- Ver#1.1
        BEGIN
          SELECT owner,
                 COUNT(*)
          INTO   l_dpurg_owner,
                 l_dpurg_count
          FROM   dba_tables
          WHERE  table_name=p_rec.lookup_code
          GROUP BY OWNER;
        EXCEPTION
        WHEN OTHERS THEN
          l_calling:='Error while fetching the direct purge table count '||p_rec.lookup_code;
		  l_error_msg:=l_calling||' '||SUBSTR(SQLERRM,1,240);  -- Ver#1.1
		  fnd_file.put_line(fnd_file.log,l_error_msg); -- Ver#1.1
          RAISE l_program_err;
        END;

        fnd_file.put_line(fnd_file.log,'Direct purge table exists : '||l_dpurg_count);  -- Ver#1.1

  -- --------------------------------------------------------------------------------
  -- If Archive table is null then purging data from the base table directly Based on
  -- attribute3 condition
  -- --------------------------------------------------------------------------------
        l_calling:='If Archive table is null then purging data from the base table directly Based on  attribute3 condition'; -- Ver#1.1
        IF l_dpurg_count=1 THEN
          l_calling:='Base table exists'; -- Ver#1.1
		  IF p_rec.attribute2 IS NULL AND p_rec.attribute3 IS NOT NULL
		    THEN
            l_statement  := 'DELETE FROM '||l_dpurg_owner||'.'||p_rec.lookup_code||' WHERE TRUNC(creation_date)< TRUNC(SYSDATE)-'||p_rec.tag|| 'AND '||p_rec.attribute3;
			 
			 fnd_file.put_line(fnd_file.log,'l_statement : ' || l_statement);
			 l_error_msg:= l_statement;--Ver#1.1
			  
             EXECUTE IMMEDIATE l_statement;
	    
			 fnd_file.put_line(fnd_file.log,'Total Records Deleted: ' ||SQL%ROWCOUNT);--Ver#1.1
			 
             COMMIT;

             fnd_file.put_line(fnd_file.log,'Data purged directly from the table incase attribute3 condition is not null for direct purge tables :'||' '||p_rec.lookup_code);

		  ELSE
  -- --------------------------------------------------------------------------------
  -- Purging the data from base table directly
  -- --------------------------------------------------------------------------------
            l_calling:='Incase Attribute2 and Attribute3 is null'; -- Ver#1.1

		    l_statement  := 'DELETE FROM '||l_dpurg_owner||'.'||p_rec.lookup_code||' WHERE TRUNC(creation_date)< TRUNC(SYSDATE)-'||p_rec.tag; 
			
			fnd_file.put_line(fnd_file.log,'l_statement : ' || l_statement);--Ver#1.1
			l_error_msg:= l_statement;--Ver#1.1

            EXECUTE IMMEDIATE l_statement;

            fnd_file.put_line(fnd_file.log,'Total Records Deleted: ' ||SQL%ROWCOUNT);--Ver#1.1		

            COMMIT;

            fnd_file.put_line(fnd_file.log,'Data purged directly from the table incase attribute2 and attribute3 condition is null for direct purge tables :'||' '||p_rec.lookup_code);

		  END IF;

        ELSE
            l_calling:='Error or Duplicate direct Purge tables found for '||' '||p_rec.lookup_code;
			l_error_msg:=l_calling;   -- Ver#1.1
			fnd_file.put_line(fnd_file.log,l_error_msg);  -- Ver#1.1
          RAISE l_program_err;
        END IF; 
      END IF;
  EXCEPTION
    WHEN l_program_err THEN
	fnd_file.put_line(fnd_file.log,' Error in l_program_err Exception '||p_rec.lookup_code);  -- Ver#1.1
      xxcus_error_pkg.xxcus_error_main_api (p_called_from => l_package_name,
	                                        p_calling => l_calling,
	  									    p_request_id => fnd_global.conc_request_id,
	  									    p_ora_error_msg => SUBSTR(l_error_msg,1,2000),  -- Ver#1.1
	  									    p_error_desc => SUBSTR(SQLERRM,1,240), -- Ver#1.1
	  									    p_distribution_list => l_distro_list,
	  									    p_module => 'OM'
	  									    );
    WHEN OTHERS THEN
	fnd_file.put_line(fnd_file.log,' Error in when others Exception '||l_error_msg||' '||SQLERRM);  -- Ver#1.1
      xxcus_error_pkg.xxcus_error_main_api (p_called_from => l_package_name,
	                                        p_calling => l_calling,
	  									    p_request_id => fnd_global.conc_request_id,
	  									    p_ora_error_msg => SUBSTR(l_error_msg,1,2000),  -- Ver#1.1
	  									    p_error_desc => SUBSTR(SQLERRM,1,240),  -- Ver#1.1
	  									    p_distribution_list => l_distro_list,
	  									    p_module => 'OM'
	  									    );
  END;
    END LOOP;
    -- COMMIT;

EXCEPTION
  WHEN OTHERS THEN
  fnd_file.put_line(fnd_file.log,' Error in when others Last Exception '||l_error_msg||' '||SQLERRM);  -- Ver#1.1
    xxcus_error_pkg.xxcus_error_main_api (p_called_from => l_package_name,
	                                      p_calling => l_calling,
										  p_request_id => fnd_global.conc_request_id,
										  p_ora_error_msg => SUBSTR(l_error_msg,1,2000),  -- Ver#1.1
	  									  p_error_desc => SUBSTR(SQLERRM,1,240),  -- Ver#1.1
										  p_distribution_list => l_distro_list,
										  p_module => 'OM'
										  );
END data_purge;

END XXCUS_INTF_TBL_ARCHIVE_PKG;
/