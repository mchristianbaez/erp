CREATE OR REPLACE PACKAGE BODY APPS.XXWC_RCV_INTORG_PKG AS

/******************************************************************************
   NAME:       XXWC_RVC_INTORG_PKG

   PURPOSE:    Process Interorg Shipments That Were Received Short And Intorg Transfer It Back to the Shipping Organization
   

   REVISIONS:
   Ver        Date         Author           Description
   ---------  -----------  ---------------  ------------------------------------
   1.0        03-NOV-2012  Lee Spitzer       1. Create the PL/SQL Package
   1.1        28-AUG-2013  Lee Spitzer       1.1.  Update Package for TMS Ticket - 20130826-00733 BOD Issue on Transfers
   1.2        07-NOV-2014  Pattabhi Avula    1.2 Canada Changes made for TMS# 20141002-00072
******************************************************************************/


  /*************************************************************************
 *   Procedure : Write_Log
 *
 *   PURPOSE:   This procedure logs error message
 *   Parameter:
 *          IN
 *              p_debug_msg      -- Debug Message
 * ************************************************************************/
   -- add debug message to log table and concurrent log file
   PROCEDURE Write_Log (p_debug_level   IN NUMBER,
                        p_mod_name      IN VARCHAR2,
                        p_debug_msg     IN VARCHAR2)
   IS
      PRAGMA AUTONOMOUS_TRANSACTION;
   BEGIN
      --IF (p_debug_level >= FND_LOG.G_CURRENT_RUNTIME_LEVEL)
      --THEN
      -- Call Oracle FND_LOG API to insert the debug message
      FND_LOG.STRING (p_debug_level, p_mod_name, P_DEBUG_MSG);
      -- END IF;
      fnd_file.put_line (fnd_file.LOG, P_DEBUG_MSG);
      --DBMS_OUTPUT.put_line (P_DEBUG_MSG);
      COMMIT;
   END Write_Log;

  /*************************************************************************
 *   Procedure : Write_Error
 *
 *   PURPOSE:   This procedure logs error message
 *   Parameter:
 *          IN
 *              p_debug_msg      -- Debug Message
 * ************************************************************************/

   --add message to concurrent output file
   PROCEDURE Write_Error (p_debug_msg IN VARCHAR2)
   IS
      l_req_id          NUMBER := fnd_global.conc_request_id;
      l_err_callfrom    VARCHAR2 (75) DEFAULT G_PACKAGE_NAME ;
      l_err_callpoint   VARCHAR2 (75) DEFAULT 'START' ;
      l_distro_list VARCHAR2 (75)
            DEFAULT 'HDSOracleDevelopers@hdsupply.com'  ;
   BEGIN
      xxcus_error_pkg.xxcus_error_main_api (
         p_called_from         => l_err_callfrom,
         p_calling             => l_err_callpoint,
         p_request_id          => l_req_id,
         p_ora_error_msg       => SUBSTR (p_debug_msg, 1, 2000),
         p_error_desc          => 'Error running '|| G_PACKAGE_NAME ||' with PROGRAM ERROR',
         p_distribution_list   => l_distro_list,
         p_module              => 'INV'
      );
   END Write_Error;

   /*************************************************************************
    *   Procedure : Write_output
    *
    *   PURPOSE:   This procedure logs message into concurrent output
    *   Parameter:
    *          IN
    *              p_debug_msg      -- Debug Message
    * ************************************************************************/
   PROCEDURE Write_output (p_debug_msg IN VARCHAR2)
   IS
   BEGIN
      fnd_file.put_line (fnd_file.output, p_debug_msg);
   END Write_output;

   /*************************************************************************
    *   Procedure : Write_log_output
    *
    *   PURPOSE:   This procedure logs message into concurrent logfile
    *   Parameter:
    *          IN
    *              p_debug_msg      -- Debug Message
    * ************************************************************************/

   PROCEDURE Write_log_output (p_debug_msg IN VARCHAR2)
   IS
   BEGIN
      fnd_file.put_line (fnd_file.log, p_debug_msg);
   END write_log_output;

/*PROCEDURE INSERT_INTO_RCV_STAGE Used to query Interg Org Shipments that were received short and insert into XXWC_RCV_INTORG_TBL
          (P_DATE     IN DATE);
*/

PROCEDURE INSERT_INTO_RCV_STAGE
          (P_ORG_ID   IN NUMBER,
           P_DATE     IN DATE,
           P_TRANSACTION_TYPE IN VARCHAR2,
           P_GROUP_ID IN NUMBER,
           P_BATCH_ID IN NUMBER,
           X_RETCODE  OUT NUMBER)
IS

  --Find Open Line
  CURSOR c_lines IS
    SELECT  rsh.receipt_num,
            rsh.shipment_num,
            rsh.shipment_header_id,
            rsh.organization_id,
            rsl.shipment_line_id,
            rsl.to_organization_id,
            rsh.receipt_source_code,
            max(trunc(rt.transaction_date)) transaction_date,
            rsl.unit_of_measure,
            msib.primary_unit_of_measure,
            nvl(rsl.uom_conversion_rate,1) uom_conversion_rate,
            prha.segment1 requisition_number,
            prla.line_num,
            rsl.category_id,
            msib.inventory_item_id,
            msib.segment1 item_number, 
            msib.description,
            ooha.header_id,
            ooha.order_number,
            oola.line_id,
            oola.line_number,
            case when count(rt.transaction_date) = 0 then 0 else sum(rsl.quantity_shipped) / count(rt.transaction_date) end quantity_shipped,
            case when count(rt.transaction_date) = 0 then 0 else sum(rsl.quantity_received) / count(rt.transaction_date) end quantity_received,
            case when count(rt.transaction_date) = 0 then 0 else nvl(sum(rsl.quantity_shipped),0) / count(rt.transaction_date) - nvl(sum(rsl.quantity_received),0) / count(rt.transaction_date) end quantity_open,
            rsh.expected_receipt_date,
            rsh.shipped_date,
            get_lot_number(rsl.shipment_line_id) lot_number
    FROM    rcv_shipment_headers rsh,
            rcv_transactions rt,
            rcv_shipment_lines rsl,
            apps.po_requisition_lines prla,
            apps.po_requisition_headers prha,
            mtl_system_items_b msib,
            oe_order_sources oos,
            apps.oe_order_headers ooha,
            apps.oe_order_lines oola,
            --Added 04/22/2013 to prevent duplicate lines TMS Ticket 20130419-01566 
            mtl_material_transactions mmt
    WHERE   rsh.shipment_header_id = rt.shipment_header_id
    AND     rsh.receipt_source_code = 'INTERNAL ORDER'
    AND     rt.destination_type_code = 'INVENTORY'
    AND     rt.transaction_type = 'DELIVER'
    AND     rsl.shipment_line_id = rt.shipment_line_id
    AND     rt.requisition_line_id = prla.requisition_line_id
    AND     prla.requisition_header_id = prha.requisition_header_id
    AND     prla.item_id = msib.inventory_item_id
    AND     rt.organization_id = msib.organization_id
    AND     prla.requisition_header_id = oola.source_document_id(+)
    AND     prla.requisition_line_id = oola.source_document_line_id(+)
    AND     oola.header_id = ooha.header_id(+)
    AND     ooha.order_source_id = oos.order_source_id(+)
    AND     oos.NAME(+) = 'Internal'
    AND     trunc(rt.transaction_date) = trunc(nvl(p_date,rt.transaction_date))
    AND     rsl.attribute1 IS NULL -- 1/23/2013 If Line had a DFF of H, don't consider it for this query as it means hold, expecting shipment, C is handled in query below
    AND     rsl.shipment_line_id = nvl(g_shipment_line_id, rsl.shipment_line_id) -- Added 4/5/13 TMS Ticket # 20130228-01300
    AND     nvl(g_proc_c_dff,NULL) IS NULL -- Added 4/5/13 TMS Ticket # 20130228-01300
    --Added 04/22/2013 to prevent duplicate lines TMS Ticket 20130419-01566 
    AND     rsl.mmt_transaction_id = mmt.transaction_id
    AND     mmt.source_line_id = oola.line_Id
    AND     mmt.organization_id = oola.ship_from_org_id
    AND     mmt.inventory_item_id = oola.inventory_item_Id
    --End added 4/22/2013
    group by rsh.receipt_num,
            rsh.shipment_num,
            rsh.shipment_header_id,
            rsh.receipt_source_code,
            rsh.organization_id,
            rsl.shipment_line_id,
            rsl.to_organization_id,
            trunc(rt.transaction_date),
            rsl.unit_of_measure,
            msib.primary_unit_of_measure,
            nvl(rsl.uom_conversion_rate,1),
            prha.segment1,
            prla.line_num,
            rsl.category_id,
            msib.inventory_item_id,
            msib.segment1,
            msib.description,
            ooha.order_number,
            oola.line_number,
            ooha.header_id,
            oola.line_id,
            rsh.expected_receipt_date,
            rsh.shipped_date,
            get_lot_number(rsl.shipment_line_id)
     HAVING  nvl(sum(rsl.quantity_shipped),0) / count(rt.transaction_date) - nvl(sum(rsl.quantity_received),0) / count(rt.transaction_date) > 0
--Added 1/23/2013 to handle 0 quantity or force closed lines
--Updated 2/21/2013 to populate requisition and order line formation 
 UNION
 SELECT  rsh.receipt_num,
            rsh.shipment_num,
            rsh.shipment_header_id,
            rsh.organization_id,
            rsl.shipment_line_id,
            rsl.to_organization_id,
            rsh.receipt_source_code,
            trunc(mmt.transaction_date) transaction_date,
            rsl.unit_of_measure,
            msib.primary_unit_of_measure,
            nvl(rsl.uom_conversion_rate,1) uom_conversion_rate,
            prha.segment1, --updated 2/21/2013 --NULL REQUISITION_NUMBER,--prha.segment1 requisition_number,
            prla.line_num, --updated 2/21/2013 NULL LINE_NUM, --prla.line_num,
            rsl.category_id,
            msib.inventory_item_id,
            msib.segment1 item_number, 
            msib.description,
            ooha.header_id, --updated 2/21/2013 NULL header_id, --ooha.header_id,
            ooha.order_number, --updated 2/21/2013 NULL order_number, --ooha.order_number,
            oola.line_id, --updated 2/21/2013 NULL line_id, --oola.line_id,
            oola.line_number, --updated 2/21/2013 NULL line_number, --oola.line_number,
            sum(rsl.quantity_shipped) quantity_shipped,
            sum(rsl.quantity_received) quantity_received,
            nvl(sum(rsl.quantity_shipped),0) - nvl(sum(rsl.quantity_received),0) quantity_open,
            rsh.expected_receipt_date,
            rsh.shipped_date,
            get_lot_number(rsl.shipment_line_id) lot_number
    FROM    rcv_shipment_headers rsh,
            rcv_shipment_lines rsl,
            mtl_system_items_b msib,
            mtl_material_transactions mmt,
            ---Added the following tables on 2/21/2013 to support requisition and order information
            apps.po_requisition_lines prla,
            apps.po_requisition_headers prha,
            oe_order_sources oos,
            apps.oe_order_headers ooha,
            apps.oe_order_lines oola
    WHERE   rsh.receipt_source_code = 'INTERNAL ORDER'
    AND     rsl.mmt_transaction_id = mmt.transaction_id
    AND     rsl.shipment_header_id = rsh.shipment_header_id
    and     mmt.inventory_item_id = msib.inventory_item_id
    AND     mmt.organization_id= msib.organization_id
    AND     trunc(mmt.transaction_date) = trunc(nvl(p_date,mmt.transaction_date))
    -- 04/05/13 CG Changed
    AND     rsl.attribute1 = 'C' --1/23/2013 Force Close 
    AND     nvl(g_proc_c_dff,'C') = 'C'  -- Added 4/5/13 TMS Ticket # 20130228-01300
    ----- added 2/21/13
    AND     rsl.requisition_line_id = prla.requisition_line_id
    AND     prha.requisition_header_id = prla.requisition_header_id
    AND     prla.requisition_header_id = oola.source_document_id(+)
    AND     prla.requisition_line_id = oola.source_document_line_id(+)
    AND     oola.header_id = ooha.header_id(+)
    AND     ooha.order_source_id = oos.order_source_id(+)
    AND     oos.NAME(+) = 'Internal'
    AND     nvl(oola.cancelled_flag,'Y') = 'N'
    AND     rsl.shipment_line_id = nvl(g_shipment_line_id, rsl.shipment_line_id) -- Added 3/21/13 TMS Ticket # 20130228-01300
    --Added 4/22/2013 to prevent duplicate lines TMS Ticket 20130419-01566 
    AND     mmt.source_line_id = oola.line_Id
    AND     mmt.organization_id = oola.ship_from_org_id
    AND     mmt.inventory_item_id = oola.inventory_item_Id
    --End
    group by rsh.receipt_num,
            rsh.shipment_num,
            rsh.shipment_header_id,
            rsh.receipt_source_code,
            rsh.organization_id,
            rsl.shipment_line_id,
            rsl.to_organization_id,
            trunc(mmt.transaction_date),
            rsl.unit_of_measure,
            msib.primary_unit_of_measure,
            nvl(rsl.uom_conversion_rate,1),
            prha.segment1, --updated 2/21/2013 NULL, --prha.segment1,
            prla.line_num, --updated 2/21/2013 NULL, --prla.line_num,
            rsl.category_id,
            msib.inventory_item_id,
            msib.segment1,
            msib.description,
            ooha.order_number, --2/21/2013 NULL, --ooha.order_number,
            oola.line_number, --2/21/2013 NULL, --oola.line_number
            ooha.header_id, --2/21/2013 NULL, --ooha.header_id,
            oola.line_id, --2/21/2013 NULL, --oola.line_id,
            rsh.expected_receipt_date,
            rsh.shipped_date,
            get_lot_number(rsl.shipment_line_id)
    HAVING nvl(sum(rsl.quantity_shipped),0) - nvl(sum(rsl.quantity_received),0)  > 0;
     
    cursor c_xfer_lines is
    SELECT  rsh.receipt_num,
            substr(rsh.shipment_num||'-RETURN',1,30) shipment_num,
            rsh.shipment_header_id,
            rsh.organization_id,
            rsl.shipment_line_id,
            rsl.to_organization_id,
            rsh.receipt_source_code,
            max(trunc(rt.transaction_date)) transaction_date,
            rsl.unit_of_measure,
            msib.primary_unit_of_measure,
            nvl(rsl.uom_conversion_rate,1) uom_conversion_rate,
            prha.segment1 requisition_number,
            prla.line_num,
            rsl.category_id,
            msib.inventory_item_id,
            msib.segment1 item_number, 
            msib.description,
            ooha.header_id,
            ooha.order_number,
            oola.line_id,
            oola.line_number,
            case when count(rt.transaction_date) = 0 then 0 else sum(rsl.quantity_shipped) / count(rt.transaction_date) end quantity_shipped,
            case when count(rt.transaction_date) = 0 then 0 else sum(rt.primary_quantity) / count(rt.transaction_date) end quantity_received,
            case when count(rt.transaction_date) = 0 then 0 else nvl(sum(rsl.quantity_shipped),0) / count(rt.transaction_date) - nvl(sum(rt.primary_quantity),0) / count(rt.transaction_date) end quantity_open,
            rsh.expected_receipt_date,
            rsh.shipped_date,
            rt.group_id group_id,
            get_lot_number(rsl.shipment_line_id) lot_number
    FROM    rcv_shipment_headers rsh,
            rcv_transactions rt,
            rcv_shipment_lines rsl,
            apps.po_requisition_lines prla,
            apps.po_requisition_headers prha,
            mtl_system_items_b msib,
            oe_order_sources oos,
            apps.oe_order_headers ooha,
            apps.oe_order_lines oola,
            --Added 4/22/2013 to prevent duplicate TMS Ticket 20130419-01566 
            mtl_material_transactions mmt
    WHERE   rsh.shipment_header_id = rt.shipment_header_id
    AND     rsh.receipt_source_code = 'INTERNAL ORDER'
    AND     rt.destination_type_code = 'INVENTORY'
    AND     rt.transaction_type = 'DELIVER'
    AND     rsl.shipment_line_id = rt.shipment_line_id
    AND     rt.requisition_line_id = prla.requisition_line_id
    AND     prla.requisition_header_id = prha.requisition_header_id
    AND     prla.item_id = msib.inventory_item_id
    AND     rt.organization_id = msib.organization_id
    AND     prla.requisition_header_id = oola.source_document_id(+)
    AND     prla.requisition_line_id = oola.source_document_line_id(+)
    AND     oola.header_id = ooha.header_id(+)
    AND     ooha.order_source_id = oos.order_source_id(+)
    AND     oos.NAME(+) = 'Internal'
    AND     rt.group_id = p_group_id
    --Added 4/22/2013 to prevent duplicate lines TMS Ticket 20130419-01566 
    AND     rsl.mmt_transaction_id = mmt.transaction_id
    AND     mmt.source_line_id = oola.line_Id
    AND     mmt.organization_id = oola.ship_from_org_id
    AND     mmt.inventory_item_id = oola.inventory_item_Id
    group by rsh.receipt_num,
            rsh.shipment_num,
            rsh.shipment_header_id,
            rsh.receipt_source_code,
            rsh.organization_id,
            rsl.shipment_line_id,
            rsl.to_organization_id,
            trunc(rt.transaction_date),
            rsl.unit_of_measure,
            msib.primary_unit_of_measure,
            nvl(rsl.uom_conversion_rate,1),
            prha.segment1,
            prla.line_num,
            rsl.category_id,
            msib.inventory_item_id,
            msib.segment1,
            msib.description,
            ooha.order_number,
            oola.line_number,
            ooha.header_id,
            oola.line_id,
            rsh.expected_receipt_date,
            rsh.shipped_date,
            rt.group_id,
            get_lot_number(rsl.shipment_line_id);
            
    cursor c_rtv_lines is
    SELECT  rsh.receipt_num,
            rsh.shipment_num,
            rsh.shipment_header_id,
            rsh.organization_id,
            rsl.shipment_line_id,
            rsl.to_organization_id,
            rsh.receipt_source_code,
            trunc(mmt.transaction_date) transaction_date,
            rsl.unit_of_measure,
            msib.primary_unit_of_measure,
            nvl(rsl.uom_conversion_rate,1) uom_conversion_rate,
            NULL REQUISITION_NUMBER,--prha.segment1 requisition_number,
            NULL LINE_NUM, --prla.line_num,
            rsl.category_id,
            msib.inventory_item_id,
            msib.segment1 item_number, 
            msib.description,
            NULL header_id, --ooha.header_id,
            NULL order_number, --ooha.order_number,
            NULL line_id, --oola.line_id,
            NULL line_number, --oola.line_number,
            sum(rsl.quantity_shipped) quantity_shipped,
            sum(rsl.quantity_received) quantity_received,
            nvl(sum(rsl.quantity_shipped),0) - nvl(sum(rsl.quantity_received),0) quantity_open,
            rsh.expected_receipt_date,
            rsh.shipped_date,
            NULL group_id,
            get_lot_number(rsl.shipment_line_id) lot_number
    FROM    rcv_shipment_headers rsh,
            rcv_shipment_lines rsl,
            mtl_system_items_b msib,
            mtl_material_transactions mmt
    WHERE   rsh.receipt_source_code = 'INVENTORY'
    AND     rsl.mmt_transaction_id = mmt.transaction_id
    AND     rsl.shipment_header_id = rsh.shipment_header_id
    and     mmt.inventory_item_id = msib.inventory_item_id
    and     mmt.organization_id= msib.organization_id
    and     trunc(mmt.transaction_date) = trunc(nvl(p_date,mmt.transaction_date))
    group by rsh.receipt_num,
            rsh.shipment_num,
            rsh.shipment_header_id,
            rsh.receipt_source_code,
            rsh.organization_id,
            rsl.shipment_line_id,
            rsl.to_organization_id,
            trunc(mmt.transaction_date),
            rsl.unit_of_measure,
            msib.primary_unit_of_measure,
            nvl(rsl.uom_conversion_rate,1),
            NULL, --prha.segment1,
            NULL, --prla.line_num,
            rsl.category_id,
            msib.inventory_item_id,
            msib.segment1,
            msib.description,
            NULL, --ooha.order_number,
            NULL, --oola.line_number,
            NULL, --ooha.header_id,
            NULL, --oola.line_id,
            rsh.expected_receipt_date,
            rsh.shipped_date,
            get_lot_number(rsl.shipment_line_id)
    HAVING nvl(sum(rsl.quantity_shipped),0) - nvl(sum(rsl.quantity_received),0)  > 0;

    l_batch_id NUMBER;
    l_employee_id NUMBER;
    l_shipment_number varchar2(20);
    l_item_number varchar2(30);
    l_from_org varchar2(11);
    l_to_org varchar2(11);
    l_qty varchar2(11);
    l_lot_number varchar2(30);
    
BEGIN

  G_NAME := 'INSERT_INTO_RCV_STAGE';
  G_PROGRAM_NAME := G_PACKAGE_NAME||'.'||G_NAME;

  write_log_output('******************************************************************');
  write_log_output('Starting Program '||G_PROGRAM_NAME);
  write_log_output('******************************************************************');
  write_log_output(G_PROGRAM_NAME||' Parameters');
  write_log_output('--------------------------------------------');
  write_log_output(G_PROGRAM_NAME||' P_ORG_ID = ' || P_ORG_ID);
  write_log_output(G_PROGRAM_NAME||' P_DATE = ' || P_DATE);
  write_log_output(G_PROGRAM_NAME||' P_TRANSACTION_TYPE = ' || P_TRANSACTION_TYPE);
  write_log_output(G_PROGRAM_NAME||' P_GROUP_ID = '|| P_GROUP_ID);
  write_log_output('--------------------------------------------');
  

  l_batch_id := p_batch_id;
  
  write_log_output(G_PROGRAM_NAME || ' Batch Id = ' || l_batch_id);
  
  
  
  BEGIN
     SELECT employee_id
     INTO   l_employee_id
     FROM   fnd_user
     WHERE user_id = g_user_id;
  EXCEPTION
    WHEN OTHERS THEN
     l_employee_id := NULL;
  END;
  
  write_log_output(G_PROGRAM_NAME || ' l_employee_id = ' || l_employee_id);
  
  write_output('                                           '|| P_TRANSACTION_TYPE ||' TRANSACTIONS');
  write_output(' ');
  write_output('SHIPMENT NUMBER     |ITEM NUMBER                   |FROM ORG   |TO ORG     |LOT NUMBER                    |QUANTITY   |SUBINVENTORY');
  write_output('-----------------------------------------------------------------------------------------------------------------------------------');
  
 IF P_TRANSACTION_TYPE = 'RECEIVE' THEN 
 
  FOR r_lines IN c_lines loop
  
    BEGIN
      INSERT INTO XXWC.XXWC_RCV_INTORG_TBL
        VALUES
         (SYSDATE,
          G_USER_ID,
          SYSDATE,
          G_USER_ID,
          l_employee_id,
          P_ORG_ID,
          P_TRANSACTION_TYPE,
          r_lines.receipt_num,
          r_lines.shipment_num,
          r_lines.shipment_header_id,
          r_lines.organization_id,
          r_lines.shipment_line_id,
          r_lines.to_organization_id,
          r_lines.receipt_source_code,
          r_lines.transaction_date,
          r_lines.unit_of_measure,
          r_lines.primary_unit_of_measure,
          r_lines.uom_conversion_rate,
          r_lines.requisition_number,
          r_lines.line_num,
          r_lines.category_id,
          r_lines.inventory_item_id,
          r_lines.item_number, 
          r_lines.description,
          r_lines.header_id,
          r_lines.order_number,
          r_lines.line_id,
          r_lines.line_number,
          r_lines.quantity_shipped,
          r_lines.quantity_received,
          r_lines.quantity_open,
          r_lines.expected_receipt_date,
          r_lines.shipped_date,
          r_lines.lot_number,
          NULL,
          l_batch_id,
          1,
          NULL);
        
          
      EXCEPTION
          WHEN OTHERS THEN
              G_MESSAGE := 'P_TRANSACTION_TYPE ' || P_TRANSACTION_TYPE ||' '|| SQLCODE || SQLERRM;
              write_log_output (G_PROGRAM_NAME || ' ' || g_message);
              rollback;
      END;
     
      
      select rpad(substr(r_lines.shipment_num,1,20),20,' ') into l_shipment_number from dual;
      
      select rpad(substr(r_lines.item_number,1,30),30,' ') into l_item_number from dual;
      
      select rpad(get_organization_code(r_lines.organization_id),11,' ') into l_from_org  from dual;
      
      select rpad(get_organization_code(r_lines.to_organization_id),11,' ') into l_to_org from dual;
      
      select rpad(substr(nvl(r_lines.lot_number,' '),1,30),30,' ') into l_lot_number from dual;
      
      select rpad(substr(r_lines.quantity_open,1,11),11,' ') into l_qty from dual;
       
      write_output(l_shipment_number ||'|'||l_item_number ||'|'||l_from_org ||'|'||l_to_org||'|'||l_lot_number||'|'||l_qty||'|'||g_rcv_subinventory); 
   
     COMMIT;
     
    END LOOP;

 ELSIF P_TRANSACTION_TYPE = 'TRANSFER' THEN

 FOR r_lines IN c_xfer_lines loop
  
    BEGIN
      INSERT INTO XXWC.XXWC_RCV_INTORG_TBL
        VALUES
         (SYSDATE,
          G_USER_ID,
          SYSDATE,
          G_USER_ID,
          l_employee_id,
          P_ORG_ID,
          P_TRANSACTION_TYPE,
          r_lines.receipt_num,
          r_lines.shipment_num,
          r_lines.shipment_header_id,
          r_lines.organization_id,
          r_lines.shipment_line_id,
          r_lines.to_organization_id,
          r_lines.receipt_source_code,
          r_lines.transaction_date,
          r_lines.unit_of_measure,
          r_lines.primary_unit_of_measure,
          r_lines.uom_conversion_rate,
          r_lines.requisition_number,
          r_lines.line_num,
          r_lines.category_id,
          r_lines.inventory_item_id,
          r_lines.item_number, 
          r_lines.description,
          r_lines.header_id,
          r_lines.order_number,
          r_lines.line_id,
          r_lines.line_number,
          r_lines.quantity_shipped,
          r_lines.quantity_received,
          r_lines.quantity_open,
          r_lines.expected_receipt_date,
          r_lines.shipped_date,
          r_lines.lot_number,
          r_lines.group_id,
          l_batch_id,
          1,
          NULL);
 
        
          
      EXCEPTION
          WHEN OTHERS THEN
              G_MESSAGE := 'P_TRANSACTION_TYPE ' || P_TRANSACTION_TYPE ||' '|| SQLCODE || SQLERRM;
              write_log_output (G_PROGRAM_NAME || ' ' || g_message);
              rollback;
      END;
      
      select rpad(r_lines.shipment_num,20,' ') into l_shipment_number from dual;
      
      select rpad(r_lines.item_number,30,' ') into l_item_number from dual;
      
      select rpad(get_organization_code(r_lines.to_organization_id),11,' ') into l_from_org  from dual;
      
      select rpad(get_organization_code(r_lines.organization_id),11,' ') into l_to_org from dual;
      
      select rpad(substr(nvl(r_lines.lot_number,' '),1,30),30,' ') into l_lot_number from dual;
      
      select rpad(r_lines.quantity_received,11,' ') into l_qty from dual;
       
      write_output(l_shipment_number ||'|'||l_item_number ||'|'||l_from_org ||'|'||l_to_org||'|'||l_lot_number||'|'||l_qty||'|'||'**INTRANSIT**');
      
    COMMIT;
     
    END LOOP;
 
 ELSIF P_TRANSACTION_TYPE = 'RETURN' THEN

 FOR r_lines IN c_rtv_lines loop
  
    BEGIN
      INSERT INTO XXWC.XXWC_RCV_INTORG_TBL
        VALUES
         (SYSDATE,
          G_USER_ID,
          SYSDATE,
          G_USER_ID,
          l_employee_id,
          P_ORG_ID,
          P_TRANSACTION_TYPE,
          r_lines.receipt_num,
          r_lines.shipment_num,
          r_lines.shipment_header_id,
          r_lines.organization_id,
          r_lines.shipment_line_id,
          r_lines.to_organization_id,
          r_lines.receipt_source_code,
          r_lines.transaction_date,
          r_lines.unit_of_measure,
          r_lines.primary_unit_of_measure,
          r_lines.uom_conversion_rate,
          r_lines.requisition_number,
          r_lines.line_num,
          r_lines.category_id,
          r_lines.inventory_item_id,
          r_lines.item_number, 
          r_lines.description,
          r_lines.header_id,
          r_lines.order_number,
          r_lines.line_id,
          r_lines.line_number,
          r_lines.quantity_shipped,
          r_lines.quantity_received,
          r_lines.quantity_open,
          r_lines.expected_receipt_date,
          r_lines.shipped_date,
          r_lines.lot_number,
          r_lines.group_id,
          l_batch_id,
          1,
          NULL);
 
        
          
      EXCEPTION
          WHEN OTHERS THEN
              G_MESSAGE := 'P_TRANSACTION_TYPE ' || P_TRANSACTION_TYPE ||' '|| SQLCODE || SQLERRM;
              write_log_output (G_PROGRAM_NAME || ' ' || g_message);
              rollback;
      END;
      
      select rpad(r_lines.shipment_num,20,' ') into l_shipment_number from dual;
      
      select rpad(r_lines.item_number,30,' ') into l_item_number from dual;
      
      select rpad(get_organization_code(r_lines.organization_id),11,' ') into l_from_org  from dual;
      
      select rpad(get_organization_code(r_lines.to_organization_id),11,' ') into l_to_org from dual;
      
      select rpad(substr(nvl(r_lines.lot_number,' '),1,30),30,' ') into l_lot_number from dual;
      
      select rpad(r_lines.quantity_open,11,' ') into l_qty from dual;
       
      write_output(l_shipment_number ||'|'||l_item_number ||'|'||l_from_org ||'|'||l_to_org||'|'||l_lot_number||'|'||l_qty||'|'||g_rtv_subinventory);
       
     COMMIT;
     
    END LOOP;
 
  END IF;
  
 
   BEGIN
   
      g_count := 0;
      
      SELECT count(*)
      into   g_count
      from   XXWC.XXWC_RCV_INTORG_TBL
      WHERE  batch_id = l_batch_id;
   exception
    WHEN others THEN
       g_message := 'Could not retrieve count on XXWC_RCV_INTORG_TBL';
       write_log_output (G_PROGRAM_NAME || ' ' || g_message);
   END;
   
   write_log_output(G_PROGRAM_NAME || ' Count of records inserted into XXWC.XXWC_RCV_INTORG_TBL for Batch_id '|| l_batch_id ||' is '|| g_count);

    --If count is greater than 0 then retcode = 0 else 1
    IF g_count > 0 THEN
      
      x_retcode := 0;
    
    ELSE
    
      x_retcode := 1;
      
    END IF;
    
  write_output('                                                                                            ');
  write_output('                                                                                            ');
    
    
  write_log_output('******************************************************************');
  write_log_output('Ending Program '||G_PROGRAM_NAME);
  write_log_output('******************************************************************');

EXCEPTION
   WHEN OTHERS THEN
  
   G_MESSAGE := SQLCODE || SQLERRM;
   write_log_output (G_PROGRAM_NAME || ' When others exception ' || g_message);
   x_retcode := 2;
  
END INSERT_INTO_RCV_STAGE;


PROCEDURE INSERT_INTO_RCV_INTERFACE
    (P_ORG_ID IN NUMBER,
     P_BATCH_ID IN NUMBER,
     P_SUBINVENTORY IN VARCHAR2,
     P_TRANSACTION_TYPE IN VARCHAR2,
     X_RETCODE OUT NUMBER,
     X_RCV_GROUP_ID OUT NUMBER)
  IS
  
  CURSOR c_stg_lines IS
     SELECT
          rowid,
          created_by,
          last_updated_by,
          org_id,
          employee_id,
          receipt_num,
          shipment_num,
          shipment_header_id,
          organization_id,
          shipment_line_id,
          to_organization_id,
          receipt_source_code,
          transaction_date,
          unit_of_measure,
          primary_unit_of_measure,
          uom_conversion_rate,
          requisition_number,
          requisition_line_num,
          category_id,
          inventory_item_id,
          item_number, 
          description,
          oe_header_id,
          order_number,
          oe_line_id,
          oe_line_number,
          quantity_shipped,
          quantity_received,
          quantity_open,
          expected_receipt_date,
          shipped_date,
          lot_number,
          batch_id,
          process_flag
      FROM xxwc.xxwc_rcv_intorg_tbl
      WHERE process_flag = g_process_flag
      AND   batch_id = p_batch_id
      AND   org_id = p_org_id
      AND   transaction_type = p_transaction_type;
  
  
     cursor c_lots (p_shipment_line_id in number) is
        select mtln.lot_number,
               sum(mtln.transaction_quantity)
        from   mtl_transaction_lot_numbers mtln
        where  exists (select *
                       from   rcv_shipment_lines rsl
                       where  rsl.mmt_transaction_id = mtln.transaction_id
                       and    rsl.shipment_line_id = p_shipment_line_id)
        group by mtln.lot_number;
                       

     l_rcv_group_id NUMBER;    
     l_source_doc_code VARCHAR2(25);
     l_receipt_source_code VARCHAR2(25);
  
BEGIN
    G_NAME := 'INSERT_INTO_RCV_INTERFACE';
    G_PROGRAM_NAME := G_PACKAGE_NAME||'.'||G_NAME;

    write_log_output('******************************************************************');
    write_log_output('Starting Program '||G_PROGRAM_NAME);
    write_log_output('******************************************************************');
    write_log_output(G_PROGRAM_NAME || ' Parmeters');
    write_log_output('--------------------------------------------');
    write_log_output(G_PROGRAM_NAME||' P_ORG_ID ' || P_ORG_ID);
    write_log_output(G_PROGRAM_NAME||' P_BATCH_ID ' || P_BATCH_ID);
    write_log_output(G_PROGRAM_NAME||' P_SUBINVENTORY ' || P_SUBINVENTORY);
    write_log_output(G_PROGRAM_NAME||' P_TRANSACTION_TYPE '|| P_TRANSACTION_TYPE);
    write_log_output('--------------------------------------------');
    
    
   BEGIN
      SELECT rcv_interface_groups_s.NEXTVAL 
      INTO l_rcv_group_id 
      FROM DUAL;
   EXCEPTION
      WHEN OTHERS THEN
          g_message := G_PROGRAM_NAME ||' Could not retrieve l_rcv_group_id ' || SQLCODE || SQLERRM;
          raise g_exception;
   END;
   
  write_log_output (G_PROGRAM_NAME ||'l_rcv_group_id = ' || l_rcv_group_id);
  x_rcv_group_id := l_rcv_group_id;
   
  BEGIN
    select decode(P_TRANSACTION_TYPE,
                  'RECEIVE','REQ',
                  'RETURN','INVENTORY')
    into   l_source_doc_code
    from   dual;
  EXCEPTION
    WHEN OTHERS THEN
            l_source_doc_code := NULL;
  END;                
  
  write_log_output (G_PROGRAM_NAME ||'l_source_doc_code = ' || l_source_doc_code);
    
  
    BEGIN
    select decode(P_TRANSACTION_TYPE,
                  'RECEIVE','INTERNAL ORDER',
                  'RETURN','INVENTORY')
    into   l_receipt_source_code
    from   dual;
  EXCEPTION
    WHEN OTHERS THEN
            l_receipt_source_code := NULL;
  END; 
  
  write_log_output (G_PROGRAM_NAME ||'l_receipt_source_code = ' || l_receipt_source_code);
  
  IF l_source_doc_code is not null then
    FOR r_stg_lines IN c_stg_lines
      LOOP
      
      BEGIN
      
                   INSERT INTO rcv_headers_interface
                               (
                                header_interface_id,
                                org_id,
                                group_id,
                                processing_status_code,
                                receipt_source_code,
                                transaction_type,
                                auto_transact_code,
                                last_update_date,
                                last_update_login,
                                last_updated_by,
                                creation_date,
                                created_by,
                                validation_flag,
                                comments,
                                shipment_num,
                                from_organization_id,
                                ship_to_organization_id,
                                expected_receipt_date,
                                receipt_header_id,
                                employee_id,
                                attribute1
                                )
                        VALUES (
                                rcv_headers_interface_s.NEXTVAL,                                  --Header Interface ID
                                r_stg_lines.org_id,                                                        -- Org Id
                                l_rcv_group_id,                                                  --Group ID
                                'PENDING',                                                        --Processing Status Code
                                r_stg_lines.receipt_source_code,                                                 --Receipt source Code
                                'NEW',                                                            --Transaction Type
                                'DELIVER'  ,                                                      --AUTO Transact Code
                                SYSDATE,                                                          --last update date
                                r_stg_lines.last_updated_by,                                                             --last updated by
                                r_stg_lines.last_updated_by,                                                       --Last Update Login
                                SYSDATE,                                                          --creation date
                                r_stg_lines.created_by,                                                       --created by
                                'Y',                                                              --Validation Flag
                                'Auto Receive Balance of Short Intransit Shipments into Transfer Subinventory',--p_transaction_iface_rectype.transaction_reference,                --Comments
                                r_stg_lines.shipment_num,                      --Shipment Number
                                r_stg_lines.organization_id,--p_transaction_iface_rectype.organization_id,               --From Org
                                r_stg_lines.to_organization_id,                                                 --To org
                                r_stg_lines.expected_receipt_date,                --Expected Receipt Date
                                r_stg_lines.shipment_header_id,
                                r_stg_lines.employee_id,
                                g_suppress_traveler --Indicate to suppress printing the receipt traveler if Y then do not print if N then Print
                                );

      
      EXCEPTION
        WHEN OTHERS THEN
           g_message := 'Could not insert into RCV_HEADERS_INTERFACE ' || SQLCODE || SQLERRM;
           write_log_output (G_PROGRAM_NAME || ' ' || g_message);
           
                update xxwc.xxwc_rcv_intorg_tbl set process_flag = 3,
                                                    message = g_message
                where rowid = r_stg_lines.rowid
                and   batch_id = p_batch_id
                and   process_flag = g_process_flag;

      END;
    
    
      BEGIN
                                       INSERT INTO rcv_transactions_interface
                                                   (
                                                    header_interface_id,
                                                    org_id,
                                                    group_id,
                                                    interface_transaction_id,
                                                    transaction_type,
                                                    transaction_date,
                                                    processing_status_code,
                                                    processing_mode_code,
                                                    transaction_status_code,
                                                    category_id,
                                                    quantity,
                                                    primary_quantity,
                                                    last_update_date,
                                                    last_updated_by,
                                                    creation_date,
                                                    created_by,
                                                    receipt_source_code,
                                                    destination_type_code,
                                                    auto_transact_code,
                                                    source_document_code,
                                                    interface_source_code,
                                                    item_id,
                                                    item_description,
                                                    unit_of_measure,
                                                    primary_unit_of_measure,
                                                    --uom_code,
                                                    employee_id,
                                                    to_organization_id,
                                                    subinventory,
                                                    locator_id,
                                                    from_organization_id,
                                                    from_subinventory,
                                                    from_locator_id,
                                                    expected_receipt_date,
                                                    shipped_date,
                                                    shipment_num,
                                                    shipment_header_id,
                                                    shipment_line_id,
                                                    routing_header_id,
                                                    routing_step_id,
                                                    destination_context,
                                                    use_mtl_lot,
                                                    use_mtl_serial,
                                                    validation_flag,
                                                    reason_id,
                                                    attribute_category,
                                                    attribute1,
                                                    attribute2
                                                    )
                                          VALUES    (
                                                     rcv_headers_interface_s.CURRVAL,                    --Header Interface ID
                                                     r_stg_lines.org_id,                                          --Org Id
                                                     l_rcv_group_id,                                    --Group ID
                                                     rcv_transactions_interface_s.NEXTVAL,               --Interface_transaction_id
                                                     'RECEIVE',                                          --Transaction Type
                                                      SYSDATE,                                           --Transaction Date
                                                     'PENDING',                                          --Processing Status Code
                                                     'BATCH',                                            --Processing Mode Code
                                                     'PENDING',                                                    --Transaction Status Code
                                                      r_stg_lines.category_id,                                               --Category ID
                                                      r_stg_lines.quantity_open,                --Quantity
                                                      r_stg_lines.quantity_open,                 --Primary Quantity
                                                      SYSDATE,                                                     --last update date
                                                      r_stg_lines.last_updated_by,                                                  --last updated by
                                                      SYSDATE,                                                     --creation date
                                                      r_stg_lines.created_by,                                                  --created by
                                                      l_receipt_source_code,                                                 --Receipt source Code
                                                      'INVENTORY',                                                 --Destination Type Code
                                                      'DELIVER' ,                                                  --AUTO Transact Code
                                                      l_source_doc_code,                                                 --Source Document Code
                                                      'RCV',                                                       --Interface Source Code
                                                      r_stg_lines.inventory_item_id,                                         --Item ID
                                                      r_stg_lines.description,                                          --Item Description
                                                      r_stg_lines.unit_of_measure,                                           --Unit Of Measure
                                                      r_stg_lines.primary_unit_of_measure,                                   -- Primary Unit Of Measure
                                                      --p_transaction_iface_rectype.transaction_uom,               --UOM COde
                                                      r_stg_lines.employee_id,                                              --Employee
                                                      r_stg_lines.to_organization_id,                                            --To Organization ID
                                                      p_subinventory,                                              --Sub Inventory ID
                                                      NULL,                --To Locator
                                                      r_stg_lines.organization_id,                                          --From Organization
                                                      NULL,                                                        --From Subinventory
                                                      NULL,                      --From Locator
                                                      r_stg_lines.expected_receipt_date,           --Expected Receipt Date
                                                      r_stg_lines.shipped_date,                                              --Shipped Date
                                                      r_stg_lines.shipment_num,                                              --Shipment Number
                                                      r_stg_lines.shipment_header_id,
                                                      r_stg_lines.shipment_line_id ,
                                                      3,
                                                      1,
                                                      'INVENTORY',
                                                      2,
                                                      1,
                                                      'Y',
                                                      g_reason_id,
                                                      'WC RTV ORG',
                                                       P_TRANSACTION_TYPE,
                                                       P_BATCH_ID);

         EXCEPTION
              WHEN OTHERS THEN
                g_message := 'Could not insert into RCV_TRANSACTIONS_INTERFACE ' || SQLCODE || SQLERRM;
                write_log_output (G_PROGRAM_NAME || ' ' || g_message);
                
                update xxwc.xxwc_rcv_intorg_tbl set process_flag = 3,
                                                    message = g_message
                where rowid = r_stg_lines.rowid
                and   batch_id = p_batch_id
                and   process_flag = g_process_flag;

         END;
    
    
    
        BEGIN
                update xxwc.xxwc_rcv_intorg_tbl set process_flag = 2,
                                                    message = 'success'
                where rowid = r_stg_lines.rowid
                and   batch_id = p_batch_id
                and   process_flag = g_process_flag;
        
        EXCEPTION
            WHEN OTHERS THEN
                g_message := 'Could not update XXWC.XXWC_RCV_INTORG_TBL';
                write_log_output (G_PROGRAM_NAME || ' ' || g_message);
                
        END;
    
    
        g_lot_control_code := NULL;
        
        g_lot_control_code := lot_control_code(r_stg_lines.inventory_item_id, r_stg_lines.organization_id);
        
            If g_lot_control_code = 2 THEN 

             --FOR R_LOTS IN C_LOTS (r_stg_lines.shipment_line_id) 
             
              --LOOP
             
               BEGIN

                
                 
                    INSERT INTO mtl_transaction_lots_interface(
                                                               --source_code,
                                                               --source_line_id,
                                                               transaction_interface_id,
                                                               lot_number,
                                                               transaction_quantity,
                                                               primary_quantity,
                                                               process_flag,
                                                               product_code,
                                                               product_transaction_id,
                                                               reason_id,
                                                               last_update_date,
                                                               last_updated_by,
                                                               creation_date,
                                                               created_by
                                                               )
                                                       VALUES  (
                                                                --l_source_code,
                                                                --l_source_line_id,
                                                                mtl_material_transactions_s.NEXTVAL,
                                                                r_stg_lines.lot_number,
                                                                r_stg_lines.quantity_open,  --Transaction Quantity
                                                                r_stg_lines.quantity_open,  --Transaction Quantity
                                                                NULL,--l_process_flag,                                    --Process flag   , 1-yes ,  2-no  , 3-error
                                                                'RCV',
                                                                rcv_transactions_interface_s.CURRVAL,
                                                                g_reason_id,
                                                                SYSDATE,
                                                                r_stg_lines.last_updated_by,
                                                                SYSDATE,
                                                                r_stg_lines.created_by
                                                                );

               END;
               
              --END LOOP;
           
        END IF;
        
        
        COMMIT;
        
    END LOOP;
 
   ELSE
   
    g_message:= (G_PROGRAM_NAME ||'Did not insert records into RCV_HEADERS_INTERFACE or RCV_TRANSACTIONS_INTERFACE due to missing receipt code');
    raise g_exception;
   
   END IF;
   
    
    
   BEGIN
   
    g_count := 0;
    
        select count(*)
        into   g_count
        from   rcv_headers_interface
        where  group_id = l_rcv_group_id;
   
   EXCEPTION
   
    WHEN OTHERS THEN
    
        g_count := 0;  
   
   END;
    
   
   write_log_output(G_PROGRAM_NAME || ' RCV_HEADERS_INTERFACE count ' || g_count|| '  for GROUP_ID '||l_rcv_group_id);
   
   if g_count > 0 THEN

      x_retcode := 0;
      
   else
   
      x_retcode := 1;
    
   end if;   
       
   
   write_output(' ');
   write_output ('Count of records inserted into RCV_HEADERS_INTERFACE ' || g_count  ||' for GROUP_ID '||l_rcv_group_id);
   
   
   if x_retcode = 0 THEN 
       
       BEGIN
       
        g_count := 0;
        
            select count(*)
            into   g_count
            from   rcv_transactions_interface
            where  group_id = l_rcv_group_id;
       
       EXCEPTION
       
        WHEN OTHERS THEN
        
            g_count := 0;  
       
       END;
        
   
   write_output ('Count of records inserted into RCV_TRANSACTIONS_INTERFACE ' || g_count ||' for GROUP_ID '||l_rcv_group_id);
    
       if g_count > 0 THEN

          x_retcode := 0;
          
       else
       
          x_retcode := 1;
        
       end if;   
   
   begin
   
    select count(mtli.product_transaction_id)
    into   g_count
    from   mtl_transaction_lots_interface mtli,
           rcv_transactions_interface rti
    where  rti.group_id = l_rcv_group_id
    and    rti.interface_transaction_id = mtli.product_transaction_id;
       
    
   exception
        when others then
            g_count := 0;
   
   end;
   
      write_output('Count of records inserted into MTL_TRANSACTION_LOTS_INTERFACE ' || g_count);
      write_output(' ');
      
  end if;
   
   write_log_output('******************************************************************');
   write_log_output('Ending Program '||G_PROGRAM_NAME);
   write_log_output('******************************************************************');

EXCEPTION
   WHEN G_EXCEPTION THEN
  
      write_log_output(G_MESSAGE);
      x_retcode := 2;   
  
  WHEN OTHERS THEN
  
     G_MESSAGE := SQLCODE || SQLERRM;
     write_log_output (G_PROGRAM_NAME || ' When others exception ' || g_message);
     x_retcode := 2;
     
END INSERT_INTO_RCV_INTERFACE;

 PROCEDURE PROCESS_RCV_MGR    (P_RCV_GROUP_ID  IN   NUMBER,
                               P_ORG_ID        IN   NUMBER,
                               X_RETCODE       OUT  NUMBER
                              )
        IS


 retval          NUMBER;
 l_trans_count   NUMBER;
 l_msg_cnt       NUMBER;
 l_req_id        NUMBER;
 l_complete_flag BOOLEAN;
 l_phase         VARCHAR2(100);
 l_status        VARCHAR2(100);
 l_dev_phase     VARCHAR2(100);
 l_dev_status    VARCHAR2(100);
 l_message       VARCHAR2(1000);
 l_msg_data      VARCHAR2(2000);

 BEGIN

    G_NAME := 'PROCESS_RCV_MGR';
    G_PROGRAM_NAME := G_PACKAGE_NAME||'.'||G_NAME;

    
    write_log_output('******************************************************************');
    write_log_output('Starting Program '||G_PROGRAM_NAME);
    write_log_output('******************************************************************');
    write_log_output(G_PROGRAM_NAME || ' Parmeters');
    write_log_output('--------------------------------------------');
    write_log_output(G_PROGRAM_NAME||' P_ORG_ID ' || P_ORG_ID);
    write_log_output(G_PROGRAM_NAME||' P_RCV_GROUP_ID ' || P_RCV_GROUP_ID);
    write_log_output('--------------------------------------------');


 MO_GLOBAL.INIT('PO');
 

 l_req_id := fnd_request.submit_request('PO', 
                                        'RVCTP', 
                                        '', 
                                        '', 
                                        FALSE, 
                                        'BATCH', 
                                        p_rcv_group_id, 
                                        p_org_id
                                        );


    IF l_req_id <= 0 THEN
     x_retcode:= 2;
     write_log_output(G_PROGRAM_NAME || '*** Receiving Transaction Processor runing exception ***');
    ELSIF l_req_id > 0 THEN
     write_log_output(G_PROGRAM_NAME || '*** Receiving Transaction Processor Request Submit successful ***');
    COMMIT;
    l_complete_flag := fnd_concurrent.wait_for_request(l_req_id,
                                                       1,
                                                       3600,
                                                       l_phase,
                                                       l_status,
                                                       l_dev_phase,
                                                       l_dev_status,
                                                       l_message);


    END IF;

    IF l_complete_flag  THEN
        write_log_output(G_PROGRAM_NAME || ' PLEASE VALIDATE RECEVING TRANSACTIONS');
    END IF;

    COMMIT;
    
    x_retcode := 0;
    
    write_log_output('******************************************************************');
    write_log_output('Ending Program '||G_PROGRAM_NAME);
    write_log_output('******************************************************************');
    
    

 EXCEPTION
 WHEN OTHERS THEN
 x_retcode := 2;
    write_log_output(G_PROGRAM_NAME || 'ERROR IN PROCEDURE BB_PROCESS_RCV_MGR. ERROR : '||SUBSTR(SQLERRM,1,2000));
 END PROCESS_RCV_MGR;

PROCEDURE INSERT_INTO_MT_INTERFACE
          (P_ORG_ID IN NUMBER,
           P_BATCH_ID IN NUMBER,
           P_SUBINVENTORY IN VARCHAR2,
           P_TRANSACTION_TYPE IN VARCHAR2,
           X_RETCODE OUT NUMBER,
           X_TXN_HEADER_ID OUT NUMBER,
           X_TRANSACTION_DATE OUT DATE) IS

CURSOR c_stg_lines IS
     SELECT
          rowid,
          created_by,
          last_updated_by,
          org_id,
          employee_id,
          receipt_num,
          shipment_num,
          shipment_header_id,
          organization_id,
          shipment_line_id,
          to_organization_id,
          receipt_source_code,
          transaction_date,
          unit_of_measure,
          primary_unit_of_measure,
          uom_conversion_rate,
          requisition_number,
          requisition_line_num,
          category_id,
          inventory_item_id,
          item_number, 
          description,
          oe_header_id,
          order_number,
          oe_line_id,
          oe_line_number,
          quantity_shipped,
          quantity_received,
          quantity_open,
          expected_receipt_date,
          shipped_date,
          lot_number,
          batch_id,
          process_flag,
          group_id
      FROM xxwc.xxwc_rcv_intorg_tbl
      WHERE process_flag = g_process_flag
      AND   batch_id = p_batch_id
      AND   org_id = p_org_id
      AND   transaction_type = p_transaction_type;
      
      cursor c_lots (p_shipment_line_id in number,
                     p_quantity in NUMBER) is
       select mtln.lot_number, mtln.transaction_quantity
       from   rcv_transactions rt,
               mtl_material_transactions mmt,
               mtl_transaction_lot_numbers mtln
       where  rt.transaction_id = mmt.rcv_transaction_id
       and    mtln.transaction_id = mmt.transaction_id
       and    rt.shipment_line_id = p_shipment_line_id
       and    mtln.transaction_quantity = p_quantity;

      
      l_subinventory varchar2(10);     
      l_uom_code varchar2(3);
      l_transaction_interface_id number;
      l_source_line_id               NUMBER;
      l_source_header_id             NUMBER;
      l_process_flag                 NUMBER    :=  1;       -- 1=yes , 2=no .... set to no until all translations completed
      l_transaction_mode             NUMBER    :=  3;       -- background process
      l_transaction_header_id        NUMBER;
      l_lock_flag                    NUMBER    :=  2;       -- not locked
      l_validation_required          NUMBER    :=  2;
              
      
BEGIN

    G_NAME := 'INSERT_INTO_MT_INTERFACE';
    G_PROGRAM_NAME := G_PACKAGE_NAME||'.'||G_NAME;

    
    write_log_output('******************************************************************');
    write_log_output('Starting Program '||G_PROGRAM_NAME);
    write_log_output('******************************************************************');
    write_log_output(G_PROGRAM_NAME || ' Parmeters');
    write_log_output('--------------------------------------------');
    write_log_output(G_PROGRAM_NAME||' P_ORG_ID ' || P_ORG_ID);
    write_log_output(G_PROGRAM_NAME||' P_BATCH_ID ' || P_BATCH_ID);
    write_log_output(G_PROGRAM_NAME||' P_SUBINVENTORY ' || P_SUBINVENTORY);
    write_log_output(G_PROGRAM_NAME||' P_TRANSACTION_TYPE ' || P_TRANSACTION_TYPE);
    write_log_output('--------------------------------------------');
    


     BEGIN
              
                select mtl_material_transactions_s.NEXTVAL
                into   l_transaction_header_id
                from   dual;
              
     END;
              
     x_txn_header_id := l_transaction_header_id;
    
    for r_stg_lines in c_stg_lines 
    
    
        LOOP
        
          BEGIN
            select mmt.SUBINVENTORY_CODE
            into   l_subinventory
            from   mtl_material_transactions mmt,
                   rcv_transactions rt
            where  mmt.rcv_transaction_id = rt.transaction_id
            and    rt.DESTINATION_TYPE_CODE = 'INVENTORY'
            and    rt.group_id = r_stg_lines.group_id
            and    rt.shipment_line_id = r_stg_lines.shipment_line_id
            and    rownum = 1;
            
           EXCEPTION
              WHEN OTHERS THEN
                   g_message := ' Could not find subinventory code ' ||SQLCODE || SQLERRM;
                   
                   update xxwc.xxwc_rcv_intorg_tbl set process_flag = 3,
                                                       message = g_program_name || g_message
                    where rowid = r_stg_lines.rowid; 
           
           END;
        
        
          BEGIN
          
            select uom_code
            into   l_uom_code
            from   mtl_units_of_measure
            where  unit_of_measure = r_stg_lines.primary_unit_of_measure;
          EXCEPTION
              WHEN OTHERS THEN
                   g_message := ' Could not find uom code ' ||SQLCODE || SQLERRM;
                   
                   update xxwc.xxwc_rcv_intorg_tbl set process_flag = 3,
                                                       message = g_program_name || g_message
                    where rowid = r_stg_lines.rowid; 
           
          END;
            
        
        if l_subinventory is not null and l_uom_code is not null then

              --TMS Ticket # 20130826-00733 Added insert statement 8/26/2013 by Lee Spitzer for BOD Issue
              BEGIN
                   
                   Insert Into XXWC.XXWCINV_INTERCO_TRANSFERS
                                         (transaction_id
                                         ,inventory_item_id
                                         ,organization_id
                                         ,subinventory_code
                                         ,locator_id
                                         ,revision
                                         ,orig_date_received
                                         ,quantity
                                         ,creation_date
                                         ,shipment_number
                                         )
                                select NULL
                                       ,moqd.inventory_item_id               -- inventory_item_id
                                       ,moqd.organization_id                 -- organization_id
                                       ,moqd.subinventory_code               -- subinventory_code
                                       ,moqd.locator_id                      -- location_id
                                       ,moqd.revision                        -- revision
                                       ,moqd.orig_date_received              -- orig_date_received
                                       ,moqd.primary_transaction_quantity    -- quantity 
                                       ,SYSDATE      
                                       ,r_stg_lines.shipment_num
                                from   mtl_onhand_quantities_detail moqd
                                where  moqd.subinventory_code = l_subinventory
                                AND    moqd.inventory_item_id = r_stg_lines.inventory_item_id
                                --AND    moqd.organization_id = r_stg_lines.organization_id --removed 1/21/2014 by LSPITZER per TMS Ticket 20131023-00301, need to use the to_organization_id
                                AND    moqd.organization_id = r_stg_lines.to_organization_id --added 1/21/2014 by LSPITZER per TMS Ticket 20131023-00301,  replaced with to_organization_id instead of organization_id
                                AND    nvl(moqd.lot_number,'~') = nvl(r_stg_lines.lot_number,'~')
                                ;

                                         
                                Exception
                                  When OTHERS Then
                                    ROLLBACK;
                                    NULL;
                                  write_log_output ('When others exception inserting records into intercompany table =' || SQLCODE || SQLERRM);
                                   
                                  
                                  
                             End;
                            
                           -- COMMIT;
                            
                        --End 20130826-00733 8/26/2013 by Lee Spitzer for BOD Issue
                        
                       
                       
                        COMMIT;
                        



              BEGIN
              
                select mtl_material_transactions_s.NEXTVAL
                into   l_transaction_interface_id
                from   dual;
              
              END;
              
              INSERT INTO mtl_transactions_interface (inventory_item_id,
                                                      subinventory_code,
                                                      transaction_quantity,
                                                      transaction_uom,
                                                      organization_id,
                                                      source_code,
                                                      source_line_id,
                                                      source_header_id,
                                                      process_flag,
                                                      transaction_mode,
                                                      transaction_header_id,
                                                      lock_flag,
                                                      validation_required,
                                                      transaction_date,
                                                      transaction_type_id,
                                                      reason_id,
                                                      transaction_source_type_id,
                                                      transaction_source_name,
                                                      material_account,
                                                      distribution_account_id,
                                                      transfer_organization,
                                                      transfer_subinventory,
                                                      shipment_number,
                                                      transaction_reference,
                                                      transaction_interface_id,
                                                      last_updated_by,
                                                      last_update_date,
                                                      created_by,
                                                      creation_date
                                                     )
                                              VALUES
                                                    (r_stg_lines.inventory_item_id,
                                                     l_subinventory,
                                                     -1*r_stg_lines.quantity_received,
                                                     l_uom_code,
                                                     r_stg_lines.to_organization_id,
                                                     'WC INTERORG XFER',
                                                     l_transaction_interface_id,--l_source_line_id,
                                                     l_transaction_interface_id,--l_source_header_id,
                                                     l_process_flag,
                                                     l_validation_required,
                                                     l_transaction_header_id,
                                                     l_lock_flag,
                                                     l_validation_required,
                                                     sysdate, --Transaction Date
                                                     21, --transaction_type_id
                                                     g_reason_id, --reason_id
                                                     13, --Transaction Source Type Id 
                                                     'WC INTERORG XFER',
                                                     NULL, --Material Account
                                                     NULL, --Distribution Accoun
                                                     r_stg_lines.organization_id, --transfer_organization,
                                                     p_subinventory, --transfer_subinventory,
                                                     r_stg_lines.shipment_num,
                                                     r_stg_lines.shipment_num,
                                                     l_transaction_interface_id,
                                                     g_user_id,
                                                     SYSDATE,
                                                     g_user_id,
                                                     SYSDATE
                                                    );
      
      
            
                   update xxwc.xxwc_rcv_intorg_tbl set process_flag = 2,
                                                       message = 'success'
                    where rowid = r_stg_lines.rowid; 
           
        
        g_lot_control_code := NULL;
        
        g_lot_control_code := lot_control_code(r_stg_lines.inventory_item_id, r_stg_lines.organization_id);
        
            If g_lot_control_code = 2 THEN 

             --FOR R_LOTS IN C_LOTS (r_stg_lines.shipment_line_id, r_stg_lines.quantity_received) 
             
             -- LOOP
             
               BEGIN
                 
                                    INSERT INTO mtl_transaction_lots_interface(
                                                               source_code,
                                                               source_line_id,
                                                               transaction_interface_id,
                                                               lot_number,
                                                               transaction_quantity,
                                                               reason_id,
                                                               process_flag,
                                                               last_update_date,
                                                               last_updated_by,
                                                               creation_date,
                                                               created_by
                                                               )
                                                       VALUES  (
                                                                'WC INTERORG XFER',
                                                                l_transaction_interface_id,
                                                                l_transaction_interface_id,
                                                                r_stg_lines.lot_number,
                                                                -1*r_stg_lines.quantity_received,  --Transaction Quantity
                                                                g_reason_id,
                                                                g_process_flag,                                    --Process flag   , 1-yes ,  2-no  , 3-error
                                                                SYSDATE,
                                                                r_stg_lines.last_updated_by,
                                                                SYSDATE,
                                                                r_stg_lines.created_by
                                                                );


               
               END;
               
              --END LOOP;
           
        END IF;
        
        

    
      END IF;
      
    END LOOP;
    
    
    BEGIN
    
        select count(*)
        into   g_count
        from   mtl_transactions_interface
        where  transaction_header_id = l_transaction_header_id
        and    process_flag = 1;
    
    EXCEPTION
    
        when others then       
    
            g_count := 0;
    
    END;


    write_output(' ');
    write_output('Count of records inserted into MTL_TRANSACTIONS_INTERFACE ' || g_count);
    
    
    x_transaction_date := sysdate;
    
    if g_count > 0 THEN
    
        x_retcode := 0;
    
    else
    
        x_retcode := 1;
        
    end if;

    begin
   
    select count(mtli.transaction_interface_id)
    into   g_count
    from   mtl_transaction_lots_interface mtli,
           mtl_transactions_interface mti
    where  mti.transaction_header_id = l_transaction_header_id
    and    mti.process_flag = 1
    and    mti.transaction_interface_id = mtli.transaction_interface_id;
       
    
   exception
        when others then
            g_count := 0;
   
   end;
   
   write_output('Count of records inserted into MTL_TRANSACTION_LOTS_INTERFACE ' || g_count);
   write_output(' ');   

    write_log_output('******************************************************************');
    write_log_output('Ending Program '||G_PROGRAM_NAME);
    write_log_output('******************************************************************');
    
    
END INSERT_INTO_MT_INTERFACE;

 PROCEDURE   process_txn_mgr(p_txn_hdr_id    IN   NUMBER,
                              x_trans_count  OUT  NUMBER,
                              x_retcode      OUT  NUMBER)
        IS


 retval          NUMBER;
 l_trans_count   NUMBER;
 l_msg_cnt       NUMBER;
 l_return_status VARCHAR2(1);

 l_msg_data      VARCHAR2(2000);

 BEGIN
 
    G_NAME := 'PROCESS_TXN_MGR';
    G_PROGRAM_NAME := G_PACKAGE_NAME||'.'||G_NAME;

    
    write_log_output('******************************************************************');
    write_log_output('Starting Program '||G_PROGRAM_NAME);
    write_log_output('******************************************************************');
    write_log_output(G_PROGRAM_NAME || ' Parmeters');
    write_log_output('--------------------------------------------');
    write_log_output(G_PROGRAM_NAME||' P_TXN_HDR_ID ' || P_TXN_HDR_ID);
    write_log_output('--------------------------------------------');


 retval := inv_txn_manager_pub.process_Transactions(
                                                    p_api_version      => 1.0
                                                   ,p_init_msg_list    => fnd_api.g_true
                                                   ,p_commit           => fnd_api.g_true
                                                   ,p_validation_level => fnd_api.g_valid_level_full
                                                   ,x_return_status    => l_return_status
                                                   ,x_msg_count        => l_msg_cnt
                                                   ,x_msg_data         => l_msg_data
                                                   ,x_trans_count      => x_trans_count
                                                   ,p_table            => 1
                                                   ,p_header_id        => p_txn_hdr_id
                                                    );

    COMMIT;
    
 IF (l_return_status <> 'S') THEN
     write_log_output(G_PROGRAM_NAME||'ERROR EXISTS IN MATERIAL TRANSACTIONS INTERFACE, PLEASE VALIDATE AND FIX THE ERRORS.');
     write_log_output(G_PROGRAM_NAME||'ERROR : '||l_msg_data);
     x_retcode := 2;
 END IF;

   
    x_retcode :=0;
   
    write_log_output('******************************************************************');
    write_log_output('Ending Program '||G_PROGRAM_NAME);
    write_log_output('******************************************************************');
    

 EXCEPTION
 WHEN OTHERS THEN
 x_retcode := 2;
 write_log_output(G_PROGRAM_NAME||'ERROR IN PROCEDURE BB_PROCESS_TXN_MGR. ERROR : '||SUBSTR(SQLERRM,1,2000));
 END process_txn_mgr;


FUNCTION  LOT_CONTROL_CODE
            (P_INVENTORY_ITEM_ID IN NUMBER,
             P_ORGANIZATION_ID IN NUMBER)
    RETURN NUMBER
    
    IS
    
    l_lot_control_code NUMBER;

BEGIN

    
    BEGIN
    
        SELECT lot_control_code
        INTO   l_lot_control_code
        FROM   mtl_system_items_b
        WHERE  inventory_item_id = p_inventory_item_id
        AND    organization_id = p_organization_id;
    
    EXCEPTION
        WHEN OTHERS THEN
        l_lot_control_code := NULL;
    
    END;
    
    RETURN l_lot_control_code;
    
    
END LOT_CONTROL_CODE;


PROCEDURE  MAIN_PROGRAM_CCR
              (retbuf         OUT  VARCHAR2,
               retcode        OUT  NUMBER,
               P_ORG_ID IN NUMBER,
               P_DATE     IN VARCHAR2,
               P_RCV_SUBINVENTORY IN VARCHAR2,
               P_RTV_SUBINVENTORY IN VARCHAR2,
               P_PROCESS_FLAG IN NUMBER,
               P_BATCH_ID IN NUMBER,
               P_SUPPRESS_RECEIPT_TRAVELER IN NUMBER,
               P_REASON_ID IN NUMBER,
               P_SHIPMENT_LINE_ID IN NUMBER,  -- Added 3/21/13 TMS Ticket # 20130228-01300
               P_PROC_C_DFF IN VARCHAR2  -- Added 04/05/13 CG 
               )            IS
    
    l_date DATE; 
    l_transaction_type VARCHAR2(10);
    l_group_id NUMBER DEFAULT NULL;
    
    
    l_batch_id NUMBER;
    x_retcode NUMBER;
    x_rcv_group_id NUMBER;
    x_txn_hdr_id NUMBER;
    x_count NUMBER;
    x_transaction_date DATE;
    l_reason_name VARCHAR2(30);
           
 BEGIN
        
    G_NAME := 'MAIN_PROGRAM_CCR';
    G_PROGRAM_NAME := G_PACKAGE_NAME||'.'||G_NAME;

    write_log_output('******************************************************************');
    write_log_output('Starting Program '||G_PROGRAM_NAME);
    write_log_output('******************************************************************');
    write_log_output(G_PROGRAM_NAME ||' Parameters');
    write_log_output('--------------------------------------------');
    write_log_output(G_PROGRAM_NAME ||' P_ORG_ID = ' || P_ORG_ID);
    write_log_output(G_PROGRAM_NAME ||' P_DATE = ' || P_DATE);
    write_log_output(G_PROGRAM_NAME ||' P_RCV_SUBINVENTORY = ' || P_RCV_SUBINVENTORY);
    write_log_output(G_PROGRAM_NAME ||' P_RTV_SUBINVENTORY = ' || P_RTV_SUBINVENTORY);
    write_log_output(G_PROGRAM_NAME ||' P_PROCESS_FLAG = ' || P_PROCESS_FLAG);
    write_log_output(G_PROGRAM_NAME ||' P_BATCH_ID = ' || P_BATCH_ID);
    write_log_output(G_PROGRAM_NAME ||' P_SUPPRESS_RECEIPT_TRAVELER = ' || P_SUPPRESS_RECEIPT_TRAVELER); 
    write_log_output(G_PROGRAM_NAME ||' P_REASON_ID = ' || P_REASON_ID);
    write_log_output(G_PROGRAM_NAME ||' P_SHIPMENT_LINE_ID = '|| P_SHIPMENT_LINE_ID);
    write_log_output('--------------------------------------------');
    
    l_date := FND_DATE.CANONICAL_TO_DATE(p_date);   --Change the p_date parameter from a varchar2 to a date 
 
    write_log_output(G_PROGRAM_NAME ||' Update date parameter from varchar2 to date format, l_date = ' || l_date);
    
    g_shipment_line_id := p_shipment_line_id; --Updated g_shipment_header_id with p_shipment_header_id Added 3/21/13 TMS Ticket # 20130228-01300
    
    write_log_output(G_PROGRAM_NAME ||' Set g_shipment_line_id with p_shipment_line_id');
    write_log_output(G_PROGRAM_NAME ||' g_shipment_line_id = ' || g_shipment_line_id);
    
    -- 04/05/13 CG added
    g_proc_c_dff := P_PROC_C_DFF;
    write_log_output(G_PROGRAM_NAME ||' Set g_proc_c_dff with P_PROC_C_DFF');
    write_log_output(G_PROGRAM_NAME ||' g_proc_c_dff = ' || g_proc_c_dff);
    -- 04/05/13 CG added
    
    
    if p_batch_id is NULL then
    
      BEGIN
        SELECT xxwc.xxwc_rcv_intorg_s.nextval 
        INTO   l_batch_id
        FROM   dual;
      Exception
        WHEN others THEN
          g_message := 'Could not retrieve l_batch_id ' || SQLCODE || SQLERRM;
          write_log_output (G_PROGRAM_NAME || ' ' || g_message);
      END;
   
    else
    
        l_batch_id := p_batch_id;
        
    end if;
    
    
    
    BEGIN
    
        select decode(P_SUPPRESS_RECEIPT_TRAVELER,
                      1,'Y',
                      2,'N')
        into   g_suppress_traveler
        from   dual;
        
    EXCEPTION
        WHEN OTHERS THEN
            g_suppress_traveler := 'Y';
    
    END;
    
    
    BEGIN
    
        select reason_name
        into   l_reason_name
        from   mtl_transaction_reasons
        where  reason_id = p_reason_id;
    
    EXCEPTION
        WHEN OTHERS THEN 
            l_reason_name := NULL;
    
    END;
    
    g_process_flag := p_process_flag;
    
    g_reason_id := p_reason_id;
    
    write_log_output(G_PROGRAM_NAME||' G_PROCESS_FLAG  = ' || G_PROCESS_FLAG); 
    write_log_output(G_PROGRAM_NAME||' G_SUPPRESS_TRAVELER = ' || G_SUPPRESS_TRAVELER);
    write_log_output(G_PROGRAM_NAME||' G_REASON_ID = ' || G_REASON_ID);
    
    
    g_rcv_subinventory := TRIM(p_rcv_subinventory); 
    
    g_rtv_subinventory := TRIM(p_rtv_subinventory);
    
    
    write_log_output(G_PROGRAM_NAME||' G_RCV_SUBINVENTORY = ' || G_RCV_SUBINVENTORY);
    write_log_output(G_PROGRAM_NAME||' G_RTV_SUBINVENTORY = ' || G_RTV_SUBINVENTORY);
    
    
  write_output('               XXWC INV Intransit Short Receipt and Return Transfer Program                ');
  write_output('*******************************************************************************************');
  
  write_output(' ');
  write_output('BATCH_ID               :' || L_BATCH_ID);
  write_output('RECEIVING SUBINVENTORY :' || G_RCV_SUBINVENTORY);
  write_output('RETURN SUBINVENTORY    :' || G_RTV_SUBINVENTORY); 
  write_output('REASON CODE            :' || L_REASON_NAME);
  write_output('SYSDATE                :' || SYSDATE);
  write_output(' ');
      
    --Look for open shipments that were not full received
         ---For Open Intransit Shipment Receiving Transactions--
         ---Transaction Type = Receive and Group Id Must Be NULL
         ---Pass the date from concurrnet request
         
        l_transaction_type := 'RECEIVE';
        l_group_id := NULL;
        
    XXWC_RCV_INTORG_PKG.INSERT_INTO_RCV_STAGE(p_org_id, l_date, l_transaction_type, l_group_id, l_batch_id, x_retcode);
    
    --if x_retcode is greater than 1 then return error and exit
    if x_retcode > 0 then
        
        raise g_exception;
        
    end if;
 
 
    --Look Insert Open Shipments into Receviing Interface Tables
         
         ---For Open Intransit Shipment Receiving Transactions--
         ---Transaction Type = Receive and Group Id Must Be NULL
         ---Pass the date from concurrnet request
            
         l_transaction_type := 'RECEIVE';
            
    XXWC_RCV_INTORG_PKG.INSERT_INTO_RCV_INTERFACE(p_org_id, l_batch_id, g_rcv_subinventory, l_transaction_type, x_retcode, x_rcv_group_id);
 
 
    if x_retcode > 0 then
    
        raise g_exception;
        
    end if;
 
   
   
    --Process Receiving Transactions-- 
   XXWC_RCV_INTORG_PKG.PROCESS_RCV_MGR(x_rcv_group_id, p_org_id, x_retcode);
   
   if x_retcode > 0 then
        
        raise g_exception;
        
   end if;
   
  
 --For Transfer Transactions--
  --l_date DATE := NULL;
  --l_transaction_type VARCHAR2(10) := 'TRANSFER';
  --l_group_id NUMBER := 3928;
 
        l_transaction_type := 'TRANSFER';
        l_group_id := x_rcv_group_id;
        
    XXWC_RCV_INTORG_PKG.INSERT_INTO_RCV_STAGE(p_org_id, NULL, l_transaction_type, l_group_id, l_batch_id, x_retcode);
    
    --if x_retcode is greater than 1 then return error and exit
    if x_retcode > 0 then
        
        raise g_exception;
        
    end if;

   --Insert Interorg Transfers 
    /*p_org_id  NUMBER := 162;
      p_batch_id NUMBER := 7;
      p_subinventory VARCHAR2(10) := 'General';
      p_transaction_type varchar2(10) := 'TRANSFER';
      x_retcode NUMBER;
      x_txn_header_id NUMBER;*/
  
    
       
        l_transaction_type := 'TRANSFER';
  
   XXWC_RCV_INTORG_PKG.INSERT_INTO_MT_INTERFACE(p_org_id, l_batch_id, g_rtv_subinventory, l_transaction_type, x_retcode, x_txn_hdr_id, x_transaction_date);
   
   
   if x_retcode > 0 THEN
   
    raise g_exception;
    
   end if;
   
   
    --Execute the Transaction Manager
    
     XXWC_RCV_INTORG_PKG.process_txn_mgr(x_txn_hdr_id, x_count, x_retcode);
   
   
   if x_retcode > 0 then
   
    raise g_exception;
   
   end if;
   
   

 
  
  
    --Insert into  
    --For RTV Transactions--
    l_transaction_type := 'RETURN';
    l_group_id := NULL;
  
  XXWC_RCV_INTORG_PKG.INSERT_INTO_RCV_STAGE(p_org_id, x_transaction_date, l_transaction_type, l_group_id, l_batch_id, x_retcode);
  
  
   if x_retcode > 0 then
   
    raise g_exception;
   
   end if;
   
   
    l_transaction_type := 'RETURN';
            
    XXWC_RCV_INTORG_PKG.INSERT_INTO_RCV_INTERFACE(p_org_id, l_batch_id, g_rtv_subinventory, l_transaction_type, x_retcode, x_rcv_group_id);
 
 
    if x_retcode > 0 then
    
        raise g_exception;
        
    end if;
 
  
    --Process Receiving Transactions-- 
   XXWC_RCV_INTORG_PKG.PROCESS_RCV_MGR(x_rcv_group_id, p_org_id, x_retcode);
   
   if x_retcode > 0 then
        
        raise g_exception;
        
   end if;
   
  
   write_log_output('******************************************************************');
   write_log_output('Ending Program '||G_PROGRAM_NAME);
   write_log_output('******************************************************************');
    
   retcode := x_retcode;
    
 EXCEPTION
 
    when g_exception then
    
    write_log_output('******************************************************************');
    write_log_output('Ending Program '||G_PROGRAM_NAME);
    write_log_output('******************************************************************');
    
        retcode := x_retcode;
 
 END MAIN_PROGRAM_CCR;


    FUNCTION  GET_ORGANIZATION_CODE
            (P_ORGANIZATION_ID IN NUMBER)
    RETURN VARCHAR2 IS

    l_organization_code varchar2(3);    

    BEGIN

    BEGIN
        select organization_code
        into   l_organization_code
        from   mtl_parameters
        where  organization_id = p_organization_id;
    EXCEPTION
        when others then 
                l_organization_code := NULL;
    END;
    
    RETURN l_organization_code; 



    END GET_ORGANIZATION_CODE;


FUNCTION GET_LOT_NUMBER 
         (P_SHIPMENT_LINE_ID IN NUMBER)
         RETURN VARCHAR2 is
  
   l_lot_number varchar2(30);   
     
  BEGIN
  
        begin
            select mtln.lot_number
            into   l_lot_number
            from   mtl_transaction_lot_numbers mtln
            where  exists (select *
                           from   rcv_shipment_lines rsl
                           where  rsl.mmt_transaction_id = mtln.transaction_id
                           and    rsl.shipment_line_id = p_shipment_line_id);
            
        EXCEPTION
            WHEN OTHERS THEN
                    l_lot_number := NULL;
        END;
  
  
    RETURN l_lot_number;
    
  END;
  
  
  END XXWC_RCV_INTORG_PKG;
/
