CREATE OR REPLACE PACKAGE BODY APPS.xxwcap_vendor_upd_pkg IS

  -- Global variable declaration
  l_request_id             NUMBER := fnd_global.conc_request_id;
  l_program_application_id NUMBER := fnd_global.prog_appl_id;
  l_program_id             NUMBER := fnd_global.conc_program_id;
  l_err_callpoint          VARCHAR2(75);
  l_err_callfrom CONSTANT VARCHAR2(75) := 'xxwcap_vendor_upd_pkg';
  l_distro_list VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';


  /*******************************************************************************
  *
  * Description:
  *   This procedure updates the vendor site code. 
  *
   ===============================================================================
    VERSION  DATE         AUTHOR(S)         DESCRIPTION
    ------- -----------   --------------- --------------------------------------
    1.0     5-Mar-2013   Luong Vu        Created this procedure.
    1.1     10-OCT-2014   Veera 		  TMS# 20141001-00166 Canada Multi Org Changes
  ********************************************************************************/

  PROCEDURE update_ven_site_code(errbuf  OUT VARCHAR2
                                ,retcode OUT VARCHAR2) IS
  
    l_vendor_rec      ap_vendor_pub_pkg.r_vendor_rec_type;
    l_vendor_site_rec ap_vendor_pub_pkg.r_vendor_site_rec_type;
    l_return_status   VARCHAR2(2000);
    l_msg_count       NUMBER;
    l_msg_data        VARCHAR2(2000);
    l_message         VARCHAR2(2000) := NULL;
    --l_org             NUMBER := 162;  10/10/2014  TMS# 20141001-00166 Commented by Veera As per the Canada OU Test
	l_org             NUMBER := mo_global.get_current_org_id;  -- 10/10/2014 TMS# 20141001-00166 Added by Veera As per the Canada OU Test
    v_count           NUMBER;
    l_vendor_id       NUMBER;
    l_found           VARCHAR2(1);
  
  BEGIN
    dbms_output.enable(10000000);
    l_message := NULL;
  
    FOR c_code IN (SELECT *
                     FROM xxcus.xxcus_vendor_site_update_tmp)
    LOOP
      -- Vendor_id lookup
      l_found := 'Y';
      BEGIN
        SELECT vendor_id
          INTO l_vendor_id
          FROM apps.ap_supplier_sites s --ap.ap_suppliers s
         WHERE s.vendor_site_id = c_code.vendor_site_id;
      EXCEPTION
        WHEN no_data_found THEN
          l_found := 'N';
          dbms_output.put_line('vendor id was not found for vendor_site_id: ' ||
                               c_code.vendor_site_id);
      END;
      dbms_output.put_line('l_found = ' || l_found || 'vendor_site_id ' ||
                           c_code.vendor_site_id);
    
      l_vendor_site_rec.org_id           := l_org;
      l_vendor_site_rec.vendor_id        := l_vendor_id;
      l_vendor_site_rec.vendor_site_code := c_code.new_vendor_site_code;
    
      IF l_found = 'Y' THEN
        ap_vendor_pub_pkg.update_vendor_site(p_api_version => 1,
                                             p_init_msg_list => fnd_api.g_true,
                                             p_commit => fnd_api.g_false,
                                             p_validation_level => fnd_api.g_valid_level_full,
                                             x_return_status => l_return_status,
                                             x_msg_count => l_msg_count,
                                             x_msg_data => l_msg_data,
                                             p_vendor_site_rec => l_vendor_site_rec,
                                             p_vendor_site_id => c_code.vendor_site_id);
      
        IF l_return_status = 'S' THEN
          l_message := 'PROCESSED';
        
          dbms_output.put_line('Vendor site id:  ' ||
                               c_code.vendor_site_id || '  -  ' ||
                               l_message);
        
          SELECT COUNT(*)
            INTO v_count
            FROM ap.ap_suppliers s
           WHERE s.vendor_id = l_vendor_id
             AND NOT EXISTS
           ( --  No Active sites
                  SELECT 'Active Sites'
                    FROM apps.ap_supplier_sites ss
                   WHERE ss.vendor_id = s.vendor_id
                     AND ((ss.inactive_date IS NULL OR
                         ss.inactive_date > trunc(SYSDATE))));
        
          IF v_count = 1 THEN
          
            --l_vendor_rec.end_date_active := l_curr_date;
            --l_vendor_rec.enabled_flag    := 'N';   Version 1.1
          
            ap_vendor_pub_pkg.update_vendor(p_api_version => 1.0,
                                            p_init_msg_list => fnd_api.g_true,
                                            p_commit => fnd_api.g_false,
                                            p_validation_level => fnd_api.g_valid_level_full,
                                            x_return_status => l_return_status,
                                            x_msg_count => l_msg_count,
                                            x_msg_data => l_msg_data,
                                            p_vendor_rec => l_vendor_rec,
                                            p_vendor_id => l_vendor_id);
          
            dbms_output.put_line('Vendor id:  ' || l_vendor_id || '  -  ' ||
                                 l_message);
          
          END IF;
        
        ELSE
          l_message := 'ERROR:  ' || l_msg_data;
        
          dbms_output.put_line('Vendor site id:  ' ||
                               c_code.vendor_site_id || '  -  ' ||
                               l_message);
        
        END IF;
      END IF;
    END LOOP;
    COMMIT;
  END update_ven_site_code;

  /*******************************************************************************
  *
  * Description:
  *   This procedure updates the party site name. 
  *
   ===============================================================================
    VERSION  DATE         AUTHOR(S)         DESCRIPTION
    ------- -----------   --------------- --------------------------------------
    1.0     5-Mar-2013   Luong Vu        Created this procedure.
  
  ********************************************************************************/

  PROCEDURE update_party_site_name(errbuf  OUT VARCHAR2
                                  ,retcode OUT VARCHAR2) IS
  
    l_party_site_rec        hz_party_site_v2pub.party_site_rec_type;
    l_init_msg_list         VARCHAR2(250) := fnd_api.g_false;
    l_object_version_number NUMBER;
    l_return_status         VARCHAR2(2000);
    l_msg_count             NUMBER;
    l_msg_data              VARCHAR2(2000);
    l_message               VARCHAR2(2000) := NULL;
    --l_org             NUMBER := 162;  08/10/2014 Commented by Veera As per the Canada OU Test
	l_org                   NUMBER := mo_global.get_current_org_id;  --08/10/2014 Added by Veera As per the Canada OU Test
    l_party_id              NUMBER;
    v_count                 NUMBER;
  
  BEGIN
    dbms_output.enable(10000000);
    l_message := NULL;
  
    FOR c_name IN (SELECT *
                     FROM xxcus.xxcus_vendor_site_update_tmp)
    LOOP
    
      -- party_id lookup
      BEGIN
        SELECT party_id
          INTO l_party_id
          FROM ar.hz_party_sites
         WHERE party_site_id = c_name.party_site_id;
      EXCEPTION
        WHEN no_data_found THEN
          dbms_output.put_line('party id was not found for party site id: ' ||
                               c_name.party_site_id);
      END;
      -- get object version number
      BEGIN
        SELECT object_version_number
          INTO l_object_version_number
          FROM ar.hz_party_sites
         WHERE party_site_id = c_name.party_site_id;
      EXCEPTION
        WHEN no_data_found THEN
          dbms_output.put_line('party id was not found for party site id: ' ||
                               c_name.party_site_id);
      END;
    
      l_party_site_rec.party_id        := l_party_id;
      l_party_site_rec.party_site_id   := c_name.party_site_id;
      l_party_site_rec.party_site_name := c_name.new_supplier_site_name;
      --l_object_version_number          := l_object_version_number;
    
      -- Call the procedure
      hz_party_site_v2pub.update_party_site(p_init_msg_list => l_init_msg_list,
                                            p_party_site_rec => l_party_site_rec,
                                            p_object_version_number => l_object_version_number,
                                            x_return_status => l_return_status,
                                            x_msg_count => l_msg_count,
                                            x_msg_data => l_msg_data);
    END LOOP;
    COMMIT;
  END update_party_site_name;

END xxwcap_vendor_upd_pkg;
/
