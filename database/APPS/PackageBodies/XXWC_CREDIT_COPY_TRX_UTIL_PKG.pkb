--
-- XXWC_CREDIT_COPY_TRX_UTIL_PKG  (Package Body) 
--
CREATE OR REPLACE PACKAGE BODY APPS.XXWC_CREDIT_COPY_TRX_UTIL_PKG
IS
   /*************************************************************************
   *  Copyright (c) 2011 Lucidity Consulting Group
   *  All rights reserved.
   **************************************************************************
   *   $Header XXWC_CREDIT_COPY_TRX_UTIL_PKG.pkb $
   *   Module Name: XXWC_CREDIT_COPY_TRX_UTIL_PKG.pkb
   *
   *   PURPOSE:   This package is used by the utility package for HDS Copy Rebill
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        09/01/2011  Vivek Lakaman             Initial Version
   *   1.1        09/22/2014  Maharajan Shunmugam       TMS# 20141001-00155 Canada Multi Org changes
   * ***************************************************************************/

   /*************************************************************************
    *   Procedure : Get_Balance_due
    *
    *   PURPOSE:   This function return balance due for the invoice
    *   Parameter:
    *          IN
    *              p_customer_trx_id    -- Customer Trx id
    * ************************************************************************/

   FUNCTION Get_Balance_due (p_customer_trx_id IN NUMBER)
      RETURN NUMBER
   IS
      l_balance_due   NUMBER;
   BEGIN
      SELECT NVL (SUM (amount_due_remaining), 0)
        INTO l_balance_due
        FROM apps.ar_payment_schedules
       WHERE customer_trx_id = p_customer_trx_id;

      RETURN l_balance_due;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END Get_Balance_due;

   /*************************************************************************
    *   Procedure : Get_Invoice_Total
    *
    *   PURPOSE:   This function return invoice total for the invoice
    *   Parameter:
    *          IN
    *              p_customer_trx_id    -- Customer Trx id
    * ************************************************************************/
   FUNCTION Get_Invoice_Total (p_customer_trx_id IN NUMBER)
      RETURN NUMBER
   IS
      l_Invoice_Total   NUMBER;
   BEGIN
      SELECT NVL (AMOUNT_DUE_ORIGINAL, 0)
        INTO l_Invoice_Total
        FROM apps.ar_payment_schedules
       WHERE customer_trx_id = p_customer_trx_id AND ROWNUM = 1;

      RETURN l_Invoice_Total;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END Get_Invoice_Total;

   /*************************************************************************
    *   Procedure : Get_Batch_source_Id
    *
    *   PURPOSE:   This function return batch_source_id
    *   Parameter:
    *          IN
    *              p_customer_trx_id    -- Customer Trx id
    * ************************************************************************/
   FUNCTION Get_Batch_source_Id (p_customer_trx_id IN NUMBER)
      RETURN NUMBER
   IS
      l_batch_source_id   NUMBER;
   BEGIN
      SELECT batch_source_id
        INTO l_batch_source_id
        FROM apps.ra_customer_trx
       WHERE customer_trx_id = p_customer_trx_id;

      RETURN l_batch_source_id;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END Get_Batch_source_Id;

   /*************************************************************************
    *   Procedure : Get_Batch_Source_Name
    *
    *   PURPOSE:   This function return batch_source_id
    *   Parameter:
    *          IN
    *              p_customer_trx_id    -- Customer Trx id
    * ************************************************************************/
   FUNCTION Get_Batch_Source_Name (p_customer_trx_id IN NUMBER)
      RETURN VARCHAR2
   IS
      l_batch_source_name   ra_batch_sources.name%TYPE;
   BEGIN
      SELECT src.name
        INTO l_batch_source_name
        FROM apps.ra_customer_trx trx, apps.ra_batch_sources src
       WHERE     trx.customer_trx_id = p_customer_trx_id
             AND trx.batch_source_id = src.batch_source_id;

      RETURN l_batch_source_name;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END Get_Batch_Source_Name;

   /*************************************************************************
    *   Procedure : Get_GL_Date
    *
    *   PURPOSE:   This function return GL date for the invoice
    *   Parameter:
    *          IN
    *              p_customer_trx_id    -- Customer Trx id
    * ***********************************************************************/

   FUNCTION Get_GL_Date (p_customer_trx_id IN NUMBER)
      RETURN DATE
   IS
      l_ledger_id        NUMBER := fnd_profile.VALUE ('GL_SET_OF_BKS_ID');
      l_closing_status   gl_period_statuses_v.closing_status%TYPE;
      l_gl_date          DATE;
      l_trx_gl_date      DATE;

      CURSOR C1
      IS
           SELECT a.start_date
             FROM apps.gl_period_statuses_v a, apps.fnd_application b
            WHERE     a.application_id = b.application_id
                  AND a.ledger_id = l_ledger_id
                  AND b.application_short_name = 'SQLGL'
                  AND closing_status = 'O'
         --AND ROWNUM = 1
         ORDER BY period_year DESC, period_num DESC;
   BEGIN
      BEGIN
         SELECT DISTINCT b.gl_date
           INTO l_trx_gl_date
           FROM apps.ra_cust_trx_line_gl_dist b
          WHERE b.customer_trx_id = p_customer_trx_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_trx_gl_date := NULL;
      END;

      BEGIN
         SELECT a.closing_status
           INTO l_closing_status
           FROM apps.gl_period_statuses_v a, apps.fnd_application b
          WHERE     l_trx_gl_date BETWEEN a.start_date AND a.end_date
                AND a.application_id = b.application_id
                AND a.ledger_id = l_ledger_id
                AND b.application_short_name = 'SQLGL';
      EXCEPTION
         WHEN OTHERS
         THEN
            l_closing_status := NULL;
      END;

      IF l_closing_status = 'O'
      THEN
         l_gl_date := l_trx_gl_date;
      ELSE
         FOR i IN c1
         LOOP
            l_gl_date := i.start_date;
            EXIT;
         END LOOP;
      END IF;

      RETURN l_gl_date;
   END Get_GL_Date;


   /*************************************************************************
    *   Procedure : Get_Ship_To_Site__Number
    *
    *   PURPOSE:   This function return Site Number for the site use id
    *   Parameter:
    *          IN
    *              p_customer_trx_id    -- Customer Trx id
    * ************************************************************************/
   FUNCTION Get_Ship_To_Site__Number (p_SHIP_TO_SITE_USE_ID   IN NUMBER
                                     ,p_CUSTOMER_ID           IN NUMBER)
      RETURN VARCHAR2
   IS
      lx_ship_to_site_number   VARCHAR2 (200);
   BEGIN
      --Derive the ship to site number
      IF p_SHIP_TO_SITE_USE_ID IS NULL
      THEN
         lx_ship_to_site_number := NULL;
      ELSE
         BEGIN
            SELECT hps.party_site_number
              INTO lx_ship_to_site_number
              FROM apps.hz_cust_site_uses hcsu
                  ,apps.hz_cust_acct_sites hcas
                  ,apps.hz_party_sites hps
             WHERE     hcsu.site_use_id = p_SHIP_TO_SITE_USE_ID
                   AND hcsu.cust_acct_site_id = hcas.cust_acct_site_id
                   AND hcas.party_site_id = hps.party_site_id
                   AND hcas.cust_account_id = p_CUSTOMER_ID;
         EXCEPTION
            WHEN OTHERS
            THEN
               lx_ship_to_site_number := NULL;
         END;
      END IF;

      RETURN lx_ship_to_site_number;
   END Get_Ship_To_Site__Number;


   /*************************************************************************
    *   Procedure : Get_Bill_To_Site__Number
    *
    *   PURPOSE:   This function return Site Number for the site use id
    *   Parameter:
    *          IN
    *              p_customer_trx_id    -- Customer Trx id
    * ************************************************************************/


   FUNCTION Get_Bill_To_Site__Number (p_BILL_TO_SITE_USE_ID   IN NUMBER
                                     ,p_CUSTOMER_ID           IN NUMBER)
      RETURN VARCHAR2
   IS
      lx_bill_to_site_number   VARCHAR2 (200);
   BEGIN
      IF p_BILL_TO_SITE_USE_ID IS NULL
      THEN
         lx_bill_to_site_number := NULL;
      ELSE
         BEGIN
            SELECT hps.party_site_number
              INTO lx_bill_to_site_number
              FROM apps.hz_cust_site_uses hcsu
                  ,apps.hz_cust_acct_sites hcas
                  ,apps.hz_party_sites hps
             WHERE     hcsu.site_use_id = p_BILL_TO_SITE_USE_ID
                   AND hcsu.cust_acct_site_id = hcas.cust_acct_site_id
                   AND hcas.party_site_id = hps.party_site_id
                   AND hcas.cust_account_id = p_CUSTOMER_ID;
         EXCEPTION
            WHEN OTHERS
            THEN
               lx_bill_to_site_number := NULL;
         END;
      END IF;

      RETURN lx_bill_to_site_number;
   END Get_Bill_To_Site__Number;
END XXWC_CREDIT_COPY_TRX_UTIL_PKG;
/

