create or replace 
package body      XXWC_COMP_DEMAND_HIST_PKG as
/******************************************************************************
   NAME:       XXWC_COMP_DEMAND_HIST_PKG
   PURPOSE:
/* 
 -- Date: 24-APR-2013 
 -- Author: Dheeresh Chintala
 -- Scope: This package is to run Compile Demand History for all (multiple) inventory organizations.
 -- TMS ticket - 20121217-00830 
 -- Update History:
 --
 -- Parameters: None
 
   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        4/23/2013             1. Created this package.
******************************************************************************/

 -- Global variables
 G_Application    Fnd_Application.Application_Short_Name%Type :='INV';
 G_Cp_Short_Code  Fnd_Concurrent_Programs.Concurrent_Program_Name%Type :='INCFDH'; -- Compile Demand History

  procedure MAIN(
     retcode                 OUT NUMBER
   ,errbuf                  OUT VARCHAR2
   ,P_REGION                IN  VARCHAR2) is

  /*  CURSOR wc_inv_orgs (n_org_id IN NUMBER) IS
      SELECT organization_id,
             organization_code,
             organization_name,
             set_of_books_id,
             chart_of_accounts_id,
             legal_entity
        FROM org_organization_definitions
       WHERE 1 = 1
         AND operating_unit =n_org_id
         AND disable_date IS NULL
         AND organization_code NOT IN ('WCC', 'MST') --WC Corporate Organization, WC Item Master
         --AND rownum <3    --testing for upto 7 organizations
    ORDER BY organization_code ASC;
*/
    CURSOR wc_inv_orgs (n_org_id IN NUMBER) IS
      SELECT ood.organization_id,
             ood.organization_code,
             ood.organization_name,
             ood.set_of_books_id,
             ood.chart_of_accounts_id,
             ood.legal_entity,
             mp.attribute9 region
        FROM org_organization_definitions ood,
             mtl_parameters mp
       WHERE 1 = 1         
         AND ood.operating_unit = n_org_id 
         AND ood.disable_date IS NULL
         AND ood.organization_code NOT IN ('WCC', 'MST') --WC Corporate Organization, WC Item Master
         AND ood.organization_id = mp.organization_id
         AND mp.attribute9 = nvl(p_region,mp.attribute9)
    ORDER BY ood.organization_code ASC;


    type wc_inv_orgs_tbl is table of wc_inv_orgs%rowtype index by binary_integer;
    wc_inv_orgs_rec wc_inv_orgs_tbl;

   lc_request_data VARCHAR2(20) :='';
   ln_request_id   NUMBER :=0;
   n_req_id NUMBER :=0;
   l_org_id NUMBER :=0;
   n_count_child_requests NUMBER :=0;
   N_conc_pgm_id NUMBER :=0;
   N_running_count NUMBER :=0;
   b_keep_looping BOOLEAN;
   n_program_app_id NUMBER :=0;
   N_user_id NUMBER :=0;
   v_email       fnd_user.email_address%type :=Null;
   v_cp_long_name   fnd_concurrent_programs_tl.user_concurrent_program_name%type :=Null;
   v_cp_phase       Varchar2(40) :=Null;
   v_cp_status      Varchar2(40) :=Null;
   v_child_requests Varchar2(240) :=Null;

  
   output_file_id   UTL_FILE.FILE_TYPE;
   v_path varchar2(80);
   p_period_id NUMBER :=0;

  begin

   lc_request_data :=fnd_conc_global.request_data;

   n_req_id :=fnd_global.conc_request_id;

   if lc_request_data is null then

       lc_request_data :='1';

       l_org_id :=mo_global.get_current_org_id;

       --n_req_id :=fnd_global.conc_request_id;

       N_user_id :=fnd_global.user_id;

       fnd_file.put_line(fnd_file.log, '');
       fnd_file.put_line(fnd_file.log, 'l_org_id ='||l_org_id);
       fnd_file.put_line(fnd_file.log, 'n_user_id ='||n_user_id);
       fnd_file.put_line(fnd_file.log, '');

      Open wc_inv_orgs(l_org_id);
      Fetch wc_inv_orgs Bulk Collect Into wc_inv_orgs_rec;
      Close wc_inv_orgs;

      If wc_inv_orgs_rec.count =0 Then

       fnd_file.put_line(fnd_file.log,'No active inventory organizations found for operating unit WhiteCap');

      Else --wc_inv_orgs_rec.count >0

         -- We will kick off for every single inventory orgs and not wait to check how many are running at a time
          for indx in 1 .. wc_inv_orgs_rec.count loop

           
                --
                -- If the concurrent program requires operating unit as a parameter then uncomment the below call
                -- fnd_request.set_org_id(mo_global.get_current_org_id);
                --

                --Except for arguments 1, 2 and 7 all other arguments are defaulted based on the parameter values
                --established from a sample run by the end user

                ln_request_id :=fnd_request.submit_request
                      (
                       application      =>G_Application,
                       program          =>G_Cp_Short_Code,
                       description      =>'',
                       start_time       =>'',
                       sub_request      =>TRUE,
                       argument1        =>3, --p_period_type --(Days,Weeks, Periods)
                       argument2        =>1, --p_selection, --(All Inventory Items)
                       argument3        =>wc_inv_orgs_rec(Indx).organization_id, --Organization Id v_resolution_type, --Report Type
                       argument4        =>null, --v_specific_item
                       argument5        =>1100000062, --p_specific_category_set   (Inventory Category)
                       argument6        =>101,   ---p_category_structure
                       argument7        =>null   ---p_specific_category
                      );

                  if ln_request_id >0 then
                    fnd_file.put_line(fnd_file.log, 'Submitted Period Close Pending Transactions report  for organization ='
                                                     ||wc_inv_orgs_rec(indx).organization_name
                                                     ||', Request Id ='
                                                     ||ln_request_id
                                                    );
                  else
                    fnd_file.put_line(fnd_file.log, 'Failed to submit Period Close Pending Transactions report for organization ='
                                                     ||wc_inv_orgs_rec(indx).organization_name
                                                    );
                  end if;

                 fnd_conc_global.set_req_globals(conc_status =>'PAUSED' ,request_data =>lc_request_data);

                 lc_request_data :=to_char(to_number(lc_request_data)+1);
          end loop;

      End If; -- checking if wc_inv_orgs_rec.count =0

   else

        Begin
            select a.user_concurrent_program_name
                  ,c.email_address
            into   v_cp_long_name
                  ,v_email
            from fnd_amp_requests_v a
                ,fnd_concurrent_programs d
                ,fnd_user c
            where 1 =1
              and a.request_id =n_req_id
              and a.concurrent_program_id =d.concurrent_program_id
              and a.requested_by =c.user_id;

        Exception
         When Others Then
           Null;
        End;
    
      retcode :=0;
      errbuf :='Child request[s] completed. Exit XXWC Period Close Pending Transactions Report -Master';
      fnd_file.put_line(fnd_file.log,errbuf);
      --return;
   end if;
  exception
   when others then
    fnd_file.put_line(fnd_file.log, 'Error in caller apps.XXWC_PER_CLOSE_PEN_TRANS_PKG.main ='||sqlerrm);
    rollback;
  end main;

END XXWC_COMP_DEMAND_HIST_PKG; 