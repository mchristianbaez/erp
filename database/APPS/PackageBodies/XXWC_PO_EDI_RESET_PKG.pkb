CREATE OR REPLACE PACKAGE BODY APPS.xxwc_po_edi_reset_pkg
/*************************************************************************
  $Header XXWC_PO_EDI_FLAG_RESET_PKG.pkb $
  Module Name : XXWC_PO_EDI_FLAG_RESET_PKG

  PURPOSE     : Package to Reset the Fields for Retriggering POO and POCO documents
                POO  - PO Outbound
                POCO - PO change outbound
  TMS Task Id : 20140516-00168

  REVISIONS:
  Ver        Date          Author                Description
  ---------  ----------    ------------------    ----------------
  1.0        18-Feb-2015   Manjula Chellappan    Initial Version
  1.1        14-Oct-2015   M Hari Prasad         TMS # 20150911-00044 -- Research issue with XXWC PO EDI Reset Program.Added replace function for special charecters.

**************************************************************************/
IS
   g_distribution_list   VARCHAR2 (240) := 'HDSOracleDevelopers@hdsupply.com';

   PROCEDURE main (errbuf                  OUT VARCHAR2,
                   retcode                 OUT NUMBER,
                   p_trx_type              IN     VARCHAR2,
                   p_enable_attr           IN     VARCHAR2,
                   p_po_number             IN     VARCHAR2,
                   p_header_vendor_note    IN     VARCHAR2,
                   p_freight_acct_number   IN     VARCHAR2,
                   p_line_number           IN     NUMBER,
                   p_enable_item_desc      IN     VARCHAR2,
                   p_item_desc             IN     VARCHAR2)
   /*************************************************************************
      $Header XXWC_PO_EDI_FLAG_RESET_PKG.pkb $
      Module Name : XXWC_PO_EDI_FLAG_RESET_PKG.main

      PURPOSE     : Package to Reset the EDI related Fields for POO and POCO Retriggering
      TMS Task Id : 20140516-00168

      REVISIONS:
      Ver        Date          Author                Description
      ---------  ----------    ------------------    ----------------
      1.0        18-Feb-2015   Manjula Chellappan    Initial Version

    **************************************************************************/
   IS
      l_sec                VARCHAR2 (100);
      l_retcode            NUMBER;
      l_error_msg          VARCHAR2 (2000);
      l_validation_error   EXCEPTION;
      l_po_count           NUMBER;
      l_po_number          VARCHAR2 (15);
      l_po_full_number     VARCHAR2 (240);
      l_po_start_pos       NUMBER;
      l_po_end_pos         NUMBER;
   BEGIN
      fnd_file.put_line (fnd_file.LOG, 'Parameters ');

      fnd_file.put_line (fnd_file.LOG, '/****************************/');

      fnd_file.put_line (fnd_file.LOG,
                         'p_trx_type                :' || p_trx_type);
      fnd_file.put_line (fnd_file.LOG,
                         'p_po_number               :' || p_po_number);

      fnd_file.put_line (
         fnd_file.LOG,
         'p_header_vendor_note      :' || p_header_vendor_note);

      fnd_file.put_line (
         fnd_file.LOG,
         'p_freight_acct_num        :' || p_freight_acct_number);

      fnd_file.put_line (fnd_file.LOG,
                         'p_line_number             :' || p_line_number);
      fnd_file.put_line (fnd_file.LOG,
                         'p_item_desc               :' || p_item_desc);
      fnd_file.put_line (fnd_file.LOG, '/****************************/');


      l_sec := 'Start Process ';

      l_po_full_number := p_po_number;

      l_sec := 'Get PO count ';

      l_po_count :=
           (  LENGTH (l_po_full_number)
            - LENGTH (REPLACE (l_po_full_number, ',')))
         + 1;

      fnd_file.put_line (fnd_file.LOG,
                         'PO Count               :' || l_po_count);
      fnd_file.put_line (fnd_file.LOG, '/****************************/');

      IF p_trx_type = 'ATTR'
      THEN
         IF l_po_count = 1
         THEN
            l_sec := 'Update Attributes ';

            update_attributes (TRIM (p_po_number),
                               TRIM (p_header_vendor_note),
                               TRIM (p_freight_acct_number),
                               TRIM (p_line_number),
                               TRIM (p_item_desc),
                               l_retcode,
                               l_error_msg);
         ELSE
            FND_FILE.put_line (
               fnd_file.LOG,
               'Attributes can be updated for only one PO, please select one PO number');
         END IF;

         IF l_retcode = 2
         THEN
            retcode := 2;
            errbuf := l_error_msg;
         END IF;
      ELSIF p_trx_type = 'POO'
      THEN
         l_sec := 'Update POO EDI flags';

         l_po_start_pos := 1;

         FOR i IN 1 .. l_po_count
         LOOP
            IF i < l_po_count
            THEN
               l_po_end_pos :=
                  INSTR (l_po_full_number,
                         ',',
                         1,
                         i);
            ELSE
               l_po_end_pos := LENGTH (l_po_full_number) + 1;
            END IF;


            l_po_end_pos := l_po_end_pos - l_po_start_pos;



            fnd_file.put_line (
               fnd_file.LOG,
               'l_po_start_pos             :' || l_po_start_pos);
            fnd_file.put_line (
               fnd_file.LOG,
               'l_po_end_pos               :' || l_po_end_pos);

            l_po_number :=
               TRIM (SUBSTR (l_po_full_number, l_po_start_pos, l_po_end_pos));

            fnd_file.put_line (fnd_file.LOG,
                               'l_po_number                :' || l_po_number);

            update_poo_flag (l_po_number, l_retcode, l_error_msg);

            IF l_retcode = 2
            THEN
               retcode := 2;
               errbuf := l_error_msg;
            END IF;

            l_po_start_pos := l_po_start_pos + l_po_end_pos + 1;
         END LOOP;
      ELSIF p_trx_type = 'POCO'
      THEN
         l_sec := 'Update POCO EDI flags';

         l_po_start_pos := 1;

         FOR i IN 1 .. l_po_count
         LOOP
            IF i < l_po_count
            THEN
               l_po_end_pos :=
                  INSTR (l_po_full_number,
                         ',',
                         1,
                         i);
            ELSE
               l_po_end_pos := LENGTH (l_po_full_number) + 1;
            END IF;

            l_po_end_pos := l_po_end_pos - l_po_start_pos;

            fnd_file.put_line (
               fnd_file.LOG,
               'l_po_start_pos             :' || l_po_start_pos);
            fnd_file.put_line (
               fnd_file.LOG,
               'l_po_end_pos               :' || l_po_end_pos);

            l_po_number :=
               TRIM (SUBSTR (l_po_full_number, l_po_start_pos, l_po_end_pos));

            fnd_file.put_line (fnd_file.LOG,
                               'l_po_number                :' || l_po_number);

            update_poco_flag (l_po_number, l_retcode, l_error_msg);

            IF l_retcode = 2
            THEN
               retcode := 2;
               errbuf := l_error_msg;
            END IF;

            l_po_start_pos := l_po_start_pos + l_po_end_pos + 1;
         END LOOP;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_PO_EDI_RESET_PKG.MAIN',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (SQLERRM, 1, 240),
            p_distribution_list   => g_distribution_list,
            p_module              => 'PO');
   END main;



   PROCEDURE validate_poo (p_po_number   IN     VARCHAR2,
                           p_retcode        OUT VARCHAR2,
                           p_error_msg      OUT VARCHAR2)
   /*************************************************************************
     $Header XXWC_PO_EDI_FLAG_RESET_PKG.pkb $
     Module Name : XXWC_PO_EDI_FLAG_RESET_PKG.validate_poo

     PURPOSE     : Package to Reset the EDI related Fields for POO and POCO Retriggering
     TMS Task Id : 20140516-00168

     REVISIONS:
     Ver        Date          Author                Description
     ---------  ----------    ------------------    ----------------
     1.0        18-Feb-2015   Manjula Chellappan    Initial Version

   **************************************************************************/

   IS
      l_po_header_id           NUMBER;
      l_po_number              VARCHAR2 (20);
      l_vendor_id              NUMBER;
      l_edi_processed_flag     VARCHAR2 (1);
      l_latest_edi_flag        VARCHAR2 (1);
      l_authorization_status   VARCHAR2 (30);
      l_cancel_flag            VARCHAR2 (1);
      l_closed_code            VARCHAR2 (40);
      l_print_count            NUMBER;
      l_revision_num           NUMBER;
      l_header_vendor_note     VARCHAR2 (480);
      l_error_msg              VARCHAR2 (2000);
      l_validation_error       EXCEPTION;
   BEGIN
      BEGIN
             SELECT ph.po_header_id,
                    ph.segment1,
                    ph.edi_processed_flag,
                    pha.print_count,
                    ph.revision_num,
                    ph.note_to_vendor,
                    ph.authorization_status,
                    ph.cancel_flag,
                    ph.closed_code,
                    pha.edi_processed_flag
               INTO l_po_header_id,
                    l_po_number,
                    l_edi_processed_flag,
                    l_print_count,
                    l_revision_num,
                    l_header_vendor_note,
                    l_authorization_status,
                    l_cancel_flag,
                    l_closed_code,
                    l_latest_edi_flag
               FROM po_headers ph, po_headers_archive pha
              WHERE     ph.segment1 = p_po_number
                    AND ph.po_header_id = pha.po_header_id(+)
                    AND pha.latest_external_flag(+) = 'Y'
         FOR UPDATE NOWAIT;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_error_msg :=
               'PO ' || p_po_number || ' Can not be found' || ' ' || SQLERRM;
            fnd_file.put_line (fnd_file.LOG, l_error_msg);

            RAISE l_validation_error;
      END;

      fnd_file.put_line (fnd_file.LOG, '/****************************/');

      fnd_file.put_line (fnd_file.LOG, 'Validate for POO');

      fnd_file.put_line (fnd_file.LOG, '/****************************/');

      fnd_file.put_line (
         fnd_file.LOG,
         'Edi Processed Flag        :' || l_edi_processed_flag);

      fnd_file.put_line (fnd_file.LOG,
                         'Revision Num              :' || l_revision_num);

      fnd_file.put_line (fnd_file.LOG,
                         'Print Count               :' || l_print_count);
      fnd_file.put_line (
         fnd_file.LOG,
         'Authorization Status      :' || l_authorization_status);


      IF l_authorization_status <> 'APPROVED'
      THEN
         l_error_msg :=
               'PO '
            || p_po_number
            || ' is not Approved,Can not resend the POO ';

         fnd_file.put_line (fnd_file.LOG, l_error_msg);

         RAISE l_validation_error;
      ELSIF l_closed_code LIKE '%CLOSED%'
      THEN
         l_error_msg :=
            'PO ' || p_po_number || ' is Closed,Can not resend the POO ';

         fnd_file.put_line (fnd_file.LOG, l_error_msg);

         RAISE l_validation_error;
      ELSIF NVL (l_cancel_flag, 'N') = 'Y'
      THEN
         l_error_msg :=
            'PO ' || p_po_number || ' is cancelled, cannot resend POO ';

         fnd_file.put_line (fnd_file.LOG, l_error_msg);

         RAISE l_validation_error;
      ELSIF NVL (l_latest_edi_flag, 'N') <> 'Y'
      THEN
         l_error_msg :=
               'PO '
            || p_po_number
            || ' was not sent via EDI POO, can not resend POO ';


         fnd_file.put_line (fnd_file.LOG, l_error_msg);

         RAISE l_validation_error;
      END IF;

      fnd_file.put_line (fnd_file.LOG, 'POO can be resent ');

      p_retcode := 0;


      fnd_file.put_line (fnd_file.LOG, '/****************************/');
   EXCEPTION
      WHEN l_validation_error
      THEN
         p_retcode := 2;
         p_error_msg := l_error_msg;
         fnd_file.put_line (fnd_file.LOG, '/****************************/');
      WHEN OTHERS
      THEN
         fnd_file.put_line (fnd_file.LOG, l_error_msg);

         p_retcode := 2;
         p_error_msg := l_error_msg || ' ' || SQLERRM;

         fnd_file.put_line (fnd_file.LOG, p_error_msg);

         fnd_file.put_line (fnd_file.LOG, '/****************************/');
   END validate_poo;



   PROCEDURE validate_poco (p_po_number   IN     VARCHAR2,
                            p_retcode        OUT VARCHAR2,
                            p_error_msg      OUT VARCHAR2)
   /*************************************************************************
     $Header XXWC_PO_EDI_FLAG_RESET_PKG.pkb $
     Module Name : XXWC_PO_EDI_FLAG_RESET_PKG.validate_poco

     PURPOSE     : Procedure to validate if the PO can be updated to resend POCO
     TMS Task Id : 20140516-00168

     REVISIONS:
     Ver        Date          Author                Description
     ---------  ----------    ------------------    ----------------
     1.0        18-Feb-2015   Manjula Chellappan    Initial Version

   **************************************************************************/

   IS
      l_po_header_id           NUMBER;
      l_po_number              VARCHAR2 (20);
      l_vendor_id              NUMBER;
      l_edi_processed_flag     VARCHAR2 (1);
      l_latest_edi_flag        VARCHAR2 (1);
      l_vendor_site_id         NUMBER;
      l_authorization_status   VARCHAR2 (30);
      l_cancel_flag            VARCHAR2 (1);
      l_closed_code            VARCHAR2 (40);
      l_print_count            NUMBER;
      l_revision_num           NUMBER;
      l_poco_count             NUMBER;
      l_header_vendor_note     VARCHAR2 (480);
      l_error_msg              VARCHAR2 (2000);
      l_validation_error       EXCEPTION;
   BEGIN
      fnd_file.put_line (fnd_file.LOG, '/****************************/');
      fnd_file.put_line (fnd_file.LOG, 'Validate for POCO');
      fnd_file.put_line (fnd_file.LOG, '/****************************/');

      BEGIN
             SELECT ph.po_header_id,
                    ph.segment1,
                    ph.edi_processed_flag,
                    ph.print_count,
                    ph.revision_num,
                    ph.note_to_vendor,
                    ph.authorization_status,
                    ph.cancel_flag,
                    ph.closed_code,
                    pha.print_count,
                    pha.edi_processed_flag
               INTO l_po_header_id,
                    l_po_number,
                    l_edi_processed_flag,
                    l_print_count,
                    l_revision_num,
                    l_header_vendor_note,
                    l_authorization_status,
                    l_cancel_flag,
                    l_closed_code,
                    l_poco_count,
                    l_latest_edi_flag
               FROM po_headers ph, po_headers_archive pha
              WHERE     ph.segment1 = p_po_number
                    AND ph.po_header_id = pha.po_header_id(+)
                    AND pha.latest_external_flag(+) = 'Y'
         FOR UPDATE NOWAIT;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_error_msg := 'PO ' || p_po_number || ' Can not be found';
            fnd_file.put_line (fnd_file.LOG, l_error_msg);
            RAISE l_validation_error;
      END;

      fnd_file.put_line (fnd_file.LOG,
                         'Revision Num              :' || l_revision_num);
      fnd_file.put_line (fnd_file.LOG,
                         'Edi Processed Flag        :' || l_latest_edi_flag);

      fnd_file.put_line (fnd_file.LOG,
                         'Print Count               :' || l_poco_count);

      IF l_authorization_status <> 'APPROVED'
      THEN
         l_error_msg :=
               'PO '
            || p_po_number
            || ' is not Approved,Can not resend the POCO ';
         fnd_file.put_line (fnd_file.LOG, l_error_msg);

         RAISE l_validation_error;
      ELSIF l_closed_code LIKE '%CLOSED%'
      THEN
         l_error_msg :=
            'PO ' || p_po_number || ' is Closed,Can not resend the POCO ';
         fnd_file.put_line (fnd_file.LOG, l_error_msg);
         RAISE l_validation_error;
      ELSIF NVL (l_cancel_flag, 'N') = 'Y'
      THEN
         l_error_msg :=
            'PO ' || p_po_number || ' is cancelled, cannot resend POCO ';
         fnd_file.put_line (fnd_file.LOG, l_error_msg);
         RAISE l_validation_error;
      ELSIF NVL (l_edi_processed_flag, 'N') <> 'Y' AND l_revision_num = 0
      THEN
         l_error_msg :=
               'PO '
            || p_po_number
            || ' was not sent via EDI POCO, can not resend POCO ';

         fnd_file.put_line (fnd_file.LOG, l_error_msg);

         RAISE l_validation_error;
      ELSIF NVL (l_latest_edi_flag, 'N') <> 'Y'
      THEN
         l_error_msg :=
               'PO '
            || p_po_number
            || ' latest revision was not sent via EDI POCO, can not resend POCO ';

         fnd_file.put_line (fnd_file.LOG, l_error_msg);

         RAISE l_validation_error;
      END IF;

      fnd_file.put_line (fnd_file.LOG, 'POCO can be resent');

      p_retcode := 0;
      fnd_file.put_line (fnd_file.LOG, '/****************************/');
   EXCEPTION
      WHEN l_validation_error
      THEN
         p_retcode := 2;
         p_error_msg := l_error_msg || ' ' || SQLERRM;
         fnd_file.put_line (fnd_file.LOG, '/****************************/');
      WHEN OTHERS
      THEN
         p_retcode := 2;
         p_error_msg := l_error_msg || ' ' || SQLERRM;
         fnd_file.put_line (fnd_file.LOG, p_error_msg);
         fnd_file.put_line (fnd_file.LOG, '/****************************/');
   END validate_poco;


   PROCEDURE update_attributes (p_po_number             IN     VARCHAR2,
                                p_header_vendor_note    IN     VARCHAR2,
                                p_freight_acct_number   IN     VARCHAR2,
                                p_line_number           IN     NUMBER,
                                p_item_desc             IN     VARCHAR2,
                                p_retcode                  OUT VARCHAR2,
                                p_error_msg                OUT VARCHAR2)
   /*************************************************************************
     $Header XXWC_PO_EDI_FLAG_RESET_PKG.pkb $
     Module Name : XXWC_PO_EDI_FLAG_RESET_PKG.update_attributes

     PURPOSE     : Procedure to validate if the PO can be updated to resend POCO
     TMS Task Id : 20140516-00168

     REVISIONS:
     Ver        Date          Author                Description
     ---------  ----------    ------------------    ----------------
     1.0        18-Feb-2015   Manjula Chellappan    Initial Version
	 1.1        14-Oct-2015   M Hari Prasad         TMS # 20150911-00044 -- Research issue with XXWC PO EDI Reset Program.Added replace function for special charecters.

   **************************************************************************/
   IS
      l_po_header_id           NUMBER;
      l_po_line_id             NUMBER;
      l_po_number              VARCHAR2 (20);
      l_line_number            NUMBER;
      l_edi_processed_flag     VARCHAR2 (1);
      l_authorization_status   VARCHAR2 (30);
      l_header_cancel_flag     VARCHAR2 (1);
      l_header_closed_code     VARCHAR2 (40);
      l_print_count            NUMBER;
      l_revision_num           NUMBER;
      l_item_desc              VARCHAR2 (240);
      l_header_vendor_note     VARCHAR2 (480);
      l_freight_acct_number    VARCHAR2 (150);
      l_retcode                NUMBER;
      l_error_msg              VARCHAR2 (2000);
      l_validation_error       EXCEPTION;
   BEGIN
      fnd_file.put_line (fnd_file.LOG, '/****************************/');

      fnd_file.put_line (fnd_file.LOG, 'Update Attributes');

      BEGIN
             SELECT ph.po_header_id,
                    ph.segment1,
                    ph.edi_processed_flag,
                    ph.print_count,
                    ph.revision_num,
                    ph.note_to_vendor,
                    ph.attribute5,
                    ph.authorization_status,
                    ph.cancel_flag,
                    ph.closed_code
               INTO l_po_header_id,
                    l_po_number,
                    l_edi_processed_flag,
                    l_print_count,
                    l_revision_num,
                    l_header_vendor_note,
                    l_freight_acct_number,
                    l_authorization_status,
                    l_header_cancel_flag,
                    l_header_closed_code
               FROM po_headers ph
              WHERE ph.segment1 = p_po_number
         FOR UPDATE NOWAIT;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_error_msg :=
                  l_error_msg
               || ' '
               || 'PO '
               || p_po_number
               || ' Can not be found'
               || ' '
               || SQLERRM;

            fnd_file.put_line (fnd_file.LOG, l_error_msg);

            RAISE l_validation_error;
      END;


      IF p_line_number IS NOT NULL
      THEN
         BEGIN
                 select PL.PO_LINE_ID, PL.LINE_NUM, replace(replace(replace(PL.ITEM_DESCRIPTION,'�',' '),'"',''),'�','') --Added for ver # 1.1
                  into L_PO_LINE_ID, L_LINE_NUMBER, L_ITEM_DESC
                  from PO_LINES_all PL
                 WHERE     pl.po_header_id = l_po_header_id
                       AND pl.line_num = p_line_number
            FOR UPDATE NOWAIT;
         EXCEPTION
            WHEN OTHERS
            THEN
               fnd_file.put_line (
                  fnd_file.LOG,
                     'Line details can not be found for Line :'
                  || p_line_number
                  || ' '
                  || SQLERRM);
         END;
      END IF;


      fnd_file.put_line (fnd_file.LOG, 'Values to update');

      fnd_file.put_line (fnd_file.LOG, '/****************************/');


      fnd_file.put_line (
         fnd_file.LOG,
         'Header Vendor Note       :' || l_header_vendor_note);

      fnd_file.put_line (
         fnd_file.LOG,
         'Freight Acct Num         :' || l_freight_acct_number);

      fnd_file.put_line (fnd_file.LOG,
                         'Item description         :' || l_item_desc);

      fnd_file.put_line (fnd_file.LOG, '/****************************/');


      -- Check if the values are NOT NULL and different from Database

      IF     NVL (p_header_vendor_note, 'X') <>
                NVL (l_header_vendor_note, 'X')
         AND p_header_vendor_note IS NOT NULL
      THEN
         l_header_vendor_note := p_header_vendor_note;
      ELSE
         l_header_vendor_note := NULL;
      END IF;

      IF     NVL (p_freight_acct_number, 'X') <>
                NVL (l_freight_acct_number, 'X')
         AND p_freight_acct_number IS NOT NULL
      THEN
         l_freight_acct_number := p_freight_acct_number;
      ELSE
         l_freight_acct_number := NULL;
      END IF;

      IF     NVL (p_item_desc, 'X') <> NVL (l_item_desc, 'X')
         AND p_item_desc IS NOT NULL
      THEN
         l_item_desc := p_item_desc;
      ELSE
         l_item_desc := NULL;
      END IF;


      -- Update the attributes only if they can be sent via POO or POCO

      Validate_poo (p_po_number, l_retcode, l_error_msg);

      IF l_retcode = 2
      THEN
         Validate_poco (p_po_number, l_retcode, l_error_msg);

         IF l_retcode = 2
         THEN
            l_error_msg :=
                  l_error_msg
               || ' '
               || 'PO '
               || p_po_number
               || ' can not be sent via EDI POO or POCO, So attributes cannot be updated ';

            RAISE l_validation_error;
         END IF;
      END IF;



      -- Update PO_HEADERS

      IF    l_freight_acct_number IS NOT NULL
         OR l_header_vendor_note IS NOT NULL
      THEN
         BEGIN
            UPDATE po_headers
               SET note_to_vendor = NVL (l_header_vendor_note, note_to_vendor),
                   attribute5 = NVL (l_freight_acct_number, attribute5)
             WHERE po_header_id = l_po_header_id;

            fnd_file.put_line (fnd_file.LOG, 'PO Header details Updated ');
         EXCEPTION
            WHEN OTHERS
            THEN
               l_error_msg :=
                  l_error_msg || ' ' || l_error_msg || ' ' || SQLERRM;
               l_retcode := 2;

               fnd_file.put_line (
                  fnd_file.LOG,
                  'PO Header details not Updated ' || l_error_msg);

               RAISE l_validation_error;
         END;
      ELSE
         fnd_file.put_line (
            fnd_file.LOG,
            'PO Header details not Updated as the values are same ');
      END IF;


      -- Update PO_LINES


      IF l_item_Desc IS NOT NULL
      THEN
         BEGIN
            UPDATE po_lines
               SET item_description = l_item_desc
             WHERE po_line_id = l_po_line_id;

            fnd_file.put_line (fnd_file.LOG, 'PO Line details Updated ');
         EXCEPTION
            WHEN OTHERS
            THEN
               l_error_msg :=
                  l_error_msg || ' ' || l_error_msg || ' ' || SQLERRM;
               l_retcode := 2;

               fnd_file.put_line (
                  fnd_file.LOG,
                  'PO Line details not Updated ' || l_error_msg);

               RAISE l_validation_error;
         END;
      ELSE
         fnd_file.put_line (fnd_file.LOG, 'Line not updated ');
      END IF;

      COMMIT;

      p_retcode := 0;

      fnd_file.put_line (fnd_file.LOG, '/****************************/');
   EXCEPTION
      WHEN l_validation_error
      THEN
         p_retcode := 2;
         p_error_msg := l_error_msg;

         ROLLBACK;
   END update_attributes;


   PROCEDURE update_poo_flag (p_po_number   IN     VARCHAR2,
                              p_retcode        OUT VARCHAR2,
                              p_error_msg      OUT VARCHAR2)
   /*************************************************************************
     $Header XXWC_PO_EDI_FLAG_RESET_PKG.pkb $
     Module Name : XXWC_PO_EDI_FLAG_RESET_PKG.update_poo_flag

     PURPOSE     : Package to Reset the EDI related Fields for POO and POCO Retriggering
     TMS Task Id : 20140516-00168

     REVISIONS:
     Ver        Date          Author                Description
     ---------  ----------    ------------------    ----------------
     1.0        18-Feb-2015   Manjula Chellappan    Initial Version

   **************************************************************************/

   IS
      l_retcode            NUMBER;
      l_error_msg          VARCHAR2 (2000);

      l_validation_error   EXCEPTION;
   BEGIN
      validate_poo (p_po_number, l_retcode, l_error_msg);


      IF l_retcode = 0
      THEN
         BEGIN
            UPDATE po_headers
               SET edi_processed_flag = NULL,
                   print_count = 0,
                   printed_date = NULL
             WHERE segment1 = p_po_number;

            fnd_file.put_line (fnd_file.LOG, 'PO_HEADERS updated');

            BEGIN
               UPDATE po_headers_archive
                  SET edi_processed_flag = NULL,
                      print_count = 0,
                      printed_date = NULL
                WHERE segment1 = p_po_number AND latest_external_flag = 'Y';

               fnd_file.put_line (fnd_file.LOG, 'PO_HEADERS_ARCHIVE updated');
            EXCEPTION
               WHEN OTHERS
               THEN
                  p_retcode := 2;

                  l_error_msg :=
                     ' PO_HEADERS_ARCHIVE can not be updated ' || SQLERRM;

                  p_error_msg := l_error_msg;

                  fnd_file.put_line (fnd_file.LOG, l_error_msg);
            END;
         EXCEPTION
            WHEN OTHERS
            THEN
               p_retcode := 2;

               l_error_msg := ' PO_HEADERS can not be updated ' || SQLERRM;

               p_error_msg := l_error_msg;

               fnd_file.put_line (fnd_file.LOG, l_error_msg);
         END;
      END IF;

      COMMIT;
      p_retcode := 0;
   EXCEPTION
      WHEN l_validation_error
      THEN
         p_retcode := 2;
         p_error_msg := l_error_msg;

         ROLLBACK;
   END update_poo_flag;


   PROCEDURE update_poco_flag (p_po_number   IN     VARCHAR2,
                               p_retcode        OUT VARCHAR2,
                               p_error_msg      OUT VARCHAR2)
   /*************************************************************************
      $Header XXWC_PO_EDI_FLAG_RESET_PKG.pkb $
      Module Name : XXWC_PO_EDI_FLAG_RESET_PKG.update_poco_flag

      PURPOSE     : Package to Reset the EDI related Fields for POO and POCO Retriggering
      TMS Task Id : 20140516-00168

      REVISIONS:
      Ver        Date          Author                Description
      ---------  ----------    ------------------    ----------------
      1.0        18-Feb-2015   Manjula Chellappan    Initial Version

    **************************************************************************/
   IS
      l_retcode     NUMBER;
      l_error_msg   VARCHAR2 (2000);
   BEGIN
      validate_poco (p_po_number, l_retcode, l_error_msg);

      IF l_retcode = 0
      THEN
         BEGIN
            UPDATE po_headers
               SET print_count = print_count - 1
             WHERE segment1 = p_po_number;

            fnd_file.put_line (fnd_file.LOG, 'PO_HEADERS updated');

            BEGIN
               UPDATE po_headers_archive
                  SET edi_processed_flag = NULL,
                      print_count = print_count - 1
                WHERE segment1 = p_po_number AND latest_external_flag = 'Y';

               fnd_file.put_line (fnd_file.LOG, 'PO_HEADERS_ARCHIVE updated');
            EXCEPTION
               WHEN OTHERS
               THEN
                  p_retcode := 2;

                  l_error_msg :=
                     ' PO_HEADERS_ARCHIVE can not be updated ' || SQLERRM;

                  p_error_msg := l_error_msg;

                  fnd_file.put_line (fnd_file.LOG, l_error_msg);
                  ROLLBACK;
            END;
         EXCEPTION
            WHEN OTHERS
            THEN
               p_retcode := 2;

               l_error_msg := ' PO_HEADERS can not be updated ' || SQLERRM;
               p_error_msg := l_error_msg;

               fnd_file.put_line (fnd_file.LOG, l_error_msg);
               ROLLBACK;
         END;
      END IF;

      COMMIT;
      p_retcode := 0;
   EXCEPTION
      WHEN OTHERS
      THEN
         p_retcode := 2;
         p_error_msg := l_error_msg || SQLERRM;
         fnd_file.put_line (fnd_file.LOG, p_error_msg);
         ROLLBACK;
   END update_poco_flag;
END xxwc_po_edi_reset_pkg;
/