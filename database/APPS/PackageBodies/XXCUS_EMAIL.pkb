CREATE OR REPLACE PACKAGE BODY APPS."XXCUS_EMAIL" AS
  --
  FUNCTION eol RETURN VARCHAR2 IS
  BEGIN
    RETURN chr(13) || chr(10);
  END eol;
  --
  FUNCTION send(p_sender     IN VARCHAR2
               ,p_receipient IN VARCHAR2
               ,p_subject    IN VARCHAR2
               ,p_message    IN VARCHAR2) RETURN NUMBER IS
    --p_sender     VARCHAR2(255) ;
    l_mailhost  VARCHAR2(255) := 'hsim1.hsi.hughessupply.com'; --'hsim1';
    l_mail_conn utl_smtp.connection;
  BEGIN
    l_mail_conn := utl_smtp.open_connection(l_mailhost, 25);
    utl_smtp.helo(l_mail_conn, l_mailhost);
    utl_smtp.mail(l_mail_conn, p_sender);
    utl_smtp.rcpt(l_mail_conn, p_receipient);
    utl_smtp.open_data(l_mail_conn);
    utl_smtp.write_data(l_mail_conn,
                        'Subject:' || p_subject || eol || p_message);
    utl_smtp.close_data(l_mail_conn);
    utl_smtp.quit(l_mail_conn);
    RETURN 0;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN 1;
  END send;
  --
  PROCEDURE email_notify(p_process_name    IN VARCHAR2
                        ,p_system_code     IN VARCHAR2 DEFAULT NULL
                        ,p_subject         IN VARCHAR2
                        ,p_message         IN VARCHAR2
                        ,p_processs_status IN NUMBER
                        , -- 0=Success; Greater Than 0 = Error --
                         x_ret_status      OUT NUMBER -- 0=Success; Greater Than 0 = Error --
                         ) IS
    --
    CURSOR c1 IS
      SELECT receipient_on_error
            ,receipient_on_success
            ,nvl(email_sender, 'gl_error@hughessupply.com') email_sender
        FROM xxcus.xxcus_process_info
       WHERE process_short_name = p_process_name
         AND system_code = nvl(p_system_code, system_code)
         AND status = 'A';
    --
    l_receipient VARCHAR2(1000);
    l_sender     VARCHAR2(100);
    --
  BEGIN
    --
    FOR r1 IN c1
    LOOP
      --
      IF p_processs_status = 0 THEN
        IF r1.receipient_on_success IS NOT NULL THEN
          l_receipient := r1.receipient_on_success;
        END IF;
      ELSIF p_processs_status > 0 THEN
        IF r1.receipient_on_error IS NOT NULL THEN
          l_receipient := r1.receipient_on_error;
        END IF;
      END IF;
    
      IF l_receipient IS NOT NULL THEN
        /*
          DBMS_OUTPUT.PUT_LINE('Sending Mail .....');
          DBMS_OUTPUT.PUT_LINE('l_sender:'||r1.email_sender);
          DBMS_OUTPUT.PUT_LINE('l_receipient:'||l_receipient);
          DBMS_OUTPUT.PUT_LINE('l_subject:'||p_subject);
          DBMS_OUTPUT.PUT_LINE('l_message:'||p_message);
          
          Fnd_File.put_line (Fnd_File.LOG, 'Sending Mail from email notify procedure.....');
          Fnd_File.put_line (Fnd_File.LOG, 'l_sender:'||r1.email_sender);
          Fnd_File.put_line (Fnd_File.LOG, 'l_receipient:'||l_receipient);
          Fnd_File.put_line (Fnd_File.LOG, 'l_subject:'||p_subject);
          Fnd_File.put_line (Fnd_File.LOG, 'l_message:'||p_message);
        */
      
        x_ret_status := send(r1.email_sender, l_receipient, p_subject,
                             p_message);
        --x_ret_status := send('deep.singh@hughessupply.com','bidhu.prakash@hughessupply.com', p_subject, p_message);
        --x_ret_status := 0;
      ELSE
        dbms_output.put_line('l_receipient is null');
        x_ret_status := 1;
      END IF;
      --
    END LOOP;
    --
  
    --
  END;
  --
--
END xxcus_email;
/
