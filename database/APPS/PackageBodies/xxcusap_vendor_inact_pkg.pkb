CREATE OR REPLACE PACKAGE BODY APPS.xxcusap_vendor_inact_pkg IS

  -- Global variable declaration
  l_request_id             NUMBER := fnd_global.conc_request_id;
  l_program_application_id NUMBER := fnd_global.prog_appl_id;
  l_program_id             NUMBER := fnd_global.conc_program_id;
  l_err_callpoint          VARCHAR2(75);
  l_err_callfrom CONSTANT VARCHAR2(75) := 'XXCUSAP_VENDOR_INACT_PKG';
  l_distro_list VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';

  /*******************************************************************************
  * Description: the purpose of this program is to inactivate vendor sites
  *              provided by user/functional.  
  *
   ===============================================================================
     VERSION DATE         AUTHOR(S)       DESCRIPTION
     ------- -----------   --------------- --------------------------------------
     1.0     13-Nov-2012   Kathy Poling    Created this procedure.
     1.1     30-Apr-2013   Kathy Poling    Adding parmeters to send responsibility
                                           and org
                                           SR 190380   
     1.2     07-May-2015  Maharajan Shunmugam ESMS#287609 Mass Vendor Inactivation Tool - Break/Fix  
   ********************************************************************************/

  PROCEDURE vendor_inactive(errbuf           OUT VARCHAR2
                           ,retcode          OUT NUMBER
                           ,p_user           IN VARCHAR2
                           ,p_responsibility IN VARCHAR2
                           ,p_org            IN VARCHAR2) IS
                           
    --Variable declaration
    v_difference_count NUMBER := 0;
    v_data_count       NUMBER;
    v_count            NUMBER;
  
    l_req_id    NUMBER NULL;
    l_complete  VARCHAR2(20);
    l_argument1 VARCHAR2(255);
    l_argument2 VARCHAR2(255);
    l_argument3 VARCHAR2(255);
    l_org       hr_all_organization_units.organization_id%TYPE;
  
    -- Error DEBUG
    l_err_procedure VARCHAR2(75) DEFAULT 'VENDOR_ALL_INACTIVE';
    l_err_msg       VARCHAR2(3000);
    l_err_code      NUMBER;
    l_sec           VARCHAR2(255);
    l_message       VARCHAR2(150);
    l_statement     VARCHAR2(9000);
  
  BEGIN
  
    -- process all the records with complete_flag of NEW or error in staging table
    -- 
    FOR c1 IN (SELECT xvsu.rowid
                     ,xvsu.vendor_number
                     ,xvsu.vendor_site_code
                     ,s.vendor_id
                     ,s.vendor_name
                     ,s.vendor_type_lookup_code
                     ,nvl(s.end_date_active, NULL) end_date_active
                     ,(SELECT ss.vendor_site_id
                         FROM ap.ap_supplier_sites_all ss
                        WHERE ss.vendor_id = s.vendor_id
                          AND ss.vendor_site_code = xvsu.vendor_site_code
			  AND ss.org_id = ou.organization_id) vendor_site_id  --Added org id condition for ver#1.2
                     ,ou.organization_id
                 FROM xxcus.xxcusap_vendor_inact_tbl xvsu
                     ,ap.ap_suppliers                s
                     ,apps.hr_operating_units        ou
                WHERE xvsu.complete_flag IN ('ERROR', 'N')
                  AND xvsu.vendor_number = s.segment1(+)
                  AND xvsu.organization_name = ou.name(+)
                  and xvsu.organization_name = p_org)
    LOOP
    
      l_argument1 := c1.vendor_number;
      l_argument2 := c1.vendor_site_id;
      l_argument3 := c1.vendor_site_code;
    
      l_sec := 'Updating Vendor Info in staging table';
    
      IF c1.end_date_active IS NOT NULL
      THEN
        UPDATE xxcus.xxcusap_vendor_inact_tbl
           SET complete_flag     = 'ALL READY PROCESSED'
              ,output_message    = 'Vendor inactived already'
              ,last_update_date  = SYSDATE
              ,last_updated_by   = fnd_global.user_id
              ,last_update_login = fnd_global.login_id
              ,update_by_user    = p_user
         WHERE ROWID = c1.rowid;
      
      ELSIF c1.organization_id IS NULL
      THEN
        UPDATE xxcus.xxcusap_vendor_inact_tbl
           SET complete_flag     = 'ERROR'
              ,output_message    = 'Organization Name is invalid'
              ,last_update_date  = SYSDATE
              ,last_updated_by   = fnd_global.user_id
              ,last_update_login = fnd_global.login_id
              ,update_by_user    = p_user
         WHERE ROWID = c1.rowid;
      
      ELSIF c1.vendor_id IS NULL
      THEN
        UPDATE xxcus.xxcusap_vendor_inact_tbl
           SET complete_flag     = 'ERROR'
              ,output_message    = 'Vendor is invalid'
              ,last_update_date  = SYSDATE
              ,last_updated_by   = fnd_global.user_id
              ,last_update_login = fnd_global.login_id
              ,update_by_user    = p_user
         WHERE ROWID = c1.rowid;
      
      ELSIF c1.vendor_site_id IS NULL
      THEN
        UPDATE xxcus.xxcusap_vendor_inact_tbl
           SET complete_flag     = 'ERROR'
              ,output_message    = 'Site Code is invalid'
              ,vendor_id         = c1.vendor_id
              ,vendor_name       = c1.vendor_name
              ,last_update_date  = SYSDATE
              ,last_updated_by   = fnd_global.user_id
              ,last_update_login = fnd_global.login_id
              ,update_by_user    = p_user
         WHERE ROWID = c1.rowid;
      
      ELSE
        UPDATE xxcus.xxcusap_vendor_inact_tbl
           SET vendor_id         = c1.vendor_id
              ,vendor_name       = c1.vendor_name
              ,vendor_type       = c1.vendor_type_lookup_code
              ,vendor_site_id    = c1.vendor_site_id
              ,vendor_end_date   = c1.end_date_active
              ,last_update_date  = SYSDATE
              ,last_updated_by   = fnd_global.user_id
              ,last_update_login = fnd_global.login_id
              ,update_by_user    = p_user
         WHERE ROWID = c1.rowid;
      
        SELECT org_id
          INTO l_org
          FROM ap.ap_supplier_sites_all
         WHERE vendor_site_id = c1.vendor_site_id;
      
        IF c1.organization_id = l_org
        THEN
        
          l_sec := 'Calling API to Inactive';
        
          update_ven_site_inact_date(l_err_msg
                                    ,l_err_code
                                    ,c1.vendor_site_id
                                    ,c1.vendor_id
                                    ,c1.organization_id
                                    ,l_message);
          IF l_message = 'PROCESSED'
          THEN
            l_complete := 'PROCESSED';
          ELSE
            l_complete := 'ERROR';
          END IF;
        
          UPDATE xxcus.xxcusap_vendor_inact_tbl
             SET complete_flag     = l_complete
                ,output_message    = l_message
                ,last_update_date  = SYSDATE
                ,last_updated_by   = fnd_global.user_id
                ,last_update_login = fnd_global.login_id
                ,update_by_user    = p_user
           WHERE ROWID = c1.rowid;
        END IF;
      
      END IF;
    
    END LOOP;
  
    fnd_file.put_line(fnd_file.log, 'Process completed.');
  
  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;
      l_err_code := 2;
      l_err_msg  := l_sec;
      l_err_msg  := l_err_msg || ' ERROR ' || substr(SQLERRM, 1, 2000);
      --dbms_output.put_line(l_sec);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode         := l_err_code;
      errbuf          := l_err_msg;
      l_err_callpoint := l_sec;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom || '.' ||
                                                                  l_err_procedure
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => substr(SQLERRM
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'AP'
                                          ,p_argument1         => l_argument1
                                          ,p_argument2         => l_argument2
                                          ,p_argument3         => l_argument3);
    
    WHEN OTHERS THEN
      ROLLBACK;
      --dbms_output.put_line(l_sec);
    
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := l_sec || l_err_msg || ' ERROR ' || SQLCODE ||
                    substr(SQLERRM, 1, 2000);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode         := l_err_code;
      errbuf          := l_err_msg;
      l_err_callpoint := l_sec;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom || '.' ||
                                                                  l_err_procedure
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => substr(SQLERRM
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'AP'
                                          ,p_argument1         => l_argument1
                                          ,p_argument2         => l_argument2
                                          ,p_argument3         => l_argument3);
  END vendor_inactive;

  /*******************************************************************************
  
  * |----------------------< UPDATE_VEN_SITE_INACT_DATE >------------------------|
  * -----------------------------------------------------------------------------
  *
  * {Start Of Comments}
  *
  * Description:
  *   This procedure updates the vendor site table po_vendor_sites_all to inactivate 
  *   the vendor site.
  *
   ===============================================================================
    VERSION DATE         AUTHOR(S)       DESCRIPTION
    ------- -----------   --------------- --------------------------------------
    1.0     13-Nov-2012   Kathy Poling    Created this procedure.
    1.1     31-Jan-2013   Kathy Poling    Removed changing the enabled_flag.  User
                                          don't have access thru application to  
                                          re-activate vendor. SR 187934
  ********************************************************************************/

  PROCEDURE update_ven_site_inact_date(errbuf           OUT VARCHAR2
                                      ,retcode          OUT VARCHAR2
                                      ,p_vendor_site_id IN NUMBER
                                      ,p_vendor_id      IN NUMBER
                                      ,p_org            IN NUMBER
                                      ,p_message        OUT VARCHAR2) IS
  
    l_vendor_rec      ap_vendor_pub_pkg.r_vendor_rec_type;
    l_vendor_site_rec ap_vendor_pub_pkg.r_vendor_site_rec_type;
    l_return_status   VARCHAR2(2000);
    l_msg_count       NUMBER;
    l_msg_data        VARCHAR2(2000);
    l_curr_date       DATE;
  
    v_count NUMBER;
  
  BEGIN
    l_curr_date := trunc(SYSDATE);
    p_message   := NULL;
  
    l_vendor_site_rec.org_id        := p_org;
    l_vendor_site_rec.vendor_id     := p_vendor_id;
    l_vendor_site_rec.inactive_date := l_curr_date;
  
    ap_vendor_pub_pkg.update_vendor_site(p_api_version      => 1
                                        ,p_init_msg_list    => fnd_api.g_true
                                        ,p_commit           => fnd_api.g_false
                                        ,p_validation_level => fnd_api.g_valid_level_full
                                        ,x_return_status    => l_return_status
                                        ,x_msg_count        => l_msg_count
                                        ,x_msg_data         => l_msg_data
                                        ,p_vendor_site_rec  => l_vendor_site_rec
                                        ,p_vendor_site_id   => p_vendor_site_id);
  
    IF l_return_status = 'S'
    THEN
      p_message := 'PROCESSED';
    
      dbms_output.put_line('Vendor site id:  ' || p_vendor_site_id ||
                           '  -  ' || p_message);
    
      SELECT COUNT(*)
        INTO v_count
        FROM ap.ap_suppliers s
       WHERE s.vendor_id = p_vendor_id
         AND NOT EXISTS
       ( --  No Active sites
              SELECT 'Active Sites'
                FROM ap.ap_supplier_sites_all ss
               WHERE ss.vendor_id = s.vendor_id
                 AND ((ss.inactive_date IS NULL OR
                     ss.inactive_date > trunc(SYSDATE))));
    
      IF v_count = 1
      THEN
      
        l_vendor_rec.end_date_active := l_curr_date;
        --l_vendor_rec.enabled_flag    := 'N';   Version 1.1
      
        ap_vendor_pub_pkg.update_vendor(p_api_version      => 1.0
                                       ,p_init_msg_list    => fnd_api.g_true
                                       ,p_commit           => fnd_api.g_false
                                       ,p_validation_level => fnd_api.g_valid_level_full
                                       ,x_return_status    => l_return_status
                                       ,x_msg_count        => l_msg_count
                                       ,x_msg_data         => l_msg_data
                                       ,p_vendor_rec       => l_vendor_rec
                                       ,p_vendor_id        => p_vendor_id);
      
        dbms_output.put_line('Vendor id:  ' || p_vendor_id || '  -  ' ||
                             p_message);
      
      END IF;
    
    ELSE
      p_message := 'ERROR:  ' || l_msg_data;
    
      dbms_output.put_line('Vendor site id:  ' || p_vendor_site_id ||
                           '  -  ' || p_message);
    
    END IF;
  
  END update_ven_site_inact_date;

  /*************************************************************************
  Procedure:  submit_job
  
  Purpose:    Procedure called from procedure to initiate the vendor
              inactive Apex application for uploading Mass Vendor Inactivation. 
  HISTORY
  ===============================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- ------------------------------------
  1.0     21-Nov-2012        Kathy Poling    Initial creation of the procedure
  1.1     30-Apr-2013        Kathy Poling    Adding parmeters to send responsibility
                                             and org
                                             SR 190380
  ********************************************************************************/
  PROCEDURE submit_job(errbuf           OUT VARCHAR2
                      ,retcode          OUT VARCHAR2
                      ,p_user           IN VARCHAR2
                      ,p_responsibility IN VARCHAR2
                      ,p_org            IN VARCHAR2) IS
  
    --Intialize Variables
    l_err_msg            VARCHAR2(2000);
    l_err_code           NUMBER;
    l_sec                VARCHAR2(500);
    l_req_id             NUMBER;
    l_err_callfrom       VARCHAR2(75) := 'XXCUSAP_VENDOR_INACT_PKG.SUBMIT_JOB';
    l_distro_list        VARCHAR2(75) DEFAULT 'hdsoracledevelopers@hdsupply.com';
    --l_responsibilty      VARCHAR2(100) := 'HDS Payables Supplier Maintenance - US GSC';  Version 1.1
    l_user               VARCHAR2(100);
    l_can_submit_request BOOLEAN := TRUE;
    l_program             VARCHAR2(100) := 'XXCUSAP_VENDOR_INACT';   --Version 1.1
    l_app                 VARCHAR2(10) := 'XXCUS';                   --Version 1.1
    
  
    v_phase      VARCHAR2(50);
    v_status     VARCHAR2(50);
    v_dev_status VARCHAR2(50);
    v_dev_phase  VARCHAR2(50);
    v_message    VARCHAR2(250);
    v_interval NUMBER := 6; -- In seconds
    v_max_time NUMBER := 7500; -- In seconds
  
    v_error_message VARCHAR2(250);
    l_statement     VARCHAR2(250);
  
    --Start 
  BEGIN
  
    l_sec := 'Call Vendor Inactivation process; ';
  
    l_can_submit_request := xxcus_misc_pkg.set_responsibility(p_user
                                                             ,p_responsibility);
    IF l_can_submit_request
    THEN
      l_sec := 'Global Variables are set.';
    
    ELSE
    
      --l_globalset := 'Global Variables are not set.';
      l_sec := 'Global Variables are not set for the ' || p_user || '.';
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);

        UPDATE xxcus.xxcusap_vendor_inact_tbl
           SET complete_flag    = 'ERROR'
              ,output_message   = l_sec
              ,last_update_date = SYSDATE
         WHERE complete_flag = 'N'
           AND output_message IS NULL
           and organization_name = p_org;
        COMMIT;      
      RAISE program_error;
    END IF;
  
    -- Call concurrent request XXCUS AP VENDOR INACTIVATION
    l_req_id := fnd_request.submit_request(application => l_app
                                          ,program     => l_program
                                          ,description => NULL
                                          ,start_time  => NULL
                                          ,sub_request => FALSE
                                          ,argument1   => p_user
                                          ,argument2   => p_responsibility
                                          ,argument3   => p_org);
    COMMIT;
  
    IF (l_req_id != 0)
    THEN
      IF fnd_concurrent.wait_for_request(l_req_id
                                        ,v_interval
                                        ,v_max_time
                                        ,v_phase
                                        ,v_status
                                        ,v_dev_phase
                                        ,v_dev_status
                                        ,v_message)
      THEN
        l_sec := chr(10) || 'ReqID=' || l_req_id || ' DPhase ' ||
                 v_dev_phase || ' DStatus ' || v_dev_status || chr(10) ||
                 ' MSG - ' || v_message;
        -- Error Returned
        IF v_dev_phase != 'COMPLETE' OR v_dev_status != 'NORMAL'
        THEN
        
          fnd_file.put_line(fnd_file.log, l_sec);
          fnd_file.put_line(fnd_file.output, l_sec);
          RAISE program_error;
        
        END IF;
        -- Then Success!
        --retcode := 0;
      ELSE
        l_sec := 'XXCUS AP VENDOR INACTIVATION in XXCUSAP_VENDOR_INACT_PKG Wait Timed Out';
        fnd_file.put_line(fnd_file.log, l_sec);
        fnd_file.put_line(fnd_file.output, l_sec);
        RAISE program_error;
      END IF;
    
    ELSE
      l_sec := 'An error occured when submitting concurrent request XXCUS AP VENDOR INACTIVATION in XXCUSAP_VENDOR_INACT_PKG ';
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      RAISE program_error;
    END IF;
  
    dbms_output.put_line(l_sec);
    dbms_output.put_line('Request ID:  ' || l_req_id);
  
  EXCEPTION
    WHEN program_error THEN
      --ROLLBACK;
      l_err_code := 2;
      l_err_msg  := substr((l_err_msg || ' ERROR ' ||
                           substr(SQLERRM, 1, 2000))
                          ,1
                          ,3000);
      dbms_output.put_line(l_sec || ' - ' || l_err_msg);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_sec
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => substr(SQLERRM
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'AP');
    
    WHEN OTHERS THEN
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := substr((l_err_msg || ' ERROR ' ||
                           substr(SQLERRM, 1, 2000))
                          ,1
                          ,3000);
      dbms_output.put_line(l_sec || ' - ' || l_err_msg);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_sec
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => substr(SQLERRM
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'AP');
  END submit_job;

END xxcusap_vendor_inact_pkg;
/
