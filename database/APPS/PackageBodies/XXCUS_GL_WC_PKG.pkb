CREATE OR REPLACE PACKAGE BODY APPS."XXCUS_GL_WC_PKG" IS
  /********************************************************************************
  
  File Name: XXCUS_GL_WC_PKG
  
  PROGRAM TYPE: PL/SQL Package spec and body
  
  PURPOSE: Procedures and functions for creating the loads to WC Intranet
  
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     02/06/2012    John Bozik    Initial creation of the package for R12
  Modified for R12 from 11i version developed by Jason Sloan
  ********************************************************************************/

  /*******************************************************************************
  * Procedure:   create_wc_dw_hdr_ln_extract
  * Description: Writes GL Header and Line data used by Enterprise Translator (ET).
  *              ET will translate this data and create/perform the following:
  *              WCE_GL_HDR_<Period>_<date in YYYYMMDDHH24MISS format>.txt
  *              WCE_GL_ITEM_<Period>_<date in YYYYMMDDHH24MISS format>.txt that will be
  *              ftp'ed to WhiteCap California.  Auto version picks parms base on today.
  *
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)          DESCRIPTION
  ------- -----------   ---------------   -----------------------------------------
  1.0     02/06/2012    John Bozik        Initial creation of the package for R12
  2.0    07/06/2013     Balaguru Seshadri Added application_id to the detail table
        ESMS 205965                       to improve the performance 
  3.0    02/02/2014     Manny             modified to exlude bug from FYE -
                                          Service ticket -   237858
                                          RFC -           39268   
  3.1    09/29/2014     Kathy Poling      ESMS 265189 added code to remove control                                                   
                                          characters from line descr 
  3.2    09/30/2014     Kathy Poling      ESMS 265189 RFC 41754 remove nonASCII
                                          characters for the description                              
   ********************************************************************************/

  PROCEDURE create_wc_dw_hdr_ln_extr_auto(errbuf  OUT VARCHAR2,
                                          retcode OUT NUMBER) IS
  
    --Intialize Variables
    l_err_msg   VARCHAR2(2000);
    l_err_code  NUMBER;
    l_days_back NUMBER := '10'; --Days back to pull for extract
    lc_calendar CONSTANT gl_periods.period_set_name%TYPE := '4-4-QTR'; --Select the '4-4-QTR' calendar
    lc_coa      CONSTANT gl_code_combinations.chart_of_accounts_id%TYPE := '50328'; --HSI Main Chart of Accounts
    lc_ledgerid CONSTANT gl_period_statuses.ledger_id%TYPE := 2061; --HSI USD Ledger ID
    l_period      gl_balances.period_name%TYPE; --Will hold the period the extract is to be run for
    l_shortperiod gl_balances.period_name%TYPE; --Will hold the period the extract is to be run for
    l_extract_dt  DATE;
    l_seq_num     number := 0;
  
    --Error Handler Variables
    l_sec            VARCHAR2(4000);
    l_procedure_name VARCHAR2(100) := 'XXCUS_GL_WC_PKG.CREATE_WC_DW_HDR_LN_EXTR_AUTO';
    pl_dflt_email2   fnd_user.email_address%TYPE := 'hds.oracleglsupport@hdsupply.com';
  
    --Start Main Program
  BEGIN
    l_sec := 'Obtain Parameters; ';
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
  
    l_extract_dt := (SYSDATE);
  
    SELECT period_name, substr(period_name, 1, 3)
      INTO l_period, l_shortperiod
      FROM gl.gl_periods
     WHERE period_set_name = lc_calendar
       AND start_date <= trunc(SYSDATE)
       AND end_date >= trunc(SYSDATE)
       and period_name not like 'FYE%';  --v 3.0
  
    SELECT to_number(h.description)
      INTO l_days_back
      FROM apps.fnd_lookup_values_vl h
     WHERE h.lookup_type = 'HDS_WCEHDREXTRAUTO'
       AND h.meaning = 'DAYS_BACK';
  
    --Truncate data tables prior to load
    BEGIN
      l_sec := 'Truncate the Header table before loading specified time frame.';
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      
      EXECUTE IMMEDIATE 'TRUNCATE TABLE XXCUS.XXCUS_wc_dw_gl_hdr_t';
    EXCEPTION
      WHEN OTHERS THEN
        l_err_msg := 'Failed to Truncate XXCUS.XXCUS_wc_dw_gl_hdr_t: ' ||
                     SQLERRM;
        RAISE program_error;
    END;
  
    BEGIN
      l_sec := 'Truncate the Detail table before loading specified time frame.';
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      
      EXECUTE IMMEDIATE 'TRUNCATE TABLE XXCUS.XXCUS_wc_dw_gl_item_t';
    EXCEPTION
      WHEN OTHERS THEN
        l_err_msg := 'Failed to Truncate XXCUS.XXCUS_wc_dw_gl_item_t: ' ||
                     SQLERRM;
        RAISE program_error;
    END;
  
    --Insert Sequence
    Select XXCUS_WC_DW_SEQ.nextval into l_seq_num from dual;
  
    --Insert Header Table
    l_sec := 'Loading Header Table; ';
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
  
    INSERT INTO XXCUS.XXCUS_wc_dw_gl_hdr_t
      (SELECT DISTINCT l_seq_num,
                       b.je_header_id,
                       d.user_je_category_name,
                       substr(b.name, 1, 30) Name
         FROM gl.gl_je_lines          a,
              gl.gl_je_headers        b,
              gl.gl_code_combinations c,
              gl.gl_je_categories_tl  d
        WHERE a.code_combination_id = c.code_combination_id
          AND d.je_category_name = b.je_category
          AND a.je_header_id = b.je_header_id
          AND b.currency_code <> 'STAT'
          AND a.ledger_id = lc_ledgerid
          AND c.segment1 IN (SELECT h.description
                               FROM apps.fnd_lookup_values_vl h
                              WHERE h.lookup_type = 'HDS_WCEHDREXTRAUTO'
                                AND h.meaning LIKE 'SEG1_%')
          AND a.status = 'P'
          --following 2 lines were added for go live concerns of duplicating data
          AND b.period_name NOT LIKE ('%2011%')
          --AND b.period_name <> 'Feb-2012'
          AND b.period_name not in ('Feb-2012', 'May-2012')
          AND trunc(b.posted_date) >= (trunc(l_extract_dt) - l_days_back)
          AND trunc(b.posted_date) <= trunc(l_extract_dt)
          AND c.chart_of_accounts_id = lc_coa);
  
    --Insert Line Table
    l_sec := 'Loading LINE table; ';
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
  
 INSERT INTO XXCUS.XXCUS_wc_dw_gl_item_t
   
  (select l_seq_num,
          c.segment1,
          c.segment2,
          c.segment3,
          c.segment4,
          c.segment5,
          to_char(a.effective_date, 'YYYY-MM-DD') effective_date,
          a.je_header_id,
          a.je_line_num,
          --trim(substr(REGEXP_REPLACE (a.description, '[[:cntrl][:print:]]', NULL), 1, 50)) gl_line_descr,    --Version 3.1
          trim(substr(REGEXP_REPLACE(ASCIISTR(a.description), '\\[[:xdigit:]]{4}', ''),1,50)),  --Version 3.2
          a.attribute2,
          a.attribute3,
          a.attribute4,
          a.attribute5,
          a.attribute6,
          d.user_je_category_name,
          to_char(b.posted_date, 'YYYY-MM-DD') posted_date,
          CASE
            WHEN nvl(a.accounted_cr, 0) > nvl(a.accounted_dr, 0) THEN
             nvl(a.accounted_cr, 0) - nvl(a.accounted_dr, 0)
            WHEN nvl(a.accounted_dr, 0) > nvl(a.accounted_cr, 0) THEN
             nvl(a.accounted_dr, 0) - nvl(a.accounted_cr, 0)
            WHEN nvl(a.accounted_cr, 0) = nvl(a.accounted_dr, 0) THEN
             0
            ELSE
             0
          END AS accounted_amount,
          --NVL(DECODE(A.ENTERED_CR, NULL, A.ENTERED_DR, 0, A.ENTERED_DR, A.ENTERED_CR),0),
          CASE
            WHEN nvl(a.entered_cr, 0) > nvl(a.entered_dr, 0) THEN
             nvl(a.entered_cr, 0) - nvl(a.entered_dr, 0)
            WHEN nvl(a.entered_dr, 0) > nvl(a.entered_cr, 0) THEN
             nvl(a.entered_dr, 0) - nvl(a.entered_cr, 0)
            WHEN nvl(a.entered_cr, 0) = nvl(a.entered_dr, 0) THEN
             0
            ELSE
             0
          END as entered_amount,
          --DECODE(A.ENTERED_CR, NULL, 'D', 0,'D','C'),
          CASE
            WHEN nvl(a.accounted_cr, 0) > nvl(a.accounted_dr, 0) THEN
             'C'
            WHEN nvl(a.accounted_dr, 0) > nvl(a.accounted_cr, 0) THEN
             'D'
            WHEN nvl(a.accounted_cr, 0) = nvl(a.accounted_dr, 0) THEN
             'C'
            ELSE
             'C'
          END as accounted_d_or_c
     FROM gl.gl_je_lines          a,
          gl.gl_je_headers        b,
          gl.gl_code_combinations c,
          gl.gl_je_categories_tl  d
    WHERE a.code_combination_id = c.code_combination_id
      AND a.je_header_id = b.je_header_id
      AND b.currency_code <> 'STAT'
      AND b.je_source <> 'Payables'
      AND a.ledger_id = lc_ledgerid
      AND d.je_category_name = b.je_category
      AND b.je_source NOT IN
          (SELECT h.description
             FROM apps.fnd_lookup_values_vl h
            WHERE h.lookup_type = 'HDS_WCEHDREXTRAUTO'
              AND h.meaning LIKE 'JRNL_SRC_%')
      AND c.segment1 IN (SELECT h.description
                           FROM apps.fnd_lookup_values_vl h
                          WHERE h.lookup_type = 'HDS_WCEHDREXTRAUTO'
                            AND h.meaning LIKE 'SEG1_%')
      AND a.status = 'P'
         --following 2 lines were added for go live concerns of duplicating data
      AND b.period_name NOT LIKE ('%2011%')
      --AND b.period_name <> 'Feb-2012'
      AND b.period_name not in ('Feb-2012', 'May-2012')
      AND trunc(b.posted_date) >= (trunc(l_extract_dt) - l_days_back)
      AND trunc(b.posted_date) <= trunc(l_extract_dt)
      AND c.chart_of_accounts_id = lc_coa
   UNION ALL
   SELECT l_seq_num,
          gcc.segment1,
          gcc.segment2,
          gcc.segment3,
          gcc.segment4,
          gcc.segment5,
          to_char(glh.default_effective_date, 'YYYY-MM-DD') effective_date,
          gir.je_header_id,
          gir.je_line_num,
         /* trim(substr(REGEXP_REPLACE ((i.invoice_num || ' - ' || s.party_name || ' - ' ||
                 t.description), '[[:cntrl][:print:]]', NULL),
                 1,
                 230)) gl_line_descr,     --Version 3.1
          */       
          trim(substr(REGEXP_REPLACE(ASCIISTR(i.invoice_num || ' - ' || s.party_name || ' - ' || t.description), '\\[[:xdigit:]]{4}', ''), 1, 230)),  --Version 3.2       
          NULL ATTRIBUTE2,
          NULL ATTRIBUTE3,
          NULL ATTRIBUTE4,
          NULL ATTRIBUTE5,
          NULL ATTRIBUTE6,
          glh.je_category,
          to_char(glh.posted_date, 'YYYY-MM-DD') posted_date,
          CASE
            WHEN nvl(t.accounted_cr, 0) > nvl(t.accounted_dr, 0) THEN
             nvl(t.accounted_cr, 0) - nvl(t.accounted_dr, 0)
            WHEN nvl(t.accounted_dr, 0) > nvl(t.accounted_cr, 0) THEN
             nvl(t.accounted_dr, 0) - nvl(t.accounted_cr, 0)
            WHEN nvl(t.accounted_cr, 0) = nvl(t.accounted_dr, 0) THEN
             0
            ELSE
             0
          END AS accounted_amount,
          --NVL(DECODE(A.ENTERED_CR, NULL, A.ENTERED_DR, 0, A.ENTERED_DR, A.ENTERED_CR),0),
          CASE
            WHEN nvl(t.entered_cr, 0) > nvl(t.entered_dr, 0) THEN
             nvl(t.entered_cr, 0) - nvl(t.entered_dr, 0)
            WHEN nvl(t.entered_dr, 0) > nvl(t.entered_cr, 0) THEN
             nvl(t.entered_dr, 0) - nvl(t.entered_cr, 0)
            WHEN nvl(t.entered_cr, 0) = nvl(t.entered_dr, 0) THEN
             0
            ELSE
             0
          END as entered_amount,
          --DECODE(A.ENTERED_CR, NULL, 'D', 0,'D','C'),
          CASE
            WHEN nvl(t.accounted_cr, 0) > nvl(t.accounted_dr, 0) THEN
             'C'
            WHEN nvl(t.accounted_dr, 0) > nvl(t.accounted_cr, 0) THEN
             'D'
            WHEN nvl(t.accounted_cr, 0) = nvl(t.accounted_dr, 0) THEN
             'C'
            ELSE
             'C'
          END as accounted_d_or_c
     FROM xla.xla_ae_lines        t,
          xla.xla_ae_headers      h,
          gl.gl_import_references gir,
          gl.gl_code_combinations gcc,
          ap.ap_invoices_all      i,
          ar.hz_parties         s,
          gl.gl_je_headers        glh
    WHERE 1 =1
      AND h.application_id =200 --SBala 07/09/2013
      AND t.application_id =h.application_id --SBala 07/09/2013    
      AND t.ae_header_id = h.ae_header_id
      AND glh.je_source = 'Payables'
      AND gir.je_header_id = glh.je_header_id
      AND t.gl_sl_link_id = gir.gl_sl_link_id
      AND h.event_id IN (SELECT DISTINCT accounting_event_id
                           FROM ap.ap_invoice_distributions_all d
                          WHERE d.accounting_event_id = h.event_id
                            AND d.invoice_id = i.invoice_id)
      AND i.party_id = s.party_id
      AND t.code_combination_id = gcc.code_combination_id
      AND gcc.segment1 IN (SELECT h.description
                             FROM apps.fnd_lookup_values_vl h
                            WHERE h.lookup_type = 'HDS_WCEHDREXTRAUTO'
                              AND h.meaning LIKE 'SEG1_%')
      AND glh.status = 'P'
      AND glh.period_name NOT LIKE ('%2011%')
      --AND glh.period_name <> 'Feb-2012'
      AND glh.period_name not in ('Feb-2012', 'May-2012')
      AND trunc(glh.posted_date) >= (trunc(l_extract_dt) - l_days_back)
      AND trunc(glh.posted_date) <= trunc(l_extract_dt)) 
UNION ALL
  SELECT l_seq_num,
         gcc.segment1,
         gcc.segment2,
         gcc.segment3,
         gcc.segment4,
         gcc.segment5,
         to_char(jh.default_effective_date, 'YYYY-MM-DD') effective_date,
         jh.je_header_id je_header_id,
         gir.je_line_num,
        /* trim(substr(REGEXP_REPLACE ((ai.invoice_num || ' - ' || ac.vendor_name || ' - ' ||
                aeh.event_type_code), '[[:cntrl][:print:]]', NULL),
                1,
                230)) gl_line_descr,    --Version 3.1
         */       
         trim(substr(REGEXP_REPLACE(ASCIISTR(ai.invoice_num || ' - ' || ac.vendor_name || ' - ' ||
                aeh.event_type_code), '\\[[:xdigit:]]{4}', ''), 1, 230)),  --Version 3.2       
         NULL ATTRIBUTE2,
         NULL ATTRIBUTE3,
         NULL ATTRIBUTE4,
         NULL ATTRIBUTE5,
         NULL ATTRIBUTE6,
         jh.je_category,
         to_char(jh.posted_date, 'YYYY-MM-DD') posted_date,
         CASE
           WHEN nvl(decode(adl.application_id,
                           null,
                           ael.entered_cr,
                           adl.unrounded_entered_cr),
                    0) > nvl(decode(adl.application_id,
                                    null,
                                    ael.entered_dr,
                                    adl.unrounded_entered_dr),
                             0) THEN
            nvl(decode(adl.application_id,
                       null,
                       ael.entered_cr,
                       adl.unrounded_entered_cr),
                0) - nvl(decode(adl.application_id,
                                null,
                                ael.entered_dr,
                                adl.unrounded_entered_dr),
                         0)
           WHEN nvl(decode(adl.application_id,
                           null,
                           ael.entered_dr,
                           adl.unrounded_entered_dr),
                    0) > nvl(decode(adl.application_id,
                                    null,
                                    ael.entered_cr,
                                    adl.unrounded_entered_cr),
                             0) THEN
            nvl(decode(adl.application_id,
                       null,
                       ael.entered_dr,
                       adl.unrounded_entered_dr),
                0) - nvl(decode(adl.application_id,
                                null,
                                ael.entered_cr,
                                adl.unrounded_entered_cr),
                         0)
           WHEN nvl(decode(adl.application_id,
                           null,
                           ael.entered_cr,
                           adl.unrounded_entered_cr),
                    0) = nvl(decode(adl.application_id,
                                    null,
                                    ael.entered_dr,
                                    adl.unrounded_entered_dr),
                             0) THEN
            0
           ELSE
            0
         END AS accounted_amount,
         --NVL(DECODE(A.ENTERED_CR, NULL, A.ENTERED_DR, 0, A.ENTERED_DR, A.ENTERED_CR),0),
         CASE
           WHEN nvl(decode(adl.application_id,
                           null,
                           ael.entered_cr,
                           adl.unrounded_entered_cr),
                    0) > nvl(decode(adl.application_id,
                                    null,
                                    ael.entered_dr,
                                    adl.unrounded_entered_dr),
                             0) THEN
            nvl(decode(adl.application_id,
                       null,
                       ael.entered_cr,
                       adl.unrounded_entered_cr),
                0) - nvl(decode(adl.application_id,
                                null,
                                ael.entered_dr,
                                adl.unrounded_entered_dr),
                         0)
           WHEN nvl(decode(adl.application_id,
                           null,
                           ael.entered_dr,
                           adl.unrounded_entered_dr),
                    0) > nvl(decode(adl.application_id,
                                    null,
                                    ael.entered_cr,
                                    adl.unrounded_entered_cr),
                             0) THEN
            nvl(decode(adl.application_id,
                       null,
                       ael.entered_dr,
                       adl.unrounded_entered_dr),
                0) - nvl(decode(adl.application_id,
                                null,
                                ael.entered_cr,
                                adl.unrounded_entered_cr),
                         0)
           WHEN nvl(decode(adl.application_id,
                           null,
                           ael.entered_cr,
                           adl.unrounded_entered_cr),
                    0) = nvl(decode(adl.application_id,
                                    null,
                                    ael.entered_dr,
                                    adl.unrounded_entered_dr),
                             0) THEN
            0
           ELSE
            0
         END as entered_amount,
         --DECODE(A.ENTERED_CR, NULL, 'D', 0,'D','C'),
         CASE
           WHEN nvl(decode(adl.application_id,
                           null,
                           ael.entered_cr,
                           adl.unrounded_entered_cr),
                    0) > nvl(decode(adl.application_id,
                                    null,
                                    ael.entered_dr,
                                    adl.unrounded_entered_dr),
                             0) THEN
            'C'
           WHEN nvl(decode(adl.application_id,
                           null,
                           ael.entered_dr,
                           adl.unrounded_entered_dr),
                    0) > nvl(decode(adl.application_id,
                                    null,
                                    ael.entered_cr,
                                    adl.unrounded_entered_cr),
                             0) THEN
            'D'
           WHEN nvl(decode(adl.application_id,
                           null,
                           ael.entered_cr,
                           adl.unrounded_entered_cr),
                    0) = nvl(decode(adl.application_id,
                                    null,
                                    ael.entered_dr,
                                    adl.unrounded_entered_dr),
                             0) THEN
            'C'
           ELSE
            'C'
         END as accounted_d_or_c
  
    FROM gl_ledgers               le,
         gl_je_batches            jb,
         gl_je_headers            jh,
         gl_je_lines              jl,
         gl_import_references     gir,
         gl_code_combinations_kfv gcc,
         gl_je_sources            jes,
         gl_je_categories         jec,
         gl_budget_versions       gbv,
         xla_ae_lines             ael,
         gl_code_combinations_kfv gcc1,
         xla_ae_headers           aeh,
         xla_distribution_links   adl,
         ap_payment_hist_dists    aphd,
         ap_invoice_payments_all  aip,
         ap_invoices_all          ai,
         ap_checks_all            ac,
         po_vendors               pov
  -- to have multiple outer joins.. used for ap_invoice_payments.check_id
   WHERE 1 =1
     and aeh.application_id =200 --SBala 07/09/2013
     and ael.application_id =aeh.application_id --SBala 07/09/2013     
     and adl.application_id =aeh.application_id --SBala 07/09/2013  
     AND le.ledger_id = jh.ledger_id
     AND jb.je_batch_id = jh.je_batch_id
     AND jh.je_header_id = jl.je_header_id
     AND gir.je_header_id = jh.je_header_id
     AND gir.je_line_num = jl.je_line_num
     AND jl.code_combination_id = gcc.code_combination_id
     AND jh.je_source = jes.je_source_name
     AND jh.je_category = jec.je_category_name
     AND jh.budget_version_id = gbv.budget_version_id(+)
     AND gir.gl_sl_link_id = ael.gl_sl_link_id(+)
     AND gir.gl_sl_link_table = ael.gl_sl_link_table(+)
     AND ael.ae_header_id = aeh.ae_header_id(+)
     AND ael.code_combination_id = gcc1.code_combination_id(+)
     And ael.ae_header_id = adl.ae_header_id(+)
     And ael.ae_line_num = adl.ae_line_num(+)
     And adl.source_distribution_id_num_1 = aphd.payment_hist_dist_id(+)
     And aphd.invoice_payment_id = aip.invoice_payment_id(+)
     And aip.check_id = ac.check_id(+)
     AND aip.invoice_id = ai.invoice_id(+)
     AND pov.vendor_id = ai.vendor_id
     AND jh.je_source = 'Payables'
     AND jh.je_category = 'Payments'
     AND le.object_type_code = 'L'
     AND jh.status = 'P'
     AND gcc.segment1 IN (SELECT h.description
                            FROM apps.fnd_lookup_values_vl h
                           WHERE h.lookup_type = 'HDS_WCEHDREXTRAUTO'
                             AND h.meaning LIKE 'SEG1_%')
     AND jh.status = 'P'
     AND jh.period_name NOT LIKE ('%2011%')
     --AND jh.period_name <> 'Feb-2012'
     AND jh.period_name not in ('Feb-2012', 'May-2012')
     AND trunc(jh.posted_date) >= (trunc(l_extract_dt) - l_days_back)
     AND trunc(jh.posted_date) <= trunc(l_extract_dt);

COMMIT;
  
  EXCEPTION
  
    WHEN program_error THEN
      ROLLBACK;
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := 'Error in program ' || l_procedure_name || ' Error: ' ||
                    l_sec;
      l_err_msg  := l_err_msg || ' ERROR ' || substr(SQLERRM, 1, 2000);
    
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode := l_err_code;
      errbuf  := l_err_msg;
    
      XXCUS_error_pkg.XXCUS_error_main_api(p_called_from       => l_procedure_name,
                                           p_calling           => l_sec,
                                           p_request_id        => fnd_global.conc_request_id,
                                           p_ora_error_msg     => SQLERRM,
                                           p_error_desc        => 'Error running WhiteCap GL Header Line Extract, custom error.',
                                           p_distribution_list => pl_dflt_email2,
                                           p_module            => 'GL');
      COMMIT;
    
    WHEN OTHERS THEN
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := l_sec || l_err_msg || ' ERROR ' || SQLCODE ||
                    substr(SQLERRM, 1, 2000);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode := l_err_code;
      errbuf  := l_err_msg;
    
      XXCUS_error_pkg.XXCUS_error_main_api(p_called_from       => l_procedure_name,
                                           p_calling           => l_sec,
                                           p_request_id        => fnd_global.conc_request_id,
                                           p_ora_error_msg     => SQLERRM,
                                           p_error_desc        => 'Error running WhiteCap GL Header Line Extract, others error.',
                                           p_distribution_list => pl_dflt_email2,
                                           p_module            => 'GL');
    
      COMMIT;
    
  END create_wc_dw_hdr_ln_extr_auto;
END xxcus_gl_wc_pkg;
/
